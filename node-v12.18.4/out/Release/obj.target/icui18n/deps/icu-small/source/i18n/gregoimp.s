	.file	"gregoimp.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ClockMath11floorDivideEii
	.type	_ZN6icu_679ClockMath11floorDivideEii, @function
_ZN6icu_679ClockMath11floorDivideEii:
.LFB2449:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	testl	%edi, %edi
	js	.L2
	cltd
	idivl	%esi
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	addl	$1, %eax
	cltd
	idivl	%esi
	subl	$1, %eax
	ret
	.cfi_endproc
.LFE2449:
	.size	_ZN6icu_679ClockMath11floorDivideEii, .-_ZN6icu_679ClockMath11floorDivideEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ClockMath11floorDivideEll
	.type	_ZN6icu_679ClockMath11floorDivideEll, @function
_ZN6icu_679ClockMath11floorDivideEll:
.LFB2450:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	testq	%rdi, %rdi
	js	.L6
	cqto
	idivq	%rsi
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$1, %rax
	cqto
	idivq	%rsi
	subq	$1, %rax
	ret
	.cfi_endproc
.LFE2450:
	.size	_ZN6icu_679ClockMath11floorDivideEll, .-_ZN6icu_679ClockMath11floorDivideEll
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ClockMath11floorDivideEdiRi
	.type	_ZN6icu_679ClockMath11floorDivideEdiRi, @function
_ZN6icu_679ClockMath11floorDivideEdiRi:
.LFB2451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$24, %rsp
	movsd	%xmm0, -32(%rbp)
	divsd	%xmm2, %xmm0
	movsd	%xmm2, -24(%rbp)
	call	uprv_floor_67@PLT
	movsd	-24(%rbp), %xmm2
	movsd	-32(%rbp), %xmm1
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	cvttsd2sil	%xmm1, %eax
	movl	%eax, (%rbx)
	addq	$24, %rsp
	cvttsd2sil	%xmm0, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2451:
	.size	_ZN6icu_679ClockMath11floorDivideEdiRi, .-_ZN6icu_679ClockMath11floorDivideEdiRi
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ClockMath11floorDivideEddRd
	.type	_ZN6icu_679ClockMath11floorDivideEddRd, @function
_ZN6icu_679ClockMath11floorDivideEddRd:
.LFB2452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movsd	%xmm0, -32(%rbp)
	divsd	%xmm1, %xmm0
	movsd	%xmm1, -24(%rbp)
	call	uprv_floor_67@PLT
	movsd	-24(%rbp), %xmm1
	movsd	-32(%rbp), %xmm2
	pxor	%xmm4, %xmm4
	movapd	%xmm1, %xmm3
	movapd	%xmm2, %xmm5
	mulsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm5
	comisd	%xmm5, %xmm4
	ja	.L15
	comisd	%xmm1, %xmm5
	jb	.L19
	movsd	.LC1(%rip), %xmm3
	addsd	%xmm0, %xmm3
	ucomisd	%xmm0, %xmm3
	jp	.L13
.L20:
	jne	.L13
	movq	$0x000000000, (%rbx)
	addq	$24, %rsp
	movapd	%xmm3, %xmm0
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movsd	%xmm5, (%rbx)
	movapd	%xmm0, %xmm3
	addq	$24, %rsp
	movapd	%xmm3, %xmm0
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movsd	.LC0(%rip), %xmm3
	addsd	%xmm0, %xmm3
	ucomisd	%xmm0, %xmm3
	jnp	.L20
.L13:
	mulsd	%xmm3, %xmm1
	movapd	%xmm3, %xmm0
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2452:
	.size	_ZN6icu_679ClockMath11floorDivideEddRd, .-_ZN6icu_679ClockMath11floorDivideEddRd
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Grego11fieldsToDayEiii
	.type	_ZN6icu_675Grego11fieldsToDayEiii, @function
_ZN6icu_675Grego11fieldsToDayEiii:
.LFB2453:
	.cfi_startproc
	endbr64
	leal	-1(%rdi), %r9d
	movl	%edx, %ecx
	imull	$365, %r9d, %edx
	testl	%r9d, %r9d
	js	.L22
	movl	%r9d, %eax
	sarl	$2, %eax
	leal	1721423(%rdx,%rax), %r8d
	movslq	%r9d, %rax
	sarl	$31, %r9d
	imulq	$1374389535, %rax, %rax
	movq	%rax, %rdx
	sarq	$37, %rax
	sarq	$39, %rdx
	subl	%r9d, %eax
	subl	%r9d, %edx
	addl	%r8d, %edx
.L23:
	subl	%eax, %edx
	leal	2(%rdx), %eax
	testb	$3, %dil
	jne	.L24
	imull	$-1030792151, %edi, %edi
	addl	$85899344, %edi
	movl	%edi, %edx
	rorl	$2, %edx
	cmpl	$42949672, %edx
	jbe	.L25
	addl	$12, %esi
.L24:
	movslq	%esi, %rsi
	leaq	_ZN6icu_675Grego11DAYS_BEFOREE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movswl	(%rdx,%rsi,2), %edx
	addl	%eax, %edx
	addl	%ecx, %edx
	cvtsi2sdl	%edx, %xmm0
	subsd	.LC3(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	rorl	$4, %edi
	cmpl	$10737418, %edi
	ja	.L24
	addl	$12, %esi
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L22:
	testl	%edi, %edi
	leal	3(%rdi), %eax
	movl	%edi, %r8d
	cmovns	%edi, %eax
	sarl	$31, %r8d
	sarl	$2, %eax
	leal	1721422(%rdx,%rax), %r9d
	movslq	%edi, %rax
	imulq	$1374389535, %rax, %rax
	movq	%rax, %rdx
	sarq	$37, %rax
	sarq	$39, %rdx
	subl	%r8d, %eax
	subl	%r8d, %edx
	subl	$1, %eax
	leal	-1(%r9,%rdx), %edx
	jmp	.L23
	.cfi_endproc
.LFE2453:
	.size	_ZN6icu_675Grego11fieldsToDayEiii, .-_ZN6icu_675Grego11fieldsToDayEiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Grego11dayToFieldsEdRiS1_S1_S1_S1_
	.type	_ZN6icu_675Grego11dayToFieldsEdRiS1_S1_S1_S1_, @function
_ZN6icu_675Grego11dayToFieldsEdRiS1_S1_S1_S1_:
.LFB2454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$56, %rsp
	movsd	.LC4(%rip), %xmm3
	addsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm0
	movsd	%xmm3, -56(%rbp)
	divsd	.LC5(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-56(%rbp), %xmm3
	movapd	%xmm0, %xmm2
	movsd	.LC5(%rip), %xmm0
	movapd	%xmm3, %xmm1
	movsd	%xmm2, -88(%rbp)
	mulsd	%xmm2, %xmm0
	movsd	%xmm3, -80(%rbp)
	subsd	%xmm0, %xmm1
	cvttsd2sil	%xmm1, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movl	%eax, (%rbx)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -56(%rbp)
	divsd	.LC6(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	.LC6(%rip), %xmm4
	movsd	-56(%rbp), %xmm1
	cvttsd2sil	%xmm0, %edx
	mulsd	%xmm0, %xmm4
	movl	%edx, -68(%rbp)
	subsd	%xmm4, %xmm1
	cvttsd2sil	%xmm1, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movl	%eax, (%rbx)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -56(%rbp)
	divsd	.LC7(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-56(%rbp), %xmm1
	movapd	%xmm0, %xmm4
	movsd	.LC7(%rip), %xmm0
	movsd	%xmm4, -64(%rbp)
	mulsd	%xmm4, %xmm0
	subsd	%xmm0, %xmm1
	cvttsd2sil	%xmm1, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movl	%eax, (%rbx)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -56(%rbp)
	divsd	.LC8(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	.LC8(%rip), %xmm5
	movsd	-56(%rbp), %xmm1
	movsd	-88(%rbp), %xmm2
	movl	-68(%rbp), %edx
	cvttsd2sil	%xmm0, %ecx
	mulsd	%xmm0, %xmm5
	movsd	-64(%rbp), %xmm4
	movsd	-80(%rbp), %xmm3
	imull	$100, %edx, %esi
	subsd	%xmm5, %xmm1
	cvttsd2sil	%xmm1, %eax
	movl	%eax, (%rbx)
	cvttsd2sil	%xmm2, %eax
	imull	$400, %eax, %eax
	addl	%esi, %eax
	cvttsd2sil	%xmm4, %esi
	leal	(%rax,%rsi,4), %eax
	addl	%ecx, %eax
	cmpl	$4, %edx
	movl	%eax, (%r15)
	je	.L44
	cmpl	$4, %ecx
	je	.L44
	addl	$1, %eax
	movl	%eax, (%r15)
	testb	$3, %al
	jne	.L32
.L51:
	imull	$-1030792151, %eax, %eax
	addl	$85899344, %eax
	movl	%eax, %edx
	rorl	$2, %edx
	cmpl	$42949672, %edx
	jbe	.L49
.L33:
	addsd	.LC1(%rip), %xmm3
	movsd	.LC9(%rip), %xmm1
	movapd	%xmm3, %xmm0
	call	uprv_fmod_67@PLT
	cvttsd2sil	%xmm0, %eax
	leal	8(%rax), %ecx
	leal	1(%rax), %edx
	testl	%eax, %eax
	movl	%ecx, %eax
	cmovns	%edx, %eax
	movl	%eax, (%r14)
	movl	(%rbx), %eax
	cmpl	$59, %eax
	jle	.L50
	leal	3(%rax,%rax,2), %eax
	leal	6(,%rax,4), %eax
	imulq	$1497972245, %rax, %rax
	shrq	$39, %rax
	movl	%eax, (%r12)
	addl	$12, %eax
	movl	(%rbx), %edx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$365, (%rbx)
	movl	(%r15), %eax
	testb	$3, %al
	je	.L51
.L32:
	addsd	.LC1(%rip), %xmm3
	movsd	.LC9(%rip), %xmm1
	movapd	%xmm3, %xmm0
	call	uprv_fmod_67@PLT
	cvttsd2sil	%xmm0, %eax
	leal	8(%rax), %ecx
	leal	1(%rax), %edx
	testl	%eax, %eax
	movl	%ecx, %eax
	cmovns	%edx, %eax
	movl	%eax, (%r14)
	movl	(%rbx), %eax
	cmpl	$58, %eax
	jg	.L37
	leal	(%rax,%rax,2), %eax
	leal	6(,%rax,4), %edx
	movslq	%edx, %rax
	sarl	$31, %edx
	imulq	$1497972245, %rax, %rax
	sarq	$39, %rax
	subl	%edx, %eax
	movl	%eax, (%r12)
	movl	(%rbx), %edx
.L36:
	cltq
	leaq	_ZN6icu_675Grego11DAYS_BEFOREE(%rip), %rcx
	movswl	(%rcx,%rax,2), %eax
	subl	%eax, %edx
	addl	$1, %edx
	movl	%edx, 0(%r13)
	addl	$1, (%rbx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	leal	6(%rax,%rax,2), %eax
	leal	6(,%rax,4), %eax
	imulq	$1497972245, %rax, %rax
	shrq	$39, %rax
	movl	%eax, (%r12)
	movl	(%rbx), %edx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L49:
	rorl	$4, %eax
	cmpl	$10737418, %eax
	jbe	.L33
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L50:
	leal	(%rax,%rax,2), %eax
	leal	6(,%rax,4), %edx
	movslq	%edx, %rax
	sarl	$31, %edx
	imulq	$1497972245, %rax, %rax
	sarq	$39, %rax
	subl	%edx, %eax
	movl	%eax, (%r12)
	addl	$12, %eax
	movl	(%rbx), %edx
	jmp	.L36
	.cfi_endproc
.LFE2454:
	.size	_ZN6icu_675Grego11dayToFieldsEdRiS1_S1_S1_S1_, .-_ZN6icu_675Grego11dayToFieldsEdRiS1_S1_S1_S1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Grego12timeToFieldsEdRiS1_S1_S1_S1_S1_
	.type	_ZN6icu_675Grego12timeToFieldsEdRiS1_S1_S1_S1_S1_, @function
_ZN6icu_675Grego12timeToFieldsEdRiS1_S1_S1_S1_S1_:
.LFB2455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	%r9, -64(%rbp)
	movsd	%xmm0, -56(%rbp)
	divsd	.LC10(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-56(%rbp), %xmm1
	pxor	%xmm3, %xmm3
	movsd	.LC10(%rip), %xmm2
	movq	-64(%rbp), %r9
	mulsd	%xmm0, %xmm2
	movapd	%xmm1, %xmm4
	subsd	%xmm2, %xmm4
	comisd	%xmm4, %xmm3
	ja	.L56
	comisd	.LC10(%rip), %xmm4
	jb	.L61
	movsd	.LC1(%rip), %xmm3
	addsd	%xmm0, %xmm3
	ucomisd	%xmm3, %xmm0
	jp	.L59
.L62:
	jne	.L59
	xorl	%eax, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L61:
	cvttsd2sil	%xmm4, %eax
	movapd	%xmm0, %xmm3
.L54:
	movl	%eax, (%r9)
	addq	$24, %rsp
	movq	%rbx, %r8
	movq	%r15, %rcx
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movapd	%xmm3, %xmm0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_675Grego11dayToFieldsEdRiS1_S1_S1_S1_
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movsd	.LC0(%rip), %xmm3
	addsd	%xmm0, %xmm3
	ucomisd	%xmm3, %xmm0
	jnp	.L62
.L59:
	movsd	.LC10(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	subsd	%xmm2, %xmm1
	cvttsd2sil	%xmm1, %eax
	jmp	.L54
	.cfi_endproc
.LFE2455:
	.size	_ZN6icu_675Grego12timeToFieldsEdRiS1_S1_S1_S1_S1_, .-_ZN6icu_675Grego12timeToFieldsEdRiS1_S1_S1_S1_S1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Grego9dayOfWeekEd
	.type	_ZN6icu_675Grego9dayOfWeekEd, @function
_ZN6icu_675Grego9dayOfWeekEd:
.LFB2456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movsd	.LC11(%rip), %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -8(%rbp)
	divsd	.LC9(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-8(%rbp), %xmm1
	movl	$7, %edx
	mulsd	.LC9(%rip), %xmm0
	leave
	.cfi_def_cfa 7, 8
	subsd	%xmm0, %xmm1
	cvttsd2sil	%xmm1, %eax
	testl	%eax, %eax
	cmove	%edx, %eax
	ret
	.cfi_endproc
.LFE2456:
	.size	_ZN6icu_675Grego9dayOfWeekEd, .-_ZN6icu_675Grego9dayOfWeekEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Grego16dayOfWeekInMonthEiii
	.type	_ZN6icu_675Grego16dayOfWeekInMonthEiii, @function
_ZN6icu_675Grego16dayOfWeekInMonthEiii:
.LFB2457:
	.cfi_startproc
	endbr64
	addl	$6, %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$2, %eax
	subl	%ecx, %eax
	cmpl	$4, %eax
	je	.L73
	cmpl	$5, %eax
	je	.L74
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	testb	$3, %dil
	jne	.L68
	imull	$-1030792151, %edi, %edi
	addl	$85899344, %edi
	movl	%edi, %ecx
	rorl	$2, %ecx
	cmpl	$42949672, %ecx
	ja	.L70
	rorl	$4, %edi
	cmpl	$10737418, %edi
	ja	.L68
.L70:
	addl	$12, %esi
.L68:
	movslq	%esi, %rsi
	leaq	_ZN6icu_675Grego12MONTH_LENGTHE(%rip), %rcx
	movsbl	(%rcx,%rsi), %ecx
	cmpl	%ecx, %edx
	movl	$-1, %edx
	cmovge	%edx, %eax
	ret
.L74:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2457:
	.size	_ZN6icu_675Grego16dayOfWeekInMonthEiii, .-_ZN6icu_675Grego16dayOfWeekInMonthEiii
	.globl	_ZN6icu_675Grego12MONTH_LENGTHE
	.section	.rodata
	.align 16
	.type	_ZN6icu_675Grego12MONTH_LENGTHE, @object
	.size	_ZN6icu_675Grego12MONTH_LENGTHE, 24
_ZN6icu_675Grego12MONTH_LENGTHE:
	.ascii	"\037\034\037\036\037\036\037\037\036\037\036\037\037\035\037"
	.ascii	"\036\037\036\037\037\036\037\036\037"
	.globl	_ZN6icu_675Grego11DAYS_BEFOREE
	.align 32
	.type	_ZN6icu_675Grego11DAYS_BEFOREE, @object
	.size	_ZN6icu_675Grego11DAYS_BEFOREE, 48
_ZN6icu_675Grego11DAYS_BEFOREE:
	.value	0
	.value	31
	.value	59
	.value	90
	.value	120
	.value	151
	.value	181
	.value	212
	.value	243
	.value	273
	.value	304
	.value	334
	.value	0
	.value	31
	.value	60
	.value	91
	.value	121
	.value	152
	.value	182
	.value	213
	.value	244
	.value	274
	.value	305
	.value	335
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-1074790400
	.align 8
.LC1:
	.long	0
	.long	1072693248
	.align 8
.LC3:
	.long	0
	.long	1094885062
	.align 8
.LC4:
	.long	0
	.long	1093005940
	.align 8
.LC5:
	.long	0
	.long	1090639240
	.align 8
.LC6:
	.long	0
	.long	1088542080
	.align 8
.LC7:
	.long	0
	.long	1083626496
	.align 8
.LC8:
	.long	0
	.long	1081528320
	.align 8
.LC9:
	.long	0
	.long	1075576832
	.align 8
.LC10:
	.long	0
	.long	1100257648
	.align 8
.LC11:
	.long	0
	.long	1075052544
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
