	.file	"usearch.cpp"
	.text
	.p2align 4
	.type	usearch_cleanup, @function
usearch_cleanup:
.LFB3224:
	.cfi_startproc
	endbr64
	movq	$0, _ZL9g_nfcImpl(%rip)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3224:
	.size	usearch_cleanup, .-usearch_cleanup
	.p2align 4
	.type	_ZL6getFCDPKDsPii, @function
_ZL6getFCDPKDsPii:
.LFB3226:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movslq	(%rsi), %rax
	movq	_ZL9g_nfcImpl(%rip), %r8
	leaq	(%rdi,%rax,2), %rdi
	xorl	%eax, %eax
	movzwl	(%rdi), %ecx
	leaq	2(%rdi), %rbx
	cmpw	8(%r8), %cx
	jb	.L4
	movq	56(%r8), %r9
	movzwl	%cx, %esi
	movzbl	%ch, %ecx
	movzbl	(%r9,%rcx), %ecx
	testb	%cl, %cl
	je	.L4
	movl	%esi, %r9d
	shrb	$5, %r9b
	btl	%r9d, %ecx
	jnc	.L4
	movslq	%edx, %rdx
	leaq	(%r12,%rdx,2), %rax
	cmpq	%rax, %rbx
	je	.L5
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L5
	movzwl	2(%rdi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L18
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%r8, %rdi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
.L4:
	subq	%r12, %rbx
	sarq	%rbx
	movl	%ebx, 0(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	sall	$10, %esi
	leaq	4(%rdi), %rbx
	leal	-56613888(%rax,%rsi), %esi
	jmp	.L5
	.cfi_endproc
.LFE3226:
	.size	_ZL6getFCDPKDsPii, .-_ZL6getFCDPKDsPii
	.p2align 4
	.type	_ZL10initializeP13UStringSearchP10UErrorCode, @function
_ZL10initializeP13UStringSearchP10UErrorCode:
.LFB3235:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L32
	movl	4196(%rdi), %eax
	movl	$0, -60(%rbp)
	movq	%rsi, %rbx
	movq	8(%rdi), %r12
	movl	16(%rdi), %r13d
	testl	%eax, %eax
	jne	.L21
	xorl	%eax, %eax
	movw	%ax, 3120(%rdi)
.L22:
	movq	1064(%r15), %rdi
	testq	%rdi, %rdi
	je	.L24
	leaq	1072(%r15), %rax
	cmpq	%rax, %rdi
	je	.L25
	call	uprv_free_67@PLT
.L25:
	movq	$0, 1064(%r15)
.L24:
	movq	4184(%r15), %r14
	movl	16(%r15), %r12d
	movq	8(%r15), %rsi
	testq	%r14, %r14
	je	.L97
	movq	%rbx, %rcx
	movl	%r12d, %edx
	movq	%r14, %rdi
	call	ucol_setText_67@PLT
.L27:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L32
	movq	24(%r15), %rdi
	leaq	32(%r15), %rax
	movq	%rax, -96(%rbp)
	testq	%rdi, %rdi
	je	.L29
	cmpq	%rdi, %rax
	je	.L29
	call	uprv_free_67@PLT
.L29:
	movq	-96(%rbp), %rax
	movl	$256, -80(%rbp)
	xorl	%r13d, %r13d
	movq	%rax, -72(%rbp)
	xorl	%eax, %eax
	movw	%ax, -74(%rbp)
	leal	1(%r12), %eax
	movl	%eax, -104(%rbp)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L33:
	cmpl	4204(%r15), %ecx
	jnb	.L36
	xorw	%cx, %cx
	cmpl	$2, 4196(%r15)
	jle	.L37
	.p2align 4,,10
	.p2align 3
.L36:
	testl	%ecx, %ecx
	jne	.L38
.L37:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	ucol_getMaxExpansion_67@PLT
	movzwl	-74(%rbp), %esi
	leal	-1(%rsi,%rax), %eax
	movw	%ax, -74(%rbp)
.L41:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	ucol_next_67@PLT
	movl	%eax, %r12d
	cmpl	$-1, %eax
	je	.L30
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L98
	movl	4200(%r15), %ecx
	andl	%r12d, %ecx
	cmpb	$0, 4208(%r15)
	jne	.L33
	cmpl	$2, 4196(%r15)
	jle	.L36
	testl	%ecx, %ecx
	movl	$65535, %eax
	cmove	%eax, %ecx
.L38:
	movq	%r14, %rdi
	movl	%ecx, -88(%rbp)
	call	ucol_getOffset_67@PLT
	leal	1(%r13), %r9d
	cmpl	-80(%rbp), %r9d
	movl	-88(%rbp), %ecx
	je	.L39
	movl	(%rbx), %eax
	movq	-72(%rbp), %rsi
	movl	%ecx, (%rsi,%r13,4)
	testl	%eax, %eax
	jg	.L32
	movl	%r9d, %r13d
	jmp	.L37
.L100:
	movl	$7, (%rbx)
	.p2align 4,,10
	.p2align 3
.L32:
	xorl	%r10d, %r10d
	movw	%r10w, 3122(%r15)
.L19:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	(%rbx), %eax
	movq	-72(%rbp), %rbx
	movl	%r13d, %ecx
	movl	$0, (%rbx,%rcx,4)
	movq	%rbx, 24(%r15)
	movl	%r13d, 20(%r15)
	testl	%eax, %eax
	jg	.L32
	testl	%r13d, %r13d
	jle	.L32
	movswl	-74(%rbp), %ebx
	cmpl	%ebx, %r13d
	jg	.L42
	movl	$1, %edi
	movl	$1, %r10d
.L43:
	leaq	3124(%r15), %rax
	movd	%r10d, %xmm0
	movq	24(%r15), %r11
	movw	%r10w, 3122(%r15)
	movq	%rax, -72(%rbp)
	punpcklwd	%xmm0, %xmm0
	leaq	3638(%r15), %r8
	leaq	3636(%r15), %rcx
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L45:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L45
	movl	%r13d, %edx
	leal	-1(%rdi), %ecx
	movq	%r11, %r9
	subl	%r13d, %edi
	movw	%r10w, 3636(%r15)
	movl	$1, %r12d
	subl	$1, %edx
	je	.L50
.L51:
	movl	(%r9), %r13d
	testl	%ecx, %ecx
	movl	%r12d, %esi
	cmovg	%ecx, %esi
	movl	%r13d, %eax
	shrl	$24, %eax
	leal	(%rax,%rax,8), %r14d
	leal	(%rax,%r14,4), %eax
	movl	%r13d, %r14d
	shrl	$16, %r14d
	addl	%r14d, %eax
	leal	(%rax,%rax,8), %r14d
	leal	(%rax,%r14,4), %eax
	movl	%r13d, %r14d
	shrl	$8, %r14d
	addl	%r14d, %eax
	leal	(%rax,%rax,8), %r14d
	leal	(%rax,%r14,4), %eax
	addl	%eax, %r13d
	movslq	%r13d, %rax
	movl	%r13d, %r14d
	imulq	$2139127681, %rax, %rax
	sarl	$31, %r14d
	sarq	$39, %rax
	subl	%r14d, %eax
	movl	%eax, %r14d
	sall	$8, %r14d
	addl	%r14d, %eax
	movq	-72(%rbp), %r14
	subl	%eax, %r13d
	movl	%r13d, %eax
	leal	257(%r13), %r13d
	cmovs	%r13d, %eax
	subl	$1, %ecx
	addq	$4, %r9
	cltq
	movw	%si, (%r14,%rax,2)
	cmpl	%edi, %ecx
	jne	.L51
	.p2align 4,,10
	.p2align 3
.L50:
	movslq	%edx, %rcx
	movl	$1, %r9d
	movl	(%r11,%rcx,4), %esi
	movl	%esi, %eax
	shrl	$24, %eax
	leal	(%rax,%rax,8), %edi
	leal	(%rax,%rdi,4), %eax
	movl	%esi, %edi
	shrl	$16, %edi
	addl	%edi, %eax
	leal	(%rax,%rax,8), %edi
	leal	(%rax,%rdi,4), %eax
	movl	%esi, %edi
	shrl	$8, %edi
	addl	%edi, %eax
	leal	(%rax,%rax,8), %edi
	leal	(%rax,%rdi,4), %eax
	addl	%eax, %esi
	movslq	%esi, %rax
	movl	%esi, %edi
	imulq	$2139127681, %rax, %rax
	sarl	$31, %edi
	sarq	$39, %rax
	subl	%edi, %eax
	movl	%eax, %edi
	sall	$8, %edi
	addl	%edi, %eax
	movl	$1, %edi
	subl	%eax, %esi
	movl	%esi, %eax
	leal	257(%rsi), %esi
	cmovs	%esi, %eax
	movq	-72(%rbp), %rsi
	cltq
	movw	%di, (%rsi,%rax,2)
	movq	%r8, %rax
	leaq	4150(%r15), %rsi
	movw	%r9w, 3124(%r15)
	.p2align 4,,10
	.p2align 3
.L52:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L52
	movl	%edx, %esi
	movw	%r10w, 4150(%r15)
	subl	%ecx, %esi
	subw	-74(%rbp), %si
	testl	%edx, %edx
	je	.L58
.L53:
	movl	(%r11,%rcx,4), %edx
	cmpl	%ecx, %ebx
	leal	(%rsi,%rcx), %edi
	movl	$1, %eax
	cmovge	%eax, %edi
	movl	%edx, %eax
	shrl	$24, %eax
	leal	(%rax,%rax,8), %r9d
	leal	(%rax,%r9,4), %eax
	movl	%edx, %r9d
	shrl	$16, %r9d
	addl	%r9d, %eax
	leal	(%rax,%rax,8), %r9d
	leal	(%rax,%r9,4), %eax
	movl	%edx, %r9d
	shrl	$8, %r9d
	addl	%r9d, %eax
	leal	(%rax,%rax,8), %r9d
	leal	(%rax,%r9,4), %eax
	addl	%eax, %edx
	movslq	%edx, %rax
	movl	%edx, %r9d
	imulq	$2139127681, %rax, %rax
	sarl	$31, %r9d
	sarq	$39, %rax
	subl	%r9d, %eax
	movl	%eax, %r9d
	sall	$8, %r9d
	addl	%r9d, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	leal	257(%rdx), %edx
	cmovs	%edx, %eax
	subq	$1, %rcx
	cltq
	movw	%di, (%r8,%rax,2)
	testl	%ecx, %ecx
	jne	.L53
	.p2align 4,,10
	.p2align 3
.L58:
	movl	(%r11), %ecx
	movl	%ecx, %eax
	shrl	$24, %eax
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	movl	%ecx, %edx
	shrl	$16, %edx
	addl	%edx, %eax
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	movl	%ecx, %edx
	shrl	$8, %edx
	addl	%edx, %eax
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %edx
	addl	%ecx, %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$2139127681, %rax, %rax
	sarl	$31, %ecx
	sarq	$39, %rax
	subl	%ecx, %eax
	movl	%eax, %ecx
	sall	$8, %ecx
	addl	%ecx, %eax
	movl	$1, %ecx
	subl	%eax, %edx
	movl	%edx, %eax
	leal	257(%rdx), %edx
	cmovs	%edx, %eax
	movl	$1, %edx
	cltq
	movw	%dx, (%r8,%rax,2)
	movw	%cx, 3638(%r15)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L39:
	movl	-80(%rbp), %edx
	addl	-104(%rbp), %edx
	movl	%r9d, -100(%rbp)
	subl	%eax, %edx
	movl	%ecx, -88(%rbp)
	leal	0(,%rdx,4), %edi
	movl	%edx, -80(%rbp)
	call	uprv_malloc_67@PLT
	movl	-88(%rbp), %ecx
	movl	-100(%rbp), %r9d
	testq	%rax, %rax
	je	.L100
	movl	(%rbx), %r11d
	movl	%r9d, -100(%rbp)
	movl	%ecx, -88(%rbp)
	testl	%r11d, %r11d
	jg	.L32
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	leaq	0(,%r13,4), %rdx
	call	memcpy@PLT
	movq	-72(%rbp), %rdi
	movl	-88(%rbp), %ecx
	movl	-100(%rbp), %r9d
	movq	%rax, %r8
	cmpq	%rax, %rdi
	movl	%ecx, (%rax,%r13,4)
	je	.L60
	cmpq	%rdi, -96(%rbp)
	je	.L60
	movl	%r9d, -72(%rbp)
	movq	%rax, -88(%rbp)
	call	uprv_free_67@PLT
	movl	-72(%rbp), %r9d
	movq	-88(%rbp), %r8
	movl	%r9d, %r13d
	movq	%r8, -72(%rbp)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	-60(%rbp), %r14
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZL6getFCDPKDsPii
	leal	-1(%r13), %edx
	movzbl	%ah, %eax
	movl	%edx, -60(%rbp)
	movb	%al, 3120(%r15)
	movslq	%edx, %rax
	leaq	(%rax,%rax), %rcx
	movzwl	(%r12,%rax,2), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L23
	testl	%edx, %edx
	jg	.L101
.L23:
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZL6getFCDPKDsPii
	movb	%al, 3121(%r15)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L97:
	movq	4152(%r15), %rdi
	movq	%rbx, %rcx
	movl	%r12d, %edx
	call	ucol_openElements_67@PLT
	movq	%rax, 4184(%r15)
	movq	%rax, %r14
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L98:
	movq	-72(%rbp), %rbx
	movl	%r13d, %eax
	movl	$0, (%rbx,%rax,4)
	movq	%rbx, 24(%r15)
	movl	%r13d, 20(%r15)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L42:
	movl	%r13d, %r10d
	subw	-74(%rbp), %r10w
	movswl	%r10w, %edi
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L101:
	movzwl	-2(%r12,%rcx), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L23
	leal	-2(%r13), %eax
	movl	%eax, -60(%rbp)
	jmp	.L23
.L99:
	call	__stack_chk_fail@PLT
.L60:
	movq	%r8, -72(%rbp)
	movl	%r9d, %r13d
	jmp	.L37
	.cfi_endproc
.LFE3235:
	.size	_ZL10initializeP13UStringSearchP10UErrorCode, .-_ZL10initializeP13UStringSearchP10UErrorCode
	.p2align 4
	.type	_ZL25initializePatternPCETableP13UStringSearchP10UErrorCode, @function
_ZL25initializePatternPCETableP13UStringSearchP10UErrorCode:
.LFB3232:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -400(%rbp)
	movq	4184(%rdi), %rcx
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movl	16(%rdi), %esi
	movq	%rcx, -368(%rbp)
	movl	%esi, -360(%rbp)
	movq	8(%rdi), %rsi
	testq	%rcx, %rcx
	je	.L130
	movl	-360(%rbp), %edx
	movq	%rcx, %rdi
	movq	%r12, %rcx
	call	ucol_setText_67@PLT
.L104:
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L105
	movq	-400(%rbp), %rax
	movq	1064(%rax), %rdi
	leaq	1072(%rax), %rsi
	movq	%rsi, -392(%rbp)
	testq	%rdi, %rdi
	je	.L106
	cmpq	%rsi, %rdi
	je	.L106
	call	uprv_free_67@PLT
.L106:
	leaq	-352(%rbp), %r15
	movq	-368(%rbp), %rsi
	xorl	%ebx, %ebx
	movq	%r15, %rdi
	call	_ZN6icu_6713UCollationPCEC1EP18UCollationElements@PLT
	movq	%r15, -376(%rbp)
	movq	-392(%rbp), %r13
	movl	$256, -356(%rbp)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L131:
	movl	(%r12), %eax
	movq	%r15, 0(%r13,%rbx,8)
	testl	%eax, %eax
	jg	.L127
.L111:
	movl	%r14d, %ebx
.L107:
	movq	-376(%rbp), %rdi
	movq	%r12, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN6icu_6713UCollationPCE13nextProcessedEPiS1_P10UErrorCode@PLT
	movq	%rax, %r15
	movabsq	$9223372036854775807, %rax
	cmpq	%rax, %r15
	je	.L108
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L108
	movq	-368(%rbp), %rdi
	leal	1(%rbx), %r14d
	call	ucol_getOffset_67@PLT
	cmpl	-356(%rbp), %r14d
	jne	.L131
	movl	-356(%rbp), %edx
	movl	-360(%rbp), %esi
	leal	1(%rsi,%rdx), %edx
	subl	%eax, %edx
	leal	0(,%rdx,8), %edi
	movl	%edx, -356(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L132
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L127
	movq	%r9, %rdi
	leaq	0(,%rbx,8), %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	%r15, (%rax,%rbx,8)
	movq	%rax, %r9
	cmpq	%rax, %r13
	je	.L129
	cmpq	-392(%rbp), %r13
	je	.L129
	movq	%r13, %rdi
	movq	%rax, -384(%rbp)
	call	uprv_free_67@PLT
	movq	-384(%rbp), %r9
.L129:
	movq	%r9, %r13
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L108:
	movl	%ebx, %eax
	movq	-376(%rbp), %r15
	movq	$0, 0(%r13,%rax,8)
	movq	-400(%rbp), %rax
	movq	%r13, 1064(%rax)
	movl	%ebx, 1056(%rax)
.L115:
	movq	%r15, %rdi
	call	_ZN6icu_6713UCollationPCED1Ev@PLT
.L105:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L133
	addq	$360, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	-376(%rbp), %r15
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%rdi, %rbx
	movl	-360(%rbp), %edx
	movq	4152(%rdi), %rdi
	movq	%r12, %rcx
	call	ucol_openElements_67@PLT
	movq	%rax, -368(%rbp)
	movq	%rax, 4184(%rbx)
	jmp	.L104
.L133:
	call	__stack_chk_fail@PLT
.L132:
	movl	$7, (%r12)
	movq	-376(%rbp), %r15
	jmp	.L115
	.cfi_endproc
.LFE3232:
	.size	_ZL25initializePatternPCETableP13UStringSearchP10UErrorCode, .-_ZL25initializePatternPCETableP13UStringSearchP10UErrorCode
	.p2align 4
	.type	_ZL12compareCE64slls.part.0, @function
_ZL12compareCE64slls.part.0:
.LFB4449:
	.cfi_startproc
	movq	%rdi, %rax
	movq	%rsi, %r8
	sarq	$32, %rax
	sarq	$32, %r8
	movl	%eax, %ecx
	movl	%r8d, %r9d
	xorw	%cx, %cx
	xorw	%r9w, %r9w
	cmpl	%r9d, %ecx
	je	.L135
	movl	$1, %eax
	testl	%ecx, %ecx
	je	.L134
	testl	%r9d, %r9d
	jne	.L146
	movl	$2, %eax
	cmpw	$4, %dx
	jne	.L146
.L134:
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	movzwl	%ax, %ecx
	movzwl	%r8w, %r8d
	cmpl	%r8d, %ecx
	je	.L138
	movl	$1, %eax
	testl	%ecx, %ecx
	je	.L134
	cmpw	$4, %dx
	sete	%dl
	testl	%r8d, %r8d
	jne	.L147
	movl	$2, %eax
	testb	%dl, %dl
	jne	.L134
.L147:
	movl	$-1, %eax
	cmpl	$5, %r8d
	je	.L134
	xorl	%eax, %eax
	cmpl	$5, %ecx
	sete	%al
	andl	%edx, %eax
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	xorw	%di, %di
	xorw	%si, %si
	cmpl	%esi, %edi
	je	.L145
	cmpl	$327680, %esi
	je	.L145
	cmpw	$4, %dx
	sete	%dl
	xorl	%eax, %eax
	cmpl	$327680, %edi
	sete	%al
	andl	%edx, %eax
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE4449:
	.size	_ZL12compareCE64slls.part.0, .-_ZL12compareCE64slls.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_19CEIBufferC2EP13UStringSearchP10UErrorCode, @function
_ZN6icu_6712_GLOBAL__N_19CEIBufferC2EP13UStringSearchP10UErrorCode:
.LFB3266:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	1056(%rsi), %eax
	movq	%rdi, %rbx
	movq	%rsi, 1568(%rdi)
	leal	32(%rax), %esi
	movq	(%r12), %rax
	movq	%rdi, 1536(%rdi)
	movl	%esi, 1544(%rdi)
	cmpw	$0, 14(%rax)
	je	.L158
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L158
	movslq	16(%r12), %rdx
	leaq	(%rax,%rdx,2), %rdx
	cmpq	%rdx, %rax
	jb	.L161
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L175:
	subw	$12645, %cx
	cmpw	$33, %cx
	jbe	.L159
	addl	$3, %esi
	cmpq	%rdx, %rax
	jnb	.L174
.L161:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	leal	-12593(%rcx), %edi
	cmpw	$29, %di
	jbe	.L159
	leal	-4352(%rcx), %edi
	cmpw	$94, %di
	ja	.L175
.L159:
	addl	$8, %esi
	cmpq	%rdx, %rax
	jb	.L161
.L174:
	movl	%esi, 1544(%rbx)
.L158:
	movq	4168(%r12), %rsi
	movl	0(%r13), %eax
	movq	$0, 1548(%rbx)
	movq	%rsi, 1560(%rbx)
	testl	%eax, %eax
	jg	.L157
	movq	4176(%r12), %rdi
	testq	%rdi, %rdi
	je	.L176
	call	_ZN6icu_6713UCollationPCE4initEP18UCollationElements@PLT
.L166:
	movslq	1544(%rbx), %rdi
	cmpl	$96, %edi
	jle	.L157
	salq	$4, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 1536(%rbx)
	testq	%rax, %rax
	je	.L173
.L157:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movl	$296, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L165
	movq	4168(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UCollationPCEC1EP18UCollationElements@PLT
	movq	%r14, 4176(%r12)
	jmp	.L166
.L165:
	movq	$0, 4176(%r12)
.L173:
	movl	$7, 0(%r13)
	jmp	.L157
	.cfi_endproc
.LFE3266:
	.size	_ZN6icu_6712_GLOBAL__N_19CEIBufferC2EP13UStringSearchP10UErrorCode, .-_ZN6icu_6712_GLOBAL__N_19CEIBufferC2EP13UStringSearchP10UErrorCode
	.set	_ZN6icu_6712_GLOBAL__N_19CEIBufferC1EP13UStringSearchP10UErrorCode,_ZN6icu_6712_GLOBAL__N_19CEIBufferC2EP13UStringSearchP10UErrorCode
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.type	usearch_searchBackwards_67.part.0, @function
usearch_searchBackwards_67.part.0:
.LFB4467:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-1632(%rbp), %rdi
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$1880, %rsp
	movl	%esi, -1884(%rbp)
	movq	%r15, %rsi
	movq	%rdx, -1896(%rbp)
	movq	%r8, %rdx
	movq	%rcx, -1904(%rbp)
	movq	%r8, -1920(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1912(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_19CEIBufferC1EP13UStringSearchP10UErrorCode
	movq	(%r15), %rax
	cmpl	%ebx, 8(%rax)
	jle	.L178
	movq	16(%rax), %rdi
	movl	%ebx, %esi
	xorl	%r12d, %r12d
	leaq	-1832(%rbp), %r13
	call	ubrk_following_67@PLT
	movq	4168(%r15), %rdi
	movq	%r14, %rdx
	movl	%eax, %esi
	call	ucol_setOffset_67@PLT
	movq	%r15, -1848(%rbp)
	movl	-88(%rbp), %ecx
	movl	-84(%rbp), %esi
	movl	-80(%rbp), %edi
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L179:
	cmpl	%edi, %r12d
	jne	.L181
	leal	1(%r12), %eax
	movl	%eax, -80(%rbp)
	subl	%esi, %eax
	cmpl	%ecx, %eax
	jl	.L182
	addl	$1, %esi
	movl	%esi, -84(%rbp)
.L182:
	movq	-96(%rbp), %r15
	movq	-64(%rbp), %rax
	movslq	%edx, %r14
	movq	%r13, %rcx
	movl	$0, -1832(%rbp)
	salq	$4, %r14
	addq	%r14, %r15
	movq	4176(%rax), %rdi
	leaq	8(%r15), %rsi
	leaq	12(%r15), %rdx
	call	_ZN6icu_6713UCollationPCE17previousProcessedEPiS1_P10UErrorCode@PLT
	movl	-88(%rbp), %ecx
	movl	-84(%rbp), %esi
	movq	%rax, (%r15)
	movq	-96(%rbp), %rax
	movl	-80(%rbp), %edi
	addq	%rax, %r14
	cmpl	8(%r14), %ebx
	jg	.L322
.L326:
	addl	$1, %r12d
.L184:
	movl	%r12d, %eax
	cltd
	idivl	%ecx
	cmpl	%esi, %r12d
	jl	.L179
	cmpl	%edi, %r12d
	jge	.L179
	movq	-96(%rbp), %rax
	movslq	%edx, %r14
	salq	$4, %r14
	addq	%rax, %r14
	cmpl	8(%r14), %ebx
	jle	.L326
.L322:
	movq	-1848(%rbp), %r15
	movq	%rax, %rbx
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L178:
	movq	4168(%r15), %rdi
	movl	-1884(%rbp), %esi
	xorl	%r12d, %r12d
	movq	-1920(%rbp), %rdx
	call	ucol_setOffset_67@PLT
	movl	-88(%rbp), %ecx
	movl	-84(%rbp), %esi
	movl	-80(%rbp), %edi
	movq	-96(%rbp), %rbx
.L183:
	leaq	-1832(%rbp), %r13
	movq	%rbx, %r10
	.p2align 4,,10
	.p2align 3
.L241:
	movl	%r12d, %eax
	cltd
	idivl	%ecx
	cmpl	%esi, %r12d
	jl	.L185
	cmpl	%edi, %r12d
	jl	.L327
.L185:
	cmpl	%edi, %r12d
	jne	.L188
	leal	1(%r12), %eax
	movl	%eax, -80(%rbp)
	subl	%esi, %eax
	cmpl	%ecx, %eax
	jl	.L189
	addl	$1, %esi
	movl	%esi, -84(%rbp)
.L189:
	movq	-64(%rbp), %rax
	movslq	%edx, %rbx
	movq	%r13, %rcx
	movl	$0, -1832(%rbp)
	salq	$4, %rbx
	leaq	(%r10,%rbx), %r14
	movq	4176(%rax), %rdi
	leaq	12(%r14), %rdx
	leaq	8(%r14), %rsi
	call	_ZN6icu_6713UCollationPCE17previousProcessedEPiS1_P10UErrorCode@PLT
	movq	-96(%rbp), %r10
	movq	%rax, (%r14)
	leaq	(%r10,%rbx), %rax
	movq	%rax, -1880(%rbp)
.L186:
	cmpq	$0, -1880(%rbp)
	je	.L188
	movl	1056(%r15), %eax
	xorl	%r14d, %r14d
	movl	%eax, %ebx
	subl	$1, %ebx
	js	.L192
	movslq	%ebx, %rdi
	movq	%rdi, -1848(%rbp)
	movq	%r15, %rdi
	movl	%ebx, %r15d
	movq	%rdi, %rbx
.L201:
	leal	-1(%r12,%rax), %ecx
	movq	1064(%rbx), %rdx
	movq	-1848(%rbp), %rsi
	subl	%r15d, %ecx
	movl	-80(%rbp), %edi
	addl	%r14d, %ecx
	movq	(%rdx,%rsi,8), %r8
	movl	-88(%rbp), %esi
	movl	%ecx, %eax
	cltd
	idivl	%esi
	movl	-84(%rbp), %eax
	cmpl	%eax, %ecx
	jl	.L193
	cmpl	%edi, %ecx
	jl	.L328
.L193:
	cmpl	%edi, %ecx
	jne	.L195
	addl	$1, %ecx
	movl	%ecx, -80(%rbp)
	subl	%eax, %ecx
	cmpl	%ecx, %esi
	jg	.L196
	addl	$1, %eax
	movl	%eax, -84(%rbp)
.L196:
	movq	-64(%rbp), %rax
	movslq	%edx, %r11
	movq	%r13, %rcx
	movq	%r8, -1872(%rbp)
	movl	$0, -1832(%rbp)
	salq	$4, %r11
	addq	%r11, %r10
	movq	4176(%rax), %rdi
	movq	%r11, -1864(%rbp)
	leaq	12(%r10), %rdx
	leaq	8(%r10), %rsi
	movq	%r10, -1856(%rbp)
	call	_ZN6icu_6713UCollationPCE17previousProcessedEPiS1_P10UErrorCode@PLT
	movq	-1856(%rbp), %r10
	movq	-1864(%rbp), %r11
	movq	-1872(%rbp), %r8
	movq	%rax, (%r10)
	movq	-96(%rbp), %r10
	addq	%r10, %r11
.L194:
	movq	(%rbx), %rax
	movq	(%r11), %r11
	movswl	14(%rax), %edx
	movl	%edx, %eax
	cmpq	%r11, %r8
	je	.L198
	testw	%ax, %ax
	je	.L197
	movq	%r8, %rsi
	movq	%r11, %rdi
	call	_ZL12compareCE64slls.part.0
	testl	%eax, %eax
	je	.L197
	jle	.L198
	cmpl	$1, %eax
	je	.L329
	subl	$1, %r14d
.L198:
	subl	$1, %r15d
	cmpl	$-1, %r15d
	je	.L320
	movslq	%r15d, %rax
	movq	%rax, -1848(%rbp)
.L200:
	movl	1056(%rbx), %eax
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L320:
	movl	1056(%rbx), %eax
	movq	%rbx, %r15
.L192:
	leal	-1(%r12,%rax), %eax
	movl	-88(%rbp), %ecx
	movl	-80(%rbp), %esi
	addl	%eax, %r14d
	movl	%r14d, %eax
	cltd
	idivl	%ecx
	movl	-84(%rbp), %eax
	cmpl	%eax, %r14d
	jl	.L203
	cmpl	%esi, %r14d
	jl	.L330
.L203:
	cmpl	%esi, %r14d
	jne	.L205
	addl	$1, %r14d
	movl	%r14d, -80(%rbp)
	subl	%eax, %r14d
	cmpl	%r14d, %ecx
	jg	.L206
	addl	$1, %eax
	movl	%eax, -84(%rbp)
.L206:
	movq	-64(%rbp), %rax
	movslq	%edx, %rbx
	movq	%r13, %rcx
	movl	$0, -1832(%rbp)
	salq	$4, %rbx
	leaq	(%r10,%rbx), %r14
	movq	4176(%rax), %rdi
	leaq	12(%r14), %rdx
	leaq	8(%r14), %rsi
	call	_ZN6icu_6713UCollationPCE17previousProcessedEPiS1_P10UErrorCode@PLT
	addq	-96(%rbp), %rbx
	movq	%rax, (%r14)
.L204:
	movl	8(%rbx), %eax
	movl	%eax, -1848(%rbp)
	movq	(%r15), %rax
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L331
.L207:
	movl	-1848(%rbp), %esi
	call	ubrk_isBoundary_67@PLT
	testb	%al, %al
	setne	%r11b
.L208:
	movl	-1848(%rbp), %eax
	cmpl	12(%rbx), %eax
	movl	$0, %eax
	cmove	%eax, %r11d
	movq	-1880(%rbp), %rax
	movl	8(%rax), %r14d
	testl	%r12d, %r12d
	jne	.L332
	movq	(%r15), %rax
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L333
.L229:
	movl	%r14d, %esi
	movb	%r11b, -1856(%rbp)
	call	ubrk_following_67@PLT
	movzbl	-1856(%rbp), %r11d
	movl	%eax, %r14d
.L230:
	testl	%r14d, %r14d
	jle	.L263
	cmpl	%r14d, -1884(%rbp)
	jle	.L263
.L225:
	cmpl	$15, 4196(%r15)
	je	.L245
.L232:
	movq	-96(%rbp), %r10
	testb	%r11b, %r11b
	jne	.L323
.L202:
	movl	-88(%rbp), %ecx
	movl	-84(%rbp), %esi
	addl	$1, %r12d
	movl	-80(%rbp), %edi
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L328:
	movslq	%edx, %r11
	salq	$4, %r11
	addq	%r10, %r11
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L197:
	movabsq	$9223372036854775807, %rax
	movq	%rbx, %r15
	cmpq	%rax, %r11
	jne	.L202
	movl	$-1, -1848(%rbp)
	movq	%r10, %rbx
	xorl	%r11d, %r11d
	movl	$-1, %r14d
.L191:
	movq	-1896(%rbp), %rax
	testq	%rax, %rax
	je	.L242
	movl	-1848(%rbp), %esi
	movl	%esi, (%rax)
.L242:
	movq	-1904(%rbp), %rax
	testq	%rax, %rax
	je	.L243
	movl	%r14d, (%rax)
.L243:
	cmpq	-1912(%rbp), %rbx
	je	.L177
	movq	%rbx, %rdi
	movb	%r11b, -1848(%rbp)
	call	uprv_free_67@PLT
	movzbl	-1848(%rbp), %r11d
.L177:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L334
	addq	$1880, %rsp
	movl	%r11d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	addl	$1, %r14d
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L331:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L207
	xorl	%r11d, %r11d
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L332:
	leal	-1(%r12), %ecx
	movl	-88(%rbp), %esi
	movl	-80(%rbp), %edi
	movl	%ecx, %eax
	cltd
	idivl	%esi
	movl	-84(%rbp), %eax
	cmpl	%ecx, %eax
	jg	.L211
	cmpl	%ecx, %edi
	jg	.L335
.L211:
	cmpl	%ecx, %edi
	jne	.L213
	movl	%r12d, %ecx
	movl	%r12d, -80(%rbp)
	subl	%eax, %ecx
	cmpl	%ecx, %esi
	jg	.L214
	addl	$1, %eax
	movl	%eax, -84(%rbp)
.L214:
	movq	-96(%rbp), %r8
	movslq	%edx, %rax
	movq	%r13, %rcx
	movb	%r11b, -1864(%rbp)
	movl	$0, -1832(%rbp)
	salq	$4, %rax
	addq	%rax, %r8
	movq	%rax, %rbx
	movq	-64(%rbp), %rax
	leaq	12(%r8), %rdx
	leaq	8(%r8), %rsi
	movq	%r8, -1856(%rbp)
	movq	4176(%rax), %rdi
	call	_ZN6icu_6713UCollationPCE17previousProcessedEPiS1_P10UErrorCode@PLT
	movq	-1856(%rbp), %r8
	movzbl	-1864(%rbp), %r11d
	movq	%rax, (%r8)
	movq	-96(%rbp), %rax
	addq	%rbx, %rax
.L212:
	movl	8(%rax), %ebx
	movl	12(%rax), %esi
	cmpl	%esi, %ebx
	jne	.L215
	movabsq	$9223372036854775807, %rdi
	cmpq	%rdi, (%rax)
	movl	$0, %edi
	cmovne	%edi, %r11d
.L215:
	movq	(%r15), %rdx
	movq	(%rdx), %rcx
	movq	24(%rdx), %rdi
	testq	%rcx, %rcx
	je	.L216
	movl	8(%rdx), %r8d
	cmpl	%r8d, %ebx
	jge	.L216
	testq	%rdi, %rdi
	je	.L336
.L217:
	cmpl	%ebx, %r14d
	jge	.L251
	xorl	%ecx, %ecx
.L250:
	movq	(%r15), %rdx
	movq	24(%rdx), %rdi
	.p2align 4,,10
	.p2align 3
.L249:
	testq	%rdi, %rdi
	je	.L337
.L222:
	movl	%r14d, %esi
	movb	%r11b, -1864(%rbp)
	movb	%cl, -1856(%rbp)
	call	ubrk_following_67@PLT
	movzbl	-1856(%rbp), %ecx
	movzbl	-1864(%rbp), %r11d
	movl	%eax, %r14d
	movq	-1880(%rbp), %rax
	cmpl	12(%rax), %r14d
	jl	.L221
	testb	%cl, %cl
	je	.L319
	cmpl	%r14d, %ebx
	jg	.L248
.L221:
	testb	%cl, %cl
	je	.L251
.L259:
	cmpl	$15, 4196(%r15)
	movl	%ebx, %r14d
	jne	.L232
.L245:
	movq	4160(%r15), %r9
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%esi, %esi
	movl	$2, %ecx
	movq	%rax, -1824(%rbp)
	movl	$2, %edx
	leaq	-1696(%rbp), %rbx
	movq	%rax, -1760(%rbp)
	movq	(%r9), %rax
	movq	%rbx, %rdi
	movw	%cx, -1752(%rbp)
	movq	(%r15), %rcx
	movq	24(%rax), %rax
	movw	%dx, -1816(%rbp)
	movq	(%rcx), %rcx
	movslq	-1848(%rbp), %rdx
	movb	%r11b, -1885(%rbp)
	movq	%rax, -1872(%rbp)
	movq	%rdx, %rax
	leaq	(%rcx,%rdx,2), %rdx
	movl	%r14d, %ecx
	movq	%r9, -1880(%rbp)
	subl	%eax, %ecx
	movq	%rdx, -1832(%rbp)
	movq	%r13, %rdx
	movl	$0, -1836(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-1880(%rbp), %r9
	leaq	-1836(%rbp), %r8
	leaq	-1824(%rbp), %rdx
	movq	%rdx, -1856(%rbp)
	movq	%r8, %rcx
	movq	%rbx, %rsi
	movq	-1872(%rbp), %rax
	movq	%r9, %rdi
	movq	%r8, -1864(%rbp)
	call	*%rax
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	4160(%r15), %r10
	movq	8(%r15), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	16(%r15), %ecx
	movq	(%r10), %rax
	movq	%r10, -1880(%rbp)
	movq	24(%rax), %rax
	movq	%rdx, -1832(%rbp)
	movq	%r13, %rdx
	movq	%rax, -1872(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-1864(%rbp), %r8
	movq	-1880(%rbp), %r10
	leaq	-1760(%rbp), %r9
	movq	%r9, -1864(%rbp)
	movq	%r9, %rdx
	movq	%rbx, %rsi
	movq	-1872(%rbp), %rax
	movq	%r10, %rdi
	movq	%r8, %rcx
	call	*%rax
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-1836(%rbp), %esi
	movq	-1864(%rbp), %r9
	movzbl	-1885(%rbp), %r11d
	testl	%esi, %esi
	jle	.L338
.L233:
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-1856(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-96(%rbp), %r10
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L263:
	movl	-1884(%rbp), %r14d
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L333:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L230
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L216:
	xorl	%ecx, %ecx
	cmpl	%ebx, %r14d
	jl	.L249
.L226:
	testq	%rdi, %rdi
	je	.L339
.L227:
	movl	%ebx, %esi
	movb	%r11b, -1856(%rbp)
	call	ubrk_isBoundary_67@PLT
	movzbl	-1856(%rbp), %r11d
	testb	%al, %al
	jne	.L259
	cmpl	$15, 4196(%r15)
	je	.L258
.L340:
	movq	-96(%rbp), %r10
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L327:
	movslq	%edx, %rdx
	salq	$4, %rdx
	leaq	(%r10,%rdx), %rax
	movq	%rax, -1880(%rbp)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L330:
	movslq	%edx, %rdx
	salq	$4, %rdx
	leaq	(%r10,%rdx), %rbx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L339:
	movq	16(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L227
	cmpl	$15, 4196(%r15)
	jne	.L340
.L258:
	movl	%ebx, %r14d
	xorl	%r11d, %r11d
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L337:
	movq	16(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L222
	movq	-1880(%rbp), %rax
	cmpl	12(%rax), %r14d
	jl	.L221
.L248:
	testb	%cl, %cl
	jne	.L225
.L319:
	movq	(%r15), %rdx
	cmpl	%r14d, %ebx
	movl	%r14d, %ebx
	movq	24(%rdx), %rdi
	jge	.L226
	xorl	%r11d, %r11d
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L338:
	movswl	-1752(%rbp), %ecx
	movzwl	-1816(%rbp), %eax
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L234
	testw	%ax, %ax
	js	.L235
	movswl	%ax, %edx
	sarl	$5, %edx
.L236:
	testw	%cx, %cx
	js	.L237
	sarl	$5, %ecx
.L238:
	cmpl	%edx, %ecx
	jne	.L233
	testb	%sil, %sil
	jne	.L233
	movq	-1856(%rbp), %rdi
	movq	%r9, %rsi
	movb	%r11b, -1872(%rbp)
	movq	%r9, -1864(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movzbl	-1872(%rbp), %r11d
	movq	-1864(%rbp), %r9
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L234:
	testb	%sil, %sil
	je	.L233
	movq	%r9, %rdi
	movb	%r11b, -1864(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-1856(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-1864(%rbp), %r11d
	movq	-96(%rbp), %r10
	testb	%r11b, %r11b
	je	.L202
.L323:
	movq	%r10, %rbx
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L335:
	movq	-96(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L188:
	movq	-1920(%rbp), %rax
	movq	%r10, %rbx
	xorl	%r11d, %r11d
	movl	$-1, %r14d
	movl	$-1, -1848(%rbp)
	movl	$5, (%rax)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L336:
	movq	(%rax), %rax
	sarq	$32, %rax
	testl	$4294901760, %eax
	je	.L217
	movq	-1880(%rbp), %rax
	cmpl	12(%rax), %ebx
	jl	.L217
	cmpl	%esi, %ebx
	jge	.L217
	movq	4160(%r15), %rdi
	movq	(%rdi), %rax
	movq	120(%rax), %r9
	movslq	%ebx, %rax
	movzwl	(%rcx,%rax,2), %esi
	leaq	(%rax,%rax), %rdx
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L218
	leal	1(%rbx), %eax
	cmpl	%r8d, %eax
	jne	.L341
.L218:
	movb	%r11b, -1864(%rbp)
	movq	%rdx, -1856(%rbp)
	call	*%r9
	movzbl	-1864(%rbp), %r11d
	testb	%al, %al
	jne	.L219
	movq	4160(%r15), %rdi
	movq	(%rdi), %rax
	movq	128(%rax), %r8
	movq	(%r15), %rax
	testl	%ebx, %ebx
	jle	.L255
	movq	(%rax), %rax
	movq	-1856(%rbp), %rdx
	movzwl	-2(%rax,%rdx), %esi
	movl	%esi, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L220
	cmpl	$1, %ebx
	jne	.L342
.L220:
	movb	%r11b, -1856(%rbp)
	call	*%r8
	movzbl	-1856(%rbp), %r11d
	testb	%al, %al
	setne	%cl
	cmpl	%ebx, %r14d
	jge	.L221
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L237:
	movl	-1748(%rbp), %ecx
	jmp	.L238
.L235:
	movl	-1812(%rbp), %edx
	jmp	.L236
.L219:
	cmpl	%ebx, %r14d
	jge	.L259
	movl	$1, %ecx
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L341:
	movzwl	2(%rcx,%rdx), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L218
	sall	$10, %esi
	leal	-56613888(%rax,%rsi), %esi
	jmp	.L218
.L342:
	movzwl	-4(%rax,%rdx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L220
	sall	$10, %eax
	leal	-56613888(%rsi,%rax), %esi
	jmp	.L220
.L251:
	movq	(%r15), %rdx
	movq	24(%rdx), %rdi
	jmp	.L226
.L334:
	call	__stack_chk_fail@PLT
.L255:
	orl	$-1, %esi
	jmp	.L220
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	usearch_searchBackwards_67.part.0.cold, @function
usearch_searchBackwards_67.part.0.cold:
.LFSB4467:
.L181:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	8, %eax
	ud2
.L205:
	movl	8, %eax
	ud2
.L213:
	movl	8, %eax
	ud2
.L195:
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE4467:
	.text
	.size	usearch_searchBackwards_67.part.0, .-usearch_searchBackwards_67.part.0
	.section	.text.unlikely
	.size	usearch_searchBackwards_67.part.0.cold, .-usearch_searchBackwards_67.part.0.cold
.LCOLDE0:
	.text
.LHOTE0:
	.section	.text.unlikely
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4
	.type	usearch_search_67.part.0, @function
usearch_search_67.part.0:
.LFB4466:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1880, %rsp
	movq	%rdx, -1904(%rbp)
	movq	%rcx, -1912(%rbp)
	movq	%r8, -1896(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1632(%rbp), %rax
	cmpq	$0, 1064(%rdi)
	movq	%rax, -1888(%rbp)
	je	.L519
.L344:
	movq	-1896(%rbp), %r14
	movq	4168(%rbx), %rdi
	movl	%r13d, %esi
	movq	%r14, %rdx
	call	ucol_setOffset_67@PLT
	movq	-1888(%rbp), %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	leaq	-1832(%rbp), %r14
	call	_ZN6icu_6712_GLOBAL__N_19CEIBufferC1EP13UStringSearchP10UErrorCode
	movq	-96(%rbp), %r10
	movl	$0, -1844(%rbp)
	.p2align 4,,10
	.p2align 3
.L423:
	movl	-1844(%rbp), %eax
	movl	-88(%rbp), %ecx
	movl	-1844(%rbp), %edi
	movl	-80(%rbp), %esi
	cltd
	idivl	%ecx
	movl	-84(%rbp), %eax
	cmpl	%eax, %edi
	jl	.L358
	cmpl	%esi, %edi
	jl	.L520
.L358:
	movl	-1844(%rbp), %edi
	cmpl	%esi, %edi
	jne	.L361
	leal	1(%rdi), %esi
	movl	%esi, -80(%rbp)
	subl	%eax, %esi
	cmpl	%esi, %ecx
	jg	.L362
	addl	$1, %eax
	movl	%eax, -84(%rbp)
.L362:
	movq	-64(%rbp), %rax
	movslq	%edx, %rdx
	movq	%r14, %rcx
	movl	$0, -1832(%rbp)
	salq	$4, %rdx
	leaq	(%r10,%rdx), %r15
	movq	4176(%rax), %rdi
	movq	%rdx, %r13
	leaq	8(%r15), %rsi
	leaq	12(%r15), %rdx
	call	_ZN6icu_6713UCollationPCE13nextProcessedEPiS1_P10UErrorCode@PLT
	movq	-96(%rbp), %r10
	movq	%rax, (%r15)
	leaq	(%r10,%r13), %rax
	movq	%rax, -1872(%rbp)
.L359:
	cmpq	$0, -1872(%rbp)
	je	.L361
	movl	1056(%rbx), %r13d
	testl	%r13d, %r13d
	jle	.L434
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
.L375:
	movq	1064(%rbx), %rdx
	movslq	%r12d, %rax
	movl	-88(%rbp), %esi
	movl	-80(%rbp), %edi
	movq	(%rdx,%rax,8), %r15
	movl	-1844(%rbp), %eax
	leal	(%rax,%r12), %ecx
	addl	%r13d, %ecx
	movl	%ecx, %eax
	cltd
	idivl	%esi
	movl	-84(%rbp), %eax
	cmpl	%eax, %ecx
	jl	.L366
	cmpl	%edi, %ecx
	jl	.L521
.L366:
	cmpl	%edi, %ecx
	jne	.L368
	addl	$1, %ecx
	movl	%ecx, -80(%rbp)
	subl	%eax, %ecx
	cmpl	%ecx, %esi
	jg	.L369
	addl	$1, %eax
	movl	%eax, -84(%rbp)
.L369:
	movq	-64(%rbp), %rax
	movslq	%edx, %rdx
	movq	%r14, %rcx
	movl	$0, -1832(%rbp)
	salq	$4, %rdx
	addq	%rdx, %r10
	movq	4176(%rax), %rdi
	movq	%rdx, -1864(%rbp)
	leaq	8(%r10), %rsi
	leaq	12(%r10), %rdx
	movq	%r10, -1856(%rbp)
	call	_ZN6icu_6713UCollationPCE13nextProcessedEPiS1_P10UErrorCode@PLT
	movq	-1856(%rbp), %r10
	movq	-1864(%rbp), %r9
	movq	%rax, (%r10)
	movq	-96(%rbp), %r10
	leaq	(%r10,%r9), %rdx
.L367:
	movq	(%rbx), %rax
	movq	(%rdx), %r11
	movswl	14(%rax), %r9d
	cmpq	%r11, %r15
	je	.L518
	testw	%r9w, %r9w
	je	.L372
	movl	%r9d, %edx
	movq	%r15, %rsi
	movq	%r11, %rdi
	call	_ZL12compareCE64slls.part.0
	testl	%eax, %eax
	je	.L372
	jle	.L518
	cmpl	$1, %eax
	je	.L522
	subl	$1, %r13d
.L518:
	addl	$1, %r12d
.L371:
	movl	1056(%rbx), %eax
	cmpl	%r12d, %eax
	jg	.L375
	addl	%eax, %r13d
.L365:
	addl	-1844(%rbp), %r13d
	movl	-88(%rbp), %esi
	leal	-1(%r13), %ecx
	movl	-84(%rbp), %edi
	movl	-80(%rbp), %r9d
	movl	%ecx, %eax
	cltd
	idivl	%esi
	cmpl	%edi, %ecx
	jl	.L377
	cmpl	%r9d, %ecx
	jl	.L523
.L377:
	cmpl	%r9d, %ecx
	jne	.L379
	addl	$1, %ecx
	movl	%ecx, -80(%rbp)
	subl	%edi, %ecx
	cmpl	%ecx, %esi
	jg	.L380
	addl	$1, %edi
	movl	%edi, -84(%rbp)
.L380:
	movq	-64(%rbp), %rax
	movslq	%edx, %r8
	movq	%r14, %rcx
	movl	$0, -1832(%rbp)
	salq	$4, %r8
	addq	%r8, %r10
	movq	4176(%rax), %rdi
	movq	%r8, %r12
	leaq	8(%r10), %rsi
	leaq	12(%r10), %rdx
	movq	%r10, -1856(%rbp)
	call	_ZN6icu_6713UCollationPCE13nextProcessedEPiS1_P10UErrorCode@PLT
	movq	-1856(%rbp), %r10
	movl	-88(%rbp), %esi
	movl	-84(%rbp), %edi
	movl	-80(%rbp), %r9d
	movq	%rax, (%r10)
	movq	-96(%rbp), %r10
	leaq	(%r10,%r12), %rax
	movq	%rax, -1880(%rbp)
.L378:
	movq	-1872(%rbp), %rax
	movq	(%rbx), %r11
	movl	8(%rax), %eax
	movl	%eax, -1848(%rbp)
	movq	-1880(%rbp), %rax
	movl	8(%rax), %eax
	movl	%eax, -1864(%rbp)
	movl	%r13d, %eax
	cltd
	idivl	%esi
	cmpw	$0, 14(%r11)
	jne	.L524
	cmpl	%edi, %r13d
	jl	.L383
	cmpl	%r9d, %r13d
	jl	.L525
.L383:
	cmpl	%r9d, %r13d
	jne	.L385
	leal	1(%r13), %r8d
	movl	%r8d, -80(%rbp)
	subl	%edi, %r8d
	cmpl	%esi, %r8d
	jl	.L386
	addl	$1, %edi
	movl	%edi, -84(%rbp)
.L386:
	movq	-64(%rbp), %rax
	movslq	%edx, %rcx
	movl	$0, -1832(%rbp)
	salq	$4, %rcx
	leaq	(%r10,%rcx), %r15
	movq	4176(%rax), %rdi
	movq	%rcx, %r13
	movq	%r14, %rcx
	leaq	12(%r15), %rdx
	leaq	8(%r15), %rsi
	call	_ZN6icu_6713UCollationPCE13nextProcessedEPiS1_P10UErrorCode@PLT
	movq	(%rbx), %r11
	addq	-96(%rbp), %r13
	movq	%rax, (%r15)
.L384:
	movl	8(%r13), %eax
	movl	$1, %edx
	movl	%eax, %r12d
	cmpl	12(%r13), %eax
	jne	.L387
	movabsq	$9223372036854775807, %rax
	cmpq	%rax, 0(%r13)
	sete	%dl
	.p2align 4,,10
	.p2align 3
.L387:
	movq	24(%r11), %rdi
	testq	%rdi, %rdi
	je	.L526
.L394:
	movl	-1848(%rbp), %esi
	movb	%dl, -1856(%rbp)
	call	ubrk_isBoundary_67@PLT
	movq	(%rbx), %r11
	movl	%eax, %r15d
	movq	24(%r11), %rdi
	testb	%al, %al
	je	.L395
	movzbl	-1856(%rbp), %edx
	movl	%edx, %r15d
.L395:
	movq	-1872(%rbp), %rax
	movl	-1848(%rbp), %ecx
	cmpl	12(%rax), %ecx
	movl	$0, %eax
	cmove	%eax, %r15d
	movq	(%r11), %rax
	testq	%rax, %rax
	je	.L398
	movl	8(%r11), %edx
	cmpl	%r12d, %edx
	jle	.L398
	testq	%rdi, %rdi
	je	.L527
.L399:
	cmpl	%r12d, -1864(%rbp)
	jge	.L432
	xorl	%edx, %edx
.L431:
	movq	(%rbx), %r11
	movq	24(%r11), %rdi
.L430:
	movq	-1880(%rbp), %rax
	movl	-1864(%rbp), %esi
	cmpl	12(%rax), %esi
	je	.L528
.L404:
	testq	%rdi, %rdi
	je	.L529
.L408:
	movl	-1864(%rbp), %esi
	movb	%dl, -1856(%rbp)
	call	ubrk_following_67@PLT
	movq	-1880(%rbp), %rsi
	movzbl	-1856(%rbp), %edx
	movl	%eax, -1864(%rbp)
	cmpl	12(%rsi), %eax
	jl	.L403
	testb	%dl, %dl
	je	.L512
	cmpl	%eax, %r12d
	jg	.L429
.L403:
	testb	%dl, %dl
	je	.L432
.L410:
	cmpl	$15, 4196(%rbx)
	je	.L427
	movq	-96(%rbp), %r10
	testb	%r15b, %r15b
	jne	.L514
.L376:
	addl	$1, -1844(%rbp)
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L398:
	xorl	%edx, %edx
	cmpl	%r12d, -1864(%rbp)
	jl	.L430
.L411:
	testq	%rdi, %rdi
	je	.L530
.L412:
	movl	%r12d, %esi
	call	ubrk_isBoundary_67@PLT
	testb	%al, %al
	jne	.L410
.L413:
	cmpl	$15, 4196(%rbx)
	je	.L443
	movq	-96(%rbp), %r10
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L521:
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%r10, %rdx
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L372:
	movabsq	$9223372036854775807, %rax
	cmpq	%rax, %r11
	jne	.L376
	movl	$-1, -1848(%rbp)
	xorl	%r9d, %r9d
	movl	$-1, %r13d
.L364:
	movq	-1904(%rbp), %rax
	testq	%rax, %rax
	je	.L424
	movl	-1848(%rbp), %ebx
	movl	%ebx, (%rax)
.L424:
	movq	-1912(%rbp), %rax
	testq	%rax, %rax
	je	.L425
	movl	%r13d, (%rax)
.L425:
	cmpq	-1888(%rbp), %r10
	je	.L343
	movq	%r10, %rdi
	movb	%r9b, -1844(%rbp)
	call	uprv_free_67@PLT
	movzbl	-1844(%rbp), %r9d
.L343:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L531
	addq	$1880, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	addl	$1, %r13d
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L526:
	movq	16(%r11), %rdi
	testq	%rdi, %rdi
	jne	.L394
	xorl	%r15d, %r15d
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L443:
	xorl	%r15d, %r15d
.L427:
	movq	4160(%rbx), %r9
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%esi, %esi
	movl	$2, %ecx
	movq	%rax, -1824(%rbp)
	movl	$2, %edx
	leaq	-1696(%rbp), %r8
	movq	%rax, -1760(%rbp)
	movq	(%r9), %rax
	movq	%r8, %rdi
	movw	%cx, -1752(%rbp)
	movq	(%rbx), %rcx
	movq	24(%rax), %rax
	movw	%dx, -1816(%rbp)
	movq	(%rcx), %rcx
	movslq	-1848(%rbp), %rdx
	movq	%r9, -1880(%rbp)
	movq	%rax, -1872(%rbp)
	movq	%rdx, %rax
	leaq	(%rcx,%rdx,2), %rdx
	movl	%r12d, %ecx
	movq	%r8, -1856(%rbp)
	subl	%eax, %ecx
	movq	%rdx, -1832(%rbp)
	movq	%r14, %rdx
	movl	$0, -1836(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-1856(%rbp), %r8
	movq	-1880(%rbp), %r9
	leaq	-1836(%rbp), %r11
	leaq	-1824(%rbp), %rdx
	movq	%r11, -1864(%rbp)
	movq	%r11, %rcx
	movq	-1872(%rbp), %rax
	movq	%r8, %rsi
	movq	%r9, %rdi
	movq	%rdx, %r13
	call	*%rax
	movq	-1856(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	4160(%rbx), %r10
	movq	8(%rbx), %rdx
	xorl	%esi, %esi
	movq	-1856(%rbp), %r8
	movl	16(%rbx), %ecx
	movq	(%r10), %rax
	movq	%r10, -1880(%rbp)
	movq	%r8, %rdi
	movq	24(%rax), %rax
	movq	%rdx, -1832(%rbp)
	movq	%r14, %rdx
	movq	%rax, -1872(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-1864(%rbp), %r11
	movq	-1856(%rbp), %r8
	leaq	-1760(%rbp), %r9
	movq	-1880(%rbp), %r10
	movq	%r9, -1864(%rbp)
	movq	%r9, %rdx
	movq	%r8, %rsi
	movq	%r11, %rcx
	movq	-1872(%rbp), %rax
	movq	%r10, %rdi
	call	*%rax
	movq	-1856(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-1836(%rbp), %esi
	movq	-1864(%rbp), %r9
	testl	%esi, %esi
	jle	.L532
.L415:
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-96(%rbp), %r10
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L524:
	movq	%r10, %rax
	movq	%rbx, -1856(%rbp)
	movq	%r11, %r10
	movl	%r13d, %r12d
	movq	%rax, %r11
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L534:
	movq	%r15, %rsi
	call	_ZL12compareCE64slls.part.0
	andl	$-3, %eax
	je	.L515
.L393:
	movl	-88(%rbp), %esi
	movl	-84(%rbp), %edi
	addl	$1, %r12d
	movl	-80(%rbp), %r9d
.L382:
	movl	%r12d, %eax
	cltd
	idivl	%esi
	cmpl	%edi, %r12d
	jl	.L388
	cmpl	%r9d, %r12d
	jl	.L533
.L388:
	cmpl	%r9d, %r12d
	jne	.L390
	leal	1(%r12), %eax
	movl	%eax, -80(%rbp)
	subl	%edi, %eax
	cmpl	%esi, %eax
	jl	.L391
	addl	$1, %edi
	movl	%edi, -84(%rbp)
.L391:
	movq	-64(%rbp), %rax
	movslq	%edx, %r13
	movq	%r14, %rcx
	movl	$0, -1832(%rbp)
	salq	$4, %r13
	leaq	(%r11,%r13), %rbx
	movq	4176(%rax), %rdi
	leaq	12(%rbx), %rdx
	leaq	8(%rbx), %rsi
	call	_ZN6icu_6713UCollationPCE13nextProcessedEPiS1_P10UErrorCode@PLT
	movq	-96(%rbp), %r11
	movq	%rax, (%rbx)
	movq	-1856(%rbp), %rax
	addq	%r11, %r13
	movq	(%rax), %r10
.L389:
	movq	0(%r13), %rdi
	movl	8(%r13), %ebx
	movabsq	$9223372036854775807, %rax
	cmpq	%rax, %rdi
	je	.L436
	movq	%rdi, %rax
	sarq	$32, %rax
	testl	$4294901760, %eax
	jne	.L392
	movswl	14(%r10), %edx
	cmpq	%r15, %rdi
	je	.L393
	testw	%dx, %dx
	jne	.L534
.L515:
	movl	%ebx, %r12d
	movq	%r10, %r11
	movq	-1856(%rbp), %rbx
	xorl	%edx, %edx
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L533:
	movslq	%edx, %rdx
	salq	$4, %rdx
	leaq	(%r11,%rdx), %r13
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L520:
	movslq	%edx, %rdx
	salq	$4, %rdx
	leaq	(%r10,%rdx), %rax
	movq	%rax, -1872(%rbp)
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L529:
	movq	16(%r11), %rdi
	testq	%rdi, %rdi
	jne	.L408
.L406:
	movq	-1880(%rbp), %rax
	movl	-1864(%rbp), %ecx
	cmpl	12(%rax), %ecx
	jl	.L403
.L429:
	testb	%dl, %dl
	jne	.L444
.L512:
	movq	(%rbx), %r11
	movl	-1864(%rbp), %eax
	movq	24(%r11), %rdi
	cmpl	%eax, %r12d
	jge	.L442
	movl	%eax, %r12d
	xorl	%r15d, %r15d
	testq	%rdi, %rdi
	jne	.L412
.L530:
	movq	16(%r11), %rdi
	testq	%rdi, %rdi
	je	.L413
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L528:
	testq	%rdi, %rdi
	je	.L535
.L405:
	movl	-1864(%rbp), %esi
	movb	%dl, -1856(%rbp)
	call	ubrk_isBoundary_67@PLT
	movzbl	-1856(%rbp), %edx
	testb	%al, %al
	je	.L536
	testb	%dl, %dl
	jne	.L444
	movq	(%rbx), %r11
	movl	-1864(%rbp), %eax
	movq	24(%r11), %rdi
	movl	%eax, %r12d
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L523:
	movslq	%edx, %rdx
	salq	$4, %rdx
	leaq	(%r10,%rdx), %rax
	movq	%rax, -1880(%rbp)
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L392:
	movl	%ebx, %eax
	cmpl	%eax, 12(%r13)
	movl	%ebx, %r12d
	movq	%r10, %r11
	movq	-1856(%rbp), %rbx
	setne	%dl
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L436:
	movl	%ebx, %r12d
	movq	%r10, %r11
	movl	$1, %edx
	movq	-1856(%rbp), %rbx
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L525:
	movslq	%edx, %rdx
	salq	$4, %rdx
	leaq	(%r10,%rdx), %r13
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L532:
	movswl	-1752(%rbp), %ecx
	movzwl	-1816(%rbp), %eax
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L416
	testw	%ax, %ax
	js	.L417
	movswl	%ax, %edx
	sarl	$5, %edx
.L418:
	testw	%cx, %cx
	js	.L419
	sarl	$5, %ecx
.L420:
	testb	%sil, %sil
	jne	.L415
	cmpl	%edx, %ecx
	jne	.L415
	movq	%r9, %rsi
	movq	%r13, %rdi
	movq	%r9, -1856(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-1856(%rbp), %r9
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L416:
	testb	%sil, %sil
	je	.L415
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-96(%rbp), %r10
	testb	%r15b, %r15b
	je	.L376
.L514:
	movl	%r12d, %r13d
	movl	%r15d, %r9d
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L535:
	movq	16(%r11), %rdi
	testq	%rdi, %rdi
	je	.L406
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L527:
	movq	0(%r13), %rsi
	sarq	$32, %rsi
	testl	$4294901760, %esi
	je	.L399
	movq	-1880(%rbp), %rsi
	cmpl	12(%rsi), %r12d
	jl	.L399
	cmpl	12(%r13), %r12d
	jge	.L399
	movq	4160(%rbx), %rdi
	movslq	%r12d, %rsi
	leal	1(%r12), %r9d
	movq	(%rdi), %rcx
	movq	120(%rcx), %r8
	leaq	(%rsi,%rsi), %rcx
	movzwl	(%rax,%rsi,2), %esi
	cmpl	%edx, %r9d
	je	.L400
	movl	%esi, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L400
	movzwl	2(%rax,%rcx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L400
	sall	$10, %esi
	leal	-56613888(%rax,%rsi), %esi
.L400:
	movq	%rcx, -1856(%rbp)
	call	*%r8
	testb	%al, %al
	jne	.L401
	movq	4160(%rbx), %rdi
	movq	(%rbx), %rdx
	movq	(%rdi), %rax
	movq	128(%rax), %rax
	testl	%r12d, %r12d
	jle	.L440
	movq	(%rdx), %rdx
	movq	-1856(%rbp), %rcx
	movzwl	-2(%rdx,%rcx), %esi
	movl	%esi, %r8d
	andl	$-1024, %r8d
	cmpl	$56320, %r8d
	jne	.L402
	cmpl	$1, %r12d
	jne	.L537
.L402:
	call	*%rax
	testb	%al, %al
	setne	%dl
	cmpl	%r12d, -1864(%rbp)
	jge	.L403
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L536:
	movq	(%rbx), %r11
	movq	24(%r11), %rdi
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L361:
	movq	-1896(%rbp), %rax
	xorl	%r9d, %r9d
	movl	$-1, %r13d
	movl	$-1, -1848(%rbp)
	movl	$5, (%rax)
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L434:
	xorl	%r15d, %r15d
	jmp	.L365
.L519:
	movl	16(%rdi), %eax
	movq	8(%rdi), %rsi
	movl	%eax, -1848(%rbp)
	movq	4184(%rdi), %rax
	movq	%rax, -1856(%rbp)
	testq	%rax, %rax
	je	.L538
	movl	-1848(%rbp), %edx
	movq	%r8, %rcx
	movq	%rax, %rdi
	call	ucol_setText_67@PLT
.L346:
	leaq	-1632(%rbp), %rax
	movq	%rax, -1888(%rbp)
	movq	-1896(%rbp), %rax
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jg	.L344
	movq	1064(%rbx), %rdi
	leaq	1072(%rbx), %rax
	movq	%rax, -1872(%rbp)
	cmpq	%rdi, %rax
	je	.L348
	testq	%rdi, %rdi
	je	.L348
	call	uprv_free_67@PLT
.L348:
	leaq	-1632(%rbp), %rax
	movq	-1856(%rbp), %rsi
	xorl	%r12d, %r12d
	movq	%rax, %rdi
	movq	%rax, -1888(%rbp)
	call	_ZN6icu_6713UCollationPCEC1EP18UCollationElements@PLT
	movq	%rbx, -1880(%rbp)
	movq	-1872(%rbp), %r14
	movl	%r13d, -1916(%rbp)
	movq	-1896(%rbp), %rbx
	movl	$256, -1844(%rbp)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L539:
	movl	(%rbx), %eax
	movq	%r13, (%r14,%r12,8)
	testl	%eax, %eax
	jg	.L513
.L353:
	movl	%r15d, %r12d
.L349:
	movq	-1888(%rbp), %rdi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN6icu_6713UCollationPCE13nextProcessedEPiS1_P10UErrorCode@PLT
	movq	%rax, %r13
	movabsq	$9223372036854775807, %rax
	cmpq	%rax, %r13
	je	.L350
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L350
	movq	-1856(%rbp), %rdi
	leal	1(%r12), %r15d
	call	ucol_getOffset_67@PLT
	cmpl	-1844(%rbp), %r15d
	jne	.L539
	movl	-1844(%rbp), %esi
	movl	-1848(%rbp), %ecx
	leal	1(%rcx,%rsi), %edx
	subl	%eax, %edx
	leal	0(,%rdx,8), %edi
	movl	%edx, -1844(%rbp)
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L540
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L513
	leaq	0(,%r12,8), %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%r13, (%rax,%r12,8)
	movq	%rax, %r9
	cmpq	%rax, %r14
	je	.L517
	cmpq	%r14, -1872(%rbp)
	je	.L517
	movq	%r14, %rdi
	movq	%rax, -1864(%rbp)
	call	uprv_free_67@PLT
	movq	-1864(%rbp), %r9
.L517:
	movq	%r9, %r14
	jmp	.L353
.L419:
	movl	-1748(%rbp), %ecx
	jmp	.L420
.L417:
	movl	-1812(%rbp), %edx
	jmp	.L418
.L350:
	movq	-1880(%rbp), %rbx
	movl	%r12d, %eax
	movl	-1916(%rbp), %r13d
	movq	$0, (%r14,%rax,8)
	movq	%r14, 1064(%rbx)
	movl	%r12d, 1056(%rbx)
.L357:
	movq	-1888(%rbp), %rdi
	call	_ZN6icu_6713UCollationPCED1Ev@PLT
	jmp	.L344
.L513:
	movq	-1880(%rbp), %rbx
	movl	-1916(%rbp), %r13d
	jmp	.L357
.L538:
	movq	4152(%rdi), %rdi
	movl	-1848(%rbp), %edx
	movq	%r8, %rcx
	call	ucol_openElements_67@PLT
	movq	%rax, -1856(%rbp)
	movq	%rax, 4184(%rbx)
	jmp	.L346
.L401:
	cmpl	%r12d, -1864(%rbp)
	jge	.L410
	movl	$1, %edx
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L537:
	movzwl	-4(%rdx,%rcx), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L402
	sall	$10, %edx
	leal	-56613888(%rsi,%rdx), %esi
	jmp	.L402
.L540:
	movq	-1896(%rbp), %rax
	movq	-1880(%rbp), %rbx
	movl	-1916(%rbp), %r13d
	movl	$7, (%rax)
	jmp	.L357
.L440:
	orl	$-1, %esi
	jmp	.L402
.L442:
	movl	-1864(%rbp), %eax
	movl	%eax, %r12d
	jmp	.L411
.L444:
	movl	-1864(%rbp), %eax
	movl	%eax, %r12d
	jmp	.L410
.L432:
	movq	(%rbx), %r11
	movq	24(%r11), %rdi
	jmp	.L411
.L531:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	usearch_search_67.part.0.cold, @function
usearch_search_67.part.0.cold:
.LFSB4466:
.L385:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	8, %eax
	ud2
.L390:
	movl	8, %eax
	ud2
.L379:
	movl	8, %eax
	ud2
.L368:
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE4466:
	.text
	.size	usearch_search_67.part.0, .-usearch_search_67.part.0
	.section	.text.unlikely
	.size	usearch_search_67.part.0.cold, .-usearch_search_67.part.0.cold
.LCOLDE1:
	.text
.LHOTE1:
	.p2align 4
	.globl	usearch_close_67
	.type	usearch_close_67, @function
usearch_close_67:
.LFB3241:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L541
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	24(%rdi), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L543
	testq	%rdi, %rdi
	je	.L543
	call	uprv_free_67@PLT
.L543:
	movq	1064(%r12), %rdi
	testq	%rdi, %rdi
	je	.L544
	leaq	1072(%r12), %rax
	cmpq	%rax, %rdi
	je	.L544
	call	uprv_free_67@PLT
.L544:
	movq	4176(%r12), %r13
	testq	%r13, %r13
	je	.L545
	movq	%r13, %rdi
	call	_ZN6icu_6713UCollationPCED1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L545:
	movq	4168(%r12), %rdi
	call	ucol_closeElements_67@PLT
	movq	4184(%r12), %rdi
	call	ucol_closeElements_67@PLT
	cmpb	$0, 4192(%r12)
	je	.L546
	movq	4152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L546
	call	ucol_close_67@PLT
.L546:
	movq	(%r12), %rdi
	movq	16(%rdi), %r8
	testq	%r8, %r8
	je	.L547
	movq	%r8, %rdi
	call	ubrk_close_67@PLT
	movq	(%r12), %rdi
.L547:
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L541:
	ret
	.cfi_endproc
.LFE3241:
	.size	usearch_close_67, .-usearch_close_67
	.p2align 4
	.globl	usearch_openFromCollator_67
	.type	usearch_openFromCollator_67, @function
usearch_openFromCollator_67:
.LFB3240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movl	%esi, -56(%rbp)
	movl	%ecx, -52(%rbp)
	movl	(%rbx), %r12d
	movq	%r9, -64(%rbp)
	testl	%r12d, %r12d
	jg	.L595
	testq	%rdx, %rdx
	movq	%rdx, %r12
	movq	%r8, %r14
	sete	%dl
	testq	%r8, %r8
	sete	%al
	orb	%al, %dl
	jne	.L581
	movq	%rdi, %r13
	testq	%rdi, %rdi
	je	.L581
	movq	%rbx, %rdx
	movl	$7, %esi
	movq	%r8, %rdi
	call	ucol_getAttribute_67@PLT
	cmpl	$17, %eax
	je	.L598
	movl	(%rbx), %r11d
	testl	%r11d, %r11d
	jg	.L595
	cmpq	$0, _ZL9g_nfcImpl(%rip)
	je	.L576
.L579:
	cmpl	$-1, -52(%rbp)
	je	.L599
.L578:
	cmpl	$-1, -56(%rbp)
	je	.L600
.L580:
	movl	-52(%rbp), %r9d
	testl	%r9d, %r9d
	jle	.L581
	movl	-56(%rbp), %r8d
	testl	%r8d, %r8d
	jle	.L581
	movl	$5240, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L601
	movq	%r14, 4152(%rax)
	movq	%r14, %rdi
	call	ucol_getStrength_67@PLT
	movl	$-65536, %edx
	movl	%eax, 4196(%r15)
	testl	%eax, %eax
	je	.L583
	cmpl	$1, %eax
	movl	$-1, %edx
	movl	$-256, %eax
	cmove	%eax, %edx
.L583:
	movl	%edx, 4200(%r15)
	movl	$1, %esi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	ucol_getAttribute_67@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	cmpl	$20, %eax
	sete	4208(%r15)
	call	ucol_getVariableTop_67@PLT
	movq	%rbx, %rdi
	movl	%eax, 4204(%r15)
	call	_ZN6icu_6711Normalizer214getNFDInstanceER10UErrorCode@PLT
	movl	(%rbx), %edi
	movq	%rax, 4160(%r15)
	testl	%edi, %edi
	jg	.L597
	movl	$48, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L602
	movl	-56(%rbp), %esi
	movl	-52(%rbp), %ecx
	movq	%r13, 8(%r15)
	movq	%rbx, %rdx
	movq	%r12, (%rax)
	movq	4152(%r15), %rdi
	movl	%esi, 16(%r15)
	movq	-64(%rbp), %rsi
	movl	%ecx, 8(%rax)
	movq	%rsi, 24(%rax)
	movl	$1, %esi
	movq	$0, 24(%r15)
	movq	$0, 1064(%r15)
	call	ucol_getLocaleByType_67@PLT
	movl	-52(%rbp), %ecx
	xorl	%edi, %edi
	movq	%rbx, %r8
	movq	%rax, %rsi
	movq	%r12, %rdx
	movq	(%r15), %r13
	call	ubrk_open_67@PLT
	movq	-64(%rbp), %rdi
	movq	%rax, 16(%r13)
	testq	%rdi, %rdi
	je	.L586
	movl	-52(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r12, %rsi
	call	ubrk_setText_67@PLT
.L586:
	movq	(%r15), %rax
	movl	$4294967295, %ecx
	movl	-52(%rbp), %edx
	movq	%r12, %rsi
	movb	$0, 4192(%r15)
	movq	%r14, %rdi
	movq	%rcx, 32(%rax)
	movq	%rbx, %rcx
	movq	$0, 4184(%r15)
	call	ucol_openElements_67@PLT
	movl	(%rbx), %esi
	movq	$0, 4176(%r15)
	movq	%rax, 4168(%r15)
	testl	%esi, %esi
	jg	.L596
	movq	(%r15), %rax
	movl	$257, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	$0, 12(%rax)
	movw	%dx, 40(%rax)
	call	_ZL10initializeP13UStringSearchP10UErrorCode
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L569
.L596:
	movq	%r15, %rdi
	call	usearch_close_67
	.p2align 4,,10
	.p2align 3
.L595:
	xorl	%r15d, %r15d
.L569:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L581:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%r15d, %r15d
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L598:
	movl	$16, (%rbx)
	xorl	%r15d, %r15d
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L576:
	movq	%rbx, %rdi
	call	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode@PLT
	leaq	usearch_cleanup(%rip), %rsi
	movl	$26, %edi
	movq	%rax, _ZL9g_nfcImpl(%rip)
	call	ucln_i18n_registerCleanup_67@PLT
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L595
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L599:
	movq	%r12, %rdi
	call	u_strlen_67@PLT
	movl	%eax, -52(%rbp)
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L600:
	movq	%r13, %rdi
	call	u_strlen_67@PLT
	movl	%eax, -56(%rbp)
	jmp	.L580
.L602:
	movl	$7, (%rbx)
	.p2align 4,,10
	.p2align 3
.L597:
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	uprv_free_67@PLT
	jmp	.L569
.L601:
	movl	$7, (%rbx)
	jmp	.L569
	.cfi_endproc
.LFE3240:
	.size	usearch_openFromCollator_67, .-usearch_openFromCollator_67
	.p2align 4
	.globl	usearch_open_67
	.type	usearch_open_67, @function
usearch_open_67:
.LFB3239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r15
	movl	%esi, -56(%rbp)
	movq	%r9, -64(%rbp)
	movl	(%r15), %ebx
	testl	%ebx, %ebx
	jg	.L640
	testq	%r8, %r8
	je	.L606
	movq	%rdi, %rbx
	movq	%r15, %rsi
	movq	%r8, %rdi
	movq	%rdx, %r12
	movl	%ecx, %r13d
	call	ucol_open_67@PLT
	movl	(%r15), %r11d
	movq	%rax, %r14
	testl	%r11d, %r11d
	jg	.L607
	testq	%rbx, %rbx
	sete	%dl
	testq	%r12, %r12
	sete	%al
	orb	%al, %dl
	jne	.L631
	testq	%r14, %r14
	je	.L631
	movq	%r15, %rdx
	movl	$7, %esi
	movq	%r14, %rdi
	call	ucol_getAttribute_67@PLT
	cmpl	$17, %eax
	je	.L641
	movl	(%r15), %r10d
	testl	%r10d, %r10d
	jle	.L642
.L611:
	movq	%r14, %rdi
	call	ucol_close_67@PLT
	xorl	%r10d, %r10d
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L606:
	movl	$1, (%r15)
.L640:
	xorl	%r10d, %r10d
.L603:
	addq	$40, %rsp
	movq	%r10, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L631:
	.cfi_restore_state
	movl	$1, (%r15)
.L607:
	testq	%r14, %r14
	je	.L640
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L642:
	cmpq	$0, _ZL9g_nfcImpl(%rip)
	je	.L613
.L617:
	cmpl	$-1, %r13d
	je	.L643
.L615:
	cmpl	$-1, -56(%rbp)
	je	.L644
.L618:
	testl	%r13d, %r13d
	jle	.L632
	movl	-56(%rbp), %r8d
	testl	%r8d, %r8d
	jle	.L632
	movl	$5240, %edi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L645
	movq	%r14, 4152(%rax)
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	ucol_getStrength_67@PLT
	movq	-72(%rbp), %r10
	movl	$-65536, %edx
	movl	%eax, 4196(%r10)
	testl	%eax, %eax
	je	.L622
	cmpl	$1, %eax
	movl	$-1, %edx
	movl	$-256, %eax
	cmove	%eax, %edx
.L622:
	movl	%edx, 4200(%r10)
	movl	$1, %esi
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%r10, -72(%rbp)
	call	ucol_getAttribute_67@PLT
	movq	-72(%rbp), %r10
	movq	%r15, %rsi
	movq	%r14, %rdi
	cmpl	$20, %eax
	sete	4208(%r10)
	call	ucol_getVariableTop_67@PLT
	movq	-72(%rbp), %r10
	movq	%r15, %rdi
	movl	%eax, 4204(%r10)
	call	_ZN6icu_6711Normalizer214getNFDInstanceER10UErrorCode@PLT
	movq	-72(%rbp), %r10
	movl	(%r15), %edi
	movq	%rax, 4160(%r10)
	testl	%edi, %edi
	jg	.L638
	movl	$48, %edi
	movq	%r10, -72(%rbp)
	call	uprv_malloc_67@PLT
	movq	-72(%rbp), %r10
	movq	%rax, (%r10)
	testq	%rax, %rax
	je	.L646
	movq	%rbx, 8(%r10)
	movq	-64(%rbp), %rdi
	movq	%r15, %rdx
	movl	$1, %esi
	movl	-56(%rbp), %ebx
	movq	$0, 24(%r10)
	movq	$0, 1064(%r10)
	movl	%ebx, 16(%r10)
	movq	%rdi, 24(%rax)
	movq	4152(%r10), %rdi
	movq	%r12, (%rax)
	movl	%r13d, 8(%rax)
	movq	%r10, -56(%rbp)
	call	ucol_getLocaleByType_67@PLT
	movq	-56(%rbp), %r10
	xorl	%edi, %edi
	movq	%r15, %r8
	movq	%rax, %rsi
	movl	%r13d, %ecx
	movq	%r12, %rdx
	movq	(%r10), %rbx
	call	ubrk_open_67@PLT
	movq	-64(%rbp), %rdi
	movq	-56(%rbp), %r10
	movq	%rax, 16(%rbx)
	testq	%rdi, %rdi
	je	.L625
	movq	%r15, %rcx
	movl	%r13d, %edx
	movq	%r12, %rsi
	call	ubrk_setText_67@PLT
	movq	-56(%rbp), %r10
.L625:
	movq	(%r10), %rax
	movl	$4294967295, %ebx
	movq	%r12, %rsi
	movq	%r15, %rcx
	movb	$0, 4192(%r10)
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%rbx, 32(%rax)
	movq	$0, 4184(%r10)
	movq	%r10, -56(%rbp)
	call	ucol_openElements_67@PLT
	movq	-56(%rbp), %r10
	movl	(%r15), %esi
	movq	%rax, 4168(%r10)
	movq	$0, 4176(%r10)
	testl	%esi, %esi
	jg	.L639
	movq	(%r10), %rax
	movl	$257, %edx
	movq	%r10, %rdi
	movq	%r15, %rsi
	movq	%r10, -56(%rbp)
	movl	$0, 12(%rax)
	movw	%dx, 40(%rax)
	call	_ZL10initializeP13UStringSearchP10UErrorCode
	movl	(%r15), %ecx
	movq	-56(%rbp), %r10
	testl	%ecx, %ecx
	jg	.L639
	movb	$1, 4192(%r10)
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L639:
	movq	%r10, %rdi
	call	usearch_close_67
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L641:
	movl	$16, (%r15)
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L613:
	movq	%r15, %rdi
	call	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode@PLT
	leaq	usearch_cleanup(%rip), %rsi
	movl	$26, %edi
	movq	%rax, _ZL9g_nfcImpl(%rip)
	call	ucln_i18n_registerCleanup_67@PLT
	movl	(%r15), %r9d
	testl	%r9d, %r9d
	jg	.L611
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L632:
	movl	$1, (%r15)
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L644:
	movq	%rbx, %rdi
	call	u_strlen_67@PLT
	movl	%eax, -56(%rbp)
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L643:
	movq	%r12, %rdi
	call	u_strlen_67@PLT
	movl	%eax, %r13d
	jmp	.L615
.L646:
	movl	$7, (%r15)
	.p2align 4,,10
	.p2align 3
.L638:
	movq	%r10, %rdi
	call	uprv_free_67@PLT
	jmp	.L611
.L645:
	movl	$7, (%r15)
	jmp	.L611
	.cfi_endproc
.LFE3239:
	.size	usearch_open_67, .-usearch_open_67
	.p2align 4
	.globl	usearch_setOffset_67
	.type	usearch_setOffset_67, @function
usearch_setOffset_67:
.LFB3243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L647
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	jne	.L661
.L647:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L662
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L661:
	.cfi_restore_state
	movq	(%rdi), %rax
	cmpl	%esi, 8(%rax)
	jl	.L653
	testl	%esi, %esi
	js	.L653
	movq	4168(%rdi), %rdi
	leaq	-28(%rbp), %rdx
	movl	$0, -28(%rbp)
	call	ucol_setOffset_67@PLT
	movq	(%rbx), %rax
.L651:
	movl	$4294967295, %ecx
	movb	$0, 41(%rax)
	movq	%rcx, 32(%rax)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L653:
	movl	$8, (%rdx)
	jmp	.L651
.L662:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3243:
	.size	usearch_setOffset_67, .-usearch_setOffset_67
	.p2align 4
	.globl	usearch_getOffset_67
	.type	usearch_getOffset_67, @function
usearch_getOffset_67:
.LFB3244:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L670
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	4168(%rdi), %rdi
	call	ucol_getOffset_67@PLT
	movq	(%rbx), %rdx
	cmpl	%eax, 8(%rdx)
	jl	.L666
	testl	%eax, %eax
	js	.L666
.L663:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L666:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L663
.L670:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE3244:
	.size	usearch_getOffset_67, .-usearch_getOffset_67
	.p2align 4
	.globl	usearch_setAttribute_67
	.type	usearch_setAttribute_67, @function
usearch_setAttribute_67:
.LFB3245:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L672
	testq	%rdi, %rdi
	jne	.L684
.L672:
	cmpl	$5, %edx
	je	.L685
	ret
	.p2align 4,,10
	.p2align 3
.L685:
	movl	$1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L684:
	cmpl	$1, %esi
	je	.L673
	cmpl	$2, %esi
	je	.L674
	testl	%esi, %esi
	je	.L686
	movl	$1, (%rcx)
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L674:
	leal	-3(%rdx), %esi
	movq	(%rdi), %rax
	cmpl	$1, %esi
	jbe	.L687
	xorl	%esi, %esi
	movw	%si, 14(%rax)
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L686:
	movq	(%rdi), %rax
	cmpl	$1, %edx
	sete	12(%rax)
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L673:
	movq	(%rdi), %rax
	cmpl	$1, %edx
	sete	13(%rax)
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L687:
	movw	%dx, 14(%rax)
	ret
	.cfi_endproc
.LFE3245:
	.size	usearch_setAttribute_67, .-usearch_setAttribute_67
	.p2align 4
	.globl	usearch_getAttribute_67
	.type	usearch_getAttribute_67, @function
usearch_getAttribute_67:
.LFB3246:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L692
	cmpl	$1, %esi
	je	.L690
	cmpl	$2, %esi
	je	.L691
	movl	$-1, %eax
	testl	%esi, %esi
	je	.L695
	ret
	.p2align 4,,10
	.p2align 3
.L695:
	movq	(%rdi), %rax
	cmpb	$1, 12(%rax)
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L690:
	movq	(%rdi), %rax
	cmpb	$1, 13(%rax)
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L691:
	movq	(%rdi), %rax
	movswl	14(%rax), %eax
	leal	-3(%rax), %edx
	cmpw	$2, %dx
	cmovnb	%esi, %eax
	ret
.L692:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3246:
	.size	usearch_getAttribute_67, .-usearch_getAttribute_67
	.p2align 4
	.globl	usearch_getMatchedStart_67
	.type	usearch_getMatchedStart_67, @function
usearch_getMatchedStart_67:
.LFB3247:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L698
	movq	(%rdi), %rax
	movl	32(%rax), %eax
	ret
.L698:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3247:
	.size	usearch_getMatchedStart_67, .-usearch_getMatchedStart_67
	.p2align 4
	.globl	usearch_getMatchedText_67
	.type	usearch_getMatchedText_67, @function
usearch_getMatchedText_67:
.LFB3248:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L713
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L701
	movl	%edx, %r12d
	testl	%edx, %edx
	js	.L701
	movq	%rsi, %rdi
	jle	.L702
	testq	%rsi, %rsi
	je	.L701
.L702:
	movq	(%rbx), %rcx
	movslq	32(%rcx), %rsi
	movl	36(%rcx), %edx
	cmpl	$-1, %esi
	je	.L716
	cmpl	%edx, %r12d
	movl	%edx, %eax
	cmovle	%r12d, %eax
	testl	%eax, %eax
	jle	.L704
	movslq	%eax, %rdx
	movq	(%rcx), %rax
	addq	%rdx, %rdx
	leaq	(%rax,%rsi,2), %rsi
	call	memcpy@PLT
	movq	%rax, %rdi
	movq	(%rbx), %rax
	movl	36(%rax), %edx
.L704:
	addq	$8, %rsp
	movq	%r13, %rcx
	movl	%r12d, %esi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	u_terminateUChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L701:
	.cfi_restore_state
	movl	$1, 0(%r13)
.L699:
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L716:
	.cfi_restore_state
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	%r12d, %esi
	call	u_terminateUChars_67@PLT
	jmp	.L699
.L713:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE3248:
	.size	usearch_getMatchedText_67, .-usearch_getMatchedText_67
	.p2align 4
	.globl	usearch_getMatchedLength_67
	.type	usearch_getMatchedLength_67, @function
usearch_getMatchedLength_67:
.LFB3249:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L719
	movq	(%rdi), %rax
	movl	36(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3249:
	.size	usearch_getMatchedLength_67, .-usearch_getMatchedLength_67
	.p2align 4
	.globl	usearch_setBreakIterator_67
	.type	usearch_setBreakIterator_67, @function
usearch_setBreakIterator_67:
.LFB3250:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	movq	%rsi, %r8
	testl	%eax, %eax
	jg	.L720
	testq	%rdi, %rdi
	jne	.L731
.L720:
	ret
	.p2align 4,,10
	.p2align 3
.L731:
	movq	(%rdi), %rax
	movq	%rsi, 24(%rax)
	testq	%rsi, %rsi
	je	.L720
	movl	8(%rax), %r9d
	movq	(%rax), %rsi
	movq	%rdx, %rcx
	movq	%r8, %rdi
	movl	%r9d, %edx
	jmp	ubrk_setText_67@PLT
	.cfi_endproc
.LFE3250:
	.size	usearch_setBreakIterator_67, .-usearch_setBreakIterator_67
	.p2align 4
	.globl	usearch_getBreakIterator_67
	.type	usearch_getBreakIterator_67, @function
usearch_getBreakIterator_67:
.LFB3251:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L732
	movq	(%rdi), %rax
	movq	24(%rax), %rax
.L732:
	ret
	.cfi_endproc
.LFE3251:
	.size	usearch_getBreakIterator_67, .-usearch_getBreakIterator_67
	.p2align 4
	.globl	usearch_setText_67
	.type	usearch_setText_67, @function
usearch_setText_67:
.LFB3252:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L750
	ret
	.p2align 4,,10
	.p2align 3
.L750:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	testq	%rdi, %rdi
	je	.L738
	testq	%rsi, %rsi
	je	.L738
	cmpl	$-1, %edx
	jl	.L738
	testl	%edx, %edx
	je	.L738
	cmpl	$-1, %edx
	je	.L751
.L741:
	movq	(%rbx), %rax
	movq	4168(%rbx), %rdi
	movq	%rcx, -40(%rbp)
	movl	%edx, -32(%rbp)
	movq	%rsi, (%rax)
	movl	%edx, 8(%rax)
	movq	%rsi, -24(%rbp)
	call	ucol_setText_67@PLT
	movq	(%rbx), %rax
	movl	$4294967295, %edi
	movl	-32(%rbp), %edx
	movq	-24(%rbp), %rsi
	movq	-40(%rbp), %rcx
	movq	%rdi, 32(%rax)
	movq	24(%rax), %rdi
	movb	$1, 41(%rax)
	testq	%rdi, %rdi
	je	.L742
	call	ubrk_setText_67@PLT
	movq	(%rbx), %rax
	movq	-40(%rbp), %rcx
	movl	-32(%rbp), %edx
	movq	-24(%rbp), %rsi
.L742:
	movq	16(%rax), %rdi
	addq	$40, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	ubrk_setText_67@PLT
	.p2align 4,,10
	.p2align 3
.L738:
	.cfi_restore_state
	movl	$1, (%rcx)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L751:
	.cfi_restore_state
	movq	%rsi, %rdi
	movq	%rcx, -32(%rbp)
	movq	%rsi, -24(%rbp)
	call	u_strlen_67@PLT
	movq	-32(%rbp), %rcx
	movq	-24(%rbp), %rsi
	movl	%eax, %edx
	jmp	.L741
	.cfi_endproc
.LFE3252:
	.size	usearch_setText_67, .-usearch_setText_67
	.p2align 4
	.globl	usearch_getText_67
	.type	usearch_getText_67, @function
usearch_getText_67:
.LFB3253:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L752
	movq	(%rdi), %rax
	movl	8(%rax), %edx
	movq	(%rax), %rax
	movl	%edx, (%rsi)
.L752:
	ret
	.cfi_endproc
.LFE3253:
	.size	usearch_getText_67, .-usearch_getText_67
	.p2align 4
	.globl	usearch_setCollator_67
	.type	usearch_setCollator_67, @function
usearch_setCollator_67:
.LFB3254:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L775
	ret
	.p2align 4,,10
	.p2align 3
.L775:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L776
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L756
	movq	4176(%rdi), %r15
	testq	%r15, %r15
	je	.L759
	movq	%r15, %rdi
	call	_ZN6icu_6713UCollationPCED1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L759:
	movq	$0, 4176(%r12)
	movq	4168(%r12), %rdi
	call	ucol_closeElements_67@PLT
	movq	4184(%r12), %rdi
	call	ucol_closeElements_67@PLT
	cmpb	$0, 4192(%r12)
	movq	$0, 4184(%r12)
	movq	$0, 4168(%r12)
	je	.L760
	movq	4152(%r12), %rdi
	cmpq	%r14, %rdi
	je	.L760
	call	ucol_close_67@PLT
	movb	$0, 4192(%r12)
.L760:
	movq	%r14, 4152(%r12)
	movq	%r14, %rdi
	call	ucol_getStrength_67@PLT
	movl	$-65536, %edx
	movl	%eax, 4196(%r12)
	testl	%eax, %eax
	je	.L761
	cmpl	$1, %eax
	movl	$-1, %edx
	movl	$-256, %eax
	cmove	%eax, %edx
.L761:
	movq	(%r12), %rax
	movl	%edx, 4200(%r12)
	movq	16(%rax), %rdi
	call	ubrk_close_67@PLT
	movq	(%r12), %rax
	movq	%r13, %rdx
	movq	%r14, %rdi
	movl	$1, %esi
	movl	8(%rax), %ecx
	movq	(%rax), %rbx
	movl	%ecx, -52(%rbp)
	call	ucol_getLocaleByType_67@PLT
	movl	-52(%rbp), %ecx
	movq	%r13, %r8
	movq	(%r12), %r15
	movq	%rbx, %rdx
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	ubrk_open_67@PLT
	movq	%r13, %rdx
	movq	%r14, %rdi
	movl	$1, %esi
	movq	%rax, 16(%r15)
	call	ucol_getAttribute_67@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	cmpl	$20, %eax
	sete	4208(%r12)
	call	ucol_getVariableTop_67@PLT
	movq	%r13, %rcx
	movq	%r14, %rdi
	movl	%eax, 4204(%r12)
	movq	(%r12), %rax
	movl	8(%rax), %edx
	movq	(%rax), %rsi
	call	ucol_openElements_67@PLT
	movq	8(%r12), %rsi
	movq	%r14, %rdi
	movq	%r13, %rcx
	movq	%rax, 4168(%r12)
	movl	16(%r12), %edx
	call	ucol_openElements_67@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, 4184(%r12)
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZL10initializeP13UStringSearchP10UErrorCode
	.p2align 4,,10
	.p2align 3
.L776:
	.cfi_restore_state
	movl	$1, (%rdx)
.L756:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3254:
	.size	usearch_setCollator_67, .-usearch_setCollator_67
	.p2align 4
	.globl	usearch_getCollator_67
	.type	usearch_getCollator_67, @function
usearch_getCollator_67:
.LFB3255:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L777
	movq	4152(%rdi), %rax
.L777:
	ret
	.cfi_endproc
.LFE3255:
	.size	usearch_getCollator_67, .-usearch_getCollator_67
	.p2align 4
	.globl	usearch_setPattern_67
	.type	usearch_setPattern_67, @function
usearch_setPattern_67:
.LFB3256:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L794
	ret
	.p2align 4,,10
	.p2align 3
.L794:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	subq	$24, %rsp
	testq	%rdi, %rdi
	je	.L786
	testq	%rsi, %rsi
	je	.L786
	cmpl	$-1, %edx
	je	.L795
.L785:
	testl	%edx, %edx
	jne	.L796
.L786:
	movl	$1, (%rcx)
	addq	$24, %rsp
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L796:
	.cfi_restore_state
	movq	%rsi, 8(%r13)
	movq	%r13, %rdi
	movq	%rcx, %rsi
	movl	%edx, 16(%r13)
	addq	$24, %rsp
	popq	%r13
	.cfi_remember_state
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZL10initializeP13UStringSearchP10UErrorCode
	.p2align 4,,10
	.p2align 3
.L795:
	.cfi_restore_state
	movq	%rsi, %rdi
	movq	%rcx, -32(%rbp)
	movq	%rsi, -24(%rbp)
	call	u_strlen_67@PLT
	movq	-32(%rbp), %rcx
	movq	-24(%rbp), %rsi
	movl	%eax, %edx
	jmp	.L785
	.cfi_endproc
.LFE3256:
	.size	usearch_setPattern_67, .-usearch_setPattern_67
	.p2align 4
	.globl	usearch_getPattern_67
	.type	usearch_getPattern_67, @function
usearch_getPattern_67:
.LFB3257:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L797
	movl	16(%rdi), %eax
	movl	%eax, (%rsi)
	movq	8(%rdi), %rax
.L797:
	ret
	.cfi_endproc
.LFE3257:
	.size	usearch_getPattern_67, .-usearch_getPattern_67
	.p2align 4
	.globl	usearch_reset_67
	.type	usearch_reset_67, @function
usearch_reset_67:
.LFB3264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L801
	movq	%rdi, %rbx
	movq	4152(%rdi), %rdi
	movl	$0, -44(%rbp)
	call	ucol_getStrength_67@PLT
	cmpl	$2, 4196(%rbx)
	jg	.L803
	cmpl	$2, %eax
	setle	%r12b
.L804:
	movq	4152(%rbx), %rdi
	call	ucol_getStrength_67@PLT
	movl	$-65536, %edx
	movl	%eax, 4196(%rbx)
	testl	%eax, %eax
	je	.L805
	cmpl	$1, %eax
	movl	$-1, %edx
	movl	$-256, %eax
	cmove	%eax, %edx
.L805:
	cmpl	%edx, 4200(%rbx)
	je	.L806
	movl	%edx, 4200(%rbx)
	xorl	%r12d, %r12d
.L806:
	movq	4152(%rbx), %rdi
	leaq	-44(%rbp), %r13
	movl	$1, %esi
	movq	%r13, %rdx
	call	ucol_getAttribute_67@PLT
	cmpl	$20, %eax
	sete	%al
	cmpb	%al, 4208(%rbx)
	je	.L807
	movb	%al, 4208(%rbx)
	movq	4152(%rbx), %rdi
	movq	%r13, %rsi
	call	ucol_getVariableTop_67@PLT
	cmpl	4204(%rbx), %eax
	je	.L809
.L808:
	movl	%eax, 4204(%rbx)
.L809:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZL10initializeP13UStringSearchP10UErrorCode
.L812:
	movq	(%rbx), %rax
	movq	4168(%rbx), %rdi
	movq	%r13, %rcx
	movl	8(%rax), %edx
	movq	(%rax), %rsi
	call	ucol_setText_67@PLT
	movq	(%rbx), %rax
	movl	$4294967295, %ecx
	movl	$257, %edx
	movl	$0, 12(%rax)
	movq	%rcx, 32(%rax)
	movw	%dx, 40(%rax)
.L801:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L824
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L803:
	.cfi_restore_state
	cmpl	$2, %eax
	setg	%r12b
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L807:
	movq	4152(%rbx), %rdi
	movq	%r13, %rsi
	call	ucol_getVariableTop_67@PLT
	cmpl	%eax, 4204(%rbx)
	jne	.L808
	testb	%r12b, %r12b
	jne	.L812
	jmp	.L809
.L824:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3264:
	.size	usearch_reset_67, .-usearch_reset_67
	.p2align 4
	.globl	usearch_search_67
	.type	usearch_search_67, @function
usearch_search_67:
.LFB3278:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L825
	movl	20(%rdi), %eax
	testl	%eax, %eax
	je	.L827
	testl	%esi, %esi
	js	.L827
	movq	(%rdi), %rax
	cmpl	%esi, 8(%rax)
	jl	.L827
	cmpq	$0, 24(%rdi)
	je	.L827
	jmp	usearch_search_67.part.0
	.p2align 4,,10
	.p2align 3
.L827:
	movl	$1, (%r8)
.L825:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3278:
	.size	usearch_search_67, .-usearch_search_67
	.p2align 4
	.globl	usearch_searchBackwards_67
	.type	usearch_searchBackwards_67, @function
usearch_searchBackwards_67:
.LFB3279:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L837
	movl	20(%rdi), %eax
	testl	%eax, %eax
	je	.L838
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	subq	$40, %rsp
	testl	%esi, %esi
	js	.L832
	movq	(%rdi), %rax
	cmpl	%esi, 8(%rax)
	jl	.L832
	cmpq	$0, 24(%rdi)
	je	.L832
	cmpq	$0, 1064(%rdi)
	je	.L841
.L834:
	addq	$40, %rsp
	movl	%r12d, %esi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	usearch_searchBackwards_67.part.0
	.p2align 4,,10
	.p2align 3
.L832:
	.cfi_restore_state
	movl	$1, (%r8)
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L838:
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, (%r8)
.L837:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L841:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	%r8, %rsi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%r8, -32(%rbp)
	movq	%rdi, -24(%rbp)
	call	_ZL25initializePatternPCETableP13UStringSearchP10UErrorCode
	movq	-48(%rbp), %rcx
	movq	-40(%rbp), %rdx
	movq	-32(%rbp), %r8
	movq	-24(%rbp), %rdi
	jmp	.L834
	.cfi_endproc
.LFE3279:
	.size	usearch_searchBackwards_67, .-usearch_searchBackwards_67
	.p2align 4
	.globl	usearch_handleNextExact_67
	.type	usearch_handleNextExact_67, @function
usearch_handleNextExact_67:
.LFB3280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movl	(%rsi), %r8d
	movq	4168(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L854
	movq	%rsi, %r12
	call	ucol_getOffset_67@PLT
	movl	(%r12), %esi
	movl	$-1, -36(%rbp)
	movl	$-1, -32(%rbp)
	movq	(%rbx), %rdx
	testl	%esi, %esi
	jg	.L849
	movl	20(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L847
	testl	%eax, %eax
	js	.L847
	cmpl	8(%rdx), %eax
	jg	.L847
	cmpq	$0, 24(%rbx)
	je	.L847
	leaq	-36(%rbp), %rdx
	leaq	-32(%rbp), %rcx
	movq	%r12, %r8
	movl	%eax, %esi
	movq	%rbx, %rdi
	call	usearch_search_67.part.0
	movq	(%rbx), %rdx
	testb	%al, %al
	je	.L849
	movl	-36(%rbp), %eax
	movl	-32(%rbp), %ecx
	movl	$1, %r12d
	subl	%eax, %ecx
	movl	%eax, 32(%rdx)
	movl	%ecx, 36(%rdx)
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L847:
	movl	$1, (%r12)
.L849:
	movzbl	40(%rdx), %r12d
	movl	$4294967295, %eax
	movq	4168(%rbx), %rdi
	movq	%rax, 32(%rdx)
	testb	%r12b, %r12b
	jne	.L855
.L851:
	leaq	-28(%rbp), %rdx
	xorl	%esi, %esi
	movl	$0, -28(%rbp)
	call	ucol_setOffset_67@PLT
.L842:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L856
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L854:
	.cfi_restore_state
	movq	(%rbx), %rax
	movl	$4294967295, %ecx
	movzbl	40(%rax), %r12d
	movq	%rcx, 32(%rax)
	testb	%r12b, %r12b
	je	.L851
	movl	8(%rax), %esi
	leaq	-28(%rbp), %rdx
	xorl	%r12d, %r12d
	movl	$0, -28(%rbp)
	call	ucol_setOffset_67@PLT
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L855:
	movl	8(%rdx), %esi
	leaq	-28(%rbp), %rdx
	xorl	%r12d, %r12d
	movl	$0, -28(%rbp)
	call	ucol_setOffset_67@PLT
	jmp	.L842
.L856:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3280:
	.size	usearch_handleNextExact_67, .-usearch_handleNextExact_67
	.p2align 4
	.globl	usearch_handleNextCanonical_67
	.type	usearch_handleNextCanonical_67, @function
usearch_handleNextCanonical_67:
.LFB3281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movl	(%rsi), %r8d
	movq	4168(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L869
	movq	%rsi, %r12
	call	ucol_getOffset_67@PLT
	movl	(%r12), %esi
	movl	$-1, -36(%rbp)
	movl	$-1, -32(%rbp)
	movq	(%rbx), %rdx
	testl	%esi, %esi
	jg	.L864
	movl	20(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L862
	testl	%eax, %eax
	js	.L862
	cmpl	8(%rdx), %eax
	jg	.L862
	cmpq	$0, 24(%rbx)
	je	.L862
	leaq	-36(%rbp), %rdx
	leaq	-32(%rbp), %rcx
	movq	%r12, %r8
	movl	%eax, %esi
	movq	%rbx, %rdi
	call	usearch_search_67.part.0
	movq	(%rbx), %rdx
	testb	%al, %al
	je	.L864
	movl	-36(%rbp), %eax
	movl	-32(%rbp), %ecx
	movl	$1, %r12d
	subl	%eax, %ecx
	movl	%eax, 32(%rdx)
	movl	%ecx, 36(%rdx)
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L862:
	movl	$1, (%r12)
.L864:
	movzbl	40(%rdx), %r12d
	movl	$4294967295, %eax
	movq	4168(%rbx), %rdi
	movq	%rax, 32(%rdx)
	testb	%r12b, %r12b
	jne	.L870
.L866:
	leaq	-28(%rbp), %rdx
	xorl	%esi, %esi
	movl	$0, -28(%rbp)
	call	ucol_setOffset_67@PLT
.L857:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L871
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L869:
	.cfi_restore_state
	movq	(%rbx), %rax
	movl	$4294967295, %ecx
	movzbl	40(%rax), %r12d
	movq	%rcx, 32(%rax)
	testb	%r12b, %r12b
	je	.L866
	movl	8(%rax), %esi
	leaq	-28(%rbp), %rdx
	xorl	%r12d, %r12d
	movl	$0, -28(%rbp)
	call	ucol_setOffset_67@PLT
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L870:
	movl	8(%rdx), %esi
	leaq	-28(%rbp), %rdx
	xorl	%r12d, %r12d
	movl	$0, -28(%rbp)
	call	ucol_setOffset_67@PLT
	jmp	.L857
.L871:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3281:
	.size	usearch_handleNextCanonical_67, .-usearch_handleNextCanonical_67
	.p2align 4
	.globl	usearch_next_67
	.type	usearch_next_67, @function
usearch_next_67:
.LFB3262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L911
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L911
	movq	4168(%rdi), %rdi
	movq	%rsi, %r13
	call	ucol_getOffset_67@PLT
	movq	(%r12), %rbx
	movl	%eax, %esi
	movl	8(%rbx), %r14d
	testl	%eax, %eax
	js	.L896
	cmpl	%r14d, %eax
	jg	.L896
.L876:
	cmpb	$0, 40(%rbx)
	movb	$0, 41(%rbx)
	jne	.L912
	movl	32(%rbx), %eax
	movb	$1, 40(%rbx)
	cmpl	$-1, %eax
	jne	.L872
.L883:
	movl	0(%r13), %edi
	testl	%edi, %edi
	jg	.L911
	movl	20(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L884
	movl	32(%rbx), %eax
	cmpl	$-1, %eax
	je	.L910
	movq	(%rbx), %rcx
	movslq	%eax, %rdx
	leal	1(%rax), %esi
	leaq	(%rdx,%rdx), %rdi
	movl	%esi, 32(%rbx)
	movzwl	(%rcx,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L886
	cmpl	%r14d, %esi
	je	.L886
	movzwl	2(%rcx,%rdi), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L913
	.p2align 4,,10
	.p2align 3
.L886:
	movl	$0, 36(%rbx)
	movq	4168(%r12), %rdi
	leaq	-44(%rbp), %rdx
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
	cmpl	%r14d, 32(%rbx)
	je	.L914
.L887:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L911
	movl	32(%rbx), %esi
	movq	4168(%r12), %rdi
	cmpl	$-1, %esi
	je	.L888
	movq	%r13, %rdx
	call	ucol_setOffset_67@PLT
.L894:
	movl	32(%rbx), %eax
.L872:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L915
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L912:
	.cfi_restore_state
	cmpl	%esi, %r14d
	je	.L879
	cmpb	$0, 12(%rbx)
	jne	.L883
	cmpl	$-1, 32(%rbx)
	je	.L883
	movl	36(%rbx), %eax
	addl	%esi, %eax
	cmpl	%r14d, %eax
	jle	.L883
.L879:
	movl	$4294967295, %eax
	movq	4168(%r12), %rdi
	leaq	-44(%rbp), %rdx
	movl	%r14d, %esi
	movq	%rax, 32(%rbx)
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
	.p2align 4,,10
	.p2align 3
.L911:
	movl	$-1, %eax
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L884:
	movl	36(%rbx), %eax
	testl	%eax, %eax
	jle	.L889
	cmpb	$0, 12(%rbx)
	movq	4168(%r12), %rdi
	jne	.L916
	addl	%eax, %esi
	movq	%r13, %rdx
	call	ucol_setOffset_67@PLT
.L891:
	cmpb	$0, 13(%rbx)
	movq	%r13, %rsi
	movq	%r12, %rdi
	je	.L892
.L917:
	call	usearch_handleNextCanonical_67
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L914:
	movl	0(%r13), %edx
	movl	$-1, 32(%rbx)
	testl	%edx, %edx
	jg	.L911
	movq	4168(%r12), %rdi
.L888:
	movl	8(%rbx), %esi
	movq	%r13, %rdx
	call	ucol_setOffset_67@PLT
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L916:
	addl	$1, %esi
	movq	%r13, %rdx
	call	ucol_setOffset_67@PLT
	cmpb	$0, 13(%rbx)
	movq	%r13, %rsi
	movq	%r12, %rdi
	jne	.L917
.L892:
	call	usearch_handleNextExact_67
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L889:
	subl	$1, %esi
	movl	%esi, 32(%rbx)
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L913:
	leal	2(%rax), %esi
	.p2align 4,,10
	.p2align 3
.L910:
	movl	%esi, 32(%rbx)
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L896:
	movl	$-1, %esi
	jmp	.L876
.L915:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3262:
	.size	usearch_next_67, .-usearch_next_67
	.p2align 4
	.globl	usearch_following_67
	.type	usearch_following_67, @function
usearch_following_67:
.LFB3259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L956
	movl	(%rdx), %r8d
	movq	%rdx, %r13
	testl	%r8d, %r8d
	jg	.L956
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movb	$1, 40(%rax)
	cmpl	8(%rax), %esi
	jg	.L943
	testl	%esi, %esi
	jns	.L922
.L943:
	movl	$4294967295, %ecx
	movl	$8, 0(%r13)
	movq	%rcx, 32(%rax)
	movb	$0, 41(%rax)
.L956:
	movl	$-1, %eax
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L922:
	movq	4168(%rdi), %rdi
	leaq	-60(%rbp), %r14
	movl	$0, -60(%rbp)
	movq	%r14, %rdx
	call	ucol_setOffset_67@PLT
	movl	0(%r13), %edx
	movq	(%rbx), %rax
	movl	$4294967295, %ecx
	movq	%rcx, 32(%rax)
	movb	$0, 41(%rax)
	testl	%edx, %edx
	jg	.L956
	movq	4168(%rbx), %rdi
	call	ucol_getOffset_67@PLT
	movq	(%rbx), %r12
	movl	%eax, %esi
	movl	8(%r12), %r15d
	testl	%eax, %eax
	js	.L944
	cmpl	%r15d, %eax
	jg	.L944
.L924:
	cmpb	$0, 40(%r12)
	movb	$0, 41(%r12)
	jne	.L957
	movl	32(%r12), %eax
	movb	$1, 40(%r12)
	cmpl	$-1, %eax
	jne	.L918
.L930:
	movl	0(%r13), %edi
	testl	%edi, %edi
	jg	.L956
	movl	20(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L931
	movl	32(%r12), %eax
	cmpl	$-1, %eax
	je	.L955
	movq	(%r12), %rcx
	movslq	%eax, %rdx
	leal	1(%rax), %esi
	leaq	(%rdx,%rdx), %rdi
	movl	%esi, 32(%r12)
	movzwl	(%rcx,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L933
	cmpl	%r15d, %esi
	jne	.L958
.L933:
	movl	$0, 36(%r12)
	movq	4168(%rbx), %rdi
	movq	%r14, %rdx
	movl	$0, -60(%rbp)
	call	ucol_setOffset_67@PLT
	cmpl	%r15d, 32(%r12)
	je	.L959
.L934:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L956
	movl	32(%r12), %esi
	movq	4168(%rbx), %rdi
	cmpl	$-1, %esi
	je	.L935
	movq	%r13, %rdx
	call	ucol_setOffset_67@PLT
.L941:
	movl	32(%r12), %eax
.L918:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L960
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L957:
	.cfi_restore_state
	cmpl	%esi, %r15d
	je	.L927
	cmpb	$0, 12(%r12)
	jne	.L930
	cmpl	$-1, 32(%r12)
	je	.L930
	movl	36(%r12), %eax
	addl	%esi, %eax
	cmpl	%r15d, %eax
	jle	.L930
.L927:
	movl	$4294967295, %eax
	movq	4168(%rbx), %rdi
	movq	%r14, %rdx
	movl	%r15d, %esi
	movq	%rax, 32(%r12)
	movl	$0, -60(%rbp)
	call	ucol_setOffset_67@PLT
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L931:
	movl	36(%r12), %eax
	testl	%eax, %eax
	jle	.L936
	cmpb	$0, 12(%r12)
	movq	4168(%rbx), %rdi
	je	.L937
	addl	$1, %esi
	movq	%r13, %rdx
	call	ucol_setOffset_67@PLT
.L938:
	cmpb	$0, 13(%r12)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	je	.L939
.L961:
	call	usearch_handleNextCanonical_67
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L959:
	movl	$-1, 32(%r12)
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L956
	movq	4168(%rbx), %rdi
.L935:
	movl	8(%r12), %esi
	movq	%r13, %rdx
	call	ucol_setOffset_67@PLT
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L936:
	subl	$1, %esi
	cmpb	$0, 13(%r12)
	movq	%rbx, %rdi
	movl	%esi, 32(%r12)
	movq	%r13, %rsi
	jne	.L961
.L939:
	call	usearch_handleNextExact_67
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L958:
	movzwl	2(%rcx,%rdi), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L933
	leal	2(%rax), %esi
	.p2align 4,,10
	.p2align 3
.L955:
	movl	%esi, 32(%r12)
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L937:
	addl	%eax, %esi
	movq	%r13, %rdx
	call	ucol_setOffset_67@PLT
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L944:
	movl	$-1, %esi
	jmp	.L924
.L960:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3259:
	.size	usearch_following_67, .-usearch_following_67
	.p2align 4
	.globl	usearch_first_67
	.type	usearch_first_67, @function
usearch_first_67:
.LFB3258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L998
	movl	(%rsi), %r9d
	movq	%rsi, %r13
	testl	%r9d, %r9d
	jg	.L998
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movl	8(%rax), %r8d
	movb	$1, 40(%rax)
	testl	%r8d, %r8d
	jns	.L966
	movl	$4294967295, %ecx
	movl	$8, (%rsi)
	movq	%rcx, 32(%rax)
	movb	$0, 41(%rax)
.L998:
	movl	$-1, %eax
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L966:
	movq	4168(%rdi), %rdi
	leaq	-60(%rbp), %r14
	xorl	%esi, %esi
	movl	$0, -60(%rbp)
	movq	%r14, %rdx
	call	ucol_setOffset_67@PLT
	movl	0(%r13), %edx
	movq	(%rbx), %rax
	movl	$4294967295, %ecx
	movq	%rcx, 32(%rax)
	movb	$0, 41(%rax)
	testl	%edx, %edx
	jg	.L998
	movq	4168(%rbx), %rdi
	call	ucol_getOffset_67@PLT
	movq	(%rbx), %r12
	movl	%eax, %esi
	movl	8(%r12), %r15d
	testl	%eax, %eax
	js	.L986
	cmpl	%r15d, %eax
	jg	.L986
.L967:
	cmpb	$0, 40(%r12)
	movb	$0, 41(%r12)
	jne	.L999
	movl	32(%r12), %eax
	movb	$1, 40(%r12)
	cmpl	$-1, %eax
	jne	.L962
.L973:
	movl	0(%r13), %edi
	testl	%edi, %edi
	jg	.L998
	movl	20(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L974
	movl	32(%r12), %eax
	cmpl	$-1, %eax
	je	.L997
	movq	(%r12), %rcx
	movslq	%eax, %rdx
	leal	1(%rax), %esi
	leaq	(%rdx,%rdx), %rdi
	movl	%esi, 32(%r12)
	movzwl	(%rcx,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L976
	cmpl	%r15d, %esi
	jne	.L1000
.L976:
	movl	$0, 36(%r12)
	movq	4168(%rbx), %rdi
	movq	%r14, %rdx
	movl	$0, -60(%rbp)
	call	ucol_setOffset_67@PLT
	cmpl	%r15d, 32(%r12)
	je	.L1001
.L977:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L998
	movl	32(%r12), %esi
	movq	4168(%rbx), %rdi
	cmpl	$-1, %esi
	je	.L978
	movq	%r13, %rdx
	call	ucol_setOffset_67@PLT
.L984:
	movl	32(%r12), %eax
.L962:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1002
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L999:
	.cfi_restore_state
	cmpl	%esi, %r15d
	je	.L970
	cmpb	$0, 12(%r12)
	jne	.L973
	cmpl	$-1, 32(%r12)
	je	.L973
	movl	36(%r12), %eax
	addl	%esi, %eax
	cmpl	%r15d, %eax
	jle	.L973
.L970:
	movl	$4294967295, %eax
	movq	4168(%rbx), %rdi
	movq	%r14, %rdx
	movl	%r15d, %esi
	movq	%rax, 32(%r12)
	movl	$0, -60(%rbp)
	call	ucol_setOffset_67@PLT
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L974:
	movl	36(%r12), %eax
	testl	%eax, %eax
	jle	.L979
	cmpb	$0, 12(%r12)
	movq	4168(%rbx), %rdi
	je	.L980
	addl	$1, %esi
	movq	%r13, %rdx
	call	ucol_setOffset_67@PLT
.L981:
	cmpb	$0, 13(%r12)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	je	.L982
.L1003:
	call	usearch_handleNextCanonical_67
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1001:
	movl	$-1, 32(%r12)
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L998
	movq	4168(%rbx), %rdi
.L978:
	movl	8(%r12), %esi
	movq	%r13, %rdx
	call	ucol_setOffset_67@PLT
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L979:
	subl	$1, %esi
	cmpb	$0, 13(%r12)
	movq	%rbx, %rdi
	movl	%esi, 32(%r12)
	movq	%r13, %rsi
	jne	.L1003
.L982:
	call	usearch_handleNextExact_67
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1000:
	movzwl	2(%rcx,%rdi), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L976
	leal	2(%rax), %esi
	.p2align 4,,10
	.p2align 3
.L997:
	movl	%esi, 32(%r12)
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L980:
	addl	%eax, %esi
	movq	%r13, %rdx
	call	ucol_setOffset_67@PLT
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L986:
	movl	$-1, %esi
	jmp	.L967
.L1002:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3258:
	.size	usearch_first_67, .-usearch_first_67
	.p2align 4
	.globl	usearch_handlePreviousExact_67
	.type	usearch_handlePreviousExact_67, @function
usearch_handlePreviousExact_67:
.LFB3282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rdi), %rdx
	movl	(%rsi), %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L1022
	cmpb	$0, 12(%rdx)
	movq	%rsi, %r12
	je	.L1008
	movl	32(%rdx), %eax
	cmpl	$-1, %eax
	je	.L1009
	movl	$-1, -52(%rbp)
	addl	36(%rdx), %eax
	movl	$-1, -48(%rbp)
	leal	-1(%rax), %r13d
.L1010:
	movl	20(%rbx), %eax
	testl	%eax, %eax
	je	.L1023
	testl	%r13d, %r13d
	js	.L1023
	cmpl	8(%rdx), %r13d
	jg	.L1023
	cmpq	$0, 24(%rbx)
	je	.L1023
	cmpq	$0, 1064(%rbx)
	je	.L1039
.L1025:
	leaq	-52(%rbp), %rdx
	leaq	-48(%rbp), %rcx
	movq	%r12, %r8
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	usearch_searchBackwards_67.part.0
	movq	(%rbx), %rdx
	testb	%al, %al
	je	.L1022
	movl	-52(%rbp), %eax
	movl	-48(%rbp), %ecx
	movl	$1, %r12d
	subl	%eax, %ecx
	movl	%eax, 32(%rdx)
	movl	%ecx, 36(%rdx)
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	4168(%rbx), %rdi
.L1033:
	call	ucol_getOffset_67@PLT
	movl	$-1, -52(%rbp)
	movq	(%rbx), %rdx
	movl	%eax, %r13d
	movl	(%r12), %eax
	movl	$-1, -48(%rbp)
	testl	%eax, %eax
	jle	.L1010
.L1022:
	movzbl	40(%rdx), %r12d
	movl	$4294967295, %eax
	movq	4168(%rbx), %rdi
	movq	%rax, 32(%rdx)
	testb	%r12b, %r12b
	je	.L1027
	movl	8(%rdx), %esi
	leaq	-44(%rbp), %rdx
	xorl	%r12d, %r12d
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
.L1004:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1040
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1023:
	.cfi_restore_state
	movl	$1, (%r12)
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1027:
	leaq	-44(%rbp), %rdx
	xorl	%esi, %esi
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	%rbx, %rdi
	call	_ZL25initializePatternPCETableP13UStringSearchP10UErrorCode
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L1011
	movq	4176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1041
	movq	4168(%rbx), %rsi
	call	_ZN6icu_6713UCollationPCE4initEP18UCollationElements@PLT
.L1014:
	movabsq	$9223372036854775807, %r14
	xorl	%r13d, %r13d
	cmpl	$1, 1056(%rbx)
	jg	.L1015
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1042:
	movl	1056(%rbx), %eax
	addl	$1, %r13d
	subl	$1, %eax
	cmpl	%r13d, %eax
	jle	.L1019
.L1015:
	movq	4176(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rcx
	call	_ZN6icu_6713UCollationPCE13nextProcessedEPiS1_P10UErrorCode@PLT
	cmpq	%r14, %rax
	jne	.L1042
.L1019:
	movl	(%r12), %edx
	movq	4168(%rbx), %rdi
	testl	%edx, %edx
	jle	.L1033
	movq	(%rbx), %rax
.L1037:
	movzbl	40(%rax), %r12d
	movl	$4294967295, %ecx
	movq	%rcx, 32(%rax)
	testb	%r12b, %r12b
	je	.L1027
	movl	8(%rax), %esi
	leaq	-44(%rbp), %rdx
	xorl	%r12d, %r12d
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1041:
	movl	$296, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1013
	movq	4168(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UCollationPCEC1EP18UCollationElements@PLT
	movq	%r13, 4176(%rbx)
	jmp	.L1014
.L1013:
	movq	$0, 4176(%rbx)
	movl	$7, (%r12)
	.p2align 4,,10
	.p2align 3
.L1011:
	movq	(%rbx), %rax
	movq	4168(%rbx), %rdi
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1039:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZL25initializePatternPCETableP13UStringSearchP10UErrorCode
	jmp	.L1025
.L1040:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3282:
	.size	usearch_handlePreviousExact_67, .-usearch_handlePreviousExact_67
	.p2align 4
	.globl	usearch_previous_67
	.type	usearch_previous_67, @function
usearch_previous_67:
.LFB3263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L1079
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L1079
	movq	(%rdi), %rbx
	movq	%rsi, %r13
	movq	4168(%rdi), %rdi
	cmpb	$0, 41(%rbx)
	je	.L1047
	xorl	%esi, %esi
	movl	8(%rbx), %r14d
	leaq	-44(%rbp), %rdx
	movw	%si, 40(%rbx)
	movl	%r14d, %esi
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
.L1048:
	cmpb	$1, 40(%rbx)
	movl	32(%rbx), %eax
	je	.L1080
	testl	%r14d, %r14d
	je	.L1063
	testl	%eax, %eax
	je	.L1063
.L1051:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L1079
	movl	20(%r12), %edx
	testl	%edx, %edx
	jne	.L1055
	cmpl	$-1, %eax
	movq	4168(%r12), %rdi
	cmovne	%eax, %r14d
	movl	%r14d, 32(%rbx)
	testl	%r14d, %r14d
	je	.L1081
	leal	-1(%r14), %esi
	movq	(%rbx), %rdx
	movslq	%esi, %rax
	movl	%esi, 32(%rbx)
	leaq	(%rax,%rax), %rcx
	movzwl	(%rdx,%rax,2), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L1060
	testl	%esi, %esi
	jle	.L1060
	movzwl	-2(%rdx,%rcx), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L1082
	.p2align 4,,10
	.p2align 3
.L1060:
	leaq	-44(%rbp), %rdx
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
	movl	$0, 36(%rbx)
.L1059:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1079
	movl	32(%rbx), %eax
.L1043:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1083
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1047:
	.cfi_restore_state
	call	ucol_getOffset_67@PLT
	movl	%eax, %r14d
	movq	(%r12), %rax
	cmpl	8(%rax), %r14d
	jg	.L1062
	testl	%r14d, %r14d
	jns	.L1048
.L1062:
	movl	$-1, %r14d
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1055:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	usearch_handlePreviousExact_67
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1080:
	movb	$0, 40(%rbx)
	cmpl	$-1, %eax
	jne	.L1043
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	(%r12), %rax
	movl	$4294967295, %ecx
	cmpb	$0, 40(%rax)
	movq	%rcx, 32(%rax)
	je	.L1058
	movl	8(%rax), %esi
	leaq	-44(%rbp), %rdx
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
	jmp	.L1059
.L1063:
	movq	(%r12), %rax
	movl	$4294967295, %ecx
	movq	4168(%r12), %rdi
	cmpb	$0, 40(%rax)
	movq	%rcx, 32(%rax)
	je	.L1053
	movl	8(%rax), %esi
	leaq	-44(%rbp), %rdx
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
	.p2align 4,,10
	.p2align 3
.L1079:
	movl	$-1, %eax
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1058:
	leaq	-44(%rbp), %rdx
	xorl	%esi, %esi
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1082:
	leal	-2(%r14), %esi
	movl	%esi, 32(%rbx)
	jmp	.L1060
.L1053:
	leaq	-44(%rbp), %rdx
	xorl	%esi, %esi
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
	movl	$-1, %eax
	jmp	.L1043
.L1083:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3263:
	.size	usearch_previous_67, .-usearch_previous_67
	.p2align 4
	.globl	usearch_preceding_67
	.type	usearch_preceding_67, @function
usearch_preceding_67:
.LFB3261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1123
	movl	(%rdx), %r8d
	movq	%rdx, %rbx
	testl	%r8d, %r8d
	jg	.L1123
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movb	$0, 40(%rax)
	cmpl	8(%rax), %esi
	jg	.L1103
	testl	%esi, %esi
	jns	.L1088
.L1103:
	movl	$4294967295, %ecx
	movl	$8, (%rbx)
	movq	%rcx, 32(%rax)
	movb	$0, 41(%rax)
.L1123:
	movl	$-1, %r8d
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1088:
	movq	4168(%rdi), %rdi
	leaq	-44(%rbp), %r13
	movl	$0, -44(%rbp)
	movq	%r13, %rdx
	call	ucol_setOffset_67@PLT
	movl	(%rbx), %eax
	movq	(%r12), %r14
	movl	$4294967295, %ecx
	movq	%rcx, 32(%r14)
	movb	$0, 41(%r14)
	testl	%eax, %eax
	jg	.L1123
	movq	4168(%r12), %rdi
	call	ucol_getOffset_67@PLT
	movq	(%r12), %rdx
	movl	8(%rdx), %esi
	testl	%eax, %eax
	js	.L1104
	cmpl	%esi, %eax
	jg	.L1104
.L1090:
	cmpb	$1, 40(%r14)
	movl	32(%r14), %r8d
	je	.L1124
	testl	%eax, %eax
	je	.L1105
	testl	%r8d, %r8d
	je	.L1105
.L1093:
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L1123
	movl	20(%r12), %ecx
	testl	%ecx, %ecx
	je	.L1125
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	usearch_handlePreviousExact_67
.L1100:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1123
	movl	32(%r14), %r8d
.L1084:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1126
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1125:
	.cfi_restore_state
	cmpl	$-1, %r8d
	movq	4168(%r12), %rdi
	cmovne	%r8d, %eax
	movl	%eax, 32(%r14)
	testl	%eax, %eax
	je	.L1127
	leal	-1(%rax), %esi
	movq	(%r14), %rcx
	movslq	%esi, %rdx
	movl	%esi, 32(%r14)
	leaq	(%rdx,%rdx), %r8
	movzwl	(%rcx,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L1101
	testl	%esi, %esi
	jg	.L1128
.L1101:
	movq	%r13, %rdx
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
	movl	$0, 36(%r14)
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1124:
	movb	$0, 40(%r14)
	cmpl	$-1, %r8d
	jne	.L1084
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1127:
	cmpb	$0, 40(%rdx)
	movl	$4294967295, %eax
	movq	%rax, 32(%rdx)
	movq	%r13, %rdx
	movl	$0, -44(%rbp)
	jne	.L1121
	xorl	%esi, %esi
.L1121:
	call	ucol_setOffset_67@PLT
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1128:
	movzwl	-2(%rcx,%r8), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1101
	leal	-2(%rax), %esi
	movl	%esi, 32(%r14)
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1104:
	movl	$-1, %eax
	jmp	.L1090
.L1105:
	cmpb	$0, 40(%rdx)
	movl	$4294967295, %eax
	movq	4168(%r12), %rdi
	movq	%rax, 32(%rdx)
	movq	%r13, %rdx
	movl	$0, -44(%rbp)
	jne	.L1122
	xorl	%esi, %esi
.L1122:
	call	ucol_setOffset_67@PLT
	jmp	.L1123
.L1126:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3261:
	.size	usearch_preceding_67, .-usearch_preceding_67
	.p2align 4
	.globl	usearch_last_67
	.type	usearch_last_67, @function
usearch_last_67:
.LFB3260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1166
	movl	(%rsi), %r8d
	movq	%rsi, %rbx
	testl	%r8d, %r8d
	jg	.L1166
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movl	8(%rax), %esi
	movb	$0, 40(%rax)
	testl	%esi, %esi
	jns	.L1133
	movl	$4294967295, %ecx
	movl	$8, (%rbx)
	movq	%rcx, 32(%rax)
	movb	$0, 41(%rax)
.L1166:
	movl	$-1, %r8d
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1133:
	movq	4168(%rdi), %rdi
	leaq	-44(%rbp), %r13
	movl	$0, -44(%rbp)
	movq	%r13, %rdx
	call	ucol_setOffset_67@PLT
	movl	(%rbx), %eax
	movq	(%r12), %r14
	movl	$4294967295, %ecx
	movq	%rcx, 32(%r14)
	movb	$0, 41(%r14)
	testl	%eax, %eax
	jg	.L1166
	movq	4168(%r12), %rdi
	call	ucol_getOffset_67@PLT
	movq	(%r12), %rdx
	movl	8(%rdx), %esi
	testl	%eax, %eax
	js	.L1147
	cmpl	%esi, %eax
	jg	.L1147
.L1134:
	cmpb	$1, 40(%r14)
	movl	32(%r14), %r8d
	je	.L1167
	testl	%eax, %eax
	je	.L1148
	testl	%r8d, %r8d
	je	.L1148
.L1137:
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L1166
	movl	20(%r12), %ecx
	testl	%ecx, %ecx
	je	.L1168
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	usearch_handlePreviousExact_67
.L1144:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1166
	movl	32(%r14), %r8d
.L1129:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1169
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1168:
	.cfi_restore_state
	cmpl	$-1, %r8d
	movq	4168(%r12), %rdi
	cmovne	%r8d, %eax
	movl	%eax, 32(%r14)
	testl	%eax, %eax
	je	.L1170
	leal	-1(%rax), %esi
	movq	(%r14), %rcx
	movslq	%esi, %rdx
	movl	%esi, 32(%r14)
	leaq	(%rdx,%rdx), %r8
	movzwl	(%rcx,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L1145
	testl	%esi, %esi
	jg	.L1171
.L1145:
	movq	%r13, %rdx
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
	movl	$0, 36(%r14)
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1167:
	movb	$0, 40(%r14)
	cmpl	$-1, %r8d
	jne	.L1129
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1170:
	cmpb	$0, 40(%rdx)
	movl	$4294967295, %eax
	movq	%rax, 32(%rdx)
	movq	%r13, %rdx
	movl	$0, -44(%rbp)
	jne	.L1164
	xorl	%esi, %esi
.L1164:
	call	ucol_setOffset_67@PLT
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1171:
	movzwl	-2(%rcx,%r8), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1145
	leal	-2(%rax), %esi
	movl	%esi, 32(%r14)
	jmp	.L1145
	.p2align 4,,10
	.p2align 3
.L1147:
	movl	$-1, %eax
	jmp	.L1134
.L1148:
	cmpb	$0, 40(%rdx)
	movl	$4294967295, %eax
	movq	4168(%r12), %rdi
	movq	%rax, 32(%rdx)
	movq	%r13, %rdx
	movl	$0, -44(%rbp)
	jne	.L1165
	xorl	%esi, %esi
.L1165:
	call	ucol_setOffset_67@PLT
	jmp	.L1166
.L1169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3260:
	.size	usearch_last_67, .-usearch_last_67
	.p2align 4
	.globl	usearch_handlePreviousCanonical_67
	.type	usearch_handlePreviousCanonical_67, @function
usearch_handlePreviousCanonical_67:
.LFB4471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rdi), %rdx
	movl	(%rsi), %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L1190
	cmpb	$0, 12(%rdx)
	movq	%rsi, %r12
	je	.L1176
	movl	32(%rdx), %eax
	cmpl	$-1, %eax
	je	.L1177
	movl	$-1, -52(%rbp)
	addl	36(%rdx), %eax
	movl	$-1, -48(%rbp)
	leal	-1(%rax), %r13d
.L1178:
	movl	20(%rbx), %eax
	testl	%eax, %eax
	je	.L1191
	testl	%r13d, %r13d
	js	.L1191
	cmpl	%r13d, 8(%rdx)
	jl	.L1191
	cmpq	$0, 24(%rbx)
	je	.L1191
	cmpq	$0, 1064(%rbx)
	je	.L1207
.L1193:
	leaq	-52(%rbp), %rdx
	leaq	-48(%rbp), %rcx
	movq	%r12, %r8
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	usearch_searchBackwards_67.part.0
	movq	(%rbx), %rdx
	testb	%al, %al
	je	.L1190
	movl	-52(%rbp), %eax
	movl	-48(%rbp), %ecx
	movl	$1, %r12d
	subl	%eax, %ecx
	movl	%eax, 32(%rdx)
	movl	%ecx, 36(%rdx)
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1176:
	movq	4168(%rbx), %rdi
.L1201:
	call	ucol_getOffset_67@PLT
	movl	$-1, -52(%rbp)
	movq	(%rbx), %rdx
	movl	%eax, %r13d
	movl	(%r12), %eax
	movl	$-1, -48(%rbp)
	testl	%eax, %eax
	jle	.L1178
.L1190:
	movzbl	40(%rdx), %r12d
	movl	$4294967295, %eax
	movq	4168(%rbx), %rdi
	movq	%rax, 32(%rdx)
	testb	%r12b, %r12b
	je	.L1195
	movl	8(%rdx), %esi
	leaq	-44(%rbp), %rdx
	xorl	%r12d, %r12d
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
.L1172:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1208
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1191:
	.cfi_restore_state
	movl	$1, (%r12)
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1195:
	leaq	-44(%rbp), %rdx
	xorl	%esi, %esi
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1177:
	movq	%rbx, %rdi
	call	_ZL25initializePatternPCETableP13UStringSearchP10UErrorCode
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L1179
	movq	4176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1209
	movq	4168(%rbx), %rsi
	call	_ZN6icu_6713UCollationPCE4initEP18UCollationElements@PLT
.L1182:
	movabsq	$9223372036854775807, %r14
	xorl	%r13d, %r13d
	cmpl	$1, 1056(%rbx)
	jg	.L1183
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1210:
	movl	1056(%rbx), %eax
	addl	$1, %r13d
	subl	$1, %eax
	cmpl	%r13d, %eax
	jle	.L1187
.L1183:
	movq	4176(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rcx
	call	_ZN6icu_6713UCollationPCE13nextProcessedEPiS1_P10UErrorCode@PLT
	cmpq	%r14, %rax
	jne	.L1210
.L1187:
	movl	(%r12), %edx
	movq	4168(%rbx), %rdi
	testl	%edx, %edx
	jle	.L1201
	movq	(%rbx), %rax
.L1205:
	movzbl	40(%rax), %r12d
	movl	$4294967295, %ecx
	movq	%rcx, 32(%rax)
	testb	%r12b, %r12b
	je	.L1195
	movl	8(%rax), %esi
	leaq	-44(%rbp), %rdx
	xorl	%r12d, %r12d
	movl	$0, -44(%rbp)
	call	ucol_setOffset_67@PLT
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1209:
	movl	$296, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1181
	movq	4168(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UCollationPCEC1EP18UCollationElements@PLT
	movq	%r13, 4176(%rbx)
	jmp	.L1182
.L1181:
	movq	$0, 4176(%rbx)
	movl	$7, (%r12)
	.p2align 4,,10
	.p2align 3
.L1179:
	movq	(%rbx), %rax
	movq	4168(%rbx), %rdi
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1207:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZL25initializePatternPCETableP13UStringSearchP10UErrorCode
	jmp	.L1193
.L1208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4471:
	.size	usearch_handlePreviousCanonical_67, .-usearch_handlePreviousCanonical_67
	.local	_ZL9g_nfcImpl
	.comm	_ZL9g_nfcImpl,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
