	.file	"fpositer.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721FieldPositionIteratorD2Ev
	.type	_ZN6icu_6721FieldPositionIteratorD2Ev, @function
_ZN6icu_6721FieldPositionIteratorD2Ev:
.LFB2114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721FieldPositionIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2
	movq	(%rdi), %rax
	call	*8(%rax)
.L2:
	movq	$0, 8(%r12)
	movq	%r12, %rdi
	movl	$-1, 16(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2114:
	.size	_ZN6icu_6721FieldPositionIteratorD2Ev, .-_ZN6icu_6721FieldPositionIteratorD2Ev
	.globl	_ZN6icu_6721FieldPositionIteratorD1Ev
	.set	_ZN6icu_6721FieldPositionIteratorD1Ev,_ZN6icu_6721FieldPositionIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721FieldPositionIteratorD0Ev
	.type	_ZN6icu_6721FieldPositionIteratorD0Ev, @function
_ZN6icu_6721FieldPositionIteratorD0Ev:
.LFB2116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721FieldPositionIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L9
	movq	(%rdi), %rax
	call	*8(%rax)
.L9:
	movq	$0, 8(%r12)
	movq	%r12, %rdi
	movl	$-1, 16(%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2116:
	.size	_ZN6icu_6721FieldPositionIteratorD0Ev, .-_ZN6icu_6721FieldPositionIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721FieldPositionIteratorC2Ev
	.type	_ZN6icu_6721FieldPositionIteratorC2Ev, @function
_ZN6icu_6721FieldPositionIteratorC2Ev:
.LFB2118:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6721FieldPositionIteratorE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$-1, 16(%rdi)
	ret
	.cfi_endproc
.LFE2118:
	.size	_ZN6icu_6721FieldPositionIteratorC2Ev, .-_ZN6icu_6721FieldPositionIteratorC2Ev
	.globl	_ZN6icu_6721FieldPositionIteratorC1Ev
	.set	_ZN6icu_6721FieldPositionIteratorC1Ev,_ZN6icu_6721FieldPositionIteratorC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721FieldPositionIteratorC2ERKS0_
	.type	_ZN6icu_6721FieldPositionIteratorC2ERKS0_, @function
_ZN6icu_6721FieldPositionIteratorC2ERKS0_:
.LFB2121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6721FieldPositionIteratorE(%rip), %rax
	cmpq	$0, 8(%rsi)
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	16(%rsi), %eax
	movl	%eax, 16(%rdi)
	je	.L15
	movq	%rdi, %rbx
	movl	$32, %edi
	leaq	-44(%rbp), %r14
	movq	%rsi, %r12
	movl	$0, -44(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L17
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
.L17:
	movq	%r13, 8(%rbx)
	movq	8(%r12), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector326assignERKS0_R10UErrorCode@PLT
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jne	.L28
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L19
	movq	(%rdi), %rax
	call	*8(%rax)
.L19:
	movq	$0, 8(%rbx)
	movl	$-1, 16(%rbx)
	jmp	.L15
.L29:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2121:
	.size	_ZN6icu_6721FieldPositionIteratorC2ERKS0_, .-_ZN6icu_6721FieldPositionIteratorC2ERKS0_
	.globl	_ZN6icu_6721FieldPositionIteratorC1ERKS0_
	.set	_ZN6icu_6721FieldPositionIteratorC1ERKS0_,_ZN6icu_6721FieldPositionIteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721FieldPositionIteratoreqERKS0_
	.type	_ZNK6icu_6721FieldPositionIteratoreqERKS0_, @function
_ZNK6icu_6721FieldPositionIteratoreqERKS0_:
.LFB2123:
	.cfi_startproc
	endbr64
	cmpq	%rdi, %rsi
	je	.L33
	movl	16(%rsi), %eax
	cmpl	%eax, 16(%rdi)
	jne	.L35
	movq	8(%rdi), %rdi
	movq	8(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L36
	testq	%rsi, %rsi
	je	.L35
	jmp	_ZN6icu_679UVector32eqERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	testq	%rsi, %rsi
	sete	%al
	ret
	.cfi_endproc
.LFE2123:
	.size	_ZNK6icu_6721FieldPositionIteratoreqERKS0_, .-_ZNK6icu_6721FieldPositionIteratoreqERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721FieldPositionIterator7setDataEPNS_9UVector32ER10UErrorCode
	.type	_ZN6icu_6721FieldPositionIterator7setDataEPNS_9UVector32ER10UErrorCode, @function
_ZN6icu_6721FieldPositionIterator7setDataEPNS_9UVector32ER10UErrorCode:
.LFB2124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L64
	testq	%rsi, %rsi
	je	.L37
.L44:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	%rdi, %r13
	testq	%rsi, %rsi
	je	.L54
	movq	%rdx, %rbx
	movl	8(%rsi), %edx
	testl	%edx, %edx
	je	.L65
	testb	$3, %dl
	jne	.L47
	cmpl	$2, %edx
	jle	.L55
	leal	-6(%rdx), %edi
	leal	-2(%rdx), %eax
	subl	$3, %edx
	andl	$-4, %edx
	subl	%edx, %edi
	movl	$12, %edx
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L66:
	movq	24(%r12), %rsi
	movl	-4(%rsi,%rdx), %ecx
	cmpl	$1, %eax
	jne	.L53
	xorl	%esi, %esi
.L46:
	cmpl	%ecx, %esi
	jle	.L47
	subl	$4, %eax
	addq	$16, %rdx
	cmpl	%eax, %edi
	je	.L55
.L48:
	testl	%eax, %eax
	jg	.L66
	leal	-1(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L47
	movq	24(%r12), %rsi
	xorl	%ecx, %ecx
.L53:
	movl	(%rsi,%rdx), %esi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L65:
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	call	*8(%rax)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L37
	movq	8(%r13), %rdi
	movl	$-1, %ebx
	xorl	%r12d, %r12d
	testq	%rdi, %rdi
	je	.L52
	.p2align 4,,10
	.p2align 3
.L51:
	movq	(%rdi), %rax
	call	*8(%rax)
.L50:
	movq	%r12, 8(%r13)
.L52:
	movl	%ebx, 16(%r13)
.L37:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movl	$-1, %ebx
.L39:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L51
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L47:
	movl	$1, (%rbx)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L55:
	xorl	%ebx, %ebx
	jmp	.L39
	.cfi_endproc
.LFE2124:
	.size	_ZN6icu_6721FieldPositionIterator7setDataEPNS_9UVector32ER10UErrorCode, .-_ZN6icu_6721FieldPositionIterator7setDataEPNS_9UVector32ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721FieldPositionIterator4nextERNS_13FieldPositionE
	.type	_ZN6icu_6721FieldPositionIterator4nextERNS_13FieldPositionE, @function
_ZN6icu_6721FieldPositionIterator4nextERNS_13FieldPositionE:
.LFB2125:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	xorl	%r8d, %r8d
	cmpl	$-1, %eax
	je	.L67
	leal	2(%rax), %ecx
	movq	8(%rdi), %rdx
	xorl	%r8d, %r8d
	movl	%ecx, 16(%rdi)
	addl	$1, %eax
	js	.L69
	movl	8(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L69
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L69
	movq	24(%rdx), %rcx
	cltq
	movl	(%rcx,%rax,4), %r8d
.L69:
	movl	%r8d, 8(%rsi)
	movslq	16(%rdi), %rax
	xorl	%r8d, %r8d
	leal	1(%rax), %ecx
	movl	%ecx, 16(%rdi)
	testl	%eax, %eax
	js	.L70
	movl	8(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L70
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L70
	movq	24(%rdx), %rcx
	movl	(%rcx,%rax,4), %r8d
.L70:
	movl	%r8d, 12(%rsi)
	movslq	16(%rdi), %rax
	xorl	%r8d, %r8d
	leal	1(%rax), %ecx
	movl	%ecx, 16(%rdi)
	testl	%eax, %eax
	js	.L71
	movl	8(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L71
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L71
	movq	24(%rdx), %rcx
	movl	(%rcx,%rax,4), %r8d
.L71:
	movl	%r8d, 16(%rsi)
	movl	$1, %r8d
	movl	8(%rdx), %eax
	cmpl	%eax, 16(%rdi)
	jne	.L67
	movl	$-1, 16(%rdi)
.L67:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2125:
	.size	_ZN6icu_6721FieldPositionIterator4nextERNS_13FieldPositionE, .-_ZN6icu_6721FieldPositionIterator4nextERNS_13FieldPositionE
	.weak	_ZTSN6icu_6721FieldPositionIteratorE
	.section	.rodata._ZTSN6icu_6721FieldPositionIteratorE,"aG",@progbits,_ZTSN6icu_6721FieldPositionIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6721FieldPositionIteratorE, @object
	.size	_ZTSN6icu_6721FieldPositionIteratorE, 33
_ZTSN6icu_6721FieldPositionIteratorE:
	.string	"N6icu_6721FieldPositionIteratorE"
	.weak	_ZTIN6icu_6721FieldPositionIteratorE
	.section	.data.rel.ro._ZTIN6icu_6721FieldPositionIteratorE,"awG",@progbits,_ZTIN6icu_6721FieldPositionIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6721FieldPositionIteratorE, @object
	.size	_ZTIN6icu_6721FieldPositionIteratorE, 24
_ZTIN6icu_6721FieldPositionIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6721FieldPositionIteratorE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6721FieldPositionIteratorE
	.section	.data.rel.ro._ZTVN6icu_6721FieldPositionIteratorE,"awG",@progbits,_ZTVN6icu_6721FieldPositionIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6721FieldPositionIteratorE, @object
	.size	_ZTVN6icu_6721FieldPositionIteratorE, 40
_ZTVN6icu_6721FieldPositionIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6721FieldPositionIteratorE
	.quad	_ZN6icu_6721FieldPositionIteratorD1Ev
	.quad	_ZN6icu_6721FieldPositionIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
