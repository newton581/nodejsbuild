	.file	"utrans.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ReplaceableGlue17getDynamicClassIDEv
	.type	_ZNK6icu_6715ReplaceableGlue17getDynamicClassIDEv, @function
_ZNK6icu_6715ReplaceableGlue17getDynamicClassIDEv:
.LFB2390:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6715ReplaceableGlue16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2390:
	.size	_ZNK6icu_6715ReplaceableGlue17getDynamicClassIDEv, .-_ZNK6icu_6715ReplaceableGlue17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ReplaceableGlue9getLengthEv
	.type	_ZNK6icu_6715ReplaceableGlue9getLengthEv, @function
_ZNK6icu_6715ReplaceableGlue9getLengthEv:
.LFB2398:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	8(%rdi), %r8
	movq	(%rax), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE2398:
	.size	_ZNK6icu_6715ReplaceableGlue9getLengthEv, .-_ZNK6icu_6715ReplaceableGlue9getLengthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ReplaceableGlue9getCharAtEi
	.type	_ZNK6icu_6715ReplaceableGlue9getCharAtEi, @function
_ZNK6icu_6715ReplaceableGlue9getCharAtEi:
.LFB2399:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	8(%rdi), %r8
	movq	8(%rax), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE2399:
	.size	_ZNK6icu_6715ReplaceableGlue9getCharAtEi, .-_ZNK6icu_6715ReplaceableGlue9getCharAtEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ReplaceableGlue11getChar32AtEi
	.type	_ZNK6icu_6715ReplaceableGlue11getChar32AtEi, @function
_ZNK6icu_6715ReplaceableGlue11getChar32AtEi:
.LFB2400:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	8(%rdi), %r8
	movq	16(%rax), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE2400:
	.size	_ZNK6icu_6715ReplaceableGlue11getChar32AtEi, .-_ZNK6icu_6715ReplaceableGlue11getChar32AtEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ReplaceableGlue20handleReplaceBetweenEiiRKNS_13UnicodeStringE
	.type	_ZN6icu_6715ReplaceableGlue20handleReplaceBetweenEiiRKNS_13UnicodeStringE, @function
_ZN6icu_6715ReplaceableGlue20handleReplaceBetweenEiiRKNS_13UnicodeStringE:
.LFB2401:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	24(%rax), %r9
	movzwl	8(%rcx), %eax
	testw	%ax, %ax
	js	.L7
	movswl	%ax, %r8d
	sarl	$5, %r8d
	testb	$17, %al
	jne	.L11
.L13:
	testb	$2, %al
	jne	.L12
	movq	24(%rcx), %rcx
	movq	8(%rdi), %rdi
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L12:
	movq	8(%rdi), %rdi
	addq	$10, %rcx
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L7:
	movl	12(%rcx), %r8d
	testb	$17, %al
	je	.L13
.L11:
	movq	8(%rdi), %rdi
	xorl	%ecx, %ecx
	jmp	*%r9
	.cfi_endproc
.LFE2401:
	.size	_ZN6icu_6715ReplaceableGlue20handleReplaceBetweenEiiRKNS_13UnicodeStringE, .-_ZN6icu_6715ReplaceableGlue20handleReplaceBetweenEiiRKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ReplaceableGlue4copyEiii
	.type	_ZN6icu_6715ReplaceableGlue4copyEiii, @function
_ZN6icu_6715ReplaceableGlue4copyEiii:
.LFB2403:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	8(%rdi), %r8
	movq	40(%rax), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE2403:
	.size	_ZN6icu_6715ReplaceableGlue4copyEiii, .-_ZN6icu_6715ReplaceableGlue4copyEiii
	.p2align 4
	.type	utrans_enum_count, @function
utrans_enum_count:
.LFB2417:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L15
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L15
	movl	60(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	ret
	.cfi_endproc
.LFE2417:
	.size	utrans_enum_count, .-utrans_enum_count
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ReplaceableGlueD2Ev
	.type	_ZN6icu_6715ReplaceableGlueD2Ev, @function
_ZN6icu_6715ReplaceableGlueD2Ev:
.LFB2395:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6715ReplaceableGlueE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6711ReplaceableD2Ev@PLT
	.cfi_endproc
.LFE2395:
	.size	_ZN6icu_6715ReplaceableGlueD2Ev, .-_ZN6icu_6715ReplaceableGlueD2Ev
	.globl	_ZN6icu_6715ReplaceableGlueD1Ev
	.set	_ZN6icu_6715ReplaceableGlueD1Ev,_ZN6icu_6715ReplaceableGlueD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ReplaceableGlueD0Ev
	.type	_ZN6icu_6715ReplaceableGlueD0Ev, @function
_ZN6icu_6715ReplaceableGlueD0Ev:
.LFB2397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715ReplaceableGlueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6711ReplaceableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2397:
	.size	_ZN6icu_6715ReplaceableGlueD0Ev, .-_ZN6icu_6715ReplaceableGlueD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ReplaceableGlue14extractBetweenEiiRNS_13UnicodeStringE
	.type	_ZNK6icu_6715ReplaceableGlue14extractBetweenEiiRNS_13UnicodeStringE, @function
_ZNK6icu_6715ReplaceableGlue14extractBetweenEiiRNS_13UnicodeStringE:
.LFB2402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	subl	%esi, %r15d
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movl	%r15d, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	%rcx, %rdi
	movq	32(%rax), %r8
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	8(%rbx), %rdi
	movl	%r13d, %esi
	movq	-56(%rbp), %r8
	movl	%r14d, %edx
	movq	%rax, %rcx
	call	*%r8
	addq	$24, %rsp
	movl	%r15d, %esi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	.cfi_endproc
.LFE2402:
	.size	_ZNK6icu_6715ReplaceableGlue14extractBetweenEiiRNS_13UnicodeStringE, .-_ZNK6icu_6715ReplaceableGlue14extractBetweenEiiRNS_13UnicodeStringE
	.p2align 4
	.type	utrans_enum_unext, @function
utrans_enum_unext:
.LFB2418:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L40
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L40
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	56(%rdi), %r13d
	cmpl	%r13d, 60(%rdi)
	jg	.L44
	testq	%rsi, %rsi
	je	.L43
	movl	$0, (%rsi)
.L43:
	xorl	%eax, %eax
.L24:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movl	%r13d, %edi
	addl	$1, %r13d
	call	_ZN6icu_6714Transliterator14getAvailableIDEi@PLT
	movl	%r13d, 56(%rbx)
	testq	%r12, %r12
	je	.L29
	movswl	8(%rax), %edx
	testw	%dx, %dx
	js	.L30
	sarl	$5, %edx
.L31:
	movl	%edx, (%r12)
.L29:
	movzwl	8(%rax), %edx
	testb	$17, %dl
	jne	.L43
	andl	$2, %edx
	je	.L32
	addq	$10, %rax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	24(%rax), %rax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L30:
	movl	12(%rax), %edx
	jmp	.L31
	.cfi_endproc
.LFE2418:
	.size	utrans_enum_unext, .-utrans_enum_unext
	.p2align 4
	.type	utrans_enum_close, @function
utrans_enum_close:
.LFB2420:
	.cfi_startproc
	endbr64
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2420:
	.size	utrans_enum_close, .-utrans_enum_close
	.p2align 4
	.type	utrans_enum_reset, @function
utrans_enum_reset:
.LFB2419:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L52
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L55
.L52:
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	$0, 56(%rdi)
	call	_ZN6icu_6714Transliterator17countAvailableIDsEv@PLT
	movl	%eax, 60(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2419:
	.size	utrans_enum_reset, .-utrans_enum_reset
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ReplaceableGlue16getStaticClassIDEv
	.type	_ZN6icu_6715ReplaceableGlue16getStaticClassIDEv, @function
_ZN6icu_6715ReplaceableGlue16getStaticClassIDEv:
.LFB2389:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6715ReplaceableGlue16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2389:
	.size	_ZN6icu_6715ReplaceableGlue16getStaticClassIDEv, .-_ZN6icu_6715ReplaceableGlue16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ReplaceableGlueC2EPPvPK21UReplaceableCallbacks
	.type	_ZN6icu_6715ReplaceableGlueC2EPPvPK21UReplaceableCallbacks, @function
_ZN6icu_6715ReplaceableGlueC2EPPvPK21UReplaceableCallbacks:
.LFB2392:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6715ReplaceableGlueE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	ret
	.cfi_endproc
.LFE2392:
	.size	_ZN6icu_6715ReplaceableGlueC2EPPvPK21UReplaceableCallbacks, .-_ZN6icu_6715ReplaceableGlueC2EPPvPK21UReplaceableCallbacks
	.globl	_ZN6icu_6715ReplaceableGlueC1EPPvPK21UReplaceableCallbacks
	.set	_ZN6icu_6715ReplaceableGlueC1EPPvPK21UReplaceableCallbacks,_ZN6icu_6715ReplaceableGlueC2EPPvPK21UReplaceableCallbacks
	.p2align 4
	.globl	utrans_openU_67
	.type	utrans_openU_67, @function
utrans_openU_67:
.LFB2404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -276(%rbp)
	movq	16(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L58
	movl	(%r15), %r10d
	testl	%r10d, %r10d
	jg	.L58
	testq	%rdi, %rdi
	je	.L71
	movl	%esi, %r10d
	testq	%r9, %r9
	movq	%r9, %r12
	movq	%rdi, -264(%rbp)
	leaq	-128(%rbp), %rax
	leaq	-264(%rbp), %r13
	movq	%rcx, %rbx
	movl	%r10d, %ecx
	leaq	-256(%rbp), %r14
	cmove	%rax, %r12
	movq	%r13, %rdx
	shrl	$31, %esi
	movq	%r14, %rdi
	movl	%r8d, -280(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	testq	%rbx, %rbx
	movl	-280(%rbp), %r8d
	je	.L72
	movq	%rbx, -264(%rbp)
	movl	%r8d, %esi
	movl	%r8d, %ecx
	movq	%r13, %rdx
	leaq	-192(%rbp), %rbx
	shrl	$31, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	-276(%rbp), %edx
	movq	%r14, %rdi
	movq	%r15, %r8
	movq	%r12, %rcx
	movq	%rbx, %rsi
	call	_ZN6icu_6714Transliterator15createFromRulesERKNS_13UnicodeStringES3_15UTransDirectionR11UParseErrorR10UErrorCode@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	movl	(%r15), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%rax, %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L63:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L58:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$248, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movl	-276(%rbp), %esi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r14, %rdi
	call	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	cmovle	%rax, %rbx
	movq	%rbx, %r13
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$1, (%r15)
	jmp	.L58
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2404:
	.size	utrans_openU_67, .-utrans_openU_67
	.p2align 4
	.globl	utrans_open_67
	.type	utrans_open_67, @function
utrans_open_67:
.LFB2405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$-1, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$328, %rsp
	movl	%esi, -344(%rbp)
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movl	%ecx, -348(%rbp)
	xorl	%ecx, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movzwl	-312(%rbp), %eax
	testw	%ax, %ax
	js	.L75
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testb	$17, %al
	jne	.L95
.L77:
	testb	$2, %al
	leaq	-310(%rbp), %rdx
	cmove	-296(%rbp), %rdx
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.L78
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L78
	testq	%rdx, %rdx
	je	.L79
	testq	%r12, %r12
	leaq	-256(%rbp), %r9
	leaq	-128(%rbp), %rax
	movl	%ecx, %esi
	leaq	-328(%rbp), %r14
	movq	%rdx, -328(%rbp)
	cmove	%rax, %r12
	movq	%r9, %rdi
	shrl	$31, %esi
	movq	%r14, %rdx
	movq	%r9, -360(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	testq	%r13, %r13
	movq	-360(%rbp), %r9
	je	.L96
	movl	-348(%rbp), %ecx
	movq	%r13, -328(%rbp)
	leaq	-192(%rbp), %r13
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%r9, -360(%rbp)
	movl	%ecx, %esi
	shrl	$31, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-360(%rbp), %r9
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rsi
	movl	-344(%rbp), %edx
	movq	%r9, %rdi
	movq	%r9, -344(%rbp)
	call	_ZN6icu_6714Transliterator15createFromRulesERKNS_13UnicodeStringES3_15UTransDirectionR11UParseErrorR10UErrorCode@PLT
	movl	$0, %edx
	movq	%r13, %rdi
	movq	%rax, %r14
	movl	(%rbx), %eax
	testl	%eax, %eax
	cmovg	%rdx, %r14
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-344(%rbp), %r9
.L84:
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L78:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$328, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movl	-308(%rbp), %ecx
	testb	$17, %al
	je	.L77
.L95:
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.L78
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L78
.L79:
	movl	$1, (%rbx)
	xorl	%r14d, %r14d
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L96:
	movl	-344(%rbp), %esi
	movq	%r12, %rdx
	movq	%r9, %rdi
	movq	%rbx, %rcx
	movq	%r9, -344(%rbp)
	movq	%r13, %r14
	call	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode@PLT
	movl	(%rbx), %edx
	movq	-344(%rbp), %r9
	testl	%edx, %edx
	cmovle	%rax, %r14
	jmp	.L84
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2405:
	.size	utrans_open_67, .-utrans_open_67
	.p2align 4
	.globl	utrans_openInverse_67
	.type	utrans_openInverse_67, @function
utrans_openInverse_67:
.LFB2406:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L98
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L98
	jmp	_ZNK6icu_6714Transliterator13createInverseER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2406:
	.size	utrans_openInverse_67, .-utrans_openInverse_67
	.p2align 4
	.globl	utrans_clone_67
	.type	utrans_clone_67, @function
utrans_clone_67:
.LFB2407:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L106
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L103
	testq	%rdi, %rdi
	je	.L112
	movq	(%rdi), %rax
	call	*24(%rax)
	testq	%rax, %rax
	je	.L113
.L103:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movl	$7, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$1, (%rsi)
	jmp	.L103
	.cfi_endproc
.LFE2407:
	.size	utrans_clone_67, .-utrans_clone_67
	.p2align 4
	.globl	utrans_close_67
	.type	utrans_close_67, @function
utrans_close_67:
.LFB2408:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L114
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L114:
	ret
	.cfi_endproc
.LFE2408:
	.size	utrans_close_67, .-utrans_close_67
	.p2align 4
	.globl	utrans_getUnicodeID_67
	.type	utrans_getUnicodeID_67, @function
utrans_getUnicodeID_67:
.LFB2409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*104(%rax)
	testq	%rbx, %rbx
	je	.L117
	movswl	8(%rax), %edx
	testw	%dx, %dx
	js	.L118
	sarl	$5, %edx
	movl	%edx, (%rbx)
.L117:
	movzwl	8(%rax), %edx
	testb	$17, %dl
	jne	.L122
	andl	$2, %edx
	jne	.L127
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	addq	$8, %rsp
	addq	$10, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movl	12(%rax), %edx
	movl	%edx, (%rbx)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L122:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2409:
	.size	utrans_getUnicodeID_67, .-utrans_getUnicodeID_67
	.p2align 4
	.globl	utrans_getID_67
	.type	utrans_getID_67, @function
utrans_getID_67:
.LFB2410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movl	%edx, %ebx
	call	*104(%rax)
	movl	%ebx, %r8d
	movq	%r12, %rcx
	popq	%rbx
	movq	%rax, %rdi
	popq	%r12
	xorl	%r9d, %r9d
	movl	$2147483647, %edx
	xorl	%esi, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	.cfi_endproc
.LFE2410:
	.size	utrans_getID_67, .-utrans_getID_67
	.p2align 4
	.globl	utrans_register_67
	.type	utrans_register_67, @function
utrans_register_67:
.LFB2411:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L130
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L135
.L130:
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	jmp	_ZN6icu_6714Transliterator16registerInstanceEPS0_@PLT
	.cfi_endproc
.LFE2411:
	.size	utrans_register_67, .-utrans_register_67
	.p2align 4
	.globl	utrans_unregisterID_67
	.type	utrans_unregisterID_67, @function
utrans_unregisterID_67:
.LFB2412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	shrl	$31, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-96(%rbp), %r12
	leaq	-104(%rbp), %rdx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator10unregisterERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L139
	addq	$104, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L139:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2412:
	.size	utrans_unregisterID_67, .-utrans_unregisterID_67
	.p2align 4
	.globl	utrans_unregister_67
	.type	utrans_unregister_67, @function
utrans_unregister_67:
.LFB2413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator10unregisterERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L143
	addq	$88, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L143:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2413:
	.size	utrans_unregister_67, .-utrans_unregister_67
	.p2align 4
	.globl	utrans_setFilter_67
	.type	utrans_setFilter_67, @function
utrans_setFilter_67:
.LFB2414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L144
	movq	%rcx, %rbx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jg	.L144
	movq	%rdi, %r13
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L146
	cmpw	$0, (%rsi)
	jne	.L158
	xorl	%r12d, %r12d
.L146:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6714Transliterator11adoptFilterEPNS_13UnicodeFilterE@PLT
.L144:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	%rsi, -120(%rbp)
	leaq	-120(%rbp), %r8
	movl	%edx, %esi
	leaq	-112(%rbp), %r14
	movl	%edx, %ecx
	shrl	$31, %esi
	movq	%r8, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L147
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L148
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L148:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L146
.L159:
	call	__stack_chk_fail@PLT
.L147:
	movl	$7, (%rbx)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L144
	.cfi_endproc
.LFE2414:
	.size	utrans_setFilter_67, .-utrans_setFilter_67
	.p2align 4
	.globl	utrans_countAvailableIDs_67
	.type	utrans_countAvailableIDs_67, @function
utrans_countAvailableIDs_67:
.LFB2415:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6714Transliterator17countAvailableIDsEv@PLT
	.cfi_endproc
.LFE2415:
	.size	utrans_countAvailableIDs_67, .-utrans_countAvailableIDs_67
	.p2align 4
	.globl	utrans_getAvailableID_67
	.type	utrans_getAvailableID_67, @function
utrans_getAvailableID_67:
.LFB2416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	call	_ZN6icu_6714Transliterator14getAvailableIDEi@PLT
	movl	%ebx, %r8d
	movq	%r12, %rcx
	popq	%rbx
	movq	%rax, %rdi
	popq	%r12
	xorl	%r9d, %r9d
	movl	$2147483647, %edx
	xorl	%esi, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	.cfi_endproc
.LFE2416:
	.size	utrans_getAvailableID_67, .-utrans_getAvailableID_67
	.p2align 4
	.globl	utrans_openIDs_67
	.type	utrans_openIDs_67, @function
utrans_openIDs_67:
.LFB2421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L166
	movl	(%rdi), %eax
	movq	%rdi, %rbx
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jg	.L163
	movl	$64, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L169
	pxor	%xmm0, %xmm0
	leaq	utrans_enum_close(%rip), %rdx
	movups	%xmm0, (%rax)
	leaq	utrans_enum_count(%rip), %rax
	movq	%rdx, %xmm0
	movl	$0, 56(%r12)
	movq	%rax, %xmm1
	leaq	utrans_enum_unext(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r12)
	movq	%rax, %xmm0
	leaq	utrans_enum_reset(%rip), %rax
	movq	%rax, 48(%r12)
	movhps	uenum_nextDefault_67@GOTPCREL(%rip), %xmm0
	movups	%xmm0, 32(%r12)
	call	_ZN6icu_6714Transliterator17countAvailableIDsEv@PLT
	movl	%eax, 60(%r12)
.L163:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L163
.L169:
	movl	$7, (%rbx)
	jmp	.L163
	.cfi_endproc
.LFE2421:
	.size	utrans_openIDs_67, .-utrans_openIDs_67
	.p2align 4
	.globl	utrans_trans_67
	.type	utrans_trans_67, @function
utrans_trans_67:
.LFB2422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L170
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L170
	testq	%rdi, %rdi
	je	.L172
	testq	%rsi, %rsi
	je	.L172
	testq	%rdx, %rdx
	je	.L172
	movq	%r8, %r13
	testq	%r8, %r8
	je	.L172
	movq	(%rdi), %rax
	movl	%ecx, %r10d
	leaq	16+_ZTVN6icu_6715ReplaceableGlueE(%rip), %rbx
	movl	(%r8), %ecx
	leaq	-64(%rbp), %r12
	movq	%rsi, -56(%rbp)
	movq	%rdx, -48(%rbp)
	movq	%r12, %rsi
	movl	%r10d, %edx
	movq	%rbx, -64(%rbp)
	call	*32(%rax)
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movl	%eax, 0(%r13)
	call	_ZN6icu_6711ReplaceableD2Ev@PLT
.L170:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L181
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movl	$1, (%r9)
	jmp	.L170
.L181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2422:
	.size	utrans_trans_67, .-utrans_trans_67
	.p2align 4
	.globl	utrans_transIncremental_67
	.type	utrans_transIncremental_67, @function
utrans_transIncremental_67:
.LFB2423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L182
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L182
	testq	%rdi, %rdi
	je	.L184
	testq	%rsi, %rsi
	je	.L184
	testq	%rdx, %rdx
	je	.L184
	movq	%rcx, %r9
	testq	%rcx, %rcx
	je	.L184
	movq	(%rdi), %rax
	leaq	-48(%rbp), %r12
	movq	%rsi, -40(%rbp)
	movq	%r8, %rcx
	leaq	16+_ZTVN6icu_6715ReplaceableGlueE(%rip), %rbx
	movq	%rdx, -32(%rbp)
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%rbx, -48(%rbp)
	call	*64(%rax)
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	call	_ZN6icu_6711ReplaceableD2Ev@PLT
.L182:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L193
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movl	$1, (%r8)
	jmp	.L182
.L193:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2423:
	.size	utrans_transIncremental_67, .-utrans_transIncremental_67
	.p2align 4
	.globl	utrans_transUChars_67
	.type	utrans_transUChars_67, @function
utrans_transUChars_67:
.LFB2424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L194
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L194
	testq	%rsi, %rsi
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%r9, %r13
	sete	%dl
	testq	%r9, %r9
	sete	%al
	orb	%al, %dl
	jne	.L203
	movq	%rdi, %r15
	testq	%rdi, %rdi
	je	.L203
	movl	%ecx, %r10d
	testq	%r14, %r14
	je	.L198
	movl	(%r14), %edx
	testl	%edx, %edx
	js	.L198
.L199:
	leaq	-128(%rbp), %r9
	movl	%r10d, %ecx
	movq	%rbx, %rsi
	movl	%r10d, -156(%rbp)
	movq	%r9, %rdi
	movl	%r8d, -160(%rbp)
	movq	%r9, -152(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	movq	(%r15), %rax
	movl	0(%r13), %ecx
	movq	%r15, %rdi
	movl	-160(%rbp), %r8d
	movq	-152(%rbp), %r9
	movl	%r8d, %edx
	movq	%r9, %rsi
	call	*32(%rax)
	movl	-156(%rbp), %r10d
	movq	-152(%rbp), %r9
	movq	%r12, %rcx
	movl	%eax, 0(%r13)
	leaq	-136(%rbp), %rsi
	movl	%r10d, %edx
	movq	%r9, %rdi
	movq	%rbx, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	testq	%r14, %r14
	movq	-152(%rbp), %r9
	je	.L200
	movl	%eax, (%r14)
.L200:
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L194:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L214
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movl	$1, (%r12)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%rbx, %rdi
	movl	%r8d, -156(%rbp)
	movl	%r10d, -152(%rbp)
	call	u_strlen_67@PLT
	movl	-156(%rbp), %r8d
	movl	-152(%rbp), %r10d
	movl	%eax, %edx
	jmp	.L199
.L214:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2424:
	.size	utrans_transUChars_67, .-utrans_transUChars_67
	.p2align 4
	.globl	utrans_transIncrementalUChars_67
	.type	utrans_transIncrementalUChars_67, @function
utrans_transIncrementalUChars_67:
.LFB2425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L215
	movl	(%r9), %eax
	movq	%r9, %r12
	testl	%eax, %eax
	jg	.L215
	testq	%rsi, %rsi
	movq	%rdx, %r13
	movq	%rsi, %rbx
	sete	%dl
	testq	%r8, %r8
	sete	%al
	orb	%al, %dl
	jne	.L224
	movq	%rdi, %r14
	testq	%rdi, %rdi
	je	.L224
	movl	%ecx, %r15d
	testq	%r13, %r13
	je	.L219
	movl	0(%r13), %edx
	testl	%edx, %edx
	js	.L219
.L220:
	leaq	-128(%rbp), %r9
	movl	%r15d, %ecx
	movq	%rbx, %rsi
	movq	%r8, -160(%rbp)
	movq	%r9, %rdi
	movq	%r9, -152(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	movq	(%r14), %rax
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %r9
	movq	%r8, %rdx
	movq	%r9, %rsi
	call	*64(%rax)
	movq	-152(%rbp), %r9
	movq	%r12, %rcx
	movl	%r15d, %edx
	leaq	-136(%rbp), %rsi
	movq	%rbx, -136(%rbp)
	movq	%r9, %rdi
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	testq	%r13, %r13
	movq	-152(%rbp), %r9
	je	.L221
	movl	%eax, 0(%r13)
.L221:
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L215:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L235
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	movl	$1, (%r12)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%rbx, %rdi
	movq	%r8, -152(%rbp)
	call	u_strlen_67@PLT
	movq	-152(%rbp), %r8
	movl	%eax, %edx
	jmp	.L220
.L235:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2425:
	.size	utrans_transIncrementalUChars_67, .-utrans_transIncrementalUChars_67
	.p2align 4
	.globl	utrans_toRules_67
	.type	utrans_toRules_67, @function
utrans_toRules_67:
.LFB2426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L242
	movq	%r8, %r12
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jg	.L236
	movl	%ecx, %eax
	movq	%rdi, %r14
	movl	%esi, %ebx
	movq	%rdx, %r15
	movl	%ecx, %r13d
	shrl	$31, %eax
	testq	%rdx, %rdx
	je	.L245
.L239:
	testb	%al, %al
	je	.L240
	movl	$1, (%r12)
	xorl	%eax, %eax
.L236:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L246
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	leaq	-128(%rbp), %r8
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movq	%rax, -128(%rbp)
	movq	%r15, %rsi
	movq	%r8, %rdi
	movl	$2, %eax
	movq	%r8, -152(%rbp)
	movw	%ax, -120(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movq	(%r14), %rax
	movsbl	%bl, %edx
	movq	%r14, %rdi
	movq	-152(%rbp), %r8
	movq	%r8, %rsi
	call	*112(%rax)
	movq	-152(%rbp), %r8
	movq	%r12, %rcx
	movl	%r13d, %edx
	leaq	-136(%rbp), %rsi
	movq	%r15, -136(%rbp)
	movq	%r8, %rdi
	movq	%r8, -160(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -152(%rbp)
	movq	-160(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-152(%rbp), %eax
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L242:
	xorl	%eax, %eax
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L245:
	testl	%ecx, %ecx
	setne	%al
	jmp	.L239
.L246:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2426:
	.size	utrans_toRules_67, .-utrans_toRules_67
	.p2align 4
	.globl	utrans_getSourceSet_67
	.type	utrans_getSourceSet_67, @function
utrans_getSourceSet_67:
.LFB2427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$24, %rsp
	testq	%rcx, %rcx
	je	.L248
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L248
	testq	%rdx, %rdx
	je	.L255
.L249:
	testb	%sil, %sil
	je	.L250
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*120(%rax)
.L248:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	movq	%r12, %rsi
	call	_ZNK6icu_6714Transliterator12getSourceSetERNS_10UnicodeSetE@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movl	%esi, -28(%rbp)
	movq	%rdi, -24(%rbp)
	call	uset_openEmpty_67@PLT
	movl	-28(%rbp), %esi
	movq	-24(%rbp), %rdi
	movq	%rax, %r12
	jmp	.L249
	.cfi_endproc
.LFE2427:
	.size	utrans_getSourceSet_67, .-utrans_getSourceSet_67
	.weak	_ZTSN6icu_6715ReplaceableGlueE
	.section	.rodata._ZTSN6icu_6715ReplaceableGlueE,"aG",@progbits,_ZTSN6icu_6715ReplaceableGlueE,comdat
	.align 16
	.type	_ZTSN6icu_6715ReplaceableGlueE, @object
	.size	_ZTSN6icu_6715ReplaceableGlueE, 27
_ZTSN6icu_6715ReplaceableGlueE:
	.string	"N6icu_6715ReplaceableGlueE"
	.weak	_ZTIN6icu_6715ReplaceableGlueE
	.section	.data.rel.ro._ZTIN6icu_6715ReplaceableGlueE,"awG",@progbits,_ZTIN6icu_6715ReplaceableGlueE,comdat
	.align 8
	.type	_ZTIN6icu_6715ReplaceableGlueE, @object
	.size	_ZTIN6icu_6715ReplaceableGlueE, 24
_ZTIN6icu_6715ReplaceableGlueE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715ReplaceableGlueE
	.quad	_ZTIN6icu_6711ReplaceableE
	.weak	_ZTVN6icu_6715ReplaceableGlueE
	.section	.data.rel.ro._ZTVN6icu_6715ReplaceableGlueE,"awG",@progbits,_ZTVN6icu_6715ReplaceableGlueE,comdat
	.align 8
	.type	_ZTVN6icu_6715ReplaceableGlueE, @object
	.size	_ZTVN6icu_6715ReplaceableGlueE, 104
_ZTVN6icu_6715ReplaceableGlueE:
	.quad	0
	.quad	_ZTIN6icu_6715ReplaceableGlueE
	.quad	_ZN6icu_6715ReplaceableGlueD1Ev
	.quad	_ZN6icu_6715ReplaceableGlueD0Ev
	.quad	_ZNK6icu_6715ReplaceableGlue17getDynamicClassIDEv
	.quad	_ZNK6icu_6715ReplaceableGlue14extractBetweenEiiRNS_13UnicodeStringE
	.quad	_ZN6icu_6715ReplaceableGlue20handleReplaceBetweenEiiRKNS_13UnicodeStringE
	.quad	_ZN6icu_6715ReplaceableGlue4copyEiii
	.quad	_ZNK6icu_6711Replaceable11hasMetaDataEv
	.quad	_ZNK6icu_6711Replaceable5cloneEv
	.quad	_ZNK6icu_6715ReplaceableGlue9getLengthEv
	.quad	_ZNK6icu_6715ReplaceableGlue9getCharAtEi
	.quad	_ZNK6icu_6715ReplaceableGlue11getChar32AtEi
	.local	_ZZN6icu_6715ReplaceableGlue16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6715ReplaceableGlue16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
