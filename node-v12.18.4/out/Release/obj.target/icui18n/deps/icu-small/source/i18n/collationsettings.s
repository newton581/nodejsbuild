	.file	"collationsettings.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationSettingsD2Ev
	.type	_ZN6icu_6717CollationSettingsD2Ev, @function
_ZN6icu_6717CollationSettingsD2Ev:
.LFB3274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CollationSettingsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	76(%rdi), %eax
	testl	%eax, %eax
	jne	.L5
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712SharedObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movq	64(%rdi), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712SharedObjectD2Ev@PLT
	.cfi_endproc
.LFE3274:
	.size	_ZN6icu_6717CollationSettingsD2Ev, .-_ZN6icu_6717CollationSettingsD2Ev
	.globl	_ZN6icu_6717CollationSettingsD1Ev
	.set	_ZN6icu_6717CollationSettingsD1Ev,_ZN6icu_6717CollationSettingsD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6717CollationSettings16setReorderArraysEPKiiPKjiPKhR10UErrorCode.part.0, @function
_ZN6icu_6717CollationSettings16setReorderArraysEPKiiPKjiPKhR10UErrorCode.part.0:
.LFB4335:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	(%rdx,%r8), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$40, %rsp
	movslq	76(%rdi), %rdx
	movq	%rcx, -56(%rbp)
	cmpl	%edx, %eax
	jg	.L7
	movq	64(%rdi), %r15
.L8:
	movdqu	(%rbx), %xmm0
	leaq	(%r15,%rdx,4), %rax
	movq	%r15, %rdi
	movups	%xmm0, (%rax)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 48(%rax)
	movdqu	64(%rbx), %xmm4
	movups	%xmm4, 64(%rax)
	movdqu	80(%rbx), %xmm5
	movups	%xmm5, 80(%rax)
	movdqu	96(%rbx), %xmm6
	movups	%xmm6, 96(%rax)
	movdqu	112(%rbx), %xmm7
	movups	%xmm7, 112(%rax)
	movdqu	128(%rbx), %xmm0
	movups	%xmm0, 128(%rax)
	movdqu	144(%rbx), %xmm1
	movups	%xmm1, 144(%rax)
	movdqu	160(%rbx), %xmm2
	movups	%xmm2, 160(%rax)
	movdqu	176(%rbx), %xmm3
	movups	%xmm3, 176(%rax)
	movdqu	192(%rbx), %xmm4
	movups	%xmm4, 192(%rax)
	movdqu	208(%rbx), %xmm5
	movups	%xmm5, 208(%rax)
	movdqu	224(%rbx), %xmm6
	movups	%xmm6, 224(%rax)
	movdqu	240(%rbx), %xmm7
	leal	0(,%r14,4), %ebx
	movslq	%ebx, %rbx
	movups	%xmm7, 240(%rax)
	movq	%rbx, %rdx
	addq	%rbx, %r15
	call	memcpy@PLT
	movq	-56(%rbp), %rsi
	leal	0(,%r13,4), %edx
	movq	%r15, %rdi
	movslq	%edx, %rdx
	call	memcpy@PLT
	movslq	76(%r12), %rdx
	movq	64(%r12), %rax
	movl	%r14d, 72(%r12)
	movq	%r15, 48(%r12)
	leaq	(%rax,%rdx,4), %rax
	movl	%r13d, 56(%r12)
	movq	%rax, 32(%r12)
.L6:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	leal	3(%rax), %edx
	movq	%rsi, -72(%rbp)
	andl	$-4, %edx
	leal	256(,%rdx,4), %edi
	movl	%edx, -60(%rbp)
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movslq	-60(%rbp), %rdx
	movq	-72(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L13
	movl	76(%r12), %eax
	testl	%eax, %eax
	jne	.L14
.L11:
	movq	%r15, 64(%r12)
	movl	%edx, 76(%r12)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L14:
	movq	64(%r12), %rdi
	movq	%rsi, -72(%rbp)
	movl	%edx, -60(%rbp)
	call	uprv_free_67@PLT
	movq	-72(%rbp), %rsi
	movslq	-60(%rbp), %rdx
	jmp	.L11
.L13:
	movq	$0, 32(%r12)
	movq	16(%rbp), %rax
	movl	$0, 40(%r12)
	movl	$0, 56(%r12)
	movl	$0, 72(%r12)
	movl	$7, (%rax)
	jmp	.L6
	.cfi_endproc
.LFE4335:
	.size	_ZN6icu_6717CollationSettings16setReorderArraysEPKiiPKjiPKhR10UErrorCode.part.0, .-_ZN6icu_6717CollationSettings16setReorderArraysEPKiiPKjiPKhR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationSettingsD0Ev
	.type	_ZN6icu_6717CollationSettingsD0Ev, @function
_ZN6icu_6717CollationSettingsD0Ev:
.LFB3276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CollationSettingsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	76(%rdi), %eax
	testl	%eax, %eax
	jne	.L18
.L16:
	movq	%r12, %rdi
	call	_ZN6icu_6712SharedObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	64(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L16
	.cfi_endproc
.LFE3276:
	.size	_ZN6icu_6717CollationSettingsD0Ev, .-_ZN6icu_6717CollationSettingsD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationSettingsC2ERKS0_
	.type	_ZN6icu_6717CollationSettingsC2ERKS0_, @function
_ZN6icu_6717CollationSettingsC2ERKS0_:
.LFB3271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$40, %rsp
	movq	32(%rsi), %r9
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6717CollationSettingsE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	%rax, (%rdi)
	movq	24(%rsi), %rax
	movq	$0, 32(%rdi)
	movq	%rax, 24(%rdi)
	movl	40(%rsi), %eax
	movq	$0, 48(%rdi)
	movl	%eax, 40(%rdi)
	movl	80(%rsi), %eax
	movl	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movl	%eax, 80(%rdi)
	movl	$0, -28(%rbp)
	testq	%r9, %r9
	je	.L26
	movl	76(%r12), %r8d
	movq	48(%rsi), %rcx
	movq	64(%rsi), %rsi
	testl	%r8d, %r8d
	jne	.L22
	movl	56(%r12), %edx
	movq	%r9, 32(%rdi)
	movq	%rcx, 48(%rdi)
	movl	%edx, 56(%rdi)
	movl	72(%r12), %edx
	movq	%rsi, 64(%rdi)
	movl	%edx, 72(%rdi)
.L21:
	testl	%eax, %eax
	js	.L19
	movq	84(%r12), %rax
	leaq	84(%rdi), %rcx
	addq	$92, %rdi
	leaq	84(%r12), %rsi
	movq	%rax, -8(%rdi)
	movq	844(%r12), %rax
	movq	%rax, 752(%rdi)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	$768, %ecx
	shrl	$3, %ecx
	rep movsq
.L19:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	subq	$8, %rsp
	leaq	-28(%rbp), %rax
	movl	72(%r12), %edx
	movl	56(%r12), %r8d
	pushq	%rax
	movq	%rdi, -40(%rbp)
	call	_ZN6icu_6717CollationSettings16setReorderArraysEPKiiPKjiPKhR10UErrorCode.part.0
	movq	-40(%rbp), %rdi
	popq	%rdx
	popq	%rcx
	movl	80(%rdi), %eax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$0, 40(%rdi)
	jmp	.L21
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3271:
	.size	_ZN6icu_6717CollationSettingsC2ERKS0_, .-_ZN6icu_6717CollationSettingsC2ERKS0_
	.globl	_ZN6icu_6717CollationSettingsC1ERKS0_
	.set	_ZN6icu_6717CollationSettingsC1ERKS0_,_ZN6icu_6717CollationSettingsC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CollationSettingseqERKS0_
	.type	_ZNK6icu_6717CollationSettingseqERKS0_, @function
_ZNK6icu_6717CollationSettingseqERKS0_:
.LFB3277:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %edx
	xorl	%eax, %eax
	cmpl	24(%rsi), %edx
	jne	.L28
	andl	$12, %edx
	je	.L30
	movl	28(%rsi), %ecx
	cmpl	%ecx, 28(%rdi)
	je	.L30
.L28:
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movl	72(%rdi), %edx
	xorl	%eax, %eax
	cmpl	72(%rsi), %edx
	jne	.L28
	testl	%edx, %edx
	jle	.L35
	movq	64(%rsi), %rcx
	movq	64(%rdi), %rdi
	leal	-1(%rdx), %esi
	xorl	%eax, %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L41:
	leaq	1(%rax), %rdx
	cmpq	%rax, %rsi
	je	.L35
	movq	%rdx, %rax
.L31:
	movl	(%rcx,%rax,4), %edx
	cmpl	%edx, (%rdi,%rax,4)
	je	.L41
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3277:
	.size	_ZNK6icu_6717CollationSettingseqERKS0_, .-_ZNK6icu_6717CollationSettingseqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CollationSettings8hashCodeEv
	.type	_ZNK6icu_6717CollationSettings8hashCodeEv, @function
_ZNK6icu_6717CollationSettings8hashCodeEv:
.LFB3278:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %edx
	movl	%edx, %eax
	sall	$8, %eax
	andl	$12, %edx
	je	.L43
	xorl	28(%rdi), %eax
.L43:
	movl	72(%rdi), %edx
	xorl	%edx, %eax
	testl	%edx, %edx
	jle	.L42
	movq	64(%rdi), %rdi
	leal	-1(%rdx), %esi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L45:
	movl	(%rdi,%rcx,4), %edx
	sall	%cl, %edx
	xorl	%edx, %eax
	movq	%rcx, %rdx
	addq	$1, %rcx
	cmpq	%rdx, %rsi
	jne	.L45
.L42:
	ret
	.cfi_endproc
.LFE3278:
	.size	_ZNK6icu_6717CollationSettings8hashCodeEv, .-_ZNK6icu_6717CollationSettings8hashCodeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationSettings15resetReorderingEv
	.type	_ZN6icu_6717CollationSettings15resetReorderingEv, @function
_ZN6icu_6717CollationSettings15resetReorderingEv:
.LFB3279:
	.cfi_startproc
	endbr64
	movq	$0, 32(%rdi)
	movl	$0, 40(%rdi)
	movl	$0, 56(%rdi)
	movl	$0, 72(%rdi)
	ret
	.cfi_endproc
.LFE3279:
	.size	_ZN6icu_6717CollationSettings15resetReorderingEv, .-_ZN6icu_6717CollationSettings15resetReorderingEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationSettings16setReorderArraysEPKiiPKjiPKhR10UErrorCode
	.type	_ZN6icu_6717CollationSettings16setReorderArraysEPKiiPKjiPKhR10UErrorCode, @function
_ZN6icu_6717CollationSettings16setReorderArraysEPKiiPKjiPKhR10UErrorCode:
.LFB3282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L51
	movl	%edx, %r14d
	leal	(%rdx,%r8), %eax
	movslq	76(%rdi), %rdx
	movq	%rdi, %r12
	movq	%rcx, %r15
	movl	%r8d, %r13d
	movq	%r9, %rbx
	cmpl	%edx, %eax
	jg	.L53
	movq	64(%rdi), %rcx
.L54:
	movdqu	(%rbx), %xmm0
	leaq	(%rcx,%rdx,4), %rax
	movq	%rcx, %rdi
	movups	%xmm0, (%rax)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 48(%rax)
	movdqu	64(%rbx), %xmm4
	movups	%xmm4, 64(%rax)
	movdqu	80(%rbx), %xmm5
	movups	%xmm5, 80(%rax)
	movdqu	96(%rbx), %xmm6
	movups	%xmm6, 96(%rax)
	movdqu	112(%rbx), %xmm7
	movups	%xmm7, 112(%rax)
	movdqu	128(%rbx), %xmm0
	movups	%xmm0, 128(%rax)
	movdqu	144(%rbx), %xmm1
	movups	%xmm1, 144(%rax)
	movdqu	160(%rbx), %xmm2
	movups	%xmm2, 160(%rax)
	movdqu	176(%rbx), %xmm3
	movups	%xmm3, 176(%rax)
	movdqu	192(%rbx), %xmm4
	movups	%xmm4, 192(%rax)
	movdqu	208(%rbx), %xmm5
	movups	%xmm5, 208(%rax)
	movdqu	224(%rbx), %xmm6
	movups	%xmm6, 224(%rax)
	movdqu	240(%rbx), %xmm7
	leal	0(,%r14,4), %ebx
	movslq	%ebx, %rbx
	movups	%xmm7, 240(%rax)
	movq	%rbx, %rdx
	call	memcpy@PLT
	leal	0(,%r13,4), %edx
	movq	%r15, %rsi
	movq	%rax, %rcx
	movslq	%edx, %rdx
	addq	%rbx, %rcx
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	76(%r12), %rdx
	movl	%r14d, 72(%r12)
	movq	%rax, %rcx
	movq	64(%r12), %rax
	movl	%r13d, 56(%r12)
	movq	%rcx, 48(%r12)
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, 32(%r12)
.L51:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	leal	3(%rax), %edx
	movq	%rsi, -64(%rbp)
	andl	$-4, %edx
	leal	256(,%rdx,4), %edi
	movl	%edx, -52(%rbp)
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movslq	-52(%rbp), %rdx
	movq	-64(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L58
	movl	76(%r12), %eax
	testl	%eax, %eax
	jne	.L59
.L56:
	movq	%rcx, 64(%r12)
	movl	%edx, 76(%r12)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L59:
	movq	64(%r12), %rdi
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movl	%edx, -52(%rbp)
	call	uprv_free_67@PLT
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movslq	-52(%rbp), %rdx
	jmp	.L56
.L58:
	movq	$0, 32(%r12)
	movq	16(%rbp), %rax
	movl	$0, 40(%r12)
	movl	$0, 56(%r12)
	movl	$0, 72(%r12)
	movl	$7, (%rax)
	jmp	.L51
	.cfi_endproc
.LFE3282:
	.size	_ZN6icu_6717CollationSettings16setReorderArraysEPKiiPKjiPKhR10UErrorCode, .-_ZN6icu_6717CollationSettings16setReorderArraysEPKiiPKjiPKhR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationSettings18copyReorderingFromERKS0_R10UErrorCode
	.type	_ZN6icu_6717CollationSettings18copyReorderingFromERKS0_R10UErrorCode, @function
_ZN6icu_6717CollationSettings18copyReorderingFromERKS0_R10UErrorCode:
.LFB3283:
	.cfi_startproc
	endbr64
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L65
	movq	32(%rsi), %r9
	testq	%r9, %r9
	je	.L67
	movl	40(%rsi), %eax
	movl	56(%rsi), %r8d
	movq	48(%rsi), %rcx
	movl	72(%rsi), %r11d
	movq	64(%rsi), %r10
	movl	76(%rsi), %esi
	movl	%eax, 40(%rdi)
	testl	%esi, %esi
	jne	.L63
	movq	%r9, 32(%rdi)
	movq	%rcx, 48(%rdi)
	movl	%r8d, 56(%rdi)
	movq	%r10, 64(%rdi)
	movl	%r11d, 72(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	movq	$0, 32(%rdi)
	movl	$0, 40(%rdi)
	movl	$0, 56(%rdi)
	movl	$0, 72(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r10, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%rdx
	movl	%r11d, %edx
	call	_ZN6icu_6717CollationSettings16setReorderArraysEPKiiPKjiPKhR10UErrorCode.part.0
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3283:
	.size	_ZN6icu_6717CollationSettings18copyReorderingFromERKS0_R10UErrorCode, .-_ZN6icu_6717CollationSettings18copyReorderingFromERKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationSettings25reorderTableHasSplitBytesEPKh
	.type	_ZN6icu_6717CollationSettings25reorderTableHasSplitBytesEPKh, @function
_ZN6icu_6717CollationSettings25reorderTableHasSplitBytesEPKh:
.LFB3284:
	.cfi_startproc
	endbr64
	leaq	1(%rdi), %rax
	addq	$256, %rdi
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L74:
	addq	$1, %rax
	cmpq	%rdi, %rax
	je	.L73
.L70:
	cmpb	$0, (%rax)
	jne	.L74
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3284:
	.size	_ZN6icu_6717CollationSettings25reorderTableHasSplitBytesEPKh, .-_ZN6icu_6717CollationSettings25reorderTableHasSplitBytesEPKh
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CollationSettings9reorderExEj
	.type	_ZNK6icu_6717CollationSettings9reorderExEj, @function
_ZNK6icu_6717CollationSettings9reorderExEj:
.LFB3285:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	cmpl	%esi, 40(%rdi)
	jbe	.L75
	movq	48(%rdi), %rdx
	movl	%esi, %ecx
	orl	$65535, %ecx
	movl	(%rdx), %eax
	cmpl	%eax, %ecx
	jb	.L77
	.p2align 4,,10
	.p2align 3
.L78:
	movl	4(%rdx), %eax
	addq	$4, %rdx
	cmpl	%eax, %ecx
	jnb	.L78
.L77:
	sall	$24, %eax
	addl	%esi, %eax
.L75:
	ret
	.cfi_endproc
.LFE3285:
	.size	_ZNK6icu_6717CollationSettings9reorderExEj, .-_ZNK6icu_6717CollationSettings9reorderExEj
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationSettings11setStrengthEiiR10UErrorCode
	.type	_ZN6icu_6717CollationSettings11setStrengthEiiR10UErrorCode, @function
_ZN6icu_6717CollationSettings11setStrengthEiiR10UErrorCode:
.LFB3286:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L81
	movl	24(%rdi), %eax
	andb	$15, %ah
	cmpl	$3, %esi
	jg	.L83
	testl	%esi, %esi
	jns	.L84
	cmpl	$-1, %esi
	jne	.L86
	andl	$61440, %edx
	orl	%eax, %edx
	movl	%edx, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	cmpl	$15, %esi
	je	.L84
.L86:
	movl	$1, (%rcx)
.L81:
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	sall	$12, %esi
	orl	%eax, %esi
	movl	%esi, 24(%rdi)
	ret
	.cfi_endproc
.LFE3286:
	.size	_ZN6icu_6717CollationSettings11setStrengthEiiR10UErrorCode, .-_ZN6icu_6717CollationSettings11setStrengthEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationSettings7setFlagEi18UColAttributeValueiR10UErrorCode
	.type	_ZN6icu_6717CollationSettings7setFlagEi18UColAttributeValueiR10UErrorCode, @function
_ZN6icu_6717CollationSettings7setFlagEi18UColAttributeValueiR10UErrorCode:
.LFB3287:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L87
	cmpl	$16, %edx
	je	.L89
	cmpl	$17, %edx
	je	.L90
	cmpl	$-1, %edx
	je	.L94
	movl	$1, (%r8)
.L87:
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movl	24(%rdi), %eax
	xorl	%eax, %ecx
	andl	%esi, %ecx
	xorl	%eax, %ecx
	movl	%ecx, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	orl	%esi, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	notl	%esi
	andl	%esi, 24(%rdi)
	ret
	.cfi_endproc
.LFE3287:
	.size	_ZN6icu_6717CollationSettings7setFlagEi18UColAttributeValueiR10UErrorCode, .-_ZN6icu_6717CollationSettings7setFlagEi18UColAttributeValueiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationSettings12setCaseFirstE18UColAttributeValueiR10UErrorCode
	.type	_ZN6icu_6717CollationSettings12setCaseFirstE18UColAttributeValueiR10UErrorCode, @function
_ZN6icu_6717CollationSettings12setCaseFirstE18UColAttributeValueiR10UErrorCode:
.LFB3288:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L95
	movl	24(%rdi), %eax
	andb	$-4, %ah
	cmpl	$24, %esi
	je	.L97
	jg	.L98
	cmpl	$-1, %esi
	je	.L99
	cmpl	$16, %esi
	jne	.L101
	movl	%eax, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	cmpl	$25, %esi
	jne	.L101
	orb	$3, %ah
	movl	%eax, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$1, (%rcx)
.L95:
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	andl	$768, %edx
	orl	%eax, %edx
	movl	%edx, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	orb	$2, %ah
	movl	%eax, 24(%rdi)
	ret
	.cfi_endproc
.LFE3288:
	.size	_ZN6icu_6717CollationSettings12setCaseFirstE18UColAttributeValueiR10UErrorCode, .-_ZN6icu_6717CollationSettings12setCaseFirstE18UColAttributeValueiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationSettings20setAlternateHandlingE18UColAttributeValueiR10UErrorCode
	.type	_ZN6icu_6717CollationSettings20setAlternateHandlingE18UColAttributeValueiR10UErrorCode, @function
_ZN6icu_6717CollationSettings20setAlternateHandlingE18UColAttributeValueiR10UErrorCode:
.LFB3289:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L103
	movl	24(%rdi), %eax
	andl	$-13, %eax
	cmpl	$20, %esi
	je	.L105
	cmpl	$21, %esi
	je	.L106
	cmpl	$-1, %esi
	je	.L110
	movl	$1, (%rcx)
.L103:
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	andl	$12, %edx
	orl	%eax, %edx
	movl	%edx, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	movl	%eax, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	orl	$4, %eax
	movl	%eax, 24(%rdi)
	ret
	.cfi_endproc
.LFE3289:
	.size	_ZN6icu_6717CollationSettings20setAlternateHandlingE18UColAttributeValueiR10UErrorCode, .-_ZN6icu_6717CollationSettings20setAlternateHandlingE18UColAttributeValueiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationSettings14setMaxVariableEiiR10UErrorCode
	.type	_ZN6icu_6717CollationSettings14setMaxVariableEiiR10UErrorCode, @function
_ZN6icu_6717CollationSettings14setMaxVariableEiiR10UErrorCode:
.LFB3290:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L111
	movl	24(%rdi), %eax
	andl	$-113, %eax
	cmpl	$-1, %esi
	je	.L113
	cmpl	$3, %esi
	ja	.L114
	sall	$4, %esi
	orl	%eax, %esi
	movl	%esi, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$1, (%rcx)
.L111:
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	andl	$112, %edx
	orl	%eax, %edx
	movl	%edx, 24(%rdi)
	ret
	.cfi_endproc
.LFE3290:
	.size	_ZN6icu_6717CollationSettings14setMaxVariableEiiR10UErrorCode, .-_ZN6icu_6717CollationSettings14setMaxVariableEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationSettings13setReorderingERKNS_13CollationDataEPKiiR10UErrorCode
	.type	_ZN6icu_6717CollationSettings13setReorderingERKNS_13CollationDataEPKiiR10UErrorCode, @function
_ZN6icu_6717CollationSettings13setReorderingERKNS_13CollationDataEPKiiR10UErrorCode:
.LFB3281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L115
	movq	%rdi, %r12
	movl	%ecx, %r15d
	testl	%ecx, %ecx
	je	.L117
	movq	%rsi, %r9
	movq	%rdx, %r13
	movq	%r8, %rbx
	cmpl	$1, %ecx
	jne	.L118
	cmpl	$103, (%rdx)
	je	.L117
.L118:
	leaq	-352(%rbp), %r14
	movq	%rbx, %rsi
	movq	%r9, -360(%rbp)
	movq	%r14, %rdi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movq	%r14, %rcx
	movq	%rbx, %r8
	movl	%r15d, %edx
	movq	-360(%rbp), %r9
	movq	%r13, %rsi
	movq	%r9, %rdi
	call	_ZNK6icu_6713CollationData17makeReorderRangesEPKiiRNS_9UVector32ER10UErrorCode@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L119
	movl	-344(%rbp), %eax
	movl	%eax, -360(%rbp)
	testl	%eax, %eax
	je	.L282
	movq	-328(%rbp), %r11
	cltq
	movq	%rax, %rdx
	movl	-4(%r11,%rax,4), %eax
	xorw	%ax, %ax
	movl	%eax, 40(%r12)
	testl	%edx, %edx
	jle	.L283
	leaq	-320(%rbp), %rsi
	leal	-1(%rdx), %ecx
	movq	%r12, -376(%rbp)
	movdqa	.LC0(%rip), %xmm9
	movdqa	.LC1(%rip), %xmm8
	movq	%rsi, -368(%rbp)
	xorl	%edx, %edx
	movl	$-1, %r10d
	movdqa	.LC2(%rip), %xmm7
	movdqa	.LC3(%rip), %xmm6
	movq	%r13, -384(%rbp)
	xorl	%eax, %eax
	movdqa	.LC4(%rip), %xmm5
	movq	%rbx, -392(%rbp)
	movq	%rcx, %r13
	movq	%rsi, %rbx
	movdqa	.LC5(%rip), %xmm4
	movdqa	.LC6(%rip), %xmm12
	movdqa	.LC7(%rip), %xmm11
	movdqa	.LC8(%rip), %xmm10
	.p2align 4,,10
	.p2align 3
.L131:
	movl	(%r11,%rdx,4), %esi
	movl	%edx, %r12d
	movl	%esi, %ecx
	shrl	$24, %ecx
	cmpl	%eax, %ecx
	jle	.L125
	movl	%ecx, %r9d
	subl	%eax, %r9d
	leal	-1(%r9), %edi
	cmpl	$14, %edi
	jbe	.L126
	movd	%eax, %xmm2
	movl	%r9d, %r8d
	movslq	%eax, %rdi
	pshufd	$0, %xmm2, %xmm0
	movd	%esi, %xmm2
	addq	%rbx, %rdi
	shrl	$4, %r8d
	movdqa	%xmm0, %xmm1
	movdqa	%xmm0, %xmm13
	movdqa	%xmm0, %xmm15
	paddd	%xmm7, %xmm13
	paddd	%xmm8, %xmm1
	punpcklbw	%xmm2, %xmm2
	movdqa	%xmm1, %xmm14
	punpcklwd	%xmm13, %xmm1
	paddd	%xmm5, %xmm15
	punpckhwd	%xmm13, %xmm14
	movdqa	%xmm1, %xmm13
	punpcklwd	%xmm2, %xmm2
	punpckhwd	%xmm14, %xmm13
	punpcklwd	%xmm14, %xmm1
	pshufd	$0, %xmm2, %xmm2
	punpcklwd	%xmm13, %xmm1
	movdqa	%xmm0, %xmm13
	movdqa	%xmm0, %xmm3
	paddd	%xmm6, %xmm13
	pand	%xmm4, %xmm1
	paddd	%xmm9, %xmm3
	movdqa	%xmm13, %xmm14
	punpcklwd	%xmm15, %xmm13
	punpckhwd	%xmm15, %xmm14
	movdqa	%xmm13, %xmm15
	punpckhwd	%xmm14, %xmm15
	punpcklwd	%xmm14, %xmm13
	punpcklwd	%xmm15, %xmm13
	pand	%xmm4, %xmm13
	packuswb	%xmm13, %xmm1
	paddb	%xmm2, %xmm1
	movups	%xmm1, (%rdi)
	cmpl	$1, %r8d
	je	.L127
	movdqa	%xmm0, %xmm1
	movdqa	%xmm3, %xmm14
	movdqa	%xmm0, %xmm13
	movdqa	.LC9(%rip), %xmm15
	paddd	%xmm11, %xmm1
	paddd	%xmm12, %xmm13
	punpcklwd	%xmm1, %xmm3
	punpckhwd	%xmm1, %xmm14
	paddd	%xmm0, %xmm15
	movdqa	%xmm3, %xmm1
	punpcklwd	%xmm14, %xmm3
	punpckhwd	%xmm14, %xmm1
	punpcklwd	%xmm1, %xmm3
	movdqa	%xmm0, %xmm1
	paddd	%xmm10, %xmm1
	pand	%xmm4, %xmm3
	movdqa	%xmm1, %xmm14
	punpcklwd	%xmm15, %xmm1
	punpckhwd	%xmm15, %xmm14
	movdqa	%xmm1, %xmm15
	punpckhwd	%xmm14, %xmm15
	punpcklwd	%xmm14, %xmm1
	punpcklwd	%xmm15, %xmm1
	pand	%xmm4, %xmm1
	packuswb	%xmm1, %xmm3
	paddb	%xmm2, %xmm3
	movups	%xmm3, 16(%rdi)
	cmpl	$2, %r8d
	je	.L127
	movdqa	.LC11(%rip), %xmm1
	movdqa	%xmm13, %xmm14
	movdqa	.LC13(%rip), %xmm15
	movdqa	.LC10(%rip), %xmm3
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm15
	punpcklwd	%xmm1, %xmm13
	punpckhwd	%xmm1, %xmm14
	paddd	%xmm0, %xmm3
	movdqa	%xmm13, %xmm1
	punpcklwd	%xmm14, %xmm13
	punpckhwd	%xmm14, %xmm1
	punpcklwd	%xmm1, %xmm13
	movdqa	.LC12(%rip), %xmm1
	pand	%xmm4, %xmm13
	paddd	%xmm0, %xmm1
	movdqa	%xmm1, %xmm14
	punpcklwd	%xmm15, %xmm1
	punpckhwd	%xmm15, %xmm14
	movdqa	%xmm1, %xmm15
	punpckhwd	%xmm14, %xmm15
	punpcklwd	%xmm14, %xmm1
	punpcklwd	%xmm15, %xmm1
	pand	%xmm4, %xmm1
	packuswb	%xmm1, %xmm13
	paddb	%xmm2, %xmm13
	movups	%xmm13, 32(%rdi)
	cmpl	$3, %r8d
	je	.L127
	movdqa	.LC15(%rip), %xmm1
	movdqa	%xmm3, %xmm14
	movdqa	.LC17(%rip), %xmm15
	movdqa	.LC14(%rip), %xmm13
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm15
	punpcklwd	%xmm1, %xmm3
	punpckhwd	%xmm1, %xmm14
	paddd	%xmm0, %xmm13
	movdqa	%xmm3, %xmm1
	punpcklwd	%xmm14, %xmm3
	punpckhwd	%xmm14, %xmm1
	punpcklwd	%xmm1, %xmm3
	movdqa	.LC16(%rip), %xmm1
	pand	%xmm4, %xmm3
	paddd	%xmm0, %xmm1
	movdqa	%xmm1, %xmm14
	punpcklwd	%xmm15, %xmm1
	punpckhwd	%xmm15, %xmm14
	movdqa	%xmm1, %xmm15
	punpckhwd	%xmm14, %xmm15
	punpcklwd	%xmm14, %xmm1
	punpcklwd	%xmm15, %xmm1
	pand	%xmm4, %xmm1
	packuswb	%xmm1, %xmm3
	paddb	%xmm2, %xmm3
	movups	%xmm3, 48(%rdi)
	cmpl	$4, %r8d
	je	.L127
	movdqa	.LC19(%rip), %xmm1
	movdqa	%xmm13, %xmm14
	movdqa	.LC21(%rip), %xmm15
	movdqa	.LC18(%rip), %xmm3
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm15
	punpcklwd	%xmm1, %xmm13
	punpckhwd	%xmm1, %xmm14
	paddd	%xmm0, %xmm3
	movdqa	%xmm13, %xmm1
	punpcklwd	%xmm14, %xmm13
	punpckhwd	%xmm14, %xmm1
	punpcklwd	%xmm1, %xmm13
	movdqa	.LC20(%rip), %xmm1
	pand	%xmm4, %xmm13
	paddd	%xmm0, %xmm1
	movdqa	%xmm1, %xmm14
	punpcklwd	%xmm15, %xmm1
	punpckhwd	%xmm15, %xmm14
	movdqa	%xmm1, %xmm15
	punpckhwd	%xmm14, %xmm15
	punpcklwd	%xmm14, %xmm1
	punpcklwd	%xmm15, %xmm1
	pand	%xmm4, %xmm1
	packuswb	%xmm1, %xmm13
	paddb	%xmm2, %xmm13
	movups	%xmm13, 64(%rdi)
	cmpl	$5, %r8d
	je	.L127
	movdqa	.LC23(%rip), %xmm1
	movdqa	%xmm3, %xmm14
	movdqa	.LC25(%rip), %xmm15
	movdqa	.LC22(%rip), %xmm13
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm15
	punpcklwd	%xmm1, %xmm3
	punpckhwd	%xmm1, %xmm14
	paddd	%xmm0, %xmm13
	movdqa	%xmm3, %xmm1
	punpcklwd	%xmm14, %xmm3
	punpckhwd	%xmm14, %xmm1
	punpcklwd	%xmm1, %xmm3
	movdqa	.LC24(%rip), %xmm1
	pand	%xmm4, %xmm3
	paddd	%xmm0, %xmm1
	movdqa	%xmm1, %xmm14
	punpcklwd	%xmm15, %xmm1
	punpckhwd	%xmm15, %xmm14
	movdqa	%xmm1, %xmm15
	punpckhwd	%xmm14, %xmm15
	punpcklwd	%xmm14, %xmm1
	punpcklwd	%xmm15, %xmm1
	pand	%xmm4, %xmm1
	packuswb	%xmm1, %xmm3
	paddb	%xmm2, %xmm3
	movups	%xmm3, 80(%rdi)
	cmpl	$6, %r8d
	je	.L127
	movdqa	.LC27(%rip), %xmm1
	movdqa	%xmm13, %xmm14
	movdqa	.LC26(%rip), %xmm3
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm3
	punpcklwd	%xmm1, %xmm13
	punpckhwd	%xmm1, %xmm14
	movdqa	%xmm13, %xmm1
	punpcklwd	%xmm14, %xmm13
	punpckhwd	%xmm14, %xmm1
	movdqa	.LC28(%rip), %xmm14
	punpcklwd	%xmm1, %xmm13
	movdqa	.LC29(%rip), %xmm1
	paddd	%xmm0, %xmm14
	pand	%xmm4, %xmm13
	paddd	%xmm0, %xmm1
	movdqa	%xmm14, %xmm15
	punpcklwd	%xmm1, %xmm14
	punpckhwd	%xmm1, %xmm15
	movdqa	%xmm14, %xmm1
	punpckhwd	%xmm15, %xmm14
	punpcklwd	%xmm15, %xmm1
	movdqa	%xmm13, %xmm15
	punpcklwd	%xmm14, %xmm1
	pand	%xmm4, %xmm1
	packuswb	%xmm1, %xmm15
	movdqa	%xmm15, %xmm1
	paddb	%xmm2, %xmm1
	movups	%xmm1, 96(%rdi)
	cmpl	$7, %r8d
	je	.L127
	movdqa	.LC31(%rip), %xmm1
	movdqa	%xmm3, %xmm14
	movdqa	.LC30(%rip), %xmm13
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm13
	punpcklwd	%xmm1, %xmm3
	punpckhwd	%xmm1, %xmm14
	movdqa	%xmm3, %xmm1
	punpcklwd	%xmm14, %xmm3
	punpckhwd	%xmm14, %xmm1
	movdqa	.LC32(%rip), %xmm14
	punpcklwd	%xmm1, %xmm3
	movdqa	.LC33(%rip), %xmm1
	paddd	%xmm0, %xmm14
	pand	%xmm4, %xmm3
	paddd	%xmm0, %xmm1
	movdqa	%xmm14, %xmm15
	punpcklwd	%xmm1, %xmm14
	punpckhwd	%xmm1, %xmm15
	movdqa	%xmm14, %xmm1
	punpckhwd	%xmm15, %xmm14
	punpcklwd	%xmm15, %xmm1
	movdqa	%xmm3, %xmm15
	punpcklwd	%xmm14, %xmm1
	pand	%xmm4, %xmm1
	packuswb	%xmm1, %xmm15
	movdqa	%xmm15, %xmm1
	paddb	%xmm2, %xmm1
	movups	%xmm1, 112(%rdi)
	cmpl	$8, %r8d
	je	.L127
	movdqa	.LC35(%rip), %xmm1
	movdqa	%xmm13, %xmm14
	movdqa	.LC34(%rip), %xmm3
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm3
	punpcklwd	%xmm1, %xmm13
	punpckhwd	%xmm1, %xmm14
	movdqa	%xmm13, %xmm1
	punpcklwd	%xmm14, %xmm13
	punpckhwd	%xmm14, %xmm1
	movdqa	.LC36(%rip), %xmm14
	punpcklwd	%xmm1, %xmm13
	movdqa	.LC37(%rip), %xmm1
	paddd	%xmm0, %xmm14
	pand	%xmm4, %xmm13
	paddd	%xmm0, %xmm1
	movdqa	%xmm14, %xmm15
	punpcklwd	%xmm1, %xmm14
	punpckhwd	%xmm1, %xmm15
	movdqa	%xmm14, %xmm1
	punpckhwd	%xmm15, %xmm14
	punpcklwd	%xmm15, %xmm1
	movdqa	%xmm13, %xmm15
	punpcklwd	%xmm14, %xmm1
	pand	%xmm4, %xmm1
	packuswb	%xmm1, %xmm15
	movdqa	%xmm15, %xmm1
	paddb	%xmm2, %xmm1
	movups	%xmm1, 128(%rdi)
	cmpl	$9, %r8d
	je	.L127
	movdqa	.LC39(%rip), %xmm1
	movdqa	%xmm3, %xmm14
	movdqa	.LC38(%rip), %xmm13
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm13
	punpcklwd	%xmm1, %xmm3
	punpckhwd	%xmm1, %xmm14
	movdqa	%xmm3, %xmm1
	punpcklwd	%xmm14, %xmm3
	punpckhwd	%xmm14, %xmm1
	movdqa	.LC40(%rip), %xmm14
	punpcklwd	%xmm1, %xmm3
	movdqa	.LC41(%rip), %xmm1
	paddd	%xmm0, %xmm14
	pand	%xmm4, %xmm3
	paddd	%xmm0, %xmm1
	movdqa	%xmm14, %xmm15
	punpcklwd	%xmm1, %xmm14
	punpckhwd	%xmm1, %xmm15
	movdqa	%xmm14, %xmm1
	punpckhwd	%xmm15, %xmm14
	punpcklwd	%xmm15, %xmm1
	movdqa	%xmm3, %xmm15
	punpcklwd	%xmm14, %xmm1
	pand	%xmm4, %xmm1
	packuswb	%xmm1, %xmm15
	movdqa	%xmm15, %xmm1
	paddb	%xmm2, %xmm1
	movups	%xmm1, 144(%rdi)
	cmpl	$10, %r8d
	je	.L127
	movdqa	.LC43(%rip), %xmm1
	movdqa	%xmm13, %xmm14
	movdqa	.LC42(%rip), %xmm3
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm3
	punpcklwd	%xmm1, %xmm13
	punpckhwd	%xmm1, %xmm14
	movdqa	%xmm13, %xmm1
	punpcklwd	%xmm14, %xmm13
	punpckhwd	%xmm14, %xmm1
	movdqa	.LC44(%rip), %xmm14
	punpcklwd	%xmm1, %xmm13
	movdqa	.LC45(%rip), %xmm1
	paddd	%xmm0, %xmm14
	pand	%xmm4, %xmm13
	paddd	%xmm0, %xmm1
	movdqa	%xmm14, %xmm15
	punpcklwd	%xmm1, %xmm14
	punpckhwd	%xmm1, %xmm15
	movdqa	%xmm14, %xmm1
	punpckhwd	%xmm15, %xmm14
	punpcklwd	%xmm15, %xmm1
	movdqa	%xmm13, %xmm15
	punpcklwd	%xmm14, %xmm1
	pand	%xmm4, %xmm1
	packuswb	%xmm1, %xmm15
	movdqa	%xmm15, %xmm1
	paddb	%xmm2, %xmm1
	movups	%xmm1, 160(%rdi)
	cmpl	$11, %r8d
	je	.L127
	movdqa	.LC47(%rip), %xmm14
	movdqa	%xmm3, %xmm1
	movdqa	.LC46(%rip), %xmm13
	paddd	%xmm0, %xmm14
	paddd	%xmm0, %xmm13
	punpcklwd	%xmm14, %xmm3
	punpckhwd	%xmm14, %xmm1
	movdqa	%xmm3, %xmm14
	punpcklwd	%xmm1, %xmm3
	punpckhwd	%xmm1, %xmm14
	movdqa	.LC49(%rip), %xmm1
	punpcklwd	%xmm14, %xmm3
	movdqa	.LC48(%rip), %xmm14
	paddd	%xmm0, %xmm1
	pand	%xmm4, %xmm3
	paddd	%xmm0, %xmm14
	movdqa	%xmm14, %xmm15
	punpcklwd	%xmm1, %xmm14
	punpckhwd	%xmm1, %xmm15
	movdqa	%xmm14, %xmm1
	punpcklwd	%xmm15, %xmm1
	punpckhwd	%xmm15, %xmm14
	movdqa	%xmm3, %xmm15
	punpcklwd	%xmm14, %xmm1
	pand	%xmm4, %xmm1
	packuswb	%xmm1, %xmm15
	movdqa	%xmm15, %xmm1
	paddb	%xmm2, %xmm1
	movups	%xmm1, 176(%rdi)
	cmpl	$12, %r8d
	je	.L127
	movdqa	.LC51(%rip), %xmm14
	movdqa	%xmm13, %xmm1
	movdqa	.LC50(%rip), %xmm3
	paddd	%xmm0, %xmm14
	paddd	%xmm0, %xmm3
	punpcklwd	%xmm14, %xmm13
	punpckhwd	%xmm14, %xmm1
	movdqa	%xmm13, %xmm14
	punpcklwd	%xmm1, %xmm13
	punpckhwd	%xmm1, %xmm14
	movdqa	.LC53(%rip), %xmm1
	punpcklwd	%xmm14, %xmm13
	movdqa	.LC52(%rip), %xmm14
	paddd	%xmm0, %xmm1
	pand	%xmm4, %xmm13
	paddd	%xmm0, %xmm14
	movdqa	%xmm14, %xmm15
	punpcklwd	%xmm1, %xmm14
	punpckhwd	%xmm1, %xmm15
	movdqa	%xmm14, %xmm1
	punpcklwd	%xmm15, %xmm1
	punpckhwd	%xmm15, %xmm14
	movdqa	%xmm13, %xmm15
	punpcklwd	%xmm14, %xmm1
	pand	%xmm4, %xmm1
	packuswb	%xmm1, %xmm15
	movdqa	%xmm15, %xmm1
	paddb	%xmm2, %xmm1
	movups	%xmm1, 192(%rdi)
	cmpl	$13, %r8d
	je	.L127
	movdqa	.LC55(%rip), %xmm14
	movdqa	%xmm3, %xmm1
	movdqa	.LC54(%rip), %xmm13
	paddd	%xmm0, %xmm14
	paddd	%xmm0, %xmm13
	punpcklwd	%xmm14, %xmm3
	punpckhwd	%xmm14, %xmm1
	movdqa	%xmm3, %xmm14
	punpcklwd	%xmm1, %xmm3
	punpckhwd	%xmm1, %xmm14
	movdqa	.LC57(%rip), %xmm1
	punpcklwd	%xmm14, %xmm3
	movdqa	.LC56(%rip), %xmm14
	paddd	%xmm0, %xmm1
	pand	%xmm4, %xmm3
	paddd	%xmm0, %xmm14
	movdqa	%xmm14, %xmm15
	punpcklwd	%xmm1, %xmm14
	punpckhwd	%xmm1, %xmm15
	movdqa	%xmm14, %xmm1
	punpcklwd	%xmm15, %xmm1
	punpckhwd	%xmm15, %xmm14
	movdqa	%xmm3, %xmm15
	punpcklwd	%xmm14, %xmm1
	pand	%xmm4, %xmm1
	packuswb	%xmm1, %xmm15
	movdqa	%xmm15, %xmm1
	paddb	%xmm2, %xmm1
	movups	%xmm1, 208(%rdi)
	cmpl	$14, %r8d
	je	.L127
	movdqa	.LC58(%rip), %xmm3
	movdqa	%xmm13, %xmm1
	paddd	%xmm0, %xmm3
	punpcklwd	%xmm3, %xmm13
	punpckhwd	%xmm3, %xmm1
	movdqa	%xmm13, %xmm3
	punpcklwd	%xmm1, %xmm13
	punpckhwd	%xmm1, %xmm3
	movdqa	.LC59(%rip), %xmm1
	punpcklwd	%xmm3, %xmm13
	paddd	%xmm0, %xmm1
	paddd	.LC60(%rip), %xmm0
	movdqa	%xmm1, %xmm3
	punpcklwd	%xmm0, %xmm1
	punpckhwd	%xmm0, %xmm3
	movdqa	%xmm1, %xmm0
	punpcklwd	%xmm3, %xmm1
	punpckhwd	%xmm3, %xmm0
	punpcklwd	%xmm0, %xmm1
	movdqa	%xmm13, %xmm0
	pand	%xmm4, %xmm0
	pand	%xmm4, %xmm1
	packuswb	%xmm1, %xmm0
	paddb	%xmm0, %xmm2
	movups	%xmm2, 224(%rdi)
	.p2align 4,,10
	.p2align 3
.L127:
	movl	%r9d, %edi
	andl	$-16, %edi
	addl	%edi, %eax
	cmpl	%edi, %r9d
	je	.L128
.L126:
	movslq	%eax, %rdi
	leal	(%rsi,%rax), %r8d
	movb	%r8b, -320(%rbp,%rdi)
	leal	1(%rax), %edi
	cmpl	%edi, %ecx
	jle	.L128
	movslq	%edi, %r8
	addl	%esi, %edi
	movb	%dil, -320(%rbp,%r8)
	leal	2(%rax), %edi
	cmpl	%edi, %ecx
	jle	.L128
	movslq	%edi, %r8
	addl	%esi, %edi
	movb	%dil, -320(%rbp,%r8)
	leal	3(%rax), %edi
	cmpl	%edi, %ecx
	jle	.L128
	movslq	%edi, %r8
	addl	%esi, %edi
	movb	%dil, -320(%rbp,%r8)
	leal	4(%rax), %edi
	cmpl	%edi, %ecx
	jle	.L128
	movslq	%edi, %r8
	addl	%esi, %edi
	movb	%dil, -320(%rbp,%r8)
	leal	5(%rax), %edi
	cmpl	%edi, %ecx
	jle	.L128
	movslq	%edi, %r8
	addl	%esi, %edi
	movb	%dil, -320(%rbp,%r8)
	leal	6(%rax), %edi
	cmpl	%edi, %ecx
	jle	.L128
	movslq	%edi, %r8
	addl	%esi, %edi
	movb	%dil, -320(%rbp,%r8)
	leal	7(%rax), %edi
	cmpl	%edi, %ecx
	jle	.L128
	movslq	%edi, %r8
	addl	%esi, %edi
	movb	%dil, -320(%rbp,%r8)
	leal	8(%rax), %edi
	cmpl	%edi, %ecx
	jle	.L128
	movslq	%edi, %r8
	addl	%esi, %edi
	movb	%dil, -320(%rbp,%r8)
	leal	9(%rax), %edi
	cmpl	%edi, %ecx
	jle	.L128
	movslq	%edi, %r8
	addl	%esi, %edi
	movb	%dil, -320(%rbp,%r8)
	leal	10(%rax), %edi
	cmpl	%edi, %ecx
	jle	.L128
	movslq	%edi, %r8
	addl	%esi, %edi
	movb	%dil, -320(%rbp,%r8)
	leal	11(%rax), %edi
	cmpl	%edi, %ecx
	jle	.L128
	movslq	%edi, %r8
	addl	%esi, %edi
	movb	%dil, -320(%rbp,%r8)
	leal	12(%rax), %edi
	cmpl	%edi, %ecx
	jle	.L128
	movslq	%edi, %r8
	addl	%esi, %edi
	movb	%dil, -320(%rbp,%r8)
	leal	13(%rax), %edi
	cmpl	%edi, %ecx
	jle	.L128
	movslq	%edi, %r8
	addl	$14, %eax
	addl	%esi, %edi
	movb	%dil, -320(%rbp,%r8)
	cmpl	%eax, %ecx
	jle	.L128
	movslq	%eax, %rdi
	addl	%esi, %eax
	movb	%al, -320(%rbp,%rdi)
	.p2align 4,,10
	.p2align 3
.L128:
	movl	%ecx, %eax
.L125:
	andl	$16711680, %esi
	je	.L129
	cmpl	$-1, %r10d
	movslq	%ecx, %rax
	cmove	%r12d, %r10d
	movb	$0, -320(%rbp,%rax)
	leal	1(%rcx), %eax
.L129:
	leaq	1(%rdx), %rcx
	cmpq	%rdx, %r13
	je	.L130
	movq	%rcx, %rdx
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L130:
	movq	-376(%rbp), %r12
	movq	-384(%rbp), %r13
	movq	-392(%rbp), %rbx
	cmpl	$256, %eax
	je	.L123
	movl	$256, %ecx
	movl	$1, %edx
	subl	%eax, %ecx
	cmpl	$255, %eax
	cmovg	%edx, %ecx
	movl	$255, %edx
	subl	%eax, %edx
	cmpl	$14, %edx
	jbe	.L132
.L121:
	movdqa	.LC1(%rip), %xmm1
	movd	%eax, %xmm7
	movdqa	.LC2(%rip), %xmm4
	movl	%ecx, %esi
	pshufd	$0, %xmm7, %xmm0
	movdqa	.LC4(%rip), %xmm5
	movslq	%eax, %rdx
	shrl	$4, %esi
	paddd	%xmm0, %xmm4
	paddd	%xmm0, %xmm1
	movdqa	.LC0(%rip), %xmm2
	addq	-368(%rbp), %rdx
	movdqa	%xmm1, %xmm3
	punpcklwd	%xmm4, %xmm1
	paddd	%xmm0, %xmm5
	punpckhwd	%xmm4, %xmm3
	movdqa	%xmm1, %xmm4
	paddd	%xmm0, %xmm2
	punpckhwd	%xmm3, %xmm4
	punpcklwd	%xmm3, %xmm1
	punpcklwd	%xmm4, %xmm1
	movdqa	.LC3(%rip), %xmm4
	paddd	%xmm0, %xmm4
	movdqa	%xmm4, %xmm3
	punpcklwd	%xmm5, %xmm4
	punpckhwd	%xmm5, %xmm3
	movdqa	%xmm4, %xmm5
	punpckhwd	%xmm3, %xmm5
	punpcklwd	%xmm3, %xmm4
	movdqa	.LC5(%rip), %xmm3
	punpcklwd	%xmm5, %xmm4
	pand	%xmm3, %xmm1
	pand	%xmm3, %xmm4
	packuswb	%xmm4, %xmm1
	movups	%xmm1, (%rdx)
	cmpl	$1, %esi
	je	.L133
	movdqa	.LC7(%rip), %xmm1
	movdqa	%xmm2, %xmm7
	movdqa	%xmm2, %xmm5
	movdqa	.LC9(%rip), %xmm6
	movdqa	.LC6(%rip), %xmm4
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm6
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm5
	paddd	%xmm0, %xmm4
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm2
	punpckhwd	%xmm5, %xmm2
	punpcklwd	%xmm5, %xmm1
	punpcklwd	%xmm2, %xmm1
	movdqa	.LC8(%rip), %xmm2
	pand	%xmm3, %xmm1
	paddd	%xmm0, %xmm2
	movdqa	%xmm2, %xmm5
	punpcklwd	%xmm6, %xmm2
	punpckhwd	%xmm6, %xmm5
	movdqa	%xmm2, %xmm6
	punpckhwd	%xmm5, %xmm6
	punpcklwd	%xmm5, %xmm2
	punpcklwd	%xmm6, %xmm2
	pand	%xmm3, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, 16(%rdx)
	cmpl	$2, %esi
	je	.L133
	movdqa	.LC11(%rip), %xmm1
	movdqa	%xmm4, %xmm7
	movdqa	%xmm4, %xmm2
	movdqa	.LC13(%rip), %xmm6
	movdqa	.LC10(%rip), %xmm5
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm6
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm2
	paddd	%xmm0, %xmm5
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm4
	punpckhwd	%xmm2, %xmm4
	punpcklwd	%xmm2, %xmm1
	movdqa	.LC12(%rip), %xmm2
	punpcklwd	%xmm4, %xmm1
	paddd	%xmm0, %xmm2
	pand	%xmm3, %xmm1
	movdqa	%xmm2, %xmm4
	punpcklwd	%xmm6, %xmm2
	punpckhwd	%xmm6, %xmm4
	movdqa	%xmm2, %xmm6
	punpckhwd	%xmm4, %xmm6
	punpcklwd	%xmm4, %xmm2
	punpcklwd	%xmm6, %xmm2
	pand	%xmm3, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, 32(%rdx)
	cmpl	$3, %esi
	je	.L133
	movdqa	.LC15(%rip), %xmm1
	movdqa	%xmm5, %xmm7
	movdqa	%xmm5, %xmm2
	movdqa	.LC17(%rip), %xmm6
	movdqa	.LC14(%rip), %xmm4
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm6
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm2
	paddd	%xmm0, %xmm4
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm5
	punpckhwd	%xmm2, %xmm5
	punpcklwd	%xmm2, %xmm1
	movdqa	.LC16(%rip), %xmm2
	punpcklwd	%xmm5, %xmm1
	paddd	%xmm0, %xmm2
	pand	%xmm3, %xmm1
	movdqa	%xmm2, %xmm5
	punpcklwd	%xmm6, %xmm2
	punpckhwd	%xmm6, %xmm5
	movdqa	%xmm2, %xmm6
	punpckhwd	%xmm5, %xmm6
	punpcklwd	%xmm5, %xmm2
	punpcklwd	%xmm6, %xmm2
	pand	%xmm3, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, 48(%rdx)
	cmpl	$4, %esi
	je	.L133
	movdqa	.LC19(%rip), %xmm1
	movdqa	%xmm4, %xmm7
	movdqa	%xmm4, %xmm2
	movdqa	.LC21(%rip), %xmm6
	movdqa	.LC18(%rip), %xmm5
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm6
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm2
	paddd	%xmm0, %xmm5
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm4
	punpckhwd	%xmm2, %xmm4
	punpcklwd	%xmm2, %xmm1
	movdqa	.LC20(%rip), %xmm2
	punpcklwd	%xmm4, %xmm1
	paddd	%xmm0, %xmm2
	pand	%xmm3, %xmm1
	movdqa	%xmm2, %xmm4
	punpcklwd	%xmm6, %xmm2
	punpckhwd	%xmm6, %xmm4
	movdqa	%xmm2, %xmm6
	punpckhwd	%xmm4, %xmm6
	punpcklwd	%xmm4, %xmm2
	punpcklwd	%xmm6, %xmm2
	pand	%xmm3, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, 64(%rdx)
	cmpl	$5, %esi
	je	.L133
	movdqa	.LC23(%rip), %xmm1
	movdqa	%xmm5, %xmm7
	movdqa	%xmm5, %xmm2
	movdqa	.LC25(%rip), %xmm6
	movdqa	.LC22(%rip), %xmm4
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm6
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm2
	paddd	%xmm0, %xmm4
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm5
	punpckhwd	%xmm2, %xmm5
	punpcklwd	%xmm2, %xmm1
	movdqa	.LC24(%rip), %xmm2
	punpcklwd	%xmm5, %xmm1
	paddd	%xmm0, %xmm2
	pand	%xmm3, %xmm1
	movdqa	%xmm2, %xmm5
	punpcklwd	%xmm6, %xmm2
	punpckhwd	%xmm6, %xmm5
	movdqa	%xmm2, %xmm6
	punpckhwd	%xmm5, %xmm6
	punpcklwd	%xmm5, %xmm2
	punpcklwd	%xmm6, %xmm2
	pand	%xmm3, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, 80(%rdx)
	cmpl	$6, %esi
	je	.L133
	movdqa	.LC27(%rip), %xmm1
	movdqa	%xmm4, %xmm7
	movdqa	%xmm4, %xmm5
	movdqa	.LC29(%rip), %xmm6
	movdqa	.LC26(%rip), %xmm2
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm6
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm5
	paddd	%xmm0, %xmm2
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm4
	punpckhwd	%xmm5, %xmm4
	punpcklwd	%xmm5, %xmm1
	punpcklwd	%xmm4, %xmm1
	movdqa	.LC28(%rip), %xmm4
	pand	%xmm3, %xmm1
	paddd	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	punpcklwd	%xmm6, %xmm4
	punpckhwd	%xmm6, %xmm5
	movdqa	%xmm4, %xmm6
	punpckhwd	%xmm5, %xmm6
	punpcklwd	%xmm5, %xmm4
	punpcklwd	%xmm6, %xmm4
	pand	%xmm3, %xmm4
	packuswb	%xmm4, %xmm1
	movups	%xmm1, 96(%rdx)
	cmpl	$7, %esi
	je	.L133
	movdqa	.LC31(%rip), %xmm1
	movdqa	%xmm2, %xmm7
	movdqa	%xmm2, %xmm5
	movdqa	.LC33(%rip), %xmm6
	movdqa	.LC30(%rip), %xmm4
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm6
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm5
	paddd	%xmm0, %xmm4
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm2
	punpckhwd	%xmm5, %xmm2
	punpcklwd	%xmm5, %xmm1
	punpcklwd	%xmm2, %xmm1
	movdqa	.LC32(%rip), %xmm2
	pand	%xmm3, %xmm1
	paddd	%xmm0, %xmm2
	movdqa	%xmm2, %xmm5
	punpcklwd	%xmm6, %xmm2
	punpckhwd	%xmm6, %xmm5
	movdqa	%xmm2, %xmm6
	punpckhwd	%xmm5, %xmm6
	punpcklwd	%xmm5, %xmm2
	punpcklwd	%xmm6, %xmm2
	pand	%xmm3, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, 112(%rdx)
	cmpl	$8, %esi
	je	.L133
	movdqa	.LC35(%rip), %xmm1
	movdqa	%xmm4, %xmm7
	movdqa	%xmm4, %xmm5
	movdqa	.LC37(%rip), %xmm6
	movdqa	.LC34(%rip), %xmm2
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm6
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm5
	paddd	%xmm0, %xmm2
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm4
	punpckhwd	%xmm5, %xmm4
	punpcklwd	%xmm5, %xmm1
	punpcklwd	%xmm4, %xmm1
	movdqa	.LC36(%rip), %xmm4
	pand	%xmm3, %xmm1
	paddd	%xmm0, %xmm4
	movdqa	%xmm4, %xmm5
	punpcklwd	%xmm6, %xmm4
	punpckhwd	%xmm6, %xmm5
	movdqa	%xmm4, %xmm6
	punpckhwd	%xmm5, %xmm6
	punpcklwd	%xmm5, %xmm4
	punpcklwd	%xmm6, %xmm4
	pand	%xmm3, %xmm4
	packuswb	%xmm4, %xmm1
	movups	%xmm1, 128(%rdx)
	cmpl	$9, %esi
	je	.L133
	movdqa	.LC39(%rip), %xmm1
	movdqa	%xmm2, %xmm7
	movdqa	%xmm2, %xmm5
	movdqa	.LC41(%rip), %xmm6
	movdqa	.LC38(%rip), %xmm4
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm6
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm5
	paddd	%xmm0, %xmm4
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm2
	punpckhwd	%xmm5, %xmm2
	punpcklwd	%xmm5, %xmm1
	punpcklwd	%xmm2, %xmm1
	movdqa	.LC40(%rip), %xmm2
	pand	%xmm3, %xmm1
	paddd	%xmm0, %xmm2
	movdqa	%xmm2, %xmm5
	punpcklwd	%xmm6, %xmm2
	punpckhwd	%xmm6, %xmm5
	movdqa	%xmm2, %xmm6
	punpckhwd	%xmm5, %xmm6
	punpcklwd	%xmm5, %xmm2
	punpcklwd	%xmm6, %xmm2
	pand	%xmm3, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, 144(%rdx)
	cmpl	$10, %esi
	je	.L133
	movdqa	.LC43(%rip), %xmm1
	movdqa	%xmm4, %xmm7
	movdqa	%xmm4, %xmm2
	movdqa	.LC45(%rip), %xmm6
	movdqa	.LC42(%rip), %xmm5
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm6
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm2
	paddd	%xmm0, %xmm5
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm4
	punpckhwd	%xmm2, %xmm4
	punpcklwd	%xmm2, %xmm1
	movdqa	.LC44(%rip), %xmm2
	punpcklwd	%xmm4, %xmm1
	paddd	%xmm0, %xmm2
	pand	%xmm3, %xmm1
	movdqa	%xmm2, %xmm4
	punpcklwd	%xmm6, %xmm2
	punpckhwd	%xmm6, %xmm4
	movdqa	%xmm2, %xmm6
	punpckhwd	%xmm4, %xmm6
	punpcklwd	%xmm4, %xmm2
	punpcklwd	%xmm6, %xmm2
	pand	%xmm3, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, 160(%rdx)
	cmpl	$11, %esi
	je	.L133
	movdqa	.LC47(%rip), %xmm1
	movdqa	%xmm5, %xmm7
	movdqa	%xmm5, %xmm4
	movdqa	.LC49(%rip), %xmm6
	movdqa	.LC46(%rip), %xmm2
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm6
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm4
	paddd	%xmm0, %xmm2
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm5
	punpckhwd	%xmm4, %xmm5
	punpcklwd	%xmm4, %xmm1
	movdqa	.LC48(%rip), %xmm4
	punpcklwd	%xmm5, %xmm1
	paddd	%xmm0, %xmm4
	pand	%xmm3, %xmm1
	movdqa	%xmm4, %xmm5
	punpcklwd	%xmm6, %xmm4
	punpckhwd	%xmm6, %xmm5
	movdqa	%xmm4, %xmm6
	punpckhwd	%xmm5, %xmm6
	punpcklwd	%xmm5, %xmm4
	punpcklwd	%xmm6, %xmm4
	pand	%xmm3, %xmm4
	packuswb	%xmm4, %xmm1
	movups	%xmm1, 176(%rdx)
	cmpl	$12, %esi
	je	.L133
	movdqa	.LC51(%rip), %xmm5
	movdqa	%xmm2, %xmm4
	movdqa	.LC53(%rip), %xmm6
	movdqa	.LC50(%rip), %xmm1
	paddd	%xmm0, %xmm5
	paddd	%xmm0, %xmm6
	punpcklwd	%xmm5, %xmm2
	punpckhwd	%xmm5, %xmm4
	paddd	%xmm0, %xmm1
	movdqa	%xmm2, %xmm5
	punpcklwd	%xmm4, %xmm2
	punpckhwd	%xmm4, %xmm5
	movdqa	.LC52(%rip), %xmm4
	punpcklwd	%xmm5, %xmm2
	paddd	%xmm0, %xmm4
	pand	%xmm3, %xmm2
	movdqa	%xmm4, %xmm5
	punpcklwd	%xmm6, %xmm4
	punpckhwd	%xmm6, %xmm5
	movdqa	%xmm4, %xmm6
	punpckhwd	%xmm5, %xmm6
	punpcklwd	%xmm5, %xmm4
	punpcklwd	%xmm6, %xmm4
	pand	%xmm3, %xmm4
	packuswb	%xmm4, %xmm2
	movups	%xmm2, 192(%rdx)
	cmpl	$13, %esi
	je	.L133
	movdqa	.LC55(%rip), %xmm5
	movdqa	%xmm1, %xmm4
	movdqa	.LC57(%rip), %xmm6
	movdqa	.LC54(%rip), %xmm2
	paddd	%xmm0, %xmm5
	paddd	%xmm0, %xmm6
	punpcklwd	%xmm5, %xmm1
	punpckhwd	%xmm5, %xmm4
	paddd	%xmm0, %xmm2
	movdqa	%xmm1, %xmm5
	punpcklwd	%xmm4, %xmm1
	punpckhwd	%xmm4, %xmm5
	movdqa	.LC56(%rip), %xmm4
	punpcklwd	%xmm5, %xmm1
	paddd	%xmm0, %xmm4
	pand	%xmm3, %xmm1
	movdqa	%xmm4, %xmm5
	punpcklwd	%xmm6, %xmm4
	punpckhwd	%xmm6, %xmm5
	movdqa	%xmm4, %xmm6
	punpckhwd	%xmm5, %xmm6
	punpcklwd	%xmm5, %xmm4
	punpcklwd	%xmm6, %xmm4
	pand	%xmm3, %xmm4
	packuswb	%xmm4, %xmm1
	movups	%xmm1, 208(%rdx)
	cmpl	$14, %esi
	je	.L133
	movdqa	.LC58(%rip), %xmm5
	movdqa	%xmm2, %xmm4
	movdqa	.LC60(%rip), %xmm6
	movdqa	.LC61(%rip), %xmm1
	paddd	%xmm0, %xmm5
	paddd	%xmm0, %xmm6
	punpcklwd	%xmm5, %xmm2
	punpckhwd	%xmm5, %xmm4
	paddd	%xmm0, %xmm1
	movdqa	%xmm2, %xmm5
	punpcklwd	%xmm4, %xmm2
	punpckhwd	%xmm4, %xmm5
	movdqa	.LC59(%rip), %xmm4
	punpcklwd	%xmm5, %xmm2
	paddd	%xmm0, %xmm4
	pand	%xmm3, %xmm2
	movdqa	%xmm4, %xmm5
	punpcklwd	%xmm6, %xmm4
	punpckhwd	%xmm6, %xmm5
	movdqa	%xmm4, %xmm6
	punpckhwd	%xmm5, %xmm6
	punpcklwd	%xmm5, %xmm4
	punpcklwd	%xmm6, %xmm4
	pand	%xmm3, %xmm4
	packuswb	%xmm4, %xmm2
	movups	%xmm2, 224(%rdx)
	cmpl	$15, %esi
	je	.L133
	movdqa	.LC62(%rip), %xmm4
	movdqa	%xmm1, %xmm2
	paddd	%xmm0, %xmm4
	punpcklwd	%xmm4, %xmm1
	punpckhwd	%xmm4, %xmm2
	movdqa	%xmm1, %xmm4
	punpcklwd	%xmm2, %xmm1
	punpckhwd	%xmm2, %xmm4
	movdqa	.LC63(%rip), %xmm2
	punpcklwd	%xmm4, %xmm1
	paddd	%xmm0, %xmm2
	paddd	.LC64(%rip), %xmm0
	movdqa	%xmm2, %xmm4
	punpcklwd	%xmm0, %xmm2
	punpckhwd	%xmm0, %xmm4
	movdqa	%xmm2, %xmm0
	punpcklwd	%xmm4, %xmm2
	punpckhwd	%xmm4, %xmm0
	punpcklwd	%xmm0, %xmm2
	movdqa	%xmm1, %xmm0
	pand	%xmm3, %xmm0
	pand	%xmm2, %xmm3
	packuswb	%xmm3, %xmm0
	movups	%xmm0, 240(%rdx)
	.p2align 4,,10
	.p2align 3
.L133:
	movl	%ecx, %edx
	andl	$-16, %edx
	addl	%edx, %eax
	cmpl	%ecx, %edx
	je	.L123
.L132:
	movslq	%eax, %rdx
	movb	%al, -320(%rbp,%rdx)
	leal	1(%rax), %edx
	cmpl	$255, %eax
	je	.L123
	movslq	%edx, %rcx
	movb	%dl, -320(%rbp,%rcx)
	leal	2(%rax), %edx
	cmpl	$254, %eax
	je	.L123
	movslq	%edx, %rcx
	movb	%dl, -320(%rbp,%rcx)
	leal	3(%rax), %edx
	cmpl	$253, %eax
	je	.L123
	movslq	%edx, %rcx
	movb	%dl, -320(%rbp,%rcx)
	leal	4(%rax), %edx
	cmpl	$252, %eax
	je	.L123
	movslq	%edx, %rcx
	movb	%dl, -320(%rbp,%rcx)
	leal	5(%rax), %edx
	cmpl	$251, %eax
	je	.L123
	movslq	%edx, %rcx
	movb	%dl, -320(%rbp,%rcx)
	leal	6(%rax), %edx
	cmpl	$250, %eax
	je	.L123
	movslq	%edx, %rcx
	movb	%dl, -320(%rbp,%rcx)
	leal	7(%rax), %edx
	cmpl	$249, %eax
	je	.L123
	movslq	%edx, %rcx
	movb	%dl, -320(%rbp,%rcx)
	leal	8(%rax), %edx
	cmpl	$248, %eax
	je	.L123
	movslq	%edx, %rcx
	movb	%dl, -320(%rbp,%rcx)
	leal	9(%rax), %edx
	cmpl	$247, %eax
	je	.L123
	movslq	%edx, %rcx
	movb	%dl, -320(%rbp,%rcx)
	leal	10(%rax), %edx
	cmpl	$246, %eax
	je	.L123
	movslq	%edx, %rcx
	movb	%dl, -320(%rbp,%rcx)
	leal	11(%rax), %edx
	cmpl	$245, %eax
	je	.L123
	movslq	%edx, %rcx
	movb	%dl, -320(%rbp,%rcx)
	leal	12(%rax), %edx
	cmpl	$244, %eax
	je	.L123
	movslq	%edx, %rcx
	movb	%dl, -320(%rbp,%rcx)
	leal	13(%rax), %edx
	cmpl	$243, %eax
	je	.L123
	movslq	%edx, %rcx
	movb	%dl, -320(%rbp,%rcx)
	leal	14(%rax), %edx
	cmpl	$242, %eax
	je	.L123
	movslq	%edx, %rax
	movb	%dl, -320(%rbp,%rax)
	.p2align 4,,10
	.p2align 3
.L123:
	xorl	%r8d, %r8d
	cmpl	$-1, %r10d
	je	.L135
	movl	-360(%rbp), %r8d
	movslq	%r10d, %rax
	leaq	(%r11,%rax,4), %r11
	subl	%r10d, %r8d
.L135:
	subq	$8, %rsp
	movl	%r15d, %edx
	movq	%r11, %rcx
	movq	%r13, %rsi
	pushq	%rbx
	movq	-368(%rbp), %r9
	movq	%r12, %rdi
	call	_ZN6icu_6717CollationSettings16setReorderArraysEPKiiPKjiPKhR10UErrorCode.part.0
	movq	%r14, %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	popq	%rax
	popq	%rdx
	.p2align 4,,10
	.p2align 3
.L115:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L284
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	movq	$0, 32(%r12)
	movl	$0, 40(%r12)
	movl	$0, 56(%r12)
	movl	$0, 72(%r12)
	jmp	.L115
.L282:
	movq	$0, 32(%r12)
	movl	$0, 40(%r12)
	movl	$0, 56(%r12)
	movl	$0, 72(%r12)
.L119:
	movq	%r14, %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	jmp	.L115
.L283:
	leaq	-320(%rbp), %rdx
	movl	$256, %ecx
	movl	$-1, %r10d
	xorl	%eax, %eax
	movq	%rdx, -368(%rbp)
	jmp	.L121
.L284:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3281:
	.size	_ZN6icu_6717CollationSettings13setReorderingERKNS_13CollationDataEPKiiR10UErrorCode, .-_ZN6icu_6717CollationSettings13setReorderingERKNS_13CollationDataEPKiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationSettings15aliasReorderingERKNS_13CollationDataEPKiiPKjiPKhR10UErrorCode
	.type	_ZN6icu_6717CollationSettings15aliasReorderingERKNS_13CollationDataEPKiiPKjiPKhR10UErrorCode, @function
_ZN6icu_6717CollationSettings15aliasReorderingERKNS_13CollationDataEPKiiPKjiPKhR10UErrorCode:
.LFB3280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rbp), %r8
	movq	16(%rbp), %rbx
	movl	(%r8), %r10d
	testl	%r10d, %r10d
	jg	.L285
	movq	%rdi, %r12
	testq	%rbx, %rbx
	je	.L287
	testl	%r9d, %r9d
	jne	.L288
	leaq	1(%rbx), %rax
	leaq	256(%rbx), %rdi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L306:
	addq	$1, %rax
	cmpq	%rax, %rdi
	je	.L305
.L290:
	cmpb	$0, (%rax)
	jne	.L306
.L287:
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717CollationSettings13setReorderingERKNS_13CollationDataEPKiiR10UErrorCode
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	jne	.L307
.L294:
	movl	$0, 40(%r12)
	movq	$0, 48(%r12)
	movl	$0, 56(%r12)
.L285:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	cmpl	$1, %r9d
	jle	.L287
	cmpw	$0, 0(%r13)
	jne	.L287
	movslq	%r9d, %r14
	cmpw	$0, -4(%r13,%r14,4)
	je	.L287
	movl	76(%rdi), %esi
	testl	%esi, %esi
	jne	.L308
	movq	%rbx, 32(%r12)
	movq	%rdx, 64(%r12)
	movl	%ecx, 72(%r12)
.L295:
	movq	%r13, %rax
	xorl	%edx, %edx
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L310:
	addl	$1, %edx
	addq	$4, %rax
	cmpl	%edx, %r9d
	jle	.L309
.L293:
	testl	$16711680, (%rax)
	je	.L310
.L292:
	movl	-4(%r13,%r14,4), %ecx
	subl	%edx, %r9d
	movq	%rax, 48(%r12)
	movl	%r9d, 56(%r12)
	xorw	%cx, %cx
	movl	%ecx, 40(%r12)
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L305:
	movl	76(%r12), %eax
	testl	%eax, %eax
	je	.L298
	movq	64(%r12), %rdi
	movl	%ecx, -44(%rbp)
	movq	%rdx, -40(%rbp)
	call	uprv_free_67@PLT
	movq	-40(%rbp), %rdx
	movl	-44(%rbp), %ecx
	movl	$0, 76(%r12)
	movq	%rbx, 32(%r12)
	movq	%rdx, 64(%r12)
	movl	%ecx, 72(%r12)
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L307:
	movslq	%edx, %rax
	leaq	0(%r13,%rax,4), %rax
	jmp	.L292
.L298:
	movq	%rbx, 32(%r12)
	movq	%rdx, 64(%r12)
	movl	%ecx, 72(%r12)
	jmp	.L294
.L308:
	movq	64(%rdi), %rdi
	movl	%r9d, -48(%rbp)
	movl	%ecx, -44(%rbp)
	movq	%rdx, -40(%rbp)
	call	uprv_free_67@PLT
	movq	-40(%rbp), %rdx
	movl	-44(%rbp), %ecx
	movl	$0, 76(%r12)
	movq	%rbx, 32(%r12)
	movl	-48(%rbp), %r9d
	movq	%rdx, 64(%r12)
	movl	%ecx, 72(%r12)
	jmp	.L295
	.cfi_endproc
.LFE3280:
	.size	_ZN6icu_6717CollationSettings15aliasReorderingERKNS_13CollationDataEPKiiPKjiPKhR10UErrorCode, .-_ZN6icu_6717CollationSettings15aliasReorderingERKNS_13CollationDataEPKiiPKjiPKhR10UErrorCode
	.weak	_ZTSN6icu_6717CollationSettingsE
	.section	.rodata._ZTSN6icu_6717CollationSettingsE,"aG",@progbits,_ZTSN6icu_6717CollationSettingsE,comdat
	.align 16
	.type	_ZTSN6icu_6717CollationSettingsE, @object
	.size	_ZTSN6icu_6717CollationSettingsE, 29
_ZTSN6icu_6717CollationSettingsE:
	.string	"N6icu_6717CollationSettingsE"
	.weak	_ZTIN6icu_6717CollationSettingsE
	.section	.data.rel.ro._ZTIN6icu_6717CollationSettingsE,"awG",@progbits,_ZTIN6icu_6717CollationSettingsE,comdat
	.align 8
	.type	_ZTIN6icu_6717CollationSettingsE, @object
	.size	_ZTIN6icu_6717CollationSettingsE, 24
_ZTIN6icu_6717CollationSettingsE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717CollationSettingsE
	.quad	_ZTIN6icu_6712SharedObjectE
	.weak	_ZTVN6icu_6717CollationSettingsE
	.section	.data.rel.ro._ZTVN6icu_6717CollationSettingsE,"awG",@progbits,_ZTVN6icu_6717CollationSettingsE,comdat
	.align 8
	.type	_ZTVN6icu_6717CollationSettingsE, @object
	.size	_ZTVN6icu_6717CollationSettingsE, 40
_ZTVN6icu_6717CollationSettingsE:
	.quad	0
	.quad	_ZTIN6icu_6717CollationSettingsE
	.quad	_ZN6icu_6717CollationSettingsD1Ev
	.quad	_ZN6icu_6717CollationSettingsD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	16
	.long	17
	.long	18
	.long	19
	.align 16
.LC1:
	.long	0
	.long	1
	.long	2
	.long	3
	.align 16
.LC2:
	.long	4
	.long	5
	.long	6
	.long	7
	.align 16
.LC3:
	.long	8
	.long	9
	.long	10
	.long	11
	.align 16
.LC4:
	.long	12
	.long	13
	.long	14
	.long	15
	.align 16
.LC5:
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.align 16
.LC6:
	.long	32
	.long	33
	.long	34
	.long	35
	.align 16
.LC7:
	.long	20
	.long	21
	.long	22
	.long	23
	.align 16
.LC8:
	.long	24
	.long	25
	.long	26
	.long	27
	.align 16
.LC9:
	.long	28
	.long	29
	.long	30
	.long	31
	.align 16
.LC10:
	.long	48
	.long	49
	.long	50
	.long	51
	.align 16
.LC11:
	.long	36
	.long	37
	.long	38
	.long	39
	.align 16
.LC12:
	.long	40
	.long	41
	.long	42
	.long	43
	.align 16
.LC13:
	.long	44
	.long	45
	.long	46
	.long	47
	.align 16
.LC14:
	.long	64
	.long	65
	.long	66
	.long	67
	.align 16
.LC15:
	.long	52
	.long	53
	.long	54
	.long	55
	.align 16
.LC16:
	.long	56
	.long	57
	.long	58
	.long	59
	.align 16
.LC17:
	.long	60
	.long	61
	.long	62
	.long	63
	.align 16
.LC18:
	.long	80
	.long	81
	.long	82
	.long	83
	.align 16
.LC19:
	.long	68
	.long	69
	.long	70
	.long	71
	.align 16
.LC20:
	.long	72
	.long	73
	.long	74
	.long	75
	.align 16
.LC21:
	.long	76
	.long	77
	.long	78
	.long	79
	.align 16
.LC22:
	.long	96
	.long	97
	.long	98
	.long	99
	.align 16
.LC23:
	.long	84
	.long	85
	.long	86
	.long	87
	.align 16
.LC24:
	.long	88
	.long	89
	.long	90
	.long	91
	.align 16
.LC25:
	.long	92
	.long	93
	.long	94
	.long	95
	.align 16
.LC26:
	.long	112
	.long	113
	.long	114
	.long	115
	.align 16
.LC27:
	.long	100
	.long	101
	.long	102
	.long	103
	.align 16
.LC28:
	.long	104
	.long	105
	.long	106
	.long	107
	.align 16
.LC29:
	.long	108
	.long	109
	.long	110
	.long	111
	.align 16
.LC30:
	.long	128
	.long	129
	.long	130
	.long	131
	.align 16
.LC31:
	.long	116
	.long	117
	.long	118
	.long	119
	.align 16
.LC32:
	.long	120
	.long	121
	.long	122
	.long	123
	.align 16
.LC33:
	.long	124
	.long	125
	.long	126
	.long	127
	.align 16
.LC34:
	.long	144
	.long	145
	.long	146
	.long	147
	.align 16
.LC35:
	.long	132
	.long	133
	.long	134
	.long	135
	.align 16
.LC36:
	.long	136
	.long	137
	.long	138
	.long	139
	.align 16
.LC37:
	.long	140
	.long	141
	.long	142
	.long	143
	.align 16
.LC38:
	.long	160
	.long	161
	.long	162
	.long	163
	.align 16
.LC39:
	.long	148
	.long	149
	.long	150
	.long	151
	.align 16
.LC40:
	.long	152
	.long	153
	.long	154
	.long	155
	.align 16
.LC41:
	.long	156
	.long	157
	.long	158
	.long	159
	.align 16
.LC42:
	.long	176
	.long	177
	.long	178
	.long	179
	.align 16
.LC43:
	.long	164
	.long	165
	.long	166
	.long	167
	.align 16
.LC44:
	.long	168
	.long	169
	.long	170
	.long	171
	.align 16
.LC45:
	.long	172
	.long	173
	.long	174
	.long	175
	.align 16
.LC46:
	.long	192
	.long	193
	.long	194
	.long	195
	.align 16
.LC47:
	.long	180
	.long	181
	.long	182
	.long	183
	.align 16
.LC48:
	.long	184
	.long	185
	.long	186
	.long	187
	.align 16
.LC49:
	.long	188
	.long	189
	.long	190
	.long	191
	.align 16
.LC50:
	.long	208
	.long	209
	.long	210
	.long	211
	.align 16
.LC51:
	.long	196
	.long	197
	.long	198
	.long	199
	.align 16
.LC52:
	.long	200
	.long	201
	.long	202
	.long	203
	.align 16
.LC53:
	.long	204
	.long	205
	.long	206
	.long	207
	.align 16
.LC54:
	.long	224
	.long	225
	.long	226
	.long	227
	.align 16
.LC55:
	.long	212
	.long	213
	.long	214
	.long	215
	.align 16
.LC56:
	.long	216
	.long	217
	.long	218
	.long	219
	.align 16
.LC57:
	.long	220
	.long	221
	.long	222
	.long	223
	.align 16
.LC58:
	.long	228
	.long	229
	.long	230
	.long	231
	.align 16
.LC59:
	.long	232
	.long	233
	.long	234
	.long	235
	.align 16
.LC60:
	.long	236
	.long	237
	.long	238
	.long	239
	.align 16
.LC61:
	.long	240
	.long	241
	.long	242
	.long	243
	.align 16
.LC62:
	.long	244
	.long	245
	.long	246
	.long	247
	.align 16
.LC63:
	.long	248
	.long	249
	.long	250
	.long	251
	.align 16
.LC64:
	.long	252
	.long	253
	.long	254
	.long	255
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
