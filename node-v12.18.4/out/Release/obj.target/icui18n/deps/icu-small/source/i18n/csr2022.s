	.file	"csr2022.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ISO-2022-JP"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_2022JP7getNameEv
	.type	_ZNK6icu_6719CharsetRecog_2022JP7getNameEv, @function
_ZNK6icu_6719CharsetRecog_2022JP7getNameEv:
.LFB2058:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE2058:
	.size	_ZNK6icu_6719CharsetRecog_2022JP7getNameEv, .-_ZNK6icu_6719CharsetRecog_2022JP7getNameEv
	.section	.rodata.str1.1
.LC1:
	.string	"ISO-2022-KR"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_2022KR7getNameEv
	.type	_ZNK6icu_6719CharsetRecog_2022KR7getNameEv, @function
_ZNK6icu_6719CharsetRecog_2022KR7getNameEv:
.LFB2064:
	.cfi_startproc
	endbr64
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
.LFE2064:
	.size	_ZNK6icu_6719CharsetRecog_2022KR7getNameEv, .-_ZNK6icu_6719CharsetRecog_2022KR7getNameEv
	.section	.rodata.str1.1
.LC2:
	.string	"ISO-2022-CN"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_2022CN7getNameEv
	.type	_ZNK6icu_6719CharsetRecog_2022CN7getNameEv, @function
_ZNK6icu_6719CharsetRecog_2022CN7getNameEv:
.LFB2070:
	.cfi_startproc
	endbr64
	leaq	.LC2(%rip), %rax
	ret
	.cfi_endproc
.LFE2070:
	.size	_ZNK6icu_6719CharsetRecog_2022CN7getNameEv, .-_ZNK6icu_6719CharsetRecog_2022CN7getNameEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_2022JPD2Ev
	.type	_ZN6icu_6719CharsetRecog_2022JPD2Ev, @function
_ZN6icu_6719CharsetRecog_2022JPD2Ev:
.LFB2055:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_2022E(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2055:
	.size	_ZN6icu_6719CharsetRecog_2022JPD2Ev, .-_ZN6icu_6719CharsetRecog_2022JPD2Ev
	.globl	_ZN6icu_6719CharsetRecog_2022JPD1Ev
	.set	_ZN6icu_6719CharsetRecog_2022JPD1Ev,_ZN6icu_6719CharsetRecog_2022JPD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_2022JPD0Ev
	.type	_ZN6icu_6719CharsetRecog_2022JPD0Ev, @function
_ZN6icu_6719CharsetRecog_2022JPD0Ev:
.LFB2057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_2022E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2057:
	.size	_ZN6icu_6719CharsetRecog_2022JPD0Ev, .-_ZN6icu_6719CharsetRecog_2022JPD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_2022KRD2Ev
	.type	_ZN6icu_6719CharsetRecog_2022KRD2Ev, @function
_ZN6icu_6719CharsetRecog_2022KRD2Ev:
.LFB2061:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_2022E(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2061:
	.size	_ZN6icu_6719CharsetRecog_2022KRD2Ev, .-_ZN6icu_6719CharsetRecog_2022KRD2Ev
	.globl	_ZN6icu_6719CharsetRecog_2022KRD1Ev
	.set	_ZN6icu_6719CharsetRecog_2022KRD1Ev,_ZN6icu_6719CharsetRecog_2022KRD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_2022KRD0Ev
	.type	_ZN6icu_6719CharsetRecog_2022KRD0Ev, @function
_ZN6icu_6719CharsetRecog_2022KRD0Ev:
.LFB2063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_2022E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2063:
	.size	_ZN6icu_6719CharsetRecog_2022KRD0Ev, .-_ZN6icu_6719CharsetRecog_2022KRD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_2022CND2Ev
	.type	_ZN6icu_6719CharsetRecog_2022CND2Ev, @function
_ZN6icu_6719CharsetRecog_2022CND2Ev:
.LFB2067:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_2022E(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2067:
	.size	_ZN6icu_6719CharsetRecog_2022CND2Ev, .-_ZN6icu_6719CharsetRecog_2022CND2Ev
	.globl	_ZN6icu_6719CharsetRecog_2022CND1Ev
	.set	_ZN6icu_6719CharsetRecog_2022CND1Ev,_ZN6icu_6719CharsetRecog_2022CND2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_2022CND0Ev
	.type	_ZN6icu_6719CharsetRecog_2022CND0Ev, @function
_ZN6icu_6719CharsetRecog_2022CND0Ev:
.LFB2069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_2022E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2069:
	.size	_ZN6icu_6719CharsetRecog_2022CND0Ev, .-_ZN6icu_6719CharsetRecog_2022CND0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_2022JP5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6719CharsetRecog_2022JP5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6719CharsetRecog_2022JP5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movl	8(%rsi), %r8d
	movq	%rax, -56(%rbp)
	testl	%r8d, %r8d
	jle	.L25
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r13, -88(%rbp)
	xorl	%r12d, %r12d
	movq	%rdi, -80(%rbp)
	xorl	%ebx, %ebx
	movl	%r8d, %r14d
	movl	%esi, %r13d
	movq	%rdx, -96(%rbp)
	movl	%ecx, %r15d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L16:
	subl	$14, %eax
	cmpb	$2, %al
	adcl	$0, %r15d
.L20:
	addl	$1, %r12d
	cmpl	%r12d, %r14d
	jle	.L33
.L23:
	movslq	%r12d, %rdx
	addq	-56(%rbp), %rdx
	movzbl	(%rdx), %eax
	cmpb	$27, %al
	jne	.L16
	movl	%r14d, %eax
	movl	%r13d, %ecx
	leaq	_ZN6icu_67L22escapeSequences_2022JPE(%rip), %rdi
	movl	%r12d, %r13d
	subl	%r12d, %eax
	movl	%r15d, %esi
	movq	%rdx, %r12
	movl	%ebx, %r15d
	movl	%eax, -60(%rbp)
	movq	%rdi, %rbx
	movl	$4, %eax
	movl	%ecx, %edx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L17:
	addq	$5, %rbx
	leaq	60+_ZN6icu_67L22escapeSequences_2022JPE(%rip), %rax
	cmpq	%rbx, %rax
	je	.L21
	movq	%rbx, %rdi
	movl	%esi, -68(%rbp)
	movl	%edx, -64(%rbp)
	call	strlen@PLT
	movl	-64(%rbp), %edx
	movl	-68(%rbp), %esi
.L22:
	movl	%eax, %r10d
	cmpl	-60(%rbp), %eax
	jg	.L17
	cmpl	$1, %eax
	jle	.L30
	leal	-2(%rax), %ecx
	movl	$1, %eax
	addq	$2, %rcx
	.p2align 4,,10
	.p2align 3
.L19:
	movzbl	(%r12,%rax), %edi
	cmpb	%dil, (%rbx,%rax)
	jne	.L17
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L19
.L30:
	movl	%r13d, %r12d
	movl	%r15d, %ebx
	movl	%edx, %r13d
	movl	%esi, %r15d
	leal	-1(%r12,%r10), %r12d
	addl	$1, %ebx
	addl	$1, %r12d
	cmpl	%r12d, %r14d
	jg	.L23
	.p2align 4,,10
	.p2align 3
.L33:
	movl	%r13d, %esi
	movl	%r15d, %ecx
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %r13
	movq	-96(%rbp), %r15
	movl	%ebx, %r12d
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	je	.L15
	movl	%ebx, %eax
	addl	%ebx, %ecx
	subl	%esi, %eax
	addl	%ebx, %esi
	imull	$100, %eax, %eax
	cltd
	idivl	%esi
	cmpl	$4, %ecx
	jle	.L34
.L24:
	testl	%eax, %eax
	movl	$0, %r12d
	cmovns	%eax, %r12d
	setg	%r14b
.L15:
	movl	%r12d, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r11, %rdx
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	addq	$56, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movl	$5, %edx
	subl	%ecx, %edx
	imull	$-10, %edx, %edx
	addl	%edx, %eax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%r13d, %r12d
	movl	%edx, %r13d
	movl	%r15d, %ebx
	movl	%esi, %r15d
	addl	$1, %r13d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L15
	.cfi_endproc
.LFE2059:
	.size	_ZNK6icu_6719CharsetRecog_2022JP5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6719CharsetRecog_2022JP5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_2022KR5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6719CharsetRecog_2022KR5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6719CharsetRecog_2022KR5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	(%rsi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	8(%rsi), %ebx
	testl	%ebx, %ebx
	jle	.L42
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L37:
	subl	$14, %r8d
	cmpb	$2, %r8b
	adcl	$0, %r9d
.L39:
	cmpl	%r11d, %ebx
	jle	.L47
.L43:
	movl	%r11d, %eax
.L40:
	movslq	%eax, %r8
	leal	1(%rax), %r11d
	movzbl	(%r12,%r8), %r8d
	cmpb	$27, %r8b
	jne	.L37
	movl	%ebx, %r8d
	subl	%eax, %r8d
	cmpl	$3, %r8d
	jg	.L48
.L38:
	addl	$1, %edx
	cmpl	%r11d, %ebx
	jg	.L43
	.p2align 4,,10
	.p2align 3
.L47:
	xorl	%r12d, %r12d
	testl	%ecx, %ecx
	je	.L36
	movl	%ecx, %eax
	leal	(%rcx,%rdx), %r8d
	addl	%r9d, %ecx
	subl	%edx, %eax
	imull	$100, %eax, %eax
	cltd
	idivl	%r8d
	cmpl	$4, %ecx
	jle	.L49
.L41:
	testl	%eax, %eax
	movl	$0, %ecx
	cmovns	%eax, %ecx
	setg	%r12b
.L36:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r10, %rdx
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movl	$5, %edx
	subl	%ecx, %edx
	imull	$-10, %edx, %edx
	addl	%edx, %eax
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L48:
	movslq	%r11d, %r8
	cmpb	$36, (%r12,%r8)
	jne	.L38
	leal	2(%rax), %r8d
	movslq	%r8d, %r8
	cmpb	$41, (%r12,%r8)
	jne	.L38
	leal	3(%rax), %r8d
	movslq	%r8d, %r8
	cmpb	$67, (%r12,%r8)
	jne	.L38
	addl	$1, %ecx
	leal	4(%rax), %r11d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
	jmp	.L36
	.cfi_endproc
.LFE2065:
	.size	_ZNK6icu_6719CharsetRecog_2022KR5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6719CharsetRecog_2022KR5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_2022CN5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6719CharsetRecog_2022CN5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6719CharsetRecog_2022CN5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movl	8(%rsi), %r8d
	movq	%rax, -56(%rbp)
	testl	%r8d, %r8d
	jle	.L61
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r13, -88(%rbp)
	xorl	%r12d, %r12d
	movq	%rdi, -80(%rbp)
	xorl	%ebx, %ebx
	movl	%r8d, %r14d
	movl	%esi, %r13d
	movq	%rdx, -96(%rbp)
	movl	%ecx, %r15d
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L52:
	subl	$14, %eax
	cmpb	$2, %al
	adcl	$0, %r15d
.L56:
	addl	$1, %r12d
	cmpl	%r12d, %r14d
	jle	.L69
.L59:
	movslq	%r12d, %rdx
	addq	-56(%rbp), %rdx
	movzbl	(%rdx), %eax
	cmpb	$27, %al
	jne	.L52
	movl	%r14d, %eax
	movl	%r13d, %ecx
	leaq	_ZN6icu_67L22escapeSequences_2022CNE(%rip), %rdi
	movl	%r12d, %r13d
	subl	%r12d, %eax
	movl	%r15d, %esi
	movq	%rdx, %r12
	movl	%ebx, %r15d
	movl	%eax, -60(%rbp)
	movq	%rdi, %rbx
	movl	$4, %eax
	movl	%ecx, %edx
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L53:
	addq	$5, %rbx
	leaq	55+_ZN6icu_67L22escapeSequences_2022CNE(%rip), %rax
	cmpq	%rbx, %rax
	je	.L57
	movq	%rbx, %rdi
	movl	%esi, -68(%rbp)
	movl	%edx, -64(%rbp)
	call	strlen@PLT
	movl	-64(%rbp), %edx
	movl	-68(%rbp), %esi
.L58:
	movl	%eax, %r10d
	cmpl	-60(%rbp), %eax
	jg	.L53
	cmpl	$1, %eax
	jle	.L66
	leal	-2(%rax), %ecx
	movl	$1, %eax
	addq	$2, %rcx
	.p2align 4,,10
	.p2align 3
.L55:
	movzbl	(%r12,%rax), %edi
	cmpb	%dil, (%rbx,%rax)
	jne	.L53
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L55
.L66:
	movl	%r13d, %r12d
	movl	%r15d, %ebx
	movl	%edx, %r13d
	movl	%esi, %r15d
	leal	-1(%r12,%r10), %r12d
	addl	$1, %ebx
	addl	$1, %r12d
	cmpl	%r12d, %r14d
	jg	.L59
	.p2align 4,,10
	.p2align 3
.L69:
	movl	%r13d, %esi
	movl	%r15d, %ecx
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %r13
	movq	-96(%rbp), %r15
	movl	%ebx, %r12d
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	je	.L51
	movl	%ebx, %eax
	addl	%ebx, %ecx
	subl	%esi, %eax
	addl	%ebx, %esi
	imull	$100, %eax, %eax
	cltd
	idivl	%esi
	cmpl	$4, %ecx
	jle	.L70
.L60:
	testl	%eax, %eax
	movl	$0, %r12d
	cmovns	%eax, %r12d
	setg	%r14b
.L51:
	movl	%r12d, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r11, %rdx
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	addq	$56, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movl	$5, %edx
	subl	%ecx, %edx
	imull	$-10, %edx, %edx
	addl	%edx, %eax
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L57:
	movl	%r13d, %r12d
	movl	%edx, %r13d
	movl	%r15d, %ebx
	movl	%esi, %r15d
	addl	$1, %r13d
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L61:
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L51
	.cfi_endproc
.LFE2071:
	.size	_ZNK6icu_6719CharsetRecog_2022CN5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6719CharsetRecog_2022CN5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CharsetRecog_202210match_2022EPKhiPA5_S1_i
	.type	_ZNK6icu_6717CharsetRecog_202210match_2022EPKhiPA5_S1_i, @function
_ZNK6icu_6717CharsetRecog_202210match_2022EPKhiPA5_S1_i:
.LFB2053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	%rcx, -88(%rbp)
	movl	%r8d, -76(%rbp)
	testl	%edx, %edx
	jle	.L82
	leal	-1(%r8), %eax
	movl	$0, -80(%rbp)
	movl	%edx, %r12d
	xorl	%ebx, %ebx
	leaq	(%rax,%rax,4), %rax
	movl	$0, -68(%rbp)
	xorl	%r15d, %r15d
	leaq	5(%rcx,%rax), %rax
	movq	%rax, -64(%rbp)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L73:
	subl	$14, %eax
	cmpb	$2, %al
	adcl	$0, %ebx
.L78:
	addl	$1, %r15d
	cmpl	%r15d, %r12d
	jle	.L92
.L80:
	movq	-56(%rbp), %rax
	movslq	%r15d, %rdx
	leaq	(%rax,%rdx), %r14
	movzbl	(%r14), %eax
	cmpb	$27, %al
	jne	.L73
	movl	-76(%rbp), %eax
	testl	%eax, %eax
	jle	.L74
	movq	-88(%rbp), %r13
	movl	%r12d, %eax
	movl	%ebx, -72(%rbp)
	subl	%r15d, %eax
	movq	%r13, %rbx
	movl	%eax, %r13d
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L75:
	addq	$5, %rbx
	cmpq	%rbx, -64(%rbp)
	je	.L93
.L79:
	movq	%rbx, %rdi
	call	strlen@PLT
	movl	%eax, %r8d
	cmpl	%r13d, %eax
	jg	.L75
	cmpl	$1, %eax
	jle	.L89
	leal	-2(%rax), %ecx
	movl	$1, %eax
	addq	$2, %rcx
	.p2align 4,,10
	.p2align 3
.L77:
	movzbl	(%r14,%rax), %esi
	cmpb	%sil, (%rbx,%rax)
	jne	.L75
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L77
.L89:
	leal	-1(%r8,%r15), %r15d
	addl	$1, -68(%rbp)
	movl	-72(%rbp), %ebx
	addl	$1, %r15d
	cmpl	%r15d, %r12d
	jg	.L80
	.p2align 4,,10
	.p2align 3
.L92:
	movl	-68(%rbp), %edi
	testl	%edi, %edi
	je	.L71
	movl	-80(%rbp), %edx
	movl	%edi, %eax
	addl	%edi, %ebx
	subl	%edx, %eax
	addl	%edi, %edx
	imull	$100, %eax, %eax
	movl	%edx, %ecx
	cltd
	idivl	%ecx
	cmpl	$4, %ebx
	jle	.L94
.L81:
	testl	%eax, %eax
	movl	$0, %r14d
	cmovns	%eax, %r14d
	movl	%r14d, -68(%rbp)
.L71:
	movl	-68(%rbp), %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movl	$5, %edx
	subl	%ebx, %edx
	imull	$-10, %edx, %edx
	addl	%edx, %eax
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L93:
	movl	-72(%rbp), %ebx
.L74:
	addl	$1, -80(%rbp)
	jmp	.L78
.L82:
	movl	$0, -68(%rbp)
	jmp	.L71
	.cfi_endproc
.LFE2053:
	.size	_ZNK6icu_6717CharsetRecog_202210match_2022EPKhiPA5_S1_i, .-_ZNK6icu_6717CharsetRecog_202210match_2022EPKhiPA5_S1_i
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharsetRecog_2022D2Ev
	.type	_ZN6icu_6717CharsetRecog_2022D2Ev, @function
_ZN6icu_6717CharsetRecog_2022D2Ev:
.LFB2073:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_2022E(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2073:
	.size	_ZN6icu_6717CharsetRecog_2022D2Ev, .-_ZN6icu_6717CharsetRecog_2022D2Ev
	.globl	_ZN6icu_6717CharsetRecog_2022D1Ev
	.set	_ZN6icu_6717CharsetRecog_2022D1Ev,_ZN6icu_6717CharsetRecog_2022D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharsetRecog_2022D0Ev
	.type	_ZN6icu_6717CharsetRecog_2022D0Ev, @function
_ZN6icu_6717CharsetRecog_2022D0Ev:
.LFB2075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_2022E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2075:
	.size	_ZN6icu_6717CharsetRecog_2022D0Ev, .-_ZN6icu_6717CharsetRecog_2022D0Ev
	.weak	_ZTSN6icu_6717CharsetRecog_2022E
	.section	.rodata._ZTSN6icu_6717CharsetRecog_2022E,"aG",@progbits,_ZTSN6icu_6717CharsetRecog_2022E,comdat
	.align 16
	.type	_ZTSN6icu_6717CharsetRecog_2022E, @object
	.size	_ZTSN6icu_6717CharsetRecog_2022E, 29
_ZTSN6icu_6717CharsetRecog_2022E:
	.string	"N6icu_6717CharsetRecog_2022E"
	.weak	_ZTIN6icu_6717CharsetRecog_2022E
	.section	.data.rel.ro._ZTIN6icu_6717CharsetRecog_2022E,"awG",@progbits,_ZTIN6icu_6717CharsetRecog_2022E,comdat
	.align 8
	.type	_ZTIN6icu_6717CharsetRecog_2022E, @object
	.size	_ZTIN6icu_6717CharsetRecog_2022E, 24
_ZTIN6icu_6717CharsetRecog_2022E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717CharsetRecog_2022E
	.quad	_ZTIN6icu_6717CharsetRecognizerE
	.weak	_ZTSN6icu_6719CharsetRecog_2022JPE
	.section	.rodata._ZTSN6icu_6719CharsetRecog_2022JPE,"aG",@progbits,_ZTSN6icu_6719CharsetRecog_2022JPE,comdat
	.align 16
	.type	_ZTSN6icu_6719CharsetRecog_2022JPE, @object
	.size	_ZTSN6icu_6719CharsetRecog_2022JPE, 31
_ZTSN6icu_6719CharsetRecog_2022JPE:
	.string	"N6icu_6719CharsetRecog_2022JPE"
	.weak	_ZTIN6icu_6719CharsetRecog_2022JPE
	.section	.data.rel.ro._ZTIN6icu_6719CharsetRecog_2022JPE,"awG",@progbits,_ZTIN6icu_6719CharsetRecog_2022JPE,comdat
	.align 8
	.type	_ZTIN6icu_6719CharsetRecog_2022JPE, @object
	.size	_ZTIN6icu_6719CharsetRecog_2022JPE, 24
_ZTIN6icu_6719CharsetRecog_2022JPE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CharsetRecog_2022JPE
	.quad	_ZTIN6icu_6717CharsetRecog_2022E
	.weak	_ZTSN6icu_6719CharsetRecog_2022KRE
	.section	.rodata._ZTSN6icu_6719CharsetRecog_2022KRE,"aG",@progbits,_ZTSN6icu_6719CharsetRecog_2022KRE,comdat
	.align 16
	.type	_ZTSN6icu_6719CharsetRecog_2022KRE, @object
	.size	_ZTSN6icu_6719CharsetRecog_2022KRE, 31
_ZTSN6icu_6719CharsetRecog_2022KRE:
	.string	"N6icu_6719CharsetRecog_2022KRE"
	.weak	_ZTIN6icu_6719CharsetRecog_2022KRE
	.section	.data.rel.ro._ZTIN6icu_6719CharsetRecog_2022KRE,"awG",@progbits,_ZTIN6icu_6719CharsetRecog_2022KRE,comdat
	.align 8
	.type	_ZTIN6icu_6719CharsetRecog_2022KRE, @object
	.size	_ZTIN6icu_6719CharsetRecog_2022KRE, 24
_ZTIN6icu_6719CharsetRecog_2022KRE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CharsetRecog_2022KRE
	.quad	_ZTIN6icu_6717CharsetRecog_2022E
	.weak	_ZTSN6icu_6719CharsetRecog_2022CNE
	.section	.rodata._ZTSN6icu_6719CharsetRecog_2022CNE,"aG",@progbits,_ZTSN6icu_6719CharsetRecog_2022CNE,comdat
	.align 16
	.type	_ZTSN6icu_6719CharsetRecog_2022CNE, @object
	.size	_ZTSN6icu_6719CharsetRecog_2022CNE, 31
_ZTSN6icu_6719CharsetRecog_2022CNE:
	.string	"N6icu_6719CharsetRecog_2022CNE"
	.weak	_ZTIN6icu_6719CharsetRecog_2022CNE
	.section	.data.rel.ro._ZTIN6icu_6719CharsetRecog_2022CNE,"awG",@progbits,_ZTIN6icu_6719CharsetRecog_2022CNE,comdat
	.align 8
	.type	_ZTIN6icu_6719CharsetRecog_2022CNE, @object
	.size	_ZTIN6icu_6719CharsetRecog_2022CNE, 24
_ZTIN6icu_6719CharsetRecog_2022CNE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CharsetRecog_2022CNE
	.quad	_ZTIN6icu_6717CharsetRecog_2022E
	.weak	_ZTVN6icu_6717CharsetRecog_2022E
	.section	.data.rel.ro._ZTVN6icu_6717CharsetRecog_2022E,"awG",@progbits,_ZTVN6icu_6717CharsetRecog_2022E,comdat
	.align 8
	.type	_ZTVN6icu_6717CharsetRecog_2022E, @object
	.size	_ZTVN6icu_6717CharsetRecog_2022E, 56
_ZTVN6icu_6717CharsetRecog_2022E:
	.quad	0
	.quad	_ZTIN6icu_6717CharsetRecog_2022E
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6719CharsetRecog_2022JPE
	.section	.data.rel.ro._ZTVN6icu_6719CharsetRecog_2022JPE,"awG",@progbits,_ZTVN6icu_6719CharsetRecog_2022JPE,comdat
	.align 8
	.type	_ZTVN6icu_6719CharsetRecog_2022JPE, @object
	.size	_ZTVN6icu_6719CharsetRecog_2022JPE, 56
_ZTVN6icu_6719CharsetRecog_2022JPE:
	.quad	0
	.quad	_ZTIN6icu_6719CharsetRecog_2022JPE
	.quad	_ZNK6icu_6719CharsetRecog_2022JP7getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	_ZNK6icu_6719CharsetRecog_2022JP5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6719CharsetRecog_2022JPD1Ev
	.quad	_ZN6icu_6719CharsetRecog_2022JPD0Ev
	.weak	_ZTVN6icu_6719CharsetRecog_2022KRE
	.section	.data.rel.ro._ZTVN6icu_6719CharsetRecog_2022KRE,"awG",@progbits,_ZTVN6icu_6719CharsetRecog_2022KRE,comdat
	.align 8
	.type	_ZTVN6icu_6719CharsetRecog_2022KRE, @object
	.size	_ZTVN6icu_6719CharsetRecog_2022KRE, 56
_ZTVN6icu_6719CharsetRecog_2022KRE:
	.quad	0
	.quad	_ZTIN6icu_6719CharsetRecog_2022KRE
	.quad	_ZNK6icu_6719CharsetRecog_2022KR7getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	_ZNK6icu_6719CharsetRecog_2022KR5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6719CharsetRecog_2022KRD1Ev
	.quad	_ZN6icu_6719CharsetRecog_2022KRD0Ev
	.weak	_ZTVN6icu_6719CharsetRecog_2022CNE
	.section	.data.rel.ro._ZTVN6icu_6719CharsetRecog_2022CNE,"awG",@progbits,_ZTVN6icu_6719CharsetRecog_2022CNE,comdat
	.align 8
	.type	_ZTVN6icu_6719CharsetRecog_2022CNE, @object
	.size	_ZTVN6icu_6719CharsetRecog_2022CNE, 56
_ZTVN6icu_6719CharsetRecog_2022CNE:
	.quad	0
	.quad	_ZTIN6icu_6719CharsetRecog_2022CNE
	.quad	_ZNK6icu_6719CharsetRecog_2022CN7getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	_ZNK6icu_6719CharsetRecog_2022CN5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6719CharsetRecog_2022CND1Ev
	.quad	_ZN6icu_6719CharsetRecog_2022CND0Ev
	.section	.rodata
	.align 32
	.type	_ZN6icu_67L22escapeSequences_2022CNE, @object
	.size	_ZN6icu_67L22escapeSequences_2022CNE, 55
_ZN6icu_67L22escapeSequences_2022CNE:
	.string	"\033$)A"
	.string	"\033$)G"
	.string	"\033$*H"
	.string	"\033$)E"
	.string	"\033$+I"
	.string	"\033$+J"
	.string	"\033$+K"
	.string	"\033$+L"
	.string	"\033$+M"
	.string	"\033N"
	.zero	2
	.string	"\033O"
	.zero	2
	.align 32
	.type	_ZN6icu_67L22escapeSequences_2022JPE, @object
	.size	_ZN6icu_67L22escapeSequences_2022JPE, 60
_ZN6icu_67L22escapeSequences_2022JPE:
	.string	"\033$(C"
	.string	"\033$(D"
	.string	"\033$@"
	.zero	1
	.string	"\033$A"
	.zero	1
	.string	"\033$B"
	.zero	1
	.string	"\033&@"
	.zero	1
	.string	"\033(B"
	.zero	1
	.string	"\033(H"
	.zero	1
	.string	"\033(I"
	.zero	1
	.string	"\033(J"
	.zero	1
	.string	"\033.A"
	.zero	1
	.string	"\033.F"
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
