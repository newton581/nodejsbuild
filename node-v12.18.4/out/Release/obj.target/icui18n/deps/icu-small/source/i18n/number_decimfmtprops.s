	.file	"number_decimfmtprops.cpp"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_121initDefaultPropertiesER10UErrorCode, @function
_ZN12_GLOBAL__N_121initDefaultPropertiesER10UErrorCode:
.LFB3378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rdi
	movb	$1, _ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 8+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6712CurrencyUnitC1Ev@PLT
	movl	$2, %edi
	movl	$2, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, 128+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movl	$2, %ecx
	movl	$2, %esi
	movq	%rax, 192+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movl	$2, %r8d
	movl	$2, %r9d
	movl	$2, %r10d
	movq	%rax, 256+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movl	$2, %r11d
	movq	%rax, 320+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 392+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 480+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 544+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 608+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 672+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movl	$2, %eax
	movw	%ax, 680+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movabsq	$-4294967296, %rax
	movw	%di, 328+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	leaq	128+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rdi
	movq	%rax, 64+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movabsq	$8589934591, %rax
	movaps	%xmm0, 80+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	pcmpeqd	%xmm0, %xmm0
	movw	%dx, 136+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%cx, 200+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%si, 264+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%r8w, 400+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%r9w, 488+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%r10w, 552+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%r11w, 616+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movaps	%xmm0, 96+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 112+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	$0, 48+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 56+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 384+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 460+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 744+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, _ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 8+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movl	$-1, 72+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 76+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movl	$0, 120+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	192+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	256+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	320+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	392+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rdi
	movb	$1, 384+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	xorl	%eax, %eax
	leaq	480+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rdi
	movb	$1, 460+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%ax, 456+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	xorl	%eax, %eax
	movw	%ax, 468+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movl	$2, 472+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	544+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	608+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	672+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movb	$1, 744+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0x000000000, 736+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movl	$-1, 752+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$0, 756+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	ret
	.cfi_endproc
.LFE3378:
	.size	_ZN12_GLOBAL__N_121initDefaultPropertiesER10UErrorCode, .-_ZN12_GLOBAL__N_121initDefaultPropertiesER10UErrorCode
	.section	.text._ZNK6icu_6713UnicodeStringeqERKS0_,"axG",@progbits,_ZNK6icu_6713UnicodeStringeqERKS0_,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6713UnicodeStringeqERKS0_
	.type	_ZNK6icu_6713UnicodeStringeqERKS0_, @function
_ZNK6icu_6713UnicodeStringeqERKS0_:
.LFB1186:
	.cfi_startproc
	endbr64
	movswl	8(%rdi), %edx
	movswl	8(%rsi), %eax
	movl	%edx, %ecx
	movl	%eax, %r8d
	andl	$1, %r8d
	andl	$1, %ecx
	jne	.L18
	testw	%dx, %dx
	js	.L6
	sarl	$5, %edx
	testw	%ax, %ax
	js	.L8
.L23:
	sarl	$5, %eax
.L9:
	andl	$1, %r8d
	jne	.L19
	cmpl	%edx, %eax
	je	.L22
.L19:
	movl	%ecx, %r8d
.L18:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	12(%rdi), %edx
	testw	%ax, %ax
	jns	.L23
.L8:
	movl	12(%rsi), %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L22:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	setne	%cl
	movl	%ecx, %eax
	ret
	.cfi_endproc
.LFE1186:
	.size	_ZNK6icu_6713UnicodeStringeqERKS0_, .-_ZNK6icu_6713UnicodeStringeqERKS0_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl23DecimalFormatPropertiesC2Ev
	.type	_ZN6icu_676number4impl23DecimalFormatPropertiesC2Ev, @function
_ZN6icu_676number4impl23DecimalFormatPropertiesC2Ev:
.LFB3389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movb	$1, -16(%rdi)
	movb	$1, -8(%rdi)
	call	_ZN6icu_6712CurrencyUnitC1Ev@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edi
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, 128(%rbx)
	movl	$2, %edx
	movl	$2, %ecx
	movl	$2, %esi
	movq	%rax, 192(%rbx)
	movl	$2, %r8d
	movl	$2, %r9d
	movl	$2, %r10d
	movq	%rax, 256(%rbx)
	movl	$2, %r11d
	movq	%rax, 320(%rbx)
	movq	%rax, 392(%rbx)
	movq	%rax, 480(%rbx)
	movq	%rax, 544(%rbx)
	movq	%rax, 608(%rbx)
	movq	%rax, 672(%rbx)
	movl	$2, %eax
	movw	%ax, 680(%rbx)
	movabsq	$-4294967296, %rax
	movw	%di, 328(%rbx)
	leaq	128(%rbx), %rdi
	movq	%rax, 64(%rbx)
	movabsq	$8589934591, %rax
	movups	%xmm0, 80(%rbx)
	pcmpeqd	%xmm0, %xmm0
	movw	%dx, 136(%rbx)
	movw	%cx, 200(%rbx)
	movw	%si, 264(%rbx)
	movw	%r8w, 400(%rbx)
	movw	%r9w, 488(%rbx)
	movw	%r10w, 552(%rbx)
	movw	%r11w, 616(%rbx)
	movups	%xmm0, 96(%rbx)
	movq	%rax, 112(%rbx)
	movq	$0, 48(%rbx)
	movb	$1, 56(%rbx)
	movb	$1, 384(%rbx)
	movb	$1, 460(%rbx)
	movb	$1, 744(%rbx)
	movb	$1, (%rbx)
	movb	$1, 8(%rbx)
	movl	$-1, 72(%rbx)
	movb	$1, 76(%rbx)
	movl	$0, 120(%rbx)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	192(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	256(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	320(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movb	$1, 384(%rbx)
	leaq	392(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	xorl	%eax, %eax
	movb	$1, 460(%rbx)
	leaq	480(%rbx), %rdi
	movw	%ax, 456(%rbx)
	xorl	%eax, %eax
	movw	%ax, 468(%rbx)
	movl	$2, 472(%rbx)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	544(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	608(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	672(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movb	$1, 744(%rbx)
	movq	$0x000000000, 736(%rbx)
	movl	$-1, 752(%rbx)
	movb	$0, 756(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3389:
	.size	_ZN6icu_676number4impl23DecimalFormatPropertiesC2Ev, .-_ZN6icu_676number4impl23DecimalFormatPropertiesC2Ev
	.globl	_ZN6icu_676number4impl23DecimalFormatPropertiesC1Ev
	.set	_ZN6icu_676number4impl23DecimalFormatPropertiesC1Ev,_ZN6icu_676number4impl23DecimalFormatPropertiesC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl23DecimalFormatProperties5clearEv
	.type	_ZN6icu_676number4impl23DecimalFormatProperties5clearEv, @function
_ZN6icu_676number4impl23DecimalFormatProperties5clearEv:
.LFB3391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movb	$1, (%rdi)
	movb	$1, 8(%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L27
	movq	(%rdi), %rax
	call	*8(%rax)
.L27:
	movabsq	$-4294967296, %rax
	movdqa	.LC0(%rip), %xmm0
	movb	$1, 56(%rbx)
	leaq	128(%rbx), %rdi
	movq	%rax, 64(%rbx)
	movabsq	$8589934591, %rax
	movq	%rax, 112(%rbx)
	movq	$0, 48(%rbx)
	movl	$-1, 72(%rbx)
	movb	$1, 76(%rbx)
	movl	$0, 120(%rbx)
	movups	%xmm0, 80(%rbx)
	pcmpeqd	%xmm0, %xmm0
	movups	%xmm0, 96(%rbx)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	192(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	256(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	320(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movb	$1, 384(%rbx)
	leaq	392(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	xorl	%eax, %eax
	xorl	%edx, %edx
	movb	$1, 460(%rbx)
	movw	%ax, 456(%rbx)
	leaq	480(%rbx), %rdi
	movw	%dx, 468(%rbx)
	movl	$2, 472(%rbx)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	544(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	608(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	672(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movb	$1, 744(%rbx)
	movq	$0x000000000, 736(%rbx)
	movl	$-1, 752(%rbx)
	movb	$0, 756(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3391:
	.size	_ZN6icu_676number4impl23DecimalFormatProperties5clearEv, .-_ZN6icu_676number4impl23DecimalFormatProperties5clearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv
	.type	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv, @function
_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv:
.LFB3394:
	.cfi_startproc
	endbr64
	movl	_ZN12_GLOBAL__N_126gDefaultPropertiesInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L43
	leaq	_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN12_GLOBAL__N_126gDefaultPropertiesInitOnceE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L34
	leaq	16+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rdi
	movb	$1, _ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 8+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	call	_ZN6icu_6712CurrencyUnitC1Ev@PLT
	movl	$2, %edi
	movl	$2, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 128+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movl	$2, %ecx
	movl	$2, %esi
	movl	$2, %r8d
	movq	%rax, 192+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movl	$2, %r9d
	movl	$2, %r10d
	movl	$2, %r11d
	movq	%rax, 256+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 320+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%di, 328+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	leaq	_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rdi
	movq	%rax, 392+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 480+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 544+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 608+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 672+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movl	$2, %eax
	movq	$0, 48+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 56+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%dx, 136+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%cx, 200+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%si, 264+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 384+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%r8w, 400+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 460+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%r9w, 488+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%r10w, 552+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%r11w, 616+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%ax, 680+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 744+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	call	_ZN6icu_676number4impl23DecimalFormatProperties5clearEv
	leaq	_ZN12_GLOBAL__N_126gDefaultPropertiesInitOnceE(%rip), %rdi
	movl	$0, 4+_ZN12_GLOBAL__N_126gDefaultPropertiesInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L34:
	leaq	_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3394:
	.size	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv, .-_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl23DecimalFormatProperties7_equalsERKS2_b
	.type	_ZNK6icu_676number4impl23DecimalFormatProperties7_equalsERKS2_b, @function
_ZNK6icu_676number4impl23DecimalFormatProperties7_equalsERKS2_b:
.LFB3392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	cmpb	$0, (%rdi)
	movzbl	(%rsi), %ecx
	jne	.L45
	testb	%cl, %cl
	je	.L106
.L46:
	xorl	%eax, %eax
.L44:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movl	4(%rdi), %eax
	cmpl	%eax, 4(%rsi)
	sete	%cl
.L45:
	testb	%cl, %cl
	je	.L46
	cmpb	$0, 8(%r12)
	movzbl	8(%rbx), %eax
	je	.L107
.L49:
	testb	%al, %al
	je	.L46
	movq	48(%rbx), %rax
	cmpq	%rax, 48(%r12)
	jne	.L46
	cmpb	$0, 56(%r12)
	movzbl	56(%rbx), %eax
	jne	.L50
	testb	%al, %al
	jne	.L46
	movl	60(%rbx), %eax
	cmpl	%eax, 60(%r12)
	sete	%al
.L50:
	testb	%al, %al
	je	.L46
	movzbl	65(%rbx), %eax
	cmpb	%al, 65(%r12)
	jne	.L46
	movzbl	66(%rbx), %eax
	cmpb	%al, 66(%r12)
	jne	.L46
	movzbl	67(%rbx), %eax
	cmpb	%al, 67(%r12)
	jne	.L46
	movl	68(%rbx), %eax
	cmpl	%eax, 68(%r12)
	jne	.L46
	movl	80(%rbx), %eax
	cmpl	%eax, 80(%r12)
	jne	.L46
	movl	92(%rbx), %eax
	cmpl	%eax, 92(%r12)
	jne	.L46
	movl	96(%rbx), %eax
	cmpl	%eax, 96(%r12)
	jne	.L46
	movl	104(%rbx), %eax
	cmpl	%eax, 104(%r12)
	jne	.L46
	movl	112(%rbx), %eax
	cmpl	%eax, 112(%r12)
	jne	.L46
	movl	116(%rbx), %eax
	cmpl	%eax, 116(%r12)
	jne	.L46
	movl	120(%rbx), %eax
	cmpl	%eax, 120(%r12)
	jne	.L46
	leaq	128(%rbx), %rsi
	leaq	128(%r12), %rdi
	movl	%edx, -20(%rbp)
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	testb	%al, %al
	je	.L46
	leaq	256(%rbx), %rsi
	leaq	256(%r12), %rdi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	testb	%al, %al
	je	.L46
	cmpb	$0, 384(%r12)
	movzbl	384(%rbx), %eax
	movl	-20(%rbp), %edx
	jne	.L51
	xorl	%ecx, %ecx
	testb	%al, %al
	jne	.L52
	xorl	%ecx, %ecx
	movl	388(%rbx), %eax
	cmpl	%eax, 388(%r12)
	sete	%cl
.L52:
	movl	%ecx, %eax
	andl	$1, %eax
.L51:
	testb	%al, %al
	je	.L46
	leaq	392(%rbx), %rsi
	leaq	392(%r12), %rdi
	movl	%edx, -20(%rbp)
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	testb	%al, %al
	je	.L46
	leaq	480(%rbx), %rsi
	leaq	480(%r12), %rdi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	testb	%al, %al
	je	.L46
	leaq	608(%rbx), %rsi
	leaq	608(%r12), %rdi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	testb	%al, %al
	je	.L46
	movsd	736(%r12), %xmm0
	ucomisd	736(%rbx), %xmm0
	jp	.L46
	jne	.L46
	cmpb	$0, 744(%r12)
	movzbl	744(%rbx), %eax
	movl	-20(%rbp), %edx
	jne	.L54
	xorl	%ecx, %ecx
	testb	%al, %al
	jne	.L55
	xorl	%ecx, %ecx
	movl	748(%rbx), %eax
	cmpl	%eax, 748(%r12)
	sete	%cl
.L55:
	movl	%ecx, %eax
	andl	$1, %eax
.L54:
	testb	%al, %al
	je	.L46
	movl	752(%rbx), %eax
	cmpl	%eax, 752(%r12)
	jne	.L46
	movzbl	756(%rbx), %eax
	cmpb	%al, 756(%r12)
	jne	.L46
	testb	%dl, %dl
	jne	.L60
	movl	72(%rbx), %eax
	cmpl	%eax, 72(%r12)
	jne	.L46
	movzbl	76(%rbx), %eax
	cmpb	%al, 76(%r12)
	jne	.L46
	movl	100(%rbx), %eax
	cmpl	%eax, 100(%r12)
	jne	.L46
	movl	84(%rbx), %eax
	cmpl	%eax, 84(%r12)
	jne	.L46
	movl	88(%rbx), %eax
	cmpl	%eax, 88(%r12)
	jne	.L46
	movl	108(%rbx), %eax
	cmpl	%eax, 108(%r12)
	jne	.L46
	leaq	192(%rbx), %rsi
	leaq	192(%r12), %rdi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	testb	%al, %al
	je	.L46
	leaq	320(%rbx), %rsi
	leaq	320(%r12), %rdi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	testb	%al, %al
	je	.L46
	leaq	544(%rbx), %rsi
	leaq	544(%r12), %rdi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	testb	%al, %al
	je	.L46
	leaq	672(%rbx), %rsi
	leaq	672(%r12), %rdi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	testb	%al, %al
	je	.L46
	movzbl	64(%rbx), %eax
	cmpb	%al, 64(%r12)
	jne	.L46
	movzbl	456(%rbx), %eax
	cmpb	%al, 456(%r12)
	jne	.L46
	movzbl	457(%rbx), %eax
	cmpb	%al, 457(%r12)
	jne	.L46
	cmpb	$0, 460(%r12)
	movzbl	460(%rbx), %eax
	jne	.L56
	xorl	%edx, %edx
	testb	%al, %al
	jne	.L57
	xorl	%edx, %edx
	movl	464(%rbx), %eax
	cmpl	%eax, 464(%r12)
	sete	%dl
.L57:
	movl	%edx, %eax
	andl	$1, %eax
.L56:
	testb	%al, %al
	je	.L46
	movzbl	468(%rbx), %eax
	cmpb	%al, 468(%r12)
	jne	.L46
	movzbl	469(%rbx), %eax
	cmpb	%al, 469(%r12)
	jne	.L46
	movl	472(%rbx), %eax
	cmpl	%eax, 472(%r12)
	sete	%al
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L107:
	testb	%al, %al
	jne	.L46
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	movl	%edx, -20(%rbp)
	call	_ZNK6icu_6711MeasureUniteqERKNS_7UObjectE@PLT
	movl	-20(%rbp), %edx
	testb	%al, %al
	setne	%al
	jmp	.L49
.L60:
	movl	%edx, %eax
	jmp	.L44
	.cfi_endproc
.LFE3392:
	.size	_ZNK6icu_676number4impl23DecimalFormatProperties7_equalsERKS2_b, .-_ZNK6icu_676number4impl23DecimalFormatProperties7_equalsERKS2_b
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl23DecimalFormatProperties29equalsDefaultExceptFastFormatEv
	.type	_ZNK6icu_676number4impl23DecimalFormatProperties29equalsDefaultExceptFastFormatEv, @function
_ZNK6icu_676number4impl23DecimalFormatProperties29equalsDefaultExceptFastFormatEv:
.LFB3393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN12_GLOBAL__N_126gDefaultPropertiesInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L157
.L110:
	cmpb	$0, (%rbx)
	movzbl	_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	jne	.L112
	testb	%al, %al
	je	.L158
.L113:
	xorl	%eax, %eax
.L108:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movl	4+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpl	%eax, 4(%rbx)
	sete	%al
.L112:
	testb	%al, %al
	je	.L113
	cmpb	$0, 8(%rbx)
	movzbl	8+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	je	.L159
.L116:
	testb	%al, %al
	je	.L113
	movq	48+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rax
	cmpq	%rax, 48(%rbx)
	jne	.L113
	cmpb	$0, 56(%rbx)
	movzbl	56+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	jne	.L117
	testb	%al, %al
	jne	.L113
	movl	60+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpl	%eax, 60(%rbx)
	sete	%al
.L117:
	testb	%al, %al
	je	.L113
	movzbl	65+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpb	%al, 65(%rbx)
	jne	.L113
	movzbl	66+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpb	%al, 66(%rbx)
	jne	.L113
	movzbl	67+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpb	%al, 67(%rbx)
	jne	.L113
	movl	68+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpl	%eax, 68(%rbx)
	jne	.L113
	movl	80+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpl	%eax, 80(%rbx)
	jne	.L113
	movl	92+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpl	%eax, 92(%rbx)
	jne	.L113
	movl	96+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpl	%eax, 96(%rbx)
	jne	.L113
	movl	104+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpl	%eax, 104(%rbx)
	jne	.L113
	movl	112+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpl	%eax, 112(%rbx)
	jne	.L113
	movl	116+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpl	%eax, 116(%rbx)
	jne	.L113
	movl	120+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpl	%eax, 120(%rbx)
	jne	.L113
	leaq	128(%rbx), %rdi
	leaq	128+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rsi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	testb	%al, %al
	je	.L113
	leaq	256(%rbx), %rdi
	leaq	256+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rsi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	testb	%al, %al
	je	.L113
	cmpb	$0, 384(%rbx)
	movzbl	384+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	jne	.L118
	xorl	%edx, %edx
	testb	%al, %al
	jne	.L119
	xorl	%edx, %edx
	movl	388+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpl	%eax, 388(%rbx)
	sete	%dl
.L119:
	movl	%edx, %eax
	andl	$1, %eax
.L118:
	testb	%al, %al
	je	.L113
	leaq	392(%rbx), %rdi
	leaq	392+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rsi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	testb	%al, %al
	je	.L113
	leaq	480(%rbx), %rdi
	leaq	480+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rsi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	testb	%al, %al
	je	.L113
	leaq	608(%rbx), %rdi
	leaq	608+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rsi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	testb	%al, %al
	je	.L113
	movsd	736(%rbx), %xmm0
	ucomisd	736+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %xmm0
	jp	.L113
	jne	.L113
	cmpb	$0, 744(%rbx)
	movzbl	744+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	jne	.L121
	xorl	%edx, %edx
	testb	%al, %al
	jne	.L122
	xorl	%edx, %edx
	movl	748+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpl	%eax, 748(%rbx)
	sete	%dl
.L122:
	movl	%edx, %eax
	andl	$1, %eax
.L121:
	testb	%al, %al
	je	.L113
	movl	752+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpl	%eax, 752(%rbx)
	jne	.L113
	movzbl	756+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %eax
	cmpb	%al, 756(%rbx)
	sete	%al
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L159:
	testb	%al, %al
	jne	.L113
	leaq	16(%rbx), %rdi
	leaq	16+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rsi
	call	_ZNK6icu_6711MeasureUniteqERKNS_7UObjectE@PLT
	testb	%al, %al
	setne	%al
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L157:
	leaq	_ZN12_GLOBAL__N_126gDefaultPropertiesInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L110
	leaq	16+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rdi
	movb	$1, _ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 8+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	call	_ZN6icu_6712CurrencyUnitC1Ev@PLT
	movl	$2, %edi
	movl	$2, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	movl	$2, %esi
	movl	$2, %r8d
	movl	$2, %r9d
	movl	$2, %r10d
	movl	$2, %r11d
	movq	%rax, 128+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 192+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 256+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 320+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%di, 328+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	leaq	_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip), %rdi
	movq	%rax, 392+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 480+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 544+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 608+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	%rax, 672+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movl	$2, %eax
	movw	%dx, 136+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%cx, 200+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%si, 264+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%r8w, 400+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%r9w, 488+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%r10w, 552+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%r11w, 616+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movw	%ax, 680+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movq	$0, 48+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 56+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 384+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 460+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	movb	$1, 744+_ZN12_GLOBAL__N_121kRawDefaultPropertiesE(%rip)
	call	_ZN6icu_676number4impl23DecimalFormatProperties5clearEv
	leaq	_ZN12_GLOBAL__N_126gDefaultPropertiesInitOnceE(%rip), %rdi
	movl	$0, 4+_ZN12_GLOBAL__N_126gDefaultPropertiesInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L110
	.cfi_endproc
.LFE3393:
	.size	_ZNK6icu_676number4impl23DecimalFormatProperties29equalsDefaultExceptFastFormatEv, .-_ZNK6icu_676number4impl23DecimalFormatProperties29equalsDefaultExceptFastFormatEv
	.local	_ZN12_GLOBAL__N_126gDefaultPropertiesInitOnceE
	.comm	_ZN12_GLOBAL__N_126gDefaultPropertiesInitOnceE,8,8
	.local	_ZN12_GLOBAL__N_121kRawDefaultPropertiesE
	.comm	_ZN12_GLOBAL__N_121kRawDefaultPropertiesE,760,16
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	-1
	.long	-1
	.long	-1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
