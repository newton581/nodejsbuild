	.file	"quant.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710Quantifier17getDynamicClassIDEv
	.type	_ZNK6icu_6710Quantifier17getDynamicClassIDEv, @function
_ZNK6icu_6710Quantifier17getDynamicClassIDEv:
.LFB1302:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6710Quantifier16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE1302:
	.size	_ZNK6icu_6710Quantifier17getDynamicClassIDEv, .-_ZNK6icu_6710Quantifier17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710Quantifier9toMatcherEv
	.type	_ZNK6icu_6710Quantifier9toMatcherEv, @function
_ZNK6icu_6710Quantifier9toMatcherEv:
.LFB1329:
	.cfi_startproc
	endbr64
	leaq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1329:
	.size	_ZNK6icu_6710Quantifier9toMatcherEv, .-_ZNK6icu_6710Quantifier9toMatcherEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Quantifier7setDataEPKNS_23TransliterationRuleDataE
	.type	_ZN6icu_6710Quantifier7setDataEPKNS_23TransliterationRuleDataE, @function
_ZN6icu_6710Quantifier7setDataEPKNS_23TransliterationRuleDataE:
.LFB1334:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*48(%rax)
	.cfi_endproc
.LFE1334:
	.size	_ZN6icu_6710Quantifier7setDataEPKNS_23TransliterationRuleDataE, .-_ZN6icu_6710Quantifier7setDataEPKNS_23TransliterationRuleDataE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710Quantifier5cloneEv
	.type	_ZNK6icu_6710Quantifier5cloneEv, @function
_ZNK6icu_6710Quantifier5cloneEv:
.LFB1328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L5
	leaq	120+_ZTVN6icu_6710QuantifierE(%rip), %rax
	movq	16(%rbx), %rdi
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	(%rdi), %rax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	call	*24(%rax)
	movq	%rax, 16(%r12)
	movq	24(%rbx), %rax
	movq	%rax, 24(%r12)
.L5:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1328:
	.size	_ZNK6icu_6710Quantifier5cloneEv, .-_ZNK6icu_6710Quantifier5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Quantifier7matchesERKNS_11ReplaceableERiia
	.type	_ZN6icu_6710Quantifier7matchesERKNS_11ReplaceableERiia, @function
_ZN6icu_6710Quantifier7matchesERKNS_11ReplaceableERiia:
.LFB1330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$40, %rsp
	movl	(%rdx), %eax
	movq	%rsi, -64(%rbp)
	movl	%r8d, -56(%rbp)
	movb	%r8b, -49(%rbp)
	movl	%eax, %r14d
	movl	%eax, -68(%rbp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L30:
	movq	-64(%rbp), %rsi
	subq	$8, %rdi
	movl	%r15d, %ecx
	movq	%r12, %rdx
	call	.LTHUNK2
	cmpl	$2, %eax
	jne	.L15
.L31:
	movl	(%r12), %eax
	addl	$1, %ebx
	cmpl	%r14d, %eax
	je	.L12
	movl	%eax, %r14d
.L16:
	cmpl	%ebx, 28(%r13)
	jbe	.L12
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	_ZThn8_N6icu_6710Quantifier7matchesERKNS_11ReplaceableERiia(%rip), %rcx
	movsbl	-49(%rbp), %r8d
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	je	.L30
	movq	-64(%rbp), %rsi
	movl	%r15d, %ecx
	movq	%r12, %rdx
	call	*%rax
	cmpl	$2, %eax
	je	.L31
.L15:
	cmpb	$0, -56(%rbp)
	je	.L18
	cmpl	$1, %eax
	jne	.L12
.L19:
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	cmpb	$0, -56(%rbp)
	je	.L18
	cmpl	%r15d, (%r12)
	je	.L19
.L18:
	movl	$2, %eax
	cmpl	%ebx, 24(%r13)
	jbe	.L11
	movl	-68(%rbp), %eax
	movl	%eax, (%r12)
	xorl	%eax, %eax
.L11:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1330:
	.size	_ZN6icu_6710Quantifier7matchesERKNS_11ReplaceableERiia, .-_ZN6icu_6710Quantifier7matchesERKNS_11ReplaceableERiia
	.set	.LTHUNK2,_ZN6icu_6710Quantifier7matchesERKNS_11ReplaceableERiia
	.p2align 4
	.globl	_ZThn8_N6icu_6710Quantifier7matchesERKNS_11ReplaceableERiia
	.type	_ZThn8_N6icu_6710Quantifier7matchesERKNS_11ReplaceableERiia, @function
_ZThn8_N6icu_6710Quantifier7matchesERKNS_11ReplaceableERiia:
.LFB1802:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK2
	.cfi_endproc
.LFE1802:
	.size	_ZThn8_N6icu_6710Quantifier7matchesERKNS_11ReplaceableERiia, .-_ZThn8_N6icu_6710Quantifier7matchesERKNS_11ReplaceableERiia
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710Quantifier9toPatternERNS_13UnicodeStringEa
	.type	_ZNK6icu_6710Quantifier9toPatternERNS_13UnicodeStringEa, @function
_ZNK6icu_6710Quantifier9toPatternERNS_13UnicodeStringEa:
.LFB1331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rsi), %eax
	testb	$1, %al
	je	.L33
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L34:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	_ZThn8_NK6icu_6710Quantifier9toPatternERNS_13UnicodeStringEa(%rip), %rcx
	movsbl	%r13b, %edx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L37
	subq	$8, %rdi
	movq	%r12, %rsi
	call	.LTHUNK3
.L38:
	movl	24(%rbx), %r9d
	testl	%r9d, %r9d
	jne	.L39
	movl	28(%rbx), %eax
	cmpl	$1, %eax
	je	.L53
	cmpl	$2147483647, %eax
	je	.L54
.L42:
	movl	$123, %edx
	leaq	-42(%rbp), %r13
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%dx, -42(%rbp)
	movq	%r13, %rsi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	24(%rbx), %esi
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$10, %edx
	call	_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii@PLT
	movl	$44, %ecx
	movq	%r13, %rsi
	xorl	%edx, %edx
	movw	%cx, -42(%rbp)
	movq	%r12, %rdi
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	28(%rbx), %esi
	cmpl	$2147483647, %esi
	jne	.L55
.L43:
	movl	$125, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%r12, %rax
.L32:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L56
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L35
	movswl	%ax, %edx
	sarl	$5, %edx
.L36:
	testl	%edx, %edx
	je	.L34
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L39:
	movabsq	$9223372032559808513, %rax
	cmpq	%rax, 24(%rbx)
	jne	.L42
	movl	$43, %esi
	movw	%si, -42(%rbp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$1, %ecx
	movl	$10, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L35:
	movl	12(%rsi), %edx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%r12, %rsi
	call	*%rax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$63, %r8d
	movw	%r8w, -42(%rbp)
.L52:
	leaq	-42(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$42, %edi
	movw	%di, -42(%rbp)
	jmp	.L52
.L56:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1331:
	.size	_ZNK6icu_6710Quantifier9toPatternERNS_13UnicodeStringEa, .-_ZNK6icu_6710Quantifier9toPatternERNS_13UnicodeStringEa
	.set	.LTHUNK3,_ZNK6icu_6710Quantifier9toPatternERNS_13UnicodeStringEa
	.p2align 4
	.globl	_ZThn8_NK6icu_6710Quantifier9toPatternERNS_13UnicodeStringEa
	.type	_ZThn8_NK6icu_6710Quantifier9toPatternERNS_13UnicodeStringEa, @function
_ZThn8_NK6icu_6710Quantifier9toPatternERNS_13UnicodeStringEa:
.LFB1803:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK3
	.cfi_endproc
.LFE1803:
	.size	_ZThn8_NK6icu_6710Quantifier9toPatternERNS_13UnicodeStringEa, .-_ZThn8_NK6icu_6710Quantifier9toPatternERNS_13UnicodeStringEa
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710Quantifier17matchesIndexValueEh
	.type	_ZNK6icu_6710Quantifier17matchesIndexValueEh, @function
_ZNK6icu_6710Quantifier17matchesIndexValueEh:
.LFB1332:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	testl	%eax, %eax
	je	.L62
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	_ZThn8_NK6icu_6710Quantifier17matchesIndexValueEh(%rip), %rdx
	movzbl	%bl, %esi
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L59
	subq	$8, %rdi
	call	.LTHUNK4
	testb	%al, %al
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	*%rax
	testb	%al, %al
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1332:
	.size	_ZNK6icu_6710Quantifier17matchesIndexValueEh, .-_ZNK6icu_6710Quantifier17matchesIndexValueEh
	.set	.LTHUNK4,_ZNK6icu_6710Quantifier17matchesIndexValueEh
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE
	.type	_ZNK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE, @function
_ZNK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE:
.LFB1333:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %edx
	testl	%edx, %edx
	jne	.L78
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	_ZThn8_NK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE(%rip), %rbx
.L68:
	movq	16(%rdi), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%rax), %rdx
	movq	40(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L70
	leaq	-8(%rax), %rdi
	movl	20(%rax), %eax
	testl	%eax, %eax
	jne	.L68
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	popq	%rbx
	.cfi_restore 3
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.cfi_endproc
.LFE1333:
	.size	_ZNK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE, .-_ZNK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE
	.set	.LTHUNK5,_ZNK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710QuantifierD2Ev
	.type	_ZN6icu_6710QuantifierD2Ev, @function
_ZN6icu_6710QuantifierD2Ev:
.LFB1325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_6710QuantifierE(%rip), %rax
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rdi), %r13
	movq	%rdi, %r12
	movups	%xmm0, (%rdi)
	testq	%r13, %r13
	je	.L80
	movq	0(%r13), %rax
	leaq	_ZN6icu_6710QuantifierD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L81
	call	_ZN6icu_6710QuantifierD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L80:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	call	*%rax
	jmp	.L80
	.cfi_endproc
.LFE1325:
	.size	_ZN6icu_6710QuantifierD2Ev, .-_ZN6icu_6710QuantifierD2Ev
	.globl	_ZN6icu_6710QuantifierD1Ev
	.set	_ZN6icu_6710QuantifierD1Ev,_ZN6icu_6710QuantifierD2Ev
	.p2align 4
	.globl	_ZThn8_NK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE
	.type	_ZThn8_NK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE, @function
_ZThn8_NK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE:
.LFB1798:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %edx
	testl	%edx, %edx
	je	.L86
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	_ZThn8_NK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L92
	addq	$8, %rsp
	movq	%r12, %rsi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L86:
	ret
.L92:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	popq	%rax
	movq	%r12, %rsi
	subq	$8, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	.LTHUNK5
	.cfi_endproc
.LFE1798:
	.size	_ZThn8_NK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE, .-_ZThn8_NK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE
	.p2align 4
	.globl	_ZThn8_N6icu_6710QuantifierD0Ev
	.type	_ZThn8_N6icu_6710QuantifierD0Ev, @function
_ZThn8_N6icu_6710QuantifierD0Ev:
.LFB1797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_6710QuantifierE(%rip), %rax
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 13, -24
	leaq	-8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movups	%xmm0, -8(%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L94
	movq	(%rdi), %rax
	leaq	_ZN6icu_6710QuantifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	je	.L100
	call	*%rax
.L94:
	movq	%r12, %rdi
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	addq	$16, %rsp
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
.L100:
	.cfi_restore_state
	movq	%rdi, -24(%rbp)
	call	_ZN6icu_6710QuantifierD1Ev
	movq	-24(%rbp), %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L94
	.cfi_endproc
.LFE1797:
	.size	_ZThn8_N6icu_6710QuantifierD0Ev, .-_ZThn8_N6icu_6710QuantifierD0Ev
	.p2align 4
	.globl	_ZThn8_N6icu_6710QuantifierD1Ev
	.type	_ZThn8_N6icu_6710QuantifierD1Ev, @function
_ZThn8_N6icu_6710QuantifierD1Ev:
.LFB1799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_6710QuantifierE(%rip), %rax
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movups	%xmm0, -8(%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L102
	movq	(%rdi), %rax
	leaq	_ZN6icu_6710QuantifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	je	.L108
	call	*%rax
.L102:
	movq	%r12, %rdi
	leaq	-8(%r12), %r13
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	addq	$16, %rsp
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
.L108:
	.cfi_restore_state
	movq	%rdi, -24(%rbp)
	call	_ZN6icu_6710QuantifierD1Ev
	movq	-24(%rbp), %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L102
	.cfi_endproc
.LFE1799:
	.size	_ZThn8_N6icu_6710QuantifierD1Ev, .-_ZThn8_N6icu_6710QuantifierD1Ev
	.p2align 4
	.globl	_ZThn8_NK6icu_6710Quantifier17matchesIndexValueEh
	.type	_ZThn8_NK6icu_6710Quantifier17matchesIndexValueEh, @function
_ZThn8_NK6icu_6710Quantifier17matchesIndexValueEh:
.LFB1800:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	testl	%eax, %eax
	je	.L114
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	_ZThn8_NK6icu_6710Quantifier17matchesIndexValueEh(%rip), %rdx
	movzbl	%bl, %esi
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L119
	call	*%rax
.L112:
	testb	%al, %al
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
.L119:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	subq	$8, %rdi
	call	.LTHUNK4
	jmp	.L112
	.cfi_endproc
.LFE1800:
	.size	_ZThn8_NK6icu_6710Quantifier17matchesIndexValueEh, .-_ZThn8_NK6icu_6710Quantifier17matchesIndexValueEh
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710QuantifierD0Ev
	.type	_ZN6icu_6710QuantifierD0Ev, @function
_ZN6icu_6710QuantifierD0Ev:
.LFB1327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_6710QuantifierE(%rip), %rax
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movups	%xmm0, (%rdi)
	testq	%r13, %r13
	je	.L121
	movq	0(%r13), %rax
	movq	8(%rax), %rdx
	leaq	_ZN6icu_6710QuantifierD0Ev(%rip), %rax
	cmpq	%rax, %rdx
	jne	.L122
	movq	16(%r13), %r14
	movups	%xmm0, 0(%r13)
	testq	%r14, %r14
	je	.L123
	movq	(%r14), %rdx
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L124
	movq	16(%r14), %r15
	movups	%xmm0, (%r14)
	testq	%r15, %r15
	je	.L125
	movq	(%r15), %rdx
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L126
	movq	16(%r15), %rbx
	movups	%xmm0, (%r15)
	testq	%rbx, %rbx
	je	.L127
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L128
	movq	16(%rbx), %r8
	movups	%xmm0, (%rbx)
	testq	%r8, %r8
	je	.L129
	movq	(%r8), %rdx
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L130
	movq	16(%r8), %rdi
	movups	%xmm0, (%r8)
	testq	%rdi, %rdi
	je	.L131
	movq	(%rdi), %rdx
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L132
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN6icu_6710QuantifierD1Ev
	movq	-56(%rbp), %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r8
.L131:
	leaq	8(%r8), %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L129:
	leaq	8(%rbx), %rdi
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L127:
	leaq	8(%r15), %rdi
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L125:
	leaq	8(%r14), %rdi
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L123:
	leaq	8(%r13), %rdi
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L121:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%r14, %rdi
	call	*%rdx
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%r8, %rdi
	call	*%rdx
	jmp	.L129
.L132:
	movq	%r8, -56(%rbp)
	call	*%rdx
	movq	-56(%rbp), %r8
	jmp	.L131
	.cfi_endproc
.LFE1327:
	.size	_ZN6icu_6710QuantifierD0Ev, .-_ZN6icu_6710QuantifierD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Quantifier16getStaticClassIDEv
	.type	_ZN6icu_6710Quantifier16getStaticClassIDEv, @function
_ZN6icu_6710Quantifier16getStaticClassIDEv:
.LFB1301:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6710Quantifier16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE1301:
	.size	_ZN6icu_6710Quantifier16getStaticClassIDEv, .-_ZN6icu_6710Quantifier16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710QuantifierC2EPNS_14UnicodeFunctorEjj
	.type	_ZN6icu_6710QuantifierC2EPNS_14UnicodeFunctorEjj, @function
_ZN6icu_6710QuantifierC2EPNS_14UnicodeFunctorEjj:
.LFB1310:
	.cfi_startproc
	endbr64
	leaq	120+_ZTVN6icu_6710QuantifierE(%rip), %rax
	movq	%rsi, 16(%rdi)
	leaq	-104(%rax), %r8
	movq	%rax, %xmm1
	movl	%edx, 24(%rdi)
	movq	%r8, %xmm0
	movl	%ecx, 28(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE1310:
	.size	_ZN6icu_6710QuantifierC2EPNS_14UnicodeFunctorEjj, .-_ZN6icu_6710QuantifierC2EPNS_14UnicodeFunctorEjj
	.globl	_ZN6icu_6710QuantifierC1EPNS_14UnicodeFunctorEjj
	.set	_ZN6icu_6710QuantifierC1EPNS_14UnicodeFunctorEjj,_ZN6icu_6710QuantifierC2EPNS_14UnicodeFunctorEjj
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710QuantifierC2ERKS0_
	.type	_ZN6icu_6710QuantifierC2ERKS0_, @function
_ZN6icu_6710QuantifierC2ERKS0_:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_6710QuantifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rsi), %r15
	movq	%rdi, -72(%rbp)
	movaps	%xmm0, -64(%rbp)
	movq	(%r15), %rax
	movups	%xmm0, (%rdi)
	movq	24(%rax), %r13
	leaq	_ZNK6icu_6710Quantifier5cloneEv(%rip), %rax
	cmpq	%rax, %r13
	jne	.L155
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L156
	movdqa	-64(%rbp), %xmm0
	movq	16(%r15), %r12
	movups	%xmm0, (%rax)
	movq	(%r12), %rax
	movq	24(%rax), %rdx
	cmpq	%r13, %rdx
	jne	.L157
	movl	$32, %edi
	movq	%rdx, -80(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L158
	movdqa	-64(%rbp), %xmm0
	movq	16(%r12), %r9
	movq	-80(%rbp), %rdx
	movups	%xmm0, (%rax)
	movq	(%r9), %rax
	movq	24(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L159
	movl	$32, %edi
	movq	%rcx, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L160
	movq	-80(%rbp), %r9
	movdqa	-64(%rbp), %xmm0
	movq	-88(%rbp), %rcx
	movq	16(%r9), %r10
	movups	%xmm0, (%rax)
	movq	(%r10), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L161
	movl	$32, %edi
	movq	%rdx, -88(%rbp)
	movq	%r10, -96(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-80(%rbp), %r9
	movq	-88(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L162
	movq	-96(%rbp), %r10
	movdqa	-64(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movq	%rdx, -104(%rbp)
	movq	16(%r10), %rdi
	movups	%xmm0, (%rax)
	movq	%r9, -88(%rbp)
	movq	(%rdi), %rax
	movq	%r10, -64(%rbp)
	call	*24(%rax)
	movq	-80(%rbp), %rcx
	movq	-64(%rbp), %r10
	movq	-88(%rbp), %r9
	movq	-104(%rbp), %rdx
	movq	%rax, 16(%rcx)
	movq	24(%r10), %rax
	movq	%rax, 24(%rcx)
.L162:
	movq	24(%r9), %rax
	movq	%rcx, 16(%rdx)
	movq	%rax, 24(%rdx)
.L160:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r13)
	movq	%rax, 24(%r13)
.L158:
	movq	24(%r15), %rax
	movq	%r13, 16(%rbx)
	movq	%rax, 24(%rbx)
.L156:
	movq	-72(%rbp), %rsi
	movq	24(%r14), %rax
	movq	%rbx, 16(%rsi)
	movq	%rax, 24(%rsi)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movq	%r15, %rdi
	call	*%r13
	movq	%rax, %rbx
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%rdx, -80(%rbp)
	movq	%r10, %rdi
	movq	%r9, -64(%rbp)
	call	*%rax
	movq	-80(%rbp), %rdx
	movq	-64(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%r12, %rdi
	call	*%rdx
	movq	%rax, %r13
	movq	24(%r15), %rax
	movq	%r13, 16(%rbx)
	movq	%rax, 24(%rbx)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r9, %rdi
	call	*%rcx
	movq	%rax, %rdx
	movq	24(%r12), %rax
	movq	%rdx, 16(%r13)
	movq	%rax, 24(%r13)
	jmp	.L158
	.cfi_endproc
.LFE1322:
	.size	_ZN6icu_6710QuantifierC2ERKS0_, .-_ZN6icu_6710QuantifierC2ERKS0_
	.globl	_ZN6icu_6710QuantifierC1ERKS0_
	.set	_ZN6icu_6710QuantifierC1ERKS0_,_ZN6icu_6710QuantifierC2ERKS0_
	.weak	_ZTSN6icu_6710QuantifierE
	.section	.rodata._ZTSN6icu_6710QuantifierE,"aG",@progbits,_ZTSN6icu_6710QuantifierE,comdat
	.align 16
	.type	_ZTSN6icu_6710QuantifierE, @object
	.size	_ZTSN6icu_6710QuantifierE, 22
_ZTSN6icu_6710QuantifierE:
	.string	"N6icu_6710QuantifierE"
	.weak	_ZTIN6icu_6710QuantifierE
	.section	.data.rel.ro._ZTIN6icu_6710QuantifierE,"awG",@progbits,_ZTIN6icu_6710QuantifierE,comdat
	.align 8
	.type	_ZTIN6icu_6710QuantifierE, @object
	.size	_ZTIN6icu_6710QuantifierE, 56
_ZTIN6icu_6710QuantifierE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_6710QuantifierE
	.long	0
	.long	2
	.quad	_ZTIN6icu_6714UnicodeFunctorE
	.quad	2
	.quad	_ZTIN6icu_6714UnicodeMatcherE
	.quad	2050
	.weak	_ZTVN6icu_6710QuantifierE
	.section	.data.rel.ro._ZTVN6icu_6710QuantifierE,"awG",@progbits,_ZTVN6icu_6710QuantifierE,comdat
	.align 8
	.type	_ZTVN6icu_6710QuantifierE, @object
	.size	_ZTVN6icu_6710QuantifierE, 168
_ZTVN6icu_6710QuantifierE:
	.quad	0
	.quad	_ZTIN6icu_6710QuantifierE
	.quad	_ZN6icu_6710QuantifierD1Ev
	.quad	_ZN6icu_6710QuantifierD0Ev
	.quad	_ZNK6icu_6710Quantifier17getDynamicClassIDEv
	.quad	_ZNK6icu_6710Quantifier5cloneEv
	.quad	_ZNK6icu_6710Quantifier9toMatcherEv
	.quad	_ZNK6icu_6714UnicodeFunctor10toReplacerEv
	.quad	_ZN6icu_6710Quantifier7setDataEPKNS_23TransliterationRuleDataE
	.quad	_ZN6icu_6710Quantifier7matchesERKNS_11ReplaceableERiia
	.quad	_ZNK6icu_6710Quantifier9toPatternERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6710Quantifier17matchesIndexValueEh
	.quad	_ZNK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE
	.quad	-8
	.quad	_ZTIN6icu_6710QuantifierE
	.quad	_ZThn8_N6icu_6710QuantifierD1Ev
	.quad	_ZThn8_N6icu_6710QuantifierD0Ev
	.quad	_ZThn8_N6icu_6710Quantifier7matchesERKNS_11ReplaceableERiia
	.quad	_ZThn8_NK6icu_6710Quantifier9toPatternERNS_13UnicodeStringEa
	.quad	_ZThn8_NK6icu_6710Quantifier17matchesIndexValueEh
	.quad	_ZThn8_NK6icu_6710Quantifier13addMatchSetToERNS_10UnicodeSetE
	.local	_ZZN6icu_6710Quantifier16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6710Quantifier16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
