	.file	"ucln_in.cpp"
	.text
	.p2align 4
	.type	_ZL12i18n_cleanupv, @function
_ZL12i18n_cleanupv:
.LFB2487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	_ZL17gCleanupFunctions(%rip), %rbx
	leaq	288(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L2:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L4
	call	*%rax
	movq	$0, (%rbx)
.L4:
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jne	.L2
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2487:
	.size	_ZL12i18n_cleanupv, .-_ZL12i18n_cleanupv
	.p2align 4
	.globl	ucln_i18n_registerCleanup_67
	.type	ucln_i18n_registerCleanup_67, @function
ucln_i18n_registerCleanup_67:
.LFB2488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movslq	%edi, %rbx
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	leaq	_ZL12i18n_cleanupv(%rip), %rsi
	movl	$7, %edi
	call	ucln_registerCleanup_67@PLT
	cmpl	$35, %ebx
	ja	.L11
	leaq	_ZL17gCleanupFunctions(%rip), %rax
	movq	%r12, (%rax,%rbx,8)
.L11:
	popq	%rbx
	xorl	%edi, %edi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.cfi_endproc
.LFE2488:
	.size	ucln_i18n_registerCleanup_67, .-ucln_i18n_registerCleanup_67
	.local	_ZL17gCleanupFunctions
	.comm	_ZL17gCleanupFunctions,288,32
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
