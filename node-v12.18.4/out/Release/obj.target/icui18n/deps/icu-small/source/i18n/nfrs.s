	.file	"nfrs.cpp"
	.text
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB0:
	.text
.LHOTB0:
	.align 2
	.p2align 4
	.type	_ZNK6icu_679NFRuleSet14findNormalRuleEl.part.0, @function
_ZNK6icu_679NFRuleSet14findNormalRuleEl.part.0:
.LFB3426:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	js	.L27
.L2:
	movl	80(%r13), %ebx
	xorl	%ecx, %ecx
	testl	%ebx, %ebx
	jg	.L7
	movq	112(%r13), %r12
.L1:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	72(%r13), %rdx
	leal	(%rbx,%rcx), %eax
	sarl	%eax
	testq	%rdx, %rdx
	je	.L6
	movslq	%eax, %rdi
	movq	(%rdx,%rdi,8), %r12
	cmpq	%rsi, (%r12)
	je	.L1
	jle	.L28
	movl	%eax, %ebx
.L7:
	cmpl	%ecx, %ebx
	jg	.L29
	testl	%ebx, %ebx
	je	.L11
	movq	72(%r13), %r12
	leal	-1(%rbx), %eax
	testq	%r12, %r12
	je	.L10
	cltq
	movq	(%r12,%rax,8), %r12
.L10:
	movq	%r12, %rdi
	call	_ZNK6icu_676NFRule14shouldRollBackEl@PLT
	testb	%al, %al
	je	.L1
	cmpl	$1, %ebx
	je	.L11
	movq	72(%r13), %rax
	subl	$2, %ebx
	testq	%rax, %rax
	je	.L11
	movslq	%ebx, %rbx
	movq	(%rax,%rbx,8), %r12
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	movq	88(%rdi), %r12
	testq	%r12, %r12
	jne	.L1
	negq	%rsi
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L28:
	leal	1(%rax), %ecx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%r12d, %r12d
	jmp	.L1
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_679NFRuleSet14findNormalRuleEl.part.0.cold, @function
_ZNK6icu_679NFRuleSet14findNormalRuleEl.part.0.cold:
.LFSB3426:
.L6:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE3426:
	.text
	.size	_ZNK6icu_679NFRuleSet14findNormalRuleEl.part.0, .-_ZNK6icu_679NFRuleSet14findNormalRuleEl.part.0
	.section	.text.unlikely
	.size	_ZNK6icu_679NFRuleSet14findNormalRuleEl.part.0.cold, .-_ZNK6icu_679NFRuleSet14findNormalRuleEl.part.0.cold
.LCOLDE0:
	.text
.LHOTE0:
	.section	.text._ZN6icu_6710NFRuleListD2Ev,"axG",@progbits,_ZN6icu_6710NFRuleListD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6710NFRuleListD2Ev
	.type	_ZN6icu_6710NFRuleListD2Ev, @function
_ZN6icu_6710NFRuleListD2Ev:
.LFB2531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L30
	movl	8(%r13), %edx
	testl	%edx, %edx
	je	.L32
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L36:
	movl	%ebx, %eax
	movq	(%rdi,%rax,8), %r12
	testq	%r12, %r12
	je	.L33
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_676NFRuleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	8(%r13), %edx
	movq	0(%r13), %rdi
	cmpl	%edx, %ebx
	jb	.L36
.L32:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	addl	$1, %ebx
	cmpl	%edx, %ebx
	jb	.L36
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2531:
	.size	_ZN6icu_6710NFRuleListD2Ev, .-_ZN6icu_6710NFRuleListD2Ev
	.weak	_ZN6icu_6710NFRuleListD1Ev
	.set	_ZN6icu_6710NFRuleListD1Ev,_ZN6icu_6710NFRuleListD2Ev
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC1:
	.string	"%"
	.string	"d"
	.string	"e"
	.string	"f"
	.string	"a"
	.string	"u"
	.string	"l"
	.string	"t"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_679NFRuleSetC2EPNS_21RuleBasedNumberFormatEPNS_13UnicodeStringEiR10UErrorCode
	.type	_ZN6icu_679NFRuleSetC2EPNS_21RuleBasedNumberFormatEPNS_13UnicodeStringEiR10UErrorCode, @function
_ZN6icu_679NFRuleSetC2EPNS_21RuleBasedNumberFormatEPNS_13UnicodeStringEiR10UErrorCode:
.LFB2551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movslq	%ecx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$2, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, 16(%rdi)
	movq	%rsi, 136(%rdi)
	movq	%rax, 8(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movl	$80, %edi
	call	uprv_malloc_67@PLT
	movl	(%r14), %esi
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%rax, 144(%rbx)
	movabsq	$42949672960, %rax
	movq	%rax, 152(%rbx)
	movw	%cx, 160(%rbx)
	movb	$1, 162(%rbx)
	movups	%xmm0, 88(%rbx)
	movups	%xmm0, 104(%rbx)
	movups	%xmm0, 120(%rbx)
	testl	%esi, %esi
	jg	.L41
	salq	$6, %r13
	addq	%r13, %r12
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L44
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testl	%ecx, %ecx
	je	.L93
.L46:
	testb	$2, %al
	jne	.L94
	movq	24(%r12), %rax
	leaq	8(%rbx), %r15
	cmpw	$37, (%rax)
	je	.L95
.L49:
	leaq	-128(%rbp), %r13
	movl	$-1, %ecx
	leaq	.LC1(%rip), %rax
	movl	$1, %esi
	leaq	-136(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzwl	8(%r12), %eax
.L51:
	testw	%ax, %ax
	js	.L64
.L63:
	cwtl
	sarl	$5, %eax
.L65:
	testl	%eax, %eax
	jne	.L66
	movl	$9, (%r14)
.L66:
	movzwl	16(%rbx), %eax
	testw	%ax, %ax
	js	.L67
	movswl	%ax, %r9d
	xorl	%r8d, %r8d
	sarl	$5, %r9d
.L68:
	xorl	%edx, %edx
	movl	$2, %ecx
	leaq	_ZN6icu_67L15gPercentPercentE(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	testl	%eax, %eax
	movzwl	16(%rbx), %eax
	setne	161(%rbx)
	testw	%ax, %ax
	js	.L69
	movswl	%ax, %esi
	movl	%esi, %eax
	sarl	$5, %eax
.L70:
	leal	-8(%rax), %esi
	movl	$8, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	leaq	_ZN6icu_67L8gNoparseE(%rip), %rcx
	movl	$8, %edx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L8gNoparseE(%rip), %rdx
	testb	%al, %al
	jne	.L41
	movzwl	16(%rbx), %eax
	movb	$0, 162(%rbx)
	movl	%eax, %esi
	andl	$1, %esi
	testw	%ax, %ax
	js	.L72
	movswl	%ax, %ecx
	sarl	$5, %ecx
	leal	-8(%rcx), %edx
	testb	%sil, %sil
	je	.L91
	testl	%edx, %edx
	je	.L79
.L91:
	cmpl	%ecx, %edx
	jb	.L78
	.p2align 4,,10
	.p2align 3
.L41:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movl	12(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L46
.L93:
	movl	$9, (%r14)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L94:
	cmpw	$37, 10(%r12)
	leaq	10(%r12), %rax
	leaq	8(%rbx), %r15
	jne	.L49
.L95:
	xorl	%edx, %edx
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %r13d
	cmpl	$-1, %eax
	jne	.L50
	movl	$9, (%r14)
	movzwl	8(%r12), %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L69:
	movl	20(%rbx), %eax
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L67:
	movl	20(%rbx), %r9d
	movl	$0, %r8d
	testl	%r9d, %r9d
	cmovle	%r9d, %r8d
	subl	%r8d, %r9d
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L64:
	movl	12(%r12), %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L72:
	movl	20(%rbx), %ecx
	leal	-8(%rcx), %edx
	testb	%sil, %sil
	je	.L76
	testl	%edx, %edx
	jne	.L76
.L79:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L41
.L76:
	cmpl	%edx, %ecx
	jbe	.L41
	cmpl	$1023, %edx
	jle	.L78
	orl	$-32, %eax
	movl	%edx, 20(%rbx)
	movw	%ax, 16(%rbx)
	jmp	.L41
.L50:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	16(%rbx), %eax
	testw	%ax, %ax
	js	.L52
	movswl	%ax, %edx
	sarl	$5, %edx
.L53:
	movq	%r12, %rcx
	movl	%r13d, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movslq	%r13d, %rax
	leaq	2(%rax,%rax), %rcx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L97:
	movswl	%dx, %eax
	sarl	$5, %eax
.L55:
	cmpl	%eax, %r13d
	jge	.L56
	addl	$1, %r13d
	movl	$65535, %edi
	cmpl	%r13d, %eax
	jbe	.L57
	andl	$2, %edx
	leaq	10(%r12), %rax
	jne	.L59
	movq	24(%r12), %rax
.L59:
	movzwl	(%rax,%rcx), %edi
.L57:
	movq	%rcx, -152(%rbp)
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	movq	-152(%rbp), %rcx
	addq	$2, %rcx
	testb	%al, %al
	je	.L56
.L60:
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	jns	.L97
	movl	12(%r12), %eax
	jmp	.L55
.L78:
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, 16(%rbx)
	jmp	.L41
.L52:
	movl	20(%rbx), %edx
	jmp	.L53
.L56:
	cmpl	$2147483647, %r13d
	jne	.L61
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L62
	movl	$2, %eax
	movw	%ax, 8(%r12)
	movl	$2, %eax
	jmp	.L63
.L61:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movzwl	8(%r12), %eax
	jmp	.L51
.L62:
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L63
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2551:
	.size	_ZN6icu_679NFRuleSetC2EPNS_21RuleBasedNumberFormatEPNS_13UnicodeStringEiR10UErrorCode, .-_ZN6icu_679NFRuleSetC2EPNS_21RuleBasedNumberFormatEPNS_13UnicodeStringEiR10UErrorCode
	.globl	_ZN6icu_679NFRuleSetC1EPNS_21RuleBasedNumberFormatEPNS_13UnicodeStringEiR10UErrorCode
	.set	_ZN6icu_679NFRuleSetC1EPNS_21RuleBasedNumberFormatEPNS_13UnicodeStringEiR10UErrorCode,_ZN6icu_679NFRuleSetC2EPNS_21RuleBasedNumberFormatEPNS_13UnicodeStringEiR10UErrorCode
	.section	.text.unlikely
	.align 2
.LCOLDB2:
	.text
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN6icu_679NFRuleSet10parseRulesERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_679NFRuleSet10parseRulesERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_679NFRuleSet10parseRulesERNS_13UnicodeStringER10UErrorCode:
.LFB2553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -144(%rbp)
	movl	(%rdx), %edx
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	testl	%edx, %edx
	jle	.L147
.L98:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L148
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movl	80(%rdi), %r15d
	leaq	72(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, -152(%rbp)
	testl	%r15d, %r15d
	jg	.L149
.L101:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%r14d, %r14d
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L151:
	sarl	$5, %ecx
	cmpl	%ecx, %r14d
	jge	.L109
.L152:
	xorl	%edx, %edx
	testl	%r14d, %r14d
	js	.L110
	cmpl	%ecx, %r14d
	movl	%ecx, %edx
	cmovle	%r14d, %edx
	subl	%edx, %ecx
.L110:
	movl	$59, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %r15d
	cmpl	$-1, %eax
	je	.L150
.L111:
	movl	%r15d, %r9d
	leaq	-128(%rbp), %r13
	subl	%r14d, %r9d
	movq	%r13, %rdi
	movl	%r9d, -136(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-120(%rbp), %edx
	movl	-136(%rbp), %r9d
	testw	%dx, %dx
	js	.L113
	sarl	$5, %edx
.L114:
	movq	%r12, %rcx
	movl	%r14d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movl	80(%rbx), %eax
	movq	136(%rbx), %rcx
	movq	72(%rbx), %rdx
	testl	%eax, %eax
	je	.L127
	testq	%rdx, %rdx
	je	.L127
	subl	$1, %eax
	movq	(%rdx,%rax,8), %rdx
.L115:
	movq	-144(%rbp), %r9
	movq	-152(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leal	1(%r15), %r14d
	call	_ZN6icu_676NFRule9makeRulesERNS_13UnicodeStringEPNS_9NFRuleSetEPKS0_PKNS_21RuleBasedNumberFormatERNS_10NFRuleListER10UErrorCode@PLT
.L116:
	movswl	8(%r12), %ecx
	testw	%cx, %cx
	jns	.L151
	movl	12(%r12), %ecx
	cmpl	%ecx, %r14d
	jl	.L152
.L109:
	movl	80(%rbx), %eax
	testl	%eax, %eax
	jle	.L146
	subl	$1, %eax
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	leaq	8(,%rax,8), %r15
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L120:
	cmpq	%r14, %rax
	jl	.L153
.L121:
	cmpb	$1, 160(%rbx)
	adcq	$0, %rax
	addq	$8, %r12
	movq	%rax, %r14
	cmpq	%r12, %r15
	je	.L146
.L124:
	movq	72(%rbx), %rax
	testq	%rax, %rax
	je	.L119
	movq	(%rax,%r12), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L120
	movq	-144(%rbp), %rdx
	movq	%r14, %rsi
	call	_ZN6icu_676NFRule12setBaseValueElR10UErrorCode@PLT
	movq	%r14, %rax
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L113:
	movl	-116(%rbp), %edx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L127:
	xorl	%edx, %edx
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L150:
	movswl	8(%r12), %r15d
	testw	%r15w, %r15w
	js	.L112
	sarl	$5, %r15d
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L112:
	movl	12(%r12), %r15d
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L153:
	movq	-144(%rbp), %rax
	movl	$9, (%rax)
.L146:
	leaq	-128(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L149:
	movq	72(%rdi), %rax
	movq	%rax, -136(%rbp)
	cmpl	84(%rdi), %r15d
	je	.L154
.L102:
	movq	-136(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L103
	movl	80(%rbx), %eax
	movq	$0, (%rsi,%rax,8)
.L103:
	movq	-136(%rbp), %rax
	movq	$0, 72(%rbx)
	leal	-1(%r15), %edx
	movq	$0, 80(%rbx)
	movq	%rax, %r14
	leaq	8(%rax,%rdx,8), %r15
	.p2align 4,,10
	.p2align 3
.L105:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L104
	movq	%r13, %rdi
	call	_ZN6icu_676NFRuleD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L104:
	addq	$8, %r14
	cmpq	%r14, %r15
	jne	.L105
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L101
	movq	%rax, %rdi
	call	uprv_free_67@PLT
	jmp	.L101
.L154:
	movq	%rax, %rdi
	leal	10(%r15), %eax
	movl	%eax, 84(%rbx)
	leaq	0(,%rax,8), %rsi
	call	uprv_realloc_67@PLT
	movq	%rax, -136(%rbp)
	jmp	.L102
.L148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_679NFRuleSet10parseRulesERNS_13UnicodeStringER10UErrorCode.cold, @function
_ZN6icu_679NFRuleSet10parseRulesERNS_13UnicodeStringER10UErrorCode.cold:
.LFSB2553:
.L119:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE2553:
	.text
	.size	_ZN6icu_679NFRuleSet10parseRulesERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_679NFRuleSet10parseRulesERNS_13UnicodeStringER10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_679NFRuleSet10parseRulesERNS_13UnicodeStringER10UErrorCode.cold, .-_ZN6icu_679NFRuleSet10parseRulesERNS_13UnicodeStringER10UErrorCode.cold
.LCOLDE2:
	.text
.LHOTE2:
	.align 2
	.p2align 4
	.globl	_ZN6icu_679NFRuleSet19setBestFractionRuleEiPNS_6NFRuleEa
	.type	_ZN6icu_679NFRuleSet19setBestFractionRuleEiPNS_6NFRuleEa, @function
_ZN6icu_679NFRuleSet19setBestFractionRuleEiPNS_6NFRuleEa:
.LFB2555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movslq	%esi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%cl, %cl
	jne	.L171
.L156:
	leaq	(%rbx,%r12,8), %r12
	cmpq	$0, 88(%r12)
	je	.L165
	movq	136(%rbx), %rdi
	leaq	-128(%rbp), %r14
	call	_ZNK6icu_6721RuleBasedNumberFormat23getDecimalFormatSymbolsEv@PLT
	movq	%r14, %rdi
	leaq	8(%rax), %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L161
	movswl	%ax, %edx
	sarl	$5, %edx
.L162:
	movl	$-1, %ebx
	testl	%edx, %edx
	je	.L163
	leaq	-118(%rbp), %rdx
	testb	$2, %al
	movq	%rdx, %rax
	cmove	-104(%rbp), %rax
	movzwl	(%rax), %ebx
.L163:
	movzwl	14(%r13), %r15d
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpw	%bx, %r15w
	jne	.L155
.L165:
	movq	%r13, 88(%r12)
.L155:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L172
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movl	-116(%rbp), %edx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L171:
	movl	156(%rdi), %eax
	movq	144(%rdi), %rdi
	cmpl	%eax, 152(%rbx)
	je	.L173
.L157:
	testq	%rdi, %rdi
	je	.L158
	movl	152(%rbx), %eax
	leal	1(%rax), %edx
	movl	%edx, 152(%rbx)
	movq	%r13, (%rdi,%rax,8)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L158:
	movq	$0, 152(%rbx)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L173:
	addl	$10, %eax
	movl	%eax, 156(%rbx)
	leaq	0(,%rax,8), %rsi
	call	uprv_realloc_67@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rdi
	jmp	.L157
.L172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2555:
	.size	_ZN6icu_679NFRuleSet19setBestFractionRuleEiPNS_6NFRuleEa, .-_ZN6icu_679NFRuleSet19setBestFractionRuleEiPNS_6NFRuleEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_679NFRuleSet19setNonNumericalRuleEPNS_6NFRuleE
	.type	_ZN6icu_679NFRuleSet19setNonNumericalRuleEPNS_6NFRuleE, @function
_ZN6icu_679NFRuleSet19setNonNumericalRuleEPNS_6NFRuleE:
.LFB2554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	cmpq	$-1, %rax
	je	.L195
	cmpq	$-2, %rax
	je	.L196
	cmpq	$-3, %rax
	je	.L197
	cmpq	$-4, %rax
	je	.L198
	cmpq	$-5, %rax
	je	.L199
	cmpq	$-6, %rax
	je	.L200
.L174:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	88(%rdi), %r14
	testq	%r14, %r14
	je	.L176
	movq	%r14, %rdi
	call	_ZN6icu_676NFRuleD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L176:
	movq	%r12, 88(%r13)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	movq	%rsi, %rdx
	movl	$1, %ecx
	movl	$2, %esi
.L194:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679NFRuleSet19setBestFractionRuleEiPNS_6NFRuleEa
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	128(%rdi), %r14
	testq	%r14, %r14
	je	.L183
	movq	%r14, %rdi
	call	_ZN6icu_676NFRuleD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L183:
	movq	%r12, 128(%r13)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%rsi, %rdx
	movl	$1, %ecx
	movl	$1, %esi
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L199:
	movq	120(%rdi), %r14
	testq	%r14, %r14
	je	.L182
	movq	%r14, %rdi
	call	_ZN6icu_676NFRuleD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L182:
	movq	%r12, 120(%r13)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movq	%rsi, %rdx
	movl	$1, %ecx
	movl	$3, %esi
	jmp	.L194
	.cfi_endproc
.LFE2554:
	.size	_ZN6icu_679NFRuleSet19setNonNumericalRuleEPNS_6NFRuleE, .-_ZN6icu_679NFRuleSet19setNonNumericalRuleEPNS_6NFRuleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_679NFRuleSetD2Ev
	.type	_ZN6icu_679NFRuleSetD2Ev, @function
_ZN6icu_679NFRuleSetD2Ev:
.LFB2557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	88(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L202
	movq	%r12, %rdi
	call	_ZN6icu_676NFRuleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L202:
	movq	120(%rbx), %r12
	testq	%r12, %r12
	je	.L203
	movq	%r12, %rdi
	call	_ZN6icu_676NFRuleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L203:
	movq	128(%rbx), %r12
	testq	%r12, %r12
	je	.L204
	movq	%r12, %rdi
	call	_ZN6icu_676NFRuleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L204:
	leaq	144(%rbx), %rdi
	call	_ZN6icu_6710NFRuleListD1Ev
	leaq	72(%rbx), %rdi
	call	_ZN6icu_6710NFRuleListD1Ev
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE2557:
	.size	_ZN6icu_679NFRuleSetD2Ev, .-_ZN6icu_679NFRuleSetD2Ev
	.globl	_ZN6icu_679NFRuleSetD1Ev
	.set	_ZN6icu_679NFRuleSetD1Ev,_ZN6icu_679NFRuleSetD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679NFRuleSeteqERKS0_
	.type	_ZNK6icu_679NFRuleSeteqERKS0_, @function
_ZNK6icu_679NFRuleSeteqERKS0_:
.LFB2560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	80(%rsi), %eax
	cmpl	%eax, 80(%rdi)
	je	.L216
.L218:
	xorl	%r13d, %r13d
.L215:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movzbl	160(%rsi), %eax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	cmpb	%al, 160(%rdi)
	jne	.L218
	movswl	16(%rsi), %ecx
	movzwl	16(%rdi), %eax
	movl	%ecx, %r13d
	andl	$1, %r13d
	testb	$1, %al
	jne	.L219
	testw	%ax, %ax
	js	.L220
	movswl	%ax, %edx
	sarl	$5, %edx
.L221:
	testw	%cx, %cx
	js	.L222
	sarl	$5, %ecx
.L223:
	cmpl	%edx, %ecx
	jne	.L218
	testb	%r13b, %r13b
	jne	.L218
	leaq	8(%r12), %rsi
	leaq	8(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r13b
	.p2align 4,,10
	.p2align 3
.L219:
	testb	%r13b, %r13b
	je	.L218
	movl	$88, %r14d
.L226:
	movq	(%rbx,%r14), %rdi
	movq	(%r12,%r14), %rsi
	testq	%rdi, %rdi
	je	.L224
	testq	%rsi, %rsi
	je	.L218
	call	_ZNK6icu_676NFRuleeqERKS0_@PLT
	testb	%al, %al
	je	.L218
.L227:
	addq	$8, %r14
	cmpq	$136, %r14
	jne	.L226
	movl	80(%rbx), %eax
	xorl	%r14d, %r14d
	testl	%eax, %eax
	jne	.L228
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L263:
	addl	$1, %r14d
	cmpl	80(%rbx), %r14d
	jnb	.L215
.L228:
	movq	72(%r12), %rsi
	testq	%rsi, %rsi
	je	.L229
	movl	%r14d, %eax
	movq	(%rsi,%rax,8), %rsi
.L229:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L230
	movl	%r14d, %eax
	movq	(%rdi,%rax,8), %rdi
.L230:
	call	_ZNK6icu_676NFRuleeqERKS0_@PLT
	testb	%al, %al
	jne	.L263
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L222:
	movl	20(%r12), %ecx
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L220:
	movl	20(%rdi), %edx
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L224:
	testq	%rsi, %rsi
	jne	.L218
	jmp	.L227
	.cfi_endproc
.LFE2560:
	.size	_ZNK6icu_679NFRuleSeteqERKS0_, .-_ZNK6icu_679NFRuleSeteqERKS0_
	.section	.text.unlikely
	.align 2
.LCOLDB3:
	.text
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZN6icu_679NFRuleSet23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode
	.type	_ZN6icu_679NFRuleSet23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode, @function
_ZN6icu_679NFRuleSet23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode:
.LFB2561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	80(%rdi), %eax
	testl	%eax, %eax
	je	.L269
	.p2align 4,,10
	.p2align 3
.L265:
	movq	72(%rbx), %rax
	testq	%rax, %rax
	je	.L267
	movl	%r12d, %edx
	movq	%r13, %rsi
	addl	$1, %r12d
	movq	(%rax,%rdx,8), %rdi
	movq	%r14, %rdx
	call	_ZN6icu_676NFRule23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
	cmpl	80(%rbx), %r12d
	jb	.L265
.L269:
	movl	$1, %r15d
.L266:
	cmpq	$0, 88(%rbx,%r15,8)
	movl	%r15d, %r8d
	jne	.L270
.L272:
	addq	$1, %r15
	cmpq	$4, %r15
	jne	.L266
	leaq	88(%rbx), %r12
	addq	$136, %rbx
	.p2align 4,,10
	.p2align 3
.L278:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L277
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN6icu_676NFRule23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
.L277:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L278
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	movl	152(%rbx), %esi
	testl	%esi, %esi
	je	.L272
	movq	144(%rbx), %rax
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L276:
	testq	%rax, %rax
	je	.L273
	movl	%r12d, %edx
	movq	88(%rbx,%r15,8), %rcx
	movq	(%rax,%rdx,8), %rdx
	movq	(%rdx), %rdi
	cmpq	%rdi, (%rcx)
	je	.L291
	addl	$1, %r12d
	cmpl	%esi, %r12d
	jb	.L276
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L291:
	movl	%r8d, %esi
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	%r8d, -52(%rbp)
	call	_ZN6icu_679NFRuleSet19setBestFractionRuleEiPNS_6NFRuleEa
	movl	152(%rbx), %esi
	addl	$1, %r12d
	cmpl	%esi, %r12d
	jnb	.L272
	movq	144(%rbx), %rax
	movl	-52(%rbp), %r8d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%r14, %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	addl	$1, %r12d
	call	_ZN6icu_676NFRule23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
	cmpl	80(%rbx), %r12d
	jb	.L265
	jmp	.L269
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_679NFRuleSet23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode.cold, @function
_ZN6icu_679NFRuleSet23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode.cold:
.LFSB2561:
.L273:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE2561:
	.text
	.size	_ZN6icu_679NFRuleSet23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode, .-_ZN6icu_679NFRuleSet23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_679NFRuleSet23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode.cold, .-_ZN6icu_679NFRuleSet23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode.cold
.LCOLDE3:
	.text
.LHOTE3:
	.section	.text.unlikely
	.align 2
.LCOLDB8:
	.text
.LHOTB8:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679NFRuleSet23findFractionRuleSetRuleEd
	.type	_ZNK6icu_679NFRuleSet23findFractionRuleSetRuleEd, @function
_ZNK6icu_679NFRuleSet23findFractionRuleSetRuleEd:
.LFB2566:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	72(%rdi), %rax
	movsd	%xmm0, -40(%rbp)
	testq	%rax, %rax
	je	.L293
	movq	(%rax), %rdx
	movq	%rdi, %r12
	movq	(%rdx), %rbx
	movl	80(%rdi), %edx
	cmpl	$1, %edx
	jbe	.L294
	subl	$2, %edx
	leaq	8(%rax), %r8
	leaq	16(%rax,%rdx,8), %r10
	.p2align 4,,10
	.p2align 3
.L305:
	movq	(%r8), %rax
	movq	(%rax), %r9
	testb	$1, %bl
	jne	.L327
	testb	$1, %r9b
	jne	.L328
	movq	%r9, %rdx
	movq	%rbx, %rax
	xorl	%ecx, %ecx
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L352:
	testb	$1, %dl
	jne	.L329
.L297:
	sarq	%rax
	addl	$1, %ecx
	sarq	%rdx
	testb	$1, %al
	je	.L352
.L295:
	movq	%rdx, %rsi
	negq	%rsi
.L296:
	testq	%rsi, %rsi
	jne	.L356
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L300:
	sarq	%rsi
.L356:
	testb	$1, %sil
	je	.L300
	testq	%rsi, %rsi
	jle	.L357
	movq	%rsi, %rdi
	subq	%rdx, %rdi
	je	.L302
	movq	%rsi, %rax
.L303:
	movq	%rdi, %rsi
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L357:
	movq	%rsi, %rdx
	movq	%rsi, %rdi
	negq	%rdx
	addq	%rax, %rdi
	jne	.L303
.L338:
	movq	%rax, %rsi
.L302:
	movq	%rbx, %rax
	salq	%cl, %rsi
	addq	$8, %r8
	cqto
	idivq	%rsi
	movq	%rax, %rbx
	imulq	%r9, %rbx
	cmpq	%r8, %r10
	jne	.L305
.L294:
	pxor	%xmm1, %xmm1
	xorl	%r13d, %r13d
	cvtsi2sdq	%rbx, %xmm1
	mulsd	-40(%rbp), %xmm1
	addsd	.LC4(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -48(%rbp)
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	je	.L358
.L306:
	call	uprv_maxMantissa_67@PLT
	movsd	%xmm0, -48(%rbp)
	call	uprv_isNaN_67@PLT
	xorl	%edi, %edi
	testb	%al, %al
	je	.L359
.L311:
	movl	80(%r12), %r10d
	movq	72(%r12), %r8
	testl	%r10d, %r10d
	je	.L316
	testq	%r8, %r8
	je	.L360
	leal	-1(%r10), %r9d
	xorl	%esi, %esi
	xorl	%r11d, %r11d
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%rax, %rsi
.L320:
	movq	(%r8,%rsi,8), %rax
	movq	(%rax), %rcx
	imulq	%r13, %rcx
	movq	%rcx, %rax
	movq	%rbx, %rcx
	cqto
	idivq	%rbx
	subq	%rdx, %rcx
	cmpq	%rdx, %rcx
	cmovg	%rdx, %rcx
	cmpq	%rdi, %rcx
	jge	.L318
	movl	%esi, %r11d
	testq	%rcx, %rcx
	je	.L319
	movq	%rcx, %rdi
.L318:
	leaq	1(%rsi), %rax
	cmpq	%rsi, %r9
	jne	.L335
.L319:
	leal	1(%r11), %eax
	cmpl	%r10d, %eax
	jnb	.L361
	movq	(%r8,%r11,8), %r9
	movq	(%r8,%rax,8), %rcx
	movq	(%r9), %rdx
	cmpq	(%rcx), %rdx
	je	.L362
	addq	$24, %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	movq	%rax, %rsi
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L361:
	movl	%r11d, %eax
.L326:
	movq	(%r8,%rax,8), %r9
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	movq	%r9, %rax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movq	%r9, %rdx
	movq	%rbx, %rax
	xorl	%ecx, %ecx
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L328:
	movq	%rbx, %rsi
	movq	%r9, %rdx
	movq	%rbx, %rax
	xorl	%ecx, %ecx
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L358:
	call	uprv_maxMantissa_67@PLT
	movq	.LC5(%rip), %xmm2
	movsd	-48(%rbp), %xmm1
	movapd	%xmm0, %xmm3
	xorpd	%xmm2, %xmm0
	comisd	%xmm1, %xmm0
	jbe	.L363
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L364
.L353:
	call	uprv_floor_67@PLT
	cvttsd2siq	%xmm0, %r13
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L359:
	call	uprv_maxMantissa_67@PLT
	movq	.LC5(%rip), %xmm2
	movsd	-48(%rbp), %xmm1
	movapd	%xmm0, %xmm3
	xorpd	%xmm2, %xmm0
	comisd	%xmm1, %xmm0
	jbe	.L365
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L366
.L354:
	call	uprv_floor_67@PLT
	cvttsd2siq	%xmm0, %rdi
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L365:
	minsd	%xmm1, %xmm3
	pxor	%xmm1, %xmm1
	movapd	%xmm3, %xmm0
	comisd	%xmm0, %xmm1
	jbe	.L354
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L363:
	minsd	%xmm1, %xmm3
	pxor	%xmm1, %xmm1
	movapd	%xmm3, %xmm0
	comisd	%xmm0, %xmm1
	jbe	.L353
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L366:
	xorpd	%xmm2, %xmm0
	call	uprv_floor_67@PLT
	cvttsd2siq	%xmm0, %rdi
	negq	%rdi
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L364:
	xorpd	%xmm2, %xmm0
	call	uprv_floor_67@PLT
	cvttsd2siq	%xmm0, %r13
	negq	%r13
	jmp	.L306
.L362:
	pxor	%xmm0, %xmm0
	movsd	.LC4(%rip), %xmm6
	cvtsi2sdq	%rdx, %xmm0
	mulsd	-40(%rbp), %xmm0
	comisd	%xmm0, %xmm6
	ja	.L326
	ucomisd	.LC7(%rip), %xmm0
	cmovb	%r11, %rax
	jmp	.L326
.L316:
	testq	%r8, %r8
	jne	.L367
	addq	$24, %rsp
	xorl	%r9d, %r9d
	popq	%rbx
	movq	%r9, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L367:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L326
.L360:
	movq	0, %rax
	ud2
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_679NFRuleSet23findFractionRuleSetRuleEd.cold, @function
_ZNK6icu_679NFRuleSet23findFractionRuleSetRuleEd.cold:
.LFSB2566:
.L293:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE2566:
	.text
	.size	_ZNK6icu_679NFRuleSet23findFractionRuleSetRuleEd, .-_ZNK6icu_679NFRuleSet23findFractionRuleSetRuleEd
	.section	.text.unlikely
	.size	_ZNK6icu_679NFRuleSet23findFractionRuleSetRuleEd.cold, .-_ZNK6icu_679NFRuleSet23findFractionRuleSetRuleEd.cold
.LCOLDE8:
	.text
.LHOTE8:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679NFRuleSet14findNormalRuleEl
	.type	_ZNK6icu_679NFRuleSet14findNormalRuleEl, @function
_ZNK6icu_679NFRuleSet14findNormalRuleEl:
.LFB2565:
	.cfi_startproc
	endbr64
	cmpb	$0, 160(%rdi)
	jne	.L370
	jmp	_ZNK6icu_679NFRuleSet14findNormalRuleEl.part.0
	.p2align 4,,10
	.p2align 3
.L370:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
	jmp	_ZNK6icu_679NFRuleSet23findFractionRuleSetRuleEd
	.cfi_endproc
.LFE2565:
	.size	_ZNK6icu_679NFRuleSet14findNormalRuleEl, .-_ZNK6icu_679NFRuleSet14findNormalRuleEl
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679NFRuleSet6formatElRNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZNK6icu_679NFRuleSet6formatElRNS_13UnicodeStringEiiR10UErrorCode, @function
_ZNK6icu_679NFRuleSet6formatElRNS_13UnicodeStringEiiR10UErrorCode:
.LFB2562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpl	$63, %r8d
	jle	.L372
	movl	$27, (%r9)
.L371:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	cmpb	$0, 160(%rdi)
	movq	%rsi, %r13
	movq	%rdx, %r14
	movl	%ecx, %r15d
	movl	%r8d, %ebx
	jne	.L380
	call	_ZNK6icu_679NFRuleSet14findNormalRuleEl.part.0
	movq	%rax, %rdi
.L375:
	testq	%rdi, %rdi
	je	.L371
	addq	$8, %rsp
	movq	%r12, %r9
	leal	1(%rbx), %r8d
	movl	%r15d, %ecx
	popq	%rbx
	movq	%r14, %rdx
	popq	%r12
	movq	%r13, %rsi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676NFRule8doFormatElRNS_13UnicodeStringEiiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
	call	_ZNK6icu_679NFRuleSet23findFractionRuleSetRuleEd
	movq	%rax, %rdi
	jmp	.L375
	.cfi_endproc
.LFE2562:
	.size	_ZNK6icu_679NFRuleSet6formatElRNS_13UnicodeStringEiiR10UErrorCode, .-_ZNK6icu_679NFRuleSet6formatElRNS_13UnicodeStringEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679NFRuleSet14findDoubleRuleEd
	.type	_ZNK6icu_679NFRuleSet14findDoubleRuleEd, @function
_ZNK6icu_679NFRuleSet14findDoubleRuleEd:
.LFB2564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	cmpb	$0, 160(%rdi)
	jne	.L407
	movsd	%xmm0, -24(%rbp)
	call	uprv_isNaN_67@PLT
	movsd	-24(%rbp), %xmm1
	testb	%al, %al
	jne	.L408
	pxor	%xmm4, %xmm4
	comisd	%xmm1, %xmm4
	jbe	.L385
	movq	88(%r12), %rax
	testq	%rax, %rax
	je	.L409
.L381:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L412:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L407:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_679NFRuleSet23findFractionRuleSetRuleEd
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	xorpd	.LC5(%rip), %xmm1
.L385:
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -24(%rbp)
	call	uprv_isInfinite_67@PLT
	movsd	-24(%rbp), %xmm1
	testb	%al, %al
	je	.L387
	movq	120(%r12), %rax
	testq	%rax, %rax
	jne	.L381
	movq	136(%r12), %rdi
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6721RuleBasedNumberFormat22getDefaultInfinityRuleEv@PLT
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	movq	128(%r12), %rax
	testq	%rax, %rax
	jne	.L381
	movq	136(%r12), %rdi
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6721RuleBasedNumberFormat17getDefaultNaNRuleEv@PLT
	.p2align 4,,10
	.p2align 3
.L387:
	.cfi_restore_state
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -24(%rbp)
	call	uprv_floor_67@PLT
	movsd	-24(%rbp), %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L401
	je	.L388
.L401:
	movsd	.LC9(%rip), %xmm0
	comisd	%xmm1, %xmm0
	ja	.L410
.L390:
	movq	96(%r12), %rax
	testq	%rax, %rax
	jne	.L381
.L388:
	movq	112(%r12), %rax
	testq	%rax, %rax
	jne	.L381
	addsd	.LC4(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -24(%rbp)
	call	uprv_isNaN_67@PLT
	xorl	%esi, %esi
	testb	%al, %al
	je	.L411
.L392:
	cmpb	$0, 160(%r12)
	jne	.L412
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_679NFRuleSet14findNormalRuleEl.part.0
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	movq	104(%r12), %rax
	testq	%rax, %rax
	jne	.L381
	jmp	.L390
.L411:
	call	uprv_maxMantissa_67@PLT
	movq	.LC5(%rip), %xmm3
	movsd	-24(%rbp), %xmm1
	movapd	%xmm0, %xmm2
	xorpd	%xmm3, %xmm0
	comisd	%xmm1, %xmm0
	ja	.L393
	minsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm0
.L393:
	pxor	%xmm5, %xmm5
	comisd	%xmm0, %xmm5
	ja	.L413
	call	uprv_floor_67@PLT
	cvttsd2siq	%xmm0, %rsi
	jmp	.L392
.L413:
	xorpd	%xmm3, %xmm0
	call	uprv_floor_67@PLT
	cvttsd2siq	%xmm0, %rsi
	negq	%rsi
	jmp	.L392
	.cfi_endproc
.LFE2564:
	.size	_ZNK6icu_679NFRuleSet14findDoubleRuleEd, .-_ZNK6icu_679NFRuleSet14findDoubleRuleEd
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679NFRuleSet6formatEdRNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZNK6icu_679NFRuleSet6formatEdRNS_13UnicodeStringEiiR10UErrorCode, @function
_ZNK6icu_679NFRuleSet6formatEdRNS_13UnicodeStringEiiR10UErrorCode:
.LFB2563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpl	$63, %ecx
	jle	.L415
	movl	$27, (%r8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	cmpb	$0, 160(%rdi)
	movq	%rdi, %r13
	movq	%rsi, %r14
	movl	%edx, %r15d
	movl	%ecx, %ebx
	movsd	%xmm0, -56(%rbp)
	jne	.L448
	call	uprv_isNaN_67@PLT
	movsd	-56(%rbp), %xmm1
	testb	%al, %al
	jne	.L450
	pxor	%xmm5, %xmm5
	comisd	%xmm1, %xmm5
	jbe	.L445
	movq	88(%r13), %rdi
	testq	%rdi, %rdi
	je	.L451
.L420:
	addq	$24, %rsp
	leal	1(%rbx), %ecx
	movq	%r12, %r8
	movl	%r15d, %edx
	popq	%rbx
	movq	%r14, %rsi
	movapd	%xmm1, %xmm0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676NFRule8doFormatEdRNS_13UnicodeStringEiiR10UErrorCode@PLT
.L453:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
	.p2align 4,,10
	.p2align 3
.L448:
	movq	%r13, %rdi
	call	_ZNK6icu_679NFRuleSet23findFractionRuleSetRuleEd
	movsd	-56(%rbp), %xmm1
	movq	%rax, %rdi
.L418:
	testq	%rdi, %rdi
	jne	.L420
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	.cfi_restore_state
	movq	128(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L420
	movq	136(%r13), %rdi
	call	_ZNK6icu_6721RuleBasedNumberFormat17getDefaultNaNRuleEv@PLT
	movsd	-56(%rbp), %xmm1
	movq	%rax, %rdi
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L445:
	movapd	%xmm1, %xmm2
.L421:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -64(%rbp)
	movsd	%xmm2, -56(%rbp)
	call	uprv_isInfinite_67@PLT
	movsd	-56(%rbp), %xmm2
	movsd	-64(%rbp), %xmm1
	testb	%al, %al
	je	.L423
	movq	120(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L420
	movq	136(%r13), %rdi
	movsd	%xmm1, -56(%rbp)
	call	_ZNK6icu_6721RuleBasedNumberFormat22getDefaultInfinityRuleEv@PLT
	movsd	-56(%rbp), %xmm1
	movq	%rax, %rdi
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L423:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -64(%rbp)
	movsd	%xmm2, -56(%rbp)
	call	uprv_floor_67@PLT
	movsd	-56(%rbp), %xmm2
	movsd	-64(%rbp), %xmm1
	ucomisd	%xmm0, %xmm2
	jp	.L438
	jne	.L438
.L424:
	movq	112(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L420
	addsd	.LC4(%rip), %xmm2
	movsd	%xmm1, -64(%rbp)
	movapd	%xmm2, %xmm0
	movsd	%xmm2, -56(%rbp)
	call	uprv_isNaN_67@PLT
	xorl	%esi, %esi
	movsd	-64(%rbp), %xmm1
	testb	%al, %al
	je	.L452
.L428:
	cmpb	$0, 160(%r13)
	movsd	%xmm1, -56(%rbp)
	jne	.L453
	movq	%r13, %rdi
	call	_ZNK6icu_679NFRuleSet14findNormalRuleEl.part.0
	movsd	-56(%rbp), %xmm1
	movq	%rax, %rdi
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L451:
	movapd	%xmm1, %xmm2
	xorpd	.LC5(%rip), %xmm2
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L438:
	movsd	.LC9(%rip), %xmm0
	comisd	%xmm2, %xmm0
	ja	.L454
.L426:
	movq	96(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L420
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L454:
	movq	104(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L420
	jmp	.L426
.L452:
	call	uprv_maxMantissa_67@PLT
	movsd	-56(%rbp), %xmm2
	movsd	-64(%rbp), %xmm1
	movq	.LC5(%rip), %xmm4
	movapd	%xmm0, %xmm3
	xorpd	%xmm4, %xmm0
	comisd	%xmm2, %xmm0
	ja	.L429
	minsd	%xmm2, %xmm3
	movapd	%xmm3, %xmm0
.L429:
	pxor	%xmm6, %xmm6
	movsd	%xmm1, -56(%rbp)
	comisd	%xmm0, %xmm6
	ja	.L455
	call	uprv_floor_67@PLT
	movsd	-56(%rbp), %xmm1
	cvttsd2siq	%xmm0, %rsi
	jmp	.L428
.L455:
	xorpd	%xmm4, %xmm0
	call	uprv_floor_67@PLT
	movsd	-56(%rbp), %xmm1
	cvttsd2siq	%xmm0, %rsi
	negq	%rsi
	jmp	.L428
	.cfi_endproc
.LFE2563:
	.size	_ZNK6icu_679NFRuleSet6formatEdRNS_13UnicodeStringEiiR10UErrorCode, .-_ZNK6icu_679NFRuleSet6formatEdRNS_13UnicodeStringEiiR10UErrorCode
	.section	.text.unlikely
	.align 2
.LCOLDB10:
	.text
.LHOTB10:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679NFRuleSet11appendRulesERNS_13UnicodeStringE
	.type	_ZNK6icu_679NFRuleSet11appendRulesERNS_13UnicodeStringE, @function
_ZNK6icu_679NFRuleSet11appendRulesERNS_13UnicodeStringE:
.LFB2568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	8(%rdi), %rsi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %eax
	testw	%ax, %ax
	js	.L457
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L458:
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-58(%rbp), %r13
	xorl	%ebx, %ebx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$58, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%si, -58(%rbp)
	movl	$1, %ecx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$10, %edi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movw	%di, -58(%rbp)
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	80(%r15), %r8d
	testl	%r8d, %r8d
	jne	.L459
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L484:
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	(%rax,%rdx,8), %rdi
.L483:
	call	_ZNK6icu_676NFRule15_appendRuleTextERNS_13UnicodeStringE@PLT
	movl	$10, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movw	%cx, -58(%rbp)
	movq	%r12, %rdi
	movl	$1, %ecx
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	80(%r15), %ebx
	jnb	.L463
.L459:
	movq	72(%r15), %rax
	testq	%rax, %rax
	jne	.L484
	movq	%r12, %rsi
	xorl	%edi, %edi
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L463:
	leaq	136(%r15), %rax
	leaq	88(%r15), %r14
	movq	%rax, -72(%rbp)
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L486:
	cmpq	$-4, %rax
	je	.L466
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZNK6icu_676NFRule15_appendRuleTextERNS_13UnicodeStringE@PLT
	movl	$10, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movw	%dx, -58(%rbp)
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L465:
	addq	$8, %r14
	cmpq	%r14, -72(%rbp)
	je	.L485
.L460:
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L465
	movq	(%r8), %rax
	leaq	3(%rax), %rdx
	cmpq	$1, %rdx
	ja	.L486
.L466:
	movl	152(%r15), %ecx
	testl	%ecx, %ecx
	je	.L465
	movq	144(%r15), %rax
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L471:
	testq	%rax, %rax
	je	.L468
	movl	%ebx, %edx
	movq	(%r8), %rsi
	movq	(%rax,%rdx,8), %rdi
	cmpq	%rsi, (%rdi)
	je	.L487
	addl	$1, %ebx
	cmpl	%ecx, %ebx
	jb	.L471
	addq	$8, %r14
	cmpq	%r14, -72(%rbp)
	jne	.L460
.L485:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L488
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r8, -80(%rbp)
	addl	$1, %ebx
	call	_ZNK6icu_676NFRule15_appendRuleTextERNS_13UnicodeStringE@PLT
	movl	$10, %eax
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	152(%r15), %ecx
	cmpl	%ecx, %ebx
	jnb	.L465
	movq	144(%r15), %rax
	movq	-80(%rbp), %r8
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L457:
	movl	20(%rdi), %ecx
	jmp	.L458
.L488:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_679NFRuleSet11appendRulesERNS_13UnicodeStringE.cold, @function
_ZNK6icu_679NFRuleSet11appendRulesERNS_13UnicodeStringE.cold:
.LFSB2568:
.L468:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE2568:
	.text
	.size	_ZNK6icu_679NFRuleSet11appendRulesERNS_13UnicodeStringE, .-_ZNK6icu_679NFRuleSet11appendRulesERNS_13UnicodeStringE
	.section	.text.unlikely
	.size	_ZNK6icu_679NFRuleSet11appendRulesERNS_13UnicodeStringE.cold, .-_ZNK6icu_679NFRuleSet11appendRulesERNS_13UnicodeStringE.cold
.LCOLDE10:
	.text
.LHOTE10:
	.p2align 4
	.globl	_ZN6icu_6717util64_fromDoubleEd
	.type	_ZN6icu_6717util64_fromDoubleEd, @function
_ZN6icu_6717util64_fromDoubleEd:
.LFB2569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movsd	%xmm0, -8(%rbp)
	call	uprv_isNaN_67@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L501
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_restore_state
	call	uprv_maxMantissa_67@PLT
	movq	.LC5(%rip), %xmm3
	movsd	-8(%rbp), %xmm1
	movapd	%xmm0, %xmm2
	xorpd	%xmm3, %xmm0
	comisd	%xmm1, %xmm0
	jbe	.L502
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L503
.L499:
	call	uprv_floor_67@PLT
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cvttsd2siq	%xmm0, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	.cfi_restore_state
	minsd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	movapd	%xmm2, %xmm0
	comisd	%xmm0, %xmm1
	jbe	.L499
.L503:
	xorpd	%xmm3, %xmm0
	call	uprv_floor_67@PLT
	leave
	.cfi_def_cfa 7, 8
	cvttsd2siq	%xmm0, %rax
	negq	%rax
	ret
	.cfi_endproc
.LFE2569:
	.size	_ZN6icu_6717util64_fromDoubleEd, .-_ZN6icu_6717util64_fromDoubleEd
	.p2align 4
	.globl	_ZN6icu_6710util64_powEjt
	.type	_ZN6icu_6710util64_powEjt, @function
_ZN6icu_6710util64_powEjt:
.LFB2570:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	testl	%edi, %edi
	je	.L504
	movl	%edi, %edi
	movl	$1, %r8d
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L516:
	imulq	%rdi, %rdi
.L507:
	movzwl	%si, %eax
	andl	$1, %esi
	je	.L506
	imulq	%rdi, %r8
.L506:
	sarl	%eax
	movl	%eax, %esi
	jne	.L516
.L504:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE2570:
	.size	_ZN6icu_6710util64_powEjt, .-_ZN6icu_6710util64_powEjt
	.p2align 4
	.globl	_ZN6icu_6710util64_touElPDsjja
	.type	_ZN6icu_6710util64_touElPDsjja, @function
_ZN6icu_6710util64_touElPDsjja:
.LFB2571:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	movl	%edx, %r9d
	movq	%rdi, %rax
	setne	%dl
	testq	%rdi, %rdi
	jns	.L518
	testb	%dl, %dl
	je	.L518
	cmpl	$10, %ecx
	jne	.L519
	testb	%r8b, %r8b
	jne	.L519
	movl	$45, %edx
	leaq	2(%rsi), %rdi
	negq	%rax
	movw	%dx, (%rsi)
	subl	$1, %r9d
	je	.L534
	movl	$10, %r10d
.L521:
	leaq	_ZN6icu_67L11asciiDigitsE(%rip), %r8
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L561:
	testq	%rax, %rax
	je	.L526
.L528:
	cqto
	movq	%rdi, %rcx
	addq	$2, %rdi
	idivq	%r10
	movslq	%edx, %rdx
	movzbl	(%r8,%rdx), %edx
	movw	%dx, -2(%rdi)
	subl	$1, %r9d
	jne	.L561
.L526:
	movq	%rdi, %r8
	subq	%rsi, %r8
	sarq	%r8
.L530:
	testl	%r9d, %r9d
	je	.L531
.L524:
	xorl	%eax, %eax
	movw	%ax, (%rdi)
.L531:
	leaq	2(%rsi), %rax
	cmpw	$45, (%rsi)
	cmove	%rax, %rsi
	cmpq	%rcx, %rsi
	jnb	.L517
	.p2align 4,,10
	.p2align 3
.L533:
	movzwl	(%rcx), %eax
	movzwl	(%rsi), %edx
	subq	$2, %rcx
	addq	$2, %rsi
	movw	%dx, 2(%rcx)
	movw	%ax, -2(%rsi)
	cmpq	%rsi, %rcx
	ja	.L533
.L517:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	testq	%rax, %rax
	sete	%dil
	testb	%dl, %dl
	je	.L522
	testb	%dil, %dil
	je	.L522
	cmpb	$1, %r8b
	leaq	2(%rsi), %rdi
	movl	$1, %r8d
	sbbl	%eax, %eax
	andl	$48, %eax
	movw	%ax, (%rsi)
	cmpl	$1, %r9d
	je	.L517
	movq	%rsi, %rcx
	movl	$1, %r8d
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L522:
	testl	%r9d, %r9d
	je	.L535
	testb	%dil, %dil
	jne	.L535
.L519:
	cmpl	$2, %ecx
	movl	$2, %r10d
	movq	%rsi, %rdi
	cmovb	%r10d, %ecx
	movl	$36, %r10d
	cmpl	$36, %ecx
	cmova	%r10d, %ecx
	movl	%ecx, %r10d
	testb	%r8b, %r8b
	jne	.L527
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L562:
	testl	%r9d, %r9d
	je	.L526
.L527:
	cqto
	movq	%rdi, %rcx
	addq	$2, %rdi
	subl	$1, %r9d
	idivq	%r10
	movw	%dx, -2(%rdi)
	testq	%rax, %rax
	jne	.L562
	jmp	.L526
.L535:
	movq	%rsi, %rdi
	xorl	%r8d, %r8d
.L520:
	leaq	-2(%rdi), %rcx
	jmp	.L530
.L534:
	movl	$1, %r8d
	jmp	.L520
	.cfi_endproc
.LFE2571:
	.size	_ZN6icu_6710util64_touElPDsjja, .-_ZN6icu_6710util64_touElPDsjja
	.section	.text.unlikely
	.align 2
.LCOLDB11:
	.text
.LHOTB11:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679NFRuleSet5parseERKNS_13UnicodeStringERNS_13ParsePositionEdjRNS_11FormattableE
	.type	_ZNK6icu_679NFRuleSet5parseERKNS_13UnicodeStringERNS_13ParsePositionEdjRNS_11FormattableE, @function
_ZNK6icu_679NFRuleSet5parseERKNS_13UnicodeStringERNS_13ParsePositionEdjRNS_11FormattableE:
.LFB2567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r8, %rdi
	subq	$216, %rsp
	movl	%ecx, -212(%rbp)
	movq	%r8, -248(%rbp)
	movsd	%xmm0, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711Formattable7setLongEi@PLT
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L564
	sarl	$5, %eax
	testl	%eax, %eax
	je	.L602
.L628:
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	cmpq	$0, 88(%rbx)
	movabsq	$-4294967296, %rsi
	movq	%rax, -208(%rbp)
	movq	%rax, -192(%rbp)
	movq	8(%r15), %rax
	movq	%rsi, -200(%rbp)
	movq	%rax, -184(%rbp)
	je	.L567
	testb	$1, -212(%rbp)
	je	.L616
.L567:
	cmpq	$0, 96(%rbx)
	je	.L570
.L630:
	testb	$2, -212(%rbp)
	je	.L617
.L570:
	cmpq	$0, 104(%rbx)
	je	.L573
.L632:
	testb	$4, -212(%rbp)
	je	.L618
.L573:
	cmpq	$0, 112(%rbx)
	je	.L576
.L634:
	testb	$8, -212(%rbp)
	je	.L619
.L576:
	cmpq	$0, 120(%rbx)
	je	.L579
.L636:
	testb	$16, -212(%rbp)
	je	.L620
.L579:
	cmpq	$0, 128(%rbx)
	je	.L582
.L638:
	testb	$32, -212(%rbp)
	je	.L621
.L582:
	movsd	-224(%rbp), %xmm0
	call	uprv_isNaN_67@PLT
	movq	$0, -240(%rbp)
	testb	%al, %al
	je	.L622
.L584:
	movslq	80(%rbx), %rax
	movl	-200(%rbp), %edx
	movl	%eax, %ecx
	subl	$1, %ecx
	js	.L589
	movslq	%ecx, %r12
	movl	%ecx, %ecx
	leaq	-176(%rbp), %r14
	subq	%rcx, %rax
	salq	$3, %r12
	leaq	-16(,%rax,8), %rax
	movq	%rax, -232(%rbp)
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L624:
	sarl	$5, %eax
	cmpl	%edx, %eax
	jle	.L589
.L593:
	cmpb	$0, 160(%rbx)
	jne	.L594
	movq	72(%rbx), %rax
	testq	%rax, %rax
	je	.L595
	movq	(%rax,%r12), %rax
	movq	-240(%rbp), %rsi
	cmpq	%rsi, (%rax)
	jge	.L599
.L594:
	movq	%r14, %rdi
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L598
	movq	(%rdi,%r12), %rdi
.L598:
	movsbl	160(%rbx), %ecx
	movq	%r14, %r9
	movq	%r13, %rsi
	movl	-212(%rbp), %r8d
	movsd	-224(%rbp), %xmm0
	leaq	-192(%rbp), %rdx
	call	_ZNK6icu_676NFRule7doParseERKNS_13UnicodeStringERNS_13ParsePositionEadjRNS_11FormattableE@PLT
	testb	%al, %al
	je	.L600
	movl	-200(%rbp), %eax
	cmpl	%eax, -184(%rbp)
	jg	.L623
.L600:
	movq	8(%r15), %rax
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movl	-200(%rbp), %edx
.L599:
	subq	$8, %r12
	cmpq	-232(%rbp), %r12
	je	.L589
.L590:
	movswl	8(%r13), %eax
	testw	%ax, %ax
	jns	.L624
	movl	12(%r13), %eax
	cmpl	%edx, %eax
	jg	.L593
	.p2align 4,,10
	.p2align 3
.L589:
	movl	-196(%rbp), %eax
	movl	%edx, 8(%r15)
	leaq	-192(%rbp), %rdi
	movl	%eax, 12(%r15)
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	leaq	-208(%rbp), %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movl	$1, %eax
.L563:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L625
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	.cfi_restore_state
	movq	-248(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	movq	-184(%rbp), %rax
	movq	%rax, -200(%rbp)
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L622:
	call	uprv_maxMantissa_67@PLT
	movq	.LC5(%rip), %xmm2
	movsd	-224(%rbp), %xmm3
	movapd	%xmm0, %xmm1
	xorpd	%xmm2, %xmm0
	comisd	%xmm3, %xmm0
	jbe	.L626
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L627
.L613:
	call	uprv_floor_67@PLT
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -240(%rbp)
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L564:
	movl	12(%r13), %eax
	testl	%eax, %eax
	jne	.L628
.L602:
	xorl	%eax, %eax
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L616:
	leaq	-176(%rbp), %r12
	orl	$1, -212(%rbp)
	movl	-212(%rbp), %r14d
	movq	%r12, %rdi
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	88(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r12, %r9
	movsd	-224(%rbp), %xmm0
	leaq	-192(%rbp), %rdx
	movl	%r14d, %r8d
	movq	%r13, %rsi
	call	_ZNK6icu_676NFRule7doParseERKNS_13UnicodeStringERNS_13ParsePositionEadjRNS_11FormattableE@PLT
	testb	%al, %al
	je	.L569
	movl	-200(%rbp), %eax
	cmpl	%eax, -184(%rbp)
	jg	.L629
.L569:
	movq	8(%r15), %rax
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6711FormattableD1Ev@PLT
	cmpq	$0, 96(%rbx)
	jne	.L630
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L617:
	leaq	-176(%rbp), %r12
	orl	$2, -212(%rbp)
	movl	-212(%rbp), %r14d
	movq	%r12, %rdi
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	96(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r12, %r9
	movsd	-224(%rbp), %xmm0
	leaq	-192(%rbp), %rdx
	movl	%r14d, %r8d
	movq	%r13, %rsi
	call	_ZNK6icu_676NFRule7doParseERKNS_13UnicodeStringERNS_13ParsePositionEadjRNS_11FormattableE@PLT
	testb	%al, %al
	je	.L572
	movl	-200(%rbp), %eax
	cmpl	%eax, -184(%rbp)
	jg	.L631
.L572:
	movq	8(%r15), %rax
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6711FormattableD1Ev@PLT
	cmpq	$0, 104(%rbx)
	jne	.L632
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L618:
	leaq	-176(%rbp), %r12
	orl	$4, -212(%rbp)
	movl	-212(%rbp), %r14d
	movq	%r12, %rdi
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	104(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r12, %r9
	movsd	-224(%rbp), %xmm0
	leaq	-192(%rbp), %rdx
	movl	%r14d, %r8d
	movq	%r13, %rsi
	call	_ZNK6icu_676NFRule7doParseERKNS_13UnicodeStringERNS_13ParsePositionEadjRNS_11FormattableE@PLT
	testb	%al, %al
	je	.L575
	movl	-200(%rbp), %eax
	cmpl	%eax, -184(%rbp)
	jg	.L633
.L575:
	movq	8(%r15), %rax
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6711FormattableD1Ev@PLT
	cmpq	$0, 112(%rbx)
	jne	.L634
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L619:
	leaq	-176(%rbp), %r12
	orl	$8, -212(%rbp)
	movl	-212(%rbp), %r14d
	movq	%r12, %rdi
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	112(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r12, %r9
	movsd	-224(%rbp), %xmm0
	leaq	-192(%rbp), %rdx
	movl	%r14d, %r8d
	movq	%r13, %rsi
	call	_ZNK6icu_676NFRule7doParseERKNS_13UnicodeStringERNS_13ParsePositionEadjRNS_11FormattableE@PLT
	testb	%al, %al
	je	.L578
	movl	-200(%rbp), %eax
	cmpl	%eax, -184(%rbp)
	jg	.L635
.L578:
	movq	8(%r15), %rax
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6711FormattableD1Ev@PLT
	cmpq	$0, 120(%rbx)
	jne	.L636
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L620:
	leaq	-176(%rbp), %r12
	orl	$16, -212(%rbp)
	movl	-212(%rbp), %r14d
	movq	%r12, %rdi
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	120(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r12, %r9
	movsd	-224(%rbp), %xmm0
	leaq	-192(%rbp), %rdx
	movl	%r14d, %r8d
	movq	%r13, %rsi
	call	_ZNK6icu_676NFRule7doParseERKNS_13UnicodeStringERNS_13ParsePositionEadjRNS_11FormattableE@PLT
	testb	%al, %al
	je	.L581
	movl	-200(%rbp), %eax
	cmpl	%eax, -184(%rbp)
	jg	.L637
.L581:
	movq	8(%r15), %rax
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6711FormattableD1Ev@PLT
	cmpq	$0, 128(%rbx)
	jne	.L638
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L621:
	leaq	-176(%rbp), %r12
	orl	$32, -212(%rbp)
	movl	-212(%rbp), %r14d
	movq	%r12, %rdi
	call	_ZN6icu_6711FormattableC1Ev@PLT
	xorl	%ecx, %ecx
	movq	%r12, %r9
	movl	%r14d, %r8d
	movq	128(%rbx), %rdi
	movsd	-224(%rbp), %xmm0
	leaq	-192(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZNK6icu_676NFRule7doParseERKNS_13UnicodeStringERNS_13ParsePositionEadjRNS_11FormattableE@PLT
	testb	%al, %al
	je	.L583
	movl	-200(%rbp), %eax
	cmpl	%eax, -184(%rbp)
	jg	.L639
.L583:
	movq	8(%r15), %rax
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6711FormattableD1Ev@PLT
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L626:
	minsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L613
.L627:
	xorpd	%xmm2, %xmm0
	call	uprv_floor_67@PLT
	cvttsd2siq	%xmm0, %rax
	negq	%rax
	movq	%rax, -240(%rbp)
	jmp	.L584
.L629:
	movq	-248(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	movq	-184(%rbp), %rax
	movq	%rax, -200(%rbp)
	jmp	.L569
.L639:
	movq	-248(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	movq	-184(%rbp), %rax
	movq	%rax, -200(%rbp)
	jmp	.L583
.L637:
	movq	-248(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	movq	-184(%rbp), %rax
	movq	%rax, -200(%rbp)
	jmp	.L581
.L635:
	movq	-248(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	movq	-184(%rbp), %rax
	movq	%rax, -200(%rbp)
	jmp	.L578
.L633:
	movq	-248(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	movq	-184(%rbp), %rax
	movq	%rax, -200(%rbp)
	jmp	.L575
.L631:
	movq	-248(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	movq	-184(%rbp), %rax
	movq	%rax, -200(%rbp)
	jmp	.L572
.L625:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_679NFRuleSet5parseERKNS_13UnicodeStringERNS_13ParsePositionEdjRNS_11FormattableE.cold, @function
_ZNK6icu_679NFRuleSet5parseERKNS_13UnicodeStringERNS_13ParsePositionEdjRNS_11FormattableE.cold:
.LFSB2567:
.L595:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE2567:
	.text
	.size	_ZNK6icu_679NFRuleSet5parseERKNS_13UnicodeStringERNS_13ParsePositionEdjRNS_11FormattableE, .-_ZNK6icu_679NFRuleSet5parseERKNS_13UnicodeStringERNS_13ParsePositionEdjRNS_11FormattableE
	.section	.text.unlikely
	.size	_ZNK6icu_679NFRuleSet5parseERKNS_13UnicodeStringERNS_13ParsePositionEdjRNS_11FormattableE.cold, .-_ZNK6icu_679NFRuleSet5parseERKNS_13UnicodeStringERNS_13ParsePositionEdjRNS_11FormattableE.cold
.LCOLDE11:
	.text
.LHOTE11:
	.section	.rodata
	.align 32
	.type	_ZN6icu_67L11asciiDigitsE, @object
	.size	_ZN6icu_67L11asciiDigitsE, 36
_ZN6icu_67L11asciiDigitsE:
	.ascii	"0123456789abcdefghijklmnopqrstuvwxyz"
	.align 16
	.type	_ZN6icu_67L8gNoparseE, @object
	.size	_ZN6icu_67L8gNoparseE, 18
_ZN6icu_67L8gNoparseE:
	.value	64
	.value	110
	.value	111
	.value	112
	.value	97
	.value	114
	.value	115
	.value	101
	.value	0
	.align 2
	.type	_ZN6icu_67L15gPercentPercentE, @object
	.size	_ZN6icu_67L15gPercentPercentE, 6
_ZN6icu_67L15gPercentPercentE:
	.value	37
	.value	37
	.value	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	0
	.long	1071644672
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC7:
	.long	0
	.long	1073741824
	.align 8
.LC9:
	.long	0
	.long	1072693248
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
