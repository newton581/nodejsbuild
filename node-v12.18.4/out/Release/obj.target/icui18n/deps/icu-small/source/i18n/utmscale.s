	.file	"utmscale.cpp"
	.text
	.p2align 4
	.globl	utmscale_getTimeScaleValue_67
	.type	utmscale_getTimeScaleValue_67, @function
utmscale_getTimeScaleValue_67:
.LFB2:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L1
	cmpl	$9, %edi
	ja	.L7
	cmpl	$10, %esi
	ja	.L7
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	leaq	(%rdi,%rdi,4), %rax
	leaq	(%rdi,%rax,2), %rax
	addq	%rax, %rsi
	leaq	_ZL14timeScaleTable(%rip), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1, (%rdx)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2:
	.size	utmscale_getTimeScaleValue_67, .-utmscale_getTimeScaleValue_67
	.p2align 4
	.globl	utmscale_fromInt64_67
	.type	utmscale_fromInt64_67, @function
utmscale_fromInt64_67:
.LFB3:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L9
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L9
	cmpl	$9, %esi
	ja	.L12
	movl	%esi, %esi
	leaq	(%rsi,%rsi,4), %rax
	leaq	(%rsi,%rax,2), %rcx
	leaq	_ZL14timeScaleTable(%rip), %rax
	leaq	(%rax,%rcx,8), %rcx
	cmpq	%rdi, 16(%rcx)
	jg	.L12
	cmpq	%rdi, 24(%rcx)
	jl	.L12
	movq	8(%rcx), %rax
	addq	%rdi, %rax
	imulq	(%rcx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$1, (%rdx)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3:
	.size	utmscale_fromInt64_67, .-utmscale_fromInt64_67
	.p2align 4
	.globl	utmscale_toInt64_67
	.type	utmscale_toInt64_67, @function
utmscale_toInt64_67:
.LFB4:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L15
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L15
	cmpl	$9, %esi
	ja	.L18
	movl	%esi, %esi
	leaq	(%rsi,%rsi,4), %rax
	leaq	(%rsi,%rax,2), %rcx
	leaq	_ZL14timeScaleTable(%rip), %rax
	leaq	(%rax,%rcx,8), %rsi
	cmpq	%rdi, 32(%rsi)
	jg	.L18
	cmpq	%rdi, 40(%rsi)
	jl	.L18
	movq	64(%rsi), %rcx
	movq	(%rsi), %r8
	testq	%rdi, %rdi
	js	.L24
	cmpq	%rdi, 80(%rsi)
	jl	.L25
	leaq	(%rdi,%rcx), %rax
	cqto
	idivq	%r8
	subq	8(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$1, (%rdx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	cmpq	%rdi, 72(%rsi)
	jle	.L20
	leaq	(%rdi,%rcx), %rax
	cqto
	idivq	%r8
	subq	48(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%rdi, %rax
	subq	%rcx, %rax
	cqto
	idivq	%r8
	subq	8(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rdi, %rax
	subq	%rcx, %rax
	cqto
	idivq	%r8
	subq	56(%rsi), %rax
	ret
	.cfi_endproc
.LFE4:
	.size	utmscale_toInt64_67, .-utmscale_toInt64_67
	.section	.rodata
	.align 32
	.type	_ZL14timeScaleTable, @object
	.size	_ZL14timeScaleTable, 880
_ZL14timeScaleTable:
	.quad	10000
	.quad	62135596800000
	.quad	-984472800485477
	.quad	860201606885477
	.quad	-9223372036854774999
	.quad	9223372036854774999
	.quad	62135596800001
	.quad	62135596799999
	.quad	5000
	.quad	-9223372036854770808
	.quad	9223372036854770807
	.quad	10000000
	.quad	62135596800
	.quad	-984472800485
	.quad	860201606885
	.quad	-9223372036854775808
	.quad	9223372036854775807
	.quad	62135596801
	.quad	62135596799
	.quad	5000000
	.quad	-9223372036849775808
	.quad	9223372036849775807
	.quad	10000
	.quad	62135596800000
	.quad	-984472800485477
	.quad	860201606885477
	.quad	-9223372036854774999
	.quad	9223372036854774999
	.quad	62135596800001
	.quad	62135596799999
	.quad	5000
	.quad	-9223372036854770808
	.quad	9223372036854770807
	.quad	1
	.quad	504911232000000000
	.quad	-9223372036854775808
	.quad	8718460804854775807
	.quad	-8718460804854775808
	.quad	9223372036854775807
	.quad	504911232000000000
	.quad	504911232000000000
	.quad	0
	.quad	-9223372036854775808
	.quad	9223372036854775807
	.quad	1
	.quad	0
	.quad	-9223372036854775808
	.quad	9223372036854775807
	.quad	-9223372036854775808
	.quad	9223372036854775807
	.quad	0
	.quad	0
	.quad	0
	.quad	-9223372036854775808
	.quad	9223372036854775807
	.quad	10000000
	.quad	60052752000
	.quad	-982389955685
	.quad	862284451685
	.quad	-9223372036854775808
	.quad	9223372036854775807
	.quad	60052752001
	.quad	60052751999
	.quad	5000000
	.quad	-9223372036849775808
	.quad	9223372036849775807
	.quad	10000000
	.quad	63113904000
	.quad	-985451107685
	.quad	859223299685
	.quad	-9223372036854775808
	.quad	9223372036854775807
	.quad	63113904001
	.quad	63113903999
	.quad	5000000
	.quad	-9223372036849775808
	.quad	9223372036849775807
	.quad	864000000000
	.quad	693594
	.quad	-11368793
	.quad	9981605
	.quad	-9223372036854775808
	.quad	9223372036854775807
	.quad	693595
	.quad	693593
	.quad	432000000000
	.quad	-9223371604854775808
	.quad	9223371604854775807
	.quad	864000000000
	.quad	693594
	.quad	-11368793
	.quad	9981605
	.quad	-9223372036854775808
	.quad	9223372036854775807
	.quad	693595
	.quad	693593
	.quad	432000000000
	.quad	-9223371604854775808
	.quad	9223371604854775807
	.quad	10
	.quad	62135596800000000
	.quad	-984472800485477580
	.quad	860201606885477580
	.quad	-9223372036854775804
	.quad	9223372036854775804
	.quad	62135596800000001
	.quad	62135596799999999
	.quad	5
	.quad	-9223372036854775803
	.quad	9223372036854775802
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
