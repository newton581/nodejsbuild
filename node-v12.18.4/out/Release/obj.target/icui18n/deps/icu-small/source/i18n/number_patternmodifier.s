	.file	"number_patternmodifier.cpp"
	.text
	.section	.text._ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE,"axG",@progbits,_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.type	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE, @function
_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE:
.LFB3290:
	.cfi_startproc
	endbr64
	leal	(%rsi,%rdx,4), %eax
	cltq
	movq	8(%rdi,%rax,8), %rax
	testq	%rax, %rax
	jne	.L1
	cmpl	$5, %edx
	je	.L1
	addl	$20, %esi
	movslq	%esi, %rsi
	movq	8(%rdi,%rsi,8), %rax
.L1:
	ret
	.cfi_endproc
.LFE3290:
	.size	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE, .-_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl22MutablePatternModifier8isStrongEv
	.type	_ZNK6icu_676number4impl22MutablePatternModifier8isStrongEv, @function
_ZNK6icu_676number4impl22MutablePatternModifier8isStrongEv:
.LFB3391:
	.cfi_startproc
	endbr64
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE3391:
	.size	_ZNK6icu_676number4impl22MutablePatternModifier8isStrongEv, .-_ZNK6icu_676number4impl22MutablePatternModifier8isStrongEv
	.section	.text.unlikely,"ax",@progbits
	.align 2
	.globl	_ZNK6icu_676number4impl22MutablePatternModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.type	_ZNK6icu_676number4impl22MutablePatternModifier13containsFieldENS_22FormattedStringBuilder5FieldE, @function
_ZNK6icu_676number4impl22MutablePatternModifier13containsFieldENS_22FormattedStringBuilder5FieldE:
.LFB3392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	abort@PLT
	.cfi_endproc
.LFE3392:
	.size	_ZNK6icu_676number4impl22MutablePatternModifier13containsFieldENS_22FormattedStringBuilder5FieldE, .-_ZNK6icu_676number4impl22MutablePatternModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.align 2
	.globl	_ZNK6icu_676number4impl22MutablePatternModifier13getParametersERNS1_8Modifier10ParametersE
	.type	_ZNK6icu_676number4impl22MutablePatternModifier13getParametersERNS1_8Modifier10ParametersE, @function
_ZNK6icu_676number4impl22MutablePatternModifier13getParametersERNS1_8Modifier10ParametersE:
.LFB3393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	abort@PLT
	.cfi_endproc
.LFE3393:
	.size	_ZNK6icu_676number4impl22MutablePatternModifier13getParametersERNS1_8Modifier10ParametersE, .-_ZNK6icu_676number4impl22MutablePatternModifier13getParametersERNS1_8Modifier10ParametersE
	.align 2
	.globl	_ZNK6icu_676number4impl22MutablePatternModifier22semanticallyEquivalentERKNS1_8ModifierE
	.type	_ZNK6icu_676number4impl22MutablePatternModifier22semanticallyEquivalentERKNS1_8ModifierE, @function
_ZNK6icu_676number4impl22MutablePatternModifier22semanticallyEquivalentERKNS1_8ModifierE:
.LFB3394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	abort@PLT
	.cfi_endproc
.LFE3394:
	.size	_ZNK6icu_676number4impl22MutablePatternModifier22semanticallyEquivalentERKNS1_8ModifierE, .-_ZNK6icu_676number4impl22MutablePatternModifier22semanticallyEquivalentERKNS1_8ModifierE
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"\375\377"
	.string	""
	.section	.text.unlikely
	.align 2
.LCOLDB1:
	.text
.LHOTB1:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE
	.type	_ZNK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE, @function
_ZNK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE:
.LFB3398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	cmpl	$-9, %edx
	jb	.L18
	leal	9(%rdx), %eax
	leaq	.L20(%rip), %rdx
	movq	%rdi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L20:
	.long	.L27-.L20
	.long	.L27-.L20
	.long	.L26-.L20
	.long	.L25-.L20
	.long	.L24-.L20
	.long	.L23-.L20
	.long	.L22-.L20
	.long	.L21-.L20
	.long	.L19-.L20
	.text
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	.LC0(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
.L17:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	leaq	-28(%rbp), %rdx
	addq	$72, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15CurrencySymbols21getIntlCurrencySymbolER10UErrorCode@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L22:
	movq	56(%rsi), %rsi
	addq	$200, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L21:
	movq	56(%rsi), %rsi
	addq	$456, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L19:
	movq	56(%rsi), %rsi
	addq	$392, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L24:
	movl	64(%rsi), %eax
	cmpl	$3, %eax
	je	.L25
	cmpl	$4, %eax
	je	.L35
	addq	$72, %rsi
	leaq	-28(%rbp), %rdx
	testl	%eax, %eax
	jne	.L31
	call	_ZNK6icu_676number4impl15CurrencySymbols23getNarrowCurrencySymbolER10UErrorCode@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L23:
	movq	56(%rsi), %rsi
	addq	$776, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L26:
	movl	316(%rsi), %edx
	leaq	-28(%rbp), %rcx
	addq	$72, %rsi
	call	_ZNK6icu_676number4impl15CurrencySymbols13getPluralNameENS_14StandardPlural4FormER10UErrorCode@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L31:
	call	_ZNK6icu_676number4impl15CurrencySymbols17getCurrencySymbolER10UErrorCode@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	jmp	.L17
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE.cold, @function
_ZNK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE.cold:
.LFSB3398:
.L18:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	call	abort@PLT
	.cfi_endproc
.LFE3398:
	.text
	.size	_ZNK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE, .-_ZNK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE
	.section	.text.unlikely
	.size	_ZNK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE.cold, .-_ZNK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE.cold
.LCOLDE1:
	.text
.LHOTE1:
	.section	.text._ZN6icu_676number4impl24ImmutablePatternModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl24ImmutablePatternModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.type	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev, @function
_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev:
.LFB4545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L37
	movq	(%rdi), %rax
	call	*8(%rax)
.L37:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.cfi_endproc
.LFE4545:
	.size	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev, .-_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD1Ev
	.set	_ZN6icu_676number4impl24ImmutablePatternModifierD1Ev,_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.section	.text.unlikely
	.globl	_ZThn8_NK6icu_676number4impl22MutablePatternModifier22semanticallyEquivalentERKNS1_8ModifierE
	.type	_ZThn8_NK6icu_676number4impl22MutablePatternModifier22semanticallyEquivalentERKNS1_8ModifierE, @function
_ZThn8_NK6icu_676number4impl22MutablePatternModifier22semanticallyEquivalentERKNS1_8ModifierE:
.LFB4559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	abort@PLT
	.cfi_endproc
.LFE4559:
	.size	_ZThn8_NK6icu_676number4impl22MutablePatternModifier22semanticallyEquivalentERKNS1_8ModifierE, .-_ZThn8_NK6icu_676number4impl22MutablePatternModifier22semanticallyEquivalentERKNS1_8ModifierE
	.text
	.p2align 4
	.globl	_ZThn8_NK6icu_676number4impl22MutablePatternModifier8isStrongEv
	.type	_ZThn8_NK6icu_676number4impl22MutablePatternModifier8isStrongEv, @function
_ZThn8_NK6icu_676number4impl22MutablePatternModifier8isStrongEv:
.LFB4560:
	.cfi_startproc
	endbr64
	movzbl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE4560:
	.size	_ZThn8_NK6icu_676number4impl22MutablePatternModifier8isStrongEv, .-_ZThn8_NK6icu_676number4impl22MutablePatternModifier8isStrongEv
	.section	.text.unlikely
	.globl	_ZThn8_NK6icu_676number4impl22MutablePatternModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.type	_ZThn8_NK6icu_676number4impl22MutablePatternModifier13containsFieldENS_22FormattedStringBuilder5FieldE, @function
_ZThn8_NK6icu_676number4impl22MutablePatternModifier13containsFieldENS_22FormattedStringBuilder5FieldE:
.LFB4561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	abort@PLT
	.cfi_endproc
.LFE4561:
	.size	_ZThn8_NK6icu_676number4impl22MutablePatternModifier13containsFieldENS_22FormattedStringBuilder5FieldE, .-_ZThn8_NK6icu_676number4impl22MutablePatternModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.globl	_ZThn8_NK6icu_676number4impl22MutablePatternModifier13getParametersERNS1_8Modifier10ParametersE
	.type	_ZThn8_NK6icu_676number4impl22MutablePatternModifier13getParametersERNS1_8Modifier10ParametersE, @function
_ZThn8_NK6icu_676number4impl22MutablePatternModifier13getParametersERNS1_8Modifier10ParametersE:
.LFB4562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	abort@PLT
	.cfi_endproc
.LFE4562:
	.size	_ZThn8_NK6icu_676number4impl22MutablePatternModifier13getParametersERNS1_8Modifier10ParametersE, .-_ZThn8_NK6icu_676number4impl22MutablePatternModifier13getParametersERNS1_8Modifier10ParametersE
	.section	.text._ZN6icu_676number4impl24ImmutablePatternModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl24ImmutablePatternModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev
	.type	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev, @function
_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev:
.LFB4547:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L50
	movq	(%rdi), %rax
	call	*8(%rax)
.L50:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4547:
	.size	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev, .-_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev
	.text
	.p2align 4
	.globl	_ZThn8_NK6icu_676number4impl22MutablePatternModifier15getPrefixLengthEv
	.type	_ZThn8_NK6icu_676number4impl22MutablePatternModifier15getPrefixLengthEv, @function
_ZThn8_NK6icu_676number4impl22MutablePatternModifier15getPrefixLengthEv:
.LFB4565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	320(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movzbl	40(%rdi), %r14d
	movl	304(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	308(%rdi), %r13d
	movl	36(%rdi), %edi
	movl	$0, -44(%rbp)
	call	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE@PLT
	movq	24(%rbx), %rdi
	movl	%r13d, %ecx
	movq	%r12, %r9
	movl	%eax, %edx
	movl	%r14d, %r8d
	movl	$1, %esi
	call	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	leaq	-44(%rbp), %rdx
	leaq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl10AffixUtils23unescapedCodePointCountERKNS_13UnicodeStringERKNS1_14SymbolProviderER10UErrorCode@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L58
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L58:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4565:
	.size	_ZThn8_NK6icu_676number4impl22MutablePatternModifier15getPrefixLengthEv, .-_ZThn8_NK6icu_676number4impl22MutablePatternModifier15getPrefixLengthEv
	.p2align 4
	.globl	_ZThn8_NK6icu_676number4impl22MutablePatternModifier17getCodePointCountEv
	.type	_ZThn8_NK6icu_676number4impl22MutablePatternModifier17getCodePointCountEv, @function
_ZThn8_NK6icu_676number4impl22MutablePatternModifier17getCodePointCountEv:
.LFB4568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-60(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	320(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movzbl	40(%rdi), %r14d
	movl	304(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	308(%rdi), %r12d
	movl	36(%rdi), %edi
	movl	$0, -60(%rbp)
	call	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE@PLT
	movq	24(%rbx), %rdi
	movq	%r13, %r9
	movl	%r14d, %r8d
	movl	%r12d, %ecx
	movl	%eax, %edx
	leaq	8(%rbx), %r14
	movl	$1, %esi
	call	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl10AffixUtils23unescapedCodePointCountERKNS_13UnicodeStringERKNS1_14SymbolProviderER10UErrorCode@PLT
	movzbl	40(%rbx), %r8d
	movl	308(%rbx), %ecx
	movl	304(%rbx), %esi
	movl	36(%rbx), %edi
	movl	%eax, %r12d
	movl	%r8d, -72(%rbp)
	movl	%ecx, -68(%rbp)
	call	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE@PLT
	movl	-68(%rbp), %ecx
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	movl	-72(%rbp), %r8d
	movl	%eax, %edx
	movq	%r13, %r9
	call	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl10AffixUtils23unescapedCodePointCountERKNS_13UnicodeStringERKNS1_14SymbolProviderER10UErrorCode@PLT
	addl	%r12d, %eax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L62
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L62:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4568:
	.size	_ZThn8_NK6icu_676number4impl22MutablePatternModifier17getCodePointCountEv, .-_ZThn8_NK6icu_676number4impl22MutablePatternModifier17getCodePointCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl22MutablePatternModifier15getPrefixLengthEv
	.type	_ZNK6icu_676number4impl22MutablePatternModifier15getPrefixLengthEv, @function
_ZNK6icu_676number4impl22MutablePatternModifier15getPrefixLengthEv:
.LFB3389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	328(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movzbl	48(%rdi), %r14d
	movl	312(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	316(%rdi), %r13d
	movl	44(%rdi), %edi
	movl	$0, -44(%rbp)
	call	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE@PLT
	movq	32(%rbx), %rdi
	movl	%r13d, %ecx
	movq	%r12, %r9
	movl	%eax, %edx
	movl	%r14d, %r8d
	movl	$1, %esi
	call	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	leaq	-44(%rbp), %rdx
	leaq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl10AffixUtils23unescapedCodePointCountERKNS_13UnicodeStringERKNS1_14SymbolProviderER10UErrorCode@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L66
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L66:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3389:
	.size	_ZNK6icu_676number4impl22MutablePatternModifier15getPrefixLengthEv, .-_ZNK6icu_676number4impl22MutablePatternModifier15getPrefixLengthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl22MutablePatternModifier17getCodePointCountEv
	.type	_ZNK6icu_676number4impl22MutablePatternModifier17getCodePointCountEv, @function
_ZNK6icu_676number4impl22MutablePatternModifier17getCodePointCountEv:
.LFB3390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-60(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	328(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movzbl	48(%rdi), %r14d
	movl	312(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	316(%rdi), %r12d
	movl	44(%rdi), %edi
	movl	$0, -60(%rbp)
	call	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE@PLT
	movq	32(%rbx), %rdi
	movq	%r13, %r9
	movl	%r14d, %r8d
	movl	%r12d, %ecx
	movl	%eax, %edx
	leaq	16(%rbx), %r14
	movl	$1, %esi
	call	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl10AffixUtils23unescapedCodePointCountERKNS_13UnicodeStringERKNS1_14SymbolProviderER10UErrorCode@PLT
	movzbl	48(%rbx), %r8d
	movl	316(%rbx), %ecx
	movl	312(%rbx), %esi
	movl	44(%rbx), %edi
	movl	%eax, %r12d
	movl	%r8d, -72(%rbp)
	movl	%ecx, -68(%rbp)
	call	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE@PLT
	movl	-68(%rbp), %ecx
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	movl	-72(%rbp), %r8d
	movl	%eax, %edx
	movq	%r13, %r9
	call	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl10AffixUtils23unescapedCodePointCountERKNS_13UnicodeStringERKNS1_14SymbolProviderER10UErrorCode@PLT
	addl	%r12d, %eax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L70
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L70:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3390:
	.size	_ZNK6icu_676number4impl22MutablePatternModifier17getCodePointCountEv, .-_ZNK6icu_676number4impl22MutablePatternModifier17getCodePointCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl22MutablePatternModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.type	_ZNK6icu_676number4impl22MutablePatternModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, @function
_ZNK6icu_676number4impl22MutablePatternModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode:
.LFB3388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	328(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movl	%edx, -132(%rbp)
	movzbl	48(%rdi), %r8d
	movl	%ecx, -136(%rbp)
	movl	316(%rdi), %r13d
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movl	312(%rdi), %esi
	movl	44(%rdi), %edi
	movl	%r8d, -140(%rbp)
	call	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %r9
	movl	%r13d, %ecx
	movl	-140(%rbp), %r8d
	movl	%eax, %edx
	movl	$1, %esi
	call	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	leaq	16(%rbx), %r11
	movq	%r15, %r9
	movq	%r12, %rdi
	movl	-132(%rbp), %edx
	movzbl	40(%rbx), %r8d
	movq	%r11, %rcx
	movq	%r14, %rsi
	movq	%r11, -160(%rbp)
	call	_ZN6icu_676number4impl10AffixUtils8unescapeERKNS_13UnicodeStringERNS_22FormattedStringBuilderEiRKNS1_14SymbolProviderENS6_5FieldER10UErrorCode@PLT
	movzbl	48(%rbx), %r8d
	movl	316(%rbx), %ecx
	movl	%eax, %r13d
	movl	-136(%rbp), %eax
	movl	312(%rbx), %esi
	movl	44(%rbx), %edi
	movl	%r8d, -152(%rbp)
	leal	(%rax,%r13), %r10d
	movl	%ecx, -140(%rbp)
	movl	%r10d, -144(%rbp)
	call	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %r9
	xorl	%esi, %esi
	movl	-152(%rbp), %r8d
	movl	-140(%rbp), %ecx
	movl	%eax, %edx
	call	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	movzbl	40(%rbx), %r8d
	movq	%r12, %rdi
	movq	%r15, %r9
	movl	-144(%rbp), %r10d
	movq	-160(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rcx
	movl	%r10d, %edx
	movl	%r10d, -140(%rbp)
	call	_ZN6icu_676number4impl10AffixUtils8unescapeERKNS_13UnicodeStringERNS_22FormattedStringBuilderEiRKNS1_14SymbolProviderENS6_5FieldER10UErrorCode@PLT
	movq	32(%rbx), %rdi
	movl	%eax, %r12d
	movq	(%rdi), %rax
	call	*80(%rax)
	movl	-140(%rbp), %r10d
	movl	%r13d, %r11d
	testb	%al, %al
	je	.L77
.L72:
	subq	$8, %rsp
	movq	56(%rbx), %r9
	movl	%r13d, %edx
	movl	%r12d, %r8d
	pushq	%r15
	movl	-132(%rbp), %esi
	movl	%r10d, %ecx
	movq	%r14, %rdi
	movl	%r11d, -136(%rbp)
	call	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier20applyCurrencySpacingERNS_22FormattedStringBuilderEiiiiRKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
	movl	-136(%rbp), %r11d
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	leal	(%r12,%r11), %eax
	jne	.L78
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	pushq	%r15
	leaq	-128(%rbp), %r11
	movl	%r10d, %edx
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movq	%r11, %rcx
	xorl	%r9d, %r9d
	movw	%ax, -120(%rbp)
	movl	-132(%rbp), %eax
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r11, -152(%rbp)
	leal	(%rax,%r13), %esi
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %eax
	pushq	%rax
	call	_ZN6icu_6722FormattedStringBuilder6spliceEiiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode@PLT
	movq	-152(%rbp), %r11
	movl	%eax, -140(%rbp)
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-140(%rbp), %eax
	movl	-136(%rbp), %r10d
	popq	%rdx
	popq	%rcx
	addl	%eax, %r10d
	leal	(%rax,%r13), %r11d
	addl	%r13d, %r10d
	jmp	.L72
.L78:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3388:
	.size	_ZNK6icu_676number4impl22MutablePatternModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, .-_ZNK6icu_676number4impl22MutablePatternModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.set	.LTHUNK0,_ZNK6icu_676number4impl22MutablePatternModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.p2align 4
	.globl	_ZThn8_NK6icu_676number4impl22MutablePatternModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.type	_ZThn8_NK6icu_676number4impl22MutablePatternModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, @function
_ZThn8_NK6icu_676number4impl22MutablePatternModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode:
.LFB4577:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK0
	.cfi_endproc
.LFE4577:
	.size	_ZThn8_NK6icu_676number4impl22MutablePatternModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, .-_ZThn8_NK6icu_676number4impl22MutablePatternModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.section	.text.unlikely
.LCOLDB2:
	.text
.LHOTB2:
	.p2align 4
	.globl	_ZThn16_NK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE
	.type	_ZThn16_NK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE, @function
_ZThn16_NK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE:
.LFB4569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	cmpl	$-9, %edx
	jb	.L80
	leal	9(%rdx), %eax
	leaq	.L82(%rip), %rdx
	movq	%rdi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L82:
	.long	.L89-.L82
	.long	.L89-.L82
	.long	.L88-.L82
	.long	.L87-.L82
	.long	.L86-.L82
	.long	.L85-.L82
	.long	.L84-.L82
	.long	.L83-.L82
	.long	.L81-.L82
	.text
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	.LC0(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
.L79:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	leaq	-28(%rbp), %rdx
	addq	$56, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15CurrencySymbols21getIntlCurrencySymbolER10UErrorCode@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L84:
	movq	40(%rsi), %rsi
	addq	$200, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L83:
	movq	40(%rsi), %rsi
	addq	$456, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L81:
	movq	40(%rsi), %rsi
	addq	$392, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L86:
	movl	48(%rsi), %eax
	cmpl	$3, %eax
	je	.L87
	cmpl	$4, %eax
	je	.L97
	addq	$56, %rsi
	leaq	-28(%rbp), %rdx
	testl	%eax, %eax
	jne	.L93
	call	_ZNK6icu_676number4impl15CurrencySymbols23getNarrowCurrencySymbolER10UErrorCode@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L85:
	movq	40(%rsi), %rsi
	addq	$776, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L88:
	movl	300(%rsi), %edx
	leaq	-28(%rbp), %rcx
	addq	$56, %rsi
	call	_ZNK6icu_676number4impl15CurrencySymbols13getPluralNameENS_14StandardPlural4FormER10UErrorCode@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L93:
	call	_ZNK6icu_676number4impl15CurrencySymbols17getCurrencySymbolER10UErrorCode@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L97:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	jmp	.L79
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZThn16_NK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE.cold, @function
_ZThn16_NK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE.cold:
.LFSB4569:
.L80:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	call	abort@PLT
	.cfi_endproc
.LFE4569:
	.text
	.size	_ZThn16_NK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE, .-_ZThn16_NK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE
	.section	.text.unlikely
	.size	_ZThn16_NK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE.cold, .-_ZThn16_NK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE.cold
.LCOLDE2:
	.text
.LHOTE2:
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3875:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3875:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3878:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L111
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L99
	cmpb	$0, 12(%rbx)
	jne	.L112
.L103:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L99:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L103
	.cfi_endproc
.LFE3878:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3881:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L115
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3881:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3884:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L118
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3884:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L124
.L120:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L125
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L125:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3886:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3887:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3887:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3888:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3888:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3889:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3889:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3890:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3890:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3891:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3891:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3892:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L141
	testl	%edx, %edx
	jle	.L141
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L144
.L133:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L133
	.cfi_endproc
.LFE3892:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L148
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L148
	testl	%r12d, %r12d
	jg	.L155
	cmpb	$0, 12(%rbx)
	jne	.L156
.L150:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L150
.L156:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L148:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3893:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L158
	movq	(%rdi), %r8
.L159:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L162
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L162
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L162:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3894:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3895:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L169
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3895:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3896:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3896:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3897:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3897:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3898:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3898:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3900:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3900:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3902:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3902:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl20AffixPatternProviderD2Ev
	.type	_ZN6icu_676number4impl20AffixPatternProviderD2Ev, @function
_ZN6icu_676number4impl20AffixPatternProviderD2Ev:
.LFB3345:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3345:
	.size	_ZN6icu_676number4impl20AffixPatternProviderD2Ev, .-_ZN6icu_676number4impl20AffixPatternProviderD2Ev
	.globl	_ZN6icu_676number4impl20AffixPatternProviderD1Ev
	.set	_ZN6icu_676number4impl20AffixPatternProviderD1Ev,_ZN6icu_676number4impl20AffixPatternProviderD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl20AffixPatternProviderD0Ev
	.type	_ZN6icu_676number4impl20AffixPatternProviderD0Ev, @function
_ZN6icu_676number4impl20AffixPatternProviderD0Ev:
.LFB3347:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3347:
	.size	_ZN6icu_676number4impl20AffixPatternProviderD0Ev, .-_ZN6icu_676number4impl20AffixPatternProviderD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl22MutablePatternModifierC2Eb
	.type	_ZN6icu_676number4impl22MutablePatternModifierC2Eb, @function
_ZN6icu_676number4impl22MutablePatternModifierC2Eb:
.LFB3361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	80(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -64(%rdi)
	movb	%sil, -56(%rdi)
	movups	%xmm0, -80(%rdi)
	call	_ZN6icu_6712CurrencyUnitC1Ev@PLT
	leaq	125(%rbx), %rax
	movl	$2, %edx
	movl	$2, %ecx
	movq	%rax, 112(%rbx)
	xorl	%eax, %eax
	movl	$2, %esi
	movw	%ax, 124(%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$0, 168(%rbx)
	movl	$40, 120(%rbx)
	movq	%rax, 176(%rbx)
	movw	%dx, 184(%rbx)
	movq	%rax, 240(%rbx)
	movw	%cx, 248(%rbx)
	movq	%rax, 328(%rbx)
	movw	%si, 336(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3361:
	.size	_ZN6icu_676number4impl22MutablePatternModifierC2Eb, .-_ZN6icu_676number4impl22MutablePatternModifierC2Eb
	.globl	_ZN6icu_676number4impl22MutablePatternModifierC1Eb
	.set	_ZN6icu_676number4impl22MutablePatternModifierC1Eb,_ZN6icu_676number4impl22MutablePatternModifierC2Eb
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl22MutablePatternModifier14setPatternInfoEPKNS1_20AffixPatternProviderENS_22FormattedStringBuilder5FieldE
	.type	_ZN6icu_676number4impl22MutablePatternModifier14setPatternInfoEPKNS1_20AffixPatternProviderENS_22FormattedStringBuilder5FieldE, @function
_ZN6icu_676number4impl22MutablePatternModifier14setPatternInfoEPKNS1_20AffixPatternProviderENS_22FormattedStringBuilder5FieldE:
.LFB3363:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	movb	%dl, 40(%rdi)
	ret
	.cfi_endproc
.LFE3363:
	.size	_ZN6icu_676number4impl22MutablePatternModifier14setPatternInfoEPKNS1_20AffixPatternProviderENS_22FormattedStringBuilder5FieldE, .-_ZN6icu_676number4impl22MutablePatternModifier14setPatternInfoEPKNS1_20AffixPatternProviderENS_22FormattedStringBuilder5FieldE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl22MutablePatternModifier20setPatternAttributesE18UNumberSignDisplayb
	.type	_ZN6icu_676number4impl22MutablePatternModifier20setPatternAttributesE18UNumberSignDisplayb, @function
_ZN6icu_676number4impl22MutablePatternModifier20setPatternAttributesE18UNumberSignDisplayb:
.LFB3364:
	.cfi_startproc
	endbr64
	movl	%esi, 44(%rdi)
	movb	%dl, 48(%rdi)
	ret
	.cfi_endproc
.LFE3364:
	.size	_ZN6icu_676number4impl22MutablePatternModifier20setPatternAttributesE18UNumberSignDisplayb, .-_ZN6icu_676number4impl22MutablePatternModifier20setPatternAttributesE18UNumberSignDisplayb
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl22MutablePatternModifier19setNumberPropertiesENS1_6SignumENS_14StandardPlural4FormE
	.type	_ZN6icu_676number4impl22MutablePatternModifier19setNumberPropertiesENS1_6SignumENS_14StandardPlural4FormE, @function
_ZN6icu_676number4impl22MutablePatternModifier19setNumberPropertiesENS1_6SignumENS_14StandardPlural4FormE:
.LFB3367:
	.cfi_startproc
	endbr64
	movl	%esi, 312(%rdi)
	movl	%edx, 316(%rdi)
	ret
	.cfi_endproc
.LFE3367:
	.size	_ZN6icu_676number4impl22MutablePatternModifier19setNumberPropertiesENS1_6SignumENS_14StandardPlural4FormE, .-_ZN6icu_676number4impl22MutablePatternModifier19setNumberPropertiesENS1_6SignumENS_14StandardPlural4FormE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl22MutablePatternModifier12needsPluralsEv
	.type	_ZNK6icu_676number4impl22MutablePatternModifier12needsPluralsEv, @function
_ZNK6icu_676number4impl22MutablePatternModifier12needsPluralsEv:
.LFB3368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-7, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	32(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rdx
	movl	$0, -12(%rbp)
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L185
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L185:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3368:
	.size	_ZNK6icu_676number4impl22MutablePatternModifier12needsPluralsEv, .-_ZNK6icu_676number4impl22MutablePatternModifier12needsPluralsEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl22MutablePatternModifier22createConstantModifierER10UErrorCode
	.type	_ZN6icu_676number4impl22MutablePatternModifier22createConstantModifierER10UErrorCode, @function
_ZN6icu_676number4impl22MutablePatternModifier22createConstantModifierER10UErrorCode:
.LFB3376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-336(%rbp), %r14
	leaq	-192(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	leaq	328(%rbx), %r12
	subq	$312, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6722FormattedStringBuilderC1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6722FormattedStringBuilderC1Ev@PLT
	movzbl	48(%rbx), %r8d
	movl	316(%rbx), %ecx
	movl	312(%rbx), %esi
	movl	44(%rbx), %edi
	movl	%r8d, -344(%rbp)
	movl	%ecx, -340(%rbp)
	call	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %r9
	movl	$1, %esi
	movl	-344(%rbp), %r8d
	movl	-340(%rbp), %ecx
	movl	%eax, %edx
	call	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	leaq	16(%rbx), %r10
	movzbl	40(%rbx), %r8d
	xorl	%edx, %edx
	movq	%r10, %rcx
	movq	%r13, %r9
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r10, -352(%rbp)
	call	_ZN6icu_676number4impl10AffixUtils8unescapeERKNS_13UnicodeStringERNS_22FormattedStringBuilderEiRKNS1_14SymbolProviderENS6_5FieldER10UErrorCode@PLT
	movzbl	48(%rbx), %r8d
	movl	316(%rbx), %ecx
	movl	312(%rbx), %esi
	movl	44(%rbx), %edi
	movl	%r8d, -344(%rbp)
	movl	%ecx, -340(%rbp)
	call	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE@PLT
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %r9
	movl	-344(%rbp), %r8d
	movl	-340(%rbp), %ecx
	movl	%eax, %edx
	call	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	movzbl	40(%rbx), %r8d
	xorl	%edx, %edx
	movq	%r13, %r9
	movq	-352(%rbp), %r10
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r10, %rcx
	call	_ZN6icu_676number4impl10AffixUtils8unescapeERKNS_13UnicodeStringERNS_22FormattedStringBuilderEiRKNS1_14SymbolProviderENS6_5FieldER10UErrorCode@PLT
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	32(%rbx), %rdi
	testb	%al, %al
	movq	(%rdi), %rax
	je	.L187
	call	*80(%rax)
	movl	$832, %edi
	xorl	$1, %eax
	movzbl	%al, %ecx
	movl	%ecx, -340(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L188
	subq	$8, %rsp
	movq	56(%rbx), %r9
	movzbl	24(%rbx), %r8d
	movq	%r15, %rdx
	pushq	%r13
	movl	-340(%rbp), %ecx
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierC1ERKNS_22FormattedStringBuilderES5_bbRKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
	popq	%rax
	popq	%rdx
.L188:
	movq	%r15, %rdi
	call	_ZN6icu_6722FormattedStringBuilderD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6722FormattedStringBuilderD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	call	*80(%rax)
	movl	$304, %edi
	xorl	$1, %eax
	movl	%eax, %r13d
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L188
	leaq	16+_ZTVN6icu_676number4impl26ConstantMultiFieldModifierE(%rip), %rax
	movzbl	24(%rbx), %ebx
	leaq	8(%r12), %rdi
	movq	%r14, %rsi
	movq	%rax, (%r12)
	call	_ZN6icu_6722FormattedStringBuilderC1ERKS0_@PLT
	leaq	144(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6722FormattedStringBuilderC1ERKS0_@PLT
	movb	%r13b, 280(%r12)
	leaq	288(%r12), %rdi
	movb	%bl, 281(%r12)
	call	_ZN6icu_676number4impl8Modifier10ParametersC1Ev@PLT
	jmp	.L188
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3376:
	.size	_ZN6icu_676number4impl22MutablePatternModifier22createConstantModifierER10UErrorCode, .-_ZN6icu_676number4impl22MutablePatternModifier22createConstantModifierER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl22MutablePatternModifier15createImmutableER10UErrorCode
	.type	_ZN6icu_676number4impl22MutablePatternModifier15createImmutableER10UErrorCode, @function
_ZN6icu_676number4impl22MutablePatternModifier15createImmutableER10UErrorCode:
.LFB3369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$200, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L199
	leaq	16+_ZTVN6icu_676number4impl21AdoptingModifierStoreE(%rip), %rdx
	leaq	8(%rax), %rdi
	movl	$24, %ecx
	movl	$-7, %esi
	movq	%rdx, (%rax)
	xorl	%eax, %eax
	leaq	-60(%rbp), %rdx
	rep stosq
	movq	32(%r12), %rdi
	movl	$0, -60(%rbp)
	movq	(%rdi), %rax
	call	*72(%rax)
	testb	%al, %al
	je	.L200
	leaq	_ZZN6icu_676number4impl22MutablePatternModifier15createImmutableER10UErrorCodeE22STANDARD_PLURAL_VALUES(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L201:
	movl	(%r15), %r14d
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addq	$4, %r15
	movl	$3, 312(%r12)
	movl	%r14d, 316(%r12)
	call	_ZN6icu_676number4impl22MutablePatternModifier22createConstantModifierER10UErrorCode
	movq	-72(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r8
	leal	0(,%r14,4), %eax
	cltq
	leaq	(%rcx,%rax,8), %r13
	movq	%r8, 32(%r13)
	movl	$1, 312(%r12)
	movl	%r14d, 316(%r12)
	call	_ZN6icu_676number4impl22MutablePatternModifier22createConstantModifierER10UErrorCode
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, 16(%r13)
	movl	$2, 312(%r12)
	movl	%r14d, 316(%r12)
	call	_ZN6icu_676number4impl22MutablePatternModifier22createConstantModifierER10UErrorCode
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, 24(%r13)
	movl	$0, 312(%r12)
	movl	%r14d, 316(%r12)
	call	_ZN6icu_676number4impl22MutablePatternModifier22createConstantModifierER10UErrorCode
	movq	%rax, 8(%r13)
	leaq	24+_ZZN6icu_676number4impl22MutablePatternModifier15createImmutableER10UErrorCodeE22STANDARD_PLURAL_VALUES(%rip), %rax
	cmpq	%rax, %r15
	jne	.L201
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L214
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L198
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rcx
	movq	304(%r12), %rdx
	movq	%rcx, (%rax)
	movq	-72(%rbp), %rcx
	movq	%rdx, 16(%rax)
	movq	%rcx, 8(%rax)
	movq	$0, 24(%rax)
.L198:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L215
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movq	-72(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	xorl	%eax, %eax
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L200:
	movabsq	$25769803779, %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, 312(%r12)
	call	_ZN6icu_676number4impl22MutablePatternModifier22createConstantModifierER10UErrorCode
	movq	-72(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, 192(%r15)
	movabsq	$25769803777, %rax
	movq	%rax, 312(%r12)
	call	_ZN6icu_676number4impl22MutablePatternModifier22createConstantModifierER10UErrorCode
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, 176(%r15)
	movabsq	$25769803778, %rax
	movq	%rax, 312(%r12)
	call	_ZN6icu_676number4impl22MutablePatternModifier22createConstantModifierER10UErrorCode
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, 184(%r15)
	movabsq	$25769803776, %rax
	movq	%rax, 312(%r12)
	call	_ZN6icu_676number4impl22MutablePatternModifier22createConstantModifierER10UErrorCode
	movq	%rax, 168(%r15)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L214
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L198
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rdx
	movq	$0, 16(%rax)
	movq	%rdx, (%rax)
	movq	-72(%rbp), %rdx
	movq	$0, 24(%rax)
	movq	%rdx, 8(%rax)
	jmp	.L198
.L215:
	call	__stack_chk_fail@PLT
.L199:
	movl	$7, (%rbx)
	xorl	%eax, %eax
	jmp	.L198
	.cfi_endproc
.LFE3369:
	.size	_ZN6icu_676number4impl22MutablePatternModifier15createImmutableER10UErrorCode, .-_ZN6icu_676number4impl22MutablePatternModifier15createImmutableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl24ImmutablePatternModifierC2EPNS1_21AdoptingModifierStoreEPKNS_11PluralRulesE
	.type	_ZN6icu_676number4impl24ImmutablePatternModifierC2EPNS1_21AdoptingModifierStoreEPKNS_11PluralRulesE, @function
_ZN6icu_676number4impl24ImmutablePatternModifierC2EPNS1_21AdoptingModifierStoreEPKNS_11PluralRulesE:
.LFB3380:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	movq	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE3380:
	.size	_ZN6icu_676number4impl24ImmutablePatternModifierC2EPNS1_21AdoptingModifierStoreEPKNS_11PluralRulesE, .-_ZN6icu_676number4impl24ImmutablePatternModifierC2EPNS1_21AdoptingModifierStoreEPKNS_11PluralRulesE
	.globl	_ZN6icu_676number4impl24ImmutablePatternModifierC1EPNS1_21AdoptingModifierStoreEPKNS_11PluralRulesE
	.set	_ZN6icu_676number4impl24ImmutablePatternModifierC1EPNS1_21AdoptingModifierStoreEPKNS_11PluralRulesE,_ZN6icu_676number4impl24ImmutablePatternModifierC2EPNS1_21AdoptingModifierStoreEPKNS_11PluralRulesE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl24ImmutablePatternModifier11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.type	_ZNK6icu_676number4impl24ImmutablePatternModifier11getModifierENS1_6SignumENS_14StandardPlural4FormE, @function
_ZNK6icu_676number4impl24ImmutablePatternModifier11getModifierENS1_6SignumENS_14StandardPlural4FormE:
.LFB3384:
	.cfi_startproc
	endbr64
	cmpq	$0, 16(%rdi)
	movq	8(%rdi), %r8
	je	.L227
	movq	(%r8), %rax
	leaq	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE(%rip), %rcx
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L220
	leal	(%rsi,%rdx,4), %eax
	cltq
	movq	8(%r8,%rax,8), %rax
	testq	%rax, %rax
	jne	.L217
	cmpl	$5, %edx
	je	.L217
.L227:
	addl	$20, %esi
	movslq	%esi, %rsi
	movq	8(%r8,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE3384:
	.size	_ZNK6icu_676number4impl24ImmutablePatternModifier11getModifierENS1_6SignumENS_14StandardPlural4FormE, .-_ZNK6icu_676number4impl24ImmutablePatternModifier11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl24ImmutablePatternModifier10addToChainEPKNS1_19MicroPropsGeneratorE
	.type	_ZN6icu_676number4impl24ImmutablePatternModifier10addToChainEPKNS1_19MicroPropsGeneratorE, @function
_ZN6icu_676number4impl24ImmutablePatternModifier10addToChainEPKNS1_19MicroPropsGeneratorE:
.LFB3385:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE3385:
	.size	_ZN6icu_676number4impl24ImmutablePatternModifier10addToChainEPKNS1_19MicroPropsGeneratorE, .-_ZN6icu_676number4impl24ImmutablePatternModifier10addToChainEPKNS1_19MicroPropsGeneratorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl22MutablePatternModifier10addToChainEPKNS1_19MicroPropsGeneratorE
	.type	_ZN6icu_676number4impl22MutablePatternModifier10addToChainEPKNS1_19MicroPropsGeneratorE, @function
_ZN6icu_676number4impl22MutablePatternModifier10addToChainEPKNS1_19MicroPropsGeneratorE:
.LFB3386:
	.cfi_startproc
	endbr64
	movq	%rsi, 320(%rdi)
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE3386:
	.size	_ZN6icu_676number4impl22MutablePatternModifier10addToChainEPKNS1_19MicroPropsGeneratorE, .-_ZN6icu_676number4impl22MutablePatternModifier10addToChainEPKNS1_19MicroPropsGeneratorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl22MutablePatternModifier12insertPrefixERNS_22FormattedStringBuilderEiR10UErrorCode
	.type	_ZN6icu_676number4impl22MutablePatternModifier12insertPrefixERNS_22FormattedStringBuilderEiR10UErrorCode, @function
_ZN6icu_676number4impl22MutablePatternModifier12insertPrefixERNS_22FormattedStringBuilderEiR10UErrorCode:
.LFB3395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	328(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzbl	48(%rdi), %r8d
	movl	316(%rdi), %ecx
	movl	312(%rdi), %esi
	movl	44(%rdi), %edi
	movl	%r8d, -56(%rbp)
	movl	%ecx, -52(%rbp)
	call	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE@PLT
	movq	32(%rbx), %rdi
	movl	-56(%rbp), %r8d
	movq	%r15, %r9
	movl	-52(%rbp), %ecx
	movl	%eax, %edx
	movl	$1, %esi
	call	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	movzbl	40(%rbx), %r8d
	movq	%r14, %r9
	movl	%r13d, %edx
	addq	$24, %rsp
	leaq	16(%rbx), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl10AffixUtils8unescapeERKNS_13UnicodeStringERNS_22FormattedStringBuilderEiRKNS1_14SymbolProviderENS6_5FieldER10UErrorCode@PLT
	.cfi_endproc
.LFE3395:
	.size	_ZN6icu_676number4impl22MutablePatternModifier12insertPrefixERNS_22FormattedStringBuilderEiR10UErrorCode, .-_ZN6icu_676number4impl22MutablePatternModifier12insertPrefixERNS_22FormattedStringBuilderEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl22MutablePatternModifier12insertSuffixERNS_22FormattedStringBuilderEiR10UErrorCode
	.type	_ZN6icu_676number4impl22MutablePatternModifier12insertSuffixERNS_22FormattedStringBuilderEiR10UErrorCode, @function
_ZN6icu_676number4impl22MutablePatternModifier12insertSuffixERNS_22FormattedStringBuilderEiR10UErrorCode:
.LFB3396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	328(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzbl	48(%rdi), %r8d
	movl	316(%rdi), %ecx
	movl	312(%rdi), %esi
	movl	44(%rdi), %edi
	movl	%r8d, -56(%rbp)
	movl	%ecx, -52(%rbp)
	call	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE@PLT
	movq	32(%rbx), %rdi
	movl	-56(%rbp), %r8d
	movq	%r15, %r9
	movl	-52(%rbp), %ecx
	movl	%eax, %edx
	xorl	%esi, %esi
	call	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	movzbl	40(%rbx), %r8d
	movq	%r14, %r9
	movl	%r13d, %edx
	addq	$24, %rsp
	leaq	16(%rbx), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl10AffixUtils8unescapeERKNS_13UnicodeStringERNS_22FormattedStringBuilderEiRKNS1_14SymbolProviderENS6_5FieldER10UErrorCode@PLT
	.cfi_endproc
.LFE3396:
	.size	_ZN6icu_676number4impl22MutablePatternModifier12insertSuffixERNS_22FormattedStringBuilderEiR10UErrorCode, .-_ZN6icu_676number4impl22MutablePatternModifier12insertSuffixERNS_22FormattedStringBuilderEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl22MutablePatternModifier12prepareAffixEb
	.type	_ZN6icu_676number4impl22MutablePatternModifier12prepareAffixEb, @function
_ZN6icu_676number4impl22MutablePatternModifier12prepareAffixEb:
.LFB3397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movzbl	48(%rdi), %r14d
	movl	316(%rdi), %r13d
	movl	312(%rdi), %esi
	movl	44(%rdi), %edi
	call	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE@PLT
	movq	32(%rbx), %rdi
	movzbl	%r12b, %esi
	movl	%r14d, %r8d
	leaq	328(%rbx), %r9
	movl	%r13d, %ecx
	popq	%rbx
	movl	%eax, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	.cfi_endproc
.LFE3397:
	.size	_ZN6icu_676number4impl22MutablePatternModifier12prepareAffixEb, .-_ZN6icu_676number4impl22MutablePatternModifier12prepareAffixEb
	.section	.text.unlikely
	.align 2
	.globl	_ZNK6icu_676number4impl22MutablePatternModifier15toUnicodeStringEv
	.type	_ZNK6icu_676number4impl22MutablePatternModifier15toUnicodeStringEv, @function
_ZNK6icu_676number4impl22MutablePatternModifier15toUnicodeStringEv:
.LFB3399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	abort@PLT
	.cfi_endproc
.LFE3399:
	.size	_ZNK6icu_676number4impl22MutablePatternModifier15toUnicodeStringEv, .-_ZNK6icu_676number4impl22MutablePatternModifier15toUnicodeStringEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl22MutablePatternModifier10setSymbolsEPKNS_20DecimalFormatSymbolsERKNS_12CurrencyUnitE16UNumberUnitWidthPKNS_11PluralRulesER10UErrorCode
	.type	_ZN6icu_676number4impl22MutablePatternModifier10setSymbolsEPKNS_20DecimalFormatSymbolsERKNS_12CurrencyUnitE16UNumberUnitWidthPKNS_11PluralRulesER10UErrorCode, @function
_ZN6icu_676number4impl22MutablePatternModifier10setSymbolsEPKNS_20DecimalFormatSymbolsERKNS_12CurrencyUnitE16UNumberUnitWidthPKNS_11PluralRulesER10UErrorCode:
.LFB3365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-544(%rbp), %r14
	leaq	-512(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$536, %rsp
	movq	%r9, -560(%rbp)
	movq	%r8, -552(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, 56(%rdi)
	movq	%r14, %rdi
	call	_ZN6icu_6712CurrencyUnitC1ERKS0_@PLT
	leaq	1872(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	-560(%rbp), %r8
	leaq	-288(%rbp), %rdi
	leaq	-280(%rbp), %r12
	call	_ZN6icu_676number4impl15CurrencySymbolsC1ENS_12CurrencyUnitERKNS_6LocaleERKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
	leaq	80(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6712CurrencyUnitaSERKS0_@PLT
	leaq	-248(%rbp), %rsi
	leaq	112(%rbx), %rdi
	call	_ZN6icu_6710CharStringaSEOS0_@PLT
	leaq	-184(%rbp), %r9
	leaq	176(%rbx), %rdi
	movq	%r9, %rsi
	movq	%r9, -568(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-120(%rbp), %r10
	leaq	240(%rbx), %rdi
	movq	%r10, %rsi
	movq	%r10, -560(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-560(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-568(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -236(%rbp)
	jne	.L242
.L239:
	movq	%r12, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	-552(%rbp), %rax
	movl	%r13d, 64(%rbx)
	movq	%rax, 304(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L243
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movq	-248(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L239
.L243:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3365:
	.size	_ZN6icu_676number4impl22MutablePatternModifier10setSymbolsEPKNS_20DecimalFormatSymbolsERKNS_12CurrencyUnitE16UNumberUnitWidthPKNS_11PluralRulesER10UErrorCode, .-_ZN6icu_676number4impl22MutablePatternModifier10setSymbolsEPKNS_20DecimalFormatSymbolsERKNS_12CurrencyUnitE16UNumberUnitWidthPKNS_11PluralRulesER10UErrorCode
	.section	.text._ZN6icu_676number4impl22MutablePatternModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl22MutablePatternModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl22MutablePatternModifierD2Ev
	.type	_ZN6icu_676number4impl22MutablePatternModifierD2Ev, @function
_ZN6icu_676number4impl22MutablePatternModifierD2Ev:
.LFB4541:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$328, %rdi
	subq	$8, %rsp
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r12)
	jne	.L247
.L245:
	leaq	80(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movq	112(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L245
	.cfi_endproc
.LFE4541:
	.size	_ZN6icu_676number4impl22MutablePatternModifierD2Ev, .-_ZN6icu_676number4impl22MutablePatternModifierD2Ev
	.weak	_ZN6icu_676number4impl22MutablePatternModifierD1Ev
	.set	_ZN6icu_676number4impl22MutablePatternModifierD1Ev,_ZN6icu_676number4impl22MutablePatternModifierD2Ev
	.section	.text._ZN6icu_676number4impl22MutablePatternModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl22MutablePatternModifierD5Ev,comdat
	.p2align 4
	.weak	_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev
	.type	_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev, @function
_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev:
.LFB4563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$312, %rdi
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	224(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	160(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 108(%rbx)
	jne	.L251
.L249:
	leaq	64(%rbx), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L249
	.cfi_endproc
.LFE4563:
	.size	_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev, .-_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev
	.p2align 4
	.weak	_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev
	.type	_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev, @function
_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev:
.LFB4564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 13, -24
	leaq	-8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$320, %rdi
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	232(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	168(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 116(%r12)
	jne	.L255
.L253:
	leaq	72(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	104(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L253
	.cfi_endproc
.LFE4564:
	.size	_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev, .-_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl22MutablePatternModifierD0Ev
	.type	_ZN6icu_676number4impl22MutablePatternModifierD0Ev, @function
_ZN6icu_676number4impl22MutablePatternModifierD0Ev:
.LFB4543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$328, %rdi
	subq	$8, %rsp
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r12)
	jne	.L259
.L257:
	leaq	80(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	movq	112(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L257
	.cfi_endproc
.LFE4543:
	.size	_ZN6icu_676number4impl22MutablePatternModifierD0Ev, .-_ZN6icu_676number4impl22MutablePatternModifierD0Ev
	.section	.text._ZN6icu_676number4impl22MutablePatternModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl22MutablePatternModifierD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev
	.type	_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev, @function
_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev:
.LFB4566:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$320, %rdi
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	232(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	168(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 116(%r12)
	jne	.L263
.L261:
	leaq	72(%r12), %rdi
	leaq	-8(%r12), %r13
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	movq	104(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L261
	.cfi_endproc
.LFE4566:
	.size	_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev, .-_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev
	.p2align 4
	.weak	_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev
	.type	_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev, @function
_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev:
.LFB4567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$312, %rdi
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	224(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	160(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 108(%rbx)
	jne	.L267
.L265:
	leaq	64(%rbx), %rdi
	leaq	-16(%rbx), %r12
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L265
	.cfi_endproc
.LFE4567:
	.size	_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev, .-_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl22MutablePatternModifier15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.type	_ZNK6icu_676number4impl22MutablePatternModifier15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode, @function
_ZNK6icu_676number4impl22MutablePatternModifier15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode:
.LFB3387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	8(%r12), %r15
	subq	$184, %rsp
	movq	320(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode@PLT
	cmpq	$0, 120(%r12)
	je	.L277
.L268:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L278
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	leaq	-208(%rbp), %r8
	movl	$-7, %esi
	movl	$0, -208(%rbp)
	movq	%r8, -216(%rbp)
	movq	%r8, %rdx
	movq	(%rdi), %rax
	call	*72(%rax)
	testb	%al, %al
	je	.L270
	movq	-216(%rbp), %r8
	movq	304(%rbx), %r9
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%r9, -224(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1ERKS2_@PLT
	movq	-216(%rbp), %r8
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode@PLT
	movl	(%r14), %eax
	movq	-216(%rbp), %r8
	testl	%eax, %eax
	jg	.L275
	movq	-224(%rbp), %r9
	testq	%r9, %r9
	je	.L275
	leaq	-128(%rbp), %r15
	movq	%r8, %rdx
	movq	%r9, %rsi
	movl	$5, %r14d
	movq	%r15, %rdi
	call	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringERKNS_13UnicodeStringE@PLT
	movq	%r15, %rdi
	testl	%eax, %eax
	cmovns	%eax, %r14d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-216(%rbp), %r8
.L271:
	movq	%r8, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	%r13, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity6signumEv@PLT
	movl	%r14d, 316(%rbx)
	movl	%eax, 312(%rbx)
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L270:
	movq	%r13, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity6signumEv@PLT
	movl	$6, 316(%rbx)
	movl	%eax, 312(%rbx)
.L273:
	addq	$8, %rbx
	movq	%rbx, 120(%r12)
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L275:
	movl	$5, %r14d
	jmp	.L271
.L278:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3387:
	.size	_ZNK6icu_676number4impl22MutablePatternModifier15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode, .-_ZNK6icu_676number4impl22MutablePatternModifier15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl24ImmutablePatternModifier13applyToMicrosERNS1_10MicroPropsERKNS1_15DecimalQuantityER10UErrorCode
	.type	_ZNK6icu_676number4impl24ImmutablePatternModifier13applyToMicrosERNS1_10MicroPropsERKNS1_15DecimalQuantityER10UErrorCode, @function
_ZNK6icu_676number4impl24ImmutablePatternModifier13applyToMicrosERNS1_10MicroPropsERKNS1_15DecimalQuantityER10UErrorCode:
.LFB3383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$184, %rsp
	movq	16(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L296
	leaq	-208(%rbp), %r15
	movq	%rcx, %r14
	movq	%rdx, %rsi
	movq	%r8, -216(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityC1ERKS2_@PLT
	leaq	8(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L282
	movq	-216(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rdi, -216(%rbp)
	movq	%r8, %rsi
	call	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE@PLT
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringERKNS_13UnicodeStringE@PLT
	movq	-216(%rbp), %rdi
	testl	%eax, %eax
	movl	%eax, %r14d
	movl	%eax, %edx
	js	.L297
.L283:
	movl	%edx, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	8(%r12), %r12
	movq	%r13, %rdi
	movq	(%r12), %rax
	movq	16(%rax), %r15
	call	_ZNK6icu_676number4impl15DecimalQuantity6signumEv@PLT
	movl	-216(%rbp), %edx
	movl	%eax, %esi
	leaq	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE(%rip), %rax
	cmpq	%rax, %r15
	jne	.L284
	leal	(%rsi,%r14,4), %eax
	cltq
	movq	8(%r12,%rax,8), %rax
	testq	%rax, %rax
	jne	.L285
	cmpl	$5, %edx
	je	.L285
.L286:
	addl	$20, %esi
	movslq	%esi, %rsi
	movq	8(%r12,%rsi,8), %rax
.L285:
	movq	%rax, 120(%rbx)
.L279:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L298
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	8(%r12), %r12
	movq	%r13, %rdi
	movq	(%r12), %rax
	movq	16(%rax), %r15
	call	_ZNK6icu_676number4impl15DecimalQuantity6signumEv@PLT
	movl	$5, %edx
	movl	%eax, %esi
	leaq	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE(%rip), %rax
	cmpq	%rax, %r15
	je	.L286
.L284:
	movq	%r12, %rdi
	call	*%r15
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L296:
	movq	8(%rdi), %r12
	movq	%rdx, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity6signumEv@PLT
	addl	$20, %eax
	cltq
	movq	8(%r12,%rax,8), %rax
	movq	%rax, 120(%rbx)
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L297:
	movl	$5, %edx
	movl	$5, %r14d
	jmp	.L283
.L298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3383:
	.size	_ZNK6icu_676number4impl24ImmutablePatternModifier13applyToMicrosERNS1_10MicroPropsERKNS1_15DecimalQuantityER10UErrorCode, .-_ZNK6icu_676number4impl24ImmutablePatternModifier13applyToMicrosERNS1_10MicroPropsERKNS1_15DecimalQuantityER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl24ImmutablePatternModifier15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.type	_ZNK6icu_676number4impl24ImmutablePatternModifier15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode, @function
_ZNK6icu_676number4impl24ImmutablePatternModifier15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode:
.LFB3382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	24(%rdi), %rdi
	movq	%rdx, %r12
	movq	(%rdi), %rax
	call	*16(%rax)
	leaq	8(%r12), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode@PLT
	cmpq	$0, 120(%r12)
	je	.L302
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676number4impl24ImmutablePatternModifier13applyToMicrosERNS1_10MicroPropsERKNS1_15DecimalQuantityER10UErrorCode
	.cfi_endproc
.LFE3382:
	.size	_ZNK6icu_676number4impl24ImmutablePatternModifier15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode, .-_ZNK6icu_676number4impl24ImmutablePatternModifier15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_676number4impl20AffixPatternProviderE
	.section	.rodata._ZTSN6icu_676number4impl20AffixPatternProviderE,"aG",@progbits,_ZTSN6icu_676number4impl20AffixPatternProviderE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl20AffixPatternProviderE, @object
	.size	_ZTSN6icu_676number4impl20AffixPatternProviderE, 44
_ZTSN6icu_676number4impl20AffixPatternProviderE:
	.string	"N6icu_676number4impl20AffixPatternProviderE"
	.weak	_ZTIN6icu_676number4impl20AffixPatternProviderE
	.section	.data.rel.ro._ZTIN6icu_676number4impl20AffixPatternProviderE,"awG",@progbits,_ZTIN6icu_676number4impl20AffixPatternProviderE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl20AffixPatternProviderE, @object
	.size	_ZTIN6icu_676number4impl20AffixPatternProviderE, 16
_ZTIN6icu_676number4impl20AffixPatternProviderE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl20AffixPatternProviderE
	.weak	_ZTSN6icu_676number4impl24ImmutablePatternModifierE
	.section	.rodata._ZTSN6icu_676number4impl24ImmutablePatternModifierE,"aG",@progbits,_ZTSN6icu_676number4impl24ImmutablePatternModifierE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl24ImmutablePatternModifierE, @object
	.size	_ZTSN6icu_676number4impl24ImmutablePatternModifierE, 48
_ZTSN6icu_676number4impl24ImmutablePatternModifierE:
	.string	"N6icu_676number4impl24ImmutablePatternModifierE"
	.weak	_ZTIN6icu_676number4impl24ImmutablePatternModifierE
	.section	.data.rel.ro._ZTIN6icu_676number4impl24ImmutablePatternModifierE,"awG",@progbits,_ZTIN6icu_676number4impl24ImmutablePatternModifierE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl24ImmutablePatternModifierE, @object
	.size	_ZTIN6icu_676number4impl24ImmutablePatternModifierE, 56
_ZTIN6icu_676number4impl24ImmutablePatternModifierE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl24ImmutablePatternModifierE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl19MicroPropsGeneratorE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTSN6icu_676number4impl22MutablePatternModifierE
	.section	.rodata._ZTSN6icu_676number4impl22MutablePatternModifierE,"aG",@progbits,_ZTSN6icu_676number4impl22MutablePatternModifierE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl22MutablePatternModifierE, @object
	.size	_ZTSN6icu_676number4impl22MutablePatternModifierE, 46
_ZTSN6icu_676number4impl22MutablePatternModifierE:
	.string	"N6icu_676number4impl22MutablePatternModifierE"
	.weak	_ZTIN6icu_676number4impl22MutablePatternModifierE
	.section	.data.rel.ro._ZTIN6icu_676number4impl22MutablePatternModifierE,"awG",@progbits,_ZTIN6icu_676number4impl22MutablePatternModifierE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl22MutablePatternModifierE, @object
	.size	_ZTIN6icu_676number4impl22MutablePatternModifierE, 88
_ZTIN6icu_676number4impl22MutablePatternModifierE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl22MutablePatternModifierE
	.long	0
	.long	4
	.quad	_ZTIN6icu_676number4impl19MicroPropsGeneratorE
	.quad	2
	.quad	_ZTIN6icu_676number4impl8ModifierE
	.quad	2050
	.quad	_ZTIN6icu_676number4impl14SymbolProviderE
	.quad	4098
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTVN6icu_676number4impl20AffixPatternProviderE
	.section	.data.rel.ro._ZTVN6icu_676number4impl20AffixPatternProviderE,"awG",@progbits,_ZTVN6icu_676number4impl20AffixPatternProviderE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl20AffixPatternProviderE, @object
	.size	_ZTVN6icu_676number4impl20AffixPatternProviderE, 104
_ZTVN6icu_676number4impl20AffixPatternProviderE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl20AffixPatternProviderE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_676number4impl24ImmutablePatternModifierE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl24ImmutablePatternModifierE,"awG",@progbits,_ZTVN6icu_676number4impl24ImmutablePatternModifierE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl24ImmutablePatternModifierE, @object
	.size	_ZTVN6icu_676number4impl24ImmutablePatternModifierE, 40
_ZTVN6icu_676number4impl24ImmutablePatternModifierE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl24ImmutablePatternModifierE
	.quad	_ZN6icu_676number4impl24ImmutablePatternModifierD1Ev
	.quad	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev
	.quad	_ZNK6icu_676number4impl24ImmutablePatternModifier15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.weak	_ZTVN6icu_676number4impl22MutablePatternModifierE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl22MutablePatternModifierE,"awG",@progbits,_ZTVN6icu_676number4impl22MutablePatternModifierE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl22MutablePatternModifierE, @object
	.size	_ZTVN6icu_676number4impl22MutablePatternModifierE, 232
_ZTVN6icu_676number4impl22MutablePatternModifierE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl22MutablePatternModifierE
	.quad	_ZN6icu_676number4impl22MutablePatternModifierD1Ev
	.quad	_ZN6icu_676number4impl22MutablePatternModifierD0Ev
	.quad	_ZNK6icu_676number4impl22MutablePatternModifier15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.quad	_ZNK6icu_676number4impl22MutablePatternModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.quad	_ZNK6icu_676number4impl22MutablePatternModifier15getPrefixLengthEv
	.quad	_ZNK6icu_676number4impl22MutablePatternModifier17getCodePointCountEv
	.quad	_ZNK6icu_676number4impl22MutablePatternModifier8isStrongEv
	.quad	_ZNK6icu_676number4impl22MutablePatternModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.quad	_ZNK6icu_676number4impl22MutablePatternModifier13getParametersERNS1_8Modifier10ParametersE
	.quad	_ZNK6icu_676number4impl22MutablePatternModifier22semanticallyEquivalentERKNS1_8ModifierE
	.quad	_ZNK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE
	.quad	-8
	.quad	_ZTIN6icu_676number4impl22MutablePatternModifierE
	.quad	_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev
	.quad	_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev
	.quad	_ZThn8_NK6icu_676number4impl22MutablePatternModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.quad	_ZThn8_NK6icu_676number4impl22MutablePatternModifier15getPrefixLengthEv
	.quad	_ZThn8_NK6icu_676number4impl22MutablePatternModifier17getCodePointCountEv
	.quad	_ZThn8_NK6icu_676number4impl22MutablePatternModifier8isStrongEv
	.quad	_ZThn8_NK6icu_676number4impl22MutablePatternModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.quad	_ZThn8_NK6icu_676number4impl22MutablePatternModifier13getParametersERNS1_8Modifier10ParametersE
	.quad	_ZThn8_NK6icu_676number4impl22MutablePatternModifier22semanticallyEquivalentERKNS1_8ModifierE
	.quad	-16
	.quad	_ZTIN6icu_676number4impl22MutablePatternModifierE
	.quad	_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev
	.quad	_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev
	.quad	_ZThn16_NK6icu_676number4impl22MutablePatternModifier9getSymbolENS1_16AffixPatternTypeE
	.section	.rodata
	.align 16
	.type	_ZZN6icu_676number4impl22MutablePatternModifier15createImmutableER10UErrorCodeE22STANDARD_PLURAL_VALUES, @object
	.size	_ZZN6icu_676number4impl22MutablePatternModifier15createImmutableER10UErrorCodeE22STANDARD_PLURAL_VALUES, 24
_ZZN6icu_676number4impl22MutablePatternModifier15createImmutableER10UErrorCodeE22STANDARD_PLURAL_VALUES:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.type	_ZN6icu_67L15kUndefinedFieldE, @object
	.size	_ZN6icu_67L15kUndefinedFieldE, 1
_ZN6icu_67L15kUndefinedFieldE:
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
