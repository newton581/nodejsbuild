	.file	"coleitr.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724CollationElementIterator17getDynamicClassIDEv
	.type	_ZNK6icu_6724CollationElementIterator17getDynamicClassIDEv, @function
_ZNK6icu_6724CollationElementIterator17getDynamicClassIDEv:
.LFB3359:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6724CollationElementIterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3359:
	.size	_ZNK6icu_6724CollationElementIterator17getDynamicClassIDEv, .-_ZNK6icu_6724CollationElementIterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_110MaxExpSink8handleCEEl, @function
_ZN6icu_6712_GLOBAL__N_110MaxExpSink8handleCEEl:
.LFB3393:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3393:
	.size	_ZN6icu_6712_GLOBAL__N_110MaxExpSink8handleCEEl, .-_ZN6icu_6712_GLOBAL__N_110MaxExpSink8handleCEEl
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIteratorD2Ev
	.type	_ZN6icu_6724CollationElementIteratorD2Ev, @function
_ZN6icu_6724CollationElementIteratorD2Ev:
.LFB3364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724CollationElementIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5
	movq	(%rdi), %rax
	call	*8(%rax)
.L5:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L6
	movq	(%rdi), %rax
	call	*8(%rax)
.L6:
	leaq	40(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3364:
	.size	_ZN6icu_6724CollationElementIteratorD2Ev, .-_ZN6icu_6724CollationElementIteratorD2Ev
	.globl	_ZN6icu_6724CollationElementIteratorD1Ev
	.set	_ZN6icu_6724CollationElementIteratorD1Ev,_ZN6icu_6724CollationElementIteratorD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_110MaxExpSinkD2Ev, @function
_ZN6icu_6712_GLOBAL__N_110MaxExpSinkD2Ev:
.LFB3396:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_110MaxExpSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6725ContractionsAndExpansions6CESinkD2Ev@PLT
	.cfi_endproc
.LFE3396:
	.size	_ZN6icu_6712_GLOBAL__N_110MaxExpSinkD2Ev, .-_ZN6icu_6712_GLOBAL__N_110MaxExpSinkD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_110MaxExpSinkD1Ev,_ZN6icu_6712_GLOBAL__N_110MaxExpSinkD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_110MaxExpSinkD0Ev, @function
_ZN6icu_6712_GLOBAL__N_110MaxExpSinkD0Ev:
.LFB3398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_110MaxExpSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6725ContractionsAndExpansions6CESinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3398:
	.size	_ZN6icu_6712_GLOBAL__N_110MaxExpSinkD0Ev, .-_ZN6icu_6712_GLOBAL__N_110MaxExpSinkD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_110MaxExpSink15handleExpansionEPKli, @function
_ZN6icu_6712_GLOBAL__N_110MaxExpSink15handleExpansionEPKli:
.LFB3394:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	jle	.L28
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-1(%rdx), %ecx
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	8(%rsi,%rcx,8), %rdi
	movabsq	$281470698455103, %rcx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L23:
	testq	%rcx, (%rax)
	je	.L20
	addq	$8, %rax
	addl	$2, %r12d
	cmpq	%rdi, %rax
	jne	.L23
.L21:
	movslq	%edx, %rdx
	movq	-8(%rsi,%rdx,8), %rax
	movq	%rax, %rdi
	movl	%eax, %ecx
	sarq	$32, %rdi
	shrl	$8, %ecx
	movl	%edi, %edx
	movl	%ecx, %esi
	andl	$65280, %esi
	sall	$16, %edx
	orl	%esi, %edx
	movl	%eax, %esi
	andl	$63, %esi
	orl	%esi, %edx
	movl	%edx, %r13d
	orb	$-64, %r13b
	testl	%edx, %edx
	jne	.L25
	movl	%eax, %esi
	xorw	%di, %di
	movzbl	%cl, %ecx
	shrl	$16, %esi
	andl	$65280, %esi
	orl	%edi, %esi
	orl	%ecx, %esi
	movl	%esi, %r13d
.L25:
	movq	8(%rbx), %rdi
	movl	%r13d, %esi
	call	uhash_igeti_67@PLT
	cmpl	%r12d, %eax
	jl	.L31
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	addq	$8, %rax
	addl	$1, %r12d
	cmpq	%rax, %rdi
	jne	.L23
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L31:
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	movl	%r12d, %edx
	popq	%rbx
	.cfi_restore 3
	movl	%r13d, %esi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uhash_iputi_67@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	ret
	.cfi_endproc
.LFE3394:
	.size	_ZN6icu_6712_GLOBAL__N_110MaxExpSink15handleExpansionEPKli, .-_ZN6icu_6712_GLOBAL__N_110MaxExpSink15handleExpansionEPKli
	.align 2
	.p2align 4
	.type	_ZN6icu_6724CollationElementIterator7setTextERKNS_13UnicodeStringER10UErrorCode.part.0, @function
_ZN6icu_6724CollationElementIterator7setTextERKNS_13UnicodeStringER10UErrorCode.part.0:
.LFB4471:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	addq	$40, %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	48(%r14), %eax
	testb	$17, %al
	jne	.L45
	leaq	50(%r14), %r12
	testb	$2, %al
	je	.L53
.L33:
	movq	16(%r14), %rdx
	movq	16(%rdx), %rdx
	movl	24(%rdx), %edx
	movl	%edx, %r13d
	shrl	%r13d
	andl	$1, %r13d
	andl	$1, %edx
	jne	.L35
	testw	%ax, %ax
	js	.L36
	sarl	$5, %eax
.L37:
	cltq
	movl	$416, %edi
	leaq	(%r12,%rax,2), %r15
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L38
	movq	16(%r14), %rax
	movq	%r12, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	8(%rax), %rax
	movb	%r13b, 388(%rbx)
	movl	$0, 368(%rbx)
	movq	(%rax), %rdx
	movq	%rax, 16(%rbx)
	leaq	48(%rbx), %rax
	movq	%rax, 32(%rbx)
	leaq	16+_ZTVN6icu_6722UTF16CollationIteratorE(%rip), %rax
	movq	%rdx, 8(%rbx)
	movl	$0, 24(%rbx)
	movl	$40, 40(%rbx)
	movb	$0, 44(%rbx)
	movq	$0, 376(%rbx)
	movl	$-1, 384(%rbx)
	movq	%rax, (%rbx)
	movq	%r15, 408(%rbx)
	movups	%xmm0, 392(%rbx)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L35:
	testw	%ax, %ax
	js	.L40
	sarl	$5, %eax
.L41:
	cltq
	movl	$528, %edi
	leaq	(%r12,%rax,2), %r15
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L38
	movq	16(%r14), %rax
	movq	%r12, %xmm0
	movq	%r12, %xmm1
	leaq	16+_ZTVN6icu_6725FCDUTF16CollationIteratorE(%rip), %rcx
	punpcklqdq	%xmm0, %xmm0
	movq	8(%rax), %rax
	movb	%r13b, 388(%rbx)
	movl	$0, 368(%rbx)
	movq	(%rax), %rdx
	movq	%rax, 16(%rbx)
	movb	$0, 44(%rbx)
	movq	48(%rax), %rax
	movq	%rdx, 8(%rbx)
	leaq	48(%rbx), %rdx
	movq	%rax, 448(%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 456(%rbx)
	movl	$2, %eax
	movups	%xmm0, 392(%rbx)
	movq	%r15, %xmm0
	movl	$0, 24(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movq	%rdx, 32(%rbx)
	movl	$40, 40(%rbx)
	movq	$0, 376(%rbx)
	movl	$-1, 384(%rbx)
	movq	%rcx, (%rbx)
	movq	%r12, 424(%rbx)
	movq	$0, 432(%rbx)
	movq	%r15, 440(%rbx)
	movw	%ax, 464(%rbx)
	movb	$1, 520(%rbx)
	movups	%xmm0, 408(%rbx)
.L39:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L44
	movq	(%rdi), %rax
	call	*8(%rax)
.L44:
	movq	%rbx, 8(%r14)
	movl	$0, 24(%r14)
	movb	$0, 28(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	64(%r14), %r12
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L36:
	movl	52(%r14), %eax
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L40:
	movl	52(%r14), %eax
	jmp	.L41
.L38:
	movq	-56(%rbp), %rax
	movl	$7, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L33
	.cfi_endproc
.LFE4471:
	.size	_ZN6icu_6724CollationElementIterator7setTextERKNS_13UnicodeStringER10UErrorCode.part.0, .-_ZN6icu_6724CollationElementIterator7setTextERKNS_13UnicodeStringER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIteratorD0Ev
	.type	_ZN6icu_6724CollationElementIteratorD0Ev, @function
_ZN6icu_6724CollationElementIteratorD0Ev:
.LFB3366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724CollationElementIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L55
	movq	(%rdi), %rax
	call	*8(%rax)
.L55:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L56
	movq	(%rdi), %rax
	call	*8(%rax)
.L56:
	leaq	40(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3366:
	.size	_ZN6icu_6724CollationElementIteratorD0Ev, .-_ZN6icu_6724CollationElementIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIterator16getStaticClassIDEv
	.type	_ZN6icu_6724CollationElementIterator16getStaticClassIDEv, @function
_ZN6icu_6724CollationElementIterator16getStaticClassIDEv:
.LFB3358:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6724CollationElementIterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3358:
	.size	_ZN6icu_6724CollationElementIterator16getStaticClassIDEv, .-_ZN6icu_6724CollationElementIterator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIteratorC2ERKS0_
	.type	_ZN6icu_6724CollationElementIteratorC2ERKS0_, @function
_ZN6icu_6724CollationElementIteratorC2ERKS0_:
.LFB3361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6724CollationElementIteratorE(%rip), %rax
	movl	$0, 24(%rdi)
	movq	%rax, (%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 40(%rdi)
	movl	$2, %eax
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movb	$0, 28(%rdi)
	movq	$0, 32(%rdi)
	movw	%ax, 48(%rdi)
	cmpq	%rsi, %rdi
	je	.L65
	movq	8(%rsi), %r13
	movq	%rdi, %rbx
	movq	%rsi, %r12
	testq	%r13, %r13
	je	.L69
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6725FCDUTF16CollationIteratorE(%rip), %rdx
	leaq	_ZTIN6icu_6717CollationIteratorE(%rip), %rsi
	movq	%r13, %rdi
	call	__dynamic_cast@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L70
	movl	$528, %edi
	leaq	50(%rbx), %r15
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L69
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6725FCDUTF16CollationIteratorC1ERKS0_PKDs@PLT
.L72:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L77
	movq	(%rdi), %rax
	call	*8(%rax)
.L77:
	movq	16(%r12), %rax
	movq	%r13, 8(%rbx)
	leaq	40(%r12), %rsi
	leaq	40(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movl	24(%r12), %eax
	movl	%eax, 24(%rbx)
	movzbl	28(%r12), %eax
	movb	%al, 28(%rbx)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L69:
	cmpb	$0, 28(%r12)
	js	.L97
.L65:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movq	32(%r12), %rsi
	testq	%rsi, %rsi
	je	.L65
	movl	8(%rsi), %r13d
	testl	%r13d, %r13d
	je	.L65
	movq	32(%rbx), %r14
	movl	$0, -60(%rbp)
	leaq	-60(%rbp), %r15
	testq	%r14, %r14
	je	.L99
.L74:
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_679UVector326assignERKS0_R10UErrorCode@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L70:
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6722UTF16CollationIteratorE(%rip), %rdx
	leaq	_ZTIN6icu_6717CollationIteratorE(%rip), %rsi
	movq	%r13, %rdi
	call	__dynamic_cast@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L69
	movl	$416, %edi
	leaq	50(%rbx), %r15
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L69
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722UTF16CollationIteratorC1ERKS0_PKDs@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L75
	movl	%r13d, %esi
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movq	%r14, 32(%rbx)
	movq	32(%r12), %rsi
	jmp	.L74
.L98:
	call	__stack_chk_fail@PLT
.L75:
	movq	$0, 32(%rbx)
	jmp	.L65
	.cfi_endproc
.LFE3361:
	.size	_ZN6icu_6724CollationElementIteratorC2ERKS0_, .-_ZN6icu_6724CollationElementIteratorC2ERKS0_
	.globl	_ZN6icu_6724CollationElementIteratorC1ERKS0_
	.set	_ZN6icu_6724CollationElementIteratorC1ERKS0_,_ZN6icu_6724CollationElementIteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724CollationElementIterator9getOffsetEv
	.type	_ZNK6icu_6724CollationElementIterator9getOffsetEv, @function
_ZNK6icu_6724CollationElementIterator9getOffsetEv:
.LFB3370:
	.cfi_startproc
	endbr64
	cmpb	$0, 28(%rdi)
	movq	8(%rdi), %r8
	js	.L117
.L101:
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	40(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L117:
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L101
	movl	8(%rax), %edx
	testl	%edx, %edx
	je	.L101
	movl	24(%r8), %ecx
	cmpl	$1, 24(%rdi)
	sbbl	$-1, %ecx
	testl	%ecx, %ecx
	js	.L105
	testl	%edx, %edx
	jle	.L105
	subl	%ecx, %edx
	xorl	%r8d, %r8d
	testl	%edx, %edx
	jle	.L100
	movq	24(%rax), %rax
	movslq	%ecx, %rcx
	movl	(%rax,%rcx,4), %r8d
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L105:
	xorl	%r8d, %r8d
.L100:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3370:
	.size	_ZNK6icu_6724CollationElementIterator9getOffsetEv, .-_ZNK6icu_6724CollationElementIterator9getOffsetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIterator4nextER10UErrorCode
	.type	_ZN6icu_6724CollationElementIterator4nextER10UErrorCode, @function
_ZN6icu_6724CollationElementIterator4nextER10UErrorCode:
.LFB3371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L156
	cmpb	$1, 28(%rdi)
	movq	%rdi, %rbx
	movq	%rsi, %r13
	jle	.L121
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jne	.L157
.L122:
	movq	8(%rbx), %r12
	movslq	368(%r12), %rax
	movl	24(%r12), %edx
	cmpl	%edx, %eax
	je	.L158
	jge	.L126
	leal	1(%rax), %edx
	movl	%edx, 368(%r12)
	movq	32(%r12), %rdx
	movq	(%rdx,%rax,8), %rdx
.L127:
	movabsq	$4311744768, %rax
	cmpq	%rax, %rdx
	je	.L156
.L142:
	movq	%rdx, %rcx
	movl	%edx, %esi
	movl	%edx, %eax
	sarq	$32, %rcx
	shrl	$8, %esi
	movl	%ecx, %edi
	shrl	$16, %eax
	andl	$65280, %esi
	xorw	%di, %di
	andl	$65280, %eax
	sall	$16, %ecx
	orl	%edi, %eax
	orl	%esi, %ecx
	movzbl	%dh, %edi
	andl	$63, %edx
	orl	%edi, %eax
	orl	%edx, %ecx
	jne	.L159
.L118:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L160
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	orb	$-64, %cl
	movl	%ecx, 24(%rbx)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L158:
	movl	$0, 368(%r12)
	xorl	%edx, %edx
.L125:
	movq	(%r12), %rax
	addl	$1, %edx
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	movl	%edx, 24(%r12)
	movq	%r13, %rdx
	call	*64(%rax)
	movl	%eax, %ecx
	movzbl	%al, %esi
	cmpb	$-65, %al
	jbe	.L155
	cmpl	$192, %esi
	je	.L161
	movq	16(%r12), %r9
.L138:
	cmpl	$193, %esi
	jne	.L139
	movslq	368(%r12), %rax
	leal	-193(%rcx), %edx
	salq	$32, %rdx
	leal	1(%rax), %ecx
	orq	$83887360, %rdx
	movl	%ecx, 368(%r12)
	movq	32(%r12), %rcx
	movq	%rdx, (%rcx,%rax,8)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L121:
	ja	.L162
	movb	$2, 28(%rbx)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L157:
	movl	$0, 24(%rdi)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L126:
	cmpl	$39, %edx
	jle	.L125
	leaq	24(%r12), %rdi
	movq	%r13, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode@PLT
	movl	%eax, %r8d
	movl	$-1, %eax
	testb	%r8b, %r8b
	je	.L118
	movl	24(%r12), %edx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L161:
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	js	.L163
	movq	16(%r12), %rdx
	movq	32(%rdx), %r9
	movq	(%r9), %rsi
	movq	16(%rsi), %rcx
	cmpl	$55295, %eax
	jg	.L133
	movl	%eax, %edx
	movq	(%rsi), %rsi
	andl	$31, %eax
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L134:
	movl	(%rcx,%rdx), %ecx
	movzbl	%cl, %esi
	cmpb	$-65, %cl
	ja	.L138
	.p2align 4,,10
	.p2align 3
.L155:
	movabsq	$-281474976710656, %rax
	movq	%rcx, %rdx
	sall	$16, %ecx
	salq	$32, %rdx
	andl	$-16777216, %ecx
	sall	$8, %esi
	andq	%rax, %rdx
	movl	%esi, %eax
	orq	%rcx, %rdx
	orq	%rax, %rdx
	movslq	368(%r12), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 368(%r12)
	movq	32(%r12), %rcx
	movq	%rdx, (%rcx,%rax,8)
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L139:
	movl	-44(%rbp), %edx
	movq	%r13, %r8
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode@PLT
	movq	%rax, %rdx
	jmp	.L127
.L162:
	movl	$27, (%rsi)
	.p2align 4,,10
	.p2align 3
.L156:
	movl	$-1, %eax
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L133:
	cmpl	$65535, %eax
	jg	.L135
	cmpl	$56320, %eax
	movl	$320, %edx
	movq	(%rsi), %rdi
	movl	$0, %esi
	cmovl	%edx, %esi
	movl	%eax, %edx
	sarl	$5, %edx
.L154:
	addl	%esi, %edx
	andl	$31, %eax
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L134
.L163:
	movslq	368(%r12), %rax
	movabsq	$4311744768, %rbx
	leal	1(%rax), %edx
	movl	%edx, 368(%r12)
	movq	32(%r12), %rdx
	movq	%rbx, (%rdx,%rax,8)
	movl	$-1, %eax
	jmp	.L118
.L135:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L134
	cmpl	44(%rsi), %eax
	jl	.L137
	movslq	48(%rsi), %rdx
	salq	$2, %rdx
	jmp	.L134
.L137:
	movl	%eax, %edx
	movq	(%rsi), %rdi
	movl	%eax, %esi
	sarl	$11, %edx
	sarl	$5, %esi
	addl	$2080, %edx
	andl	$63, %esi
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	jmp	.L154
.L160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3371:
	.size	_ZN6icu_6724CollationElementIterator4nextER10UErrorCode, .-_ZN6icu_6724CollationElementIterator4nextER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724CollationElementIteratorneERKS0_
	.type	_ZNK6icu_6724CollationElementIteratorneERKS0_, @function
_ZNK6icu_6724CollationElementIteratorneERKS0_:
.LFB3372:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L179
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rsi), %rsi
	movq	16(%rdi), %rdi
	cmpq	%rsi, %rdi
	je	.L169
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	jne	.L169
.L168:
	movl	$1, %eax
.L164:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movl	24(%r12), %eax
	cmpl	%eax, 24(%rbx)
	jne	.L168
	movzbl	28(%rbx), %edx
	movzbl	28(%r12), %eax
	cmpb	$1, %dl
	je	.L171
	cmpb	$1, %al
	movl	$0, %ecx
	cmove	%ecx, %eax
.L172:
	cmpb	%dl, %al
	jne	.L168
.L178:
	movswl	48(%r12), %edx
	movswl	48(%rbx), %eax
	movl	%edx, %ecx
	andl	$1, %ecx
	testb	$1, %al
	jne	.L173
	testw	%ax, %ax
	js	.L174
	sarl	$5, %eax
.L175:
	testw	%dx, %dx
	js	.L176
	sarl	$5, %edx
.L177:
	testb	%cl, %cl
	jne	.L168
	cmpl	%eax, %edx
	jne	.L168
	leaq	40(%r12), %rsi
	leaq	40(%rbx), %rdi
	movl	%eax, %edx
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L173:
	testb	%cl, %cl
	je	.L168
	movq	8(%rbx), %rdi
	movq	8(%r12), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	sete	%al
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	xorl	%edx, %edx
	cmpb	$1, %al
	jne	.L172
	jmp	.L178
.L176:
	movl	52(%r12), %edx
	jmp	.L177
.L174:
	movl	52(%rbx), %eax
	jmp	.L175
	.cfi_endproc
.LFE3372:
	.size	_ZNK6icu_6724CollationElementIteratorneERKS0_, .-_ZNK6icu_6724CollationElementIteratorneERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724CollationElementIteratoreqERKS0_
	.type	_ZNK6icu_6724CollationElementIteratoreqERKS0_, @function
_ZNK6icu_6724CollationElementIteratoreqERKS0_:
.LFB3373:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L210
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rsi), %rsi
	movq	16(%rdi), %rdi
	cmpq	%rsi, %rdi
	je	.L200
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	jne	.L200
.L199:
	xorl	%eax, %eax
.L195:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movl	24(%r12), %eax
	cmpl	%eax, 24(%rbx)
	jne	.L199
	movzbl	28(%rbx), %edx
	movzbl	28(%r12), %eax
	cmpb	$1, %dl
	je	.L202
	cmpb	$1, %al
	movl	$0, %ecx
	cmove	%ecx, %eax
.L203:
	cmpb	%dl, %al
	jne	.L199
.L209:
	movswl	48(%r12), %edx
	movswl	48(%rbx), %eax
	movl	%edx, %ecx
	andl	$1, %ecx
	testb	$1, %al
	jne	.L204
	testw	%ax, %ax
	js	.L205
	sarl	$5, %eax
.L206:
	testw	%dx, %dx
	js	.L207
	sarl	$5, %edx
.L208:
	cmpl	%eax, %edx
	jne	.L199
	testb	%cl, %cl
	jne	.L199
	leaq	40(%r12), %rsi
	leaq	40(%rbx), %rdi
	movl	%eax, %edx
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L204:
	testb	%cl, %cl
	je	.L199
	movq	8(%rbx), %rdi
	movq	8(%r12), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	setne	%al
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	xorl	%edx, %edx
	cmpb	$1, %al
	jne	.L203
	jmp	.L209
.L207:
	movl	52(%r12), %edx
	jmp	.L208
.L205:
	movl	52(%rbx), %eax
	jmp	.L206
	.cfi_endproc
.LFE3373:
	.size	_ZNK6icu_6724CollationElementIteratoreqERKS0_, .-_ZNK6icu_6724CollationElementIteratoreqERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIterator8previousER10UErrorCode
	.type	_ZN6icu_6724CollationElementIterator8previousER10UErrorCode, @function
_ZN6icu_6724CollationElementIterator8previousER10UErrorCode:
.LFB3374:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L263
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzbl	28(%rdi), %eax
	testb	%al, %al
	js	.L264
	je	.L265
	cmpb	$1, %al
	jne	.L234
	movb	$-1, 28(%rdi)
.L230:
	movq	32(%rbx), %r14
	testq	%r14, %r14
	je	.L266
.L235:
	movq	8(%rbx), %rdi
	xorl	%r13d, %r13d
	movl	24(%rdi), %edx
	testl	%edx, %edx
	jne	.L237
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	8(%rbx), %rdi
	movq	32(%rbx), %r14
	movl	%eax, %r13d
.L237:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_6717CollationIterator10previousCEERNS_9UVector32ER10UErrorCode@PLT
	movabsq	$4311744768, %rdx
	cmpq	%rdx, %rax
	je	.L262
	movq	%rax, %rdx
	movl	%eax, %ecx
	movl	%eax, %r15d
	sarq	$32, %rdx
	shrl	$8, %ecx
	shrl	$16, %r15d
	movl	%edx, %esi
	andl	$65280, %ecx
	sall	$16, %edx
	xorw	%si, %si
	andl	$65280, %r15d
	orl	%ecx, %edx
	orl	%esi, %r15d
	movzbl	%ah, %esi
	andl	$63, %eax
	orl	%esi, %r15d
	orl	%eax, %edx
	movl	%r15d, %r8d
	movl	%edx, %r14d
	jne	.L267
.L226:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movswl	48(%rbx), %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	testw	%si, %si
	js	.L232
	sarl	$5, %esi
.L233:
	call	*%rax
	movq	32(%rbx), %r14
	movb	$-1, 28(%rbx)
	testq	%r14, %r14
	jne	.L235
.L266:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L236
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movq	%r14, 32(%rbx)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L264:
	movl	24(%rdi), %r8d
	testl	%r8d, %r8d
	je	.L230
	movl	$0, 24(%rdi)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L267:
	movq	32(%rbx), %rcx
	movl	8(%rcx), %eax
	movq	%rcx, -56(%rbp)
	testl	%eax, %eax
	jne	.L241
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	-56(%rbp), %rcx
	movl	%eax, %r8d
	movslq	8(%rcx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L242
	cmpl	12(%rcx), %esi
	jle	.L243
.L242:
	movq	%r12, %rdx
	movq	%rcx, %rdi
	movl	%r8d, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L244
	movq	-56(%rbp), %rcx
	movl	-60(%rbp), %r8d
	movslq	8(%rcx), %rax
.L243:
	movq	24(%rcx), %rdx
	movl	%r8d, (%rdx,%rax,4)
	addl	$1, 8(%rcx)
.L244:
	movq	32(%rbx), %rcx
	movslq	8(%rcx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L245
	cmpl	12(%rcx), %esi
	jle	.L246
.L245:
	movq	%r12, %rdx
	movq	%rcx, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L241
	movq	-56(%rbp), %rcx
	movslq	8(%rcx), %rax
.L246:
	movq	24(%rcx), %rdx
	movl	%r13d, (%rdx,%rax,4)
	addl	$1, 8(%rcx)
.L241:
	movl	%r14d, %r8d
	movl	%r15d, 24(%rbx)
	orb	$-64, %r8b
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L232:
	movl	52(%rbx), %esi
	jmp	.L233
.L236:
	movq	$0, 32(%rbx)
	movl	$7, (%r12)
.L262:
	movl	$-1, %r8d
	jmp	.L226
.L234:
	movl	$27, (%rsi)
	movl	$-1, %r8d
	jmp	.L226
.L263:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	orl	$-1, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3374:
	.size	_ZN6icu_6724CollationElementIterator8previousER10UErrorCode, .-_ZN6icu_6724CollationElementIterator8previousER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIterator5resetEv
	.type	_ZN6icu_6724CollationElementIterator5resetEv, @function
_ZN6icu_6724CollationElementIterator5resetEv:
.LFB3375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movl	$0, 24(%rbx)
	movb	$0, 28(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3375:
	.size	_ZN6icu_6724CollationElementIterator5resetEv, .-_ZN6icu_6724CollationElementIterator5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIterator9setOffsetEiR10UErrorCode
	.type	_ZN6icu_6724CollationElementIterator9setOffsetEiR10UErrorCode, @function
_ZN6icu_6724CollationElementIterator9setOffsetEiR10UErrorCode:
.LFB3376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -68(%rbp)
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L270
	movq	%rdi, %r14
	testl	%esi, %esi
	jle	.L274
	movzwl	48(%rdi), %eax
	movq	%rdx, %r12
	testw	%ax, %ax
	js	.L275
	movswl	%ax, %edx
	movl	%esi, %ebx
	sarl	$5, %edx
.L276:
	cmpl	%edx, %ebx
	jge	.L274
	movslq	%ebx, %rcx
	leaq	(%rcx,%rcx), %r13
	leaq	40(%r14), %rcx
	movq	%rcx, -80(%rbp)
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L330:
	movswl	%ax, %edx
	movq	16(%r14), %rdi
	sarl	$5, %edx
	cmpl	%ebx, %edx
	jbe	.L280
.L331:
	testb	$2, %al
	je	.L281
	leaq	50(%r14), %rax
.L282:
	movzwl	(%rax,%r13), %r15d
	movl	%r15d, %esi
	call	_ZNK6icu_6717RuleBasedCollator8isUnsafeEi@PLT
	testb	%al, %al
	je	.L283
	andl	$-1024, %r15d
	cmpl	$55296, %r15d
	je	.L284
.L287:
	subq	$2, %r13
	subl	$1, %ebx
	je	.L304
	movzwl	48(%r14), %eax
.L288:
	testw	%ax, %ax
	jns	.L330
	movl	52(%r14), %edx
	movq	16(%r14), %rdi
	cmpl	%ebx, %edx
	ja	.L331
.L280:
	movl	$65535, %esi
	call	_ZNK6icu_6717RuleBasedCollator8isUnsafeEi@PLT
	testb	%al, %al
	jne	.L287
	.p2align 4,,10
	.p2align 3
.L283:
	cmpl	-68(%rbp), %ebx
	jl	.L304
	.p2align 4,,10
	.p2align 3
.L274:
	movq	8(%r14), %rdi
	movl	-68(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movl	$0, 24(%r14)
	movb	$1, 28(%r14)
.L270:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L332
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movl	52(%rdi), %edx
	movl	%esi, %ebx
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L333:
	cmpl	%eax, -68(%rbp)
	jl	.L308
	je	.L274
	movl	%eax, %ebx
.L304:
	movq	8(%r14), %rdi
	movl	%ebx, %esi
	leaq	-60(%rbp), %r13
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L334:
	addl	$1, %eax
	movl	%eax, 368(%r15)
.L290:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L270
	movq	8(%r14), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	cmpl	%eax, %ebx
	jne	.L333
.L303:
	movq	8(%r14), %r15
	movl	368(%r15), %eax
	movl	24(%r15), %edx
	cmpl	%edx, %eax
	jl	.L334
	cmpl	$39, %edx
	jle	.L291
	leaq	24(%r15), %rdi
	movq	%r12, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L290
	movl	24(%r15), %edx
.L291:
	movq	(%r15), %rax
	addl	$1, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%edx, 24(%r15)
	movq	%r12, %rdx
	call	*64(%rax)
	movl	%eax, %ecx
	movzbl	%al, %esi
	cmpb	$-65, %al
	ja	.L292
	movslq	368(%r15), %rdi
	movq	%rcx, %rdx
	sall	$16, %ecx
	movq	32(%r15), %r8
	salq	$32, %rdx
	andl	$-16777216, %ecx
	sall	$8, %esi
	leal	1(%rdi), %eax
	movl	%eax, 368(%r15)
	movabsq	$-281474976710656, %rax
	andq	%rax, %rdx
	orq	%rcx, %rdx
	orq	%rsi, %rdx
	movq	%rdx, (%r8,%rdi,8)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L281:
	movq	64(%r14), %rax
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L284:
	movq	-80(%rbp), %rdi
	movq	16(%r14), %r15
	movl	%ebx, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZNK6icu_6717RuleBasedCollator8isUnsafeEi@PLT
	testb	%al, %al
	jne	.L287
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L292:
	cmpl	$192, %esi
	je	.L335
	movq	16(%r15), %r11
.L300:
	cmpl	$193, %esi
	jne	.L301
	movl	368(%r15), %eax
	movq	32(%r15), %rsi
	leal	1(%rax), %edx
	movl	%edx, 368(%r15)
	movslq	%eax, %rdx
	leal	-193(%rcx), %eax
	salq	$32, %rax
	orq	$83887360, %rax
	movq	%rax, (%rsi,%rdx,8)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L301:
	movl	-60(%rbp), %edx
	movq	%r12, %r8
	movq	%r11, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode@PLT
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L335:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	js	.L336
	movq	16(%r15), %rdx
	movq	32(%rdx), %r11
	movq	(%r11), %rsi
	movq	16(%rsi), %rcx
	cmpl	$55295, %eax
	jg	.L295
	movl	%eax, %edx
	movq	(%rsi), %rsi
	sarl	$5, %edx
	movslq	%edx, %rdx
.L328:
	movzwl	(%rsi,%rdx,2), %edx
	andl	$31, %eax
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L296:
	movl	(%rcx,%rdx), %ecx
	movzbl	%cl, %esi
	cmpb	$-65, %cl
	ja	.L300
	movslq	368(%r15), %rdi
	movq	%rcx, %rdx
	sall	$16, %ecx
	movq	32(%r15), %r8
	salq	$32, %rdx
	andl	$-16777216, %ecx
	sall	$8, %esi
	leal	1(%rdi), %eax
	movl	%eax, 368(%r15)
	movabsq	$-281474976710656, %rax
	andq	%rax, %rdx
	movl	%esi, %eax
	orq	%rcx, %rdx
	orq	%rax, %rdx
	movq	%rdx, (%r8,%rdi,8)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L308:
	movl	%ebx, -68(%rbp)
	jmp	.L274
.L336:
	movslq	368(%r15), %rax
	movabsq	$4311744768, %rcx
	leal	1(%rax), %edx
	movl	%edx, 368(%r15)
	movq	32(%r15), %rdx
	movq	%rcx, (%rdx,%rax,8)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L295:
	cmpl	$65535, %eax
	jg	.L297
	cmpl	$56320, %eax
	movl	$320, %edx
	movl	$0, %edi
	movq	(%rsi), %rsi
	cmovl	%edx, %edi
	movl	%eax, %edx
	sarl	$5, %edx
.L329:
	addl	%edi, %edx
	movslq	%edx, %rdx
	jmp	.L328
.L297:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L296
	cmpl	44(%rsi), %eax
	jl	.L299
	movslq	48(%rsi), %rdx
	salq	$2, %rdx
	jmp	.L296
.L299:
	movl	%eax, %edx
	movq	(%rsi), %rsi
	movl	%eax, %edi
	sarl	$11, %edx
	sarl	$5, %edi
	addl	$2080, %edx
	andl	$63, %edi
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	jmp	.L329
.L332:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3376:
	.size	_ZN6icu_6724CollationElementIterator9setOffsetEiR10UErrorCode, .-_ZN6icu_6724CollationElementIterator9setOffsetEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIterator7setTextERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6724CollationElementIterator7setTextERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6724CollationElementIterator7setTextERKNS_13UnicodeStringER10UErrorCode:
.LFB3377:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L361
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	40(%rdi), %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	48(%r12), %eax
	testb	$17, %al
	jne	.L350
	testb	$2, %al
	jne	.L362
	movq	64(%r12), %rsi
	movq	%rsi, -56(%rbp)
.L339:
	movq	16(%r12), %rcx
	movq	16(%rcx), %rcx
	movl	24(%rcx), %ecx
	movl	%ecx, %r14d
	shrl	%r14d
	andl	$1, %r14d
	andl	$1, %ecx
	jne	.L341
	testw	%ax, %ax
	js	.L342
	sarl	$5, %eax
.L343:
	movq	-56(%rbp), %rdx
	cltq
	movl	$416, %edi
	leaq	(%rdx,%rax,2), %r15
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L344
	movq	16(%r12), %rax
	movq	-56(%rbp), %xmm0
	movq	8(%rax), %rax
	movb	%r14b, 388(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movl	$0, 368(%rbx)
	movq	(%rax), %rdx
	movq	%rax, 16(%rbx)
	leaq	48(%rbx), %rax
	movq	%rax, 32(%rbx)
	leaq	16+_ZTVN6icu_6722UTF16CollationIteratorE(%rip), %rax
	movq	%rdx, 8(%rbx)
	movl	$0, 24(%rbx)
	movl	$40, 40(%rbx)
	movb	$0, 44(%rbx)
	movq	$0, 376(%rbx)
	movl	$-1, 384(%rbx)
	movq	%rax, (%rbx)
	movq	%r15, 408(%rbx)
	movups	%xmm0, 392(%rbx)
.L345:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L349
	movq	(%rdi), %rax
	call	*8(%rax)
.L349:
	movq	%rbx, 8(%r12)
	movl	$0, 24(%r12)
	movb	$0, 28(%r12)
.L337:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	leaq	50(%r12), %rcx
	movq	%rcx, -56(%rbp)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L341:
	testw	%ax, %ax
	js	.L346
	sarl	$5, %eax
.L347:
	movq	-56(%rbp), %rcx
	cltq
	movl	$528, %edi
	leaq	(%rcx,%rax,2), %r15
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L344
	movq	16(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	8(%rax), %rax
	movq	%rcx, %xmm0
	movb	%r14b, 388(%rbx)
	movq	%rcx, %xmm1
	movl	$0, 368(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movq	(%rax), %rdx
	movq	%rax, 16(%rbx)
	movb	$0, 44(%rbx)
	movq	48(%rax), %rax
	movq	%rdx, 8(%rbx)
	leaq	48(%rbx), %rdx
	movq	%rax, 448(%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movups	%xmm0, 392(%rbx)
	movq	%r15, %xmm0
	movq	%rdx, 32(%rbx)
	punpcklqdq	%xmm1, %xmm0
	leaq	16+_ZTVN6icu_6725FCDUTF16CollationIteratorE(%rip), %rdx
	movq	%rax, 456(%rbx)
	movl	$2, %eax
	movl	$0, 24(%rbx)
	movl	$40, 40(%rbx)
	movq	$0, 376(%rbx)
	movl	$-1, 384(%rbx)
	movq	%rdx, (%rbx)
	movq	%rcx, 424(%rbx)
	movq	$0, 432(%rbx)
	movq	%r15, 440(%rbx)
	movw	%ax, 464(%rbx)
	movb	$1, 520(%rbx)
	movups	%xmm0, 408(%rbx)
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L342:
	movl	52(%r12), %eax
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L346:
	movl	52(%r12), %eax
	jmp	.L347
.L344:
	movl	$7, 0(%r13)
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L350:
	movq	$0, -56(%rbp)
	jmp	.L339
	.cfi_endproc
.LFE3377:
	.size	_ZN6icu_6724CollationElementIterator7setTextERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6724CollationElementIterator7setTextERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIterator7setTextERNS_17CharacterIteratorER10UErrorCode
	.type	_ZN6icu_6724CollationElementIterator7setTextERNS_17CharacterIteratorER10UErrorCode, @function
_ZN6icu_6724CollationElementIterator7setTextERNS_17CharacterIteratorER10UErrorCode:
.LFB3378:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L368
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	leaq	40(%r13), %r14
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	%r14, %rsi
	call	*208(%rax)
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L371
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6724CollationElementIterator7setTextERKNS_13UnicodeStringER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L368:
	ret
	.cfi_endproc
.LFE3378:
	.size	_ZN6icu_6724CollationElementIterator7setTextERNS_17CharacterIteratorER10UErrorCode, .-_ZN6icu_6724CollationElementIterator7setTextERNS_17CharacterIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724CollationElementIterator13strengthOrderEi
	.type	_ZNK6icu_6724CollationElementIterator13strengthOrderEi, @function
_ZNK6icu_6724CollationElementIterator13strengthOrderEi:
.LFB3379:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	16(%rax), %rax
	movl	24(%rax), %eax
	sarl	$12, %eax
	jne	.L373
	movl	%esi, %eax
	xorw	%ax, %ax
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	movl	%esi, %edx
	xorb	%dl, %dl
	cmpl	$1, %eax
	movl	%esi, %eax
	cmove	%edx, %eax
	ret
	.cfi_endproc
.LFE3379:
	.size	_ZNK6icu_6724CollationElementIterator13strengthOrderEi, .-_ZNK6icu_6724CollationElementIterator13strengthOrderEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIteratorC2ERKNS_13UnicodeStringEPKNS_17RuleBasedCollatorER10UErrorCode
	.type	_ZN6icu_6724CollationElementIteratorC2ERKNS_13UnicodeStringEPKNS_17RuleBasedCollatorER10UErrorCode, @function
_ZN6icu_6724CollationElementIteratorC2ERKNS_13UnicodeStringEPKNS_17RuleBasedCollatorER10UErrorCode:
.LFB3381:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6724CollationElementIteratorE(%rip), %rax
	movq	%rdx, 16(%rdi)
	movl	(%rcx), %edx
	movq	%rax, (%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 40(%rdi)
	movl	$2, %eax
	movq	$0, 8(%rdi)
	movl	$0, 24(%rdi)
	movb	$0, 28(%rdi)
	movq	$0, 32(%rdi)
	movw	%ax, 48(%rdi)
	testl	%edx, %edx
	jle	.L378
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	movq	%rcx, %rdx
	jmp	_ZN6icu_6724CollationElementIterator7setTextERKNS_13UnicodeStringER10UErrorCode.part.0
	.cfi_endproc
.LFE3381:
	.size	_ZN6icu_6724CollationElementIteratorC2ERKNS_13UnicodeStringEPKNS_17RuleBasedCollatorER10UErrorCode, .-_ZN6icu_6724CollationElementIteratorC2ERKNS_13UnicodeStringEPKNS_17RuleBasedCollatorER10UErrorCode
	.globl	_ZN6icu_6724CollationElementIteratorC1ERKNS_13UnicodeStringEPKNS_17RuleBasedCollatorER10UErrorCode
	.set	_ZN6icu_6724CollationElementIteratorC1ERKNS_13UnicodeStringEPKNS_17RuleBasedCollatorER10UErrorCode,_ZN6icu_6724CollationElementIteratorC2ERKNS_13UnicodeStringEPKNS_17RuleBasedCollatorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIteratorC2ERKNS_17CharacterIteratorEPKNS_17RuleBasedCollatorER10UErrorCode
	.type	_ZN6icu_6724CollationElementIteratorC2ERKNS_17CharacterIteratorEPKNS_17RuleBasedCollatorER10UErrorCode, @function
_ZN6icu_6724CollationElementIteratorC2ERKNS_17CharacterIteratorEPKNS_17RuleBasedCollatorER10UErrorCode:
.LFB3384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724CollationElementIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdx, 16(%rdi)
	movl	$2, %edx
	movq	$0, 8(%rdi)
	movl	$0, 24(%rdi)
	movb	$0, 28(%rdi)
	movq	$0, 32(%rdi)
	movq	%rax, 40(%rdi)
	movw	%dx, 48(%rdi)
	movl	(%rcx), %edi
	testl	%edi, %edi
	jg	.L379
	movq	(%rsi), %rax
	movq	%rcx, %r13
	leaq	40(%r12), %r14
	movq	%rsi, %rdi
	movq	%r14, %rsi
	call	*208(%rax)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L384
.L379:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6724CollationElementIterator7setTextERKNS_13UnicodeStringER10UErrorCode.part.0
	.cfi_endproc
.LFE3384:
	.size	_ZN6icu_6724CollationElementIteratorC2ERKNS_17CharacterIteratorEPKNS_17RuleBasedCollatorER10UErrorCode, .-_ZN6icu_6724CollationElementIteratorC2ERKNS_17CharacterIteratorEPKNS_17RuleBasedCollatorER10UErrorCode
	.globl	_ZN6icu_6724CollationElementIteratorC1ERKNS_17CharacterIteratorEPKNS_17RuleBasedCollatorER10UErrorCode
	.set	_ZN6icu_6724CollationElementIteratorC1ERKNS_17CharacterIteratorEPKNS_17RuleBasedCollatorER10UErrorCode,_ZN6icu_6724CollationElementIteratorC2ERKNS_17CharacterIteratorEPKNS_17RuleBasedCollatorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIteratoraSERKS0_
	.type	_ZN6icu_6724CollationElementIteratoraSERKS0_, @function
_ZN6icu_6724CollationElementIteratoraSERKS0_:
.LFB3386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L387
	movq	8(%rsi), %r13
	movq	%rsi, %rbx
	testq	%r13, %r13
	je	.L389
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6725FCDUTF16CollationIteratorE(%rip), %rdx
	leaq	_ZTIN6icu_6717CollationIteratorE(%rip), %rsi
	movq	%r13, %rdi
	call	__dynamic_cast@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L390
	movzwl	48(%r12), %eax
	testb	$17, %al
	jne	.L404
	leaq	50(%r12), %r15
	testb	$2, %al
	je	.L423
.L391:
	movl	$528, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L389
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6725FCDUTF16CollationIteratorC1ERKS0_PKDs@PLT
.L394:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L401
	movq	(%rdi), %rax
	call	*8(%rax)
.L401:
	movq	16(%rbx), %rax
	movq	%r13, 8(%r12)
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	movq	%rax, 16(%r12)
	movl	24(%rbx), %eax
	movl	%eax, 24(%r12)
	movzbl	28(%rbx), %eax
	movb	%al, 28(%r12)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L389:
	cmpb	$0, 28(%rbx)
	js	.L424
.L387:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L425
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	movq	32(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L387
	movl	8(%rsi), %r13d
	testl	%r13d, %r13d
	je	.L387
	movq	32(%r12), %r14
	movl	$0, -60(%rbp)
	leaq	-60(%rbp), %r15
	testq	%r14, %r14
	je	.L426
.L398:
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_679UVector326assignERKS0_R10UErrorCode@PLT
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L390:
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6722UTF16CollationIteratorE(%rip), %rdx
	leaq	_ZTIN6icu_6717CollationIteratorE(%rip), %rsi
	movq	%r13, %rdi
	call	__dynamic_cast@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L389
	movzwl	48(%r12), %eax
	testb	$17, %al
	jne	.L395
	leaq	50(%r12), %r14
	testb	$2, %al
	jne	.L395
	movq	64(%r12), %r14
.L395:
	movl	$416, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L389
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722UTF16CollationIteratorC1ERKS0_PKDs@PLT
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L423:
	movq	64(%r12), %r15
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L404:
	xorl	%r15d, %r15d
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L426:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L399
	movl	%r13d, %esi
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movq	%r14, 32(%r12)
	movq	32(%rbx), %rsi
	jmp	.L398
.L425:
	call	__stack_chk_fail@PLT
.L399:
	movq	$0, 32(%r12)
	jmp	.L387
	.cfi_endproc
.LFE3386:
	.size	_ZN6icu_6724CollationElementIteratoraSERKS0_, .-_ZN6icu_6724CollationElementIteratoraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724CollationElementIterator15getMaxExpansionEi
	.type	_ZNK6icu_6724CollationElementIterator15getMaxExpansionEi, @function
_ZNK6icu_6724CollationElementIterator15getMaxExpansionEi:
.LFB3403:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L438
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	24(%rax), %rax
	movq	384(%rax), %rdi
	testq	%rdi, %rdi
	je	.L431
	call	uhash_igeti_67@PLT
	testl	%eax, %eax
	jne	.L427
.L431:
	andl	$192, %ebx
	movl	$2, %eax
	cmpl	$192, %ebx
	je	.L427
	movl	$1, %eax
.L427:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3403:
	.size	_ZNK6icu_6724CollationElementIterator15getMaxExpansionEi, .-_ZNK6icu_6724CollationElementIterator15getMaxExpansionEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIterator15getMaxExpansionEPK10UHashtablei
	.type	_ZN6icu_6724CollationElementIterator15getMaxExpansionEPK10UHashtablei, @function
_ZN6icu_6724CollationElementIterator15getMaxExpansionEPK10UHashtablei:
.LFB3404:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L453
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L446
	call	uhash_igeti_67@PLT
	testl	%eax, %eax
	jne	.L442
.L446:
	andl	$192, %ebx
	movl	$2, %eax
	cmpl	$192, %ebx
	je	.L442
	movl	$1, %eax
.L442:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3404:
	.size	_ZN6icu_6724CollationElementIterator15getMaxExpansionEPK10UHashtablei, .-_ZN6icu_6724CollationElementIterator15getMaxExpansionEPK10UHashtablei
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CollationElementIterator20computeMaxExpansionsEPKNS_13CollationDataER10UErrorCode
	.type	_ZN6icu_6724CollationElementIterator20computeMaxExpansionsEPKNS_13CollationDataER10UErrorCode, @function
_ZN6icu_6724CollationElementIterator20computeMaxExpansionsEPKNS_13CollationDataER10UErrorCode:
.LFB3399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$856, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L460
	movq	%rsi, %rbx
	movq	%rsi, %rcx
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	movq	%rdi, %r13
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	movq	%rsi, %rdx
	call	uhash_open_67@PLT
	movl	(%rbx), %esi
	movq	%rax, %r12
	testl	%esi, %esi
	jg	.L460
	leaq	-792(%rbp), %r8
	pxor	%xmm0, %xmm0
	leaq	-832(%rbp), %r10
	movq	%rax, -856(%rbp)
	movq	%r8, %rdi
	movl	$1, %eax
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_110MaxExpSinkE(%rip), %r15
	movq	%r8, -880(%rbp)
	leaq	-864(%rbp), %r14
	movaps	%xmm0, -832(%rbp)
	movw	%ax, -800(%rbp)
	movq	%r10, -888(%rbp)
	movq	%r15, -864(%rbp)
	movq	%rbx, -848(%rbp)
	movq	$0, -816(%rbp)
	movq	%r14, -808(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	-592(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -872(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	-888(%rbp), %r10
	movl	$2, %edx
	movq	%r13, %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -384(%rbp)
	movq	%rbx, %rdx
	movq	%r10, %rdi
	movq	%rax, -392(%rbp)
	movq	$0, -328(%rbp)
	movl	$0, -72(%rbp)
	call	_ZN6icu_6725ContractionsAndExpansions7forDataEPKNS_13CollationDataER10UErrorCode@PLT
	leaq	-392(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-872(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-880(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L464
.L461:
	movq	%r14, %rdi
	movq	%r15, -864(%rbp)
	call	_ZN6icu_6725ContractionsAndExpansions6CESinkD2Ev@PLT
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L460:
	xorl	%r12d, %r12d
.L457:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L465
	addq	$856, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	uhash_close_67@PLT
	jmp	.L461
.L465:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3399:
	.size	_ZN6icu_6724CollationElementIterator20computeMaxExpansionsEPKNS_13CollationDataER10UErrorCode, .-_ZN6icu_6724CollationElementIterator20computeMaxExpansionsEPKNS_13CollationDataER10UErrorCode
	.weak	_ZTSN6icu_6724CollationElementIteratorE
	.section	.rodata._ZTSN6icu_6724CollationElementIteratorE,"aG",@progbits,_ZTSN6icu_6724CollationElementIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6724CollationElementIteratorE, @object
	.size	_ZTSN6icu_6724CollationElementIteratorE, 36
_ZTSN6icu_6724CollationElementIteratorE:
	.string	"N6icu_6724CollationElementIteratorE"
	.weak	_ZTIN6icu_6724CollationElementIteratorE
	.section	.data.rel.ro._ZTIN6icu_6724CollationElementIteratorE,"awG",@progbits,_ZTIN6icu_6724CollationElementIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6724CollationElementIteratorE, @object
	.size	_ZTIN6icu_6724CollationElementIteratorE, 24
_ZTIN6icu_6724CollationElementIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6724CollationElementIteratorE
	.quad	_ZTIN6icu_677UObjectE
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_110MaxExpSinkE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_110MaxExpSinkE, 24
_ZTIN6icu_6712_GLOBAL__N_110MaxExpSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_110MaxExpSinkE
	.quad	_ZTIN6icu_6725ContractionsAndExpansions6CESinkE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_110MaxExpSinkE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_110MaxExpSinkE, 37
_ZTSN6icu_6712_GLOBAL__N_110MaxExpSinkE:
	.string	"*N6icu_6712_GLOBAL__N_110MaxExpSinkE"
	.weak	_ZTVN6icu_6724CollationElementIteratorE
	.section	.data.rel.ro.local._ZTVN6icu_6724CollationElementIteratorE,"awG",@progbits,_ZTVN6icu_6724CollationElementIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6724CollationElementIteratorE, @object
	.size	_ZTVN6icu_6724CollationElementIteratorE, 40
_ZTVN6icu_6724CollationElementIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6724CollationElementIteratorE
	.quad	_ZN6icu_6724CollationElementIteratorD1Ev
	.quad	_ZN6icu_6724CollationElementIteratorD0Ev
	.quad	_ZNK6icu_6724CollationElementIterator17getDynamicClassIDEv
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_110MaxExpSinkE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_110MaxExpSinkE, 48
_ZTVN6icu_6712_GLOBAL__N_110MaxExpSinkE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_110MaxExpSinkE
	.quad	_ZN6icu_6712_GLOBAL__N_110MaxExpSinkD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_110MaxExpSinkD0Ev
	.quad	_ZN6icu_6712_GLOBAL__N_110MaxExpSink8handleCEEl
	.quad	_ZN6icu_6712_GLOBAL__N_110MaxExpSink15handleExpansionEPKli
	.local	_ZZN6icu_6724CollationElementIterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6724CollationElementIterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
