	.file	"tmunit.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678TimeUnit17getDynamicClassIDEv
	.type	_ZNK6icu_678TimeUnit17getDynamicClassIDEv, @function
_ZNK6icu_678TimeUnit17getDynamicClassIDEv:
.LFB2198:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_678TimeUnit16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2198:
	.size	_ZNK6icu_678TimeUnit17getDynamicClassIDEv, .-_ZNK6icu_678TimeUnit17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678TimeUnit5cloneEv
	.type	_ZNK6icu_678TimeUnit5cloneEv, @function
_ZNK6icu_678TimeUnit5cloneEv:
.LFB2206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6711MeasureUnitC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_678TimeUnitE(%rip), %rax
	movq	%rax, (%r12)
	movl	20(%rbx), %eax
	movl	%eax, 20(%r12)
.L3:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2206:
	.size	_ZNK6icu_678TimeUnit5cloneEv, .-_ZNK6icu_678TimeUnit5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678TimeUnitD2Ev
	.type	_ZN6icu_678TimeUnitD2Ev, @function
_ZN6icu_678TimeUnitD2Ev:
.LFB2210:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678TimeUnitE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6711MeasureUnitD2Ev@PLT
	.cfi_endproc
.LFE2210:
	.size	_ZN6icu_678TimeUnitD2Ev, .-_ZN6icu_678TimeUnitD2Ev
	.globl	_ZN6icu_678TimeUnitD1Ev
	.set	_ZN6icu_678TimeUnitD1Ev,_ZN6icu_678TimeUnitD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_678TimeUnitD0Ev
	.type	_ZN6icu_678TimeUnitD0Ev, @function
_ZN6icu_678TimeUnitD0Ev:
.LFB2212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678TimeUnitE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6711MeasureUnitD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2212:
	.size	_ZN6icu_678TimeUnitD0Ev, .-_ZN6icu_678TimeUnitD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_678TimeUnit16getStaticClassIDEv
	.type	_ZN6icu_678TimeUnit16getStaticClassIDEv, @function
_ZN6icu_678TimeUnit16getStaticClassIDEv:
.LFB2197:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_678TimeUnit16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2197:
	.size	_ZN6icu_678TimeUnit16getStaticClassIDEv, .-_ZN6icu_678TimeUnit16getStaticClassIDEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"year"
.LC1:
	.string	"month"
.LC2:
	.string	"day"
.LC3:
	.string	"week"
.LC4:
	.string	"hour"
.LC5:
	.string	"minute"
.LC6:
	.string	"second"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678TimeUnit14createInstanceENS0_15UTimeUnitFieldsER10UErrorCode
	.type	_ZN6icu_678TimeUnit14createInstanceENS0_15UTimeUnitFieldsER10UErrorCode, @function
_ZN6icu_678TimeUnit14createInstanceENS0_15UTimeUnitFieldsER10UErrorCode:
.LFB2199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L30
	movl	%edi, %ebx
	cmpl	$6, %ebx
	ja	.L31
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L30
	movq	%rax, %rdi
	call	_ZN6icu_6711MeasureUnitC2Ev@PLT
	leaq	16+_ZTVN6icu_678TimeUnitE(%rip), %rax
	movl	%ebx, 20(%r12)
	leaq	.L20(%rip), %rdx
	movq	%rax, (%r12)
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L20:
	.long	.L18-.L20
	.long	.L25-.L20
	.long	.L24-.L20
	.long	.L23-.L20
	.long	.L22-.L20
	.long	.L21-.L20
	.long	.L19-.L20
	.text
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$1, (%rsi)
.L30:
	xorl	%r12d, %r12d
.L13:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnit8initTimeEPKc@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L25:
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnit8initTimeEPKc@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnit8initTimeEPKc@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L23:
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnit8initTimeEPKc@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnit8initTimeEPKc@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnit8initTimeEPKc@PLT
	jmp	.L13
.L18:
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnit8initTimeEPKc@PLT
	jmp	.L13
	.cfi_endproc
.LFE2199:
	.size	_ZN6icu_678TimeUnit14createInstanceENS0_15UTimeUnitFieldsER10UErrorCode, .-_ZN6icu_678TimeUnit14createInstanceENS0_15UTimeUnitFieldsER10UErrorCode
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB7:
	.text
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZN6icu_678TimeUnitC2ENS0_15UTimeUnitFieldsE
	.type	_ZN6icu_678TimeUnitC2ENS0_15UTimeUnitFieldsE, @function
_ZN6icu_678TimeUnitC2ENS0_15UTimeUnitFieldsE:
.LFB2201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	call	_ZN6icu_6711MeasureUnitC2Ev@PLT
	leaq	16+_ZTVN6icu_678TimeUnitE(%rip), %rax
	movl	%ebx, 20(%r12)
	movq	%rax, (%r12)
	cmpl	$6, %ebx
	ja	.L33
	leaq	.L35(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L35:
	.long	.L41-.L35
	.long	.L40-.L35
	.long	.L39-.L35
	.long	.L38-.L35
	.long	.L37-.L35
	.long	.L36-.L35
	.long	.L34-.L35
	.text
	.p2align 4,,10
	.p2align 3
.L36:
	popq	%rbx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711MeasureUnit8initTimeEPKc@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711MeasureUnit8initTimeEPKc@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711MeasureUnit8initTimeEPKc@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711MeasureUnit8initTimeEPKc@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711MeasureUnit8initTimeEPKc@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711MeasureUnit8initTimeEPKc@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	leaq	.LC4(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711MeasureUnit8initTimeEPKc@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_678TimeUnitC2ENS0_15UTimeUnitFieldsE.cold, @function
_ZN6icu_678TimeUnitC2ENS0_15UTimeUnitFieldsE.cold:
.LFSB2201:
.L33:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	call	abort@PLT
	.cfi_endproc
.LFE2201:
	.text
	.size	_ZN6icu_678TimeUnitC2ENS0_15UTimeUnitFieldsE, .-_ZN6icu_678TimeUnitC2ENS0_15UTimeUnitFieldsE
	.section	.text.unlikely
	.size	_ZN6icu_678TimeUnitC2ENS0_15UTimeUnitFieldsE.cold, .-_ZN6icu_678TimeUnitC2ENS0_15UTimeUnitFieldsE.cold
.LCOLDE7:
	.text
.LHOTE7:
	.globl	_ZN6icu_678TimeUnitC1ENS0_15UTimeUnitFieldsE
	.set	_ZN6icu_678TimeUnitC1ENS0_15UTimeUnitFieldsE,_ZN6icu_678TimeUnitC2ENS0_15UTimeUnitFieldsE
	.align 2
	.p2align 4
	.globl	_ZN6icu_678TimeUnitC2ERKS0_
	.type	_ZN6icu_678TimeUnitC2ERKS0_, @function
_ZN6icu_678TimeUnitC2ERKS0_:
.LFB2204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6711MeasureUnitC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_678TimeUnitE(%rip), %rax
	movq	%rax, (%rbx)
	movl	20(%r12), %eax
	movl	%eax, 20(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2204:
	.size	_ZN6icu_678TimeUnitC2ERKS0_, .-_ZN6icu_678TimeUnitC2ERKS0_
	.globl	_ZN6icu_678TimeUnitC1ERKS0_
	.set	_ZN6icu_678TimeUnitC1ERKS0_,_ZN6icu_678TimeUnitC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_678TimeUnitaSERKS0_
	.type	_ZN6icu_678TimeUnitaSERKS0_, @function
_ZN6icu_678TimeUnitaSERKS0_:
.LFB2207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L47
	movq	%rsi, %rbx
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movl	20(%rbx), %eax
	movl	%eax, 20(%r12)
.L47:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2207:
	.size	_ZN6icu_678TimeUnitaSERKS0_, .-_ZN6icu_678TimeUnitaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678TimeUnit16getTimeUnitFieldEv
	.type	_ZNK6icu_678TimeUnit16getTimeUnitFieldEv, @function
_ZNK6icu_678TimeUnit16getTimeUnitFieldEv:
.LFB2208:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	ret
	.cfi_endproc
.LFE2208:
	.size	_ZNK6icu_678TimeUnit16getTimeUnitFieldEv, .-_ZNK6icu_678TimeUnit16getTimeUnitFieldEv
	.weak	_ZTSN6icu_678TimeUnitE
	.section	.rodata._ZTSN6icu_678TimeUnitE,"aG",@progbits,_ZTSN6icu_678TimeUnitE,comdat
	.align 16
	.type	_ZTSN6icu_678TimeUnitE, @object
	.size	_ZTSN6icu_678TimeUnitE, 19
_ZTSN6icu_678TimeUnitE:
	.string	"N6icu_678TimeUnitE"
	.weak	_ZTIN6icu_678TimeUnitE
	.section	.data.rel.ro._ZTIN6icu_678TimeUnitE,"awG",@progbits,_ZTIN6icu_678TimeUnitE,comdat
	.align 8
	.type	_ZTIN6icu_678TimeUnitE, @object
	.size	_ZTIN6icu_678TimeUnitE, 24
_ZTIN6icu_678TimeUnitE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678TimeUnitE
	.quad	_ZTIN6icu_6711MeasureUnitE
	.weak	_ZTVN6icu_678TimeUnitE
	.section	.data.rel.ro._ZTVN6icu_678TimeUnitE,"awG",@progbits,_ZTVN6icu_678TimeUnitE,comdat
	.align 8
	.type	_ZTVN6icu_678TimeUnitE, @object
	.size	_ZTVN6icu_678TimeUnitE, 56
_ZTVN6icu_678TimeUnitE:
	.quad	0
	.quad	_ZTIN6icu_678TimeUnitE
	.quad	_ZN6icu_678TimeUnitD1Ev
	.quad	_ZN6icu_678TimeUnitD0Ev
	.quad	_ZNK6icu_678TimeUnit17getDynamicClassIDEv
	.quad	_ZNK6icu_678TimeUnit5cloneEv
	.quad	_ZNK6icu_6711MeasureUniteqERKNS_7UObjectE
	.local	_ZZN6icu_678TimeUnit16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_678TimeUnit16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
