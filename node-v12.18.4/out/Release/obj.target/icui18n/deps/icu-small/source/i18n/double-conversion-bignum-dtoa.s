	.file	"double-conversion-bignum-dtoa.cpp"
	.text
	.p2align 4
	.type	_ZN6icu_6717double_conversionL21GenerateCountedDigitsEiPiPNS0_6BignumES3_NS0_6VectorIcEES1_.isra.0, @function
_ZN6icu_6717double_conversionL21GenerateCountedDigitsEiPiPNS0_6BignumES3_NS0_6VectorIcEES1_.isra.0:
.LFB433:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	leal	-1(%rdi), %ecx
	pushq	%r14
	movslq	%ecx, %rax
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	addq	%r8, %rax
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	%edi, -52(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%r9, -64(%rbp)
	movl	%ecx, -56(%rbp)
	movq	%rax, -72(%rbp)
	testl	%ecx, %ecx
	jle	.L2
	leal	-2(%rdi), %eax
	movq	%r8, %rbx
	leaq	1(%r8,%rax), %r13
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_@PLT
	movl	$10, %esi
	movq	%r12, %rdi
	addl	$48, %eax
	movb	%al, -1(%rbx)
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej@PLT
	cmpq	%r13, %rbx
	jne	.L4
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_@PLT
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	%r15, %rdx
	movl	%eax, %ebx
	call	_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_@PLT
	movl	-56(%rbp), %r12d
	movl	%eax, %r8d
	leal	49(%rbx), %eax
	addl	$48, %ebx
	testl	%r8d, %r8d
	cmovns	%eax, %ebx
	movq	-72(%rbp), %rax
	movb	%bl, (%rax)
	subl	%eax, %r12d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L17:
	movb	$48, (%rax)
	addb	$1, -1(%rax)
	subq	$1, %rax
	leal	(%r12,%rax), %edx
	testl	%edx, %edx
	jle	.L5
.L7:
	cmpb	$58, (%rax)
	je	.L17
.L5:
	cmpb	$58, (%r14)
	jne	.L8
	movq	-80(%rbp), %rax
	movb	$49, (%r14)
	addl	$1, (%rax)
.L8:
	movq	-64(%rbp), %rax
	movl	-52(%rbp), %ecx
	movl	%ecx, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_@PLT
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_@PLT
	movl	%eax, %r8d
	leal	49(%rbx), %eax
	addl	$48, %ebx
	testl	%r8d, %r8d
	cmovns	%eax, %ebx
	movq	-72(%rbp), %rax
	movb	%bl, (%rax)
	jmp	.L5
	.cfi_endproc
.LFE433:
	.size	_ZN6icu_6717double_conversionL21GenerateCountedDigitsEiPiPNS0_6BignumES3_NS0_6VectorIcEES1_.isra.0, .-_ZN6icu_6717double_conversionL21GenerateCountedDigitsEiPiPNS0_6BignumES3_NS0_6VectorIcEES1_.isra.0
	.section	.text.unlikely,"ax",@progbits
.LCOLDB5:
	.text
.LHOTB5:
	.p2align 4
	.globl	_ZN6icu_6717double_conversion10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_
	.type	_ZN6icu_6717double_conversion10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_, @function
_ZN6icu_6717double_conversion10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_:
.LFB410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, %ebx
	subq	$2184, %rsp
	movl	%esi, -2176(%rbp)
	movq	%rdx, -2168(%rbp)
	movq	%r9, -2192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -2200(%rbp)
	cmpl	$1, %edi
	je	.L107
	movabsq	$4503599627370495, %r8
	movq	%xmm0, %rax
	movabsq	$9218868437227405312, %rdx
	andq	%rax, %r8
	testq	%rdx, %rax
	jne	.L108
	movb	$0, -2210(%rbp)
	movl	$-1074, %r14d
.L21:
	movq	%r8, %rax
	cmpl	$1, %ebx
	movq	%r8, %rdx
	movabsq	$4503599627370496, %rcx
	notq	%rax
	setbe	-2209(%rbp)
	movq	%rax, -2208(%rbp)
	movl	%r14d, %eax
	.p2align 4,,10
	.p2align 3
.L24:
	addq	%rdx, %rdx
	subl	$1, %eax
	testq	%rcx, %rdx
	je	.L24
.L62:
	addl	$52, %eax
	pxor	%xmm0, %xmm0
	movsd	.LC3(%rip), %xmm3
	movsd	.LC2(%rip), %xmm4
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LC0(%rip), %xmm0
	subsd	.LC1(%rip), %xmm0
	movapd	%xmm0, %xmm2
	movapd	%xmm0, %xmm1
	andpd	%xmm3, %xmm2
	ucomisd	%xmm2, %xmm4
	jbe	.L25
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm2, %xmm2
	movsd	.LC4(%rip), %xmm4
	andnpd	%xmm0, %xmm3
	cvtsi2sdq	%rax, %xmm2
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm4, %xmm1
	addsd	%xmm2, %xmm1
	orpd	%xmm3, %xmm1
.L25:
	cvttsd2sil	%xmm1, %eax
	movl	%eax, -2172(%rbp)
	cmpl	$2, %ebx
	jne	.L26
	notl	%eax
	cmpl	-2176(%rbp), %eax
	jg	.L109
.L26:
	movl	$0, -2160(%rbp)
	movl	$0, -1632(%rbp)
	movl	$0, -1104(%rbp)
	movl	$0, -576(%rbp)
	testl	%r14d, %r14d
	jns	.L110
	movl	-2172(%rbp), %eax
	negl	%r14d
	testl	%eax, %eax
	js	.L32
	leaq	-2160(%rbp), %r12
	movq	%r8, %rsi
	leaq	-1632(%rbp), %r13
	movq	%r12, %rdi
	call	_ZN6icu_6717double_conversion6Bignum12AssignUInt64Em@PLT
	movl	-2172(%rbp), %edx
	movl	$10, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6717double_conversion6Bignum17AssignPowerUInt16Eti@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	leaq	-576(%rbp), %rax
	cmpb	$0, -2209(%rbp)
	movq	%rax, -2184(%rbp)
	jne	.L111
.L30:
	movzbl	-2208(%rbp), %eax
	movq	-2184(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	andl	$1, %eax
	movb	%al, -2208(%rbp)
	je	.L35
	call	_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_@PLT
	notl	%eax
	shrl	$31, %eax
	testb	%al, %al
	je	.L37
.L117:
	movl	-2172(%rbp), %eax
	movq	-2192(%rbp), %rcx
	addl	$1, %eax
	movl	%eax, (%rcx)
.L38:
	cmpl	$2, %ebx
	je	.L40
.L118:
	ja	.L112
	movq	-2184(%rbp), %r14
	leaq	-1104(%rbp), %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6717double_conversion6Bignum7CompareERKS1_S3_@PLT
	movl	$0, (%r15)
	testl	%eax, %eax
	cmove	%rbx, %r14
.L52:
	cmpb	$0, -2208(%rbp)
	jne	.L45
	cmpq	%rbx, %r14
	je	.L48
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L113:
	testb	%r14b, %r14b
	jne	.L53
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej@PLT
	movl	$10, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej@PLT
.L48:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_@PLT
	movq	-2168(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r8d
	movslq	(%r15), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r15)
	leal	48(%r8), %edx
	movb	%dl, (%rcx,%rax)
	call	_ZN6icu_6717double_conversion6Bignum7CompareERKS1_S3_@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	shrl	$31, %eax
	movb	%al, -2172(%rbp)
	movl	%eax, %r14d
	call	_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_@PLT
	testl	%eax, %eax
	setg	%dl
	jle	.L113
.L47:
	cmpb	$0, -2172(%rbp)
	jne	.L67
.L57:
	movl	(%r15), %eax
	movq	-2168(%rbp), %rcx
	subl	$1, %eax
	cltq
	addb	$1, (%rcx,%rax)
.L106:
	movslq	(%r15), %rax
.L55:
	addq	-2168(%rbp), %rax
	movq	%rax, -2200(%rbp)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L108:
	shrq	$52, %rax
	andl	$2047, %eax
	testq	%r8, %r8
	je	.L114
	movabsq	$4503599627370496, %rdx
	movb	$0, -2210(%rbp)
	addq	%rdx, %r8
	movq	%r8, %rcx
	notq	%rcx
	movq	%rcx, -2208(%rbp)
.L23:
	subl	$1075, %eax
	cmpl	$1, %ebx
	setbe	-2209(%rbp)
	movl	%eax, %r14d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L114:
	movabsq	$-4503599627370497, %rcx
	cmpl	$1, %eax
	movabsq	$4503599627370496, %r8
	movq	%rcx, -2208(%rbp)
	setne	-2210(%rbp)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L112:
	cmpl	$3, %ebx
	jne	.L115
	movq	-2168(%rbp), %rbx
	movq	%r15, %r9
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	-2192(%rbp), %rsi
	movl	-2176(%rbp), %edi
	movq	%rbx, %r8
	call	_ZN6icu_6717double_conversionL21GenerateCountedDigitsEiPiPNS0_6BignumES3_NS0_6VectorIcEES1_.isra.0
	movslq	(%r15), %rax
	addq	%rbx, %rax
	movq	%rax, -2200(%rbp)
.L58:
	movb	$0, (%rax)
.L18:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L116
	addq	$2184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	call	_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_@PLT
	testl	%eax, %eax
	setg	%al
	testb	%al, %al
	jne	.L117
.L37:
	movq	-2192(%rbp), %rax
	movl	-2172(%rbp), %ecx
	movl	$10, %esi
	movq	%r12, %rdi
	leaq	-1104(%rbp), %r14
	movl	%ecx, (%rax)
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej@PLT
	movq	-2184(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717double_conversion6Bignum7CompareERKS1_S3_@PLT
	movl	$10, %esi
	movq	%r14, %rdi
	testl	%eax, %eax
	jne	.L39
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej@PLT
	movq	-2184(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6717double_conversion6Bignum12AssignBignumERKS1_@PLT
	cmpl	$2, %ebx
	jne	.L118
	.p2align 4,,10
	.p2align 3
.L40:
	movq	-2192(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	negl	%edx
	cmpl	%edx, -2176(%rbp)
	jl	.L119
	je	.L120
	movq	-2168(%rbp), %rbx
	movq	%r15, %r9
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	-2176(%rbp), %edi
	movq	-2192(%rbp), %rsi
	movq	%rbx, %r8
	addl	%eax, %edi
	call	_ZN6icu_6717double_conversionL21GenerateCountedDigitsEiPiPNS0_6BignumES3_NS0_6VectorIcEES1_.isra.0
	movslq	(%r15), %rax
	addq	%rbx, %rax
	movq	%rax, -2200(%rbp)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L110:
	leaq	-2160(%rbp), %r12
	movq	%r8, %rsi
	leaq	-1632(%rbp), %r13
	movq	%r12, %rdi
	call	_ZN6icu_6717double_conversion6Bignum12AssignUInt64Em@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	movl	-2172(%rbp), %edx
	movl	$10, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6717double_conversion6Bignum17AssignPowerUInt16Eti@PLT
	cmpb	$0, -2209(%rbp)
	jne	.L29
.L103:
	leaq	-576(%rbp), %rax
	movq	%rax, -2184(%rbp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L107:
	pxor	%xmm5, %xmm5
	cvtsd2ss	%xmm0, %xmm5
	movd	%xmm5, %edx
	movd	%xmm5, %eax
	andl	$8388607, %edx
	testl	$2139095040, %eax
	je	.L20
	shrl	$23, %eax
	movb	$0, -2210(%rbp)
	leal	8388608(%rdx), %r8d
	movzbl	%al, %eax
	leal	-150(%rax), %r14d
	testl	%edx, %edx
	jne	.L21
	cmpl	$1, %eax
	setne	-2210(%rbp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L45:
	cmpq	%rbx, %r14
	je	.L51
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L121:
	testb	%r14b, %r14b
	jne	.L67
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej@PLT
	movl	$10, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej@PLT
.L51:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_@PLT
	movq	-2168(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r8d
	movslq	(%r15), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r15)
	leal	48(%r8), %edx
	movb	%dl, (%rcx,%rax)
	call	_ZN6icu_6717double_conversion6Bignum7CompareERKS1_S3_@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	testl	%eax, %eax
	setle	%r14b
	movb	%r14b, -2172(%rbp)
	call	_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_@PLT
	notl	%eax
	shrl	$31, %eax
	movl	%eax, %edx
	testb	%al, %al
	je	.L121
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L39:
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej@PLT
	movq	-2184(%rbp), %rdi
	movl	$10, %esi
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	movq	-2184(%rbp), %rdi
	movl	$1, %esi
	call	_ZN6icu_6717double_conversion6Bignum12AssignUInt16Et@PLT
	leaq	-1104(%rbp), %rdi
	movl	$1, %esi
	call	_ZN6icu_6717double_conversion6Bignum12AssignUInt16Et@PLT
.L31:
	cmpb	$0, -2209(%rbp)
	je	.L30
	cmpb	$0, -2210(%rbp)
	je	.L30
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	movq	-2184(%rbp), %rdi
	movl	$1, %esi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L119:
	movl	-2176(%rbp), %esi
	movq	-2192(%rbp), %rax
	negl	%esi
	movl	%esi, (%rax)
	movq	-2200(%rbp), %rax
	movl	$0, (%r15)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L32:
	movl	-2172(%rbp), %edx
	leaq	-2160(%rbp), %r12
	movl	$10, %esi
	movq	%r8, -2184(%rbp)
	movq	%r12, %rdi
	negl	%edx
	call	_ZN6icu_6717double_conversion6Bignum17AssignPowerUInt16Eti@PLT
	cmpb	$0, -2209(%rbp)
	movq	-2184(%rbp), %r8
	jne	.L122
	movq	%r8, %rsi
	movq	%r12, %rdi
	leaq	-1632(%rbp), %r13
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt64Em@PLT
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN6icu_6717double_conversion6Bignum12AssignUInt16Et@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L109:
	movq	-2168(%rbp), %rax
	movl	-2176(%rbp), %esi
	movb	$0, (%rax)
	movq	-2192(%rbp), %rax
	negl	%esi
	movl	$0, (%r15)
	movl	%esi, (%rax)
	jmp	.L18
.L67:
	testb	%dl, %dl
	jne	.L123
.L53:
	cmpb	$0, -2172(%rbp)
	jne	.L106
	jmp	.L57
.L50:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_@PLT
	movq	-2168(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r8d
	movslq	(%r15), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r15)
	leal	48(%r8), %edx
	movb	%dl, (%rcx,%rax)
	call	_ZN6icu_6717double_conversion6Bignum7CompareERKS1_S3_@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	testl	%eax, %eax
	setle	-2172(%rbp)
	call	_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_@PLT
	notl	%eax
	shrl	$31, %eax
	cmpb	$0, -2172(%rbp)
	movl	%eax, %edx
	jne	.L67
	testb	%al, %al
	jne	.L57
.L104:
	movq	%r12, %rdi
	movl	$10, %esi
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej@PLT
	movq	%rbx, %rdi
	movl	$10, %esi
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej@PLT
	movl	$10, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej@PLT
	jmp	.L52
.L46:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_@PLT
	movq	-2168(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r8d
	movslq	(%r15), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r15)
	leal	48(%r8), %edx
	movb	%dl, (%rcx,%rax)
	call	_ZN6icu_6717double_conversion6Bignum7CompareERKS1_S3_@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	shrl	$31, %eax
	movb	%al, -2172(%rbp)
	movl	%eax, -2184(%rbp)
	call	_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_@PLT
	testl	%eax, %eax
	setg	%dl
	jg	.L47
	movl	-2184(%rbp), %ecx
	testb	%cl, %cl
	je	.L104
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	leaq	-576(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rax, -2184(%rbp)
	call	_ZN6icu_6717double_conversion6Bignum12AssignUInt16Et@PLT
	movq	-2184(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	leaq	-1104(%rbp), %rdi
	movl	$1, %esi
	movq	%rdi, -2224(%rbp)
	call	_ZN6icu_6717double_conversion6Bignum12AssignUInt16Et@PLT
	movq	-2224(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	jmp	.L31
.L122:
	leaq	-576(%rbp), %rax
	movq	%r12, %rsi
	movq	%r8, -2224(%rbp)
	leaq	-1632(%rbp), %r13
	movq	%rax, %rdi
	movq	%rax, -2184(%rbp)
	call	_ZN6icu_6717double_conversion6Bignum12AssignBignumERKS1_@PLT
	leaq	-1104(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6717double_conversion6Bignum12AssignBignumERKS1_@PLT
	movq	-2224(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt64Em@PLT
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN6icu_6717double_conversion6Bignum12AssignUInt16Et@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	jmp	.L31
.L123:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_@PLT
	movl	%eax, %edx
	movslq	(%r15), %rax
	testl	%edx, %edx
	js	.L55
	leal	-1(%rax), %ecx
	movslq	%ecx, %rcx
	addq	-2168(%rbp), %rcx
	movzbl	(%rcx), %esi
	testl	%edx, %edx
	jne	.L105
	testb	$1, %sil
	je	.L55
.L105:
	addl	$1, %esi
	movb	%sil, (%rcx)
	movslq	(%r15), %rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L20:
	movb	$0, -2210(%rbp)
	movl	%edx, %r8d
	movl	$-149, %r14d
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$10, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_@PLT
	testl	%eax, %eax
	js	.L61
	movq	-2168(%rbp), %rcx
	movq	-2192(%rbp), %rax
	movb	$49, (%rcx)
	movl	$1, (%r15)
	addl	$1, (%rax)
	movslq	(%r15), %rax
	addq	%rcx, %rax
	movq	%rax, -2200(%rbp)
	jmp	.L58
.L61:
	movl	$0, (%r15)
	movq	-2200(%rbp), %rax
	jmp	.L58
.L116:
	call	__stack_chk_fail@PLT
.L115:
	jmp	.L93
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_.cold, @function
_ZN6icu_6717double_conversion10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_.cold:
.LFSB410:
.L93:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE410:
	.text
	.size	_ZN6icu_6717double_conversion10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_, .-_ZN6icu_6717double_conversion10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_.cold, .-_ZN6icu_6717double_conversion10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_.cold
.LCOLDE5:
	.text
.LHOTE5:
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1352628734
	.long	1070810131
	.align 8
.LC1:
	.long	3654794683
	.long	1037794527
	.align 8
.LC2:
	.long	0
	.long	1127219200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC4:
	.long	0
	.long	1072693248
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
