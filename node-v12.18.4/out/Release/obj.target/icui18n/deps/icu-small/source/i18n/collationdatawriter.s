	.file	"collationdatawriter.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationDataWriter8copyDataEPKiiPKvPh
	.type	_ZN6icu_6719CollationDataWriter8copyDataEPKiiPKvPh, @function
_ZN6icu_6719CollationDataWriter8copyDataEPKiiPKvPh:
.LFB3363:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movq	%rdx, %r8
	movl	(%rdi,%rsi,4), %eax
	movl	4(%rdi,%rsi,4), %edx
	cmpl	%edx, %eax
	jl	.L4
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movslq	%eax, %rdi
	subl	%eax, %edx
	movq	%r8, %rsi
	addq	%rcx, %rdi
	movslq	%edx, %rdx
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3363:
	.size	_ZN6icu_6719CollationDataWriter8copyDataEPKiiPKvPh, .-_ZN6icu_6719CollationDataWriter8copyDataEPKiiPKvPh
	.align 2
	.p2align 4
	.type	_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode.part.0, @function
_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode.part.0:
.LFB4493:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edi, %ebx
	subq	$440, %rsp
	movq	24(%rbp), %rax
	movl	%edi, -420(%rbp)
	movq	%rsi, -448(%rbp)
	movq	16(%rbp), %r15
	movq	%rax, -400(%rbp)
	movq	40(%rbp), %rax
	movq	%rcx, -376(%rbp)
	movq	%r8, -456(%rbp)
	movl	%r9d, -432(%rbp)
	movq	%rax, -408(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -384(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	cmpq	$0, 88(%r12)
	movq	32(%r12), %rax
	setne	%r14b
	movq	%rax, -416(%rbp)
	sall	$17, %r14d
	testb	%bl, %bl
	jne	.L99
	cmpq	$0, -416(%rbp)
	je	.L100
	cmpl	$1, 68(%r12)
	movq	80(%r12), %rsi
	sbbl	%r13d, %r13d
	movq	-384(%rbp), %rdi
	andl	$-8, %r13d
	addl	$60, %r13d
	cmpl	$1, 68(%r12)
	sbbl	%ebx, %ebx
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	andl	$-2, %ebx
	movq	%rax, %rdi
	movq	-416(%rbp), %rax
	addl	$15, %ebx
	movq	80(%rax), %rsi
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	movq	-384(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet7isEmptyEv@PLT
	movl	$64, %edx
	testb	%al, %al
	movl	$16, %eax
	cmovne	%r13d, %edx
	cmove	%eax, %ebx
	movq	-416(%rbp), %rax
	movl	%edx, -388(%rbp)
	movq	88(%rax), %rax
	cmpq	%rax, 88(%r12)
	je	.L68
	movl	96(%r12), %eax
	movb	$1, -389(%rbp)
	movl	$17, %ebx
	movl	$68, -388(%rbp)
	movl	%eax, -428(%rbp)
.L11:
	movq	-408(%rbp), %rsi
	leaq	-352(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movq	-376(%rbp), %rax
	movq	64(%rax), %rsi
	movq	32(%rax), %rdi
	movl	72(%rax), %edx
	movq	%rsi, -440(%rbp)
	testq	%rdi, %rdi
	je	.L19
	movl	%edx, -424(%rbp)
	call	_ZN6icu_6717CollationSettings25reorderTableHasSplitBytesEPKh@PLT
	movl	-424(%rbp), %edx
	testb	%al, %al
	je	.L19
.L63:
	movq	-440(%rbp), %rsi
	movq	-408(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	%edx, -424(%rbp)
	call	_ZNK6icu_6713CollationData17makeReorderRangesEPKiiRNS_9UVector32ER10UErrorCode@PLT
	movl	-424(%rbp), %edx
	leal	-1(%rdx), %eax
	movq	%rax, %rsi
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L17
	movl	%r14d, -424(%rbp)
	movq	-408(%rbp), %r14
	movl	%ebx, -460(%rbp)
	movq	-440(%rbp), %rbx
	movq	%r15, -472(%rbp)
	movq	%rax, %r15
	movq	%r12, -440(%rbp)
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L18:
	movl	(%rbx,%r15,4), %esi
	movl	%r15d, %edx
	movq	%r14, %rcx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3215insertElementAtEiiR10UErrorCode@PLT
	movq	%r15, %rdx
	addq	$1, %r15
	cmpq	%r12, %rdx
	jne	.L18
	movl	-424(%rbp), %r14d
	movl	-460(%rbp), %ebx
	movq	-440(%rbp), %r12
	movq	-472(%rbp), %r15
.L17:
	movq	-408(%rbp), %rax
	movl	$0, -424(%rbp)
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jg	.L15
	movq	-328(%rbp), %rax
	cmpb	$0, -420(%rbp)
	movl	-344(%rbp), %edx
	movq	%rax, -440(%rbp)
	je	.L19
	movq	-376(%rbp), %rax
	movl	$0, -424(%rbp)
	movq	32(%rax), %rdi
	movq	%rax, %rcx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-376(%rbp), %rax
	movb	$0, -389(%rbp)
	movl	72(%rax), %eax
	movl	%eax, -428(%rbp)
	testl	%eax, %eax
	jne	.L101
	movl	$8, -388(%rbp)
	movl	$2, %ebx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L19:
	movdqa	_ZN6icu_67L8dataInfoE(%rip), %xmm1
	movl	16+_ZN6icu_67L8dataInfoE(%rip), %eax
	cmpb	$0, -389(%rbp)
	movl	%eax, -300(%rbp)
	movups	%xmm1, -316(%rbp)
	je	.L71
	movl	64(%r12), %r8d
	testl	%r8d, %r8d
	jne	.L102
.L71:
	movl	$24, %esi
	xorl	%r11d, %r11d
	movl	$24, %eax
	movl	$24, -424(%rbp)
	movl	-424(%rbp), %ecx
	cmpl	32(%rbp), %ecx
	jle	.L103
.L73:
	movl	$0, 32(%rbp)
	movq	$0, -400(%rbp)
.L21:
	movq	-376(%rbp), %rax
	movq	32(%rax), %rdi
	movq	%rax, %rcx
.L9:
	movl	%ebx, (%r15)
	movl	56(%r12), %eax
	orl	24(%rcx), %eax
	movq	$0, 8(%r15)
	orl	%eax, %r14d
	movl	-388(%rbp), %eax
	cmpb	$0, -389(%rbp)
	movl	%r14d, 4(%r15)
	leal	(%rax,%rdx,4), %ebx
	je	.L24
	cmpb	$0, -420(%rbp)
	movq	40(%r12), %rax
	jne	.L25
	movq	-416(%rbp), %rsi
	cmpq	%rax, 40(%rsi)
	je	.L26
.L25:
	subq	8(%r12), %rax
	sarq	$2, %rax
	movl	%eax, 16(%r15)
.L98:
	movl	-388(%rbp), %eax
	movl	%ebx, 24(%r15)
	movl	%eax, 20(%r15)
	testq	%rdi, %rdi
	je	.L95
	addl	$256, %ebx
.L95:
	movl	%ebx, 28(%r15)
	movq	(%r12), %rdi
	leaq	-356(%rbp), %r14
	movl	$0, -356(%rbp)
	cmpl	%ebx, 32(%rbp)
	jle	.L29
	movl	32(%rbp), %edx
	movslq	%ebx, %rsi
	movq	%r14, %rcx
	addq	-400(%rbp), %rsi
	subl	%ebx, %edx
	call	utrie2_serialize_67@PLT
.L30:
	movl	-356(%rbp), %edx
	cmpl	$15, %edx
	je	.L77
	testl	%edx, %edx
	jg	.L37
.L77:
	addl	%eax, %ebx
	movl	-432(%rbp), %esi
	movq	-384(%rbp), %rdi
	movl	%ebx, 32(%r15)
	movl	%ebx, 36(%r15)
	movl	64(%r12), %eax
	leal	(%rbx,%rax,8), %edx
	testl	%eax, %eax
	cmovne	%edx, %ebx
	movl	%ebx, 40(%r15)
	movl	%ebx, 44(%r15)
	movl	60(%r12), %eax
	leal	(%rbx,%rax,4), %eax
	movl	%eax, 48(%r15)
	leal	(%rax,%rsi,4), %eax
	movl	%eax, 52(%r15)
	movl	68(%r12), %edx
	leal	(%rax,%rdx,2), %ebx
	movl	%ebx, 56(%r15)
	call	_ZNK6icu_6710UnicodeSet7isEmptyEv@PLT
	testb	%al, %al
	je	.L56
.L39:
	movl	-428(%rbp), %eax
	cmpb	$0, -420(%rbp)
	movl	%ebx, 60(%r15)
	leal	(%rbx,%rax,2), %r8d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -320(%rbp)
	movl	$2, %eax
	movw	%ax, -312(%rbp)
	movl	%r8d, 64(%r15)
	jne	.L104
	movl	%r8d, 68(%r15)
	movl	$2, %ebx
	leaq	-320(%rbp), %r14
.L55:
	movl	-424(%rbp), %eax
	movl	%r8d, 72(%r15)
	movl	%r8d, 76(%r15)
	addl	%r8d, %eax
	movl	%eax, -424(%rbp)
	cmpl	%r8d, 32(%rbp)
	jge	.L43
	movq	-408(%rbp), %rax
	movl	$15, (%rax)
.L44:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L15:
	movq	%r13, %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	movq	-384(%rbp), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	movl	-424(%rbp), %eax
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movl	$-1, 16(%r15)
	movl	%eax, 20(%r15)
	movl	%ebx, 24(%r15)
	testq	%rdi, %rdi
	je	.L96
	addl	$256, %ebx
.L96:
	movl	-432(%rbp), %eax
	movd	%ebx, %xmm2
	movl	%ebx, 28(%r15)
	movl	%ebx, 48(%r15)
	pshufd	$0, %xmm2, %xmm0
	leal	(%rbx,%rax,4), %ebx
	movups	%xmm0, 32(%r15)
	movl	%ebx, 52(%r15)
	movl	%ebx, 56(%r15)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L43:
	movq	-400(%rbp), %rcx
	movq	(%r15), %rax
	movq	%r15, %rsi
	movq	%rax, (%rcx)
	movl	-388(%rbp), %eax
	leaq	8(%rcx), %rdi
	andq	$-8, %rdi
	movq	-8(%r15,%rax), %rdx
	movq	%rdx, -8(%rcx,%rax)
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	%eax, %ecx
	shrl	$3, %ecx
	rep movsq
	movl	20(%r15), %eax
	movl	24(%r15), %edx
	cmpl	%edx, %eax
	jl	.L106
.L45:
	movl	28(%r15), %eax
	cmpl	%edx, %eax
	jg	.L107
.L46:
	movl	36(%r15), %eax
	movl	40(%r15), %edx
	cmpl	%edx, %eax
	jl	.L108
.L47:
	movl	44(%r15), %edx
	movl	48(%r15), %eax
	cmpl	%eax, %edx
	jl	.L109
.L48:
	movl	52(%r15), %edx
	cmpl	%edx, %eax
	jl	.L110
.L49:
	movl	56(%r15), %eax
	cmpl	%eax, %edx
	jl	.L111
.L50:
	movl	60(%r15), %eax
	movl	64(%r15), %ecx
	cmpl	%ecx, %eax
	jl	.L112
.L51:
	testb	$17, %bl
	jne	.L74
	leaq	-310(%rbp), %rsi
	andl	$2, %ebx
	cmove	-296(%rbp), %rsi
.L52:
	movl	68(%r15), %edx
	cmpl	%edx, %ecx
	jl	.L113
.L53:
	movl	72(%r15), %eax
	cmpl	%eax, %edx
	jge	.L44
	subl	%edx, %eax
	movq	72(%r12), %rsi
	movslq	%edx, %rdi
	addq	-400(%rbp), %rdi
	movslq	%eax, %rdx
	call	memcpy@PLT
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L99:
	movq	80(%r12), %rsi
	movq	-384(%rbp), %rdi
	leaq	-352(%rbp), %r13
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movl	96(%r12), %eax
	movq	-408(%rbp), %rsi
	movq	%r13, %rdi
	movl	%eax, -428(%rbp)
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movq	-376(%rbp), %rax
	movq	64(%rax), %rcx
	movq	32(%rax), %rdi
	movl	72(%rax), %edx
	movq	%rcx, -440(%rbp)
	testq	%rdi, %rdi
	je	.L94
	movl	%edx, -388(%rbp)
	call	_ZN6icu_6717CollationSettings25reorderTableHasSplitBytesEPKh@PLT
	movl	-388(%rbp), %edx
	testb	%al, %al
	jne	.L76
	movq	-376(%rbp), %rax
	movq	32(%rax), %rdi
.L94:
	movb	$1, -389(%rbp)
	movl	$20, %ebx
	movq	%rax, %rcx
	movl	$80, -388(%rbp)
	movl	$0, -424(%rbp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L104:
	movl	100(%r12), %eax
	leaq	-320(%rbp), %r14
	xorl	%edx, %edx
	movl	$1, %ecx
	leaq	-356(%rbp), %rsi
	movq	%r14, %rdi
	movl	%r8d, -416(%rbp)
	movw	%ax, -356(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	104(%r12), %rbx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	100(%r12), %eax
	movq	%rbx, %rsi
	leal	16(%rax), %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	112(%r12), %rbx
	movl	120(%r12), %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	-312(%rbp), %ebx
	movl	-416(%rbp), %r8d
	testw	%bx, %bx
	js	.L41
	movswl	%bx, %eax
	sarl	$5, %eax
.L42:
	leal	(%r8,%rax,2), %r8d
	movl	%r8d, 68(%r15)
	addl	$256, %r8d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r14, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	utrie2_serialize_67@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$0, -356(%rbp)
	cmpl	%ebx, 32(%rbp)
	jle	.L35
	movl	32(%rbp), %edx
	movq	-384(%rbp), %rdi
	movslq	%ebx, %rsi
	movq	%r14, %rcx
	addq	-400(%rbp), %rsi
	subl	%ebx, %edx
	sarl	%edx
	call	_ZNK6icu_6710UnicodeSet9serializeEPtiR10UErrorCode@PLT
.L36:
	movl	-356(%rbp), %edx
	testl	%edx, %edx
	jle	.L78
	cmpl	$15, %edx
	jne	.L37
.L78:
	leal	(%rbx,%rax,2), %ebx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L37:
	movq	-408(%rbp), %rax
	movl	$0, -424(%rbp)
	movl	%edx, (%rax)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L102:
	leal	(%rbx,%rdx), %ecx
	sall	$2, %ecx
	andl	$7, %ecx
	cmpl	$1, %ecx
	movl	%ecx, %r11d
	sbbq	%rsi, %rsi
	andq	$-4, %rsi
	addq	$28, %rsi
	cmpl	$1, %ecx
	sbbl	%eax, %eax
	andl	$-4, %eax
	addl	$28, %eax
	cmpl	$1, %ecx
	sbbl	%ecx, %ecx
	andl	$-4, %ecx
	addl	$28, %ecx
	movl	%ecx, -424(%rbp)
	movl	-424(%rbp), %ecx
	cmpl	32(%rbp), %ecx
	jg	.L73
.L103:
	movq	-448(%rbp), %rdi
	movw	%ax, -320(%rbp)
	movl	%r11d, %r8d
	movl	(%rdi), %ecx
	movl	$10202, %edi
	movw	%di, -318(%rbp)
	movdqa	-320(%rbp), %xmm3
	movl	%ecx, -300(%rbp)
	movq	-400(%rbp), %rcx
	movq	-304(%rbp), %rax
	movups	%xmm3, (%rcx)
	leaq	24(%rcx), %rdi
	movq	%rax, 16(%rcx)
	testl	%r11d, %r11d
	je	.L23
	xorl	%eax, %eax
.L22:
	movl	%eax, %ecx
	addl	$1, %eax
	movb	$0, (%rdi,%rcx)
	cmpl	%r8d, %eax
	jb	.L22
.L23:
	movl	-424(%rbp), %eax
	addq	%rsi, -400(%rbp)
	subl	%eax, 32(%rbp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L41:
	movl	-308(%rbp), %eax
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$32, -388(%rbp)
	movl	$8, %ebx
	movl	$0, -428(%rbp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$-1, 16(%r15)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$0, -428(%rbp)
	movb	$1, -389(%rbp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L113:
	subl	%ecx, %edx
	movslq	%ecx, %rdi
	addq	-400(%rbp), %rdi
	movslq	%edx, %rdx
	call	memcpy@PLT
	movl	68(%r15), %edx
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L112:
	subl	%eax, %ecx
	movq	88(%r12), %rsi
	movslq	%eax, %rdi
	addq	-400(%rbp), %rdi
	movslq	%ecx, %rdx
	call	memcpy@PLT
	movl	64(%r15), %ecx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L111:
	subl	%edx, %eax
	movq	24(%r12), %rsi
	movslq	%edx, %rdi
	addq	-400(%rbp), %rdi
	movslq	%eax, %rdx
	call	memcpy@PLT
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L110:
	subl	%eax, %edx
	movq	-456(%rbp), %rsi
	movslq	%eax, %rdi
	addq	-400(%rbp), %rdi
	movslq	%edx, %rdx
	call	memcpy@PLT
	movl	52(%r15), %edx
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L109:
	subl	%edx, %eax
	movq	8(%r12), %rsi
	movslq	%edx, %rdi
	addq	-400(%rbp), %rdi
	movslq	%eax, %rdx
	call	memcpy@PLT
	movl	48(%r15), %eax
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L108:
	subl	%eax, %edx
	movq	16(%r12), %rsi
	movslq	%eax, %rdi
	addq	-400(%rbp), %rdi
	movslq	%edx, %rdx
	call	memcpy@PLT
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L107:
	subl	%edx, %eax
	movslq	%edx, %rdi
	addq	-400(%rbp), %rdi
	movslq	%eax, %rdx
	movq	-376(%rbp), %rax
	movq	32(%rax), %rsi
	call	memcpy@PLT
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L106:
	subl	%eax, %edx
	movq	-440(%rbp), %rsi
	movslq	%eax, %rdi
	addq	-400(%rbp), %rdi
	movslq	%edx, %rdx
	call	memcpy@PLT
	movl	24(%r15), %edx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L35:
	movq	-384(%rbp), %rdi
	movq	%r14, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNK6icu_6710UnicodeSet9serializeEPtiR10UErrorCode@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L74:
	xorl	%esi, %esi
	jmp	.L52
.L105:
	call	__stack_chk_fail@PLT
.L76:
	movl	$80, -388(%rbp)
	movl	$20, %ebx
	movb	$1, -389(%rbp)
	jmp	.L63
	.cfi_endproc
.LFE4493:
	.size	_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode.part.0, .-_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode
	.type	_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode, @function
_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode:
.LFB3362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	40(%rbp), %rax
	movq	16(%rbp), %rbx
	movq	24(%rbp), %r11
	movl	32(%rbp), %r10d
	cmpl	$0, (%rax)
	jg	.L114
	testl	%r10d, %r10d
	js	.L116
	jle	.L117
	testq	%r11, %r11
	jne	.L117
.L116:
	movl	$1, (%rax)
.L114:
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	movq	%rbx, 16(%rbp)
	movsbl	%dil, %edi
	popq	%rbx
	movq	%rax, 40(%rbp)
	movl	%r10d, 32(%rbp)
	movq	%r11, 24(%rbp)
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode.part.0
	.cfi_endproc
.LFE3362:
	.size	_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode, .-_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationDataWriter9writeBaseERKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode
	.type	_ZN6icu_6719CollationDataWriter9writeBaseERKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode, @function
_ZN6icu_6719CollationDataWriter9writeBaseERKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode:
.LFB3360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	24(%rbp), %r10
	movl	(%r10), %r11d
	testl	%r11d, %r11d
	jg	.L126
	movl	16(%rbp), %eax
	testl	%eax, %eax
	js	.L128
	jle	.L129
	testq	%r9, %r9
	jne	.L129
.L128:
	movl	$1, (%r10)
	xorl	%eax, %eax
.L126:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movl	16(%rbp), %eax
	pushq	%r10
	pushq	%rax
	pushq	%r9
	movl	%ecx, %r9d
	movq	%rsi, %rcx
	xorl	%esi, %esi
	pushq	%r8
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movl	$1, %edi
	call	_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode.part.0
	addq	$32, %rsp
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3360:
	.size	_ZN6icu_6719CollationDataWriter9writeBaseERKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode, .-_ZN6icu_6719CollationDataWriter9writeBaseERKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationDataWriter14writeTailoringERKNS_18CollationTailoringERKNS_17CollationSettingsEPiPhiR10UErrorCode
	.type	_ZN6icu_6719CollationDataWriter14writeTailoringERKNS_18CollationTailoringERKNS_17CollationSettingsEPiPhiR10UErrorCode, @function
_ZN6icu_6719CollationDataWriter14writeTailoringERKNS_18CollationTailoringERKNS_17CollationSettingsEPiPhiR10UErrorCode:
.LFB3361:
	.cfi_startproc
	endbr64
	movl	(%r9), %r10d
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L150
	testl	%r8d, %r8d
	js	.L140
	jle	.L141
	testq	%rcx, %rcx
	jne	.L141
.L140:
	movl	$1, (%r9)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	24(%rdi), %r11
	leaq	328(%rdi), %r10
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r9
	xorl	%r9d, %r9d
	pushq	%r8
	xorl	%r8d, %r8d
	pushq	%rcx
	movq	%rsi, %rcx
	movq	%r10, %rsi
	pushq	%rdx
	movq	%r11, %rdx
	call	_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode.part.0
	addq	$32, %rsp
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3361:
	.size	_ZN6icu_6719CollationDataWriter14writeTailoringERKNS_18CollationTailoringERKNS_17CollationSettingsEPiPhiR10UErrorCode, .-_ZN6icu_6719CollationDataWriter14writeTailoringERKNS_18CollationTailoringERKNS_17CollationSettingsEPiPhiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator11cloneBinaryEPhiR10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator11cloneBinaryEPhiR10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator11cloneBinaryEPhiR10UErrorCode:
.LFB3359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$104, %rsp
	.cfi_offset 12, -24
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L152
	testl	%edx, %edx
	js	.L154
	jle	.L155
	testq	%rsi, %rsi
	jne	.L155
.L154:
	movl	$1, (%rcx)
	xorl	%eax, %eax
.L152:
	movq	-24(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L165
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movq	24(%rdi), %rax
	movq	16(%rdi), %r12
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	movq	24(%rax), %r11
	leaq	328(%rax), %r10
	leaq	-112(%rbp), %rax
	pushq	%rcx
	pushq	%rdx
	movq	%r12, %rcx
	pushq	%rsi
	movq	%r11, %rdx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode.part.0
	addq	$32, %rsp
	jmp	.L152
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3359:
	.size	_ZNK6icu_6717RuleBasedCollator11cloneBinaryEPhiR10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator11cloneBinaryEPhiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator13cloneRuleDataERiR10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator13cloneRuleDataERiR10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator13cloneRuleDataERiR10UErrorCode:
.LFB3358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L166
	movq	%rdi, %r13
	movl	$20000, %edi
	movq	%rsi, %r14
	movq	%rdx, %rbx
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L190
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L191
	movq	24(%r13), %rsi
	leaq	-144(%rbp), %r15
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movq	16(%r13), %rcx
	xorl	%r8d, %r8d
	movq	24(%rsi), %rdx
	pushq	%rbx
	addq	$328, %rsi
	pushq	$20000
	pushq	%r12
	pushq	%r15
	call	_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode.part.0
	movl	(%rbx), %edx
	addq	$32, %rsp
	movl	%eax, (%r14)
	cmpl	$15, %edx
	je	.L192
.L172:
	testl	%edx, %edx
	jg	.L189
	xorl	%r10d, %r10d
.L169:
	movq	%r10, %rdi
	call	uprv_free_67@PLT
.L166:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L193
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movl	$0, (%r14)
	cmpl	$15, %eax
	je	.L171
.L189:
	movq	%r12, %r10
.L175:
	xorl	%r12d, %r12d
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L192:
	testl	%eax, %eax
	jle	.L171
	movslq	%eax, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	movq	%rax, -152(%rbp)
	je	.L171
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	movl	(%r14), %eax
	movl	$0, (%rbx)
	movq	-152(%rbp), %r10
	testl	%eax, %eax
	js	.L194
	movq	24(%r13), %rsi
	movq	16(%r13), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	movq	%r10, -152(%rbp)
	movq	24(%rsi), %rdx
	pushq	%rbx
	addq	$328, %rsi
	pushq	%rax
	pushq	%r10
	pushq	%r15
	call	_ZN6icu_6719CollationDataWriter5writeEaPKhRKNS_13CollationDataERKNS_17CollationSettingsEPKviPiPhiR10UErrorCode.part.0
	movq	-152(%rbp), %r10
	movl	(%rbx), %edx
	addq	$32, %rsp
	movl	%eax, (%r14)
	movq	%r10, %r12
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L171:
	movl	$7, (%rbx)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$1, (%rbx)
	movl	$0, (%r14)
	jmp	.L175
.L193:
	call	__stack_chk_fail@PLT
.L190:
	movl	$7, (%rbx)
	xorl	%r10d, %r10d
	jmp	.L169
	.cfi_endproc
.LFE3358:
	.size	_ZNK6icu_6717RuleBasedCollator13cloneRuleDataERiR10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator13cloneRuleDataERiR10UErrorCode
	.section	.rodata
	.align 16
	.type	_ZN6icu_67L8dataInfoE, @object
	.size	_ZN6icu_67L8dataInfoE, 20
_ZN6icu_67L8dataInfoE:
	.value	20
	.value	0
	.byte	0
	.byte	0
	.byte	2
	.byte	0
	.ascii	"UCol"
	.string	"\005"
	.zero	2
	.string	"\006\003"
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
