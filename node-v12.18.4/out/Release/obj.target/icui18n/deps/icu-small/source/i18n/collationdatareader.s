	.file	"collationdatareader.cpp"
	.text
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3797:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3797:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3800:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L15
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3
	cmpb	$0, 12(%rbx)
	jne	.L16
.L7:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L7
	.cfi_endproc
.LFE3800:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3803:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L19
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3803:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3806:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L22
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3806:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L28
.L24:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L29
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3808:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3809:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3809:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3810:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3810:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3811:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3811:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3812:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3812:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3813:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3813:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3814:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L45
	testl	%edx, %edx
	jle	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L48
.L37:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L37
	.cfi_endproc
.LFE3814:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L52
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L52
	testl	%r12d, %r12d
	jg	.L59
	cmpb	$0, 12(%rbx)
	jne	.L60
.L54:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L54
.L60:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3815:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L62
	movq	(%rdi), %r8
.L63:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L66
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L66
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3816:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3817:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L73
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3817:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3818:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3818:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3819:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3819:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3820:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3820:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3822:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3822:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3824:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3824:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationDataReader12isAcceptableEPvPKcS3_PK9UDataInfo
	.type	_ZN6icu_6719CollationDataReader12isAcceptableEPvPKcS3_PK9UDataInfo, @function
_ZN6icu_6719CollationDataReader12isAcceptableEPvPKcS3_PK9UDataInfo:
.LFB3351:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpw	$19, (%rcx)
	jbe	.L79
	cmpw	$0, 4(%rcx)
	jne	.L79
	cmpw	$17237, 8(%rcx)
	jne	.L79
	cmpw	$27759, 10(%rcx)
	jne	.L79
	cmpb	$5, 12(%rcx)
	je	.L88
.L79:
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	movl	$1, %eax
	testq	%rdi, %rdi
	je	.L79
	movl	16(%rcx), %edx
	movl	%edx, (%rdi)
	ret
	.cfi_endproc
.LFE3351:
	.size	_ZN6icu_6719CollationDataReader12isAcceptableEPvPKcS3_PK9UDataInfo, .-_ZN6icu_6719CollationDataReader12isAcceptableEPvPKcS3_PK9UDataInfo
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationDataReader4readEPKNS_18CollationTailoringEPKhiRS1_R10UErrorCode
	.type	_ZN6icu_6719CollationDataReader4readEPKNS_18CollationTailoringEPKhiRS1_R10UErrorCode, @function
_ZN6icu_6719CollationDataReader4readEPKNS_18CollationTailoringEPKhiRS1_R10UErrorCode:
.LFB3350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$904, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L89
	testq	%rsi, %rsi
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%rcx, %r13
	sete	%r15b
	testq	%rdi, %rdi
	je	.L91
	cmpl	$23, %edx
	setbe	%al
	orb	%al, %r15b
	jne	.L98
	cmpw	$10202, 2(%rsi)
	je	.L285
.L147:
	movl	$3, (%r8)
.L89:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L286
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movl	$1, (%r8)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L291:
	movzwl	(%rbx), %ecx
	movl	-864(%rbp), %edx
	addq	%rcx, %rbx
	testl	%edx, %edx
	js	.L97
	subl	%ecx, %edx
	.p2align 4,,10
	.p2align 3
.L91:
	cmpl	$7, %edx
	jbe	.L98
	testb	%r15b, %r15b
	jne	.L98
	movl	(%rbx), %r15d
	cmpl	$1, %r15d
	jle	.L147
	testl	%edx, %edx
	js	.L99
	leal	0(,%r15,4), %eax
	cmpl	%edx, %eax
	jg	.L147
.L99:
	cmpl	$19, %r15d
	jg	.L287
	cmpl	$5, %r15d
	jle	.L102
	movslq	%r15d, %rax
	movl	-4(%rbx,%rax,4), %eax
.L101:
	testl	%edx, %edx
	js	.L194
	cmpl	%edx, %eax
	jg	.L147
.L194:
	testq	%r12, %r12
	je	.L105
.L176:
	movq	24(%r12), %r12
.L105:
	cmpl	$5, %r15d
	jle	.L106
	movslq	20(%rbx), %rax
	cmpl	$6, %r15d
	je	.L181
	movslq	24(%rbx), %rsi
.L107:
	movl	%esi, %r10d
	subl	%eax, %r10d
	cmpl	$3, %r10d
	jle	.L283
	testq	%r12, %r12
	je	.L147
	leaq	(%rbx,%rax), %rcx
	sarl	$2, %r10d
	xorl	%r14d, %r14d
	movq	%rcx, -864(%rbp)
	movslq	%r10d, %rax
	leaq	(%rcx,%rax,4), %rax
	.p2align 4,,10
	.p2align 3
.L111:
	movl	%r10d, %edx
	subl	%r14d, %edx
	testl	$-65536, -4(%rax)
	je	.L109
	addl	$1, %r14d
	subq	$4, %rax
	cmpl	%r14d, %r10d
	jne	.L111
	xorl	%edx, %edx
.L110:
	movq	-864(%rbp), %rcx
	movslq	%edx, %rax
	movl	%r10d, %r14d
	movl	%edx, %r10d
	leaq	(%rcx,%rax,4), %rax
	movq	%rax, -872(%rbp)
.L108:
	cmpl	$7, %r15d
	jle	.L185
	movl	28(%rbx), %ecx
.L112:
	movl	%ecx, %eax
	subl	%esi, %eax
	cmpl	$255, %eax
	jle	.L186
	testl	%r10d, %r10d
	je	.L147
	leaq	(%rbx,%rsi), %rax
	movq	%rax, -880(%rbp)
.L113:
	testq	%r12, %r12
	je	.L114
	movl	4(%rbx), %eax
	andl	$-16777216, %eax
	cmpl	%eax, 56(%r12)
	jne	.L147
.L114:
	cmpl	$8, %r15d
	jle	.L187
	movl	32(%rbx), %edx
.L115:
	subl	%ecx, %edx
	cmpl	$7, %edx
	jg	.L288
	testq	%r12, %r12
	je	.L147
	movq	%r12, 24(%r13)
	movq	$0, -856(%rbp)
.L117:
	cmpl	$9, %r15d
	jle	.L118
	movslq	36(%rbx), %rdx
	cmpl	$10, %r15d
	je	.L188
	movl	40(%rbx), %eax
.L119:
	subl	%edx, %eax
	cmpl	$7, %eax
	jle	.L120
	movq	-856(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L147
	addq	%rbx, %rdx
	sarl	$3, %eax
	movq	%rdx, 16(%rcx)
	movl	%eax, 64(%rcx)
.L120:
	cmpl	$11, %r15d
	jle	.L118
	movslq	44(%rbx), %rdx
	cmpl	$12, %r15d
	je	.L121
	movl	48(%rbx), %eax
	subl	%edx, %eax
	cmpl	$3, %eax
	jg	.L122
.L125:
	movslq	16(%rbx), %rax
	testl	%eax, %eax
	js	.L124
	cmpq	$0, -856(%rbp)
	je	.L147
.L178:
	movq	-856(%rbp), %rcx
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L147
	leaq	(%rdx,%rax,4), %rax
	movq	%rax, 40(%rcx)
.L127:
	cmpl	$12, %r15d
	jle	.L135
	movslq	48(%rbx), %rax
	cmpl	$13, %r15d
	je	.L130
	movl	52(%rbx), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	cmpl	$3, %ecx
	jle	.L132
.L131:
	movq	-856(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L147
	cmpl	$19, %ecx
	jle	.L147
	addq	%rbx, %rax
	sarl	$2, %ecx
	movl	%ecx, 136(%rdi)
	cmpl	$83887360, 12(%rax)
	movq	%rax, 128(%rdi)
	jne	.L147
	cmpb	$68, 19(%rax)
	jbe	.L147
	cmpl	$13, %r15d
	jle	.L135
	movl	52(%rbx), %edx
.L132:
	cmpl	$14, %r15d
	jle	.L136
	movl	56(%rbx), %r11d
	movl	%r11d, %ecx
	subl	%edx, %ecx
	cmpl	$1, %ecx
	jle	.L138
.L137:
	movq	-856(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L147
	movslq	%edx, %rax
	sarl	%ecx
	addq	%rbx, %rax
	movl	%ecx, 68(%rdi)
	movq	%rax, 24(%rdi)
.L135:
	cmpl	$14, %r15d
	jle	.L141
	movl	56(%rbx), %r11d
.L138:
	cmpl	$15, %r15d
	jle	.L189
	movl	60(%rbx), %eax
.L140:
	subl	%r11d, %eax
	cmpl	$1, %eax
	jle	.L141
	cmpq	$0, -856(%rbp)
	je	.L147
	testq	%r12, %r12
	je	.L289
	movq	80(%r12), %rdi
	movq	%r8, -912(%rbp)
	movl	%r11d, -904(%rbp)
	movl	%r10d, -888(%rbp)
	movl	%eax, -896(%rbp)
	call	_ZNK6icu_6710UnicodeSet13cloneAsThawedEv@PLT
	movl	-888(%rbp), %r10d
	movl	-896(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, 376(%r13)
	movl	-904(%rbp), %r11d
	movq	-912(%rbp), %r8
	je	.L284
.L146:
	movl	%r9d, %edx
	movslq	%r11d, %rsi
	leaq	-832(%rbp), %r9
	movq	%r8, -896(%rbp)
	sarl	%edx
	addq	%rbx, %rsi
	movq	%r9, %rdi
	movl	%r10d, -912(%rbp)
	movq	%r9, -888(%rbp)
	call	uset_getSerializedSet_67@PLT
	movq	-896(%rbp), %r8
	testb	%al, %al
	je	.L147
	movq	-888(%rbp), %r9
	movq	%r8, -904(%rbp)
	movq	%r9, %rdi
	movq	%r9, -896(%rbp)
	call	uset_getSerializedRangeCount_67@PLT
	movq	-896(%rbp), %r9
	movq	-904(%rbp), %r8
	testl	%eax, %eax
	movl	%eax, -888(%rbp)
	movl	-912(%rbp), %r10d
	jle	.L150
	movl	%r10d, -904(%rbp)
	xorl	%r11d, %r11d
	leaq	-836(%rbp), %rax
	leaq	-840(%rbp), %rcx
	movq	%r8, -936(%rbp)
	movl	%r15d, -896(%rbp)
	movq	%rax, %r15
	movl	%r14d, -912(%rbp)
	movq	%rcx, %r14
	movq	%r12, -920(%rbp)
	movl	%r11d, %r12d
	movq	%rbx, -928(%rbp)
	movq	%r9, %rbx
.L148:
	movl	%r12d, %esi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	uset_getSerializedRange_67@PLT
	movq	376(%r13), %rdi
	movl	-836(%rbp), %edx
	addl	$1, %r12d
	movl	-840(%rbp), %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	cmpl	%r12d, -888(%rbp)
	jne	.L148
	movl	-896(%rbp), %r15d
	movl	-904(%rbp), %r10d
	movl	-912(%rbp), %r14d
	movq	-920(%rbp), %r12
	movq	-928(%rbp), %rbx
	movq	-936(%rbp), %r8
.L150:
	movl	$55296, %ecx
	movq	%r12, -896(%rbp)
	movq	%r13, %r12
	movq	%r8, %r13
	movq	%rbx, -904(%rbp)
	movl	%ecx, %ebx
	movl	%r10d, -888(%rbp)
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L151:
	addl	$1, %ebx
	cmpl	$56320, %ebx
	je	.L290
.L149:
	movl	%ebx, %esi
	movq	376(%r12), %rdi
	sall	$10, %esi
	leal	-56556545(%rsi), %edx
	subl	$56557568, %esi
	call	_ZNK6icu_6710UnicodeSet12containsNoneEii@PLT
	testb	%al, %al
	jne	.L151
	movq	376(%r12), %rdi
	movl	%ebx, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L285:
	cmpw	$19, 4(%rsi)
	jbe	.L147
	cmpw	$0, 8(%rsi)
	jne	.L147
	cmpw	$17237, 12(%rsi)
	jne	.L147
	cmpw	$27759, 14(%rsi)
	jne	.L147
	cmpb	$5, 16(%rsi)
	jne	.L147
	movl	20(%rsi), %eax
	movq	%r8, -856(%rbp)
	movl	%edx, -864(%rbp)
	movl	%eax, 328(%rcx)
	call	_ZNK6icu_6718CollationTailoring13getUCAVersionEv@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	_ZNK6icu_6718CollationTailoring13getUCAVersionEv@PLT
	movq	-856(%rbp), %r8
	cmpl	%eax, %r14d
	je	.L291
	movl	$28, (%r8)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L287:
	movl	76(%rbx), %eax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L102:
	testq	%r12, %r12
	jne	.L176
.L106:
	orq	$-1, %rsi
	.p2align 4,,10
	.p2align 3
.L283:
	movq	$0, -872(%rbp)
	xorl	%r14d, %r14d
	xorl	%r10d, %r10d
	movq	$0, -864(%rbp)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L186:
	movq	$0, -880(%rbp)
	jmp	.L113
.L109:
	testl	%r14d, %r14d
	jne	.L292
	movq	$0, -872(%rbp)
	jmp	.L108
.L97:
	movl	(%rbx), %r15d
	cmpl	$1, %r15d
	jg	.L99
	jmp	.L147
.L288:
	movq	%r8, %rsi
	movq	%r13, %rdi
	movl	%ecx, -912(%rbp)
	movl	%edx, -904(%rbp)
	movl	%r10d, -896(%rbp)
	movq	%r8, -888(%rbp)
	call	_ZN6icu_6718CollationTailoring15ensureOwnedDataER10UErrorCode@PLT
	testb	%al, %al
	je	.L89
	movl	4(%rbx), %eax
	movq	336(%r13), %rdi
	movslq	-912(%rbp), %rcx
	movq	-888(%rbp), %r8
	andl	$-16777216, %eax
	movq	%r12, 32(%rdi)
	movl	-904(%rbp), %edx
	movl	%eax, 56(%rdi)
	leaq	(%rbx,%rcx), %rsi
	xorl	%ecx, %ecx
	movq	%rdi, -856(%rbp)
	movl	$1, %edi
	call	utrie2_openFromSerialized_67@PLT
	movq	-888(%rbp), %r8
	movq	-856(%rbp), %rdi
	movq	%rax, 368(%r13)
	movl	-896(%rbp), %r10d
	movl	(%r8), %ecx
	movq	%rax, (%rdi)
	testl	%ecx, %ecx
	jle	.L117
	jmp	.L89
.L121:
	movl	%edx, %eax
	notl	%eax
	cmpl	$3, %eax
	jle	.L125
.L122:
	cmpq	$0, -856(%rbp)
	je	.L147
	movq	-856(%rbp), %rdi
	sarl	$2, %eax
	addq	%rbx, %rdx
	movl	%eax, 60(%rdi)
	movslq	16(%rbx), %rax
	movq	%rdx, 8(%rdi)
	testl	%eax, %eax
	jns	.L178
.L128:
	testq	%r12, %r12
	je	.L147
	movq	40(%r12), %rax
	movq	-856(%rbp), %rcx
	movq	%rax, 40(%rcx)
	jmp	.L127
.L118:
	cmpl	$4, %r15d
	jg	.L125
.L124:
	cmpq	$0, -856(%rbp)
	je	.L127
	jmp	.L128
.L130:
	movl	%eax, %ecx
	notl	%ecx
	cmpl	$3, %ecx
	jg	.L131
.L141:
	movq	-856(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L154
	testq	%r12, %r12
	je	.L147
	movq	80(%r12), %rax
	movq	%rax, 80(%rcx)
	movq	%rcx, %rax
.L152:
	movl	$0, 96(%rax)
	cmpb	$2, 6(%rbx)
	movq	$0, 88(%rax)
	je	.L293
.L154:
	cmpl	$16, %r15d
	jle	.L158
	movslq	64(%rbx), %rdx
	cmpl	$17, %r15d
	je	.L192
	movl	68(%rbx), %eax
.L159:
	subl	%edx, %eax
	cmpl	$1, %eax
	jle	.L158
	movq	-856(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L147
	addq	%rbx, %rdx
	sarl	%eax
	movzwl	(%rdx), %esi
	movl	%esi, 100(%rdi)
	movq	%rsi, %rcx
	addl	$17, %esi
	subl	%esi, %eax
	leal	-3(%rax), %esi
	movl	%eax, 120(%rdi)
	cmpl	$253, %esi
	ja	.L147
	leaq	2(%rdx), %rsi
	leaq	34(%rdx,%rcx,2), %rdx
	cmpw	$0, (%rdx)
	movq	%rsi, 104(%rdi)
	movq	%rdx, 112(%rdi)
	jne	.L147
	cmpw	$768, 2(%rdx)
	jne	.L147
	cltq
	cmpw	$-256, -2(%rdx,%rax,2)
	jne	.L147
	cmpl	$17, %r15d
	jle	.L177
.L161:
	movslq	68(%rbx), %rax
	cmpl	$18, %r15d
	je	.L193
	movl	72(%rbx), %edx
.L165:
	subl	%eax, %edx
	cmpl	$255, %edx
	jle	.L162
	movq	-856(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L147
	addq	%rbx, %rax
	movq	%rax, 72(%rcx)
.L166:
	movq	32(%r13), %r15
	movzwl	4(%rbx), %eax
	leaq	-832(%rbp), %r9
	movl	$384, %ecx
	movq	24(%r13), %rdi
	movq	%r9, %rdx
	movq	%r8, -904(%rbp)
	movq	%r15, %rsi
	movl	%r10d, -896(%rbp)
	movl	%eax, -856(%rbp)
	movq	%r9, -888(%rbp)
	call	_ZN6icu_6718CollationFastLatin10getOptionsEPKNS_13CollationDataERKNS_17CollationSettingsEPti@PLT
	movl	-896(%rbp), %r10d
	movq	-904(%rbp), %r8
	movl	%eax, %ebx
	movl	-856(%rbp), %eax
	cmpl	%eax, 24(%r15)
	jne	.L167
	cmpl	$0, 28(%r15)
	je	.L167
	cmpl	%r10d, 72(%r15)
	movq	-888(%rbp), %r9
	je	.L294
.L167:
	movq	32(%r13), %r15
	movq	%r8, -896(%rbp)
	movl	%r10d, -888(%rbp)
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	movl	-888(%rbp), %r10d
	movq	-896(%rbp), %r8
	cmpl	$1, %eax
	jle	.L169
	movl	$856, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movl	-888(%rbp), %r10d
	movq	-896(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L284
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%r8, -896(%rbp)
	movl	%r10d, -888(%rbp)
	call	_ZN6icu_6717CollationSettingsC1ERKS0_@PLT
	movq	%r15, %rdi
	movq	%rbx, %r15
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	%rbx, 32(%r13)
	movq	%rbx, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movl	-888(%rbp), %r10d
	movq	-896(%rbp), %r8
.L171:
	movl	-856(%rbp), %eax
	movq	24(%r13), %rdi
	movq	%r8, -896(%rbp)
	movl	%r10d, -888(%rbp)
	movl	%eax, 24(%r15)
	sarl	$4, %eax
	movl	%eax, %esi
	andl	$7, %esi
	addl	$4096, %esi
	call	_ZNK6icu_6713CollationData22getLastPrimaryForGroupEi@PLT
	movq	-896(%rbp), %r8
	testl	%eax, %eax
	movl	%eax, 28(%r15)
	je	.L147
	movl	-888(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L295
.L173:
	movq	24(%r13), %rdi
	leaq	84(%r15), %rdx
	movl	$384, %ecx
	movq	%r15, %rsi
	call	_ZN6icu_6718CollationFastLatin10getOptionsEPKNS_13CollationDataERKNS_17CollationSettingsEPti@PLT
	movl	%eax, 80(%r15)
	jmp	.L89
.L158:
	movq	-856(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L163
	testq	%r12, %r12
	je	.L163
	movl	100(%r12), %eax
	movdqu	104(%r12), %xmm0
	movl	%eax, 100(%rcx)
	movl	120(%r12), %eax
	movups	%xmm0, 104(%rcx)
	movl	%eax, 120(%rcx)
	cmpl	$17, %r15d
	jg	.L161
.L164:
	movq	72(%r12), %rax
	movq	-856(%rbp), %rcx
	movq	%rax, 72(%rcx)
	jmp	.L166
.L181:
	orq	$-1, %rsi
	jmp	.L107
.L185:
	orl	$-1, %ecx
	jmp	.L112
.L290:
	movl	-888(%rbp), %r10d
	movq	%r13, -888(%rbp)
	movq	%r12, %r13
	movq	376(%r13), %rdi
	movq	-904(%rbp), %rbx
	movl	%r10d, -912(%rbp)
	movq	-896(%rbp), %r12
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	376(%r13), %rax
	movq	-856(%rbp), %rcx
	movq	-888(%rbp), %r8
	movl	-912(%rbp), %r10d
	movq	%rax, 80(%rcx)
	movq	%rcx, %rax
	jmp	.L152
.L289:
	movl	$200, %edi
	movq	%r8, -888(%rbp)
	movl	%r11d, -920(%rbp)
	movl	%eax, -912(%rbp)
	movl	%r10d, -904(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-888(%rbp), %r8
	testq	%rax, %rax
	je	.L145
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movl	$850, %edx
	movq	%r8, -896(%rbp)
	leaq	_ZL21unsafe_serializedData(%rip), %rsi
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6710UnicodeSetC1EPKtiNS0_14ESerializationER10UErrorCode@PLT
	movq	-896(%rbp), %r8
	movq	-888(%rbp), %rax
	movl	-904(%rbp), %r10d
	movl	-912(%rbp), %r9d
	cmpl	$0, (%r8)
	movq	%rax, 376(%r13)
	movl	-920(%rbp), %r11d
	jle	.L146
	jmp	.L89
.L293:
	cmpl	$15, %r15d
	jle	.L155
	movslq	60(%rbx), %rax
	cmpl	$16, %r15d
	je	.L191
	movl	64(%rbx), %edx
.L156:
	subl	%eax, %edx
	cmpl	$1, %edx
	jle	.L155
	movq	-856(%rbp), %rcx
	addq	%rbx, %rax
	sarl	%edx
	cmpb	$2, 1(%rax)
	movq	%rax, 88(%rcx)
	movl	%edx, 96(%rcx)
	jne	.L147
	jmp	.L154
.L145:
	movq	$0, 376(%r13)
.L284:
	movl	$7, (%r8)
	jmp	.L89
.L163:
	cmpl	$17, %r15d
	jg	.L161
.L162:
	cmpq	$0, -856(%rbp)
	je	.L166
.L177:
	testq	%r12, %r12
	jne	.L164
	jmp	.L147
.L187:
	orl	$-1, %edx
	jmp	.L115
.L286:
	call	__stack_chk_fail@PLT
.L169:
	testq	%r15, %r15
	je	.L284
	jmp	.L171
.L155:
	testq	%r12, %r12
	je	.L154
	movq	88(%r12), %rax
	movq	-856(%rbp), %rcx
	movq	%rax, 88(%rcx)
	movl	96(%r12), %eax
	movl	%eax, 96(%rcx)
	jmp	.L154
.L188:
	orl	$-1, %eax
	jmp	.L119
.L295:
	pushq	%r8
	movq	-864(%rbp), %rdx
	movl	%r14d, %r9d
	movl	%r10d, %ecx
	pushq	-880(%rbp)
	movq	-872(%rbp), %r8
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6717CollationSettings15aliasReorderingERKNS_13CollationDataEPKiiPKjiPKhR10UErrorCode@PLT
	popq	%rax
	popq	%rdx
	jmp	.L173
.L294:
	testl	%r10d, %r10d
	je	.L168
	leal	0(,%r10,4), %edx
	movq	64(%r15), %rsi
	movq	-864(%rbp), %rdi
	movq	%r9, -904(%rbp)
	movslq	%edx, %rdx
	movq	%r8, -896(%rbp)
	movl	%r10d, -888(%rbp)
	call	memcmp@PLT
	movl	-888(%rbp), %r10d
	movq	-896(%rbp), %r8
	testl	%eax, %eax
	movq	-904(%rbp), %r9
	jne	.L167
.L168:
	cmpl	%ebx, 80(%r15)
	jne	.L167
	movq	%r8, -896(%rbp)
	movl	%r10d, -888(%rbp)
	testl	%ebx, %ebx
	js	.L89
	leaq	84(%r15), %rsi
	movl	$768, %edx
	movq	%r9, %rdi
	call	memcmp@PLT
	movl	-888(%rbp), %r10d
	movq	-896(%rbp), %r8
	testl	%eax, %eax
	jne	.L167
	jmp	.L89
.L192:
	orl	$-1, %eax
	jmp	.L159
.L189:
	orl	$-1, %eax
	jmp	.L140
.L136:
	movl	%edx, %ecx
	notl	%ecx
	cmpl	$1, %ecx
	jg	.L137
	jmp	.L141
.L292:
	movl	%r14d, %r10d
	jmp	.L110
.L193:
	orl	$-1, %edx
	jmp	.L165
.L191:
	orl	$-1, %edx
	jmp	.L156
	.cfi_endproc
.LFE3350:
	.size	_ZN6icu_6719CollationDataReader4readEPKNS_18CollationTailoringEPKhiRS1_R10UErrorCode, .-_ZN6icu_6719CollationDataReader4readEPKNS_18CollationTailoringEPKhiRS1_R10UErrorCode
	.section	.rodata
	.align 32
	.type	_ZL21unsafe_serializedData, @object
	.size	_ZL21unsafe_serializedData, 1700
_ZL21unsafe_serializedData:
	.value	-31920
	.value	440
	.value	52
	.value	53
	.value	76
	.value	77
	.value	160
	.value	161
	.value	768
	.value	847
	.value	848
	.value	880
	.value	937
	.value	938
	.value	994
	.value	995
	.value	1071
	.value	1072
	.value	1155
	.value	1160
	.value	1329
	.value	1330
	.value	1425
	.value	1470
	.value	1471
	.value	1472
	.value	1473
	.value	1475
	.value	1476
	.value	1478
	.value	1479
	.value	1480
	.value	1488
	.value	1489
	.value	1552
	.value	1563
	.value	1576
	.value	1577
	.value	1611
	.value	1632
	.value	1648
	.value	1649
	.value	1750
	.value	1757
	.value	1759
	.value	1765
	.value	1767
	.value	1769
	.value	1770
	.value	1774
	.value	1808
	.value	1810
	.value	1840
	.value	1867
	.value	1932
	.value	1933
	.value	2008
	.value	2009
	.value	2027
	.value	2036
	.value	2048
	.value	2049
	.value	2070
	.value	2074
	.value	2075
	.value	2084
	.value	2085
	.value	2088
	.value	2089
	.value	2094
	.value	2112
	.value	2113
	.value	2137
	.value	2140
	.value	2275
	.value	2304
	.value	2309
	.value	2310
	.value	2364
	.value	2365
	.value	2381
	.value	2382
	.value	2385
	.value	2389
	.value	2453
	.value	2454
	.value	2492
	.value	2493
	.value	2494
	.value	2495
	.value	2509
	.value	2510
	.value	2519
	.value	2520
	.value	2581
	.value	2582
	.value	2620
	.value	2621
	.value	2637
	.value	2638
	.value	2709
	.value	2710
	.value	2748
	.value	2749
	.value	2765
	.value	2766
	.value	2837
	.value	2838
	.value	2876
	.value	2877
	.value	2878
	.value	2879
	.value	2893
	.value	2894
	.value	2902
	.value	2904
	.value	2965
	.value	2966
	.value	3006
	.value	3007
	.value	3021
	.value	3022
	.value	3031
	.value	3032
	.value	3093
	.value	3094
	.value	3149
	.value	3150
	.value	3157
	.value	3159
	.value	3221
	.value	3222
	.value	3260
	.value	3261
	.value	3266
	.value	3267
	.value	3277
	.value	3278
	.value	3285
	.value	3287
	.value	3349
	.value	3350
	.value	3390
	.value	3391
	.value	3405
	.value	3406
	.value	3415
	.value	3416
	.value	3461
	.value	3462
	.value	3530
	.value	3531
	.value	3535
	.value	3536
	.value	3551
	.value	3552
	.value	3585
	.value	3631
	.value	3634
	.value	3635
	.value	3640
	.value	3643
	.value	3656
	.value	3660
	.value	3713
	.value	3715
	.value	3716
	.value	3717
	.value	3719
	.value	3721
	.value	3722
	.value	3723
	.value	3725
	.value	3726
	.value	3732
	.value	3736
	.value	3737
	.value	3744
	.value	3745
	.value	3748
	.value	3749
	.value	3750
	.value	3751
	.value	3752
	.value	3754
	.value	3756
	.value	3757
	.value	3759
	.value	3762
	.value	3763
	.value	3768
	.value	3770
	.value	3784
	.value	3788
	.value	3804
	.value	3808
	.value	3864
	.value	3866
	.value	3893
	.value	3894
	.value	3895
	.value	3896
	.value	3897
	.value	3898
	.value	3904
	.value	3905
	.value	3953
	.value	3958
	.value	3962
	.value	3966
	.value	3968
	.value	3973
	.value	3974
	.value	3976
	.value	4038
	.value	4039
	.value	4096
	.value	4097
	.value	4142
	.value	4143
	.value	4151
	.value	4152
	.value	4153
	.value	4155
	.value	4237
	.value	4238
	.value	4307
	.value	4308
	.value	4768
	.value	4769
	.value	4957
	.value	4960
	.value	5060
	.value	5061
	.value	5312
	.value	5313
	.value	5775
	.value	5776
	.value	5792
	.value	5793
	.value	5891
	.value	5892
	.value	5908
	.value	5909
	.value	5923
	.value	5924
	.value	5940
	.value	5941
	.value	5955
	.value	5956
	.value	5987
	.value	5988
	.value	6016
	.value	6017
	.value	6098
	.value	6099
	.value	6109
	.value	6110
	.value	6182
	.value	6183
	.value	6313
	.value	6314
	.value	6400
	.value	6401
	.value	6457
	.value	6460
	.value	6480
	.value	6481
	.value	6528
	.value	6572
	.value	6656
	.value	6657
	.value	6679
	.value	6681
	.value	6688
	.value	6689
	.value	6752
	.value	6753
	.value	6773
	.value	6781
	.value	6783
	.value	6784
	.value	6832
	.value	6846
	.value	6917
	.value	6918
	.value	6964
	.value	6966
	.value	6980
	.value	6981
	.value	7019
	.value	7028
	.value	7043
	.value	7044
	.value	7082
	.value	7084
	.value	7104
	.value	7105
	.value	7142
	.value	7143
	.value	7154
	.value	7156
	.value	7168
	.value	7169
	.value	7223
	.value	7224
	.value	7258
	.value	7259
	.value	7376
	.value	7379
	.value	7380
	.value	7393
	.value	7394
	.value	7401
	.value	7405
	.value	7406
	.value	7412
	.value	7413
	.value	7416
	.value	7418
	.value	7616
	.value	7670
	.value	7676
	.value	7680
	.value	8220
	.value	8221
	.value	8364
	.value	8365
	.value	8400
	.value	8413
	.value	8417
	.value	8418
	.value	8421
	.value	8433
	.value	9786
	.value	9787
	.value	11264
	.value	11265
	.value	11503
	.value	11506
	.value	11614
	.value	11615
	.value	11647
	.value	11648
	.value	11744
	.value	11776
	.value	12330
	.value	12336
	.value	12363
	.value	12364
	.value	12441
	.value	12443
	.value	12459
	.value	12460
	.value	12549
	.value	12550
	.value	23383
	.value	23384
	.value	-23928
	.value	-23927
	.value	-23320
	.value	-23319
	.value	-23223
	.value	-23222
	.value	-22929
	.value	-22928
	.value	-22924
	.value	-22914
	.value	-22882
	.value	-22879
	.value	-22800
	.value	-22798
	.value	-22528
	.value	-22527
	.value	-22522
	.value	-22521
	.value	-22464
	.value	-22463
	.value	-22398
	.value	-22397
	.value	-22332
	.value	-22331
	.value	-22304
	.value	-22286
	.value	-22262
	.value	-22261
	.value	-22229
	.value	-22226
	.value	-22224
	.value	-22223
	.value	-22189
	.value	-22188
	.value	-22140
	.value	-22139
	.value	-22093
	.value	-22092
	.value	-22080
	.value	-22079
	.value	-22016
	.value	-22015
	.value	-21888
	.value	-21839
	.value	-21838
	.value	-21835
	.value	-21833
	.value	-21831
	.value	-21826
	.value	-21824
	.value	-21823
	.value	-21822
	.value	-21770
	.value	-21769
	.value	-21568
	.value	-21567
	.value	-21523
	.value	-21522
	.value	-21504
	.value	-21503
	.value	-10240
	.value	-10233
	.value	-10232
	.value	-10231
	.value	-10228
	.value	-10227
	.value	-10223
	.value	-10222
	.value	-10214
	.value	-10212
	.value	-10193
	.value	-10192
	.value	-10188
	.value	-10187
	.value	-10182
	.value	-10181
	.value	-9216
	.value	-8192
	.value	-1250
	.value	-1249
	.value	-560
	.value	-559
	.value	-480
	.value	-464
	.value	1
	.value	0
	.value	1
	.value	1
	.value	1
	.value	509
	.value	1
	.value	510
	.value	1
	.value	640
	.value	1
	.value	641
	.value	1
	.value	695
	.value	1
	.value	696
	.value	1
	.value	736
	.value	1
	.value	737
	.value	1
	.value	776
	.value	1
	.value	777
	.value	1
	.value	816
	.value	1
	.value	817
	.value	1
	.value	875
	.value	1
	.value	876
	.value	1
	.value	886
	.value	1
	.value	891
	.value	1
	.value	896
	.value	1
	.value	897
	.value	1
	.value	928
	.value	1
	.value	929
	.value	1
	.value	1044
	.value	1
	.value	1045
	.value	1
	.value	1104
	.value	1
	.value	1105
	.value	1
	.value	1152
	.value	1
	.value	1153
	.value	1
	.value	1280
	.value	1
	.value	1281
	.value	1
	.value	1335
	.value	1
	.value	1336
	.value	1
	.value	1607
	.value	1
	.value	1608
	.value	1
	.value	2048
	.value	1
	.value	2049
	.value	1
	.value	2112
	.value	1
	.value	2113
	.value	1
	.value	2163
	.value	1
	.value	2164
	.value	1
	.value	2198
	.value	1
	.value	2199
	.value	1
	.value	2292
	.value	1
	.value	2293
	.value	1
	.value	2304
	.value	1
	.value	2305
	.value	1
	.value	2336
	.value	1
	.value	2337
	.value	1
	.value	2432
	.value	1
	.value	2433
	.value	1
	.value	2464
	.value	1
	.value	2465
	.value	1
	.value	2560
	.value	1
	.value	2561
	.value	1
	.value	2573
	.value	1
	.value	2574
	.value	1
	.value	2575
	.value	1
	.value	2576
	.value	1
	.value	2616
	.value	1
	.value	2619
	.value	1
	.value	2623
	.value	1
	.value	2624
	.value	1
	.value	2656
	.value	1
	.value	2657
	.value	1
	.value	2709
	.value	1
	.value	2710
	.value	1
	.value	2753
	.value	1
	.value	2754
	.value	1
	.value	2789
	.value	1
	.value	2791
	.value	1
	.value	2816
	.value	1
	.value	2817
	.value	1
	.value	2880
	.value	1
	.value	2881
	.value	1
	.value	2912
	.value	1
	.value	2913
	.value	1
	.value	2959
	.value	1
	.value	2960
	.value	1
	.value	3072
	.value	1
	.value	3073
	.value	1
	.value	3233
	.value	1
	.value	3234
	.value	1
	.value	4101
	.value	1
	.value	4102
	.value	1
	.value	4166
	.value	1
	.value	4167
	.value	1
	.value	4223
	.value	1
	.value	4224
	.value	1
	.value	4227
	.value	1
	.value	4228
	.value	1
	.value	4281
	.value	1
	.value	4283
	.value	1
	.value	4304
	.value	1
	.value	4305
	.value	1
	.value	4352
	.value	1
	.value	4356
	.value	1
	.value	4391
	.value	1
	.value	4392
	.value	1
	.value	4403
	.value	1
	.value	4405
	.value	1
	.value	4434
	.value	1
	.value	4435
	.value	1
	.value	4467
	.value	1
	.value	4468
	.value	1
	.value	4483
	.value	1
	.value	4484
	.value	1
	.value	4544
	.value	1
	.value	4545
	.value	1
	.value	4554
	.value	1
	.value	4555
	.value	1
	.value	4616
	.value	1
	.value	4617
	.value	1
	.value	4661
	.value	1
	.value	4663
	.value	1
	.value	4751
	.value	1
	.value	4752
	.value	1
	.value	4798
	.value	1
	.value	4799
	.value	1
	.value	4841
	.value	1
	.value	4843
	.value	1
	.value	4885
	.value	1
	.value	4886
	.value	1
	.value	4924
	.value	1
	.value	4925
	.value	1
	.value	4926
	.value	1
	.value	4927
	.value	1
	.value	4941
	.value	1
	.value	4942
	.value	1
	.value	4951
	.value	1
	.value	4952
	.value	1
	.value	4966
	.value	1
	.value	4973
	.value	1
	.value	4976
	.value	1
	.value	4981
	.value	1
	.value	5252
	.value	1
	.value	5253
	.value	1
	.value	5296
	.value	1
	.value	5297
	.value	1
	.value	5306
	.value	1
	.value	5307
	.value	1
	.value	5309
	.value	1
	.value	5310
	.value	1
	.value	5314
	.value	1
	.value	5316
	.value	1
	.value	5518
	.value	1
	.value	5519
	.value	1
	.value	5551
	.value	1
	.value	5552
	.value	1
	.value	5567
	.value	1
	.value	5569
	.value	1
	.value	5646
	.value	1
	.value	5647
	.value	1
	.value	5695
	.value	1
	.value	5696
	.value	1
	.value	5760
	.value	1
	.value	5761
	.value	1
	.value	5814
	.value	1
	.value	5816
	.value	1
	.value	5911
	.value	1
	.value	5912
	.value	1
	.value	5931
	.value	1
	.value	5932
	.value	1
	.value	6324
	.value	1
	.value	6325
	.value	1
	.value	6848
	.value	1
	.value	6849
	.value	1
	.value	8192
	.value	1
	.value	8193
	.value	1
	.value	12627
	.value	1
	.value	12628
	.value	1
	.value	17408
	.value	1
	.value	17409
	.value	1
	.value	27215
	.value	1
	.value	27216
	.value	1
	.value	27366
	.value	1
	.value	27367
	.value	1
	.value	27376
	.value	1
	.value	27381
	.value	1
	.value	27420
	.value	1
	.value	27421
	.value	1
	.value	27440
	.value	1
	.value	27447
	.value	1
	.value	28416
	.value	1
	.value	28417
	.value	1
	.value	-17376
	.value	1
	.value	-17375
	.value	1
	.value	-17250
	.value	1
	.value	-17249
	.value	1
	.value	-11931
	.value	1
	.value	-11926
	.value	1
	.value	-11923
	.value	1
	.value	-11917
	.value	1
	.value	-11909
	.value	1
	.value	-11901
	.value	1
	.value	-11899
	.value	1
	.value	-11892
	.value	1
	.value	-11862
	.value	1
	.value	-11858
	.value	1
	.value	-11710
	.value	1
	.value	-11707
	.value	1
	.value	-6142
	.value	1
	.value	-6141
	.value	1
	.value	-5936
	.value	1
	.value	-5929
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
