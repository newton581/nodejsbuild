	.file	"ucsdet.cpp"
	.text
	.p2align 4
	.globl	ucsdet_open_67
	.type	ucsdet_open_67, @function
ucsdet_open_67:
.LFB2093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rdi), %edx
	testl	%edx, %edx
	jg	.L9
	movq	%rdi, %rbx
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L9
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6715CharsetDetectorC1ER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1
	movq	%r12, %rdi
	call	_ZN6icu_6715CharsetDetectorD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L9:
	xorl	%r12d, %r12d
.L1:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2093:
	.size	ucsdet_open_67, .-ucsdet_open_67
	.p2align 4
	.globl	ucsdet_close_67
	.type	ucsdet_close_67, @function
ucsdet_close_67:
.LFB2094:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6715CharsetDetectorD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.cfi_endproc
.LFE2094:
	.size	ucsdet_close_67, .-ucsdet_close_67
	.p2align 4
	.globl	ucsdet_setText_67
	.type	ucsdet_setText_67, @function
ucsdet_setText_67:
.LFB2095:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L18
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	jmp	_ZN6icu_6715CharsetDetector7setTextEPKci@PLT
	.cfi_endproc
.LFE2095:
	.size	ucsdet_setText_67, .-ucsdet_setText_67
	.p2align 4
	.globl	ucsdet_getName_67
	.type	ucsdet_getName_67, @function
ucsdet_getName_67:
.LFB2096:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L20
	jmp	_ZNK6icu_6712CharsetMatch7getNameEv@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2096:
	.size	ucsdet_getName_67, .-ucsdet_getName_67
	.p2align 4
	.globl	ucsdet_getConfidence_67
	.type	ucsdet_getConfidence_67, @function
ucsdet_getConfidence_67:
.LFB2097:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L23
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	jmp	_ZNK6icu_6712CharsetMatch13getConfidenceEv@PLT
	.cfi_endproc
.LFE2097:
	.size	ucsdet_getConfidence_67, .-ucsdet_getConfidence_67
	.p2align 4
	.globl	ucsdet_getLanguage_67
	.type	ucsdet_getLanguage_67, @function
ucsdet_getLanguage_67:
.LFB2098:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L25
	jmp	_ZNK6icu_6712CharsetMatch11getLanguageEv@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2098:
	.size	ucsdet_getLanguage_67, .-ucsdet_getLanguage_67
	.p2align 4
	.globl	ucsdet_detect_67
	.type	ucsdet_detect_67, @function
ucsdet_detect_67:
.LFB2099:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L27
	jmp	_ZN6icu_6715CharsetDetector6detectER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2099:
	.size	ucsdet_detect_67, .-ucsdet_detect_67
	.p2align 4
	.globl	ucsdet_setDeclaredEncoding_67
	.type	ucsdet_setDeclaredEncoding_67, @function
ucsdet_setDeclaredEncoding_67:
.LFB2100:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L30
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	jmp	_ZNK6icu_6715CharsetDetector19setDeclaredEncodingEPKci@PLT
	.cfi_endproc
.LFE2100:
	.size	ucsdet_setDeclaredEncoding_67, .-ucsdet_setDeclaredEncoding_67
	.p2align 4
	.globl	ucsdet_detectAll_67
	.type	ucsdet_detectAll_67, @function
ucsdet_detectAll_67:
.LFB2101:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L32
	jmp	_ZN6icu_6715CharsetDetector9detectAllERiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2101:
	.size	ucsdet_detectAll_67, .-ucsdet_detectAll_67
	.p2align 4
	.globl	ucsdet_isInputFilterEnabled_67
	.type	ucsdet_isInputFilterEnabled_67, @function
ucsdet_isInputFilterEnabled_67:
.LFB2102:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L34
	jmp	_ZNK6icu_6715CharsetDetector16getStripTagsFlagEv@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2102:
	.size	ucsdet_isInputFilterEnabled_67, .-ucsdet_isInputFilterEnabled_67
	.p2align 4
	.globl	ucsdet_enableInputFilter_67
	.type	ucsdet_enableInputFilter_67, @function
ucsdet_enableInputFilter_67:
.LFB2103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L35
	movq	%rdi, %r12
	movl	%esi, %ebx
	call	_ZNK6icu_6715CharsetDetector16getStripTagsFlagEv@PLT
	movsbl	%bl, %esi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN6icu_6715CharsetDetector16setStripTagsFlagEa@PLT
.L35:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2103:
	.size	ucsdet_enableInputFilter_67, .-ucsdet_enableInputFilter_67
	.p2align 4
	.globl	ucsdet_getUChars_67
	.type	ucsdet_getUChars_67, @function
ucsdet_getUChars_67:
.LFB2104:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L43
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	jmp	_ZNK6icu_6712CharsetMatch9getUCharsEPDsiP10UErrorCode@PLT
	.cfi_endproc
.LFE2104:
	.size	ucsdet_getUChars_67, .-ucsdet_getUChars_67
	.p2align 4
	.globl	ucsdet_setDetectableCharset_67
	.type	ucsdet_setDetectableCharset_67, @function
ucsdet_setDetectableCharset_67:
.LFB2105:
	.cfi_startproc
	endbr64
	movsbl	%dl, %edx
	jmp	_ZN6icu_6715CharsetDetector20setDetectableCharsetEPKcaR10UErrorCode@PLT
	.cfi_endproc
.LFE2105:
	.size	ucsdet_setDetectableCharset_67, .-ucsdet_setDetectableCharset_67
	.p2align 4
	.globl	ucsdet_getAllDetectableCharsets_67
	.type	ucsdet_getAllDetectableCharsets_67, @function
ucsdet_getAllDetectableCharsets_67:
.LFB2106:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	jmp	_ZN6icu_6715CharsetDetector24getAllDetectableCharsetsER10UErrorCode@PLT
	.cfi_endproc
.LFE2106:
	.size	ucsdet_getAllDetectableCharsets_67, .-ucsdet_getAllDetectableCharsets_67
	.p2align 4
	.globl	ucsdet_getDetectableCharsets_67
	.type	ucsdet_getDetectableCharsets_67, @function
ucsdet_getDetectableCharsets_67:
.LFB2107:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6715CharsetDetector21getDetectableCharsetsER10UErrorCode@PLT
	.cfi_endproc
.LFE2107:
	.size	ucsdet_getDetectableCharsets_67, .-ucsdet_getDetectableCharsets_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
