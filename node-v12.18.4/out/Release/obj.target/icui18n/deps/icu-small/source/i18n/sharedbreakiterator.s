	.file	"sharedbreakiterator.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719SharedBreakIteratorD2Ev
	.type	_ZN6icu_6719SharedBreakIteratorD2Ev, @function
_ZN6icu_6719SharedBreakIteratorD2Ev:
.LFB2980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6719SharedBreakIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2
	movq	(%rdi), %rax
	call	*8(%rax)
.L2:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712SharedObjectD2Ev@PLT
	.cfi_endproc
.LFE2980:
	.size	_ZN6icu_6719SharedBreakIteratorD2Ev, .-_ZN6icu_6719SharedBreakIteratorD2Ev
	.globl	_ZN6icu_6719SharedBreakIteratorD1Ev
	.set	_ZN6icu_6719SharedBreakIteratorD1Ev,_ZN6icu_6719SharedBreakIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719SharedBreakIteratorD0Ev
	.type	_ZN6icu_6719SharedBreakIteratorD0Ev, @function
_ZN6icu_6719SharedBreakIteratorD0Ev:
.LFB2982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6719SharedBreakIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L9
	movq	(%rdi), %rax
	call	*8(%rax)
.L9:
	movq	%r12, %rdi
	call	_ZN6icu_6712SharedObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2982:
	.size	_ZN6icu_6719SharedBreakIteratorD0Ev, .-_ZN6icu_6719SharedBreakIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719SharedBreakIteratorC2EPNS_13BreakIteratorE
	.type	_ZN6icu_6719SharedBreakIteratorC2EPNS_13BreakIteratorE, @function
_ZN6icu_6719SharedBreakIteratorC2EPNS_13BreakIteratorE:
.LFB2977:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6719SharedBreakIteratorE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	%rax, (%rdi)
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE2977:
	.size	_ZN6icu_6719SharedBreakIteratorC2EPNS_13BreakIteratorE, .-_ZN6icu_6719SharedBreakIteratorC2EPNS_13BreakIteratorE
	.globl	_ZN6icu_6719SharedBreakIteratorC1EPNS_13BreakIteratorE
	.set	_ZN6icu_6719SharedBreakIteratorC1EPNS_13BreakIteratorE,_ZN6icu_6719SharedBreakIteratorC2EPNS_13BreakIteratorE
	.weak	_ZTSN6icu_6719SharedBreakIteratorE
	.section	.rodata._ZTSN6icu_6719SharedBreakIteratorE,"aG",@progbits,_ZTSN6icu_6719SharedBreakIteratorE,comdat
	.align 16
	.type	_ZTSN6icu_6719SharedBreakIteratorE, @object
	.size	_ZTSN6icu_6719SharedBreakIteratorE, 31
_ZTSN6icu_6719SharedBreakIteratorE:
	.string	"N6icu_6719SharedBreakIteratorE"
	.weak	_ZTIN6icu_6719SharedBreakIteratorE
	.section	.data.rel.ro._ZTIN6icu_6719SharedBreakIteratorE,"awG",@progbits,_ZTIN6icu_6719SharedBreakIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6719SharedBreakIteratorE, @object
	.size	_ZTIN6icu_6719SharedBreakIteratorE, 24
_ZTIN6icu_6719SharedBreakIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719SharedBreakIteratorE
	.quad	_ZTIN6icu_6712SharedObjectE
	.weak	_ZTVN6icu_6719SharedBreakIteratorE
	.section	.data.rel.ro._ZTVN6icu_6719SharedBreakIteratorE,"awG",@progbits,_ZTVN6icu_6719SharedBreakIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6719SharedBreakIteratorE, @object
	.size	_ZTVN6icu_6719SharedBreakIteratorE, 40
_ZTVN6icu_6719SharedBreakIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6719SharedBreakIteratorE
	.quad	_ZN6icu_6719SharedBreakIteratorD1Ev
	.quad	_ZN6icu_6719SharedBreakIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
