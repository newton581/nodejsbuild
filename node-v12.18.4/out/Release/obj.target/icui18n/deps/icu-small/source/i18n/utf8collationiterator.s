	.file	"utf8collationiterator.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721UTF8CollationIterator9getOffsetEv
	.type	_ZNK6icu_6721UTF8CollationIterator9getOffsetEv, @function
_ZNK6icu_6721UTF8CollationIterator9getOffsetEv:
.LFB3262:
	.cfi_startproc
	endbr64
	movl	400(%rdi), %eax
	ret
	.cfi_endproc
.LFE3262:
	.size	_ZNK6icu_6721UTF8CollationIterator9getOffsetEv, .-_ZNK6icu_6721UTF8CollationIterator9getOffsetEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721UTF8CollationIterator25forbidSurrogateCodePointsEv
	.type	_ZNK6icu_6721UTF8CollationIterator25forbidSurrogateCodePointsEv, @function
_ZNK6icu_6721UTF8CollationIterator25forbidSurrogateCodePointsEv:
.LFB3265:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3265:
	.size	_ZNK6icu_6721UTF8CollationIterator25forbidSurrogateCodePointsEv, .-_ZNK6icu_6721UTF8CollationIterator25forbidSurrogateCodePointsEv
	.section	.rodata
.LC1:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\036\017\017\017"
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	" 000000000000\02000"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721UTF8CollationIterator13nextCodePointER10UErrorCode
	.type	_ZN6icu_6721UTF8CollationIterator13nextCodePointER10UErrorCode, @function
_ZN6icu_6721UTF8CollationIterator13nextCodePointER10UErrorCode:
.LFB3266:
	.cfi_startproc
	endbr64
	movl	400(%rdi), %edx
	movl	404(%rdi), %r8d
	cmpl	%r8d, %edx
	je	.L13
	movq	392(%rdi), %r9
	movslq	%edx, %rax
	addq	%r9, %rax
	cmpb	$0, (%rax)
	jne	.L6
	testl	%r8d, %r8d
	js	.L23
.L6:
	leal	1(%rdx), %esi
	movl	%esi, 400(%rdi)
	movzbl	(%rax), %eax
	movl	%eax, %ecx
	testb	%al, %al
	js	.L24
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	cmpl	%esi, %r8d
	je	.L10
	cmpl	$223, %eax
	jg	.L25
	cmpl	$193, %eax
	jle	.L10
	andl	$31, %eax
.L12:
	movslq	%esi, %rdx
	movzbl	(%r9,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L10
	sall	$6, %eax
	addl	$1, %esi
	movl	%eax, %ecx
	movzbl	%dl, %eax
	movl	%esi, 400(%rdi)
	orl	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	subl	$240, %eax
	cmpl	$4, %eax
	jg	.L10
	movslq	%esi, %rsi
	movzbl	(%r9,%rsi), %r10d
	leaq	.LC1(%rip), %rsi
	movq	%r10, %rcx
	shrq	$4, %rcx
	andl	$15, %ecx
	movsbl	(%rsi,%rcx), %ecx
	btl	%eax, %ecx
	jc	.L26
.L10:
	movl	$65533, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	cmpl	$239, %eax
	jg	.L9
	movslq	%esi, %rdx
	andl	$15, %ecx
	leaq	.LC0(%rip), %r10
	andl	$15, %eax
	movzbl	(%r9,%rdx), %edx
	movsbl	(%r10,%rcx), %r10d
	movl	%edx, %ecx
	andl	$63, %edx
	shrb	$5, %cl
	btl	%ecx, %r10d
	jnc	.L10
.L11:
	sall	$6, %eax
	movzbl	%dl, %ecx
	addl	$1, %esi
	movl	%esi, 400(%rdi)
	orl	%ecx, %eax
	cmpl	%esi, %r8d
	jne	.L12
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L26:
	leal	2(%rdx), %esi
	movl	%esi, 400(%rdi)
	cmpl	%esi, %r8d
	je	.L10
	movslq	%esi, %rdx
	movzbl	(%r9,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L10
	sall	$6, %eax
	andl	$63, %r10d
	orl	%r10d, %eax
	jmp	.L11
.L23:
	movl	%edx, 404(%rdi)
	movl	$-1, %eax
	ret
.L13:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3266:
	.size	_ZN6icu_6721UTF8CollationIterator13nextCodePointER10UErrorCode, .-_ZN6icu_6721UTF8CollationIterator13nextCodePointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721UTF8CollationIterator20forwardNumCodePointsEiR10UErrorCode
	.type	_ZN6icu_6721UTF8CollationIterator20forwardNumCodePointsEiR10UErrorCode, @function
_ZN6icu_6721UTF8CollationIterator20forwardNumCodePointsEiR10UErrorCode:
.LFB3268:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jle	.L56
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	404(%rdi), %r8d
	leaq	.LC0(%rip), %r11
	movl	400(%rdi), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	.LC1(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L35:
	cmpl	%edx, %r8d
	jle	.L29
	movq	392(%rdi), %r9
	movslq	%edx, %rax
	addq	%r9, %rax
.L30:
	leal	1(%rdx), %ecx
	movl	%ecx, 400(%rdi)
	movzbl	(%rax), %eax
	leal	62(%rax), %r10d
	cmpb	$50, %r10b
	ja	.L41
	cmpl	%ecx, %r8d
	je	.L41
	movslq	%ecx, %r10
	leal	32(%rax), %r12d
	movzbl	(%r9,%r10), %r10d
	cmpb	$15, %r12b
	ja	.L33
	andl	$15, %eax
	shrb	$5, %r10b
	movsbl	(%r11,%rax), %eax
	btl	%r10d, %eax
	jnc	.L41
	leal	2(%rdx), %eax
	movl	%eax, 400(%rdi)
	cmpl	%eax, %r8d
	je	.L44
	movslq	%eax, %rcx
	cmpb	$-64, (%r9,%rcx)
	jl	.L60
.L45:
	movl	%eax, %edx
	.p2align 4,,10
	.p2align 3
.L32:
	subl	$1, %esi
	jne	.L35
.L27:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	%ecx, %edx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L29:
	testl	%r8d, %r8d
	jns	.L27
	movq	392(%rdi), %r9
	movslq	%edx, %rax
	addq	%r9, %rax
	cmpb	$0, (%rax)
	jne	.L30
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	cmpb	$-33, %al
	ja	.L34
	cmpb	$-64, %r10b
	jge	.L41
	addl	$2, %edx
	movl	%edx, 400(%rdi)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L34:
	shrq	$4, %r10
	andl	$7, %eax
	andl	$15, %r10d
	movsbl	(%rbx,%r10), %r10d
	btl	%eax, %r10d
	jnc	.L41
	leal	2(%rdx), %eax
	movl	%eax, 400(%rdi)
	cmpl	%eax, %r8d
	je	.L44
	movslq	%eax, %rcx
	cmpb	$-64, (%r9,%rcx)
	jge	.L45
	leal	3(%rdx), %eax
	movl	%eax, 400(%rdi)
	cmpl	%eax, %r8d
	je	.L44
	movslq	%eax, %rcx
	cmpb	$-64, (%r9,%rcx)
	jge	.L45
	addl	$4, %edx
	movl	%edx, 400(%rdi)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L44:
	movl	%r8d, %edx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L60:
	addl	$3, %edx
	movl	%edx, 400(%rdi)
	jmp	.L32
.L56:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE3268:
	.size	_ZN6icu_6721UTF8CollationIterator20forwardNumCodePointsEiR10UErrorCode, .-_ZN6icu_6721UTF8CollationIterator20forwardNumCodePointsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721UTF8CollationIteratorD2Ev
	.type	_ZN6icu_6721UTF8CollationIteratorD2Ev, @function
_ZN6icu_6721UTF8CollationIteratorD2Ev:
.LFB3258:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6721UTF8CollationIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CollationIteratorD2Ev@PLT
	.cfi_endproc
.LFE3258:
	.size	_ZN6icu_6721UTF8CollationIteratorD2Ev, .-_ZN6icu_6721UTF8CollationIteratorD2Ev
	.globl	_ZN6icu_6721UTF8CollationIteratorD1Ev
	.set	_ZN6icu_6721UTF8CollationIteratorD1Ev,_ZN6icu_6721UTF8CollationIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721UTF8CollationIteratorD0Ev
	.type	_ZN6icu_6721UTF8CollationIteratorD0Ev, @function
_ZN6icu_6721UTF8CollationIteratorD0Ev:
.LFB3260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721UTF8CollationIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CollationIteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3260:
	.size	_ZN6icu_6721UTF8CollationIteratorD0Ev, .-_ZN6icu_6721UTF8CollationIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721UTF8CollationIterator13resetToOffsetEi
	.type	_ZN6icu_6721UTF8CollationIterator13resetToOffsetEi, @function
_ZN6icu_6721UTF8CollationIterator13resetToOffsetEi:
.LFB3261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6717CollationIterator5resetEv@PLT
	movl	%r12d, 400(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3261:
	.size	_ZN6icu_6721UTF8CollationIterator13resetToOffsetEi, .-_ZN6icu_6721UTF8CollationIterator13resetToOffsetEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIterator13resetToOffsetEi
	.type	_ZN6icu_6724FCDUTF8CollationIterator13resetToOffsetEi, @function
_ZN6icu_6724FCDUTF8CollationIterator13resetToOffsetEi:
.LFB3274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6717CollationIterator5resetEv@PLT
	movl	%r12d, 400(%rbx)
	movl	%r12d, 412(%rbx)
	movl	$0, 408(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3274:
	.size	_ZN6icu_6724FCDUTF8CollationIterator13resetToOffsetEi, .-_ZN6icu_6724FCDUTF8CollationIterator13resetToOffsetEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721UTF8CollationIterator14handleNextCE32ERiR10UErrorCode
	.type	_ZN6icu_6721UTF8CollationIterator14handleNextCE32ERiR10UErrorCode, @function
_ZN6icu_6721UTF8CollationIterator14handleNextCE32ERiR10UErrorCode:
.LFB3263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movslq	400(%rdi), %rax
	cmpl	404(%rdi), %eax
	je	.L88
	movq	%rdi, %rbx
	movq	392(%rdi), %rdi
	leal	1(%rax), %edx
	movl	%edx, 400(%rbx)
	movzbl	(%rdi,%rax), %ecx
	movl	%ecx, (%rsi)
	movq	%rcx, %rax
	testb	%cl, %cl
	jns	.L89
	leal	-224(%rcx), %esi
	movl	404(%rbx), %edx
	cmpl	$15, %esi
	jbe	.L90
	leal	-194(%rcx), %eax
	cmpl	$29, %eax
	ja	.L73
	movslq	400(%rbx), %rax
	cmpl	%edx, %eax
	je	.L73
	movzbl	(%rdi,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L73
	movq	8(%rbx), %rsi
	movzbl	%al, %edx
	leal	1888(%rcx), %eax
	sall	$6, %ecx
	cltq
	andl	$1984, %ecx
	movq	(%rsi), %rdi
	movq	16(%rsi), %rsi
	movzwl	(%rdi,%rax,2), %eax
	addl	%edx, %eax
	orl	%ecx, %edx
	cltq
	movl	(%rsi,%rax,4), %eax
	movl	%edx, (%r12)
	addl	$1, 400(%rbx)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L90:
	movslq	400(%rbx), %rsi
	leal	1(%rsi), %r8d
	cmpl	%edx, %r8d
	jl	.L82
	testl	%edx, %edx
	jns	.L73
.L82:
	movzbl	(%rdi,%rsi), %r8d
	andl	$15, %eax
	leaq	.LC0(%rip), %r9
	movsbl	(%r9,%rax), %r9d
	movl	%r8d, %eax
	sarl	$5, %eax
	btl	%eax, %r9d
	jnc	.L73
	movzbl	1(%rdi,%rsi), %esi
	addl	$-128, %esi
	cmpb	$63, %sil
	ja	.L73
	movl	%r8d, %eax
	sall	$12, %ecx
	movzbl	%sil, %esi
	sall	$6, %eax
	movzwl	%cx, %ecx
	andl	$4032, %eax
	orl	%eax, %ecx
	orl	%esi, %ecx
	movl	%ecx, (%r12)
	movq	8(%rbx), %rcx
	addl	$2, 400(%rbx)
	movl	(%r12), %eax
	movq	(%rcx), %rsi
	popq	%rbx
	movl	%eax, %edx
	andl	$31, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %eax
	movq	16(%rcx), %rdx
	cltq
	movl	(%rdx,%rax,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	leaq	400(%rbx), %rsi
	movl	$-3, %r8d
	call	utf8_nextCharSafeBody_67@PLT
	movq	16(%rbx), %rdx
	movl	%eax, (%r12)
	movq	(%rdx), %rcx
	movq	16(%rcx), %rsi
	cmpl	$55295, %eax
	jbe	.L91
	cmpl	$65535, %eax
	ja	.L77
	cmpl	$56320, %eax
	movl	$320, %edx
	movq	(%rcx), %rdi
	movl	$0, %ecx
	cmovl	%edx, %ecx
	movl	%eax, %edx
	sarl	$5, %edx
.L87:
	addl	%ecx, %edx
	andl	$31, %eax
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L76:
	popq	%rbx
	movl	(%rsi,%rdx), %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	movq	16(%rdx), %rdx
	movl	(%rdx,%rcx,4), %eax
.L68:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movl	%eax, %edx
	movq	(%rcx), %rcx
	andl	$31, %eax
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L88:
	movl	$-1, (%rsi)
	movl	$192, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L76
	cmpl	44(%rcx), %eax
	jl	.L79
	movslq	48(%rcx), %rdx
	salq	$2, %rdx
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L79:
	movl	%eax, %edx
	movq	(%rcx), %rdi
	sarl	$11, %edx
	addl	$2080, %edx
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %ecx
	movl	%eax, %edx
	sarl	$5, %edx
	andl	$63, %edx
	jmp	.L87
	.cfi_endproc
.LFE3263:
	.size	_ZN6icu_6721UTF8CollationIterator14handleNextCE32ERiR10UErrorCode, .-_ZN6icu_6721UTF8CollationIterator14handleNextCE32ERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721UTF8CollationIterator17previousCodePointER10UErrorCode
	.type	_ZN6icu_6721UTF8CollationIterator17previousCodePointER10UErrorCode, @function
_ZN6icu_6721UTF8CollationIterator17previousCodePointER10UErrorCode:
.LFB3267:
	.cfi_startproc
	endbr64
	movl	400(%rdi), %eax
	testl	%eax, %eax
	je	.L94
	subl	$1, %eax
	movq	392(%rdi), %r9
	movl	%eax, 400(%rdi)
	cltq
	movzbl	(%r9,%rax), %r10d
	testb	%r10b, %r10b
	js	.L95
.L92:
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	400(%rdi), %rdx
	movl	$-3, %r8d
	movl	%r10d, %ecx
	xorl	%esi, %esi
	movq	%r9, %rdi
	jmp	utf8_prevCharSafeBody_67@PLT
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$-1, %r10d
	jmp	.L92
	.cfi_endproc
.LFE3267:
	.size	_ZN6icu_6721UTF8CollationIterator17previousCodePointER10UErrorCode, .-_ZN6icu_6721UTF8CollationIterator17previousCodePointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721UTF8CollationIterator21backwardNumCodePointsEiR10UErrorCode
	.type	_ZN6icu_6721UTF8CollationIterator21backwardNumCodePointsEiR10UErrorCode, @function
_ZN6icu_6721UTF8CollationIterator21backwardNumCodePointsEiR10UErrorCode:
.LFB3269:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jle	.L105
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	movl	400(%rdi), %edx
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L99:
	subl	$1, %ebx
	je	.L96
.L100:
	testl	%edx, %edx
	jle	.L96
	subl	$1, %edx
	movq	392(%r12), %rdi
	movslq	%edx, %rax
	movl	%edx, 400(%r12)
	cmpb	$-64, (%rdi,%rax)
	jge	.L99
	xorl	%esi, %esi
	call	utf8_back1SafeBody_67@PLT
	movl	%eax, 400(%r12)
	movl	%eax, %edx
	subl	$1, %ebx
	jne	.L100
.L96:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L105:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE3269:
	.size	_ZN6icu_6721UTF8CollationIterator21backwardNumCodePointsEiR10UErrorCode, .-_ZN6icu_6721UTF8CollationIterator21backwardNumCodePointsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIteratorD2Ev
	.type	_ZN6icu_6724FCDUTF8CollationIteratorD2Ev, @function
_ZN6icu_6724FCDUTF8CollationIteratorD2Ev:
.LFB3271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724FCDUTF8CollationIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	432(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -432(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6721UTF8CollationIteratorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717CollationIteratorD2Ev@PLT
	.cfi_endproc
.LFE3271:
	.size	_ZN6icu_6724FCDUTF8CollationIteratorD2Ev, .-_ZN6icu_6724FCDUTF8CollationIteratorD2Ev
	.globl	_ZN6icu_6724FCDUTF8CollationIteratorD1Ev
	.set	_ZN6icu_6724FCDUTF8CollationIteratorD1Ev,_ZN6icu_6724FCDUTF8CollationIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIteratorD0Ev
	.type	_ZN6icu_6724FCDUTF8CollationIteratorD0Ev, @function
_ZN6icu_6724FCDUTF8CollationIteratorD0Ev:
.LFB3273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724FCDUTF8CollationIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	432(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -432(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6721UTF8CollationIteratorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6717CollationIteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3273:
	.size	_ZN6icu_6724FCDUTF8CollationIteratorD0Ev, .-_ZN6icu_6724FCDUTF8CollationIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721UTF8CollationIterator18foundNULTerminatorEv
	.type	_ZN6icu_6721UTF8CollationIterator18foundNULTerminatorEv, @function
_ZN6icu_6721UTF8CollationIterator18foundNULTerminatorEv:
.LFB3264:
	.cfi_startproc
	endbr64
	movl	404(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L115
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	movl	400(%rdi), %eax
	subl	$1, %eax
	movl	%eax, 400(%rdi)
	movl	%eax, 404(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3264:
	.size	_ZN6icu_6721UTF8CollationIterator18foundNULTerminatorEv, .-_ZN6icu_6721UTF8CollationIterator18foundNULTerminatorEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724FCDUTF8CollationIterator9getOffsetEv
	.type	_ZNK6icu_6724FCDUTF8CollationIterator9getOffsetEv, @function
_ZNK6icu_6724FCDUTF8CollationIterator9getOffsetEv:
.LFB3275:
	.cfi_startproc
	endbr64
	cmpl	$3, 408(%rdi)
	movl	400(%rdi), %eax
	jne	.L116
	testl	%eax, %eax
	jne	.L118
	movl	412(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	movl	416(%rdi), %eax
.L116:
	ret
	.cfi_endproc
.LFE3275:
	.size	_ZNK6icu_6724FCDUTF8CollationIterator9getOffsetEv, .-_ZNK6icu_6724FCDUTF8CollationIterator9getOffsetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIterator18foundNULTerminatorEv
	.type	_ZN6icu_6724FCDUTF8CollationIterator18foundNULTerminatorEv, @function
_ZN6icu_6724FCDUTF8CollationIterator18foundNULTerminatorEv:
.LFB3280:
	.cfi_startproc
	endbr64
	movl	408(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L119
	movl	404(%rdi), %edx
	testl	%edx, %edx
	js	.L123
.L119:
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	movl	400(%rdi), %eax
	subl	$1, %eax
	movl	%eax, 400(%rdi)
	movl	%eax, 404(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3280:
	.size	_ZN6icu_6724FCDUTF8CollationIterator18foundNULTerminatorEv, .-_ZN6icu_6724FCDUTF8CollationIterator18foundNULTerminatorEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIterator23handleGetTrailSurrogateEv
	.type	_ZN6icu_6724FCDUTF8CollationIterator23handleGetTrailSurrogateEv, @function
_ZN6icu_6724FCDUTF8CollationIterator23handleGetTrailSurrogateEv:
.LFB3279:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$3, 408(%rdi)
	jne	.L124
	movzwl	440(%rdi), %ecx
	movl	400(%rdi), %edx
	testw	%cx, %cx
	js	.L126
	movswl	%cx, %esi
	sarl	$5, %esi
.L127:
	movl	$-1, %eax
	cmpl	%edx, %esi
	jbe	.L124
	andl	$2, %ecx
	leaq	442(%rdi), %rcx
	jne	.L129
	movq	456(%rdi), %rcx
.L129:
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %eax
	movl	%eax, %ecx
	andl	$64512, %ecx
	cmpl	$56320, %ecx
	je	.L132
.L124:
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	movl	444(%rdi), %esi
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L132:
	addl	$1, %edx
	movl	%edx, 400(%rdi)
	ret
	.cfi_endproc
.LFE3279:
	.size	_ZN6icu_6724FCDUTF8CollationIterator23handleGetTrailSurrogateEv, .-_ZN6icu_6724FCDUTF8CollationIterator23handleGetTrailSurrogateEv
	.align 2
	.p2align 4
	.type	_ZN6icu_6724FCDUTF8CollationIterator11nextSegmentER10UErrorCode.part.0, @function
_ZN6icu_6724FCDUTF8CollationIterator11nextSegmentER10UErrorCode.part.0:
.LFB4319:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%rsi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	400(%rdi), %eax
	movq	%rdx, -128(%rbp)
	movl	$2, %edx
	movw	%dx, -120(%rbp)
	movl	%eax, -144(%rbp)
	movl	%eax, %r13d
	movq	%r14, -136(%rbp)
	movl	%r15d, %r14d
	.p2align 4,,10
	.p2align 3
.L160:
	movq	392(%rbx), %r10
	movq	424(%rbx), %rdi
	leal	1(%r13), %ecx
	movslq	%r13d, %rax
	movl	%ecx, 400(%rbx)
	movzwl	8(%rdi), %r8d
	movzbl	(%r10,%rax), %r12d
	movl	%r8d, %r9d
	movl	%r12d, %esi
	testb	%r12b, %r12b
	js	.L235
	cmpl	%r8d, %r12d
	jge	.L236
.L223:
	movq	-136(%rbp), %r14
.L151:
	cmpl	%r13d, -144(%rbp)
	jne	.L154
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	400(%rbx), %r13d
.L154:
	movl	-144(%rbp), %eax
	movl	%r13d, 416(%rbx)
	movl	$1, %r12d
	movl	$2, 408(%rbx)
	movl	%eax, 400(%rbx)
.L181:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L237
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	movl	404(%rbx), %r11d
	cmpl	%r11d, %ecx
	je	.L228
	cmpl	$223, %r12d
	jle	.L138
	cmpl	$239, %r12d
	jg	.L139
	movslq	%ecx, %rax
	andl	$15, %esi
	leaq	.LC0(%rip), %rdx
	movl	%r12d, %r15d
	movzbl	(%r10,%rax), %eax
	movsbl	(%rdx,%rsi), %r12d
	andl	$15, %r15d
	movl	%eax, %esi
	shrb	$5, %sil
	btl	%esi, %r12d
	jc	.L140
	.p2align 4,,10
	.p2align 3
.L228:
	cmpw	$-3, %r9w
	ja	.L238
	movl	$255, %ecx
	movl	$65533, %r12d
.L137:
	movq	56(%rdi), %rax
	movzbl	(%rax,%rcx), %eax
	testb	%al, %al
	je	.L223
	movl	%r12d, %ecx
	shrb	$5, %cl
	btl	%ecx, %eax
	jnc	.L223
.L152:
	movl	%r12d, %esi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	movl	%eax, %r15d
	movzbl	%ah, %ecx
	shrw	$8, %ax
	jne	.L153
	cmpl	%r13d, -144(%rbp)
	jne	.L217
.L153:
	movq	-136(%rbp), %rdi
	movl	%r12d, %esi
	movl	%ecx, -140(%rbp)
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	-140(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L155
	cmpb	%r14b, %cl
	jb	.L159
	leal	32382(%r15), %eax
	testw	$-3, %ax
	je	.L159
.L155:
	movl	400(%rbx), %r13d
	movl	%r15d, %r14d
	cmpl	%r13d, 404(%rbx)
	je	.L217
	testb	%r15b, %r15b
	jne	.L160
.L217:
	movq	-136(%rbp), %r14
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L236:
	xorl	%ecx, %ecx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L138:
	cmpl	$193, %r12d
	jle	.L228
	andl	$31, %esi
.L148:
	movslq	%ecx, %rax
	movzbl	(%r10,%rax), %r12d
	addl	$-128, %r12d
	cmpb	$63, %r12b
	ja	.L228
	sall	$6, %esi
	movzbl	%r12b, %r12d
	addl	$1, %ecx
	orl	%esi, %r12d
	movl	%ecx, 400(%rbx)
	cmpl	%r8d, %r12d
	jl	.L223
	cmpl	$65535, %r12d
	jg	.L152
	movl	%r12d, %ecx
	sarl	$8, %ecx
	movslq	%ecx, %rcx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L238:
	movq	-136(%rbp), %r14
	movl	$65533, %r12d
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L139:
	subl	$240, %r12d
	cmpl	$4, %r12d
	jg	.L228
	movslq	%ecx, %rcx
	leaq	.LC1(%rip), %rdx
	movzbl	(%r10,%rcx), %r15d
	movq	%r15, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movsbl	(%rdx,%rax), %eax
	btl	%r12d, %eax
	jnc	.L228
	leal	2(%r13), %ecx
	movl	%ecx, 400(%rbx)
	cmpl	%ecx, %r11d
	je	.L228
	movslq	%ecx, %rax
	movzbl	(%r10,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L228
	sall	$6, %r12d
	andl	$63, %r15d
	orl	%r12d, %r15d
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L140:
	andl	$63, %eax
.L142:
	addl	$1, %ecx
	movl	%ecx, 400(%rbx)
	cmpl	%ecx, %r11d
	je	.L228
	sall	$6, %r15d
	movzbl	%al, %esi
	orl	%r15d, %esi
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L159:
	movl	400(%rbx), %r13d
	movl	404(%rbx), %esi
	leaq	.LC0(%rip), %r15
	movq	-136(%rbp), %r14
	movq	424(%rbx), %rdi
	cmpl	%r13d, %esi
	je	.L157
.L158:
	movq	392(%rbx), %r9
	movslq	%r13d, %rax
	leal	1(%r13), %edx
	movzwl	8(%rdi), %ecx
	movl	%edx, 400(%rbx)
	movzbl	(%r9,%rax), %r12d
	movl	%ecx, %r8d
	movl	%r12d, %eax
	testb	%r12b, %r12b
	js	.L239
	cmpl	%ecx, %r12d
	jge	.L240
.L163:
	movl	%r13d, 400(%rbx)
.L157:
	movq	-152(%rbp), %r15
	leaq	432(%rbx), %rdx
	movq	%r14, %rsi
	xorl	%r12d, %r12d
	movq	%r15, %rcx
	call	_ZNK6icu_6715Normalizer2Impl9decomposeERKNS_13UnicodeStringERS1_R10UErrorCode@PLT
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L181
	movl	-144(%rbp), %eax
	movl	$1, %r12d
	movl	$3, 408(%rbx)
	movl	%eax, 412(%rbx)
	movl	400(%rbx), %eax
	movl	$0, 400(%rbx)
	movl	%eax, 416(%rbx)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L239:
	cmpl	%esi, %edx
	je	.L225
	cmpl	$223, %r12d
	jle	.L165
	cmpl	$239, %r12d
	jle	.L241
	subl	$240, %r12d
	cmpl	$4, %r12d
	jg	.L225
	movslq	%edx, %rdx
	movzbl	(%r9,%rdx), %r10d
	leaq	.LC1(%rip), %rdx
	movq	%r10, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movsbl	(%rdx,%rax), %eax
	btl	%r12d, %eax
	jnc	.L225
	leal	2(%r13), %edx
	movl	%edx, 400(%rbx)
	cmpl	%esi, %edx
	je	.L225
	movslq	%edx, %rax
	movzbl	(%r9,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L225
	sall	$6, %r12d
	andl	$63, %r10d
	orl	%r12d, %r10d
.L169:
	addl	$1, %edx
	movl	%edx, 400(%rbx)
	cmpl	%esi, %edx
	je	.L225
	sall	$6, %r10d
	movzbl	%al, %eax
	orl	%r10d, %eax
	jmp	.L175
.L165:
	cmpl	$193, %r12d
	jg	.L176
	.p2align 4,,10
	.p2align 3
.L225:
	cmpw	$-3, %r8w
	ja	.L163
	movl	$255, %eax
	movl	$65533, %r12d
.L164:
	movq	56(%rdi), %rdx
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L163
	movl	%r12d, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L163
.L178:
	movl	%r12d, %esi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	cmpw	$255, %ax
	jbe	.L242
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	400(%rbx), %r13d
	movl	404(%rbx), %esi
	movq	424(%rbx), %rdi
	cmpl	%esi, %r13d
	jne	.L158
	jmp	.L157
.L240:
	xorl	%eax, %eax
	jmp	.L164
.L176:
	andl	$31, %eax
.L175:
	movslq	%edx, %rsi
	movzbl	(%r9,%rsi), %esi
	addl	$-128, %esi
	cmpb	$63, %sil
	ja	.L225
	sall	$6, %eax
	movzbl	%sil, %r12d
	addl	$1, %edx
	orl	%eax, %r12d
	movl	%edx, 400(%rbx)
	cmpl	%ecx, %r12d
	jl	.L163
	cmpl	$65535, %r12d
	jg	.L178
	movl	%r12d, %eax
	sarl	$8, %eax
	cltq
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L241:
	movslq	%edx, %r11
	andl	$15, %eax
	movl	%r12d, %r10d
	movzbl	(%r9,%r11), %r11d
	movsbl	(%r15,%rax), %r12d
	andl	$15, %r10d
	movl	%r11d, %eax
	shrb	$5, %al
	btl	%eax, %r12d
	jnc	.L225
	movl	%r11d, %eax
	andl	$63, %eax
	jmp	.L169
.L242:
	movq	424(%rbx), %rdi
	jmp	.L163
.L237:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4319:
	.size	_ZN6icu_6724FCDUTF8CollationIterator11nextSegmentER10UErrorCode.part.0, .-_ZN6icu_6724FCDUTF8CollationIterator11nextSegmentER10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6724FCDUTF8CollationIterator15previousSegmentER10UErrorCode.part.0, @function
_ZN6icu_6724FCDUTF8CollationIterator15previousSegmentER10UErrorCode.part.0:
.LFB4320:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	subq	$120, %rsp
	movq	%rsi, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	400(%rdi), %eax
	movq	%rbx, -128(%rbp)
	xorl	%ebx, %ebx
	movw	%dx, -120(%rbp)
	movl	%eax, -144(%rbp)
	movl	%eax, %r14d
	leaq	400(%rdi), %rax
	movq	%rax, -152(%rbp)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L268:
	movl	%r15d, %eax
	movq	56(%rdi), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L245
	movl	%r15d, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L245
.L246:
	movl	%r15d, %esi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	movl	%eax, %edx
	movl	%eax, %ecx
	testb	%al, %al
	jne	.L247
	cmpl	%r14d, -144(%rbp)
	jne	.L248
.L247:
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	%edx, -140(%rbp)
	movb	%cl, -136(%rbp)
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movzbl	-136(%rbp), %ecx
	movl	-140(%rbp), %edx
	testb	%cl, %cl
	je	.L249
	testb	%bl, %bl
	je	.L250
	cmpb	%bl, %cl
	ja	.L253
.L250:
	leal	32382(%rdx), %eax
	testw	$-3, %ax
	je	.L253
.L249:
	movl	400(%r13), %r14d
	movzbl	%dh, %edx
	movl	%edx, %ebx
	testl	%r14d, %r14d
	je	.L248
	testl	%edx, %edx
	je	.L248
.L254:
	leal	-1(%r14), %eax
	movq	392(%r13), %rdi
	movl	%eax, 400(%r13)
	cltq
	movzbl	(%rdi,%rax), %r15d
	testb	%r15b, %r15b
	js	.L311
	movq	424(%r13), %rdi
	movzwl	8(%rdi), %eax
	cmpl	%eax, %r15d
	jge	.L268
.L245:
	cmpl	%r14d, -144(%rbp)
	jne	.L248
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	400(%r13), %r14d
.L248:
	movl	-144(%rbp), %eax
	movl	%r14d, 412(%r13)
	movl	$1, %r14d
	movl	$2, 408(%r13)
	movl	%eax, 400(%r13)
.L262:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L312
	addq	$120, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movq	-152(%rbp), %rdx
	movl	%r15d, %ecx
	movl	$-3, %r8d
	xorl	%esi, %esi
	call	utf8_prevCharSafeBody_67@PLT
	movq	424(%r13), %rdi
	movl	%eax, %r15d
	movzwl	8(%rdi), %eax
	cmpl	%eax, %r15d
	jl	.L245
	cmpl	$65535, %r15d
	jg	.L246
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L253:
	cmpw	$255, %dx
	jbe	.L252
	leaq	400(%r13), %rax
	movq	%rax, -136(%rbp)
.L251:
	movl	400(%r13), %r14d
	testl	%r14d, %r14d
	je	.L252
	leal	-1(%r14), %eax
	movq	392(%r13), %rdi
	movl	%eax, 400(%r13)
	cltq
	movzbl	(%rdi,%rax), %r15d
	testb	%r15b, %r15b
	js	.L313
	movq	424(%r13), %rdi
	movzwl	8(%rdi), %eax
	cmpl	%eax, %r15d
	jl	.L259
.L265:
	movl	%r15d, %eax
	movq	56(%rdi), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L259
	movl	%r15d, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L259
.L258:
	movl	%r15d, %esi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	movl	%eax, %ebx
	testw	%ax, %ax
	je	.L259
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpw	$255, %bx
	ja	.L251
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L259:
	movl	%r14d, 400(%r13)
.L252:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L260
	movswl	%ax, %edx
	sarl	$5, %edx
.L261:
	xorl	%esi, %esi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6713UnicodeString9doReverseEii@PLT
	movq	-160(%rbp), %rbx
	movq	424(%r13), %rdi
	movq	%r12, %rsi
	leaq	432(%r13), %rdx
	movq	%rbx, %rcx
	call	_ZNK6icu_6715Normalizer2Impl9decomposeERKNS_13UnicodeStringERS1_R10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L262
	movl	-144(%rbp), %eax
	movl	$3, 408(%r13)
	movl	%eax, 416(%r13)
	movl	400(%r13), %eax
	movl	%eax, 412(%r13)
	movswl	440(%r13), %eax
	testw	%ax, %ax
	js	.L263
	sarl	$5, %eax
.L264:
	movl	%eax, 400(%r13)
	movl	$1, %r14d
	jmp	.L262
.L260:
	movl	-116(%rbp), %edx
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L313:
	movq	-136(%rbp), %rdx
	movl	%r15d, %ecx
	movl	$-3, %r8d
	xorl	%esi, %esi
	call	utf8_prevCharSafeBody_67@PLT
	movq	424(%r13), %rdi
	movl	%eax, %r15d
	movzwl	8(%rdi), %eax
	cmpl	%eax, %r15d
	jl	.L259
	cmpl	$65535, %r15d
	jg	.L258
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L263:
	movl	444(%r13), %eax
	jmp	.L264
.L312:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4320:
	.size	_ZN6icu_6724FCDUTF8CollationIterator15previousSegmentER10UErrorCode.part.0, .-_ZN6icu_6724FCDUTF8CollationIterator15previousSegmentER10UErrorCode.part.0
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3526:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3526:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3529:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L327
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L315
	cmpb	$0, 12(%rbx)
	jne	.L328
.L319:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L315:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L319
	.cfi_endproc
.LFE3529:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3532:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L331
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3532:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3535:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L334
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3535:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L340
.L336:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L341
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L341:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3537:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3538:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3538:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3539:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3539:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3540:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3540:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3541:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3541:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3542:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3542:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3543:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L357
	testl	%edx, %edx
	jle	.L357
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L360
.L349:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L349
	.cfi_endproc
.LFE3543:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L364
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L364
	testl	%r12d, %r12d
	jg	.L371
	cmpb	$0, 12(%rbx)
	jne	.L372
.L366:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L366
.L372:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L364:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3544:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L374
	movq	(%rdi), %r8
.L375:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L378
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L378
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L378:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3545:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3546:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L385
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3546:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3547:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3547:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3548:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3548:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3549:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3549:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3551:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3551:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3553:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3553:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724FCDUTF8CollationIterator11nextHasLcccEv
	.type	_ZNK6icu_6724FCDUTF8CollationIterator11nextHasLcccEv, @function
_ZNK6icu_6724FCDUTF8CollationIterator11nextHasLcccEv:
.LFB3277:
	.cfi_startproc
	endbr64
	movslq	400(%rdi), %rax
	movq	392(%rdi), %r8
	movzbl	(%r8,%rax), %edx
	movq	%rax, %rsi
	xorl	%eax, %eax
	movl	%edx, %ecx
	cmpl	$203, %edx
	jle	.L391
	leal	-228(%rdx), %r9d
	cmpl	$9, %r9d
	ja	.L413
	cmpl	$234, %edx
	jne	.L391
.L413:
	movl	404(%rdi), %edi
	leal	1(%rsi), %eax
	cmpl	%eax, %edi
	je	.L410
	cmpl	$223, %edx
	jle	.L395
	cmpl	$239, %edx
	jg	.L396
	movslq	%eax, %rsi
	movl	%ecx, %edx
	leaq	.LC0(%rip), %r9
	andl	$15, %ecx
	movzbl	(%r8,%rsi), %esi
	movsbl	(%r9,%rcx), %r9d
	andl	$15, %edx
	movl	%esi, %ecx
	andl	$63, %esi
	shrb	$5, %cl
	btl	%ecx, %r9d
	jnc	.L410
.L397:
	sall	$6, %edx
	movzbl	%sil, %ecx
	addl	$1, %eax
	orl	%ecx, %edx
	cmpl	%eax, %edi
	je	.L410
	cltq
	movzbl	(%r8,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L410
	movl	%edx, %ecx
	movzbl	%al, %eax
	sall	$6, %ecx
	orl	%eax, %ecx
	cmpl	$65535, %ecx
	jle	.L400
	sarl	$10, %ecx
	subw	$10304, %cx
	movzwl	%cx, %ecx
	movl	%ecx, %eax
	sarl	$5, %eax
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L395:
	cltq
	movzbl	(%r8,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	jbe	.L399
.L410:
	movl	$2047, %eax
	movl	$65533, %ecx
.L394:
	cltq
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %rdx
	movzbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L418
.L391:
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %rax
	movl	(%rax,%rdx,4), %eax
	shrl	%cl, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	subl	$240, %edx
	cmpl	$4, %edx
	jg	.L410
	cltq
	leaq	.LC1(%rip), %r9
	movzbl	(%r8,%rax), %ecx
	movq	%rcx, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movsbl	(%r9,%rax), %eax
	btl	%edx, %eax
	jnc	.L410
	leal	2(%rsi), %eax
	cmpl	%eax, %edi
	je	.L410
	movslq	%eax, %rsi
	movzbl	(%r8,%rsi), %esi
	addl	$-128, %esi
	cmpb	$63, %sil
	ja	.L410
	sall	$6, %edx
	andl	$63, %ecx
	orl	%ecx, %edx
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L399:
	andl	$31, %ecx
	movzbl	%al, %eax
	sall	$6, %ecx
	orl	%eax, %ecx
.L400:
	cmpl	$767, %ecx
	jg	.L419
	xorl	%eax, %eax
	ret
.L419:
	movl	%ecx, %eax
	sarl	$5, %eax
	jmp	.L394
	.cfi_endproc
.LFE3277:
	.size	_ZNK6icu_6724FCDUTF8CollationIterator11nextHasLcccEv, .-_ZNK6icu_6724FCDUTF8CollationIterator11nextHasLcccEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIterator14handleNextCE32ERiR10UErrorCode
	.type	_ZN6icu_6724FCDUTF8CollationIterator14handleNextCE32ERiR10UErrorCode, @function
_ZN6icu_6724FCDUTF8CollationIterator14handleNextCE32ERiR10UErrorCode:
.LFB3276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %r10
	leaq	.LC0(%rip), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	400(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	_ZN6icu_6712CollationFCD9tcccIndexE(%rip), %rbx
	subq	$24, %rsp
	movl	408(%rdi), %eax
.L421:
	testl	%eax, %eax
	je	.L492
	cmpl	$2, %eax
	je	.L493
	cmpl	$3, %eax
	jne	.L448
	movzwl	440(%r12), %edx
	movslq	400(%r12), %rcx
	testw	%dx, %dx
	js	.L449
	movswl	%dx, %eax
	sarl	$5, %eax
.L450:
	cmpl	%eax, %ecx
	jne	.L494
.L452:
	movslq	416(%r12), %rax
	movl	%eax, 400(%r12)
	movl	%eax, 412(%r12)
.L447:
	movl	$0, 408(%r12)
.L458:
	cmpl	404(%r12), %eax
	je	.L443
	movq	392(%r12), %rdi
	leal	1(%rax), %edx
	movl	%edx, 400(%r12)
	movzbl	(%rdi,%rax), %ecx
	movl	%ecx, 0(%r13)
	movq	%rcx, %rax
	testb	%cl, %cl
	jns	.L495
	leal	-224(%rcx), %edx
	movl	404(%r12), %r11d
	cmpl	$15, %edx
	jbe	.L496
	leal	-194(%rcx), %eax
	cmpl	$29, %eax
	ja	.L427
	movslq	400(%r12), %rax
	cmpl	%r11d, %eax
	je	.L427
	movzbl	(%rdi,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L427
	movq	8(%r12), %rsi
	leal	1888(%rcx), %edx
	movzbl	%al, %eax
	sall	$6, %ecx
	movslq	%edx, %rdx
	andl	$1984, %ecx
	movq	(%rsi), %rdi
	movq	16(%rsi), %rsi
	orl	%eax, %ecx
	movzwl	(%rdi,%rdx,2), %edx
	addl	%eax, %edx
	movslq	%edx, %rdx
	movl	(%rsi,%rdx,4), %r11d
	movl	%ecx, 0(%r13)
	movl	400(%r12), %edx
	leal	1(%rdx), %ecx
	movl	%edx, -52(%rbp)
	movl	%ecx, 400(%r12)
	movl	0(%r13), %eax
	cmpl	$191, %eax
	jle	.L420
	movl	%eax, %esi
	sarl	$5, %esi
	movslq	%esi, %rsi
	movzbl	(%rbx,%rsi), %esi
	testl	%esi, %esi
	je	.L420
	movl	(%r10,%rsi,4), %esi
	btl	%eax, %esi
	jnc	.L420
	cmpl	404(%r12), %ecx
	je	.L420
	movq	%r12, %rdi
	call	_ZNK6icu_6724FCDUTF8CollationIterator11nextHasLcccEv
	testb	%al, %al
	je	.L420
	movl	-52(%rbp), %edx
	subl	$1, %edx
	movl	%edx, 400(%r12)
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L448:
	cmpl	$1, %eax
	jne	.L452
	movslq	400(%r12), %rax
	movl	%eax, 412(%r12)
	cmpl	416(%r12), %eax
	je	.L447
	movl	$2, 408(%r12)
.L459:
	addq	$24, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6721UTF8CollationIterator14handleNextCE32ERiR10UErrorCode
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	movl	400(%r12), %edx
	leal	1(%rdx), %esi
	cmpl	%r11d, %esi
	jl	.L462
	testl	%r11d, %r11d
	jns	.L427
.L462:
	movslq	%edx, %rsi
	andl	$15, %eax
	movzbl	(%rdi,%rsi), %edx
	movsbl	(%r9,%rax), %r8d
	movl	%edx, %eax
	sarl	$5, %eax
	btl	%eax, %r8d
	jc	.L497
.L427:
	movl	%r11d, %edx
	movl	$-3, %r8d
	movq	%r15, %rsi
	call	utf8_nextCharSafeBody_67@PLT
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %r10
	cmpl	$65533, %eax
	movl	%eax, 0(%r13)
	movl	%eax, %r11d
	je	.L498
	movl	%eax, %edx
	sarl	$10, %edx
	leal	-10304(%rdx), %eax
	cmpw	$191, %ax
	jbe	.L438
	salq	$48, %rax
	shrq	$53, %rax
	movzbl	(%rbx,%rax), %eax
	testl	%eax, %eax
	je	.L438
	movl	(%r10,%rax,4), %eax
	btl	%edx, %eax
	jnc	.L438
	movl	400(%r12), %edx
	cmpl	404(%r12), %edx
	movl	%edx, -52(%rbp)
	je	.L438
	movq	%r12, %rdi
	call	_ZNK6icu_6724FCDUTF8CollationIterator11nextHasLcccEv
	movl	-52(%rbp), %edx
	testb	%al, %al
	je	.L438
	subl	$4, %edx
	movl	%edx, 400(%r12)
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L438:
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rcx
	cmpl	44(%rax), %r11d
	jge	.L499
	movq	(%rax), %rsi
	movl	%r11d, %eax
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rsi,%rax,2), %edx
	movl	%r11d, %eax
	andl	$31, %r11d
	sarl	$5, %eax
	andl	$63, %eax
	addl	%edx, %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	leal	(%r11,%rax,4), %eax
	cltq
	salq	$2, %rax
.L441:
	movl	(%rcx,%rax), %r11d
.L420:
	addq	$24, %rsp
	movl	%r11d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	movslq	400(%r12), %rax
	cmpl	416(%r12), %eax
	je	.L447
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L492:
	movslq	400(%r12), %rax
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L497:
	movzbl	1(%rdi,%rsi), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L427
	sall	$12, %ecx
	sall	$6, %edx
	movzbl	%al, %eax
	andl	$4032, %edx
	movzwl	%cx, %ecx
	orl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 0(%r13)
	movl	400(%r12), %esi
	leal	2(%rsi), %edx
	movl	%edx, 400(%r12)
	movl	0(%r13), %eax
	cmpl	$191, %eax
	jle	.L429
	movl	%eax, %ecx
	movl	%eax, %r11d
	sarl	$5, %ecx
	andl	$31, %r11d
	movslq	%ecx, %rcx
	movzbl	(%rbx,%rcx), %edi
	testl	%edi, %edi
	je	.L490
	movl	(%r10,%rdi,4), %edi
	btl	%r11d, %edi
	jnc	.L490
	andl	$2096897, %eax
	cmpl	$3841, %eax
	je	.L433
	cmpl	404(%r12), %edx
	je	.L491
	movq	%r12, %rdi
	movq	%rcx, -64(%rbp)
	movl	%esi, -52(%rbp)
	call	_ZNK6icu_6724FCDUTF8CollationIterator11nextHasLcccEv
	movl	-52(%rbp), %esi
	movq	-64(%rbp), %rcx
	testb	%al, %al
	jne	.L433
.L491:
	leaq	(%rcx,%rcx), %rax
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L499:
	movslq	48(%rax), %rax
	salq	$2, %rax
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L433:
	leal	-1(%rsi), %eax
	movl	%eax, 400(%r12)
.L435:
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L443
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6724FCDUTF8CollationIterator11nextSegmentER10UErrorCode.part.0
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %r10
	leaq	.LC0(%rip), %r9
	testb	%al, %al
	je	.L443
	movl	408(%r12), %eax
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L443:
	movl	$-1, 0(%r13)
	movl	$192, %r11d
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L429:
	movl	%eax, %r11d
	sarl	$5, %eax
	cltq
	andl	$31, %r11d
	addq	%rax, %rax
.L431:
	movq	8(%r12), %rdx
	movq	(%rdx), %rcx
	movq	16(%rdx), %rdx
	movzwl	(%rcx,%rax), %eax
	leal	(%r11,%rax,4), %eax
	cltq
	movl	(%rdx,%rax,4), %r11d
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L490:
	leaq	(%rcx,%rcx), %rax
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L449:
	movl	444(%r12), %eax
	jmp	.L450
.L495:
	movq	8(%r12), %rdx
	movq	16(%rdx), %rdx
	movl	(%rdx,%rcx,4), %r11d
	jmp	.L420
.L498:
	movl	$-195323, %r11d
	jmp	.L420
.L494:
	leal	1(%rcx), %esi
	movl	%esi, 400(%r12)
	cmpl	%ecx, %eax
	jbe	.L461
	andl	$2, %edx
	leaq	442(%r12), %rax
	jne	.L456
	movq	456(%r12), %rax
.L456:
	movzwl	(%rax,%rcx,2), %edx
	movl	%edx, %eax
	movl	%edx, %r11d
	sarl	$5, %eax
	andl	$31, %r11d
	cltq
	addq	%rax, %rax
.L454:
	movl	%edx, 0(%r13)
	jmp	.L431
.L461:
	movl	$31, %r11d
	movl	$4094, %eax
	movl	$65535, %edx
	jmp	.L454
	.cfi_endproc
.LFE3276:
	.size	_ZN6icu_6724FCDUTF8CollationIterator14handleNextCE32ERiR10UErrorCode, .-_ZN6icu_6724FCDUTF8CollationIterator14handleNextCE32ERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIterator13nextCodePointER10UErrorCode
	.type	_ZN6icu_6724FCDUTF8CollationIterator13nextCodePointER10UErrorCode, @function
_ZN6icu_6724FCDUTF8CollationIterator13nextCodePointER10UErrorCode:
.LFB3281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC1(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	.LC0(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	_ZN6icu_6712CollationFCD9tcccIndexE(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	408(%rdi), %eax
.L501:
	testl	%eax, %eax
	je	.L572
	cmpl	$2, %eax
	je	.L573
	cmpl	$3, %eax
	jne	.L531
	movswl	440(%rbx), %eax
	movl	400(%rbx), %r8d
	testw	%ax, %ax
	js	.L532
	sarl	$5, %eax
.L533:
	cmpl	%eax, %r8d
	jne	.L574
.L535:
	movl	416(%rbx), %edx
	movl	%edx, 400(%rbx)
	movl	%edx, 412(%rbx)
.L524:
	movl	$0, 408(%rbx)
.L539:
	movl	404(%rbx), %edi
	cmpl	%edi, %edx
	je	.L503
	movq	392(%rbx), %r8
	movslq	%edx, %rax
	addq	%r8, %rax
	movzbl	(%rax), %r10d
	movl	%r10d, %r9d
	testl	%edi, %edi
	jns	.L542
	testl	%r10d, %r10d
	je	.L503
.L542:
	leal	1(%rdx), %ecx
	movl	%ecx, 400(%rbx)
	testb	%r9b, %r9b
	jns	.L500
	movzbl	(%rax), %r10d
	movl	%r10d, %eax
	testb	%r10b, %r10b
	js	.L575
.L507:
	cmpl	$191, %r10d
	jg	.L576
.L500:
	addq	$24, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	.cfi_restore_state
	cmpl	$1, %eax
	jne	.L535
	movl	400(%rbx), %edx
	movl	%edx, 412(%rbx)
	cmpl	416(%rbx), %edx
	je	.L524
	movl	$2, 408(%rbx)
.L540:
	movq	392(%rbx), %rsi
	movslq	%edx, %rax
	leal	1(%rdx), %ecx
	movl	%ecx, 400(%rbx)
	movzbl	(%rsi,%rax), %r10d
	movl	%r10d, %eax
	testb	%r10b, %r10b
	jns	.L500
	movl	404(%rbx), %edi
	cmpl	%edi, %ecx
	je	.L528
	cmpl	$223, %r10d
	jle	.L526
	cmpl	$239, %r10d
	jg	.L527
	movslq	%ecx, %rcx
	andl	$15, %eax
	leaq	.LC0(%rip), %r8
	movl	%r10d, %edx
	movzbl	(%rsi,%rcx), %ecx
	movsbl	(%r8,%rax), %r8d
	andl	$15, %edx
	movl	%ecx, %eax
	shrb	$5, %al
	btl	%eax, %r8d
	jnc	.L528
	movl	%ecx, %eax
	andl	$63, %eax
.L529:
	movl	400(%rbx), %ecx
	addl	$1, %ecx
	movl	%ecx, 400(%rbx)
	cmpl	%ecx, %edi
	je	.L528
	sall	$6, %edx
	movzbl	%al, %eax
	orl	%edx, %eax
.L530:
	movslq	400(%rbx), %rdx
	movq	%rdx, %rcx
	movzbl	(%rsi,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L528
	sall	$6, %eax
	movzbl	%dl, %edx
	addl	$1, %ecx
	orl	%edx, %eax
	movl	%ecx, 400(%rbx)
	movl	%eax, %r10d
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L575:
	cmpl	%ecx, %edi
	je	.L512
	cmpl	$223, %r10d
	jle	.L510
	cmpl	$239, %r10d
	jg	.L511
	movslq	%ecx, %rdx
	andl	$15, %eax
	andl	$15, %r10d
	movzbl	(%r8,%rdx), %edx
	movsbl	(%r14,%rax), %r9d
	movl	%edx, %eax
	andl	$63, %edx
	shrb	$5, %al
	btl	%eax, %r9d
	jnc	.L512
.L513:
	movzbl	%dl, %eax
	sall	$6, %r10d
	orl	%eax, %r10d
	leal	1(%rcx), %eax
	movl	%eax, 400(%rbx)
	cmpl	%eax, %edi
	je	.L512
	cltq
	movzbl	(%r8,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L512
	sall	$6, %r10d
	movzbl	%al, %eax
	addl	$2, %ecx
	orl	%eax, %r10d
	movl	%ecx, 400(%rbx)
	cmpl	$65535, %r10d
	jle	.L507
	movl	%r10d, %eax
	sarl	$10, %eax
	subw	$10304, %ax
	movzwl	%ax, %eax
	movl	%eax, %edx
	sarl	$5, %edx
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L510:
	cmpl	$193, %r10d
	jle	.L512
	movslq	%ecx, %rcx
	movzbl	(%r8,%rcx), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	jbe	.L577
	.p2align 4,,10
	.p2align 3
.L512:
	movl	$2047, %edx
	movl	$65533, %r10d
	movl	$65533, %eax
.L509:
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %edx
	testl	%edx, %edx
	je	.L500
	movl	0(%r13,%rdx,4), %edx
	btl	%eax, %edx
	jnc	.L500
	movl	%r10d, %eax
	movl	400(%rbx), %r11d
	andl	$2096897, %eax
	cmpl	$3841, %eax
	je	.L517
	movq	%rsi, -56(%rbp)
	cmpl	%r11d, %edi
	je	.L500
	movq	%rbx, %rdi
	call	_ZNK6icu_6724FCDUTF8CollationIterator11nextHasLcccEv
	movq	-56(%rbp), %rsi
	testb	%al, %al
	je	.L500
.L517:
	cmpl	$2047, %r10d
	jg	.L518
	subl	$2, %r11d
.L519:
	movl	(%rsi), %eax
	movl	%r11d, 400(%rbx)
	testl	%eax, %eax
	jg	.L503
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN6icu_6724FCDUTF8CollationIterator11nextSegmentER10UErrorCode.part.0
	movq	-56(%rbp), %rsi
	testb	%al, %al
	je	.L503
	movl	408(%rbx), %eax
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L573:
	movl	400(%rbx), %edx
	cmpl	416(%rbx), %edx
	je	.L524
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L572:
	movl	400(%rbx), %edx
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L532:
	movl	444(%rbx), %eax
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L503:
	movl	$-1, %r10d
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L511:
	subl	$240, %r10d
	cmpl	$4, %r10d
	jg	.L512
	movslq	%ecx, %rcx
	movzbl	(%r8,%rcx), %r9d
	movq	%r9, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movsbl	(%r15,%rax), %eax
	btl	%r10d, %eax
	jnc	.L512
	leal	2(%rdx), %ecx
	movl	%ecx, 400(%rbx)
	cmpl	%ecx, %edi
	je	.L512
	movslq	%ecx, %rax
	movzbl	(%r8,%rax), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L512
	sall	$6, %r10d
	andl	$63, %r9d
	orl	%r9d, %r10d
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L518:
	cmpl	$55295, %r10d
	ja	.L520
	subl	$3, %r11d
	jmp	.L519
.L520:
	leal	-57344(%r10), %eax
	cmpl	$1056767, %eax
	ja	.L519
	cmpl	$65536, %r10d
	adcl	$-4, %r11d
	jmp	.L519
.L574:
	leaq	432(%rbx), %rdi
	movl	%r8d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r10d
	xorl	%eax, %eax
	cmpl	$65535, %r10d
	seta	%al
	addl	$1, %eax
	addl	%eax, 400(%rbx)
	jmp	.L500
.L528:
	movl	$65533, %r10d
	jmp	.L500
.L526:
	cmpl	$193, %r10d
	jle	.L528
	andl	$31, %eax
	jmp	.L530
.L527:
	subl	$240, %r10d
	cmpl	$4, %r10d
	jg	.L528
	movslq	%ecx, %rcx
	leaq	.LC1(%rip), %r8
	movzbl	(%rsi,%rcx), %ecx
	movq	%rcx, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movsbl	(%r8,%rax), %eax
	btl	%r10d, %eax
	jnc	.L528
	addl	$2, %edx
	movl	%edx, 400(%rbx)
	cmpl	%edx, %edi
	je	.L528
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L528
	movl	%ecx, %edx
	sall	$6, %r10d
	andl	$63, %edx
	orl	%r10d, %edx
	jmp	.L529
.L576:
	movl	%r10d, %edx
	movl	%r10d, %eax
	sarl	$5, %edx
	jmp	.L509
.L577:
	andl	$31, %eax
	movzbl	%cl, %ecx
	addl	$2, %edx
	sall	$6, %eax
	movl	%edx, 400(%rbx)
	orl	%ecx, %eax
	movl	%eax, %r10d
	jmp	.L507
	.cfi_endproc
.LFE3281:
	.size	_ZN6icu_6724FCDUTF8CollationIterator13nextCodePointER10UErrorCode, .-_ZN6icu_6724FCDUTF8CollationIterator13nextCodePointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIterator20forwardNumCodePointsEiR10UErrorCode
	.type	_ZN6icu_6724FCDUTF8CollationIterator20forwardNumCodePointsEiR10UErrorCode, @function
_ZN6icu_6724FCDUTF8CollationIterator20forwardNumCodePointsEiR10UErrorCode:
.LFB3283:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jle	.L583
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L586:
	subl	$1, %ebx
	je	.L578
.L580:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6724FCDUTF8CollationIterator13nextCodePointER10UErrorCode
	testl	%eax, %eax
	jns	.L586
.L578:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L583:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE3283:
	.size	_ZN6icu_6724FCDUTF8CollationIterator20forwardNumCodePointsEiR10UErrorCode, .-_ZN6icu_6724FCDUTF8CollationIterator20forwardNumCodePointsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724FCDUTF8CollationIterator15previousHasTcccEv
	.type	_ZNK6icu_6724FCDUTF8CollationIterator15previousHasTcccEv, @function
_ZNK6icu_6724FCDUTF8CollationIterator15previousHasTcccEv:
.LFB3278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	392(%rdi), %r9
	movslq	400(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpb	$0, -1(%r9,%rcx)
	jns	.L587
	movq	%rcx, %rdx
	subl	$1, %edx
	movl	%edx, -12(%rbp)
	movslq	%edx, %rdx
	movzbl	(%r9,%rdx), %ecx
	testb	%cl, %cl
	js	.L597
.L589:
	xorl	%eax, %eax
	cmpl	$191, %ecx
	jg	.L598
	.p2align 4,,10
	.p2align 3
.L587:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L599
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L598:
	.cfi_restore_state
	movl	%ecx, %edx
	leaq	_ZN6icu_6712CollationFCD9tcccIndexE(%rip), %rsi
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %edx
	testl	%edx, %edx
	je	.L587
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %rax
	movl	(%rax,%rdx,4), %eax
	shrl	%cl, %eax
	andl	$1, %eax
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L597:
	leaq	-12(%rbp), %rdx
	movl	$-3, %r8d
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	utf8_prevCharSafeBody_67@PLT
	movl	%eax, %ecx
	cmpl	$65535, %eax
	jle	.L589
	sarl	$10, %ecx
	xorl	%eax, %eax
	subw	$10304, %cx
	movzwl	%cx, %ecx
	cmpl	$191, %ecx
	jle	.L587
	jmp	.L598
.L599:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3278:
	.size	_ZNK6icu_6724FCDUTF8CollationIterator15previousHasTcccEv, .-_ZNK6icu_6724FCDUTF8CollationIterator15previousHasTcccEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIterator17previousCodePointER10UErrorCode
	.type	_ZN6icu_6724FCDUTF8CollationIterator17previousCodePointER10UErrorCode, @function
_ZN6icu_6724FCDUTF8CollationIterator17previousCodePointER10UErrorCode:
.LFB3282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	400(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	408(%rdi), %eax
.L601:
	cmpl	$1, %eax
	je	.L644
	cmpl	$2, %eax
	je	.L645
	jle	.L618
	movl	400(%rbx), %eax
	testl	%eax, %eax
	jne	.L646
.L619:
	movl	412(%rbx), %eax
	movl	%eax, 400(%rbx)
	movl	%eax, 416(%rbx)
.L617:
	movl	$1, 408(%rbx)
.L622:
	testl	%eax, %eax
	je	.L603
	movq	392(%rbx), %rdi
	movslq	%eax, %rdx
	subl	$1, %eax
	movzbl	-1(%rdi,%rdx), %r9d
	movl	%eax, 400(%rbx)
	testb	%r9b, %r9b
	jns	.L600
	cltq
	movzbl	(%rdi,%rax), %r9d
	testb	%r9b, %r9b
	js	.L647
.L600:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L618:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L619
	movl	400(%rbx), %eax
	movl	%eax, 416(%rbx)
	cmpl	412(%rbx), %eax
	je	.L617
	movl	$2, 408(%rbx)
.L623:
	subl	$1, %eax
	movq	392(%rbx), %rdi
	movl	%eax, 400(%rbx)
	cltq
	movzbl	(%rdi,%rax), %r9d
	testb	%r9b, %r9b
	jns	.L600
	addq	$24, %rsp
	leaq	400(%rbx), %rdx
	movl	%r9d, %ecx
	xorl	%esi, %esi
	popq	%rbx
	movl	$-3, %r8d
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	utf8_prevCharSafeBody_67@PLT
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore_state
	movl	400(%rbx), %eax
	cmpl	412(%rbx), %eax
	je	.L617
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L647:
	movl	%r9d, %ecx
	movl	$-3, %r8d
	movq	%r15, %rdx
	xorl	%esi, %esi
	call	utf8_prevCharSafeBody_67@PLT
	movl	%eax, %r9d
	cmpl	$65535, %eax
	jle	.L606
	sarl	$10, %eax
	subw	$10304, %ax
	movzwl	%ax, %eax
.L606:
	cmpl	$767, %eax
	jle	.L600
	movl	%eax, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %edx
	testl	%edx, %edx
	je	.L600
	movl	0(%r13,%rdx,4), %edx
	btl	%eax, %edx
	jnc	.L600
	movl	%r9d, %eax
	andl	$2096897, %eax
	cmpl	$3841, %eax
	je	.L610
	movl	400(%rbx), %edx
	testl	%edx, %edx
	je	.L600
	movq	%rbx, %rdi
	movl	%r9d, -52(%rbp)
	call	_ZNK6icu_6724FCDUTF8CollationIterator15previousHasTcccEv
	movl	-52(%rbp), %r9d
	testb	%al, %al
	je	.L600
.L610:
	movl	400(%rbx), %eax
	cmpl	$127, %r9d
	jbe	.L648
	cmpl	$2047, %r9d
	jbe	.L649
	cmpl	$55295, %r9d
	ja	.L613
	addl	$3, %eax
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L644:
	movl	400(%rbx), %eax
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L603:
	movl	$-1, %r9d
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L648:
	addl	$1, %eax
.L611:
	movl	%eax, 400(%rbx)
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L603
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6724FCDUTF8CollationIterator15previousSegmentER10UErrorCode.part.0
	testb	%al, %al
	je	.L603
	movl	408(%rbx), %eax
	jmp	.L601
.L649:
	addl	$2, %eax
	jmp	.L611
.L646:
	leal	-1(%rax), %esi
	leaq	432(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r9d
	xorl	%eax, %eax
	cmpl	$65535, %r9d
	seta	%al
	addl	$1, %eax
	subl	%eax, 400(%rbx)
	jmp	.L600
.L613:
	leal	-57344(%r9), %edx
	cmpl	$1056767, %edx
	ja	.L611
	movl	%eax, %edx
	xorl	%eax, %eax
	cmpl	$65535, %r9d
	seta	%al
	leal	3(%rax,%rdx), %eax
	jmp	.L611
	.cfi_endproc
.LFE3282:
	.size	_ZN6icu_6724FCDUTF8CollationIterator17previousCodePointER10UErrorCode, .-_ZN6icu_6724FCDUTF8CollationIterator17previousCodePointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIterator21backwardNumCodePointsEiR10UErrorCode
	.type	_ZN6icu_6724FCDUTF8CollationIterator21backwardNumCodePointsEiR10UErrorCode, @function
_ZN6icu_6724FCDUTF8CollationIterator21backwardNumCodePointsEiR10UErrorCode:
.LFB3284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	testl	%esi, %esi
	jle	.L650
	movq	%rdi, %rbx
	movl	%esi, %r14d
	leaq	400(%rdi), %r12
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %r13
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %r15
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L704:
	movl	400(%rbx), %esi
	testl	%esi, %esi
	jne	.L700
.L673:
	movslq	412(%rbx), %rax
	movl	%eax, 400(%rbx)
	movl	%eax, 416(%rbx)
.L670:
	movl	$1, 408(%rbx)
.L676:
	testl	%eax, %eax
	je	.L650
	movq	392(%rbx), %rdi
	leal	-1(%rax), %edx
	cmpb	$0, -1(%rdi,%rax)
	movl	%edx, 400(%rbx)
	jns	.L656
	movslq	%edx, %rdx
	movzbl	(%rdi,%rdx), %ecx
	testb	%cl, %cl
	js	.L701
.L656:
	subl	$1, %r14d
	je	.L650
.L698:
	movl	408(%rbx), %eax
	cmpl	$1, %eax
	je	.L702
	cmpl	$2, %eax
	je	.L703
	jg	.L704
	testl	%eax, %eax
	jne	.L673
	movslq	400(%rbx), %rax
	movl	%eax, 416(%rbx)
	cmpl	412(%rbx), %eax
	je	.L670
	movl	$2, 408(%rbx)
.L677:
	subl	$1, %eax
	movq	392(%rbx), %rdi
	movl	%eax, 400(%rbx)
	cltq
	movzbl	(%rdi,%rax), %eax
	testb	%al, %al
	jns	.L656
	movzbl	%al, %ecx
	movl	$-3, %r8d
	movq	%r12, %rdx
	xorl	%esi, %esi
	call	utf8_prevCharSafeBody_67@PLT
	movl	%eax, %ecx
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L703:
	movslq	400(%rbx), %rax
	cmpl	412(%rbx), %eax
	je	.L670
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L701:
	movq	%r12, %rdx
	movl	$-3, %r8d
	xorl	%esi, %esi
	call	utf8_prevCharSafeBody_67@PLT
	movl	%eax, %ecx
	movl	%eax, %edx
	cmpl	$65535, %eax
	jle	.L658
	sarl	$10, %edx
	subw	$10304, %dx
	movzwl	%dx, %edx
.L658:
	cmpl	$767, %edx
	jle	.L659
	movl	%edx, %eax
	sarl	$5, %eax
	cltq
	movzbl	0(%r13,%rax), %eax
	testl	%eax, %eax
	jne	.L705
.L659:
	testl	%ecx, %ecx
	jns	.L656
.L650:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L705:
	.cfi_restore_state
	movl	(%r15,%rax,4), %eax
	btl	%edx, %eax
	jnc	.L659
	movl	%ecx, %eax
	andl	$2096897, %eax
	cmpl	$3841, %eax
	je	.L663
	movl	400(%rbx), %esi
	testl	%esi, %esi
	je	.L659
	movq	%rbx, %rdi
	movl	%ecx, -60(%rbp)
	call	_ZNK6icu_6724FCDUTF8CollationIterator15previousHasTcccEv
	movl	-60(%rbp), %ecx
	testb	%al, %al
	je	.L659
.L663:
	movl	400(%rbx), %eax
	cmpl	$127, %ecx
	jbe	.L706
	cmpl	$2047, %ecx
	jbe	.L707
	cmpl	$55295, %ecx
	jbe	.L708
	leal	-57344(%rcx), %edx
	cmpl	$1056767, %edx
	ja	.L664
	movl	%eax, %edx
	xorl	%eax, %eax
	cmpl	$65535, %ecx
	seta	%al
	leal	3(%rax,%rdx), %eax
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L702:
	movslq	400(%rbx), %rax
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L706:
	addl	$1, %eax
.L664:
	movl	%eax, 400(%rbx)
	movq	-56(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L650
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6724FCDUTF8CollationIterator15previousSegmentER10UErrorCode.part.0
	testb	%al, %al
	jne	.L698
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L707:
	addl	$2, %eax
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L708:
	addl	$3, %eax
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L700:
	subl	$1, %esi
	leaq	432(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	$65535, %ecx
	seta	%al
	addl	$1, %eax
	subl	%eax, 400(%rbx)
	jmp	.L659
	.cfi_endproc
.LFE3284:
	.size	_ZN6icu_6724FCDUTF8CollationIterator21backwardNumCodePointsEiR10UErrorCode, .-_ZN6icu_6724FCDUTF8CollationIterator21backwardNumCodePointsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIterator15switchToForwardEv
	.type	_ZN6icu_6724FCDUTF8CollationIterator15switchToForwardEv, @function
_ZN6icu_6724FCDUTF8CollationIterator15switchToForwardEv:
.LFB3285:
	.cfi_startproc
	endbr64
	movl	408(%rdi), %eax
	cmpl	$1, %eax
	je	.L717
	cmpl	$2, %eax
	je	.L713
	movl	416(%rdi), %eax
	movl	%eax, 400(%rdi)
	movl	%eax, 412(%rdi)
.L713:
	movl	$0, 408(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	movl	400(%rdi), %eax
	movl	%eax, 412(%rdi)
	cmpl	416(%rdi), %eax
	je	.L713
	movl	$2, 408(%rdi)
	ret
	.cfi_endproc
.LFE3285:
	.size	_ZN6icu_6724FCDUTF8CollationIterator15switchToForwardEv, .-_ZN6icu_6724FCDUTF8CollationIterator15switchToForwardEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIterator11nextSegmentER10UErrorCode
	.type	_ZN6icu_6724FCDUTF8CollationIterator11nextSegmentER10UErrorCode, @function
_ZN6icu_6724FCDUTF8CollationIterator11nextSegmentER10UErrorCode:
.LFB3286:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L719
	jmp	_ZN6icu_6724FCDUTF8CollationIterator11nextSegmentER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L719:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3286:
	.size	_ZN6icu_6724FCDUTF8CollationIterator11nextSegmentER10UErrorCode, .-_ZN6icu_6724FCDUTF8CollationIterator11nextSegmentER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIterator16switchToBackwardEv
	.type	_ZN6icu_6724FCDUTF8CollationIterator16switchToBackwardEv, @function
_ZN6icu_6724FCDUTF8CollationIterator16switchToBackwardEv:
.LFB3287:
	.cfi_startproc
	endbr64
	movl	408(%rdi), %eax
	testl	%eax, %eax
	jne	.L721
	movl	400(%rdi), %eax
	movl	%eax, 416(%rdi)
	cmpl	412(%rdi), %eax
	je	.L724
	movl	$2, 408(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L721:
	cmpl	$2, %eax
	je	.L724
	movl	412(%rdi), %eax
	movl	%eax, 400(%rdi)
	movl	%eax, 416(%rdi)
.L724:
	movl	$1, 408(%rdi)
	ret
	.cfi_endproc
.LFE3287:
	.size	_ZN6icu_6724FCDUTF8CollationIterator16switchToBackwardEv, .-_ZN6icu_6724FCDUTF8CollationIterator16switchToBackwardEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIterator15previousSegmentER10UErrorCode
	.type	_ZN6icu_6724FCDUTF8CollationIterator15previousSegmentER10UErrorCode, @function
_ZN6icu_6724FCDUTF8CollationIterator15previousSegmentER10UErrorCode:
.LFB3288:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L729
	jmp	_ZN6icu_6724FCDUTF8CollationIterator15previousSegmentER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L729:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3288:
	.size	_ZN6icu_6724FCDUTF8CollationIterator15previousSegmentER10UErrorCode, .-_ZN6icu_6724FCDUTF8CollationIterator15previousSegmentER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FCDUTF8CollationIterator9normalizeERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6724FCDUTF8CollationIterator9normalizeERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6724FCDUTF8CollationIterator9normalizeERKNS_13UnicodeStringER10UErrorCode:
.LFB3289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	leaq	432(%rdi), %rdx
	movq	%rbx, %rcx
	subq	$8, %rsp
	movq	424(%rdi), %rdi
	call	_ZNK6icu_6715Normalizer2Impl9decomposeERKNS_13UnicodeStringERS1_R10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	setle	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3289:
	.size	_ZN6icu_6724FCDUTF8CollationIterator9normalizeERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6724FCDUTF8CollationIterator9normalizeERKNS_13UnicodeStringER10UErrorCode
	.weak	_ZTSN6icu_6721UTF8CollationIteratorE
	.section	.rodata._ZTSN6icu_6721UTF8CollationIteratorE,"aG",@progbits,_ZTSN6icu_6721UTF8CollationIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6721UTF8CollationIteratorE, @object
	.size	_ZTSN6icu_6721UTF8CollationIteratorE, 33
_ZTSN6icu_6721UTF8CollationIteratorE:
	.string	"N6icu_6721UTF8CollationIteratorE"
	.weak	_ZTIN6icu_6721UTF8CollationIteratorE
	.section	.data.rel.ro._ZTIN6icu_6721UTF8CollationIteratorE,"awG",@progbits,_ZTIN6icu_6721UTF8CollationIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6721UTF8CollationIteratorE, @object
	.size	_ZTIN6icu_6721UTF8CollationIteratorE, 24
_ZTIN6icu_6721UTF8CollationIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6721UTF8CollationIteratorE
	.quad	_ZTIN6icu_6717CollationIteratorE
	.weak	_ZTSN6icu_6724FCDUTF8CollationIteratorE
	.section	.rodata._ZTSN6icu_6724FCDUTF8CollationIteratorE,"aG",@progbits,_ZTSN6icu_6724FCDUTF8CollationIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6724FCDUTF8CollationIteratorE, @object
	.size	_ZTSN6icu_6724FCDUTF8CollationIteratorE, 36
_ZTSN6icu_6724FCDUTF8CollationIteratorE:
	.string	"N6icu_6724FCDUTF8CollationIteratorE"
	.weak	_ZTIN6icu_6724FCDUTF8CollationIteratorE
	.section	.data.rel.ro._ZTIN6icu_6724FCDUTF8CollationIteratorE,"awG",@progbits,_ZTIN6icu_6724FCDUTF8CollationIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6724FCDUTF8CollationIteratorE, @object
	.size	_ZTIN6icu_6724FCDUTF8CollationIteratorE, 24
_ZTIN6icu_6724FCDUTF8CollationIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6724FCDUTF8CollationIteratorE
	.quad	_ZTIN6icu_6721UTF8CollationIteratorE
	.weak	_ZTVN6icu_6721UTF8CollationIteratorE
	.section	.data.rel.ro._ZTVN6icu_6721UTF8CollationIteratorE,"awG",@progbits,_ZTVN6icu_6721UTF8CollationIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6721UTF8CollationIteratorE, @object
	.size	_ZTVN6icu_6721UTF8CollationIteratorE, 144
_ZTVN6icu_6721UTF8CollationIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6721UTF8CollationIteratorE
	.quad	_ZN6icu_6721UTF8CollationIteratorD1Ev
	.quad	_ZN6icu_6721UTF8CollationIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717CollationIteratoreqERKS0_
	.quad	_ZN6icu_6721UTF8CollationIterator13resetToOffsetEi
	.quad	_ZNK6icu_6721UTF8CollationIterator9getOffsetEv
	.quad	_ZN6icu_6721UTF8CollationIterator13nextCodePointER10UErrorCode
	.quad	_ZN6icu_6721UTF8CollationIterator17previousCodePointER10UErrorCode
	.quad	_ZN6icu_6721UTF8CollationIterator14handleNextCE32ERiR10UErrorCode
	.quad	_ZN6icu_6717CollationIterator23handleGetTrailSurrogateEv
	.quad	_ZN6icu_6721UTF8CollationIterator18foundNULTerminatorEv
	.quad	_ZNK6icu_6721UTF8CollationIterator25forbidSurrogateCodePointsEv
	.quad	_ZN6icu_6721UTF8CollationIterator20forwardNumCodePointsEiR10UErrorCode
	.quad	_ZN6icu_6721UTF8CollationIterator21backwardNumCodePointsEiR10UErrorCode
	.quad	_ZNK6icu_6717CollationIterator11getDataCE32Ei
	.quad	_ZN6icu_6717CollationIterator22getCE32FromBuilderDataEjR10UErrorCode
	.weak	_ZTVN6icu_6724FCDUTF8CollationIteratorE
	.section	.data.rel.ro._ZTVN6icu_6724FCDUTF8CollationIteratorE,"awG",@progbits,_ZTVN6icu_6724FCDUTF8CollationIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6724FCDUTF8CollationIteratorE, @object
	.size	_ZTVN6icu_6724FCDUTF8CollationIteratorE, 144
_ZTVN6icu_6724FCDUTF8CollationIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6724FCDUTF8CollationIteratorE
	.quad	_ZN6icu_6724FCDUTF8CollationIteratorD1Ev
	.quad	_ZN6icu_6724FCDUTF8CollationIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717CollationIteratoreqERKS0_
	.quad	_ZN6icu_6724FCDUTF8CollationIterator13resetToOffsetEi
	.quad	_ZNK6icu_6724FCDUTF8CollationIterator9getOffsetEv
	.quad	_ZN6icu_6724FCDUTF8CollationIterator13nextCodePointER10UErrorCode
	.quad	_ZN6icu_6724FCDUTF8CollationIterator17previousCodePointER10UErrorCode
	.quad	_ZN6icu_6724FCDUTF8CollationIterator14handleNextCE32ERiR10UErrorCode
	.quad	_ZN6icu_6724FCDUTF8CollationIterator23handleGetTrailSurrogateEv
	.quad	_ZN6icu_6724FCDUTF8CollationIterator18foundNULTerminatorEv
	.quad	_ZNK6icu_6721UTF8CollationIterator25forbidSurrogateCodePointsEv
	.quad	_ZN6icu_6724FCDUTF8CollationIterator20forwardNumCodePointsEiR10UErrorCode
	.quad	_ZN6icu_6724FCDUTF8CollationIterator21backwardNumCodePointsEiR10UErrorCode
	.quad	_ZNK6icu_6717CollationIterator11getDataCE32Ei
	.quad	_ZN6icu_6717CollationIterator22getCE32FromBuilderDataEjR10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
