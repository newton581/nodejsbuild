	.file	"number_notation.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number8Notation10scientificEv
	.type	_ZN6icu_676number8Notation10scientificEv, @function
_ZN6icu_676number8Notation10scientificEv:
.LFB2700:
	.cfi_startproc
	endbr64
	movabsq	$281479271677952, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2700:
	.size	_ZN6icu_676number8Notation10scientificEv, .-_ZN6icu_676number8Notation10scientificEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number8Notation11engineeringEv
	.type	_ZN6icu_676number8Notation11engineeringEv, @function
_ZN6icu_676number8Notation11engineeringEv:
.LFB2704:
	.cfi_startproc
	endbr64
	movabsq	$281487861612544, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2704:
	.size	_ZN6icu_676number8Notation11engineeringEv, .-_ZN6icu_676number8Notation11engineeringEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number18ScientificNotationC2Eabs18UNumberSignDisplay
	.type	_ZN6icu_676number18ScientificNotationC2Eabs18UNumberSignDisplay, @function
_ZN6icu_676number18ScientificNotationC2Eabs18UNumberSignDisplay:
.LFB2706:
	.cfi_startproc
	endbr64
	movl	$0, (%rdi)
	movb	%sil, 4(%rdi)
	movb	%dl, 5(%rdi)
	movw	%cx, 6(%rdi)
	movl	%r8d, 8(%rdi)
	ret
	.cfi_endproc
.LFE2706:
	.size	_ZN6icu_676number18ScientificNotationC2Eabs18UNumberSignDisplay, .-_ZN6icu_676number18ScientificNotationC2Eabs18UNumberSignDisplay
	.globl	_ZN6icu_676number18ScientificNotationC1Eabs18UNumberSignDisplay
	.set	_ZN6icu_676number18ScientificNotationC1Eabs18UNumberSignDisplay,_ZN6icu_676number18ScientificNotationC2Eabs18UNumberSignDisplay
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number8Notation12compactShortEv
	.type	_ZN6icu_676number8Notation12compactShortEv, @function
_ZN6icu_676number8Notation12compactShortEv:
.LFB2708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	-4(%rbp), %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2708:
	.size	_ZN6icu_676number8Notation12compactShortEv, .-_ZN6icu_676number8Notation12compactShortEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number8Notation11compactLongEv
	.type	_ZN6icu_676number8Notation11compactLongEv, @function
_ZN6icu_676number8Notation11compactLongEv:
.LFB2709:
	.cfi_startproc
	endbr64
	movabsq	$4294967297, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	-4(%rbp), %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2709:
	.size	_ZN6icu_676number8Notation11compactLongEv, .-_ZN6icu_676number8Notation11compactLongEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number8Notation6simpleEv
	.type	_ZN6icu_676number8Notation6simpleEv, @function
_ZN6icu_676number8Notation6simpleEv:
.LFB2710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %eax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2710:
	.size	_ZN6icu_676number8Notation6simpleEv, .-_ZN6icu_676number8Notation6simpleEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number18ScientificNotation21withMinExponentDigitsEi
	.type	_ZNK6icu_676number18ScientificNotation21withMinExponentDigitsEi, @function
_ZNK6icu_676number18ScientificNotation21withMinExponentDigitsEi:
.LFB2711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-1(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$998, %eax
	ja	.L14
	movl	8(%rdi), %eax
	movzwl	4(%rdi), %edx
	movw	%si, -6(%rbp)
	movl	%eax, -4(%rbp)
	xorl	%eax, %eax
	movw	%dx, -8(%rbp)
	movl	-4(%rbp), %edx
	movl	%eax, -12(%rbp)
	movq	-12(%rbp), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	$3, %eax
	movl	$65810, -8(%rbp)
	movl	-4(%rbp), %edx
	movl	%eax, -12(%rbp)
	movq	-12(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2711:
	.size	_ZNK6icu_676number18ScientificNotation21withMinExponentDigitsEi, .-_ZNK6icu_676number18ScientificNotation21withMinExponentDigitsEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number18ScientificNotation23withExponentSignDisplayE18UNumberSignDisplay
	.type	_ZNK6icu_676number18ScientificNotation23withExponentSignDisplayE18UNumberSignDisplay, @function
_ZNK6icu_676number18ScientificNotation23withExponentSignDisplayE18UNumberSignDisplay:
.LFB2715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$0, -12(%rbp)
	movl	4(%rdi), %eax
	movl	%eax, -8(%rbp)
	movq	-12(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2715:
	.size	_ZNK6icu_676number18ScientificNotation23withExponentSignDisplayE18UNumberSignDisplay, .-_ZNK6icu_676number18ScientificNotation23withExponentSignDisplayE18UNumberSignDisplay
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
