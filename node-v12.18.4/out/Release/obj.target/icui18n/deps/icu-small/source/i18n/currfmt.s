	.file	"currfmt.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714CurrencyFormat17getDynamicClassIDEv
	.type	_ZNK6icu_6714CurrencyFormat17getDynamicClassIDEv, @function
_ZNK6icu_6714CurrencyFormat17getDynamicClassIDEv:
.LFB2406:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714CurrencyFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2406:
	.size	_ZNK6icu_6714CurrencyFormat17getDynamicClassIDEv, .-_ZNK6icu_6714CurrencyFormat17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CurrencyFormatD2Ev
	.type	_ZN6icu_6714CurrencyFormatD2Ev, @function
_ZN6icu_6714CurrencyFormatD2Ev:
.LFB2399:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714CurrencyFormatE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6713MeasureFormatD2Ev@PLT
	.cfi_endproc
.LFE2399:
	.size	_ZN6icu_6714CurrencyFormatD2Ev, .-_ZN6icu_6714CurrencyFormatD2Ev
	.globl	_ZN6icu_6714CurrencyFormatD1Ev
	.set	_ZN6icu_6714CurrencyFormatD1Ev,_ZN6icu_6714CurrencyFormatD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CurrencyFormatD0Ev
	.type	_ZN6icu_6714CurrencyFormatD0Ev, @function
_ZN6icu_6714CurrencyFormatD0Ev:
.LFB2401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714CurrencyFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6713MeasureFormatD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2401:
	.size	_ZN6icu_6714CurrencyFormatD0Ev, .-_ZN6icu_6714CurrencyFormatD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714CurrencyFormat5cloneEv
	.type	_ZNK6icu_6714CurrencyFormat5cloneEv, @function
_ZNK6icu_6714CurrencyFormat5cloneEv:
.LFB2402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$368, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L6
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713MeasureFormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714CurrencyFormatE(%rip), %rax
	movq	%rax, (%r12)
.L6:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2402:
	.size	_ZNK6icu_6714CurrencyFormat5cloneEv, .-_ZNK6icu_6714CurrencyFormat5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714CurrencyFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6714CurrencyFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6714CurrencyFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB2403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%r8, %rbx
	call	_ZNK6icu_6713MeasureFormat25getCurrencyFormatInternalEv@PLT
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%r12
	popq	%r13
	movq	40(%rax), %rax
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE2403:
	.size	_ZNK6icu_6714CurrencyFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6714CurrencyFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714CurrencyFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.type	_ZNK6icu_6714CurrencyFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, @function
_ZNK6icu_6714CurrencyFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE:
.LFB2404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$8, %rsp
	call	_ZNK6icu_6713MeasureFormat25getCurrencyFormatInternalEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*176(%rax)
	testq	%rax, %rax
	je	.L14
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711Formattable11adoptObjectEPNS_7UObjectE@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2404:
	.size	_ZNK6icu_6714CurrencyFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, .-_ZNK6icu_6714CurrencyFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CurrencyFormatC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6714CurrencyFormatC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6714CurrencyFormatC2ERKNS_6LocaleER10UErrorCode:
.LFB2393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6713MeasureFormatC2ERKNS_6LocaleE19UMeasureFormatWidthR10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6714CurrencyFormatE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2393:
	.size	_ZN6icu_6714CurrencyFormatC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6714CurrencyFormatC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6714CurrencyFormatC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6714CurrencyFormatC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6714CurrencyFormatC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CurrencyFormatC2ERKS0_
	.type	_ZN6icu_6714CurrencyFormatC2ERKS0_, @function
_ZN6icu_6714CurrencyFormatC2ERKS0_:
.LFB2396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6713MeasureFormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714CurrencyFormatE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2396:
	.size	_ZN6icu_6714CurrencyFormatC2ERKS0_, .-_ZN6icu_6714CurrencyFormatC2ERKS0_
	.globl	_ZN6icu_6714CurrencyFormatC1ERKS0_
	.set	_ZN6icu_6714CurrencyFormatC1ERKS0_,_ZN6icu_6714CurrencyFormatC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CurrencyFormat16getStaticClassIDEv
	.type	_ZN6icu_6714CurrencyFormat16getStaticClassIDEv, @function
_ZN6icu_6714CurrencyFormat16getStaticClassIDEv:
.LFB2405:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714CurrencyFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2405:
	.size	_ZN6icu_6714CurrencyFormat16getStaticClassIDEv, .-_ZN6icu_6714CurrencyFormat16getStaticClassIDEv
	.weak	_ZTSN6icu_6714CurrencyFormatE
	.section	.rodata._ZTSN6icu_6714CurrencyFormatE,"aG",@progbits,_ZTSN6icu_6714CurrencyFormatE,comdat
	.align 16
	.type	_ZTSN6icu_6714CurrencyFormatE, @object
	.size	_ZTSN6icu_6714CurrencyFormatE, 26
_ZTSN6icu_6714CurrencyFormatE:
	.string	"N6icu_6714CurrencyFormatE"
	.weak	_ZTIN6icu_6714CurrencyFormatE
	.section	.data.rel.ro._ZTIN6icu_6714CurrencyFormatE,"awG",@progbits,_ZTIN6icu_6714CurrencyFormatE,comdat
	.align 8
	.type	_ZTIN6icu_6714CurrencyFormatE, @object
	.size	_ZTIN6icu_6714CurrencyFormatE, 24
_ZTIN6icu_6714CurrencyFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714CurrencyFormatE
	.quad	_ZTIN6icu_6713MeasureFormatE
	.weak	_ZTVN6icu_6714CurrencyFormatE
	.section	.data.rel.ro._ZTVN6icu_6714CurrencyFormatE,"awG",@progbits,_ZTVN6icu_6714CurrencyFormatE,comdat
	.align 8
	.type	_ZTVN6icu_6714CurrencyFormatE, @object
	.size	_ZTVN6icu_6714CurrencyFormatE, 80
_ZTVN6icu_6714CurrencyFormatE:
	.quad	0
	.quad	_ZTIN6icu_6714CurrencyFormatE
	.quad	_ZN6icu_6714CurrencyFormatD1Ev
	.quad	_ZN6icu_6714CurrencyFormatD0Ev
	.quad	_ZNK6icu_6714CurrencyFormat17getDynamicClassIDEv
	.quad	_ZNK6icu_6713MeasureFormateqERKNS_6FormatE
	.quad	_ZNK6icu_6714CurrencyFormat5cloneEv
	.quad	_ZNK6icu_6714CurrencyFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6714CurrencyFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.local	_ZZN6icu_6714CurrencyFormat16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6714CurrencyFormat16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
