	.file	"numparse_currency.cpp"
	.text
	.section	.text._ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv,"axG",@progbits,_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.type	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv, @function
_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv:
.LFB2799:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2799:
	.size	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv, .-_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.section	.text._ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE,"axG",@progbits,_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.type	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE, @function
_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE:
.LFB2800:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2800:
	.size	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE, .-_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher9smokeTestERKNS_13StringSegmentE
	.type	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher9smokeTestERKNS_13StringSegmentE, @function
_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher9smokeTestERKNS_13StringSegmentE:
.LFB2854:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2854:
	.size	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher9smokeTestERKNS_13StringSegmentE, .-_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher9smokeTestERKNS_13StringSegmentE
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC0:
	.string	"<"
	.string	"C"
	.string	"o"
	.string	"m"
	.string	"b"
	.string	"i"
	.string	"n"
	.string	"e"
	.string	"d"
	.string	"C"
	.string	"u"
	.string	"r"
	.string	"r"
	.string	"e"
	.string	"n"
	.string	"c"
	.string	"y"
	.string	"M"
	.string	"a"
	.string	"t"
	.string	"c"
	.string	"h"
	.string	"e"
	.string	"r"
	.string	">"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher8toStringEv
	.type	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher8toStringEv, @function
_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher8toStringEv:
.LFB2855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2855:
	.size	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher8toStringEv, .-_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher8toStringEv
	.section	.text._ZN6icu_678numparse4impl23CombinedCurrencyMatcherD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD2Ev
	.type	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD2Ev, @function
_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD2Ev:
.LFB3984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpb	$0, 676(%rdi)
	movq	%rax, (%rdi)
	jne	.L12
.L8:
	leaq	600(%r12), %rdi
	leaq	472(%r12), %rbx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	536(%r12), %rdi
	leaq	88(%r12), %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%r13, %rbx
	jne	.L9
	leaq	80(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	664(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L8
	.cfi_endproc
.LFE3984:
	.size	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD2Ev, .-_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD2Ev
	.weak	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD1Ev
	.set	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD1Ev,_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD2Ev
	.section	.text._ZN6icu_678numparse4impl23CombinedCurrencyMatcherD0Ev,"axG",@progbits,_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD0Ev
	.type	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD0Ev, @function
_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD0Ev:
.LFB3986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpb	$0, 676(%rdi)
	movq	%rax, (%rdi)
	jne	.L18
.L14:
	leaq	600(%r12), %rdi
	leaq	472(%r12), %rbx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	536(%r12), %rdi
	leaq	88(%r12), %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%r13, %rbx
	jne	.L15
	leaq	80(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	664(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L14
	.cfi_endproc
.LFE3986:
	.size	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD0Ev, .-_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD0Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3322:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3322:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3325:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L32
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L20
	cmpb	$0, 12(%rbx)
	jne	.L33
.L24:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L24
	.cfi_endproc
.LFE3325:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3328:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L36
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3328:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3331:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L39
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3331:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L45
.L41:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L46
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3333:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3334:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3334:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3335:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3335:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3336:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3336:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3337:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3337:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3338:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3338:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3339:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L62
	testl	%edx, %edx
	jle	.L62
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L65
.L54:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L54
	.cfi_endproc
.LFE3339:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L69
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L69
	testl	%r12d, %r12d
	jg	.L76
	cmpb	$0, 12(%rbx)
	jne	.L77
.L71:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L71
.L77:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L69:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3340:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L79
	movq	(%rdi), %r8
.L80:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L83
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L83
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L83:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3341:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3342:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L90
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3342:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3343:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3343:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3344:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3344:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3345:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3345:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3347:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3347:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3349:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3349:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher13matchCurrencyERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.type	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher13matchCurrencyERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, @function
_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher13matchCurrencyERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode:
.LFB2853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$-1, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%rdx, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movswl	24(%rdi), %eax
	shrl	$5, %eax
	jne	.L132
.L97:
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	%eax, %edx
	movswl	24(%rbx), %eax
	testw	%ax, %ax
	js	.L98
	sarl	$5, %eax
	cmpl	%eax, %r15d
	je	.L133
.L100:
	movswl	88(%rbx), %eax
	movl	$-1, %esi
	shrl	$5, %eax
	jne	.L134
.L102:
	movl	$1, %r13d
	cmpl	%r15d, %edx
	jne	.L135
.L103:
	movswl	88(%rbx), %eax
	testw	%ax, %ax
	js	.L104
	sarl	$5, %eax
	cmpl	%eax, %esi
	je	.L136
.L106:
	cmpb	$0, 144(%rbx)
	jne	.L137
	leaq	536(%rbx), %rax
	leaq	152(%rbx), %r15
	xorl	%r14d, %r14d
	movq	%rax, -168(%rbp)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L139:
	sarl	$5, %edx
.L111:
	cmpl	%edx, %eax
	jne	.L112
	cmpl	%eax, %r14d
	cmovl	%eax, %r14d
.L112:
	testl	%eax, %eax
	setg	%al
	addq	$64, %r15
	orl	%eax, %r13d
	cmpq	-168(%rbp), %r15
	je	.L138
.L115:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movswl	8(%r15), %edx
	testw	%dx, %dx
	jns	.L139
	movl	12(%r15), %edx
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L138:
	testl	%r14d, %r14d
	jne	.L140
.L96:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L141
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movl	28(%rbx), %eax
	cmpl	%eax, %r15d
	jne	.L100
.L133:
	movl	8(%rbx), %eax
	movq	-176(%rbp), %rcx
	cmpl	%r15d, %edx
	movl	%r15d, %esi
	sete	%r13b
	xorl	%r8d, %r8d
	movl	%eax, 208(%rcx)
	movzwl	12(%rbx), %eax
	movq	%rcx, %rbx
	movw	%r8w, 214(%rcx)
	movw	%ax, 212(%rcx)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L104:
	movl	92(%rbx), %eax
	cmpl	%eax, %esi
	jne	.L106
.L136:
	movl	8(%rbx), %eax
	movq	-176(%rbp), %rcx
	xorl	%edi, %edi
	movl	%eax, 208(%rcx)
	movzwl	12(%rbx), %eax
	movq	%rcx, %rbx
	movw	%di, 214(%rcx)
	movw	%ax, 212(%rcx)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	16(%rdi), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment28getCaseSensitivePrefixLengthERKNS_13UnicodeStringE@PLT
	movl	%eax, %r15d
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%r12, %rdi
	movl	%esi, -168(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-168(%rbp), %esi
	cmpl	%esi, %eax
	sete	%r13b
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L134:
	leaq	80(%rbx), %rsi
	movq	%r12, %rdi
	movl	%edx, -168(%rbp)
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movl	-168(%rbp), %edx
	movl	%eax, %esi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	-128(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment19toTempUnicodeStringEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	664(%rbx), %rdi
	pushq	%r14
	leaq	-144(%rbp), %r11
	movq	%rax, -144(%rbp)
	movq	%r11, %rdx
	movabsq	$-4294967296, %rax
	leaq	-148(%rbp), %r8
	movq	%rax, -136(%rbp)
	movq	-176(%rbp), %rax
	movq	%r11, -168(%rbp)
	leaq	208(%rax), %r9
	movl	$0, -148(%rbp)
	call	uprv_parseCurrency_67@PLT
	testb	%r13b, %r13b
	popq	%rcx
	movq	-168(%rbp), %r11
	popq	%rsi
	je	.L142
.L108:
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L109
	movl	-136(%rbp), %esi
	testl	%esi, %esi
	jne	.L143
.L109:
	movq	%r11, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L140:
	movl	8(%rbx), %eax
	movq	-176(%rbp), %rcx
	movl	%r14d, %esi
	movl	%eax, 208(%rcx)
	movzwl	12(%rbx), %eax
	movq	%rcx, %rbx
	movw	%ax, 212(%rcx)
	xorl	%eax, %eax
	movw	%ax, 214(%rcx)
.L131:
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment12adjustOffsetEi@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_678numparse4impl12ParsedNumber16setCharsConsumedERKNS_13StringSegmentE@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L142:
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%eax, -148(%rbp)
	movq	-168(%rbp), %r11
	sete	%r13b
	jmp	.L108
.L143:
	movq	%r12, %rdi
	movq	%r11, -168(%rbp)
	call	_ZN6icu_6713StringSegment12adjustOffsetEi@PLT
	movq	-176(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_678numparse4impl12ParsedNumber16setCharsConsumedERKNS_13StringSegmentE@PLT
	movq	-168(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L96
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2853:
	.size	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher13matchCurrencyERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, .-_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher13matchCurrencyERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.type	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, @function
_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode:
.LFB2852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpw	$0, 208(%rdx)
	je	.L164
.L144:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	%rdx, %r12
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r15
	movq	%rcx, %r14
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	_ZNK6icu_678numparse4impl12ParsedNumber10seenNumberEv@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L148
	movswl	608(%rbx), %eax
	shrl	$5, %eax
	jne	.L165
.L148:
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher13matchCurrencyERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	movl	%eax, %r13d
.L147:
	cmpw	$0, 208(%r12)
	je	.L166
	movq	%r12, %rdi
	call	_ZNK6icu_678numparse4impl12ParsedNumber10seenNumberEv@PLT
	testb	%al, %al
	jne	.L144
	movswl	544(%rbx), %eax
	shrl	$5, %eax
	je	.L144
	leaq	536(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movl	%eax, %r12d
	movswl	544(%rbx), %eax
	testw	%ax, %ax
	js	.L153
	sarl	$5, %eax
.L154:
	cmpl	%eax, %r12d
	je	.L167
.L155:
	testb	%r13b, %r13b
	jne	.L144
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%eax, %r12d
	sete	%r13b
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L166:
	movl	-52(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713StringSegment9setOffsetEi@PLT
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L165:
	leaq	600(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movl	%eax, %edx
	movswl	608(%rbx), %eax
	testw	%ax, %ax
	js	.L149
	sarl	$5, %eax
.L150:
	cmpl	%eax, %edx
	je	.L168
.L151:
	movq	%r15, %rdi
	movl	%edx, -56(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-56(%rbp), %edx
	cmpl	%eax, %edx
	jne	.L148
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L153:
	movl	548(%rbx), %eax
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L149:
	movl	612(%rbx), %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L168:
	movl	%edx, %esi
	movq	%r15, %rdi
	movl	%edx, -56(%rbp)
	call	_ZN6icu_6713StringSegment12adjustOffsetEi@PLT
	movl	-56(%rbp), %edx
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L167:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713StringSegment12adjustOffsetEi@PLT
	jmp	.L155
	.cfi_endproc
.LFE2852:
	.size	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, .-_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherC2ERKNS_6number4impl15CurrencySymbolsERKNS_20DecimalFormatSymbolsEiR10UErrorCode
	.type	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherC2ERKNS_6number4impl15CurrencySymbolsERKNS_20DecimalFormatSymbolsEiR10UErrorCode, @function
_ZN6icu_678numparse4impl23CombinedCurrencyMatcherC2ERKNS_6number4impl15CurrencySymbolsERKNS_20DecimalFormatSymbolsEiR10UErrorCode:
.LFB2850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movq	%r8, %rdx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	shrl	$13, %r12d
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$16, %rdi
	xorl	$1, %r12d
	andl	$1, %r12d
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE(%rip), %rax
	movq	%rax, -16(%rdi)
	call	_ZNK6icu_676number4impl15CurrencySymbols17getCurrencySymbolER10UErrorCode@PLT
	leaq	80(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZNK6icu_676number4impl15CurrencySymbols21getIntlCurrencySymbolER10UErrorCode@PLT
	movl	$2, %r8d
	movl	$2, %r9d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movl	$2, %ecx
	movl	$2, %esi
	movl	$2, %edi
	movw	%r8w, 416(%rbx)
	movw	%r9w, 480(%rbx)
	movb	%r12b, 144(%rbx)
	leaq	-288(%rbp), %r12
	movw	%dx, 160(%rbx)
	xorl	%edx, %edx
	movw	%cx, 224(%rbx)
	movq	%r13, %rcx
	movw	%si, 288(%rbx)
	movl	$2, %esi
	movw	%di, 352(%rbx)
	movq	%r15, %rdi
	movq	%rax, 152(%rbx)
	movq	%rax, 216(%rbx)
	movq	%rax, 280(%rbx)
	movq	%rax, 344(%rbx)
	movq	%rax, 408(%rbx)
	movq	%rax, 472(%rbx)
	call	_ZNK6icu_6720DecimalFormatSymbols28getPatternForCurrencySpacingE16UCurrencySpacingaR10UErrorCode@PLT
	leaq	536(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	movl	$2, %esi
	call	_ZNK6icu_6720DecimalFormatSymbols28getPatternForCurrencySpacingE16UCurrencySpacingaR10UErrorCode@PLT
	leaq	600(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	1872(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	xorl	%r10d, %r10d
	movl	$-1, %edx
	movq	%r13, %rcx
	leaq	677(%rbx), %rax
	movw	%r10w, 676(%rbx)
	movq	-248(%rbp), %rsi
	leaq	664(%rbx), %rdi
	movq	%rax, 664(%rbx)
	movl	$0, 720(%rbx)
	movl	$40, 672(%rbx)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_676number4impl15CurrencySymbols10getIsoCodeEv@PLT
	xorl	%r11d, %r11d
	cmpb	$0, 144(%rbx)
	movl	(%rax), %edx
	movl	%edx, 8(%rbx)
	movzwl	4(%rax), %eax
	movw	%r11w, 14(%rbx)
	movw	%ax, 12(%rbx)
	jne	.L169
	addq	$152, %rbx
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L171:
	movl	%r15d, %edx
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15CurrencySymbols13getPluralNameENS_14StandardPlural4FormER10UErrorCode@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	addl	$1, %r15d
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	addq	$64, %rbx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	$6, %r15d
	jne	.L171
.L169:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L175
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L175:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2850:
	.size	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherC2ERKNS_6number4impl15CurrencySymbolsERKNS_20DecimalFormatSymbolsEiR10UErrorCode, .-_ZN6icu_678numparse4impl23CombinedCurrencyMatcherC2ERKNS_6number4impl15CurrencySymbolsERKNS_20DecimalFormatSymbolsEiR10UErrorCode
	.globl	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherC1ERKNS_6number4impl15CurrencySymbolsERKNS_20DecimalFormatSymbolsEiR10UErrorCode
	.set	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherC1ERKNS_6number4impl15CurrencySymbolsERKNS_20DecimalFormatSymbolsEiR10UErrorCode,_ZN6icu_678numparse4impl23CombinedCurrencyMatcherC2ERKNS_6number4impl15CurrencySymbolsERKNS_20DecimalFormatSymbolsEiR10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_678numparse4impl23CombinedCurrencyMatcherE
	.section	.rodata._ZTSN6icu_678numparse4impl23CombinedCurrencyMatcherE,"aG",@progbits,_ZTSN6icu_678numparse4impl23CombinedCurrencyMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl23CombinedCurrencyMatcherE, @object
	.size	_ZTSN6icu_678numparse4impl23CombinedCurrencyMatcherE, 49
_ZTSN6icu_678numparse4impl23CombinedCurrencyMatcherE:
	.string	"N6icu_678numparse4impl23CombinedCurrencyMatcherE"
	.weak	_ZTIN6icu_678numparse4impl23CombinedCurrencyMatcherE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl23CombinedCurrencyMatcherE,"awG",@progbits,_ZTIN6icu_678numparse4impl23CombinedCurrencyMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl23CombinedCurrencyMatcherE, @object
	.size	_ZTIN6icu_678numparse4impl23CombinedCurrencyMatcherE, 56
_ZTIN6icu_678numparse4impl23CombinedCurrencyMatcherE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl23CombinedCurrencyMatcherE
	.long	0
	.long	2
	.quad	_ZTIN6icu_678numparse4impl18NumberParseMatcherE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE
	.section	.data.rel.ro.local._ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE,"awG",@progbits,_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE, @object
	.size	_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE, 72
_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl23CombinedCurrencyMatcherE
	.quad	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD1Ev
	.quad	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherD0Ev
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher9smokeTestERKNS_13StringSegmentE
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.quad	_ZNK6icu_678numparse4impl23CombinedCurrencyMatcher8toStringEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
