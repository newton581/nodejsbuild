	.file	"double-conversion-string-to-double.cpp"
	.text
	.p2align 4
	.type	_ZN6icu_6717double_conversionL12isWhitespaceEi, @function
_ZN6icu_6717double_conversionL12isWhitespaceEi:
.LFB403:
	.cfi_startproc
	cmpl	$127, %edi
	jg	.L2
	cmpl	$32, %edi
	je	.L10
	cmpl	$13, %edi
	je	.L10
	cmpl	$10, %edi
	je	.L10
	cmpl	$9, %edi
	je	.L10
	cmpl	$11, %edi
	je	.L10
	cmpl	$12, %edi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %rax
	leaq	40(%rax), %rcx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$2, %rax
	cmpq	%rcx, %rax
	je	.L13
.L4:
	movzwl	(%rax), %edx
	cmpl	%edi, %edx
	jne	.L14
.L10:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE403:
	.size	_ZN6icu_6717double_conversionL12isWhitespaceEi, .-_ZN6icu_6717double_conversionL12isWhitespaceEi
	.p2align 4
	.type	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0, @function
_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0:
.LFB448:
	.cfi_startproc
	movq	(%rdi), %rax
	movsbl	(%rax), %r8d
	leal	-48(%r8), %r9d
	cmpl	$9, %r9d
	ja	.L16
	leal	47(%rdx), %r9d
	cmpl	%r9d, %r8d
	jg	.L16
.L17:
	leaq	1(%rax), %r8
	movq	%r8, (%rdi)
	movq	(%rcx), %r9
	cmpq	%r9, %r8
	je	.L25
.L53:
	leaq	2(%rax), %r10
	cmpq	%r10, %r9
	je	.L26
	movsbl	1(%rax), %r11d
	movzwl	%si, %esi
	cmpl	%esi, %r11d
	je	.L52
.L21:
	cmpq	%r8, %r9
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	cmpl	$10, %edx
	setg	%r9b
	cmpl	$96, %r8d
	jle	.L18
	testb	%r9b, %r9b
	je	.L18
	leal	86(%rdx), %r9d
	cmpl	%r9d, %r8d
	jle	.L17
.L19:
	addq	$1, %rax
	movq	%rax, (%rdi)
	cmpq	%rax, (%rcx)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	cmpl	$64, %r8d
	jle	.L19
	testb	%r9b, %r9b
	je	.L19
	leal	54(%rdx), %r9d
	cmpl	%r9d, %r8d
	jg	.L19
	leaq	1(%rax), %r8
	movq	%r8, (%rdi)
	movq	(%rcx), %r9
	cmpq	%r9, %r8
	jne	.L53
.L25:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	movsbl	2(%rax), %esi
	leal	-48(%rsi), %eax
	cmpl	$9, %eax
	ja	.L22
	leal	47(%rdx), %eax
	cmpl	%eax, %esi
	jle	.L23
.L22:
	cmpl	$10, %edx
	setg	%al
	cmpl	$96, %esi
	jle	.L24
	testb	%al, %al
	je	.L24
	addl	$86, %edx
	cmpl	%edx, %esi
	jg	.L21
.L23:
	movq	%r10, (%rdi)
	movq	%r10, %r8
	movq	(%rcx), %r9
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L24:
	cmpl	$64, %esi
	jle	.L21
	testb	%al, %al
	je	.L21
	leal	54(%rdx), %eax
	cmpl	%eax, %esi
	jle	.L23
	jmp	.L21
	.cfi_endproc
.LFE448:
	.size	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0, .-_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0
	.set	_ZN6icu_6717double_conversionL7AdvanceIPcEEbPT_tiRS3_.part.0,_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0
	.section	.text._ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0,"axG",@progbits,_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKcEEdT_ibPi,comdat
	.p2align 4
	.type	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0, @function
_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0:
.LFB459:
	.cfi_startproc
	movq	(%rdi), %rax
	movsbl	(%rax), %r8d
	leal	-48(%r8), %r9d
	movl	%r8d, %ecx
	cmpb	$9, %r9b
	jbe	.L55
	cmpl	$96, %r8d
	jg	.L64
	subl	$65, %ecx
	cmpb	$5, %cl
	jbe	.L55
.L57:
	addq	$1, %rax
	movq	%rax, (%rdi)
	cmpq	%rax, (%rdx)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	cmpl	$102, %r8d
	jg	.L57
.L55:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rdi)
	movq	(%rdx), %r8
	cmpq	%r8, %rcx
	je	.L62
	leaq	2(%rax), %r9
	cmpq	%r9, %r8
	je	.L63
	movsbl	1(%rax), %r10d
	movzwl	%si, %esi
	cmpl	%esi, %r10d
	je	.L65
.L59:
	cmpq	%r8, %rcx
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	movzbl	2(%rax), %eax
	leal	-48(%rax), %esi
	cmpb	$9, %sil
	jbe	.L60
	movsbl	%al, %esi
	cmpb	$96, %al
	jle	.L61
	cmpl	$102, %esi
	jg	.L59
.L60:
	movq	%r9, (%rdi)
	movq	%r9, %rcx
	movq	(%rdx), %r8
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L59
	jmp	.L60
	.cfi_endproc
.LFE459:
	.size	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0, .-_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
	.section	.text._ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0,"axG",@progbits,_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKtEEdT_ibPi,comdat
	.p2align 4
	.type	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0, @function
_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0:
.LFB461:
	.cfi_startproc
	movq	(%rdi), %rax
	movzwl	(%rax), %r8d
	leal	-48(%r8), %r9d
	movl	%r8d, %ecx
	cmpw	$9, %r9w
	jbe	.L67
	cmpl	$96, %r8d
	jg	.L76
	subl	$65, %ecx
	cmpw	$5, %cx
	jbe	.L67
.L69:
	addq	$2, %rax
	movq	%rax, (%rdi)
	cmpq	%rax, (%rdx)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	cmpl	$102, %r8d
	jg	.L69
.L67:
	leaq	2(%rax), %rcx
	movq	%rcx, (%rdi)
	movq	(%rdx), %r8
	cmpq	%r8, %rcx
	je	.L74
	leaq	4(%rax), %r9
	cmpq	%r9, %r8
	je	.L75
	cmpw	%si, 2(%rax)
	je	.L77
.L71:
	cmpq	%r8, %rcx
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movzwl	4(%rax), %eax
	leal	-48(%rax), %esi
	cmpw	$9, %si
	jbe	.L72
	movzwl	%ax, %esi
	cmpl	$96, %esi
	jle	.L73
	cmpl	$102, %esi
	jg	.L71
.L72:
	movq	%r9, (%rdi)
	movq	%r9, %rcx
	movq	(%rdx), %r8
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	subl	$65, %eax
	cmpw	$5, %ax
	ja	.L71
	jmp	.L72
	.cfi_endproc
.LFE461:
	.size	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0, .-_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
	.section	.text._ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0,"axG",@progbits,_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKtEEdT_ibPi,comdat
	.p2align 4
	.type	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0, @function
_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0:
.LFB449:
	.cfi_startproc
	movq	(%rdi), %rax
	movzwl	(%rax), %r8d
	leal	-48(%r8), %r9d
	cmpl	$9, %r9d
	ja	.L79
	leal	47(%rdx), %r9d
	cmpl	%r9d, %r8d
	jg	.L79
.L80:
	leaq	2(%rax), %r8
	movq	%r8, (%rdi)
	movq	(%rcx), %r9
	cmpq	%r9, %r8
	je	.L88
.L116:
	leaq	4(%rax), %r10
	cmpq	%r10, %r9
	je	.L89
	cmpw	%si, 2(%rax)
	je	.L115
.L84:
	cmpq	%r8, %r9
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	cmpl	$10, %edx
	setg	%r9b
	cmpl	$96, %r8d
	jle	.L81
	testb	%r9b, %r9b
	je	.L81
	leal	86(%rdx), %r9d
	cmpl	%r9d, %r8d
	jle	.L80
.L82:
	addq	$2, %rax
	movq	%rax, (%rdi)
	cmpq	%rax, (%rcx)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	cmpl	$64, %r8d
	jle	.L82
	testb	%r9b, %r9b
	je	.L82
	leal	54(%rdx), %r9d
	cmpl	%r9d, %r8d
	jg	.L82
	leaq	2(%rax), %r8
	movq	%r8, (%rdi)
	movq	(%rcx), %r9
	cmpq	%r9, %r8
	jne	.L116
.L88:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	movzwl	4(%rax), %eax
	leal	-48(%rax), %esi
	cmpl	$9, %esi
	ja	.L85
	leal	47(%rdx), %esi
	cmpl	%esi, %eax
	jle	.L86
.L85:
	cmpl	$10, %edx
	setg	%sil
	cmpl	$96, %eax
	jle	.L87
	testb	%sil, %sil
	je	.L87
	addl	$86, %edx
	cmpl	%edx, %eax
	jg	.L84
.L86:
	movq	%r10, (%rdi)
	movq	%r10, %r8
	movq	(%rcx), %r9
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L87:
	cmpl	$64, %eax
	jle	.L84
	testb	%sil, %sil
	je	.L84
	addl	$54, %edx
	cmpl	%edx, %eax
	jle	.L86
	jmp	.L84
	.cfi_endproc
.LFE449:
	.size	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0, .-_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0
	.text
	.p2align 4
	.type	_ZN6icu_6717double_conversionL17RadixStringToIeeeILi3EPcEEdPT0_S3_btbbdbPb.constprop.0, @function
_ZN6icu_6717double_conversionL17RadixStringToIeeeILi3EPcEEdPT0_S3_btbbdbPb.constprop.0:
.LFB460:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movzwl	%cx, %ebx
	subq	$40, %rsp
	movq	16(%rbp), %r8
	movq	(%rdi), %rax
	cmpb	$1, %r9b
	sbbl	%r13d, %r13d
	movq	%rsi, -56(%rbp)
	movb	$1, (%r8)
	movsbl	(%rax), %edx
	andl	$-29, %r13d
	addl	$53, %r13d
	cmpb	$48, %dl
	jne	.L119
	movzwl	%cx, %r10d
	testw	%cx, %cx
	jne	.L125
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L212:
	leaq	2(%rax), %r9
	movsbl	1(%rax), %edx
	cmpq	%r9, %rsi
	je	.L169
	cmpl	%edx, %r10d
	je	.L211
	movq	%rcx, %rax
.L123:
	movsbl	(%rax), %edx
	cmpb	$48, %dl
	jne	.L124
.L125:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rdi)
	cmpq	%rcx, %rsi
	jne	.L212
.L121:
	movb	$0, (%r8)
	pxor	%xmm0, %xmm0
	testb	%r14b, %r14b
	je	.L181
.L154:
	movsd	.LC2(%rip), %xmm0
.L181:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	addq	$1, %rax
	movq	%rax, (%rdi)
	cmpq	%rsi, %rax
	je	.L121
	movsbl	(%rax), %edx
	cmpb	$48, %dl
	je	.L120
.L126:
	leal	-48(%rdx), %r10d
	movq	-56(%rbp), %rsi
	xorl	%r9d, %r9d
	cmpb	$7, %r10b
	ja	.L128
.L214:
	subl	$48, %edx
	movl	%r13d, %ecx
	movslq	%edx, %rdx
	leaq	(%rdx,%r9,8), %r9
	movq	%r9, %rbx
	sarq	%cl, %rbx
	movl	%ebx, %edx
	testl	%ebx, %ebx
	jne	.L213
	addq	$1, %rax
	movq	%rax, (%rdi)
	cmpq	%rsi, %rax
	je	.L130
	movsbl	(%rax), %edx
	leal	-48(%rdx), %r10d
	cmpb	$7, %r10b
	ja	.L128
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L211:
	movzbl	2(%rax), %edx
	movq	%rcx, %rax
	subl	$48, %edx
	cmpb	$7, %dl
	ja	.L123
	movq	%r9, (%rdi)
	movq	%r9, %rax
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%rcx, %rax
	cmpb	$48, %dl
	je	.L125
	.p2align 4,,10
	.p2align 3
.L124:
	xorl	%r9d, %r9d
.L131:
	leal	-48(%rdx), %ecx
	cmpb	$7, %cl
	jbe	.L215
.L128:
	testb	%r15b, %r15b
	je	.L216
.L130:
	movb	$0, (%r8)
	testb	%r14b, %r14b
	je	.L163
	testq	%r9, %r9
	je	.L154
	negq	%r9
.L163:
	addq	$40, %rsp
	pxor	%xmm0, %xmm0
	popq	%rbx
	cvtsi2sdq	%r9, %xmm0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movq	-56(%rbp), %rsi
	cmpq	%rax, %rsi
	je	.L130
	.p2align 4,,10
	.p2align 3
.L135:
	movzbl	(%rax), %edx
	leal	-9(%rdx), %ecx
	cmpb	$4, %cl
	jbe	.L134
	cmpb	$32, %dl
	jne	.L181
.L134:
	addq	$1, %rax
	movq	%rax, (%rdi)
	cmpq	%rax, %rsi
	jne	.L135
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L215:
	subl	$48, %edx
	movl	%r13d, %ecx
	movslq	%edx, %rdx
	leaq	(%rdx,%r9,8), %r9
	movq	%r9, %rsi
	sarq	%cl, %rsi
	movq	%rsi, %rcx
	movl	%esi, %edx
	movl	%ebx, %esi
	testl	%ecx, %ecx
	jne	.L129
	leaq	-56(%rbp), %rcx
	movl	$8, %edx
	movq	%r8, 16(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZN6icu_6717double_conversionL7AdvanceIPcEEbPT_tiRS3_.part.0
	movq	-64(%rbp), %r9
	movq	16(%rbp), %r8
	testb	%al, %al
	jne	.L130
	movq	(%rdi), %rax
	movsbl	(%rax), %edx
	jmp	.L131
.L119:
	testw	%cx, %cx
	jne	.L124
	jmp	.L126
.L129:
	cmpl	$1, %ecx
	jle	.L217
.L165:
	movl	$1, %ebx
.L136:
	sarl	%edx
	movl	%ebx, %r10d
	addl	$1, %ebx
	cmpl	$1, %edx
	jne	.L136
	movl	%ebx, %ecx
	sall	%cl, %edx
	movl	%edx, %r11d
	subl	$1, %r11d
	andl	%r9d, %r11d
	sarq	%cl, %r9
	testw	%r12w, %r12w
	je	.L218
.L166:
	movl	%r10d, -64(%rbp)
	movl	$1, %r12d
	leaq	-56(%rbp), %rcx
	movl	%r14d, -68(%rbp)
	movl	%r13d, %r14d
	movl	%esi, %r13d
	movl	%r15d, -72(%rbp)
	movq	%r9, %r15
.L139:
	movl	$8, %edx
	movl	%r13d, %esi
	movq	%r8, 16(%rbp)
	movl	%r11d, -76(%rbp)
	call	_ZN6icu_6717double_conversionL7AdvanceIPcEEbPT_tiRS3_.part.0
	movl	-76(%rbp), %r11d
	movq	16(%rbp), %r8
	testb	%al, %al
	jne	.L206
	movq	(%rdi), %rax
	movzbl	(%rax), %eax
	leal	-48(%rax), %edx
	cmpb	$7, %dl
	jbe	.L219
.L206:
	movl	%r14d, %r13d
	movq	%r15, %r9
	movl	-64(%rbp), %r10d
	movl	-68(%rbp), %r14d
	movl	-72(%rbp), %r15d
.L138:
	testb	%r15b, %r15b
	jne	.L144
	movq	-56(%rbp), %rcx
	movq	(%rdi), %rax
	cmpq	%rax, %rcx
	jne	.L147
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L174:
	addq	$1, %rax
	movq	%rax, (%rdi)
	cmpq	%rax, %rcx
	je	.L144
.L147:
	movzbl	(%rax), %edx
	leal	-9(%rdx), %esi
	cmpb	$4, %sil
	jbe	.L174
	cmpb	$32, %dl
	jne	.L181
	jmp	.L174
.L144:
	movl	$1, %eax
	movl	%r10d, %ecx
	sall	%cl, %eax
	cmpl	%r11d, %eax
	jl	.L175
	je	.L220
.L148:
	btq	%r13, %r9
	jnc	.L150
	addl	$1, %ebx
	sarq	%r9
.L150:
	movb	$0, (%r8)
	testq	%r9, %r9
	je	.L221
	movabsq	$9007199254740991, %rax
	cmpq	%rax, %r9
	jbe	.L155
	.p2align 4,,10
	.p2align 3
.L156:
	shrq	%r9
	addl	$1, %ebx
	cmpq	%rax, %r9
	ja	.L156
.L155:
	cmpl	$971, %ebx
	jg	.L173
	movabsq	$4503599627370496, %rax
	.p2align 4,,10
	.p2align 3
.L160:
	testq	%rax, %r9
	jne	.L222
	subl	$1, %ebx
	addq	%r9, %r9
	cmpl	$-1074, %ebx
	jne	.L160
	movabsq	$4503599627370496, %rdx
	andq	%r9, %rax
	cmovne	%rdx, %rax
.L161:
	movabsq	$4503599627370495, %rdx
	andq	%rdx, %r9
	orq	%rax, %r9
	movq	%r9, %xmm0
.L157:
	testb	%r14b, %r14b
	je	.L181
	xorpd	.LC3(%rip), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L222:
	.cfi_restore_state
	leal	1075(%rbx), %eax
	salq	$52, %rax
	jmp	.L161
.L173:
	movsd	.LC0(%rip), %xmm0
	jmp	.L157
.L220:
	testb	$1, %r9b
	jne	.L175
	testb	%r12b, %r12b
	jne	.L148
.L175:
	addq	$1, %r9
	jmp	.L148
.L213:
	cmpl	$1, %ebx
	jle	.L164
	xorl	%esi, %esi
	jmp	.L165
.L218:
	movq	-56(%rbp), %rsi
.L137:
	movl	$1, %r12d
.L140:
	addq	$1, %rax
	movq	%rax, (%rdi)
	cmpq	%rsi, %rax
	je	.L138
	movzbl	(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpb	$7, %cl
	ja	.L138
	cmpb	$48, %dl
	sete	%dl
	addl	$3, %ebx
	andl	%edx, %r12d
	jmp	.L140
.L219:
	cmpb	$48, %al
	sete	%al
	addl	$3, %ebx
	andl	%eax, %r12d
	jmp	.L139
.L217:
	movl	%r9d, %r11d
	xorl	%r10d, %r10d
	sarq	%r9
	movl	$1, %ebx
	andl	$1, %r11d
	jmp	.L166
.L221:
	testb	%r14b, %r14b
	jne	.L154
	jmp	.L163
.L164:
	movl	%r9d, %r11d
	movl	$1, %ebx
	sarq	%r9
	xorl	%r10d, %r10d
	andl	$1, %r11d
	jmp	.L137
	.cfi_endproc
.LFE460:
	.size	_ZN6icu_6717double_conversionL17RadixStringToIeeeILi3EPcEEdPT0_S3_btbbdbPb.constprop.0, .-_ZN6icu_6717double_conversionL17RadixStringToIeeeILi3EPcEEdPT0_S3_btbbdbPb.constprop.0
	.section	.text._ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKcEEdT_ibPi,"axG",@progbits,_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKcEEdT_ibPi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKcEEdT_ibPi
	.type	_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKcEEdT_ibPi, @function
_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKcEEdT_ibPi:
.LFB427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	addq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$904, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -896(%rbp)
	movq	%r8, -888(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, (%r8)
	movl	(%rdi), %r14d
	movq	%rsi, -880(%rbp)
	movl	%r14d, %r13d
	movl	%r14d, %r11d
	movq	%rdx, -872(%rbp)
	andl	$8, %r13d
	andl	$32, %r11d
	cmpq	%rsi, %rdx
	je	.L697
	movq	%rsi, %rbx
	testb	$24, %r14b
	je	.L483
	movq	%rsi, %r8
	movq	%rsi, %rax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L229:
	movzbl	(%rax), %esi
	movl	%ecx, %edi
	leal	-9(%rsi), %ecx
	cmpb	$4, %cl
	setbe	%cl
	cmpb	$32, %sil
	sete	%sil
	orb	%sil, %cl
	jne	.L700
	testb	%dil, %dil
	je	.L230
	movq	%r8, -880(%rbp)
.L230:
	cmpq	%rax, %rbx
	je	.L226
	testl	%r13d, %r13d
	jne	.L226
.L232:
	movsd	16(%r12), %xmm0
.L223:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L701
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	movq	%rsi, %rax
.L226:
	movzbl	(%rax), %esi
	leal	-43(%rsi), %ecx
	andl	$253, %ecx
	jne	.L484
	leaq	1(%rax), %r8
	movq	%r8, -880(%rbp)
	cmpq	%r8, %rdx
	je	.L232
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L235:
	movzbl	(%rax), %ecx
	leal	-9(%rcx), %edi
	cmpb	$4, %dil
	jbe	.L503
	cmpb	$32, %cl
	je	.L503
	testl	%r11d, %r11d
	jne	.L236
	cmpq	%rax, %r8
	jne	.L232
.L236:
	cmpb	$45, %sil
	movq	%rax, -880(%rbp)
	movzbl	(%rax), %esi
	sete	-905(%rbp)
.L231:
	movl	%r14d, %edi
	movq	24(%r12), %rcx
	andl	$4, %edi
	movl	%edi, -904(%rbp)
	movl	%r14d, %edi
	andl	$64, %edi
	testq	%rcx, %rcx
	je	.L237
	testl	%edi, %edi
	jne	.L249
	cmpb	%sil, (%rcx)
	je	.L702
	movq	32(%r12), %rcx
	testq	%rcx, %rcx
	je	.L248
.L468:
	cmpb	%sil, (%rcx)
	je	.L703
.L248:
	movl	-904(%rbp), %edi
	testl	%edi, %edi
	movl	%r14d, %edi
	setne	-924(%rbp)
	andl	$16, %edi
	movl	%edi, -932(%rbp)
	cmpb	$48, %sil
	je	.L704
	movb	$1, -906(%rbp)
	xorl	%r8d, %r8d
	movb	$0, -907(%rbp)
.L258:
	xorl	%r15d, %r15d
	xorl	%edi, %edi
	movl	%r14d, -928(%rbp)
	xorl	%r13d, %r13d
	movq	%rbx, -920(%rbp)
	leaq	-872(%rbp), %rcx
	movl	%r15d, %ebx
	movl	%edi, %r14d
	movl	%r8d, %r15d
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L706:
	movslq	%ebx, %rdx
	addl	$1, %ebx
	movb	%sil, -848(%rbp,%rdx)
.L410:
	testb	%r15b, %r15b
	je	.L411
	cmpb	$55, (%rax)
	setle	%r15b
.L411:
	movzwl	40(%r12), %esi
	testw	%si, %si
	je	.L705
	leaq	-880(%rbp), %rdi
	movl	$10, %edx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0
	testb	%al, %al
	jne	.L682
.L414:
	movq	-880(%rbp), %rax
	movzbl	(%rax), %esi
.L415:
	leal	-48(%rsi), %edx
	cmpb	$9, %dl
	ja	.L408
	cmpl	$771, %ebx
	jle	.L706
	addl	$1, %r13d
	cmpb	$48, %sil
	setne	%dl
	orl	%edx, %r14d
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L700:
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L228
	movq	%rax, %r8
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L503:
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L235
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L705:
	addq	$1, %rax
	movq	%rax, -880(%rbp)
	cmpq	-872(%rbp), %rax
	jne	.L414
.L682:
	movb	%r14b, -912(%rbp)
	movl	%r15d, %r8d
	movl	%ebx, %r15d
	movq	-920(%rbp), %rbx
.L413:
	testb	%r8b, %r8b
	jne	.L421
.L425:
	cmpb	$0, -912(%rbp)
	je	.L461
	movslq	%r15d, %rax
	subl	$1, %r13d
	addl	$1, %r15d
	movb	$49, -848(%rbp,%rax)
.L461:
	movslq	%r15d, %rax
	cmpb	$0, -896(%rbp)
	movl	%r15d, %esi
	movl	%r13d, %edx
	movb	$0, -848(%rbp,%rax)
	leaq	-848(%rbp), %rdi
	je	.L462
	call	_ZN6icu_6717double_conversion6StrtodENS0_6VectorIKcEEi@PLT
.L463:
	movq	-880(%rbp), %rax
	subq	%rbx, %rax
	movq	-888(%rbp), %rbx
	cmpb	$0, -905(%rbp)
	movl	%eax, (%rbx)
	je	.L223
	xorpd	.LC3(%rip), %xmm0
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L228:
	movq	-888(%rbp), %rax
	subq	%rbx, %rdx
	movl	%edx, (%rax)
.L697:
	movsd	8(%r12), %xmm0
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L484:
	movb	$0, -905(%rbp)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L237:
	movq	32(%r12), %rcx
	testq	%rcx, %rcx
	je	.L248
	testl	%edi, %edi
	je	.L468
.L249:
	call	abort@PLT
	.p2align 4,,10
	.p2align 3
.L408:
	movl	%r15d, %r8d
	movl	%ebx, %r15d
	movb	%r14b, -912(%rbp)
	movq	-920(%rbp), %rbx
	movl	-928(%rbp), %r14d
	testl	%r15d, %r15d
	jne	.L416
	cmpb	$46, %sil
	je	.L417
	xorl	%r9d, %r9d
	cmpb	$0, -907(%rbp)
	je	.L232
.L438:
	andl	$-33, %esi
	cmpb	$69, %sil
	je	.L478
.L695:
	xorl	%r8d, %r8d
.L467:
	andl	$20, %r14d
	jne	.L452
	cmpq	%rax, -872(%rbp)
	jne	.L232
	movl	-904(%rbp), %edx
	testl	%edx, %edx
	je	.L455
.L454:
	addl	%r9d, %r13d
	movl	-932(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L413
	movq	-872(%rbp), %rsi
	cmpq	%rax, %rsi
	jne	.L460
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L513:
	addq	$1, %rax
	movq	%rax, -880(%rbp)
	cmpq	%rax, %rsi
	je	.L413
.L460:
	movzbl	(%rax), %edx
	leal	-9(%rdx), %ecx
	cmpb	$4, %cl
	jbe	.L513
	cmpb	$32, %dl
	jne	.L413
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L704:
	movzwl	40(%r12), %esi
	testw	%si, %si
	je	.L707
	leaq	-872(%rbp), %rcx
	leaq	-880(%rbp), %rdi
	movl	$10, %edx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0
	movb	%al, -906(%rbp)
	movq	-880(%rbp), %rax
.L260:
	cmpb	$0, -906(%rbp)
	jne	.L692
	movl	(%r12), %edi
	movzbl	(%rax), %esi
	testb	$-127, %dil
	je	.L263
	movl	%esi, %edx
	andl	$-33, %edx
	cmpb	$88, %dl
	jne	.L263
	movq	-872(%rbp), %rcx
	leaq	1(%rax), %rdx
	movq	%rdx, -880(%rbp)
	cmpq	%rcx, %rdx
	je	.L232
	andl	$128, %edi
	jne	.L708
.L265:
	movzbl	(%rdx), %eax
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	jbe	.L490
	movsbl	%al, %ecx
	cmpb	$96, %al
	jle	.L312
	cmpl	$102, %ecx
	jg	.L232
.L490:
	movzbl	-924(%rbp), %r9d
	xorl	%r13d, %r13d
.L311:
	cmpb	$1, -896(%rbp)
	movq	-872(%rbp), %rdi
	sbbl	%eax, %eax
	movzwl	40(%r12), %r15d
	movsd	16(%r12), %xmm0
	andl	$-29, %eax
	movq	%rdi, -856(%rbp)
	addl	$53, %eax
	movl	%r15d, %r12d
	movl	%eax, -920(%rbp)
	movzbl	(%rdx), %eax
	cmpb	$48, %al
	jne	.L324
	testw	%r15w, %r15w
	jne	.L325
	jmp	.L317
.L710:
	leaq	2(%rdx), %rsi
	movsbl	1(%rdx), %eax
	cmpq	%rsi, %rdi
	je	.L492
	cmpl	%eax, %r15d
	je	.L709
.L493:
	movq	%rcx, %rdx
.L320:
	movzbl	(%rdx), %eax
.L319:
	cmpb	$48, %al
	jne	.L324
.L325:
	leaq	1(%rdx), %rcx
	movq	%rcx, -880(%rbp)
	cmpq	%rcx, %rdi
	jne	.L710
.L318:
	cmpb	$0, -905(%rbp)
	jne	.L711
	movq	-880(%rbp), %rax
	pxor	%xmm0, %xmm0
.L389:
	movl	-932(%rbp), %esi
	testl	%esi, %esi
	je	.L399
	movq	-872(%rbp), %rsi
	cmpq	%rax, %rsi
	je	.L399
.L401:
	movzbl	(%rax), %edx
	leal	-9(%rdx), %ecx
	cmpb	$4, %cl
	jbe	.L510
	cmpb	$32, %dl
	je	.L510
.L399:
	subq	%rbx, %rax
	movq	-888(%rbp), %rbx
	movl	%eax, (%rbx)
	jmp	.L223
.L720:
	movq	-920(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L692:
	subq	%rbx, %rax
	movq	-888(%rbp), %rbx
	cmpb	$0, -905(%rbp)
	pxor	%xmm0, %xmm0
	movl	%eax, (%rbx)
	je	.L223
	movsd	.LC2(%rip), %xmm0
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L462:
	call	_ZN6icu_6717double_conversion6StrtofENS0_6VectorIKcEEi@PLT
	cvtss2sd	%xmm0, %xmm0
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L416:
	cmpb	$46, %sil
	je	.L712
	andl	$-33, %esi
	xorl	%r9d, %r9d
	cmpb	$69, %sil
	jne	.L467
	movl	-904(%rbp), %edx
	testl	%edx, %edx
	jne	.L511
	testb	%r8b, %r8b
	jne	.L232
.L662:
	xorl	%r9d, %r9d
.L478:
	movq	-872(%rbp), %r11
	leaq	1(%rax), %rdx
	movq	%rdx, -880(%rbp)
	cmpq	%r11, %rdx
	je	.L476
	movzbl	1(%rax), %ecx
	leal	-43(%rcx), %edi
	movl	%ecx, %esi
	andl	$253, %edi
	jne	.L500
	leaq	2(%rax), %rdx
	movq	%rdx, -880(%rbp)
	cmpq	%rdx, %r11
	je	.L476
	movzbl	2(%rax), %esi
	movl	%ecx, %r8d
	movl	%esi, %ecx
.L443:
	subl	$48, %esi
	cmpb	$9, %sil
	jbe	.L713
.L476:
	movl	-904(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L232
	movq	%rax, -880(%rbp)
	addl	%r9d, %r13d
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L263:
	leaq	-872(%rbp), %rcx
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L403:
	leaq	-880(%rbp), %rdi
	movl	$10, %edx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0
	movl	%eax, %r8d
	movq	-880(%rbp), %rax
	testb	%r8b, %r8b
	jne	.L692
.L405:
	movzbl	(%rax), %esi
.L406:
	cmpb	$48, %sil
	jne	.L402
	movzwl	40(%r12), %esi
	testw	%si, %si
	jne	.L403
	addq	$1, %rax
	movq	%rax, -880(%rbp)
	cmpq	-872(%rbp), %rax
	jne	.L405
	jmp	.L692
.L712:
	movl	-904(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L420
	testb	%r8b, %r8b
	jne	.L232
.L417:
	movzwl	40(%r12), %edx
	testw	%dx, %dx
	je	.L714
	movzwl	%dx, %esi
	leaq	-872(%rbp), %rcx
	leaq	-880(%rbp), %rdi
	movl	$10, %edx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0
.L423:
	testb	%al, %al
	je	.L424
	testl	%r15d, %r15d
	jne	.L425
	cmpb	$0, -906(%rbp)
	jne	.L232
	jmp	.L425
.L317:
	addq	$1, %rdx
	movq	%rdx, -880(%rbp)
	cmpq	%rdx, %rdi
	je	.L318
	movzbl	(%rdx), %eax
	cmpb	$48, %al
	je	.L317
.L324:
	movl	%r15d, %ecx
	movl	%r12d, %r15d
	movb	%r9b, -912(%rbp)
	xorl	%r8d, %r8d
	movl	%ecx, %r12d
	movsbl	%al, %ecx
	leaq	-856(%rbp), %r10
	movq	%rdx, %rdi
	leal	-48(%rcx), %r9d
	xorl	%r14d, %r14d
	xorl	%r11d, %r11d
	movq	%r10, %rdx
	movq	%rbx, -896(%rbp)
	movl	%r8d, %ebx
	cmpl	$9, %r9d
	ja	.L328
.L718:
	leal	-4(%r14), %eax
	testb	%bl, %bl
	movl	%r9d, %ecx
	cmovne	%eax, %r14d
.L330:
	movslq	%ecx, %rcx
	salq	$4, %r11
	addq	%rcx, %r11
	movzbl	-920(%rbp), %ecx
	movq	%r11, %rax
	sarq	%cl, %rax
	testl	%eax, %eax
	jne	.L715
	testw	%r15w, %r15w
	je	.L716
	leaq	-880(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
.L371:
	testb	%al, %al
	jne	.L717
.L341:
	movq	-880(%rbp), %rdi
	movzbl	(%rdi), %eax
	movsbl	%al, %ecx
	leal	-48(%rcx), %r9d
	cmpl	$9, %r9d
	jbe	.L718
.L328:
	cmpl	$96, %ecx
	jle	.L653
	cmpl	$102, %ecx
	jg	.L719
	subl	$87, %ecx
	leal	-4(%r14), %eax
	testb	%bl, %bl
	cmovne	%eax, %r14d
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L707:
	addq	$1, %rax
	cmpq	%rax, %rdx
	movq	%rax, -880(%rbp)
	sete	-906(%rbp)
	jmp	.L260
.L511:
	testb	%r8b, %r8b
	je	.L662
.L421:
	leaq	-848(%rbp), %rax
	movslq	%r15d, %rsi
	subq	$8, %rsp
	movsd	16(%r12), %xmm0
	addq	%rax, %rsi
	movq	%rax, -856(%rbp)
	leaq	-864(%rbp), %rax
	movzwl	40(%r12), %ecx
	movl	-924(%rbp), %r8d
	pushq	%rax
	leaq	-856(%rbp), %rdi
	movzbl	-905(%rbp), %edx
	movzbl	-896(%rbp), %r9d
	andl	$1, %r8d
	call	_ZN6icu_6717double_conversionL17RadixStringToIeeeILi3EPcEEdPT0_S3_btbbdbPb.constprop.0
	movq	-880(%rbp), %rax
	subq	%rbx, %rax
	movq	-888(%rbp), %rbx
	movl	%eax, (%rbx)
	popq	%rsi
	popq	%rdi
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L702:
	cmpb	$0, 1(%rcx)
	leaq	1(%rcx), %rsi
	je	.L240
	leaq	1(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%rcx, -880(%rbp)
	movq	%rcx, %rax
	cmpq	%rcx, %rdx
	je	.L232
	movzbl	(%rsi), %edi
	cmpb	%dil, (%rcx)
	jne	.L232
	addq	$1, %rsi
	addq	$1, %rcx
	cmpb	$0, (%rsi)
	jne	.L242
.L240:
	addq	$1, %rax
	andl	$20, %r14d
	movq	%rax, -880(%rbp)
	jne	.L243
	cmpq	%rax, %rdx
	jne	.L232
.L244:
	subq	%rbx, %rax
	movq	-888(%rbp), %rbx
	cmpb	$0, -905(%rbp)
	movsd	.LC6(%rip), %xmm0
	movl	%eax, (%rbx)
	jne	.L223
	movsd	.LC0(%rip), %xmm0
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L703:
	cmpb	$0, 1(%rcx)
	leaq	1(%rcx), %rsi
	je	.L250
	leaq	1(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L252:
	movq	%rcx, -880(%rbp)
	movq	%rcx, %rax
	cmpq	%rdx, %rcx
	je	.L232
	movzbl	(%rsi), %edi
	cmpb	%dil, (%rcx)
	jne	.L232
	addq	$1, %rsi
	addq	$1, %rcx
	cmpb	$0, (%rsi)
	jne	.L252
.L250:
	addq	$1, %rax
	andl	$20, %r14d
	movq	%rax, -880(%rbp)
	jne	.L253
	cmpq	%rax, %rdx
	jne	.L232
.L254:
	subq	%rbx, %rax
	movq	-888(%rbp), %rbx
	cmpb	$0, -905(%rbp)
	movsd	.LC5(%rip), %xmm0
	movl	%eax, (%rbx)
	jne	.L223
	movsd	.LC4(%rip), %xmm0
	jmp	.L223
.L402:
	movl	(%r12), %r11d
	movb	$1, -907(%rbp)
	shrl	%r11d
	movl	%r11d, %r8d
	andl	$1, %r8d
	jmp	.L258
.L243:
	movl	-904(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L689
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L504:
	addq	$1, %rax
	movq	%rax, -880(%rbp)
.L689:
	cmpq	%rax, %rdx
	je	.L244
	movzbl	(%rax), %ecx
	leal	-9(%rcx), %esi
	cmpb	$4, %sil
	jbe	.L504
	cmpb	$32, %cl
	jne	.L232
	jmp	.L504
.L452:
	movl	-904(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L454
	movq	-872(%rbp), %rsi
	cmpq	%rax, %rsi
	je	.L455
	.p2align 4,,10
	.p2align 3
.L458:
	movzbl	(%rax), %edx
	leal	-9(%rdx), %ecx
	cmpb	$4, %cl
	jbe	.L512
	cmpb	$32, %dl
	jne	.L232
.L512:
	addq	$1, %rax
	movq	%rax, -880(%rbp)
	cmpq	%rax, %rsi
	jne	.L458
.L455:
	addl	%r9d, %r13d
	jmp	.L413
.L253:
	movl	-904(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L690
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L505:
	addq	$1, %rax
	movq	%rax, -880(%rbp)
.L690:
	cmpq	%rax, %rdx
	je	.L254
	movzbl	(%rax), %ecx
	leal	-9(%rcx), %esi
	cmpb	$4, %sil
	jbe	.L505
	cmpb	$32, %cl
	jne	.L232
	jmp	.L505
.L510:
	addq	$1, %rax
	movq	%rax, -880(%rbp)
	cmpq	%rax, %rsi
	jne	.L401
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L424:
	movq	-880(%rbp), %rax
	movzbl	(%rax), %esi
	testl	%r15d, %r15d
	jne	.L498
	cmpb	$48, %sil
	jne	.L498
	xorl	%edi, %edi
	movq	%rbx, -920(%rbp)
	leaq	-872(%rbp), %rcx
	movq	%r12, %rbx
	movl	%edi, %r12d
	jmp	.L430
.L427:
	movzwl	%dx, %esi
	leaq	-880(%rbp), %rdi
	movl	$10, %edx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0
	movl	%eax, %edx
	movq	-880(%rbp), %rax
.L428:
	testb	%dl, %dl
	jne	.L720
	movzbl	(%rax), %esi
	subl	$1, %r12d
	cmpb	$48, %sil
	jne	.L721
.L430:
	movzwl	40(%rbx), %edx
	testw	%dx, %dx
	jne	.L427
	addq	$1, %rax
	cmpq	%rax, -872(%rbp)
	movq	%rax, -880(%rbp)
	sete	%dl
	jmp	.L428
.L420:
	testb	%r8b, %r8b
	jne	.L421
	jmp	.L417
.L714:
	addq	$1, %rax
	cmpq	%rax, -872(%rbp)
	movq	%rax, -880(%rbp)
	sete	%al
	jmp	.L423
.L500:
	movl	$43, %r8d
	jmp	.L443
.L498:
	xorl	%r9d, %r9d
.L426:
	movl	%r14d, -928(%rbp)
	leaq	-872(%rbp), %rcx
	movl	%r9d, %r14d
	movl	%r13d, -920(%rbp)
	movl	%r15d, %r13d
	movq	%rbx, %r15
	movzbl	-912(%rbp), %ebx
	jmp	.L437
.L723:
	movslq	%r13d, %rdx
	subl	$1, %r14d
	addl	$1, %r13d
	movb	%sil, -848(%rbp,%rdx)
.L433:
	movzwl	40(%r12), %edx
	testw	%dx, %dx
	je	.L722
	movzwl	%dx, %esi
	leaq	-880(%rbp), %rdi
	movl	$10, %edx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0
	testb	%al, %al
	jne	.L683
.L436:
	movq	-880(%rbp), %rax
	movzbl	(%rax), %esi
.L437:
	leal	-48(%rsi), %edx
	cmpb	$9, %dl
	ja	.L431
	cmpl	$771, %r13d
	jle	.L723
	cmpb	$48, %sil
	setne	%dl
	orl	%edx, %ebx
	jmp	.L433
.L722:
	addq	$1, %rax
	movq	%rax, -880(%rbp)
	cmpq	-872(%rbp), %rax
	jne	.L436
.L683:
	movb	%bl, -912(%rbp)
	movq	%r15, %rbx
	movl	%r13d, %r15d
	movl	-920(%rbp), %r13d
	addl	%r14d, %r13d
	jmp	.L425
.L709:
	movzbl	2(%rdx), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L323
	movsbl	%al, %edx
	cmpb	$96, %al
	jg	.L322
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L493
.L323:
	movq	%rsi, -880(%rbp)
	movq	%rsi, %rdx
	jmp	.L320
.L708:
	movzwl	40(%r12), %r11d
	movq	%rcx, -864(%rbp)
	movq	%rdx, -856(%rbp)
	movl	%r11d, %r14d
	testw	%r11w, %r11w
	je	.L266
	xorl	%r13d, %r13d
	leaq	-864(%rbp), %r15
.L272:
	movsbl	(%rdx), %eax
	leal	-48(%rax), %ecx
	movl	%eax, %r10d
	cmpb	$9, %cl
	jbe	.L267
	cmpl	$96, %eax
	jg	.L268
	cmpl	$64, %eax
	jg	.L724
	cmpb	$46, %al
	jne	.L277
	leaq	-856(%rbp), %r8
	leaq	-864(%rbp), %r15
	movl	%r11d, %esi
	movq	%r15, %rdx
	movq	%r8, %rdi
	movq	%r8, -920(%rbp)
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
	testb	%al, %al
	jne	.L277
	movq	-920(%rbp), %r8
	movq	%r8, %rdi
.L281:
	movq	-856(%rbp), %rdx
	movsbl	(%rdx), %eax
	leal	-48(%rax), %ecx
	movl	%eax, %r10d
	cmpb	$9, %cl
	jbe	.L279
	cmpl	$96, %eax
	jg	.L280
	leal	-65(%rax), %eax
	cmpb	$5, %al
	jbe	.L279
.L270:
	testb	%r13b, %r13b
	je	.L277
	andl	$-33, %r10d
	cmpb	$80, %r10b
	jne	.L277
	testw	%r14w, %r14w
	je	.L725
	leaq	-864(%rbp), %rdx
	leaq	-856(%rbp), %rdi
	movl	%r11d, %esi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
.L288:
	testb	%al, %al
	jne	.L277
	movq	-856(%rbp), %rsi
	movzbl	(%rsi), %eax
	subl	$43, %eax
	testb	$-3, %al
	jne	.L296
	testw	%r14w, %r14w
	je	.L726
	leaq	-864(%rbp), %rdx
	leaq	-856(%rbp), %rdi
	movl	%r11d, %esi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
.L294:
	testb	%al, %al
	jne	.L277
	movq	-856(%rbp), %rsi
.L296:
	movsbl	(%rsi), %eax
	movl	%eax, %edx
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L277
	cmpb	$57, %dl
	jg	.L277
	testw	%r14w, %r14w
	je	.L727
	leaq	-864(%rbp), %rdx
	leaq	-856(%rbp), %rdi
	movl	%r11d, %esi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
	movl	%eax, %r9d
.L299:
	testb	%r9b, %r9b
	jne	.L306
	leaq	-864(%rbp), %rdx
.L307:
	movq	-856(%rbp), %rsi
	movzbl	(%rsi), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L301
	cmpl	$0, -904(%rbp)
	jne	.L308
	movq	-864(%rbp), %r8
	jmp	.L310
.L728:
	movsbl	(%rsi), %edi
	call	_ZN6icu_6717double_conversionL12isWhitespaceEi
	testb	%al, %al
	je	.L277
	addq	$1, %rsi
	movq	%rsi, -856(%rbp)
.L310:
	cmpq	%rsi, %r8
	jne	.L728
.L308:
	movq	-880(%rbp), %rdx
	movl	%r13d, %r9d
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L274:
	cmpl	$64, %esi
	jg	.L729
	cmpb	$46, %sil
	jne	.L277
	movq	%rax, -856(%rbp)
	cmpq	-864(%rbp), %rax
	je	.L277
	movq	-864(%rbp), %rcx
.L284:
	movq	-856(%rbp), %rdx
	movsbl	(%rdx), %eax
	leal	-48(%rax), %esi
	movl	%eax, %r10d
	cmpb	$9, %sil
	jbe	.L282
	cmpl	$96, %eax
	jle	.L283
	cmpl	$102, %eax
	jg	.L270
.L282:
	addq	$1, %rdx
	movl	$1, %r13d
	movq	%rdx, -856(%rbp)
	cmpq	%rcx, %rdx
	jne	.L284
.L277:
	movq	-880(%rbp), %rdx
	jmp	.L265
.L312:
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L232
	jmp	.L490
.L724:
	cmpl	$70, %eax
	jg	.L270
.L267:
	leaq	-856(%rbp), %rdi
	movq	%r15, %rdx
	movl	%r11d, %esi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
	testb	%al, %al
	jne	.L277
	movq	-856(%rbp), %rdx
	movl	$1, %r13d
	jmp	.L272
.L653:
	leal	-65(%rax), %r9d
	cmpb	$5, %r9b
	jbe	.L730
	testb	%r13b, %r13b
	je	.L731
	cmpb	$46, %al
	je	.L732
	movq	%rdi, %rdx
	movq	-896(%rbp), %rbx
	movl	%r12d, %edi
	movl	%r15d, %r12d
	movl	%edi, %r15d
.L339:
	andl	$-33, %eax
	cmpb	$80, %al
	je	.L342
	cmpb	$0, -924(%rbp)
	je	.L733
.L342:
	testw	%r12w, %r12w
	je	.L734
	leaq	-856(%rbp), %r8
	leaq	-880(%rbp), %rdi
	movl	%r15d, %esi
	movq	%r8, %rdx
	movq	%r8, -896(%rbp)
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
	movq	-880(%rbp), %rax
	movq	-896(%rbp), %r8
	movzbl	(%rax), %edx
	cmpb	$43, %dl
	je	.L735
	cmpb	$45, %dl
	jne	.L472
	movq	%r8, %rdx
	movl	%r15d, %esi
	movq	%r8, -896(%rbp)
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
	movb	$1, -906(%rbp)
	movq	-880(%rbp), %rax
	movq	-896(%rbp), %r8
.L472:
	xorl	%r13d, %r13d
	movq	%r8, %r12
.L382:
	movsbl	(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	jbe	.L736
.L380:
	movl	%r13d, %edx
	negl	%edx
	cmpb	$0, -906(%rbp)
	cmovne	%edx, %r13d
	addl	%r13d, %r14d
.L372:
	testl	%r14d, %r14d
	je	.L509
	testq	%r11, %r11
	je	.L509
	movabsq	$9007199254740991, %rdx
	cmpq	%rdx, %r11
	jbe	.L390
.L391:
	shrq	%r11
	addl	$1, %r14d
	cmpq	%rdx, %r11
	ja	.L391
.L390:
	movabsq	$9218868437227405312, %rdx
	cmpl	$971, %r14d
	jg	.L392
	xorl	%edx, %edx
	cmpl	$-1074, %r14d
	jl	.L392
	movl	$1, %edx
	movq	%r11, %rcx
	salq	$52, %rdx
	andq	%rdx, %rcx
	cmpl	$-1074, %r14d
	je	.L393
	testq	%rcx, %rcx
	je	.L395
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L396:
	testq	%rdx, %r11
	jne	.L394
.L395:
	subl	$1, %r14d
	addq	%r11, %r11
	cmpl	$-1074, %r14d
	jne	.L396
.L393:
	movl	$1, %edx
	salq	$52, %rdx
	andq	%r11, %rdx
	je	.L397
	movl	$-1074, %r14d
.L394:
	leal	1075(%r14), %edx
	movslq	%edx, %rdx
.L397:
	movabsq	$4503599627370495, %rcx
	salq	$52, %rdx
	andq	%rcx, %r11
	orq	%r11, %rdx
.L392:
	cmpb	$0, -905(%rbp)
	movq	%rdx, %xmm0
	je	.L389
	xorpd	.LC3(%rip), %xmm0
	jmp	.L389
.L431:
	movl	%r14d, %edi
	movb	%bl, -912(%rbp)
	movl	%r14d, %r9d
	movq	%r15, %rbx
	movl	-928(%rbp), %r14d
	movl	%r13d, %r15d
	movl	-920(%rbp), %r13d
	testl	%edi, %edi
	jne	.L438
	cmpb	$0, -906(%rbp)
	je	.L438
	testl	%r15d, %r15d
	je	.L232
	xorl	%r9d, %r9d
	jmp	.L438
.L716:
	addq	$1, %rdi
	cmpq	%rdi, -856(%rbp)
	movq	%rdi, -880(%rbp)
	sete	%al
	jmp	.L371
.L711:
	movq	-880(%rbp), %rax
.L326:
	movsd	.LC2(%rip), %xmm0
	jmp	.L389
.L492:
	movq	%rcx, %rdx
	jmp	.L319
.L268:
	cmpl	$102, %eax
	jg	.L270
	jmp	.L267
.L719:
	movq	%rdi, %rdx
	movl	%r12d, %edi
	movq	-896(%rbp), %rbx
	movl	%r15d, %r12d
	movl	%edi, %r15d
	testb	%r13b, %r13b
	jne	.L339
.L338:
	movl	-904(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L663
.L694:
	movq	-880(%rbp), %rax
	jmp	.L372
.L721:
	movl	%r12d, %r9d
	movq	%rbx, %r12
	movq	-920(%rbp), %rbx
	jmp	.L426
.L322:
	cmpl	$102, %edx
	jle	.L323
	jmp	.L493
.L713:
	addq	$1, %rdx
	xorl	%esi, %esi
	jmp	.L450
.L737:
	cmpl	$3, %eax
	jg	.L501
.L447:
	leal	(%rsi,%rsi,4), %ecx
	leal	(%rax,%rcx,2), %esi
.L448:
	movq	%rdx, -880(%rbp)
	movq	%rdx, %rax
	cmpq	%r11, %rdx
	je	.L449
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	leal	-48(%rcx), %edi
	cmpb	$9, %dil
	ja	.L449
.L450:
	movsbl	%cl, %eax
	subl	$48, %eax
	cmpl	$107374181, %esi
	jle	.L447
	cmpl	$107374182, %esi
	je	.L737
.L501:
	movl	$1073741823, %esi
	jmp	.L448
.L449:
	movl	%esi, %edx
	movl	%r9d, %edi
	negl	%edx
	cmpb	$45, %r8b
	cmove	%edx, %esi
	addl	%esi, %edi
	movl	%edi, %r9d
	jmp	.L695
.L509:
	cmpb	$0, -905(%rbp)
	je	.L388
	testq	%r11, %r11
	je	.L326
	negq	%r11
.L388:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r11, %xmm0
	jmp	.L389
.L266:
	addq	$2, %rax
	xorl	%r13d, %r13d
.L276:
	movsbl	-1(%rax), %esi
	leaq	-1(%rax), %rdx
	movq	%rax, %rdi
	leal	-48(%rsi), %r8d
	movl	%esi, %r10d
	cmpb	$9, %r8b
	jbe	.L273
	cmpl	$96, %esi
	jle	.L274
	cmpl	$102, %esi
	jg	.L270
.L273:
	movq	%rax, -856(%rbp)
	movl	$1, %r13d
	addq	$1, %rax
	cmpq	%rdi, %rcx
	jne	.L276
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L730:
	subl	$55, %ecx
	leal	-4(%r14), %eax
	testb	%bl, %bl
	cmovne	%eax, %r14d
	jmp	.L330
.L280:
	cmpl	$102, %eax
	jg	.L270
.L279:
	movq	%r15, %rdx
	movl	%r11d, %esi
	movl	$1, %r13d
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
	testb	%al, %al
	je	.L281
	jmp	.L277
.L732:
	testw	%r15w, %r15w
	je	.L738
	leaq	-880(%rbp), %rdi
	movl	%r12d, %esi
	movl	%r13d, %ebx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
	jmp	.L341
.L663:
	movq	-856(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L694
.L347:
	movzbl	(%rdx), %ecx
	leal	-9(%rcx), %esi
	cmpb	$4, %sil
	jbe	.L506
	cmpb	$32, %cl
	jne	.L223
.L506:
	addq	$1, %rdx
	movq	%rdx, -880(%rbp)
	cmpq	%rdx, %rax
	jne	.L347
.L346:
	movq	-880(%rbp), %rax
	testb	%r13b, %r13b
	je	.L372
	jmp	.L342
.L738:
	addq	$1, %rdi
	movl	%r13d, %ebx
	movq	%rdi, -880(%rbp)
	jmp	.L341
.L725:
	addq	$1, %rdx
	cmpq	%rdx, -864(%rbp)
	movq	%rdx, -856(%rbp)
	sete	%al
	jmp	.L288
.L717:
	movl	%r12d, %eax
	movq	-896(%rbp), %rbx
	movl	%r15d, %r12d
	movl	%eax, %r15d
	jmp	.L346
.L283:
	leal	-65(%rax), %eax
	cmpb	$5, %al
	ja	.L270
	jmp	.L282
.L701:
	call	__stack_chk_fail@PLT
.L729:
	cmpl	$70, %esi
	jle	.L273
	jmp	.L270
.L733:
	movq	-856(%rbp), %rax
	cmpq	%rdx, %rax
	jne	.L347
	jmp	.L342
.L715:
	movl	%r12d, %edi
	movl	%ebx, %r8d
	movl	%r15d, %r12d
	movl	$1, %ecx
	movzbl	-912(%rbp), %r9d
	movq	-896(%rbp), %rbx
	movl	%edi, %r15d
	cmpl	$1, %eax
	jle	.L349
.L350:
	sarl	%eax
	addl	$1, %ecx
	cmpl	$1, %eax
	jne	.L350
.L349:
	movl	$1, %eax
	movb	%r9b, -896(%rbp)
	movl	$1, %r10d
	addl	%ecx, %r14d
	sall	%cl, %eax
	movl	%ecx, -912(%rbp)
	leaq	-856(%rbp), %rdx
	subl	$1, %eax
	andl	%r11d, %eax
	sarq	%cl, %r11
	movl	%eax, -924(%rbp)
	movq	%rbx, %rax
	movl	%r12d, %ebx
	movl	%r15d, %r12d
	movq	%r11, -904(%rbp)
	movq	%rax, %r15
	movl	%r10d, %r11d
.L351:
	testw	%bx, %bx
	je	.L739
	leaq	-880(%rbp), %rdi
	movl	%r12d, %esi
	movb	%r8b, -928(%rbp)
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
	movzbl	-928(%rbp), %r8d
.L353:
	testb	%al, %al
	jne	.L676
	movq	-880(%rbp), %rdi
	movzbl	(%rdi), %eax
	testb	%r13b, %r13b
	je	.L355
	cmpb	$46, %al
	je	.L740
.L355:
	leal	-48(%rax), %r10d
	movsbl	%al, %edi
	cmpb	$9, %r10b
	jbe	.L358
.L474:
	cmpl	$96, %edi
	jle	.L359
	cmpl	$102, %edi
	jg	.L676
.L358:
	cmpb	$48, %al
	sete	%al
	andl	%eax, %r11d
	testb	%r8b, %r8b
	jne	.L351
	addl	$4, %r14d
	jmp	.L351
.L359:
	subl	$65, %edi
	cmpl	$5, %edi
	jbe	.L358
.L676:
	movzbl	-896(%rbp), %r9d
	movq	%r15, %rax
	movl	%r11d, %r10d
	movl	%r12d, %r15d
	movl	-912(%rbp), %ecx
	movl	%ebx, %r12d
	movq	-904(%rbp), %r11
	movq	%rax, %rbx
	testb	%r9b, %r9b
	jne	.L364
	movq	-856(%rbp), %r8
	movq	-880(%rbp), %rax
	cmpq	%rax, %r8
	je	.L364
.L367:
	movzbl	(%rax), %edx
	leal	-9(%rdx), %esi
	cmpb	$4, %sil
	jbe	.L507
	cmpb	$32, %dl
	jne	.L223
.L507:
	addq	$1, %rax
	movq	%rax, -880(%rbp)
	cmpq	%rax, %r8
	jne	.L367
.L364:
	subl	$1, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	cmpl	%eax, -924(%rbp)
	jg	.L508
	je	.L741
.L368:
	movl	-920(%rbp), %eax
	btq	%rax, %r11
	jnc	.L346
	addl	$1, %r14d
	sarq	%r11
	jmp	.L346
.L739:
	movq	-880(%rbp), %rax
	addq	$1, %rax
	cmpq	%rax, -856(%rbp)
	movq	%rax, -880(%rbp)
	sete	%al
	jmp	.L353
.L736:
	leal	97200(%r13), %eax
	cmpl	$194400, %eax
	ja	.L381
	imull	$10, %r13d, %r13d
	leal	-48(%r13,%rdx), %r13d
.L381:
	movq	%r12, %rdx
	movl	%r15d, %esi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
	testb	%al, %al
	movq	-880(%rbp), %rax
	je	.L382
	jmp	.L380
.L735:
	movq	%r8, %rdx
	movl	%r15d, %esi
	movq	%r8, -896(%rbp)
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
	movq	-880(%rbp), %rax
	movq	-896(%rbp), %r8
	jmp	.L472
.L734:
	movq	-880(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, -880(%rbp)
	movzbl	1(%rax), %edx
	cmpb	$43, %dl
	je	.L742
	cmpb	$45, %dl
	jne	.L470
	addq	$2, %rax
	movb	$1, -906(%rbp)
	movq	%rax, -880(%rbp)
.L470:
	movq	-856(%rbp), %rcx
	xorl	%r13d, %r13d
.L384:
	movq	-880(%rbp), %rax
	movsbl	(%rax), %edx
	leal	-48(%rdx), %esi
	cmpb	$9, %sil
	ja	.L380
	leal	97200(%r13), %esi
	cmpl	$194400, %esi
	ja	.L383
	imull	$10, %r13d, %r13d
	leal	-48(%r13,%rdx), %r13d
.L383:
	addq	$1, %rax
	movq	%rax, -880(%rbp)
	cmpq	%rcx, %rax
	jne	.L384
	movq	%rcx, %rax
	jmp	.L380
.L742:
	addq	$2, %rax
	movq	%rax, -880(%rbp)
	jmp	.L470
.L740:
	testw	%bx, %bx
	je	.L743
	leaq	-880(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
.L357:
	movq	-880(%rbp), %rdi
	movzbl	(%rdi), %eax
	leal	-48(%rax), %edi
	cmpb	$9, %dil
	jbe	.L473
	movsbl	%al, %edi
	movl	%r13d, %r8d
	jmp	.L474
.L741:
	testb	$1, %r11b
	jne	.L508
	testb	%r10b, %r10b
	jne	.L368
.L508:
	addq	$1, %r11
	jmp	.L368
.L301:
	testw	%r14w, %r14w
	je	.L744
	leaq	-856(%rbp), %rdi
	movl	%r11d, %esi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKcEEbPT_tiRS4_.part.0.constprop.0
	movl	%eax, %r9d
.L305:
	testb	%r9b, %r9b
	je	.L307
.L306:
	movq	-880(%rbp), %rdx
	movl	%r9d, %r13d
	jmp	.L311
.L727:
	addq	$1, %rsi
	cmpq	%rsi, -864(%rbp)
	movq	%rsi, -856(%rbp)
	sete	%r9b
	jmp	.L299
.L744:
	addq	$1, %rsi
	cmpq	%rsi, -864(%rbp)
	movq	%rsi, -856(%rbp)
	sete	%r9b
	jmp	.L305
.L726:
	addq	$1, %rsi
	cmpq	%rsi, -864(%rbp)
	movq	%rsi, -856(%rbp)
	sete	%al
	jmp	.L294
.L731:
	movl	%r12d, %eax
	movq	-896(%rbp), %rbx
	movl	%r15d, %r12d
	movq	%rdi, %rdx
	movl	%eax, %r15d
	jmp	.L338
.L473:
	cmpb	$48, %al
	movl	%r13d, %r8d
	sete	%al
	andl	%eax, %r11d
	jmp	.L351
.L743:
	addq	$1, %rdi
	movq	%rdi, -880(%rbp)
	jmp	.L357
	.cfi_endproc
.LFE427:
	.size	_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKcEEdT_ibPi, .-_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKcEEdT_ibPi
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717double_conversion23StringToDoubleConverter14StringToDoubleEPKciPi
	.type	_ZNK6icu_6717double_conversion23StringToDoubleConverter14StringToDoubleEPKciPi, @function
_ZNK6icu_6717double_conversion23StringToDoubleConverter14StringToDoubleEPKciPi:
.LFB413:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	movl	$1, %ecx
	jmp	_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKcEEdT_ibPi
	.cfi_endproc
.LFE413:
	.size	_ZNK6icu_6717double_conversion23StringToDoubleConverter14StringToDoubleEPKciPi, .-_ZNK6icu_6717double_conversion23StringToDoubleConverter14StringToDoubleEPKciPi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717double_conversion23StringToDoubleConverter13StringToFloatEPKciPi
	.type	_ZNK6icu_6717double_conversion23StringToDoubleConverter13StringToFloatEPKciPi, @function
_ZNK6icu_6717double_conversion23StringToDoubleConverter13StringToFloatEPKciPi:
.LFB415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKcEEdT_ibPi
	popq	%rbp
	.cfi_def_cfa 7, 8
	cvtsd2ss	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE415:
	.size	_ZNK6icu_6717double_conversion23StringToDoubleConverter13StringToFloatEPKciPi, .-_ZNK6icu_6717double_conversion23StringToDoubleConverter13StringToFloatEPKciPi
	.section	.text._ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKtEEdT_ibPi,"axG",@progbits,_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKtEEdT_ibPi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKtEEdT_ibPi
	.type	_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKtEEdT_ibPi, @function
_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKtEEdT_ibPi:
.LFB428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$888, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -904(%rbp)
	leaq	(%rsi,%rdx,2), %rcx
	movq	%r8, -896(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -880(%rbp)
	movq	%rcx, -872(%rbp)
	movl	$0, (%r8)
	cmpq	%rsi, %rcx
	je	.L1277
	movl	(%rdi), %eax
	movq	%rsi, %rbx
	movl	%eax, -888(%rbp)
	testb	$24, %al
	jne	.L1280
	movq	%rsi, %rax
.L751:
	movzwl	(%rax), %esi
	leal	-43(%rsi), %edx
	testw	$-3, %dx
	jne	.L1042
	leaq	2(%rax), %r15
	cmpq	%r15, %rcx
	je	.L760
	movq	%r15, %rax
	leaq	40+_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L765:
	movzwl	(%rax), %edi
	movl	%edi, %r10d
	cmpl	$127, %edi
	jg	.L761
	leal	-9(%rdi), %edx
	cmpw	$4, %dx
	jbe	.L762
	cmpl	$32, %edi
	je	.L762
.L763:
	testb	$32, -888(%rbp)
	jne	.L766
	cmpq	%rax, %r15
	jne	.L760
.L766:
	cmpw	$45, %si
	movq	%rax, -880(%rbp)
	movl	%r10d, %esi
	sete	-909(%rbp)
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	%rsi, %rdx
	xorl	%edi, %edi
	leaq	40+_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %r8
	.p2align 4,,10
	.p2align 3
.L758:
	movzwl	(%rdx), %esi
	movl	%esi, %eax
	cmpl	$127, %esi
	jg	.L752
	subl	$9, %eax
	cmpw	$4, %ax
	jbe	.L753
	cmpl	$32, %esi
	je	.L753
.L755:
	movq	%rbx, %rax
	testb	%dil, %dil
	je	.L754
	movq	%rdx, -880(%rbp)
	movq	%rdx, %rax
.L754:
	testb	$8, -888(%rbp)
	jne	.L751
	cmpq	%rdx, %rbx
	je	.L751
	.p2align 4,,10
	.p2align 3
.L760:
	movsd	16(%r12), %xmm0
.L748:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1281
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L752:
	.cfi_restore_state
	leaq	_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %rax
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L1282:
	addq	$2, %rax
	cmpq	%rax, %r8
	je	.L755
.L756:
	movzwl	(%rax), %r9d
	cmpl	%r9d, %esi
	jne	.L1282
.L753:
	addq	$2, %rdx
	movl	$1, %edi
	cmpq	%rdx, %rcx
	jne	.L758
	movq	-896(%rbp), %rax
	subq	%rbx, %rcx
	sarq	%rcx
	movl	%ecx, (%rax)
.L1277:
	movsd	8(%r12), %xmm0
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L761:
	leaq	_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %rdx
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L1283:
	addq	$2, %rdx
	cmpq	%rdx, %r14
	je	.L763
.L764:
	movzwl	(%rdx), %r8d
	cmpl	%r8d, %edi
	jne	.L1283
.L762:
	addq	$2, %rax
	cmpq	%rax, %rcx
	jne	.L765
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L1042:
	movb	$0, -909(%rbp)
.L759:
	movl	-888(%rbp), %edi
	movl	%edi, %edx
	andl	$64, %edi
	andl	$4, %edx
	movl	%edi, %r8d
	movl	%edx, -908(%rbp)
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L767
	movl	%esi, %edi
	testl	%r8d, %r8d
	jne	.L781
	cmpb	(%rdx), %sil
	je	.L1284
	movq	32(%r12), %rdx
	testq	%rdx, %rdx
	je	.L780
.L1026:
	cmpb	%dil, (%rdx)
	je	.L1285
.L780:
	movl	-908(%rbp), %edi
	testl	%edi, %edi
	movl	-888(%rbp), %edi
	setne	-916(%rbp)
	andl	$16, %edi
	movl	%edi, -920(%rbp)
	cmpw	$48, %si
	je	.L1286
	movb	$1, -910(%rbp)
	xorl	%r11d, %r11d
	movb	$0, -911(%rbp)
.L792:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	leaq	-872(%rbp), %rcx
	xorl	%r15d, %r15d
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L1288:
	movslq	%r15d, %rdx
	addl	$1, %r15d
	movb	%sil, -848(%rbp,%rdx)
.L959:
	testb	%r11b, %r11b
	je	.L960
	cmpw	$55, (%rax)
	setbe	%r11b
.L960:
	movzwl	40(%r12), %esi
	testw	%si, %si
	je	.L1287
	leaq	-880(%rbp), %rdi
	movl	$10, %edx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0
	testb	%al, %al
	jne	.L962
.L963:
	movq	-880(%rbp), %rax
	movzwl	(%rax), %esi
.L964:
	leal	-48(%rsi), %edx
	cmpw	$9, %dx
	ja	.L957
	cmpl	$771, %r15d
	jle	.L1288
	addl	$1, %r13d
	cmpw	$48, %si
	setne	%dl
	orl	%edx, %r14d
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L1287:
	addq	$2, %rax
	movq	%rax, -880(%rbp)
	cmpq	-872(%rbp), %rax
	jne	.L963
.L962:
	testb	%r11b, %r11b
	jne	.L970
.L974:
	testb	%r14b, %r14b
	je	.L1018
	movslq	%r15d, %rax
	subl	$1, %r13d
	addl	$1, %r15d
	movb	$49, -848(%rbp,%rax)
.L1018:
	movslq	%r15d, %rax
	cmpb	$0, -904(%rbp)
	movl	%r15d, %esi
	movl	%r13d, %edx
	movb	$0, -848(%rbp,%rax)
	leaq	-848(%rbp), %rdi
	je	.L1019
	call	_ZN6icu_6717double_conversion6StrtodENS0_6VectorIKcEEi@PLT
.L1020:
	movq	-880(%rbp), %rax
	subq	%rbx, %rax
	movq	-896(%rbp), %rbx
	sarq	%rax
	cmpb	$0, -909(%rbp)
	movl	%eax, (%rbx)
	je	.L748
	xorpd	.LC3(%rip), %xmm0
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L767:
	movq	32(%r12), %rdx
	testq	%rdx, %rdx
	je	.L780
	movl	%esi, %edi
	testl	%r8d, %r8d
	je	.L1026
.L781:
	call	abort@PLT
	.p2align 4,,10
	.p2align 3
.L957:
	testl	%r15d, %r15d
	jne	.L965
	cmpw	$46, %si
	je	.L966
	xorl	%r9d, %r9d
	cmpb	$0, -911(%rbp)
	je	.L760
.L987:
	andl	$-33, %esi
	cmpw	$69, %si
	je	.L1035
.L1273:
	xorl	%r11d, %r11d
.L1025:
	testb	$20, -888(%rbp)
	jne	.L1002
	cmpq	%rax, -872(%rbp)
	jne	.L760
	movl	-908(%rbp), %edx
	testl	%edx, %edx
	je	.L1005
.L1004:
	movl	-920(%rbp), %r8d
	addl	%r9d, %r13d
	testl	%r8d, %r8d
	je	.L962
	movq	-872(%rbp), %r8
	cmpq	%rax, %r8
	je	.L962
	xorl	%r9d, %r9d
	leaq	40+_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L1017:
	movzwl	(%rax), %esi
	movl	%esi, %edx
	cmpl	$127, %esi
	jg	.L1012
	subl	$9, %edx
	cmpw	$4, %dx
	jbe	.L1013
	cmpl	$32, %esi
	je	.L1013
.L1275:
	testb	%r9b, %r9b
	je	.L962
	movq	%rax, -880(%rbp)
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L1286:
	movzwl	40(%r12), %esi
	testw	%si, %si
	je	.L1289
	leaq	-872(%rbp), %rcx
	leaq	-880(%rbp), %rdi
	movl	$10, %edx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0
	movb	%al, -910(%rbp)
	movq	-880(%rbp), %rax
.L794:
	cmpb	$0, -910(%rbp)
	jne	.L1264
	movl	(%r12), %edi
	movzwl	(%rax), %esi
	testb	$-127, %dil
	je	.L797
	movl	%esi, %edx
	andl	$-33, %edx
	cmpw	$88, %dx
	jne	.L797
	movq	-872(%rbp), %rcx
	leaq	2(%rax), %rdx
	movq	%rdx, -880(%rbp)
	cmpq	%rcx, %rdx
	je	.L760
	andl	$128, %edi
	jne	.L1290
.L799:
	movzwl	(%rdx), %edi
	leal	-48(%rdi), %eax
	cmpw	$9, %ax
	jbe	.L1046
	movzwl	%di, %eax
	cmpl	$96, %eax
	jle	.L853
	cmpl	$102, %eax
	jg	.L760
.L854:
	movq	-872(%rbp), %rcx
	cmpb	$0, -904(%rbp)
	movsd	16(%r12), %xmm0
	movzwl	40(%r12), %r9d
	movq	%rcx, -856(%rbp)
	movzbl	-916(%rbp), %r15d
	jne	.L1062
	movl	$24, %r12d
.L1036:
	movl	%r13d, %r10d
.L858:
	leaq	-856(%rbp), %r8
	movq	%rdx, %rsi
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	movzwl	%r9w, %ecx
	movq	%r8, %rdx
	leal	-48(%rax), %r8d
	xorl	%r14d, %r14d
	movq	%rbx, -904(%rbp)
	movl	%r10d, %ebx
	movl	%r9d, %r10d
	movl	%ecx, -888(%rbp)
	cmpl	$9, %r8d
	ja	.L872
.L1294:
	movsbl	%dil, %eax
	leal	-4(%r11), %edi
	subl	$48, %eax
	testb	%r13b, %r13b
	cmovne	%edi, %r11d
.L874:
	cltq
	salq	$4, %r14
	movl	%r12d, %ecx
	addq	%rax, %r14
	movq	%r14, %rdi
	sarq	%cl, %rdi
	movl	%edi, %eax
	testl	%edi, %edi
	jne	.L1291
	testw	%r10w, %r10w
	je	.L1292
	movl	-888(%rbp), %esi
	leaq	-880(%rbp), %rdi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
.L917:
	testb	%al, %al
	jne	.L1293
.L885:
	movq	-880(%rbp), %rsi
	movzwl	(%rsi), %eax
	leal	-48(%rax), %r8d
	movl	%eax, %edi
	cmpl	$9, %r8d
	jbe	.L1294
.L872:
	cmpl	$96, %eax
	jle	.L1227
	cmpl	$102, %eax
	jg	.L1295
	movsbl	%dil, %eax
	leal	-4(%r11), %edi
	subl	$87, %eax
	testb	%r13b, %r13b
	cmovne	%edi, %r11d
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L797:
	leaq	-872(%rbp), %rcx
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L952:
	leaq	-880(%rbp), %rdi
	movl	$10, %edx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0
	movl	%eax, %r8d
	movq	-880(%rbp), %rax
	testb	%r8b, %r8b
	jne	.L1264
.L954:
	movzwl	(%rax), %esi
.L955:
	cmpw	$48, %si
	jne	.L951
	movzwl	40(%r12), %esi
	testw	%si, %si
	jne	.L952
	addq	$2, %rax
	movq	%rax, -880(%rbp)
	cmpq	-872(%rbp), %rax
	jne	.L954
	.p2align 4,,10
	.p2align 3
.L1264:
	subq	%rbx, %rax
	movq	-896(%rbp), %rbx
	pxor	%xmm0, %xmm0
	sarq	%rax
	cmpb	$0, -909(%rbp)
	movl	%eax, (%rbx)
	je	.L748
	movsd	.LC2(%rip), %xmm0
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L1019:
	call	_ZN6icu_6717double_conversion6StrtofENS0_6VectorIKcEEi@PLT
	cvtss2sd	%xmm0, %xmm0
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L965:
	cmpw	$46, %si
	je	.L1296
	andl	$-33, %esi
	xorl	%r9d, %r9d
	cmpw	$69, %si
	jne	.L1025
	movl	-908(%rbp), %edx
	testl	%edx, %edx
	jne	.L1065
	testb	%r11b, %r11b
	jne	.L760
.L1236:
	xorl	%r9d, %r9d
.L1035:
	movq	-872(%rbp), %rcx
	leaq	2(%rax), %rdx
	movq	%rdx, -880(%rbp)
	cmpq	%rcx, %rdx
	je	.L1034
	movzwl	2(%rax), %edi
	leal	-43(%rdi), %esi
	testw	$-3, %si
	jne	.L1059
	leaq	4(%rax), %rdx
	movq	%rdx, -880(%rbp)
	cmpq	%rdx, %rcx
	je	.L1034
	movl	%edi, %esi
	movzwl	4(%rax), %edi
.L992:
	subl	$48, %edi
	cmpw	$9, %di
	jbe	.L1297
.L1034:
	movl	-908(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L760
	movq	%rax, -880(%rbp)
	addl	%r9d, %r13d
	jmp	.L974
.L1296:
	movl	-908(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L969
	testb	%r11b, %r11b
	jne	.L760
.L966:
	movzwl	40(%r12), %edx
	testw	%dx, %dx
	je	.L1298
	movzwl	%dx, %esi
	leaq	-872(%rbp), %rcx
	leaq	-880(%rbp), %rdi
	movl	$10, %edx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0
.L972:
	testb	%al, %al
	je	.L973
	testl	%r15d, %r15d
	jne	.L974
	cmpb	$0, -910(%rbp)
	jne	.L760
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L1012:
	leaq	_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %rdx
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1299:
	addq	$2, %rdx
	cmpq	%rdx, %rdi
	je	.L1275
.L1015:
	movzwl	(%rdx), %ecx
	cmpl	%ecx, %esi
	jne	.L1299
.L1013:
	addq	$2, %rax
	movl	$1, %r9d
	cmpq	%rax, %r8
	jne	.L1017
	movq	%r8, -880(%rbp)
	jmp	.L962
.L1289:
	addq	$2, %rax
	cmpq	%rax, %rcx
	movq	%rax, -880(%rbp)
	sete	-910(%rbp)
	jmp	.L794
.L1065:
	testb	%r11b, %r11b
	je	.L1236
.L970:
	leaq	-848(%rbp), %rax
	movslq	%r15d, %rsi
	subq	$8, %rsp
	movsd	16(%r12), %xmm0
	addq	%rax, %rsi
	movq	%rax, -856(%rbp)
	leaq	-864(%rbp), %rax
	movzwl	40(%r12), %ecx
	movl	-916(%rbp), %r8d
	pushq	%rax
	leaq	-856(%rbp), %rdi
	movzbl	-909(%rbp), %edx
	movzbl	-904(%rbp), %r9d
	andl	$1, %r8d
	call	_ZN6icu_6717double_conversionL17RadixStringToIeeeILi3EPcEEdPT0_S3_btbbdbPb.constprop.0
	movq	-880(%rbp), %rax
	subq	%rbx, %rax
	movq	-896(%rbp), %rbx
	sarq	%rax
	movl	%eax, (%rbx)
	popq	%rsi
	popq	%rdi
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L1284:
	cmpb	$0, 1(%rdx)
	leaq	1(%rdx), %rsi
	je	.L770
	leaq	2(%rax), %rdx
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L1300:
	movzbl	(%rdx), %edi
	cmpb	%dil, (%rsi)
	jne	.L760
	addq	$1, %rsi
	addq	$2, %rdx
	cmpb	$0, (%rsi)
	je	.L770
.L772:
	movq	%rdx, -880(%rbp)
	movq	%rdx, %rax
	cmpq	%rdx, %rcx
	jne	.L1300
	jmp	.L760
.L770:
	addq	$2, %rax
	movq	%rax, -880(%rbp)
	testb	$20, -888(%rbp)
	jne	.L773
	cmpq	%rax, %rcx
	jne	.L760
.L774:
	subq	%rbx, %rax
	movq	-896(%rbp), %rbx
	movsd	.LC6(%rip), %xmm0
	sarq	%rax
	cmpb	$0, -909(%rbp)
	movl	%eax, (%rbx)
	jne	.L748
	movsd	.LC0(%rip), %xmm0
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L1285:
	cmpb	$0, 1(%rdx)
	leaq	1(%rdx), %rsi
	je	.L782
	leaq	2(%rax), %rdx
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L1301:
	movzbl	(%rdx), %edi
	cmpb	%dil, (%rsi)
	jne	.L760
	addq	$1, %rsi
	addq	$2, %rdx
	cmpb	$0, (%rsi)
	je	.L782
.L784:
	movq	%rdx, -880(%rbp)
	movq	%rdx, %rax
	cmpq	%rdx, %rcx
	jne	.L1301
	jmp	.L760
.L782:
	addq	$2, %rax
	movq	%rax, -880(%rbp)
	testb	$20, -888(%rbp)
	jne	.L785
	cmpq	%rax, %rcx
	jne	.L760
.L786:
	subq	%rbx, %rax
	movq	-896(%rbp), %rbx
	movsd	.LC5(%rip), %xmm0
	sarq	%rax
	cmpb	$0, -909(%rbp)
	movl	%eax, (%rbx)
	jne	.L748
	movsd	.LC4(%rip), %xmm0
	jmp	.L748
.L951:
	movl	(%r12), %r10d
	movb	$1, -911(%rbp)
	shrl	%r10d
	movl	%r10d, %r11d
	andl	$1, %r11d
	jmp	.L792
.L773:
	movl	-908(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L774
	cmpq	%rax, %rcx
	je	.L774
	leaq	40+_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %r8
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L1302:
	subl	$9, %edx
	cmpw	$4, %dx
	jbe	.L776
	cmpl	$32, %edi
	jne	.L760
	.p2align 4,,10
	.p2align 3
.L776:
	addq	$2, %rax
	cmpq	%rax, %rcx
	je	.L774
.L779:
	movzwl	(%rax), %edi
	movl	%edi, %edx
	cmpl	$127, %edi
	jle	.L1302
	leaq	_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L778:
	movzwl	(%rdx), %esi
	cmpl	%esi, %edi
	je	.L776
	addq	$2, %rdx
	cmpq	%r8, %rdx
	jne	.L778
	jmp	.L760
.L1002:
	movl	-908(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L1004
	movq	-872(%rbp), %r8
	cmpq	%rax, %r8
	je	.L1005
	leaq	40+_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %rdi
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1304:
	subl	$9, %edx
	cmpw	$4, %dx
	jbe	.L1007
	cmpl	$32, %esi
	jne	.L760
	.p2align 4,,10
	.p2align 3
.L1007:
	addq	$2, %rax
	cmpq	%rax, %r8
	je	.L1303
.L1011:
	movzwl	(%rax), %esi
	movl	%esi, %edx
	cmpl	$127, %esi
	jle	.L1304
	leaq	_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L1009:
	movzwl	(%rdx), %ecx
	cmpl	%ecx, %esi
	je	.L1007
	addq	$2, %rdx
	cmpq	%rdx, %rdi
	jne	.L1009
	jmp	.L760
.L785:
	movl	-908(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L786
	cmpq	%rax, %rcx
	je	.L786
	leaq	40+_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %r8
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L1305:
	subl	$9, %edx
	cmpw	$4, %dx
	jbe	.L788
	cmpl	$32, %edi
	jne	.L760
	.p2align 4,,10
	.p2align 3
.L788:
	addq	$2, %rax
	cmpq	%rax, %rcx
	je	.L786
.L791:
	movzwl	(%rax), %edi
	movl	%edi, %edx
	cmpl	$127, %edi
	jle	.L1305
	leaq	_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L790:
	movzwl	(%rdx), %esi
	cmpl	%esi, %edi
	je	.L788
	addq	$2, %rdx
	cmpq	%rdx, %r8
	jne	.L790
	jmp	.L760
.L973:
	movq	-880(%rbp), %rax
	movzwl	(%rax), %esi
	testl	%r15d, %r15d
	jne	.L1057
	cmpw	$48, %si
	jne	.L1057
	xorl	%r11d, %r11d
	leaq	-872(%rbp), %rcx
	jmp	.L979
.L976:
	movzwl	%dx, %esi
	leaq	-880(%rbp), %rdi
	movl	$10, %edx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0
	movl	%eax, %edx
	movq	-880(%rbp), %rax
.L977:
	testb	%dl, %dl
	jne	.L1264
	movzwl	(%rax), %esi
	subl	$1, %r11d
	cmpw	$48, %si
	jne	.L1306
.L979:
	movzwl	40(%r12), %edx
	testw	%dx, %dx
	jne	.L976
	addq	$2, %rax
	cmpq	%rax, -872(%rbp)
	movq	%rax, -880(%rbp)
	sete	%dl
	jmp	.L977
.L969:
	testb	%r11b, %r11b
	jne	.L970
	jmp	.L966
.L1303:
	movq	%r8, -880(%rbp)
.L1005:
	addl	%r9d, %r13d
	jmp	.L962
.L1298:
	addq	$2, %rax
	cmpq	%rax, -872(%rbp)
	movq	%rax, -880(%rbp)
	sete	%al
	jmp	.L972
.L1059:
	movl	$43, %esi
	jmp	.L992
.L1057:
	xorl	%r9d, %r9d
.L975:
	leaq	-872(%rbp), %rcx
	movl	%r9d, %r11d
	jmp	.L986
.L1308:
	movslq	%r15d, %rdx
	subl	$1, %r11d
	addl	$1, %r15d
	movb	%sil, -848(%rbp,%rdx)
.L982:
	movzwl	40(%r12), %edx
	testw	%dx, %dx
	je	.L1307
	movzwl	%dx, %esi
	leaq	-880(%rbp), %rdi
	movl	$10, %edx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0
	testb	%al, %al
	jne	.L1256
.L985:
	movq	-880(%rbp), %rax
	movzwl	(%rax), %esi
.L986:
	leal	-48(%rsi), %edx
	cmpw	$9, %dx
	ja	.L980
	cmpl	$771, %r15d
	jle	.L1308
	cmpw	$48, %si
	setne	%dl
	orl	%edx, %r14d
	jmp	.L982
.L1307:
	addq	$2, %rax
	movq	%rax, -880(%rbp)
	cmpq	-872(%rbp), %rax
	jne	.L985
.L1256:
	addl	%r11d, %r13d
	jmp	.L974
.L853:
	leal	-65(%rdi), %ecx
	cmpw	$5, %cx
	ja	.L760
	jmp	.L854
.L1290:
	movzwl	40(%r12), %r14d
	movq	%rcx, -864(%rbp)
	movq	%rdx, -856(%rbp)
	movl	%r14d, %r11d
	testw	%r14w, %r14w
	je	.L800
	xorl	%r15d, %r15d
	leaq	-864(%rbp), %r10
.L806:
	movzwl	(%rdx), %ecx
	leal	-48(%rcx), %esi
	movl	%ecx, %eax
	cmpw	$9, %si
	jbe	.L801
	cmpl	$96, %ecx
	jg	.L802
	cmpl	$64, %ecx
	jg	.L1309
	cmpw	$46, %cx
	jne	.L804
	leaq	-864(%rbp), %r8
	leaq	-856(%rbp), %r10
	movl	%r14d, %esi
	movq	%r8, %rdx
	movq	%r10, %rdi
	movq	%r8, -888(%rbp)
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
	testb	%al, %al
	jne	.L815
	movq	-888(%rbp), %r8
	movq	%r8, %r10
.L819:
	movq	-856(%rbp), %rdx
	movzwl	(%rdx), %ecx
	leal	-48(%rcx), %esi
	movl	%ecx, %eax
	cmpw	$9, %si
	jbe	.L817
	cmpl	$96, %ecx
	jg	.L818
	leal	-65(%rcx), %ecx
	cmpw	$5, %cx
	jbe	.L817
.L804:
	testb	%r15b, %r15b
	je	.L815
	andl	$-33, %eax
	cmpw	$80, %ax
	jne	.L815
	testw	%r11w, %r11w
	je	.L1310
	leaq	-864(%rbp), %rdx
	leaq	-856(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
.L829:
	testb	%al, %al
	jne	.L815
	movq	-856(%rbp), %rax
	movzwl	(%rax), %ecx
	leal	-43(%rcx), %edx
	testw	$-3, %dx
	jne	.L837
	testw	%r11w, %r11w
	je	.L1311
	leaq	-864(%rbp), %rdx
	leaq	-856(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
.L835:
	testb	%al, %al
	jne	.L815
	movq	-856(%rbp), %rax
.L837:
	movzwl	(%rax), %edx
	movl	%edx, %ecx
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L815
	cmpw	$57, %cx
	ja	.L815
	testw	%r11w, %r11w
	je	.L1312
	leaq	-864(%rbp), %rdx
	leaq	-856(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
	movl	%eax, %r10d
.L840:
	testb	%r10b, %r10b
	jne	.L847
	leaq	-864(%rbp), %rdx
.L848:
	movq	-856(%rbp), %r8
	movzwl	(%r8), %eax
	subl	$48, %eax
	cmpw	$9, %ax
	jbe	.L842
	cmpl	$0, -908(%rbp)
	jne	.L849
	movq	-864(%rbp), %rsi
	jmp	.L850
.L1313:
	movzwl	(%r8), %edi
	call	_ZN6icu_6717double_conversionL12isWhitespaceEi
	addq	$2, %r8
	testb	%al, %al
	je	.L815
.L850:
	cmpq	%r8, %rsi
	jne	.L1313
.L849:
	movq	-880(%rbp), %rdx
	movl	%r15d, %r10d
	movzwl	(%rdx), %edi
	jmp	.L852
.L821:
	leal	-65(%rsi), %esi
	cmpw	$5, %si
	ja	.L823
.L820:
	addq	$2, %rdx
	movl	$1, %r15d
	movl	$1, %ecx
	cmpq	%r8, %rdx
	jne	.L824
.L815:
	movq	-880(%rbp), %rdx
	jmp	.L799
.L1292:
	addq	$2, %rsi
	cmpq	%rsi, -856(%rbp)
	movq	%rsi, -880(%rbp)
	sete	%al
	jmp	.L917
.L980:
	movl	%r11d, %r9d
	testl	%r11d, %r11d
	jne	.L987
	cmpb	$0, -910(%rbp)
	je	.L987
	testl	%r15d, %r15d
	je	.L760
	xorl	%r9d, %r9d
	jmp	.L987
.L1309:
	cmpl	$70, %ecx
	jg	.L804
.L801:
	leaq	-856(%rbp), %rdi
	movq	%r10, %rdx
	movl	%r14d, %esi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
	testb	%al, %al
	jne	.L815
	movq	-856(%rbp), %rdx
	movl	$1, %r15d
	jmp	.L806
.L1227:
	leal	-65(%rdi), %r8d
	cmpw	$5, %r8w
	jbe	.L1314
	testb	%bl, %bl
	je	.L1315
	cmpw	$46, %di
	je	.L1316
	movl	%r10d, %r9d
	movl	%ebx, %r10d
	movq	-904(%rbp), %rbx
	movq	%rsi, %rdx
.L883:
	andl	$-33, %edi
	cmpw	$80, %di
	je	.L886
	cmpb	$0, -916(%rbp)
	je	.L1317
.L886:
	testw	%r9w, %r9w
	je	.L1318
	leaq	-856(%rbp), %r10
	movl	-888(%rbp), %esi
	leaq	-880(%rbp), %rdi
	movq	%r10, %rdx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
	movq	-880(%rbp), %rax
	movzwl	(%rax), %edx
	cmpw	$43, %dx
	je	.L1319
	cmpw	$45, %dx
	je	.L1029
	xorl	%r13d, %r13d
.L1030:
	xorl	%r12d, %r12d
.L928:
	movzwl	(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpw	$9, %cx
	jbe	.L1320
.L926:
	movl	%r12d, %edx
	negl	%edx
	testb	%r13b, %r13b
	cmovne	%edx, %r12d
	addl	%r12d, %r11d
.L918:
	testl	%r11d, %r11d
	je	.L1064
	testq	%r14, %r14
	je	.L1064
	movabsq	$9007199254740991, %rdx
	cmpq	%rdx, %r14
	jbe	.L938
.L939:
	shrq	%r14
	addl	$1, %r11d
	cmpq	%rdx, %r14
	ja	.L939
.L938:
	movabsq	$9218868437227405312, %rdx
	cmpl	$971, %r11d
	jg	.L940
	xorl	%edx, %edx
	cmpl	$-1074, %r11d
	jl	.L940
	movl	$1, %edx
	movq	%r14, %rcx
	salq	$52, %rdx
	andq	%rdx, %rcx
	cmpl	$-1074, %r11d
	jne	.L1234
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L1321:
	addq	%r14, %r14
	subl	$1, %r11d
	movq	%r14, %rcx
	andq	%rdx, %rcx
	cmpl	$-1074, %r11d
	je	.L941
.L1234:
	testq	%rcx, %rcx
	je	.L1321
.L942:
	leal	1075(%r11), %edx
	movslq	%edx, %rcx
.L1021:
	movabsq	$4503599627370495, %rdx
	salq	$52, %rcx
	andq	%r14, %rdx
	orq	%rcx, %rdx
.L940:
	cmpb	$0, -909(%rbp)
	movq	%rdx, %xmm0
	je	.L937
	xorpd	.LC3(%rip), %xmm0
.L937:
	movl	-920(%rbp), %esi
	movq	%rax, %rdi
	testl	%esi, %esi
	je	.L946
	movq	-872(%rbp), %r9
	cmpq	%rax, %r9
	je	.L946
	movzbl	-910(%rbp), %r10d
	leaq	40+_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %r8
.L950:
	movzwl	(%rdi), %esi
	movl	%esi, %edx
	cmpl	$127, %esi
	jg	.L947
	subl	$9, %edx
	cmpw	$4, %dx
	jbe	.L948
	cmpl	$32, %esi
	je	.L948
.L1272:
	testb	%r10b, %r10b
	cmove	%rax, %rdi
.L946:
	subq	%rbx, %rdi
	movq	-896(%rbp), %rbx
	movq	%rdi, %rax
	sarq	%rax
	movl	%eax, (%rbx)
	jmp	.L748
.L947:
	leaq	_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %rdx
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L1322:
	addq	$2, %rdx
	cmpq	%rdx, %r8
	je	.L1272
.L949:
	movzwl	(%rdx), %ecx
	cmpl	%ecx, %esi
	jne	.L1322
.L948:
	addq	$2, %rdi
	movl	$1, %r10d
	cmpq	%rdi, %r9
	jne	.L950
	movq	%r9, %rdi
	jmp	.L946
.L1046:
	movzbl	-916(%rbp), %r15d
	xorl	%r10d, %r10d
.L852:
	cmpb	$1, -904(%rbp)
	movsd	16(%r12), %xmm0
	movzwl	40(%r12), %r9d
	movq	-872(%rbp), %rcx
	sbbl	%r12d, %r12d
	andl	$-29, %r12d
	movq	%rcx, -856(%rbp)
	addl	$53, %r12d
	cmpw	$48, %di
	jne	.L1323
	testw	%r9w, %r9w
	jne	.L866
	jmp	.L860
.L1325:
	leaq	4(%rdx), %rsi
	movzwl	2(%rdx), %edi
	cmpq	%rsi, %rcx
	je	.L1048
	cmpw	%di, %r9w
	je	.L1324
	movq	%rax, %rdx
.L863:
	movzwl	(%rdx), %edi
.L862:
	cmpw	$48, %di
	jne	.L865
.L866:
	leaq	2(%rdx), %rax
	cmpq	%rax, %rcx
	jne	.L1325
.L867:
	cmpb	$0, -909(%rbp)
	movq	%rcx, -880(%rbp)
	je	.L1226
	movq	-880(%rbp), %rax
.L870:
	movsd	.LC2(%rip), %xmm0
	jmp	.L937
.L860:
	addq	$2, %rdx
	cmpq	%rdx, %rcx
	je	.L867
	movzwl	(%rdx), %edi
	cmpw	$48, %di
	je	.L860
.L865:
	movq	%rdx, -880(%rbp)
	movzwl	%di, %eax
	jmp	.L858
.L1324:
	movzwl	4(%rdx), %edi
	movq	%rsi, %rdx
	leal	-48(%rdi), %r8d
	cmpw	$9, %r8w
	jbe	.L863
	movzwl	%di, %esi
	cmpl	$96, %esi
	jg	.L864
	subl	$65, %edi
	cmpw	$5, %di
	cmova	%rax, %rdx
	jmp	.L863
.L1048:
	movq	%rax, %rdx
	jmp	.L862
.L802:
	cmpl	$102, %ecx
	jg	.L804
	jmp	.L801
.L1295:
	movl	%r10d, %r9d
	movl	%ebx, %r10d
	movq	%rsi, %rdx
	movq	-904(%rbp), %rbx
	testb	%r10b, %r10b
	jne	.L883
.L882:
	movl	-908(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1237
.L1270:
	movq	-880(%rbp), %rax
	jmp	.L918
.L1306:
	movl	%r11d, %r9d
	jmp	.L975
.L1226:
	movq	-880(%rbp), %rax
	pxor	%xmm0, %xmm0
	jmp	.L937
.L864:
	cmpl	$102, %esi
	cmovg	%rax, %rdx
	jmp	.L863
.L1297:
	xorl	%edi, %edi
	jmp	.L999
.L1326:
	cmpl	$3, %eax
	jg	.L1060
.L996:
	leal	(%rdi,%rdi,4), %edi
	leal	(%rax,%rdi,2), %edi
.L997:
	addq	$2, %rdx
	cmpq	%rdx, %rcx
	je	.L998
	movzwl	(%rdx), %eax
	subl	$48, %eax
	cmpw	$9, %ax
	ja	.L998
.L999:
	movzwl	(%rdx), %eax
	subl	$48, %eax
	cmpl	$107374181, %edi
	jle	.L996
	cmpl	$107374182, %edi
	je	.L1326
.L1060:
	movl	$1073741823, %edi
	jmp	.L997
.L998:
	movl	%edi, %eax
	movq	%rdx, -880(%rbp)
	negl	%eax
	cmpb	$45, %sil
	cmove	%eax, %edi
	movl	%r9d, %eax
	addl	%edi, %eax
	movl	%eax, %r9d
	movq	%rdx, %rax
	jmp	.L1273
.L1064:
	cmpb	$0, -909(%rbp)
	je	.L936
	testq	%r14, %r14
	je	.L870
	negq	%r14
.L936:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r14, %xmm0
	jmp	.L937
.L800:
	movq	%rcx, -888(%rbp)
	xorl	%r10d, %r10d
	movq	%rdx, %rdi
	xorl	%r15d, %r15d
	leaq	4(%rax), %r8
.L814:
	movzwl	-2(%r8), %esi
	movq	%rdi, %r9
	leaq	-2(%r8), %rdx
	movq	%r8, %rdi
	leal	-48(%rsi), %ecx
	movl	%esi, %eax
	cmpw	$9, %cx
	jbe	.L807
	cmpl	$96, %esi
	jle	.L808
	cmpl	$102, %esi
	jle	.L807
.L1268:
	testb	%r10b, %r10b
	je	.L804
	movq	%r9, -856(%rbp)
	jmp	.L804
.L1327:
	cmpl	$70, %esi
	jg	.L1268
.L807:
	addq	$2, %r8
	movl	$1, %r15d
	movl	$1, %r10d
	cmpq	%rdi, -888(%rbp)
	jne	.L814
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L808:
	cmpl	$64, %esi
	jg	.L1327
	testb	%r10b, %r10b
	je	.L812
	movq	%r9, -856(%rbp)
.L812:
	cmpw	$46, %ax
	jne	.L804
	addq	$2, %rdx
	movq	%rdx, -856(%rbp)
	cmpq	-864(%rbp), %rdx
	je	.L815
	movq	-864(%rbp), %r8
	xorl	%ecx, %ecx
.L824:
	movzwl	(%rdx), %esi
	leal	-48(%rsi), %edi
	movl	%esi, %eax
	cmpw	$9, %di
	jbe	.L820
	cmpl	$96, %esi
	jle	.L821
	cmpl	$102, %esi
	jle	.L820
.L823:
	testb	%cl, %cl
	je	.L804
	movq	%rdx, -856(%rbp)
	jmp	.L804
.L1314:
	subl	$55, %eax
	leal	-4(%r11), %edi
	testb	%r13b, %r13b
	cmovne	%edi, %r11d
	jmp	.L874
.L818:
	cmpl	$102, %ecx
	jg	.L804
.L817:
	movq	%r10, %rdx
	movl	%r14d, %esi
	movl	$1, %r15d
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
	testb	%al, %al
	je	.L819
	jmp	.L815
.L1237:
	movq	-856(%rbp), %r8
	cmpq	%rdx, %r8
	je	.L1270
.L1033:
	leaq	40+_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %rdi
	jmp	.L892
.L1329:
	subl	$9, %eax
	cmpw	$4, %ax
	jbe	.L889
	cmpl	$32, %esi
	jne	.L748
.L889:
	addq	$2, %rdx
	cmpq	%r8, %rdx
	je	.L1328
.L892:
	movzwl	(%rdx), %esi
	movl	%esi, %eax
	cmpl	$127, %esi
	jle	.L1329
	leaq	_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %rax
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L1330:
	addq	$2, %rax
	cmpq	%rax, %rdi
	je	.L748
.L891:
	movzwl	(%rax), %ecx
	cmpl	%ecx, %esi
	jne	.L1330
	jmp	.L889
.L1316:
	testw	%r10w, %r10w
	je	.L1331
	movl	-888(%rbp), %esi
	leaq	-880(%rbp), %rdi
	movl	%ebx, %r13d
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
	jmp	.L885
.L1317:
	movq	-856(%rbp), %r8
	cmpq	%rdx, %r8
	jne	.L1033
	jmp	.L886
.L1291:
	movl	%r10d, %r9d
	movl	$1, %ecx
	movl	%ebx, %r10d
	movq	-904(%rbp), %rbx
	cmpl	$1, %edi
	jle	.L895
.L896:
	sarl	%eax
	addl	$1, %ecx
	cmpl	$1, %eax
	jne	.L896
.L895:
	movl	$1, %eax
	movl	%ecx, -908(%rbp)
	movl	$1, %r8d
	addl	%ecx, %r11d
	sall	%cl, %eax
	movl	%r12d, -904(%rbp)
	leaq	-856(%rbp), %rdx
	movl	%r10d, %r12d
	subl	$1, %eax
	movl	%r8d, %r10d
	movl	%r13d, %r8d
	movq	%rbx, %r13
	andl	%r14d, %eax
	movl	%r9d, %ebx
	sarq	%cl, %r14
	movl	%eax, -916(%rbp)
.L897:
	testw	%bx, %bx
	je	.L1332
	movl	-888(%rbp), %esi
	leaq	-880(%rbp), %rdi
	movb	%r8b, -911(%rbp)
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
	movzbl	-911(%rbp), %r8d
.L899:
	testb	%al, %al
	jne	.L1250
	movq	-880(%rbp), %rsi
	movzwl	(%rsi), %eax
	testb	%r12b, %r12b
	je	.L901
	cmpw	$46, %ax
	je	.L1333
.L901:
	leal	-48(%rax), %edi
	movzwl	%ax, %esi
	cmpw	$9, %di
	jbe	.L904
.L1032:
	cmpl	$96, %esi
	jle	.L905
	cmpl	$102, %esi
	jg	.L1250
.L904:
	cmpw	$48, %ax
	sete	%al
	andl	%eax, %r10d
	testb	%r8b, %r8b
	jne	.L897
	addl	$4, %r11d
	jmp	.L897
.L905:
	subl	$65, %esi
	cmpl	$5, %esi
	jbe	.L904
.L1250:
	movl	%ebx, %r9d
	movl	%r10d, %r8d
	movl	-908(%rbp), %ecx
	movl	%r12d, %r10d
	movq	%r13, %rbx
	movl	-904(%rbp), %r12d
	testb	%r15b, %r15b
	jne	.L907
	movq	-856(%rbp), %r15
	movq	-880(%rbp), %rax
	cmpq	%rax, %r15
	je	.L907
	leaq	40+_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %r13
	jmp	.L912
.L1335:
	subl	$9, %edx
	cmpw	$4, %dx
	jbe	.L909
	cmpl	$32, %esi
	jne	.L748
.L909:
	addq	$2, %rax
	cmpq	%rax, %r15
	je	.L1334
.L912:
	movzwl	(%rax), %esi
	movl	%esi, %edx
	cmpl	$127, %esi
	jle	.L1335
	leaq	_ZN6icu_6717double_conversionL18kWhitespaceTable16E(%rip), %rdx
	jmp	.L911
.L1336:
	addq	$2, %rdx
	cmpq	%rdx, %r13
	je	.L748
.L911:
	movzwl	(%rdx), %edi
	cmpl	%edi, %esi
	jne	.L1336
	jmp	.L909
.L1334:
	movq	%r15, -880(%rbp)
.L907:
	subl	$1, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	cmpl	%eax, -916(%rbp)
	jg	.L1063
	je	.L1337
.L914:
	btq	%r12, %r14
	jnc	.L893
	addl	$1, %r11d
	sarq	%r14
.L893:
	movq	-880(%rbp), %rax
	testb	%r10b, %r10b
	je	.L918
	jmp	.L886
.L1332:
	movq	-880(%rbp), %rax
	addq	$2, %rax
	cmpq	%rax, -856(%rbp)
	movq	%rax, -880(%rbp)
	sete	%al
	jmp	.L899
.L1331:
	addq	$2, %rsi
	movl	%ebx, %r13d
	movq	%rsi, -880(%rbp)
	jmp	.L885
.L1062:
	xorl	%r10d, %r10d
	movl	$53, %r12d
	jmp	.L858
.L1328:
	movq	%rdx, -880(%rbp)
	jmp	.L893
.L1293:
	movl	%r10d, %r9d
	movl	%ebx, %r10d
	movq	-904(%rbp), %rbx
	jmp	.L893
.L941:
	movl	$1, %edx
	movl	$-1074, %r11d
	salq	$52, %rdx
	movq	%rdx, %rcx
	andq	%r14, %rcx
	jne	.L942
	jmp	.L1021
.L1310:
	addq	$2, %rdx
	cmpq	%rdx, -864(%rbp)
	movq	%rdx, -856(%rbp)
	sete	%al
	jmp	.L829
.L1281:
	call	__stack_chk_fail@PLT
.L1320:
	leal	97200(%r12), %eax
	cmpl	$194400, %eax
	ja	.L927
	imull	$10, %r12d, %r12d
	leal	-48(%r12,%rdx), %r12d
.L927:
	movl	-888(%rbp), %esi
	movq	%r10, %rdx
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
	testb	%al, %al
	movq	-880(%rbp), %rax
	je	.L928
	jmp	.L926
.L1029:
	movl	-888(%rbp), %esi
	movq	%r10, %rdx
	movl	$1, %r13d
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
	movq	-880(%rbp), %rax
	jmp	.L1030
.L1319:
	movl	-888(%rbp), %esi
	movq	%r10, %rdx
	xorl	%r13d, %r13d
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
	movq	-880(%rbp), %rax
	jmp	.L1030
.L1318:
	movq	-880(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	%rdx, -880(%rbp)
	movzwl	2(%rax), %edx
	cmpw	$43, %dx
	je	.L1338
	cmpw	$45, %dx
	je	.L1027
	xorl	%r13d, %r13d
.L1028:
	movq	-880(%rbp), %rax
	movq	-856(%rbp), %rdi
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	movq	%rax, %rdx
.L932:
	movzwl	(%rdx), %ecx
	leal	-48(%rcx), %r8d
	cmpw	$9, %r8w
	jbe	.L929
	testb	%sil, %sil
	je	.L926
.L1269:
	movq	%rdx, -880(%rbp)
	movq	%rdx, %rax
	jmp	.L926
.L1323:
	movl	%r10d, %r13d
	movzwl	%di, %eax
	jmp	.L1036
.L1315:
	movl	%r10d, %r9d
	movq	%rsi, %rdx
	movl	%ebx, %r10d
	movq	-904(%rbp), %rbx
	jmp	.L882
.L1311:
	addq	$2, %rax
	cmpq	%rax, -864(%rbp)
	movq	%rax, -856(%rbp)
	sete	%al
	jmp	.L835
.L929:
	leal	97200(%r12), %esi
	cmpl	$194400, %esi
	ja	.L931
	imull	$10, %r12d, %r12d
	leal	-48(%r12,%rcx), %r12d
.L931:
	addq	$2, %rdx
	movl	$1, %esi
	cmpq	%rdi, %rdx
	jne	.L932
	jmp	.L1269
.L1027:
	addq	$4, %rax
	movl	$1, %r13d
	movq	%rax, -880(%rbp)
	jmp	.L1028
.L1338:
	addq	$4, %rax
	xorl	%r13d, %r13d
	movq	%rax, -880(%rbp)
	jmp	.L1028
.L1333:
	testw	%bx, %bx
	je	.L1339
	movl	-888(%rbp), %esi
	leaq	-880(%rbp), %rdi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
.L903:
	movq	-880(%rbp), %rsi
	movzwl	(%rsi), %eax
	leal	-48(%rax), %esi
	cmpw	$9, %si
	jbe	.L1031
	movzwl	%ax, %esi
	movl	%r12d, %r8d
	jmp	.L1032
.L1337:
	testb	$1, %r14b
	jne	.L1063
	testb	%r8b, %r8b
	jne	.L914
.L1063:
	addq	$1, %r14
	jmp	.L914
.L1031:
	cmpw	$48, %ax
	movl	%r12d, %r8d
	sete	%al
	andl	%eax, %r10d
	jmp	.L897
.L1339:
	addq	$2, %rsi
	movq	%rsi, -880(%rbp)
	jmp	.L903
.L842:
	testw	%r11w, %r11w
	je	.L1340
	leaq	-856(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN6icu_6717double_conversionL7AdvanceIPKtEEbPT_tiRS4_.part.0.constprop.0
	movl	%eax, %r10d
.L846:
	testb	%r10b, %r10b
	je	.L848
.L847:
	movq	-880(%rbp), %rdx
	movl	%r10d, %r15d
	movzwl	(%rdx), %edi
	jmp	.L852
.L1312:
	addq	$2, %rax
	cmpq	%rax, -864(%rbp)
	movq	%rax, -856(%rbp)
	sete	%r10b
	jmp	.L840
.L1340:
	addq	$2, %r8
	cmpq	%r8, -864(%rbp)
	movq	%r8, -856(%rbp)
	sete	%r10b
	jmp	.L846
	.cfi_endproc
.LFE428:
	.size	_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKtEEdT_ibPi, .-_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKtEEdT_ibPi
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717double_conversion23StringToDoubleConverter14StringToDoubleEPKtiPi
	.type	_ZNK6icu_6717double_conversion23StringToDoubleConverter14StringToDoubleEPKtiPi, @function
_ZNK6icu_6717double_conversion23StringToDoubleConverter14StringToDoubleEPKtiPi:
.LFB414:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	movl	$1, %ecx
	jmp	_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKtEEdT_ibPi
	.cfi_endproc
.LFE414:
	.size	_ZNK6icu_6717double_conversion23StringToDoubleConverter14StringToDoubleEPKtiPi, .-_ZNK6icu_6717double_conversion23StringToDoubleConverter14StringToDoubleEPKtiPi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717double_conversion23StringToDoubleConverter13StringToFloatEPKtiPi
	.type	_ZNK6icu_6717double_conversion23StringToDoubleConverter13StringToFloatEPKtiPi, @function
_ZNK6icu_6717double_conversion23StringToDoubleConverter13StringToFloatEPKtiPi:
.LFB416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6717double_conversion23StringToDoubleConverter12StringToIeeeIPKtEEdT_ibPi
	popq	%rbp
	.cfi_def_cfa 7, 8
	cvtsd2ss	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE416:
	.size	_ZNK6icu_6717double_conversion23StringToDoubleConverter13StringToFloatEPKtiPi, .-_ZNK6icu_6717double_conversion23StringToDoubleConverter13StringToFloatEPKtiPi
	.section	.rodata
	.align 32
	.type	_ZN6icu_6717double_conversionL18kWhitespaceTable16E, @object
	.size	_ZN6icu_6717double_conversionL18kWhitespaceTable16E, 40
_ZN6icu_6717double_conversionL18kWhitespaceTable16E:
	.value	160
	.value	8232
	.value	8233
	.value	5760
	.value	6158
	.value	8192
	.value	8193
	.value	8194
	.value	8195
	.value	8196
	.value	8197
	.value	8198
	.value	8199
	.value	8200
	.value	8201
	.value	8202
	.value	8239
	.value	8287
	.value	12288
	.value	-257
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	2146435072
	.align 8
.LC2:
	.long	0
	.long	-2147483648
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC4:
	.long	0
	.long	2146959360
	.align 8
.LC5:
	.long	0
	.long	-524288
	.align 8
.LC6:
	.long	0
	.long	-1048576
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
