	.file	"listformatter.cpp"
	.text
	.align 2
	.p2align 4
	.type	_ZNK6icu_6712_GLOBAL__N_114PatternHandler13getTwoPatternERKNS_13UnicodeStringE, @function
_ZNK6icu_6712_GLOBAL__N_114PatternHandler13getTwoPatternERKNS_13UnicodeStringE:
.LFB3375:
	.cfi_startproc
	endbr64
	leaq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE3375:
	.size	_ZNK6icu_6712_GLOBAL__N_114PatternHandler13getTwoPatternERKNS_13UnicodeStringE, .-_ZNK6icu_6712_GLOBAL__N_114PatternHandler13getTwoPatternERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.type	_ZNK6icu_6712_GLOBAL__N_114PatternHandler13getEndPatternERKNS_13UnicodeStringE, @function
_ZNK6icu_6712_GLOBAL__N_114PatternHandler13getEndPatternERKNS_13UnicodeStringE:
.LFB3376:
	.cfi_startproc
	endbr64
	leaq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE3376:
	.size	_ZNK6icu_6712_GLOBAL__N_114PatternHandler13getEndPatternERKNS_13UnicodeStringE, .-_ZNK6icu_6712_GLOBAL__N_114PatternHandler13getEndPatternERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.type	_ZNK6icu_6712_GLOBAL__N_117ContextualHandler13getTwoPatternERKNS_13UnicodeStringE, @function
_ZNK6icu_6712_GLOBAL__N_117ContextualHandler13getTwoPatternERKNS_13UnicodeStringE:
.LFB3388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	*152(%rbx)
	movl	%eax, %r8d
	leaq	160(%rbx), %rax
	addq	$8, %rbx
	testb	%r8b, %r8b
	cmove	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3388:
	.size	_ZNK6icu_6712_GLOBAL__N_117ContextualHandler13getTwoPatternERKNS_13UnicodeStringE, .-_ZNK6icu_6712_GLOBAL__N_117ContextualHandler13getTwoPatternERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.type	_ZNK6icu_6712_GLOBAL__N_117ContextualHandler13getEndPatternERKNS_13UnicodeStringE, @function
_ZNK6icu_6712_GLOBAL__N_117ContextualHandler13getEndPatternERKNS_13UnicodeStringE:
.LFB3389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	*152(%rbx)
	movl	%eax, %r8d
	leaq	232(%rbx), %rax
	addq	$80, %rbx
	testb	%r8b, %r8b
	cmove	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3389:
	.size	_ZNK6icu_6712_GLOBAL__N_117ContextualHandler13getEndPatternERKNS_13UnicodeStringE, .-_ZNK6icu_6712_GLOBAL__N_117ContextualHandler13getEndPatternERKNS_13UnicodeStringE
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_1L15shouldChangeToEERKNS_13UnicodeStringE, @function
_ZN6icu_6712_GLOBAL__N_1L15shouldChangeToEERKNS_13UnicodeStringE:
.LFB3394:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L13
	movswl	%dx, %eax
	sarl	$5, %eax
.L14:
	xorl	%r8d, %r8d
	testl	%eax, %eax
	je	.L12
	andl	$2, %edx
	je	.L16
	movzwl	10(%rdi), %edx
	cmpw	$104, %dx
	je	.L68
	cmpw	$72, %dx
	je	.L68
.L32:
	movl	$1, %r8d
	cmpw	$105, %dx
	je	.L12
	cmpw	$73, %dx
	sete	%r8b
.L12:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	24(%rdi), %rcx
	movzwl	(%rcx), %edx
	cmpw	$104, %dx
	je	.L70
	cmpw	$72, %dx
	jne	.L32
.L70:
	cmpl	$1, %eax
	jle	.L12
	movzwl	2(%rcx), %edx
	cmpw	$105, %dx
	je	.L39
	cmpw	$73, %dx
	je	.L39
.L71:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	12(%rdi), %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L68:
	cmpl	$1, %eax
	jle	.L12
	movzwl	12(%rdi), %edx
	cmpw	$105, %dx
	je	.L37
	cmpw	$73, %dx
	jne	.L71
.L37:
	movl	$1, %r8d
	cmpl	$2, %eax
	je	.L12
	movzwl	14(%rdi), %eax
.L35:
	cmpw	$97, %ax
	je	.L71
	cmpw	$65, %ax
	je	.L71
	cmpw	$69, %ax
	setne	%r8b
	cmpw	$101, %ax
	movl	$0, %eax
	cmove	%eax, %r8d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$1, %r8d
	cmpl	$2, %eax
	je	.L12
	movzwl	4(%rcx), %eax
	jmp	.L35
	.cfi_endproc
.LFE3394:
	.size	_ZN6icu_6712_GLOBAL__N_1L15shouldChangeToEERKNS_13UnicodeStringE, .-_ZN6icu_6712_GLOBAL__N_1L15shouldChangeToEERKNS_13UnicodeStringE
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_1L15shouldChangeToUERKNS_13UnicodeStringE, @function
_ZN6icu_6712_GLOBAL__N_1L15shouldChangeToUERKNS_13UnicodeStringE:
.LFB3395:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %ecx
	testw	%cx, %cx
	js	.L76
	movswl	%cx, %edx
	sarl	$5, %edx
.L77:
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L75
	andl	$2, %ecx
	je	.L79
	movzwl	10(%rdi), %ecx
	movl	%ecx, %eax
	andl	$-33, %eax
	cmpw	$79, %ax
	sete	%al
	cmpw	$56, %cx
	sete	%sil
	orb	%sil, %al
	je	.L104
.L75:
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	movq	24(%rdi), %rdi
	movzwl	(%rdi), %ecx
	movl	%ecx, %eax
	andl	$-33, %eax
	cmpw	$79, %ax
	sete	%al
	cmpw	$56, %cx
	sete	%sil
	orb	%sil, %al
	jne	.L75
	cmpw	$104, %cx
	jne	.L103
.L82:
	xorl	%eax, %eax
	cmpl	$1, %edx
	jle	.L75
	movzwl	2(%rdi), %eax
	andl	$-33, %eax
	cmpw	$79, %ax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	movl	12(%rdi), %edx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L104:
	addq	$10, %rdi
	cmpw	$104, %cx
	je	.L82
.L103:
	cmpl	$1, %edx
	setg	%sil
	cmpw	$72, %cx
	je	.L82
	cmpw	$49, %cx
	sete	%al
	andb	%sil, %al
	je	.L75
	cmpw	$49, 2(%rdi)
	je	.L105
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	cmpl	$2, %edx
	je	.L75
	cmpw	$32, 4(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE3395:
	.size	_ZN6icu_6712_GLOBAL__N_1L15shouldChangeToUERKNS_13UnicodeStringE, .-_ZN6icu_6712_GLOBAL__N_1L15shouldChangeToUERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717FormattedListDataD2Ev
	.type	_ZN6icu_6717FormattedListDataD2Ev, @function
_ZN6icu_6717FormattedListDataD2Ev:
.LFB3413:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717FormattedListDataE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD2Ev@PLT
	.cfi_endproc
.LFE3413:
	.size	_ZN6icu_6717FormattedListDataD2Ev, .-_ZN6icu_6717FormattedListDataD2Ev
	.globl	_ZN6icu_6717FormattedListDataD1Ev
	.set	_ZN6icu_6717FormattedListDataD1Ev,_ZN6icu_6717FormattedListDataD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717FormattedListDataD0Ev
	.type	_ZN6icu_6717FormattedListDataD0Ev, @function
_ZN6icu_6717FormattedListDataD0Ev:
.LFB3415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717FormattedListDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3415:
	.size	_ZN6icu_6717FormattedListDataD0Ev, .-_ZN6icu_6717FormattedListDataD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713FormattedList8toStringER10UErrorCode
	.type	_ZNK6icu_6713FormattedList8toStringER10UErrorCode, @function
_ZNK6icu_6713FormattedList8toStringER10UErrorCode:
.LFB3424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L115
	movq	8(%rsi), %r8
	testq	%r8, %r8
	je	.L116
	movq	(%r8), %rax
	movq	%r8, %rsi
	call	*16(%rax)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L116:
	movl	16(%rsi), %eax
	movl	%eax, (%rdx)
	.p2align 4,,10
	.p2align 3
.L115:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L109:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L117:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3424:
	.size	_ZNK6icu_6713FormattedList8toStringER10UErrorCode, .-_ZNK6icu_6713FormattedList8toStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713FormattedList12toTempStringER10UErrorCode
	.type	_ZNK6icu_6713FormattedList12toTempStringER10UErrorCode, @function
_ZNK6icu_6713FormattedList12toTempStringER10UErrorCode:
.LFB3425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L124
	movq	8(%rsi), %r8
	testq	%r8, %r8
	je	.L125
	movq	(%r8), %rax
	movq	%r8, %rsi
	call	*24(%rax)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L125:
	movl	16(%rsi), %eax
	movl	%eax, (%rdx)
	.p2align 4,,10
	.p2align 3
.L124:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L118:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L126:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3425:
	.size	_ZNK6icu_6713FormattedList12toTempStringER10UErrorCode, .-_ZNK6icu_6713FormattedList12toTempStringER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6712_GLOBAL__N_114PatternHandler5cloneEv, @function
_ZNK6icu_6712_GLOBAL__N_114PatternHandler5cloneEv:
.LFB3374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$152, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L127
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_114PatternHandlerE(%rip), %rax
	leaq	16(%rbx), %rsi
	movq	%rax, (%r12)
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	88(%rbx), %rsi
	leaq	88(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L127:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3374:
	.size	_ZNK6icu_6712_GLOBAL__N_114PatternHandler5cloneEv, .-_ZNK6icu_6712_GLOBAL__N_114PatternHandler5cloneEv
	.align 2
	.p2align 4
	.type	_ZNK6icu_6712_GLOBAL__N_117ContextualHandler5cloneEv, @function
_ZNK6icu_6712_GLOBAL__N_117ContextualHandler5cloneEv:
.LFB3387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-200(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-120(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	88(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$304, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L134
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_114PatternHandlerE(%rip), %rax
	movq	152(%rbx), %r15
	leaq	16(%r12), %rdi
	movq	%r14, %rsi
	movq	%rax, (%r12)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	88(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_117ContextualHandlerE(%rip), %rax
	movq	%r15, 152(%r12)
	leaq	168(%rbx), %rsi
	movq	%rax, (%r12)
	leaq	168(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	240(%rbx), %rsi
	leaq	240(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L134:
	leaq	-128(%rbp), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	-208(%rbp), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L140
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L140:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3387:
	.size	_ZNK6icu_6712_GLOBAL__N_117ContextualHandler5cloneEv, .-_ZNK6icu_6712_GLOBAL__N_117ContextualHandler5cloneEv
	.p2align 4
	.type	uprv_listformatter_cleanup, @function
uprv_listformatter_cleanup:
.LFB3428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	_ZN6icu_67L15listPatternHashE(%rip), %r12
	testq	%r12, %r12
	je	.L142
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L143
	call	uhash_close_67@PLT
.L143:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L142:
	movq	$0, _ZN6icu_67L15listPatternHashE(%rip)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3428:
	.size	uprv_listformatter_cleanup, .-uprv_listformatter_cleanup
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatter16ListPatternsSinkD2Ev
	.type	_ZN6icu_6713ListFormatter16ListPatternsSinkD2Ev, @function
_ZN6icu_6713ListFormatter16ListPatternsSinkD2Ev:
.LFB3453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713ListFormatter16ListPatternsSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	200(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -200(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE3453:
	.size	_ZN6icu_6713ListFormatter16ListPatternsSinkD2Ev, .-_ZN6icu_6713ListFormatter16ListPatternsSinkD2Ev
	.globl	_ZN6icu_6713ListFormatter16ListPatternsSinkD1Ev
	.set	_ZN6icu_6713ListFormatter16ListPatternsSinkD1Ev,_ZN6icu_6713ListFormatter16ListPatternsSinkD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_114PatternHandlerD2Ev, @function
_ZN6icu_6712_GLOBAL__N_114PatternHandlerD2Ev:
.LFB3378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_114PatternHandlerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	80(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -80(%rdi)
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3378:
	.size	_ZN6icu_6712_GLOBAL__N_114PatternHandlerD2Ev, .-_ZN6icu_6712_GLOBAL__N_114PatternHandlerD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_114PatternHandlerD1Ev,_ZN6icu_6712_GLOBAL__N_114PatternHandlerD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713FormattedList8appendToERNS_10AppendableER10UErrorCode
	.type	_ZNK6icu_6713FormattedList8appendToERNS_10AppendableER10UErrorCode, @function
_ZNK6icu_6713FormattedList8appendToERNS_10AppendableER10UErrorCode:
.LFB3426:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L155
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L159
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	32(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L159:
	movl	16(%rdi), %eax
	movl	%eax, (%rdx)
.L155:
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3426:
	.size	_ZNK6icu_6713FormattedList8appendToERNS_10AppendableER10UErrorCode, .-_ZNK6icu_6713FormattedList8appendToERNS_10AppendableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713FormattedList12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.type	_ZNK6icu_6713FormattedList12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode, @function
_ZNK6icu_6713FormattedList12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode:
.LFB3427:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L160
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L164
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	40(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L164:
	movl	16(%rdi), %eax
	movl	%eax, (%rdx)
.L160:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3427:
	.size	_ZNK6icu_6713FormattedList12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode, .-_ZNK6icu_6713FormattedList12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_1L21shouldChangeToVavDashERKNS_13UnicodeStringE, @function
_ZN6icu_6712_GLOBAL__N_1L21shouldChangeToVavDashERKNS_13UnicodeStringE:
.LFB3396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movswl	8(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	shrl	$5, %edx
	je	.L165
	xorl	%esi, %esi
	movl	$0, -12(%rbp)
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	leaq	-12(%rbp), %rsi
	movl	%eax, %edi
	call	uscript_getScript_67@PLT
	cmpl	$19, %eax
	setne	%al
.L165:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L171
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L171:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3396:
	.size	_ZN6icu_6712_GLOBAL__N_1L21shouldChangeToVavDashERKNS_13UnicodeStringE, .-_ZN6icu_6712_GLOBAL__N_1L21shouldChangeToVavDashERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_114PatternHandlerD0Ev, @function
_ZN6icu_6712_GLOBAL__N_114PatternHandlerD0Ev:
.LFB3380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_114PatternHandlerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	80(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -80(%rdi)
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3380:
	.size	_ZN6icu_6712_GLOBAL__N_114PatternHandlerD0Ev, .-_ZN6icu_6712_GLOBAL__N_114PatternHandlerD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_117ContextualHandlerD0Ev, @function
_ZN6icu_6712_GLOBAL__N_117ContextualHandlerD0Ev:
.LFB3393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_117ContextualHandlerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	232(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -232(%rdi)
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	160(%r12), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_114PatternHandlerE(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3393:
	.size	_ZN6icu_6712_GLOBAL__N_117ContextualHandlerD0Ev, .-_ZN6icu_6712_GLOBAL__N_117ContextualHandlerD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_117ContextualHandlerD2Ev, @function
_ZN6icu_6712_GLOBAL__N_117ContextualHandlerD2Ev:
.LFB3391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_117ContextualHandlerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	232(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -232(%rdi)
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	160(%r12), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_114PatternHandlerE(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3391:
	.size	_ZN6icu_6712_GLOBAL__N_117ContextualHandlerD2Ev, .-_ZN6icu_6712_GLOBAL__N_117ContextualHandlerD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_117ContextualHandlerD1Ev,_ZN6icu_6712_GLOBAL__N_117ContextualHandlerD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713FormattedListD2Ev
	.type	_ZN6icu_6713FormattedListD2Ev, @function
_ZN6icu_6713FormattedListD2Ev:
.LFB3420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713FormattedListE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L179
	movq	0(%r13), %rax
	leaq	_ZN6icu_6717FormattedListDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L180
	leaq	16+_ZTVN6icu_6717FormattedListDataE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L179:
	movq	$0, 8(%r12)
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714FormattedValueD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L179
	.cfi_endproc
.LFE3420:
	.size	_ZN6icu_6713FormattedListD2Ev, .-_ZN6icu_6713FormattedListD2Ev
	.globl	_ZN6icu_6713FormattedListD1Ev
	.set	_ZN6icu_6713FormattedListD1Ev,_ZN6icu_6713FormattedListD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713FormattedListD0Ev
	.type	_ZN6icu_6713FormattedListD0Ev, @function
_ZN6icu_6713FormattedListD0Ev:
.LFB3422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713FormattedListE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L186
	movq	0(%r13), %rax
	leaq	_ZN6icu_6717FormattedListDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L187
	leaq	16+_ZTVN6icu_6717FormattedListDataE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L186:
	movq	$0, 8(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_6714FormattedValueD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L186
	.cfi_endproc
.LFE3422:
	.size	_ZN6icu_6713FormattedListD0Ev, .-_ZN6icu_6713FormattedListD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatter16ListPatternsSinkD0Ev
	.type	_ZN6icu_6713ListFormatter16ListPatternsSinkD0Ev, @function
_ZN6icu_6713ListFormatter16ListPatternsSinkD0Ev:
.LFB3455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713ListFormatter16ListPatternsSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	200(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -200(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3455:
	.size	_ZN6icu_6713ListFormatter16ListPatternsSinkD0Ev, .-_ZN6icu_6713ListFormatter16ListPatternsSinkD0Ev
	.p2align 4
	.type	uprv_deleteListFormatInternal, @function
uprv_deleteListFormatInternal:
.LFB3429:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L194
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	152(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L196
	movq	(%rdi), %rax
	call	*8(%rax)
.L196:
	leaq	80(%r12), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L194:
	ret
	.cfi_endproc
.LFE3429:
	.size	uprv_deleteListFormatInternal, .-uprv_deleteListFormatInternal
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatterD0Ev
	.type	_ZN6icu_6713ListFormatterD0Ev, @function
_ZN6icu_6713ListFormatterD0Ev:
.LFB3470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713ListFormatterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L204
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L205
	movq	(%rdi), %rax
	call	*8(%rax)
.L205:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L204:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3470:
	.size	_ZN6icu_6713ListFormatterD0Ev, .-_ZN6icu_6713ListFormatterD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatterD2Ev
	.type	_ZN6icu_6713ListFormatterD2Ev, @function
_ZN6icu_6713ListFormatterD2Ev:
.LFB3468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713ListFormatterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L214
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L215
	movq	(%rdi), %rax
	call	*8(%rax)
.L215:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L214:
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3468:
	.size	_ZN6icu_6713ListFormatterD2Ev, .-_ZN6icu_6713ListFormatterD2Ev
	.globl	_ZN6icu_6713ListFormatterD1Ev
	.set	_ZN6icu_6713ListFormatterD1Ev,_ZN6icu_6713ListFormatterD2Ev
	.section	.rodata._ZN6icu_6713ListFormatter16ListPatternsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode.str1.1,"aMS",@progbits,1
.LC0:
	.string	"end"
.LC1:
	.string	"middle"
.LC2:
	.string	"start"
	.section	.text._ZN6icu_6713ListFormatter16ListPatternsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode,"axG",@progbits,_ZN6icu_6713ListFormatter16ListPatternsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6713ListFormatter16ListPatternsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.type	_ZN6icu_6713ListFormatter16ListPatternsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, @function
_ZN6icu_6713ListFormatter16ListPatternsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode:
.LFB3451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -208(%rbp)
	movq	%rsi, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, 264(%rdi)
	movq	(%rdx), %rax
	movq	%rdx, %rdi
	call	*24(%rax)
	cmpl	$3, %eax
	je	.L318
	movq	(%r15), %rax
	leaq	-176(%rbp), %r14
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*88(%rax)
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L223
	leaq	-200(%rbp), %rax
	leaq	-188(%rbp), %rcx
	movq	%r12, -216(%rbp)
	xorl	%ebx, %ebx
	movq	%r15, %r12
	movq	%rcx, -224(%rbp)
	movq	%rax, %r15
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L319:
	cmpb	$0, 1(%rax)
	jne	.L286
	movq	-208(%rbp), %rax
	movswl	16(%rax), %r13d
	sarl	$5, %r13d
	jne	.L236
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	cmpl	$3, %eax
	je	.L316
	movq	(%r12), %rax
	movq	-216(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-128(%rbp), %r13
	movq	-224(%rbp), %rsi
	movl	$0, -188(%rbp)
	call	*32(%rax)
	movl	-188(%rbp), %ecx
	leaq	-184(%rbp), %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-208(%rbp), %rax
	movq	%r13, %rsi
	leaq	8(%rax), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L236:
	movq	-216(%rbp), %rax
	addl	$1, %ebx
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L223
.L264:
	movq	%r12, %rcx
	movq	%r15, %rdx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L223
	movq	-200(%rbp), %rax
	cmpb	$50, (%rax)
	je	.L319
.L286:
	movl	$4, %ecx
	movq	%rax, %rsi
	leaq	.LC0(%rip), %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L320
	movl	$7, %ecx
	movq	%rax, %rsi
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L321
	movq	%rax, %rsi
	movl	$6, %ecx
	leaq	.LC2(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L236
	movq	-208(%rbp), %rax
	movswl	80(%rax), %r13d
	sarl	$5, %r13d
	jne	.L236
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	cmpl	$3, %eax
	jne	.L258
	movq	-208(%rbp), %rax
	cmpb	$0, 264(%rax)
	jne	.L236
	movq	(%r12), %rax
	movq	-216(%rbp), %rdx
	movq	%r12, %rdi
	movl	$0, -188(%rbp)
	movq	-224(%rbp), %rsi
	call	*40(%rax)
	movl	-188(%rbp), %ecx
	leaq	-128(%rbp), %rdi
	leaq	-184(%rbp), %rdx
	movl	$1, %esi
	movq	%rax, -184(%rbp)
	movq	%rdi, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	movq	-232(%rbp), %rdi
	testw	%ax, %ax
	js	.L259
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L260:
	xorl	%edx, %edx
	movl	%r13d, %r8d
	movl	$12, %ecx
	movq	%rdi, -232(%rbp)
	leaq	_ZN6icu_67L11aliasPrefixE(%rip), %rsi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movq	-232(%rbp), %rdi
	testl	%eax, %eax
	js	.L302
	leal	12(%rax), %r13d
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L322
	movswl	%ax, %ecx
	movl	%r13d, %edx
	sarl	$5, %ecx
	cmpl	%r13d, %ecx
	cmovle	%ecx, %edx
.L261:
	subl	%edx, %ecx
	movl	$47, %esi
	movq	%rdi, -232(%rbp)
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movq	-232(%rbp), %rdi
	testl	%eax, %eax
	js	.L323
.L262:
	movq	-208(%rbp), %rsi
	subl	%r13d, %eax
	xorl	%r9d, %r9d
	movl	$25, %r8d
	movl	%eax, %edx
	movq	%rdi, -232(%rbp)
	leaq	264(%rsi), %rcx
	movl	%r13d, %esi
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movq	-208(%rbp), %rsi
	movq	-232(%rbp), %rdi
	movb	$0, 288(%rsi)
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L320:
	movq	-208(%rbp), %rax
	movswl	208(%rax), %r13d
	sarl	$5, %r13d
	jne	.L236
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	cmpl	$3, %eax
	jne	.L245
.L316:
	movq	-208(%rbp), %rax
	cmpb	$0, 264(%rax)
	jne	.L236
	movq	(%r12), %rax
	movq	-216(%rbp), %rdx
	movq	%r12, %rdi
	movl	$0, -188(%rbp)
	movq	-224(%rbp), %rsi
	call	*40(%rax)
	movl	-188(%rbp), %ecx
	leaq	-128(%rbp), %rdi
	leaq	-184(%rbp), %rdx
	movl	$1, %esi
	movq	%rax, -184(%rbp)
	movq	%rdi, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	movq	-232(%rbp), %rdi
	testw	%ax, %ax
	js	.L253
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L247:
	xorl	%edx, %edx
	movl	%r13d, %r8d
	movl	$12, %ecx
	movq	%rdi, -232(%rbp)
	leaq	_ZN6icu_67L11aliasPrefixE(%rip), %rsi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movq	-232(%rbp), %rdi
	testl	%eax, %eax
	jns	.L324
.L302:
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-216(%rbp), %rax
	addl	$1, %ebx
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L264
	.p2align 4,,10
	.p2align 3
.L223:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L325
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	movq	-208(%rbp), %rax
	movswl	144(%rax), %r13d
	sarl	$5, %r13d
	jne	.L236
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	cmpl	$3, %eax
	je	.L316
	movq	(%r12), %rax
	movq	-216(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-128(%rbp), %r13
	movq	-224(%rbp), %rsi
	movl	$0, -188(%rbp)
	call	*32(%rax)
	movl	-188(%rbp), %ecx
	leaq	-184(%rbp), %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-208(%rbp), %rax
	movq	%r13, %rsi
	leaq	136(%rax), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L245:
	movq	(%r12), %rax
	movq	-216(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-128(%rbp), %r13
	movq	-224(%rbp), %rsi
	movl	$0, -188(%rbp)
	call	*32(%rax)
	movl	-188(%rbp), %ecx
	leaq	-184(%rbp), %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-208(%rbp), %rax
	movq	%r13, %rsi
	leaq	200(%rax), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L258:
	movq	(%r12), %rax
	movq	-216(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-128(%rbp), %r13
	movq	-224(%rbp), %rsi
	movl	$0, -188(%rbp)
	call	*32(%rax)
	movl	-188(%rbp), %ecx
	leaq	-184(%rbp), %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-208(%rbp), %rax
	movq	%r13, %rsi
	leaq	72(%rax), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L253:
	movl	-116(%rbp), %r9d
	testl	%r9d, %r9d
	cmovle	%r9d, %r13d
	subl	%r13d, %r9d
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L324:
	leal	12(%rax), %r13d
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L326
	movswl	%ax, %ecx
	movl	%r13d, %edx
	sarl	$5, %ecx
	cmpl	%r13d, %ecx
	cmovle	%ecx, %edx
.L255:
	subl	%edx, %ecx
	movl	$47, %esi
	movq	%rdi, -232(%rbp)
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movq	-232(%rbp), %rdi
	testl	%eax, %eax
	js	.L327
.L256:
	movq	-208(%rbp), %rcx
	subl	%r13d, %eax
	xorl	%r9d, %r9d
	movl	%r13d, %esi
	movl	%eax, %edx
	movl	$25, %r8d
	movq	%rdi, -232(%rbp)
	addq	$264, %rcx
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movq	-208(%rbp), %rcx
	movb	$0, 288(%rcx)
	movq	-232(%rbp), %rdi
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L318:
	movq	(%r15), %rax
	leaq	-184(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	$0, -184(%rbp)
	leaq	-128(%rbp), %r13
	call	*40(%rax)
	movl	-184(%rbp), %ecx
	leaq	-176(%rbp), %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -176(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L225
	movswl	%ax, %r9d
	xorl	%r8d, %r8d
	sarl	$5, %r9d
.L226:
	xorl	%edx, %edx
	movl	$12, %ecx
	leaq	_ZN6icu_67L11aliasPrefixE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	testl	%eax, %eax
	jns	.L328
.L267:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L327:
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L257
	sarl	$5, %eax
	jmp	.L256
.L326:
	movl	-116(%rbp), %ecx
	movl	%r13d, %edx
	cmpl	%r13d, %ecx
	cmovle	%ecx, %edx
	jmp	.L255
.L259:
	movl	-116(%rbp), %r9d
	testl	%r9d, %r9d
	cmovle	%r9d, %r13d
	subl	%r13d, %r9d
	jmp	.L260
.L225:
	movl	-116(%rbp), %r9d
	movl	$0, %r8d
	testl	%r9d, %r9d
	cmovle	%r9d, %r8d
	subl	%r8d, %r9d
	jmp	.L226
.L257:
	movl	-116(%rbp), %eax
	jmp	.L256
.L328:
	leal	12(%rax), %r12d
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L329
	movswl	%ax, %ecx
	movl	%r12d, %edx
	sarl	$5, %ecx
	cmpl	%r12d, %ecx
	cmovle	%ecx, %edx
.L227:
	subl	%edx, %ecx
	movl	$47, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	js	.L330
.L228:
	movq	-208(%rbp), %rbx
	subl	%r12d, %eax
	xorl	%r9d, %r9d
	movl	%r12d, %esi
	movl	%eax, %edx
	movl	$25, %r8d
	movq	%r13, %rdi
	leaq	264(%rbx), %rcx
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movb	$0, 288(%rbx)
	jmp	.L267
.L329:
	movl	-116(%rbp), %ecx
	movl	%r12d, %edx
	cmpl	%r12d, %ecx
	cmovle	%ecx, %edx
	jmp	.L227
.L330:
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L229
	sarl	$5, %eax
	jmp	.L228
.L323:
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L263
	sarl	$5, %eax
	jmp	.L262
.L322:
	movl	-116(%rbp), %ecx
	movl	%r13d, %edx
	cmpl	%r13d, %ecx
	cmovle	%ecx, %edx
	jmp	.L261
.L229:
	movl	-116(%rbp), %eax
	jmp	.L228
.L263:
	movl	-116(%rbp), %eax
	jmp	.L262
.L325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3451:
	.size	_ZN6icu_6713ListFormatter16ListPatternsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, .-_ZN6icu_6713ListFormatter16ListPatternsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.text
	.align 2
	.p2align 4
	.type	_ZNK6icu_6713ListFormatter7format_EPKNS_13UnicodeStringEiRS1_iRiPNS_20FieldPositionHandlerER10UErrorCode.part.0, @function
_ZNK6icu_6713ListFormatter7format_EPKNS_13UnicodeStringEiRS1_iRiPNS_20FieldPositionHandlerER10UErrorCode.part.0:
.LFB4747:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -248(%rbp)
	movq	16(%rbp), %r13
	movq	%rsi, -240(%rbp)
	movq	24(%rbp), %rbx
	movl	%edx, -232(%rbp)
	movq	%rcx, -256(%rbp)
	movl	%r8d, -264(%rbp)
	movq	%r9, -272(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %edx
	je	.L462
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	-264(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L344
	movq	-272(%rbp), %rax
	movl	$0, (%rax)
.L344:
	testq	%r13, %r13
	je	.L463
	movl	-232(%rbp), %eax
	movb	$0, -180(%rbp)
	movl	$10, -184(%rbp)
	leal	2(%rax,%rax), %r12d
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	cmpl	$10, %r12d
	jle	.L346
	movslq	%r12d, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L346
	cmpb	$0, -180(%rbp)
	jne	.L464
.L349:
	movq	%r14, -192(%rbp)
	movl	%r12d, -184(%rbp)
	movb	$1, -180(%rbp)
.L346:
	movq	-240(%rbp), %rax
	cmpl	$2, -232(%rbp)
	leaq	64(%rax), %r12
	movq	-248(%rbp), %rax
	movq	16(%rax), %rdi
	jne	.L350
	movq	152(%rdi), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L465
.L354:
	movl	$0, -228(%rbp)
	xorl	%r11d, %r11d
.L357:
	testq	%r13, %r13
	jne	.L466
.L459:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L467
	.p2align 4,,10
	.p2align 3
.L388:
	cmpb	$0, -180(%rbp)
	jne	.L468
.L411:
	movq	-280(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L331:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L469
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L462:
	.cfi_restore_state
	testl	%r8d, %r8d
	jne	.L333
	movswl	8(%rcx), %eax
	testw	%ax, %ax
	js	.L334
	sarl	$5, %eax
	movq	%r9, %rbx
	movl	%eax, (%rbx)
.L333:
	testq	%r13, %r13
	je	.L336
	movq	0(%r13), %rax
	movq	16(%rax), %r8
	movq	-256(%rbp), %rax
	movswl	8(%rax), %edx
	testw	%dx, %dx
	js	.L337
	movq	-240(%rbp), %rax
	sarl	$5, %edx
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	js	.L339
.L479:
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L340:
	addl	%edx, %ecx
	movl	$1, %esi
	movq	%r13, %rdi
	call	*%r8
.L336:
	movq	-240(%rbp), %rax
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	js	.L341
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L342:
	movq	-240(%rbp), %rsi
	movq	-256(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L350:
	movl	(%rbx), %r14d
	testl	%r14d, %r14d
	jle	.L470
.L361:
	movl	$0, -228(%rbp)
	xorl	%r11d, %r11d
.L364:
	testq	%r13, %r13
	je	.L471
	movq	-192(%rbp), %rcx
	movl	-232(%rbp), %esi
	movl	%r11d, %eax
	subl	-228(%rbp), %eax
	movl	%eax, 4(%rcx)
	leal	-1(%rsi), %eax
	movl	%eax, -300(%rbp)
	movq	-248(%rbp), %rax
	movl	$0, (%rcx)
	movq	16(%rax), %rax
	cmpl	$3, %esi
	je	.L472
	leal	-4(%rsi), %r12d
	movq	-240(%rbp), %rsi
	movq	%r13, -312(%rbp)
	xorl	%r14d, %r14d
	movl	-228(%rbp), %r15d
	movl	%r11d, %r13d
	subq	$-128, %rsi
	movq	%rsi, -296(%rbp)
	movq	%rcx, %rsi
	movq	-280(%rbp), %rcx
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L382:
	addl	-228(%rbp), %r15d
	movl	%r13d, %edx
	subl	%r15d, %edx
	movl	%edx, 8(%rsi,%r14,4)
	leaq	1(%r14), %rdx
	cmpq	%r14, %r12
	je	.L473
	movq	%rdx, %r14
.L383:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L382
	subq	$8, %rsp
	movq	%r14, %rdx
	leaq	80(%rax), %rdi
	movl	$2, %r9d
	pushq	%rbx
	salq	$6, %rdx
	addq	-296(%rbp), %rdx
	leaq	-208(%rbp), %rsi
	movq	%rdx, -200(%rbp)
	leaq	-216(%rbp), %r8
	movl	$2, %edx
	movq	%rcx, -208(%rbp)
	movq	%rcx, -288(%rbp)
	call	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	movl	(%rbx), %eax
	popq	%r10
	movq	-288(%rbp), %rcx
	popq	%r11
	testl	%eax, %eax
	jg	.L457
	movl	-216(%rbp), %edx
	cmpl	$-1, %edx
	je	.L378
	movl	-212(%rbp), %esi
	cmpl	$-1, %esi
	je	.L378
	leal	2(%r14), %eax
	cmpl	%eax, -264(%rbp)
	je	.L474
	movq	-272(%rbp), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	js	.L381
	addl	%edx, %eax
	movl	%eax, (%rdi)
.L381:
	movq	-248(%rbp), %rax
	movl	%esi, %r13d
	movl	%edx, -228(%rbp)
	movq	-192(%rbp), %rsi
	movq	16(%rax), %rax
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L378:
	movl	$3, (%rbx)
.L457:
	movq	-248(%rbp), %rax
	movq	-192(%rbp), %rsi
	movq	16(%rax), %rax
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L473:
	movl	%r13d, %r11d
	movq	-312(%rbp), %r13
.L413:
	movslq	-232(%rbp), %rdx
	movq	152(%rax), %rdi
	movl	%r11d, -248(%rbp)
	movq	-240(%rbp), %rcx
	salq	$6, %rdx
	movq	(%rdi), %rax
	leaq	-64(%rcx,%rdx), %r14
	movq	%r14, %rsi
	call	*40(%rax)
	movl	(%rbx), %r9d
	movl	-248(%rbp), %r11d
	testl	%r9d, %r9d
	jle	.L475
.L384:
	testq	%r13, %r13
	je	.L388
.L387:
	addl	%r15d, -228(%rbp)
	movl	-228(%rbp), %esi
	movslq	-300(%rbp), %rax
	movq	-192(%rbp), %rdi
	subl	%esi, %r11d
	movl	%r11d, (%rdi,%rax,4)
	movq	-256(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L393
.L478:
	sarl	$5, %eax
	movl	%eax, -248(%rbp)
	movl	%eax, %r15d
.L394:
	movl	-232(%rbp), %esi
	addl	-228(%rbp), %r15d
	testl	%esi, %esi
	jle	.L395
	movq	-240(%rbp), %r12
	leal	-1(%rsi), %eax
	movslq	%esi, %r14
	movq	%rbx, -264(%rbp)
	salq	$2, %rax
	movl	%r15d, %ebx
	addq	$8, %r12
	movq	%rax, -240(%rbp)
	leaq	0(,%r14,4), %rax
	xorl	%r14d, %r14d
	movq	%r12, %r15
	movq	%r13, %r12
	movq	%r14, %r13
	movq	%rax, %r14
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L477:
	leaq	(%r14,%r13), %rsi
	sarl	$5, %eax
	addq	%rsi, %rdi
	addl	%ecx, %eax
.L461:
	addl	%ebx, %eax
	movl	$1, %esi
	addq	$64, %r15
	movl	%eax, (%rdi)
	movl	(%rdx), %r10d
	movq	(%r12), %rax
	addl	%ebx, %r10d
	movl	%r10d, (%rdx)
	movl	%r10d, %edx
	movl	(%rdi), %ecx
	movq	%r12, %rdi
	call	*16(%rax)
	cmpq	%r13, -240(%rbp)
	je	.L476
	movq	-192(%rbp), %rdi
	addq	$4, %r13
.L399:
	movswl	(%r15), %eax
	leaq	(%rdi,%r13), %rdx
	movl	(%rdx), %ecx
	testw	%ax, %ax
	jns	.L477
	leaq	(%r14,%r13), %rax
	addq	%rax, %rdi
	movl	4(%r15), %eax
	addl	%ecx, %eax
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L476:
	movl	%ebx, %r15d
	movq	-192(%rbp), %rdi
	movq	-264(%rbp), %rbx
	movq	%r12, %r13
.L395:
	movl	-232(%rbp), %eax
	movl	-248(%rbp), %ecx
	leal	(%rax,%rax), %esi
	movslq	%esi, %rax
	movl	%ecx, (%rdi,%rax,4)
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L400
	sarl	$5, %eax
.L401:
	leal	1(%rsi), %edx
	subq	$8, %rsp
	leal	(%r15,%rax), %r9d
	addl	$2, %esi
	subl	-228(%rbp), %r9d
	movslq	%edx, %rdx
	movq	uprv_int32Comparator_67@GOTPCREL(%rip), %rcx
	xorl	%r8d, %r8d
	movl	%r9d, (%rdi,%rdx,4)
	movl	$4, %edx
	xorl	%r9d, %r9d
	pushq	%rbx
	call	uprv_sortArray_67@PLT
	movl	-232(%rbp), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	js	.L459
	movslq	-232(%rbp), %rdx
	xorl	%r14d, %r14d
	leaq	8(,%rdx,8), %r12
	.p2align 4,,10
	.p2align 3
.L405:
	movq	-192(%rbp), %rax
	movl	(%rax,%r14), %edx
	movl	4(%rax,%r14), %ecx
	cmpl	%ecx, %edx
	je	.L404
	movq	0(%r13), %rax
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	*16(%rax)
.L404:
	addq	$8, %r14
	cmpq	%r14, %r12
	jne	.L405
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L388
.L467:
	movq	-272(%rbp), %rax
	movl	(%rax), %edx
.L392:
	testl	%edx, %edx
	js	.L406
	movq	-256(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L407
	sarl	$5, %eax
.L408:
	movq	-272(%rbp), %rbx
	addl	%edx, %eax
	movl	%eax, (%rbx)
.L406:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L409
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L410:
	movq	-280(%rbp), %rsi
	movq	-256(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	cmpb	$0, -180(%rbp)
	je	.L411
.L468:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L463:
	leaq	-176(%rbp), %rax
	movb	$0, -180(%rbp)
	movq	%rax, -192(%rbp)
	movl	$10, -184(%rbp)
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L474:
	movq	-272(%rbp), %rax
	movl	%esi, (%rax)
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L466:
	movq	-192(%rbp), %rdi
	movq	-256(%rbp), %rax
	subl	-228(%rbp), %r11d
	movl	$0, (%rdi)
	movl	%r11d, 4(%rdi)
	movswl	8(%rax), %eax
	testw	%ax, %ax
	jns	.L478
.L393:
	movq	-256(%rbp), %rax
	movl	12(%rax), %eax
	movl	%eax, -248(%rbp)
	movl	%eax, %r15d
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L465:
	subq	$8, %rsp
	movq	-280(%rbp), %rcx
	movq	%rax, %rdi
	leaq	-208(%rbp), %rsi
	pushq	%rbx
	movl	$2, %r9d
	movl	$2, %edx
	leaq	-216(%rbp), %r8
	movq	%rcx, -208(%rbp)
	movq	%r12, -200(%rbp)
	call	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	popq	%r15
	popq	%rax
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L354
	movl	-216(%rbp), %eax
	movl	%eax, -228(%rbp)
	cmpl	$-1, %eax
	je	.L352
	movl	-212(%rbp), %r11d
	cmpl	$-1, %r11d
	je	.L352
	cmpl	$1, -264(%rbp)
	jne	.L355
	movq	-272(%rbp), %rax
	movl	%r11d, (%rax)
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L470:
	subq	$8, %rsp
	addq	$8, %rdi
	movl	$2, %edx
	movq	-280(%rbp), %rcx
	pushq	%rbx
	movl	$2, %r9d
	leaq	-208(%rbp), %rsi
	leaq	-216(%rbp), %r8
	movq	%rcx, -208(%rbp)
	movq	%r12, -200(%rbp)
	call	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	movl	(%rbx), %r11d
	popq	%r9
	popq	%r10
	testl	%r11d, %r11d
	jg	.L361
	movl	-216(%rbp), %eax
	movl	%eax, -228(%rbp)
	cmpl	$-1, %eax
	je	.L359
	movl	-212(%rbp), %r11d
	cmpl	$-1, %r11d
	je	.L359
	cmpl	$1, -264(%rbp)
	jne	.L362
	movq	-272(%rbp), %rax
	movl	%r11d, (%rax)
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L409:
	movl	-116(%rbp), %ecx
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L337:
	movl	12(%rax), %edx
	movq	-240(%rbp), %rax
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	jns	.L479
.L339:
	movq	-240(%rbp), %rax
	movl	12(%rax), %ecx
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L464:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L349
.L471:
	cmpl	$2, -232(%rbp)
	je	.L459
	movl	-232(%rbp), %ecx
	leal	-1(%rcx), %eax
	movl	%eax, -300(%rbp)
	movq	-248(%rbp), %rax
	movq	16(%rax), %rax
	cmpl	$3, %ecx
	je	.L480
	movq	-240(%rbp), %rsi
	leal	-4(%rcx), %r15d
	xorl	%r14d, %r14d
	movq	%r13, -296(%rbp)
	movl	%r11d, -288(%rbp)
	movq	%r15, %r13
	movq	%rbx, %r15
	movq	-280(%rbp), %rbx
	leaq	128(%rsi), %r12
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L366:
	leaq	1(%r14), %rdx
	cmpq	%r14, %r13
	je	.L421
.L482:
	movq	%rdx, %r14
.L375:
	movl	(%r15), %r8d
	testl	%r8d, %r8d
	jg	.L366
	subq	$8, %rsp
	movq	%r14, %rdx
	leaq	80(%rax), %rdi
	movq	%rbx, %rcx
	pushq	%r15
	salq	$6, %rdx
	leaq	-208(%rbp), %rsi
	movl	$2, %r9d
	addq	%r12, %rdx
	leaq	-216(%rbp), %r8
	movq	%rbx, -208(%rbp)
	movq	%rdx, -200(%rbp)
	movl	$2, %edx
	call	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	movl	(%r15), %edi
	popq	%rcx
	popq	%rsi
	testl	%edi, %edi
	jg	.L367
	movl	-216(%rbp), %edx
	cmpl	$-1, %edx
	je	.L370
	movl	-212(%rbp), %esi
	cmpl	$-1, %esi
	je	.L370
	leal	2(%r14), %eax
	cmpl	%eax, -264(%rbp)
	jne	.L481
	movq	-272(%rbp), %rax
	movl	%esi, (%rax)
.L373:
	movq	-248(%rbp), %rax
	movl	%edx, -228(%rbp)
	leaq	1(%r14), %rdx
	movl	%esi, -288(%rbp)
	movq	16(%rax), %rax
	cmpq	%r14, %r13
	jne	.L482
	.p2align 4,,10
	.p2align 3
.L421:
	movq	%r15, %rbx
	movl	-288(%rbp), %r11d
	xorl	%r15d, %r15d
	movq	-296(%rbp), %r13
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L400:
	movl	-116(%rbp), %eax
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L367:
	movq	-248(%rbp), %rax
	movq	16(%rax), %rax
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L475:
	subq	$8, %rsp
	movq	%rax, %rdi
	movl	$2, %r9d
	movq	-280(%rbp), %rcx
	pushq	%rbx
	leaq	-208(%rbp), %rsi
	movl	$2, %edx
	leaq	-216(%rbp), %r8
	movq	%rcx, -208(%rbp)
	movq	%r14, -200(%rbp)
	call	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	movl	(%rbx), %r8d
	popq	%rsi
	movl	-248(%rbp), %r11d
	popq	%rdi
	testl	%r8d, %r8d
	jg	.L384
	movl	-216(%rbp), %ecx
	cmpl	$-1, %ecx
	je	.L385
	movl	-212(%rbp), %esi
	cmpl	$-1, %esi
	je	.L385
	movl	-300(%rbp), %edi
	cmpl	%edi, -264(%rbp)
	je	.L483
	movq	-272(%rbp), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	js	.L391
	leal	(%rcx,%rax), %edx
	movl	%edx, (%rdi)
.L390:
	testq	%r13, %r13
	je	.L392
	movl	%ecx, -228(%rbp)
	movl	%esi, %r11d
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L341:
	movq	-240(%rbp), %rax
	movl	12(%rax), %ecx
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L370:
	movq	-248(%rbp), %rax
	movl	$3, (%r15)
	movq	16(%rax), %rax
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L481:
	movq	-272(%rbp), %rcx
	movl	(%rcx), %eax
	testl	%eax, %eax
	js	.L373
	addl	%edx, %eax
	movl	%eax, (%rcx)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L334:
	movl	12(%rcx), %eax
	movq	%r9, %rbx
	movl	%eax, (%rbx)
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L407:
	movq	-256(%rbp), %rax
	movl	12(%rax), %eax
	jmp	.L408
.L483:
	movq	-272(%rbp), %rax
	movl	%esi, %edx
	movl	%esi, (%rax)
	jmp	.L390
.L352:
	movl	$3, (%rbx)
	jmp	.L354
.L359:
	movl	$3, (%rbx)
	jmp	.L361
.L385:
	movl	$3, (%rbx)
	testq	%r13, %r13
	jne	.L387
	jmp	.L388
.L472:
	movl	-228(%rbp), %r15d
	jmp	.L413
.L355:
	movq	-272(%rbp), %rsi
	movl	(%rsi), %eax
	testl	%eax, %eax
	js	.L357
	addl	-228(%rbp), %eax
	movl	%eax, (%rsi)
	jmp	.L357
.L362:
	movq	-272(%rbp), %rsi
	movl	(%rsi), %eax
	testl	%eax, %eax
	js	.L364
	addl	-228(%rbp), %eax
	movl	%eax, (%rsi)
	jmp	.L364
.L480:
	xorl	%r15d, %r15d
	jmp	.L413
.L391:
	movl	%ecx, -228(%rbp)
	movl	%esi, %r11d
	testq	%r13, %r13
	jne	.L387
	jmp	.L406
.L469:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4747:
	.size	_ZNK6icu_6713ListFormatter7format_EPKNS_13UnicodeStringEiRS1_iRiPNS_20FieldPositionHandlerER10UErrorCode.part.0, .-_ZNK6icu_6713ListFormatter7format_EPKNS_13UnicodeStringEiRS1_iRiPNS_20FieldPositionHandlerER10UErrorCode.part.0
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC3:
	.string	"{"
	.string	"0"
	.string	"}"
	.string	" "
	.string	"y"
	.string	" "
	.string	"{"
	.string	"1"
	.string	"}"
	.string	""
	.string	""
	.align 2
.LC4:
	.string	"{"
	.string	"0"
	.string	"}"
	.string	" "
	.string	"e"
	.string	" "
	.string	"{"
	.string	"1"
	.string	"}"
	.string	""
	.string	""
	.align 2
.LC5:
	.string	"{"
	.string	"0"
	.string	"}"
	.string	" "
	.string	"o"
	.string	" "
	.string	"{"
	.string	"1"
	.string	"}"
	.string	""
	.string	""
	.align 2
.LC6:
	.string	"{"
	.string	"0"
	.string	"}"
	.string	" "
	.string	"u"
	.string	" "
	.string	"{"
	.string	"1"
	.string	"}"
	.string	""
	.string	""
	.align 2
.LC7:
	.string	"{"
	.string	"0"
	.string	"}"
	.string	" "
	.string	"\325\005{"
	.string	"1"
	.string	"}"
	.string	""
	.string	""
	.align 2
.LC8:
	.string	"{"
	.string	"0"
	.string	"}"
	.string	" "
	.string	"\325\005-"
	.string	"{"
	.string	"1"
	.string	"}"
	.string	""
	.string	""
	.text
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_120createPatternHandlerEPKcRKNS_13UnicodeStringES5_R10UErrorCode, @function
_ZN6icu_6712_GLOBAL__N_120createPatternHandlerEPKcRKNS_13UnicodeStringES5_R10UErrorCode:
.LFB3397:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rdi), %eax
	cmpl	$101, %eax
	jne	.L486
	cmpb	$115, 1(%rdi)
	jne	.L486
	cmpb	$0, 2(%rdi)
	jne	.L486
	leaq	.LC3(%rip), %rax
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rax, -264(%rbp)
	leaq	-264(%rbp), %r15
	leaq	-256(%rbp), %rax
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	8(%r13), %edx
	movl	%edx, %r14d
	andl	$1, %r14d
	jne	.L607
	testw	%dx, %dx
	js	.L489
	sarl	$5, %edx
.L490:
	movswl	-248(%rbp), %eax
	testw	%ax, %ax
	js	.L491
	movswl	%ax, %esi
	sarl	$5, %esi
.L492:
	movl	%eax, %ecx
	andl	$1, %ecx
	cmpl	%edx, %esi
	jne	.L488
	testb	%cl, %cl
	je	.L608
.L488:
	movswl	8(%r12), %edx
	movl	%edx, %r8d
	andl	$1, %r8d
	je	.L494
.L612:
	movl	%r14d, %eax
	movl	%ecx, %r8d
	orl	%ecx, %eax
.L495:
	movb	%r8b, -288(%rbp)
	testb	%al, %al
	je	.L501
.L614:
	leaq	-128(%rbp), %r10
	movl	$-1, %ecx
	movq	%r15, %rdx
	movl	$1, %esi
	leaq	.LC4(%rip), %rax
	movq	%r10, %rdi
	movq	%r10, -296(%rbp)
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$304, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-296(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L502
	movzbl	-288(%rbp), %r8d
	testb	%r8b, %r8b
	je	.L546
	testb	%r14b, %r14b
	jne	.L547
	movq	%r13, %r11
	movq	%r10, %r9
.L503:
	leaq	8(%r15), %rdi
	movq	%rbx, %r8
	movl	$2, %ecx
	movq	%r13, %rsi
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_114PatternHandlerE(%rip), %rax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r14
	movl	$2, %edx
	movq	%r10, -304(%rbp)
	movq	%rax, (%r15)
	movl	$2, %eax
	movq	%r14, 16(%r15)
	movw	%ax, 24(%r15)
	movq	%r9, -288(%rbp)
	movq	%r11, -296(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movl	$2, %eax
	movq	%rbx, %r8
	movq	%r12, %rsi
	movq	%r14, 88(%r15)
	leaq	80(%r15), %rdi
	movl	$2, %ecx
	movl	$2, %edx
	movw	%ax, 96(%r15)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_117ContextualHandlerE(%rip), %rax
	movq	%rbx, %r8
	movq	-296(%rbp), %r11
	movq	%rax, (%r15)
	movl	$2, %ecx
	movl	$2, %edx
	leaq	_ZN6icu_6712_GLOBAL__N_1L15shouldChangeToEERKNS_13UnicodeStringE(%rip), %rax
	movq	%rax, 152(%r15)
	movl	$2, %eax
	movq	%r11, %rsi
	leaq	160(%r15), %rdi
	movq	%r14, 168(%r15)
	movw	%ax, 176(%r15)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movl	$2, %eax
	movq	%rbx, %r8
	movq	-288(%rbp), %r9
	movq	%r14, 240(%r15)
	movl	$2, %ecx
	movl	$2, %edx
	leaq	232(%r15), %rdi
	movw	%ax, 248(%r15)
	movq	%r9, %rsi
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	-304(%rbp), %r10
.L502:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L504:
	movq	-280(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L486:
	cmpl	$104, %eax
	je	.L609
.L554:
	cmpl	$105, %eax
	jne	.L522
	cmpb	$119, 1(%rdi)
	je	.L610
	.p2align 4,,10
	.p2align 3
.L522:
	movl	$152, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L484
	leaq	8(%r15), %rdi
	movq	%rbx, %r8
	movl	$2, %ecx
	movq	%r13, %rsi
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_114PatternHandlerE(%rip), %rax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r14
	movl	$2, %edx
	movq	%rax, (%r15)
	movl	$2, %eax
	movq	%r14, 16(%r15)
	movw	%ax, 24(%r15)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movl	$2, %edx
	movq	%rbx, %r8
	movq	%r12, %rsi
	movw	%dx, 96(%r15)
	leaq	80(%r15), %rdi
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r14, 88(%r15)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
.L484:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L611
	addq	$264, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L609:
	.cfi_restore_state
	cmpb	$101, 1(%rdi)
	jne	.L554
	cmpb	$0, 2(%rdi)
	jne	.L554
.L525:
	leaq	-264(%rbp), %r15
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	-192(%rbp), %r14
	leaq	.LC7(%rip), %rax
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	8(%r13), %edx
	movl	%edx, %r9d
	andl	$1, %r9d
	je	.L527
	movswl	-184(%rbp), %eax
	movl	%eax, %ecx
	andl	$1, %ecx
	movl	%ecx, %r9d
.L528:
	movswl	8(%r12), %edx
	movl	%edx, %r8d
	andl	$1, %r8d
	je	.L534
	movl	%r9d, %eax
	movl	%ecx, %r8d
	orl	%ecx, %eax
.L535:
	movb	%r9b, -288(%rbp)
	movb	%r8b, -280(%rbp)
	testb	%al, %al
	je	.L541
	leaq	-128(%rbp), %r10
	movl	$-1, %ecx
	movq	%r15, %rdx
	movl	$1, %esi
	leaq	.LC8(%rip), %rax
	movq	%r10, %rdi
	movq	%r10, -296(%rbp)
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$304, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-296(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L542
	movzbl	-280(%rbp), %r8d
	testb	%r8b, %r8b
	je	.L552
	movzbl	-288(%rbp), %r9d
	testb	%r9b, %r9b
	jne	.L553
	movq	%r13, %r11
	movq	%r10, %r9
.L543:
	movl	$2, %ecx
	movq	%r13, %rsi
	leaq	8(%r15), %rdi
	movq	%rbx, %r8
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_114PatternHandlerE(%rip), %rax
	movw	%cx, 24(%r15)
	movl	$2, %edx
	movl	$2, %ecx
	movq	%rax, (%r15)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r13
	movq	%rax, 16(%r15)
	movq	%r10, -296(%rbp)
	movq	%r9, -288(%rbp)
	movq	%r11, -280(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	%r13, 88(%r15)
	leaq	80(%r15), %rdi
	movq	%rbx, %r8
	movl	$2, %esi
	movl	$2, %ecx
	movl	$2, %edx
	movw	%si, 96(%r15)
	movq	%r12, %rsi
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_117ContextualHandlerE(%rip), %rax
	movq	%rbx, %r8
	movl	$2, %edi
	movq	%rax, (%r15)
	leaq	_ZN6icu_6712_GLOBAL__N_1L21shouldChangeToVavDashERKNS_13UnicodeStringE(%rip), %rax
	movq	-280(%rbp), %r11
	movl	$2, %ecx
	movw	%di, 176(%r15)
	movl	$2, %edx
	leaq	160(%r15), %rdi
	movq	%rax, 152(%r15)
	movq	%r11, %rsi
	movq	%r13, 168(%r15)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movl	$2, %r8d
	movq	-288(%rbp), %r9
	movq	%r13, 240(%r15)
	movw	%r8w, 248(%r15)
	leaq	232(%r15), %rdi
	movq	%rbx, %r8
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r9, %rsi
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	-296(%rbp), %r10
.L542:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L607:
	movswl	-248(%rbp), %eax
	movswl	8(%r12), %edx
	movl	%eax, %ecx
	movl	%edx, %r8d
	andl	$1, %ecx
	movl	%ecx, %r14d
	andl	$1, %r8d
	jne	.L612
.L494:
	testw	%dx, %dx
	js	.L496
	sarl	$5, %edx
.L497:
	testw	%ax, %ax
	js	.L498
	sarl	$5, %eax
.L499:
	testb	%cl, %cl
	jne	.L545
	cmpl	%edx, %eax
	je	.L613
.L545:
	movl	%r14d, %eax
	movb	%r8b, -288(%rbp)
	testb	%al, %al
	jne	.L614
.L501:
	leaq	-192(%rbp), %r14
	movl	$-1, %ecx
	movq	%r15, %rdx
	movl	$1, %esi
	leaq	.LC5(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	8(%r13), %eax
	movl	%eax, %r8d
	andl	$1, %r8d
	je	.L505
	movswl	-184(%rbp), %ecx
	movl	%ecx, %esi
	andl	$1, %esi
	movl	%esi, %r8d
.L506:
	movzwl	8(%r12), %eax
	movl	%eax, %r11d
	andl	$1, %r11d
	je	.L512
	movl	%r8d, %eax
	movl	%esi, %r11d
	orl	%esi, %eax
.L513:
	movb	%r8b, -296(%rbp)
	movb	%r11b, -288(%rbp)
	testb	%al, %al
	je	.L519
	leaq	-128(%rbp), %r10
	movl	$-1, %ecx
	movq	%r15, %rdx
	movl	$1, %esi
	leaq	.LC6(%rip), %rax
	movq	%r10, %rdi
	movq	%r10, -304(%rbp)
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$304, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-304(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L520
	movzbl	-288(%rbp), %r11d
	testb	%r11b, %r11b
	je	.L549
	movzbl	-296(%rbp), %r8d
	testb	%r8b, %r8b
	jne	.L550
	movq	%r13, %r11
	movq	%r10, %r9
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L610:
	cmpb	$0, 2(%rdi)
	je	.L525
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L541:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L534:
	testw	%dx, %dx
	js	.L536
	sarl	$5, %edx
.L537:
	testw	%ax, %ax
	js	.L538
	sarl	$5, %eax
.L539:
	testb	%cl, %cl
	jne	.L551
	cmpl	%edx, %eax
	je	.L615
.L551:
	movl	%r9d, %eax
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L527:
	testw	%dx, %dx
	js	.L529
	sarl	$5, %edx
.L530:
	movswl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L531
	movswl	%ax, %esi
	sarl	$5, %esi
.L532:
	movl	%eax, %ecx
	andl	$1, %ecx
	jne	.L528
	cmpl	%edx, %esi
	jne	.L528
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	movswl	-184(%rbp), %eax
	setne	%r9b
	movl	%eax, %ecx
	andl	$1, %ecx
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L552:
	movq	%r12, %r9
	movq	%r10, %r11
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L538:
	movl	-180(%rbp), %eax
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L536:
	movl	12(%r12), %edx
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L531:
	movl	-180(%rbp), %esi
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L529:
	movl	12(%r13), %edx
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L546:
	movq	%r12, %r9
	movq	%r10, %r11
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L549:
	movq	%r12, %r9
	movq	%r10, %r11
.L521:
	movq	%r13, %rsi
	leaq	8(%r15), %rdi
	movq	%rbx, %r8
	movl	$2, %ecx
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_114PatternHandlerE(%rip), %rax
	movq	%r9, -288(%rbp)
	movl	$2, %r9d
	movl	$2, %edx
	movq	%rax, (%r15)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r13
	movw	%r9w, 24(%r15)
	movq	%rax, 16(%r15)
	movq	%r11, -296(%rbp)
	movq	%r10, -304(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	%r13, 88(%r15)
	movq	%r12, %rsi
	leaq	80(%r15), %rdi
	movl	$2, %r10d
	movq	%rbx, %r8
	movl	$2, %ecx
	movl	$2, %edx
	movw	%r10w, 96(%r15)
	movl	$2, %r12d
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_117ContextualHandlerE(%rip), %rax
	movq	%rbx, %r8
	movl	$2, %r11d
	movq	%rax, (%r15)
	leaq	_ZN6icu_6712_GLOBAL__N_1L15shouldChangeToUERKNS_13UnicodeStringE(%rip), %rax
	leaq	160(%r15), %rdi
	movl	$2, %ecx
	movw	%r11w, 176(%r15)
	movq	-296(%rbp), %r11
	movl	$2, %edx
	movq	%rax, 152(%r15)
	movq	%r13, 168(%r15)
	movq	%r11, %rsi
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	-288(%rbp), %r9
	movq	%rbx, %r8
	movq	%r13, 240(%r15)
	movw	%r12w, 248(%r15)
	movl	$2, %ecx
	movl	$2, %edx
	leaq	232(%r15), %rdi
	movq	%r9, %rsi
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	-304(%rbp), %r10
.L520:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L519:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-280(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L512:
	testw	%ax, %ax
	js	.L514
	movswl	%ax, %edx
	sarl	$5, %edx
.L515:
	testw	%cx, %cx
	js	.L516
	sarl	$5, %ecx
.L517:
	testb	%sil, %sil
	jne	.L548
	cmpl	%edx, %ecx
	je	.L616
.L548:
	movl	%r8d, %eax
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L505:
	testw	%ax, %ax
	js	.L507
	movswl	%ax, %edx
	sarl	$5, %edx
.L508:
	movswl	-184(%rbp), %ecx
	testw	%cx, %cx
	js	.L509
	movswl	%cx, %eax
	sarl	$5, %eax
.L510:
	movl	%ecx, %esi
	andl	$1, %esi
	cmpl	%edx, %eax
	jne	.L506
	testb	%sil, %sil
	jne	.L506
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movswl	-184(%rbp), %ecx
	testb	%al, %al
	movl	%ecx, %esi
	setne	%r8b
	andl	$1, %esi
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L491:
	movl	-244(%rbp), %esi
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L498:
	movl	-244(%rbp), %eax
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L496:
	movl	12(%r12), %edx
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L489:
	movl	12(%r13), %edx
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L514:
	movl	12(%r12), %edx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L507:
	movl	12(%r13), %edx
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L516:
	movl	-180(%rbp), %ecx
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L509:
	movl	-180(%rbp), %eax
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L615:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	%r9b, -280(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movzbl	-280(%rbp), %r9d
	testb	%al, %al
	setne	%r8b
	movl	%r9d, %eax
	orl	%r8d, %eax
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L613:
	movq	-280(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	movl	%r14d, %eax
	setne	%r8b
	orl	%r8d, %eax
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L608:
	movq	-280(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	movswl	-248(%rbp), %eax
	setne	%r14b
	movl	%eax, %ecx
	andl	$1, %ecx
	jmp	.L488
.L616:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	%r8b, -288(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movzbl	-288(%rbp), %r8d
	testb	%al, %al
	setne	%r11b
	movl	%r8d, %eax
	orl	%r11d, %eax
	jmp	.L513
.L611:
	call	__stack_chk_fail@PLT
.L547:
	movq	%r10, %r9
	movq	%r10, %r11
	jmp	.L503
.L553:
	movq	%r10, %r9
	movq	%r10, %r11
	jmp	.L543
.L550:
	movq	%r10, %r9
	movq	%r10, %r11
	jmp	.L521
	.cfi_endproc
.LFE3397:
	.size	_ZN6icu_6712_GLOBAL__N_120createPatternHandlerEPKcRKNS_13UnicodeStringES5_R10UErrorCode, .-_ZN6icu_6712_GLOBAL__N_120createPatternHandlerEPKcRKNS_13UnicodeStringES5_R10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3921:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3921:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3924:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L630
	ret
	.p2align 4,,10
	.p2align 3
.L630:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L618
	cmpb	$0, 12(%rbx)
	jne	.L631
.L622:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L618:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L631:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L622
	.cfi_endproc
.LFE3924:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3927:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L634
	ret
	.p2align 4,,10
	.p2align 3
.L634:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3927:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3930:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L637
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L637:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3930:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L643
.L639:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L644
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L643:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L644:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3932:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3933:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3933:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3934:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3934:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3935:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3935:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3936:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3936:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3937:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3937:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3938:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L660
	testl	%edx, %edx
	jle	.L660
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L663
.L652:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L660:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L663:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L652
	.cfi_endproc
.LFE3938:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L667
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L667
	testl	%r12d, %r12d
	jg	.L674
	cmpb	$0, 12(%rbx)
	jne	.L675
.L669:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L669
.L675:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L667:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3939:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L677
	movq	(%rdi), %r8
.L678:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L677:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L681
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L681
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L681:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3940:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3941:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L688
	ret
	.p2align 4,,10
	.p2align 3
.L688:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3941:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3942:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3942:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3943:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3943:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3944:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3944:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3946:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3946:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3948:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3948:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713FormattedListC2EOS0_
	.type	_ZN6icu_6713FormattedListC2EOS0_, @function
_ZN6icu_6713FormattedListC2EOS0_:
.LFB3417:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713FormattedListE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rsi), %rax
	movq	$0, 8(%rsi)
	movq	%rax, 8(%rdi)
	movl	16(%rsi), %eax
	movl	$27, 16(%rsi)
	movl	%eax, 16(%rdi)
	ret
	.cfi_endproc
.LFE3417:
	.size	_ZN6icu_6713FormattedListC2EOS0_, .-_ZN6icu_6713FormattedListC2EOS0_
	.globl	_ZN6icu_6713FormattedListC1EOS0_
	.set	_ZN6icu_6713FormattedListC1EOS0_,_ZN6icu_6713FormattedListC2EOS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713FormattedListaSEOS0_
	.type	_ZN6icu_6713FormattedListaSEOS0_, @function
_ZN6icu_6713FormattedListaSEOS0_:
.LFB3423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L696
	movq	0(%r13), %rax
	leaq	_ZN6icu_6717FormattedListDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L697
	leaq	16+_ZTVN6icu_6717FormattedListDataE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L696:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	movl	16(%rbx), %eax
	movq	$0, 8(%rbx)
	movl	%eax, 16(%r12)
	movq	%r12, %rax
	movl	$27, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L697:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L696
	.cfi_endproc
.LFE3423:
	.size	_ZN6icu_6713FormattedListaSEOS0_, .-_ZN6icu_6713FormattedListaSEOS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatterC2ERKS0_
	.type	_ZN6icu_6713ListFormatterC2ERKS0_, @function
_ZN6icu_6713ListFormatterC2ERKS0_:
.LFB3434:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713ListFormatterE(%rip), %rax
	movq	16(%rsi), %rdx
	movq	%rax, (%rdi)
	movq	8(%rsi), %rax
	movq	%rdx, 16(%rdi)
	movq	%rax, 8(%rdi)
	testq	%rax, %rax
	je	.L712
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$160, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L704
	movq	8(%r12), %r12
	leaq	16(%rax), %rdi
	leaq	16(%r12), %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	88(%r12), %rsi
	leaq	88(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	152(%r12), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 152(%r13)
.L704:
	movq	%r13, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE3434:
	.size	_ZN6icu_6713ListFormatterC2ERKS0_, .-_ZN6icu_6713ListFormatterC2ERKS0_
	.globl	_ZN6icu_6713ListFormatterC1ERKS0_
	.set	_ZN6icu_6713ListFormatterC1ERKS0_,_ZN6icu_6713ListFormatterC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatteraSERKS0_
	.type	_ZN6icu_6713ListFormatteraSERKS0_, @function
_ZN6icu_6713ListFormatteraSERKS0_:
.LFB3436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L716
	movq	8(%rdi), %r13
	movq	%rsi, %rbx
	testq	%r13, %r13
	je	.L717
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L718
	movq	(%rdi), %rax
	call	*8(%rax)
.L718:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L717:
	cmpq	$0, 8(%rbx)
	je	.L719
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L720
	movq	8(%rbx), %rbx
	leaq	16(%rax), %rdi
	leaq	16(%rbx), %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	88(%rbx), %rsi
	leaq	88(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	152(%rbx), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 152(%r13)
.L720:
	movq	%r13, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
.L716:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	.cfi_restore_state
	movq	$0, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3436:
	.size	_ZN6icu_6713ListFormatteraSERKS0_, .-_ZN6icu_6713ListFormatteraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatter14initializeHashER10UErrorCode
	.type	_ZN6icu_6713ListFormatter14initializeHashER10UErrorCode, @function
_ZN6icu_6713ListFormatter14initializeHashER10UErrorCode:
.LFB3437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L737
.L731:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L738
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L737:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L733
	movq	$0, (%rax)
	leaq	8(%rax), %r13
	leaq	-44(%rbp), %r8
	xorl	%ecx, %ecx
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	movl	$0, -44(%rbp)
	call	uhash_init_67@PLT
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L739
.L734:
	movq	(%r12), %rdi
	leaq	uprv_deleteListFormatInternal(%rip), %rsi
	movq	%r12, _ZN6icu_67L15listPatternHashE(%rip)
	call	uhash_setValueDeleter_67@PLT
	leaq	uprv_listformatter_cleanup(%rip), %rsi
	movl	$34, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L739:
	movq	%r13, (%r12)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	uhash_setKeyDeleter_67@PLT
	jmp	.L734
.L738:
	call	__stack_chk_fail@PLT
.L733:
	movq	$0, _ZN6icu_67L15listPatternHashE(%rip)
	movl	$7, (%rbx)
	jmp	.L731
	.cfi_endproc
.LFE3437:
	.size	_ZN6icu_6713ListFormatter14initializeHashER10UErrorCode, .-_ZN6icu_6713ListFormatter14initializeHashER10UErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC9:
	.string	"listPattern"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatter22loadListFormatInternalERKNS_6LocaleEPKcR10UErrorCode
	.type	_ZN6icu_6713ListFormatter22loadListFormatInternalERKNS_6LocaleEPKcR10UErrorCode, @function
_ZN6icu_6713ListFormatter22loadListFormatInternalERKNS_6LocaleEPKcR10UErrorCode:
.LFB3456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -408(%rbp)
	movq	40(%rdi), %rsi
	xorl	%edi, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ures_open_67@PLT
	movq	%r12, %rcx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %rdx
	call	ures_getByKeyWithFallback_67@PLT
	movl	(%r12), %r15d
	movq	%rax, %r14
	testl	%r15d, %r15d
	jg	.L770
	movl	$2, %ebx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r13, %rsi
	movq	.LC10(%rip), %xmm0
	movl	$2, %r9d
	movl	$2, %r10d
	movl	$24, %edx
	movw	%bx, -192(%rbp)
	movhps	.LC11(%rip), %xmm0
	leaq	-96(%rbp), %rbx
	movl	$2, %r11d
	movw	%r9w, -384(%rbp)
	movaps	%xmm0, -400(%rbp)
	movq	%rbx, %rdi
	pxor	%xmm0, %xmm0
	leaq	-400(%rbp), %r15
	movq	%rax, -328(%rbp)
	leaq	-136(%rbp), %r13
	movw	%r10w, -320(%rbp)
	movq	%rax, -264(%rbp)
	movw	%r11w, -256(%rbp)
	movq	%rax, -200(%rbp)
	movq	$0, -120(%rbp)
	movb	$0, -112(%rbp)
	movups	%xmm0, -136(%rbp)
	call	strncpy@PLT
	movb	$0, -72(%rbp)
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L771:
	cmpb	$0, -136(%rbp)
	je	.L743
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L743
	movl	$25, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__strcpy_chk@PLT
.L744:
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	ures_getAllItemsWithFallback_67@PLT
	movl	(%r12), %r8d
	testl	%r8d, %r8d
	jle	.L771
.L743:
	movq	%r14, %rdi
	call	ures_close_67@PLT
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L752
	movswl	-384(%rbp), %eax
	shrl	$5, %eax
	je	.L746
	movswl	-320(%rbp), %eax
	shrl	$5, %eax
	je	.L746
	movswl	-256(%rbp), %eax
	shrl	$5, %eax
	je	.L746
	movswl	-192(%rbp), %eax
	shrl	$5, %eax
	je	.L746
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L772
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	movl	$2, %ecx
	leaq	8(%rax), %r10
	movq	%r12, %r8
	movq	%rbx, 16(%rax)
	leaq	-328(%rbp), %r14
	movq	%r10, %rdi
	movl	$2, %edx
	movw	%cx, 24(%rax)
	movq	%r14, %rsi
	movl	$2, %ecx
	movq	%r10, -432(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	%rbx, 88(%r13)
	leaq	80(%r13), %r9
	movq	%r12, %r8
	movl	$2, %esi
	leaq	-264(%rbp), %rbx
	movq	%r9, %rdi
	movl	$2, %ecx
	movw	%si, 96(%r13)
	movl	$2, %edx
	movq	%rbx, %rsi
	movq	%r9, -424(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	-408(%rbp), %rdi
	leaq	-200(%rbp), %r8
	movq	%r12, %rcx
	leaq	-392(%rbp), %r11
	movq	%r8, %rdx
	movq	%r8, -416(%rbp)
	addq	$8, %rdi
	movq	%r11, %rsi
	movq	%r11, -408(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_120createPatternHandlerEPKcRKNS_13UnicodeStringES5_R10UErrorCode
	movq	-408(%rbp), %r11
	movq	-416(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, 152(%r13)
	movq	%rax, %rdi
	movq	-424(%rbp), %r9
	movq	-432(%rbp), %r10
	je	.L773
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L745
	movq	(%rdi), %rax
	movq	%r11, -432(%rbp)
	movq	%r8, -424(%rbp)
	movq	%r9, -416(%rbp)
	movq	%r10, -408(%rbp)
	call	*8(%rax)
	movq	-408(%rbp), %r10
	movq	-416(%rbp), %r9
	movq	-424(%rbp), %r8
	movq	-432(%rbp), %r11
.L750:
	movq	%r9, %rdi
	movq	%r11, -424(%rbp)
	movq	%r8, -416(%rbp)
	movq	%r10, -408(%rbp)
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	-408(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-416(%rbp), %r8
	movq	-424(%rbp), %r11
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L746:
	movl	$2, (%r12)
.L752:
	xorl	%r13d, %r13d
	leaq	-328(%rbp), %r14
	leaq	-264(%rbp), %rbx
	leaq	-200(%rbp), %r8
	leaq	-392(%rbp), %r11
.L745:
	leaq	16+_ZTVN6icu_6713ListFormatter16ListPatternsSinkE(%rip), %rax
	movq	%r8, %rdi
	movq	%r11, -408(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-408(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
.L740:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L774
	addq	$392, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L770:
	.cfi_restore_state
	movq	%rax, %rdi
	xorl	%r13d, %r13d
	call	ures_close_67@PLT
	jmp	.L740
.L773:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L750
	movl	$7, (%r12)
	jmp	.L750
.L774:
	call	__stack_chk_fail@PLT
.L772:
	movl	$7, (%r12)
	leaq	-328(%rbp), %r14
	leaq	-264(%rbp), %rbx
	leaq	-200(%rbp), %r8
	leaq	-392(%rbp), %r11
	jmp	.L745
	.cfi_endproc
.LFE3456:
	.size	_ZN6icu_6713ListFormatter22loadListFormatInternalERKNS_6LocaleEPKcR10UErrorCode, .-_ZN6icu_6713ListFormatter22loadListFormatInternalERKNS_6LocaleEPKcR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCode.part.0, @function
_ZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCode.part.0:
.LFB4740:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-192(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$184, %rsp
	movq	%rsi, -216(%rbp)
	movq	40(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movl	-200(%rbp), %edx
	movq	-208(%rbp), %rsi
	leaq	-179(%rbp), %rax
	movw	%r8w, -180(%rbp)
	movq	%rax, -192(%rbp)
	movl	$0, -136(%rbp)
	movl	$40, -184(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	$58, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	leaq	-128(%rbp), %r15
	movq	%rax, %r12
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-200(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	-208(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r15, %rdi
	movq	-192(%rbp), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	leaq	_ZZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCodeE18listFormatterMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	_ZN6icu_67L15listPatternHashE(%rip), %rax
	testq	%rax, %rax
	je	.L798
.L776:
	movq	(%rax), %rdi
	movq	%r15, %rsi
	call	uhash_get_67@PLT
	leaq	_ZZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCodeE18listFormatterMutex(%rip), %rdi
	movq	%rax, %r12
	call	umtx_unlock_67@PLT
	testq	%r12, %r12
	je	.L799
.L780:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -180(%rbp)
	jne	.L800
.L775:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L801
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L798:
	.cfi_restore_state
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.L802
.L785:
	leaq	_ZZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCodeE18listFormatterMutex(%rip), %rdi
	xorl	%r12d, %r12d
	call	umtx_unlock_67@PLT
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L800:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L799:
	movq	-216(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6713ListFormatter22loadListFormatInternalERKNS_6LocaleEPKcR10UErrorCode
	movl	(%rbx), %edx
	movq	%rax, %r13
	testl	%edx, %edx
	jg	.L780
	leaq	_ZZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCodeE18listFormatterMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	_ZN6icu_67L15listPatternHashE(%rip), %rax
	movq	%r15, %rsi
	movq	(%rax), %rdi
	call	uhash_get_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L781
	testq	%r13, %r13
	je	.L782
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L783
	movq	(%rdi), %rax
	call	*8(%rax)
.L783:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L782:
	leaq	_ZZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCodeE18listFormatterMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L802:
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L778
	movq	$0, (%rax)
	leaq	8(%rax), %rdi
	xorl	%ecx, %ecx
	movq	%r14, %r8
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	%rdi, -224(%rbp)
	movl	$0, -208(%rbp)
	call	uhash_init_67@PLT
	movl	-208(%rbp), %esi
	movq	-224(%rbp), %rdi
	testl	%esi, %esi
	jle	.L803
.L779:
	movq	(%r12), %rdi
	leaq	uprv_deleteListFormatInternal(%rip), %rsi
	movq	%r12, _ZN6icu_67L15listPatternHashE(%rip)
	call	uhash_setValueDeleter_67@PLT
	leaq	uprv_listformatter_cleanup(%rip), %rsi
	movl	$34, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L785
	movq	_ZN6icu_67L15listPatternHashE(%rip), %rax
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L803:
	movq	%rdi, (%r12)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	call	uhash_setKeyDeleter_67@PLT
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L781:
	movl	$64, %edi
	movq	_ZN6icu_67L15listPatternHashE(%rip), %r14
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L784
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L784:
	movq	(%r14), %rdi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	uhash_put_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L785
	movq	%r13, %r12
	jmp	.L782
.L801:
	call	__stack_chk_fail@PLT
.L778:
	movq	$0, _ZN6icu_67L15listPatternHashE(%rip)
	movl	$7, (%rbx)
	jmp	.L785
	.cfi_endproc
.LFE4740:
	.size	_ZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCode.part.0, .-_ZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCode
	.type	_ZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCode, @function
_ZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCode:
.LFB3438:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L805
	jmp	_ZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L805:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3438:
	.size	_ZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCode, .-_ZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCode
	.section	.rodata.str1.1
.LC12:
	.string	"standard"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleER10UErrorCode:
.LFB3458:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L814
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	.LC12(%rip), %rsi
	call	_ZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCode.part.0
	movq	%rax, %r12
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L809
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L810
	leaq	16+_ZTVN6icu_6713ListFormatterE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movq	%r12, 16(%rax)
.L806:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L809:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L814:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
.L810:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$7, (%rbx)
	jmp	.L806
	.cfi_endproc
.LFE3458:
	.size	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleER10UErrorCode
	.section	.rodata.str1.1
.LC13:
	.string	"standard-narrow"
.LC14:
	.string	"or-short"
.LC15:
	.string	"unit-short"
.LC16:
	.string	"unit-narrow"
.LC17:
	.string	"standard-short"
.LC18:
	.string	"or"
.LC19:
	.string	"or-narrow"
.LC20:
	.string	"unit"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleE18UListFormatterType19UListFormatterWidthR10UErrorCode
	.type	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleE18UListFormatterType19UListFormatterWidthR10UErrorCode, @function
_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleE18UListFormatterType19UListFormatterWidthR10UErrorCode:
.LFB3459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rcx, %rbx
	cmpl	$1, %esi
	je	.L816
	cmpl	$2, %esi
	je	.L817
	testl	%esi, %esi
	je	.L834
.L818:
	movl	$1, (%rbx)
	xorl	%eax, %eax
.L815:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L816:
	.cfi_restore_state
	cmpl	$1, %edx
	je	.L827
	cmpl	$2, %edx
	je	.L828
	testl	%edx, %edx
	jne	.L818
	leaq	.LC18(%rip), %rsi
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L834:
	cmpl	$1, %edx
	je	.L824
	cmpl	$2, %edx
	je	.L825
	testl	%edx, %edx
	jne	.L818
	leaq	.LC12(%rip), %rsi
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L817:
	cmpl	$1, %edx
	je	.L830
	cmpl	$2, %edx
	je	.L831
	testl	%edx, %edx
	jne	.L818
	leaq	.LC20(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L819:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L822
	movq	%rbx, %rdx
	call	_ZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCode.part.0
	movq	%rax, %r12
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L822
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L823
	leaq	16+_ZTVN6icu_6713ListFormatterE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movq	%r12, 16(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L831:
	.cfi_restore_state
	leaq	.LC16(%rip), %rsi
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L828:
	leaq	.LC19(%rip), %rsi
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L825:
	leaq	.LC13(%rip), %rsi
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L824:
	leaq	.LC17(%rip), %rsi
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L827:
	leaq	.LC14(%rip), %rsi
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L830:
	leaq	.LC15(%rip), %rsi
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L822:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L823:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L815
	.cfi_endproc
.LFE3459:
	.size	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleE18UListFormatterType19UListFormatterWidthR10UErrorCode, .-_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleE18UListFormatterType19UListFormatterWidthR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleEPKcR10UErrorCode
	.type	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleEPKcR10UErrorCode, @function
_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleEPKcR10UErrorCode:
.LFB3460:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L843
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	call	_ZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCode.part.0
	movq	%rax, %r12
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L838
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L839
	leaq	16+_ZTVN6icu_6713ListFormatterE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movq	%r12, 16(%rax)
.L835:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L838:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L843:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
.L839:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$7, (%rbx)
	jmp	.L835
	.cfi_endproc
.LFE3460:
	.size	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleEPKcR10UErrorCode, .-_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleEPKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatterC2ERKNS_14ListFormatDataER10UErrorCode
	.type	_ZN6icu_6713ListFormatterC2ERKNS_14ListFormatDataER10UErrorCode, @function
_ZN6icu_6713ListFormatterC2ERKNS_14ListFormatDataER10UErrorCode:
.LFB3462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713ListFormatterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L846
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r15
	movl	$2, %edx
	leaq	8(%rax), %rdi
	movq	%r13, %r8
	movw	%dx, 24(%rax)
	leaq	72(%r12), %rsi
	movl	$2147483647, %ecx
	xorl	%edx, %edx
	movq	%r15, 16(%rax)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movl	$2, %ecx
	movq	%r15, 88(%rbx)
	xorl	%edx, %edx
	movw	%cx, 96(%rbx)
	leaq	80(%rbx), %rdi
	movq	%r13, %r8
	movl	$2147483647, %ecx
	leaq	136(%r12), %rsi
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	leaq	200(%r12), %rdx
	leaq	8(%r12), %rsi
	movq	%r13, %rcx
	leaq	272(%r12), %rdi
	call	_ZN6icu_6712_GLOBAL__N_120createPatternHandlerEPKcRKNS_13UnicodeStringES5_R10UErrorCode
	movq	%rax, 152(%rbx)
	testq	%rax, %rax
	je	.L849
.L846:
	movq	%rbx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L849:
	.cfi_restore_state
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L846
	movl	$7, 0(%r13)
	jmp	.L846
	.cfi_endproc
.LFE3462:
	.size	_ZN6icu_6713ListFormatterC2ERKNS_14ListFormatDataER10UErrorCode, .-_ZN6icu_6713ListFormatterC2ERKNS_14ListFormatDataER10UErrorCode
	.globl	_ZN6icu_6713ListFormatterC1ERKNS_14ListFormatDataER10UErrorCode
	.set	_ZN6icu_6713ListFormatterC1ERKNS_14ListFormatDataER10UErrorCode,_ZN6icu_6713ListFormatterC2ERKNS_14ListFormatDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatterC2EPKNS_18ListFormatInternalE
	.type	_ZN6icu_6713ListFormatterC2EPKNS_18ListFormatInternalE, @function
_ZN6icu_6713ListFormatterC2EPKNS_18ListFormatInternalE:
.LFB3465:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713ListFormatterE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE3465:
	.size	_ZN6icu_6713ListFormatterC2EPKNS_18ListFormatInternalE, .-_ZN6icu_6713ListFormatterC2EPKNS_18ListFormatInternalE
	.globl	_ZN6icu_6713ListFormatterC1EPKNS_18ListFormatInternalE
	.set	_ZN6icu_6713ListFormatterC1EPKNS_18ListFormatInternalE,_ZN6icu_6713ListFormatterC2EPKNS_18ListFormatInternalE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713ListFormatter6formatEPKNS_13UnicodeStringEiRS1_R10UErrorCode
	.type	_ZNK6icu_6713ListFormatter6formatEPKNS_13UnicodeStringEiRS1_R10UErrorCode, @function
_ZNK6icu_6713ListFormatter6formatEPKNS_13UnicodeStringEiRS1_R10UErrorCode:
.LFB3472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L852
	cmpq	$0, 16(%rdi)
	movq	%r8, %rbx
	je	.L896
	movslq	%edx, %r12
	testl	%r12d, %r12d
	jle	.L852
	movq	%rsi, %r15
	cmpl	$1, %r12d
	je	.L897
	leaq	-128(%rbp), %r13
	movq	%rdi, -232(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	-176(%rbp), %rax
	cmpl	$2, %r12d
	movl	$10, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	-232(%rbp), %r11
	leaq	64(%r15), %rdx
	movb	$0, -180(%rbp)
	jne	.L857
	movq	16(%r11), %rax
	movq	%rdx, -232(%rbp)
	movq	%rdx, %rsi
	movq	152(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movl	(%rbx), %edx
	testl	%edx, %edx
	movq	-232(%rbp), %rdx
	jle	.L898
.L861:
	cmpb	$0, -180(%rbp)
	jne	.L899
.L880:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L852:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L900
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L857:
	.cfi_restore_state
	movl	(%rbx), %eax
	movq	16(%r11), %rdi
	testl	%eax, %eax
	jle	.L901
.L868:
	cmpl	$3, %r12d
	je	.L881
	leal	-4(%r12), %eax
	leaq	128(%r15), %r10
	movq	%r15, -240(%rbp)
	salq	$6, %rax
	leaq	-216(%rbp), %r8
	movl	%r12d, -244(%rbp)
	leaq	192(%r15,%rax), %rax
	movq	%r14, -256(%rbp)
	movq	%r10, %r15
	movq	%r13, %r14
	movq	%r11, -232(%rbp)
	movq	%rbx, %r13
	movq	%r8, %r12
	movq	%rax, %rbx
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L873:
	addq	$64, %r15
	cmpq	%r15, %rbx
	je	.L902
.L874:
	movl	0(%r13), %r10d
	testl	%r10d, %r10d
	jg	.L873
	subq	$8, %rsp
	addq	$80, %rdi
	movq	%r12, %r8
	movq	%r14, %rcx
	pushq	%r13
	movl	$2, %r9d
	movl	$2, %edx
	leaq	-208(%rbp), %rsi
	movq	%r14, -208(%rbp)
	movq	%r15, -200(%rbp)
	call	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	movl	0(%r13), %r9d
	movq	-232(%rbp), %rax
	popq	%rdi
	popq	%r8
	testl	%r9d, %r9d
	movq	16(%rax), %rdi
	jg	.L873
	cmpl	$-1, -216(%rbp)
	je	.L871
	cmpl	$-1, -212(%rbp)
	jne	.L873
.L871:
	addq	$64, %r15
	movl	$3, 0(%r13)
	cmpq	%r15, %rbx
	jne	.L874
	.p2align 4,,10
	.p2align 3
.L902:
	movq	%r13, %rbx
	movq	-240(%rbp), %r15
	movq	%r14, %r13
	movslq	-244(%rbp), %r12
	movq	-256(%rbp), %r14
.L881:
	movq	152(%rdi), %rdi
	salq	$6, %r12
	leaq	-64(%r15,%r12), %r12
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*40(%rax)
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L861
	movq	%r13, -208(%rbp)
	movq	%r12, -200(%rbp)
.L895:
	subq	$8, %rsp
	movq	%r13, %rcx
	movl	$2, %edx
	movq	%rax, %rdi
	pushq	%rbx
	leaq	-208(%rbp), %rsi
	movl	$2, %r9d
	leaq	-216(%rbp), %r8
	call	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	movl	(%rbx), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L861
	cmpl	$-1, -216(%rbp)
	je	.L876
	cmpl	$-1, -212(%rbp)
	je	.L876
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L878
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L879:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L896:
	movl	$27, (%r8)
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L897:
	movswl	8(%rsi), %ecx
	testw	%cx, %cx
	js	.L855
	sarl	$5, %ecx
.L856:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L899:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L876:
	movl	$3, (%rbx)
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L855:
	movl	12(%rsi), %ecx
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L898:
	movq	%r13, -208(%rbp)
	movq	%rdx, -200(%rbp)
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L901:
	subq	$8, %rsp
	addq	$8, %rdi
	movq	%r13, %rcx
	movq	%rdx, -200(%rbp)
	pushq	%rbx
	leaq	-208(%rbp), %rsi
	movl	$2, %r9d
	leaq	-216(%rbp), %r8
	movl	$2, %edx
	movq	%r11, -232(%rbp)
	movq	%r13, -208(%rbp)
	call	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	popq	%r11
	popq	%rax
	movl	(%rbx), %eax
	movq	-232(%rbp), %r11
	testl	%eax, %eax
	movq	16(%r11), %rdi
	jg	.L868
	cmpl	$-1, -216(%rbp)
	je	.L866
	cmpl	$-1, -212(%rbp)
	jne	.L868
.L866:
	movl	$3, (%rbx)
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L878:
	movl	-116(%rbp), %ecx
	jmp	.L879
.L900:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3472:
	.size	_ZNK6icu_6713ListFormatter6formatEPKNS_13UnicodeStringEiRS1_R10UErrorCode, .-_ZNK6icu_6713ListFormatter6formatEPKNS_13UnicodeStringEiRS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713ListFormatter6formatEPKNS_13UnicodeStringEiRS1_iRiR10UErrorCode
	.type	_ZNK6icu_6713ListFormatter6formatEPKNS_13UnicodeStringEiRS1_iRiR10UErrorCode, @function
_ZNK6icu_6713ListFormatter6formatEPKNS_13UnicodeStringEiRS1_iRiR10UErrorCode:
.LFB3473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r12
	movq	%rsi, -232(%rbp)
	movl	%r8d, -236(%rbp)
	movl	(%r12), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$-1, (%r9)
	testl	%r10d, %r10d
	jg	.L904
	cmpq	$0, 16(%rdi)
	je	.L973
	movslq	%edx, %rbx
	testl	%ebx, %ebx
	jle	.L904
	movq	%r9, %r13
	cmpl	$1, %ebx
	je	.L974
	leaq	-128(%rbp), %r15
	movq	%rdi, -248(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	-236(%rbp), %edi
	movq	-248(%rbp), %r11
	testl	%edi, %edi
	jne	.L912
	movl	$0, 0(%r13)
.L912:
	leaq	-176(%rbp), %rax
	movb	$0, -180(%rbp)
	movq	16(%r11), %rdi
	movq	%rax, -192(%rbp)
	movq	-232(%rbp), %rax
	movl	$10, -184(%rbp)
	leaq	64(%rax), %rdx
	cmpl	$2, %ebx
	jne	.L913
	movq	152(%rdi), %rdi
	movq	%rdx, %rsi
	movq	%rdx, -232(%rbp)
	movq	(%rdi), %rax
	call	*32(%rax)
	movl	(%r12), %esi
	movq	-232(%rbp), %rdx
	testl	%esi, %esi
	jle	.L975
.L917:
	cmpb	$0, -180(%rbp)
	jne	.L976
.L947:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L904:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L977
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L913:
	.cfi_restore_state
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L978
.L928:
	leal	-1(%rbx), %eax
	movl	%eax, -268(%rbp)
	cmpl	$3, %ebx
	je	.L948
	movq	-232(%rbp), %rsi
	leal	-4(%rbx), %eax
	xorl	%r10d, %r10d
	movl	%ebx, -240(%rbp)
	movq	%r14, -264(%rbp)
	movq	%rax, %rbx
	movq	%r10, %r14
	leaq	128(%rsi), %rdx
	movq	%r13, -256(%rbp)
	movq	%r15, %r13
	movq	%r12, %r15
	movq	%rdx, -248(%rbp)
	movq	%r11, %r12
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L935:
	leaq	1(%r14), %rax
	cmpq	%r14, %rbx
	je	.L979
.L951:
	movq	%rax, %r14
.L936:
	movl	(%r15), %r10d
	testl	%r10d, %r10d
	jg	.L935
	subq	$8, %rsp
	movq	%r14, %rax
	addq	$80, %rdi
	movq	%r13, %rcx
	pushq	%r15
	movl	$2, %r9d
	salq	$6, %rax
	leaq	-216(%rbp), %r8
	addq	-248(%rbp), %rax
	leaq	-208(%rbp), %rsi
	movl	$2, %edx
	movq	%r13, -208(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	movl	(%r15), %r9d
	popq	%rdi
	popq	%r8
	testl	%r9d, %r9d
	jg	.L934
	movl	-216(%rbp), %eax
	cmpl	$-1, %eax
	je	.L931
	movl	-212(%rbp), %edx
	cmpl	$-1, %edx
	je	.L931
	leal	2(%r14), %esi
	cmpl	%esi, -236(%rbp)
	jne	.L933
	movq	-256(%rbp), %rax
	movl	%edx, (%rax)
.L934:
	movq	16(%r12), %rdi
	leaq	1(%r14), %rax
	cmpq	%r14, %rbx
	jne	.L951
.L979:
	movq	%r15, %r12
	movslq	-240(%rbp), %rbx
	movq	%r13, %r15
	movq	-264(%rbp), %r14
	movq	-256(%rbp), %r13
.L948:
	movq	-232(%rbp), %rax
	movq	152(%rdi), %rdi
	salq	$6, %rbx
	leaq	-64(%rax,%rbx), %rbx
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	call	*40(%rax)
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L917
	subq	$8, %rsp
	movq	%r15, %rcx
	movl	$2, %edx
	movq	%rax, %rdi
	pushq	%r12
	leaq	-208(%rbp), %rsi
	movl	$2, %r9d
	leaq	-216(%rbp), %r8
	movq	%r15, -208(%rbp)
	movq	%rbx, -200(%rbp)
	call	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	movl	(%r12), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L917
	movl	-216(%rbp), %edx
	cmpl	$-1, %edx
	je	.L938
	movl	-212(%rbp), %eax
	cmpl	$-1, %eax
	je	.L938
	movl	-268(%rbp), %esi
	cmpl	%esi, -236(%rbp)
	je	.L971
	movl	0(%r13), %eax
	testl	%eax, %eax
	js	.L942
.L970:
	addl	%edx, %eax
.L971:
	movl	%eax, 0(%r13)
.L920:
	testl	%eax, %eax
	js	.L942
	movswl	8(%r14), %edx
	testw	%dx, %dx
	js	.L943
	sarl	$5, %edx
.L944:
	addl	%edx, %eax
	movl	%eax, 0(%r13)
.L942:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L945
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L946:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L973:
	movl	$27, (%r12)
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L974:
	movl	-236(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L907
	movswl	8(%rcx), %eax
	testw	%ax, %ax
	js	.L908
	sarl	$5, %eax
.L909:
	movl	%eax, 0(%r13)
.L907:
	movq	-232(%rbp), %rax
	movswl	8(%rax), %ecx
	testw	%cx, %cx
	js	.L910
	sarl	$5, %ecx
	movq	%rax, %rsi
.L911:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L931:
	movl	$3, (%r15)
	movq	16(%r12), %rdi
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L933:
	movq	-256(%rbp), %rcx
	movl	(%rcx), %edx
	testl	%edx, %edx
	js	.L934
	addl	%edx, %eax
	movl	%eax, (%rcx)
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L976:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L908:
	movl	12(%rcx), %eax
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L910:
	movl	12(%rax), %ecx
	movq	%rax, %rsi
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L975:
	subq	$8, %rsp
	movq	%r15, %rcx
	movl	$2, %r9d
	movq	%rax, %rdi
	pushq	%r12
	leaq	-208(%rbp), %rsi
	leaq	-216(%rbp), %r8
	movq	%rdx, -200(%rbp)
	movl	$2, %edx
	movq	%r15, -208(%rbp)
	call	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	movl	(%r12), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L917
	movl	-216(%rbp), %edx
	cmpl	$-1, %edx
	je	.L938
	movl	-212(%rbp), %eax
	cmpl	$-1, %eax
	je	.L938
	cmpl	$1, -236(%rbp)
	je	.L971
	movl	0(%r13), %eax
	testl	%eax, %eax
	jns	.L970
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L978:
	subq	$8, %rsp
	addq	$8, %rdi
	movq	%r15, %rcx
	movq	%rdx, -200(%rbp)
	pushq	%r12
	leaq	-208(%rbp), %rsi
	movl	$2, %r9d
	leaq	-216(%rbp), %r8
	movl	$2, %edx
	movq	%r11, -248(%rbp)
	movq	%r15, -208(%rbp)
	call	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	popq	%r11
	popq	%rax
	movl	(%r12), %eax
	movq	-248(%rbp), %r11
	testl	%eax, %eax
	jg	.L972
	movl	-216(%rbp), %eax
	cmpl	$-1, %eax
	je	.L924
	movl	-212(%rbp), %edx
	cmpl	$-1, %edx
	je	.L924
	cmpl	$1, -236(%rbp)
	jne	.L926
	movl	%edx, 0(%r13)
	.p2align 4,,10
	.p2align 3
.L972:
	movq	16(%r11), %rdi
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L945:
	movl	-116(%rbp), %ecx
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L938:
	movl	$3, (%r12)
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L943:
	movl	12(%r14), %edx
	jmp	.L944
.L924:
	movl	$3, (%r12)
	jmp	.L972
.L926:
	movl	0(%r13), %edx
	testl	%edx, %edx
	js	.L972
	addl	%edx, %eax
	movl	%eax, 0(%r13)
	jmp	.L972
.L977:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3473:
	.size	_ZNK6icu_6713ListFormatter6formatEPKNS_13UnicodeStringEiRS1_iRiR10UErrorCode, .-_ZNK6icu_6713ListFormatter6formatEPKNS_13UnicodeStringEiRS1_iRiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713ListFormatter7format_EPKNS_13UnicodeStringEiRS1_iRiPNS_20FieldPositionHandlerER10UErrorCode
	.type	_ZNK6icu_6713ListFormatter7format_EPKNS_13UnicodeStringEiRS1_iRiPNS_20FieldPositionHandlerER10UErrorCode, @function
_ZNK6icu_6713ListFormatter7format_EPKNS_13UnicodeStringEiRS1_iRiPNS_20FieldPositionHandlerER10UErrorCode:
.LFB3481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	subq	$8, %rsp
	movq	24(%rbp), %rax
	movl	$-1, (%r9)
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L981
	cmpq	$0, 16(%rdi)
	je	.L984
	testl	%edx, %edx
	jle	.L981
	pushq	%rax
	movq	%r12, %rcx
	pushq	16(%rbp)
	call	_ZNK6icu_6713ListFormatter7format_EPKNS_13UnicodeStringEiRS1_iRiPNS_20FieldPositionHandlerER10UErrorCode.part.0
	popq	%rax
	popq	%rdx
.L981:
	movq	%r12, %rax
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L984:
	.cfi_restore_state
	movl	$27, (%rax)
	movq	%r12, %rax
	movq	-8(%rbp), %r12
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3481:
	.size	_ZNK6icu_6713ListFormatter7format_EPKNS_13UnicodeStringEiRS1_iRiPNS_20FieldPositionHandlerER10UErrorCode, .-_ZNK6icu_6713ListFormatter7format_EPKNS_13UnicodeStringEiRS1_iRiPNS_20FieldPositionHandlerER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ListFormatter14createInstanceER10UErrorCode
	.type	_ZN6icu_6713ListFormatter14createInstanceER10UErrorCode, @function
_ZN6icu_6713ListFormatter14createInstanceER10UErrorCode:
.LFB3457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-272(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676LocaleC1Ev@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L988
	movq	%rbx, %rdx
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCode.part.0
	movq	%rax, %r14
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L988
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L989
	leaq	16+_ZTVN6icu_6713ListFormatterE(%rip), %rax
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
.L987:
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L993
	addq	$240, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L988:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L987
.L993:
	call	__stack_chk_fail@PLT
.L989:
	movl	$7, (%rbx)
	jmp	.L987
	.cfi_endproc
.LFE3457:
	.size	_ZN6icu_6713ListFormatter14createInstanceER10UErrorCode, .-_ZN6icu_6713ListFormatter14createInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713ListFormatter20formatStringsToValueEPKNS_13UnicodeStringEiR10UErrorCode
	.type	_ZNK6icu_6713ListFormatter20formatStringsToValueEPKNS_13UnicodeStringEiR10UErrorCode, @function
_ZNK6icu_6713ListFormatter20formatStringsToValueEPKNS_13UnicodeStringEiR10UErrorCode:
.LFB3474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$104, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$280, %rsp
	movq	%rdx, -304(%rbp)
	movl	%ecx, -292(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L995
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movl	$5, %esi
	call	_ZN6icu_6739FormattedValueFieldPositionIteratorImplC2EiR10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6717FormattedListDataE(%rip), %rax
	movq	%rax, (%r14)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1018
.L996:
	leaq	16+_ZTVN6icu_6713FormattedListE(%rip), %rcx
	movl	%eax, 16(%r12)
	movq	%rcx, (%r12)
	movq	$0, 8(%r12)
	testq	%r14, %r14
	je	.L994
.L998:
	movq	(%r14), %rax
	leaq	_ZN6icu_6717FormattedListDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1011
	leaq	16+_ZTVN6icu_6717FormattedListDataE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L994:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1019
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L995:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L996
	movl	$7, (%rbx)
	movl	$7, %eax
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1018:
	movl	$2, %edi
	leaq	-240(%rbp), %r15
	movq	%rbx, %rdx
	movq	%r14, %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%di, -184(%rbp)
	movq	%r15, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl10getHandlerER10UErrorCode@PLT
	movl	(%rbx), %r8d
	leaq	-192(%rbp), %rax
	movl	$3, -204(%rbp)
	movl	$-1, -276(%rbp)
	movq	%rax, -312(%rbp)
	testl	%r8d, %r8d
	jg	.L1013
	cmpq	$0, 16(%r13)
	je	.L1020
	movl	-292(%rbp), %esi
	testl	%esi, %esi
	jle	.L1004
	pushq	%rbx
	movl	-292(%rbp), %edx
	movq	%rax, %rcx
	movl	$-1, %r8d
	pushq	%r15
	movq	-304(%rbp), %rsi
	leaq	-276(%rbp), %r9
	movq	%r13, %rdi
	call	_ZNK6icu_6713ListFormatter7format_EPKNS_13UnicodeStringEiRS1_iRiPNS_20FieldPositionHandlerER10UErrorCode.part.0
	movl	(%rbx), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L1013
.L1004:
	movl	-208(%rbp), %eax
	testl	%eax, %eax
	jle	.L1013
	movl	%eax, (%rbx)
.L1013:
	leaq	-128(%rbp), %r13
	movq	-312(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl12appendStringENS_13UnicodeStringER10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1005
	leaq	16+_ZTVN6icu_6713FormattedListE(%rip), %rcx
	movl	%eax, 16(%r12)
	movq	%rcx, (%r12)
	movq	$0, 8(%r12)
.L1006:
	movq	%r15, %rdi
	call	_ZN6icu_6728FieldPositionIteratorHandlerD1Ev@PLT
	movq	-312(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1005:
	leaq	-272(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_6724ConstrainedFieldPositionC1Ev@PLT
	movl	$1, %edx
	movl	$3, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6724ConstrainedFieldPosition14constrainFieldEii@PLT
	xorl	%r8d, %r8d
	movl	$4099, -204(%rbp)
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1021:
	movl	-292(%rbp), %r8d
	movl	-256(%rbp), %ecx
	movq	%r15, %rdi
	movl	-260(%rbp), %edx
	leal	1(%r8), %eax
	movl	%r8d, %esi
	movl	%eax, -292(%rbp)
	call	_ZN6icu_6728FieldPositionIteratorHandler12addAttributeEiii@PLT
	movl	-292(%rbp), %r8d
.L1008:
	movq	(%r14), %rax
	movl	%r8d, -292(%rbp)
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L1021
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1022
.L1009:
	leaq	16+_ZTVN6icu_6713FormattedListE(%rip), %rbx
	movl	%eax, 16(%r12)
	movq	%r13, %rdi
	movq	%rbx, (%r12)
	movq	$0, 8(%r12)
	call	_ZN6icu_6724ConstrainedFieldPositionD1Ev@PLT
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1011:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1022:
	movl	-208(%rbp), %eax
	testl	%eax, %eax
	jle	.L1010
	movl	%eax, (%rbx)
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1020:
	movl	$27, (%rbx)
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	%r14, %rdi
	call	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl4sortEv@PLT
	leaq	16+_ZTVN6icu_6713FormattedListE(%rip), %rax
	movq	%r14, 8(%r12)
	movq	%r13, %rdi
	movq	%rax, (%r12)
	movl	$0, 16(%r12)
	call	_ZN6icu_6724ConstrainedFieldPositionD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6728FieldPositionIteratorHandlerD1Ev@PLT
	movq	-312(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L994
.L1019:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3474:
	.size	_ZNK6icu_6713ListFormatter20formatStringsToValueEPKNS_13UnicodeStringEiR10UErrorCode, .-_ZNK6icu_6713ListFormatter20formatStringsToValueEPKNS_13UnicodeStringEiR10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6713FormattedListE
	.section	.rodata._ZTSN6icu_6713FormattedListE,"aG",@progbits,_ZTSN6icu_6713FormattedListE,comdat
	.align 16
	.type	_ZTSN6icu_6713FormattedListE, @object
	.size	_ZTSN6icu_6713FormattedListE, 25
_ZTSN6icu_6713FormattedListE:
	.string	"N6icu_6713FormattedListE"
	.weak	_ZTIN6icu_6713FormattedListE
	.section	.data.rel.ro._ZTIN6icu_6713FormattedListE,"awG",@progbits,_ZTIN6icu_6713FormattedListE,comdat
	.align 8
	.type	_ZTIN6icu_6713FormattedListE, @object
	.size	_ZTIN6icu_6713FormattedListE, 56
_ZTIN6icu_6713FormattedListE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_6713FormattedListE
	.long	0
	.long	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.quad	_ZTIN6icu_6714FormattedValueE
	.quad	2
	.weak	_ZTSN6icu_6713ListFormatterE
	.section	.rodata._ZTSN6icu_6713ListFormatterE,"aG",@progbits,_ZTSN6icu_6713ListFormatterE,comdat
	.align 16
	.type	_ZTSN6icu_6713ListFormatterE, @object
	.size	_ZTSN6icu_6713ListFormatterE, 25
_ZTSN6icu_6713ListFormatterE:
	.string	"N6icu_6713ListFormatterE"
	.weak	_ZTIN6icu_6713ListFormatterE
	.section	.data.rel.ro._ZTIN6icu_6713ListFormatterE,"awG",@progbits,_ZTIN6icu_6713ListFormatterE,comdat
	.align 8
	.type	_ZTIN6icu_6713ListFormatterE, @object
	.size	_ZTIN6icu_6713ListFormatterE, 24
_ZTIN6icu_6713ListFormatterE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713ListFormatterE
	.quad	_ZTIN6icu_677UObjectE
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_114PatternHandlerE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_114PatternHandlerE, 24
_ZTIN6icu_6712_GLOBAL__N_114PatternHandlerE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_114PatternHandlerE
	.quad	_ZTIN6icu_677UObjectE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_114PatternHandlerE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_114PatternHandlerE, 41
_ZTSN6icu_6712_GLOBAL__N_114PatternHandlerE:
	.string	"*N6icu_6712_GLOBAL__N_114PatternHandlerE"
	.section	.data.rel.ro
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_117ContextualHandlerE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_117ContextualHandlerE, 24
_ZTIN6icu_6712_GLOBAL__N_117ContextualHandlerE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_117ContextualHandlerE
	.quad	_ZTIN6icu_6712_GLOBAL__N_114PatternHandlerE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_117ContextualHandlerE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_117ContextualHandlerE, 44
_ZTSN6icu_6712_GLOBAL__N_117ContextualHandlerE:
	.string	"*N6icu_6712_GLOBAL__N_117ContextualHandlerE"
	.weak	_ZTSN6icu_6717FormattedListDataE
	.section	.rodata._ZTSN6icu_6717FormattedListDataE,"aG",@progbits,_ZTSN6icu_6717FormattedListDataE,comdat
	.align 16
	.type	_ZTSN6icu_6717FormattedListDataE, @object
	.size	_ZTSN6icu_6717FormattedListDataE, 29
_ZTSN6icu_6717FormattedListDataE:
	.string	"N6icu_6717FormattedListDataE"
	.weak	_ZTIN6icu_6717FormattedListDataE
	.section	.data.rel.ro._ZTIN6icu_6717FormattedListDataE,"awG",@progbits,_ZTIN6icu_6717FormattedListDataE,comdat
	.align 8
	.type	_ZTIN6icu_6717FormattedListDataE, @object
	.size	_ZTIN6icu_6717FormattedListDataE, 24
_ZTIN6icu_6717FormattedListDataE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717FormattedListDataE
	.quad	_ZTIN6icu_6739FormattedValueFieldPositionIteratorImplE
	.weak	_ZTSN6icu_6713ListFormatter16ListPatternsSinkE
	.section	.rodata._ZTSN6icu_6713ListFormatter16ListPatternsSinkE,"aG",@progbits,_ZTSN6icu_6713ListFormatter16ListPatternsSinkE,comdat
	.align 32
	.type	_ZTSN6icu_6713ListFormatter16ListPatternsSinkE, @object
	.size	_ZTSN6icu_6713ListFormatter16ListPatternsSinkE, 43
_ZTSN6icu_6713ListFormatter16ListPatternsSinkE:
	.string	"N6icu_6713ListFormatter16ListPatternsSinkE"
	.weak	_ZTIN6icu_6713ListFormatter16ListPatternsSinkE
	.section	.data.rel.ro._ZTIN6icu_6713ListFormatter16ListPatternsSinkE,"awG",@progbits,_ZTIN6icu_6713ListFormatter16ListPatternsSinkE,comdat
	.align 8
	.type	_ZTIN6icu_6713ListFormatter16ListPatternsSinkE, @object
	.size	_ZTIN6icu_6713ListFormatter16ListPatternsSinkE, 24
_ZTIN6icu_6713ListFormatter16ListPatternsSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713ListFormatter16ListPatternsSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.section	.data.rel.ro
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_114PatternHandlerE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_114PatternHandlerE, 64
_ZTVN6icu_6712_GLOBAL__N_114PatternHandlerE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_114PatternHandlerE
	.quad	_ZN6icu_6712_GLOBAL__N_114PatternHandlerD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_114PatternHandlerD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6712_GLOBAL__N_114PatternHandler5cloneEv
	.quad	_ZNK6icu_6712_GLOBAL__N_114PatternHandler13getTwoPatternERKNS_13UnicodeStringE
	.quad	_ZNK6icu_6712_GLOBAL__N_114PatternHandler13getEndPatternERKNS_13UnicodeStringE
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_117ContextualHandlerE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_117ContextualHandlerE, 64
_ZTVN6icu_6712_GLOBAL__N_117ContextualHandlerE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_117ContextualHandlerE
	.quad	_ZN6icu_6712_GLOBAL__N_117ContextualHandlerD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_117ContextualHandlerD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6712_GLOBAL__N_117ContextualHandler5cloneEv
	.quad	_ZNK6icu_6712_GLOBAL__N_117ContextualHandler13getTwoPatternERKNS_13UnicodeStringE
	.quad	_ZNK6icu_6712_GLOBAL__N_117ContextualHandler13getEndPatternERKNS_13UnicodeStringE
	.weak	_ZTVN6icu_6717FormattedListDataE
	.section	.data.rel.ro._ZTVN6icu_6717FormattedListDataE,"awG",@progbits,_ZTVN6icu_6717FormattedListDataE,comdat
	.align 8
	.type	_ZTVN6icu_6717FormattedListDataE, @object
	.size	_ZTVN6icu_6717FormattedListDataE, 64
_ZTVN6icu_6717FormattedListDataE:
	.quad	0
	.quad	_ZTIN6icu_6717FormattedListDataE
	.quad	_ZN6icu_6717FormattedListDataD1Ev
	.quad	_ZN6icu_6717FormattedListDataD0Ev
	.quad	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl8toStringER10UErrorCode
	.quad	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl12toTempStringER10UErrorCode
	.quad	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl8appendToERNS_10AppendableER10UErrorCode
	.quad	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.weak	_ZTVN6icu_6713FormattedListE
	.section	.data.rel.ro.local._ZTVN6icu_6713FormattedListE,"awG",@progbits,_ZTVN6icu_6713FormattedListE,comdat
	.align 8
	.type	_ZTVN6icu_6713FormattedListE, @object
	.size	_ZTVN6icu_6713FormattedListE, 64
_ZTVN6icu_6713FormattedListE:
	.quad	0
	.quad	_ZTIN6icu_6713FormattedListE
	.quad	_ZN6icu_6713FormattedListD1Ev
	.quad	_ZN6icu_6713FormattedListD0Ev
	.quad	_ZNK6icu_6713FormattedList8toStringER10UErrorCode
	.quad	_ZNK6icu_6713FormattedList12toTempStringER10UErrorCode
	.quad	_ZNK6icu_6713FormattedList8appendToERNS_10AppendableER10UErrorCode
	.quad	_ZNK6icu_6713FormattedList12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.weak	_ZTVN6icu_6713ListFormatter16ListPatternsSinkE
	.section	.data.rel.ro._ZTVN6icu_6713ListFormatter16ListPatternsSinkE,"awG",@progbits,_ZTVN6icu_6713ListFormatter16ListPatternsSinkE,comdat
	.align 8
	.type	_ZTVN6icu_6713ListFormatter16ListPatternsSinkE, @object
	.size	_ZTVN6icu_6713ListFormatter16ListPatternsSinkE, 48
_ZTVN6icu_6713ListFormatter16ListPatternsSinkE:
	.quad	0
	.quad	_ZTIN6icu_6713ListFormatter16ListPatternsSinkE
	.quad	_ZN6icu_6713ListFormatter16ListPatternsSinkD1Ev
	.quad	_ZN6icu_6713ListFormatter16ListPatternsSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6713ListFormatter16ListPatternsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.weak	_ZTVN6icu_6713ListFormatterE
	.section	.data.rel.ro._ZTVN6icu_6713ListFormatterE,"awG",@progbits,_ZTVN6icu_6713ListFormatterE,comdat
	.align 8
	.type	_ZTVN6icu_6713ListFormatterE, @object
	.size	_ZTVN6icu_6713ListFormatterE, 40
_ZTVN6icu_6713ListFormatterE:
	.quad	0
	.quad	_ZTIN6icu_6713ListFormatterE
	.quad	_ZN6icu_6713ListFormatterD1Ev
	.quad	_ZN6icu_6713ListFormatterD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.section	.rodata
	.align 16
	.type	_ZN6icu_67L11aliasPrefixE, @object
	.size	_ZN6icu_67L11aliasPrefixE, 24
_ZN6icu_67L11aliasPrefixE:
	.value	108
	.value	105
	.value	115
	.value	116
	.value	80
	.value	97
	.value	116
	.value	116
	.value	101
	.value	114
	.value	110
	.value	47
	.local	_ZZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCodeE18listFormatterMutex
	.comm	_ZZN6icu_6713ListFormatter21getListFormatInternalERKNS_6LocaleEPKcR10UErrorCodeE18listFormatterMutex,56,32
	.local	_ZN6icu_67L15listPatternHashE
	.comm	_ZN6icu_67L15listPatternHashE,8,8
	.section	.data.rel.ro.local,"aw"
	.align 8
.LC10:
	.quad	_ZTVN6icu_6713ListFormatter16ListPatternsSinkE+16
	.section	.data.rel.ro
	.align 8
.LC11:
	.quad	_ZTVN6icu_6713UnicodeStringE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
