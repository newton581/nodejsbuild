	.file	"number_utils.cpp"
	.text
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	""
	.string	""
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"NumberElements/"
.LC2:
	.string	"/patterns/"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_112doGetPatternEP15UResourceBundlePKcS3_R10UErrorCodeS5_.constprop.0, @function
_ZN12_GLOBAL__N_112doGetPatternEP15UResourceBundlePKcS3_R10UErrorCodeS5_.constprop.0:
.LFB4932:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	leaq	.LC1(%rip), %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$120, %rsp
	movq	%r8, -160(%rbp)
	movq	%rdi, -152(%rbp)
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-115(%rbp), %rax
	movl	$0, -72(%rbp)
	movq	%rax, -128(%rbp)
	xorl	%eax, %eax
	movw	%ax, -116(%rbp)
	movl	$40, -120(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%r12, %rdi
	movq	%rbx, %rcx
	movq	-144(%rbp), %rsi
	leaq	.LC0(%rip), %r12
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L2
	movq	-160(%rbp), %rcx
	movq	-128(%rbp), %rsi
	xorl	%edx, %edx
	movq	-152(%rbp), %rdi
	call	ures_getStringByKeyWithFallback_67@PLT
	movq	%rax, %r12
.L2:
	cmpb	$0, -116(%rbp)
	jne	.L9
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L10
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L1
.L10:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4932:
	.size	_ZN12_GLOBAL__N_112doGetPatternEP15UResourceBundlePKcS3_R10UErrorCodeS5_.constprop.0, .-_ZN12_GLOBAL__N_112doGetPatternEP15UResourceBundlePKcS3_R10UErrorCodeS5_.constprop.0
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3930:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3930:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3933:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L24
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L12
	cmpb	$0, 12(%rbx)
	jne	.L25
.L16:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L12:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L16
	.cfi_endproc
.LFE3933:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3936:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L28
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3936:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3939:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L31
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3939:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L37
.L33:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L38
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3941:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3942:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3942:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3943:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3943:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3944:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3944:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3945:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3945:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3946:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3946:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3947:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L54
	testl	%edx, %edx
	jle	.L54
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L57
.L46:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L46
	.cfi_endproc
.LFE3947:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L61
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L61
	testl	%r12d, %r12d
	jg	.L68
	cmpb	$0, 12(%rbx)
	jne	.L69
.L63:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L63
.L69:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L61:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3948:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L71
	movq	(%rdi), %r8
.L72:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L75
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L75
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L75:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3949:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3950:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L82
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3950:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3951:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3951:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3952:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3952:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3953:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3953:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3955:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3955:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3957:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3957:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.section	.rodata.str1.1
.LC3:
	.string	"scientificFormat"
.LC4:
	.string	"decimalFormat"
.LC5:
	.string	"accountingFormat"
.LC6:
	.string	"percentFormat"
.LC7:
	.string	"currencyFormat"
.LC8:
	.string	"latn"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB9:
	.text
.LHOTB9:
	.p2align 4
	.globl	_ZN6icu_676number4impl5utils18getPatternForStyleERKNS_6LocaleEPKcNS1_16CldrPatternStyleER10UErrorCode
	.type	_ZN6icu_676number4impl5utils18getPatternForStyleERKNS_6LocaleEPKcNS1_16CldrPatternStyleER10UErrorCode, @function
_ZN6icu_676number4impl5utils18getPatternForStyleERKNS_6LocaleEPKcNS1_16CldrPatternStyleER10UErrorCode:
.LFB3676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$4, %edx
	ja	.L89
	movq	%rcx, %rbx
	movl	%edx, %edx
	leaq	.L91(%rip), %rcx
	movq	%rdi, %r8
	movslq	(%rcx,%rdx,4), %rax
	movq	%rsi, %r12
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L91:
	.long	.L95-.L91
	.long	.L94-.L91
	.long	.L100-.L91
	.long	.L92-.L91
	.long	.L90-.L91
	.text
	.p2align 4,,10
	.p2align 3
.L92:
	leaq	.LC6(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L93:
	movq	40(%r8), %rsi
	movq	%rbx, %rdx
	xorl	%edi, %edi
	call	ures_open_67@PLT
	movl	(%rbx), %esi
	movq	%rax, %r13
	testl	%esi, %esi
	jle	.L108
.L96:
	leaq	.LC0(%rip), %r14
.L97:
	testq	%r13, %r13
	je	.L88
	movq	%r13, %rdi
	call	ures_close_67@PLT
.L88:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	leaq	.LC5(%rip), %r15
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L94:
	leaq	.LC7(%rip), %r15
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	.LC4(%rip), %r15
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	.LC3(%rip), %r15
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	-60(%rbp), %r8
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movl	$0, -60(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN12_GLOBAL__N_112doGetPatternEP15UResourceBundlePKcS3_R10UErrorCodeS5_.constprop.0
	movl	(%rbx), %ecx
	movq	%rax, %r14
	testl	%ecx, %ecx
	jg	.L96
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jle	.L97
	leaq	.LC8(%rip), %r9
	movl	$5, %ecx
	movq	%r12, %rdi
	movq	-72(%rbp), %r8
	movq	%r9, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L97
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%r13, %rdi
	movl	$0, -60(%rbp)
	call	_ZN12_GLOBAL__N_112doGetPatternEP15UResourceBundlePKcS3_R10UErrorCodeS5_.constprop.0
	movq	%rax, %r14
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L97
	jmp	.L96
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl5utils18getPatternForStyleERKNS_6LocaleEPKcNS1_16CldrPatternStyleER10UErrorCode.cold, @function
_ZN6icu_676number4impl5utils18getPatternForStyleERKNS_6LocaleEPKcNS1_16CldrPatternStyleER10UErrorCode.cold:
.LFSB3676:
.L89:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3676:
	.text
	.size	_ZN6icu_676number4impl5utils18getPatternForStyleERKNS_6LocaleEPKcNS1_16CldrPatternStyleER10UErrorCode, .-_ZN6icu_676number4impl5utils18getPatternForStyleERKNS_6LocaleEPKcNS1_16CldrPatternStyleER10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl5utils18getPatternForStyleERKNS_6LocaleEPKcNS1_16CldrPatternStyleER10UErrorCode.cold, .-_ZN6icu_676number4impl5utils18getPatternForStyleERKNS_6LocaleEPKcNS1_16CldrPatternStyleER10UErrorCode.cold
.LCOLDE9:
	.text
.LHOTE9:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6DecNumC2Ev
	.type	_ZN6icu_676number4impl6DecNumC2Ev, @function
_ZN6icu_676number4impl6DecNumC2Ev:
.LFB3678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	64(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movl	$34, 8(%rdi)
	movb	$0, 12(%rdi)
	movq	%r12, %rdi
	call	uprv_decContextDefault_67@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	call	uprv_decContextSetRounding_67@PLT
	movl	$0, 80(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3678:
	.size	_ZN6icu_676number4impl6DecNumC2Ev, .-_ZN6icu_676number4impl6DecNumC2Ev
	.globl	_ZN6icu_676number4impl6DecNumC1Ev
	.set	_ZN6icu_676number4impl6DecNumC1Ev,_ZN6icu_676number4impl6DecNumC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6DecNumC2ERKS2_R10UErrorCode
	.type	_ZN6icu_676number4impl6DecNumC2ERKS2_R10UErrorCode, @function
_ZN6icu_676number4impl6DecNumC2ERKS2_R10UErrorCode:
.LFB3681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	$34, 8(%rdi)
	movb	$0, 12(%rdi)
	movq	80(%rsi), %rax
	movdqu	64(%rsi), %xmm0
	movl	64(%rsi), %r14d
	movq	%r13, (%rdi)
	movq	%rax, 80(%rdi)
	movl	88(%rsi), %eax
	movups	%xmm0, 64(%rdi)
	movl	%eax, 88(%rdi)
	cmpl	$34, %r14d
	jle	.L113
	movslq	%r14d, %rdi
	movq	%rdx, %r15
	addq	$12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L114
	movq	(%rbx), %rdi
	cmpb	$0, 12(%rbx)
	movq	(%rdi), %rax
	movq	%rax, 0(%r13)
	movl	8(%rdi), %eax
	movl	%eax, 8(%r13)
	jne	.L118
.L115:
	movq	%r13, (%rbx)
	movl	%r14d, 8(%rbx)
	movb	$1, 12(%rbx)
.L113:
	movq	(%r12), %rax
	movq	(%rax), %rdx
	movq	%rdx, 0(%r13)
	movl	8(%rax), %eax
	movl	%eax, 8(%r13)
	movq	(%rbx), %rdi
	movq	(%r12), %rsi
	movslq	8(%r12), %rdx
	addq	$8, %rsp
	popq	%rbx
	addq	$12, %rdi
	popq	%r12
	addq	$12, %rsi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcpy@PLT
.L114:
	.cfi_restore_state
	movl	$7, (%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	call	uprv_free_67@PLT
	jmp	.L115
	.cfi_endproc
.LFE3681:
	.size	_ZN6icu_676number4impl6DecNumC2ERKS2_R10UErrorCode, .-_ZN6icu_676number4impl6DecNumC2ERKS2_R10UErrorCode
	.globl	_ZN6icu_676number4impl6DecNumC1ERKS2_R10UErrorCode
	.set	_ZN6icu_676number4impl6DecNumC1ERKS2_R10UErrorCode,_ZN6icu_676number4impl6DecNumC2ERKS2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6DecNum5setToEPKcR10UErrorCode
	.type	_ZN6icu_676number4impl6DecNum5setToEPKcR10UErrorCode, @function
_ZN6icu_676number4impl6DecNum5setToEPKcR10UErrorCode:
.LFB3684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	strlen@PLT
	cmpl	$34, %eax
	jg	.L130
	movl	$34, 64(%rbx)
	movq	(%rbx), %r15
.L124:
	leaq	64(%rbx), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	uprv_decNumberFromString_67@PLT
	movl	84(%rbx), %eax
	testb	$1, %al
	jne	.L131
	testl	%eax, %eax
	jne	.L128
	movq	(%rbx), %rax
	testb	$112, 8(%rax)
	jne	.L128
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movl	$16, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	movl	$65808, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movslq	%eax, %rdi
	movq	%rax, %r13
	addq	$12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L132
	movq	(%rbx), %rdi
	cmpb	$0, 12(%rbx)
	movq	(%rdi), %rax
	movq	%rax, (%r15)
	movl	8(%rdi), %eax
	movl	%eax, 8(%r15)
	jne	.L133
.L123:
	movq	%r15, (%rbx)
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
.L122:
	movl	%r13d, 64(%rbx)
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L133:
	call	uprv_free_67@PLT
	jmp	.L123
.L132:
	movq	(%rbx), %r15
	jmp	.L122
	.cfi_endproc
.LFE3684:
	.size	_ZN6icu_676number4impl6DecNum5setToEPKcR10UErrorCode, .-_ZN6icu_676number4impl6DecNum5setToEPKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6DecNum5setToEdR10UErrorCode
	.type	_ZN6icu_676number4impl6DecNum5setToEdR10UErrorCode, @function
_ZN6icu_676number4impl6DecNum5setToEdR10UErrorCode:
.LFB3685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm0
	jp	.L135
	movsd	.LC11(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC10(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jnb	.L153
.L135:
	movl	$16, (%r12)
.L134:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L156
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	subq	$8, %rsp
	leaq	-84(%rbp), %rax
	leaq	-80(%rbp), %r14
	xorl	%esi, %esi
	pushq	%rax
	movq	%r14, %rdx
	movq	%rdi, %rbx
	leaq	-88(%rbp), %r9
	xorl	%edi, %edi
	leaq	-89(%rbp), %r8
	movl	$23, %ecx
	movsd	%xmm0, -104(%rbp)
	call	_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_@PLT
	movl	-88(%rbp), %r15d
	popq	%rax
	movsd	-104(%rbp), %xmm0
	popq	%rdx
	cmpl	$34, %r15d
	jg	.L157
	movl	$34, 64(%rbx)
	movq	(%rbx), %r13
.L143:
	leaq	64(%rbx), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movsd	%xmm0, -104(%rbp)
	call	uprv_decNumberFromString_67@PLT
	movl	84(%rbx), %eax
	movsd	-104(%rbp), %xmm0
	testb	$1, %al
	jne	.L158
	movq	(%rbx), %rdx
	testl	%eax, %eax
	jne	.L155
	testb	$112, 8(%rdx)
	jne	.L155
.L145:
	movl	-84(%rbp), %eax
	subl	-88(%rbp), %eax
	addl	%eax, 4(%rdx)
	movmskpd	%xmm0, %eax
	andl	$1, %eax
	negl	%eax
	andl	$-128, %eax
	orb	%al, 8(%rdx)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L155:
	movl	$16, (%r12)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L158:
	movl	$65808, (%r12)
	movq	(%rbx), %rdx
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L157:
	movslq	%r15d, %rdi
	addq	$12, %rdi
	call	uprv_malloc_67@PLT
	movsd	-104(%rbp), %xmm0
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L159
	movq	(%rbx), %rdi
	cmpb	$0, 12(%rbx)
	movq	(%rdi), %rax
	movq	%rax, 0(%r13)
	movl	8(%rdi), %eax
	movl	%eax, 8(%r13)
	jne	.L160
.L142:
	movq	%r13, (%rbx)
	movl	%r15d, 8(%rbx)
	movb	$1, 12(%rbx)
.L141:
	movl	%r15d, 64(%rbx)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L160:
	movsd	%xmm0, -104(%rbp)
	call	uprv_free_67@PLT
	movsd	-104(%rbp), %xmm0
	jmp	.L142
.L156:
	call	__stack_chk_fail@PLT
.L159:
	movq	(%rbx), %r13
	jmp	.L141
	.cfi_endproc
.LFE3685:
	.size	_ZN6icu_676number4impl6DecNum5setToEdR10UErrorCode, .-_ZN6icu_676number4impl6DecNum5setToEdR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6DecNum6_setToEPKciR10UErrorCode
	.type	_ZN6icu_676number4impl6DecNum6_setToEPKciR10UErrorCode, @function
_ZN6icu_676number4impl6DecNum6_setToEPKciR10UErrorCode:
.LFB3686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$34, %edx
	jg	.L172
	movl	$34, 64(%rdi)
	movq	(%rdi), %r13
.L166:
	leaq	64(%rbx), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	uprv_decNumberFromString_67@PLT
	movl	84(%rbx), %eax
	testb	$1, %al
	jne	.L173
	testl	%eax, %eax
	jne	.L170
	movq	(%rbx), %rax
	testb	$112, 8(%rax)
	jne	.L170
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movl	$16, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movl	$65808, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movslq	%edx, %rdi
	movl	%edx, %r12d
	addq	$12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L174
	movq	(%rbx), %rdi
	cmpb	$0, 12(%rbx)
	movq	(%rdi), %rax
	movq	%rax, 0(%r13)
	movl	8(%rdi), %eax
	movl	%eax, 8(%r13)
	jne	.L175
.L165:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L164:
	movl	%r12d, 64(%rbx)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L175:
	call	uprv_free_67@PLT
	jmp	.L165
.L174:
	movq	(%rbx), %r13
	jmp	.L164
	.cfi_endproc
.LFE3686:
	.size	_ZN6icu_676number4impl6DecNum6_setToEPKciR10UErrorCode, .-_ZN6icu_676number4impl6DecNum6_setToEPKciR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6DecNum5setToENS_11StringPieceER10UErrorCode
	.type	_ZN6icu_676number4impl6DecNum5setToENS_11StringPieceER10UErrorCode, @function
_ZN6icu_676number4impl6DecNum5setToENS_11StringPieceER10UErrorCode:
.LFB3683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	leaq	-112(%rbp), %rdi
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-99(%rbp), %rax
	movl	$0, -56(%rbp)
	movq	%rax, -112(%rbp)
	xorl	%eax, %eax
	movl	$40, -104(%rbp)
	movw	%ax, -100(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L185
	movq	-112(%rbp), %rsi
	movq	%r12, %rcx
	movl	%ebx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl6DecNum6_setToEPKciR10UErrorCode
.L185:
	cmpb	$0, -100(%rbp)
	jne	.L186
.L176:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L187
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L176
.L187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3683:
	.size	_ZN6icu_676number4impl6DecNum5setToENS_11StringPieceER10UErrorCode, .-_ZN6icu_676number4impl6DecNum5setToENS_11StringPieceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6DecNum5setToEPKhiibR10UErrorCode
	.type	_ZN6icu_676number4impl6DecNum5setToEPKhiibR10UErrorCode, @function
_ZN6icu_676number4impl6DecNum5setToEPKhiibR10UErrorCode:
.LFB3687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$34, %edx
	jg	.L202
	movl	$34, 64(%rdi)
.L192:
	leal	-1(%r12), %eax
	cmpl	$999999998, %eax
	ja	.L195
	movl	$1000000000, %eax
	subl	%r12d, %eax
	cmpl	%r13d, %eax
	jl	.L195
	movl	$-999999998, %eax
	subl	%r12d, %eax
	cmpl	%r13d, %eax
	jg	.L195
	cmpb	$1, %r8b
	movq	(%rbx), %rdx
	movq	%r15, %rsi
	sbbl	%eax, %eax
	notl	%eax
	movl	%r12d, (%rdx)
	andl	$-128, %eax
	movl	%r13d, 4(%rdx)
	movb	%al, 8(%rdx)
	movq	(%rbx), %rdi
	movl	%r12d, %edx
	call	uprv_decNumberSetBCD_67@PLT
	movl	84(%rbx), %eax
	testl	%eax, %eax
	je	.L188
	movl	$5, (%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movl	$16, (%r14)
.L188:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movslq	%edx, %rdi
	movl	%r8d, -56(%rbp)
	addq	$12, %rdi
	call	uprv_malloc_67@PLT
	movl	-56(%rbp), %r8d
	testq	%rax, %rax
	je	.L190
	movq	(%rbx), %rdi
	cmpb	$0, 12(%rbx)
	movq	(%rdi), %rdx
	movq	%rdx, (%rax)
	movl	8(%rdi), %edx
	movl	%edx, 8(%rax)
	jne	.L203
.L191:
	movq	%rax, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L190:
	movl	%r12d, 64(%rbx)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L203:
	movl	%r8d, -60(%rbp)
	movq	%rax, -56(%rbp)
	call	uprv_free_67@PLT
	movl	-60(%rbp), %r8d
	movq	-56(%rbp), %rax
	jmp	.L191
	.cfi_endproc
.LFE3687:
	.size	_ZN6icu_676number4impl6DecNum5setToEPKhiibR10UErrorCode, .-_ZN6icu_676number4impl6DecNum5setToEPKhiibR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6DecNum9normalizeEv
	.type	_ZN6icu_676number4impl6DecNum9normalizeEv, @function
_ZN6icu_676number4impl6DecNum9normalizeEv:
.LFB3688:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	leaq	64(%rdi), %rdx
	movq	%r8, %rsi
	movq	%r8, %rdi
	jmp	uprv_decNumberReduce_67@PLT
	.cfi_endproc
.LFE3688:
	.size	_ZN6icu_676number4impl6DecNum9normalizeEv, .-_ZN6icu_676number4impl6DecNum9normalizeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6DecNum10multiplyByERKS2_R10UErrorCode
	.type	_ZN6icu_676number4impl6DecNum10multiplyByERKS2_R10UErrorCode, @function
_ZN6icu_676number4impl6DecNum10multiplyByERKS2_R10UErrorCode:
.LFB3689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	(%rsi), %rdx
	leaq	64(%rbx), %rcx
	movq	%rdi, %rsi
	call	uprv_decNumberMultiply_67@PLT
	movl	84(%rbx), %eax
	testl	%eax, %eax
	je	.L205
	movl	$5, (%r12)
.L205:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3689:
	.size	_ZN6icu_676number4impl6DecNum10multiplyByERKS2_R10UErrorCode, .-_ZN6icu_676number4impl6DecNum10multiplyByERKS2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6DecNum8divideByERKS2_R10UErrorCode
	.type	_ZN6icu_676number4impl6DecNum8divideByERKS2_R10UErrorCode, @function
_ZN6icu_676number4impl6DecNum8divideByERKS2_R10UErrorCode:
.LFB3690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	(%rsi), %rdx
	leaq	64(%rbx), %rcx
	movq	%rdi, %rsi
	call	uprv_decNumberDivide_67@PLT
	movl	84(%rbx), %eax
	testb	$32, %al
	jne	.L208
	testl	%eax, %eax
	jne	.L217
.L208:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	popq	%rbx
	movl	$5, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3690:
	.size	_ZN6icu_676number4impl6DecNum8divideByERKS2_R10UErrorCode, .-_ZN6icu_676number4impl6DecNum8divideByERKS2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl6DecNum10isNegativeEv
	.type	_ZNK6icu_676number4impl6DecNum10isNegativeEv, @function
_ZNK6icu_676number4impl6DecNum10isNegativeEv:
.LFB3691:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movzbl	8(%rax), %eax
	shrb	$7, %al
	ret
	.cfi_endproc
.LFE3691:
	.size	_ZNK6icu_676number4impl6DecNum10isNegativeEv, .-_ZNK6icu_676number4impl6DecNum10isNegativeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl6DecNum6isZeroEv
	.type	_ZNK6icu_676number4impl6DecNum6isZeroEv, @function
_ZNK6icu_676number4impl6DecNum6isZeroEv:
.LFB3692:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	xorl	%r8d, %r8d
	cmpb	$0, 9(%rax)
	jne	.L219
	cmpl	$1, (%rax)
	je	.L223
.L219:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	testb	$112, 8(%rax)
	sete	%r8b
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3692:
	.size	_ZNK6icu_676number4impl6DecNum6isZeroEv, .-_ZNK6icu_676number4impl6DecNum6isZeroEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl6DecNum8toStringERNS_8ByteSinkER10UErrorCode
	.type	_ZNK6icu_676number4impl6DecNum8toStringERNS_8ByteSinkER10UErrorCode, @function
_ZNK6icu_676number4impl6DecNum8toStringERNS_8ByteSinkER10UErrorCode:
.LFB3693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L224
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	%rsi, %r12
	leaq	-83(%rbp), %rsi
	movl	(%rdi), %eax
	movq	%rsi, -96(%rbp)
	movl	$30, -88(%rbp)
	leal	14(%rax), %r13d
	movb	$0, -84(%rbp)
	cmpl	$30, %r13d
	jg	.L233
.L226:
	call	uprv_decNumberToString_67@PLT
	movq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	(%r12), %rax
	call	*16(%rax)
	cmpb	$0, -84(%rbp)
	jne	.L234
.L224:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L235
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	-96(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L233:
	movslq	%r13d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L236
	cmpb	$0, -84(%rbp)
	jne	.L237
.L228:
	movq	%rsi, -96(%rbp)
	movq	(%rbx), %rdi
	movl	%r13d, -88(%rbp)
	movb	$1, -84(%rbp)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L237:
	movq	-96(%rbp), %rdi
	movq	%rax, -104(%rbp)
	call	uprv_free_67@PLT
	movq	-104(%rbp), %rsi
	jmp	.L228
.L235:
	call	__stack_chk_fail@PLT
.L236:
	movq	(%rbx), %rdi
	movq	-96(%rbp), %rsi
	jmp	.L226
	.cfi_endproc
.LFE3693:
	.size	_ZNK6icu_676number4impl6DecNum8toStringERNS_8ByteSinkER10UErrorCode, .-_ZNK6icu_676number4impl6DecNum8toStringERNS_8ByteSinkER10UErrorCode
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC10:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC11:
	.long	4294967295
	.long	2146435071
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
