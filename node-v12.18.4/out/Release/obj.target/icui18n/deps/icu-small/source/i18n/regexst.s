	.file	"regexst.cpp"
	.text
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"a"
	.string	"c"
	.string	"e"
	.string	"f"
	.string	"n"
	.string	"r"
	.string	"t"
	.string	"u"
	.string	"U"
	.string	"x"
	.string	""
	.string	""
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC1:
	.string	"["
	.string	"\\"
	.string	"p"
	.string	"{"
	.string	"A"
	.string	"l"
	.string	"p"
	.string	"h"
	.string	"a"
	.string	"b"
	.string	"e"
	.string	"t"
	.string	"i"
	.string	"c"
	.string	"}"
	.string	"\\"
	.string	"p"
	.string	"{"
	.string	"M"
	.string	"}"
	.string	"\\"
	.string	"p"
	.string	"{"
	.string	"N"
	.string	"d"
	.string	"}"
	.string	"\\"
	.string	"p"
	.string	"{"
	.string	"P"
	.string	"c"
	.string	"}"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"0"
	.string	"0"
	.string	"c"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"0"
	.string	"0"
	.string	"d"
	.string	"]"
	.string	""
	.string	""
	.align 8
.LC2:
	.string	"["
	.string	"\\"
	.string	"p"
	.string	"{"
	.string	"W"
	.string	"h"
	.string	"i"
	.string	"t"
	.string	"e"
	.string	"S"
	.string	"p"
	.string	"a"
	.string	"c"
	.string	"e"
	.string	"}"
	.string	"]"
	.string	""
	.string	""
	.align 8
.LC3:
	.string	"["
	.string	"\\"
	.string	"p"
	.string	"{"
	.string	"G"
	.string	"r"
	.string	"a"
	.string	"p"
	.string	"h"
	.string	"e"
	.string	"m"
	.string	"e"
	.string	"_"
	.string	"E"
	.string	"x"
	.string	"t"
	.string	"e"
	.string	"n"
	.string	"d"
	.string	"}"
	.string	"]"
	.string	""
	.string	""
	.align 8
.LC4:
	.string	"["
	.string	"["
	.string	":"
	.string	"Z"
	.string	"l"
	.string	":"
	.string	"]"
	.string	"["
	.string	":"
	.string	"Z"
	.string	"p"
	.string	":"
	.string	"]"
	.string	"["
	.string	":"
	.string	"C"
	.string	"c"
	.string	":"
	.string	"]"
	.string	"["
	.string	":"
	.string	"C"
	.string	"f"
	.string	":"
	.string	"]"
	.string	"-"
	.string	"["
	.string	":"
	.string	"G"
	.string	"r"
	.string	"a"
	.string	"p"
	.string	"h"
	.string	"e"
	.string	"m"
	.string	"e"
	.string	"_"
	.string	"E"
	.string	"x"
	.string	"t"
	.string	"e"
	.string	"n"
	.string	"d"
	.string	":"
	.string	"]"
	.string	"]"
	.string	""
	.string	""
	.align 8
.LC5:
	.string	"["
	.string	"\\"
	.string	"p"
	.string	"{"
	.string	"H"
	.string	"a"
	.string	"n"
	.string	"g"
	.string	"u"
	.string	"l"
	.string	"_"
	.string	"S"
	.string	"y"
	.string	"l"
	.string	"l"
	.string	"a"
	.string	"b"
	.string	"l"
	.string	"e"
	.string	"_"
	.string	"T"
	.string	"y"
	.string	"p"
	.string	"e"
	.string	"="
	.string	"L"
	.string	"}"
	.string	"]"
	.string	""
	.string	""
	.align 8
.LC6:
	.string	"["
	.string	"\\"
	.string	"p"
	.string	"{"
	.string	"H"
	.string	"a"
	.string	"n"
	.string	"g"
	.string	"u"
	.string	"l"
	.string	"_"
	.string	"S"
	.string	"y"
	.string	"l"
	.string	"l"
	.string	"a"
	.string	"b"
	.string	"l"
	.string	"e"
	.string	"_"
	.string	"T"
	.string	"y"
	.string	"p"
	.string	"e"
	.string	"="
	.string	"V"
	.string	"}"
	.string	"]"
	.string	""
	.string	""
	.align 8
.LC7:
	.string	"["
	.string	"\\"
	.string	"p"
	.string	"{"
	.string	"H"
	.string	"a"
	.string	"n"
	.string	"g"
	.string	"u"
	.string	"l"
	.string	"_"
	.string	"S"
	.string	"y"
	.string	"l"
	.string	"l"
	.string	"a"
	.string	"b"
	.string	"l"
	.string	"e"
	.string	"_"
	.string	"T"
	.string	"y"
	.string	"p"
	.string	"e"
	.string	"="
	.string	"T"
	.string	"}"
	.string	"]"
	.string	""
	.string	""
	.align 8
.LC8:
	.string	"["
	.string	"\\"
	.string	"p"
	.string	"{"
	.string	"H"
	.string	"a"
	.string	"n"
	.string	"g"
	.string	"u"
	.string	"l"
	.string	"_"
	.string	"S"
	.string	"y"
	.string	"l"
	.string	"l"
	.string	"a"
	.string	"b"
	.string	"l"
	.string	"e"
	.string	"_"
	.string	"T"
	.string	"y"
	.string	"p"
	.string	"e"
	.string	"="
	.string	"L"
	.string	"V"
	.string	"}"
	.string	"]"
	.string	""
	.string	""
	.align 8
.LC9:
	.string	"["
	.string	"\\"
	.string	"p"
	.string	"{"
	.string	"H"
	.string	"a"
	.string	"n"
	.string	"g"
	.string	"u"
	.string	"l"
	.string	"_"
	.string	"S"
	.string	"y"
	.string	"l"
	.string	"l"
	.string	"a"
	.string	"b"
	.string	"l"
	.string	"e"
	.string	"_"
	.string	"T"
	.string	"y"
	.string	"p"
	.string	"e"
	.string	"="
	.string	"L"
	.string	"V"
	.string	"T"
	.string	"}"
	.string	"]"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC10:
	.string	"*"
	.string	"?"
	.string	"+"
	.string	"["
	.string	"("
	.string	")"
	.string	"{"
	.string	"}"
	.string	"^"
	.string	"$"
	.string	"|"
	.string	"\\"
	.string	"."
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RegexStaticSetsC2EP10UErrorCode
	.type	_ZN6icu_6715RegexStaticSetsC2EP10UErrorCode, @function
_ZN6icu_6715RegexStaticSetsC2EP10UErrorCode:
.LFB3058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-136(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	1008(%rbx), %r15
	leaq	2408(%rbx), %r13
	subq	$216, %rsp
	movq	%rsi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	8(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	208(%rbx), %r11
	movq	%r11, %rdi
	movq	%r11, -256(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	408(%rbx), %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	608(%rbx), %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	808(%rbx), %r10
	movq	%r10, %rdi
	movq	%r10, -248(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	1208(%rbx), %r9
	movq	%r9, %rdi
	movq	%r9, -240(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	1408(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	1608(%rbx), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -168(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	1808(%rbx), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -184(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	2008(%rbx), %r8
	movq	%r8, %rdi
	movq	%r8, -232(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	2208(%rbx), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -176(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	pxor	%xmm0, %xmm0
	leaq	3024(%rbx), %r8
	movq	%r8, %rdi
	movups	%xmm0, 2608(%rbx)
	movups	%xmm0, 2624(%rbx)
	movups	%xmm0, 2640(%rbx)
	movups	%xmm0, 2656(%rbx)
	movups	%xmm0, 2672(%rbx)
	movups	%xmm0, 2688(%rbx)
	movups	%xmm0, 2704(%rbx)
	movups	%xmm0, 2720(%rbx)
	movups	%xmm0, 2736(%rbx)
	movups	%xmm0, 2752(%rbx)
	movups	%xmm0, 2768(%rbx)
	movups	%xmm0, 2784(%rbx)
	movups	%xmm0, 2800(%rbx)
	movups	%xmm0, 2816(%rbx)
	movups	%xmm0, 2832(%rbx)
	movups	%xmm0, 2848(%rbx)
	movups	%xmm0, 2864(%rbx)
	movups	%xmm0, 2880(%rbx)
	movups	%xmm0, 2896(%rbx)
	movups	%xmm0, 2912(%rbx)
	movups	%xmm0, 2928(%rbx)
	movups	%xmm0, 2944(%rbx)
	movups	%xmm0, 2960(%rbx)
	movups	%xmm0, 2976(%rbx)
	movups	%xmm0, 2992(%rbx)
	movups	%xmm0, 3008(%rbx)
	movq	%r8, -192(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	3224(%rbx), %r9
	movq	%r9, %rdi
	movq	%r9, -200(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	3424(%rbx), %r10
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	3624(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %r11
	movl	$-1, %ecx
	movl	$1, %esi
	movups	%xmm0, 3824(%rbx)
	movq	%r11, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-224(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKNS_13UnicodeStringE@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC1(%rip), %r11
	movq	%r11, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-256(%rbp), %r11
	movq	-152(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC2(%rip), %rsi
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	movl	$1, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-248(%rbp), %r10
	movq	-152(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC3(%rip), %rsi
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	movl	$1, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-240(%rbp), %r9
	movq	-152(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC4(%rip), %rsi
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	movl	$1, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-152(%rbp), %rdx
	movq	-160(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC5(%rip), %rax
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-152(%rbp), %rdx
	movq	-168(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC6(%rip), %rax
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-152(%rbp), %rdx
	movq	-176(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC7(%rip), %rax
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-152(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC8(%rip), %rax
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-152(%rbp), %rdx
	movq	-184(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC9(%rip), %rax
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-232(%rbp), %r8
	movq	-152(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	movl	$55204, %edx
	movl	$44032, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet6removeEii@PLT
	movq	-160(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	movq	-168(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	movq	-176(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	-216(%rbp), %r13
	movq	%r12, -160(%rbp)
	movq	%r13, %r12
	.p2align 4,,10
	.p2align 3
.L4:
	movslq	%r14d, %r13
	xorl	%r15d, %r15d
	salq	$5, %r13
	addq	%rbx, %r13
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L2
	movl	%r15d, %eax
	movl	%r15d, %ecx
	movl	$1, %edx
	sarl	$3, %eax
	andl	$7, %ecx
	cltq
	sall	%cl, %edx
	orb	%dl, 2608(%rax,%r13)
.L2:
	addl	$1, %r15d
	cmpl	$256, %r15d
	jne	.L3
	addl	$1, %r14d
	addq	$200, %r12
	cmpl	$13, %r14d
	jne	.L4
	movq	-160(%rbp), %r12
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-208(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKNS_13UnicodeStringE@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$57, %edx
	movl	$48, %esi
	movq	-192(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movl	$90, %edx
	movl	$65, %esi
	movq	-200(%rbp), %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$122, %edx
	movl	$97, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r14, 3824(%rbx)
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	-152(%rbp), %rcx
	xorl	%edi, %edi
	call	utext_openUChars_67@PLT
	movq	%rax, 3832(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L13
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L13:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3058:
	.size	_ZN6icu_6715RegexStaticSetsC2EP10UErrorCode, .-_ZN6icu_6715RegexStaticSetsC2EP10UErrorCode
	.globl	_ZN6icu_6715RegexStaticSetsC1EP10UErrorCode
	.set	_ZN6icu_6715RegexStaticSetsC1EP10UErrorCode,_ZN6icu_6715RegexStaticSetsC2EP10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RegexStaticSetsD2Ev
	.type	_ZN6icu_6715RegexStaticSetsD2Ev, @function
_ZN6icu_6715RegexStaticSetsD2Ev:
.LFB3061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	2408(%r12), %rbx
	movq	$0, 3824(%rdi)
	movq	3832(%rdi), %rdi
	call	utext_close_67@PLT
	leaq	3624(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	3424(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	3224(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	3024(%r12), %rdi
	subq	$192, %r12
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rbx, %rdi
	subq	$200, %rbx
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	cmpq	%rbx, %r12
	jne	.L15
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3061:
	.size	_ZN6icu_6715RegexStaticSetsD2Ev, .-_ZN6icu_6715RegexStaticSetsD2Ev
	.globl	_ZN6icu_6715RegexStaticSetsD1Ev
	.set	_ZN6icu_6715RegexStaticSetsD1Ev,_ZN6icu_6715RegexStaticSetsD2Ev
	.p2align 4
	.type	regex_cleanup, @function
regex_cleanup:
.LFB3063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %r12
	testq	%r12, %r12
	je	.L19
	movq	3832(%r12), %rdi
	leaq	2408(%r12), %rbx
	movq	$0, 3824(%r12)
	leaq	-192(%r12), %r13
	call	utext_close_67@PLT
	leaq	3624(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	3424(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	3224(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	3024(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%rbx, %rdi
	subq	$200, %rbx
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	cmpq	%rbx, %r13
	jne	.L20
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L19:
	movq	$0, _ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip)
	movl	$1, %eax
	movl	$0, _ZN6icu_6719gStaticSetsInitOnceE(%rip)
	mfence
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3063:
	.size	regex_cleanup, .-regex_cleanup
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RegexStaticSets11initGlobalsEP10UErrorCode
	.type	_ZN6icu_6715RegexStaticSets11initGlobalsEP10UErrorCode, @function
_ZN6icu_6715RegexStaticSets11initGlobalsEP10UErrorCode:
.LFB3065:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L47
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	_ZN6icu_6719gStaticSetsInitOnceE(%rip), %eax
	movq	%rdi, %rbx
	cmpl	$2, %eax
	jne	.L48
.L29:
	movl	4+_ZN6icu_6719gStaticSetsInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L26
	movl	%eax, (%rbx)
.L26:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	leaq	_ZN6icu_6719gStaticSetsInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L29
	leaq	regex_cleanup(%rip), %rsi
	movl	$6, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movl	$3840, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L30
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6715RegexStaticSetsC1EP10UErrorCode
	movl	(%rbx), %eax
	movq	%r13, _ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip)
	testl	%eax, %eax
	jle	.L31
	movq	$0, 3824(%r13)
	movq	3832(%r13), %rdi
	leaq	2608(%r13), %r12
	leaq	8(%r13), %r14
	call	utext_close_67@PLT
	leaq	3624(%r13), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	3424(%r13), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	3224(%r13), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	3024(%r13), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	subq	$200, %r12
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	cmpq	%r14, %r12
	jne	.L32
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L34:
	movq	$0, _ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L35
.L31:
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	leaq	_ZN6icu_6719gStaticSetsInitOnceE(%rip), %rdi
	popq	%r12
	.cfi_restore 12
	movl	%eax, 4+_ZN6icu_6719gStaticSetsInitOnceE(%rip)
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L30:
	.cfi_restore_state
	movq	$0, _ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip)
	cmpl	$0, (%rbx)
	jg	.L34
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$7, (%rbx)
	movl	$7, %eax
	jmp	.L31
	.cfi_endproc
.LFE3065:
	.size	_ZN6icu_6715RegexStaticSets11initGlobalsEP10UErrorCode, .-_ZN6icu_6715RegexStaticSets11initGlobalsEP10UErrorCode
	.globl	_ZN6icu_6719gStaticSetsInitOnceE
	.bss
	.align 8
	.type	_ZN6icu_6719gStaticSetsInitOnceE, @object
	.size	_ZN6icu_6719gStaticSetsInitOnceE, 8
_ZN6icu_6719gStaticSetsInitOnceE:
	.zero	8
	.globl	_ZN6icu_6715RegexStaticSets11gStaticSetsE
	.align 8
	.type	_ZN6icu_6715RegexStaticSets11gStaticSetsE, @object
	.size	_ZN6icu_6715RegexStaticSets11gStaticSetsE, 8
_ZN6icu_6715RegexStaticSets11gStaticSetsE:
	.zero	8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
