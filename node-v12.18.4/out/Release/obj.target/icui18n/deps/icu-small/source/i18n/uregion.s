	.file	"uregion.cpp"
	.text
	.p2align 4
	.globl	uregion_getRegionFromCode_67
	.type	uregion_getRegionFromCode_67, @function
uregion_getRegionFromCode_67:
.LFB2249:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_676Region11getInstanceEPKcR10UErrorCode@PLT
	.cfi_endproc
.LFE2249:
	.size	uregion_getRegionFromCode_67, .-uregion_getRegionFromCode_67
	.p2align 4
	.globl	uregion_getRegionFromNumericCode_67
	.type	uregion_getRegionFromNumericCode_67, @function
uregion_getRegionFromNumericCode_67:
.LFB2250:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_676Region11getInstanceEiR10UErrorCode@PLT
	.cfi_endproc
.LFE2250:
	.size	uregion_getRegionFromNumericCode_67, .-uregion_getRegionFromNumericCode_67
	.p2align 4
	.globl	uregion_getAvailable_67
	.type	uregion_getAvailable_67, @function
uregion_getAvailable_67:
.LFB2251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	_ZN6icu_676Region12getAvailableE11URegionTypeR10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uenum_openFromStringEnumeration_67@PLT
	.cfi_endproc
.LFE2251:
	.size	uregion_getAvailable_67, .-uregion_getAvailable_67
	.p2align 4
	.globl	uregion_areEqual_67
	.type	uregion_areEqual_67, @function
uregion_areEqual_67:
.LFB2252:
	.cfi_startproc
	endbr64
	cmpq	%rdi, %rsi
	sete	%al
	ret
	.cfi_endproc
.LFE2252:
	.size	uregion_areEqual_67, .-uregion_areEqual_67
	.p2align 4
	.globl	uregion_getContainingRegion_67
	.type	uregion_getContainingRegion_67, @function
uregion_getContainingRegion_67:
.LFB2253:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_676Region19getContainingRegionEv@PLT
	.cfi_endproc
.LFE2253:
	.size	uregion_getContainingRegion_67, .-uregion_getContainingRegion_67
	.p2align 4
	.globl	uregion_getContainingRegionOfType_67
	.type	uregion_getContainingRegionOfType_67, @function
uregion_getContainingRegionOfType_67:
.LFB2254:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_676Region19getContainingRegionE11URegionType@PLT
	.cfi_endproc
.LFE2254:
	.size	uregion_getContainingRegionOfType_67, .-uregion_getContainingRegionOfType_67
	.p2align 4
	.globl	uregion_getContainedRegions_67
	.type	uregion_getContainedRegions_67, @function
uregion_getContainedRegions_67:
.LFB2255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	_ZNK6icu_676Region19getContainedRegionsER10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uenum_openFromStringEnumeration_67@PLT
	.cfi_endproc
.LFE2255:
	.size	uregion_getContainedRegions_67, .-uregion_getContainedRegions_67
	.p2align 4
	.globl	uregion_getContainedRegionsOfType_67
	.type	uregion_getContainedRegionsOfType_67, @function
uregion_getContainedRegionsOfType_67:
.LFB2256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	call	_ZNK6icu_676Region19getContainedRegionsE11URegionTypeR10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uenum_openFromStringEnumeration_67@PLT
	.cfi_endproc
.LFE2256:
	.size	uregion_getContainedRegionsOfType_67, .-uregion_getContainedRegionsOfType_67
	.p2align 4
	.globl	uregion_contains_67
	.type	uregion_contains_67, @function
uregion_contains_67:
.LFB2257:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_676Region8containsERKS0_@PLT
	.cfi_endproc
.LFE2257:
	.size	uregion_contains_67, .-uregion_contains_67
	.p2align 4
	.globl	uregion_getPreferredValues_67
	.type	uregion_getPreferredValues_67, @function
uregion_getPreferredValues_67:
.LFB2258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	_ZNK6icu_676Region18getPreferredValuesER10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uenum_openFromStringEnumeration_67@PLT
	.cfi_endproc
.LFE2258:
	.size	uregion_getPreferredValues_67, .-uregion_getPreferredValues_67
	.p2align 4
	.globl	uregion_getRegionCode_67
	.type	uregion_getRegionCode_67, @function
uregion_getRegionCode_67:
.LFB2259:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_676Region13getRegionCodeEv@PLT
	.cfi_endproc
.LFE2259:
	.size	uregion_getRegionCode_67, .-uregion_getRegionCode_67
	.p2align 4
	.globl	uregion_getNumericCode_67
	.type	uregion_getNumericCode_67, @function
uregion_getNumericCode_67:
.LFB2260:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_676Region14getNumericCodeEv@PLT
	.cfi_endproc
.LFE2260:
	.size	uregion_getNumericCode_67, .-uregion_getNumericCode_67
	.p2align 4
	.globl	uregion_getType_67
	.type	uregion_getType_67, @function
uregion_getType_67:
.LFB2261:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_676Region7getTypeEv@PLT
	.cfi_endproc
.LFE2261:
	.size	uregion_getType_67, .-uregion_getType_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
