	.file	"ulocdata.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"supplementalData"
.LC1:
	.string	"measurementData"
.LC2:
	.string	"001"
	.text
	.p2align 4
	.type	_ZL30measurementTypeBundleForLocalePKcS0_P10UErrorCode, @function
_ZL30measurementTypeBundleForLocalePKcS0_P10UErrorCode:
.LFB2449:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movl	$4, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$1, %esi
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-60(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movq	%r14, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ulocimp_getRegionForSupplementalData_67@PLT
	movq	%r12, %rdx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	ures_openDirect_67@PLT
	movq	%r12, %rcx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdx
	movq	%rax, %rdi
	call	ures_getByKey_67@PLT
	testq	%r13, %r13
	je	.L2
	movq	%r14, %rsi
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	ures_getByKey_67@PLT
	movq	%rax, %r14
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L15
.L3:
	cmpl	$2, %eax
	je	.L16
.L4:
	movq	%r14, %rdi
	call	ures_close_67@PLT
.L2:
	movq	%r13, %rdi
	call	ures_close_67@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movl	$0, (%r12)
	testq	%r14, %r14
	je	.L5
	movq	%r14, %rdi
	call	ures_close_67@PLT
.L5:
	movq	%r12, %rcx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	ures_getByKey_67@PLT
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	ures_getByKey_67@PLT
	movq	%rax, %rbx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	ures_getByKey_67@PLT
	movq	%rax, %rbx
	movl	(%r12), %eax
	jmp	.L3
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2449:
	.size	_ZL30measurementTypeBundleForLocalePKcS0_P10UErrorCode, .-_ZL30measurementTypeBundleForLocalePKcS0_P10UErrorCode
	.section	.rodata.str1.1
.LC3:
	.string	"icudt67l-lang"
	.text
	.p2align 4
	.globl	ulocdata_open_67
	.type	ulocdata_open_67, @function
ulocdata_open_67:
.LFB2443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L18
	movq	%rdi, %r13
	movl	$24, %edi
	movq	%rsi, %rbx
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L23
	movq	$0, 16(%rax)
	movq	%rbx, %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	movb	$0, (%rax)
	call	ures_open_67@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	leaq	.LC3(%rip), %rdi
	movq	%rax, 8(%r12)
	call	ures_open_67@PLT
	movq	%rax, 16(%r12)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L24
.L18:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	jmp	.L18
.L23:
	movl	$7, (%rbx)
	jmp	.L18
	.cfi_endproc
.LFE2443:
	.size	ulocdata_open_67, .-ulocdata_open_67
	.p2align 4
	.globl	ulocdata_close_67
	.type	ulocdata_close_67, @function
ulocdata_close_67:
.LFB2444:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L25
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	ures_close_67@PLT
	movq	8(%r12), %rdi
	call	ures_close_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	ret
	.cfi_endproc
.LFE2444:
	.size	ulocdata_close_67, .-ulocdata_close_67
	.p2align 4
	.globl	ulocdata_setNoSubstitute_67
	.type	ulocdata_setNoSubstitute_67, @function
ulocdata_setNoSubstitute_67:
.LFB2445:
	.cfi_startproc
	endbr64
	movb	%sil, (%rdi)
	ret
	.cfi_endproc
.LFE2445:
	.size	ulocdata_setNoSubstitute_67, .-ulocdata_setNoSubstitute_67
	.p2align 4
	.globl	ulocdata_getNoSubstitute_67
	.type	ulocdata_getNoSubstitute_67, @function
ulocdata_getNoSubstitute_67:
.LFB2446:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE2446:
	.size	ulocdata_getNoSubstitute_67, .-ulocdata_getNoSubstitute_67
	.p2align 4
	.globl	ulocdata_getExemplarSet_67
	.type	ulocdata_getExemplarSet_67, @function
ulocdata_getExemplarSet_67:
.LFB2447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	movl	$0, -48(%rbp)
	movl	$0, -44(%rbp)
	testl	%eax, %eax
	jg	.L37
	movslq	%ecx, %rcx
	leaq	_ZZ26ulocdata_getExemplarSet_67E16exemplarSetTypes(%rip), %rax
	movq	%rdi, %r14
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	(%rax,%rcx,8), %rsi
	movq	%r8, %r12
	leaq	-44(%rbp), %r8
	movl	%edx, %r13d
	movq	%r8, %rcx
	leaq	-48(%rbp), %rdx
	call	ures_getStringByKey_67@PLT
	movl	-44(%rbp), %edx
	cmpl	$-127, %edx
	je	.L44
	testl	%edx, %edx
	jne	.L38
	movl	(%r12), %edx
.L39:
	testl	%edx, %edx
	jg	.L37
	movl	-48(%rbp), %r9d
	orl	$1, %r13d
	testq	%rbx, %rbx
	je	.L40
.L46:
	movq	%r12, %r8
	movl	%r13d, %ecx
	movl	%r9d, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	uset_applyPattern_67@PLT
	movq	%rbx, %rax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L44:
	cmpb	$0, (%r14)
	je	.L36
	movl	$2, (%r12)
.L37:
	xorl	%eax, %eax
.L32:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L45
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movl	$-127, (%r12)
	movl	-48(%rbp), %r9d
	orl	$1, %r13d
	testq	%rbx, %rbx
	jne	.L46
.L40:
	movq	%r12, %rcx
	movl	%r13d, %edx
	movl	%r9d, %esi
	movq	%rax, %rdi
	call	uset_openPatternOptions_67@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L38:
	movl	%edx, (%r12)
	jmp	.L39
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2447:
	.size	ulocdata_getExemplarSet_67, .-ulocdata_getExemplarSet_67
	.section	.rodata.str1.1
.LC4:
	.string	"delimiters"
	.text
	.p2align 4
	.globl	ulocdata_getDelimiter_67
	.type	ulocdata_getDelimiter_67, @function
ulocdata_getDelimiter_67:
.LFB2448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	movl	$0, -64(%rbp)
	movl	$0, -60(%rbp)
	testl	%eax, %eax
	jle	.L48
.L64:
	xorl	%eax, %eax
.L47:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L65
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	8(%rdi), %rdi
	movl	%esi, %r15d
	movq	%rdx, %r13
	movl	%ecx, %r14d
	xorl	%edx, %edx
	leaq	-60(%rbp), %rcx
	movq	%r8, %rbx
	leaq	.LC4(%rip), %rsi
	movq	%rcx, -72(%rbp)
	call	ures_getByKey_67@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %rdi
	movl	-60(%rbp), %eax
	cmpl	$-127, %eax
	je	.L66
	testl	%eax, %eax
	jne	.L53
	movl	(%rbx), %eax
.L54:
	testl	%eax, %eax
	jg	.L52
.L55:
	movslq	%r15d, %rsi
	leaq	_ZZ24ulocdata_getDelimiter_67E13delimiterKeys(%rip), %rax
	leaq	-64(%rbp), %rdx
	movq	%rdi, -72(%rbp)
	movq	(%rax,%rsi,8), %rsi
	call	ures_getStringByKey_67@PLT
	movq	-72(%rbp), %rdi
	movq	%rax, %r15
	call	ures_close_67@PLT
	movl	-60(%rbp), %eax
	cmpl	$-127, %eax
	je	.L67
	testl	%eax, %eax
	jne	.L59
	movl	(%rbx), %eax
.L60:
	testl	%eax, %eax
	jg	.L64
.L61:
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	u_strncpy_67@PLT
	movl	-64(%rbp), %eax
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L53:
	movl	%eax, (%rbx)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L66:
	cmpb	$0, (%r12)
	je	.L51
	movl	$2, -60(%rbp)
	movl	$2, (%rbx)
.L52:
	call	ures_close_67@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L67:
	cmpb	$0, (%r12)
	je	.L57
	movl	$2, (%rbx)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$-127, (%rbx)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$-127, (%rbx)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L59:
	movl	%eax, (%rbx)
	jmp	.L60
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2448:
	.size	ulocdata_getDelimiter_67, .-ulocdata_getDelimiter_67
	.section	.rodata.str1.1
.LC5:
	.string	"MeasurementSystem"
	.text
	.p2align 4
	.globl	ulocdata_getMeasurementSystem_67
	.type	ulocdata_getMeasurementSystem_67, @function
ulocdata_getMeasurementSystem_67:
.LFB2450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	testq	%rsi, %rsi
	je	.L70
	movl	(%rsi), %eax
	movq	%rsi, %r12
	movl	$3, %r13d
	testl	%eax, %eax
	jg	.L68
	movq	%rsi, %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZL30measurementTypeBundleForLocalePKcS0_P10UErrorCode
	movq	%r12, %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	ures_getInt_67@PLT
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	ures_close_67@PLT
.L68:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$3, %r13d
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2450:
	.size	ulocdata_getMeasurementSystem_67, .-ulocdata_getMeasurementSystem_67
	.section	.rodata.str1.1
.LC6:
	.string	"PaperSize"
	.text
	.p2align 4
	.globl	ulocdata_getPaperSize_67
	.type	ulocdata_getPaperSize_67, @function
ulocdata_getPaperSize_67:
.LFB2451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	testq	%rcx, %rcx
	je	.L73
	movq	%rcx, %rbx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jle	.L84
.L73:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	%rsi, %r13
	movq	%rdx, %r12
	leaq	.LC6(%rip), %rsi
	movq	%rbx, %rdx
	call	_ZL30measurementTypeBundleForLocalePKcS0_P10UErrorCode
	movq	%rbx, %rdx
	leaq	-44(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	ures_getIntVector_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L86
.L77:
	movq	%r14, %rdi
	call	ures_close_67@PLT
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L86:
	cmpl	$1, -44(%rbp)
	jle	.L87
	movl	(%rax), %edx
	movl	%edx, 0(%r13)
	movl	4(%rax), %eax
	movl	%eax, (%r12)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$5, (%rbx)
	jmp	.L77
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2451:
	.size	ulocdata_getPaperSize_67, .-ulocdata_getPaperSize_67
	.section	.rodata.str1.1
.LC7:
	.string	"cldrVersion"
	.text
	.p2align 4
	.globl	ulocdata_getCLDRVersion_67
	.type	ulocdata_getCLDRVersion_67, @function
ulocdata_getCLDRVersion_67:
.LFB2452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	xorl	%edi, %edi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	ures_openDirect_67@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ures_getVersionByKey_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ures_close_67@PLT
	.cfi_endproc
.LFE2452:
	.size	ulocdata_getCLDRVersion_67, .-ulocdata_getCLDRVersion_67
	.section	.rodata.str1.1
.LC8:
	.string	"localeDisplayPattern"
.LC9:
	.string	"pattern"
	.text
	.p2align 4
	.globl	ulocdata_getLocaleDisplayPattern_67
	.type	ulocdata_getLocaleDisplayPattern_67, @function
ulocdata_getLocaleDisplayPattern_67:
.LFB2453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	movl	$0, -64(%rbp)
	movl	$0, -60(%rbp)
	testl	%eax, %eax
	jle	.L91
.L107:
	xorl	%eax, %eax
.L90:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L108
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	%rdi, %r12
	leaq	-60(%rbp), %r15
	movq	16(%rdi), %rdi
	movq	%rsi, %r13
	movl	%edx, %r14d
	movq	%rcx, %rbx
	xorl	%edx, %edx
	movq	%r15, %rcx
	leaq	.LC8(%rip), %rsi
	call	ures_getByKey_67@PLT
	movq	%rax, %rdi
	movl	-60(%rbp), %eax
	cmpl	$-127, %eax
	je	.L109
	testl	%eax, %eax
	jne	.L96
	movl	(%rbx), %eax
.L97:
	testl	%eax, %eax
	jg	.L95
.L98:
	movq	%r15, %rcx
	leaq	-64(%rbp), %rdx
	leaq	.LC9(%rip), %rsi
	movq	%rdi, -72(%rbp)
	call	ures_getStringByKey_67@PLT
	movq	-72(%rbp), %rdi
	movq	%rax, %r15
	call	ures_close_67@PLT
	movl	-60(%rbp), %eax
	cmpl	$-127, %eax
	je	.L110
	testl	%eax, %eax
	jne	.L102
	movl	(%rbx), %eax
.L103:
	testl	%eax, %eax
	jg	.L107
.L104:
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	u_strncpy_67@PLT
	movl	-64(%rbp), %eax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L96:
	movl	%eax, (%rbx)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L109:
	cmpb	$0, (%r12)
	je	.L94
	movl	$2, -60(%rbp)
	movl	$2, (%rbx)
.L95:
	call	ures_close_67@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L110:
	cmpb	$0, (%r12)
	je	.L100
	movl	$2, (%rbx)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$-127, (%rbx)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$-127, (%rbx)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L102:
	movl	%eax, (%rbx)
	jmp	.L103
.L108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2453:
	.size	ulocdata_getLocaleDisplayPattern_67, .-ulocdata_getLocaleDisplayPattern_67
	.section	.rodata.str1.1
.LC10:
	.string	"separator"
	.text
	.p2align 4
	.globl	ulocdata_getLocaleSeparator_67
	.type	ulocdata_getLocaleSeparator_67, @function
ulocdata_getLocaleSeparator_67:
.LFB2454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -64(%rbp)
	movl	$0, -60(%rbp)
	testl	%r8d, %r8d
	jle	.L112
.L135:
	xorl	%eax, %eax
.L111:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L136
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	%rdi, %r13
	leaq	-60(%rbp), %r15
	movq	16(%rdi), %rdi
	movq	%rsi, %r12
	movl	%edx, %r14d
	movq	%rcx, %rbx
	xorl	%edx, %edx
	movq	%r15, %rcx
	leaq	.LC8(%rip), %rsi
	call	ures_getByKey_67@PLT
	movq	%rax, %rdi
	movl	-60(%rbp), %eax
	cmpl	$-127, %eax
	je	.L137
	testl	%eax, %eax
	jne	.L117
	movl	(%rbx), %eax
.L118:
	testl	%eax, %eax
	jg	.L116
.L119:
	movq	%r15, %rcx
	leaq	-64(%rbp), %rdx
	leaq	.LC10(%rip), %rsi
	movq	%rdi, -72(%rbp)
	call	ures_getStringByKey_67@PLT
	movq	-72(%rbp), %rdi
	movq	%rax, %r15
	call	ures_close_67@PLT
	movl	-60(%rbp), %eax
	cmpl	$-127, %eax
	je	.L138
	testl	%eax, %eax
	jne	.L123
	movl	(%rbx), %eax
.L124:
	testl	%eax, %eax
	jg	.L135
.L126:
	leaq	_ZZ30ulocdata_getLocaleSeparator_67E4sub0(%rip), %rsi
	movq	%r15, %rdi
	call	u_strstr_67@PLT
	leaq	_ZZ30ulocdata_getLocaleSeparator_67E4sub1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	u_strstr_67@PLT
	testq	%rbx, %rbx
	setne	%cl
	testq	%rax, %rax
	setne	%dl
	testb	%dl, %cl
	je	.L125
	cmpq	%rax, %rbx
	ja	.L125
	leaq	6(%rbx), %r15
	subq	%r15, %rax
	sarq	%rax
	movl	%eax, -64(%rbp)
	cmpl	%r14d, %eax
	jge	.L125
	movl	%eax, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	u_strncpy_67@PLT
	movslq	-64(%rbp), %rdx
	xorl	%ecx, %ecx
	movw	%cx, (%r12,%rdx,2)
	movq	%rdx, %rax
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L117:
	movl	%eax, (%rbx)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L125:
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	u_strncpy_67@PLT
	movl	-64(%rbp), %eax
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L137:
	cmpb	$0, 0(%r13)
	je	.L115
	movl	$2, -60(%rbp)
	movl	$2, (%rbx)
.L116:
	call	ures_close_67@PLT
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L138:
	cmpb	$0, 0(%r13)
	je	.L121
	movl	$2, (%rbx)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L115:
	movl	$-127, (%rbx)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$-127, (%rbx)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L123:
	movl	%eax, (%rbx)
	jmp	.L124
.L136:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2454:
	.size	ulocdata_getLocaleSeparator_67, .-ulocdata_getLocaleSeparator_67
	.section	.rodata
	.align 8
	.type	_ZZ30ulocdata_getLocaleSeparator_67E4sub1, @object
	.size	_ZZ30ulocdata_getLocaleSeparator_67E4sub1, 8
_ZZ30ulocdata_getLocaleSeparator_67E4sub1:
	.value	123
	.value	49
	.value	125
	.zero	2
	.align 8
	.type	_ZZ30ulocdata_getLocaleSeparator_67E4sub0, @object
	.size	_ZZ30ulocdata_getLocaleSeparator_67E4sub0, 8
_ZZ30ulocdata_getLocaleSeparator_67E4sub0:
	.value	123
	.value	48
	.value	125
	.zero	2
	.section	.rodata.str1.1
.LC11:
	.string	"quotationStart"
.LC12:
	.string	"quotationEnd"
.LC13:
	.string	"alternateQuotationStart"
.LC14:
	.string	"alternateQuotationEnd"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZZ24ulocdata_getDelimiter_67E13delimiterKeys, @object
	.size	_ZZ24ulocdata_getDelimiter_67E13delimiterKeys, 32
_ZZ24ulocdata_getDelimiter_67E13delimiterKeys:
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.section	.rodata.str1.1
.LC15:
	.string	"ExemplarCharacters"
.LC16:
	.string	"AuxExemplarCharacters"
.LC17:
	.string	"ExemplarCharactersIndex"
.LC18:
	.string	"ExemplarCharactersPunctuation"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZZ26ulocdata_getExemplarSet_67E16exemplarSetTypes, @object
	.size	_ZZ26ulocdata_getExemplarSet_67E16exemplarSetTypes, 32
_ZZ26ulocdata_getExemplarSet_67E16exemplarSetTypes:
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
