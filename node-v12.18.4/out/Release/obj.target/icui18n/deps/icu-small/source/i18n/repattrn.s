	.file	"repattrn.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexPattern5flagsEv
	.type	_ZNK6icu_6712RegexPattern5flagsEv, @function
_ZNK6icu_6712RegexPattern5flagsEv:
.LFB3168:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE3168:
	.size	_ZNK6icu_6712RegexPattern5flagsEv, .-_ZNK6icu_6712RegexPattern5flagsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexPattern17getDynamicClassIDEv
	.type	_ZNK6icu_6712RegexPattern17getDynamicClassIDEv, @function
_ZNK6icu_6712RegexPattern17getDynamicClassIDEv:
.LFB3182:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712RegexPattern16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3182:
	.size	_ZNK6icu_6712RegexPattern17getDynamicClassIDEv, .-_ZNK6icu_6712RegexPattern17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexPattern7patternEv
	.type	_ZNK6icu_6712RegexPattern7patternEv, @function
_ZNK6icu_6712RegexPattern7patternEv:
.LFB3173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	16(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L5
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L4:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L10
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L11
	movl	$0, -132(%rbp)
	leaq	-128(%rbp), %r14
	call	utext_nativeLength_67@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	-132(%rbp), %r9
	movq	%rax, %rdx
	xorl	%esi, %esi
	movq	%rax, %r15
	movq	%r9, -152(%rbp)
	call	utext_extract_67@PLT
	movq	%r14, %rdi
	movl	$0, -132(%rbp)
	movl	%eax, %r13d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movl	%r13d, %esi
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	8(%rbx), %rdi
	movl	%r13d, %r8d
	movq	%r15, %rdx
	movq	-152(%rbp), %r9
	movq	%rax, %rcx
	xorl	%esi, %esi
	call	utext_extract_67@PLT
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movq	%rax, (%r12)
	movw	%dx, 8(%r12)
	jmp	.L4
.L10:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3173:
	.size	_ZNK6icu_6712RegexPattern7patternEv, .-_ZNK6icu_6712RegexPattern7patternEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexPattern11patternTextER10UErrorCode
	.type	_ZNK6icu_6712RegexPattern11patternTextER10UErrorCode, @function
_ZNK6icu_6712RegexPattern11patternTextER10UErrorCode:
.LFB3174:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jg	.L16
	movq	8(%rdi), %r8
	movl	$0, (%rsi)
	testq	%r8, %r8
	je	.L19
.L16:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6715RegexStaticSets11initGlobalsEP10UErrorCode@PLT
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	3832(%rax), %r8
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE3174:
	.size	_ZNK6icu_6712RegexPattern11patternTextER10UErrorCode, .-_ZNK6icu_6712RegexPattern11patternTextER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexPattern19groupNumberFromNameEPKciR10UErrorCode
	.type	_ZNK6icu_6712RegexPattern19groupNumberFromNameEPKciR10UErrorCode, @function
_ZNK6icu_6712RegexPattern19groupNumberFromNameEPKciR10UErrorCode:
.LFB3176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L32
.L20:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$80, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	leaq	-112(%rbp), %r14
	movq	%rcx, %rbx
	movq	%rdi, %r12
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L22
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.L24
	movq	%r14, %rsi
	call	uhash_geti_67@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L22
.L24:
	movl	$66325, (%rbx)
	xorl	%r13d, %r13d
.L22:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L20
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3176:
	.size	_ZNK6icu_6712RegexPattern19groupNumberFromNameEPKciR10UErrorCode, .-_ZNK6icu_6712RegexPattern19groupNumberFromNameEPKciR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexPatternaSERKS0_.part.0, @function
_ZN6icu_6712RegexPatternaSERKS0_.part.0:
.LFB4141:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 16(%rsi)
	je	.L67
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L37
	movq	16(%r12), %rsi
	movq	%rax, %rdi
	leaq	120(%rbx), %r14
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r13, 16(%rbx)
	movq	%r14, %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	call	utext_openConstUnicodeString_67@PLT
	movq	%rax, 8(%rbx)
.L36:
	movl	120(%rbx), %esi
	testl	%esi, %esi
	jle	.L68
.L34:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	24(%r12), %eax
	leaq	40(%r12), %rsi
	leaq	40(%rbx), %rdi
	movl	%eax, 24(%rbx)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	124(%r12), %eax
	movq	160(%r12), %rsi
	movq	160(%rbx), %rdi
	movl	%eax, 124(%rbx)
	movl	128(%r12), %eax
	movl	%eax, 128(%rbx)
	movl	132(%r12), %eax
	movl	%eax, 132(%rbx)
	movl	144(%r12), %eax
	movl	%eax, 144(%rbx)
	movl	148(%r12), %eax
	movl	%eax, 148(%rbx)
	movl	152(%r12), %eax
	movl	%eax, 152(%rbx)
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movq	176(%r12), %rdx
	movl	168(%r12), %eax
	movl	%eax, 168(%rbx)
	movq	176(%rbx), %rax
	movdqu	(%rdx), %xmm3
	movups	%xmm3, (%rax)
	movdqu	16(%rdx), %xmm4
	movq	%r14, %rdx
	movups	%xmm4, 16(%rax)
	movq	32(%rbx), %rdi
	movzbl	184(%r12), %eax
	movb	%al, 184(%rbx)
	movq	32(%r12), %rsi
	call	_ZN6icu_679UVector646assignERKS0_R10UErrorCode@PLT
	movq	136(%rbx), %rdi
	movq	136(%r12), %rsi
	movq	%r14, %rdx
	call	_ZN6icu_679UVector326assignERKS0_R10UErrorCode@PLT
	movq	104(%r12), %rax
	movslq	8(%rax), %r13
	movabsq	$288230376151711743, %rax
	movq	%r13, %rdi
	movl	%r13d, -80(%rbp)
	salq	$5, %rdi
	cmpq	%rax, %r13
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_ZN6icu_677UMemorynaEm@PLT
	testq	%rax, %rax
	je	.L41
	movq	%r13, %rcx
	subq	$1, %rcx
	js	.L42
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L43:
	pxor	%xmm0, %xmm0
	subq	$1, %rcx
	addq	$32, %rdx
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	$-1, %rcx
	jne	.L43
.L42:
	cmpl	$1, -80(%rbp)
	movq	%rax, 112(%rbx)
	jle	.L52
	movl	$32, %r13d
	movl	$1, %r15d
	.p2align 4,,10
	.p2align 3
.L46:
	movl	120(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L34
	movq	104(%r12), %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	$200, %edi
	movq	%rax, -72(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L45
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	addl	$1, %r15d
	call	_ZN6icu_6710UnicodeSetC1ERKS0_@PLT
	movq	-72(%rbp), %r8
	movq	104(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	112(%r12), %rdx
	movq	112(%rbx), %rax
	addq	%r13, %rdx
	addq	%r13, %rax
	addq	$32, %r13
	movdqu	(%rdx), %xmm1
	movups	%xmm1, (%rax)
	movdqu	16(%rdx), %xmm2
	movups	%xmm2, 16(%rax)
	cmpl	%r15d, -80(%rbp)
	jne	.L46
.L52:
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.L34
	cmpq	$0, 192(%rbx)
	je	.L70
.L47:
	leaq	-60(%rbp), %rax
	movl	$-1, -60(%rbp)
	movq	%rax, -80(%rbp)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L71:
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	8(%r13), %edx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	192(%rbx), %rdi
	call	uhash_puti_67@PLT
.L51:
	movq	192(%r12), %rdi
.L48:
	movq	-80(%rbp), %rsi
	call	uhash_nextElement_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L34
	movl	120(%rbx), %eax
	testl	%eax, %eax
	jg	.L34
	movq	16(%r13), %rsi
	movl	$64, %edi
	movq	%rsi, -72(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L71
	movl	$7, 120(%rbx)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	120(%rdi), %r14
	movq	8(%rsi), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	$0, 16(%rdi)
	movq	8(%rdi), %rdi
	movq	%r14, %r8
	call	utext_clone_67@PLT
	movq	%rax, 8(%rbx)
	jmp	.L36
.L70:
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rdx
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rdi
	movq	%r14, %r8
	movl	$7, %ecx
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rsi
	call	uhash_openSize_67@PLT
	movl	120(%rbx), %edx
	movq	%rax, 192(%rbx)
	movq	%rax, %rdi
	testl	%edx, %edx
	jg	.L34
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	call	uhash_setKeyDeleter_67@PLT
	movq	192(%r12), %rdi
	jmp	.L47
.L45:
	movl	$7, 120(%rbx)
	jmp	.L52
.L69:
	call	__stack_chk_fail@PLT
.L37:
	movq	$0, 16(%rbx)
	movl	$7, 120(%rbx)
	jmp	.L34
.L41:
	movq	$0, 112(%rbx)
	movl	$7, 120(%rbx)
	jmp	.L34
	.cfi_endproc
.LFE4141:
	.size	_ZN6icu_6712RegexPatternaSERKS0_.part.0, .-_ZN6icu_6712RegexPatternaSERKS0_.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexPattern19groupNumberFromNameERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6712RegexPattern19groupNumberFromNameERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6712RegexPattern19groupNumberFromNameERKNS_13UnicodeStringER10UErrorCode:
.LFB3175:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L81
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	192(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L75
	call	uhash_geti_67@PLT
	testl	%eax, %eax
	jne	.L72
.L75:
	movl	$66325, (%rbx)
	xorl	%eax, %eax
.L72:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE3175:
	.size	_ZNK6icu_6712RegexPattern19groupNumberFromNameERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6712RegexPattern19groupNumberFromNameERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexPattern7matcherER10UErrorCode
	.type	_ZNK6icu_6712RegexPattern7matcherER10UErrorCode, @function
_ZNK6icu_6712RegexPattern7matcherER10UErrorCode:
.LFB3170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L84
	movl	120(%rdi), %eax
	movq	%rdi, %r13
	movq	%rsi, %rbx
	testl	%eax, %eax
	jg	.L90
	movl	$336, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L87
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6712RegexMatcherC1EPKNS_12RegexPatternE@PLT
.L84:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movl	%eax, (%rsi)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L87:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L84
	.cfi_endproc
.LFE3170:
	.size	_ZNK6icu_6712RegexPattern7matcherER10UErrorCode, .-_ZNK6icu_6712RegexPattern7matcherER10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3465:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3465:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3468:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L104
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L92
	cmpb	$0, 12(%rbx)
	jne	.L105
.L96:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L92:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L96
	.cfi_endproc
.LFE3468:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3471:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L108
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3471:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3474:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L111
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3474:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L117
.L113:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L118
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L118:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3476:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3477:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3477:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3478:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3478:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3479:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3479:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3480:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3480:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3481:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3481:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3482:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L134
	testl	%edx, %edx
	jle	.L134
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L137
.L126:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L126
	.cfi_endproc
.LFE3482:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L141
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L141
	testl	%r12d, %r12d
	jg	.L148
	cmpb	$0, 12(%rbx)
	jne	.L149
.L143:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L143
.L149:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L141:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3483:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L151
	movq	(%rdi), %r8
.L152:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L155
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L155
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L155:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3484:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3485:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L162
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3485:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3486:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3486:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3487:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3487:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3488:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3488:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3490:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3490:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3492:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3492:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPattern4initEv
	.type	_ZN6icu_6712RegexPattern4initEv, @function
_ZN6icu_6712RegexPattern4initEv:
.LFB3153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	120(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzwl	48(%rdi), %edx
	movups	%xmm0, 104(%rdi)
	movl	$0, 24(%rdi)
	movl	%edx, %eax
	movups	%xmm1, 120(%rdi)
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	movq	$0, 32(%rdi)
	cmovne	%edx, %eax
	movups	%xmm0, 8(%rdi)
	movq	$0, 136(%rdi)
	movw	%ax, 48(%rdi)
	movq	$0, 144(%rdi)
	movl	$0, 152(%rdi)
	movq	$0, 160(%rdi)
	movl	$0, 168(%rdi)
	movq	$0, 176(%rdi)
	movb	$0, 184(%rdi)
	movq	$0, 192(%rdi)
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L170
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679UVector64C1ER10UErrorCode@PLT
.L170:
	movq	%r12, 32(%rbx)
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L171
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
.L171:
	movq	%r12, 136(%rbx)
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L172
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
.L172:
	movq	%r12, 104(%rbx)
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L173
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
.L173:
	movq	%r12, 160(%rbx)
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L174
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
.L174:
	movl	120(%rbx), %edx
	movq	%rax, 176(%rbx)
	testl	%edx, %edx
	jg	.L168
	cmpq	$0, 32(%rbx)
	je	.L176
	cmpq	$0, 136(%rbx)
	je	.L176
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L176
	cmpq	$0, 160(%rbx)
	je	.L176
	testq	%rax, %rax
	je	.L176
	addq	$8, %rsp
	movq	%r13, %rdx
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movl	$7, 120(%rbx)
.L168:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3153:
	.size	_ZN6icu_6712RegexPattern4initEv, .-_ZN6icu_6712RegexPattern4initEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPatternC2Ev
	.type	_ZN6icu_6712RegexPatternC2Ev, @function
_ZN6icu_6712RegexPatternC2Ev:
.LFB3144:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712RegexPatternE(%rip), %rax
	movq	%rax, (%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 40(%rdi)
	movl	$2, %eax
	movw	%ax, 48(%rdi)
	jmp	_ZN6icu_6712RegexPattern4initEv
	.cfi_endproc
.LFE3144:
	.size	_ZN6icu_6712RegexPatternC2Ev, .-_ZN6icu_6712RegexPatternC2Ev
	.globl	_ZN6icu_6712RegexPatternC1Ev
	.set	_ZN6icu_6712RegexPatternC1Ev,_ZN6icu_6712RegexPatternC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPattern19initNamedCaptureMapEv
	.type	_ZN6icu_6712RegexPattern19initNamedCaptureMapEv, @function
_ZN6icu_6712RegexPattern19initNamedCaptureMapEv:
.LFB3154:
	.cfi_startproc
	endbr64
	cmpq	$0, 192(%rdi)
	movl	$1, %eax
	je	.L208
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120(%rdi), %r8
	movl	$7, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rdx
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rdi
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rsi
	call	uhash_openSize_67@PLT
	movl	120(%rbx), %edx
	movq	%rax, 192(%rbx)
	movq	%rax, %rdi
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L209
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	call	uhash_setKeyDeleter_67@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3154:
	.size	_ZN6icu_6712RegexPattern19initNamedCaptureMapEv, .-_ZN6icu_6712RegexPattern19initNamedCaptureMapEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexPatterneqERKS0_
	.type	_ZNK6icu_6712RegexPatterneqERKS0_, @function
_ZNK6icu_6712RegexPatterneqERKS0_:
.LFB3161:
	.cfi_startproc
	endbr64
	movl	24(%rsi), %ecx
	xorl	%eax, %eax
	cmpl	%ecx, 24(%rdi)
	je	.L242
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	120(%rsi), %edx
	cmpl	%edx, 120(%rdi)
	je	.L243
.L210:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L212
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L212
	movswl	8(%rdi), %edx
	movswl	8(%rsi), %ecx
	movl	%edx, %r8d
	movl	%ecx, %eax
	andl	$1, %eax
	andl	$1, %r8d
	jne	.L210
	testw	%dx, %dx
	js	.L213
	sarl	$5, %edx
.L214:
	testw	%cx, %cx
	js	.L215
	sarl	$5, %ecx
.L216:
	testb	$1, %al
	jne	.L217
	cmpl	%edx, %ecx
	je	.L244
.L217:
	movl	%r8d, %eax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L212:
	movq	8(%r12), %rdi
	movq	8(%rbx), %r8
	testq	%rdi, %rdi
	sete	%al
	testq	%r8, %r8
	je	.L210
	testq	%rdi, %rdi
	je	.L225
	movq	32(%r8), %rax
	testq	%rax, %rax
	jg	.L219
	movslq	28(%r8), %rdx
	negq	%rax
	cmpq	%rax, %rdx
	jle	.L219
	movq	48(%r8), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L219
	movl	%eax, 40(%r8)
	.p2align 4,,10
	.p2align 3
.L220:
	movq	32(%rdi), %rax
	testq	%rax, %rax
	jg	.L221
	movslq	28(%rdi), %rdx
	negq	%rax
	cmpq	%rax, %rdx
	jle	.L221
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L221
	movl	%eax, 40(%rdi)
.L222:
	movq	8(%rbx), %r8
	movq	%rdi, %rsi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	%r8, %rdi
	jmp	utext_equals_67@PLT
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L221:
	xorl	%esi, %esi
	call	utext_setNativeIndex_67@PLT
	movq	8(%r12), %rdi
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%r8, %rdi
	xorl	%esi, %esi
	call	utext_setNativeIndex_67@PLT
	movq	8(%r12), %rdi
	jmp	.L220
.L215:
	movl	12(%rsi), %ecx
	jmp	.L216
.L213:
	movl	12(%rdi), %edx
	jmp	.L214
.L244:
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r8b
	jmp	.L217
	.cfi_endproc
.LFE3161:
	.size	_ZNK6icu_6712RegexPatterneqERKS0_, .-_ZNK6icu_6712RegexPatterneqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexPattern6dumpOpEi
	.type	_ZNK6icu_6712RegexPattern6dumpOpEi, @function
_ZNK6icu_6712RegexPattern6dumpOpEi:
.LFB3179:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3179:
	.size	_ZNK6icu_6712RegexPattern6dumpOpEi, .-_ZNK6icu_6712RegexPattern6dumpOpEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexPattern11dumpPatternEv
	.type	_ZNK6icu_6712RegexPattern11dumpPatternEv, @function
_ZNK6icu_6712RegexPattern11dumpPatternEv:
.LFB3180:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3180:
	.size	_ZNK6icu_6712RegexPattern11dumpPatternEv, .-_ZNK6icu_6712RegexPattern11dumpPatternEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPattern16getStaticClassIDEv
	.type	_ZN6icu_6712RegexPattern16getStaticClassIDEv, @function
_ZN6icu_6712RegexPattern16getStaticClassIDEv:
.LFB3181:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712RegexPattern16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3181:
	.size	_ZN6icu_6712RegexPattern16getStaticClassIDEv, .-_ZN6icu_6712RegexPattern16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPattern3zapEv
	.type	_ZN6icu_6712RegexPattern3zapEv, @function
_ZN6icu_6712RegexPattern3zapEv:
.LFB3155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L249
	movq	(%rdi), %rax
	call	*8(%rax)
.L249:
	movq	$0, 32(%r12)
	movq	104(%r12), %rdi
	cmpl	$1, 8(%rdi)
	jle	.L250
	movl	$1, %ebx
	.p2align 4,,10
	.p2align 3
.L254:
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L251
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movq	104(%r12), %rdi
	addl	$1, %ebx
	cmpl	%ebx, 8(%rdi)
	jg	.L254
.L250:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	112(%r12), %rdi
	movq	$0, 104(%r12)
	testq	%rdi, %rdi
	je	.L255
	call	_ZN6icu_677UMemorydaEPv@PLT
.L255:
	movq	136(%r12), %rdi
	movq	$0, 112(%r12)
	testq	%rdi, %rdi
	je	.L256
	movq	(%rdi), %rax
	call	*8(%rax)
.L256:
	movq	160(%r12), %rdi
	movq	$0, 136(%r12)
	testq	%rdi, %rdi
	je	.L257
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L257:
	movq	176(%r12), %rdi
	movq	$0, 160(%r12)
	testq	%rdi, %rdi
	je	.L258
	call	_ZN6icu_677UMemorydlEPv@PLT
.L258:
	movq	8(%r12), %rdi
	movq	$0, 176(%r12)
	testq	%rdi, %rdi
	je	.L259
	call	utext_close_67@PLT
	movq	$0, 8(%r12)
.L259:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L260
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 16(%r12)
.L260:
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.L248
	call	uhash_close_67@PLT
	movq	$0, 192(%r12)
.L248:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movq	104(%r12), %rdi
	addl	$1, %ebx
	cmpl	8(%rdi), %ebx
	jl	.L254
	jmp	.L250
	.cfi_endproc
.LFE3155:
	.size	_ZN6icu_6712RegexPattern3zapEv, .-_ZN6icu_6712RegexPattern3zapEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPatternaSERKS0_
	.type	_ZN6icu_6712RegexPatternaSERKS0_, @function
_ZN6icu_6712RegexPatternaSERKS0_:
.LFB3152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	cmpq	%rsi, %rdi
	je	.L289
	movq	%rsi, %r13
	call	_ZN6icu_6712RegexPattern3zapEv
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexPattern4initEv
	movl	120(%r13), %eax
	movl	%eax, 120(%r12)
	testl	%eax, %eax
	jg	.L289
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexPatternaSERKS0_.part.0
.L289:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3152:
	.size	_ZN6icu_6712RegexPatternaSERKS0_, .-_ZN6icu_6712RegexPatternaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPatternD2Ev
	.type	_ZN6icu_6712RegexPatternD2Ev, @function
_ZN6icu_6712RegexPatternD2Ev:
.LFB3157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712RegexPatternE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712RegexPattern3zapEv
	leaq	40(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3157:
	.size	_ZN6icu_6712RegexPatternD2Ev, .-_ZN6icu_6712RegexPatternD2Ev
	.globl	_ZN6icu_6712RegexPatternD1Ev
	.set	_ZN6icu_6712RegexPatternD1Ev,_ZN6icu_6712RegexPatternD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPatternC2ERKS0_
	.type	_ZN6icu_6712RegexPatternC2ERKS0_, @function
_ZN6icu_6712RegexPatternC2ERKS0_:
.LFB3150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712RegexPatternE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 40(%rdi)
	movl	$2, %eax
	movw	%ax, 48(%rdi)
	call	_ZN6icu_6712RegexPattern4initEv
	cmpq	%r13, %r12
	je	.L294
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexPattern3zapEv
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexPattern4initEv
	movl	120(%r13), %eax
	movl	%eax, 120(%r12)
	testl	%eax, %eax
	jg	.L294
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexPatternaSERKS0_.part.0
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3150:
	.size	_ZN6icu_6712RegexPatternC2ERKS0_, .-_ZN6icu_6712RegexPatternC2ERKS0_
	.globl	_ZN6icu_6712RegexPatternC1ERKS0_
	.set	_ZN6icu_6712RegexPatternC1ERKS0_,_ZN6icu_6712RegexPatternC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexPattern5cloneEv
	.type	_ZNK6icu_6712RegexPattern5cloneEv, @function
_ZNK6icu_6712RegexPattern5cloneEv:
.LFB3160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$200, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L299
	leaq	16+_ZTVN6icu_6712RegexPatternE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 40(%r12)
	movl	$2, %eax
	movw	%ax, 48(%r12)
	call	_ZN6icu_6712RegexPattern4initEv
	cmpq	%r13, %r12
	je	.L299
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexPattern3zapEv
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexPattern4initEv
	movl	120(%r13), %eax
	movl	%eax, 120(%r12)
	testl	%eax, %eax
	jg	.L299
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexPatternaSERKS0_.part.0
.L299:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3160:
	.size	_ZNK6icu_6712RegexPattern5cloneEv, .-_ZNK6icu_6712RegexPattern5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPatternD0Ev
	.type	_ZN6icu_6712RegexPatternD0Ev, @function
_ZN6icu_6712RegexPatternD0Ev:
.LFB3159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712RegexPatternE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712RegexPattern3zapEv
	leaq	40(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3159:
	.size	_ZN6icu_6712RegexPatternD0Ev, .-_ZN6icu_6712RegexPatternD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode.part.0, @function
_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode.part.0:
.LFB4142:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$568, %rsp
	movq	%rdi, -600(%rbp)
	movl	$200, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L307
	leaq	16+_ZTVN6icu_6712RegexPatternE(%rip), %r15
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%r15, (%rax)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, 48(%r12)
	movq	%rax, 40(%r12)
	call	_ZN6icu_6712RegexPattern4initEv
	movl	120(%r12), %edx
	testl	%edx, %edx
	jg	.L313
	movl	%r14d, 24(%r12)
	leaq	-592(%rbp), %r14
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712RegexCompileC1EPNS_12RegexPatternER10UErrorCode@PLT
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	-600(%rbp), %rsi
	call	_ZN6icu_6712RegexCompile7compileERKNS_13UnicodeStringER11UParseErrorR10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L314
.L310:
	movq	%r14, %rdi
	call	_ZN6icu_6712RegexCompileD1Ev@PLT
.L306:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L315
	addq	$568, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movq	%r15, (%r12)
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexPattern3zapEv
	leaq	40(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L313:
	movl	%edx, (%rbx)
	movq	%r12, %rdi
	movq	%r15, (%r12)
	call	_ZN6icu_6712RegexPattern3zapEv
	leaq	40(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L306
.L307:
	movl	$7, (%rbx)
	jmp	.L306
.L315:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4142:
	.size	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode.part.0, .-_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode
	.type	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode, @function
_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode:
.LFB3162:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L316
	testl	$-960, %esi
	jne	.L321
	testb	$-128, %sil
	jne	.L322
	jmp	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L321:
	movl	$66315, (%rcx)
.L316:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	movl	$66309, (%rcx)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3162:
	.size	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode, .-_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, @function
_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringER11UParseErrorR10UErrorCode:
.LFB3164:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	movq	%rdx, %rcx
	testl	%eax, %eax
	jg	.L324
	movq	%rsi, %rdx
	xorl	%esi, %esi
	jmp	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L324:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3164:
	.size	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, .-_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR10UErrorCode
	.type	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR10UErrorCode, @function
_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR10UErrorCode:
.LFB3166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L325
	movq	%rdx, %rcx
	testl	$-960, %esi
	jne	.L332
	testb	$-128, %sil
	jne	.L333
	leaq	-80(%rbp), %rdx
	call	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode.part.0
.L325:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L334
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movl	$66309, (%rdx)
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L332:
	movl	$66315, (%rdx)
	jmp	.L325
.L334:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3166:
	.size	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR10UErrorCode, .-_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode.part.0, @function
_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode.part.0:
.LFB4143:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$568, %rsp
	movq	%rdi, -600(%rbp)
	movl	$200, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L336
	leaq	16+_ZTVN6icu_6712RegexPatternE(%rip), %r15
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%r15, (%rax)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, 48(%r12)
	movq	%rax, 40(%r12)
	call	_ZN6icu_6712RegexPattern4initEv
	movl	120(%r12), %edx
	testl	%edx, %edx
	jg	.L342
	movl	%r14d, 24(%r12)
	leaq	-592(%rbp), %r14
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712RegexCompileC1EPNS_12RegexPatternER10UErrorCode@PLT
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	-600(%rbp), %rsi
	call	_ZN6icu_6712RegexCompile7compileEP5UTextR11UParseErrorR10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L343
.L339:
	movq	%r14, %rdi
	call	_ZN6icu_6712RegexCompileD1Ev@PLT
.L335:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L344
	addq	$568, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	movq	%r15, (%r12)
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexPattern3zapEv
	leaq	40(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L342:
	movl	%edx, (%rbx)
	movq	%r12, %rdi
	movq	%r15, (%r12)
	call	_ZN6icu_6712RegexPattern3zapEv
	leaq	40(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L335
.L336:
	movl	$7, (%rbx)
	jmp	.L335
.L344:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4143:
	.size	_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode.part.0, .-_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode
	.type	_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode, @function
_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode:
.LFB3163:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L345
	testl	$-960, %esi
	jne	.L350
	testb	$-128, %sil
	jne	.L351
	jmp	_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L350:
	movl	$66315, (%rcx)
.L345:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	movl	$66309, (%rcx)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3163:
	.size	_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode, .-_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPattern7compileEP5UTextR11UParseErrorR10UErrorCode
	.type	_ZN6icu_6712RegexPattern7compileEP5UTextR11UParseErrorR10UErrorCode, @function
_ZN6icu_6712RegexPattern7compileEP5UTextR11UParseErrorR10UErrorCode:
.LFB3165:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	movq	%rdx, %rcx
	testl	%eax, %eax
	jg	.L353
	movq	%rsi, %rdx
	xorl	%esi, %esi
	jmp	_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L353:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3165:
	.size	_ZN6icu_6712RegexPattern7compileEP5UTextR11UParseErrorR10UErrorCode, .-_ZN6icu_6712RegexPattern7compileEP5UTextR11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPattern7compileEP5UTextjR10UErrorCode
	.type	_ZN6icu_6712RegexPattern7compileEP5UTextjR10UErrorCode, @function
_ZN6icu_6712RegexPattern7compileEP5UTextjR10UErrorCode:
.LFB3167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L354
	movq	%rdx, %rcx
	testl	$-960, %esi
	jne	.L361
	testb	$-128, %sil
	jne	.L362
	leaq	-80(%rbp), %rdx
	call	_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode.part.0
.L354:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L363
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movl	$66309, (%rdx)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L361:
	movl	$66315, (%rdx)
	jmp	.L354
.L363:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3167:
	.size	_ZN6icu_6712RegexPattern7compileEP5UTextjR10UErrorCode, .-_ZN6icu_6712RegexPattern7compileEP5UTextjR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexPattern7matcherERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6712RegexPattern7matcherERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6712RegexPattern7matcherERKNS_13UnicodeStringER10UErrorCode:
.LFB3169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdx), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	testl	%eax, %eax
	jg	.L364
	movl	120(%rdi), %eax
	movq	%rdi, %r14
	movq	%rdx, %rbx
	testl	%eax, %eax
	jg	.L370
	movl	$336, %edi
	movq	%rsi, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L367
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6712RegexMatcherC1EPKNS_12RegexPatternE@PLT
	movl	(%rbx), %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, 316(%r12)
	call	_ZN6icu_6712RegexMatcher5resetERKNS_13UnicodeStringE@PLT
.L364:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	popq	%rbx
	movl	%eax, (%rdx)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L367:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L364
	.cfi_endproc
.LFE3169:
	.size	_ZNK6icu_6712RegexPattern7matcherERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6712RegexPattern7matcherERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPattern7matchesERKNS_13UnicodeStringES3_R11UParseErrorR10UErrorCode
	.type	_ZN6icu_6712RegexPattern7matchesERKNS_13UnicodeStringES3_R11UParseErrorR10UErrorCode, @function
_ZN6icu_6712RegexPattern7matchesERKNS_13UnicodeStringES3_R11UParseErrorR10UErrorCode:
.LFB3171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jle	.L379
.L371:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	movq	%rsi, %r14
	xorl	%esi, %esi
	movq	%rcx, %r12
	call	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode.part.0
	movq	%rax, %r15
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L380
	movl	120(%r15), %eax
	testl	%eax, %eax
	jg	.L381
	movl	$336, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L376
	movq	%rax, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6712RegexMatcherC1EPKNS_12RegexPatternE@PLT
	movl	(%r12), %eax
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%eax, 316(%rbx)
	call	_ZN6icu_6712RegexMatcher5resetERKNS_13UnicodeStringE@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6712RegexMatcher7matchesER10UErrorCode@PLT
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	_ZN6icu_6712RegexMatcherD0Ev@PLT
.L374:
	leaq	16+_ZTVN6icu_6712RegexPatternE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, (%r15)
	call	_ZN6icu_6712RegexPattern3zapEv
	leaq	40(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L381:
	movl	%eax, (%r12)
	movq	%r12, %rsi
	xorl	%edi, %edi
	call	_ZN6icu_6712RegexMatcher7matchesER10UErrorCode@PLT
	movl	%eax, %r13d
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L380:
	movq	%r12, %rsi
	xorl	%edi, %edi
	call	_ZN6icu_6712RegexMatcher7matchesER10UErrorCode@PLT
	movl	%eax, %r13d
	testq	%r15, %r15
	je	.L371
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L376:
	movl	$7, (%r12)
	movq	%r12, %rsi
	xorl	%edi, %edi
	call	_ZN6icu_6712RegexMatcher7matchesER10UErrorCode@PLT
	movl	%eax, %r13d
	jmp	.L374
	.cfi_endproc
.LFE3171:
	.size	_ZN6icu_6712RegexPattern7matchesERKNS_13UnicodeStringES3_R11UParseErrorR10UErrorCode, .-_ZN6icu_6712RegexPattern7matchesERKNS_13UnicodeStringES3_R11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexPattern7matchesEP5UTextS2_R11UParseErrorR10UErrorCode
	.type	_ZN6icu_6712RegexPattern7matchesEP5UTextS2_R11UParseErrorR10UErrorCode, @function
_ZN6icu_6712RegexPattern7matchesEP5UTextS2_R11UParseErrorR10UErrorCode:
.LFB3172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jle	.L383
.L391:
	xorl	%r15d, %r15d
.L382:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L383:
	.cfi_restore_state
	movq	%rcx, %r12
	movq	%rsi, %r13
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode.part.0
	movl	(%r12), %edx
	movq	%rax, %r14
	testl	%edx, %edx
	jg	.L385
	movl	120(%rax), %eax
	testl	%eax, %eax
	jg	.L395
	movl	$336, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L388
	movq	%rax, %rdi
	movq	%r14, %rsi
	xorl	%r15d, %r15d
	call	_ZN6icu_6712RegexMatcherC1EPKNS_12RegexPatternE@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L396
.L389:
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcherD0Ev@PLT
.L390:
	leaq	16+_ZTVN6icu_6712RegexPatternE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_6712RegexPattern3zapEv
	leaq	40(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L395:
	movl	%eax, (%r12)
.L393:
	xorl	%r15d, %r15d
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L385:
	testq	%rax, %rax
	je	.L391
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher5resetEP5UText@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher7matchesER10UErrorCode@PLT
	movl	%eax, %r15d
	jmp	.L389
.L388:
	movl	$7, (%r12)
	jmp	.L393
	.cfi_endproc
.LFE3172:
	.size	_ZN6icu_6712RegexPattern7matchesEP5UTextS2_R11UParseErrorR10UErrorCode, .-_ZN6icu_6712RegexPattern7matchesEP5UTextS2_R11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6712RegexPattern5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode.part.0, @function
_ZNK6icu_6712RegexPattern5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode.part.0:
.LFB4145:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-400(%rbp), %r15
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdi, %rsi
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$376, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6712RegexMatcherC1EPKNS_12RegexPatternE@PLT
	movl	-84(%rbp), %eax
	testl	%eax, %eax
	jle	.L403
	xorl	%eax, %eax
.L398:
	movq	%r15, %rdi
	movl	%eax, -404(%rbp)
	call	_ZN6icu_6712RegexMatcherD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movl	-404(%rbp), %eax
	jne	.L404
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movq	%rbx, %r8
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6712RegexMatcher5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode@PLT
	jmp	.L398
.L404:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4145:
	.size	_ZNK6icu_6712RegexPattern5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode.part.0, .-_ZNK6icu_6712RegexPattern5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexPattern5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode
	.type	_ZNK6icu_6712RegexPattern5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode, @function
_ZNK6icu_6712RegexPattern5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode:
.LFB3177:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L407
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	jmp	_ZNK6icu_6712RegexPattern5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode.part.0
	.cfi_endproc
.LFE3177:
	.size	_ZNK6icu_6712RegexPattern5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode, .-_ZNK6icu_6712RegexPattern5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6712RegexPattern5splitEP5UTextPS2_iR10UErrorCode.part.0, @function
_ZNK6icu_6712RegexPattern5splitEP5UTextPS2_iR10UErrorCode.part.0:
.LFB4146:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-400(%rbp), %r15
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdi, %rsi
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$376, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6712RegexMatcherC1EPKNS_12RegexPatternE@PLT
	movl	-84(%rbp), %eax
	testl	%eax, %eax
	jle	.L414
	xorl	%eax, %eax
.L409:
	movq	%r15, %rdi
	movl	%eax, -404(%rbp)
	call	_ZN6icu_6712RegexMatcherD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movl	-404(%rbp), %eax
	jne	.L415
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	.cfi_restore_state
	movq	%rbx, %r8
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCode@PLT
	jmp	.L409
.L415:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4146:
	.size	_ZNK6icu_6712RegexPattern5splitEP5UTextPS2_iR10UErrorCode.part.0, .-_ZNK6icu_6712RegexPattern5splitEP5UTextPS2_iR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexPattern5splitEP5UTextPS2_iR10UErrorCode
	.type	_ZNK6icu_6712RegexPattern5splitEP5UTextPS2_iR10UErrorCode, @function
_ZNK6icu_6712RegexPattern5splitEP5UTextPS2_iR10UErrorCode:
.LFB3178:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L418
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	jmp	_ZNK6icu_6712RegexPattern5splitEP5UTextPS2_iR10UErrorCode.part.0
	.cfi_endproc
.LFE3178:
	.size	_ZNK6icu_6712RegexPattern5splitEP5UTextPS2_iR10UErrorCode, .-_ZNK6icu_6712RegexPattern5splitEP5UTextPS2_iR10UErrorCode
	.weak	_ZTSN6icu_6712RegexPatternE
	.section	.rodata._ZTSN6icu_6712RegexPatternE,"aG",@progbits,_ZTSN6icu_6712RegexPatternE,comdat
	.align 16
	.type	_ZTSN6icu_6712RegexPatternE, @object
	.size	_ZTSN6icu_6712RegexPatternE, 24
_ZTSN6icu_6712RegexPatternE:
	.string	"N6icu_6712RegexPatternE"
	.weak	_ZTIN6icu_6712RegexPatternE
	.section	.data.rel.ro._ZTIN6icu_6712RegexPatternE,"awG",@progbits,_ZTIN6icu_6712RegexPatternE,comdat
	.align 8
	.type	_ZTIN6icu_6712RegexPatternE, @object
	.size	_ZTIN6icu_6712RegexPatternE, 24
_ZTIN6icu_6712RegexPatternE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712RegexPatternE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6712RegexPatternE
	.section	.data.rel.ro.local._ZTVN6icu_6712RegexPatternE,"awG",@progbits,_ZTVN6icu_6712RegexPatternE,comdat
	.align 8
	.type	_ZTVN6icu_6712RegexPatternE, @object
	.size	_ZTVN6icu_6712RegexPatternE, 120
_ZTVN6icu_6712RegexPatternE:
	.quad	0
	.quad	_ZTIN6icu_6712RegexPatternE
	.quad	_ZN6icu_6712RegexPatternD1Ev
	.quad	_ZN6icu_6712RegexPatternD0Ev
	.quad	_ZNK6icu_6712RegexPattern17getDynamicClassIDEv
	.quad	_ZNK6icu_6712RegexPattern5cloneEv
	.quad	_ZNK6icu_6712RegexPattern5flagsEv
	.quad	_ZNK6icu_6712RegexPattern7matcherERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6712RegexPattern7matcherER10UErrorCode
	.quad	_ZNK6icu_6712RegexPattern7patternEv
	.quad	_ZNK6icu_6712RegexPattern11patternTextER10UErrorCode
	.quad	_ZNK6icu_6712RegexPattern19groupNumberFromNameERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6712RegexPattern19groupNumberFromNameEPKciR10UErrorCode
	.quad	_ZNK6icu_6712RegexPattern5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode
	.quad	_ZNK6icu_6712RegexPattern5splitEP5UTextPS2_iR10UErrorCode
	.local	_ZZN6icu_6712RegexPattern16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6712RegexPattern16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
