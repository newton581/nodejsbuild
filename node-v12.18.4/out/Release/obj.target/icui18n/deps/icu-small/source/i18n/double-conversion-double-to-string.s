	.file	"double-conversion-double-to-string.cpp"
	.text
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB2:
	.text
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_
	.type	_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_, @function
_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_:
.LFB1276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	movq	%xmm0, %rdx
	subq	$16, %rsp
	testq	%rdx, %rdx
	jns	.L2
	xorpd	.LC0(%rip), %xmm0
	movl	$1, %eax
.L2:
	movb	%al, (%r8)
	cmpl	$3, %edi
	jne	.L3
	testl	%r13d, %r13d
	jne	.L3
	movb	$0, (%rbx)
	movl	$0, (%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	ucomisd	.LC1(%rip), %xmm0
	jnp	.L23
.L5:
	testl	%edi, %edi
	jne	.L7
	movq	16(%rbp), %r9
	xorl	%esi, %esi
	movq	%r12, %r8
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movsd	%xmm0, -40(%rbp)
	call	_ZN6icu_6717double_conversion8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_@PLT
	testb	%al, %al
	jne	.L1
	movsd	-40(%rbp), %xmm0
	movq	16(%rbp), %r9
	movq	%r12, %r8
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movl	%r13d, %esi
	xorl	%edi, %edi
	call	_ZN6icu_6717double_conversion10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_@PLT
	movslq	(%r12), %rax
	movb	$0, (%rbx,%rax)
.L1:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	jne	.L5
	movl	$48, %eax
	movw	%ax, (%rbx)
	movq	16(%rbp), %rax
	movl	$1, (%r12)
	movl	$1, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_.cold, @function
_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_.cold:
.LFSB1276:
.L7:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	call	abort@PLT
	.cfi_endproc
.LFE1276:
	.text
	.size	_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_, .-_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_.cold, .-_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_.cold
.LCOLDE2:
	.text
.LHOTE2:
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
