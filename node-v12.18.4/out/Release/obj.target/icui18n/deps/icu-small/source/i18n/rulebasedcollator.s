	.file	"rulebasedcollator.cpp"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSink6ResizeEii, @function
_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSink6ResizeEii:
.LFB3461:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3461:
	.size	_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSink6ResizeEii, .-_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSink6ResizeEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator17getDynamicClassIDEv
	.type	_ZNK6icu_6717RuleBasedCollator17getDynamicClassIDEv, @function
_ZNK6icu_6717RuleBasedCollator17getDynamicClassIDEv:
.LFB3490:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6717RuleBasedCollator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3490:
	.size	_ZNK6icu_6717RuleBasedCollator17getDynamicClassIDEv, .-_ZNK6icu_6717RuleBasedCollator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator14getMaxVariableEv
	.type	_ZNK6icu_6717RuleBasedCollator14getMaxVariableEv, @function
_ZNK6icu_6717RuleBasedCollator14getMaxVariableEv:
.LFB3514:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	24(%rax), %eax
	sarl	$4, %eax
	andl	$7, %eax
	addl	$4096, %eax
	ret
	.cfi_endproc
.LFE3514:
	.size	_ZNK6icu_6717RuleBasedCollator14getMaxVariableEv, .-_ZNK6icu_6717RuleBasedCollator14getMaxVariableEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator14getVariableTopER10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator14getVariableTopER10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator14getVariableTopER10UErrorCode:
.LFB3515:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	28(%rax), %eax
	ret
	.cfi_endproc
.LFE3515:
	.size	_ZNK6icu_6717RuleBasedCollator14getVariableTopER10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator14getVariableTopER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_116UTF16NFDIterator16nextRawCodePointEv, @function
_ZN6icu_6712_GLOBAL__N_116UTF16NFDIterator16nextRawCodePointEv:
.LFB3539:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	40(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L9
	movzwl	(%rax), %r8d
	leaq	2(%rax), %rcx
	movq	%rcx, 32(%rdi)
	movl	%r8d, %esi
	testq	%rdx, %rdx
	jne	.L8
	testl	%r8d, %r8d
	je	.L22
.L8:
	movl	%esi, %r9d
	andl	$-1024, %r9d
	cmpl	$55296, %r9d
	jne	.L6
	cmpq	%rcx, %rdx
	jne	.L23
.L6:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movzwl	2(%rax), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L6
	addq	$4, %rax
	sall	$10, %esi
	movq	%rax, 32(%rdi)
	leal	-56613888(%rdx,%rsi), %r8d
	jmp	.L6
.L22:
	movq	$0, 32(%rdi)
	movl	$-1, %r8d
	jmp	.L6
.L9:
	movl	$-1, %r8d
	jmp	.L6
	.cfi_endproc
.LFE3539:
	.size	_ZN6icu_6712_GLOBAL__N_116UTF16NFDIterator16nextRawCodePointEv, .-_ZN6icu_6712_GLOBAL__N_116UTF16NFDIterator16nextRawCodePointEv
	.section	.rodata
.LC1:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\036\017\017\017"
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	" 000000000000\02000"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_115UTF8NFDIterator16nextRawCodePointEv, @function
_ZN6icu_6712_GLOBAL__N_115UTF8NFDIterator16nextRawCodePointEv:
.LFB3550:
	.cfi_startproc
	endbr64
	movl	40(%rdi), %edx
	movl	44(%rdi), %r8d
	cmpl	%r8d, %edx
	je	.L34
	movq	32(%rdi), %r9
	movslq	%edx, %rax
	addq	%r9, %rax
	cmpb	$0, (%rax)
	jne	.L35
	testl	%r8d, %r8d
	js	.L34
.L35:
	leal	1(%rdx), %esi
	movl	%esi, 40(%rdi)
	movzbl	(%rax), %eax
	movl	%eax, %ecx
	testb	%al, %al
	js	.L45
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	cmpl	%esi, %r8d
	je	.L30
	cmpl	$223, %eax
	jg	.L46
	cmpl	$193, %eax
	jle	.L30
	andl	$31, %eax
.L32:
	movslq	%esi, %rdx
	movzbl	(%r9,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L30
	sall	$6, %eax
	addl	$1, %esi
	movl	%eax, %ecx
	movzbl	%dl, %eax
	movl	%esi, 40(%rdi)
	orl	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	subl	$240, %eax
	cmpl	$4, %eax
	jg	.L30
	movslq	%esi, %rsi
	movzbl	(%r9,%rsi), %r10d
	leaq	.LC1(%rip), %rsi
	movq	%r10, %rcx
	shrq	$4, %rcx
	andl	$15, %ecx
	movsbl	(%rsi,%rcx), %ecx
	btl	%eax, %ecx
	jc	.L47
.L30:
	movl	$65533, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	cmpl	$239, %eax
	jg	.L29
	movslq	%esi, %rdx
	andl	$15, %ecx
	leaq	.LC0(%rip), %r10
	andl	$15, %eax
	movzbl	(%r9,%rdx), %edx
	movsbl	(%r10,%rcx), %r10d
	movl	%edx, %ecx
	andl	$63, %edx
	shrb	$5, %cl
	btl	%ecx, %r10d
	jnc	.L30
.L31:
	sall	$6, %eax
	movzbl	%dl, %ecx
	addl	$1, %esi
	movl	%esi, 40(%rdi)
	orl	%ecx, %eax
	cmpl	%esi, %r8d
	jne	.L32
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L47:
	leal	2(%rdx), %esi
	movl	%esi, 40(%rdi)
	cmpl	%esi, %r8d
	je	.L30
	movslq	%esi, %rdx
	movzbl	(%r9,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L30
	sall	$6, %eax
	andl	$63, %r10d
	orl	%r10d, %eax
	jmp	.L31
.L34:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3550:
	.size	_ZN6icu_6712_GLOBAL__N_115UTF8NFDIterator16nextRawCodePointEv, .-_ZN6icu_6712_GLOBAL__N_115UTF8NFDIterator16nextRawCodePointEv
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_117PartLevelCallback11needToWriteENS_9Collation5LevelE, @function
_ZN6icu_6712_GLOBAL__N_117PartLevelCallback11needToWriteENS_9Collation5LevelE:
.LFB3583:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	xorl	%r8d, %r8d
	movl	16(%rdx), %eax
	movl	20(%rdx), %ecx
	cmpl	%ecx, %eax
	jl	.L48
	addl	24(%rdx), %eax
	movl	%esi, 16(%rdi)
	movl	$1, %r8d
	subl	%ecx, %eax
	movl	%eax, 20(%rdi)
.L48:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3583:
	.size	_ZN6icu_6712_GLOBAL__N_117PartLevelCallback11needToWriteENS_9Collation5LevelE, .-_ZN6icu_6712_GLOBAL__N_117PartLevelCallback11needToWriteENS_9Collation5LevelE
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSink20AppendBeyondCapacityEPKcii, @function
_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSink20AppendBeyondCapacityEPKcii:
.LFB3460:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %edx
	subl	%ecx, %edx
	testl	%edx, %edx
	jg	.L53
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	movslq	%ecx, %rcx
	addq	8(%rdi), %rcx
	movslq	%edx, %rdx
	movq	%rcx, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3460:
	.size	_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSink20AppendBeyondCapacityEPKcii, .-_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSink20AppendBeyondCapacityEPKcii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator10getVersionEPh
	.type	_ZNK6icu_6717RuleBasedCollator10getVersionEPh, @function
_ZNK6icu_6717RuleBasedCollator10getVersionEPh:
.LFB3500:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	328(%rax), %eax
	movl	%eax, (%rsi)
	subb	$112, (%rsi)
	ret
	.cfi_endproc
.LFE3500:
	.size	_ZNK6icu_6717RuleBasedCollator10getVersionEPh, .-_ZNK6icu_6717RuleBasedCollator10getVersionEPh
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationKeyByteSinkD2Ev
	.type	_ZN6icu_6720CollationKeyByteSinkD2Ev, @function
_ZN6icu_6720CollationKeyByteSinkD2Ev:
.LFB3466:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6720CollationKeyByteSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6715SortKeyByteSinkD2Ev@PLT
	.cfi_endproc
.LFE3466:
	.size	_ZN6icu_6720CollationKeyByteSinkD2Ev, .-_ZN6icu_6720CollationKeyByteSinkD2Ev
	.globl	_ZN6icu_6720CollationKeyByteSinkD1Ev
	.set	_ZN6icu_6720CollationKeyByteSinkD1Ev,_ZN6icu_6720CollationKeyByteSinkD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkD2Ev, @function
_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkD2Ev:
.LFB3457:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6715SortKeyByteSinkD2Ev@PLT
	.cfi_endproc
.LFE3457:
	.size	_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkD2Ev, .-_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkD1Ev,_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationKeyByteSinkD0Ev
	.type	_ZN6icu_6720CollationKeyByteSinkD0Ev, @function
_ZN6icu_6720CollationKeyByteSinkD0Ev:
.LFB3468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6720CollationKeyByteSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6715SortKeyByteSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3468:
	.size	_ZN6icu_6720CollationKeyByteSinkD0Ev, .-_ZN6icu_6720CollationKeyByteSinkD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkD0Ev, @function
_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkD0Ev:
.LFB3459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6715SortKeyByteSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3459:
	.size	_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkD0Ev, .-_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollator10setLocalesERKNS_6LocaleES3_S3_
	.type	_ZN6icu_6717RuleBasedCollator10setLocalesERKNS_6LocaleES3_S3_, @function
_ZN6icu_6717RuleBasedCollator10setLocalesERKNS_6LocaleES3_S3_:
.LFB3495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	movq	24(%rbx), %rax
	leaq	104(%rax), %rsi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	leaq	40(%rbx), %rdi
	movq	%r12, %rsi
	testb	%al, %al
	sete	268(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleaSERKS0_@PLT
	.cfi_endproc
.LFE3495:
	.size	_ZN6icu_6717RuleBasedCollator10setLocalesERKNS_6LocaleES3_S3_, .-_ZN6icu_6717RuleBasedCollator10setLocalesERKNS_6LocaleES3_S3_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator9getLocaleE18ULocDataLocaleTypeR10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator9getLocaleE18ULocDataLocaleTypeR10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator9getLocaleE18ULocDataLocaleTypeR10UErrorCode:
.LFB3496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L74
	testl	%edx, %edx
	je	.L66
	cmpl	$1, %edx
	je	.L67
	movl	$1, (%rcx)
.L74:
	call	_ZN6icu_676Locale7getRootEv@PLT
	movq	%rax, %rsi
.L73:
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	addq	$40, %rsi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	cmpb	$0, 268(%rsi)
	je	.L69
	addq	$40, %rsi
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L69:
	movq	24(%rsi), %rsi
	addq	$104, %rsi
	jmp	.L73
	.cfi_endproc
.LFE3496:
	.size	_ZNK6icu_6717RuleBasedCollator9getLocaleE18ULocDataLocaleTypeR10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator9getLocaleE18ULocDataLocaleTypeR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_116UTF16NFDIteratorD2Ev, @function
_ZN6icu_6712_GLOBAL__N_116UTF16NFDIteratorD2Ev:
.LFB3542:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3542:
	.size	_ZN6icu_6712_GLOBAL__N_116UTF16NFDIteratorD2Ev, .-_ZN6icu_6712_GLOBAL__N_116UTF16NFDIteratorD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_116UTF16NFDIteratorD1Ev,_ZN6icu_6712_GLOBAL__N_116UTF16NFDIteratorD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_116UTF16NFDIteratorD0Ev, @function
_ZN6icu_6712_GLOBAL__N_116UTF16NFDIteratorD0Ev:
.LFB3544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3544:
	.size	_ZN6icu_6712_GLOBAL__N_116UTF16NFDIteratorD0Ev, .-_ZN6icu_6712_GLOBAL__N_116UTF16NFDIteratorD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorD2Ev, @function
_ZN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorD2Ev:
.LFB4842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	48(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -48(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE4842:
	.size	_ZN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorD2Ev, .-_ZN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorD1Ev,_ZN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorD0Ev, @function
_ZN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorD0Ev:
.LFB4844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	48(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -48(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4844:
	.size	_ZN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorD0Ev, .-_ZN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_115UTF8NFDIteratorD2Ev, @function
_ZN6icu_6712_GLOBAL__N_115UTF8NFDIteratorD2Ev:
.LFB4838:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE4838:
	.size	_ZN6icu_6712_GLOBAL__N_115UTF8NFDIteratorD2Ev, .-_ZN6icu_6712_GLOBAL__N_115UTF8NFDIteratorD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_115UTF8NFDIteratorD1Ev,_ZN6icu_6712_GLOBAL__N_115UTF8NFDIteratorD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_115UTF8NFDIteratorD0Ev, @function
_ZN6icu_6712_GLOBAL__N_115UTF8NFDIteratorD0Ev:
.LFB4840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4840:
	.size	_ZN6icu_6712_GLOBAL__N_115UTF8NFDIteratorD0Ev, .-_ZN6icu_6712_GLOBAL__N_115UTF8NFDIteratorD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_116UIterNFDIteratorD2Ev, @function
_ZN6icu_6712_GLOBAL__N_116UIterNFDIteratorD2Ev:
.LFB4830:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE4830:
	.size	_ZN6icu_6712_GLOBAL__N_116UIterNFDIteratorD2Ev, .-_ZN6icu_6712_GLOBAL__N_116UIterNFDIteratorD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_116UIterNFDIteratorD1Ev,_ZN6icu_6712_GLOBAL__N_116UIterNFDIteratorD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_116UIterNFDIteratorD0Ev, @function
_ZN6icu_6712_GLOBAL__N_116UIterNFDIteratorD0Ev:
.LFB4832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4832:
	.size	_ZN6icu_6712_GLOBAL__N_116UIterNFDIteratorD0Ev, .-_ZN6icu_6712_GLOBAL__N_116UIterNFDIteratorD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorD2Ev, @function
_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorD2Ev:
.LFB4834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	32(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	call	_ZN6icu_6724FCDUTF8CollationIteratorD1Ev@PLT
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE4834:
	.size	_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorD2Ev, .-_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorD1Ev,_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorD0Ev, @function
_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorD0Ev:
.LFB4836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	32(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	call	_ZN6icu_6724FCDUTF8CollationIteratorD1Ev@PLT
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4836:
	.size	_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorD0Ev, .-_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIterator16nextRawCodePointEv, @function
_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIterator16nextRawCodePointEv:
.LFB3554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$32, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6724FCDUTF8CollationIterator13nextCodePointER10UErrorCode@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L95
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L95:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3554:
	.size	_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIterator16nextRawCodePointEv, .-_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIterator16nextRawCodePointEv
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorD2Ev, @function
_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorD2Ev:
.LFB4826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	32(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	call	_ZN6icu_6725FCDUIterCollationIteratorD1Ev@PLT
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE4826:
	.size	_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorD2Ev, .-_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorD1Ev,_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorD0Ev, @function
_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorD0Ev:
.LFB4828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	32(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	call	_ZN6icu_6725FCDUIterCollationIteratorD1Ev@PLT
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4828:
	.size	_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorD0Ev, .-_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_116UIterNFDIterator16nextRawCodePointEv, @function
_ZN6icu_6712_GLOBAL__N_116UIterNFDIterator16nextRawCodePointEv:
.LFB3558:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	uiter_next32_67@PLT
	.cfi_endproc
.LFE3558:
	.size	_ZN6icu_6712_GLOBAL__N_116UIterNFDIterator16nextRawCodePointEv, .-_ZN6icu_6712_GLOBAL__N_116UIterNFDIterator16nextRawCodePointEv
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIterator16nextRawCodePointEv, @function
_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIterator16nextRawCodePointEv:
.LFB3562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$32, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6725FCDUIterCollationIterator13nextCodePointER10UErrorCode@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L104
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L104:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3562:
	.size	_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIterator16nextRawCodePointEv, .-_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIterator16nextRawCodePointEv
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_117PartLevelCallbackD2Ev, @function
_ZN6icu_6712_GLOBAL__N_117PartLevelCallbackD2Ev:
.LFB3580:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_117PartLevelCallbackE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6713CollationKeys13LevelCallbackD2Ev@PLT
	.cfi_endproc
.LFE3580:
	.size	_ZN6icu_6712_GLOBAL__N_117PartLevelCallbackD2Ev, .-_ZN6icu_6712_GLOBAL__N_117PartLevelCallbackD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_117PartLevelCallbackD1Ev,_ZN6icu_6712_GLOBAL__N_117PartLevelCallbackD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_117PartLevelCallbackD0Ev, @function
_ZN6icu_6712_GLOBAL__N_117PartLevelCallbackD0Ev:
.LFB3582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_117PartLevelCallbackE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6713CollationKeys13LevelCallbackD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3582:
	.size	_ZN6icu_6712_GLOBAL__N_117PartLevelCallbackD0Ev, .-_ZN6icu_6712_GLOBAL__N_117PartLevelCallbackD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollator20computeMaxExpansionsEPKNS_18CollationTailoringER10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollator20computeMaxExpansionsEPKNS_18CollationTailoringER10UErrorCode, @function
_ZN6icu_6717RuleBasedCollator20computeMaxExpansionsEPKNS_18CollationTailoringER10UErrorCode:
.LFB3592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	_ZN6icu_6724CollationElementIterator20computeMaxExpansionsEPKNS_13CollationDataER10UErrorCode@PLT
	movq	%rax, 384(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3592:
	.size	_ZN6icu_6717RuleBasedCollator20computeMaxExpansionsEPKNS_18CollationTailoringER10UErrorCode, .-_ZN6icu_6717RuleBasedCollator20computeMaxExpansionsEPKNS_18CollationTailoringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator15getReorderCodesEPiiR10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator15getReorderCodesEPiiR10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator15getReorderCodesEPiiR10UErrorCode:
.LFB3519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	subq	$8, %rsp
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L110
	testl	%edx, %edx
	js	.L112
	movq	%rdi, %rax
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	jne	.L113
	testl	%edx, %edx
	jle	.L113
.L112:
	movl	$1, (%rcx)
	xorl	%r12d, %r12d
.L110:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movq	16(%rax), %rax
	movl	72(%rax), %r12d
	testl	%r12d, %r12d
	je	.L110
	cmpl	%r12d, %edx
	jge	.L114
	movl	$15, (%rcx)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L114:
	leal	0(,%r12,4), %edx
	movq	64(%rax), %rsi
	movslq	%edx, %rdx
	call	memcpy@PLT
	jmp	.L110
	.cfi_endproc
.LFE3519:
	.size	_ZNK6icu_6717RuleBasedCollator15getReorderCodesEPiiR10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator15getReorderCodesEPiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationKeyByteSink6ResizeEii
	.type	_ZN6icu_6720CollationKeyByteSink6ResizeEii, @function
_ZN6icu_6720CollationKeyByteSink6ResizeEii:
.LFB3470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	$0, 8(%rdi)
	je	.L126
	movl	16(%rdi), %eax
	movl	$200, %ecx
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	leal	(%rax,%rax), %r12d
	leal	(%rdx,%rsi,2), %eax
	cmpl	$200, %eax
	cmovl	%ecx, %eax
	cmpl	%r12d, %eax
	cmovge	%eax, %r12d
	movl	%r12d, %esi
	call	_ZN6icu_6712CollationKey10reallocateEii@PLT
	testq	%rax, %rax
	je	.L131
	movq	%rax, 8(%rbx)
	movl	$1, %r13d
	movl	%r12d, 16(%rbx)
.L126:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	movq	$0, 8(%rbx)
	movl	%r13d, %eax
	movl	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3470:
	.size	_ZN6icu_6720CollationKeyByteSink6ResizeEii, .-_ZN6icu_6720CollationKeyByteSink6ResizeEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator12getAttributeE13UColAttributeR10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator12getAttributeE13UColAttributeR10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator12getAttributeE13UColAttributeR10UErrorCode:
.LFB3511:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L143
	cmpl	$7, %esi
	ja	.L134
	leaq	.L136(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L136:
	.long	.L142-.L136
	.long	.L141-.L136
	.long	.L140-.L136
	.long	.L139-.L136
	.long	.L144-.L136
	.long	.L137-.L136
	.long	.L145-.L136
	.long	.L135-.L136
	.text
.L145:
	movl	$16, %eax
.L132:
	ret
.L144:
	movl	$1, %eax
.L138:
	movq	16(%rdi), %rdx
	testl	%eax, 24(%rdx)
	setne	%al
	movzbl	%al, %eax
	addl	$16, %eax
	ret
.L139:
	movl	$1024, %eax
	jmp	.L138
.L142:
	movl	$2048, %eax
	jmp	.L138
.L141:
	movq	16(%rdi), %rax
	testb	$12, 24(%rax)
	sete	%al
	movzbl	%al, %eax
	addl	$20, %eax
	ret
.L140:
	movq	16(%rdi), %rax
	movl	24(%rax), %edx
	movl	$16, %eax
	andl	$768, %edx
	je	.L132
	xorl	%eax, %eax
	cmpl	$512, %edx
	setne	%al
	addl	$24, %eax
	ret
.L137:
	movq	16(%rdi), %rax
	movl	24(%rax), %eax
	sarl	$12, %eax
	ret
.L135:
	movl	$2, %eax
	jmp	.L138
.L134:
	movl	$1, (%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3511:
	.size	_ZNK6icu_6717RuleBasedCollator12getAttributeE13UColAttributeR10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator12getAttributeE13UColAttributeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationKeyByteSink20AppendBeyondCapacityEPKcii
	.type	_ZN6icu_6720CollationKeyByteSink20AppendBeyondCapacityEPKcii, @function
_ZN6icu_6720CollationKeyByteSink20AppendBeyondCapacityEPKcii:
.LFB3469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	leaq	_ZN6icu_6720CollationKeyByteSink6ResizeEii(%rip), %rdx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L152
	cmpq	$0, 8(%rdi)
	je	.L151
	movl	16(%rdi), %eax
	movl	$200, %edx
	movq	32(%rdi), %rdi
	leal	(%rax,%rax), %r13d
	leal	(%rcx,%r14,2), %eax
	cmpl	$200, %eax
	cmovl	%edx, %eax
	movl	%ecx, %edx
	cmpl	%r13d, %eax
	cmovge	%eax, %r13d
	movl	%r13d, %esi
	call	_ZN6icu_6712CollationKey10reallocateEii@PLT
	testq	%rax, %rax
	je	.L162
	movq	%rax, 8(%rbx)
	movl	%r13d, 16(%rbx)
.L156:
	addq	$8, %rsp
	movslq	%r12d, %rdi
	movslq	%r14d, %rdx
	movq	%r15, %rsi
	popq	%rbx
	addq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movq	$0, 8(%rbx)
	movl	$0, 16(%rbx)
.L151:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movl	%ecx, %edx
	movl	%r14d, %esi
	call	*%rax
	testb	%al, %al
	je	.L151
	movq	8(%rbx), %rax
	jmp	.L156
	.cfi_endproc
.LFE3469:
	.size	_ZN6icu_6720CollationKeyByteSink20AppendBeyondCapacityEPKcii, .-_ZN6icu_6720CollationKeyByteSink20AppendBeyondCapacityEPKcii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator5cloneEv
	.type	_ZNK6icu_6717RuleBasedCollator5cloneEv, @function
_ZNK6icu_6717RuleBasedCollator5cloneEv:
.LFB3487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$272, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L163
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_678CollatorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6717RuleBasedCollatorE(%rip), %rax
	leaq	40(%rbx), %rsi
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	leaq	40(%r12), %rdi
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
	movq	24(%rbx), %rax
	movq	%rax, 24(%r12)
	movq	32(%rbx), %rax
	movq	%rax, 32(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	264(%rbx), %eax
	movq	16(%r12), %rdi
	movl	%eax, 264(%r12)
	movzbl	268(%rbx), %eax
	movb	%al, 268(%r12)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	32(%r12), %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L163:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3487:
	.size	_ZNK6icu_6717RuleBasedCollator5cloneEv, .-_ZNK6icu_6717RuleBasedCollator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollator14setMaxVariableE15UColReorderCodeR10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollator14setMaxVariableE15UColReorderCodeR10UErrorCode, @function
_ZN6icu_6717RuleBasedCollator14setMaxVariableE15UColReorderCodeR10UErrorCode:
.LFB3513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L169
	movl	%esi, %r14d
	movq	%rdx, %rbx
	cmpl	$-1, %esi
	je	.L172
	leal	-4096(%rsi), %r13d
	cmpl	$3, %r13d
	ja	.L173
	movq	16(%rdi), %r15
	movl	24(%r15), %eax
	sarl	$4, %eax
	andl	$7, %eax
	cmpl	%eax, %r13d
	je	.L194
	movq	24(%rdi), %rax
	movq	%r15, %rdi
	movq	32(%rax), %rax
	movq	%rax, -56(%rbp)
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	cmpl	$1, %eax
	jle	.L195
	movl	$856, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L186
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN6icu_6717CollationSettingsC1ERKS0_@PLT
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-64(%rbp), %rax
	movb	$0, -64(%rbp)
	movq	%rax, %r15
.L181:
	movq	8(%r12), %rdi
	movl	%r14d, %esi
	call	_ZNK6icu_6713CollationData22getLastPrimaryForGroupEi@PLT
	movq	%rbx, %rcx
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	%eax, %r14d
	movq	-56(%rbp), %rax
	movl	24(%rax), %edx
	call	_ZN6icu_6717CollationSettings14setMaxVariableEiiR10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L169
	movl	%r14d, 28(%r15)
	movq	8(%r12), %rdi
	leaq	84(%r15), %rdx
	movq	%r15, %rsi
	movl	$384, %ecx
	call	_ZN6icu_6718CollationFastLatin10getOptionsEPKNS_13CollationDataERKNS_17CollationSettingsEPti@PLT
	cmpb	$0, -64(%rbp)
	movl	%eax, 80(%r15)
	movl	264(%r12), %eax
	jne	.L196
	orb	$1, %ah
.L178:
	movl	%eax, 264(%r12)
.L169:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movq	24(%rdi), %rax
	movq	16(%rdi), %r15
	movq	32(%rax), %rax
	movq	%rax, -56(%rbp)
	cmpq	%r15, %rax
	je	.L197
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	cmpl	$1, %eax
	jle	.L183
	movl	$856, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L198
.L186:
	movl	$7, (%rbx)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L194:
	orl	$256, 264(%rdi)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$1, (%rdx)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717CollationSettingsC1ERKS0_@PLT
	movq	%r15, %rdi
	movq	%r13, %r15
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	%r13, 16(%r12)
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L183:
	movq	-56(%rbp), %rax
	movl	$-1, %r13d
	movl	24(%rax), %eax
	movl	%eax, -64(%rbp)
	sarl	$4, %eax
	andl	$7, %eax
	movb	$1, -64(%rbp)
	leal	4096(%rax), %r14d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L195:
	movb	$0, -64(%rbp)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L196:
	andb	$-2, %ah
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L197:
	andl	$-257, 264(%r12)
	jmp	.L169
	.cfi_endproc
.LFE3513:
	.size	_ZN6icu_6717RuleBasedCollator14setMaxVariableE15UColReorderCodeR10UErrorCode, .-_ZN6icu_6717RuleBasedCollator14setMaxVariableE15UColReorderCodeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollator15setReorderCodesEPKiiR10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollator15setReorderCodesEPKiiR10UErrorCode, @function
_ZN6icu_6717RuleBasedCollator15setReorderCodesEPKiiR10UErrorCode:
.LFB3520:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L243
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testl	%edx, %edx
	js	.L201
	movq	%rdi, %rbx
	movq	%rsi, %r14
	testq	%rsi, %rsi
	jne	.L202
	testl	%edx, %edx
	jle	.L202
.L201:
	movl	$1, 0(%r13)
.L199:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movq	16(%rbx), %r15
	movl	72(%r15), %eax
	cmpl	$1, %r12d
	je	.L247
	cmpl	%eax, %r12d
	je	.L248
.L206:
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	cmpl	$1, %eax
	jle	.L209
	movl	$856, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L208
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_6717CollationSettingsC1ERKS0_@PLT
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-56(%rbp), %rax
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-56(%rbp), %rax
	movq	%rax, %r15
.L209:
	movq	8(%rbx), %rsi
	movq	%r13, %r8
	movl	%r12d, %ecx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6717CollationSettings13setReorderingERKNS_13CollationDataEPKiiR10UErrorCode@PLT
.L246:
	movq	8(%rbx), %rdi
	leaq	84(%r15), %rdx
	movq	%r15, %rsi
	movl	$384, %ecx
	call	_ZN6icu_6718CollationFastLatin10getOptionsEPKNS_13CollationDataERKNS_17CollationSettingsEPti@PLT
	movl	%eax, 80(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	(%r14), %ecx
	cmpl	$103, %ecx
	je	.L249
	cmpl	$1, %eax
	je	.L250
.L241:
	movq	24(%rbx), %rax
	movl	$1, %r12d
	movq	32(%rax), %rax
	movq	%rax, -56(%rbp)
	cmpl	$-1, %ecx
	jne	.L206
	cmpq	%r15, -56(%rbp)
	je	.L199
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	cmpl	$1, %eax
	jle	.L207
	movl	$856, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L208
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6717CollationSettingsC1ERKS0_@PLT
	movq	%r15, %rdi
	movq	%r12, %r15
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	%r12, 16(%rbx)
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L207:
	movq	-56(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6717CollationSettings18copyReorderingFromERKS0_R10UErrorCode@PLT
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L248:
	leal	0(,%r12,4), %edx
	movq	64(%r15), %rsi
	movq	%r14, %rdi
	movslq	%edx, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L206
	jmp	.L199
.L208:
	movl	$7, 0(%r13)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L249:
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jne	.L206
	jmp	.L199
.L250:
	movq	64(%r15), %rsi
	movl	$4, %edx
	movq	%r14, %rdi
	movl	%ecx, -56(%rbp)
	call	memcmp@PLT
	movl	-56(%rbp), %ecx
	testl	%eax, %eax
	jne	.L241
	jmp	.L199
	.cfi_endproc
.LFE3520:
	.size	_ZN6icu_6717RuleBasedCollator15setReorderCodesEPKiiR10UErrorCode, .-_ZN6icu_6717RuleBasedCollator15setReorderCodesEPKiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollator14setVariableTopEjR10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollator14setVariableTopEjR10UErrorCode, @function
_ZN6icu_6717RuleBasedCollator14setVariableTopEjR10UErrorCode:
.LFB3518:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L263
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movl	28(%rax), %eax
	cmpl	%eax, %esi
	jne	.L266
.L254:
	movq	24(%rbx), %rcx
	movl	264(%rbx), %edx
	movq	32(%rcx), %rcx
	cmpl	%eax, 28(%rcx)
	je	.L267
	orb	$1, %dh
	movl	%edx, 264(%rbx)
.L251:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movq	%rdx, %r12
	call	_ZNK6icu_6713CollationData18getGroupForPrimaryEj@PLT
	leal	-4096(%rax), %r13d
	movl	%eax, %esi
	cmpl	$3, %r13d
	jbe	.L255
	movl	$1, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	8(%rbx), %rdi
	call	_ZNK6icu_6713CollationData22getLastPrimaryForGroupEi@PLT
	movq	16(%rbx), %r15
	movl	%eax, %r14d
	movl	28(%r15), %eax
	cmpl	%eax, %r14d
	je	.L254
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	cmpl	$1, %eax
	jle	.L256
	movl	$856, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L257
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_6717CollationSettingsC1ERKS0_@PLT
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-56(%rbp), %rax
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-56(%rbp), %rax
	movq	%rax, %r15
.L256:
	movq	24(%rbx), %rax
	movq	%r12, %rcx
	movl	%r13d, %esi
	movq	%r15, %rdi
	movq	32(%rax), %rax
	movl	24(%rax), %edx
	call	_ZN6icu_6717CollationSettings14setMaxVariableEiiR10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L251
	movl	%r14d, 28(%r15)
	movq	8(%rbx), %rdi
	leaq	84(%r15), %rdx
	movq	%r15, %rsi
	movl	$384, %ecx
	call	_ZN6icu_6718CollationFastLatin10getOptionsEPKNS_13CollationDataERKNS_17CollationSettingsEPti@PLT
	movl	%eax, 80(%r15)
	movl	%r14d, %eax
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L267:
	andb	$-2, %dh
	movl	%edx, 264(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L257:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L251
	.cfi_endproc
.LFE3518:
	.size	_ZN6icu_6717RuleBasedCollator14setVariableTopEjR10UErrorCode, .-_ZN6icu_6717RuleBasedCollator14setVariableTopEjR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollator12setAttributeE13UColAttribute18UColAttributeValueR10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollator12setAttributeE13UColAttribute18UColAttributeValueR10UErrorCode, @function
_ZN6icu_6717RuleBasedCollator12setAttributeE13UColAttribute18UColAttributeValueR10UErrorCode:
.LFB3512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	leaq	_ZNK6icu_6717RuleBasedCollator12getAttributeE13UColAttributeR10UErrorCode(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	192(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L269
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jg	.L268
	cmpl	$7, %esi
	ja	.L323
	leaq	.L273(%rip), %rcx
	movl	%esi, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L273:
	.long	.L280-.L273
	.long	.L279-.L273
	.long	.L278-.L273
	.long	.L277-.L273
	.long	.L300-.L273
	.long	.L275-.L273
	.long	.L301-.L273
	.long	.L272-.L273
	.text
.L301:
	movl	$16, %eax
.L274:
	cmpl	%eax, %r14d
	je	.L325
.L282:
	movq	24(%rbx), %rax
	movq	16(%rbx), %r8
	movq	32(%rax), %rax
	movq	%rax, -64(%rbp)
	cmpq	%r8, %rax
	jne	.L284
	cmpl	$-1, %r14d
	je	.L326
.L284:
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	movq	-56(%rbp), %r8
	cmpl	$1, %eax
	jle	.L285
	movl	$856, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L288
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6717CollationSettingsC1ERKS0_@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	%r15, 16(%rbx)
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L287:
	cmpl	$7, %r12d
	ja	.L323
	leaq	.L290(%rip), %rcx
	movl	%r12d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L290:
	.long	.L297-.L290
	.long	.L296-.L290
	.long	.L295-.L290
	.long	.L294-.L290
	.long	.L293-.L290
	.long	.L292-.L290
	.long	.L291-.L290
	.long	.L289-.L290
	.text
.L300:
	movl	$1, %eax
.L276:
	movq	16(%rbx), %rdx
	testl	%eax, 24(%rdx)
	setne	%al
	movzbl	%al, %eax
	addl	$16, %eax
	cmpl	%eax, %r14d
	jne	.L282
.L325:
	movl	$1, %eax
	movl	%r12d, %ecx
	sall	%cl, %eax
	orl	%eax, 264(%rbx)
.L268:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L277:
	.cfi_restore_state
	movl	$1024, %eax
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%rcx, %rdx
	call	*%rax
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L274
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L275:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movl	24(%rax), %eax
	sarl	$12, %eax
	jmp	.L274
.L272:
	movl	$2, %eax
	jmp	.L276
.L280:
	movl	$2048, %eax
	jmp	.L276
.L279:
	movq	16(%rdi), %rax
	testb	$12, 24(%rax)
	sete	%al
	movzbl	%al, %eax
	addl	$20, %eax
	jmp	.L274
.L278:
	movq	16(%rdi), %rax
	movl	24(%rax), %edx
	movl	$16, %eax
	andl	$768, %edx
	je	.L274
	xorl	%eax, %eax
	cmpl	$512, %edx
	setne	%al
	addl	$24, %eax
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L285:
	testq	%r8, %r8
	je	.L288
	movq	%r8, %r15
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L291:
	leal	-16(%r14), %eax
	cmpl	$1, %eax
	jbe	.L298
	cmpl	$-1, %r14d
	je	.L298
.L323:
	movl	$1, 0(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	movl	$7, 0(%r13)
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L297:
	movq	-64(%rbp), %rax
	movq	%r13, %r8
	movl	%r14d, %edx
	movl	$2048, %esi
	movq	%r15, %rdi
	movl	24(%rax), %ecx
	call	_ZN6icu_6717CollationSettings7setFlagEi18UColAttributeValueiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L298:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L268
	movq	8(%rbx), %rdi
	movl	$384, %ecx
	leaq	84(%r15), %rdx
	movq	%r15, %rsi
	call	_ZN6icu_6718CollationFastLatin10getOptionsEPKNS_13CollationDataERKNS_17CollationSettingsEPti@PLT
	movl	%r12d, %ecx
	movl	%eax, 80(%r15)
	movl	$1, %eax
	sall	%cl, %eax
	cmpl	$-1, %r14d
	je	.L327
	orl	%eax, 264(%rbx)
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L296:
	movq	-64(%rbp), %rax
	movq	%r13, %rcx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	24(%rax), %edx
	call	_ZN6icu_6717CollationSettings20setAlternateHandlingE18UColAttributeValueiR10UErrorCode@PLT
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L293:
	movq	-64(%rbp), %rax
	movq	%r13, %r8
	movl	%r14d, %edx
	movl	$1, %esi
	movq	%r15, %rdi
	movl	24(%rax), %ecx
	call	_ZN6icu_6717CollationSettings7setFlagEi18UColAttributeValueiR10UErrorCode@PLT
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L292:
	movq	-64(%rbp), %rax
	movq	%r13, %rcx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	24(%rax), %edx
	call	_ZN6icu_6717CollationSettings11setStrengthEiiR10UErrorCode@PLT
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L289:
	movq	-64(%rbp), %rax
	movq	%r13, %r8
	movl	%r14d, %edx
	movl	$2, %esi
	movq	%r15, %rdi
	movl	24(%rax), %ecx
	call	_ZN6icu_6717CollationSettings7setFlagEi18UColAttributeValueiR10UErrorCode@PLT
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L295:
	movq	-64(%rbp), %rax
	movq	%r13, %rcx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	24(%rax), %edx
	call	_ZN6icu_6717CollationSettings12setCaseFirstE18UColAttributeValueiR10UErrorCode@PLT
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L294:
	movq	-64(%rbp), %rax
	movq	%r13, %r8
	movl	%r14d, %edx
	movl	$1024, %esi
	movq	%r15, %rdi
	movl	24(%rax), %ecx
	call	_ZN6icu_6717CollationSettings7setFlagEi18UColAttributeValueiR10UErrorCode@PLT
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L327:
	notl	%eax
	andl	%eax, 264(%rbx)
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L326:
	movl	$-2, %eax
	movl	%r12d, %ecx
	roll	%cl, %eax
	andl	%eax, 264(%rbx)
	jmp	.L268
	.cfi_endproc
.LFE3512:
	.size	_ZN6icu_6717RuleBasedCollator12setAttributeE13UColAttribute18UColAttributeValueR10UErrorCode, .-_ZN6icu_6717RuleBasedCollator12setAttributeE13UColAttribute18UColAttributeValueR10UErrorCode
	.section	.rodata.str1.1
.LC2:
	.string	"root"
.LC3:
	.string	"collation"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"1234...........IXO..SN..LU......"
	.text
	.align 2
	.p2align 4
	.type	_ZNK6icu_6717RuleBasedCollator32internalGetShortDefinitionStringEPKcPciR10UErrorCode.part.0, @function
_ZNK6icu_6717RuleBasedCollator32internalGetShortDefinitionStringEPKcPciR10UErrorCode.part.0:
.LFB4900:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -424(%rbp)
	movl	%ecx, -412(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L434
.L329:
	leaq	-224(%rbp), %r15
	movq	%r12, %r9
	xorl	%r8d, %r8d
	movq	%r10, %rcx
	leaq	.LC3(%rip), %rdx
	movl	$157, %esi
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	ucol_getFunctionalEquivalent_67@PLT
	movl	(%r12), %r11d
	testl	%r11d, %r11d
	jg	.L328
	cltq
	xorl	%r10d, %r10d
	movl	$0, -344(%rbp)
	leaq	-400(%rbp), %rbx
	movb	$0, -224(%rbp,%rax)
	leaq	-387(%rbp), %rax
	movq	%rax, -400(%rbp)
	movl	264(%r13), %eax
	movl	$40, -392(%rbp)
	movw	%r10w, -388(%rbp)
	testb	$2, %al
	je	.L331
	movq	0(%r13), %rax
	leaq	_ZNK6icu_6717RuleBasedCollator12getAttributeE13UColAttributeR10UErrorCode(%rip), %rdx
	movq	192(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L332
	movq	16(%r13), %rax
	xorl	%r14d, %r14d
	testb	$12, 24(%rax)
	sete	%r14b
	addq	$20, %r14
.L335:
	movq	%r12, %rdx
	movl	$65, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	leaq	.LC4(%rip), %rax
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movsbl	(%rax,%r14), %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
.L429:
	movl	264(%r13), %eax
.L331:
	testb	$4, %al
	je	.L336
	movq	0(%r13), %rdx
	movq	192(%rdx), %rcx
	leaq	_ZNK6icu_6717RuleBasedCollator12getAttributeE13UColAttributeR10UErrorCode(%rip), %rdx
	cmpq	%rdx, %rcx
	jne	.L337
	movl	(%r12), %r8d
	testl	%r8d, %r8d
	jg	.L336
	movq	16(%r13), %rax
	movl	$16, %r14d
	movl	24(%rax), %eax
	andl	$768, %eax
	jne	.L435
.L402:
	movl	-344(%rbp), %esi
	testl	%esi, %esi
	jne	.L436
.L340:
	movq	%r12, %rdx
	movl	$67, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	leaq	.LC4(%rip), %rax
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movsbl	(%rax,%r14), %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
.L430:
	movl	264(%r13), %eax
.L336:
	testb	%al, %al
	jns	.L341
	movq	0(%r13), %rdx
	movq	192(%rdx), %rcx
	leaq	_ZNK6icu_6717RuleBasedCollator12getAttributeE13UColAttributeR10UErrorCode(%rip), %rdx
	cmpq	%rdx, %rcx
	jne	.L342
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L341
	movq	16(%r13), %rax
	xorl	%r14d, %r14d
	testb	$2, 24(%rax)
	setne	%r14b
	addq	$16, %r14
.L401:
	movl	-344(%rbp), %eax
	testl	%eax, %eax
	jne	.L437
.L345:
	movq	%r12, %rdx
	movl	$68, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	leaq	.LC4(%rip), %rax
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movsbl	(%rax,%r14), %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
.L431:
	movl	264(%r13), %eax
.L341:
	testb	$8, %al
	je	.L346
	movq	0(%r13), %rdx
	movq	192(%rdx), %rcx
	leaq	_ZNK6icu_6717RuleBasedCollator12getAttributeE13UColAttributeR10UErrorCode(%rip), %rdx
	cmpq	%rdx, %rcx
	jne	.L347
	movl	(%r12), %r14d
	testl	%r14d, %r14d
	jg	.L346
	movq	16(%r13), %rax
	xorl	%r14d, %r14d
	testb	$4, 25(%rax)
	setne	%r14b
	addq	$16, %r14
.L400:
	movl	-344(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L438
.L350:
	movq	%r12, %rdx
	movl	$69, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	leaq	.LC4(%rip), %rax
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movsbl	(%rax,%r14), %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
.L432:
	movl	264(%r13), %eax
.L346:
	testb	$1, %al
	je	.L352
	movq	0(%r13), %rax
	leaq	_ZNK6icu_6717RuleBasedCollator12getAttributeE13UColAttributeR10UErrorCode(%rip), %rdx
	movq	192(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L353
	movl	(%r12), %r9d
	testl	%r9d, %r9d
	jg	.L352
	movq	16(%r13), %rax
	xorl	%r14d, %r14d
	testb	$8, 25(%rax)
	setne	%r14b
	addq	$16, %r14
.L399:
	movl	-344(%rbp), %edi
	testl	%edi, %edi
	jne	.L439
.L356:
	movq	%r12, %rdx
	movl	$70, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	leaq	.LC4(%rip), %rax
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movsbl	(%rax,%r14), %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
.L352:
	leaq	-336(%rbp), %r14
	movq	%r12, %r8
	movl	$100, %ecx
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdx
	call	uloc_getKeywordValue_67@PLT
	movl	(%r12), %esi
	movl	%eax, %r8d
	testl	%esi, %esi
	jg	.L362
	testl	%eax, %eax
	je	.L362
	movl	-344(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L440
.L361:
	movq	%r12, %rdx
	movl	$75, %esi
	movq	%rbx, %rdi
	movl	%r8d, -408(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-408(%rbp), %r8d
	testl	%r8d, %r8d
	jle	.L362
	leal	-1(%r8), %eax
	movq	%r14, %rcx
	leaq	-335(%rbp,%rax), %rax
	movq	%rax, -432(%rbp)
	.p2align 4,,10
	.p2align 3
.L363:
	movsbl	(%rcx), %edi
	movq	%rcx, -408(%rbp)
	call	uprv_toupper_67@PLT
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movsbl	%al, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-408(%rbp), %rcx
	addq	$1, %rcx
	cmpq	%rcx, -432(%rbp)
	jne	.L363
.L362:
	movq	%r12, %rcx
	movl	$100, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	uloc_getLanguage_67@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jne	.L441
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L368
	movl	-344(%rbp), %eax
	testl	%eax, %eax
	jne	.L442
.L367:
	movq	%r12, %rdx
	movl	$76, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	leaq	.LC2(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L369:
	movsbl	(%rcx), %edi
	movq	%rcx, -408(%rbp)
	call	uprv_toupper_67@PLT
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movsbl	%al, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-408(%rbp), %rcx
	leaq	4+.LC2(%rip), %rax
	addq	$1, %rcx
	cmpq	%rcx, %rax
	jne	.L369
.L368:
	testb	$16, 264(%r13)
	je	.L372
	movq	0(%r13), %rax
	leaq	_ZNK6icu_6717RuleBasedCollator12getAttributeE13UColAttributeR10UErrorCode(%rip), %rdx
	movq	192(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L373
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L372
	movq	16(%r13), %rax
	movl	24(%rax), %ecx
	andl	$1, %ecx
	addl	$16, %ecx
.L374:
	movl	-344(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L443
.L375:
	movq	%r12, %rdx
	movl	$78, %esi
	movq	%rbx, %rdi
	movl	%ecx, -408(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movslq	-408(%rbp), %rcx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	leaq	.LC4(%rip), %rax
	movsbl	(%rax,%rcx), %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
.L372:
	movq	%r12, %rcx
	movl	$100, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	uloc_getCountry_67@PLT
	movl	(%r12), %r10d
	movl	%eax, %r8d
	testl	%r10d, %r10d
	jg	.L381
	testl	%eax, %eax
	je	.L381
	movl	-344(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L444
.L380:
	movq	%r12, %rdx
	movl	$82, %esi
	movq	%rbx, %rdi
	movl	%r8d, -408(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-408(%rbp), %r8d
	testl	%r8d, %r8d
	jle	.L381
	leal	-1(%r8), %eax
	movq	%r14, %rcx
	leaq	-335(%rbp,%rax), %rax
	movq	%rax, -432(%rbp)
	.p2align 4,,10
	.p2align 3
.L382:
	movsbl	(%rcx), %edi
	movq	%rcx, -408(%rbp)
	call	uprv_toupper_67@PLT
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movsbl	%al, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-408(%rbp), %rcx
	addq	$1, %rcx
	cmpq	%rcx, -432(%rbp)
	jne	.L382
.L381:
	testb	$32, 264(%r13)
	je	.L383
	movq	0(%r13), %rax
	leaq	_ZNK6icu_6717RuleBasedCollator12getAttributeE13UColAttributeR10UErrorCode(%rip), %rdx
	movq	192(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L384
	movl	(%r12), %r8d
	testl	%r8d, %r8d
	jg	.L383
	movq	16(%r13), %rax
	movl	24(%rax), %r13d
	sarl	$12, %r13d
.L385:
	movl	-344(%rbp), %esi
	testl	%esi, %esi
	jne	.L445
.L386:
	movq	%r12, %rdx
	movl	$83, %esi
	movq	%rbx, %rdi
	movslq	%r13d, %r13
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	leaq	.LC4(%rip), %rax
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movsbl	(%rax,%r13), %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
.L383:
	movq	%r12, %rcx
	movl	$100, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	uloc_getVariant_67@PLT
	movl	(%r12), %ecx
	movl	%eax, %r13d
	testl	%ecx, %ecx
	jg	.L392
	testl	%eax, %eax
	je	.L392
	movl	-344(%rbp), %edx
	testl	%edx, %edx
	jne	.L446
.L391:
	movq	%r12, %rdx
	movl	$86, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	testl	%r13d, %r13d
	jle	.L392
	leal	-1(%r13), %eax
	movq	%r14, %rcx
	leaq	-335(%rbp,%rax), %r13
	.p2align 4,,10
	.p2align 3
.L393:
	movsbl	(%rcx), %edi
	movq	%rcx, -408(%rbp)
	call	uprv_toupper_67@PLT
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movsbl	%al, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-408(%rbp), %rcx
	addq	$1, %rcx
	cmpq	%r13, %rcx
	jne	.L393
.L392:
	movq	%r12, %rcx
	movl	$100, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	uloc_getScript_67@PLT
	movl	%eax, %r13d
	movl	(%r12), %eax
	testl	%r13d, %r13d
	je	.L389
	testl	%eax, %eax
	jg	.L389
	movl	-344(%rbp), %eax
	testl	%eax, %eax
	jne	.L447
.L394:
	movq	%r12, %rdx
	movl	$90, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	testl	%r13d, %r13d
	jle	.L433
	leal	-1(%r13), %eax
	leaq	-335(%rbp,%rax), %r13
	.p2align 4,,10
	.p2align 3
.L396:
	movsbl	(%r14), %edi
	addq	$1, %r14
	call	uprv_toupper_67@PLT
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movsbl	%al, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	cmpq	%r14, %r13
	jne	.L396
.L433:
	movl	(%r12), %eax
.L389:
	xorl	%r14d, %r14d
	testl	%eax, %eax
	jg	.L397
	movl	-344(%rbp), %r13d
	cmpl	-412(%rbp), %r13d
	jle	.L448
.L398:
	movl	-412(%rbp), %esi
	movq	-424(%rbp), %rdi
	movq	%r12, %rcx
	movl	%r13d, %edx
	call	u_terminateChars_67@PLT
	movl	%eax, %r14d
.L397:
	cmpb	$0, -388(%rbp)
	jne	.L449
.L328:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L450
	addq	$392, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L368
	movl	-344(%rbp), %eax
	testl	%eax, %eax
	jne	.L451
.L370:
	movq	%r12, %rdx
	movl	$76, %esi
	movq	%rbx, %rdi
	movl	%r8d, -408(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-408(%rbp), %r8d
	testl	%r8d, %r8d
	jle	.L368
	leal	-1(%r8), %eax
	movq	%r14, %rcx
	leaq	-335(%rbp,%rax), %rax
	movq	%rax, -432(%rbp)
	.p2align 4,,10
	.p2align 3
.L371:
	movsbl	(%rcx), %edi
	movq	%rcx, -408(%rbp)
	call	uprv_toupper_67@PLT
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movsbl	%al, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-408(%rbp), %rcx
	addq	$1, %rcx
	cmpq	%rcx, -432(%rbp)
	jne	.L371
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L434:
	cmpb	$0, 256(%rdi)
	jne	.L329
	movq	80(%rdi), %r10
	leaq	.LC2(%rip), %rax
	cmpb	$0, (%r10)
	cmove	%rax, %r10
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L449:
	movq	-400(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L435:
	xorl	%r14d, %r14d
	cmpl	$512, %eax
	setne	%r14b
	addq	$24, %r14
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L444:
	movq	%r12, %rdx
	movl	$95, %esi
	movq	%rbx, %rdi
	movl	%eax, -408(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-408(%rbp), %r8d
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L447:
	movq	%r12, %rdx
	movl	$95, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L446:
	movq	%r12, %rdx
	movl	$95, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L440:
	movq	%r12, %rdx
	movl	$95, %esi
	movq	%rbx, %rdi
	movl	%eax, -408(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-408(%rbp), %r8d
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%r12, %rdx
	movl	$95, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%r12, %rdx
	movl	$95, %esi
	movq	%rbx, %rdi
	movl	%ecx, -408(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-408(%rbp), %ecx
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L439:
	movq	%r12, %rdx
	movl	$95, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L438:
	movq	%r12, %rdx
	movl	$95, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L437:
	movq	%r12, %rdx
	movl	$95, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L332:
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	*%rax
	movl	(%r12), %r9d
	testl	%r9d, %r9d
	jg	.L429
	cmpl	$0, -344(%rbp)
	movslq	%eax, %r14
	je	.L335
	movq	%r12, %rdx
	movl	$95, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L384:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	*%rax
	movl	(%r12), %edi
	movl	%eax, %r13d
	testl	%edi, %edi
	jg	.L383
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L347:
	movq	%r12, %rdx
	movl	$3, %esi
	movq	%r13, %rdi
	call	*%rcx
	movl	(%r12), %r11d
	testl	%r11d, %r11d
	jg	.L432
	movslq	%eax, %r14
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L342:
	movq	%r12, %rdx
	movl	$7, %esi
	movq	%r13, %rdi
	call	*%rcx
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L431
	movslq	%eax, %r14
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L337:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$2, %esi
	call	*%rcx
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L430
	movslq	%eax, %r14
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L373:
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%r13, %rdi
	call	*%rax
	movl	%eax, %ecx
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L372
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L353:
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	*%rax
	movl	(%r12), %r8d
	testl	%r8d, %r8d
	jg	.L352
	movslq	%eax, %r14
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L448:
	movq	-400(%rbp), %rsi
	movq	-424(%rbp), %rdi
	movslq	%r13d, %rdx
	call	memcpy@PLT
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L442:
	movq	%r12, %rdx
	movl	$95, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L451:
	movq	%r12, %rdx
	movl	$95, %esi
	movq	%rbx, %rdi
	movl	%r8d, -408(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-408(%rbp), %r8d
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L436:
	movq	%r12, %rdx
	movl	$95, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	jmp	.L340
.L450:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4900:
	.size	_ZNK6icu_6717RuleBasedCollator32internalGetShortDefinitionStringEPKcPciR10UErrorCode.part.0, .-_ZNK6icu_6717RuleBasedCollator32internalGetShortDefinitionStringEPKcPciR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator32internalGetShortDefinitionStringEPKcPciR10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator32internalGetShortDefinitionStringEPKcPciR10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator32internalGetShortDefinitionStringEPKcPciR10UErrorCode:
.LFB3590:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L452
	movl	%ecx, %eax
	shrl	$31, %eax
	testq	%rdx, %rdx
	je	.L458
	testb	%al, %al
	jne	.L459
.L456:
	jmp	_ZNK6icu_6717RuleBasedCollator32internalGetShortDefinitionStringEPKcPciR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$1, (%r8)
.L452:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	testl	%ecx, %ecx
	setne	%al
	testb	%al, %al
	je	.L456
	jmp	.L459
	.cfi_endproc
.LFE3590:
	.size	_ZNK6icu_6717RuleBasedCollator32internalGetShortDefinitionStringEPKcPciR10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator32internalGetShortDefinitionStringEPKcPciR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6717RuleBasedCollator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode.part.0, @function
_ZNK6icu_6717RuleBasedCollator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode.part.0:
.LFB4904:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$696, %rsp
	movq	%rdi, -696(%rbp)
	movq	%rbx, %rdi
	movq	%rcx, -704(%rbp)
	movq	%r9, -680(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE(%rip), %rax
	movl	%r8d, -624(%rbp)
	movq	%rax, -640(%rbp)
	movl	4(%rdx), %eax
	xorl	%edx, %edx
	movq	%rcx, -632(%rbp)
	movl	$0, -620(%rbp)
	movl	%eax, -616(%rbp)
	call	*40(%rbx)
	movl	(%r12), %r8d
	cmpl	$5, %r8d
	jg	.L461
	movq	16(%r14), %rdx
	leaq	-640(%rbp), %r11
	movq	.LC5(%rip), %xmm0
	leaq	-544(%rbp), %r15
	movl	-624(%rbp), %eax
	addl	-616(%rbp), %eax
	movq	%r11, %xmm1
	movq	%r11, -688(%rbp)
	movl	24(%rdx), %edi
	subl	-620(%rbp), %eax
	punpcklqdq	%xmm1, %xmm0
	movl	$1, -656(%rbp)
	movl	%eax, -652(%rbp)
	movq	8(%r14), %rax
	movl	%edi, %ecx
	movaps	%xmm0, -672(%rbp)
	shrl	%ecx
	movq	(%rax), %r9
	movq	72(%rax), %rsi
	movq	%rax, -528(%rbp)
	movl	$0, -520(%rbp)
	andl	$1, %ecx
	andl	$1, %edi
	movq	%r9, -536(%rbp)
	je	.L501
	movb	%cl, -156(%rbp)
	leaq	-496(%rbp), %rdi
	leaq	16+_ZTVN6icu_6725FCDUIterCollationIteratorE(%rip), %rcx
	movb	$0, -500(%rbp)
	leaq	-672(%rbp), %r14
	movq	$0, -144(%rbp)
	movq	48(%rax), %rax
	movq	%r14, %r9
	pushq	-680(%rbp)
	pushq	$0
	movq	%rdi, -512(%rbp)
	movl	$2, %edi
	movq	%rcx, -544(%rbp)
	movq	-688(%rbp), %rcx
	movq	%rax, -128(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%di, -112(%rbp)
	movq	%r15, %rdi
	movq	%rax, -120(%rbp)
	movl	$40, -504(%rbp)
	movl	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movl	$-1, -160(%rbp)
	movq	%rbx, -152(%rbp)
	call	_ZN6icu_6713CollationKeys26writeSortKeyUpToQuaternaryERNS_17CollationIteratorEPKaRKNS_17CollationSettingsERNS_15SortKeyByteSinkENS_9Collation5LevelERNS0_13LevelCallbackEaR10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6725FCDUIterCollationIteratorD1Ev@PLT
	movq	-680(%rbp), %rax
	popq	%r8
	popq	%r9
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L491
.L506:
	cmpl	-620(%rbp), %r13d
	jl	.L502
	movq	-696(%rbp), %rax
	movq	16(%rax), %rax
	movl	24(%rax), %eax
	sarl	$12, %eax
	cmpl	$15, %eax
	je	.L503
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_117PartLevelCallbackE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN6icu_6713CollationKeys13LevelCallbackD2Ev@PLT
.L468:
	movq	$7, (%r12)
	movl	-620(%rbp), %r12d
	cmpl	%r12d, %r13d
	jle	.L469
	movl	%r12d, %eax
	movslq	%r12d, %rdi
	xorl	%esi, %esi
	addq	-704(%rbp), %rdi
	notl	%eax
	leal	(%rax,%r13), %edx
	addq	$1, %rdx
	call	memset@PLT
.L469:
	movq	-688(%rbp), %rdi
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE(%rip), %rax
	movq	%rax, -640(%rbp)
	call	_ZN6icu_6715SortKeyByteSinkD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L504
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	leaq	-640(%rbp), %rax
	movq	%rax, -688(%rbp)
	cmpl	$6, %r8d
	jne	.L468
.L467:
	movl	-616(%rbp), %eax
	movl	$2, %ecx
	leaq	-608(%rbp), %r14
	leaq	-672(%rbp), %r15
	movw	%cx, -600(%rbp)
	movl	%eax, -708(%rbp)
	movl	-624(%rbp), %eax
	movl	%eax, -712(%rbp)
	movl	-620(%rbp), %eax
	movl	%eax, -716(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -608(%rbp)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L505:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movw	%ax, -672(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L471:
	movq	%rbx, %rdi
	call	*72(%rbx)
	testl	%eax, %eax
	jns	.L505
	movswl	-600(%rbp), %eax
	testb	$17, %al
	jne	.L492
	leaq	-598(%rbp), %rbx
	testb	$2, %al
	cmove	-584(%rbp), %rbx
.L472:
	testw	%ax, %ax
	js	.L473
	sarl	$5, %eax
.L474:
	cltq
	movq	-680(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	leaq	(%rbx,%rax,2), %r15
	movq	-696(%rbp), %rax
	movq	%r15, %rdx
	movq	8(%rax), %rax
	movq	48(%rax), %rdi
	call	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_PNS_16ReorderingBufferER10UErrorCode@PLT
	movq	%rax, %r10
	movq	-680(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L497
	movl	-616(%rbp), %eax
	testl	%eax, %eax
	jle	.L476
	subl	$1, %eax
	movl	%eax, -616(%rbp)
.L477:
	xorl	%r11d, %r11d
	cmpq	%r10, %rbx
	je	.L479
	movq	%r10, %rdx
	movq	-688(%rbp), %rcx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	subq	%rbx, %rdx
	movq	%r10, -728(%rbp)
	sarq	%rdx
	call	u_writeIdenticalLevelRun_67@PLT
	movq	-728(%rbp), %r10
	movl	%eax, %r11d
.L479:
	testq	%r15, %r15
	je	.L480
	cmpq	%r10, %r15
	je	.L500
	movq	%r15, %r8
	subq	%r10, %r8
	sarq	%r8
.L482:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	-680(%rbp), %r9
	movq	%r15, %rdx
	movq	%r10, %rsi
	movq	%rax, -544(%rbp)
	movl	$2, %eax
	leaq	-544(%rbp), %rbx
	movw	%ax, -536(%rbp)
	movq	-696(%rbp), %rax
	movq	%rbx, %rcx
	movl	%r11d, -728(%rbp)
	movq	8(%rax), %rax
	movq	48(%rax), %rdi
	call	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_RNS_13UnicodeStringEiR10UErrorCode@PLT
	movzwl	-536(%rbp), %eax
	movl	-728(%rbp), %r11d
	testw	%ax, %ax
	js	.L484
	movswl	%ax, %edx
	sarl	$5, %edx
.L485:
	testb	$17, %al
	jne	.L495
	leaq	-534(%rbp), %rsi
	testb	$2, %al
	cmove	-520(%rbp), %rsi
.L486:
	movq	-688(%rbp), %rcx
	movl	%r11d, %edi
	call	u_writeIdenticalLevelRun_67@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L500:
	movq	-680(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L497
	cmpl	-620(%rbp), %r13d
	jge	.L489
	movl	-708(%rbp), %eax
	addl	-712(%rbp), %eax
	movl	$6, (%r12)
	subl	-716(%rbp), %eax
	movl	%eax, 4(%r12)
	movl	%r13d, %r12d
.L488:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L501:
	pushq	-680(%rbp)
	leaq	-496(%rbp), %rax
	leaq	-672(%rbp), %r14
	movq	%r15, %rdi
	pushq	$0
	movq	%r14, %r9
	movq	%rax, -512(%rbp)
	leaq	16+_ZTVN6icu_6722UIterCollationIteratorE(%rip), %rax
	movb	%cl, -156(%rbp)
	movq	%r11, %rcx
	movq	%rax, -544(%rbp)
	movl	$40, -504(%rbp)
	movb	$0, -500(%rbp)
	movl	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movl	$-1, -160(%rbp)
	movq	%rbx, -152(%rbp)
	call	_ZN6icu_6713CollationKeys26writeSortKeyUpToQuaternaryERNS_17CollationIteratorEPKaRKNS_17CollationSettingsERNS_15SortKeyByteSinkENS_9Collation5LevelERNS0_13LevelCallbackEaR10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6722UIterCollationIteratorD1Ev@PLT
	movq	-680(%rbp), %rax
	popq	%r10
	popq	%r11
	movl	(%rax), %esi
	testl	%esi, %esi
	jle	.L506
.L491:
	xorl	%r12d, %r12d
.L464:
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_117PartLevelCallbackE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN6icu_6713CollationKeys13LevelCallbackD2Ev@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L502:
	movq	-656(%rbp), %rax
	movq	%rax, (%r12)
	movl	%r13d, %r12d
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L497:
	xorl	%r12d, %r12d
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L473:
	movl	-596(%rbp), %eax
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L489:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L503:
	movq	%rbx, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	*40(%rbx)
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_117PartLevelCallbackE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN6icu_6713CollationKeys13LevelCallbackD2Ev@PLT
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L476:
	movslq	-620(%rbp), %rax
	cmpl	-624(%rbp), %eax
	jge	.L478
	movq	-632(%rbp), %rdx
	movb	$1, (%rdx,%rax)
	movl	-620(%rbp), %eax
.L478:
	addl	$1, %eax
	movl	%eax, -620(%rbp)
	jmp	.L477
.L492:
	xorl	%ebx, %ebx
	jmp	.L472
.L484:
	movl	-532(%rbp), %edx
	jmp	.L485
.L480:
	cmpw	$0, (%r10)
	je	.L500
	movl	$-1, %r8d
	jmp	.L482
.L495:
	xorl	%esi, %esi
	jmp	.L486
.L504:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4904:
	.size	_ZNK6icu_6717RuleBasedCollator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode.part.0, .-_ZNK6icu_6717RuleBasedCollator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode:
.LFB3586:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L507
	testq	%rsi, %rsi
	sete	%r10b
	testq	%rdx, %rdx
	sete	%al
	orb	%al, %r10b
	jne	.L509
	testl	%r8d, %r8d
	js	.L509
	jle	.L510
	testq	%rcx, %rcx
	je	.L509
.L510:
	testl	%r8d, %r8d
	je	.L507
	jmp	_ZNK6icu_6717RuleBasedCollator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L509:
	movl	$1, (%r9)
.L507:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3586:
	.size	_ZNK6icu_6717RuleBasedCollator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6717RuleBasedCollator14setVariableTopEPKDsiR10UErrorCode.part.0, @function
_ZN6icu_6717RuleBasedCollator14setVariableTopEPKDsiR10UErrorCode.part.0:
.LFB4907:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$584, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L601
.L521:
	testl	%edx, %edx
	je	.L602
	movq	16(%r12), %rax
	movslq	%edx, %rdx
	leaq	-592(%rbp), %r14
	leaq	(%rbx,%rdx,2), %rsi
	movq	8(%r12), %rdx
	movl	24(%rax), %eax
	movq	(%rdx), %rdi
	movq	%rdx, -576(%rbp)
	movl	%eax, %ecx
	notl	%eax
	movl	$40, -552(%rbp)
	shrl	%ecx
	movq	%rdi, -584(%rbp)
	andl	$1, %ecx
	andl	$1, %eax
	movb	$0, -548(%rbp)
	movl	%eax, -616(%rbp)
	leaq	-544(%rbp), %rax
	movq	%rax, -560(%rbp)
	movl	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movl	$-1, -208(%rbp)
	movb	%cl, -204(%rbp)
	je	.L524
	movq	%rbx, %xmm0
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -184(%rbp)
	leaq	-596(%rbp), %r15
	punpcklqdq	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6722UTF16CollationIteratorE(%rip), %rax
	movl	$1, -568(%rbp)
	movq	%r15, %rsi
	movq	%rax, -592(%rbp)
	movups	%xmm0, -200(%rbp)
	call	_ZN6icu_6722UTF16CollationIterator14handleNextCE32ERiR10UErrorCode@PLT
	movzbl	%al, %edx
	cmpb	$-65, %al
	jbe	.L603
	movq	-576(%rbp), %rsi
	cmpl	$192, %edx
	je	.L604
.L534:
	cmpl	$193, %edx
	jne	.L535
	leal	-193(%rax), %ebx
	salq	$32, %rbx
	orq	$83887360, %rbx
.L593:
	movslq	-224(%rbp), %rdx
	movq	-560(%rbp), %rcx
	leal	1(%rdx), %eax
	movl	%eax, -224(%rbp)
	movq	%rbx, (%rcx,%rdx,8)
	movb	$0, -609(%rbp)
.L526:
	movl	-568(%rbp), %edx
	cmpl	%eax, %edx
	jle	.L536
	movq	-560(%rbp), %rcx
	leal	1(%rax), %edx
	cltq
	movl	%edx, -224(%rbp)
	movabsq	$4311744768, %rdx
	cmpq	%rdx, (%rcx,%rax,8)
	setne	%al
	orb	%al, -609(%rbp)
.L537:
	movq	%r14, %rdi
	call	_ZN6icu_6722UTF16CollationIteratorD1Ev@PLT
.L550:
	cmpb	$0, -609(%rbp)
	jne	.L540
	movq	(%r12), %rax
	sarq	$32, %rbx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	*232(%rax)
	movq	16(%r12), %rax
	movl	28(%rax), %eax
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L524:
	leaq	16+_ZTVN6icu_6725FCDUTF16CollationIteratorE(%rip), %rax
	movq	%rbx, %xmm0
	movq	%rbx, %xmm1
	movq	%rsi, -152(%rbp)
	movq	%rax, -592(%rbp)
	movq	48(%rdx), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, %rdx
	leaq	-596(%rbp), %r15
	movq	%r14, %rdi
	movb	$1, -72(%rbp)
	movq	%rax, -144(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movups	%xmm0, -200(%rbp)
	movq	%rsi, %xmm0
	movq	%r15, %rsi
	movq	%rax, -136(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movl	$2, %eax
	movq	%rbx, -168(%rbp)
	movq	$0, -160(%rbp)
	movw	%ax, -128(%rbp)
	movl	$1, -568(%rbp)
	movups	%xmm0, -184(%rbp)
	call	_ZN6icu_6725FCDUTF16CollationIterator14handleNextCE32ERiR10UErrorCode@PLT
	movzbl	%al, %edx
	cmpb	$-65, %al
	ja	.L551
	movabsq	$-281474976710656, %rsi
	movq	%rax, %rbx
	sall	$16, %eax
	movl	%eax, %ecx
	salq	$32, %rbx
	movl	%edx, %eax
	andq	%rsi, %rbx
	andl	$-16777216, %ecx
	sall	$8, %eax
	orq	%rcx, %rbx
	orq	%rax, %rbx
.L596:
	movslq	-224(%rbp), %rdx
	movq	-560(%rbp), %rcx
	leal	1(%rdx), %eax
	movl	%eax, -224(%rbp)
	movq	%rbx, (%rcx,%rdx,8)
	movb	$0, -609(%rbp)
.L552:
	movl	-568(%rbp), %edx
	cmpl	%eax, %edx
	jle	.L562
	movq	-560(%rbp), %rcx
	leal	1(%rax), %edx
	cltq
	movl	%edx, -224(%rbp)
	movabsq	$4311744768, %rdx
	cmpq	%rdx, (%rcx,%rax,8)
	setne	%al
	orb	%al, -609(%rbp)
.L563:
	movq	%r14, %rdi
	call	_ZN6icu_6725FCDUTF16CollationIteratorD1Ev@PLT
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L602:
	movl	$1, 0(%r13)
	xorl	%eax, %eax
.L520:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L605
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	u_strlen_67@PLT
	movl	%eax, %edx
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L551:
	movq	-576(%rbp), %rsi
	cmpl	$192, %edx
	je	.L606
.L560:
	cmpl	$193, %edx
	jne	.L561
	leal	-193(%rax), %ebx
	salq	$32, %rbx
	orq	$83887360, %rbx
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L603:
	movabsq	$-281474976710656, %rsi
	movq	%rax, %rbx
	salq	$32, %rbx
	andq	%rsi, %rbx
.L600:
	sall	$16, %eax
	sall	$8, %edx
	andl	$-16777216, %eax
	orq	%rax, %rbx
	movl	%edx, %eax
	orq	%rax, %rbx
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L562:
	cmpl	$39, %edx
	jle	.L564
	leaq	-568(%rbp), %rdi
	movq	%r13, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L563
	movl	-568(%rbp), %edx
.L564:
	addl	$1, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%edx, -568(%rbp)
	movq	%r13, %rdx
	call	_ZN6icu_6725FCDUTF16CollationIterator14handleNextCE32ERiR10UErrorCode@PLT
	movl	%eax, %ecx
	movzbl	%al, %eax
	cmpb	$-65, %cl
	jbe	.L599
	movq	-576(%rbp), %rsi
	cmpl	$192, %eax
	je	.L607
.L573:
	cmpl	$193, %eax
	jne	.L574
	movl	-224(%rbp), %eax
	movq	%r14, %rdi
	leal	1(%rax), %edx
	movl	%edx, -224(%rbp)
	movslq	%eax, %rdx
	leal	-193(%rcx), %eax
	movq	-560(%rbp), %rcx
	salq	$32, %rax
	orq	$83887360, %rax
	movq	%rax, (%rcx,%rdx,8)
	call	_ZN6icu_6725FCDUTF16CollationIteratorD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L540:
	movl	$21, 0(%r13)
	xorl	%eax, %eax
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L536:
	cmpl	$39, %edx
	jle	.L538
	leaq	-568(%rbp), %rdi
	movq	%r13, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L537
	movl	-568(%rbp), %edx
.L538:
	addl	$1, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%edx, -568(%rbp)
	movq	%r13, %rdx
	call	_ZN6icu_6722UTF16CollationIterator14handleNextCE32ERiR10UErrorCode@PLT
	movzbl	%al, %esi
	cmpb	$-65, %al
	ja	.L539
	movslq	-224(%rbp), %rdi
	movq	%rax, %rdx
	sall	$16, %eax
	salq	$32, %rdx
	leal	1(%rdi), %ecx
	movl	%ecx, -224(%rbp)
	movabsq	$-281474976710656, %rcx
	andq	%rcx, %rdx
	movl	%eax, %ecx
	movl	%esi, %eax
	andl	$-16777216, %ecx
	sall	$8, %eax
	orq	%rcx, %rdx
	orq	%rax, %rdx
	movq	-560(%rbp), %rax
	movq	%rdx, (%rax,%rdi,8)
	movq	%r14, %rdi
	call	_ZN6icu_6722UTF16CollationIteratorD1Ev@PLT
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L607:
	movl	-596(%rbp), %eax
	testl	%eax, %eax
	js	.L608
	movq	32(%rsi), %rsi
	movq	(%rsi), %rdi
	movq	16(%rdi), %rcx
	cmpl	$55295, %eax
	jg	.L568
	movl	%eax, %edx
	movq	(%rdi), %rdi
	sarl	$5, %edx
	movslq	%edx, %rdx
.L597:
	movzwl	(%rdi,%rdx,2), %edx
	andl	$31, %eax
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L569:
	movl	(%rcx,%rdx), %ecx
	movzbl	%cl, %eax
	cmpb	$-65, %cl
	ja	.L573
	.p2align 4,,10
	.p2align 3
.L599:
	movl	-224(%rbp), %edx
	sall	$8, %eax
	movabsq	$-281474976710656, %rdi
	leal	1(%rdx), %esi
	movl	%esi, -224(%rbp)
	movslq	%edx, %rsi
	movq	%rcx, %rdx
	sall	$16, %ecx
	salq	$32, %rdx
	andl	$-16777216, %ecx
	andq	%rdi, %rdx
	movq	%r14, %rdi
	orq	%rcx, %rdx
	orq	%rax, %rdx
	movq	-560(%rbp), %rax
	movq	%rdx, (%rax,%rsi,8)
	call	_ZN6icu_6725FCDUTF16CollationIteratorD1Ev@PLT
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L539:
	movq	-576(%rbp), %r10
	cmpl	$192, %esi
	je	.L609
.L548:
	cmpl	$193, %esi
	jne	.L549
	movslq	-224(%rbp), %rdx
	subl	$193, %eax
	movq	%r14, %rdi
	salq	$32, %rax
	leal	1(%rdx), %ecx
	orq	$83887360, %rax
	movl	%ecx, -224(%rbp)
	movq	-560(%rbp), %rcx
	movq	%rax, (%rcx,%rdx,8)
	call	_ZN6icu_6722UTF16CollationIteratorD1Ev@PLT
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L561:
	movl	-596(%rbp), %edx
	movq	%r13, %r8
	movl	%eax, %ecx
	movq	%r14, %rdi
	call	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode@PLT
	movabsq	$4311744768, %rdx
	movq	%rax, %rbx
	movl	-224(%rbp), %eax
	cmpq	%rdx, %rbx
	sete	-609(%rbp)
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L549:
	movl	-596(%rbp), %edx
	movq	%r13, %r8
	movl	%eax, %ecx
	movq	%r10, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode@PLT
	movq	%rax, %r8
	movabsq	$4311744768, %rax
	cmpq	%rax, %r8
	setne	%al
	orb	%al, -609(%rbp)
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L535:
	movl	-596(%rbp), %edx
	movq	%r13, %r8
	movl	%eax, %ecx
	movq	%r14, %rdi
	call	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode@PLT
	movabsq	$4311744768, %rdx
	movq	%rax, %rbx
	movl	-224(%rbp), %eax
	cmpq	%rdx, %rbx
	sete	-609(%rbp)
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L574:
	movl	-596(%rbp), %edx
	movq	%r13, %r8
	movq	%r14, %rdi
	call	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode@PLT
	movq	%rax, %r8
	movabsq	$4311744768, %rax
	cmpq	%rax, %r8
	setne	%al
	orb	%al, -609(%rbp)
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L606:
	movl	-596(%rbp), %eax
	testl	%eax, %eax
	js	.L610
	movq	32(%rsi), %rsi
	movq	(%rsi), %rdi
	movq	16(%rdi), %rcx
	cmpl	$55295, %eax
	jg	.L555
	movl	%eax, %edx
	movq	(%rdi), %rdi
	sarl	$5, %edx
	movslq	%edx, %rdx
.L595:
	movzwl	(%rdi,%rdx,2), %edx
	andl	$31, %eax
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L556:
	movl	(%rcx,%rdx), %eax
	movzbl	%al, %edx
	cmpb	$-65, %al
	ja	.L560
	movabsq	$-281474976710656, %rcx
	movq	%rax, %rbx
	sall	$16, %eax
	salq	$32, %rbx
	andl	$-16777216, %eax
	andq	%rcx, %rbx
	orq	%rax, %rbx
	movl	%edx, %eax
	sall	$8, %eax
	orq	%rax, %rbx
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L609:
	movl	-596(%rbp), %eax
	testl	%eax, %eax
	js	.L611
	movq	32(%r10), %r10
	movq	(%r10), %rsi
	movq	16(%rsi), %rcx
	cmpl	$55295, %eax
	jg	.L543
	movl	%eax, %edx
	movq	(%rsi), %rsi
	sarl	$5, %edx
	movslq	%edx, %rdx
.L594:
	movzwl	(%rsi,%rdx,2), %edx
	andl	$31, %eax
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L544:
	movl	(%rcx,%rdx), %eax
	movzbl	%al, %esi
	cmpb	$-65, %al
	ja	.L548
	movabsq	$-281474976710656, %rdi
	movl	-224(%rbp), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, -224(%rbp)
	movslq	%edx, %rcx
	movq	%rax, %rdx
	sall	$16, %eax
	salq	$32, %rdx
	andl	$-16777216, %eax
	andq	%rdi, %rdx
	movq	%r14, %rdi
	orq	%rax, %rdx
	movl	%esi, %eax
	sall	$8, %eax
	orq	%rax, %rdx
	movq	-560(%rbp), %rax
	movq	%rdx, (%rax,%rcx,8)
	call	_ZN6icu_6722UTF16CollationIteratorD1Ev@PLT
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L604:
	movl	-596(%rbp), %eax
	testl	%eax, %eax
	js	.L612
	movq	32(%rsi), %rsi
	movq	(%rsi), %rdi
	movq	16(%rdi), %rcx
	cmpl	$55295, %eax
	jg	.L529
	movl	%eax, %edx
	movq	(%rdi), %rdi
	sarl	$5, %edx
	movslq	%edx, %rdx
.L592:
	movzwl	(%rdi,%rdx,2), %edx
	andl	$31, %eax
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L530:
	movl	(%rcx,%rdx), %eax
	movzbl	%al, %edx
	cmpb	$-65, %al
	ja	.L534
	movabsq	$-281474976710656, %rcx
	movq	%rax, %rbx
	salq	$32, %rbx
	andq	%rcx, %rbx
	jmp	.L600
.L543:
	cmpl	$65535, %eax
	jg	.L545
	cmpl	$56320, %eax
	movq	(%rsi), %rdi
	movl	$320, %edx
	movl	$0, %esi
	cmovl	%edx, %esi
	movl	%eax, %edx
	andl	$31, %eax
	sarl	$5, %edx
	addl	%esi, %edx
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L544
.L529:
	cmpl	$65535, %eax
	jg	.L531
	cmpl	$56320, %eax
	movq	(%rdi), %r8
	movl	$320, %edx
	movl	$0, %edi
	cmovl	%edx, %edi
	movl	%eax, %edx
	andl	$31, %eax
	sarl	$5, %edx
	addl	%edi, %edx
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L530
.L568:
	cmpl	$65535, %eax
	jg	.L570
	cmpl	$56319, %eax
	movl	$320, %edx
	movq	(%rdi), %rdi
	cmovg	-616(%rbp), %edx
	movl	%edx, %r9d
	movl	%eax, %edx
	sarl	$5, %edx
	addl	%r9d, %edx
.L598:
	movslq	%edx, %rdx
	jmp	.L597
.L555:
	cmpl	$65535, %eax
	jg	.L557
	movl	$320, %edx
	cmpl	$56320, %eax
	movq	(%rdi), %r8
	movl	%edx, %edi
	movl	%eax, %edx
	cmovge	-616(%rbp), %edi
	andl	$31, %eax
	sarl	$5, %edx
	addl	%edi, %edx
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L556
.L611:
	movslq	-224(%rbp), %rax
	movabsq	$4311744768, %rdi
	leal	1(%rax), %edx
	movl	%edx, -224(%rbp)
	movq	-560(%rbp), %rdx
	movq	%rdi, (%rdx,%rax,8)
	jmp	.L537
.L608:
	movslq	-224(%rbp), %rax
	movabsq	$4311744768, %rdi
	leal	1(%rax), %edx
	movl	%edx, -224(%rbp)
	movq	-560(%rbp), %rdx
	movq	%rdi, (%rdx,%rax,8)
	jmp	.L563
.L610:
	movslq	-224(%rbp), %rdx
	movq	-560(%rbp), %rcx
	movabsq	$4311744768, %rbx
	movb	$1, -609(%rbp)
	leal	1(%rdx), %eax
	movl	%eax, -224(%rbp)
	movq	%rbx, (%rcx,%rdx,8)
	jmp	.L552
.L612:
	movslq	-224(%rbp), %rdx
	movq	-560(%rbp), %rcx
	movabsq	$4311744768, %rbx
	movb	$1, -609(%rbp)
	leal	1(%rdx), %eax
	movl	%eax, -224(%rbp)
	movq	%rbx, (%rcx,%rdx,8)
	jmp	.L526
.L545:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L544
	cmpl	44(%rsi), %eax
	jl	.L547
	movslq	48(%rsi), %rdx
	salq	$2, %rdx
	jmp	.L544
.L531:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L530
	cmpl	44(%rdi), %eax
	jl	.L533
	movslq	48(%rdi), %rdx
	salq	$2, %rdx
	jmp	.L530
.L557:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L556
	cmpl	44(%rdi), %eax
	jl	.L559
	movslq	48(%rdi), %rdx
	salq	$2, %rdx
	jmp	.L556
.L570:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L569
	cmpl	44(%rdi), %eax
	jl	.L572
	movslq	48(%rdi), %rdx
	salq	$2, %rdx
	jmp	.L569
.L533:
	movl	%eax, %edx
	movq	(%rdi), %rdi
	movl	%eax, %r8d
	sarl	$11, %edx
	sarl	$5, %r8d
	addl	$2080, %edx
	andl	$63, %r8d
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	addl	%r8d, %edx
	movslq	%edx, %rdx
	jmp	.L592
.L547:
	movl	%eax, %edx
	movq	(%rsi), %rsi
	movl	%eax, %edi
	sarl	$11, %edx
	sarl	$5, %edi
	addl	$2080, %edx
	andl	$63, %edi
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	addl	%edi, %edx
	movslq	%edx, %rdx
	jmp	.L594
.L559:
	movl	%eax, %edx
	movq	(%rdi), %rdi
	movl	%eax, %r8d
	sarl	$11, %edx
	sarl	$5, %r8d
	addl	$2080, %edx
	andl	$63, %r8d
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	addl	%r8d, %edx
	movslq	%edx, %rdx
	jmp	.L595
.L572:
	movl	%eax, %edx
	movq	(%rdi), %rdi
	movl	%eax, %r8d
	sarl	$11, %edx
	sarl	$5, %r8d
	addl	$2080, %edx
	andl	$63, %r8d
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	addl	%r8d, %edx
	jmp	.L598
.L605:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4907:
	.size	_ZN6icu_6717RuleBasedCollator14setVariableTopEPKDsiR10UErrorCode.part.0, .-_ZN6icu_6717RuleBasedCollator14setVariableTopEPKDsiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollator14setVariableTopEPKDsiR10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollator14setVariableTopEPKDsiR10UErrorCode, @function
_ZN6icu_6717RuleBasedCollator14setVariableTopEPKDsiR10UErrorCode:
.LFB3516:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L613
	testq	%rsi, %rsi
	jne	.L615
	testl	%edx, %edx
	je	.L615
	movl	$1, (%rcx)
.L613:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L615:
	jmp	_ZN6icu_6717RuleBasedCollator14setVariableTopEPKDsiR10UErrorCode.part.0
	.cfi_endproc
.LFE3516:
	.size	_ZN6icu_6717RuleBasedCollator14setVariableTopEPKDsiR10UErrorCode, .-_ZN6icu_6717RuleBasedCollator14setVariableTopEPKDsiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollator14setVariableTopERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollator14setVariableTopERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6717RuleBasedCollator14setVariableTopERKNS_13UnicodeStringER10UErrorCode:
.LFB3517:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rdx, %rcx
	movq	216(%rax), %r8
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L624
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	$17, %al
	jne	.L631
.L641:
	testb	$2, %al
	jne	.L639
	movq	24(%rsi), %rsi
.L626:
	leaq	_ZN6icu_6717RuleBasedCollator14setVariableTopEPKDsiR10UErrorCode(%rip), %rax
	cmpq	%rax, %r8
	jne	.L628
.L640:
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L623
	testq	%rsi, %rsi
	jne	.L630
	testl	%edx, %edx
	je	.L630
	movl	$1, (%rcx)
.L623:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	leaq	_ZN6icu_6717RuleBasedCollator14setVariableTopEPKDsiR10UErrorCode(%rip), %rax
	addq	$10, %rsi
	cmpq	%rax, %r8
	je	.L640
.L628:
	jmp	*%r8
	.p2align 4,,10
	.p2align 3
.L630:
	jmp	_ZN6icu_6717RuleBasedCollator14setVariableTopEPKDsiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L624:
	movl	12(%rsi), %edx
	testb	$17, %al
	je	.L641
.L631:
	xorl	%esi, %esi
	jmp	.L626
	.cfi_endproc
.LFE3517:
	.size	_ZN6icu_6717RuleBasedCollator14setVariableTopERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6717RuleBasedCollator14setVariableTopERKNS_13UnicodeStringER10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3925:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3925:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3928:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L655
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L643
	cmpb	$0, 12(%rbx)
	jne	.L656
.L647:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L643:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L647
	.cfi_endproc
.LFE3928:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3931:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L659
	ret
	.p2align 4,,10
	.p2align 3
.L659:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3931:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3934:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L662
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L662:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3934:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L668
.L664:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L669
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L668:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L669:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3936:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3937:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3937:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3938:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3938:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3939:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3939:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3940:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3940:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3941:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3941:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3942:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L685
	testl	%edx, %edx
	jle	.L685
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L688
.L677:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L685:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L677
	.cfi_endproc
.LFE3942:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L692
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L692
	testl	%r12d, %r12d
	jg	.L699
	cmpb	$0, 12(%rbx)
	jne	.L700
.L694:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L699:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L694
.L700:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L692:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3943:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L702
	movq	(%rdi), %r8
.L703:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L702:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L706
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L706
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L706:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3944:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3945:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L713
	ret
	.p2align 4,,10
	.p2align 3
.L713:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3945:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3946:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3946:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3947:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3947:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3948:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3948:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3950:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3950:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3952:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3952:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollatorC2ERKS0_
	.type	_ZN6icu_6717RuleBasedCollatorC2ERKS0_, @function
_ZN6icu_6717RuleBasedCollatorC2ERKS0_:
.LFB3472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_678CollatorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6717RuleBasedCollatorE(%rip), %rax
	leaq	40(%r12), %rsi
	movq	%rax, (%rbx)
	movq	8(%r12), %rax
	leaq	40(%rbx), %rdi
	movq	%rax, 8(%rbx)
	movq	16(%r12), %rax
	movq	%rax, 16(%rbx)
	movq	24(%r12), %rax
	movq	%rax, 24(%rbx)
	movq	32(%r12), %rax
	movq	%rax, 32(%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	264(%r12), %eax
	movq	16(%rbx), %rdi
	movl	%eax, 264(%rbx)
	movzbl	268(%r12), %eax
	movb	%al, 268(%rbx)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	32(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6712SharedObject6addRefEv@PLT
	.cfi_endproc
.LFE3472:
	.size	_ZN6icu_6717RuleBasedCollatorC2ERKS0_, .-_ZN6icu_6717RuleBasedCollatorC2ERKS0_
	.globl	_ZN6icu_6717RuleBasedCollatorC1ERKS0_
	.set	_ZN6icu_6717RuleBasedCollatorC1ERKS0_,_ZN6icu_6717RuleBasedCollatorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollatorC2EPKNS_19CollationCacheEntryE
	.type	_ZN6icu_6717RuleBasedCollatorC2EPKNS_19CollationCacheEntryE, @function
_ZN6icu_6717RuleBasedCollatorC2EPKNS_19CollationCacheEntryE:
.LFB3480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_678CollatorC2Ev@PLT
	leaq	16+_ZTVN6icu_6717RuleBasedCollatorE(%rip), %rax
	leaq	24(%r12), %rsi
	movq	%rax, (%rbx)
	movq	248(%r12), %rax
	leaq	40(%rbx), %rdi
	movq	24(%rax), %rdx
	movq	%rdx, 8(%rbx)
	movq	32(%rax), %rdx
	movq	%rax, 24(%rbx)
	movq	%rdx, 16(%rbx)
	movq	%r12, 32(%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movb	$0, 268(%rbx)
	movq	16(%rbx), %rdi
	movl	$0, 264(%rbx)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	32(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6712SharedObject6addRefEv@PLT
	.cfi_endproc
.LFE3480:
	.size	_ZN6icu_6717RuleBasedCollatorC2EPKNS_19CollationCacheEntryE, .-_ZN6icu_6717RuleBasedCollatorC2EPKNS_19CollationCacheEntryE
	.globl	_ZN6icu_6717RuleBasedCollatorC1EPKNS_19CollationCacheEntryE
	.set	_ZN6icu_6717RuleBasedCollatorC1EPKNS_19CollationCacheEntryE,_ZN6icu_6717RuleBasedCollatorC2EPKNS_19CollationCacheEntryE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollator14adoptTailoringEPNS_18CollationTailoringER10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollator14adoptTailoringEPNS_18CollationTailoringER10UErrorCode, @function
_ZN6icu_6717RuleBasedCollator14adoptTailoringEPNS_18CollationTailoringER10UErrorCode:
.LFB3486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdx), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	testl	%eax, %eax
	jg	.L732
	movq	%rdi, %rbx
	movl	$256, %edi
	movq	%rdx, %r14
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L725
	movq	$0, 8(%rax)
	leaq	104(%r12), %r14
	leaq	24(%r13), %rdi
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6719CollationCacheEntryE(%rip), %rax
	movq	%r14, %rsi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r12, 248(%r13)
	testq	%r12, %r12
	je	.L726
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L726:
	movq	%r13, 32(%rbx)
	movdqu	24(%r12), %xmm0
	movq	32(%r12), %rdi
	movups	%xmm0, 8(%rbx)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	%r12, 24(%rbx)
	movq	32(%rbx), %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	leaq	40(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movb	$0, 268(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L725:
	.cfi_restore_state
	movq	$0, 32(%rbx)
	movl	$7, (%r14)
.L732:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6712SharedObject20deleteIfZeroRefCountEv@PLT
	.cfi_endproc
.LFE3486:
	.size	_ZN6icu_6717RuleBasedCollator14adoptTailoringEPNS_18CollationTailoringER10UErrorCode, .-_ZN6icu_6717RuleBasedCollator14adoptTailoringEPNS_18CollationTailoringER10UErrorCode
	.section	.rodata.str1.1
.LC6:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollatorC2EPKhiPKS0_R10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollatorC2EPKhiPKS0_R10UErrorCode, @function
_ZN6icu_6717RuleBasedCollatorC2EPKhiPKS0_R10UErrorCode:
.LFB3477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	call	_ZN6icu_678CollatorC2Ev@PLT
	leaq	16+_ZTVN6icu_6717RuleBasedCollatorE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, (%r12)
	leaq	40(%r12), %rdi
	xorl	%r8d, %r8d
	leaq	.LC6(%rip), %rsi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	0(%r13), %ecx
	movl	$0, 264(%r12)
	movb	$0, 268(%r12)
	testl	%ecx, %ecx
	jg	.L733
	testl	%r14d, %r14d
	sete	%dl
	testq	%rbx, %rbx
	sete	%al
	orb	%al, %dl
	jne	.L743
	testq	%r15, %r15
	je	.L743
	movq	%r13, %rdi
	call	_ZN6icu_6713CollationRoot7getRootER10UErrorCode@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L733
	cmpq	24(%rbx), %rax
	je	.L739
	movl	$16, 0(%r13)
.L733:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L743:
	.cfi_restore_state
	movl	$1, 0(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L739:
	.cfi_restore_state
	movl	$400, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L740
	movq	24(%rbx), %rax
	movq	%r9, %rdi
	movq	%r9, -56(%rbp)
	movq	32(%rax), %rsi
	call	_ZN6icu_6718CollationTailoringC1EPKNS_17CollationSettingsE@PLT
	movq	-56(%rbp), %r9
	cmpq	$0, 32(%r9)
	je	.L741
	movq	24(%rbx), %rdi
	movq	%r9, %rcx
	movq	%r13, %r8
	movl	%r14d, %edx
	movq	%r15, %rsi
	call	_ZN6icu_6719CollationDataReader4readEPKNS_18CollationTailoringEPKhiRS1_R10UErrorCode@PLT
	movl	0(%r13), %eax
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	jle	.L745
.L742:
	movq	(%r9), %rax
	movq	%r9, %rdi
	movq	8(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L745:
	.cfi_restore_state
	leaq	104(%r9), %rdi
	call	_ZN6icu_676Locale10setToBogusEv@PLT
	movq	-56(%rbp), %r9
	addq	$24, %rsp
	movq	%r13, %rdx
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movq	%r9, %rsi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717RuleBasedCollator14adoptTailoringEPNS_18CollationTailoringER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L741:
	.cfi_restore_state
	movl	$7, 0(%r13)
	jmp	.L742
.L740:
	movl	$7, 0(%r13)
	jmp	.L733
	.cfi_endproc
.LFE3477:
	.size	_ZN6icu_6717RuleBasedCollatorC2EPKhiPKS0_R10UErrorCode, .-_ZN6icu_6717RuleBasedCollatorC2EPKhiPKS0_R10UErrorCode
	.globl	_ZN6icu_6717RuleBasedCollatorC1EPKhiPKS0_R10UErrorCode
	.set	_ZN6icu_6717RuleBasedCollatorC1EPKhiPKS0_R10UErrorCode,_ZN6icu_6717RuleBasedCollatorC2EPKhiPKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollatoraSERKS0_
	.type	_ZN6icu_6717RuleBasedCollatoraSERKS0_, @function
_ZN6icu_6717RuleBasedCollatoraSERKS0_:
.LFB3488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L747
	movq	16(%rsi), %r13
	movq	16(%rdi), %rdi
	movq	%rsi, %rbx
	cmpq	%rdi, %r13
	je	.L749
	testq	%rdi, %rdi
	je	.L750
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L750:
	movq	%r13, 16(%r12)
	testq	%r13, %r13
	je	.L749
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L749:
	movq	24(%rbx), %rax
	movq	32(%rbx), %r13
	movq	32(%r12), %rdi
	movq	%rax, 24(%r12)
	cmpq	%rdi, %r13
	je	.L752
	testq	%rdi, %rdi
	je	.L753
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L753:
	movq	%r13, 32(%r12)
	testq	%r13, %r13
	je	.L765
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L765:
	movq	24(%r12), %rax
.L752:
	movq	24(%rax), %rax
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	movq	%rax, 8(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movl	264(%rbx), %eax
	movl	%eax, 264(%r12)
	movzbl	268(%rbx), %eax
	movb	%al, 268(%r12)
.L747:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3488:
	.size	_ZN6icu_6717RuleBasedCollatoraSERKS0_, .-_ZN6icu_6717RuleBasedCollatoraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollator16getStaticClassIDEv
	.type	_ZN6icu_6717RuleBasedCollator16getStaticClassIDEv, @function
_ZN6icu_6717RuleBasedCollator16getStaticClassIDEv:
.LFB3489:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6717RuleBasedCollator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3489:
	.size	_ZN6icu_6717RuleBasedCollator16getStaticClassIDEv, .-_ZN6icu_6717RuleBasedCollator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator19internalGetLocaleIDE18ULocDataLocaleTypeR10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator19internalGetLocaleIDE18ULocDataLocaleTypeR10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator19internalGetLocaleIDE18ULocDataLocaleTypeR10UErrorCode:
.LFB3497:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L767
	testl	%esi, %esi
	je	.L769
	cmpl	$1, %esi
	jne	.L777
.L770:
	addq	$40, %rdi
.L773:
	cmpb	$0, 216(%rdi)
	jne	.L775
	movq	40(%rdi), %rax
	leaq	.LC2(%rip), %rdx
	cmpb	$0, (%rax)
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L775:
	xorl	%eax, %eax
.L767:
	ret
	.p2align 4,,10
	.p2align 3
.L777:
	movl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L769:
	cmpb	$0, 268(%rdi)
	jne	.L770
	movq	24(%rdi), %rdi
	addq	$104, %rdi
	jmp	.L773
	.cfi_endproc
.LFE3497:
	.size	_ZNK6icu_6717RuleBasedCollator19internalGetLocaleIDE18ULocDataLocaleTypeR10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator19internalGetLocaleIDE18ULocDataLocaleTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator8getRulesEv
	.type	_ZNK6icu_6717RuleBasedCollator8getRulesEv, @function
_ZNK6icu_6717RuleBasedCollator8getRulesEv:
.LFB3498:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	addq	$40, %rax
	ret
	.cfi_endproc
.LFE3498:
	.size	_ZNK6icu_6717RuleBasedCollator8getRulesEv, .-_ZNK6icu_6717RuleBasedCollator8getRulesEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator8getRulesE14UColRuleOptionRNS_13UnicodeStringE
	.type	_ZNK6icu_6717RuleBasedCollator8getRulesE14UColRuleOptionRNS_13UnicodeStringE, @function
_ZNK6icu_6717RuleBasedCollator8getRulesE14UColRuleOptionRNS_13UnicodeStringE:
.LFB3499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testl	%esi, %esi
	je	.L786
	movzwl	8(%rdx), %edx
	movq	%r12, %rdi
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 8(%r12)
	call	_ZN6icu_6715CollationLoader15appendRootRulesERNS_13UnicodeStringE@PLT
	movq	24(%rbx), %rax
	movswl	48(%rax), %ecx
	leaq	40(%rax), %rsi
	testw	%cx, %cx
	js	.L782
	sarl	$5, %ecx
.L783:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	popq	%rbx
	popq	%r12
	movq	%rax, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore_state
	movl	52(%rax), %ecx
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L786:
	movq	24(%rdi), %rsi
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	addq	$40, %rsi
	jmp	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	.cfi_endproc
.LFE3499:
	.size	_ZNK6icu_6717RuleBasedCollator8getRulesE14UColRuleOptionRNS_13UnicodeStringE, .-_ZNK6icu_6717RuleBasedCollator8getRulesE14UColRuleOptionRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator18getDefaultSettingsEv
	.type	_ZNK6icu_6717RuleBasedCollator18getDefaultSettingsEv, @function
_ZNK6icu_6717RuleBasedCollator18getDefaultSettingsEv:
.LFB3510:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	32(%rax), %rax
	ret
	.cfi_endproc
.LFE3510:
	.size	_ZNK6icu_6717RuleBasedCollator18getDefaultSettingsEv, .-_ZNK6icu_6717RuleBasedCollator18getDefaultSettingsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator19setFastLatinOptionsERNS_17CollationSettingsE
	.type	_ZNK6icu_6717RuleBasedCollator19setFastLatinOptionsERNS_17CollationSettingsE, @function
_ZNK6icu_6717RuleBasedCollator19setFastLatinOptionsERNS_17CollationSettingsE:
.LFB3521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	84(%rsi), %rdx
	movl	$384, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	_ZN6icu_6718CollationFastLatin10getOptionsEPKNS_13CollationDataERKNS_17CollationSettingsEPti@PLT
	movl	%eax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3521:
	.size	_ZNK6icu_6717RuleBasedCollator19setFastLatinOptionsERNS_17CollationSettingsE, .-_ZNK6icu_6717RuleBasedCollator19setFastLatinOptionsERNS_17CollationSettingsE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator19writeIdenticalLevelEPKDsS2_RNS_15SortKeyByteSinkER10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator19writeIdenticalLevelEPKDsS2_RNS_15SortKeyByteSinkER10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator19writeIdenticalLevelEPKDsS2_RNS_15SortKeyByteSinkER10UErrorCode:
.LFB3575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	xorl	%ecx, %ecx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rdi, -136(%rbp)
	movq	48(%rax), %rdi
	call	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_PNS_16ReorderingBufferER10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L790
	movq	%rax, %r14
	movl	24(%r12), %eax
	movq	-136(%rbp), %r10
	testl	%eax, %eax
	jle	.L792
	subl	$1, %eax
	movl	%eax, 24(%r12)
.L793:
	xorl	%r11d, %r11d
	cmpq	%r14, %r15
	je	.L796
	movq	%r14, %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	xorl	%edi, %edi
	subq	%r15, %rdx
	movq	%r10, -136(%rbp)
	sarq	%rdx
	call	u_writeIdenticalLevelRun_67@PLT
	movq	-136(%rbp), %r10
	movl	%eax, %r11d
.L796:
	testq	%r13, %r13
	je	.L797
	cmpq	%r14, %r13
	je	.L790
	movq	%r13, %r8
	subq	%r14, %r8
	sarq	%r8
.L798:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rbx, %r9
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	leaq	-128(%rbp), %r15
	movw	%ax, -120(%rbp)
	movq	8(%r10), %rax
	movq	%r15, %rcx
	movl	%r11d, -136(%rbp)
	movq	48(%rax), %rdi
	call	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_RNS_13UnicodeStringEiR10UErrorCode@PLT
	movzwl	-120(%rbp), %eax
	movl	-136(%rbp), %r11d
	testw	%ax, %ax
	js	.L799
	movswl	%ax, %edx
	sarl	$5, %edx
.L800:
	testb	$17, %al
	jne	.L805
	leaq	-118(%rbp), %rsi
	testb	$2, %al
	cmove	-104(%rbp), %rsi
.L801:
	movl	%r11d, %edi
	movq	%r12, %rcx
	call	u_writeIdenticalLevelRun_67@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L790:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L811
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L792:
	.cfi_restore_state
	movslq	20(%r12), %rdx
	cmpl	16(%r12), %edx
	jge	.L812
.L794:
	movq	8(%r12), %rax
	movb	$1, (%rax,%rdx)
.L795:
	addl	$1, 20(%r12)
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L812:
	movq	(%r12), %rax
	movq	%r10, -136(%rbp)
	movl	$1, %esi
	movq	%r12, %rdi
	call	*48(%rax)
	movq	-136(%rbp), %r10
	testb	%al, %al
	je	.L795
	movslq	20(%r12), %rdx
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L799:
	movl	-116(%rbp), %edx
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L797:
	cmpw	$0, (%r14)
	je	.L790
	movl	$-1, %r8d
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L805:
	xorl	%esi, %esi
	jmp	.L801
.L811:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3575:
	.size	_ZNK6icu_6717RuleBasedCollator19writeIdenticalLevelEPKDsS2_RNS_15SortKeyByteSinkER10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator19writeIdenticalLevelEPKDsS2_RNS_15SortKeyByteSinkER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCode.part.0, @function
_ZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCode.part.0:
.LFB4901:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$584, %rsp
	movq	%rsi, -616(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L814
	movslq	%edx, %rdx
	leaq	(%rsi,%rdx,2), %r15
.L814:
	movq	16(%r13), %rdx
	leaq	16+_ZTVN6icu_6713CollationKeys13LevelCallbackE(%rip), %rax
	movq	%rax, -600(%rbp)
	movq	8(%r13), %rax
	movl	24(%rdx), %edi
	movq	(%rax), %r8
	movq	72(%rax), %rsi
	movq	%rax, -576(%rbp)
	movl	$0, -568(%rbp)
	movl	%edi, %ecx
	shrl	%ecx
	movq	%r8, -584(%rbp)
	andl	$1, %ecx
	andl	$1, %edi
	leaq	-592(%rbp), %rdi
	jne	.L815
	pushq	%rbx
	leaq	-544(%rbp), %rax
	movq	-616(%rbp), %xmm0
	leaq	-600(%rbp), %r14
	pushq	$1
	movq	%r14, %r9
	movl	$1, %r8d
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -560(%rbp)
	leaq	16+_ZTVN6icu_6722UTF16CollationIteratorE(%rip), %rax
	movb	%cl, -204(%rbp)
	movq	%r12, %rcx
	movq	%rax, -592(%rbp)
	movq	%rdi, -624(%rbp)
	movups	%xmm0, -200(%rbp)
	movl	$40, -552(%rbp)
	movb	$0, -548(%rbp)
	movl	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movl	$-1, -208(%rbp)
	movq	%r15, -184(%rbp)
	call	_ZN6icu_6713CollationKeys26writeSortKeyUpToQuaternaryERNS_17CollationIteratorEPKaRKNS_17CollationSettingsERNS_15SortKeyByteSinkENS_9Collation5LevelERNS0_13LevelCallbackEaR10UErrorCode@PLT
	movq	-624(%rbp), %rdi
	call	_ZN6icu_6722UTF16CollationIteratorD1Ev@PLT
	movq	16(%r13), %rax
	popq	%rsi
	popq	%rdi
	movl	24(%rax), %eax
	sarl	$12, %eax
	cmpl	$15, %eax
	je	.L822
.L817:
	movq	(%r12), %rax
	movl	$1, %edx
	leaq	_ZZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCodeE10terminator(%rip), %rsi
	movq	%r12, %rdi
	call	*16(%rax)
	movq	%r14, %rdi
	call	_ZN6icu_6713CollationKeys13LevelCallbackD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L823
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L815:
	.cfi_restore_state
	movb	%cl, -204(%rbp)
	leaq	16+_ZTVN6icu_6725FCDUTF16CollationIteratorE(%rip), %rcx
	leaq	-544(%rbp), %r8
	movq	%rcx, -592(%rbp)
	movq	-616(%rbp), %rcx
	leaq	-600(%rbp), %r14
	movb	$0, -548(%rbp)
	movq	48(%rax), %rax
	movq	%r14, %r9
	movq	%rcx, %xmm0
	pushq	%rbx
	movq	%rcx, %xmm1
	pushq	$1
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -144(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movups	%xmm0, -200(%rbp)
	movq	%r15, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%r8, -560(%rbp)
	movl	$1, %r8d
	movq	%rcx, -168(%rbp)
	movq	%r12, %rcx
	movq	%rax, -136(%rbp)
	movl	$2, %eax
	movw	%ax, -128(%rbp)
	movq	%rdi, -624(%rbp)
	movups	%xmm0, -184(%rbp)
	movl	$40, -552(%rbp)
	movl	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movl	$-1, -208(%rbp)
	movq	$0, -160(%rbp)
	movq	%r15, -152(%rbp)
	movb	$1, -72(%rbp)
	call	_ZN6icu_6713CollationKeys26writeSortKeyUpToQuaternaryERNS_17CollationIteratorEPKaRKNS_17CollationSettingsERNS_15SortKeyByteSinkENS_9Collation5LevelERNS0_13LevelCallbackEaR10UErrorCode@PLT
	movq	-624(%rbp), %rdi
	call	_ZN6icu_6725FCDUTF16CollationIteratorD1Ev@PLT
	movq	16(%r13), %rax
	popq	%rdx
	popq	%rcx
	movl	24(%rax), %eax
	sarl	$12, %eax
	cmpl	$15, %eax
	jne	.L817
.L822:
	movq	-616(%rbp), %rsi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNK6icu_6717RuleBasedCollator19writeIdenticalLevelEPKDsS2_RNS_15SortKeyByteSinkER10UErrorCode
	jmp	.L817
.L823:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4901:
	.size	_ZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCode.part.0, .-_ZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCode:
.LFB3571:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L824
	jmp	_ZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L824:
	ret
	.cfi_endproc
.LFE3571:
	.size	_ZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator15getCollationKeyEPKDsiRNS_12CollationKeyER10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator15getCollationKeyEPKDsiRNS_12CollationKeyER10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator15getCollationKeyEPKDsiRNS_12CollationKeyER10UErrorCode:
.LFB3568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L844
	movq	%rdi, %r13
	movq	%r8, %rbx
	testq	%rsi, %rsi
	jne	.L829
	testl	%edx, %edx
	je	.L829
	movl	$1, (%r8)
.L844:
	movq	%r12, %rdi
	call	_ZN6icu_6712CollationKey10setToBogusEv@PLT
.L826:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L845
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L829:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%edx, -108(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN6icu_6712CollationKey5resetEv@PLT
	movl	8(%r12), %esi
	movl	-108(%rbp), %edx
	testl	%esi, %esi
	movq	-104(%rbp), %rsi
	jns	.L830
	movl	24(%r12), %eax
	movq	16(%r12), %rcx
.L835:
	movq	%rcx, -88(%rbp)
	movl	(%rbx), %ecx
	leaq	-96(%rbp), %r15
	leaq	16+_ZTVN6icu_6720CollationKeyByteSinkE(%rip), %r14
	movl	%eax, -80(%rbp)
	movq	$0, -76(%rbp)
	movq	%r14, -96(%rbp)
	movq	%r12, -64(%rbp)
	testl	%ecx, %ecx
	jg	.L833
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r13, %rdi
	call	_ZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCode.part.0
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L833
	cmpl	$2, 12(%r12)
	jne	.L834
	movl	$7, (%rbx)
.L832:
	movq	%r15, %rdi
	movq	%r14, -96(%rbp)
	call	_ZN6icu_6715SortKeyByteSinkD2Ev@PLT
	movq	%r12, %rax
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L830:
	leaq	16(%r12), %rcx
	movl	$32, %eax
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L833:
	movq	%r12, %rdi
	call	_ZN6icu_6712CollationKey10setToBogusEv@PLT
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L834:
	movl	-76(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712CollationKey9setLengthEi@PLT
	jmp	.L832
.L845:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3568:
	.size	_ZNK6icu_6717RuleBasedCollator15getCollationKeyEPKDsiRNS_12CollationKeyER10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator15getCollationKeyEPKDsiRNS_12CollationKeyER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator10getSortKeyEPKDsiPhi
	.type	_ZNK6icu_6717RuleBasedCollator10getSortKeyEPKDsiPhi, @function
_ZNK6icu_6717RuleBasedCollator10getSortKeyEPKDsiPhi:
.LFB3570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	sete	%r9b
	testl	%edx, %edx
	setne	%al
	testb	%al, %r9b
	jne	.L852
	testl	%r8d, %r8d
	js	.L852
	testq	%rcx, %rcx
	jne	.L856
	xorl	%r12d, %r12d
	testl	%r8d, %r8d
	jg	.L846
.L856:
	movb	$0, -41(%rbp)
	testq	%rcx, %rcx
	je	.L861
.L849:
	leaq	-80(%rbp), %r13
	movq	%rcx, -72(%rbp)
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE(%rip), %rbx
	xorl	%r12d, %r12d
	movl	%r8d, -64(%rbp)
	movq	%r13, %rcx
	leaq	-84(%rbp), %r8
	movq	$0, -60(%rbp)
	movq	%rbx, -80(%rbp)
	movl	$0, -84(%rbp)
	call	_ZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCode.part.0
	movl	-84(%rbp), %eax
	testl	%eax, %eax
	jle	.L862
	movq	%r13, %rdi
	movq	%rbx, -80(%rbp)
	call	_ZN6icu_6715SortKeyByteSinkD2Ev@PLT
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L852:
	xorl	%r12d, %r12d
.L846:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L863
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L862:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rbx, -80(%rbp)
	movl	-60(%rbp), %r12d
	call	_ZN6icu_6715SortKeyByteSinkD2Ev@PLT
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L861:
	xorl	%r8d, %r8d
	leaq	-41(%rbp), %rcx
	jmp	.L849
.L863:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3570:
	.size	_ZNK6icu_6717RuleBasedCollator10getSortKeyEPKDsiPhi, .-_ZNK6icu_6717RuleBasedCollator10getSortKeyEPKDsiPhi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator10getSortKeyERKNS_13UnicodeStringEPhi
	.type	_ZNK6icu_6717RuleBasedCollator10getSortKeyERKNS_13UnicodeStringEPhi, @function
_ZNK6icu_6717RuleBasedCollator10getSortKeyERKNS_13UnicodeStringEPhi:
.LFB3569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	272(%rax), %r10
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L865
	movswl	%ax, %r9d
	sarl	$5, %r9d
	testb	$17, %al
	jne	.L877
.L895:
	testb	$2, %al
	jne	.L891
	leaq	_ZNK6icu_6717RuleBasedCollator10getSortKeyEPKDsiPhi(%rip), %rax
	movq	24(%rsi), %rsi
	cmpq	%rax, %r10
	jne	.L870
.L896:
	testq	%rsi, %rsi
	jne	.L869
	xorl	%eax, %eax
	testl	%r9d, %r9d
	je	.L869
.L864:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L892
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L891:
	.cfi_restore_state
	leaq	_ZNK6icu_6717RuleBasedCollator10getSortKeyEPKDsiPhi(%rip), %rax
	addq	$10, %rsi
	cmpq	%rax, %r10
	jne	.L870
.L869:
	xorl	%eax, %eax
	testl	%ecx, %ecx
	js	.L864
	testq	%rdx, %rdx
	jne	.L883
	testl	%ecx, %ecx
	jg	.L864
.L883:
	movb	$0, -25(%rbp)
	testq	%rdx, %rdx
	je	.L893
.L874:
	leaq	-64(%rbp), %r12
	movq	%rdx, -56(%rbp)
	leaq	-68(%rbp), %r8
	movl	%r9d, %edx
	movl	%ecx, -48(%rbp)
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE(%rip), %rbx
	movq	%r12, %rcx
	movq	$0, -44(%rbp)
	movq	%rbx, -64(%rbp)
	movl	$0, -68(%rbp)
	call	_ZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCode.part.0
	movl	-68(%rbp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L894
.L875:
	movq	%r12, %rdi
	movl	%eax, -84(%rbp)
	movq	%rbx, -64(%rbp)
	call	_ZN6icu_6715SortKeyByteSinkD2Ev@PLT
	movl	-84(%rbp), %eax
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L865:
	movl	12(%rsi), %r9d
	testb	$17, %al
	je	.L895
.L877:
	leaq	_ZNK6icu_6717RuleBasedCollator10getSortKeyEPKDsiPhi(%rip), %rax
	xorl	%esi, %esi
	cmpq	%rax, %r10
	je	.L896
	.p2align 4,,10
	.p2align 3
.L870:
	movl	%ecx, %r8d
	movq	%rdx, %rcx
	movl	%r9d, %edx
	call	*%r10
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L894:
	movl	-44(%rbp), %eax
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L893:
	xorl	%ecx, %ecx
	leaq	-25(%rbp), %rdx
	jmp	.L874
.L892:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3569:
	.size	_ZNK6icu_6717RuleBasedCollator10getSortKeyERKNS_13UnicodeStringEPhi, .-_ZNK6icu_6717RuleBasedCollator10getSortKeyERKNS_13UnicodeStringEPhi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator15getCollationKeyERKNS_13UnicodeStringERNS_12CollationKeyER10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator15getCollationKeyERKNS_13UnicodeStringERNS_12CollationKeyER10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator15getCollationKeyERKNS_13UnicodeStringERNS_12CollationKeyER10UErrorCode:
.LFB3567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	120(%rax), %r9
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L898
	movswl	%ax, %r15d
	sarl	$5, %r15d
	testb	$17, %al
	jne	.L913
.L925:
	leaq	10(%rsi), %r13
	testb	$2, %al
	je	.L922
.L900:
	leaq	_ZNK6icu_6717RuleBasedCollator15getCollationKeyEPKDsiRNS_12CollationKeyER10UErrorCode(%rip), %rax
	cmpq	%rax, %r9
	jne	.L902
.L924:
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L921
	testq	%r13, %r13
	jne	.L905
	testl	%r15d, %r15d
	je	.L905
	movl	$1, (%rbx)
.L921:
	movq	%r12, %rdi
	call	_ZN6icu_6712CollationKey10setToBogusEv@PLT
.L897:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L923
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L922:
	.cfi_restore_state
	leaq	_ZNK6icu_6717RuleBasedCollator15getCollationKeyEPKDsiRNS_12CollationKeyER10UErrorCode(%rip), %rax
	movq	24(%rsi), %r13
	cmpq	%rax, %r9
	je	.L924
.L902:
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*%r9
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L898:
	movl	12(%rsi), %r15d
	testb	$17, %al
	je	.L925
.L913:
	xorl	%r13d, %r13d
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L905:
	movq	%r12, %rdi
	call	_ZN6icu_6712CollationKey5resetEv@PLT
	movl	8(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L906
	movl	24(%r12), %eax
	movq	16(%r12), %rdx
.L911:
	movq	%rdx, -88(%rbp)
	movl	(%rbx), %edx
	leaq	-96(%rbp), %r10
	leaq	16+_ZTVN6icu_6720CollationKeyByteSinkE(%rip), %r9
	movl	%eax, -80(%rbp)
	movq	$0, -76(%rbp)
	movq	%r9, -96(%rbp)
	movq	%r12, -64(%rbp)
	testl	%edx, %edx
	jg	.L909
	movq	%r10, %rcx
	movq	%rbx, %r8
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r10, -104(%rbp)
	call	_ZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCode.part.0
	movl	(%rbx), %eax
	movq	-104(%rbp), %r10
	leaq	16+_ZTVN6icu_6720CollationKeyByteSinkE(%rip), %r9
	testl	%eax, %eax
	jg	.L909
	cmpl	$2, 12(%r12)
	jne	.L910
	movl	$7, (%rbx)
.L908:
	movq	%r10, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN6icu_6715SortKeyByteSinkD2Ev@PLT
	movq	%r12, %rax
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L906:
	leaq	16(%r12), %rdx
	movl	$32, %eax
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L909:
	movq	%r12, %rdi
	movq	%r10, -104(%rbp)
	call	_ZN6icu_6712CollationKey10setToBogusEv@PLT
	movq	-104(%rbp), %r10
	leaq	16+_ZTVN6icu_6720CollationKeyByteSinkE(%rip), %r9
	jmp	.L908
	.p2align 4,,10
	.p2align 3
.L910:
	movl	-76(%rbp), %esi
	movq	%r12, %rdi
	movq	%r10, -104(%rbp)
	call	_ZN6icu_6712CollationKey9setLengthEi@PLT
	movq	-104(%rbp), %r10
	leaq	16+_ZTVN6icu_6720CollationKeyByteSinkE(%rip), %r9
	jmp	.L908
.L923:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3567:
	.size	_ZNK6icu_6717RuleBasedCollator15getCollationKeyERKNS_13UnicodeStringERNS_12CollationKeyER10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator15getCollationKeyERKNS_13UnicodeStringERNS_12CollationKeyER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator14internalGetCEsERKNS_13UnicodeStringERNS_9UVector64ER10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator14internalGetCEsERKNS_13UnicodeStringERNS_9UVector64ER10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator14internalGetCEsERKNS_13UnicodeStringERNS_9UVector64ER10UErrorCode:
.LFB3587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L926
	movswl	8(%rsi), %eax
	movq	%rdx, %rbx
	movq	%rcx, %r15
	testb	$17, %al
	jne	.L977
	leaq	10(%rsi), %rdx
	testb	$2, %al
	jne	.L928
	movq	24(%rsi), %rdx
.L928:
	testw	%ax, %ax
	js	.L930
	sarl	$5, %eax
.L931:
	cltq
	movq	8(%rdi), %rcx
	leaq	(%rdx,%rax,2), %r9
	movq	16(%rdi), %rax
	movq	(%rcx), %rdi
	movl	24(%rax), %esi
	movq	%rcx, -576(%rbp)
	movq	%rdi, -584(%rbp)
	movl	$0, -568(%rbp)
	movl	%esi, %eax
	shrl	%eax
	andl	$1, %eax
	andl	$1, %esi
	jne	.L932
	leaq	-544(%rbp), %rcx
	movq	%rdx, %xmm0
	movb	%al, -204(%rbp)
	leaq	16+_ZTVN6icu_6722UTF16CollationIteratorE(%rip), %rax
	movq	%rcx, -560(%rbp)
	punpcklqdq	%xmm0, %xmm0
	xorl	%edx, %edx
	leaq	-596(%rbp), %rcx
	movq	%rax, -592(%rbp)
	xorl	%eax, %eax
	leaq	-592(%rbp), %r12
	movabsq	$4311744768, %r13
	movl	$40, -552(%rbp)
	movb	$0, -548(%rbp)
	movl	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movl	$-1, -208(%rbp)
	movq	%r9, -184(%rbp)
	movq	%rcx, -616(%rbp)
	movups	%xmm0, -200(%rbp)
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L1000:
	leal	1(%rax), %edx
	movl	%edx, -224(%rbp)
	movq	-560(%rbp), %rdx
	movq	(%rdx,%rax,8), %r14
.L935:
	cmpq	%r13, %r14
	je	.L938
.L974:
	movslq	8(%rbx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L950
	cmpl	12(%rbx), %esi
	jle	.L951
.L950:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L952
	movslq	8(%rbx), %rax
	leal	1(%rax), %esi
.L951:
	movq	24(%rbx), %rdx
	movq	%r14, (%rdx,%rax,8)
	movl	%esi, 8(%rbx)
.L952:
	movslq	-224(%rbp), %rax
	movl	-568(%rbp), %edx
.L933:
	cmpl	%edx, %eax
	jl	.L1000
	cmpl	$39, %edx
	jle	.L936
	leaq	-568(%rbp), %rdi
	movq	%r15, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L938
	movl	-568(%rbp), %edx
.L936:
	addl	$1, %edx
	movq	-616(%rbp), %rsi
	movq	%r12, %rdi
	movl	%edx, -568(%rbp)
	movq	%r15, %rdx
	call	_ZN6icu_6722UTF16CollationIterator14handleNextCE32ERiR10UErrorCode@PLT
	movzbl	%al, %edx
	cmpb	$-65, %al
	jbe	.L997
	movq	-576(%rbp), %rsi
	cmpl	$192, %edx
	je	.L1001
.L948:
	cmpl	$193, %edx
	jne	.L949
	leal	-193(%rax), %ecx
	movslq	-224(%rbp), %rax
	salq	$32, %rcx
	leal	1(%rax), %edx
	orq	$83887360, %rcx
	movl	%edx, -224(%rbp)
	movq	-560(%rbp), %rdx
	movq	%rcx, %r14
	movq	%rcx, (%rdx,%rax,8)
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L1007:
	movslq	-224(%rbp), %rax
	movabsq	$4311744768, %rdi
	leal	1(%rax), %edx
	movl	%edx, -224(%rbp)
	movq	-560(%rbp), %rdx
	movq	%rdi, (%rdx,%rax,8)
	.p2align 4,,10
	.p2align 3
.L960:
	movq	%r12, %rdi
	call	_ZN6icu_6725FCDUTF16CollationIteratorD1Ev@PLT
.L926:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1002
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1001:
	.cfi_restore_state
	movl	-596(%rbp), %eax
	testl	%eax, %eax
	js	.L1003
	movq	32(%rsi), %rsi
	movq	(%rsi), %rdi
	movq	16(%rdi), %rcx
	cmpl	$55295, %eax
	jle	.L1004
	cmpl	$65535, %eax
	jg	.L945
	cmpl	$56320, %eax
	movq	(%rdi), %r9
	movl	$320, %edx
	movl	$0, %edi
	cmovl	%edx, %edi
	movl	%eax, %edx
	andl	$31, %eax
	sarl	$5, %edx
	addl	%edi, %edx
	movslq	%edx, %rdx
	movzwl	(%r9,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L944:
	movl	(%rcx,%rdx), %eax
	movzbl	%al, %edx
	cmpb	$-65, %al
	ja	.L948
	.p2align 4,,10
	.p2align 3
.L997:
	movabsq	$-281474976710656, %rdi
	movq	%rax, %rcx
	sall	$16, %eax
	salq	$32, %rcx
	andl	$-16777216, %eax
	sall	$8, %edx
	andq	%rdi, %rcx
	orq	%rax, %rcx
	movl	%edx, %eax
	orq	%rax, %rcx
	movslq	-224(%rbp), %rax
	movq	%rcx, %r14
	leal	1(%rax), %edx
	movl	%edx, -224(%rbp)
	movq	-560(%rbp), %rdx
	movq	%rcx, (%rdx,%rax,8)
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L932:
	movb	%al, -204(%rbp)
	movq	%rdx, %xmm0
	leaq	16+_ZTVN6icu_6725FCDUTF16CollationIteratorE(%rip), %rax
	movq	%rdx, %xmm1
	movb	$0, -548(%rbp)
	punpcklqdq	%xmm0, %xmm0
	leaq	-544(%rbp), %rsi
	leaq	-592(%rbp), %r12
	movq	%rax, -592(%rbp)
	movq	48(%rcx), %rax
	leaq	-596(%rbp), %rcx
	movabsq	$4311744768, %r13
	movups	%xmm0, -200(%rbp)
	movq	%r9, %xmm0
	movq	%rax, -144(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -136(%rbp)
	movl	$2, %eax
	movq	%rdx, -168(%rbp)
	xorl	%edx, %edx
	movw	%ax, -128(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -560(%rbp)
	movl	$40, -552(%rbp)
	movl	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movl	$-1, -208(%rbp)
	movq	$0, -160(%rbp)
	movq	%r9, -152(%rbp)
	movb	$1, -72(%rbp)
	movq	%rcx, -616(%rbp)
	movups	%xmm0, -184(%rbp)
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L1005:
	leal	1(%rax), %edx
	movl	%edx, -224(%rbp)
	movq	-560(%rbp), %rdx
	movq	(%rdx,%rax,8), %r14
.L955:
	cmpq	%r13, %r14
	je	.L960
.L975:
	movslq	8(%rbx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L971
	cmpl	12(%rbx), %esi
	jle	.L972
.L971:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L973
	movslq	8(%rbx), %rax
	leal	1(%rax), %esi
.L972:
	movq	24(%rbx), %rdx
	movq	%r14, (%rdx,%rax,8)
	movl	%esi, 8(%rbx)
.L973:
	movslq	-224(%rbp), %rax
	movl	-568(%rbp), %edx
.L953:
	cmpl	%edx, %eax
	jl	.L1005
	cmpl	$39, %edx
	jle	.L956
	leaq	-568(%rbp), %rdi
	movq	%r15, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L960
	movl	-568(%rbp), %edx
.L956:
	addl	$1, %edx
	movq	-616(%rbp), %rsi
	movq	%r12, %rdi
	movl	%edx, -568(%rbp)
	movq	%r15, %rdx
	call	_ZN6icu_6725FCDUTF16CollationIterator14handleNextCE32ERiR10UErrorCode@PLT
	movzbl	%al, %edx
	cmpb	$-65, %al
	jbe	.L999
	movq	-576(%rbp), %rsi
	cmpl	$192, %edx
	je	.L1006
.L969:
	cmpl	$193, %edx
	jne	.L970
	leal	-193(%rax), %ecx
	movslq	-224(%rbp), %rax
	salq	$32, %rcx
	leal	1(%rax), %edx
	orq	$83887360, %rcx
	movl	%edx, -224(%rbp)
	movq	-560(%rbp), %rdx
	movq	%rcx, %r14
	movq	%rcx, (%rdx,%rax,8)
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L1006:
	movl	-596(%rbp), %eax
	testl	%eax, %eax
	js	.L1007
	movq	32(%rsi), %rsi
	movq	(%rsi), %rdi
	movq	16(%rdi), %rcx
	cmpl	$55295, %eax
	jle	.L1008
	cmpl	$65535, %eax
	jg	.L966
	cmpl	$56320, %eax
	movq	(%rdi), %r9
	movl	$320, %edx
	movl	$0, %edi
	cmovl	%edx, %edi
	movl	%eax, %edx
	andl	$31, %eax
	sarl	$5, %edx
	addl	%edi, %edx
	movslq	%edx, %rdx
	movzwl	(%r9,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L965:
	movl	(%rcx,%rdx), %eax
	movzbl	%al, %edx
	cmpb	$-65, %al
	ja	.L969
	.p2align 4,,10
	.p2align 3
.L999:
	movabsq	$-281474976710656, %rdi
	movq	%rax, %rcx
	sall	$16, %eax
	salq	$32, %rcx
	andl	$-16777216, %eax
	sall	$8, %edx
	andq	%rdi, %rcx
	orq	%rax, %rcx
	movl	%edx, %eax
	orq	%rax, %rcx
	movslq	-224(%rbp), %rax
	movq	%rcx, %r14
	leal	1(%rax), %edx
	movl	%edx, -224(%rbp)
	movq	-560(%rbp), %rdx
	movq	%rcx, (%rdx,%rax,8)
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1003:
	movslq	-224(%rbp), %rax
	movabsq	$4311744768, %rdi
	leal	1(%rax), %edx
	movl	%edx, -224(%rbp)
	movq	-560(%rbp), %rdx
	movq	%rdi, (%rdx,%rax,8)
	.p2align 4,,10
	.p2align 3
.L938:
	movq	%r12, %rdi
	call	_ZN6icu_6722UTF16CollationIteratorD1Ev@PLT
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L930:
	movl	12(%rsi), %eax
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L949:
	movl	-596(%rbp), %edx
	movq	%r15, %r8
	movl	%eax, %ecx
	movq	%r12, %rdi
	call	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode@PLT
	movq	%rax, %r14
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L970:
	movl	-596(%rbp), %edx
	movq	%r15, %r8
	movl	%eax, %ecx
	movq	%r12, %rdi
	call	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode@PLT
	movq	%rax, %r14
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L977:
	xorl	%edx, %edx
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L1004:
	movl	%eax, %edx
	movq	(%rdi), %rdi
	sarl	$5, %edx
	movslq	%edx, %rdx
.L996:
	movzwl	(%rdi,%rdx,2), %edx
	andl	$31, %eax
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L1008:
	movl	%eax, %edx
	movq	(%rdi), %rdi
	sarl	$5, %edx
	movslq	%edx, %rdx
.L998:
	movzwl	(%rdi,%rdx,2), %edx
	andl	$31, %eax
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L965
.L966:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L965
	cmpl	44(%rdi), %eax
	jl	.L968
	movslq	48(%rdi), %rdx
	salq	$2, %rdx
	jmp	.L965
.L945:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L944
	cmpl	44(%rdi), %eax
	jl	.L947
	movslq	48(%rdi), %rdx
	salq	$2, %rdx
	jmp	.L944
.L968:
	movl	%eax, %edx
	movq	(%rdi), %rdi
	movl	%eax, %r9d
	sarl	$11, %edx
	sarl	$5, %r9d
	addl	$2080, %edx
	andl	$63, %r9d
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	addl	%r9d, %edx
	movslq	%edx, %rdx
	jmp	.L998
.L947:
	movl	%eax, %edx
	movq	(%rdi), %rdi
	movl	%eax, %r9d
	sarl	$11, %edx
	sarl	$5, %r9d
	addl	$2080, %edx
	andl	$63, %r9d
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	addl	%r9d, %edx
	movslq	%edx, %rdx
	jmp	.L996
.L1002:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3587:
	.size	_ZNK6icu_6717RuleBasedCollator14internalGetCEsERKNS_13UnicodeStringERNS_9UVector64ER10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator14internalGetCEsERKNS_13UnicodeStringERNS_9UVector64ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator17initMaxExpansionsER10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator17initMaxExpansionsER10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator17initMaxExpansionsER10UErrorCode:
.LFB3593:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L1023
	testl	%eax, %eax
	setle	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1023:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %r12
	movl	392(%r12), %eax
	leaq	392(%r12), %r13
	cmpl	$2, %eax
	jne	.L1024
.L1011:
	movl	396(%r12), %eax
	testl	%eax, %eax
	jle	.L1025
	movl	%eax, (%rbx)
.L1010:
	testl	%eax, %eax
	setle	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	setle	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1024:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L1011
	movq	24(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6724CollationElementIterator20computeMaxExpansionsEPKNS_13CollationDataER10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rax, 384(%r12)
	movl	(%rbx), %eax
	movl	%eax, 396(%r12)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movl	(%rbx), %eax
	jmp	.L1010
	.cfi_endproc
.LFE3593:
	.size	_ZNK6icu_6717RuleBasedCollator17initMaxExpansionsER10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator17initMaxExpansionsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator15getMaxExpansionEi
	.type	_ZNK6icu_6717RuleBasedCollator15getMaxExpansionEi, @function
_ZNK6icu_6717RuleBasedCollator15getMaxExpansionEi:
.LFB3596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	movl	392(%r12), %eax
	cmpl	$2, %eax
	jne	.L1037
.L1027:
	movl	396(%r12), %eax
	testl	%eax, %eax
	jle	.L1028
	movl	%eax, -44(%rbp)
.L1028:
	movq	24(%rbx), %rax
	movl	%r13d, %esi
	movq	384(%rax), %rdi
	call	_ZN6icu_6724CollationElementIterator15getMaxExpansionEPK10UHashtablei@PLT
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1038
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1037:
	.cfi_restore_state
	leaq	392(%r12), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L1027
	movq	24(%r12), %rdi
	leaq	-44(%rbp), %rsi
	call	_ZN6icu_6724CollationElementIterator20computeMaxExpansionsEPKNS_13CollationDataER10UErrorCode@PLT
	movq	%r14, %rdi
	movq	%rax, 384(%r12)
	movl	-44(%rbp), %eax
	movl	%eax, 396(%r12)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L1028
.L1038:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3596:
	.size	_ZNK6icu_6717RuleBasedCollator15getMaxExpansionEi, .-_ZNK6icu_6717RuleBasedCollator15getMaxExpansionEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollatorD2Ev
	.type	_ZN6icu_6717RuleBasedCollatorD2Ev, @function
_ZN6icu_6717RuleBasedCollatorD2Ev:
.LFB3483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717RuleBasedCollatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1040
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	$0, 16(%r12)
.L1040:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1041
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	$0, 32(%r12)
.L1041:
	leaq	40(%r12), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678CollatorD2Ev@PLT
	.cfi_endproc
.LFE3483:
	.size	_ZN6icu_6717RuleBasedCollatorD2Ev, .-_ZN6icu_6717RuleBasedCollatorD2Ev
	.globl	_ZN6icu_6717RuleBasedCollatorD1Ev
	.set	_ZN6icu_6717RuleBasedCollatorD1Ev,_ZN6icu_6717RuleBasedCollatorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollatorD0Ev
	.type	_ZN6icu_6717RuleBasedCollatorD0Ev, @function
_ZN6icu_6717RuleBasedCollatorD0Ev:
.LFB3485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717RuleBasedCollatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1050
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	$0, 16(%r12)
.L1050:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1051
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	$0, 32(%r12)
.L1051:
	leaq	40(%r12), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_678CollatorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3485:
	.size	_ZN6icu_6717RuleBasedCollatorD0Ev, .-_ZN6icu_6717RuleBasedCollatorD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator14getTailoredSetER10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator14getTailoredSetER10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator14getTailoredSetER10UErrorCode:
.LFB3501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movl	(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1059
	movq	%rdi, %r13
	movl	$200, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1061
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	8(%r13), %rsi
	cmpq	$0, 32(%rsi)
	je	.L1059
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movq	%r12, -128(%rbp)
	movq	%rax, -120(%rbp)
	leaq	-144(%rbp), %rdi
	movl	$2, %eax
	movw	%ax, -112(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	$0, -56(%rbp)
	movl	$0, -48(%rbp)
	call	_ZN6icu_6711TailoredSet7forDataEPKNS_13CollationDataER10UErrorCode@PLT
	leaq	-120(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L1065
.L1059:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1066
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1065:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L1059
.L1066:
	call	__stack_chk_fail@PLT
.L1061:
	movl	$7, (%rbx)
	jmp	.L1059
	.cfi_endproc
.LFE3501:
	.size	_ZNK6icu_6717RuleBasedCollator14getTailoredSetER10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator14getTailoredSetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollatoreqERKNS_8CollatorE
	.type	_ZNK6icu_6717RuleBasedCollatoreqERKNS_8CollatorE, @function
_ZNK6icu_6717RuleBasedCollatoreqERKNS_8CollatorE:
.LFB3491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L1067
	movq	%rdi, %r12
	movq	%rsi, %r13
	call	_ZNK6icu_678CollatoreqERKS0_@PLT
	testb	%al, %al
	jne	.L1069
.L1070:
	xorl	%eax, %eax
.L1067:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1134
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1069:
	.cfi_restore_state
	movq	16(%r13), %rsi
	movq	16(%r12), %rdi
	call	_ZNK6icu_6717CollationSettingseqERKS0_@PLT
	testb	%al, %al
	je	.L1070
	movq	8(%r12), %rdx
	movq	8(%r13), %rax
	cmpq	%rax, %rdx
	je	.L1083
	movq	32(%rdx), %rdx
	movq	32(%rax), %rcx
	testq	%rdx, %rdx
	sete	%dil
	testq	%rcx, %rcx
	sete	%sil
	xorl	%eax, %eax
	cmpb	%sil, %dil
	jne	.L1067
	testq	%rdx, %rdx
	je	.L1073
	movq	24(%r12), %rax
	movswl	48(%rax), %eax
	shrl	$5, %eax
	jne	.L1073
.L1077:
	movq	(%r12), %rax
	leaq	_ZNK6icu_6717RuleBasedCollator14getTailoredSetER10UErrorCode(%rip), %r15
	movl	$0, -164(%rbp)
	movq	248(%rax), %rbx
	cmpq	%r15, %rbx
	jne	.L1135
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1084
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	8(%r12), %rsi
	cmpq	$0, 32(%rsi)
	je	.L1085
	leaq	-164(%rbp), %r8
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r10d
	movq	%r8, %rdx
	leaq	-160(%rbp), %rdi
	movq	%r8, -184(%rbp)
	movq	%rax, -136(%rbp)
	movw	%r10w, -128(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%r14, -144(%rbp)
	movq	$0, -72(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN6icu_6711TailoredSet7forDataEPKNS_13CollationDataER10UErrorCode@PLT
	leaq	-136(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-164(%rbp), %r11d
	movq	-184(%rbp), %r8
	testl	%r11d, %r11d
	jg	.L1136
	movq	0(%r13), %rax
	movq	248(%rax), %rdx
	cmpq	%rbx, %rdx
	jne	.L1087
.L1096:
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1089
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	8(%r13), %rsi
	cmpq	$0, 32(%rsi)
	je	.L1090
	movl	$2, %edi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, -144(%rbp)
	movw	%di, -128(%rbp)
	leaq	-164(%rbp), %rdx
	leaq	-160(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	$0, -72(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN6icu_6711TailoredSet7forDataEPKNS_13CollationDataER10UErrorCode@PLT
	leaq	-136(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-164(%rbp), %r8d
	testl	%r8d, %r8d
	jg	.L1137
.L1091:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6710UnicodeSeteqERKS0_@PLT
	testb	%al, %al
	setne	%al
.L1094:
	testq	%r12, %r12
	je	.L1092
.L1095:
	movq	%r12, %rdi
	movb	%al, -184(%rbp)
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movzbl	-184(%rbp), %eax
.L1092:
	testq	%r14, %r14
	je	.L1067
	movq	%r14, %rdi
	movb	%al, -184(%rbp)
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movzbl	-184(%rbp), %eax
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	24(%r13), %rsi
	movswl	48(%rsi), %eax
	testq	%rcx, %rcx
	je	.L1076
	movswl	%ax, %edx
	shrl	$5, %edx
	je	.L1077
.L1076:
	movq	24(%r12), %rcx
	movl	%eax, %edi
	andl	$1, %edi
	movswl	48(%rcx), %edx
	testb	$1, %dl
	jne	.L1078
	testw	%dx, %dx
	js	.L1079
	sarl	$5, %edx
.L1080:
	testw	%ax, %ax
	js	.L1081
	sarl	$5, %eax
.L1082:
	testb	%dil, %dil
	jne	.L1077
	cmpl	%edx, %eax
	jne	.L1077
	addq	$40, %rsi
	leaq	40(%rcx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%dil
	.p2align 4,,10
	.p2align 3
.L1078:
	testb	%dil, %dil
	je	.L1077
.L1083:
	movl	$1, %eax
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1135:
	leaq	-164(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	*%rbx
	movq	%rax, %r14
.L1085:
	movq	0(%r13), %rax
	movq	248(%rax), %rdx
	cmpq	%r15, %rdx
	jne	.L1133
	movl	-164(%rbp), %r9d
	testl	%r9d, %r9d
	jle	.L1096
.L1100:
	xorl	%eax, %eax
	jmp	.L1092
	.p2align 4,,10
	.p2align 3
.L1090:
	movl	-164(%rbp), %eax
	testl	%eax, %eax
	jle	.L1091
	xorl	%eax, %eax
	jmp	.L1095
.L1084:
	movl	$7, -164(%rbp)
	movq	0(%r13), %rax
	movq	248(%rax), %rdx
	xorl	%eax, %eax
	cmpq	%rbx, %rdx
	je	.L1067
	.p2align 4,,10
	.p2align 3
.L1133:
	leaq	-164(%rbp), %r8
.L1087:
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	*%rdx
	movl	-164(%rbp), %edx
	movq	%rax, %r12
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1094
	jmp	.L1091
.L1137:
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movl	-164(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L1100
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6710UnicodeSeteqERKS0_@PLT
	testb	%al, %al
	setne	%al
	jmp	.L1092
.L1081:
	movl	52(%rsi), %eax
	jmp	.L1082
.L1079:
	movl	52(%rcx), %edx
	jmp	.L1080
.L1136:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L1085
.L1134:
	call	__stack_chk_fail@PLT
.L1089:
	movl	$7, -164(%rbp)
	xorl	%eax, %eax
	jmp	.L1092
	.cfi_endproc
.LFE3491:
	.size	_ZNK6icu_6717RuleBasedCollatoreqERKNS_8CollatorE, .-_ZNK6icu_6717RuleBasedCollatoreqERKNS_8CollatorE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator8hashCodeEv
	.type	_ZNK6icu_6717RuleBasedCollator8hashCodeEv, @function
_ZNK6icu_6717RuleBasedCollator8hashCodeEv:
.LFB3494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$192, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6717CollationSettings8hashCodeEv@PLT
	movl	%eax, %r12d
	movq	8(%rbx), %rax
	cmpq	$0, 32(%rax)
	je	.L1138
	movq	(%rbx), %rax
	leaq	_ZNK6icu_6717RuleBasedCollator14getTailoredSetER10UErrorCode(%rip), %rdx
	movl	$0, -212(%rbp)
	movq	248(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1140
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1144
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	8(%rbx), %rsi
	cmpq	$0, 32(%rsi)
	je	.L1142
	movl	$2, %edi
	pxor	%xmm0, %xmm0
	movq	%r13, -128(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%di, -112(%rbp)
	leaq	-212(%rbp), %rdx
	leaq	-144(%rbp), %rdi
	movq	%rax, -120(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	$0, -56(%rbp)
	movl	$0, -48(%rbp)
	call	_ZN6icu_6711TailoredSet7forDataEPKNS_13CollationDataER10UErrorCode@PLT
	leaq	-120(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-212(%rbp), %r8d
	testl	%r8d, %r8d
	jg	.L1170
.L1143:
	leaq	-208(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1171:
	movl	%eax, %edx
	movq	(%rsi), %rsi
	andl	$31, %eax
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L1148:
	xorl	(%rcx,%rdx), %r12d
.L1152:
	movq	%r14, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	je	.L1146
	movl	-200(%rbp), %eax
	cmpl	$-1, %eax
	je	.L1146
	movq	8(%rbx), %rdx
	movq	(%rdx), %rsi
	movq	16(%rsi), %rcx
	cmpl	$55295, %eax
	jbe	.L1171
	cmpl	$65535, %eax
	ja	.L1149
	cmpl	$56320, %eax
	movl	$320, %edx
	movq	(%rsi), %rdi
	movl	$0, %esi
	cmovl	%edx, %esi
	movl	%eax, %edx
	sarl	$5, %edx
.L1169:
	addl	%esi, %edx
	andl	$31, %eax
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1140:
	leaq	-212(%rbp), %rsi
	movq	%rbx, %rdi
	call	*%rax
	movl	-212(%rbp), %edx
	movq	%rax, %r13
	testl	%edx, %edx
	jle	.L1143
	xorl	%r12d, %r12d
.L1145:
	testq	%r13, %r13
	je	.L1138
.L1153:
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L1138:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1172
	addq	$192, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1149:
	.cfi_restore_state
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L1148
	cmpl	44(%rsi), %eax
	jl	.L1151
	movslq	48(%rsi), %rdx
	salq	$2, %rdx
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	%r14, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	jmp	.L1145
	.p2align 4,,10
	.p2align 3
.L1142:
	movl	-212(%rbp), %eax
	testl	%eax, %eax
	jle	.L1143
	xorl	%r12d, %r12d
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1151:
	movl	%eax, %edx
	movq	(%rsi), %rdi
	sarl	$11, %edx
	addl	$2080, %edx
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %esi
	movl	%eax, %edx
	sarl	$5, %edx
	andl	$63, %edx
	jmp	.L1169
.L1170:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movl	-212(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L1143
.L1144:
	xorl	%r12d, %r12d
	jmp	.L1138
.L1172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3494:
	.size	_ZNK6icu_6717RuleBasedCollator8hashCodeEv, .-_ZNK6icu_6717RuleBasedCollator8hashCodeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator23internalAddContractionsEiRNS_10UnicodeSetER10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator23internalAddContractionsEiRNS_10UnicodeSetER10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator23internalAddContractionsEiRNS_10UnicodeSetER10UErrorCode:
.LFB3509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$808, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L1173
	leaq	-792(%rbp), %r14
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movl	%esi, %r13d
	leaq	-832(%rbp), %r8
	movq	%r14, %rdi
	leaq	-592(%rbp), %r15
	movq	%rdx, -824(%rbp)
	movw	%ax, -800(%rbp)
	movq	%rcx, %r12
	movq	%r8, -840(%rbp)
	movq	$0, -832(%rbp)
	movq	$0, -816(%rbp)
	movq	$0, -808(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	-840(%rbp), %r8
	movq	8(%rbx), %rsi
	movl	$2, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rcx
	movw	%dx, -384(%rbp)
	movl	%r13d, %edx
	movq	%r8, %rdi
	movq	%rax, -392(%rbp)
	movq	$0, -328(%rbp)
	movl	$0, -72(%rbp)
	call	_ZN6icu_6725ContractionsAndExpansions12forCodePointEPKNS_13CollationDataEiR10UErrorCode@PLT
	leaq	-392(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
.L1173:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1177
	addq	$808, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1177:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3509:
	.size	_ZNK6icu_6717RuleBasedCollator23internalAddContractionsEiRNS_10UnicodeSetER10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator23internalAddContractionsEiRNS_10UnicodeSetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator36internalGetContractionsAndExpansionsEPNS_10UnicodeSetES2_aR10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator36internalGetContractionsAndExpansionsEPNS_10UnicodeSetES2_aR10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator36internalGetContractionsAndExpansionsEPNS_10UnicodeSetES2_aR10UErrorCode:
.LFB3505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$792, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L1178
	movq	%rdi, %r13
	movq	%rsi, %r14
	movq	%rdx, %rbx
	movl	%ecx, %r15d
	movq	%r8, %r12
	testq	%rsi, %rsi
	je	.L1180
	movq	%rsi, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
.L1180:
	testq	%rbx, %rbx
	je	.L1181
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
.L1181:
	movq	%r14, -824(%rbp)
	leaq	-792(%rbp), %r14
	movb	%r15b, -800(%rbp)
	movq	%r14, %rdi
	leaq	-592(%rbp), %r15
	movq	%rbx, -816(%rbp)
	leaq	-832(%rbp), %rbx
	movq	$0, -832(%rbp)
	movq	$0, -808(%rbp)
	movb	$0, -799(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	8(%r13), %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$0, -72(%rbp)
	movq	%rax, -392(%rbp)
	movl	$2, %eax
	movw	%ax, -384(%rbp)
	movq	$0, -328(%rbp)
	call	_ZN6icu_6725ContractionsAndExpansions7forDataEPKNS_13CollationDataER10UErrorCode@PLT
	leaq	-392(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
.L1178:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1190
	addq	$792, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1190:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3505:
	.size	_ZNK6icu_6717RuleBasedCollator36internalGetContractionsAndExpansionsEPNS_10UnicodeSetES2_aR10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator36internalGetContractionsAndExpansionsEPNS_10UnicodeSetES2_aR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator8isUnsafeEi
	.type	_ZNK6icu_6717RuleBasedCollator8isUnsafeEi, @function
_ZNK6icu_6717RuleBasedCollator8isUnsafeEi:
.LFB3591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	8(%rdi), %r12
	movq	16(%rdi), %rax
	movq	80(%r12), %rdi
	movl	24(%rax), %r13d
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L1201
	andl	$2, %r13d
	je	.L1191
	cmpl	$1631, %ebx
	jle	.L1209
	movq	(%r12), %rcx
	movq	16(%rcx), %rdi
	cmpl	$55295, %ebx
	jg	.L1195
	movl	%ebx, %edx
	movq	(%rcx), %rcx
	andl	$31, %ebx
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	leal	(%rbx,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L1196:
	movl	(%rdi,%rdx), %edx
	cmpb	$-65, %dl
	jbe	.L1191
	andl	$15, %edx
	cmpl	$10, %edx
	sete	%al
.L1191:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1201:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1209:
	.cfi_restore_state
	subl	$48, %ebx
	cmpl	$9, %ebx
	setbe	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1195:
	.cfi_restore_state
	cmpl	$65535, %ebx
	jbe	.L1210
	movl	$512, %edx
	cmpl	$1114111, %ebx
	ja	.L1196
	cmpl	44(%rcx), %ebx
	jl	.L1199
	movslq	48(%rcx), %rdx
	salq	$2, %rdx
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1210:
	cmpl	$56320, %ebx
	movl	$320, %edx
	movq	(%rcx), %rsi
	movl	$0, %ecx
	cmovl	%edx, %ecx
	movl	%ebx, %edx
	sarl	$5, %edx
.L1208:
	addl	%ecx, %edx
	andl	$31, %ebx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	leal	(%rbx,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L1196
.L1199:
	movl	%ebx, %edx
	movq	(%rcx), %rsi
	sarl	$11, %edx
	addl	$2080, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %ecx
	movl	%ebx, %edx
	sarl	$5, %edx
	andl	$63, %edx
	jmp	.L1208
	.cfi_endproc
.LFE3591:
	.size	_ZNK6icu_6717RuleBasedCollator8isUnsafeEi, .-_ZNK6icu_6717RuleBasedCollator8isUnsafeEi
	.align 2
	.p2align 4
	.type	_ZNK6icu_6717RuleBasedCollator9doCompareEPKDsiS2_iR10UErrorCode.part.0, @function
_ZNK6icu_6717RuleBasedCollator9doCompareEPKDsiS2_iR10UErrorCode.part.0:
.LFB4921:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1272, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -1208(%rbp)
	movq	%r9, -1248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L1546
	movslq	%edx, %rcx
	leaq	(%rsi,%rcx,2), %rax
	movq	%rax, -1264(%rbp)
	movslq	%r8d, %rax
	leaq	(%r14,%rax,2), %rdi
	movq	%rdi, -1272(%rbp)
	je	.L1218
	testl	%r8d, %r8d
	je	.L1219
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1549:
	addl	$1, %r12d
	cmpl	%r12d, %edx
	je	.L1547
	addq	$2, %rbx
	cmpl	%r12d, %r8d
	je	.L1548
.L1220:
	movzwl	(%r11,%rbx), %r13d
	leaq	(%r14,%rbx), %r15
	cmpw	(%r14,%rbx), %r13w
	je	.L1549
	movq	-1208(%rbp), %rdi
	movq	16(%rdi), %rsi
	movl	24(%rsi), %ecx
	movq	%rsi, %rax
	andl	$2, %ecx
	movl	%ecx, -1276(%rbp)
	setne	-1232(%rbp)
	movzbl	-1232(%rbp), %ecx
	movl	%ecx, %r9d
	movq	8(%rdi), %rcx
	movq	%rcx, %r10
	testl	%r12d, %r12d
	jne	.L1550
	movl	80(%rsi), %r10d
	testl	%r10d, %r10d
	js	.L1551
	cmpw	$383, (%r11)
	movq	%r11, -1216(%rbp)
	movq	%r14, %r15
	ja	.L1374
.L1373:
	cmpw	$383, (%r15)
	jbe	.L1262
.L1362:
	movl	24(%rsi), %edi
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L1223:
	movq	(%rcx), %rdx
	leaq	-1072(%rbp), %r10
	andl	$1, %edi
	movq	%rcx, -1104(%rbp)
	movq	%r10, -1088(%rbp)
	leaq	-1120(%rbp), %r12
	movq	%rdx, -1112(%rbp)
	movl	$0, -1096(%rbp)
	movl	$40, -1080(%rbp)
	movb	$0, -1076(%rbp)
	movl	$0, -752(%rbp)
	movq	$0, -744(%rbp)
	movl	$-1, -736(%rbp)
	jne	.L1266
	movq	-1264(%rbp), %rbx
	leaq	16+_ZTVN6icu_6722UTF16CollationIteratorE(%rip), %rsi
	movq	%r11, %xmm0
	movzbl	-1232(%rbp), %edi
	movhps	-1216(%rbp), %xmm0
	movq	%rsi, -1120(%rbp)
	movq	%r15, %xmm1
	leaq	-544(%rbp), %r11
	movq	%rsi, -592(%rbp)
	movq	-1272(%rbp), %rsi
	movq	%rbx, -712(%rbp)
	leaq	-592(%rbp), %rbx
	movq	%rcx, -576(%rbp)
	movq	-1248(%rbp), %rcx
	movups	%xmm0, -728(%rbp)
	movq	%r14, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movb	%dil, -732(%rbp)
	movq	%rdx, -584(%rbp)
	movq	%rax, %rdx
	movb	%dil, -204(%rbp)
	movq	%r12, %rdi
	movq	%rsi, -184(%rbp)
	movq	%rbx, %rsi
	movq	%r11, -560(%rbp)
	movups	%xmm0, -200(%rbp)
	movl	$0, -568(%rbp)
	movl	$40, -552(%rbp)
	movb	$0, -548(%rbp)
	movl	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movl	$-1, -208(%rbp)
	call	_ZN6icu_6716CollationCompare21compareUpToQuaternaryERNS_17CollationIteratorES2_RKNS_17CollationSettingsER10UErrorCode@PLT
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	_ZN6icu_6722UTF16CollationIteratorD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6722UTF16CollationIteratorD1Ev@PLT
.L1265:
	testl	%r13d, %r13d
	jne	.L1211
	movq	-1208(%rbp), %rsi
	movq	16(%rsi), %rax
	movl	24(%rax), %r12d
	cmpl	$61439, %r12d
	jle	.L1221
	movq	-1248(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1221
	movq	8(%rsi), %rax
	notl	%r12d
	movq	48(%rax), %rbx
	andl	$1, %r12d
	je	.L1267
	movl	$4294967295, %ecx
	movl	$-1, %edx
	movq	-1216(%rbp), %xmm0
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_116UTF16NFDIteratorE(%rip), %rax
	movq	%rax, -1120(%rbp)
	movl	$-1, %r8d
	movhps	-1264(%rbp), %xmm0
	movq	%rax, -592(%rbp)
	movl	$-1, %eax
	movaps	%xmm0, -1088(%rbp)
	movq	%r15, %xmm0
	movhps	-1272(%rbp), %xmm0
	movq	%rcx, -1096(%rbp)
	movq	%rcx, -568(%rbp)
	movaps	%xmm0, -560(%rbp)
	testl	%edx, %edx
	js	.L1268
	cmpl	%edx, -1092(%rbp)
	jne	.L1269
.L1559:
	movl	%r8d, -1096(%rbp)
.L1268:
	movq	-1088(%rbp), %rdx
	movq	-1080(%rbp), %rcx
	cmpq	%rcx, %rdx
	je	.L1377
	movzwl	(%rdx), %r14d
	leaq	2(%rdx), %rsi
	movq	%rsi, -1088(%rbp)
	movl	%r14d, %edi
	testl	%r14d, %r14d
	jne	.L1271
	testq	%rcx, %rcx
	je	.L1552
.L1271:
	movl	%edi, %r9d
	andl	$-1024, %r9d
	cmpl	$55296, %r9d
	jne	.L1270
	cmpq	%rsi, %rcx
	jne	.L1553
.L1270:
	testl	%eax, %eax
	js	.L1272
	cmpl	%eax, -564(%rbp)
	jne	.L1273
	movl	%r8d, -568(%rbp)
.L1272:
	movq	-560(%rbp), %rax
	movq	-552(%rbp), %rcx
	cmpq	%rcx, %rax
	je	.L1542
	movzwl	(%rax), %edx
	leaq	2(%rax), %rsi
	movq	%rsi, -560(%rbp)
	movl	%edx, %r15d
	testl	%edx, %edx
	jne	.L1281
	testq	%rcx, %rcx
	je	.L1554
.L1281:
	movl	%edx, %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	jne	.L1274
	cmpq	%rsi, %rcx
	jne	.L1555
.L1274:
	cmpl	%r14d, %r15d
	je	.L1556
	testl	%r14d, %r14d
	js	.L1285
	cmpl	$65534, %r14d
	je	.L1378
	movl	-1096(%rbp), %eax
	testl	%eax, %eax
	js	.L1557
.L1287:
	testl	%r15d, %r15d
	js	.L1290
	cmpl	$65534, %r15d
	je	.L1558
.L1349:
	movl	-568(%rbp), %eax
	testl	%eax, %eax
	js	.L1365
.L1289:
	cmpl	%r15d, %r14d
	jl	.L1398
.L1348:
	cmpl	%r14d, %r15d
	jl	.L1290
.L1284:
	movl	-1096(%rbp), %edx
	movl	-568(%rbp), %eax
	testl	%edx, %edx
	js	.L1268
	cmpl	%edx, -1092(%rbp)
	je	.L1559
.L1269:
	leal	1(%rdx), %ecx
	movq	-1112(%rbp), %rsi
	movl	%ecx, -1096(%rbp)
	movslq	%edx, %rcx
	movzwl	(%rsi,%rcx,2), %r14d
	leaq	(%rcx,%rcx), %rdi
	movl	%r14d, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L1270
	addl	$2, %edx
	sall	$10, %r14d
	movl	%edx, -1096(%rbp)
	movzwl	2(%rsi,%rdi), %edx
	leal	-56613888(%rdx,%r14), %r14d
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1546:
	movzwl	(%rsi), %r13d
	cmpw	%r13w, (%rcx)
	jne	.L1213
	movl	$1, %eax
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1214:
	movzwl	(%r11,%rax,2), %r13d
	movl	%eax, %r12d
	leaq	(%rax,%rax), %rbx
	addq	$1, %rax
	cmpw	-2(%r14,%rax,2), %r13w
	jne	.L1560
.L1216:
	testw	%r13w, %r13w
	jne	.L1214
.L1221:
	xorl	%r13d, %r13d
.L1211:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1561
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1551:
	.cfi_restore_state
	movq	%r11, -1216(%rbp)
	movq	%r14, %r15
.L1374:
	movl	24(%rsi), %edi
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1266:
	movq	%r11, %xmm0
	movq	-1216(%rbp), %rsi
	movq	-1264(%rbp), %rbx
	movq	%r11, %xmm2
	movq	%r15, %xmm3
	movzbl	-1232(%rbp), %r10d
	movq	%r14, %xmm4
	movl	$2, %r9d
	movq	%rsi, %xmm7
	leaq	-544(%rbp), %r11
	movq	%rsi, -696(%rbp)
	leaq	16+_ZTVN6icu_6725FCDUTF16CollationIteratorE(%rip), %rdi
	punpcklqdq	%xmm7, %xmm0
	movb	%r10b, -732(%rbp)
	movq	48(%rcx), %r8
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rsi
	movups	%xmm0, -728(%rbp)
	movq	%rbx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, -576(%rbp)
	movq	-1272(%rbp), %rcx
	movups	%xmm0, -712(%rbp)
	movq	%r14, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movq	%r8, -672(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movw	%r9w, -656(%rbp)
	movq	%r11, -560(%rbp)
	movups	%xmm0, -184(%rbp)
	movq	%rdi, -1120(%rbp)
	movq	%rbx, -680(%rbp)
	leaq	-592(%rbp), %rbx
	movq	%rsi, -664(%rbp)
	movq	%rdx, -584(%rbp)
	movq	%rax, %rdx
	movb	%r10b, -204(%rbp)
	movl	$2, %r10d
	movq	%rdi, -592(%rbp)
	movq	%r12, %rdi
	movq	%rcx, -152(%rbp)
	movq	$0, -688(%rbp)
	movb	$1, -600(%rbp)
	movl	$0, -568(%rbp)
	movl	$40, -552(%rbp)
	movb	$0, -548(%rbp)
	movl	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movl	$-1, -208(%rbp)
	movq	%r15, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	%r8, -144(%rbp)
	movq	-1248(%rbp), %rcx
	movq	%rsi, -136(%rbp)
	movq	%rbx, %rsi
	movw	%r10w, -128(%rbp)
	movb	$1, -72(%rbp)
	call	_ZN6icu_6716CollationCompare21compareUpToQuaternaryERNS_17CollationIteratorES2_RKNS_17CollationSettingsER10UErrorCode@PLT
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	_ZN6icu_6725FCDUTF16CollationIteratorD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6725FCDUTF16CollationIteratorD1Ev@PLT
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1550:
	cmpl	%r12d, %edx
	jne	.L1347
.L1230:
	cmpl	%r12d, %r8d
	jne	.L1376
.L1249:
	movq	-1208(%rbp), %rax
.L1528:
	leaq	(%r11,%rbx), %rdi
	movq	16(%rax), %rsi
	movq	%rdi, -1216(%rbp)
	movq	%rsi, %rax
.L1241:
	movl	80(%rsi), %r10d
	testl	%r10d, %r10d
	js	.L1374
	cmpl	%r12d, %edx
	jne	.L1346
.L1261:
	cmpl	%r12d, %r8d
	jne	.L1373
.L1262:
	movq	88(%rcx), %rdi
	addq	$84, %rsi
	testl	%edx, %edx
	js	.L1263
.L1360:
	movq	%r11, -1224(%rbp)
	subl	%r12d, %r8d
	subl	%r12d, %edx
	movq	%r15, %r9
	subq	$8, %rsp
	pushq	%r8
	movl	%edx, %r8d
.L1529:
	movq	-1216(%rbp), %rcx
	movl	%r10d, %edx
	call	_ZN6icu_6718CollationFastLatin12compareUTF16EPKtS2_iPKDsiS4_i@PLT
	popq	%r11
	movq	-1224(%rbp), %r11
	movl	%eax, %r13d
	popq	%rbx
	cmpl	$-2, %eax
	jne	.L1265
	movq	-1208(%rbp), %rcx
	movq	16(%rcx), %rax
	movq	8(%rcx), %rcx
	movl	24(%rax), %edi
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1560:
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
.L1217:
	movq	-1208(%rbp), %rsi
	movq	16(%rsi), %rax
	movq	8(%rsi), %r10
	movl	24(%rax), %eax
	andl	$2, %eax
	setne	%r9b
	movl	%eax, -1276(%rbp)
	movb	%r9b, -1232(%rbp)
.L1347:
	movq	80(%r10), %rdi
	movzwl	%r13w, %r15d
	movl	%r8d, -1288(%rbp)
	movl	%r15d, %esi
	movl	%edx, -1256(%rbp)
	movq	%r11, -1240(%rbp)
	movb	%r9b, -1224(%rbp)
	movq	%r10, -1216(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movq	-1216(%rbp), %r10
	movzbl	-1224(%rbp), %r9d
	testb	%al, %al
	movq	-1208(%rbp), %rax
	movq	-1240(%rbp), %r11
	movl	-1256(%rbp), %edx
	movl	-1288(%rbp), %r8d
	movq	8(%rax), %rcx
	jne	.L1232
	testb	%r9b, %r9b
	je	.L1562
	cmpl	$1631, %r15d
	jg	.L1234
	subl	$48, %r15d
	cmpl	$9, %r15d
	setbe	-1232(%rbp)
.L1235:
	cmpb	$0, -1232(%rbp)
	jne	.L1232
.L1239:
	movb	$1, -1232(%rbp)
	leaq	(%r14,%rbx), %r15
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1547:
	cmpl	%edx, %r8d
	je	.L1221
	movq	-1208(%rbp), %rsi
	leaq	(%rcx,%rcx), %rbx
	leaq	(%r14,%rbx), %r15
	movq	16(%rsi), %rax
	movq	8(%rsi), %r9
	movl	24(%rax), %eax
	andl	$2, %eax
	movl	%eax, -1276(%rbp)
	setne	-1232(%rbp)
.L1240:
	movzwl	(%r15), %esi
	movq	80(%r9), %rdi
	movl	%r8d, -1288(%rbp)
	movl	%edx, -1256(%rbp)
	movq	%r11, -1240(%rbp)
	movl	%esi, %r13d
	movq	%r9, -1224(%rbp)
	movl	%esi, -1216(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	-1216(%rbp), %esi
	movq	-1224(%rbp), %r9
	testb	%al, %al
	movq	-1208(%rbp), %rax
	movq	-1240(%rbp), %r11
	movl	-1256(%rbp), %edx
	movl	-1288(%rbp), %r8d
	movq	8(%rax), %rcx
	jne	.L1232
	movl	-1276(%rbp), %edi
	testl	%edi, %edi
	je	.L1528
	cmpl	$1631, %esi
	jg	.L1244
	subl	$48, %esi
	cmpl	$9, %esi
	setbe	%al
.L1245:
	testb	%al, %al
	je	.L1249
	.p2align 4,,10
	.p2align 3
.L1232:
	subl	$1, %r12d
	movslq	%r12d, %r13
	leaq	(%r13,%r13), %rdi
	leaq	(%r11,%rdi), %r15
	testl	%r12d, %r12d
	jle	.L1563
	movq	%r11, -1288(%rbp)
	movq	%r15, %r12
	movl	%edx, -1280(%rbp)
	movl	%r8d, -1300(%rbp)
	movq	%r14, -1296(%rbp)
	movq	%rcx, %r14
	.p2align 4,,10
	.p2align 3
.L1251:
	movzwl	(%r12), %ebx
	movq	80(%r14), %rdi
	leaq	(%r13,%r13), %rax
	movl	%r13d, -1224(%rbp)
	movq	%rax, -1256(%rbp)
	movl	%r13d, %r15d
	movl	%ebx, %esi
	movq	%r12, -1216(%rbp)
	movl	%ebx, -1240(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L1252
	movl	-1276(%rbp), %eax
	movl	-1240(%rbp), %r9d
	testl	%eax, %eax
	jne	.L1253
	movq	-1208(%rbp), %rdi
	movl	-1280(%rbp), %edx
	movl	%r13d, %r12d
	movq	-1296(%rbp), %r14
	movq	-1256(%rbp), %r15
	movq	16(%rdi), %rsi
	movq	-1288(%rbp), %r11
	movl	-1300(%rbp), %r8d
	movq	8(%rdi), %rcx
	addq	%r14, %r15
	movq	%rsi, %rax
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1548:
	movzwl	(%r11,%rax,2), %r13d
	leaq	(%rax,%rax), %rbx
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L1253:
	cmpl	$1631, %ebx
	jg	.L1254
	leal	-48(%rbx), %esi
	cmpl	$9, %esi
	setbe	%al
.L1255:
	testb	%al, %al
	je	.L1259
.L1252:
	movl	-1224(%rbp), %r15d
	movq	-1208(%rbp), %rax
	subq	$1, %r13
	subq	$2, %r12
	movq	8(%rax), %r14
	subl	$1, %r15d
	testl	%r13d, %r13d
	jg	.L1251
	movslq	%r15d, %r15
	movq	-1288(%rbp), %r11
	movq	%r14, %rcx
	movq	-1296(%rbp), %r14
	movq	%r15, %r12
	addq	%r15, %r15
	movl	-1280(%rbp), %edx
	movl	-1300(%rbp), %r8d
	leaq	(%r11,%r15), %rax
	addq	%r14, %r15
	movq	%rax, -1216(%rbp)
	movq	-1208(%rbp), %rax
	movq	16(%rax), %rsi
	movq	%rsi, %rax
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1254:
	movq	(%r14), %rax
	movl	%r9d, %edi
	andl	$31, %edi
	movq	16(%rax), %rcx
	movq	(%rax), %r11
	movl	%ebx, %eax
	sarl	$5, %eax
	cmpw	$-10241, %r9w
	jbe	.L1527
	leal	320(%rax), %r9d
	cmpl	$56320, %ebx
	cmovl	%r9d, %eax
.L1527:
	cltq
	movzwl	(%r11,%rax,2), %eax
	leal	(%rdi,%rax,4), %eax
	cltq
	salq	$2, %rax
	movl	(%rcx,%rax), %eax
	cmpb	$-65, %al
	ja	.L1564
.L1259:
	movq	-1208(%rbp), %rcx
	movl	%r15d, %r12d
	movq	-1296(%rbp), %r14
	movq	-1256(%rbp), %r15
	movq	-1288(%rbp), %r11
	movq	16(%rcx), %rsi
	movl	-1280(%rbp), %edx
	movl	-1300(%rbp), %r8d
	movq	8(%rcx), %rcx
	addq	%r14, %r15
	movq	%rsi, %rax
	jmp	.L1241
.L1213:
	movq	$0, -1272(%rbp)
	movq	16(%rdi), %rsi
	movq	$0, -1264(%rbp)
	movl	24(%rsi), %edi
	movq	%rsi, %rax
	movl	%edi, %ecx
	shrl	%ecx
	andl	$1, %ecx
	movb	%cl, -1232(%rbp)
.L1228:
	movq	-1208(%rbp), %rcx
	movl	80(%rsi), %r10d
	movq	8(%rcx), %rcx
	testl	%r10d, %r10d
	js	.L1530
	movq	%r11, -1216(%rbp)
	movq	%r14, %r15
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L1346:
	movq	-1216(%rbp), %rdi
	cmpw	$383, (%rdi)
	jbe	.L1261
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1556:
	testl	%r15d, %r15d
	jns	.L1284
.L1277:
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rbx
	leaq	-592(%rbp), %rdi
	movq	%rbx, -592(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	leaq	-1120(%rbp), %rdi
	movq	%rbx, -1120(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1273:
	leal	1(%rax), %edx
	movq	-584(%rbp), %rcx
	movl	%edx, -568(%rbp)
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %r15d
	leaq	(%rdx,%rdx), %rsi
	movl	%r15d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1274
	addl	$2, %eax
	sall	$10, %r15d
	movl	%eax, -568(%rbp)
	movzwl	2(%rcx,%rsi), %eax
	leal	-56613888(%rax,%r15), %r15d
	jmp	.L1274
	.p2align 4,,10
	.p2align 3
.L1285:
	testl	%r15d, %r15d
	js	.L1284
	cmpl	$65534, %r15d
	je	.L1398
	movl	-568(%rbp), %edx
	testl	%edx, %edx
	jns	.L1398
	movl	$-2, %r14d
.L1365:
	leaq	-564(%rbp), %rcx
	leaq	-576(%rbp), %rdx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movl	$-1, %r8d
	testq	%rax, %rax
	movq	%rax, -584(%rbp)
	je	.L1289
	movl	$1, -568(%rbp)
	movzwl	(%rax), %r15d
	movl	%r15d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1289
	movzwl	2(%rax), %eax
	sall	$10, %r15d
	movl	$2, -568(%rbp)
	leal	-56613888(%r15,%rax), %r15d
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1267:
	movl	$2, %r11d
	movl	$4294967295, %eax
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	-1264(%rbp), %rdx
	movq	-1216(%rbp), %rsi
	pxor	%xmm0, %xmm0
	leaq	-1188(%rbp), %r13
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE(%rip), %r12
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r14
	movq	%r13, %r8
	movw	%r11w, -1064(%rbp)
	movq	%rax, -1096(%rbp)
	movq	%r12, -1120(%rbp)
	movq	%r14, -1072(%rbp)
	movl	$0, -1188(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl7makeFCDEPKDsS2_PNS_16ReorderingBufferER10UErrorCode@PLT
	movq	%rax, %r11
	movl	-1188(%rbp), %eax
	testl	%eax, %eax
	jg	.L1565
	movq	-1264(%rbp), %rax
	cmpq	%rax, %r11
	je	.L1294
	testq	%rax, %rax
	je	.L1566
.L1295:
	leaq	-1072(%rbp), %r10
	movq	%r11, %r9
	subq	-1216(%rbp), %r9
	movq	%r11, -1232(%rbp)
	sarq	%r9
	movq	%r10, %rdi
	movq	%r10, -1208(%rbp)
	movq	%r9, -1224(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	-1064(%rbp), %eax
	movq	-1208(%rbp), %r10
	movq	-1224(%rbp), %r9
	movq	-1232(%rbp), %r11
	testw	%ax, %ax
	js	.L1297
	movswl	%ax, %edx
	sarl	$5, %edx
.L1298:
	movq	-1216(%rbp), %rcx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r10, %rdi
	movq	%r11, -1224(%rbp)
	movq	%r10, -1208(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movzwl	-1064(%rbp), %eax
	movq	-1208(%rbp), %r10
	movq	%rbx, -1184(%rbp)
	movq	$0, -1168(%rbp)
	movq	-1224(%rbp), %r11
	testw	%ax, %ax
	movq	%r10, -1176(%rbp)
	movq	$0, -1160(%rbp)
	movq	$0, -1152(%rbp)
	movl	$0, -1144(%rbp)
	movb	$0, -1140(%rbp)
	js	.L1299
	movswl	%ax, %esi
	sarl	$5, %esi
.L1300:
	leaq	-1184(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r10, -1224(%rbp)
	movq	%rcx, %rdi
	movq	%r11, -1216(%rbp)
	movq	%rcx, -1208(%rbp)
	call	_ZN6icu_6716ReorderingBuffer4initEiR10UErrorCode@PLT
	movq	-1208(%rbp), %rcx
	movq	-1216(%rbp), %r11
	testb	%al, %al
	movq	-1224(%rbp), %r10
	jne	.L1567
.L1301:
	movq	-1168(%rbp), %rax
	testq	%rax, %rax
	je	.L1302
	movq	-1152(%rbp), %rsi
	movq	-1176(%rbp), %rdi
	movq	%r10, -1208(%rbp)
	subq	%rax, %rsi
	sarq	%rsi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movq	-1208(%rbp), %r10
.L1302:
	movl	-1188(%rbp), %r8d
	testl	%r8d, %r8d
	jle	.L1568
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	-1272(%rbp), %rdx
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$4294967295, %eax
	movl	$2, %r9d
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r10, -1208(%rbp)
	movq	%r14, -544(%rbp)
	movq	%rax, -568(%rbp)
	movq	%r12, -592(%rbp)
	movw	%r9w, -536(%rbp)
	movl	$0, -1188(%rbp)
	movaps	%xmm0, -560(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl7makeFCDEPKDsS2_PNS_16ReorderingBufferER10UErrorCode@PLT
	movl	-1188(%rbp), %r10d
	leaq	-544(%rbp), %r11
	movq	%rax, %r14
	testl	%r10d, %r10d
	movq	-1208(%rbp), %r10
	jg	.L1292
	movq	-1272(%rbp), %rax
	cmpq	%r14, %rax
	je	.L1307
	testq	%rax, %rax
	je	.L1569
.L1308:
	movq	%r14, %r9
	leaq	-544(%rbp), %r11
	movq	%r10, -1224(%rbp)
	subq	%r15, %r9
	movq	%r11, %rdi
	movq	%r11, -1208(%rbp)
	sarq	%r9
	movq	%r9, -1216(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	-536(%rbp), %eax
	movq	-1208(%rbp), %r11
	movq	-1216(%rbp), %r9
	movq	-1224(%rbp), %r10
	testw	%ax, %ax
	js	.L1309
	movswl	%ax, %edx
	sarl	$5, %edx
.L1310:
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r11, %rdi
	movq	%r15, %rcx
	movq	%r10, -1216(%rbp)
	movq	%r11, -1208(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movzwl	-536(%rbp), %eax
	movq	-1208(%rbp), %r11
	movq	%rbx, -1184(%rbp)
	movq	$0, -1168(%rbp)
	movq	-1216(%rbp), %r10
	testw	%ax, %ax
	movq	%r11, -1176(%rbp)
	movq	$0, -1160(%rbp)
	movq	$0, -1152(%rbp)
	movl	$0, -1144(%rbp)
	movb	$0, -1140(%rbp)
	js	.L1311
	movswl	%ax, %esi
	sarl	$5, %esi
.L1312:
	leaq	-1184(%rbp), %r15
	movq	%r13, %rdx
	movq	%r10, -1216(%rbp)
	movq	%r15, %rdi
	movq	%r11, -1208(%rbp)
	call	_ZN6icu_6716ReorderingBuffer4initEiR10UErrorCode@PLT
	movq	-1208(%rbp), %r11
	movq	-1216(%rbp), %r10
	testb	%al, %al
	jne	.L1570
.L1313:
	movq	-1168(%rbp), %rax
	testq	%rax, %rax
	je	.L1314
	movq	-1152(%rbp), %rsi
	movq	-1176(%rbp), %rdi
	movq	%r10, -1216(%rbp)
	movq	%r11, -1208(%rbp)
	subq	%rax, %rsi
	sarq	%rsi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movq	-1216(%rbp), %r10
	movq	-1208(%rbp), %r11
.L1314:
	movl	-1188(%rbp), %edi
	testl	%edi, %edi
	jle	.L1571
	.p2align 4,,10
	.p2align 3
.L1292:
	movl	$-1, %r15d
.L1306:
	movl	-1096(%rbp), %eax
	testl	%eax, %eax
	js	.L1318
	cmpl	-1092(%rbp), %eax
	jne	.L1319
	movl	%r15d, -1096(%rbp)
.L1318:
	movq	-1088(%rbp), %rax
	movq	-1080(%rbp), %rdx
	cmpq	%rdx, %rax
	je	.L1386
	movzwl	(%rax), %r13d
	leaq	2(%rax), %rcx
	movq	%rcx, -1088(%rbp)
	movl	%r13d, %esi
	testl	%r13d, %r13d
	jne	.L1321
	testq	%rdx, %rdx
	je	.L1572
.L1321:
	movl	%esi, %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	jne	.L1320
	cmpq	%rcx, %rdx
	jne	.L1573
.L1320:
	movl	-568(%rbp), %eax
	testl	%eax, %eax
	js	.L1322
	cmpl	-564(%rbp), %eax
	jne	.L1323
	movl	%r15d, -568(%rbp)
.L1322:
	movq	-560(%rbp), %rax
	movq	-552(%rbp), %rcx
	cmpq	%rcx, %rax
	je	.L1543
	movzwl	(%rax), %edx
	leaq	2(%rax), %rsi
	movq	%rsi, -560(%rbp)
	movl	%edx, %r14d
	testl	%edx, %edx
	jne	.L1331
	testq	%rcx, %rcx
	je	.L1574
.L1331:
	movl	%edx, %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	jne	.L1324
	cmpq	%rsi, %rcx
	jne	.L1575
.L1324:
	cmpl	%r13d, %r14d
	je	.L1576
	testl	%r13d, %r13d
	js	.L1335
	cmpl	$65534, %r13d
	je	.L1387
	movl	-1096(%rbp), %esi
	testl	%esi, %esi
	js	.L1577
.L1337:
	testl	%r14d, %r14d
	js	.L1340
	cmpl	$65534, %r14d
	je	.L1578
.L1356:
	movl	-568(%rbp), %ecx
	testl	%ecx, %ecx
	js	.L1369
.L1339:
	cmpl	%r14d, %r13d
	jl	.L1401
.L1355:
	cmpl	%r13d, %r14d
	jge	.L1306
.L1340:
	movl	$1, %r13d
	jmp	.L1327
	.p2align 4,,10
	.p2align 3
.L1564:
	andl	$15, %eax
	cmpl	$10, %eax
	sete	%al
	jmp	.L1255
	.p2align 4,,10
	.p2align 3
.L1263:
	subq	$8, %rsp
	movq	%r11, -1224(%rbp)
	movq	%r15, %r9
	movl	$-1, %r8d
	pushq	$-1
	jmp	.L1529
	.p2align 4,,10
	.p2align 3
.L1555:
	movzwl	2(%rax), %ecx
	movl	%ecx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L1274
	addq	$4, %rax
	sall	$10, %edx
	movq	%rax, -560(%rbp)
	leal	-56613888(%rcx,%rdx), %r15d
	jmp	.L1274
	.p2align 4,,10
	.p2align 3
.L1553:
	movzwl	2(%rdx), %ecx
	movl	%ecx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L1270
	addq	$4, %rdx
	sall	$10, %edi
	movq	%rdx, -1088(%rbp)
	leal	-56613888(%rcx,%rdi), %r14d
	jmp	.L1270
.L1378:
	movl	$-1, %r14d
	jmp	.L1287
.L1562:
	movl	$0, -1276(%rbp)
	leaq	(%r14,%rbx), %r15
	jmp	.L1230
.L1566:
	cmpw	$0, (%r11)
	jne	.L1295
.L1294:
	movq	-1216(%rbp), %xmm0
	movq	%r11, %xmm5
	leaq	-1072(%rbp), %r10
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -1088(%rbp)
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1576:
	testl	%r14d, %r14d
	jns	.L1306
.L1332:
	xorl	%r13d, %r13d
.L1327:
	movq	%r11, %rdi
	leaq	-592(%rbp), %r14
	movq	%r10, -1208(%rbp)
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rbx
	movq	%r12, -592(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	movq	%rbx, -592(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	-1208(%rbp), %r10
	movq	%r12, -1120(%rbp)
	leaq	-1120(%rbp), %r12
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	movq	%rbx, -1120(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1323:
	leal	1(%rax), %edx
	movq	-584(%rbp), %rcx
	movl	%edx, -568(%rbp)
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %r14d
	leaq	(%rdx,%rdx), %rsi
	movl	%r14d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1324
	addl	$2, %eax
	sall	$10, %r14d
	movl	%eax, -568(%rbp)
	movzwl	2(%rcx,%rsi), %eax
	leal	-56613888(%rax,%r14), %r14d
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1319:
	leal	1(%rax), %edx
	movq	-1112(%rbp), %rcx
	movl	%edx, -1096(%rbp)
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %r13d
	leaq	(%rdx,%rdx), %rsi
	movl	%r13d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1320
	addl	$2, %eax
	sall	$10, %r13d
	movl	%eax, -1096(%rbp)
	movzwl	2(%rcx,%rsi), %eax
	leal	-56613888(%rax,%r13), %r13d
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1335:
	testl	%r14d, %r14d
	js	.L1306
	cmpl	$65534, %r14d
	je	.L1401
	movl	-568(%rbp), %eax
	testl	%eax, %eax
	jns	.L1401
	movl	$-2, %r13d
.L1369:
	leaq	-564(%rbp), %rcx
	leaq	-576(%rbp), %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movq	%r10, -1216(%rbp)
	movq	%r11, -1208(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movq	-1208(%rbp), %r11
	movq	-1216(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -584(%rbp)
	je	.L1339
	movl	$1, -568(%rbp)
	movzwl	(%rax), %r14d
	movl	%r14d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1339
	movzwl	2(%rax), %eax
	sall	$10, %r14d
	movl	$2, -568(%rbp)
	leal	-56613888(%r14,%rax), %r14d
	jmp	.L1339
	.p2align 4,,10
	.p2align 3
.L1575:
	movzwl	2(%rax), %ecx
	movl	%ecx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L1324
	addq	$4, %rax
	sall	$10, %edx
	movq	%rax, -560(%rbp)
	leal	-56613888(%rcx,%rdx), %r14d
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1573:
	movzwl	2(%rax), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L1320
	addq	$4, %rax
	sall	$10, %esi
	movq	%rax, -1088(%rbp)
	leal	-56613888(%rdx,%rsi), %r13d
	jmp	.L1320
.L1387:
	movl	$-1, %r13d
	jmp	.L1337
.L1578:
	movl	$-1, %r14d
	jmp	.L1355
.L1558:
	movl	$-1, %r15d
	jmp	.L1348
.L1565:
	leaq	-1072(%rbp), %r10
	jmp	.L1296
.L1557:
	leaq	-1092(%rbp), %rcx
	leaq	-1104(%rbp), %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movl	$-1, %r8d
	testq	%rax, %rax
	movq	%rax, -1112(%rbp)
	je	.L1288
	movl	$1, -1096(%rbp)
	movzwl	(%rax), %r14d
	movl	%r14d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1287
.L1368:
	movzwl	2(%rax), %eax
	sall	$10, %r14d
	movl	$2, -1096(%rbp)
	leal	-56613888(%r14,%rax), %r14d
.L1288:
	testl	%r15d, %r15d
	jns	.L1579
.L1394:
	movl	$-2, %r15d
	jmp	.L1289
.L1577:
	leaq	-1092(%rbp), %rcx
	leaq	-1104(%rbp), %rdx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movq	%r10, -1216(%rbp)
	movq	%r11, -1208(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movq	-1208(%rbp), %r11
	movq	-1216(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -1112(%rbp)
	je	.L1338
	movl	$1, -1096(%rbp)
	movzwl	(%rax), %r13d
	movl	%r13d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1337
.L1372:
	movzwl	2(%rax), %eax
	sall	$10, %r13d
	movl	$2, -1096(%rbp)
	leal	-56613888(%r13,%rax), %r13d
.L1338:
	testl	%r14d, %r14d
	jns	.L1580
.L1397:
	movl	$-2, %r14d
	jmp	.L1339
.L1569:
	cmpw	$0, (%r14)
	jne	.L1308
.L1307:
	movq	%r15, %xmm0
	movq	%r14, %xmm6
	leaq	-544(%rbp), %r11
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -560(%rbp)
	jmp	.L1292
.L1234:
	movq	(%r10), %rax
	movl	%r13d, %r10d
	andl	$31, %r10d
	movq	16(%rax), %rdi
	movq	(%rax), %r9
	movl	%r15d, %eax
	sarl	$5, %eax
	cmpw	$-10241, %r13w
	jbe	.L1525
	leal	320(%rax), %esi
	cmpl	$56320, %r15d
	cmovl	%esi, %eax
.L1525:
	cltq
	movzwl	(%r9,%rax,2), %eax
	leal	(%r10,%rax,4), %eax
	cltq
	salq	$2, %rax
	movl	(%rdi,%rax), %eax
	cmpb	$-65, %al
	jbe	.L1239
	andl	$15, %eax
	cmpl	$10, %eax
	sete	-1232(%rbp)
	jmp	.L1235
.L1530:
	movq	%r11, -1216(%rbp)
	movq	%r14, %r15
	jmp	.L1223
.L1218:
	testl	%r8d, %r8d
	je	.L1221
	movq	-1208(%rbp), %rbx
	movq	16(%rbx), %rax
	movl	24(%rax), %edi
	movl	80(%rax), %r10d
	movq	%rax, %rsi
	movl	%edi, %ecx
	shrl	%ecx
	andl	$1, %ecx
	movb	%cl, -1232(%rbp)
	movq	8(%rbx), %rcx
	testl	%r10d, %r10d
	js	.L1530
	cmpw	$383, (%r14)
	jbe	.L1361
	movq	%r11, -1216(%rbp)
	movq	%r14, %r15
	jmp	.L1362
.L1219:
	movq	-1208(%rbp), %rax
	movq	16(%rax), %rsi
	movl	24(%rsi), %edi
	movq	%rsi, %rax
	movl	%edi, %ecx
	shrl	%ecx
	andl	$1, %ecx
	movb	%cl, -1232(%rbp)
	jmp	.L1228
.L1244:
	movq	(%r9), %rax
	movl	%r13d, %r10d
	andl	$31, %r10d
	movq	16(%rax), %rdi
	movq	(%rax), %r9
	movl	%esi, %eax
	sarl	$5, %eax
	cmpw	$-10241, %r13w
	jbe	.L1526
	leal	320(%rax), %r13d
	cmpl	$56320, %esi
	cmovl	%r13d, %eax
.L1526:
	cltq
	movzwl	(%r9,%rax,2), %eax
	leal	(%r10,%rax,4), %eax
	cltq
	salq	$2, %rax
	movl	(%rdi,%rax), %eax
	cmpb	$-65, %al
	jbe	.L1249
	andl	$15, %eax
	cmpl	$10, %eax
	sete	%al
	jmp	.L1245
.L1311:
	movl	-532(%rbp), %esi
	jmp	.L1312
.L1309:
	movl	-532(%rbp), %edx
	jmp	.L1310
.L1299:
	movl	-1060(%rbp), %esi
	jmp	.L1300
.L1297:
	movl	-1060(%rbp), %edx
	jmp	.L1298
.L1568:
	movswl	-1064(%rbp), %eax
	testb	$17, %al
	jne	.L1382
	leaq	-1062(%rbp), %rdx
	testb	$2, %al
	cmove	-1048(%rbp), %rdx
.L1303:
	movq	%rdx, -1088(%rbp)
	testw	%ax, %ax
	js	.L1304
	sarl	$5, %eax
.L1305:
	cltq
	leaq	(%rdx,%rax,2), %rax
	movq	%rax, -1080(%rbp)
	jmp	.L1296
.L1571:
	movswl	-536(%rbp), %eax
	testb	$17, %al
	jne	.L1384
	leaq	-534(%rbp), %rdx
	testb	$2, %al
	cmove	-520(%rbp), %rdx
.L1315:
	movq	%rdx, -560(%rbp)
	testw	%ax, %ax
	js	.L1316
	sarl	$5, %eax
.L1317:
	cltq
	leaq	(%rdx,%rax,2), %rax
	movq	%rax, -552(%rbp)
	jmp	.L1292
.L1567:
	movq	-1264(%rbp), %rdx
	movq	%r13, %r8
	movq	%r11, %rsi
	movq	%rbx, %rdi
	movq	%r10, -1208(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl7makeFCDEPKDsS2_PNS_16ReorderingBufferER10UErrorCode@PLT
	movq	-1208(%rbp), %r10
	jmp	.L1301
.L1570:
	movq	-1272(%rbp), %rdx
	movq	%r13, %r8
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6715Normalizer2Impl7makeFCDEPKDsS2_PNS_16ReorderingBufferER10UErrorCode@PLT
	movq	-1216(%rbp), %r10
	movq	-1208(%rbp), %r11
	jmp	.L1313
.L1563:
	movq	-1208(%rbp), %rax
	movq	%r15, -1216(%rbp)
	leaq	(%r14,%rdi), %r15
	movq	16(%rax), %rsi
	movq	%rsi, %rax
	jmp	.L1241
.L1398:
	movl	$-1, %r13d
	jmp	.L1277
.L1401:
	movl	$-1, %r13d
	jmp	.L1327
.L1290:
	movl	%r12d, %r13d
	jmp	.L1277
.L1316:
	movl	-532(%rbp), %eax
	jmp	.L1317
.L1361:
	movq	88(%rcx), %rdi
	addq	$84, %rsi
	movq	%r14, %r15
	xorl	%r12d, %r12d
	movq	%r11, -1216(%rbp)
	jmp	.L1360
.L1304:
	movl	-1060(%rbp), %eax
	jmp	.L1305
.L1382:
	xorl	%edx, %edx
	jmp	.L1303
.L1574:
	movq	$0, -560(%rbp)
.L1543:
	cmpl	$-1, %r13d
	je	.L1332
	testl	%r13d, %r13d
	js	.L1306
	cmpl	$65534, %r13d
	je	.L1340
	cmpl	$0, -1096(%rbp)
	jns	.L1340
	leaq	-1092(%rbp), %rcx
	leaq	-1104(%rbp), %rdx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movq	%r10, -1216(%rbp)
	movq	%r11, -1208(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movq	-1208(%rbp), %r11
	movq	-1216(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -1112(%rbp)
	je	.L1397
	movl	$1, -1096(%rbp)
	movzwl	(%rax), %r13d
	movl	%r13d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1340
	orl	$-1, %r14d
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1384:
	xorl	%edx, %edx
	jmp	.L1315
.L1554:
	movq	$0, -560(%rbp)
.L1542:
	cmpl	$-1, %r14d
	je	.L1277
	testl	%r14d, %r14d
	js	.L1284
	cmpl	$65534, %r14d
	je	.L1290
	cmpl	$0, -1096(%rbp)
	jns	.L1290
	leaq	-1092(%rbp), %rcx
	leaq	-1104(%rbp), %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movl	$-1, %r8d
	testq	%rax, %rax
	movq	%rax, -1112(%rbp)
	je	.L1394
	movl	$1, -1096(%rbp)
	movzwl	(%rax), %r14d
	movl	%r14d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1290
	orl	$-1, %r15d
	jmp	.L1368
.L1552:
	movq	$0, -1088(%rbp)
	orl	$-1, %r14d
	jmp	.L1270
.L1377:
	movl	$-1, %r14d
	jmp	.L1270
.L1386:
	movl	$-1, %r13d
	jmp	.L1320
.L1572:
	movq	$0, -1088(%rbp)
	orl	$-1, %r13d
	jmp	.L1320
.L1561:
	call	__stack_chk_fail@PLT
.L1579:
	cmpl	$65534, %r15d
	jne	.L1349
	orl	$-1, %r15d
	jmp	.L1289
.L1580:
	cmpl	$65534, %r14d
	jne	.L1356
	orl	$-1, %r14d
	jmp	.L1339
.L1376:
	movq	%rcx, %r9
	jmp	.L1240
	.cfi_endproc
.LFE4921:
	.size	_ZNK6icu_6717RuleBasedCollator9doCompareEPKDsiS2_iR10UErrorCode.part.0, .-_ZNK6icu_6717RuleBasedCollator9doCompareEPKDsiS2_iR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator9doCompareEPKDsiS2_iR10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator9doCompareEPKDsiS2_iR10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator9doCompareEPKDsiS2_iR10UErrorCode:
.LFB3564:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rsi
	jne	.L1584
	cmpl	%r8d, %edx
	jne	.L1584
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1584:
	jmp	_ZNK6icu_6717RuleBasedCollator9doCompareEPKDsiS2_iR10UErrorCode.part.0
	.cfi_endproc
.LFE3564:
	.size	_ZNK6icu_6717RuleBasedCollator9doCompareEPKDsiS2_iR10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator9doCompareEPKDsiS2_iR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator7compareERKNS_13UnicodeStringES3_R10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator7compareERKNS_13UnicodeStringES3_R10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator7compareERKNS_13UnicodeStringES3_R10UErrorCode:
.LFB3522:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	movq	%rcx, %r9
	testl	%eax, %eax
	jg	.L1588
	movzwl	8(%rdx), %eax
	testw	%ax, %ax
	js	.L1590
	movswl	%ax, %r8d
	sarl	$5, %r8d
.L1591:
	testb	$17, %al
	jne	.L1600
	leaq	10(%rdx), %rcx
	testb	$2, %al
	jne	.L1592
	movq	24(%rdx), %rcx
.L1592:
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L1594
	movswl	%ax, %edx
	sarl	$5, %edx
.L1595:
	testb	$17, %al
	jne	.L1601
	testb	$2, %al
	je	.L1597
	addq	$10, %rsi
.L1596:
	cmpq	%rcx, %rsi
	jne	.L1603
	cmpl	%r8d, %edx
	jne	.L1603
.L1588:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1590:
	movl	12(%rdx), %r8d
	jmp	.L1591
	.p2align 4,,10
	.p2align 3
.L1603:
	jmp	_ZNK6icu_6717RuleBasedCollator9doCompareEPKDsiS2_iR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	24(%rsi), %rsi
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1594:
	movl	12(%rsi), %edx
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1600:
	xorl	%ecx, %ecx
	jmp	.L1592
	.p2align 4,,10
	.p2align 3
.L1601:
	xorl	%esi, %esi
	jmp	.L1596
	.cfi_endproc
.LFE3522:
	.size	_ZNK6icu_6717RuleBasedCollator7compareERKNS_13UnicodeStringES3_R10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator7compareERKNS_13UnicodeStringES3_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator7compareERKNS_13UnicodeStringES3_iR10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator7compareERKNS_13UnicodeStringES3_iR10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator7compareERKNS_13UnicodeStringES3_iR10UErrorCode:
.LFB3523:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L1630
	testl	%ecx, %ecx
	je	.L1630
	movq	%r8, %r9
	js	.L1632
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L1610
	movzwl	8(%rdx), %r11d
	movswl	%ax, %r10d
	sarl	$5, %r10d
	testw	%r11w, %r11w
	js	.L1612
.L1634:
	movswl	%r11w, %r8d
	sarl	$5, %r8d
.L1613:
	cmpl	%r10d, %ecx
	movl	%eax, %ebx
	cmovle	%ecx, %r10d
	cmpl	%r8d, %ecx
	cmovle	%ecx, %r8d
	andl	$17, %ebx
	testb	$17, %r11b
	jne	.L1622
	andl	$2, %r11d
	je	.L1615
	leaq	10(%rdx), %rcx
	testw	%bx, %bx
	jne	.L1633
.L1617:
	testb	$2, %al
	je	.L1619
	addq	$10, %rsi
.L1618:
	cmpq	%rcx, %rsi
	jne	.L1616
	cmpl	%r8d, %r10d
	jne	.L1616
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1632:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, (%r8)
.L1630:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1610:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movzwl	8(%rdx), %r11d
	movl	12(%rsi), %r10d
	testw	%r11w, %r11w
	jns	.L1634
.L1612:
	movl	12(%rdx), %r8d
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1633:
	xorl	%esi, %esi
.L1616:
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movl	%r10d, %edx
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6717RuleBasedCollator9doCompareEPKDsiS2_iR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1619:
	.cfi_restore_state
	movq	24(%rsi), %rsi
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L1615:
	movq	24(%rdx), %rcx
.L1614:
	testw	%bx, %bx
	je	.L1617
	xorl	%esi, %esi
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L1622:
	xorl	%ecx, %ecx
	jmp	.L1614
	.cfi_endproc
.LFE3523:
	.size	_ZNK6icu_6717RuleBasedCollator7compareERKNS_13UnicodeStringES3_iR10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator7compareERKNS_13UnicodeStringES3_iR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator7compareEPKDsiS2_iR10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator7compareEPKDsiS2_iR10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator7compareEPKDsiS2_iR10UErrorCode:
.LFB3524:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L1660
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	testq	%rsi, %rsi
	jne	.L1645
	testl	%edx, %edx
	je	.L1645
.L1638:
	movl	$1, (%r9)
.L1635:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1645:
	.cfi_restore_state
	testq	%rcx, %rcx
	jne	.L1640
	testl	%r8d, %r8d
	jne	.L1638
.L1640:
	testl	%edx, %edx
	js	.L1641
	testl	%r8d, %r8d
	js	.L1663
.L1642:
	cmpq	%rcx, %rsi
	jne	.L1646
	cmpl	%r8d, %edx
	je	.L1635
.L1646:
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6717RuleBasedCollator9doCompareEPKDsiS2_iR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1660:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1641:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	testl	%r8d, %r8d
	js	.L1642
	movq	%rsi, %rdi
	movq	%r9, -48(%rbp)
	movl	%r8d, -36(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%rsi, -24(%rbp)
	call	u_strlen_67@PLT
	movq	-48(%rbp), %r9
	movl	-36(%rbp), %r8d
	movq	-32(%rbp), %rcx
	movq	-24(%rbp), %rsi
	movl	%eax, %edx
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1663:
	movq	%rcx, %rdi
	movq	%r9, -48(%rbp)
	movl	%edx, -36(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rcx, -24(%rbp)
	call	u_strlen_67@PLT
	movq	-24(%rbp), %rcx
	movq	-32(%rbp), %rsi
	movl	-36(%rbp), %edx
	movq	-48(%rbp), %r9
	movl	%eax, %r8d
	jmp	.L1642
	.cfi_endproc
.LFE3524:
	.size	_ZNK6icu_6717RuleBasedCollator7compareEPKDsiS2_iR10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator7compareEPKDsiS2_iR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6717RuleBasedCollator9doCompareEPKhiS2_iR10UErrorCode.part.0, @function
_ZNK6icu_6717RuleBasedCollator9doCompareEPKhiS2_iR10UErrorCode.part.0:
.LFB4925:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1128, %rsp
	movl	%edx, -1144(%rbp)
	movl	%r8d, -1148(%rbp)
	movq	%r9, -1160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -1128(%rbp)
	testl	%edx, %edx
	js	.L2055
	je	.L2056
	movl	-1148(%rbp), %esi
	leal	-1(%rdx), %r8d
	xorl	%eax, %eax
	xorl	%edi, %edi
	jmp	.L1676
	.p2align 4,,10
	.p2align 3
.L1673:
	movzbl	0(%r13,%rax), %ecx
	cmpb	%cl, (%rbx,%rax)
	jne	.L2057
	addl	$1, %edx
	leaq	1(%rax), %rcx
	movl	$1, %edi
	cmpq	%rax, %r8
	je	.L2058
	movq	%rcx, %rax
.L1676:
	movl	%eax, %edx
	cmpq	%rax, %rsi
	jne	.L1673
	testb	%dil, %dil
	je	.L1821
	movl	%esi, -1128(%rbp)
.L1674:
	testl	%edx, %edx
	jle	.L1666
.L1677:
	cmpl	%edx, -1144(%rbp)
	je	.L1678
	movslq	%edx, %rax
	cmpb	$-64, (%rbx,%rax)
	jge	.L1678
.L1679:
	subl	$1, %edx
	movslq	%edx, %rax
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L2060:
	movzbl	(%rbx,%rax), %ecx
	subq	$1, %rax
	cmpb	$-64, %cl
	jge	.L2059
.L1683:
	movl	%eax, %edx
	testl	%eax, %eax
	jg	.L2060
	movl	%eax, -1128(%rbp)
.L1666:
	movq	16(%r12), %r8
	movq	8(%r12), %r14
	movl	24(%r8), %r11d
	shrl	%r11d
	andl	$1, %r11d
.L1701:
	movl	80(%r8), %r15d
	testl	%r15d, %r15d
	js	.L2019
	movslq	%edx, %rax
	cmpl	%edx, -1144(%rbp)
	je	.L1730
	cmpb	$-59, (%rbx,%rax)
	ja	.L2019
.L1730:
	leaq	0(%r13,%rax), %r9
	cmpl	%edx, -1148(%rbp)
	je	.L1731
	cmpb	$-59, (%r9)
	ja	.L2019
.L1731:
	movq	88(%r14), %rdi
	movl	-1144(%rbp), %r14d
	leaq	(%rbx,%rax), %r10
	leaq	84(%r8), %rsi
	testl	%r14d, %r14d
	jns	.L1800
	subq	$8, %rsp
	movb	%r11b, -1152(%rbp)
	movl	$-1, %r8d
	pushq	$-1
.L2018:
	movq	%r10, %rcx
	movl	%r15d, %edx
	call	_ZN6icu_6718CollationFastLatin11compareUTF8EPKtS2_iPKhiS4_i@PLT
	popq	%rdi
	movzbl	-1152(%rbp), %r11d
	movl	%eax, %r8d
	popq	%r9
	cmpl	$-2, %eax
	je	.L2061
	.p2align 4,,10
	.p2align 3
.L1734:
	testl	%r8d, %r8d
	jne	.L1664
	movq	16(%r12), %rax
	movl	24(%rax), %eax
	cmpl	$61439, %eax
	jle	.L1672
	movq	-1160(%rbp), %rsi
	movl	(%rsi), %r11d
	testl	%r11d, %r11d
	jg	.L1672
	movslq	-1128(%rbp), %rcx
	movq	8(%r12), %rdi
	movl	-1144(%rbp), %esi
	movq	%rcx, %rdx
	addq	%rcx, %rbx
	movq	48(%rdi), %r12
	addq	%r13, %rcx
	testl	%esi, %esi
	jle	.L1736
	subl	%edx, %esi
	subl	%edx, -1148(%rbp)
	movl	%esi, -1144(%rbp)
.L1736:
	notl	%eax
	andl	$1, %eax
	movl	%eax, -1160(%rbp)
	je	.L1737
	movl	$4294967295, %esi
	movl	$-1, %edx
	movq	%rbx, -1088(%rbp)
	movl	-1144(%rbp), %ebx
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_115UTF8NFDIteratorE(%rip), %rax
	movq	%rsi, -1096(%rbp)
	movl	$-1, %r14d
	leaq	.LC0(%rip), %r15
	movq	%rax, -1120(%rbp)
	movq	%rax, -592(%rbp)
	movl	-1148(%rbp), %eax
	movl	$0, -1080(%rbp)
	movl	%ebx, -1076(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%rcx, -560(%rbp)
	movl	$0, -552(%rbp)
	movl	%eax, -548(%rbp)
	movl	$-1, %eax
.L1738:
	testl	%eax, %eax
	js	.L1739
	cmpl	%eax, -1092(%rbp)
	jne	.L1740
	movl	%r14d, -1096(%rbp)
	movl	$-1, %eax
.L1739:
	movl	-1080(%rbp), %r10d
	movl	-1076(%rbp), %edi
	cmpl	%edi, %r10d
	je	.L1849
	movq	-1088(%rbp), %r9
	movslq	%r10d, %rcx
	addq	%r9, %rcx
	cmpb	$0, (%rcx)
	jne	.L1875
	testl	%edi, %edi
	js	.L1849
.L1875:
	leal	1(%r10), %esi
	movl	%esi, -1080(%rbp)
	movzbl	(%rcx), %ebx
	movl	%ebx, %ecx
	testb	%bl, %bl
	js	.L2062
.L1741:
	testl	%edx, %edx
	js	.L1747
	cmpl	%edx, -564(%rbp)
	jne	.L1748
	movl	%r14d, -568(%rbp)
	movl	$-1, %edx
.L1747:
	movl	-552(%rbp), %esi
	movl	-548(%rbp), %edi
	cmpl	%edi, %esi
	je	.L2044
	movq	-560(%rbp), %r9
	movslq	%esi, %rcx
	addq	%r9, %rcx
	cmpb	$0, (%rcx)
	jne	.L1755
	testl	%edi, %edi
	js	.L2044
.L1755:
	leal	1(%rsi), %r10d
	movl	%r10d, -552(%rbp)
	movzbl	(%rcx), %r13d
	movl	%r13d, %ecx
	testb	%r13b, %r13b
	js	.L2063
.L1749:
	cmpl	%ebx, %r13d
	je	.L2064
	testl	%ebx, %ebx
	js	.L1773
	cmpl	$65534, %ebx
	je	.L1859
	testl	%eax, %eax
	js	.L2065
.L1775:
	testl	%r13d, %r13d
	js	.L1756
	cmpl	$65534, %r13d
	jne	.L1795
	movl	$-1, %r13d
.L1804:
	cmpl	%r13d, %ebx
	jg	.L1756
	movl	-568(%rbp), %edx
	movl	-1096(%rbp), %eax
	jmp	.L1738
	.p2align 4,,10
	.p2align 3
.L2055:
	movzbl	(%rsi), %ecx
	cmpb	0(%r13), %cl
	jne	.L1821
	movl	$1, %eax
	jmp	.L1669
	.p2align 4,,10
	.p2align 3
.L1667:
	movzbl	(%rbx,%rax), %ecx
	movl	%eax, %edx
	addq	$1, %rax
	cmpb	-1(%r13,%rax), %cl
	jne	.L2066
.L1669:
	testb	%cl, %cl
	jne	.L1667
.L1672:
	xorl	%r8d, %r8d
.L1664:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2067
	leaq	-40(%rbp), %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2019:
	.cfi_restore_state
	movl	24(%r8), %esi
.L1729:
	movq	(%r14), %rax
	andl	$1, %esi
	movq	%r14, -1104(%rbp)
	movl	$0, -1096(%rbp)
	movq	%rax, -1112(%rbp)
	jne	.L1735
	movl	-1144(%rbp), %edi
	movq	%rax, -584(%rbp)
	leaq	-1072(%rbp), %rsi
	leaq	-544(%rbp), %rax
	leaq	-1120(%rbp), %r9
	movq	-1160(%rbp), %rcx
	movq	%rsi, -1088(%rbp)
	leaq	-592(%rbp), %r15
	leaq	16+_ZTVN6icu_6721UTF8CollationIteratorE(%rip), %rsi
	movq	%rax, -560(%rbp)
	movl	-1148(%rbp), %eax
	movq	%rsi, -1120(%rbp)
	movl	%edx, -720(%rbp)
	movl	%edi, -716(%rbp)
	movq	%r9, %rdi
	movq	%rsi, -592(%rbp)
	movq	%r15, %rsi
	movl	%edx, -192(%rbp)
	movq	%r8, %rdx
	movb	%r11b, -732(%rbp)
	movb	%r11b, -204(%rbp)
	movq	%r9, -1168(%rbp)
	movl	$40, -1080(%rbp)
	movb	$0, -1076(%rbp)
	movl	$0, -752(%rbp)
	movq	$0, -744(%rbp)
	movl	$-1, -736(%rbp)
	movq	%rbx, -728(%rbp)
	movq	%r14, -576(%rbp)
	movl	$0, -568(%rbp)
	movl	$40, -552(%rbp)
	movb	$0, -548(%rbp)
	movl	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movl	$-1, -208(%rbp)
	movq	%r13, -200(%rbp)
	movl	%eax, -188(%rbp)
	call	_ZN6icu_6716CollationCompare21compareUpToQuaternaryERNS_17CollationIteratorES2_RKNS_17CollationSettingsER10UErrorCode@PLT
	movq	%r15, %rdi
	movl	%eax, -1152(%rbp)
	call	_ZN6icu_6721UTF8CollationIteratorD1Ev@PLT
	movq	-1168(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6721UTF8CollationIteratorD1Ev@PLT
	movl	-1152(%rbp), %r8d
	jmp	.L1734
	.p2align 4,,10
	.p2align 3
.L1735:
	leaq	-1072(%rbp), %rsi
	movb	%r11b, -732(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdi
	movl	$2, %ecx
	movq	%rsi, -1088(%rbp)
	movl	-1144(%rbp), %esi
	leaq	-1120(%rbp), %r15
	movb	$0, -1076(%rbp)
	movq	48(%r14), %r9
	movq	%rax, -584(%rbp)
	leaq	-544(%rbp), %rax
	movq	%rax, -560(%rbp)
	movl	-1148(%rbp), %eax
	movl	%esi, -716(%rbp)
	leaq	16+_ZTVN6icu_6724FCDUTF8CollationIteratorE(%rip), %rsi
	movq	%r9, -696(%rbp)
	movb	%r11b, -204(%rbp)
	movl	%edx, -720(%rbp)
	movq	%rsi, -1120(%rbp)
	movl	%edx, -708(%rbp)
	movq	%rdi, -688(%rbp)
	movw	%cx, -680(%rbp)
	movq	%r14, -576(%rbp)
	leaq	-592(%rbp), %r14
	movl	%edx, -192(%rbp)
	movl	%eax, -188(%rbp)
	movl	$40, -1080(%rbp)
	movl	$0, -752(%rbp)
	movq	$0, -744(%rbp)
	movl	$-1, -736(%rbp)
	movq	%rbx, -728(%rbp)
	movl	$0, -712(%rbp)
	movl	$0, -568(%rbp)
	movl	$40, -552(%rbp)
	movb	$0, -548(%rbp)
	movl	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movl	$-1, -208(%rbp)
	movq	%r13, -200(%rbp)
	movq	%rsi, -592(%rbp)
	movq	-1160(%rbp), %rcx
	movl	$2, %esi
	movl	%edx, -180(%rbp)
	movq	%r8, %rdx
	movq	%rdi, -160(%rbp)
	movq	%r15, %rdi
	movw	%si, -152(%rbp)
	movq	%r14, %rsi
	movq	%r9, -168(%rbp)
	movl	$0, -184(%rbp)
	call	_ZN6icu_6716CollationCompare21compareUpToQuaternaryERNS_17CollationIteratorES2_RKNS_17CollationSettingsER10UErrorCode@PLT
	movq	%r14, %rdi
	movl	%eax, -1152(%rbp)
	call	_ZN6icu_6724FCDUTF8CollationIteratorD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6724FCDUTF8CollationIteratorD1Ev@PLT
	movl	-1152(%rbp), %r8d
	jmp	.L1734
	.p2align 4,,10
	.p2align 3
.L1678:
	cmpl	%edx, -1148(%rbp)
	je	.L1680
	movslq	%edx, %rax
	cmpb	$-64, 0(%r13,%rax)
	jl	.L1679
	movq	16(%r12), %rcx
	movl	24(%rcx), %r14d
	shrl	%r14d
	movl	%r14d, %esi
	movq	8(%r12), %r14
	andl	$1, %esi
	movb	%sil, -1152(%rbp)
	cmpl	%edx, -1144(%rbp)
	je	.L1685
.L1802:
	movzbl	(%rbx,%rax), %r15d
	movl	%r15d, %eax
	testb	%r15b, %r15b
	js	.L2068
.L1687:
	movq	80(%r14), %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L2014
	cmpb	$0, -1152(%rbp)
	je	.L1693
	cmpl	$1631, %r15d
	jle	.L2069
	movq	(%r14), %rcx
	movq	16(%rcx), %rdx
	cmpl	$55295, %r15d
	jg	.L1696
	movl	%r15d, %eax
	movq	(%rcx), %rcx
	sarl	$5, %eax
	cltq
.L2011:
	movzwl	(%rcx,%rax,2), %eax
	andl	$31, %r15d
	leal	(%r15,%rax,4), %eax
	cltq
	salq	$2, %rax
.L1697:
	movl	(%rdx,%rax), %eax
	cmpb	$-65, %al
	jbe	.L1693
	andl	$15, %eax
	cmpl	$10, %eax
	sete	%dl
.L1695:
	movl	-1128(%rbp), %eax
	testb	%dl, %dl
	jne	.L1692
	.p2align 4,,10
	.p2align 3
.L1693:
	movslq	-1128(%rbp), %rax
	movq	8(%r12), %r14
	movq	%rax, %rdx
	cmpl	%eax, -1148(%rbp)
	jne	.L1685
.L2017:
	movq	16(%r12), %r8
	movzbl	-1152(%rbp), %r11d
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L2059:
	movq	16(%r12), %r8
	movl	%edx, -1128(%rbp)
	movl	24(%r8), %esi
	movl	%esi, %r14d
	shrl	%r14d
	movl	%r14d, %eax
	movq	8(%r12), %r14
	andl	$1, %eax
	movb	%al, -1152(%rbp)
	movslq	%edx, %rax
	cmpl	%edx, -1144(%rbp)
	jne	.L1802
	movl	-1144(%rbp), %ecx
	cmpl	%ecx, -1148(%rbp)
	je	.L1686
.L1685:
	movzbl	0(%r13,%rax), %r15d
	movl	%r15d, %eax
	testb	%r15b, %r15b
	js	.L2070
.L1702:
	movq	80(%r14), %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	%eax, %r11d
	testb	%al, %al
	jne	.L2014
	movl	-1128(%rbp), %edx
	cmpb	$0, -1152(%rbp)
	movl	%edx, %eax
	je	.L2071
	cmpl	$1631, %r15d
	jg	.L1708
	subl	$48, %r15d
	cmpl	$9, %r15d
	setbe	%cl
.L1709:
	testb	%cl, %cl
	jne	.L1692
.L1715:
	movq	16(%r12), %r8
	movq	8(%r12), %r14
	movl	$1, %r11d
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L2014:
	movl	-1128(%rbp), %eax
.L1692:
	leaq	-1128(%rbp), %rdi
	movq	%rdi, -1168(%rbp)
	.p2align 4,,10
	.p2align 3
.L1994:
	leal	-1(%rax), %edx
	movslq	%edx, %rax
	movl	%edx, -1128(%rbp)
	movzbl	(%rbx,%rax), %r15d
	testb	%r15b, %r15b
	js	.L2072
.L1716:
	movq	8(%r12), %r14
	testl	%edx, %edx
	jle	.L2017
	movq	80(%r14), %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	%eax, %r11d
	testb	%al, %al
	jne	.L2015
	cmpb	$0, -1152(%rbp)
	jne	.L1719
	movq	16(%r12), %r8
	movl	-1128(%rbp), %edx
	movq	8(%r12), %r14
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L1719:
	cmpl	$1631, %r15d
	jg	.L1720
	subl	$48, %r15d
	cmpl	$9, %r15d
	setbe	%al
.L1721:
	testb	%al, %al
	je	.L1727
.L2015:
	movl	-1128(%rbp), %eax
	jmp	.L1994
	.p2align 4,,10
	.p2align 3
.L2072:
	movq	-1168(%rbp), %rdx
	movl	%r15d, %ecx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	$-3, %r8d
	call	utf8_prevCharSafeBody_67@PLT
	movl	-1128(%rbp), %edx
	movl	%eax, %r15d
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1720:
	movq	(%r14), %rcx
	movq	16(%rcx), %rdx
	cmpl	$55295, %r15d
	jg	.L1722
	movl	%r15d, %eax
	movq	(%rcx), %rcx
	andl	$31, %r15d
	sarl	$5, %eax
	cltq
	movzwl	(%rcx,%rax,2), %eax
	leal	(%r15,%rax,4), %eax
	cltq
	salq	$2, %rax
.L1723:
	movl	(%rdx,%rax), %eax
	cmpb	$-65, %al
	ja	.L2073
.L1727:
	movq	16(%r12), %r8
	movl	-1128(%rbp), %edx
	movl	$1, %r11d
	movq	8(%r12), %r14
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L2057:
	testb	%dil, %dil
	je	.L1821
	movl	%eax, -1128(%rbp)
	jmp	.L1674
	.p2align 4,,10
	.p2align 3
.L2066:
	movl	%edx, -1128(%rbp)
	jmp	.L1677
	.p2align 4,,10
	.p2align 3
.L2058:
	movl	%edx, -1128(%rbp)
	cmpl	%edx, -1148(%rbp)
	jne	.L1674
	jmp	.L1672
.L2056:
	movl	-1148(%rbp), %r15d
	testl	%r15d, %r15d
	je	.L1672
	.p2align 4,,10
	.p2align 3
.L1821:
	xorl	%edx, %edx
	jmp	.L1666
	.p2align 4,,10
	.p2align 3
.L2073:
	andl	$15, %eax
	cmpl	$10, %eax
	sete	%al
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1722:
	cmpl	$65535, %r15d
	ja	.L1724
	cmpl	$56320, %r15d
	movl	$320, %eax
	movq	(%rcx), %rsi
	movl	$0, %ecx
	cmovl	%eax, %ecx
	movl	%r15d, %eax
	sarl	$5, %eax
.L2016:
	addl	%ecx, %eax
	andl	$31, %r15d
	cltq
	movzwl	(%rsi,%rax,2), %eax
	leal	(%r15,%rax,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L2068:
	leal	1(%rdx), %esi
	cmpl	%esi, -1144(%rbp)
	je	.L1830
	cmpl	$223, %r15d
	jle	.L1688
	cmpl	$239, %r15d
	jg	.L1689
	movslq	%esi, %rdi
	andl	$15, %eax
	leaq	.LC0(%rip), %r8
	movl	%r15d, %edx
	movzbl	(%rbx,%rdi), %edi
	movsbl	(%r8,%rax), %r8d
	andl	$15, %edx
	movl	$65533, %r15d
	movl	%edi, %eax
	shrb	$5, %al
	btl	%eax, %r8d
	jnc	.L1687
	movl	%edi, %eax
	andl	$63, %eax
.L1690:
	addl	$1, %esi
	movl	$65533, %r15d
	cmpl	%esi, -1144(%rbp)
	je	.L1687
	sall	$6, %edx
	movzbl	%al, %eax
	orl	%edx, %eax
	jmp	.L1691
	.p2align 4,,10
	.p2align 3
.L1680:
	movq	16(%r12), %r8
	movl	24(%r8), %esi
	movl	%esi, %r14d
	shrl	%r14d
	movl	%r14d, %eax
	movq	8(%r12), %r14
	andl	$1, %eax
	movb	%al, -1152(%rbp)
	movslq	-1148(%rbp), %rax
	cmpl	%eax, -1144(%rbp)
	jne	.L1802
.L1686:
	movl	80(%r8), %r15d
	movzbl	-1152(%rbp), %r11d
	testl	%r15d, %r15d
	js	.L1729
	movslq	%edx, %rax
	movq	88(%r14), %rdi
	movzbl	-1152(%rbp), %r11d
	leaq	84(%r8), %rsi
	leaq	0(%r13,%rax), %r9
	leaq	(%rbx,%rax), %r10
.L1800:
	movl	-1148(%rbp), %ecx
	movl	-1144(%rbp), %r8d
	subq	$8, %rsp
	movb	%r11b, -1152(%rbp)
	subl	%edx, %ecx
	subl	%edx, %r8d
	pushq	%rcx
	jmp	.L2018
	.p2align 4,,10
	.p2align 3
.L1740:
	movq	-1112(%rbp), %rdi
	movslq	%eax, %rcx
	leal	1(%rax), %esi
	leaq	(%rcx,%rcx), %r9
	movl	%esi, -1096(%rbp)
	movzwl	(%rdi,%rcx,2), %ebx
	movl	%ebx, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	je	.L2074
	movl	%esi, %eax
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1748:
	movq	-584(%rbp), %rdi
	movslq	%edx, %rcx
	leal	1(%rdx), %esi
	leaq	(%rcx,%rcx), %r9
	movl	%esi, -568(%rbp)
	movzwl	(%rdi,%rcx,2), %r13d
	movl	%r13d, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	je	.L2075
	movl	%esi, %edx
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L2063:
	cmpl	%r10d, %edi
	je	.L2046
	cmpl	$223, %r13d
	jle	.L1761
	cmpl	$239, %r13d
	jg	.L1762
	movslq	%r10d, %r11
	andl	$15, %ecx
	movl	%r13d, %esi
	movzbl	(%r9,%r11), %r11d
	movsbl	(%r15,%rcx), %r13d
	andl	$15, %esi
	movl	%r11d, %ecx
	andl	$63, %r11d
	shrb	$5, %cl
	btl	%ecx, %r13d
	jnc	.L2046
.L1764:
	sall	$6, %esi
	movzbl	%r11b, %r11d
	addl	$1, %r10d
	movl	%esi, %ecx
	movl	%r10d, -552(%rbp)
	orl	%r11d, %ecx
	cmpl	%r10d, %edi
	je	.L2046
.L1769:
	movslq	%r10d, %rsi
	movzbl	(%r9,%rsi), %esi
	addl	$-128, %esi
	cmpb	$63, %sil
	ja	.L2046
	sall	$6, %ecx
	movzbl	%sil, %r13d
	addl	$1, %r10d
	movl	%r10d, -552(%rbp)
	orl	%ecx, %r13d
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L2062:
	cmpl	%esi, %edi
	je	.L1856
	cmpl	$223, %ebx
	jle	.L1743
	cmpl	$239, %ebx
	jg	.L1744
	movslq	%esi, %r10
	andl	$15, %ecx
	movl	%ebx, %r11d
	movl	$65533, %ebx
	movzbl	(%r9,%r10), %r10d
	movsbl	(%r15,%rcx), %r13d
	andl	$15, %r11d
	movl	%r10d, %ecx
	andl	$63, %r10d
	shrb	$5, %cl
	btl	%ecx, %r13d
	jnc	.L1741
.L1745:
	sall	$6, %r11d
	movzbl	%r10b, %ecx
	addl	$1, %esi
	movl	%esi, -1080(%rbp)
	orl	%r11d, %ecx
	cmpl	%esi, %edi
	jne	.L1746
.L1856:
	movl	$65533, %ebx
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L2064:
	testl	%r13d, %r13d
	jns	.L1738
.L1754:
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rbx
	leaq	-592(%rbp), %rdi
	movl	%r8d, -1144(%rbp)
	movq	%rbx, -592(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	leaq	-1120(%rbp), %rdi
	movq	%rbx, -1120(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movl	-1144(%rbp), %r8d
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1737:
	leaq	-1040(%rbp), %rax
	movq	(%rdi), %rsi
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE(%rip), %r9
	movl	$4294967295, %r10d
	movq	%rax, -1056(%rbp)
	movl	-1144(%rbp), %eax
	leaq	16+_ZTVN6icu_6724FCDUTF8CollationIteratorE(%rip), %rdx
	movl	$2, %r8d
	movq	%r9, -1120(%rbp)
	leaq	-1088(%rbp), %r15
	leaq	-560(%rbp), %r13
	movq	%rsi, -1080(%rbp)
	movl	%eax, -684(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r9, -592(%rbp)
	movl	$2, %r9d
	movq	%rsi, -552(%rbp)
	leaq	-512(%rbp), %rsi
	movq	%rbx, -696(%rbp)
	movq	%rax, -656(%rbp)
	movq	%r10, -1096(%rbp)
	movq	%rdi, -1072(%rbp)
	movl	$0, -1064(%rbp)
	movl	$40, -1048(%rbp)
	movb	$0, -1044(%rbp)
	movl	$0, -720(%rbp)
	movq	$0, -712(%rbp)
	movl	$-1, -704(%rbp)
	movb	$0, -700(%rbp)
	movl	$0, -688(%rbp)
	movq	%rdx, -1088(%rbp)
	movq	$0, -680(%rbp)
	movq	%r12, -664(%rbp)
	movw	%r8w, -648(%rbp)
	movq	%r10, -568(%rbp)
	movq	%rdi, -544(%rbp)
	movl	$0, -536(%rbp)
	movq	%rsi, -528(%rbp)
	movl	$40, -520(%rbp)
	movb	$0, -516(%rbp)
	movl	$0, -192(%rbp)
	movq	$0, -184(%rbp)
	movl	$-1, -176(%rbp)
	movl	-1148(%rbp), %ebx
	movb	$0, -172(%rbp)
	movq	%rcx, -168(%rbp)
	movl	$0, -160(%rbp)
	movq	%rdx, -560(%rbp)
	movq	$0, -152(%rbp)
	movq	%r12, -136(%rbp)
	movw	%r9w, -120(%rbp)
	movq	%r12, -1144(%rbp)
	movl	%ebx, -156(%rbp)
	leaq	-1124(%rbp), %rbx
	movq	%rax, -128(%rbp)
	movl	$-1, %eax
.L1779:
	testl	%eax, %eax
	js	.L1780
	cmpl	%eax, -1092(%rbp)
	jne	.L1781
	movl	$-1, -1096(%rbp)
.L1780:
	movl	$0, -1124(%rbp)
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6724FCDUTF8CollationIterator13nextCodePointER10UErrorCode@PLT
	movl	%eax, %r12d
.L1782:
	movl	-568(%rbp), %eax
	testl	%eax, %eax
	js	.L1783
	cmpl	-564(%rbp), %eax
	jne	.L1784
	movl	$-1, -568(%rbp)
.L1783:
	movl	$0, -1124(%rbp)
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6724FCDUTF8CollationIterator13nextCodePointER10UErrorCode@PLT
	movl	%eax, %r14d
.L1785:
	cmpl	%r12d, %r14d
	je	.L2076
	testl	%r12d, %r12d
	js	.L1789
	cmpl	$65534, %r12d
	je	.L1862
	movl	-1096(%rbp), %ecx
	testl	%ecx, %ecx
	js	.L2077
.L1791:
	testl	%r14d, %r14d
	jns	.L2078
	movl	$-2, %r14d
	jmp	.L1792
	.p2align 4,,10
	.p2align 3
.L2076:
	testl	%r14d, %r14d
	jns	.L1788
.L1787:
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE(%rip), %r14
	leaq	-592(%rbp), %r12
	movq	%r13, %rdi
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rbx
	movq	%r14, -592(%rbp)
	call	_ZN6icu_6724FCDUTF8CollationIteratorD1Ev@PLT
	movq	%r12, %rdi
	movq	%rbx, -592(%rbp)
	leaq	-1120(%rbp), %r12
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r15, %rdi
	movq	%r14, -1120(%rbp)
	call	_ZN6icu_6724FCDUTF8CollationIteratorD1Ev@PLT
	movq	%r12, %rdi
	movq	%rbx, -1120(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movl	-1160(%rbp), %r8d
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1789:
	testl	%r14d, %r14d
	js	.L1788
	cmpl	$65534, %r14d
	je	.L1870
	movl	$-2, %r12d
.L1812:
	movl	-568(%rbp), %edx
	testl	%edx, %edx
	js	.L2079
.L1792:
	cmpl	%r12d, %r14d
	jg	.L1870
.L1813:
	cmpl	%r14d, %r12d
	jg	.L1798
.L1788:
	movl	-1096(%rbp), %eax
	jmp	.L1779
	.p2align 4,,10
	.p2align 3
.L1781:
	leal	1(%rax), %edx
	movq	-1112(%rbp), %rcx
	movl	%edx, -1096(%rbp)
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %r12d
	leaq	(%rdx,%rdx), %rsi
	movl	%r12d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1782
	addl	$2, %eax
	movl	%r12d, %r8d
	movl	%eax, -1096(%rbp)
	movzwl	2(%rcx,%rsi), %eax
	sall	$10, %r8d
	leal	-56613888(%rax,%r8), %r12d
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1784:
	leal	1(%rax), %edx
	movq	-584(%rbp), %rcx
	movl	%edx, -568(%rbp)
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %r14d
	leaq	(%rdx,%rdx), %rsi
	movl	%r14d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1785
	addl	$2, %eax
	sall	$10, %r14d
	movl	%eax, -568(%rbp)
	movzwl	2(%rcx,%rsi), %eax
	leal	-56613888(%rax,%r14), %r14d
	jmp	.L1785
.L1761:
	andl	$31, %ecx
	cmpl	$193, %r13d
	jg	.L1769
	.p2align 4,,10
	.p2align 3
.L2046:
	cmpl	$65533, %ebx
	je	.L1738
	testl	%ebx, %ebx
	js	.L1758
	cmpl	$65534, %ebx
	je	.L2080
	movl	$65533, %r13d
	testl	%eax, %eax
	js	.L2081
.L1795:
	movl	-568(%rbp), %r10d
	testl	%r10d, %r10d
	js	.L1814
.L1777:
	cmpl	%ebx, %r13d
	jle	.L1804
.L1805:
	movl	$-1, %r8d
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1773:
	testl	%r13d, %r13d
	js	.L1738
	cmpl	$65534, %r13d
	je	.L1805
	movl	-568(%rbp), %eax
	testl	%eax, %eax
	jns	.L1805
	movl	$-2, %ebx
.L1814:
	leaq	-564(%rbp), %rcx
	leaq	-576(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%r8d, -1144(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movl	-1144(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, -584(%rbp)
	je	.L1777
.L1816:
	movl	$1, -568(%rbp)
	movzwl	(%rax), %r13d
	movl	%r13d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1777
	movzwl	2(%rax), %eax
	sall	$10, %r13d
	movl	$2, -568(%rbp)
	leal	-56613888(%r13,%rax), %r13d
	jmp	.L1777
	.p2align 4,,10
	.p2align 3
.L2070:
	leal	1(%rdx), %esi
	cmpl	%esi, -1148(%rbp)
	je	.L1841
	cmpl	$223, %r15d
	jle	.L1703
	cmpl	$239, %r15d
	jg	.L1704
	movslq	%esi, %rdi
	andl	$15, %eax
	leaq	.LC0(%rip), %r8
	movl	%r15d, %edx
	movzbl	0(%r13,%rdi), %edi
	movsbl	(%r8,%rax), %r8d
	andl	$15, %edx
	movl	$65533, %r15d
	movl	%edi, %eax
	shrb	$5, %al
	btl	%eax, %r8d
	jnc	.L1702
	movl	%edi, %eax
	andl	$63, %eax
.L1705:
	addl	$1, %esi
	movl	$65533, %r15d
	cmpl	%esi, -1148(%rbp)
	je	.L1702
	sall	$6, %edx
	movzbl	%al, %eax
	orl	%edx, %eax
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L2061:
	movq	16(%r12), %r8
	movl	-1128(%rbp), %edx
	movq	8(%r12), %r14
	movl	24(%r8), %esi
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L1724:
	movl	$512, %eax
	cmpl	$1114111, %r15d
	ja	.L1723
	cmpl	44(%rcx), %r15d
	jl	.L1726
	movslq	48(%rcx), %rax
	salq	$2, %rax
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1830:
	movl	$65533, %r15d
	jmp	.L1687
.L1862:
	movl	$-1, %r12d
.L1790:
	testl	%r14d, %r14d
	js	.L1798
	cmpl	$65534, %r14d
	jne	.L1812
	movl	$-1, %r14d
	jmp	.L1813
.L2071:
	movq	16(%r12), %r8
	movq	8(%r12), %r14
	jmp	.L1701
.L1743:
	cmpl	$193, %ebx
	jle	.L1856
	andl	$31, %ecx
.L1746:
	movslq	%esi, %rdi
	movl	$65533, %ebx
	movzbl	(%r9,%rdi), %edi
	addl	$-128, %edi
	cmpb	$63, %dil
	ja	.L1741
	sall	$6, %ecx
	movzbl	%dil, %ebx
	addl	$1, %esi
	movl	%esi, -1080(%rbp)
	orl	%ecx, %ebx
	jmp	.L1741
.L1688:
	cmpl	$193, %r15d
	jle	.L1830
	andl	$31, %eax
.L1691:
	movslq	%esi, %rsi
	movl	$65533, %r15d
	movzbl	(%rbx,%rsi), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L1687
	sall	$6, %eax
	movzbl	%dl, %r15d
	orl	%eax, %r15d
	jmp	.L1687
.L2078:
	cmpl	$65534, %r14d
	jne	.L1812
	movl	$-1, %r14d
	jmp	.L1792
.L2069:
	subl	$48, %r15d
	cmpl	$9, %r15d
	setbe	%dl
	jmp	.L1695
.L2065:
	leaq	-1092(%rbp), %rcx
	leaq	-1104(%rbp), %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	movl	%r8d, -1144(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movl	-1144(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, -1112(%rbp)
	je	.L1776
	movl	$1, -1096(%rbp)
	movzwl	(%rax), %ebx
	movl	%ebx, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1775
.L1815:
	movzwl	2(%rax), %eax
	sall	$10, %ebx
	movl	$2, -1096(%rbp)
	leal	-56613888(%rbx,%rax), %ebx
.L1776:
	testl	%r13d, %r13d
	jns	.L2082
.L1868:
	movl	$-2, %r13d
	jmp	.L1777
.L1703:
	cmpl	$193, %r15d
	jle	.L1841
	andl	$31, %eax
.L1706:
	movslq	%esi, %rsi
	movl	$65533, %r15d
	movzbl	0(%r13,%rsi), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L1702
	sall	$6, %eax
	movzbl	%dl, %edx
	orl	%edx, %eax
	movl	%eax, %r15d
	jmp	.L1702
.L1758:
	leaq	-564(%rbp), %rcx
	leaq	-576(%rbp), %rdx
	movl	$65533, %esi
	movq	%r12, %rdi
	movl	%r8d, -1144(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movq	%rax, -584(%rbp)
	testq	%rax, %rax
	je	.L1805
	movl	-1144(%rbp), %r8d
	movl	$-2, %ebx
	jmp	.L1816
.L1841:
	movl	$65533, %r15d
	jmp	.L1702
.L2077:
	movq	-1144(%rbp), %rdi
	leaq	-1092(%rbp), %rcx
	leaq	-1104(%rbp), %rdx
	movl	%r12d, %esi
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movq	%rax, -1112(%rbp)
	testq	%rax, %rax
	je	.L1791
	movl	$1, -1096(%rbp)
	movzwl	(%rax), %r12d
	movl	%r12d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1790
	movl	%r12d, %r8d
	movzwl	2(%rax), %edx
	movl	$2, -1096(%rbp)
	sall	$10, %r8d
	leal	-56613888(%r8,%rdx), %r12d
	jmp	.L1791
.L2079:
	movq	-1144(%rbp), %rdi
	leaq	-564(%rbp), %rcx
	leaq	-576(%rbp), %rdx
	movl	%r14d, %esi
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movq	%rax, -584(%rbp)
	testq	%rax, %rax
	je	.L1792
	movl	$1, -568(%rbp)
	movzwl	(%rax), %r14d
	movl	%r14d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1792
	movzwl	2(%rax), %edx
	sall	$10, %r14d
	movl	$2, -568(%rbp)
	leal	-56613888(%r14,%rdx), %r14d
	jmp	.L1792
.L1859:
	movl	$-1, %ebx
	jmp	.L1775
.L2074:
	movzwl	2(%rdi,%r9), %ecx
	addl	$2, %eax
	sall	$10, %ebx
	movl	%eax, -1096(%rbp)
	leal	-56613888(%rcx,%rbx), %ebx
	jmp	.L1741
.L2075:
	movzwl	2(%rdi,%r9), %ecx
	addl	$2, %edx
	sall	$10, %r13d
	movl	%edx, -568(%rbp)
	leal	-56613888(%rcx,%r13), %r13d
	jmp	.L1749
.L1708:
	movq	(%r14), %rdi
	movq	16(%rdi), %rsi
	cmpl	$55295, %r15d
	jg	.L1710
	movl	%r15d, %ecx
	movq	(%rdi), %rdi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
.L2012:
	movzwl	(%rdi,%rcx,2), %ecx
	andl	$31, %r15d
	leal	(%r15,%rcx,4), %ecx
	movslq	%ecx, %rcx
	salq	$2, %rcx
.L1711:
	movl	(%rsi,%rcx), %ecx
	cmpb	$-65, %cl
	jbe	.L1715
	andl	$15, %ecx
	cmpl	$10, %ecx
	sete	%cl
	jmp	.L1709
.L1744:
	leal	-240(%rbx), %ecx
	movl	$65533, %ebx
	cmpl	$4, %ecx
	jg	.L1741
	movslq	%esi, %rsi
	leaq	.LC1(%rip), %r11
	movzbl	(%r9,%rsi), %r13d
	movq	%r13, %rsi
	shrq	$4, %rsi
	andl	$15, %esi
	movsbl	(%r11,%rsi), %esi
	btl	%ecx, %esi
	jnc	.L1741
	leal	2(%r10), %esi
	movl	%esi, -1080(%rbp)
	cmpl	%esi, %edi
	je	.L1741
	movslq	%esi, %r10
	movzbl	(%r9,%r10), %r10d
	addl	$-128, %r10d
	cmpb	$63, %r10b
	ja	.L1741
	movl	%r13d, %r11d
	sall	$6, %ecx
	andl	$63, %r11d
	orl	%ecx, %r11d
	jmp	.L1745
.L1762:
	subl	$240, %r13d
	cmpl	$4, %r13d
	jg	.L2046
	movslq	%r10d, %r10
	leaq	.LC1(%rip), %r11
	movzbl	(%r9,%r10), %ecx
	movq	%rcx, %r10
	shrq	$4, %r10
	andl	$15, %r10d
	movsbl	(%r11,%r10), %r10d
	btl	%r13d, %r10d
	jnc	.L2046
	leal	2(%rsi), %r10d
	movl	%r10d, -552(%rbp)
	cmpl	%r10d, %edi
	je	.L2046
	movslq	%r10d, %rsi
	movzbl	(%r9,%rsi), %r11d
	addl	$-128, %r11d
	cmpb	$63, %r11b
	ja	.L2046
	movl	%r13d, %esi
	andl	$63, %ecx
	sall	$6, %esi
	orl	%ecx, %esi
	jmp	.L1764
.L1689:
	leal	-240(%r15), %edi
	movl	$65533, %r15d
	cmpl	$4, %edi
	jg	.L1687
	movslq	%esi, %rsi
	movzbl	(%rbx,%rsi), %r8d
	leaq	.LC1(%rip), %rsi
	movq	%r8, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movsbl	(%rsi,%rax), %eax
	btl	%edi, %eax
	jnc	.L1687
	leal	2(%rdx), %esi
	cmpl	%esi, -1144(%rbp)
	je	.L1687
	movslq	%esi, %rax
	movzbl	(%rbx,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L1687
	sall	$6, %edi
	andl	$63, %r8d
	movl	%edi, %edx
	orl	%r8d, %edx
	jmp	.L1690
	.p2align 4,,10
	.p2align 3
.L1696:
	cmpl	$65535, %r15d
	jg	.L1698
	cmpl	$56320, %r15d
	movq	(%rcx), %rsi
	movl	$320, %eax
	movl	$0, %ecx
	cmovl	%eax, %ecx
	movl	%r15d, %eax
	andl	$31, %r15d
	sarl	$5, %eax
	addl	%ecx, %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	leal	(%r15,%rax,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L1697
.L1726:
	movl	%r15d, %eax
	movq	(%rcx), %rsi
	movl	%r15d, %ecx
	sarl	$11, %eax
	sarl	$5, %ecx
	addl	$2080, %eax
	andl	$63, %ecx
	cltq
	movzwl	(%rsi,%rax,2), %eax
	jmp	.L2016
.L1704:
	leal	-240(%r15), %edi
	movl	$65533, %r15d
	cmpl	$4, %edi
	jg	.L1702
	movslq	%esi, %rsi
	movzbl	0(%r13,%rsi), %r8d
	leaq	.LC1(%rip), %rsi
	movq	%r8, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movsbl	(%rsi,%rax), %eax
	btl	%edi, %eax
	jnc	.L1702
	leal	2(%rdx), %esi
	cmpl	%esi, -1148(%rbp)
	je	.L1702
	movslq	%esi, %rax
	movzbl	0(%r13,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L1702
	sall	$6, %edi
	andl	$63, %r8d
	movl	%edi, %edx
	orl	%r8d, %edx
	jmp	.L1705
	.p2align 4,,10
	.p2align 3
.L1710:
	cmpl	$65535, %r15d
	jg	.L1712
	cmpl	$56320, %r15d
	movl	$320, %ecx
	movl	$0, %r8d
	movq	(%rdi), %rdi
	cmovl	%ecx, %r8d
	movl	%r15d, %ecx
	sarl	$5, %ecx
.L2013:
	addl	%r8d, %ecx
	movslq	%ecx, %rcx
	jmp	.L2012
.L2080:
	leaq	-564(%rbp), %rcx
	leaq	-576(%rbp), %rdx
	movl	$65533, %esi
	movq	%r12, %rdi
	movl	%r8d, -1144(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movq	%rax, -584(%rbp)
	testq	%rax, %rax
	je	.L1805
	movl	-1144(%rbp), %r8d
	movl	$-1, %ebx
	jmp	.L1816
.L1698:
	movl	$512, %eax
	cmpl	$1114111, %r15d
	ja	.L1697
	cmpl	44(%rcx), %r15d
	jl	.L1700
	movslq	48(%rcx), %rax
	salq	$2, %rax
	jmp	.L1697
.L1870:
	movl	$-1, -1160(%rbp)
	jmp	.L1787
.L1798:
	movl	$1, -1160(%rbp)
	jmp	.L1787
.L1756:
	movl	-1160(%rbp), %r8d
	jmp	.L1754
.L2081:
	leaq	-1092(%rbp), %rcx
	leaq	-1104(%rbp), %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	movl	%r8d, -1144(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movl	-1144(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, -1112(%rbp)
	je	.L1795
	movl	$1, -1096(%rbp)
	movzwl	(%rax), %ebx
	movl	%ebx, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1795
	jmp	.L1815
.L1712:
	movl	$512, %ecx
	cmpl	$1114111, %r15d
	ja	.L1711
	cmpl	44(%rdi), %r15d
	jl	.L1714
	movslq	48(%rdi), %rcx
	salq	$2, %rcx
	jmp	.L1711
.L1700:
	movl	%r15d, %eax
	movq	(%rcx), %rcx
	movl	%r15d, %esi
	sarl	$11, %eax
	sarl	$5, %esi
	addl	$2080, %eax
	andl	$63, %esi
	cltq
	movzwl	(%rcx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	jmp	.L2011
.L2044:
	cmpl	$-1, %ebx
	je	.L1754
	testl	%ebx, %ebx
	js	.L1738
	cmpl	$65534, %ebx
	je	.L1756
	testl	%eax, %eax
	jns	.L1756
	leaq	-1092(%rbp), %rcx
	leaq	-1104(%rbp), %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	movl	%r8d, -1144(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movl	-1144(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, -1112(%rbp)
	je	.L1868
	movl	$1, -1096(%rbp)
	movzwl	(%rax), %ebx
	movl	%ebx, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1756
	orl	$-1, %r13d
	jmp	.L1815
	.p2align 4,,10
	.p2align 3
.L1849:
	movl	$-1, %ebx
	jmp	.L1741
.L1714:
	movl	%r15d, %ecx
	movq	(%rdi), %rdi
	movl	%r15d, %r8d
	sarl	$11, %ecx
	sarl	$5, %r8d
	addl	$2080, %ecx
	andl	$63, %r8d
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	jmp	.L2013
.L2067:
	call	__stack_chk_fail@PLT
.L2082:
	cmpl	$65534, %r13d
	jne	.L1795
	orl	$-1, %r13d
	jmp	.L1777
	.cfi_endproc
.LFE4925:
	.size	_ZNK6icu_6717RuleBasedCollator9doCompareEPKhiS2_iR10UErrorCode.part.0, .-_ZNK6icu_6717RuleBasedCollator9doCompareEPKhiS2_iR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator9doCompareEPKhiS2_iR10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator9doCompareEPKhiS2_iR10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator9doCompareEPKhiS2_iR10UErrorCode:
.LFB3565:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rsi
	jne	.L2086
	cmpl	%r8d, %edx
	jne	.L2086
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2086:
	jmp	_ZNK6icu_6717RuleBasedCollator9doCompareEPKhiS2_iR10UErrorCode.part.0
	.cfi_endproc
.LFE3565:
	.size	_ZNK6icu_6717RuleBasedCollator9doCompareEPKhiS2_iR10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator9doCompareEPKhiS2_iR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator11compareUTF8ERKNS_11StringPieceES3_R10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator11compareUTF8ERKNS_11StringPieceES3_R10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator11compareUTF8ERKNS_11StringPieceES3_R10UErrorCode:
.LFB3525:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	movq	%rcx, %r9
	testl	%eax, %eax
	jg	.L2090
	movq	(%rsi), %r10
	movq	(%rdx), %rcx
	testq	%r10, %r10
	je	.L2109
	movl	8(%rdx), %r8d
	testq	%rcx, %rcx
	je	.L2110
	movl	8(%rsi), %r11d
	cmpq	%rcx, %r10
	jne	.L2099
	cmpl	%r8d, %r11d
	jne	.L2099
.L2090:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2110:
	testl	%r8d, %r8d
	jne	.L2093
	movl	8(%rsi), %r11d
.L2099:
	movl	%r11d, %edx
	movq	%r10, %rsi
	jmp	_ZNK6icu_6717RuleBasedCollator9doCompareEPKhiS2_iR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L2109:
	movl	8(%rsi), %r11d
	testl	%r11d, %r11d
	jne	.L2093
	movl	8(%rdx), %r8d
	testq	%rcx, %rcx
	jne	.L2099
	testl	%r8d, %r8d
	je	.L2090
.L2093:
	movl	$1, (%r9)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3525:
	.size	_ZNK6icu_6717RuleBasedCollator11compareUTF8ERKNS_11StringPieceES3_R10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator11compareUTF8ERKNS_11StringPieceES3_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator19internalCompareUTF8EPKciS2_iR10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator19internalCompareUTF8EPKciS2_iR10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator19internalCompareUTF8EPKciS2_iR10UErrorCode:
.LFB3526:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L2136
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	subq	$40, %rsp
	testq	%rsi, %rsi
	jne	.L2121
	testl	%edx, %edx
	je	.L2121
.L2113:
	movl	$1, (%r9)
.L2111:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2121:
	.cfi_restore_state
	testq	%rcx, %rcx
	jne	.L2115
	testl	%r8d, %r8d
	jne	.L2113
.L2115:
	testl	%edx, %edx
	js	.L2116
	testl	%r8d, %r8d
	jns	.L2117
	movq	%rcx, %rdi
	movq	%r9, -48(%rbp)
	movl	%edx, -36(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rcx, -24(%rbp)
	call	strlen@PLT
	movq	-24(%rbp), %rcx
	movq	-32(%rbp), %rsi
	movl	-36(%rbp), %edx
	movq	-48(%rbp), %r9
	movl	%eax, %r8d
.L2117:
	cmpq	%rcx, %rsi
	jne	.L2122
	cmpl	%r8d, %edx
	je	.L2111
.L2122:
	addq	$40, %rsp
	movq	%r13, %rdi
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6717RuleBasedCollator9doCompareEPKhiS2_iR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L2136:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2116:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 13, -24
	testl	%r8d, %r8d
	js	.L2117
	movq	%rsi, %rdi
	movq	%r9, -48(%rbp)
	movl	%r8d, -36(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%rsi, -24(%rbp)
	call	strlen@PLT
	movq	-48(%rbp), %r9
	movl	-36(%rbp), %r8d
	movq	-32(%rbp), %rcx
	movq	-24(%rbp), %rsi
	movl	%eax, %edx
	jmp	.L2117
	.cfi_endproc
.LFE3526:
	.size	_ZNK6icu_6717RuleBasedCollator19internalCompareUTF8EPKciS2_iR10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator19internalCompareUTF8EPKciS2_iR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6717RuleBasedCollator7compareER13UCharIteratorS2_R10UErrorCode.part.0, @function
_ZNK6icu_6717RuleBasedCollator7compareER13UCharIteratorS2_R10UErrorCode.part.0:
.LFB4928:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1128, %rsp
	movq	%rcx, -1128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movl	24(%rax), %eax
	movl	%eax, -1136(%rbp)
	jmp	.L2143
	.p2align 4,,10
	.p2align 3
.L2266:
	testl	%r14d, %r14d
	js	.L2181
	addl	$1, %r12d
.L2143:
	movq	%r15, %rdi
	call	*72(%r15)
	movq	%rbx, %rdi
	movl	%eax, %r14d
	call	*72(%rbx)
	movl	%eax, %edx
	cmpl	%eax, %r14d
	je	.L2266
	testl	%r14d, %r14d
	js	.L2144
	movl	%eax, -1144(%rbp)
	movq	%r15, %rdi
	call	*80(%r15)
	movl	-1144(%rbp), %edx
.L2144:
	testl	%edx, %edx
	js	.L2145
	movl	%edx, -1144(%rbp)
	movq	%rbx, %rdi
	call	*80(%rbx)
	movl	-1144(%rbp), %edx
.L2145:
	movl	-1136(%rbp), %eax
	movq	8(%r13), %rcx
	andl	$2, %eax
	movl	%eax, -1136(%rbp)
	testl	%r12d, %r12d
	je	.L2146
	testl	%r14d, %r14d
	jns	.L2267
.L2147:
	testl	%edx, %edx
	jns	.L2268
	.p2align 4,,10
	.p2align 3
.L2146:
	movq	16(%r13), %rdx
	movl	-1136(%rbp), %r10d
	leaq	-1056(%rbp), %rdi
	leaq	-1104(%rbp), %r11
	movq	(%rcx), %rsi
	testl	%r10d, %r10d
	setne	%al
	testb	$1, 24(%rdx)
	movq	%rsi, -1096(%rbp)
	movq	%rcx, -1088(%rbp)
	movl	$0, -1080(%rbp)
	movq	%rdi, -1072(%rbp)
	movl	$40, -1064(%rbp)
	movb	$0, -1060(%rbp)
	movl	$0, -736(%rbp)
	movq	$0, -728(%rbp)
	movl	$-1, -720(%rbp)
	movb	%al, -716(%rbp)
	je	.L2269
	movq	48(%rcx), %r9
	movq	%rcx, -560(%rbp)
	leaq	-528(%rbp), %rcx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdi
	movq	%rcx, -544(%rbp)
	leaq	-576(%rbp), %r14
	movq	-1128(%rbp), %rcx
	leaq	16+_ZTVN6icu_6725FCDUIterCollationIteratorE(%rip), %r10
	movl	$2, %r8d
	movq	%r9, -688(%rbp)
	movq	%rdi, -680(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%r14, %rsi
	movq	%r9, -160(%rbp)
	movl	$2, %r9d
	movq	%rdi, -152(%rbp)
	movq	%r11, %rdi
	movw	%r9w, -144(%rbp)
	movq	%r10, -1104(%rbp)
	movw	%r8w, -672(%rbp)
	movq	%r10, -576(%rbp)
	movq	%r11, -1136(%rbp)
	movq	%r15, -712(%rbp)
	movl	$0, -704(%rbp)
	movl	%r12d, -700(%rbp)
	movl	$0, -552(%rbp)
	movl	$40, -536(%rbp)
	movb	$0, -532(%rbp)
	movl	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movl	$-1, -192(%rbp)
	movb	%al, -188(%rbp)
	movq	%rbx, -184(%rbp)
	movl	$0, -176(%rbp)
	movl	%r12d, -172(%rbp)
	call	_ZN6icu_6716CollationCompare21compareUpToQuaternaryERNS_17CollationIteratorES2_RKNS_17CollationSettingsER10UErrorCode@PLT
	movq	%r14, %rdi
	movl	%eax, -1144(%rbp)
	call	_ZN6icu_6725FCDUIterCollationIteratorD1Ev@PLT
	movq	-1136(%rbp), %rdi
	call	_ZN6icu_6725FCDUIterCollationIteratorD1Ev@PLT
	movl	-1144(%rbp), %r9d
.L2180:
	testl	%r9d, %r9d
	jne	.L2139
	movq	16(%r13), %rax
	movl	%r9d, -1144(%rbp)
	cmpl	$61439, 24(%rax)
	jle	.L2181
	movq	-1128(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L2181
	movl	$3, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	*40(%r15)
	movl	$3, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	*40(%rbx)
	movq	8(%r13), %rdx
	movq	48(%rdx), %rax
	movq	%rax, -1152(%rbp)
	movq	16(%r13), %rax
	movl	24(%rax), %eax
	notl	%eax
	andl	$1, %eax
	movl	%eax, -1156(%rbp)
	je	.L2182
	movl	$4294967295, %esi
	movq	%r15, -1072(%rbp)
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_116UIterNFDIteratorE(%rip), %rax
	movl	-1144(%rbp), %r9d
	movq	%rsi, -1080(%rbp)
	movq	%rsi, -552(%rbp)
	movq	%rbx, -544(%rbp)
	movq	%rax, -1104(%rbp)
	movq	%rax, -576(%rbp)
	movl	$-1, %eax
.L2183:
	testl	%eax, %eax
	js	.L2184
	cmpl	%eax, -1076(%rbp)
	jne	.L2185
	movl	$-1, -1080(%rbp)
.L2184:
	movq	-1072(%rbp), %rdi
	movl	%r9d, -1128(%rbp)
	call	uiter_next32_67@PLT
	movl	-1128(%rbp), %r9d
	movl	%eax, %r12d
.L2186:
	movl	-552(%rbp), %eax
	testl	%eax, %eax
	js	.L2187
	cmpl	-548(%rbp), %eax
	jne	.L2188
	movl	$-1, -552(%rbp)
.L2187:
	movq	-544(%rbp), %rdi
	movl	%r9d, -1128(%rbp)
	call	uiter_next32_67@PLT
	movl	-1128(%rbp), %r9d
	movl	%eax, %ebx
.L2189:
	cmpl	%r12d, %ebx
	je	.L2270
	testl	%r12d, %r12d
	js	.L2193
	cmpl	$65534, %r12d
	je	.L2228
	movl	-1080(%rbp), %r13d
	testl	%r13d, %r13d
	js	.L2271
.L2195:
	testl	%ebx, %ebx
	jns	.L2272
	movl	$-2, %ebx
	jmp	.L2196
	.p2align 4,,10
	.p2align 3
.L2181:
	xorl	%r9d, %r9d
.L2139:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2273
	addq	$1128, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2269:
	.cfi_restore_state
	movq	%rcx, -560(%rbp)
	leaq	-528(%rbp), %rcx
	leaq	16+_ZTVN6icu_6722UIterCollationIteratorE(%rip), %rdi
	movq	%rcx, -544(%rbp)
	leaq	-576(%rbp), %r14
	movq	-1128(%rbp), %rcx
	movq	%rdi, -1104(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%r14, %rsi
	movq	%rdi, -576(%rbp)
	movq	%r11, %rdi
	movq	%r11, -1136(%rbp)
	movq	%r15, -712(%rbp)
	movl	$0, -552(%rbp)
	movl	$40, -536(%rbp)
	movb	$0, -532(%rbp)
	movl	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movl	$-1, -192(%rbp)
	movb	%al, -188(%rbp)
	movq	%rbx, -184(%rbp)
	call	_ZN6icu_6716CollationCompare21compareUpToQuaternaryERNS_17CollationIteratorES2_RKNS_17CollationSettingsER10UErrorCode@PLT
	movq	%r14, %rdi
	movl	%eax, -1144(%rbp)
	call	_ZN6icu_6722UIterCollationIteratorD1Ev@PLT
	movq	-1136(%rbp), %rdi
	call	_ZN6icu_6722UIterCollationIteratorD1Ev@PLT
	movl	-1144(%rbp), %r9d
	jmp	.L2180
	.p2align 4,,10
	.p2align 3
.L2267:
	movq	80(%rcx), %rdi
	movl	%r14d, %esi
	movl	%edx, -1152(%rbp)
	movq	%rcx, -1144(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L2167
	movl	-1136(%rbp), %eax
	movq	-1144(%rbp), %rcx
	movl	-1152(%rbp), %edx
	testl	%eax, %eax
	je	.L2157
	cmpl	$1631, %r14d
	jg	.L2150
	subl	$48, %r14d
	cmpl	$9, %r14d
	setbe	%al
.L2151:
	testb	%al, %al
	je	.L2157
	.p2align 4,,10
	.p2align 3
.L2167:
	movq	%r15, %rdi
	subl	$1, %r12d
	call	*80(%r15)
	movq	%rbx, %rdi
	movl	%eax, %r14d
	call	*80(%rbx)
	testl	%r12d, %r12d
	je	.L2178
	movq	8(%r13), %rdx
	movl	%r14d, %esi
	movq	80(%rdx), %rdi
	movq	%rdx, -1144(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L2167
	movl	-1136(%rbp), %r11d
	movq	-1144(%rbp), %rdx
	testl	%r11d, %r11d
	je	.L2178
	cmpl	$1631, %r14d
	jg	.L2171
	subl	$48, %r14d
	cmpl	$9, %r14d
	setbe	%al
.L2172:
	testb	%al, %al
	jne	.L2167
	.p2align 4,,10
	.p2align 3
.L2178:
	movq	8(%r13), %rcx
	jmp	.L2146
.L2268:
	movq	80(%rcx), %rdi
	movl	%edx, %esi
	movq	%rcx, -1152(%rbp)
	movl	%edx, -1144(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L2167
	movl	-1136(%rbp), %r14d
	movl	-1144(%rbp), %edx
	movq	-1152(%rbp), %rcx
	testl	%r14d, %r14d
	je	.L2178
	cmpl	$1631, %edx
	jg	.L2159
	leal	-48(%rdx), %eax
	cmpl	$9, %eax
	setbe	%al
	jmp	.L2172
	.p2align 4,,10
	.p2align 3
.L2171:
	movq	(%rdx), %rcx
	movq	16(%rcx), %rdx
	cmpl	$55295, %r14d
	jg	.L2173
	movl	%r14d, %eax
	movq	(%rcx), %rcx
	sarl	$5, %eax
	cltq
.L2263:
	movzwl	(%rcx,%rax,2), %eax
	andl	$31, %r14d
	leal	(%r14,%rax,4), %eax
	cltq
	salq	$2, %rax
.L2174:
	movl	(%rdx,%rax), %eax
	cmpb	$-65, %al
	jbe	.L2178
.L2265:
	andl	$15, %eax
	cmpl	$10, %eax
	sete	%al
	jmp	.L2172
.L2270:
	testl	%ebx, %ebx
	jns	.L2192
.L2191:
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rbx
	movq	%r14, %rdi
	movl	%r9d, -1128(%rbp)
	movq	%rbx, -576(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	-1136(%rbp), %rdi
	movq	%rbx, -1104(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movl	-1128(%rbp), %r9d
	jmp	.L2139
.L2193:
	testl	%ebx, %ebx
	jns	.L2274
	.p2align 4,,10
	.p2align 3
.L2192:
	movl	-1080(%rbp), %eax
	jmp	.L2183
.L2188:
	leal	1(%rax), %edx
	movq	-568(%rbp), %rcx
	movl	%edx, -552(%rbp)
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %ebx
	leaq	(%rdx,%rdx), %rsi
	movl	%ebx, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L2189
	addl	$2, %eax
	sall	$10, %ebx
	movl	%eax, -552(%rbp)
	movzwl	2(%rcx,%rsi), %eax
	leal	-56613888(%rax,%rbx), %ebx
	jmp	.L2189
.L2185:
	leal	1(%rax), %edx
	movq	-1096(%rbp), %rcx
	movl	%edx, -1080(%rbp)
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %r12d
	leaq	(%rdx,%rdx), %rsi
	movl	%r12d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L2186
	addl	$2, %eax
	sall	$10, %r12d
	movl	%eax, -1080(%rbp)
	movzwl	2(%rcx,%rsi), %eax
	leal	-56613888(%rax,%r12), %r12d
	jmp	.L2186
.L2182:
	movq	(%rdx), %rsi
	movq	-1152(%rbp), %r8
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE(%rip), %r10
	leaq	-1024(%rbp), %rax
	movq	%r10, -1104(%rbp)
	movl	$4294967295, %edi
	leaq	16+_ZTVN6icu_6725FCDUIterCollationIteratorE(%rip), %rcx
	movl	$2, %r9d
	movq	%rdx, -1056(%rbp)
	leaq	-1072(%rbp), %r13
	movq	%rax, -1040(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r10, -576(%rbp)
	movl	$2, %r10d
	movq	%rdx, -528(%rbp)
	leaq	-496(%rbp), %rdx
	movl	%r12d, -668(%rbp)
	movq	%rax, -648(%rbp)
	movq	%rdi, -1080(%rbp)
	movq	%rsi, -1064(%rbp)
	movl	$0, -1048(%rbp)
	movl	$40, -1032(%rbp)
	movb	$0, -1028(%rbp)
	movl	$0, -704(%rbp)
	movq	$0, -696(%rbp)
	movl	$-1, -688(%rbp)
	movb	$0, -684(%rbp)
	movq	%r15, -680(%rbp)
	movq	%rcx, -1072(%rbp)
	movl	$0, -672(%rbp)
	movq	%r8, -656(%rbp)
	movw	%r9w, -640(%rbp)
	movq	%rdi, -552(%rbp)
	movq	%rsi, -536(%rbp)
	movl	$0, -520(%rbp)
	movq	%rdx, -512(%rbp)
	movl	$40, -504(%rbp)
	movb	$0, -500(%rbp)
	movl	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movl	$-1, -160(%rbp)
	movb	$0, -156(%rbp)
	movq	%rcx, -544(%rbp)
	movl	$0, -144(%rbp)
	movq	%r8, -128(%rbp)
	movw	%r10w, -112(%rbp)
	movq	%rbx, -152(%rbp)
	leaq	-1108(%rbp), %rbx
	movl	%r12d, -140(%rbp)
	leaq	-544(%rbp), %r12
	movq	%rax, -120(%rbp)
	movl	$-1, %eax
.L2197:
	testl	%eax, %eax
	js	.L2198
	cmpl	%eax, -1076(%rbp)
	jne	.L2199
	movl	$-1, -1080(%rbp)
.L2198:
	movl	$0, -1108(%rbp)
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6725FCDUIterCollationIterator13nextCodePointER10UErrorCode@PLT
	movl	%eax, %r15d
.L2200:
	movl	-552(%rbp), %eax
	testl	%eax, %eax
	js	.L2201
	cmpl	-548(%rbp), %eax
	jne	.L2202
	movl	$-1, -552(%rbp)
.L2201:
	movl	$0, -1108(%rbp)
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6725FCDUIterCollationIterator13nextCodePointER10UErrorCode@PLT
	movl	%eax, %r8d
.L2203:
	cmpl	%r15d, %r8d
	je	.L2275
	testl	%r15d, %r15d
	js	.L2207
	cmpl	$65534, %r15d
	je	.L2232
	movl	-1080(%rbp), %edx
	testl	%edx, %edx
	js	.L2276
.L2209:
	testl	%r8d, %r8d
	jns	.L2277
	movl	$-2, %r8d
	jmp	.L2210
.L2275:
	testl	%r8d, %r8d
	jns	.L2206
.L2205:
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE(%rip), %r15
	movq	%r12, %rdi
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE(%rip), %rbx
	movq	%r15, -576(%rbp)
	call	_ZN6icu_6725FCDUIterCollationIteratorD1Ev@PLT
	movq	%r14, %rdi
	movq	%rbx, -576(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	movq	%r15, -1104(%rbp)
	call	_ZN6icu_6725FCDUIterCollationIteratorD1Ev@PLT
	movq	-1136(%rbp), %rdi
	movq	%rbx, -1104(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movl	-1156(%rbp), %r9d
	jmp	.L2139
.L2207:
	testl	%r8d, %r8d
	jns	.L2278
	.p2align 4,,10
	.p2align 3
.L2206:
	movl	-1080(%rbp), %eax
	jmp	.L2197
.L2199:
	leal	1(%rax), %edx
	movq	-1096(%rbp), %rcx
	movl	%edx, -1080(%rbp)
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %r15d
	leaq	(%rdx,%rdx), %rsi
	movl	%r15d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L2200
	addl	$2, %eax
	movl	%r15d, %r9d
	movl	%eax, -1080(%rbp)
	movzwl	2(%rcx,%rsi), %eax
	sall	$10, %r9d
	leal	-56613888(%rax,%r9), %r15d
	jmp	.L2200
.L2202:
	leal	1(%rax), %edx
	movq	-568(%rbp), %rcx
	movl	%edx, -552(%rbp)
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %r8d
	leaq	(%rdx,%rdx), %rsi
	movl	%r8d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L2203
	addl	$2, %eax
	sall	$10, %r8d
	movl	%eax, -552(%rbp)
	movzwl	2(%rcx,%rsi), %eax
	leal	-56613888(%rax,%r8), %r8d
	jmp	.L2203
.L2173:
	cmpl	$65535, %r14d
	jbe	.L2279
	movl	$512, %eax
	cmpl	$1114111, %r14d
	ja	.L2174
	cmpl	44(%rcx), %r14d
	jl	.L2177
	movslq	48(%rcx), %rax
	salq	$2, %rax
	jmp	.L2174
.L2278:
	cmpl	$65534, %r8d
	je	.L2238
	movl	$-2, %r15d
.L2219:
	movl	-552(%rbp), %eax
	testl	%eax, %eax
	js	.L2280
.L2210:
	cmpl	%r8d, %r15d
	jl	.L2238
.L2220:
	cmpl	%r15d, %r8d
	jge	.L2206
.L2215:
	movl	$1, -1156(%rbp)
	jmp	.L2205
.L2274:
	cmpl	$65534, %ebx
	je	.L2237
	movl	$-2, %r12d
.L2218:
	movl	-552(%rbp), %r11d
	testl	%r11d, %r11d
	js	.L2281
.L2196:
	cmpl	%r12d, %ebx
	jg	.L2237
.L2217:
	cmpl	%ebx, %r12d
	jle	.L2192
.L2212:
	movl	-1156(%rbp), %r9d
	jmp	.L2191
.L2157:
	movq	8(%r13), %rcx
	jmp	.L2147
.L2271:
	movq	-1152(%rbp), %rdi
	leaq	-1076(%rbp), %rcx
	movl	%r12d, %esi
	leaq	-1088(%rbp), %rdx
	movl	%r9d, -1128(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movl	-1128(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, -1096(%rbp)
	je	.L2195
	movl	$1, -1080(%rbp)
	movzwl	(%rax), %r12d
	movl	%r12d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L2282
.L2194:
	testl	%ebx, %ebx
	js	.L2212
	cmpl	$65534, %ebx
	jne	.L2218
	movl	$-1, %ebx
	jmp	.L2217
.L2276:
	movq	-1152(%rbp), %rdi
	leaq	-1076(%rbp), %rcx
	movl	%r15d, %esi
	leaq	-1088(%rbp), %rdx
	movl	%r8d, -1128(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movl	-1128(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, -1096(%rbp)
	je	.L2209
	movl	$1, -1080(%rbp)
	movzwl	(%rax), %r15d
	movl	%r15d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L2283
.L2208:
	testl	%r8d, %r8d
	js	.L2215
	cmpl	$65534, %r8d
	jne	.L2219
	movl	$-1, %r8d
	jmp	.L2220
.L2279:
	cmpl	$56320, %r14d
	movq	(%rcx), %rsi
	movl	$320, %eax
	movl	$0, %ecx
	cmovl	%eax, %ecx
	movl	%r14d, %eax
	andl	$31, %r14d
	sarl	$5, %eax
	addl	%ecx, %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	leal	(%r14,%rax,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L2174
.L2277:
	cmpl	$65534, %r8d
	jne	.L2219
	movl	$-1, %r8d
	jmp	.L2210
.L2272:
	cmpl	$65534, %ebx
	jne	.L2218
	movl	$-1, %ebx
	jmp	.L2196
.L2228:
	movl	$-1, %r12d
	jmp	.L2194
.L2232:
	movl	$-1, %r15d
	jmp	.L2208
.L2280:
	movq	-1152(%rbp), %rdi
	movl	%r8d, %esi
	leaq	-548(%rbp), %rcx
	leaq	-560(%rbp), %rdx
	movl	%r8d, -1128(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movl	-1128(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, -568(%rbp)
	je	.L2210
	movl	$1, -552(%rbp)
	movzwl	(%rax), %r8d
	movl	%r8d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L2210
	movzwl	2(%rax), %eax
	sall	$10, %r8d
	movl	$2, -552(%rbp)
	leal	-56613888(%r8,%rax), %r8d
	jmp	.L2210
.L2281:
	movq	-1152(%rbp), %rdi
	leaq	-548(%rbp), %rcx
	leaq	-560(%rbp), %rdx
	movl	%ebx, %esi
	movl	%r9d, -1128(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movl	-1128(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, -568(%rbp)
	je	.L2196
	movl	$1, -552(%rbp)
	movzwl	(%rax), %ebx
	movl	%ebx, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L2196
	movzwl	2(%rax), %eax
	sall	$10, %ebx
	movl	$2, -552(%rbp)
	leal	-56613888(%rbx,%rax), %ebx
	jmp	.L2196
.L2150:
	movq	(%rcx), %rsi
	movq	16(%rsi), %rcx
	cmpl	$55295, %r14d
	jg	.L2152
	movl	%r14d, %eax
	movq	(%rsi), %rsi
	sarl	$5, %eax
	cltq
.L2260:
	movzwl	(%rsi,%rax,2), %eax
	andl	$31, %r14d
	leal	(%r14,%rax,4), %eax
	cltq
	salq	$2, %rax
.L2153:
	movl	(%rcx,%rax), %eax
	cmpb	$-65, %al
	jbe	.L2157
	andl	$15, %eax
	cmpl	$10, %eax
	sete	%al
	jmp	.L2151
.L2159:
	movq	(%rcx), %rsi
	movq	16(%rsi), %rcx
	cmpl	$55295, %edx
	jg	.L2161
	movl	%edx, %eax
	movq	(%rsi), %rsi
	sarl	$5, %eax
	cltq
.L2261:
	movzwl	(%rsi,%rax,2), %esi
.L2262:
	movl	%edx, %eax
	andl	$31, %eax
	leal	(%rax,%rsi,4), %eax
	cltq
	salq	$2, %rax
.L2162:
	movl	(%rcx,%rax), %eax
	cmpb	$-65, %al
	jbe	.L2178
	jmp	.L2265
.L2282:
	movzwl	2(%rax), %eax
	sall	$10, %r12d
	movl	$2, -1080(%rbp)
	leal	-56613888(%r12,%rax), %r12d
	jmp	.L2195
.L2283:
	movl	%r15d, %r9d
	movzwl	2(%rax), %eax
	movl	$2, -1080(%rbp)
	sall	$10, %r9d
	leal	-56613888(%r9,%rax), %r15d
	jmp	.L2209
.L2238:
	movl	$-1, -1156(%rbp)
	jmp	.L2205
.L2237:
	movl	$-1, %r9d
	jmp	.L2191
.L2152:
	cmpl	$65535, %r14d
	jg	.L2154
	cmpl	$56320, %r14d
	movq	(%rsi), %rdi
	movl	$320, %eax
	movl	$0, %esi
	cmovl	%eax, %esi
	movl	%r14d, %eax
	andl	$31, %r14d
	sarl	$5, %eax
	addl	%esi, %eax
	cltq
	movzwl	(%rdi,%rax,2), %eax
	leal	(%r14,%rax,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L2153
.L2177:
	movl	%r14d, %eax
	movq	(%rcx), %rcx
	movl	%r14d, %esi
	sarl	$11, %eax
	sarl	$5, %esi
	addl	$2080, %eax
	andl	$63, %esi
	cltq
	movzwl	(%rcx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	jmp	.L2263
.L2161:
	cmpl	$65535, %edx
	jg	.L2163
	cmpl	$56320, %edx
	movq	(%rsi), %rdi
	movl	$320, %eax
	movl	$0, %esi
	cmovl	%eax, %esi
	movl	%edx, %eax
	sarl	$5, %eax
	addl	%esi, %eax
	cltq
	movzwl	(%rdi,%rax,2), %esi
	jmp	.L2262
.L2154:
	movl	$512, %eax
	cmpl	$1114111, %r14d
	ja	.L2153
	cmpl	44(%rsi), %r14d
	jl	.L2156
	movslq	48(%rsi), %rax
	salq	$2, %rax
	jmp	.L2153
.L2163:
	movl	$512, %eax
	cmpl	$1114111, %edx
	ja	.L2162
	cmpl	44(%rsi), %edx
	jl	.L2165
	movslq	48(%rsi), %rax
	salq	$2, %rax
	jmp	.L2162
.L2156:
	movl	%r14d, %eax
	movq	(%rsi), %rsi
	movl	%r14d, %edi
	sarl	$11, %eax
	sarl	$5, %edi
	addl	$2080, %eax
	andl	$63, %edi
	cltq
	movzwl	(%rsi,%rax,2), %eax
	addl	%edi, %eax
	cltq
	jmp	.L2260
.L2165:
	movl	%edx, %eax
	movq	(%rsi), %rsi
	movl	%edx, %edi
	sarl	$11, %eax
	sarl	$5, %edi
	addl	$2080, %eax
	andl	$63, %edi
	cltq
	movzwl	(%rsi,%rax,2), %eax
	addl	%edi, %eax
	cltq
	jmp	.L2261
.L2273:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4928:
	.size	_ZNK6icu_6717RuleBasedCollator7compareER13UCharIteratorS2_R10UErrorCode.part.0, .-_ZNK6icu_6717RuleBasedCollator7compareER13UCharIteratorS2_R10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator7compareER13UCharIteratorS2_R10UErrorCode
	.type	_ZNK6icu_6717RuleBasedCollator7compareER13UCharIteratorS2_R10UErrorCode, @function
_ZNK6icu_6717RuleBasedCollator7compareER13UCharIteratorS2_R10UErrorCode:
.LFB3566:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L2285
	cmpq	%rdx, %rsi
	je	.L2285
	jmp	_ZNK6icu_6717RuleBasedCollator7compareER13UCharIteratorS2_R10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L2285:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3566:
	.size	_ZNK6icu_6717RuleBasedCollator7compareER13UCharIteratorS2_R10UErrorCode, .-_ZNK6icu_6717RuleBasedCollator7compareER13UCharIteratorS2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator30createCollationElementIteratorERKNS_13UnicodeStringE
	.type	_ZNK6icu_6717RuleBasedCollator30createCollationElementIteratorERKNS_13UnicodeStringE, @function
_ZNK6icu_6717RuleBasedCollator30createCollationElementIteratorERKNS_13UnicodeStringE:
.LFB3594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	movl	392(%r12), %eax
	cmpl	$2, %eax
	jne	.L2303
.L2287:
	movl	396(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L2288
.L2302:
	xorl	%r12d, %r12d
.L2286:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2304
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2303:
	.cfi_restore_state
	leaq	392(%r12), %r15
	movq	%r15, %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L2287
	movq	24(%r12), %rdi
	leaq	-44(%rbp), %rsi
	call	_ZN6icu_6724CollationElementIterator20computeMaxExpansionsEPKNS_13CollationDataER10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%rax, 384(%r12)
	movl	-44(%rbp), %eax
	movl	%eax, 396(%r12)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L2288:
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jg	.L2302
	movl	$104, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2302
	movq	%rax, %rdi
	leaq	-44(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_6724CollationElementIteratorC1ERKNS_13UnicodeStringEPKNS_17RuleBasedCollatorER10UErrorCode@PLT
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L2286
	movq	%r12, %rdi
	call	_ZN6icu_6724CollationElementIteratorD0Ev@PLT
	jmp	.L2302
.L2304:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3594:
	.size	_ZNK6icu_6717RuleBasedCollator30createCollationElementIteratorERKNS_13UnicodeStringE, .-_ZNK6icu_6717RuleBasedCollator30createCollationElementIteratorERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717RuleBasedCollator30createCollationElementIteratorERKNS_17CharacterIteratorE
	.type	_ZNK6icu_6717RuleBasedCollator30createCollationElementIteratorERKNS_17CharacterIteratorE, @function
_ZNK6icu_6717RuleBasedCollator30createCollationElementIteratorERKNS_17CharacterIteratorE:
.LFB3595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	movl	392(%r12), %eax
	cmpl	$2, %eax
	jne	.L2322
.L2306:
	movl	396(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L2307
.L2321:
	xorl	%r12d, %r12d
.L2305:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2323
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2322:
	.cfi_restore_state
	leaq	392(%r12), %r15
	movq	%r15, %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L2306
	movq	24(%r12), %rdi
	leaq	-44(%rbp), %rsi
	call	_ZN6icu_6724CollationElementIterator20computeMaxExpansionsEPKNS_13CollationDataER10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%rax, 384(%r12)
	movl	-44(%rbp), %eax
	movl	%eax, 396(%r12)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L2307:
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jg	.L2321
	movl	$104, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2321
	movq	%rax, %rdi
	leaq	-44(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_6724CollationElementIteratorC1ERKNS_17CharacterIteratorEPKNS_17RuleBasedCollatorER10UErrorCode@PLT
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L2305
	movq	%r12, %rdi
	call	_ZN6icu_6724CollationElementIteratorD0Ev@PLT
	jmp	.L2321
.L2323:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3595:
	.size	_ZNK6icu_6717RuleBasedCollator30createCollationElementIteratorERKNS_17CharacterIteratorE, .-_ZNK6icu_6717RuleBasedCollator30createCollationElementIteratorERKNS_17CharacterIteratorE
	.weak	_ZTSN6icu_6717RuleBasedCollatorE
	.section	.rodata._ZTSN6icu_6717RuleBasedCollatorE,"aG",@progbits,_ZTSN6icu_6717RuleBasedCollatorE,comdat
	.align 16
	.type	_ZTSN6icu_6717RuleBasedCollatorE, @object
	.size	_ZTSN6icu_6717RuleBasedCollatorE, 29
_ZTSN6icu_6717RuleBasedCollatorE:
	.string	"N6icu_6717RuleBasedCollatorE"
	.weak	_ZTIN6icu_6717RuleBasedCollatorE
	.section	.data.rel.ro._ZTIN6icu_6717RuleBasedCollatorE,"awG",@progbits,_ZTIN6icu_6717RuleBasedCollatorE,comdat
	.align 8
	.type	_ZTIN6icu_6717RuleBasedCollatorE, @object
	.size	_ZTIN6icu_6717RuleBasedCollatorE, 24
_ZTIN6icu_6717RuleBasedCollatorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717RuleBasedCollatorE
	.quad	_ZTIN6icu_678CollatorE
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE, 24
_ZTIN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE
	.quad	_ZTIN6icu_6715SortKeyByteSinkE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE, 47
_ZTSN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE:
	.string	"*N6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE"
	.weak	_ZTSN6icu_6720CollationKeyByteSinkE
	.section	.rodata._ZTSN6icu_6720CollationKeyByteSinkE,"aG",@progbits,_ZTSN6icu_6720CollationKeyByteSinkE,comdat
	.align 32
	.type	_ZTSN6icu_6720CollationKeyByteSinkE, @object
	.size	_ZTSN6icu_6720CollationKeyByteSinkE, 32
_ZTSN6icu_6720CollationKeyByteSinkE:
	.string	"N6icu_6720CollationKeyByteSinkE"
	.weak	_ZTIN6icu_6720CollationKeyByteSinkE
	.section	.data.rel.ro._ZTIN6icu_6720CollationKeyByteSinkE,"awG",@progbits,_ZTIN6icu_6720CollationKeyByteSinkE,comdat
	.align 8
	.type	_ZTIN6icu_6720CollationKeyByteSinkE, @object
	.size	_ZTIN6icu_6720CollationKeyByteSinkE, 24
_ZTIN6icu_6720CollationKeyByteSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6720CollationKeyByteSinkE
	.quad	_ZTIN6icu_6715SortKeyByteSinkE
	.section	.data.rel.ro
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_111NFDIteratorE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_111NFDIteratorE, 24
_ZTIN6icu_6712_GLOBAL__N_111NFDIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_111NFDIteratorE
	.quad	_ZTIN6icu_677UObjectE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_111NFDIteratorE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_111NFDIteratorE, 38
_ZTSN6icu_6712_GLOBAL__N_111NFDIteratorE:
	.string	"*N6icu_6712_GLOBAL__N_111NFDIteratorE"
	.section	.data.rel.ro
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_116UTF16NFDIteratorE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_116UTF16NFDIteratorE, 24
_ZTIN6icu_6712_GLOBAL__N_116UTF16NFDIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_116UTF16NFDIteratorE
	.quad	_ZTIN6icu_6712_GLOBAL__N_111NFDIteratorE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_116UTF16NFDIteratorE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_116UTF16NFDIteratorE, 43
_ZTSN6icu_6712_GLOBAL__N_116UTF16NFDIteratorE:
	.string	"*N6icu_6712_GLOBAL__N_116UTF16NFDIteratorE"
	.section	.data.rel.ro
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE, 24
_ZTIN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE
	.quad	_ZTIN6icu_6712_GLOBAL__N_116UTF16NFDIteratorE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE, 46
_ZTSN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE:
	.string	"*N6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE"
	.section	.data.rel.ro
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_115UTF8NFDIteratorE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_115UTF8NFDIteratorE, 24
_ZTIN6icu_6712_GLOBAL__N_115UTF8NFDIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_115UTF8NFDIteratorE
	.quad	_ZTIN6icu_6712_GLOBAL__N_111NFDIteratorE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_115UTF8NFDIteratorE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_115UTF8NFDIteratorE, 42
_ZTSN6icu_6712_GLOBAL__N_115UTF8NFDIteratorE:
	.string	"*N6icu_6712_GLOBAL__N_115UTF8NFDIteratorE"
	.section	.data.rel.ro
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE, 24
_ZTIN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE
	.quad	_ZTIN6icu_6712_GLOBAL__N_111NFDIteratorE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE, 45
_ZTSN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE:
	.string	"*N6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE"
	.section	.data.rel.ro
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_116UIterNFDIteratorE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_116UIterNFDIteratorE, 24
_ZTIN6icu_6712_GLOBAL__N_116UIterNFDIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_116UIterNFDIteratorE
	.quad	_ZTIN6icu_6712_GLOBAL__N_111NFDIteratorE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_116UIterNFDIteratorE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_116UIterNFDIteratorE, 43
_ZTSN6icu_6712_GLOBAL__N_116UIterNFDIteratorE:
	.string	"*N6icu_6712_GLOBAL__N_116UIterNFDIteratorE"
	.section	.data.rel.ro
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE, 24
_ZTIN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE
	.quad	_ZTIN6icu_6712_GLOBAL__N_111NFDIteratorE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE, 46
_ZTSN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE:
	.string	"*N6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE"
	.section	.data.rel.ro
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_117PartLevelCallbackE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_117PartLevelCallbackE, 24
_ZTIN6icu_6712_GLOBAL__N_117PartLevelCallbackE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_117PartLevelCallbackE
	.quad	_ZTIN6icu_6713CollationKeys13LevelCallbackE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_117PartLevelCallbackE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_117PartLevelCallbackE, 44
_ZTSN6icu_6712_GLOBAL__N_117PartLevelCallbackE:
	.string	"*N6icu_6712_GLOBAL__N_117PartLevelCallbackE"
	.section	.data.rel.ro
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE, 72
_ZTVN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkE
	.quad	_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSinkD0Ev
	.quad	_ZN6icu_6715SortKeyByteSink6AppendEPKci
	.quad	_ZN6icu_6715SortKeyByteSink15GetAppendBufferEiiPciPi
	.quad	_ZN6icu_678ByteSink5FlushEv
	.quad	_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSink20AppendBeyondCapacityEPKcii
	.quad	_ZN6icu_6712_GLOBAL__N_120FixedSortKeyByteSink6ResizeEii
	.weak	_ZTVN6icu_6720CollationKeyByteSinkE
	.section	.data.rel.ro._ZTVN6icu_6720CollationKeyByteSinkE,"awG",@progbits,_ZTVN6icu_6720CollationKeyByteSinkE,comdat
	.align 8
	.type	_ZTVN6icu_6720CollationKeyByteSinkE, @object
	.size	_ZTVN6icu_6720CollationKeyByteSinkE, 72
_ZTVN6icu_6720CollationKeyByteSinkE:
	.quad	0
	.quad	_ZTIN6icu_6720CollationKeyByteSinkE
	.quad	_ZN6icu_6720CollationKeyByteSinkD1Ev
	.quad	_ZN6icu_6720CollationKeyByteSinkD0Ev
	.quad	_ZN6icu_6715SortKeyByteSink6AppendEPKci
	.quad	_ZN6icu_6715SortKeyByteSink15GetAppendBufferEiiPciPi
	.quad	_ZN6icu_678ByteSink5FlushEv
	.quad	_ZN6icu_6720CollationKeyByteSink20AppendBeyondCapacityEPKcii
	.quad	_ZN6icu_6720CollationKeyByteSink6ResizeEii
	.weak	_ZTVN6icu_6717RuleBasedCollatorE
	.section	.data.rel.ro._ZTVN6icu_6717RuleBasedCollatorE,"awG",@progbits,_ZTVN6icu_6717RuleBasedCollatorE,comdat
	.align 8
	.type	_ZTVN6icu_6717RuleBasedCollatorE, @object
	.size	_ZTVN6icu_6717RuleBasedCollatorE, 344
_ZTVN6icu_6717RuleBasedCollatorE:
	.quad	0
	.quad	_ZTIN6icu_6717RuleBasedCollatorE
	.quad	_ZN6icu_6717RuleBasedCollatorD1Ev
	.quad	_ZN6icu_6717RuleBasedCollatorD0Ev
	.quad	_ZNK6icu_6717RuleBasedCollator17getDynamicClassIDEv
	.quad	_ZNK6icu_6717RuleBasedCollatoreqERKNS_8CollatorE
	.quad	_ZNK6icu_678CollatorneERKS0_
	.quad	_ZNK6icu_6717RuleBasedCollator5cloneEv
	.quad	_ZNK6icu_678Collator7compareERKNS_13UnicodeStringES3_
	.quad	_ZNK6icu_6717RuleBasedCollator7compareERKNS_13UnicodeStringES3_R10UErrorCode
	.quad	_ZNK6icu_678Collator7compareERKNS_13UnicodeStringES3_i
	.quad	_ZNK6icu_6717RuleBasedCollator7compareERKNS_13UnicodeStringES3_iR10UErrorCode
	.quad	_ZNK6icu_678Collator7compareEPKDsiS2_i
	.quad	_ZNK6icu_6717RuleBasedCollator7compareEPKDsiS2_iR10UErrorCode
	.quad	_ZNK6icu_6717RuleBasedCollator7compareER13UCharIteratorS2_R10UErrorCode
	.quad	_ZNK6icu_6717RuleBasedCollator11compareUTF8ERKNS_11StringPieceES3_R10UErrorCode
	.quad	_ZNK6icu_6717RuleBasedCollator15getCollationKeyERKNS_13UnicodeStringERNS_12CollationKeyER10UErrorCode
	.quad	_ZNK6icu_6717RuleBasedCollator15getCollationKeyEPKDsiRNS_12CollationKeyER10UErrorCode
	.quad	_ZNK6icu_6717RuleBasedCollator8hashCodeEv
	.quad	_ZNK6icu_6717RuleBasedCollator9getLocaleE18ULocDataLocaleTypeR10UErrorCode
	.quad	_ZNK6icu_678Collator11getStrengthEv
	.quad	_ZN6icu_678Collator11setStrengthENS0_18ECollationStrengthE
	.quad	_ZNK6icu_6717RuleBasedCollator15getReorderCodesEPiiR10UErrorCode
	.quad	_ZN6icu_6717RuleBasedCollator15setReorderCodesEPKiiR10UErrorCode
	.quad	_ZNK6icu_6717RuleBasedCollator10getVersionEPh
	.quad	_ZN6icu_6717RuleBasedCollator12setAttributeE13UColAttribute18UColAttributeValueR10UErrorCode
	.quad	_ZNK6icu_6717RuleBasedCollator12getAttributeE13UColAttributeR10UErrorCode
	.quad	_ZN6icu_6717RuleBasedCollator14setMaxVariableE15UColReorderCodeR10UErrorCode
	.quad	_ZNK6icu_6717RuleBasedCollator14getMaxVariableEv
	.quad	_ZN6icu_6717RuleBasedCollator14setVariableTopEPKDsiR10UErrorCode
	.quad	_ZN6icu_6717RuleBasedCollator14setVariableTopERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6717RuleBasedCollator14setVariableTopEjR10UErrorCode
	.quad	_ZNK6icu_6717RuleBasedCollator14getVariableTopER10UErrorCode
	.quad	_ZNK6icu_6717RuleBasedCollator14getTailoredSetER10UErrorCode
	.quad	_ZNK6icu_678Collator9safeCloneEv
	.quad	_ZNK6icu_6717RuleBasedCollator10getSortKeyERKNS_13UnicodeStringEPhi
	.quad	_ZNK6icu_6717RuleBasedCollator10getSortKeyEPKDsiPhi
	.quad	_ZN6icu_6717RuleBasedCollator10setLocalesERKNS_6LocaleES3_S3_
	.quad	_ZNK6icu_6717RuleBasedCollator32internalGetShortDefinitionStringEPKcPciR10UErrorCode
	.quad	_ZNK6icu_6717RuleBasedCollator19internalCompareUTF8EPKciS2_iR10UErrorCode
	.quad	_ZNK6icu_6717RuleBasedCollator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode
	.quad	_ZNK6icu_6717RuleBasedCollator30createCollationElementIteratorERKNS_13UnicodeStringE
	.quad	_ZNK6icu_6717RuleBasedCollator30createCollationElementIteratorERKNS_17CharacterIteratorE
	.section	.data.rel.ro
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE, 48
_ZTVN6icu_6712_GLOBAL__N_111NFDIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_111NFDIteratorE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_116UTF16NFDIteratorE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_116UTF16NFDIteratorE, 48
_ZTVN6icu_6712_GLOBAL__N_116UTF16NFDIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_116UTF16NFDIteratorE
	.quad	_ZN6icu_6712_GLOBAL__N_116UTF16NFDIteratorD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_116UTF16NFDIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6712_GLOBAL__N_116UTF16NFDIterator16nextRawCodePointEv
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE, 48
_ZTVN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorE
	.quad	_ZN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_119FCDUTF16NFDIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6712_GLOBAL__N_116UTF16NFDIterator16nextRawCodePointEv
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_115UTF8NFDIteratorE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_115UTF8NFDIteratorE, 48
_ZTVN6icu_6712_GLOBAL__N_115UTF8NFDIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_115UTF8NFDIteratorE
	.quad	_ZN6icu_6712_GLOBAL__N_115UTF8NFDIteratorD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_115UTF8NFDIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6712_GLOBAL__N_115UTF8NFDIterator16nextRawCodePointEv
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE, 48
_ZTVN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorE
	.quad	_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6712_GLOBAL__N_118FCDUTF8NFDIterator16nextRawCodePointEv
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_116UIterNFDIteratorE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_116UIterNFDIteratorE, 48
_ZTVN6icu_6712_GLOBAL__N_116UIterNFDIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_116UIterNFDIteratorE
	.quad	_ZN6icu_6712_GLOBAL__N_116UIterNFDIteratorD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_116UIterNFDIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6712_GLOBAL__N_116UIterNFDIterator16nextRawCodePointEv
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE, 48
_ZTVN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorE
	.quad	_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6712_GLOBAL__N_119FCDUIterNFDIterator16nextRawCodePointEv
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_117PartLevelCallbackE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_117PartLevelCallbackE, 40
_ZTVN6icu_6712_GLOBAL__N_117PartLevelCallbackE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_117PartLevelCallbackE
	.quad	_ZN6icu_6712_GLOBAL__N_117PartLevelCallbackD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_117PartLevelCallbackD0Ev
	.quad	_ZN6icu_6712_GLOBAL__N_117PartLevelCallback11needToWriteENS_9Collation5LevelE
	.section	.rodata
	.type	_ZZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCodeE10terminator, @object
	.size	_ZZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCodeE10terminator, 1
_ZZNK6icu_6717RuleBasedCollator12writeSortKeyEPKDsiRNS_15SortKeyByteSinkER10UErrorCodeE10terminator:
	.zero	1
	.local	_ZZN6icu_6717RuleBasedCollator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6717RuleBasedCollator16getStaticClassIDEvE7classID,1,1
	.section	.data.rel.ro.local
	.align 8
.LC5:
	.quad	_ZTVN6icu_6712_GLOBAL__N_117PartLevelCallbackE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
