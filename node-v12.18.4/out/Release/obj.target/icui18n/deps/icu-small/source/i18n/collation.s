	.file	"collation.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_679Collation25incTwoBytePrimaryByOffsetEjai
	.type	_ZN6icu_679Collation25incTwoBytePrimaryByOffsetEjai, @function
_ZN6icu_679Collation25incTwoBytePrimaryByOffsetEjai:
.LFB54:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	shrl	$16, %eax
	movzbl	%al, %eax
	testb	%sil, %sil
	je	.L2
	leal	-4(%rax,%rdx), %eax
	andl	$-16777216, %edi
	movslq	%eax, %rdx
	movl	%eax, %ecx
	imulq	$-2104705089, %rdx, %rdx
	sarl	$31, %ecx
	shrq	$32, %rdx
	addl	%eax, %edx
	sarl	$7, %edx
	subl	%ecx, %edx
	imull	$251, %edx, %ecx
	sall	$24, %edx
	addl	%edi, %edx
	subl	%ecx, %eax
	addl	$4, %eax
	sall	$16, %eax
	orl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	leal	-2(%rax,%rdx), %eax
	andl	$-16777216, %edi
	movslq	%eax, %rdx
	movl	%eax, %ecx
	imulq	$-2130574327, %rdx, %rdx
	sarl	$31, %ecx
	shrq	$32, %rdx
	addl	%eax, %edx
	sarl	$7, %edx
	subl	%ecx, %edx
	imull	$254, %edx, %ecx
	sall	$24, %edx
	addl	%edi, %edx
	subl	%ecx, %eax
	addl	$2, %eax
	sall	$16, %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE54:
	.size	_ZN6icu_679Collation25incTwoBytePrimaryByOffsetEjai, .-_ZN6icu_679Collation25incTwoBytePrimaryByOffsetEjai
	.align 2
	.p2align 4
	.globl	_ZN6icu_679Collation27incThreeBytePrimaryByOffsetEjai
	.type	_ZN6icu_679Collation27incThreeBytePrimaryByOffsetEjai, @function
_ZN6icu_679Collation27incThreeBytePrimaryByOffsetEjai:
.LFB55:
	.cfi_startproc
	endbr64
	movl	%edi, %ecx
	movl	%esi, %r8d
	movzbl	%ch, %eax
	leal	-2(%rax,%rdx), %eax
	movslq	%eax, %rsi
	cltd
	imulq	$-2130574327, %rsi, %rsi
	shrq	$32, %rsi
	addl	%eax, %esi
	sarl	$7, %esi
	subl	%edx, %esi
	imull	$254, %esi, %edx
	subl	%edx, %eax
	movl	%edi, %edx
	addl	$2, %eax
	shrl	$16, %edx
	sall	$8, %eax
	movzbl	%dl, %edx
	testb	%r8b, %r8b
	je	.L6
	leal	-4(%rdx,%rsi), %esi
	andl	$-16777216, %ecx
	movslq	%esi, %rdx
	movl	%esi, %edi
	imulq	$-2104705089, %rdx, %rdx
	sarl	$31, %edi
	shrq	$32, %rdx
	addl	%esi, %edx
	sarl	$7, %edx
	subl	%edi, %edx
	imull	$251, %edx, %edi
	sall	$24, %edx
	addl	%ecx, %edx
	subl	%edi, %esi
	addl	$4, %esi
	sall	$16, %esi
	orl	%esi, %eax
	orl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	leal	-2(%rdx,%rsi), %esi
	andl	$-16777216, %ecx
	movslq	%esi, %rdx
	movl	%esi, %edi
	imulq	$-2130574327, %rdx, %rdx
	sarl	$31, %edi
	shrq	$32, %rdx
	addl	%esi, %edx
	sarl	$7, %edx
	subl	%edi, %edx
	imull	$254, %edx, %edi
	sall	$24, %edx
	addl	%ecx, %edx
	subl	%edi, %esi
	addl	$2, %esi
	sall	$16, %esi
	orl	%esi, %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE55:
	.size	_ZN6icu_679Collation27incThreeBytePrimaryByOffsetEjai, .-_ZN6icu_679Collation27incThreeBytePrimaryByOffsetEjai
	.align 2
	.p2align 4
	.globl	_ZN6icu_679Collation26decTwoBytePrimaryByOneStepEjai
	.type	_ZN6icu_679Collation26decTwoBytePrimaryByOneStepEjai, @function
_ZN6icu_679Collation26decTwoBytePrimaryByOneStepEjai:
.LFB56:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	shrl	$16, %eax
	movzbl	%al, %eax
	subl	%edx, %eax
	testb	%sil, %sil
	je	.L9
	cmpl	$3, %eax
	jg	.L10
	addl	$251, %eax
	subl	$16777216, %edi
.L10:
	andl	$-16777216, %edi
	sall	$16, %eax
	orl	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	cmpl	$1, %eax
	jg	.L10
	addl	$254, %eax
	subl	$16777216, %edi
	andl	$-16777216, %edi
	sall	$16, %eax
	orl	%edi, %eax
	ret
	.cfi_endproc
.LFE56:
	.size	_ZN6icu_679Collation26decTwoBytePrimaryByOneStepEjai, .-_ZN6icu_679Collation26decTwoBytePrimaryByOneStepEjai
	.align 2
	.p2align 4
	.globl	_ZN6icu_679Collation28decThreeBytePrimaryByOneStepEjai
	.type	_ZN6icu_679Collation28decThreeBytePrimaryByOneStepEjai, @function
_ZN6icu_679Collation28decThreeBytePrimaryByOneStepEjai:
.LFB57:
	.cfi_startproc
	endbr64
	movl	%edi, %ecx
	movzbl	%ch, %eax
	subl	%edx, %eax
	cmpl	$1, %eax
	jg	.L19
	movl	%edi, %edx
	addl	$254, %eax
	shrl	$16, %edx
	movzbl	%dl, %edx
	subl	$1, %edx
	testb	%sil, %sil
	je	.L14
	cmpl	$3, %edx
	jle	.L15
.L18:
	sall	$16, %edx
	sall	$8, %eax
	andl	$-16777216, %ecx
	orl	%edx, %eax
	orl	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	cmpl	$1, %edx
	jg	.L18
	subl	$16777216, %ecx
	movl	$16711680, %edx
	sall	$8, %eax
	orl	%edx, %eax
	andl	$-16777216, %ecx
	orl	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	xorw	%cx, %cx
	sall	$8, %eax
	orl	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	subl	$16777216, %ecx
	movl	$16646144, %edx
	sall	$8, %eax
	orl	%edx, %eax
	andl	$-16777216, %ecx
	orl	%ecx, %eax
	ret
	.cfi_endproc
.LFE57:
	.size	_ZN6icu_679Collation28decThreeBytePrimaryByOneStepEjai, .-_ZN6icu_679Collation28decThreeBytePrimaryByOneStepEjai
	.align 2
	.p2align 4
	.globl	_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil
	.type	_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil, @function
_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil:
.LFB58:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	movq	%rsi, %rcx
	sarl	$8, %eax
	sarq	$32, %rcx
	subl	%eax, %edi
	movl	%esi, %eax
	andl	$127, %eax
	imull	%eax, %edi
	movq	%rsi, %rax
	shrq	$40, %rax
	movzbl	%al, %eax
	leal	-2(%rdi,%rax), %eax
	movslq	%eax, %rdi
	cltd
	imulq	$-2130574327, %rdi, %rdi
	shrq	$32, %rdi
	addl	%eax, %edi
	sarl	$7, %edi
	subl	%edx, %edi
	imull	$254, %edi, %edx
	subl	%edx, %eax
	movq	%rsi, %rdx
	addl	$2, %eax
	shrq	$48, %rdx
	sall	$8, %eax
	andl	$128, %esi
	movzbl	%dl, %edx
	je	.L21
	leal	-4(%rdx,%rdi), %esi
	andl	$-16777216, %ecx
	movslq	%esi, %rdx
	movl	%esi, %edi
	imulq	$-2104705089, %rdx, %rdx
	sarl	$31, %edi
	shrq	$32, %rdx
	addl	%esi, %edx
	sarl	$7, %edx
	subl	%edi, %edx
	imull	$251, %edx, %edi
	sall	$24, %edx
	addl	%ecx, %edx
	subl	%edi, %esi
	addl	$4, %esi
	sall	$16, %esi
	orl	%esi, %eax
	orl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	leal	-2(%rdx,%rdi), %esi
	andl	$-16777216, %ecx
	movslq	%esi, %rdx
	movl	%esi, %edi
	imulq	$-2130574327, %rdx, %rdx
	sarl	$31, %edi
	shrq	$32, %rdx
	addl	%esi, %edx
	sarl	$7, %edx
	subl	%edi, %edx
	imull	$254, %edx, %edi
	sall	$24, %edx
	addl	%ecx, %edx
	subl	%edi, %esi
	addl	$2, %esi
	sall	$16, %esi
	orl	%esi, %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE58:
	.size	_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil, .-_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil
	.align 2
	.p2align 4
	.globl	_ZN6icu_679Collation30unassignedPrimaryFromCodePointEi
	.type	_ZN6icu_679Collation30unassignedPrimaryFromCodePointEi, @function
_ZN6icu_679Collation30unassignedPrimaryFromCodePointEi:
.LFB59:
	.cfi_startproc
	endbr64
	addl	$1, %edi
	movslq	%edi, %rdx
	movl	%edi, %eax
	imulq	$954437177, %rdx, %rdx
	sarl	$31, %eax
	sarq	$34, %rdx
	subl	%eax, %edx
	movslq	%edx, %rcx
	movl	%edx, %eax
	imulq	$-2130574327, %rcx, %rcx
	sarl	$31, %eax
	shrq	$32, %rcx
	addl	%edx, %ecx
	sarl	$7, %ecx
	subl	%eax, %ecx
	movslq	%ecx, %rsi
	movl	%ecx, %eax
	imulq	$-2104705089, %rsi, %rsi
	sarl	$31, %eax
	shrq	$32, %rsi
	addl	%ecx, %esi
	sarl	$7, %esi
	subl	%eax, %esi
	movl	%ecx, %eax
	imull	$251, %esi, %esi
	imull	$254, %ecx, %ecx
	subl	%esi, %eax
	movl	%eax, %esi
	leal	(%rdx,%rdx,8), %eax
	subl	%ecx, %edx
	addl	%eax, %eax
	addl	$4, %esi
	subl	%eax, %edi
	sall	$16, %esi
	imull	$14, %edi, %edi
	leal	2(%rdi), %eax
	orl	%eax, %esi
	leal	2(%rdx), %eax
	sall	$8, %eax
	orl	%esi, %eax
	orl	$-33554432, %eax
	ret
	.cfi_endproc
.LFE59:
	.size	_ZN6icu_679Collation30unassignedPrimaryFromCodePointEi, .-_ZN6icu_679Collation30unassignedPrimaryFromCodePointEi
	.globl	_ZN6icu_679Collation22CASE_AND_TERTIARY_MASKE
	.section	.rodata
	.align 4
	.type	_ZN6icu_679Collation22CASE_AND_TERTIARY_MASKE, @object
	.size	_ZN6icu_679Collation22CASE_AND_TERTIARY_MASKE, 4
_ZN6icu_679Collation22CASE_AND_TERTIARY_MASKE:
	.long	65343
	.globl	_ZN6icu_679Collation18ONLY_TERTIARY_MASKE
	.align 4
	.type	_ZN6icu_679Collation18ONLY_TERTIARY_MASKE, @object
	.size	_ZN6icu_679Collation18ONLY_TERTIARY_MASKE, 4
_ZN6icu_679Collation18ONLY_TERTIARY_MASKE:
	.long	16191
	.globl	_ZN6icu_679Collation20MERGE_SEPARATOR_BYTEE
	.type	_ZN6icu_679Collation20MERGE_SEPARATOR_BYTEE, @object
	.size	_ZN6icu_679Collation20MERGE_SEPARATOR_BYTEE, 1
_ZN6icu_679Collation20MERGE_SEPARATOR_BYTEE:
	.byte	2
	.globl	_ZN6icu_679Collation20LEVEL_SEPARATOR_BYTEE
	.type	_ZN6icu_679Collation20LEVEL_SEPARATOR_BYTEE, @object
	.size	_ZN6icu_679Collation20LEVEL_SEPARATOR_BYTEE, 1
_ZN6icu_679Collation20LEVEL_SEPARATOR_BYTEE:
	.byte	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
