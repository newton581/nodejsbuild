	.file	"format.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713FieldPosition17getDynamicClassIDEv
	.type	_ZNK6icu_6713FieldPosition17getDynamicClassIDEv, @function
_ZNK6icu_6713FieldPosition17getDynamicClassIDEv:
.LFB2425:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713FieldPosition16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2425:
	.size	_ZNK6icu_6713FieldPosition17getDynamicClassIDEv, .-_ZNK6icu_6713FieldPosition17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB2443:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	movl	(%r8), %edx
	testl	%edx, %edx
	jle	.L5
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$16, (%r8)
	ret
	.cfi_endproc
.LFE2443:
	.size	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713FieldPositionD2Ev
	.type	_ZN6icu_6713FieldPositionD2Ev, @function
_ZN6icu_6713FieldPositionD2Ev:
.LFB2427:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2427:
	.size	_ZN6icu_6713FieldPositionD2Ev, .-_ZN6icu_6713FieldPositionD2Ev
	.globl	_ZN6icu_6713FieldPositionD1Ev
	.set	_ZN6icu_6713FieldPositionD1Ev,_ZN6icu_6713FieldPositionD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713FieldPositionD0Ev
	.type	_ZN6icu_6713FieldPositionD0Ev, @function
_ZN6icu_6713FieldPositionD0Ev:
.LFB2429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2429:
	.size	_ZN6icu_6713FieldPositionD0Ev, .-_ZN6icu_6713FieldPositionD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713FieldPosition16getStaticClassIDEv
	.type	_ZN6icu_6713FieldPosition16getStaticClassIDEv, @function
_ZN6icu_6713FieldPosition16getStaticClassIDEv:
.LFB2424:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713FieldPosition16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2424:
	.size	_ZN6icu_6713FieldPosition16getStaticClassIDEv, .-_ZN6icu_6713FieldPosition16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713FieldPosition5cloneEv
	.type	_ZNK6icu_6713FieldPosition5cloneEv, @function
_ZNK6icu_6713FieldPosition5cloneEv:
.LFB2430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L10
	movl	16(%rbx), %edx
	movq	8(%rbx), %rcx
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rsi
	movq	%rsi, (%rax)
	movq	%rcx, 8(%rax)
	movl	%edx, 16(%rax)
.L10:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2430:
	.size	_ZNK6icu_6713FieldPosition5cloneEv, .-_ZNK6icu_6713FieldPosition5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676FormatC2Ev
	.type	_ZN6icu_676FormatC2Ev, @function
_ZN6icu_676FormatC2Ev:
.LFB2432:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676FormatE(%rip), %rax
	movb	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 165(%rdi)
	ret
	.cfi_endproc
.LFE2432:
	.size	_ZN6icu_676FormatC2Ev, .-_ZN6icu_676FormatC2Ev
	.globl	_ZN6icu_676FormatC1Ev
	.set	_ZN6icu_676FormatC1Ev,_ZN6icu_676FormatC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676FormatD2Ev
	.type	_ZN6icu_676FormatD2Ev, @function
_ZN6icu_676FormatD2Ev:
.LFB2435:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676FormatE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2435:
	.size	_ZN6icu_676FormatD2Ev, .-_ZN6icu_676FormatD2Ev
	.globl	_ZN6icu_676FormatD1Ev
	.set	_ZN6icu_676FormatD1Ev,_ZN6icu_676FormatD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676FormatD0Ev
	.type	_ZN6icu_676FormatD0Ev, @function
_ZN6icu_676FormatD0Ev:
.LFB2437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676FormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2437:
	.size	_ZN6icu_676FormatD0Ev, .-_ZN6icu_676FormatD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676FormatC2ERKS0_
	.type	_ZN6icu_676FormatC2ERKS0_, @function
_ZN6icu_676FormatC2ERKS0_:
.LFB2439:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676FormatE(%rip), %rax
	movq	%rax, (%rdi)
	cmpq	%rsi, %rdi
	je	.L20
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	165(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	165(%rdi), %rdi
	call	strcpy@PLT
	leaq	8(%r12), %rsi
	leaq	8(%rbx), %rdi
	popq	%rbx
	.cfi_restore 3
	movl	$157, %edx
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	__strcpy_chk@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	ret
	.cfi_endproc
.LFE2439:
	.size	_ZN6icu_676FormatC2ERKS0_, .-_ZN6icu_676FormatC2ERKS0_
	.globl	_ZN6icu_676FormatC1ERKS0_
	.set	_ZN6icu_676FormatC1ERKS0_,_ZN6icu_676FormatC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676FormataSERKS0_
	.type	_ZN6icu_676FormataSERKS0_, @function
_ZN6icu_676FormataSERKS0_:
.LFB2441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L26
	movq	%rsi, %rbx
	leaq	165(%rdi), %rdi
	leaq	165(%rsi), %rsi
	call	strcpy@PLT
	leaq	8(%rbx), %rsi
	leaq	8(%r12), %rdi
	movl	$157, %edx
	call	__strcpy_chk@PLT
.L26:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2441:
	.size	_ZN6icu_676FormataSERKS0_, .-_ZN6icu_676FormataSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode:
.LFB2442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L28
	movl	$4294967295, %eax
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rbx
	leaq	-64(%rbp), %r13
	movq	%rcx, %r8
	movq	%rax, -56(%rbp)
	movq	(%rdi), %rax
	movq	%r13, %rcx
	movq	%rbx, -64(%rbp)
	movl	$0, -48(%rbp)
	call	*40(%rax)
	movq	%r13, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, %r12
	call	_ZN6icu_677UObjectD2Ev@PLT
.L28:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L33:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2442:
	.size	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Format11parseObjectERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode
	.type	_ZNK6icu_676Format11parseObjectERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode, @function
_ZNK6icu_676Format11parseObjectERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode:
.LFB2444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L34
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	leaq	-48(%rbp), %r12
	movq	%rcx, %rbx
	movq	%rax, -48(%rbp)
	movq	%r12, %rcx
	movabsq	$-4294967296, %rax
	movq	%rax, -40(%rbp)
	movq	(%rdi), %rax
	call	*56(%rax)
	movl	-40(%rbp), %eax
	testl	%eax, %eax
	je	.L40
.L37:
	movq	%r12, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
.L34:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movl	$3, (%rbx)
	jmp	.L37
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2444:
	.size	_ZNK6icu_676Format11parseObjectERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode, .-_ZNK6icu_676Format11parseObjectERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676FormateqERKS0_
	.type	_ZNK6icu_676FormateqERKS0_, @function
_ZNK6icu_676FormateqERKS0_:
.LFB2445:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L47
	xorl	%eax, %eax
	cmpb	$42, (%rdi)
	je	.L47
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	strcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2445:
	.size	_ZNK6icu_676FormateqERKS0_, .-_ZNK6icu_676FormateqERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676Format11syntaxErrorERKNS_13UnicodeStringEiR11UParseError
	.type	_ZN6icu_676Format11syntaxErrorERKNS_13UnicodeStringEiR11UParseError, @function
_ZN6icu_676Format11syntaxErrorERKNS_13UnicodeStringEiR11UParseError:
.LFB2446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdx), %r15
	pushq	%r14
	movq	%r15, %rcx
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movl	%ebx, %r14d
	subq	$8, %rsp
	cmpl	$15, %esi
	movl	%esi, 4(%rdx)
	movl	$15, %esi
	cmovge	%ebx, %esi
	movl	$0, (%rdx)
	xorl	%r8d, %r8d
	subl	$15, %esi
	subl	%esi, %r14d
	movl	%r14d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movswl	8(%r13), %esi
	movslq	%r14d, %r14
	xorl	%edx, %edx
	leal	1(%rbx), %r9d
	movw	%dx, 8(%r12,%r14,2)
	addl	$15, %ebx
	testw	%si, %si
	js	.L51
	sarl	$5, %esi
.L52:
	cmpl	%esi, %ebx
	leaq	40(%r12), %r14
	movq	%r13, %rdi
	cmovg	%esi, %ebx
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	%r9d, %esi
	subl	%r9d, %ebx
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%eax, %eax
	movslq	%ebx, %rbx
	movw	%ax, 40(%r12,%rbx,2)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movl	12(%r13), %esi
	jmp	.L52
	.cfi_endproc
.LFE2446:
	.size	_ZN6icu_676Format11syntaxErrorERKNS_13UnicodeStringEiR11UParseError, .-_ZN6icu_676Format11syntaxErrorERKNS_13UnicodeStringEiR11UParseError
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Format9getLocaleE18ULocDataLocaleTypeR10UErrorCode
	.type	_ZNK6icu_676Format9getLocaleE18ULocDataLocaleTypeR10UErrorCode, @function
_ZNK6icu_676Format9getLocaleE18ULocDataLocaleTypeR10UErrorCode:
.LFB2447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	165(%rsi), %rax
	addq	$8, %rsi
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	leaq	-48(%rbp), %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZNK6icu_6711LocaleBased9getLocaleE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2447:
	.size	_ZNK6icu_676Format9getLocaleE18ULocDataLocaleTypeR10UErrorCode, .-_ZNK6icu_676Format9getLocaleE18ULocDataLocaleTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Format11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode
	.type	_ZNK6icu_676Format11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode, @function
_ZNK6icu_676Format11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode:
.LFB2448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	165(%rdi), %rax
	addq	$8, %rdi
	movq	%rdi, %xmm1
	movq	%rax, %xmm0
	leaq	-32(%rbp), %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -32(%rbp)
	call	_ZNK6icu_6711LocaleBased11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L61
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L61:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2448:
	.size	_ZNK6icu_676Format11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode, .-_ZNK6icu_676Format11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676Format12setLocaleIDsEPKcS2_
	.type	_ZN6icu_676Format12setLocaleIDsEPKcS2_, @function
_ZN6icu_676Format12setLocaleIDsEPKcS2_:
.LFB2449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	165(%rdi), %rax
	addq	$8, %rdi
	movq	%rax, %xmm0
	movq	%rdi, %xmm1
	leaq	-32(%rbp), %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -32(%rbp)
	call	_ZN6icu_6711LocaleBased12setLocaleIDsEPKcS2_@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2449:
	.size	_ZN6icu_676Format12setLocaleIDsEPKcS2_, .-_ZN6icu_676Format12setLocaleIDsEPKcS2_
	.weak	_ZTSN6icu_6713FieldPositionE
	.section	.rodata._ZTSN6icu_6713FieldPositionE,"aG",@progbits,_ZTSN6icu_6713FieldPositionE,comdat
	.align 16
	.type	_ZTSN6icu_6713FieldPositionE, @object
	.size	_ZTSN6icu_6713FieldPositionE, 25
_ZTSN6icu_6713FieldPositionE:
	.string	"N6icu_6713FieldPositionE"
	.weak	_ZTIN6icu_6713FieldPositionE
	.section	.data.rel.ro._ZTIN6icu_6713FieldPositionE,"awG",@progbits,_ZTIN6icu_6713FieldPositionE,comdat
	.align 8
	.type	_ZTIN6icu_6713FieldPositionE, @object
	.size	_ZTIN6icu_6713FieldPositionE, 24
_ZTIN6icu_6713FieldPositionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713FieldPositionE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_676FormatE
	.section	.rodata._ZTSN6icu_676FormatE,"aG",@progbits,_ZTSN6icu_676FormatE,comdat
	.align 16
	.type	_ZTSN6icu_676FormatE, @object
	.size	_ZTSN6icu_676FormatE, 17
_ZTSN6icu_676FormatE:
	.string	"N6icu_676FormatE"
	.weak	_ZTIN6icu_676FormatE
	.section	.data.rel.ro._ZTIN6icu_676FormatE,"awG",@progbits,_ZTIN6icu_676FormatE,comdat
	.align 8
	.type	_ZTIN6icu_676FormatE, @object
	.size	_ZTIN6icu_676FormatE, 24
_ZTIN6icu_676FormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676FormatE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6713FieldPositionE
	.section	.data.rel.ro.local._ZTVN6icu_6713FieldPositionE,"awG",@progbits,_ZTVN6icu_6713FieldPositionE,comdat
	.align 8
	.type	_ZTVN6icu_6713FieldPositionE, @object
	.size	_ZTVN6icu_6713FieldPositionE, 40
_ZTVN6icu_6713FieldPositionE:
	.quad	0
	.quad	_ZTIN6icu_6713FieldPositionE
	.quad	_ZN6icu_6713FieldPositionD1Ev
	.quad	_ZN6icu_6713FieldPositionD0Ev
	.quad	_ZNK6icu_6713FieldPosition17getDynamicClassIDEv
	.weak	_ZTVN6icu_676FormatE
	.section	.data.rel.ro._ZTVN6icu_676FormatE,"awG",@progbits,_ZTVN6icu_676FormatE,comdat
	.align 8
	.type	_ZTVN6icu_676FormatE, @object
	.size	_ZTVN6icu_676FormatE, 80
_ZTVN6icu_676FormatE:
	.quad	0
	.quad	_ZTIN6icu_676FormatE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	__cxa_pure_virtual
	.local	_ZZN6icu_6713FieldPosition16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6713FieldPosition16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
