	.file	"uitercollationiterator.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722UIterCollationIterator9getOffsetEv
	.type	_ZNK6icu_6722UIterCollationIterator9getOffsetEv, @function
_ZNK6icu_6722UIterCollationIterator9getOffsetEv:
.LFB3262:
	.cfi_startproc
	endbr64
	movq	392(%rdi), %rdi
	movl	$1, %esi
	movq	32(%rdi), %rax
	jmp	*%rax
	.cfi_endproc
.LFE3262:
	.size	_ZNK6icu_6722UIterCollationIterator9getOffsetEv, .-_ZNK6icu_6722UIterCollationIterator9getOffsetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UIterCollationIterator14handleNextCE32ERiR10UErrorCode
	.type	_ZN6icu_6722UIterCollationIterator14handleNextCE32ERiR10UErrorCode, @function
_ZN6icu_6722UIterCollationIterator14handleNextCE32ERiR10UErrorCode:
.LFB3263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	392(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, %rdi
	call	*72(%rax)
	movl	%eax, (%r12)
	testl	%eax, %eax
	js	.L5
	movq	8(%rbx), %rcx
	movl	%eax, %edx
	andl	$31, %eax
	popq	%rbx
	sarl	$5, %edx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	(%rcx), %rsi
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %eax
	movq	16(%rcx), %rdx
	cltq
	movl	(%rdx,%rax,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	popq	%rbx
	movl	$192, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3263:
	.size	_ZN6icu_6722UIterCollationIterator14handleNextCE32ERiR10UErrorCode, .-_ZN6icu_6722UIterCollationIterator14handleNextCE32ERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UIterCollationIterator23handleGetTrailSurrogateEv
	.type	_ZN6icu_6722UIterCollationIterator23handleGetTrailSurrogateEv, @function
_ZN6icu_6722UIterCollationIterator23handleGetTrailSurrogateEv:
.LFB3264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	392(%rdi), %rax
	movq	%rax, %rdi
	call	*72(%rax)
	movl	%eax, %ebx
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L8
	testl	%ebx, %ebx
	js	.L8
	movq	392(%r12), %rax
	movq	%rax, %rdi
	call	*80(%rax)
.L8:
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3264:
	.size	_ZN6icu_6722UIterCollationIterator23handleGetTrailSurrogateEv, .-_ZN6icu_6722UIterCollationIterator23handleGetTrailSurrogateEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UIterCollationIteratorD2Ev
	.type	_ZN6icu_6722UIterCollationIteratorD2Ev, @function
_ZN6icu_6722UIterCollationIteratorD2Ev:
.LFB3258:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6722UIterCollationIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CollationIteratorD2Ev@PLT
	.cfi_endproc
.LFE3258:
	.size	_ZN6icu_6722UIterCollationIteratorD2Ev, .-_ZN6icu_6722UIterCollationIteratorD2Ev
	.globl	_ZN6icu_6722UIterCollationIteratorD1Ev
	.set	_ZN6icu_6722UIterCollationIteratorD1Ev,_ZN6icu_6722UIterCollationIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UIterCollationIteratorD0Ev
	.type	_ZN6icu_6722UIterCollationIteratorD0Ev, @function
_ZN6icu_6722UIterCollationIteratorD0Ev:
.LFB3260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722UIterCollationIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CollationIteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3260:
	.size	_ZN6icu_6722UIterCollationIteratorD0Ev, .-_ZN6icu_6722UIterCollationIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UIterCollationIterator13resetToOffsetEi
	.type	_ZN6icu_6722UIterCollationIterator13resetToOffsetEi, @function
_ZN6icu_6722UIterCollationIterator13resetToOffsetEi:
.LFB3261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6717CollationIterator5resetEv@PLT
	movq	392(%rbx), %rdi
	movl	%r12d, %esi
	popq	%rbx
	xorl	%edx, %edx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	40(%rdi), %rax
	jmp	*%rax
	.cfi_endproc
.LFE3261:
	.size	_ZN6icu_6722UIterCollationIterator13resetToOffsetEi, .-_ZN6icu_6722UIterCollationIterator13resetToOffsetEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUIterCollationIterator13resetToOffsetEi
	.type	_ZN6icu_6725FCDUIterCollationIterator13resetToOffsetEi, @function
_ZN6icu_6725FCDUIterCollationIterator13resetToOffsetEi:
.LFB3273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6717CollationIterator5resetEv@PLT
	movq	392(%rbx), %rax
	movl	%r12d, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	*40(%rax)
	movl	%r12d, 404(%rbx)
	movl	$0, 400(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3273:
	.size	_ZN6icu_6725FCDUIterCollationIterator13resetToOffsetEi, .-_ZN6icu_6725FCDUIterCollationIterator13resetToOffsetEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UIterCollationIterator13nextCodePointER10UErrorCode
	.type	_ZN6icu_6722UIterCollationIterator13nextCodePointER10UErrorCode, @function
_ZN6icu_6722UIterCollationIterator13nextCodePointER10UErrorCode:
.LFB3265:
	.cfi_startproc
	endbr64
	movq	392(%rdi), %rdi
	jmp	uiter_next32_67@PLT
	.cfi_endproc
.LFE3265:
	.size	_ZN6icu_6722UIterCollationIterator13nextCodePointER10UErrorCode, .-_ZN6icu_6722UIterCollationIterator13nextCodePointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UIterCollationIterator20forwardNumCodePointsEiR10UErrorCode
	.type	_ZN6icu_6722UIterCollationIterator20forwardNumCodePointsEiR10UErrorCode, @function
_ZN6icu_6722UIterCollationIterator20forwardNumCodePointsEiR10UErrorCode:
.LFB3267:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jle	.L26
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L29:
	subl	$1, %ebx
	je	.L21
.L23:
	movq	392(%r12), %rdi
	call	uiter_next32_67@PLT
	testl	%eax, %eax
	jns	.L29
.L21:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L26:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE3267:
	.size	_ZN6icu_6722UIterCollationIterator20forwardNumCodePointsEiR10UErrorCode, .-_ZN6icu_6722UIterCollationIterator20forwardNumCodePointsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UIterCollationIterator17previousCodePointER10UErrorCode
	.type	_ZN6icu_6722UIterCollationIterator17previousCodePointER10UErrorCode, @function
_ZN6icu_6722UIterCollationIterator17previousCodePointER10UErrorCode:
.LFB3266:
	.cfi_startproc
	endbr64
	movq	392(%rdi), %rdi
	jmp	uiter_previous32_67@PLT
	.cfi_endproc
.LFE3266:
	.size	_ZN6icu_6722UIterCollationIterator17previousCodePointER10UErrorCode, .-_ZN6icu_6722UIterCollationIterator17previousCodePointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UIterCollationIterator21backwardNumCodePointsEiR10UErrorCode
	.type	_ZN6icu_6722UIterCollationIterator21backwardNumCodePointsEiR10UErrorCode, @function
_ZN6icu_6722UIterCollationIterator21backwardNumCodePointsEiR10UErrorCode:
.LFB3268:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jle	.L36
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L39:
	subl	$1, %ebx
	je	.L31
.L33:
	movq	392(%r12), %rdi
	call	uiter_previous32_67@PLT
	testl	%eax, %eax
	jns	.L39
.L31:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L36:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE3268:
	.size	_ZN6icu_6722UIterCollationIterator21backwardNumCodePointsEiR10UErrorCode, .-_ZN6icu_6722UIterCollationIterator21backwardNumCodePointsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUIterCollationIteratorD2Ev
	.type	_ZN6icu_6725FCDUIterCollationIteratorD2Ev, @function
_ZN6icu_6725FCDUIterCollationIteratorD2Ev:
.LFB3270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725FCDUIterCollationIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	424(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -424(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6722UIterCollationIteratorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717CollationIteratorD2Ev@PLT
	.cfi_endproc
.LFE3270:
	.size	_ZN6icu_6725FCDUIterCollationIteratorD2Ev, .-_ZN6icu_6725FCDUIterCollationIteratorD2Ev
	.globl	_ZN6icu_6725FCDUIterCollationIteratorD1Ev
	.set	_ZN6icu_6725FCDUIterCollationIteratorD1Ev,_ZN6icu_6725FCDUIterCollationIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUIterCollationIteratorD0Ev
	.type	_ZN6icu_6725FCDUIterCollationIteratorD0Ev, @function
_ZN6icu_6725FCDUIterCollationIteratorD0Ev:
.LFB3272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725FCDUIterCollationIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	424(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -424(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6722UIterCollationIteratorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6717CollationIteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3272:
	.size	_ZN6icu_6725FCDUIterCollationIteratorD0Ev, .-_ZN6icu_6725FCDUIterCollationIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725FCDUIterCollationIterator9getOffsetEv
	.type	_ZNK6icu_6725FCDUIterCollationIterator9getOffsetEv, @function
_ZNK6icu_6725FCDUIterCollationIterator9getOffsetEv:
.LFB3274:
	.cfi_startproc
	endbr64
	movl	400(%rdi), %eax
	cmpl	$1, %eax
	jle	.L51
	movl	408(%rdi), %r8d
	cmpl	$2, %eax
	je	.L44
	testl	%r8d, %r8d
	jne	.L47
	movl	404(%rdi), %r8d
.L44:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	movq	392(%rdi), %rdi
	movl	$1, %esi
	movq	32(%rdi), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L47:
	movl	412(%rdi), %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3274:
	.size	_ZNK6icu_6725FCDUIterCollationIterator9getOffsetEv, .-_ZNK6icu_6725FCDUIterCollationIterator9getOffsetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUIterCollationIterator23handleGetTrailSurrogateEv
	.type	_ZN6icu_6725FCDUIterCollationIterator23handleGetTrailSurrogateEv, @function
_ZN6icu_6725FCDUIterCollationIterator23handleGetTrailSurrogateEv:
.LFB3276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpl	$2, 400(%rdi)
	movq	%rdi, %rbx
	jg	.L53
	movq	392(%rdi), %rax
	movq	%rax, %rdi
	call	*72(%rax)
	movl	%eax, %r12d
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L63
	testl	%r12d, %r12d
	js	.L55
	movq	392(%rbx), %rax
	movq	%rax, %rdi
	call	*80(%rax)
.L55:
	movl	%r12d, %eax
.L52:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movzwl	432(%rdi), %ecx
	movl	408(%rdi), %edx
	testw	%cx, %cx
	js	.L57
	movswl	%cx, %esi
	sarl	$5, %esi
.L58:
	movl	$-1, %eax
	cmpl	%edx, %esi
	jbe	.L52
	andl	$2, %ecx
	leaq	434(%rbx), %rcx
	je	.L64
.L60:
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %eax
	movl	%eax, %ecx
	andl	$64512, %ecx
	cmpl	$56320, %ecx
	jne	.L52
	addl	$1, %edx
	movl	%edx, 408(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movl	436(%rdi), %esi
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L64:
	movq	448(%rbx), %rcx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L63:
	cmpl	$2, 400(%rbx)
	jne	.L55
	addl	$1, 408(%rbx)
	jmp	.L55
	.cfi_endproc
.LFE3276:
	.size	_ZN6icu_6725FCDUIterCollationIterator23handleGetTrailSurrogateEv, .-_ZN6icu_6725FCDUIterCollationIterator23handleGetTrailSurrogateEv
	.align 2
	.p2align 4
	.type	_ZN6icu_6725FCDUIterCollationIterator15previousSegmentER10UErrorCode.part.0, @function
_ZN6icu_6725FCDUIterCollationIterator15previousSegmentER10UErrorCode.part.0:
.LFB4315:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rsi, -144(%rbp)
	movl	$1, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	392(%rdi), %rax
	movq	%rax, %rdi
	call	*32(%rax)
	movl	$2, %edx
	movl	%eax, 408(%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movw	%dx, -120(%rbp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L115:
	cmpl	$65535, %r12d
	jg	.L68
	movl	%r12d, %eax
	movq	56(%rdi), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L95
	movl	%r12d, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L95
.L68:
	movl	%r12d, %esi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	movl	%eax, %r15d
	testb	%al, %al
	je	.L67
	movl	%r12d, %esi
	movq	%r14, %rdi
	movb	%al, -129(%rbp)
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	testb	%r13b, %r13b
	je	.L91
	movzbl	-129(%rbp), %ecx
	cmpb	%r13b, %cl
	ja	.L89
.L91:
	leal	32382(%r15), %eax
	testw	$-3, %ax
	je	.L89
	movl	%r15d, %eax
	movzbl	%ah, %eax
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L71
.L75:
	movq	392(%rbx), %rdi
	call	uiter_previous32_67@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L71
	movq	416(%rbx), %rdi
	movzwl	8(%rdi), %eax
	cmpl	%eax, %r12d
	jge	.L115
.L95:
	xorl	%r15d, %r15d
.L67:
	movswl	-120(%rbp), %eax
	shrl	$5, %eax
	jne	.L116
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	%r15d, %eax
	movzbl	%ah, %eax
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L75
.L71:
	movswl	-120(%rbp), %esi
	movl	408(%rbx), %eax
	testw	%si, %si
	js	.L87
.L118:
	sarl	$5, %esi
.L88:
	subl	%esi, %eax
	movl	$1, %edx
	movl	$1, %r12d
	movl	%eax, 404(%rbx)
	movq	392(%rbx), %rax
	movq	%rax, %rdi
	call	*40(%rax)
	movl	$2, 400(%rbx)
.L82:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movl	-116(%rbp), %esi
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L89:
	cmpw	$255, %r15w
	jbe	.L77
.L74:
	movq	392(%rbx), %rdi
	call	uiter_previous32_67@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L77
	movq	416(%rbx), %rdi
	movzwl	8(%rdi), %eax
	cmpl	%eax, %r12d
	jl	.L79
	cmpl	$65535, %r12d
	jg	.L78
	movl	%r12d, %eax
	movq	56(%rdi), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L79
	movl	%r12d, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L79
.L78:
	movl	%r12d, %esi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	movl	%eax, %r13d
	testw	%ax, %ax
	je	.L79
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpw	$255, %r13w
	ja	.L74
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L79:
	movq	392(%rbx), %rdi
	call	uiter_next32_67@PLT
.L77:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L80
	movswl	%ax, %edx
	sarl	$5, %edx
.L81:
	xorl	%esi, %esi
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6713UnicodeString9doReverseEii@PLT
	movq	-144(%rbp), %r15
	movq	416(%rbx), %rdi
	movq	%r14, %rsi
	leaq	424(%rbx), %rdx
	movq	%r15, %rcx
	call	_ZNK6icu_6715Normalizer2Impl9decomposeERKNS_13UnicodeStringERS1_R10UErrorCode@PLT
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L82
	movl	408(%rbx), %edx
	movswl	-120(%rbp), %eax
	movl	%edx, 412(%rbx)
	testw	%ax, %ax
	js	.L83
	sarl	$5, %eax
.L84:
	movl	$4, 400(%rbx)
	subl	%eax, %edx
	movswl	432(%rbx), %eax
	movl	%edx, 404(%rbx)
	testw	%ax, %ax
	js	.L85
	sarl	$5, %eax
.L86:
	movl	%eax, 408(%rbx)
	movl	$1, %r12d
	jmp	.L82
.L80:
	movl	-116(%rbp), %edx
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L116:
	movq	392(%rbx), %rdi
	call	uiter_next32_67@PLT
	movswl	-120(%rbp), %esi
	movl	408(%rbx), %eax
	testw	%si, %si
	jns	.L118
	jmp	.L87
.L83:
	movl	-116(%rbp), %eax
	jmp	.L84
.L85:
	movl	436(%rbx), %eax
	jmp	.L86
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4315:
	.size	_ZN6icu_6725FCDUIterCollationIterator15previousSegmentER10UErrorCode.part.0, .-_ZN6icu_6725FCDUIterCollationIterator15previousSegmentER10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6725FCDUIterCollationIterator11nextSegmentER10UErrorCode.part.0, @function
_ZN6icu_6725FCDUIterCollationIterator11nextSegmentER10UErrorCode.part.0:
.LFB4314:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rsi, -144(%rbp)
	movl	$1, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	392(%rdi), %rax
	movq	%rax, %rdi
	call	*32(%rax)
	movl	$2, %edx
	movl	%eax, 408(%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movw	%dx, -120(%rbp)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L160:
	cmpl	$65535, %r15d
	jg	.L122
	movl	%r15d, %eax
	movq	56(%rdi), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L143
	movl	%r15d, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L143
.L122:
	movl	%r15d, %esi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	movl	%eax, %r14d
	movzbl	%ah, %eax
	movl	%eax, -132(%rbp)
	testw	$-256, %r14w
	je	.L121
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpb	%r12b, -132(%rbp)
	jb	.L132
	leal	32382(%r14), %eax
	testw	$-3, %ax
	je	.L132
	movl	%r14d, %r12d
	testb	%r14b, %r14b
	je	.L125
.L127:
	movq	392(%rbx), %rdi
	call	uiter_next32_67@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	js	.L125
	movq	416(%rbx), %rdi
	movzwl	8(%rdi), %eax
	cmpl	%eax, %r15d
	jge	.L160
.L143:
	xorl	%r14d, %r14d
.L121:
	movswl	-120(%rbp), %eax
	shrl	$5, %eax
	jne	.L161
	movl	%r15d, %esi
	movq	%r13, %rdi
	movl	%r14d, %r12d
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	testb	%r14b, %r14b
	jne	.L127
.L125:
	movswl	-120(%rbp), %esi
	movl	408(%rbx), %eax
	testw	%si, %si
	js	.L136
.L163:
	sarl	$5, %esi
.L137:
	addl	%esi, %eax
	movl	$1, %edx
	negl	%esi
	movl	$1, %r12d
	movl	%eax, 412(%rbx)
	movq	392(%rbx), %rax
	movq	%rax, %rdi
	call	*40(%rax)
	movl	$2, 400(%rbx)
.L133:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L162
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	cmpl	$65535, %r12d
	jg	.L130
	movl	%r12d, %eax
	movq	56(%rdi), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L131
	movl	%r12d, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L131
.L130:
	movl	%r12d, %esi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	cmpw	$255, %ax
	jbe	.L131
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
.L132:
	movq	392(%rbx), %rdi
	call	uiter_next32_67@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L128
	movq	416(%rbx), %rdi
	movzwl	8(%rdi), %eax
	cmpl	%eax, %r12d
	jge	.L129
.L131:
	movq	392(%rbx), %rdi
	call	uiter_previous32_67@PLT
.L128:
	movq	-144(%rbp), %r14
	movq	416(%rbx), %rdi
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	leaq	424(%rbx), %rdx
	movq	%r14, %rcx
	call	_ZNK6icu_6715Normalizer2Impl9decomposeERKNS_13UnicodeStringERS1_R10UErrorCode@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L133
	movl	408(%rbx), %edx
	movswl	-120(%rbp), %eax
	movl	%edx, 404(%rbx)
	testw	%ax, %ax
	js	.L134
	sarl	$5, %eax
.L135:
	addl	%edx, %eax
	movl	$1, %r12d
	movl	$3, 400(%rbx)
	movl	%eax, 412(%rbx)
	movl	$0, 408(%rbx)
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L136:
	movl	-116(%rbp), %esi
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L161:
	movq	392(%rbx), %rdi
	call	uiter_previous32_67@PLT
	movswl	-120(%rbp), %esi
	movl	408(%rbx), %eax
	testw	%si, %si
	jns	.L163
	jmp	.L136
.L134:
	movl	-116(%rbp), %eax
	jmp	.L135
.L162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4314:
	.size	_ZN6icu_6725FCDUIterCollationIterator11nextSegmentER10UErrorCode.part.0, .-_ZN6icu_6725FCDUIterCollationIterator11nextSegmentER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUIterCollationIterator17previousCodePointER10UErrorCode
	.type	_ZN6icu_6725FCDUIterCollationIterator17previousCodePointER10UErrorCode, @function
_ZN6icu_6725FCDUIterCollationIterator17previousCodePointER10UErrorCode:
.LFB3278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZN6icu_6712CollationFCD9tcccIndexE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	400(%rdi), %eax
.L165:
	cmpl	$1, %eax
	je	.L189
	cmpl	$2, %eax
	je	.L190
	jle	.L184
	movl	408(%r15), %edx
	testl	%edx, %edx
	jne	.L221
	movl	404(%r15), %esi
	cmpl	$3, %eax
	je	.L222
.L191:
	movl	%esi, 412(%r15)
.L182:
	movl	$1, 400(%r15)
.L189:
	movq	392(%r15), %rax
	movq	%rax, %rdi
	call	*80(%rax)
	movl	%eax, %r8d
	testl	%eax, %eax
	js	.L223
	cmpl	$767, %eax
	jle	.L164
	sarl	$5, %eax
	cltq
	movzbl	(%r12,%rax), %eax
	testl	%eax, %eax
	je	.L164
	movl	0(%r13,%rax,4), %eax
	btl	%r8d, %eax
	jnc	.L164
	movq	392(%r15), %rax
	movl	%r8d, %edx
	movl	%r8d, -52(%rbp)
	andl	$2096897, %edx
	movq	%rax, %rdi
	cmpl	$3841, %edx
	je	.L217
	call	*80(%rax)
	movl	-52(%rbp), %r8d
	cmpl	$191, %eax
	jg	.L224
	movl	%r8d, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L175
	testl	%eax, %eax
	jns	.L180
	movq	392(%r15), %rax
	movl	%r8d, -52(%rbp)
	movq	%rax, %rdi
	call	*80(%rax)
	movl	-52(%rbp), %r8d
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L192
.L175:
	testl	%eax, %eax
	js	.L164
	.p2align 4,,10
	.p2align 3
.L193:
	movq	392(%r15), %rax
	movl	%r8d, -52(%rbp)
	movq	%rax, %rdi
	call	*72(%rax)
	movl	-52(%rbp), %r8d
.L164:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L219
	movq	392(%r15), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	*32(%rax)
	movl	%eax, 408(%r15)
	movl	%eax, 412(%r15)
	cmpl	404(%r15), %eax
	je	.L182
	movl	$2, 400(%r15)
.L190:
	movl	404(%r15), %eax
	cmpl	%eax, 408(%r15)
	je	.L182
	movq	392(%r15), %rdi
	call	uiter_previous32_67@PLT
.L220:
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$65535, %r8d
	seta	%al
	addl	$1, %eax
	subl	%eax, 408(%r15)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L222:
	movq	392(%r15), %rax
	subl	412(%r15), %esi
	movl	$1, %edx
	movq	%rax, %rdi
	call	*40(%rax)
.L219:
	movl	404(%r15), %esi
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L224:
	movl	%eax, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %edx
	testl	%edx, %edx
	je	.L171
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %rcx
	movl	(%rcx,%rdx,4), %edx
	btl	%eax, %edx
	jnc	.L171
	movq	392(%r15), %rax
	movq	%rax, %rdi
	call	*72(%rax)
	movq	392(%r15), %rax
	movq	%rax, %rdi
.L217:
	call	*72(%rax)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L178
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6725FCDUIterCollationIterator15previousSegmentER10UErrorCode.part.0
	testb	%al, %al
	je	.L178
	movl	400(%r15), %eax
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L178:
	movl	$-1, %r8d
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L171:
	movl	%r8d, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L193
.L180:
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L193
.L192:
	sall	$10, %eax
	leal	-56613888(%r8,%rax), %r8d
	jmp	.L164
.L221:
	leal	-1(%rdx), %esi
	leaq	424(%r15), %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	jmp	.L220
.L223:
	movq	$2, 400(%r15)
	movl	$-1, %r8d
	movl	$0, 408(%r15)
	jmp	.L164
	.cfi_endproc
.LFE3278:
	.size	_ZN6icu_6725FCDUIterCollationIterator17previousCodePointER10UErrorCode, .-_ZN6icu_6725FCDUIterCollationIterator17previousCodePointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUIterCollationIterator14handleNextCE32ERiR10UErrorCode
	.type	_ZN6icu_6725FCDUIterCollationIterator14handleNextCE32ERiR10UErrorCode, @function
_ZN6icu_6725FCDUIterCollationIterator14handleNextCE32ERiR10UErrorCode:
.LFB3275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN6icu_6712CollationFCD9tcccIndexE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	400(%rdi), %eax
.L226:
	testl	%eax, %eax
	jne	.L227
.L248:
	movq	392(%r15), %rax
	movq	%rax, %rdi
	call	*72(%rax)
	movl	%eax, (%rbx)
	testl	%eax, %eax
	js	.L254
	movl	%eax, %edx
	movl	%eax, %esi
	sarl	$5, %edx
	andl	$31, %esi
	movslq	%edx, %rdx
	cmpl	$191, %eax
	jg	.L229
.L273:
	addq	%rdx, %rdx
.L230:
	movq	8(%r15), %rcx
	movq	(%rcx), %rax
	movzwl	(%rax,%rdx), %eax
	movq	16(%rcx), %rdx
	leal	(%rsi,%rax,4), %eax
	cltq
	movl	(%rdx,%rax,4), %eax
.L225:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movzbl	0(%r13,%rdx), %edi
	testl	%edi, %edi
	je	.L273
	movl	(%r14,%rdi,4), %edi
	btl	%esi, %edi
	jnc	.L273
	andl	$2096897, %eax
	cmpl	$3841, %eax
	je	.L233
	movq	392(%r15), %rax
	movq	%rax, %rdi
	call	*64(%rax)
	cmpl	$767, %eax
	jle	.L235
	movl	%eax, %edx
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %rcx
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	testl	%edx, %edx
	je	.L235
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %rcx
	movl	(%rcx,%rdx,4), %edx
	btl	%eax, %edx
	jnc	.L235
	.p2align 4,,10
	.p2align 3
.L233:
	movq	392(%r15), %rax
	movq	%rax, %rdi
	call	*80(%rax)
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L237
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6725FCDUIterCollationIterator11nextSegmentER10UErrorCode.part.0
	testb	%al, %al
	je	.L237
	movl	400(%r15), %eax
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L227:
	cmpl	$2, %eax
	je	.L249
	jle	.L241
	movzwl	432(%r15), %esi
	movslq	408(%r15), %rdi
	testw	%si, %si
	js	.L242
	movswl	%si, %edx
	sarl	$5, %edx
.L243:
	cmpl	%edx, %edi
	jne	.L274
	movl	412(%r15), %esi
	cmpl	$4, %eax
	je	.L275
.L250:
	movl	%esi, 404(%r15)
.L240:
	movl	$0, 400(%r15)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L241:
	cmpl	$1, %eax
	jne	.L271
	movq	392(%r15), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	*32(%rax)
	movl	%eax, 408(%r15)
	movl	%eax, 404(%r15)
	cmpl	412(%r15), %eax
	je	.L240
	movl	$2, 400(%r15)
.L249:
	movl	412(%r15), %eax
	cmpl	%eax, 408(%r15)
	je	.L240
	movq	392(%r15), %rax
	movq	%rax, %rdi
	call	*72(%rax)
	movl	%eax, (%rbx)
	addl	$1, 408(%r15)
	movl	(%rbx), %eax
	movl	%eax, %esi
	sarl	$5, %eax
	andl	$31, %esi
	movslq	%eax, %rdx
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L242:
	movl	436(%r15), %edx
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L235:
	movl	(%rbx), %edx
	movl	%edx, %esi
	sarl	$5, %edx
	andl	$31, %esi
	movslq	%edx, %rdx
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L275:
	movq	392(%r15), %rax
	subl	404(%r15), %esi
	movl	$1, %edx
	movq	%rax, %rdi
	call	*40(%rax)
.L271:
	movl	412(%r15), %esi
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L237:
	movl	$-1, (%rbx)
	movl	$192, %eax
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L254:
	movl	$192, %eax
	jmp	.L225
.L274:
	leal	1(%rdi), %eax
	movl	%eax, 408(%r15)
	cmpl	%edi, %edx
	jbe	.L255
	andl	$2, %esi
	leaq	434(%r15), %rax
	jne	.L253
	movq	448(%r15), %rax
.L253:
	movzwl	(%rax,%rdi,2), %eax
	movl	%eax, %edx
	movl	%eax, %esi
	movl	%eax, (%rbx)
	sarl	$5, %edx
	andl	$31, %esi
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	jmp	.L230
.L255:
	movl	$65535, %eax
	movl	$31, %esi
	movl	$4094, %edx
	movl	%eax, (%rbx)
	jmp	.L230
	.cfi_endproc
.LFE3275:
	.size	_ZN6icu_6725FCDUIterCollationIterator14handleNextCE32ERiR10UErrorCode, .-_ZN6icu_6725FCDUIterCollationIterator14handleNextCE32ERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUIterCollationIterator13nextCodePointER10UErrorCode
	.type	_ZN6icu_6725FCDUIterCollationIterator13nextCodePointER10UErrorCode, @function
_ZN6icu_6725FCDUIterCollationIterator13nextCodePointER10UErrorCode:
.LFB3277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	_ZN6icu_6712CollationFCD9tcccIndexE(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	400(%rdi), %edx
.L277:
	testl	%edx, %edx
	jne	.L278
.L296:
	movq	392(%r14), %rax
	movq	%rax, %rdi
	call	*72(%rax)
	movl	%eax, %r15d
	testl	%eax, %eax
	js	.L276
	cmpl	$191, %eax
	jle	.L276
	sarl	$5, %eax
	cltq
	movzbl	(%r12,%rax), %eax
	testl	%eax, %eax
	je	.L280
	movl	0(%r13,%rax,4), %eax
	btl	%r15d, %eax
	jc	.L325
.L280:
	movl	%r15d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L276
	movq	392(%r14), %rax
	movq	%rax, %rdi
	call	*72(%rax)
	movl	%eax, %edx
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L326
	testl	%edx, %edx
	js	.L276
	movq	392(%r14), %rax
	movq	%rax, %rdi
	call	*80(%rax)
.L276:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	cmpl	$2, %edx
	je	.L297
	jle	.L289
	movswl	432(%r14), %eax
	movl	408(%r14), %esi
	testw	%ax, %ax
	js	.L290
	sarl	$5, %eax
.L291:
	cmpl	%eax, %esi
	jne	.L327
	movl	412(%r14), %esi
	cmpl	$4, %edx
	je	.L328
.L298:
	movl	%esi, 404(%r14)
.L287:
	movl	$0, 400(%r14)
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L289:
	cmpl	$1, %edx
	jne	.L323
	movq	392(%r14), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	*32(%rax)
	movl	%eax, 408(%r14)
	movl	%eax, 404(%r14)
	cmpl	412(%r14), %eax
	je	.L287
	movl	$2, 400(%r14)
.L297:
	movl	412(%r14), %eax
	cmpl	%eax, 408(%r14)
	je	.L287
	movq	392(%r14), %rdi
	call	uiter_next32_67@PLT
.L324:
	movl	%eax, %r15d
	xorl	%eax, %eax
	cmpl	$65535, %r15d
	seta	%al
	addl	$1, %eax
	addl	%eax, 408(%r14)
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L290:
	movl	436(%r14), %eax
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L325:
	movl	%r15d, %edx
	movq	392(%r14), %rax
	andl	$2096897, %edx
	cmpl	$3841, %edx
	je	.L281
	movq	%rax, %rdi
	call	*64(%rax)
	cmpl	$767, %eax
	jle	.L280
	movl	%eax, %edx
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %rcx
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	testl	%edx, %edx
	je	.L280
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %rcx
	movl	(%rcx,%rdx,4), %edx
	btl	%eax, %edx
	jnc	.L280
	movq	392(%r14), %rax
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%rax, %rdi
	call	*80(%rax)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L283
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6725FCDUIterCollationIterator11nextSegmentER10UErrorCode.part.0
	testb	%al, %al
	je	.L283
	movl	400(%r14), %edx
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L326:
	movl	%r15d, %r8d
	sall	$10, %r8d
	leal	-56613888(%rdx,%r8), %r15d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L328:
	movq	392(%r14), %rax
	subl	404(%r14), %esi
	movl	$1, %edx
	movq	%rax, %rdi
	call	*40(%rax)
.L323:
	movl	412(%r14), %esi
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$-1, %r15d
	jmp	.L276
.L327:
	leaq	424(%r14), %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	jmp	.L324
	.cfi_endproc
.LFE3277:
	.size	_ZN6icu_6725FCDUIterCollationIterator13nextCodePointER10UErrorCode, .-_ZN6icu_6725FCDUIterCollationIterator13nextCodePointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUIterCollationIterator20forwardNumCodePointsEiR10UErrorCode
	.type	_ZN6icu_6725FCDUIterCollationIterator20forwardNumCodePointsEiR10UErrorCode, @function
_ZN6icu_6725FCDUIterCollationIterator20forwardNumCodePointsEiR10UErrorCode:
.LFB3279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	testl	%esi, %esi
	jle	.L329
	movq	%rdi, %r14
	movl	%esi, %ebx
	leaq	_ZN6icu_6712CollationFCD9tcccIndexE(%rip), %r13
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L330:
	movl	400(%r14), %edx
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %r12
.L331:
	testl	%edx, %edx
	jne	.L332
.L351:
	movq	392(%r14), %rax
	movq	%rax, %rdi
	call	*72(%rax)
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L329
	cmpl	$191, %eax
	jle	.L339
	sarl	$5, %eax
	cltq
	movzbl	0(%r13,%rax), %eax
	testl	%eax, %eax
	je	.L335
	movl	(%r15,%rax,4), %eax
	btl	%edx, %eax
	jnc	.L335
	movl	%edx, %esi
	movl	%edx, -52(%rbp)
	movq	392(%r14), %rax
	andl	$2096897, %esi
	cmpl	$3841, %esi
	je	.L336
	movq	%rax, %rdi
	call	*64(%rax)
	movl	-52(%rbp), %edx
	cmpl	$767, %eax
	jg	.L386
	.p2align 4,,10
	.p2align 3
.L335:
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L339
	movq	392(%r14), %rax
	movq	%rax, %rdi
	call	*72(%rax)
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L339
	testl	%eax, %eax
	js	.L339
	movq	392(%r14), %rax
	movq	%rax, %rdi
	call	*80(%rax)
	.p2align 4,,10
	.p2align 3
.L339:
	subl	$1, %ebx
	jne	.L330
.L329:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	cmpl	$2, %edx
	je	.L352
	jle	.L344
	movswl	432(%r14), %eax
	movl	408(%r14), %esi
	testw	%ax, %ax
	js	.L345
	sarl	$5, %eax
	cmpl	%eax, %esi
	jne	.L387
.L381:
	movl	412(%r14), %esi
	cmpl	$4, %edx
	je	.L388
.L353:
	movl	%esi, 404(%r14)
.L341:
	movl	$0, 400(%r14)
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L344:
	cmpl	$1, %edx
	jne	.L383
	movq	392(%r14), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	*32(%rax)
	movl	%eax, 408(%r14)
	movl	%eax, 404(%r14)
	cmpl	412(%r14), %eax
	je	.L341
	movl	$2, 400(%r14)
.L352:
	movl	412(%r14), %eax
	cmpl	%eax, 408(%r14)
	je	.L341
	movq	392(%r14), %rdi
	call	uiter_next32_67@PLT
.L384:
	xorl	%edx, %edx
	cmpl	$65535, %eax
	seta	%dl
	addl	$1, %edx
	addl	%edx, 408(%r14)
	testl	%eax, %eax
	jns	.L339
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore_state
	movl	436(%r14), %eax
	cmpl	%eax, %esi
	je	.L381
.L387:
	leaq	424(%r14), %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L388:
	movq	392(%r14), %rax
	subl	404(%r14), %esi
	movl	$1, %edx
	movq	%rax, %rdi
	call	*40(%rax)
.L383:
	movl	412(%r14), %esi
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L386:
	movl	%eax, %esi
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %rcx
	sarl	$5, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	testl	%esi, %esi
	je	.L335
	movl	(%r12,%rsi,4), %esi
	btl	%eax, %esi
	jnc	.L335
	movq	392(%r14), %rax
	.p2align 4,,10
	.p2align 3
.L336:
	movq	%rax, %rdi
	call	*80(%rax)
	movq	-64(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L329
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6725FCDUIterCollationIterator11nextSegmentER10UErrorCode.part.0
	testb	%al, %al
	je	.L329
	movl	400(%r14), %edx
	jmp	.L331
	.cfi_endproc
.LFE3279:
	.size	_ZN6icu_6725FCDUIterCollationIterator20forwardNumCodePointsEiR10UErrorCode, .-_ZN6icu_6725FCDUIterCollationIterator20forwardNumCodePointsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUIterCollationIterator21backwardNumCodePointsEiR10UErrorCode
	.type	_ZN6icu_6725FCDUIterCollationIterator21backwardNumCodePointsEiR10UErrorCode, @function
_ZN6icu_6725FCDUIterCollationIterator21backwardNumCodePointsEiR10UErrorCode:
.LFB3280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	testl	%esi, %esi
	jle	.L389
	movq	%rdi, %r14
	movl	%esi, %ebx
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %r13
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L390:
	movl	400(%r14), %eax
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %r12
.L392:
	cmpl	$1, %eax
	je	.L418
	cmpl	$2, %eax
	je	.L419
	jle	.L413
	movl	408(%r14), %esi
	testl	%esi, %esi
	jne	.L454
	movl	404(%r14), %esi
	cmpl	$3, %eax
	je	.L455
.L420:
	movl	%esi, 412(%r14)
.L410:
	movl	$1, 400(%r14)
.L418:
	movq	392(%r14), %rax
	movq	%rax, %rdi
	call	*80(%rax)
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L456
	cmpl	$767, %eax
	jle	.L396
	sarl	$5, %eax
	cltq
	movzbl	0(%r13,%rax), %eax
	testl	%eax, %eax
	je	.L396
	movl	(%r15,%rax,4), %eax
	btl	%edx, %eax
	jnc	.L396
	movq	392(%r14), %rax
	movl	%edx, %esi
	movl	%edx, -52(%rbp)
	andl	$2096897, %esi
	movq	%rax, %rdi
	cmpl	$3841, %esi
	je	.L448
	call	*80(%rax)
	movl	-52(%rbp), %edx
	cmpl	$191, %eax
	jle	.L457
	movl	%eax, %esi
	leaq	_ZN6icu_6712CollationFCD9tcccIndexE(%rip), %rcx
	sarl	$5, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	testl	%esi, %esi
	je	.L450
	movl	(%r12,%rsi,4), %esi
	btl	%eax, %esi
	jnc	.L450
	movq	392(%r14), %rax
	movq	%rax, %rdi
	call	*72(%rax)
	movq	392(%r14), %rax
	movq	%rax, %rdi
.L448:
	call	*72(%rax)
	movq	-64(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L389
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6725FCDUIterCollationIterator15previousSegmentER10UErrorCode.part.0
	testb	%al, %al
	je	.L389
	movl	400(%r14), %eax
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L450:
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L404
.L403:
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L396
.L404:
	movq	392(%r14), %rax
	movq	%rax, %rdi
	call	*72(%rax)
	.p2align 4,,10
	.p2align 3
.L396:
	subl	$1, %ebx
	jne	.L390
.L389:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L451
	movq	392(%r14), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	*32(%rax)
	movl	%eax, 408(%r14)
	movl	%eax, 412(%r14)
	cmpl	404(%r14), %eax
	je	.L410
	movl	$2, 400(%r14)
.L419:
	movl	404(%r14), %eax
	cmpl	%eax, 408(%r14)
	je	.L410
	movq	392(%r14), %rdi
	call	uiter_previous32_67@PLT
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L455:
	movq	392(%r14), %rax
	subl	412(%r14), %esi
	movl	$1, %edx
	movq	%rax, %rdi
	call	*40(%rax)
.L451:
	movl	404(%r14), %esi
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L457:
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L401
	testl	%eax, %eax
	jns	.L403
	movq	392(%r14), %rax
	movq	%rax, %rdi
	call	*80(%rax)
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L396
.L401:
	testl	%eax, %eax
	jns	.L404
	subl	$1, %ebx
	jne	.L390
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L454:
	subl	$1, %esi
	leaq	424(%r14), %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
.L452:
	xorl	%edx, %edx
	cmpl	$65535, %eax
	seta	%dl
	addl	$1, %edx
	subl	%edx, 408(%r14)
	testl	%eax, %eax
	js	.L389
	subl	$1, %ebx
	jne	.L390
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L456:
	movq	$2, 400(%r14)
	movl	$0, 408(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3280:
	.size	_ZN6icu_6725FCDUIterCollationIterator21backwardNumCodePointsEiR10UErrorCode, .-_ZN6icu_6725FCDUIterCollationIterator21backwardNumCodePointsEiR10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3522:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3522:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3525:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L471
	ret
	.p2align 4,,10
	.p2align 3
.L471:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L459
	cmpb	$0, 12(%rbx)
	jne	.L472
.L463:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L459:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L463
	.cfi_endproc
.LFE3525:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3528:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L475
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3528:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3531:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L478
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3531:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L484
.L480:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L485
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L485:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3533:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3534:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3534:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3535:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3535:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3536:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3536:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3537:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3537:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3538:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3538:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3539:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L501
	testl	%edx, %edx
	jle	.L501
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L504
.L493:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L493
	.cfi_endproc
.LFE3539:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L508
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L508
	testl	%r12d, %r12d
	jg	.L515
	cmpb	$0, 12(%rbx)
	jne	.L516
.L510:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L510
.L516:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L508:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3540:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3541:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L518
	movq	(%rdi), %r8
.L519:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L522
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L522
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L522:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3541:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3542:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L529
	ret
	.p2align 4,,10
	.p2align 3
.L529:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3542:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3543:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3543:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3544:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3544:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3545:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3545:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3547:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3547:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3549:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3549:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUIterCollationIterator15switchToForwardEv
	.type	_ZN6icu_6725FCDUIterCollationIterator15switchToForwardEv, @function
_ZN6icu_6725FCDUIterCollationIterator15switchToForwardEv:
.LFB3281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	400(%rdi), %eax
	cmpl	$1, %eax
	je	.L545
	cmpl	$2, %eax
	je	.L539
	movl	412(%rdi), %esi
	cmpl	$4, %eax
	jne	.L540
	movq	392(%rdi), %rax
	subl	404(%rdi), %esi
	movl	$1, %edx
	movq	%rax, %rdi
	call	*40(%rax)
	movl	412(%rbx), %esi
.L540:
	movl	%esi, 404(%rbx)
.L539:
	movl	$0, 400(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	.cfi_restore_state
	movq	392(%rdi), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	*32(%rax)
	movl	%eax, 408(%rbx)
	movl	%eax, 404(%rbx)
	cmpl	412(%rbx), %eax
	je	.L539
	movl	$2, 400(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3281:
	.size	_ZN6icu_6725FCDUIterCollationIterator15switchToForwardEv, .-_ZN6icu_6725FCDUIterCollationIterator15switchToForwardEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUIterCollationIterator11nextSegmentER10UErrorCode
	.type	_ZN6icu_6725FCDUIterCollationIterator11nextSegmentER10UErrorCode, @function
_ZN6icu_6725FCDUIterCollationIterator11nextSegmentER10UErrorCode:
.LFB3282:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L547
	jmp	_ZN6icu_6725FCDUIterCollationIterator11nextSegmentER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L547:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3282:
	.size	_ZN6icu_6725FCDUIterCollationIterator11nextSegmentER10UErrorCode, .-_ZN6icu_6725FCDUIterCollationIterator11nextSegmentER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUIterCollationIterator16switchToBackwardEv
	.type	_ZN6icu_6725FCDUIterCollationIterator16switchToBackwardEv, @function
_ZN6icu_6725FCDUIterCollationIterator16switchToBackwardEv:
.LFB3283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	400(%rdi), %eax
	testl	%eax, %eax
	jne	.L549
	movq	392(%rdi), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	*32(%rax)
	movl	%eax, 408(%rbx)
	movl	%eax, 412(%rbx)
	cmpl	404(%rbx), %eax
	je	.L552
	movl	$2, 400(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L549:
	.cfi_restore_state
	cmpl	$2, %eax
	je	.L552
	movl	404(%rdi), %esi
	cmpl	$3, %eax
	jne	.L553
	movq	392(%rdi), %rax
	subl	412(%rdi), %esi
	movl	$1, %edx
	movq	%rax, %rdi
	call	*40(%rax)
	movl	404(%rbx), %esi
.L553:
	movl	%esi, 412(%rbx)
.L552:
	movl	$1, 400(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3283:
	.size	_ZN6icu_6725FCDUIterCollationIterator16switchToBackwardEv, .-_ZN6icu_6725FCDUIterCollationIterator16switchToBackwardEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUIterCollationIterator15previousSegmentER10UErrorCode
	.type	_ZN6icu_6725FCDUIterCollationIterator15previousSegmentER10UErrorCode, @function
_ZN6icu_6725FCDUIterCollationIterator15previousSegmentER10UErrorCode:
.LFB3284:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L559
	jmp	_ZN6icu_6725FCDUIterCollationIterator15previousSegmentER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L559:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3284:
	.size	_ZN6icu_6725FCDUIterCollationIterator15previousSegmentER10UErrorCode, .-_ZN6icu_6725FCDUIterCollationIterator15previousSegmentER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUIterCollationIterator9normalizeERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6725FCDUIterCollationIterator9normalizeERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6725FCDUIterCollationIterator9normalizeERKNS_13UnicodeStringER10UErrorCode:
.LFB3285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	leaq	424(%rdi), %rdx
	movq	%rbx, %rcx
	subq	$8, %rsp
	movq	416(%rdi), %rdi
	call	_ZNK6icu_6715Normalizer2Impl9decomposeERKNS_13UnicodeStringERS1_R10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	setle	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3285:
	.size	_ZN6icu_6725FCDUIterCollationIterator9normalizeERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6725FCDUIterCollationIterator9normalizeERKNS_13UnicodeStringER10UErrorCode
	.weak	_ZTSN6icu_6722UIterCollationIteratorE
	.section	.rodata._ZTSN6icu_6722UIterCollationIteratorE,"aG",@progbits,_ZTSN6icu_6722UIterCollationIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6722UIterCollationIteratorE, @object
	.size	_ZTSN6icu_6722UIterCollationIteratorE, 34
_ZTSN6icu_6722UIterCollationIteratorE:
	.string	"N6icu_6722UIterCollationIteratorE"
	.weak	_ZTIN6icu_6722UIterCollationIteratorE
	.section	.data.rel.ro._ZTIN6icu_6722UIterCollationIteratorE,"awG",@progbits,_ZTIN6icu_6722UIterCollationIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6722UIterCollationIteratorE, @object
	.size	_ZTIN6icu_6722UIterCollationIteratorE, 24
_ZTIN6icu_6722UIterCollationIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722UIterCollationIteratorE
	.quad	_ZTIN6icu_6717CollationIteratorE
	.weak	_ZTSN6icu_6725FCDUIterCollationIteratorE
	.section	.rodata._ZTSN6icu_6725FCDUIterCollationIteratorE,"aG",@progbits,_ZTSN6icu_6725FCDUIterCollationIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6725FCDUIterCollationIteratorE, @object
	.size	_ZTSN6icu_6725FCDUIterCollationIteratorE, 37
_ZTSN6icu_6725FCDUIterCollationIteratorE:
	.string	"N6icu_6725FCDUIterCollationIteratorE"
	.weak	_ZTIN6icu_6725FCDUIterCollationIteratorE
	.section	.data.rel.ro._ZTIN6icu_6725FCDUIterCollationIteratorE,"awG",@progbits,_ZTIN6icu_6725FCDUIterCollationIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6725FCDUIterCollationIteratorE, @object
	.size	_ZTIN6icu_6725FCDUIterCollationIteratorE, 24
_ZTIN6icu_6725FCDUIterCollationIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725FCDUIterCollationIteratorE
	.quad	_ZTIN6icu_6722UIterCollationIteratorE
	.weak	_ZTVN6icu_6722UIterCollationIteratorE
	.section	.data.rel.ro._ZTVN6icu_6722UIterCollationIteratorE,"awG",@progbits,_ZTVN6icu_6722UIterCollationIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6722UIterCollationIteratorE, @object
	.size	_ZTVN6icu_6722UIterCollationIteratorE, 144
_ZTVN6icu_6722UIterCollationIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6722UIterCollationIteratorE
	.quad	_ZN6icu_6722UIterCollationIteratorD1Ev
	.quad	_ZN6icu_6722UIterCollationIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717CollationIteratoreqERKS0_
	.quad	_ZN6icu_6722UIterCollationIterator13resetToOffsetEi
	.quad	_ZNK6icu_6722UIterCollationIterator9getOffsetEv
	.quad	_ZN6icu_6722UIterCollationIterator13nextCodePointER10UErrorCode
	.quad	_ZN6icu_6722UIterCollationIterator17previousCodePointER10UErrorCode
	.quad	_ZN6icu_6722UIterCollationIterator14handleNextCE32ERiR10UErrorCode
	.quad	_ZN6icu_6722UIterCollationIterator23handleGetTrailSurrogateEv
	.quad	_ZN6icu_6717CollationIterator18foundNULTerminatorEv
	.quad	_ZNK6icu_6717CollationIterator25forbidSurrogateCodePointsEv
	.quad	_ZN6icu_6722UIterCollationIterator20forwardNumCodePointsEiR10UErrorCode
	.quad	_ZN6icu_6722UIterCollationIterator21backwardNumCodePointsEiR10UErrorCode
	.quad	_ZNK6icu_6717CollationIterator11getDataCE32Ei
	.quad	_ZN6icu_6717CollationIterator22getCE32FromBuilderDataEjR10UErrorCode
	.weak	_ZTVN6icu_6725FCDUIterCollationIteratorE
	.section	.data.rel.ro._ZTVN6icu_6725FCDUIterCollationIteratorE,"awG",@progbits,_ZTVN6icu_6725FCDUIterCollationIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6725FCDUIterCollationIteratorE, @object
	.size	_ZTVN6icu_6725FCDUIterCollationIteratorE, 144
_ZTVN6icu_6725FCDUIterCollationIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6725FCDUIterCollationIteratorE
	.quad	_ZN6icu_6725FCDUIterCollationIteratorD1Ev
	.quad	_ZN6icu_6725FCDUIterCollationIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717CollationIteratoreqERKS0_
	.quad	_ZN6icu_6725FCDUIterCollationIterator13resetToOffsetEi
	.quad	_ZNK6icu_6725FCDUIterCollationIterator9getOffsetEv
	.quad	_ZN6icu_6725FCDUIterCollationIterator13nextCodePointER10UErrorCode
	.quad	_ZN6icu_6725FCDUIterCollationIterator17previousCodePointER10UErrorCode
	.quad	_ZN6icu_6725FCDUIterCollationIterator14handleNextCE32ERiR10UErrorCode
	.quad	_ZN6icu_6725FCDUIterCollationIterator23handleGetTrailSurrogateEv
	.quad	_ZN6icu_6717CollationIterator18foundNULTerminatorEv
	.quad	_ZNK6icu_6717CollationIterator25forbidSurrogateCodePointsEv
	.quad	_ZN6icu_6725FCDUIterCollationIterator20forwardNumCodePointsEiR10UErrorCode
	.quad	_ZN6icu_6725FCDUIterCollationIterator21backwardNumCodePointsEiR10UErrorCode
	.quad	_ZNK6icu_6717CollationIterator11getDataCE32Ei
	.quad	_ZN6icu_6717CollationIterator22getCE32FromBuilderDataEjR10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
