	.file	"translit.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator5cloneEv
	.type	_ZNK6icu_6714Transliterator5cloneEv, @function
_ZNK6icu_6714Transliterator5cloneEv:
.LFB3397:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3397:
	.size	_ZNK6icu_6714Transliterator5cloneEv, .-_ZNK6icu_6714Transliterator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.type	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii, @function
_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii:
.LFB3399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L7
	movl	%edx, %ebx
	movl	%ecx, %r12d
	cmpl	%ecx, %edx
	jg	.L7
	movq	(%rsi), %rax
	movq	%rdi, %r14
	movq	%rsi, %r13
	movq	%rsi, %rdi
	call	*64(%rax)
	cmpl	%eax, %r12d
	jg	.L7
	movd	%r12d, %xmm1
	movd	%ebx, %xmm0
	movq	(%r14), %rax
	leaq	-64(%rbp), %rdx
	punpckldq	%xmm1, %xmm0
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, %rdi
	movaps	%xmm0, -64(%rbp)
	call	*96(%rax)
	movl	-52(%rbp), %eax
.L3:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L10
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L3
.L10:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3399:
	.size	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii, .-_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.type	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona, @function
_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona:
.LFB3407:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movsbl	%cl, %ecx
	xorl	%r8d, %r8d
	movq	96(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE3407:
	.size	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona, .-_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator5getIDEv
	.type	_ZNK6icu_6714Transliterator5getIDEv, @function
_ZNK6icu_6714Transliterator5getIDEv:
.LFB3409:
	.cfi_startproc
	endbr64
	leaq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE3409:
	.size	_ZNK6icu_6714Transliterator5getIDEv, .-_ZNK6icu_6714Transliterator5getIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.type	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa, @function
_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa:
.LFB3406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movb	%cl, -136(%rbp)
	movq	72(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	sete	%dl
	testb	%r8b, %r8b
	sete	%al
	testb	%al, %dl
	jne	.L56
	testb	%cl, %cl
	movl	12(%r15), %edx
	movq	%rdi, -144(%rbp)
	setne	-156(%rbp)
	testb	%r8b, %r8b
	movzbl	-156(%rbp), %ecx
	setne	%al
	movl	%edx, -132(%rbp)
	andl	%ecx, %eax
	movsbl	-136(%rbp), %ecx
	movb	%al, -152(%rbp)
	movl	%edx, %eax
	movl	%ecx, -160(%rbp)
	movq	%r15, %rcx
	movq	%r13, %r15
	movq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L37:
	movl	8(%r13), %esi
	testq	%r15, %r15
	jne	.L57
.L16:
	cmpl	%esi, %eax
	je	.L54
	cmpl	%eax, -132(%rbp)
	jle	.L39
	xorl	%ebx, %ebx
	movl	%eax, %r15d
	xorl	%ecx, %ecx
.L29:
	movq	-144(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*80(%rax)
	movl	12(%r13), %eax
	movl	%eax, %ecx
	subl	%r15d, %ecx
	cmpb	$0, -136(%rbp)
	jne	.L36
	cmpl	8(%r13), %eax
	je	.L36
	movl	%eax, 8(%r13)
.L36:
	movq	-144(%rbp), %rdx
	addl	%ecx, -132(%rbp)
	movq	72(%rdx), %r15
	testq	%r15, %r15
	je	.L54
	testb	%bl, %bl
	je	.L37
.L54:
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L23:
	movl	-132(%rbp), %eax
	movl	%eax, 12(%r15)
.L13:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movl	8(%r15), %esi
	movq	%r13, %r14
	movq	%r15, %r13
.L19:
	movl	%esi, 12(%r13)
	cmpl	%esi, -132(%rbp)
	jle	.L54
	.p2align 4,,10
	.p2align 3
.L22:
	movq	-144(%rbp), %rax
	movq	%r14, %rdi
	movq	72(%rax), %r15
	movq	(%r15), %rax
	movq	56(%rax), %r12
	movq	(%r14), %rax
	call	*80(%rax)
	movq	%r15, %rdi
	movl	%eax, %ebx
	movl	%eax, %esi
	call	*%r12
	testb	%al, %al
	je	.L59
	movl	12(%r13), %esi
	cmpl	$65535, %ebx
	ja	.L26
	addl	$1, %esi
	movl	%esi, 12(%r13)
	cmpl	%esi, -132(%rbp)
	jg	.L22
.L41:
	movl	%esi, %eax
	movl	8(%r13), %esi
	cmpl	%eax, %esi
	je	.L54
.L39:
	cmpb	$0, -152(%rbp)
	je	.L60
	movq	%r13, %r15
	movl	%eax, -136(%rbp)
	movl	%eax, %r13d
	subl	%esi, %eax
	movl	%eax, -176(%rbp)
	movq	(%r14), %rax
	movq	%r14, %rdi
	movl	%esi, %ebx
	movq	-144(%rbp), %r12
	call	*64(%rax)
	movq	%r14, %rdi
	movl	%ebx, %esi
	movl	%eax, %edx
	movq	(%r14), %rax
	movl	%edx, -172(%rbp)
	movl	%edx, %ecx
	movl	%r13d, %edx
	call	*40(%rax)
	movl	-172(%rbp), %eax
	movl	8(%r15), %r13d
	movl	$0, -168(%rbp)
	movl	$0, -144(%rbp)
	movl	%eax, -164(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	%r15, %rax
	movq	%r14, %r15
	movq	%r12, %r14
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L31:
	movq	(%r15), %rax
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	*80(%rax)
	cmpl	$65535, %eax
	seta	%al
	movzbl	%al, %eax
	addl	$1, %eax
	addl	%eax, %r13d
	cmpl	%r13d, -136(%rbp)
	jl	.L33
	addl	%eax, -144(%rbp)
	movq	(%r14), %rax
	movq	%r12, %rdx
	movq	%r15, %rsi
	movl	%r13d, 12(%r12)
	movl	$1, %ecx
	movq	%r14, %rdi
	call	*80(%rax)
	movl	12(%r12), %edx
	movl	%edx, %eax
	subl	%r13d, %eax
	cmpl	8(%r12), %edx
	je	.L34
	movl	-164(%rbp), %edi
	movq	(%r15), %rcx
	movl	%eax, -160(%rbp)
	movl	%ebx, %esi
	leal	(%rdi,%rax), %r10d
	movl	%edx, %edi
	movq	32(%rcx), %r11
	movl	$2, %ecx
	subl	%ebx, %edi
	movw	%cx, -120(%rbp)
	movq	-152(%rbp), %rcx
	subl	%edi, %r10d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdi
	movl	%r10d, -156(%rbp)
	movq	%rdi, -128(%rbp)
	movq	%r15, %rdi
	call	*%r11
	movq	-152(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-144(%rbp), %eax
	movq	(%r15), %r11
	movl	%ebx, %ecx
	movl	-156(%rbp), %r10d
	movq	%r15, %rdi
	leal	(%rax,%r10), %edx
	movl	%r10d, %esi
	call	*40(%r11)
	movl	-160(%rbp), %eax
	movl	%ebx, 8(%r12)
	subl	%eax, 4(%r12)
	movl	%r13d, 12(%r12)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L34:
	movl	-144(%rbp), %ecx
	addl	%eax, -136(%rbp)
	movl	%edx, %r13d
	movl	%edx, %ebx
	addl	%eax, -168(%rbp)
	movl	$0, -144(%rbp)
	addl	%eax, %ecx
	addl	%ecx, -164(%rbp)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L57:
	cmpl	%esi, -132(%rbp)
	jle	.L54
	movq	%r13, %rax
	movq	%r14, %r13
	movq	%r15, %r14
	movq	%rax, %r15
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L63:
	addl	$1, %esi
	movl	%esi, 8(%r15)
	cmpl	%esi, -132(%rbp)
	jle	.L61
.L21:
	movq	-144(%rbp), %rax
	movq	72(%rax), %r14
.L24:
	movq	(%r14), %rax
	movq	%r13, %rdi
	movq	56(%rax), %r12
	movq	0(%r13), %rax
	call	*80(%rax)
	movq	%r14, %rdi
	movl	%eax, %ebx
	movl	%eax, %esi
	call	*%r12
	testb	%al, %al
	jne	.L62
	movl	8(%r15), %esi
	cmpl	$65535, %ebx
	jbe	.L63
	addl	$2, %esi
	movl	%esi, 8(%r15)
	cmpl	-132(%rbp), %esi
	jl	.L21
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L33:
	movl	-168(%rbp), %eax
	movl	-172(%rbp), %esi
	movq	%r15, %r14
	movl	$2, %edx
	addl	%eax, -132(%rbp)
	movq	%r12, %r15
	movq	-152(%rbp), %r12
	movq	%r14, %rdi
	addl	%eax, %esi
	movq	(%r14), %rax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movq	32(%rax), %rax
	movw	%dx, -120(%rbp)
	movl	-176(%rbp), %edx
	movq	%rcx, -128(%rbp)
	movq	%r12, %rcx
	addl	%esi, %edx
	call	*%rax
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	%ebx, 8(%r15)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L26:
	addl	$2, %esi
	movl	%esi, 12(%r13)
	cmpl	-132(%rbp), %esi
	jl	.L22
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L59:
	movl	12(%r13), %eax
	movl	8(%r13), %esi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L60:
	movl	-160(%rbp), %ecx
	movzbl	-156(%rbp), %ebx
	movl	%eax, %r15d
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%r13, %r14
	movq	%r15, %r13
	jmp	.L19
.L56:
	movq	(%rdi), %rax
	movsbl	%cl, %ecx
	movq	%r15, %rdx
	call	*80(%rax)
	jmp	.L13
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3406:
	.size	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa, .-_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.p2align 4
	.globl	utrans_transliterator_cleanup_67
	.type	utrans_transliterator_cleanup_67, @function
utrans_transliterator_cleanup_67:
.LFB3452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_ZN6icu_6722TransliteratorIDParser7cleanupEv@PLT
	movq	_ZL8registry(%rip), %r12
	testq	%r12, %r12
	je	.L65
	movq	%r12, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	$0, _ZL8registry(%rip)
.L65:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3452:
	.size	utrans_transliterator_cleanup_67, .-utrans_transliterator_cleanup_67
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.type	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition, @function
_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition:
.LFB3404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	call	*64(%rax)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L70
	movl	8(%r12), %ecx
	cmpl	%ecx, %edx
	jg	.L70
	movl	12(%r12), %edx
	cmpl	%edx, %ecx
	jg	.L70
	movl	4(%r12), %ecx
	cmpl	%eax, %ecx
	jg	.L70
	cmpl	%ecx, %edx
	jle	.L79
.L70:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	96(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE3404:
	.size	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition, .-_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.type	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode, @function
_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode:
.LFB3402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	movq	%r15, %rdi
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	%ecx, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L82
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*64(%rax)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L83
	movl	8(%r12), %ecx
	cmpl	%ecx, %edx
	jg	.L83
	movl	12(%r12), %esi
	cmpl	%esi, %ecx
	jg	.L83
	movl	4(%r12), %edx
	cmpl	%edx, %eax
	jl	.L83
	cmpl	%edx, %esi
	jle	.L84
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$1, (%rbx)
.L82:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r15, %rcx
	movl	%esi, %edx
	movq	%r13, %rdi
	call	*32(%rax)
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L86
	sarl	$5, %eax
.L87:
	movl	12(%r12), %edx
	addl	%eax, 4(%r12)
	addl	%eax, %edx
	movl	%edx, 12(%r12)
	testl	%edx, %edx
	jle	.L90
	movq	0(%r13), %rax
	leal	-1(%rdx), %esi
	movq	%r13, %rdi
	call	*72(%rax)
	andl	$64512, %eax
	cmpl	$55296, %eax
	je	.L82
.L90:
	movq	(%r14), %rax
	movl	$1, %r8d
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	call	*96(%rax)
	jmp	.L82
.L86:
	movl	-116(%rbp), %eax
	jmp	.L87
.L99:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3402:
	.size	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode, .-_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"icudt67l-translit"
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC1:
	.string	"-"
	.string	"t"
	.string	"-"
	.string	""
	.string	""
	.section	.rodata.str1.1
.LC2:
	.string	"resource"
.LC3:
	.string	"direction"
	.section	.rodata.str2.2
	.align 2
.LC4:
	.string	"N"
	.string	"u"
	.string	"l"
	.string	"l"
	.string	""
	.string	""
	.align 2
.LC5:
	.string	"L"
	.string	"o"
	.string	"w"
	.string	"e"
	.string	"r"
	.string	""
	.string	""
	.align 2
.LC6:
	.string	"U"
	.string	"p"
	.string	"p"
	.string	"e"
	.string	"r"
	.string	""
	.string	""
	.align 2
.LC7:
	.string	"T"
	.string	"i"
	.string	"t"
	.string	"l"
	.string	"e"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0, @function
_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0:
.LFB4699:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	leaq	.LC0(%rip), %rdi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-280(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdx
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -280(%rbp)
	call	ures_open_67@PLT
	movq	%r13, %rcx
	xorl	%edx, %edx
	leaq	_ZL17RB_RULE_BASED_IDS(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -312(%rbp)
	call	ures_getByKey_67@PLT
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	-264(%rbp), %rdx
	movq	%rax, %r14
	leaq	.LC1(%rip), %rax
	movq	%rdx, -296(%rbp)
	movq	%rax, -264(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	-280(%rbp), %eax
	cmpl	$7, %eax
	je	.L211
	testl	%eax, %eax
	jle	.L105
.L108:
	movq	%r14, %rdi
	call	ures_close_67@PLT
	movq	-312(%rbp), %rdi
	call	ures_close_67@PLT
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L107
	movq	%rax, %rdi
	call	_ZN6icu_6718NullTransliteratorC1Ev@PLT
.L107:
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -304(%rbp)
	testq	%rax, %rax
	je	.L128
	movq	%rax, %rdi
	call	_ZN6icu_6723LowercaseTransliteratorC1Ev@PLT
.L128:
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L129
	movq	%rax, %rdi
	call	_ZN6icu_6723UppercaseTransliteratorC1Ev@PLT
.L129:
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L130
	movq	%rax, %rdi
	call	_ZN6icu_6723TitlecaseTransliteratorC1Ev@PLT
.L130:
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L131
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6725UnicodeNameTransliteratorC1EPNS_13UnicodeFilterE@PLT
.L131:
	movl	$288, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -312(%rbp)
	testq	%rax, %rax
	je	.L132
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6725NameUnicodeTransliteratorC1EPNS_13UnicodeFilterE@PLT
.L132:
	movl	$168, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L133
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN6icu_6719BreakTransliteratorC1EPNS_13UnicodeFilterE@PLT
	movq	-328(%rbp), %r8
.L133:
	testq	%rbx, %rbx
	je	.L137
	cmpq	$0, -304(%rbp)
	je	.L134
	testq	%r14, %r14
	je	.L135
	testq	%r13, %r13
	je	.L135
	testq	%r12, %r12
	sete	%dl
	testq	%r8, %r8
	sete	%al
	orb	%al, %dl
	jne	.L135
	cmpq	$0, -312(%rbp)
	je	.L135
	movq	_ZL8registry(%rip), %rdi
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movl	$1, %edx
	movq	%r8, -328(%rbp)
	leaq	.LC4(%rip), %rbx
	call	_ZN6icu_6722TransliteratorRegistry3putEPNS_14TransliteratorEaR10UErrorCode@PLT
	movq	-304(%rbp), %rsi
	movq	%r15, %rcx
	movq	_ZL8registry(%rip), %rdi
	movl	$1, %edx
	call	_ZN6icu_6722TransliteratorRegistry3putEPNS_14TransliteratorEaR10UErrorCode@PLT
	movq	%r15, %rcx
	movq	%r14, %rsi
	movl	$1, %edx
	movq	_ZL8registry(%rip), %rdi
	leaq	-276(%rbp), %r14
	call	_ZN6icu_6722TransliteratorRegistry3putEPNS_14TransliteratorEaR10UErrorCode@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	$1, %edx
	movq	_ZL8registry(%rip), %rdi
	leaq	-192(%rbp), %r13
	call	_ZN6icu_6722TransliteratorRegistry3putEPNS_14TransliteratorEaR10UErrorCode@PLT
	movq	%r15, %rcx
	movq	%r12, %rsi
	movl	$1, %edx
	movq	_ZL8registry(%rip), %rdi
	leaq	-128(%rbp), %r12
	call	_ZN6icu_6722TransliteratorRegistry3putEPNS_14TransliteratorEaR10UErrorCode@PLT
	movq	-312(%rbp), %rsi
	movq	%r15, %rcx
	movq	_ZL8registry(%rip), %rdi
	movl	$1, %edx
	call	_ZN6icu_6722TransliteratorRegistry3putEPNS_14TransliteratorEaR10UErrorCode@PLT
	movq	-328(%rbp), %r8
	movq	_ZL8registry(%rip), %rdi
	xorl	%edx, %edx
	movq	%r15, %rcx
	leaq	-272(%rbp), %r15
	movq	%r8, %rsi
	call	_ZN6icu_6722TransliteratorRegistry3putEPNS_14TransliteratorEaR10UErrorCode@PLT
	call	_ZN6icu_6720RemoveTransliterator11registerIDsEv@PLT
	call	_ZN6icu_6720EscapeTransliterator11registerIDsEv@PLT
	call	_ZN6icu_6722UnescapeTransliterator11registerIDsEv@PLT
	call	_ZN6icu_6727NormalizationTransliterator11registerIDsEv@PLT
	call	_ZN6icu_6717AnyTransliterator11registerIDsEv@PLT
	movq	-296(%rbp), %rdx
	movq	%r12, %rdi
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rbx, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rbx, -272(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$0, -276(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_6722TransliteratorIDParser22registerSpecialInverseERKNS_13UnicodeStringES3_aR10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-296(%rbp), %rdx
	movq	%r12, %rdi
	movl	$-1, %ecx
	leaq	.LC5(%rip), %rbx
	movl	$1, %esi
	movq	%rbx, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movl	$-1, %ecx
	leaq	.LC6(%rip), %rax
	movl	$1, %esi
	movq	%rax, -272(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$0, -276(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_6722TransliteratorIDParser22registerSpecialInverseERKNS_13UnicodeStringES3_aR10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-296(%rbp), %rdx
	movq	%r12, %rdi
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rbx, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movl	$-1, %ecx
	leaq	.LC7(%rip), %rax
	movl	$1, %esi
	movq	%rax, -272(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$0, -276(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_6722TransliteratorIDParser22registerSpecialInverseERKNS_13UnicodeStringES3_aR10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	utrans_transliterator_cleanup_67(%rip), %rsi
	movl	$5, %edi
	movl	$1, %r12d
	call	ucln_i18n_registerCleanup_67@PLT
.L104:
	movq	-320(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L212
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L134:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L137
.L135:
	movq	(%rbx), %rax
	movq	%r8, -296(%rbp)
	movq	%rbx, %rdi
	call	*8(%rax)
	movq	-296(%rbp), %r8
.L137:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L138
	movq	(%rdi), %rax
	movq	%r8, -296(%rbp)
	call	*8(%rax)
	movq	-296(%rbp), %r8
.L138:
	testq	%r14, %r14
	je	.L139
	movq	(%r14), %rax
	movq	%r8, -296(%rbp)
	movq	%r14, %rdi
	call	*8(%rax)
	movq	-296(%rbp), %r8
.L139:
	testq	%r13, %r13
	je	.L140
	movq	0(%r13), %rax
	movq	%r8, -296(%rbp)
	movq	%r13, %rdi
	call	*8(%rax)
	movq	-296(%rbp), %r8
.L140:
	testq	%r12, %r12
	je	.L141
	movq	(%r12), %rax
	movq	%r8, -296(%rbp)
	movq	%r12, %rdi
	call	*8(%rax)
	movq	-296(%rbp), %r8
.L141:
	movq	-312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L142
	movq	(%rdi), %rax
	movq	%r8, -296(%rbp)
	call	*8(%rax)
	movq	-296(%rbp), %r8
.L142:
	testq	%r8, %r8
	je	.L211
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L211:
	movq	_ZL8registry(%rip), %r12
	testq	%r12, %r12
	je	.L103
	movq	%r12, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L103:
	movq	$0, _ZL8registry(%rip)
	xorl	%r12d, %r12d
	movl	$7, (%r15)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L105:
	movq	%r14, %rdi
	call	ures_getSize_67@PLT
	movl	%eax, -304(%rbp)
	testl	%eax, %eax
	jle	.L108
	movq	%r15, -336(%rbp)
	xorl	%ebx, %ebx
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%r12, %rdi
	addl	$1, %ebx
	call	ures_close_67@PLT
	cmpl	%ebx, -304(%rbp)
	je	.L213
.L127:
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	ures_getByIndex_67@PLT
	movl	-280(%rbp), %r8d
	movq	%rax, %r12
	testl	%r8d, %r8d
	jg	.L109
	movq	%rax, %rdi
	leaq	-192(%rbp), %r15
	call	ures_getKey_67@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movswl	-184(%rbp), %r9d
	testw	%r9w, %r9w
	js	.L110
	sarl	$5, %r9d
.L111:
	movzwl	-248(%rbp), %eax
	movl	%eax, %edx
	andl	$1, %edx
	testw	%ax, %ax
	js	.L112
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testb	%dl, %dl
	jne	.L114
.L113:
	testl	%ecx, %ecx
	je	.L114
	testb	$2, %al
	leaq	-246(%rbp), %rsi
	movq	%r15, %rdi
	cmove	-232(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	cmpl	$-1, %eax
	je	.L114
	movq	%r12, %rdi
	addl	$1, %ebx
	call	ures_close_67@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	%ebx, -304(%rbp)
	jne	.L127
.L213:
	movq	-336(%rbp), %r15
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L112:
	movl	-244(%rbp), %ecx
	testb	%dl, %dl
	jne	.L114
	testl	%ecx, %ecx
	jns	.L113
.L114:
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	ures_getNextResource_67@PLT
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	call	ures_getKey_67@PLT
	leaq	-282(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	call	u_charsToUChars_67@PLT
	movl	-280(%rbp), %edi
	movq	-328(%rbp), %r10
	testl	%edi, %edi
	jle	.L214
.L117:
	movq	%r10, %rdi
	call	ures_close_67@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L110:
	movl	-180(%rbp), %r9d
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L214:
	movzwl	-282(%rbp), %eax
	movl	$0, -276(%rbp)
	cmpw	$102, %ax
	je	.L118
	cmpw	$105, %ax
	je	.L118
	cmpw	$97, %ax
	jne	.L117
	movq	%r10, %rdi
	leaq	-276(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r10, -352(%rbp)
	call	ures_getString_67@PLT
	movq	_ZL8registry(%rip), %rcx
	leaq	-128(%rbp), %r11
	movq	-296(%rbp), %rdx
	movq	%r11, %rdi
	movl	$1, %esi
	movq	%rax, -264(%rbp)
	movq	%rcx, -328(%rbp)
	movl	-276(%rbp), %ecx
	movq	%r11, -344(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %r9
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	-344(%rbp), %r11
	movq	-328(%rbp), %rdi
	movl	$1, %r8d
	movq	%r11, %rdx
	call	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringES3_aaR10UErrorCode@PLT
	movq	-344(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-352(%rbp), %r10
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r10, %rdi
	leaq	-276(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r10, -352(%rbp)
	leaq	.LC2(%rip), %rsi
	call	ures_getStringByKey_67@PLT
	movq	-352(%rbp), %r10
	movl	$2, %ecx
	cmpw	$102, -282(%rbp)
	movq	%rax, -328(%rbp)
	leaq	.LC3(%rip), %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, -120(%rbp)
	movq	%r10, %rdi
	movq	%r13, %rcx
	leaq	-272(%rbp), %rdx
	sete	-344(%rbp)
	movq	%rax, -128(%rbp)
	movl	$0, -272(%rbp)
	call	ures_getStringByKey_67@PLT
	movl	-280(%rbp), %esi
	movq	-352(%rbp), %r10
	testl	%esi, %esi
	movq	%r10, -360(%rbp)
	jle	.L215
	leaq	-128(%rbp), %r11
	movq	%r11, %rdi
	movq	%r11, -352(%rbp)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movq	-360(%rbp), %r10
	movq	-352(%rbp), %r11
.L122:
	movzwl	-120(%rbp), %edx
	testw	%dx, %dx
	js	.L123
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L124:
	movl	$1, %eax
	testl	%ecx, %ecx
	je	.L125
	andl	$2, %edx
	leaq	-118(%rbp), %rax
	cmove	-104(%rbp), %rax
	cmpw	$70, (%rax)
	setne	%al
	movzbl	%al, %eax
.L125:
	movq	%r11, %rdi
	movq	%r10, -368(%rbp)
	movl	%eax, -372(%rbp)
	movq	%r11, -360(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-328(%rbp), %rcx
	movq	-360(%rbp), %r11
	movl	$1, %esi
	movq	_ZL8registry(%rip), %rax
	movq	-296(%rbp), %rdx
	movq	%rcx, -264(%rbp)
	movl	-276(%rbp), %ecx
	movq	%r11, %rdi
	movq	%rax, -352(%rbp)
	movq	%r11, -328(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	subq	$8, %rsp
	movq	%r15, %rsi
	movl	-372(%rbp), %eax
	pushq	%r13
	movq	-328(%rbp), %r11
	movl	$1, %r8d
	movl	-344(%rbp), %r9d
	movq	-352(%rbp), %rdi
	movl	%eax, %ecx
	movq	%r11, %rdx
	andl	$1, %r9d
	call	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringES3_15UTransDirectionaaR10UErrorCode@PLT
	movq	-328(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	popq	%rax
	movq	-368(%rbp), %r10
	popq	%rdx
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L123:
	movl	-116(%rbp), %ecx
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L215:
	leaq	-128(%rbp), %r11
	movl	-272(%rbp), %ecx
	movq	-296(%rbp), %rdx
	movl	$1, %esi
	movq	%r11, %rdi
	movq	%rax, -264(%rbp)
	movq	%r11, -352(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	-352(%rbp), %r11
	movq	-360(%rbp), %r10
	jmp	.L122
.L212:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4699:
	.size	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0, .-_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.type	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE, @function
_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE:
.LFB3400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rsi, %rdi
	movq	32(%rax), %rbx
	movq	(%rsi), %rax
	call	*64(%rax)
	movl	%eax, %r13d
	leaq	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii(%rip), %rax
	cmpq	%rax, %rbx
	jne	.L217
	testl	%r13d, %r13d
	js	.L216
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*64(%rax)
	cmpl	%eax, %r13d
	jg	.L216
	movq	(%r14), %rax
	movl	$0, -64(%rbp)
	leaq	-64(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	%r13d, -60(%rbp)
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$0, -56(%rbp)
	movl	%r13d, -52(%rbp)
	call	*96(%rax)
.L216:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L224
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*%rbx
	jmp	.L216
.L224:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3400:
	.size	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE, .-_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.section	.rodata.str2.2
	.align 2
.LC8:
	.string	":"
	.string	":"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa
	.type	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa, @function
_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa:
.LFB3422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L250
	movq	(%rdi), %rax
	leaq	_ZNK6icu_6714Transliterator5getIDEv(%rip), %rdx
	movq	104(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L240
	leaq	8(%rdi), %rsi
.L241:
	movq	%r12, %rdi
	leaq	-112(%rbp), %r13
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L239:
	leaq	-120(%rbp), %r14
	leaq	.LC8(%rip), %rax
	movl	$-1, %ecx
	movq	%r13, %rdi
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-104(%rbp), %r9d
	testw	%r9w, %r9w
	js	.L242
	sarl	$5, %r9d
.L243:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$59, %eax
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movw	%ax, -120(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L251
	addq	$96, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movl	-100(%rbp), %r9d
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L250:
	movzwl	8(%rsi), %eax
	testb	$1, %al
	jne	.L252
	testw	%ax, %ax
	js	.L229
	movswl	%ax, %edx
	sarl	$5, %edx
.L230:
	testl	%edx, %edx
	je	.L228
	andl	$31, %eax
	movw	%ax, 8(%r12)
.L228:
	movq	0(%r13), %rax
	leaq	_ZNK6icu_6714Transliterator5getIDEv(%rip), %rdx
	movq	104(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L231
	leaq	8(%r13), %rsi
.L232:
	leaq	-112(%rbp), %r13
	xorl	%r14d, %r14d
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L254:
	sarl	$5, %eax
	cmpl	%eax, %r14d
	jge	.L235
.L255:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	%eax, %ebx
	call	_ZN6icu_6711ICU_Utility17escapeUnprintableERNS_13UnicodeStringEi@PLT
	testb	%al, %al
	je	.L253
.L236:
	xorl	%eax, %eax
	cmpl	$65535, %ebx
	seta	%al
	leal	1(%r14,%rax), %r14d
.L238:
	movswl	-104(%rbp), %eax
	testw	%ax, %ax
	jns	.L254
	movl	-100(%rbp), %eax
	cmpl	%eax, %r14d
	jl	.L255
.L235:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L252:
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L253:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L240:
	call	*%rax
	movq	%rax, %rsi
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L229:
	movl	12(%rsi), %edx
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L231:
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L232
.L251:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3422:
	.size	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa, .-_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.type	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode, @function
_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode:
.LFB3403:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L272
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rcx, %rbx
	call	*64(%rax)
	movl	0(%r13), %edx
	testl	%edx, %edx
	js	.L259
	movl	8(%r13), %ecx
	cmpl	%ecx, %edx
	jg	.L259
	movl	12(%r13), %edx
	cmpl	%edx, %ecx
	jg	.L259
	movl	4(%r13), %ecx
	cmpl	%ecx, %edx
	jg	.L259
	cmpl	%ecx, %eax
	jge	.L260
	.p2align 4,,10
	.p2align 3
.L259:
	movl	$1, (%rbx)
.L256:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	testl	%edx, %edx
	jle	.L264
	movq	(%r12), %rax
	leal	-1(%rdx), %esi
	movq	%r12, %rdi
	call	*72(%rax)
	andl	$64512, %eax
	cmpl	$55296, %eax
	je	.L256
.L264:
	movq	(%r14), %rax
	popq	%rbx
	.cfi_restore 3
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	popq	%r12
	.cfi_restore 12
	movl	$1, %r8d
	popq	%r13
	.cfi_restore 13
	movq	96(%rax), %rax
	popq	%r14
	.cfi_restore 14
	movl	$1, %ecx
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE3403:
	.size	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode, .-_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode:
.LFB3401:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L293
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	call	*64(%rax)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L278
	movl	8(%r12), %ecx
	cmpl	%ecx, %edx
	jg	.L278
	movl	12(%r12), %esi
	cmpl	%esi, %ecx
	jg	.L278
	movl	4(%r12), %edx
	cmpl	%edx, %eax
	jl	.L278
	cmpl	%edx, %esi
	jle	.L279
	.p2align 4,,10
	.p2align 3
.L278:
	movl	$1, (%rbx)
.L275:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%r15), %rax
	movq	%r14, %rcx
	movl	%esi, %edx
	movq	%r15, %rdi
	call	*32(%rax)
	movswl	8(%r14), %eax
	testw	%ax, %ax
	js	.L281
	sarl	$5, %eax
.L282:
	movl	12(%r12), %edx
	addl	%eax, 4(%r12)
	addl	%eax, %edx
	movl	%edx, 12(%r12)
	testl	%edx, %edx
	jle	.L285
	movq	(%r15), %rax
	leal	-1(%rdx), %esi
	movq	%r15, %rdi
	call	*72(%rax)
	andl	$64512, %eax
	cmpl	$55296, %eax
	je	.L275
.L285:
	movq	0(%r13), %rax
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	movq	96(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L281:
	.cfi_restore_state
	movl	12(%r14), %eax
	jmp	.L282
	.cfi_endproc
.LFE3401:
	.size	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator16getStaticClassIDEv
	.type	_ZN6icu_6714Transliterator16getStaticClassIDEv, @function
_ZN6icu_6714Transliterator16getStaticClassIDEv:
.LFB3385:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714Transliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3385:
	.size	_ZN6icu_6714Transliterator16getStaticClassIDEv, .-_ZN6icu_6714Transliterator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE
	.type	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE, @function
_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE:
.LFB3388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6714TransliteratorE(%rip), %rax
	movq	%rax, (%rdi)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r12, 72(%rbx)
	xorl	%eax, %eax
	leaq	-42(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%ax, -42(%rbp)
	movl	$0, 80(%rbx)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	16(%rbx), %eax
	testw	%ax, %ax
	js	.L298
	movswl	%ax, %ecx
	sarl	$5, %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L316
	testl	%edx, %edx
	je	.L306
.L316:
	cmpl	%ecx, %edx
	jb	.L305
.L297:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L317
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, 16(%rbx)
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L298:
	movl	20(%rbx), %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L302
	testl	%edx, %edx
	je	.L306
.L302:
	cmpl	%edx, %ecx
	jbe	.L297
	cmpl	$1023, %edx
	jle	.L305
	orl	$-32, %eax
	movl	%edx, 20(%rbx)
	movw	%ax, 16(%rbx)
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L297
.L317:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3388:
	.size	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE, .-_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE
	.globl	_ZN6icu_6714TransliteratorC1ERKNS_13UnicodeStringEPNS_13UnicodeFilterE
	.set	_ZN6icu_6714TransliteratorC1ERKNS_13UnicodeStringEPNS_13UnicodeFilterE,_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TransliteratorD2Ev
	.type	_ZN6icu_6714TransliteratorD2Ev, @function
_ZN6icu_6714TransliteratorD2Ev:
.LFB3391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714TransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L319
	movq	(%rdi), %rax
	call	*8(%rax)
.L319:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3391:
	.size	_ZN6icu_6714TransliteratorD2Ev, .-_ZN6icu_6714TransliteratorD2Ev
	.globl	_ZN6icu_6714TransliteratorD1Ev
	.set	_ZN6icu_6714TransliteratorD1Ev,_ZN6icu_6714TransliteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TransliteratorD0Ev
	.type	_ZN6icu_6714TransliteratorD0Ev, @function
_ZN6icu_6714TransliteratorD0Ev:
.LFB3393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714TransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L325
	movq	(%rdi), %rax
	call	*8(%rax)
.L325:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3393:
	.size	_ZN6icu_6714TransliteratorD0Ev, .-_ZN6icu_6714TransliteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TransliteratorC2ERKS0_
	.type	_ZN6icu_6714TransliteratorC2ERKS0_, @function
_ZN6icu_6714TransliteratorC2ERKS0_:
.LFB3395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	addq	$8, %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6714TransliteratorE(%rip), %rax
	movq	%rax, (%rdi)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	80(%r12), %eax
	leaq	-42(%rbp), %rsi
	xorl	%edx, %edx
	movq	$0, 72(%rbx)
	movl	$1, %ecx
	movq	%r13, %rdi
	movl	%eax, 80(%rbx)
	xorl	%eax, %eax
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	16(%rbx), %eax
	testw	%ax, %ax
	js	.L331
	movswl	%ax, %ecx
	sarl	$5, %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L353
	testl	%edx, %edx
	je	.L340
.L353:
	cmpl	%ecx, %edx
	jb	.L339
.L336:
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.L330
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 72(%rbx)
.L330:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L354
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, 16(%rbx)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L331:
	movl	20(%rbx), %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L335
	testl	%edx, %edx
	je	.L340
.L335:
	cmpl	%edx, %ecx
	jbe	.L336
	cmpl	$1023, %edx
	jle	.L339
	orl	$-32, %eax
	movl	%edx, 20(%rbx)
	movw	%ax, 16(%rbx)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L336
.L354:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3395:
	.size	_ZN6icu_6714TransliteratorC2ERKS0_, .-_ZN6icu_6714TransliteratorC2ERKS0_
	.globl	_ZN6icu_6714TransliteratorC1ERKS0_
	.set	_ZN6icu_6714TransliteratorC1ERKS0_,_ZN6icu_6714TransliteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TransliteratoraSERKS0_
	.type	_ZN6icu_6714TransliteratoraSERKS0_, @function
_ZN6icu_6714TransliteratoraSERKS0_:
.LFB3398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	addq	$8, %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movl	80(%r13), %eax
	movq	72(%r13), %r13
	movl	%eax, 80(%r12)
	testq	%r13, %r13
	je	.L356
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%rax, %r13
.L356:
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.L357
	movq	(%rdi), %rax
	call	*8(%rax)
.L357:
	movq	%r13, 72(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3398:
	.size	_ZN6icu_6714TransliteratoraSERKS0_, .-_ZN6icu_6714TransliteratoraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator14_transliterateERNS_11ReplaceableER14UTransPositionPKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6714Transliterator14_transliterateERNS_11ReplaceableER14UTransPositionPKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6714Transliterator14_transliterateERNS_11ReplaceableER14UTransPositionPKNS_13UnicodeStringER10UErrorCode:
.LFB3405:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L386
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	call	*64(%rax)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L368
	movl	8(%r12), %ecx
	cmpl	%ecx, %edx
	jg	.L368
	movl	12(%r12), %esi
	cmpl	%esi, %ecx
	jg	.L368
	movl	4(%r12), %edx
	cmpl	%edx, %eax
	jl	.L368
	cmpl	%edx, %esi
	jle	.L389
	.p2align 4,,10
	.p2align 3
.L368:
	movl	$1, (%rbx)
.L365:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	testq	%r14, %r14
	je	.L370
	movq	(%r15), %rax
	movq	%r14, %rcx
	movl	%esi, %edx
	movq	%r15, %rdi
	call	*32(%rax)
	movswl	8(%r14), %eax
	testw	%ax, %ax
	js	.L371
	sarl	$5, %eax
.L372:
	movl	12(%r12), %esi
	addl	%eax, 4(%r12)
	addl	%eax, %esi
	movl	%esi, 12(%r12)
.L370:
	testl	%esi, %esi
	jle	.L375
	movq	(%r15), %rax
	subl	$1, %esi
	movq	%r15, %rdi
	call	*72(%rax)
	andl	$64512, %eax
	cmpl	$55296, %eax
	je	.L365
.L375:
	movq	0(%r13), %rax
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	movq	96(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L371:
	.cfi_restore_state
	movl	12(%r14), %eax
	jmp	.L372
	.cfi_endproc
.LFE3405:
	.size	_ZNK6icu_6714Transliterator14_transliterateERNS_11ReplaceableER14UTransPositionPKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6714Transliterator14_transliterateERNS_11ReplaceableER14UTransPositionPKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator23setMaximumContextLengthEi
	.type	_ZN6icu_6714Transliterator23setMaximumContextLengthEi, @function
_ZN6icu_6714Transliterator23setMaximumContextLengthEi:
.LFB3408:
	.cfi_startproc
	endbr64
	movl	%esi, 80(%rdi)
	ret
	.cfi_endproc
.LFE3408:
	.size	_ZN6icu_6714Transliterator23setMaximumContextLengthEi, .-_ZN6icu_6714Transliterator23setMaximumContextLengthEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator9getFilterEv
	.type	_ZNK6icu_6714Transliterator9getFilterEv, @function
_ZNK6icu_6714Transliterator9getFilterEv:
.LFB3412:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE3412:
	.size	_ZNK6icu_6714Transliterator9getFilterEv, .-_ZNK6icu_6714Transliterator9getFilterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator12orphanFilterEv
	.type	_ZN6icu_6714Transliterator12orphanFilterEv, @function
_ZN6icu_6714Transliterator12orphanFilterEv:
.LFB3413:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	movq	$0, 72(%rdi)
	ret
	.cfi_endproc
.LFE3413:
	.size	_ZN6icu_6714Transliterator12orphanFilterEv, .-_ZN6icu_6714Transliterator12orphanFilterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator11adoptFilterEPNS_13UnicodeFilterE
	.type	_ZN6icu_6714Transliterator11adoptFilterEPNS_13UnicodeFilterE, @function
_ZN6icu_6714Transliterator11adoptFilterEPNS_13UnicodeFilterE:
.LFB3414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L394
	movq	(%rdi), %rax
	call	*8(%rax)
.L394:
	movq	%r12, 72(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3414:
	.size	_ZN6icu_6714Transliterator11adoptFilterEPNS_13UnicodeFilterE, .-_ZN6icu_6714Transliterator11adoptFilterEPNS_13UnicodeFilterE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator19createBasicInstanceERKNS_13UnicodeStringEPS2_
	.type	_ZN6icu_6714Transliterator19createBasicInstanceERKNS_13UnicodeStringEPS2_, @function
_ZN6icu_6714Transliterator19createBasicInstanceERKNS_13UnicodeStringEPS2_:
.LFB3420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	_ZL13registryMutex(%rip), %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-652(%rbp), %r12
	pushq	%rbx
	subq	$632, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -664(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -652(%rbp)
	movq	$0, -648(%rbp)
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %r13
	testq	%r13, %r13
	je	.L477
.L400:
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-648(%rbp), %rdx
	movq	%r12, %rcx
	call	_ZN6icu_6722TransliteratorRegistry3getERKNS_13UnicodeStringERPNS_19TransliteratorAliasER10UErrorCode@PLT
	leaq	_ZL13registryMutex(%rip), %rdi
	movq	%rax, %r13
	call	umtx_unlock_67@PLT
	movl	-652(%rbp), %esi
	testl	%esi, %esi
	jg	.L417
.L407:
	leaq	-652(%rbp), %rbx
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L413:
	leaq	-648(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_6722TransliteratorRegistry5regetERKNS_13UnicodeStringERNS_20TransliteratorParserERPNS_19TransliteratorAliasER10UErrorCode@PLT
	movq	%rax, %r13
.L416:
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6720TransliteratorParserD1Ev@PLT
	movl	-652(%rbp), %edx
	testl	%edx, %edx
	jg	.L417
.L418:
	movq	-648(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L410
	call	_ZNK6icu_6719TransliteratorAlias11isRuleBasedEv@PLT
	testb	%al, %al
	je	.L411
	leaq	-560(%rbp), %r12
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6720TransliteratorParserC1ER10UErrorCode@PLT
	movq	-648(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r12, %rsi
	leaq	-640(%rbp), %rdx
	call	_ZNK6icu_6719TransliteratorAlias5parseERNS_20TransliteratorParserER11UParseErrorR10UErrorCode@PLT
	movq	-648(%rbp), %r15
	testq	%r15, %r15
	je	.L412
	movq	%r15, %rdi
	call	_ZN6icu_6719TransliteratorAliasD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L412:
	leaq	_ZL13registryMutex(%rip), %rdi
	movq	$0, -648(%rbp)
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %rdi
	testq	%rdi, %rdi
	jne	.L413
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L414
	movq	%rbx, %rsi
	movq	%rax, -672(%rbp)
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movq	-672(%rbp), %rdi
	movl	-652(%rbp), %ecx
	movq	%rdi, _ZL8registry(%rip)
	testl	%ecx, %ecx
	jle	.L478
	movq	%rdi, -672(%rbp)
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	-672(%rbp), %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L414:
	movq	$0, _ZL8registry(%rip)
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L478:
	movq	%rbx, %rdi
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	movq	_ZL8registry(%rip), %rdi
	testb	%al, %al
	jne	.L413
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L411:
	movq	-648(%rbp), %rdi
	leaq	-652(%rbp), %rdx
	leaq	-640(%rbp), %rsi
	call	_ZN6icu_6719TransliteratorAlias6createER11UParseErrorR10UErrorCode@PLT
	movq	-648(%rbp), %r12
	movq	%rax, %r13
	testq	%r12, %r12
	je	.L419
	movq	%r12, %rdi
	call	_ZN6icu_6719TransliteratorAliasD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L419:
	movq	$0, -648(%rbp)
.L410:
	testq	%r13, %r13
	je	.L399
	movq	-664(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L399
	leaq	8(%r13), %r12
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%eax, %eax
	leaq	-654(%rbp), %rsi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%ax, -654(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	16(%r13), %eax
	testw	%ax, %ax
	js	.L422
	movswl	%ax, %ecx
	sarl	$5, %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L473
	testl	%edx, %edx
	je	.L429
.L473:
	cmpl	%ecx, %edx
	jnb	.L399
.L428:
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, 16(%r13)
	.p2align 4,,10
	.p2align 3
.L399:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L479
	addq	$632, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L420
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L420:
	movq	-648(%rbp), %r12
	testq	%r12, %r12
	je	.L474
	movq	%r12, %rdi
	call	_ZN6icu_6719TransliteratorAliasD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L474:
	xorl	%r13d, %r13d
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L422:
	movl	20(%r13), %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L426
	testl	%edx, %edx
	je	.L429
.L426:
	cmpl	%edx, %ecx
	jbe	.L399
	cmpl	$1023, %edx
	jle	.L428
	orl	$-32, %eax
	movl	%edx, 20(%r13)
	movw	%ax, 16(%r13)
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L477:
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L401
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	-652(%rbp), %r9d
	movq	%rbx, _ZL8registry(%rip)
	testl	%r9d, %r9d
	jle	.L480
	movq	%rbx, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L401:
	leaq	_ZL13registryMutex(%rip), %rdi
	movq	$0, _ZL8registry(%rip)
	call	umtx_unlock_67@PLT
	movl	-652(%rbp), %r8d
	testl	%r8d, %r8d
	jle	.L407
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	testb	%al, %al
	je	.L403
	movq	_ZL8registry(%rip), %r13
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L403:
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movl	-652(%rbp), %edi
	testl	%edi, %edi
	jle	.L407
	jmp	.L420
.L429:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L399
.L479:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3420:
	.size	_ZN6icu_6714Transliterator19createBasicInstanceERKNS_13UnicodeStringEPS2_, .-_ZN6icu_6714Transliterator19createBasicInstanceERKNS_13UnicodeStringEPS2_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator13countElementsEv
	.type	_ZNK6icu_6714Transliterator13countElementsEv, @function
_ZNK6icu_6714Transliterator13countElementsEv:
.LFB3423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6722CompoundTransliteratorE(%rip), %rdx
	leaq	_ZTIN6icu_6714TransliteratorE(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L482
	movq	%rax, %rdi
	movq	(%rax), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	136(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L482:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3423:
	.size	_ZNK6icu_6714Transliterator13countElementsEv, .-_ZNK6icu_6714Transliterator13countElementsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator10getElementEiR10UErrorCode
	.type	_ZNK6icu_6714Transliterator10getElementEiR10UErrorCode, @function
_ZNK6icu_6714Transliterator10getElementEiR10UErrorCode:
.LFB3424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L484
	movl	%esi, %r13d
	movq	%rdx, %rbx
	leaq	_ZTIN6icu_6714TransliteratorE(%rip), %rsi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6722CompoundTransliteratorE(%rip), %rdx
	call	__dynamic_cast@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L497
	movq	(%rax), %rax
	movq	%r14, %rdi
	call	*136(%rax)
	testl	%r13d, %r13d
	js	.L489
	cmpl	%r13d, %eax
	jle	.L489
	cmpl	$1, %eax
	je	.L484
	movq	(%r14), %rax
	popq	%rbx
	movl	%r13d, %esi
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	movq	144(%rax), %rax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	testl	%r13d, %r13d
	je	.L484
.L489:
	movl	$8, (%rbx)
.L484:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3424:
	.size	_ZNK6icu_6714Transliterator10getElementEiR10UErrorCode, .-_ZNK6icu_6714Transliterator10getElementEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator15registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_
	.type	_ZN6icu_6714Transliterator15registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_, @function
_ZN6icu_6714Transliterator15registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_:
.LFB3428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	_ZL13registryMutex(%rip), %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %rdi
	movl	$0, -64(%rbp)
	testq	%rdi, %rdi
	je	.L511
.L499:
	leaq	-60(%rbp), %r9
	movl	$1, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movl	$0, -60(%rbp)
	call	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringEPFPNS_14TransliteratorES3_NS4_5TokenEES6_aR10UErrorCode@PLT
.L502:
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L512
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L511:
	.cfi_restore_state
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L500
	leaq	-64(%rbp), %rbx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	-64(%rbp), %eax
	movq	%r15, _ZL8registry(%rip)
	testl	%eax, %eax
	jle	.L513
	movq	%r15, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L500:
	movq	$0, _ZL8registry(%rip)
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L513:
	movq	%rbx, %rdi
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	movq	_ZL8registry(%rip), %rdi
	testb	%al, %al
	jne	.L499
	jmp	.L502
.L512:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3428:
	.size	_ZN6icu_6714Transliterator15registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_, .-_ZN6icu_6714Transliterator15registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_
	.type	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_, @function
_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_:
.LFB3429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	_ZL8registry(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %r9
	movl	$0, -12(%rbp)
	call	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringEPFPNS_14TransliteratorES3_NS4_5TokenEES6_aR10UErrorCode@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L517
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L517:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3429:
	.size	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_, .-_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator23_registerSpecialInverseERKNS_13UnicodeStringES3_a
	.type	_ZN6icu_6714Transliterator23_registerSpecialInverseERKNS_13UnicodeStringES3_a, @function
_ZN6icu_6714Transliterator23_registerSpecialInverseERKNS_13UnicodeStringES3_a:
.LFB3430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsbl	%dl, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rcx
	movl	$0, -12(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser22registerSpecialInverseERKNS_13UnicodeStringES3_aR10UErrorCode@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L521
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L521:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3430:
	.size	_ZN6icu_6714Transliterator23_registerSpecialInverseERKNS_13UnicodeStringES3_a, .-_ZN6icu_6714Transliterator23_registerSpecialInverseERKNS_13UnicodeStringES3_a
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator16registerInstanceEPS0_
	.type	_ZN6icu_6714Transliterator16registerInstanceEPS0_, @function
_ZN6icu_6714Transliterator16registerInstanceEPS0_:
.LFB3431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	_ZL13registryMutex(%rip), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %rdi
	movl	$0, -48(%rbp)
	testq	%rdi, %rdi
	je	.L535
.L523:
	leaq	-44(%rbp), %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6722TransliteratorRegistry3putEPNS_14TransliteratorEaR10UErrorCode@PLT
.L526:
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L536
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L524
	leaq	-48(%rbp), %r14
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	-48(%rbp), %eax
	movq	%r13, _ZL8registry(%rip)
	testl	%eax, %eax
	jle	.L537
	movq	%r13, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L524:
	movq	$0, _ZL8registry(%rip)
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L537:
	movq	%r14, %rdi
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	movq	_ZL8registry(%rip), %rdi
	testb	%al, %al
	jne	.L523
	jmp	.L526
.L536:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3431:
	.size	_ZN6icu_6714Transliterator16registerInstanceEPS0_, .-_ZN6icu_6714Transliterator16registerInstanceEPS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator17_registerInstanceEPS0_
	.type	_ZN6icu_6714Transliterator17_registerInstanceEPS0_, @function
_ZN6icu_6714Transliterator17_registerInstanceEPS0_:
.LFB3432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	_ZL8registry(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rcx
	movl	$0, -12(%rbp)
	call	_ZN6icu_6722TransliteratorRegistry3putEPNS_14TransliteratorEaR10UErrorCode@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L541
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L541:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3432:
	.size	_ZN6icu_6714Transliterator17_registerInstanceEPS0_, .-_ZN6icu_6714Transliterator17_registerInstanceEPS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator13registerAliasERKNS_13UnicodeStringES3_
	.type	_ZN6icu_6714Transliterator13registerAliasERKNS_13UnicodeStringES3_, @function
_ZN6icu_6714Transliterator13registerAliasERKNS_13UnicodeStringES3_:
.LFB3433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	_ZL13registryMutex(%rip), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %rdi
	movl	$0, -48(%rbp)
	testq	%rdi, %rdi
	je	.L555
.L543:
	leaq	-44(%rbp), %r9
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringES3_aaR10UErrorCode@PLT
.L546:
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L556
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L555:
	.cfi_restore_state
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L544
	leaq	-48(%rbp), %r15
	movq	%rax, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	-48(%rbp), %eax
	movq	%r14, _ZL8registry(%rip)
	testl	%eax, %eax
	jle	.L557
	movq	%r14, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L544:
	movq	$0, _ZL8registry(%rip)
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L557:
	movq	%r15, %rdi
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	movq	_ZL8registry(%rip), %rdi
	testb	%al, %al
	jne	.L543
	jmp	.L546
.L556:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3433:
	.size	_ZN6icu_6714Transliterator13registerAliasERKNS_13UnicodeStringES3_, .-_ZN6icu_6714Transliterator13registerAliasERKNS_13UnicodeStringES3_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator14_registerAliasERKNS_13UnicodeStringES3_
	.type	_ZN6icu_6714Transliterator14_registerAliasERKNS_13UnicodeStringES3_, @function
_ZN6icu_6714Transliterator14_registerAliasERKNS_13UnicodeStringES3_:
.LFB3434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	_ZL8registry(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %r9
	movl	$0, -12(%rbp)
	call	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringES3_aaR10UErrorCode@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L561
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L561:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3434:
	.size	_ZN6icu_6714Transliterator14_registerAliasERKNS_13UnicodeStringES3_, .-_ZN6icu_6714Transliterator14_registerAliasERKNS_13UnicodeStringES3_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator10unregisterERKNS_13UnicodeStringE
	.type	_ZN6icu_6714Transliterator10unregisterERKNS_13UnicodeStringE, @function
_ZN6icu_6714Transliterator10unregisterERKNS_13UnicodeStringE:
.LFB3435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	_ZL13registryMutex(%rip), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %rdi
	movl	$0, -44(%rbp)
	testq	%rdi, %rdi
	je	.L575
.L563:
	movq	%r12, %rsi
	call	_ZN6icu_6722TransliteratorRegistry6removeERKNS_13UnicodeStringE@PLT
.L566:
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L576
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L564
	leaq	-44(%rbp), %r14
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	-44(%rbp), %eax
	movq	%r13, _ZL8registry(%rip)
	testl	%eax, %eax
	jle	.L577
	movq	%r13, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L564:
	movq	$0, _ZL8registry(%rip)
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L577:
	movq	%r14, %rdi
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	movq	_ZL8registry(%rip), %rdi
	testb	%al, %al
	jne	.L563
	jmp	.L566
.L576:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3435:
	.size	_ZN6icu_6714Transliterator10unregisterERKNS_13UnicodeStringE, .-_ZN6icu_6714Transliterator10unregisterERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator17countAvailableIDsEv
	.type	_ZN6icu_6714Transliterator17countAvailableIDsEv, @function
_ZN6icu_6714Transliterator17countAvailableIDsEv:
.LFB3436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL13registryMutex(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %rdi
	movl	$0, -28(%rbp)
	testq	%rdi, %rdi
	je	.L590
.L579:
	call	_ZNK6icu_6722TransliteratorRegistry17countAvailableIDsEv@PLT
	movl	%eax, %r12d
.L582:
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L591
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L590:
	.cfi_restore_state
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L580
	leaq	-28(%rbp), %r13
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	-28(%rbp), %eax
	movq	%r12, _ZL8registry(%rip)
	testl	%eax, %eax
	jle	.L592
	movq	%r12, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L580:
	movq	$0, _ZL8registry(%rip)
	xorl	%r12d, %r12d
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L592:
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	movq	_ZL8registry(%rip), %rdi
	testb	%al, %al
	jne	.L579
	jmp	.L582
.L591:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3436:
	.size	_ZN6icu_6714Transliterator17countAvailableIDsEv, .-_ZN6icu_6714Transliterator17countAvailableIDsEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator14getAvailableIDEi
	.type	_ZN6icu_6714Transliterator14getAvailableIDEi, @function
_ZN6icu_6714Transliterator14getAvailableIDEi:
.LFB3437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edi, %r13d
	leaq	_ZL13registryMutex(%rip), %rdi
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %r12
	movl	$0, -44(%rbp)
	testq	%r12, %r12
	je	.L606
.L594:
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_ZNK6icu_6722TransliteratorRegistry14getAvailableIDEi@PLT
	movq	%rax, %r12
.L597:
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L607
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_restore_state
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L595
	leaq	-44(%rbp), %r15
	movq	%rax, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	-44(%rbp), %eax
	movq	%r14, _ZL8registry(%rip)
	testl	%eax, %eax
	jle	.L608
	movq	%r14, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L595:
	movq	$0, _ZL8registry(%rip)
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L608:
	movq	%r15, %rdi
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	testb	%al, %al
	je	.L597
	movq	_ZL8registry(%rip), %r12
	jmp	.L594
.L607:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3437:
	.size	_ZN6icu_6714Transliterator14getAvailableIDEi, .-_ZN6icu_6714Transliterator14getAvailableIDEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator15getAvailableIDsER10UErrorCode
	.type	_ZN6icu_6714Transliterator15getAvailableIDsER10UErrorCode, @function
_ZN6icu_6714Transliterator15getAvailableIDsER10UErrorCode:
.LFB3438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rdi), %edx
	testl	%edx, %edx
	jg	.L609
	movq	%rdi, %rbx
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %rdi
	testq	%rdi, %rdi
	je	.L621
.L611:
	call	_ZNK6icu_6722TransliteratorRegistry15getAvailableIDsEv@PLT
	leaq	_ZL13registryMutex(%rip), %rdi
	movq	%rax, %r12
	call	umtx_unlock_67@PLT
	testq	%r12, %r12
	je	.L615
.L609:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L612
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	(%rbx), %eax
	movq	%r12, _ZL8registry(%rip)
	testl	%eax, %eax
	jle	.L622
	movq	%r12, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L612:
	leaq	_ZL13registryMutex(%rip), %rdi
	movq	$0, _ZL8registry(%rip)
	call	umtx_unlock_67@PLT
.L615:
	xorl	%r12d, %r12d
	movl	$65568, (%rbx)
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	movq	_ZL8registry(%rip), %rdi
	testb	%al, %al
	jne	.L611
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	jmp	.L615
	.cfi_endproc
.LFE3438:
	.size	_ZN6icu_6714Transliterator15getAvailableIDsER10UErrorCode, .-_ZN6icu_6714Transliterator15getAvailableIDsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator21countAvailableSourcesEv
	.type	_ZN6icu_6714Transliterator21countAvailableSourcesEv, @function
_ZN6icu_6714Transliterator21countAvailableSourcesEv:
.LFB3439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL13registryMutex(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %rdi
	movl	$0, -28(%rbp)
	testq	%rdi, %rdi
	je	.L635
.L624:
	call	_ZNK6icu_6722TransliteratorRegistry21countAvailableSourcesEv@PLT
	movl	%eax, %r12d
.L627:
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L636
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L635:
	.cfi_restore_state
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L625
	leaq	-28(%rbp), %r13
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	-28(%rbp), %eax
	movq	%r12, _ZL8registry(%rip)
	testl	%eax, %eax
	jle	.L637
	movq	%r12, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L625:
	movq	$0, _ZL8registry(%rip)
	xorl	%r12d, %r12d
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L637:
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	movq	_ZL8registry(%rip), %rdi
	testb	%al, %al
	jne	.L624
	jmp	.L627
.L636:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3439:
	.size	_ZN6icu_6714Transliterator21countAvailableSourcesEv, .-_ZN6icu_6714Transliterator21countAvailableSourcesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator18getAvailableSourceEiRNS_13UnicodeStringE
	.type	_ZN6icu_6714Transliterator18getAvailableSourceEiRNS_13UnicodeStringE, @function
_ZN6icu_6714Transliterator18getAvailableSourceEiRNS_13UnicodeStringE:
.LFB3440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edi, %r13d
	leaq	_ZL13registryMutex(%rip), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %rdi
	movl	$0, -44(%rbp)
	testq	%rdi, %rdi
	je	.L651
.L639:
	movq	%r12, %rdx
	movl	%r13d, %esi
	call	_ZNK6icu_6722TransliteratorRegistry18getAvailableSourceEiRNS_13UnicodeStringE@PLT
.L642:
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L652
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L640
	leaq	-44(%rbp), %r15
	movq	%rax, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	-44(%rbp), %eax
	movq	%r14, _ZL8registry(%rip)
	testl	%eax, %eax
	jle	.L653
	movq	%r14, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L640:
	movq	$0, _ZL8registry(%rip)
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L653:
	movq	%r15, %rdi
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	movq	_ZL8registry(%rip), %rdi
	testb	%al, %al
	jne	.L639
	jmp	.L642
.L652:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3440:
	.size	_ZN6icu_6714Transliterator18getAvailableSourceEiRNS_13UnicodeStringE, .-_ZN6icu_6714Transliterator18getAvailableSourceEiRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator21countAvailableTargetsERKNS_13UnicodeStringE
	.type	_ZN6icu_6714Transliterator21countAvailableTargetsERKNS_13UnicodeStringE, @function
_ZN6icu_6714Transliterator21countAvailableTargetsERKNS_13UnicodeStringE:
.LFB3441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	_ZL13registryMutex(%rip), %rdi
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %rdi
	movl	$0, -44(%rbp)
	testq	%rdi, %rdi
	je	.L666
.L655:
	movq	%r13, %rsi
	call	_ZNK6icu_6722TransliteratorRegistry21countAvailableTargetsERKNS_13UnicodeStringE@PLT
	movl	%eax, %r12d
.L658:
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L667
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L666:
	.cfi_restore_state
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L656
	leaq	-44(%rbp), %r14
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	-44(%rbp), %eax
	movq	%r12, _ZL8registry(%rip)
	testl	%eax, %eax
	jle	.L668
	movq	%r12, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L656:
	movq	$0, _ZL8registry(%rip)
	xorl	%r12d, %r12d
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L668:
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	movq	_ZL8registry(%rip), %rdi
	testb	%al, %al
	jne	.L655
	jmp	.L658
.L667:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3441:
	.size	_ZN6icu_6714Transliterator21countAvailableTargetsERKNS_13UnicodeStringE, .-_ZN6icu_6714Transliterator21countAvailableTargetsERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator18getAvailableTargetEiRKNS_13UnicodeStringERS1_
	.type	_ZN6icu_6714Transliterator18getAvailableTargetEiRKNS_13UnicodeStringERS1_, @function
_ZN6icu_6714Transliterator18getAvailableTargetEiRKNS_13UnicodeStringERS1_:
.LFB3442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	leaq	_ZL13registryMutex(%rip), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %rdi
	movl	$0, -60(%rbp)
	testq	%rdi, %rdi
	je	.L682
.L670:
	movq	%r12, %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	call	_ZNK6icu_6722TransliteratorRegistry18getAvailableTargetEiRKNS_13UnicodeStringERS1_@PLT
.L673:
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L683
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L682:
	.cfi_restore_state
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L671
	leaq	-60(%rbp), %rbx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	-60(%rbp), %eax
	movq	%r15, _ZL8registry(%rip)
	testl	%eax, %eax
	jle	.L684
	movq	%r15, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L671:
	movq	$0, _ZL8registry(%rip)
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L684:
	movq	%rbx, %rdi
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	movq	_ZL8registry(%rip), %rdi
	testb	%al, %al
	jne	.L670
	jmp	.L673
.L683:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3442:
	.size	_ZN6icu_6714Transliterator18getAvailableTargetEiRKNS_13UnicodeStringERS1_, .-_ZN6icu_6714Transliterator18getAvailableTargetEiRKNS_13UnicodeStringERS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator22countAvailableVariantsERKNS_13UnicodeStringES3_
	.type	_ZN6icu_6714Transliterator22countAvailableVariantsERKNS_13UnicodeStringES3_, @function
_ZN6icu_6714Transliterator22countAvailableVariantsERKNS_13UnicodeStringES3_:
.LFB3443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	_ZL13registryMutex(%rip), %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %rdi
	movl	$0, -44(%rbp)
	testq	%rdi, %rdi
	je	.L697
.L686:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZNK6icu_6722TransliteratorRegistry22countAvailableVariantsERKNS_13UnicodeStringES3_@PLT
	movl	%eax, %r12d
.L689:
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L698
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L697:
	.cfi_restore_state
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L687
	leaq	-44(%rbp), %r15
	movq	%rax, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	-44(%rbp), %eax
	movq	%r12, _ZL8registry(%rip)
	testl	%eax, %eax
	jle	.L699
	movq	%r12, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L687:
	movq	$0, _ZL8registry(%rip)
	xorl	%r12d, %r12d
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L699:
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	movq	_ZL8registry(%rip), %rdi
	testb	%al, %al
	jne	.L686
	jmp	.L689
.L698:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3443:
	.size	_ZN6icu_6714Transliterator22countAvailableVariantsERKNS_13UnicodeStringES3_, .-_ZN6icu_6714Transliterator22countAvailableVariantsERKNS_13UnicodeStringES3_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator19getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_
	.type	_ZN6icu_6714Transliterator19getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_, @function
_ZN6icu_6714Transliterator19getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_:
.LFB3444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	leaq	_ZL13registryMutex(%rip), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	_ZL8registry(%rip), %rdi
	movl	$0, -60(%rbp)
	testq	%rdi, %rdi
	je	.L713
.L701:
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	call	_ZNK6icu_6722TransliteratorRegistry19getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_@PLT
.L704:
	leaq	_ZL13registryMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L714
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L713:
	.cfi_restore_state
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L702
	leaq	-60(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	movq	%r8, -72(%rbp)
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	-60(%rbp), %eax
	movq	%rbx, _ZL8registry(%rip)
	movq	-72(%rbp), %r8
	testl	%eax, %eax
	jle	.L715
	movq	%rbx, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L702:
	movq	$0, _ZL8registry(%rip)
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L715:
	movq	%r8, %rdi
	call	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	movq	_ZL8registry(%rip), %rdi
	testb	%al, %al
	jne	.L701
	jmp	.L704
.L714:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3444:
	.size	_ZN6icu_6714Transliterator19getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_, .-_ZN6icu_6714Transliterator19getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator22_countAvailableSourcesEv
	.type	_ZN6icu_6714Transliterator22_countAvailableSourcesEv, @function
_ZN6icu_6714Transliterator22_countAvailableSourcesEv:
.LFB3445:
	.cfi_startproc
	endbr64
	movq	_ZL8registry(%rip), %rdi
	jmp	_ZNK6icu_6722TransliteratorRegistry21countAvailableSourcesEv@PLT
	.cfi_endproc
.LFE3445:
	.size	_ZN6icu_6714Transliterator22_countAvailableSourcesEv, .-_ZN6icu_6714Transliterator22_countAvailableSourcesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator19_getAvailableSourceEiRNS_13UnicodeStringE
	.type	_ZN6icu_6714Transliterator19_getAvailableSourceEiRNS_13UnicodeStringE, @function
_ZN6icu_6714Transliterator19_getAvailableSourceEiRNS_13UnicodeStringE:
.LFB3446:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movl	%edi, %esi
	movq	_ZL8registry(%rip), %rdi
	jmp	_ZNK6icu_6722TransliteratorRegistry18getAvailableSourceEiRNS_13UnicodeStringE@PLT
	.cfi_endproc
.LFE3446:
	.size	_ZN6icu_6714Transliterator19_getAvailableSourceEiRNS_13UnicodeStringE, .-_ZN6icu_6714Transliterator19_getAvailableSourceEiRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator22_countAvailableTargetsERKNS_13UnicodeStringE
	.type	_ZN6icu_6714Transliterator22_countAvailableTargetsERKNS_13UnicodeStringE, @function
_ZN6icu_6714Transliterator22_countAvailableTargetsERKNS_13UnicodeStringE:
.LFB3447:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	movq	_ZL8registry(%rip), %rdi
	jmp	_ZNK6icu_6722TransliteratorRegistry21countAvailableTargetsERKNS_13UnicodeStringE@PLT
	.cfi_endproc
.LFE3447:
	.size	_ZN6icu_6714Transliterator22_countAvailableTargetsERKNS_13UnicodeStringE, .-_ZN6icu_6714Transliterator22_countAvailableTargetsERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator19_getAvailableTargetEiRKNS_13UnicodeStringERS1_
	.type	_ZN6icu_6714Transliterator19_getAvailableTargetEiRKNS_13UnicodeStringERS1_, @function
_ZN6icu_6714Transliterator19_getAvailableTargetEiRKNS_13UnicodeStringERS1_:
.LFB3448:
	.cfi_startproc
	endbr64
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movl	%edi, %esi
	movq	_ZL8registry(%rip), %rdi
	jmp	_ZNK6icu_6722TransliteratorRegistry18getAvailableTargetEiRKNS_13UnicodeStringERS1_@PLT
	.cfi_endproc
.LFE3448:
	.size	_ZN6icu_6714Transliterator19_getAvailableTargetEiRKNS_13UnicodeStringERS1_, .-_ZN6icu_6714Transliterator19_getAvailableTargetEiRKNS_13UnicodeStringERS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator23_countAvailableVariantsERKNS_13UnicodeStringES3_
	.type	_ZN6icu_6714Transliterator23_countAvailableVariantsERKNS_13UnicodeStringES3_, @function
_ZN6icu_6714Transliterator23_countAvailableVariantsERKNS_13UnicodeStringES3_:
.LFB3449:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	_ZL8registry(%rip), %rdi
	jmp	_ZNK6icu_6722TransliteratorRegistry22countAvailableVariantsERKNS_13UnicodeStringES3_@PLT
	.cfi_endproc
.LFE3449:
	.size	_ZN6icu_6714Transliterator23_countAvailableVariantsERKNS_13UnicodeStringES3_, .-_ZN6icu_6714Transliterator23_countAvailableVariantsERKNS_13UnicodeStringES3_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator20_getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_
	.type	_ZN6icu_6714Transliterator20_getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_, @function
_ZN6icu_6714Transliterator20_getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_:
.LFB3450:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movl	%edi, %esi
	movq	_ZL8registry(%rip), %rdi
	jmp	_ZNK6icu_6722TransliteratorRegistry19getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_@PLT
	.cfi_endproc
.LFE3450:
	.size	_ZN6icu_6714Transliterator20_getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_, .-_ZN6icu_6714Transliterator20_getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode
	.type	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode, @function
_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode:
.LFB3451:
	.cfi_startproc
	endbr64
	cmpq	$0, _ZL8registry(%rip)
	je	.L735
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L735:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$264, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L724
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode@PLT
	movl	(%r12), %eax
	movq	%r13, _ZL8registry(%rip)
	testl	%eax, %eax
	jle	.L736
	movq	%r13, %rdi
	call	_ZN6icu_6722TransliteratorRegistryD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L724:
	movq	$0, _ZL8registry(%rip)
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L736:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode.part.0
	.cfi_endproc
.LFE3451:
	.size	_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode, .-_ZN6icu_6714Transliterator18initializeRegistryER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator14getDisplayNameERKNS_13UnicodeStringERKNS_6LocaleERS1_
	.type	_ZN6icu_6714Transliterator14getDisplayNameERKNS_13UnicodeStringERKNS_6LocaleERS1_, @function
_ZN6icu_6714Transliterator14getDisplayNameERKNS_13UnicodeStringERKNS_6LocaleERS1_:
.LFB3411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-1972(%rbp), %rcx
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movq	%rsi, %rdx
	pushq	%rbx
	subq	$2040, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -2040(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1968(%rbp), %rax
	movq	%rcx, -2008(%rbp)
	movq	%rax, %rdi
	movq	%rax, -2000(%rbp)
	movl	$0, -1972(%rbp)
	call	_ZN6icu_6714ResourceBundleC1EPKcRKNS_6LocaleER10UErrorCode@PLT
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L738
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L739:
	movl	$2, %r13d
	movl	$2, %r10d
	movq	%r15, %rdi
	movl	$2, %r11d
	leaq	-1840(%rbp), %rax
	movw	%r13w, -1768(%rbp)
	leaq	-1904(%rbp), %r14
	leaq	-1776(%rbp), %r13
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%rax, -1992(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	leaq	-1973(%rbp), %r8
	movq	%r13, %rcx
	movw	%r10w, -1896(%rbp)
	movq	%rbx, -1904(%rbp)
	movq	%rbx, -1840(%rbp)
	movw	%r11w, -1832(%rbp)
	movq	%rbx, -1776(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser7IDtoSTVERKNS_13UnicodeStringERS1_S4_S4_Ra@PLT
	movzwl	-1832(%rbp), %eax
	testw	%ax, %ax
	js	.L742
	movswl	%ax, %edx
	sarl	$5, %edx
.L743:
	movq	%r12, %rax
	testl	%edx, %edx
	jle	.L744
	movswl	-1768(%rbp), %eax
	testw	%ax, %ax
	js	.L745
	sarl	$5, %eax
.L746:
	leaq	-1936(%rbp), %rdx
	movq	%rdx, -2016(%rbp)
	testl	%eax, %eax
	jle	.L747
	movl	$47, %r9d
	movq	%rdx, %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movw	%r9w, -1936(%rbp)
	xorl	%esi, %esi
	movl	$1, %r9d
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
.L747:
	leaq	-1712(%rbp), %r15
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$45, %r8d
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	-2016(%rbp), %rsi
	movl	$1, %ecx
	movw	%r8w, -1936(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	-1832(%rbp), %ecx
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L748
	sarl	$5, %ecx
.L749:
	movq	-1992(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	-1768(%rbp), %ecx
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L750
	sarl	$5, %ecx
.L751:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movzwl	-1704(%rbp), %eax
	testw	%ax, %ax
	js	.L752
	movswl	%ax, %esi
	sarl	$5, %esi
	testb	$17, %al
	jne	.L780
.L802:
	leaq	-1702(%rbp), %rdi
	testb	$2, %al
	cmove	-1688(%rbp), %rdi
.L754:
	call	uprv_isInvariantUString_67@PLT
	testb	%al, %al
	je	.L755
	movq	_ZL22RB_DISPLAY_NAME_PREFIX(%rip), %rax
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movq	%r15, %rdi
	leaq	-245(%rbp), %rcx
	movl	$189, %r8d
	movl	$189, %edx
	movq	%rax, -256(%rbp)
	movl	8+_ZL22RB_DISPLAY_NAME_PREFIX(%rip), %eax
	movl	%eax, -248(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, -2048(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	leaq	-1648(%rbp), %rsi
	movq	-2008(%rbp), %rcx
	movq	-2048(%rbp), %rdx
	movq	%rsi, %rdi
	movq	%rsi, -2032(%rbp)
	movq	-2000(%rbp), %rsi
	call	_ZNK6icu_6714ResourceBundle11getStringExEPKcR10UErrorCode@PLT
	movl	-1972(%rbp), %edi
	testl	%edi, %edi
	jg	.L756
	movswl	-1640(%rbp), %eax
	testw	%ax, %ax
	js	.L757
	sarl	$5, %eax
.L758:
	testl	%eax, %eax
	je	.L756
	movq	-2032(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L759:
	movq	-2032(%rbp), %rdi
	movq	%rax, -2008(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-2008(%rbp), %rax
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L756:
	leaq	-1072(%rbp), %rax
	movq	-2008(%rbp), %rcx
	movq	-2000(%rbp), %rsi
	leaq	_ZL23RB_DISPLAY_NAME_PATTERN(%rip), %rdx
	movq	%rax, %rdi
	movq	%rax, -2024(%rbp)
	movl	$0, -1972(%rbp)
	call	_ZNK6icu_6714ResourceBundle11getStringExEPKcR10UErrorCode@PLT
	movq	-2024(%rbp), %rsi
	movq	-2032(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-2024(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-1972(%rbp), %esi
	testl	%esi, %esi
	jg	.L760
	movswl	-1640(%rbp), %eax
	testw	%ax, %ax
	js	.L761
	sarl	$5, %eax
.L762:
	testl	%eax, %eax
	je	.L760
	movq	-2008(%rbp), %rcx
	movq	-2040(%rbp), %rdx
	movq	-2032(%rbp), %rsi
	movq	-2024(%rbp), %rdi
	call	_ZN6icu_6713MessageFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode@PLT
	leaq	-1408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2040(%rbp)
	call	_ZN6icu_6711FormattableC1Ev@PLT
	leaq	-1296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2024(%rbp)
	call	_ZN6icu_6711FormattableC1Ev@PLT
	leaq	-1184(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -2056(%rbp)
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	-2040(%rbp), %rdi
	movl	$2, %esi
	call	_ZN6icu_6711Formattable7setLongEi@PLT
	movq	-2024(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6711Formattable9setStringERKNS_13UnicodeStringE@PLT
	movq	-2056(%rbp), %r8
	movq	-1992(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6711Formattable9setStringERKNS_13UnicodeStringE@PLT
	movq	-2024(%rbp), %rax
	movq	%rbx, -1584(%rbp)
	movl	$2, %ecx
	movw	%cx, -1576(%rbp)
	movq	%rax, %rbx
	leaq	-1072(%rbp), %rax
	movq	%r13, -2072(%rbp)
	movq	%rax, -2024(%rbp)
	leaq	-1584(%rbp), %rax
	movq	%rax, -2056(%rbp)
	movq	%r12, -2064(%rbp)
	movq	%rbx, %r12
	movq	%rax, %rbx
.L769:
	movq	_ZL29RB_SCRIPT_DISPLAY_NAME_PREFIX(%rip), %rax
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	movl	$0, -1972(%rbp)
	movq	%rax, -256(%rbp)
	movzwl	8+_ZL29RB_SCRIPT_DISPLAY_NAME_PREFIX(%rip), %eax
	movw	%ax, -248(%rbp)
	movzbl	10+_ZL29RB_SCRIPT_DISPLAY_NAME_PREFIX(%rip), %eax
	movb	%al, -246(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzwl	-1576(%rbp), %eax
	testw	%ax, %ax
	js	.L763
	movswl	%ax, %esi
	sarl	$5, %esi
.L764:
	testb	$17, %al
	jne	.L782
	leaq	-1574(%rbp), %rdi
	testb	$2, %al
	cmove	-1560(%rbp), %rdi
.L765:
	call	uprv_isInvariantUString_67@PLT
	testb	%al, %al
	jne	.L799
.L767:
	movq	-2024(%rbp), %rdi
	addq	$112, %r12
	cmpq	%rdi, %r12
	jne	.L769
	movq	-2064(%rbp), %r12
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	-2008(%rbp), %r9
	movl	$3, %edx
	movq	-2016(%rbp), %r8
	movq	-2040(%rbp), %rsi
	movq	%rax, -1936(%rbp)
	movl	$4294967295, %eax
	movq	%r12, %rcx
	movl	$0, -1972(%rbp)
	movq	-2072(%rbp), %r13
	movq	%rax, -1928(%rbp)
	movl	$0, -1920(%rbp)
	call	_ZNK6icu_6713MessageFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode@PLT
	movl	-1972(%rbp), %eax
	testl	%eax, %eax
	jle	.L800
	movq	-2016(%rbp), %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	-2056(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-2024(%rbp), %rbx
.L775:
	movq	-112(%rbx), %rdx
	subq	$112, %rbx
	movq	%rbx, %rdi
	call	*(%rdx)
	cmpq	-2040(%rbp), %rbx
	jne	.L775
	movq	-2024(%rbp), %rdi
	call	_ZN6icu_6713MessageFormatD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L760:
	movq	-2032(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L755:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r12, %rax
.L777:
	movq	%r15, %rdi
	movq	%rax, -2008(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-2008(%rbp), %rax
.L744:
	movq	%r13, %rdi
	movq	%rax, -2008(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-1992(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-2000(%rbp), %rdi
	call	_ZN6icu_6714ResourceBundleD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movq	-2008(%rbp), %rax
	jne	.L801
	addq	$2040, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L738:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L740
	movswl	%ax, %edx
	sarl	$5, %edx
.L741:
	testl	%edx, %edx
	je	.L739
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L745:
	movl	-1764(%rbp), %eax
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L740:
	movl	12(%r12), %edx
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L742:
	movl	-1828(%rbp), %edx
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L752:
	movl	-1700(%rbp), %esi
	testb	$17, %al
	je	.L802
.L780:
	xorl	%edi, %edi
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L750:
	movl	-1764(%rbp), %ecx
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L748:
	movl	-1828(%rbp), %ecx
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L757:
	movl	-1636(%rbp), %eax
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L761:
	movl	-1636(%rbp), %eax
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L800:
	movzwl	-1768(%rbp), %eax
	testw	%ax, %ax
	js	.L771
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L772:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-2016(%rbp), %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	-2056(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-2024(%rbp), %rbx
.L773:
	movq	-112(%rbx), %rdx
	subq	$112, %rbx
	movq	%rbx, %rdi
	call	*(%rdx)
	cmpq	-2040(%rbp), %rbx
	jne	.L773
	movq	-2024(%rbp), %rdi
	call	_ZN6icu_6713MessageFormatD1Ev@PLT
	movq	%r12, %rax
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L763:
	movl	-1572(%rbp), %esi
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L799:
	xorl	%r9d, %r9d
	movl	$189, %r8d
	movl	$189, %edx
	xorl	%esi, %esi
	leaq	-246(%rbp), %rcx
	movq	%rbx, %rdi
	leaq	-1520(%rbp), %r13
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movq	-2048(%rbp), %rdx
	movq	-2008(%rbp), %rcx
	movq	%r13, %rdi
	movq	-2000(%rbp), %rsi
	call	_ZNK6icu_6714ResourceBundle11getStringExEPKcR10UErrorCode@PLT
	movq	-2032(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-1972(%rbp), %edx
	testl	%edx, %edx
	jg	.L767
	movq	-2032(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711FormattableC1ERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L782:
	xorl	%edi, %edi
	jmp	.L765
.L771:
	movl	-1764(%rbp), %ecx
	jmp	.L772
.L801:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3411:
	.size	_ZN6icu_6714Transliterator14getDisplayNameERKNS_13UnicodeStringERKNS_6LocaleERS1_, .-_ZN6icu_6714Transliterator14getDisplayNameERKNS_13UnicodeStringERKNS_6LocaleERS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator14getDisplayNameERKNS_13UnicodeStringERS1_
	.type	_ZN6icu_6714Transliterator14getDisplayNameERKNS_13UnicodeStringERS1_, @function
_ZN6icu_6714Transliterator14getDisplayNameERKNS_13UnicodeStringERS1_:
.LFB3410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714Transliterator14getDisplayNameERKNS_13UnicodeStringERKNS_6LocaleERS1_
	.cfi_endproc
.LFE3410:
	.size	_ZN6icu_6714Transliterator14getDisplayNameERKNS_13UnicodeStringERS1_, .-_ZN6icu_6714Transliterator14getDisplayNameERKNS_13UnicodeStringERS1_
	.align 2
	.p2align 4
	.type	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode.part.0, @function
_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode.part.0:
.LFB4702:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r15
	movl	%esi, %r14d
	movl	$2, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-128(%rbp), %rbx
	subq	$168, %rsp
	movq	%rdx, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%si, -120(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, -128(%rbp)
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	(%r12), %edi
	testl	%edi, %edi
	jle	.L855
.L854:
	xorl	%r14d, %r14d
.L807:
	movq	%r15, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L856
	addq	$168, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L855:
	.cfi_restore_state
	leaq	-128(%rbp), %rbx
	movq	%r15, %rcx
	movl	%r14d, %esi
	movq	%r13, %rdi
	leaq	-184(%rbp), %r8
	movq	%rbx, %rdx
	movq	$0, -184(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser15parseCompoundIDERKNS_13UnicodeStringEiRS1_RNS_7UVectorERPNS_10UnicodeSetE@PLT
	testb	%al, %al
	jne	.L808
	movq	-184(%rbp), %rdi
	movl	$65569, (%r12)
	testq	%rdi, %rdi
	je	.L854
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L808:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	-184(%rbp), %r13
	call	_ZN6icu_6722TransliteratorIDParser15instantiateListERNS_7UVectorER10UErrorCode@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L811
	cmpl	$1, -168(%rbp)
	jg	.L817
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L815
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L816:
	xorl	%edx, %edx
	movl	$59, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	jns	.L817
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L818
.L814:
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L857
.L811:
	testq	%r13, %r13
	je	.L854
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L817:
	movl	$104, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L814
	movq	-200(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722CompoundTransliteratorC1ERNS_7UVectorER11UParseErrorR10UErrorCode@PLT
.L818:
	leaq	8(%r14), %r12
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%dx, -186(%rbp)
	leaq	-186(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	16(%r14), %eax
	testw	%ax, %ax
	js	.L819
	movswl	%ax, %ecx
	sarl	$5, %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L852
	testl	%edx, %edx
	je	.L829
.L852:
	cmpl	%ecx, %edx
	jb	.L828
.L824:
	testq	%r13, %r13
	je	.L807
	movq	72(%r14), %rdi
	testq	%rdi, %rdi
	je	.L826
	movq	(%rdi), %rax
	call	*8(%rax)
.L826:
	movq	%r13, 72(%r14)
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L815:
	movl	-116(%rbp), %ecx
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L819:
	movl	20(%r14), %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L823
	testl	%edx, %edx
	je	.L829
.L823:
	cmpl	%edx, %ecx
	jbe	.L824
	cmpl	$1023, %edx
	jle	.L828
	orl	$-32, %eax
	movl	%edx, 20(%r14)
	movw	%ax, 16(%r14)
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L857:
	movl	$7, (%r12)
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L828:
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, 16(%r14)
	jmp	.L824
.L829:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L824
.L856:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4702:
	.size	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode.part.0, .-_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator13createInverseER10UErrorCode
	.type	_ZNK6icu_6714Transliterator13createInverseER10UErrorCode, @function
_ZNK6icu_6714Transliterator13createInverseER10UErrorCode:
.LFB3415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L858
	movq	%rsi, %r12
	leaq	-240(%rbp), %r14
	movl	$2, %esi
	movq	%rdi, %rbx
	movw	%si, -184(%rbp)
	movq	%r14, %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rsi
	movq	%rax, -192(%rbp)
	leaq	-192(%rbp), %r15
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	(%r12), %edi
	testl	%edi, %edi
	jle	.L909
.L908:
	xorl	%r13d, %r13d
.L861:
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L858:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L910
	addq	$216, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	.cfi_restore_state
	leaq	-192(%rbp), %r15
	leaq	8(%rbx), %rdi
	movq	%r14, %rcx
	movl	$1, %esi
	leaq	-248(%rbp), %r8
	movq	%r15, %rdx
	movq	$0, -248(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser15parseCompoundIDERKNS_13UnicodeStringEiRS1_RNS_7UVectorERPNS_10UnicodeSetE@PLT
	testb	%al, %al
	jne	.L862
	movq	-248(%rbp), %rdi
	movl	$65569, (%r12)
	testq	%rdi, %rdi
	je	.L908
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L862:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-248(%rbp), %rbx
	call	_ZN6icu_6722TransliteratorIDParser15instantiateListERNS_7UVectorER10UErrorCode@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L865
	cmpl	$1, -232(%rbp)
	jg	.L871
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L869
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L870:
	xorl	%edx, %edx
	movl	$59, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	jns	.L871
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L872
.L868:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L865
	movl	$7, (%r12)
	.p2align 4,,10
	.p2align 3
.L865:
	testq	%rbx, %rbx
	je	.L908
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L908
	.p2align 4,,10
	.p2align 3
.L871:
	movl	$104, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L868
	leaq	-128(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722CompoundTransliteratorC1ERNS_7UVectorER11UParseErrorR10UErrorCode@PLT
.L872:
	leaq	8(%r13), %r12
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%dx, -250(%rbp)
	leaq	-250(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	16(%r13), %eax
	testw	%ax, %ax
	js	.L873
	movswl	%ax, %ecx
	sarl	$5, %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L906
	testl	%edx, %edx
	je	.L883
.L906:
	cmpl	%ecx, %edx
	jb	.L882
.L878:
	testq	%rbx, %rbx
	je	.L861
	movq	72(%r13), %rdi
	testq	%rdi, %rdi
	je	.L880
	movq	(%rdi), %rax
	call	*8(%rax)
.L880:
	movq	%rbx, 72(%r13)
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L873:
	movl	20(%r13), %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L877
	testl	%edx, %edx
	je	.L883
.L877:
	cmpl	%edx, %ecx
	jbe	.L878
	cmpl	$1023, %edx
	jle	.L882
	orl	$-32, %eax
	movl	%edx, 20(%r13)
	movw	%ax, 16(%r13)
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L869:
	movl	-180(%rbp), %ecx
	jmp	.L870
.L882:
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, 16(%r13)
	jmp	.L878
.L883:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L878
.L910:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3415:
	.size	_ZNK6icu_6714Transliterator13createInverseER10UErrorCode, .-_ZNK6icu_6714Transliterator13createInverseER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR10UErrorCode
	.type	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR10UErrorCode, @function
_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR10UErrorCode:
.LFB3416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -260(%rbp)
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L911
	movl	$2, %esi
	movq	%rdx, %r12
	movq	%rdi, %r13
	leaq	-240(%rbp), %r14
	movw	%si, -184(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdx, %rsi
	movq	%r14, %rdi
	movq	%rax, -192(%rbp)
	leaq	-192(%rbp), %rbx
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	(%r12), %edi
	movl	-260(%rbp), %r9d
	testl	%edi, %edi
	jle	.L962
.L961:
	xorl	%r15d, %r15d
.L914:
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L911:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L963
	addq	$232, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L962:
	.cfi_restore_state
	leaq	-192(%rbp), %rbx
	movq	%r14, %rcx
	movl	%r9d, %esi
	movq	%r13, %rdi
	leaq	-248(%rbp), %r8
	movq	%rbx, %rdx
	movq	$0, -248(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser15parseCompoundIDERKNS_13UnicodeStringEiRS1_RNS_7UVectorERPNS_10UnicodeSetE@PLT
	testb	%al, %al
	jne	.L915
	movq	-248(%rbp), %rdi
	movl	$65569, (%r12)
	testq	%rdi, %rdi
	je	.L961
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L915:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-248(%rbp), %r13
	call	_ZN6icu_6722TransliteratorIDParser15instantiateListERNS_7UVectorER10UErrorCode@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L918
	cmpl	$1, -232(%rbp)
	jg	.L924
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L922
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L923:
	xorl	%edx, %edx
	movl	$59, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	jns	.L924
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L925
.L921:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L918
	movl	$7, (%r12)
	.p2align 4,,10
	.p2align 3
.L918:
	testq	%r13, %r13
	je	.L961
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L924:
	movl	$104, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L921
	leaq	-128(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722CompoundTransliteratorC1ERNS_7UVectorER11UParseErrorR10UErrorCode@PLT
.L925:
	leaq	8(%r15), %r12
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%dx, -250(%rbp)
	leaq	-250(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	16(%r15), %eax
	testw	%ax, %ax
	js	.L926
	movswl	%ax, %ecx
	sarl	$5, %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L959
	testl	%edx, %edx
	je	.L936
.L959:
	cmpl	%ecx, %edx
	jb	.L935
.L931:
	testq	%r13, %r13
	je	.L914
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.L933
	movq	(%rdi), %rax
	call	*8(%rax)
.L933:
	movq	%r13, 72(%r15)
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L926:
	movl	20(%r15), %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L930
	testl	%edx, %edx
	je	.L936
.L930:
	cmpl	%edx, %ecx
	jbe	.L931
	cmpl	$1023, %edx
	jle	.L935
	orl	$-32, %eax
	movl	%edx, 20(%r15)
	movw	%ax, 16(%r15)
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L922:
	movl	-180(%rbp), %ecx
	jmp	.L923
.L935:
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, 16(%r15)
	jmp	.L931
.L936:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L931
.L963:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3416:
	.size	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR10UErrorCode, .-_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode
	.type	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode, @function
_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode:
.LFB3417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -200(%rbp)
	movl	(%rcx), %r8d
	movq	%rdx, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L964
	movl	%esi, %r13d
	movl	$2, %esi
	leaq	-176(%rbp), %r15
	movq	%rcx, %r12
	movw	%si, -120(%rbp)
	movq	%r15, %rdi
	movq	%rcx, %rsi
	leaq	-128(%rbp), %rbx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	(%r12), %edi
	movq	-200(%rbp), %r10
	testl	%edi, %edi
	jle	.L1015
.L1014:
	xorl	%r14d, %r14d
.L967:
	movq	%r15, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L964:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1016
	addq	$168, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1015:
	.cfi_restore_state
	leaq	-128(%rbp), %rbx
	movq	%r15, %rcx
	movl	%r13d, %esi
	movq	%r10, %rdi
	leaq	-184(%rbp), %r8
	movq	%rbx, %rdx
	movq	$0, -184(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser15parseCompoundIDERKNS_13UnicodeStringEiRS1_RNS_7UVectorERPNS_10UnicodeSetE@PLT
	testb	%al, %al
	jne	.L968
	movq	-184(%rbp), %rdi
	movl	$65569, (%r12)
	testq	%rdi, %rdi
	je	.L1014
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L968:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	-184(%rbp), %r13
	call	_ZN6icu_6722TransliteratorIDParser15instantiateListERNS_7UVectorER10UErrorCode@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L971
	cmpl	$1, -168(%rbp)
	jg	.L977
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L975
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L976:
	xorl	%edx, %edx
	movl	$59, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	jns	.L977
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L978
.L974:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L971
	movl	$7, (%r12)
	.p2align 4,,10
	.p2align 3
.L971:
	testq	%r13, %r13
	je	.L1014
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L977:
	movl	$104, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L974
	movq	-208(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722CompoundTransliteratorC1ERNS_7UVectorER11UParseErrorR10UErrorCode@PLT
.L978:
	leaq	8(%r14), %r12
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%dx, -186(%rbp)
	leaq	-186(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	16(%r14), %eax
	testw	%ax, %ax
	js	.L979
	movswl	%ax, %ecx
	sarl	$5, %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L1012
	testl	%edx, %edx
	je	.L989
.L1012:
	cmpl	%ecx, %edx
	jb	.L988
.L984:
	testq	%r13, %r13
	je	.L967
	movq	72(%r14), %rdi
	testq	%rdi, %rdi
	je	.L986
	movq	(%rdi), %rax
	call	*8(%rax)
.L986:
	movq	%r13, 72(%r14)
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L979:
	movl	20(%r14), %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L983
	testl	%edx, %edx
	je	.L989
.L983:
	cmpl	%edx, %ecx
	jbe	.L984
	cmpl	$1023, %edx
	jle	.L988
	orl	$-32, %eax
	movl	%edx, 20(%r14)
	movw	%ax, 16(%r14)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L975:
	movl	-116(%rbp), %ecx
	jmp	.L976
.L988:
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, 16(%r14)
	jmp	.L984
.L989:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L984
.L1016:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3417:
	.size	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode, .-_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714Transliterator15createFromRulesERKNS_13UnicodeStringES3_15UTransDirectionR11UParseErrorR10UErrorCode
	.type	_ZN6icu_6714Transliterator15createFromRulesERKNS_13UnicodeStringES3_15UTransDirectionR11UParseErrorR10UErrorCode, @function
_ZN6icu_6714Transliterator15createFromRulesERKNS_13UnicodeStringES3_15UTransDirectionR11UParseErrorR10UErrorCode:
.LFB3421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%r8, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$904, %rsp
	movq	%rdi, -936(%rbp)
	movq	%rcx, -920(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-560(%rbp), %rax
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%rax, -928(%rbp)
	call	_ZN6icu_6720TransliteratorParserC1ER10UErrorCode@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6720TransliteratorParser5parseERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1099
	movl	-504(%rbp), %eax
	testl	%eax, %eax
	jne	.L1020
	movl	-544(%rbp), %eax
	testl	%eax, %eax
	je	.L1100
	cmpl	$1, %eax
	je	.L1101
.L1025:
	leaq	-864(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -912(%rbp)
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	-504(%rbp), %eax
	cmpl	%eax, -544(%rbp)
	movl	%eax, %ecx
	cmovge	-544(%rbp), %ecx
	movl	%ecx, -888(%rbp)
	testl	%ecx, %ecx
	jle	.L1062
	xorl	%r15d, %r15d
	movl	$1, %r12d
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1040:
	movl	-544(%rbp), %esi
	testl	%esi, %esi
	jne	.L1102
.L1047:
	addl	$1, %r15d
	cmpl	-888(%rbp), %r15d
	je	.L1028
.L1104:
	movl	-504(%rbp), %eax
.L1029:
	cmpl	%r15d, %eax
	jle	.L1040
	leaq	-512(%rbp), %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rdi
	movswl	8(%rax), %eax
	shrl	$5, %eax
	je	.L1040
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L1044
	movq	-920(%rbp), %rdx
	movq	%rbx, %rcx
	xorl	%esi, %esi
	call	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode.part.0
	movl	(%rbx), %edi
	movq	%rax, %r14
	testl	%edi, %edi
	jg	.L1103
	testq	%rax, %rax
	je	.L1040
	movq	(%rax), %r13
	movq	-8(%r13), %rax
	movq	8(%rax), %rdi
	cmpq	8+_ZTIN6icu_6718NullTransliteratorE(%rip), %rdi
	je	.L1045
	cmpb	$42, (%rdi)
	je	.L1046
	movq	8+_ZTIN6icu_6718NullTransliteratorE(%rip), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1045
.L1046:
	movq	-912(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	-544(%rbp), %esi
	testl	%esi, %esi
	je	.L1047
	.p2align 4,,10
	.p2align 3
.L1102:
	xorl	%esi, %esi
	leaq	-552(%rbp), %rdi
	leaq	-688(%rbp), %r14
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	movl	%r12d, %esi
	movq	%r14, %rdi
	leaq	-624(%rbp), %r13
	movq	%rax, -904(%rbp)
	leal	1(%r12), %eax
	leaq	-752(%rbp), %r12
	movl	%eax, -892(%rbp)
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	leaq	_ZN6icu_6722CompoundTransliterator11PASS_STRINGE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movl	$104, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1048
	movq	%rax, %rdi
	movl	$1, %ecx
	movq	%r13, %rsi
	addl	$1, %r15d
	movq	-904(%rbp), %r10
	movq	%rax, -904(%rbp)
	movq	%r10, %rdx
	call	_ZN6icu_6723RuleBasedTransliteratorC1ERKNS_13UnicodeStringEPNS_23TransliterationRuleDataEa@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-904(%rbp), %r8
	movq	-912(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r8, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	-892(%rbp), %r12d
	cmpl	-888(%rbp), %r15d
	jne	.L1104
.L1028:
	movl	$104, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1049
	movq	-920(%rbp), %rcx
	movq	-912(%rbp), %rsi
	movq	%rbx, %r8
	movq	%rax, %rdi
	leal	-1(%r12), %edx
	leaq	8(%r13), %r12
	call	_ZN6icu_6722CompoundTransliteratorC1ERNS_7UVectorEiR11UParseErrorR10UErrorCode@PLT
	movq	-936(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%ecx, %ecx
	leaq	-866(%rbp), %rsi
	xorl	%edx, %edx
	movw	%cx, -866(%rbp)
	movq	%r12, %rdi
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	16(%r13), %eax
	testw	%ax, %ax
	js	.L1050
	movswl	%ax, %edx
	sarl	$5, %edx
	leal	-1(%rdx), %ecx
	testb	$1, %al
	je	.L1105
.L1051:
	testl	%ecx, %ecx
	je	.L1106
.L1053:
	cmpl	%ecx, %edx
	jbe	.L1054
	cmpl	$1023, %ecx
	jle	.L1059
	orl	$-32, %eax
	movl	%ecx, 20(%r13)
	movw	%ax, 16(%r13)
.L1054:
	movq	-928(%rbp), %rdi
	call	_ZN6icu_6720TransliteratorParser20orphanCompoundFilterEv@PLT
	movq	72(%r13), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1056
	movq	(%rdi), %rax
	call	*8(%rax)
.L1056:
	movq	%rbx, 72(%r13)
	movq	-912(%rbp), %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1020:
	cmpl	$1, %eax
	jne	.L1025
	movl	-544(%rbp), %eax
	testl	%eax, %eax
	jne	.L1025
	movq	-472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1030
	leaq	-816(%rbp), %r12
	movl	$2, %r11d
	xorl	%edx, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rsi
	movw	%r11w, -808(%rbp)
	leaq	-752(%rbp), %r15
	movq	%rax, -816(%rbp)
	leaq	-688(%rbp), %r14
	call	_ZNK6icu_6710UnicodeSet9toPatternERNS_13UnicodeStringEa@PLT
	xorl	%esi, %esi
	leaq	-512(%rbp), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	$59, %esi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	leaq	-624(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rdi, -888(%rbp)
	xorl	%r13d, %r13d
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movl	(%rbx), %eax
	movq	-888(%rbp), %rdi
	testl	%eax, %eax
	jg	.L1031
	movq	-920(%rbp), %rdx
	movq	%rbx, %rcx
	xorl	%esi, %esi
	call	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode.part.0
	movq	-888(%rbp), %rdi
	movq	%rax, %r13
.L1031:
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1032:
	testq	%r13, %r13
	je	.L1022
	movq	-936(%rbp), %rsi
	leaq	8(%r13), %r12
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%r9d, %r9d
	movl	$1, %ecx
	xorl	%edx, %edx
	leaq	-864(%rbp), %rsi
	movq	%r12, %rdi
	movw	%r9w, -864(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	16(%r13), %eax
	testw	%ax, %ax
	js	.L1034
	movswl	%ax, %edx
	sarl	$5, %edx
	leal	-1(%rdx), %ecx
	testb	$1, %al
	jne	.L1035
	cmpl	%edx, %ecx
	jnb	.L1019
.L1060:
	andl	$31, %eax
	sall	$5, %ecx
	orl	%ecx, %eax
	movw	%ax, 16(%r13)
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	%r14, %rdi
	call	*8(%r13)
	jmp	.L1040
.L1049:
	movq	-912(%rbp), %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	cmpl	$0, (%rbx)
	jg	.L1019
.L1057:
	movl	$7, (%rbx)
.L1099:
	xorl	%r13d, %r13d
.L1019:
	movq	-928(%rbp), %rdi
	call	_ZN6icu_6720TransliteratorParserD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1107
	addq	$904, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1105:
	.cfi_restore_state
	cmpl	%edx, %ecx
	jnb	.L1054
.L1059:
	andl	$31, %eax
	sall	$5, %ecx
	orl	%ecx, %eax
	movw	%ax, 16(%r13)
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1050:
	movl	20(%r13), %edx
	leal	-1(%rdx), %ecx
	testb	$1, %al
	je	.L1053
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1100:
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1022
	movq	%rax, %rdi
	call	_ZN6icu_6718NullTransliteratorC1Ev@PLT
	jmp	.L1019
.L1101:
	leaq	-552(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	movl	$104, %edi
	movq	%rax, %r12
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1022
	movq	-936(%rbp), %rsi
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6723RuleBasedTransliteratorC1ERKNS_13UnicodeStringEPNS_23TransliterationRuleDataEa@PLT
	jmp	.L1019
.L1022:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L1099
	jmp	.L1057
.L1103:
	testq	%rax, %rax
	je	.L1044
	movq	(%rax), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	-912(%rbp), %rdi
	xorl	%r13d, %r13d
	call	_ZN6icu_677UVectorD1Ev@PLT
	jmp	.L1019
.L1034:
	movl	20(%r13), %edx
	leal	-1(%rdx), %ecx
	testb	$1, %al
	je	.L1037
.L1035:
	testl	%ecx, %ecx
	je	.L1108
.L1037:
	cmpl	%ecx, %edx
	jbe	.L1019
	cmpl	$1023, %ecx
	jle	.L1060
	orl	$-32, %eax
	movl	%ecx, 20(%r13)
	movw	%ax, 16(%r13)
	jmp	.L1019
.L1030:
	leaq	-512(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	(%rbx), %r10d
	movq	%rax, %rdi
	testl	%r10d, %r10d
	jg	.L1099
	movq	-920(%rbp), %rdx
	movq	%rbx, %rcx
	xorl	%esi, %esi
	call	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode.part.0
	movq	%rax, %r13
	jmp	.L1032
.L1106:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L1054
.L1062:
	movl	$1, %r12d
	jmp	.L1028
.L1108:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L1019
.L1048:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1044
	movl	$7, (%rbx)
	jmp	.L1044
.L1107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3421:
	.size	_ZN6icu_6714Transliterator15createFromRulesERKNS_13UnicodeStringES3_15UTransDirectionR11UParseErrorR10UErrorCode, .-_ZN6icu_6714Transliterator15createFromRulesERKNS_13UnicodeStringES3_15UTransDirectionR11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator12getSourceSetERNS_10UnicodeSetE
	.type	_ZNK6icu_6714Transliterator12getSourceSetERNS_10UnicodeSetE, @function
_ZNK6icu_6714Transliterator12getSourceSetERNS_10UnicodeSetE:
.LFB3425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*120(%rax)
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1111
	leaq	_ZTIN6icu_6713UnicodeFilterE(%rip), %rsi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6710UnicodeSetE(%rip), %rdx
	call	__dynamic_cast@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1112
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet9retainAllERKS0_@PLT
.L1111:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1112:
	.cfi_restore_state
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1111
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	72(%rbx), %rax
	movq	%r13, %rsi
	leaq	8(%rax), %rdi
	movq	8(%rax), %rax
	call	*40(%rax)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet9retainAllERKS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3425:
	.size	_ZNK6icu_6714Transliterator12getSourceSetERNS_10UnicodeSetE, .-_ZNK6icu_6714Transliterator12getSourceSetERNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE
	.type	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE, @function
_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE:
.LFB3426:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	jmp	_ZN6icu_6710UnicodeSet5clearEv@PLT
	.cfi_endproc
.LFE3426:
	.size	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE, .-_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE
	.type	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE, @function
_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE:
.LFB3427:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	jmp	_ZN6icu_6710UnicodeSet5clearEv@PLT
	.cfi_endproc
.LFE3427:
	.size	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE, .-_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE
	.weak	_ZTSN6icu_6714TransliteratorE
	.section	.rodata._ZTSN6icu_6714TransliteratorE,"aG",@progbits,_ZTSN6icu_6714TransliteratorE,comdat
	.align 16
	.type	_ZTSN6icu_6714TransliteratorE, @object
	.size	_ZTSN6icu_6714TransliteratorE, 26
_ZTSN6icu_6714TransliteratorE:
	.string	"N6icu_6714TransliteratorE"
	.weak	_ZTIN6icu_6714TransliteratorE
	.section	.data.rel.ro._ZTIN6icu_6714TransliteratorE,"awG",@progbits,_ZTIN6icu_6714TransliteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6714TransliteratorE, @object
	.size	_ZTIN6icu_6714TransliteratorE, 24
_ZTIN6icu_6714TransliteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714TransliteratorE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6714TransliteratorE
	.section	.data.rel.ro._ZTVN6icu_6714TransliteratorE,"awG",@progbits,_ZTVN6icu_6714TransliteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6714TransliteratorE, @object
	.size	_ZTVN6icu_6714TransliteratorE, 152
_ZTVN6icu_6714TransliteratorE:
	.quad	0
	.quad	_ZTIN6icu_6714TransliteratorE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6714Transliterator5cloneEv
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.quad	_ZNK6icu_6714Transliterator5getIDEv
	.quad	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE
	.local	_ZZN6icu_6714Transliterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6714Transliterator16getStaticClassIDEvE7classID,1,1
	.local	_ZL8registry
	.comm	_ZL8registry,8,8
	.local	_ZL13registryMutex
	.comm	_ZL13registryMutex,56,32
	.section	.rodata
	.align 16
	.type	_ZL17RB_RULE_BASED_IDS, @object
	.size	_ZL17RB_RULE_BASED_IDS, 27
_ZL17RB_RULE_BASED_IDS:
	.string	"RuleBasedTransliteratorIDs"
	.align 16
	.type	_ZL23RB_DISPLAY_NAME_PATTERN, @object
	.size	_ZL23RB_DISPLAY_NAME_PATTERN, 26
_ZL23RB_DISPLAY_NAME_PATTERN:
	.string	"TransliteratorNamePattern"
	.align 8
	.type	_ZL29RB_SCRIPT_DISPLAY_NAME_PREFIX, @object
	.size	_ZL29RB_SCRIPT_DISPLAY_NAME_PREFIX, 11
_ZL29RB_SCRIPT_DISPLAY_NAME_PREFIX:
	.string	"%Translit%"
	.align 8
	.type	_ZL22RB_DISPLAY_NAME_PREFIX, @object
	.size	_ZL22RB_DISPLAY_NAME_PREFIX, 12
_ZL22RB_DISPLAY_NAME_PREFIX:
	.string	"%Translit%%"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
