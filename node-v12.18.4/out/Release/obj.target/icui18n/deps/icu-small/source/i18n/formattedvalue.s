	.file	"formattedvalue.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724ConstrainedFieldPositionC2Ev
	.type	_ZN6icu_6724ConstrainedFieldPositionC2Ev, @function
_ZN6icu_6724ConstrainedFieldPositionC2Ev:
.LFB2419:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, (%rdi)
	movb	$0, 24(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE2419:
	.size	_ZN6icu_6724ConstrainedFieldPositionC2Ev, .-_ZN6icu_6724ConstrainedFieldPositionC2Ev
	.globl	_ZN6icu_6724ConstrainedFieldPositionC1Ev
	.set	_ZN6icu_6724ConstrainedFieldPositionC1Ev,_ZN6icu_6724ConstrainedFieldPositionC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724ConstrainedFieldPositionD2Ev
	.type	_ZN6icu_6724ConstrainedFieldPositionD2Ev, @function
_ZN6icu_6724ConstrainedFieldPositionD2Ev:
.LFB2422:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2422:
	.size	_ZN6icu_6724ConstrainedFieldPositionD2Ev, .-_ZN6icu_6724ConstrainedFieldPositionD2Ev
	.globl	_ZN6icu_6724ConstrainedFieldPositionD1Ev
	.set	_ZN6icu_6724ConstrainedFieldPositionD1Ev,_ZN6icu_6724ConstrainedFieldPositionD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724ConstrainedFieldPosition5resetEv
	.type	_ZN6icu_6724ConstrainedFieldPosition5resetEv, @function
_ZN6icu_6724ConstrainedFieldPosition5resetEv:
.LFB2424:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, (%rdi)
	movb	$0, 24(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE2424:
	.size	_ZN6icu_6724ConstrainedFieldPosition5resetEv, .-_ZN6icu_6724ConstrainedFieldPosition5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724ConstrainedFieldPosition17constrainCategoryEi
	.type	_ZN6icu_6724ConstrainedFieldPosition17constrainCategoryEi, @function
_ZN6icu_6724ConstrainedFieldPosition17constrainCategoryEi:
.LFB2425:
	.cfi_startproc
	endbr64
	movb	$1, 24(%rdi)
	movl	%esi, 20(%rdi)
	ret
	.cfi_endproc
.LFE2425:
	.size	_ZN6icu_6724ConstrainedFieldPosition17constrainCategoryEi, .-_ZN6icu_6724ConstrainedFieldPosition17constrainCategoryEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724ConstrainedFieldPosition14constrainFieldEii
	.type	_ZN6icu_6724ConstrainedFieldPosition14constrainFieldEii, @function
_ZN6icu_6724ConstrainedFieldPosition14constrainFieldEii:
.LFB2426:
	.cfi_startproc
	endbr64
	movb	$2, 24(%rdi)
	movl	%esi, 20(%rdi)
	movl	%edx, 8(%rdi)
	ret
	.cfi_endproc
.LFE2426:
	.size	_ZN6icu_6724ConstrainedFieldPosition14constrainFieldEii, .-_ZN6icu_6724ConstrainedFieldPosition14constrainFieldEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724ConstrainedFieldPosition24setInt64IterationContextEl
	.type	_ZN6icu_6724ConstrainedFieldPosition24setInt64IterationContextEl, @function
_ZN6icu_6724ConstrainedFieldPosition24setInt64IterationContextEl:
.LFB2427:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE2427:
	.size	_ZN6icu_6724ConstrainedFieldPosition24setInt64IterationContextEl, .-_ZN6icu_6724ConstrainedFieldPosition24setInt64IterationContextEl
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB0:
	.text
.LHOTB0:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724ConstrainedFieldPosition12matchesFieldEii
	.type	_ZNK6icu_6724ConstrainedFieldPosition12matchesFieldEii, @function
_ZNK6icu_6724ConstrainedFieldPosition12matchesFieldEii:
.LFB2428:
	.cfi_startproc
	endbr64
	movzbl	24(%rdi), %eax
	cmpb	$1, %al
	je	.L9
	cmpb	$2, %al
	jne	.L21
	xorl	%r8d, %r8d
	cmpl	20(%rdi), %esi
	je	.L22
.L8:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$1, %r8d
	testb	%al, %al
	je	.L8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	cmpl	%esi, 20(%rdi)
	sete	%r8b
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	cmpl	%edx, 8(%rdi)
	sete	%r8b
	movl	%r8d, %eax
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_6724ConstrainedFieldPosition12matchesFieldEii.cold, @function
_ZNK6icu_6724ConstrainedFieldPosition12matchesFieldEii.cold:
.LFSB2428:
.L17:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE2428:
	.text
	.size	_ZNK6icu_6724ConstrainedFieldPosition12matchesFieldEii, .-_ZNK6icu_6724ConstrainedFieldPosition12matchesFieldEii
	.section	.text.unlikely
	.size	_ZNK6icu_6724ConstrainedFieldPosition12matchesFieldEii.cold, .-_ZNK6icu_6724ConstrainedFieldPosition12matchesFieldEii.cold
.LCOLDE0:
	.text
.LHOTE0:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724ConstrainedFieldPosition8setStateEiiii
	.type	_ZN6icu_6724ConstrainedFieldPosition8setStateEiiii, @function
_ZN6icu_6724ConstrainedFieldPosition8setStateEiiii:
.LFB2429:
	.cfi_startproc
	endbr64
	movd	%r8d, %xmm1
	movd	%esi, %xmm2
	movd	%edx, %xmm0
	movd	%ecx, %xmm3
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE2429:
	.size	_ZN6icu_6724ConstrainedFieldPosition8setStateEiiii, .-_ZN6icu_6724ConstrainedFieldPosition8setStateEiiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714FormattedValueD2Ev
	.type	_ZN6icu_6714FormattedValueD2Ev, @function
_ZN6icu_6714FormattedValueD2Ev:
.LFB2431:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2431:
	.size	_ZN6icu_6714FormattedValueD2Ev, .-_ZN6icu_6714FormattedValueD2Ev
	.globl	_ZN6icu_6714FormattedValueD1Ev
	.set	_ZN6icu_6714FormattedValueD1Ev,_ZN6icu_6714FormattedValueD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714FormattedValueD0Ev
	.type	_ZN6icu_6714FormattedValueD0Ev, @function
_ZN6icu_6714FormattedValueD0Ev:
.LFB2433:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE2433:
	.size	_ZN6icu_6714FormattedValueD0Ev, .-_ZN6icu_6714FormattedValueD0Ev
	.p2align 4
	.globl	ucfpos_open_67
	.type	ucfpos_open_67, @function
ucfpos_open_67:
.LFB2434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$40, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L27
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rax)
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	movl	$1430472192, (%rax)
	movq	$0, 8(%rax)
	movups	%xmm0, 16(%rax)
.L26:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L27:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L26
	.cfi_endproc
.LFE2434:
	.size	ucfpos_open_67, .-ucfpos_open_67
	.p2align 4
	.globl	ucfpos_reset_67
	.type	ucfpos_reset_67, @function
ucfpos_reset_67:
.LFB2441:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L30
	testq	%rdi, %rdi
	je	.L35
	cmpl	$1430472192, (%rdi)
	jne	.L36
	movq	$0, 8(%rdi)
	pxor	%xmm0, %xmm0
	movb	$0, 32(%rdi)
	movups	%xmm0, 16(%rdi)
.L30:
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$3, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$1, (%rsi)
	ret
	.cfi_endproc
.LFE2441:
	.size	ucfpos_reset_67, .-ucfpos_reset_67
	.p2align 4
	.globl	ucfpos_constrainCategory_67
	.type	ucfpos_constrainCategory_67, @function
ucfpos_constrainCategory_67:
.LFB2442:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L37
	testq	%rdi, %rdi
	je	.L42
	cmpl	$1430472192, (%rdi)
	jne	.L43
	movb	$1, 32(%rdi)
	movl	%esi, 28(%rdi)
.L37:
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$3, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$1, (%rdx)
	ret
	.cfi_endproc
.LFE2442:
	.size	ucfpos_constrainCategory_67, .-ucfpos_constrainCategory_67
	.p2align 4
	.globl	ucfpos_constrainField_67
	.type	ucfpos_constrainField_67, @function
ucfpos_constrainField_67:
.LFB2443:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L44
	testq	%rdi, %rdi
	je	.L49
	cmpl	$1430472192, (%rdi)
	jne	.L50
	movb	$2, 32(%rdi)
	movl	%esi, 28(%rdi)
	movl	%edx, 16(%rdi)
.L44:
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$3, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$1, (%rcx)
	ret
	.cfi_endproc
.LFE2443:
	.size	ucfpos_constrainField_67, .-ucfpos_constrainField_67
	.p2align 4
	.globl	ucfpos_getCategory_67
	.type	ucfpos_getCategory_67, @function
ucfpos_getCategory_67:
.LFB2444:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L52
	testq	%rdi, %rdi
	je	.L56
	cmpl	$1430472192, (%rdi)
	jne	.L57
	movl	28(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$3, (%rsi)
.L52:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$1, (%rsi)
	jmp	.L52
	.cfi_endproc
.LFE2444:
	.size	ucfpos_getCategory_67, .-ucfpos_getCategory_67
	.p2align 4
	.globl	ucfpos_getField_67
	.type	ucfpos_getField_67, @function
ucfpos_getField_67:
.LFB2445:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L59
	testq	%rdi, %rdi
	je	.L63
	cmpl	$1430472192, (%rdi)
	jne	.L64
	movl	16(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$3, (%rsi)
.L59:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$1, (%rsi)
	jmp	.L59
	.cfi_endproc
.LFE2445:
	.size	ucfpos_getField_67, .-ucfpos_getField_67
	.p2align 4
	.globl	ucfpos_getIndexes_67
	.type	ucfpos_getIndexes_67, @function
ucfpos_getIndexes_67:
.LFB2446:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L65
	testq	%rdi, %rdi
	je	.L70
	cmpl	$1430472192, (%rdi)
	jne	.L71
	movl	20(%rdi), %eax
	movl	%eax, (%rsi)
	movl	24(%rdi), %eax
	movl	%eax, (%rdx)
.L65:
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$3, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$1, (%rcx)
	ret
	.cfi_endproc
.LFE2446:
	.size	ucfpos_getIndexes_67, .-ucfpos_getIndexes_67
	.p2align 4
	.globl	ucfpos_getInt64IterationContext_67
	.type	ucfpos_getInt64IterationContext_67, @function
ucfpos_getInt64IterationContext_67:
.LFB2447:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L73
	testq	%rdi, %rdi
	je	.L77
	cmpl	$1430472192, (%rdi)
	jne	.L78
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$3, (%rsi)
.L73:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$1, (%rsi)
	jmp	.L73
	.cfi_endproc
.LFE2447:
	.size	ucfpos_getInt64IterationContext_67, .-ucfpos_getInt64IterationContext_67
	.p2align 4
	.globl	ucfpos_setInt64IterationContext_67
	.type	ucfpos_setInt64IterationContext_67, @function
ucfpos_setInt64IterationContext_67:
.LFB2448:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L79
	testq	%rdi, %rdi
	je	.L84
	cmpl	$1430472192, (%rdi)
	jne	.L85
	movq	%rsi, 8(%rdi)
.L79:
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$3, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$1, (%rdx)
	ret
	.cfi_endproc
.LFE2448:
	.size	ucfpos_setInt64IterationContext_67, .-ucfpos_setInt64IterationContext_67
	.section	.text.unlikely
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4
	.globl	ucfpos_matchesField_67
	.type	ucfpos_matchesField_67, @function
ucfpos_matchesField_67:
.LFB2449:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L87
	testq	%rdi, %rdi
	je	.L103
	cmpl	$1430472192, (%rdi)
	jne	.L104
	movzbl	32(%rdi), %ecx
	cmpb	$1, %cl
	je	.L92
	cmpb	$2, %cl
	jne	.L105
	xorl	%eax, %eax
	cmpl	28(%rdi), %esi
	je	.L106
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$1, (%rcx)
.L87:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	movl	$3, (%rcx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%cl, %cl
	jne	.L98
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore 6
	cmpl	%esi, 28(%rdi)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	cmpl	%edx, 16(%rdi)
	sete	%al
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	ucfpos_matchesField_67.cold, @function
ucfpos_matchesField_67.cold:
.LFSB2449:
.L98:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE2449:
	.text
	.size	ucfpos_matchesField_67, .-ucfpos_matchesField_67
	.section	.text.unlikely
	.size	ucfpos_matchesField_67.cold, .-ucfpos_matchesField_67.cold
.LCOLDE1:
	.text
.LHOTE1:
	.p2align 4
	.globl	ucfpos_setState_67
	.type	ucfpos_setState_67, @function
ucfpos_setState_67:
.LFB2450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%esi, -4(%rbp)
	movl	(%r9), %eax
	movl	%edx, -8(%rbp)
	movl	%ecx, -12(%rbp)
	movl	%r8d, -16(%rbp)
	testl	%eax, %eax
	jg	.L107
	testq	%rdi, %rdi
	je	.L113
	cmpl	$1430472192, (%rdi)
	jne	.L114
	movd	%r8d, %xmm1
	movd	%esi, %xmm2
	movd	%edx, %xmm0
	movd	%ecx, %xmm3
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%rdi)
.L107:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movl	$3, (%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movl	$1, (%r9)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2450:
	.size	ucfpos_setState_67, .-ucfpos_setState_67
	.p2align 4
	.globl	ucfpos_close_67
	.type	ucfpos_close_67, @function
ucfpos_close_67:
.LFB2451:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L115
	cmpl	$1430472192, (%rdi)
	jne	.L115
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L115:
	ret
	.cfi_endproc
.LFE2451:
	.size	ucfpos_close_67, .-ucfpos_close_67
	.p2align 4
	.globl	ufmtval_nextPosition_67
	.type	ufmtval_nextPosition_67, @function
ufmtval_nextPosition_67:
.LFB2456:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L129
	testq	%rdi, %rdi
	je	.L127
	cmpl	$1430672896, (%rdi)
	jne	.L131
	testq	%rsi, %rsi
	je	.L127
	cmpl	$1430472192, (%rsi)
	jne	.L131
	movq	8(%rdi), %rdi
	addq	$8, %rsi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L131:
	movl	$3, (%rdx)
.L129:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	movl	$1, (%rdx)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2456:
	.size	ufmtval_nextPosition_67, .-ufmtval_nextPosition_67
	.p2align 4
	.globl	ufmtval_getString_67
	.type	ufmtval_getString_67, @function
ufmtval_getString_67:
.LFB2455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L133
	movq	%rdx, %rbx
	testq	%rdi, %rdi
	je	.L151
	cmpl	$1430672896, (%rdi)
	jne	.L152
	movq	%rsi, %r12
	movq	8(%rdi), %rsi
	leaq	-112(%rbp), %r13
	movq	%r13, %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L141
	movzwl	-104(%rbp), %eax
	testq	%r12, %r12
	je	.L136
	testw	%ax, %ax
	js	.L137
	movswl	%ax, %edx
	sarl	$5, %edx
.L138:
	movl	%edx, (%r12)
.L136:
	testb	$17, %al
	jne	.L141
	leaq	-102(%rbp), %r12
	testb	$2, %al
	cmove	-88(%rbp), %r12
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L141:
	xorl	%r12d, %r12d
.L140:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L132:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movl	$3, (%rdx)
.L133:
	xorl	%r12d, %r12d
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L137:
	movl	-100(%rbp), %edx
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$1, (%rdx)
	jmp	.L133
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2455:
	.size	ufmtval_getString_67, .-ufmtval_getString_67
	.weak	_ZTSN6icu_6714FormattedValueE
	.section	.rodata._ZTSN6icu_6714FormattedValueE,"aG",@progbits,_ZTSN6icu_6714FormattedValueE,comdat
	.align 16
	.type	_ZTSN6icu_6714FormattedValueE, @object
	.size	_ZTSN6icu_6714FormattedValueE, 26
_ZTSN6icu_6714FormattedValueE:
	.string	"N6icu_6714FormattedValueE"
	.weak	_ZTIN6icu_6714FormattedValueE
	.section	.data.rel.ro._ZTIN6icu_6714FormattedValueE,"awG",@progbits,_ZTIN6icu_6714FormattedValueE,comdat
	.align 8
	.type	_ZTIN6icu_6714FormattedValueE, @object
	.size	_ZTIN6icu_6714FormattedValueE, 16
_ZTIN6icu_6714FormattedValueE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_6714FormattedValueE
	.weak	_ZTVN6icu_6714FormattedValueE
	.section	.data.rel.ro._ZTVN6icu_6714FormattedValueE,"awG",@progbits,_ZTVN6icu_6714FormattedValueE,comdat
	.align 8
	.type	_ZTVN6icu_6714FormattedValueE, @object
	.size	_ZTVN6icu_6714FormattedValueE, 64
_ZTVN6icu_6714FormattedValueE:
	.quad	0
	.quad	_ZTIN6icu_6714FormattedValueE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
