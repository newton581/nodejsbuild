	.file	"ulistformatter.cpp"
	.text
	.p2align 4
	.type	_ZL17getUnicodeStringsPKPKDsPKiiPN6icu_6713UnicodeStringERNS5_10LocalArrayIS6_EER10UErrorCode.constprop.0, @function
_ZL17getUnicodeStringsPKPKDsPKiiPN6icu_6713UnicodeStringERNS5_10LocalArrayIS6_EER10UErrorCode.constprop.0:
.LFB3415:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L2
	movq	%rdi, %r12
	movq	%rsi, %r13
	movl	%edx, %ebx
	movq	%r8, %r14
	testq	%rdi, %rdi
	jne	.L3
	testl	%edx, %edx
	jg	.L2
.L3:
	cmpl	$4, %ebx
	jg	.L53
	movq	%rcx, -80(%rbp)
	testq	%r13, %r13
	je	.L54
	testl	%ebx, %ebx
	je	.L1
.L16:
	movq	-80(%rbp), %rdi
	xorl	%r14d, %r14d
	leaq	-64(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L19:
	movl	0(%r13,%r14,4), %ecx
	movq	(%r12,%r14,8), %rax
	movq	%r15, %rdx
	movq	%rdi, -72(%rbp)
	movl	%ecx, %esi
	movq	%rax, -64(%rbp)
	shrl	$31, %esi
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	-72(%rbp), %rdi
	addq	$1, %r14
	addq	$64, %rdi
	cmpl	%r14d, %ebx
	jg	.L19
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	movq	-80(%rbp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movslq	%ebx, %rdx
	movq	%rdx, %rcx
	movq	%rdx, -80(%rbp)
	salq	$6, %rcx
	leaq	8(%rcx), %rdi
	movq	%rcx, -72(%rbp)
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L6
	movq	-80(%rbp), %rdx
	addq	$8, %rax
	movq	-72(%rbp), %rcx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rsi
	movq	%rdx, -8(%rax)
	subq	$1, %rdx
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$2, %edi
	subq	$1, %rdx
	movq	%rsi, (%rax)
	addq	$64, %rax
	movw	%di, -56(%rax)
	cmpq	$-1, %rdx
	jne	.L8
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L56
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.L57
	movq	-8(%rdx), %rax
	salq	$6, %rax
	addq	%rdx, %rax
	cmpq	%rdx, %rax
	je	.L58
	.p2align 4,,10
	.p2align 3
.L9:
	movq	-64(%rax), %rdx
	movq	%rax, -88(%rbp)
	subq	$64, %rax
	movq	%rax, -72(%rbp)
	movq	%rax, %rdi
	call	*(%rdx)
	movq	-72(%rbp), %rax
	cmpq	%rax, (%r14)
	jne	.L9
	movq	-88(%rbp), %rdi
	subq	$72, %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
	movq	-80(%rbp), %rax
	movq	%rax, (%r14)
	testq	%rax, %rax
	jne	.L51
.L10:
	movl	$7, (%r15)
.L12:
	movq	$0, -80(%rbp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$1, (%r15)
	movq	$0, -80(%rbp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L56:
	addq	-80(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L13:
	movq	-64(%rcx), %rdx
	subq	$64, %rcx
	movq	%rcx, -72(%rbp)
	movq	%rcx, %rdi
	call	*(%rdx)
	movq	-72(%rbp), %rcx
	cmpq	%rcx, -80(%rbp)
	jne	.L13
	movq	-88(%rbp), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L51:
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L12
.L24:
	movq	(%r14), %rax
	movq	%rax, -80(%rbp)
	movq	%rax, %r14
	testq	%r13, %r13
	jne	.L16
.L15:
	xorl	%r13d, %r13d
	leaq	-64(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%r12,%r13,8), %rax
	movl	$-1, %ecx
	movq	%r15, %rdx
	movq	%r14, %rdi
	movl	$1, %esi
	movq	%rax, -64(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	addq	$1, %r13
	addq	$64, %r14
	cmpl	%r13d, %ebx
	jg	.L18
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L54:
	testl	%ebx, %ebx
	je	.L1
	movq	%rcx, %r14
	jmp	.L15
.L57:
	movq	-80(%rbp), %rax
	movq	%rax, (%r14)
	jmp	.L24
.L58:
	leaq	-8(%rax), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
	movq	-80(%rbp), %rax
	movq	%rax, (%r14)
	jmp	.L51
.L55:
	call	__stack_chk_fail@PLT
.L6:
	cmpl	$0, (%r15)
	jg	.L12
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.L10
	movq	-8(%rdx), %rax
	salq	$6, %rax
	addq	%rdx, %rax
	cmpq	%rdx, %rax
	je	.L27
	movq	$0, -80(%rbp)
	jmp	.L9
.L27:
	leaq	-8(%rax), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
	movq	$0, (%r14)
	jmp	.L10
	.cfi_endproc
.LFE3415:
	.size	_ZL17getUnicodeStringsPKPKDsPKiiPN6icu_6713UnicodeStringERNS5_10LocalArrayIS6_EER10UErrorCode.constprop.0, .-_ZL17getUnicodeStringsPKPKDsPKiiPN6icu_6713UnicodeStringERNS5_10LocalArrayIS6_EER10UErrorCode.constprop.0
	.p2align 4
	.globl	ulistfmt_close_67
	.type	ulistfmt_close_67, @function
ulistfmt_close_67:
.LFB2515:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L59
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L59:
	ret
	.cfi_endproc
.LFE2515:
	.size	ulistfmt_close_67, .-ulistfmt_close_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UFormattedListImplC2Ev
	.type	_ZN6icu_6718UFormattedListImplC2Ev, @function
_ZN6icu_6718UFormattedListImplC2Ev:
.LFB2529:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713FormattedListE(%rip), %rax
	movl	$1430672896, (%rdi)
	movq	%rax, 24(%rdi)
	leaq	24(%rdi), %rax
	movl	$1179407188, 16(%rdi)
	movq	$0, 32(%rdi)
	movl	$27, 40(%rdi)
	movq	%rax, 8(%rdi)
	ret
	.cfi_endproc
.LFE2529:
	.size	_ZN6icu_6718UFormattedListImplC2Ev, .-_ZN6icu_6718UFormattedListImplC2Ev
	.globl	_ZN6icu_6718UFormattedListImplC1Ev
	.set	_ZN6icu_6718UFormattedListImplC1Ev,_ZN6icu_6718UFormattedListImplC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UFormattedListImplD2Ev
	.type	_ZN6icu_6718UFormattedListImplD2Ev, @function
_ZN6icu_6718UFormattedListImplD2Ev:
.LFB2532:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN6icu_6713FormattedListD1Ev@PLT
	.cfi_endproc
.LFE2532:
	.size	_ZN6icu_6718UFormattedListImplD2Ev, .-_ZN6icu_6718UFormattedListImplD2Ev
	.globl	_ZN6icu_6718UFormattedListImplD1Ev
	.set	_ZN6icu_6718UFormattedListImplD1Ev,_ZN6icu_6718UFormattedListImplD2Ev
	.p2align 4
	.globl	ulistfmt_openResult_67
	.type	ulistfmt_openResult_67, @function
ulistfmt_openResult_67:
.LFB2534:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L68
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L65
	leaq	16+_ZTVN6icu_6713FormattedListE(%rip), %rcx
	leaq	24(%rax), %rdx
	movl	$1430672896, (%rax)
	movl	$1179407188, 16(%rax)
	movq	%rcx, 24(%rax)
	movq	$0, 32(%rax)
	movl	$27, 40(%rax)
	movq	%rdx, 8(%rax)
.L63:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L65:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L63
	.cfi_endproc
.LFE2534:
	.size	ulistfmt_openResult_67, .-ulistfmt_openResult_67
	.p2align 4
	.globl	ulistfmt_resultAsValue_67
	.type	ulistfmt_resultAsValue_67, @function
ulistfmt_resultAsValue_67:
.LFB2535:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movq	%rdi, %rax
	testl	%edx, %edx
	jg	.L72
	testq	%rdi, %rdi
	je	.L76
	cmpl	$1179407188, 16(%rdi)
	jne	.L77
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$3, (%rsi)
.L72:
	xorl	%eax, %eax
.L78:
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	jmp	.L78
	.cfi_endproc
.LFE2535:
	.size	ulistfmt_resultAsValue_67, .-ulistfmt_resultAsValue_67
	.p2align 4
	.globl	ulistfmt_closeResult_67
	.type	ulistfmt_closeResult_67, @function
ulistfmt_closeResult_67:
.LFB2536:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L85
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$1179407188, 16(%rdi)
	jne	.L79
	leaq	24(%rdi), %rdi
	call	_ZN6icu_6713FormattedListD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE2536:
	.size	ulistfmt_closeResult_67, .-ulistfmt_closeResult_67
	.p2align 4
	.globl	ulistfmt_format_67
	.type	ulistfmt_format_67, @function
ulistfmt_format_67:
.LFB2538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r14
	movq	%rdi, -416(%rbp)
	movl	(%r14), %r11d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r11d, %r11d
	jg	.L100
	movl	%r9d, %eax
	movq	%rsi, %rdi
	movl	%ecx, %r11d
	movq	%rdx, %rsi
	movq	%r8, %r12
	movl	%r9d, %r13d
	shrl	$31, %eax
	testq	%r8, %r8
	je	.L111
.L91:
	testb	%al, %al
	jne	.L112
	movl	$2, %edx
	movl	$2, %ecx
	movl	$2, %r8d
	movl	$2, %r9d
	leaq	-320(%rbp), %rbx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r15
	movw	%dx, -312(%rbp)
	movl	%r11d, %edx
	movw	%cx, -248(%rbp)
	movq	%rbx, %rcx
	movw	%r8w, -184(%rbp)
	leaq	-400(%rbp), %r8
	movw	%r9w, -120(%rbp)
	movq	%r14, %r9
	movq	%r15, -320(%rbp)
	movq	%r15, -256(%rbp)
	movq	%r15, -192(%rbp)
	movq	%r15, -128(%rbp)
	movq	$0, -400(%rbp)
	movl	%r11d, -420(%rbp)
	call	_ZL17getUnicodeStringsPKPKDsPKiiPN6icu_6713UnicodeStringERNS5_10LocalArrayIS6_EER10UErrorCode.constprop.0
	movl	(%r14), %r10d
	movl	$-1, -408(%rbp)
	movq	%rax, %r9
	testl	%r10d, %r10d
	jg	.L93
	movl	$2, %eax
	testq	%r12, %r12
	movq	%r15, -384(%rbp)
	movl	-420(%rbp), %r11d
	movw	%ax, -376(%rbp)
	leaq	-384(%rbp), %r15
	je	.L94
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r9, -408(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movl	-420(%rbp), %r11d
	movq	-408(%rbp), %r9
.L94:
	movq	-416(%rbp), %rdi
	movl	%r11d, %edx
	movq	%r9, %rsi
	movq	%r14, %r8
	movq	%r15, %rcx
	call	_ZNK6icu_6713ListFormatter6formatEPKNS_13UnicodeStringEiRS1_R10UErrorCode@PLT
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r15, %rdi
	leaq	-392(%rbp), %rsi
	movq	%r12, -392(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -408(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L93:
	movq	-400(%rbp), %rax
	testq	%rax, %rax
	je	.L95
	movq	-8(%rax), %r12
	salq	$6, %r12
	addq	%rax, %r12
	cmpq	%r12, %rax
	je	.L96
	.p2align 4,,10
	.p2align 3
.L97:
	movq	-64(%r12), %rax
	subq	$64, %r12
	movq	%r12, %rdi
	call	*(%rax)
	cmpq	%r12, -400(%rbp)
	jne	.L97
.L96:
	leaq	-8(%r12), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L95:
	leaq	-64(%rbp), %r12
.L98:
	movq	-64(%r12), %rax
	subq	$64, %r12
	movq	%r12, %rdi
	call	*(%rax)
	cmpq	%rbx, %r12
	jne	.L98
.L88:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	movl	-408(%rbp), %eax
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	testl	%r9d, %r9d
	setne	%al
	jmp	.L91
.L100:
	movl	$-1, -408(%rbp)
	jmp	.L88
.L112:
	movl	$1, (%r14)
	movl	$-1, -408(%rbp)
	jmp	.L88
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2538:
	.size	ulistfmt_format_67, .-ulistfmt_format_67
	.p2align 4
	.globl	ulistfmt_formatStringsToResult_67
	.type	ulistfmt_formatStringsToResult_67, @function
ulistfmt_formatStringsToResult_67:
.LFB2539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r11d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r11d, %r11d
	jg	.L114
	movq	%r8, %r13
	movq	%r9, %rbx
	testq	%r8, %r8
	je	.L138
	cmpl	$1179407188, 16(%r8)
	jne	.L139
	movl	%ecx, %r14d
	movl	$2, %r8d
	movl	$2, %ecx
	movq	%rdi, %r15
	movl	$2, %r9d
	movq	%rsi, %rdi
	leaq	-320(%rbp), %r12
	movq	%rdx, %rsi
	movl	$2, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, -248(%rbp)
	movq	%r12, %rcx
	movw	%dx, -312(%rbp)
	movl	%r14d, %edx
	movw	%r8w, -184(%rbp)
	leaq	-360(%rbp), %r8
	movw	%r9w, -120(%rbp)
	movq	%rbx, %r9
	movq	%rax, -320(%rbp)
	movq	%rax, -256(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -360(%rbp)
	call	_ZL17getUnicodeStringsPKPKDsPKiiPN6icu_6713UnicodeStringERNS5_10LocalArrayIS6_EER10UErrorCode.constprop.0
	movl	(%rbx), %r10d
	movq	%rax, %rdx
	testl	%r10d, %r10d
	jg	.L140
	leaq	-352(%rbp), %r9
	movq	%rbx, %r8
	movl	%r14d, %ecx
	movq	%r15, %rsi
	movq	%r9, %rdi
	movq	%r9, -376(%rbp)
	call	_ZNK6icu_6713ListFormatter20formatStringsToValueEPKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	-376(%rbp), %r9
	leaq	24(%r13), %rdi
	movq	%r9, %rsi
	call	_ZN6icu_6713FormattedListaSEOS0_@PLT
	movq	-376(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713FormattedListD1Ev@PLT
	movq	-360(%rbp), %rax
	testq	%rax, %rax
	je	.L123
	movq	-8(%rax), %rbx
	salq	$6, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L124
	.p2align 4,,10
	.p2align 3
.L125:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, -360(%rbp)
	jne	.L125
.L124:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L123:
	leaq	-64(%rbp), %rbx
.L127:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L127
.L114:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L141
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	-360(%rbp), %rax
	testq	%rax, %rax
	je	.L119
	movq	-8(%rax), %rbx
	salq	$6, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L120
	.p2align 4,,10
	.p2align 3
.L121:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, -360(%rbp)
	jne	.L121
.L120:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L119:
	leaq	-64(%rbp), %rbx
.L122:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L122
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L139:
	movl	$3, (%r9)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L138:
	movl	$1, (%r9)
	jmp	.L114
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2539:
	.size	ulistfmt_formatStringsToResult_67, .-ulistfmt_formatStringsToResult_67
	.p2align 4
	.globl	ulistfmt_open_67
	.type	ulistfmt_open_67, @function
ulistfmt_open_67:
.LFB2511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L151
	leaq	-272(%rbp), %r13
	movq	%rsi, %rbx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L152
.L142:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L151
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L151:
	xorl	%r12d, %r12d
	jmp	.L142
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2511:
	.size	ulistfmt_open_67, .-ulistfmt_open_67
	.p2align 4
	.globl	ulistfmt_openForType_67
	.type	ulistfmt_openForType_67, @function
ulistfmt_openForType_67:
.LFB2514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$240, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L163
	leaq	-272(%rbp), %r14
	movl	%esi, %r12d
	movq	%rcx, %rbx
	xorl	%r8d, %r8d
	movq	%rdi, %rsi
	movl	%edx, %r13d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	%r12d, %esi
	movq	%rbx, %rcx
	movl	%r13d, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleE18UListFormatterType19UListFormatterWidthR10UErrorCode@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L164
.L154:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	addq	$240, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L163
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L163:
	xorl	%r12d, %r12d
	jmp	.L154
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2514:
	.size	ulistfmt_openForType_67, .-ulistfmt_openForType_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
