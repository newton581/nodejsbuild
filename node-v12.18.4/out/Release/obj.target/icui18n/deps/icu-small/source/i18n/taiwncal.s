	.file	"taiwncal.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TaiwanCalendar17getDynamicClassIDEv
	.type	_ZNK6icu_6714TaiwanCalendar17getDynamicClassIDEv, @function
_ZNK6icu_6714TaiwanCalendar17getDynamicClassIDEv:
.LFB2971:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714TaiwanCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2971:
	.size	_ZNK6icu_6714TaiwanCalendar17getDynamicClassIDEv, .-_ZNK6icu_6714TaiwanCalendar17getDynamicClassIDEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"roc"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TaiwanCalendar7getTypeEv
	.type	_ZNK6icu_6714TaiwanCalendar7getTypeEv, @function
_ZNK6icu_6714TaiwanCalendar7getTypeEv:
.LFB2984:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE2984:
	.size	_ZNK6icu_6714TaiwanCalendar7getTypeEv, .-_ZNK6icu_6714TaiwanCalendar7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TaiwanCalendar18haveDefaultCenturyEv
	.type	_ZNK6icu_6714TaiwanCalendar18haveDefaultCenturyEv, @function
_ZNK6icu_6714TaiwanCalendar18haveDefaultCenturyEv:
.LFB2988:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2988:
	.size	_ZNK6icu_6714TaiwanCalendar18haveDefaultCenturyEv, .-_ZNK6icu_6714TaiwanCalendar18haveDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TaiwanCalendarD2Ev
	.type	_ZN6icu_6714TaiwanCalendarD2Ev, @function
_ZN6icu_6714TaiwanCalendarD2Ev:
.LFB2976:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714TaiwanCalendarE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717GregorianCalendarD2Ev@PLT
	.cfi_endproc
.LFE2976:
	.size	_ZN6icu_6714TaiwanCalendarD2Ev, .-_ZN6icu_6714TaiwanCalendarD2Ev
	.globl	_ZN6icu_6714TaiwanCalendarD1Ev
	.set	_ZN6icu_6714TaiwanCalendarD1Ev,_ZN6icu_6714TaiwanCalendarD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TaiwanCalendarD0Ev
	.type	_ZN6icu_6714TaiwanCalendarD0Ev, @function
_ZN6icu_6714TaiwanCalendarD0Ev:
.LFB2978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714TaiwanCalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717GregorianCalendarD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2978:
	.size	_ZN6icu_6714TaiwanCalendarD0Ev, .-_ZN6icu_6714TaiwanCalendarD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TaiwanCalendar5cloneEv
	.type	_ZNK6icu_6714TaiwanCalendar5cloneEv, @function
_ZNK6icu_6714TaiwanCalendar5cloneEv:
.LFB2983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$656, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L8
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6717GregorianCalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714TaiwanCalendarE(%rip), %rax
	movq	%rax, (%r12)
.L8:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2983:
	.size	_ZNK6icu_6714TaiwanCalendar5cloneEv, .-_ZNK6icu_6714TaiwanCalendar5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TaiwanCalendar21handleGetExtendedYearEv
	.type	_ZN6icu_6714TaiwanCalendar21handleGetExtendedYearEv, @function
_ZN6icu_6714TaiwanCalendar21handleGetExtendedYearEv:
.LFB2985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movl	$19, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_@PLT
	cmpl	$19, %eax
	je	.L26
.L15:
	movl	128(%rbx), %esi
	testl	%esi, %esi
	jle	.L18
	movl	12(%rbx), %eax
	cmpl	$1, %eax
	je	.L18
	testl	%eax, %eax
	jne	.L21
	movl	132(%rbx), %edx
	movl	$1911, %eax
	testl	%edx, %edx
	jle	.L14
	movl	$1912, %eax
	subl	16(%rbx), %eax
.L14:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movl	132(%rbx), %ecx
	movl	$1912, %eax
	testl	%ecx, %ecx
	jle	.L14
	movl	16(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addl	$1911, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$19, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_@PLT
	cmpl	$19, %eax
	jne	.L15
	movl	204(%rbx), %edi
	testl	%edi, %edi
	jle	.L21
	movl	88(%rbx), %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L21:
	addq	$8, %rsp
	movl	$1970, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2985:
	.size	_ZN6icu_6714TaiwanCalendar21handleGetExtendedYearEv, .-_ZN6icu_6714TaiwanCalendar21handleGetExtendedYearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TaiwanCalendar16getStaticClassIDEv
	.type	_ZN6icu_6714TaiwanCalendar16getStaticClassIDEv, @function
_ZN6icu_6714TaiwanCalendar16getStaticClassIDEv:
.LFB2970:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714TaiwanCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2970:
	.size	_ZN6icu_6714TaiwanCalendar16getStaticClassIDEv, .-_ZN6icu_6714TaiwanCalendar16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TaiwanCalendarC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6714TaiwanCalendarC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6714TaiwanCalendarC2ERKNS_6LocaleER10UErrorCode:
.LFB2973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_6717GregorianCalendarC2ERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6714TaiwanCalendarE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE2973:
	.size	_ZN6icu_6714TaiwanCalendarC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6714TaiwanCalendarC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6714TaiwanCalendarC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6714TaiwanCalendarC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6714TaiwanCalendarC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TaiwanCalendarC2ERKS0_
	.type	_ZN6icu_6714TaiwanCalendarC2ERKS0_, @function
_ZN6icu_6714TaiwanCalendarC2ERKS0_:
.LFB2980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6717GregorianCalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714TaiwanCalendarE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2980:
	.size	_ZN6icu_6714TaiwanCalendarC2ERKS0_, .-_ZN6icu_6714TaiwanCalendarC2ERKS0_
	.globl	_ZN6icu_6714TaiwanCalendarC1ERKS0_
	.set	_ZN6icu_6714TaiwanCalendarC1ERKS0_,_ZN6icu_6714TaiwanCalendarC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TaiwanCalendaraSERKS0_
	.type	_ZN6icu_6714TaiwanCalendaraSERKS0_, @function
_ZN6icu_6714TaiwanCalendaraSERKS0_:
.LFB2982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6717GregorianCalendaraSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2982:
	.size	_ZN6icu_6714TaiwanCalendaraSERKS0_, .-_ZN6icu_6714TaiwanCalendaraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TaiwanCalendar19handleComputeFieldsEiR10UErrorCode
	.type	_ZN6icu_6714TaiwanCalendar19handleComputeFieldsEiR10UErrorCode, @function
_ZN6icu_6714TaiwanCalendar19handleComputeFieldsEiR10UErrorCode:
.LFB2986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6717GregorianCalendar19handleComputeFieldsEiR10UErrorCode@PLT
	movl	88(%rbx), %eax
	subl	$1911, %eax
	testl	%eax, %eax
	jle	.L35
	movl	%eax, 16(%rbx)
	movl	$257, %edx
	movabsq	$4294967297, %rax
	movl	$1, 12(%rbx)
	movq	%rax, 128(%rbx)
	movw	%dx, 104(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movl	$1, %edx
	movl	$0, 12(%rbx)
	subl	%eax, %edx
	movabsq	$4294967297, %rax
	movq	%rax, 128(%rbx)
	movl	$257, %eax
	movl	%edx, 16(%rbx)
	movw	%ax, 104(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2986:
	.size	_ZN6icu_6714TaiwanCalendar19handleComputeFieldsEiR10UErrorCode, .-_ZN6icu_6714TaiwanCalendar19handleComputeFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TaiwanCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.type	_ZNK6icu_6714TaiwanCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, @function
_ZNK6icu_6714TaiwanCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE:
.LFB2987:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L39
	xorl	%eax, %eax
	cmpl	$1, %edx
	seta	%al
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	jmp	_ZNK6icu_6717GregorianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE@PLT
	.cfi_endproc
.LFE2987:
	.size	_ZNK6icu_6714TaiwanCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, .-_ZNK6icu_6714TaiwanCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.section	.rodata.str1.1
.LC1:
	.string	"@calendar=roc"
	.text
	.p2align 4
	.type	_ZN6icu_67L30initializeSystemDefaultCenturyEv, @function
_ZN6icu_67L30initializeSystemDefaultCenturyEv:
.LFB2989:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-928(%rbp), %r14
	leaq	-932(%rbp), %r13
	pushq	%r12
	movq	%r14, %rdi
	.cfi_offset 12, -40
	leaq	-704(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVN6icu_6714TaiwanCalendarE(%rip), %rbx
	subq	$912, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -932(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6717GregorianCalendarC2ERKNS_6LocaleER10UErrorCode@PLT
	movq	%rbx, -704(%rbp)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	-932(%rbp), %eax
	testl	%eax, %eax
	jle	.L46
.L43:
	movq	%r12, %rdi
	movq	%rbx, -704(%rbp)
	call	_ZN6icu_6717GregorianCalendarD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$912, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r13, %rcx
	movl	$-80, %edx
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movsd	%xmm0, _ZN6icu_67L26gSystemDefaultCenturyStartE(%rip)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	%eax, _ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip)
	jmp	.L43
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2989:
	.size	_ZN6icu_67L30initializeSystemDefaultCenturyEv, .-_ZN6icu_67L30initializeSystemDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TaiwanCalendar19defaultCenturyStartEv
	.type	_ZNK6icu_6714TaiwanCalendar19defaultCenturyStartEv, @function
_ZNK6icu_6714TaiwanCalendar19defaultCenturyStartEv:
.LFB2990:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %eax
	cmpl	$2, %eax
	je	.L56
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L50
	call	_ZN6icu_67L30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L50:
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore 6
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	ret
	.cfi_endproc
.LFE2990:
	.size	_ZNK6icu_6714TaiwanCalendar19defaultCenturyStartEv, .-_ZNK6icu_6714TaiwanCalendar19defaultCenturyStartEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TaiwanCalendar23defaultCenturyStartYearEv
	.type	_ZNK6icu_6714TaiwanCalendar23defaultCenturyStartYearEv, @function
_ZNK6icu_6714TaiwanCalendar23defaultCenturyStartYearEv:
.LFB2991:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %eax
	cmpl	$2, %eax
	je	.L67
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L61
	call	_ZN6icu_67L30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L61:
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore 6
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	ret
	.cfi_endproc
.LFE2991:
	.size	_ZNK6icu_6714TaiwanCalendar23defaultCenturyStartYearEv, .-_ZNK6icu_6714TaiwanCalendar23defaultCenturyStartYearEv
	.weak	_ZTSN6icu_6714TaiwanCalendarE
	.section	.rodata._ZTSN6icu_6714TaiwanCalendarE,"aG",@progbits,_ZTSN6icu_6714TaiwanCalendarE,comdat
	.align 16
	.type	_ZTSN6icu_6714TaiwanCalendarE, @object
	.size	_ZTSN6icu_6714TaiwanCalendarE, 26
_ZTSN6icu_6714TaiwanCalendarE:
	.string	"N6icu_6714TaiwanCalendarE"
	.weak	_ZTIN6icu_6714TaiwanCalendarE
	.section	.data.rel.ro._ZTIN6icu_6714TaiwanCalendarE,"awG",@progbits,_ZTIN6icu_6714TaiwanCalendarE,comdat
	.align 8
	.type	_ZTIN6icu_6714TaiwanCalendarE, @object
	.size	_ZTIN6icu_6714TaiwanCalendarE, 24
_ZTIN6icu_6714TaiwanCalendarE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714TaiwanCalendarE
	.quad	_ZTIN6icu_6717GregorianCalendarE
	.weak	_ZTVN6icu_6714TaiwanCalendarE
	.section	.data.rel.ro._ZTVN6icu_6714TaiwanCalendarE,"awG",@progbits,_ZTVN6icu_6714TaiwanCalendarE,comdat
	.align 8
	.type	_ZTVN6icu_6714TaiwanCalendarE, @object
	.size	_ZTVN6icu_6714TaiwanCalendarE, 448
_ZTVN6icu_6714TaiwanCalendarE:
	.quad	0
	.quad	_ZTIN6icu_6714TaiwanCalendarE
	.quad	_ZN6icu_6714TaiwanCalendarD1Ev
	.quad	_ZN6icu_6714TaiwanCalendarD0Ev
	.quad	_ZNK6icu_6714TaiwanCalendar17getDynamicClassIDEv
	.quad	_ZNK6icu_6714TaiwanCalendar5cloneEv
	.quad	_ZNK6icu_678CalendareqERKS0_
	.quad	_ZNK6icu_6717GregorianCalendar14isEquivalentToERKNS_8CalendarE
	.quad	_ZN6icu_678Calendar3addENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_6717GregorianCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_6717GregorianCalendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6717GregorianCalendar14inDaylightTimeER10UErrorCode
	.quad	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_6717GregorianCalendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6717GregorianCalendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6714TaiwanCalendar7getTypeEv
	.quad	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEv
	.quad	_ZN6icu_678Calendar11computeTimeER10UErrorCode
	.quad	_ZN6icu_678Calendar13computeFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode
	.quad	_ZNK6icu_6714TaiwanCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.quad	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE
	.quad	_ZNK6icu_6717GregorianCalendar23handleComputeMonthStartEiia
	.quad	_ZNK6icu_6717GregorianCalendar20handleGetMonthLengthEii
	.quad	_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi
	.quad	_ZN6icu_6714TaiwanCalendar21handleGetExtendedYearEv
	.quad	_ZN6icu_6717GregorianCalendar22handleComputeJulianDayE19UCalendarDateFields
	.quad	_ZN6icu_6717GregorianCalendar35handleGetExtendedYearFromWeekFieldsEii
	.quad	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar23getFieldResolutionTableEv
	.quad	_ZN6icu_6714TaiwanCalendar19handleComputeFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar21getDefaultMonthInYearEi
	.quad	_ZN6icu_678Calendar20getDefaultDayInMonthEii
	.quad	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6714TaiwanCalendar18haveDefaultCenturyEv
	.quad	_ZNK6icu_6714TaiwanCalendar19defaultCenturyStartEv
	.quad	_ZNK6icu_6714TaiwanCalendar23defaultCenturyStartYearEv
	.quad	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode
	.quad	_ZN6icu_678Calendar14setRelatedYearEi
	.quad	_ZNK6icu_6717GregorianCalendar14internalGetEraEv
	.quad	_ZNK6icu_6717GregorianCalendar11monthLengthEi
	.quad	_ZNK6icu_6717GregorianCalendar11monthLengthEii
	.quad	_ZN6icu_6717GregorianCalendar11getEpochDayER10UErrorCode
	.local	_ZN6icu_67L25gSystemDefaultCenturyInitE
	.comm	_ZN6icu_67L25gSystemDefaultCenturyInitE,8,8
	.data
	.align 4
	.type	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, @object
	.size	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, 4
_ZN6icu_67L30gSystemDefaultCenturyStartYearE:
	.long	-1
	.align 8
	.type	_ZN6icu_67L26gSystemDefaultCenturyStartE, @object
	.size	_ZN6icu_67L26gSystemDefaultCenturyStartE, 8
_ZN6icu_67L26gSystemDefaultCenturyStartE:
	.long	0
	.long	1048576
	.local	_ZZN6icu_6714TaiwanCalendar16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6714TaiwanCalendar16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
