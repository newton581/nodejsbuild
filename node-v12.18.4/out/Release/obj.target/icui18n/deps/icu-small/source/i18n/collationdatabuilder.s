	.file	"collationdatabuilder.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6728DataBuilderCollationIterator9getOffsetEv
	.type	_ZNK6icu_6728DataBuilderCollationIterator9getOffsetEv, @function
_ZNK6icu_6728DataBuilderCollationIterator9getOffsetEv:
.LFB3442:
	.cfi_startproc
	endbr64
	movl	824(%rdi), %eax
	ret
	.cfi_endproc
.LFE3442:
	.size	_ZNK6icu_6728DataBuilderCollationIterator9getOffsetEv, .-_ZNK6icu_6728DataBuilderCollationIterator9getOffsetEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720CollationDataBuilder22isCompressibleLeadByteEj
	.type	_ZNK6icu_6720CollationDataBuilder22isCompressibleLeadByteEj, @function
_ZNK6icu_6720CollationDataBuilder22isCompressibleLeadByteEj:
.LFB3460:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	%esi, %esi
	movq	72(%rax), %rax
	movzbl	(%rax,%rsi), %eax
	ret
	.cfi_endproc
.LFE3460:
	.size	_ZNK6icu_6720CollationDataBuilder22isCompressibleLeadByteEj, .-_ZNK6icu_6720CollationDataBuilder22isCompressibleLeadByteEj
	.p2align 4
	.type	enumRangeLeadValue, @function
enumRangeLeadValue:
.LFB3487:
	.cfi_startproc
	endbr64
	cmpl	$-1, %ecx
	je	.L9
	cmpl	$192, %ecx
	jne	.L8
	movl	(%rdi), %eax
	movl	$256, %edx
	testl	%eax, %eax
	js	.L11
.L7:
	cmpl	%edx, %eax
	je	.L12
.L8:
	movl	$512, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	(%rdi), %eax
	xorl	%edx, %edx
	testl	%eax, %eax
	jns	.L7
.L11:
	movl	%edx, (%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3487:
	.size	enumRangeLeadValue, .-enumRangeLeadValue
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728DataBuilderCollationIteratorD2Ev
	.type	_ZN6icu_6728DataBuilderCollationIteratorD2Ev, @function
_ZN6icu_6728DataBuilderCollationIteratorD2Ev:
.LFB3437:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6728DataBuilderCollationIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CollationIteratorD2Ev@PLT
	.cfi_endproc
.LFE3437:
	.size	_ZN6icu_6728DataBuilderCollationIteratorD2Ev, .-_ZN6icu_6728DataBuilderCollationIteratorD2Ev
	.globl	_ZN6icu_6728DataBuilderCollationIteratorD1Ev
	.set	_ZN6icu_6728DataBuilderCollationIteratorD1Ev,_ZN6icu_6728DataBuilderCollationIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728DataBuilderCollationIteratorD0Ev
	.type	_ZN6icu_6728DataBuilderCollationIteratorD0Ev, @function
_ZN6icu_6728DataBuilderCollationIteratorD0Ev:
.LFB3439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6728DataBuilderCollationIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CollationIteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3439:
	.size	_ZN6icu_6728DataBuilderCollationIteratorD0Ev, .-_ZN6icu_6728DataBuilderCollationIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728DataBuilderCollationIterator13resetToOffsetEi
	.type	_ZN6icu_6728DataBuilderCollationIterator13resetToOffsetEi, @function
_ZN6icu_6728DataBuilderCollationIterator13resetToOffsetEi:
.LFB3441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6717CollationIterator5resetEv@PLT
	movl	%r12d, 824(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3441:
	.size	_ZN6icu_6728DataBuilderCollationIterator13resetToOffsetEi, .-_ZN6icu_6728DataBuilderCollationIterator13resetToOffsetEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728DataBuilderCollationIterator17previousCodePointER10UErrorCode
	.type	_ZN6icu_6728DataBuilderCollationIterator17previousCodePointER10UErrorCode, @function
_ZN6icu_6728DataBuilderCollationIterator17previousCodePointER10UErrorCode:
.LFB3444:
	.cfi_startproc
	endbr64
	movl	824(%rdi), %esi
	testl	%esi, %esi
	je	.L21
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	816(%rdi), %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%edx, %edx
	cmpl	$65535, %eax
	seta	%dl
	addl	$1, %edx
	subl	%edx, 824(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore 3
	.cfi_restore 6
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3444:
	.size	_ZN6icu_6728DataBuilderCollationIterator17previousCodePointER10UErrorCode, .-_ZN6icu_6728DataBuilderCollationIterator17previousCodePointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728DataBuilderCollationIterator13nextCodePointER10UErrorCode
	.type	_ZN6icu_6728DataBuilderCollationIterator13nextCodePointER10UErrorCode, @function
_ZN6icu_6728DataBuilderCollationIterator13nextCodePointER10UErrorCode:
.LFB3443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	824(%rdi), %esi
	movq	816(%rdi), %rdi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L28
	sarl	$5, %eax
.L29:
	cmpl	%eax, %esi
	je	.L32
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%edx, %edx
	cmpl	$65535, %eax
	seta	%dl
	addl	$1, %edx
	addl	%edx, 824(%rbx)
.L27:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	12(%rdi), %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$-1, %eax
	jmp	.L27
	.cfi_endproc
.LFE3443:
	.size	_ZN6icu_6728DataBuilderCollationIterator13nextCodePointER10UErrorCode, .-_ZN6icu_6728DataBuilderCollationIterator13nextCodePointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6728DataBuilderCollationIterator11getDataCE32Ei
	.type	_ZNK6icu_6728DataBuilderCollationIterator11getDataCE32Ei, @function
_ZNK6icu_6728DataBuilderCollationIterator11getDataCE32Ei:
.LFB3447:
	.cfi_startproc
	endbr64
	movq	392(%rdi), %rax
	movq	32(%rax), %rdi
	jmp	utrie2_get32_67@PLT
	.cfi_endproc
.LFE3447:
	.size	_ZNK6icu_6728DataBuilderCollationIterator11getDataCE32Ei, .-_ZNK6icu_6728DataBuilderCollationIterator11getDataCE32Ei
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728DataBuilderCollationIterator20forwardNumCodePointsEiR10UErrorCode
	.type	_ZN6icu_6728DataBuilderCollationIterator20forwardNumCodePointsEiR10UErrorCode, @function
_ZN6icu_6728DataBuilderCollationIterator20forwardNumCodePointsEiR10UErrorCode:
.LFB3445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	824(%rdi), %esi
	movq	816(%rdi), %rdi
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movl	%eax, 824(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3445:
	.size	_ZN6icu_6728DataBuilderCollationIterator20forwardNumCodePointsEiR10UErrorCode, .-_ZN6icu_6728DataBuilderCollationIterator20forwardNumCodePointsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728DataBuilderCollationIterator21backwardNumCodePointsEiR10UErrorCode
	.type	_ZN6icu_6728DataBuilderCollationIterator21backwardNumCodePointsEiR10UErrorCode, @function
_ZN6icu_6728DataBuilderCollationIterator21backwardNumCodePointsEiR10UErrorCode:
.LFB3446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	negl	%edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	824(%rdi), %esi
	movq	816(%rdi), %rdi
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movl	%eax, 824(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3446:
	.size	_ZN6icu_6728DataBuilderCollationIterator21backwardNumCodePointsEiR10UErrorCode, .-_ZN6icu_6728DataBuilderCollationIterator21backwardNumCodePointsEiR10UErrorCode
	.p2align 4
	.globl	uprv_deleteConditionalCE32_67
	.type	uprv_deleteConditionalCE32_67, @function
uprv_deleteConditionalCE32_67:
.LFB3429:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L40
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	ret
	.cfi_endproc
.LFE3429:
	.size	uprv_deleteConditionalCE32_67, .-uprv_deleteConditionalCE32_67
	.align 2
	.p2align 4
	.type	_ZN6icu_6720CollationDataBuilder15encodeExpansionEPKliR10UErrorCode.part.0, @function
_ZN6icu_6720CollationDataBuilder15encodeExpansionEPKliR10UErrorCode.part.0:
.LFB4579:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	80(%rdi), %ebx
	movq	(%rsi), %r14
	movl	%ebx, %r15d
	subl	%edx, %r15d
	js	.L46
	movq	%rsi, %r9
	leal	-2(%rdx), %eax
	xorl	%edi, %edi
	negq	%r9
	leaq	16(%rsi,%rax,8), %r11
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L48:
	movl	%r10d, %edi
	addq	$8, %r9
	cmpl	%r10d, %r15d
	jl	.L46
.L55:
	xorl	%eax, %eax
	cmpl	%edi, %ebx
	jle	.L47
	movq	96(%r13), %rax
	addq	%r9, %rax
	movq	(%rsi,%rax), %rax
.L47:
	leal	1(%rdi), %r10d
	cmpq	%rax, %r14
	jne	.L48
	cmpl	$524287, %edi
	jg	.L56
	cmpl	$1, %r12d
	je	.L53
	movl	%r10d, %edx
	leaq	8(%rsi), %rax
	.p2align 4,,10
	.p2align 3
.L54:
	xorl	%r8d, %r8d
	cmpl	%edx, %ebx
	jle	.L52
	movq	96(%r13), %r8
	addq	%rax, %r8
	movq	(%r8,%r9), %r8
.L52:
	cmpq	%r8, (%rax)
	jne	.L48
	addq	$8, %rax
	addl	$1, %edx
	cmpq	%r11, %rax
	jne	.L54
.L53:
	movl	%edi, %eax
	sall	$8, %r12d
	addq	$40, %rsp
	sall	$13, %eax
	popq	%rbx
	orl	%r12d, %eax
	popq	%r12
	popq	%r13
	orb	$-58, %al
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	cmpl	$524287, %ebx
	jg	.L56
	testl	%r12d, %r12d
	jle	.L57
	leaq	72(%r13), %rax
	movq	%rsi, %r14
	movq	%rax, -72(%rbp)
	leal	-1(%r12), %eax
	leaq	8(%rsi,%rax,8), %rax
	movq	%rax, -64(%rbp)
	movslq	%ebx, %rax
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L70:
	movslq	80(%r13), %rax
.L61:
	movl	%eax, %esi
	movq	(%r14), %r15
	addl	$1, %esi
	js	.L58
	cmpl	84(%r13), %esi
	jle	.L59
.L58:
	movq	-72(%rbp), %rdi
	movq	%rcx, %rdx
	movq	%rcx, -56(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-56(%rbp), %rcx
	testb	%al, %al
	je	.L60
	movslq	80(%r13), %rax
	leal	1(%rax), %esi
.L59:
	movq	96(%r13), %rdx
	movq	%r15, (%rdx,%rax,8)
	movl	%esi, 80(%r13)
.L60:
	addq	$8, %r14
	cmpq	%r14, -64(%rbp)
	jne	.L70
.L57:
	movl	%ebx, %eax
	sall	$8, %r12d
	addq	$40, %rsp
	sall	$13, %eax
	popq	%rbx
	orl	%r12d, %eax
	popq	%r12
	popq	%r13
	orb	$-58, %al
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L56:
	.cfi_restore_state
	movl	$15, (%rcx)
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4579:
	.size	_ZN6icu_6720CollationDataBuilder15encodeExpansionEPKliR10UErrorCode.part.0, .-_ZN6icu_6720CollationDataBuilder15encodeExpansionEPKliR10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6720CollationDataBuilder17encodeExpansion32EPKiiR10UErrorCode.part.0, @function
_ZN6icu_6720CollationDataBuilder17encodeExpansion32EPKiiR10UErrorCode.part.0:
.LFB4580:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	48(%rdi), %ebx
	movl	(%rsi), %r11d
	movq	%rcx, -64(%rbp)
	movl	%ebx, %r14d
	subl	%edx, %r14d
	js	.L72
	leal	-2(%rdx), %eax
	leal	-1(%rbx), %ecx
	leaq	8(%rsi,%rax,4), %r10
	xorl	%eax, %eax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L74:
	addq	$1, %rax
	cmpl	%eax, %r14d
	jl	.L72
.L83:
	movl	%eax, %r9d
	movl	%eax, %edx
	xorl	%edi, %edi
	testl	%ebx, %ebx
	jle	.L73
	movl	%ebx, %r8d
	subl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L73
	movq	64(%r12), %rdi
	movl	(%rdi,%rax,4), %edi
.L73:
	cmpl	%edi, %r11d
	jne	.L74
	cmpq	$524287, %rax
	ja	.L84
	cmpl	$1, %r13d
	je	.L79
	testl	%ebx, %ebx
	jle	.L101
	movl	%ecx, %r8d
	leaq	0(,%rax,4), %r15
	leaq	4(%rsi), %rdi
	subl	%r9d, %r8d
	subq	%rsi, %r15
	.p2align 4,,10
	.p2align 3
.L82:
	xorl	%r9d, %r9d
	testl	%r8d, %r8d
	jle	.L81
	movq	64(%r12), %r9
	addq	%rdi, %r9
	movl	(%r9,%r15), %r9d
.L81:
	cmpl	%r9d, (%rdi)
	jne	.L74
	addq	$4, %rdi
	subl	$1, %r8d
	cmpq	%r10, %rdi
	jne	.L82
.L79:
	movl	%edx, %eax
	sall	$8, %r13d
	addq	$40, %rsp
	sall	$13, %eax
	popq	%rbx
	popq	%r12
	orl	%r13d, %eax
	popq	%r13
	popq	%r14
	orb	$-59, %al
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	leaq	4(%rsi), %rdi
	.p2align 4,,10
	.p2align 3
.L80:
	movl	(%rdi), %r8d
	testl	%r8d, %r8d
	jne	.L74
	addq	$4, %rdi
	cmpq	%r10, %rdi
	jne	.L80
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L72:
	cmpl	$524287, %ebx
	jg	.L84
	testl	%r13d, %r13d
	jle	.L85
	leal	-1(%r13), %eax
	leaq	40(%r12), %rcx
	movl	%ebx, -68(%rbp)
	movq	-64(%rbp), %r14
	leaq	4(%rsi,%rax,4), %rax
	movl	%r13d, -72(%rbp)
	movq	%r12, %r13
	movq	%rcx, %r12
	movq	%rax, -56(%rbp)
	movslq	%ebx, %rax
	movq	%rsi, %rbx
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L102:
	movslq	48(%r13), %rax
.L89:
	movl	%eax, %esi
	movl	(%rbx), %r15d
	addl	$1, %esi
	js	.L86
	cmpl	52(%r13), %esi
	jle	.L87
.L86:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L88
	movslq	48(%r13), %rax
.L87:
	movq	64(%r13), %rdx
	movl	%r15d, (%rdx,%rax,4)
	addl	$1, 48(%r13)
.L88:
	addq	$4, %rbx
	cmpq	%rbx, -56(%rbp)
	jne	.L102
	movl	-68(%rbp), %ebx
	movl	-72(%rbp), %r13d
.L85:
	movl	%ebx, %eax
	sall	$8, %r13d
	addq	$40, %rsp
	sall	$13, %eax
	popq	%rbx
	popq	%r12
	orl	%r13d, %eax
	popq	%r13
	popq	%r14
	orb	$-59, %al
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L84:
	.cfi_restore_state
	movq	-64(%rbp), %rax
	movl	$15, (%rax)
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4580:
	.size	_ZN6icu_6720CollationDataBuilder17encodeExpansion32EPKiiR10UErrorCode.part.0, .-_ZN6icu_6720CollationDataBuilder17encodeExpansion32EPKiiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder10CEModifierD2Ev
	.type	_ZN6icu_6720CollationDataBuilder10CEModifierD2Ev, @function
_ZN6icu_6720CollationDataBuilder10CEModifierD2Ev:
.LFB3418:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6720CollationDataBuilder10CEModifierE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3418:
	.size	_ZN6icu_6720CollationDataBuilder10CEModifierD2Ev, .-_ZN6icu_6720CollationDataBuilder10CEModifierD2Ev
	.globl	_ZN6icu_6720CollationDataBuilder10CEModifierD1Ev
	.set	_ZN6icu_6720CollationDataBuilder10CEModifierD1Ev,_ZN6icu_6720CollationDataBuilder10CEModifierD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder10CEModifierD0Ev
	.type	_ZN6icu_6720CollationDataBuilder10CEModifierD0Ev, @function
_ZN6icu_6720CollationDataBuilder10CEModifierD0Ev:
.LFB3420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6720CollationDataBuilder10CEModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3420:
	.size	_ZN6icu_6720CollationDataBuilder10CEModifierD0Ev, .-_ZN6icu_6720CollationDataBuilder10CEModifierD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728DataBuilderCollationIteratorC2ERNS_20CollationDataBuilderE
	.type	_ZN6icu_6728DataBuilderCollationIteratorC2ERNS_20CollationDataBuilderE, @function
_ZN6icu_6728DataBuilderCollationIteratorC2ERNS_20CollationDataBuilderE:
.LFB3434:
	.cfi_startproc
	endbr64
	movq	400(%rdi), %rax
	movb	$0, 44(%rdi)
	pxor	%xmm0, %xmm0
	movb	$0, 388(%rdi)
	movq	%rax, 8(%rdi)
	leaq	400(%rdi), %rax
	movq	%rax, 16(%rdi)
	leaq	48(%rdi), %rax
	movq	%rax, 32(%rdi)
	leaq	16+_ZTVN6icu_6728DataBuilderCollationIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rsi), %rax
	movq	$301989888, 456(%rdi)
	movq	%rax, 448(%rdi)
	movups	%xmm0, 472(%rdi)
	movq	16(%rsi), %rax
	movl	$0, 24(%rdi)
	movq	%rax, 432(%rdi)
	xorl	%eax, %eax
	movl	$40, 40(%rdi)
	movl	$0, 368(%rdi)
	movq	$0, 376(%rdi)
	movl	$-1, 384(%rdi)
	movq	%rsi, 392(%rdi)
	movq	$0, 400(%rdi)
	movq	$0, 408(%rdi)
	movq	$0, 416(%rdi)
	movq	$0, 424(%rdi)
	movq	$0, 464(%rdi)
	movq	$0, 488(%rdi)
	movq	$0, 496(%rdi)
	movl	$0, 520(%rdi)
	movq	$0, 528(%rdi)
	movl	$0, 536(%rdi)
	movq	$0, 816(%rdi)
	movl	$0, 824(%rdi)
	movups	%xmm0, 504(%rdi)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L114:
	leal	4430(%rax), %edx
	cmpl	$39, %eax
	jle	.L108
	movl	%eax, %edx
	sall	$13, %edx
	addl	$36700160, %edx
	orl	$455, %edx
	movl	%edx, 544(%rdi,%rax,4)
	cmpl	$66, %eax
	je	.L110
.L112:
	addq	$1, %rax
.L111:
	leal	4352(%rax), %edx
	cmpq	$18, %rax
	ja	.L114
.L108:
	sall	$13, %edx
	orl	$455, %edx
	movl	%edx, 544(%rdi,%rax,4)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L110:
	leaq	544(%rdi), %rax
	movq	%rax, 440(%rdi)
	ret
	.cfi_endproc
.LFE3434:
	.size	_ZN6icu_6728DataBuilderCollationIteratorC2ERNS_20CollationDataBuilderE, .-_ZN6icu_6728DataBuilderCollationIteratorC2ERNS_20CollationDataBuilderE
	.globl	_ZN6icu_6728DataBuilderCollationIteratorC1ERNS_20CollationDataBuilderE
	.set	_ZN6icu_6728DataBuilderCollationIteratorC1ERNS_20CollationDataBuilderE,_ZN6icu_6728DataBuilderCollationIteratorC2ERNS_20CollationDataBuilderE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728DataBuilderCollationIterator8fetchCEsERKNS_13UnicodeStringEiPli
	.type	_ZN6icu_6728DataBuilderCollationIterator8fetchCEsERKNS_13UnicodeStringEiPli, @function
_ZN6icu_6728DataBuilderCollationIterator8fetchCEsERKNS_13UnicodeStringEiPli:
.LFB3440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movl	%edx, %esi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	392(%rdi), %rax
	movq	64(%rax), %rdx
	movq	%rdx, 408(%rdi)
	movq	96(%rax), %rdx
	movq	%rdx, 416(%rdi)
	movzwl	352(%rax), %edx
	testb	$17, %dl
	jne	.L138
	andl	$2, %edx
	jne	.L150
	movq	368(%rax), %rax
.L116:
	movq	%rax, 424(%r13)
	movq	%r13, %rdi
	movl	%esi, -52(%rbp)
	call	_ZN6icu_6717CollationIterator5resetEv@PLT
	movl	-52(%rbp), %esi
	movswl	8(%r14), %eax
	movq	%r14, 816(%r13)
	movl	$0, -44(%rbp)
	movl	%esi, 824(%r13)
	testw	%ax, %ax
	js	.L118
	.p2align 4,,10
	.p2align 3
.L152:
	sarl	$5, %eax
	cmpl	%esi, %eax
	jle	.L115
.L153:
	movl	$0, 24(%r13)
	movq	%r14, %rdi
	movl	$0, 368(%r13)
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r14d
	movl	824(%r13), %eax
	cmpl	$65535, %r14d
	ja	.L146
	addl	$1, %eax
	movl	%r14d, %esi
	movl	%eax, 824(%r13)
	movq	392(%r13), %rax
	movq	32(%rax), %rdi
	call	utrie2_get32_67@PLT
	movl	%eax, %ecx
	cmpl	$192, %eax
	jne	.L123
	movq	392(%r13), %rax
	movq	16(%rax), %rsi
	movq	(%rsi), %rax
	movq	16(%rax), %rdx
	cmpl	$55295, %r14d
	ja	.L124
	movl	%r14d, %ecx
	movq	(%rax), %rax
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movzwl	(%rax,%rcx,2), %ecx
.L148:
	movl	%r14d, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
.L125:
	movl	(%rdx,%rax), %ecx
.L128:
	leaq	-44(%rbp), %r9
	movl	$1, %r8d
	movl	%r14d, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6717CollationIterator17appendCEsFromCE32EPKNS_13CollationDataEijaR10UErrorCode@PLT
	movl	24(%r13), %eax
	testl	%eax, %eax
	jle	.L134
	movq	32(%r13), %rdx
	leal	-1(%rax), %ecx
	leaq	8(%rdx), %rax
	leaq	(%rax,%rcx,8), %rcx
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L151:
	addq	$8, %rax
.L133:
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L131
	cmpl	$30, %r12d
	jg	.L132
	movslq	%r12d, %rsi
	movq	%rdx, (%rbx,%rsi,8)
.L132:
	addl	$1, %r12d
.L131:
	movq	%rax, %rdx
	cmpq	%rax, %rcx
	jne	.L151
.L134:
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L115
	movq	816(%r13), %r14
	movl	824(%r13), %esi
	movswl	8(%r14), %eax
	testw	%ax, %ax
	jns	.L152
.L118:
	movl	12(%r14), %eax
	cmpl	%esi, %eax
	jg	.L153
.L115:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L150:
	.cfi_restore_state
	addq	$354, %rax
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L146:
	addl	$2, %eax
	movl	%r14d, %esi
	movl	%eax, 824(%r13)
	movq	392(%r13), %rax
	movq	32(%rax), %rdi
	call	utrie2_get32_67@PLT
	movl	%eax, %ecx
	cmpl	$192, %eax
	je	.L155
.L123:
	leaq	400(%r13), %rsi
	jmp	.L128
.L155:
	movq	392(%r13), %rax
	movq	16(%rax), %rsi
	movq	(%rsi), %rax
	movq	16(%rax), %rdx
	cmpl	$1114111, %r14d
	jbe	.L135
	movl	$512, %eax
	jmp	.L125
.L124:
	cmpl	$56320, %r14d
	movq	(%rax), %rdi
	movl	$0, %ecx
	movl	$320, %eax
	cmovl	%eax, %ecx
	movl	%r14d, %eax
	sarl	$5, %eax
.L149:
	addl	%ecx, %eax
	cltq
	movzwl	(%rdi,%rax,2), %ecx
	jmp	.L148
.L135:
	cmpl	44(%rax), %r14d
	jl	.L127
	movslq	48(%rax), %rax
	salq	$2, %rax
	jmp	.L125
.L127:
	movq	(%rax), %rdi
	movl	%r14d, %eax
	movl	%r14d, %ecx
	sarl	$11, %eax
	sarl	$5, %ecx
	addl	$2080, %eax
	andl	$63, %ecx
	cltq
	movzwl	(%rdi,%rax,2), %eax
	jmp	.L149
.L138:
	xorl	%eax, %eax
	jmp	.L116
.L154:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3440:
	.size	_ZN6icu_6728DataBuilderCollationIterator8fetchCEsERKNS_13UnicodeStringEiPli, .-_ZN6icu_6728DataBuilderCollationIterator8fetchCEsERKNS_13UnicodeStringEiPli
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilderC2ER10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilderC2ER10UErrorCode, @function
_ZN6icu_6720CollationDataBuilderC2ER10UErrorCode:
.LFB3450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6720CollationDataBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	%rsi, %rdi
	leaq	40(%rbx), %r14
	leaq	104(%rbx), %r13
	call	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode@PLT
	movq	$0, 16(%rbx)
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	leaq	72(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_679UVector64C1ER10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	leaq	144(%rbx), %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	408(%rbx), %rdi
	movq	%rax, 344(%rbx)
	movl	$2, %eax
	movw	%ax, 352(%rbx)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movslq	48(%rbx), %rax
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movw	%dx, 608(%rbx)
	movl	%eax, %esi
	movups	%xmm0, 616(%rbx)
	addl	$1, %esi
	js	.L157
	cmpl	52(%rbx), %esi
	jle	.L158
.L157:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L159
	movslq	48(%rbx), %rax
.L158:
	movq	64(%rbx), %rdx
	movl	$0, (%rdx,%rax,4)
	addl	$1, 48(%rbx)
.L159:
	popq	%rbx
	movq	%r13, %rdi
	leaq	uprv_deleteConditionalCE32_67(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	.cfi_endproc
.LFE3450:
	.size	_ZN6icu_6720CollationDataBuilderC2ER10UErrorCode, .-_ZN6icu_6720CollationDataBuilderC2ER10UErrorCode
	.globl	_ZN6icu_6720CollationDataBuilderC1ER10UErrorCode
	.set	_ZN6icu_6720CollationDataBuilderC1ER10UErrorCode,_ZN6icu_6720CollationDataBuilderC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder20maybeSetPrimaryRangeEiijiR10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder20maybeSetPrimaryRangeEiijiR10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder20maybeSetPrimaryRangeEiijiR10UErrorCode:
.LFB3457:
	.cfi_startproc
	endbr64
	movl	(%r9), %r10d
	testl	%r10d, %r10d
	jg	.L186
	leal	-2(%r8), %eax
	cmpl	$125, %eax
	ja	.L186
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	sarl	$5, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	%esi, %edx
	pushq	%r13
	sarl	$5, %edx
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%rbx
	subl	%edx, %eax
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpl	$2, %eax
	jle	.L190
.L168:
	movq	%rcx, %r15
	movslq	%r8d, %r8
	movl	%r13d, %eax
	shrl	$24, %ecx
	salq	$32, %r15
	sall	$8, %eax
	leaq	_ZNK6icu_6720CollationDataBuilder22isCompressibleLeadByteEj(%rip), %rdx
	orq	%r15, %r8
	cltq
	movq	%r8, %r15
	orq	%rax, %r15
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L169
	movq	16(%rbx), %rax
	movl	%ecx, %esi
	movq	72(%rax), %rax
	movzbl	(%rax,%rsi), %eax
.L170:
	movq	%r15, %rdx
	movl	80(%rbx), %r8d
	orb	$-128, %dl
	testb	%al, %al
	cmovne	%rdx, %r15
	testl	%r8d, %r8d
	jle	.L172
	movq	96(%rbx), %rdi
	leal	-1(%r8), %esi
	xorl	%eax, %eax
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L191:
	leaq	1(%rax), %rdx
	cmpq	%rax, %rsi
	je	.L174
	movq	%rdx, %rax
.L175:
	movl	%eax, %ecx
	cmpq	(%rdi,%rax,8), %r15
	jne	.L191
.L173:
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L189
	cmpl	$524287, %ecx
	jle	.L178
	movl	$15, (%r9)
.L189:
	xorl	%eax, %eax
.L164:
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	testl	%eax, %eax
	jle	.L189
	movl	%esi, %eax
	andl	$31, %eax
	cmpl	$28, %eax
	jg	.L189
	movl	%r14d, %eax
	andl	$31, %eax
	cmpl	$2, %eax
	jg	.L168
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L174:
	leaq	72(%rbx), %rdi
	leal	1(%r8), %esi
.L179:
	cmpl	84(%rbx), %esi
	jle	.L180
.L176:
	movq	%r9, %rdx
	movl	%r8d, -44(%rbp)
	movq	%r9, -40(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movl	-44(%rbp), %r8d
	movq	-40(%rbp), %r9
	testb	%al, %al
	movl	%r8d, %ecx
	je	.L173
	movslq	80(%rbx), %rax
	leal	1(%rax), %esi
.L177:
	movq	96(%rbx), %rdx
	movl	%r8d, %ecx
	movq	%r15, (%rdx,%rax,8)
	movl	%esi, 80(%rbx)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%r9, -40(%rbp)
	movl	%ecx, %esi
	movq	%rbx, %rdi
	call	*%rax
	movq	-40(%rbp), %r9
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L178:
	movq	32(%rbx), %rdi
	sall	$13, %ecx
	movl	$1, %r8d
	movl	%r14d, %edx
	orb	$-50, %cl
	movl	%r13d, %esi
	call	utrie2_setRange32_67@PLT
	movb	$1, 608(%rbx)
	movl	$1, %eax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L172:
	movl	%r8d, %esi
	leaq	72(%rbx), %rdi
	addl	$1, %esi
	js	.L176
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L180:
	movslq	%r8d, %rax
	jmp	.L177
	.cfi_endproc
.LFE3457:
	.size	_ZN6icu_6720CollationDataBuilder20maybeSetPrimaryRangeEiijiR10UErrorCode, .-_ZN6icu_6720CollationDataBuilder20maybeSetPrimaryRangeEiijiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder28setPrimaryRangeAndReturnNextEiijiR10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder28setPrimaryRangeAndReturnNextEiijiR10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder28setPrimaryRangeAndReturnNextEiijiR10UErrorCode:
.LFB3458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %eax
	movl	%edx, -60(%rbp)
	testl	%eax, %eax
	jg	.L197
	movq	(%rdi), %rax
	movl	%esi, %ebx
	leaq	_ZNK6icu_6720CollationDataBuilder22isCompressibleLeadByteEj(%rip), %rdx
	movl	%ecx, %esi
	movq	%rdi, %r13
	movl	%r8d, %r14d
	shrl	$24, %esi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L194
	movq	16(%rdi), %rax
	movq	72(%rax), %rax
	movsbl	(%rax,%rsi), %r15d
.L195:
	movl	-60(%rbp), %edx
	movl	%r14d, %r8d
	movl	%ebx, %esi
	movq	%r13, %rdi
	movq	%r9, -72(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN6icu_6720CollationDataBuilder20maybeSetPrimaryRangeEiijiR10UErrorCode
	movl	-56(%rbp), %ecx
	movq	-72(%rbp), %r9
	testb	%al, %al
	movl	%ecx, %r12d
	je	.L196
	movl	-60(%rbp), %r13d
	addq	$40, %rsp
	movl	%r15d, %esi
	movl	%ecx, %edi
	subl	%ebx, %r13d
	popq	%rbx
	popq	%r12
	leal	1(%r13), %edx
	popq	%r13
	imull	%r14d, %edx
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679Collation27incThreeBytePrimaryByOffsetEjai@PLT
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movq	32(%r13), %rdi
	movl	%r12d, %edx
	movq	%r9, %rcx
	movl	%ebx, %esi
	orb	$-63, %dl
	movq	%r9, -56(%rbp)
	addl	$1, %ebx
	call	utrie2_set32_67@PLT
	movl	%r12d, %edi
	movl	%r14d, %edx
	movl	%r15d, %esi
	call	_ZN6icu_679Collation27incThreeBytePrimaryByOffsetEjai@PLT
	cmpl	%ebx, -60(%rbp)
	movq	-56(%rbp), %r9
	movl	%eax, %r12d
	jge	.L196
.L192:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r9, -72(%rbp)
	movl	%ecx, -56(%rbp)
	call	*%rax
	movq	-72(%rbp), %r9
	movl	-56(%rbp), %ecx
	movsbl	%al, %r15d
	jmp	.L195
	.cfi_endproc
.LFE3458:
	.size	_ZN6icu_6720CollationDataBuilder28setPrimaryRangeAndReturnNextEiijiR10UErrorCode, .-_ZN6icu_6720CollationDataBuilder28setPrimaryRangeAndReturnNextEiijiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720CollationDataBuilder21getCE32FromOffsetCE32Eaij
	.type	_ZNK6icu_6720CollationDataBuilder21getCE32FromOffsetCE32Eaij, @function
_ZNK6icu_6720CollationDataBuilder21getCE32FromOffsetCE32Eaij:
.LFB3459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	shrl	$13, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%sil, %sil
	je	.L204
	movq	16(%rdi), %rax
	movq	16(%rax), %rax
	movq	(%rax,%rcx,8), %rsi
.L205:
	movl	%r8d, %edi
	call	_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	orb	$-63, %al
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	xorl	%esi, %esi
	cmpl	80(%rdi), %ecx
	jge	.L205
	movq	96(%rdi), %rax
	movslq	%ecx, %rcx
	movl	%r8d, %edi
	movq	(%rax,%rcx,8), %rsi
	call	_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	orb	$-63, %al
	ret
	.cfi_endproc
.LFE3459:
	.size	_ZNK6icu_6720CollationDataBuilder21getCE32FromOffsetCE32Eaij, .-_ZNK6icu_6720CollationDataBuilder21getCE32FromOffsetCE32Eaij
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720CollationDataBuilder10isAssignedEi
	.type	_ZNK6icu_6720CollationDataBuilder10isAssignedEi, @function
_ZNK6icu_6720CollationDataBuilder10isAssignedEi:
.LFB3461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	utrie2_get32_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$192, %eax
	setne	%dl
	cmpl	$-1, %eax
	setne	%al
	andl	%edx, %eax
	ret
	.cfi_endproc
.LFE3461:
	.size	_ZNK6icu_6720CollationDataBuilder10isAssignedEi, .-_ZNK6icu_6720CollationDataBuilder10isAssignedEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720CollationDataBuilder24getLongPrimaryIfSingleCEEi
	.type	_ZNK6icu_6720CollationDataBuilder24getLongPrimaryIfSingleCEEi, @function
_ZNK6icu_6720CollationDataBuilder24getLongPrimaryIfSingleCEEi:
.LFB3462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	utrie2_get32_67@PLT
	xorl	%r8d, %r8d
	cmpb	$-65, %al
	jbe	.L210
	movl	%eax, %edx
	xorb	%al, %al
	andl	$15, %edx
	cmpl	$1, %edx
	cmove	%eax, %r8d
.L210:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3462:
	.size	_ZNK6icu_6720CollationDataBuilder24getLongPrimaryIfSingleCEEi, .-_ZNK6icu_6720CollationDataBuilder24getLongPrimaryIfSingleCEEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720CollationDataBuilder11getSingleCEEiR10UErrorCode
	.type	_ZNK6icu_6720CollationDataBuilder11getSingleCEEiR10UErrorCode, @function
_ZNK6icu_6720CollationDataBuilder11getSingleCEEiR10UErrorCode:
.LFB3463:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L216
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	call	utrie2_get32_67@PLT
	cmpl	$192, %eax
	je	.L264
.L218:
	cmpb	$-65, %al
	jbe	.L225
	leaq	.L228(%rip), %r14
.L224:
	movl	%eax, %edx
	andl	$15, %edx
	movslq	(%r14,%rdx,4), %rdx
	addq	%r14, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L228:
	.long	.L226-.L228
	.long	.L236-.L228
	.long	.L235-.L228
	.long	.L226-.L228
	.long	.L230-.L228
	.long	.L234-.L228
	.long	.L233-.L228
	.long	.L230-.L228
	.long	.L230-.L228
	.long	.L230-.L228
	.long	.L232-.L228
	.long	.L231-.L228
	.long	.L230-.L228
	.long	.L230-.L228
	.long	.L229-.L228
	.long	.L227-.L228
	.text
	.p2align 4,,10
	.p2align 3
.L233:
	movl	%eax, %edx
	shrl	$8, %edx
	andl	$31, %edx
	cmpl	$1, %edx
	je	.L265
	.p2align 4,,10
	.p2align 3
.L230:
	movl	$16, (%rbx)
.L261:
	xorl	%eax, %eax
.L215:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	andl	$-256, %eax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L236:
	movabsq	$-1099511627776, %rdx
	salq	$32, %rax
	andq	%rdx, %rax
	orq	$83887360, %rax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L227:
	movl	%r12d, %edi
	call	_ZN6icu_679Collation30unassignedPrimaryFromCodePointEi@PLT
	salq	$32, %rax
	orq	$83887360, %rax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L229:
	shrl	$13, %eax
	testb	%r15b, %r15b
	je	.L243
	movq	16(%r13), %rdx
	movq	16(%rdx), %rdx
	movq	(%rdx,%rax,8), %rsi
.L244:
	movl	%r12d, %edi
	call	_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil@PLT
	movl	%eax, %edx
	orb	$-63, %al
	movzbl	%dl, %edx
	orb	$-63, %dl
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L231:
	testb	%r15b, %r15b
	jne	.L266
	movl	48(%r13), %eax
	testl	%eax, %eax
	jle	.L252
	movq	64(%r13), %rax
	movl	(%rax), %eax
	movzbl	%al, %edx
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L232:
	movl	48(%r13), %edx
	testl	%edx, %edx
	jle	.L252
	shrl	$13, %eax
.L263:
	subl	%eax, %edx
	testl	%edx, %edx
	jle	.L252
	movq	64(%r13), %rdx
	movl	(%rdx,%rax,4), %eax
	movzbl	%al, %edx
	.p2align 4,,10
	.p2align 3
.L239:
	cmpl	$191, %edx
	ja	.L224
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L234:
	movl	%eax, %edx
	shrl	$8, %edx
	andl	$31, %edx
	cmpl	$1, %edx
	jne	.L230
	shrl	$13, %eax
	testb	%r15b, %r15b
	je	.L238
	movq	16(%r13), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx,%rax,4), %eax
	movzbl	%al, %edx
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L238:
	movl	48(%r13), %edx
	testl	%edx, %edx
	jg	.L263
	.p2align 4,,10
	.p2align 3
.L252:
	xorl	%eax, %eax
.L225:
	movabsq	$-281474976710656, %rcx
	movq	%rax, %rdx
	salq	$32, %rdx
	andq	%rcx, %rdx
	movl	%eax, %ecx
	sall	$8, %eax
	sall	$16, %ecx
	andl	$65280, %eax
	andl	$-16777216, %ecx
	orq	%rcx, %rdx
	orq	%rdx, %rax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L226:
	movl	$5, (%rbx)
	xorl	%eax, %eax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L264:
	movq	16(%r13), %rax
	movq	(%rax), %rdx
	movq	16(%rdx), %rcx
	cmpl	$55295, %r12d
	ja	.L219
	movl	%r12d, %eax
	movq	(%rdx), %rdx
	sarl	$5, %eax
	cltq
	movzwl	(%rdx,%rax,2), %edx
.L258:
	movl	%r12d, %eax
	andl	$31, %eax
	leal	(%rax,%rdx,4), %eax
	cltq
	salq	$2, %rax
.L220:
	movl	(%rcx,%rax), %eax
	movl	$1, %r15d
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L219:
	cmpl	$65535, %r12d
	jbe	.L267
	movl	$512, %eax
	cmpl	$1114111, %r12d
	ja	.L220
	cmpl	44(%rdx), %r12d
	jl	.L223
	movslq	48(%rdx), %rax
	salq	$2, %rax
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L267:
	cmpl	$56320, %r12d
	movl	$320, %eax
	movq	(%rdx), %rsi
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%r12d, %eax
	sarl	$5, %eax
.L259:
	addl	%edx, %eax
	cltq
	movzwl	(%rsi,%rax,2), %edx
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L266:
	movq	16(%r13), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
	movzbl	%al, %edx
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L243:
	xorl	%esi, %esi
	cmpl	80(%r13), %eax
	jge	.L244
	movq	96(%r13), %rdx
	cltq
	movq	(%rdx,%rax,8), %rsi
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L265:
	shrl	$13, %eax
	testb	%r15b, %r15b
	je	.L240
	movq	16(%r13), %rdx
	movq	16(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L240:
	cmpl	80(%r13), %eax
	jge	.L261
	movq	96(%r13), %rdx
	cltq
	movq	(%rdx,%rax,8), %rax
	jmp	.L215
.L223:
	movl	%r12d, %eax
	movq	(%rdx), %rsi
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rsi,%rax,2), %edx
	movl	%r12d, %eax
	sarl	$5, %eax
	andl	$63, %eax
	jmp	.L259
	.cfi_endproc
.LFE3463:
	.size	_ZNK6icu_6720CollationDataBuilder11getSingleCEEiR10UErrorCode, .-_ZNK6icu_6720CollationDataBuilder11getSingleCEEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder5addCEElR10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder5addCEElR10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder5addCEElR10UErrorCode:
.LFB3464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	80(%rdi), %r13d
	testl	%r13d, %r13d
	jle	.L269
	movq	96(%rdi), %rsi
	xorl	%eax, %eax
	leal	-1(%r13), %edi
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L280:
	leaq	1(%rax), %rcx
	cmpq	%rax, %rdi
	je	.L271
	movq	%rcx, %rax
.L272:
	movl	%eax, %r8d
	cmpq	(%rsi,%rax,8), %rbx
	jne	.L280
.L268:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	leaq	72(%r12), %rdi
	leal	1(%r13), %esi
.L275:
	cmpl	84(%r12), %esi
	jle	.L276
.L273:
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movl	%r13d, %r8d
	testb	%al, %al
	je	.L268
	movslq	80(%r12), %rax
	leal	1(%rax), %esi
.L274:
	movq	96(%r12), %rdx
	movl	%r13d, %r8d
	movq	%rbx, (%rdx,%rax,8)
	movl	%r8d, %eax
	movl	%esi, 80(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L269:
	.cfi_restore_state
	movl	%r13d, %esi
	leaq	72(%rdi), %rdi
	addl	$1, %esi
	js	.L273
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L276:
	movslq	%r13d, %rax
	jmp	.L274
	.cfi_endproc
.LFE3464:
	.size	_ZN6icu_6720CollationDataBuilder5addCEElR10UErrorCode, .-_ZN6icu_6720CollationDataBuilder5addCEElR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder7addCE32EjR10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder7addCE32EjR10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder7addCE32EjR10UErrorCode:
.LFB3465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	48(%rdi), %ebx
	testl	%ebx, %ebx
	jle	.L282
	leal	-1(%rbx), %edi
	xorl	%eax, %eax
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L295:
	leaq	1(%rax), %rcx
	cmpq	%rdi, %rax
	je	.L285
	movq	%rcx, %rax
.L286:
	movl	%ebx, %esi
	movl	%eax, %r8d
	xorl	%ecx, %ecx
	subl	%eax, %esi
	testl	%esi, %esi
	jle	.L283
	movq	64(%r13), %rcx
	movl	(%rcx,%rax,4), %ecx
.L283:
	cmpl	%ecx, %r12d
	jne	.L295
.L281:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	leaq	40(%r13), %rdi
	leal	1(%rbx), %esi
.L289:
	cmpl	%esi, 52(%r13)
	jge	.L291
.L287:
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movl	%ebx, %r8d
	testb	%al, %al
	je	.L281
	movslq	48(%r13), %rax
.L288:
	movq	64(%r13), %rdx
	movl	%ebx, %r8d
	movl	%r12d, (%rdx,%rax,4)
	movl	%r8d, %eax
	addl	$1, 48(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L282:
	.cfi_restore_state
	movl	%ebx, %esi
	leaq	40(%rdi), %rdi
	addl	$1, %esi
	js	.L287
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L291:
	movslq	%ebx, %rax
	jmp	.L288
	.cfi_endproc
.LFE3465:
	.size	_ZN6icu_6720CollationDataBuilder7addCE32EjR10UErrorCode, .-_ZN6icu_6720CollationDataBuilder7addCE32EjR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder18addConditionalCE32ERKNS_13UnicodeStringEjR10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder18addConditionalCE32ERKNS_13UnicodeStringEjR10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder18addConditionalCE32ERKNS_13UnicodeStringEjR10UErrorCode:
.LFB3466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L302
	movl	112(%rdi), %r14d
	movq	%rdi, %rbx
	movq	%rcx, %r15
	cmpl	$524287, %r14d
	jg	.L303
	movl	$88, %edi
	movq	%rsi, -56(%rbp)
	movl	%edx, %r13d
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L299
	movq	-56(%rbp), %rsi
	leaq	8(%rax), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	%r13d, 72(%r12)
	movq	%r15, %rdx
	movq	%r12, %rsi
	movabsq	$-4294967295, %rax
	leaq	104(%rbx), %rdi
	movl	$1, 76(%r12)
	movq	%rax, 80(%r12)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L296:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movl	$15, (%rcx)
	movl	$-1, %r14d
	jmp	.L296
.L299:
	movl	$7, (%r15)
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$-1, %r14d
	jmp	.L296
	.cfi_endproc
.LFE3466:
	.size	_ZN6icu_6720CollationDataBuilder18addConditionalCE32ERKNS_13UnicodeStringEjR10UErrorCode, .-_ZN6icu_6720CollationDataBuilder18addConditionalCE32ERKNS_13UnicodeStringEjR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder17encodeOneCEAsCE32El
	.type	_ZN6icu_6720CollationDataBuilder17encodeOneCEAsCE32El, @function
_ZN6icu_6720CollationDataBuilder17encodeOneCEAsCE32El:
.LFB3469:
	.cfi_startproc
	endbr64
	movabsq	$281470698455295, %rcx
	movq	%rdi, %rdx
	sarq	$32, %rdi
	testq	%rcx, %rdx
	je	.L309
	movabsq	$1099511627775, %rcx
	andq	%rdx, %rcx
	cmpq	$83887360, %rcx
	je	.L310
	movzbl	%dl, %eax
	orb	$-62, %dl
	orl	%edi, %eax
	movl	$1, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	movl	%edi, %eax
	orb	$-63, %al
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	movl	%edx, %eax
	shrl	$16, %eax
	orl	%eax, %edi
	movzbl	%dh, %eax
	orl	%edi, %eax
	ret
	.cfi_endproc
.LFE3469:
	.size	_ZN6icu_6720CollationDataBuilder17encodeOneCEAsCE32El, .-_ZN6icu_6720CollationDataBuilder17encodeOneCEAsCE32El
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder11encodeOneCEElR10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder11encodeOneCEElR10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder11encodeOneCEElR10UErrorCode:
.LFB3470:
	.cfi_startproc
	endbr64
	movabsq	$281470698455295, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	sarq	$32, %rsi
	subq	$24, %rsp
	testq	%rcx, %rbx
	je	.L330
	movabsq	$1099511627775, %rcx
	movl	%esi, %eax
	andq	%rbx, %rcx
	orb	$-63, %al
	cmpq	$83887360, %rcx
	je	.L311
	movzbl	%bl, %eax
	orl	%esi, %eax
	je	.L331
.L314:
	movl	80(%r12), %r13d
	testl	%r13d, %r13d
	jle	.L316
	movq	96(%r12), %rdi
	leal	-1(%r13), %esi
	xorl	%eax, %eax
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L332:
	leaq	1(%rax), %rcx
	cmpq	%rax, %rsi
	je	.L318
	movq	%rcx, %rax
.L319:
	movl	%eax, %ecx
	cmpq	(%rdi,%rax,8), %rbx
	jne	.L332
.L317:
	movl	(%rdx), %esi
	xorl	%eax, %eax
	testl	%esi, %esi
	jg	.L311
.L333:
	movl	%ecx, %eax
	sall	$13, %eax
	orl	$454, %eax
	cmpl	$524287, %ecx
	jle	.L311
	movl	$15, (%rdx)
	xorl	%eax, %eax
.L311:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	movl	%ebx, %eax
	movzbl	%bh, %ecx
	shrl	$16, %eax
	orl	%esi, %eax
	orl	%ecx, %eax
	cmpl	$1, %eax
	je	.L314
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	addq	$24, %rsp
	movl	%ebx, %eax
	orb	$-62, %al
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore_state
	leaq	72(%r12), %rdi
	leal	1(%r13), %esi
.L323:
	cmpl	84(%r12), %esi
	jle	.L324
.L320:
	movq	%rdx, -40(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-40(%rbp), %rdx
	movl	%r13d, %ecx
	testb	%al, %al
	je	.L317
	movslq	80(%r12), %rax
	leal	1(%rax), %esi
.L321:
	movq	96(%r12), %rcx
	movq	%rbx, (%rcx,%rax,8)
	movl	%r13d, %ecx
	xorl	%eax, %eax
	movl	%esi, 80(%r12)
	movl	(%rdx), %esi
	testl	%esi, %esi
	jle	.L333
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L316:
	movl	%r13d, %esi
	leaq	72(%r12), %rdi
	addl	$1, %esi
	js	.L320
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L324:
	movslq	%r13d, %rax
	jmp	.L321
	.cfi_endproc
.LFE3470:
	.size	_ZN6icu_6720CollationDataBuilder11encodeOneCEElR10UErrorCode, .-_ZN6icu_6720CollationDataBuilder11encodeOneCEElR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder9encodeCEsEPKliR10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder9encodeCEsEPKliR10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder9encodeCEsEPKliR10UErrorCode:
.LFB3471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$144, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L351
	movl	%edx, %r13d
	movq	%rcx, %r12
	cmpl	$31, %edx
	ja	.L366
	movq	%rdi, %r14
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L339
	movq	%rsi, %r15
	call	utrie2_isFrozen_67@PLT
	testb	%al, %al
	jne	.L339
	testl	%r13d, %r13d
	je	.L351
	cmpl	$1, %r13d
	je	.L367
	cmpl	$2, %r13d
	je	.L368
.L342:
	movabsq	$281470698455295, %rdi
	leal	-1(%r13), %r8d
	xorl	%ecx, %ecx
	movabsq	$1099511627775, %r9
	.p2align 4,,10
	.p2align 3
.L350:
	movq	(%r15,%rcx,8), %rax
	movq	%rax, %rsi
	sarq	$32, %rsi
	testq	%rdi, %rax
	je	.L369
	movq	%rax, %rdx
	andq	%r9, %rdx
	cmpq	$83887360, %rdx
	je	.L370
	movzbl	%al, %edx
	orl	%esi, %edx
	je	.L371
.L346:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L351
	movq	%r12, %rcx
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6720CollationDataBuilder15encodeExpansionEPKliR10UErrorCode.part.0
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L351:
	xorl	%eax, %eax
.L334:
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L372
	addq	$144, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	movl	$27, (%r12)
	xorl	%eax, %eax
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L366:
	movl	$1, (%rcx)
	xorl	%eax, %eax
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L369:
	movl	%eax, %edx
	movzbl	%ah, %eax
	shrl	$16, %edx
	orl	%esi, %edx
	orl	%edx, %eax
	cmpl	$1, %eax
	je	.L346
.L347:
	leaq	-176(%rbp), %rsi
	movl	%eax, (%rsi,%rcx,4)
	leaq	1(%rcx), %rax
	cmpq	%r8, %rcx
	je	.L349
	movq	%rax, %rcx
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L370:
	movl	%esi, %eax
	orb	$-63, %al
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L371:
	orb	$-62, %al
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L368:
	movabsq	$72057594037862655, %rdx
	movq	(%r15), %rax
	andq	%rax, %rdx
	cmpq	$83886080, %rdx
	jne	.L342
	movq	8(%r15), %rcx
	movq	%rax, %rsi
	movabsq	$-4278190081, %rdx
	sarq	$32, %rsi
	andq	%rcx, %rdx
	cmpq	$1280, %rdx
	jne	.L342
	testl	%esi, %esi
	je	.L342
	sall	$8, %eax
	sarq	$16, %rcx
	andl	$16711680, %eax
	orl	%esi, %ecx
	orl	%ecx, %eax
	orb	$-60, %al
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L349:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L351
	movq	%r12, %rcx
	movl	%r13d, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6720CollationDataBuilder17encodeExpansion32EPKiiR10UErrorCode.part.0
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L367:
	movq	(%r15), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6720CollationDataBuilder11encodeOneCEElR10UErrorCode
	jmp	.L334
.L372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3471:
	.size	_ZN6icu_6720CollationDataBuilder9encodeCEsEPKliR10UErrorCode, .-_ZN6icu_6720CollationDataBuilder9encodeCEsEPKliR10UErrorCode
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB0:
	.text
.LHOTB0:
	.align 2
	.p2align 4
	.type	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0, @function
_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0:
.LFB4583:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$360, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edx, %eax
	leaq	.L376(%rip), %rdx
	andl	$15, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L376:
	.long	.L374-.L376
	.long	.L416-.L376
	.long	.L416-.L376
	.long	.L374-.L376
	.long	.L416-.L376
	.long	.L382-.L376
	.long	.L381-.L376
	.long	.L374-.L376
	.long	.L380-.L376
	.long	.L379-.L376
	.long	.L374-.L376
	.long	.L374-.L376
	.long	.L378-.L376
	.long	.L374-.L376
	.long	.L377-.L376
	.long	.L375-.L376
	.text
	.p2align 4,,10
	.p2align 3
.L416:
	movl	%r13d, %r15d
.L373:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L430
	addq	$360, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	movl	%esi, %edi
	call	_ZN6icu_679Collation30unassignedPrimaryFromCodePointEi@PLT
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	salq	$32, %rsi
	orq	$83887360, %rsi
	call	_ZN6icu_6720CollationDataBuilder11encodeOneCEElR10UErrorCode
	movl	%eax, %r15d
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L382:
	movq	16(%rdi), %rax
	movl	%r13d, %edx
	shrl	$13, %r13d
	movq	%r8, %rcx
	shrl	$8, %edx
	movq	8(%rax), %rax
	andl	$31, %edx
	leaq	(%rax,%r13,4), %rsi
	call	_ZN6icu_6720CollationDataBuilder17encodeExpansion32EPKiiR10UErrorCode.part.0
	movl	%eax, %r15d
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L381:
	movq	16(%rdi), %rax
	movl	%r13d, %edx
	shrl	$13, %r13d
	movq	%r8, %rcx
	shrl	$8, %edx
	movq	16(%rax), %rax
	andl	$31, %edx
	leaq	(%rax,%r13,8), %rsi
	call	_ZN6icu_6720CollationDataBuilder15encodeExpansionEPKliR10UErrorCode.part.0
	movl	%eax, %r15d
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L380:
	movq	16(%rdi), %rax
	shrl	$13, %r13d
	movq	24(%rax), %rax
	leaq	(%rax,%r13,2), %rdi
	movzwl	(%rdi), %eax
	movzwl	2(%rdi), %r15d
	movq	%rdi, -368(%rbp)
	sall	$16, %eax
	orl	%eax, %r15d
	testb	%cl, %cl
	jne	.L384
	movl	(%r8), %edi
	testl	%edi, %edi
	jg	.L417
	cmpb	$-65, %r15b
	ja	.L429
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L379:
	testb	%cl, %cl
	jne	.L408
	movl	(%r8), %ecx
	xorl	%r15d, %r15d
	testl	%ecx, %ecx
	jg	.L373
	movq	16(%rdi), %rax
	shrl	$13, %r13d
	movq	24(%rax), %rax
	leaq	(%rax,%r13,2), %rdx
	movzwl	(%rdx), %eax
	movzwl	2(%rdx), %edx
	sall	$16, %eax
	orl	%edx, %eax
	movl	%eax, %r15d
	cmpb	$-65, %dl
	jbe	.L373
.L429:
	movl	%r15d, %edx
	movq	%r14, %r8
	xorl	%ecx, %ecx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0
	movl	%eax, %r15d
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L378:
	movl	$16, (%r8)
	movl	%r13d, %r15d
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L377:
	movq	16(%rdi), %rax
	shrl	$13, %r13d
	movl	%r12d, %edi
	movq	16(%rax), %rax
	movq	(%rax,%r13,8), %rsi
	call	_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil@PLT
	orb	$-63, %al
	movl	%eax, %r15d
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L384:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %esi
	leaq	-336(%rbp), %r13
	movq	%rax, -264(%rbp)
	movq	%r13, %rdi
	movabsq	$4294967296, %rax
	movw	%si, -256(%rbp)
	xorl	%esi, %esi
	movq	%rax, -200(%rbp)
	movabsq	$-4294967295, %rax
	movq	%rax, -192(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movl	(%r14), %eax
	cmpb	$-65, %r15b
	jbe	.L386
	movl	%r15d, %edx
	andl	$15, %edx
	cmpl	$9, %edx
	je	.L431
	testl	%eax, %eax
	jg	.L423
	movl	%r15d, %edx
	movq	%r14, %r8
	movl	$1, %ecx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0
	movl	%eax, %r15d
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L423
.L414:
	movl	112(%rbx), %r8d
	cmpl	$524287, %r8d
	jg	.L432
	movl	$88, %edi
	movl	%r8d, -376(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L392
	leaq	8(%rax), %rdi
	movq	%r13, %rsi
	movq	%rax, -360(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-360(%rbp), %r9
	leaq	104(%rbx), %rdi
	movq	%r14, %rdx
	movabsq	$-4294967295, %rax
	movq	%rax, 80(%r9)
	movq	%r9, %rsi
	movl	%r15d, 72(%r9)
	movl	$1, 76(%r9)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%r14), %eax
	movl	-376(%rbp), %r8d
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L408:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-272(%rbp), %r10
	xorl	%esi, %esi
	movq	%rax, -168(%rbp)
	movl	$2, %eax
	movq	%r10, %rdi
	movw	%ax, -160(%rbp)
	movabsq	$4294967296, %rax
	movq	%rax, -104(%rbp)
	movabsq	$-4294967295, %rax
	movq	%r10, -360(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movl	(%r14), %edx
	movq	-360(%rbp), %r10
	testl	%edx, %edx
	jg	.L410
	movq	%r10, %rsi
	movq	%r14, %r9
	leaq	-176(%rbp), %r8
	movl	%r13d, %ecx
	movl	%r12d, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6720CollationDataBuilder28copyContractionsFromBaseCE32ERNS_13UnicodeStringEijPNS_15ConditionalCE32ER10UErrorCode.part.0
	movq	-360(%rbp), %r10
.L410:
	movl	-92(%rbp), %eax
	leaq	144(%rbx), %rdi
	movl	%r12d, %esi
	movq	%r10, -360(%rbp)
	sall	$13, %eax
	orb	$-57, %al
	movl	%eax, %r15d
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	-360(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-168(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L386:
	movl	$-1, %r8d
	testl	%eax, %eax
	jle	.L414
.L390:
	movl	%r8d, -188(%rbp)
.L389:
	testl	%eax, %eax
	jle	.L433
.L401:
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-264(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L417:
	xorl	%r15d, %r15d
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L433:
	leaq	104(%rbx), %r15
	movl	%r8d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	leaq	-344(%rbp), %rsi
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%rax, -360(%rbp)
	movq	-368(%rbp), %rax
	movq	%rsi, -392(%rbp)
	addq	$4, %rax
	movq	%rax, -344(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -384(%rbp)
	call	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode@PLT
	movq	%rbx, -368(%rbp)
	movl	%r12d, -396(%rbp)
.L407:
	movq	-384(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	je	.L393
	leaq	-136(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzwl	-328(%rbp), %eax
	testw	%ax, %ax
	js	.L394
	movswl	%ax, %edx
	sarl	$5, %edx
.L395:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReverseEii@PLT
	movswl	-328(%rbp), %eax
	testw	%ax, %ax
	js	.L396
	sarl	$5, %eax
.L397:
	movq	-392(%rbp), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %r9d
	movq	%r13, %rdi
	movw	%ax, -344(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movl	-68(%rbp), %r12d
	movl	(%r14), %eax
	cmpb	$-65, %r12b
	jbe	.L398
	movl	%r12d, %ecx
	andl	$15, %ecx
	cmpl	$9, %ecx
	je	.L434
	testl	%eax, %eax
	jg	.L422
	movl	-396(%rbp), %esi
	movq	-368(%rbp), %rdi
	movl	%r12d, %edx
	movq	%r14, %r8
	movl	$1, %ecx
	call	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0
	movl	%eax, %r12d
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L422
.L411:
	movq	-368(%rbp), %rax
	movl	112(%rax), %ebx
	cmpl	$524287, %ebx
	jg	.L435
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L406
	leaq	8(%rax), %rdi
	movq	%r13, %rsi
	movq	%rax, -376(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-376(%rbp), %r8
	movq	%r14, %rdx
	movq	%r15, %rdi
	movabsq	$-4294967295, %rax
	movq	%rax, 80(%r8)
	movq	%r8, %rsi
	movl	%r12d, 72(%r8)
	movl	$1, 76(%r8)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%r14), %eax
.L403:
	movq	-360(%rbp), %rcx
	movl	%ebx, 84(%rcx)
.L402:
	testl	%eax, %eax
	jg	.L405
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, -360(%rbp)
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L398:
	movl	$-1, %ebx
	testl	%eax, %eax
	jg	.L403
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L396:
	movl	-324(%rbp), %eax
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L394:
	movl	-324(%rbp), %edx
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L434:
	testl	%eax, %eax
	jg	.L405
	movq	-360(%rbp), %r8
	movq	%r14, %r9
	movl	%r12d, %ecx
	movq	%r13, %rsi
	movl	-396(%rbp), %edx
	movq	-368(%rbp), %rdi
	call	_ZN6icu_6720CollationDataBuilder28copyContractionsFromBaseCE32ERNS_13UnicodeStringEijPNS_15ConditionalCE32ER10UErrorCode.part.0
	movl	%eax, %ebx
	movl	(%r14), %eax
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L393:
	movl	-188(%rbp), %eax
	movq	-368(%rbp), %rbx
	movl	-396(%rbp), %r12d
	sall	$13, %eax
	leaq	144(%rbx), %rdi
	movl	%r12d, %esi
	orb	$-57, %al
	movl	%eax, %r15d
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	-384(%rbp), %rdi
	call	_ZN6icu_6710UCharsTrie8IteratorD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-264(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L373
.L406:
	movq	-360(%rbp), %rax
	movl	$7, (%r14)
	movl	$-1, 84(%rax)
	.p2align 4,,10
	.p2align 3
.L405:
	movq	-384(%rbp), %rdi
	call	_ZN6icu_6710UCharsTrie8IteratorD1Ev@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L431:
	testl	%eax, %eax
	jg	.L401
	leaq	-272(%rbp), %r8
	movq	%r14, %r9
	movl	%r15d, %ecx
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6720CollationDataBuilder28copyContractionsFromBaseCE32ERNS_13UnicodeStringEijPNS_15ConditionalCE32ER10UErrorCode.part.0
	movl	%eax, %r8d
	movl	(%r14), %eax
	jmp	.L389
.L435:
	movq	-360(%rbp), %rax
	movl	$15, (%r14)
	movl	$-1, 84(%rax)
	jmp	.L405
.L422:
	movl	$-1, %ebx
	jmp	.L403
.L432:
	movl	$15, (%r14)
	movl	$-1, -188(%rbp)
	jmp	.L401
.L423:
	orl	$-1, %r8d
	jmp	.L390
.L430:
	call	__stack_chk_fail@PLT
.L392:
	movl	$7, (%r14)
	movl	$-1, -188(%rbp)
	jmp	.L401
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0.cold, @function
_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0.cold:
.LFSB4583:
.L374:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE4583:
	.text
	.size	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0, .-_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0
	.section	.text.unlikely
	.size	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0.cold, .-_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0.cold
.LCOLDE0:
	.text
.LHOTE0:
	.align 2
	.p2align 4
	.type	_ZN6icu_6720CollationDataBuilder7addCE32ERKNS_13UnicodeStringES3_jR10UErrorCode.part.0, @function
_ZN6icu_6720CollationDataBuilder7addCE32ERKNS_13UnicodeStringES3_jR10UErrorCode.part.0:
.LFB4584:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -208(%rbp)
	xorl	%esi, %esi
	movl	%ecx, -220(%rbp)
	movq	%r8, -200(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	32(%r13), %rdi
	cmpl	$65535, %eax
	movl	%eax, %esi
	movl	%eax, %r12d
	seta	%r15b
	call	utrie2_get32_67@PLT
	addl	$1, %r15d
	movl	%eax, %ebx
	movq	-208(%rbp), %rax
	movswl	8(%rax), %eax
	shrl	$5, %eax
	jne	.L438
	movswl	8(%r14), %eax
	testw	%ax, %ax
	js	.L439
	sarl	$5, %eax
	cmpl	%eax, %r15d
	setl	%r8b
	cmpl	$192, %ebx
	je	.L486
.L441:
	movzbl	%bl, %ecx
	cmpl	%eax, %r15d
	jl	.L452
.L488:
	cmpl	$191, %ecx
	ja	.L517
.L448:
	movq	32(%r13), %rdi
	movq	-200(%rbp), %rcx
	movl	%r12d, %esi
	movl	-220(%rbp), %edx
	call	utrie2_set32_67@PLT
.L453:
	movb	$1, 608(%r13)
.L436:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L518
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	cmpl	$192, %ebx
	je	.L499
.L510:
	movzbl	%bl, %ecx
.L452:
	cmpl	$191, %ecx
	jbe	.L454
	movl	%ebx, %eax
	andl	$15, %eax
	cmpl	$7, %eax
	je	.L519
.L454:
	leaq	-128(%rbp), %rax
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movq	-200(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L496
	movl	112(%r13), %eax
	movl	%eax, -224(%rbp)
	cmpl	$524287, %eax
	jg	.L520
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L458
	movq	-216(%rbp), %rsi
	leaq	8(%rax), %rdi
	movq	%rax, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-232(%rbp), %rax
	movq	-200(%rbp), %rdx
	leaq	104(%r13), %rdi
	movl	%ebx, 72(%rax)
	movq	%rax, %rsi
	movabsq	$-4294967295, %rbx
	movl	$1, 76(%rax)
	movq	%rbx, 80(%rax)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L456:
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-200(%rbp), %rax
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jg	.L436
	movl	-224(%rbp), %ebx
	movq	32(%r13), %rdi
	movq	%rax, %rcx
	movl	%r12d, %esi
	movl	%ebx, %edx
	sall	$13, %edx
	orb	$-57, %dl
	call	utrie2_set32_67@PLT
	movl	%r12d, %esi
	leaq	144(%r13), %rdi
	leaq	104(%r13), %r12
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rbx
.L455:
	leaq	-192(%rbp), %rax
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_i@PLT
	movq	-208(%rbp), %rax
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	js	.L460
	movswl	%ax, %esi
	sarl	$5, %esi
.L461:
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movq	-208(%rbp), %rax
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	js	.L462
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L463:
	movq	-216(%rbp), %rdi
	movq	-208(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movzwl	-184(%rbp), %edx
	movq	%rax, %rdi
	testw	%dx, %dx
	js	.L464
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L465:
	movq	-232(%rbp), %r15
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r15, %rsi
	leaq	408(%r13), %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKNS_13UnicodeStringE@PLT
	movq	%r13, -208(%rbp)
	movq	-216(%rbp), %r15
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L523:
	notl	%eax
	andl	$1, %eax
.L477:
	testb	%al, %al
	je	.L521
	movq	%r14, %rbx
.L482:
	movl	84(%rbx), %r13d
	testl	%r13d, %r13d
	js	.L522
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movzwl	16(%rax), %esi
	movq	%rax, %r14
	testw	%si, %si
	js	.L472
	movswl	%si, %ecx
	sarl	$5, %ecx
.L473:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L474
	movswl	%ax, %edx
	sarl	$5, %edx
.L475:
	testb	$1, %sil
	jne	.L523
	testl	%ecx, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L478
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L478:
	andl	$2, %esi
	leaq	18(%r14), %rcx
	jne	.L480
	movq	32(%r14), %rcx
.L480:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jns	.L477
	movq	-200(%rbp), %rax
	movl	%r13d, %r15d
	movq	-208(%rbp), %r13
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L469
	movl	112(%r13), %edx
	movl	%edx, -208(%rbp)
	cmpl	$524287, %edx
	jg	.L481
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L470
	movq	-216(%rbp), %rsi
	leaq	8(%rax), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	-220(%rbp), %edi
	movl	$1, 76(%r14)
	movq	%r14, %rsi
	movq	-200(%rbp), %rdx
	movl	%edi, 72(%r14)
	movabsq	$-4294967295, %rdi
	movq	%rdi, 80(%r14)
	movq	%r12, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	-200(%rbp), %rax
	movl	(%rax), %edi
	testl	%edi, %edi
	jle	.L524
.L469:
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-232(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L439:
	movl	12(%r14), %eax
	cmpl	%eax, %r15d
	setl	%r8b
	cmpl	$192, %ebx
	jne	.L441
.L486:
	movq	16(%r13), %rdi
	movq	(%rdi), %rcx
	movq	16(%rcx), %rdx
	cmpl	$55295, %r12d
	jbe	.L525
	cmpl	$65535, %r12d
	ja	.L444
	cmpl	$56320, %r12d
	movq	(%rcx), %rsi
	movl	$320, %eax
	movl	$0, %ecx
	cmovl	%eax, %ecx
	movl	%r12d, %eax
	sarl	$5, %eax
	addl	%ecx, %eax
	cltq
	movzwl	(%rsi,%rax,2), %ecx
.L513:
	movl	%r12d, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
.L443:
	movl	(%rdx,%rax), %esi
	movb	%r8b, -216(%rbp)
	call	_ZNK6icu_6713CollationData12getFinalCE32Ej@PLT
	movzbl	-216(%rbp), %r8d
	movl	%eax, %ebx
	testb	%r8b, %r8b
	jne	.L447
	cmpb	$-65, %al
	jbe	.L448
	andl	$15, %eax
	subl	$8, %eax
	cmpl	$1, %eax
	ja	.L448
	movq	-200(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L526
	movl	%ebx, %edx
	movq	%rax, %r8
	movl	$1, %ecx
	movl	%r12d, %esi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0
	movq	32(%r13), %rdi
	movq	%r15, %rcx
	movl	%r12d, %esi
	movl	%eax, %edx
	movl	%eax, %ebx
	call	utrie2_set32_67@PLT
	movl	(%r15), %edx
	movzbl	%bl, %ecx
	testl	%edx, %edx
	jle	.L488
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L525:
	movl	%r12d, %eax
	movq	(%rcx), %rcx
	sarl	$5, %eax
	cltq
.L512:
	movzwl	(%rcx,%rax,2), %ecx
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L499:
	movl	$1, %r8d
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L519:
	shrl	$13, %ebx
	leaq	104(%r13), %r12
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	$1, 80(%rax)
	movq	%rax, %rbx
	leaq	-128(%rbp), %rax
	movq	%rax, -216(%rbp)
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L474:
	movl	-116(%rbp), %edx
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L472:
	movl	20(%rax), %ecx
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L517:
	movl	%ebx, %eax
	andl	$15, %eax
	cmpl	$7, %eax
	jne	.L448
	shrl	$13, %ebx
	leaq	104(%r13), %rdi
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	-220(%rbp), %ebx
	movl	$1, 80(%rax)
	movl	%ebx, 72(%rax)
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L447:
	movq	-200(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L495
	cmpb	$-65, %bl
	jbe	.L514
	movl	$1, %ecx
	movl	%ebx, %edx
	movq	%r13, %rdi
	movq	%rax, %r8
	movl	%r12d, %esi
	call	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0
	movq	32(%r13), %rdi
	movq	-200(%rbp), %rcx
	movl	%eax, %ebx
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L495:
	xorl	%ebx, %ebx
.L514:
	movq	32(%r13), %rdi
	movq	%rax, %rcx
.L516:
	movl	%ebx, %edx
	movl	%r12d, %esi
	call	utrie2_set32_67@PLT
	movq	-200(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L510
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L464:
	movl	-180(%rbp), %ecx
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L462:
	movq	-208(%rbp), %rax
	movl	12(%rax), %ecx
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L460:
	movq	-208(%rbp), %rax
	movzwl	12(%rax), %esi
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L522:
	movq	-200(%rbp), %rax
	movq	-208(%rbp), %r13
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L469
	movl	112(%r13), %r15d
	cmpl	$524287, %r15d
	jg	.L481
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L470
	movq	-216(%rbp), %rsi
	leaq	8(%rax), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	-220(%rbp), %eax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1, 76(%r14)
	movq	-200(%rbp), %rdx
	movl	%eax, 72(%r14)
	movabsq	$-4294967295, %rax
	movq	%rax, 80(%r14)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	-200(%rbp), %rax
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jg	.L469
	movl	%r15d, 84(%rbx)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L521:
	movl	-220(%rbp), %eax
	movq	-208(%rbp), %r13
	movl	%eax, 72(%r14)
.L471:
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-232(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L444:
	movl	$512, %eax
	cmpl	$1114111, %r12d
	ja	.L443
	cmpl	44(%rcx), %r12d
	jl	.L446
	movslq	48(%rcx), %rax
	salq	$2, %rax
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L524:
	movl	-208(%rbp), %edx
	movq	%r12, %rdi
	movl	%edx, 84(%rbx)
	movl	%edx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%r15d, 84(%rax)
	jmp	.L471
.L446:
	movl	%r12d, %eax
	movq	(%rcx), %rcx
	movl	%r12d, %esi
	sarl	$11, %eax
	sarl	$5, %esi
	addl	$2080, %eax
	andl	$63, %esi
	cltq
	movzwl	(%rcx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	jmp	.L512
.L526:
	movq	32(%r13), %rdi
	movq	%rax, %rbx
	movq	%rax, %rcx
	xorl	%edx, %edx
	movl	%r12d, %esi
	call	utrie2_set32_67@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L448
	jmp	.L436
.L520:
	movq	-200(%rbp), %rax
	movl	$-1, -224(%rbp)
	movl	$15, (%rax)
	jmp	.L456
.L496:
	movl	$-1, -224(%rbp)
	jmp	.L456
.L481:
	movq	-200(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L469
.L518:
	call	__stack_chk_fail@PLT
.L458:
	movq	-200(%rbp), %rax
	movl	$-1, -224(%rbp)
	movl	$7, (%rax)
	jmp	.L456
.L470:
	movq	-200(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L469
	.cfi_endproc
.LFE4584:
	.size	_ZN6icu_6720CollationDataBuilder7addCE32ERKNS_13UnicodeStringES3_jR10UErrorCode.part.0, .-_ZN6icu_6720CollationDataBuilder7addCE32ERKNS_13UnicodeStringES3_jR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder7addCE32ERKNS_13UnicodeStringES3_jR10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder7addCE32ERKNS_13UnicodeStringES3_jR10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder7addCE32ERKNS_13UnicodeStringES3_jR10UErrorCode:
.LFB3468:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L533
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	movswl	8(%rdx), %eax
	shrl	$5, %eax
	je	.L536
	movq	%rdi, %r12
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L531
	movq	%rsi, %r13
	movl	%ecx, %r15d
	call	utrie2_isFrozen_67@PLT
	testb	%al, %al
	je	.L537
.L531:
	movl	$27, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L533:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1, (%r8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rbx, %r8
	movl	%r15d, %ecx
	movq	%r14, %rdx
	popq	%rbx
	.cfi_restore 3
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6720CollationDataBuilder7addCE32ERKNS_13UnicodeStringES3_jR10UErrorCode.part.0
	.cfi_endproc
.LFE3468:
	.size	_ZN6icu_6720CollationDataBuilder7addCE32ERKNS_13UnicodeStringES3_jR10UErrorCode, .-_ZN6icu_6720CollationDataBuilder7addCE32ERKNS_13UnicodeStringES3_jR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6720CollationDataBuilder12getJamoCE32sEPjR10UErrorCode.part.0, @function
_ZN6icu_6720CollationDataBuilder12getJamoCE32sEPjR10UErrorCode.part.0:
.LFB4585:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rdx, -64(%rbp)
	cmpq	$0, 16(%rdi)
	movb	$0, -49(%rbp)
	sete	%r14b
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L552:
	leal	4352(%r15), %r12d
	cmpq	$18, %r15
	jbe	.L540
	leal	4430(%r15), %eax
	leal	4480(%r15), %r8d
	cmpl	$39, %r15d
	cmovg	%r8d, %eax
	movl	%eax, %r12d
.L540:
	movq	32(%r13), %rdi
	movl	%r12d, %esi
	call	utrie2_get32_67@PLT
	cmpl	$192, %eax
	setne	%dl
	cmpl	$-1, %eax
	setne	%cl
	andl	%ecx, %edx
	xorl	%ecx, %ecx
	orl	%edx, %r14d
	cmpl	$192, %eax
	jne	.L542
	movq	16(%r13), %rax
	movq	(%rax), %rdx
	movl	%r12d, %eax
	sarl	$5, %eax
	movq	(%rdx), %rcx
	cltq
	movq	16(%rdx), %rdx
	movzwl	(%rcx,%rax,2), %ecx
	movl	%r12d, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	movl	$1, %ecx
	cltq
	movl	(%rdx,%rax,4), %eax
.L542:
	cmpb	$-65, %al
	jbe	.L543
	movl	%eax, %edx
	leaq	.L545(%rip), %rsi
	andl	$15, %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L545:
	.long	.L547-.L545
	.long	.L543-.L545
	.long	.L543-.L545
	.long	.L547-.L545
	.long	.L543-.L545
	.long	.L548-.L545
	.long	.L548-.L545
	.long	.L547-.L545
	.long	.L548-.L545
	.long	.L548-.L545
	.long	.L547-.L545
	.long	.L547-.L545
	.long	.L547-.L545
	.long	.L547-.L545
	.long	.L546-.L545
	.long	.L563-.L545
	.text
	.p2align 4,,10
	.p2align 3
.L547:
	movq	-64(%rbp), %rax
	xorl	%r14d, %r14d
	movl	$5, (%rax)
.L538:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L548:
	.cfi_restore_state
	movzbl	-49(%rbp), %edi
	testb	%cl, %cl
	movl	$192, %edx
	cmovne	%edx, %eax
	cmovne	%ecx, %edi
	movb	%dil, -49(%rbp)
	.p2align 4,,10
	.p2align 3
.L543:
	movl	%eax, (%rbx,%r15,4)
	addq	$1, %r15
	cmpq	$67, %r15
	jne	.L552
	testb	%r14b, -49(%rbp)
	je	.L553
	movq	-64(%rbp), %rax
	xorl	%r12d, %r12d
	movl	(%rax), %ecx
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L554:
	addq	$1, %r12
	cmpq	$67, %r12
	je	.L559
.L560:
	cmpl	$192, (%rbx,%r12,4)
	jne	.L554
	leal	4352(%r12), %esi
	cmpq	$18, %r12
	jbe	.L556
	leal	4430(%r12), %esi
	leal	4480(%r12), %eax
	cmpl	$39, %r12d
	cmovg	%eax, %esi
.L556:
	xorl	%edx, %edx
	testl	%ecx, %ecx
	jg	.L558
	movq	16(%r13), %rax
	movq	(%rax), %rdx
	movl	%esi, %eax
	sarl	$5, %eax
	movq	(%rdx), %rdi
	cltq
	movq	16(%rdx), %rdx
	movzwl	(%rdi,%rax,2), %edi
	movl	%esi, %eax
	andl	$31, %eax
	leal	(%rax,%rdi,4), %eax
	cltq
	movl	(%rdx,%rax,4), %edx
	cmpb	$-65, %dl
	jbe	.L558
	movq	-64(%rbp), %r14
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	%r14, %r8
	call	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0
	movl	(%r14), %ecx
	movl	%eax, %edx
.L558:
	movl	%edx, (%rbx,%r12,4)
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L546:
	shrl	$13, %eax
	testb	%cl, %cl
	jne	.L576
	xorl	%esi, %esi
	cmpl	80(%r13), %eax
	jge	.L550
	movq	96(%r13), %rdx
	cltq
	movq	(%rdx,%rax,8), %rsi
.L550:
	movl	%r12d, %edi
	call	_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil@PLT
	orb	$-63, %al
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L576:
	movq	16(%r13), %rdx
	movq	16(%rdx), %rdx
	movq	(%rdx,%rax,8), %rsi
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L563:
	movb	$1, -49(%rbp)
	movl	$192, %eax
	jmp	.L543
.L553:
	testb	%r14b, %r14b
	je	.L538
	movq	-64(%rbp), %rax
	movl	(%rax), %ecx
.L559:
	testl	%ecx, %ecx
	setle	%r14b
	jmp	.L538
	.cfi_endproc
.LFE4585:
	.size	_ZN6icu_6720CollationDataBuilder12getJamoCE32sEPjR10UErrorCode.part.0, .-_ZN6icu_6720CollationDataBuilder12getJamoCE32sEPjR10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6720CollationDataBuilder28copyContractionsFromBaseCE32ERNS_13UnicodeStringEijPNS_15ConditionalCE32ER10UErrorCode.part.0, @function
_ZN6icu_6720CollationDataBuilder28copyContractionsFromBaseCE32ERNS_13UnicodeStringEijPNS_15ConditionalCE32ER10UErrorCode.part.0:
.LFB4582:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	$-1, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movl	%edx, -228(%rbp)
	movq	16(%rdi), %rdx
	movq	%r8, -208(%rbp)
	movq	24(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%ecx, %eax
	shrl	$13, %eax
	andb	$1, %ch
	leaq	(%rdx,%rax,2), %r15
	je	.L615
.L578:
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L584
	sarl	$5, %eax
	movl	%eax, -200(%rbp)
.L585:
	leaq	-176(%rbp), %rax
	addq	$4, %r15
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	-184(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r15, -184(%rbp)
	movq	%rax, -224(%rbp)
	call	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode@PLT
	movzwl	-200(%rbp), %eax
	sall	$5, %eax
	movw	%ax, -230(%rbp)
	.p2align 4,,10
	.p2align 3
.L586:
	movq	-224(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	je	.L587
	movswl	-128(%rbp), %ecx
	testw	%cx, %cx
	js	.L588
	sarl	$5, %ecx
.L589:
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L590
	movl	-68(%rbp), %r15d
	cmpb	$-65, %r15b
	jbe	.L591
	movl	-228(%rbp), %esi
	movl	%r15d, %edx
	movq	%rbx, %r8
	movq	%r14, %rdi
	movl	$1, %ecx
	call	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0
	movl	(%rbx), %ecx
	movl	%eax, %r15d
	testl	%ecx, %ecx
	jg	.L590
.L591:
	movl	112(%r14), %r13d
	cmpl	$524287, %r13d
	jg	.L616
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L593
	leaq	8(%rax), %rdi
	movq	%r12, %rsi
	movq	%rax, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-216(%rbp), %r8
	leaq	104(%r14), %rdi
	movq	%rbx, %rdx
	movabsq	$-4294967295, %rax
	movq	%rdi, -216(%rbp)
	movq	%rax, 80(%r8)
	movq	%r8, %rsi
	movl	%r15d, 72(%r8)
	movl	$1, 76(%r8)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%rbx), %eax
	movq	-208(%rbp), %rdx
	movl	%r13d, 84(%rdx)
	testl	%eax, %eax
	jg	.L602
	movq	-216(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	-200(%rbp), %edx
	movq	%rax, -208(%rbp)
	movzwl	8(%r12), %eax
	testl	%edx, %edx
	jne	.L594
	testb	$1, %al
	jne	.L617
.L594:
	testw	%ax, %ax
	js	.L596
	movswl	%ax, %edx
	sarl	$5, %edx
.L597:
	movl	-200(%rbp), %ecx
	cmpl	%ecx, %edx
	jbe	.L586
	cmpl	$1023, %ecx
	jg	.L598
	andl	$31, %eax
	orw	-230(%rbp), %ax
	movw	%ax, 8(%r12)
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L584:
	movl	12(%r12), %eax
	movl	%eax, -200(%rbp)
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L588:
	movl	-124(%rbp), %ecx
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L596:
	movl	12(%r12), %edx
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L598:
	orl	$-32, %eax
	movw	%ax, 8(%r12)
	movl	-200(%rbp), %eax
	movl	%eax, 12(%r12)
	jmp	.L586
.L593:
	movq	-208(%rbp), %rax
	movl	$7, (%rbx)
	xorl	%r13d, %r13d
	movl	$-1, 84(%rax)
	.p2align 4,,10
	.p2align 3
.L587:
	movq	-224(%rbp), %rdi
	call	_ZN6icu_6710UCharsTrie8IteratorD1Ev@PLT
.L577:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L618
	addq	$200, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L617:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L615:
	movl	(%r9), %r8d
	testl	%r8d, %r8d
	jg	.L579
	movzwl	(%r15), %edx
	movzwl	2(%r15), %eax
	sall	$16, %edx
	orl	%eax, %edx
	cmpb	$-65, %al
	jbe	.L580
	movl	-228(%rbp), %esi
	movq	%r9, %r8
	movl	$1, %ecx
	call	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0
	movl	(%rbx), %edi
	movl	%eax, %edx
	testl	%edi, %edi
	jg	.L579
.L580:
	movl	112(%r14), %r13d
	cmpl	$524287, %r13d
	jg	.L619
	movl	$88, %edi
	movl	%edx, -216(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L583
	leaq	8(%rax), %rdi
	movq	%r12, %rsi
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-200(%rbp), %r8
	movl	-216(%rbp), %edx
	movabsq	$-4294967295, %rax
	leaq	104(%r14), %rdi
	movl	%edx, 72(%r8)
	movq	%r8, %rsi
	movq	%rbx, %rdx
	movq	%rax, 80(%r8)
	movl	$1, 76(%r8)
	movq	%rdi, -200(%rbp)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%rbx), %eax
	movq	-208(%rbp), %rdx
	movl	%r13d, 84(%rdx)
	testl	%eax, %eax
	jg	.L614
	movq	-200(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, -208(%rbp)
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L590:
	movq	-208(%rbp), %rax
	xorl	%r13d, %r13d
	movl	$-1, 84(%rax)
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L579:
	movq	-208(%rbp), %rax
	movl	$-1, 84(%rax)
.L614:
	xorl	%r13d, %r13d
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L616:
	movq	-208(%rbp), %rax
	movl	$15, (%rbx)
	xorl	%r13d, %r13d
	movl	$-1, 84(%rax)
	jmp	.L587
.L619:
	movq	-208(%rbp), %rax
	movl	$15, (%rbx)
	xorl	%r13d, %r13d
	movl	$-1, 84(%rax)
	jmp	.L577
.L618:
	call	__stack_chk_fail@PLT
.L583:
	movq	-208(%rbp), %rax
	movl	$7, (%rbx)
	xorl	%r13d, %r13d
	movl	$-1, 84(%rax)
	jmp	.L577
.L602:
	xorl	%r13d, %r13d
	jmp	.L587
	.cfi_endproc
.LFE4582:
	.size	_ZN6icu_6720CollationDataBuilder28copyContractionsFromBaseCE32ERNS_13UnicodeStringEijPNS_15ConditionalCE32ER10UErrorCode.part.0, .-_ZN6icu_6720CollationDataBuilder28copyContractionsFromBaseCE32ERNS_13UnicodeStringEijPNS_15ConditionalCE32ER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder3addERKNS_13UnicodeStringES3_PKliR10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder3addERKNS_13UnicodeStringES3_PKliR10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder3addERKNS_13UnicodeStringES3_PKliR10UErrorCode:
.LFB3467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	leaq	_ZN6icu_6720CollationDataBuilder9encodeCEsEPKliR10UErrorCode(%rip), %rdx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$168, %rsp
	movq	%rsi, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L621
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L620
	cmpl	$31, %r8d
	ja	.L654
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L640
	call	utrie2_isFrozen_67@PLT
	testb	%al, %al
	jne	.L640
	testl	%r13d, %r13d
	jne	.L626
	movl	(%rbx), %esi
	xorl	%ecx, %ecx
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L621:
	movq	%r9, %rcx
	movq	%r14, %rsi
	movl	%r8d, %edx
	call	*%rax
	movl	(%rbx), %esi
	movl	%eax, %ecx
.L627:
	testl	%esi, %esi
	jg	.L620
	movswl	8(%r15), %edx
	shrl	$5, %edx
	je	.L654
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L640
	movl	%ecx, -204(%rbp)
	call	utrie2_isFrozen_67@PLT
	testb	%al, %al
	jne	.L640
	movl	-204(%rbp), %ecx
	movq	%rbx, %r8
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-200(%rbp), %rsi
	call	_ZN6icu_6720CollationDataBuilder7addCE32ERKNS_13UnicodeStringES3_jR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L620:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L655
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L640:
	.cfi_restore_state
	movl	$27, (%rbx)
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L654:
	movl	$1, (%rbx)
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L626:
	movq	(%r14), %r8
	cmpl	$1, %r13d
	je	.L656
	movl	(%rbx), %esi
	cmpl	$2, %r13d
	je	.L657
.L629:
	movabsq	$281470698455295, %rcx
	leal	-1(%r13), %edi
	xorl	%edx, %edx
	movabsq	$1099511627775, %r9
	.p2align 4,,10
	.p2align 3
.L636:
	movq	(%r14,%rdx,8), %rax
	movq	%rax, %r11
	sarq	$32, %r11
	testq	%rcx, %rax
	je	.L658
	movq	%rax, %r8
	andq	%r9, %r8
	cmpq	$83887360, %r8
	je	.L659
	movzbl	%al, %r8d
	orl	%r11d, %r8d
	je	.L660
.L632:
	testl	%esi, %esi
	jg	.L620
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6720CollationDataBuilder15encodeExpansionEPKliR10UErrorCode.part.0
	movl	(%rbx), %esi
	movl	%eax, %ecx
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L658:
	movl	%eax, %r8d
	movzbl	%ah, %eax
	shrl	$16, %r8d
	orl	%r11d, %r8d
	orl	%r8d, %eax
	cmpl	$1, %eax
	je	.L632
.L633:
	leaq	-192(%rbp), %r8
	movl	%eax, (%r8,%rdx,4)
	leaq	1(%rdx), %rax
	cmpq	%rdi, %rdx
	je	.L635
	movq	%rax, %rdx
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L659:
	movl	%r11d, %eax
	orb	$-63, %al
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L660:
	orb	$-62, %al
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L657:
	movabsq	$72057594037862655, %rax
	andq	%r8, %rax
	cmpq	$83886080, %rax
	jne	.L629
	movq	8(%r14), %rdx
	movq	%r8, %rdi
	movabsq	$-4278190081, %rax
	sarq	$32, %rdi
	andq	%rdx, %rax
	cmpq	$1280, %rax
	jne	.L629
	testl	%edi, %edi
	je	.L629
	movl	%r8d, %ecx
	sarq	$16, %rdx
	sall	$8, %ecx
	orl	%edi, %edx
	andl	$16711680, %ecx
	orl	%edx, %ecx
	orb	$-60, %cl
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L635:
	testl	%esi, %esi
	jg	.L620
	movq	%rbx, %rcx
	movq	%r8, %rsi
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6720CollationDataBuilder17encodeExpansion32EPKiiR10UErrorCode.part.0
	movl	(%rbx), %esi
	movl	%eax, %ecx
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L656:
	movq	%r8, %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6720CollationDataBuilder11encodeOneCEElR10UErrorCode
	movl	(%rbx), %esi
	movl	%eax, %ecx
	jmp	.L627
.L655:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3467:
	.size	_ZN6icu_6720CollationDataBuilder3addERKNS_13UnicodeStringES3_PKliR10UErrorCode, .-_ZN6icu_6720CollationDataBuilder3addERKNS_13UnicodeStringES3_PKliR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder15encodeExpansionEPKliR10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder15encodeExpansionEPKliR10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder15encodeExpansionEPKliR10UErrorCode:
.LFB3472:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L662
	jmp	_ZN6icu_6720CollationDataBuilder15encodeExpansionEPKliR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L662:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3472:
	.size	_ZN6icu_6720CollationDataBuilder15encodeExpansionEPKliR10UErrorCode, .-_ZN6icu_6720CollationDataBuilder15encodeExpansionEPKliR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder17encodeExpansion32EPKiiR10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder17encodeExpansion32EPKiiR10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder17encodeExpansion32EPKiiR10UErrorCode:
.LFB3473:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L664
	jmp	_ZN6icu_6720CollationDataBuilder17encodeExpansion32EPKiiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L664:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3473:
	.size	_ZN6icu_6720CollationDataBuilder17encodeExpansion32EPKiiR10UErrorCode, .-_ZN6icu_6720CollationDataBuilder17encodeExpansion32EPKiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode:
.LFB3474:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L667
	cmpb	$-65, %dl
	jbe	.L668
	movsbl	%cl, %ecx
	jmp	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L667:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L668:
	movl	%edx, %eax
	ret
	.cfi_endproc
.LFE3474:
	.size	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode, .-_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder28copyContractionsFromBaseCE32ERNS_13UnicodeStringEijPNS_15ConditionalCE32ER10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder28copyContractionsFromBaseCE32ERNS_13UnicodeStringEijPNS_15ConditionalCE32ER10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder28copyContractionsFromBaseCE32ERNS_13UnicodeStringEijPNS_15ConditionalCE32ER10UErrorCode:
.LFB3475:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L670
	jmp	_ZN6icu_6720CollationDataBuilder28copyContractionsFromBaseCE32ERNS_13UnicodeStringEijPNS_15ConditionalCE32ER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L670:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3475:
	.size	_ZN6icu_6720CollationDataBuilder28copyContractionsFromBaseCE32ERNS_13UnicodeStringEijPNS_15ConditionalCE32ER10UErrorCode, .-_ZN6icu_6720CollationDataBuilder28copyContractionsFromBaseCE32ERNS_13UnicodeStringEijPNS_15ConditionalCE32ER10UErrorCode
	.section	.text._ZN6icu_6710CopyHelper8copyCE32Ej,"axG",@progbits,_ZN6icu_6710CopyHelper8copyCE32Ej,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6710CopyHelper8copyCE32Ej
	.type	_ZN6icu_6710CopyHelper8copyCE32Ej, @function
_ZN6icu_6710CopyHelper8copyCE32Ej:
.LFB3480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$-65, %sil
	ja	.L672
	movq	16(%rdi), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rsi
	movabsq	$4311744768, %rax
	cmpq	%rax, %rsi
	jne	.L673
.L707:
	movl	%r12d, %r15d
.L671:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L801
	addq	$152, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L673:
	.cfi_restore_state
	movq	8(%r14), %rdi
	leaq	272(%r14), %rdx
	call	_ZN6icu_6720CollationDataBuilder11encodeOneCEElR10UErrorCode
	movl	%eax, %r15d
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L672:
	movl	%esi, %eax
	andl	$15, %eax
	cmpl	$5, %eax
	je	.L802
	cmpl	$6, %eax
	je	.L803
	cmpl	$7, %eax
	jne	.L707
	movq	(%rdi), %rax
	shrl	$13, %esi
	leaq	104(%rax), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r14, %rdi
	movq	8(%r14), %r13
	movq	%rax, %rbx
	leaq	272(%r14), %rax
	movl	72(%rbx), %esi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6710CopyHelper8copyCE32Ej
	movl	272(%r14), %edx
	leaq	8(%rbx), %rsi
	movl	%eax, -136(%rbp)
	testl	%edx, %edx
	jg	.L725
	movl	112(%r13), %r12d
	cmpl	$524287, %r12d
	jg	.L804
	movl	$88, %edi
	movq	%rsi, -160(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L710
	movq	-160(%rbp), %rsi
	leaq	8(%rax), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	-136(%rbp), %eax
	leaq	104(%r13), %rdi
	movl	$1, 76(%r15)
	leaq	272(%r14), %rdx
	movq	%r15, %rsi
	movl	%eax, 72(%r15)
	movabsq	$-4294967295, %rax
	movq	%rax, 80(%r15)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L708:
	movl	%r12d, %eax
	movl	84(%rbx), %esi
	sall	$13, %eax
	orb	$-57, %al
	movl	%eax, %r15d
	testl	%esi, %esi
	js	.L671
	leaq	-128(%rbp), %rax
	movq	%rax, -136(%rbp)
.L719:
	movq	(%r14), %rax
	leaq	104(%rax), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%r12d, %esi
	movq	%rax, %rbx
	movq	8(%r14), %rax
	leaq	104(%rax), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	72(%rbx), %esi
	movq	%r14, %rdi
	movq	8(%r14), %r13
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6710CopyHelper8copyCE32Ej
	leaq	8(%rbx), %r11
	movl	%eax, -144(%rbp)
	movl	272(%r14), %eax
	testl	%eax, %eax
	jg	.L726
	movl	112(%r13), %r12d
	cmpl	$524287, %r12d
	jg	.L805
	movl	$88, %edi
	movq	%r11, -168(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-168(%rbp), %r11
	testq	%rax, %rax
	je	.L713
	movq	%r11, %rsi
	leaq	8(%rax), %rdi
	movq	%rax, -176(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-176(%rbp), %r10
	movl	-144(%rbp), %eax
	leaq	104(%r13), %rdi
	movq	-184(%rbp), %rdx
	movl	%eax, 72(%r10)
	movq	%r10, %rsi
	movabsq	$-4294967295, %rax
	movl	$1, 76(%r10)
	movq	%rax, 80(%r10)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	-168(%rbp), %r11
.L711:
	movzwl	16(%rbx), %eax
	testw	%ax, %ax
	js	.L714
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L715:
	movl	$65536, %edx
	testl	%ecx, %ecx
	je	.L716
	testb	$2, %al
	je	.L717
	leaq	18(%rbx), %rax
.L718:
	movzwl	(%rax), %edx
	addl	$1, %edx
.L716:
	movq	8(%r14), %rax
	movq	-136(%rbp), %rdi
	movq	%r11, %rsi
	movl	$2147483647, %ecx
	leaq	408(%rax), %r13
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	-136(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKNS_13UnicodeStringE@PLT
	movq	-136(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-160(%rbp), %rax
	movl	%r12d, 84(%rax)
	movl	84(%rbx), %esi
	testl	%esi, %esi
	jns	.L719
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L802:
	movq	(%rdi), %rdx
	movl	%esi, %eax
	shrl	$8, %r12d
	shrl	$13, %eax
	movq	64(%rdx), %rdx
	leaq	(%rdx,%rax,4), %r15
	leaq	272(%rdi), %rax
	movq	%rax, -144(%rbp)
	andl	$31, %r12d
	je	.L676
	movl	(%r15), %esi
	cmpb	$-65, %sil
	ja	.L677
	movq	16(%rdi), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movabsq	$4311744768, %rdx
	cmpq	%rdx, %rax
	je	.L677
	movq	%rax, 24(%r14)
	movl	$1, %ebx
.L678:
	cmpl	$1, %r12d
	je	.L688
	movl	$1, %r13d
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L806:
	movq	16(%r14), %rdi
	movl	%r10d, -160(%rbp)
	movl	%edx, %esi
	movl	%edx, -136(%rbp)
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	-136(%rbp), %edx
	movl	-160(%rbp), %r10d
	movabsq	$4311744768, %rcx
	cmpq	%rcx, %rax
	je	.L680
	testb	%bl, %bl
	je	.L722
.L685:
	movq	%rax, 24(%r14,%r13,8)
	movl	$1, %ebx
.L682:
	addq	$1, %r13
	cmpl	%r13d, %r12d
	jle	.L688
.L689:
	movl	(%r15,%r13,4), %edx
	movzbl	%dl, %r10d
	cmpb	$-65, %dl
	jbe	.L806
	testb	%bl, %bl
	jne	.L807
.L686:
	addq	$1, %r13
	xorl	%ebx, %ebx
	cmpl	%r13d, %r12d
	jg	.L689
.L688:
	movq	8(%r14), %rdi
	testb	%bl, %bl
	je	.L690
	movq	(%rdi), %rax
	movq	-144(%rbp), %rcx
	leaq	24(%r14), %rsi
	movl	%r12d, %edx
	call	*32(%rax)
	movl	%eax, %r15d
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L807:
	movl	%edx, %esi
	movl	%edx, %eax
	subl	%r10d, %esi
	andl	$15, %eax
	movq	%rsi, %rdx
	salq	$32, %rdx
	orq	$83887360, %rdx
	cmpl	$1, %eax
	cmove	%rdx, %rsi
.L687:
	movq	%rsi, 24(%r14,%r13,8)
	movl	$1, %ebx
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L677:
	xorl	%ebx, %ebx
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L680:
	testb	%bl, %bl
	je	.L686
	movl	%edx, %esi
	movq	%rdx, %rax
	sall	$8, %r10d
	movabsq	$-281474976710656, %rcx
	sall	$16, %esi
	salq	$32, %rax
	andl	$-16777216, %esi
	andq	%rcx, %rax
	orq	%rax, %rsi
	orq	%r10, %rsi
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L722:
	xorl	%edi, %edi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L683:
	movl	%edx, %esi
	andl	$15, %edx
	subl	%r9d, %esi
	movq	%rsi, %r9
	salq	$32, %r9
	orq	$83887360, %r9
	cmpl	$1, %edx
	cmove	%r9, %rsi
.L684:
	movq	%rsi, 24(%r14,%rdi,8)
	addq	$1, %rdi
	cmpl	%edi, %r13d
	jle	.L685
.L681:
	movl	(%r15,%rdi,4), %edx
	movzbl	%dl, %r9d
	cmpb	$-65, %dl
	ja	.L683
	movq	%rdx, %rsi
	sall	$16, %edx
	movabsq	$-281474976710656, %rcx
	salq	$32, %rsi
	andl	$-16777216, %edx
	sall	$8, %r9d
	andq	%rcx, %rsi
	orq	%rsi, %rdx
	movl	%r9d, %esi
	orq	%rdx, %rsi
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L803:
	movq	(%rdi), %rax
	movl	%esi, %ebx
	shrl	$8, %r12d
	shrl	$13, %ebx
	andl	$31, %r12d
	movq	96(%rax), %r13
	leaq	272(%rdi), %rax
	movl	%r12d, -136(%rbp)
	movq	%rax, -176(%rbp)
	leaq	0(%r13,%rbx,8), %r15
	je	.L693
	movq	16(%rdi), %rdi
	movq	(%r15), %rsi
	xorl	%r12d, %r12d
	movq	(%rdi), %rax
	call	*32(%rax)
	movabsq	$4311744768, %rcx
	cmpq	%rcx, %rax
	je	.L694
	movq	%rax, 24(%r14)
	movl	$1, %r12d
.L694:
	cmpl	$1, -136(%rbp)
	je	.L703
	leaq	16(%r13,%rbx,8), %rcx
	leaq	24(%r14), %rax
	leaq	0(%r13,%rbx,8), %rdi
	cmpq	%rax, %rcx
	leaq	40(%r14), %rax
	movl	$1, %r13d
	setbe	%cl
	cmpq	%rax, %rdi
	movq	%rdi, -168(%rbp)
	setnb	%al
	orl	%eax, %ecx
	movb	%cl, -144(%rbp)
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L695:
	testb	%r12b, %r12b
	je	.L697
.L700:
	movq	%rax, 24(%r14,%r13,8)
	movl	$1, %r12d
.L696:
	addq	$1, %r13
	cmpl	%r13d, -136(%rbp)
	jle	.L703
.L704:
	movq	16(%r14), %rdi
	movq	(%r15,%r13,8), %rbx
	movl	%r13d, -160(%rbp)
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	call	*32(%rax)
	movabsq	$4311744768, %rcx
	cmpq	%rcx, %rax
	jne	.L695
	testb	%r12b, %r12b
	je	.L696
	movq	%rbx, 24(%r14,%r13,8)
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L697:
	movl	%r13d, %edi
	movl	%r13d, %ebx
	cmpl	$4, %r13d
	jle	.L724
	cmpb	$0, -144(%rbp)
	je	.L724
	testl	%r13d, %r13d
	movl	$1, %edx
	movq	-168(%rbp), %rcx
	cmovle	%edx, %ebx
	movdqu	(%rcx), %xmm0
	movl	%ebx, %edx
	shrl	%edx
	movaps	%xmm0, -160(%rbp)
	movups	%xmm0, 24(%r14)
	cmpl	$1, %edx
	je	.L699
	movdqu	16(%rcx), %xmm2
	movaps	%xmm2, -160(%rbp)
	movups	%xmm2, 40(%r14)
	cmpl	$2, %edx
	je	.L699
	movdqu	32(%rcx), %xmm4
	movaps	%xmm4, -160(%rbp)
	movups	%xmm4, 56(%r14)
	cmpl	$3, %edx
	je	.L699
	movdqu	48(%rcx), %xmm6
	movaps	%xmm6, -160(%rbp)
	movups	%xmm6, 72(%r14)
	cmpl	$4, %edx
	je	.L699
	movdqu	64(%rcx), %xmm6
	movaps	%xmm6, -160(%rbp)
	movups	%xmm6, 88(%r14)
	cmpl	$5, %edx
	je	.L699
	movdqu	80(%rcx), %xmm4
	movaps	%xmm4, -160(%rbp)
	movups	%xmm4, 104(%r14)
	cmpl	$6, %edx
	je	.L699
	movdqu	96(%rcx), %xmm2
	movaps	%xmm2, -160(%rbp)
	movups	%xmm2, 120(%r14)
	cmpl	$7, %edx
	je	.L699
	movdqu	112(%rcx), %xmm0
	movaps	%xmm0, -160(%rbp)
	movups	%xmm0, 136(%r14)
	cmpl	$8, %edx
	je	.L699
	movdqu	128(%rcx), %xmm6
	movaps	%xmm6, -160(%rbp)
	movups	%xmm6, 152(%r14)
	cmpl	$9, %edx
	je	.L699
	movdqu	144(%rcx), %xmm4
	movaps	%xmm4, -160(%rbp)
	movups	%xmm4, 168(%r14)
	cmpl	$10, %edx
	je	.L699
	movdqu	160(%rcx), %xmm2
	movaps	%xmm2, -160(%rbp)
	movups	%xmm2, 184(%r14)
	cmpl	$11, %edx
	je	.L699
	movdqu	176(%rcx), %xmm0
	movaps	%xmm0, -160(%rbp)
	movups	%xmm0, 200(%r14)
	cmpl	$12, %edx
	je	.L699
	movdqu	192(%rcx), %xmm4
	movaps	%xmm4, -160(%rbp)
	movups	%xmm4, 216(%r14)
	cmpl	$13, %edx
	je	.L699
	movdqu	208(%rcx), %xmm6
	movaps	%xmm6, -160(%rbp)
	movups	%xmm6, 232(%r14)
	cmpl	$14, %edx
	je	.L699
	movdqu	224(%rcx), %xmm2
	movaps	%xmm2, -160(%rbp)
	movups	%xmm2, 248(%r14)
	.p2align 4,,10
	.p2align 3
.L699:
	movl	%ebx, %edx
	andl	$-2, %edx
	andl	$1, %ebx
	je	.L700
	movl	%edx, %esi
	movslq	%edx, %rdx
	movq	(%r15,%rsi,8), %rsi
	movq	%rsi, 24(%r14,%rdx,8)
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L703:
	movq	8(%r14), %rdi
	testb	%r12b, %r12b
	je	.L705
	movq	(%rdi), %rax
	movq	-176(%rbp), %rcx
	leaq	24(%r14), %rsi
	movl	-136(%rbp), %edx
	call	*32(%rax)
	movl	%eax, %r15d
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L676:
	movq	8(%rdi), %rdi
.L690:
	movl	272(%r14), %esi
	testl	%esi, %esi
	jg	.L706
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movl	%r12d, %edx
	call	_ZN6icu_6720CollationDataBuilder17encodeExpansion32EPKiiR10UErrorCode.part.0
	movl	%eax, %r15d
	jmp	.L671
.L693:
	movq	8(%rdi), %rdi
.L705:
	movl	272(%r14), %ecx
	testl	%ecx, %ecx
	jle	.L808
.L706:
	xorl	%r15d, %r15d
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L808:
	movq	-176(%rbp), %rcx
	movl	-136(%rbp), %edx
	movq	%r15, %rsi
	call	_ZN6icu_6720CollationDataBuilder15encodeExpansionEPKliR10UErrorCode.part.0
	movl	%eax, %r15d
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L714:
	movl	20(%rbx), %ecx
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L717:
	movq	32(%rbx), %rax
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L724:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L698:
	movq	(%r15,%rdx,8), %rsi
	movq	%rsi, 24(%r14,%rdx,8)
	addq	$1, %rdx
	cmpl	%edx, %edi
	jg	.L698
	jmp	.L700
.L805:
	movl	$15, 272(%r14)
	movl	$-1, %r12d
	jmp	.L711
.L726:
	movl	$-1, %r12d
	jmp	.L711
.L804:
	movl	$15, 272(%r14)
	orl	$-1, %r12d
	jmp	.L708
.L725:
	orl	$-1, %r12d
	jmp	.L708
.L801:
	call	__stack_chk_fail@PLT
.L710:
	movl	$7, 272(%r14)
	orl	$-1, %r12d
	jmp	.L708
.L713:
	movl	$7, 272(%r14)
	orl	$-1, %r12d
	jmp	.L711
	.cfi_endproc
.LFE3480:
	.size	_ZN6icu_6710CopyHelper8copyCE32Ej, .-_ZN6icu_6710CopyHelper8copyCE32Ej
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder8copyFromERKS0_RKNS0_10CEModifierER10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder8copyFromERKS0_RKNS0_10CEModifierER10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder8copyFromERKS0_RKNS0_10CEModifierER10UErrorCode:
.LFB3482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$288, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L809
	movq	%rdi, %r13
	movq	32(%rdi), %rdi
	movq	%rcx, %rbx
	testq	%rdi, %rdi
	je	.L812
	movq	%rsi, %r14
	movq	%rdx, %r12
	call	utrie2_isFrozen_67@PLT
	testb	%al, %al
	je	.L816
.L812:
	movl	$27, (%rbx)
.L809:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L817
	addq	$288, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L816:
	.cfi_restore_state
	movl	(%rbx), %eax
	movq	%r13, %xmm1
	movq	32(%r14), %rdi
	movq	%r14, %xmm0
	punpcklqdq	%xmm1, %xmm0
	leaq	-320(%rbp), %rcx
	leaq	enumRangeForCopy(%rip), %rdx
	xorl	%esi, %esi
	movl	%eax, -48(%rbp)
	movq	%r12, -304(%rbp)
	movaps	%xmm0, -320(%rbp)
	call	utrie2_enum_67@PLT
	movl	-48(%rbp), %eax
	movl	%eax, (%rbx)
	movzbl	608(%r14), %eax
	orb	%al, 608(%r13)
	jmp	.L809
.L817:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3482:
	.size	_ZN6icu_6720CollationDataBuilder8copyFromERKS0_RKNS0_10CEModifierER10UErrorCode, .-_ZN6icu_6720CollationDataBuilder8copyFromERKS0_RKNS0_10CEModifierER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder12getJamoCE32sEPjR10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder12getJamoCE32sEPjR10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder12getJamoCE32sEPjR10UErrorCode:
.LFB3485:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L819
	jmp	_ZN6icu_6720CollationDataBuilder12getJamoCE32sEPjR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L819:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3485:
	.size	_ZN6icu_6720CollationDataBuilder12getJamoCE32sEPjR10UErrorCode, .-_ZN6icu_6720CollationDataBuilder12getJamoCE32sEPjR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder17setLeadSurrogatesER10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder17setLeadSurrogatesER10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder17setLeadSurrogatesER10UErrorCode:
.LFB3488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-60(%rbp), %r15
	leaq	enumRangeLeadValue(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	$55296, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L821:
	movq	32(%r12), %rdi
	movl	%ebx, %esi
	movq	%r15, %r8
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$-1, -60(%rbp)
	call	utrie2_enumForLeadSurrogate_67@PLT
	movl	-60(%rbp), %edx
	movq	32(%r12), %rdi
	movl	%ebx, %esi
	movq	%r13, %rcx
	addl	$1, %ebx
	orb	$-51, %dl
	call	utrie2_set32ForLeadSurrogateCodeUnit_67@PLT
	cmpl	$56320, %ebx
	jne	.L821
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L825
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L825:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3488:
	.size	_ZN6icu_6720CollationDataBuilder17setLeadSurrogatesER10UErrorCode, .-_ZN6icu_6720CollationDataBuilder17setLeadSurrogatesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder14addContextTrieEjRNS_17UCharsTrieBuilderER10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder14addContextTrieEjRNS_17UCharsTrieBuilderER10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder14addContextTrieEjRNS_17UCharsTrieBuilderER10UErrorCode:
.LFB3494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	$2, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movl	$1, %ecx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%esi, %eax
	movw	%dx, -184(%rbp)
	leaq	-194(%rbp), %rsi
	shrl	$16, %eax
	xorl	%edx, %edx
	movq	%r8, -192(%rbp)
	movq	%rsi, -216(%rbp)
	movw	%ax, -194(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-216(%rbp), %rsi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%rax, %rdi
	movw	%r14w, -194(%rbp)
	leaq	-128(%rbp), %r14
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$2, %ecx
	movq	%r14, %rdx
	movq	%r15, %rdi
	movw	%cx, -120(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r8
	movq	%r12, %rcx
	movl	$1, %esi
	movq	%r8, -128(%rbp)
	call	_ZN6icu_6717UCharsTrieBuilder18buildUnicodeStringE22UStringTrieBuildOptionRNS_13UnicodeStringER10UErrorCode@PLT
	movswl	8(%rax), %ecx
	movq	%rax, %rsi
	testw	%cx, %cx
	js	.L827
	sarl	$5, %ecx
.L828:
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L841
	movzwl	352(%rbx), %eax
	leaq	344(%rbx), %r15
	testw	%ax, %ax
	js	.L830
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L831:
	movzwl	-184(%rbp), %edx
	movl	%edx, %esi
	andl	$1, %esi
	testw	%dx, %dx
	js	.L832
	movswl	%dx, %ecx
	sarl	$5, %ecx
	testb	%sil, %sil
	jne	.L834
.L833:
	testl	%ecx, %ecx
	je	.L834
	andl	$2, %edx
	leaq	-182(%rbp), %rsi
	movq	%r15, %rdi
	cmove	-168(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L851
.L829:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L852
	addq	$184, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L827:
	.cfi_restore_state
	movl	12(%rax), %ecx
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L851:
	movzwl	-184(%rbp), %edx
	movzwl	352(%rbx), %eax
.L834:
	testw	%ax, %ax
	js	.L836
	.p2align 4,,10
	.p2align 3
.L853:
	movswl	%ax, %r12d
	sarl	$5, %r12d
	testw	%dx, %dx
	js	.L838
.L854:
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L839:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L832:
	movl	-180(%rbp), %ecx
	testb	%sil, %sil
	jne	.L834
	testl	%ecx, %ecx
	jns	.L833
	testw	%ax, %ax
	jns	.L853
.L836:
	movl	356(%rbx), %r12d
	testw	%dx, %dx
	jns	.L854
.L838:
	movl	-180(%rbp), %ecx
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L830:
	movl	356(%rbx), %r9d
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L841:
	movl	$-1, %r12d
	jmp	.L829
.L852:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3494:
	.size	_ZN6icu_6720CollationDataBuilder14addContextTrieEjRNS_17UCharsTrieBuilderER10UErrorCode, .-_ZN6icu_6720CollationDataBuilder14addContextTrieEjRNS_17UCharsTrieBuilderER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder19buildFastLatinTableERNS_13CollationDataER10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder19buildFastLatinTableERNS_13CollationDataER10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder19buildFastLatinTableERNS_13CollationDataER10UErrorCode:
.LFB3495:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L877
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 609(%rdi)
	je	.L855
	movq	616(%rdi), %rdi
	movq	%rsi, %r13
	movq	%rdx, %r12
	testq	%rdi, %rdi
	je	.L857
	movq	(%rdi), %rax
	call	*8(%rax)
.L857:
	movl	$7376, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L858
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6725CollationFastLatinBuilderC1ER10UErrorCode@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r14, 616(%rbx)
	call	_ZN6icu_6725CollationFastLatinBuilder7forDataERKNS_13CollationDataER10UErrorCode@PLT
	testb	%al, %al
	je	.L859
	movq	616(%rbx), %r14
	movzwl	7312(%r14), %eax
	testb	$17, %al
	jne	.L866
	leaq	7314(%r14), %r15
	testb	$2, %al
	jne	.L860
	movq	7328(%r14), %r15
.L860:
	testw	%ax, %ax
	js	.L862
	movswl	%ax, %r12d
	sarl	$5, %r12d
.L863:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L864
	cmpl	%r12d, 96(%rax)
	je	.L880
.L864:
	movq	%r15, 88(%r13)
	movl	%r12d, 96(%r13)
.L855:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L877:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L859:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	616(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L865
	movq	(%rdi), %rax
	call	*8(%rax)
.L865:
	movq	$0, 616(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L862:
	.cfi_restore_state
	movl	7316(%r14), %r12d
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L880:
	leal	(%r12,%r12), %edx
	movq	88(%rax), %rsi
	movq	%r15, %rdi
	movslq	%edx, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L864
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	movq	16(%rbx), %rax
	movq	$0, 616(%rbx)
	movq	88(%rax), %r15
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L866:
	xorl	%r15d, %r15d
	jmp	.L860
.L858:
	movq	$0, 616(%rbx)
	movl	$7, (%r12)
	jmp	.L855
	.cfi_endproc
.LFE3495:
	.size	_ZN6icu_6720CollationDataBuilder19buildFastLatinTableERNS_13CollationDataER10UErrorCode, .-_ZN6icu_6720CollationDataBuilder19buildFastLatinTableERNS_13CollationDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringEPli
	.type	_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringEPli, @function
_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringEPli:
.LFB3496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	624(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L886
.L882:
	popq	%rbx
	movl	%r14d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rsi
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6728DataBuilderCollationIterator8fetchCEsERKNS_13UnicodeStringEiPli
	.p2align 4,,10
	.p2align 3
.L886:
	.cfi_restore_state
	movl	$832, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L883
	movq	%rbx, %rsi
	call	_ZN6icu_6728DataBuilderCollationIteratorC1ERNS_20CollationDataBuilderE
	movq	%rdi, 624(%rbx)
	jmp	.L882
.L883:
	movq	$0, 624(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3496:
	.size	_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringEPli, .-_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringEPli
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringES3_Pli
	.type	_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringES3_Pli, @function
_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringES3_Pli:
.LFB3497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movswl	8(%rsi), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testw	%r12w, %r12w
	js	.L888
	sarl	$5, %r12d
.L889:
	testl	%r12d, %r12d
	jne	.L890
	movq	624(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L899
.L891:
	movl	%r15d, %r8d
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6728DataBuilderCollationIterator8fetchCEsERKNS_13UnicodeStringEiPli
	movl	%eax, %r12d
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L890:
	leaq	-128(%rbp), %r9
	movq	%r13, %rdx
	movq	%r9, %rdi
	movq	%r9, -136(%rbp)
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movq	624(%rbx), %rdi
	movq	-136(%rbp), %r9
	testq	%rdi, %rdi
	je	.L900
.L894:
	movl	%r12d, %edx
	movq	%r9, %rsi
	movl	%r15d, %r8d
	movq	%r14, %rcx
	movq	%r9, -136(%rbp)
	call	_ZN6icu_6728DataBuilderCollationIterator8fetchCEsERKNS_13UnicodeStringEiPli
	movq	-136(%rbp), %r9
	movl	%eax, %r12d
.L896:
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L887:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L901
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L888:
	.cfi_restore_state
	movl	12(%rsi), %r12d
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L900:
	movl	$832, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-136(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L895
	movq	%rbx, %rsi
	call	_ZN6icu_6728DataBuilderCollationIteratorC1ERNS_20CollationDataBuilderE
	movq	-136(%rbp), %r9
	movq	%rdi, 624(%rbx)
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L899:
	movl	$832, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L892
	movq	%rbx, %rsi
	call	_ZN6icu_6728DataBuilderCollationIteratorC1ERNS_20CollationDataBuilderE
	movq	%rdi, 624(%rbx)
	jmp	.L891
.L901:
	call	__stack_chk_fail@PLT
.L892:
	movq	$0, 624(%rbx)
	jmp	.L887
.L895:
	movq	$0, 624(%rbx)
	xorl	%r12d, %r12d
	jmp	.L896
	.cfi_endproc
.LFE3497:
	.size	_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringES3_Pli, .-_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringES3_Pli
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringEiPli
	.type	_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringEiPli, @function
_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringEiPli:
.LFB3498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	624(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L907
.L903:
	addq	$8, %rsp
	movl	%r15d, %r8d
	movq	%r14, %rcx
	movl	%r13d, %edx
	popq	%rbx
	movq	%r12, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6728DataBuilderCollationIterator8fetchCEsERKNS_13UnicodeStringEiPli
	.p2align 4,,10
	.p2align 3
.L907:
	.cfi_restore_state
	movl	$832, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L904
	movq	%rbx, %rsi
	call	_ZN6icu_6728DataBuilderCollationIteratorC1ERNS_20CollationDataBuilderE
	movq	%rdi, 624(%rbx)
	jmp	.L903
.L904:
	movq	$0, 624(%rbx)
	xorl	%eax, %eax
	popq	%rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3498:
	.size	_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringEiPli, .-_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringEiPli
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilderD2Ev
	.type	_ZN6icu_6720CollationDataBuilderD2Ev, @function
_ZN6icu_6720CollationDataBuilderD2Ev:
.LFB3453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6720CollationDataBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	call	utrie2_close_67@PLT
	movq	616(%r12), %rdi
	testq	%rdi, %rdi
	je	.L909
	movq	(%rdi), %rax
	call	*8(%rax)
.L909:
	movq	624(%r12), %r13
	testq	%r13, %r13
	je	.L910
	movq	0(%r13), %rax
	leaq	_ZN6icu_6728DataBuilderCollationIteratorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L911
	leaq	16+_ZTVN6icu_6728DataBuilderCollationIteratorE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_6717CollationIteratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L910:
	leaq	408(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	344(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	144(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	104(%r12), %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	leaq	72(%r12), %rdi
	call	_ZN6icu_679UVector64D1Ev@PLT
	leaq	40(%r12), %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L911:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L910
	.cfi_endproc
.LFE3453:
	.size	_ZN6icu_6720CollationDataBuilderD2Ev, .-_ZN6icu_6720CollationDataBuilderD2Ev
	.globl	_ZN6icu_6720CollationDataBuilderD1Ev
	.set	_ZN6icu_6720CollationDataBuilderD1Ev,_ZN6icu_6720CollationDataBuilderD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilderD0Ev
	.type	_ZN6icu_6720CollationDataBuilderD0Ev, @function
_ZN6icu_6720CollationDataBuilderD0Ev:
.LFB3455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6720CollationDataBuilderD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3455:
	.size	_ZN6icu_6720CollationDataBuilderD0Ev, .-_ZN6icu_6720CollationDataBuilderD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder16initForTailoringEPKNS_13CollationDataER10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder16initForTailoringEPKNS_13CollationDataER10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder16initForTailoringEPKNS_13CollationDataER10UErrorCode:
.LFB3456:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L928
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	$0, 32(%rdi)
	je	.L923
	movl	$27, (%rdx)
.L921:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L923:
	.cfi_restore_state
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L931
	movq	%rsi, 16(%rdi)
	movl	$192, %edi
	movl	$-195323, %esi
	movl	$192, %ebx
	call	utrie2_open_67@PLT
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L926:
	movl	%ebx, %esi
	movq	%r14, %rcx
	movl	$192, %edx
	addl	$1, %ebx
	call	utrie2_set32_67@PLT
	movq	32(%r12), %rdi
	cmpl	$256, %ebx
	jne	.L926
	movq	%r14, %r9
	movl	$44032, %esi
	movl	$1, %r8d
	movl	$204, %ecx
	movl	$55203, %edx
	call	utrie2_setRange32_67@PLT
	movq	80(%r13), %rsi
	popq	%rbx
	.cfi_restore 3
	leaq	408(%r12), %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L928:
	ret
.L931:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$1, (%rdx)
	jmp	.L921
	.cfi_endproc
.LFE3456:
	.size	_ZN6icu_6720CollationDataBuilder16initForTailoringEPKNS_13CollationDataER10UErrorCode, .-_ZN6icu_6720CollationDataBuilder16initForTailoringEPKNS_13CollationDataER10UErrorCode
	.p2align 4
	.type	enumRangeForCopy, @function
enumRangeForCopy:
.LFB3481:
	.cfi_startproc
	endbr64
	cmpl	$-1, %ecx
	je	.L935
	cmpl	$192, %ecx
	jne	.L940
.L935:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L940:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	movl	%ecx, %esi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN6icu_6710CopyHelper8copyCE32Ej
	leaq	272(%rbx), %r9
	movl	%r14d, %edx
	movl	%r12d, %esi
	movl	%eax, %r13d
	movq	8(%rbx), %rax
	movl	$1, %r8d
	movl	%r13d, %ecx
	movq	32(%rax), %rdi
	call	utrie2_setRange32_67@PLT
	cmpb	$-65, %r13b
	jbe	.L934
	andl	$15, %r13d
	cmpl	$7, %r13d
	jne	.L934
	movq	8(%rbx), %rax
	movl	%r14d, %edx
	movl	%r12d, %esi
	leaq	144(%rax), %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
.L934:
	movl	272(%rbx), %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	testl	%eax, %eax
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	setle	%al
	ret
	.cfi_endproc
.LFE3481:
	.size	enumRangeForCopy, .-enumRangeForCopy
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder8optimizeERKNS_10UnicodeSetER10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder8optimizeERKNS_10UnicodeSetER10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder8optimizeERKNS_10UnicodeSetER10UErrorCode:
.LFB3483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L966
.L941:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L967
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L966:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r12
	movq	%rdx, %r13
	call	_ZNK6icu_6710UnicodeSet7isEmptyEv@PLT
	testb	%al, %al
	jne	.L941
	leaq	-112(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L969:
	movl	-104(%rbp), %r12d
	cmpl	$-1, %r12d
	je	.L945
	movq	32(%rbx), %rdi
	movl	%r12d, %esi
	call	utrie2_get32_67@PLT
	cmpl	$192, %eax
	je	.L968
.L947:
	movq	%r14, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	jne	.L969
.L945:
	movb	$1, 608(%rbx)
	movq	%r14, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L968:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rcx
	movq	16(%rcx), %rdx
	cmpl	$55295, %r12d
	jbe	.L970
	cmpl	$65535, %r12d
	ja	.L950
	cmpl	$56320, %r12d
	movl	$320, %eax
	movq	(%rcx), %rsi
	movl	$0, %ecx
	cmovl	%eax, %ecx
	movl	%r12d, %eax
	sarl	$5, %eax
.L965:
	addl	%ecx, %eax
	cltq
	movzwl	(%rsi,%rax,2), %ecx
.L964:
	movl	%r12d, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
.L949:
	movl	(%rdx,%rax), %esi
	call	_ZNK6icu_6713CollationData12getFinalCE32Ej@PLT
	movl	%eax, %edx
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L958
	cmpb	$-65, %dl
	jbe	.L953
	movq	%r13, %r8
	movl	$1, %ecx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0
	movl	%eax, %edx
.L953:
	movq	32(%rbx), %rdi
	movq	%r13, %rcx
	movl	%r12d, %esi
	call	utrie2_set32_67@PLT
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L970:
	movl	%r12d, %eax
	movq	(%rcx), %rcx
	sarl	$5, %eax
	cltq
	movzwl	(%rcx,%rax,2), %ecx
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L958:
	xorl	%edx, %edx
	jmp	.L953
.L950:
	movl	$512, %eax
	cmpl	$1114111, %r12d
	ja	.L949
	cmpl	44(%rcx), %r12d
	jl	.L952
	movslq	48(%rcx), %rax
	salq	$2, %rax
	jmp	.L949
.L952:
	movl	%r12d, %eax
	movq	(%rcx), %rsi
	movl	%r12d, %ecx
	sarl	$11, %eax
	sarl	$5, %ecx
	addl	$2080, %eax
	andl	$63, %ecx
	cltq
	movzwl	(%rsi,%rax,2), %eax
	jmp	.L965
.L967:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3483:
	.size	_ZN6icu_6720CollationDataBuilder8optimizeERKNS_10UnicodeSetER10UErrorCode, .-_ZN6icu_6720CollationDataBuilder8optimizeERKNS_10UnicodeSetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder20suppressContractionsERKNS_10UnicodeSetER10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder20suppressContractionsERKNS_10UnicodeSetER10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder20suppressContractionsERKNS_10UnicodeSetER10UErrorCode:
.LFB3484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L997
.L971:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L998
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L997:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r14
	movq	%rdx, %r12
	call	_ZNK6icu_6710UnicodeSet7isEmptyEv@PLT
	testb	%al, %al
	jne	.L971
	leaq	-128(%rbp), %r13
	movq	%r14, %rsi
	leaq	104(%rbx), %r14
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	.p2align 4,,10
	.p2align 3
.L975:
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	je	.L976
.L1000:
	movl	-120(%rbp), %r15d
	cmpl	$-1, %r15d
	je	.L976
	movq	32(%rbx), %rdi
	movl	%r15d, %esi
	call	utrie2_get32_67@PLT
	cmpl	$192, %eax
	je	.L999
	cmpb	$-65, %al
	jbe	.L975
	movl	%eax, %edx
	andl	$15, %edx
	cmpl	$7, %edx
	jne	.L975
	shrl	$13, %eax
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %rcx
	movl	%r15d, %esi
	movl	72(%rax), %edx
	call	utrie2_set32_67@PLT
	leaq	144(%rbx), %rdi
	movl	%r15d, %esi
	call	_ZN6icu_6710UnicodeSet6removeEi@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	jne	.L1000
	.p2align 4,,10
	.p2align 3
.L976:
	movb	$1, 608(%rbx)
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L999:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rcx
	movq	16(%rcx), %rdx
	cmpl	$55295, %r15d
	ja	.L979
	movl	%r15d, %eax
	movq	(%rcx), %rcx
	sarl	$5, %eax
	cltq
	movzwl	(%rcx,%rax,2), %ecx
.L995:
	movl	%r15d, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
.L980:
	movl	(%rdx,%rax), %esi
	call	_ZNK6icu_6713CollationData12getFinalCE32Ej@PLT
	movl	%eax, %edx
	cmpb	$-65, %al
	jbe	.L975
	andl	$15, %eax
	subl	$8, %eax
	cmpl	$1, %eax
	ja	.L975
	movl	(%r12), %eax
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jg	.L985
	movq	%r12, %r8
	xorl	%ecx, %ecx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6720CollationDataBuilder16copyFromBaseCE32EijaR10UErrorCode.part.0
	movl	%eax, %r8d
.L985:
	movq	32(%rbx), %rdi
	movq	%r12, %rcx
	movl	%r8d, %edx
	movl	%r15d, %esi
	call	utrie2_set32_67@PLT
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L979:
	cmpl	$65535, %r15d
	ja	.L981
	cmpl	$56320, %r15d
	movl	$320, %eax
	movq	(%rcx), %rsi
	movl	$0, %ecx
	cmovl	%eax, %ecx
	movl	%r15d, %eax
	sarl	$5, %eax
.L996:
	addl	%ecx, %eax
	cltq
	movzwl	(%rsi,%rax,2), %ecx
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L981:
	movl	$512, %eax
	cmpl	$1114111, %r15d
	ja	.L980
	cmpl	44(%rcx), %r15d
	jl	.L983
	movslq	48(%rcx), %rax
	salq	$2, %rax
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L983:
	movl	%r15d, %eax
	movq	(%rcx), %rsi
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rsi,%rax,2), %ecx
	movl	%r15d, %eax
	sarl	$5, %eax
	andl	$63, %eax
	jmp	.L996
.L998:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3484:
	.size	_ZN6icu_6720CollationDataBuilder20suppressContractionsERKNS_10UnicodeSetER10UErrorCode, .-_ZN6icu_6720CollationDataBuilder20suppressContractionsERKNS_10UnicodeSetER10UErrorCode
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC1:
	.string	"["
	.string	":"
	.string	"N"
	.string	"d"
	.string	":"
	.string	"]"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder12setDigitTagsER10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder12setDigitTagsER10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder12setDigitTagsER10UErrorCode:
.LFB3486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r15
	leaq	-256(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-384(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$360, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rax
	movq	%rax, -384(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L1028
.L1002:
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1029
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1028:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	leaq	40(%rbx), %rax
	movq	%rax, -400(%rbp)
	.p2align 4,,10
	.p2align 3
.L1023:
	movq	%r12, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	je	.L1012
.L1032:
	movl	-376(%rbp), %r15d
	movq	32(%rbx), %rdi
	movl	%r15d, %esi
	call	utrie2_get32_67@PLT
	movl	%eax, %ecx
	cmpl	$192, %eax
	je	.L1023
	cmpl	$-1, %eax
	je	.L1023
	movl	48(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L1005
	leal	-1(%r9), %r10d
	xorl	%eax, %eax
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1030:
	leaq	1(%rax), %rdx
	cmpq	%rax, %r10
	je	.L1008
	movq	%rdx, %rax
.L1009:
	movl	%r9d, %edi
	movl	%eax, %esi
	xorl	%edx, %edx
	subl	%eax, %edi
	testl	%edi, %edi
	jle	.L1006
	movq	64(%rbx), %rdx
	movl	(%rdx,%rax,4), %edx
.L1006:
	cmpl	%edx, %ecx
	jne	.L1030
.L1007:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1012
	cmpl	$524287, %esi
	jg	.L1031
	movl	%r15d, %edi
	movl	%esi, -388(%rbp)
	call	u_charDigitValue_67@PLT
	movl	-388(%rbp), %esi
	movq	32(%rbx), %rdi
	movq	%r13, %rcx
	sall	$8, %eax
	sall	$13, %esi
	orl	%eax, %esi
	orb	$-54, %sil
	movl	%esi, %edx
	movl	%r15d, %esi
	call	utrie2_set32_67@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	jne	.L1032
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	%r12, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1008:
	leal	1(%r9), %esi
.L1016:
	cmpl	52(%rbx), %esi
	jle	.L1019
.L1010:
	movq	-400(%rbp), %rdi
	movq	%r13, %rdx
	movl	%r9d, -388(%rbp)
	movl	%ecx, -392(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movl	-388(%rbp), %r9d
	movl	%r9d, %esi
	testb	%al, %al
	je	.L1007
	movslq	48(%rbx), %rax
	movl	-392(%rbp), %ecx
.L1011:
	movq	64(%rbx), %rdx
	movl	%r9d, %esi
	movl	%ecx, (%rdx,%rax,4)
	addl	$1, 48(%rbx)
	jmp	.L1007
.L1019:
	movslq	%r9d, %rax
	jmp	.L1011
.L1005:
	movl	%r9d, %esi
	addl	$1, %esi
	js	.L1010
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1031:
	movl	$15, 0(%r13)
	jmp	.L1012
.L1029:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3486:
	.size	_ZN6icu_6720CollationDataBuilder12setDigitTagsER10UErrorCode, .-_ZN6icu_6720CollationDataBuilder12setDigitTagsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder13clearContextsEv
	.type	_ZN6icu_6720CollationDataBuilder13clearContextsEv, @function
_ZN6icu_6720CollationDataBuilder13clearContextsEv:
.LFB3491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	144(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	104(%rbx), %r13
	subq	$88, %rsp
	movzwl	352(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 352(%rdi)
	movq	%r12, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1040:
	movq	32(%rbx), %rdi
	movl	-104(%rbp), %esi
	call	utrie2_get32_67@PLT
	movq	%r13, %rdi
	shrl	$13, %eax
	movl	%eax, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	$1, 80(%rax)
.L1036:
	movq	%r12, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	jne	.L1040
	movq	%r12, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1041
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1041:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3491:
	.size	_ZN6icu_6720CollationDataBuilder13clearContextsEv, .-_ZN6icu_6720CollationDataBuilder13clearContextsEv
	.align 2
	.p2align 4
	.type	_ZN6icu_6720CollationDataBuilder12buildContextEPNS_15ConditionalCE32ER10UErrorCode.part.0, @function
_ZN6icu_6720CollationDataBuilder12buildContextEPNS_15ConditionalCE32ER10UErrorCode.part.0:
.LFB4588:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	addq	$104, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$472, %rsp
	movq	%rdi, -488(%rbp)
	movq	%rsi, -496(%rbp)
	movq	%rdx, %rsi
	movq	%rdx, -448(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-288(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -504(%rbp)
	call	_ZN6icu_6717UCharsTrieBuilderC1ER10UErrorCode@PLT
	leaq	-176(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN6icu_6717UCharsTrieBuilderC1ER10UErrorCode@PLT
	leaq	-416(%rbp), %rax
	movq	%r14, -432(%rbp)
	movq	%rax, -472(%rbp)
	movq	%r14, %rax
	movzwl	16(%rax), %eax
	testw	%ax, %ax
	js	.L1043
	.p2align 4,,10
	.p2align 3
.L1145:
	movswl	%ax, %edx
	sarl	$5, %edx
	testl	%edx, %edx
	je	.L1106
.L1146:
	testb	$2, %al
	movq	-432(%rbp), %rax
	je	.L1046
	addq	$18, %rax
.L1047:
	movzwl	(%rax), %eax
	movl	%eax, -424(%rbp)
	addl	$1, %eax
	movl	%eax, -420(%rbp)
	movl	%eax, %ecx
.L1045:
	movq	-432(%rbp), %rbx
	movq	-472(%rbp), %rdi
	xorl	%edx, %edx
	leaq	8(%rbx), %r14
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1144:
	testb	$1, %al
	jne	.L1052
	movswl	%ax, %edx
	xorl	%r8d, %r8d
	sarl	$5, %edx
.L1104:
	movl	%edx, %r9d
	subl	%r8d, %r9d
	cmpl	%edx, %r9d
	cmovg	%edx, %r9d
.L1056:
	leaq	-406(%rbp), %rcx
	leaq	8(%r12), %rdi
	testb	$2, %al
	cmove	-392(%rbp), %rcx
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jne	.L1059
.L1109:
	movq	%r12, %rbx
.L1058:
	movl	84(%rbx), %esi
	testl	%esi, %esi
	js	.L1059
	movq	%r15, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r12
	movzwl	-408(%rbp), %eax
	testw	%ax, %ax
	jns	.L1144
	movl	-404(%rbp), %edx
	testb	$1, %al
	jne	.L1052
	testl	%edx, %edx
	movl	$0, %r8d
	cmovle	%edx, %r8d
	jns	.L1104
	xorl	%r9d, %r9d
	jmp	.L1056
	.p2align 4,,10
	.p2align 3
.L1052:
	movzbl	16(%r12), %eax
	notl	%eax
	andl	$1, %eax
	testb	%al, %al
	je	.L1109
.L1059:
	movswl	16(%rbx), %eax
	movl	%eax, %edx
	sarl	$5, %eax
	testw	%dx, %dx
	jns	.L1060
	movl	20(%rbx), %eax
.L1060:
	cmpl	-420(%rbp), %eax
	jne	.L1061
	movl	72(%rbx), %r12d
.L1062:
	movq	-432(%rbp), %rax
	movl	-424(%rbp), %esi
	movl	%r12d, 76(%rax)
	testl	%esi, %esi
	jne	.L1096
	movl	84(%rbx), %ecx
	testl	%ecx, %ecx
	js	.L1094
.L1101:
	movq	-472(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	84(%rbx), %esi
	movq	%r15, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, -432(%rbp)
	movzwl	16(%rax), %eax
	testw	%ax, %ax
	jns	.L1145
.L1043:
	movq	-432(%rbp), %rcx
	movl	20(%rcx), %edx
	testl	%edx, %edx
	jne	.L1146
.L1106:
	movl	$65536, -420(%rbp)
	movl	-420(%rbp), %ecx
	movl	$65535, -424(%rbp)
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1061:
	movzwl	-152(%rbp), %edx
	movl	$0, -84(%rbp)
	movl	$0, -68(%rbp)
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, -152(%rbp)
	movq	-432(%rbp), %rax
	movswl	16(%rax), %eax
	testw	%ax, %ax
	js	.L1064
	sarl	$5, %eax
.L1065:
	movl	$0, -480(%rbp)
	movq	-496(%rbp), %r13
	movl	$1, %r12d
	cmpl	-420(%rbp), %eax
	je	.L1147
	movq	%r13, %r10
	movq	%rbx, -440(%rbp)
	xorl	%ebx, %ebx
	movl	-424(%rbp), %r13d
	movzwl	16(%r10), %edi
	testw	%di, %di
	js	.L1068
	.p2align 4,,10
	.p2align 3
.L1149:
	movswl	%di, %eax
	sarl	$5, %eax
	testl	%eax, %eax
	je	.L1148
.L1070:
	leaq	18(%r10), %rdx
	testb	$2, %dil
	jne	.L1074
	movq	32(%r10), %rdx
.L1074:
	movzwl	(%rdx), %edx
	cmpl	%edx, %r13d
	je	.L1114
	movl	76(%r10), %ecx
	cmpl	$1, %ecx
	je	.L1072
	testl	%edx, %edx
	jne	.L1071
.L1075:
	movl	%ecx, %ebx
.L1072:
	movl	84(%r10), %esi
	movq	%r15, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r10
	movzwl	16(%r10), %edi
	testw	%di, %di
	jns	.L1149
.L1068:
	movl	20(%r10), %eax
	testl	%eax, %eax
	jne	.L1070
.L1148:
	cmpl	$65535, %r13d
	je	.L1114
	cmpl	$1, 76(%r10)
	movl	$65535, %edx
	je	.L1072
.L1071:
	movzwl	-408(%rbp), %ecx
	testw	%cx, %cx
	js	.L1076
	movswl	%cx, %esi
	sarl	$5, %esi
.L1077:
	testb	$1, %dil
	je	.L1078
	notl	%ecx
	andl	$1, %ecx
.L1079:
	testb	%cl, %cl
	jne	.L1072
	movl	76(%r10), %ecx
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1046:
	movq	32(%rax), %rax
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1096:
	movq	-472(%rbp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movzwl	-408(%rbp), %eax
	testw	%ax, %ax
	js	.L1098
	movswl	%ax, %edx
	sarl	$5, %edx
.L1099:
	movq	-472(%rbp), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString9doReverseEii@PLT
	movq	-448(%rbp), %rcx
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	-504(%rbp), %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movl	84(%rbx), %edx
	testl	%edx, %edx
	jns	.L1101
	movq	-472(%rbp), %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-496(%rbp), %rax
	movq	-448(%rbp), %rbx
	movq	-504(%rbp), %rdx
	movq	-488(%rbp), %rdi
	movl	76(%rax), %esi
	movq	%rbx, %rcx
	call	_ZN6icu_6720CollationDataBuilder14addContextTrieEjRNS_17UCharsTrieBuilderER10UErrorCode
	movl	%eax, %edx
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1102
	movl	%edx, %eax
	sall	$13, %eax
	orb	$-56, %al
	movl	%eax, %r12d
	cmpl	$524287, %edx
	jle	.L1102
	movq	-448(%rbp), %rax
	xorl	%r12d, %r12d
	movl	$15, (%rax)
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1114:
	movl	%ebx, -480(%rbp)
	movq	-432(%rbp), %r12
	movl	$768, -476(%rbp)
	movq	-440(%rbp), %rbx
.L1067:
	movq	%rbx, -440(%rbp)
	movq	%r14, %rsi
	movl	-476(%rbp), %ebx
	leaq	-352(%rbp), %r13
	movq	%r15, -464(%rbp)
	movq	-488(%rbp), %r14
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1150:
	movswl	%ax, %esi
	sarl	$5, %esi
.L1089:
	subl	$1, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %esi
	movzwl	8(%r15), %eax
	cmpl	%eax, %esi
	jl	.L1090
	cmpl	$65535, %esi
	jg	.L1091
	movl	%esi, %eax
	movq	56(%r15), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L1090
	movl	%esi, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L1090
.L1091:
	movq	%r15, %rdi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	movl	%eax, %r8d
	movl	%ebx, %eax
	orb	$4, %ah
	cmpw	$255, %r8w
	cmova	%eax, %ebx
.L1090:
	movl	72(%r12), %edx
	movq	-448(%rbp), %rcx
	movq	%r13, %rsi
	movq	-456(%rbp), %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	cmpq	%r12, -440(%rbp)
	je	.L1092
	movl	84(%r12), %esi
	movq	-464(%rbp), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rsi
.L1093:
	movl	-420(%rbp), %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_i@PLT
	movq	8(%r14), %r15
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %esi
	movzwl	8(%r15), %eax
	cmpl	%eax, %esi
	jl	.L1087
	cmpl	$65535, %esi
	jg	.L1086
	movl	%esi, %eax
	movq	56(%r15), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L1087
	movl	%esi, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L1087
.L1086:
	movq	%r15, %rdi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	cmpw	$255, %ax
	jbe	.L1087
.L1085:
	movzwl	-344(%rbp), %eax
	movq	8(%r14), %r15
	testw	%ax, %ax
	jns	.L1150
	movl	-340(%rbp), %esi
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1087:
	andb	$-3, %bh
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1092:
	movq	%r13, %rdi
	movl	%ebx, -476(%rbp)
	movq	-464(%rbp), %r15
	movq	-440(%rbp), %rbx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-448(%rbp), %r14
	movq	-488(%rbp), %rdi
	movq	-456(%rbp), %rdx
	movl	-480(%rbp), %esi
	movq	%r14, %rcx
	call	_ZN6icu_6720CollationDataBuilder14addContextTrieEjRNS_17UCharsTrieBuilderER10UErrorCode
	movl	(%r14), %edi
	testl	%edi, %edi
	jg	.L1117
	cmpl	$524287, %eax
	jg	.L1151
	movl	-476(%rbp), %r12d
	sall	$13, %eax
	orb	$-55, %al
	orl	%eax, %r12d
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1078:
	testl	%eax, %eax
	movl	%r12d, %ecx
	movl	$0, %r8d
	cmovle	%eax, %ecx
	js	.L1080
	cmpl	%ecx, %eax
	movl	%ecx, %r8d
	cmovle	%eax, %r8d
.L1080:
	movl	%eax, %r11d
	subl	%ecx, %r11d
	cmpl	%edx, %r11d
	cmovle	%r11d, %edx
	xorl	%r9d, %r9d
	testl	%edx, %edx
	js	.L1081
	subl	%r8d, %eax
	cmpl	%edx, %eax
	cmovg	%edx, %eax
	movl	%eax, %r9d
.L1081:
	andl	$2, %edi
	leaq	18(%r10), %rcx
	jne	.L1083
	movq	32(%r10), %rcx
.L1083:
	movq	-472(%rbp), %rdi
	subl	%edx, %esi
	movq	%r10, -464(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movq	-464(%rbp), %r10
	movl	%eax, %ecx
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1076:
	movl	-404(%rbp), %esi
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1064:
	movq	-432(%rbp), %rax
	movl	20(%rax), %eax
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1147:
	movq	-432(%rbp), %rax
	movq	%r15, %rdi
	movl	72(%rax), %ecx
	movl	84(%rax), %esi
	movl	%ecx, -480(%rbp)
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	$512, -476(%rbp)
	movq	%rax, %r12
	leaq	8(%rax), %r14
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1098:
	movl	-404(%rbp), %edx
	jmp	.L1099
.L1151:
	movq	-448(%rbp), %rax
	xorl	%r12d, %r12d
	movl	$15, (%rax)
.L1094:
	movq	-472(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1102:
	movq	-456(%rbp), %rdi
	call	_ZN6icu_6717UCharsTrieBuilderD1Ev@PLT
	movq	-504(%rbp), %rdi
	call	_ZN6icu_6717UCharsTrieBuilderD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1152
	addq	$472, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1117:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L1094
.L1152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4588:
	.size	_ZN6icu_6720CollationDataBuilder12buildContextEPNS_15ConditionalCE32ER10UErrorCode.part.0, .-_ZN6icu_6720CollationDataBuilder12buildContextEPNS_15ConditionalCE32ER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder12buildContextEPNS_15ConditionalCE32ER10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder12buildContextEPNS_15ConditionalCE32ER10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder12buildContextEPNS_15ConditionalCE32ER10UErrorCode:
.LFB3493:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L1155
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1155:
	jmp	_ZN6icu_6720CollationDataBuilder12buildContextEPNS_15ConditionalCE32ER10UErrorCode.part.0
	.cfi_endproc
.LFE3493:
	.size	_ZN6icu_6720CollationDataBuilder12buildContextEPNS_15ConditionalCE32ER10UErrorCode, .-_ZN6icu_6720CollationDataBuilder12buildContextEPNS_15ConditionalCE32ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728DataBuilderCollationIterator22getCE32FromBuilderDataEjR10UErrorCode
	.type	_ZN6icu_6728DataBuilderCollationIterator22getCE32FromBuilderDataEjR10UErrorCode, @function
_ZN6icu_6728DataBuilderCollationIterator22getCE32FromBuilderDataEjR10UErrorCode:
.LFB3448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	shrl	$13, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$104, %rsp
	movq	392(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$256, %ebx
	je	.L1157
	movq	32(%rdi), %rdi
	movl	%r8d, %esi
	call	utrie2_get32_67@PLT
.L1156:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1173
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1157:
	.cfi_restore_state
	addq	$104, %rdi
	movl	%r8d, %esi
	movq	%rdx, %r12
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r13
	movl	80(%rax), %eax
	cmpl	$1, %eax
	jne	.L1156
	movl	(%r12), %edx
	movq	392(%r15), %r14
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1160
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6720CollationDataBuilder12buildContextEPNS_15ConditionalCE32ER10UErrorCode.part.0
	movl	(%r12), %edx
	movq	392(%r15), %r14
.L1160:
	movl	%eax, 80(%r13)
	cmpl	$15, %edx
	je	.L1174
.L1161:
	movzwl	352(%r14), %edx
	testb	$17, %dl
	jne	.L1171
	andl	$2, %edx
	leaq	354(%r14), %r8
	jne	.L1166
	movq	368(%r14), %r8
.L1166:
	movq	%r8, 424(%r15)
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1174:
	movzwl	352(%r14), %edx
	movl	$0, (%r12)
	leaq	144(%r14), %rsi
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 352(%r14)
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	leaq	104(%r14), %rax
	movq	%rax, -144(%rbp)
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1175:
	movq	32(%r14), %rdi
	movl	-120(%rbp), %esi
	call	utrie2_get32_67@PLT
	movq	-144(%rbp), %rdi
	shrl	$13, %eax
	movl	%eax, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	$1, 80(%rax)
.L1164:
	movq	-136(%rbp), %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	jne	.L1175
	movq	-136(%rbp), %rdi
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	movl	(%r12), %eax
	movq	392(%r15), %r14
	testl	%eax, %eax
	jg	.L1165
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN6icu_6720CollationDataBuilder12buildContextEPNS_15ConditionalCE32ER10UErrorCode.part.0
	movq	392(%r15), %r14
	movl	%eax, %ebx
.L1165:
	movl	%ebx, 80(%r13)
	movl	%ebx, %eax
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1171:
	xorl	%r8d, %r8d
	jmp	.L1166
.L1173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3448:
	.size	_ZN6icu_6728DataBuilderCollationIterator22getCE32FromBuilderDataEjR10UErrorCode, .-_ZN6icu_6728DataBuilderCollationIterator22getCE32FromBuilderDataEjR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder13buildContextsER10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder13buildContextsER10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder13buildContextsER10UErrorCode:
.LFB3492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L1176
	movzwl	352(%rdi), %edx
	leaq	-128(%rbp), %r13
	movq	%rsi, %r12
	movq	%rdi, %rbx
	leaq	144(%rdi), %rsi
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 352(%rdi)
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L1194
	leaq	104(%rbx), %r14
.L1186:
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	je	.L1194
	movl	-120(%rbp), %r15d
	movq	32(%rbx), %rdi
	movl	%r15d, %esi
	call	utrie2_get32_67@PLT
	cmpb	$-65, %al
	ja	.L1180
.L1181:
	movl	$5, (%r12)
.L1194:
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
.L1176:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1195
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1180:
	.cfi_restore_state
	movl	%eax, %edx
	andl	$15, %edx
	cmpl	$7, %edx
	jne	.L1181
	shrl	$13, %eax
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L1196
	movq	32(%rbx), %rdi
	movq	%r12, %rcx
	xorl	%edx, %edx
	movl	%r15d, %esi
	call	utrie2_set32_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L1186
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	%r12, %rdx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6720CollationDataBuilder12buildContextEPNS_15ConditionalCE32ER10UErrorCode.part.0
	movq	32(%rbx), %rdi
	movq	%r12, %rcx
	movl	%r15d, %esi
	movl	%eax, %edx
	call	utrie2_set32_67@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L1186
	jmp	.L1194
.L1195:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3492:
	.size	_ZN6icu_6720CollationDataBuilder13buildContextsER10UErrorCode, .-_ZN6icu_6720CollationDataBuilder13buildContextsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder13buildMappingsERNS_13CollationDataER10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder13buildMappingsERNS_13CollationDataER10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder13buildMappingsERNS_13CollationDataER10UErrorCode:
.LFB3490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -368(%rbp)
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1197
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	movq	%rdx, %r12
	testq	%rdi, %rdi
	je	.L1201
	call	utrie2_isFrozen_67@PLT
	testb	%al, %al
	je	.L1249
.L1201:
	movl	$27, (%r12)
.L1197:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1250
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1249:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6720CollationDataBuilder13buildContextsER10UErrorCode
	movl	(%r12), %edx
	leaq	40(%rbx), %rax
	movq	%rax, -360(%rbp)
	testl	%edx, %edx
	jg	.L1202
	leaq	-336(%rbp), %r14
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6720CollationDataBuilder12getJamoCE32sEPjR10UErrorCode.part.0
	testb	%al, %al
	je	.L1202
	movslq	48(%rbx), %rax
	movq	%r14, -376(%rbp)
	leaq	-68(%rbp), %r13
	movq	%r14, %r15
	movl	%eax, -380(%rbp)
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1251:
	movslq	48(%rbx), %rax
.L1207:
	movl	%eax, %esi
	movl	(%r15), %r14d
	addl	$1, %esi
	js	.L1203
	cmpl	52(%rbx), %esi
	jle	.L1204
.L1203:
	movq	-360(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L1205
	movslq	48(%rbx), %rax
.L1204:
	movq	64(%rbx), %rdx
	movl	%r14d, (%rdx,%rax,4)
	addl	$1, 48(%rbx)
.L1205:
	addq	$4, %r15
	cmpq	%r15, %r13
	jne	.L1251
	leaq	-260(%rbp), %rax
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1253:
	addq	$4, %rax
	cmpq	%rax, %r13
	je	.L1252
.L1210:
	cmpb	$-65, (%rax)
	jbe	.L1253
	movl	$44032, %r13d
	.p2align 4,,10
	.p2align 3
.L1208:
	movl	%r13d, %esi
	movq	32(%rbx), %rdi
	movq	%r12, %r9
	movl	$1, %r8d
	addl	$588, %r13d
	leal	587(%rsi), %edx
	movl	$204, %ecx
	call	utrie2_setRange32_67@PLT
	cmpl	$55204, %r13d
	jne	.L1208
	jmp	.L1214
	.p2align 4,,10
	.p2align 3
.L1202:
	movl	$44032, %r13d
	.p2align 4,,10
	.p2align 3
.L1215:
	movq	16(%rbx), %rax
	movl	%r13d, %esi
	movq	32(%rbx), %rdi
	movq	%r12, %r9
	movl	$1, %r8d
	movq	(%rax), %rdx
	movl	%r13d, %eax
	sarl	$5, %eax
	movq	(%rdx), %rcx
	cltq
	movq	16(%rdx), %rdx
	movzwl	(%rcx,%rax,2), %ecx
	movl	%r13d, %eax
	addl	$588, %r13d
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	movl	(%rdx,%rax,4), %ecx
	leal	587(%rsi), %edx
	call	utrie2_setRange32_67@PLT
	cmpl	$55204, %r13d
	jne	.L1215
	movl	$-1, -380(%rbp)
.L1214:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	$55296, %r15d
	call	_ZN6icu_6720CollationDataBuilder12setDigitTagsER10UErrorCode
	leaq	-340(%rbp), %r14
	leaq	enumRangeLeadValue(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L1216:
	movq	32(%rbx), %rdi
	movl	%r15d, %esi
	movq	%r14, %r8
	movq	%r13, %rcx
	movl	$-1, -340(%rbp)
	xorl	%edx, %edx
	call	utrie2_enumForLeadSurrogate_67@PLT
	movq	32(%rbx), %rdi
	movl	%r15d, %esi
	movq	%r12, %rcx
	movl	-340(%rbp), %edx
	addl	$1, %r15d
	orb	$-51, %dl
	call	utrie2_set32ForLeadSurrogateCodeUnit_67@PLT
	cmpl	$56320, %r15d
	jne	.L1216
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	call	utrie2_get32_67@PLT
	movq	-360(%rbp), %rdi
	xorl	%edx, %edx
	movl	%eax, %esi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %rcx
	xorl	%esi, %esi
	movl	$203, %edx
	call	utrie2_set32_67@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %rdx
	movl	$1, %esi
	call	utrie2_freeze_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L1197
	leaq	408(%rbx), %r12
	movl	$55296, %r13d
	jmp	.L1218
	.p2align 4,,10
	.p2align 3
.L1217:
	addl	$1, %r13d
	cmpl	$56320, %r13d
	je	.L1254
.L1218:
	movl	%r13d, %esi
	movq	%r12, %rdi
	sall	$10, %esi
	leal	-56556545(%rsi), %edx
	subl	$56557568, %esi
	call	_ZNK6icu_6710UnicodeSet12containsNoneEii@PLT
	testb	%al, %al
	jne	.L1217
	movl	%r13d, %esi
	movq	%r12, %rdi
	addl	$1, %r13d
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	cmpl	$56320, %r13d
	jne	.L1218
.L1254:
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	32(%rbx), %rax
	movq	-368(%rbp), %rcx
	movq	64(%rbx), %rdx
	movq	%rax, (%rcx)
	movq	96(%rbx), %rax
	movq	%rdx, 8(%rcx)
	movq	%rax, 16(%rcx)
	movswl	352(%rbx), %eax
	testb	$17, %al
	jne	.L1230
	leaq	354(%rbx), %rcx
	testb	$2, %al
	je	.L1255
.L1219:
	movq	-368(%rbp), %rdi
	movq	%rcx, 24(%rdi)
	movl	48(%rbx), %ecx
	movl	%ecx, 60(%rdi)
	movl	80(%rbx), %ecx
	movl	%ecx, 64(%rdi)
	testw	%ax, %ax
	js	.L1221
	sarl	$5, %eax
	movq	%rdi, %rcx
.L1222:
	movl	%eax, 68(%rcx)
	movq	16(%rbx), %rax
	movl	-380(%rbp), %ebx
	movq	%rax, 32(%rcx)
	testl	%ebx, %ebx
	js	.L1223
	movslq	%ebx, %rax
	leaq	(%rdx,%rax,4), %rax
.L1224:
	movq	%rax, 40(%rcx)
	movq	%r12, 80(%rcx)
	jmp	.L1197
.L1255:
	movq	368(%rbx), %rcx
	jmp	.L1219
.L1223:
	movq	40(%rax), %rax
	jmp	.L1224
.L1221:
	movl	356(%rbx), %eax
	movq	%rdi, %rcx
	jmp	.L1222
.L1230:
	xorl	%ecx, %ecx
	jmp	.L1219
.L1252:
	movl	$44032, %esi
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1256:
	movl	$204, %ecx
	addq	$4, %r14
	call	utrie2_setRange32_67@PLT
	movq	%r14, -376(%rbp)
	cmpl	$55204, %r13d
	je	.L1214
.L1213:
	movl	%r13d, %esi
.L1209:
	movq	-376(%rbp), %r14
	movq	32(%rbx), %rdi
	leal	588(%rsi), %r13d
	leal	587(%rsi), %edx
	movq	%r12, %r9
	movl	$1, %r8d
	cmpb	$-65, (%r14)
	ja	.L1256
	movl	$460, %ecx
	call	utrie2_setRange32_67@PLT
	addq	$4, -376(%rbp)
	cmpl	$55204, %r13d
	jne	.L1213
	jmp	.L1214
.L1250:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3490:
	.size	_ZN6icu_6720CollationDataBuilder13buildMappingsERNS_13CollationDataER10UErrorCode, .-_ZN6icu_6720CollationDataBuilder13buildMappingsERNS_13CollationDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CollationDataBuilder5buildERNS_13CollationDataER10UErrorCode
	.type	_ZN6icu_6720CollationDataBuilder5buildERNS_13CollationDataER10UErrorCode, @function
_ZN6icu_6720CollationDataBuilder5buildERNS_13CollationDataER10UErrorCode:
.LFB3489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6720CollationDataBuilder13buildMappingsERNS_13CollationDataER10UErrorCode
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.L1258
	movl	56(%rax), %edx
	movdqu	104(%rax), %xmm0
	movl	%edx, 56(%rbx)
	movq	72(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movl	100(%rax), %edx
	movl	120(%rax), %eax
	movups	%xmm0, 104(%rbx)
	movl	%edx, 100(%rbx)
	movl	%eax, 120(%rbx)
.L1258:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L1257
	cmpb	$0, 609(%r14)
	je	.L1257
	movq	616(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1260
	movq	(%rdi), %rax
	call	*8(%rax)
.L1260:
	movl	$7376, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1261
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6725CollationFastLatinBuilderC1ER10UErrorCode@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r13, 616(%r14)
	call	_ZN6icu_6725CollationFastLatinBuilder7forDataERKNS_13CollationDataER10UErrorCode@PLT
	testb	%al, %al
	je	.L1262
	movq	616(%r14), %r13
	movzwl	7312(%r13), %eax
	testb	$17, %al
	jne	.L1269
	leaq	7314(%r13), %r15
	testb	$2, %al
	jne	.L1263
	movq	7328(%r13), %r15
.L1263:
	testw	%ax, %ax
	js	.L1265
	movswl	%ax, %r12d
	sarl	$5, %r12d
.L1266:
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.L1267
	cmpl	%r12d, 96(%rax)
	je	.L1283
.L1267:
	movq	%r15, 88(%rbx)
	movl	%r12d, 96(%rbx)
.L1257:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1262:
	.cfi_restore_state
	movq	616(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1268
	movq	(%rdi), %rax
	call	*8(%rax)
.L1268:
	movq	$0, 616(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1265:
	.cfi_restore_state
	movl	7316(%r13), %r12d
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1283:
	leal	(%r12,%r12), %edx
	movq	88(%rax), %rsi
	movq	%r15, %rdi
	movslq	%edx, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L1267
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	movq	16(%r14), %rax
	movq	$0, 616(%r14)
	movq	88(%rax), %r15
	jmp	.L1267
	.p2align 4,,10
	.p2align 3
.L1269:
	xorl	%r15d, %r15d
	jmp	.L1263
.L1261:
	movq	$0, 616(%r14)
	movl	$7, (%r12)
	jmp	.L1257
	.cfi_endproc
.LFE3489:
	.size	_ZN6icu_6720CollationDataBuilder5buildERNS_13CollationDataER10UErrorCode, .-_ZN6icu_6720CollationDataBuilder5buildERNS_13CollationDataER10UErrorCode
	.weak	_ZTSN6icu_6720CollationDataBuilder10CEModifierE
	.section	.rodata._ZTSN6icu_6720CollationDataBuilder10CEModifierE,"aG",@progbits,_ZTSN6icu_6720CollationDataBuilder10CEModifierE,comdat
	.align 32
	.type	_ZTSN6icu_6720CollationDataBuilder10CEModifierE, @object
	.size	_ZTSN6icu_6720CollationDataBuilder10CEModifierE, 44
_ZTSN6icu_6720CollationDataBuilder10CEModifierE:
	.string	"N6icu_6720CollationDataBuilder10CEModifierE"
	.weak	_ZTIN6icu_6720CollationDataBuilder10CEModifierE
	.section	.data.rel.ro._ZTIN6icu_6720CollationDataBuilder10CEModifierE,"awG",@progbits,_ZTIN6icu_6720CollationDataBuilder10CEModifierE,comdat
	.align 8
	.type	_ZTIN6icu_6720CollationDataBuilder10CEModifierE, @object
	.size	_ZTIN6icu_6720CollationDataBuilder10CEModifierE, 24
_ZTIN6icu_6720CollationDataBuilder10CEModifierE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6720CollationDataBuilder10CEModifierE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6720CollationDataBuilderE
	.section	.rodata._ZTSN6icu_6720CollationDataBuilderE,"aG",@progbits,_ZTSN6icu_6720CollationDataBuilderE,comdat
	.align 32
	.type	_ZTSN6icu_6720CollationDataBuilderE, @object
	.size	_ZTSN6icu_6720CollationDataBuilderE, 32
_ZTSN6icu_6720CollationDataBuilderE:
	.string	"N6icu_6720CollationDataBuilderE"
	.weak	_ZTIN6icu_6720CollationDataBuilderE
	.section	.data.rel.ro._ZTIN6icu_6720CollationDataBuilderE,"awG",@progbits,_ZTIN6icu_6720CollationDataBuilderE,comdat
	.align 8
	.type	_ZTIN6icu_6720CollationDataBuilderE, @object
	.size	_ZTIN6icu_6720CollationDataBuilderE, 24
_ZTIN6icu_6720CollationDataBuilderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6720CollationDataBuilderE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6728DataBuilderCollationIteratorE
	.section	.rodata._ZTSN6icu_6728DataBuilderCollationIteratorE,"aG",@progbits,_ZTSN6icu_6728DataBuilderCollationIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6728DataBuilderCollationIteratorE, @object
	.size	_ZTSN6icu_6728DataBuilderCollationIteratorE, 40
_ZTSN6icu_6728DataBuilderCollationIteratorE:
	.string	"N6icu_6728DataBuilderCollationIteratorE"
	.weak	_ZTIN6icu_6728DataBuilderCollationIteratorE
	.section	.data.rel.ro._ZTIN6icu_6728DataBuilderCollationIteratorE,"awG",@progbits,_ZTIN6icu_6728DataBuilderCollationIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6728DataBuilderCollationIteratorE, @object
	.size	_ZTIN6icu_6728DataBuilderCollationIteratorE, 24
_ZTIN6icu_6728DataBuilderCollationIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6728DataBuilderCollationIteratorE
	.quad	_ZTIN6icu_6717CollationIteratorE
	.weak	_ZTVN6icu_6720CollationDataBuilder10CEModifierE
	.section	.data.rel.ro._ZTVN6icu_6720CollationDataBuilder10CEModifierE,"awG",@progbits,_ZTVN6icu_6720CollationDataBuilder10CEModifierE,comdat
	.align 8
	.type	_ZTVN6icu_6720CollationDataBuilder10CEModifierE, @object
	.size	_ZTVN6icu_6720CollationDataBuilder10CEModifierE, 56
_ZTVN6icu_6720CollationDataBuilder10CEModifierE:
	.quad	0
	.quad	_ZTIN6icu_6720CollationDataBuilder10CEModifierE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6728DataBuilderCollationIteratorE
	.section	.data.rel.ro._ZTVN6icu_6728DataBuilderCollationIteratorE,"awG",@progbits,_ZTVN6icu_6728DataBuilderCollationIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6728DataBuilderCollationIteratorE, @object
	.size	_ZTVN6icu_6728DataBuilderCollationIteratorE, 144
_ZTVN6icu_6728DataBuilderCollationIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6728DataBuilderCollationIteratorE
	.quad	_ZN6icu_6728DataBuilderCollationIteratorD1Ev
	.quad	_ZN6icu_6728DataBuilderCollationIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717CollationIteratoreqERKS0_
	.quad	_ZN6icu_6728DataBuilderCollationIterator13resetToOffsetEi
	.quad	_ZNK6icu_6728DataBuilderCollationIterator9getOffsetEv
	.quad	_ZN6icu_6728DataBuilderCollationIterator13nextCodePointER10UErrorCode
	.quad	_ZN6icu_6728DataBuilderCollationIterator17previousCodePointER10UErrorCode
	.quad	_ZN6icu_6717CollationIterator14handleNextCE32ERiR10UErrorCode
	.quad	_ZN6icu_6717CollationIterator23handleGetTrailSurrogateEv
	.quad	_ZN6icu_6717CollationIterator18foundNULTerminatorEv
	.quad	_ZNK6icu_6717CollationIterator25forbidSurrogateCodePointsEv
	.quad	_ZN6icu_6728DataBuilderCollationIterator20forwardNumCodePointsEiR10UErrorCode
	.quad	_ZN6icu_6728DataBuilderCollationIterator21backwardNumCodePointsEiR10UErrorCode
	.quad	_ZNK6icu_6728DataBuilderCollationIterator11getDataCE32Ei
	.quad	_ZN6icu_6728DataBuilderCollationIterator22getCE32FromBuilderDataEjR10UErrorCode
	.weak	_ZTVN6icu_6720CollationDataBuilderE
	.section	.data.rel.ro._ZTVN6icu_6720CollationDataBuilderE,"awG",@progbits,_ZTVN6icu_6720CollationDataBuilderE,comdat
	.align 8
	.type	_ZTVN6icu_6720CollationDataBuilderE, @object
	.size	_ZTVN6icu_6720CollationDataBuilderE, 64
_ZTVN6icu_6720CollationDataBuilderE:
	.quad	0
	.quad	_ZTIN6icu_6720CollationDataBuilderE
	.quad	_ZN6icu_6720CollationDataBuilderD1Ev
	.quad	_ZN6icu_6720CollationDataBuilderD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6720CollationDataBuilder22isCompressibleLeadByteEj
	.quad	_ZN6icu_6720CollationDataBuilder9encodeCEsEPKliR10UErrorCode
	.quad	_ZN6icu_6720CollationDataBuilder5buildERNS_13CollationDataER10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
