	.file	"uspoof_conf.cpp"
	.text
	.p2align 4
	.type	_ZL16SPUStringCompare8UElementS_, @function
_ZL16SPUStringCompare8UElementS_:
.LFB3184:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r10
	movzwl	8(%r10), %eax
	testw	%ax, %ax
	js	.L2
	movswl	%ax, %edx
	sarl	$5, %edx
.L3:
	movq	(%rsi), %rcx
	movzwl	8(%rcx), %edi
	testw	%di, %di
	js	.L4
	movswl	%di, %esi
	sarl	$5, %esi
.L5:
	cmpl	%edx, %esi
	jg	.L11
	movl	$1, %r8d
	jge	.L15
.L1:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	testb	$1, %dil
	je	.L7
	notl	%eax
	andl	$1, %eax
	movl	%eax, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	12(%rcx), %esi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L2:
	movl	12(%r10), %edx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L7:
	testl	%esi, %esi
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%esi, %r8d
	js	.L8
	movl	%esi, %r9d
	subl	%r8d, %r9d
	cmpl	%esi, %r9d
	cmovg	%esi, %r9d
.L8:
	andl	$2, %edi
	jne	.L16
	movq	24(%rcx), %rcx
	xorl	%esi, %esi
	movq	%r10, %rdi
	jmp	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$10, %rcx
	xorl	%esi, %esi
	movq	%r10, %rdi
	jmp	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$-1, %r8d
	jmp	.L1
	.cfi_endproc
.LFE3184:
	.size	_ZL16SPUStringCompare8UElementS_, .-_ZL16SPUStringCompare8UElementS_
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SPUStringC2EPNS_13UnicodeStringE
	.type	_ZN6icu_679SPUStringC2EPNS_13UnicodeStringE, @function
_ZN6icu_679SPUStringC2EPNS_13UnicodeStringE:
.LFB3171:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	$0, 8(%rdi)
	ret
	.cfi_endproc
.LFE3171:
	.size	_ZN6icu_679SPUStringC2EPNS_13UnicodeStringE, .-_ZN6icu_679SPUStringC2EPNS_13UnicodeStringE
	.globl	_ZN6icu_679SPUStringC1EPNS_13UnicodeStringE
	.set	_ZN6icu_679SPUStringC1EPNS_13UnicodeStringE,_ZN6icu_679SPUStringC2EPNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SPUStringD2Ev
	.type	_ZN6icu_679SPUStringD2Ev, @function
_ZN6icu_679SPUStringD2Ev:
.LFB3174:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L18
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L18:
	ret
	.cfi_endproc
.LFE3174:
	.size	_ZN6icu_679SPUStringD2Ev, .-_ZN6icu_679SPUStringD2Ev
	.globl	_ZN6icu_679SPUStringD1Ev
	.set	_ZN6icu_679SPUStringD1Ev,_ZN6icu_679SPUStringD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713SPUStringPoolC2ER10UErrorCode
	.type	_ZN6icu_6713SPUStringPoolC2ER10UErrorCode, @function
_ZN6icu_6713SPUStringPoolC2ER10UErrorCode:
.LFB3177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L21
	movq	%rax, %r12
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	%r12, (%rbx)
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rdi
	call	uhash_open_67@PLT
	movq	%rax, 8(%rbx)
.L20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	movq	$0, (%rbx)
	movl	$7, 0(%r13)
	jmp	.L20
	.cfi_endproc
.LFE3177:
	.size	_ZN6icu_6713SPUStringPoolC2ER10UErrorCode, .-_ZN6icu_6713SPUStringPoolC2ER10UErrorCode
	.globl	_ZN6icu_6713SPUStringPoolC1ER10UErrorCode
	.set	_ZN6icu_6713SPUStringPoolC1ER10UErrorCode,_ZN6icu_6713SPUStringPoolC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713SPUStringPoolD2Ev
	.type	_ZN6icu_6713SPUStringPoolD2Ev, @function
_ZN6icu_6713SPUStringPoolD2Ev:
.LFB3180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	movl	8(%rdi), %ebx
	subl	$1, %ebx
	js	.L25
	.p2align 4,,10
	.p2align 3
.L28:
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L26
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L27
	movq	(%rdi), %rax
	call	*8(%rax)
.L27:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L26:
	subl	$1, %ebx
	movq	0(%r13), %rdi
	cmpl	$-1, %ebx
	jne	.L28
	testq	%rdi, %rdi
	je	.L29
.L25:
	movq	(%rdi), %rax
	call	*8(%rax)
.L29:
	movq	8(%r13), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uhash_close_67@PLT
	.cfi_endproc
.LFE3180:
	.size	_ZN6icu_6713SPUStringPoolD2Ev, .-_ZN6icu_6713SPUStringPoolD2Ev
	.globl	_ZN6icu_6713SPUStringPoolD1Ev
	.set	_ZN6icu_6713SPUStringPoolD1Ev,_ZN6icu_6713SPUStringPoolD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713SPUStringPool4sizeEv
	.type	_ZN6icu_6713SPUStringPool4sizeEv, @function
_ZN6icu_6713SPUStringPool4sizeEv:
.LFB3182:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rax), %eax
	ret
	.cfi_endproc
.LFE3182:
	.size	_ZN6icu_6713SPUStringPool4sizeEv, .-_ZN6icu_6713SPUStringPool4sizeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713SPUStringPool10getByIndexEi
	.type	_ZN6icu_6713SPUStringPool10getByIndexEi, @function
_ZN6icu_6713SPUStringPool10getByIndexEi:
.LFB3183:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	_ZNK6icu_677UVector9elementAtEi@PLT
	.cfi_endproc
.LFE3183:
	.size	_ZN6icu_6713SPUStringPool10getByIndexEi, .-_ZN6icu_6713SPUStringPool10getByIndexEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713SPUStringPool4sortER10UErrorCode
	.type	_ZN6icu_6713SPUStringPool4sortER10UErrorCode, @function
_ZN6icu_6713SPUStringPool4sortER10UErrorCode:
.LFB3185:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	%rsi, %rdx
	leaq	_ZL16SPUStringCompare8UElementS_(%rip), %rsi
	jmp	_ZN6icu_677UVector4sortEPFa8UElementS1_ER10UErrorCode@PLT
	.cfi_endproc
.LFE3185:
	.size	_ZN6icu_6713SPUStringPool4sortER10UErrorCode, .-_ZN6icu_6713SPUStringPool4sortER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713SPUStringPool9addStringEPNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6713SPUStringPool9addStringEPNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6713SPUStringPool9addStringEPNS_13UnicodeStringER10UErrorCode:
.LFB3186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L45
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L44
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L44:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L47
	movq	%r13, (%rax)
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	movl	$0, 8(%rax)
	movq	%rax, %rdx
	call	uhash_put_67@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L47:
	.cfi_restore_state
	movl	$7, (%r14)
	jmp	.L44
	.cfi_endproc
.LFE3186:
	.size	_ZN6icu_6713SPUStringPool9addStringEPNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6713SPUStringPool9addStringEPNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721ConfusabledataBuilderC2EPNS_9SpoofImplER10UErrorCode
	.type	_ZN6icu_6721ConfusabledataBuilderC2EPNS_9SpoofImplER10UErrorCode, @function
_ZN6icu_6721ConfusabledataBuilderC2EPNS_9SpoofImplER10UErrorCode:
.LFB3188:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movl	$0, 80(%rdi)
	testl	%eax, %eax
	jle	.L64
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	movq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	call	uhash_open_67@PLT
	movl	$200, %edi
	movq	%rax, 16(%rbx)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L54
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r13, 24(%rbx)
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L55
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	%r13, 32(%rbx)
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L56
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	%r13, 40(%rbx)
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L57
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movups	%xmm0, (%rax)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L58
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	%r14, 0(%r13)
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rdi
	call	uhash_open_67@PLT
	movq	%rax, 8(%r13)
.L59:
	movq	%r13, 56(%rbx)
.L52:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L54:
	.cfi_restore_state
	movq	$0, 24(%rbx)
	movl	$7, (%r12)
	jmp	.L52
.L55:
	movq	$0, 32(%rbx)
	movl	$7, (%r12)
	jmp	.L52
.L56:
	movq	$0, 40(%rbx)
	movl	$7, (%r12)
	jmp	.L52
.L57:
	movq	$0, 56(%rbx)
	movl	$7, (%r12)
	jmp	.L52
.L58:
	movq	$0, 0(%r13)
	movl	$7, (%r12)
	jmp	.L59
	.cfi_endproc
.LFE3188:
	.size	_ZN6icu_6721ConfusabledataBuilderC2EPNS_9SpoofImplER10UErrorCode, .-_ZN6icu_6721ConfusabledataBuilderC2EPNS_9SpoofImplER10UErrorCode
	.globl	_ZN6icu_6721ConfusabledataBuilderC1EPNS_9SpoofImplER10UErrorCode
	.set	_ZN6icu_6721ConfusabledataBuilderC1EPNS_9SpoofImplER10UErrorCode,_ZN6icu_6721ConfusabledataBuilderC2EPNS_9SpoofImplER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721ConfusabledataBuilder10outputDataER10UErrorCode
	.type	_ZN6icu_6721ConfusabledataBuilder10outputDataER10UErrorCode, @function
_ZN6icu_6721ConfusabledataBuilder10outputDataER10UErrorCode:
.LFB3195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rbx, %rdx
	subq	$40, %rsp
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movl	8(%rax), %r14d
	movq	(%rdi), %rax
	movq	16(%rax), %rdi
	leal	0(,%r14,4), %esi
	call	_ZN6icu_679SpoofData12reserveSpaceEiR10UErrorCode@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L65
	movq	%rax, %r12
	testl	%r14d, %r14d
	jle	.L68
	leal	-1(%r14), %r15d
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L69:
	movq	32(%r13), %rdi
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector10elementAtiEi@PLT
	movl	%eax, (%r12,%rbx,4)
	movq	%rbx, %rax
	addq	$1, %rbx
	cmpq	%rax, %r15
	jne	.L69
.L68:
	movq	0(%r13), %rax
	movq	%r12, %rdx
	leal	(%r14,%r14), %esi
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	subq	%rax, %rdx
	movl	%r14d, 16(%rax)
	movq	-72(%rbp), %r14
	movl	%edx, 12(%rax)
	movq	40(%r13), %rax
	movq	%r12, 32(%rdi)
	movq	%r14, %rdx
	movl	8(%rax), %r12d
	call	_ZN6icu_679SpoofData12reserveSpaceEiR10UErrorCode@PLT
	movl	(%r14), %edx
	movq	%rax, %rbx
	testl	%edx, %edx
	jg	.L65
	testl	%r12d, %r12d
	jle	.L71
	leal	-1(%r12), %r14d
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L72:
	movq	40(%r13), %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_677UVector10elementAtiEi@PLT
	movw	%ax, (%rbx,%r15,2)
	movq	%r15, %rax
	addq	$1, %r15
	cmpq	%r14, %rax
	jne	.L72
.L71:
	movq	0(%r13), %rax
	movq	%rbx, %rdx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	subq	%rax, %rdx
	movl	%r12d, 24(%rax)
	movl	%edx, 20(%rax)
	movq	48(%r13), %rdx
	movq	%rbx, 40(%rdi)
	movzwl	8(%rdx), %eax
	testw	%ax, %ax
	js	.L73
	movswl	%ax, %r12d
	sarl	$5, %r12d
.L74:
	movq	-72(%rbp), %r15
	leal	1(%r12), %r14d
	leal	(%r14,%r14), %esi
	movq	%r15, %rdx
	call	_ZN6icu_679SpoofData12reserveSpaceEiR10UErrorCode@PLT
	movq	%rax, %rbx
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L65
	movq	48(%r13), %rdi
	movq	-72(%rbp), %rcx
	leaq	-64(%rbp), %rsi
	movl	%r14d, %edx
	movq	%rbx, -64(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movq	0(%r13), %rax
	movq	%rbx, %rcx
	movq	16(%rax), %rdx
	movq	(%rdx), %rax
	subq	%rax, %rcx
	movl	%r12d, 32(%rax)
	movl	%ecx, 28(%rax)
	movq	%rbx, 48(%rdx)
	.p2align 4,,10
	.p2align 3
.L65:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movl	12(%rdx), %r12d
	jmp	.L74
.L79:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3195:
	.size	_ZN6icu_6721ConfusabledataBuilder10outputDataER10UErrorCode, .-_ZN6icu_6721ConfusabledataBuilder10outputDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721ConfusabledataBuilderD2Ev
	.type	_ZN6icu_6721ConfusabledataBuilderD2Ev, @function
_ZN6icu_6721ConfusabledataBuilderD2Ev:
.LFB3191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	64(%rbx), %rdi
	call	uregex_close_67@PLT
	movq	72(%rbx), %rdi
	call	uregex_close_67@PLT
	movq	16(%rbx), %rdi
	call	uhash_close_67@PLT
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L81
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L81:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L82
	movq	(%rdi), %rax
	call	*8(%rax)
.L82:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L83
	movq	(%rdi), %rax
	call	*8(%rax)
.L83:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L84
	movq	(%rdi), %rax
	call	*8(%rax)
.L84:
	movq	56(%rbx), %r12
	testq	%r12, %r12
	je	.L80
	movq	%r12, %rdi
	call	_ZN6icu_6713SPUStringPoolD1Ev
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3191:
	.size	_ZN6icu_6721ConfusabledataBuilderD2Ev, .-_ZN6icu_6721ConfusabledataBuilderD2Ev
	.globl	_ZN6icu_6721ConfusabledataBuilderD1Ev
	.set	_ZN6icu_6721ConfusabledataBuilderD1Ev,_ZN6icu_6721ConfusabledataBuilderD2Ev
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"(?m)^[ \\t]*([0-9A-Fa-f]+)[ \\t]+;[ \\t]*([0-9A-Fa-f]+(?:[ \\t]+[0-9A-Fa-f]+)*)[ \\t]*;\\s*(?:(SL)|(SA)|(ML)|(MA))[ \\t]*(?:#.*?)?$|^([ \\t]*(?:#.*?)?)$|^(.*?)$"
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC1:
	.string	"\\"
	.string	"s"
	.string	"*"
	.string	"("
	.string	"["
	.string	"0"
	.string	"-"
	.string	"9"
	.string	"A"
	.string	"-"
	.string	"F"
	.string	"]"
	.string	"+"
	.string	")"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721ConfusabledataBuilder5buildEPKciR10UErrorCode
	.type	_ZN6icu_6721ConfusabledataBuilder5buildEPKciR10UErrorCode, @function
_ZN6icu_6721ConfusabledataBuilder5buildEPKciR10UErrorCode:
.LFB3194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -204(%rbp)
	testl	%r8d, %r8d
	jle	.L152
.L99:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movl	%edx, %ebx
	movq	%rcx, %r13
	movq	%rcx, %r9
	movq	%rdi, %r12
	movq	%rsi, %rcx
	xorl	%edi, %edi
	movq	%rsi, %r14
	movl	%ebx, %r8d
	xorl	%esi, %esi
	leaq	-204(%rbp), %rdx
	call	u_strFromUTF8_67@PLT
	cmpl	$15, 0(%r13)
	jne	.L99
	movl	-204(%rbp), %eax
	movl	$0, 0(%r13)
	leal	1(%rax), %edi
	movslq	%edi, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L154
	movl	-204(%rbp), %eax
	movq	%r13, %r9
	movl	%ebx, %r8d
	movq	%r14, %rcx
	xorl	%edx, %edx
	leal	1(%rax), %esi
	call	u_strFromUTF8_67@PLT
	leaq	-192(%rbp), %rax
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rax, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%rax, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L104
	movswl	%ax, %esi
	sarl	$5, %esi
.L105:
	testb	$17, %al
	jne	.L141
	leaq	-182(%rbp), %rdi
	testb	$2, %al
	cmove	-168(%rbp), %rdi
.L106:
	movq	%r13, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	uregex_open_67@PLT
	leaq	-128(%rbp), %r14
	movl	$-1, %ecx
	leaq	-200(%rbp), %rdx
	movq	%rax, 64(%r12)
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rax
	movl	$1, %esi
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-232(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L107
	movswl	%ax, %esi
	sarl	$5, %esi
.L108:
	testb	$17, %al
	jne	.L143
	leaq	-182(%rbp), %rdi
	testb	$2, %al
	cmove	-168(%rbp), %rdi
.L109:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %r8
	call	uregex_open_67@PLT
	movq	8(%r12), %rsi
	movq	%rax, 72(%r12)
	cmpw	$-257, (%rsi)
	jne	.L110
	movl	$32, %edi
	movw	%di, (%rsi)
.L110:
	movq	64(%r12), %rdi
	movl	-204(%rbp), %edx
	movq	%r13, %rcx
	call	uregex_setText_67@PLT
	.p2align 4,,10
	.p2align 3
.L112:
	movq	64(%r12), %rdi
	movq	%r13, %rsi
	call	uregex_findNext_67@PLT
	testb	%al, %al
	je	.L111
	addl	$1, 80(%r12)
	movq	64(%r12), %rdi
	movq	%r13, %rdx
	movl	$7, %esi
	call	uregex_start_67@PLT
	testl	%eax, %eax
	jns	.L112
	movq	64(%r12), %rdi
	movq	%r13, %rdx
	movl	$8, %esi
	call	uregex_start_67@PLT
	testl	%eax, %eax
	jns	.L155
	movq	64(%r12), %rdi
	movq	%r13, %rdx
	movl	$1, %esi
	call	uregex_end_67@PLT
	movq	64(%r12), %rdi
	movq	%r13, %rdx
	movl	$1, %esi
	movl	%eax, %r14d
	call	uregex_start_67@PLT
	movq	8(%r12), %rdi
	movq	%r13, %rcx
	movl	%r14d, %edx
	movl	%eax, %esi
	call	_ZN6icu_679SpoofImpl7ScanHexEPKDsiiR10UErrorCode@PLT
	movq	64(%r12), %rdi
	movq	%r13, %rdx
	movl	$2, %esi
	movl	%eax, -212(%rbp)
	call	uregex_start_67@PLT
	movq	64(%r12), %rdi
	movq	%r13, %rdx
	movl	$2, %esi
	movslq	%eax, %rbx
	call	uregex_end_67@PLT
	movq	%rbx, %r14
	movq	8(%r12), %rsi
	addq	%rbx, %rbx
	subl	%r14d, %eax
	movq	72(%r12), %rdi
	movq	%r13, %rcx
	movl	%eax, %edx
	addq	%rbx, %rsi
	call	uregex_setText_67@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L115
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %esi
	movq	%rax, (%r14)
	movw	%si, 8(%r14)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L156:
	movq	72(%r12), %rdi
	movq	%r13, %rdx
	movl	$1, %esi
	call	uregex_end_67@PLT
	movq	72(%r12), %rdi
	movq	%r13, %rdx
	movl	$1, %esi
	movl	%eax, %r15d
	call	uregex_start_67@PLT
	movq	8(%r12), %rdi
	movq	%r13, %rcx
	movl	%r15d, %edx
	movl	%eax, %esi
	addq	%rbx, %rdi
	call	_ZN6icu_679SpoofImpl7ScanHexEPKDsiiR10UErrorCode@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
.L117:
	movq	72(%r12), %rdi
	movq	%r13, %rsi
	call	uregex_findNext_67@PLT
	testb	%al, %al
	jne	.L156
	movq	56(%r12), %r8
	movq	%r14, %rsi
	movq	8(%r8), %rdi
	movq	%r8, -224(%rbp)
	call	uhash_get_67@PLT
	movq	-224(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L118
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L119:
	movq	%rbx, %rdx
	movl	-212(%rbp), %ebx
	movq	16(%r12), %rdi
	movq	%r13, %rcx
	movl	%ebx, %esi
	call	uhash_iput_67@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L114
	movq	24(%r12), %rdi
	movl	%ebx, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L133:
	xorl	%r14d, %r14d
.L124:
	movq	24(%r12), %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	cmpl	%r14d, %eax
	jle	.L134
	movq	24(%r12), %rdi
	movl	%r14d, %esi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	%eax, %ebx
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L157:
	movswl	%dx, %ecx
	movl	%ecx, %edx
	sarl	$5, %edx
	cmpl	$8223, %ecx
	jg	.L137
.L138:
	leal	-1(%rdx), %esi
	movq	32(%r12), %rdi
	movl	8(%rax), %r15d
	movq	%r13, %rdx
	sall	$24, %esi
	orl	%ebx, %esi
	addl	$1, %ebx
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movq	40(%r12), %rdi
	movq	%r13, %rdx
	movl	%r15d, %esi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
.L139:
	movq	24(%r12), %rdi
	movl	%r14d, %esi
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	cmpl	%ebx, %eax
	jl	.L135
	movq	16(%r12), %rdi
	movl	%ebx, %esi
	call	uhash_iget_67@PLT
	movq	(%rax), %rcx
	movzwl	8(%rcx), %edx
	testw	%dx, %dx
	jns	.L157
	movl	12(%rcx), %edx
	cmpl	$256, %edx
	jle	.L138
.L137:
	movl	$1, 0(%r13)
.L114:
	movq	-232(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L118:
	movl	$16, %edi
	movq	%r8, -224(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L120
	movq	-224(%rbp), %r8
	movq	%r14, (%rax)
	movq	%r14, %rsi
	movq	%r13, %rcx
	movl	$0, 8(%rax)
	movq	%rax, %rdx
	movq	8(%r8), %rdi
	call	uhash_put_67@PLT
	movq	-224(%rbp), %r8
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	(%r8), %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L111:
	movq	56(%r12), %rax
	movq	%r13, %rdx
	leaq	_ZL16SPUStringCompare8UElementS_(%rip), %rsi
	movq	(%rax), %rdi
	call	_ZN6icu_677UVector4sortEPFa8UElementS1_ER10UErrorCode@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L122
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movl	$2, %edx
	movq	%rcx, (%rax)
	movw	%dx, 8(%rax)
.L122:
	movq	%rax, 48(%r12)
	movq	56(%r12), %rax
	xorl	%r14d, %r14d
	movq	(%rax), %rdi
	movl	8(%rdi), %ebx
	testl	%ebx, %ebx
	jg	.L123
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L158:
	andl	$2, %r8d
	je	.L130
	addq	$10, %rsi
.L131:
	movzwl	(%rsi), %edx
	movl	%edx, 8(%rax)
.L132:
	addl	$1, %r14d
	cmpl	%ebx, %r14d
	je	.L133
	movq	56(%r12), %rax
	movq	(%rax), %rdi
.L123:
	movl	%r14d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	(%rax), %rsi
	movzwl	8(%rsi), %r8d
	testw	%r8w, %r8w
	js	.L125
	movswl	%r8w, %ecx
	sarl	$5, %ecx
.L126:
	movq	48(%r12), %rdi
	movswl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L127
	sarl	$5, %edx
.L128:
	cmpl	$1, %ecx
	je	.L158
	movl	%edx, 8(%rax)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L127:
	movl	12(%rdi), %edx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L125:
	movl	12(%rsi), %ecx
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L130:
	movq	24(%rsi), %rsi
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L135:
	addl	$1, %r14d
	jmp	.L124
.L107:
	movl	-180(%rbp), %esi
	jmp	.L108
.L104:
	movl	-180(%rbp), %esi
	jmp	.L105
.L143:
	xorl	%edi, %edi
	jmp	.L109
.L141:
	xorl	%edi, %edi
	jmp	.L106
.L155:
	movl	$9, 0(%r13)
	jmp	.L114
.L134:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6721ConfusabledataBuilder10outputDataER10UErrorCode
	jmp	.L114
.L153:
	call	__stack_chk_fail@PLT
.L154:
	movl	$7, 0(%r13)
	jmp	.L99
.L115:
	movl	$7, 0(%r13)
	jmp	.L114
.L120:
	movl	$7, 0(%r13)
	jmp	.L119
	.cfi_endproc
.LFE3194:
	.size	_ZN6icu_6721ConfusabledataBuilder5buildEPKciR10UErrorCode, .-_ZN6icu_6721ConfusabledataBuilder5buildEPKciR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721ConfusabledataBuilder19buildConfusableDataEPNS_9SpoofImplEPKciPiP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6721ConfusabledataBuilder19buildConfusableDataEPNS_9SpoofImplEPKciPiP11UParseErrorR10UErrorCode, @function
_ZN6icu_6721ConfusabledataBuilder19buildConfusableDataEPNS_9SpoofImplEPKciPiP11UParseErrorR10UErrorCode:
.LFB3193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L178
.L159:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	%rdi, -144(%rbp)
	movq	%rsi, %r13
	movl	%edx, %r14d
	movq	%rcx, %r12
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	xorl	%edx, %edx
	movq	%r9, %rcx
	movq	%r8, %r15
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	movq	$0, -128(%rbp)
	movq	%r9, %rbx
	movq	$0, -136(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	$0, -64(%rbp)
	call	uhash_open_67@PLT
	movl	$200, %edi
	movq	%rax, -128(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L162
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	-152(%rbp), %rax
	movl	$40, %edi
	movq	%rax, -120(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L163
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%rax, -152(%rbp)
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	-152(%rbp), %rax
	movl	$40, %edi
	movq	%rax, -112(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L164
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%rax, -152(%rbp)
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	-152(%rbp), %rax
	movl	$16, %edi
	movq	%rax, -104(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L165
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	%rax, -152(%rbp)
	movups	%xmm0, (%rax)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-152(%rbp), %r8
	testq	%rax, %rax
	je	.L166
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%r8, -160(%rbp)
	movq	%rax, -152(%rbp)
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	-160(%rbp), %r8
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	-152(%rbp), %rax
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rdi
	movq	%r8, -152(%rbp)
	movq	%rax, (%r8)
	call	uhash_open_67@PLT
	movq	-152(%rbp), %r8
	movq	%rax, 8(%r8)
.L167:
	movq	%r8, -88(%rbp)
.L169:
	leaq	-144(%rbp), %rdi
	movq	%rbx, %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%rdi, -152(%rbp)
	call	_ZN6icu_6721ConfusabledataBuilder5buildEPKciR10UErrorCode
	movl	(%rbx), %eax
	movq	-152(%rbp), %rdi
	testl	%eax, %eax
	jle	.L168
	testq	%r12, %r12
	je	.L168
	movl	-64(%rbp), %eax
	movl	$1, (%r12)
	movl	%eax, (%r15)
.L168:
	call	_ZN6icu_6721ConfusabledataBuilderD1Ev
	jmp	.L159
.L163:
	movq	$0, -112(%rbp)
	movl	$7, (%rbx)
	jmp	.L169
.L179:
	call	__stack_chk_fail@PLT
.L166:
	movq	$0, (%r8)
	movl	$7, (%rbx)
	jmp	.L167
.L162:
	movq	$0, -120(%rbp)
	movl	$7, (%rbx)
	jmp	.L169
.L165:
	movq	$0, -88(%rbp)
	movl	$7, (%rbx)
	jmp	.L169
.L164:
	movq	$0, -104(%rbp)
	movl	$7, (%rbx)
	jmp	.L169
	.cfi_endproc
.LFE3193:
	.size	_ZN6icu_6721ConfusabledataBuilder19buildConfusableDataEPNS_9SpoofImplEPKciPiP11UParseErrorR10UErrorCode, .-_ZN6icu_6721ConfusabledataBuilder19buildConfusableDataEPNS_9SpoofImplEPKciPiP11UParseErrorR10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
