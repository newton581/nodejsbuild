	.file	"nfsubs.cpp"
	.text
	.section	.text._ZNK6icu_6721SameValueSubstitution15transformNumberEl,"axG",@progbits,_ZNK6icu_6721SameValueSubstitution15transformNumberEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721SameValueSubstitution15transformNumberEl
	.type	_ZNK6icu_6721SameValueSubstitution15transformNumberEl, @function
_ZNK6icu_6721SameValueSubstitution15transformNumberEl:
.LFB2878:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2878:
	.size	_ZNK6icu_6721SameValueSubstitution15transformNumberEl, .-_ZNK6icu_6721SameValueSubstitution15transformNumberEl
	.section	.text._ZNK6icu_6721SameValueSubstitution15transformNumberEd,"axG",@progbits,_ZNK6icu_6721SameValueSubstitution15transformNumberEd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721SameValueSubstitution15transformNumberEd
	.type	_ZNK6icu_6721SameValueSubstitution15transformNumberEd, @function
_ZNK6icu_6721SameValueSubstitution15transformNumberEd:
.LFB2879:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2879:
	.size	_ZNK6icu_6721SameValueSubstitution15transformNumberEd, .-_ZNK6icu_6721SameValueSubstitution15transformNumberEd
	.section	.text._ZNK6icu_6721SameValueSubstitution16composeRuleValueEdd,"axG",@progbits,_ZNK6icu_6721SameValueSubstitution16composeRuleValueEdd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721SameValueSubstitution16composeRuleValueEdd
	.type	_ZNK6icu_6721SameValueSubstitution16composeRuleValueEdd, @function
_ZNK6icu_6721SameValueSubstitution16composeRuleValueEdd:
.LFB2880:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2880:
	.size	_ZNK6icu_6721SameValueSubstitution16composeRuleValueEdd, .-_ZNK6icu_6721SameValueSubstitution16composeRuleValueEdd
	.section	.text._ZNK6icu_6721SameValueSubstitution14calcUpperBoundEd,"axG",@progbits,_ZNK6icu_6721SameValueSubstitution14calcUpperBoundEd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721SameValueSubstitution14calcUpperBoundEd
	.type	_ZNK6icu_6721SameValueSubstitution14calcUpperBoundEd, @function
_ZNK6icu_6721SameValueSubstitution14calcUpperBoundEd:
.LFB2881:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2881:
	.size	_ZNK6icu_6721SameValueSubstitution14calcUpperBoundEd, .-_ZNK6icu_6721SameValueSubstitution14calcUpperBoundEd
	.section	.text._ZNK6icu_6721SameValueSubstitution9tokenCharEv,"axG",@progbits,_ZNK6icu_6721SameValueSubstitution9tokenCharEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721SameValueSubstitution9tokenCharEv
	.type	_ZNK6icu_6721SameValueSubstitution9tokenCharEv, @function
_ZNK6icu_6721SameValueSubstitution9tokenCharEv:
.LFB2882:
	.cfi_startproc
	endbr64
	movl	$61, %eax
	ret
	.cfi_endproc
.LFE2882:
	.size	_ZNK6icu_6721SameValueSubstitution9tokenCharEv, .-_ZNK6icu_6721SameValueSubstitution9tokenCharEv
	.section	.text._ZNK6icu_6722MultiplierSubstitution15transformNumberEl,"axG",@progbits,_ZNK6icu_6722MultiplierSubstitution15transformNumberEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6722MultiplierSubstitution15transformNumberEl
	.type	_ZNK6icu_6722MultiplierSubstitution15transformNumberEl, @function
_ZNK6icu_6722MultiplierSubstitution15transformNumberEl:
.LFB2891:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	cqto
	idivq	32(%rdi)
	ret
	.cfi_endproc
.LFE2891:
	.size	_ZNK6icu_6722MultiplierSubstitution15transformNumberEl, .-_ZNK6icu_6722MultiplierSubstitution15transformNumberEl
	.section	.text._ZNK6icu_6722MultiplierSubstitution16composeRuleValueEdd,"axG",@progbits,_ZNK6icu_6722MultiplierSubstitution16composeRuleValueEdd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6722MultiplierSubstitution16composeRuleValueEdd
	.type	_ZNK6icu_6722MultiplierSubstitution16composeRuleValueEdd, @function
_ZNK6icu_6722MultiplierSubstitution16composeRuleValueEdd:
.LFB2893:
	.cfi_startproc
	endbr64
	movapd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdq	32(%rdi), %xmm0
	mulsd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE2893:
	.size	_ZNK6icu_6722MultiplierSubstitution16composeRuleValueEdd, .-_ZNK6icu_6722MultiplierSubstitution16composeRuleValueEdd
	.section	.text._ZNK6icu_6722MultiplierSubstitution14calcUpperBoundEd,"axG",@progbits,_ZNK6icu_6722MultiplierSubstitution14calcUpperBoundEd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6722MultiplierSubstitution14calcUpperBoundEd
	.type	_ZNK6icu_6722MultiplierSubstitution14calcUpperBoundEd, @function
_ZNK6icu_6722MultiplierSubstitution14calcUpperBoundEd:
.LFB2894:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	cvtsi2sdq	32(%rdi), %xmm0
	ret
	.cfi_endproc
.LFE2894:
	.size	_ZNK6icu_6722MultiplierSubstitution14calcUpperBoundEd, .-_ZNK6icu_6722MultiplierSubstitution14calcUpperBoundEd
	.section	.text._ZNK6icu_6722MultiplierSubstitution9tokenCharEv,"axG",@progbits,_ZNK6icu_6722MultiplierSubstitution9tokenCharEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6722MultiplierSubstitution9tokenCharEv
	.type	_ZNK6icu_6722MultiplierSubstitution9tokenCharEv, @function
_ZNK6icu_6722MultiplierSubstitution9tokenCharEv:
.LFB2895:
	.cfi_startproc
	endbr64
	movl	$60, %eax
	ret
	.cfi_endproc
.LFE2895:
	.size	_ZNK6icu_6722MultiplierSubstitution9tokenCharEv, .-_ZNK6icu_6722MultiplierSubstitution9tokenCharEv
	.section	.text._ZNK6icu_6719ModulusSubstitution15transformNumberEl,"axG",@progbits,_ZNK6icu_6719ModulusSubstitution15transformNumberEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719ModulusSubstitution15transformNumberEl
	.type	_ZNK6icu_6719ModulusSubstitution15transformNumberEl, @function
_ZNK6icu_6719ModulusSubstitution15transformNumberEl:
.LFB2901:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	cqto
	idivq	32(%rdi)
	movq	%rdx, %rax
	ret
	.cfi_endproc
.LFE2901:
	.size	_ZNK6icu_6719ModulusSubstitution15transformNumberEl, .-_ZNK6icu_6719ModulusSubstitution15transformNumberEl
	.section	.text._ZNK6icu_6719ModulusSubstitution14calcUpperBoundEd,"axG",@progbits,_ZNK6icu_6719ModulusSubstitution14calcUpperBoundEd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719ModulusSubstitution14calcUpperBoundEd
	.type	_ZNK6icu_6719ModulusSubstitution14calcUpperBoundEd, @function
_ZNK6icu_6719ModulusSubstitution14calcUpperBoundEd:
.LFB2904:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	cvtsi2sdq	32(%rdi), %xmm0
	ret
	.cfi_endproc
.LFE2904:
	.size	_ZNK6icu_6719ModulusSubstitution14calcUpperBoundEd, .-_ZNK6icu_6719ModulusSubstitution14calcUpperBoundEd
	.section	.text._ZNK6icu_6719ModulusSubstitution21isModulusSubstitutionEv,"axG",@progbits,_ZNK6icu_6719ModulusSubstitution21isModulusSubstitutionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719ModulusSubstitution21isModulusSubstitutionEv
	.type	_ZNK6icu_6719ModulusSubstitution21isModulusSubstitutionEv, @function
_ZNK6icu_6719ModulusSubstitution21isModulusSubstitutionEv:
.LFB2905:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2905:
	.size	_ZNK6icu_6719ModulusSubstitution21isModulusSubstitutionEv, .-_ZNK6icu_6719ModulusSubstitution21isModulusSubstitutionEv
	.section	.text._ZNK6icu_6719ModulusSubstitution9tokenCharEv,"axG",@progbits,_ZNK6icu_6719ModulusSubstitution9tokenCharEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719ModulusSubstitution9tokenCharEv
	.type	_ZNK6icu_6719ModulusSubstitution9tokenCharEv, @function
_ZNK6icu_6719ModulusSubstitution9tokenCharEv:
.LFB2906:
	.cfi_startproc
	endbr64
	movl	$62, %eax
	ret
	.cfi_endproc
.LFE2906:
	.size	_ZNK6icu_6719ModulusSubstitution9tokenCharEv, .-_ZNK6icu_6719ModulusSubstitution9tokenCharEv
	.section	.text._ZNK6icu_6724IntegralPartSubstitution15transformNumberEl,"axG",@progbits,_ZNK6icu_6724IntegralPartSubstitution15transformNumberEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6724IntegralPartSubstitution15transformNumberEl
	.type	_ZNK6icu_6724IntegralPartSubstitution15transformNumberEl, @function
_ZNK6icu_6724IntegralPartSubstitution15transformNumberEl:
.LFB2914:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2914:
	.size	_ZNK6icu_6724IntegralPartSubstitution15transformNumberEl, .-_ZNK6icu_6724IntegralPartSubstitution15transformNumberEl
	.section	.text._ZNK6icu_6724IntegralPartSubstitution16composeRuleValueEdd,"axG",@progbits,_ZNK6icu_6724IntegralPartSubstitution16composeRuleValueEdd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6724IntegralPartSubstitution16composeRuleValueEdd
	.type	_ZNK6icu_6724IntegralPartSubstitution16composeRuleValueEdd, @function
_ZNK6icu_6724IntegralPartSubstitution16composeRuleValueEdd:
.LFB2916:
	.cfi_startproc
	endbr64
	addsd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE2916:
	.size	_ZNK6icu_6724IntegralPartSubstitution16composeRuleValueEdd, .-_ZNK6icu_6724IntegralPartSubstitution16composeRuleValueEdd
	.section	.text._ZNK6icu_6724IntegralPartSubstitution14calcUpperBoundEd,"axG",@progbits,_ZNK6icu_6724IntegralPartSubstitution14calcUpperBoundEd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6724IntegralPartSubstitution14calcUpperBoundEd
	.type	_ZNK6icu_6724IntegralPartSubstitution14calcUpperBoundEd, @function
_ZNK6icu_6724IntegralPartSubstitution14calcUpperBoundEd:
.LFB2917:
	.cfi_startproc
	endbr64
	movsd	.LC0(%rip), %xmm0
	ret
	.cfi_endproc
.LFE2917:
	.size	_ZNK6icu_6724IntegralPartSubstitution14calcUpperBoundEd, .-_ZNK6icu_6724IntegralPartSubstitution14calcUpperBoundEd
	.section	.text._ZNK6icu_6724IntegralPartSubstitution9tokenCharEv,"axG",@progbits,_ZNK6icu_6724IntegralPartSubstitution9tokenCharEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6724IntegralPartSubstitution9tokenCharEv
	.type	_ZNK6icu_6724IntegralPartSubstitution9tokenCharEv, @function
_ZNK6icu_6724IntegralPartSubstitution9tokenCharEv:
.LFB2918:
	.cfi_startproc
	endbr64
	movl	$60, %eax
	ret
	.cfi_endproc
.LFE2918:
	.size	_ZNK6icu_6724IntegralPartSubstitution9tokenCharEv, .-_ZNK6icu_6724IntegralPartSubstitution9tokenCharEv
	.section	.text._ZNK6icu_6726FractionalPartSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode,"axG",@progbits,_ZNK6icu_6726FractionalPartSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6726FractionalPartSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZNK6icu_6726FractionalPartSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode, @function
_ZNK6icu_6726FractionalPartSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode:
.LFB2923:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2923:
	.size	_ZNK6icu_6726FractionalPartSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode, .-_ZNK6icu_6726FractionalPartSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.section	.text._ZNK6icu_6726FractionalPartSubstitution15transformNumberEl,"axG",@progbits,_ZNK6icu_6726FractionalPartSubstitution15transformNumberEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6726FractionalPartSubstitution15transformNumberEl
	.type	_ZNK6icu_6726FractionalPartSubstitution15transformNumberEl, @function
_ZNK6icu_6726FractionalPartSubstitution15transformNumberEl:
.LFB2924:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2924:
	.size	_ZNK6icu_6726FractionalPartSubstitution15transformNumberEl, .-_ZNK6icu_6726FractionalPartSubstitution15transformNumberEl
	.section	.text._ZNK6icu_6726FractionalPartSubstitution16composeRuleValueEdd,"axG",@progbits,_ZNK6icu_6726FractionalPartSubstitution16composeRuleValueEdd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6726FractionalPartSubstitution16composeRuleValueEdd
	.type	_ZNK6icu_6726FractionalPartSubstitution16composeRuleValueEdd, @function
_ZNK6icu_6726FractionalPartSubstitution16composeRuleValueEdd:
.LFB2926:
	.cfi_startproc
	endbr64
	addsd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE2926:
	.size	_ZNK6icu_6726FractionalPartSubstitution16composeRuleValueEdd, .-_ZNK6icu_6726FractionalPartSubstitution16composeRuleValueEdd
	.section	.text._ZNK6icu_6726FractionalPartSubstitution14calcUpperBoundEd,"axG",@progbits,_ZNK6icu_6726FractionalPartSubstitution14calcUpperBoundEd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6726FractionalPartSubstitution14calcUpperBoundEd
	.type	_ZNK6icu_6726FractionalPartSubstitution14calcUpperBoundEd, @function
_ZNK6icu_6726FractionalPartSubstitution14calcUpperBoundEd:
.LFB2927:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE2927:
	.size	_ZNK6icu_6726FractionalPartSubstitution14calcUpperBoundEd, .-_ZNK6icu_6726FractionalPartSubstitution14calcUpperBoundEd
	.section	.text._ZNK6icu_6726FractionalPartSubstitution9tokenCharEv,"axG",@progbits,_ZNK6icu_6726FractionalPartSubstitution9tokenCharEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6726FractionalPartSubstitution9tokenCharEv
	.type	_ZNK6icu_6726FractionalPartSubstitution9tokenCharEv, @function
_ZNK6icu_6726FractionalPartSubstitution9tokenCharEv:
.LFB2928:
	.cfi_startproc
	endbr64
	movl	$62, %eax
	ret
	.cfi_endproc
.LFE2928:
	.size	_ZNK6icu_6726FractionalPartSubstitution9tokenCharEv, .-_ZNK6icu_6726FractionalPartSubstitution9tokenCharEv
	.section	.text._ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEl,"axG",@progbits,_ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEl
	.type	_ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEl, @function
_ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEl:
.LFB2936:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	cqto
	xorq	%rdx, %rax
	subq	%rdx, %rax
	ret
	.cfi_endproc
.LFE2936:
	.size	_ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEl, .-_ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEl
	.section	.text._ZNK6icu_6725AbsoluteValueSubstitution16composeRuleValueEdd,"axG",@progbits,_ZNK6icu_6725AbsoluteValueSubstitution16composeRuleValueEdd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6725AbsoluteValueSubstitution16composeRuleValueEdd
	.type	_ZNK6icu_6725AbsoluteValueSubstitution16composeRuleValueEdd, @function
_ZNK6icu_6725AbsoluteValueSubstitution16composeRuleValueEdd:
.LFB2938:
	.cfi_startproc
	endbr64
	xorpd	.LC2(%rip), %xmm0
	ret
	.cfi_endproc
.LFE2938:
	.size	_ZNK6icu_6725AbsoluteValueSubstitution16composeRuleValueEdd, .-_ZNK6icu_6725AbsoluteValueSubstitution16composeRuleValueEdd
	.section	.text._ZNK6icu_6725AbsoluteValueSubstitution14calcUpperBoundEd,"axG",@progbits,_ZNK6icu_6725AbsoluteValueSubstitution14calcUpperBoundEd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6725AbsoluteValueSubstitution14calcUpperBoundEd
	.type	_ZNK6icu_6725AbsoluteValueSubstitution14calcUpperBoundEd, @function
_ZNK6icu_6725AbsoluteValueSubstitution14calcUpperBoundEd:
.LFB2939:
	.cfi_startproc
	endbr64
	movsd	.LC0(%rip), %xmm0
	ret
	.cfi_endproc
.LFE2939:
	.size	_ZNK6icu_6725AbsoluteValueSubstitution14calcUpperBoundEd, .-_ZNK6icu_6725AbsoluteValueSubstitution14calcUpperBoundEd
	.section	.text._ZNK6icu_6725AbsoluteValueSubstitution9tokenCharEv,"axG",@progbits,_ZNK6icu_6725AbsoluteValueSubstitution9tokenCharEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6725AbsoluteValueSubstitution9tokenCharEv
	.type	_ZNK6icu_6725AbsoluteValueSubstitution9tokenCharEv, @function
_ZNK6icu_6725AbsoluteValueSubstitution9tokenCharEv:
.LFB2940:
	.cfi_startproc
	endbr64
	movl	$62, %eax
	ret
	.cfi_endproc
.LFE2940:
	.size	_ZNK6icu_6725AbsoluteValueSubstitution9tokenCharEv, .-_ZNK6icu_6725AbsoluteValueSubstitution9tokenCharEv
	.section	.text._ZNK6icu_6721NumeratorSubstitution15transformNumberEl,"axG",@progbits,_ZNK6icu_6721NumeratorSubstitution15transformNumberEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721NumeratorSubstitution15transformNumberEl
	.type	_ZNK6icu_6721NumeratorSubstitution15transformNumberEl, @function
_ZNK6icu_6721NumeratorSubstitution15transformNumberEl:
.LFB2949:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	imulq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2949:
	.size	_ZNK6icu_6721NumeratorSubstitution15transformNumberEl, .-_ZNK6icu_6721NumeratorSubstitution15transformNumberEl
	.section	.text._ZNK6icu_6721NumeratorSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode,"axG",@progbits,_ZNK6icu_6721NumeratorSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721NumeratorSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZNK6icu_6721NumeratorSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode, @function
_ZNK6icu_6721NumeratorSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode:
.LFB2951:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2951:
	.size	_ZNK6icu_6721NumeratorSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode, .-_ZNK6icu_6721NumeratorSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.section	.text._ZNK6icu_6721NumeratorSubstitution16composeRuleValueEdd,"axG",@progbits,_ZNK6icu_6721NumeratorSubstitution16composeRuleValueEdd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721NumeratorSubstitution16composeRuleValueEdd
	.type	_ZNK6icu_6721NumeratorSubstitution16composeRuleValueEdd, @function
_ZNK6icu_6721NumeratorSubstitution16composeRuleValueEdd:
.LFB2952:
	.cfi_startproc
	endbr64
	divsd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE2952:
	.size	_ZNK6icu_6721NumeratorSubstitution16composeRuleValueEdd, .-_ZNK6icu_6721NumeratorSubstitution16composeRuleValueEdd
	.section	.text._ZNK6icu_6721NumeratorSubstitution14calcUpperBoundEd,"axG",@progbits,_ZNK6icu_6721NumeratorSubstitution14calcUpperBoundEd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721NumeratorSubstitution14calcUpperBoundEd
	.type	_ZNK6icu_6721NumeratorSubstitution14calcUpperBoundEd, @function
_ZNK6icu_6721NumeratorSubstitution14calcUpperBoundEd:
.LFB2953:
	.cfi_startproc
	endbr64
	movsd	32(%rdi), %xmm0
	ret
	.cfi_endproc
.LFE2953:
	.size	_ZNK6icu_6721NumeratorSubstitution14calcUpperBoundEd, .-_ZNK6icu_6721NumeratorSubstitution14calcUpperBoundEd
	.section	.text._ZNK6icu_6721NumeratorSubstitution9tokenCharEv,"axG",@progbits,_ZNK6icu_6721NumeratorSubstitution9tokenCharEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721NumeratorSubstitution9tokenCharEv
	.type	_ZNK6icu_6721NumeratorSubstitution9tokenCharEv, @function
_ZNK6icu_6721NumeratorSubstitution9tokenCharEv:
.LFB2954:
	.cfi_startproc
	endbr64
	movl	$60, %eax
	ret
	.cfi_endproc
.LFE2954:
	.size	_ZNK6icu_6721NumeratorSubstitution9tokenCharEv, .-_ZNK6icu_6721NumeratorSubstitution9tokenCharEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714NFSubstitution10setDivisorEisR10UErrorCode
	.type	_ZN6icu_6714NFSubstitution10setDivisorEisR10UErrorCode, @function
_ZN6icu_6714NFSubstitution10setDivisorEisR10UErrorCode:
.LFB2967:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2967:
	.size	_ZN6icu_6714NFSubstitution10setDivisorEisR10UErrorCode, .-_ZN6icu_6714NFSubstitution10setDivisorEisR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714NFSubstitution17getDynamicClassIDEv
	.type	_ZNK6icu_6714NFSubstitution17getDynamicClassIDEv, @function
_ZNK6icu_6714NFSubstitution17getDynamicClassIDEv:
.LFB2970:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714NFSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2970:
	.size	_ZNK6icu_6714NFSubstitution17getDynamicClassIDEv, .-_ZNK6icu_6714NFSubstitution17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714NFSubstitution21isModulusSubstitutionEv
	.type	_ZNK6icu_6714NFSubstitution21isModulusSubstitutionEv, @function
_ZNK6icu_6714NFSubstitution21isModulusSubstitutionEv:
.LFB2976:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2976:
	.size	_ZNK6icu_6714NFSubstitution21isModulusSubstitutionEv, .-_ZNK6icu_6714NFSubstitution21isModulusSubstitutionEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721SameValueSubstitution17getDynamicClassIDEv
	.type	_ZNK6icu_6721SameValueSubstitution17getDynamicClassIDEv, @function
_ZNK6icu_6721SameValueSubstitution17getDynamicClassIDEv:
.LFB2981:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6721SameValueSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2981:
	.size	_ZNK6icu_6721SameValueSubstitution17getDynamicClassIDEv, .-_ZNK6icu_6721SameValueSubstitution17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722MultiplierSubstitution17getDynamicClassIDEv
	.type	_ZNK6icu_6722MultiplierSubstitution17getDynamicClassIDEv, @function
_ZNK6icu_6722MultiplierSubstitution17getDynamicClassIDEv:
.LFB2983:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6722MultiplierSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2983:
	.size	_ZNK6icu_6722MultiplierSubstitution17getDynamicClassIDEv, .-_ZNK6icu_6722MultiplierSubstitution17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719ModulusSubstitution17getDynamicClassIDEv
	.type	_ZNK6icu_6719ModulusSubstitution17getDynamicClassIDEv, @function
_ZNK6icu_6719ModulusSubstitution17getDynamicClassIDEv:
.LFB2989:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6719ModulusSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2989:
	.size	_ZNK6icu_6719ModulusSubstitution17getDynamicClassIDEv, .-_ZNK6icu_6719ModulusSubstitution17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724IntegralPartSubstitution17getDynamicClassIDEv
	.type	_ZNK6icu_6724IntegralPartSubstitution17getDynamicClassIDEv, @function
_ZNK6icu_6724IntegralPartSubstitution17getDynamicClassIDEv:
.LFB2996:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6724IntegralPartSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2996:
	.size	_ZNK6icu_6724IntegralPartSubstitution17getDynamicClassIDEv, .-_ZNK6icu_6724IntegralPartSubstitution17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6726FractionalPartSubstitution17getDynamicClassIDEv
	.type	_ZNK6icu_6726FractionalPartSubstitution17getDynamicClassIDEv, @function
_ZNK6icu_6726FractionalPartSubstitution17getDynamicClassIDEv:
.LFB3004:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6726FractionalPartSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3004:
	.size	_ZNK6icu_6726FractionalPartSubstitution17getDynamicClassIDEv, .-_ZNK6icu_6726FractionalPartSubstitution17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725AbsoluteValueSubstitution17getDynamicClassIDEv
	.type	_ZNK6icu_6725AbsoluteValueSubstitution17getDynamicClassIDEv, @function
_ZNK6icu_6725AbsoluteValueSubstitution17getDynamicClassIDEv:
.LFB3006:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6725AbsoluteValueSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3006:
	.size	_ZNK6icu_6725AbsoluteValueSubstitution17getDynamicClassIDEv, .-_ZNK6icu_6725AbsoluteValueSubstitution17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721NumeratorSubstitution17getDynamicClassIDEv
	.type	_ZNK6icu_6721NumeratorSubstitution17getDynamicClassIDEv, @function
_ZNK6icu_6721NumeratorSubstitution17getDynamicClassIDEv:
.LFB3011:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6721NumeratorSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3011:
	.size	_ZNK6icu_6721NumeratorSubstitution17getDynamicClassIDEv, .-_ZNK6icu_6721NumeratorSubstitution17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714NFSubstitutioneqERKS0_
	.type	_ZNK6icu_6714NFSubstitutioneqERKS0_, @function
_ZNK6icu_6714NFSubstitutioneqERKS0_:
.LFB2971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L44
	xorl	%r12d, %r12d
	cmpb	$42, (%rdi)
	je	.L43
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L43
.L44:
	movl	8(%rbx), %eax
	xorl	%r12d, %r12d
	cmpl	%eax, 8(%r13)
	je	.L53
.L43:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	cmpq	$0, 16(%r13)
	sete	%dl
	cmpq	$0, 16(%rbx)
	sete	%al
	cmpb	%al, %dl
	jne	.L43
	movq	24(%rbx), %rsi
	movq	24(%r13), %rdi
	testq	%rsi, %rsi
	sete	%r12b
	testq	%rdi, %rdi
	je	.L43
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	setne	%r12b
	jmp	.L43
	.cfi_endproc
.LFE2971:
	.size	_ZNK6icu_6714NFSubstitutioneqERKS0_, .-_ZNK6icu_6714NFSubstitutioneqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714NFSubstitution8toStringERNS_13UnicodeStringE
	.type	_ZNK6icu_6714NFSubstitution8toStringERNS_13UnicodeStringE, @function
_ZNK6icu_6714NFSubstitution8toStringERNS_13UnicodeStringE:
.LFB2972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-114(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$96, %rsp
	movzwl	8(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 8(%rsi)
	movq	(%rdi), %rax
	call	*104(%rax)
	movq	%r14, %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%ax, -114(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	16(%r12), %rsi
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	testq	%rsi, %rsi
	je	.L56
	leaq	-112(%rbp), %r15
	addq	$8, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
.L57:
	movswl	-104(%rbp), %ecx
	testw	%cx, %cx
	js	.L65
.L58:
	sarl	$5, %ecx
.L60:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*104(%rax)
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movw	%ax, -114(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L66
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movl	-100(%rbp), %ecx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L56:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L63
	movq	(%rdi), %rax
	leaq	-112(%rbp), %r15
	movq	%r15, %rsi
	call	*544(%rax)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$2, %ecx
	leaq	-112(%rbp), %r15
	jmp	.L58
.L66:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2972:
	.size	_ZNK6icu_6714NFSubstitution8toStringERNS_13UnicodeStringE, .-_ZNK6icu_6714NFSubstitution8toStringERNS_13UnicodeStringE
	.section	.text._ZNK6icu_6726FractionalPartSubstitution15transformNumberEd,"axG",@progbits,_ZNK6icu_6726FractionalPartSubstitution15transformNumberEd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6726FractionalPartSubstitution15transformNumberEd
	.type	_ZNK6icu_6726FractionalPartSubstitution15transformNumberEd, @function
_ZNK6icu_6726FractionalPartSubstitution15transformNumberEd:
.LFB2925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movsd	%xmm0, -8(%rbp)
	call	uprv_floor_67@PLT
	movsd	-8(%rbp), %xmm1
	leave
	.cfi_def_cfa 7, 8
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE2925:
	.size	_ZNK6icu_6726FractionalPartSubstitution15transformNumberEd, .-_ZNK6icu_6726FractionalPartSubstitution15transformNumberEd
	.section	.text._ZNK6icu_6724IntegralPartSubstitution15transformNumberEd,"axG",@progbits,_ZNK6icu_6724IntegralPartSubstitution15transformNumberEd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6724IntegralPartSubstitution15transformNumberEd
	.type	_ZNK6icu_6724IntegralPartSubstitution15transformNumberEd, @function
_ZNK6icu_6724IntegralPartSubstitution15transformNumberEd:
.LFB2915:
	.cfi_startproc
	endbr64
	jmp	uprv_floor_67@PLT
	.cfi_endproc
.LFE2915:
	.size	_ZNK6icu_6724IntegralPartSubstitution15transformNumberEd, .-_ZNK6icu_6724IntegralPartSubstitution15transformNumberEd
	.section	.text._ZNK6icu_6722MultiplierSubstitution15transformNumberEd,"axG",@progbits,_ZNK6icu_6722MultiplierSubstitution15transformNumberEd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6722MultiplierSubstitution15transformNumberEd
	.type	_ZNK6icu_6722MultiplierSubstitution15transformNumberEd, @function
_ZNK6icu_6722MultiplierSubstitution15transformNumberEd:
.LFB2892:
	.cfi_startproc
	endbr64
	pxor	%xmm1, %xmm1
	cmpq	$0, 16(%rdi)
	cvtsi2sdq	32(%rdi), %xmm1
	divsd	%xmm1, %xmm0
	je	.L70
	jmp	uprv_floor_67@PLT
	.p2align 4,,10
	.p2align 3
.L70:
	ret
	.cfi_endproc
.LFE2892:
	.size	_ZNK6icu_6722MultiplierSubstitution15transformNumberEd, .-_ZNK6icu_6722MultiplierSubstitution15transformNumberEd
	.section	.text._ZNK6icu_6721NumeratorSubstitution15transformNumberEd,"axG",@progbits,_ZNK6icu_6721NumeratorSubstitution15transformNumberEd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721NumeratorSubstitution15transformNumberEd
	.type	_ZNK6icu_6721NumeratorSubstitution15transformNumberEd, @function
_ZNK6icu_6721NumeratorSubstitution15transformNumberEd:
.LFB2950:
	.cfi_startproc
	endbr64
	mulsd	32(%rdi), %xmm0
	jmp	uprv_round_67@PLT
	.cfi_endproc
.LFE2950:
	.size	_ZNK6icu_6721NumeratorSubstitution15transformNumberEd, .-_ZNK6icu_6721NumeratorSubstitution15transformNumberEd
	.section	.text._ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEd,"axG",@progbits,_ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEd
	.type	_ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEd, @function
_ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEd:
.LFB2937:
	.cfi_startproc
	endbr64
	jmp	uprv_fabs_67@PLT
	.cfi_endproc
.LFE2937:
	.size	_ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEd, .-_ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEd
	.section	.text._ZNK6icu_6719ModulusSubstitution15transformNumberEd,"axG",@progbits,_ZNK6icu_6719ModulusSubstitution15transformNumberEd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719ModulusSubstitution15transformNumberEd
	.type	_ZNK6icu_6719ModulusSubstitution15transformNumberEd, @function
_ZNK6icu_6719ModulusSubstitution15transformNumberEd:
.LFB2902:
	.cfi_startproc
	endbr64
	pxor	%xmm1, %xmm1
	cvtsi2sdq	32(%rdi), %xmm1
	jmp	uprv_fmod_67@PLT
	.cfi_endproc
.LFE2902:
	.size	_ZNK6icu_6719ModulusSubstitution15transformNumberEd, .-_ZNK6icu_6719ModulusSubstitution15transformNumberEd
	.section	.text._ZNK6icu_6719ModulusSubstitution16composeRuleValueEdd,"axG",@progbits,_ZNK6icu_6719ModulusSubstitution16composeRuleValueEdd,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719ModulusSubstitution16composeRuleValueEdd
	.type	_ZNK6icu_6719ModulusSubstitution16composeRuleValueEdd, @function
_ZNK6icu_6719ModulusSubstitution16composeRuleValueEdd:
.LFB2903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	cvtsi2sdq	32(%rdi), %xmm1
	movsd	%xmm0, -8(%rbp)
	movapd	%xmm2, %xmm0
	movsd	%xmm2, -16(%rbp)
	call	uprv_fmod_67@PLT
	movsd	-16(%rbp), %xmm2
	subsd	%xmm0, %xmm2
	movsd	-8(%rbp), %xmm0
	leave
	.cfi_def_cfa 7, 8
	addsd	%xmm2, %xmm0
	ret
	.cfi_endproc
.LFE2903:
	.size	_ZNK6icu_6719ModulusSubstitution16composeRuleValueEdd, .-_ZNK6icu_6719ModulusSubstitution16composeRuleValueEdd
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714NFSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZNK6icu_6714NFSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode, @function
_ZNK6icu_6714NFSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode:
.LFB2974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*72(%rax)
	movsd	%xmm0, -136(%rbp)
	call	uprv_isInfinite_67@PLT
	movsd	-136(%rbp), %xmm1
	testb	%al, %al
	jne	.L97
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -136(%rbp)
	call	uprv_floor_67@PLT
	movsd	-136(%rbp), %xmm1
	movq	16(%rbx), %rdi
	ucomisd	%xmm1, %xmm0
	jp	.L80
	jne	.L80
	testq	%rdi, %rdi
	movq	%rdi, -136(%rbp)
	je	.L82
	movapd	%xmm1, %xmm0
	addl	8(%rbx), %r12d
	call	_ZN6icu_6717util64_fromDoubleEd@PLT
	movq	%r15, %r9
	movl	%r14d, %r8d
	movl	%r12d, %ecx
	movq	-136(%rbp), %rdi
	movq	%rax, %rsi
	movq	%r13, %rdx
	call	_ZNK6icu_679NFRuleSet6formatElRNS_13UnicodeStringEiiR10UErrorCode@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L80:
	testq	%rdi, %rdi
	je	.L82
	movl	8(%rbx), %edx
	movq	%r15, %r8
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movapd	%xmm1, %xmm0
	addl	%r12d, %edx
	call	_ZNK6icu_679NFRuleSet6formatEdRNS_13UnicodeStringEiiR10UErrorCode@PLT
.L77:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L77
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r14
	movapd	%xmm1, %xmm0
	movq	%rax, -128(%rbp)
	movq	%r14, %rsi
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	call	_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringE@PLT
	movl	8(%rbx), %esi
	movswl	-120(%rbp), %r9d
	addl	%r12d, %esi
	testw	%r9w, %r9w
	js	.L83
	sarl	$5, %r9d
.L84:
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L97:
	movq	16(%rbx), %rdi
	movsd	%xmm1, -144(%rbp)
	movq	%rdi, -136(%rbp)
	call	uprv_getInfinity_67@PLT
	movq	-136(%rbp), %rdi
	call	_ZNK6icu_679NFRuleSet14findDoubleRuleEd@PLT
	movl	8(%rbx), %edx
	movq	%r15, %r8
	movl	%r14d, %ecx
	movsd	-144(%rbp), %xmm1
	movq	%rax, %rdi
	movq	%r13, %rsi
	addl	%r12d, %edx
	movapd	%xmm1, %xmm0
	call	_ZNK6icu_676NFRule8doFormatEdRNS_13UnicodeStringEiiR10UErrorCode@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L83:
	movl	-116(%rbp), %r9d
	jmp	.L84
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2974:
	.size	_ZNK6icu_6714NFSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode, .-_ZNK6icu_6714NFSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714NFSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.type	_ZNK6icu_6714NFSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE, @function
_ZNK6icu_6714NFSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE:
.LFB2975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movl	%ecx, -68(%rbp)
	movsd	%xmm0, -80(%rbp)
	movapd	%xmm1, %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L100
	movq	%r13, %r8
	movl	%r15d, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZNK6icu_679NFRuleSet5parseERKNS_13UnicodeStringERNS_13ParsePositionEdjRNS_11FormattableE@PLT
	cmpb	$0, -68(%rbp)
	movl	8(%rbx), %eax
	je	.L102
	movq	16(%r12), %rdx
	cmpb	$0, 160(%rdx)
	jne	.L102
	testl	%eax, %eax
	je	.L103
	.p2align 4,,10
	.p2align 3
.L109:
	leaq	-60(%rbp), %rsi
	movq	%r13, %rdi
	movl	$0, -60(%rbp)
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	movq	(%r12), %rax
	movsd	-80(%rbp), %xmm1
	movq	%r12, %rdi
	call	*88(%rax)
	movq	%r13, %rdi
	call	_ZN6icu_6711Formattable9setDoubleEd@PLT
	movl	$1, %eax
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L100:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L115
	movq	(%rdi), %rax
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	*160(%rax)
.L115:
	movl	8(%rbx), %eax
.L102:
	testl	%eax, %eax
	jne	.L109
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6711Formattable7setLongEi@PLT
	xorl	%eax, %eax
.L99:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L116
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	leaq	-60(%rbp), %rdi
	movl	$0, -60(%rbp)
	call	_ZN6icu_6712NumberFormat14createInstanceER10UErrorCode@PLT
	movq	%rax, %r15
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L117
	testq	%r15, %r15
	je	.L115
.L106:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L117:
	movq	(%r15), %rax
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*160(%rax)
	jmp	.L106
.L116:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2975:
	.size	_ZNK6icu_6714NFSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE, .-_ZNK6icu_6714NFSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.section	.text._ZN6icu_6719ModulusSubstitution10setDivisorEisR10UErrorCode,"axG",@progbits,_ZN6icu_6719ModulusSubstitution10setDivisorEisR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6719ModulusSubstitution10setDivisorEisR10UErrorCode
	.type	_ZN6icu_6719ModulusSubstitution10setDivisorEisR10UErrorCode, @function
_ZN6icu_6719ModulusSubstitution10setDivisorEisR10UErrorCode:
.LFB2900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	%esi, %edi
	movzwl	%dx, %esi
	call	_ZN6icu_6710util64_powEjt@PLT
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.L118
	movl	$9, (%r12)
.L118:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2900:
	.size	_ZN6icu_6719ModulusSubstitution10setDivisorEisR10UErrorCode, .-_ZN6icu_6719ModulusSubstitution10setDivisorEisR10UErrorCode
	.section	.text._ZN6icu_6722MultiplierSubstitution10setDivisorEisR10UErrorCode,"axG",@progbits,_ZN6icu_6722MultiplierSubstitution10setDivisorEisR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6722MultiplierSubstitution10setDivisorEisR10UErrorCode
	.type	_ZN6icu_6722MultiplierSubstitution10setDivisorEisR10UErrorCode, @function
_ZN6icu_6722MultiplierSubstitution10setDivisorEisR10UErrorCode:
.LFB2890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	%esi, %edi
	movzwl	%dx, %esi
	call	_ZN6icu_6710util64_powEjt@PLT
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.L121
	movl	$9, (%r12)
.L121:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2890:
	.size	_ZN6icu_6722MultiplierSubstitution10setDivisorEisR10UErrorCode, .-_ZN6icu_6722MultiplierSubstitution10setDivisorEisR10UErrorCode
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719ModulusSubstitution8toStringERNS_13UnicodeStringE
	.type	_ZNK6icu_6719ModulusSubstitution8toStringERNS_13UnicodeStringE, @function
_ZNK6icu_6719ModulusSubstitution8toStringERNS_13UnicodeStringE:
.LFB2994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 40(%rdi)
	je	.L125
	movzwl	8(%rsi), %edx
	leaq	_ZNK6icu_6719ModulusSubstitution9tokenCharEv(%rip), %rbx
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 8(%rsi)
	movq	(%rdi), %rax
	movq	104(%rax), %rdx
	movl	$62, %eax
	cmpq	%rbx, %rdx
	jne	.L137
.L127:
	leaq	-42(%rbp), %r14
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	0(%r13), %rax
	movq	104(%rax), %rdx
	movl	$62, %eax
	cmpq	%rbx, %rdx
	jne	.L138
.L128:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	0(%r13), %rax
	movq	104(%rax), %rdx
	movl	$62, %eax
	cmpq	%rbx, %rdx
	jne	.L139
.L129:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L124:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L140
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	call	_ZNK6icu_6714NFSubstitution8toStringERNS_13UnicodeStringE
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L137:
	call	*%rdx
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L129
.L140:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2994:
	.size	_ZNK6icu_6719ModulusSubstitution8toStringERNS_13UnicodeStringE, .-_ZNK6icu_6719ModulusSubstitution8toStringERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719ModulusSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZNK6icu_6719ModulusSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode, @function
_ZNK6icu_6719ModulusSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode:
.LFB2992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	cmpq	$0, 40(%rdi)
	je	.L146
	movq	(%rdi), %rax
	leaq	_ZNK6icu_6719ModulusSubstitution15transformNumberEd(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L143
	pxor	%xmm1, %xmm1
	cvtsi2sdq	32(%rdi), %xmm1
	call	uprv_fmod_67@PLT
.L144:
	movl	8(%r12), %edx
	movq	40(%r12), %rdi
	movq	%rbx, %r8
	movl	%r14d, %ecx
	addq	$8, %rsp
	movq	%r15, %rsi
	popq	%rbx
	addl	%r13d, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676NFRule8doFormatEdRNS_13UnicodeStringEiiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6714NFSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	call	*%rax
	jmp	.L144
	.cfi_endproc
.LFE2992:
	.size	_ZNK6icu_6719ModulusSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode, .-_ZNK6icu_6719ModulusSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721NumeratorSubstitutionD2Ev
	.type	_ZN6icu_6721NumeratorSubstitutionD2Ev, @function
_ZN6icu_6721NumeratorSubstitutionD2Ev:
.LFB2956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L148
	movq	(%rdi), %rax
	call	*8(%rax)
.L148:
	movq	$0, 24(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2956:
	.size	_ZN6icu_6721NumeratorSubstitutionD2Ev, .-_ZN6icu_6721NumeratorSubstitutionD2Ev
	.globl	_ZN6icu_6721NumeratorSubstitutionD1Ev
	.set	_ZN6icu_6721NumeratorSubstitutionD1Ev,_ZN6icu_6721NumeratorSubstitutionD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6726FractionalPartSubstitutionD2Ev
	.type	_ZN6icu_6726FractionalPartSubstitutionD2Ev, @function
_ZN6icu_6726FractionalPartSubstitutionD2Ev:
.LFB2930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L154
	movq	(%rdi), %rax
	call	*8(%rax)
.L154:
	movq	$0, 24(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2930:
	.size	_ZN6icu_6726FractionalPartSubstitutionD2Ev, .-_ZN6icu_6726FractionalPartSubstitutionD2Ev
	.globl	_ZN6icu_6726FractionalPartSubstitutionD1Ev
	.set	_ZN6icu_6726FractionalPartSubstitutionD1Ev,_ZN6icu_6726FractionalPartSubstitutionD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725AbsoluteValueSubstitutionD2Ev
	.type	_ZN6icu_6725AbsoluteValueSubstitutionD2Ev, @function
_ZN6icu_6725AbsoluteValueSubstitutionD2Ev:
.LFB2942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L160
	movq	(%rdi), %rax
	call	*8(%rax)
.L160:
	movq	$0, 24(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2942:
	.size	_ZN6icu_6725AbsoluteValueSubstitutionD2Ev, .-_ZN6icu_6725AbsoluteValueSubstitutionD2Ev
	.globl	_ZN6icu_6725AbsoluteValueSubstitutionD1Ev
	.set	_ZN6icu_6725AbsoluteValueSubstitutionD1Ev,_ZN6icu_6725AbsoluteValueSubstitutionD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721SameValueSubstitutionD2Ev
	.type	_ZN6icu_6721SameValueSubstitutionD2Ev, @function
_ZN6icu_6721SameValueSubstitutionD2Ev:
.LFB2884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L166
	movq	(%rdi), %rax
	call	*8(%rax)
.L166:
	movq	$0, 24(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2884:
	.size	_ZN6icu_6721SameValueSubstitutionD2Ev, .-_ZN6icu_6721SameValueSubstitutionD2Ev
	.globl	_ZN6icu_6721SameValueSubstitutionD1Ev
	.set	_ZN6icu_6721SameValueSubstitutionD1Ev,_ZN6icu_6721SameValueSubstitutionD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722MultiplierSubstitutionD2Ev
	.type	_ZN6icu_6722MultiplierSubstitutionD2Ev, @function
_ZN6icu_6722MultiplierSubstitutionD2Ev:
.LFB2897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L172
	movq	(%rdi), %rax
	call	*8(%rax)
.L172:
	movq	$0, 24(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2897:
	.size	_ZN6icu_6722MultiplierSubstitutionD2Ev, .-_ZN6icu_6722MultiplierSubstitutionD2Ev
	.globl	_ZN6icu_6722MultiplierSubstitutionD1Ev
	.set	_ZN6icu_6722MultiplierSubstitutionD1Ev,_ZN6icu_6722MultiplierSubstitutionD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719ModulusSubstitutionD2Ev
	.type	_ZN6icu_6719ModulusSubstitutionD2Ev, @function
_ZN6icu_6719ModulusSubstitutionD2Ev:
.LFB2908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L178
	movq	(%rdi), %rax
	call	*8(%rax)
.L178:
	movq	$0, 24(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2908:
	.size	_ZN6icu_6719ModulusSubstitutionD2Ev, .-_ZN6icu_6719ModulusSubstitutionD2Ev
	.globl	_ZN6icu_6719ModulusSubstitutionD1Ev
	.set	_ZN6icu_6719ModulusSubstitutionD1Ev,_ZN6icu_6719ModulusSubstitutionD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724IntegralPartSubstitutionD2Ev
	.type	_ZN6icu_6724IntegralPartSubstitutionD2Ev, @function
_ZN6icu_6724IntegralPartSubstitutionD2Ev:
.LFB2920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L184
	movq	(%rdi), %rax
	call	*8(%rax)
.L184:
	movq	$0, 24(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2920:
	.size	_ZN6icu_6724IntegralPartSubstitutionD2Ev, .-_ZN6icu_6724IntegralPartSubstitutionD2Ev
	.globl	_ZN6icu_6724IntegralPartSubstitutionD1Ev
	.set	_ZN6icu_6724IntegralPartSubstitutionD1Ev,_ZN6icu_6724IntegralPartSubstitutionD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722MultiplierSubstitutionD0Ev
	.type	_ZN6icu_6722MultiplierSubstitutionD0Ev, @function
_ZN6icu_6722MultiplierSubstitutionD0Ev:
.LFB2899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L190
	movq	(%rdi), %rax
	call	*8(%rax)
.L190:
	movq	$0, 24(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2899:
	.size	_ZN6icu_6722MultiplierSubstitutionD0Ev, .-_ZN6icu_6722MultiplierSubstitutionD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719ModulusSubstitutionD0Ev
	.type	_ZN6icu_6719ModulusSubstitutionD0Ev, @function
_ZN6icu_6719ModulusSubstitutionD0Ev:
.LFB2910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L196
	movq	(%rdi), %rax
	call	*8(%rax)
.L196:
	movq	$0, 24(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2910:
	.size	_ZN6icu_6719ModulusSubstitutionD0Ev, .-_ZN6icu_6719ModulusSubstitutionD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6726FractionalPartSubstitutionD0Ev
	.type	_ZN6icu_6726FractionalPartSubstitutionD0Ev, @function
_ZN6icu_6726FractionalPartSubstitutionD0Ev:
.LFB2932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L202
	movq	(%rdi), %rax
	call	*8(%rax)
.L202:
	movq	$0, 24(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2932:
	.size	_ZN6icu_6726FractionalPartSubstitutionD0Ev, .-_ZN6icu_6726FractionalPartSubstitutionD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721SameValueSubstitutionD0Ev
	.type	_ZN6icu_6721SameValueSubstitutionD0Ev, @function
_ZN6icu_6721SameValueSubstitutionD0Ev:
.LFB2886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L208
	movq	(%rdi), %rax
	call	*8(%rax)
.L208:
	movq	$0, 24(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2886:
	.size	_ZN6icu_6721SameValueSubstitutionD0Ev, .-_ZN6icu_6721SameValueSubstitutionD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721NumeratorSubstitutionD0Ev
	.type	_ZN6icu_6721NumeratorSubstitutionD0Ev, @function
_ZN6icu_6721NumeratorSubstitutionD0Ev:
.LFB2958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L214
	movq	(%rdi), %rax
	call	*8(%rax)
.L214:
	movq	$0, 24(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2958:
	.size	_ZN6icu_6721NumeratorSubstitutionD0Ev, .-_ZN6icu_6721NumeratorSubstitutionD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724IntegralPartSubstitutionD0Ev
	.type	_ZN6icu_6724IntegralPartSubstitutionD0Ev, @function
_ZN6icu_6724IntegralPartSubstitutionD0Ev:
.LFB2922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L220
	movq	(%rdi), %rax
	call	*8(%rax)
.L220:
	movq	$0, 24(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2922:
	.size	_ZN6icu_6724IntegralPartSubstitutionD0Ev, .-_ZN6icu_6724IntegralPartSubstitutionD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725AbsoluteValueSubstitutionD0Ev
	.type	_ZN6icu_6725AbsoluteValueSubstitutionD0Ev, @function
_ZN6icu_6725AbsoluteValueSubstitutionD0Ev:
.LFB2944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L226
	movq	(%rdi), %rax
	call	*8(%rax)
.L226:
	movq	$0, 24(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2944:
	.size	_ZN6icu_6725AbsoluteValueSubstitutionD0Ev, .-_ZN6icu_6725AbsoluteValueSubstitutionD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6726FractionalPartSubstitutioneqERKNS_14NFSubstitutionE
	.type	_ZNK6icu_6726FractionalPartSubstitutioneqERKNS_14NFSubstitutionE, @function
_ZNK6icu_6726FractionalPartSubstitutioneqERKNS_14NFSubstitutionE:
.LFB3002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L232
	cmpb	$42, (%rdi)
	je	.L235
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L232
.L235:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movl	8(%r12), %eax
	cmpl	%eax, 8(%rbx)
	jne	.L235
	cmpq	$0, 16(%rbx)
	sete	%dl
	cmpq	$0, 16(%r12)
	sete	%al
	cmpb	%al, %dl
	jne	.L235
	movq	24(%rbx), %rdi
	movq	24(%r12), %rsi
	testq	%rdi, %rdi
	je	.L242
	movq	(%rdi), %rax
	call	*24(%rax)
	movsbl	%al, %eax
.L237:
	testl	%eax, %eax
	je	.L235
	movzbl	32(%rbx), %eax
	cmpb	%al, 32(%r12)
	popq	%rbx
	sete	%al
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	xorl	%eax, %eax
	testq	%rsi, %rsi
	sete	%al
	jmp	.L237
	.cfi_endproc
.LFE3002:
	.size	_ZNK6icu_6726FractionalPartSubstitutioneqERKNS_14NFSubstitutionE, .-_ZNK6icu_6726FractionalPartSubstitutioneqERKNS_14NFSubstitutionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721NumeratorSubstitutioneqERKNS_14NFSubstitutionE
	.type	_ZNK6icu_6721NumeratorSubstitutioneqERKNS_14NFSubstitutionE, @function
_ZNK6icu_6721NumeratorSubstitutioneqERKNS_14NFSubstitutionE:
.LFB3009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L244
	cmpb	$42, (%rdi)
	je	.L247
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L244
.L247:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movl	8(%rbx), %eax
	cmpl	%eax, 8(%r12)
	jne	.L247
	cmpq	$0, 16(%r12)
	sete	%dl
	cmpq	$0, 16(%rbx)
	sete	%al
	cmpb	%al, %dl
	jne	.L247
	movq	24(%r12), %rdi
	movq	24(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L254
	movq	(%rdi), %rax
	call	*24(%rax)
	movsbl	%al, %eax
.L249:
	testl	%eax, %eax
	je	.L247
	movsd	32(%r12), %xmm0
	ucomisd	32(%rbx), %xmm0
	movl	$0, %edx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setnp	%al
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	xorl	%eax, %eax
	testq	%rsi, %rsi
	sete	%al
	jmp	.L249
	.cfi_endproc
.LFE3009:
	.size	_ZNK6icu_6721NumeratorSubstitutioneqERKNS_14NFSubstitutionE, .-_ZNK6icu_6721NumeratorSubstitutioneqERKNS_14NFSubstitutionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722MultiplierSubstitutioneqERKNS_14NFSubstitutionE
	.type	_ZNK6icu_6722MultiplierSubstitutioneqERKNS_14NFSubstitutionE, @function
_ZNK6icu_6722MultiplierSubstitutioneqERKNS_14NFSubstitutionE:
.LFB2984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L256
	cmpb	$42, (%rdi)
	je	.L259
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L256
.L259:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movl	8(%rbx), %eax
	cmpl	%eax, 8(%r12)
	jne	.L259
	cmpq	$0, 16(%r12)
	sete	%dl
	cmpq	$0, 16(%rbx)
	sete	%al
	cmpb	%al, %dl
	jne	.L259
	movq	24(%r12), %rdi
	movq	24(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L266
	movq	(%rdi), %rax
	call	*24(%rax)
	movsbl	%al, %eax
.L261:
	testl	%eax, %eax
	je	.L259
	movq	32(%rbx), %rax
	cmpq	%rax, 32(%r12)
	popq	%rbx
	sete	%al
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	xorl	%eax, %eax
	testq	%rsi, %rsi
	sete	%al
	jmp	.L261
	.cfi_endproc
.LFE2984:
	.size	_ZNK6icu_6722MultiplierSubstitutioneqERKNS_14NFSubstitutionE, .-_ZNK6icu_6722MultiplierSubstitutioneqERKNS_14NFSubstitutionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719ModulusSubstitutioneqERKNS_14NFSubstitutionE
	.type	_ZNK6icu_6719ModulusSubstitutioneqERKNS_14NFSubstitutionE, @function
_ZNK6icu_6719ModulusSubstitutioneqERKNS_14NFSubstitutionE:
.LFB2990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L268
	cmpb	$42, (%rdi)
	je	.L271
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L268
.L271:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movl	8(%rbx), %eax
	cmpl	%eax, 8(%r12)
	jne	.L271
	cmpq	$0, 16(%r12)
	sete	%dl
	cmpq	$0, 16(%rbx)
	sete	%al
	cmpb	%al, %dl
	jne	.L271
	movq	24(%r12), %rdi
	movq	24(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L278
	movq	(%rdi), %rax
	call	*24(%rax)
	movsbl	%al, %eax
.L273:
	testl	%eax, %eax
	je	.L271
	movq	32(%rbx), %rax
	cmpq	%rax, 32(%r12)
	jne	.L271
	movq	40(%rbx), %rax
	cmpq	%rax, 40(%r12)
	popq	%rbx
	sete	%al
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	xorl	%eax, %eax
	testq	%rsi, %rsi
	sete	%al
	jmp	.L273
	.cfi_endproc
.LFE2990:
	.size	_ZNK6icu_6719ModulusSubstitutioneqERKNS_14NFSubstitutionE, .-_ZNK6icu_6719ModulusSubstitutioneqERKNS_14NFSubstitutionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719ModulusSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.type	_ZNK6icu_6719ModulusSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE, @function
_ZNK6icu_6719ModulusSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE:
.LFB2993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	40(%rdi), %rdi
	movsd	%xmm0, -72(%rbp)
	movapd	%xmm1, %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L300
	xorl	%ecx, %ecx
	call	_ZNK6icu_676NFRule7doParseERKNS_13UnicodeStringERNS_13ParsePositionEadjRNS_11FormattableE@PLT
	movl	8(%rbx), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L301
.L279:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L302
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	%r8d, -80(%rbp)
	call	*96(%rax)
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L281
	movl	-80(%rbp), %ecx
	movq	%r13, %r8
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZNK6icu_679NFRuleSet5parseERKNS_13UnicodeStringERNS_13ParsePositionEdjRNS_11FormattableE@PLT
	movl	8(%rbx), %eax
	testb	%r15b, %r15b
	je	.L283
	movq	16(%r12), %rdx
	cmpb	$0, 160(%rdx)
	jne	.L283
	testl	%eax, %eax
	je	.L284
	.p2align 4,,10
	.p2align 3
.L290:
	leaq	-60(%rbp), %rsi
	movq	%r13, %rdi
	movl	$0, -60(%rbp)
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	movq	(%r12), %rax
	movsd	-72(%rbp), %xmm1
	movq	%r12, %rdi
	call	*88(%rax)
	movq	%r13, %rdi
	call	_ZN6icu_6711Formattable9setDoubleEd@PLT
	movl	$1, %eax
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L284:
	leaq	-60(%rbp), %rdi
	movl	$0, -60(%rbp)
	call	_ZN6icu_6712NumberFormat14createInstanceER10UErrorCode@PLT
	movl	-60(%rbp), %ecx
	movq	%rax, %r15
	testl	%ecx, %ecx
	jle	.L303
	testq	%rax, %rax
	je	.L299
.L287:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L299:
	movl	8(%rbx), %eax
.L283:
	testl	%eax, %eax
	jne	.L290
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6711Formattable7setLongEi@PLT
	xorl	%eax, %eax
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L281:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L299
	movq	(%rdi), %rax
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	*160(%rax)
	movl	8(%rbx), %eax
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L301:
	leaq	-60(%rbp), %rsi
	movq	%r13, %rdi
	movl	$0, -60(%rbp)
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	movq	(%r12), %rax
	leaq	_ZNK6icu_6719ModulusSubstitution16composeRuleValueEdd(%rip), %rdx
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L291
	pxor	%xmm1, %xmm1
	movsd	%xmm0, -80(%rbp)
	movsd	-72(%rbp), %xmm0
	cvtsi2sdq	32(%r12), %xmm1
	call	uprv_fmod_67@PLT
	movsd	-72(%rbp), %xmm3
	movsd	-80(%rbp), %xmm2
	subsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm0
	addsd	%xmm2, %xmm0
.L292:
	movq	%r13, %rdi
	call	_ZN6icu_6711Formattable9setDoubleEd@PLT
	movl	$1, %eax
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L291:
	movsd	-72(%rbp), %xmm1
	movq	%r12, %rdi
	call	*%rax
	jmp	.L292
.L303:
	movq	(%rax), %rax
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*160(%rax)
	jmp	.L287
.L302:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2993:
	.size	_ZNK6icu_6719ModulusSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE, .-_ZNK6icu_6719ModulusSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714NFSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6714NFSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6714NFSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode:
.LFB2961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movl	%esi, 8(%rdi)
	movq	%rcx, %rsi
	movq	%rax, (%rdi)
	movups	%xmm0, 16(%rdi)
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	js	.L305
	movswl	%dx, %eax
	sarl	$5, %eax
.L306:
	cmpl	$1, %eax
	jle	.L307
	andl	$2, %edx
	leal	-1(%rax), %esi
	jne	.L343
	movq	24(%r12), %rcx
	movl	$-1, %edx
	movzwl	(%rcx), %edi
	cmpl	%esi, %eax
	jbe	.L310
.L309:
	movslq	%esi, %rax
	movzwl	(%rcx,%rax,2), %edx
.L310:
	cmpw	%di, %dx
	je	.L344
.L311:
	movl	$9, (%r14)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L307:
	testl	%eax, %eax
	jne	.L311
.L312:
	movzwl	-120(%rbp), %edx
	testw	%dx, %dx
	js	.L314
	movswl	%dx, %eax
	sarl	$5, %eax
	testl	%eax, %eax
	jne	.L316
.L346:
	movq	%r13, 16(%rbx)
.L313:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L345
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	movzwl	10(%r12), %edi
	leaq	10(%r12), %rcx
	movl	$-1, %edx
	cmpl	%esi, %eax
	ja	.L309
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L314:
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	je	.L346
.L316:
	andl	$2, %edx
	jne	.L317
	movq	-104(%rbp), %rax
	movzwl	(%rax), %eax
	cmpw	$37, %ax
	je	.L325
.L339:
	cmpw	$35, %ax
	je	.L324
	cmpw	$48, %ax
	je	.L324
	cmpw	$62, %ax
	jne	.L311
	movq	%r13, 16(%rbx)
	movq	$0, 24(%rbx)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L305:
	movl	12(%r12), %eax
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L317:
	movzwl	-118(%rbp), %eax
	cmpw	$37, %ax
	jne	.L339
.L325:
	movq	136(%r13), %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZNK6icu_6721RuleBasedNumberFormat11findRuleSetERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rax, 16(%rbx)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L324:
	movq	136(%r13), %rdi
	call	_ZNK6icu_6721RuleBasedNumberFormat23getDecimalFormatSymbolsEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L347
	movl	$368, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L322
	movq	%rax, %rdi
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L348
	movq	%r13, 24(%rbx)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L344:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movl	$1, %edx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L347:
	movl	$2, (%r14)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L348:
	movq	%r13, %rdi
	call	_ZN6icu_6713DecimalFormatD0Ev@PLT
	jmp	.L313
.L345:
	call	__stack_chk_fail@PLT
.L322:
	movl	$7, (%r14)
	jmp	.L313
	.cfi_endproc
.LFE2961:
	.size	_ZN6icu_6714NFSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6714NFSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6714NFSubstitutionC1EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6714NFSubstitutionC1EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6714NFSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714NFSubstitutionD2Ev
	.type	_ZN6icu_6714NFSubstitutionD2Ev, @function
_ZN6icu_6714NFSubstitutionD2Ev:
.LFB2964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L350
	movq	(%rdi), %rax
	call	*8(%rax)
.L350:
	movq	$0, 24(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2964:
	.size	_ZN6icu_6714NFSubstitutionD2Ev, .-_ZN6icu_6714NFSubstitutionD2Ev
	.globl	_ZN6icu_6714NFSubstitutionD1Ev
	.set	_ZN6icu_6714NFSubstitutionD1Ev,_ZN6icu_6714NFSubstitutionD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714NFSubstitutionD0Ev
	.type	_ZN6icu_6714NFSubstitutionD0Ev, @function
_ZN6icu_6714NFSubstitutionD0Ev:
.LFB2966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L356
	movq	(%rdi), %rax
	call	*8(%rax)
.L356:
	movq	$0, 24(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2966:
	.size	_ZN6icu_6714NFSubstitutionD0Ev, .-_ZN6icu_6714NFSubstitutionD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714NFSubstitution23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode
	.type	_ZN6icu_6714NFSubstitution23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode, @function
_ZN6icu_6714NFSubstitution23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode:
.LFB2968:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L361
	movq	(%rdi), %rax
	jmp	*328(%rax)
	.p2align 4,,10
	.p2align 3
.L361:
	ret
	.cfi_endproc
.LFE2968:
	.size	_ZN6icu_6714NFSubstitution23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode, .-_ZN6icu_6714NFSubstitution23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714NFSubstitution16getStaticClassIDEv
	.type	_ZN6icu_6714NFSubstitution16getStaticClassIDEv, @function
_ZN6icu_6714NFSubstitution16getStaticClassIDEv:
.LFB2969:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714NFSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2969:
	.size	_ZN6icu_6714NFSubstitution16getStaticClassIDEv, .-_ZN6icu_6714NFSubstitution16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721SameValueSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6721SameValueSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6721SameValueSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode:
.LFB2978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movl	%esi, 8(%rdi)
	movq	%rcx, %rsi
	movq	%rax, (%rdi)
	movups	%xmm0, 16(%rdi)
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	js	.L365
	movswl	%dx, %eax
	sarl	$5, %eax
.L366:
	cmpl	$1, %eax
	jle	.L367
	andl	$2, %edx
	leal	-1(%rax), %esi
	jne	.L405
	movq	24(%r12), %rcx
	movl	$-1, %edx
	movzwl	(%rcx), %edi
	cmpl	%esi, %eax
	jbe	.L370
.L369:
	movslq	%esi, %rax
	movzwl	(%rcx,%rax,2), %edx
.L370:
	cmpw	%di, %dx
	je	.L406
.L371:
	movl	$9, 0(%r13)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L367:
	testl	%eax, %eax
	jne	.L371
.L372:
	movzwl	-120(%rbp), %edx
	testw	%dx, %dx
	js	.L374
	movswl	%dx, %eax
	sarl	$5, %eax
	testl	%eax, %eax
	jne	.L376
.L408:
	movq	%r14, 16(%rbx)
.L373:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movswl	8(%r12), %edx
	leaq	16+_ZTVN6icu_6721SameValueSubstitutionE(%rip), %rax
	movq	%rax, (%rbx)
	testw	%dx, %dx
	js	.L384
	sarl	$5, %edx
.L385:
	movl	$2, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	_ZL13gEqualsEquals(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZL13gEqualsEquals(%rip), %rdx
	testb	%al, %al
	jne	.L364
	movl	$9, 0(%r13)
.L364:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L407
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore_state
	movzwl	10(%r12), %edi
	leaq	10(%r12), %rcx
	movl	$-1, %edx
	cmpl	%esi, %eax
	ja	.L369
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L374:
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	je	.L408
.L376:
	andl	$2, %edx
	jne	.L377
	movq	-104(%rbp), %rax
	movzwl	(%rax), %eax
	cmpw	$37, %ax
	je	.L388
.L401:
	cmpw	$35, %ax
	je	.L387
	cmpw	$48, %ax
	je	.L387
	cmpw	$62, %ax
	jne	.L371
	movq	%r14, 16(%rbx)
	movq	$0, 24(%rbx)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L365:
	movl	12(%r12), %eax
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L384:
	movl	12(%r12), %edx
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L377:
	movzwl	-118(%rbp), %eax
	cmpw	$37, %ax
	jne	.L401
.L388:
	movq	136(%r14), %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZNK6icu_6721RuleBasedNumberFormat11findRuleSetERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rax, 16(%rbx)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L387:
	movq	136(%r14), %rdi
	call	_ZNK6icu_6721RuleBasedNumberFormat23getDecimalFormatSymbolsEv@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L409
	movl	$368, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L382
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
	movl	0(%r13), %eax
	movq	-136(%rbp), %rdi
	testl	%eax, %eax
	jg	.L410
	movq	%rdi, 24(%rbx)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L406:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movl	$1, %edx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L409:
	movl	$2, 0(%r13)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L410:
	call	_ZN6icu_6713DecimalFormatD0Ev@PLT
	jmp	.L373
.L407:
	call	__stack_chk_fail@PLT
.L382:
	movl	$7, 0(%r13)
	jmp	.L373
	.cfi_endproc
.LFE2978:
	.size	_ZN6icu_6721SameValueSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6721SameValueSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6721SameValueSubstitutionC1EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6721SameValueSubstitutionC1EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6721SameValueSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721SameValueSubstitution16getStaticClassIDEv
	.type	_ZN6icu_6721SameValueSubstitution16getStaticClassIDEv, @function
_ZN6icu_6721SameValueSubstitution16getStaticClassIDEv:
.LFB2980:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6721SameValueSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2980:
	.size	_ZN6icu_6721SameValueSubstitution16getStaticClassIDEv, .-_ZN6icu_6721SameValueSubstitution16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722MultiplierSubstitution16getStaticClassIDEv
	.type	_ZN6icu_6722MultiplierSubstitution16getStaticClassIDEv, @function
_ZN6icu_6722MultiplierSubstitution16getStaticClassIDEv:
.LFB2982:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6722MultiplierSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2982:
	.size	_ZN6icu_6722MultiplierSubstitution16getStaticClassIDEv, .-_ZN6icu_6722MultiplierSubstitution16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719ModulusSubstitutionC2EiPKNS_6NFRuleES3_PKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6719ModulusSubstitutionC2EiPKNS_6NFRuleES3_PKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6719ModulusSubstitutionC2EiPKNS_6NFRuleES3_PKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode:
.LFB2986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rcx, -144(%rbp)
	movq	16(%rbp), %r15
	movq	%r8, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movl	%esi, 8(%rdi)
	movq	%r9, %rsi
	movq	%rax, (%rdi)
	movups	%xmm0, 16(%rdi)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	8(%r14), %edx
	testw	%dx, %dx
	js	.L414
	movswl	%dx, %eax
	sarl	$5, %eax
.L415:
	cmpl	$1, %eax
	jle	.L416
	andl	$2, %edx
	leal	-1(%rax), %esi
	jne	.L455
	movq	24(%r14), %rcx
	movl	$-1, %edx
	movzwl	(%rcx), %edi
	cmpl	%eax, %esi
	jnb	.L419
.L418:
	movslq	%esi, %rax
	movzwl	(%rcx,%rax,2), %edx
.L419:
	cmpw	%di, %dx
	je	.L456
.L420:
	movl	$9, (%r15)
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L416:
	testl	%eax, %eax
	jne	.L420
.L421:
	movzwl	-120(%rbp), %edx
	testw	%dx, %dx
	js	.L423
	movswl	%dx, %eax
	sarl	$5, %eax
	testl	%eax, %eax
	jne	.L425
.L458:
	movq	-136(%rbp), %rax
	movq	%rax, 16(%rbx)
.L422:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6719ModulusSubstitutionE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	call	_ZNK6icu_676NFRule10getDivisorEv@PLT
	movq	$0, 40(%rbx)
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.L433
	movl	$9, (%r15)
.L433:
	movswl	8(%r14), %edx
	testw	%dx, %dx
	js	.L434
	sarl	$5, %edx
.L435:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	_ZL26gGreaterGreaterGreaterThan(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZL26gGreaterGreaterGreaterThan(%rip), %rdx
	testb	%al, %al
	jne	.L413
	movq	-144(%rbp), %rax
	movq	%rax, 40(%rbx)
.L413:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L457
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	movzwl	10(%r14), %edi
	leaq	10(%r14), %rcx
	movl	$-1, %edx
	cmpl	%eax, %esi
	jb	.L418
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L423:
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	je	.L458
.L425:
	andl	$2, %edx
	jne	.L426
	movq	-104(%rbp), %rax
	movzwl	(%rax), %eax
	cmpw	$37, %ax
	je	.L438
.L451:
	cmpw	$35, %ax
	je	.L437
	cmpw	$48, %ax
	je	.L437
	cmpw	$62, %ax
	jne	.L420
	movq	-136(%rbp), %rax
	movq	$0, 24(%rbx)
	movq	%rax, 16(%rbx)
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L414:
	movl	12(%r14), %eax
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L434:
	movl	12(%r14), %edx
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L426:
	movzwl	-118(%rbp), %eax
	cmpw	$37, %ax
	jne	.L451
.L438:
	movq	-136(%rbp), %rax
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	136(%rax), %rdi
	call	_ZNK6icu_6721RuleBasedNumberFormat11findRuleSetERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rax, 16(%rbx)
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L437:
	movq	-136(%rbp), %rax
	movq	136(%rax), %rdi
	call	_ZNK6icu_6721RuleBasedNumberFormat23getDecimalFormatSymbolsEv@PLT
	testq	%rax, %rax
	je	.L459
	movl	$368, %edi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L431
	movq	-136(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
	movl	(%r15), %eax
	movq	-136(%rbp), %rdi
	testl	%eax, %eax
	jg	.L460
	movq	%rdi, 24(%rbx)
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L456:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$2, (%r15)
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L460:
	call	_ZN6icu_6713DecimalFormatD0Ev@PLT
	jmp	.L422
.L457:
	call	__stack_chk_fail@PLT
.L431:
	movl	$7, (%r15)
	jmp	.L422
	.cfi_endproc
.LFE2986:
	.size	_ZN6icu_6719ModulusSubstitutionC2EiPKNS_6NFRuleES3_PKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6719ModulusSubstitutionC2EiPKNS_6NFRuleES3_PKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6719ModulusSubstitutionC1EiPKNS_6NFRuleES3_PKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6719ModulusSubstitutionC1EiPKNS_6NFRuleES3_PKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6719ModulusSubstitutionC2EiPKNS_6NFRuleES3_PKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719ModulusSubstitution16getStaticClassIDEv
	.type	_ZN6icu_6719ModulusSubstitution16getStaticClassIDEv, @function
_ZN6icu_6719ModulusSubstitution16getStaticClassIDEv:
.LFB2988:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6719ModulusSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2988:
	.size	_ZN6icu_6719ModulusSubstitution16getStaticClassIDEv, .-_ZN6icu_6719ModulusSubstitution16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724IntegralPartSubstitution16getStaticClassIDEv
	.type	_ZN6icu_6724IntegralPartSubstitution16getStaticClassIDEv, @function
_ZN6icu_6724IntegralPartSubstitution16getStaticClassIDEv:
.LFB2995:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6724IntegralPartSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2995:
	.size	_ZN6icu_6724IntegralPartSubstitution16getStaticClassIDEv, .-_ZN6icu_6724IntegralPartSubstitution16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6726FractionalPartSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6726FractionalPartSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6726FractionalPartSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode:
.LFB2998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6714NFSubstitutionE(%rip), %rax
	movl	%esi, 8(%rdi)
	movq	%rcx, %rsi
	movq	%rax, (%rdi)
	movups	%xmm0, 16(%rdi)
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	js	.L464
	movswl	%dx, %eax
	sarl	$5, %eax
.L465:
	cmpl	$1, %eax
	jle	.L466
	andl	$2, %edx
	leal	-1(%rax), %esi
	jne	.L512
	movq	24(%r12), %rcx
	movl	$-1, %edx
	movzwl	(%rcx), %edi
	cmpl	%esi, %eax
	jbe	.L469
.L468:
	movslq	%esi, %rax
	movzwl	(%rcx,%rax,2), %edx
.L469:
	cmpw	%di, %dx
	je	.L513
.L470:
	movl	$9, (%r14)
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L466:
	testl	%eax, %eax
	jne	.L470
.L471:
	movzwl	-120(%rbp), %edx
	testw	%dx, %dx
	js	.L473
	movswl	%dx, %eax
	sarl	$5, %eax
	testl	%eax, %eax
	jne	.L475
.L514:
	movq	%r13, 16(%rbx)
.L472:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6726FractionalPartSubstitutionE(%rip), %rax
	movq	%rax, (%rbx)
	movl	$256, %eax
	movw	%ax, 32(%rbx)
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L483
	sarl	$5, %edx
.L484:
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	$2, %r9d
	movq	%r12, %rdi
	leaq	_ZL19gGreaterGreaterThan(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jne	.L485
	leaq	_ZL19gGreaterGreaterThan(%rip), %rax
	leaq	_ZL26gGreaterGreaterGreaterThan(%rip), %r13
.L486:
	movb	$1, 32(%rbx)
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L491
	sarl	$5, %edx
.L492:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	_ZL26gGreaterGreaterGreaterThan(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jne	.L463
	movb	$0, 33(%rbx)
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L512:
	movzwl	10(%r12), %edi
	leaq	10(%r12), %rcx
	movl	$-1, %edx
	cmpl	%esi, %eax
	ja	.L468
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L473:
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	je	.L514
.L475:
	andl	$2, %edx
	jne	.L476
	movq	-104(%rbp), %rax
	movzwl	(%rax), %eax
	cmpw	$37, %ax
	je	.L494
	cmpw	$35, %ax
	je	.L493
	cmpw	$48, %ax
	je	.L493
.L478:
	cmpw	$62, %ax
	jne	.L470
	movq	%r13, 16(%rbx)
	movq	$0, 24(%rbx)
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L464:
	movl	12(%r12), %eax
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L483:
	movl	12(%r12), %edx
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L485:
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L487
	sarl	$5, %edx
.L488:
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	$3, %r9d
	movq	%r12, %rdi
	leaq	_ZL26gGreaterGreaterGreaterThan(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	je	.L489
	cmpq	16(%rbx), %r13
	je	.L489
	leaq	_ZL26gGreaterGreaterGreaterThan(%rip), %rax
	leaq	_ZL19gGreaterGreaterThan(%rip), %rax
	movq	16(%rbx), %rax
	movb	$1, 160(%rax)
.L463:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L515
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	movzwl	-118(%rbp), %eax
	cmpw	$37, %ax
	je	.L494
	cmpw	$48, %ax
	je	.L493
	cmpw	$35, %ax
	jne	.L478
.L493:
	movq	136(%r13), %rdi
	call	_ZNK6icu_6721RuleBasedNumberFormat23getDecimalFormatSymbolsEv@PLT
	testq	%rax, %rax
	je	.L516
	movl	$368, %edi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L481
	movq	-136(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
	movl	(%r14), %edx
	movq	-136(%rbp), %rdi
	testl	%edx, %edx
	jg	.L517
	movq	%rdi, 24(%rbx)
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L491:
	movl	12(%r12), %edx
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L487:
	movl	12(%r12), %edx
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L489:
	leaq	_ZL26gGreaterGreaterGreaterThan(%rip), %r13
	leaq	_ZL19gGreaterGreaterThan(%rip), %rax
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L513:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movl	$1, %edx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L494:
	movq	136(%r13), %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZNK6icu_6721RuleBasedNumberFormat11findRuleSetERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rax, 16(%rbx)
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L516:
	movl	$2, (%r14)
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L517:
	call	_ZN6icu_6713DecimalFormatD0Ev@PLT
	jmp	.L472
.L515:
	call	__stack_chk_fail@PLT
.L481:
	movl	$7, (%r14)
	jmp	.L472
	.cfi_endproc
.LFE2998:
	.size	_ZN6icu_6726FractionalPartSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6726FractionalPartSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6726FractionalPartSubstitutionC1EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6726FractionalPartSubstitutionC1EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6726FractionalPartSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714NFSubstitution16makeSubstitutionEiPKNS_6NFRuleES3_PKNS_9NFRuleSetEPKNS_21RuleBasedNumberFormatERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6714NFSubstitution16makeSubstitutionEiPKNS_6NFRuleES3_PKNS_9NFRuleSetEPKNS_21RuleBasedNumberFormatERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6714NFSubstitution16makeSubstitutionEiPKNS_6NFRuleES3_PKNS_9NFRuleSetEPKNS_21RuleBasedNumberFormatERKNS_13UnicodeStringER10UErrorCode:
.LFB2959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	8(%r9), %eax
	testw	%ax, %ax
	js	.L519
	movswl	%ax, %edx
	sarl	$5, %edx
.L520:
	testl	%edx, %edx
	je	.L575
	testb	$2, %al
	jne	.L577
	movq	24(%r12), %rax
	movzwl	(%rax), %eax
	cmpw	$61, %ax
	je	.L525
.L580:
	cmpw	$62, %ax
	je	.L526
	cmpw	$60, %ax
	je	.L578
.L527:
	movl	$9, (%rbx)
.L575:
	xorl	%r13d, %r13d
.L518:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L579
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_restore_state
	leaq	10(%r12), %rax
	movzwl	(%rax), %eax
	cmpw	$61, %ax
	jne	.L580
.L525:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L575
	movq	%r14, %rdx
	movq	%rax, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	%r15d, %esi
	call	_ZN6icu_6714NFSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	movswl	8(%r12), %edx
	leaq	16+_ZTVN6icu_6721SameValueSubstitutionE(%rip), %rax
	movq	%rax, 0(%r13)
	testw	%dx, %dx
	js	.L548
	sarl	$5, %edx
.L549:
	movl	$2, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	_ZL13gEqualsEquals(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZL13gEqualsEquals(%rip), %rdx
	testb	%al, %al
	jne	.L518
.L576:
	movl	$9, (%rbx)
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L519:
	movl	12(%r9), %edx
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L578:
	movq	(%r10), %rax
	cmpq	$-1, %rax
	je	.L527
	leaq	3(%rax), %rdx
	cmpq	$1, %rdx
	jbe	.L529
	cmpq	$-4, %rax
	je	.L529
	cmpb	$0, 160(%r14)
	je	.L581
	pxor	%xmm2, %xmm2
	movl	$56, %edi
	cvtsi2sdq	%rax, %xmm2
	movq	384(%r8), %rax
	movq	%rax, -208(%rbp)
	movsd	%xmm2, -200(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L575
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L533
	movswl	%ax, %esi
	sarl	$5, %esi
.L534:
	subl	$2, %esi
	movl	$2, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	_ZN6icu_6721NumeratorSubstitution4LTLTE(%rip), %rcx
	movl	$2, %edx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_6721NumeratorSubstitution4LTLTE(%rip), %rcx
	testb	%al, %al
	jne	.L535
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L536
	movswl	%ax, %ecx
	movl	%ecx, %eax
	sarl	$5, %eax
.L537:
	leaq	-128(%rbp), %r8
	leal	-1(%rax), %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -216(%rbp)
	leaq	-192(%rbp), %r14
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	movq	-216(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	movq	-216(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L538:
	movq	-208(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r14, %rcx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6714NFSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6721NumeratorSubstitutionE(%rip), %rax
	movsd	-200(%rbp), %xmm0
	movq	%rax, 0(%r13)
	movsd	%xmm0, 32(%r13)
	call	_ZN6icu_6717util64_fromDoubleEd@PLT
	movq	%rax, 40(%r13)
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L539
	movswl	%ax, %esi
	sarl	$5, %esi
.L540:
	subl	$2, %esi
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movq	%r12, %rdi
	leaq	_ZN6icu_6721NumeratorSubstitution4LTLTE(%rip), %rcx
	movl	$2, %edx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	leaq	_ZN6icu_6721NumeratorSubstitution4LTLTE(%rip), %rax
	sete	48(%r13)
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L526:
	movq	(%r10), %rax
	cmpq	$-1, %rax
	je	.L582
	leaq	3(%rax), %rdx
	cmpq	$1, %rdx
	jbe	.L542
	cmpq	$-4, %rax
	je	.L542
	cmpb	$0, 160(%r14)
	jne	.L527
	movl	$48, %edi
	movq	%r11, -208(%rbp)
	movq	%r10, -200(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L575
	movq	%rax, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r14, %rdx
	movl	%r15d, %esi
	call	_ZN6icu_6714NFSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	leaq	16+_ZTVN6icu_6719ModulusSubstitutionE(%rip), %rax
	movq	-200(%rbp), %r10
	movq	%rax, 0(%r13)
	movq	%r10, %rdi
	call	_ZNK6icu_676NFRule10getDivisorEv@PLT
	movq	$0, 40(%r13)
	movq	-208(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, 32(%r13)
	jne	.L545
	movl	$9, (%rbx)
.L545:
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L546
	movswl	%ax, %edx
	sarl	$5, %edx
.L547:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	_ZL26gGreaterGreaterGreaterThan(%rip), %rcx
	movq	%r11, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZL26gGreaterGreaterGreaterThan(%rip), %rdx
	testb	%al, %al
	jne	.L518
	movq	-200(%rbp), %r11
	movq	%r11, 40(%r13)
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L548:
	movl	12(%r12), %edx
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L529:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L518
	movq	%rax, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r14, %rdx
	movl	%r15d, %esi
	call	_ZN6icu_6714NFSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	leaq	16+_ZTVN6icu_6724IntegralPartSubstitutionE(%rip), %rax
	movq	%rax, 0(%r13)
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L542:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L518
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r14, %rdx
	movl	%r15d, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6726FractionalPartSubstitutionC1EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L582:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L518
	movq	%rax, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r14, %rdx
	movl	%r15d, %esi
	call	_ZN6icu_6714NFSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	leaq	16+_ZTVN6icu_6725AbsoluteValueSubstitutionE(%rip), %rax
	movq	%rax, 0(%r13)
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L581:
	movl	$40, %edi
	movq	%r10, -200(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L575
	movq	%rax, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r14, %rdx
	movl	%r15d, %esi
	call	_ZN6icu_6714NFSubstitutionC2EiPKNS_9NFRuleSetERKNS_13UnicodeStringER10UErrorCode
	leaq	16+_ZTVN6icu_6722MultiplierSubstitutionE(%rip), %rax
	movq	-200(%rbp), %r10
	movq	%rax, 0(%r13)
	movq	%r10, %rdi
	call	_ZNK6icu_676NFRule10getDivisorEv@PLT
	movq	%rax, 32(%r13)
	testq	%rax, %rax
	jne	.L518
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L546:
	movl	12(%r12), %edx
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L539:
	movl	12(%r12), %esi
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L533:
	movl	12(%r12), %esi
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L535:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L538
.L536:
	movl	12(%r12), %eax
	jmp	.L537
.L579:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2959:
	.size	_ZN6icu_6714NFSubstitution16makeSubstitutionEiPKNS_6NFRuleES3_PKNS_9NFRuleSetEPKNS_21RuleBasedNumberFormatERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6714NFSubstitution16makeSubstitutionEiPKNS_6NFRuleES3_PKNS_9NFRuleSetEPKNS_21RuleBasedNumberFormatERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6726FractionalPartSubstitution16getStaticClassIDEv
	.type	_ZN6icu_6726FractionalPartSubstitution16getStaticClassIDEv, @function
_ZN6icu_6726FractionalPartSubstitution16getStaticClassIDEv:
.LFB3003:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6726FractionalPartSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3003:
	.size	_ZN6icu_6726FractionalPartSubstitution16getStaticClassIDEv, .-_ZN6icu_6726FractionalPartSubstitution16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725AbsoluteValueSubstitution16getStaticClassIDEv
	.type	_ZN6icu_6725AbsoluteValueSubstitution16getStaticClassIDEv, @function
_ZN6icu_6725AbsoluteValueSubstitution16getStaticClassIDEv:
.LFB3005:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6725AbsoluteValueSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3005:
	.size	_ZN6icu_6725AbsoluteValueSubstitution16getStaticClassIDEv, .-_ZN6icu_6725AbsoluteValueSubstitution16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721NumeratorSubstitution16getStaticClassIDEv
	.type	_ZN6icu_6721NumeratorSubstitution16getStaticClassIDEv, @function
_ZN6icu_6721NumeratorSubstitution16getStaticClassIDEv:
.LFB3010:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6721NumeratorSubstitution16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3010:
	.size	_ZN6icu_6721NumeratorSubstitution16getStaticClassIDEv, .-_ZN6icu_6721NumeratorSubstitution16getStaticClassIDEv
	.align 2
	.p2align 4
	.type	_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode.part.0, @function
_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode.part.0:
.LFB4237:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movabsq	$9007199254740991, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	cmpq	%rdx, %rsi
	jg	.L587
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
	call	*72(%rax)
	movq	24(%rbx), %rdi
	movsd	%xmm0, -248(%rbp)
	call	_ZNK6icu_6712NumberFormat24getMaximumFractionDigitsEv@PLT
	movsd	-248(%rbp), %xmm0
	testl	%eax, %eax
	je	.L599
.L588:
	leaq	-176(%rbp), %r8
	movq	24(%rbx), %r9
	movl	$2, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r8, %rdi
	movq	%rax, -240(%rbp)
	movw	%dx, -232(%rbp)
	movq	%r9, -256(%rbp)
	movq	%r8, -248(%rbp)
	call	_ZN6icu_6711FormattableC1Ed@PLT
.L598:
	movq	-256(%rbp), %r9
	movq	-248(%rbp), %r8
	leaq	-240(%rbp), %r15
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%r9, %rdi
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
	movq	-248(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movl	8(%rbx), %esi
	movswl	-232(%rbp), %r9d
	addl	%r12d, %esi
	testw	%r9w, %r9w
	js	.L592
	sarl	$5, %r9d
.L593:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r15, %rcx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L600
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	call	*64(%rax)
	leaq	-176(%rbp), %r8
	movq	24(%rbx), %r9
	movq	%rax, %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r8, %rdi
	movq	%r8, -248(%rbp)
	movq	%rax, -240(%rbp)
	movl	$2, %eax
	movw	%ax, -232(%rbp)
	movq	%r9, -256(%rbp)
	call	_ZN6icu_6711FormattableC1El@PLT
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L592:
	movl	-228(%rbp), %r9d
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L599:
	call	uprv_floor_67@PLT
	jmp	.L588
.L600:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4237:
	.size	_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode.part.0, .-_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode, @function
_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode:
.LFB2973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r14
	testq	%r14, %r14
	je	.L602
	movq	(%rdi), %rax
	addl	8(%rdi), %ecx
	movl	%r8d, %r15d
	movl	%ecx, %r12d
	call	*64(%rax)
	addq	$8, %rsp
	movq	%rbx, %r9
	movl	%r15d, %r8d
	popq	%rbx
	movl	%r12d, %ecx
	movq	%r13, %rdx
	popq	%r12
	movq	%r14, %rdi
	popq	%r13
	movq	%rax, %rsi
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_679NFRuleSet6formatElRNS_13UnicodeStringEiiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L602:
	.cfi_restore_state
	cmpq	$0, 24(%rdi)
	je	.L601
	addq	$8, %rsp
	movq	%r9, %r8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2973:
	.size	_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode, .-_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719ModulusSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZNK6icu_6719ModulusSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode, @function
_ZNK6icu_6719ModulusSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode:
.LFB2991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L612
	movq	(%r12), %rdx
	leaq	_ZNK6icu_6719ModulusSubstitution15transformNumberEl(%rip), %rcx
	movq	64(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L609
	movq	%rsi, %rax
	cqto
	idivq	32(%r12)
	movq	%rdx, %rsi
.L610:
	movl	8(%r12), %ecx
	addq	$16, %rsp
	movq	%r14, %rdx
	popq	%r12
	addl	%r13d, %ecx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676NFRule8doFormatElRNS_13UnicodeStringEiiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L612:
	.cfi_restore_state
	movq	16(%r12), %r15
	movl	%r8d, -36(%rbp)
	testq	%r15, %r15
	je	.L607
	movq	(%r12), %rdx
	addl	8(%r12), %r13d
	movq	%r9, -48(%rbp)
	movq	%r12, %rdi
	call	*64(%rdx)
	movq	-48(%rbp), %r9
	movl	-36(%rbp), %r8d
	addq	$16, %rsp
	popq	%r12
	movl	%r13d, %ecx
	movq	%r14, %rdx
	popq	%r13
	movq	%r15, %rdi
	popq	%r14
	movq	%rax, %rsi
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_679NFRuleSet6formatElRNS_13UnicodeStringEiiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L609:
	.cfi_restore_state
	movq	%r9, -48(%rbp)
	movq	%r12, %rdi
	movl	%r8d, -36(%rbp)
	call	*%rdx
	movq	40(%r12), %rdi
	movq	-48(%rbp), %r9
	movl	-36(%rbp), %r8d
	movq	%rax, %rsi
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L607:
	cmpq	$0, 24(%r12)
	je	.L605
	addq	$16, %rsp
	movq	%r12, %rdi
	movq	%r9, %r8
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L605:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2991:
	.size	_ZNK6icu_6719ModulusSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode, .-_ZNK6icu_6719ModulusSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721NumeratorSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZNK6icu_6721NumeratorSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode, @function
_ZNK6icu_6721NumeratorSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode:
.LFB3007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	leaq	_ZNK6icu_6721NumeratorSubstitution15transformNumberEd(%rip), %rdx
	subq	$264, %rsp
	movq	%r8, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L614
	mulsd	32(%rdi), %xmm0
	call	uprv_round_67@PLT
	movsd	%xmm0, -272(%rbp)
.L615:
	call	_ZN6icu_6717util64_fromDoubleEd@PLT
	cmpb	$0, 48(%r13)
	movq	16(%r13), %r15
	movq	%rax, -296(%rbp)
	je	.L616
	testq	%r15, %r15
	je	.L617
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L618
	movswl	%ax, %edx
	sarl	$5, %edx
	movl	%edx, -300(%rbp)
.L619:
	movq	-296(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movsd	32(%r13), %xmm1
	leaq	(%rdi,%rdi,4), %r10
	leaq	-242(%rbp), %rdi
	addq	%r10, %r10
	movq	%rdi, -288(%rbp)
	cvtsi2sdq	%r10, %xmm0
	comisd	%xmm0, %xmm1
	jbe	.L620
	movl	%r14d, -280(%rbp)
	movq	%r13, %r14
	movq	%r10, %r13
	.p2align 4,,10
	.p2align 3
.L622:
	movl	8(%r14), %esi
	movl	$32, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	-288(%rbp), %rcx
	movw	%dx, -242(%rbp)
	xorl	%edx, %edx
	movl	$1, %r9d
	addl	%ebx, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movl	8(%r14), %ecx
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	-264(%rbp), %r9
	movl	-280(%rbp), %r8d
	movq	%r15, %rdi
	addl	%ebx, %ecx
	call	_ZNK6icu_679NFRuleSet6formatElRNS_13UnicodeStringEiiR10UErrorCode@PLT
	leaq	0(%r13,%r13,4), %r10
	pxor	%xmm0, %xmm0
	movsd	32(%r14), %xmm1
	leaq	(%r10,%r10), %r13
	cvtsi2sdq	%r13, %xmm0
	comisd	%xmm0, %xmm1
	ja	.L622
	movq	%r14, %r13
	movswl	8(%r12), %eax
	movl	-280(%rbp), %r14d
.L620:
	testw	%ax, %ax
	js	.L623
	sarl	$5, %eax
.L624:
	pxor	%xmm0, %xmm0
	movsd	-272(%rbp), %xmm3
	subl	-300(%rbp), %eax
	cvtsi2sdq	-296(%rbp), %xmm0
	addl	%eax, %ebx
	ucomisd	%xmm0, %xmm3
	jp	.L627
	jne	.L627
.L625:
	addl	8(%r13), %ebx
	movl	%r14d, %r8d
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	-264(%rbp), %r9
	movq	-296(%rbp), %rsi
	movl	%ebx, %ecx
	call	_ZNK6icu_679NFRuleSet6formatElRNS_13UnicodeStringEiiR10UErrorCode@PLT
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L616:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	-296(%rbp), %xmm0
	ucomisd	-272(%rbp), %xmm0
	jp	.L628
	jne	.L628
	testq	%r15, %r15
	jne	.L625
.L617:
	movq	24(%r13), %r8
	movsd	-272(%rbp), %xmm0
	leaq	-176(%rbp), %r15
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -240(%rbp)
	movq	%r15, %rdi
	movl	$2, %eax
	leaq	-240(%rbp), %r14
	movw	%ax, -232(%rbp)
	movq	%r8, -280(%rbp)
	call	_ZN6icu_6711FormattableC1Ed@PLT
	movq	-280(%rbp), %r8
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	-264(%rbp), %rcx
	movq	%r8, %rdi
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movzwl	-232(%rbp), %eax
	addl	8(%r13), %ebx
	movl	%ebx, %esi
	testw	%ax, %ax
	js	.L631
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L632:
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L628:
	testq	%r15, %r15
	je	.L617
.L627:
	addl	8(%r13), %ebx
	movl	%r14d, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	-264(%rbp), %r8
	movsd	-272(%rbp), %xmm0
	movl	%ebx, %edx
	call	_ZNK6icu_679NFRuleSet6formatEdRNS_13UnicodeStringEiiR10UErrorCode@PLT
.L613:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L647
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	call	*%rax
	movsd	%xmm0, -272(%rbp)
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L631:
	movl	-228(%rbp), %r9d
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L618:
	movl	12(%r12), %edi
	movl	%edi, -300(%rbp)
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L623:
	movl	12(%r12), %eax
	jmp	.L624
.L647:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3007:
	.size	_ZNK6icu_6721NumeratorSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode, .-_ZNK6icu_6721NumeratorSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721NumeratorSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.type	_ZNK6icu_6721NumeratorSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE, @function
_ZNK6icu_6721NumeratorSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE:
.LFB3008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-240(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$296, %rsp
	movq	%rsi, -328(%rbp)
	movq	%r9, -304(%rbp)
	movsd	%xmm0, -312(%rbp)
	movsd	%xmm1, -320(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -260(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	cmpb	$0, 48(%r14)
	jne	.L688
.L649:
	movq	-304(%rbp), %r9
	xorl	%ecx, %ecx
	movl	%r15d, %r8d
	movq	%r12, %rdx
	movsd	-320(%rbp), %xmm1
	movsd	-312(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6714NFSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	cmpb	$0, 48(%r14)
	jne	.L689
.L666:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L690
	addq	$296, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_restore_state
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rax, -256(%rbp)
	movabsq	$-4294967295, %rax
	movq	%rax, -248(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN6icu_6711FormattableC1Ev@PLT
	leaq	-256(%rbp), %rcx
	movzwl	-232(%rbp), %eax
	movq	%rcx, -280(%rbp)
	.p2align 4,,10
	.p2align 3
.L650:
	movl	%ebx, -292(%rbp)
	testw	%ax, %ax
	js	.L651
.L692:
	cwtl
	sarl	$5, %eax
	testl	%eax, %eax
	jle	.L653
.L693:
	movl	-248(%rbp), %edx
	testl	%edx, %edx
	je	.L653
	movq	.LC3(%rip), %rax
	movq	16(%r14), %rdi
	movl	%r15d, %ecx
	movq	%r13, %rsi
	movq	-280(%rbp), %rdx
	movq	-288(%rbp), %r8
	movl	$0, -248(%rbp)
	movq	%rax, %xmm0
	call	_ZNK6icu_679NFRuleSet5parseERKNS_13UnicodeStringERNS_13ParsePositionEdjRNS_11FormattableE@PLT
	movl	-248(%rbp), %edx
	testl	%edx, %edx
	je	.L653
	addl	%edx, 8(%r12)
	addl	$1, %ebx
	cmpl	$2147483647, %edx
	jne	.L654
	movzwl	-232(%rbp), %eax
	testb	$1, %al
	je	.L655
	movl	$2, %eax
	movw	%ax, -232(%rbp)
	movl	$2, %eax
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L691:
	movq	-216(%rbp), %rdx
	cmpw	$32, (%rdx)
	jne	.L650
.L662:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	addl	$1, 8(%r12)
	movzwl	-232(%rbp), %eax
.L656:
	testw	%ax, %ax
	js	.L658
	movswl	%ax, %edx
	sarl	$5, %edx
.L659:
	testl	%edx, %edx
	jle	.L650
	testb	$2, %al
	je	.L691
	cmpw	$32, -230(%rbp)
	je	.L662
	movl	%ebx, -292(%rbp)
	testw	%ax, %ax
	jns	.L692
.L651:
	movl	-228(%rbp), %eax
	testl	%eax, %eax
	jg	.L693
	.p2align 4,,10
	.p2align 3
.L653:
	movq	-328(%rbp), %rsi
	movq	%r13, %rdi
	movl	-292(%rbp), %ebx
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	8(%r12), %edx
	cmpl	$2147483647, %edx
	jne	.L663
	movzwl	-232(%rbp), %eax
	movl	%eax, %edx
	andl	$31, %edx
	testb	$1, %al
	movl	$2, %eax
	cmove	%edx, %eax
	movw	%ax, -232(%rbp)
.L665:
	movl	$0, 8(%r12)
	movq	-288(%rbp), %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	-280(%rbp), %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	cmpb	$0, 48(%r14)
	je	.L649
	movsd	.LC3(%rip), %xmm2
	movsd	%xmm2, -312(%rbp)
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L658:
	movl	-228(%rbp), %edx
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L655:
	andl	$31, %eax
	movw	%ax, -232(%rbp)
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L654:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movzwl	-232(%rbp), %eax
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L689:
	movq	-304(%rbp), %rdi
	leaq	-260(%rbp), %rsi
	call	_ZNK6icu_6711Formattable7getLongER10UErrorCode@PLT
	movl	$1, %edx
	movslq	%eax, %rcx
	testq	%rcx, %rcx
	jle	.L670
	.p2align 4,,10
	.p2align 3
.L667:
	leaq	(%rdx,%rdx,4), %rdx
	addq	%rdx, %rdx
	cmpq	%rdx, %rcx
	jge	.L667
.L670:
	testl	%ebx, %ebx
	je	.L668
	.p2align 4,,10
	.p2align 3
.L669:
	leaq	(%rdx,%rdx,4), %rdx
	addq	%rdx, %rdx
	subl	$1, %ebx
	jne	.L669
.L668:
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	-304(%rbp), %rdi
	cvtsi2sdl	%eax, %xmm0
	cvtsi2sdq	%rdx, %xmm1
	divsd	%xmm1, %xmm0
	call	_ZN6icu_6711Formattable9setDoubleEd@PLT
	jmp	.L666
.L663:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L665
.L690:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3008:
	.size	_ZNK6icu_6721NumeratorSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE, .-_ZNK6icu_6721NumeratorSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6726FractionalPartSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZNK6icu_6726FractionalPartSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode, @function
_ZNK6icu_6726FractionalPartSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode:
.LFB3000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$120, %rsp
	movl	%edx, -148(%rbp)
	movl	%ecx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rdi)
	jne	.L695
	call	_ZNK6icu_6714NFSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
.L694:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L707
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L695:
	.cfi_restore_state
	leaq	-128(%rbp), %r15
	movsd	%xmm0, -160(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movsd	-160(%rbp), %xmm0
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	movq	%rbx, %rcx
	movl	$4, %edx
	movq	%r15, %rdi
	movl	$-20, %esi
	call	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity24getLowerDisplayMagnitudeEv@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jns	.L697
	leaq	-130(%rbp), %rax
	movq	%rax, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L698:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity8getDigitEi@PLT
	movq	16(%r12), %rdi
	movq	%rbx, %r9
	movq	%r13, %rdx
	movl	-152(%rbp), %r8d
	movl	-148(%rbp), %ecx
	movsbq	%al, %rsi
	addl	8(%r12), %ecx
	call	_ZNK6icu_679NFRuleSet6formatElRNS_13UnicodeStringEiiR10UErrorCode@PLT
	addl	$1, %r14d
	je	.L699
	cmpb	$0, 33(%r12)
	je	.L698
	movq	-160(%rbp), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	-148(%rbp), %esi
	movl	$32, %eax
	addl	8(%r12), %esi
	movl	$1, %r9d
	movw	%ax, -130(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L697:
	movq	16(%r12), %rdi
	movl	-148(%rbp), %ecx
	movq	%rbx, %r9
	xorl	%esi, %esi
	movl	-152(%rbp), %r8d
	addl	8(%r12), %ecx
	movq	%r13, %rdx
	call	_ZNK6icu_679NFRuleSet6formatElRNS_13UnicodeStringEiiR10UErrorCode@PLT
.L699:
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	jmp	.L694
.L707:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3000:
	.size	_ZNK6icu_6726FractionalPartSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode, .-_ZNK6icu_6726FractionalPartSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6726FractionalPartSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE.part.0, @function
_ZNK6icu_6726FractionalPartSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE.part.0:
.LFB4239:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-336(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-240(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$376, %rsp
	movq	%rdi, -360(%rbp)
	movq	%r13, %rdi
	movl	%r8d, -368(%rbp)
	movq	%r9, -400(%rbp)
	movb	%cl, -361(%rbp)
	movsd	%xmm0, -408(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rax, -336(%rbp)
	movabsq	$-4294967295, %rax
	movq	%rax, -328(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -384(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movswl	-232(%rbp), %eax
	movq	$0, -376(%rbp)
	movl	$0, -388(%rbp)
	testw	%ax, %ax
	js	.L709
	.p2align 4,,10
	.p2align 3
.L741:
	sarl	$5, %eax
	testl	%eax, %eax
	jle	.L711
.L742:
	movl	-328(%rbp), %esi
	testl	%esi, %esi
	je	.L711
	leaq	-176(%rbp), %r12
	leaq	-340(%rbp), %r14
	movl	$0, -328(%rbp)
	movq	%r12, %rdi
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	%r12, %r8
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	-360(%rbp), %rax
	movl	-368(%rbp), %ecx
	movq	16(%rax), %rdi
	movq	.LC4(%rip), %rax
	movq	%rax, %xmm0
	call	_ZNK6icu_679NFRuleSet5parseERKNS_13UnicodeStringERNS_13ParsePositionEdjRNS_11FormattableE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$0, -340(%rbp)
	call	_ZNK6icu_6711Formattable7getLongER10UErrorCode@PLT
	cmpb	$0, -361(%rbp)
	movl	%eax, %esi
	je	.L713
	movl	-328(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L739
.L716:
	movq	-384(%rbp), %rdi
	xorl	%edx, %edx
	movsbl	%sil, %esi
	movl	$1, %ecx
	call	_ZN6icu_676number4impl15DecimalQuantity11appendDigitEaib@PLT
	movl	-328(%rbp), %edx
	addl	%edx, 8(%rbx)
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	addl	$1, -388(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L740:
	movq	-216(%rbp), %rax
	cmpw	$32, (%rax)
	jne	.L719
.L723:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	addl	$1, 8(%rbx)
.L724:
	movzwl	-232(%rbp), %eax
	testw	%ax, %ax
	js	.L720
	movswl	%ax, %edx
	sarl	$5, %edx
.L721:
	testl	%edx, %edx
	jle	.L719
	testb	$2, %al
	je	.L740
	cmpw	$32, -230(%rbp)
	je	.L723
.L719:
	movq	%r12, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L745:
	movswl	-232(%rbp), %eax
	testw	%ax, %ax
	jns	.L741
.L709:
	movl	-228(%rbp), %eax
	testl	%eax, %eax
	jg	.L742
.L711:
	cmpq	$0, -376(%rbp)
	je	.L715
	movq	-376(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
.L715:
	movq	-384(%rbp), %rbx
	movl	-388(%rbp), %esi
	movq	%rbx, %rdi
	negl	%esi
	call	_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi@PLT
	movq	%rbx, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv@PLT
	movq	-360(%rbp), %rax
	leaq	_ZNK6icu_6726FractionalPartSubstitution16composeRuleValueEdd(%rip), %rdx
	movq	(%rax), %rax
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L726
	addsd	-408(%rbp), %xmm0
.L727:
	movq	-400(%rbp), %rdi
	call	_ZN6icu_6711Formattable9setDoubleEd@PLT
	movq	-384(%rbp), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L743
	addq	$376, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L739:
	.cfi_restore_state
	cmpq	$0, -376(%rbp)
	je	.L744
.L717:
	movq	-376(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*160(%rax)
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6711Formattable7getLongER10UErrorCode@PLT
	movl	%eax, %esi
.L713:
	movl	-328(%rbp), %eax
	testl	%eax, %eax
	jne	.L716
.L738:
	movq	%r12, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L720:
	movl	-228(%rbp), %edx
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L744:
	movq	%r14, %rdi
	movl	%eax, -392(%rbp)
	movl	$0, -340(%rbp)
	call	_ZN6icu_6712NumberFormat14createInstanceER10UErrorCode@PLT
	movl	-340(%rbp), %edx
	movl	-392(%rbp), %esi
	movq	%rax, %rdi
	testl	%edx, %edx
	jle	.L718
	testq	%rax, %rax
	je	.L713
	movq	(%rax), %rax
	call	*8(%rax)
	movl	-328(%rbp), %eax
	movl	-392(%rbp), %esi
	testl	%eax, %eax
	je	.L738
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L718:
	testq	%rax, %rax
	je	.L713
	movq	%rax, -376(%rbp)
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L726:
	movsd	-408(%rbp), %xmm1
	movq	-360(%rbp), %rdi
	call	*%rax
	jmp	.L727
.L743:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4239:
	.size	_ZNK6icu_6726FractionalPartSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE.part.0, .-_ZNK6icu_6726FractionalPartSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6726FractionalPartSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.type	_ZNK6icu_6726FractionalPartSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE, @function
_ZNK6icu_6726FractionalPartSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE:
.LFB3001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movzbl	32(%rdi), %r15d
	movsd	%xmm0, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%r15b, %r15b
	jne	.L747
	movq	(%rdi), %rax
	movq	%rsi, -80(%rbp)
	pxor	%xmm0, %xmm0
	movl	%r8d, -84(%rbp)
	call	*96(%rax)
	movq	16(%r12), %rdi
	movq	-80(%rbp), %rsi
	testq	%rdi, %rdi
	je	.L748
	movl	-84(%rbp), %ecx
	movq	%r13, %r8
	movq	%r14, %rdx
	call	_ZNK6icu_679NFRuleSet5parseERKNS_13UnicodeStringERNS_13ParsePositionEdjRNS_11FormattableE@PLT
	testb	%bl, %bl
	movl	8(%r14), %eax
	movq	-80(%rbp), %rsi
	je	.L750
	movq	16(%r12), %rdx
	cmpb	$0, 160(%rdx)
	jne	.L750
	testl	%eax, %eax
	je	.L751
	.p2align 4,,10
	.p2align 3
.L757:
	leaq	-60(%rbp), %rsi
	movq	%r13, %rdi
	movl	$0, -60(%rbp)
	movl	$1, %r15d
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	movq	(%r12), %rax
	movsd	-72(%rbp), %xmm1
	movq	%r12, %rdi
	call	*88(%rax)
	movq	%r13, %rdi
	call	_ZN6icu_6711Formattable9setDoubleEd@PLT
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L748:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L763
	movq	(%rdi), %rax
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	*160(%rax)
.L763:
	movl	8(%r14), %eax
.L750:
	testl	%eax, %eax
	jne	.L757
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6711Formattable7setLongEi@PLT
.L746:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L764
	addq	$56, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L747:
	.cfi_restore_state
	movsbl	%bl, %r10d
	movl	%r10d, %ecx
	call	_ZNK6icu_6726FractionalPartSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE.part.0
	movl	%eax, %r15d
	jmp	.L746
.L751:
	leaq	-60(%rbp), %rdi
	movq	%rsi, -80(%rbp)
	movl	$0, -60(%rbp)
	call	_ZN6icu_6712NumberFormat14createInstanceER10UErrorCode@PLT
	movq	%rax, %rdi
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L765
	testq	%rdi, %rdi
	je	.L763
.L754:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L763
.L765:
	movq	(%rdi), %rax
	movq	-80(%rbp), %rsi
	movq	%r14, %rcx
	movq	%rdi, -80(%rbp)
	movq	%r13, %rdx
	call	*160(%rax)
	movq	-80(%rbp), %rdi
	jmp	.L754
.L764:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3001:
	.size	_ZNK6icu_6726FractionalPartSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE, .-_ZNK6icu_6726FractionalPartSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.weak	_ZTSN6icu_6714NFSubstitutionE
	.section	.rodata._ZTSN6icu_6714NFSubstitutionE,"aG",@progbits,_ZTSN6icu_6714NFSubstitutionE,comdat
	.align 16
	.type	_ZTSN6icu_6714NFSubstitutionE, @object
	.size	_ZTSN6icu_6714NFSubstitutionE, 26
_ZTSN6icu_6714NFSubstitutionE:
	.string	"N6icu_6714NFSubstitutionE"
	.weak	_ZTIN6icu_6714NFSubstitutionE
	.section	.data.rel.ro._ZTIN6icu_6714NFSubstitutionE,"awG",@progbits,_ZTIN6icu_6714NFSubstitutionE,comdat
	.align 8
	.type	_ZTIN6icu_6714NFSubstitutionE, @object
	.size	_ZTIN6icu_6714NFSubstitutionE, 24
_ZTIN6icu_6714NFSubstitutionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714NFSubstitutionE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6721SameValueSubstitutionE
	.section	.rodata._ZTSN6icu_6721SameValueSubstitutionE,"aG",@progbits,_ZTSN6icu_6721SameValueSubstitutionE,comdat
	.align 32
	.type	_ZTSN6icu_6721SameValueSubstitutionE, @object
	.size	_ZTSN6icu_6721SameValueSubstitutionE, 33
_ZTSN6icu_6721SameValueSubstitutionE:
	.string	"N6icu_6721SameValueSubstitutionE"
	.weak	_ZTIN6icu_6721SameValueSubstitutionE
	.section	.data.rel.ro._ZTIN6icu_6721SameValueSubstitutionE,"awG",@progbits,_ZTIN6icu_6721SameValueSubstitutionE,comdat
	.align 8
	.type	_ZTIN6icu_6721SameValueSubstitutionE, @object
	.size	_ZTIN6icu_6721SameValueSubstitutionE, 24
_ZTIN6icu_6721SameValueSubstitutionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6721SameValueSubstitutionE
	.quad	_ZTIN6icu_6714NFSubstitutionE
	.weak	_ZTSN6icu_6722MultiplierSubstitutionE
	.section	.rodata._ZTSN6icu_6722MultiplierSubstitutionE,"aG",@progbits,_ZTSN6icu_6722MultiplierSubstitutionE,comdat
	.align 32
	.type	_ZTSN6icu_6722MultiplierSubstitutionE, @object
	.size	_ZTSN6icu_6722MultiplierSubstitutionE, 34
_ZTSN6icu_6722MultiplierSubstitutionE:
	.string	"N6icu_6722MultiplierSubstitutionE"
	.weak	_ZTIN6icu_6722MultiplierSubstitutionE
	.section	.data.rel.ro._ZTIN6icu_6722MultiplierSubstitutionE,"awG",@progbits,_ZTIN6icu_6722MultiplierSubstitutionE,comdat
	.align 8
	.type	_ZTIN6icu_6722MultiplierSubstitutionE, @object
	.size	_ZTIN6icu_6722MultiplierSubstitutionE, 24
_ZTIN6icu_6722MultiplierSubstitutionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722MultiplierSubstitutionE
	.quad	_ZTIN6icu_6714NFSubstitutionE
	.weak	_ZTSN6icu_6719ModulusSubstitutionE
	.section	.rodata._ZTSN6icu_6719ModulusSubstitutionE,"aG",@progbits,_ZTSN6icu_6719ModulusSubstitutionE,comdat
	.align 16
	.type	_ZTSN6icu_6719ModulusSubstitutionE, @object
	.size	_ZTSN6icu_6719ModulusSubstitutionE, 31
_ZTSN6icu_6719ModulusSubstitutionE:
	.string	"N6icu_6719ModulusSubstitutionE"
	.weak	_ZTIN6icu_6719ModulusSubstitutionE
	.section	.data.rel.ro._ZTIN6icu_6719ModulusSubstitutionE,"awG",@progbits,_ZTIN6icu_6719ModulusSubstitutionE,comdat
	.align 8
	.type	_ZTIN6icu_6719ModulusSubstitutionE, @object
	.size	_ZTIN6icu_6719ModulusSubstitutionE, 24
_ZTIN6icu_6719ModulusSubstitutionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719ModulusSubstitutionE
	.quad	_ZTIN6icu_6714NFSubstitutionE
	.weak	_ZTSN6icu_6724IntegralPartSubstitutionE
	.section	.rodata._ZTSN6icu_6724IntegralPartSubstitutionE,"aG",@progbits,_ZTSN6icu_6724IntegralPartSubstitutionE,comdat
	.align 32
	.type	_ZTSN6icu_6724IntegralPartSubstitutionE, @object
	.size	_ZTSN6icu_6724IntegralPartSubstitutionE, 36
_ZTSN6icu_6724IntegralPartSubstitutionE:
	.string	"N6icu_6724IntegralPartSubstitutionE"
	.weak	_ZTIN6icu_6724IntegralPartSubstitutionE
	.section	.data.rel.ro._ZTIN6icu_6724IntegralPartSubstitutionE,"awG",@progbits,_ZTIN6icu_6724IntegralPartSubstitutionE,comdat
	.align 8
	.type	_ZTIN6icu_6724IntegralPartSubstitutionE, @object
	.size	_ZTIN6icu_6724IntegralPartSubstitutionE, 24
_ZTIN6icu_6724IntegralPartSubstitutionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6724IntegralPartSubstitutionE
	.quad	_ZTIN6icu_6714NFSubstitutionE
	.weak	_ZTSN6icu_6726FractionalPartSubstitutionE
	.section	.rodata._ZTSN6icu_6726FractionalPartSubstitutionE,"aG",@progbits,_ZTSN6icu_6726FractionalPartSubstitutionE,comdat
	.align 32
	.type	_ZTSN6icu_6726FractionalPartSubstitutionE, @object
	.size	_ZTSN6icu_6726FractionalPartSubstitutionE, 38
_ZTSN6icu_6726FractionalPartSubstitutionE:
	.string	"N6icu_6726FractionalPartSubstitutionE"
	.weak	_ZTIN6icu_6726FractionalPartSubstitutionE
	.section	.data.rel.ro._ZTIN6icu_6726FractionalPartSubstitutionE,"awG",@progbits,_ZTIN6icu_6726FractionalPartSubstitutionE,comdat
	.align 8
	.type	_ZTIN6icu_6726FractionalPartSubstitutionE, @object
	.size	_ZTIN6icu_6726FractionalPartSubstitutionE, 24
_ZTIN6icu_6726FractionalPartSubstitutionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6726FractionalPartSubstitutionE
	.quad	_ZTIN6icu_6714NFSubstitutionE
	.weak	_ZTSN6icu_6725AbsoluteValueSubstitutionE
	.section	.rodata._ZTSN6icu_6725AbsoluteValueSubstitutionE,"aG",@progbits,_ZTSN6icu_6725AbsoluteValueSubstitutionE,comdat
	.align 32
	.type	_ZTSN6icu_6725AbsoluteValueSubstitutionE, @object
	.size	_ZTSN6icu_6725AbsoluteValueSubstitutionE, 37
_ZTSN6icu_6725AbsoluteValueSubstitutionE:
	.string	"N6icu_6725AbsoluteValueSubstitutionE"
	.weak	_ZTIN6icu_6725AbsoluteValueSubstitutionE
	.section	.data.rel.ro._ZTIN6icu_6725AbsoluteValueSubstitutionE,"awG",@progbits,_ZTIN6icu_6725AbsoluteValueSubstitutionE,comdat
	.align 8
	.type	_ZTIN6icu_6725AbsoluteValueSubstitutionE, @object
	.size	_ZTIN6icu_6725AbsoluteValueSubstitutionE, 24
_ZTIN6icu_6725AbsoluteValueSubstitutionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725AbsoluteValueSubstitutionE
	.quad	_ZTIN6icu_6714NFSubstitutionE
	.weak	_ZTSN6icu_6721NumeratorSubstitutionE
	.section	.rodata._ZTSN6icu_6721NumeratorSubstitutionE,"aG",@progbits,_ZTSN6icu_6721NumeratorSubstitutionE,comdat
	.align 32
	.type	_ZTSN6icu_6721NumeratorSubstitutionE, @object
	.size	_ZTSN6icu_6721NumeratorSubstitutionE, 33
_ZTSN6icu_6721NumeratorSubstitutionE:
	.string	"N6icu_6721NumeratorSubstitutionE"
	.weak	_ZTIN6icu_6721NumeratorSubstitutionE
	.section	.data.rel.ro._ZTIN6icu_6721NumeratorSubstitutionE,"awG",@progbits,_ZTIN6icu_6721NumeratorSubstitutionE,comdat
	.align 8
	.type	_ZTIN6icu_6721NumeratorSubstitutionE, @object
	.size	_ZTIN6icu_6721NumeratorSubstitutionE, 24
_ZTIN6icu_6721NumeratorSubstitutionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6721NumeratorSubstitutionE
	.quad	_ZTIN6icu_6714NFSubstitutionE
	.weak	_ZTVN6icu_6721SameValueSubstitutionE
	.section	.data.rel.ro.local._ZTVN6icu_6721SameValueSubstitutionE,"awG",@progbits,_ZTVN6icu_6721SameValueSubstitutionE,comdat
	.align 8
	.type	_ZTVN6icu_6721SameValueSubstitutionE, @object
	.size	_ZTVN6icu_6721SameValueSubstitutionE, 136
_ZTVN6icu_6721SameValueSubstitutionE:
	.quad	0
	.quad	_ZTIN6icu_6721SameValueSubstitutionE
	.quad	_ZN6icu_6721SameValueSubstitutionD1Ev
	.quad	_ZN6icu_6721SameValueSubstitutionD0Ev
	.quad	_ZNK6icu_6721SameValueSubstitution17getDynamicClassIDEv
	.quad	_ZNK6icu_6714NFSubstitutioneqERKS0_
	.quad	_ZN6icu_6714NFSubstitution10setDivisorEisR10UErrorCode
	.quad	_ZNK6icu_6714NFSubstitution8toStringERNS_13UnicodeStringE
	.quad	_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6714NFSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6721SameValueSubstitution15transformNumberEl
	.quad	_ZNK6icu_6721SameValueSubstitution15transformNumberEd
	.quad	_ZNK6icu_6714NFSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.quad	_ZNK6icu_6721SameValueSubstitution16composeRuleValueEdd
	.quad	_ZNK6icu_6721SameValueSubstitution14calcUpperBoundEd
	.quad	_ZNK6icu_6721SameValueSubstitution9tokenCharEv
	.quad	_ZNK6icu_6714NFSubstitution21isModulusSubstitutionEv
	.weak	_ZTVN6icu_6722MultiplierSubstitutionE
	.section	.data.rel.ro.local._ZTVN6icu_6722MultiplierSubstitutionE,"awG",@progbits,_ZTVN6icu_6722MultiplierSubstitutionE,comdat
	.align 8
	.type	_ZTVN6icu_6722MultiplierSubstitutionE, @object
	.size	_ZTVN6icu_6722MultiplierSubstitutionE, 136
_ZTVN6icu_6722MultiplierSubstitutionE:
	.quad	0
	.quad	_ZTIN6icu_6722MultiplierSubstitutionE
	.quad	_ZN6icu_6722MultiplierSubstitutionD1Ev
	.quad	_ZN6icu_6722MultiplierSubstitutionD0Ev
	.quad	_ZNK6icu_6722MultiplierSubstitution17getDynamicClassIDEv
	.quad	_ZNK6icu_6722MultiplierSubstitutioneqERKNS_14NFSubstitutionE
	.quad	_ZN6icu_6722MultiplierSubstitution10setDivisorEisR10UErrorCode
	.quad	_ZNK6icu_6714NFSubstitution8toStringERNS_13UnicodeStringE
	.quad	_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6714NFSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6722MultiplierSubstitution15transformNumberEl
	.quad	_ZNK6icu_6722MultiplierSubstitution15transformNumberEd
	.quad	_ZNK6icu_6714NFSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.quad	_ZNK6icu_6722MultiplierSubstitution16composeRuleValueEdd
	.quad	_ZNK6icu_6722MultiplierSubstitution14calcUpperBoundEd
	.quad	_ZNK6icu_6722MultiplierSubstitution9tokenCharEv
	.quad	_ZNK6icu_6714NFSubstitution21isModulusSubstitutionEv
	.weak	_ZTVN6icu_6719ModulusSubstitutionE
	.section	.data.rel.ro.local._ZTVN6icu_6719ModulusSubstitutionE,"awG",@progbits,_ZTVN6icu_6719ModulusSubstitutionE,comdat
	.align 8
	.type	_ZTVN6icu_6719ModulusSubstitutionE, @object
	.size	_ZTVN6icu_6719ModulusSubstitutionE, 136
_ZTVN6icu_6719ModulusSubstitutionE:
	.quad	0
	.quad	_ZTIN6icu_6719ModulusSubstitutionE
	.quad	_ZN6icu_6719ModulusSubstitutionD1Ev
	.quad	_ZN6icu_6719ModulusSubstitutionD0Ev
	.quad	_ZNK6icu_6719ModulusSubstitution17getDynamicClassIDEv
	.quad	_ZNK6icu_6719ModulusSubstitutioneqERKNS_14NFSubstitutionE
	.quad	_ZN6icu_6719ModulusSubstitution10setDivisorEisR10UErrorCode
	.quad	_ZNK6icu_6719ModulusSubstitution8toStringERNS_13UnicodeStringE
	.quad	_ZNK6icu_6719ModulusSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6719ModulusSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6719ModulusSubstitution15transformNumberEl
	.quad	_ZNK6icu_6719ModulusSubstitution15transformNumberEd
	.quad	_ZNK6icu_6719ModulusSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.quad	_ZNK6icu_6719ModulusSubstitution16composeRuleValueEdd
	.quad	_ZNK6icu_6719ModulusSubstitution14calcUpperBoundEd
	.quad	_ZNK6icu_6719ModulusSubstitution9tokenCharEv
	.quad	_ZNK6icu_6719ModulusSubstitution21isModulusSubstitutionEv
	.weak	_ZTVN6icu_6724IntegralPartSubstitutionE
	.section	.data.rel.ro.local._ZTVN6icu_6724IntegralPartSubstitutionE,"awG",@progbits,_ZTVN6icu_6724IntegralPartSubstitutionE,comdat
	.align 8
	.type	_ZTVN6icu_6724IntegralPartSubstitutionE, @object
	.size	_ZTVN6icu_6724IntegralPartSubstitutionE, 136
_ZTVN6icu_6724IntegralPartSubstitutionE:
	.quad	0
	.quad	_ZTIN6icu_6724IntegralPartSubstitutionE
	.quad	_ZN6icu_6724IntegralPartSubstitutionD1Ev
	.quad	_ZN6icu_6724IntegralPartSubstitutionD0Ev
	.quad	_ZNK6icu_6724IntegralPartSubstitution17getDynamicClassIDEv
	.quad	_ZNK6icu_6714NFSubstitutioneqERKS0_
	.quad	_ZN6icu_6714NFSubstitution10setDivisorEisR10UErrorCode
	.quad	_ZNK6icu_6714NFSubstitution8toStringERNS_13UnicodeStringE
	.quad	_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6714NFSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6724IntegralPartSubstitution15transformNumberEl
	.quad	_ZNK6icu_6724IntegralPartSubstitution15transformNumberEd
	.quad	_ZNK6icu_6714NFSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.quad	_ZNK6icu_6724IntegralPartSubstitution16composeRuleValueEdd
	.quad	_ZNK6icu_6724IntegralPartSubstitution14calcUpperBoundEd
	.quad	_ZNK6icu_6724IntegralPartSubstitution9tokenCharEv
	.quad	_ZNK6icu_6714NFSubstitution21isModulusSubstitutionEv
	.weak	_ZTVN6icu_6726FractionalPartSubstitutionE
	.section	.data.rel.ro.local._ZTVN6icu_6726FractionalPartSubstitutionE,"awG",@progbits,_ZTVN6icu_6726FractionalPartSubstitutionE,comdat
	.align 8
	.type	_ZTVN6icu_6726FractionalPartSubstitutionE, @object
	.size	_ZTVN6icu_6726FractionalPartSubstitutionE, 136
_ZTVN6icu_6726FractionalPartSubstitutionE:
	.quad	0
	.quad	_ZTIN6icu_6726FractionalPartSubstitutionE
	.quad	_ZN6icu_6726FractionalPartSubstitutionD1Ev
	.quad	_ZN6icu_6726FractionalPartSubstitutionD0Ev
	.quad	_ZNK6icu_6726FractionalPartSubstitution17getDynamicClassIDEv
	.quad	_ZNK6icu_6726FractionalPartSubstitutioneqERKNS_14NFSubstitutionE
	.quad	_ZN6icu_6714NFSubstitution10setDivisorEisR10UErrorCode
	.quad	_ZNK6icu_6714NFSubstitution8toStringERNS_13UnicodeStringE
	.quad	_ZNK6icu_6726FractionalPartSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6726FractionalPartSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6726FractionalPartSubstitution15transformNumberEl
	.quad	_ZNK6icu_6726FractionalPartSubstitution15transformNumberEd
	.quad	_ZNK6icu_6726FractionalPartSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.quad	_ZNK6icu_6726FractionalPartSubstitution16composeRuleValueEdd
	.quad	_ZNK6icu_6726FractionalPartSubstitution14calcUpperBoundEd
	.quad	_ZNK6icu_6726FractionalPartSubstitution9tokenCharEv
	.quad	_ZNK6icu_6714NFSubstitution21isModulusSubstitutionEv
	.weak	_ZTVN6icu_6725AbsoluteValueSubstitutionE
	.section	.data.rel.ro.local._ZTVN6icu_6725AbsoluteValueSubstitutionE,"awG",@progbits,_ZTVN6icu_6725AbsoluteValueSubstitutionE,comdat
	.align 8
	.type	_ZTVN6icu_6725AbsoluteValueSubstitutionE, @object
	.size	_ZTVN6icu_6725AbsoluteValueSubstitutionE, 136
_ZTVN6icu_6725AbsoluteValueSubstitutionE:
	.quad	0
	.quad	_ZTIN6icu_6725AbsoluteValueSubstitutionE
	.quad	_ZN6icu_6725AbsoluteValueSubstitutionD1Ev
	.quad	_ZN6icu_6725AbsoluteValueSubstitutionD0Ev
	.quad	_ZNK6icu_6725AbsoluteValueSubstitution17getDynamicClassIDEv
	.quad	_ZNK6icu_6714NFSubstitutioneqERKS0_
	.quad	_ZN6icu_6714NFSubstitution10setDivisorEisR10UErrorCode
	.quad	_ZNK6icu_6714NFSubstitution8toStringERNS_13UnicodeStringE
	.quad	_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6714NFSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEl
	.quad	_ZNK6icu_6725AbsoluteValueSubstitution15transformNumberEd
	.quad	_ZNK6icu_6714NFSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.quad	_ZNK6icu_6725AbsoluteValueSubstitution16composeRuleValueEdd
	.quad	_ZNK6icu_6725AbsoluteValueSubstitution14calcUpperBoundEd
	.quad	_ZNK6icu_6725AbsoluteValueSubstitution9tokenCharEv
	.quad	_ZNK6icu_6714NFSubstitution21isModulusSubstitutionEv
	.weak	_ZTVN6icu_6721NumeratorSubstitutionE
	.section	.data.rel.ro.local._ZTVN6icu_6721NumeratorSubstitutionE,"awG",@progbits,_ZTVN6icu_6721NumeratorSubstitutionE,comdat
	.align 8
	.type	_ZTVN6icu_6721NumeratorSubstitutionE, @object
	.size	_ZTVN6icu_6721NumeratorSubstitutionE, 136
_ZTVN6icu_6721NumeratorSubstitutionE:
	.quad	0
	.quad	_ZTIN6icu_6721NumeratorSubstitutionE
	.quad	_ZN6icu_6721NumeratorSubstitutionD1Ev
	.quad	_ZN6icu_6721NumeratorSubstitutionD0Ev
	.quad	_ZNK6icu_6721NumeratorSubstitution17getDynamicClassIDEv
	.quad	_ZNK6icu_6721NumeratorSubstitutioneqERKNS_14NFSubstitutionE
	.quad	_ZN6icu_6714NFSubstitution10setDivisorEisR10UErrorCode
	.quad	_ZNK6icu_6714NFSubstitution8toStringERNS_13UnicodeStringE
	.quad	_ZNK6icu_6721NumeratorSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6721NumeratorSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6721NumeratorSubstitution15transformNumberEl
	.quad	_ZNK6icu_6721NumeratorSubstitution15transformNumberEd
	.quad	_ZNK6icu_6721NumeratorSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.quad	_ZNK6icu_6721NumeratorSubstitution16composeRuleValueEdd
	.quad	_ZNK6icu_6721NumeratorSubstitution14calcUpperBoundEd
	.quad	_ZNK6icu_6721NumeratorSubstitution9tokenCharEv
	.quad	_ZNK6icu_6714NFSubstitution21isModulusSubstitutionEv
	.weak	_ZTVN6icu_6714NFSubstitutionE
	.section	.data.rel.ro._ZTVN6icu_6714NFSubstitutionE,"awG",@progbits,_ZTVN6icu_6714NFSubstitutionE,comdat
	.align 8
	.type	_ZTVN6icu_6714NFSubstitutionE, @object
	.size	_ZTVN6icu_6714NFSubstitutionE, 136
_ZTVN6icu_6714NFSubstitutionE:
	.quad	0
	.quad	_ZTIN6icu_6714NFSubstitutionE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_6714NFSubstitution17getDynamicClassIDEv
	.quad	_ZNK6icu_6714NFSubstitutioneqERKS0_
	.quad	_ZN6icu_6714NFSubstitution10setDivisorEisR10UErrorCode
	.quad	_ZNK6icu_6714NFSubstitution8toStringERNS_13UnicodeStringE
	.quad	_ZNK6icu_6714NFSubstitution14doSubstitutionElRNS_13UnicodeStringEiiR10UErrorCode
	.quad	_ZNK6icu_6714NFSubstitution14doSubstitutionEdRNS_13UnicodeStringEiiR10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6714NFSubstitution7doParseERKNS_13UnicodeStringERNS_13ParsePositionEddajRNS_11FormattableE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6714NFSubstitution21isModulusSubstitutionEv
	.globl	_ZN6icu_6721NumeratorSubstitution4LTLTE
	.section	.rodata
	.align 2
	.type	_ZN6icu_6721NumeratorSubstitution4LTLTE, @object
	.size	_ZN6icu_6721NumeratorSubstitution4LTLTE, 4
_ZN6icu_6721NumeratorSubstitution4LTLTE:
	.value	60
	.value	60
	.local	_ZZN6icu_6721NumeratorSubstitution16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6721NumeratorSubstitution16getStaticClassIDEvE7classID,1,1
	.local	_ZZN6icu_6725AbsoluteValueSubstitution16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6725AbsoluteValueSubstitution16getStaticClassIDEvE7classID,1,1
	.local	_ZZN6icu_6726FractionalPartSubstitution16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6726FractionalPartSubstitution16getStaticClassIDEvE7classID,1,1
	.local	_ZZN6icu_6724IntegralPartSubstitution16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6724IntegralPartSubstitution16getStaticClassIDEvE7classID,1,1
	.local	_ZZN6icu_6719ModulusSubstitution16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6719ModulusSubstitution16getStaticClassIDEvE7classID,1,1
	.local	_ZZN6icu_6722MultiplierSubstitution16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6722MultiplierSubstitution16getStaticClassIDEvE7classID,1,1
	.local	_ZZN6icu_6721SameValueSubstitution16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6721SameValueSubstitution16getStaticClassIDEvE7classID,1,1
	.local	_ZZN6icu_6714NFSubstitution16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6714NFSubstitution16getStaticClassIDEvE7classID,1,1
	.align 2
	.type	_ZL19gGreaterGreaterThan, @object
	.size	_ZL19gGreaterGreaterThan, 6
_ZL19gGreaterGreaterThan:
	.value	62
	.value	62
	.value	0
	.align 8
	.type	_ZL26gGreaterGreaterGreaterThan, @object
	.size	_ZL26gGreaterGreaterGreaterThan, 8
_ZL26gGreaterGreaterGreaterThan:
	.value	62
	.value	62
	.value	62
	.value	0
	.align 2
	.type	_ZL13gEqualsEquals, @object
	.size	_ZL13gEqualsEquals, 6
_ZL13gEqualsEquals:
	.value	61
	.value	61
	.value	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	4294967295
	.long	2146435071
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	0
	.long	1072693248
	.align 8
.LC4:
	.long	0
	.long	1076101120
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
