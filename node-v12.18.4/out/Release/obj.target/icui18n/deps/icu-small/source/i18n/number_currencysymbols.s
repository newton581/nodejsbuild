	.file	"number_currencysymbols.cpp"
	.text
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3315:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3315:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3318:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L15
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3
	cmpb	$0, 12(%rbx)
	jne	.L16
.L7:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L7
	.cfi_endproc
.LFE3318:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3321:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L19
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3321:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3324:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L22
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3324:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L28
.L24:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L29
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3326:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3327:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3327:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3328:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3328:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3329:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3329:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3330:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3330:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3331:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3331:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3332:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L45
	testl	%edx, %edx
	jle	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L48
.L37:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L37
	.cfi_endproc
.LFE3332:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L52
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L52
	testl	%r12d, %r12d
	jg	.L59
	cmpb	$0, 12(%rbx)
	jne	.L60
.L54:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L54
.L60:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3333:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L62
	movq	(%rdi), %r8
.L63:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L66
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L66
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3334:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3335:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L73
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3335:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3336:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3336:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3337:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3337:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3338:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3338:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3340:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3340:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3342:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3342:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15CurrencySymbolsC2ENS_12CurrencyUnitERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_676number4impl15CurrencySymbolsC2ENS_12CurrencyUnitERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_676number4impl15CurrencySymbolsC2ENS_12CurrencyUnitERKNS_6LocaleER10UErrorCode:
.LFB2834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6712CurrencyUnitC1ERKS0_@PLT
	movq	40(%r13), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	leaq	53(%rbx), %rax
	movq	-64(%rbp), %rsi
	movl	-56(%rbp), %edx
	movq	%rax, 40(%rbx)
	xorl	%eax, %eax
	leaq	40(%rbx), %rdi
	movq	%r12, %rcx
	movl	$0, 96(%rbx)
	movl	$40, 48(%rbx)
	movw	%ax, 52(%rbx)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movl	$2, %ecx
	movq	%rax, 104(%rbx)
	leaq	104(%rbx), %rdi
	movq	%rax, 168(%rbx)
	movw	%dx, 112(%rbx)
	movw	%cx, 176(%rbx)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	168(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L82:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2834:
	.size	_ZN6icu_676number4impl15CurrencySymbolsC2ENS_12CurrencyUnitERKNS_6LocaleER10UErrorCode, .-_ZN6icu_676number4impl15CurrencySymbolsC2ENS_12CurrencyUnitERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_676number4impl15CurrencySymbolsC1ENS_12CurrencyUnitERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_676number4impl15CurrencySymbolsC1ENS_12CurrencyUnitERKNS_6LocaleER10UErrorCode,_ZN6icu_676number4impl15CurrencySymbolsC2ENS_12CurrencyUnitERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15CurrencySymbols10getIsoCodeEv
	.type	_ZNK6icu_676number4impl15CurrencySymbols10getIsoCodeEv, @function
_ZNK6icu_676number4impl15CurrencySymbols10getIsoCodeEv:
.LFB2842:
	.cfi_startproc
	endbr64
	leaq	28(%rdi), %rax
	ret
	.cfi_endproc
.LFE2842:
	.size	_ZNK6icu_676number4impl15CurrencySymbols10getIsoCodeEv, .-_ZNK6icu_676number4impl15CurrencySymbols10getIsoCodeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15CurrencySymbols23getNarrowCurrencySymbolER10UErrorCode
	.type	_ZNK6icu_676number4impl15CurrencySymbols23getNarrowCurrencySymbolER10UErrorCode, @function
_ZNK6icu_676number4impl15CurrencySymbols23getNarrowCurrencySymbolER10UErrorCode:
.LFB2843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	28(%rsi), %r13
	pushq	%r12
	leaq	-36(%rbp), %r8
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	40(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -36(%rbp)
	call	ucurr_getName_67@PLT
	cmpq	%rax, %r13
	je	.L89
	movl	-36(%rbp), %ecx
	leaq	-32(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
.L84:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	jmp	.L84
.L90:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2843:
	.size	_ZNK6icu_676number4impl15CurrencySymbols23getNarrowCurrencySymbolER10UErrorCode, .-_ZNK6icu_676number4impl15CurrencySymbols23getNarrowCurrencySymbolER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15CurrencySymbols17getCurrencySymbolER10UErrorCode
	.type	_ZNK6icu_676number4impl15CurrencySymbols17getCurrencySymbolER10UErrorCode, @function
_ZNK6icu_676number4impl15CurrencySymbols17getCurrencySymbolER10UErrorCode:
.LFB2844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	$1, 112(%rsi)
	jne	.L92
	addq	$104, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L91:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	leaq	28(%rsi), %r13
	movq	40(%rsi), %rsi
	movq	%rdx, %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-36(%rbp), %r8
	movq	%r13, %rdi
	movl	$0, -36(%rbp)
	call	ucurr_getName_67@PLT
	cmpq	%rax, %r13
	je	.L99
	movl	-36(%rbp), %ecx
	leaq	-32(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	jmp	.L91
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2844:
	.size	_ZNK6icu_676number4impl15CurrencySymbols17getCurrencySymbolER10UErrorCode, .-_ZNK6icu_676number4impl15CurrencySymbols17getCurrencySymbolER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15CurrencySymbols10loadSymbolE14UCurrNameStyleR10UErrorCode
	.type	_ZNK6icu_676number4impl15CurrencySymbols10loadSymbolE14UCurrNameStyleR10UErrorCode, @function
_ZNK6icu_676number4impl15CurrencySymbols10loadSymbolE14UCurrNameStyleR10UErrorCode:
.LFB2845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	28(%rsi), %r13
	pushq	%r12
	leaq	-36(%rbp), %r8
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	40(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -36(%rbp)
	call	ucurr_getName_67@PLT
	cmpq	%rax, %r13
	je	.L105
	movl	-36(%rbp), %ecx
	leaq	-32(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
.L100:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	jmp	.L100
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2845:
	.size	_ZNK6icu_676number4impl15CurrencySymbols10loadSymbolE14UCurrNameStyleR10UErrorCode, .-_ZNK6icu_676number4impl15CurrencySymbols10loadSymbolE14UCurrNameStyleR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15CurrencySymbols21getIntlCurrencySymbolER10UErrorCode
	.type	_ZNK6icu_676number4impl15CurrencySymbols21getIntlCurrencySymbolER10UErrorCode, @function
_ZNK6icu_676number4impl15CurrencySymbols21getIntlCurrencySymbolER10UErrorCode:
.LFB2846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	testb	$1, 176(%rsi)
	jne	.L108
	addq	$168, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	addq	$28, %rsi
	movl	$3, %edx
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2846:
	.size	_ZNK6icu_676number4impl15CurrencySymbols21getIntlCurrencySymbolER10UErrorCode, .-_ZNK6icu_676number4impl15CurrencySymbols21getIntlCurrencySymbolER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15CurrencySymbols13getPluralNameENS_14StandardPlural4FormER10UErrorCode
	.type	_ZNK6icu_676number4impl15CurrencySymbols13getPluralNameENS_14StandardPlural4FormER10UErrorCode, @function
_ZNK6icu_676number4impl15CurrencySymbols13getPluralNameENS_14StandardPlural4FormER10UErrorCode:
.LFB2847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	28(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	%edx, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -52(%rbp)
	call	_ZN6icu_6714StandardPlural10getKeywordENS0_4FormE@PLT
	movq	40(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r14, %r9
	movq	%rax, %rcx
	leaq	-52(%rbp), %r8
	movq	%r13, %rdi
	call	ucurr_getPluralName_67@PLT
	cmpq	%rax, %r13
	je	.L116
	movl	-52(%rbp), %ecx
	leaq	-48(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
.L111:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	jmp	.L111
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2847:
	.size	_ZNK6icu_676number4impl15CurrencySymbols13getPluralNameENS_14StandardPlural4FormER10UErrorCode, .-_ZNK6icu_676number4impl15CurrencySymbols13getPluralNameENS_14StandardPlural4FormER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl15resolveCurrencyERKNS1_23DecimalFormatPropertiesERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_676number4impl15resolveCurrencyERKNS1_23DecimalFormatPropertiesERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_676number4impl15resolveCurrencyERKNS1_23DecimalFormatPropertiesERKNS_6LocaleER10UErrorCode:
.LFB2848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 8(%rsi)
	jne	.L119
	addq	$16, %rsi
	call	_ZN6icu_6712CurrencyUnitC1ERKS0_@PLT
.L118:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movq	40(%rdx), %rdi
	leaq	-48(%rbp), %rbx
	movq	%rcx, %r13
	movl	$4, %edx
	leaq	-60(%rbp), %rcx
	movq	%rbx, %rsi
	movl	$0, -60(%rbp)
	movq	$0, -48(%rbp)
	call	ucurr_forLocale_67@PLT
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L126
	movq	%r12, %rdi
	call	_ZN6icu_6712CurrencyUnitC1Ev@PLT
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L126:
	leaq	-56(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rbx, -56(%rbp)
	call	_ZN6icu_6712CurrencyUnitC1ENS_14ConstChar16PtrER10UErrorCode@PLT
	jmp	.L118
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2848:
	.size	_ZN6icu_676number4impl15resolveCurrencyERKNS1_23DecimalFormatPropertiesERKNS_6LocaleER10UErrorCode, .-_ZN6icu_676number4impl15resolveCurrencyERKNS1_23DecimalFormatPropertiesERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15CurrencySymbolsC2ENS_12CurrencyUnitERKNS_6LocaleERKNS_20DecimalFormatSymbolsER10UErrorCode
	.type	_ZN6icu_676number4impl15CurrencySymbolsC2ENS_12CurrencyUnitERKNS_6LocaleERKNS_20DecimalFormatSymbolsER10UErrorCode, @function
_ZN6icu_676number4impl15CurrencySymbolsC2ENS_12CurrencyUnitERKNS_6LocaleERKNS_20DecimalFormatSymbolsER10UErrorCode:
.LFB2840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6712CurrencyUnitC1ERKS0_@PLT
	leaq	8(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6712CurrencyUnitC1ERKS0_@PLT
	movq	40(%r15), %rsi
	leaq	-112(%rbp), %rdi
	leaq	104(%rbx), %r15
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	leaq	53(%rbx), %rax
	movq	-112(%rbp), %rsi
	movl	-104(%rbp), %edx
	movq	%rax, 40(%rbx)
	xorl	%eax, %eax
	movq	%r14, %rcx
	leaq	40(%rbx), %rdi
	movl	$0, 96(%rbx)
	leaq	168(%rbx), %r14
	movl	$40, 48(%rbx)
	movw	%ax, 52(%rbx)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movq	%r15, %rdi
	movl	$2, %ecx
	movq	%rax, 104(%rbx)
	movw	%dx, 112(%rbx)
	movq	%rax, 168(%rbx)
	movw	%cx, 176(%rbx)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	cmpb	$0, 2808(%r12)
	jne	.L132
	cmpb	$0, 2809(%r12)
	jne	.L133
.L127:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L134
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	leaq	520(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	cmpb	$0, 2809(%r12)
	je	.L127
.L133:
	leaq	584(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L127
.L134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2840:
	.size	_ZN6icu_676number4impl15CurrencySymbolsC2ENS_12CurrencyUnitERKNS_6LocaleERKNS_20DecimalFormatSymbolsER10UErrorCode, .-_ZN6icu_676number4impl15CurrencySymbolsC2ENS_12CurrencyUnitERKNS_6LocaleERKNS_20DecimalFormatSymbolsER10UErrorCode
	.globl	_ZN6icu_676number4impl15CurrencySymbolsC1ENS_12CurrencyUnitERKNS_6LocaleERKNS_20DecimalFormatSymbolsER10UErrorCode
	.set	_ZN6icu_676number4impl15CurrencySymbolsC1ENS_12CurrencyUnitERKNS_6LocaleERKNS_20DecimalFormatSymbolsER10UErrorCode,_ZN6icu_676number4impl15CurrencySymbolsC2ENS_12CurrencyUnitERKNS_6LocaleERKNS_20DecimalFormatSymbolsER10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
