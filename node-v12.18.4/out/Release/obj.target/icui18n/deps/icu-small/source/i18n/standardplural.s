	.file	"standardplural.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714StandardPlural10getKeywordENS0_4FormE
	.type	_ZN6icu_6714StandardPlural10getKeywordENS0_4FormE, @function
_ZN6icu_6714StandardPlural10getKeywordENS0_4FormE:
.LFB2286:
	.cfi_startproc
	endbr64
	movslq	%edi, %rdi
	leaq	_ZN6icu_67L9gKeywordsE(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.cfi_endproc
.LFE2286:
	.size	_ZN6icu_6714StandardPlural10getKeywordENS0_4FormE, .-_ZN6icu_6714StandardPlural10getKeywordENS0_4FormE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"any"
.LC1:
	.string	"ther"
.LC2:
	.string	"ero"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringEPKc
	.type	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringEPKc, @function
_ZN6icu_6714StandardPlural25indexOrNegativeFromStringEPKc:
.LFB2287:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	movq	%rdi, %rdx
	leaq	1(%rdi), %r8
	subl	$102, %eax
	cmpb	$20, %al
	ja	.L19
	leaq	.L6(%rip), %rcx
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L6:
	.long	.L10-.L6
	.long	.L19-.L6
	.long	.L19-.L6
	.long	.L19-.L6
	.long	.L19-.L6
	.long	.L19-.L6
	.long	.L19-.L6
	.long	.L9-.L6
	.long	.L19-.L6
	.long	.L8-.L6
	.long	.L19-.L6
	.long	.L19-.L6
	.long	.L19-.L6
	.long	.L19-.L6
	.long	.L7-.L6
	.long	.L19-.L6
	.long	.L19-.L6
	.long	.L19-.L6
	.long	.L19-.L6
	.long	.L19-.L6
	.long	.L5-.L6
	.text
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$5, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%r8, %rsi
	movl	$5, %eax
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L3
	cmpb	$110, 1(%rdx)
	jne	.L19
	cmpb	$101, 1(%r8)
	je	.L21
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$-1, %eax
.L3:
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$4, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	setne	%al
	movzbl	%al, %eax
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	cmpb	$101, 1(%rdi)
	jne	.L19
	cmpb	$119, 1(%r8)
	jne	.L19
	cmpb	$0, 2(%r8)
	jne	.L19
	movl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$4, %ecx
	leaq	.LC0(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andl	$5, %eax
	subl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	cmpb	$119, 1(%rdi)
	jne	.L19
	cmpb	$111, 1(%r8)
	jne	.L19
	cmpb	$0, 2(%r8)
	jne	.L19
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	cmpb	$0, 2(%r8)
	jne	.L19
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2287:
	.size	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringEPKc, .-_ZN6icu_6714StandardPlural25indexOrNegativeFromStringEPKc
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringERKNS_13UnicodeStringE
	.type	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringERKNS_13UnicodeStringE, @function
_ZN6icu_6714StandardPlural25indexOrNegativeFromStringERKNS_13UnicodeStringE:
.LFB2288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L23
	sarl	$5, %eax
	cmpl	$4, %eax
	je	.L24
	cmpl	$5, %eax
	je	.L25
	cmpl	$3, %eax
	je	.L26
.L34:
	movl	$-1, %eax
.L22:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movl	$5, %r9d
	xorl	%r8d, %r8d
	movl	$5, %edx
	xorl	%esi, %esi
	leaq	_ZN6icu_67L6gOtherE(%rip), %rcx
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L6gOtherE(%rip), %rdx
	testb	%al, %al
	jne	.L34
	addq	$8, %rsp
	movl	$5, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movl	12(%rdi), %edx
	cmpl	$4, %edx
	je	.L24
	cmpl	$5, %edx
	je	.L25
	movl	$-1, %eax
	cmpl	$3, %edx
	jne	.L22
.L26:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	movl	$3, %edx
	xorl	%esi, %esi
	leaq	_ZN6icu_67L4gOneE(%rip), %rcx
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movl	%eax, %edx
	leaq	_ZN6icu_67L4gOneE(%rip), %rax
	movl	$1, %eax
	testb	%dl, %dl
	je	.L22
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L28
	sarl	$5, %edx
.L29:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	_ZN6icu_67L4gTwoE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movl	%eax, %edx
	leaq	_ZN6icu_67L4gTwoE(%rip), %rax
	movl	$2, %eax
	testb	%dl, %dl
	je	.L22
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L30
	movswl	%ax, %edx
	sarl	$5, %edx
.L31:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	_ZN6icu_67L4gFewE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movl	%eax, %edx
	leaq	_ZN6icu_67L4gFewE(%rip), %rax
	movl	$3, %eax
	testb	%dl, %dl
	je	.L22
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$4, %r9d
	xorl	%r8d, %r8d
	movl	$4, %edx
	xorl	%esi, %esi
	leaq	_ZN6icu_67L5gManyE(%rip), %rcx
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movl	%eax, %edx
	leaq	_ZN6icu_67L5gManyE(%rip), %rax
	movl	$4, %eax
	testb	%dl, %dl
	je	.L22
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L32
	sarl	$5, %edx
.L33:
	movl	$4, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	_ZN6icu_67L5gZeroE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L5gZeroE(%rip), %rdx
	testb	%al, %al
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	12(%r12), %edx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L28:
	movl	12(%r12), %edx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	movl	12(%r12), %edx
	jmp	.L31
	.cfi_endproc
.LFE2288:
	.size	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringERKNS_13UnicodeStringE, .-_ZN6icu_6714StandardPlural25indexOrNegativeFromStringERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714StandardPlural15indexFromStringEPKcR10UErrorCode
	.type	_ZN6icu_6714StandardPlural15indexFromStringEPKcR10UErrorCode, @function
_ZN6icu_6714StandardPlural15indexFromStringEPKcR10UErrorCode:
.LFB2289:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rdi, %rdx
	movq	%rsi, %r8
	movl	$5, %eax
	testl	%ecx, %ecx
	jg	.L60
	movzbl	(%rdi), %eax
	leaq	1(%rdi), %r9
	subl	$102, %eax
	cmpb	$20, %al
	ja	.L62
	leaq	.L64(%rip), %rcx
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L64:
	.long	.L68-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L67-.L64
	.long	.L62-.L64
	.long	.L66-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L65-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L62-.L64
	.long	.L63-.L64
	.text
	.p2align 4,,10
	.p2align 3
.L65:
	cmpb	$119, 1(%rdi)
	jne	.L62
	cmpb	$111, 1(%r9)
	jne	.L62
	cmpb	$0, 2(%r9)
	jne	.L62
	movl	$2, %eax
.L60:
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	cmpb	$101, 1(%rdi)
	jne	.L62
	cmpb	$119, 1(%r9)
	je	.L85
.L62:
	movl	$1, (%r8)
	movl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$4, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%r9, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	testl	%eax, %eax
	jne	.L62
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$5, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%r9, %rsi
	movl	$5, %eax
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L60
	cmpb	$110, 1(%rdx)
	jne	.L62
	cmpb	$101, 1(%r9)
	jne	.L62
	cmpb	$0, 2(%r9)
	jne	.L62
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$4, %ecx
	leaq	.LC0(%rip), %rdi
	movq	%r9, %rsi
	movl	$4, %eax
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L62
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	cmpb	$0, 2(%r9)
	jne	.L62
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE2289:
	.size	_ZN6icu_6714StandardPlural15indexFromStringEPKcR10UErrorCode, .-_ZN6icu_6714StandardPlural15indexFromStringEPKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714StandardPlural15indexFromStringERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6714StandardPlural15indexFromStringERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6714StandardPlural15indexFromStringERKNS_13UnicodeStringER10UErrorCode:
.LFB2290:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L138
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movswl	8(%rdi), %eax
	movq	%rsi, %rbx
	testw	%ax, %ax
	js	.L89
	sarl	$5, %eax
	cmpl	$4, %eax
	je	.L90
.L137:
	cmpl	$5, %eax
	je	.L91
	cmpl	$3, %eax
	je	.L139
.L93:
	movl	$1, (%rbx)
.L135:
	movl	$5, %eax
.L86:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movl	12(%rdi), %eax
	cmpl	$4, %eax
	jne	.L137
.L90:
	movl	$4, %r9d
	xorl	%r8d, %r8d
	movl	$4, %edx
	xorl	%esi, %esi
	leaq	_ZN6icu_67L5gManyE(%rip), %rcx
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movl	%eax, %edx
	leaq	_ZN6icu_67L5gManyE(%rip), %rax
	movl	$4, %eax
	testb	%dl, %dl
	je	.L86
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L98
	movswl	%ax, %edx
	sarl	$5, %edx
.L99:
	movl	$4, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	_ZN6icu_67L5gZeroE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movl	%eax, %edx
	leaq	_ZN6icu_67L5gZeroE(%rip), %rax
	xorl	%eax, %eax
	testb	%dl, %dl
	je	.L86
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$3, %r9d
	xorl	%r8d, %r8d
	movl	$3, %edx
	xorl	%esi, %esi
	leaq	_ZN6icu_67L4gOneE(%rip), %rcx
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movl	%eax, %edx
	leaq	_ZN6icu_67L4gOneE(%rip), %rax
	movl	$1, %eax
	testb	%dl, %dl
	je	.L86
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L94
	movswl	%ax, %edx
	sarl	$5, %edx
.L95:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	_ZN6icu_67L4gTwoE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movl	%eax, %edx
	leaq	_ZN6icu_67L4gTwoE(%rip), %rax
	movl	$2, %eax
	testb	%dl, %dl
	je	.L86
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L96
	movswl	%ax, %edx
	sarl	$5, %edx
.L97:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	_ZN6icu_67L4gFewE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movl	%eax, %edx
	leaq	_ZN6icu_67L4gFewE(%rip), %rax
	movl	$3, %eax
	testb	%dl, %dl
	je	.L86
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L91:
	movl	$5, %r9d
	xorl	%r8d, %r8d
	movl	$5, %edx
	xorl	%esi, %esi
	leaq	_ZN6icu_67L6gOtherE(%rip), %rcx
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L6gOtherE(%rip), %rdx
	testb	%al, %al
	je	.L135
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L94:
	movl	12(%r12), %edx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L98:
	movl	12(%r12), %edx
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L96:
	movl	12(%r12), %edx
	jmp	.L97
	.cfi_endproc
.LFE2290:
	.size	_ZN6icu_6714StandardPlural15indexFromStringERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6714StandardPlural15indexFromStringERKNS_13UnicodeStringER10UErrorCode
	.section	.rodata
	.align 8
	.type	_ZN6icu_67L6gOtherE, @object
	.size	_ZN6icu_67L6gOtherE, 10
_ZN6icu_67L6gOtherE:
	.value	111
	.value	116
	.value	104
	.value	101
	.value	114
	.align 8
	.type	_ZN6icu_67L5gManyE, @object
	.size	_ZN6icu_67L5gManyE, 8
_ZN6icu_67L5gManyE:
	.value	109
	.value	97
	.value	110
	.value	121
	.align 2
	.type	_ZN6icu_67L4gFewE, @object
	.size	_ZN6icu_67L4gFewE, 6
_ZN6icu_67L4gFewE:
	.value	102
	.value	101
	.value	119
	.align 2
	.type	_ZN6icu_67L4gTwoE, @object
	.size	_ZN6icu_67L4gTwoE, 6
_ZN6icu_67L4gTwoE:
	.value	116
	.value	119
	.value	111
	.align 2
	.type	_ZN6icu_67L4gOneE, @object
	.size	_ZN6icu_67L4gOneE, 6
_ZN6icu_67L4gOneE:
	.value	111
	.value	110
	.value	101
	.align 8
	.type	_ZN6icu_67L5gZeroE, @object
	.size	_ZN6icu_67L5gZeroE, 8
_ZN6icu_67L5gZeroE:
	.value	122
	.value	101
	.value	114
	.value	111
	.section	.rodata.str1.1
.LC3:
	.string	"zero"
.LC4:
	.string	"one"
.LC5:
	.string	"two"
.LC6:
	.string	"few"
.LC7:
	.string	"many"
.LC8:
	.string	"other"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZN6icu_67L9gKeywordsE, @object
	.size	_ZN6icu_67L9gKeywordsE, 48
_ZN6icu_67L9gKeywordsE:
	.quad	.LC3
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
