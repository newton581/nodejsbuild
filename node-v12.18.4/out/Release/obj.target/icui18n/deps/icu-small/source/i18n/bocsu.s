	.file	"bocsu.cpp"
	.text
	.p2align 4
	.globl	u_writeIdenticalLevelRun_67
	.type	u_writeIdenticalLevelRun_67, @function
u_writeIdenticalLevelRun_67:
.LFB1084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L1
	leal	(%rdx,%rdx), %eax
	movq	%rsi, %r13
	movl	%edx, %ebx
	movq	%rcx, %r14
	movl	%eax, -156(%rbp)
	leaq	-132(%rbp), %rax
	xorl	%r15d, %r15d
	movq	%rax, -168(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L24:
	movq	(%r14), %rax
	movl	$1, %esi
	movq	%r14, %rdi
	movq	-168(%rbp), %r9
	movq	-152(%rbp), %rcx
	movl	-156(%rbp), %edx
	movl	$64, %r8d
	call	*24(%rax)
	movslq	-132(%rbp), %r9
	movq	%rax, %rsi
	cmpl	$15, %r9d
	jle	.L3
	subq	$4, %r9
	addq	%rsi, %r9
	jc	.L27
.L55:
	cmpl	%r15d, %ebx
	jle	.L27
	movq	%rsi, %rdx
	movl	$30292, %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L52:
	movb	$2, (%rdx)
	xorl	%r12d, %r12d
	addq	$1, %rdx
.L11:
	cmpl	%ebx, %edi
	jge	.L36
.L53:
	cmpq	%r9, %rdx
	ja	.L36
	movl	%edi, %r15d
.L6:
	leal	-19968(%r12), %ecx
	andl	$-128, %r12d
	movslq	%r15d, %r8
	addl	$80, %r12d
	cmpl	$20991, %ecx
	leaq	(%r8,%r8), %r10
	movl	%r12d, %ecx
	movzwl	0(%r13,%r8,2), %r12d
	leal	1(%r15), %edi
	cmovbe	%eax, %ecx
	movl	%r12d, %r8d
	andl	$-1024, %r8d
	cmpl	$55296, %r8d
	jne	.L9
	cmpl	%edi, %ebx
	jne	.L51
.L9:
	cmpl	$65534, %r12d
	je	.L52
	movl	%r12d, %r8d
	subl	%ecx, %r8d
	cmpl	$-80, %r8d
	jl	.L12
	cmpl	$80, %r8d
	jg	.L13
	subl	$127, %r8d
	addq	$1, %rdx
	movb	%r8b, -1(%rdx)
	cmpl	%ebx, %edi
	jl	.L53
	.p2align 4,,10
	.p2align 3
.L36:
	subl	%esi, %edx
	movl	%edi, %r15d
.L5:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	cmpl	%r15d, %ebx
	jg	.L24
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movslq	%r8d, %rcx
	movl	%r8d, %r10d
	movl	%r8d, %r15d
	imulq	$-2122019415, %rcx, %rcx
	sarl	$31, %r10d
	shrq	$32, %rcx
	addl	%r8d, %ecx
	sarl	$7, %ecx
	movl	%ecx, %r11d
	subl	%r10d, %ecx
	subl	%r10d, %r11d
	imull	$253, %r11d, %r11d
	subl	%r11d, %r15d
	cmpl	$-10668, %r8d
	jl	.L16
	movl	$3, %r8d
	testl	%r15d, %r15d
	je	.L17
	subl	$1, %ecx
	movl	%r15d, %r8d
.L17:
	addl	$49, %ecx
	movb	%r8b, 1(%rdx)
	addq	$2, %rdx
	movb	%cl, -2(%rdx)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L51:
	movzwl	2(%r13,%r10), %r8d
	movl	%r8d, %r10d
	andl	$-1024, %r10d
	cmpl	$56320, %r10d
	jne	.L9
	sall	$10, %r12d
	leal	2(%r15), %edi
	leal	-56613888(%r8,%r12), %r12d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L13:
	movslq	%r8d, %rcx
	movl	%r8d, %r10d
	movl	%r8d, %r11d
	imulq	$-2122019415, %rcx, %rcx
	sarl	$31, %r10d
	shrq	$32, %rcx
	addl	%r8d, %ecx
	sarl	$7, %ecx
	subl	%r10d, %ecx
	imull	$253, %ecx, %r10d
	subl	%r10d, %r11d
	addl	$3, %r11d
	cmpl	$10667, %r8d
	jg	.L14
	subl	$46, %ecx
	movb	%r11b, 1(%rdx)
	addq	$2, %rdx
	movb	%cl, -2(%rdx)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L16:
	cmpl	$-192786, %r8d
	movl	$3, %r8d
	jl	.L18
	testl	%r15d, %r15d
	je	.L19
	subl	$1, %ecx
	movl	%r15d, %r8d
.L19:
	movb	%r8b, 2(%rdx)
	movslq	%ecx, %r8
	movl	%ecx, %r11d
	imulq	$-2122019415, %r8, %r8
	sarl	$31, %r11d
	shrq	$32, %r8
	addl	%ecx, %r8d
	sarl	$7, %r8d
	movl	%r8d, %r10d
	subl	%r11d, %r8d
	subl	%r11d, %r10d
	movl	$3, %r11d
	imull	$253, %r10d, %r10d
	subl	%r10d, %ecx
	je	.L20
	subl	$1, %r8d
	movl	%ecx, %r11d
.L20:
	addl	$7, %r8d
	movb	%r11b, 1(%rdx)
	addq	$3, %rdx
	movb	%r8b, -3(%rdx)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L3:
	movq	-152(%rbp), %rsi
	movl	$60, %r9d
	movl	$64, -132(%rbp)
	addq	%rsi, %r9
	jnc	.L55
.L27:
	xorl	%edx, %edx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L18:
	testl	%r15d, %r15d
	je	.L21
	subl	$1, %ecx
	movl	%r15d, %r8d
.L21:
	movb	%r8b, 3(%rdx)
	movslq	%ecx, %r8
	movl	%ecx, %r11d
	imulq	$-2122019415, %r8, %r8
	sarl	$31, %r11d
	shrq	$32, %r8
	addl	%ecx, %r8d
	sarl	$7, %r8d
	movl	%r8d, %r10d
	subl	%r11d, %r8d
	subl	%r11d, %r10d
	movl	$3, %r11d
	imull	$253, %r10d, %r10d
	subl	%r10d, %ecx
	je	.L22
	subl	$1, %r8d
	movl	%ecx, %r11d
.L22:
	movslq	%r8d, %rcx
	movl	%r8d, %r10d
	movb	%r11b, 2(%rdx)
	movl	$3, %r11d
	imulq	$-2122019415, %rcx, %rcx
	sarl	$31, %r10d
	movb	$3, (%rdx)
	shrq	$32, %rcx
	addl	%r8d, %ecx
	sarl	$7, %ecx
	subl	%r10d, %ecx
	imull	$253, %ecx, %ecx
	subl	%ecx, %r8d
	movl	%r8d, %ecx
	cmove	%r11d, %ecx
	addq	$4, %rdx
	movb	%cl, -3(%rdx)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%ecx, %r10d
	movl	$2172947881, %r15d
	imulq	%r15, %r10
	shrq	$39, %r10
	imull	$253, %r10d, %r15d
	subl	%r15d, %ecx
	addl	$3, %ecx
	cmpl	$192785, %r8d
	jg	.L15
	subl	$4, %r10d
	movb	%r11b, 2(%rdx)
	addq	$3, %rdx
	movb	%cl, -2(%rdx)
	movb	%r10b, -3(%rdx)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L15:
	movb	%r11b, 3(%rdx)
	movl	$2172947881, %r11d
	addq	$4, %rdx
	movb	%cl, -2(%rdx)
	movl	%r10d, %ecx
	imulq	%r11, %rcx
	movb	$-1, -4(%rdx)
	shrq	$39, %rcx
	imull	$253, %ecx, %ecx
	subl	%ecx, %r10d
	addl	$3, %r10d
	movb	%r10b, -3(%rdx)
	jmp	.L11
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1084:
	.size	u_writeIdenticalLevelRun_67, .-u_writeIdenticalLevelRun_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
