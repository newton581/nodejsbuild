	.file	"sortkey.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712CollationKey17getDynamicClassIDEv
	.type	_ZNK6icu_6712CollationKey17getDynamicClassIDEv, @function
_ZNK6icu_6712CollationKey17getDynamicClassIDEv:
.LFB2404:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712CollationKey16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2404:
	.size	_ZNK6icu_6712CollationKey17getDynamicClassIDEv, .-_ZNK6icu_6712CollationKey17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CollationKeyD2Ev
	.type	_ZN6icu_6712CollationKeyD2Ev, @function
_ZN6icu_6712CollationKeyD2Ev:
.LFB2418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712CollationKeyE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	8(%rdi), %eax
	testl	%eax, %eax
	js	.L6
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2418:
	.size	_ZN6icu_6712CollationKeyD2Ev, .-_ZN6icu_6712CollationKeyD2Ev
	.globl	_ZN6icu_6712CollationKeyD1Ev
	.set	_ZN6icu_6712CollationKeyD1Ev,_ZN6icu_6712CollationKeyD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CollationKeyD0Ev
	.type	_ZN6icu_6712CollationKeyD0Ev, @function
_ZN6icu_6712CollationKeyD0Ev:
.LFB2420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712CollationKeyE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	8(%rdi), %eax
	testl	%eax, %eax
	js	.L10
.L8:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L8
	.cfi_endproc
.LFE2420:
	.size	_ZN6icu_6712CollationKeyD0Ev, .-_ZN6icu_6712CollationKeyD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CollationKey16getStaticClassIDEv
	.type	_ZN6icu_6712CollationKey16getStaticClassIDEv, @function
_ZN6icu_6712CollationKey16getStaticClassIDEv:
.LFB2403:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712CollationKey16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2403:
	.size	_ZN6icu_6712CollationKey16getStaticClassIDEv, .-_ZN6icu_6712CollationKey16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CollationKeyC2Ev
	.type	_ZN6icu_6712CollationKeyC2Ev, @function
_ZN6icu_6712CollationKeyC2Ev:
.LFB2406:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712CollationKeyE(%rip), %rax
	movq	%rax, (%rdi)
	movabsq	$4294967296, %rax
	movq	%rax, 8(%rdi)
	ret
	.cfi_endproc
.LFE2406:
	.size	_ZN6icu_6712CollationKeyC2Ev, .-_ZN6icu_6712CollationKeyC2Ev
	.globl	_ZN6icu_6712CollationKeyC1Ev
	.set	_ZN6icu_6712CollationKeyC1Ev,_ZN6icu_6712CollationKeyC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CollationKeyC2EPKhi
	.type	_ZN6icu_6712CollationKeyC2EPKhi, @function
_ZN6icu_6712CollationKeyC2EPKhi:
.LFB2409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712CollationKeyE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	%rax, (%rdi)
	movl	%edx, 8(%rdi)
	movl	$0, 12(%rdi)
	testl	%edx, %edx
	js	.L14
	testq	%rsi, %rsi
	jne	.L22
	testl	%edx, %edx
	jne	.L14
.L22:
	cmpl	$32, %ebx
	jg	.L27
	testl	%ebx, %ebx
	je	.L13
	movslq	%ebx, %rdx
	leaq	16(%r12), %rdi
.L20:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcpy@PLT
.L28:
	.cfi_restore_state
	movl	8(%r12), %ebx
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$2, 12(%r12)
	andl	$-2147483648, %ebx
	movl	%ebx, 8(%r12)
.L13:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movslq	%ebx, %r14
	movq	%rsi, -40(%rbp)
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	movq	-40(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L28
	movl	8(%r12), %eax
	testl	%eax, %eax
	js	.L29
.L18:
	orl	$-2147483648, %eax
	movq	%r13, 16(%r12)
	movq	%r14, %rdx
	movq	%r13, %rdi
	movl	%ebx, 24(%r12)
	movl	%eax, 8(%r12)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L29:
	movq	16(%r12), %rdi
	movq	%rsi, -40(%rbp)
	call	uprv_free_67@PLT
	movl	8(%r12), %eax
	movq	-40(%rbp), %rsi
	jmp	.L18
	.cfi_endproc
.LFE2409:
	.size	_ZN6icu_6712CollationKeyC2EPKhi, .-_ZN6icu_6712CollationKeyC2EPKhi
	.globl	_ZN6icu_6712CollationKeyC1EPKhi
	.set	_ZN6icu_6712CollationKeyC1EPKhi,_ZN6icu_6712CollationKeyC2EPKhi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CollationKeyC2ERKS0_
	.type	_ZN6icu_6712CollationKeyC2ERKS0_, @function
_ZN6icu_6712CollationKeyC2ERKS0_:
.LFB2415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712CollationKeyE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	8(%rsi), %edx
	movq	%rax, (%rdi)
	movl	12(%rsi), %eax
	movl	%edx, %r13d
	andl	$2147483647, %r13d
	movl	%eax, 12(%rdi)
	movl	%r13d, 8(%rdi)
	cmpl	$2, %eax
	je	.L45
	movq	%rsi, %r12
	cmpl	$32, %r13d
	jg	.L46
	testl	%r13d, %r13d
	jne	.L47
.L30:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movslq	%r13d, %r15
	movq	%r15, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L48
	movl	8(%rbx), %eax
	testl	%eax, %eax
	js	.L49
.L35:
	orl	$-2147483648, %eax
	movq	%r14, 16(%rbx)
	movl	8(%r12), %edx
	movl	%eax, 8(%rbx)
	movl	%r13d, 24(%rbx)
	movl	%eax, %r13d
.L40:
	leaq	16(%r12), %rsi
	testl	%edx, %edx
	jns	.L37
	movq	16(%r12), %rsi
.L37:
	leaq	16(%rbx), %rdi
	testl	%r13d, %r13d
	jns	.L39
	movq	16(%rbx), %rdi
.L39:
	addq	$8, %rsp
	movq	%r15, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movabsq	$8589934592, %rax
	movq	%rax, 8(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movslq	%r13d, %r15
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L49:
	movq	16(%rbx), %rdi
	call	uprv_free_67@PLT
	movl	8(%rbx), %eax
	jmp	.L35
.L48:
	andl	$-2147483648, 8(%rbx)
	movl	$2, 12(%rbx)
	jmp	.L30
	.cfi_endproc
.LFE2415:
	.size	_ZN6icu_6712CollationKeyC2ERKS0_, .-_ZN6icu_6712CollationKeyC2ERKS0_
	.globl	_ZN6icu_6712CollationKeyC1ERKS0_
	.set	_ZN6icu_6712CollationKeyC1ERKS0_,_ZN6icu_6712CollationKeyC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CollationKey10reallocateEii
	.type	_ZN6icu_6712CollationKey10reallocateEii, @function
_ZN6icu_6712CollationKey10reallocateEii:
.LFB2421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movq	%rdi, %r14
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L50
	movl	8(%rbx), %r13d
	testl	%r15d, %r15d
	jle	.L52
	movslq	%r15d, %rdx
	leaq	16(%rbx), %rsi
	testl	%r13d, %r13d
	js	.L60
.L54:
	movq	%r12, %rdi
	call	memcpy@PLT
.L52:
	testl	%r13d, %r13d
	js	.L61
.L55:
	orl	$-2147483648, %r13d
	movq	%r12, 16(%rbx)
	movl	%r14d, 24(%rbx)
	movl	%r13d, 8(%rbx)
.L50:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movq	16(%rbx), %rsi
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L61:
	movq	16(%rbx), %rdi
	call	uprv_free_67@PLT
	movl	8(%rbx), %r13d
	jmp	.L55
	.cfi_endproc
.LFE2421:
	.size	_ZN6icu_6712CollationKey10reallocateEii, .-_ZN6icu_6712CollationKey10reallocateEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CollationKey9setLengthEi
	.type	_ZN6icu_6712CollationKey9setLengthEi, @function
_ZN6icu_6712CollationKey9setLengthEi:
.LFB2422:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	movl	8(%rdi), %esi
	movl	$0, 12(%rdi)
	andl	$-2147483648, %esi
	orl	%r8d, %esi
	movl	%esi, 8(%rdi)
	ret
	.cfi_endproc
.LFE2422:
	.size	_ZN6icu_6712CollationKey9setLengthEi, .-_ZN6icu_6712CollationKey9setLengthEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CollationKey5resetEv
	.type	_ZN6icu_6712CollationKey5resetEv, @function
_ZN6icu_6712CollationKey5resetEv:
.LFB2423:
	.cfi_startproc
	endbr64
	andl	$-2147483648, 8(%rdi)
	movq	%rdi, %rax
	movl	$1, 12(%rdi)
	ret
	.cfi_endproc
.LFE2423:
	.size	_ZN6icu_6712CollationKey5resetEv, .-_ZN6icu_6712CollationKey5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CollationKey10setToBogusEv
	.type	_ZN6icu_6712CollationKey10setToBogusEv, @function
_ZN6icu_6712CollationKey10setToBogusEv:
.LFB2424:
	.cfi_startproc
	endbr64
	andl	$-2147483648, 8(%rdi)
	movq	%rdi, %rax
	movl	$2, 12(%rdi)
	ret
	.cfi_endproc
.LFE2424:
	.size	_ZN6icu_6712CollationKey10setToBogusEv, .-_ZN6icu_6712CollationKey10setToBogusEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712CollationKeyeqERKS0_
	.type	_ZNK6icu_6712CollationKeyeqERKS0_, @function
_ZNK6icu_6712CollationKeyeqERKS0_:
.LFB2425:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %ecx
	movl	8(%rsi), %r8d
	xorl	%eax, %eax
	movl	%ecx, %edx
	movl	%r8d, %r9d
	andl	$2147483647, %edx
	andl	$2147483647, %r9d
	cmpl	%r9d, %edx
	je	.L77
.L74:
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L74
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%r8d, %r8d
	js	.L67
	addq	$16, %rsi
.L68:
	testl	%ecx, %ecx
	js	.L69
	addq	$16, %rdi
.L70:
	call	memcmp@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	16(%rsi), %rsi
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L69:
	movq	16(%rdi), %rdi
	jmp	.L70
	.cfi_endproc
.LFE2425:
	.size	_ZNK6icu_6712CollationKeyeqERKS0_, .-_ZNK6icu_6712CollationKeyeqERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CollationKeyaSERKS0_
	.type	_ZN6icu_6712CollationKeyaSERKS0_, @function
_ZN6icu_6712CollationKeyaSERKS0_:
.LFB2426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	%rsi, %rdi
	je	.L92
	cmpl	$2, 12(%rsi)
	movl	8(%rdi), %eax
	movq	%rsi, %rbx
	je	.L96
	movl	8(%rsi), %r13d
	movl	$32, %edx
	andl	$2147483647, %r13d
	testl	%eax, %eax
	jns	.L81
	movl	24(%rdi), %edx
.L81:
	cmpl	%r13d, %edx
	jl	.L97
.L82:
	testl	%r13d, %r13d
	je	.L85
	movl	8(%rbx), %ecx
	movslq	%r13d, %rdx
	leaq	16(%rbx), %rsi
	testl	%ecx, %ecx
	js	.L98
.L87:
	leaq	16(%r12), %rdi
	testl	%eax, %eax
	js	.L99
.L89:
	call	memcpy@PLT
	movl	8(%r12), %eax
.L85:
	andl	$-2147483648, %eax
	orl	%r13d, %eax
	movl	%eax, 8(%r12)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
.L92:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	andl	$-2147483648, %eax
	movl	$2, 12(%rdi)
	movl	%eax, 8(%rdi)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movq	16(%r12), %rdi
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L98:
	movq	16(%rbx), %rsi
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L97:
	movslq	%r13d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L100
	movl	8(%r12), %eax
	testl	%eax, %eax
	js	.L101
.L84:
	orl	$-2147483648, %eax
	movq	%r14, 16(%r12)
	movl	%r13d, 24(%r12)
	movl	%eax, 8(%r12)
	jmp	.L82
.L100:
	andl	$-2147483648, 8(%r12)
	movl	$2, 12(%r12)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L101:
	movq	16(%r12), %rdi
	call	uprv_free_67@PLT
	movl	8(%r12), %eax
	jmp	.L84
	.cfi_endproc
.LFE2426:
	.size	_ZN6icu_6712CollationKeyaSERKS0_, .-_ZN6icu_6712CollationKeyaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712CollationKey9compareToERKS0_
	.type	_ZNK6icu_6712CollationKey9compareToERKS0_, @function
_ZNK6icu_6712CollationKey9compareToERKS0_:
.LFB2427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movl	8(%rdi), %edx
	testl	%edx, %edx
	js	.L103
	addq	$16, %rdi
.L104:
	movl	8(%rsi), %eax
	testl	%eax, %eax
	js	.L105
	addq	$16, %rsi
	cmpq	%rdi, %rsi
	je	.L109
.L119:
	andl	$2147483647, %edx
	andl	$2147483647, %eax
	movl	$-1, %r12d
	cmpl	%eax, %edx
	jge	.L118
.L108:
	testl	%edx, %edx
	je	.L102
	movslq	%edx, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jg	.L111
	movl	$-1, %eax
	cmovne	%eax, %r12d
.L102:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	setne	%r12b
	movl	%eax, %edx
	movzbl	%r12b, %r12d
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$1, %r12d
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movq	16(%rsi), %rsi
	cmpq	%rdi, %rsi
	jne	.L119
.L109:
	xorl	%r12d, %r12d
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	jmp	.L104
	.cfi_endproc
.LFE2427:
	.size	_ZNK6icu_6712CollationKey9compareToERKS0_, .-_ZNK6icu_6712CollationKey9compareToERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712CollationKey9compareToERKS0_R10UErrorCode
	.type	_ZNK6icu_6712CollationKey9compareToERKS0_R10UErrorCode, @function
_ZNK6icu_6712CollationKey9compareToERKS0_R10UErrorCode:
.LFB2428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	subq	$8, %rsp
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L120
	movl	8(%rdi), %edx
	testl	%edx, %edx
	js	.L122
	movl	8(%rsi), %eax
	addq	$16, %rdi
	testl	%eax, %eax
	js	.L124
.L137:
	addq	$16, %rsi
	cmpq	%rdi, %rsi
	je	.L128
.L138:
	andl	$2147483647, %edx
	andl	$2147483647, %eax
	movl	$-1, %r12d
	cmpl	%eax, %edx
	jl	.L126
	setne	%r12b
	movl	%eax, %edx
	movzbl	%r12b, %r12d
.L126:
	testl	%edx, %edx
	je	.L120
	movslq	%edx, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jg	.L130
	movl	$-1, %eax
	cmovne	%eax, %r12d
.L120:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movl	8(%rsi), %eax
	movq	16(%rdi), %rdi
	testl	%eax, %eax
	jns	.L137
.L124:
	movq	16(%rsi), %rsi
	cmpq	%rdi, %rsi
	jne	.L138
.L128:
	xorl	%r12d, %r12d
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movl	$1, %r12d
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2428:
	.size	_ZNK6icu_6712CollationKey9compareToERKS0_R10UErrorCode, .-_ZNK6icu_6712CollationKey9compareToERKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712CollationKey8hashCodeEv
	.type	_ZNK6icu_6712CollationKey8hashCodeEv, @function
_ZNK6icu_6712CollationKey8hashCodeEv:
.LFB2430:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	testl	%eax, %eax
	jne	.L146
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movl	-8(%rdi), %eax
	movl	%eax, %esi
	andl	$2147483647, %esi
	testl	%eax, %eax
	js	.L149
	testl	%esi, %esi
	je	.L143
.L151:
	testq	%rdi, %rdi
	jne	.L150
.L143:
	movl	$1, %eax
.L144:
	movl	%eax, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	16(%rbx), %rdi
	testl	%esi, %esi
	jne	.L151
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L150:
	call	ustr_hashCharsN_67@PLT
	testl	$-3, %eax
	jne	.L144
	jmp	.L143
	.cfi_endproc
.LFE2430:
	.size	_ZNK6icu_6712CollationKey8hashCodeEv, .-_ZNK6icu_6712CollationKey8hashCodeEv
	.p2align 4
	.globl	ucol_keyHashCode_67
	.type	ucol_keyHashCode_67, @function
ucol_keyHashCode_67:
.LFB2431:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L161
	testl	%esi, %esi
	jne	.L153
.L161:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ustr_hashCharsN_67@PLT
	testl	$-3, %eax
	jne	.L152
	movl	$1, %eax
.L152:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2431:
	.size	ucol_keyHashCode_67, .-ucol_keyHashCode_67
	.weak	_ZTSN6icu_6712CollationKeyE
	.section	.rodata._ZTSN6icu_6712CollationKeyE,"aG",@progbits,_ZTSN6icu_6712CollationKeyE,comdat
	.align 16
	.type	_ZTSN6icu_6712CollationKeyE, @object
	.size	_ZTSN6icu_6712CollationKeyE, 24
_ZTSN6icu_6712CollationKeyE:
	.string	"N6icu_6712CollationKeyE"
	.weak	_ZTIN6icu_6712CollationKeyE
	.section	.data.rel.ro._ZTIN6icu_6712CollationKeyE,"awG",@progbits,_ZTIN6icu_6712CollationKeyE,comdat
	.align 8
	.type	_ZTIN6icu_6712CollationKeyE, @object
	.size	_ZTIN6icu_6712CollationKeyE, 24
_ZTIN6icu_6712CollationKeyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712CollationKeyE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6712CollationKeyE
	.section	.data.rel.ro.local._ZTVN6icu_6712CollationKeyE,"awG",@progbits,_ZTVN6icu_6712CollationKeyE,comdat
	.align 8
	.type	_ZTVN6icu_6712CollationKeyE, @object
	.size	_ZTVN6icu_6712CollationKeyE, 40
_ZTVN6icu_6712CollationKeyE:
	.quad	0
	.quad	_ZTIN6icu_6712CollationKeyE
	.quad	_ZN6icu_6712CollationKeyD1Ev
	.quad	_ZN6icu_6712CollationKeyD0Ev
	.quad	_ZNK6icu_6712CollationKey17getDynamicClassIDEv
	.local	_ZZN6icu_6712CollationKey16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6712CollationKey16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
