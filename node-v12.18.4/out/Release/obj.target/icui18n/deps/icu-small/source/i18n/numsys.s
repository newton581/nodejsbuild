	.file	"numsys.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715NumberingSystem17getDynamicClassIDEv
	.type	_ZNK6icu_6715NumberingSystem17getDynamicClassIDEv, @function
_ZNK6icu_6715NumberingSystem17getDynamicClassIDEv:
.LFB3191:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6715NumberingSystem16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3191:
	.size	_ZNK6icu_6715NumberingSystem17getDynamicClassIDEv, .-_ZNK6icu_6715NumberingSystem17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721NumsysNameEnumeration17getDynamicClassIDEv
	.type	_ZNK6icu_6721NumsysNameEnumeration17getDynamicClassIDEv, @function
_ZNK6icu_6721NumsysNameEnumeration17getDynamicClassIDEv:
.LFB3193:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6721NumsysNameEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3193:
	.size	_ZNK6icu_6721NumsysNameEnumeration17getDynamicClassIDEv, .-_ZNK6icu_6721NumsysNameEnumeration17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721NumsysNameEnumeration5resetER10UErrorCode
	.type	_ZN6icu_6721NumsysNameEnumeration5resetER10UErrorCode, @function
_ZN6icu_6721NumsysNameEnumeration5resetER10UErrorCode:
.LFB3225:
	.cfi_startproc
	endbr64
	movl	$0, 116(%rdi)
	ret
	.cfi_endproc
.LFE3225:
	.size	_ZN6icu_6721NumsysNameEnumeration5resetER10UErrorCode, .-_ZN6icu_6721NumsysNameEnumeration5resetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721NumsysNameEnumeration5countER10UErrorCode
	.type	_ZNK6icu_6721NumsysNameEnumeration5countER10UErrorCode, @function
_ZNK6icu_6721NumsysNameEnumeration5countER10UErrorCode:
.LFB3226:
	.cfi_startproc
	endbr64
	movq	_ZN6icu_6712_GLOBAL__N_112gNumsysNamesE(%rip), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L5
	movl	8(%rdx), %eax
.L5:
	ret
	.cfi_endproc
.LFE3226:
	.size	_ZNK6icu_6721NumsysNameEnumeration5countER10UErrorCode, .-_ZNK6icu_6721NumsysNameEnumeration5countER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NumberingSystemD2Ev
	.type	_ZN6icu_6715NumberingSystemD2Ev, @function
_ZN6icu_6715NumberingSystemD2Ev:
.LFB3207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715NumberingSystemE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3207:
	.size	_ZN6icu_6715NumberingSystemD2Ev, .-_ZN6icu_6715NumberingSystemD2Ev
	.globl	_ZN6icu_6715NumberingSystemD1Ev
	.set	_ZN6icu_6715NumberingSystemD1Ev,_ZN6icu_6715NumberingSystemD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NumberingSystemD0Ev
	.type	_ZN6icu_6715NumberingSystemD0Ev, @function
_ZN6icu_6715NumberingSystemD0Ev:
.LFB3209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715NumberingSystemE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3209:
	.size	_ZN6icu_6715NumberingSystemD0Ev, .-_ZN6icu_6715NumberingSystemD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715NumberingSystem14getDescriptionEv
	.type	_ZNK6icu_6715NumberingSystem14getDescriptionEv, @function
_ZNK6icu_6715NumberingSystem14getDescriptionEv:
.LFB3211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3211:
	.size	_ZNK6icu_6715NumberingSystem14getDescriptionEv, .-_ZNK6icu_6715NumberingSystem14getDescriptionEv
	.p2align 4
	.globl	numSysCleanup_67
	.type	numSysCleanup_67, @function
numSysCleanup_67:
.LFB3218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZN6icu_6712_GLOBAL__N_112gNumsysNamesE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L16
	movq	(%rdi), %rax
	call	*8(%rax)
.L16:
	movq	$0, _ZN6icu_6712_GLOBAL__N_112gNumsysNamesE(%rip)
	movl	$1, %eax
	movl	$0, _ZN6icu_6712_GLOBAL__N_115gNumSysInitOnceE(%rip)
	mfence
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3218:
	.size	numSysCleanup_67, .-numSysCleanup_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721NumsysNameEnumerationD2Ev
	.type	_ZN6icu_6721NumsysNameEnumerationD2Ev, @function
_ZN6icu_6721NumsysNameEnumerationD2Ev:
.LFB3228:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6721NumsysNameEnumerationE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717StringEnumerationD2Ev@PLT
	.cfi_endproc
.LFE3228:
	.size	_ZN6icu_6721NumsysNameEnumerationD2Ev, .-_ZN6icu_6721NumsysNameEnumerationD2Ev
	.globl	_ZN6icu_6721NumsysNameEnumerationD1Ev
	.set	_ZN6icu_6721NumsysNameEnumerationD1Ev,_ZN6icu_6721NumsysNameEnumerationD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721NumsysNameEnumerationD0Ev
	.type	_ZN6icu_6721NumsysNameEnumerationD0Ev, @function
_ZN6icu_6721NumsysNameEnumerationD0Ev:
.LFB3230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721NumsysNameEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3230:
	.size	_ZN6icu_6721NumsysNameEnumerationD0Ev, .-_ZN6icu_6721NumsysNameEnumerationD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721NumsysNameEnumeration5snextER10UErrorCode
	.type	_ZN6icu_6721NumsysNameEnumeration5snextER10UErrorCode, @function
_ZN6icu_6721NumsysNameEnumeration5snextER10UErrorCode:
.LFB3224:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L24
	movq	_ZN6icu_6712_GLOBAL__N_112gNumsysNamesE(%rip), %r8
	testq	%r8, %r8
	je	.L24
	movl	116(%rdi), %esi
	cmpl	8(%r8), %esi
	jge	.L24
	leal	1(%rsi), %eax
	movl	%eax, 116(%rdi)
	movq	%r8, %rdi
	jmp	_ZNK6icu_677UVector9elementAtEi@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3224:
	.size	_ZN6icu_6721NumsysNameEnumeration5snextER10UErrorCode, .-_ZN6icu_6721NumsysNameEnumeration5snextER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NumberingSystem16getStaticClassIDEv
	.type	_ZN6icu_6715NumberingSystem16getStaticClassIDEv, @function
_ZN6icu_6715NumberingSystem16getStaticClassIDEv:
.LFB3190:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6715NumberingSystem16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3190:
	.size	_ZN6icu_6715NumberingSystem16getStaticClassIDEv, .-_ZN6icu_6715NumberingSystem16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721NumsysNameEnumeration16getStaticClassIDEv
	.type	_ZN6icu_6721NumsysNameEnumeration16getStaticClassIDEv, @function
_ZN6icu_6721NumsysNameEnumeration16getStaticClassIDEv:
.LFB3192:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6721NumsysNameEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3192:
	.size	_ZN6icu_6721NumsysNameEnumeration16getStaticClassIDEv, .-_ZN6icu_6721NumsysNameEnumeration16getStaticClassIDEv
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"0"
	.string	"1"
	.string	"2"
	.string	"3"
	.string	"4"
	.string	"5"
	.string	"6"
	.string	"7"
	.string	"8"
	.string	"9"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NumberingSystemC2Ev
	.type	_ZN6icu_6715NumberingSystemC2Ev, @function
_ZN6icu_6715NumberingSystemC2Ev:
.LFB3195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715NumberingSystemE(%rip), %rcx
	movl	$1, %esi
	movq	%rcx, %xmm0
	movl	$-1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-96(%rbp), %r12
	movq	%rdi, %rbx
	leaq	-104(%rbp), %rdx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$10, 72(%rdi)
	movq	%rax, %xmm1
	movl	$2, %eax
	movb	$0, 76(%rdi)
	movw	%ax, 16(%rdi)
	punpcklqdq	%xmm1, %xmm0
	leaq	.LC0(%rip), %rax
	movups	%xmm0, (%rdi)
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	leaq	8(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	movl	_ZN6icu_67L5gLatnE(%rip), %eax
	movq	%r12, %rdi
	movl	%eax, 77(%rbx)
	movzbl	4+_ZN6icu_67L5gLatnE(%rip), %eax
	movb	%al, 81(%rbx)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L36:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3195:
	.size	_ZN6icu_6715NumberingSystemC2Ev, .-_ZN6icu_6715NumberingSystemC2Ev
	.globl	_ZN6icu_6715NumberingSystemC1Ev
	.set	_ZN6icu_6715NumberingSystemC1Ev,_ZN6icu_6715NumberingSystemC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NumberingSystemC2ERKS0_
	.type	_ZN6icu_6715NumberingSystemC2ERKS0_, @function
_ZN6icu_6715NumberingSystemC2ERKS0_:
.LFB3200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	16+_ZTVN6icu_6715NumberingSystemE(%rip), %rdx
	movq	%rax, %xmm1
	movl	$2, %eax
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	8(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	movw	%ax, 8(%rdi)
	movups	%xmm0, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	72(%r12), %eax
	movl	%eax, 72(%rbx)
	movzbl	76(%r12), %eax
	movb	%al, 76(%rbx)
	movzbl	77(%r12), %eax
	movb	%al, 77(%rbx)
	movzbl	78(%r12), %eax
	movb	%al, 78(%rbx)
	movzbl	79(%r12), %eax
	movb	%al, 79(%rbx)
	movzbl	80(%r12), %eax
	movb	%al, 80(%rbx)
	movzbl	81(%r12), %eax
	movb	%al, 81(%rbx)
	movzbl	82(%r12), %eax
	movb	%al, 82(%rbx)
	movzbl	83(%r12), %eax
	movb	%al, 83(%rbx)
	movzbl	84(%r12), %eax
	movb	%al, 84(%rbx)
	movzbl	85(%r12), %eax
	movb	%al, 85(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3200:
	.size	_ZN6icu_6715NumberingSystemC2ERKS0_, .-_ZN6icu_6715NumberingSystemC2ERKS0_
	.globl	_ZN6icu_6715NumberingSystemC1ERKS0_
	.set	_ZN6icu_6715NumberingSystemC1ERKS0_,_ZN6icu_6715NumberingSystemC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NumberingSystem14createInstanceEiaRKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6715NumberingSystem14createInstanceEiaRKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6715NumberingSystem14createInstanceEiaRKNS_13UnicodeStringER10UErrorCode:
.LFB3202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -148(%rbp)
	movl	(%rcx), %esi
	movq	%rdx, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jg	.L56
	movl	%edi, %ebx
	movq	%rcx, %r15
	cmpl	$1, %edi
	jle	.L44
	cmpb	$0, -148(%rbp)
	je	.L57
.L43:
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L45
	leaq	8(%rax), %r13
	movl	$2, %edx
	movq	.LC1(%rip), %xmm0
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, %xmm1
	movw	%dx, 16(%r12)
	leaq	-128(%rbp), %r14
	leaq	.LC0(%rip), %rax
	movl	$10, 72(%r12)
	punpcklqdq	%xmm1, %xmm0
	movq	%r14, %rdi
	leaq	-136(%rbp), %rdx
	movb	$0, 76(%r12)
	movl	$-1, %ecx
	movl	$1, %esi
	movups	%xmm0, (%r12)
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	movl	_ZN6icu_67L5gLatnE(%rip), %eax
	movq	%r14, %rdi
	movl	%eax, 77(%r12)
	movzbl	4+_ZN6icu_67L5gLatnE(%rip), %eax
	movb	%al, 81(%r12)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jle	.L58
	movq	(%r12), %rax
	leaq	_ZN6icu_6715NumberingSystemD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L59
	leaq	16+_ZTVN6icu_6715NumberingSystemE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L56:
	xorl	%r12d, %r12d
.L39:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	%rdx, %rdi
	xorl	%esi, %esi
	movl	$2147483647, %edx
	call	_ZNK6icu_6713UnicodeString11countChar32Eii@PLT
	cmpl	%ebx, %eax
	je	.L43
.L44:
	movl	$1, (%r15)
	xorl	%r12d, %r12d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L58:
	movl	%ebx, 72(%r12)
	movq	-160(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	movzbl	-148(%rbp), %eax
	movb	$0, 77(%r12)
	movb	%al, 76(%r12)
	jmp	.L39
.L45:
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L56
	movl	$7, (%r15)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*%rax
	jmp	.L39
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3202:
	.size	_ZN6icu_6715NumberingSystem14createInstanceEiaRKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6715NumberingSystem14createInstanceEiaRKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NumberingSystem20createInstanceByNameEPKcR10UErrorCode
	.type	_ZN6icu_6715NumberingSystem20createInstanceByNameEPKcR10UErrorCode, @function
_ZN6icu_6715NumberingSystem20createInstanceByNameEPKcR10UErrorCode:
.LFB3205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	_ZN6icu_67L17gNumberingSystemsE(%rip), %rsi
	subq	$120, %rsp
	movq	%rdi, -160(%rbp)
	xorl	%edi, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ures_openDirect_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	_ZN6icu_67L17gNumberingSystemsE(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ures_getByKey_67@PLT
	movl	$2, %ecx
	leaq	_ZN6icu_67L5gDescE(%rip), %rsi
	leaq	-140(%rbp), %rdx
	movq	%rax, %r13
	movw	%cx, -120(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	movl	$0, -140(%rbp)
	call	ures_getStringByKey_67@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L91
	leaq	-128(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L63:
	movq	%rbx, %rcx
	movq	%r12, %rdx
	leaq	_ZN6icu_67L6gRadixE(%rip), %rsi
	movq	%r13, %rdi
	call	ures_getByKey_67@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	ures_getInt_67@PLT
	movq	%r12, %rdx
	movq	%rbx, %rcx
	movq	%r13, %rdi
	leaq	_ZN6icu_67L12gAlgorithmicE(%rip), %rsi
	movl	%eax, %r14d
	call	ures_getByKey_67@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	ures_getInt_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L92
	xorl	%esi, %esi
	cmpl	$1, %eax
	movl	%r14d, %edi
	movq	%rbx, %rcx
	sete	%sil
	movq	%r15, %rdx
	call	_ZN6icu_6715NumberingSystem14createInstanceEiaRKNS_13UnicodeStringER10UErrorCode
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L93
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L94
	movq	(%r14), %rax
	leaq	_ZN6icu_6715NumberingSystemD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L95
	leaq	16+_ZTVN6icu_6715NumberingSystemE(%rip), %rax
	leaq	8(%r14), %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L66:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r13, %r13
	je	.L70
	movq	%r13, %rdi
	call	ures_close_67@PLT
.L70:
	testq	%r12, %r12
	je	.L71
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L71:
	movq	-152(%rbp), %rax
	testq	%rax, %rax
	je	.L61
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L61:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$120, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movl	-140(%rbp), %ecx
	leaq	-128(%rbp), %r15
	leaq	-136(%rbp), %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L92:
	cmpl	$7, %edx
	je	.L90
	movl	$16, (%rbx)
	xorl	%r14d, %r14d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L94:
	cmpq	$0, -160(%rbp)
	je	.L97
	movq	-160(%rbp), %rsi
	leaq	77(%r14), %rdi
	movl	$8, %edx
	call	strncpy@PLT
	movb	$0, 85(%r14)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L93:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L90
	movl	$7, (%rbx)
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%r14d, %r14d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L97:
	movb	$0, 77(%r14)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L90
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3205:
	.size	_ZN6icu_6715NumberingSystem20createInstanceByNameEPKcR10UErrorCode, .-_ZN6icu_6715NumberingSystem20createInstanceByNameEPKcR10UErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"numbers"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode.part.0, @function
_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode.part.0:
.LFB4219:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsi, %r8
	movl	$96, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	leaq	.LC2(%rip), %rsi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	(%r15), %edx
	movl	%eax, -236(%rbp)
	testl	%edx, %edx
	jg	.L121
	cmpl	$-124, %edx
	jne	.L99
.L121:
	movl	$0, -236(%rbp)
	movl	$0, (%r15)
.L101:
	movq	_ZN6icu_67L8gDefaultE(%rip), %rax
	movq	%rax, -160(%rbp)
.L102:
	movq	40(%rbx), %rsi
	xorl	%edi, %edi
	leaq	-232(%rbp), %r14
	movl	$0, -232(%rbp)
	movq	%r14, %rdx
	leaq	-236(%rbp), %rbx
	call	ures_open_67@PLT
	xorl	%edx, %edx
	movq	%r14, %rcx
	leaq	_ZN6icu_67L15gNumberElementsE(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -248(%rbp)
	call	ures_getByKey_67@PLT
	cmpl	$7, -232(%rbp)
	movq	%rax, %r13
	je	.L106
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$0, -232(%rbp)
	movl	$0, -236(%rbp)
	call	ures_getStringByKeyWithFallback_67@PLT
	cmpl	$7, -232(%rbp)
	je	.L106
	movl	-236(%rbp), %edx
	leal	-1(%rdx), %ecx
	cmpl	$94, %ecx
	jbe	.L147
	leaq	_ZN6icu_67L7gNativeE(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L109
	leaq	_ZN6icu_67L8gFinanceE(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L109
	leaq	_ZN6icu_67L12gTraditionalE(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L120
	movl	_ZN6icu_67L7gNativeE(%rip), %eax
	movl	%eax, -160(%rbp)
	movzwl	4+_ZN6icu_67L7gNativeE(%rip), %eax
	movw	%ax, -156(%rbp)
	movzbl	6+_ZN6icu_67L7gNativeE(%rip), %eax
	movb	%al, -154(%rbp)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L99:
	testl	%eax, %eax
	jle	.L101
	cltq
	leaq	_ZN6icu_67L8gDefaultE(%rip), %rsi
	movq	%r12, %rdi
	movb	$0, -160(%rbp,%rax)
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L102
	leaq	_ZN6icu_67L7gNativeE(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L102
	leaq	_ZN6icu_67L12gTraditionalE(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L102
	leaq	_ZN6icu_67L8gFinanceE(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L102
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L109:
	movq	_ZN6icu_67L8gDefaultE(%rip), %rax
	movq	%rax, -160(%rbp)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L147:
	movq	%rax, %rdi
	movq	%r12, %rsi
	xorl	%ebx, %ebx
	call	u_UCharsToChars_67@PLT
	movslq	-236(%rbp), %rax
	movb	$0, -160(%rbp,%rax)
.L108:
	testq	%r13, %r13
	je	.L112
	movq	%r13, %rdi
	call	ures_close_67@PLT
.L112:
	movq	-248(%rbp), %rax
	testq	%rax, %rax
	je	.L113
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L113:
	testb	%bl, %bl
	jne	.L148
.L103:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6715NumberingSystem20createInstanceByNameEPKcR10UErrorCode
	movq	%rax, %r12
.L98:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movl	$7, (%r15)
	testq	%r13, %r13
	je	.L105
	movq	%r13, %rdi
	call	ures_close_67@PLT
.L105:
	movq	-248(%rbp), %rax
	testq	%rax, %rax
	je	.L115
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L115:
	xorl	%r12d, %r12d
	jmp	.L98
.L148:
	movl	$-128, (%r15)
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L117
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	16+_ZTVN6icu_6715NumberingSystemE(%rip), %rbx
	movl	$-1, %ecx
	movl	$10, 72(%r12)
	movq	%rax, %xmm1
	movl	$2, %eax
	movb	$0, 76(%r12)
	movq	%rbx, %xmm0
	movw	%ax, 16(%r12)
	punpcklqdq	%xmm1, %xmm0
	movq	%r14, %rdx
	movl	$1, %esi
	leaq	-224(%rbp), %r13
	movups	%xmm0, (%r12)
	leaq	.LC0(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	leaq	8(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	movl	_ZN6icu_67L5gLatnE(%rip), %eax
	movq	%r13, %rdi
	movl	%eax, 77(%r12)
	movzbl	4+_ZN6icu_67L5gLatnE(%rip), %eax
	movb	%al, 81(%r12)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L98
.L120:
	movl	$1, %ebx
	jmp	.L108
.L117:
	movl	$7, (%r15)
	jmp	.L98
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4219:
	.size	_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode.part.0, .-_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode:
.LFB3203:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L151
	jmp	_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L151:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3203:
	.size	_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NumberingSystem14createInstanceER10UErrorCode
	.type	_ZN6icu_6715NumberingSystem14createInstanceER10UErrorCode, @function
_ZN6icu_6715NumberingSystem14createInstanceER10UErrorCode:
.LFB3204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L153
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3204:
	.size	_ZN6icu_6715NumberingSystem14createInstanceER10UErrorCode, .-_ZN6icu_6715NumberingSystem14createInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715NumberingSystem8getRadixEv
	.type	_ZNK6icu_6715NumberingSystem8getRadixEv, @function
_ZNK6icu_6715NumberingSystem8getRadixEv:
.LFB3210:
	.cfi_startproc
	endbr64
	movl	72(%rdi), %eax
	ret
	.cfi_endproc
.LFE3210:
	.size	_ZNK6icu_6715NumberingSystem8getRadixEv, .-_ZNK6icu_6715NumberingSystem8getRadixEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715NumberingSystem7getNameEv
	.type	_ZNK6icu_6715NumberingSystem7getNameEv, @function
_ZNK6icu_6715NumberingSystem7getNameEv:
.LFB3212:
	.cfi_startproc
	endbr64
	leaq	77(%rdi), %rax
	ret
	.cfi_endproc
.LFE3212:
	.size	_ZNK6icu_6715NumberingSystem7getNameEv, .-_ZNK6icu_6715NumberingSystem7getNameEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NumberingSystem8setRadixEi
	.type	_ZN6icu_6715NumberingSystem8setRadixEi, @function
_ZN6icu_6715NumberingSystem8setRadixEi:
.LFB3213:
	.cfi_startproc
	endbr64
	movl	%esi, 72(%rdi)
	ret
	.cfi_endproc
.LFE3213:
	.size	_ZN6icu_6715NumberingSystem8setRadixEi, .-_ZN6icu_6715NumberingSystem8setRadixEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NumberingSystem14setAlgorithmicEa
	.type	_ZN6icu_6715NumberingSystem14setAlgorithmicEa, @function
_ZN6icu_6715NumberingSystem14setAlgorithmicEa:
.LFB3214:
	.cfi_startproc
	endbr64
	movb	%sil, 76(%rdi)
	ret
	.cfi_endproc
.LFE3214:
	.size	_ZN6icu_6715NumberingSystem14setAlgorithmicEa, .-_ZN6icu_6715NumberingSystem14setAlgorithmicEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NumberingSystem7setDescERKNS_13UnicodeStringE
	.type	_ZN6icu_6715NumberingSystem7setDescERKNS_13UnicodeStringE, @function
_ZN6icu_6715NumberingSystem7setDescERKNS_13UnicodeStringE:
.LFB3215:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	xorl	%edx, %edx
	jmp	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	.cfi_endproc
.LFE3215:
	.size	_ZN6icu_6715NumberingSystem7setDescERKNS_13UnicodeStringE, .-_ZN6icu_6715NumberingSystem7setDescERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NumberingSystem7setNameEPKc
	.type	_ZN6icu_6715NumberingSystem7setNameEPKc, @function
_ZN6icu_6715NumberingSystem7setNameEPKc:
.LFB3216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L164
	leaq	77(%rdi), %rdi
	movl	$8, %edx
	call	strncpy@PLT
	movb	$0, 85(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movb	$0, 77(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3216:
	.size	_ZN6icu_6715NumberingSystem7setNameEPKc, .-_ZN6icu_6715NumberingSystem7setNameEPKc
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715NumberingSystem13isAlgorithmicEv
	.type	_ZNK6icu_6715NumberingSystem13isAlgorithmicEv, @function
_ZNK6icu_6715NumberingSystem13isAlgorithmicEv:
.LFB3217:
	.cfi_startproc
	endbr64
	movzbl	76(%rdi), %eax
	ret
	.cfi_endproc
.LFE3217:
	.size	_ZNK6icu_6715NumberingSystem13isAlgorithmicEv, .-_ZNK6icu_6715NumberingSystem13isAlgorithmicEv
	.section	.rodata.str1.1
.LC3:
	.string	"numberingSystems"
	.text
	.p2align 4
	.globl	initNumsysNames_67
	.type	initNumsysNames_67, @function
initNumsysNames_67:
.LFB3219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	numSysCleanup_67(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$35, %edi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ucln_i18n_registerCleanup_67@PLT
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L167
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L168
.L185:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L166:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L199
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	leaq	-60(%rbp), %r15
	leaq	.LC3(%rip), %rsi
	xorl	%edi, %edi
	movl	$0, -60(%rbp)
	movq	%r15, %rdx
	call	ures_openDirect_67@PLT
	movq	%r15, %rcx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %rdx
	call	ures_getByKey_67@PLT
	movq	%rax, %r13
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L200
	.p2align 4,,10
	.p2align 3
.L182:
	movq	%r13, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L176
.L202:
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L176
	xorl	%esi, %esi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	ures_getNextResource_67@PLT
	cmpl	$7, -60(%rbp)
	movq	%rax, %r12
	je	.L201
	movq	%rax, %rdi
	call	ures_getKey_67@PLT
	movl	$64, %edi
	movq	%rax, -72(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L177
	movq	-72(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movl	$-1, %edx
	movq	%rax, -72(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movl	(%rbx), %edi
	movq	-72(%rbp), %r8
	testl	%edi, %edi
	jle	.L178
.L181:
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L179:
	testq	%r12, %r12
	je	.L182
	movq	%r12, %rdi
	call	ures_close_67@PLT
	movq	%r13, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	jne	.L202
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%r13, %rdi
	call	ures_close_67@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L185
	movq	%r14, _ZN6icu_6712_GLOBAL__N_112gNumsysNamesE(%rip)
	jmp	.L166
.L177:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L179
	movl	$7, (%rbx)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%r8, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%rbx), %esi
	movq	-72(%rbp), %r8
	testl	%esi, %esi
	jg	.L181
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L167:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L166
	movl	$7, (%rbx)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L200:
	cmpl	$7, %eax
	movl	$2, %edx
	movq	%r13, %rdi
	cmovne	%edx, %eax
	movl	%eax, (%rbx)
	call	ures_close_67@PLT
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L201:
	movl	$7, (%rbx)
	testq	%rax, %rax
	je	.L176
	movq	%rax, %rdi
	call	ures_close_67@PLT
	jmp	.L176
.L199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3219:
	.size	initNumsysNames_67, .-initNumsysNames_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NumberingSystem17getAvailableNamesER10UErrorCode
	.type	_ZN6icu_6715NumberingSystem17getAvailableNamesER10UErrorCode, @function
_ZN6icu_6715NumberingSystem17getAvailableNamesER10UErrorCode:
.LFB3220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rdi), %edx
	movq	%rdi, %rbx
	testl	%edx, %edx
	jle	.L218
.L205:
	movl	$120, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L208
	movq	%rax, %rdi
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	leaq	16+_ZTVN6icu_6721NumsysNameEnumerationE(%rip), %rax
	movl	$0, 116(%r12)
	movq	%rax, (%r12)
.L203:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	movl	_ZN6icu_6712_GLOBAL__N_115gNumSysInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L219
.L206:
	movl	4+_ZN6icu_6712_GLOBAL__N_115gNumSysInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L205
	movl	%eax, (%rbx)
	jmp	.L205
.L208:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L203
	movq	%r12, %rax
	movl	$7, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	leaq	_ZN6icu_6712_GLOBAL__N_115gNumSysInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L206
	movq	%rbx, %rdi
	call	initNumsysNames_67
	movl	(%rbx), %eax
	leaq	_ZN6icu_6712_GLOBAL__N_115gNumSysInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_6712_GLOBAL__N_115gNumSysInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L205
	.cfi_endproc
.LFE3220:
	.size	_ZN6icu_6715NumberingSystem17getAvailableNamesER10UErrorCode, .-_ZN6icu_6715NumberingSystem17getAvailableNamesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721NumsysNameEnumerationC2ER10UErrorCode
	.type	_ZN6icu_6721NumsysNameEnumerationC2ER10UErrorCode, @function
_ZN6icu_6721NumsysNameEnumerationC2ER10UErrorCode:
.LFB3222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	leaq	16+_ZTVN6icu_6721NumsysNameEnumerationE(%rip), %rax
	movl	$0, 116(%rbx)
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3222:
	.size	_ZN6icu_6721NumsysNameEnumerationC2ER10UErrorCode, .-_ZN6icu_6721NumsysNameEnumerationC2ER10UErrorCode
	.globl	_ZN6icu_6721NumsysNameEnumerationC1ER10UErrorCode
	.set	_ZN6icu_6721NumsysNameEnumerationC1ER10UErrorCode,_ZN6icu_6721NumsysNameEnumerationC2ER10UErrorCode
	.weak	_ZTSN6icu_6715NumberingSystemE
	.section	.rodata._ZTSN6icu_6715NumberingSystemE,"aG",@progbits,_ZTSN6icu_6715NumberingSystemE,comdat
	.align 16
	.type	_ZTSN6icu_6715NumberingSystemE, @object
	.size	_ZTSN6icu_6715NumberingSystemE, 27
_ZTSN6icu_6715NumberingSystemE:
	.string	"N6icu_6715NumberingSystemE"
	.weak	_ZTIN6icu_6715NumberingSystemE
	.section	.data.rel.ro._ZTIN6icu_6715NumberingSystemE,"awG",@progbits,_ZTIN6icu_6715NumberingSystemE,comdat
	.align 8
	.type	_ZTIN6icu_6715NumberingSystemE, @object
	.size	_ZTIN6icu_6715NumberingSystemE, 24
_ZTIN6icu_6715NumberingSystemE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715NumberingSystemE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6721NumsysNameEnumerationE
	.section	.rodata._ZTSN6icu_6721NumsysNameEnumerationE,"aG",@progbits,_ZTSN6icu_6721NumsysNameEnumerationE,comdat
	.align 32
	.type	_ZTSN6icu_6721NumsysNameEnumerationE, @object
	.size	_ZTSN6icu_6721NumsysNameEnumerationE, 33
_ZTSN6icu_6721NumsysNameEnumerationE:
	.string	"N6icu_6721NumsysNameEnumerationE"
	.weak	_ZTIN6icu_6721NumsysNameEnumerationE
	.section	.data.rel.ro._ZTIN6icu_6721NumsysNameEnumerationE,"awG",@progbits,_ZTIN6icu_6721NumsysNameEnumerationE,comdat
	.align 8
	.type	_ZTIN6icu_6721NumsysNameEnumerationE, @object
	.size	_ZTIN6icu_6721NumsysNameEnumerationE, 24
_ZTIN6icu_6721NumsysNameEnumerationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6721NumsysNameEnumerationE
	.quad	_ZTIN6icu_6717StringEnumerationE
	.weak	_ZTVN6icu_6715NumberingSystemE
	.section	.data.rel.ro.local._ZTVN6icu_6715NumberingSystemE,"awG",@progbits,_ZTVN6icu_6715NumberingSystemE,comdat
	.align 8
	.type	_ZTVN6icu_6715NumberingSystemE, @object
	.size	_ZTVN6icu_6715NumberingSystemE, 48
_ZTVN6icu_6715NumberingSystemE:
	.quad	0
	.quad	_ZTIN6icu_6715NumberingSystemE
	.quad	_ZN6icu_6715NumberingSystemD1Ev
	.quad	_ZN6icu_6715NumberingSystemD0Ev
	.quad	_ZNK6icu_6715NumberingSystem17getDynamicClassIDEv
	.quad	_ZNK6icu_6715NumberingSystem14getDescriptionEv
	.weak	_ZTVN6icu_6721NumsysNameEnumerationE
	.section	.data.rel.ro._ZTVN6icu_6721NumsysNameEnumerationE,"awG",@progbits,_ZTVN6icu_6721NumsysNameEnumerationE,comdat
	.align 8
	.type	_ZTVN6icu_6721NumsysNameEnumerationE, @object
	.size	_ZTVN6icu_6721NumsysNameEnumerationE, 104
_ZTVN6icu_6721NumsysNameEnumerationE:
	.quad	0
	.quad	_ZTIN6icu_6721NumsysNameEnumerationE
	.quad	_ZN6icu_6721NumsysNameEnumerationD1Ev
	.quad	_ZN6icu_6721NumsysNameEnumerationD0Ev
	.quad	_ZNK6icu_6721NumsysNameEnumeration17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringEnumeration5cloneEv
	.quad	_ZNK6icu_6721NumsysNameEnumeration5countER10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration4nextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode
	.quad	_ZN6icu_6721NumsysNameEnumeration5snextER10UErrorCode
	.quad	_ZN6icu_6721NumsysNameEnumeration5resetER10UErrorCode
	.quad	_ZNK6icu_6717StringEnumerationeqERKS0_
	.quad	_ZNK6icu_6717StringEnumerationneERKS0_
	.local	_ZN6icu_6712_GLOBAL__N_115gNumSysInitOnceE
	.comm	_ZN6icu_6712_GLOBAL__N_115gNumSysInitOnceE,8,8
	.local	_ZN6icu_6712_GLOBAL__N_112gNumsysNamesE
	.comm	_ZN6icu_6712_GLOBAL__N_112gNumsysNamesE,8,8
	.local	_ZZN6icu_6721NumsysNameEnumeration16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6721NumsysNameEnumeration16getStaticClassIDEvE7classID,1,1
	.local	_ZZN6icu_6715NumberingSystem16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6715NumberingSystem16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.type	_ZN6icu_67L5gLatnE, @object
	.size	_ZN6icu_67L5gLatnE, 5
_ZN6icu_67L5gLatnE:
	.string	"latn"
	.align 8
	.type	_ZN6icu_67L12gAlgorithmicE, @object
	.size	_ZN6icu_67L12gAlgorithmicE, 12
_ZN6icu_67L12gAlgorithmicE:
	.string	"algorithmic"
	.type	_ZN6icu_67L6gRadixE, @object
	.size	_ZN6icu_67L6gRadixE, 6
_ZN6icu_67L6gRadixE:
	.string	"radix"
	.type	_ZN6icu_67L5gDescE, @object
	.size	_ZN6icu_67L5gDescE, 5
_ZN6icu_67L5gDescE:
	.string	"desc"
	.align 8
	.type	_ZN6icu_67L8gFinanceE, @object
	.size	_ZN6icu_67L8gFinanceE, 8
_ZN6icu_67L8gFinanceE:
	.string	"finance"
	.align 8
	.type	_ZN6icu_67L12gTraditionalE, @object
	.size	_ZN6icu_67L12gTraditionalE, 12
_ZN6icu_67L12gTraditionalE:
	.string	"traditional"
	.type	_ZN6icu_67L7gNativeE, @object
	.size	_ZN6icu_67L7gNativeE, 7
_ZN6icu_67L7gNativeE:
	.string	"native"
	.align 8
	.type	_ZN6icu_67L8gDefaultE, @object
	.size	_ZN6icu_67L8gDefaultE, 8
_ZN6icu_67L8gDefaultE:
	.string	"default"
	.align 8
	.type	_ZN6icu_67L15gNumberElementsE, @object
	.size	_ZN6icu_67L15gNumberElementsE, 15
_ZN6icu_67L15gNumberElementsE:
	.string	"NumberElements"
	.align 16
	.type	_ZN6icu_67L17gNumberingSystemsE, @object
	.size	_ZN6icu_67L17gNumberingSystemsE, 17
_ZN6icu_67L17gNumberingSystemsE:
	.string	"numberingSystems"
	.section	.data.rel.ro.local,"aw"
	.align 8
.LC1:
	.quad	_ZTVN6icu_6715NumberingSystemE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
