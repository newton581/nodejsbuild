	.file	"ucal.cpp"
	.text
	.p2align 4
	.globl	ucal_openTimeZoneIDEnumeration_67
	.type	ucal_openTimeZoneIDEnumeration_67, @function
ucal_openTimeZoneIDEnumeration_67:
.LFB2390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	subq	$8, %rsp
	call	_ZN6icu_678TimeZone27createTimeZoneIDEnumerationE19USystemTimeZoneTypePKcPKiR10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uenum_openFromStringEnumeration_67@PLT
	.cfi_endproc
.LFE2390:
	.size	ucal_openTimeZoneIDEnumeration_67, .-ucal_openTimeZoneIDEnumeration_67
	.p2align 4
	.globl	ucal_openTimeZones_67
	.type	ucal_openTimeZones_67, @function
ucal_openTimeZones_67:
.LFB2391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_678TimeZone17createEnumerationEv@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uenum_openFromStringEnumeration_67@PLT
	.cfi_endproc
.LFE2391:
	.size	ucal_openTimeZones_67, .-ucal_openTimeZones_67
	.p2align 4
	.globl	ucal_openCountryTimeZones_67
	.type	ucal_openCountryTimeZones_67, @function
ucal_openCountryTimeZones_67:
.LFB2392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	_ZN6icu_678TimeZone17createEnumerationEPKc@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uenum_openFromStringEnumeration_67@PLT
	.cfi_endproc
.LFE2392:
	.size	ucal_openCountryTimeZones_67, .-ucal_openCountryTimeZones_67
	.p2align 4
	.globl	ucal_getDefaultTimeZone_67
	.type	ucal_getDefaultTimeZone_67, @function
ucal_getDefaultTimeZone_67:
.LFB2393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L8
	movq	%rdx, %r12
	movl	(%rdx), %edx
	testl	%edx, %edx
	jle	.L16
.L8:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	addq	$120, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	%esi, %r14d
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L18
	leaq	-128(%rbp), %r8
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	8(%r15), %rsi
	movq	%r8, %rdi
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movq	%r8, -152(%rbp)
	movw	%ax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movq	-152(%rbp), %r8
	movq	%r12, %rcx
	movl	%r14d, %edx
	leaq	-136(%rbp), %rsi
	movq	%rbx, -136(%rbp)
	movq	%r8, %rdi
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r13d
	movq	-152(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$7, (%r12)
	jmp	.L8
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2393:
	.size	ucal_getDefaultTimeZone_67, .-ucal_getDefaultTimeZone_67
	.p2align 4
	.globl	ucal_setDefaultTimeZone_67
	.type	ucal_setDefaultTimeZone_67, @function
ucal_setDefaultTimeZone_67:
.LFB2394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L19
	movl	(%rsi), %edx
	movq	%rsi, %rbx
	testl	%edx, %edx
	jg	.L19
	movq	%rdi, %r12
	leaq	-112(%rbp), %r13
	call	u_strlen_67@PLT
	leaq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -120(%rbp)
	movl	%eax, %ecx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$1, %esi
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdi
	call	_ZN6icu_678TimeZone14createTimeZoneERKNS_13UnicodeStringE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L27
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_678TimeZone12adoptDefaultEPS0_@PLT
.L19:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movl	$7, (%rbx)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L19
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2394:
	.size	ucal_setDefaultTimeZone_67, .-ucal_setDefaultTimeZone_67
	.p2align 4
	.globl	ucal_getHostTimeZone_67
	.type	ucal_getHostTimeZone_67, @function
ucal_getHostTimeZone_67:
.LFB2395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L29
	movq	%rdx, %r12
	movl	(%rdx), %edx
	testl	%edx, %edx
	jle	.L37
.L29:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	addq	$120, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	%esi, %r14d
	call	_ZN6icu_678TimeZone18detectHostTimeZoneEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L39
	leaq	-128(%rbp), %r8
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	8(%r15), %rsi
	movq	%r8, %rdi
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movq	%r8, -152(%rbp)
	movw	%ax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movq	-152(%rbp), %r8
	movq	%r12, %rcx
	movl	%r14d, %edx
	leaq	-136(%rbp), %rsi
	movq	%rbx, -136(%rbp)
	movq	%r8, %rdi
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r13d
	movq	-152(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$7, (%r12)
	jmp	.L29
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2395:
	.size	ucal_getHostTimeZone_67, .-ucal_getHostTimeZone_67
	.p2align 4
	.globl	ucal_getDSTSavings_67
	.type	ucal_getDSTSavings_67, @function
ucal_getDSTSavings_67:
.LFB2396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L61
	movl	(%rsi), %r8d
	movq	%rsi, %rbx
	testl	%r8d, %r8d
	jg	.L46
	movq	%rdi, %r12
	leaq	-136(%rbp), %r14
	leaq	-128(%rbp), %r13
	call	u_strlen_67@PLT
	movl	$2, %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movw	%si, -120(%rbp)
	movl	%eax, %ecx
	movl	$1, %esi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	%r12, -136(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdi
	call	_ZN6icu_678TimeZone14createTimeZoneERKNS_13UnicodeStringE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L62
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L53
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6714SimpleTimeZoneE(%rip), %rdx
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L45
	movq	(%rax), %rax
	call	*104(%rax)
.L47:
	movq	(%r12), %rdx
	movl	%eax, -152(%rbp)
	movq	%r12, %rdi
	call	*8(%rdx)
	movl	-152(%rbp), %eax
.L40:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L63
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movl	$7, (%rbx)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L45
.L46:
	xorl	%eax, %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L45:
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movl	$53, %r13d
	leaq	-140(%rbp), %r15
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L64:
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	jne	.L47
	movsd	-152(%rbp), %xmm0
	addsd	.LC0(%rip), %xmm0
	subl	$1, %r13d
	je	.L53
.L50:
	movq	(%r12), %rax
	movq	%r15, %rdx
	movq	%rbx, %r8
	movq	%r14, %rcx
	xorl	%esi, %esi
	movsd	%xmm0, -152(%rbp)
	movq	%r12, %rdi
	call	*48(%rax)
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L64
.L53:
	xorl	%eax, %eax
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L61:
	movl	0, %eax
	ud2
.L63:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2396:
	.size	ucal_getDSTSavings_67, .-ucal_getDSTSavings_67
	.p2align 4
	.globl	ucal_getNow_67
	.type	ucal_getNow_67, @function
ucal_getNow_67:
.LFB2397:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_678Calendar6getNowEv@PLT
	.cfi_endproc
.LFE2397:
	.size	ucal_getNow_67, .-ucal_getNow_67
	.p2align 4
	.globl	ucal_close_67
	.type	ucal_close_67, @function
ucal_close_67:
.LFB2399:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L66
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L66:
	ret
	.cfi_endproc
.LFE2399:
	.size	ucal_close_67, .-ucal_close_67
	.p2align 4
	.globl	ucal_clone_67
	.type	ucal_clone_67, @function
ucal_clone_67:
.LFB2400:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L72
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*24(%rax)
	testq	%rax, %rax
	je	.L75
.L68:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L68
	.cfi_endproc
.LFE2400:
	.size	ucal_clone_67, .-ucal_clone_67
	.p2align 4
	.globl	ucal_setTimeZone_67
	.type	ucal_setTimeZone_67, @function
ucal_setTimeZone_67:
.LFB2401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L76
	movq	%rdi, %r13
	testq	%rsi, %rsi
	je	.L90
	movq	%rcx, %r12
	movl	%edx, %ebx
	movl	%edx, %ecx
	testl	%edx, %edx
	js	.L91
.L81:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	shrl	$31, %ebx
	leaq	-128(%rbp), %r15
	movq	%rsi, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rdx
	movl	%ebx, %esi
	movq	%r15, %rdi
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	%r15, %rdi
	call	_ZN6icu_678TimeZone14createTimeZoneERKNS_13UnicodeStringE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L92
.L82:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L80:
	testq	%r14, %r14
	je	.L76
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_678Calendar13adoptTimeZoneEPNS_8TimeZoneE@PLT
.L76:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%rax, %r14
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$7, (%r12)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%rsi, %rdi
	movq	%rsi, -152(%rbp)
	call	u_strlen_67@PLT
	movq	-152(%rbp), %rsi
	movl	%eax, %ecx
	jmp	.L81
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2401:
	.size	ucal_setTimeZone_67, .-ucal_setTimeZone_67
	.p2align 4
	.globl	ucal_getTimeZoneID_67
	.type	ucal_getTimeZoneID_67, @function
ucal_getTimeZoneID_67:
.LFB2402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L94
	movq	%rsi, %rbx
	movl	%edx, %r13d
	leaq	-128(%rbp), %r15
	movq	%rcx, %r12
	call	_ZNK6icu_678Calendar11getTimeZoneEv@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%r15, %rdi
	movq	%rdx, -128(%rbp)
	leaq	8(%rax), %rsi
	movl	$2, %edx
	movw	%dx, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r12, %rcx
	movl	%r13d, %edx
	movq	%r15, %rdi
	leaq	-136(%rbp), %rsi
	movq	%rbx, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r14d
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L94:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$104, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L100:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2402:
	.size	ucal_getTimeZoneID_67, .-ucal_getTimeZoneID_67
	.p2align 4
	.globl	ucal_inDaylightTime_67
	.type	ucal_inDaylightTime_67, @function
ucal_inDaylightTime_67:
.LFB2404:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L102
	movq	(%rdi), %rax
	jmp	*96(%rax)
	.p2align 4,,10
	.p2align 3
.L102:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2404:
	.size	ucal_inDaylightTime_67, .-ucal_inDaylightTime_67
	.p2align 4
	.globl	ucal_setGregorianChange_67
	.type	ucal_setGregorianChange_67, @function
ucal_setGregorianChange_67:
.LFB2405:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L112
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$16, %rsp
	testq	%rdi, %rdi
	je	.L105
	movq	(%rdi), %rax
	movq	8+_ZTIN6icu_6717GregorianCalendarE(%rip), %rsi
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	cmpq	%rsi, %rdi
	je	.L106
	cmpb	$42, (%rdi)
	movsd	%xmm0, -24(%rbp)
	je	.L107
	call	strcmp@PLT
	movsd	-24(%rbp), %xmm0
	testl	%eax, %eax
	je	.L106
.L107:
	movl	$16, (%r12)
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	_ZTIN6icu_678CalendarE(%rip), %rsi
	xorl	%ecx, %ecx
	movsd	%xmm0, -24(%rbp)
	leaq	_ZTIN6icu_6717GregorianCalendarE(%rip), %rdx
	call	__dynamic_cast@PLT
	movsd	-24(%rbp), %xmm0
	addq	$16, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717GregorianCalendar18setGregorianChangeEdR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L112:
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$1, (%rsi)
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2405:
	.size	ucal_setGregorianChange_67, .-ucal_setGregorianChange_67
	.p2align 4
	.globl	ucal_getGregorianChange_67
	.type	ucal_getGregorianChange_67, @function
ucal_getGregorianChange_67:
.LFB2406:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L120
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.L117
	movq	(%rdi), %rax
	movq	8+_ZTIN6icu_6717GregorianCalendarE(%rip), %rsi
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	cmpq	%rsi, %rdi
	je	.L118
	cmpb	$42, (%rdi)
	je	.L119
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L118
.L119:
	movl	$16, (%rbx)
	pxor	%xmm0, %xmm0
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717GregorianCalendarE(%rip), %rdx
	leaq	_ZTIN6icu_678CalendarE(%rip), %rsi
	call	__dynamic_cast@PLT
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	movq	%rax, %rdi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6717GregorianCalendar18getGregorianChangeEv@PLT
	.p2align 4,,10
	.p2align 3
.L120:
	pxor	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$1, (%rsi)
	pxor	%xmm0, %xmm0
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2406:
	.size	ucal_getGregorianChange_67, .-ucal_getGregorianChange_67
	.section	.text.unlikely,"ax",@progbits
.LCOLDB2:
	.text
.LHOTB2:
	.p2align 4
	.globl	ucal_getAttribute_67
	.type	ucal_getAttribute_67, @function
ucal_getAttribute_67:
.LFB2407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$4, %esi
	ja	.L136
	leaq	.L131(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L131:
	.long	.L135-.L131
	.long	.L134-.L131
	.long	.L133-.L131
	.long	.L132-.L131
	.long	.L130-.L131
	.text
	.p2align 4,,10
	.p2align 3
.L132:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_678Calendar25getRepeatedWallTimeOptionEv@PLT
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_678Calendar24getSkippedWallTimeOptionEv@PLT
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	call	_ZNK6icu_678Calendar9isLenientEv@PLT
	movsbl	%al, %eax
.L128:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_678Calendar17getFirstDayOfWeekEv@PLT
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	call	_ZNK6icu_678Calendar25getMinimalDaysInFirstWeekEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	ucal_getAttribute_67.cold, @function
ucal_getAttribute_67.cold:
.LFSB2407:
.L136:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	orl	$-1, %eax
	jmp	.L128
	.cfi_endproc
.LFE2407:
	.text
	.size	ucal_getAttribute_67, .-ucal_getAttribute_67
	.section	.text.unlikely
	.size	ucal_getAttribute_67.cold, .-ucal_getAttribute_67.cold
.LCOLDE2:
	.text
.LHOTE2:
	.p2align 4
	.globl	ucal_setAttribute_67
	.type	ucal_setAttribute_67, @function
ucal_setAttribute_67:
.LFB2408:
	.cfi_startproc
	endbr64
	cmpl	$4, %esi
	ja	.L138
	leaq	.L141(%rip), %rcx
	movl	%esi, %esi
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L141:
	.long	.L145-.L141
	.long	.L144-.L141
	.long	.L143-.L141
	.long	.L142-.L141
	.long	.L140-.L141
	.text
	.p2align 4,,10
	.p2align 3
.L142:
	movl	%edx, %esi
	jmp	_ZN6icu_678Calendar25setRepeatedWallTimeOptionE23UCalendarWallTimeOption@PLT
	.p2align 4,,10
	.p2align 3
.L140:
	movl	%edx, %esi
	jmp	_ZN6icu_678Calendar24setSkippedWallTimeOptionE23UCalendarWallTimeOption@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	movsbl	%dl, %esi
	jmp	_ZN6icu_678Calendar10setLenientEa@PLT
	.p2align 4,,10
	.p2align 3
.L144:
	movl	%edx, %esi
	jmp	_ZN6icu_678Calendar17setFirstDayOfWeekE19UCalendarDaysOfWeek@PLT
	.p2align 4,,10
	.p2align 3
.L143:
	movzbl	%dl, %esi
	jmp	_ZN6icu_678Calendar25setMinimalDaysInFirstWeekEh@PLT
.L138:
	ret
	.cfi_endproc
.LFE2408:
	.size	ucal_setAttribute_67, .-ucal_setAttribute_67
	.p2align 4
	.globl	ucal_getAvailable_67
	.type	ucal_getAvailable_67, @function
ucal_getAvailable_67:
.LFB2409:
	.cfi_startproc
	endbr64
	jmp	uloc_getAvailable_67@PLT
	.cfi_endproc
.LFE2409:
	.size	ucal_getAvailable_67, .-ucal_getAvailable_67
	.p2align 4
	.globl	ucal_countAvailable_67
	.type	ucal_countAvailable_67, @function
ucal_countAvailable_67:
.LFB2410:
	.cfi_startproc
	endbr64
	jmp	uloc_countAvailable_67@PLT
	.cfi_endproc
.LFE2410:
	.size	ucal_countAvailable_67, .-ucal_countAvailable_67
	.p2align 4
	.globl	ucal_getMillis_67
	.type	ucal_getMillis_67, @function
ucal_getMillis_67:
.LFB2411:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L150
	pxor	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	jmp	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	.cfi_endproc
.LFE2411:
	.size	ucal_getMillis_67, .-ucal_getMillis_67
	.p2align 4
	.globl	ucal_setMillis_67
	.type	ucal_setMillis_67, @function
ucal_setMillis_67:
.LFB2412:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L153
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE2412:
	.size	ucal_setMillis_67, .-ucal_setMillis_67
	.p2align 4
	.globl	ucal_setDate_67
	.type	ucal_setDate_67, @function
ucal_setDate_67:
.LFB2413:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L156
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	jmp	_ZN6icu_678Calendar3setEiii@PLT
	.cfi_endproc
.LFE2413:
	.size	ucal_setDate_67, .-ucal_setDate_67
	.p2align 4
	.globl	ucal_setDateTime_67
	.type	ucal_setDateTime_67, @function
ucal_setDateTime_67:
.LFB2414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	24(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L160
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar3setEiiiiii@PLT
	.cfi_endproc
.LFE2414:
	.size	ucal_setDateTime_67, .-ucal_setDateTime_67
	.p2align 4
	.globl	ucal_equivalentTo_67
	.type	ucal_equivalentTo_67, @function
ucal_equivalentTo_67:
.LFB2415:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE2415:
	.size	ucal_equivalentTo_67, .-ucal_equivalentTo_67
	.p2align 4
	.globl	ucal_add_67
	.type	ucal_add_67, @function
ucal_add_67:
.LFB2416:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L162
	movq	(%rdi), %rax
	jmp	*56(%rax)
	.p2align 4,,10
	.p2align 3
.L162:
	ret
	.cfi_endproc
.LFE2416:
	.size	ucal_add_67, .-ucal_add_67
	.p2align 4
	.globl	ucal_roll_67
	.type	ucal_roll_67, @function
ucal_roll_67:
.LFB2417:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L164
	movq	(%rdi), %rax
	jmp	*72(%rax)
	.p2align 4,,10
	.p2align 3
.L164:
	ret
	.cfi_endproc
.LFE2417:
	.size	ucal_roll_67, .-ucal_roll_67
	.p2align 4
	.globl	ucal_get_67
	.type	ucal_get_67, @function
ucal_get_67:
.LFB2418:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L167
	jmp	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2418:
	.size	ucal_get_67, .-ucal_get_67
	.p2align 4
	.globl	ucal_set_67
	.type	ucal_set_67, @function
ucal_set_67:
.LFB2419:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	.cfi_endproc
.LFE2419:
	.size	ucal_set_67, .-ucal_set_67
	.p2align 4
	.globl	ucal_isSet_67
	.type	ucal_isSet_67, @function
ucal_isSet_67:
.LFB2420:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_678Calendar5isSetE19UCalendarDateFields@PLT
	.cfi_endproc
.LFE2420:
	.size	ucal_isSet_67, .-ucal_isSet_67
	.p2align 4
	.globl	ucal_clearField_67
	.type	ucal_clearField_67, @function
ucal_clearField_67:
.LFB2421:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_678Calendar5clearE19UCalendarDateFields@PLT
	.cfi_endproc
.LFE2421:
	.size	ucal_clearField_67, .-ucal_clearField_67
	.p2align 4
	.globl	ucal_clear_67
	.type	ucal_clear_67, @function
ucal_clear_67:
.LFB2422:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_678Calendar5clearEv@PLT
	.cfi_endproc
.LFE2422:
	.size	ucal_clear_67, .-ucal_clear_67
	.p2align 4
	.globl	ucal_getLimit_67
	.type	ucal_getLimit_67, @function
ucal_getLimit_67:
.LFB2423:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	testq	%rcx, %rcx
	je	.L172
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L172
	cmpl	$5, %edx
	ja	.L172
	leaq	.L175(%rip), %rcx
	movl	%edx, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L175:
	.long	.L180-.L175
	.long	.L179-.L175
	.long	.L178-.L175
	.long	.L177-.L175
	.long	.L176-.L175
	.long	.L174-.L175
	.text
.L176:
	movq	(%rdi), %rax
	movq	%r8, %rdx
	movq	168(%rax), %rax
	jmp	*%rax
.L174:
	movq	(%rdi), %rax
	movq	%r8, %rdx
	movq	176(%rax), %rax
	jmp	*%rax
.L180:
	movq	(%rdi), %rax
	jmp	*112(%rax)
.L179:
	movq	(%rdi), %rax
	jmp	*128(%rax)
.L178:
	movq	(%rdi), %rax
	jmp	*144(%rax)
.L177:
	movq	(%rdi), %rax
	jmp	*160(%rax)
.L172:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2423:
	.size	ucal_getLimit_67, .-ucal_getLimit_67
	.p2align 4
	.globl	ucal_getLocaleByType_67
	.type	ucal_getLocaleByType_67, @function
ucal_getLocaleByType_67:
.LFB2424:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L189
	jmp	_ZNK6icu_678Calendar11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L189:
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L185
	movl	$1, (%rdx)
.L185:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2424:
	.size	ucal_getLocaleByType_67, .-ucal_getLocaleByType_67
	.p2align 4
	.globl	ucal_getTZDataVersion_67
	.type	ucal_getTZDataVersion_67, @function
ucal_getTZDataVersion_67:
.LFB2425:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_678TimeZone16getTZDataVersionER10UErrorCode@PLT
	.cfi_endproc
.LFE2425:
	.size	ucal_getTZDataVersion_67, .-ucal_getTZDataVersion_67
	.p2align 4
	.globl	ucal_getCanonicalTimeZoneID_67
	.type	ucal_getCanonicalTimeZoneID_67, @function
ucal_getCanonicalTimeZoneID_67:
.LFB2426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L200
	movq	%r9, %r12
	movl	(%r9), %r9d
	testl	%r9d, %r9d
	jg	.L191
	movq	%rdx, %rbx
	movl	%ecx, %r13d
	testq	%r8, %r8
	je	.L193
	movb	$0, (%r8)
.L193:
	testq	%rdi, %rdi
	je	.L194
	testl	%esi, %esi
	je	.L194
	testq	%rbx, %rbx
	je	.L194
	testl	%r13d, %r13d
	jle	.L194
	leaq	-128(%rbp), %r14
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	%esi, %edx
	movq	%rdi, %rsi
	movq	%rax, -192(%rbp)
	movq	%r14, %rdi
	movl	$2, %eax
	leaq	-192(%rbp), %r15
	movq	%r8, -216(%rbp)
	movw	%ax, -184(%rbp)
	movb	$0, -201(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	leaq	-201(%rbp), %rdx
	call	_ZN6icu_678TimeZone14getCanonicalIDERKNS_13UnicodeStringERS1_RaR10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r12), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L210
.L197:
	movq	%r15, %rdi
	movl	%eax, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-216(%rbp), %eax
	.p2align 4,,10
	.p2align 3
.L191:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L211
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L200:
	xorl	%eax, %eax
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L210:
	movq	-216(%rbp), %r8
	testq	%r8, %r8
	je	.L198
	movzbl	-201(%rbp), %eax
	movb	%al, (%r8)
.L198:
	leaq	-200(%rbp), %rsi
	movq	%r12, %rcx
	movl	%r13d, %edx
	movq	%r15, %rdi
	movq	%rbx, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	jmp	.L197
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2426:
	.size	ucal_getCanonicalTimeZoneID_67, .-ucal_getCanonicalTimeZoneID_67
	.p2align 4
	.globl	ucal_getType_67
	.type	ucal_getType_67, @function
ucal_getType_67:
.LFB2427:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L213
	movq	(%rdi), %rax
	jmp	*184(%rax)
	.p2align 4,,10
	.p2align 3
.L213:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2427:
	.size	ucal_getType_67, .-ucal_getType_67
	.p2align 4
	.globl	ucal_getDayOfWeekType_67
	.type	ucal_getDayOfWeekType_67, @function
ucal_getDayOfWeekType_67:
.LFB2428:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L215
	movq	(%rdi), %rax
	jmp	*192(%rax)
	.p2align 4,,10
	.p2align 3
.L215:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2428:
	.size	ucal_getDayOfWeekType_67, .-ucal_getDayOfWeekType_67
	.p2align 4
	.globl	ucal_getWeekendTransition_67
	.type	ucal_getWeekendTransition_67, @function
ucal_getWeekendTransition_67:
.LFB2429:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L217
	movq	(%rdi), %rax
	jmp	*200(%rax)
	.p2align 4,,10
	.p2align 3
.L217:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2429:
	.size	ucal_getWeekendTransition_67, .-ucal_getWeekendTransition_67
	.p2align 4
	.globl	ucal_isWeekend_67
	.type	ucal_isWeekend_67, @function
ucal_isWeekend_67:
.LFB2430:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L219
	movq	(%rdi), %rax
	jmp	*208(%rax)
	.p2align 4,,10
	.p2align 3
.L219:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2430:
	.size	ucal_isWeekend_67, .-ucal_isWeekend_67
	.p2align 4
	.globl	ucal_getFieldDifference_67
	.type	ucal_getFieldDifference_67, @function
ucal_getFieldDifference_67:
.LFB2431:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L221
	movq	(%rdi), %rax
	jmp	*88(%rax)
	.p2align 4,,10
	.p2align 3
.L221:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2431:
	.size	ucal_getFieldDifference_67, .-ucal_getFieldDifference_67
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"gregorian"
.LC4:
	.string	"supplementalData"
.LC5:
	.string	"calendarPreferenceData"
.LC6:
	.string	"001"
	.text
	.p2align 4
	.globl	ucal_getKeywordValuesForLocale_67
	.type	ucal_getKeywordValuesForLocale_67, @function
ucal_getKeywordValuesForLocale_67:
.LFB2432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rsi, %rdi
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-60(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	movl	$4, %ecx
	subq	$56, %rsp
	movl	%edx, -84(%rbp)
	movq	%r13, %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ulocimp_getRegionForSupplementalData_67@PLT
	movq	%rbx, %rdx
	leaq	.LC4(%rip), %rsi
	xorl	%edi, %edi
	call	ures_openDirect_67@PLT
	movq	%rbx, %rcx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdx
	movq	%rax, %rdi
	call	ures_getByKey_67@PLT
	movq	%r13, %rsi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	ures_getByKey_67@PLT
	movq	%rax, %r13
	movl	(%rbx), %eax
	testq	%r12, %r12
	je	.L223
	cmpl	$2, %eax
	je	.L256
.L223:
	testl	%eax, %eax
	jle	.L224
.L255:
	movq	%r13, %rdi
	call	ures_close_67@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ures_close_67@PLT
.L222:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L257
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	ulist_createEmptyList_67@PLT
	movl	(%rbx), %esi
	movq	%rax, %r15
	testl	%esi, %esi
	jle	.L258
.L226:
	movq	%r13, %rdi
	call	ures_close_67@PLT
	movq	%r12, %rdi
	call	ures_close_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L242
	testq	%r15, %r15
	je	.L242
	movl	$56, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L259
	movq	%r15, %rdi
	call	ulist_resetList_67@PLT
	movdqa	_ZL20defaultKeywordValues(%rip), %xmm0
	movdqa	16+_ZL20defaultKeywordValues(%rip), %xmm1
	movdqa	32+_ZL20defaultKeywordValues(%rip), %xmm2
	movq	48+_ZL20defaultKeywordValues(%rip), %rax
	movups	%xmm0, (%r12)
	movq	%rax, 48(%r12)
	movq	%r15, 8(%r12)
	movups	%xmm1, 16(%r12)
	movups	%xmm2, 32(%r12)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L256:
	movl	$0, (%rbx)
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	call	ures_getByKey_67@PLT
	movq	%rax, %r13
	movl	(%rbx), %eax
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L258:
	leaq	-64(%rbp), %rax
	xorl	%r14d, %r14d
	movq	%rax, -80(%rbp)
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L227:
	movq	-80(%rbp), %rdx
	movl	%r14d, %esi
	movq	%rbx, %rcx
	movq	%r13, %rdi
	call	ures_getStringByIndex_67@PLT
	movq	%rax, -72(%rbp)
	movl	-64(%rbp), %eax
	leal	1(%rax), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	-72(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L260
	movl	-64(%rbp), %edx
	movq	%r8, %rdi
	movq	%rax, -72(%rbp)
	call	u_UCharsToChars_67@PLT
	movslq	-64(%rbp), %rax
	movq	-72(%rbp), %rsi
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	movb	$0, (%rsi,%rax)
	call	ulist_addItemEndList_67@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L232
	addl	$1, %r14d
.L234:
	movq	%r13, %rdi
	call	ures_getSize_67@PLT
	cmpl	%r14d, %eax
	jg	.L227
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L232
	leaq	8+_ZL9CAL_TYPES(%rip), %rcx
	cmpb	$0, -84(%rbp)
	movl	$9, %edx
	movq	%rcx, -72(%rbp)
	leaq	.LC3(%rip), %r14
	je	.L239
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L238:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	-8(%rax), %r14
	testq	%r14, %r14
	je	.L261
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
.L239:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	ulist_containsString_67@PLT
	testb	%al, %al
	jne	.L238
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	ulist_addItemEndList_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L238
	.p2align 4,,10
	.p2align 3
.L232:
	movq	%r15, %rdi
	call	ulist_deleteList_67@PLT
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L242:
	xorl	%r12d, %r12d
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L261:
	movl	(%rbx), %eax
.L230:
	testl	%eax, %eax
	jle	.L226
	jmp	.L232
.L260:
	movl	$7, (%rbx)
	jmp	.L232
.L257:
	call	__stack_chk_fail@PLT
.L259:
	movl	$7, (%rbx)
	movq	%r15, %rdi
	call	ulist_deleteList_67@PLT
	jmp	.L222
	.cfi_endproc
.LFE2432:
	.size	ucal_getKeywordValuesForLocale_67, .-ucal_getKeywordValuesForLocale_67
	.p2align 4
	.globl	ucal_getWindowsTimeZoneID_67
	.type	ucal_getWindowsTimeZoneID_67, @function
ucal_getWindowsTimeZoneID_67:
.LFB2434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L272
.L262:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L273
	addq	$184, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	leaq	-128(%rbp), %r15
	movq	%rdx, %rbx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	%esi, %edx
	movq	%rax, -192(%rbp)
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%r8, %r12
	movl	$2, %eax
	movl	%ecx, %r14d
	movw	%ax, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	leaq	-192(%rbp), %r8
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%r8, %rsi
	movq	%r8, -216(%rbp)
	call	_ZN6icu_678TimeZone12getWindowsIDERKNS_13UnicodeStringERS1_R10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r12), %edx
	movq	-216(%rbp), %r8
	testl	%edx, %edx
	jle	.L274
.L264:
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L274:
	movswl	-184(%rbp), %r13d
	testw	%r13w, %r13w
	js	.L265
	sarl	$5, %r13d
.L266:
	testl	%r13d, %r13d
	jg	.L275
	xorl	%r13d, %r13d
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L275:
	leaq	-200(%rbp), %rsi
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%r8, %rdi
	movq	%rbx, -200(%rbp)
	movq	%r8, -216(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movq	-216(%rbp), %r8
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L265:
	movl	-180(%rbp), %r13d
	jmp	.L266
.L273:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2434:
	.size	ucal_getWindowsTimeZoneID_67, .-ucal_getWindowsTimeZoneID_67
	.p2align 4
	.globl	ucal_getTimeZoneIDForWindowsID_67
	.type	ucal_getTimeZoneIDForWindowsID_67, @function
ucal_getTimeZoneIDForWindowsID_67:
.LFB2435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L286
.L276:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L287
	addq	$184, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	leaq	-128(%rbp), %r15
	movq	%rdx, %r14
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	%esi, %edx
	movq	%rax, -192(%rbp)
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%r9, %r12
	movl	$2, %eax
	movl	%r8d, -220(%rbp)
	movq	%rcx, %rbx
	movw	%ax, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	-192(%rbp), %r8
	movq	%r8, %rdx
	movq	%r8, -216(%rbp)
	call	_ZN6icu_678TimeZone17getIDForWindowsIDERKNS_13UnicodeStringEPKcRS1_R10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r12), %edx
	movq	-216(%rbp), %r8
	testl	%edx, %edx
	jle	.L288
.L278:
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L288:
	movswl	-184(%rbp), %r13d
	movl	-220(%rbp), %r10d
	testw	%r13w, %r13w
	js	.L279
	sarl	$5, %r13d
.L280:
	testl	%r13d, %r13d
	jg	.L289
	xorl	%r13d, %r13d
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L289:
	leaq	-200(%rbp), %rsi
	movq	%r12, %rcx
	movl	%r10d, %edx
	movq	%r8, %rdi
	movq	%rbx, -200(%rbp)
	movq	%r8, -216(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movq	-216(%rbp), %r8
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L279:
	movl	-180(%rbp), %r13d
	jmp	.L280
.L287:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2435:
	.size	ucal_getTimeZoneIDForWindowsID_67, .-ucal_getTimeZoneIDForWindowsID_67
	.section	.rodata.str1.1
.LC7:
	.string	"calendar"
	.text
	.p2align 4
	.globl	ucal_open_67
	.type	ucal_open_67, @function
ucal_open_67:
.LFB2398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L309
	movq	%rdx, %r14
	movl	%ecx, %r15d
	movq	%r8, %r12
	testq	%rdi, %rdi
	je	.L310
	movl	%esi, %ecx
	testl	%esi, %esi
	js	.L311
.L295:
	movl	$2, %edx
	leaq	-544(%rbp), %rbx
	shrl	$31, %esi
	movq	%rdi, -552(%rbp)
	movw	%dx, -536(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rbx, %rdi
	leaq	-552(%rbp), %rdx
	movq	%rax, -544(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_678TimeZone14createTimeZoneERKNS_13UnicodeStringE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L312
.L296:
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L294:
	movl	(%r12), %eax
	testq	%r13, %r13
	je	.L313
	testl	%eax, %eax
	jle	.L314
.L303:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L309:
	xorl	%eax, %eax
.L290:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L315
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	cmpl	$1, %r15d
	jne	.L299
	testq	%r14, %r14
	je	.L316
.L300:
	movq	%r14, %rdi
	call	strlen@PLT
	cmpl	$257, %eax
	jle	.L301
	movq	0(%r13), %rax
	movl	$1, (%r12)
	movq	%r13, %rdi
	call	*8(%rax)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L313:
	testl	%eax, %eax
	jg	.L309
	movl	$7, (%r12)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L310:
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%rax, %r13
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L299:
	leaq	-544(%rbp), %r15
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_678Calendar14createInstanceEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-568(%rbp), %rax
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L312:
	movl	$7, (%r12)
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L311:
	movl	%esi, -572(%rbp)
	movq	%rdi, -568(%rbp)
	call	u_strlen_67@PLT
	movl	-572(%rbp), %esi
	movq	-568(%rbp), %rdi
	movl	%eax, %ecx
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L301:
	leaq	-320(%rbp), %r15
	leaq	1(%rax), %rdx
	movl	$258, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	__memcpy_chk@PLT
	movq	%r12, %r8
	movl	$258, %ecx
	movq	%r15, %rdx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	uloc_setKeywordValue_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L303
	leaq	-544(%rbp), %r14
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_678Calendar14createInstanceEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	movq	%r14, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-568(%rbp), %rax
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L316:
	call	uloc_getDefault_67@PLT
	movq	%rax, %r14
	jmp	.L300
.L315:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2398:
	.size	ucal_open_67, .-ucal_open_67
	.p2align 4
	.globl	ucal_getTimeZoneDisplayName_67
	.type	ucal_getTimeZoneDisplayName_67, @function
ucal_getTimeZoneDisplayName_67:
.LFB2403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -376(%rbp)
	movl	(%r9), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L328
	movl	%esi, %ebx
	movq	%rcx, %r12
	movl	%r8d, %r13d
	movq	%r9, %r14
	call	_ZNK6icu_678Calendar11getTimeZoneEv@PLT
	movq	%rax, %r10
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -352(%rbp)
	movl	$2, %eax
	movw	%ax, -344(%rbp)
	testq	%r12, %r12
	jne	.L329
	leaq	-352(%rbp), %r15
	testl	%r13d, %r13d
	jne	.L329
	cmpl	$2, %ebx
	je	.L321
.L339:
	ja	.L322
	testl	%ebx, %ebx
	je	.L337
	movq	-376(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-288(%rbp), %rbx
	movq	%r10, -384(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r15, %r8
	movq	%rbx, %rcx
	movl	$1, %edx
	xorl	%esi, %esi
.L335:
	movq	-384(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK6icu_678TimeZone14getDisplayNameEaNS0_12EDisplayTypeERKNS_6LocaleERNS_13UnicodeStringE@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
.L326:
	leaq	-360(%rbp), %rsi
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r15, %rdi
	movq	%r12, -360(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L317:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	addq	$344, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	cmpl	$3, %ebx
	jne	.L326
	movq	-376(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-288(%rbp), %rbx
	movq	%r10, -384(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r15, %r8
	movq	%rbx, %rcx
	movl	$1, %edx
	movl	$1, %esi
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L329:
	leaq	-352(%rbp), %r15
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r10, -384(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movq	-384(%rbp), %r10
	cmpl	$2, %ebx
	jne	.L339
.L321:
	movq	-376(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-288(%rbp), %rbx
	movq	%r10, -384(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r15, %r8
	movq	%rbx, %rcx
	movl	$2, %edx
	movl	$1, %esi
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L337:
	movq	-376(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-288(%rbp), %rbx
	movq	%r10, -384(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r15, %r8
	movq	%rbx, %rcx
	movl	$2, %edx
	xorl	%esi, %esi
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L328:
	movl	$-1, %r12d
	jmp	.L317
.L338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2403:
	.size	ucal_getTimeZoneDisplayName_67, .-ucal_getTimeZoneDisplayName_67
	.p2align 4
	.globl	ucal_getTimeZoneTransitionDate_67
	.type	ucal_getTimeZoneTransitionDate_67, @function
ucal_getTimeZoneTransitionDate_67:
.LFB2433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L341
.L343:
	xorl	%eax, %eax
.L340:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L355
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	.cfi_restore_state
	movq	%rdi, %r12
	movl	%esi, %r13d
	movq	%rcx, %rsi
	movq	%rdx, %r14
	movq	%rcx, %rbx
	call	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -104(%rbp)
	call	_ZNK6icu_678Calendar11getTimeZoneEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L343
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713BasicTimeZoneE(%rip), %rdx
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L343
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L343
	leaq	-96(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_6718TimeZoneTransitionC1Ev@PLT
	movl	%r13d, %eax
	xorl	%esi, %esi
	movsd	-104(%rbp), %xmm0
	andl	$-3, %eax
	movq	%r15, %rdx
	movq	%r12, %rdi
	cmpl	$1, %eax
	movq	(%r12), %rax
	sete	%sil
	cmpl	$1, %r13d
	jbe	.L356
	call	*120(%rax)
.L345:
	testb	%al, %al
	jne	.L357
	movq	%r15, %rdi
	movb	%al, -104(%rbp)
	call	_ZN6icu_6718TimeZoneTransitionD1Ev@PLT
	movzbl	-104(%rbp), %eax
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L356:
	call	*112(%rax)
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L357:
	movq	%r15, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movq	%r15, %rdi
	movsd	%xmm0, (%r14)
	call	_ZN6icu_6718TimeZoneTransitionD1Ev@PLT
	movl	$1, %eax
	jmp	.L340
.L355:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2433:
	.size	ucal_getTimeZoneTransitionDate_67, .-ucal_getTimeZoneTransitionDate_67
	.section	.rodata.str1.1
.LC8:
	.string	"japanese"
.LC9:
	.string	"buddhist"
.LC10:
	.string	"roc"
.LC11:
	.string	"persian"
.LC12:
	.string	"islamic-civil"
.LC13:
	.string	"islamic"
.LC14:
	.string	"hebrew"
.LC15:
	.string	"chinese"
.LC16:
	.string	"indian"
.LC17:
	.string	"coptic"
.LC18:
	.string	"ethiopic"
.LC19:
	.string	"ethiopic-amete-alem"
.LC20:
	.string	"iso8601"
.LC21:
	.string	"dangi"
.LC22:
	.string	"islamic-umalqura"
.LC23:
	.string	"islamic-tbla"
.LC24:
	.string	"islamic-rgsa"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL9CAL_TYPES, @object
	.size	_ZL9CAL_TYPES, 152
_ZL9CAL_TYPES:
	.quad	.LC3
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.quad	0
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL20defaultKeywordValues, @object
	.size	_ZL20defaultKeywordValues, 56
_ZL20defaultKeywordValues:
	.quad	0
	.quad	0
	.quad	ulist_close_keyword_values_iterator_67
	.quad	ulist_count_keyword_values_67
	.quad	uenum_unextDefault_67
	.quad	ulist_next_keyword_value_67
	.quad	ulist_reset_keyword_values_iterator_67
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1103234626
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
