	.file	"numrange_impl.cpp"
	.text
	.section	.text._ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.type	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, @function
_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode:
.LFB3509:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3509:
	.size	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, .-_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.section	.text._ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv
	.type	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv, @function
_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv:
.LFB3510:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3510:
	.size	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv, .-_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv
	.section	.text._ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv
	.type	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv, @function
_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv:
.LFB3511:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3511:
	.size	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv, .-_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv
	.section	.text._ZNK6icu_676number4impl13EmptyModifier8isStrongEv,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier8isStrongEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv
	.type	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv, @function
_ZNK6icu_676number4impl13EmptyModifier8isStrongEv:
.LFB3512:
	.cfi_startproc
	endbr64
	movzbl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3512:
	.size	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv, .-_ZNK6icu_676number4impl13EmptyModifier8isStrongEv
	.section	.text._ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.type	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE, @function
_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE:
.LFB3513:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3513:
	.size	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE, .-_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.section	.text._ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE
	.type	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE, @function
_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE:
.LFB3514:
	.cfi_startproc
	endbr64
	movq	$0, (%rsi)
	ret
	.cfi_endproc
.LFE3514:
	.size	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE, .-_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE
	.section	.text._ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE
	.type	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE, @function
_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE:
.LFB3515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*32(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE3515:
	.size	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE, .-_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE
	.section	.text._ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE,"axG",@progbits,_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.type	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE, @function
_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE:
.LFB3518:
	.cfi_startproc
	endbr64
	leal	(%rsi,%rdx,4), %eax
	cltq
	movq	8(%rdi,%rax,8), %rax
	testq	%rax, %rax
	jne	.L10
	cmpl	$5, %edx
	je	.L10
	addl	$20, %esi
	movslq	%esi, %rsi
	movq	8(%rdi,%rsi,8), %rax
.L10:
	ret
	.cfi_endproc
.LFE3518:
	.size	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE, .-_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_120PluralRangesDataSinkD2Ev, @function
_ZN12_GLOBAL__N_120PluralRangesDataSinkD2Ev:
.LFB5048:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_120PluralRangesDataSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE5048:
	.size	_ZN12_GLOBAL__N_120PluralRangesDataSinkD2Ev, .-_ZN12_GLOBAL__N_120PluralRangesDataSinkD2Ev
	.set	_ZN12_GLOBAL__N_120PluralRangesDataSinkD1Ev,_ZN12_GLOBAL__N_120PluralRangesDataSinkD2Ev
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_120PluralRangesDataSinkD0Ev, @function
_ZN12_GLOBAL__N_120PluralRangesDataSinkD0Ev:
.LFB5050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_120PluralRangesDataSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE5050:
	.size	_ZN12_GLOBAL__N_120PluralRangesDataSinkD0Ev, .-_ZN12_GLOBAL__N_120PluralRangesDataSinkD0Ev
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_119NumberRangeDataSinkD2Ev, @function
_ZN12_GLOBAL__N_119NumberRangeDataSinkD2Ev:
.LFB5052:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_119NumberRangeDataSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE5052:
	.size	_ZN12_GLOBAL__N_119NumberRangeDataSinkD2Ev, .-_ZN12_GLOBAL__N_119NumberRangeDataSinkD2Ev
	.set	_ZN12_GLOBAL__N_119NumberRangeDataSinkD1Ev,_ZN12_GLOBAL__N_119NumberRangeDataSinkD2Ev
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_119NumberRangeDataSinkD0Ev, @function
_ZN12_GLOBAL__N_119NumberRangeDataSinkD0Ev:
.LFB5054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_119NumberRangeDataSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE5054:
	.size	_ZN12_GLOBAL__N_119NumberRangeDataSinkD0Ev, .-_ZN12_GLOBAL__N_119NumberRangeDataSinkD0Ev
	.section	.text._ZN6icu_676number4impl13EmptyModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl13EmptyModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl13EmptyModifierD2Ev
	.type	_ZN6icu_676number4impl13EmptyModifierD2Ev, @function
_ZN6icu_676number4impl13EmptyModifierD2Ev:
.LFB5060:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	.cfi_endproc
.LFE5060:
	.size	_ZN6icu_676number4impl13EmptyModifierD2Ev, .-_ZN6icu_676number4impl13EmptyModifierD2Ev
	.weak	_ZN6icu_676number4impl13EmptyModifierD1Ev
	.set	_ZN6icu_676number4impl13EmptyModifierD1Ev,_ZN6icu_676number4impl13EmptyModifierD2Ev
	.section	.text._ZN6icu_676number4impl13EmptyModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl13EmptyModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl13EmptyModifierD0Ev
	.type	_ZN6icu_676number4impl13EmptyModifierD0Ev, @function
_ZN6icu_676number4impl13EmptyModifierD0Ev:
.LFB5062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE5062:
	.size	_ZN6icu_676number4impl13EmptyModifierD0Ev, .-_ZN6icu_676number4impl13EmptyModifierD0Ev
	.section	.text._ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode,"axG",@progbits,_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode
	.type	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode, @function
_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode:
.LFB3620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpq	%rdx, %rdi
	je	.L31
	movdqu	8(%rdi), %xmm0
	movq	%rdx, %r12
	leaq	200(%rdi), %rsi
	movups	%xmm0, 8(%rdx)
	movdqu	24(%rdi), %xmm1
	movups	%xmm1, 24(%rdx)
	movl	40(%rdi), %eax
	movl	%eax, 40(%rdx)
	movzbl	44(%rdi), %eax
	movb	%al, 44(%rdx)
	movq	48(%rdi), %rax
	movq	%rax, 48(%rdx)
	movl	56(%rdi), %eax
	movl	%eax, 56(%rdx)
	movq	60(%rdi), %rax
	movq	%rax, 60(%rdx)
	movl	68(%rdi), %eax
	movl	%eax, 68(%rdx)
	movq	72(%rdi), %rax
	movq	%rax, 72(%rdx)
	movzbl	80(%rdi), %eax
	movb	%al, 80(%rdx)
	movl	84(%rdi), %eax
	movl	%eax, 84(%rdx)
	movl	88(%rdi), %eax
	movl	%eax, 88(%rdx)
	movzbl	92(%rdi), %eax
	movb	%al, 92(%rdx)
	movzbl	93(%rdi), %eax
	movb	%al, 93(%rdx)
	movzbl	94(%rdi), %eax
	movb	%al, 94(%rdx)
	movzbl	95(%rdi), %eax
	movb	%al, 95(%rdx)
	movzbl	96(%rdi), %eax
	movb	%al, 96(%rdx)
	movzbl	97(%rdi), %eax
	movb	%al, 97(%rdx)
	movzbl	98(%rdi), %eax
	movb	%al, 98(%rdx)
	movzbl	99(%rdi), %eax
	movb	%al, 99(%rdx)
	movzbl	100(%rdi), %eax
	movb	%al, 100(%rdx)
	movzbl	101(%rdi), %eax
	movb	%al, 101(%rdx)
	movq	104(%rdi), %rax
	movq	%rax, 104(%rdx)
	movq	112(%rdi), %rax
	movq	%rax, 112(%rdx)
	movq	120(%rdi), %rax
	movq	%rax, 120(%rdx)
	movq	128(%rdi), %rax
	movq	%rax, 128(%rdx)
	movq	152(%rdi), %rax
	movl	144(%rdi), %edx
	movq	%rax, 152(%r12)
	movzbl	168(%rdi), %eax
	movl	%edx, 144(%r12)
	movb	%al, 168(%r12)
	movzbl	184(%rdi), %eax
	leaq	200(%r12), %rdi
	movb	%al, 184(%r12)
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	movq	224(%rbx), %rax
	movq	%rax, 224(%r12)
	movzbl	232(%rbx), %eax
	movb	%al, 232(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movb	$1, 232(%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3620:
	.size	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode, .-_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode
	.section	.text._ZN6icu_676number4impl10MicroPropsD2Ev,"axG",@progbits,_ZN6icu_676number4impl10MicroPropsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl10MicroPropsD2Ev
	.type	_ZN6icu_676number4impl10MicroPropsD2Ev, @function
_ZN6icu_676number4impl10MicroPropsD2Ev:
.LFB5056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	192(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	200(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	subq	$8, %rsp
	movq	%rax, -200(%rdi)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rax, -8(%rdi)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%rbx, 176(%r12)
	leaq	176(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 160(%r12)
	leaq	160(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	136(%r12), %rdi
	movq	%rax, 136(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.cfi_endproc
.LFE5056:
	.size	_ZN6icu_676number4impl10MicroPropsD2Ev, .-_ZN6icu_676number4impl10MicroPropsD2Ev
	.weak	_ZN6icu_676number4impl10MicroPropsD1Ev
	.set	_ZN6icu_676number4impl10MicroPropsD1Ev,_ZN6icu_676number4impl10MicroPropsD2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_120PluralRangesDataSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode, @function
_ZN12_GLOBAL__N_120PluralRangesDataSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode:
.LFB3702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	movq	%r15, %rsi
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$216, %rsp
	movq	%rdi, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-192(%rbp), %rax
	movq	%rax, %rcx
	movq	%rax, -240(%rbp)
	movq	(%rdx), %rax
	movq	%r8, %rdx
	movq	%rcx, %rdi
	call	*80(%rax)
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L34
	movq	8(%r14), %r12
	movl	$0, %eax
	movl	-176(%rbp), %r13d
	movl	8(%r12), %edi
	testl	%edi, %edi
	cmovns	8(%r12), %eax
	cmpl	%eax, %r13d
	jg	.L50
.L37:
	movl	$0, -220(%rbp)
	leaq	-160(%rbp), %r13
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L51:
	movq	(%r15), %rax
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	*80(%rax)
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L34
	movq	%r15, %rdx
	xorl	%esi, %esi
	leaq	-200(%rbp), %r14
	movq	%r13, %rdi
	call	_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE@PLT
	movq	(%r15), %rax
	movq	%rbx, %rdx
	movq	%r15, %rdi
	leaq	-204(%rbp), %rcx
	leaq	-128(%rbp), %r12
	movl	$0, -204(%rbp)
	movq	%rcx, -216(%rbp)
	movq	%rcx, %rsi
	call	*32(%rax)
	movl	-204(%rbp), %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714StandardPlural15indexFromStringERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r12, %rdi
	movl	%eax, -224(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L34
	movq	%r15, %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE@PLT
	movq	(%r15), %rax
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	-216(%rbp), %rsi
	movl	$0, -204(%rbp)
	call	*32(%rax)
	movl	-204(%rbp), %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714StandardPlural15indexFromStringERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r12, %rdi
	movl	%eax, -228(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L34
	movq	%r15, %rdx
	movq	%r13, %rdi
	movl	$2, %esi
	call	_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE@PLT
	movq	(%r15), %rax
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	-216(%rbp), %rsi
	movl	$0, -204(%rbp)
	call	*32(%rax)
	movl	-204(%rbp), %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714StandardPlural15indexFromStringERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L34
	movq	-248(%rbp), %rcx
	movl	-224(%rbp), %edi
	addl	$1, -220(%rbp)
	movq	8(%rcx), %rdx
	movslq	56(%rdx), %rcx
	movq	%rcx, %rax
	leaq	(%rcx,%rcx,2), %rsi
	movq	(%rdx), %rcx
	addl	$1, %eax
	leaq	(%rcx,%rsi,4), %rcx
	movl	%edi, (%rcx)
	movl	-228(%rbp), %edi
	movl	%r14d, 8(%rcx)
	movl	%edi, 4(%rcx)
	movl	%eax, 56(%rdx)
.L41:
	movl	-220(%rbp), %esi
	movq	-240(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE@PLT
	testb	%al, %al
	jne	.L51
	.p2align 4,,10
	.p2align 3
.L34:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movslq	%r13d, %rax
	leaq	(%rax,%rax,2), %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L37
	cmpb	$0, 12(%r12)
	jne	.L53
.L39:
	movq	%r14, (%r12)
	movl	%r13d, 8(%r12)
	movb	$1, 12(%r12)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L39
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3702:
	.size	_ZN12_GLOBAL__N_120PluralRangesDataSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode, .-_ZN12_GLOBAL__N_120PluralRangesDataSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode
	.section	.text._ZN6icu_676number4impl10MicroPropsD0Ev,"axG",@progbits,_ZN6icu_676number4impl10MicroPropsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl10MicroPropsD0Ev
	.type	_ZN6icu_676number4impl10MicroPropsD0Ev, @function
_ZN6icu_676number4impl10MicroPropsD0Ev:
.LFB5058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	192(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	200(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	subq	$8, %rsp
	movq	%rax, -200(%rdi)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rax, -8(%rdi)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%rbx, 176(%r12)
	leaq	176(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 160(%r12)
	leaq	160(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	136(%r12), %rdi
	movq	%rax, 136(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$240, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5058:
	.size	_ZN6icu_676number4impl10MicroPropsD0Ev, .-_ZN6icu_676number4impl10MicroPropsD0Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"range"
.LC1:
	.string	"approximately"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_119NumberRangeDataSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode, @function
_ZN12_GLOBAL__N_119NumberRangeDataSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode:
.LFB3693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-240(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$264, %rsp
	movq	%rsi, -264(%rbp)
	movq	%rbx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%r8, %rdx
	call	*88(%rax)
	leaq	-264(%rbp), %rax
	movl	(%r12), %ecx
	movq	%rax, -272(%rbp)
	leaq	-252(%rbp), %rax
	movq	%rax, -280(%rbp)
	testl	%ecx, %ecx
	jg	.L56
	xorl	%r15d, %r15d
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L87:
	movq	8(%r13), %rax
	movzwl	16(%rax), %edx
	testw	%dx, %dx
	js	.L60
	movswl	%dx, %ecx
	sarl	$5, %ecx
	testb	$17, %dl
	jne	.L73
.L90:
	andl	$2, %edx
	je	.L63
	addq	$18, %rax
.L62:
	testl	%ecx, %ecx
	je	.L64
	cmpw	$0, (%rax)
	jne	.L65
.L64:
	movq	(%rbx), %rax
	movq	-280(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	$0, -252(%rbp)
	call	*32(%rax)
	leaq	-192(%rbp), %r10
	movl	-252(%rbp), %ecx
	leaq	-248(%rbp), %rdx
	movl	$1, %esi
	movq	%r10, %rdi
	movq	%rax, -248(%rbp)
	movq	%r10, -288(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-288(%rbp), %r10
	leaq	-128(%rbp), %r11
	movl	$2, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -112(%rbp)
	movq	%r11, %rdi
	movq	%r12, %r8
	movl	$2147483647, %ecx
	xorl	%edx, %edx
	movq	%r10, %rsi
	movq	%rax, -120(%rbp)
	movq	%r10, -296(%rbp)
	movq	%r11, -288(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	8(%r13), %rdi
.L86:
	movq	-288(%rbp), %r11
	movq	%r11, %rsi
	call	_ZN6icu_6715SimpleFormatteraSERKS0_@PLT
	movq	-288(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	-296(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L65:
	addl	$1, %r15d
.L57:
	movq	-272(%rbp), %rdx
	movq	%rbx, %rcx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L56
	movq	-264(%rbp), %rdx
	movl	$6, %ecx
	leaq	.LC0(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L87
	movl	$14, %ecx
	movq	%rdx, %rsi
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L65
	movq	8(%r13), %rax
	movzwl	88(%rax), %edx
	testw	%dx, %dx
	js	.L66
	movswl	%dx, %ecx
	sarl	$5, %ecx
	testb	$17, %dl
	jne	.L74
.L91:
	andl	$2, %edx
	jne	.L88
	movq	104(%rax), %rax
.L68:
	testl	%ecx, %ecx
	je	.L70
	cmpw	$0, (%rax)
	jne	.L65
.L70:
	movq	(%rbx), %rax
	movq	-280(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	$0, -252(%rbp)
	call	*32(%rax)
	leaq	-192(%rbp), %r10
	movl	-252(%rbp), %ecx
	leaq	-248(%rbp), %rdx
	movl	$1, %esi
	movq	%r10, %rdi
	movq	%rax, -248(%rbp)
	movq	%r10, -288(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-288(%rbp), %r10
	leaq	-128(%rbp), %r11
	movq	%r12, %r8
	xorl	%edx, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r11, %rdi
	movl	$2147483647, %ecx
	movq	%r11, -288(%rbp)
	movq	%rax, -120(%rbp)
	movq	%r10, %rsi
	movl	$2, %eax
	movw	%ax, -112(%rbp)
	movq	%r10, -296(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	8(%r13), %rax
	leaq	72(%rax), %rdi
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L56:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movq	32(%rax), %rax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L60:
	movl	20(%rax), %ecx
	testb	$17, %dl
	je	.L90
.L73:
	xorl	%eax, %eax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$90, %rax
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L66:
	movl	92(%rax), %ecx
	testb	$17, %dl
	je	.L91
.L74:
	xorl	%eax, %eax
	jmp	.L68
.L89:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3693:
	.size	_ZN12_GLOBAL__N_119NumberRangeDataSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode, .-_ZN12_GLOBAL__N_119NumberRangeDataSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB4247:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4247:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB4250:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L105
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L93
	cmpb	$0, 12(%rbx)
	jne	.L106
.L97:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L93:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L97
	.cfi_endproc
.LFE4250:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB4253:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L109
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4253:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB4256:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L112
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE4256:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB4258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L118
.L114:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L119
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L119:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4258:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB4259:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4259:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB4260:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4260:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB4261:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4261:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4262:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4262:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4263:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4263:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB4264:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L135
	testl	%edx, %edx
	jle	.L135
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L138
.L127:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L127
	.cfi_endproc
.LFE4264:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB4265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L142
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L142
	testl	%r12d, %r12d
	jg	.L149
	cmpb	$0, 12(%rbx)
	jne	.L150
.L144:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L144
.L150:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L142:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4265:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB4266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L152
	movq	(%rdi), %r8
.L153:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L156
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L156
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L156:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4266:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB4267:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L163
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4267:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB4268:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4268:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB4269:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4269:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB4270:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4270:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB4272:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4272:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB4274:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4274:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.section	.rodata.str1.1
.LC2:
	.string	"pluralRanges"
.LC3:
	.string	"locales/"
.LC4:
	.string	"rules/"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl20StandardPluralRanges10initializeERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_676number4impl20StandardPluralRanges10initializeERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_676number4impl20StandardPluralRanges10initializeERKNS_6LocaleER10UErrorCode:
.LFB3704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L190
.L169:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L191
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movq	%rdx, %rbx
	movq	%rdi, %r13
	movq	%rsi, %r12
	xorl	%edi, %edi
	leaq	.LC2(%rip), %rsi
	call	ures_openDirect_67@PLT
	movl	(%rbx), %r8d
	movq	%rax, %r15
	testl	%r8d, %r8d
	jle	.L192
.L172:
	testq	%r15, %r15
	je	.L169
	movq	%r15, %rdi
	call	ures_close_67@PLT
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L192:
	leaq	-128(%rbp), %r14
	xorl	%esi, %esi
	leaq	-115(%rbp), %rax
	movq	%rbx, %rcx
	movw	%si, -116(%rbp)
	movl	$-1, %edx
	movq	%r14, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, -128(%rbp)
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%r14, %rdi
	leaq	8(%r12), %rsi
	movq	%rbx, %rcx
	movl	$-1, %edx
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.L193
.L175:
	cmpb	$0, -116(%rbp)
	je	.L172
.L189:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L193:
	movq	-128(%rbp), %rsi
	leaq	-148(%rbp), %rcx
	leaq	-152(%rbp), %rdx
	movq	%r15, %rdi
	movl	$0, -148(%rbp)
	call	ures_getStringByKeyWithFallback_67@PLT
	movl	-148(%rbp), %ecx
	movq	%rax, %r12
	testl	%ecx, %ecx
	jg	.L175
	movq	-128(%rbp), %rax
	movq	%rbx, %rcx
	movl	$-1, %edx
	movq	%r14, %rdi
	movl	$0, -72(%rbp)
	leaq	.LC4(%rip), %rsi
	movb	$0, (%rax)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	-152(%rbp), %edx
	call	_ZN6icu_6710CharString20appendInvariantCharsEPKDsiR10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L175
	leaq	-144(%rbp), %r12
	movq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rbx, %rcx
	leaq	16+_ZTVN12_GLOBAL__N_120PluralRangesDataSinkE(%rip), %r14
	movq	%r12, %rdx
	movq	%r13, -136(%rbp)
	movq	%r14, -144(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	movl	(%rbx), %eax
	movq	%r14, -144(%rbp)
	movq	%r12, %rdi
	testl	%eax, %eax
	jg	.L194
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	cmpb	$0, -116(%rbp)
	je	.L172
	jmp	.L189
.L194:
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	jmp	.L175
.L191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3704:
	.size	_ZN6icu_676number4impl20StandardPluralRanges10initializeERKNS_6LocaleER10UErrorCode, .-_ZN6icu_676number4impl20StandardPluralRanges10initializeERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl20StandardPluralRanges14addPluralRangeENS_14StandardPlural4FormES4_S4_
	.type	_ZN6icu_676number4impl20StandardPluralRanges14addPluralRangeENS_14StandardPlural4FormES4_S4_, @function
_ZN6icu_676number4impl20StandardPluralRanges14addPluralRangeENS_14StandardPlural4FormES4_S4_:
.LFB3705:
	.cfi_startproc
	endbr64
	movl	%edx, %r8d
	movslq	56(%rdi), %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,2), %r9
	movq	(%rdi), %rdx
	addl	$1, %eax
	leaq	(%rdx,%r9,4), %rdx
	movl	%esi, (%rdx)
	movl	%r8d, 4(%rdx)
	movl	%ecx, 8(%rdx)
	movl	%eax, 56(%rdi)
	ret
	.cfi_endproc
.LFE3705:
	.size	_ZN6icu_676number4impl20StandardPluralRanges14addPluralRangeENS_14StandardPlural4FormES4_S4_, .-_ZN6icu_676number4impl20StandardPluralRanges14addPluralRangeENS_14StandardPlural4FormES4_S4_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl20StandardPluralRanges11setCapacityEi
	.type	_ZN6icu_676number4impl20StandardPluralRanges11setCapacityEi, @function
_ZN6icu_676number4impl20StandardPluralRanges11setCapacityEi:
.LFB3706:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovns	8(%rdi), %eax
	cmpl	%eax, %esi
	jg	.L208
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	(%rax,%rax,2), %rdi
	salq	$2, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L196
	cmpb	$0, 12(%rbx)
	jne	.L209
.L200:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L196:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L200
	.cfi_endproc
.LFE3706:
	.size	_ZN6icu_676number4impl20StandardPluralRanges11setCapacityEi, .-_ZN6icu_676number4impl20StandardPluralRanges11setCapacityEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl20StandardPluralRanges7resolveENS_14StandardPlural4FormES4_
	.type	_ZNK6icu_676number4impl20StandardPluralRanges7resolveENS_14StandardPlural4FormES4_, @function
_ZNK6icu_676number4impl20StandardPluralRanges7resolveENS_14StandardPlural4FormES4_:
.LFB3707:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L214
	movq	(%rdi), %rax
	subl	$1, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	12(%rax,%rcx,4), %rcx
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L212:
	addq	$12, %rax
	cmpq	%rcx, %rax
	je	.L214
.L213:
	cmpl	%esi, (%rax)
	jne	.L212
	cmpl	%edx, 4(%rax)
	jne	.L212
	movl	8(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	movl	$5, %eax
	ret
	.cfi_endproc
.LFE3707:
	.size	_ZNK6icu_676number4impl20StandardPluralRanges7resolveENS_14StandardPlural4FormES4_, .-_ZNK6icu_676number4impl20StandardPluralRanges7resolveENS_14StandardPlural4FormES4_
	.section	.rodata.str1.1
.LC5:
	.string	"NumberElements/"
.LC6:
	.string	"/miscPatterns"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"NumberElements/latn/miscPatterns"
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC8:
	.string	"{"
	.string	"0"
	.string	"}"
	.string	"\023 {"
	.string	"1"
	.string	"}"
	.string	""
	.string	""
	.align 2
.LC9:
	.string	"~"
	.string	"{"
	.string	"0"
	.string	"}"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl24NumberRangeFormatterImplC2ERKNS1_15RangeMacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl24NumberRangeFormatterImplC2ERKNS1_15RangeMacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl24NumberRangeFormatterImplC2ERKNS1_15RangeMacroPropsER10UErrorCode:
.LFB3731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	addq	$8, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	109(%r14), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	$8, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$472, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676number4impl19NumberFormatterImplC1ERKNS1_10MacroPropsER10UErrorCode@PLT
	movq	%rbx, %rdx
	leaq	456(%r12), %rsi
	leaq	552(%r14), %rdi
	call	_ZN6icu_676number4impl19NumberFormatterImplC1ERKNS1_10MacroPropsER10UErrorCode@PLT
	xorl	%esi, %esi
	leaq	1120(%r14), %rdi
	movzbl	904(%r12), %eax
	movb	%al, 1096(%r14)
	movq	908(%r12), %rax
	movq	%rax, 1100(%r14)
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	leaq	1184(%r14), %rdi
	call	_ZN6icu_676number4impl14SimpleModifierC1Ev@PLT
	leaq	1304(%r14), %rax
	movb	$0, 1300(%r14)
	movq	%r13, %rdi
	movq	%rax, 1288(%r14)
	leaq	653(%r14), %rsi
	movl	$3, 1296(%r14)
	movl	$0, 1344(%r14)
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L217
	movl	$1, (%rbx)
.L216:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$472, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	leaq	-208(%rbp), %rax
	leaq	-200(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, -488(%rbp)
	leaq	-136(%rbp), %r15
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	leaq	-128(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L304
.L250:
	movq	%r15, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	-488(%rbp), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L304:
	movq	960(%r12), %rsi
	movq	%rbx, %rdx
	xorl	%edi, %edi
	call	ures_open_67@PLT
	movq	%rax, %r15
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L305
.L220:
	testq	%r15, %r15
	je	.L296
	movq	%r15, %rdi
	call	ures_close_67@PLT
.L296:
	movl	(%rbx), %eax
	leaq	-136(%rbp), %r15
	testl	%eax, %eax
	jg	.L250
	movq	-488(%rbp), %rsi
	leaq	1112(%r14), %rdi
	leaq	-320(%rbp), %r13
	leaq	-136(%rbp), %r15
	call	_ZN6icu_6715SimpleFormatteraSERKS0_@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %edx
	call	_ZN6icu_676number4impl14SimpleModifierC1ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEb@PLT
	leaq	-312(%rbp), %r8
	leaq	1192(%r14), %rdi
	movq	%r8, %rsi
	movq	%r8, -496(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movzbl	-248(%rbp), %eax
	movdqu	-232(%rbp), %xmm1
	movq	-496(%rbp), %r8
	movb	%al, 1256(%r14)
	movzbl	-247(%rbp), %eax
	movq	%r8, %rdi
	movups	%xmm1, 1272(%r14)
	movb	%al, 1257(%r14)
	movl	-244(%rbp), %eax
	movl	%eax, 1260(%r14)
	movq	-240(%rbp), %rax
	movq	%rax, 1264(%r14)
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L250
	movq	%rbx, %rdx
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	call	ures_openDirect_67@PLT
	movl	(%rbx), %r9d
	movq	%rax, %r8
	testl	%r9d, %r9d
	jle	.L306
.L252:
	testq	%r8, %r8
	je	.L250
	movq	%r8, %rdi
	call	ures_close_67@PLT
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L305:
	leaq	-435(%rbp), %rax
	leaq	-448(%rbp), %rdi
	movq	%rbx, %rcx
	movq	.LC10(%rip), %xmm0
	movq	%rax, -448(%rbp)
	movl	$-1, %edx
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rsi
	movhps	-488(%rbp), %xmm0
	movq	%rdi, -496(%rbp)
	movaps	%xmm0, -464(%rbp)
	movl	$0, -392(%rbp)
	movl	$40, -440(%rbp)
	movw	%ax, -436(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%rbx, %rcx
	movl	$-1, %edx
	movq	-496(%rbp), %rdi
	leaq	-464(%rbp), %r13
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-496(%rbp), %rdi
	movq	%rbx, %rcx
	movl	$-1, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L307
.L221:
	cmpb	$0, -436(%rbp)
	jne	.L308
.L248:
	leaq	16+_ZTVN12_GLOBAL__N_119NumberRangeDataSinkE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -464(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L306:
	xorl	%esi, %esi
	movq	%rax, -496(%rbp)
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movw	%si, -308(%rbp)
	movl	$-1, %edx
	leaq	-307(%rbp), %rax
	leaq	.LC3(%rip), %rsi
	movq	%rax, -320(%rbp)
	movl	$0, -264(%rbp)
	movl	$40, -312(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rbx, %rcx
	movl	$-1, %edx
	leaq	928(%r12), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	(%rbx), %edi
	movq	-496(%rbp), %r8
	testl	%edi, %edi
	jle	.L309
.L255:
	cmpb	$0, -308(%rbp)
	je	.L252
	movq	%r8, -496(%rbp)
.L301:
	movq	-320(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-496(%rbp), %r8
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L307:
	movq	-448(%rbp), %rsi
	leaq	-468(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movl	$0, -468(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	movl	-468(%rbp), %eax
	cmpl	$2, %eax
	je	.L222
	testl	%eax, %eax
	jg	.L310
.L222:
	movq	-456(%rbp), %rax
	movzwl	16(%rax), %edx
	testw	%dx, %dx
	js	.L223
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L224:
	movl	%edx, %esi
	andw	$17, %si
	jne	.L263
	leaq	18(%rax), %rdi
	testb	$2, %dl
	jne	.L225
	movq	32(%rax), %rdi
.L225:
	testl	%ecx, %ecx
	jne	.L311
.L227:
	movq	%r13, %rdx
	leaq	.LC7(%rip), %rsi
	movq	%rbx, %rcx
	movq	%r15, %rdi
	call	ures_getAllItemsWithFallback_67@PLT
	movq	-456(%rbp), %rax
	movzwl	16(%rax), %edx
	movl	%edx, %esi
	andl	$17, %esi
.L232:
	testw	%dx, %dx
	js	.L233
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L234:
	testw	%si, %si
	jne	.L265
	andl	$2, %edx
	leaq	18(%rax), %rdx
	jne	.L235
	movq	32(%rax), %rdx
.L235:
	testl	%ecx, %ecx
	je	.L237
	cmpw	$0, (%rdx)
	jne	.L238
.L237:
	leaq	-384(%rbp), %r9
	leaq	.LC8(%rip), %rsi
	movq	%r9, %rdi
	movq	%r9, -496(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %r8
	movl	$2147483647, %ecx
	xorl	%edx, %edx
	movq	-496(%rbp), %r9
	leaq	-320(%rbp), %r11
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r11, %rdi
	movq	%rax, -312(%rbp)
	movl	$2, %eax
	movq	%r9, %rsi
	movq	%r9, -504(%rbp)
	movq	%r11, -496(%rbp)
	movw	%ax, -304(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	-496(%rbp), %r11
	movq	-456(%rbp), %rdi
	movq	%r11, %rsi
	call	_ZN6icu_6715SimpleFormatteraSERKS0_@PLT
	movq	-496(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	-504(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-456(%rbp), %rax
.L238:
	movzwl	88(%rax), %edx
	testw	%dx, %dx
	js	.L239
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L240:
	testb	$17, %dl
	jne	.L266
	andl	$2, %edx
	je	.L242
	addq	$90, %rax
.L241:
	testl	%ecx, %ecx
	je	.L243
	cmpw	$0, (%rax)
	jne	.L221
.L243:
	leaq	-384(%rbp), %r9
	leaq	.LC9(%rip), %rsi
	movq	%r9, %rdi
	movq	%r9, -496(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movl	$2, %r11d
	movq	%rbx, %r8
	xorl	%edx, %edx
	movq	-496(%rbp), %r9
	movw	%r11w, -304(%rbp)
	leaq	-320(%rbp), %r11
	movl	$2147483647, %ecx
	movq	%r11, %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r11, -496(%rbp)
	movq	%r9, %rsi
	movq	%r9, -504(%rbp)
	movq	%rax, -312(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	-496(%rbp), %r11
	movq	-456(%rbp), %rax
	movq	%r11, %rsi
	leaq	72(%rax), %rdi
	call	_ZN6icu_6715SimpleFormatteraSERKS0_@PLT
	movq	-496(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	-504(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L308:
	movq	-448(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L309:
	movq	-320(%rbp), %rsi
	leaq	-468(%rbp), %rcx
	movq	%r8, %rdi
	leaq	-472(%rbp), %rdx
	movl	$0, -468(%rbp)
	call	ures_getStringByKeyWithFallback_67@PLT
	movl	-468(%rbp), %ecx
	movq	-496(%rbp), %r8
	movq	%rax, %r12
	testl	%ecx, %ecx
	jg	.L255
	movq	-320(%rbp), %rax
	movq	%rbx, %rcx
	movl	$-1, %edx
	movq	%r13, %rdi
	movl	$0, -264(%rbp)
	leaq	.LC4(%rip), %rsi
	movb	$0, (%rax)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	-472(%rbp), %edx
	call	_ZN6icu_6710CharString20appendInvariantCharsEPKDsiR10UErrorCode@PLT
	movl	(%rbx), %edx
	movq	-496(%rbp), %r8
	testl	%edx, %edx
	jg	.L255
	movq	-320(%rbp), %rsi
	leaq	-464(%rbp), %r12
	movq	%r8, %rdi
	movq	%rbx, %rcx
	leaq	1288(%r14), %r9
	leaq	16+_ZTVN12_GLOBAL__N_120PluralRangesDataSinkE(%rip), %r13
	movq	%r12, %rdx
	movq	%r13, -464(%rbp)
	movq	%r9, -456(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	movl	(%rbx), %eax
	movq	-496(%rbp), %r8
	testl	%eax, %eax
	jg	.L312
	movq	%r12, %rdi
	movq	%r8, -496(%rbp)
	movq	%r13, -464(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	cmpb	$0, -308(%rbp)
	movq	-496(%rbp), %r8
	je	.L252
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L311:
	cmpw	$0, (%rdi)
	je	.L227
	movzwl	88(%rax), %ecx
	testw	%cx, %cx
	js	.L228
	movswl	%cx, %edi
	sarl	$5, %edi
.L229:
	testb	$17, %cl
	jne	.L264
	andl	$2, %ecx
	leaq	90(%rax), %rcx
	jne	.L230
	movq	104(%rax), %rcx
.L230:
	testl	%edi, %edi
	je	.L227
	cmpw	$0, (%rcx)
	jne	.L232
	jmp	.L227
.L310:
	movl	%eax, (%rbx)
	jmp	.L221
.L242:
	movq	104(%rax), %rax
	jmp	.L241
.L239:
	movl	92(%rax), %ecx
	jmp	.L240
.L223:
	movl	20(%rax), %ecx
	jmp	.L224
.L233:
	movl	20(%rax), %ecx
	jmp	.L234
.L265:
	xorl	%edx, %edx
	jmp	.L235
.L263:
	xorl	%edi, %edi
	jmp	.L225
.L266:
	xorl	%eax, %eax
	jmp	.L241
.L228:
	movl	92(%rax), %edi
	jmp	.L229
.L264:
	xorl	%ecx, %ecx
	jmp	.L230
.L312:
	movq	%r12, %rdi
	movq	%r13, -464(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	movq	-496(%rbp), %r8
	jmp	.L255
.L303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3731:
	.size	_ZN6icu_676number4impl24NumberRangeFormatterImplC2ERKNS1_15RangeMacroPropsER10UErrorCode, .-_ZN6icu_676number4impl24NumberRangeFormatterImplC2ERKNS1_15RangeMacroPropsER10UErrorCode
	.globl	_ZN6icu_676number4impl24NumberRangeFormatterImplC1ERKNS1_15RangeMacroPropsER10UErrorCode
	.set	_ZN6icu_676number4impl24NumberRangeFormatterImplC1ERKNS1_15RangeMacroPropsER10UErrorCode,_ZN6icu_676number4impl24NumberRangeFormatterImplC2ERKNS1_15RangeMacroPropsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl24NumberRangeFormatterImpl22resolveModifierPluralsERKNS1_8ModifierES5_
	.type	_ZNK6icu_676number4impl24NumberRangeFormatterImpl22resolveModifierPluralsERKNS1_8ModifierES5_, @function
_ZNK6icu_676number4impl24NumberRangeFormatterImpl22resolveModifierPluralsERKNS1_8ModifierES5_:
.LFB3752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	movq	%r15, %rdi
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676number4impl8Modifier10ParametersC1Ev@PLT
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*56(%rax)
	cmpq	$0, -80(%rbp)
	je	.L316
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movl	-68(%rbp), %ebx
	movq	%r15, %rsi
	call	*56(%rax)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L316
	movl	1344(%r14), %edx
	movl	-68(%rbp), %ecx
	testl	%edx, %edx
	jle	.L317
	movq	1288(%r14), %rax
	subl	$1, %edx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	12(%rax,%rdx,4), %rdx
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L318:
	addq	$12, %rax
	cmpq	%rax, %rdx
	je	.L317
.L321:
	cmpl	(%rax), %ebx
	jne	.L318
	cmpl	4(%rax), %ecx
	jne	.L318
	movl	8(%rax), %edx
	movq	(%rdi), %rax
	leaq	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE(%rip), %rcx
	movl	-72(%rbp), %esi
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L320
	leal	(%rsi,%rdx,4), %eax
	cltq
	movq	8(%rdi,%rax,8), %rax
	testq	%rax, %rax
	jne	.L313
	cmpl	$5, %edx
	je	.L313
.L322:
	addl	$20, %esi
	movslq	%esi, %rsi
	movq	8(%rdi,%rsi,8), %rax
.L313:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L334
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE(%rip), %rdx
	movl	-72(%rbp), %esi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L322
	movl	$5, %edx
.L320:
	call	*%rax
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L316:
	movq	%r12, %rax
	jmp	.L313
.L334:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3752:
	.size	_ZNK6icu_676number4impl24NumberRangeFormatterImpl22resolveModifierPluralsERKNS1_8ModifierES5_, .-_ZNK6icu_676number4impl24NumberRangeFormatterImpl22resolveModifierPluralsERKNS1_8ModifierES5_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl24NumberRangeFormatterImpl11formatRangeERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode
	.type	_ZNK6icu_676number4impl24NumberRangeFormatterImpl11formatRangeERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode, @function
_ZNK6icu_676number4impl24NumberRangeFormatterImpl11formatRangeERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode:
.LFB3751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -88(%rbp)
	movl	(%r8), %r10d
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L335
	movl	1100(%rdi), %eax
	movq	%rsi, %r12
	movq	%rdx, %r14
	movq	%r8, %rbx
	testl	%eax, %eax
	je	.L337
	subl	$2, %eax
	cmpl	$1, %eax
	jbe	.L337
	movb	$0, -78(%rbp)
	movb	$0, -77(%rbp)
.L338:
	subq	$8, %rsp
	movq	-88(%rbp), %rax
	leaq	8(%r12), %r15
	xorl	%edx, %edx
	pushq	%rbx
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %r13d
	leaq	-64(%rbp), %rcx
	movq	%r15, %rsi
	leaq	1112(%rax), %rdi
	leaq	-60(%rbp), %r8
	movl	$0, -64(%rbp)
	movl	%r13d, %r9d
	movl	$0, -60(%rbp)
	call	_ZN6icu_676number4impl14SimpleModifier19formatTwoArgPatternERKNS_15SimpleFormatterERNS_22FormattedStringBuilderEiPiS8_NS6_5FieldER10UErrorCode@PLT
	movl	(%rbx), %esi
	popq	%rdx
	popq	%rcx
	testl	%esi, %esi
	jle	.L386
.L335:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movq	112(%r14), %rdi
	movq	-72(%rbp), %r15
	movq	(%rdi), %rax
	movq	112(%r15), %rsi
	call	*64(%rax)
	movb	%al, -77(%rbp)
	testb	%al, %al
	je	.L368
	movq	120(%r14), %rdi
	movq	120(%r15), %rsi
	movq	(%rdi), %rax
	call	*64(%rax)
	movb	%al, -78(%rbp)
	testb	%al, %al
	je	.L368
	movq	-88(%rbp), %rax
	movq	120(%r14), %r13
	movl	1100(%rax), %eax
	cmpl	$2, %eax
	je	.L388
	testl	%eax, %eax
	jne	.L342
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*32(%rax)
	cmpl	$1, %eax
	jle	.L368
.L385:
	movq	-88(%rbp), %rax
	movl	1100(%rax), %eax
.L342:
	movzbl	-78(%rbp), %esi
	movb	%sil, -77(%rbp)
	cmpl	$3, %eax
	jne	.L338
	movq	128(%r14), %rdi
	movq	-72(%rbp), %rax
	leaq	8(%r12), %r15
	movq	128(%rax), %rsi
	movq	(%rdi), %rax
	call	*64(%rax)
	subq	$8, %rsp
	movq	-88(%rbp), %rsi
	leaq	-60(%rbp), %r8
	pushq	%rbx
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %r13d
	xorl	%edx, %edx
	leaq	-64(%rbp), %rcx
	leaq	1112(%rsi), %rdi
	movq	%r15, %rsi
	movb	%al, -77(%rbp)
	movl	%r13d, %r9d
	movl	$0, -64(%rbp)
	movl	$0, -60(%rbp)
	call	_ZN6icu_676number4impl14SimpleModifier19formatTwoArgPatternERKNS_15SimpleFormatterERNS_22FormattedStringBuilderEiPiS8_NS6_5FieldER10UErrorCode@PLT
	movl	(%rbx), %r9d
	popq	%rdi
	movl	%eax, %edx
	popq	%r8
	testl	%r9d, %r9d
	jg	.L335
	subl	-64(%rbp), %edx
	subl	-60(%rbp), %edx
	cmpb	$0, -77(%rbp)
	movl	%edx, -76(%rbp)
	je	.L389
	movzbl	-77(%rbp), %eax
	movb	%al, -79(%rbp)
	movb	%al, -78(%rbp)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L368:
	movb	$0, -78(%rbp)
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L386:
	movq	128(%r14), %rdi
	subl	-64(%rbp), %eax
	subl	-60(%rbp), %eax
	movl	%eax, -76(%rbp)
	movq	(%rdi), %rax
	call	*32(%rax)
	testl	%eax, %eax
	setg	-80(%rbp)
	cmpb	$0, -78(%rbp)
	jne	.L363
	movq	120(%r14), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	testl	%eax, %eax
	setg	-89(%rbp)
.L346:
	cmpb	$0, -77(%rbp)
	jne	.L390
	movq	112(%r14), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movb	$0, -79(%rbp)
	testl	%eax, %eax
	jle	.L391
.L352:
	cmpb	$0, 8(%r12)
	movl	-64(%rbp), %eax
	je	.L392
	movq	16(%r12), %rdx
.L353:
	addl	136(%r12), %eax
	cltq
	movzwl	(%rdx,%rax,2), %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	je	.L393
.L354:
	movl	-76(%rbp), %eax
	addl	-64(%rbp), %eax
	leaq	16(%r12), %rdx
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	je	.L356
	movq	16(%r12), %rdx
.L356:
	addl	136(%r12), %eax
	cltq
	movzwl	(%rdx,%rax,2), %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	je	.L394
.L348:
	movl	-64(%rbp), %ecx
	movq	%rbx, %r8
	movq	%r15, %rdx
	movq	%r14, %rdi
	leaq	152(%r12), %rsi
	call	_ZN6icu_676number4impl19NumberFormatterImpl11writeNumberERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode@PLT
	movl	-64(%rbp), %ecx
	movq	-72(%rbp), %rdi
	movq	%rbx, %r8
	leaq	224(%r12), %rsi
	movq	%r15, %rdx
	movl	%eax, %r13d
	addl	%eax, %ecx
	addl	-76(%rbp), %ecx
	call	_ZN6icu_676number4impl19NumberFormatterImpl11writeNumberERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode@PLT
	cmpb	$0, -79(%rbp)
	movl	%eax, %r12d
	jne	.L395
	movq	128(%r14), %rdi
	movl	-64(%rbp), %edx
	movq	%rbx, %r8
	movq	%r15, %rsi
	movq	(%rdi), %rax
	leal	(%rdx,%r13), %ecx
	call	*16(%rax)
	movl	-64(%rbp), %edx
	movq	%rbx, %r8
	movq	%r15, %rsi
	addl	%eax, %r13d
	movq	-72(%rbp), %rax
	addl	%r13d, %edx
	addl	-76(%rbp), %edx
	movq	128(%rax), %rdi
	leal	(%rdx,%r12), %ecx
	movq	(%rdi), %rax
	call	*16(%rax)
	addl	%eax, %r12d
.L358:
	cmpb	$0, -78(%rbp)
	movq	120(%r14), %rdi
	jne	.L396
	movl	-64(%rbp), %edx
	movq	(%rdi), %rax
	movq	%rbx, %r8
	movq	%r15, %rsi
	leal	(%rdx,%r13), %ecx
	call	*16(%rax)
	movl	-64(%rbp), %edx
	movq	%rbx, %r8
	movq	%r15, %rsi
	addl	%eax, %r13d
	movq	-72(%rbp), %rax
	addl	%r13d, %edx
	addl	-76(%rbp), %edx
	movq	120(%rax), %rdi
	leal	(%rdx,%r12), %ecx
	movq	(%rdi), %rax
	call	*16(%rax)
	addl	%eax, %r12d
.L360:
	cmpb	$0, -77(%rbp)
	movq	112(%r14), %rdi
	jne	.L397
	movl	-64(%rbp), %edx
	movq	(%rdi), %rax
	movq	%rbx, %r8
	movq	%r15, %rsi
	leal	(%rdx,%r13), %ecx
	call	*16(%rax)
	movq	-72(%rbp), %rsi
	movq	%rbx, %r8
	leal	0(%r13,%rax), %edx
	addl	-64(%rbp), %edx
	addl	-76(%rbp), %edx
	movq	112(%rsi), %rdi
	leal	(%rdx,%r12), %ecx
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L392:
	leaq	16(%r12), %rdx
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L397:
	movq	-72(%rbp), %rax
	movq	%rdi, %rsi
	movq	-88(%rbp), %rdi
	movq	112(%rax), %rdx
	call	_ZNK6icu_676number4impl24NumberRangeFormatterImpl22resolveModifierPluralsERKNS1_8ModifierES5_
	movl	-64(%rbp), %edx
	movq	%rbx, %r8
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	leal	(%rdx,%r13), %ecx
	addl	-76(%rbp), %ecx
	addl	%r12d, %ecx
	call	*16(%rax)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L396:
	movq	-72(%rbp), %rax
	movq	%rdi, %rsi
	movq	-88(%rbp), %rdi
	movq	120(%rax), %rdx
	call	_ZNK6icu_676number4impl24NumberRangeFormatterImpl22resolveModifierPluralsERKNS1_8ModifierES5_
	movl	-64(%rbp), %edx
	movq	%rbx, %r8
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	leal	(%rdx,%r13), %ecx
	addl	-76(%rbp), %ecx
	addl	%r12d, %ecx
	call	*16(%rax)
	addl	%eax, -76(%rbp)
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L395:
	movq	-72(%rbp), %rax
	movq	128(%r14), %rsi
	movq	-88(%rbp), %rdi
	movq	128(%rax), %rdx
	call	_ZNK6icu_676number4impl24NumberRangeFormatterImpl22resolveModifierPluralsERKNS1_8ModifierES5_
	movl	-64(%rbp), %edx
	movq	%rbx, %r8
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	leal	(%rdx,%r13), %ecx
	addl	-76(%rbp), %ecx
	addl	%r12d, %ecx
	call	*16(%rax)
	addl	%eax, -76(%rbp)
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L394:
	movl	-76(%rbp), %esi
	movq	%rbx, %r8
	addl	-64(%rbp), %esi
	movl	%r13d, %ecx
	movl	$32, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode@PLT
	addl	%eax, -76(%rbp)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L393:
	movl	-64(%rbp), %esi
	movq	%rbx, %r8
	movl	%r13d, %ecx
	movl	$32, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode@PLT
	addl	%eax, -76(%rbp)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L391:
	movzbl	-89(%rbp), %eax
	orb	-80(%rbp), %al
	movb	%al, -79(%rbp)
	je	.L348
	movb	$0, -79(%rbp)
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L388:
	movq	0(%r13), %rax
	movl	$39, %esi
	movq	%r13, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L385
	movq	0(%r13), %rax
	movl	$40, %esi
	movq	%r13, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L385
	movzbl	-78(%rbp), %eax
	movb	$0, -78(%rbp)
	movb	%al, -77(%rbp)
	jmp	.L338
.L389:
	movq	128(%r14), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	testl	%eax, %eax
	movzbl	-78(%rbp), %eax
	setg	-80(%rbp)
	movb	%al, -77(%rbp)
	.p2align 4,,10
	.p2align 3
.L363:
	movb	$1, -78(%rbp)
	movb	$0, -89(%rbp)
	jmp	.L346
.L387:
	call	__stack_chk_fail@PLT
.L390:
	cmpb	$0, -89(%rbp)
	movb	$0, -79(%rbp)
	jne	.L352
	cmpb	$0, -80(%rbp)
	je	.L348
	jmp	.L352
	.cfi_endproc
.LFE3751:
	.size	_ZNK6icu_676number4impl24NumberRangeFormatterImpl11formatRangeERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode, .-_ZNK6icu_676number4impl24NumberRangeFormatterImpl11formatRangeERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl24NumberRangeFormatterImpl17formatSingleValueERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode
	.type	_ZNK6icu_676number4impl24NumberRangeFormatterImpl17formatSingleValueERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode, @function
_ZNK6icu_676number4impl24NumberRangeFormatterImpl17formatSingleValueERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode:
.LFB3749:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L398
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subq	$16, %rsp
	cmpb	$0, 1096(%rdi)
	je	.L400
	leaq	8(%rsi), %r13
	movq	%r12, %rdi
	addq	$152, %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r8, -24(%rbp)
	call	_ZN6icu_676number4impl19NumberFormatterImpl11writeNumberERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode@PLT
	movq	-24(%rbp), %r8
	addq	$16, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	xorl	%edx, %edx
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19NumberFormatterImpl12writeAffixesERKNS1_10MicroPropsERNS_22FormattedStringBuilderEiiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676number4impl24NumberRangeFormatterImpl11formatRangeERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode
	.p2align 4,,10
	.p2align 3
.L398:
	ret
	.cfi_endproc
.LFE3749:
	.size	_ZNK6icu_676number4impl24NumberRangeFormatterImpl17formatSingleValueERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode, .-_ZNK6icu_676number4impl24NumberRangeFormatterImpl17formatSingleValueERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl24NumberRangeFormatterImpl19formatApproximatelyERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode
	.type	_ZNK6icu_676number4impl24NumberRangeFormatterImpl19formatApproximatelyERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode, @function
_ZNK6icu_676number4impl24NumberRangeFormatterImpl19formatApproximatelyERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode:
.LFB3750:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L404
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	cmpb	$0, 1096(%rdi)
	je	.L406
	leaq	8(%rsi), %r15
	movq	%r13, %rdi
	addq	$152, %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	call	_ZN6icu_676number4impl19NumberFormatterImpl11writeNumberERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode@PLT
	movq	%rbx, %r8
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	128(%r13), %rdi
	movl	%eax, %r14d
	movl	%r14d, %ecx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	120(%r13), %rdi
	movq	%rbx, %r8
	movq	%r15, %rsi
	addl	%eax, %r14d
	xorl	%edx, %edx
	movq	(%rdi), %rax
	movl	%r14d, %ecx
	call	*16(%rax)
	leaq	1184(%r12), %rdi
	movq	%rbx, %r8
	xorl	%edx, %edx
	addl	%eax, %r14d
	movq	%r15, %rsi
	movl	%r14d, %ecx
	call	_ZNK6icu_676number4impl14SimpleModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode@PLT
	movq	112(%r13), %rdi
	movq	%rbx, %r8
	movq	%r15, %rsi
	leal	(%r14,%rax), %ecx
	xorl	%edx, %edx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676number4impl24NumberRangeFormatterImpl11formatRangeERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode
	.p2align 4,,10
	.p2align 3
.L404:
	ret
	.cfi_endproc
.LFE3750:
	.size	_ZNK6icu_676number4impl24NumberRangeFormatterImpl19formatApproximatelyERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode, .-_ZNK6icu_676number4impl24NumberRangeFormatterImpl19formatApproximatelyERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB11:
	.text
.LHOTB11:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl24NumberRangeFormatterImpl6formatERNS1_25UFormattedNumberRangeDataEbR10UErrorCode
	.type	_ZNK6icu_676number4impl24NumberRangeFormatterImpl6formatERNS1_25UFormattedNumberRangeDataEbR10UErrorCode, @function
_ZNK6icu_676number4impl24NumberRangeFormatterImpl6formatERNS1_25UFormattedNumberRangeDataEbR10UErrorCode:
.LFB3733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -556(%rbp)
	movl	(%rcx), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L436
.L410:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L437
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	leaq	-408(%rbp), %rbx
	movq	%rdi, %r12
	movq	%rsi, %r13
	movq	%rcx, %r15
	movl	$-1, %esi
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	movq	%rbx, %rdi
	movl	$-3, %ecx
	movw	%si, -472(%rbp)
	leaq	-544(%rbp), %r14
	movq	%rax, -544(%rbp)
	movw	%cx, -496(%rbp)
	movq	%rbx, -568(%rbp)
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	movl	$0, -536(%rbp)
	movb	$1, -500(%rbp)
	movl	$-2, -484(%rbp)
	movb	$0, -464(%rbp)
	movq	$0, -424(%rbp)
	call	_ZN6icu_676number4impl18ScientificModifierC1Ev@PLT
	movl	$-3, %edi
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	-304(%rbp), %rax
	movl	$-1, %r8d
	movq	%rax, -552(%rbp)
	leaq	-168(%rbp), %rax
	movw	%di, -256(%rbp)
	movq	%rax, %rdi
	movq	%rcx, -352(%rbp)
	movw	%r8w, -232(%rbp)
	movq	%rax, -576(%rbp)
	movq	%rbx, -384(%rbp)
	movb	$0, -376(%rbp)
	movq	%rbx, -368(%rbp)
	movb	$1, -360(%rbp)
	movl	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movl	$0, -328(%rbp)
	movb	$0, -312(%rbp)
	movl	$0, -296(%rbp)
	movb	$1, -260(%rbp)
	movl	$-2, -244(%rbp)
	movb	$0, -224(%rbp)
	movq	$0, -184(%rbp)
	call	_ZN6icu_676number4impl18ScientificModifierC1Ev@PLT
	leaq	152(%r13), %rax
	leaq	8(%r12), %rdi
	movq	%r14, %rdx
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rcx
	movq	%rax, %rsi
	movq	%rbx, -128(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r15, %rcx
	movq	%rbx, -144(%rbp)
	movb	$0, -136(%rbp)
	movb	$1, -120(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movl	$0, -88(%rbp)
	movb	$0, -72(%rbp)
	movq	%rax, -600(%rbp)
	movq	%rdi, -592(%rbp)
	call	_ZNK6icu_676number4impl19NumberFormatterImpl10preProcessERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode@PLT
	leaq	224(%r13), %rsi
	cmpb	$0, 1096(%r12)
	movq	%rsi, -584(%rbp)
	jne	.L438
	movq	-552(%rbp), %rdx
	leaq	552(%r12), %rdi
	movq	%r15, %rcx
	call	_ZNK6icu_676number4impl19NumberFormatterImpl10preProcessERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L414
.L439:
	movq	-416(%rbp), %rdi
	movq	-176(%rbp), %rsi
	movq	(%rdi), %rax
	call	*64(%rax)
	testb	%al, %al
	je	.L416
	movq	-424(%rbp), %rdi
	movq	-184(%rbp), %rsi
	movq	(%rdi), %rax
	call	*64(%rax)
	testb	%al, %al
	je	.L416
	movq	-432(%rbp), %rdi
	movq	-192(%rbp), %rsi
	movq	(%rdi), %rax
	call	*64(%rax)
	testb	%al, %al
	je	.L416
	cmpb	$0, -556(%rbp)
	je	.L417
	movl	$0, 296(%r13)
	xorl	%eax, %eax
.L418:
	orb	1104(%r12), %al
	cmpb	$35, %al
	ja	.L420
	leaq	.L422(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L422:
	.long	.L424-.L422
	.long	.L424-.L422
	.long	.L423-.L422
	.long	.L426-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L424-.L422
	.long	.L423-.L422
	.long	.L423-.L422
	.long	.L426-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L420-.L422
	.long	.L426-.L422
	.long	.L426-.L422
	.long	.L426-.L422
	.long	.L426-.L422
	.text
	.p2align 4,,10
	.p2align 3
.L438:
	movq	-552(%rbp), %rdx
	movq	-592(%rbp), %rdi
	movq	%r15, %rcx
	call	_ZNK6icu_676number4impl19NumberFormatterImpl10preProcessERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	jle	.L439
.L414:
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %r15
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %r13
	leaq	-104(%rbp), %rdi
	movq	%r15, -304(%rbp)
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %r12
	movq	%r13, -112(%rbp)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-112(%rbp), %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	leaq	-128(%rbp), %rdi
	movq	%rbx, -128(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	-144(%rbp), %rdi
	movq	%rbx, -144(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	-576(%rbp), %rdi
	movq	%r12, -168(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	-552(%rbp), %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	leaq	-344(%rbp), %rdi
	movq	%r15, -544(%rbp)
	movq	%r13, -352(%rbp)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-352(%rbp), %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	leaq	-368(%rbp), %rdi
	movq	%rbx, -368(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	-384(%rbp), %rdi
	movq	%rbx, -384(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	-568(%rbp), %rdi
	movq	%r12, -408(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L416:
	movq	-552(%rbp), %rcx
	movq	%r15, %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl24NumberRangeFormatterImpl11formatRangeERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode
	movl	$2, 296(%r13)
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L426:
	movq	-552(%rbp), %rcx
	movq	%r15, %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl24NumberRangeFormatterImpl11formatRangeERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode
	jmp	.L414
.L423:
	movq	-552(%rbp), %rcx
	movq	%r15, %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl24NumberRangeFormatterImpl19formatApproximatelyERNS1_25UFormattedNumberRangeDataERNS1_10MicroPropsES6_R10UErrorCode
	jmp	.L414
.L424:
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L414
	cmpb	$0, 1096(%r12)
	je	.L426
	movq	-600(%rbp), %rsi
	addq	$8, %r13
	movq	%r15, %r8
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19NumberFormatterImpl11writeNumberERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode@PLT
	movq	%r15, %r8
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	%eax, %ecx
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19NumberFormatterImpl12writeAffixesERKNS1_10MicroPropsERNS_22FormattedStringBuilderEiiR10UErrorCode@PLT
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L417:
	movq	-584(%rbp), %rsi
	movq	-600(%rbp), %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantityeqERKS2_@PLT
	testb	%al, %al
	je	.L419
	movl	$1, 296(%r13)
	movl	$16, %eax
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L419:
	movl	$2, 296(%r13)
	movl	$32, %eax
	jmp	.L418
.L437:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_676number4impl24NumberRangeFormatterImpl6formatERNS1_25UFormattedNumberRangeDataEbR10UErrorCode.cold, @function
_ZNK6icu_676number4impl24NumberRangeFormatterImpl6formatERNS1_25UFormattedNumberRangeDataEbR10UErrorCode.cold:
.LFSB3733:
.L420:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3733:
	.text
	.size	_ZNK6icu_676number4impl24NumberRangeFormatterImpl6formatERNS1_25UFormattedNumberRangeDataEbR10UErrorCode, .-_ZNK6icu_676number4impl24NumberRangeFormatterImpl6formatERNS1_25UFormattedNumberRangeDataEbR10UErrorCode
	.section	.text.unlikely
	.size	_ZNK6icu_676number4impl24NumberRangeFormatterImpl6formatERNS1_25UFormattedNumberRangeDataEbR10UErrorCode.cold, .-_ZNK6icu_676number4impl24NumberRangeFormatterImpl6formatERNS1_25UFormattedNumberRangeDataEbR10UErrorCode.cold
.LCOLDE11:
	.text
.LHOTE11:
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_676number4impl13EmptyModifierE
	.section	.rodata._ZTSN6icu_676number4impl13EmptyModifierE,"aG",@progbits,_ZTSN6icu_676number4impl13EmptyModifierE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl13EmptyModifierE, @object
	.size	_ZTSN6icu_676number4impl13EmptyModifierE, 37
_ZTSN6icu_676number4impl13EmptyModifierE:
	.string	"N6icu_676number4impl13EmptyModifierE"
	.weak	_ZTIN6icu_676number4impl13EmptyModifierE
	.section	.data.rel.ro._ZTIN6icu_676number4impl13EmptyModifierE,"awG",@progbits,_ZTIN6icu_676number4impl13EmptyModifierE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl13EmptyModifierE, @object
	.size	_ZTIN6icu_676number4impl13EmptyModifierE, 56
_ZTIN6icu_676number4impl13EmptyModifierE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl13EmptyModifierE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl8ModifierE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTSN6icu_676number4impl10MicroPropsE
	.section	.rodata._ZTSN6icu_676number4impl10MicroPropsE,"aG",@progbits,_ZTSN6icu_676number4impl10MicroPropsE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl10MicroPropsE, @object
	.size	_ZTSN6icu_676number4impl10MicroPropsE, 34
_ZTSN6icu_676number4impl10MicroPropsE:
	.string	"N6icu_676number4impl10MicroPropsE"
	.weak	_ZTIN6icu_676number4impl10MicroPropsE
	.section	.data.rel.ro._ZTIN6icu_676number4impl10MicroPropsE,"awG",@progbits,_ZTIN6icu_676number4impl10MicroPropsE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl10MicroPropsE, @object
	.size	_ZTIN6icu_676number4impl10MicroPropsE, 24
_ZTIN6icu_676number4impl10MicroPropsE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl10MicroPropsE
	.quad	_ZTIN6icu_676number4impl19MicroPropsGeneratorE
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTIN12_GLOBAL__N_119NumberRangeDataSinkE, @object
	.size	_ZTIN12_GLOBAL__N_119NumberRangeDataSinkE, 24
_ZTIN12_GLOBAL__N_119NumberRangeDataSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN12_GLOBAL__N_119NumberRangeDataSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.section	.rodata
	.align 32
	.type	_ZTSN12_GLOBAL__N_119NumberRangeDataSinkE, @object
	.size	_ZTSN12_GLOBAL__N_119NumberRangeDataSinkE, 39
_ZTSN12_GLOBAL__N_119NumberRangeDataSinkE:
	.string	"*N12_GLOBAL__N_119NumberRangeDataSinkE"
	.section	.data.rel.ro
	.align 8
	.type	_ZTIN12_GLOBAL__N_120PluralRangesDataSinkE, @object
	.size	_ZTIN12_GLOBAL__N_120PluralRangesDataSinkE, 24
_ZTIN12_GLOBAL__N_120PluralRangesDataSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN12_GLOBAL__N_120PluralRangesDataSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.section	.rodata
	.align 32
	.type	_ZTSN12_GLOBAL__N_120PluralRangesDataSinkE, @object
	.size	_ZTSN12_GLOBAL__N_120PluralRangesDataSinkE, 40
_ZTSN12_GLOBAL__N_120PluralRangesDataSinkE:
	.string	"*N12_GLOBAL__N_120PluralRangesDataSinkE"
	.weak	_ZTVN6icu_676number4impl13EmptyModifierE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl13EmptyModifierE,"awG",@progbits,_ZTVN6icu_676number4impl13EmptyModifierE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl13EmptyModifierE, @object
	.size	_ZTVN6icu_676number4impl13EmptyModifierE, 88
_ZTVN6icu_676number4impl13EmptyModifierE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl13EmptyModifierE
	.quad	_ZN6icu_676number4impl13EmptyModifierD1Ev
	.quad	_ZN6icu_676number4impl13EmptyModifierD0Ev
	.quad	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.quad	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv
	.quad	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv
	.quad	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv
	.quad	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.quad	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE
	.quad	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE
	.weak	_ZTVN6icu_676number4impl10MicroPropsE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl10MicroPropsE,"awG",@progbits,_ZTVN6icu_676number4impl10MicroPropsE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl10MicroPropsE, @object
	.size	_ZTVN6icu_676number4impl10MicroPropsE, 40
_ZTVN6icu_676number4impl10MicroPropsE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl10MicroPropsE
	.quad	_ZN6icu_676number4impl10MicroPropsD1Ev
	.quad	_ZN6icu_676number4impl10MicroPropsD0Ev
	.quad	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode
	.section	.data.rel.ro
	.align 8
	.type	_ZTVN12_GLOBAL__N_119NumberRangeDataSinkE, @object
	.size	_ZTVN12_GLOBAL__N_119NumberRangeDataSinkE, 48
_ZTVN12_GLOBAL__N_119NumberRangeDataSinkE:
	.quad	0
	.quad	_ZTIN12_GLOBAL__N_119NumberRangeDataSinkE
	.quad	_ZN12_GLOBAL__N_119NumberRangeDataSinkD1Ev
	.quad	_ZN12_GLOBAL__N_119NumberRangeDataSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN12_GLOBAL__N_119NumberRangeDataSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode
	.align 8
	.type	_ZTVN12_GLOBAL__N_120PluralRangesDataSinkE, @object
	.size	_ZTVN12_GLOBAL__N_120PluralRangesDataSinkE, 48
_ZTVN12_GLOBAL__N_120PluralRangesDataSinkE:
	.quad	0
	.quad	_ZTIN12_GLOBAL__N_120PluralRangesDataSinkE
	.quad	_ZN12_GLOBAL__N_120PluralRangesDataSinkD1Ev
	.quad	_ZN12_GLOBAL__N_120PluralRangesDataSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN12_GLOBAL__N_120PluralRangesDataSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode
	.section	.rodata
	.type	_ZN6icu_67L15kUndefinedFieldE, @object
	.size	_ZN6icu_67L15kUndefinedFieldE, 1
_ZN6icu_67L15kUndefinedFieldE:
	.zero	1
	.section	.data.rel.ro.local,"aw"
	.align 8
.LC10:
	.quad	_ZTVN12_GLOBAL__N_119NumberRangeDataSinkE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
