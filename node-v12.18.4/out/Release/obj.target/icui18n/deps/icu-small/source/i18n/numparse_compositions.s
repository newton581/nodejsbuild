	.file	"numparse_compositions.cpp"
	.text
	.section	.text._ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv,"axG",@progbits,_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.type	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv, @function
_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv:
.LFB2799:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2799:
	.size	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv, .-_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.section	.text._ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE,"axG",@progbits,_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.type	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE, @function
_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE:
.LFB2800:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2800:
	.size	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE, .-_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl18ArraySeriesMatcher6lengthEv
	.type	_ZNK6icu_678numparse4impl18ArraySeriesMatcher6lengthEv, @function
_ZNK6icu_678numparse4impl18ArraySeriesMatcher6lengthEv:
.LFB2835:
	.cfi_startproc
	endbr64
	movl	48(%rdi), %eax
	ret
	.cfi_endproc
.LFE2835:
	.size	_ZNK6icu_678numparse4impl18ArraySeriesMatcher6lengthEv, .-_ZNK6icu_678numparse4impl18ArraySeriesMatcher6lengthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl18ArraySeriesMatcher5beginEv
	.type	_ZNK6icu_678numparse4impl18ArraySeriesMatcher5beginEv, @function
_ZNK6icu_678numparse4impl18ArraySeriesMatcher5beginEv:
.LFB2836:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE2836:
	.size	_ZNK6icu_678numparse4impl18ArraySeriesMatcher5beginEv, .-_ZNK6icu_678numparse4impl18ArraySeriesMatcher5beginEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl18ArraySeriesMatcher3endEv
	.type	_ZNK6icu_678numparse4impl18ArraySeriesMatcher3endEv, @function
_ZNK6icu_678numparse4impl18ArraySeriesMatcher3endEv:
.LFB2837:
	.cfi_startproc
	endbr64
	movslq	48(%rdi), %rdx
	movq	8(%rdi), %rax
	leaq	(%rax,%rdx,8), %rax
	ret
	.cfi_endproc
.LFE2837:
	.size	_ZNK6icu_678numparse4impl18ArraySeriesMatcher3endEv, .-_ZNK6icu_678numparse4impl18ArraySeriesMatcher3endEv
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"<"
	.string	"A"
	.string	"r"
	.string	"r"
	.string	"a"
	.string	"y"
	.string	"S"
	.string	"e"
	.string	"r"
	.string	"i"
	.string	"e"
	.string	"s"
	.string	">"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl18ArraySeriesMatcher8toStringEv
	.type	_ZNK6icu_678numparse4impl18ArraySeriesMatcher8toStringEv, @function
_ZNK6icu_678numparse4impl18ArraySeriesMatcher8toStringEv:
.LFB2838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2838:
	.size	_ZNK6icu_678numparse4impl18ArraySeriesMatcher8toStringEv, .-_ZNK6icu_678numparse4impl18ArraySeriesMatcher8toStringEv
	.section	.text._ZN6icu_678numparse4impl18ArraySeriesMatcherD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl18ArraySeriesMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl18ArraySeriesMatcherD2Ev
	.type	_ZN6icu_678numparse4impl18ArraySeriesMatcherD2Ev, @function
_ZN6icu_678numparse4impl18ArraySeriesMatcherD2Ev:
.LFB3935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 20(%rdi)
	movq	%rax, (%rdi)
	jne	.L12
.L10:
	leaq	16+_ZTVN6icu_678numparse4impl18CompositionMatcherE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L10
	.cfi_endproc
.LFE3935:
	.size	_ZN6icu_678numparse4impl18ArraySeriesMatcherD2Ev, .-_ZN6icu_678numparse4impl18ArraySeriesMatcherD2Ev
	.weak	_ZN6icu_678numparse4impl18ArraySeriesMatcherD1Ev
	.set	_ZN6icu_678numparse4impl18ArraySeriesMatcherD1Ev,_ZN6icu_678numparse4impl18ArraySeriesMatcherD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl13SeriesMatcher9smokeTestERKNS_13StringSegmentE
	.type	_ZNK6icu_678numparse4impl13SeriesMatcher9smokeTestERKNS_13StringSegmentE, @function
_ZNK6icu_678numparse4impl13SeriesMatcher9smokeTestERKNS_13StringSegmentE:
.LFB2809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_678numparse4impl18ArraySeriesMatcher5beginEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	56(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L14
	movq	8(%rdi), %rbx
.L15:
	movq	64(%rax), %rax
	leaq	_ZNK6icu_678numparse4impl18ArraySeriesMatcher3endEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L16
	movslq	48(%rdi), %rdx
	movq	8(%rdi), %rax
	leaq	(%rax,%rdx,8), %rax
.L17:
	cmpq	%rax, %rbx
	je	.L18
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	call	*%rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rdi, -24(%rbp)
	call	*%rdx
	movq	-24(%rbp), %rdi
	movq	%rax, %rbx
	movq	(%rdi), %rax
	jmp	.L15
	.cfi_endproc
.LFE2809:
	.size	_ZNK6icu_678numparse4impl13SeriesMatcher9smokeTestERKNS_13StringSegmentE, .-_ZNK6icu_678numparse4impl13SeriesMatcher9smokeTestERKNS_13StringSegmentE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl13SeriesMatcher11postProcessERNS1_12ParsedNumberE
	.type	_ZNK6icu_678numparse4impl13SeriesMatcher11postProcessERNS1_12ParsedNumberE, @function
_ZNK6icu_678numparse4impl13SeriesMatcher11postProcessERNS1_12ParsedNumberE:
.LFB2810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_678numparse4impl18ArraySeriesMatcher5beginEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	movq	56(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L21
	movq	64(%rax), %rax
	leaq	_ZNK6icu_678numparse4impl18ArraySeriesMatcher3endEv(%rip), %rdx
	movq	8(%rdi), %rbx
	cmpq	%rdx, %rax
	jne	.L23
.L30:
	movslq	48(%rdi), %rdx
	movq	8(%rdi), %rax
	leaq	(%rax,%rdx,8), %r13
.L24:
	cmpq	%r13, %rbx
	je	.L20
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*40(%rax)
	cmpq	%rbx, %r13
	jne	.L27
.L20:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	%rdi, -40(%rbp)
	call	*%rdx
	movq	-40(%rbp), %rdi
	leaq	_ZNK6icu_678numparse4impl18ArraySeriesMatcher3endEv(%rip), %rdx
	movq	%rax, %rbx
	movq	(%rdi), %rax
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	je	.L30
.L23:
	call	*%rax
	movq	%rax, %r13
	jmp	.L24
	.cfi_endproc
.LFE2810:
	.size	_ZNK6icu_678numparse4impl13SeriesMatcher11postProcessERNS1_12ParsedNumberE, .-_ZNK6icu_678numparse4impl13SeriesMatcher11postProcessERNS1_12ParsedNumberE
	.section	.text._ZN6icu_678numparse4impl18ArraySeriesMatcherD0Ev,"axG",@progbits,_ZN6icu_678numparse4impl18ArraySeriesMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl18ArraySeriesMatcherD0Ev
	.type	_ZN6icu_678numparse4impl18ArraySeriesMatcherD0Ev, @function
_ZN6icu_678numparse4impl18ArraySeriesMatcherD0Ev:
.LFB3937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 20(%rdi)
	movq	%rax, (%rdi)
	jne	.L34
.L32:
	leaq	16+_ZTVN6icu_678numparse4impl18CompositionMatcherE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L32
	.cfi_endproc
.LFE3937:
	.size	_ZN6icu_678numparse4impl18ArraySeriesMatcherD0Ev, .-_ZN6icu_678numparse4impl18ArraySeriesMatcherD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl18ArraySeriesMatcherC2Ev
	.type	_ZN6icu_678numparse4impl18ArraySeriesMatcherC2Ev, @function
_ZN6icu_678numparse4impl18ArraySeriesMatcherC2Ev:
.LFB2829:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE(%rip), %rax
	movl	$3, 16(%rdi)
	movq	%rax, (%rdi)
	leaq	24(%rdi), %rax
	movq	%rax, 8(%rdi)
	movb	$0, 20(%rdi)
	movl	$0, 48(%rdi)
	ret
	.cfi_endproc
.LFE2829:
	.size	_ZN6icu_678numparse4impl18ArraySeriesMatcherC2Ev, .-_ZN6icu_678numparse4impl18ArraySeriesMatcherC2Ev
	.globl	_ZN6icu_678numparse4impl18ArraySeriesMatcherC1Ev
	.set	_ZN6icu_678numparse4impl18ArraySeriesMatcherC1Ev,_ZN6icu_678numparse4impl18ArraySeriesMatcherC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl18ArraySeriesMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEi
	.type	_ZN6icu_678numparse4impl18ArraySeriesMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEi, @function
_ZN6icu_678numparse4impl18ArraySeriesMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEi:
.LFB2833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rsi), %r8
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movzbl	12(%rsi), %eax
	movslq	8(%rsi), %rdx
	movq	%r8, 8(%rdi)
	movb	%al, 20(%rdi)
	leaq	16(%rsi), %rax
	movl	%edx, 16(%rdi)
	cmpq	%rax, %r8
	je	.L40
	movq	%rax, (%rsi)
	movl	$3, 8(%rsi)
	movb	$0, 12(%rsi)
	movl	%r12d, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	leaq	24(%rdi), %rdi
	salq	$3, %rdx
	movq	%r8, %rsi
	movq	%rdi, 8(%rbx)
	call	memcpy@PLT
	movl	%r12d, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2833:
	.size	_ZN6icu_678numparse4impl18ArraySeriesMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEi, .-_ZN6icu_678numparse4impl18ArraySeriesMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEi
	.globl	_ZN6icu_678numparse4impl18ArraySeriesMatcherC1ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEi
	.set	_ZN6icu_678numparse4impl18ArraySeriesMatcherC1ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEi,_ZN6icu_678numparse4impl18ArraySeriesMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl13SeriesMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.type	_ZNK6icu_678numparse4impl13SeriesMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, @function
_ZNK6icu_678numparse4impl13SeriesMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode:
.LFB2801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$312, %rsp
	movq	%rcx, -296(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-272(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1ERKS2_@PLT
	movq	72(%r15), %rax
	leaq	80(%r15), %rsi
	movq	%rsi, -336(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	-128(%rbp), %rax
	leaq	144(%r15), %rsi
	movq	%rax, %rdi
	movq	%rsi, -344(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	208(%r15), %rax
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	leaq	_ZNK6icu_678numparse4impl18ArraySeriesMatcher5beginEv(%rip), %rcx
	movl	%eax, -324(%rbp)
	movq	(%rbx), %rax
	movq	56(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L42
	movq	8(%rbx), %r14
.L43:
	movq	64(%rax), %rax
	leaq	_ZNK6icu_678numparse4impl18ArraySeriesMatcher3endEv(%rip), %rcx
	movb	$1, -273(%rbp)
	movq	%r15, -288(%rbp)
	cmpq	%rcx, %rax
	jne	.L45
	.p2align 4,,10
	.p2align 3
.L80:
	movslq	48(%rbx), %rdx
	movq	8(%rbx), %rax
	leaq	(%rax,%rdx,8), %rax
.L46:
	cmpq	%r14, %rax
	jbe	.L47
	movq	%r12, %rdi
	movq	(%r14), %r15
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movb	$1, -273(%rbp)
	testl	%eax, %eax
	je	.L48
	movq	(%r15), %rax
	movq	-296(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	-288(%rbp), %rdx
	call	*24(%rax)
	movb	%al, -273(%rbp)
.L48:
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	leaq	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv(%rip), %rsi
	movl	%eax, %edx
	movq	(%r15), %rax
	movq	16(%rax), %rax
	cmpq	%rsi, %rax
	je	.L78
	movl	%edx, -280(%rbp)
	movq	%r15, %rdi
	call	*%rax
	movl	-280(%rbp), %edx
	cmpl	%edx, %r13d
	je	.L51
	testb	%al, %al
	je	.L79
	.p2align 4,,10
	.p2align 3
.L49:
	movq	(%rbx), %rax
	leaq	_ZNK6icu_678numparse4impl18ArraySeriesMatcher3endEv(%rip), %rcx
	movq	64(%rax), %rax
	cmpq	%rcx, %rax
	je	.L80
.L45:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L78:
	cmpl	%edx, %r13d
	je	.L76
.L55:
	movq	(%rbx), %rax
	leaq	_ZNK6icu_678numparse4impl18ArraySeriesMatcher3endEv(%rip), %rcx
	addq	$8, %r14
	movq	64(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L52
	movslq	48(%rbx), %rdx
	movq	8(%rbx), %rax
	leaq	(%rax,%rdx,8), %rax
.L53:
	cmpq	%r14, %rax
	jbe	.L49
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	movq	-288(%rbp), %rcx
	movl	72(%rcx), %esi
	cmpl	%esi, %eax
	je	.L49
	cmpl	%r13d, %esi
	jle	.L49
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment9setOffsetEi@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L79:
	cmpl	%edx, %r13d
	jne	.L55
	.p2align 4,,10
	.p2align 3
.L51:
	testb	%al, %al
	jne	.L81
	.p2align 4,,10
	.p2align 3
.L76:
	movl	-324(%rbp), %esi
	movq	-288(%rbp), %r15
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment9setOffsetEi@PLT
	movq	-304(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityaSERKS2_@PLT
	movq	-200(%rbp), %rax
	movq	-312(%rbp), %rsi
	movq	-336(%rbp), %rdi
	movq	%rax, 72(%r15)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-320(%rbp), %rsi
	movq	-344(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-64(%rbp), %rax
	movq	%rax, 208(%r15)
.L47:
	movq	-320(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-312(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-304(%rbp), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	movzbl	-273(%rbp), %eax
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L81:
	addq	$8, %r14
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rbx, %rdi
	call	*%rdx
	movq	%rax, %r14
	movq	(%rbx), %rax
	jmp	.L43
.L82:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2801:
	.size	_ZNK6icu_678numparse4impl13SeriesMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, .-_ZNK6icu_678numparse4impl13SeriesMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.weak	_ZTSN6icu_678numparse4impl18CompositionMatcherE
	.section	.rodata._ZTSN6icu_678numparse4impl18CompositionMatcherE,"aG",@progbits,_ZTSN6icu_678numparse4impl18CompositionMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl18CompositionMatcherE, @object
	.size	_ZTSN6icu_678numparse4impl18CompositionMatcherE, 44
_ZTSN6icu_678numparse4impl18CompositionMatcherE:
	.string	"N6icu_678numparse4impl18CompositionMatcherE"
	.weak	_ZTIN6icu_678numparse4impl18CompositionMatcherE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl18CompositionMatcherE,"awG",@progbits,_ZTIN6icu_678numparse4impl18CompositionMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl18CompositionMatcherE, @object
	.size	_ZTIN6icu_678numparse4impl18CompositionMatcherE, 24
_ZTIN6icu_678numparse4impl18CompositionMatcherE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl18CompositionMatcherE
	.quad	_ZTIN6icu_678numparse4impl18NumberParseMatcherE
	.weak	_ZTSN6icu_678numparse4impl13SeriesMatcherE
	.section	.rodata._ZTSN6icu_678numparse4impl13SeriesMatcherE,"aG",@progbits,_ZTSN6icu_678numparse4impl13SeriesMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl13SeriesMatcherE, @object
	.size	_ZTSN6icu_678numparse4impl13SeriesMatcherE, 39
_ZTSN6icu_678numparse4impl13SeriesMatcherE:
	.string	"N6icu_678numparse4impl13SeriesMatcherE"
	.weak	_ZTIN6icu_678numparse4impl13SeriesMatcherE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl13SeriesMatcherE,"awG",@progbits,_ZTIN6icu_678numparse4impl13SeriesMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl13SeriesMatcherE, @object
	.size	_ZTIN6icu_678numparse4impl13SeriesMatcherE, 24
_ZTIN6icu_678numparse4impl13SeriesMatcherE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl13SeriesMatcherE
	.quad	_ZTIN6icu_678numparse4impl18CompositionMatcherE
	.weak	_ZTSN6icu_678numparse4impl18ArraySeriesMatcherE
	.section	.rodata._ZTSN6icu_678numparse4impl18ArraySeriesMatcherE,"aG",@progbits,_ZTSN6icu_678numparse4impl18ArraySeriesMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl18ArraySeriesMatcherE, @object
	.size	_ZTSN6icu_678numparse4impl18ArraySeriesMatcherE, 44
_ZTSN6icu_678numparse4impl18ArraySeriesMatcherE:
	.string	"N6icu_678numparse4impl18ArraySeriesMatcherE"
	.weak	_ZTIN6icu_678numparse4impl18ArraySeriesMatcherE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl18ArraySeriesMatcherE,"awG",@progbits,_ZTIN6icu_678numparse4impl18ArraySeriesMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl18ArraySeriesMatcherE, @object
	.size	_ZTIN6icu_678numparse4impl18ArraySeriesMatcherE, 24
_ZTIN6icu_678numparse4impl18ArraySeriesMatcherE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl18ArraySeriesMatcherE
	.quad	_ZTIN6icu_678numparse4impl13SeriesMatcherE
	.weak	_ZTVN6icu_678numparse4impl18CompositionMatcherE
	.section	.data.rel.ro._ZTVN6icu_678numparse4impl18CompositionMatcherE,"awG",@progbits,_ZTVN6icu_678numparse4impl18CompositionMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl18CompositionMatcherE, @object
	.size	_ZTVN6icu_678numparse4impl18CompositionMatcherE, 88
_ZTVN6icu_678numparse4impl18CompositionMatcherE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl18CompositionMatcherE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_678numparse4impl13SeriesMatcherE
	.section	.data.rel.ro._ZTVN6icu_678numparse4impl13SeriesMatcherE,"awG",@progbits,_ZTVN6icu_678numparse4impl13SeriesMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl13SeriesMatcherE, @object
	.size	_ZTVN6icu_678numparse4impl13SeriesMatcherE, 96
_ZTVN6icu_678numparse4impl13SeriesMatcherE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl13SeriesMatcherE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl13SeriesMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl13SeriesMatcher9smokeTestERKNS_13StringSegmentE
	.quad	_ZNK6icu_678numparse4impl13SeriesMatcher11postProcessERNS1_12ParsedNumberE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE
	.section	.data.rel.ro.local._ZTVN6icu_678numparse4impl18ArraySeriesMatcherE,"awG",@progbits,_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE, @object
	.size	_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE, 96
_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl18ArraySeriesMatcherE
	.quad	_ZN6icu_678numparse4impl18ArraySeriesMatcherD1Ev
	.quad	_ZN6icu_678numparse4impl18ArraySeriesMatcherD0Ev
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl13SeriesMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl13SeriesMatcher9smokeTestERKNS_13StringSegmentE
	.quad	_ZNK6icu_678numparse4impl13SeriesMatcher11postProcessERNS1_12ParsedNumberE
	.quad	_ZNK6icu_678numparse4impl18ArraySeriesMatcher8toStringEv
	.quad	_ZNK6icu_678numparse4impl18ArraySeriesMatcher5beginEv
	.quad	_ZNK6icu_678numparse4impl18ArraySeriesMatcher3endEv
	.quad	_ZNK6icu_678numparse4impl18ArraySeriesMatcher6lengthEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
