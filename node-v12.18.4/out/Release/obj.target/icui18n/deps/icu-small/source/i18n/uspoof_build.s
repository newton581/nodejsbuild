	.file	"uspoof_build.cpp"
	.text
	.p2align 4
	.globl	uspoof_openFromSource_67
	.type	uspoof_openFromSource_67, @function
uspoof_openFromSource_67:
.LFB3170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	%rdi, -56(%rbp)
	movq	%rbx, %rdi
	call	uspoof_internalInitStatics_67@PLT
	movl	(%rbx), %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L1
	testq	%r13, %r13
	je	.L3
	movl	$0, 0(%r13)
.L3:
	testq	%r12, %r12
	je	.L4
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	$0, (%r12)
	movw	%si, 8(%r12)
	movw	%di, 40(%r12)
.L4:
	movl	$56, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L5
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679SpoofDataC1ER10UErrorCode@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L18
	movl	$48, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L7
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN6icu_679SpoofImplC1EPNS_9SpoofDataER10UErrorCode@PLT
	movl	(%rbx), %edx
	movq	-64(%rbp), %rax
	testl	%edx, %edx
	jg	.L19
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rbx, %r9
	movq	%r12, %r8
	movq	%r13, %rcx
	movl	%r15d, %edx
	movq	%rax, -56(%rbp)
	call	_ZN6icu_6721ConfusabledataBuilder19buildConfusableDataEPNS_9SpoofImplEPKciPiP11UParseErrorR10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	movq	-56(%rbp), %rax
	jg	.L20
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	%rax, %rdi
	call	_ZN6icu_679SpoofImplD0Ev@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	(%rax), %rdx
	movq	%rax, %rdi
	call	*8(%rdx)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN6icu_679SpoofDataD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	movl	$7, (%rbx)
	xorl	%eax, %eax
	jmp	.L1
.L7:
	movl	$7, (%rbx)
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_679SpoofDataD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %rax
	jmp	.L1
	.cfi_endproc
.LFE3170:
	.size	uspoof_openFromSource_67, .-uspoof_openFromSource_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
