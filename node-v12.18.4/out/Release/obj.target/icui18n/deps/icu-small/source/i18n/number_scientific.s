	.file	"number_scientific.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl18ScientificModifier15getPrefixLengthEv
	.type	_ZNK6icu_676number4impl18ScientificModifier15getPrefixLengthEv, @function
_ZNK6icu_676number4impl18ScientificModifier15getPrefixLengthEv:
.LFB3349:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3349:
	.size	_ZNK6icu_676number4impl18ScientificModifier15getPrefixLengthEv, .-_ZNK6icu_676number4impl18ScientificModifier15getPrefixLengthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl18ScientificModifier17getCodePointCountEv
	.type	_ZNK6icu_676number4impl18ScientificModifier17getCodePointCountEv, @function
_ZNK6icu_676number4impl18ScientificModifier17getCodePointCountEv:
.LFB3350:
	.cfi_startproc
	endbr64
	movl	$999, %eax
	ret
	.cfi_endproc
.LFE3350:
	.size	_ZNK6icu_676number4impl18ScientificModifier17getCodePointCountEv, .-_ZNK6icu_676number4impl18ScientificModifier17getCodePointCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl18ScientificModifier8isStrongEv
	.type	_ZNK6icu_676number4impl18ScientificModifier8isStrongEv, @function
_ZNK6icu_676number4impl18ScientificModifier8isStrongEv:
.LFB3351:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3351:
	.size	_ZNK6icu_676number4impl18ScientificModifier8isStrongEv, .-_ZNK6icu_676number4impl18ScientificModifier8isStrongEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl18ScientificModifier13getParametersERNS1_8Modifier10ParametersE
	.type	_ZNK6icu_676number4impl18ScientificModifier13getParametersERNS1_8Modifier10ParametersE, @function
_ZNK6icu_676number4impl18ScientificModifier13getParametersERNS1_8Modifier10ParametersE:
.LFB3353:
	.cfi_startproc
	endbr64
	movq	$0, (%rsi)
	ret
	.cfi_endproc
.LFE3353:
	.size	_ZNK6icu_676number4impl18ScientificModifier13getParametersERNS1_8Modifier10ParametersE, .-_ZNK6icu_676number4impl18ScientificModifier13getParametersERNS1_8Modifier10ParametersE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl17ScientificHandler13getMultiplierEi
	.type	_ZNK6icu_676number4impl17ScientificHandler13getMultiplierEi, @function
_ZNK6icu_676number4impl17ScientificHandler13getMultiplierEi:
.LFB3365:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	cmpb	$0, 1(%rax)
	movsbl	(%rax), %ecx
	jne	.L7
	cmpl	$1, %ecx
	jle	.L8
	movl	%esi, %eax
	cltd
	idivl	%ecx
	leal	(%rdx,%rcx), %eax
	cltd
	idivl	%ecx
	leal	1(%rdx), %ecx
.L7:
	subl	%esi, %ecx
	leal	-1(%rcx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$1, %ecx
	subl	%esi, %ecx
	leal	-1(%rcx), %eax
	ret
	.cfi_endproc
.LFE3365:
	.size	_ZNK6icu_676number4impl17ScientificHandler13getMultiplierEi, .-_ZNK6icu_676number4impl17ScientificHandler13getMultiplierEi
	.section	.text.unlikely,"ax",@progbits
	.align 2
	.globl	_ZNK6icu_676number4impl18ScientificModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.type	_ZNK6icu_676number4impl18ScientificModifier13containsFieldENS_22FormattedStringBuilder5FieldE, @function
_ZNK6icu_676number4impl18ScientificModifier13containsFieldENS_22FormattedStringBuilder5FieldE:
.LFB3352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	abort@PLT
	.cfi_endproc
.LFE3352:
	.size	_ZNK6icu_676number4impl18ScientificModifier13containsFieldENS_22FormattedStringBuilder5FieldE, .-_ZNK6icu_676number4impl18ScientificModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl18ScientificModifier22semanticallyEquivalentERKNS1_8ModifierE
	.type	_ZNK6icu_676number4impl18ScientificModifier22semanticallyEquivalentERKNS1_8ModifierE, @function
_ZNK6icu_676number4impl18ScientificModifier22semanticallyEquivalentERKNS1_8ModifierE:
.LFB3354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_676number4impl18ScientificModifierE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	_ZTIN6icu_676number4impl8ModifierE(%rip), %rsi
	subq	$8, %rsp
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L13
	movl	8(%rax), %eax
	cmpl	%eax, 8(%rbx)
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3354:
	.size	_ZNK6icu_676number4impl18ScientificModifier22semanticallyEquivalentERKNS1_8ModifierE, .-_ZNK6icu_676number4impl18ScientificModifier22semanticallyEquivalentERKNS1_8ModifierE
	.section	.text._ZN6icu_676number4impl17ScientificHandlerD2Ev,"axG",@progbits,_ZN6icu_676number4impl17ScientificHandlerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl17ScientificHandlerD2Ev
	.type	_ZN6icu_676number4impl17ScientificHandlerD2Ev, @function
_ZN6icu_676number4impl17ScientificHandlerD2Ev:
.LFB4494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN6icu_676number4impl17ScientificHandlerE(%rip), %rax
	leaq	-48(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	call	_ZN6icu_676number4impl18MultiplierProducerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.cfi_endproc
.LFE4494:
	.size	_ZN6icu_676number4impl17ScientificHandlerD2Ev, .-_ZN6icu_676number4impl17ScientificHandlerD2Ev
	.weak	_ZN6icu_676number4impl17ScientificHandlerD1Ev
	.set	_ZN6icu_676number4impl17ScientificHandlerD1Ev,_ZN6icu_676number4impl17ScientificHandlerD2Ev
	.section	.text._ZN6icu_676number4impl17ScientificHandlerD0Ev,"axG",@progbits,_ZN6icu_676number4impl17ScientificHandlerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl17ScientificHandlerD0Ev
	.type	_ZN6icu_676number4impl17ScientificHandlerD0Ev, @function
_ZN6icu_676number4impl17ScientificHandlerD0Ev:
.LFB4496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN6icu_676number4impl17ScientificHandlerE(%rip), %rax
	leaq	-48(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	call	_ZN6icu_676number4impl18MultiplierProducerD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4496:
	.size	_ZN6icu_676number4impl17ScientificHandlerD0Ev, .-_ZN6icu_676number4impl17ScientificHandlerD0Ev
	.section	.text._ZN6icu_676number4impl18ScientificModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl18ScientificModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl18ScientificModifierD2Ev
	.type	_ZN6icu_676number4impl18ScientificModifierD2Ev, @function
_ZN6icu_676number4impl18ScientificModifierD2Ev:
.LFB4498:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	.cfi_endproc
.LFE4498:
	.size	_ZN6icu_676number4impl18ScientificModifierD2Ev, .-_ZN6icu_676number4impl18ScientificModifierD2Ev
	.weak	_ZN6icu_676number4impl18ScientificModifierD1Ev
	.set	_ZN6icu_676number4impl18ScientificModifierD1Ev,_ZN6icu_676number4impl18ScientificModifierD2Ev
	.section	.text._ZN6icu_676number4impl18ScientificModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl18ScientificModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl18ScientificModifierD0Ev
	.type	_ZN6icu_676number4impl18ScientificModifierD0Ev, @function
_ZN6icu_676number4impl18ScientificModifierD0Ev:
.LFB4500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4500:
	.size	_ZN6icu_676number4impl18ScientificModifierD0Ev, .-_ZN6icu_676number4impl18ScientificModifierD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl17ScientificHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.type	_ZNK6icu_676number4impl17ScientificHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode, @function
_ZNK6icu_676number4impl17ScientificHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode:
.LFB3364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	32(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L22
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*32(%rax)
	testb	%al, %al
	jne	.L25
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	testb	%al, %al
	je	.L35
.L25:
	leaq	176(%rbx), %rax
	movq	%rax, 128(%rbx)
.L22:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	8(%rbx), %r15
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	testb	%al, %al
	je	.L26
	movq	16(%r14), %rax
	cmpb	$0, 1(%rax)
	je	.L29
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl12RoundingImpl19isSignificantDigitsEv@PLT
	testb	%al, %al
	jne	.L37
.L29:
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode@PLT
	xorl	%esi, %esi
.L28:
	leaq	136(%rbx), %rax
	movl	%esi, 144(%rbx)
	movq	%r12, %rdi
	movq	%rax, 128(%rbx)
	movq	%r14, 152(%rbx)
	call	_ZN6icu_676number4impl15DecimalQuantity14adjustExponentEi@PLT
	leaq	-96(%rbp), %rdi
	call	_ZN6icu_676number4impl12RoundingImpl11passThroughEv@PLT
	movl	-64(%rbp), %eax
	movdqa	-96(%rbp), %xmm0
	movdqa	-80(%rbp), %xmm1
	movl	%eax, 40(%rbx)
	movzbl	-60(%rbp), %eax
	movups	%xmm0, 8(%rbx)
	movb	%al, 44(%rbx)
	movups	%xmm1, 24(%rbx)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%r12, %rsi
	leaq	8(%r14), %rdx
	movq	%r13, %rcx
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl12RoundingImpl24chooseMultiplierAndApplyERNS1_15DecimalQuantityERKNS1_18MultiplierProducerER10UErrorCode@PLT
	negl	%eax
	movl	%eax, %esi
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L37:
	movq	16(%r14), %rax
	movl	0(%r13), %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movsbl	(%rax), %edx
	call	_ZN6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityEi10UErrorCode@PLT
	xorl	%esi, %esi
	jmp	.L28
.L36:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3364:
	.size	_ZNK6icu_676number4impl17ScientificHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode, .-_ZNK6icu_676number4impl17ScientificHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.section	.text._ZN6icu_676number4impl17ScientificHandlerD2Ev,"axG",@progbits,_ZN6icu_676number4impl17ScientificHandlerD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N6icu_676number4impl17ScientificHandlerD1Ev
	.type	_ZThn8_N6icu_676number4impl17ScientificHandlerD1Ev, @function
_ZThn8_N6icu_676number4impl17ScientificHandlerD1Ev:
.LFB4512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN6icu_676number4impl17ScientificHandlerE(%rip), %rax
	leaq	-48(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	call	_ZN6icu_676number4impl18MultiplierProducerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.cfi_endproc
.LFE4512:
	.size	_ZThn8_N6icu_676number4impl17ScientificHandlerD1Ev, .-_ZThn8_N6icu_676number4impl17ScientificHandlerD1Ev
	.section	.text._ZN6icu_676number4impl17ScientificHandlerD0Ev,"axG",@progbits,_ZN6icu_676number4impl17ScientificHandlerD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N6icu_676number4impl17ScientificHandlerD0Ev
	.type	_ZThn8_N6icu_676number4impl17ScientificHandlerD0Ev, @function
_ZThn8_N6icu_676number4impl17ScientificHandlerD0Ev:
.LFB4513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN6icu_676number4impl17ScientificHandlerE(%rip), %rax
	leaq	-48(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	call	_ZN6icu_676number4impl18MultiplierProducerD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4513:
	.size	_ZThn8_N6icu_676number4impl17ScientificHandlerD0Ev, .-_ZThn8_N6icu_676number4impl17ScientificHandlerD0Ev
	.text
	.p2align 4
	.globl	_ZThn8_NK6icu_676number4impl17ScientificHandler13getMultiplierEi
	.type	_ZThn8_NK6icu_676number4impl17ScientificHandler13getMultiplierEi, @function
_ZThn8_NK6icu_676number4impl17ScientificHandler13getMultiplierEi:
.LFB4514:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	cmpb	$0, 1(%rax)
	movsbl	(%rax), %ecx
	jne	.L43
	cmpl	$1, %ecx
	jle	.L44
	movl	%esi, %eax
	cltd
	idivl	%ecx
	leal	(%rdx,%rcx), %eax
	cltd
	idivl	%ecx
	leal	1(%rdx), %ecx
.L43:
	subl	%esi, %ecx
	leal	-1(%rcx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$1, %ecx
	subl	%esi, %ecx
	leal	-1(%rcx), %eax
	ret
	.cfi_endproc
.LFE4514:
	.size	_ZThn8_NK6icu_676number4impl17ScientificHandler13getMultiplierEi, .-_ZThn8_NK6icu_676number4impl17ScientificHandler13getMultiplierEi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3841:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3841:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3844:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L58
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L46
	cmpb	$0, 12(%rbx)
	jne	.L59
.L50:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L46:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L50
	.cfi_endproc
.LFE3844:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3847:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L62
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3847:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3850:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L65
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3850:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L71
.L67:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L72
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L72:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3852:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3853:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3853:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3854:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3854:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3855:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3855:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3856:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3856:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3857:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3857:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3858:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L88
	testl	%edx, %edx
	jle	.L88
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L91
.L80:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L80
	.cfi_endproc
.LFE3858:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L95
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L95
	testl	%r12d, %r12d
	jg	.L102
	cmpb	$0, 12(%rbx)
	jne	.L103
.L97:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L97
.L103:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3859:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L105
	movq	(%rdi), %r8
.L106:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L109
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L109
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L109:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3860:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3861:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L116
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3861:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3862:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3862:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3863:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3863:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3864:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3864:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3866:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3866:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3868:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3868:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl18ScientificModifierC2Ev
	.type	_ZN6icu_676number4impl18ScientificModifierC2Ev, @function
_ZN6icu_676number4impl18ScientificModifierC2Ev:
.LFB3345:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	movl	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 16(%rdi)
	ret
	.cfi_endproc
.LFE3345:
	.size	_ZN6icu_676number4impl18ScientificModifierC2Ev, .-_ZN6icu_676number4impl18ScientificModifierC2Ev
	.globl	_ZN6icu_676number4impl18ScientificModifierC1Ev
	.set	_ZN6icu_676number4impl18ScientificModifierC1Ev,_ZN6icu_676number4impl18ScientificModifierC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl18ScientificModifier3setEiPKNS1_17ScientificHandlerE
	.type	_ZN6icu_676number4impl18ScientificModifier3setEiPKNS1_17ScientificHandlerE, @function
_ZN6icu_676number4impl18ScientificModifier3setEiPKNS1_17ScientificHandlerE:
.LFB3347:
	.cfi_startproc
	endbr64
	movl	%esi, 8(%rdi)
	movq	%rdx, 16(%rdi)
	ret
	.cfi_endproc
.LFE3347:
	.size	_ZN6icu_676number4impl18ScientificModifier3setEiPKNS1_17ScientificHandlerE, .-_ZN6icu_676number4impl18ScientificModifier3setEiPKNS1_17ScientificHandlerE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl17ScientificHandlerC2EPKNS0_8NotationEPKNS_20DecimalFormatSymbolsEPKNS1_19MicroPropsGeneratorE
	.type	_ZN6icu_676number4impl17ScientificHandlerC2EPKNS0_8NotationEPKNS_20DecimalFormatSymbolsEPKNS1_19MicroPropsGeneratorE, @function
_ZN6icu_676number4impl17ScientificHandlerC2EPKNS0_8NotationEPKNS_20DecimalFormatSymbolsEPKNS1_19MicroPropsGeneratorE:
.LFB3362:
	.cfi_startproc
	endbr64
	leaq	64+_ZTVN6icu_676number4impl17ScientificHandlerE(%rip), %rax
	addq	$4, %rsi
	movq	%rdx, 24(%rdi)
	leaq	-48(%rax), %r8
	movq	%rax, %xmm1
	movq	%rsi, 16(%rdi)
	movq	%r8, %xmm0
	movq	%rcx, 32(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE3362:
	.size	_ZN6icu_676number4impl17ScientificHandlerC2EPKNS0_8NotationEPKNS_20DecimalFormatSymbolsEPKNS1_19MicroPropsGeneratorE, .-_ZN6icu_676number4impl17ScientificHandlerC2EPKNS0_8NotationEPKNS_20DecimalFormatSymbolsEPKNS1_19MicroPropsGeneratorE
	.globl	_ZN6icu_676number4impl17ScientificHandlerC1EPKNS0_8NotationEPKNS_20DecimalFormatSymbolsEPKNS1_19MicroPropsGeneratorE
	.set	_ZN6icu_676number4impl17ScientificHandlerC1EPKNS0_8NotationEPKNS_20DecimalFormatSymbolsEPKNS1_19MicroPropsGeneratorE,_ZN6icu_676number4impl17ScientificHandlerC2EPKNS0_8NotationEPKNS_20DecimalFormatSymbolsEPKNS1_19MicroPropsGeneratorE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl18ScientificModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.type	_ZNK6icu_676number4impl18ScientificModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, @function
_ZNK6icu_676number4impl18ScientificModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode:
.LFB3348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%rdi, -136(%rbp)
	movl	%ecx, -148(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	addq	$712, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	%r15d, %esi
	movq	%r14, %r8
	movl	$35, %ecx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	%r12, %rdi
	addl	%eax, %r15d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	8(%rbx), %eax
	testl	%eax, %eax
	js	.L147
	movq	-136(%rbp), %rdi
	movq	16(%rdi), %rsi
	movq	16(%rsi), %rdi
	cmpl	$1, 4(%rdi)
	je	.L148
.L128:
	cltd
	movl	%edx, %ecx
	xorl	%eax, %ecx
	subl	%edx, %ecx
	cmpw	$0, 2(%rdi)
	jg	.L136
.L151:
	testl	%eax, %eax
	je	.L129
.L136:
	xorl	%r12d, %r12d
	movl	$3435973837, %ebx
	.p2align 4,,10
	.p2align 3
.L142:
	movl	%ecx, %r9d
	movq	%r9, %rdx
	imulq	%rbx, %rdx
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movq	24(%rsi), %rax
	movl	%r15d, %esi
	movl	%ecx, %edx
	subl	%r12d, %esi
	movl	1864(%rax), %ecx
	cmpl	$-1, %ecx
	je	.L131
	addl	%ecx, %edx
	movq	%r14, %r8
	movl	$37, %ecx
	movq	%r13, %rdi
	movq	%r9, -144(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode@PLT
	movq	-144(%rbp), %r9
.L132:
	addl	%eax, %r15d
	movq	-136(%rbp), %rax
	imulq	%rbx, %r9
	addl	$1, %r12d
	movq	16(%rax), %rsi
	shrq	$35, %r9
	movq	16(%rsi), %rax
	movq	%r9, %rcx
	movswl	2(%rax), %eax
	cmpl	%r12d, %eax
	jg	.L142
	testl	%r9d, %r9d
	jne	.L142
.L129:
	movl	%r15d, %eax
	subl	-148(%rbp), %eax
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L149
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L150
	addl	$17, %edx
	movslq	%edx, %rdx
	salq	$6, %rdx
	leaq	8(%rax,%rdx), %rdx
.L134:
	movq	%r14, %r8
	movl	$37, %ecx
	movq	%r13, %rdi
	movq	%r9, -144(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	-144(%rbp), %r9
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	264(%rax), %rdx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L147:
	movq	16(%rbx), %rsi
	negl	%eax
	movl	%eax, %ecx
	movq	16(%rsi), %rdx
	cmpl	$2, 4(%rdx)
	je	.L136
	movq	24(%rsi), %rsi
	movq	%r12, %rdi
	addq	$392, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	%r15d, %esi
	movl	$36, %ecx
	movq	%r12, %rdx
	movq	%r14, %r8
	movq	%r13, %rdi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	%r12, %rdi
	addl	%eax, %r15d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	8(%rbx), %eax
	movq	16(%rbx), %rsi
	cltd
	movq	16(%rsi), %rdi
	movl	%edx, %ecx
	xorl	%eax, %ecx
	subl	%edx, %ecx
	cmpw	$0, 2(%rdi)
	jle	.L151
	jmp	.L136
.L148:
	movq	24(%rsi), %rsi
	movq	%r12, %rdi
	addq	$456, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	%r15d, %esi
	movq	%r14, %r8
	movl	$36, %ecx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	%r12, %rdi
	addl	%eax, %r15d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-136(%rbp), %rdi
	movq	16(%rdi), %rsi
	movl	8(%rdi), %eax
	movq	16(%rsi), %rdi
	jmp	.L128
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3348:
	.size	_ZNK6icu_676number4impl18ScientificModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, .-_ZNK6icu_676number4impl18ScientificModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_676number4impl18ScientificModifierE
	.section	.rodata._ZTSN6icu_676number4impl18ScientificModifierE,"aG",@progbits,_ZTSN6icu_676number4impl18ScientificModifierE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl18ScientificModifierE, @object
	.size	_ZTSN6icu_676number4impl18ScientificModifierE, 42
_ZTSN6icu_676number4impl18ScientificModifierE:
	.string	"N6icu_676number4impl18ScientificModifierE"
	.weak	_ZTIN6icu_676number4impl18ScientificModifierE
	.section	.data.rel.ro._ZTIN6icu_676number4impl18ScientificModifierE,"awG",@progbits,_ZTIN6icu_676number4impl18ScientificModifierE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl18ScientificModifierE, @object
	.size	_ZTIN6icu_676number4impl18ScientificModifierE, 56
_ZTIN6icu_676number4impl18ScientificModifierE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl18ScientificModifierE
	.long	0
	.long	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.quad	_ZTIN6icu_676number4impl8ModifierE
	.quad	2
	.weak	_ZTSN6icu_676number4impl17ScientificHandlerE
	.section	.rodata._ZTSN6icu_676number4impl17ScientificHandlerE,"aG",@progbits,_ZTSN6icu_676number4impl17ScientificHandlerE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl17ScientificHandlerE, @object
	.size	_ZTSN6icu_676number4impl17ScientificHandlerE, 41
_ZTSN6icu_676number4impl17ScientificHandlerE:
	.string	"N6icu_676number4impl17ScientificHandlerE"
	.weak	_ZTIN6icu_676number4impl17ScientificHandlerE
	.section	.data.rel.ro._ZTIN6icu_676number4impl17ScientificHandlerE,"awG",@progbits,_ZTIN6icu_676number4impl17ScientificHandlerE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl17ScientificHandlerE, @object
	.size	_ZTIN6icu_676number4impl17ScientificHandlerE, 72
_ZTIN6icu_676number4impl17ScientificHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl17ScientificHandlerE
	.long	0
	.long	3
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.quad	_ZTIN6icu_676number4impl19MicroPropsGeneratorE
	.quad	2
	.quad	_ZTIN6icu_676number4impl18MultiplierProducerE
	.quad	2050
	.weak	_ZTVN6icu_676number4impl18ScientificModifierE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl18ScientificModifierE,"awG",@progbits,_ZTVN6icu_676number4impl18ScientificModifierE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl18ScientificModifierE, @object
	.size	_ZTVN6icu_676number4impl18ScientificModifierE, 88
_ZTVN6icu_676number4impl18ScientificModifierE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl18ScientificModifierE
	.quad	_ZN6icu_676number4impl18ScientificModifierD1Ev
	.quad	_ZN6icu_676number4impl18ScientificModifierD0Ev
	.quad	_ZNK6icu_676number4impl18ScientificModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.quad	_ZNK6icu_676number4impl18ScientificModifier15getPrefixLengthEv
	.quad	_ZNK6icu_676number4impl18ScientificModifier17getCodePointCountEv
	.quad	_ZNK6icu_676number4impl18ScientificModifier8isStrongEv
	.quad	_ZNK6icu_676number4impl18ScientificModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.quad	_ZNK6icu_676number4impl18ScientificModifier13getParametersERNS1_8Modifier10ParametersE
	.quad	_ZNK6icu_676number4impl18ScientificModifier22semanticallyEquivalentERKNS1_8ModifierE
	.weak	_ZTVN6icu_676number4impl17ScientificHandlerE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl17ScientificHandlerE,"awG",@progbits,_ZTVN6icu_676number4impl17ScientificHandlerE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl17ScientificHandlerE, @object
	.size	_ZTVN6icu_676number4impl17ScientificHandlerE, 88
_ZTVN6icu_676number4impl17ScientificHandlerE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl17ScientificHandlerE
	.quad	_ZN6icu_676number4impl17ScientificHandlerD1Ev
	.quad	_ZN6icu_676number4impl17ScientificHandlerD0Ev
	.quad	_ZNK6icu_676number4impl17ScientificHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.quad	_ZNK6icu_676number4impl17ScientificHandler13getMultiplierEi
	.quad	-8
	.quad	_ZTIN6icu_676number4impl17ScientificHandlerE
	.quad	_ZThn8_N6icu_676number4impl17ScientificHandlerD1Ev
	.quad	_ZThn8_N6icu_676number4impl17ScientificHandlerD0Ev
	.quad	_ZThn8_NK6icu_676number4impl17ScientificHandler13getMultiplierEi
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
