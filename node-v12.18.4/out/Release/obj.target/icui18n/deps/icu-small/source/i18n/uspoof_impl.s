	.file	"uspoof_impl.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679SpoofImpl17getDynamicClassIDEv
	.type	_ZNK6icu_679SpoofImpl17getDynamicClassIDEv, @function
_ZNK6icu_679SpoofImpl17getDynamicClassIDEv:
.LFB3070:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_679SpoofImpl16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3070:
	.size	_ZNK6icu_679SpoofImpl17getDynamicClassIDEv, .-_ZNK6icu_679SpoofImpl17getDynamicClassIDEv
	.p2align 4
	.type	_ZN6icu_67L21spoofDataIsAcceptableEPvPKcS2_PK9UDataInfo, @function
_ZN6icu_67L21spoofDataIsAcceptableEPvPKcS2_PK9UDataInfo:
.LFB3122:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpw	$19, (%rcx)
	jbe	.L3
	cmpw	$0, 4(%rcx)
	jne	.L3
	cmpw	$26179, 8(%rcx)
	jne	.L3
	cmpw	$8309, 10(%rcx)
	jne	.L3
	cmpb	$2, 12(%rcx)
	je	.L12
.L3:
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$1, %eax
	testq	%rdi, %rdi
	je	.L3
	movl	16(%rcx), %edx
	movl	%edx, (%rdi)
	ret
	.cfi_endproc
.LFE3122:
	.size	_ZN6icu_67L21spoofDataIsAcceptableEPvPKcS2_PK9UDataInfo, .-_ZN6icu_67L21spoofDataIsAcceptableEPvPKcS2_PK9UDataInfo
	.align 2
	.p2align 4
	.type	_ZN6icu_679SpoofImpl21getAugmentedScriptSetEiRNS_9ScriptSetER10UErrorCode.part.0, @function
_ZN6icu_679SpoofImpl21getAugmentedScriptSetEiRNS_9ScriptSetER10UErrorCode.part.0:
.LFB4084:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$17, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode@PLT
	testb	%al, %al
	jne	.L37
.L14:
	movq	%r13, %rdx
	movl	$20, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode@PLT
	testb	%al, %al
	jne	.L38
.L15:
	movq	%r13, %rdx
	movl	$22, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode@PLT
	testb	%al, %al
	jne	.L39
.L16:
	movq	%r13, %rdx
	movl	$18, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode@PLT
	testb	%al, %al
	jne	.L40
.L17:
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode@PLT
	testb	%al, %al
	jne	.L41
.L18:
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode@PLT
	testb	%al, %al
	jne	.L20
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode@PLT
	testb	%al, %al
	je	.L42
.L20:
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679ScriptSet6setAllEv@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	%r13, %rdx
	movl	$172, %esi
	movq	%r12, %rdi
	call	_ZN6icu_679ScriptSet3setE11UScriptCodeR10UErrorCode@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%r13, %rdx
	movl	$119, %esi
	movq	%r12, %rdi
	call	_ZN6icu_679ScriptSet3setE11UScriptCodeR10UErrorCode@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r13, %rdx
	movl	$105, %esi
	movq	%r12, %rdi
	call	_ZN6icu_679ScriptSet3setE11UScriptCodeR10UErrorCode@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%r13, %rdx
	movl	$105, %esi
	movq	%r12, %rdi
	call	_ZN6icu_679ScriptSet3setE11UScriptCodeR10UErrorCode@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$172, %esi
	call	_ZN6icu_679ScriptSet3setE11UScriptCodeR10UErrorCode@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$105, %esi
	call	_ZN6icu_679ScriptSet3setE11UScriptCodeR10UErrorCode@PLT
	movq	%r13, %rdx
	movl	$119, %esi
	movq	%r12, %rdi
	call	_ZN6icu_679ScriptSet3setE11UScriptCodeR10UErrorCode@PLT
	jmp	.L14
	.cfi_endproc
.LFE4084:
	.size	_ZN6icu_679SpoofImpl21getAugmentedScriptSetEiRNS_9ScriptSetER10UErrorCode.part.0, .-_ZN6icu_679SpoofImpl21getAugmentedScriptSetEiRNS_9ScriptSetER10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZNK6icu_679SpoofImpl27getResolvedScriptSetWithoutERKNS_13UnicodeStringE11UScriptCodeRNS_9ScriptSetER10UErrorCode.constprop.0, @function
_ZNK6icu_679SpoofImpl27getResolvedScriptSetWithoutERKNS_13UnicodeStringE11UScriptCodeRNS_9ScriptSetER10UErrorCode.constprop.0:
.LFB4097:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rsi, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_679ScriptSet6setAllEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_679ScriptSetC1Ev@PLT
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L44
	.p2align 4,,10
	.p2align 3
.L59:
	sarl	$5, %eax
.L45:
	cmpl	%eax, %r14d
	jge	.L46
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN6icu_679ScriptSet8resetAllEv@PLT
	movq	%r13, %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN6icu_679ScriptSet19setScriptExtensionsEiR10UErrorCode@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L57
.L46:
	movq	%r12, %rdi
	call	_ZN6icu_679ScriptSetD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_679SpoofImpl21getAugmentedScriptSetEiRNS_9ScriptSetER10UErrorCode.part.0
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L46
	movq	-104(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_679ScriptSet9intersectERKS0_@PLT
	xorl	%eax, %eax
	cmpl	$65535, %ebx
	seta	%al
	leal	1(%r14,%rax), %r14d
	movswl	8(%r15), %eax
	testw	%ax, %ax
	jns	.L59
.L44:
	movl	12(%r15), %eax
	jmp	.L45
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4097:
	.size	_ZNK6icu_679SpoofImpl27getResolvedScriptSetWithoutERKNS_13UnicodeStringE11UScriptCodeRNS_9ScriptSetER10UErrorCode.constprop.0, .-_ZNK6icu_679SpoofImpl27getResolvedScriptSetWithoutERKNS_13UnicodeStringE11UScriptCodeRNS_9ScriptSetER10UErrorCode.constprop.0
	.p2align 4
	.type	_ZN6icu_67L25uspoof_cleanupDefaultDataEv, @function
_ZN6icu_67L25uspoof_cleanupDefaultDataEv:
.LFB3123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	_ZN6icu_67L17gDefaultSpoofDataE(%rip), %r12
	testq	%r12, %r12
	je	.L61
	lock subl	$1, 28(%r12)
	je	.L72
.L62:
	movq	$0, _ZN6icu_67L17gDefaultSpoofDataE(%rip)
	movl	$0, _ZN6icu_67L21gSpoofInitDefaultOnceE(%rip)
	mfence
.L61:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	cmpb	$0, 8(%r12)
	jne	.L73
.L63:
	movq	16(%r12), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L64
	call	udata_close_67@PLT
.L64:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L63
	.cfi_endproc
.LFE3123:
	.size	_ZN6icu_67L25uspoof_cleanupDefaultDataEv, .-_ZN6icu_67L25uspoof_cleanupDefaultDataEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"confusables"
.LC1:
	.string	"cfu"
	.text
	.p2align 4
	.type	_ZN6icu_67L22uspoof_loadDefaultDataER10UErrorCode, @function
_ZN6icu_67L22uspoof_loadDefaultDataER10UErrorCode:
.LFB3124:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	leaq	.LC1(%rip), %rsi
	xorl	%r8d, %r8d
	leaq	_ZN6icu_67L21spoofDataIsAcceptableEPvPKcS2_PK9UDataInfo(%rip), %rcx
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$8, %rsp
	call	udata_openChoice_67@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L100
.L74:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movl	$56, %edi
	movq	%rax, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L76
	movq	$0, (%rax)
	pxor	%xmm0, %xmm0
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	movl	$0, 24(%rax)
	movl	$1, 28(%rax)
	mfence
	movq	$0, 48(%rax)
	movups	%xmm0, 32(%rax)
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L101
.L77:
	cmpb	$0, 8(%r12)
	movq	%r12, _ZN6icu_67L17gDefaultSpoofDataE(%rip)
	jne	.L102
.L86:
	movq	16(%r12), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L83
	call	udata_close_67@PLT
.L83:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L84:
	movq	$0, _ZN6icu_67L17gDefaultSpoofDataE(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%r13, 16(%rax)
	movq	%r13, %rdi
	call	udata_getMemory_67@PLT
	movl	(%rbx), %edx
	movq	%rax, (%r12)
	testl	%edx, %edx
	jg	.L78
	testq	%rax, %rax
	je	.L78
	cmpl	$944111087, (%rax)
	jne	.L78
	cmpl	$2, 4(%rax)
	pxor	%xmm0, %xmm0
	jne	.L78
	movups	%xmm0, 32(%r12)
	movslq	12(%rax), %rdx
	movq	$0, 48(%r12)
	testl	%edx, %edx
	je	.L80
	addq	%rax, %rdx
	movq	%rdx, 32(%r12)
.L80:
	movslq	20(%rax), %rdx
	testl	%edx, %edx
	je	.L81
	addq	%rax, %rdx
	movq	%rdx, 40(%r12)
.L81:
	movslq	28(%rax), %rdx
	testl	%edx, %edx
	je	.L82
	addq	%rdx, %rax
	movq	%rax, 48(%r12)
.L82:
	movq	%r12, _ZN6icu_67L17gDefaultSpoofDataE(%rip)
	addq	$8, %rsp
	movl	$4, %edi
	leaq	_ZN6icu_67L25uspoof_cleanupDefaultDataEv(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ucln_i18n_registerCleanup_67@PLT
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movl	$3, (%rbx)
	movq	$0, 48(%r12)
	movups	%xmm0, 32(%r12)
	jmp	.L77
.L76:
	movq	$0, _ZN6icu_67L17gDefaultSpoofDataE(%rip)
	cmpl	$0, (%rbx)
	jg	.L84
	movl	$7, (%rbx)
	jmp	.L74
	.cfi_endproc
.LFE3124:
	.size	_ZN6icu_67L22uspoof_loadDefaultDataER10UErrorCode, .-_ZN6icu_67L22uspoof_loadDefaultDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImpl16getStaticClassIDEv
	.type	_ZN6icu_679SpoofImpl16getStaticClassIDEv, @function
_ZN6icu_679SpoofImpl16getStaticClassIDEv:
.LFB3069:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_679SpoofImpl16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3069:
	.size	_ZN6icu_679SpoofImpl16getStaticClassIDEv, .-_ZN6icu_679SpoofImpl16getStaticClassIDEv
	.section	.rodata.str1.1
.LC2:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImplC2EPNS_9SpoofDataER10UErrorCode
	.type	_ZN6icu_679SpoofImplC2EPNS_9SpoofDataER10UErrorCode, @function
_ZN6icu_679SpoofImplC2EPNS_9SpoofDataER10UErrorCode:
.LFB3075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679SpoofImplE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movabsq	$281471625854447, %rax
	movq	%rax, 8(%rdi)
	movl	(%rdx), %eax
	movq	$0, 32(%rdi)
	movl	$805306368, 40(%rdi)
	movups	%xmm0, 16(%rdi)
	testl	%eax, %eax
	jle	.L114
	movq	%r13, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movl	$200, %edi
	movq	%rdx, %r12
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L106
	movl	$1114111, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
.L106:
	movq	%r14, 24(%rbx)
	leaq	.LC2(%rip), %rdi
	call	uprv_strdup_67@PLT
	cmpq	$0, 24(%rbx)
	movq	%rax, 32(%rbx)
	je	.L109
	testq	%rax, %rax
	je	.L109
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r13, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movl	$7, (%r12)
	movq	%r13, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3075:
	.size	_ZN6icu_679SpoofImplC2EPNS_9SpoofDataER10UErrorCode, .-_ZN6icu_679SpoofImplC2EPNS_9SpoofDataER10UErrorCode
	.globl	_ZN6icu_679SpoofImplC1EPNS_9SpoofDataER10UErrorCode
	.set	_ZN6icu_679SpoofImplC1EPNS_9SpoofDataER10UErrorCode,_ZN6icu_679SpoofImplC2EPNS_9SpoofDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImplC2ER10UErrorCode
	.type	_ZN6icu_679SpoofImplC2ER10UErrorCode, @function
_ZN6icu_679SpoofImplC2ER10UErrorCode:
.LFB3078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679SpoofImplE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rsi), %ecx
	movq	%rax, (%rdi)
	movabsq	$281471625854447, %rax
	movq	%rax, 8(%rdi)
	movq	$0, 32(%rdi)
	movl	$805306368, 40(%rdi)
	movups	%xmm0, 16(%rdi)
	testl	%ecx, %ecx
	jle	.L136
.L122:
	xorl	%eax, %eax
	movq	%rax, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movl	$200, %edi
	movq	%rsi, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L117
	movl	$1114111, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
.L117:
	movq	%r12, 24(%rbx)
	leaq	.LC2(%rip), %rdi
	call	uprv_strdup_67@PLT
	cmpq	$0, 24(%rbx)
	movq	%rax, 32(%rbx)
	je	.L124
	testq	%rax, %rax
	je	.L124
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L122
	movl	_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L137
.L120:
	movl	4+_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L121
	movl	%eax, 0(%r13)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L120
	movq	%r13, %rdi
	call	_ZN6icu_67L22uspoof_loadDefaultDataER10UErrorCode
	movl	0(%r13), %eax
	leaq	_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L121:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L122
	movq	_ZN6icu_67L17gDefaultSpoofDataE(%rip), %rax
	lock addl	$1, 28(%rax)
	movq	_ZN6icu_67L17gDefaultSpoofDataE(%rip), %rax
	movq	%rax, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movl	$7, 0(%r13)
	jmp	.L122
	.cfi_endproc
.LFE3078:
	.size	_ZN6icu_679SpoofImplC2ER10UErrorCode, .-_ZN6icu_679SpoofImplC2ER10UErrorCode
	.globl	_ZN6icu_679SpoofImplC1ER10UErrorCode
	.set	_ZN6icu_679SpoofImplC1ER10UErrorCode,_ZN6icu_679SpoofImplC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImplC2Ev
	.type	_ZN6icu_679SpoofImplC2Ev, @function
_ZN6icu_679SpoofImplC2Ev:
.LFB3081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_679SpoofImplE(%rip), %rax
	movq	$0, 32(%rdi)
	movq	%rax, (%rdi)
	movabsq	$281471625854447, %rax
	movq	%rax, 8(%rdi)
	movl	$805306368, 40(%rdi)
	movups	%xmm0, 16(%rdi)
	movl	$200, %edi
	movl	$0, -28(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L139
	movl	$1114111, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
.L139:
	movq	%r12, 24(%rbx)
	leaq	.LC2(%rip), %rdi
	call	uprv_strdup_67@PLT
	cmpq	$0, 24(%rbx)
	movq	%rax, 32(%rbx)
	je	.L143
	testq	%rax, %rax
	je	.L143
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movl	-28(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L143
	movl	_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L157
.L141:
	movl	4+_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip), %edx
	testl	%edx, %edx
	jle	.L142
.L143:
	xorl	%eax, %eax
.L144:
	movq	%rax, 16(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L158
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	leaq	_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L141
	leaq	-28(%rbp), %rdi
	call	_ZN6icu_67L22uspoof_loadDefaultDataER10UErrorCode
	movl	-28(%rbp), %eax
	leaq	_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L142:
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L143
	movq	_ZN6icu_67L17gDefaultSpoofDataE(%rip), %rax
	lock addl	$1, 28(%rax)
	movq	_ZN6icu_67L17gDefaultSpoofDataE(%rip), %rax
	jmp	.L144
.L158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3081:
	.size	_ZN6icu_679SpoofImplC2Ev, .-_ZN6icu_679SpoofImplC2Ev
	.globl	_ZN6icu_679SpoofImplC1Ev
	.set	_ZN6icu_679SpoofImplC1Ev,_ZN6icu_679SpoofImplC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImpl9constructER10UErrorCode
	.type	_ZN6icu_679SpoofImpl9constructER10UErrorCode, @function
_ZN6icu_679SpoofImpl9constructER10UErrorCode:
.LFB3083:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$65535, 12(%rdi)
	movups	%xmm0, 16(%rdi)
	movl	(%rsi), %eax
	movq	$0, 32(%rdi)
	movl	$805306368, 40(%rdi)
	testl	%eax, %eax
	jle	.L172
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$200, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L161
	movl	$1114111, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
.L161:
	movq	%r13, 24(%rbx)
	leaq	.LC2(%rip), %rdi
	call	uprv_strdup_67@PLT
	cmpq	$0, 24(%rbx)
	movq	%rax, 32(%rbx)
	je	.L164
	testq	%rax, %rax
	je	.L164
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movl	$7, (%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3083:
	.size	_ZN6icu_679SpoofImpl9constructER10UErrorCode, .-_ZN6icu_679SpoofImpl9constructER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImpl15asUSpoofCheckerEv
	.type	_ZN6icu_679SpoofImpl15asUSpoofCheckerEv, @function
_ZN6icu_679SpoofImpl15asUSpoofCheckerEv:
.LFB3091:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE3091:
	.size	_ZN6icu_679SpoofImpl15asUSpoofCheckerEv, .-_ZN6icu_679SpoofImpl15asUSpoofCheckerEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode
	.type	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode, @function
_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode:
.LFB3092:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L175
	testq	%rdi, %rdi
	je	.L185
	cmpl	$944111087, 8(%rdi)
	jne	.L186
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L187
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L178
	cmpl	$944111087, (%rdx)
	jne	.L178
	cmpl	$2, 4(%rdx)
	movq	%rdi, %rax
	je	.L174
.L178:
	movl	$3, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$3, (%rsi)
.L175:
	xorl	%eax, %eax
.L174:
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	jmp	.L174
	.cfi_endproc
.LFE3092:
	.size	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode, .-_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImpl12validateThisEP13USpoofCheckerR10UErrorCode
	.type	_ZN6icu_679SpoofImpl12validateThisEP13USpoofCheckerR10UErrorCode, @function
_ZN6icu_679SpoofImpl12validateThisEP13USpoofCheckerR10UErrorCode:
.LFB3093:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L189
	testq	%rdi, %rdi
	je	.L199
	cmpl	$944111087, 8(%rdi)
	jne	.L200
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L201
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L192
	cmpl	$944111087, (%rdx)
	jne	.L192
	cmpl	$2, 4(%rdx)
	movq	%rdi, %rax
	je	.L188
.L192:
	movl	$3, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	movl	$3, (%rsi)
.L189:
	xorl	%eax, %eax
.L188:
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	jmp	.L188
	.cfi_endproc
.LFE3093:
	.size	_ZN6icu_679SpoofImpl12validateThisEP13USpoofCheckerR10UErrorCode, .-_ZN6icu_679SpoofImpl12validateThisEP13USpoofCheckerR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImpl17getAllowedLocalesER10UErrorCode
	.type	_ZN6icu_679SpoofImpl17getAllowedLocalesER10UErrorCode, @function
_ZN6icu_679SpoofImpl17getAllowedLocalesER10UErrorCode:
.LFB3095:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE3095:
	.size	_ZN6icu_679SpoofImpl17getAllowedLocalesER10UErrorCode, .-_ZN6icu_679SpoofImpl17getAllowedLocalesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImpl21getAugmentedScriptSetEiRNS_9ScriptSetER10UErrorCode
	.type	_ZN6icu_679SpoofImpl21getAugmentedScriptSetEiRNS_9ScriptSetER10UErrorCode, @function
_ZN6icu_679SpoofImpl21getAugmentedScriptSetEiRNS_9ScriptSetER10UErrorCode:
.LFB3097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edi, %r14d
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	call	_ZN6icu_679ScriptSet8resetAllEv@PLT
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_679ScriptSet19setScriptExtensionsEiR10UErrorCode@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L206
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679SpoofImpl21getAugmentedScriptSetEiRNS_9ScriptSetER10UErrorCode.part.0
	.cfi_endproc
.LFE3097:
	.size	_ZN6icu_679SpoofImpl21getAugmentedScriptSetEiRNS_9ScriptSetER10UErrorCode, .-_ZN6icu_679SpoofImpl21getAugmentedScriptSetEiRNS_9ScriptSetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679SpoofImpl20getResolvedScriptSetERKNS_13UnicodeStringERNS_9ScriptSetER10UErrorCode
	.type	_ZNK6icu_679SpoofImpl20getResolvedScriptSetERKNS_13UnicodeStringERNS_9ScriptSetER10UErrorCode, @function
_ZNK6icu_679SpoofImpl20getResolvedScriptSetERKNS_13UnicodeStringERNS_9ScriptSetER10UErrorCode:
.LFB3098:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	jmp	_ZNK6icu_679SpoofImpl27getResolvedScriptSetWithoutERKNS_13UnicodeStringE11UScriptCodeRNS_9ScriptSetER10UErrorCode.constprop.0
	.cfi_endproc
.LFE3098:
	.size	_ZNK6icu_679SpoofImpl20getResolvedScriptSetERKNS_13UnicodeStringERNS_9ScriptSetER10UErrorCode, .-_ZNK6icu_679SpoofImpl20getResolvedScriptSetERKNS_13UnicodeStringERNS_9ScriptSetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679SpoofImpl27getResolvedScriptSetWithoutERKNS_13UnicodeStringE11UScriptCodeRNS_9ScriptSetER10UErrorCode
	.type	_ZNK6icu_679SpoofImpl27getResolvedScriptSetWithoutERKNS_13UnicodeStringE11UScriptCodeRNS_9ScriptSetER10UErrorCode, @function
_ZNK6icu_679SpoofImpl27getResolvedScriptSetWithoutERKNS_13UnicodeStringE11UScriptCodeRNS_9ScriptSetER10UErrorCode:
.LFB3099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$72, %rsp
	movl	%edx, -108(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_679ScriptSet6setAllEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_679ScriptSetC1Ev@PLT
	cmpl	$193, %r15d
	je	.L223
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L216:
	movswl	8(%r14), %eax
	movl	%eax, %edx
	sarl	$5, %eax
	testw	%dx, %dx
	jns	.L211
	movl	12(%r14), %eax
.L211:
	cmpl	%eax, %r15d
	jge	.L212
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN6icu_679ScriptSet8resetAllEv@PLT
	movl	%r13d, %esi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_679ScriptSet19setScriptExtensionsEiR10UErrorCode@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L229
.L212:
	movq	%r12, %rdi
	call	_ZN6icu_679ScriptSetD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_679SpoofImpl21getAugmentedScriptSetEiRNS_9ScriptSetER10UErrorCode.part.0
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L212
	movl	-108(%rbp), %esi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode@PLT
	testb	%al, %al
	jne	.L214
	movq	-104(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_679ScriptSet9intersectERKS0_@PLT
.L214:
	xorl	%eax, %eax
	cmpl	$65535, %r13d
	seta	%al
	leal	1(%r15,%rax), %r15d
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L223:
	xorl	%r13d, %r13d
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L231:
	sarl	$5, %eax
.L218:
	cmpl	%eax, %r13d
	jge	.L212
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN6icu_679ScriptSet8resetAllEv@PLT
	movq	%rbx, %rdx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_679ScriptSet19setScriptExtensionsEiR10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L212
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_679SpoofImpl21getAugmentedScriptSetEiRNS_9ScriptSetER10UErrorCode.part.0
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L212
	movq	-104(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_679ScriptSet9intersectERKS0_@PLT
	xorl	%eax, %eax
	cmpl	$65535, %r15d
	seta	%al
	leal	1(%r13,%rax), %r13d
.L209:
	movswl	8(%r14), %eax
	testw	%ax, %ax
	jns	.L231
	movl	12(%r14), %eax
	jmp	.L218
.L230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3099:
	.size	_ZNK6icu_679SpoofImpl27getResolvedScriptSetWithoutERKNS_13UnicodeStringE11UScriptCodeRNS_9ScriptSetER10UErrorCode, .-_ZNK6icu_679SpoofImpl27getResolvedScriptSetWithoutERKNS_13UnicodeStringE11UScriptCodeRNS_9ScriptSetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679SpoofImpl19getRestrictionLevelERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_679SpoofImpl19getRestrictionLevelERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_679SpoofImpl19getRestrictionLevelERKNS_13UnicodeStringER10UErrorCode:
.LFB3101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$1610612736, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6710UnicodeSet11containsAllERKNS_13UnicodeStringE@PLT
	testb	%al, %al
	je	.L232
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	js	.L234
	movswl	%dx, %eax
	sarl	$5, %eax
.L235:
	testl	%eax, %eax
	jle	.L253
	andl	$2, %edx
	je	.L236
	leal	-1(%rax), %edx
	xorl	%eax, %eax
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L262:
	cmpq	%rdx, %rax
	je	.L253
	addq	$1, %rax
.L237:
	cmpw	$127, 10(%r12,%rax,2)
	jbe	.L262
.L238:
	leaq	-128(%rbp), %rbx
	movl	$1610612736, %r15d
	movq	%rbx, %rdi
	call	_ZN6icu_679ScriptSetC1Ev@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_679SpoofImpl27getResolvedScriptSetWithoutERKNS_13UnicodeStringE11UScriptCodeRNS_9ScriptSetER10UErrorCode.constprop.0
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L263
.L241:
	movq	%rbx, %rdi
	call	_ZN6icu_679ScriptSetD1Ev@PLT
.L232:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L264
	addq	$104, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movl	12(%r12), %eax
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L236:
	leal	-1(%rax), %ecx
	movq	24(%r12), %rdx
	xorl	%eax, %eax
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L265:
	cmpq	%rcx, %rax
	je	.L253
	addq	$1, %rax
.L239:
	cmpw	$127, (%rdx,%rax,2)
	jbe	.L265
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L263:
	movq	%rbx, %rdi
	movl	$536870912, %r15d
	call	_ZNK6icu_679ScriptSet7isEmptyEv@PLT
	testb	%al, %al
	je	.L241
	leaq	-96(%rbp), %r9
	movl	$1610612736, %r15d
	movq	%r9, %rdi
	movq	%r9, -136(%rbp)
	call	_ZN6icu_679ScriptSetC1Ev@PLT
	movl	$25, %edx
	movq	%r13, %r8
	movq	%r12, %rsi
	movq	-136(%rbp), %r9
	movq	%r14, %rdi
	movq	%r9, %rcx
	call	_ZNK6icu_679SpoofImpl27getResolvedScriptSetWithoutERKNS_13UnicodeStringE11UScriptCodeRNS_9ScriptSetER10UErrorCode
	movl	0(%r13), %edx
	movq	-136(%rbp), %r9
	testl	%edx, %edx
	jle	.L266
.L242:
	movq	%r9, %rdi
	call	_ZN6icu_679ScriptSetD1Ev@PLT
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L253:
	movl	$268435456, %r15d
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L266:
	movq	%r9, %rdi
	movq	%r13, %rdx
	movl	$172, %esi
	call	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode@PLT
	movq	-136(%rbp), %r9
	testb	%al, %al
	jne	.L244
	movq	%r9, %rdi
	movq	%r13, %rdx
	movl	$105, %esi
	movq	%r9, -136(%rbp)
	call	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode@PLT
	movq	-136(%rbp), %r9
	testb	%al, %al
	jne	.L244
	movq	%r9, %rdi
	movq	%r13, %rdx
	movl	$119, %esi
	call	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode@PLT
	movq	-136(%rbp), %r9
	testb	%al, %al
	jne	.L244
	movq	%r9, %rdi
	call	_ZNK6icu_679ScriptSet7isEmptyEv@PLT
	movq	-136(%rbp), %r9
	testb	%al, %al
	je	.L245
.L246:
	movl	$1342177280, %r15d
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L244:
	movl	$805306368, %r15d
	jmp	.L242
.L245:
	movq	%r9, %rdi
	movq	%r13, %rdx
	movl	$8, %esi
	movq	%r9, -136(%rbp)
	call	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode@PLT
	movq	-136(%rbp), %r9
	testb	%al, %al
	jne	.L246
	movq	%r9, %rdi
	movq	%r13, %rdx
	movl	$14, %esi
	call	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode@PLT
	movq	-136(%rbp), %r9
	testb	%al, %al
	jne	.L246
	movq	%r9, %rdi
	movq	%r13, %rdx
	movl	$6, %esi
	call	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode@PLT
	movq	-136(%rbp), %r9
	testb	%al, %al
	jne	.L246
	movl	$1073741824, %r15d
	jmp	.L242
.L264:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3101:
	.size	_ZNK6icu_679SpoofImpl19getRestrictionLevelERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_679SpoofImpl19getRestrictionLevelERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679SpoofImpl34isIllegalCombiningDotLeadCharacterEi
	.type	_ZNK6icu_679SpoofImpl34isIllegalCombiningDotLeadCharacterEi, @function
_ZNK6icu_679SpoofImpl34isIllegalCombiningDotLeadCharacterEi:
.LFB3104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leal	-105(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L271
	movl	%esi, %ebx
	cmpl	$305, %esi
	je	.L271
	cmpl	$567, %esi
	sete	%r12b
	cmpl	$108, %esi
	sete	%al
	orb	%al, %r12b
	je	.L285
.L271:
	movl	$1, %r12d
.L267:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L286
	addq	$96, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	movq	%rdi, %r13
	movl	$27, %esi
	movl	%ebx, %edi
	call	u_hasBinaryProperty_67@PLT
	testb	%al, %al
	jne	.L271
	movq	16(%r13), %r9
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%edx, %edx
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	movq	(%r9), %rax
	movq	32(%r9), %r10
	movl	16(%rax), %esi
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L288:
	jle	.L273
	movl	%eax, %edx
	movl	%esi, %eax
	subl	%edx, %eax
	cmpl	$1, %eax
	jle	.L287
.L274:
	leal	(%rdx,%rsi), %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	movslq	%eax, %rdi
	movl	(%r10,%rdi,4), %ecx
	movl	%ecx, %r8d
	andl	$16777215, %r8d
	cmpl	%r8d, %ebx
	jge	.L288
	movl	%eax, %esi
	movl	%esi, %eax
	subl	%edx, %eax
	cmpl	$1, %eax
	jg	.L274
.L287:
	movslq	%edx, %rdi
	movl	(%r10,%rdi,4), %ecx
	movl	%ecx, %r8d
	andl	$16777215, %r8d
.L273:
	cmpl	%r8d, %ebx
	je	.L275
	leaq	-112(%rbp), %r13
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
.L276:
	movswl	-104(%rbp), %esi
	testw	%si, %si
	js	.L278
.L291:
	sarl	$5, %esi
.L279:
	movq	%r13, %rdi
	movl	$-1, %edx
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	%eax, %ebx
	je	.L280
	leal	-105(%rax), %edx
	cmpl	$1, %edx
	setbe	%r12b
	cmpl	$305, %eax
	sete	%dl
	orb	%dl, %r12b
	jne	.L280
	cmpl	$567, %eax
	sete	%r12b
	cmpl	$108, %eax
	sete	%dl
	orb	%dl, %r12b
	je	.L289
	.p2align 4,,10
	.p2align 3
.L280:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L275:
	movq	40(%r9), %rax
	shrl	$24, %ecx
	leal	1(%rcx), %r8d
	movzwl	(%rax,%rdi,2), %eax
	je	.L290
	movq	48(%r9), %rdx
	leaq	-112(%rbp), %r13
	movl	%r8d, %ecx
	movq	%r13, %rdi
	leaq	(%rdx,%rax,2), %r14
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	-104(%rbp), %esi
	testw	%si, %si
	jns	.L291
.L278:
	movl	-100(%rbp), %esi
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L290:
	leaq	-112(%rbp), %r13
	leaq	-114(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%ax, -114(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L289:
	movl	$27, %esi
	movl	%eax, %edi
	call	u_hasBinaryProperty_67@PLT
	testb	%al, %al
	setne	%r12b
	jmp	.L280
.L286:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3104:
	.size	_ZNK6icu_679SpoofImpl34isIllegalCombiningDotLeadCharacterEi, .-_ZNK6icu_679SpoofImpl34isIllegalCombiningDotLeadCharacterEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679SpoofImpl17findHiddenOverlayERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_679SpoofImpl17findHiddenOverlayERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_679SpoofImpl17findHiddenOverlayERKNS_13UnicodeStringER10UErrorCode:
.LFB3102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L312:
	sarl	$5, %eax
	cmpl	%eax, %r15d
	jge	.L301
.L313:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %ebx
	cmpl	$775, %eax
	jne	.L303
	testb	%r13b, %r13b
	jne	.L292
.L303:
	movl	%ebx, %edi
	call	u_getCombiningClass_67@PLT
	testb	%al, %al
	je	.L304
	cmpb	$-26, %al
	je	.L304
.L297:
	xorl	%eax, %eax
	cmpl	$65535, %ebx
	seta	%al
	leal	1(%rax,%r15), %r15d
.L300:
	movswl	8(%r12), %eax
	testw	%ax, %ax
	jns	.L312
	movl	12(%r12), %eax
	cmpl	%eax, %r15d
	jl	.L313
.L301:
	movl	$-1, %r15d
.L292:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_679SpoofImpl34isIllegalCombiningDotLeadCharacterEi
	movl	%eax, %r13d
	jmp	.L297
	.cfi_endproc
.LFE3102:
	.size	_ZNK6icu_679SpoofImpl17findHiddenOverlayERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_679SpoofImpl17findHiddenOverlayERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImpl7ScanHexEPKDsiiR10UErrorCode
	.type	_ZN6icu_679SpoofImpl7ScanHexEPKDsiiR10UErrorCode, @function
_ZN6icu_679SpoofImpl7ScanHexEPKDsiiR10UErrorCode:
.LFB3105:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L314
	cmpl	%edx, %esi
	jge	.L314
	subl	$1, %edx
	movslq	%esi, %rax
	subl	%esi, %edx
	leaq	(%rdi,%rax,2), %r8
	addq	%rax, %rdx
	xorl	%eax, %eax
	leaq	2(%rdi,%rdx,2), %rdi
	.p2align 4,,10
	.p2align 3
.L318:
	movzwl	(%r8), %esi
	leal	-48(%rsi), %edx
	cmpl	$9, %edx
	jle	.L317
	leal	-55(%rsi), %edx
	cmpl	$15, %edx
	jle	.L317
	leal	-87(%rsi), %edx
.L317:
	sall	$4, %eax
	addq	$2, %r8
	addl	%edx, %eax
	cmpq	%rdi, %r8
	jne	.L318
	cmpl	$1114111, %eax
	jbe	.L314
	movl	$9, (%rcx)
	xorl	%eax, %eax
.L314:
	ret
	.cfi_endproc
.LFE3105:
	.size	_ZN6icu_679SpoofImpl7ScanHexEPKDsiiR10UErrorCode, .-_ZN6icu_679SpoofImpl7ScanHexEPKDsiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711CheckResult19asUSpoofCheckResultEv
	.type	_ZN6icu_6711CheckResult19asUSpoofCheckResultEv, @function
_ZN6icu_6711CheckResult19asUSpoofCheckResultEv:
.LFB3112:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE3112:
	.size	_ZN6icu_6711CheckResult19asUSpoofCheckResultEv, .-_ZN6icu_6711CheckResult19asUSpoofCheckResultEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711CheckResult12validateThisEPK17USpoofCheckResultR10UErrorCode
	.type	_ZN6icu_6711CheckResult12validateThisEPK17USpoofCheckResultR10UErrorCode, @function
_ZN6icu_6711CheckResult12validateThisEPK17USpoofCheckResultR10UErrorCode:
.LFB4096:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movq	%rdi, %rax
	testl	%edx, %edx
	jg	.L327
	testq	%rdi, %rdi
	je	.L328
	cmpl	$657779934, 8(%rdi)
	jne	.L329
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	movl	$3, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	movl	$1, (%rsi)
	ret
	.cfi_endproc
.LFE4096:
	.size	_ZN6icu_6711CheckResult12validateThisEPK17USpoofCheckResultR10UErrorCode, .-_ZN6icu_6711CheckResult12validateThisEPK17USpoofCheckResultR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711CheckResult12validateThisEP17USpoofCheckResultR10UErrorCode
	.type	_ZN6icu_6711CheckResult12validateThisEP17USpoofCheckResultR10UErrorCode, @function
_ZN6icu_6711CheckResult12validateThisEP17USpoofCheckResultR10UErrorCode:
.LFB3114:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movq	%rdi, %rax
	testl	%edx, %edx
	jg	.L333
	testq	%rdi, %rdi
	je	.L334
	cmpl	$657779934, 8(%rdi)
	jne	.L335
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	movl	$3, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	movl	$1, (%rsi)
	ret
	.cfi_endproc
.LFE3114:
	.size	_ZN6icu_6711CheckResult12validateThisEP17USpoofCheckResultR10UErrorCode, .-_ZN6icu_6711CheckResult12validateThisEP17USpoofCheckResultR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711CheckResult17toCombinedBitmaskEi
	.type	_ZN6icu_6711CheckResult17toCombinedBitmaskEi, @function
_ZN6icu_6711CheckResult17toCombinedBitmaskEi:
.LFB3116:
	.cfi_startproc
	endbr64
	andl	$1073741824, %esi
	movl	12(%rdi), %eax
	je	.L336
	movl	216(%rdi), %edx
	movl	%eax, %ecx
	orl	%edx, %ecx
	cmpl	$-1, %edx
	cmovne	%ecx, %eax
.L336:
	ret
	.cfi_endproc
.LFE3116:
	.size	_ZN6icu_6711CheckResult17toCombinedBitmaskEi, .-_ZN6icu_6711CheckResult17toCombinedBitmaskEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679SpoofData19validateDataVersionER10UErrorCode
	.type	_ZNK6icu_679SpoofData19validateDataVersionER10UErrorCode, @function
_ZNK6icu_679SpoofData19validateDataVersionER10UErrorCode:
.LFB3121:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L344
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L344
	cmpl	$944111087, (%rdx)
	jne	.L344
	cmpl	$2, 4(%rdx)
	movl	$1, %eax
	je	.L343
	.p2align 4,,10
	.p2align 3
.L344:
	movl	$3, (%rsi)
	xorl	%eax, %eax
.L343:
	ret
	.cfi_endproc
.LFE3121:
	.size	_ZNK6icu_679SpoofData19validateDataVersionER10UErrorCode, .-_ZNK6icu_679SpoofData19validateDataVersionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofData10getDefaultER10UErrorCode
	.type	_ZN6icu_679SpoofData10getDefaultER10UErrorCode, @function
_ZN6icu_679SpoofData10getDefaultER10UErrorCode:
.LFB3125:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	testl	%edx, %edx
	jle	.L366
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L367
.L352:
	movl	4+_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L353
	movl	%eax, (%rbx)
.L351:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	leaq	_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L352
	movq	%rbx, %rdi
	call	_ZN6icu_67L22uspoof_loadDefaultDataER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L21gSpoofInitDefaultOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L353:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L351
	movq	_ZN6icu_67L17gDefaultSpoofDataE(%rip), %rax
	lock addl	$1, 28(%rax)
	movq	_ZN6icu_67L17gDefaultSpoofDataE(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3125:
	.size	_ZN6icu_679SpoofData10getDefaultER10UErrorCode, .-_ZN6icu_679SpoofData10getDefaultER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofDataC2EP11UDataMemoryR10UErrorCode
	.type	_ZN6icu_679SpoofDataC2EP11UDataMemoryR10UErrorCode, @function
_ZN6icu_679SpoofDataC2EP11UDataMemoryR10UErrorCode:
.LFB3127:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	pxor	%xmm0, %xmm0
	movb	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movl	$0, 24(%rdi)
	movl	$1, 28(%rdi)
	mfence
	movq	$0, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L389
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, 16(%rdi)
	movq	%rsi, %rdi
	call	udata_getMemory_67@PLT
	movl	(%r12), %edx
	movq	%rax, (%rbx)
	testl	%edx, %edx
	jg	.L371
	testq	%rax, %rax
	je	.L371
	cmpl	$944111087, (%rax)
	jne	.L371
	cmpl	$2, 4(%rax)
	pxor	%xmm0, %xmm0
	jne	.L371
	movups	%xmm0, 32(%rbx)
	movslq	12(%rax), %rdx
	movq	$0, 48(%rbx)
	testl	%edx, %edx
	je	.L374
	addq	%rax, %rdx
	movq	%rdx, 32(%rbx)
.L374:
	movslq	20(%rax), %rdx
	testl	%edx, %edx
	je	.L375
	addq	%rax, %rdx
	movq	%rdx, 40(%rbx)
.L375:
	movslq	28(%rax), %rdx
	testl	%edx, %edx
	je	.L368
	addq	%rdx, %rax
	movq	%rax, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movl	$3, (%r12)
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
.L368:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3127:
	.size	_ZN6icu_679SpoofDataC2EP11UDataMemoryR10UErrorCode, .-_ZN6icu_679SpoofDataC2EP11UDataMemoryR10UErrorCode
	.globl	_ZN6icu_679SpoofDataC1EP11UDataMemoryR10UErrorCode
	.set	_ZN6icu_679SpoofDataC1EP11UDataMemoryR10UErrorCode,_ZN6icu_679SpoofDataC2EP11UDataMemoryR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofDataC2EPKviR10UErrorCode
	.type	_ZN6icu_679SpoofDataC2EPKviR10UErrorCode, @function
_ZN6icu_679SpoofDataC2EPKviR10UErrorCode:
.LFB3130:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	pxor	%xmm0, %xmm0
	movb	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movl	$0, 24(%rdi)
	movl	$1, 28(%rdi)
	mfence
	movq	$0, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L390
	movslq	%edx, %rax
	cmpq	$95, %rax
	jbe	.L396
	testq	%rsi, %rsi
	je	.L410
	movq	%rsi, (%rdi)
	cmpl	%edx, 8(%rsi)
	jg	.L396
	cmpl	$944111087, (%rsi)
	jne	.L396
	cmpl	$2, 4(%rsi)
	jne	.L396
	movslq	12(%rsi), %rax
	testl	%eax, %eax
	je	.L398
	addq	%rsi, %rax
	movq	%rax, 32(%rdi)
.L398:
	movslq	20(%rsi), %rax
	testl	%eax, %eax
	je	.L399
	addq	%rsi, %rax
	movq	%rax, 40(%rdi)
.L399:
	movslq	28(%rsi), %rax
	testl	%eax, %eax
	je	.L390
	addq	%rax, %rsi
	movq	%rsi, 48(%rdi)
.L390:
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	movl	$3, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	movl	$1, (%rcx)
	ret
	.cfi_endproc
.LFE3130:
	.size	_ZN6icu_679SpoofDataC2EPKviR10UErrorCode, .-_ZN6icu_679SpoofDataC2EPKviR10UErrorCode
	.globl	_ZN6icu_679SpoofDataC1EPKviR10UErrorCode
	.set	_ZN6icu_679SpoofDataC1EPKviR10UErrorCode,_ZN6icu_679SpoofDataC2EPKviR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofDataC2ER10UErrorCode
	.type	_ZN6icu_679SpoofDataC2ER10UErrorCode, @function
_ZN6icu_679SpoofDataC2ER10UErrorCode:
.LFB3133:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	pxor	%xmm0, %xmm0
	movb	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movl	$0, 24(%rdi)
	movl	$1, 28(%rdi)
	mfence
	movq	$0, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L418
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movb	$1, 8(%rdi)
	movl	$96, %edi
	call	uprv_malloc_67@PLT
	movl	$96, 24(%rbx)
	pxor	%xmm0, %xmm0
	testq	%rax, %rax
	movq	%rax, (%rbx)
	movq	%rax, %rdx
	je	.L419
	leaq	16(%rax), %rdi
	movq	$0, 8(%rax)
	andq	$-8, %rdi
	movq	$0, 88(%rax)
	subl	%edi, %eax
	leal	96(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movq	$0, 48(%rbx)
	movabsq	$9534045679, %rax
	movq	%rax, (%rdx)
	movups	%xmm0, 32(%rbx)
.L411:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L419:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L411
	.cfi_endproc
.LFE3133:
	.size	_ZN6icu_679SpoofDataC2ER10UErrorCode, .-_ZN6icu_679SpoofDataC2ER10UErrorCode
	.globl	_ZN6icu_679SpoofDataC1ER10UErrorCode
	.set	_ZN6icu_679SpoofDataC1ER10UErrorCode,_ZN6icu_679SpoofDataC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofData5resetEv
	.type	_ZN6icu_679SpoofData5resetEv, @function
_ZN6icu_679SpoofData5resetEv:
.LFB3135:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	pxor	%xmm0, %xmm0
	movb	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movl	$0, 24(%rdi)
	movl	$1, 28(%rdi)
	mfence
	movq	$0, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE3135:
	.size	_ZN6icu_679SpoofData5resetEv, .-_ZN6icu_679SpoofData5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofData8initPtrsER10UErrorCode
	.type	_ZN6icu_679SpoofData8initPtrsER10UErrorCode, @function
_ZN6icu_679SpoofData8initPtrsER10UErrorCode:
.LFB3136:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L421
	movq	(%rdi), %rax
	movslq	12(%rax), %rdx
	testl	%edx, %edx
	je	.L424
	addq	%rax, %rdx
	movq	%rdx, 32(%rdi)
.L424:
	movslq	20(%rax), %rdx
	testl	%edx, %edx
	je	.L425
	addq	%rax, %rdx
	movq	%rdx, 40(%rdi)
.L425:
	movslq	28(%rax), %rdx
	testl	%edx, %edx
	je	.L421
	addq	%rdx, %rax
	movq	%rax, 48(%rdi)
.L421:
	ret
	.cfi_endproc
.LFE3136:
	.size	_ZN6icu_679SpoofData8initPtrsER10UErrorCode, .-_ZN6icu_679SpoofData8initPtrsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofDataD2Ev
	.type	_ZN6icu_679SpoofDataD2Ev, @function
_ZN6icu_679SpoofDataD2Ev:
.LFB3138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 8(%rdi)
	jne	.L440
.L437:
	movq	16(%rbx), %rdi
	movq	$0, (%rbx)
	testq	%rdi, %rdi
	je	.L436
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	udata_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L440:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L437
	.cfi_endproc
.LFE3138:
	.size	_ZN6icu_679SpoofDataD2Ev, .-_ZN6icu_679SpoofDataD2Ev
	.globl	_ZN6icu_679SpoofDataD1Ev
	.set	_ZN6icu_679SpoofDataD1Ev,_ZN6icu_679SpoofDataD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofData15removeReferenceEv
	.type	_ZN6icu_679SpoofData15removeReferenceEv, @function
_ZN6icu_679SpoofData15removeReferenceEv:
.LFB3140:
	.cfi_startproc
	endbr64
	lock subl	$1, 28(%rdi)
	je	.L451
	ret
	.p2align 4,,10
	.p2align 3
.L451:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 8(%rdi)
	jne	.L452
.L443:
	movq	16(%r12), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L444
	call	udata_close_67@PLT
.L444:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L443
	.cfi_endproc
.LFE3140:
	.size	_ZN6icu_679SpoofData15removeReferenceEv, .-_ZN6icu_679SpoofData15removeReferenceEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofData12addReferenceEv
	.type	_ZN6icu_679SpoofData12addReferenceEv, @function
_ZN6icu_679SpoofData12addReferenceEv:
.LFB3141:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	lock addl	$1, 28(%rdi)
	ret
	.cfi_endproc
.LFE3141:
	.size	_ZN6icu_679SpoofData12addReferenceEv, .-_ZN6icu_679SpoofData12addReferenceEv
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB3:
	.text
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofData12reserveSpaceEiR10UErrorCode
	.type	_ZN6icu_679SpoofData12reserveSpaceEiR10UErrorCode, @function
_ZN6icu_679SpoofData12reserveSpaceEiR10UErrorCode:
.LFB3142:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L461
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpb	$0, 8(%rdi)
	movq	%rdi, %rbx
	je	.L471
	movl	24(%rdi), %r14d
	leal	15(%rsi), %r12d
	movq	%rdx, %r13
	andl	$-16, %r12d
	leal	(%r12,%r14), %esi
	movl	%esi, 24(%rdi)
	movq	(%rdi), %rdi
	call	uprv_realloc_67@PLT
	movl	24(%rbx), %edx
	xorl	%esi, %esi
	movq	%rax, (%rbx)
	leaq	(%rax,%r14), %rdi
	movl	%edx, 8(%rax)
	movslq	%r12d, %rdx
	call	memset@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movl	0(%r13), %eax
	testl	%eax, %eax
	movq	(%rbx), %rax
	jg	.L458
	movslq	12(%rax), %rdx
	testl	%edx, %edx
	je	.L459
	addq	%rax, %rdx
	movq	%rdx, 32(%rbx)
.L459:
	movslq	20(%rax), %rdx
	testl	%edx, %edx
	je	.L460
	addq	%rax, %rdx
	movq	%rdx, 40(%rbx)
.L460:
	movslq	28(%rax), %rdx
	testl	%edx, %edx
	je	.L458
	addq	%rax, %rdx
	movq	%rdx, 48(%rbx)
.L458:
	popq	%rbx
	addq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_679SpoofData12reserveSpaceEiR10UErrorCode.cold, @function
_ZN6icu_679SpoofData12reserveSpaceEiR10UErrorCode.cold:
.LFSB3142:
.L471:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	call	abort@PLT
	.cfi_endproc
.LFE3142:
	.text
	.size	_ZN6icu_679SpoofData12reserveSpaceEiR10UErrorCode, .-_ZN6icu_679SpoofData12reserveSpaceEiR10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_679SpoofData12reserveSpaceEiR10UErrorCode.cold, .-_ZN6icu_679SpoofData12reserveSpaceEiR10UErrorCode.cold
.LCOLDE3:
	.text
.LHOTE3:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679SpoofData9serializeEPviR10UErrorCode
	.type	_ZNK6icu_679SpoofData9serializeEPviR10UErrorCode, @function
_ZNK6icu_679SpoofData9serializeEPviR10UErrorCode:
.LFB3143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%r8), %rsi
	movl	8(%rsi), %r12d
	cmpl	%edx, %r12d
	jle	.L477
	movl	$15, (%rcx)
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	movslq	%r12d, %rdx
	call	memcpy@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3143:
	.size	_ZNK6icu_679SpoofData9serializeEPviR10UErrorCode, .-_ZNK6icu_679SpoofData9serializeEPviR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679SpoofData4sizeEv
	.type	_ZNK6icu_679SpoofData4sizeEv, @function
_ZNK6icu_679SpoofData4sizeEv:
.LFB3144:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rax), %eax
	ret
	.cfi_endproc
.LFE3144:
	.size	_ZNK6icu_679SpoofData4sizeEv, .-_ZNK6icu_679SpoofData4sizeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679SpoofData16confusableLookupEiRNS_13UnicodeStringE
	.type	_ZNK6icu_679SpoofData16confusableLookupEiRNS_13UnicodeStringE, @function
_ZNK6icu_679SpoofData16confusableLookupEiRNS_13UnicodeStringE:
.LFB3145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	xorl	%ecx, %ecx
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%r11), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r11), %rax
	movl	16(%rax), %r8d
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L493:
	jle	.L483
	movl	%eax, %ecx
	movl	%r8d, %eax
	subl	%ecx, %eax
	cmpl	$1, %eax
	jle	.L492
.L484:
	leal	(%rcx,%r8), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %r9
	movl	(%rbx,%r9,4), %edx
	movl	%edx, %r10d
	andl	$16777215, %r10d
	cmpl	%r10d, %esi
	jge	.L493
	movl	%eax, %r8d
	movl	%r8d, %eax
	subl	%ecx, %eax
	cmpl	$1, %eax
	jg	.L484
.L492:
	movslq	%ecx, %r9
	movl	(%rbx,%r9,4), %edx
	movl	%edx, %r10d
	andl	$16777215, %r10d
.L483:
	cmpl	%r10d, %esi
	je	.L485
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	$1, %r12d
.L481:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L494
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L485:
	.cfi_restore_state
	movq	40(%r11), %rax
	shrl	$24, %edx
	leal	1(%rdx), %r12d
	movzwl	(%rax,%r9,2), %eax
	je	.L495
	movq	48(%r11), %rdx
	movl	%r12d, %ecx
	leaq	(%rdx,%rax,2), %rbx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L495:
	leaq	-26(%rbp), %rsi
	movl	$1, %ecx
	movw	%ax, -26(%rbp)
	movl	$1, %r12d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L481
.L494:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3145:
	.size	_ZNK6icu_679SpoofData16confusableLookupEiRNS_13UnicodeStringE, .-_ZNK6icu_679SpoofData16confusableLookupEiRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679SpoofData6lengthEv
	.type	_ZNK6icu_679SpoofData6lengthEv, @function
_ZNK6icu_679SpoofData6lengthEv:
.LFB3146:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	16(%rax), %eax
	ret
	.cfi_endproc
.LFE3146:
	.size	_ZNK6icu_679SpoofData6lengthEv, .-_ZNK6icu_679SpoofData6lengthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679SpoofData11codePointAtEi
	.type	_ZNK6icu_679SpoofData11codePointAtEi, @function
_ZNK6icu_679SpoofData11codePointAtEi:
.LFB3147:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %eax
	andl	$16777215, %eax
	ret
	.cfi_endproc
.LFE3147:
	.size	_ZNK6icu_679SpoofData11codePointAtEi, .-_ZNK6icu_679SpoofData11codePointAtEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679SpoofData13appendValueToEiRNS_13UnicodeStringE
	.type	_ZNK6icu_679SpoofData13appendValueToEiRNS_13UnicodeStringE, @function
_ZNK6icu_679SpoofData13appendValueToEiRNS_13UnicodeStringE:
.LFB3148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movzbl	3(%rax,%rsi,4), %edx
	movq	40(%rdi), %rax
	leal	1(%rdx), %r12d
	movzwl	(%rax,%rsi,2), %eax
	testl	%edx, %edx
	je	.L503
	movq	48(%rdi), %rdx
	movl	%r12d, %ecx
	movq	%r8, %rdi
	leaq	(%rdx,%rax,2), %rbx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L498:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L504
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore_state
	leaq	-26(%rbp), %rsi
	movl	$1, %ecx
	movq	%r8, %rdi
	movw	%ax, -26(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L498
.L504:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3148:
	.size	_ZNK6icu_679SpoofData13appendValueToEiRNS_13UnicodeStringE, .-_ZNK6icu_679SpoofData13appendValueToEiRNS_13UnicodeStringE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"uspoof_swap(): data format %02x.%02x.%02x.%02x (format version %02x %02x %02x %02x) is not recognized\n"
	.align 8
.LC5:
	.string	"uspoof_swap(): Spoof Data header is invalid.\n"
	.align 8
.LC6:
	.string	"uspoof_swap(): too few bytes (%d after ICU Data header) for spoof data.\n"
	.text
	.p2align 4
	.globl	uspoof_swap_67
	.type	uspoof_swap_67, @function
uspoof_swap_67:
.LFB3149:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	je	.L516
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L505
	testq	%rdi, %rdi
	movq	%rdi, %r13
	sete	%dil
	testq	%rsi, %rsi
	sete	%al
	orb	%al, %dil
	jne	.L507
	cmpl	$-1, %edx
	jl	.L507
	testl	%edx, %edx
	jle	.L508
	testq	%rcx, %rcx
	je	.L507
.L508:
	cmpw	$26179, 12(%rsi)
	jne	.L509
	cmpw	$8309, 14(%rsi)
	jne	.L509
	cmpw	$2, 16(%rsi)
	jne	.L509
	cmpw	$0, 18(%rsi)
	jne	.L509
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%rsi, -56(%rbp)
	movq	%rcx, -72(%rbp)
	movl	%edx, -60(%rbp)
	call	udata_swapDataHeader_67@PLT
	movq	-56(%rbp), %rsi
	movslq	%eax, %r15
	leaq	(%rsi,%r15), %r12
	movl	(%r12), %edi
	call	*16(%r13)
	cmpl	$944111087, %eax
	jne	.L511
	movl	8(%r12), %edi
	call	*16(%r13)
	cmpl	$95, %eax
	jbe	.L511
	movl	8(%r12), %edi
	call	*16(%r13)
	movl	-60(%rbp), %edx
	movq	-72(%rbp), %rcx
	leal	(%r15,%rax), %r9d
	cmpl	$-1, %edx
	je	.L505
	cmpl	%r9d, %edx
	jl	.L532
	leaq	(%rcx,%r15), %r14
	cmpq	%r14, %r12
	je	.L514
	movslq	%eax, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	%r9d, -56(%rbp)
	call	memset@PLT
	movl	-56(%rbp), %r9d
.L514:
	movl	%r9d, -56(%rbp)
	movl	12(%r12), %edi
	call	*16(%r13)
	movl	16(%r12), %edi
	movslq	%eax, %r15
	call	*16(%r13)
	leaq	(%r14,%r15), %rcx
	leaq	(%r12,%r15), %rsi
	movq	%rbx, %r8
	leal	0(,%rax,4), %edx
	movq	%r13, %rdi
	call	*56(%r13)
	movl	20(%r12), %edi
	call	*16(%r13)
	movl	24(%r12), %edi
	movslq	%eax, %r15
	call	*16(%r13)
	leaq	(%r14,%r15), %rcx
	leaq	(%r12,%r15), %rsi
	movq	%rbx, %r8
	leal	(%rax,%rax), %edx
	movq	%r13, %rdi
	call	*48(%r13)
	movl	28(%r12), %edi
	call	*16(%r13)
	movl	32(%r12), %edi
	movslq	%eax, %r15
	call	*16(%r13)
	leaq	(%r14,%r15), %rcx
	leaq	(%r12,%r15), %rsi
	movq	%rbx, %r8
	leal	(%rax,%rax), %edx
	movq	%r13, %rdi
	call	*48(%r13)
	movl	(%r12), %edi
	call	*16(%r13)
	movq	%r14, %rdi
	movl	%eax, %esi
	call	*40(%r13)
	cmpq	%r14, %r12
	movl	-56(%rbp), %r9d
	je	.L515
	movl	4(%r12), %eax
	movl	%eax, 4(%r14)
.L515:
	movl	%r9d, -56(%rbp)
	leaq	8(%r14), %rcx
	leaq	8(%r12), %rsi
	movq	%rbx, %r8
	movl	$88, %edx
	movq	%r13, %rdi
	call	*56(%r13)
	movl	-56(%rbp), %r9d
.L505:
	leaq	-40(%rbp), %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%r9d, %r9d
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L509:
	movzbl	19(%rsi), %eax
	movzbl	13(%rsi), %ecx
	movq	%r13, %rdi
	movzbl	12(%rsi), %edx
	pushq	%rax
	movzbl	18(%rsi), %eax
	pushq	%rax
	movzbl	17(%rsi), %eax
	pushq	%rax
	movzbl	16(%rsi), %eax
	pushq	%rax
	movzbl	15(%rsi), %r9d
	xorl	%eax, %eax
	movzbl	14(%rsi), %r8d
	leaq	.LC4(%rip), %rsi
	call	udata_printError_67@PLT
	movl	$16, (%rbx)
	addq	$32, %rsp
	xorl	%r9d, %r9d
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%r9d, %r9d
	movl	%r9d, %eax
	ret
.L511:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	.LC5(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	movl	$16, (%rbx)
	xorl	%r9d, %r9d
	jmp	.L505
.L532:
	movl	%eax, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	movl	$8, (%rbx)
	xorl	%r9d, %r9d
	jmp	.L505
	.cfi_endproc
.LFE3149:
	.size	uspoof_swap_67, .-uspoof_swap_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImplC2ERKS0_R10UErrorCode
	.type	_ZN6icu_679SpoofImplC2ERKS0_R10UErrorCode, @function
_ZN6icu_679SpoofImplC2ERKS0_R10UErrorCode:
.LFB3085:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_679SpoofImplE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rdi)
	movq	%rax, (%rdi)
	movabsq	$281471625854447, %rax
	movq	%rax, 8(%rdi)
	movl	(%rdx), %eax
	movups	%xmm0, 16(%rdi)
	testl	%eax, %eax
	jg	.L546
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	12(%rsi), %eax
	movl	%eax, 12(%rdi)
	movq	16(%rsi), %rax
	testq	%rax, %rax
	je	.L535
	lock addl	$1, 28(%rax)
	movq	%rax, 16(%rdi)
.L535:
	movq	24(%r12), %rdi
	call	_ZNK6icu_6710UnicodeSet5cloneEv@PLT
	movq	32(%r12), %rdi
	movq	%rax, 24(%rbx)
	call	uprv_strdup_67@PLT
	cmpq	$0, 24(%rbx)
	movq	%rax, 32(%rbx)
	je	.L538
	testq	%rax, %rax
	je	.L538
.L536:
	movl	40(%r12), %eax
	movl	%eax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L538:
	.cfi_restore_state
	movl	$7, 0(%r13)
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE3085:
	.size	_ZN6icu_679SpoofImplC2ERKS0_R10UErrorCode, .-_ZN6icu_679SpoofImplC2ERKS0_R10UErrorCode
	.globl	_ZN6icu_679SpoofImplC1ERKS0_R10UErrorCode
	.set	_ZN6icu_679SpoofImplC1ERKS0_R10UErrorCode,_ZN6icu_679SpoofImplC2ERKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImplD2Ev
	.type	_ZN6icu_679SpoofImplD2Ev, @function
_ZN6icu_679SpoofImplD2Ev:
.LFB3088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679SpoofImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L551
	lock subl	$1, 28(%r13)
	je	.L563
.L551:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L555
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L555:
	movq	32(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L563:
	.cfi_restore_state
	cmpb	$0, 8(%r13)
	jne	.L564
.L553:
	movq	16(%r13), %rdi
	movq	$0, 0(%r13)
	testq	%rdi, %rdi
	je	.L554
	call	udata_close_67@PLT
.L554:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L564:
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L553
	.cfi_endproc
.LFE3088:
	.size	_ZN6icu_679SpoofImplD2Ev, .-_ZN6icu_679SpoofImplD2Ev
	.globl	_ZN6icu_679SpoofImplD1Ev
	.set	_ZN6icu_679SpoofImplD1Ev,_ZN6icu_679SpoofImplD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImplD0Ev
	.type	_ZN6icu_679SpoofImplD0Ev, @function
_ZN6icu_679SpoofImplD0Ev:
.LFB3090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679SpoofImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L567
	lock subl	$1, 28(%r13)
	je	.L579
.L567:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L571
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L571:
	movq	32(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_restore_state
	cmpb	$0, 8(%r13)
	jne	.L580
.L569:
	movq	16(%r13), %rdi
	movq	$0, 0(%r13)
	testq	%rdi, %rdi
	je	.L570
	call	udata_close_67@PLT
.L570:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L580:
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L569
	.cfi_endproc
.LFE3090:
	.size	_ZN6icu_679SpoofImplD0Ev, .-_ZN6icu_679SpoofImplD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImpl14addScriptCharsEPKcPNS_10UnicodeSetER10UErrorCode
	.type	_ZN6icu_679SpoofImpl14addScriptCharsEPKcPNS_10UnicodeSetER10UErrorCode, @function
_ZN6icu_679SpoofImpl14addScriptCharsEPKcPNS_10UnicodeSetER10UErrorCode:
.LFB3096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$30, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-384(%rbp), %rbx
	movq	%rbx, %rsi
	subq	$344, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uscript_getCode_67@PLT
	movl	%eax, %r15d
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L581
	cmpl	$-127, %eax
	je	.L590
	leaq	-256(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	testl	%r15d, %r15d
	jle	.L584
	leal	-1(%r15), %eax
	leaq	-380(%rbp,%rax,4), %r15
	.p2align 4,,10
	.p2align 3
.L585:
	movl	(%rbx), %edx
	movq	%r12, %rcx
	movl	$4106, %esi
	movq	%r13, %rdi
	addq	$4, %rbx
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	cmpq	%r15, %rbx
	jne	.L585
.L584:
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
.L581:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L591
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L590:
	.cfi_restore_state
	movl	$1, (%r12)
	jmp	.L581
.L591:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3096:
	.size	_ZN6icu_679SpoofImpl14addScriptCharsEPKcPNS_10UnicodeSetER10UErrorCode, .-_ZN6icu_679SpoofImpl14addScriptCharsEPKcPNS_10UnicodeSetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711CheckResultD2Ev
	.type	_ZN6icu_6711CheckResultD2Ev, @function
_ZN6icu_6711CheckResultD2Ev:
.LFB3118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711CheckResultE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3118:
	.size	_ZN6icu_6711CheckResultD2Ev, .-_ZN6icu_6711CheckResultD2Ev
	.globl	_ZN6icu_6711CheckResultD1Ev
	.set	_ZN6icu_6711CheckResultD1Ev,_ZN6icu_6711CheckResultD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711CheckResultD0Ev
	.type	_ZN6icu_6711CheckResultD0Ev, @function
_ZN6icu_6711CheckResultD0Ev:
.LFB3120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711CheckResultE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3120:
	.size	_ZN6icu_6711CheckResultD0Ev, .-_ZN6icu_6711CheckResultD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679SpoofImpl17setAllowedLocalesEPKcR10UErrorCode
	.type	_ZN6icu_679SpoofImpl17setAllowedLocalesEPKcR10UErrorCode, @function
_ZN6icu_679SpoofImpl17setAllowedLocalesEPKcR10UErrorCode:
.LFB3094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-464(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$616, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -640(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -648(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r13, %rdi
	call	strlen@PLT
	movl	$0, -620(%rbp)
	addq	%r13, %rax
	movq	%rax, -600(%rbp)
	leaq	-592(%rbp), %rax
	movq	%rax, -616(%rbp)
	.p2align 4,,10
	.p2align 3
.L609:
	movl	$44, %esi
	movq	%r13, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	cmove	-600(%rbp), %rbx
	cmpb	$32, 0(%r13)
	jne	.L598
	.p2align 4,,10
	.p2align 3
.L599:
	addq	$1, %r13
	cmpb	$32, 0(%r13)
	je	.L599
.L598:
	leaq	-1(%rbx), %rsi
	cmpq	%r13, %rsi
	ja	.L602
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L633:
	subq	$1, %rsi
	cmpq	%r13, %rsi
	je	.L600
.L602:
	cmpb	$32, (%rsi)
	je	.L633
	cmpq	%rsi, %r13
	jnb	.L600
	addq	$1, %rsi
	movq	%r13, %rdi
	subq	%r13, %rsi
	call	uprv_strndup_67@PLT
	movq	-616(%rbp), %rsi
	movq	%r12, %rcx
	movl	$30, %edx
	movq	%rax, %rdi
	addl	$1, -620(%rbp)
	movq	%rax, -608(%rbp)
	call	uscript_getCode_67@PLT
	movl	%eax, %edx
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L607
	cmpl	$-127, %eax
	je	.L634
	leaq	-256(%rbp), %r13
	movl	%edx, -632(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	-632(%rbp), %edx
	testl	%edx, %edx
	jle	.L605
	movq	-616(%rbp), %r15
	leal	-1(%rdx), %eax
	movq	%rbx, -632(%rbp)
	leaq	-588(%rbp,%rax,4), %rax
	movq	%r15, %rbx
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L606:
	movl	(%rbx), %edx
	movq	%r12, %rcx
	movl	$4106, %esi
	movq	%r13, %rdi
	addq	$4, %rbx
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	cmpq	%rbx, %r15
	jne	.L606
	movq	-632(%rbp), %rbx
.L605:
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
.L607:
	movq	-608(%rbp), %rdi
	call	uprv_free_67@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L608
	leaq	1(%rbx), %r13
	cmpq	%r13, -600(%rbp)
	ja	.L609
.L608:
	leaq	-256(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r12, %rcx
	xorl	%edx, %edx
	movl	$4106, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%r13, %rdi
	movl	$4106, %esi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L635
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L634:
	movl	$1, (%r12)
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L600:
	movl	-620(%rbp), %edx
	testl	%edx, %edx
	jne	.L608
	movq	-640(%rbp), %rbx
	movq	32(%rbx), %rdi
	call	uprv_free_67@PLT
	leaq	.LC2(%rip), %rdi
	call	uprv_strdup_67@PLT
	movl	$200, %edi
	movq	%rax, 32(%rbx)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L612
	xorl	%esi, %esi
	movl	$1114111, %edx
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
	movq	-640(%rbp), %r15
	cmpq	$0, 32(%r15)
	je	.L612
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L613
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L613:
	movq	-640(%rbp), %rax
	andl	$-65, 12(%rax)
	movq	%rbx, 24(%rax)
.L611:
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
.L596:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L636
	addq	$616, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L635:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNK6icu_6710UnicodeSet5cloneEv@PLT
	movq	-648(%rbp), %rdi
	movq	%rax, %rbx
	call	uprv_strdup_67@PLT
	movq	%rax, %r15
	testq	%rbx, %rbx
	je	.L620
	testq	%rax, %rax
	je	.L620
	movq	-640(%rbp), %r12
	movq	32(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r15, 32(%r12)
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L617
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L617:
	movq	-640(%rbp), %rax
	movq	%r13, %rdi
	orl	$64, 12(%rax)
	movq	%rbx, 24(%rax)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	jmp	.L596
.L620:
	movl	$7, (%r12)
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	jmp	.L611
.L612:
	movl	$7, (%r12)
	jmp	.L611
.L636:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3094:
	.size	_ZN6icu_679SpoofImpl17setAllowedLocalesEPKcR10UErrorCode, .-_ZN6icu_679SpoofImpl17setAllowedLocalesEPKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679SpoofImpl11getNumericsERKNS_13UnicodeStringERNS_10UnicodeSetER10UErrorCode
	.type	_ZNK6icu_679SpoofImpl11getNumericsERKNS_13UnicodeStringERNS_10UnicodeSetER10UErrorCode, @function
_ZNK6icu_679SpoofImpl11getNumericsERKNS_13UnicodeStringERNS_10UnicodeSetER10UErrorCode:
.LFB3100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L647:
	sarl	$5, %eax
	cmpl	%eax, %r13d
	jge	.L637
.L648:
	movq	%r14, %rdi
	movl	%r13d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	movl	%eax, %ebx
	call	u_charType_67@PLT
	cmpb	$9, %al
	je	.L646
.L641:
	xorl	%eax, %eax
	cmpl	$65535, %ebx
	seta	%al
	leal	1(%r13,%rax), %r13d
.L643:
	movswl	8(%r14), %eax
	testw	%ax, %ax
	jns	.L647
	movl	12(%r14), %eax
	cmpl	%eax, %r13d
	jl	.L648
.L637:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L646:
	.cfi_restore_state
	movl	%ebx, %edi
	call	u_getNumericValue_67@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	cvttsd2sil	%xmm0, %eax
	subl	%eax, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	jmp	.L641
	.cfi_endproc
.LFE3100:
	.size	_ZNK6icu_679SpoofImpl11getNumericsERKNS_13UnicodeStringERNS_10UnicodeSetER10UErrorCode, .-_ZNK6icu_679SpoofImpl11getNumericsERKNS_13UnicodeStringERNS_10UnicodeSetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711CheckResult5clearEv
	.type	_ZN6icu_6711CheckResult5clearEv, @function
_ZN6icu_6711CheckResult5clearEv:
.LFB3115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movl	$0, -4(%rdi)
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	movl	$-1, 216(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3115:
	.size	_ZN6icu_6711CheckResult5clearEv, .-_ZN6icu_6711CheckResult5clearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711CheckResultC2Ev
	.type	_ZN6icu_6711CheckResultC2Ev, @function
_ZN6icu_6711CheckResultC2Ev:
.LFB3110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711CheckResultE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movl	$657779934, 8(%rdi)
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	$0, 12(%rbx)
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	movl	$-1, 216(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3110:
	.size	_ZN6icu_6711CheckResultC2Ev, .-_ZN6icu_6711CheckResultC2Ev
	.globl	_ZN6icu_6711CheckResultC1Ev
	.set	_ZN6icu_6711CheckResultC1Ev,_ZN6icu_6711CheckResultC2Ev
	.weak	_ZTSN6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE
	.section	.rodata._ZTSN6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE,"aG",@progbits,_ZTSN6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE,comdat
	.align 32
	.type	_ZTSN6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE, @object
	.size	_ZTSN6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE, 68
_ZTSN6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE:
	.string	"N6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE"
	.weak	_ZTIN6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE
	.section	.data.rel.ro._ZTIN6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE,"awG",@progbits,_ZTIN6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE,comdat
	.align 8
	.type	_ZTIN6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE, @object
	.size	_ZTIN6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE, 16
_ZTIN6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE
	.weak	_ZTSN6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE
	.section	.rodata._ZTSN6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE,"aG",@progbits,_ZTSN6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE,comdat
	.align 32
	.type	_ZTSN6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE, @object
	.size	_ZTSN6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE, 75
_ZTSN6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE:
	.string	"N6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE"
	.weak	_ZTIN6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE
	.section	.data.rel.ro._ZTIN6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE,"awG",@progbits,_ZTIN6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE,comdat
	.align 8
	.type	_ZTIN6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE, @object
	.size	_ZTIN6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE, 16
_ZTIN6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE
	.weak	_ZTSN6icu_679SpoofImplE
	.section	.rodata._ZTSN6icu_679SpoofImplE,"aG",@progbits,_ZTSN6icu_679SpoofImplE,comdat
	.align 16
	.type	_ZTSN6icu_679SpoofImplE, @object
	.size	_ZTSN6icu_679SpoofImplE, 20
_ZTSN6icu_679SpoofImplE:
	.string	"N6icu_679SpoofImplE"
	.weak	_ZTIN6icu_679SpoofImplE
	.section	.data.rel.ro._ZTIN6icu_679SpoofImplE,"awG",@progbits,_ZTIN6icu_679SpoofImplE,comdat
	.align 8
	.type	_ZTIN6icu_679SpoofImplE, @object
	.size	_ZTIN6icu_679SpoofImplE, 56
_ZTIN6icu_679SpoofImplE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_679SpoofImplE
	.long	0
	.long	2
	.quad	_ZTIN6icu_677UObjectE
	.quad	2
	.quad	_ZTIN6icu_6713IcuCApiHelperI13USpoofCheckerNS_9SpoofImplELi944111087EEE
	.quad	2050
	.weak	_ZTSN6icu_6711CheckResultE
	.section	.rodata._ZTSN6icu_6711CheckResultE,"aG",@progbits,_ZTSN6icu_6711CheckResultE,comdat
	.align 16
	.type	_ZTSN6icu_6711CheckResultE, @object
	.size	_ZTSN6icu_6711CheckResultE, 23
_ZTSN6icu_6711CheckResultE:
	.string	"N6icu_6711CheckResultE"
	.weak	_ZTIN6icu_6711CheckResultE
	.section	.data.rel.ro._ZTIN6icu_6711CheckResultE,"awG",@progbits,_ZTIN6icu_6711CheckResultE,comdat
	.align 8
	.type	_ZTIN6icu_6711CheckResultE, @object
	.size	_ZTIN6icu_6711CheckResultE, 56
_ZTIN6icu_6711CheckResultE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_6711CheckResultE
	.long	0
	.long	2
	.quad	_ZTIN6icu_677UObjectE
	.quad	2
	.quad	_ZTIN6icu_6713IcuCApiHelperI17USpoofCheckResultNS_11CheckResultELi657779934EEE
	.quad	2050
	.weak	_ZTVN6icu_679SpoofImplE
	.section	.data.rel.ro.local._ZTVN6icu_679SpoofImplE,"awG",@progbits,_ZTVN6icu_679SpoofImplE,comdat
	.align 8
	.type	_ZTVN6icu_679SpoofImplE, @object
	.size	_ZTVN6icu_679SpoofImplE, 40
_ZTVN6icu_679SpoofImplE:
	.quad	0
	.quad	_ZTIN6icu_679SpoofImplE
	.quad	_ZN6icu_679SpoofImplD1Ev
	.quad	_ZN6icu_679SpoofImplD0Ev
	.quad	_ZNK6icu_679SpoofImpl17getDynamicClassIDEv
	.weak	_ZTVN6icu_6711CheckResultE
	.section	.data.rel.ro._ZTVN6icu_6711CheckResultE,"awG",@progbits,_ZTVN6icu_6711CheckResultE,comdat
	.align 8
	.type	_ZTVN6icu_6711CheckResultE, @object
	.size	_ZTVN6icu_6711CheckResultE, 40
_ZTVN6icu_6711CheckResultE:
	.quad	0
	.quad	_ZTIN6icu_6711CheckResultE
	.quad	_ZN6icu_6711CheckResultD1Ev
	.quad	_ZN6icu_6711CheckResultD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.local	_ZN6icu_67L17gDefaultSpoofDataE
	.comm	_ZN6icu_67L17gDefaultSpoofDataE,8,8
	.local	_ZN6icu_67L21gSpoofInitDefaultOnceE
	.comm	_ZN6icu_67L21gSpoofInitDefaultOnceE,8,8
	.local	_ZZN6icu_679SpoofImpl16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_679SpoofImpl16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
