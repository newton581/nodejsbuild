	.file	"dtptngen.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator17getDynamicClassIDEv
	.type	_ZNK6icu_6724DateTimePatternGenerator17getDynamicClassIDEv, @function
_ZNK6icu_6724DateTimePatternGenerator17getDynamicClassIDEv:
.LFB3546:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6724DateTimePatternGenerator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3546:
	.size	_ZNK6icu_6724DateTimePatternGenerator17getDynamicClassIDEv, .-_ZNK6icu_6724DateTimePatternGenerator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721DTSkeletonEnumeration17getDynamicClassIDEv
	.type	_ZNK6icu_6721DTSkeletonEnumeration17getDynamicClassIDEv, @function
_ZNK6icu_6721DTSkeletonEnumeration17getDynamicClassIDEv:
.LFB3548:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6721DTSkeletonEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3548:
	.size	_ZNK6icu_6721DTSkeletonEnumeration17getDynamicClassIDEv, .-_ZNK6icu_6721DTSkeletonEnumeration17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722DTRedundantEnumeration17getDynamicClassIDEv
	.type	_ZNK6icu_6722DTRedundantEnumeration17getDynamicClassIDEv, @function
_ZNK6icu_6722DTRedundantEnumeration17getDynamicClassIDEv:
.LFB3550:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6722DTRedundantEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3550:
	.size	_ZNK6icu_6722DTRedundantEnumeration17getDynamicClassIDEv, .-_ZNK6icu_6722DTRedundantEnumeration17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715DateTimeMatcherD2Ev
	.type	_ZN6icu_6715DateTimeMatcherD2Ev, @function
_ZN6icu_6715DateTimeMatcherD2Ev:
.LFB3681:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3681:
	.size	_ZN6icu_6715DateTimeMatcherD2Ev, .-_ZN6icu_6715DateTimeMatcherD2Ev
	.globl	_ZN6icu_6715DateTimeMatcherD1Ev
	.set	_ZN6icu_6715DateTimeMatcherD1Ev,_ZN6icu_6715DateTimeMatcherD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FormatParserD2Ev
	.type	_ZN6icu_6712FormatParserD2Ev, @function
_ZN6icu_6712FormatParserD2Ev:
.LFB3702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712FormatParserE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-56(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	3144(%rdi), %rbx
	movq	%rax, (%rdi)
	.p2align 4,,10
	.p2align 3
.L7:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L7
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3702:
	.size	_ZN6icu_6712FormatParserD2Ev, .-_ZN6icu_6712FormatParserD2Ev
	.globl	_ZN6icu_6712FormatParserD1Ev
	.set	_ZN6icu_6712FormatParserD1Ev,_ZN6icu_6712FormatParserD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FormatParser9setTokensERKNS_13UnicodeStringEiPi
	.type	_ZN6icu_6712FormatParser9setTokensERKNS_13UnicodeStringEiPi, @function
_ZN6icu_6712FormatParser9setTokensERKNS_13UnicodeStringEiPi:
.LFB3705:
	.cfi_startproc
	endbr64
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L11
	movswl	%ax, %edi
	sarl	$5, %edi
.L12:
	movl	$3, %r8d
	cmpl	%edi, %edx
	jge	.L10
	movslq	%edx, %r9
	andl	$2, %eax
	addq	%r9, %r9
	cmpl	%edx, %edi
	jbe	.L14
	testw	%ax, %ax
	jne	.L15
	movq	24(%rsi), %r10
	leaq	2(%r9), %r8
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L16:
	movzwl	-2(%r10,%r8), %esi
	andl	$-33, %esi
	subl	$65, %esi
	cmpw	$25, %si
	ja	.L14
	addl	$1, %eax
	cmpl	%eax, %edi
	jbe	.L33
	movzwl	(%r10,%r8), %esi
	cmpw	%si, (%r10,%r9)
	jne	.L27
.L30:
	addq	$2, %r8
	cmpl	%eax, %edi
	jl	.L27
	jne	.L16
.L14:
	movl	$1, (%rcx)
	movl	$1, %r8d
.L10:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	(%rsi,%r9), %r8
	movl	%edx, %eax
	leaq	10(%rsi), %r10
	.p2align 4,,10
	.p2align 3
.L23:
	movzwl	10(%r8), %esi
	andl	$-33, %esi
	subl	$65, %esi
	cmpw	$25, %si
	ja	.L14
	addl	$1, %eax
	movl	$-1, %esi
	cmpl	%eax, %edi
	jbe	.L24
	movzwl	12(%r8), %esi
.L24:
	cmpw	%si, (%r10,%r9)
	jne	.L27
	addq	$2, %r8
	cmpl	%eax, %edi
	jl	.L27
	jne	.L23
	movl	$1, (%rcx)
	movl	$1, %r8d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$-1, %esi
	cmpw	%si, (%r10,%r9)
	je	.L30
.L27:
	subl	%edx, %eax
	movl	$1, %r8d
	movl	%eax, (%rcx)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movl	12(%rsi), %edi
	jmp	.L12
	.cfi_endproc
.LFE3705:
	.size	_ZN6icu_6712FormatParser9setTokensERKNS_13UnicodeStringEiPi, .-_ZN6icu_6712FormatParser9setTokensERKNS_13UnicodeStringEiPi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712DistanceInfoD2Ev
	.type	_ZN6icu_6712DistanceInfoD2Ev, @function
_ZN6icu_6712DistanceInfoD2Ev:
.LFB3712:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3712:
	.size	_ZN6icu_6712DistanceInfoD2Ev, .-_ZN6icu_6712DistanceInfoD2Ev
	.globl	_ZN6icu_6712DistanceInfoD1Ev
	.set	_ZN6icu_6712DistanceInfoD1Ev,_ZN6icu_6712DistanceInfoD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PtnSkeletonD2Ev
	.type	_ZN6icu_6711PtnSkeletonD2Ev, @function
_ZN6icu_6711PtnSkeletonD2Ev:
.LFB3756:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3756:
	.size	_ZN6icu_6711PtnSkeletonD2Ev, .-_ZN6icu_6711PtnSkeletonD2Ev
	.globl	_ZN6icu_6711PtnSkeletonD1Ev
	.set	_ZN6icu_6711PtnSkeletonD1Ev,_ZN6icu_6711PtnSkeletonD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721DTSkeletonEnumeration5resetER10UErrorCode
	.type	_ZN6icu_6721DTSkeletonEnumeration5resetER10UErrorCode, @function
_ZN6icu_6721DTSkeletonEnumeration5resetER10UErrorCode:
.LFB3774:
	.cfi_startproc
	endbr64
	movl	$0, 116(%rdi)
	ret
	.cfi_endproc
.LFE3774:
	.size	_ZN6icu_6721DTSkeletonEnumeration5resetER10UErrorCode, .-_ZN6icu_6721DTSkeletonEnumeration5resetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721DTSkeletonEnumeration5countER10UErrorCode
	.type	_ZNK6icu_6721DTSkeletonEnumeration5countER10UErrorCode, @function
_ZNK6icu_6721DTSkeletonEnumeration5countER10UErrorCode:
.LFB3775:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L37
	movl	8(%rdx), %eax
.L37:
	ret
	.cfi_endproc
.LFE3775:
	.size	_ZNK6icu_6721DTSkeletonEnumeration5countER10UErrorCode, .-_ZNK6icu_6721DTSkeletonEnumeration5countER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722DTRedundantEnumeration5resetER10UErrorCode
	.type	_ZN6icu_6722DTRedundantEnumeration5resetER10UErrorCode, @function
_ZN6icu_6722DTRedundantEnumeration5resetER10UErrorCode:
.LFB3786:
	.cfi_startproc
	endbr64
	movl	$0, 116(%rdi)
	ret
	.cfi_endproc
.LFE3786:
	.size	_ZN6icu_6722DTRedundantEnumeration5resetER10UErrorCode, .-_ZN6icu_6722DTRedundantEnumeration5resetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722DTRedundantEnumeration5countER10UErrorCode
	.type	_ZNK6icu_6722DTRedundantEnumeration5countER10UErrorCode, @function
_ZNK6icu_6722DTRedundantEnumeration5countER10UErrorCode:
.LFB3787:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L42
	movl	8(%rdx), %eax
.L42:
	ret
	.cfi_endproc
.LFE3787:
	.size	_ZNK6icu_6722DTRedundantEnumeration5countER10UErrorCode, .-_ZNK6icu_6722DTRedundantEnumeration5countER10UErrorCode
	.p2align 4
	.globl	deleteAllowedHourFormats_67
	.type	deleteAllowedHourFormats_67, @function
deleteAllowedHourFormats_67:
.LFB3572:
	.cfi_startproc
	endbr64
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3572:
	.size	deleteAllowedHourFormats_67, .-deleteAllowedHourFormats_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715DateTimeMatcherD0Ev
	.type	_ZN6icu_6715DateTimeMatcherD0Ev, @function
_ZN6icu_6715DateTimeMatcherD0Ev:
.LFB3683:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3683:
	.size	_ZN6icu_6715DateTimeMatcherD0Ev, .-_ZN6icu_6715DateTimeMatcherD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FormatParserD0Ev
	.type	_ZN6icu_6712FormatParserD0Ev, @function
_ZN6icu_6712FormatParserD0Ev:
.LFB3704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712FormatParserE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-56(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	3144(%rdi), %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	.p2align 4,,10
	.p2align 3
.L49:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L49
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3704:
	.size	_ZN6icu_6712FormatParserD0Ev, .-_ZN6icu_6712FormatParserD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712DistanceInfoD0Ev
	.type	_ZN6icu_6712DistanceInfoD0Ev, @function
_ZN6icu_6712DistanceInfoD0Ev:
.LFB3714:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3714:
	.size	_ZN6icu_6712DistanceInfoD0Ev, .-_ZN6icu_6712DistanceInfoD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PtnSkeletonD0Ev
	.type	_ZN6icu_6711PtnSkeletonD0Ev, @function
_ZN6icu_6711PtnSkeletonD0Ev:
.LFB3758:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3758:
	.size	_ZN6icu_6711PtnSkeletonD0Ev, .-_ZN6icu_6711PtnSkeletonD0Ev
	.p2align 4
	.globl	allowedHourFormatsCleanup_67
	.type	allowedHourFormatsCleanup_67, @function
allowedHourFormatsCleanup_67:
.LFB3573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZN6icu_6712_GLOBAL__N_129localeToAllowedHourFormatsMapE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uhash_close_67@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3573:
	.size	allowedHourFormatsCleanup_67, .-allowedHourFormatsCleanup_67
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkD2Ev, @function
_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkD2Ev:
.LFB3581:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE3581:
	.size	_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkD2Ev, .-_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkD1Ev,_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkD0Ev, @function
_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkD0Ev:
.LFB3583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3583:
	.size	_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkD0Ev, .-_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD2Ev
	.type	_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD2Ev, @function
_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD2Ev:
.LFB3611:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE3611:
	.size	_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD2Ev, .-_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD2Ev
	.globl	_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD1Ev
	.set	_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD1Ev,_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD0Ev
	.type	_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD0Ev, @function
_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD0Ev:
.LFB3613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3613:
	.size	_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD0Ev, .-_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD2Ev
	.type	_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD2Ev, @function
_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD2Ev:
.LFB3615:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE3615:
	.size	_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD2Ev, .-_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD2Ev
	.globl	_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD1Ev
	.set	_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD1Ev,_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD0Ev
	.type	_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD0Ev, @function
_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD0Ev:
.LFB3617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3617:
	.size	_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD0Ev, .-_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD2Ev
	.type	_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD2Ev, @function
_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD2Ev:
.LFB3619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE3619:
	.size	_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD2Ev, .-_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD2Ev
	.globl	_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD1Ev
	.set	_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD1Ev,_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD0Ev
	.type	_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD0Ev, @function
_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD0Ev:
.LFB3621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3621:
	.size	_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD0Ev, .-_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD0Ev
	.p2align 4
	.type	_ZN6icu_67L32getAllowedHourFormatsLangCountryEPKcS1_R10UErrorCode, @function
_ZN6icu_67L32getAllowedHourFormatsLangCountryEPKcS1_R10UErrorCode:
.LFB3585:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-99(%rbp), %rax
	movl	$0, -56(%rbp)
	movq	%rax, -112(%rbp)
	xorl	%eax, %eax
	movw	%ax, -100(%rbp)
	movl	$40, -104(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-120(%rbp), %edx
	movq	-128(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%r12, %rdx
	movl	$95, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-128(%rbp), %rsi
	movl	-120(%rbp), %edx
	movq	%r14, %rdi
	movq	%r12, %rcx
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-112(%rbp), %rsi
	movq	_ZN6icu_6712_GLOBAL__N_129localeToAllowedHourFormatsMapE(%rip), %rdi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L74
.L70:
	cmpb	$0, -100(%rbp)
	jne	.L75
.L69:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L76
	addq	$112, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
	movq	%rax, -136(%rbp)
	call	uprv_free_67@PLT
	movq	-136(%rbp), %rax
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L74:
	movq	_ZN6icu_6712_GLOBAL__N_129localeToAllowedHourFormatsMapE(%rip), %rdi
	movq	%r13, %rsi
	call	uhash_get_67@PLT
	jmp	.L70
.L76:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3585:
	.size	_ZN6icu_67L32getAllowedHourFormatsLangCountryEPKcS1_R10UErrorCode, .-_ZN6icu_67L32getAllowedHourFormatsLangCountryEPKcS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721DTSkeletonEnumerationD2Ev
	.type	_ZN6icu_6721DTSkeletonEnumerationD2Ev, @function
_ZN6icu_6721DTSkeletonEnumerationD2Ev:
.LFB3778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721DTSkeletonEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L78
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L79
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L83:
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L80
	movq	(%rax), %rax
	addl	$1, %ebx
	call	*8(%rax)
	movq	120(%r12), %rdi
	cmpl	8(%rdi), %ebx
	jl	.L83
.L79:
	movq	(%rdi), %rax
	call	*8(%rax)
.L78:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717StringEnumerationD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	120(%r12), %rdi
	addl	$1, %ebx
	cmpl	8(%rdi), %ebx
	jl	.L83
	jmp	.L79
	.cfi_endproc
.LFE3778:
	.size	_ZN6icu_6721DTSkeletonEnumerationD2Ev, .-_ZN6icu_6721DTSkeletonEnumerationD2Ev
	.globl	_ZN6icu_6721DTSkeletonEnumerationD1Ev
	.set	_ZN6icu_6721DTSkeletonEnumerationD1Ev,_ZN6icu_6721DTSkeletonEnumerationD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722DTRedundantEnumerationD2Ev
	.type	_ZN6icu_6722DTRedundantEnumerationD2Ev, @function
_ZN6icu_6722DTRedundantEnumerationD2Ev:
.LFB3790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722DTRedundantEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L89
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L90
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L94:
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L91
	movq	(%rax), %rax
	addl	$1, %ebx
	call	*8(%rax)
	movq	120(%r12), %rdi
	cmpl	8(%rdi), %ebx
	jl	.L94
.L90:
	movq	(%rdi), %rax
	call	*8(%rax)
.L89:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717StringEnumerationD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	120(%r12), %rdi
	addl	$1, %ebx
	cmpl	8(%rdi), %ebx
	jl	.L94
	jmp	.L90
	.cfi_endproc
.LFE3790:
	.size	_ZN6icu_6722DTRedundantEnumerationD2Ev, .-_ZN6icu_6722DTRedundantEnumerationD2Ev
	.globl	_ZN6icu_6722DTRedundantEnumerationD1Ev
	.set	_ZN6icu_6722DTRedundantEnumerationD1Ev,_ZN6icu_6722DTRedundantEnumerationD2Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"calendar"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6724DateTimePatternGenerator20getCalendarTypeToUseERKNS_6LocaleERNS_10CharStringER10UErrorCode.part.0, @function
_ZN6icu_6724DateTimePatternGenerator20getCalendarTypeToUseERKNS_6LocaleERNS_10CharStringER10UErrorCode.part.0:
.LFB5226:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %r8
	movq	%r8, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	leaq	-416(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	xorl	%edx, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$258, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-420(%rbp), %rbx
	subq	$400, %rsp
	movq	40(%rdi), %r9
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	pushq	%rbx
	pushq	$0
	pushq	$0
	movl	$0, -420(%rbp)
	call	ures_getFunctionalEquivalent_67@PLT
	movq	%r15, %rdx
	addq	$32, %rsp
	movq	%rbx, %r8
	movl	$96, %ecx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	movb	$0, -63(%rbp)
	call	uloc_getKeywordValue_67@PLT
	movl	-420(%rbp), %edx
	testl	%edx, %edx
	jle	.L100
	cmpl	$2, %edx
	jne	.L111
.L100:
	cmpl	$95, %eax
	jle	.L112
.L99:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movl	%edx, 0(%r13)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L112:
	movq	(%r12), %rax
	movq	%r13, %rcx
	movl	$-1, %edx
	movq	%r15, %rsi
	movl	$0, 56(%r12)
	movq	%r12, %rdi
	movb	$0, (%rax)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L99
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5226:
	.size	_ZN6icu_6724DateTimePatternGenerator20getCalendarTypeToUseERKNS_6LocaleERNS_10CharStringER10UErrorCode.part.0, .-_ZN6icu_6724DateTimePatternGenerator20getCalendarTypeToUseERKNS_6LocaleERNS_10CharStringER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721DTSkeletonEnumeration5snextER10UErrorCode
	.type	_ZN6icu_6721DTSkeletonEnumeration5snextER10UErrorCode, @function
_ZN6icu_6721DTSkeletonEnumeration5snextER10UErrorCode:
.LFB3773:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L114
	movq	120(%rdi), %r8
	testq	%r8, %r8
	je	.L114
	movl	116(%rdi), %esi
	cmpl	8(%r8), %esi
	jge	.L114
	leal	1(%rsi), %eax
	movl	%eax, 116(%rdi)
	movq	%r8, %rdi
	jmp	_ZNK6icu_677UVector9elementAtEi@PLT
	.p2align 4,,10
	.p2align 3
.L114:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3773:
	.size	_ZN6icu_6721DTSkeletonEnumeration5snextER10UErrorCode, .-_ZN6icu_6721DTSkeletonEnumeration5snextER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722DTRedundantEnumeration5snextER10UErrorCode
	.type	_ZN6icu_6722DTRedundantEnumeration5snextER10UErrorCode, @function
_ZN6icu_6722DTRedundantEnumeration5snextER10UErrorCode:
.LFB3785:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L121
	movq	120(%rdi), %r8
	testq	%r8, %r8
	je	.L121
	movl	116(%rdi), %esi
	cmpl	8(%r8), %esi
	jge	.L121
	leal	1(%rsi), %eax
	movl	%eax, 116(%rdi)
	movq	%r8, %rdi
	jmp	_ZNK6icu_677UVector9elementAtEi@PLT
	.p2align 4,,10
	.p2align 3
.L121:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3785:
	.size	_ZN6icu_6722DTRedundantEnumeration5snextER10UErrorCode, .-_ZN6icu_6722DTRedundantEnumeration5snextER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6724DateTimePatternGenerator13initHashtableER10UErrorCode.part.0, @function
_ZN6icu_6724DateTimePatternGenerator13initHashtableER10UErrorCode.part.0:
.LFB5267:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$88, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L129
	movl	(%rbx), %esi
	movq	$0, (%rax)
	movq	%rax, %r12
	testl	%esi, %esi
	jle	.L138
.L132:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L136
	call	uhash_close_67@PLT
.L136:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	leaq	8(%rax), %r14
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	uhash_init_67@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L132
	movq	%r14, (%r12)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	uhash_setKeyDeleter_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L132
	movq	%r12, 4496(%r13)
.L128:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L129:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L128
	movl	$7, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5267:
	.size	_ZN6icu_6724DateTimePatternGenerator13initHashtableER10UErrorCode.part.0, .-_ZN6icu_6724DateTimePatternGenerator13initHashtableER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718PatternMapIteratorD2Ev
	.type	_ZN6icu_6718PatternMapIteratorD2Ev, @function
_ZN6icu_6718PatternMapIteratorD2Ev:
.LFB3722:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6718PatternMapIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L139
	movq	(%rdi), %rax
	leaq	_ZN6icu_6715DateTimeMatcherD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L141
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L139:
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	jmp	*%rax
	.cfi_endproc
.LFE3722:
	.size	_ZN6icu_6718PatternMapIteratorD2Ev, .-_ZN6icu_6718PatternMapIteratorD2Ev
	.globl	_ZN6icu_6718PatternMapIteratorD1Ev
	.set	_ZN6icu_6718PatternMapIteratorD1Ev,_ZN6icu_6718PatternMapIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718PatternMapIteratorD0Ev
	.type	_ZN6icu_6718PatternMapIteratorD0Ev, @function
_ZN6icu_6718PatternMapIteratorD0Ev:
.LFB3724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718PatternMapIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L143
	movq	(%rdi), %rax
	leaq	_ZN6icu_6715DateTimeMatcherD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L144
	call	_ZN6icu_677UMemorydlEPv@PLT
.L143:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	call	*%rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3724:
	.size	_ZN6icu_6718PatternMapIteratorD0Ev, .-_ZN6icu_6718PatternMapIteratorD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6710PatternMap16getDuplicateElemERKNS_13UnicodeStringERKNS_11PtnSkeletonEPNS_7PtnElemE.part.0, @function
_ZN6icu_6710PatternMap16getDuplicateElemERKNS_13UnicodeStringERKNS_11PtnSkeletonEPNS_7PtnElemE.part.0:
.LFB5244:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L166:
	notl	%eax
	andl	$1, %eax
	testb	%al, %al
	je	.L165
.L159:
	movq	152(%r12), %r12
	testq	%r12, %r12
	je	.L160
.L161:
	movzwl	16(%r12), %esi
	testw	%si, %si
	js	.L150
	movswl	%si, %ecx
	sarl	$5, %ecx
.L151:
	movzwl	8(%r14), %eax
	testw	%ax, %ax
	js	.L152
	movswl	%ax, %edx
	sarl	$5, %edx
.L153:
	testb	$1, %sil
	jne	.L166
	testl	%ecx, %ecx
	movl	%ebx, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L156
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L156:
	andl	$2, %esi
	leaq	18(%r12), %rcx
	jne	.L158
	movq	32(%r12), %rcx
.L158:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jne	.L159
.L165:
	movq	72(%r12), %rax
	movl	8(%rax), %edi
	cmpl	%edi, 8(%r13)
	jne	.L159
	movl	12(%r13), %edi
	cmpl	%edi, 12(%rax)
	jne	.L159
	movl	16(%r13), %edi
	cmpl	%edi, 16(%rax)
	jne	.L159
	movl	20(%r13), %edi
	cmpl	%edi, 20(%rax)
	jne	.L159
	movl	24(%r13), %edi
	cmpl	%edi, 24(%rax)
	jne	.L159
	movl	28(%r13), %edi
	cmpl	%edi, 28(%rax)
	jne	.L159
	movl	32(%r13), %edx
	cmpl	%edx, 32(%rax)
	jne	.L159
	movl	36(%r13), %edi
	cmpl	%edi, 36(%rax)
	jne	.L159
	movl	40(%r13), %edx
	cmpl	%edx, 40(%rax)
	jne	.L159
	movl	44(%r13), %edi
	cmpl	%edi, 44(%rax)
	jne	.L159
	movl	48(%r13), %edx
	cmpl	%edx, 48(%rax)
	jne	.L159
	movl	52(%r13), %edi
	cmpl	%edi, 52(%rax)
	jne	.L159
	movl	56(%r13), %edx
	cmpl	%edx, 56(%rax)
	jne	.L159
	movl	60(%r13), %edi
	cmpl	%edi, 60(%rax)
	jne	.L159
	movl	64(%r13), %edx
	cmpl	%edx, 64(%rax)
	jne	.L159
	movl	68(%r13), %esi
	cmpl	%esi, 68(%rax)
	jne	.L159
.L160:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movl	12(%r14), %edx
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L150:
	movl	20(%r12), %ecx
	jmp	.L151
	.cfi_endproc
.LFE5244:
	.size	_ZN6icu_6710PatternMap16getDuplicateElemERKNS_13UnicodeStringERKNS_11PtnSkeletonEPNS_7PtnElemE.part.0, .-_ZN6icu_6710PatternMap16getDuplicateElemERKNS_13UnicodeStringERKNS_11PtnSkeletonEPNS_7PtnElemE.part.0
	.section	.rodata._ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Era"
.LC2:
	.string	"Year"
.LC3:
	.string	"Quarter"
.LC4:
	.string	"Month"
.LC5:
	.string	"Week"
.LC6:
	.string	"Day-Of-Week"
.LC7:
	.string	"Day"
.LC8:
	.string	"Hour"
.LC9:
	.string	"Minute"
.LC10:
	.string	"Second"
.LC11:
	.string	"Timezone"
	.section	.text._ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode,"axG",@progbits,_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode:
.LFB3599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$184, %rsp
	movq	%rdi, -216(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -200(%rbp)
	movq	%rbx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%r8, %rdx
	call	*88(%rax)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L167
	movq	%r13, -224(%rbp)
	xorl	%r12d, %r12d
	leaq	-200(%rbp), %r15
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L199:
	movq	-200(%rbp), %rax
	movl	$4, %ecx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%r8b
	sbbb	$0, %r8b
	movsbl	%r8b, %r8d
	testl	%r8d, %r8d
	je	.L170
	movl	$5, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L177
	movl	$8, %ecx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L178
	movl	$6, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L179
	movl	$5, %ecx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L180
	cmpb	$42, (%rax)
	jne	.L187
	cmpb	$0, 1(%rax)
	je	.L181
.L187:
	movl	$12, %ecx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L182
	movl	$4, %ecx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L183
	movl	$5, %ecx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L184
	movl	$7, %ecx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L185
	movl	$7, %ecx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L186
	movq	%rax, %rdi
	movl	$9, %ecx
	leaq	.LC11(%rip), %rsi
	movl	$15, %r8d
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L170
.L174:
	addl	$1, %r12d
.L168:
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	jne	.L199
.L167:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L186:
	.cfi_restore_state
	movl	$13, %r8d
	.p2align 4,,10
	.p2align 3
.L170:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	leaq	-128(%rbp), %r13
	movl	%r8d, -208(%rbp)
	movq	-224(%rbp), %rdx
	leaq	-188(%rbp), %rsi
	movl	$0, -188(%rbp)
	call	*32(%rax)
	movl	-188(%rbp), %ecx
	leaq	-184(%rbp), %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-216(%rbp), %rax
	movslq	-208(%rbp), %r8
	movq	8(%rax), %rax
	salq	$6, %r8
	leaq	264(%rax,%r8), %rdi
	movswl	8(%rdi), %eax
	shrl	$5, %eax
	jne	.L173
	movswl	-120(%rbp), %eax
	shrl	$5, %eax
	jne	.L201
.L173:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L201:
	movq	%r13, %rsi
	movq	%rdi, -208(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-208(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	jmp	.L173
.L177:
	movl	$1, %r8d
	jmp	.L170
.L178:
	movl	$2, %r8d
	jmp	.L170
.L179:
	movl	$3, %r8d
	jmp	.L170
.L180:
	movl	$4, %r8d
	jmp	.L170
.L181:
	movl	$5, %r8d
	jmp	.L170
.L182:
	movl	$6, %r8d
	jmp	.L170
.L183:
	movl	$9, %r8d
	jmp	.L170
.L184:
	movl	$11, %r8d
	jmp	.L170
.L185:
	movl	$12, %r8d
	jmp	.L170
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3599:
	.size	_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_677PtnElemD2Ev
	.type	_ZN6icu_677PtnElemD2Ev, @function
_ZN6icu_677PtnElemD2Ev:
.LFB3765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_677PtnElemE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	152(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L203
	movq	(%r12), %rax
	leaq	_ZN6icu_677PtnElemD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L204
	call	_ZN6icu_677PtnElemD1Ev
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L203:
	leaq	80(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L205
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L206
	call	_ZN6icu_677UMemorydlEPv@PLT
.L205:
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	call	*%rax
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L206:
	call	*%rax
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE3765:
	.size	_ZN6icu_677PtnElemD2Ev, .-_ZN6icu_677PtnElemD2Ev
	.globl	_ZN6icu_677PtnElemD1Ev
	.set	_ZN6icu_677PtnElemD1Ev,_ZN6icu_677PtnElemD2Ev
	.align 2
	.p2align 4
	.type	_ZNK6icu_6710PatternMap6equalsERKS0_.part.0, @function
_ZNK6icu_6710PatternMap6equalsERKS0_.part.0:
.LFB5253:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	$8, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
.L217:
	movq	(%r15,%r13), %rbx
	movq	(%r14,%r13), %r12
	cmpq	%r12, %rbx
	je	.L242
	testq	%rbx, %rbx
	je	.L220
	testq	%r12, %r12
	je	.L220
	.p2align 4,,10
	.p2align 3
.L218:
	movswl	16(%r12), %eax
	movswl	16(%rbx), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L221
	testw	%dx, %dx
	js	.L222
	sarl	$5, %edx
	testw	%ax, %ax
	js	.L224
.L268:
	sarl	$5, %eax
.L225:
	cmpl	%edx, %eax
	jne	.L220
	testb	%cl, %cl
	je	.L267
	.p2align 4,,10
	.p2align 3
.L220:
	xorl	%eax, %eax
.L214:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	leaq	8(%r12), %rsi
	leaq	8(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L221:
	testb	%cl, %cl
	je	.L220
	movswl	88(%r12), %eax
	movswl	88(%rbx), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L226
	testw	%dx, %dx
	js	.L227
	sarl	$5, %edx
.L228:
	testw	%ax, %ax
	js	.L229
	sarl	$5, %eax
.L230:
	testb	%cl, %cl
	jne	.L220
	cmpl	%edx, %eax
	jne	.L220
	leaq	80(%r12), %rsi
	leaq	80(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L226:
	testb	%cl, %cl
	je	.L220
	movq	72(%rbx), %rdx
	movq	72(%r12), %rax
	cmpq	%rax, %rdx
	je	.L231
	movq	72(%rdx), %rcx
	movq	80(%rdx), %rsi
	xorq	72(%rax), %rcx
	xorq	80(%rax), %rsi
	orq	%rcx, %rsi
	jne	.L220
	movq	88(%rdx), %rcx
	movq	96(%rdx), %rsi
	xorq	88(%rax), %rcx
	xorq	96(%rax), %rsi
	orq	%rcx, %rsi
	jne	.L220
	movq	104(%rdx), %rcx
	movq	112(%rdx), %rsi
	xorq	104(%rax), %rcx
	xorq	112(%rax), %rsi
	orq	%rcx, %rsi
	jne	.L220
	movq	120(%rdx), %rcx
	movq	128(%rdx), %rsi
	xorq	120(%rax), %rcx
	xorq	128(%rax), %rsi
	orq	%rcx, %rsi
	jne	.L220
	leaq	8(%rdx), %rcx
	movq	8(%rcx), %rdi
	movq	8(%rdx), %rdx
	leaq	8(%rax), %rsi
	xorq	16(%rax), %rdi
	xorq	8(%rax), %rdx
	orq	%rdx, %rdi
	jne	.L220
	movq	16(%rcx), %rax
	movq	24(%rcx), %rdx
	xorq	16(%rsi), %rax
	xorq	24(%rsi), %rdx
	orq	%rax, %rdx
	jne	.L220
	movq	32(%rcx), %rax
	movq	40(%rcx), %rdx
	xorq	32(%rsi), %rax
	xorq	40(%rsi), %rdx
	orq	%rax, %rdx
	jne	.L220
	movq	48(%rcx), %rax
	movq	56(%rcx), %rdx
	xorq	48(%rsi), %rax
	xorq	56(%rsi), %rdx
	orq	%rax, %rdx
	jne	.L220
	.p2align 4,,10
	.p2align 3
.L231:
	movq	152(%r12), %r12
	movq	152(%rbx), %rbx
	testq	%r12, %r12
	movq	%rbx, %rdi
	sete	%al
	testq	%rbx, %rbx
	sete	%dl
	orq	%r12, %rdi
	je	.L242
	cmpq	%r12, %rbx
	je	.L242
	testb	%al, %al
	jne	.L220
	testb	%dl, %dl
	je	.L218
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L222:
	movl	20(%rbx), %edx
	testw	%ax, %ax
	jns	.L268
.L224:
	movl	20(%r12), %eax
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L229:
	movl	92(%r12), %eax
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L227:
	movl	92(%rbx), %edx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L242:
	addq	$8, %r13
	cmpq	$424, %r13
	jne	.L217
	movl	$1, %eax
	jmp	.L214
	.cfi_endproc
.LFE5253:
	.size	_ZNK6icu_6710PatternMap6equalsERKS0_.part.0, .-_ZNK6icu_6710PatternMap6equalsERKS0_.part.0
	.section	.rodata._ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSink3putEPKcRNS_13ResourceValueEaR10UErrorCode.str1.1,"aMS",@progbits,1
.LC12:
	.string	"-narrow"
.LC13:
	.string	"-short"
.LC14:
	.string	"era"
.LC15:
	.string	"year"
.LC16:
	.string	"quarter"
.LC17:
	.string	"month"
.LC18:
	.string	"week"
.LC19:
	.string	"weekOfMonth"
.LC20:
	.string	"weekday"
.LC21:
	.string	"dayOfYear"
.LC22:
	.string	"weekdayOfMonth"
.LC23:
	.string	"day"
.LC24:
	.string	"dayperiod"
.LC25:
	.string	"hour"
.LC26:
	.string	"minute"
.LC27:
	.string	"second"
.LC28:
	.string	"*"
.LC29:
	.string	"zone"
	.section	.text._ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSink3putEPKcRNS_13ResourceValueEaR10UErrorCode,"axG",@progbits,_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSink3putEPKcRNS_13ResourceValueEaR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSink3putEPKcRNS_13ResourceValueEaR10UErrorCode:
.LFB3604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$344, %rsp
	movq	%rdi, -384(%rbp)
	leaq	-320(%rbp), %rdi
	movq	%rsi, -344(%rbp)
	movq	%rbx, %rsi
	movq	%r8, -360(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rdi, -368(%rbp)
	movq	%r8, %rdx
	call	*88(%rax)
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L269
	xorl	%r14d, %r14d
	leaq	-344(%rbp), %r15
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L274:
	cmpl	$1918985593, (%r12)
	je	.L350
.L277:
	movabsq	$32199698154616177, %rax
	cmpq	%rax, (%r12)
	je	.L351
	cmpl	$1953394541, (%r12)
	je	.L352
.L281:
	cmpl	$1801807223, (%r12)
	je	.L353
.L283:
	movabsq	$8020179002706978167, %rax
	cmpq	%rax, (%r12)
	je	.L354
.L285:
	movabsq	$34165556108420471, %rax
	cmpq	%rax, (%r12)
	je	.L355
	movabsq	$7018113890281677156, %rax
	cmpq	%rax, (%r12)
	je	.L356
.L289:
	movabsq	$5726715485104727415, %rax
	cmpq	%rax, (%r12)
	je	.L357
.L291:
	cmpl	$7954788, (%r12)
	je	.L358
	movabsq	$8028073590768886116, %rax
	cmpq	%rax, (%r12)
	je	.L359
.L295:
	cmpl	$1920298856, (%r12)
	je	.L360
.L297:
	cmpl	$1970170221, (%r12)
	je	.L361
.L299:
	cmpl	$1868785011, (%r12)
	je	.L362
.L301:
	cmpw	$42, (%r12)
	je	.L363
	cmpl	$1701736314, (%r12)
	je	.L364
.L316:
	addl	$1, %r14d
.L315:
	movq	-368(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	%r14d, %esi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L269
	movq	-344(%rbp), %rsi
	leaq	-96(%rbp), %r12
	movl	$24, %edx
	movq	%r12, %rdi
	call	strncpy@PLT
	movl	$45, %esi
	movq	%r12, %rdi
	movb	$0, -72(%rbp)
	call	strchr@PLT
	movl	$0, -352(%rbp)
	testq	%rax, %rax
	je	.L271
	movl	$8, %ecx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L319
	movl	$7, %ecx
	leaq	.LC13(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	xorl	%ecx, %ecx
	testb	%dl, %dl
	sete	%cl
	movl	%ecx, -352(%rbp)
.L273:
	movb	$0, (%rax)
.L271:
	cmpl	$6386277, (%r12)
	jne	.L274
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L276:
	movq	(%rbx), %rax
	leaq	-272(%rbp), %r13
	movq	-360(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	*88(%rax)
	movq	-360(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L269
	xorl	%esi, %esi
	movl	%r12d, -376(%rbp)
	movq	%r13, %r12
	movl	%esi, %r13d
.L308:
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L316
	movq	-344(%rbp), %rax
	cmpb	$100, (%rax)
	jne	.L336
	cmpb	$110, 1(%rax)
	jne	.L336
	cmpb	$0, 2(%rax)
	jne	.L336
	movq	(%rbx), %rax
	movq	-360(%rbp), %rdx
	movq	%rbx, %rdi
	leaq	-332(%rbp), %rsi
	movl	$0, -332(%rbp)
	movslq	-376(%rbp), %r12
	call	*32(%rax)
	leaq	-224(%rbp), %r8
	movl	-332(%rbp), %ecx
	leaq	-328(%rbp), %rdx
	movl	$1, %esi
	movq	%r8, %rdi
	movq	%rax, -328(%rbp)
	movq	%r8, -376(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	-352(%rbp), %r13d
	leaq	(%r12,%r12,2), %rax
	leaq	20(%rax,%r13), %r12
	movq	-384(%rbp), %rax
	leaq	-160(%rbp), %r13
	salq	$6, %r12
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	addq	$8, %r12
	addq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movswl	-152(%rbp), %eax
	movq	-376(%rbp), %r8
	shrl	$5, %eax
	jne	.L314
	movswl	-216(%rbp), %eax
	shrl	$5, %eax
	je	.L314
	movq	%r13, %rdi
	movq	%r8, -352(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-384(%rbp), %rax
	movq	-352(%rbp), %r8
	addq	8(%rax), %r12
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-352(%rbp), %r8
.L313:
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L336:
	addl	$1, %r13d
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%r13, %rdi
	movq	%r8, -352(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-352(%rbp), %r8
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L350:
	cmpb	$0, 4(%r12)
	jne	.L277
	movl	$1, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L351:
	movl	$2, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L352:
	cmpw	$104, 4(%r12)
	jne	.L281
	movl	$3, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L353:
	cmpb	$0, 4(%r12)
	jne	.L283
	movl	$4, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L354:
	cmpl	$6845550, 8(%r12)
	jne	.L285
	movl	$5, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L355:
	movl	$6, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L356:
	cmpw	$114, 8(%r12)
	jne	.L289
	movl	$7, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L357:
	cmpl	$1852788070, 8(%r12)
	jne	.L291
	cmpw	$26740, 12(%r12)
	jne	.L291
	cmpb	$0, 14(%r12)
	jne	.L291
	movl	$8, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L358:
	movl	$9, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L359:
	cmpw	$100, 8(%r12)
	jne	.L295
	movl	$10, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L360:
	cmpb	$0, 4(%r12)
	jne	.L297
	movl	$11, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L361:
	cmpw	$25972, 4(%r12)
	jne	.L299
	cmpb	$0, 6(%r12)
	jne	.L299
	movl	$12, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L362:
	cmpw	$25710, 4(%r12)
	jne	.L301
	cmpb	$0, 6(%r12)
	jne	.L301
	movl	$13, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L269:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L365
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	movl	$14, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L364:
	cmpb	$0, 4(%r12)
	jne	.L316
	movl	$15, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L319:
	movl	$2, -352(%rbp)
	jmp	.L273
.L365:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3604:
	.size	_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721DTSkeletonEnumerationD0Ev
	.type	_ZN6icu_6721DTSkeletonEnumerationD0Ev, @function
_ZN6icu_6721DTSkeletonEnumerationD0Ev:
.LFB3780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721DTSkeletonEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L367
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L368
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L372:
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L369
	movq	(%rax), %rax
	addl	$1, %ebx
	call	*8(%rax)
	movq	120(%r12), %rdi
	cmpl	8(%rdi), %ebx
	jl	.L372
.L368:
	movq	(%rdi), %rax
	call	*8(%rax)
.L367:
	movq	%r12, %rdi
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore_state
	movq	120(%r12), %rdi
	addl	$1, %ebx
	cmpl	%ebx, 8(%rdi)
	jg	.L372
	jmp	.L368
	.cfi_endproc
.LFE3780:
	.size	_ZN6icu_6721DTSkeletonEnumerationD0Ev, .-_ZN6icu_6721DTSkeletonEnumerationD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722DTRedundantEnumerationD0Ev
	.type	_ZN6icu_6722DTRedundantEnumerationD0Ev, @function
_ZN6icu_6722DTRedundantEnumerationD0Ev:
.LFB3792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722DTRedundantEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L378
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L379
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L383:
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L380
	movq	(%rax), %rax
	addl	$1, %ebx
	call	*8(%rax)
	movq	120(%r12), %rdi
	cmpl	8(%rdi), %ebx
	jl	.L383
.L379:
	movq	(%rdi), %rax
	call	*8(%rax)
.L378:
	movq	%r12, %rdi
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	movq	120(%r12), %rdi
	addl	$1, %ebx
	cmpl	%ebx, 8(%rdi)
	jg	.L383
	jmp	.L379
	.cfi_endproc
.LFE3792:
	.size	_ZN6icu_6722DTRedundantEnumerationD0Ev, .-_ZN6icu_6722DTRedundantEnumerationD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710PatternMapD2Ev
	.type	_ZN6icu_6710PatternMapD2Ev, @function
_ZN6icu_6710PatternMapD2Ev:
.LFB3667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6710PatternMapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	_ZN6icu_677PtnElemD0Ev(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	8(%rdi), %rbx
	subq	$56, %rsp
	movq	%rax, (%rdi)
	leaq	424(%rdi), %rax
	movq	%rax, -56(%rbp)
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L459:
	movq	152(%r15), %r14
	leaq	16+_ZTVN6icu_677PtnElemE(%rip), %rsi
	movq	%rsi, (%r15)
	testq	%r14, %r14
	je	.L391
	movq	(%r14), %rax
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	jne	.L392
	movq	152(%r14), %r13
	movq	%rsi, (%r14)
	testq	%r13, %r13
	je	.L393
	movq	0(%r13), %rax
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	jne	.L394
	movq	152(%r13), %r11
	movq	%rsi, 0(%r13)
	testq	%r11, %r11
	je	.L395
	movq	(%r11), %rax
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	jne	.L396
	movq	152(%r11), %r8
	movq	%rsi, (%r11)
	testq	%r8, %r8
	je	.L397
	movq	(%r8), %rdx
	movq	8(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L398
	movq	152(%r8), %r9
	movq	%rsi, (%r8)
	testq	%r9, %r9
	je	.L399
	movq	(%r9), %rcx
	movq	8(%rcx), %rcx
	cmpq	%r12, %rcx
	jne	.L400
	movq	152(%r9), %rdi
	movq	%rsi, (%r9)
	testq	%rdi, %rdi
	je	.L401
	movq	(%rdi), %rcx
	movq	8(%rcx), %rcx
	cmpq	%r12, %rcx
	jne	.L402
	movq	%r9, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN6icu_677PtnElemD1Ev
	movq	-64(%rbp), %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r9
.L401:
	leaq	80(%r9), %rdi
	movq	%r8, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r8
	movq	72(%r9), %rdi
	testq	%rdi, %rdi
	je	.L403
	movq	(%rdi), %rcx
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rax
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	8(%rcx), %rcx
	movq	%r11, -64(%rbp)
	cmpq	%rax, %rcx
	jne	.L404
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
.L403:
	leaq	8(%r9), %rdi
	movq	%r8, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r8
.L399:
	leaq	80(%r8), %rdi
	movq	%r11, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %r11
	movq	72(%r8), %rdi
	testq	%rdi, %rdi
	je	.L405
	movq	(%rdi), %rdx
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rax
	movq	%r8, -72(%rbp)
	movq	%r11, -64(%rbp)
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L406
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r8
.L405:
	leaq	8(%r8), %rdi
	movq	%r11, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-72(%rbp), %r11
.L397:
	leaq	80(%r11), %rdi
	movq	%r11, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r11
	movq	72(%r11), %rdi
	testq	%rdi, %rdi
	je	.L407
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rcx
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L408
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r11
.L407:
	leaq	8(%r11), %rdi
	movq	%r11, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L395:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%r13), %rdi
	testq	%rdi, %rdi
	je	.L409
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L410
	call	_ZN6icu_677UMemorydlEPv@PLT
.L409:
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L393:
	leaq	80(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%r14), %rdi
	testq	%rdi, %rdi
	je	.L411
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L412
	call	_ZN6icu_677UMemorydlEPv@PLT
.L411:
	leaq	8(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L391:
	leaq	80(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.L413
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rcx
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L414
	call	_ZN6icu_677UMemorydlEPv@PLT
.L413:
	leaq	8(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L415:
	movq	$0, (%rbx)
.L389:
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	je	.L458
.L416:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L389
	movq	(%r15), %rax
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	je	.L459
	movq	%r15, %rdi
	call	*%rax
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L392:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L414:
	call	*%rax
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L394:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L412:
	call	*%rax
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%r11, %rdi
	call	*%rax
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L410:
	call	*%rax
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L458:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	movq	%r11, -64(%rbp)
	movq	%r8, %rdi
	call	*%rdx
	movq	-64(%rbp), %r11
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L408:
	movq	%r11, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %r11
	jmp	.L407
.L400:
	movq	%r8, -72(%rbp)
	movq	%r9, %rdi
	movq	%r11, -64(%rbp)
	call	*%rcx
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r11
	jmp	.L399
.L406:
	call	*%rdx
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r11
	jmp	.L405
.L404:
	call	*%rcx
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r11
	jmp	.L403
.L402:
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	*%rcx
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r11
	jmp	.L401
	.cfi_endproc
.LFE3667:
	.size	_ZN6icu_6710PatternMapD2Ev, .-_ZN6icu_6710PatternMapD2Ev
	.globl	_ZN6icu_6710PatternMapD1Ev
	.set	_ZN6icu_6710PatternMapD1Ev,_ZN6icu_6710PatternMapD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710PatternMapD0Ev
	.type	_ZN6icu_6710PatternMapD0Ev, @function
_ZN6icu_6710PatternMapD0Ev:
.LFB3669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	leaq	16+_ZTVN6icu_6710PatternMapE(%rip), %rdx
	addq	$424, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	_ZN6icu_677PtnElemD0Ev(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	8(%rdi), %rbx
	subq	$56, %rsp
	movq	%rdi, -88(%rbp)
	movq	%rdx, (%rdi)
	movq	%rax, -56(%rbp)
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L531:
	movq	152(%r15), %r14
	leaq	16+_ZTVN6icu_677PtnElemE(%rip), %rsi
	movq	%rsi, (%r15)
	testq	%r14, %r14
	je	.L463
	movq	(%r14), %rax
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	jne	.L464
	movq	152(%r14), %r13
	movq	%rsi, (%r14)
	testq	%r13, %r13
	je	.L465
	movq	0(%r13), %rax
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	jne	.L466
	movq	152(%r13), %r11
	movq	%rsi, 0(%r13)
	testq	%r11, %r11
	je	.L467
	movq	(%r11), %rax
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	jne	.L468
	movq	152(%r11), %r8
	movq	%rsi, (%r11)
	testq	%r8, %r8
	je	.L469
	movq	(%r8), %rdx
	movq	8(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L470
	movq	152(%r8), %r9
	movq	%rsi, (%r8)
	testq	%r9, %r9
	je	.L471
	movq	(%r9), %rcx
	movq	8(%rcx), %rcx
	cmpq	%r12, %rcx
	jne	.L472
	movq	152(%r9), %rdi
	movq	%rsi, (%r9)
	testq	%rdi, %rdi
	je	.L473
	movq	(%rdi), %rcx
	movq	8(%rcx), %rcx
	cmpq	%r12, %rcx
	jne	.L474
	movq	%r9, -96(%rbp)
	movq	%r8, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN6icu_677PtnElemD1Ev
	movq	-64(%rbp), %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r8
	movq	-96(%rbp), %r9
.L473:
	leaq	80(%r9), %rdi
	movq	%r8, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r8
	movq	72(%r9), %rdi
	testq	%rdi, %rdi
	je	.L475
	movq	(%rdi), %rcx
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rax
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	8(%rcx), %rcx
	movq	%r11, -64(%rbp)
	cmpq	%rax, %rcx
	jne	.L476
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
.L475:
	leaq	8(%r9), %rdi
	movq	%r8, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r8
.L471:
	leaq	80(%r8), %rdi
	movq	%r11, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %r11
	movq	72(%r8), %rdi
	testq	%rdi, %rdi
	je	.L477
	movq	(%rdi), %rdx
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rax
	movq	%r8, -72(%rbp)
	movq	%r11, -64(%rbp)
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L478
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r8
.L477:
	leaq	8(%r8), %rdi
	movq	%r11, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-72(%rbp), %r11
.L469:
	leaq	80(%r11), %rdi
	movq	%r11, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r11
	movq	72(%r11), %rdi
	testq	%rdi, %rdi
	je	.L479
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rcx
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L480
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r11
.L479:
	leaq	8(%r11), %rdi
	movq	%r11, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L467:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%r13), %rdi
	testq	%rdi, %rdi
	je	.L481
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L482
	call	_ZN6icu_677UMemorydlEPv@PLT
.L481:
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L465:
	leaq	80(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%r14), %rdi
	testq	%rdi, %rdi
	je	.L483
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L484
	call	_ZN6icu_677UMemorydlEPv@PLT
.L483:
	leaq	8(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L463:
	leaq	80(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.L485
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rcx
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L486
	call	_ZN6icu_677UMemorydlEPv@PLT
.L485:
	leaq	8(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L487:
	movq	$0, (%rbx)
.L461:
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	je	.L530
.L488:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L461
	movq	(%r15), %rax
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	je	.L531
	movq	%r15, %rdi
	call	*%rax
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L464:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L486:
	call	*%rax
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L466:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L484:
	call	*%rax
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L468:
	movq	%r11, %rdi
	call	*%rax
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L482:
	call	*%rax
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L530:
	movq	-88(%rbp), %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movq	%r11, -64(%rbp)
	movq	%r8, %rdi
	call	*%rdx
	movq	-64(%rbp), %r11
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%r11, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %r11
	jmp	.L479
.L472:
	movq	%r8, -72(%rbp)
	movq	%r9, %rdi
	movq	%r11, -64(%rbp)
	call	*%rcx
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r11
	jmp	.L471
.L478:
	call	*%rdx
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r11
	jmp	.L477
.L476:
	call	*%rcx
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r11
	jmp	.L475
.L474:
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	*%rcx
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r11
	jmp	.L473
	.cfi_endproc
.LFE3669:
	.size	_ZN6icu_6710PatternMapD0Ev, .-_ZN6icu_6710PatternMapD0Ev
	.section	.rodata.str1.1
.LC30:
	.string	"allowed"
.LC31:
	.string	"preferred"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, @function
_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode:
.LFB3578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$328, %rsp
	movq	%rsi, -280(%rbp)
	movq	%rbx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-224(%rbp), %rax
	movq	%rax, -352(%rbp)
	movq	%rax, %rdi
	movq	(%rdx), %rax
	movq	%r8, %rdx
	call	*88(%rax)
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L532
	leaq	-280(%rbp), %rax
	movq	%r15, -288(%rbp)
	movq	%rbx, %r15
	movq	%rax, -312(%rbp)
	leaq	-118(%rbp), %rax
	movl	$0, -356(%rbp)
	movq	%rax, -336(%rbp)
.L575:
	movq	-312(%rbp), %rdx
	movl	-356(%rbp), %esi
	movq	%r15, %rcx
	movq	-352(%rbp), %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L532
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %rbx
	movq	%r15, %rsi
	movq	%rax, -368(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rbx, %rdx
	movq	%rax, -328(%rbp)
	movq	%rax, %rdi
	movq	(%r15), %rax
	call	*88(%rax)
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L532
	movq	$0, -320(%rbp)
	xorl	%r14d, %r14d
	movl	$-1, -340(%rbp)
	movl	$0, -344(%rbp)
	.p2align 4,,10
	.p2align 3
.L569:
	movq	-312(%rbp), %rdx
	movq	-328(%rbp), %rdi
	movq	%r15, %rcx
	movl	%r14d, %esi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L535
	movq	-280(%rbp), %rdx
	movl	$8, %ecx
	leaq	.LC30(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L536
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L537
	movl	$12, %edi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L573
	movq	$0, (%rax)
	movq	-320(%rbp), %rdi
	leaq	-128(%rbp), %r12
	leaq	-264(%rbp), %r13
	movl	$0, 8(%rax)
	movq	%rax, -296(%rbp)
	call	uprv_free_67@PLT
	movq	(%r15), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-288(%rbp), %rdx
	movl	$0, -264(%rbp)
	call	*32(%rax)
	movl	-264(%rbp), %ecx
	leaq	-256(%rbp), %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -256(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %edx
	movq	-296(%rbp), %r8
	testw	%dx, %dx
	js	.L539
	movswl	%dx, %eax
	sarl	$5, %eax
	cmpl	$1, %eax
	je	.L669
.L541:
	cmpl	$2, %eax
	jne	.L589
	andl	$2, %edx
	movq	-336(%rbp), %rdx
	cmove	-104(%rbp), %rdx
	movzwl	(%rdx), %eax
	cmpw	$104, %ax
	je	.L670
	cmpw	$75, %ax
	je	.L671
	movl	$-1, %ebx
	cmpw	$72, %ax
	je	.L672
	.p2align 4,,10
	.p2align 3
.L543:
	movl	%ebx, 4(%r8)
	movq	%r12, %rdi
	movq	%r8, -296(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-296(%rbp), %r8
	movl	$2, -344(%rbp)
	movq	%r8, -320(%rbp)
.L547:
	addl	$1, %r14d
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L536:
	movl	$10, %ecx
	movq	%rdx, %rsi
	leaq	.LC31(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L547
	movq	(%r15), %rax
	leaq	-264(%rbp), %r13
	movq	%r15, %rdi
	leaq	-128(%rbp), %r12
	movq	-288(%rbp), %rdx
	movq	%r13, %rsi
	movl	$0, -264(%rbp)
	call	*32(%rax)
	movl	-264(%rbp), %ecx
	leaq	-256(%rbp), %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -256(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L561
	movswl	%ax, %edx
	sarl	$5, %edx
	movl	%edx, -340(%rbp)
	cmpl	$1, -340(%rbp)
	je	.L673
.L563:
	cmpl	$2, -340(%rbp)
	jne	.L613
	testb	$2, %al
	movq	-336(%rbp), %rdx
	cmove	-104(%rbp), %rdx
	movzwl	(%rdx), %eax
	cmpw	$104, %ax
	je	.L674
	cmpw	$75, %ax
	je	.L675
	movl	$-1, %ebx
	movl	%ebx, -340(%rbp)
	cmpw	$72, %ax
	je	.L676
.L565:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L537:
	leaq	-256(%rbp), %rax
	movq	-288(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rax, -304(%rbp)
	movq	%rax, %rdi
	movq	(%r15), %rax
	call	*80(%rax)
	movl	-240(%rbp), %r13d
	leal	2(%r13), %edx
	testl	%edx, %edx
	jle	.L573
	movslq	%edx, %rdx
	leaq	0(,%rdx,4), %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -296(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L573
	leal	1(%r13), %eax
	xorl	%esi, %esi
	movq	%r12, %rdx
	movl	%eax, %ebx
	movl	%eax, -344(%rbp)
	call	memset@PLT
	movq	-320(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpl	$1, %ebx
	jle	.L559
	leal	-1(%r13), %eax
	movl	%r14d, -360(%rbp)
	xorl	%ebx, %ebx
	leaq	-264(%rbp), %r13
	movq	%rax, -320(%rbp)
	leaq	-268(%rbp), %rax
	leaq	-128(%rbp), %r12
	movq	%rax, %r14
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L680:
	movswl	%dx, %eax
	sarl	$5, %eax
	cmpl	$1, %eax
	je	.L677
.L553:
	cmpl	$2, %eax
	jne	.L601
	andl	$2, %edx
	movq	-336(%rbp), %rcx
	cmove	-104(%rbp), %rcx
	movzwl	(%rcx), %edx
	cmpw	$104, %dx
	je	.L678
	cmpw	$75, %dx
	je	.L679
	movl	$-1, %eax
	cmpw	$72, %dx
	jne	.L555
	movzwl	2(%rcx), %edx
	cmpw	$98, %dx
	je	.L608
	cmpw	$66, %dx
	movl	$9, %edx
	cmove	%edx, %eax
.L555:
	movq	-296(%rbp), %rcx
	movq	%r12, %rdi
	movl	%eax, 4(%rcx,%rbx,4)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1(%rbx), %rax
	cmpq	%rbx, -320(%rbp)
	je	.L661
	movq	%rax, %rbx
.L560:
	movq	-304(%rbp), %rdi
	movq	%r15, %rdx
	movl	%ebx, %esi
	call	_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE@PLT
	movq	(%r15), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-288(%rbp), %rdx
	movl	$0, -268(%rbp)
	call	*32(%rax)
	movl	-268(%rbp), %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %edx
	testw	%dx, %dx
	jns	.L680
	movl	-116(%rbp), %eax
	cmpl	$1, %eax
	jne	.L553
.L677:
	andl	$2, %edx
	jne	.L554
	movq	-104(%rbp), %rdx
	movzwl	(%rdx), %edx
	cmpw	$104, %dx
	je	.L626
.L667:
	cmpw	$72, %dx
	je	.L555
	cmpw	$75, %dx
	je	.L625
	xorl	%eax, %eax
	cmpw	$107, %dx
	sete	%al
	leal	-1(,%rax,4), %eax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L678:
	movzwl	2(%rcx), %eax
	cmpw	$98, %ax
	je	.L603
	cmpw	$66, %ax
	movl	$5, %edx
	movl	$-1, %eax
	cmove	%edx, %eax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L679:
	movzwl	2(%rcx), %eax
	cmpw	$98, %ax
	je	.L605
	cmpw	$66, %ax
	sete	%al
	movzbl	%al, %eax
	leal	-1(,%rax,8), %eax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L554:
	movzwl	-118(%rbp), %edx
	cmpw	$104, %dx
	jne	.L667
.L626:
	xorl	%eax, %eax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L573:
	movq	-288(%rbp), %r15
	movl	$7, (%r15)
.L550:
	movq	-320(%rbp), %rdi
	call	uprv_free_67@PLT
.L532:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L681
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L661:
	.cfi_restore_state
	movl	-360(%rbp), %r14d
.L559:
	movq	-296(%rbp), %rax
	movq	%rax, -320(%rbp)
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L539:
	movl	-116(%rbp), %eax
	cmpl	$1, %eax
	jne	.L541
.L669:
	andl	$2, %edx
	je	.L682
	movzwl	-118(%rbp), %eax
	cmpw	$104, %ax
	je	.L543
.L666:
	movl	$1, %ebx
	cmpw	$72, %ax
	je	.L543
	cmpw	$75, %ax
	je	.L623
	xorl	%ebx, %ebx
	cmpw	$107, %ax
	sete	%bl
	leal	-1(,%rbx,4), %ebx
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L672:
	movzwl	2(%rdx), %eax
	cmpw	$98, %ax
	je	.L596
	cmpw	$66, %ax
	movl	$9, %eax
	cmove	%eax, %ebx
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L682:
	movq	-104(%rbp), %rax
	movzwl	(%rax), %eax
	cmpw	$104, %ax
	jne	.L666
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L674:
	movzwl	2(%rdx), %eax
	cmpw	$98, %ax
	je	.L615
	cmpw	$66, %ax
	movl	$-1, %edx
	movl	$5, %eax
	movq	%r12, %rdi
	cmovne	%edx, %eax
	movl	%eax, -340(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L561:
	movl	-116(%rbp), %ebx
	movl	%ebx, -340(%rbp)
	cmpl	$1, -340(%rbp)
	jne	.L563
.L673:
	testb	$2, %al
	jne	.L564
	movq	-104(%rbp), %rax
	movzwl	(%rax), %eax
	cmpw	$104, %ax
	je	.L628
.L668:
	cmpw	$72, %ax
	je	.L565
	cmpw	$75, %ax
	je	.L627
	cmpw	$107, %ax
	movq	%r12, %rdi
	sete	%al
	movzbl	%al, %eax
	leal	-1(,%rax,4), %eax
	movl	%eax, -340(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L670:
	movzwl	2(%rdx), %eax
	cmpw	$98, %ax
	je	.L591
	cmpw	$66, %ax
	movl	$-1, %ebx
	movl	$5, %eax
	cmove	%eax, %ebx
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L671:
	movzwl	2(%rdx), %eax
	cmpw	$98, %ax
	je	.L593
	xorl	%ebx, %ebx
	cmpw	$66, %ax
	sete	%bl
	leal	-1(,%rbx,8), %ebx
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L675:
	movzwl	2(%rdx), %eax
	cmpw	$98, %ax
	je	.L617
	cmpw	$66, %ax
	movq	%r12, %rdi
	sete	%al
	movzbl	%al, %eax
	leal	-1(,%rax,8), %eax
	movl	%eax, -340(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L676:
	movzwl	2(%rdx), %eax
	cmpw	$98, %ax
	je	.L620
	cmpw	$66, %ax
	movl	$9, %eax
	movq	%r12, %rdi
	cmovne	%ebx, %eax
	movl	%eax, -340(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L564:
	movzwl	-118(%rbp), %eax
	cmpw	$104, %ax
	jne	.L668
.L628:
	movl	$0, -340(%rbp)
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L603:
	movl	$4, %eax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L625:
	movl	$2, %eax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L535:
	cmpl	$1, -344(%rbp)
	jle	.L570
	cmpl	$-1, -340(%rbp)
	jne	.L571
	movq	-320(%rbp), %rax
	movl	4(%rax), %eax
	movl	%eax, -340(%rbp)
.L571:
	movq	-320(%rbp), %rax
	movl	-340(%rbp), %ebx
	movl	%ebx, (%rax)
	movslq	-344(%rbp), %rax
	salq	$2, %rax
.L572:
	movq	-320(%rbp), %rdx
	movq	-288(%rbp), %rbx
	movq	-368(%rbp), %rsi
	movq	_ZN6icu_6712_GLOBAL__N_129localeToAllowedHourFormatsMapE(%rip), %rdi
	movl	$-1, (%rdx,%rax)
	movq	%rbx, %rcx
	call	uhash_put_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L622
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	addl	$1, -356(%rbp)
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L601:
	movl	$-1, %eax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L605:
	movl	$6, %eax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L608:
	movl	$8, %eax
	jmp	.L555
.L615:
	movl	$4, -340(%rbp)
	jmp	.L565
.L591:
	movl	$4, %ebx
	jmp	.L543
.L623:
	movl	$2, %ebx
	jmp	.L543
.L570:
	movl	$12, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L573
	movq	$0, (%rax)
	movq	-320(%rbp), %rdi
	movl	$0, 8(%rax)
	call	uprv_free_67@PLT
	movl	-340(%rbp), %esi
	movl	$1, %eax
	movq	%rbx, -320(%rbp)
	cmpl	$-1, %esi
	cmovne	%esi, %eax
	movl	%eax, (%rbx)
	movl	%eax, 4(%rbx)
	movl	$8, %eax
	jmp	.L572
.L589:
	movl	$-1, %ebx
	jmp	.L543
.L593:
	movl	$6, %ebx
	jmp	.L543
.L596:
	movl	$8, %ebx
	jmp	.L543
.L627:
	movl	$2, -340(%rbp)
	jmp	.L565
.L613:
	movl	$-1, -340(%rbp)
	jmp	.L565
.L617:
	movl	$6, -340(%rbp)
	jmp	.L565
.L620:
	movl	$8, -340(%rbp)
	jmp	.L565
.L622:
	movq	$0, -320(%rbp)
	jmp	.L550
.L681:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3578:
	.size	_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, .-_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677PtnElemD0Ev
	.type	_ZN6icu_677PtnElemD0Ev, @function
_ZN6icu_677PtnElemD0Ev:
.LFB3767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_677PtnElemE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	152(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L684
	movq	0(%r13), %rdx
	movq	8(%rdx), %rcx
	leaq	_ZN6icu_677PtnElemD0Ev(%rip), %rdx
	cmpq	%rdx, %rcx
	jne	.L685
	movq	152(%r13), %r14
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L686
	movq	(%r14), %rcx
	movq	8(%rcx), %rcx
	cmpq	%rdx, %rcx
	jne	.L687
	movq	152(%r14), %r15
	movq	%rax, (%r14)
	testq	%r15, %r15
	je	.L688
	movq	(%r15), %rcx
	movq	8(%rcx), %rcx
	cmpq	%rdx, %rcx
	jne	.L689
	movq	152(%r15), %rbx
	movq	%rax, (%r15)
	testq	%rbx, %rbx
	je	.L690
	movq	(%rbx), %rcx
	movq	8(%rcx), %rcx
	cmpq	%rdx, %rcx
	jne	.L691
	movq	152(%rbx), %r8
	movq	%rax, (%rbx)
	testq	%r8, %r8
	je	.L692
	movq	(%r8), %rcx
	movq	8(%rcx), %rcx
	cmpq	%rdx, %rcx
	jne	.L693
	movq	152(%r8), %rdi
	movq	%rax, (%r8)
	testq	%rdi, %rdi
	je	.L694
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L695
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN6icu_677PtnElemD1Ev
	movq	-56(%rbp), %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r8
.L694:
	leaq	80(%r8), %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %r8
	movq	72(%r8), %rdi
	testq	%rdi, %rdi
	je	.L696
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L697
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r8
.L696:
	leaq	8(%r8), %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L692:
	leaq	80(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L698
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L699
	call	_ZN6icu_677UMemorydlEPv@PLT
.L698:
	leaq	8(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L690:
	leaq	80(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.L700
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L701
	call	_ZN6icu_677UMemorydlEPv@PLT
.L700:
	leaq	8(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L688:
	leaq	80(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%r14), %rdi
	testq	%rdi, %rdi
	je	.L702
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L703
	call	_ZN6icu_677UMemorydlEPv@PLT
.L702:
	leaq	8(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L686:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%r13), %rdi
	testq	%rdi, %rdi
	je	.L704
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L705
	call	_ZN6icu_677UMemorydlEPv@PLT
.L704:
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L684:
	leaq	80(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.L706
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L707
	call	_ZN6icu_677UMemorydlEPv@PLT
.L706:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L707:
	.cfi_restore_state
	call	*%rax
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L685:
	movq	%r13, %rdi
	call	*%rcx
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L705:
	call	*%rax
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L687:
	movq	%r14, %rdi
	call	*%rcx
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L689:
	movq	%r15, %rdi
	call	*%rcx
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L703:
	call	*%rax
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L691:
	movq	%rbx, %rdi
	call	*%rcx
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L701:
	call	*%rax
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L699:
	call	*%rax
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L693:
	movq	%r8, %rdi
	call	*%rcx
	jmp	.L692
.L695:
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L694
.L697:
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L696
	.cfi_endproc
.LFE3767:
	.size	_ZN6icu_677PtnElemD0Ev, .-_ZN6icu_677PtnElemD0Ev
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB32:
	.text
.LHOTB32:
	.align 2
	.p2align 4
	.type	_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode.part.0, @function
_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode.part.0:
.LFB5268:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movzbl	424(%rsi), %eax
	movb	%al, 424(%rdi)
	leaq	8(%rsi), %rax
	movq	%rax, -80(%rbp)
	leaq	8(%rdi), %rax
	movq	%rax, -72(%rbp)
	leaq	424(%rdi), %rax
	movq	%rax, -96(%rbp)
.L814:
	movq	-80(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L746
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L787:
	movl	$160, %edi
	movq	%r15, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L747
	leaq	16+_ZTVN6icu_677PtnElemE(%rip), %rax
	leaq	8(%r15), %r13
	movq	%rax, (%r15)
	leaq	8(%rbx), %rsi
	movq	%r13, %rdi
	leaq	80(%r15), %r14
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	$0, 72(%r15)
	movq	%r14, %rdi
	leaq	80(%rbx), %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	(%r12), %edi
	movq	$0, 152(%r15)
	testl	%edi, %edi
	jg	.L748
	movq	72(%rbx), %rdx
	movl	$144, %edi
	movq	%rdx, -64(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L751
	movq	-64(%rbp), %rdx
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	movups	%xmm0, 72(%r8)
	movups	%xmm0, 88(%r8)
	movups	%xmm0, 104(%r8)
	movups	%xmm0, 120(%r8)
	movq	%rax, (%r8)
	movdqu	8(%rdx), %xmm1
	movups	%xmm1, 8(%r8)
	movdqu	24(%rdx), %xmm2
	movups	%xmm2, 24(%r8)
	movdqu	40(%rdx), %xmm3
	movups	%xmm3, 40(%r8)
	movdqu	56(%rdx), %xmm4
	movups	%xmm4, 56(%r8)
	movdqu	72(%rdx), %xmm5
	movups	%xmm5, 72(%r8)
	movdqu	88(%rdx), %xmm6
	movups	%xmm6, 88(%r8)
	movdqu	104(%rdx), %xmm7
	movups	%xmm7, 104(%r8)
	movdqu	120(%rdx), %xmm1
	movups	%xmm1, 120(%r8)
	movzbl	136(%rdx), %eax
	movb	%al, 136(%r8)
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L752
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.L753
.L817:
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rcx
	movq	%r8, -64(%rbp)
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L754
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r8
.L755:
	movq	%r8, 72(%r15)
	testq	%r8, %r8
	je	.L818
.L756:
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L748
	movzbl	144(%rbx), %eax
	movb	%al, 144(%r15)
	movq	-72(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L904
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L760
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L761
	movq	0(%r13), %rax
	leaq	_ZN6icu_677PtnElemD0Ev(%rip), %rcx
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L762
	movq	152(%r13), %r14
	leaq	16+_ZTVN6icu_677PtnElemE(%rip), %rsi
	movq	%rsi, 0(%r13)
	testq	%r14, %r14
	je	.L763
	movq	(%r14), %rax
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L764
	movq	152(%r14), %r8
	movq	%rsi, (%r14)
	testq	%r8, %r8
	je	.L765
	movq	(%r8), %rax
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L766
	movq	152(%r8), %r9
	movq	%rsi, (%r8)
	testq	%r9, %r9
	je	.L767
	movq	(%r9), %rax
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L768
	movq	152(%r9), %r10
	movq	%rsi, (%r9)
	testq	%r10, %r10
	je	.L769
	movq	(%r10), %rax
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L770
	movq	152(%r10), %r11
	movq	%rsi, (%r10)
	testq	%r11, %r11
	je	.L771
	movq	(%r11), %rax
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L772
	movq	152(%r11), %rdi
	movq	%rsi, (%r11)
	testq	%rdi, %rdi
	je	.L773
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L774
	movq	%r11, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN6icu_677PtnElemD1Ev
	movq	-64(%rbp), %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-88(%rbp), %r8
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r11
.L773:
	leaq	80(%r11), %rdi
	movq	%r10, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r11, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r11
	movq	-88(%rbp), %r8
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %r10
	movq	72(%r11), %rdi
	testq	%rdi, %rdi
	je	.L775
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rsi
	movq	%r11, -112(%rbp)
	movq	%r10, -104(%rbp)
	movq	8(%rax), %rax
	movq	%r9, -88(%rbp)
	movq	%r8, -64(%rbp)
	cmpq	%rsi, %rax
	jne	.L776
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r8
	movq	-88(%rbp), %r9
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r11
.L775:
	leaq	8(%r11), %rdi
	movq	%r10, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r11, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-88(%rbp), %r8
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %r10
.L771:
	leaq	80(%r10), %rdi
	movq	%r9, -104(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r10, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r10
	movq	-88(%rbp), %r8
	movq	-104(%rbp), %r9
	movq	72(%r10), %rdi
	testq	%rdi, %rdi
	je	.L777
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rsi
	movq	%r10, -104(%rbp)
	movq	%r9, -88(%rbp)
	movq	8(%rax), %rax
	movq	%r8, -64(%rbp)
	cmpq	%rsi, %rax
	jne	.L778
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r8
	movq	-88(%rbp), %r9
	movq	-104(%rbp), %r10
.L777:
	leaq	8(%r10), %rdi
	movq	%r9, -104(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r10, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-88(%rbp), %r8
	movq	-104(%rbp), %r9
.L769:
	leaq	80(%r9), %rdi
	movq	%r8, -88(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r9
	movq	-88(%rbp), %r8
	movq	72(%r9), %rdi
	testq	%rdi, %rdi
	je	.L779
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rsi
	movq	%r9, -88(%rbp)
	movq	%r8, -64(%rbp)
	movq	8(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L780
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r8
	movq	-88(%rbp), %r9
.L779:
	leaq	8(%r9), %rdi
	movq	%r8, -88(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-88(%rbp), %r8
.L767:
	leaq	80(%r8), %rdi
	movq	%r8, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r8
	movq	72(%r8), %rdi
	testq	%rdi, %rdi
	je	.L781
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rcx
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L782
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r8
.L781:
	leaq	8(%r8), %rdi
	movq	%r8, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L765:
	leaq	80(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%r14), %rdi
	testq	%rdi, %rdi
	je	.L783
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rcx
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L784
	call	_ZN6icu_677UMemorydlEPv@PLT
.L783:
	leaq	8(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L763:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%r13), %rdi
	testq	%rdi, %rdi
	je	.L785
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rcx
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L786
	call	_ZN6icu_677UMemorydlEPv@PLT
.L785:
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L761:
	movq	-56(%rbp), %rax
	movq	%r15, 152(%rax)
.L759:
	movq	152(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L787
.L746:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	addq	$8, -80(%rbp)
	cmpq	%rax, -96(%rbp)
	jne	.L814
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L752:
	movq	%r8, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L756
.L747:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L745
	movl	$7, (%r12)
.L745:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L904:
	.cfi_restore_state
	movq	%r15, (%rax)
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L753:
	movq	%r8, 72(%r15)
	jmp	.L756
.L751:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L748
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L817
	.p2align 4,,10
	.p2align 3
.L818:
	movl	$7, (%r12)
.L748:
	movq	(%r15), %rax
	movq	8(%rax), %rdx
	leaq	_ZN6icu_677PtnElemD0Ev(%rip), %rax
	cmpq	%rax, %rdx
	jne	.L789
	movq	152(%r15), %r12
	leaq	16+_ZTVN6icu_677PtnElemE(%rip), %rcx
	movq	%rcx, (%r15)
	testq	%r12, %r12
	je	.L790
	movq	(%r12), %rdx
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L791
	movq	152(%r12), %rbx
	movq	%rcx, (%r12)
	testq	%rbx, %rbx
	je	.L792
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L793
	movq	152(%rbx), %r10
	movq	%rcx, (%rbx)
	testq	%r10, %r10
	je	.L794
	movq	(%r10), %rdx
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L795
	movq	152(%r10), %r8
	movq	%rcx, (%r10)
	testq	%r8, %r8
	je	.L796
	movq	(%r8), %rdx
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L797
	movq	152(%r8), %r9
	movq	%rcx, (%r8)
	testq	%r9, %r9
	je	.L798
	movq	(%r9), %rdx
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L799
	movq	152(%r9), %rdi
	movq	%rcx, (%r9)
	testq	%rdi, %rdi
	je	.L800
	movq	(%rdi), %rdx
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L801
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN6icu_677PtnElemD1Ev
	movq	-56(%rbp), %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
.L800:
	leaq	80(%r9), %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	movq	72(%r9), %rdi
	testq	%rdi, %rdi
	je	.L802
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	8(%rax), %rax
	movq	%r10, -56(%rbp)
	cmpq	%rdx, %rax
	jne	.L803
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %r9
.L802:
	leaq	8(%r9), %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
.L798:
	leaq	80(%r8), %rdi
	movq	%r10, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r10
	movq	72(%r8), %rdi
	testq	%rdi, %rdi
	je	.L804
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	%r8, -64(%rbp)
	movq	%r10, -56(%rbp)
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L805
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r8
.L804:
	leaq	8(%r8), %rdi
	movq	%r10, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-64(%rbp), %r10
.L796:
	leaq	80(%r10), %rdi
	movq	%r10, -56(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %r10
	movq	72(%r10), %rdi
	testq	%rdi, %rdi
	je	.L806
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L807
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r10
.L806:
	leaq	8(%r10), %rdi
	movq	%r10, -56(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L794:
	leaq	80(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L808
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L809
	call	_ZN6icu_677UMemorydlEPv@PLT
.L808:
	leaq	8(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L792:
	leaq	80(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.L810
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L811
	call	_ZN6icu_677UMemorydlEPv@PLT
.L810:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L790:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.L812
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L813
	call	_ZN6icu_677UMemorydlEPv@PLT
.L812:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$88, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L762:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L754:
	call	*%rax
	movq	-64(%rbp), %r8
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L786:
	call	*%rax
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L764:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L784:
	call	*%rax
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L766:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L768:
	movq	%r8, -64(%rbp)
	movq	%r9, %rdi
	call	*%rax
	movq	-64(%rbp), %r8
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L782:
	movq	%r8, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %r8
	jmp	.L781
.L770:
	movq	%r9, -88(%rbp)
	movq	%r10, %rdi
	movq	%r8, -64(%rbp)
	call	*%rax
	movq	-88(%rbp), %r9
	movq	-64(%rbp), %r8
	jmp	.L769
.L780:
	call	*%rax
	movq	-88(%rbp), %r9
	movq	-64(%rbp), %r8
	jmp	.L779
.L789:
	addq	$88, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
.L791:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L790
.L813:
	call	*%rax
	jmp	.L812
.L778:
	call	*%rax
	movq	-104(%rbp), %r10
	movq	-88(%rbp), %r9
	movq	-64(%rbp), %r8
	jmp	.L777
.L772:
	movq	%r10, -104(%rbp)
	movq	%r11, %rdi
	movq	%r9, -88(%rbp)
	movq	%r8, -64(%rbp)
	call	*%rax
	movq	-104(%rbp), %r10
	movq	-88(%rbp), %r9
	movq	-64(%rbp), %r8
	jmp	.L771
.L793:
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L792
.L811:
	call	*%rax
	jmp	.L810
.L774:
	movq	%r11, -112(%rbp)
	movq	%r10, -104(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r8, -64(%rbp)
	call	*%rax
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %r10
	movq	-88(%rbp), %r9
	movq	-64(%rbp), %r8
	jmp	.L773
.L776:
	call	*%rax
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %r10
	movq	-88(%rbp), %r9
	movq	-64(%rbp), %r8
	jmp	.L775
.L795:
	movq	%r10, %rdi
	call	*%rdx
	jmp	.L794
.L809:
	call	*%rax
	jmp	.L808
.L797:
	movq	%r10, -56(%rbp)
	movq	%r8, %rdi
	call	*%rdx
	movq	-56(%rbp), %r10
	jmp	.L796
.L807:
	movq	%r10, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r10
	jmp	.L806
.L799:
	movq	%r8, -64(%rbp)
	movq	%r9, %rdi
	movq	%r10, -56(%rbp)
	call	*%rdx
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r10
	jmp	.L798
.L805:
	call	*%rax
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r10
	jmp	.L804
.L803:
	call	*%rax
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r10
	jmp	.L802
.L801:
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	*%rdx
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r10
	jmp	.L800
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode.part.0.cold, @function
_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode.part.0.cold:
.LFSB5268:
.L760:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE5268:
	.text
	.size	_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode.part.0, .-_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode.part.0
	.section	.text.unlikely
	.size	_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode.part.0.cold, .-_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode.part.0.cold
.LCOLDE32:
	.text
.LHOTE32:
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB4307:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4307:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB4310:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L918
	ret
	.p2align 4,,10
	.p2align 3
.L918:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L906
	cmpb	$0, 12(%rbx)
	jne	.L919
.L910:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L906:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L919:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L910
	.cfi_endproc
.LFE4310:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB4313:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L922
	ret
	.p2align 4,,10
	.p2align 3
.L922:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4313:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB4316:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L925
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L925:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE4316:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB4318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L931
.L927:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L932
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L931:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L932:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4318:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB4319:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4319:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB4320:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4320:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB4321:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4321:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4322:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4322:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4323:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4323:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB4324:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L948
	testl	%edx, %edx
	jle	.L948
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L951
.L940:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L948:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L951:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L940
	.cfi_endproc
.LFE4324:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB4325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L955
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L955
	testl	%r12d, %r12d
	jg	.L962
	cmpb	$0, 12(%rbx)
	jne	.L963
.L957:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L962:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L957
.L963:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L955:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4325:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB4326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L965
	movq	(%rdi), %r8
.L966:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L965:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L969
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L969
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L969:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4326:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB4327:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L976
	ret
	.p2align 4,,10
	.p2align 3
.L976:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4327:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB4328:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4328:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB4329:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4329:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB4330:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4330:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB4332:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4332:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB4334:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4334:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator16getStaticClassIDEv
	.type	_ZN6icu_6724DateTimePatternGenerator16getStaticClassIDEv, @function
_ZN6icu_6724DateTimePatternGenerator16getStaticClassIDEv:
.LFB3545:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6724DateTimePatternGenerator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3545:
	.size	_ZN6icu_6724DateTimePatternGenerator16getStaticClassIDEv, .-_ZN6icu_6724DateTimePatternGenerator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721DTSkeletonEnumeration16getStaticClassIDEv
	.type	_ZN6icu_6721DTSkeletonEnumeration16getStaticClassIDEv, @function
_ZN6icu_6721DTSkeletonEnumeration16getStaticClassIDEv:
.LFB3547:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6721DTSkeletonEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3547:
	.size	_ZN6icu_6721DTSkeletonEnumeration16getStaticClassIDEv, .-_ZN6icu_6721DTSkeletonEnumeration16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722DTRedundantEnumeration16getStaticClassIDEv
	.type	_ZN6icu_6722DTRedundantEnumeration16getStaticClassIDEv, @function
_ZN6icu_6722DTRedundantEnumeration16getStaticClassIDEv:
.LFB3549:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6722DTRedundantEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3549:
	.size	_ZN6icu_6722DTRedundantEnumeration16getStaticClassIDEv, .-_ZN6icu_6722DTRedundantEnumeration16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGeneratorC2ER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGeneratorC2ER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGeneratorC2ER10UErrorCode:
.LFB3555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724DateTimePatternGeneratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movl	$2, %eax
	movl	$2, %edi
	movl	$2, %r8d
	movw	%ax, 592(%rbx)
	movl	$2, %eax
	movl	$2, %r9d
	movl	$2, %r10d
	movw	%ax, 656(%rbx)
	movl	$2, %eax
	movl	$2, %r11d
	movl	$2, %edx
	movw	%ax, 720(%rbx)
	movl	$2, %eax
	pxor	%xmm0, %xmm0
	movl	$2, %ecx
	movw	%ax, 784(%rbx)
	movl	$2, %eax
	movl	$2, %esi
	movw	%di, 272(%rbx)
	movl	$2, %edi
	movw	%r8w, 336(%rbx)
	movl	$2, %r8d
	movw	%r9w, 400(%rbx)
	movl	$2, %r9d
	movw	%ax, 848(%rbx)
	movl	$2, %eax
	movw	%r10w, 464(%rbx)
	movl	$2, %r10d
	movw	%r11w, 528(%rbx)
	movl	$2, %r11d
	movw	%dx, 912(%rbx)
	movl	$2, %edx
	movw	%cx, 976(%rbx)
	movl	$2, %ecx
	movw	%si, 1040(%rbx)
	movl	$2, %esi
	movw	%di, 1104(%rbx)
	movl	$2, %edi
	movw	%r8w, 1168(%rbx)
	movl	$2, %r8d
	movw	%r9w, 1232(%rbx)
	movl	$2, %r9d
	movq	%r12, 264(%rbx)
	movq	%r12, 328(%rbx)
	movq	%r12, 392(%rbx)
	movq	%r12, 456(%rbx)
	movq	%r12, 520(%rbx)
	movq	%r12, 584(%rbx)
	movq	%r12, 648(%rbx)
	movq	%r12, 712(%rbx)
	movq	%r12, 776(%rbx)
	movq	%r12, 840(%rbx)
	movq	%r12, 904(%rbx)
	movq	%r12, 968(%rbx)
	movq	%r12, 1032(%rbx)
	movq	%r12, 1096(%rbx)
	movq	%r12, 1160(%rbx)
	movq	%r12, 1224(%rbx)
	movq	%r12, 1288(%rbx)
	movw	%ax, 1424(%rbx)
	movl	$2, %eax
	movw	%ax, 1488(%rbx)
	movl	$2, %eax
	movw	%ax, 1552(%rbx)
	movl	$2, %eax
	movw	%ax, 1616(%rbx)
	movl	$2, %eax
	movw	%ax, 1680(%rbx)
	movl	$2, %eax
	movw	%r10w, 1296(%rbx)
	movl	$2, %r10d
	movw	%r11w, 1360(%rbx)
	movl	$2, %r11d
	movw	%ax, 2256(%rbx)
	movl	$2, %eax
	movw	%dx, 1744(%rbx)
	movl	$2, %edx
	movw	%cx, 1808(%rbx)
	movl	$2, %ecx
	movw	%si, 1872(%rbx)
	movl	$2, %esi
	movw	%di, 1936(%rbx)
	movl	$2, %edi
	movw	%r8w, 2000(%rbx)
	movl	$2, %r8d
	movw	%r9w, 2064(%rbx)
	movl	$2, %r9d
	movw	%r10w, 2128(%rbx)
	movl	$2, %r10d
	movw	%r11w, 2192(%rbx)
	movl	$2, %r11d
	movq	%r12, 1352(%rbx)
	movq	%r12, 1416(%rbx)
	movq	%r12, 1480(%rbx)
	movq	%r12, 1544(%rbx)
	movq	%r12, 1608(%rbx)
	movq	%r12, 1672(%rbx)
	movq	%r12, 1736(%rbx)
	movq	%r12, 1800(%rbx)
	movq	%r12, 1864(%rbx)
	movq	%r12, 1928(%rbx)
	movq	%r12, 1992(%rbx)
	movq	%r12, 2056(%rbx)
	movq	%r12, 2120(%rbx)
	movq	%r12, 2184(%rbx)
	movq	%r12, 2248(%rbx)
	movq	%r12, 2312(%rbx)
	movw	%ax, 2320(%rbx)
	movl	$2, %eax
	movw	%ax, 2384(%rbx)
	movl	$2, %eax
	movw	%ax, 2448(%rbx)
	movl	$2, %eax
	movw	%ax, 2512(%rbx)
	movl	$2, %eax
	movw	%ax, 3088(%rbx)
	movl	$2, %eax
	movw	%ax, 3152(%rbx)
	movl	$2, %eax
	movw	%ax, 3216(%rbx)
	movl	$2, %eax
	movw	%ax, 3280(%rbx)
	movl	$2, %eax
	movw	%ax, 3344(%rbx)
	movl	$2, %eax
	movw	%dx, 2576(%rbx)
	movl	$2, %edx
	movw	%cx, 2640(%rbx)
	movl	$2, %ecx
	movw	%si, 2704(%rbx)
	movl	$2, %esi
	movw	%di, 2768(%rbx)
	movl	$2, %edi
	movw	%r8w, 2832(%rbx)
	movl	$2, %r8d
	movw	%r9w, 2896(%rbx)
	movl	$2, %r9d
	movw	%r10w, 2960(%rbx)
	movl	$2, %r10d
	movw	%r11w, 3024(%rbx)
	movl	$2, %r11d
	movq	%r12, 2376(%rbx)
	movq	%r12, 2440(%rbx)
	movq	%r12, 2504(%rbx)
	movq	%r12, 2568(%rbx)
	movq	%r12, 2632(%rbx)
	movq	%r12, 2696(%rbx)
	movq	%r12, 2760(%rbx)
	movq	%r12, 2824(%rbx)
	movq	%r12, 2888(%rbx)
	movq	%r12, 2952(%rbx)
	movq	%r12, 3016(%rbx)
	movq	%r12, 3080(%rbx)
	movq	%r12, 3144(%rbx)
	movq	%r12, 3208(%rbx)
	movq	%r12, 3272(%rbx)
	movq	%r12, 3336(%rbx)
	movq	%r12, 3400(%rbx)
	movw	%ax, 3920(%rbx)
	movl	$2, %eax
	movw	%ax, 3984(%rbx)
	movl	$2, %eax
	movw	%ax, 4048(%rbx)
	movl	$2, %eax
	movw	%dx, 3408(%rbx)
	movl	$2, %edx
	movw	%cx, 3472(%rbx)
	movl	$2, %ecx
	movw	%si, 3536(%rbx)
	movl	$2, %esi
	movw	%di, 3600(%rbx)
	movl	$2, %edi
	movw	%r8w, 3664(%rbx)
	movl	$2, %r8d
	movw	%r9w, 3728(%rbx)
	xorl	%r9d, %r9d
	movw	%ax, 4112(%rbx)
	movl	$2, %eax
	movq	%r12, 3464(%rbx)
	movq	%r12, 3528(%rbx)
	movq	%r12, 3592(%rbx)
	movq	%r12, 3656(%rbx)
	movq	%r12, 3720(%rbx)
	movq	%r12, 3784(%rbx)
	movw	%r10w, 3792(%rbx)
	movq	%r12, 3848(%rbx)
	movw	%r11w, 3856(%rbx)
	movq	%r12, 3912(%rbx)
	movq	%r12, 3976(%rbx)
	movq	%r12, 4040(%rbx)
	movq	%r12, 4104(%rbx)
	movq	%r12, 4168(%rbx)
	movw	%ax, 4176(%rbx)
	movq	%r12, 4232(%rbx)
	movw	%dx, 4240(%rbx)
	movq	%r12, 4296(%rbx)
	movw	%cx, 4304(%rbx)
	movq	%r12, 4360(%rbx)
	movw	%si, 4368(%rbx)
	movq	%r12, 4424(%rbx)
	movw	%di, 4432(%rbx)
	movl	$3216, %edi
	movq	%r12, 4504(%rbx)
	movw	%r8w, 4512(%rbx)
	movw	%r9w, 4568(%rbx)
	movl	$0, 4600(%rbx)
	movups	%xmm0, 4488(%rbx)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L986
	leaq	16+_ZTVN6icu_6712FormatParserE(%rip), %rsi
	leaq	8(%rax), %rdx
	movq	%rsi, (%rax)
	leaq	3208(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L987:
	movl	$2, %esi
	movq	%r12, (%rdx)
	addq	$64, %rdx
	movw	%si, -56(%rdx)
	cmpq	%rcx, %rdx
	jne	.L987
	movq	$0, 3208(%rax)
.L986:
	movq	%rax, 232(%rbx)
	movl	$152, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L988
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rdx
	leaq	16+_ZTVN6icu_6715DateTimeMatcherE(%rip), %rsi
	movb	$0, 144(%rax)
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
.L988:
	movq	%rax, 240(%rbx)
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L989
	leaq	16+_ZTVN6icu_6712DistanceInfoE(%rip), %rsi
	movq	%rsi, (%rax)
.L989:
	movq	%rax, 248(%rbx)
	movl	$432, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L990
	leaq	16+_ZTVN6icu_6710PatternMapE(%rip), %rax
	leaq	16(%rdx), %rdi
	movq	$0, 8(%rdx)
	movq	%rax, (%rdx)
	andq	$-8, %rdi
	movl	%edx, %eax
	movq	$0, 416(%rdx)
	subl	%edi, %eax
	leal	424(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movb	$1, 424(%rdx)
.L990:
	cmpq	$0, 232(%rbx)
	movq	%rdx, 256(%rbx)
	je	.L991
	cmpq	$0, 240(%rbx)
	je	.L991
	cmpq	$0, 248(%rbx)
	je	.L991
	testq	%rdx, %rdx
	jne	.L985
	.p2align 4,,10
	.p2align 3
.L991:
	movl	$7, 0(%r13)
	movl	$7, 4600(%rbx)
.L985:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3555:
	.size	_ZN6icu_6724DateTimePatternGeneratorC2ER10UErrorCode, .-_ZN6icu_6724DateTimePatternGeneratorC2ER10UErrorCode
	.globl	_ZN6icu_6724DateTimePatternGeneratorC1ER10UErrorCode
	.set	_ZN6icu_6724DateTimePatternGeneratorC1ER10UErrorCode,_ZN6icu_6724DateTimePatternGeneratorC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGeneratoreqERKS0_
	.type	_ZNK6icu_6724DateTimePatternGeneratoreqERKS0_, @function
_ZNK6icu_6724DateTimePatternGeneratoreqERKS0_:
.LFB3566:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L1121
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	8(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	je	.L1019
	movq	256(%rbx), %rdi
	movq	256(%r12), %rsi
	cmpq	%rsi, %rdi
	je	.L1018
	call	_ZNK6icu_6710PatternMap6equalsERKS0_.part.0
	testb	%al, %al
	je	.L1019
.L1018:
	movswl	4368(%r12), %eax
	movswl	4368(%rbx), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L1016
	testw	%dx, %dx
	js	.L1020
	sarl	$5, %edx
.L1021:
	testw	%ax, %ax
	js	.L1022
	sarl	$5, %eax
.L1023:
	cmpl	%edx, %eax
	jne	.L1019
	testb	%cl, %cl
	je	.L1122
.L1019:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1122:
	.cfi_restore_state
	leaq	4360(%r12), %rsi
	leaq	4360(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L1016:
	testb	%cl, %cl
	je	.L1019
	movswl	4432(%r12), %ecx
	movzwl	4432(%rbx), %eax
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L1024
	testw	%ax, %ax
	js	.L1025
	movswl	%ax, %edx
	sarl	$5, %edx
.L1026:
	testw	%cx, %cx
	js	.L1027
	sarl	$5, %ecx
.L1028:
	cmpl	%edx, %ecx
	jne	.L1019
	testb	%sil, %sil
	jne	.L1019
	leaq	4424(%r12), %rsi
	leaq	4424(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L1024:
	testb	%sil, %sil
	je	.L1019
	leaq	264(%rbx), %r15
	leaq	264(%r12), %r14
	addq	$1288, %r12
	leaq	1288(%rbx), %r13
	addq	$4360, %rbx
.L1056:
	movswl	8(%r14), %eax
	movswl	8(%r15), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L1029
	testw	%dx, %dx
	js	.L1030
	sarl	$5, %edx
.L1031:
	testw	%ax, %ax
	js	.L1032
	sarl	$5, %eax
.L1033:
	testb	%cl, %cl
	jne	.L1019
	cmpl	%edx, %eax
	jne	.L1019
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L1029:
	testb	%cl, %cl
	je	.L1019
	movzwl	8(%r12), %eax
	movzwl	8(%r13), %ecx
	movl	%eax, %esi
	andl	$1, %esi
	testb	$1, %cl
	jne	.L1034
	movswl	%cx, %edx
	sarl	$5, %edx
	testw	%cx, %cx
	jns	.L1040
	movl	12(%r13), %edx
.L1040:
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testw	%ax, %ax
	jns	.L1039
	movl	12(%r12), %ecx
.L1039:
	testb	%sil, %sil
	jne	.L1019
	cmpl	%ecx, %edx
	jne	.L1019
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L1034:
	testb	%sil, %sil
	je	.L1019
	movzwl	72(%r12), %eax
	movzwl	72(%r13), %ecx
	leaq	64(%r12), %rsi
	leaq	64(%r13), %rdi
	movl	%eax, %r8d
	andl	$1, %r8d
	testb	$1, %cl
	jne	.L1041
	movswl	%cx, %edx
	sarl	$5, %edx
	testw	%cx, %cx
	jns	.L1047
	movl	76(%r13), %edx
.L1047:
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testw	%ax, %ax
	jns	.L1046
	movl	76(%r12), %ecx
.L1046:
	cmpl	%ecx, %edx
	jne	.L1019
	testb	%r8b, %r8b
	jne	.L1019
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r8b
	.p2align 4,,10
	.p2align 3
.L1041:
	testb	%r8b, %r8b
	je	.L1019
	movzwl	136(%r12), %eax
	movzwl	136(%r13), %ecx
	leaq	128(%r12), %rsi
	leaq	128(%r13), %rdi
	movl	%eax, %r8d
	andl	$1, %r8d
	testb	$1, %cl
	jne	.L1048
	movswl	%cx, %edx
	sarl	$5, %edx
	testw	%cx, %cx
	jns	.L1054
	movl	140(%r13), %edx
.L1054:
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testw	%ax, %ax
	jns	.L1053
	movl	140(%r12), %ecx
.L1053:
	cmpl	%ecx, %edx
	jne	.L1019
	testb	%r8b, %r8b
	jne	.L1019
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r8b
	.p2align 4,,10
	.p2align 3
.L1048:
	testb	%r8b, %r8b
	je	.L1019
	addq	$192, %r13
	addq	$64, %r15
	addq	$64, %r14
	addq	$192, %r12
	cmpq	%r13, %rbx
	jne	.L1056
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1121:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1032:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	12(%r14), %eax
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1030:
	movl	12(%r15), %edx
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1022:
	movl	4372(%r12), %eax
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1020:
	movl	4372(%rbx), %edx
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1025:
	movl	4436(%rbx), %edx
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1027:
	movl	4436(%r12), %ecx
	jmp	.L1028
	.cfi_endproc
.LFE3566:
	.size	_ZNK6icu_6724DateTimePatternGeneratoreqERKS0_, .-_ZNK6icu_6724DateTimePatternGeneratoreqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGeneratorneERKS0_
	.type	_ZNK6icu_6724DateTimePatternGeneratorneERKS0_, @function
_ZNK6icu_6724DateTimePatternGeneratorneERKS0_:
.LFB3567:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L1233
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	8(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	je	.L1131
	movq	256(%rbx), %rdi
	movq	256(%r12), %rsi
	cmpq	%rsi, %rdi
	je	.L1130
	call	_ZNK6icu_6710PatternMap6equalsERKS0_.part.0
	testb	%al, %al
	je	.L1131
.L1130:
	movswl	4368(%r12), %eax
	movswl	4368(%rbx), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L1128
	testw	%dx, %dx
	js	.L1132
	sarl	$5, %edx
.L1133:
	testw	%ax, %ax
	js	.L1134
	sarl	$5, %eax
.L1135:
	testb	%cl, %cl
	jne	.L1131
	cmpl	%edx, %eax
	je	.L1234
.L1131:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1234:
	.cfi_restore_state
	leaq	4360(%r12), %rsi
	leaq	4360(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L1128:
	testb	%cl, %cl
	je	.L1131
	movswl	4432(%r12), %ecx
	movzwl	4432(%rbx), %eax
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L1136
	testw	%ax, %ax
	js	.L1137
	movswl	%ax, %edx
	sarl	$5, %edx
.L1138:
	testw	%cx, %cx
	js	.L1139
	sarl	$5, %ecx
.L1140:
	testb	%sil, %sil
	jne	.L1131
	cmpl	%edx, %ecx
	jne	.L1131
	leaq	4424(%r12), %rsi
	leaq	4424(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L1136:
	testb	%sil, %sil
	je	.L1131
	leaq	264(%rbx), %r15
	leaq	264(%r12), %r14
	addq	$1288, %r12
	leaq	1288(%rbx), %r13
	addq	$4360, %rbx
.L1168:
	movswl	8(%r14), %eax
	movswl	8(%r15), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L1141
	testw	%dx, %dx
	js	.L1142
	sarl	$5, %edx
.L1143:
	testw	%ax, %ax
	js	.L1144
	sarl	$5, %eax
.L1145:
	testb	%cl, %cl
	jne	.L1131
	cmpl	%edx, %eax
	jne	.L1131
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L1141:
	testb	%cl, %cl
	je	.L1131
	movzwl	8(%r12), %eax
	movzwl	8(%r13), %ecx
	movl	%eax, %esi
	andl	$1, %esi
	testb	$1, %cl
	jne	.L1146
	movswl	%cx, %edx
	sarl	$5, %edx
	testw	%cx, %cx
	jns	.L1152
	movl	12(%r13), %edx
.L1152:
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testw	%ax, %ax
	jns	.L1151
	movl	12(%r12), %ecx
.L1151:
	cmpl	%ecx, %edx
	jne	.L1131
	testb	%sil, %sil
	jne	.L1131
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L1146:
	testb	%sil, %sil
	je	.L1131
	movzwl	72(%r12), %eax
	movzwl	72(%r13), %ecx
	leaq	64(%r12), %r8
	leaq	64(%r13), %rdi
	movl	%eax, %esi
	andl	$1, %esi
	testb	$1, %cl
	jne	.L1153
	movswl	%cx, %edx
	sarl	$5, %edx
	testw	%cx, %cx
	jns	.L1159
	movl	76(%r13), %edx
.L1159:
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testw	%ax, %ax
	jns	.L1158
	movl	76(%r12), %ecx
.L1158:
	testb	%sil, %sil
	jne	.L1131
	cmpl	%ecx, %edx
	jne	.L1131
	movq	%r8, %rsi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L1153:
	testb	%sil, %sil
	je	.L1131
	movzwl	136(%r12), %eax
	movzwl	136(%r13), %ecx
	leaq	128(%r12), %r8
	leaq	128(%r13), %rdi
	movl	%eax, %esi
	andl	$1, %esi
	testb	$1, %cl
	jne	.L1160
	movswl	%cx, %edx
	sarl	$5, %edx
	testw	%cx, %cx
	jns	.L1166
	movl	140(%r13), %edx
.L1166:
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testw	%ax, %ax
	jns	.L1165
	movl	140(%r12), %ecx
.L1165:
	testb	%sil, %sil
	jne	.L1131
	cmpl	%ecx, %edx
	jne	.L1131
	movq	%r8, %rsi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L1160:
	testb	%sil, %sil
	je	.L1131
	addq	$192, %r13
	addq	$64, %r15
	addq	$64, %r14
	addq	$192, %r12
	cmpq	%r13, %rbx
	jne	.L1168
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1233:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1144:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	12(%r14), %eax
	jmp	.L1145
	.p2align 4,,10
	.p2align 3
.L1142:
	movl	12(%r15), %edx
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1134:
	movl	4372(%r12), %eax
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L1132:
	movl	4372(%rbx), %edx
	jmp	.L1133
	.p2align 4,,10
	.p2align 3
.L1137:
	movl	4436(%rbx), %edx
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1139:
	movl	4436(%r12), %ecx
	jmp	.L1140
	.cfi_endproc
.LFE3567:
	.size	_ZNK6icu_6724DateTimePatternGeneratorneERKS0_, .-_ZNK6icu_6724DateTimePatternGeneratorneERKS0_
	.section	.rodata.str1.1
.LC33:
	.string	"supplementalData"
.LC34:
	.string	"timeData"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator26loadAllowedHourFormatsDataER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator26loadAllowedHourFormatsDataER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator26loadAllowedHourFormatsDataER10UErrorCode:
.LFB3584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L1250
.L1235:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1251
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1250:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	%rdi, %rcx
	movq	uhash_compareChars_67@GOTPCREL(%rip), %rsi
	xorl	%edx, %edx
	movq	uhash_hashChars_67@GOTPCREL(%rip), %rdi
	call	uhash_open_67@PLT
	movl	(%r12), %edx
	movq	%rax, _ZN6icu_6712_GLOBAL__N_129localeToAllowedHourFormatsMapE(%rip)
	testl	%edx, %edx
	jg	.L1235
	leaq	deleteAllowedHourFormats_67(%rip), %rsi
	movq	%rax, %rdi
	call	uhash_setValueDeleter_67@PLT
	leaq	allowedHourFormatsCleanup_67(%rip), %rsi
	movl	$23, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%r12, %rdx
	leaq	.LC33(%rip), %rsi
	xorl	%edi, %edi
	call	ures_openDirect_67@PLT
	movq	%rax, %r13
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L1249
	leaq	-48(%rbp), %r14
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE(%rip), %rbx
	movq	%r13, %rdi
	movq	%r12, %rcx
	movq	%r14, %rdx
	leaq	.LC34(%rip), %rsi
	movq	%rbx, -48(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	movq	%r14, %rdi
	movq	%rbx, -48(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
.L1249:
	testq	%r13, %r13
	je	.L1235
	movq	%r13, %rdi
	call	ures_close_67@PLT
	jmp	.L1235
.L1251:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3584:
	.size	_ZN6icu_6724DateTimePatternGenerator26loadAllowedHourFormatsDataER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator26loadAllowedHourFormatsDataER10UErrorCode
	.section	.text.unlikely
	.align 2
.LCOLDB35:
	.text
.LHOTB35:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator19getDefaultHourCycleER10UErrorCode
	.type	_ZNK6icu_6724DateTimePatternGenerator19getDefaultHourCycleER10UErrorCode, @function
_ZNK6icu_6724DateTimePatternGenerator19getDefaultHourCycleER10UErrorCode:
.LFB3587:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1257
	movzwl	4568(%rdi), %eax
	testw	%ax, %ax
	je	.L1266
	cmpw	$104, %ax
	je	.L1258
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	ja	.L1255
	cmpw	$72, %ax
	je	.L1259
	cmpw	$75, %ax
	jne	.L1267
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1255:
	.cfi_restore_state
	cmpw	$107, %ax
	jne	.L1256
	movl	$3, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1259:
	.cfi_restore_state
	movl	$2, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1257:
	.cfi_restore 6
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1258:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1266:
	movl	$16, (%rsi)
	movl	$2, %eax
	ret
.L1267:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	jmp	.L1256
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_6724DateTimePatternGenerator19getDefaultHourCycleER10UErrorCode.cold, @function
_ZNK6icu_6724DateTimePatternGenerator19getDefaultHourCycleER10UErrorCode.cold:
.LFSB3587:
.L1256:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3587:
	.text
	.size	_ZNK6icu_6724DateTimePatternGenerator19getDefaultHourCycleER10UErrorCode, .-_ZNK6icu_6724DateTimePatternGenerator19getDefaultHourCycleER10UErrorCode
	.section	.text.unlikely
	.size	_ZNK6icu_6724DateTimePatternGenerator19getDefaultHourCycleER10UErrorCode.cold, .-_ZNK6icu_6724DateTimePatternGenerator19getDefaultHourCycleER10UErrorCode.cold
.LCOLDE35:
	.text
.LHOTE35:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator20getCalendarTypeToUseERKNS_6LocaleERNS_10CharStringER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator20getCalendarTypeToUseERKNS_6LocaleERNS_10CharStringER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator20getCalendarTypeToUseERKNS_6LocaleERNS_10CharStringER10UErrorCode:
.LFB3594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	leaq	_ZN6icu_67L23DT_DateTimeGregorianTagE(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	movq	(%rdx), %rax
	movl	$0, 56(%rdx)
	movl	$-1, %edx
	movb	$0, (%rax)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L1271
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1271:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6724DateTimePatternGenerator20getCalendarTypeToUseERKNS_6LocaleERNS_10CharStringER10UErrorCode.part.0
	.cfi_endproc
.LFE3594:
	.size	_ZN6icu_6724DateTimePatternGenerator20getCalendarTypeToUseERKNS_6LocaleERNS_10CharStringER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator20getCalendarTypeToUseERKNS_6LocaleERNS_10CharStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator11addCLDRDataERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator11addCLDRDataERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator11addCLDRDataERKNS_6LocaleER10UErrorCode:
.LFB3622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -552(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L1334
.L1272:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1335
	addq	$600, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1334:
	.cfi_restore_state
	movl	$2, %eax
	movq	%rsi, %r14
	movq	40(%rsi), %rsi
	xorl	%edi, %edi
	movw	%ax, -392(%rbp)
	movl	$2, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r13
	movq	%rdx, %rbx
	movw	%ax, -328(%rbp)
	leaq	-272(%rbp), %rax
	movl	$2, %r15d
	movq	%rax, -568(%rbp)
	leaq	-259(%rbp), %rax
	movq	%rax, -272(%rbp)
	xorl	%eax, %eax
	movq	%r13, -464(%rbp)
	movw	%r15w, -456(%rbp)
	movq	%r13, -400(%rbp)
	movq	%r13, -336(%rbp)
	movl	$0, -216(%rbp)
	movl	$40, -264(%rbp)
	movw	%ax, -260(%rbp)
	call	ures_open_67@PLT
	movq	%rax, -560(%rbp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1336
.L1274:
	movq	-560(%rbp), %rax
	testq	%rax, %rax
	je	.L1313
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L1313:
	cmpb	$0, -260(%rbp)
	jne	.L1337
.L1314:
	leaq	-336(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-400(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-464(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1336:
	leaq	-208(%rbp), %r15
	leaq	-195(%rbp), %rax
	xorl	%r11d, %r11d
	movq	%rbx, %rcx
	movl	$-1, %edx
	leaq	_ZN6icu_67L23DT_DateTimeGregorianTagE(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -208(%rbp)
	movl	$0, -152(%rbp)
	movl	$40, -200(%rbp)
	movw	%r11w, -196(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	(%rbx), %r12d
	testl	%r12d, %r12d
	jle	.L1338
.L1276:
	cmpb	$0, -196(%rbp)
	je	.L1274
	movq	-208(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L1274
	.p2align 4,,10
	.p2align 3
.L1337:
	movq	-272(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1338:
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator20getCalendarTypeToUseERKNS_6LocaleERNS_10CharStringER10UErrorCode.part.0
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L1276
	leaq	16+_ZTVN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE(%rip), %rax
	leaq	_ZN6icu_67L22DT_DateTimeCalendarTagE(%rip), %rsi
	movl	$0, -532(%rbp)
	movq	%rax, -528(%rbp)
	movq	-552(%rbp), %rax
	movl	$0, -216(%rbp)
	movq	%rax, -520(%rbp)
	movq	-272(%rbp), %rax
	movb	$0, (%rax)
	leaq	-496(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-488(%rbp), %edx
	movq	-496(%rbp), %rsi
	movq	%rbx, %rcx
	movq	-568(%rbp), %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%rbx, %rdx
	movl	$47, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-152(%rbp), %edx
	movq	-208(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%rax, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%rbx, %rdx
	movl	$47, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	leaq	_ZN6icu_67L25DT_DateTimeAppendItemsTagE(%rip), %rsi
	movq	%rax, %r14
	leaq	-480(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -576(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-472(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movq	-480(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	(%rbx), %r9d
	leaq	-528(%rbp), %rax
	movq	%rax, -600(%rbp)
	testl	%r9d, %r9d
	jle	.L1339
.L1278:
	movq	-600(%rbp), %rdi
	leaq	16+_ZTVN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE(%rip), %rax
	movq	%rax, -528(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	-272(%rbp), %rsi
	movq	-560(%rbp), %rdi
	movq	%rax, %rdx
	leaq	-532(%rbp), %rax
	movq	%rax, %rcx
	movq	%rax, -616(%rbp)
	leaq	-144(%rbp), %r15
	call	ures_getAllItemsWithFallback_67@PLT
	movq	-576(%rbp), %rdx
	movl	$14, %ecx
	movq	%r15, %rdi
	leaq	_ZN6icu_67L17UDATPG_ItemFormatE(%rip), %rax
	movl	$1, %esi
	movq	%rax, -480(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-520(%rbp), %rax
	movswl	272(%rax), %edx
	leaq	264(%rax), %r14
	shrl	$5, %edx
	je	.L1340
.L1279:
	movswl	336(%rax), %edx
	leaq	328(%rax), %r14
	shrl	$5, %edx
	je	.L1341
.L1280:
	movswl	400(%rax), %edx
	leaq	392(%rax), %r14
	shrl	$5, %edx
	je	.L1342
.L1281:
	movswl	464(%rax), %edx
	leaq	456(%rax), %r14
	shrl	$5, %edx
	je	.L1343
.L1282:
	movswl	528(%rax), %edx
	leaq	520(%rax), %r14
	shrl	$5, %edx
	je	.L1344
.L1283:
	movswl	592(%rax), %edx
	leaq	584(%rax), %r14
	shrl	$5, %edx
	je	.L1345
.L1284:
	movswl	656(%rax), %edx
	leaq	648(%rax), %r14
	shrl	$5, %edx
	je	.L1346
.L1285:
	movswl	720(%rax), %edx
	leaq	712(%rax), %r14
	shrl	$5, %edx
	je	.L1347
.L1286:
	movswl	784(%rax), %edx
	leaq	776(%rax), %r14
	shrl	$5, %edx
	je	.L1348
.L1287:
	movswl	848(%rax), %edx
	leaq	840(%rax), %r14
	shrl	$5, %edx
	je	.L1349
.L1288:
	movswl	912(%rax), %edx
	leaq	904(%rax), %r14
	shrl	$5, %edx
	je	.L1350
.L1289:
	movswl	976(%rax), %edx
	leaq	968(%rax), %r14
	shrl	$5, %edx
	je	.L1351
.L1290:
	movswl	1040(%rax), %edx
	leaq	1032(%rax), %r14
	shrl	$5, %edx
	je	.L1352
.L1291:
	movswl	1104(%rax), %edx
	leaq	1096(%rax), %r14
	shrl	$5, %edx
	je	.L1353
.L1292:
	movswl	1168(%rax), %edx
	leaq	1160(%rax), %r14
	shrl	$5, %edx
	je	.L1354
.L1293:
	leaq	1224(%rax), %r14
	movswl	1232(%rax), %eax
	shrl	$5, %eax
	je	.L1355
.L1294:
	movq	%r15, %rdi
	movl	$1288, %r12d
	xorl	%r14d, %r14d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE(%rip), %rax
	movq	-616(%rbp), %rcx
	movq	-560(%rbp), %rdi
	movq	%rax, -512(%rbp)
	movq	-552(%rbp), %rax
	leaq	_ZN6icu_67L20DT_DateTimeFieldsTagE(%rip), %rsi
	movl	$0, -532(%rbp)
	movq	%rax, -504(%rbp)
	leaq	-512(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -632(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	movq	%rbx, -624(%rbp)
	movq	%r12, %rbx
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1299:
	leaq	128(%rsi,%rbx), %r8
	movswl	8(%r8), %eax
	shrl	$5, %eax
	je	.L1356
.L1300:
	addl	$1, %r14d
	addq	$192, %rbx
	cmpl	$16, %r14d
	je	.L1357
.L1301:
	movq	-504(%rbp), %rsi
	leaq	(%rsi,%rbx), %r12
	movswl	8(%r12), %edx
	movl	%edx, %eax
	sarl	$5, %edx
	je	.L1358
.L1295:
	leaq	64(%rbx), %rdx
	leaq	(%rsi,%rdx), %r8
	movswl	8(%r8), %eax
	shrl	$5, %eax
	jne	.L1299
	addq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rdx, -592(%rbp)
	movq	%r8, -584(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-584(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-504(%rbp), %rsi
	movq	-592(%rbp), %rdx
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1358:
	movl	$70, %r8d
	movw	%r8w, -480(%rbp)
	testw	%ax, %ax
	jns	.L1296
	movslq	%r14d, %rax
	leaq	(%rax,%rax,2), %rax
	salq	$6, %rax
	movl	1300(%rsi,%rax), %edx
.L1296:
	movq	-576(%rbp), %rcx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$1, %r9d
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	leal	48(%r14), %eax
	cmpl	$9, %r14d
	jle	.L1326
	movl	$49, %edi
	movq	-576(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movw	%di, -480(%rbp)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leal	38(%r14), %eax
.L1326:
	movq	-576(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%ax, -480(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-504(%rbp), %rsi
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1356:
	addq	%rdx, %rsi
	movq	%r15, %rdi
	movq	%r8, -584(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-584(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1357:
	movq	-624(%rbp), %rbx
	movl	$0, -532(%rbp)
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L1307
	movq	-552(%rbp), %rax
	cmpq	$0, 4496(%rax)
	je	.L1303
.L1306:
	movq	-552(%rbp), %rax
	movl	$2, %edx
	movq	%r13, -128(%rbp)
	leaq	16+_ZTVN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE(%rip), %r14
	movw	%dx, -120(%rbp)
	movq	-608(%rbp), %rdi
	leaq	_ZN6icu_67L22DT_DateTimeCalendarTagE(%rip), %rsi
	movq	%rax, -136(%rbp)
	movq	-272(%rbp), %rax
	movq	%r14, -144(%rbp)
	movl	$0, -216(%rbp)
	movb	$0, (%rax)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-488(%rbp), %edx
	movq	-496(%rbp), %rsi
	movq	%rbx, %rcx
	movq	-568(%rbp), %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%rbx, %rdx
	movl	$47, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-152(%rbp), %edx
	movq	-208(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%rax, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%rbx, %rdx
	movl	$47, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-576(%rbp), %rdi
	leaq	_ZN6icu_67L30DT_DateTimeAvailableFormatsTagE(%rip), %rsi
	movq	%rax, %r13
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-472(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-480(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1333
	movq	-616(%rbp), %rcx
	movq	-272(%rbp), %rsi
	movq	%r15, %rdx
	movq	-560(%rbp), %rdi
	call	ures_getAllItemsWithFallback_67@PLT
.L1333:
	leaq	-128(%rbp), %rdi
	movq	%r14, -144(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
.L1307:
	movq	-632(%rbp), %rdi
	leaq	16+_ZTVN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE(%rip), %rax
	movq	%rax, -512(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	jmp	.L1278
.L1355:
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	jmp	.L1294
.L1354:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1293
.L1353:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1292
.L1340:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1279
.L1344:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1283
.L1343:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1282
.L1342:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1281
.L1341:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1280
.L1352:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1291
.L1351:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1290
.L1350:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1289
.L1349:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1288
.L1348:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1287
.L1347:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1286
.L1346:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1285
.L1345:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-520(%rbp), %rax
	jmp	.L1284
.L1303:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6724DateTimePatternGenerator13initHashtableER10UErrorCode.part.0
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1307
	jmp	.L1306
.L1335:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3622:
	.size	_ZN6icu_6724DateTimePatternGenerator11addCLDRDataERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator11addCLDRDataERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator13initHashtableER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator13initHashtableER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator13initHashtableER10UErrorCode:
.LFB3623:
	.cfi_startproc
	endbr64
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	jg	.L1369
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	$0, 4496(%rdi)
	je	.L1372
.L1359:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1369:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L1372:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$88, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1361
	movl	(%rbx), %esi
	movq	$0, (%rax)
	testl	%esi, %esi
	jle	.L1373
.L1364:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1367
	call	uhash_close_67@PLT
.L1367:
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r13, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L1373:
	.cfi_restore_state
	leaq	8(%rax), %r14
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	uhash_init_67@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1364
	movq	%r14, 0(%r13)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	uhash_setKeyDeleter_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L1364
	movq	%r13, 4496(%r12)
	jmp	.L1359
.L1361:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1359
	movl	$7, (%rbx)
	jmp	.L1359
	.cfi_endproc
.LFE3623:
	.size	_ZN6icu_6724DateTimePatternGenerator13initHashtableER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator13initHashtableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator19setAppendItemFormatE21UDateTimePatternFieldRKNS_13UnicodeStringE
	.type	_ZN6icu_6724DateTimePatternGenerator19setAppendItemFormatE21UDateTimePatternFieldRKNS_13UnicodeStringE, @function
_ZN6icu_6724DateTimePatternGenerator19setAppendItemFormatE21UDateTimePatternFieldRKNS_13UnicodeStringE:
.LFB3624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	salq	$6, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	264(%rsi,%rdi), %r12
	movq	%rdx, %rsi
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	.cfi_endproc
.LFE3624:
	.size	_ZN6icu_6724DateTimePatternGenerator19setAppendItemFormatE21UDateTimePatternFieldRKNS_13UnicodeStringE, .-_ZN6icu_6724DateTimePatternGenerator19setAppendItemFormatE21UDateTimePatternFieldRKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator19getAppendItemFormatE21UDateTimePatternField
	.type	_ZNK6icu_6724DateTimePatternGenerator19getAppendItemFormatE21UDateTimePatternField, @function
_ZNK6icu_6724DateTimePatternGenerator19getAppendItemFormatE21UDateTimePatternField:
.LFB3625:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	salq	$6, %rsi
	leaq	264(%rsi,%rdi), %rax
	ret
	.cfi_endproc
.LFE3625:
	.size	_ZNK6icu_6724DateTimePatternGenerator19getAppendItemFormatE21UDateTimePatternField, .-_ZNK6icu_6724DateTimePatternGenerator19getAppendItemFormatE21UDateTimePatternField
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator17setAppendItemNameE21UDateTimePatternFieldRKNS_13UnicodeStringE
	.type	_ZN6icu_6724DateTimePatternGenerator17setAppendItemNameE21UDateTimePatternFieldRKNS_13UnicodeStringE, @function
_ZN6icu_6724DateTimePatternGenerator17setAppendItemNameE21UDateTimePatternFieldRKNS_13UnicodeStringE:
.LFB3626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi,2), %rax
	movq	%rdx, %rsi
	salq	$6, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	1288(%rax,%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	.cfi_endproc
.LFE3626:
	.size	_ZN6icu_6724DateTimePatternGenerator17setAppendItemNameE21UDateTimePatternFieldRKNS_13UnicodeStringE, .-_ZN6icu_6724DateTimePatternGenerator17setAppendItemNameE21UDateTimePatternFieldRKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator17getAppendItemNameE21UDateTimePatternField
	.type	_ZNK6icu_6724DateTimePatternGenerator17getAppendItemNameE21UDateTimePatternField, @function
_ZNK6icu_6724DateTimePatternGenerator17getAppendItemNameE21UDateTimePatternField:
.LFB3627:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi,2), %rax
	salq	$6, %rax
	leaq	1288(%rax,%rdi), %rax
	ret
	.cfi_endproc
.LFE3627:
	.size	_ZNK6icu_6724DateTimePatternGenerator17getAppendItemNameE21UDateTimePatternField, .-_ZNK6icu_6724DateTimePatternGenerator17getAppendItemNameE21UDateTimePatternField
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator19setFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidthRKNS_13UnicodeStringE
	.type	_ZN6icu_6724DateTimePatternGenerator19setFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidthRKNS_13UnicodeStringE, @function
_ZN6icu_6724DateTimePatternGenerator19setFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidthRKNS_13UnicodeStringE:
.LFB3628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	leaq	(%rsi,%rsi,2), %rax
	movq	%rcx, %rsi
	leaq	20(%rax,%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	salq	$6, %rax
	.cfi_offset 12, -24
	leaq	8(%rax,%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	.cfi_endproc
.LFE3628:
	.size	_ZN6icu_6724DateTimePatternGenerator19setFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidthRKNS_13UnicodeStringE, .-_ZN6icu_6724DateTimePatternGenerator19setFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidthRKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator19getFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidth
	.type	_ZNK6icu_6724DateTimePatternGenerator19getFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidth, @function
_ZNK6icu_6724DateTimePatternGenerator19getFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidth:
.LFB3629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movslq	%ecx, %rcx
	leaq	(%rdx,%rdx,2), %rax
	leaq	20(%rax,%rcx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	salq	$6, %rax
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rax,%rsi), %rsi
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3629:
	.size	_ZNK6icu_6724DateTimePatternGenerator19getFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidth, .-_ZNK6icu_6724DateTimePatternGenerator19getFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidth
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator26getMutableFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidth
	.type	_ZN6icu_6724DateTimePatternGenerator26getMutableFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidth, @function
_ZN6icu_6724DateTimePatternGenerator26getMutableFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidth:
.LFB3630:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	leaq	(%rsi,%rsi,2), %rax
	leaq	20(%rax,%rdx), %rax
	salq	$6, %rax
	leaq	8(%rax,%rdi), %rax
	ret
	.cfi_endproc
.LFE3630:
	.size	_ZN6icu_6724DateTimePatternGenerator26getMutableFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidth, .-_ZN6icu_6724DateTimePatternGenerator26getMutableFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidth
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator13getAppendNameE21UDateTimePatternFieldRNS_13UnicodeStringE
	.type	_ZN6icu_6724DateTimePatternGenerator13getAppendNameE21UDateTimePatternFieldRNS_13UnicodeStringE, @function
_ZN6icu_6724DateTimePatternGenerator13getAppendNameE21UDateTimePatternFieldRNS_13UnicodeStringE:
.LFB3631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$39, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	%esi, %rbx
	subq	$16, %rsp
	movswl	8(%rdx), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movw	%cx, -42(%rbp)
	testw	%dx, %dx
	js	.L1386
	sarl	$5, %edx
.L1387:
	leaq	-42(%rbp), %r13
	xorl	%esi, %esi
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	leaq	(%rbx,%rbx,2), %rax
	salq	$6, %rax
	leaq	1288(%r14,%rax), %rsi
	movswl	8(%rsi), %ecx
	testw	%cx, %cx
	js	.L1388
	sarl	$5, %ecx
.L1389:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$39, %eax
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1392
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1386:
	.cfi_restore_state
	movl	12(%r12), %edx
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L1388:
	movl	1300(%r14,%rax), %ecx
	jmp	.L1389
.L1392:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3631:
	.size	_ZN6icu_6724DateTimePatternGenerator13getAppendNameE21UDateTimePatternFieldRNS_13UnicodeStringE, .-_ZN6icu_6724DateTimePatternGenerator13getAppendNameE21UDateTimePatternFieldRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator25mapSkeletonMetacharactersERKNS_13UnicodeStringEPiR10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator25mapSkeletonMetacharactersERKNS_13UnicodeStringEPiR10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator25mapSkeletonMetacharactersERKNS_13UnicodeStringEPiR10UErrorCode:
.LFB3634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -168(%rbp)
	movzwl	8(%rdx), %edx
	movq	%rsi, -176(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%r8, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r10w, -120(%rbp)
	movq	%rax, -128(%rbp)
	testw	%dx, %dx
	js	.L1394
	movswl	%dx, %eax
	sarl	$5, %eax
	movl	%eax, -148(%rbp)
.L1395:
	movl	-148(%rbp), %r9d
	leaq	-128(%rbp), %r15
	testl	%r9d, %r9d
	jle	.L1396
	xorl	%eax, %eax
	leaq	-128(%rbp), %r15
	leaq	-130(%rbp), %r14
	xorl	%r13d, %r13d
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1456:
	movswl	%dx, %ecx
	leal	1(%rax), %ebx
	sarl	$5, %ecx
	cmpl	%eax, %ecx
	jbe	.L1399
.L1457:
	andl	$2, %edx
	leaq	10(%r12), %r9
	jne	.L1401
	movq	24(%r12), %r9
.L1401:
	movslq	%eax, %rdx
	movzwl	(%r9,%rdx,2), %esi
	cmpw	$39, %si
	je	.L1455
	testb	%r13b, %r13b
	jne	.L1403
	cmpw	$106, %si
	je	.L1439
	cmpw	$67, %si
	je	.L1439
	cmpw	$74, %si
	jne	.L1404
	movl	$72, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movw	%cx, -130(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-184(%rbp), %rax
	orl	$2, (%rax)
	.p2align 4,,10
	.p2align 3
.L1403:
	cmpl	%ebx, -148(%rbp)
	jle	.L1396
.L1458:
	movzwl	8(%r12), %edx
	movl	%ebx, %eax
.L1425:
	testw	%dx, %dx
	jns	.L1456
	movl	12(%r12), %ecx
	leal	1(%rax), %ebx
	cmpl	%eax, %ecx
	ja	.L1457
.L1399:
	testb	%r13b, %r13b
	jne	.L1403
	movl	$-1, %esi
.L1404:
	movw	%si, -130(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	xorl	%r13d, %r13d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	%ebx, -148(%rbp)
	jg	.L1458
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	-168(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
.L1419:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1459
	movq	-168(%rbp), %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1455:
	.cfi_restore_state
	xorl	$1, %r13d
	jmp	.L1403
	.p2align 4,,10
	.p2align 3
.L1439:
	movl	-148(%rbp), %edi
	cmpl	%ebx, %edi
	jle	.L1430
	leal	-1(%rdi), %r10d
	movslq	%ebx, %rdx
	xorl	%edi, %edi
	subl	%eax, %r10d
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1409:
	addl	$1, %edi
	addq	$1, %rdx
	cmpl	%r10d, %edi
	je	.L1460
.L1411:
	movl	%eax, %r11d
	movl	$-1, %r8d
	movl	%edx, %eax
	cmpl	%edx, %ecx
	jbe	.L1408
	movzwl	(%r9,%rdx,2), %r8d
.L1408:
	cmpw	%r8w, %si
	je	.L1409
	movl	%r11d, -160(%rbp)
.L1412:
	movl	%edi, %eax
	movl	%edi, %r8d
	andl	$1, %eax
	sarl	%r8d
	addl	$3, %r8d
	movl	%eax, -156(%rbp)
	cmpl	$1, %edi
	movl	$1, %eax
	cmovle	%eax, %r8d
.L1407:
	movq	-176(%rbp), %rax
	cmpw	$106, %si
	je	.L1461
	movl	4572(%rax), %eax
	cmpl	$-1, %eax
	je	.L1416
	leal	-8(%rax), %edx
	cmpl	$1, %edx
	jbe	.L1417
	cmpl	$1, %eax
	jne	.L1462
.L1417:
	movl	$72, %eax
	movl	-156(%rbp), %ebx
	movw	%ax, -150(%rbp)
	.p2align 4,,10
	.p2align 3
.L1424:
	movzwl	-150(%rbp), %eax
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	movw	%ax, -130(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	testl	%ebx, %ebx
	je	.L1463
	xorl	%ebx, %ebx
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1463:
	movl	-160(%rbp), %ebx
	addl	$1, %ebx
	jmp	.L1403
	.p2align 4,,10
	.p2align 3
.L1460:
	movl	%eax, -160(%rbp)
	jmp	.L1412
	.p2align 4,,10
	.p2align 3
.L1461:
	movzwl	4568(%rax), %eax
	movw	%ax, -150(%rbp)
	cmpw	$72, %ax
	je	.L1452
	cmpw	$107, %ax
	je	.L1452
	movl	$97, %edi
.L1415:
	leal	-1(%r8), %ebx
	movq	%r12, -192(%rbp)
	movl	%ebx, %r12d
	movl	%edi, %ebx
	.p2align 4,,10
	.p2align 3
.L1423:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	subl	$1, %r12d
	movw	%bx, -130(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	$-1, %r12d
	jne	.L1423
	movq	-192(%rbp), %r12
	movl	-156(%rbp), %ebx
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1462:
	leal	-6(%rax), %edx
	cmpl	$1, %edx
	jbe	.L1432
	cmpl	$2, %eax
	jne	.L1464
.L1432:
	movl	$75, %edx
	movw	%dx, -150(%rbp)
.L1420:
	andl	$-3, %eax
	movl	$66, %edi
	cmpl	$5, %eax
	je	.L1415
	cmpl	$4, %eax
	sete	%al
	movzbl	%al, %eax
	addl	$97, %eax
	movl	%eax, %edi
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1394:
	movl	12(%r12), %eax
	movl	%eax, -148(%rbp)
	jmp	.L1395
.L1464:
	movl	$107, %edi
	movw	%di, -150(%rbp)
	cmpl	$3, %eax
	je	.L1452
	movl	$104, %esi
	movw	%si, -150(%rbp)
	jmp	.L1420
.L1452:
	movl	-156(%rbp), %ebx
	jmp	.L1424
.L1430:
	movl	%eax, -160(%rbp)
	movl	$1, %r8d
	movl	$0, -156(%rbp)
	jmp	.L1407
.L1416:
	movq	-200(%rbp), %rax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdi
	movl	$2, %r8d
	movl	$3, (%rax)
	movq	-168(%rbp), %rax
	movq	%rdi, (%rax)
	movw	%r8w, 8(%rax)
	jmp	.L1419
.L1459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3634:
	.size	_ZN6icu_6724DateTimePatternGenerator25mapSkeletonMetacharactersERKNS_13UnicodeStringEPiR10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator25mapSkeletonMetacharactersERKNS_13UnicodeStringEPiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator10setDecimalERKNS_13UnicodeStringE
	.type	_ZN6icu_6724DateTimePatternGenerator10setDecimalERKNS_13UnicodeStringE, @function
_ZN6icu_6724DateTimePatternGenerator10setDecimalERKNS_13UnicodeStringE:
.LFB3637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	4424(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	.cfi_endproc
.LFE3637:
	.size	_ZN6icu_6724DateTimePatternGenerator10setDecimalERKNS_13UnicodeStringE, .-_ZN6icu_6724DateTimePatternGenerator10setDecimalERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator10getDecimalEv
	.type	_ZNK6icu_6724DateTimePatternGenerator10getDecimalEv, @function
_ZNK6icu_6724DateTimePatternGenerator10getDecimalEv:
.LFB3638:
	.cfi_startproc
	endbr64
	leaq	4424(%rdi), %rax
	ret
	.cfi_endproc
.LFE3638:
	.size	_ZNK6icu_6724DateTimePatternGenerator10getDecimalEv, .-_ZNK6icu_6724DateTimePatternGenerator10getDecimalEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator17setDateTimeFormatERKNS_13UnicodeStringE
	.type	_ZN6icu_6724DateTimePatternGenerator17setDateTimeFormatERKNS_13UnicodeStringE, @function
_ZN6icu_6724DateTimePatternGenerator17setDateTimeFormatERKNS_13UnicodeStringE:
.LFB3640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	4360(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	.cfi_endproc
.LFE3640:
	.size	_ZN6icu_6724DateTimePatternGenerator17setDateTimeFormatERKNS_13UnicodeStringE, .-_ZN6icu_6724DateTimePatternGenerator17setDateTimeFormatERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator17getDateTimeFormatEv
	.type	_ZNK6icu_6724DateTimePatternGenerator17getDateTimeFormatEv, @function
_ZNK6icu_6724DateTimePatternGenerator17getDateTimeFormatEv:
.LFB3641:
	.cfi_startproc
	endbr64
	leaq	4360(%rdi), %rax
	ret
	.cfi_endproc
.LFE3641:
	.size	_ZNK6icu_6724DateTimePatternGenerator17getDateTimeFormatEv, .-_ZNK6icu_6724DateTimePatternGenerator17getDateTimeFormatEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator23setDateTimeFromCalendarERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator23setDateTimeFromCalendarERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator23setDateTimeFromCalendarERKNS_6LocaleER10UErrorCode:
.LFB3642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L1516
.L1471:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1517
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1516:
	.cfi_restore_state
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rdx, %rsi
	movq	%rdx, %r12
	movl	$0, -140(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_678Calendar14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1518
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L1519
.L1490:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1519:
	movq	%r13, %rdi
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	%r12, %rdx
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	ures_open_67@PLT
	movl	(%r12), %esi
	movq	%rax, %r13
	testl	%esi, %esi
	jle	.L1520
.L1476:
	testq	%r13, %r13
	je	.L1490
	movq	%r13, %rdi
	call	ures_close_67@PLT
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1518:
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L1471
	movl	$7, (%r12)
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1520:
	movq	%r12, %rcx
	movq	%rax, %rdx
	leaq	_ZN6icu_67L22DT_DateTimeCalendarTagE(%rip), %rsi
	movq	%rax, %rdi
	call	ures_getByKey_67@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L1476
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*184(%rax)
	testq	%rax, %rax
	je	.L1479
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*184(%rax)
	cmpb	$0, (%rax)
	je	.L1479
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*184(%rax)
	leaq	_ZN6icu_67L23DT_DateTimeGregorianTagE(%rip), %rsi
	movq	%rax, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1479
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*184(%rax)
	xorl	%edx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	ures_getByKeyWithFallback_67@PLT
	movq	%r12, %rcx
	leaq	_ZN6icu_67L22DT_DateTimePatternsTagE(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdx
	movq	%rax, %rdi
	call	ures_getByKeyWithFallback_67@PLT
	testq	%r15, %r15
	je	.L1479
	movl	(%r12), %eax
	cmpl	$2, %eax
	je	.L1478
	testl	%eax, %eax
	jle	.L1489
.L1488:
	movq	%r15, %rdi
	call	ures_close_67@PLT
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1479:
	xorl	%r15d, %r15d
.L1478:
	movq	%r15, %rdx
	movq	%r12, %rcx
	leaq	_ZN6icu_67L23DT_DateTimeGregorianTagE(%rip), %rsi
	movq	%r13, %rdi
	movl	$0, (%r12)
	call	ures_getByKeyWithFallback_67@PLT
	movq	%r12, %rcx
	leaq	_ZN6icu_67L22DT_DateTimePatternsTagE(%rip), %rsi
	movq	%rax, %rdx
	movq	%rax, %rdi
	movq	%rax, %r15
	call	ures_getByKeyWithFallback_67@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L1489
.L1481:
	testq	%r15, %r15
	je	.L1476
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1489:
	movq	%r15, %rdi
	call	ures_getSize_67@PLT
	cmpl	$8, %eax
	jg	.L1482
	movl	$3, (%r12)
	jmp	.L1481
.L1482:
	leaq	-140(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movl	$8, %esi
	call	ures_getStringByIndex_67@PLT
	leaq	-128(%rbp), %r12
	movl	-140(%rbp), %ecx
	leaq	-136(%rbp), %rdx
	movq	%r12, %rdi
	addq	$4360, %rbx
	movl	$1, %esi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r15, %r15
	jne	.L1488
	jmp	.L1476
.L1517:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3642:
	.size	_ZN6icu_6724DateTimePatternGenerator23setDateTimeFromCalendarERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator23setDateTimeFromCalendarERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator21getAppendFormatNumberEPKc
	.type	_ZNK6icu_6724DateTimePatternGenerator21getAppendFormatNumberEPKc, @function
_ZNK6icu_6724DateTimePatternGenerator21getAppendFormatNumberEPKc:
.LFB3646:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movl	$4, %ecx
	leaq	.LC1(%rip), %rsi
	movq	%rdx, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	testl	%eax, %eax
	je	.L1521
	movl	$5, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%rdx, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1525
	movl	$8, %ecx
	leaq	.LC3(%rip), %rsi
	movq	%rdx, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1526
	movl	$6, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%rdx, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1527
	movl	$5, %ecx
	leaq	.LC5(%rip), %rsi
	movq	%rdx, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1528
	cmpb	$42, (%rdx)
	je	.L1540
.L1536:
	movl	$12, %ecx
	leaq	.LC6(%rip), %rsi
	movq	%rdx, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1530
	movl	$4, %ecx
	leaq	.LC7(%rip), %rsi
	movq	%rdx, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1531
	movl	$5, %ecx
	leaq	.LC8(%rip), %rsi
	movq	%rdx, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1532
	movl	$7, %ecx
	leaq	.LC9(%rip), %rsi
	movq	%rdx, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1533
	movl	$7, %ecx
	leaq	.LC10(%rip), %rsi
	movq	%rdx, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1534
	movq	%rdx, %rdi
	movl	$9, %ecx
	movl	$16, %eax
	leaq	.LC11(%rip), %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1541
.L1521:
	ret
	.p2align 4,,10
	.p2align 3
.L1540:
	cmpb	$0, 1(%rdx)
	jne	.L1536
	movl	$5, %eax
	ret
.L1525:
	movl	$1, %eax
	ret
.L1533:
	movl	$12, %eax
	ret
.L1541:
	movl	$15, %eax
	ret
.L1526:
	movl	$2, %eax
	ret
.L1527:
	movl	$3, %eax
	ret
.L1528:
	movl	$4, %eax
	ret
.L1530:
	movl	$6, %eax
	ret
.L1531:
	movl	$9, %eax
	ret
.L1532:
	movl	$11, %eax
	ret
.L1534:
	movl	$13, %eax
	ret
	.cfi_endproc
.LFE3646:
	.size	_ZNK6icu_6724DateTimePatternGenerator21getAppendFormatNumberEPKc, .-_ZNK6icu_6724DateTimePatternGenerator21getAppendFormatNumberEPKc
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator23getFieldAndWidthIndicesEPKcP23UDateTimePGDisplayWidth
	.type	_ZNK6icu_6724DateTimePatternGenerator23getFieldAndWidthIndicesEPKcP23UDateTimePGDisplayWidth, @function
_ZNK6icu_6724DateTimePatternGenerator23getFieldAndWidthIndicesEPKcP23UDateTimePGDisplayWidth:
.LFB3647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-64(%rbp), %r12
	movq	%rdx, %rbx
	movl	$24, %edx
	movq	%r12, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	strncpy@PLT
	movl	$0, (%rbx)
	movl	$45, %esi
	movq	%r12, %rdi
	movb	$0, -40(%rbp)
	call	strchr@PLT
	testq	%rax, %rax
	je	.L1543
	movl	$8, %ecx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1581
	movl	$7, %ecx
	leaq	.LC13(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1602
.L1545:
	movb	$0, (%rax)
.L1543:
	xorl	%eax, %eax
	cmpl	$6386277, -64(%rbp)
	je	.L1542
	cmpl	$1918985593, -64(%rbp)
	je	.L1603
	movabsq	$32199698154616177, %rax
	cmpq	%rax, -64(%rbp)
	je	.L1604
.L1551:
	cmpl	$1953394541, -64(%rbp)
	je	.L1605
	cmpl	$1801807223, -64(%rbp)
	je	.L1606
.L1555:
	movabsq	$8020179002706978167, %rax
	cmpq	%rax, -64(%rbp)
	je	.L1607
.L1557:
	movabsq	$34165556108420471, %rax
	cmpq	%rax, -64(%rbp)
	je	.L1608
.L1559:
	movabsq	$7018113890281677156, %rax
	cmpq	%rax, -64(%rbp)
	je	.L1609
	movabsq	$5726715485104727415, %rax
	cmpq	%rax, -64(%rbp)
	je	.L1610
.L1563:
	cmpl	$7954788, -64(%rbp)
	movl	$9, %eax
	je	.L1542
	movabsq	$8028073590768886116, %rax
	cmpq	%rax, -64(%rbp)
	je	.L1611
	cmpl	$1920298856, -64(%rbp)
	je	.L1612
.L1569:
	cmpl	$1970170221, -64(%rbp)
	je	.L1613
.L1571:
	cmpl	$1868785011, -64(%rbp)
	je	.L1614
.L1573:
	cmpw	$42, -64(%rbp)
	movl	$14, %eax
	je	.L1542
	cmpl	$1701736314, -64(%rbp)
	je	.L1615
.L1577:
	movl	$16, %eax
	.p2align 4,,10
	.p2align 3
.L1542:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1616
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1606:
	.cfi_restore_state
	cmpb	$0, -60(%rbp)
	movl	$4, %eax
	je	.L1542
	movabsq	$8020179002706978167, %rax
	cmpq	%rax, -64(%rbp)
	jne	.L1557
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1608:
	movl	$6, %eax
	jmp	.L1542
	.p2align 4,,10
	.p2align 3
.L1607:
	cmpl	$6845550, -56(%rbp)
	movl	$5, %eax
	je	.L1542
	movabsq	$34165556108420471, %rax
	cmpq	%rax, -64(%rbp)
	jne	.L1559
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1603:
	cmpb	$0, -60(%rbp)
	movl	$1, %eax
	je	.L1542
	movabsq	$32199698154616177, %rax
	cmpq	%rax, -64(%rbp)
	jne	.L1551
.L1604:
	movl	$2, %eax
	jmp	.L1542
	.p2align 4,,10
	.p2align 3
.L1605:
	cmpw	$104, -60(%rbp)
	movl	$3, %eax
	je	.L1542
	cmpl	$1801807223, -64(%rbp)
	jne	.L1555
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1609:
	cmpw	$114, -56(%rbp)
	movl	$7, %eax
	je	.L1542
	movabsq	$5726715485104727415, %rax
	cmpq	%rax, -64(%rbp)
	jne	.L1563
.L1610:
	cmpl	$1852788070, -56(%rbp)
	jne	.L1563
	cmpw	$26740, 12(%r12)
	jne	.L1563
	cmpb	$0, 14(%r12)
	movl	$8, %eax
	je	.L1542
	jmp	.L1563
	.p2align 4,,10
	.p2align 3
.L1615:
	cmpb	$0, -60(%rbp)
	movl	$15, %eax
	je	.L1542
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1611:
	cmpw	$100, -56(%rbp)
	movl	$10, %eax
	je	.L1542
	cmpl	$1920298856, -64(%rbp)
	jne	.L1569
.L1612:
	cmpb	$0, -60(%rbp)
	movl	$11, %eax
	je	.L1542
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1613:
	cmpw	$25972, -60(%rbp)
	jne	.L1571
	cmpb	$0, 6(%r12)
	movl	$12, %eax
	je	.L1542
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1614:
	cmpw	$25710, -60(%rbp)
	jne	.L1573
	cmpb	$0, 6(%r12)
	movl	$13, %eax
	je	.L1542
	jmp	.L1573
.L1581:
	movl	$2, %edx
	movl	%edx, (%rbx)
	jmp	.L1545
.L1602:
	movl	$1, %edx
	movl	%edx, (%rbx)
	jmp	.L1545
.L1616:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3647:
	.size	_ZNK6icu_6724DateTimePatternGenerator23getFieldAndWidthIndicesEPKcP23UDateTimePGDisplayWidth, .-_ZNK6icu_6724DateTimePatternGenerator23getFieldAndWidthIndicesEPKcP23UDateTimePGDisplayWidth
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator15getTopBitNumberEi
	.type	_ZNK6icu_6724DateTimePatternGenerator15getTopBitNumberEi, @function
_ZNK6icu_6724DateTimePatternGenerator15getTopBitNumberEi:
.LFB3651:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L1617
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1619:
	sarl	%esi
	movl	%edx, %eax
	leal	1(%rdx), %edx
	jne	.L1619
	cmpl	$17, %edx
	movl	$15, %edx
	cmovge	%edx, %eax
.L1617:
	ret
	.cfi_endproc
.LFE3651:
	.size	_ZNK6icu_6724DateTimePatternGenerator15getTopBitNumberEi, .-_ZNK6icu_6724DateTimePatternGenerator15getTopBitNumberEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator18setAvailableFormatERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator18setAvailableFormatERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator18setAvailableFormatERKNS_13UnicodeStringER10UErrorCode:
.LFB3652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	4496(%rdi), %rbx
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1625
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L1625:
	movq	(%rbx), %rdi
	movq	%r13, %rcx
	popq	%rbx
	movq	%r12, %rsi
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uhash_puti_67@PLT
	.cfi_endproc
.LFE3652:
	.size	_ZN6icu_6724DateTimePatternGenerator18setAvailableFormatERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator18setAvailableFormatERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator20isAvailableFormatSetERKNS_13UnicodeStringE
	.type	_ZNK6icu_6724DateTimePatternGenerator20isAvailableFormatSetERKNS_13UnicodeStringE, @function
_ZNK6icu_6724DateTimePatternGenerator20isAvailableFormatSetERKNS_13UnicodeStringE:
.LFB3653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	4496(%rdi), %rax
	movq	(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uhash_geti_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$1, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE3653:
	.size	_ZNK6icu_6724DateTimePatternGenerator20isAvailableFormatSetERKNS_13UnicodeStringE, .-_ZNK6icu_6724DateTimePatternGenerator20isAvailableFormatSetERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator13copyHashtableEPNS_9HashtableER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator13copyHashtableEPNS_9HashtableER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator13copyHashtableEPNS_9HashtableER10UErrorCode:
.LFB3654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1632
	movl	(%rdx), %r8d
	movq	%rdx, %rbx
	testl	%r8d, %r8d
	jg	.L1632
	movq	4496(%rdi), %r14
	movq	%rdi, %r13
	movq	%rsi, %r12
	testq	%r14, %r14
	je	.L1637
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1636
	call	uhash_close_67@PLT
.L1636:
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	(%rbx), %edx
	movq	$0, 4496(%r13)
	testl	%edx, %edx
	jle	.L1637
.L1632:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1654
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1637:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator13initHashtableER10UErrorCode.part.0
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1632
	leaq	-60(%rbp), %rax
	movl	$-1, -60(%rbp)
	movq	%rax, -80(%rbp)
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L1655:
	movq	16(%rax), %rsi
	movl	$64, %edi
	movq	4496(%r13), %r14
	movq	%rsi, -72(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1639
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	(%r14), %rdi
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r15, %rsi
	call	uhash_puti_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1632
.L1641:
	movq	-80(%rbp), %rsi
	movq	(%r12), %rdi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	jne	.L1655
	jmp	.L1632
.L1639:
	movq	(%r14), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rcx
	movl	$1, %edx
	call	uhash_puti_67@PLT
	cmpl	$0, (%rbx)
	jle	.L1641
	jmp	.L1632
.L1654:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3654:
	.size	_ZN6icu_6724DateTimePatternGenerator13copyHashtableEPNS_9HashtableER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator13copyHashtableEPNS_9HashtableER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6724DateTimePatternGeneratoraSERKS0_.part.0, @function
_ZN6icu_6724DateTimePatternGeneratoraSERKS0_.part.0:
.LFB5269:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movl	4600(%rsi), %eax
	movq	%rdi, -80(%rbp)
	leaq	8(%rdi), %rdi
	movq	%rsi, -72(%rbp)
	leaq	8(%rsi), %rsi
	movl	%eax, 4592(%rdi)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	232(%r15), %r12
	movzwl	4568(%rbx), %eax
	movq	232(%rbx), %r13
	movw	%ax, 4568(%r15)
	leaq	8(%r12), %r14
	leaq	3208(%r12), %rbx
	leaq	8(%r13), %r15
	.p2align 4,,10
	.p2align 3
.L1657:
	movq	%r15, %rsi
	movq	%r14, %rdi
	addq	$64, %r14
	addq	$64, %r15
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	cmpq	%r14, %rbx
	jne	.L1657
	movl	3208(%r13), %eax
	movq	-72(%rbp), %r15
	movq	-80(%rbp), %rbx
	movl	%eax, 3208(%r12)
	movq	240(%r15), %rdx
	leaq	4360(%r15), %rsi
	movl	3212(%r13), %eax
	leaq	4360(%rbx), %r13
	movq	%r13, %rdi
	movl	%eax, 3212(%r12)
	movq	240(%rbx), %rax
	leaq	4424(%rbx), %r12
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%rdx), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%rdx), %xmm3
	movups	%xmm3, 48(%rax)
	movdqu	64(%rdx), %xmm4
	movups	%xmm4, 64(%rax)
	movdqu	80(%rdx), %xmm5
	movups	%xmm5, 80(%rax)
	movdqu	96(%rdx), %xmm6
	movups	%xmm6, 96(%rax)
	movdqu	112(%rdx), %xmm7
	movups	%xmm7, 112(%rax)
	movdqu	128(%rdx), %xmm1
	movups	%xmm1, 128(%rax)
	movzbl	144(%rdx), %edx
	movb	%dl, 144(%rax)
	movq	248(%r15), %rax
	movq	8(%rax), %rdx
	movq	248(%rbx), %rax
	movq	%rdx, 8(%rax)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	4424(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	4488(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1658
	movq	(%rdi), %rax
	leaq	_ZN6icu_6715DateTimeMatcherD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1659
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1658:
	movq	-72(%rbp), %rax
	cmpq	$0, 4488(%rax)
	je	.L1674
	movl	$152, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1662
	movq	-72(%rbp), %rcx
	leaq	16+_ZTVN6icu_6715DateTimeMatcherE(%rip), %rbx
	movq	%rbx, %xmm0
	movq	4488(%rcx), %rdx
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rcx
	movb	$0, 144(%rax)
	movq	%rcx, %xmm2
	movq	-80(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movdqu	16(%rdx), %xmm3
	movups	%xmm3, 16(%rax)
	movdqu	32(%rdx), %xmm4
	movups	%xmm4, 32(%rax)
	movdqu	48(%rdx), %xmm5
	movups	%xmm5, 48(%rax)
	movdqu	64(%rdx), %xmm6
	movups	%xmm6, 64(%rax)
	movdqu	80(%rdx), %xmm7
	movups	%xmm7, 80(%rax)
	movdqu	96(%rdx), %xmm2
	movups	%xmm2, 96(%rax)
	movdqu	112(%rdx), %xmm3
	movups	%xmm3, 112(%rax)
	movdqu	128(%rdx), %xmm4
	movups	%xmm4, 128(%rax)
	movzbl	144(%rdx), %edx
	movb	%dl, 144(%rax)
	movq	%rax, 4488(%rcx)
	movq	%rcx, %rax
.L1661:
	movq	%rax, -56(%rbp)
	movq	-72(%rbp), %rax
	movl	$264, %ebx
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L1664:
	movq	-80(%rbp), %rax
	xorl	%r14d, %r14d
	leaq	(%rax,%rbx), %r12
	movq	-72(%rbp), %rax
	movq	%r12, %rdi
	leaq	(%rax,%rbx), %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	-56(%rbp), %rax
	leaq	1288(%rax), %r13
	movq	-64(%rbp), %rax
	leaq	1288(%rax), %r12
.L1663:
	leaq	0(%r13,%r14), %r15
	leaq	(%r12,%r14), %rsi
	addq	$64, %r14
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	cmpq	$192, %r14
	jne	.L1663
	addq	$64, %rbx
	addq	$192, -64(%rbp)
	addq	$192, -56(%rbp)
	cmpq	$1288, %rbx
	jne	.L1664
	movq	-80(%rbp), %rax
	movl	4600(%rax), %edx
	leaq	4600(%rax), %r12
	testl	%edx, %edx
	jg	.L1665
	movq	-72(%rbp), %rcx
	movq	256(%rax), %rdi
	movq	%r12, %rdx
	movq	256(%rcx), %rsi
	call	_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode.part.0
.L1665:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%r12, %rdx
	movq	4496(%rax), %rsi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6724DateTimePatternGenerator13copyHashtableEPNS_9HashtableER10UErrorCode
.L1674:
	.cfi_restore_state
	movq	-80(%rbp), %rax
	movq	$0, 4488(%rax)
	jmp	.L1661
.L1659:
	call	*%rax
	jmp	.L1658
.L1662:
	movq	-80(%rbp), %rax
	movq	$0, 4488(%rax)
	movl	$7, 4600(%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5269:
	.size	_ZN6icu_6724DateTimePatternGeneratoraSERKS0_.part.0, .-_ZN6icu_6724DateTimePatternGeneratoraSERKS0_.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGeneratoraSERKS0_
	.type	_ZN6icu_6724DateTimePatternGeneratoraSERKS0_, @function
_ZN6icu_6724DateTimePatternGeneratoraSERKS0_:
.LFB3563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpq	%rdi, %rsi
	je	.L1676
	call	_ZN6icu_6724DateTimePatternGeneratoraSERKS0_.part.0
.L1676:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3563:
	.size	_ZN6icu_6724DateTimePatternGeneratoraSERKS0_, .-_ZN6icu_6724DateTimePatternGeneratoraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGeneratorC2ERKS0_
	.type	_ZN6icu_6724DateTimePatternGeneratorC2ERKS0_, @function
_ZN6icu_6724DateTimePatternGeneratorC2ERKS0_:
.LFB3561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724DateTimePatternGeneratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$8, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movl	$2, %eax
	movl	$2, %edi
	movl	$2, %r8d
	movl	$2, %r9d
	movl	$2, %r10d
	movw	%ax, 592(%r12)
	movl	$2, %eax
	movw	%ax, 656(%r12)
	movl	$2, %eax
	movl	$2, %r11d
	movl	$2, %edx
	movw	%ax, 720(%r12)
	movl	$2, %eax
	movl	$2, %ecx
	movl	$2, %esi
	movw	%ax, 784(%r12)
	movl	$2, %eax
	pxor	%xmm0, %xmm0
	movw	%di, 272(%r12)
	movl	$2, %edi
	movw	%r8w, 336(%r12)
	movl	$2, %r8d
	movw	%r9w, 400(%r12)
	movl	$2, %r9d
	movw	%ax, 848(%r12)
	movl	$2, %eax
	movw	%r10w, 464(%r12)
	movl	$2, %r10d
	movw	%r11w, 528(%r12)
	movl	$2, %r11d
	movw	%dx, 912(%r12)
	movl	$2, %edx
	movw	%cx, 976(%r12)
	movl	$2, %ecx
	movw	%si, 1040(%r12)
	movl	$2, %esi
	movw	%di, 1104(%r12)
	movl	$2, %edi
	movw	%r8w, 1168(%r12)
	movl	$2, %r8d
	movw	%r9w, 1232(%r12)
	movl	$2, %r9d
	movq	%rbx, 264(%r12)
	movq	%rbx, 328(%r12)
	movq	%rbx, 392(%r12)
	movq	%rbx, 456(%r12)
	movq	%rbx, 520(%r12)
	movq	%rbx, 584(%r12)
	movq	%rbx, 648(%r12)
	movq	%rbx, 712(%r12)
	movq	%rbx, 776(%r12)
	movq	%rbx, 840(%r12)
	movq	%rbx, 904(%r12)
	movq	%rbx, 968(%r12)
	movq	%rbx, 1032(%r12)
	movq	%rbx, 1096(%r12)
	movq	%rbx, 1160(%r12)
	movq	%rbx, 1224(%r12)
	movq	%rbx, 1288(%r12)
	movw	%ax, 1424(%r12)
	movl	$2, %eax
	movw	%ax, 1488(%r12)
	movl	$2, %eax
	movw	%ax, 1552(%r12)
	movl	$2, %eax
	movw	%ax, 1616(%r12)
	movl	$2, %eax
	movw	%ax, 1680(%r12)
	movl	$2, %eax
	movw	%r10w, 1296(%r12)
	movl	$2, %r10d
	movw	%r11w, 1360(%r12)
	movl	$2, %r11d
	movw	%ax, 2256(%r12)
	movl	$2, %eax
	movw	%dx, 1744(%r12)
	movl	$2, %edx
	movw	%cx, 1808(%r12)
	movl	$2, %ecx
	movw	%si, 1872(%r12)
	movl	$2, %esi
	movw	%di, 1936(%r12)
	movl	$2, %edi
	movw	%r8w, 2000(%r12)
	movl	$2, %r8d
	movw	%r9w, 2064(%r12)
	movl	$2, %r9d
	movw	%r10w, 2128(%r12)
	movl	$2, %r10d
	movw	%r11w, 2192(%r12)
	movl	$2, %r11d
	movq	%rbx, 1352(%r12)
	movq	%rbx, 1416(%r12)
	movq	%rbx, 1480(%r12)
	movq	%rbx, 1544(%r12)
	movq	%rbx, 1608(%r12)
	movq	%rbx, 1672(%r12)
	movq	%rbx, 1736(%r12)
	movq	%rbx, 1800(%r12)
	movq	%rbx, 1864(%r12)
	movq	%rbx, 1928(%r12)
	movq	%rbx, 1992(%r12)
	movq	%rbx, 2056(%r12)
	movq	%rbx, 2120(%r12)
	movq	%rbx, 2184(%r12)
	movq	%rbx, 2248(%r12)
	movq	%rbx, 2312(%r12)
	movw	%ax, 2320(%r12)
	movl	$2, %eax
	movw	%ax, 2384(%r12)
	movl	$2, %eax
	movw	%ax, 2448(%r12)
	movl	$2, %eax
	movw	%ax, 2512(%r12)
	movl	$2, %eax
	movw	%ax, 3088(%r12)
	movl	$2, %eax
	movw	%ax, 3152(%r12)
	movl	$2, %eax
	movw	%ax, 3216(%r12)
	movl	$2, %eax
	movw	%ax, 3280(%r12)
	movl	$2, %eax
	movw	%ax, 3344(%r12)
	movl	$2, %eax
	movw	%dx, 2576(%r12)
	movl	$2, %edx
	movw	%cx, 2640(%r12)
	movl	$2, %ecx
	movw	%si, 2704(%r12)
	movl	$2, %esi
	movw	%di, 2768(%r12)
	movl	$2, %edi
	movw	%r8w, 2832(%r12)
	movl	$2, %r8d
	movw	%r9w, 2896(%r12)
	movl	$2, %r9d
	movw	%r10w, 2960(%r12)
	movl	$2, %r10d
	movw	%r11w, 3024(%r12)
	movl	$2, %r11d
	movq	%rbx, 2376(%r12)
	movq	%rbx, 2440(%r12)
	movq	%rbx, 2504(%r12)
	movq	%rbx, 2568(%r12)
	movq	%rbx, 2632(%r12)
	movq	%rbx, 2696(%r12)
	movq	%rbx, 2760(%r12)
	movq	%rbx, 2824(%r12)
	movq	%rbx, 2888(%r12)
	movq	%rbx, 2952(%r12)
	movq	%rbx, 3016(%r12)
	movq	%rbx, 3080(%r12)
	movq	%rbx, 3144(%r12)
	movq	%rbx, 3208(%r12)
	movq	%rbx, 3272(%r12)
	movq	%rbx, 3336(%r12)
	movq	%rbx, 3400(%r12)
	movw	%ax, 3920(%r12)
	movl	$2, %eax
	movw	%ax, 3984(%r12)
	movl	$2, %eax
	movw	%ax, 4048(%r12)
	movl	$2, %eax
	movw	%dx, 3408(%r12)
	movl	$2, %edx
	movw	%cx, 3472(%r12)
	movl	$2, %ecx
	movw	%si, 3536(%r12)
	movl	$2, %esi
	movw	%di, 3600(%r12)
	movl	$2, %edi
	movw	%r8w, 3664(%r12)
	movl	$2, %r8d
	movw	%r9w, 3728(%r12)
	xorl	%r9d, %r9d
	movw	%ax, 4112(%r12)
	movl	$2, %eax
	movq	%rbx, 3464(%r12)
	movq	%rbx, 3528(%r12)
	movq	%rbx, 3592(%r12)
	movq	%rbx, 3656(%r12)
	movq	%rbx, 3720(%r12)
	movq	%rbx, 3784(%r12)
	movw	%r10w, 3792(%r12)
	movq	%rbx, 3848(%r12)
	movw	%r11w, 3856(%r12)
	movq	%rbx, 3912(%r12)
	movq	%rbx, 3976(%r12)
	movq	%rbx, 4040(%r12)
	movq	%rbx, 4104(%r12)
	movq	%rbx, 4168(%r12)
	movw	%ax, 4176(%r12)
	movq	%rbx, 4232(%r12)
	movw	%dx, 4240(%r12)
	movq	%rbx, 4296(%r12)
	movw	%cx, 4304(%r12)
	movq	%rbx, 4360(%r12)
	movw	%si, 4368(%r12)
	movq	%rbx, 4424(%r12)
	movw	%di, 4432(%r12)
	movl	$3216, %edi
	movq	%rbx, 4504(%r12)
	movw	%r8w, 4512(%r12)
	movw	%r9w, 4568(%r12)
	movl	$0, 4600(%r12)
	movups	%xmm0, 4488(%r12)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1679
	leaq	16+_ZTVN6icu_6712FormatParserE(%rip), %rsi
	leaq	8(%rax), %rdx
	movq	%rsi, (%rax)
	leaq	3208(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L1680:
	movl	$2, %esi
	movq	%rbx, (%rdx)
	addq	$64, %rdx
	movw	%si, -56(%rdx)
	cmpq	%rcx, %rdx
	jne	.L1680
	movq	$0, 3208(%rax)
.L1679:
	movq	%rax, 232(%r12)
	movl	$152, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1681
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rdx
	leaq	16+_ZTVN6icu_6715DateTimeMatcherE(%rip), %rsi
	movb	$0, 144(%rax)
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
.L1681:
	movq	%rax, 240(%r12)
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1682
	leaq	16+_ZTVN6icu_6712DistanceInfoE(%rip), %rsi
	movq	%rsi, (%rax)
.L1682:
	movq	%rax, 248(%r12)
	movl	$432, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1683
	leaq	16+_ZTVN6icu_6710PatternMapE(%rip), %rax
	leaq	16(%rdx), %rdi
	movq	$0, 8(%rdx)
	movq	%rax, (%rdx)
	andq	$-8, %rdi
	movl	%edx, %eax
	movq	$0, 416(%rdx)
	subl	%edi, %eax
	leal	424(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movb	$1, 424(%rdx)
.L1683:
	cmpq	$0, 232(%r12)
	movq	%rdx, 256(%r12)
	je	.L1684
	cmpq	$0, 240(%r12)
	je	.L1684
	cmpq	$0, 248(%r12)
	je	.L1684
	testq	%rdx, %rdx
	jne	.L1685
	.p2align 4,,10
	.p2align 3
.L1684:
	movl	$7, 4600(%r12)
.L1685:
	cmpq	%r13, %r12
	je	.L1678
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6724DateTimePatternGeneratoraSERKS0_.part.0
.L1678:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3561:
	.size	_ZN6icu_6724DateTimePatternGeneratorC2ERKS0_, .-_ZN6icu_6724DateTimePatternGeneratorC2ERKS0_
	.globl	_ZN6icu_6724DateTimePatternGeneratorC1ERKS0_
	.set	_ZN6icu_6724DateTimePatternGeneratorC1ERKS0_,_ZN6icu_6724DateTimePatternGeneratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator15isCanonicalItemERKNS_13UnicodeStringE
	.type	_ZNK6icu_6724DateTimePatternGenerator15isCanonicalItemERKNS_13UnicodeStringE, @function
_ZNK6icu_6724DateTimePatternGenerator15isCanonicalItemERKNS_13UnicodeStringE:
.LFB3659:
	.cfi_startproc
	endbr64
	movzwl	8(%rsi), %edx
	testw	%dx, %dx
	js	.L1706
	movswl	%dx, %eax
	sarl	$5, %eax
.L1707:
	xorl	%r8d, %r8d
	cmpl	$1, %eax
	jne	.L1705
	andl	$2, %edx
	je	.L1757
	movzwl	10(%rsi), %eax
	cmpw	$71, %ax
	je	.L1740
.L1756:
	cmpw	$121, %ax
	je	.L1740
	cmpw	$81, %ax
	je	.L1740
	cmpw	$77, %ax
	je	.L1740
	cmpw	$119, %ax
	je	.L1740
	cmpw	$87, %ax
	je	.L1740
	cmpw	$69, %ax
	je	.L1740
	cmpw	$68, %ax
	je	.L1740
	cmpw	$70, %ax
	je	.L1740
	cmpw	$100, %ax
	je	.L1740
	cmpw	$97, %ax
	je	.L1740
	cmpw	$72, %ax
	je	.L1740
	cmpw	$109, %ax
	je	.L1740
	cmpw	$115, %ax
	je	.L1740
	cmpw	$83, %ax
	je	.L1740
	cmpw	$118, %ax
	sete	%r8b
.L1705:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1757:
	movq	24(%rsi), %rax
	movzwl	(%rax), %eax
	cmpw	$71, %ax
	jne	.L1756
.L1740:
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1706:
	movl	12(%rsi), %eax
	jmp	.L1707
	.cfi_endproc
.LFE3659:
	.size	_ZNK6icu_6724DateTimePatternGenerator15isCanonicalItemERKNS_13UnicodeStringE, .-_ZNK6icu_6724DateTimePatternGenerator15isCanonicalItemERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator5cloneEv
	.type	_ZNK6icu_6724DateTimePatternGenerator5cloneEv, @function
_ZNK6icu_6724DateTimePatternGenerator5cloneEv:
.LFB3660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$4608, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1758
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6724DateTimePatternGeneratorC1ERKS0_
.L1758:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3660:
	.size	_ZNK6icu_6724DateTimePatternGenerator5cloneEv, .-_ZNK6icu_6724DateTimePatternGenerator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710PatternMapC2Ev
	.type	_ZN6icu_6710PatternMapC2Ev, @function
_ZN6icu_6710PatternMapC2Ev:
.LFB3662:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6710PatternMapE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rdi, %rdx
	leaq	16(%rdi), %rdi
	movq	%rax, -16(%rdi)
	movl	%edx, %eax
	movq	$0, 400(%rdi)
	andq	$-8, %rdi
	subl	%edi, %eax
	leal	424(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movb	$1, 424(%rdx)
	ret
	.cfi_endproc
.LFE3662:
	.size	_ZN6icu_6710PatternMapC2Ev, .-_ZN6icu_6710PatternMapC2Ev
	.globl	_ZN6icu_6710PatternMapC1Ev
	.set	_ZN6icu_6710PatternMapC1Ev,_ZN6icu_6710PatternMapC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode
	.type	_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode, @function
_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode:
.LFB3664:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L1765
	jmp	_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1765:
	ret
	.cfi_endproc
.LFE3664:
	.size	_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode, .-_ZN6icu_6710PatternMap8copyFromERKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710PatternMap9getHeaderEDs
	.type	_ZNK6icu_6710PatternMap9getHeaderEDs, @function
_ZNK6icu_6710PatternMap9getHeaderEDs:
.LFB3665:
	.cfi_startproc
	endbr64
	leal	-65(%rsi), %edx
	movzwl	%si, %eax
	cmpw	$25, %dx
	ja	.L1768
	subl	$65, %eax
	cltq
	movq	8(%rdi,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1768:
	subl	$97, %esi
	cmpw	$25, %si
	ja	.L1770
	subl	$71, %eax
	cltq
	movq	8(%rdi,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1770:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3665:
	.size	_ZNK6icu_6710PatternMap9getHeaderEDs, .-_ZNK6icu_6710PatternMap9getHeaderEDs
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710PatternMap25getPatternFromBasePatternERKNS_13UnicodeStringERa
	.type	_ZNK6icu_6710PatternMap25getPatternFromBasePatternERKNS_13UnicodeStringERa, @function
_ZNK6icu_6710PatternMap25getPatternFromBasePatternERKNS_13UnicodeStringERa:
.LFB3673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L1772
	movswl	%ax, %edx
	sarl	$5, %edx
.L1773:
	testl	%edx, %edx
	je	.L1774
	leaq	10(%r14), %rdx
	testb	$2, %al
	je	.L1802
	movzwl	(%rdx), %edx
	leal	-65(%rdx), %esi
	movl	%edx, %ecx
	cmpw	$25, %si
	ja	.L1777
.L1805:
	subl	$65, %edx
	movslq	%edx, %rdx
	movq	8(%rdi,%rdx,8), %rbx
.L1778:
	testq	%rbx, %rbx
	je	.L1774
	xorl	%r12d, %r12d
	jmp	.L1790
	.p2align 4,,10
	.p2align 3
.L1804:
	notl	%eax
	andl	$1, %eax
	testb	%al, %al
	je	.L1803
.L1788:
	movq	152(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1774
	movzwl	8(%r14), %eax
.L1790:
	movzwl	16(%rbx), %esi
	testw	%si, %si
	js	.L1779
	movswl	%si, %ecx
	sarl	$5, %ecx
.L1780:
	testw	%ax, %ax
	js	.L1781
	movswl	%ax, %edx
	sarl	$5, %edx
.L1782:
	testb	$1, %sil
	jne	.L1804
	testl	%ecx, %ecx
	movl	%r12d, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L1785
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L1785:
	andl	$2, %esi
	leaq	18(%rbx), %rcx
	jne	.L1787
	movq	32(%rbx), %rcx
.L1787:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jne	.L1788
.L1803:
	movzbl	144(%rbx), %eax
	movb	%al, 0(%r13)
	leaq	80(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1781:
	.cfi_restore_state
	movl	12(%r14), %edx
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1779:
	movl	20(%rbx), %ecx
	jmp	.L1780
	.p2align 4,,10
	.p2align 3
.L1774:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1802:
	.cfi_restore_state
	movq	24(%r14), %rdx
	movzwl	(%rdx), %edx
	leal	-65(%rdx), %esi
	movl	%edx, %ecx
	cmpw	$25, %si
	jbe	.L1805
.L1777:
	subl	$97, %ecx
	cmpw	$25, %cx
	ja	.L1774
	subl	$71, %edx
	movslq	%edx, %rdx
	movq	8(%rdi,%rdx,8), %rbx
	jmp	.L1778
	.p2align 4,,10
	.p2align 3
.L1772:
	movl	12(%rsi), %edx
	jmp	.L1773
	.cfi_endproc
.LFE3673:
	.size	_ZNK6icu_6710PatternMap25getPatternFromBasePatternERKNS_13UnicodeStringERa, .-_ZNK6icu_6710PatternMap25getPatternFromBasePatternERKNS_13UnicodeStringERa
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710PatternMap22getPatternFromSkeletonERKNS_11PtnSkeletonEPPS2_
	.type	_ZNK6icu_6710PatternMap22getPatternFromSkeletonERKNS_11PtnSkeletonEPPS2_, @function
_ZNK6icu_6710PatternMap22getPatternFromSkeletonERKNS_11PtnSkeletonEPPS2_:
.LFB3674:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L1807
	movq	$0, (%rdx)
.L1807:
	cmpb	$0, 120(%rsi)
	jne	.L1825
	cmpb	$0, 121(%rsi)
	jne	.L1826
	cmpb	$0, 122(%rsi)
	jne	.L1827
	cmpb	$0, 123(%rsi)
	jne	.L1828
	cmpb	$0, 124(%rsi)
	jne	.L1829
	cmpb	$0, 125(%rsi)
	jne	.L1830
	cmpb	$0, 126(%rsi)
	jne	.L1831
	cmpb	$0, 127(%rsi)
	jne	.L1832
	cmpb	$0, 128(%rsi)
	jne	.L1833
	cmpb	$0, 129(%rsi)
	jne	.L1834
	cmpb	$0, 130(%rsi)
	jne	.L1835
	cmpb	$0, 131(%rsi)
	jne	.L1836
	cmpb	$0, 132(%rsi)
	jne	.L1837
	cmpb	$0, 133(%rsi)
	jne	.L1838
	cmpb	$0, 134(%rsi)
	jne	.L1839
	xorl	%eax, %eax
	cmpb	$0, 135(%rsi)
	jne	.L1855
.L1806:
	ret
.L1825:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1808:
	movsbw	104(%rsi,%rax), %cx
	leal	-65(%rcx), %r8d
	movzwl	%cx, %eax
	cmpw	$25, %r8w
	ja	.L1810
	subl	$65, %eax
	cltq
	movq	8(%rdi,%rax,8), %rax
.L1811:
	testq	%rax, %rax
	je	.L1806
	leaq	72(%rsi), %r8
	leaq	88(%rsi), %r9
	testq	%rdx, %rdx
	je	.L1856
	.p2align 4,,10
	.p2align 3
.L1812:
	movq	72(%rax), %rcx
	movq	72(%rcx), %rsi
	movq	80(%rcx), %rdi
	xorq	(%r8), %rsi
	xorq	8(%r8), %rdi
	orq	%rsi, %rdi
	jne	.L1822
	movq	88(%rcx), %rsi
	movq	96(%rcx), %rdi
	xorq	(%r9), %rsi
	xorq	8(%r9), %rdi
	orq	%rsi, %rdi
	jne	.L1822
	cmpb	$0, 144(%rax)
	je	.L1818
	movq	%rcx, (%rdx)
.L1818:
	addq	$80, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1822:
	movq	152(%rax), %rax
	testq	%rax, %rax
	jne	.L1812
	ret
.L1810:
	subl	$97, %ecx
	cmpw	$25, %cx
	ja	.L1841
	subl	$71, %eax
	cltq
	movq	8(%rdi,%rax,8), %rax
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1856:
	leaq	104(%rsi), %r8
	addq	$120, %rsi
	.p2align 4,,10
	.p2align 3
.L1819:
	movq	72(%rax), %rdx
	movq	104(%rdx), %rcx
	movq	112(%rdx), %rdi
	xorq	(%r8), %rcx
	xorq	8(%r8), %rdi
	orq	%rcx, %rdi
	jne	.L1815
	movq	120(%rdx), %rcx
	movq	128(%rdx), %rdi
	movq	(%rsi), %rdx
	xorq	8(%rsi), %rdi
	xorq	%rcx, %rdx
	orq	%rdx, %rdi
	je	.L1818
.L1815:
	movq	152(%rax), %rax
	testq	%rax, %rax
	jne	.L1819
	ret
.L1826:
	movl	$1, %eax
	jmp	.L1808
.L1837:
	movl	$12, %eax
	jmp	.L1808
.L1855:
	movl	$15, %eax
	jmp	.L1808
.L1827:
	movl	$2, %eax
	jmp	.L1808
.L1828:
	movl	$3, %eax
	jmp	.L1808
.L1829:
	movl	$4, %eax
	jmp	.L1808
.L1830:
	movl	$5, %eax
	jmp	.L1808
.L1831:
	movl	$6, %eax
	jmp	.L1808
.L1832:
	movl	$7, %eax
	jmp	.L1808
.L1833:
	movl	$8, %eax
	jmp	.L1808
.L1834:
	movl	$9, %eax
	jmp	.L1808
.L1835:
	movl	$10, %eax
	jmp	.L1808
.L1836:
	movl	$11, %eax
	jmp	.L1808
.L1838:
	movl	$13, %eax
	jmp	.L1808
.L1839:
	movl	$14, %eax
	jmp	.L1808
.L1841:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3674:
	.size	_ZNK6icu_6710PatternMap22getPatternFromSkeletonERKNS_11PtnSkeletonEPPS2_, .-_ZNK6icu_6710PatternMap22getPatternFromSkeletonERKNS_11PtnSkeletonEPPS2_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710PatternMap6equalsERKS0_
	.type	_ZNK6icu_6710PatternMap6equalsERKS0_, @function
_ZNK6icu_6710PatternMap6equalsERKS0_:
.LFB3675:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L1858
	jmp	_ZNK6icu_6710PatternMap6equalsERKS0_.part.0
	.p2align 4,,10
	.p2align 3
.L1858:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3675:
	.size	_ZNK6icu_6710PatternMap6equalsERKS0_, .-_ZNK6icu_6710PatternMap6equalsERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710PatternMap16getDuplicateElemERKNS_13UnicodeStringERKNS_11PtnSkeletonEPNS_7PtnElemE
	.type	_ZN6icu_6710PatternMap16getDuplicateElemERKNS_13UnicodeStringERKNS_11PtnSkeletonEPNS_7PtnElemE, @function
_ZN6icu_6710PatternMap16getDuplicateElemERKNS_13UnicodeStringERKNS_11PtnSkeletonEPNS_7PtnElemE:
.LFB3676:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	testq	%rcx, %rcx
	je	.L1860
	jmp	_ZN6icu_6710PatternMap16getDuplicateElemERKNS_13UnicodeStringERKNS_11PtnSkeletonEPNS_7PtnElemE.part.0
	.p2align 4,,10
	.p2align 3
.L1860:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3676:
	.size	_ZN6icu_6710PatternMap16getDuplicateElemERKNS_13UnicodeStringERKNS_11PtnSkeletonEPNS_7PtnElemE, .-_ZN6icu_6710PatternMap16getDuplicateElemERKNS_13UnicodeStringERKNS_11PtnSkeletonEPNS_7PtnElemE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715DateTimeMatcherC2Ev
	.type	_ZN6icu_6715DateTimeMatcherC2Ev, @function
_ZN6icu_6715DateTimeMatcherC2Ev:
.LFB3678:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	leaq	16+_ZTVN6icu_6715DateTimeMatcherE(%rip), %rdx
	movb	$0, 144(%rdi)
	movq	%rdx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 128(%rdi)
	ret
	.cfi_endproc
.LFE3678:
	.size	_ZN6icu_6715DateTimeMatcherC2Ev, .-_ZN6icu_6715DateTimeMatcherC2Ev
	.globl	_ZN6icu_6715DateTimeMatcherC1Ev
	.set	_ZN6icu_6715DateTimeMatcherC1Ev,_ZN6icu_6715DateTimeMatcherC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715DateTimeMatcherC2ERKS0_
	.type	_ZN6icu_6715DateTimeMatcherC2ERKS0_, @function
_ZN6icu_6715DateTimeMatcherC2ERKS0_:
.LFB3685:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	leaq	16+_ZTVN6icu_6715DateTimeMatcherE(%rip), %rcx
	movb	$0, 144(%rdi)
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 128(%rdi)
	movdqu	16(%rsi), %xmm2
	movups	%xmm2, 16(%rdi)
	movdqu	32(%rsi), %xmm3
	movups	%xmm3, 32(%rdi)
	movdqu	48(%rsi), %xmm4
	movups	%xmm4, 48(%rdi)
	movdqu	64(%rsi), %xmm5
	movups	%xmm5, 64(%rdi)
	movdqu	80(%rsi), %xmm6
	movdqu	96(%rsi), %xmm7
	movdqu	112(%rsi), %xmm1
	movdqu	128(%rsi), %xmm2
	movzbl	144(%rsi), %eax
	movups	%xmm6, 80(%rdi)
	movups	%xmm7, 96(%rdi)
	movb	%al, 144(%rdi)
	movups	%xmm1, 112(%rdi)
	movups	%xmm2, 128(%rdi)
	ret
	.cfi_endproc
.LFE3685:
	.size	_ZN6icu_6715DateTimeMatcherC2ERKS0_, .-_ZN6icu_6715DateTimeMatcherC2ERKS0_
	.globl	_ZN6icu_6715DateTimeMatcherC1ERKS0_
	.set	_ZN6icu_6715DateTimeMatcherC1ERKS0_,_ZN6icu_6715DateTimeMatcherC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715DateTimeMatcheraSERKS0_
	.type	_ZN6icu_6715DateTimeMatcheraSERKS0_, @function
_ZN6icu_6715DateTimeMatcheraSERKS0_:
.LFB3687:
	.cfi_startproc
	endbr64
	movdqu	16(%rsi), %xmm0
	movq	%rdi, %rax
	movups	%xmm0, 16(%rdi)
	movdqu	32(%rsi), %xmm1
	movups	%xmm1, 32(%rdi)
	movdqu	48(%rsi), %xmm2
	movups	%xmm2, 48(%rdi)
	movdqu	64(%rsi), %xmm3
	movups	%xmm3, 64(%rdi)
	movdqu	80(%rsi), %xmm4
	movups	%xmm4, 80(%rdi)
	movdqu	96(%rsi), %xmm5
	movups	%xmm5, 96(%rdi)
	movdqu	112(%rsi), %xmm6
	movups	%xmm6, 112(%rdi)
	movdqu	128(%rsi), %xmm7
	movups	%xmm7, 128(%rdi)
	movzbl	144(%rsi), %edx
	movb	%dl, 144(%rdi)
	ret
	.cfi_endproc
.LFE3687:
	.size	_ZN6icu_6715DateTimeMatcheraSERKS0_, .-_ZN6icu_6715DateTimeMatcheraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715DateTimeMatcher14getBasePatternERNS_13UnicodeStringE
	.type	_ZN6icu_6715DateTimeMatcher14getBasePatternERNS_13UnicodeStringE, @function
_ZN6icu_6715DateTimeMatcher14getBasePatternERNS_13UnicodeStringE:
.LFB3690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-58(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	112(%rdi), %rbx
	subq	$40, %rsp
	movzwl	8(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 8(%rsi)
	leaq	128(%rdi), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L1866:
	movsbl	16(%rbx), %r15d
	movsbw	(%rbx), %r8w
	testl	%r15d, %r15d
	jle	.L1868
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L1869:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	addl	$1, %r14d
	movw	%r8w, -58(%rbp)
	movl	%r8d, -76(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	%r14d, %r15d
	movl	-76(%rbp), %r8d
	jne	.L1869
.L1868:
	addq	$1, %rbx
	cmpq	%rbx, -72(%rbp)
	jne	.L1866
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1874
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1874:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3690:
	.size	_ZN6icu_6715DateTimeMatcher14getBasePatternERNS_13UnicodeStringE, .-_ZN6icu_6715DateTimeMatcher14getBasePatternERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715DateTimeMatcher10getPatternEv
	.type	_ZN6icu_6715DateTimeMatcher10getPatternEv, @function
_ZN6icu_6715DateTimeMatcher10getPatternEv:
.LFB3691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-130(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	80(%rsi), %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	leaq	96(%rsi), %rax
	movq	%rax, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L1878:
	movsbl	16(%rbx), %r8d
	movsbw	(%rbx), %r9w
	testl	%r8d, %r8d
	jle	.L1876
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L1877:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%r8d, -160(%rbp)
	addl	$1, %r15d
	movw	%r9w, -130(%rbp)
	movl	%r9d, -156(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-160(%rbp), %r8d
	movl	-156(%rbp), %r9d
	cmpl	%r15d, %r8d
	jne	.L1877
.L1876:
	addq	$1, %rbx
	cmpq	-152(%rbp), %rbx
	jne	.L1878
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1883
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1883:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3691:
	.size	_ZN6icu_6715DateTimeMatcher10getPatternEv, .-_ZN6icu_6715DateTimeMatcher10getPatternEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715DateTimeMatcher11getDistanceERKS0_iRNS_12DistanceInfoE
	.type	_ZNK6icu_6715DateTimeMatcher11getDistanceERKS0_iRNS_12DistanceInfoE, @function
_ZNK6icu_6715DateTimeMatcher11getDistanceERKS0_iRNS_12DistanceInfoE:
.LFB3692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r11
	xorl	%eax, %eax
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	$1, %ebx
	movq	$0, 8(%rcx)
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L1897:
	testl	%r9d, %r9d
	je	.L1886
.L1887:
	movl	%ebx, %r9d
	addl	$65536, %r10d
	sall	%cl, %r9d
	orl	%r9d, 12(%r11)
.L1886:
	addq	$1, %rax
	cmpq	$16, %rax
	je	.L1896
.L1890:
	btl	%eax, %edx
	movl	16(%rsi,%rax,4), %r9d
	movl	%eax, %ecx
	jnc	.L1897
	movl	16(%rdi,%rax,4), %r8d
	cmpl	%r9d, %r8d
	je	.L1886
	testl	%r8d, %r8d
	je	.L1887
	testl	%r9d, %r9d
	jne	.L1889
	movl	%ebx, %r8d
	addq	$1, %rax
	addl	$4096, %r10d
	sall	%cl, %r8d
	orl	%r8d, 8(%r11)
	cmpq	$16, %rax
	jne	.L1890
.L1896:
	movl	%r10d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1889:
	.cfi_restore_state
	subl	%r9d, %r8d
	movl	%r8d, %ecx
	sarl	$31, %ecx
	xorl	%ecx, %r8d
	subl	%ecx, %r8d
	addl	%r8d, %r10d
	jmp	.L1886
	.cfi_endproc
.LFE3692:
	.size	_ZNK6icu_6715DateTimeMatcher11getDistanceERKS0_iRNS_12DistanceInfoE, .-_ZNK6icu_6715DateTimeMatcher11getDistanceERKS0_iRNS_12DistanceInfoE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715DateTimeMatcher8copyFromERKNS_11PtnSkeletonE
	.type	_ZN6icu_6715DateTimeMatcher8copyFromERKNS_11PtnSkeletonE, @function
_ZN6icu_6715DateTimeMatcher8copyFromERKNS_11PtnSkeletonE:
.LFB3693:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 16(%rdi)
	movdqu	24(%rsi), %xmm1
	movups	%xmm1, 32(%rdi)
	movdqu	40(%rsi), %xmm2
	movups	%xmm2, 48(%rdi)
	movdqu	56(%rsi), %xmm3
	movups	%xmm3, 64(%rdi)
	movdqu	72(%rsi), %xmm4
	movups	%xmm4, 80(%rdi)
	movdqu	88(%rsi), %xmm5
	movups	%xmm5, 96(%rdi)
	movdqu	104(%rsi), %xmm6
	movups	%xmm6, 112(%rdi)
	movdqu	120(%rsi), %xmm7
	movups	%xmm7, 128(%rdi)
	movzbl	136(%rsi), %eax
	movb	%al, 144(%rdi)
	ret
	.cfi_endproc
.LFE3693:
	.size	_ZN6icu_6715DateTimeMatcher8copyFromERKNS_11PtnSkeletonE, .-_ZN6icu_6715DateTimeMatcher8copyFromERKNS_11PtnSkeletonE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715DateTimeMatcher8copyFromEv
	.type	_ZN6icu_6715DateTimeMatcher8copyFromEv, @function
_ZN6icu_6715DateTimeMatcher8copyFromEv:
.LFB3694:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 128(%rdi)
	ret
	.cfi_endproc
.LFE3694:
	.size	_ZN6icu_6715DateTimeMatcher8copyFromEv, .-_ZN6icu_6715DateTimeMatcher8copyFromEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715DateTimeMatcher6equalsEPKS0_
	.type	_ZNK6icu_6715DateTimeMatcher6equalsEPKS0_, @function
_ZNK6icu_6715DateTimeMatcher6equalsEPKS0_:
.LFB3695:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1907
	movq	80(%rdi), %rax
	movq	88(%rdi), %rdx
	xorl	%r8d, %r8d
	xorq	88(%rsi), %rdx
	xorq	80(%rsi), %rax
	orq	%rax, %rdx
	je	.L1909
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1909:
	movq	96(%rdi), %rax
	movq	104(%rdi), %rdx
	xorq	96(%rsi), %rax
	xorq	104(%rsi), %rdx
	orq	%rax, %rdx
	sete	%r8b
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1907:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3695:
	.size	_ZNK6icu_6715DateTimeMatcher6equalsEPKS0_, .-_ZNK6icu_6715DateTimeMatcher6equalsEPKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715DateTimeMatcher12getFieldMaskEv
	.type	_ZNK6icu_6715DateTimeMatcher12getFieldMaskEv, @function
_ZNK6icu_6715DateTimeMatcher12getFieldMaskEv:
.LFB3696:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %edx
	movl	20(%rdi), %ecx
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	testl	%ecx, %ecx
	je	.L1911
	orl	$2, %eax
.L1911:
	movl	24(%rdi), %r11d
	testl	%r11d, %r11d
	je	.L1912
	orl	$4, %eax
.L1912:
	movl	28(%rdi), %r10d
	testl	%r10d, %r10d
	je	.L1913
	orl	$8, %eax
.L1913:
	movl	32(%rdi), %r9d
	testl	%r9d, %r9d
	je	.L1914
	orl	$16, %eax
.L1914:
	movl	36(%rdi), %r8d
	testl	%r8d, %r8d
	je	.L1915
	orl	$32, %eax
.L1915:
	movl	40(%rdi), %esi
	testl	%esi, %esi
	je	.L1916
	orl	$64, %eax
.L1916:
	movl	44(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L1917
	orb	$-128, %al
.L1917:
	movl	48(%rdi), %edx
	testl	%edx, %edx
	je	.L1918
	orb	$1, %ah
.L1918:
	movl	52(%rdi), %r11d
	testl	%r11d, %r11d
	je	.L1919
	orb	$2, %ah
.L1919:
	movl	56(%rdi), %r10d
	testl	%r10d, %r10d
	je	.L1920
	orb	$4, %ah
.L1920:
	movl	60(%rdi), %r9d
	testl	%r9d, %r9d
	je	.L1921
	orb	$8, %ah
.L1921:
	movl	64(%rdi), %r8d
	testl	%r8d, %r8d
	je	.L1922
	orb	$16, %ah
.L1922:
	movl	68(%rdi), %esi
	testl	%esi, %esi
	je	.L1923
	orb	$32, %ah
.L1923:
	movl	72(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L1924
	orb	$64, %ah
.L1924:
	movl	76(%rdi), %edx
	testl	%edx, %edx
	je	.L1910
	orb	$-128, %ah
.L1910:
	ret
	.cfi_endproc
.LFE3696:
	.size	_ZNK6icu_6715DateTimeMatcher12getFieldMaskEv, .-_ZNK6icu_6715DateTimeMatcher12getFieldMaskEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715DateTimeMatcher14getSkeletonPtrEv
	.type	_ZN6icu_6715DateTimeMatcher14getSkeletonPtrEv, @function
_ZN6icu_6715DateTimeMatcher14getSkeletonPtrEv:
.LFB3697:
	.cfi_startproc
	endbr64
	leaq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE3697:
	.size	_ZN6icu_6715DateTimeMatcher14getSkeletonPtrEv, .-_ZN6icu_6715DateTimeMatcher14getSkeletonPtrEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FormatParserC2Ev
	.type	_ZN6icu_6712FormatParserC2Ev, @function
_ZN6icu_6712FormatParserC2Ev:
.LFB3699:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712FormatParserE(%rip), %rax
	leaq	3208(%rdi), %rcx
	movq	%rax, (%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	leaq	8(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L1928:
	movl	$2, %esi
	movq	%rdx, (%rax)
	addq	$64, %rax
	movw	%si, -56(%rax)
	cmpq	%rcx, %rax
	jne	.L1928
	movq	$0, 3208(%rdi)
	ret
	.cfi_endproc
.LFE3699:
	.size	_ZN6icu_6712FormatParserC2Ev, .-_ZN6icu_6712FormatParserC2Ev
	.globl	_ZN6icu_6712FormatParserC1Ev
	.set	_ZN6icu_6712FormatParserC1Ev,_ZN6icu_6712FormatParserC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FormatParser3setERKNS_13UnicodeStringE
	.type	_ZN6icu_6712FormatParser3setERKNS_13UnicodeStringE, @function
_ZN6icu_6712FormatParser3setERKNS_13UnicodeStringE:
.LFB3706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-132(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-128(%rbp), %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -132(%rbp)
	movl	$0, 3208(%rdi)
	jmp	.L1932
	.p2align 4,,10
	.p2align 3
.L1936:
	movl	-132(%rbp), %ecx
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	movslq	3208(%r14), %rax
	movq	%rbx, %rsi
	leal	1(%rax), %edx
	salq	$6, %rax
	movl	%edx, 3208(%r14)
	leaq	8(%r14,%rax), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addl	-132(%rbp), %r15d
	cmpl	$49, 3208(%r14)
	jg	.L1930
.L1932:
	movq	(%r14), %rax
	movq	%r13, %rcx
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*16(%rax)
	cmpl	$1, %eax
	je	.L1936
.L1930:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1937
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1937:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3706:
	.size	_ZN6icu_6712FormatParser3setERKNS_13UnicodeStringE, .-_ZN6icu_6712FormatParser3setERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FormatParser17getCanonicalIndexERKNS_13UnicodeStringEa
	.type	_ZN6icu_6712FormatParser17getCanonicalIndexERKNS_13UnicodeStringEa, @function
_ZN6icu_6712FormatParser17getCanonicalIndexERKNS_13UnicodeStringEa:
.LFB3707:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L1939
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1940:
	testl	%ecx, %ecx
	je	.L1954
	testb	$2, %al
	jne	.L1956
	movq	24(%rdi), %rdi
.L1943:
	movzwl	(%rdi), %r8d
	cmpl	$1, %ecx
	jle	.L1944
	leal	-2(%rcx), %r9d
	movl	$1, %eax
	addq	$2, %r9
	jmp	.L1946
	.p2align 4,,10
	.p2align 3
.L1957:
	addq	$1, %rax
	cmpq	%r9, %rax
	je	.L1944
.L1946:
	movl	$-1, %edx
	cmpl	%eax, %ecx
	jbe	.L1945
	movzwl	(%rdi,%rax,2), %edx
.L1945:
	cmpw	%dx, %r8w
	je	.L1957
.L1954:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1956:
	addq	$10, %rdi
	jmp	.L1943
	.p2align 4,,10
	.p2align 3
.L1944:
	leaq	16+_ZN6icu_67L7dtTypesE(%rip), %rdx
	xorl	%eax, %eax
	movl	$71, %r9d
	movl	$-1, %r11d
	movzwl	(%rdx), %edi
	leal	1(%rax), %r10d
	cmpw	%r9w, %r8w
	jne	.L1949
.L1959:
	cmpw	%di, %r8w
	jne	.L1938
	movswl	10(%rdx), %r9d
	cmpl	%ecx, %r9d
	jle	.L1958
.L1938:
	ret
	.p2align 4,,10
	.p2align 3
.L1958:
	addq	$16, %rdx
	movl	%eax, %r11d
.L1951:
	movl	%r10d, %eax
	movl	%edi, %r9d
	movzwl	(%rdx), %edi
	leal	1(%rax), %r10d
	cmpw	%r9w, %r8w
	je	.L1959
.L1949:
	addq	$16, %rdx
	testw	%di, %di
	jne	.L1951
	testb	%sil, %sil
	movl	$-1, %eax
	cmove	%r11d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1939:
	movl	12(%rdi), %ecx
	jmp	.L1940
	.cfi_endproc
.LFE3707:
	.size	_ZN6icu_6712FormatParser17getCanonicalIndexERKNS_13UnicodeStringEa, .-_ZN6icu_6712FormatParser17getCanonicalIndexERKNS_13UnicodeStringEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FormatParser14isQuoteLiteralERKNS_13UnicodeStringE
	.type	_ZN6icu_6712FormatParser14isQuoteLiteralERKNS_13UnicodeStringE, @function
_ZN6icu_6712FormatParser14isQuoteLiteralERKNS_13UnicodeStringE:
.LFB3708:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L1961
	movswl	%ax, %edx
	sarl	$5, %edx
.L1962:
	xorl	%r8d, %r8d
	testl	%edx, %edx
	je	.L1960
	testb	$2, %al
	jne	.L1968
	movq	24(%rdi), %rdi
.L1965:
	cmpw	$39, (%rdi)
	sete	%r8b
.L1960:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1968:
	addq	$10, %rdi
	jmp	.L1965
	.p2align 4,,10
	.p2align 3
.L1961:
	movl	12(%rdi), %edx
	jmp	.L1962
	.cfi_endproc
.LFE3708:
	.size	_ZN6icu_6712FormatParser14isQuoteLiteralERKNS_13UnicodeStringE, .-_ZN6icu_6712FormatParser14isQuoteLiteralERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FormatParser15getQuoteLiteralERNS_13UnicodeStringEPi
	.type	_ZN6icu_6712FormatParser15getQuoteLiteralERNS_13UnicodeStringEPi, @function
_ZN6icu_6712FormatParser15getQuoteLiteralERNS_13UnicodeStringEPi:
.LFB3709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movslq	(%rdx), %rsi
	movzwl	8(%r14), %edx
	movl	%edx, %eax
	movq	%rsi, %r12
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movq	%rsi, %rdx
	salq	$6, %rdx
	leaq	8(%rdi,%rdx), %r8
	movw	%ax, 8(%r14)
	movzwl	8(%r8), %eax
	testw	%ax, %ax
	js	.L1971
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1972:
	testl	%ecx, %ecx
	je	.L1973
	testb	$2, %al
	jne	.L2003
	movq	%rsi, %rax
	salq	$6, %rax
	movq	32(%rbx,%rax), %rax
	cmpw	$39, (%rax)
	je	.L2004
.L1973:
	movl	3208(%rbx), %edx
	cmpl	%edx, %r12d
	jge	.L1976
.L1977:
	movslq	%r12d, %rsi
	movq	%rsi, %rdi
	salq	$6, %rdi
	leaq	8(%rbx,%rdi), %r8
	movzwl	8(%r8), %eax
	testw	%ax, %ax
	js	.L1978
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1979:
	leal	1(%r12), %r15d
	testl	%ecx, %ecx
	je	.L1980
	testb	$2, %al
	je	.L1981
	leaq	18(%rbx,%rdi), %rax
	cmpw	$39, (%rax)
	je	.L2005
.L1980:
	xorl	%edx, %edx
	movq	%r8, %rsi
	movq	%r14, %rdi
	movl	%r15d, %r12d
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
.L1991:
	movl	3208(%rbx), %edx
	cmpl	%r12d, %edx
	jg	.L1977
.L1976:
	movl	%r12d, 0(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1978:
	.cfi_restore_state
	movl	20(%rbx,%rdi), %ecx
	jmp	.L1979
	.p2align 4,,10
	.p2align 3
.L1981:
	movq	%rsi, %rax
	salq	$6, %rax
	movq	32(%rbx,%rax), %rax
	cmpw	$39, (%rax)
	jne	.L1980
.L2005:
	cmpl	%r15d, %edx
	jle	.L1983
	leaq	72(%rbx,%rdi), %r9
	movzwl	8(%r9), %eax
	testw	%ax, %ax
	js	.L1984
	movswl	%ax, %edx
	sarl	$5, %edx
.L1985:
	testl	%edx, %edx
	je	.L1983
	testb	$2, %al
	jne	.L2006
	movslq	%r15d, %rax
	salq	$6, %rax
	movq	32(%rbx,%rax), %rax
.L1987:
	cmpw	$39, (%rax)
	je	.L1988
.L1983:
	xorl	%edx, %edx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L1976
	.p2align 4,,10
	.p2align 3
.L2003:
	leaq	18(%rbx,%rdx), %rax
	cmpw	$39, (%rax)
	jne	.L1973
.L2004:
	xorl	%edx, %edx
	movq	%r8, %rsi
	movq	%r14, %rdi
	addl	$1, %r12d
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L2006:
	leaq	82(%rbx,%rdi), %rax
	jmp	.L1987
	.p2align 4,,10
	.p2align 3
.L1971:
	movl	20(%rdi,%rdx), %ecx
	jmp	.L1972
	.p2align 4,,10
	.p2align 3
.L1984:
	movslq	%r15d, %rdx
	salq	$6, %rdx
	movl	20(%rbx,%rdx), %edx
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L1988:
	xorl	%edx, %edx
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r9, -56(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-56(%rbp), %r9
	addl	$2, %r12d
	movzwl	8(%r9), %eax
	testw	%ax, %ax
	js	.L1989
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1990:
	xorl	%edx, %edx
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L1991
.L1989:
	movslq	%r15d, %r15
	salq	$6, %r15
	movl	20(%rbx,%r15), %ecx
	jmp	.L1990
	.cfi_endproc
.LFE3709:
	.size	_ZN6icu_6712FormatParser15getQuoteLiteralERNS_13UnicodeStringEPi, .-_ZN6icu_6712FormatParser15getQuoteLiteralERNS_13UnicodeStringEPi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE
	.type	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE, @function
_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE:
.LFB3689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-128(%rbp), %rbx
	subq	$152, %rsp
	movq	%rdi, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	8(%rcx), %rax
	movups	%xmm0, 8(%rcx)
	movq	%rax, -184(%rbp)
	leaq	-132(%rbp), %rax
	movb	$0, 136(%rcx)
	movups	%xmm0, 24(%rcx)
	movups	%xmm0, 40(%rcx)
	movups	%xmm0, 56(%rcx)
	movups	%xmm0, 72(%rcx)
	movups	%xmm0, 88(%rcx)
	movups	%xmm0, 104(%rcx)
	movups	%xmm0, 120(%rcx)
	movl	$16, -136(%rbp)
	movl	$0, -132(%rbp)
	movl	$0, 3208(%rdx)
	movq	%rax, -160(%rbp)
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2048:
	movl	-132(%rbp), %ecx
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	movslq	3208(%r13), %rax
	movq	%rbx, %rsi
	leal	1(%rax), %ecx
	salq	$6, %rax
	movl	%ecx, 3208(%r13)
	leaq	8(%r13,%rax), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addl	-132(%rbp), %r15d
	cmpl	$49, 3208(%r13)
	jg	.L2047
.L2010:
	movq	0(%r13), %rax
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	-160(%rbp), %rcx
	call	*16(%rax)
	cmpl	$1, %eax
	je	.L2048
	movl	3208(%r13), %edx
	movl	$0, -136(%rbp)
	testl	%edx, %edx
	jle	.L2011
.L2035:
	leaq	-136(%rbp), %rax
	xorl	%r15d, %r15d
	movq	%rax, -192(%rbp)
	.p2align 4,,10
	.p2align 3
.L2022:
	movslq	%r15d, %r11
	movq	%r11, %rsi
	salq	$6, %rsi
	leaq	8(%r13,%rsi), %r12
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L2012
	movswl	%ax, %ecx
	movl	%ecx, %ebx
	sarl	$5, %ebx
	testl	%ebx, %ebx
	je	.L2014
.L2052:
	testb	$2, %al
	je	.L2015
	leaq	18(%r13,%rsi), %rax
	movzwl	(%rax), %r10d
	cmpw	$39, %r10w
	je	.L2049
.L2017:
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%r11, -168(%rbp)
	movl	%r10d, -160(%rbp)
	call	_ZN6icu_6712FormatParser17getCanonicalIndexERKNS_13UnicodeStringEa
	movl	-160(%rbp), %r10d
	movq	-168(%rbp), %r11
	testl	%eax, %eax
	js	.L2018
	cltq
	leaq	_ZN6icu_67L7dtTypesE(%rip), %rdx
	movq	%rax, %rsi
	salq	$4, %rsi
	movslq	4(%rdx,%rsi), %rsi
.L2032:
	salq	$4, %rax
	leaq	_ZN6icu_67L7dtTypesE(%rip), %rdx
	movb	%r10b, 72(%r14,%rsi)
	addq	%rdx, %rax
	movb	%bl, 88(%r14,%rsi)
	movzwl	10(%rax), %ecx
	movzwl	(%rax), %r10d
	movzwl	8(%rax), %eax
	movb	%r10b, 104(%r14,%rsi)
	movb	%cl, 120(%r14,%rsi)
	testw	%ax, %ax
	jle	.L2019
	movswl	8(%r12), %ecx
	testw	%cx, %cx
	js	.L2020
	sarl	$5, %ecx
.L2021:
	addl	%ecx, %eax
.L2019:
	cwtl
	movl	%eax, 8(%r14,%rsi,4)
.L2018:
	addl	$1, %r15d
	movl	%r15d, -136(%rbp)
	cmpl	%r15d, 3208(%r13)
	jg	.L2022
.L2011:
	cmpb	$0, 100(%r14)
	je	.L2023
	cmpb	$0, 102(%r14)
	jne	.L2050
.L2023:
	cmpb	$0, 99(%r14)
	je	.L2028
	movzbl	83(%r14), %eax
	cmpb	$75, %al
	je	.L2029
	cmpb	$104, %al
	je	.L2029
	movb	$0, 82(%r14)
	movb	$0, 98(%r14)
	movb	$0, 114(%r14)
	movb	$0, 130(%r14)
	movl	$0, 48(%r14)
.L2028:
	movdqu	8(%r14), %xmm1
	movq	-176(%rbp), %rdx
	movq	-184(%rbp), %rbx
	movups	%xmm1, 16(%rdx)
	movdqu	16(%rbx), %xmm2
	movups	%xmm2, 32(%rdx)
	movdqu	32(%rbx), %xmm4
	movups	%xmm4, 48(%rdx)
	movdqu	48(%rbx), %xmm6
	movaps	%xmm4, -160(%rbp)
	movups	%xmm6, 64(%rdx)
	movdqu	72(%r14), %xmm7
	movups	%xmm7, 80(%rdx)
	movdqu	88(%r14), %xmm1
	movups	%xmm1, 96(%rdx)
	movdqu	104(%r14), %xmm2
	movups	%xmm2, 112(%rdx)
	movdqu	120(%r14), %xmm3
	movups	%xmm3, 128(%rdx)
	movzbl	136(%r14), %eax
	movb	%al, 144(%rdx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2051
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2012:
	.cfi_restore_state
	movl	20(%r13,%rsi), %ebx
	testl	%ebx, %ebx
	jne	.L2052
.L2014:
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%r11, -160(%rbp)
	call	_ZN6icu_6712FormatParser17getCanonicalIndexERKNS_13UnicodeStringEa
	movq	-160(%rbp), %r11
	testl	%eax, %eax
	js	.L2018
	cltq
	leaq	_ZN6icu_67L7dtTypesE(%rip), %rdi
	movl	$-1, %r10d
	movq	%rax, %rsi
	salq	$4, %rsi
	movslq	4(%rdi,%rsi), %rsi
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L2015:
	movq	%r11, %rax
	salq	$6, %rax
	movq	32(%r13,%rax), %rax
	movzwl	(%rax), %r10d
	cmpw	$39, %r10w
	jne	.L2017
.L2049:
	movq	-192(%rbp), %rdx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	movq	%r12, %rsi
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	call	_ZN6icu_6712FormatParser15getQuoteLiteralERNS_13UnicodeStringEPi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-136(%rbp), %r15d
	jmp	.L2018
	.p2align 4,,10
	.p2align 3
.L2020:
	salq	$6, %r11
	movl	20(%r13,%r11), %ecx
	jmp	.L2021
	.p2align 4,,10
	.p2align 3
.L2050:
	cmpb	$0, 101(%r14)
	jne	.L2023
	leaq	4+_ZN6icu_67L7dtTypesE(%rip), %rax
	xorl	%esi, %esi
	movl	$71, %ecx
	xorl	%edx, %edx
	movl	$0, -136(%rbp)
	jmp	.L2027
	.p2align 4,,10
	.p2align 3
.L2024:
	movzwl	12(%rax), %ecx
	addq	$16, %rax
	addl	$1, %edx
	movl	$1, %esi
	testw	%cx, %cx
	je	.L2053
.L2027:
	cmpl	$13, (%rax)
	jne	.L2024
	testb	%sil, %sil
	je	.L2025
	movl	%edx, -136(%rbp)
.L2025:
	movslq	%edx, %rdx
	leaq	_ZN6icu_67L7dtTypesE(%rip), %rax
	movb	%cl, 85(%r14)
	salq	$4, %rdx
	movb	%cl, 117(%r14)
	addq	%rax, %rdx
	movzwl	10(%rdx), %eax
	movb	%al, 101(%r14)
	movb	%al, 133(%r14)
	movswl	8(%rdx), %eax
	testw	%ax, %ax
	setg	%dl
	movzbl	%dl, %edx
	addl	%edx, %eax
	movl	%eax, 60(%r14)
	jmp	.L2023
	.p2align 4,,10
	.p2align 3
.L2029:
	cmpb	$0, 98(%r14)
	jne	.L2028
	leaq	4+_ZN6icu_67L7dtTypesE(%rip), %rax
	movl	$71, %ecx
	xorl	%edx, %edx
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2030:
	movzwl	12(%rax), %ecx
	addq	$16, %rax
	addl	$1, %edx
	testw	%cx, %cx
	je	.L2028
.L2031:
	cmpl	$10, (%rax)
	jne	.L2030
	movslq	%edx, %rdx
	leaq	_ZN6icu_67L7dtTypesE(%rip), %rax
	movb	%cl, 82(%r14)
	salq	$4, %rdx
	movb	%cl, 114(%r14)
	addq	%rax, %rdx
	movb	$1, 136(%r14)
	movzwl	10(%rdx), %eax
	movb	%al, 98(%r14)
	movb	%al, 130(%r14)
	movswl	8(%rdx), %eax
	movl	%eax, 48(%r14)
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2047:
	movl	$0, -136(%rbp)
	jmp	.L2035
.L2053:
	movl	%edx, -136(%rbp)
	jmp	.L2023
.L2051:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3689:
	.size	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE, .-_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserE
	.type	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserE, @function
_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserE:
.LFB3688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-160(%rbp), %rcx
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	movb	$0, -24(%rbp)
	movq	%rax, -160(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movups	%xmm0, -40(%rbp)
	call	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2057
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2057:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3688:
	.size	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserE, .-_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712FormatParser18isPatternSeparatorERKNS_13UnicodeStringE
	.type	_ZNK6icu_6712FormatParser18isPatternSeparatorERKNS_13UnicodeStringE, @function
_ZNK6icu_6712FormatParser18isPatternSeparatorERKNS_13UnicodeStringE:
.LFB3710:
	.cfi_startproc
	endbr64
	movswl	8(%rsi), %eax
	movl	%eax, %edx
	sarl	$5, %eax
	movl	%edx, %ecx
	andl	$2, %ecx
	testw	%dx, %dx
	js	.L2124
	xorl	%edx, %edx
	testw	%cx, %cx
	jne	.L2105
	movabsq	$1152921504673955969, %r10
	leaq	18(%rdi), %r11
	cmpl	%edx, %eax
	jle	.L2108
.L2126:
	jbe	.L2090
	movq	24(%rsi), %rcx
	movzwl	(%rcx,%rdx,2), %ecx
	leal	-32(%rcx), %r8d
	cmpw	$60, %r8w
	ja	.L2090
	btq	%r8, %r10
	jnc	.L2125
.L2087:
	addq	$1, %rdx
	cmpl	%edx, %eax
	jg	.L2126
.L2108:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2105:
	movabsq	$1152921504673955969, %r10
	leaq	18(%rdi), %r11
	cmpl	%edx, %eax
	jle	.L2108
	jbe	.L2094
.L2128:
	movzwl	10(%rsi,%rdx,2), %ecx
	leal	-32(%rcx), %r8d
	cmpw	$60, %r8w
	ja	.L2094
	btq	%r8, %r10
	jnc	.L2127
.L2095:
	addq	$1, %rdx
	cmpl	%edx, %eax
	jle	.L2108
	ja	.L2128
.L2094:
	movq	%rdx, %rcx
	salq	$6, %rcx
	movzwl	16(%rdi,%rcx), %r8d
	testw	%r8w, %r8w
	js	.L2096
	movswl	%r8w, %r9d
	sarl	$5, %r9d
.L2097:
	testl	%r9d, %r9d
	je	.L2110
	andl	$2, %r8d
	je	.L2098
	addq	%r11, %rcx
.L2099:
	cmpw	$46, (%rcx)
	je	.L2095
.L2110:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2124:
	movl	12(%rsi), %r8d
	xorl	%eax, %eax
	testw	%cx, %cx
	jne	.L2060
	movabsq	$1152921504673955969, %r10
	leaq	18(%rdi), %r11
.L2068:
	cmpl	%eax, %r8d
	jle	.L2108
	jbe	.L2062
	movq	24(%rsi), %rdx
	movzwl	(%rdx,%rax,2), %edx
	leal	-32(%rdx), %ecx
	cmpw	$60, %cx
	ja	.L2062
	btq	%rcx, %r10
	jnc	.L2129
.L2063:
	addq	$1, %rax
	jmp	.L2068
	.p2align 4,,10
	.p2align 3
.L2125:
	leal	-44(%rcx), %r8d
	cmpw	$1, %r8w
	jbe	.L2087
	cmpw	$34, %cx
	je	.L2087
	.p2align 4,,10
	.p2align 3
.L2090:
	movq	%rdx, %r9
	salq	$6, %r9
	movswl	16(%rdi,%r9), %ecx
	movl	%ecx, %r8d
	sarl	$5, %ecx
	testw	%r8w, %r8w
	js	.L2120
.L2089:
	testl	%ecx, %ecx
	je	.L2110
	andl	$2, %r8d
	leaq	(%r11,%r9), %rcx
	je	.L2121
	cmpw	$46, (%rcx)
	je	.L2087
.L2131:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2127:
	leal	-44(%rcx), %r8d
	cmpw	$1, %r8w
	jbe	.L2095
	cmpw	$34, %cx
	je	.L2095
	jmp	.L2094
	.p2align 4,,10
	.p2align 3
.L2129:
	leal	-44(%rdx), %ecx
	cmpw	$1, %cx
	jbe	.L2063
	cmpw	$34, %dx
	je	.L2063
	.p2align 4,,10
	.p2align 3
.L2062:
	movq	%rax, %rdx
	salq	$6, %rdx
	movzwl	16(%rdi,%rdx), %ecx
	testw	%cx, %cx
	js	.L2064
	movswl	%cx, %r9d
	sarl	$5, %r9d
.L2065:
	testl	%r9d, %r9d
	je	.L2110
	andl	$2, %ecx
	je	.L2066
	addq	%r11, %rdx
.L2067:
	cmpw	$46, (%rdx)
	je	.L2063
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2060:
	movabsq	$1152921504673955969, %r10
	leaq	18(%rdi), %r11
.L2080:
	cmpl	%eax, %r8d
	jle	.L2108
	jbe	.L2077
	movzwl	10(%rsi,%rax,2), %edx
	leal	-32(%rdx), %ecx
	cmpw	$60, %cx
	ja	.L2077
	btq	%rcx, %r10
	jnc	.L2130
.L2074:
	addq	$1, %rax
	jmp	.L2080
.L2130:
	leal	-44(%rdx), %ecx
	cmpw	$1, %cx
	jbe	.L2074
	cmpw	$34, %dx
	je	.L2074
	.p2align 4,,10
	.p2align 3
.L2077:
	movq	%rax, %r9
	salq	$6, %r9
	movswl	16(%rdi,%r9), %edx
	movl	%edx, %ecx
	sarl	$5, %edx
	testw	%cx, %cx
	js	.L2117
.L2076:
	testl	%edx, %edx
	je	.L2110
	andl	$2, %ecx
	leaq	(%r11,%r9), %rdx
	jne	.L2075
	movq	32(%rdi,%r9), %rdx
.L2075:
	cmpw	$46, (%rdx)
	je	.L2074
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2120:
	movl	20(%rdi,%r9), %ecx
	jmp	.L2089
	.p2align 4,,10
	.p2align 3
.L2096:
	movl	20(%rdi,%rcx), %r9d
	jmp	.L2097
	.p2align 4,,10
	.p2align 3
.L2121:
	movq	32(%rdi,%r9), %rcx
	cmpw	$46, (%rcx)
	jne	.L2131
	jmp	.L2087
	.p2align 4,,10
	.p2align 3
.L2064:
	movl	20(%rdi,%rdx), %r9d
	jmp	.L2065
	.p2align 4,,10
	.p2align 3
.L2117:
	movl	20(%rdi,%r9), %edx
	jmp	.L2076
	.p2align 4,,10
	.p2align 3
.L2098:
	movq	32(%rdi,%rcx), %rcx
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2066:
	movq	32(%rdi,%rdx), %rdx
	jmp	.L2067
	.cfi_endproc
.LFE3710:
	.size	_ZNK6icu_6712FormatParser18isPatternSeparatorERKNS_13UnicodeStringE, .-_ZNK6icu_6712FormatParser18isPatternSeparatorERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator16adjustFieldTypesERKNS_13UnicodeStringEPKNS_11PtnSkeletonEi28UDateTimePatternMatchOptions
	.type	_ZN6icu_6724DateTimePatternGenerator16adjustFieldTypesERKNS_13UnicodeStringEPKNS_11PtnSkeletonEi28UDateTimePatternMatchOptions, @function
_ZN6icu_6724DateTimePatternGenerator16adjustFieldTypesERKNS_13UnicodeStringEPKNS_11PtnSkeletonEi28UDateTimePatternMatchOptions:
.LFB3649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$2, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-128(%rbp), %rbx
	subq	$232, %rsp
	movq	%rdi, -216(%rbp)
	movq	232(%rsi), %r15
	movq	%rcx, -240(%rbp)
	movl	%r8d, -220(%rbp)
	movl	%r9d, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r14w, 8(%rdi)
	xorl	%r14d, %r14d
	movq	%rax, (%rdi)
	leaq	-196(%rbp), %rax
	movl	%r14d, %r12d
	movl	$0, -196(%rbp)
	movq	%rax, %r14
	movl	$0, 3208(%r15)
	movq	%rax, -232(%rbp)
	movq	%rsi, -248(%rbp)
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L2133:
	movl	-196(%rbp), %ecx
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	movslq	3208(%r15), %rax
	movq	%rbx, %rsi
	leal	1(%rax), %ecx
	salq	$6, %rax
	movl	%ecx, 3208(%r15)
	leaq	8(%r15,%rax), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addl	-196(%rbp), %r12d
	cmpl	$49, 3208(%r15)
	jg	.L2257
.L2137:
	movq	(%r15), %rax
	movq	%r14, %rcx
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*16(%rax)
	cmpl	$1, %eax
	je	.L2133
.L2257:
	movq	-248(%rbp), %r12
	movl	$0, -196(%rbp)
	movq	232(%r12), %rdx
	movl	3208(%rdx), %ebx
	testl	%ebx, %ebx
	jle	.L2132
	movl	-224(%rbp), %eax
	movl	-220(%rbp), %ecx
	movl	%eax, %ebx
	shrl	%ecx
	andl	$8192, %ebx
	movl	%ecx, %esi
	movl	%ebx, -252(%rbp)
	movl	%eax, %ebx
	andl	$1, %esi
	andl	$2048, %eax
	andl	$4096, %ebx
	movl	%eax, -248(%rbp)
	xorl	%eax, %eax
	movl	%ebx, -224(%rbp)
	leaq	-192(%rbp), %rbx
	movb	%sil, -253(%rbp)
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2264:
	movswl	%r15w, %r13d
	sarl	$5, %r13d
.L2139:
	movq	232(%r12), %rdi
	testl	%r13d, %r13d
	je	.L2140
	testb	$2, %r15b
	leaq	-182(%rbp), %rax
	cmove	-168(%rbp), %rax
	cmpw	$39, (%rax)
	je	.L2261
.L2140:
	movq	%rbx, %rsi
	call	_ZNK6icu_6712FormatParser18isPatternSeparatorERKNS_13UnicodeStringE
	testb	%al, %al
	jne	.L2260
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712FormatParser17getCanonicalIndexERKNS_13UnicodeStringEa
	testl	%eax, %eax
	js	.L2260
	cltq
	leaq	_ZN6icu_67L7dtTypesE(%rip), %rcx
	movq	%rax, %rdx
	salq	$4, %rdx
	movl	4(%rcx,%rdx), %edx
	cmpl	$13, %edx
	jne	.L2147
	testb	$1, -220(%rbp)
	jne	.L2262
.L2147:
	movq	240(%r12), %rsi
	movslq	%edx, %rcx
	movl	16(%rsi,%rcx,4), %r10d
	testl	%r10d, %r10d
	jne	.L2263
.L2152:
	testw	%r15w, %r15w
	js	.L2176
.L2174:
	movswl	%r15w, %ecx
	sarl	$5, %ecx
.L2177:
	movq	-216(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
.L2144:
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-196(%rbp), %eax
	movq	232(%r12), %rdx
	addl	$1, %eax
	movl	%eax, -196(%rbp)
	cmpl	%eax, 3208(%rdx)
	jle	.L2132
.L2178:
	cltq
	movq	%rbx, %rdi
	salq	$6, %rax
	leaq	8(%rdx,%rax), %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	-184(%rbp), %r15d
	testw	%r15w, %r15w
	jns	.L2264
	movl	-180(%rbp), %r13d
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L2260:
	movq	-216(%rbp), %rdi
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L2144
	.p2align 4,,10
	.p2align 3
.L2176:
	movl	-180(%rbp), %ecx
	jmp	.L2177
	.p2align 4,,10
	.p2align 3
.L2261:
	movq	-232(%rbp), %rdx
	leaq	-128(%rbp), %r13
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r11d
	movq	%r13, %rsi
	movq	%rax, -128(%rbp)
	movw	%r11w, -120(%rbp)
	call	_ZN6icu_6712FormatParser15getQuoteLiteralERNS_13UnicodeStringEPi
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L2142
	sarl	$5, %ecx
.L2143:
	movq	-216(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L2144
	.p2align 4,,10
	.p2align 3
.L2263:
	movsbw	80(%rsi,%rcx), %di
	movsbl	96(%rsi,%rcx), %r14d
	cmpw	$69, %di
	jne	.L2153
	cmpl	$2, %r14d
	movl	$3, %esi
	cmovle	%esi, %r14d
.L2153:
	cmpl	$11, %edx
	je	.L2265
	cmpl	$12, %edx
	je	.L2266
	cmpl	$13, %edx
	jne	.L2161
	movl	-252(%rbp), %esi
	testl	%esi, %esi
	je	.L2188
.L2160:
	cmpq	$0, -240(%rbp)
	je	.L2183
.L2181:
	salq	$4, %rax
	leaq	_ZN6icu_67L7dtTypesE(%rip), %rsi
	movzwl	8(%rsi,%rax), %esi
	movq	-240(%rbp), %rax
	movl	8(%rax,%rcx,4), %r9d
	movsbl	88(%rax,%rcx), %eax
	cmpl	%eax, %r14d
	je	.L2190
	testw	%si, %si
	jle	.L2200
	testl	%r9d, %r9d
	jg	.L2254
.L2190:
	movl	%r13d, %r14d
.L2162:
	movl	%edx, %eax
	andl	$-9, %eax
	cmpl	$3, %eax
	jne	.L2267
.L2164:
	movl	$-1, %esi
	testl	%r13d, %r13d
	je	.L2167
.L2179:
	leaq	-182(%rbp), %rax
	testb	$2, %r15b
	cmove	-168(%rbp), %rax
	movzwl	(%rax), %eax
	movl	%eax, %esi
.L2167:
	cmpl	$11, %edx
	jne	.L2159
.L2180:
	movzwl	4568(%r12), %eax
	testw	%ax, %ax
	je	.L2159
	cmpb	$0, -253(%rbp)
	jne	.L2193
	cmpw	%di, %ax
	je	.L2193
	cmpw	$75, %ax
	jne	.L2202
	cmpw	$104, %di
	jne	.L2202
	movl	$75, %esi
	jmp	.L2159
	.p2align 4,,10
	.p2align 3
.L2132:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2268
	movq	-216(%rbp), %rax
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2142:
	.cfi_restore_state
	movl	-116(%rbp), %ecx
	jmp	.L2143
	.p2align 4,,10
	.p2align 3
.L2266:
	movl	-224(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L2160
.L2188:
	movl	%r13d, %r14d
.L2259:
	movl	%edi, %esi
.L2159:
	movl	%r15d, %eax
	andl	$31, %eax
	andl	$1, %r15d
	movl	$2, %r15d
	cmove	%eax, %r15d
	movw	%r15w, -184(%rbp)
	testl	%r14d, %r14d
	jle	.L2174
	leaq	-198(%rbp), %r13
	movl	%esi, %r15d
	.p2align 4,,10
	.p2align 3
.L2175:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movw	%r15w, -198(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	subl	$1, %r14d
	jne	.L2175
.L2150:
	movzwl	-184(%rbp), %r15d
	jmp	.L2152
	.p2align 4,,10
	.p2align 3
.L2265:
	movl	-248(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L2156
	cmpq	$0, -240(%rbp)
	jne	.L2181
	testl	%r13d, %r13d
	jne	.L2179
.L2258:
	movl	$-1, %esi
	jmp	.L2180
	.p2align 4,,10
	.p2align 3
.L2262:
	movzwl	4432(%r12), %eax
	leaq	4424(%r12), %rsi
	testw	%ax, %ax
	js	.L2148
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L2149:
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	240(%r12), %rax
	movsbw	94(%rax), %r15w
	movsbl	110(%rax), %eax
	testl	%eax, %eax
	jle	.L2150
	xorl	%r14d, %r14d
	leaq	-198(%rbp), %r13
	movq	%r12, -264(%rbp)
	movl	%r14d, %r12d
	movq	%r13, %r14
	movl	%eax, %r13d
	.p2align 4,,10
	.p2align 3
.L2151:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	addl	$1, %r12d
	movw	%r15w, -198(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	%r12d, %r13d
	jne	.L2151
	movq	-264(%rbp), %r12
	movzwl	-184(%rbp), %r15d
	jmp	.L2152
	.p2align 4,,10
	.p2align 3
.L2156:
	movl	%r13d, %r14d
	testl	%r13d, %r13d
	jne	.L2179
	jmp	.L2258
.L2200:
	testl	%r9d, %r9d
	jle	.L2162
.L2254:
	testw	%si, %si
	movl	%edx, %eax
	cmovle	%r13d, %r14d
	andl	$-9, %eax
	cmpl	$3, %eax
	je	.L2164
.L2267:
	cmpl	$6, %edx
	jne	.L2183
.L2165:
	movl	$-1, %esi
	testl	%r13d, %r13d
	jne	.L2179
	jmp	.L2159
.L2202:
	cmpw	$107, %ax
	jne	.L2203
	cmpw	$72, %di
	jne	.L2203
	movl	$107, %esi
	jmp	.L2159
.L2203:
	cmpw	$107, %di
	jne	.L2204
	cmpw	$72, %ax
	je	.L2196
.L2204:
	cmpw	$104, %ax
	jne	.L2159
	movl	%esi, %ecx
	cmpw	$75, %di
	cmovne	%ecx, %eax
	movl	%eax, %esi
	jmp	.L2159
.L2183:
	cmpl	$1, %edx
	jne	.L2259
	cmpw	$89, %di
	jne	.L2165
	jmp	.L2259
	.p2align 4,,10
	.p2align 3
.L2161:
	cmpq	$0, -240(%rbp)
	jne	.L2181
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2148:
	movl	4436(%r12), %ecx
	jmp	.L2149
	.p2align 4,,10
	.p2align 3
.L2193:
	movl	%eax, %esi
	jmp	.L2159
.L2196:
	movl	$72, %esi
	jmp	.L2159
.L2268:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3649:
	.size	_ZN6icu_6724DateTimePatternGenerator16adjustFieldTypesERKNS_13UnicodeStringEPKNS_11PtnSkeletonEi28UDateTimePatternMatchOptions, .-_ZN6icu_6724DateTimePatternGenerator16adjustFieldTypesERKNS_13UnicodeStringEPKNS_11PtnSkeletonEi28UDateTimePatternMatchOptions
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator17replaceFieldTypesERKNS_13UnicodeStringES3_28UDateTimePatternMatchOptionsR10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator17replaceFieldTypesERKNS_13UnicodeStringES3_28UDateTimePatternMatchOptionsR10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator17replaceFieldTypesERKNS_13UnicodeStringES3_28UDateTimePatternMatchOptionsR10UErrorCode:
.LFB3636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$184, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rdi
	movq	%rdi, -40(%rbp)
	xorl	%edi, %edi
	movl	(%r9), %edi
	testl	%edi, %edi
	jle	.L2270
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movq	%rcx, (%rax)
	movl	$2, %ecx
	movw	%cx, 8(%rax)
.L2269:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2275
	addq	$184, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2270:
	.cfi_restore_state
	movq	%rdx, %r13
	movl	4600(%rsi), %edx
	movq	%rsi, %r12
	testl	%edx, %edx
	jle	.L2272
	movl	%edx, (%r9)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rsi
	movl	$2, %edx
	movq	%rsi, (%rax)
	movw	%dx, 8(%rax)
	jmp	.L2269
	.p2align 4,,10
	.p2align 3
.L2272:
	leaq	-192(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	%rcx, %rsi
	movb	$0, -56(%rbp)
	movq	240(%r12), %rdi
	movq	232(%r12), %rdx
	movq	%r14, %rcx
	movups	%xmm0, -120(%rbp)
	movq	%rax, -200(%rbp)
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -192(%rbp)
	movl	%r8d, -204(%rbp)
	call	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	-204(%rbp), %r8d
	movq	%r14, %rdi
	movl	%r8d, %r9d
	xorl	%r8d, %r8d
	call	_ZN6icu_6724DateTimePatternGenerator16adjustFieldTypesERKNS_13UnicodeStringEPKNS_11PtnSkeletonEi28UDateTimePatternMatchOptions
	movq	-200(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-200(%rbp), %rax
	jmp	.L2269
.L2275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3636:
	.size	_ZN6icu_6724DateTimePatternGenerator17replaceFieldTypesERKNS_13UnicodeStringES3_28UDateTimePatternMatchOptionsR10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator17replaceFieldTypesERKNS_13UnicodeStringES3_28UDateTimePatternMatchOptionsR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator17replaceFieldTypesERKNS_13UnicodeStringES3_R10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator17replaceFieldTypesERKNS_13UnicodeStringES3_R10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator17replaceFieldTypesERKNS_13UnicodeStringES3_R10UErrorCode:
.LFB3635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$184, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rdi
	movq	%rdi, -40(%rbp)
	xorl	%edi, %edi
	movl	(%r8), %edi
	testl	%edi, %edi
	jle	.L2277
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movq	%rcx, (%rax)
	movl	$2, %ecx
	movw	%cx, 8(%rax)
.L2276:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2282
	addq	$184, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2277:
	.cfi_restore_state
	movq	%rdx, %r12
	movl	4600(%rsi), %edx
	movq	%rsi, %r13
	testl	%edx, %edx
	jle	.L2279
	movl	%edx, (%r8)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rsi
	movl	$2, %edx
	movq	%rsi, (%rax)
	movw	%dx, 8(%rax)
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2279:
	leaq	-192(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	%rcx, %rsi
	movb	$0, -56(%rbp)
	movq	240(%r13), %rdi
	movq	232(%r13), %rdx
	movq	%r14, %rcx
	movups	%xmm0, -120(%rbp)
	movq	%rax, -200(%rbp)
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator16adjustFieldTypesERKNS_13UnicodeStringEPKNS_11PtnSkeletonEi28UDateTimePatternMatchOptions
	movq	-200(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-200(%rbp), %rax
	jmp	.L2276
.L2282:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3635:
	.size	_ZN6icu_6724DateTimePatternGenerator17replaceFieldTypesERKNS_13UnicodeStringES3_R10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator17replaceFieldTypesERKNS_13UnicodeStringES3_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712DistanceInfo5setToERKS0_
	.type	_ZN6icu_6712DistanceInfo5setToERKS0_, @function
_ZN6icu_6712DistanceInfo5setToERKS0_:
.LFB3715:
	.cfi_startproc
	endbr64
	movl	8(%rsi), %eax
	movl	%eax, 8(%rdi)
	movl	12(%rsi), %eax
	movl	%eax, 12(%rdi)
	ret
	.cfi_endproc
.LFE3715:
	.size	_ZN6icu_6712DistanceInfo5setToERKS0_, .-_ZN6icu_6712DistanceInfo5setToERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718PatternMapIteratorC2ER10UErrorCode
	.type	_ZN6icu_6718PatternMapIteratorC2ER10UErrorCode, @function
_ZN6icu_6718PatternMapIteratorC2ER10UErrorCode:
.LFB3719:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	leaq	16+_ZTVN6icu_6718PatternMapIteratorE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 32(%rdi)
	movups	%xmm0, 16(%rdi)
	testl	%edx, %edx
	jle	.L2303
	ret
	.p2align 4,,10
	.p2align 3
.L2303:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$152, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2287
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	leaq	16+_ZTVN6icu_6715DateTimeMatcherE(%rip), %rcx
	movb	$0, 144(%r13)
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 80(%r13)
	movups	%xmm0, 96(%r13)
	movups	%xmm0, 112(%r13)
	movups	%xmm0, 128(%r13)
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L2304
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2305
.L2294:
	movq	(%rdi), %rax
	leaq	_ZN6icu_6715DateTimeMatcherD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2290
	call	_ZN6icu_677UMemorydlEPv@PLT
.L2291:
	movq	%r13, 24(%rbx)
	testq	%r13, %r13
	je	.L2296
.L2284:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2304:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L2305:
	.cfi_restore_state
	movq	%r13, 24(%rbx)
	jmp	.L2284
.L2287:
	cmpl	$0, (%r12)
	jg	.L2284
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L2294
	.p2align 4,,10
	.p2align 3
.L2296:
	movl	$7, (%r12)
	jmp	.L2284
	.p2align 4,,10
	.p2align 3
.L2290:
	call	*%rax
	jmp	.L2291
	.cfi_endproc
.LFE3719:
	.size	_ZN6icu_6718PatternMapIteratorC2ER10UErrorCode, .-_ZN6icu_6718PatternMapIteratorC2ER10UErrorCode
	.globl	_ZN6icu_6718PatternMapIteratorC1ER10UErrorCode
	.set	_ZN6icu_6718PatternMapIteratorC1ER10UErrorCode,_ZN6icu_6718PatternMapIteratorC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718PatternMapIterator3setERNS_10PatternMapE
	.type	_ZN6icu_6718PatternMapIterator3setERNS_10PatternMapE, @function
_ZN6icu_6718PatternMapIterator3setERNS_10PatternMapE:
.LFB3725:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	ret
	.cfi_endproc
.LFE3725:
	.size	_ZN6icu_6718PatternMapIterator3setERNS_10PatternMapE, .-_ZN6icu_6718PatternMapIterator3setERNS_10PatternMapE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718PatternMapIterator11getSkeletonEv
	.type	_ZNK6icu_6718PatternMapIterator11getSkeletonEv, @function
_ZNK6icu_6718PatternMapIterator11getSkeletonEv:
.LFB3726:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L2307
	movq	72(%rax), %rax
.L2307:
	ret
	.cfi_endproc
.LFE3726:
	.size	_ZNK6icu_6718PatternMapIterator11getSkeletonEv, .-_ZNK6icu_6718PatternMapIterator11getSkeletonEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718PatternMapIterator7hasNextEv
	.type	_ZNK6icu_6718PatternMapIterator7hasNextEv, @function
_ZNK6icu_6718PatternMapIterator7hasNextEv:
.LFB3727:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	movq	32(%rdi), %rdx
	cmpl	$51, %eax
	jg	.L2318
	testq	%rdx, %rdx
	je	.L2318
	movq	16(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L2314
	cmpq	$0, 152(%rcx)
	movl	$1, %r8d
	je	.L2324
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2324:
	addl	$1, %eax
	cmpl	$52, %eax
	je	.L2318
.L2314:
	movl	$51, %ecx
	movslq	%eax, %rdi
	movl	%ecx, %r9d
	leaq	(%rdx,%rdi,8), %rsi
	subl	%eax, %r9d
	movq	%r9, %rax
	addq	%rdi, %rax
	leaq	8(%rdx,%rax,8), %rax
.L2315:
	cmpq	$0, 8(%rsi)
	je	.L2325
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2325:
	addq	$8, %rsi
	cmpq	%rsi, %rax
	jne	.L2315
.L2318:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3727:
	.size	_ZNK6icu_6718PatternMapIterator7hasNextEv, .-_ZNK6icu_6718PatternMapIterator7hasNextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718PatternMapIterator4nextEv
	.type	_ZN6icu_6718PatternMapIterator4nextEv, @function
_ZN6icu_6718PatternMapIterator4nextEv:
.LFB3728:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	cmpl	$51, %edx
	jg	.L2327
	movl	%edx, %ecx
	testq	%rax, %rax
	je	.L2334
	movq	152(%rax), %rax
	testq	%rax, %rax
	je	.L2331
.L2349:
	movq	%rax, 16(%rdi)
	movq	24(%rdi), %rdx
.L2338:
	movq	72(%rax), %rax
	movdqu	8(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	movdqu	24(%rax), %xmm2
	movups	%xmm2, 32(%rdx)
	movdqu	40(%rax), %xmm3
	movups	%xmm3, 48(%rdx)
	movdqu	56(%rax), %xmm4
	movups	%xmm4, 64(%rdx)
	movdqu	72(%rax), %xmm5
	movups	%xmm5, 80(%rdx)
	movdqu	88(%rax), %xmm6
	movups	%xmm6, 96(%rdx)
	movdqu	104(%rax), %xmm7
	movups	%xmm7, 112(%rdx)
	movdqu	120(%rax), %xmm1
	movups	%xmm1, 128(%rdx)
	movzbl	136(%rax), %eax
	movb	%al, 144(%rdx)
	movq	24(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2327:
	movq	24(%rdi), %rdx
	movq	%rdx, %rcx
	leaq	16(%rdx), %rsi
	testq	%rax, %rax
	jne	.L2338
.L2330:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
	movups	%xmm0, 16(%rsi)
	movups	%xmm0, 32(%rsi)
	movups	%xmm0, 48(%rsi)
	movups	%xmm0, 80(%rcx)
	movups	%xmm0, 96(%rcx)
	movups	%xmm0, 112(%rcx)
	movups	%xmm0, 128(%rcx)
	movq	24(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2331:
	leal	1(%rdx), %ecx
	movq	$0, 16(%rdi)
	movl	%ecx, 8(%rdi)
	movslq	%ecx, %rdx
	cmpl	$52, %ecx
	je	.L2333
.L2334:
	movq	32(%rdi), %rsi
.L2329:
	movq	8(%rsi,%rdx,8), %rax
	testq	%rax, %rax
	jne	.L2349
	addl	$1, %ecx
	movl	%ecx, 8(%rdi)
	cmpl	$52, %ecx
	je	.L2333
	movslq	%ecx, %rdx
	jmp	.L2329
.L2333:
	movq	24(%rdi), %rcx
	leaq	16(%rcx), %rsi
	jmp	.L2330
	.cfi_endproc
.LFE3728:
	.size	_ZN6icu_6718PatternMapIterator4nextEv, .-_ZN6icu_6718PatternMapIterator4nextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator10getBestRawERNS_15DateTimeMatcherEiPNS_12DistanceInfoER10UErrorCodePPKNS_11PtnSkeletonE
	.type	_ZN6icu_6724DateTimePatternGenerator10getBestRawERNS_15DateTimeMatcherEiPNS_12DistanceInfoER10UErrorCodePPKNS_11PtnSkeletonE, @function
_ZN6icu_6724DateTimePatternGenerator10getBestRawERNS_15DateTimeMatcherEiPNS_12DistanceInfoER10UErrorCodePPKNS_11PtnSkeletonE:
.LFB3648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r15
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%r8, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$264, %rsp
	movq	%rcx, -288(%rbp)
	movq	%r9, -296(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -264(%rbp)
	call	_ZN6icu_6718PatternMapIteratorC1ER10UErrorCode
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L2353
	movq	256(%rbx), %rdx
	movl	-248(%rbp), %eax
	movq	%rdx, -224(%rbp)
	testq	%rdx, %rdx
	je	.L2353
	cmpl	$51, %eax
	jg	.L2353
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rsi
	xorl	%r14d, %r14d
	leaq	16+_ZTVN6icu_6715DateTimeMatcherE(%rip), %rdi
	movl	$-1, -280(%rbp)
	movq	%rdi, %xmm8
	movq	%rsi, %xmm2
	movq	%r14, %r8
	movl	$2147483647, -276(%rbp)
	movq	-240(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm8
.L2354:
	testq	%rcx, %rcx
	je	.L2356
	cmpq	$0, 152(%rcx)
	je	.L2407
.L2357:
	movq	%r15, %rdi
	call	_ZN6icu_6718PatternMapIterator4nextEv
	pxor	%xmm0, %xmm0
	movb	$0, -64(%rbp)
	leaq	-112(%rbp), %rcx
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	movdqu	16(%rax), %xmm1
	movaps	%xmm8, -208(%rbp)
	movaps	%xmm1, -192(%rbp)
	movdqu	32(%rax), %xmm2
	movdqu	128(%rax), %xmm1
	movaps	%xmm2, -176(%rbp)
	movdqu	48(%rax), %xmm3
	movaps	%xmm3, -160(%rbp)
	movdqu	64(%rax), %xmm4
	movaps	%xmm4, -144(%rbp)
	movdqu	80(%rax), %xmm5
	movdqu	96(%rax), %xmm6
	movdqu	112(%rax), %xmm7
	movzbl	144(%rax), %eax
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm5, -128(%rbp)
	movb	%al, -64(%rbp)
	movq	4488(%rbx), %rax
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	testq	%rax, %rax
	je	.L2380
	movq	-120(%rbp), %rsi
	movq	-128(%rbp), %rdx
	xorq	88(%rax), %rsi
	xorq	80(%rax), %rdx
	orq	%rdx, %rsi
	jne	.L2380
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	xorl	%r10d, %r10d
	xorq	104(%rax), %rcx
	xorq	96(%rax), %rdx
	orq	%rdx, %rcx
	je	.L2364
	xorl	%r14d, %r14d
	xorl	%r11d, %r11d
	xorl	%edx, %edx
.L2361:
	leaq	-208(%rbp), %r9
	movl	$1, %edi
	jmp	.L2370
	.p2align 4,,10
	.p2align 3
.L2409:
	movl	16(%r9,%rdx,4), %eax
	testl	%eax, %eax
	je	.L2366
.L2367:
	movl	%edi, %eax
	addl	$65536, %r10d
	sall	%cl, %eax
	orl	%eax, %r14d
.L2366:
	addq	$1, %rdx
	cmpq	$16, %rdx
	je	.L2408
.L2370:
	btl	%edx, %r13d
	movl	%edx, %ecx
	jnc	.L2409
	movl	16(%r12,%rdx,4), %eax
	movl	16(%r9,%rdx,4), %esi
	cmpl	%esi, %eax
	je	.L2366
	testl	%eax, %eax
	je	.L2367
	testl	%esi, %esi
	jne	.L2369
	movl	%edi, %eax
	addq	$1, %rdx
	addl	$4096, %r10d
	sall	%cl, %eax
	orl	%eax, %r11d
	cmpq	$16, %rdx
	jne	.L2370
	.p2align 4,,10
	.p2align 3
.L2408:
	cmpl	%r10d, -276(%rbp)
	jg	.L2371
	je	.L2410
.L2364:
	movl	-248(%rbp), %eax
	movq	-224(%rbp), %rdx
	cmpl	$51, %eax
	jg	.L2405
	testq	%rdx, %rdx
	je	.L2405
	movq	-240(%rbp), %rcx
	jmp	.L2354
	.p2align 4,,10
	.p2align 3
.L2369:
	subl	%esi, %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	xorl	%ecx, %eax
	subl	%ecx, %eax
	addl	%eax, %r10d
	jmp	.L2366
	.p2align 4,,10
	.p2align 3
.L2380:
	xorl	%edx, %edx
	xorl	%r14d, %r14d
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	jmp	.L2361
	.p2align 4,,10
	.p2align 3
.L2410:
	cmpl	-280(%rbp), %r11d
	jle	.L2364
.L2371:
	movq	256(%rbx), %rdi
	leaq	-264(%rbp), %rdx
	leaq	-200(%rbp), %rsi
	call	_ZNK6icu_6710PatternMap22getPatternFromSkeletonERKNS_11PtnSkeletonEPPS2_
	movq	%rax, %r8
	movq	-288(%rbp), %rax
	movl	%r11d, 8(%rax)
	movl	%r14d, 12(%rax)
	testl	%r10d, %r10d
	je	.L2405
	movl	%r11d, -280(%rbp)
	movl	%r10d, -276(%rbp)
	jmp	.L2364
	.p2align 4,,10
	.p2align 3
.L2407:
	addl	$1, %eax
	cmpl	$52, %eax
	je	.L2405
.L2356:
	movl	$51, %edi
	movslq	%eax, %rsi
	subl	%eax, %edi
	leaq	(%rdx,%rsi,8), %rcx
	movq	%rdi, %rax
	addq	%rsi, %rax
	leaq	8(%rdx,%rax,8), %rax
.L2358:
	cmpq	$0, 8(%rcx)
	jne	.L2357
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.L2358
	.p2align 4,,10
	.p2align 3
.L2405:
	movq	%r8, %r14
	testq	%r8, %r8
	je	.L2352
	movq	-296(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2352
	movq	-264(%rbp), %rax
	movq	%rax, (%rbx)
.L2352:
	movq	-232(%rbp), %rdi
	leaq	16+_ZTVN6icu_6718PatternMapIteratorE(%rip), %rax
	movq	%rax, -256(%rbp)
	testq	%rdi, %rdi
	je	.L2350
	movq	(%rdi), %rax
	leaq	_ZN6icu_6715DateTimeMatcherD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2375
	call	_ZN6icu_677UMemorydlEPv@PLT
.L2350:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2411
	addq	$264, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2353:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L2352
	.p2align 4,,10
	.p2align 3
.L2375:
	call	*%rax
	jmp	.L2350
.L2411:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3648:
	.size	_ZN6icu_6724DateTimePatternGenerator10getBestRawERNS_15DateTimeMatcherEiPNS_12DistanceInfoER10UErrorCodePPKNS_11PtnSkeletonE, .-_ZN6icu_6724DateTimePatternGenerator10getBestRawERNS_15DateTimeMatcherEiPNS_12DistanceInfoER10UErrorCodePPKNS_11PtnSkeletonE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator16getBestAppendingEiiR10UErrorCode28UDateTimePatternMatchOptions
	.type	_ZN6icu_6724DateTimePatternGenerator16getBestAppendingEiiR10UErrorCode28UDateTimePatternMatchOptions, @function
_ZN6icu_6724DateTimePatternGenerator16getBestAppendingEiiR10UErrorCode28UDateTimePatternMatchOptions:
.LFB3650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -384(%rbp)
	movl	(%r8), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L2413
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r8d
	movq	%rax, (%rdi)
	movw	%r8w, 8(%rdi)
.L2412:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2447
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2413:
	.cfi_restore_state
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	movl	$2, %edi
	movq	%rax, -320(%rbp)
	movw	%cx, -312(%rbp)
	movq	%rax, -256(%rbp)
	movw	%di, -248(%rbp)
	testl	%edx, %edx
	jne	.L2444
	leaq	-320(%rbp), %rax
	movq	%rax, -392(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, -376(%rbp)
.L2415:
	movq	-392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
.L2430:
	movq	-376(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-392(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L2412
	.p2align 4,,10
	.p2align 3
.L2444:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsi, %rbx
	movq	%r8, %r14
	movl	%r9d, %r15d
	movl	$2, %r13d
	movq	%rax, -128(%rbp)
	leaq	-320(%rbp), %rax
	movw	%r13w, -120(%rbp)
	leaq	-128(%rbp), %r13
	movq	%rax, %rdi
	movq	%r13, %rsi
	movl	%edx, -376(%rbp)
	movq	%rax, -392(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-376(%rbp), %r11d
	movq	%r14, %r8
	leaq	-360(%rbp), %r10
	movq	248(%rbx), %rcx
	movq	240(%rbx), %rsi
	movq	%r10, %r9
	movq	%rbx, %rdi
	movl	%r11d, %edx
	movq	%r10, -400(%rbp)
	movq	$0, -360(%rbp)
	call	_ZN6icu_6724DateTimePatternGenerator10getBestRawERNS_15DateTimeMatcherEiPNS_12DistanceInfoER10UErrorCodePPKNS_11PtnSkeletonE
	movl	(%r14), %edx
	movl	-376(%rbp), %r11d
	movq	-400(%rbp), %r10
	testl	%edx, %edx
	jle	.L2416
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r11d
	movq	%rax, (%r12)
	leaq	-256(%rbp), %rax
	movw	%r11w, 8(%r12)
	movq	%rax, -376(%rbp)
	jmp	.L2430
	.p2align 4,,10
	.p2align 3
.L2416:
	leaq	-256(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r10, -416(%rbp)
	movq	%rdx, %rdi
	movl	%r11d, -408(%rbp)
	movq	%rdx, -376(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	%r15d, %r9d
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	-360(%rbp), %rcx
	movl	-384(%rbp), %r8d
	movq	-376(%rbp), %rdx
	call	_ZN6icu_6724DateTimePatternGenerator16adjustFieldTypesERKNS_13UnicodeStringEPKNS_11PtnSkeletonEi28UDateTimePatternMatchOptions
	movq	-392(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	248(%rbx), %rcx
	movl	-408(%rbp), %r11d
	movq	-416(%rbp), %r10
	movl	8(%rcx), %eax
	testl	%eax, %eax
	movl	%eax, -400(%rbp)
	je	.L2415
	andl	$24576, %r11d
	cmpl	$24576, %r11d
	je	.L2448
.L2420:
	movq	240(%rbx), %rsi
	movq	%r10, %r9
	movq	%r14, %r8
	movq	%rbx, %rdi
	movl	-400(%rbp), %edx
	call	_ZN6icu_6724DateTimePatternGenerator10getBestRawERNS_15DateTimeMatcherEiPNS_12DistanceInfoER10UErrorCodePPKNS_11PtnSkeletonE
	movl	(%r14), %r10d
	movq	%rax, %rsi
	testl	%r10d, %r10d
	jg	.L2449
	movq	-376(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	%r15d, %r9d
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	-376(%rbp), %r15
	movl	-384(%rbp), %r8d
	movq	-360(%rbp), %rcx
	movq	%r15, %rdx
	call	_ZN6icu_6724DateTimePatternGenerator16adjustFieldTypesERKNS_13UnicodeStringEPKNS_11PtnSkeletonEi28UDateTimePatternMatchOptions
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	248(%rbx), %rax
	movl	-400(%rbp), %r15d
	movl	8(%rax), %eax
	notl	%eax
	andl	%eax, %r15d
	je	.L2432
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2425:
	movl	%eax, %edx
	addl	$1, %eax
	sarl	%r15d
	jne	.L2425
	cmpl	$16, %eax
	jg	.L2433
	movslq	%edx, %r11
	movl	%edx, %r15d
	movq	%r11, %rax
	salq	$6, %rax
	addq	$264, %rax
.L2424:
	leaq	(%rbx,%rax), %r10
	movswl	8(%r10), %eax
	testw	%ax, %ax
	js	.L2426
	sarl	$5, %eax
.L2427:
	testl	%eax, %eax
	je	.L2415
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rax, -192(%rbp)
	movl	$39, %r8d
	leaq	-192(%rbp), %rax
	leaq	-362(%rbp), %rcx
	movw	%di, -184(%rbp)
	movl	$1, %r9d
	movq	%rax, %rdi
	movw	%r8w, -362(%rbp)
	xorl	%r8d, %r8d
	movq	%r11, -416(%rbp)
	movq	%r10, -408(%rbp)
	movq	%rcx, -400(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movq	-416(%rbp), %r11
	movq	-408(%rbp), %r10
	leaq	(%r11,%r11,2), %rax
	salq	$6, %rax
	leaq	1288(%rbx,%rax), %rsi
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L2428
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L2429:
	movq	-384(%rbp), %rbx
	xorl	%edx, %edx
	movq	%r10, -408(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$39, %eax
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	-400(%rbp), %rsi
	movl	$1, %ecx
	movw	%ax, -362(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$2, %edx
	movq	%r14, %r8
	movq	%r13, %rdi
	movq	-392(%rbp), %r15
	movw	%dx, -112(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$3, %ecx
	movq	-408(%rbp), %r10
	movl	$2, %edx
	movq	%rbx, -336(%rbp)
	movq	%r15, %xmm0
	movq	%rax, -120(%rbp)
	movhps	-376(%rbp), %xmm0
	movq	%r10, %rsi
	movaps	%xmm0, -352(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	subq	$8, %rsp
	movq	%r15, %rcx
	xorl	%r9d, %r9d
	pushq	%r14
	leaq	-352(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	$3, %edx
	call	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L2415
	.p2align 4,,10
	.p2align 3
.L2426:
	movslq	%r15d, %rax
	salq	$6, %rax
	movl	276(%rax,%rbx), %eax
	jmp	.L2427
	.p2align 4,,10
	.p2align 3
.L2432:
	xorl	%r11d, %r11d
	movl	$264, %eax
	jmp	.L2424
	.p2align 4,,10
	.p2align 3
.L2433:
	movl	$15, %r11d
	movl	$1224, %eax
	movl	$15, %r15d
	jmp	.L2424
	.p2align 4,,10
	.p2align 3
.L2428:
	movslq	%r15d, %r15
	leaq	(%r15,%r15,2), %rax
	salq	$6, %rax
	movl	1300(%rbx,%rax), %ecx
	jmp	.L2429
	.p2align 4,,10
	.p2align 3
.L2449:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r9d
	movq	%rax, (%r12)
	movw	%r9w, 8(%r12)
	jmp	.L2430
.L2448:
	movl	-384(%rbp), %r8d
	movq	%r10, -424(%rbp)
	movq	%r12, -408(%rbp)
	movq	%rbx, %r12
	movq	-392(%rbp), %rbx
	orl	$1, %r8d
	movq	%r14, -416(%rbp)
	movl	%r15d, %r14d
	movl	%r8d, %r15d
	.p2align 4,,10
	.p2align 3
.L2419:
	movl	%eax, %edx
	andl	$24576, %edx
	cmpl	$16384, %edx
	je	.L2450
	movq	%r12, %rbx
	movl	%r14d, %r15d
	movl	%eax, -400(%rbp)
	movq	-408(%rbp), %r12
	movq	-416(%rbp), %r14
	movq	-424(%rbp), %r10
	jmp	.L2420
	.p2align 4,,10
	.p2align 3
.L2450:
	movq	-360(%rbp), %rcx
	movl	%r14d, %r9d
	movl	%r15d, %r8d
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator16adjustFieldTypesERKNS_13UnicodeStringEPKNS_11PtnSkeletonEi28UDateTimePatternMatchOptions
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	248(%r12), %rcx
	movl	8(%rcx), %eax
	andb	$-65, %ah
	movl	%eax, 8(%rcx)
	testl	%eax, %eax
	jne	.L2419
	movq	-408(%rbp), %r12
	jmp	.L2415
.L2447:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3650:
	.size	_ZN6icu_6724DateTimePatternGenerator16getBestAppendingEiiR10UErrorCode28UDateTimePatternMatchOptions, .-_ZN6icu_6724DateTimePatternGenerator16getBestAppendingEiiR10UErrorCode28UDateTimePatternMatchOptions
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringE28UDateTimePatternMatchOptionsR10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringE28UDateTimePatternMatchOptionsR10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringE28UDateTimePatternMatchOptionsR10UErrorCode:
.LFB3633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$552, %rsp
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L2452
.L2494:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r8d
	movq	%rax, (%r12)
	movw	%r8w, 8(%r12)
.L2451:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2495
	addq	$552, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2452:
	.cfi_restore_state
	movl	4600(%rsi), %eax
	movq	%rsi, %r13
	testl	%eax, %eax
	jle	.L2454
	movl	%eax, (%r8)
	jmp	.L2494
	.p2align 4,,10
	.p2align 3
.L2454:
	movl	$2, %esi
	movl	%ecx, %ebx
	leaq	-400(%rbp), %r15
	movl	$2, %ecx
	movw	%cx, -520(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r14
	movq	%r15, %rdi
	leaq	-540(%rbp), %rcx
	movw	%si, -456(%rbp)
	movq	%r13, %rsi
	movq	%r8, -552(%rbp)
	movq	%r14, -528(%rbp)
	movq	%r14, -464(%rbp)
	movl	$0, -540(%rbp)
	call	_ZN6icu_6724DateTimePatternGenerator25mapSkeletonMetacharactersERKNS_13UnicodeStringEPiR10UErrorCode
	movq	-552(%rbp), %r8
	movl	(%r8), %edi
	testl	%edi, %edi
	jle	.L2455
.L2493:
	movl	$2, %ebx
	movq	%r14, (%r12)
	leaq	-528(%rbp), %r13
	leaq	-464(%rbp), %r14
	movw	%bx, 8(%r12)
.L2456:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L2451
	.p2align 4,,10
	.p2align 3
.L2455:
	movzwl	-456(%rbp), %edx
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movb	$0, -72(%rbp)
	leaq	-208(%rbp), %r10
	movq	240(%r13), %rdi
	movups	%xmm0, -120(%rbp)
	movl	%edx, %eax
	movq	%r10, %rcx
	movq	%r10, -560(%rbp)
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	movups	%xmm0, -104(%rbp)
	cmovne	%edx, %eax
	movq	232(%r13), %rdx
	movq	%r8, -552(%rbp)
	movups	%xmm0, -136(%rbp)
	movw	%ax, -456(%rbp)
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	movq	%rax, -208(%rbp)
	movups	%xmm0, -88(%rbp)
	call	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE
	movq	-552(%rbp), %r8
	movl	$-1, %edx
	movq	%r13, %rdi
	movq	248(%r13), %rcx
	movq	240(%r13), %rsi
	leaq	-536(%rbp), %r9
	movq	$0, -536(%rbp)
	call	_ZN6icu_6724DateTimePatternGenerator10getBestRawERNS_15DateTimeMatcherEiPNS_12DistanceInfoER10UErrorCodePPKNS_11PtnSkeletonE
	movq	-552(%rbp), %r8
	movq	-560(%rbp), %r10
	movl	(%r8), %edx
	testl	%edx, %edx
	jg	.L2493
	movq	248(%r13), %rdx
	movl	-540(%rbp), %ecx
	cmpq	$0, 8(%rdx)
	je	.L2496
	movq	240(%r13), %rdx
	xorl	%eax, %eax
	movl	16(%rdx), %r9d
	movl	20(%rdx), %r11d
	testl	%r9d, %r9d
	setne	%al
	testl	%r11d, %r11d
	je	.L2460
	orl	$2, %eax
.L2460:
	movl	24(%rdx), %edi
	testl	%edi, %edi
	je	.L2461
	orl	$4, %eax
.L2461:
	movl	28(%rdx), %esi
	testl	%esi, %esi
	je	.L2462
	orl	$8, %eax
.L2462:
	movl	32(%rdx), %r11d
	testl	%r11d, %r11d
	je	.L2463
	orl	$16, %eax
.L2463:
	movl	36(%rdx), %r9d
	testl	%r9d, %r9d
	je	.L2464
	orl	$32, %eax
.L2464:
	movl	40(%rdx), %edi
	testl	%edi, %edi
	je	.L2465
	orl	$64, %eax
.L2465:
	movl	44(%rdx), %esi
	testl	%esi, %esi
	je	.L2466
	orb	$-128, %al
.L2466:
	movl	48(%rdx), %r11d
	testl	%r11d, %r11d
	je	.L2467
	orb	$1, %ah
.L2467:
	movl	52(%rdx), %r9d
	testl	%r9d, %r9d
	je	.L2468
	orb	$2, %ah
.L2468:
	movl	56(%rdx), %edi
	testl	%edi, %edi
	je	.L2469
	orb	$4, %ah
.L2469:
	movl	60(%rdx), %esi
	testl	%esi, %esi
	je	.L2470
	orb	$8, %ah
.L2470:
	movl	64(%rdx), %r11d
	testl	%r11d, %r11d
	je	.L2471
	orb	$16, %ah
.L2471:
	movl	68(%rdx), %r9d
	testl	%r9d, %r9d
	je	.L2472
	orb	$32, %ah
.L2472:
	movl	72(%rdx), %edi
	testl	%edi, %edi
	je	.L2473
	orb	$64, %ah
.L2473:
	movl	76(%rdx), %esi
	testl	%esi, %esi
	je	.L2474
	orb	$-128, %ah
.L2474:
	movl	%eax, %edx
	leaq	-336(%rbp), %rdi
	movl	%ebx, %r9d
	movq	%r13, %rsi
	andl	$1023, %edx
	movq	%r10, -576(%rbp)
	movq	%rdi, -552(%rbp)
	movl	%eax, -568(%rbp)
	movq	%r8, -560(%rbp)
	call	_ZN6icu_6724DateTimePatternGenerator16getBestAppendingEiiR10UErrorCode28UDateTimePatternMatchOptions
	movl	-568(%rbp), %eax
	movl	%ebx, %r9d
	movq	-560(%rbp), %r8
	leaq	-272(%rbp), %r11
	movl	-540(%rbp), %ecx
	movq	%r13, %rsi
	andl	$64512, %eax
	movq	%r11, %rdi
	movq	%r8, -568(%rbp)
	movl	%eax, %edx
	movq	%r11, -560(%rbp)
	call	_ZN6icu_6724DateTimePatternGenerator16getBestAppendingEiiR10UErrorCode28UDateTimePatternMatchOptions
	movq	-568(%rbp), %r8
	movq	-560(%rbp), %r11
	movq	-576(%rbp), %r10
	movl	(%r8), %ecx
	testl	%ecx, %ecx
	jle	.L2475
	movl	$2, %edx
	movq	%r14, (%r12)
	leaq	-528(%rbp), %r13
	leaq	-464(%rbp), %r14
	movw	%dx, 8(%r12)
.L2476:
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-552(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L2456
.L2496:
	movl	%ecx, %r8d
	movq	-536(%rbp), %rcx
	movl	%ebx, %r9d
	movq	%rax, %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -552(%rbp)
	leaq	-464(%rbp), %r14
	call	_ZN6icu_6724DateTimePatternGenerator16adjustFieldTypesERKNS_13UnicodeStringEPKNS_11PtnSkeletonEi28UDateTimePatternMatchOptions
	movq	-552(%rbp), %r10
	movq	%r14, %rdi
	leaq	-528(%rbp), %r13
	movq	%r10, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-552(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	jmp	.L2456
.L2475:
	movzwl	-328(%rbp), %eax
	testw	%ax, %ax
	js	.L2477
	movswl	%ax, %edx
	sarl	$5, %edx
.L2478:
	movswl	-264(%rbp), %eax
	testl	%edx, %edx
	jne	.L2479
	testw	%ax, %ax
	js	.L2480
	movswl	%ax, %edx
	sarl	$5, %edx
.L2481:
	testl	%edx, %edx
	jne	.L2482
	movzwl	-456(%rbp), %edx
	movl	%edx, %ecx
	andl	$31, %ecx
	andl	$1, %edx
	movl	$2, %edx
	cmove	%ecx, %edx
	movw	%dx, -456(%rbp)
.L2479:
	testw	%ax, %ax
	js	.L2484
	sarl	$5, %eax
.L2485:
	testl	%eax, %eax
	je	.L2497
	movzwl	-456(%rbp), %edx
	movl	$0, (%r8)
	leaq	4360(%r13), %rsi
	leaq	-528(%rbp), %r13
	movq	%r13, %rdi
	movq	%r11, -576(%rbp)
	movl	%edx, %eax
	movq	%r10, -584(%rbp)
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	movq	%r8, -560(%rbp)
	cmovne	%edx, %eax
	movw	%ax, -456(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-584(%rbp), %r10
	movl	$2, %eax
	movq	%r13, %rsi
	movq	-560(%rbp), %r8
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r14, -200(%rbp)
	movq	%r10, %rdi
	movq	%r10, -560(%rbp)
	leaq	-464(%rbp), %r14
	movq	%r8, -568(%rbp)
	movw	%ax, -192(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	-576(%rbp), %r11
	movq	%r14, %rcx
	movq	-560(%rbp), %r10
	movq	-568(%rbp), %r8
	movq	-552(%rbp), %rdx
	movq	%r11, %rsi
	movq	%r10, %rdi
	movq	%r11, -568(%rbp)
	call	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_RS1_R10UErrorCode@PLT
	movq	-560(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	movq	-568(%rbp), %r11
	jmp	.L2476
.L2477:
	movl	-324(%rbp), %edx
	jmp	.L2478
.L2484:
	movl	-260(%rbp), %eax
	jmp	.L2485
.L2497:
	movq	%r11, -560(%rbp)
	movq	-552(%rbp), %rsi
.L2492:
	movq	%r12, %rdi
	leaq	-464(%rbp), %r14
	leaq	-528(%rbp), %r13
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	movq	-560(%rbp), %r11
	jmp	.L2476
.L2480:
	movl	-260(%rbp), %edx
	jmp	.L2481
.L2482:
	movq	%r11, -560(%rbp)
	movq	%r11, %rsi
	jmp	.L2492
.L2495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3633:
	.size	_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringE28UDateTimePatternMatchOptionsR10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringE28UDateTimePatternMatchOptionsR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringER10UErrorCode:
.LFB3632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringE28UDateTimePatternMatchOptionsR10UErrorCode
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2501
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2501:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3632:
	.size	_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SkeletonFieldsC2Ev
	.type	_ZN6icu_6714SkeletonFieldsC2Ev, @function
_ZN6icu_6714SkeletonFieldsC2Ev:
.LFB3730:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE3730:
	.size	_ZN6icu_6714SkeletonFieldsC2Ev, .-_ZN6icu_6714SkeletonFieldsC2Ev
	.globl	_ZN6icu_6714SkeletonFieldsC1Ev
	.set	_ZN6icu_6714SkeletonFieldsC1Ev,_ZN6icu_6714SkeletonFieldsC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SkeletonFields5clearEv
	.type	_ZN6icu_6714SkeletonFields5clearEv, @function
_ZN6icu_6714SkeletonFields5clearEv:
.LFB3732:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE3732:
	.size	_ZN6icu_6714SkeletonFields5clearEv, .-_ZN6icu_6714SkeletonFields5clearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SkeletonFields8copyFromERKS0_
	.type	_ZN6icu_6714SkeletonFields8copyFromERKS0_, @function
_ZN6icu_6714SkeletonFields8copyFromERKS0_:
.LFB3733:
	.cfi_startproc
	endbr64
	movdqu	(%rsi), %xmm0
	movups	%xmm0, (%rdi)
	movdqu	16(%rsi), %xmm1
	movups	%xmm1, 16(%rdi)
	ret
	.cfi_endproc
.LFE3733:
	.size	_ZN6icu_6714SkeletonFields8copyFromERKS0_, .-_ZN6icu_6714SkeletonFields8copyFromERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SkeletonFields10clearFieldEi
	.type	_ZN6icu_6714SkeletonFields10clearFieldEi, @function
_ZN6icu_6714SkeletonFields10clearFieldEi:
.LFB3734:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movb	$0, (%rdi,%rsi)
	movb	$0, 16(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE3734:
	.size	_ZN6icu_6714SkeletonFields10clearFieldEi, .-_ZN6icu_6714SkeletonFields10clearFieldEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SkeletonFields12getFieldCharEi
	.type	_ZNK6icu_6714SkeletonFields12getFieldCharEi, @function
_ZNK6icu_6714SkeletonFields12getFieldCharEi:
.LFB3735:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movsbw	(%rdi,%rsi), %ax
	ret
	.cfi_endproc
.LFE3735:
	.size	_ZNK6icu_6714SkeletonFields12getFieldCharEi, .-_ZNK6icu_6714SkeletonFields12getFieldCharEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SkeletonFields14getFieldLengthEi
	.type	_ZNK6icu_6714SkeletonFields14getFieldLengthEi, @function
_ZNK6icu_6714SkeletonFields14getFieldLengthEi:
.LFB3736:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movsbl	16(%rsi,%rdi), %eax
	ret
	.cfi_endproc
.LFE3736:
	.size	_ZNK6icu_6714SkeletonFields14getFieldLengthEi, .-_ZNK6icu_6714SkeletonFields14getFieldLengthEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SkeletonFields8populateEiRKNS_13UnicodeStringE
	.type	_ZN6icu_6714SkeletonFields8populateEiRKNS_13UnicodeStringE, @function
_ZN6icu_6714SkeletonFields8populateEiRKNS_13UnicodeStringE:
.LFB3737:
	.cfi_startproc
	endbr64
	movzwl	8(%rdx), %ecx
	testw	%cx, %cx
	js	.L2509
	movswl	%cx, %eax
	sarl	$5, %eax
.L2510:
	movl	$-1, %r8d
	testl	%eax, %eax
	je	.L2511
	andl	$2, %ecx
	jne	.L2516
	movq	24(%rdx), %rdx
.L2513:
	movzbl	(%rdx), %r8d
.L2511:
	movslq	%esi, %rsi
	movb	%r8b, (%rdi,%rsi)
	movb	%al, 16(%rdi,%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L2516:
	addq	$10, %rdx
	jmp	.L2513
	.p2align 4,,10
	.p2align 3
.L2509:
	movl	12(%rdx), %eax
	jmp	.L2510
	.cfi_endproc
.LFE3737:
	.size	_ZN6icu_6714SkeletonFields8populateEiRKNS_13UnicodeStringE, .-_ZN6icu_6714SkeletonFields8populateEiRKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SkeletonFields8populateEiDsi
	.type	_ZN6icu_6714SkeletonFields8populateEiDsi, @function
_ZN6icu_6714SkeletonFields8populateEiDsi:
.LFB3738:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movb	%dl, (%rdi,%rsi)
	movb	%cl, 16(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE3738:
	.size	_ZN6icu_6714SkeletonFields8populateEiDsi, .-_ZN6icu_6714SkeletonFields8populateEiDsi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SkeletonFields12isFieldEmptyEi
	.type	_ZNK6icu_6714SkeletonFields12isFieldEmptyEi, @function
_ZNK6icu_6714SkeletonFields12isFieldEmptyEi:
.LFB3739:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	cmpb	$0, 16(%rsi,%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE3739:
	.size	_ZNK6icu_6714SkeletonFields12isFieldEmptyEi, .-_ZNK6icu_6714SkeletonFields12isFieldEmptyEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SkeletonFields8appendToERNS_13UnicodeStringE
	.type	_ZNK6icu_6714SkeletonFields8appendToERNS_13UnicodeStringE, @function
_ZNK6icu_6714SkeletonFields8appendToERNS_13UnicodeStringE:
.LFB3740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-58(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rax
	movq	%rdi, -72(%rbp)
	addq	$16, %rax
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L2522:
	movq	-72(%rbp), %rax
	movsbl	16(%rax), %ebx
	movsbw	(%rax), %r12w
	testl	%ebx, %ebx
	jle	.L2520
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L2521:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	addl	$1, %r14d
	movw	%r12w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	%r14d, %ebx
	jne	.L2521
.L2520:
	addq	$1, -72(%rbp)
	movq	-72(%rbp), %rax
	cmpq	-80(%rbp), %rax
	jne	.L2522
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2527
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2527:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3740:
	.size	_ZNK6icu_6714SkeletonFields8appendToERNS_13UnicodeStringE, .-_ZNK6icu_6714SkeletonFields8appendToERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SkeletonFields13appendFieldToEiRNS_13UnicodeStringE
	.type	_ZNK6icu_6714SkeletonFields13appendFieldToEiRNS_13UnicodeStringE, @function
_ZNK6icu_6714SkeletonFields13appendFieldToEiRNS_13UnicodeStringE:
.LFB3741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movsbl	16(%rdi,%rsi), %r13d
	movsbw	(%rdi,%rsi), %r14w
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r13d, %r13d
	jle	.L2529
	xorl	%ebx, %ebx
	leaq	-58(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L2530:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	addl	$1, %ebx
	movw	%r14w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	%ebx, %r13d
	jne	.L2530
.L2529:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2534
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2534:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3741:
	.size	_ZNK6icu_6714SkeletonFields13appendFieldToEiRNS_13UnicodeStringE, .-_ZNK6icu_6714SkeletonFields13appendFieldToEiRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SkeletonFields12getFirstCharEv
	.type	_ZNK6icu_6714SkeletonFields12getFirstCharEv, @function
_ZNK6icu_6714SkeletonFields12getFirstCharEv:
.LFB3742:
	.cfi_startproc
	endbr64
	cmpb	$0, 16(%rdi)
	jne	.L2538
	cmpb	$0, 17(%rdi)
	jne	.L2539
	cmpb	$0, 18(%rdi)
	jne	.L2540
	cmpb	$0, 19(%rdi)
	jne	.L2541
	cmpb	$0, 20(%rdi)
	jne	.L2542
	cmpb	$0, 21(%rdi)
	jne	.L2543
	cmpb	$0, 22(%rdi)
	jne	.L2544
	cmpb	$0, 23(%rdi)
	jne	.L2545
	cmpb	$0, 24(%rdi)
	jne	.L2546
	cmpb	$0, 25(%rdi)
	jne	.L2547
	cmpb	$0, 26(%rdi)
	jne	.L2548
	cmpb	$0, 27(%rdi)
	jne	.L2549
	cmpb	$0, 28(%rdi)
	jne	.L2550
	cmpb	$0, 29(%rdi)
	jne	.L2551
	cmpb	$0, 30(%rdi)
	jne	.L2552
	xorl	%eax, %eax
	cmpb	$0, 31(%rdi)
	jne	.L2554
	ret
.L2538:
	xorl	%eax, %eax
.L2536:
	movsbw	(%rdi,%rax), %ax
	ret
.L2539:
	movl	$1, %eax
	jmp	.L2536
.L2550:
	movl	$12, %eax
	jmp	.L2536
.L2554:
	movl	$15, %eax
	jmp	.L2536
.L2540:
	movl	$2, %eax
	jmp	.L2536
.L2541:
	movl	$3, %eax
	jmp	.L2536
.L2542:
	movl	$4, %eax
	jmp	.L2536
.L2543:
	movl	$5, %eax
	jmp	.L2536
.L2544:
	movl	$6, %eax
	jmp	.L2536
.L2545:
	movl	$7, %eax
	jmp	.L2536
.L2546:
	movl	$8, %eax
	jmp	.L2536
.L2547:
	movl	$9, %eax
	jmp	.L2536
.L2548:
	movl	$10, %eax
	jmp	.L2536
.L2549:
	movl	$11, %eax
	jmp	.L2536
.L2551:
	movl	$13, %eax
	jmp	.L2536
.L2552:
	movl	$14, %eax
	jmp	.L2536
	.cfi_endproc
.LFE3742:
	.size	_ZNK6icu_6714SkeletonFields12getFirstCharEv, .-_ZNK6icu_6714SkeletonFields12getFirstCharEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PtnSkeletonC2Ev
	.type	_ZN6icu_6711PtnSkeletonC2Ev, @function
_ZN6icu_6711PtnSkeletonC2Ev:
.LFB3744:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	movb	$0, 136(%rdi)
	movq	%rax, (%rdi)
	movups	%xmm0, 72(%rdi)
	movups	%xmm0, 88(%rdi)
	movups	%xmm0, 104(%rdi)
	movups	%xmm0, 120(%rdi)
	ret
	.cfi_endproc
.LFE3744:
	.size	_ZN6icu_6711PtnSkeletonC2Ev, .-_ZN6icu_6711PtnSkeletonC2Ev
	.globl	_ZN6icu_6711PtnSkeletonC1Ev
	.set	_ZN6icu_6711PtnSkeletonC1Ev,_ZN6icu_6711PtnSkeletonC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PtnSkeletonC2ERKS0_
	.type	_ZN6icu_6711PtnSkeletonC2ERKS0_, @function
_ZN6icu_6711PtnSkeletonC2ERKS0_:
.LFB3747:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	movups	%xmm0, 72(%rdi)
	movups	%xmm0, 88(%rdi)
	movups	%xmm0, 104(%rdi)
	movups	%xmm0, 120(%rdi)
	movq	%rax, (%rdi)
	movdqu	8(%rsi), %xmm1
	movups	%xmm1, 8(%rdi)
	movdqu	24(%rsi), %xmm2
	movups	%xmm2, 24(%rdi)
	movdqu	40(%rsi), %xmm3
	movups	%xmm3, 40(%rdi)
	movdqu	56(%rsi), %xmm4
	movups	%xmm4, 56(%rdi)
	movdqu	72(%rsi), %xmm5
	movdqu	88(%rsi), %xmm6
	movdqu	104(%rsi), %xmm7
	movdqu	120(%rsi), %xmm1
	movzbl	136(%rsi), %eax
	movups	%xmm5, 72(%rdi)
	movups	%xmm6, 88(%rdi)
	movb	%al, 136(%rdi)
	movups	%xmm7, 104(%rdi)
	movups	%xmm1, 120(%rdi)
	ret
	.cfi_endproc
.LFE3747:
	.size	_ZN6icu_6711PtnSkeletonC2ERKS0_, .-_ZN6icu_6711PtnSkeletonC2ERKS0_
	.globl	_ZN6icu_6711PtnSkeletonC1ERKS0_
	.set	_ZN6icu_6711PtnSkeletonC1ERKS0_,_ZN6icu_6711PtnSkeletonC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710PatternMap3addERKNS_13UnicodeStringERKNS_11PtnSkeletonES3_aR10UErrorCode
	.type	_ZN6icu_6710PatternMap3addERKNS_13UnicodeStringERKNS_11PtnSkeletonES3_aR10UErrorCode, @function
_ZN6icu_6710PatternMap3addERKNS_13UnicodeStringERKNS_11PtnSkeletonES3_aR10UErrorCode:
.LFB3670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movzwl	8(%rsi), %eax
	movq	%rcx, -56(%rbp)
	movl	%r8d, -60(%rbp)
	testw	%ax, %ax
	js	.L2558
	movswl	%ax, %edx
	sarl	$5, %edx
	testl	%edx, %edx
	je	.L2560
.L2622:
	testb	$2, %al
	jne	.L2618
	movq	24(%r12), %rax
.L2562:
	movzwl	(%rax), %edx
	movl	$0, 0(%r13)
	leal	-65(%rdx), %eax
	cmpw	$25, %ax
	ja	.L2563
	movzwl	%dx, %ecx
	leal	-65(%rcx), %eax
	cltq
	movq	8(%r15,%rax,8), %rbx
	testq	%rbx, %rbx
	je	.L2619
.L2566:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710PatternMap16getDuplicateElemERKNS_13UnicodeStringERKNS_11PtnSkeletonEPNS_7PtnElemE.part.0
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2579
	cmpb	$0, 424(%r15)
	jne	.L2620
.L2557:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2579:
	.cfi_restore_state
	movq	%rbx, %rdx
	movq	152(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2579
	movl	$160, %edi
	movq	%rdx, -72(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2567
	leaq	16+_ZTVN6icu_677PtnElemE(%rip), %rax
	leaq	8(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, (%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-56(%rbp), %rsi
	leaq	80(%rbx), %rdi
	movq	$0, 72(%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	0(%r13), %edi
	movq	-72(%rbp), %rdx
	movq	$0, 152(%rbx)
	testl	%edi, %edi
	jle	.L2621
.L2582:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2618:
	.cfi_restore_state
	leaq	10(%r12), %rax
	jmp	.L2562
	.p2align 4,,10
	.p2align 3
.L2558:
	movl	12(%rsi), %edx
	testl	%edx, %edx
	jne	.L2622
.L2560:
	movl	$65567, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2563:
	.cfi_restore_state
	leal	-97(%rdx), %eax
	cmpw	$25, %ax
	ja	.L2560
	movzwl	%dx, %ecx
	leal	-71(%rcx), %eax
	cltq
	movq	8(%r15,%rax,8), %rbx
	testq	%rbx, %rbx
	jne	.L2566
.L2619:
	movl	$160, %edi
	movl	%edx, -64(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2567
	leaq	16+_ZTVN6icu_677PtnElemE(%rip), %rax
	leaq	8(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, (%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-56(%rbp), %rsi
	leaq	80(%rbx), %rdi
	movq	$0, 72(%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	0(%r13), %r10d
	movl	-72(%rbp), %ecx
	movq	$0, 152(%rbx)
	movl	-64(%rbp), %edx
	testl	%r10d, %r10d
	jg	.L2582
	movl	$144, %edi
	movl	%edx, -72(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movl	-56(%rbp), %ecx
	movl	-72(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L2571
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	movups	%xmm0, 72(%r12)
	movups	%xmm0, 88(%r12)
	movups	%xmm0, 104(%r12)
	movups	%xmm0, 120(%r12)
	movq	%rax, (%r12)
	movdqu	8(%r14), %xmm1
	movups	%xmm1, 8(%r12)
	movdqu	24(%r14), %xmm2
	movups	%xmm2, 24(%r12)
	movdqu	40(%r14), %xmm3
	movups	%xmm3, 40(%r12)
	movdqu	56(%r14), %xmm4
	movups	%xmm4, 56(%r12)
	movdqu	72(%r14), %xmm5
	movups	%xmm5, 72(%r12)
	movdqu	88(%r14), %xmm6
	movups	%xmm6, 88(%r12)
	movdqu	104(%r14), %xmm7
	movups	%xmm7, 104(%r12)
	movdqu	120(%r14), %xmm1
	movups	%xmm1, 120(%r12)
	movzbl	136(%r14), %eax
	movb	%al, 136(%r12)
	movl	0(%r13), %r9d
	testl	%r9d, %r9d
	jg	.L2572
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2573
.L2600:
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rsi
	movl	%edx, -72(%rbp)
	movl	%ecx, -56(%rbp)
	movq	8(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2574
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-56(%rbp), %ecx
	movl	-72(%rbp), %edx
.L2575:
	movq	%r12, 72(%rbx)
	testq	%r12, %r12
	je	.L2605
.L2576:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L2582
.L2599:
	movzbl	-60(%rbp), %eax
	movb	%al, 144(%rbx)
	cmpw	$96, %dx
	jbe	.L2578
	leal	-71(%rcx), %eax
	cltq
	movq	%rbx, 8(%r15,%rax,8)
	jmp	.L2557
	.p2align 4,,10
	.p2align 3
.L2620:
	movq	-56(%rbp), %rsi
	leaq	80(%rdx), %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzbl	-60(%rbp), %eax
	movq	-72(%rbp), %rdx
	movb	%al, 144(%rdx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2567:
	.cfi_restore_state
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L2557
	movl	$7, 0(%r13)
	jmp	.L2557
	.p2align 4,,10
	.p2align 3
.L2621:
	movl	$144, %edi
	movq	%rdx, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L2583
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6711PtnSkeletonC1ERKS0_
	movl	0(%r13), %esi
	movq	-56(%rbp), %rdx
	testl	%esi, %esi
	jg	.L2584
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2585
.L2604:
	movq	(%rdi), %rax
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rcx
	movq	%rdx, -56(%rbp)
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2586
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %rdx
.L2587:
	movq	%r12, 72(%rbx)
	testq	%r12, %r12
	je	.L2605
.L2590:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L2582
	movzbl	-60(%rbp), %eax
	movb	%al, 144(%rbx)
	movq	152(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2592
	movq	(%rdi), %rax
	movq	%rdx, -56(%rbp)
	call	*8(%rax)
	movq	-56(%rbp), %rdx
.L2592:
	movq	%rbx, 152(%rdx)
	jmp	.L2557
	.p2align 4,,10
	.p2align 3
.L2572:
	movq	%r12, %rdi
	movl	%edx, -72(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	0(%r13), %r8d
	movl	-56(%rbp), %ecx
	movl	-72(%rbp), %edx
	testl	%r8d, %r8d
	jle	.L2599
	jmp	.L2582
	.p2align 4,,10
	.p2align 3
.L2584:
	movq	(%r12), %rcx
	leaq	_ZN6icu_6711PtnSkeletonD0Ev(%rip), %rax
	movq	%rdx, -56(%rbp)
	cmpq	%rax, 8(%rcx)
	jne	.L2589
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L2590
	.p2align 4,,10
	.p2align 3
.L2578:
	leal	-65(%rcx), %eax
	cltq
	movq	%rbx, 8(%r15,%rax,8)
	jmp	.L2557
.L2583:
	cmpl	$0, 0(%r13)
	jg	.L2582
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L2604
	.p2align 4,,10
	.p2align 3
.L2605:
	movl	$7, 0(%r13)
	jmp	.L2582
.L2573:
	movq	%r12, 72(%rbx)
	jmp	.L2576
.L2585:
	movq	%r12, 72(%rbx)
	jmp	.L2590
.L2589:
	call	_ZN6icu_6711PtnSkeletonD0Ev
	movq	-56(%rbp), %rdx
	jmp	.L2590
.L2574:
	call	*%rax
	movl	-72(%rbp), %edx
	movl	-56(%rbp), %ecx
	jmp	.L2575
.L2586:
	call	*%rax
	movq	-56(%rbp), %rdx
	jmp	.L2587
.L2571:
	cmpl	$0, 0(%r13)
	jg	.L2582
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L2600
	jmp	.L2605
	.cfi_endproc
.LFE3670:
	.size	_ZN6icu_6710PatternMap3addERKNS_13UnicodeStringERKNS_11PtnSkeletonES3_aR10UErrorCode, .-_ZN6icu_6710PatternMap3addERKNS_13UnicodeStringERKNS_11PtnSkeletonES3_aR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode.part.0, @function
_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode.part.0:
.LFB5270:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-352(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$2, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-208(%rbp), %rdi
	subq	$472, %rsp
	movl	%ecx, -444(%rbp)
	leaq	16+_ZTVN6icu_6715DateTimeMatcherE(%rip), %rcx
	movq	%rsi, -464(%rbp)
	movq	%rcx, %xmm1
	movq	%r14, %rcx
	movq	%r8, -440(%rbp)
	movq	%r9, -480(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -408(%rbp)
	movq	232(%rbx), %rdx
	movq	%rax, -416(%rbp)
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	movq	%rax, %xmm2
	movq	%rax, -352(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movb	$0, -216(%rbp)
	movb	$0, -64(%rbp)
	movups	%xmm0, -280(%rbp)
	movups	%xmm0, -264(%rbp)
	movups	%xmm0, -248(%rbp)
	movups	%xmm0, -232(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%r12, %r12
	je	.L2664
	movq	%r12, %rsi
	leaq	-424(%rbp), %r15
	leaq	-416(%rbp), %r13
	call	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE
	movzwl	-408(%rbp), %edx
	leaq	-96(%rbp), %r8
	movq	%r14, -504(%rbp)
	movq	%rbx, -488(%rbp)
	movq	%r13, %rbx
	movl	%edx, %eax
	movq	%r12, -496(%rbp)
	movq	%r15, %r12
	movq	%r8, %r15
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, -408(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -472(%rbp)
	.p2align 4,,10
	.p2align 3
.L2634:
	movsbl	16(%r15), %r14d
	movsbw	(%r15), %r9w
	xorl	%r13d, %r13d
	testl	%r14d, %r14d
	jle	.L2637
	movq	%r12, %rax
	movl	%r14d, -456(%rbp)
	movq	%rbx, %r12
	movl	%r9d, %r14d
	movq	%r15, %rbx
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L2636:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movw	%r14w, -424(%rbp)
	addl	$1, %r13d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	%r13d, -456(%rbp)
	jne	.L2636
	movq	%r15, %rax
	movq	%rbx, %r15
	movq	%r12, %rbx
	movq	%rax, %r12
.L2637:
	addq	$1, %r15
	cmpq	-472(%rbp), %r15
	jne	.L2634
	movq	%rbx, %r13
	movq	-496(%rbp), %r12
	movq	-488(%rbp), %rbx
	movq	-504(%rbp), %r14
.L2629:
	movq	256(%rbx), %rdi
	movq	%r13, %rsi
	leaq	-425(%rbp), %rdx
	call	_ZNK6icu_6710PatternMap25getPatternFromBasePatternERKNS_13UnicodeStringERa
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2632
	cmpb	$0, -425(%rbp)
	je	.L2638
	testq	%r12, %r12
	je	.L2632
	cmpb	$0, -444(%rbp)
	jne	.L2632
	movq	-440(%rbp), %rdi
	movl	$1, %r15d
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L2641:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2665
	addq	$472, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2632:
	.cfi_restore_state
	movq	256(%rbx), %rdi
	movq	%r14, %rsi
	leaq	-424(%rbp), %rdx
	movq	$0, -424(%rbp)
	call	_ZNK6icu_6710PatternMap22getPatternFromSkeletonERKNS_11PtnSkeletonEPPS2_
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2651
	movq	-440(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	cmpb	$0, -444(%rbp)
	jne	.L2644
.L2645:
	movl	$2, %r15d
	jmp	.L2641
	.p2align 4,,10
	.p2align 3
.L2638:
	movq	-440(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	cmpb	$0, -444(%rbp)
	je	.L2666
	movq	256(%rbx), %rdi
	movq	%r14, %rsi
	leaq	-424(%rbp), %rdx
	movq	$0, -424(%rbp)
	movl	$1, %r15d
	call	_ZNK6icu_6710PatternMap22getPatternFromSkeletonERKNS_11PtnSkeletonEPPS2_
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2643
	movq	-440(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L2644:
	testq	%r12, %r12
	je	.L2652
	cmpq	$0, -424(%rbp)
	jne	.L2645
.L2652:
	movl	$2, %r15d
.L2643:
	testq	%r12, %r12
	movq	256(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	-480(%rbp), %rbx
	setne	%r8b
	movq	-464(%rbp), %rcx
	movzbl	%r8b, %r8d
	movq	%rbx, %r9
	call	_ZN6icu_6710PatternMap3addERKNS_13UnicodeStringERKNS_11PtnSkeletonES3_aR10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovle	%eax, %r15d
	jmp	.L2641
	.p2align 4,,10
	.p2align 3
.L2664:
	call	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE
	movzwl	-408(%rbp), %edx
	leaq	-96(%rbp), %r15
	movq	%rbx, -488(%rbp)
	movq	%r14, -504(%rbp)
	leaq	-416(%rbp), %r13
	movl	%edx, %eax
	movq	%r12, -496(%rbp)
	movq	%r13, %r12
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, -408(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -456(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, -472(%rbp)
	.p2align 4,,10
	.p2align 3
.L2626:
	movsbl	16(%r15), %r13d
	movsbw	(%r15), %bx
	xorl	%r14d, %r14d
	testl	%r13d, %r13d
	jle	.L2630
	.p2align 4,,10
	.p2align 3
.L2628:
	movq	-472(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	addl	$1, %r14d
	movl	$1, %ecx
	movw	%bx, -424(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	%r14d, %r13d
	jne	.L2628
.L2630:
	addq	$1, %r15
	cmpq	%r15, -456(%rbp)
	jne	.L2626
	movq	%r12, %r13
	movq	-488(%rbp), %rbx
	movq	-496(%rbp), %r12
	movq	-504(%rbp), %r14
	jmp	.L2629
	.p2align 4,,10
	.p2align 3
.L2651:
	xorl	%r15d, %r15d
	jmp	.L2643
.L2665:
	call	__stack_chk_fail@PLT
.L2666:
	movl	$1, %r15d
	jmp	.L2641
	.cfi_endproc
.LFE5270:
	.size	_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode.part.0, .-_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode:
.LFB3645:
	.cfi_startproc
	endbr64
	movl	4600(%rdi), %eax
	testl	%eax, %eax
	jle	.L2672
	movl	%eax, (%r9)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2672:
	movsbl	%cl, %ecx
	jmp	_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode.part.0
	.cfi_endproc
.LFE3645:
	.size	_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator9hackTimesERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator9hackTimesERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator9hackTimesERKNS_13UnicodeStringER10UErrorCode:
.LFB3593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$2, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-128(%rbp), %rbx
	subq	$328, %rsp
	movq	%rdx, -360(%rbp)
	movq	232(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%si, -312(%rbp)
	movq	%rax, -320(%rbp)
	leaq	-324(%rbp), %rax
	movl	$0, -324(%rbp)
	movq	%rax, -344(%rbp)
	movl	$0, 3208(%r15)
	jmp	.L2678
	.p2align 4,,10
	.p2align 3
.L2674:
	movl	-324(%rbp), %ecx
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	movslq	3208(%r15), %rax
	movq	%rbx, %rsi
	leal	1(%rax), %ecx
	salq	$6, %rax
	movl	%ecx, 3208(%r15)
	leaq	8(%r15,%rax), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addl	-324(%rbp), %r14d
	cmpl	$49, 3208(%r15)
	jg	.L2677
.L2678:
	movq	(%r15), %rax
	movq	-344(%rbp), %rcx
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*16(%rax)
	cmpl	$1, %eax
	je	.L2674
.L2677:
	movl	$2, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$0, -324(%rbp)
	movw	%dx, -248(%rbp)
	movq	232(%r13), %rdx
	movq	%rax, -256(%rbp)
	movl	3208(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L2721
	leaq	-256(%rbp), %rcx
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	%rcx, -352(%rbp)
	leaq	-192(%rbp), %r12
	jmp	.L2676
	.p2align 4,,10
	.p2align 3
.L2725:
	movswl	%ax, %ecx
	sarl	$5, %ecx
	movl	%ecx, %r15d
	testl	%r15d, %r15d
	je	.L2682
.L2726:
	andw	$2, %ax
	leaq	-182(%rbp), %rsi
	movl	%eax, %r14d
	movq	%rsi, %rax
	cmove	-168(%rbp), %rax
	cmpw	$39, (%rax)
	je	.L2722
	movq	232(%r13), %rdi
	movq	%r12, %rsi
	call	_ZNK6icu_6712FormatParser18isPatternSeparatorERKNS_13UnicodeStringE
	testb	%al, %al
	je	.L2696
	testb	%bl, %bl
	jne	.L2719
.L2696:
	leaq	-182(%rbp), %rax
	testw	%r14w, %r14w
	cmove	-168(%rbp), %rax
	movzwl	(%rax), %eax
	cmpw	$109, %ax
	je	.L2719
	cmpw	$115, %ax
	je	.L2723
	testb	%bl, %bl
	jne	.L2720
	andl	$-33, %eax
	subl	$86, %eax
	testw	$-5, %ax
	je	.L2720
.L2685:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-324(%rbp), %eax
	movq	232(%r13), %rdx
	addl	$1, %eax
	movl	%eax, -324(%rbp)
	cmpl	%eax, 3208(%rdx)
	jle	.L2724
.L2676:
	cltq
	movq	%r12, %rdi
	salq	$6, %rax
	leaq	8(%rdx,%rax), %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	jns	.L2725
	movl	-180(%rbp), %r15d
	testl	%r15d, %r15d
	jne	.L2726
.L2682:
	movq	232(%r13), %rdi
	movq	%r12, %rsi
	call	_ZNK6icu_6712FormatParser18isPatternSeparatorERKNS_13UnicodeStringE
	testb	%al, %al
	je	.L2715
	testb	%bl, %bl
	je	.L2685
.L2719:
	movq	-352(%rbp), %rdi
	movl	%r15d, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %ebx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L2685
	.p2align 4,,10
	.p2align 3
.L2722:
	testb	%bl, %bl
	je	.L2685
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	232(%r13), %rdi
	movq	-344(%rbp), %rdx
	leaq	-128(%rbp), %r14
	movq	%rax, -128(%rbp)
	movq	%r14, %rsi
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	call	_ZN6icu_6712FormatParser15getQuoteLiteralERNS_13UnicodeStringEPi
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L2686
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L2687:
	movq	-352(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L2685
	.p2align 4,,10
	.p2align 3
.L2715:
	testb	%bl, %bl
	je	.L2685
	.p2align 4,,10
	.p2align 3
.L2720:
	leaq	-320(%rbp), %r14
.L2693:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L2679:
	movq	-352(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2727
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2686:
	.cfi_restore_state
	movl	-116(%rbp), %ecx
	jmp	.L2687
	.p2align 4,,10
	.p2align 3
.L2724:
	leaq	-320(%rbp), %r14
	jmp	.L2679
	.p2align 4,,10
	.p2align 3
.L2723:
	leaq	-320(%rbp), %r14
	testb	%bl, %bl
	je	.L2693
	movq	-352(%rbp), %rdi
	movl	%r15d, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	4600(%r13), %eax
	testl	%eax, %eax
	jle	.L2694
	movq	-360(%rbp), %rbx
	movl	%eax, (%rbx)
	jmp	.L2693
.L2694:
	movq	-360(%rbp), %r9
	movq	%r14, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode.part.0
	jmp	.L2693
.L2721:
	leaq	-256(%rbp), %rax
	leaq	-320(%rbp), %r14
	movq	%rax, -352(%rbp)
	jmp	.L2679
.L2727:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3593:
	.size	_ZN6icu_6724DateTimePatternGenerator9hackTimesERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator9hackTimesERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator23consumeShortTimePatternERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator23consumeShortTimePatternERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator23consumeShortTimePatternERKNS_13UnicodeStringER10UErrorCode:
.LFB3595:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L2730
	ret
	.p2align 4,,10
	.p2align 3
.L2730:
	jmp	_ZN6icu_6724DateTimePatternGenerator9hackTimesERKNS_13UnicodeStringER10UErrorCode
	.cfi_endproc
.LFE3595:
	.size	_ZN6icu_6724DateTimePatternGenerator23consumeShortTimePatternERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator23consumeShortTimePatternERKNS_13UnicodeStringER10UErrorCode
	.section	.text._ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode,"axG",@progbits,_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode:
.LFB3609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-240(%rbp), %rdi
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -264(%rbp)
	movq	%r14, %rsi
	movq	%r8, -272(%rbp)
	movb	%cl, -297(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rdi, -288(%rbp)
	movq	%r8, %rdx
	call	*88(%rax)
	leaq	-264(%rbp), %rax
	movq	%rax, -280(%rbp)
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L2731
	xorl	%r13d, %r13d
	leaq	-192(%rbp), %rbx
	jmp	.L2732
	.p2align 4,,10
	.p2align 3
.L2753:
	movq	-272(%rbp), %rcx
	movl	%eax, (%rcx)
.L2738:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L2734:
	movq	%rbx, %rdi
	addl	$1, %r13d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L2732:
	movq	-280(%rbp), %rdx
	movq	-288(%rbp), %rdi
	movq	%r14, %rcx
	movl	%r13d, %esi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L2731
	movq	-264(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	$-1, %edx
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movq	8(%r12), %rax
	movq	%rbx, %rsi
	movq	4496(%rax), %rax
	movq	(%rax), %rdi
	call	uhash_geti_67@PLT
	cmpl	$1, %eax
	je	.L2734
	movq	8(%r12), %rax
	movl	$64, %edi
	movq	4496(%rax), %r15
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L2735
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-296(%rbp), %r8
.L2735:
	movq	-272(%rbp), %rcx
	movq	(%r15), %rdi
	movq	%r8, %rsi
	movl	$1, %edx
	leaq	-128(%rbp), %r15
	call	uhash_puti_67@PLT
	movq	(%r14), %rax
	movq	-272(%rbp), %rdx
	movq	%r14, %rdi
	leaq	-252(%rbp), %rsi
	movl	$0, -252(%rbp)
	call	*32(%rax)
	movl	-252(%rbp), %ecx
	leaq	-248(%rbp), %rdx
	movq	%r15, %rdi
	movl	$1, %esi
	movq	%rax, -248(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	24(%r12), %edx
	movq	8(%r12), %rdi
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 24(%r12)
	movl	4600(%rdi), %eax
	testl	%eax, %eax
	jg	.L2753
	xorl	%ecx, %ecx
	movq	-272(%rbp), %r9
	movq	%rbx, %rdx
	movq	%r15, %rsi
	cmpb	$0, -297(%rbp)
	leaq	16(%r12), %r8
	sete	%cl
	call	_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode.part.0
	jmp	.L2738
	.p2align 4,,10
	.p2align 3
.L2731:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2754
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2754:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3609:
	.size	_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator10addPatternERKNS_13UnicodeStringEaRS1_R10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator10addPatternERKNS_13UnicodeStringEaRS1_R10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator10addPatternERKNS_13UnicodeStringEaRS1_R10UErrorCode:
.LFB3644:
	.cfi_startproc
	endbr64
	movq	%rcx, %rax
	movl	4600(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L2760
	movl	%ecx, (%r8)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2760:
	movsbl	%dl, %ecx
	movq	%r8, %r9
	xorl	%edx, %edx
	movq	%rax, %r8
	jmp	_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode.part.0
	.cfi_endproc
.LFE3644:
	.size	_ZN6icu_6724DateTimePatternGenerator10addPatternERKNS_13UnicodeStringEaRS1_R10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator10addPatternERKNS_13UnicodeStringEaRS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator17addCanonicalItemsER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator17addCanonicalItemsER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator17addCanonicalItemsER10UErrorCode:
.LFB3639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L2761
	movl	$2, %edx
	movq	%rdi, %r12
	leaq	-128(%rbp), %r14
	movq	%rsi, %rbx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	_ZN6icu_67L15Canonical_ItemsE(%rip), %r15
	movw	%dx, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-192(%rbp), %rax
	leaq	32(%r15), %r13
	movq	%rax, -200(%rbp)
	jmp	.L2766
	.p2align 4,,10
	.p2align 3
.L2763:
	addq	$2, %r15
	cmpq	%r13, %r15
	je	.L2774
.L2766:
	movzwl	(%r15), %esi
	testw	%si, %si
	je	.L2763
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movl	4600(%r12), %eax
	testl	%eax, %eax
	jle	.L2764
	movl	%eax, (%rbx)
.L2765:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L2763
.L2774:
	movq	-200(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L2761:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2775
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2764:
	.cfi_restore_state
	movq	-200(%rbp), %r8
	movq	%rbx, %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode.part.0
	jmp	.L2765
.L2775:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3639:
	.size	_ZN6icu_6724DateTimePatternGenerator17addCanonicalItemsER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator17addCanonicalItemsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator14addICUPatternsERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator14addICUPatternsERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator14addICUPatternsERKNS_6LocaleER10UErrorCode:
.LFB3592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -200(%rbp)
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L2776
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsi, %r13
	movq	%rdx, %r12
	xorl	%ebx, %ebx
	movq	%rax, -128(%rbp)
	movl	$2, %esi
	movl	$2, %edi
	leaq	-192(%rbp), %r14
	movq	%rax, -192(%rbp)
	leaq	-128(%rbp), %rax
	movw	%si, -184(%rbp)
	movw	%di, -120(%rbp)
	movq	%rax, -208(%rbp)
.L2787:
	movq	%r13, %rsi
	movl	%ebx, %edi
	call	_ZN6icu_6710DateFormat18createDateInstanceENS0_6EStyleERKNS_6LocaleE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2778
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2779
	movq	(%rax), %rax
	movq	%r14, %rsi
	call	*240(%rax)
	movq	-200(%rbp), %rax
	movl	4600(%rax), %eax
	testl	%eax, %eax
	jle	.L2780
	movl	%eax, (%r12)
.L2779:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
.L2778:
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L2781
	movq	%r13, %rsi
	movl	%ebx, %edi
	call	_ZN6icu_6710DateFormat18createTimeInstanceENS0_6EStyleERKNS_6LocaleE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2782
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2786
	movq	(%rax), %rax
	movq	%r14, %rsi
	call	*240(%rax)
	movq	-200(%rbp), %rax
	movl	4600(%rax), %eax
	testl	%eax, %eax
	jle	.L2784
	movl	%eax, (%r12)
	cmpl	$3, %ebx
	je	.L2807
.L2786:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
.L2782:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L2781
	addl	$1, %ebx
	cmpl	$4, %ebx
	jne	.L2787
.L2781:
	movq	-208(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L2776:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2808
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2780:
	.cfi_restore_state
	movq	-208(%rbp), %r8
	movq	%r12, %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-200(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode.part.0
	jmp	.L2779
	.p2align 4,,10
	.p2align 3
.L2784:
	movq	-208(%rbp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %r9
	movq	-200(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6724DateTimePatternGenerator22addPatternWithSkeletonERKNS_13UnicodeStringEPS2_aRS1_R10UErrorCode.part.0
	cmpl	$3, %ebx
	jne	.L2786
.L2807:
	movswl	-184(%rbp), %eax
	shrl	$5, %eax
	je	.L2786
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L2786
	movq	-200(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_6724DateTimePatternGenerator9hackTimesERKNS_13UnicodeStringER10UErrorCode
	jmp	.L2786
.L2808:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3592:
	.size	_ZN6icu_6724DateTimePatternGenerator14addICUPatternsERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator14addICUPatternsERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PtnSkeleton8copyFromERKS0_
	.type	_ZN6icu_6711PtnSkeleton8copyFromERKS0_, @function
_ZN6icu_6711PtnSkeleton8copyFromERKS0_:
.LFB3749:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movdqu	24(%rsi), %xmm1
	movups	%xmm1, 24(%rdi)
	movdqu	40(%rsi), %xmm2
	movups	%xmm2, 40(%rdi)
	movdqu	56(%rsi), %xmm3
	movups	%xmm3, 56(%rdi)
	movdqu	72(%rsi), %xmm4
	movups	%xmm4, 72(%rdi)
	movdqu	88(%rsi), %xmm5
	movups	%xmm5, 88(%rdi)
	movdqu	104(%rsi), %xmm6
	movups	%xmm6, 104(%rdi)
	movdqu	120(%rsi), %xmm7
	movups	%xmm7, 120(%rdi)
	movzbl	136(%rsi), %eax
	movb	%al, 136(%rdi)
	ret
	.cfi_endproc
.LFE3749:
	.size	_ZN6icu_6711PtnSkeleton8copyFromERKS0_, .-_ZN6icu_6711PtnSkeleton8copyFromERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PtnSkeleton5clearEv
	.type	_ZN6icu_6711PtnSkeleton5clearEv, @function
_ZN6icu_6711PtnSkeleton5clearEv:
.LFB3750:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rdi)
	movups	%xmm0, 24(%rdi)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 56(%rdi)
	movups	%xmm0, 72(%rdi)
	movups	%xmm0, 88(%rdi)
	movups	%xmm0, 104(%rdi)
	movups	%xmm0, 120(%rdi)
	ret
	.cfi_endproc
.LFE3750:
	.size	_ZN6icu_6711PtnSkeleton5clearEv, .-_ZN6icu_6711PtnSkeleton5clearEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PtnSkeleton6equalsERKS0_
	.type	_ZNK6icu_6711PtnSkeleton6equalsERKS0_, @function
_ZNK6icu_6711PtnSkeleton6equalsERKS0_:
.LFB3751:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	movq	80(%rdi), %rdx
	xorl	%r8d, %r8d
	xorq	80(%rsi), %rdx
	xorq	72(%rsi), %rax
	orq	%rax, %rdx
	jne	.L2811
	movq	88(%rdi), %rax
	movq	96(%rdi), %rdx
	xorq	88(%rsi), %rax
	xorq	96(%rsi), %rdx
	orq	%rax, %rdx
	je	.L2827
.L2811:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2827:
	movq	104(%rdi), %rax
	movq	112(%rdi), %rdx
	xorq	104(%rsi), %rax
	xorq	112(%rsi), %rdx
	orq	%rax, %rdx
	jne	.L2811
	movq	120(%rdi), %rax
	movq	128(%rdi), %rdx
	xorq	120(%rsi), %rax
	xorq	128(%rsi), %rdx
	orq	%rax, %rdx
	jne	.L2811
	movq	8(%rdi), %rcx
	leaq	8(%rdi), %rax
	xorq	8(%rsi), %rcx
	leaq	8(%rsi), %rdx
	movq	16(%rdi), %rdi
	xorq	16(%rsi), %rdi
	orq	%rcx, %rdi
	jne	.L2821
	movq	16(%rax), %rcx
	movq	24(%rax), %rsi
	xorq	16(%rdx), %rcx
	xorq	24(%rdx), %rsi
	orq	%rcx, %rsi
	jne	.L2821
	movq	32(%rax), %rcx
	movq	40(%rax), %rsi
	xorq	32(%rdx), %rcx
	xorq	40(%rdx), %rsi
	orq	%rcx, %rsi
	je	.L2828
	.p2align 4,,10
	.p2align 3
.L2821:
	movl	$1, %eax
.L2822:
	testl	%eax, %eax
	sete	%r8b
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2828:
	movq	48(%rax), %rcx
	movq	56(%rax), %rsi
	movq	48(%rdx), %rax
	xorq	56(%rdx), %rsi
	xorq	%rcx, %rax
	orq	%rax, %rsi
	jne	.L2821
	xorl	%eax, %eax
	jmp	.L2822
	.cfi_endproc
.LFE3751:
	.size	_ZNK6icu_6711PtnSkeleton6equalsERKS0_, .-_ZNK6icu_6711PtnSkeleton6equalsERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PtnSkeleton11getSkeletonEv
	.type	_ZNK6icu_6711PtnSkeleton11getSkeletonEv, @function
_ZNK6icu_6711PtnSkeleton11getSkeletonEv:
.LFB3752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-58(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	72(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	leaq	88(%rsi), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L2832:
	movsbl	16(%r13), %r8d
	movsbw	0(%r13), %r9w
	testl	%r8d, %r8d
	jle	.L2830
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L2831:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%r8d, -80(%rbp)
	addl	$1, %r14d
	movw	%r9w, -58(%rbp)
	movl	%r9d, -76(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-80(%rbp), %r8d
	movl	-76(%rbp), %r9d
	cmpl	%r14d, %r8d
	jne	.L2831
.L2830:
	addq	$1, %r13
	cmpq	%r13, -72(%rbp)
	jne	.L2832
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	cmpb	$0, 136(%rbx)
	je	.L2829
	movswl	8(%r12), %ecx
	testw	%cx, %cx
	js	.L2835
	sarl	$5, %ecx
.L2836:
	movl	$97, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L2829
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
.L2829:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2842
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2835:
	.cfi_restore_state
	movl	12(%r12), %ecx
	jmp	.L2836
.L2842:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3752:
	.size	_ZNK6icu_6711PtnSkeleton11getSkeletonEv, .-_ZNK6icu_6711PtnSkeleton11getSkeletonEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator11getSkeletonERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator11getSkeletonERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator11getSkeletonERKNS_13UnicodeStringER10UErrorCode:
.LFB3588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-56(%rbp), %rcx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-3264(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVN6icu_6712FormatParserE(%rip), %rbx
	subq	$3536, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -3264(%rbp)
	leaq	-3256(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L2844:
	movl	$2, %edi
	movq	%rdx, (%rax)
	addq	$64, %rax
	movw	%di, -56(%rax)
	cmpq	%rax, %rcx
	jne	.L2844
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	leaq	16+_ZTVN6icu_6715DateTimeMatcherE(%rip), %rcx
	movq	%r12, %rdx
	movq	$0, -56(%rbp)
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	leaq	-3568(%rbp), %r14
	movq	%rax, -3568(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%r14, %rcx
	leaq	-3424(%rbp), %rdi
	movb	$0, -3280(%rbp)
	movaps	%xmm0, -3424(%rbp)
	pxor	%xmm0, %xmm0
	subq	$56, %r12
	movaps	%xmm0, -3344(%rbp)
	movaps	%xmm0, -3328(%rbp)
	movaps	%xmm0, -3312(%rbp)
	movaps	%xmm0, -3296(%rbp)
	movups	%xmm0, -3496(%rbp)
	movups	%xmm0, -3480(%rbp)
	movups	%xmm0, -3464(%rbp)
	movups	%xmm0, -3448(%rbp)
	movb	$0, -3432(%rbp)
	call	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6711PtnSkeleton11getSkeletonEv
	movq	%rbx, -3264(%rbp)
	leaq	-120(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L2845:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L2845
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2850
	addq	$3536, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2850:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3588:
	.size	_ZN6icu_6724DateTimePatternGenerator11getSkeletonERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator11getSkeletonERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator17staticGetSkeletonERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator17staticGetSkeletonERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator17staticGetSkeletonERKNS_13UnicodeStringER10UErrorCode:
.LFB3589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-56(%rbp), %rcx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-3264(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVN6icu_6712FormatParserE(%rip), %rbx
	subq	$3536, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -3264(%rbp)
	leaq	-3256(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L2852:
	movl	$2, %edi
	movq	%rdx, (%rax)
	addq	$64, %rax
	movw	%di, -56(%rax)
	cmpq	%rax, %rcx
	jne	.L2852
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	leaq	16+_ZTVN6icu_6715DateTimeMatcherE(%rip), %rdx
	movq	$0, -56(%rbp)
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	-3568(%rbp), %r14
	movq	%r12, %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%r14, %rcx
	leaq	-3424(%rbp), %rdi
	movq	%rax, -3568(%rbp)
	movaps	%xmm0, -3424(%rbp)
	pxor	%xmm0, %xmm0
	subq	$56, %r12
	movaps	%xmm0, -3344(%rbp)
	movaps	%xmm0, -3328(%rbp)
	movaps	%xmm0, -3312(%rbp)
	movaps	%xmm0, -3296(%rbp)
	movups	%xmm0, -3496(%rbp)
	movups	%xmm0, -3480(%rbp)
	movups	%xmm0, -3464(%rbp)
	movups	%xmm0, -3448(%rbp)
	movb	$0, -3280(%rbp)
	movb	$0, -3432(%rbp)
	call	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6711PtnSkeleton11getSkeletonEv
	movq	%rbx, -3264(%rbp)
	leaq	-120(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L2853:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L2853
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2858
	addq	$3536, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2858:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3589:
	.size	_ZN6icu_6724DateTimePatternGenerator17staticGetSkeletonERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator17staticGetSkeletonERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator21getPatternForSkeletonERKNS_13UnicodeStringE
	.type	_ZNK6icu_6724DateTimePatternGenerator21getPatternForSkeletonERKNS_13UnicodeStringE, @function
_ZNK6icu_6724DateTimePatternGenerator21getPatternForSkeletonERKNS_13UnicodeStringE:
.LFB3656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L2860
	movswl	%ax, %edx
	sarl	$5, %edx
.L2861:
	testl	%edx, %edx
	je	.L2869
	movq	256(%r13), %rsi
	testb	$2, %al
	jne	.L2896
	movq	24(%r12), %rax
	movzwl	(%rax), %eax
	leal	-65(%rax), %ecx
	movl	%eax, %edx
	cmpw	$25, %cx
	ja	.L2866
.L2900:
	subl	$65, %eax
	cltq
	movq	8(%rsi,%rax,8), %r14
.L2867:
	testq	%r14, %r14
	je	.L2869
	leaq	-128(%rbp), %rbx
	jmp	.L2880
	.p2align 4,,10
	.p2align 3
.L2898:
	movzbl	8(%r12), %r15d
	andl	$1, %r15d
.L2872:
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testb	%r15b, %r15b
	jne	.L2897
	movq	152(%r14), %r14
	testq	%r14, %r14
	je	.L2869
.L2880:
	movq	72(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6711PtnSkeleton11getSkeletonEv
	movswl	-120(%rbp), %edx
	testb	$1, %dl
	jne	.L2898
	testw	%dx, %dx
	js	.L2873
	sarl	$5, %edx
.L2874:
	movzwl	8(%r12), %ecx
	testw	%cx, %cx
	js	.L2875
	movswl	%cx, %eax
	sarl	$5, %eax
.L2876:
	cmpl	%edx, %eax
	jne	.L2882
	andl	$1, %ecx
	je	.L2877
.L2882:
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	152(%r14), %r14
	testq	%r14, %r14
	jne	.L2880
	.p2align 4,,10
	.p2align 3
.L2869:
	leaq	4504(%r13), %rax
.L2859:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2899
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2896:
	.cfi_restore_state
	leaq	10(%r12), %rax
	movzwl	(%rax), %eax
	leal	-65(%rax), %ecx
	movl	%eax, %edx
	cmpw	$25, %cx
	jbe	.L2900
.L2866:
	subl	$97, %edx
	cmpw	$25, %dx
	ja	.L2869
	subl	$71, %eax
	cltq
	movq	8(%rsi,%rax,8), %r14
	jmp	.L2867
	.p2align 4,,10
	.p2align 3
.L2875:
	movl	12(%r12), %eax
	jmp	.L2876
	.p2align 4,,10
	.p2align 3
.L2873:
	movl	-116(%rbp), %edx
	jmp	.L2874
	.p2align 4,,10
	.p2align 3
.L2860:
	movl	12(%rsi), %edx
	jmp	.L2861
	.p2align 4,,10
	.p2align 3
.L2897:
	leaq	80(%r14), %rax
	jmp	.L2859
	.p2align 4,,10
	.p2align 3
.L2877:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r15b
	jmp	.L2872
.L2899:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3656:
	.size	_ZNK6icu_6724DateTimePatternGenerator21getPatternForSkeletonERKNS_13UnicodeStringE, .-_ZNK6icu_6724DateTimePatternGenerator21getPatternForSkeletonERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PtnSkeleton15getBaseSkeletonEv
	.type	_ZNK6icu_6711PtnSkeleton15getBaseSkeletonEv, @function
_ZNK6icu_6711PtnSkeleton15getBaseSkeletonEv:
.LFB3753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-58(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	104(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	leaq	120(%rsi), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L2904:
	movsbl	16(%r13), %r8d
	movsbw	0(%r13), %r9w
	testl	%r8d, %r8d
	jle	.L2902
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L2903:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%r8d, -80(%rbp)
	addl	$1, %r14d
	movw	%r9w, -58(%rbp)
	movl	%r9d, -76(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-80(%rbp), %r8d
	movl	-76(%rbp), %r9d
	cmpl	%r14d, %r8d
	jne	.L2903
.L2902:
	addq	$1, %r13
	cmpq	%r13, -72(%rbp)
	jne	.L2904
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	cmpb	$0, 136(%rbx)
	je	.L2901
	movswl	8(%r12), %ecx
	testw	%cx, %cx
	js	.L2907
	sarl	$5, %ecx
.L2908:
	movl	$97, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L2901
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
.L2901:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2914
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2907:
	.cfi_restore_state
	movl	12(%r12), %ecx
	jmp	.L2908
.L2914:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3753:
	.size	_ZNK6icu_6711PtnSkeleton15getBaseSkeletonEv, .-_ZNK6icu_6711PtnSkeleton15getBaseSkeletonEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator15getBaseSkeletonERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator15getBaseSkeletonERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator15getBaseSkeletonERKNS_13UnicodeStringER10UErrorCode:
.LFB3590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-56(%rbp), %rcx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-3264(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVN6icu_6712FormatParserE(%rip), %rbx
	subq	$3536, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -3264(%rbp)
	leaq	-3256(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L2916:
	movl	$2, %edi
	movq	%rdx, (%rax)
	addq	$64, %rax
	movw	%di, -56(%rax)
	cmpq	%rax, %rcx
	jne	.L2916
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	leaq	16+_ZTVN6icu_6715DateTimeMatcherE(%rip), %rcx
	movq	%r12, %rdx
	movq	$0, -56(%rbp)
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	leaq	-3568(%rbp), %r14
	movq	%rax, -3568(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%r14, %rcx
	leaq	-3424(%rbp), %rdi
	movb	$0, -3280(%rbp)
	movaps	%xmm0, -3424(%rbp)
	pxor	%xmm0, %xmm0
	subq	$56, %r12
	movaps	%xmm0, -3344(%rbp)
	movaps	%xmm0, -3328(%rbp)
	movaps	%xmm0, -3312(%rbp)
	movaps	%xmm0, -3296(%rbp)
	movups	%xmm0, -3496(%rbp)
	movups	%xmm0, -3480(%rbp)
	movups	%xmm0, -3464(%rbp)
	movups	%xmm0, -3448(%rbp)
	movb	$0, -3432(%rbp)
	call	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6711PtnSkeleton15getBaseSkeletonEv
	movq	%rbx, -3264(%rbp)
	leaq	-120(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L2917:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L2917
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2922
	addq	$3536, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2922:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3590:
	.size	_ZN6icu_6724DateTimePatternGenerator15getBaseSkeletonERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator15getBaseSkeletonERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator21staticGetBaseSkeletonERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator21staticGetBaseSkeletonERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator21staticGetBaseSkeletonERKNS_13UnicodeStringER10UErrorCode:
.LFB3591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-56(%rbp), %rcx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-3264(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVN6icu_6712FormatParserE(%rip), %rbx
	subq	$3536, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -3264(%rbp)
	leaq	-3256(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L2924:
	movl	$2, %edi
	movq	%rdx, (%rax)
	addq	$64, %rax
	movw	%di, -56(%rax)
	cmpq	%rax, %rcx
	jne	.L2924
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rax
	leaq	16+_ZTVN6icu_6715DateTimeMatcherE(%rip), %rdx
	movq	$0, -56(%rbp)
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	-3568(%rbp), %r14
	movq	%r12, %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%r14, %rcx
	leaq	-3424(%rbp), %rdi
	movq	%rax, -3568(%rbp)
	movaps	%xmm0, -3424(%rbp)
	pxor	%xmm0, %xmm0
	subq	$56, %r12
	movaps	%xmm0, -3344(%rbp)
	movaps	%xmm0, -3328(%rbp)
	movaps	%xmm0, -3312(%rbp)
	movaps	%xmm0, -3296(%rbp)
	movups	%xmm0, -3496(%rbp)
	movups	%xmm0, -3480(%rbp)
	movups	%xmm0, -3464(%rbp)
	movups	%xmm0, -3448(%rbp)
	movb	$0, -3280(%rbp)
	movb	$0, -3432(%rbp)
	call	_ZN6icu_6715DateTimeMatcher3setERKNS_13UnicodeStringEPNS_12FormatParserERNS_11PtnSkeletonE
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6711PtnSkeleton15getBaseSkeletonEv
	movq	%rbx, -3264(%rbp)
	leaq	-120(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L2925:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L2925
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2930
	addq	$3536, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2930:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3591:
	.size	_ZN6icu_6724DateTimePatternGenerator21staticGetBaseSkeletonERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator21staticGetBaseSkeletonERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PtnSkeleton12getFirstCharEv
	.type	_ZNK6icu_6711PtnSkeleton12getFirstCharEv, @function
_ZNK6icu_6711PtnSkeleton12getFirstCharEv:
.LFB3754:
	.cfi_startproc
	endbr64
	cmpb	$0, 120(%rdi)
	jne	.L2934
	cmpb	$0, 121(%rdi)
	jne	.L2935
	cmpb	$0, 122(%rdi)
	jne	.L2936
	cmpb	$0, 123(%rdi)
	jne	.L2937
	cmpb	$0, 124(%rdi)
	jne	.L2938
	cmpb	$0, 125(%rdi)
	jne	.L2939
	cmpb	$0, 126(%rdi)
	jne	.L2940
	cmpb	$0, 127(%rdi)
	jne	.L2941
	cmpb	$0, 128(%rdi)
	jne	.L2942
	cmpb	$0, 129(%rdi)
	jne	.L2943
	cmpb	$0, 130(%rdi)
	jne	.L2944
	cmpb	$0, 131(%rdi)
	jne	.L2945
	cmpb	$0, 132(%rdi)
	jne	.L2946
	cmpb	$0, 133(%rdi)
	jne	.L2947
	cmpb	$0, 134(%rdi)
	jne	.L2948
	xorl	%eax, %eax
	cmpb	$0, 135(%rdi)
	jne	.L2950
	ret
.L2934:
	xorl	%eax, %eax
.L2932:
	movsbw	104(%rdi,%rax), %ax
	ret
.L2935:
	movl	$1, %eax
	jmp	.L2932
.L2946:
	movl	$12, %eax
	jmp	.L2932
.L2950:
	movl	$15, %eax
	jmp	.L2932
.L2936:
	movl	$2, %eax
	jmp	.L2932
.L2937:
	movl	$3, %eax
	jmp	.L2932
.L2938:
	movl	$4, %eax
	jmp	.L2932
.L2939:
	movl	$5, %eax
	jmp	.L2932
.L2940:
	movl	$6, %eax
	jmp	.L2932
.L2941:
	movl	$7, %eax
	jmp	.L2932
.L2942:
	movl	$8, %eax
	jmp	.L2932
.L2943:
	movl	$9, %eax
	jmp	.L2932
.L2944:
	movl	$10, %eax
	jmp	.L2932
.L2945:
	movl	$11, %eax
	jmp	.L2932
.L2947:
	movl	$13, %eax
	jmp	.L2932
.L2948:
	movl	$14, %eax
	jmp	.L2932
	.cfi_endproc
.LFE3754:
	.size	_ZNK6icu_6711PtnSkeleton12getFirstCharEv, .-_ZNK6icu_6711PtnSkeleton12getFirstCharEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_677PtnElemC2ERKNS_13UnicodeStringES3_
	.type	_ZN6icu_677PtnElemC2ERKNS_13UnicodeStringES3_, @function
_ZN6icu_677PtnElemC2ERKNS_13UnicodeStringES3_:
.LFB3762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_677PtnElemE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	$0, 72(%rbx)
	leaq	80(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	$0, 152(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3762:
	.size	_ZN6icu_677PtnElemC2ERKNS_13UnicodeStringES3_, .-_ZN6icu_677PtnElemC2ERKNS_13UnicodeStringES3_
	.globl	_ZN6icu_677PtnElemC1ERKNS_13UnicodeStringES3_
	.set	_ZN6icu_677PtnElemC1ERKNS_13UnicodeStringES3_,_ZN6icu_677PtnElemC2ERKNS_13UnicodeStringES3_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721DTSkeletonEnumerationC2ERNS_10PatternMapENS_9dtStrEnumER10UErrorCode
	.type	_ZN6icu_6721DTSkeletonEnumerationC2ERNS_10PatternMapENS_9dtStrEnumER10UErrorCode, @function
_ZN6icu_6721DTSkeletonEnumerationC2ERNS_10PatternMapENS_9dtStrEnumER10UErrorCode:
.LFB3771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$184, %rsp
	movq	%rdi, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	leaq	16+_ZTVN6icu_6721DTSkeletonEnumerationE(%rip), %rax
	movl	$2, %edi
	movq	$0, 120(%r14)
	movq	%rax, (%r14)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$0, 116(%r14)
	movw	%di, -184(%rbp)
	movl	$40, %edi
	movq	%rax, -192(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L2954
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L3007
	movq	-200(%rbp), %rax
	movq	120(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3008
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-200(%rbp), %rax
	movq	%r14, 120(%rax)
	movl	0(%r13), %eax
	jmp	.L2982
.L3007:
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorD0Ev@PLT
	movl	0(%r13), %eax
.L2982:
	leaq	-192(%rbp), %r14
	testl	%eax, %eax
	jg	.L2959
.L2979:
	leaq	8(%rbx), %rax
	leaq	-192(%rbp), %r14
	movq	%rax, -208(%rbp)
	leaq	424(%rbx), %rax
	movq	%rax, -216(%rbp)
.L2961:
	movq	-208(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L2960
	jmp	.L2974
	.p2align 4,,10
	.p2align 3
.L3011:
	andl	$2, %edx
	jne	.L2968
	movq	-168(%rbp), %rax
	movzwl	(%rax), %eax
	leal	-68(%rax), %edx
	cmpw	$53, %dx
	ja	.L2967
	movabsq	$11401940412441119, %rcx
	btq	%rdx, %rcx
	jnc	.L3009
	.p2align 4,,10
	.p2align 3
.L2969:
	movq	152(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2974
.L2960:
	cmpl	$1, %r12d
	je	.L2962
	cmpl	$2, %r12d
	je	.L2963
	testl	%r12d, %r12d
	je	.L3010
.L2964:
	movzwl	-184(%rbp), %edx
	testw	%dx, %dx
	js	.L2965
	movswl	%dx, %eax
	sarl	$5, %eax
.L2966:
	cmpl	$1, %eax
	je	.L3011
.L2967:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2970
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L2971
	movq	-200(%rbp), %rax
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	120(%rax), %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L2969
	movq	-200(%rbp), %rax
	movq	120(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2973
	movq	(%rdi), %rdx
	call	*8(%rdx)
.L2973:
	movq	-200(%rbp), %rax
	movq	$0, 120(%rax)
.L2971:
	movq	(%r15), %rdx
	movq	%r15, %rdi
	call	*8(%rdx)
	.p2align 4,,10
	.p2align 3
.L2959:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3012
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2965:
	.cfi_restore_state
	movl	-180(%rbp), %eax
	jmp	.L2966
	.p2align 4,,10
	.p2align 3
.L3010:
	leaq	8(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L2964
	.p2align 4,,10
	.p2align 3
.L2968:
	movzwl	-182(%rbp), %eax
	leal	-68(%rax), %edx
	cmpw	$53, %dx
	ja	.L2967
	movabsq	$11401940412441119, %rcx
	btq	%rdx, %rcx
	jc	.L2969
.L3009:
	cmpw	$118, %ax
	jne	.L2967
	movq	152(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2960
.L2974:
	addq	$8, -208(%rbp)
	movq	-208(%rbp), %rax
	cmpq	%rax, -216(%rbp)
	jne	.L2961
	jmp	.L2959
	.p2align 4,,10
	.p2align 3
.L2963:
	leaq	80(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L2964
	.p2align 4,,10
	.p2align 3
.L2962:
	leaq	-128(%rbp), %r15
	movq	72(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6711PtnSkeleton11getSkeletonEv
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L2964
.L2970:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L2959
	movl	$7, 0(%r13)
	jmp	.L2959
.L3008:
	movq	%r14, 120(%rax)
	jmp	.L2979
.L2954:
	cmpl	$0, 0(%r13)
	jle	.L2957
.L3005:
	leaq	-192(%rbp), %r14
	jmp	.L2959
.L3012:
	call	__stack_chk_fail@PLT
.L2957:
	movq	-200(%rbp), %rax
	movq	120(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2978
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-200(%rbp), %rax
	movq	$0, 120(%rax)
.L2978:
	movl	$7, 0(%r13)
	jmp	.L3005
	.cfi_endproc
.LFE3771:
	.size	_ZN6icu_6721DTSkeletonEnumerationC2ERNS_10PatternMapENS_9dtStrEnumER10UErrorCode, .-_ZN6icu_6721DTSkeletonEnumerationC2ERNS_10PatternMapENS_9dtStrEnumER10UErrorCode
	.globl	_ZN6icu_6721DTSkeletonEnumerationC1ERNS_10PatternMapENS_9dtStrEnumER10UErrorCode
	.set	_ZN6icu_6721DTSkeletonEnumerationC1ERNS_10PatternMapENS_9dtStrEnumER10UErrorCode,_ZN6icu_6721DTSkeletonEnumerationC2ERNS_10PatternMapENS_9dtStrEnumER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator12getSkeletonsER10UErrorCode
	.type	_ZNK6icu_6724DateTimePatternGenerator12getSkeletonsER10UErrorCode, @function
_ZNK6icu_6724DateTimePatternGenerator12getSkeletonsER10UErrorCode:
.LFB3655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L3022
	movl	4600(%rdi), %eax
	movq	%rdi, %r13
	movq	%rsi, %rbx
	testl	%eax, %eax
	jg	.L3023
	movl	$128, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3017
	movq	256(%r13), %rsi
	movl	$1, %edx
	movq	%rbx, %rcx
	movq	%rax, %rdi
	call	_ZN6icu_6721DTSkeletonEnumerationC1ERNS_10PatternMapENS_9dtStrEnumER10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L3013
	movq	%r12, %rdi
	call	_ZN6icu_6721DTSkeletonEnumerationD0Ev
.L3022:
	xorl	%r12d, %r12d
.L3013:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3023:
	.cfi_restore_state
	movl	%eax, (%rsi)
	xorl	%r12d, %r12d
	addq	$8, %rsp
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3017:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L3022
	movl	$7, (%rbx)
	jmp	.L3022
	.cfi_endproc
.LFE3655:
	.size	_ZNK6icu_6724DateTimePatternGenerator12getSkeletonsER10UErrorCode, .-_ZNK6icu_6724DateTimePatternGenerator12getSkeletonsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724DateTimePatternGenerator16getBaseSkeletonsER10UErrorCode
	.type	_ZNK6icu_6724DateTimePatternGenerator16getBaseSkeletonsER10UErrorCode, @function
_ZNK6icu_6724DateTimePatternGenerator16getBaseSkeletonsER10UErrorCode:
.LFB3657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L3033
	movl	4600(%rdi), %eax
	movq	%rdi, %r13
	movq	%rsi, %rbx
	testl	%eax, %eax
	jg	.L3034
	movl	$128, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3028
	movq	256(%r13), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movq	%rax, %rdi
	call	_ZN6icu_6721DTSkeletonEnumerationC1ERNS_10PatternMapENS_9dtStrEnumER10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L3024
	movq	%r12, %rdi
	call	_ZN6icu_6721DTSkeletonEnumerationD0Ev
.L3033:
	xorl	%r12d, %r12d
.L3024:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3034:
	.cfi_restore_state
	movl	%eax, (%rsi)
	xorl	%r12d, %r12d
	addq	$8, %rsp
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3028:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L3033
	movl	$7, (%rbx)
	jmp	.L3033
	.cfi_endproc
.LFE3657:
	.size	_ZNK6icu_6724DateTimePatternGenerator16getBaseSkeletonsER10UErrorCode, .-_ZNK6icu_6724DateTimePatternGenerator16getBaseSkeletonsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721DTSkeletonEnumeration15isCanonicalItemERKNS_13UnicodeStringE
	.type	_ZN6icu_6721DTSkeletonEnumeration15isCanonicalItemERKNS_13UnicodeStringE, @function
_ZN6icu_6721DTSkeletonEnumeration15isCanonicalItemERKNS_13UnicodeStringE:
.LFB5276:
	.cfi_startproc
	endbr64
	movzwl	8(%rsi), %edx
	testw	%dx, %dx
	js	.L3036
	movswl	%dx, %eax
	sarl	$5, %eax
.L3037:
	xorl	%r8d, %r8d
	cmpl	$1, %eax
	jne	.L3035
	andl	$2, %edx
	je	.L3087
	movzwl	10(%rsi), %eax
	cmpw	$71, %ax
	je	.L3070
.L3086:
	cmpw	$121, %ax
	je	.L3070
	cmpw	$81, %ax
	je	.L3070
	cmpw	$77, %ax
	je	.L3070
	cmpw	$119, %ax
	je	.L3070
	cmpw	$87, %ax
	je	.L3070
	cmpw	$69, %ax
	je	.L3070
	cmpw	$68, %ax
	je	.L3070
	cmpw	$70, %ax
	je	.L3070
	cmpw	$100, %ax
	je	.L3070
	cmpw	$97, %ax
	je	.L3070
	cmpw	$72, %ax
	je	.L3070
	cmpw	$109, %ax
	je	.L3070
	cmpw	$115, %ax
	je	.L3070
	cmpw	$83, %ax
	je	.L3070
	cmpw	$118, %ax
	sete	%r8b
.L3035:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3087:
	movq	24(%rsi), %rax
	movzwl	(%rax), %eax
	cmpw	$71, %ax
	jne	.L3086
.L3070:
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3036:
	movl	12(%rsi), %eax
	jmp	.L3037
	.cfi_endproc
.LFE5276:
	.size	_ZN6icu_6721DTSkeletonEnumeration15isCanonicalItemERKNS_13UnicodeStringE, .-_ZN6icu_6721DTSkeletonEnumeration15isCanonicalItemERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722DTRedundantEnumerationC2Ev
	.type	_ZN6icu_6722DTRedundantEnumerationC2Ev, @function
_ZN6icu_6722DTRedundantEnumerationC2Ev:
.LFB3782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	leaq	16+_ZTVN6icu_6722DTRedundantEnumerationE(%rip), %rax
	movl	$0, 116(%rbx)
	movq	%rax, (%rbx)
	movq	$0, 120(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3782:
	.size	_ZN6icu_6722DTRedundantEnumerationC2Ev, .-_ZN6icu_6722DTRedundantEnumerationC2Ev
	.globl	_ZN6icu_6722DTRedundantEnumerationC1Ev
	.set	_ZN6icu_6722DTRedundantEnumerationC1Ev,_ZN6icu_6722DTRedundantEnumerationC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722DTRedundantEnumeration3addERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6722DTRedundantEnumeration3addERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6722DTRedundantEnumeration3addERKNS_13UnicodeStringER10UErrorCode:
.LFB3784:
	.cfi_startproc
	endbr64
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L3115
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	$0, 120(%rdi)
	movq	%rdx, %rbx
	je	.L3092
.L3098:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3118
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L3099
.L3108:
	movq	(%r14), %rax
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r14, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	movq	8(%rax), %rax
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3099:
	.cfi_restore_state
	movq	120(%r12), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L3119
.L3090:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3115:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L3092:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3095
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L3120
	movq	120(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3106
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r14, 120(%r12)
	movl	(%rbx), %eax
	jmp	.L3107
	.p2align 4,,10
	.p2align 3
.L3119:
	movq	120(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3100
	movq	(%rdi), %rax
	call	*8(%rax)
.L3100:
	movq	$0, 120(%r12)
	jmp	.L3108
.L3118:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L3090
.L3101:
	movl	$7, (%rbx)
	jmp	.L3090
	.p2align 4,,10
	.p2align 3
.L3120:
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorD0Ev@PLT
	movl	(%rbx), %eax
.L3107:
	testl	%eax, %eax
	jle	.L3098
	jmp	.L3090
	.p2align 4,,10
	.p2align 3
.L3106:
	movq	%r14, 120(%r12)
	jmp	.L3098
.L3095:
	cmpl	$0, (%rbx)
	jg	.L3090
	movq	120(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3101
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 120(%r12)
	jmp	.L3101
	.cfi_endproc
.LFE3784:
	.size	_ZN6icu_6722DTRedundantEnumeration3addERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6722DTRedundantEnumeration3addERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator13getRedundantsER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator13getRedundantsER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator13getRedundantsER10UErrorCode:
.LFB3658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -408(%rbp)
	movl	(%rsi), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L3213
	movl	4600(%rdi), %eax
	movq	%rsi, %r13
	testl	%eax, %eax
	jg	.L3214
	movl	$128, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3125
	movq	%rax, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	movl	0(%r13), %r8d
	leaq	16+_ZTVN6icu_6722DTRedundantEnumerationE(%rip), %rax
	movl	$0, 116(%r15)
	movq	%rax, (%r15)
	leaq	_ZN6icu_6722DTRedundantEnumerationD0Ev(%rip), %rax
	movq	$0, 120(%r15)
	testl	%r8d, %r8d
	jle	.L3215
.L3126:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L3121
.L3125:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L3213
	movl	$7, 0(%r13)
	.p2align 4,,10
	.p2align 3
.L3213:
	xorl	%r12d, %r12d
.L3121:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3216
	addq	$440, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3214:
	.cfi_restore_state
	movl	%eax, (%rsi)
	jmp	.L3213
	.p2align 4,,10
	.p2align 3
.L3215:
	leaq	-384(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -448(%rbp)
	call	_ZN6icu_6718PatternMapIteratorC1ER10UErrorCode
	movl	0(%r13), %edi
	testl	%edi, %edi
	jg	.L3129
	movq	-408(%rbp), %rax
	movq	256(%rax), %rdx
	movl	-376(%rbp), %eax
	movq	%rdx, -352(%rbp)
	testq	%rdx, %rdx
	je	.L3130
	cmpl	$51, %eax
	jg	.L3130
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rsi
	leaq	16+_ZTVN6icu_6715DateTimeMatcherE(%rip), %rdi
	movq	%r15, -464(%rbp)
	movq	-368(%rbp), %rcx
	movq	%rdi, %xmm6
	movq	%rsi, %xmm7
	movq	%r13, -456(%rbp)
	punpcklqdq	%xmm7, %xmm6
	movaps	%xmm6, -480(%rbp)
.L3131:
	testq	%rcx, %rcx
	je	.L3132
	cmpq	$0, 152(%rcx)
	je	.L3217
.L3133:
	movq	-448(%rbp), %rdi
	call	_ZN6icu_6718PatternMapIterator4nextEv
	pxor	%xmm0, %xmm0
	movb	$0, -64(%rbp)
	movdqa	-480(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	leaq	-128(%rbp), %rcx
	movq	-368(%rbp), %rsi
	leaq	-192(%rbp), %r10
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	movdqu	16(%rax), %xmm2
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movdqu	32(%rax), %xmm3
	movdqu	128(%rax), %xmm2
	movq	%rcx, -440(%rbp)
	leaq	-112(%rbp), %rcx
	movaps	%xmm3, -176(%rbp)
	movdqu	48(%rax), %xmm4
	movq	%rcx, -416(%rbp)
	movaps	%xmm4, -160(%rbp)
	movdqu	64(%rax), %xmm5
	movaps	%xmm5, -144(%rbp)
	movdqu	80(%rax), %xmm6
	movdqu	96(%rax), %xmm7
	movdqu	112(%rax), %xmm1
	movzbl	144(%rax), %eax
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm6, -128(%rbp)
	movb	%al, -64(%rbp)
	movq	-408(%rbp), %rax
	movaps	%xmm7, -112(%rbp)
	movq	256(%rax), %rdi
	movaps	%xmm1, -96(%rbp)
	testq	%rsi, %rsi
	je	.L3167
	movq	72(%rsi), %rsi
.L3167:
	xorl	%edx, %edx
	call	_ZNK6icu_6710PatternMap22getPatternFromSkeletonERKNS_11PtnSkeletonEPPS2_
	movzwl	8(%rax), %edx
	movq	%rax, -432(%rbp)
	testw	%dx, %dx
	js	.L3135
	movswl	%dx, %eax
	sarl	$5, %eax
.L3136:
	cmpl	$1, %eax
	jne	.L3137
	andl	$2, %edx
	movq	-432(%rbp), %rax
	jne	.L3138
	movq	24(%rax), %rax
	movzwl	(%rax), %eax
	leal	-68(%rax), %edx
	cmpw	$53, %dx
	jbe	.L3211
	.p2align 4,,10
	.p2align 3
.L3137:
	movq	-408(%rbp), %rax
	movq	4488(%rax), %rax
	testq	%rax, %rax
	je	.L3218
	movdqa	(%r10), %xmm3
	movups	%xmm3, 16(%rax)
	movdqa	16(%r10), %xmm4
	movups	%xmm4, 32(%rax)
	movdqa	32(%r10), %xmm5
	movups	%xmm5, 48(%rax)
	movdqa	48(%r10), %xmm6
	movups	%xmm6, 64(%rax)
	movdqa	-128(%rbp), %xmm7
	movups	%xmm7, 80(%rax)
	movdqa	-112(%rbp), %xmm3
	movups	%xmm3, 96(%rax)
	movdqa	-96(%rbp), %xmm4
	movups	%xmm4, 112(%rax)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm5, 128(%rax)
	movzbl	-64(%rbp), %edx
	movb	%dl, 144(%rax)
.L3143:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %esi
	movq	-440(%rbp), %rbx
	leaq	-272(%rbp), %r12
	movq	%rax, -272(%rbp)
	leaq	-386(%rbp), %rax
	movw	%si, -264(%rbp)
	movq	%rax, -424(%rbp)
	.p2align 4,,10
	.p2align 3
.L3146:
	movsbl	16(%rbx), %r14d
	movsbw	(%rbx), %r15w
	xorl	%r13d, %r13d
	testl	%r14d, %r14d
	jle	.L3147
	.p2align 4,,10
	.p2align 3
.L3144:
	movq	-424(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	addl	$1, %r13d
	movl	$1, %ecx
	movw	%r15w, -386(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	%r13d, %r14d
	jne	.L3144
.L3147:
	addq	$1, %rbx
	cmpq	%rbx, -416(%rbp)
	jne	.L3146
	leaq	-336(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-456(%rbp), %rbx
	movq	-408(%rbp), %rsi
	movq	%rbx, %r8
	call	_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringE28UDateTimePatternMatchOptionsR10UErrorCode
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L3209
	movq	-432(%rbp), %rcx
	movzwl	-264(%rbp), %eax
	movswl	8(%rcx), %ecx
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L3149
	testw	%ax, %ax
	js	.L3150
	movswl	%ax, %edx
	sarl	$5, %edx
.L3151:
	testw	%cx, %cx
	js	.L3152
	sarl	$5, %ecx
.L3153:
	testb	%sil, %sil
	jne	.L3154
	cmpl	%edx, %ecx
	je	.L3219
.L3154:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L3162:
	movq	-352(%rbp), %rdx
	movl	-376(%rbp), %eax
	testq	%rdx, %rdx
	je	.L3207
	cmpl	$51, %eax
	jg	.L3207
	movq	-368(%rbp), %rcx
	jmp	.L3131
.L3219:
	movq	-432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L3149:
	testb	%sil, %sil
	je	.L3154
	movq	-456(%rbp), %rbx
	movq	-432(%rbp), %rsi
	movq	-464(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZN6icu_6722DTRedundantEnumeration3addERKNS_13UnicodeStringER10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L3154
.L3209:
	movq	%r12, %rdi
	movq	-464(%rbp), %r15
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L3129:
	movq	-360(%rbp), %rdi
	leaq	16+_ZTVN6icu_6718PatternMapIteratorE(%rip), %rax
	movq	%r15, %rbx
	xorl	%r15d, %r15d
	movq	%rax, -384(%rbp)
	testq	%rdi, %rdi
	je	.L3171
.L3170:
	movq	(%rdi), %rax
	leaq	_ZN6icu_6715DateTimeMatcherD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3165
	call	_ZN6icu_677UMemorydlEPv@PLT
.L3166:
	testq	%rbx, %rbx
	je	.L3212
.L3171:
	movq	(%rbx), %rax
	movq	%r15, %r12
	movq	%rbx, %r15
	movq	8(%rax), %rax
	jmp	.L3126
	.p2align 4,,10
	.p2align 3
.L3138:
	movzwl	10(%rax), %eax
	leal	-68(%rax), %edx
	cmpw	$53, %dx
	ja	.L3137
.L3211:
	movabsq	$11401940412441119, %rcx
	btq	%rdx, %rcx
	jc	.L3162
	cmpw	$118, %ax
	je	.L3162
	jmp	.L3137
	.p2align 4,,10
	.p2align 3
.L3135:
	movl	12(%rax), %eax
	jmp	.L3136
	.p2align 4,,10
	.p2align 3
.L3217:
	addl	$1, %eax
	cmpl	$52, %eax
	je	.L3207
.L3132:
	movl	$51, %ecx
	movslq	%eax, %rdi
	movl	%ecx, %ebx
	leaq	(%rdx,%rdi,8), %rsi
	subl	%eax, %ebx
	movq	%rbx, %rax
	addq	%rdi, %rax
	leaq	8(%rdx,%rax,8), %rax
.L3134:
	cmpq	$0, 8(%rsi)
	jne	.L3133
	addq	$8, %rsi
	cmpq	%rsi, %rax
	jne	.L3134
	.p2align 4,,10
	.p2align 3
.L3207:
	movq	-464(%rbp), %r15
.L3130:
	movq	-360(%rbp), %rdi
	leaq	16+_ZTVN6icu_6718PatternMapIteratorE(%rip), %rax
	xorl	%ebx, %ebx
	movq	%rax, -384(%rbp)
	testq	%rdi, %rdi
	jne	.L3170
.L3212:
	movq	%r15, %r12
	jmp	.L3121
	.p2align 4,,10
	.p2align 3
.L3218:
	movl	$152, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3142
	movq	%rax, %rdi
	leaq	-208(%rbp), %rsi
	call	_ZN6icu_6715DateTimeMatcherC1ERKS0_
	movq	-408(%rbp), %rax
	movq	%rdx, 4488(%rax)
	jmp	.L3143
	.p2align 4,,10
	.p2align 3
.L3152:
	movq	-432(%rbp), %rax
	movl	12(%rax), %ecx
	jmp	.L3153
	.p2align 4,,10
	.p2align 3
.L3150:
	movl	-260(%rbp), %edx
	jmp	.L3151
	.p2align 4,,10
	.p2align 3
.L3165:
	call	*%rax
	jmp	.L3166
.L3216:
	call	__stack_chk_fail@PLT
.L3142:
	movq	-456(%rbp), %r13
	movq	-408(%rbp), %rax
	movq	-464(%rbp), %r15
	movq	$0, 4488(%rax)
	movl	$7, 0(%r13)
	jmp	.L3129
	.cfi_endproc
.LFE3658:
	.size	_ZN6icu_6724DateTimePatternGenerator13getRedundantsER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator13getRedundantsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722DTRedundantEnumeration15isCanonicalItemERKNS_13UnicodeStringE
	.type	_ZNK6icu_6722DTRedundantEnumeration15isCanonicalItemERKNS_13UnicodeStringE, @function
_ZNK6icu_6722DTRedundantEnumeration15isCanonicalItemERKNS_13UnicodeStringE:
.LFB5278:
	.cfi_startproc
	endbr64
	movzwl	8(%rsi), %edx
	testw	%dx, %dx
	js	.L3221
	movswl	%dx, %eax
	sarl	$5, %eax
.L3222:
	xorl	%r8d, %r8d
	cmpl	$1, %eax
	jne	.L3220
	andl	$2, %edx
	je	.L3272
	movzwl	10(%rsi), %eax
	cmpw	$71, %ax
	je	.L3255
.L3271:
	cmpw	$121, %ax
	je	.L3255
	cmpw	$81, %ax
	je	.L3255
	cmpw	$77, %ax
	je	.L3255
	cmpw	$119, %ax
	je	.L3255
	cmpw	$87, %ax
	je	.L3255
	cmpw	$69, %ax
	je	.L3255
	cmpw	$68, %ax
	je	.L3255
	cmpw	$70, %ax
	je	.L3255
	cmpw	$100, %ax
	je	.L3255
	cmpw	$97, %ax
	je	.L3255
	cmpw	$72, %ax
	je	.L3255
	cmpw	$109, %ax
	je	.L3255
	cmpw	$115, %ax
	je	.L3255
	cmpw	$83, %ax
	je	.L3255
	cmpw	$118, %ax
	sete	%r8b
.L3220:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3272:
	movq	24(%rsi), %rax
	movzwl	(%rax), %eax
	cmpw	$71, %ax
	jne	.L3271
.L3255:
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3221:
	movl	12(%rsi), %eax
	jmp	.L3222
	.cfi_endproc
.LFE5278:
	.size	_ZNK6icu_6722DTRedundantEnumeration15isCanonicalItemERKNS_13UnicodeStringE, .-_ZNK6icu_6722DTRedundantEnumeration15isCanonicalItemERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGeneratorD2Ev
	.type	_ZN6icu_6724DateTimePatternGeneratorD2Ev, @function
_ZN6icu_6724DateTimePatternGeneratorD2Ev:
.LFB3569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724DateTimePatternGeneratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	4496(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L3274
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3275
	call	uhash_close_67@PLT
.L3275:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L3274:
	movq	232(%r13), %r14
	testq	%r14, %r14
	je	.L3276
	movq	(%r14), %rax
	leaq	_ZN6icu_6712FormatParserD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3277
	leaq	16+_ZTVN6icu_6712FormatParserE(%rip), %rax
	leaq	3144(%r14), %rbx
	movq	%rax, (%r14)
	leaq	-56(%r14), %r12
	.p2align 4,,10
	.p2align 3
.L3278:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L3278
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L3276:
	movq	240(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3279
	movq	(%rdi), %rax
	leaq	_ZN6icu_6715DateTimeMatcherD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3280
	call	_ZN6icu_677UMemorydlEPv@PLT
.L3279:
	movq	248(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3281
	movq	(%rdi), %rax
	leaq	_ZN6icu_6712DistanceInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3282
	call	_ZN6icu_677UMemorydlEPv@PLT
.L3281:
	movq	256(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3283
	movq	(%rdi), %rax
	call	*8(%rax)
.L3283:
	movq	4488(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3284
	movq	(%rdi), %rax
	leaq	_ZN6icu_6715DateTimeMatcherD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3285
	call	_ZN6icu_677UMemorydlEPv@PLT
.L3284:
	leaq	4504(%r13), %rdi
	leaq	4296(%r13), %rbx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	4424(%r13), %rdi
	leaq	1224(%r13), %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	4360(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L3286:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%rbx, %r12
	jne	.L3286
	movq	%r12, %rbx
	leaq	200(%r13), %r12
	.p2align 4,,10
	.p2align 3
.L3287:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L3287
	leaq	8(%r13), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
.L3285:
	.cfi_restore_state
	call	*%rax
	jmp	.L3284
.L3277:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L3276
.L3280:
	call	*%rax
	jmp	.L3279
.L3282:
	call	*%rax
	jmp	.L3281
	.cfi_endproc
.LFE3569:
	.size	_ZN6icu_6724DateTimePatternGeneratorD2Ev, .-_ZN6icu_6724DateTimePatternGeneratorD2Ev
	.globl	_ZN6icu_6724DateTimePatternGeneratorD1Ev
	.set	_ZN6icu_6724DateTimePatternGeneratorD1Ev,_ZN6icu_6724DateTimePatternGeneratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGeneratorD0Ev
	.type	_ZN6icu_6724DateTimePatternGeneratorD0Ev, @function
_ZN6icu_6724DateTimePatternGeneratorD0Ev:
.LFB3571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6724DateTimePatternGeneratorD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3571:
	.size	_ZN6icu_6724DateTimePatternGeneratorD0Ev, .-_ZN6icu_6724DateTimePatternGeneratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator19createEmptyInstanceER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator19createEmptyInstanceER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator19createEmptyInstanceER10UErrorCode:
.LFB3553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L3316
	movq	%rdi, %rbx
	movl	$4608, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3317
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6724DateTimePatternGeneratorC1ER10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L3315
	movq	(%r12), %rdx
	leaq	_ZN6icu_6724DateTimePatternGeneratorD0Ev(%rip), %rax
	cmpq	%rax, 8(%rdx)
	jne	.L3326
	movq	%r12, %rdi
	call	_ZN6icu_6724DateTimePatternGeneratorD1Ev
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L3315:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3317:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L3316
	movl	$7, (%rbx)
.L3316:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3326:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6724DateTimePatternGeneratorD0Ev
	jmp	.L3315
	.cfi_endproc
.LFE3553:
	.size	_ZN6icu_6724DateTimePatternGenerator19createEmptyInstanceER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator19createEmptyInstanceER10UErrorCode
	.section	.rodata.str1.1
.LC36:
	.string	"und"
.LC37:
	.string	"001"
.LC38:
	.string	"hours"
.LC39:
	.string	"h24"
.LC40:
	.string	"h23"
.LC41:
	.string	"h12"
.LC42:
	.string	"h11"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6724DateTimePatternGenerator21getAllowedHourFormatsERKNS_6LocaleER10UErrorCode.part.0, @function
_ZN6icu_6724DateTimePatternGenerator21getAllowedHourFormatsERKNS_6LocaleER10UErrorCode.part.0:
.LFB5273:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-288(%rbp), %r15
	leaq	26(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	8(%rsi), %rax
	movq	%rsi, -320(%rbp)
	movq	%rax, -312(%rbp)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	-320(%rbp), %r9
	cmpb	$0, 8(%r9)
	je	.L3328
	cmpb	$0, 26(%r9)
	je	.L3328
.L3329:
	movq	-312(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, -320(%rbp)
	call	_ZN6icu_67L32getAllowedHourFormatsLangCountryEPKcS1_R10UErrorCode
	movq	-320(%rbp), %r9
	leaq	-64(%rbp), %rdx
	movq	%r13, %r8
	movl	$8, %ecx
	leaq	.LC38(%rip), %rsi
	movq	%rdx, -328(%rbp)
	movq	%rax, %r12
	movq	%r9, %rdi
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	0(%r13), %edx
	xorl	%r11d, %r11d
	movw	%r11w, 4568(%rbx)
	testl	%edx, %edx
	jg	.L3332
	testl	%eax, %eax
	movq	-328(%rbp), %rdx
	jg	.L3392
.L3332:
	testq	%r12, %r12
	jne	.L3355
.L3354:
	leaq	-292(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -292(%rbp)
	call	_ZN6icu_676Region11getInstanceEPKcR10UErrorCode@PLT
	movl	-292(%rbp), %esi
	testl	%esi, %esi
	jle	.L3393
.L3346:
	cmpw	$0, 4568(%rbx)
	jne	.L3353
	movl	$72, %eax
	movw	%ax, 4568(%rbx)
.L3353:
	movabsq	$-4294967295, %rax
	movq	%rax, 4572(%rbx)
	jmp	.L3352
	.p2align 4,,10
	.p2align 3
.L3328:
	movq	%r9, %rsi
	movq	%r15, %rdi
	movq	%r9, -320(%rbp)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	-292(%rbp), %rsi
	movq	%r15, %rdi
	movl	$0, -292(%rbp)
	call	_ZN6icu_676Locale16addLikelySubtagsER10UErrorCode@PLT
	movl	-292(%rbp), %ecx
	movq	-320(%rbp), %r9
	testl	%ecx, %ecx
	jle	.L3394
.L3330:
	movq	-312(%rbp), %rcx
	leaq	.LC36(%rip), %rax
	cmpb	$0, (%rcx)
	cmovne	%rcx, %rax
	cmpb	$0, (%r14)
	movq	%rax, -312(%rbp)
	leaq	.LC37(%rip), %rax
	cmove	%rax, %r14
	jmp	.L3329
	.p2align 4,,10
	.p2align 3
.L3393:
	movq	%rax, %rdi
	call	_ZNK6icu_676Region13getRegionCodeEv@PLT
	movq	-312(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN6icu_67L32getAllowedHourFormatsLangCountryEPKcS1_R10UErrorCode
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3346
	cmpw	$0, 4568(%rbx)
	jne	.L3347
	.p2align 4,,10
	.p2align 3
.L3355:
	movl	(%r12), %eax
	cmpl	$2, %eax
	je	.L3348
	cmpl	$3, %eax
	je	.L3349
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andl	$32, %eax
	addl	$72, %eax
	movw	%ax, 4568(%rbx)
.L3347:
	movl	4(%r12), %eax
	movl	%eax, 4572(%rbx)
	cmpl	$-1, %eax
	je	.L3352
	movl	8(%r12), %eax
	movl	%eax, 4576(%rbx)
	cmpl	$-1, %eax
	je	.L3352
	movl	12(%r12), %eax
	movl	%eax, 4580(%rbx)
	cmpl	$-1, %eax
	je	.L3352
	movl	16(%r12), %eax
	movl	%eax, 4584(%rbx)
	cmpl	$-1, %eax
	je	.L3352
	movl	20(%r12), %eax
	movl	%eax, 4588(%rbx)
	cmpl	$-1, %eax
	je	.L3352
	movl	24(%r12), %eax
	movl	%eax, 4592(%rbx)
	cmpl	$-1, %eax
	je	.L3352
	movl	28(%r12), %eax
	movl	%eax, 4596(%rbx)
.L3352:
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3395
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3394:
	.cfi_restore_state
	leaq	-280(%rbp), %rax
	leaq	-262(%rbp), %r14
	movq	%rax, -312(%rbp)
	jmp	.L3330
	.p2align 4,,10
	.p2align 3
.L3392:
	cmpl	$3420776, (%rdx)
	je	.L3396
	cmpl	$3355240, -64(%rbp)
	je	.L3397
	cmpl	$3289448, -64(%rbp)
	je	.L3398
	cmpl	$3223912, -64(%rbp)
	jne	.L3332
	movl	$75, %edi
	movw	%di, 4568(%rbx)
	jmp	.L3336
	.p2align 4,,10
	.p2align 3
.L3396:
	movl	$107, %r10d
	movw	%r10w, 4568(%rbx)
.L3336:
	testq	%r12, %r12
	jne	.L3347
	jmp	.L3354
	.p2align 4,,10
	.p2align 3
.L3349:
	movl	$107, %edx
	movw	%dx, 4568(%rbx)
	jmp	.L3347
	.p2align 4,,10
	.p2align 3
.L3348:
	movl	$75, %ecx
	movw	%cx, 4568(%rbx)
	jmp	.L3347
	.p2align 4,,10
	.p2align 3
.L3397:
	movl	$72, %r9d
	movw	%r9w, 4568(%rbx)
	jmp	.L3336
.L3398:
	movl	$104, %r8d
	movw	%r8w, 4568(%rbx)
	jmp	.L3336
.L3395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5273:
	.size	_ZN6icu_6724DateTimePatternGenerator21getAllowedHourFormatsERKNS_6LocaleER10UErrorCode.part.0, .-_ZN6icu_6724DateTimePatternGenerator21getAllowedHourFormatsERKNS_6LocaleER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator21getAllowedHourFormatsERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator21getAllowedHourFormatsERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator21getAllowedHourFormatsERKNS_6LocaleER10UErrorCode:
.LFB3586:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L3401
	ret
	.p2align 4,,10
	.p2align 3
.L3401:
	jmp	_ZN6icu_6724DateTimePatternGenerator21getAllowedHourFormatsERKNS_6LocaleER10UErrorCode.part.0
	.cfi_endproc
.LFE3586:
	.size	_ZN6icu_6724DateTimePatternGenerator21getAllowedHourFormatsERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator21getAllowedHourFormatsERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator17setDecimalSymbolsERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator17setDecimalSymbolsERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator17setDecimalSymbolsERKNS_6LocaleER10UErrorCode:
.LFB3643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-2864(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$2896, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L3406
.L3403:
	movq	%r13, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3407
	addq	$2896, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3406:
	.cfi_restore_state
	leaq	-2928(%rbp), %r14
	leaq	-2856(%rbp), %rsi
	addq	$4424, %r12
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	jmp	.L3403
.L3407:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3643:
	.size	_ZN6icu_6724DateTimePatternGenerator17setDecimalSymbolsERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator17setDecimalSymbolsERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator8initDataERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator8initDataERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator8initDataERKNS_6LocaleER10UErrorCode:
.LFB3574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movups	%xmm0, 4488(%rdi)
	call	_ZN6icu_6724DateTimePatternGenerator17addCanonicalItemsER10UErrorCode
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator14addICUPatternsERKNS_6LocaleER10UErrorCode
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator11addCLDRDataERKNS_6LocaleER10UErrorCode
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator23setDateTimeFromCalendarERKNS_6LocaleER10UErrorCode
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator17setDecimalSymbolsERKNS_6LocaleER10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L3420
.L3412:
	movl	%eax, 4600(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3420:
	.cfi_restore_state
	movl	_ZN6icu_6712_GLOBAL__N_18initOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L3421
.L3410:
	movl	4+_ZN6icu_6712_GLOBAL__N_18initOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L3411
	movl	%eax, (%rbx)
	jmp	.L3412
	.p2align 4,,10
	.p2align 3
.L3421:
	leaq	_ZN6icu_6712_GLOBAL__N_18initOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L3410
	movq	%rbx, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator26loadAllowedHourFormatsDataER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_6712_GLOBAL__N_18initOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_6712_GLOBAL__N_18initOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L3411:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L3412
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator21getAllowedHourFormatsERKNS_6LocaleER10UErrorCode.part.0
	movl	(%rbx), %eax
	jmp	.L3412
	.cfi_endproc
.LFE3574:
	.size	_ZN6icu_6724DateTimePatternGenerator8initDataERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator8initDataERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGeneratorC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGeneratorC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGeneratorC2ERKNS_6LocaleER10UErrorCode:
.LFB3558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724DateTimePatternGeneratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$8, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	movq	%rax, -8(%rdi)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movl	$2, %eax
	movl	$2, %edi
	movl	$2, %r8d
	movl	$2, %r9d
	movl	$2, %r10d
	movw	%ax, 592(%r12)
	movl	$2, %eax
	movw	%ax, 656(%r12)
	movl	$2, %eax
	movl	$2, %r11d
	movl	$2, %edx
	movw	%ax, 720(%r12)
	movl	$2, %eax
	movl	$2, %ecx
	movl	$2, %esi
	movw	%ax, 784(%r12)
	movl	$2, %eax
	pxor	%xmm0, %xmm0
	movw	%di, 272(%r12)
	movl	$2, %edi
	movw	%r8w, 336(%r12)
	movl	$2, %r8d
	movw	%r9w, 400(%r12)
	movl	$2, %r9d
	movw	%ax, 848(%r12)
	movl	$2, %eax
	movw	%r10w, 464(%r12)
	movl	$2, %r10d
	movw	%r11w, 528(%r12)
	movl	$2, %r11d
	movw	%dx, 912(%r12)
	movl	$2, %edx
	movw	%cx, 976(%r12)
	movl	$2, %ecx
	movw	%si, 1040(%r12)
	movl	$2, %esi
	movw	%di, 1104(%r12)
	movl	$2, %edi
	movw	%r8w, 1168(%r12)
	movl	$2, %r8d
	movw	%r9w, 1232(%r12)
	movl	$2, %r9d
	movq	%rbx, 264(%r12)
	movq	%rbx, 328(%r12)
	movq	%rbx, 392(%r12)
	movq	%rbx, 456(%r12)
	movq	%rbx, 520(%r12)
	movq	%rbx, 584(%r12)
	movq	%rbx, 648(%r12)
	movq	%rbx, 712(%r12)
	movq	%rbx, 776(%r12)
	movq	%rbx, 840(%r12)
	movq	%rbx, 904(%r12)
	movq	%rbx, 968(%r12)
	movq	%rbx, 1032(%r12)
	movq	%rbx, 1096(%r12)
	movq	%rbx, 1160(%r12)
	movq	%rbx, 1224(%r12)
	movq	%rbx, 1288(%r12)
	movw	%ax, 1424(%r12)
	movl	$2, %eax
	movw	%ax, 1488(%r12)
	movl	$2, %eax
	movw	%ax, 1552(%r12)
	movl	$2, %eax
	movw	%ax, 1616(%r12)
	movl	$2, %eax
	movw	%ax, 1680(%r12)
	movl	$2, %eax
	movw	%r10w, 1296(%r12)
	movl	$2, %r10d
	movw	%r11w, 1360(%r12)
	movl	$2, %r11d
	movw	%ax, 2256(%r12)
	movl	$2, %eax
	movw	%dx, 1744(%r12)
	movl	$2, %edx
	movw	%cx, 1808(%r12)
	movl	$2, %ecx
	movw	%si, 1872(%r12)
	movl	$2, %esi
	movw	%di, 1936(%r12)
	movl	$2, %edi
	movw	%r8w, 2000(%r12)
	movl	$2, %r8d
	movw	%r9w, 2064(%r12)
	movl	$2, %r9d
	movw	%r10w, 2128(%r12)
	movl	$2, %r10d
	movw	%r11w, 2192(%r12)
	movl	$2, %r11d
	movq	%rbx, 1352(%r12)
	movq	%rbx, 1416(%r12)
	movq	%rbx, 1480(%r12)
	movq	%rbx, 1544(%r12)
	movq	%rbx, 1608(%r12)
	movq	%rbx, 1672(%r12)
	movq	%rbx, 1736(%r12)
	movq	%rbx, 1800(%r12)
	movq	%rbx, 1864(%r12)
	movq	%rbx, 1928(%r12)
	movq	%rbx, 1992(%r12)
	movq	%rbx, 2056(%r12)
	movq	%rbx, 2120(%r12)
	movq	%rbx, 2184(%r12)
	movq	%rbx, 2248(%r12)
	movq	%rbx, 2312(%r12)
	movw	%ax, 2320(%r12)
	movl	$2, %eax
	movw	%ax, 2384(%r12)
	movl	$2, %eax
	movw	%ax, 2448(%r12)
	movl	$2, %eax
	movw	%ax, 2512(%r12)
	movl	$2, %eax
	movw	%ax, 3088(%r12)
	movl	$2, %eax
	movw	%ax, 3152(%r12)
	movl	$2, %eax
	movw	%ax, 3216(%r12)
	movl	$2, %eax
	movw	%ax, 3280(%r12)
	movl	$2, %eax
	movw	%ax, 3344(%r12)
	movl	$2, %eax
	movw	%dx, 2576(%r12)
	movl	$2, %edx
	movw	%cx, 2640(%r12)
	movl	$2, %ecx
	movw	%si, 2704(%r12)
	movl	$2, %esi
	movw	%di, 2768(%r12)
	movl	$2, %edi
	movw	%r8w, 2832(%r12)
	movl	$2, %r8d
	movw	%r9w, 2896(%r12)
	movl	$2, %r9d
	movw	%r10w, 2960(%r12)
	movl	$2, %r10d
	movw	%r11w, 3024(%r12)
	movl	$2, %r11d
	movq	%rbx, 2376(%r12)
	movq	%rbx, 2440(%r12)
	movq	%rbx, 2504(%r12)
	movq	%rbx, 2568(%r12)
	movq	%rbx, 2632(%r12)
	movq	%rbx, 2696(%r12)
	movq	%rbx, 2760(%r12)
	movq	%rbx, 2824(%r12)
	movq	%rbx, 2888(%r12)
	movq	%rbx, 2952(%r12)
	movq	%rbx, 3016(%r12)
	movq	%rbx, 3080(%r12)
	movq	%rbx, 3144(%r12)
	movq	%rbx, 3208(%r12)
	movq	%rbx, 3272(%r12)
	movq	%rbx, 3336(%r12)
	movq	%rbx, 3400(%r12)
	movw	%ax, 3920(%r12)
	movl	$2, %eax
	movw	%ax, 3984(%r12)
	movl	$2, %eax
	movw	%ax, 4048(%r12)
	movl	$2, %eax
	movw	%dx, 3408(%r12)
	movl	$2, %edx
	movw	%cx, 3472(%r12)
	movl	$2, %ecx
	movw	%si, 3536(%r12)
	movl	$2, %esi
	movw	%di, 3600(%r12)
	movl	$2, %edi
	movw	%r8w, 3664(%r12)
	movl	$2, %r8d
	movw	%r9w, 3728(%r12)
	xorl	%r9d, %r9d
	movw	%ax, 4112(%r12)
	movl	$2, %eax
	movq	%rbx, 3464(%r12)
	movq	%rbx, 3528(%r12)
	movq	%rbx, 3592(%r12)
	movq	%rbx, 3656(%r12)
	movq	%rbx, 3720(%r12)
	movq	%rbx, 3784(%r12)
	movw	%r10w, 3792(%r12)
	movq	%rbx, 3848(%r12)
	movw	%r11w, 3856(%r12)
	movq	%rbx, 3912(%r12)
	movq	%rbx, 3976(%r12)
	movq	%rbx, 4040(%r12)
	movq	%rbx, 4104(%r12)
	movq	%rbx, 4168(%r12)
	movw	%ax, 4176(%r12)
	movq	%rbx, 4232(%r12)
	movw	%dx, 4240(%r12)
	movq	%rbx, 4296(%r12)
	movw	%cx, 4304(%r12)
	movq	%rbx, 4360(%r12)
	movw	%si, 4368(%r12)
	movq	%rbx, 4424(%r12)
	movw	%di, 4432(%r12)
	movl	$3216, %edi
	movq	%rbx, 4504(%r12)
	movw	%r8w, 4512(%r12)
	movw	%r9w, 4568(%r12)
	movl	$0, 4600(%r12)
	movups	%xmm0, 4488(%r12)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L3423
	leaq	16+_ZTVN6icu_6712FormatParserE(%rip), %rsi
	leaq	8(%rax), %rdx
	movq	%rsi, (%rax)
	leaq	3208(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L3424:
	movl	$2, %esi
	movq	%rbx, (%rdx)
	addq	$64, %rdx
	movw	%si, -56(%rdx)
	cmpq	%rcx, %rdx
	jne	.L3424
	movq	$0, 3208(%rax)
.L3423:
	movq	%rax, 232(%r12)
	movl	$152, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L3425
	leaq	16+_ZTVN6icu_6711PtnSkeletonE(%rip), %rdx
	leaq	16+_ZTVN6icu_6715DateTimeMatcherE(%rip), %rsi
	movb	$0, 144(%rax)
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
.L3425:
	movq	%rax, 240(%r12)
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L3426
	leaq	16+_ZTVN6icu_6712DistanceInfoE(%rip), %rsi
	movq	%rsi, (%rax)
.L3426:
	movq	%rax, 248(%r12)
	movl	$432, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3427
	leaq	16+_ZTVN6icu_6710PatternMapE(%rip), %rax
	leaq	16(%rdx), %rdi
	movq	$0, 8(%rdx)
	movq	%rax, (%rdx)
	andq	$-8, %rdi
	movl	%edx, %eax
	movq	$0, 416(%rdx)
	subl	%edi, %eax
	leal	424(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movb	$1, 424(%rdx)
.L3427:
	cmpq	$0, 232(%r12)
	movq	%rdx, 256(%r12)
	je	.L3428
	cmpq	$0, 240(%r12)
	je	.L3428
	cmpq	$0, 248(%r12)
	je	.L3428
	testq	%rdx, %rdx
	je	.L3428
	popq	%rbx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6724DateTimePatternGenerator8initDataERKNS_6LocaleER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L3428:
	.cfi_restore_state
	popq	%rbx
	movl	$7, 0(%r13)
	movl	$7, 4600(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3558:
	.size	_ZN6icu_6724DateTimePatternGeneratorC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6724DateTimePatternGeneratorC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6724DateTimePatternGeneratorC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6724DateTimePatternGeneratorC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6724DateTimePatternGeneratorC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator14createInstanceER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator14createInstanceER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator14createInstanceER10UErrorCode:
.LFB3551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L3448
	movl	$4608, %edi
	movq	%rax, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3449
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6724DateTimePatternGeneratorC1ERKNS_6LocaleER10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L3447
	movq	(%r12), %rdx
	leaq	_ZN6icu_6724DateTimePatternGeneratorD0Ev(%rip), %rax
	cmpq	%rax, 8(%rdx)
	jne	.L3458
	movq	%r12, %rdi
	call	_ZN6icu_6724DateTimePatternGeneratorD1Ev
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L3447:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3449:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L3448
	movl	$7, (%rbx)
.L3448:
	addq	$8, %rsp
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3458:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6724DateTimePatternGeneratorD0Ev
	jmp	.L3447
	.cfi_endproc
.LFE3551:
	.size	_ZN6icu_6724DateTimePatternGenerator14createInstanceER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator14createInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724DateTimePatternGenerator14createInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6724DateTimePatternGenerator14createInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6724DateTimePatternGenerator14createInstanceERKNS_6LocaleER10UErrorCode:
.LFB3552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L3460
	movq	%rdi, %r13
	movl	$4608, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3461
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6724DateTimePatternGeneratorC1ERKNS_6LocaleER10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L3459
	movq	(%r12), %rdx
	leaq	_ZN6icu_6724DateTimePatternGeneratorD0Ev(%rip), %rax
	cmpq	%rax, 8(%rdx)
	jne	.L3470
	movq	%r12, %rdi
	call	_ZN6icu_6724DateTimePatternGeneratorD1Ev
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L3459:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3461:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L3460
	movl	$7, (%rbx)
.L3460:
	addq	$8, %rsp
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3470:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6724DateTimePatternGeneratorD0Ev
	jmp	.L3459
	.cfi_endproc
.LFE3552:
	.size	_ZN6icu_6724DateTimePatternGenerator14createInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6724DateTimePatternGenerator14createInstanceERKNS_6LocaleER10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6724DateTimePatternGeneratorE
	.section	.rodata._ZTSN6icu_6724DateTimePatternGeneratorE,"aG",@progbits,_ZTSN6icu_6724DateTimePatternGeneratorE,comdat
	.align 32
	.type	_ZTSN6icu_6724DateTimePatternGeneratorE, @object
	.size	_ZTSN6icu_6724DateTimePatternGeneratorE, 36
_ZTSN6icu_6724DateTimePatternGeneratorE:
	.string	"N6icu_6724DateTimePatternGeneratorE"
	.weak	_ZTIN6icu_6724DateTimePatternGeneratorE
	.section	.data.rel.ro._ZTIN6icu_6724DateTimePatternGeneratorE,"awG",@progbits,_ZTIN6icu_6724DateTimePatternGeneratorE,comdat
	.align 8
	.type	_ZTIN6icu_6724DateTimePatternGeneratorE, @object
	.size	_ZTIN6icu_6724DateTimePatternGeneratorE, 24
_ZTIN6icu_6724DateTimePatternGeneratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6724DateTimePatternGeneratorE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6711PtnSkeletonE
	.section	.rodata._ZTSN6icu_6711PtnSkeletonE,"aG",@progbits,_ZTSN6icu_6711PtnSkeletonE,comdat
	.align 16
	.type	_ZTSN6icu_6711PtnSkeletonE, @object
	.size	_ZTSN6icu_6711PtnSkeletonE, 23
_ZTSN6icu_6711PtnSkeletonE:
	.string	"N6icu_6711PtnSkeletonE"
	.weak	_ZTIN6icu_6711PtnSkeletonE
	.section	.data.rel.ro._ZTIN6icu_6711PtnSkeletonE,"awG",@progbits,_ZTIN6icu_6711PtnSkeletonE,comdat
	.align 8
	.type	_ZTIN6icu_6711PtnSkeletonE, @object
	.size	_ZTIN6icu_6711PtnSkeletonE, 24
_ZTIN6icu_6711PtnSkeletonE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6711PtnSkeletonE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_677PtnElemE
	.section	.rodata._ZTSN6icu_677PtnElemE,"aG",@progbits,_ZTSN6icu_677PtnElemE,comdat
	.align 16
	.type	_ZTSN6icu_677PtnElemE, @object
	.size	_ZTSN6icu_677PtnElemE, 18
_ZTSN6icu_677PtnElemE:
	.string	"N6icu_677PtnElemE"
	.weak	_ZTIN6icu_677PtnElemE
	.section	.data.rel.ro._ZTIN6icu_677PtnElemE,"awG",@progbits,_ZTIN6icu_677PtnElemE,comdat
	.align 8
	.type	_ZTIN6icu_677PtnElemE, @object
	.size	_ZTIN6icu_677PtnElemE, 24
_ZTIN6icu_677PtnElemE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_677PtnElemE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6712FormatParserE
	.section	.rodata._ZTSN6icu_6712FormatParserE,"aG",@progbits,_ZTSN6icu_6712FormatParserE,comdat
	.align 16
	.type	_ZTSN6icu_6712FormatParserE, @object
	.size	_ZTSN6icu_6712FormatParserE, 24
_ZTSN6icu_6712FormatParserE:
	.string	"N6icu_6712FormatParserE"
	.weak	_ZTIN6icu_6712FormatParserE
	.section	.data.rel.ro._ZTIN6icu_6712FormatParserE,"awG",@progbits,_ZTIN6icu_6712FormatParserE,comdat
	.align 8
	.type	_ZTIN6icu_6712FormatParserE, @object
	.size	_ZTIN6icu_6712FormatParserE, 24
_ZTIN6icu_6712FormatParserE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712FormatParserE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6712DistanceInfoE
	.section	.rodata._ZTSN6icu_6712DistanceInfoE,"aG",@progbits,_ZTSN6icu_6712DistanceInfoE,comdat
	.align 16
	.type	_ZTSN6icu_6712DistanceInfoE, @object
	.size	_ZTSN6icu_6712DistanceInfoE, 24
_ZTSN6icu_6712DistanceInfoE:
	.string	"N6icu_6712DistanceInfoE"
	.weak	_ZTIN6icu_6712DistanceInfoE
	.section	.data.rel.ro._ZTIN6icu_6712DistanceInfoE,"awG",@progbits,_ZTIN6icu_6712DistanceInfoE,comdat
	.align 8
	.type	_ZTIN6icu_6712DistanceInfoE, @object
	.size	_ZTIN6icu_6712DistanceInfoE, 24
_ZTIN6icu_6712DistanceInfoE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712DistanceInfoE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6715DateTimeMatcherE
	.section	.rodata._ZTSN6icu_6715DateTimeMatcherE,"aG",@progbits,_ZTSN6icu_6715DateTimeMatcherE,comdat
	.align 16
	.type	_ZTSN6icu_6715DateTimeMatcherE, @object
	.size	_ZTSN6icu_6715DateTimeMatcherE, 27
_ZTSN6icu_6715DateTimeMatcherE:
	.string	"N6icu_6715DateTimeMatcherE"
	.weak	_ZTIN6icu_6715DateTimeMatcherE
	.section	.data.rel.ro._ZTIN6icu_6715DateTimeMatcherE,"awG",@progbits,_ZTIN6icu_6715DateTimeMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_6715DateTimeMatcherE, @object
	.size	_ZTIN6icu_6715DateTimeMatcherE, 24
_ZTIN6icu_6715DateTimeMatcherE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715DateTimeMatcherE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6710PatternMapE
	.section	.rodata._ZTSN6icu_6710PatternMapE,"aG",@progbits,_ZTSN6icu_6710PatternMapE,comdat
	.align 16
	.type	_ZTSN6icu_6710PatternMapE, @object
	.size	_ZTSN6icu_6710PatternMapE, 22
_ZTSN6icu_6710PatternMapE:
	.string	"N6icu_6710PatternMapE"
	.weak	_ZTIN6icu_6710PatternMapE
	.section	.data.rel.ro._ZTIN6icu_6710PatternMapE,"awG",@progbits,_ZTIN6icu_6710PatternMapE,comdat
	.align 8
	.type	_ZTIN6icu_6710PatternMapE, @object
	.size	_ZTIN6icu_6710PatternMapE, 24
_ZTIN6icu_6710PatternMapE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6710PatternMapE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6718PatternMapIteratorE
	.section	.rodata._ZTSN6icu_6718PatternMapIteratorE,"aG",@progbits,_ZTSN6icu_6718PatternMapIteratorE,comdat
	.align 16
	.type	_ZTSN6icu_6718PatternMapIteratorE, @object
	.size	_ZTSN6icu_6718PatternMapIteratorE, 30
_ZTSN6icu_6718PatternMapIteratorE:
	.string	"N6icu_6718PatternMapIteratorE"
	.weak	_ZTIN6icu_6718PatternMapIteratorE
	.section	.data.rel.ro._ZTIN6icu_6718PatternMapIteratorE,"awG",@progbits,_ZTIN6icu_6718PatternMapIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6718PatternMapIteratorE, @object
	.size	_ZTIN6icu_6718PatternMapIteratorE, 24
_ZTIN6icu_6718PatternMapIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718PatternMapIteratorE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6721DTSkeletonEnumerationE
	.section	.rodata._ZTSN6icu_6721DTSkeletonEnumerationE,"aG",@progbits,_ZTSN6icu_6721DTSkeletonEnumerationE,comdat
	.align 32
	.type	_ZTSN6icu_6721DTSkeletonEnumerationE, @object
	.size	_ZTSN6icu_6721DTSkeletonEnumerationE, 33
_ZTSN6icu_6721DTSkeletonEnumerationE:
	.string	"N6icu_6721DTSkeletonEnumerationE"
	.weak	_ZTIN6icu_6721DTSkeletonEnumerationE
	.section	.data.rel.ro._ZTIN6icu_6721DTSkeletonEnumerationE,"awG",@progbits,_ZTIN6icu_6721DTSkeletonEnumerationE,comdat
	.align 8
	.type	_ZTIN6icu_6721DTSkeletonEnumerationE, @object
	.size	_ZTIN6icu_6721DTSkeletonEnumerationE, 24
_ZTIN6icu_6721DTSkeletonEnumerationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6721DTSkeletonEnumerationE
	.quad	_ZTIN6icu_6717StringEnumerationE
	.weak	_ZTSN6icu_6722DTRedundantEnumerationE
	.section	.rodata._ZTSN6icu_6722DTRedundantEnumerationE,"aG",@progbits,_ZTSN6icu_6722DTRedundantEnumerationE,comdat
	.align 32
	.type	_ZTSN6icu_6722DTRedundantEnumerationE, @object
	.size	_ZTSN6icu_6722DTRedundantEnumerationE, 34
_ZTSN6icu_6722DTRedundantEnumerationE:
	.string	"N6icu_6722DTRedundantEnumerationE"
	.weak	_ZTIN6icu_6722DTRedundantEnumerationE
	.section	.data.rel.ro._ZTIN6icu_6722DTRedundantEnumerationE,"awG",@progbits,_ZTIN6icu_6722DTRedundantEnumerationE,comdat
	.align 8
	.type	_ZTIN6icu_6722DTRedundantEnumerationE, @object
	.size	_ZTIN6icu_6722DTRedundantEnumerationE, 24
_ZTIN6icu_6722DTRedundantEnumerationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722DTRedundantEnumerationE
	.quad	_ZTIN6icu_6717StringEnumerationE
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE, 24
_ZTIN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE, 49
_ZTSN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE:
	.string	"*N6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE"
	.weak	_ZTSN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE
	.section	.rodata._ZTSN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE,"aG",@progbits,_ZTSN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE,comdat
	.align 32
	.type	_ZTSN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE, @object
	.size	_ZTSN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE, 59
_ZTSN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE:
	.string	"N6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE"
	.weak	_ZTIN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE
	.section	.data.rel.ro._ZTIN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE,"awG",@progbits,_ZTIN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE,comdat
	.align 8
	.type	_ZTIN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE, @object
	.size	_ZTIN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE, 24
_ZTIN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.weak	_ZTSN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE
	.section	.rodata._ZTSN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE,"aG",@progbits,_ZTSN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE,comdat
	.align 32
	.type	_ZTSN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE, @object
	.size	_ZTSN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE, 57
_ZTSN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE:
	.string	"N6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE"
	.weak	_ZTIN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE
	.section	.data.rel.ro._ZTIN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE,"awG",@progbits,_ZTIN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE,comdat
	.align 8
	.type	_ZTIN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE, @object
	.size	_ZTIN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE, 24
_ZTIN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.weak	_ZTSN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE
	.section	.rodata._ZTSN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE,"aG",@progbits,_ZTSN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE,comdat
	.align 32
	.type	_ZTSN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE, @object
	.size	_ZTSN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE, 58
_ZTSN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE:
	.string	"N6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE"
	.weak	_ZTIN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE
	.section	.data.rel.ro._ZTIN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE,"awG",@progbits,_ZTIN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE,comdat
	.align 8
	.type	_ZTIN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE, @object
	.size	_ZTIN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE, 24
_ZTIN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.weak	_ZTVN6icu_6724DateTimePatternGeneratorE
	.section	.data.rel.ro.local._ZTVN6icu_6724DateTimePatternGeneratorE,"awG",@progbits,_ZTVN6icu_6724DateTimePatternGeneratorE,comdat
	.align 8
	.type	_ZTVN6icu_6724DateTimePatternGeneratorE, @object
	.size	_ZTVN6icu_6724DateTimePatternGeneratorE, 40
_ZTVN6icu_6724DateTimePatternGeneratorE:
	.quad	0
	.quad	_ZTIN6icu_6724DateTimePatternGeneratorE
	.quad	_ZN6icu_6724DateTimePatternGeneratorD1Ev
	.quad	_ZN6icu_6724DateTimePatternGeneratorD0Ev
	.quad	_ZNK6icu_6724DateTimePatternGenerator17getDynamicClassIDEv
	.section	.data.rel.ro
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE, 48
_ZTVN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkE
	.quad	_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6712_GLOBAL__N_122AllowedHourFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.weak	_ZTVN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE
	.section	.data.rel.ro._ZTVN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE,"awG",@progbits,_ZTVN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE,comdat
	.align 8
	.type	_ZTVN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE, @object
	.size	_ZTVN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE, 48
_ZTVN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE:
	.quad	0
	.quad	_ZTIN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkE
	.quad	_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD1Ev
	.quad	_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6724DateTimePatternGenerator21AppendItemFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.weak	_ZTVN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE
	.section	.data.rel.ro._ZTVN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE,"awG",@progbits,_ZTVN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE,comdat
	.align 8
	.type	_ZTVN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE, @object
	.size	_ZTVN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE, 48
_ZTVN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE:
	.quad	0
	.quad	_ZTIN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkE
	.quad	_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD1Ev
	.quad	_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6724DateTimePatternGenerator19AppendItemNamesSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.weak	_ZTVN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE
	.section	.data.rel.ro._ZTVN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE,"awG",@progbits,_ZTVN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE,comdat
	.align 8
	.type	_ZTVN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE, @object
	.size	_ZTVN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE, 48
_ZTVN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE:
	.quad	0
	.quad	_ZTIN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkE
	.quad	_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD1Ev
	.quad	_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6724DateTimePatternGenerator20AvailableFormatsSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.weak	_ZTVN6icu_6710PatternMapE
	.section	.data.rel.ro.local._ZTVN6icu_6710PatternMapE,"awG",@progbits,_ZTVN6icu_6710PatternMapE,comdat
	.align 8
	.type	_ZTVN6icu_6710PatternMapE, @object
	.size	_ZTVN6icu_6710PatternMapE, 32
_ZTVN6icu_6710PatternMapE:
	.quad	0
	.quad	_ZTIN6icu_6710PatternMapE
	.quad	_ZN6icu_6710PatternMapD1Ev
	.quad	_ZN6icu_6710PatternMapD0Ev
	.weak	_ZTVN6icu_6715DateTimeMatcherE
	.section	.data.rel.ro.local._ZTVN6icu_6715DateTimeMatcherE,"awG",@progbits,_ZTVN6icu_6715DateTimeMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_6715DateTimeMatcherE, @object
	.size	_ZTVN6icu_6715DateTimeMatcherE, 32
_ZTVN6icu_6715DateTimeMatcherE:
	.quad	0
	.quad	_ZTIN6icu_6715DateTimeMatcherE
	.quad	_ZN6icu_6715DateTimeMatcherD1Ev
	.quad	_ZN6icu_6715DateTimeMatcherD0Ev
	.weak	_ZTVN6icu_6712FormatParserE
	.section	.data.rel.ro.local._ZTVN6icu_6712FormatParserE,"awG",@progbits,_ZTVN6icu_6712FormatParserE,comdat
	.align 8
	.type	_ZTVN6icu_6712FormatParserE, @object
	.size	_ZTVN6icu_6712FormatParserE, 40
_ZTVN6icu_6712FormatParserE:
	.quad	0
	.quad	_ZTIN6icu_6712FormatParserE
	.quad	_ZN6icu_6712FormatParserD1Ev
	.quad	_ZN6icu_6712FormatParserD0Ev
	.quad	_ZN6icu_6712FormatParser9setTokensERKNS_13UnicodeStringEiPi
	.weak	_ZTVN6icu_6712DistanceInfoE
	.section	.data.rel.ro.local._ZTVN6icu_6712DistanceInfoE,"awG",@progbits,_ZTVN6icu_6712DistanceInfoE,comdat
	.align 8
	.type	_ZTVN6icu_6712DistanceInfoE, @object
	.size	_ZTVN6icu_6712DistanceInfoE, 32
_ZTVN6icu_6712DistanceInfoE:
	.quad	0
	.quad	_ZTIN6icu_6712DistanceInfoE
	.quad	_ZN6icu_6712DistanceInfoD1Ev
	.quad	_ZN6icu_6712DistanceInfoD0Ev
	.weak	_ZTVN6icu_6718PatternMapIteratorE
	.section	.data.rel.ro.local._ZTVN6icu_6718PatternMapIteratorE,"awG",@progbits,_ZTVN6icu_6718PatternMapIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6718PatternMapIteratorE, @object
	.size	_ZTVN6icu_6718PatternMapIteratorE, 32
_ZTVN6icu_6718PatternMapIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6718PatternMapIteratorE
	.quad	_ZN6icu_6718PatternMapIteratorD1Ev
	.quad	_ZN6icu_6718PatternMapIteratorD0Ev
	.weak	_ZTVN6icu_6711PtnSkeletonE
	.section	.data.rel.ro.local._ZTVN6icu_6711PtnSkeletonE,"awG",@progbits,_ZTVN6icu_6711PtnSkeletonE,comdat
	.align 8
	.type	_ZTVN6icu_6711PtnSkeletonE, @object
	.size	_ZTVN6icu_6711PtnSkeletonE, 32
_ZTVN6icu_6711PtnSkeletonE:
	.quad	0
	.quad	_ZTIN6icu_6711PtnSkeletonE
	.quad	_ZN6icu_6711PtnSkeletonD1Ev
	.quad	_ZN6icu_6711PtnSkeletonD0Ev
	.weak	_ZTVN6icu_677PtnElemE
	.section	.data.rel.ro.local._ZTVN6icu_677PtnElemE,"awG",@progbits,_ZTVN6icu_677PtnElemE,comdat
	.align 8
	.type	_ZTVN6icu_677PtnElemE, @object
	.size	_ZTVN6icu_677PtnElemE, 32
_ZTVN6icu_677PtnElemE:
	.quad	0
	.quad	_ZTIN6icu_677PtnElemE
	.quad	_ZN6icu_677PtnElemD1Ev
	.quad	_ZN6icu_677PtnElemD0Ev
	.weak	_ZTVN6icu_6721DTSkeletonEnumerationE
	.section	.data.rel.ro._ZTVN6icu_6721DTSkeletonEnumerationE,"awG",@progbits,_ZTVN6icu_6721DTSkeletonEnumerationE,comdat
	.align 8
	.type	_ZTVN6icu_6721DTSkeletonEnumerationE, @object
	.size	_ZTVN6icu_6721DTSkeletonEnumerationE, 104
_ZTVN6icu_6721DTSkeletonEnumerationE:
	.quad	0
	.quad	_ZTIN6icu_6721DTSkeletonEnumerationE
	.quad	_ZN6icu_6721DTSkeletonEnumerationD1Ev
	.quad	_ZN6icu_6721DTSkeletonEnumerationD0Ev
	.quad	_ZNK6icu_6721DTSkeletonEnumeration17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringEnumeration5cloneEv
	.quad	_ZNK6icu_6721DTSkeletonEnumeration5countER10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration4nextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode
	.quad	_ZN6icu_6721DTSkeletonEnumeration5snextER10UErrorCode
	.quad	_ZN6icu_6721DTSkeletonEnumeration5resetER10UErrorCode
	.quad	_ZNK6icu_6717StringEnumerationeqERKS0_
	.quad	_ZNK6icu_6717StringEnumerationneERKS0_
	.weak	_ZTVN6icu_6722DTRedundantEnumerationE
	.section	.data.rel.ro._ZTVN6icu_6722DTRedundantEnumerationE,"awG",@progbits,_ZTVN6icu_6722DTRedundantEnumerationE,comdat
	.align 8
	.type	_ZTVN6icu_6722DTRedundantEnumerationE, @object
	.size	_ZTVN6icu_6722DTRedundantEnumerationE, 104
_ZTVN6icu_6722DTRedundantEnumerationE:
	.quad	0
	.quad	_ZTIN6icu_6722DTRedundantEnumerationE
	.quad	_ZN6icu_6722DTRedundantEnumerationD1Ev
	.quad	_ZN6icu_6722DTRedundantEnumerationD0Ev
	.quad	_ZNK6icu_6722DTRedundantEnumeration17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringEnumeration5cloneEv
	.quad	_ZNK6icu_6722DTRedundantEnumeration5countER10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration4nextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode
	.quad	_ZN6icu_6722DTRedundantEnumeration5snextER10UErrorCode
	.quad	_ZN6icu_6722DTRedundantEnumeration5resetER10UErrorCode
	.quad	_ZNK6icu_6717StringEnumerationeqERKS0_
	.quad	_ZNK6icu_6717StringEnumerationneERKS0_
	.local	_ZN6icu_6712_GLOBAL__N_129localeToAllowedHourFormatsMapE
	.comm	_ZN6icu_6712_GLOBAL__N_129localeToAllowedHourFormatsMapE,8,8
	.local	_ZN6icu_6712_GLOBAL__N_18initOnceE
	.comm	_ZN6icu_6712_GLOBAL__N_18initOnceE,8,8
	.local	_ZZN6icu_6722DTRedundantEnumeration16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6722DTRedundantEnumeration16getStaticClassIDEvE7classID,1,1
	.local	_ZZN6icu_6721DTSkeletonEnumeration16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6721DTSkeletonEnumeration16getStaticClassIDEvE7classID,1,1
	.local	_ZZN6icu_6724DateTimePatternGenerator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6724DateTimePatternGenerator16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.align 16
	.type	_ZN6icu_67L30DT_DateTimeAvailableFormatsTagE, @object
	.size	_ZN6icu_67L30DT_DateTimeAvailableFormatsTagE, 17
_ZN6icu_67L30DT_DateTimeAvailableFormatsTagE:
	.string	"availableFormats"
	.type	_ZN6icu_67L20DT_DateTimeFieldsTagE, @object
	.size	_ZN6icu_67L20DT_DateTimeFieldsTagE, 7
_ZN6icu_67L20DT_DateTimeFieldsTagE:
	.string	"fields"
	.align 8
	.type	_ZN6icu_67L25DT_DateTimeAppendItemsTagE, @object
	.size	_ZN6icu_67L25DT_DateTimeAppendItemsTagE, 12
_ZN6icu_67L25DT_DateTimeAppendItemsTagE:
	.string	"appendItems"
	.align 8
	.type	_ZN6icu_67L23DT_DateTimeGregorianTagE, @object
	.size	_ZN6icu_67L23DT_DateTimeGregorianTagE, 10
_ZN6icu_67L23DT_DateTimeGregorianTagE:
	.string	"gregorian"
	.align 8
	.type	_ZN6icu_67L22DT_DateTimeCalendarTagE, @object
	.size	_ZN6icu_67L22DT_DateTimeCalendarTagE, 9
_ZN6icu_67L22DT_DateTimeCalendarTagE:
	.string	"calendar"
	.align 16
	.type	_ZN6icu_67L22DT_DateTimePatternsTagE, @object
	.size	_ZN6icu_67L22DT_DateTimePatternsTagE, 17
_ZN6icu_67L22DT_DateTimePatternsTagE:
	.string	"DateTimePatterns"
	.align 16
	.type	_ZN6icu_67L17UDATPG_ItemFormatE, @object
	.size	_ZN6icu_67L17UDATPG_ItemFormatE, 30
_ZN6icu_67L17UDATPG_ItemFormatE:
	.value	123
	.value	48
	.value	125
	.value	32
	.value	9500
	.value	123
	.value	50
	.value	125
	.value	58
	.value	32
	.value	123
	.value	49
	.value	125
	.value	9508
	.value	0
	.align 32
	.type	_ZN6icu_67L7dtTypesE, @object
	.size	_ZN6icu_67L7dtTypesE, 1392
_ZN6icu_67L7dtTypesE:
	.value	71
	.zero	2
	.long	0
	.value	-259
	.value	1
	.value	3
	.zero	2
	.value	71
	.zero	2
	.long	0
	.value	-260
	.value	4
	.value	0
	.zero	2
	.value	71
	.zero	2
	.long	0
	.value	-257
	.value	5
	.value	0
	.zero	2
	.value	121
	.zero	2
	.long	1
	.value	256
	.value	1
	.value	20
	.zero	2
	.value	89
	.zero	2
	.long	1
	.value	272
	.value	1
	.value	20
	.zero	2
	.value	117
	.zero	2
	.long	1
	.value	288
	.value	1
	.value	20
	.zero	2
	.value	114
	.zero	2
	.long	1
	.value	304
	.value	1
	.value	20
	.zero	2
	.value	85
	.zero	2
	.long	1
	.value	-259
	.value	1
	.value	3
	.zero	2
	.value	85
	.zero	2
	.long	1
	.value	-260
	.value	4
	.value	0
	.zero	2
	.value	85
	.zero	2
	.long	1
	.value	-257
	.value	5
	.value	0
	.zero	2
	.value	81
	.zero	2
	.long	2
	.value	256
	.value	1
	.value	2
	.zero	2
	.value	81
	.zero	2
	.long	2
	.value	-259
	.value	3
	.value	0
	.zero	2
	.value	81
	.zero	2
	.long	2
	.value	-260
	.value	4
	.value	0
	.zero	2
	.value	81
	.zero	2
	.long	2
	.value	-257
	.value	5
	.value	0
	.zero	2
	.value	113
	.zero	2
	.long	2
	.value	272
	.value	1
	.value	2
	.zero	2
	.value	113
	.zero	2
	.long	2
	.value	-275
	.value	3
	.value	0
	.zero	2
	.value	113
	.zero	2
	.long	2
	.value	-276
	.value	4
	.value	0
	.zero	2
	.value	113
	.zero	2
	.long	2
	.value	-273
	.value	5
	.value	0
	.zero	2
	.value	77
	.zero	2
	.long	3
	.value	256
	.value	1
	.value	2
	.zero	2
	.value	77
	.zero	2
	.long	3
	.value	-259
	.value	3
	.value	0
	.zero	2
	.value	77
	.zero	2
	.long	3
	.value	-260
	.value	4
	.value	0
	.zero	2
	.value	77
	.zero	2
	.long	3
	.value	-257
	.value	5
	.value	0
	.zero	2
	.value	76
	.zero	2
	.long	3
	.value	272
	.value	1
	.value	2
	.zero	2
	.value	76
	.zero	2
	.long	3
	.value	-275
	.value	3
	.value	0
	.zero	2
	.value	76
	.zero	2
	.long	3
	.value	-276
	.value	4
	.value	0
	.zero	2
	.value	76
	.zero	2
	.long	3
	.value	-273
	.value	5
	.value	0
	.zero	2
	.value	108
	.zero	2
	.long	3
	.value	272
	.value	1
	.value	1
	.zero	2
	.value	119
	.zero	2
	.long	4
	.value	256
	.value	1
	.value	2
	.zero	2
	.value	87
	.zero	2
	.long	5
	.value	256
	.value	1
	.value	0
	.zero	2
	.value	69
	.zero	2
	.long	6
	.value	-259
	.value	1
	.value	3
	.zero	2
	.value	69
	.zero	2
	.long	6
	.value	-260
	.value	4
	.value	0
	.zero	2
	.value	69
	.zero	2
	.long	6
	.value	-257
	.value	5
	.value	0
	.zero	2
	.value	69
	.zero	2
	.long	6
	.value	-258
	.value	6
	.value	0
	.zero	2
	.value	99
	.zero	2
	.long	6
	.value	288
	.value	1
	.value	2
	.zero	2
	.value	99
	.zero	2
	.long	6
	.value	-291
	.value	3
	.value	0
	.zero	2
	.value	99
	.zero	2
	.long	6
	.value	-292
	.value	4
	.value	0
	.zero	2
	.value	99
	.zero	2
	.long	6
	.value	-289
	.value	5
	.value	0
	.zero	2
	.value	99
	.zero	2
	.long	6
	.value	-290
	.value	6
	.value	0
	.zero	2
	.value	101
	.zero	2
	.long	6
	.value	272
	.value	1
	.value	2
	.zero	2
	.value	101
	.zero	2
	.long	6
	.value	-275
	.value	3
	.value	0
	.zero	2
	.value	101
	.zero	2
	.long	6
	.value	-276
	.value	4
	.value	0
	.zero	2
	.value	101
	.zero	2
	.long	6
	.value	-273
	.value	5
	.value	0
	.zero	2
	.value	101
	.zero	2
	.long	6
	.value	-274
	.value	6
	.value	0
	.zero	2
	.value	100
	.zero	2
	.long	9
	.value	256
	.value	1
	.value	2
	.zero	2
	.value	103
	.zero	2
	.long	9
	.value	272
	.value	1
	.value	20
	.zero	2
	.value	68
	.zero	2
	.long	7
	.value	256
	.value	1
	.value	3
	.zero	2
	.value	70
	.zero	2
	.long	8
	.value	256
	.value	1
	.value	0
	.zero	2
	.value	97
	.zero	2
	.long	10
	.value	-259
	.value	1
	.value	3
	.zero	2
	.value	97
	.zero	2
	.long	10
	.value	-260
	.value	4
	.value	0
	.zero	2
	.value	97
	.zero	2
	.long	10
	.value	-257
	.value	5
	.value	0
	.zero	2
	.value	98
	.zero	2
	.long	10
	.value	-275
	.value	1
	.value	3
	.zero	2
	.value	98
	.zero	2
	.long	10
	.value	-276
	.value	4
	.value	0
	.zero	2
	.value	98
	.zero	2
	.long	10
	.value	-273
	.value	5
	.value	0
	.zero	2
	.value	66
	.zero	2
	.long	10
	.value	-307
	.value	1
	.value	3
	.zero	2
	.value	66
	.zero	2
	.long	10
	.value	-308
	.value	4
	.value	0
	.zero	2
	.value	66
	.zero	2
	.long	10
	.value	-305
	.value	5
	.value	0
	.zero	2
	.value	72
	.zero	2
	.long	11
	.value	416
	.value	1
	.value	2
	.zero	2
	.value	107
	.zero	2
	.long	11
	.value	432
	.value	1
	.value	2
	.zero	2
	.value	104
	.zero	2
	.long	11
	.value	256
	.value	1
	.value	2
	.zero	2
	.value	75
	.zero	2
	.long	11
	.value	272
	.value	1
	.value	2
	.zero	2
	.value	74
	.zero	2
	.long	11
	.value	336
	.value	1
	.value	2
	.zero	2
	.value	106
	.zero	2
	.long	11
	.value	352
	.value	1
	.value	6
	.zero	2
	.value	67
	.zero	2
	.long	11
	.value	368
	.value	1
	.value	6
	.zero	2
	.value	109
	.zero	2
	.long	12
	.value	256
	.value	1
	.value	2
	.zero	2
	.value	115
	.zero	2
	.long	13
	.value	256
	.value	1
	.value	2
	.zero	2
	.value	65
	.zero	2
	.long	13
	.value	272
	.value	1
	.value	1000
	.zero	2
	.value	83
	.zero	2
	.long	14
	.value	256
	.value	1
	.value	1000
	.zero	2
	.value	118
	.zero	2
	.long	15
	.value	-291
	.value	1
	.value	0
	.zero	2
	.value	118
	.zero	2
	.long	15
	.value	-292
	.value	4
	.value	0
	.zero	2
	.value	122
	.zero	2
	.long	15
	.value	-259
	.value	1
	.value	3
	.zero	2
	.value	122
	.zero	2
	.long	15
	.value	-260
	.value	4
	.value	0
	.zero	2
	.value	90
	.zero	2
	.long	15
	.value	-273
	.value	1
	.value	3
	.zero	2
	.value	90
	.zero	2
	.long	15
	.value	-276
	.value	4
	.value	0
	.zero	2
	.value	90
	.zero	2
	.long	15
	.value	-275
	.value	5
	.value	0
	.zero	2
	.value	79
	.zero	2
	.long	15
	.value	-275
	.value	1
	.value	0
	.zero	2
	.value	79
	.zero	2
	.long	15
	.value	-276
	.value	4
	.value	0
	.zero	2
	.value	86
	.zero	2
	.long	15
	.value	-275
	.value	1
	.value	0
	.zero	2
	.value	86
	.zero	2
	.long	15
	.value	-276
	.value	2
	.value	0
	.zero	2
	.value	86
	.zero	2
	.long	15
	.value	-277
	.value	3
	.value	0
	.zero	2
	.value	86
	.zero	2
	.long	15
	.value	-278
	.value	4
	.value	0
	.zero	2
	.value	88
	.zero	2
	.long	15
	.value	-273
	.value	1
	.value	0
	.zero	2
	.value	88
	.zero	2
	.long	15
	.value	-275
	.value	2
	.value	0
	.zero	2
	.value	88
	.zero	2
	.long	15
	.value	-276
	.value	4
	.value	0
	.zero	2
	.value	120
	.zero	2
	.long	15
	.value	-273
	.value	1
	.value	0
	.zero	2
	.value	120
	.zero	2
	.long	15
	.value	-275
	.value	2
	.value	0
	.zero	2
	.value	120
	.zero	2
	.long	15
	.value	-276
	.value	4
	.value	0
	.zero	2
	.value	0
	.zero	2
	.long	16
	.value	0
	.value	0
	.value	0
	.zero	2
	.align 32
	.type	_ZN6icu_67L15Canonical_ItemsE, @object
	.size	_ZN6icu_67L15Canonical_ItemsE, 34
_ZN6icu_67L15Canonical_ItemsE:
	.value	71
	.value	121
	.value	81
	.value	77
	.value	119
	.value	87
	.value	69
	.value	68
	.value	70
	.value	100
	.value	97
	.value	72
	.value	109
	.value	115
	.value	83
	.value	118
	.value	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
