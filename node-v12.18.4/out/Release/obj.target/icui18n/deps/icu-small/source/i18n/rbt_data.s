	.file	"rbt_data.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723TransliterationRuleDataC2ER10UErrorCode
	.type	_ZN6icu_6723TransliterationRuleDataC2ER10UErrorCode, @function
_ZN6icu_6723TransliterationRuleDataC2ER10UErrorCode:
.LFB3094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$8, %rdi
	call	_ZN6icu_6722TransliterationRuleSetC1ER10UErrorCode@PLT
	movl	(%r12), %ecx
	movq	$0, 1064(%rbx)
	testl	%ecx, %ecx
	jle	.L6
.L2:
	movq	$0, 1152(%rbx)
	movb	$1, 1160(%rbx)
.L1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%r12, %r8
	xorl	%ecx, %ecx
	leaq	1072(%rbx), %r13
	movq	%r13, %rdi
	call	uhash_init_67@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L2
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %r14
	movq	%r13, 1064(%rbx)
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	uhash_setKeyDeleter_67@PLT
	movl	(%r12), %eax
	movb	$1, 1160(%rbx)
	movq	$0, 1152(%rbx)
	testl	%eax, %eax
	jg	.L1
	movq	1064(%rbx), %rdi
	movq	%r14, %rsi
	call	uhash_setValueDeleter_67@PLT
	movq	$0, 1152(%rbx)
	movl	$0, 1164(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3094:
	.size	_ZN6icu_6723TransliterationRuleDataC2ER10UErrorCode, .-_ZN6icu_6723TransliterationRuleDataC2ER10UErrorCode
	.globl	_ZN6icu_6723TransliterationRuleDataC1ER10UErrorCode
	.set	_ZN6icu_6723TransliterationRuleDataC1ER10UErrorCode,_ZN6icu_6723TransliterationRuleDataC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723TransliterationRuleDataC2ERKS0_
	.type	_ZN6icu_6723TransliterationRuleDataC2ERKS0_, @function
_ZN6icu_6723TransliterationRuleDataC2ERKS0_:
.LFB3097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-60(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$8, %rsi
	leaq	1072(%r12), %r14
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	8(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN6icu_6722TransliterationRuleSetC1ERKS0_@PLT
	movq	%r13, %r8
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	$0, 1064(%r12)
	movl	$0, -60(%rbp)
	call	uhash_init_67@PLT
	movl	-60(%rbp), %esi
	testl	%esi, %esi
	jle	.L46
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
.L8:
	movzwl	1162(%rbx), %eax
	movq	1064(%r12), %rdi
	movb	$1, 1160(%r12)
	movl	$0, -64(%rbp)
	movw	%ax, 1162(%r12)
	movl	1164(%rbx), %eax
	movl	%eax, 1164(%r12)
	call	uhash_setValueDeleter_67@PLT
	leaq	-64(%rbp), %rax
	movl	$-1, -60(%rbp)
	movq	%rax, -80(%rbp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L7
	movq	8(%r15), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	16(%r15), %rsi
	movl	$64, %edi
	movq	%rsi, -72(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L11
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L11:
	movq	1064(%r12), %rdi
	movq	-80(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	uhash_put_67@PLT
.L12:
	movq	1064(%rbx), %rdi
	movq	%r13, %rsi
	call	uhash_nextElement_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L49
	movq	1152(%rbx), %rdi
	movq	$0, 1152(%r12)
	testq	%rdi, %rdi
	je	.L13
	movslq	1164(%r12), %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 1152(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L7
	movl	1164(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L13
	xorl	%r13d, %r13d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$1, %r13
	cmpl	%r13d, 1164(%r12)
	jle	.L50
.L18:
	movq	1152(%rbx), %rax
	leaq	(%rdi,%r13,8), %r15
	movslq	%r13d, %r14
	movq	(%rax,%r13,8), %r8
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*24(%rax)
	movq	1152(%r12), %rdi
	movq	%rax, (%r15)
	movq	(%rdi,%r13,8), %r8
	testq	%r8, %r8
	jne	.L14
	subl	$1, %r14d
	movl	$7, -64(%rbp)
	cmpl	$-1, %r14d
	je	.L15
	movslq	%r14d, %r14
	movq	(%rdi,%r14,8), %r8
.L24:
	leaq	-8(,%r14,8), %rbx
	movq	$-8, %r13
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L51:
	movq	(%rdi,%rbx), %r8
	subq	$8, %rbx
.L20:
	testq	%r8, %r8
	je	.L19
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	movq	1152(%r12), %rdi
.L19:
	cmpq	%rbx, %r13
	jne	.L51
.L15:
	call	uprv_free_67@PLT
	movq	$0, 1152(%r12)
.L7:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movl	-64(%rbp), %edx
	testl	%edx, %edx
	jg	.L15
.L23:
	movq	-88(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6722TransliterationRuleSet7setDataEPKNS_23TransliterationRuleDataE@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%r14, 1064(%r12)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	call	uhash_setKeyDeleter_67@PLT
	movq	-72(%rbp), %rsi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L50:
	movl	-64(%rbp), %eax
	testl	%eax, %eax
	jg	.L24
	jmp	.L23
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3097:
	.size	_ZN6icu_6723TransliterationRuleDataC2ERKS0_, .-_ZN6icu_6723TransliterationRuleDataC2ERKS0_
	.globl	_ZN6icu_6723TransliterationRuleDataC1ERKS0_
	.set	_ZN6icu_6723TransliterationRuleDataC1ERKS0_,_ZN6icu_6723TransliterationRuleDataC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723TransliterationRuleData6lookupEi
	.type	_ZNK6icu_6723TransliterationRuleData6lookupEi, @function
_ZNK6icu_6723TransliterationRuleData6lookupEi:
.LFB3102:
	.cfi_startproc
	endbr64
	movzwl	1162(%rdi), %eax
	xorl	%r8d, %r8d
	subl	%eax, %esi
	js	.L53
	cmpl	%esi, 1164(%rdi)
	jle	.L53
	movq	1152(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %r8
.L53:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE3102:
	.size	_ZNK6icu_6723TransliterationRuleData6lookupEi, .-_ZNK6icu_6723TransliterationRuleData6lookupEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723TransliterationRuleData13lookupMatcherEi
	.type	_ZNK6icu_6723TransliterationRuleData13lookupMatcherEi, @function
_ZNK6icu_6723TransliterationRuleData13lookupMatcherEi:
.LFB3103:
	.cfi_startproc
	endbr64
	movzwl	1162(%rdi), %eax
	subl	%eax, %esi
	js	.L57
	cmpl	1164(%rdi), %esi
	jge	.L57
	movq	1152(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rdi
	testq	%rdi, %rdi
	je	.L57
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L57:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3103:
	.size	_ZNK6icu_6723TransliterationRuleData13lookupMatcherEi, .-_ZNK6icu_6723TransliterationRuleData13lookupMatcherEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723TransliterationRuleData14lookupReplacerEi
	.type	_ZNK6icu_6723TransliterationRuleData14lookupReplacerEi, @function
_ZNK6icu_6723TransliterationRuleData14lookupReplacerEi:
.LFB3104:
	.cfi_startproc
	endbr64
	movzwl	1162(%rdi), %eax
	subl	%eax, %esi
	js	.L64
	cmpl	1164(%rdi), %esi
	jge	.L64
	movq	1152(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rdi
	testq	%rdi, %rdi
	je	.L64
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3104:
	.size	_ZNK6icu_6723TransliterationRuleData14lookupReplacerEi, .-_ZNK6icu_6723TransliterationRuleData14lookupReplacerEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723TransliterationRuleDataD2Ev
	.type	_ZN6icu_6723TransliterationRuleDataD2Ev, @function
_ZN6icu_6723TransliterationRuleDataD2Ev:
.LFB3100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpb	$0, 1160(%rdi)
	movq	%rdi, %rbx
	movq	1152(%rdi), %r8
	je	.L72
	testq	%r8, %r8
	je	.L72
	movl	1164(%rdi), %eax
	testl	%eax, %eax
	jle	.L72
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L76:
	movq	(%r8,%r12,8), %rdi
	testq	%rdi, %rdi
	je	.L73
	movq	(%rdi), %rax
	addq	$1, %r12
	call	*8(%rax)
	movl	1164(%rbx), %eax
	movq	1152(%rbx), %r8
	cmpl	%r12d, %eax
	jg	.L76
.L72:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movq	1064(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L77
	call	uhash_close_67@PLT
.L77:
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6722TransliterationRuleSetD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	addq	$1, %r12
	cmpl	%r12d, %eax
	jg	.L76
	jmp	.L72
	.cfi_endproc
.LFE3100:
	.size	_ZN6icu_6723TransliterationRuleDataD2Ev, .-_ZN6icu_6723TransliterationRuleDataD2Ev
	.globl	_ZN6icu_6723TransliterationRuleDataD1Ev
	.set	_ZN6icu_6723TransliterationRuleDataD1Ev,_ZN6icu_6723TransliterationRuleDataD2Ev
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
