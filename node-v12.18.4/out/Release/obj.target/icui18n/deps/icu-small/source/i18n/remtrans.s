	.file	"remtrans.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720RemoveTransliterator17getDynamicClassIDEv
	.type	_ZNK6icu_6720RemoveTransliterator17getDynamicClassIDEv, @function
_ZNK6icu_6720RemoveTransliterator17getDynamicClassIDEv:
.LFB2261:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6720RemoveTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2261:
	.size	_ZNK6icu_6720RemoveTransliterator17getDynamicClassIDEv, .-_ZNK6icu_6720RemoveTransliterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720RemoveTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.type	_ZNK6icu_6720RemoveTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, @function
_ZNK6icu_6720RemoveTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona:
.LFB2272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-96(%rbp), %r12
	movq	%rdx, %rbx
	movq	%r12, %rcx
	subq	$80, %rsp
	movl	12(%rdx), %edx
	movl	8(%rbx), %esi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -96(%rbp)
	movl	$2, %eax
	movw	%ax, -88(%rbp)
	movq	(%rdi), %rax
	call	*32(%rax)
	movl	8(%rbx), %edx
	movl	12(%rbx), %eax
	movq	%r12, %rdi
	subl	%edx, %eax
	subl	%eax, 4(%rbx)
	movl	%edx, 12(%rbx)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2272:
	.size	_ZNK6icu_6720RemoveTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, .-_ZNK6icu_6720RemoveTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720RemoveTransliteratorD2Ev
	.type	_ZN6icu_6720RemoveTransliteratorD2Ev, @function
_ZN6icu_6720RemoveTransliteratorD2Ev:
.LFB2268:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6720RemoveTransliteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6714TransliteratorD2Ev@PLT
	.cfi_endproc
.LFE2268:
	.size	_ZN6icu_6720RemoveTransliteratorD2Ev, .-_ZN6icu_6720RemoveTransliteratorD2Ev
	.globl	_ZN6icu_6720RemoveTransliteratorD1Ev
	.set	_ZN6icu_6720RemoveTransliteratorD1Ev,_ZN6icu_6720RemoveTransliteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720RemoveTransliteratorD0Ev
	.type	_ZN6icu_6720RemoveTransliteratorD0Ev, @function
_ZN6icu_6720RemoveTransliteratorD0Ev:
.LFB2270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6720RemoveTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2270:
	.size	_ZN6icu_6720RemoveTransliteratorD0Ev, .-_ZN6icu_6720RemoveTransliteratorD0Ev
	.p2align 4
	.type	_ZN6icu_67L27RemoveTransliterator_createERKNS_13UnicodeStringENS_14Transliterator5TokenE, @function
_ZN6icu_67L27RemoveTransliterator_createERKNS_13UnicodeStringENS_14Transliterator5TokenE:
.LFB2262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$88, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$96, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L10
	leaq	-96(%rbp), %r13
	leaq	_ZL7CURR_ID(%rip), %rax
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6720RemoveTransliteratorE(%rip), %rax
	movq	%rax, (%r12)
.L10:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	addq	$96, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L17:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2262:
	.size	_ZN6icu_67L27RemoveTransliterator_createERKNS_13UnicodeStringENS_14Transliterator5TokenE, .-_ZN6icu_67L27RemoveTransliterator_createERKNS_13UnicodeStringENS_14Transliterator5TokenE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720RemoveTransliterator5cloneEv
	.type	_ZNK6icu_6720RemoveTransliterator5cloneEv, @function
_ZNK6icu_6720RemoveTransliterator5cloneEv:
.LFB2271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$88, %edi
	pushq	%r12
	subq	$104, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L18
	leaq	-112(%rbp), %r14
	leaq	_ZL7CURR_ID(%rip), %rax
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	-120(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6720RemoveTransliteratorE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, (%r12)
	call	_ZNK6icu_6714Transliterator9getFilterEv@PLT
	testq	%rax, %rax
	je	.L18
	movq	%r13, %rdi
	call	_ZNK6icu_6714Transliterator9getFilterEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6714Transliterator11adoptFilterEPNS_13UnicodeFilterE@PLT
.L18:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L27:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2271:
	.size	_ZNK6icu_6720RemoveTransliterator5cloneEv, .-_ZNK6icu_6720RemoveTransliterator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720RemoveTransliterator16getStaticClassIDEv
	.type	_ZN6icu_6720RemoveTransliterator16getStaticClassIDEv, @function
_ZN6icu_6720RemoveTransliterator16getStaticClassIDEv:
.LFB2260:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6720RemoveTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2260:
	.size	_ZN6icu_6720RemoveTransliterator16getStaticClassIDEv, .-_ZN6icu_6720RemoveTransliterator16getStaticClassIDEv
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"N"
	.string	"u"
	.string	"l"
	.string	"l"
	.string	""
	.string	""
	.align 2
.LC1:
	.string	"R"
	.string	"e"
	.string	"m"
	.string	"o"
	.string	"v"
	.string	"e"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720RemoveTransliterator11registerIDsEv
	.type	_ZN6icu_6720RemoveTransliterator11registerIDsEv, @function
_ZN6icu_6720RemoveTransliterator11registerIDsEv:
.LFB2263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-168(%rbp), %r13
	leaq	-96(%rbp), %r12
	movq	%r13, %rdx
	movq	%r12, %rdi
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	_ZL7CURR_ID(%rip), %rax
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	leaq	_ZN6icu_67L27RemoveTransliterator_createERKNS_13UnicodeStringENS_14Transliterator5TokenE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	-160(%rbp), %r13
	leaq	.LC0(%rip), %rax
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	.LC1(%rip), %rax
	movl	$-1, %ecx
	movq	%r13, %rdi
	leaq	-176(%rbp), %rdx
	movl	$1, %esi
	movq	%rax, -176(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6714Transliterator23_registerSpecialInverseERKNS_13UnicodeStringES3_a@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L32
	addq	$160, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L32:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2263:
	.size	_ZN6icu_6720RemoveTransliterator11registerIDsEv, .-_ZN6icu_6720RemoveTransliterator11registerIDsEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720RemoveTransliteratorC2Ev
	.type	_ZN6icu_6720RemoveTransliteratorC2Ev, @function
_ZN6icu_6720RemoveTransliteratorC2Ev:
.LFB2265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-96(%rbp), %r12
	movq	%rdi, %rbx
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	_ZL7CURR_ID(%rip), %rax
	movq	%rax, -104(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6720RemoveTransliteratorE(%rip), %rax
	movq	%rax, (%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L36:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2265:
	.size	_ZN6icu_6720RemoveTransliteratorC2Ev, .-_ZN6icu_6720RemoveTransliteratorC2Ev
	.globl	_ZN6icu_6720RemoveTransliteratorC1Ev
	.set	_ZN6icu_6720RemoveTransliteratorC1Ev,_ZN6icu_6720RemoveTransliteratorC2Ev
	.weak	_ZTSN6icu_6720RemoveTransliteratorE
	.section	.rodata._ZTSN6icu_6720RemoveTransliteratorE,"aG",@progbits,_ZTSN6icu_6720RemoveTransliteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6720RemoveTransliteratorE, @object
	.size	_ZTSN6icu_6720RemoveTransliteratorE, 32
_ZTSN6icu_6720RemoveTransliteratorE:
	.string	"N6icu_6720RemoveTransliteratorE"
	.weak	_ZTIN6icu_6720RemoveTransliteratorE
	.section	.data.rel.ro._ZTIN6icu_6720RemoveTransliteratorE,"awG",@progbits,_ZTIN6icu_6720RemoveTransliteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6720RemoveTransliteratorE, @object
	.size	_ZTIN6icu_6720RemoveTransliteratorE, 24
_ZTIN6icu_6720RemoveTransliteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6720RemoveTransliteratorE
	.quad	_ZTIN6icu_6714TransliteratorE
	.weak	_ZTVN6icu_6720RemoveTransliteratorE
	.section	.data.rel.ro._ZTVN6icu_6720RemoveTransliteratorE,"awG",@progbits,_ZTVN6icu_6720RemoveTransliteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6720RemoveTransliteratorE, @object
	.size	_ZTVN6icu_6720RemoveTransliteratorE, 152
_ZTVN6icu_6720RemoveTransliteratorE:
	.quad	0
	.quad	_ZTIN6icu_6720RemoveTransliteratorE
	.quad	_ZN6icu_6720RemoveTransliteratorD1Ev
	.quad	_ZN6icu_6720RemoveTransliteratorD0Ev
	.quad	_ZNK6icu_6720RemoveTransliterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6720RemoveTransliterator5cloneEv
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.quad	_ZNK6icu_6720RemoveTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.quad	_ZNK6icu_6714Transliterator5getIDEv
	.quad	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE
	.local	_ZZN6icu_6720RemoveTransliterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6720RemoveTransliterator16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.align 16
	.type	_ZL7CURR_ID, @object
	.size	_ZL7CURR_ID, 22
_ZL7CURR_ID:
	.value	65
	.value	110
	.value	121
	.value	45
	.value	82
	.value	101
	.value	109
	.value	111
	.value	118
	.value	101
	.value	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
