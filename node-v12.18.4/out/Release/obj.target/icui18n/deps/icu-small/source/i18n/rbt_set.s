	.file	"rbt_set.cpp"
	.text
	.p2align 4
	.type	_deleteRule, @function
_deleteRule:
.LFB2368:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.cfi_endproc
.LFE2368:
	.size	_deleteRule, .-_deleteRule
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliterationRuleSet23getMaximumContextLengthEv
	.type	_ZNK6icu_6722TransliterationRuleSet23getMaximumContextLengthEv, @function
_ZNK6icu_6722TransliterationRuleSet23getMaximumContextLengthEv:
.LFB2381:
	.cfi_startproc
	endbr64
	movl	1052(%rdi), %eax
	ret
	.cfi_endproc
.LFE2381:
	.size	_ZNK6icu_6722TransliterationRuleSet23getMaximumContextLengthEv, .-_ZNK6icu_6722TransliterationRuleSet23getMaximumContextLengthEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliterationRuleSetD2Ev
	.type	_ZN6icu_6722TransliterationRuleSetD2Ev, @function
_ZN6icu_6722TransliterationRuleSetD2Ev:
.LFB2377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722TransliterationRuleSetE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L6
	movq	(%rdi), %rax
	call	*8(%rax)
.L6:
	movq	16(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2377:
	.size	_ZN6icu_6722TransliterationRuleSetD2Ev, .-_ZN6icu_6722TransliterationRuleSetD2Ev
	.globl	_ZN6icu_6722TransliterationRuleSetD1Ev
	.set	_ZN6icu_6722TransliterationRuleSetD1Ev,_ZN6icu_6722TransliterationRuleSetD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliterationRuleSet7addRuleEPNS_19TransliterationRuleER10UErrorCode
	.type	_ZN6icu_6722TransliterationRuleSet7addRuleEPNS_19TransliterationRuleER10UErrorCode, @function
_ZN6icu_6722TransliterationRuleSet7addRuleEPNS_19TransliterationRuleER10UErrorCode:
.LFB2382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdx), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testl	%eax, %eax
	jle	.L12
	testq	%rsi, %rsi
	je	.L11
	movq	(%rsi), %rax
	popq	%rbx
	movq	%rsi, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	8(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	cmpl	1052(%rbx), %eax
	jle	.L14
	movl	%eax, 1052(%rbx)
.L14:
	movq	16(%rbx), %rdi
	call	uprv_free_67@PLT
	movq	$0, 16(%rbx)
.L11:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2382:
	.size	_ZN6icu_6722TransliterationRuleSet7addRuleEPNS_19TransliterationRuleER10UErrorCode, .-_ZN6icu_6722TransliterationRuleSet7addRuleEPNS_19TransliterationRuleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliterationRuleSet7toRulesERNS_13UnicodeStringEa
	.type	_ZNK6icu_6722TransliterationRuleSet7toRulesERNS_13UnicodeStringEa, @function
_ZNK6icu_6722TransliterationRuleSet7toRulesERNS_13UnicodeStringEa:
.LFB2385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movl	8(%rax), %eax
	movl	%eax, -68(%rbp)
	movzwl	8(%rsi), %eax
	testb	$1, %al
	je	.L20
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L25:
	movl	-68(%rbp), %edx
	movsbl	%bl, %ebx
	xorl	%r14d, %r14d
	leaq	-58(%rbp), %r15
	testl	%edx, %edx
	jg	.L22
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$10, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L22:
	movq	8(%r13), %rdi
	movl	%r14d, %esi
	addl	$1, %r14d
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*32(%rax)
	cmpl	-68(%rbp), %r14d
	jne	.L34
.L26:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L23
	movswl	%ax, %edx
	sarl	$5, %edx
.L24:
	testl	%edx, %edx
	je	.L25
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L23:
	movl	12(%rsi), %edx
	jmp	.L24
.L36:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2385:
	.size	_ZNK6icu_6722TransliterationRuleSet7toRulesERNS_13UnicodeStringEa, .-_ZNK6icu_6722TransliterationRuleSet7toRulesERNS_13UnicodeStringEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliterationRuleSetD0Ev
	.type	_ZN6icu_6722TransliterationRuleSetD0Ev, @function
_ZN6icu_6722TransliterationRuleSetD0Ev:
.LFB2379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722TransliterationRuleSetE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L38
	movq	(%rdi), %rax
	call	*8(%rax)
.L38:
	movq	16(%r12), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2379:
	.size	_ZN6icu_6722TransliterationRuleSetD0Ev, .-_ZN6icu_6722TransliterationRuleSetD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliterationRuleSetC2ER10UErrorCode
	.type	_ZN6icu_6722TransliterationRuleSetC2ER10UErrorCode, @function
_ZN6icu_6722TransliterationRuleSetC2ER10UErrorCode:
.LFB2371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722TransliterationRuleSetE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L44
	movq	%rax, %rdi
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%rax, %r12
	leaq	_deleteRule(%rip), %rsi
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	movl	0(%r13), %eax
	movq	%r12, 8(%rbx)
	testl	%eax, %eax
	jg	.L43
.L45:
	movq	$0, 16(%rbx)
	movl	$0, 1052(%rbx)
.L43:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	cmpl	$0, 0(%r13)
	movq	$0, 8(%rbx)
	jg	.L43
	movl	$7, 0(%r13)
	jmp	.L45
	.cfi_endproc
.LFE2371:
	.size	_ZN6icu_6722TransliterationRuleSetC2ER10UErrorCode, .-_ZN6icu_6722TransliterationRuleSetC2ER10UErrorCode
	.globl	_ZN6icu_6722TransliterationRuleSetC1ER10UErrorCode
	.set	_ZN6icu_6722TransliterationRuleSetC1ER10UErrorCode,_ZN6icu_6722TransliterationRuleSetC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliterationRuleSet7setDataEPKNS_23TransliterationRuleDataE
	.type	_ZN6icu_6722TransliterationRuleSet7setDataEPKNS_23TransliterationRuleDataE, @function
_ZN6icu_6722TransliterationRuleSet7setDataEPKNS_23TransliterationRuleDataE:
.LFB2380:
	.cfi_startproc
	endbr64
	movl	1048(%rdi), %eax
	testl	%eax, %eax
	jle	.L56
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	8(,%rax,8), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L53:
	movq	16(%r14), %rax
	movq	%r12, %rsi
	movq	(%rax,%rbx), %rdi
	addq	$8, %rbx
	call	_ZN6icu_6719TransliterationRule7setDataEPKNS_23TransliterationRuleDataE@PLT
	cmpq	%rbx, %r13
	jne	.L53
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE2380:
	.size	_ZN6icu_6722TransliterationRuleSet7setDataEPKNS_23TransliterationRuleDataE, .-_ZN6icu_6722TransliterationRuleSet7setDataEPKNS_23TransliterationRuleDataE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliterationRuleSet13transliterateERNS_11ReplaceableER14UTransPositiona
	.type	_ZN6icu_6722TransliterationRuleSet13transliterateERNS_11ReplaceableER14UTransPositiona, @function
_ZN6icu_6722TransliterationRuleSet13transliterateERNS_11ReplaceableER14UTransPositiona:
.LFB2384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	movsbl	%bl, %ebx
	subq	$24, %rsp
	movq	(%r12), %rax
	movl	8(%rdx), %esi
	call	*80(%rax)
	movzbl	%al, %eax
	leaq	(%r14,%rax,4), %r9
	movslq	24(%r9), %r15
	movq	%r15, %r8
	salq	$3, %r15
	cmpl	28(%r9), %r8d
	jge	.L64
.L65:
	movq	16(%r14), %rax
	movl	%ebx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r9, -64(%rbp)
	movq	(%rax,%r15), %rdi
	movl	%r8d, -52(%rbp)
	call	_ZNK6icu_6719TransliterationRule15matchAndReplaceERNS_11ReplaceableER14UTransPositiona@PLT
	cmpl	$1, %eax
	je	.L67
	cmpl	$2, %eax
	je	.L68
	movl	-52(%rbp), %r8d
	movq	-64(%rbp), %r9
	addq	$8, %r15
	addl	$1, %r8d
	cmpl	%r8d, 28(%r9)
	jg	.L65
.L64:
	movq	(%r12), %rax
	movl	8(%r13), %esi
	movq	%r12, %rdi
	call	*80(%rax)
	cmpl	$65535, %eax
	seta	%al
	movzbl	%al, %eax
	addl	$1, %eax
	addl	%eax, 8(%r13)
	movl	$1, %eax
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$1, %eax
.L59:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2384:
	.size	_ZN6icu_6722TransliterationRuleSet13transliterateERNS_11ReplaceableER14UTransPositiona, .-_ZN6icu_6722TransliterationRuleSet13transliterateERNS_11ReplaceableER14UTransPositiona
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliterationRuleSet6freezeER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6722TransliterationRuleSet6freezeER11UParseErrorR10UErrorCode, @function
_ZN6icu_6722TransliterationRuleSet6freezeER11UParseErrorR10UErrorCode:
.LFB2383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$200, %rsp
	movq	%rsi, -224(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	leaq	-176(%rbp), %rdi
	movq	%rdi, -200(%rbp)
	movl	8(%rax), %eax
	leal	(%rax,%rax), %esi
	movl	%eax, -216(%rbp)
	call	_ZN6icu_677UVectorC1EiR10UErrorCode@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L123
.L73:
	movq	-200(%rbp), %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L124
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L123:
	.cfi_restore_state
	movslq	-216(%rbp), %rax
	movl	$1, %edi
	testl	%eax, %eax
	cmovg	%rax, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L88
	movl	-216(%rbp), %eax
	xorl	%r13d, %r13d
	leal	-1(%rax), %ebx
	testl	%eax, %eax
	jle	.L77
.L78:
	movq	8(%r12), %rdi
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6719TransliterationRule13getIndexValueEv@PLT
	movw	%ax, (%r15,%r13,2)
	movq	%r13, %rax
	addq	$1, %r13
	cmpq	%rax, %rbx
	jne	.L78
.L77:
	movl	-216(%rbp), %eax
	xorl	%r14d, %r14d
	leal	-1(%rax), %ebx
	.p2align 4,,10
	.p2align 3
.L76:
	movl	-168(%rbp), %eax
	movl	-216(%rbp), %edi
	movl	%r14d, -188(%rbp)
	xorl	%r13d, %r13d
	movl	%eax, 24(%r12,%r14,4)
	testl	%edi, %edi
	jg	.L86
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L126:
	cmpw	%r14w, %ax
	je	.L125
.L83:
	leaq	1(%r13), %rax
	cmpq	%r13, %rbx
	je	.L85
.L127:
	movq	%rax, %r13
.L86:
	movzwl	(%r15,%r13,2), %eax
	testw	%ax, %ax
	jns	.L126
	movq	8(%r12), %rdi
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	-188(%rbp), %esi
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	_ZNK6icu_6719TransliterationRule17matchesIndexValueEh@PLT
	movq	-184(%rbp), %r10
	testb	%al, %al
	je	.L83
	movq	-208(%rbp), %rdx
	movq	-200(%rbp), %rdi
	movq	%r10, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	leaq	1(%r13), %rax
	cmpq	%r13, %rbx
	jne	.L127
	.p2align 4,,10
	.p2align 3
.L85:
	addq	$1, %r14
	cmpq	$256, %r14
	jne	.L76
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	movl	-168(%rbp), %eax
	movq	16(%r12), %rdi
	movl	%eax, 1048(%r12)
	call	uprv_free_67@PLT
	movslq	-168(%rbp), %rax
	testl	%eax, %eax
	jne	.L87
	movq	$0, 16(%r12)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L125:
	movq	8(%r12), %rdi
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	-208(%rbp), %rdx
	movq	-200(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	jmp	.L83
.L87:
	leaq	0(,%rax,8), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L88
	movl	-168(%rbp), %esi
	testl	%esi, %esi
	jle	.L89
	movq	-200(%rbp), %r14
	xorl	%ebx, %ebx
	jmp	.L90
.L128:
	movq	16(%r12), %rax
.L90:
	movl	%ebx, %esi
	movq	%r14, %rdi
	leaq	(%rax,%rbx,8), %r13
	addq	$1, %rbx
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, 0(%r13)
	cmpl	%ebx, -168(%rbp)
	jg	.L128
.L89:
	leaq	1052(%r12), %rax
	movq	%r12, -184(%rbp)
	leaq	28(%r12), %rbx
	movq	%rax, -232(%rbp)
.L104:
	movl	-4(%rbx), %eax
	movl	%eax, -188(%rbp)
	movl	%eax, %ecx
	movl	(%rbx), %eax
	leal	-1(%rax), %edx
	cmpl	%edx, %ecx
	jge	.L91
	movslq	%ecx, %r13
	leaq	0(,%r13,8), %rcx
	movq	%rcx, -216(%rbp)
.L94:
	movq	-184(%rbp), %rcx
	addl	$1, -188(%rbp)
	movl	-188(%rbp), %r14d
	movq	16(%rcx), %rsi
	movq	-216(%rbp), %rcx
	movq	(%rsi,%rcx), %r15
	cmpl	%eax, %r14d
	jge	.L92
	movslq	%r14d, %r12
	salq	$3, %r12
	jmp	.L93
.L95:
	movl	(%rbx), %eax
	addl	$1, %r14d
	addq	$8, %r12
	cmpl	%r14d, %eax
	jle	.L129
	movq	-184(%rbp), %rax
	movq	16(%rax), %rsi
.L93:
	movq	(%rsi,%r12), %r13
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	*24(%rax)
	testb	%al, %al
	je	.L95
	movq	-208(%rbp), %rax
	leaq	-128(%rbp), %r12
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	$2, %ecx
	movq	%r13, -184(%rbp)
	movq	%r12, %rsi
	movl	$65557, (%rax)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	-224(%rbp), %rax
	movw	%cx, -120(%rbp)
	movq	$-1, (%rax)
	movq	(%r15), %rax
	call	*32(%rax)
	movswl	-120(%rbp), %eax
	movq	-184(%rbp), %r9
	testw	%ax, %ax
	js	.L96
	sarl	$5, %eax
	movl	%eax, %edi
.L97:
	movl	$15, %esi
	movq	%r9, -184(%rbp)
	call	uprv_min_67@PLT
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-224(%rbp), %r15
	movslq	%eax, %rbx
	movl	%ebx, %edx
	leaq	8(%r15), %r13
	movq	%r13, %rcx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movzwl	-120(%rbp), %eax
	xorl	%edx, %edx
	movq	-184(%rbp), %r9
	movw	%dx, 8(%r15,%rbx,2)
	testb	$1, %al
	je	.L98
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movq	-184(%rbp), %r9
.L99:
	movq	(%r9), %rax
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	*32(%rax)
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L102
	sarl	$5, %eax
	movl	%eax, %edi
.L103:
	movl	$15, %esi
	call	uprv_min_67@PLT
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-224(%rbp), %r15
	movslq	%eax, %rbx
	movl	%ebx, %edx
	leaq	40(%r15), %r13
	movq	%r13, %rcx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%eax, %eax
	movq	%r12, %rdi
	movw	%ax, 40(%r15,%rbx,2)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L73
.L129:
	leal	-1(%rax), %edx
.L92:
	addq	$8, -216(%rbp)
	cmpl	-188(%rbp), %edx
	jg	.L94
.L91:
	addq	$4, %rbx
	cmpq	%rbx, -232(%rbp)
	jne	.L104
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L98:
	testw	%ax, %ax
	js	.L100
	movswl	%ax, %edx
	sarl	$5, %edx
.L101:
	testl	%edx, %edx
	je	.L99
	andl	$31, %eax
	movw	%ax, -120(%rbp)
	jmp	.L99
.L102:
	movl	-116(%rbp), %edi
	jmp	.L103
.L96:
	movl	-116(%rbp), %edi
	jmp	.L97
.L100:
	movl	-116(%rbp), %edx
	jmp	.L101
.L124:
	call	__stack_chk_fail@PLT
.L88:
	movq	-208(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L73
	.cfi_endproc
.LFE2383:
	.size	_ZN6icu_6722TransliterationRuleSet6freezeER11UParseErrorR10UErrorCode, .-_ZN6icu_6722TransliterationRuleSet6freezeER11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliterationRuleSetC2ERKS0_
	.type	_ZN6icu_6722TransliterationRuleSetC2ERKS0_, @function
_ZN6icu_6722TransliterationRuleSetC2ERKS0_:
.LFB2374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24(%rdi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	addq	$32, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$24, %rsi
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6722TransliterationRuleSetE(%rip), %rax
	movq	$0, -24(%rdi)
	movq	%rax, -32(%rdi)
	movl	1028(%rsi), %eax
	movq	$0, -16(%rdi)
	movl	%eax, 1020(%rdi)
	movq	24(%rbx), %rax
	movq	%rax, -8(%rdi)
	movq	1044(%rbx), %rax
	movq	%rax, 1012(%rdi)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	$1028, %ecx
	shrl	$3, %ecx
	rep movsq
	movl	$40, %edi
	movl	$0, -132(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L131
	leaq	-132(%rbp), %r14
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rax, %r12
	movq	%r14, %rcx
	leaq	_deleteRule(%rip), %rsi
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	movq	8(%rbx), %rdi
	movq	%r12, 8(%r13)
	testq	%rdi, %rdi
	je	.L132
	movl	-132(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L152
.L130:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movl	8(%rdi), %eax
	xorl	%r15d, %r15d
	movl	%eax, -156(%rbp)
	testl	%eax, %eax
	jg	.L134
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L154:
	addl	$1, %r15d
	cmpl	-156(%rbp), %r15d
	je	.L135
	movq	8(%rbx), %rdi
.L134:
	movl	%r15d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	$136, %edi
	movq	%rax, -152(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L130
	movq	-152(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6719TransliterationRuleC1ERS0_@PLT
	movq	8(%r13), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	-132(%rbp), %edx
	testl	%edx, %edx
	jle	.L154
	jmp	.L130
.L131:
	movq	$0, 8(%r13)
	.p2align 4,,10
	.p2align 3
.L132:
	cmpq	$0, 16(%rbx)
	je	.L130
	movl	-132(%rbp), %eax
	testl	%eax, %eax
	jg	.L130
	leaq	-132(%rbp), %r14
.L141:
	leaq	-128(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6722TransliterationRuleSet6freezeER11UParseErrorR10UErrorCode
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L135:
	cmpq	$0, 16(%rbx)
	jne	.L141
	jmp	.L130
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2374:
	.size	_ZN6icu_6722TransliterationRuleSetC2ERKS0_, .-_ZN6icu_6722TransliterationRuleSetC2ERKS0_
	.globl	_ZN6icu_6722TransliterationRuleSetC1ERKS0_
	.set	_ZN6icu_6722TransliterationRuleSetC1ERKS0_,_ZN6icu_6722TransliterationRuleSetC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliterationRuleSet18getSourceTargetSetERNS_10UnicodeSetEa
	.type	_ZNK6icu_6722TransliterationRuleSet18getSourceTargetSetERNS_10UnicodeSetEa, @function
_ZNK6icu_6722TransliterationRuleSet18getSourceTargetSetERNS_10UnicodeSetEa:
.LFB2386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	movq	8(%r13), %rdi
	movl	8(%rdi), %r14d
	testl	%r14d, %r14d
	jle	.L156
	testb	%bl, %bl
	movl	$0, %ebx
	jne	.L157
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L164:
	movq	8(%r13), %rdi
.L159:
	movl	%ebx, %esi
	addl	$1, %ebx
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_6719TransliterationRule14addSourceSetToERNS_10UnicodeSetE@PLT
	cmpl	%ebx, %r14d
	jne	.L164
.L156:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movq	8(%r13), %rdi
.L157:
	movl	%ebx, %esi
	addl	$1, %ebx
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_6719TransliterationRule14addTargetSetToERNS_10UnicodeSetE@PLT
	cmpl	%r14d, %ebx
	jne	.L165
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2386:
	.size	_ZNK6icu_6722TransliterationRuleSet18getSourceTargetSetERNS_10UnicodeSetEa, .-_ZNK6icu_6722TransliterationRuleSet18getSourceTargetSetERNS_10UnicodeSetEa
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6722TransliterationRuleSetE
	.section	.rodata._ZTSN6icu_6722TransliterationRuleSetE,"aG",@progbits,_ZTSN6icu_6722TransliterationRuleSetE,comdat
	.align 32
	.type	_ZTSN6icu_6722TransliterationRuleSetE, @object
	.size	_ZTSN6icu_6722TransliterationRuleSetE, 34
_ZTSN6icu_6722TransliterationRuleSetE:
	.string	"N6icu_6722TransliterationRuleSetE"
	.weak	_ZTIN6icu_6722TransliterationRuleSetE
	.section	.data.rel.ro._ZTIN6icu_6722TransliterationRuleSetE,"awG",@progbits,_ZTIN6icu_6722TransliterationRuleSetE,comdat
	.align 8
	.type	_ZTIN6icu_6722TransliterationRuleSetE, @object
	.size	_ZTIN6icu_6722TransliterationRuleSetE, 24
_ZTIN6icu_6722TransliterationRuleSetE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722TransliterationRuleSetE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTVN6icu_6722TransliterationRuleSetE
	.section	.data.rel.ro.local._ZTVN6icu_6722TransliterationRuleSetE,"awG",@progbits,_ZTVN6icu_6722TransliterationRuleSetE,comdat
	.align 8
	.type	_ZTVN6icu_6722TransliterationRuleSetE, @object
	.size	_ZTVN6icu_6722TransliterationRuleSetE, 64
_ZTVN6icu_6722TransliterationRuleSetE:
	.quad	0
	.quad	_ZTIN6icu_6722TransliterationRuleSetE
	.quad	_ZN6icu_6722TransliterationRuleSetD1Ev
	.quad	_ZN6icu_6722TransliterationRuleSetD0Ev
	.quad	_ZNK6icu_6722TransliterationRuleSet23getMaximumContextLengthEv
	.quad	_ZN6icu_6722TransliterationRuleSet7addRuleEPNS_19TransliterationRuleER10UErrorCode
	.quad	_ZN6icu_6722TransliterationRuleSet6freezeER11UParseErrorR10UErrorCode
	.quad	_ZNK6icu_6722TransliterationRuleSet7toRulesERNS_13UnicodeStringEa
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
