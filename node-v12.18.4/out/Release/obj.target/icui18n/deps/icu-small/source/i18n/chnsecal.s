	.file	"chnsecal.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"chinese"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar7getTypeEv
	.type	_ZNK6icu_6715ChineseCalendar7getTypeEv, @function
_ZNK6icu_6715ChineseCalendar7getTypeEv:
.LFB3127:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE3127:
	.size	_ZNK6icu_6715ChineseCalendar7getTypeEv, .-_ZNK6icu_6715ChineseCalendar7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.type	_ZNK6icu_6715ChineseCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, @function
_ZNK6icu_6715ChineseCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE:
.LFB3130:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	leaq	_ZN6icu_67L6LIMITSE(%rip), %rax
	leaq	(%rdx,%rsi,4), %rdx
	movl	(%rax,%rdx,4), %eax
	ret
	.cfi_endproc
.LFE3130:
	.size	_ZNK6icu_6715ChineseCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, .-_ZNK6icu_6715ChineseCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar20handleGetMonthLengthEii
	.type	_ZNK6icu_6715ChineseCalendar20handleGetMonthLengthEii, @function
_ZNK6icu_6715ChineseCalendar20handleGetMonthLengthEii:
.LFB3132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	call	*264(%rax)
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movl	$1, %esi
	movl	%eax, %ebx
	leal	-2440562(%rax), %eax
	cvtsi2sdl	%eax, %xmm0
	movq	(%r12), %rax
	subl	$2440587, %ebx
	call	*408(%rax)
	subl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3132:
	.size	_ZNK6icu_6715ChineseCalendar20handleGetMonthLengthEii, .-_ZNK6icu_6715ChineseCalendar20handleGetMonthLengthEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ChineseCalendar19handleComputeFieldsEiR10UErrorCode
	.type	_ZN6icu_6715ChineseCalendar19handleComputeFieldsEiR10UErrorCode, @function
_ZN6icu_6715ChineseCalendar19handleComputeFieldsEiR10UErrorCode:
.LFB3133:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	284(%rdi), %ecx
	subl	$2440588, %esi
	movl	$1, %r8d
	movl	280(%rdi), %edx
	movq	448(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE3133:
	.size	_ZN6icu_6715ChineseCalendar19handleComputeFieldsEiR10UErrorCode, .-_ZN6icu_6715ChineseCalendar19handleComputeFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar23getFieldResolutionTableEv
	.type	_ZNK6icu_6715ChineseCalendar23getFieldResolutionTableEv, @function
_ZNK6icu_6715ChineseCalendar23getFieldResolutionTableEv:
.LFB3134:
	.cfi_startproc
	endbr64
	leaq	_ZN6icu_6715ChineseCalendar23CHINESE_DATE_PRECEDENCEE(%rip), %rax
	ret
	.cfi_endproc
.LFE3134:
	.size	_ZNK6icu_6715ChineseCalendar23getFieldResolutionTableEv, .-_ZNK6icu_6715ChineseCalendar23getFieldResolutionTableEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar20synodicMonthsBetweenEii
	.type	_ZNK6icu_6715ChineseCalendar20synodicMonthsBetweenEii, @function
_ZNK6icu_6715ChineseCalendar20synodicMonthsBetweenEii:
.LFB3144:
	.cfi_startproc
	endbr64
	subl	%esi, %edx
	pxor	%xmm0, %xmm0
	movsd	.LC1(%rip), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	_ZN6icu_6718CalendarAstronomer13SYNODIC_MONTHE(%rip), %xmm0
	comisd	.LC3(%rip), %xmm0
	jnb	.L9
	movsd	.LC2(%rip), %xmm1
.L9:
	addsd	%xmm1, %xmm0
	cvttsd2sil	%xmm0, %eax
	ret
	.cfi_endproc
.LFE3144:
	.size	_ZNK6icu_6715ChineseCalendar20synodicMonthsBetweenEii, .-_ZNK6icu_6715ChineseCalendar20synodicMonthsBetweenEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar19hasNoMajorSolarTermEi
	.type	_ZNK6icu_6715ChineseCalendar19hasNoMajorSolarTermEi, @function
_ZNK6icu_6715ChineseCalendar19hasNoMajorSolarTermEi:
.LFB3146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movl	%esi, %ebx
	addl	$25, %ebx
	call	*424(%rax)
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movl	$1, %esi
	movl	%eax, %r13d
	movq	(%r12), %rax
	cvtsi2sdl	%ebx, %xmm0
	movq	424(%rax), %r14
	call	*408(%rax)
	movq	%r12, %rdi
	movl	%eax, %esi
	call	*%r14
	popq	%rbx
	popq	%r12
	cmpl	%eax, %r13d
	popq	%r13
	popq	%r14
	sete	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3146:
	.size	_ZNK6icu_6715ChineseCalendar19hasNoMajorSolarTermEi, .-_ZNK6icu_6715ChineseCalendar19hasNoMajorSolarTermEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar18isLeapMonthBetweenEii
	.type	_ZNK6icu_6715ChineseCalendar18isLeapMonthBetweenEii, @function
_ZNK6icu_6715ChineseCalendar18isLeapMonthBetweenEii:
.LFB3147:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	%esi, %edx
	jl	.L18
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	leal	-25(%r13), %eax
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rdx
	cvtsi2sdl	%eax, %xmm0
	movq	440(%rdx), %rbx
	call	*408(%rdx)
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	*%rbx
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	je	.L21
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	(%r12), %rax
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*432(%rax)
	popq	%rbx
	popq	%r12
	testb	%al, %al
	popq	%r13
	popq	%r14
	setne	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3147:
	.size	_ZNK6icu_6715ChineseCalendar18isLeapMonthBetweenEii, .-_ZNK6icu_6715ChineseCalendar18isLeapMonthBetweenEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar18haveDefaultCenturyEv
	.type	_ZNK6icu_6715ChineseCalendar18haveDefaultCenturyEv, @function
_ZNK6icu_6715ChineseCalendar18haveDefaultCenturyEv:
.LFB3152:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3152:
	.size	_ZNK6icu_6715ChineseCalendar18haveDefaultCenturyEv, .-_ZNK6icu_6715ChineseCalendar18haveDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar17getDynamicClassIDEv
	.type	_ZNK6icu_6715ChineseCalendar17getDynamicClassIDEv, @function
_ZNK6icu_6715ChineseCalendar17getDynamicClassIDEv:
.LFB3159:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6715ChineseCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3159:
	.size	_ZNK6icu_6715ChineseCalendar17getDynamicClassIDEv, .-_ZNK6icu_6715ChineseCalendar17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar5cloneEv
	.type	_ZNK6icu_6715ChineseCalendar5cloneEv, @function
_ZNK6icu_6715ChineseCalendar5cloneEv:
.LFB3113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$624, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L24
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_678CalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6715ChineseCalendarE(%rip), %rax
	movq	%rax, (%r12)
	movzbl	610(%rbx), %eax
	movb	%al, 610(%r12)
	movl	612(%rbx), %eax
	movl	%eax, 612(%r12)
	movq	616(%rbx), %rax
	movq	%rax, 616(%r12)
.L24:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3113:
	.size	_ZNK6icu_6715ChineseCalendar5cloneEv, .-_ZNK6icu_6715ChineseCalendar5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ChineseCalendarD2Ev
	.type	_ZN6icu_6715ChineseCalendarD2Ev, @function
_ZN6icu_6715ChineseCalendarD2Ev:
.LFB3124:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6715ChineseCalendarE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678CalendarD2Ev@PLT
	.cfi_endproc
.LFE3124:
	.size	_ZN6icu_6715ChineseCalendarD2Ev, .-_ZN6icu_6715ChineseCalendarD2Ev
	.globl	_ZN6icu_6715ChineseCalendarD1Ev
	.set	_ZN6icu_6715ChineseCalendarD1Ev,_ZN6icu_6715ChineseCalendarD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ChineseCalendarD0Ev
	.type	_ZN6icu_6715ChineseCalendarD0Ev, @function
_ZN6icu_6715ChineseCalendarD0Ev:
.LFB3126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715ChineseCalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678CalendarD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3126:
	.size	_ZN6icu_6715ChineseCalendarD0Ev, .-_ZN6icu_6715ChineseCalendarD0Ev
	.p2align 4
	.type	calendar_chinese_cleanup, @function
calendar_chinese_cleanup:
.LFB3112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	_ZL21gChineseCalendarAstro(%rip), %r12
	testq	%r12, %r12
	je	.L34
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomerD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	$0, _ZL21gChineseCalendarAstro(%rip)
.L34:
	movq	_ZL35gChineseCalendarWinterSolsticeCache(%rip), %rdi
	testq	%rdi, %rdi
	je	.L35
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, _ZL35gChineseCalendarWinterSolsticeCache(%rip)
.L35:
	movq	_ZL28gChineseCalendarNewYearCache(%rip), %rdi
	testq	%rdi, %rdi
	je	.L36
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, _ZL28gChineseCalendarNewYearCache(%rip)
.L36:
	movq	_ZL29gChineseCalendarZoneAstroCalc(%rip), %rdi
	testq	%rdi, %rdi
	je	.L37
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, _ZL29gChineseCalendarZoneAstroCalc(%rip)
.L37:
	movl	$1, %eax
	movl	$0, _ZL37gChineseCalendarZoneAstroCalcInitOnce(%rip)
	mfence
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3112:
	.size	calendar_chinese_cleanup, .-calendar_chinese_cleanup
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar23handleComputeMonthStartEiia
	.type	_ZNK6icu_6715ChineseCalendar23handleComputeMonthStartEiia, @function
_ZNK6icu_6715ChineseCalendar23handleComputeMonthStartEiia:
.LFB3135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	leaq	-64(%rbp), %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$11, %r13d
	ja	.L62
.L52:
	movl	612(%rbx), %esi
	movq	(%rbx), %rax
	movq	%rdx, -72(%rbp)
	movq	%rbx, %rdi
	addl	%r12d, %esi
	subl	$1, %esi
	call	*456(%rax)
	pxor	%xmm0, %xmm0
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%eax, %r8d
	imull	$29, %r13d, %eax
	addl	%r8d, %eax
	cvtsi2sdl	%eax, %xmm0
	movq	(%rbx), %rax
	call	*408(%rax)
	movq	-72(%rbp), %rdx
	testb	%r14b, %r14b
	movq	%rbx, %rdi
	leal	2440588(%rax), %r15d
	movl	%eax, %r12d
	movl	20(%rbx), %eax
	movl	$0, %r14d
	movl	%r15d, %esi
	movl	$0, -64(%rbp)
	movl	%eax, -76(%rbp)
	movl	100(%rbx), %eax
	cmovne	%eax, %r14d
	movl	%eax, -80(%rbp)
	call	_ZN6icu_678Calendar22computeGregorianFieldsEiR10UErrorCode@PLT
	movl	-64(%rbp), %eax
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jg	.L51
	movq	(%rbx), %r10
	movl	284(%rbx), %ecx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movl	280(%rbx), %edx
	call	*448(%r10)
	cmpl	20(%rbx), %r13d
	je	.L63
.L55:
	leal	25(%r12), %eax
	pxor	%xmm0, %xmm0
	movl	$1, %esi
	movq	%rbx, %rdi
	cvtsi2sdl	%eax, %xmm0
	movq	(%rbx), %rax
	call	*408(%rax)
	leal	2440588(%rax), %r15d
.L56:
	movl	-76(%rbp), %eax
	movb	$1, 106(%rbx)
	leal	-1(%r15), %r8d
	movl	$1, 136(%rbx)
	movl	%eax, 20(%rbx)
	movl	-80(%rbp), %eax
	movl	$1, 216(%rbx)
	movl	%eax, 100(%rbx)
	movb	$1, 126(%rbx)
.L51:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movsd	.LC4(%rip), %xmm1
	movq	%rdx, %rdi
	movq	%rdx, -72(%rbp)
	cvtsi2sdl	%r13d, %xmm0
	movsd	%xmm0, -64(%rbp)
	call	_ZN6icu_679ClockMath11floorDivideEddRd@PLT
	cvttsd2sil	-64(%rbp), %r13d
	movq	-72(%rbp), %rdx
	cvttsd2sil	%xmm0, %eax
	addl	%eax, %r12d
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L63:
	cmpl	100(%rbx), %r14d
	jne	.L55
	jmp	.L56
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3135:
	.size	_ZNK6icu_6715ChineseCalendar23handleComputeMonthStartEiia, .-_ZNK6icu_6715ChineseCalendar23handleComputeMonthStartEiia
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ChineseCalendar11offsetMonthEiii
	.type	_ZN6icu_6715ChineseCalendar11offsetMonthEiii, @function
_ZN6icu_6715ChineseCalendar11offsetMonthEiii:
.LFB3150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movl	%esi, %r8d
	cvtsi2sdl	%ecx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$16, %rsp
	subsd	.LC1(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	mulsd	_ZN6icu_6718CalendarAstronomer13SYNODIC_MONTHE(%rip), %xmm0
	movq	(%rdi), %rax
	movl	$0, -44(%rbp)
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm0, %xmm0
	addl	%r8d, %esi
	cvtsi2sdl	%esi, %xmm0
	movl	$1, %esi
	call	*408(%rax)
	leal	2440587(%rbx,%rax), %r13d
	cmpl	$29, %ebx
	jle	.L66
	leal	-1(%r13), %edx
	movl	$20, %esi
	leaq	-44(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode@PLT
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jg	.L65
	movq	(%r12), %rax
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	*176(%rax)
	cmpl	%ebx, %eax
	jge	.L71
	.p2align 4,,10
	.p2align 3
.L65:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L72
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L65
	.p2align 4,,10
	.p2align 3
.L66:
	movl	%r13d, %edx
	movl	$20, %esi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	jmp	.L65
.L72:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3150:
	.size	_ZN6icu_6715ChineseCalendar11offsetMonthEiii, .-_ZN6icu_6715ChineseCalendar11offsetMonthEiii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar14inDaylightTimeER10UErrorCode
	.type	_ZNK6icu_6715ChineseCalendar14inDaylightTimeER10UErrorCode, @function
_ZNK6icu_6715ChineseCalendar14inDaylightTimeER10UErrorCode:
.LFB3151:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.L74
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK6icu_678Calendar11getTimeZoneEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*72(%rax)
	testb	%al, %al
	jne	.L85
	xorl	%eax, %eax
.L73:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode@PLT
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L73
	movl	76(%r12), %eax
	testl	%eax, %eax
	setne	%al
	jmp	.L73
	.cfi_endproc
.LFE3151:
	.size	_ZNK6icu_6715ChineseCalendar14inDaylightTimeER10UErrorCode, .-_ZNK6icu_6715ChineseCalendar14inDaylightTimeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ChineseCalendar21handleGetExtendedYearEv
	.type	_ZN6icu_6715ChineseCalendar21handleGetExtendedYearEv, @function
_ZN6icu_6715ChineseCalendar21handleGetExtendedYearEv:
.LFB3131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_678Calendar11newestStampE19UCalendarDateFieldsS1_i@PLT
	movl	%eax, %r8d
	movl	204(%rbx), %eax
	cmpl	%eax, %r8d
	jg	.L87
	movl	$1, %r8d
	testl	%eax, %eax
	jle	.L86
	movl	88(%rbx), %r8d
.L86:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movl	128(%rbx), %esi
	xorl	%edx, %edx
	testl	%esi, %esi
	jle	.L89
	movl	12(%rbx), %eax
	subl	$1, %eax
	imull	$60, %eax, %edx
.L89:
	movl	132(%rbx), %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	jle	.L90
	movl	16(%rbx), %eax
.L90:
	addl	%edx, %eax
	subl	612(%rbx), %eax
	addq	$8, %rsp
	leal	-2636(%rax), %r8d
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3131:
	.size	_ZN6icu_6715ChineseCalendar21handleGetExtendedYearEv, .-_ZN6icu_6715ChineseCalendar21handleGetExtendedYearEv
	.align 2
	.p2align 4
	.type	_ZN6icu_6715ChineseCalendar4rollE19UCalendarDateFieldsiR10UErrorCode.part.0, @function
_ZN6icu_6715ChineseCalendar4rollE19UCalendarDateFieldsiR10UErrorCode.part.0:
.LFB4076:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movl	$5, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L108
.L95:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	%rbx, %rdx
	movl	$20, %esi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	(%rbx), %ecx
	movl	%eax, %r15d
	testl	%ecx, %ecx
	jg	.L95
	movq	%rbx, %rdx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	(%rbx), %edx
	movl	%eax, %r8d
	testl	%edx, %edx
	jg	.L95
	movl	%r15d, %eax
	movl	$12, %ecx
	subl	%r14d, %eax
	cmpb	$0, 610(%r12)
	leal	-2440587(%rax), %r15d
	jne	.L109
.L99:
	leal	0(%r13,%r8), %eax
	cltd
	idivl	%ecx
	addl	%edx, %ecx
	testl	%edx, %edx
	cmovns	%edx, %ecx
	cmpl	%r8d, %ecx
	je	.L95
	movq	(%r12), %rax
	movl	%r14d, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	subl	%r8d, %ecx
	movq	464(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L109:
	.cfi_restore_state
	movq	%rbx, %rdx
	movl	$22, %esi
	movq	%r12, %rdi
	movl	%r8d, -52(%rbp)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	-52(%rbp), %r8d
	cmpl	$1, %eax
	je	.L110
	pxor	%xmm0, %xmm0
	movl	%r15d, %ecx
	movl	%r8d, -52(%rbp)
	movl	$1, %esi
	cvtsi2sdl	%r8d, %xmm0
	subsd	.LC1(%rip), %xmm0
	movq	%r12, %rdi
	mulsd	_ZN6icu_6718CalendarAstronomer13SYNODIC_MONTHE(%rip), %xmm0
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm0, %xmm0
	subl	%eax, %ecx
	movq	(%r12), %rax
	cvtsi2sdl	%ecx, %xmm0
	call	*408(%rax)
	movl	%r15d, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	movq	(%r12), %rax
	call	*440(%rax)
	movl	-52(%rbp), %r8d
	cmpb	$1, %al
	sbbl	$-1, %r8d
.L101:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L95
	cmpb	$1, 610(%r12)
	sbbl	%ecx, %ecx
	addl	$13, %ecx
	jmp	.L99
.L110:
	addl	$1, %r8d
	jmp	.L101
	.cfi_endproc
.LFE4076:
	.size	_ZN6icu_6715ChineseCalendar4rollE19UCalendarDateFieldsiR10UErrorCode.part.0, .-_ZN6icu_6715ChineseCalendar4rollE19UCalendarDateFieldsiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ChineseCalendar20computeChineseFieldsEiiia
	.type	_ZN6icu_6715ChineseCalendar20computeChineseFieldsEiiia, @function
_ZN6icu_6715ChineseCalendar20computeChineseFieldsEiiia:
.LFB3148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movl	%edx, %esi
	subq	$56, %rsp
	movl	%ecx, -76(%rbp)
	movl	%r8d, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*400(%rax)
	movl	%eax, %r14d
	cmpl	%ebx, %eax
	jle	.L112
	movq	(%r15), %rax
	leal	-1(%r12), %esi
	movq	%r15, %rdi
	movl	%r14d, %r13d
	call	*400(%rax)
.L113:
	addl	$1, %eax
	pxor	%xmm0, %xmm0
	movl	$1, %esi
	movq	%r15, %rdi
	cvtsi2sdl	%eax, %xmm0
	movq	(%r15), %rax
	addl	$1, %r13d
	call	*408(%rax)
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%eax, %r14d
	movq	(%r15), %rax
	cvtsi2sdl	%r13d, %xmm0
	leaq	_ZNK6icu_6715ChineseCalendar20synodicMonthsBetweenEii(%rip), %r13
	call	*408(%rax)
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%eax, -72(%rbp)
	leal	1(%rbx), %eax
	cvtsi2sdl	%eax, %xmm0
	movq	(%r15), %rax
	call	*408(%rax)
	movq	(%r15), %rsi
	movl	-72(%rbp), %edx
	movl	%eax, %ecx
	movq	416(%rsi), %rax
	cmpq	%r13, %rax
	jne	.L114
	subl	%r14d, %edx
	pxor	%xmm0, %xmm0
	movsd	.LC1(%rip), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	_ZN6icu_6718CalendarAstronomer13SYNODIC_MONTHE(%rip), %xmm0
	comisd	.LC3(%rip), %xmm0
	jb	.L142
.L115:
	addsd	%xmm1, %xmm0
	cvttsd2sil	%xmm0, %eax
.L116:
	cmpl	$12, %eax
	sete	%dl
	movb	%dl, 610(%r15)
	movq	416(%rsi), %rax
	cmpq	%r13, %rax
	jne	.L117
	movl	%ecx, %eax
	pxor	%xmm0, %xmm0
	movsd	.LC1(%rip), %xmm1
	subl	%r14d, %eax
	cvtsi2sdl	%eax, %xmm0
	divsd	_ZN6icu_6718CalendarAstronomer13SYNODIC_MONTHE(%rip), %xmm0
	comisd	.LC3(%rip), %xmm0
	jb	.L143
.L118:
	addsd	%xmm1, %xmm0
	cvttsd2sil	%xmm0, %r13d
.L119:
	testb	%dl, %dl
	je	.L120
	movq	(%r15), %rax
	movl	%ecx, -72(%rbp)
	movl	%ecx, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*440(%rax)
	movl	-72(%rbp), %ecx
	cmpb	$1, %al
	adcl	$-1, %r13d
	testl	%r13d, %r13d
	leal	12(%r13), %eax
	cmovle	%eax, %r13d
	cmpb	$0, 610(%r15)
	leal	-1(%r13), %r8d
	je	.L124
	movq	(%r15), %rax
	movl	%r8d, -88(%rbp)
	movl	%ecx, %esi
	movq	%r15, %rdi
	call	*432(%rax)
	movl	-72(%rbp), %ecx
	movl	-88(%rbp), %r8d
	testb	%al, %al
	je	.L124
	movq	(%r15), %rdx
	leal	-25(%rcx), %eax
	pxor	%xmm0, %xmm0
	movl	%r8d, -80(%rbp)
	xorl	%esi, %esi
	cvtsi2sdl	%eax, %xmm0
	movq	%r15, %rdi
	movq	440(%rdx), %r10
	movq	%r10, -88(%rbp)
	call	*408(%rdx)
	movq	-88(%rbp), %r10
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	%eax, %edx
	call	*%r10
	movl	-80(%rbp), %r8d
	movl	-72(%rbp), %ecx
	movb	$1, 106(%r15)
	movl	$1, 136(%r15)
	testb	%al, %al
	movl	%r8d, 20(%r15)
	je	.L144
	.p2align 4,,10
	.p2align 3
.L141:
	xorl	%eax, %eax
.L126:
	cmpb	$0, -68(%rbp)
	movl	%eax, 100(%r15)
	movl	$1, 216(%r15)
	movb	$1, 126(%r15)
	je	.L111
	movl	%r12d, %edx
	leal	2636(%r12), %eax
	subl	612(%r15), %edx
	cmpl	$10, %r13d
	jle	.L128
	cmpl	$5, -76(%rbp)
	jg	.L128
	leal	2635(%r12), %eax
.L129:
	pxor	%xmm0, %xmm0
	leaq	-60(%rbp), %rsi
	movl	$60, %edi
	movl	%ecx, -68(%rbp)
	cvtsi2sdl	%eax, %xmm0
	movl	%edx, 88(%r15)
	movl	$1, 204(%r15)
	movb	$1, 123(%r15)
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	movl	-68(%rbp), %ecx
	movb	$1, 109(%r15)
	movl	%r12d, %esi
	addl	$1, %eax
	movq	%r15, %rdi
	movl	$1, 148(%r15)
	movl	%eax, 12(%r15)
	movl	-60(%rbp), %eax
	addl	$1, %eax
	movl	%eax, 16(%r15)
	movabsq	$4294967297, %rax
	movq	%rax, 128(%r15)
	movl	$257, %eax
	movw	%ax, 104(%r15)
	movl	%ebx, %eax
	subl	%ecx, %eax
	addl	$1, %eax
	movl	%eax, 32(%r15)
	movq	(%r15), %rax
	call	*456(%rax)
	cmpl	%eax, %ebx
	jge	.L130
	movq	(%r15), %rax
	leal	-1(%r12), %esi
	movq	%r15, %rdi
	call	*456(%rax)
.L130:
	subl	%eax, %ebx
	movb	$1, 110(%r15)
	movl	$1, 152(%r15)
	addl	$1, %ebx
	movl	%ebx, 36(%r15)
.L111:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	leal	-1(%r13), %r8d
	testl	%r13d, %r13d
	jg	.L124
	leal	11(%r13), %r8d
	addl	$12, %r13d
.L124:
	movl	%r8d, 20(%r15)
	movl	$1, 136(%r15)
	movb	$1, 106(%r15)
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L112:
	movq	(%r15), %rax
	leal	1(%r12), %esi
	movq	%r15, %rdi
	call	*400(%rax)
	movl	%eax, %r13d
	movl	%r14d, %eax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L143:
	movsd	.LC2(%rip), %xmm1
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L142:
	movsd	.LC2(%rip), %xmm1
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L128:
	addl	$1, %edx
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L117:
	movl	%ecx, %edx
	movl	%ecx, -72(%rbp)
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*%rax
	movzbl	610(%r15), %edx
	movl	-72(%rbp), %ecx
	movl	%eax, %r13d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L114:
	movl	%ecx, -72(%rbp)
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*%rax
	movq	(%r15), %rsi
	movl	-72(%rbp), %ecx
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$1, %eax
	jmp	.L126
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3148:
	.size	_ZN6icu_6715ChineseCalendar20computeChineseFieldsEiiia, .-_ZN6icu_6715ChineseCalendar20computeChineseFieldsEiiia
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar7newYearEi
	.type	_ZNK6icu_6715ChineseCalendar7newYearEi, @function
_ZNK6icu_6715ChineseCalendar7newYearEi:
.LFB3149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-60(%rbp), %r15
	movl	%esi, %r14d
	pushq	%r13
	movq	%r15, %rdx
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	_ZL28gChineseCalendarNewYearCache(%rip), %rdi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L147
	movq	0(%r13), %rax
	leal	-1(%r14), %esi
	movq	%r13, %rdi
	call	*400(%rax)
	movl	%r14d, %esi
	movq	%r13, %rdi
	movl	%eax, %r12d
	movq	0(%r13), %rax
	addl	$1, %r12d
	call	*400(%rax)
	pxor	%xmm0, %xmm0
	movl	$1, %esi
	movq	%r13, %rdi
	movl	%eax, %ebx
	movq	0(%r13), %rax
	cvtsi2sdl	%r12d, %xmm0
	addl	$1, %ebx
	call	*408(%rax)
	pxor	%xmm0, %xmm0
	movl	$1, %esi
	movq	%r13, %rdi
	movl	%eax, %r8d
	leal	25(%rax), %eax
	cvtsi2sdl	%eax, %xmm0
	movq	0(%r13), %rax
	movl	%r8d, -68(%rbp)
	call	*408(%rax)
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	%eax, %r12d
	movq	0(%r13), %rax
	cvtsi2sdl	%ebx, %xmm0
	call	*408(%rax)
	leaq	_ZNK6icu_6715ChineseCalendar20synodicMonthsBetweenEii(%rip), %rcx
	movl	-68(%rbp), %r8d
	movl	%eax, %edx
	movq	0(%r13), %rax
	movq	416(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L148
	subl	%r8d, %edx
	pxor	%xmm0, %xmm0
	movsd	.LC1(%rip), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	_ZN6icu_6718CalendarAstronomer13SYNODIC_MONTHE(%rip), %xmm0
	comisd	.LC3(%rip), %xmm0
	jnb	.L149
	movsd	.LC2(%rip), %xmm1
.L149:
	addsd	%xmm1, %xmm0
	cvttsd2sil	%xmm0, %eax
	cmpl	$12, %eax
	je	.L159
.L151:
	movq	%r15, %rcx
	movl	%r12d, %edx
	leaq	_ZL28gChineseCalendarNewYearCache(%rip), %rdi
	movl	%r14d, %esi
	call	_ZN6icu_6713CalendarCache3putEPPS0_iiR10UErrorCode@PLT
.L147:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%eax, %r12d
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L160
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movl	%r8d, -68(%rbp)
	movl	%r8d, %esi
	movq	%r13, %rdi
	call	*%rax
	movl	-68(%rbp), %r8d
	cmpl	$12, %eax
	jne	.L151
	.p2align 4,,10
	.p2align 3
.L159:
	movq	0(%r13), %rax
	movl	%r8d, %esi
	movq	%r13, %rdi
	call	*432(%rax)
	testb	%al, %al
	jne	.L153
	movq	0(%r13), %rax
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	*432(%rax)
	testb	%al, %al
	je	.L151
.L153:
	movq	0(%r13), %rax
	addl	$25, %r12d
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	cvtsi2sdl	%r12d, %xmm0
	movl	$1, %esi
	call	*408(%rax)
	movl	%eax, %r12d
	jmp	.L151
.L160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3149:
	.size	_ZNK6icu_6715ChineseCalendar7newYearEi, .-_ZNK6icu_6715ChineseCalendar7newYearEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar14majorSolarTermEi
	.type	_ZNK6icu_6715ChineseCalendar14majorSolarTermEi, @function
_ZNK6icu_6715ChineseCalendar14majorSolarTermEi:
.LFB3145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	_ZL9astroLock(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	_ZL21gChineseCalendarAstro(%rip), %r13
	testq	%r13, %r13
	je	.L176
.L162:
	movq	616(%r12), %rdi
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	mulsd	.LC5(%rip), %xmm0
	testq	%rdi, %rdi
	je	.L164
	movq	(%rdi), %rax
	xorl	%esi, %esi
	movsd	%xmm0, -72(%rbp)
	leaq	-48(%rbp), %rcx
	movl	$0, -44(%rbp)
	leaq	-52(%rbp), %rdx
	leaq	-44(%rbp), %r8
	call	*48(%rax)
	movl	-44(%rbp), %eax
	movsd	-72(%rbp), %xmm0
	testl	%eax, %eax
	jle	.L177
.L164:
	subsd	.LC6(%rip), %xmm0
.L166:
	movq	%r13, %rdi
	call	_ZN6icu_6718CalendarAstronomer7setTimeEd@PLT
	movq	_ZL21gChineseCalendarAstro(%rip), %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEv@PLT
	leaq	_ZL9astroLock(%rip), %rdi
	movsd	%xmm0, -72(%rbp)
	call	umtx_unlock_67@PLT
	movsd	-72(%rbp), %xmm0
	mulsd	.LC7(%rip), %xmm0
	divsd	_ZN6icu_6718CalendarAstronomer2PIE(%rip), %xmm0
	cvttsd2sil	%xmm0, %edx
	addl	$2, %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$715827883, %rax, %rax
	sarl	$31, %ecx
	sarq	$33, %rax
	subl	%ecx, %eax
	leal	(%rax,%rax,2), %eax
	sall	$2, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	leal	12(%rdx), %edx
	testl	%eax, %eax
	cmovle	%edx, %eax
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L178
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movl	-48(%rbp), %eax
	pxor	%xmm1, %xmm1
	addl	-52(%rbp), %eax
	cvtsi2sdl	%eax, %xmm1
	subsd	%xmm1, %xmm0
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$136, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L163
	movq	%rax, %rdi
	call	_ZN6icu_6718CalendarAstronomerC1Ev@PLT
.L163:
	leaq	calendar_chinese_cleanup(%rip), %rsi
	movl	$9, %edi
	movq	%r13, _ZL21gChineseCalendarAstro(%rip)
	call	ucln_i18n_registerCleanup_67@PLT
	movq	_ZL21gChineseCalendarAstro(%rip), %r13
	jmp	.L162
.L178:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3145:
	.size	_ZNK6icu_6715ChineseCalendar14majorSolarTermEi, .-_ZNK6icu_6715ChineseCalendar14majorSolarTermEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar11newMoonNearEda
	.type	_ZNK6icu_6715ChineseCalendar11newMoonNearEda, @function
_ZNK6icu_6715ChineseCalendar11newMoonNearEda:
.LFB3143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	_ZL9astroLock(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$48, %rsp
	movsd	%xmm0, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	_ZL21gChineseCalendarAstro(%rip), %r14
	testq	%r14, %r14
	je	.L199
.L180:
	movq	616(%r12), %rdi
	movsd	.LC5(%rip), %xmm0
	leaq	-48(%rbp), %r13
	mulsd	-72(%rbp), %xmm0
	testq	%rdi, %rdi
	je	.L182
	movq	(%rdi), %rax
	leaq	-48(%rbp), %r13
	leaq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movsd	%xmm0, -72(%rbp)
	leaq	-52(%rbp), %rcx
	movq	%r13, %r8
	movl	$0, -48(%rbp)
	call	*48(%rax)
	movl	-48(%rbp), %edx
	movsd	-72(%rbp), %xmm0
	testl	%edx, %edx
	jle	.L200
.L182:
	subsd	.LC6(%rip), %xmm0
.L184:
	movq	%r14, %rdi
	call	_ZN6icu_6718CalendarAstronomer7setTimeEd@PLT
	movq	_ZL21gChineseCalendarAstro(%rip), %r14
	call	_ZN6icu_6718CalendarAstronomer8NEW_MOONEv@PLT
	movsbl	%bl, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movsd	%xmm0, -48(%rbp)
	call	_ZN6icu_6718CalendarAstronomer11getMoonTimeERKNS0_7MoonAgeEa@PLT
	leaq	_ZL9astroLock(%rip), %rdi
	movsd	%xmm0, -72(%rbp)
	call	umtx_unlock_67@PLT
	movq	616(%r12), %rdi
	testq	%rdi, %rdi
	je	.L185
	movq	(%rdi), %rax
	movl	$0, -48(%rbp)
	leaq	-52(%rbp), %rcx
	xorl	%esi, %esi
	movsd	-72(%rbp), %xmm0
	leaq	-56(%rbp), %rdx
	movq	%r13, %r8
	call	*48(%rax)
	movl	-48(%rbp), %eax
	testl	%eax, %eax
	jle	.L201
.L185:
	movsd	-72(%rbp), %xmm0
	addsd	.LC6(%rip), %xmm0
	divsd	.LC5(%rip), %xmm0
	call	uprv_floor_67@PLT
.L187:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	cvttsd2sil	%xmm0, %eax
	jne	.L202
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movl	-52(%rbp), %eax
	pxor	%xmm0, %xmm0
	addl	-56(%rbp), %eax
	cvtsi2sdl	%eax, %xmm0
	addsd	-72(%rbp), %xmm0
	divsd	.LC5(%rip), %xmm0
	call	uprv_floor_67@PLT
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L200:
	movl	-52(%rbp), %eax
	pxor	%xmm1, %xmm1
	addl	-56(%rbp), %eax
	cvtsi2sdl	%eax, %xmm1
	subsd	%xmm1, %xmm0
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L199:
	movl	$136, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L181
	movq	%rax, %rdi
	call	_ZN6icu_6718CalendarAstronomerC1Ev@PLT
.L181:
	leaq	calendar_chinese_cleanup(%rip), %rsi
	movl	$9, %edi
	movq	%r13, _ZL21gChineseCalendarAstro(%rip)
	call	ucln_i18n_registerCleanup_67@PLT
	movq	_ZL21gChineseCalendarAstro(%rip), %r14
	jmp	.L180
.L202:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3143:
	.size	_ZNK6icu_6715ChineseCalendar11newMoonNearEda, .-_ZNK6icu_6715ChineseCalendar11newMoonNearEda
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar14winterSolsticeEi
	.type	_ZNK6icu_6715ChineseCalendar14winterSolsticeEi, @function
_ZNK6icu_6715ChineseCalendar14winterSolsticeEi:
.LFB3142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-56(%rbp), %r14
	movl	%esi, %r13d
	pushq	%r12
	movq	%r14, %rdx
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	_ZL35gChineseCalendarWinterSolsticeCache(%rip), %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -56(%rbp)
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L226
.L204:
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%eax, %r12d
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L227
	addq	$48, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	movl	%r13d, %edi
	movl	$1, %edx
	movl	$11, %esi
	call	_ZN6icu_675Grego11fieldsToDayEiii@PLT
	movq	616(%rbx), %rdi
	mulsd	.LC5(%rip), %xmm0
	testq	%rdi, %rdi
	je	.L205
	movq	(%rdi), %rax
	leaq	-48(%rbp), %rcx
	xorl	%esi, %esi
	movsd	%xmm0, -72(%rbp)
	movl	$0, -44(%rbp)
	leaq	-52(%rbp), %rdx
	leaq	-44(%rbp), %r8
	call	*48(%rax)
	movl	-44(%rbp), %ecx
	movsd	-72(%rbp), %xmm0
	testl	%ecx, %ecx
	jle	.L228
.L205:
	subsd	.LC6(%rip), %xmm0
.L207:
	leaq	_ZL9astroLock(%rip), %rdi
	movsd	%xmm0, -72(%rbp)
	call	umtx_lock_67@PLT
	movq	_ZL21gChineseCalendarAstro(%rip), %rdi
	movsd	-72(%rbp), %xmm0
	testq	%rdi, %rdi
	je	.L229
.L208:
	call	_ZN6icu_6718CalendarAstronomer7setTimeEd@PLT
	movq	_ZL21gChineseCalendarAstro(%rip), %r12
	call	_ZN6icu_6718CalendarAstronomer15WINTER_SOLSTICEEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer10getSunTimeEda@PLT
	leaq	_ZL9astroLock(%rip), %rdi
	movsd	%xmm0, -72(%rbp)
	call	umtx_unlock_67@PLT
	movq	616(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L210
	movq	(%rdi), %rax
	leaq	-52(%rbp), %rdx
	movl	$0, -44(%rbp)
	xorl	%esi, %esi
	movsd	-72(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	leaq	-44(%rbp), %r8
	call	*48(%rax)
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jle	.L230
.L210:
	movsd	-72(%rbp), %xmm0
	addsd	.LC6(%rip), %xmm0
	divsd	.LC5(%rip), %xmm0
	call	uprv_floor_67@PLT
.L212:
	cvttsd2sil	%xmm0, %r12d
	movq	%r14, %rcx
	movl	%r13d, %esi
	leaq	_ZL35gChineseCalendarWinterSolsticeCache(%rip), %rdi
	movl	%r12d, %edx
	call	_ZN6icu_6713CalendarCache3putEPPS0_iiR10UErrorCode@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L228:
	movl	-48(%rbp), %eax
	pxor	%xmm1, %xmm1
	addl	-52(%rbp), %eax
	cvtsi2sdl	%eax, %xmm1
	subsd	%xmm1, %xmm0
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L230:
	movl	-48(%rbp), %eax
	pxor	%xmm0, %xmm0
	addl	-52(%rbp), %eax
	cvtsi2sdl	%eax, %xmm0
	addsd	-72(%rbp), %xmm0
	divsd	.LC5(%rip), %xmm0
	call	uprv_floor_67@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$136, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movsd	-72(%rbp), %xmm0
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L209
	movq	%rax, %rdi
	call	_ZN6icu_6718CalendarAstronomerC1Ev@PLT
	movsd	-72(%rbp), %xmm0
.L209:
	movl	$9, %edi
	leaq	calendar_chinese_cleanup(%rip), %rsi
	movsd	%xmm0, -72(%rbp)
	movq	%r12, _ZL21gChineseCalendarAstro(%rip)
	call	ucln_i18n_registerCleanup_67@PLT
	movq	_ZL21gChineseCalendarAstro(%rip), %rdi
	movsd	-72(%rbp), %xmm0
	jmp	.L208
.L227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3142:
	.size	_ZNK6icu_6715ChineseCalendar14winterSolsticeEi, .-_ZNK6icu_6715ChineseCalendar14winterSolsticeEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ChineseCalendarC2ERKNS_6LocaleEiPKNS_8TimeZoneER10UErrorCode
	.type	_ZN6icu_6715ChineseCalendarC2ERKNS_6LocaleEiPKNS_8TimeZoneER10UErrorCode, @function
_ZN6icu_6715ChineseCalendarC2ERKNS_6LocaleEiPKNS_8TimeZoneER10UErrorCode:
.LFB3118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r12, %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6715ChineseCalendarE(%rip), %rax
	movl	%r14d, 612(%r12)
	movq	%rbx, 616(%r12)
	movq	%rax, (%r12)
	movb	$0, 610(%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE3118:
	.size	_ZN6icu_6715ChineseCalendarC2ERKNS_6LocaleEiPKNS_8TimeZoneER10UErrorCode, .-_ZN6icu_6715ChineseCalendarC2ERKNS_6LocaleEiPKNS_8TimeZoneER10UErrorCode
	.globl	_ZN6icu_6715ChineseCalendarC1ERKNS_6LocaleEiPKNS_8TimeZoneER10UErrorCode
	.set	_ZN6icu_6715ChineseCalendarC1ERKNS_6LocaleEiPKNS_8TimeZoneER10UErrorCode,_ZN6icu_6715ChineseCalendarC2ERKNS_6LocaleEiPKNS_8TimeZoneER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ChineseCalendarC2ERKS0_
	.type	_ZN6icu_6715ChineseCalendarC2ERKS0_, @function
_ZN6icu_6715ChineseCalendarC2ERKS0_:
.LFB3121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_678CalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6715ChineseCalendarE(%rip), %rax
	movq	%rax, (%rbx)
	movzbl	610(%r12), %eax
	movb	%al, 610(%rbx)
	movl	612(%r12), %eax
	movl	%eax, 612(%rbx)
	movq	616(%r12), %rax
	movq	%rax, 616(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3121:
	.size	_ZN6icu_6715ChineseCalendarC2ERKS0_, .-_ZN6icu_6715ChineseCalendarC2ERKS0_
	.globl	_ZN6icu_6715ChineseCalendarC1ERKS0_
	.set	_ZN6icu_6715ChineseCalendarC1ERKS0_,_ZN6icu_6715ChineseCalendarC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar12daysToMillisEd
	.type	_ZNK6icu_6715ChineseCalendar12daysToMillisEd, @function
_ZNK6icu_6715ChineseCalendar12daysToMillisEd:
.LFB3140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	616(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	mulsd	.LC5(%rip), %xmm0
	testq	%rdi, %rdi
	je	.L236
	movq	(%rdi), %rax
	xorl	%esi, %esi
	movsd	%xmm0, -40(%rbp)
	leaq	-16(%rbp), %rcx
	movl	$0, -12(%rbp)
	leaq	-20(%rbp), %rdx
	leaq	-12(%rbp), %r8
	call	*48(%rax)
	movl	-12(%rbp), %eax
	movsd	-40(%rbp), %xmm0
	testl	%eax, %eax
	jle	.L244
.L236:
	subsd	.LC6(%rip), %xmm0
.L235:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L245
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movl	-16(%rbp), %eax
	pxor	%xmm1, %xmm1
	addl	-20(%rbp), %eax
	cvtsi2sdl	%eax, %xmm1
	subsd	%xmm1, %xmm0
	jmp	.L235
.L245:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3140:
	.size	_ZNK6icu_6715ChineseCalendar12daysToMillisEd, .-_ZNK6icu_6715ChineseCalendar12daysToMillisEd
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar12millisToDaysEd
	.type	_ZNK6icu_6715ChineseCalendar12millisToDaysEd, @function
_ZNK6icu_6715ChineseCalendar12millisToDaysEd:
.LFB3141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	616(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L247
	movq	(%rdi), %rax
	xorl	%esi, %esi
	movsd	%xmm0, -40(%rbp)
	leaq	-16(%rbp), %rcx
	movl	$0, -12(%rbp)
	leaq	-20(%rbp), %rdx
	leaq	-12(%rbp), %r8
	call	*48(%rax)
	movl	-12(%rbp), %eax
	movsd	-40(%rbp), %xmm0
	testl	%eax, %eax
	jle	.L255
.L247:
	addsd	.LC6(%rip), %xmm0
	divsd	.LC5(%rip), %xmm0
	call	uprv_floor_67@PLT
.L246:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L256
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movl	-16(%rbp), %eax
	pxor	%xmm1, %xmm1
	addl	-20(%rbp), %eax
	cvtsi2sdl	%eax, %xmm1
	addsd	%xmm1, %xmm0
	divsd	.LC5(%rip), %xmm0
	call	uprv_floor_67@PLT
	jmp	.L246
.L256:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3141:
	.size	_ZNK6icu_6715ChineseCalendar12millisToDaysEd, .-_ZNK6icu_6715ChineseCalendar12millisToDaysEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ChineseCalendar16getStaticClassIDEv
	.type	_ZN6icu_6715ChineseCalendar16getStaticClassIDEv, @function
_ZN6icu_6715ChineseCalendar16getStaticClassIDEv:
.LFB3158:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6715ChineseCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3158:
	.size	_ZN6icu_6715ChineseCalendar16getStaticClassIDEv, .-_ZN6icu_6715ChineseCalendar16getStaticClassIDEv
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC8:
	.string	"C"
	.string	"H"
	.string	"I"
	.string	"N"
	.string	"A"
	.string	"_"
	.string	"Z"
	.string	"O"
	.string	"N"
	.string	"E"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar26getChineseCalZoneAstroCalcEv
	.type	_ZNK6icu_6715ChineseCalendar26getChineseCalZoneAstroCalcEv, @function
_ZNK6icu_6715ChineseCalendar26getChineseCalZoneAstroCalcEv:
.LFB3129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZL37gChineseCalendarZoneAstroCalcInitOnce(%rip), %eax
	cmpl	$2, %eax
	je	.L260
	leaq	_ZL37gChineseCalendarZoneAstroCalcInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L260
	leaq	-96(%rbp), %r12
	leaq	-104(%rbp), %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	.LC8(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L262
	movq	%r12, %rdx
	movl	$28800000, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6714SimpleTimeZoneC1EiRKNS_13UnicodeStringE@PLT
.L262:
	movq	%r12, %rdi
	movq	%rbx, _ZL29gChineseCalendarZoneAstroCalc(%rip)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$9, %edi
	leaq	calendar_chinese_cleanup(%rip), %rsi
	call	ucln_i18n_registerCleanup_67@PLT
	leaq	_ZL37gChineseCalendarZoneAstroCalcInitOnce(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L260:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	_ZL29gChineseCalendarZoneAstroCalc(%rip), %rax
	jne	.L271
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L271:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3129:
	.size	_ZNK6icu_6715ChineseCalendar26getChineseCalZoneAstroCalcEv, .-_ZNK6icu_6715ChineseCalendar26getChineseCalZoneAstroCalcEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ChineseCalendarC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6715ChineseCalendarC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6715ChineseCalendarC2ERKNS_6LocaleER10UErrorCode:
.LFB3115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6715ChineseCalendarE(%rip), %rax
	movb	$0, 610(%r12)
	movq	%rax, (%r12)
	movl	$-2636, 612(%r12)
	movl	_ZL37gChineseCalendarZoneAstroCalcInitOnce(%rip), %eax
	cmpl	$2, %eax
	je	.L274
	leaq	_ZL37gChineseCalendarZoneAstroCalcInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L274
	leaq	-112(%rbp), %r15
	leaq	-120(%rbp), %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	.LC8(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L276
	movq	%r15, %rdx
	movl	$28800000, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6714SimpleTimeZoneC1EiRKNS_13UnicodeStringE@PLT
.L276:
	movq	%r15, %rdi
	movq	%r14, _ZL29gChineseCalendarZoneAstroCalc(%rip)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$9, %edi
	leaq	calendar_chinese_cleanup(%rip), %rsi
	call	ucln_i18n_registerCleanup_67@PLT
	leaq	_ZL37gChineseCalendarZoneAstroCalcInitOnce(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L274:
	movq	_ZL29gChineseCalendarZoneAstroCalc(%rip), %rax
	movq	%rax, 616(%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L285
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L285:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3115:
	.size	_ZN6icu_6715ChineseCalendarC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6715ChineseCalendarC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6715ChineseCalendarC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6715ChineseCalendarC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6715ChineseCalendarC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ChineseCalendar3addE19UCalendarDateFieldsiR10UErrorCode
	.type	_ZN6icu_6715ChineseCalendar3addE19UCalendarDateFieldsiR10UErrorCode, @function
_ZN6icu_6715ChineseCalendar3addE19UCalendarDateFieldsiR10UErrorCode:
.LFB3136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edx, %r12d
	subq	$16, %rsp
	cmpl	$2, %esi
	jne	.L287
	testl	%edx, %edx
	jne	.L292
.L286:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	movq	%rcx, %rdx
	movl	$5, %esi
	movq	%rcx, -32(%rbp)
	movq	%rdi, -24(%rbp)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movq	-32(%rbp), %rcx
	movq	-24(%rbp), %rdi
	movl	%eax, %r13d
	movl	(%rcx), %esi
	testl	%esi, %esi
	jg	.L286
	movq	%rcx, %rdx
	movl	$20, %esi
	movq	%rcx, -24(%rbp)
	movq	%rdi, -32(%rbp)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movq	-24(%rbp), %rcx
	movl	(%rcx), %edx
	testl	%edx, %edx
	jg	.L286
	movq	-32(%rbp), %rdi
	subl	%r13d, %eax
	movl	%r12d, %ecx
	movl	%r13d, %edx
	leal	-2440587(%rax), %esi
	movq	(%rdi), %rax
	movq	464(%rax), %rax
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE3136:
	.size	_ZN6icu_6715ChineseCalendar3addE19UCalendarDateFieldsiR10UErrorCode, .-_ZN6icu_6715ChineseCalendar3addE19UCalendarDateFieldsiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ChineseCalendar3addENS_8Calendar11EDateFieldsEiR10UErrorCode
	.type	_ZN6icu_6715ChineseCalendar3addENS_8Calendar11EDateFieldsEiR10UErrorCode, @function
_ZN6icu_6715ChineseCalendar3addENS_8Calendar11EDateFieldsEiR10UErrorCode:
.LFB3137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edx, %r12d
	leaq	_ZN6icu_6715ChineseCalendar3addE19UCalendarDateFieldsiR10UErrorCode(%rip), %rdx
	subq	$16, %rsp
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L294
	cmpl	$2, %esi
	jne	.L295
	testl	%r12d, %r12d
	jne	.L300
.L293:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	addq	$16, %rsp
	movl	%r12d, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	addq	$16, %rsp
	movl	%r12d, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	movq	%rcx, %rdx
	movl	$5, %esi
	movq	%rcx, -32(%rbp)
	movq	%rdi, -24(%rbp)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movq	-32(%rbp), %rcx
	movq	-24(%rbp), %rdi
	movl	%eax, %r13d
	movl	(%rcx), %esi
	testl	%esi, %esi
	jg	.L293
	movq	%rcx, %rdx
	movl	$20, %esi
	movq	%rcx, -24(%rbp)
	movq	%rdi, -32(%rbp)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movq	-24(%rbp), %rcx
	movl	(%rcx), %edx
	testl	%edx, %edx
	jg	.L293
	movq	-32(%rbp), %rdi
	subl	%r13d, %eax
	movl	%r12d, %ecx
	movl	%r13d, %edx
	leal	-2440587(%rax), %esi
	movq	(%rdi), %rax
	movq	464(%rax), %rax
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE3137:
	.size	_ZN6icu_6715ChineseCalendar3addENS_8Calendar11EDateFieldsEiR10UErrorCode, .-_ZN6icu_6715ChineseCalendar3addENS_8Calendar11EDateFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ChineseCalendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.type	_ZN6icu_6715ChineseCalendar4rollE19UCalendarDateFieldsiR10UErrorCode, @function
_ZN6icu_6715ChineseCalendar4rollE19UCalendarDateFieldsiR10UErrorCode:
.LFB3138:
	.cfi_startproc
	endbr64
	movl	%edx, %r8d
	cmpl	$2, %esi
	jne	.L302
	testl	%edx, %edx
	jne	.L304
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	jmp	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L304:
	movq	%rcx, %rdx
	movl	%r8d, %esi
	jmp	_ZN6icu_6715ChineseCalendar4rollE19UCalendarDateFieldsiR10UErrorCode.part.0
	.cfi_endproc
.LFE3138:
	.size	_ZN6icu_6715ChineseCalendar4rollE19UCalendarDateFieldsiR10UErrorCode, .-_ZN6icu_6715ChineseCalendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ChineseCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode
	.type	_ZN6icu_6715ChineseCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode, @function
_ZN6icu_6715ChineseCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode:
.LFB3139:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	%edx, %r8d
	leaq	_ZN6icu_6715ChineseCalendar4rollE19UCalendarDateFieldsiR10UErrorCode(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L306
	cmpl	$2, %esi
	jne	.L307
	testl	%r8d, %r8d
	jne	.L309
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	movl	%r8d, %edx
	jmp	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L306:
	movl	%r8d, %edx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L309:
	movq	%rcx, %rdx
	movl	%r8d, %esi
	jmp	_ZN6icu_6715ChineseCalendar4rollE19UCalendarDateFieldsiR10UErrorCode.part.0
	.cfi_endproc
.LFE3139:
	.size	_ZN6icu_6715ChineseCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode, .-_ZN6icu_6715ChineseCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode
	.section	.rodata.str1.1
.LC9:
	.string	"@calendar=chinese"
	.text
	.p2align 4
	.type	_ZN6icu_67L30initializeSystemDefaultCenturyEv, @function
_ZN6icu_67L30initializeSystemDefaultCenturyEv:
.LFB3155:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-912(%rbp), %r14
	pushq	%r13
	movq	%r14, %rdi
	.cfi_offset 13, -40
	leaq	-988(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-688(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVN6icu_6715ChineseCalendarE(%rip), %rbx
	subq	$968, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -988(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	movq	%rbx, -688(%rbp)
	movb	$0, -78(%rbp)
	movl	$-2636, -76(%rbp)
	movl	_ZL37gChineseCalendarZoneAstroCalcInitOnce(%rip), %eax
	cmpl	$2, %eax
	je	.L312
	leaq	_ZL37gChineseCalendarZoneAstroCalcInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L312
	leaq	-976(%rbp), %r8
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%r8, %rdi
	leaq	.LC8(%rip), %rax
	leaq	-984(%rbp), %rdx
	movq	%r8, -1000(%rbp)
	movq	%rax, -984(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-1000(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L314
	movq	%r8, %rdx
	movl	$28800000, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6714SimpleTimeZoneC1EiRKNS_13UnicodeStringE@PLT
	movq	-1000(%rbp), %r8
.L314:
	movq	%r8, %rdi
	movq	%r15, _ZL29gChineseCalendarZoneAstroCalc(%rip)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$9, %edi
	leaq	calendar_chinese_cleanup(%rip), %rsi
	call	ucln_i18n_registerCleanup_67@PLT
	leaq	_ZL37gChineseCalendarZoneAstroCalcInitOnce(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L312:
	movq	_ZL29gChineseCalendarZoneAstroCalc(%rip), %rax
	movq	%rax, -72(%rbp)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	-988(%rbp), %eax
	testl	%eax, %eax
	jle	.L324
.L315:
	movq	%r12, %rdi
	movq	%rbx, -688(%rbp)
	call	_ZN6icu_678CalendarD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L325
	addq	$968, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r13, %rcx
	movl	$-80, %edx
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movsd	%xmm0, _ZN6icu_67L26gSystemDefaultCenturyStartE(%rip)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	%eax, _ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip)
	jmp	.L315
.L325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3155:
	.size	_ZN6icu_67L30initializeSystemDefaultCenturyEv, .-_ZN6icu_67L30initializeSystemDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar19defaultCenturyStartEv
	.type	_ZNK6icu_6715ChineseCalendar19defaultCenturyStartEv, @function
_ZNK6icu_6715ChineseCalendar19defaultCenturyStartEv:
.LFB3153:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L29gSystemDefaultCenturyInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L334
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L29gSystemDefaultCenturyInitOnceE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L328
	call	_ZN6icu_67L30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L29gSystemDefaultCenturyInitOnceE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L328:
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore 6
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3153:
	.size	_ZNK6icu_6715ChineseCalendar19defaultCenturyStartEv, .-_ZNK6icu_6715ChineseCalendar19defaultCenturyStartEv
	.globl	_ZNK6icu_6715ChineseCalendar30internalGetDefaultCenturyStartEv
	.set	_ZNK6icu_6715ChineseCalendar30internalGetDefaultCenturyStartEv,_ZNK6icu_6715ChineseCalendar19defaultCenturyStartEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ChineseCalendar23defaultCenturyStartYearEv
	.type	_ZNK6icu_6715ChineseCalendar23defaultCenturyStartYearEv, @function
_ZNK6icu_6715ChineseCalendar23defaultCenturyStartYearEv:
.LFB3154:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L29gSystemDefaultCenturyInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L345
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L29gSystemDefaultCenturyInitOnceE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L339
	call	_ZN6icu_67L30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L29gSystemDefaultCenturyInitOnceE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L339:
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore 6
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	ret
	.cfi_endproc
.LFE3154:
	.size	_ZNK6icu_6715ChineseCalendar23defaultCenturyStartYearEv, .-_ZNK6icu_6715ChineseCalendar23defaultCenturyStartYearEv
	.globl	_ZNK6icu_6715ChineseCalendar34internalGetDefaultCenturyStartYearEv
	.set	_ZNK6icu_6715ChineseCalendar34internalGetDefaultCenturyStartYearEv,_ZNK6icu_6715ChineseCalendar23defaultCenturyStartYearEv
	.weak	_ZTSN6icu_6715ChineseCalendarE
	.section	.rodata._ZTSN6icu_6715ChineseCalendarE,"aG",@progbits,_ZTSN6icu_6715ChineseCalendarE,comdat
	.align 16
	.type	_ZTSN6icu_6715ChineseCalendarE, @object
	.size	_ZTSN6icu_6715ChineseCalendarE, 27
_ZTSN6icu_6715ChineseCalendarE:
	.string	"N6icu_6715ChineseCalendarE"
	.weak	_ZTIN6icu_6715ChineseCalendarE
	.section	.data.rel.ro._ZTIN6icu_6715ChineseCalendarE,"awG",@progbits,_ZTIN6icu_6715ChineseCalendarE,comdat
	.align 8
	.type	_ZTIN6icu_6715ChineseCalendarE, @object
	.size	_ZTIN6icu_6715ChineseCalendarE, 24
_ZTIN6icu_6715ChineseCalendarE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715ChineseCalendarE
	.quad	_ZTIN6icu_678CalendarE
	.weak	_ZTVN6icu_6715ChineseCalendarE
	.section	.data.rel.ro._ZTVN6icu_6715ChineseCalendarE,"awG",@progbits,_ZTVN6icu_6715ChineseCalendarE,comdat
	.align 8
	.type	_ZTVN6icu_6715ChineseCalendarE, @object
	.size	_ZTVN6icu_6715ChineseCalendarE, 488
_ZTVN6icu_6715ChineseCalendarE:
	.quad	0
	.quad	_ZTIN6icu_6715ChineseCalendarE
	.quad	_ZN6icu_6715ChineseCalendarD1Ev
	.quad	_ZN6icu_6715ChineseCalendarD0Ev
	.quad	_ZNK6icu_6715ChineseCalendar17getDynamicClassIDEv
	.quad	_ZNK6icu_6715ChineseCalendar5cloneEv
	.quad	_ZNK6icu_678CalendareqERKS0_
	.quad	_ZNK6icu_678Calendar14isEquivalentToERKS0_
	.quad	_ZN6icu_6715ChineseCalendar3addENS_8Calendar11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_6715ChineseCalendar3addE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_6715ChineseCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_6715ChineseCalendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6715ChineseCalendar14inDaylightTimeER10UErrorCode
	.quad	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6715ChineseCalendar7getTypeEv
	.quad	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEv
	.quad	_ZN6icu_678Calendar11computeTimeER10UErrorCode
	.quad	_ZN6icu_678Calendar13computeFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode
	.quad	_ZNK6icu_6715ChineseCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.quad	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE
	.quad	_ZNK6icu_6715ChineseCalendar23handleComputeMonthStartEiia
	.quad	_ZNK6icu_6715ChineseCalendar20handleGetMonthLengthEii
	.quad	_ZNK6icu_678Calendar19handleGetYearLengthEi
	.quad	_ZN6icu_6715ChineseCalendar21handleGetExtendedYearEv
	.quad	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields
	.quad	_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii
	.quad	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6715ChineseCalendar23getFieldResolutionTableEv
	.quad	_ZN6icu_6715ChineseCalendar19handleComputeFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar21getDefaultMonthInYearEi
	.quad	_ZN6icu_678Calendar20getDefaultDayInMonthEii
	.quad	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6715ChineseCalendar18haveDefaultCenturyEv
	.quad	_ZNK6icu_6715ChineseCalendar19defaultCenturyStartEv
	.quad	_ZNK6icu_6715ChineseCalendar23defaultCenturyStartYearEv
	.quad	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode
	.quad	_ZN6icu_678Calendar14setRelatedYearEi
	.quad	_ZNK6icu_6715ChineseCalendar14winterSolsticeEi
	.quad	_ZNK6icu_6715ChineseCalendar11newMoonNearEda
	.quad	_ZNK6icu_6715ChineseCalendar20synodicMonthsBetweenEii
	.quad	_ZNK6icu_6715ChineseCalendar14majorSolarTermEi
	.quad	_ZNK6icu_6715ChineseCalendar19hasNoMajorSolarTermEi
	.quad	_ZNK6icu_6715ChineseCalendar18isLeapMonthBetweenEii
	.quad	_ZN6icu_6715ChineseCalendar20computeChineseFieldsEiiia
	.quad	_ZNK6icu_6715ChineseCalendar7newYearEi
	.quad	_ZN6icu_6715ChineseCalendar11offsetMonthEiii
	.local	_ZZN6icu_6715ChineseCalendar16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6715ChineseCalendar16getStaticClassIDEvE7classID,1,1
	.local	_ZN6icu_67L29gSystemDefaultCenturyInitOnceE
	.comm	_ZN6icu_67L29gSystemDefaultCenturyInitOnceE,8,8
	.data
	.align 4
	.type	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, @object
	.size	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, 4
_ZN6icu_67L30gSystemDefaultCenturyStartYearE:
	.long	-1
	.align 8
	.type	_ZN6icu_67L26gSystemDefaultCenturyStartE, @object
	.size	_ZN6icu_67L26gSystemDefaultCenturyStartE, 8
_ZN6icu_67L26gSystemDefaultCenturyStartE:
	.long	0
	.long	1048576
	.globl	_ZN6icu_6715ChineseCalendar23CHINESE_DATE_PRECEDENCEE
	.section	.rodata
	.align 32
	.type	_ZN6icu_6715ChineseCalendar23CHINESE_DATE_PRECEDENCEE, @object
	.size	_ZN6icu_6715ChineseCalendar23CHINESE_DATE_PRECEDENCEE, 1152
_ZN6icu_6715ChineseCalendar23CHINESE_DATE_PRECEDENCEE:
	.long	5
	.long	-1
	.zero	24
	.long	3
	.long	7
	.long	-1
	.zero	20
	.long	4
	.long	7
	.long	-1
	.zero	20
	.long	8
	.long	7
	.long	-1
	.zero	20
	.long	3
	.long	18
	.long	-1
	.zero	20
	.long	4
	.long	18
	.long	-1
	.zero	20
	.long	8
	.long	18
	.long	-1
	.zero	20
	.long	6
	.long	-1
	.zero	24
	.long	37
	.long	22
	.long	-1
	.zero	20
	.long	-1
	.zero	28
	.zero	64
	.long	3
	.long	-1
	.zero	24
	.long	4
	.long	-1
	.zero	24
	.long	8
	.long	-1
	.zero	24
	.long	40
	.long	7
	.long	-1
	.zero	20
	.long	40
	.long	18
	.long	-1
	.zero	20
	.long	-1
	.zero	28
	.zero	192
	.long	-1
	.zero	28
	.zero	352
	.align 32
	.type	_ZN6icu_67L6LIMITSE, @object
	.size	_ZN6icu_67L6LIMITSE, 368
_ZN6icu_67L6LIMITSE:
	.long	1
	.long	1
	.long	83333
	.long	83333
	.long	1
	.long	1
	.long	60
	.long	60
	.long	0
	.long	0
	.long	11
	.long	11
	.long	1
	.long	1
	.long	50
	.long	55
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	1
	.long	1
	.long	29
	.long	30
	.long	1
	.long	1
	.long	353
	.long	385
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	5
	.long	5
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-5000000
	.long	-5000000
	.long	5000000
	.long	5000000
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-5000000
	.long	-5000000
	.long	5000000
	.long	5000000
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	0
	.long	0
	.long	1
	.long	1
	.local	_ZL37gChineseCalendarZoneAstroCalcInitOnce
	.comm	_ZL37gChineseCalendarZoneAstroCalcInitOnce,8,8
	.local	_ZL29gChineseCalendarZoneAstroCalc
	.comm	_ZL29gChineseCalendarZoneAstroCalc,8,8
	.local	_ZL28gChineseCalendarNewYearCache
	.comm	_ZL28gChineseCalendarNewYearCache,8,8
	.local	_ZL35gChineseCalendarWinterSolsticeCache
	.comm	_ZL35gChineseCalendarWinterSolsticeCache,8,8
	.local	_ZL21gChineseCalendarAstro
	.comm	_ZL21gChineseCalendarAstro,8,8
	.local	_ZL9astroLock
	.comm	_ZL9astroLock,56,32
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1071644672
	.align 8
.LC2:
	.long	0
	.long	-1075838976
	.align 8
.LC3:
	.long	0
	.long	0
	.align 8
.LC4:
	.long	0
	.long	1076363264
	.align 8
.LC5:
	.long	0
	.long	1100257648
	.align 8
.LC6:
	.long	0
	.long	1098610496
	.align 8
.LC7:
	.long	0
	.long	1075314688
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
