	.file	"csrucode.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"UTF-16BE"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_UTF_16_BE7getNameEv
	.type	_ZNK6icu_6722CharsetRecog_UTF_16_BE7getNameEv, @function
_ZNK6icu_6722CharsetRecog_UTF_16_BE7getNameEv:
.LFB12:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE12:
	.size	_ZNK6icu_6722CharsetRecog_UTF_16_BE7getNameEv, .-_ZNK6icu_6722CharsetRecog_UTF_16_BE7getNameEv
	.section	.rodata.str1.1
.LC1:
	.string	"UTF-16LE"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_UTF_16_LE7getNameEv
	.type	_ZNK6icu_6722CharsetRecog_UTF_16_LE7getNameEv, @function
_ZNK6icu_6722CharsetRecog_UTF_16_LE7getNameEv:
.LFB19:
	.cfi_startproc
	endbr64
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
.LFE19:
	.size	_ZNK6icu_6722CharsetRecog_UTF_16_LE7getNameEv, .-_ZNK6icu_6722CharsetRecog_UTF_16_LE7getNameEv
	.section	.rodata.str1.1
.LC2:
	.string	"UTF-32BE"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_UTF_32_BE7getNameEv
	.type	_ZNK6icu_6722CharsetRecog_UTF_32_BE7getNameEv, @function
_ZNK6icu_6722CharsetRecog_UTF_32_BE7getNameEv:
.LFB30:
	.cfi_startproc
	endbr64
	leaq	.LC2(%rip), %rax
	ret
	.cfi_endproc
.LFE30:
	.size	_ZNK6icu_6722CharsetRecog_UTF_32_BE7getNameEv, .-_ZNK6icu_6722CharsetRecog_UTF_32_BE7getNameEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_UTF_32_BE7getCharEPKhi
	.type	_ZNK6icu_6722CharsetRecog_UTF_32_BE7getCharEPKhi, @function
_ZNK6icu_6722CharsetRecog_UTF_32_BE7getCharEPKhi:
.LFB31:
	.cfi_startproc
	endbr64
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %eax
	movzbl	3(%rsi,%rdx), %ecx
	sall	$24, %eax
	orl	%ecx, %eax
	movzbl	1(%rsi,%rdx), %ecx
	movzbl	2(%rsi,%rdx), %edx
	sall	$16, %ecx
	sall	$8, %edx
	orl	%ecx, %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE31:
	.size	_ZNK6icu_6722CharsetRecog_UTF_32_BE7getCharEPKhi, .-_ZNK6icu_6722CharsetRecog_UTF_32_BE7getCharEPKhi
	.section	.rodata.str1.1
.LC3:
	.string	"UTF-32LE"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_UTF_32_LE7getNameEv
	.type	_ZNK6icu_6722CharsetRecog_UTF_32_LE7getNameEv, @function
_ZNK6icu_6722CharsetRecog_UTF_32_LE7getNameEv:
.LFB36:
	.cfi_startproc
	endbr64
	leaq	.LC3(%rip), %rax
	ret
	.cfi_endproc
.LFE36:
	.size	_ZNK6icu_6722CharsetRecog_UTF_32_LE7getNameEv, .-_ZNK6icu_6722CharsetRecog_UTF_32_LE7getNameEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_UTF_32_LE7getCharEPKhi
	.type	_ZNK6icu_6722CharsetRecog_UTF_32_LE7getCharEPKhi, @function
_ZNK6icu_6722CharsetRecog_UTF_32_LE7getCharEPKhi:
.LFB37:
	.cfi_startproc
	endbr64
	movslq	%edx, %rdx
	movzbl	3(%rsi,%rdx), %eax
	movzbl	2(%rsi,%rdx), %ecx
	sall	$24, %eax
	sall	$16, %ecx
	orl	%ecx, %eax
	movzbl	(%rsi,%rdx), %ecx
	movzbl	1(%rsi,%rdx), %edx
	orl	%ecx, %eax
	sall	$8, %edx
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE37:
	.size	_ZNK6icu_6722CharsetRecog_UTF_32_LE7getCharEPKhi, .-_ZNK6icu_6722CharsetRecog_UTF_32_LE7getCharEPKhi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_UTF_16_BED2Ev
	.type	_ZN6icu_6722CharsetRecog_UTF_16_BED2Ev, @function
_ZN6icu_6722CharsetRecog_UTF_16_BED2Ev:
.LFB9:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6720CharsetRecog_UnicodeE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE9:
	.size	_ZN6icu_6722CharsetRecog_UTF_16_BED2Ev, .-_ZN6icu_6722CharsetRecog_UTF_16_BED2Ev
	.globl	_ZN6icu_6722CharsetRecog_UTF_16_BED1Ev
	.set	_ZN6icu_6722CharsetRecog_UTF_16_BED1Ev,_ZN6icu_6722CharsetRecog_UTF_16_BED2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_UTF_16_LED2Ev
	.type	_ZN6icu_6722CharsetRecog_UTF_16_LED2Ev, @function
_ZN6icu_6722CharsetRecog_UTF_16_LED2Ev:
.LFB16:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6720CharsetRecog_UnicodeE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE16:
	.size	_ZN6icu_6722CharsetRecog_UTF_16_LED2Ev, .-_ZN6icu_6722CharsetRecog_UTF_16_LED2Ev
	.globl	_ZN6icu_6722CharsetRecog_UTF_16_LED1Ev
	.set	_ZN6icu_6722CharsetRecog_UTF_16_LED1Ev,_ZN6icu_6722CharsetRecog_UTF_16_LED2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_UTF_32_BED2Ev
	.type	_ZN6icu_6722CharsetRecog_UTF_32_BED2Ev, @function
_ZN6icu_6722CharsetRecog_UTF_32_BED2Ev:
.LFB27:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6720CharsetRecog_UnicodeE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE27:
	.size	_ZN6icu_6722CharsetRecog_UTF_32_BED2Ev, .-_ZN6icu_6722CharsetRecog_UTF_32_BED2Ev
	.globl	_ZN6icu_6722CharsetRecog_UTF_32_BED1Ev
	.set	_ZN6icu_6722CharsetRecog_UTF_32_BED1Ev,_ZN6icu_6722CharsetRecog_UTF_32_BED2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_UTF_32_LED2Ev
	.type	_ZN6icu_6722CharsetRecog_UTF_32_LED2Ev, @function
_ZN6icu_6722CharsetRecog_UTF_32_LED2Ev:
.LFB33:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6720CharsetRecog_UnicodeE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE33:
	.size	_ZN6icu_6722CharsetRecog_UTF_32_LED2Ev, .-_ZN6icu_6722CharsetRecog_UTF_32_LED2Ev
	.globl	_ZN6icu_6722CharsetRecog_UTF_32_LED1Ev
	.set	_ZN6icu_6722CharsetRecog_UTF_32_LED1Ev,_ZN6icu_6722CharsetRecog_UTF_32_LED2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_UTF_16_BED0Ev
	.type	_ZN6icu_6722CharsetRecog_UTF_16_BED0Ev, @function
_ZN6icu_6722CharsetRecog_UTF_16_BED0Ev:
.LFB11:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6720CharsetRecog_UnicodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE11:
	.size	_ZN6icu_6722CharsetRecog_UTF_16_BED0Ev, .-_ZN6icu_6722CharsetRecog_UTF_16_BED0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_UTF_16_LED0Ev
	.type	_ZN6icu_6722CharsetRecog_UTF_16_LED0Ev, @function
_ZN6icu_6722CharsetRecog_UTF_16_LED0Ev:
.LFB18:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6720CharsetRecog_UnicodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE18:
	.size	_ZN6icu_6722CharsetRecog_UTF_16_LED0Ev, .-_ZN6icu_6722CharsetRecog_UTF_16_LED0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_UTF_32_BED0Ev
	.type	_ZN6icu_6722CharsetRecog_UTF_32_BED0Ev, @function
_ZN6icu_6722CharsetRecog_UTF_32_BED0Ev:
.LFB29:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6720CharsetRecog_UnicodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE29:
	.size	_ZN6icu_6722CharsetRecog_UTF_32_BED0Ev, .-_ZN6icu_6722CharsetRecog_UTF_32_BED0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_UTF_32_LED0Ev
	.type	_ZN6icu_6722CharsetRecog_UTF_32_LED0Ev, @function
_ZN6icu_6722CharsetRecog_UTF_32_LED0Ev:
.LFB35:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6720CharsetRecog_UnicodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE35:
	.size	_ZN6icu_6722CharsetRecog_UTF_32_LED0Ev, .-_ZN6icu_6722CharsetRecog_UTF_32_LED0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_UTF_16_BE5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6722CharsetRecog_UTF_16_BE5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6722CharsetRecog_UTF_16_BE5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB14:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rdx, %rdi
	movl	$30, %edx
	movl	%edx, %ecx
	movq	40(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	48(%rsi), %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	cmpl	$30, %r12d
	cmovle	%r12d, %ecx
	cmpl	$1, %r12d
	jle	.L30
	movzbl	(%rax), %edx
	movzbl	1(%rax), %r8d
	sall	$8, %edx
	orl	%r8d, %edx
	cmpw	$-257, %dx
	je	.L31
	subl	$2, %ecx
	addq	$2, %rax
	movl	$100, %r11d
	xorl	%r9d, %r9d
	shrl	%ecx
	leaq	(%rax,%rcx,2), %rbx
	movl	$10, %ecx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L25:
	cmpl	$100, %r8d
	movl	%r11d, %ecx
	leal	-1(%r8), %edx
	cmovle	%r8d, %ecx
	testl	%ecx, %ecx
	cmovs	%r9d, %ecx
	cmpl	$98, %edx
	ja	.L41
	cmpq	%rax, %rbx
	je	.L41
	movzbl	(%rax), %edx
	movzbl	1(%rax), %r8d
	addq	$2, %rax
	sall	$8, %edx
	orl	%r8d, %edx
.L23:
	leal	-10(%rcx), %r8d
	testw	%dx, %dx
	je	.L25
	leal	-32(%rdx), %r8d
	cmpw	$223, %r8w
	jbe	.L34
	movl	%ecx, %r8d
	cmpw	$10, %dx
	jne	.L25
.L34:
	leal	10(%rcx), %r8d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L41:
	cmpl	$99, %r8d
	setle	%al
.L21:
	cmpl	$3, %r12d
	jg	.L35
	testb	%al, %al
	je	.L35
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
.L22:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r10, %rdx
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	testl	%ecx, %ecx
	setg	%r12b
	jmp	.L22
.L30:
	movl	$1, %eax
	movl	$10, %ecx
	jmp	.L21
.L31:
	movl	$1, %r12d
	movl	$100, %ecx
	jmp	.L22
	.cfi_endproc
.LFE14:
	.size	_ZNK6icu_6722CharsetRecog_UTF_16_BE5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6722CharsetRecog_UTF_16_BE5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_UTF_16_LE5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6722CharsetRecog_UTF_16_LE5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6722CharsetRecog_UTF_16_LE5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB20:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rdx, %rdi
	movl	$30, %edx
	movl	%edx, %ecx
	movq	40(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	48(%rsi), %ebx
	cmpl	$30, %ebx
	cmovle	%ebx, %ecx
	cmpl	$1, %ebx
	jle	.L53
	movzbl	1(%rax), %edx
	movzbl	(%rax), %r8d
	sall	$8, %edx
	orl	%r8d, %edx
	cmpw	$-257, %dx
	je	.L44
	subl	$2, %ecx
	addq	$2, %rax
	movl	$100, %r11d
	xorl	%r9d, %r9d
	shrl	%ecx
	leaq	(%rax,%rcx,2), %r12
	movl	$10, %ecx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L48:
	cmpl	$100, %r8d
	movl	%r11d, %ecx
	leal	-1(%r8), %edx
	cmovle	%r8d, %ecx
	testl	%ecx, %ecx
	cmovs	%r9d, %ecx
	cmpl	$98, %edx
	ja	.L66
	cmpq	%rax, %r12
	je	.L66
	movzbl	1(%rax), %edx
	movzbl	(%rax), %r8d
	addq	$2, %rax
	sall	$8, %edx
	orl	%r8d, %edx
.L45:
	leal	-10(%rcx), %r8d
	testw	%dx, %dx
	je	.L48
	leal	-32(%rdx), %r8d
	cmpw	$223, %r8w
	jbe	.L59
	movl	%ecx, %r8d
	cmpw	$10, %dx
	jne	.L48
.L59:
	leal	10(%rcx), %r8d
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L66:
	cmpl	$99, %r8d
	setle	%al
.L43:
	cmpl	$3, %ebx
	jg	.L60
	testb	%al, %al
	je	.L60
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
.L46:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r10, %rdx
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	cmpl	$3, %ebx
	jle	.L55
	cmpb	$0, 2(%rax)
	jne	.L55
	cmpb	$1, 3(%rax)
	sbbl	%r12d, %r12d
	addl	$1, %r12d
	cmpb	$1, 3(%rax)
	sbbl	%ecx, %ecx
	notl	%ecx
	andl	$100, %ecx
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$1, %r12d
	movl	$100, %ecx
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L60:
	testl	%ecx, %ecx
	setg	%r12b
	jmp	.L46
.L53:
	movl	$1, %eax
	movl	$10, %ecx
	jmp	.L43
	.cfi_endproc
.LFE20:
	.size	_ZNK6icu_6722CharsetRecog_UTF_16_LE5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6722CharsetRecog_UTF_16_LE5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_UTF_325matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6719CharsetRecog_UTF_325matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6719CharsetRecog_UTF_325matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB25:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	48(%rsi), %eax
	movq	%rsi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	leal	3(%rax), %ebx
	testl	%eax, %eax
	cmovns	%eax, %ebx
	andl	$-4, %ebx
	jle	.L78
	movq	40(%rsi), %r14
	movq	(%rdi), %rax
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	*40(%rax)
	movl	$0, -52(%rbp)
	cmpl	$65279, %eax
	movl	%eax, -76(%rbp)
	sete	-53(%rbp)
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L69:
	addl	$4, %r13d
	addl	$1, -52(%rbp)
	cmpl	%r13d, %ebx
	jle	.L98
.L72:
	movq	(%r15), %rax
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*40(%rax)
	leal	-55296(%rax), %ecx
	cmpl	$2047, %ecx
	jbe	.L84
	cmpl	$1114110, %eax
	jbe	.L69
.L84:
	addl	$4, %r13d
	addl	$1, %r12d
	cmpl	%r13d, %ebx
	jg	.L72
.L98:
	testl	%r12d, %r12d
	sete	%al
	jne	.L85
	cmpb	$0, -53(%rbp)
	movl	$1, %r14d
	movl	$100, %ecx
	je	.L85
.L68:
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	%r15, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	cmpl	$65279, -76(%rbp)
	jne	.L99
	movl	-52(%rbp), %ebx
	leal	(%r12,%r12,4), %eax
	movl	$80, %ecx
	addl	%eax, %eax
	cmpl	%ebx, %eax
	movl	$0, %eax
	cmovge	%eax, %ecx
	setl	%r14b
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L78:
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
	jmp	.L68
.L99:
	cmpl	$3, -52(%rbp)
	jle	.L76
	movl	$1, %r14d
	movl	$100, %ecx
	testb	%al, %al
	jne	.L68
.L76:
	movl	-52(%rbp), %edx
	testl	%edx, %edx
	jle	.L86
	testb	%al, %al
	je	.L86
	movl	$1, %r14d
	movl	$80, %ecx
	jmp	.L68
.L86:
	movl	-52(%rbp), %edi
	leal	(%r12,%r12,4), %eax
	movl	$0, %ecx
	addl	%eax, %eax
	cmpl	%edi, %eax
	movl	$25, %eax
	cmovl	%eax, %ecx
	setl	%r14b
	jmp	.L68
	.cfi_endproc
.LFE25:
	.size	_ZNK6icu_6719CharsetRecog_UTF_325matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6719CharsetRecog_UTF_325matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CharsetRecog_UnicodeD2Ev
	.type	_ZN6icu_6720CharsetRecog_UnicodeD2Ev, @function
_ZN6icu_6720CharsetRecog_UnicodeD2Ev:
.LFB5:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6720CharsetRecog_UnicodeE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE5:
	.size	_ZN6icu_6720CharsetRecog_UnicodeD2Ev, .-_ZN6icu_6720CharsetRecog_UnicodeD2Ev
	.globl	_ZN6icu_6720CharsetRecog_UnicodeD1Ev
	.set	_ZN6icu_6720CharsetRecog_UnicodeD1Ev,_ZN6icu_6720CharsetRecog_UnicodeD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CharsetRecog_UnicodeD0Ev
	.type	_ZN6icu_6720CharsetRecog_UnicodeD0Ev, @function
_ZN6icu_6720CharsetRecog_UnicodeD0Ev:
.LFB7:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6720CharsetRecog_UnicodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE7:
	.size	_ZN6icu_6720CharsetRecog_UnicodeD0Ev, .-_ZN6icu_6720CharsetRecog_UnicodeD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_UTF_32D2Ev
	.type	_ZN6icu_6719CharsetRecog_UTF_32D2Ev, @function
_ZN6icu_6719CharsetRecog_UTF_32D2Ev:
.LFB22:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6720CharsetRecog_UnicodeE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE22:
	.size	_ZN6icu_6719CharsetRecog_UTF_32D2Ev, .-_ZN6icu_6719CharsetRecog_UTF_32D2Ev
	.globl	_ZN6icu_6719CharsetRecog_UTF_32D1Ev
	.set	_ZN6icu_6719CharsetRecog_UTF_32D1Ev,_ZN6icu_6719CharsetRecog_UTF_32D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_UTF_32D0Ev
	.type	_ZN6icu_6719CharsetRecog_UTF_32D0Ev, @function
_ZN6icu_6719CharsetRecog_UTF_32D0Ev:
.LFB24:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6720CharsetRecog_UnicodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE24:
	.size	_ZN6icu_6719CharsetRecog_UTF_32D0Ev, .-_ZN6icu_6719CharsetRecog_UTF_32D0Ev
	.weak	_ZTSN6icu_6720CharsetRecog_UnicodeE
	.section	.rodata._ZTSN6icu_6720CharsetRecog_UnicodeE,"aG",@progbits,_ZTSN6icu_6720CharsetRecog_UnicodeE,comdat
	.align 32
	.type	_ZTSN6icu_6720CharsetRecog_UnicodeE, @object
	.size	_ZTSN6icu_6720CharsetRecog_UnicodeE, 32
_ZTSN6icu_6720CharsetRecog_UnicodeE:
	.string	"N6icu_6720CharsetRecog_UnicodeE"
	.weak	_ZTIN6icu_6720CharsetRecog_UnicodeE
	.section	.data.rel.ro._ZTIN6icu_6720CharsetRecog_UnicodeE,"awG",@progbits,_ZTIN6icu_6720CharsetRecog_UnicodeE,comdat
	.align 8
	.type	_ZTIN6icu_6720CharsetRecog_UnicodeE, @object
	.size	_ZTIN6icu_6720CharsetRecog_UnicodeE, 24
_ZTIN6icu_6720CharsetRecog_UnicodeE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6720CharsetRecog_UnicodeE
	.quad	_ZTIN6icu_6717CharsetRecognizerE
	.weak	_ZTSN6icu_6722CharsetRecog_UTF_16_BEE
	.section	.rodata._ZTSN6icu_6722CharsetRecog_UTF_16_BEE,"aG",@progbits,_ZTSN6icu_6722CharsetRecog_UTF_16_BEE,comdat
	.align 32
	.type	_ZTSN6icu_6722CharsetRecog_UTF_16_BEE, @object
	.size	_ZTSN6icu_6722CharsetRecog_UTF_16_BEE, 34
_ZTSN6icu_6722CharsetRecog_UTF_16_BEE:
	.string	"N6icu_6722CharsetRecog_UTF_16_BEE"
	.weak	_ZTIN6icu_6722CharsetRecog_UTF_16_BEE
	.section	.data.rel.ro._ZTIN6icu_6722CharsetRecog_UTF_16_BEE,"awG",@progbits,_ZTIN6icu_6722CharsetRecog_UTF_16_BEE,comdat
	.align 8
	.type	_ZTIN6icu_6722CharsetRecog_UTF_16_BEE, @object
	.size	_ZTIN6icu_6722CharsetRecog_UTF_16_BEE, 24
_ZTIN6icu_6722CharsetRecog_UTF_16_BEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722CharsetRecog_UTF_16_BEE
	.quad	_ZTIN6icu_6720CharsetRecog_UnicodeE
	.weak	_ZTSN6icu_6722CharsetRecog_UTF_16_LEE
	.section	.rodata._ZTSN6icu_6722CharsetRecog_UTF_16_LEE,"aG",@progbits,_ZTSN6icu_6722CharsetRecog_UTF_16_LEE,comdat
	.align 32
	.type	_ZTSN6icu_6722CharsetRecog_UTF_16_LEE, @object
	.size	_ZTSN6icu_6722CharsetRecog_UTF_16_LEE, 34
_ZTSN6icu_6722CharsetRecog_UTF_16_LEE:
	.string	"N6icu_6722CharsetRecog_UTF_16_LEE"
	.weak	_ZTIN6icu_6722CharsetRecog_UTF_16_LEE
	.section	.data.rel.ro._ZTIN6icu_6722CharsetRecog_UTF_16_LEE,"awG",@progbits,_ZTIN6icu_6722CharsetRecog_UTF_16_LEE,comdat
	.align 8
	.type	_ZTIN6icu_6722CharsetRecog_UTF_16_LEE, @object
	.size	_ZTIN6icu_6722CharsetRecog_UTF_16_LEE, 24
_ZTIN6icu_6722CharsetRecog_UTF_16_LEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722CharsetRecog_UTF_16_LEE
	.quad	_ZTIN6icu_6720CharsetRecog_UnicodeE
	.weak	_ZTSN6icu_6719CharsetRecog_UTF_32E
	.section	.rodata._ZTSN6icu_6719CharsetRecog_UTF_32E,"aG",@progbits,_ZTSN6icu_6719CharsetRecog_UTF_32E,comdat
	.align 16
	.type	_ZTSN6icu_6719CharsetRecog_UTF_32E, @object
	.size	_ZTSN6icu_6719CharsetRecog_UTF_32E, 31
_ZTSN6icu_6719CharsetRecog_UTF_32E:
	.string	"N6icu_6719CharsetRecog_UTF_32E"
	.weak	_ZTIN6icu_6719CharsetRecog_UTF_32E
	.section	.data.rel.ro._ZTIN6icu_6719CharsetRecog_UTF_32E,"awG",@progbits,_ZTIN6icu_6719CharsetRecog_UTF_32E,comdat
	.align 8
	.type	_ZTIN6icu_6719CharsetRecog_UTF_32E, @object
	.size	_ZTIN6icu_6719CharsetRecog_UTF_32E, 24
_ZTIN6icu_6719CharsetRecog_UTF_32E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CharsetRecog_UTF_32E
	.quad	_ZTIN6icu_6720CharsetRecog_UnicodeE
	.weak	_ZTSN6icu_6722CharsetRecog_UTF_32_BEE
	.section	.rodata._ZTSN6icu_6722CharsetRecog_UTF_32_BEE,"aG",@progbits,_ZTSN6icu_6722CharsetRecog_UTF_32_BEE,comdat
	.align 32
	.type	_ZTSN6icu_6722CharsetRecog_UTF_32_BEE, @object
	.size	_ZTSN6icu_6722CharsetRecog_UTF_32_BEE, 34
_ZTSN6icu_6722CharsetRecog_UTF_32_BEE:
	.string	"N6icu_6722CharsetRecog_UTF_32_BEE"
	.weak	_ZTIN6icu_6722CharsetRecog_UTF_32_BEE
	.section	.data.rel.ro._ZTIN6icu_6722CharsetRecog_UTF_32_BEE,"awG",@progbits,_ZTIN6icu_6722CharsetRecog_UTF_32_BEE,comdat
	.align 8
	.type	_ZTIN6icu_6722CharsetRecog_UTF_32_BEE, @object
	.size	_ZTIN6icu_6722CharsetRecog_UTF_32_BEE, 24
_ZTIN6icu_6722CharsetRecog_UTF_32_BEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722CharsetRecog_UTF_32_BEE
	.quad	_ZTIN6icu_6719CharsetRecog_UTF_32E
	.weak	_ZTSN6icu_6722CharsetRecog_UTF_32_LEE
	.section	.rodata._ZTSN6icu_6722CharsetRecog_UTF_32_LEE,"aG",@progbits,_ZTSN6icu_6722CharsetRecog_UTF_32_LEE,comdat
	.align 32
	.type	_ZTSN6icu_6722CharsetRecog_UTF_32_LEE, @object
	.size	_ZTSN6icu_6722CharsetRecog_UTF_32_LEE, 34
_ZTSN6icu_6722CharsetRecog_UTF_32_LEE:
	.string	"N6icu_6722CharsetRecog_UTF_32_LEE"
	.weak	_ZTIN6icu_6722CharsetRecog_UTF_32_LEE
	.section	.data.rel.ro._ZTIN6icu_6722CharsetRecog_UTF_32_LEE,"awG",@progbits,_ZTIN6icu_6722CharsetRecog_UTF_32_LEE,comdat
	.align 8
	.type	_ZTIN6icu_6722CharsetRecog_UTF_32_LEE, @object
	.size	_ZTIN6icu_6722CharsetRecog_UTF_32_LEE, 24
_ZTIN6icu_6722CharsetRecog_UTF_32_LEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722CharsetRecog_UTF_32_LEE
	.quad	_ZTIN6icu_6719CharsetRecog_UTF_32E
	.weak	_ZTVN6icu_6720CharsetRecog_UnicodeE
	.section	.data.rel.ro._ZTVN6icu_6720CharsetRecog_UnicodeE,"awG",@progbits,_ZTVN6icu_6720CharsetRecog_UnicodeE,comdat
	.align 8
	.type	_ZTVN6icu_6720CharsetRecog_UnicodeE, @object
	.size	_ZTVN6icu_6720CharsetRecog_UnicodeE, 56
_ZTVN6icu_6720CharsetRecog_UnicodeE:
	.quad	0
	.quad	_ZTIN6icu_6720CharsetRecog_UnicodeE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.weak	_ZTVN6icu_6722CharsetRecog_UTF_16_BEE
	.section	.data.rel.ro._ZTVN6icu_6722CharsetRecog_UTF_16_BEE,"awG",@progbits,_ZTVN6icu_6722CharsetRecog_UTF_16_BEE,comdat
	.align 8
	.type	_ZTVN6icu_6722CharsetRecog_UTF_16_BEE, @object
	.size	_ZTVN6icu_6722CharsetRecog_UTF_16_BEE, 56
_ZTVN6icu_6722CharsetRecog_UTF_16_BEE:
	.quad	0
	.quad	_ZTIN6icu_6722CharsetRecog_UTF_16_BEE
	.quad	_ZNK6icu_6722CharsetRecog_UTF_16_BE7getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	_ZNK6icu_6722CharsetRecog_UTF_16_BE5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6722CharsetRecog_UTF_16_BED1Ev
	.quad	_ZN6icu_6722CharsetRecog_UTF_16_BED0Ev
	.weak	_ZTVN6icu_6722CharsetRecog_UTF_16_LEE
	.section	.data.rel.ro._ZTVN6icu_6722CharsetRecog_UTF_16_LEE,"awG",@progbits,_ZTVN6icu_6722CharsetRecog_UTF_16_LEE,comdat
	.align 8
	.type	_ZTVN6icu_6722CharsetRecog_UTF_16_LEE, @object
	.size	_ZTVN6icu_6722CharsetRecog_UTF_16_LEE, 56
_ZTVN6icu_6722CharsetRecog_UTF_16_LEE:
	.quad	0
	.quad	_ZTIN6icu_6722CharsetRecog_UTF_16_LEE
	.quad	_ZNK6icu_6722CharsetRecog_UTF_16_LE7getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	_ZNK6icu_6722CharsetRecog_UTF_16_LE5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6722CharsetRecog_UTF_16_LED1Ev
	.quad	_ZN6icu_6722CharsetRecog_UTF_16_LED0Ev
	.weak	_ZTVN6icu_6719CharsetRecog_UTF_32E
	.section	.data.rel.ro._ZTVN6icu_6719CharsetRecog_UTF_32E,"awG",@progbits,_ZTVN6icu_6719CharsetRecog_UTF_32E,comdat
	.align 8
	.type	_ZTVN6icu_6719CharsetRecog_UTF_32E, @object
	.size	_ZTVN6icu_6719CharsetRecog_UTF_32E, 64
_ZTVN6icu_6719CharsetRecog_UTF_32E:
	.quad	0
	.quad	_ZTIN6icu_6719CharsetRecog_UTF_32E
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	_ZNK6icu_6719CharsetRecog_UTF_325matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6722CharsetRecog_UTF_32_BEE
	.section	.data.rel.ro._ZTVN6icu_6722CharsetRecog_UTF_32_BEE,"awG",@progbits,_ZTVN6icu_6722CharsetRecog_UTF_32_BEE,comdat
	.align 8
	.type	_ZTVN6icu_6722CharsetRecog_UTF_32_BEE, @object
	.size	_ZTVN6icu_6722CharsetRecog_UTF_32_BEE, 64
_ZTVN6icu_6722CharsetRecog_UTF_32_BEE:
	.quad	0
	.quad	_ZTIN6icu_6722CharsetRecog_UTF_32_BEE
	.quad	_ZNK6icu_6722CharsetRecog_UTF_32_BE7getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	_ZNK6icu_6719CharsetRecog_UTF_325matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6722CharsetRecog_UTF_32_BED1Ev
	.quad	_ZN6icu_6722CharsetRecog_UTF_32_BED0Ev
	.quad	_ZNK6icu_6722CharsetRecog_UTF_32_BE7getCharEPKhi
	.weak	_ZTVN6icu_6722CharsetRecog_UTF_32_LEE
	.section	.data.rel.ro._ZTVN6icu_6722CharsetRecog_UTF_32_LEE,"awG",@progbits,_ZTVN6icu_6722CharsetRecog_UTF_32_LEE,comdat
	.align 8
	.type	_ZTVN6icu_6722CharsetRecog_UTF_32_LEE, @object
	.size	_ZTVN6icu_6722CharsetRecog_UTF_32_LEE, 64
_ZTVN6icu_6722CharsetRecog_UTF_32_LEE:
	.quad	0
	.quad	_ZTIN6icu_6722CharsetRecog_UTF_32_LEE
	.quad	_ZNK6icu_6722CharsetRecog_UTF_32_LE7getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	_ZNK6icu_6719CharsetRecog_UTF_325matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6722CharsetRecog_UTF_32_LED1Ev
	.quad	_ZN6icu_6722CharsetRecog_UTF_32_LED0Ev
	.quad	_ZNK6icu_6722CharsetRecog_UTF_32_LE7getCharEPKhi
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
