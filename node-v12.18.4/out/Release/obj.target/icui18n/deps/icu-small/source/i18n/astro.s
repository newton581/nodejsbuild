	.file	"astro.cpp"
	.text
	.p2align 4
	.type	calendar_astro_cleanup, @function
calendar_astro_cleanup:
.LFB3335:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3335:
	.size	calendar_astro_cleanup, .-calendar_astro_cleanup
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716SunTimeAngleFuncD2Ev
	.type	_ZN6icu_6716SunTimeAngleFuncD2Ev, @function
_ZN6icu_6716SunTimeAngleFuncD2Ev:
.LFB3376:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3376:
	.size	_ZN6icu_6716SunTimeAngleFuncD2Ev, .-_ZN6icu_6716SunTimeAngleFuncD2Ev
	.globl	_ZN6icu_6716SunTimeAngleFuncD1Ev
	.set	_ZN6icu_6716SunTimeAngleFuncD1Ev,_ZN6icu_6716SunTimeAngleFuncD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RiseSetCoordFuncD2Ev
	.type	_ZN6icu_6716RiseSetCoordFuncD2Ev, @function
_ZN6icu_6716RiseSetCoordFuncD2Ev:
.LFB3392:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3392:
	.size	_ZN6icu_6716RiseSetCoordFuncD2Ev, .-_ZN6icu_6716RiseSetCoordFuncD2Ev
	.globl	_ZN6icu_6716RiseSetCoordFuncD1Ev
	.set	_ZN6icu_6716RiseSetCoordFuncD1Ev,_ZN6icu_6716RiseSetCoordFuncD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717MoonTimeAngleFuncD2Ev
	.type	_ZN6icu_6717MoonTimeAngleFuncD2Ev, @function
_ZN6icu_6717MoonTimeAngleFuncD2Ev:
.LFB3409:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3409:
	.size	_ZN6icu_6717MoonTimeAngleFuncD2Ev, .-_ZN6icu_6717MoonTimeAngleFuncD2Ev
	.globl	_ZN6icu_6717MoonTimeAngleFuncD1Ev
	.set	_ZN6icu_6717MoonTimeAngleFuncD1Ev,_ZN6icu_6717MoonTimeAngleFuncD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720MoonRiseSetCoordFuncD2Ev
	.type	_ZN6icu_6720MoonRiseSetCoordFuncD2Ev, @function
_ZN6icu_6720MoonRiseSetCoordFuncD2Ev:
.LFB3419:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3419:
	.size	_ZN6icu_6720MoonRiseSetCoordFuncD2Ev, .-_ZN6icu_6720MoonRiseSetCoordFuncD2Ev
	.globl	_ZN6icu_6720MoonRiseSetCoordFuncD1Ev
	.set	_ZN6icu_6720MoonRiseSetCoordFuncD1Ev,_ZN6icu_6720MoonRiseSetCoordFuncD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716SunTimeAngleFuncD0Ev
	.type	_ZN6icu_6716SunTimeAngleFuncD0Ev, @function
_ZN6icu_6716SunTimeAngleFuncD0Ev:
.LFB3378:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3378:
	.size	_ZN6icu_6716SunTimeAngleFuncD0Ev, .-_ZN6icu_6716SunTimeAngleFuncD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717MoonTimeAngleFuncD0Ev
	.type	_ZN6icu_6717MoonTimeAngleFuncD0Ev, @function
_ZN6icu_6717MoonTimeAngleFuncD0Ev:
.LFB3411:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3411:
	.size	_ZN6icu_6717MoonTimeAngleFuncD0Ev, .-_ZN6icu_6717MoonTimeAngleFuncD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RiseSetCoordFuncD0Ev
	.type	_ZN6icu_6716RiseSetCoordFuncD0Ev, @function
_ZN6icu_6716RiseSetCoordFuncD0Ev:
.LFB3394:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3394:
	.size	_ZN6icu_6716RiseSetCoordFuncD0Ev, .-_ZN6icu_6716RiseSetCoordFuncD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720MoonRiseSetCoordFuncD0Ev
	.type	_ZN6icu_6720MoonRiseSetCoordFuncD0Ev, @function
_ZN6icu_6720MoonRiseSetCoordFuncD0Ev:
.LFB3421:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3421:
	.size	_ZN6icu_6720MoonRiseSetCoordFuncD0Ev, .-_ZN6icu_6720MoonRiseSetCoordFuncD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CalendarCacheD2Ev
	.type	_ZN6icu_6713CalendarCacheD2Ev, @function
_ZN6icu_6713CalendarCacheD2Ev:
.LFB3440:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713CalendarCacheE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L11
	jmp	uhash_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.cfi_endproc
.LFE3440:
	.size	_ZN6icu_6713CalendarCacheD2Ev, .-_ZN6icu_6713CalendarCacheD2Ev
	.globl	_ZN6icu_6713CalendarCacheD1Ev
	.set	_ZN6icu_6713CalendarCacheD1Ev,_ZN6icu_6713CalendarCacheD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CalendarCacheD0Ev
	.type	_ZN6icu_6713CalendarCacheD0Ev, @function
_ZN6icu_6713CalendarCacheD0Ev:
.LFB3442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713CalendarCacheE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	uhash_close_67@PLT
.L14:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3442:
	.size	_ZN6icu_6713CalendarCacheD0Ev, .-_ZN6icu_6713CalendarCacheD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomerC2Ev
	.type	_ZN6icu_6718CalendarAstronomerC2Ev, @function
_ZN6icu_6718CalendarAstronomerC2Ev:
.LFB3340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	$0x000000000, 8(%rbx)
	movq	$0x000000000, 16(%rbx)
	movq	$0x000000000, 24(%rbx)
	movb	$0, 128(%rbx)
	movsd	%xmm0, (%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 112(%rbx)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%rbx)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3340:
	.size	_ZN6icu_6718CalendarAstronomerC2Ev, .-_ZN6icu_6718CalendarAstronomerC2Ev
	.globl	_ZN6icu_6718CalendarAstronomerC1Ev
	.set	_ZN6icu_6718CalendarAstronomerC1Ev,_ZN6icu_6718CalendarAstronomerC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomerC2Ed
	.type	_ZN6icu_6718CalendarAstronomerC2Ed, @function
_ZN6icu_6718CalendarAstronomerC2Ed:
.LFB3343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	$0x000000000, 8(%rdi)
	movq	$0x000000000, 16(%rdi)
	movq	$0x000000000, 24(%rdi)
	movb	$0, 128(%rdi)
	movsd	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 112(%rdi)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%rbx)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3343:
	.size	_ZN6icu_6718CalendarAstronomerC2Ed, .-_ZN6icu_6718CalendarAstronomerC2Ed
	.globl	_ZN6icu_6718CalendarAstronomerC1Ed
	.set	_ZN6icu_6718CalendarAstronomerC1Ed,_ZN6icu_6718CalendarAstronomerC2Ed
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomerC2Edd
	.type	_ZN6icu_6718CalendarAstronomerC2Edd, @function
_ZN6icu_6718CalendarAstronomerC2Edd:
.LFB3346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movsd	%xmm1, -24(%rbp)
	movsd	%xmm0, -32(%rbp)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movsd	-32(%rbp), %xmm2
	movsd	.LC1(%rip), %xmm3
	movb	$0, 128(%rbx)
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, (%rbx)
	pxor	%xmm0, %xmm0
	mulsd	%xmm2, %xmm3
	movups	%xmm0, 112(%rbx)
	addsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -32(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-32(%rbp), %xmm1
	movsd	.LC1(%rip), %xmm4
	mulsd	-24(%rbp), %xmm4
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	movsd	%xmm1, 8(%rbx)
	movsd	.LC2(%rip), %xmm1
	addsd	%xmm4, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -24(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-24(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	movsd	.LC4(%rip), %xmm0
	mulsd	8(%rbx), %xmm0
	mulsd	.LC5(%rip), %xmm0
	movsd	%xmm1, 16(%rbx)
	divsd	.LC3(%rip), %xmm0
	movsd	%xmm0, 24(%rbx)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%rbx)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3346:
	.size	_ZN6icu_6718CalendarAstronomerC2Edd, .-_ZN6icu_6718CalendarAstronomerC2Edd
	.globl	_ZN6icu_6718CalendarAstronomerC1Edd
	.set	_ZN6icu_6718CalendarAstronomerC1Edd,_ZN6icu_6718CalendarAstronomerC2Edd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomerD2Ev
	.type	_ZN6icu_6718CalendarAstronomerD2Ev, @function
_ZN6icu_6718CalendarAstronomerD2Ev:
.LFB3349:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3349:
	.size	_ZN6icu_6718CalendarAstronomerD2Ev, .-_ZN6icu_6718CalendarAstronomerD2Ev
	.globl	_ZN6icu_6718CalendarAstronomerD1Ev
	.set	_ZN6icu_6718CalendarAstronomerD1Ev,_ZN6icu_6718CalendarAstronomerD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer7setTimeEd
	.type	_ZN6icu_6718CalendarAstronomer7setTimeEd, @function
_ZN6icu_6718CalendarAstronomer7setTimeEd:
.LFB3351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movsd	%xmm0, (%rdi)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%rbx)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3351:
	.size	_ZN6icu_6718CalendarAstronomer7setTimeEd, .-_ZN6icu_6718CalendarAstronomer7setTimeEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer12setJulianDayEd
	.type	_ZN6icu_6718CalendarAstronomer12setJulianDayEd, @function
_ZN6icu_6718CalendarAstronomer12setJulianDayEd:
.LFB3352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movsd	%xmm0, -24(%rbp)
	mulsd	.LC6(%rip), %xmm0
	subsd	.LC7(%rip), %xmm0
	movsd	%xmm0, (%rdi)
	call	uprv_getNaN_67@PLT
	movsd	-24(%rbp), %xmm1
	movb	$0, 128(%rbx)
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm2, %xmm2
	unpcklpd	%xmm0, %xmm1
	movups	%xmm1, 32(%rbx)
	movups	%xmm2, 48(%rbx)
	movups	%xmm2, 64(%rbx)
	movups	%xmm2, 80(%rbx)
	movups	%xmm2, 96(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3352:
	.size	_ZN6icu_6718CalendarAstronomer12setJulianDayEd, .-_ZN6icu_6718CalendarAstronomer12setJulianDayEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer7getTimeEv
	.type	_ZN6icu_6718CalendarAstronomer7getTimeEv, @function
_ZN6icu_6718CalendarAstronomer7getTimeEv:
.LFB3353:
	.cfi_startproc
	endbr64
	movsd	(%rdi), %xmm0
	ret
	.cfi_endproc
.LFE3353:
	.size	_ZN6icu_6718CalendarAstronomer7getTimeEv, .-_ZN6icu_6718CalendarAstronomer7getTimeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer12getJulianDayEv
	.type	_ZN6icu_6718CalendarAstronomer12getJulianDayEv, @function
_ZN6icu_6718CalendarAstronomer12getJulianDayEv:
.LFB3354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movsd	32(%rdi), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L32
	movsd	32(%rbx), %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movsd	.LC7(%rip), %xmm0
	addsd	(%rbx), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3354:
	.size	_ZN6icu_6718CalendarAstronomer12getJulianDayEv, .-_ZN6icu_6718CalendarAstronomer12getJulianDayEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer16getJulianCenturyEv
	.type	_ZN6icu_6718CalendarAstronomer16getJulianCenturyEv, @function
_ZN6icu_6718CalendarAstronomer16getJulianCenturyEv:
.LFB3355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movsd	40(%rdi), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L36
	movsd	40(%rbx), %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movsd	32(%rbx), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L38
	movsd	32(%rbx), %xmm0
.L39:
	subsd	.LC8(%rip), %xmm0
	divsd	.LC9(%rip), %xmm0
	movsd	%xmm0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movsd	.LC7(%rip), %xmm0
	addsd	(%rbx), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%rbx)
	jmp	.L39
	.cfi_endproc
.LFE3355:
	.size	_ZN6icu_6718CalendarAstronomer16getJulianCenturyEv, .-_ZN6icu_6718CalendarAstronomer16getJulianCenturyEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer17getSiderealOffsetEv
	.type	_ZN6icu_6718CalendarAstronomer17getSiderealOffsetEv, @function
_ZN6icu_6718CalendarAstronomer17getSiderealOffsetEv:
.LFB3357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movsd	96(%rdi), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L42
	movsd	96(%rbx), %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movsd	32(%rbx), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L44
	movsd	32(%rbx), %xmm0
.L45:
	subsd	.LC10(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	.LC10(%rip), %xmm1
	movsd	.LC14(%rip), %xmm2
	addsd	%xmm0, %xmm1
	subsd	.LC11(%rip), %xmm1
	divsd	.LC9(%rip), %xmm1
	mulsd	%xmm1, %xmm2
	movsd	.LC12(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	.LC13(%rip), %xmm0
	mulsd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -24(%rbp)
	divsd	.LC4(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC4(%rip), %xmm0
	movsd	-24(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 96(%rbx)
	addq	$24, %rsp
	movapd	%xmm1, %xmm0
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movsd	.LC7(%rip), %xmm0
	addsd	(%rbx), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%rbx)
	jmp	.L45
	.cfi_endproc
.LFE3357:
	.size	_ZN6icu_6718CalendarAstronomer17getSiderealOffsetEv, .-_ZN6icu_6718CalendarAstronomer17getSiderealOffsetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer20getGreenwichSiderealEv
	.type	_ZN6icu_6718CalendarAstronomer20getGreenwichSiderealEv, @function
_ZN6icu_6718CalendarAstronomer20getGreenwichSiderealEv:
.LFB3356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movsd	104(%rdi), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L48
	movsd	104(%rbx), %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movsd	(%rbx), %xmm1
	divsd	.LC5(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -24(%rbp)
	divsd	.LC4(%rip), %xmm0
	call	uprv_floor_67@PLT
	movq	%rbx, %rdi
	movsd	-24(%rbp), %xmm1
	mulsd	.LC4(%rip), %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -24(%rbp)
	call	_ZN6icu_6718CalendarAstronomer17getSiderealOffsetEv
	movsd	-24(%rbp), %xmm1
	mulsd	.LC15(%rip), %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -24(%rbp)
	divsd	.LC4(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC4(%rip), %xmm0
	movsd	-24(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 104(%rbx)
	addq	$24, %rsp
	movapd	%xmm1, %xmm0
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3356:
	.size	_ZN6icu_6718CalendarAstronomer20getGreenwichSiderealEv, .-_ZN6icu_6718CalendarAstronomer20getGreenwichSiderealEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer16getLocalSiderealEv
	.type	_ZN6icu_6718CalendarAstronomer16getLocalSiderealEv, @function
_ZN6icu_6718CalendarAstronomer16getLocalSiderealEv:
.LFB3358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movsd	104(%rdi), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L52
	movsd	.LC4(%rip), %xmm5
	movsd	104(%rbx), %xmm0
	movsd	.LC5(%rip), %xmm2
	movsd	%xmm5, -24(%rbp)
.L53:
	movsd	24(%rbx), %xmm1
	divsd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -32(%rbp)
	divsd	-24(%rbp), %xmm0
	call	uprv_floor_67@PLT
	mulsd	-24(%rbp), %xmm0
	movsd	-32(%rbp), %xmm1
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movsd	.LC5(%rip), %xmm2
	movsd	(%rbx), %xmm1
	movsd	.LC4(%rip), %xmm4
	divsd	%xmm2, %xmm1
	movsd	%xmm2, -40(%rbp)
	movsd	%xmm4, -24(%rbp)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -32(%rbp)
	divsd	%xmm4, %xmm0
	call	uprv_floor_67@PLT
	mulsd	-24(%rbp), %xmm0
	movsd	-32(%rbp), %xmm1
	movq	%rbx, %rdi
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -32(%rbp)
	call	_ZN6icu_6718CalendarAstronomer17getSiderealOffsetEv
	movsd	-32(%rbp), %xmm1
	mulsd	.LC15(%rip), %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -32(%rbp)
	divsd	-24(%rbp), %xmm0
	call	uprv_floor_67@PLT
	mulsd	-24(%rbp), %xmm0
	movsd	-32(%rbp), %xmm1
	movsd	-40(%rbp), %xmm2
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, 104(%rbx)
	jmp	.L53
	.cfi_endproc
.LFE3358:
	.size	_ZN6icu_6718CalendarAstronomer16getLocalSiderealEv, .-_ZN6icu_6718CalendarAstronomer16getLocalSiderealEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer7lstToUTEd
	.type	_ZN6icu_6718CalendarAstronomer7lstToUTEd, @function
_ZN6icu_6718CalendarAstronomer7lstToUTEd:
.LFB3359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movsd	%xmm0, -24(%rbp)
	call	_ZN6icu_6718CalendarAstronomer17getSiderealOffsetEv
	movsd	-24(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	mulsd	.LC16(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -24(%rbp)
	divsd	.LC4(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC4(%rip), %xmm0
	movsd	-24(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movsd	(%rbx), %xmm0
	addsd	24(%rbx), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm1, -24(%rbp)
	call	uprv_floor_67@PLT
	movsd	-24(%rbp), %xmm1
	mulsd	.LC5(%rip), %xmm1
	movapd	%xmm0, %xmm2
	pxor	%xmm0, %xmm0
	cvttsd2siq	%xmm1, %rax
	movsd	.LC6(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	subsd	24(%rbx), %xmm1
	addq	$24, %rsp
	cvtsi2sdq	%rax, %xmm0
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	addsd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE3359:
	.size	_ZN6icu_6718CalendarAstronomer7lstToUTEd, .-_ZN6icu_6718CalendarAstronomer7lstToUTEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEdd
	.type	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEdd, @function
_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEdd:
.LFB3361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movsd	%xmm0, -56(%rbp)
	movsd	88(%rdi), %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L58
	movsd	88(%rbx), %xmm3
.L59:
	leaq	-40(%rbp), %r13
	leaq	-48(%rbp), %r14
	movsd	-80(%rbp), %xmm0
	movsd	%xmm3, -64(%rbp)
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	sincos@PLT
	movsd	-40(%rbp), %xmm4
	movq	%r14, %rsi
	movq	%r13, %rdi
	movsd	-48(%rbp), %xmm2
	movsd	-56(%rbp), %xmm0
	movsd	%xmm4, -88(%rbp)
	movsd	%xmm2, -104(%rbp)
	call	sincos@PLT
	movsd	-64(%rbp), %xmm3
	movq	%r14, %rsi
	movq	%r13, %rdi
	movsd	-48(%rbp), %xmm1
	movsd	-40(%rbp), %xmm7
	movapd	%xmm3, %xmm0
	movsd	%xmm1, -96(%rbp)
	movsd	%xmm7, -72(%rbp)
	call	sincos@PLT
	movsd	-48(%rbp), %xmm5
	movsd	-40(%rbp), %xmm2
	movsd	-80(%rbp), %xmm0
	movsd	%xmm5, -56(%rbp)
	movsd	%xmm2, -64(%rbp)
	call	tan@PLT
	movsd	-104(%rbp), %xmm2
	movsd	-64(%rbp), %xmm4
	movsd	%xmm0, -80(%rbp)
	movsd	-72(%rbp), %xmm0
	mulsd	%xmm2, %xmm4
	mulsd	%xmm4, %xmm0
	movsd	-88(%rbp), %xmm4
	mulsd	-56(%rbp), %xmm4
	addsd	%xmm4, %xmm0
	call	asin@PLT
	movsd	-56(%rbp), %xmm5
	mulsd	-72(%rbp), %xmm5
	movsd	-64(%rbp), %xmm2
	mulsd	-80(%rbp), %xmm2
	movsd	%xmm0, -88(%rbp)
	movsd	-96(%rbp), %xmm1
	movapd	%xmm5, %xmm0
	subsd	%xmm2, %xmm0
	call	atan2@PLT
	movsd	-88(%rbp), %xmm3
	movq	%r12, %rax
	unpcklpd	%xmm3, %xmm0
	movups	%xmm0, (%r12)
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movsd	32(%rbx), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L60
	movsd	32(%rbx), %xmm0
.L61:
	movsd	.LC17(%rip), %xmm1
	subsd	.LC11(%rip), %xmm0
	divsd	.LC9(%rip), %xmm0
	movsd	.LC18(%rip), %xmm3
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm3
	movsd	.LC19(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm3
	movsd	.LC20(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm3
	mulsd	.LC1(%rip), %xmm3
	movsd	%xmm3, 88(%rbx)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L60:
	movsd	.LC7(%rip), %xmm0
	addsd	(%rbx), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%rbx)
	jmp	.L61
	.cfi_endproc
.LFE3361:
	.size	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEdd, .-_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEdd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialERKNS0_8EclipticE
	.type	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialERKNS0_8EclipticE, @function
_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialERKNS0_8EclipticE:
.LFB3360:
	.cfi_startproc
	endbr64
	movsd	8(%rdx), %xmm0
	movsd	(%rdx), %xmm1
	jmp	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEdd
	.cfi_endproc
.LFE3360:
	.size	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialERKNS0_8EclipticE, .-_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialERKNS0_8EclipticE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEd
	.type	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEd, @function
_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEd:
.LFB3362:
	.cfi_startproc
	endbr64
	pxor	%xmm1, %xmm1
	jmp	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEdd
	.cfi_endproc
.LFE3362:
	.size	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEd, .-_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer17eclipticToHorizonERNS0_7HorizonEd
	.type	_ZN6icu_6718CalendarAstronomer17eclipticToHorizonERNS0_7HorizonEd, @function
_ZN6icu_6718CalendarAstronomer17eclipticToHorizonERNS0_7HorizonEd:
.LFB3363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	leaq	-64(%rbp), %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -64(%rbp)
	pxor	%xmm1, %xmm1
	call	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEdd
	movsd	104(%rbx), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L66
	movsd	.LC4(%rip), %xmm4
	movsd	104(%rbx), %xmm0
	movsd	.LC5(%rip), %xmm2
	movsd	%xmm4, -88(%rbp)
.L67:
	movsd	24(%rbx), %xmm1
	leaq	-72(%rbp), %r13
	leaq	-80(%rbp), %r14
	divsd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -96(%rbp)
	divsd	-88(%rbp), %xmm0
	call	uprv_floor_67@PLT
	mulsd	-88(%rbp), %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	movsd	-96(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movsd	.LC2(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	divsd	.LC21(%rip), %xmm0
	subsd	-64(%rbp), %xmm0
	call	sincos@PLT
	movsd	-80(%rbp), %xmm5
	movq	%r14, %rsi
	movq	%r13, %rdi
	movsd	-72(%rbp), %xmm6
	movsd	-56(%rbp), %xmm0
	movsd	%xmm5, -104(%rbp)
	movsd	%xmm6, -112(%rbp)
	call	sincos@PLT
	movsd	-80(%rbp), %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	movsd	-72(%rbp), %xmm2
	movsd	%xmm0, -88(%rbp)
	movsd	16(%rbx), %xmm0
	movsd	%xmm2, -96(%rbp)
	call	sincos@PLT
	movsd	-80(%rbp), %xmm3
	movsd	-88(%rbp), %xmm0
	movsd	-72(%rbp), %xmm1
	movsd	-96(%rbp), %xmm2
	mulsd	%xmm3, %xmm0
	movsd	%xmm3, -128(%rbp)
	mulsd	%xmm1, %xmm2
	movsd	%xmm1, -120(%rbp)
	mulsd	-104(%rbp), %xmm0
	addsd	%xmm2, %xmm0
	call	asin@PLT
	movsd	%xmm0, -104(%rbp)
	call	sin@PLT
	movsd	-120(%rbp), %xmm1
	movsd	-128(%rbp), %xmm3
	movsd	-96(%rbp), %xmm2
	mulsd	%xmm0, %xmm1
	movsd	-88(%rbp), %xmm0
	xorpd	.LC22(%rip), %xmm0
	mulsd	%xmm3, %xmm0
	mulsd	-112(%rbp), %xmm0
	subsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm1
	call	atan2@PLT
	movsd	-104(%rbp), %xmm2
	unpcklpd	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L70
	addq	$96, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movsd	.LC5(%rip), %xmm2
	movsd	(%rbx), %xmm1
	movsd	.LC4(%rip), %xmm7
	divsd	%xmm2, %xmm1
	movsd	%xmm2, -104(%rbp)
	movsd	%xmm7, -88(%rbp)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -96(%rbp)
	divsd	%xmm7, %xmm0
	call	uprv_floor_67@PLT
	mulsd	-88(%rbp), %xmm0
	movsd	-96(%rbp), %xmm1
	movq	%rbx, %rdi
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -96(%rbp)
	call	_ZN6icu_6718CalendarAstronomer17getSiderealOffsetEv
	movsd	-96(%rbp), %xmm1
	mulsd	.LC15(%rip), %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -96(%rbp)
	divsd	-88(%rbp), %xmm0
	call	uprv_floor_67@PLT
	mulsd	-88(%rbp), %xmm0
	movsd	-96(%rbp), %xmm1
	movsd	-104(%rbp), %xmm2
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, 104(%rbx)
	jmp	.L67
.L70:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3363:
	.size	_ZN6icu_6718CalendarAstronomer17eclipticToHorizonERNS0_7HorizonEd, .-_ZN6icu_6718CalendarAstronomer17eclipticToHorizonERNS0_7HorizonEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	.type	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_, @function
_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_:
.LFB3366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	leaq	-40(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	subsd	.LC23(%rip), %xmm1
	mulsd	.LC24(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -56(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-56(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	addsd	.LC25(%rip), %xmm1
	subsd	.LC26(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -56(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-56(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%r12)
	leaq	-48(%rbp), %r12
	movsd	%xmm1, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L72:
	movapd	%xmm1, %xmm0
	movq	%r12, %rsi
	movq	%r13, %rdi
	movsd	%xmm1, -64(%rbp)
	call	sincos@PLT
	movsd	.LC27(%rip), %xmm0
	mulsd	-40(%rbp), %xmm0
	movsd	-64(%rbp), %xmm1
	movsd	.LC27(%rip), %xmm2
	mulsd	-48(%rbp), %xmm2
	movsd	.LC28(%rip), %xmm4
	movapd	%xmm1, %xmm3
	subsd	%xmm0, %xmm3
	subsd	%xmm2, %xmm4
	movapd	%xmm3, %xmm0
	subsd	-56(%rbp), %xmm0
	movapd	%xmm0, %xmm5
	divsd	%xmm4, %xmm5
	subsd	%xmm5, %xmm1
	movsd	%xmm1, -64(%rbp)
	call	uprv_fabs_67@PLT
	comisd	.LC29(%rip), %xmm0
	movsd	-64(%rbp), %xmm1
	ja	.L72
	mulsd	.LC10(%rip), %xmm1
	movapd	%xmm1, %xmm0
	call	tan@PLT
	mulsd	.LC30(%rip), %xmm0
	call	atan@PLT
	movsd	.LC26(%rip), %xmm1
	addsd	%xmm0, %xmm0
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -56(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-56(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3366:
	.size	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_, .-_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEv
	.type	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEv, @function
_ZN6icu_6718CalendarAstronomer15getSunLongitudeEv:
.LFB3365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movsd	48(%rdi), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L83
	movsd	48(%rbx), %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movsd	32(%rbx), %xmm0
	leaq	56(%rbx), %r13
	leaq	48(%rbx), %r12
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L77
	movsd	32(%rbx), %xmm0
.L78:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	movsd	48(%rbx), %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movsd	.LC7(%rip), %xmm0
	addsd	(%rbx), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%rbx)
	jmp	.L78
	.cfi_endproc
.LFE3365:
	.size	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEv, .-_ZN6icu_6718CalendarAstronomer15getSunLongitudeEv
	.section	.text._ZN6icu_6716RiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_,"axG",@progbits,_ZN6icu_6716RiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6716RiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_
	.type	_ZN6icu_6716RiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_, @function
_ZN6icu_6716RiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_:
.LFB3390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movsd	48(%rdx), %xmm0
	movq	%rdx, %r12
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L92
.L85:
	movsd	48(%r12), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEdd
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movsd	32(%r12), %xmm0
	leaq	56(%r12), %r15
	leaq	48(%r12), %r14
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L86
	movsd	32(%r12), %xmm0
.L87:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L86:
	movsd	.LC7(%rip), %xmm0
	addsd	(%r12), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r12)
	jmp	.L87
	.cfi_endproc
.LFE3390:
	.size	_ZN6icu_6716RiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_, .-_ZN6icu_6716RiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_
	.section	.text._ZN6icu_6716SunTimeAngleFunc4evalERNS_18CalendarAstronomerE,"axG",@progbits,_ZN6icu_6716SunTimeAngleFunc4evalERNS_18CalendarAstronomerE,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6716SunTimeAngleFunc4evalERNS_18CalendarAstronomerE
	.type	_ZN6icu_6716SunTimeAngleFunc4evalERNS_18CalendarAstronomerE, @function
_ZN6icu_6716SunTimeAngleFunc4evalERNS_18CalendarAstronomerE:
.LFB3374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movsd	48(%rsi), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L101
	movsd	48(%rbx), %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movsd	32(%rbx), %xmm0
	leaq	56(%rbx), %r13
	leaq	48(%rbx), %r12
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L95
	movsd	32(%rbx), %xmm0
.L96:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	movsd	48(%rbx), %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movsd	.LC7(%rip), %xmm0
	addsd	(%rbx), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%rbx)
	jmp	.L96
	.cfi_endproc
.LFE3374:
	.size	_ZN6icu_6716SunTimeAngleFunc4evalERNS_18CalendarAstronomerE, .-_ZN6icu_6716SunTimeAngleFunc4evalERNS_18CalendarAstronomerE
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer14getSunPositionERNS0_10EquatorialE
	.type	_ZN6icu_6718CalendarAstronomer14getSunPositionERNS0_10EquatorialE, @function
_ZN6icu_6718CalendarAstronomer14getSunPositionERNS0_10EquatorialE:
.LFB3367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movsd	48(%rdi), %xmm0
	movq	%rdi, %r12
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L110
.L103:
	movsd	48(%r12), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEdd
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movsd	32(%r12), %xmm0
	leaq	56(%r12), %r15
	leaq	48(%r12), %r14
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L104
	movsd	32(%r12), %xmm0
.L105:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L104:
	movsd	.LC7(%rip), %xmm0
	addsd	(%r12), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r12)
	jmp	.L105
	.cfi_endproc
.LFE3367:
	.size	_ZN6icu_6718CalendarAstronomer14getSunPositionERNS0_10EquatorialE, .-_ZN6icu_6718CalendarAstronomer14getSunPositionERNS0_10EquatorialE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer15SUMMER_SOLSTICEEv
	.type	_ZN6icu_6718CalendarAstronomer15SUMMER_SOLSTICEEv, @function
_ZN6icu_6718CalendarAstronomer15SUMMER_SOLSTICEEv:
.LFB3368:
	.cfi_startproc
	endbr64
	movsd	.LC31(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3368:
	.size	_ZN6icu_6718CalendarAstronomer15SUMMER_SOLSTICEEv, .-_ZN6icu_6718CalendarAstronomer15SUMMER_SOLSTICEEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer15WINTER_SOLSTICEEv
	.type	_ZN6icu_6718CalendarAstronomer15WINTER_SOLSTICEEv, @function
_ZN6icu_6718CalendarAstronomer15WINTER_SOLSTICEEv:
.LFB3369:
	.cfi_startproc
	endbr64
	movsd	.LC32(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3369:
	.size	_ZN6icu_6718CalendarAstronomer15WINTER_SOLSTICEEv, .-_ZN6icu_6718CalendarAstronomer15WINTER_SOLSTICEEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer9AngleFuncD2Ev
	.type	_ZN6icu_6718CalendarAstronomer9AngleFuncD2Ev, @function
_ZN6icu_6718CalendarAstronomer9AngleFuncD2Ev:
.LFB3371:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3371:
	.size	_ZN6icu_6718CalendarAstronomer9AngleFuncD2Ev, .-_ZN6icu_6718CalendarAstronomer9AngleFuncD2Ev
	.globl	_ZN6icu_6718CalendarAstronomer9AngleFuncD1Ev
	.set	_ZN6icu_6718CalendarAstronomer9AngleFuncD1Ev,_ZN6icu_6718CalendarAstronomer9AngleFuncD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer9AngleFuncD0Ev
	.type	_ZN6icu_6718CalendarAstronomer9AngleFuncD0Ev, @function
_ZN6icu_6718CalendarAstronomer9AngleFuncD0Ev:
.LFB3373:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3373:
	.size	_ZN6icu_6718CalendarAstronomer9AngleFuncD0Ev, .-_ZN6icu_6718CalendarAstronomer9AngleFuncD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer9CoordFuncD2Ev
	.type	_ZN6icu_6718CalendarAstronomer9CoordFuncD2Ev, @function
_ZN6icu_6718CalendarAstronomer9CoordFuncD2Ev:
.LFB3387:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3387:
	.size	_ZN6icu_6718CalendarAstronomer9CoordFuncD2Ev, .-_ZN6icu_6718CalendarAstronomer9CoordFuncD2Ev
	.globl	_ZN6icu_6718CalendarAstronomer9CoordFuncD1Ev
	.set	_ZN6icu_6718CalendarAstronomer9CoordFuncD1Ev,_ZN6icu_6718CalendarAstronomer9CoordFuncD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer9CoordFuncD0Ev
	.type	_ZN6icu_6718CalendarAstronomer9CoordFuncD0Ev, @function
_ZN6icu_6718CalendarAstronomer9CoordFuncD0Ev:
.LFB3389:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3389:
	.size	_ZN6icu_6718CalendarAstronomer9CoordFuncD0Ev, .-_ZN6icu_6718CalendarAstronomer9CoordFuncD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer13getSunRiseSetEa
	.type	_ZN6icu_6718CalendarAstronomer13getSunRiseSetEa, @function
_ZN6icu_6718CalendarAstronomer13getSunRiseSetEa:
.LFB3395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$72, %rsp
	movb	%sil, -97(%rbp)
	movsd	(%rdi), %xmm3
	movsd	24(%rdi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addsd	%xmm3, %xmm0
	movsd	%xmm3, -96(%rbp)
	divsd	.LC6(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC6(%rip), %xmm0
	movsd	.LC33(%rip), %xmm1
	subsd	24(%r15), %xmm0
	addsd	.LC35(%rip), %xmm0
	testb	%bl, %bl
	jne	.L118
	movsd	.LC34(%rip), %xmm1
.L118:
	addsd	%xmm1, %xmm0
	movl	$5, %r12d
	leaq	-80(%rbp), %r14
	leaq	56(%r15), %r13
	movsd	%xmm0, (%r15)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r15)
	movsd	%xmm0, -112(%rbp)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r15)
	movups	%xmm0, 48(%r15)
	movups	%xmm0, 64(%r15)
	movups	%xmm0, 80(%r15)
	movups	%xmm0, 96(%r15)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	movsd	16(%r15), %xmm0
	call	tan@PLT
	movsd	-112(%rbp), %xmm1
	xorpd	.LC22(%rip), %xmm0
	movsd	%xmm0, -88(%rbp)
	movapd	%xmm1, %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L141
	.p2align 4,,10
	.p2align 3
.L119:
	movsd	48(%r15), %xmm0
	pxor	%xmm1, %xmm1
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEdd
	movsd	-72(%rbp), %xmm0
	call	tan@PLT
	mulsd	-88(%rbp), %xmm0
	call	acos@PLT
	cmpb	$0, -97(%rbp)
	je	.L122
	movsd	.LC3(%rip), %xmm4
	subsd	%xmm0, %xmm4
	movapd	%xmm4, %xmm0
.L122:
	movsd	-80(%rbp), %xmm1
	movq	%r15, %rdi
	addsd	%xmm0, %xmm1
	mulsd	.LC4(%rip), %xmm1
	divsd	.LC3(%rip), %xmm1
	movsd	%xmm1, -112(%rbp)
	call	_ZN6icu_6718CalendarAstronomer17getSiderealOffsetEv
	movsd	-112(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	mulsd	.LC16(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -112(%rbp)
	divsd	.LC4(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC4(%rip), %xmm0
	movsd	-112(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movsd	(%r15), %xmm0
	addsd	24(%r15), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm1, -112(%rbp)
	call	uprv_floor_67@PLT
	movsd	-112(%rbp), %xmm1
	mulsd	.LC5(%rip), %xmm1
	mulsd	.LC6(%rip), %xmm0
	subsd	24(%r15), %xmm0
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm2
	subsd	(%r15), %xmm2
	movsd	%xmm1, (%r15)
	movsd	%xmm2, -112(%rbp)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r15)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r15)
	movups	%xmm0, 48(%r15)
	movups	%xmm0, 64(%r15)
	movups	%xmm0, 80(%r15)
	movups	%xmm0, 96(%r15)
	subl	$1, %r12d
	je	.L123
	movsd	-112(%rbp), %xmm2
	movapd	%xmm2, %xmm0
	call	uprv_fabs_67@PLT
	comisd	.LC36(%rip), %xmm0
	jbe	.L123
	movsd	48(%r15), %xmm1
	movapd	%xmm1, %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	je	.L119
.L141:
	movsd	32(%r15), %xmm0
	leaq	48(%r15), %rsi
	movq	%rsi, -112(%rbp)
	call	uprv_isNaN_67@PLT
	movq	-112(%rbp), %rsi
	testb	%al, %al
	jne	.L120
	movsd	32(%r15), %xmm0
.L121:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L123:
	movsd	-72(%rbp), %xmm0
	call	cos@PLT
	movsd	%xmm0, -88(%rbp)
	movsd	16(%r15), %xmm0
	call	sin@PLT
	divsd	-88(%rbp), %xmm0
	call	acos@PLT
	call	sin@PLT
	movsd	.LC37(%rip), %xmm1
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	call	asin@PLT
	mulsd	.LC38(%rip), %xmm0
	movsd	(%r15), %xmm1
	mulsd	.LC39(%rip), %xmm0
	divsd	-88(%rbp), %xmm0
	mulsd	.LC40(%rip), %xmm0
	cvttsd2siq	%xmm0, %rax
	testb	%bl, %bl
	je	.L126
	negq	%rax
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L127:
	addsd	%xmm0, %xmm1
	movsd	-96(%rbp), %xmm7
	movsd	%xmm7, (%r15)
	movsd	%xmm1, -88(%rbp)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r15)
	movsd	-88(%rbp), %xmm1
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r15)
	movups	%xmm0, 48(%r15)
	movups	%xmm0, 64(%r15)
	movups	%xmm0, 80(%r15)
	movups	%xmm0, 96(%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L142
	addq	$72, %rsp
	movapd	%xmm1, %xmm0
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movsd	.LC7(%rip), %xmm0
	addsd	(%r15), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r15)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L126:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	jmp	.L127
.L142:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3395:
	.size	_ZN6icu_6718CalendarAstronomer13getSunRiseSetEa, .-_ZN6icu_6718CalendarAstronomer13getSunRiseSetEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	.type	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv, @function
_ZN6icu_6718CalendarAstronomer15getMoonPositionEv:
.LFB3402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	112(%rdi), %r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 3, -48
	cmpb	$0, 128(%rdi)
	je	.L154
	addq	$80, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movsd	48(%rdi), %xmm0
	movq	%rdi, %rbx
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L155
.L145:
	movsd	32(%rbx), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L148
	movsd	32(%rbx), %xmm3
.L149:
	subsd	.LC23(%rip), %xmm3
	movapd	%xmm3, %xmm1
	movsd	%xmm3, -64(%rbp)
	mulsd	.LC41(%rip), %xmm1
	addsd	.LC42(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -56(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-56(%rbp), %xmm1
	movsd	-64(%rbp), %xmm3
	mulsd	.LC3(%rip), %xmm0
	movapd	%xmm1, %xmm7
	movsd	%xmm3, -104(%rbp)
	subsd	%xmm0, %xmm7
	movapd	%xmm3, %xmm0
	mulsd	.LC43(%rip), %xmm0
	movapd	%xmm7, %xmm2
	movsd	%xmm7, -64(%rbp)
	subsd	%xmm0, %xmm2
	subsd	.LC44(%rip), %xmm2
	movapd	%xmm2, %xmm0
	movsd	%xmm2, -56(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-56(%rbp), %xmm2
	movsd	48(%rbx), %xmm4
	mulsd	.LC3(%rip), %xmm0
	movapd	%xmm2, %xmm1
	movsd	%xmm4, -96(%rbp)
	subsd	%xmm0, %xmm1
	movsd	-64(%rbp), %xmm0
	subsd	%xmm4, %xmm0
	movsd	%xmm1, -72(%rbp)
	addsd	%xmm0, %xmm0
	subsd	%xmm1, %xmm0
	call	sin@PLT
	movsd	.LC45(%rip), %xmm2
	mulsd	%xmm0, %xmm2
	movsd	56(%rbx), %xmm0
	movsd	%xmm2, -56(%rbp)
	call	sin@PLT
	movsd	-56(%rbp), %xmm2
	movsd	-72(%rbp), %xmm1
	movsd	.LC46(%rip), %xmm6
	mulsd	%xmm0, %xmm6
	mulsd	.LC47(%rip), %xmm0
	subsd	%xmm6, %xmm2
	movsd	%xmm6, -88(%rbp)
	subsd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 80(%rbx)
	movapd	%xmm2, %xmm0
	movsd	%xmm2, -80(%rbp)
	call	sin@PLT
	movsd	-80(%rbp), %xmm2
	movsd	%xmm0, -72(%rbp)
	addsd	%xmm2, %xmm2
	movapd	%xmm2, %xmm0
	call	sin@PLT
	movsd	-72(%rbp), %xmm5
	mulsd	.LC48(%rip), %xmm5
	movsd	-56(%rbp), %xmm1
	addsd	-64(%rbp), %xmm1
	movsd	-88(%rbp), %xmm6
	mulsd	.LC49(%rip), %xmm0
	movsd	-96(%rbp), %xmm4
	addsd	%xmm5, %xmm1
	subsd	%xmm6, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -56(%rbp)
	subsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm0
	call	sin@PLT
	movsd	-104(%rbp), %xmm3
	mulsd	.LC50(%rip), %xmm0
	mulsd	.LC51(%rip), %xmm3
	movsd	-56(%rbp), %xmm1
	movsd	.LC52(%rip), %xmm2
	addsd	%xmm0, %xmm1
	subsd	%xmm3, %xmm2
	movsd	%xmm1, 64(%rbx)
	movapd	%xmm2, %xmm0
	movsd	%xmm2, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	%xmm0, -56(%rbp)
	movsd	56(%rbx), %xmm0
	call	sin@PLT
	movsd	-64(%rbp), %xmm2
	leaq	-40(%rbp), %rdi
	movsd	.LC3(%rip), %xmm1
	mulsd	-56(%rbp), %xmm1
	leaq	-48(%rbp), %rsi
	mulsd	.LC53(%rip), %xmm0
	subsd	%xmm1, %xmm2
	subsd	%xmm0, %xmm2
	movsd	64(%rbx), %xmm0
	subsd	%xmm2, %xmm0
	movsd	%xmm2, -64(%rbp)
	call	sincos@PLT
	movsd	-40(%rbp), %xmm3
	movsd	-48(%rbp), %xmm1
	movapd	%xmm3, %xmm0
	movsd	%xmm3, -56(%rbp)
	mulsd	.LC54(%rip), %xmm0
	call	atan2@PLT
	movsd	-64(%rbp), %xmm2
	movsd	-56(%rbp), %xmm3
	mulsd	.LC55(%rip), %xmm3
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 72(%rbx)
	movapd	%xmm3, %xmm0
	call	asin@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movapd	%xmm0, %xmm1
	movsd	72(%rbx), %xmm0
	call	_ZN6icu_6718CalendarAstronomer20eclipticToEquatorialERNS0_10EquatorialEdd
	movb	$1, 128(%rbx)
	addq	$80, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movsd	.LC7(%rip), %xmm3
	addsd	(%rbx), %xmm3
	divsd	.LC6(%rip), %xmm3
	movsd	%xmm3, 32(%rbx)
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L155:
	movsd	32(%rbx), %xmm0
	leaq	56(%rbx), %r14
	leaq	48(%rbx), %r13
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L146
	movsd	32(%rbx), %xmm0
.L147:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L146:
	movsd	.LC7(%rip), %xmm0
	addsd	(%rbx), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%rbx)
	jmp	.L147
	.cfi_endproc
.LFE3402:
	.size	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv, .-_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	.section	.text._ZN6icu_6720MoonRiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_,"axG",@progbits,_ZN6icu_6720MoonRiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6720MoonRiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_
	.type	_ZN6icu_6720MoonRiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_, @function
_ZN6icu_6720MoonRiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_:
.LFB3417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movdqu	(%rax), %xmm0
	movups	%xmm0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3417:
	.size	_ZN6icu_6720MoonRiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_, .-_ZN6icu_6720MoonRiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_
	.section	.text._ZN6icu_6717MoonTimeAngleFunc4evalERNS_18CalendarAstronomerE,"axG",@progbits,_ZN6icu_6717MoonTimeAngleFunc4evalERNS_18CalendarAstronomerE,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6717MoonTimeAngleFunc4evalERNS_18CalendarAstronomerE
	.type	_ZN6icu_6717MoonTimeAngleFunc4evalERNS_18CalendarAstronomerE, @function
_ZN6icu_6717MoonTimeAngleFunc4evalERNS_18CalendarAstronomerE:
.LFB3407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%rbx), %xmm1
	subsd	48(%rbx), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -24(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-24(%rbp), %xmm1
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE3407:
	.size	_ZN6icu_6717MoonTimeAngleFunc4evalERNS_18CalendarAstronomerE, .-_ZN6icu_6717MoonTimeAngleFunc4evalERNS_18CalendarAstronomerE
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer10getMoonAgeEv
	.type	_ZN6icu_6718CalendarAstronomer10getMoonAgeEv, @function
_ZN6icu_6718CalendarAstronomer10getMoonAgeEv:
.LFB3403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%rbx), %xmm1
	subsd	48(%rbx), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -24(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-24(%rbp), %xmm1
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE3403:
	.size	_ZN6icu_6718CalendarAstronomer10getMoonAgeEv, .-_ZN6icu_6718CalendarAstronomer10getMoonAgeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer12getMoonPhaseEv
	.type	_ZN6icu_6718CalendarAstronomer12getMoonPhaseEv, @function
_ZN6icu_6718CalendarAstronomer12getMoonPhaseEv:
.LFB3404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%rbx), %xmm1
	subsd	48(%rbx), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -24(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-24(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	call	cos@PLT
	movsd	.LC28(%rip), %xmm1
	subsd	%xmm0, %xmm1
	movsd	.LC10(%rip), %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	mulsd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE3404:
	.size	_ZN6icu_6718CalendarAstronomer12getMoonPhaseEv, .-_ZN6icu_6718CalendarAstronomer12getMoonPhaseEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer8NEW_MOONEv
	.type	_ZN6icu_6718CalendarAstronomer8NEW_MOONEv, @function
_ZN6icu_6718CalendarAstronomer8NEW_MOONEv:
.LFB3405:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE3405:
	.size	_ZN6icu_6718CalendarAstronomer8NEW_MOONEv, .-_ZN6icu_6718CalendarAstronomer8NEW_MOONEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer9FULL_MOONEv
	.type	_ZN6icu_6718CalendarAstronomer9FULL_MOONEv, @function
_ZN6icu_6718CalendarAstronomer9FULL_MOONEv:
.LFB3406:
	.cfi_startproc
	endbr64
	movsd	.LC2(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3406:
	.size	_ZN6icu_6718CalendarAstronomer9FULL_MOONEv, .-_ZN6icu_6718CalendarAstronomer9FULL_MOONEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer14getMoonRiseSetEa
	.type	_ZN6icu_6718CalendarAstronomer14getMoonRiseSetEa, @function
_ZN6icu_6718CalendarAstronomer14getMoonRiseSetEa:
.LFB3422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	$5, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movsd	16(%rdi), %xmm0
	call	tan@PLT
	xorpd	.LC22(%rip), %xmm0
	movsd	%xmm0, -48(%rbp)
.L171:
	movq	%rbx, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	8(%rax), %xmm3
	movsd	(%rax), %xmm6
	movapd	%xmm3, %xmm0
	movsd	%xmm6, -40(%rbp)
	movsd	%xmm3, -56(%rbp)
	call	tan@PLT
	mulsd	-48(%rbp), %xmm0
	call	acos@PLT
	movapd	%xmm0, %xmm1
	testb	%r14b, %r14b
	je	.L167
	movsd	.LC3(%rip), %xmm4
	subsd	%xmm0, %xmm4
	movapd	%xmm4, %xmm1
.L167:
	movq	%rbx, %rdi
	movsd	%xmm1, -64(%rbp)
	call	_ZN6icu_6718CalendarAstronomer17getSiderealOffsetEv
	movsd	-64(%rbp), %xmm1
	addsd	-40(%rbp), %xmm1
	mulsd	.LC4(%rip), %xmm1
	divsd	.LC3(%rip), %xmm1
	subsd	%xmm0, %xmm1
	mulsd	.LC16(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC4(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC4(%rip), %xmm0
	movsd	-40(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movsd	(%rbx), %xmm0
	addsd	24(%rbx), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm1, -40(%rbp)
	call	uprv_floor_67@PLT
	movsd	-40(%rbp), %xmm1
	mulsd	.LC5(%rip), %xmm1
	mulsd	.LC6(%rip), %xmm0
	subsd	24(%rbx), %xmm0
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm2
	subsd	(%rbx), %xmm2
	movsd	%xmm1, (%rbx)
	movsd	%xmm2, -40(%rbp)
	call	uprv_getNaN_67@PLT
	subl	$1, %r13d
	movsd	-40(%rbp), %xmm2
	movb	$0, 128(%rbx)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	je	.L172
	movapd	%xmm2, %xmm0
	call	uprv_fabs_67@PLT
	comisd	.LC56(%rip), %xmm0
	ja	.L171
	.p2align 4,,10
	.p2align 3
.L172:
	movsd	-56(%rbp), %xmm0
	call	cos@PLT
	movsd	%xmm0, -40(%rbp)
	movsd	16(%rbx), %xmm0
	call	sin@PLT
	divsd	-40(%rbp), %xmm0
	call	acos@PLT
	call	sin@PLT
	movsd	.LC37(%rip), %xmm1
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	call	asin@PLT
	mulsd	.LC38(%rip), %xmm0
	movsd	(%rbx), %xmm1
	mulsd	.LC39(%rip), %xmm0
	divsd	-40(%rbp), %xmm0
	mulsd	.LC40(%rip), %xmm0
	cvttsd2siq	%xmm0, %rax
	testb	%r12b, %r12b
	je	.L180
	negq	%rax
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L173:
	addq	$32, %rsp
	addsd	%xmm1, %xmm0
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	jmp	.L173
	.cfi_endproc
.LFE3422:
	.size	_ZN6icu_6718CalendarAstronomer14getMoonRiseSetEa, .-_ZN6icu_6718CalendarAstronomer14getMoonRiseSetEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer11timeOfAngleERNS0_9AngleFuncEddda
	.type	_ZN6icu_6718CalendarAstronomer11timeOfAngleERNS0_9AngleFuncEddda, @function
_ZN6icu_6718CalendarAstronomer11timeOfAngleERNS0_9AngleFuncEddda:
.LFB3426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$88, %rsp
	movq	(%rsi), %rax
	movsd	%xmm1, -104(%rbp)
	movq	%rdi, %rsi
	movsd	%xmm2, -88(%rbp)
	movq	%r13, %rdi
	movsd	%xmm0, -80(%rbp)
	call	*(%rax)
	movsd	-80(%rbp), %xmm1
	movsd	%xmm0, -48(%rbp)
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-40(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movsd	.LC57(%rip), %xmm0
	testb	%bl, %bl
	je	.L182
	pxor	%xmm0, %xmm0
.L182:
	movsd	-104(%rbp), %xmm4
	addsd	%xmm1, %xmm0
	mulsd	.LC6(%rip), %xmm4
	movsd	(%r12), %xmm5
	movsd	%xmm5, -96(%rbp)
	mulsd	%xmm4, %xmm0
	movsd	%xmm4, -112(%rbp)
	divsd	.LC3(%rip), %xmm0
	movsd	%xmm0, -40(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-96(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L190:
	movsd	(%r12), %xmm3
	movsd	-40(%rbp), %xmm0
	movsd	%xmm3, -56(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-56(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movsd	-40(%rbp), %xmm0
	call	uprv_fabs_67@PLT
	comisd	-88(%rbp), %xmm0
	jbe	.L193
.L186:
	movsd	-48(%rbp), %xmm6
	movq	0(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movsd	%xmm6, -56(%rbp)
	call	*(%rax)
	movapd	%xmm0, %xmm1
	subsd	-56(%rbp), %xmm1
	movsd	%xmm0, -48(%rbp)
	addsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -56(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-56(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	movsd	-40(%rbp), %xmm0
	divsd	%xmm1, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-80(%rbp), %xmm4
	subsd	-48(%rbp), %xmm4
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, -56(%rbp)
	addsd	%xmm4, %xmm1
	movapd	%xmm1, %xmm5
	divsd	.LC3(%rip), %xmm5
	movapd	%xmm5, %xmm0
	movsd	%xmm1, -72(%rbp)
	call	uprv_floor_67@PLT
	movsd	-72(%rbp), %xmm1
	movsd	-40(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	movsd	%xmm2, -64(%rbp)
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	mulsd	-56(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	uprv_fabs_67@PLT
	movsd	-64(%rbp), %xmm2
	movsd	%xmm0, -56(%rbp)
	movapd	%xmm2, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-56(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L190
	movsd	-112(%rbp), %xmm0
	mulsd	.LC58(%rip), %xmm0
	call	uprv_ceil_67@PLT
	testb	%bl, %bl
	jne	.L185
	xorpd	.LC22(%rip), %xmm0
.L185:
	addsd	-96(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movsd	-88(%rbp), %xmm2
	movsbl	%bl, %edx
	movq	%r13, %rsi
	unpcklpd	%xmm0, %xmm0
	movsd	-104(%rbp), %xmm1
	movq	%r12, %rdi
	movb	$0, 128(%r12)
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movsd	-80(%rbp), %xmm0
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6718CalendarAstronomer11timeOfAngleERNS0_9AngleFuncEddda
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	movsd	(%r12), %xmm0
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3426:
	.size	_ZN6icu_6718CalendarAstronomer11timeOfAngleERNS0_9AngleFuncEddda, .-_ZN6icu_6718CalendarAstronomer11timeOfAngleERNS0_9AngleFuncEddda
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer10getSunTimeEda
	.type	_ZN6icu_6718CalendarAstronomer10getSunTimeEda, @function
_ZN6icu_6718CalendarAstronomer10getSunTimeEda:
.LFB3379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movsd	%xmm0, -72(%rbp)
	movsd	48(%rdi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6716SunTimeAngleFuncE(%rip), %rax
	movq	%rax, -64(%rbp)
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L332
.L195:
	movsd	48(%r12), %xmm7
	movsd	-72(%rbp), %xmm1
	subsd	%xmm7, %xmm1
	movsd	%xmm7, -96(%rbp)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm1
	pxor	%xmm6, %xmm6
	mulsd	.LC3(%rip), %xmm0
	movsd	%xmm6, -88(%rbp)
	subsd	%xmm0, %xmm1
	testb	%r14b, %r14b
	jne	.L198
	movsd	.LC57(%rip), %xmm6
	movsd	%xmm6, -88(%rbp)
.L198:
	movsd	(%r12), %xmm6
	addsd	-88(%rbp), %xmm1
	leaq	56(%r12), %r13
	mulsd	.LC59(%rip), %xmm1
	leaq	48(%r12), %rbx
	divsd	.LC3(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	movsd	%xmm6, -112(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-112(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	movapd	%xmm0, %xmm1
	unpcklpd	%xmm1, %xmm1
	movups	%xmm1, 32(%r12)
	movups	%xmm1, 48(%r12)
	movups	%xmm1, 64(%r12)
	movups	%xmm1, 80(%r12)
	movups	%xmm1, 96(%r12)
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L199:
	movsd	48(%r12), %xmm1
	movsd	%xmm1, -104(%rbp)
	subsd	-96(%rbp), %xmm1
	addsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -96(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-96(%rbp), %xmm1
	movsd	-80(%rbp), %xmm7
	mulsd	.LC3(%rip), %xmm0
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	divsd	%xmm1, %xmm7
	movapd	%xmm7, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-72(%rbp), %xmm4
	subsd	-104(%rbp), %xmm4
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, -96(%rbp)
	addsd	%xmm4, %xmm1
	movapd	%xmm1, %xmm3
	divsd	.LC3(%rip), %xmm3
	movapd	%xmm3, %xmm0
	movsd	%xmm1, -120(%rbp)
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-120(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	mulsd	-96(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -120(%rbp)
	call	uprv_fabs_67@PLT
	movsd	%xmm0, -96(%rbp)
	movsd	-80(%rbp), %xmm0
	call	uprv_fabs_67@PLT
	movsd	-96(%rbp), %xmm2
	movsd	-120(%rbp), %xmm1
	comisd	%xmm0, %xmm2
	ja	.L333
	movsd	(%r12), %xmm6
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -96(%rbp)
	movsd	%xmm6, -80(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-80(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movsd	-96(%rbp), %xmm1
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_fabs_67@PLT
	comisd	.LC56(%rip), %xmm0
	jbe	.L330
	movsd	-104(%rbp), %xmm4
	movsd	48(%r12), %xmm0
	movsd	%xmm4, -96(%rbp)
.L268:
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	je	.L199
	movsd	32(%r12), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L200
	movsd	32(%r12), %xmm0
.L201:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L200:
	movsd	.LC7(%rip), %xmm0
	addsd	(%r12), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r12)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L330:
	movsd	(%r12), %xmm0
.L194:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L334
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movq	.LC60(%rip), %rax
	movq	%rax, %xmm0
	call	uprv_ceil_67@PLT
	testb	%r14b, %r14b
	jne	.L204
	xorpd	.LC22(%rip), %xmm0
.L204:
	addsd	-112(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	movapd	%xmm0, %xmm1
	unpcklpd	%xmm1, %xmm1
	movups	%xmm1, 32(%r12)
	movups	%xmm1, 48(%r12)
	movups	%xmm1, 64(%r12)
	movups	%xmm1, 80(%r12)
	movups	%xmm1, 96(%r12)
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L335
.L205:
	movsd	-72(%rbp), %xmm1
	leaq	56(%r12), %r13
	leaq	48(%r12), %rbx
	movsd	48(%r12), %xmm2
	subsd	%xmm2, %xmm1
	movsd	%xmm2, -104(%rbp)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm1
	mulsd	.LC3(%rip), %xmm0
	movsd	(%r12), %xmm3
	movsd	%xmm3, -120(%rbp)
	subsd	%xmm0, %xmm1
	addsd	-88(%rbp), %xmm1
	mulsd	.LC59(%rip), %xmm1
	divsd	.LC3(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-120(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	movapd	%xmm0, %xmm1
	unpcklpd	%xmm1, %xmm1
	movups	%xmm1, 32(%r12)
	movups	%xmm1, 48(%r12)
	movups	%xmm1, 64(%r12)
	movups	%xmm1, 80(%r12)
	movups	%xmm1, 96(%r12)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L208:
	movsd	48(%r12), %xmm1
	movsd	%xmm1, -96(%rbp)
	subsd	-104(%rbp), %xmm1
	addsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -104(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-104(%rbp), %xmm1
	movsd	-80(%rbp), %xmm3
	mulsd	.LC3(%rip), %xmm0
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	divsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-72(%rbp), %xmm5
	subsd	-96(%rbp), %xmm5
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, -104(%rbp)
	addsd	%xmm5, %xmm1
	movapd	%xmm1, %xmm6
	divsd	.LC3(%rip), %xmm6
	movapd	%xmm6, %xmm0
	movsd	%xmm1, -112(%rbp)
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-112(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	mulsd	-104(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -112(%rbp)
	call	uprv_fabs_67@PLT
	movsd	%xmm0, -104(%rbp)
	movsd	-80(%rbp), %xmm0
	call	uprv_fabs_67@PLT
	movsd	-104(%rbp), %xmm2
	movsd	-112(%rbp), %xmm1
	comisd	%xmm0, %xmm2
	ja	.L336
	movsd	(%r12), %xmm3
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -104(%rbp)
	movsd	%xmm3, -80(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-80(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movsd	-104(%rbp), %xmm1
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_fabs_67@PLT
	comisd	.LC56(%rip), %xmm0
	jbe	.L330
	movsd	-96(%rbp), %xmm5
	movsd	48(%r12), %xmm0
	movsd	%xmm5, -104(%rbp)
.L265:
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	je	.L208
	movsd	32(%r12), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L209
	movsd	32(%r12), %xmm0
.L210:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L209:
	movsd	.LC7(%rip), %xmm0
	addsd	(%r12), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r12)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L332:
	movsd	32(%r12), %xmm0
	leaq	56(%r12), %r15
	leaq	48(%r12), %r13
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L196
	movsd	32(%r12), %xmm0
.L197:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L336:
	movq	.LC60(%rip), %rax
	movq	%rax, %xmm0
	call	uprv_ceil_67@PLT
	testb	%r14b, %r14b
	jne	.L213
	xorpd	.LC22(%rip), %xmm0
.L213:
	addsd	-120(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	movapd	%xmm0, %xmm1
	unpcklpd	%xmm1, %xmm1
	movups	%xmm1, 32(%r12)
	movups	%xmm1, 48(%r12)
	movups	%xmm1, 64(%r12)
	movups	%xmm1, 80(%r12)
	movups	%xmm1, 96(%r12)
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L337
.L214:
	movsd	-72(%rbp), %xmm1
	leaq	56(%r12), %r13
	leaq	48(%r12), %rbx
	movsd	48(%r12), %xmm7
	subsd	%xmm7, %xmm1
	movsd	%xmm7, -104(%rbp)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm1
	mulsd	.LC3(%rip), %xmm0
	movsd	(%r12), %xmm3
	movsd	%xmm3, -120(%rbp)
	subsd	%xmm0, %xmm1
	addsd	-88(%rbp), %xmm1
	mulsd	.LC59(%rip), %xmm1
	divsd	.LC3(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-120(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	movapd	%xmm0, %xmm1
	unpcklpd	%xmm1, %xmm1
	movups	%xmm1, 32(%r12)
	movups	%xmm1, 48(%r12)
	movups	%xmm1, 64(%r12)
	movups	%xmm1, 80(%r12)
	movups	%xmm1, 96(%r12)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L217:
	movsd	48(%r12), %xmm1
	movsd	%xmm1, -96(%rbp)
	subsd	-104(%rbp), %xmm1
	addsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -104(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-104(%rbp), %xmm1
	movsd	-80(%rbp), %xmm4
	mulsd	.LC3(%rip), %xmm0
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	divsd	%xmm1, %xmm4
	movapd	%xmm4, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-72(%rbp), %xmm3
	subsd	-96(%rbp), %xmm3
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, -104(%rbp)
	addsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm5
	divsd	.LC3(%rip), %xmm5
	movapd	%xmm5, %xmm0
	movsd	%xmm1, -112(%rbp)
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-112(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	mulsd	-104(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -112(%rbp)
	call	uprv_fabs_67@PLT
	movsd	%xmm0, -104(%rbp)
	movsd	-80(%rbp), %xmm0
	call	uprv_fabs_67@PLT
	movsd	-104(%rbp), %xmm2
	movsd	-112(%rbp), %xmm1
	comisd	%xmm0, %xmm2
	ja	.L338
	movsd	(%r12), %xmm7
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -104(%rbp)
	movsd	%xmm7, -80(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-80(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movsd	-104(%rbp), %xmm1
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_fabs_67@PLT
	comisd	.LC56(%rip), %xmm0
	jbe	.L330
	movsd	-96(%rbp), %xmm6
	movsd	48(%r12), %xmm0
	movsd	%xmm6, -104(%rbp)
.L262:
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	je	.L217
	movsd	32(%r12), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L218
	movsd	32(%r12), %xmm0
.L219:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L218:
	movsd	.LC7(%rip), %xmm0
	addsd	(%r12), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r12)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L335:
	movsd	32(%r12), %xmm0
	leaq	56(%r12), %r15
	leaq	48(%r12), %r13
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L206
	movsd	32(%r12), %xmm0
.L207:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L196:
	movsd	.LC7(%rip), %xmm0
	addsd	(%r12), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r12)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L337:
	movsd	32(%r12), %xmm0
	leaq	56(%r12), %r15
	leaq	48(%r12), %r13
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L215
	movsd	32(%r12), %xmm0
.L216:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L338:
	movq	.LC60(%rip), %rax
	movq	%rax, %xmm0
	call	uprv_ceil_67@PLT
	testb	%r14b, %r14b
	jne	.L222
	xorpd	.LC22(%rip), %xmm0
.L222:
	addsd	-120(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	movapd	%xmm0, %xmm1
	unpcklpd	%xmm1, %xmm1
	movups	%xmm1, 32(%r12)
	movups	%xmm1, 48(%r12)
	movups	%xmm1, 64(%r12)
	movups	%xmm1, 80(%r12)
	movups	%xmm1, 96(%r12)
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L339
.L223:
	movsd	-72(%rbp), %xmm1
	leaq	56(%r12), %r13
	leaq	48(%r12), %rbx
	movsd	48(%r12), %xmm5
	subsd	%xmm5, %xmm1
	movsd	%xmm5, -104(%rbp)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm1
	mulsd	.LC3(%rip), %xmm0
	movsd	(%r12), %xmm3
	movsd	%xmm3, -120(%rbp)
	subsd	%xmm0, %xmm1
	addsd	-88(%rbp), %xmm1
	mulsd	.LC59(%rip), %xmm1
	divsd	.LC3(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-120(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	movapd	%xmm0, %xmm1
	unpcklpd	%xmm1, %xmm1
	movups	%xmm1, 32(%r12)
	movups	%xmm1, 48(%r12)
	movups	%xmm1, 64(%r12)
	movups	%xmm1, 80(%r12)
	movups	%xmm1, 96(%r12)
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L226:
	movsd	48(%r12), %xmm1
	movsd	%xmm1, -96(%rbp)
	subsd	-104(%rbp), %xmm1
	addsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -104(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-104(%rbp), %xmm1
	movsd	-80(%rbp), %xmm5
	mulsd	.LC3(%rip), %xmm0
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	divsd	%xmm1, %xmm5
	movapd	%xmm5, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-72(%rbp), %xmm4
	subsd	-96(%rbp), %xmm4
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, -104(%rbp)
	addsd	%xmm4, %xmm1
	movapd	%xmm1, %xmm7
	divsd	.LC3(%rip), %xmm7
	movapd	%xmm7, %xmm0
	movsd	%xmm1, -112(%rbp)
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-112(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	mulsd	-104(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -112(%rbp)
	call	uprv_fabs_67@PLT
	movsd	%xmm0, -104(%rbp)
	movsd	-80(%rbp), %xmm0
	call	uprv_fabs_67@PLT
	movsd	-104(%rbp), %xmm2
	movsd	-112(%rbp), %xmm1
	comisd	%xmm0, %xmm2
	ja	.L340
	movsd	(%r12), %xmm4
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -104(%rbp)
	movsd	%xmm4, -80(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-80(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movsd	-104(%rbp), %xmm1
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_fabs_67@PLT
	comisd	.LC56(%rip), %xmm0
	jbe	.L330
	movsd	-96(%rbp), %xmm5
	movsd	48(%r12), %xmm0
	movsd	%xmm5, -104(%rbp)
.L259:
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	je	.L226
	movsd	32(%r12), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L227
	movsd	32(%r12), %xmm0
.L228:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L227:
	movsd	.LC7(%rip), %xmm0
	addsd	(%r12), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r12)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L206:
	movsd	.LC7(%rip), %xmm0
	addsd	(%r12), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r12)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L340:
	movq	.LC60(%rip), %rax
	movq	%rax, %xmm0
	call	uprv_ceil_67@PLT
	testb	%r14b, %r14b
	jne	.L231
	xorpd	.LC22(%rip), %xmm0
.L231:
	addsd	-120(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	movapd	%xmm0, %xmm1
	unpcklpd	%xmm1, %xmm1
	movups	%xmm1, 32(%r12)
	movups	%xmm1, 48(%r12)
	movups	%xmm1, 64(%r12)
	movups	%xmm1, 80(%r12)
	movups	%xmm1, 96(%r12)
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L341
.L232:
	movsd	-72(%rbp), %xmm1
	leaq	56(%r12), %r13
	leaq	48(%r12), %rbx
	movsd	48(%r12), %xmm6
	subsd	%xmm6, %xmm1
	movsd	%xmm6, -104(%rbp)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm1
	mulsd	.LC3(%rip), %xmm0
	movsd	(%r12), %xmm7
	movsd	%xmm7, -120(%rbp)
	subsd	%xmm0, %xmm1
	addsd	-88(%rbp), %xmm1
	mulsd	.LC59(%rip), %xmm1
	divsd	.LC3(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-120(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	movapd	%xmm0, %xmm1
	unpcklpd	%xmm1, %xmm1
	movups	%xmm1, 32(%r12)
	movups	%xmm1, 48(%r12)
	movups	%xmm1, 64(%r12)
	movups	%xmm1, 80(%r12)
	movups	%xmm1, 96(%r12)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L235:
	movsd	48(%r12), %xmm1
	movsd	%xmm1, -96(%rbp)
	subsd	-104(%rbp), %xmm1
	addsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -104(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-104(%rbp), %xmm1
	movsd	-80(%rbp), %xmm3
	mulsd	.LC3(%rip), %xmm0
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	divsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-72(%rbp), %xmm5
	subsd	-96(%rbp), %xmm5
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, -104(%rbp)
	addsd	%xmm5, %xmm1
	movapd	%xmm1, %xmm7
	divsd	.LC3(%rip), %xmm7
	movapd	%xmm7, %xmm0
	movsd	%xmm1, -112(%rbp)
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-112(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	mulsd	-104(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -112(%rbp)
	call	uprv_fabs_67@PLT
	movsd	%xmm0, -104(%rbp)
	movsd	-80(%rbp), %xmm0
	call	uprv_fabs_67@PLT
	movsd	-104(%rbp), %xmm2
	movsd	-112(%rbp), %xmm1
	comisd	%xmm0, %xmm2
	ja	.L342
	movsd	(%r12), %xmm5
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -104(%rbp)
	movsd	%xmm5, -80(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-80(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movsd	-104(%rbp), %xmm1
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_fabs_67@PLT
	comisd	.LC56(%rip), %xmm0
	jbe	.L330
	movsd	-96(%rbp), %xmm7
	movsd	48(%r12), %xmm0
	movsd	%xmm7, -104(%rbp)
.L256:
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	je	.L235
	movsd	32(%r12), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L236
	movsd	32(%r12), %xmm0
.L237:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L236:
	movsd	.LC7(%rip), %xmm0
	addsd	(%r12), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r12)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L215:
	movsd	.LC7(%rip), %xmm0
	addsd	(%r12), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r12)
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L341:
	movsd	32(%r12), %xmm0
	leaq	56(%r12), %r15
	leaq	48(%r12), %r13
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L233
	movsd	32(%r12), %xmm0
.L234:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L339:
	movsd	32(%r12), %xmm0
	leaq	56(%r12), %r15
	leaq	48(%r12), %r13
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L224
	movsd	32(%r12), %xmm0
.L225:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L342:
	movq	.LC60(%rip), %rax
	movq	%rax, %xmm0
	call	uprv_ceil_67@PLT
	testb	%r14b, %r14b
	jne	.L240
	xorpd	.LC22(%rip), %xmm0
.L240:
	addsd	-120(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	movapd	%xmm0, %xmm1
	unpcklpd	%xmm1, %xmm1
	movups	%xmm1, 32(%r12)
	movups	%xmm1, 48(%r12)
	movups	%xmm1, 64(%r12)
	movups	%xmm1, 80(%r12)
	movups	%xmm1, 96(%r12)
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L343
.L241:
	movsd	-72(%rbp), %xmm1
	leaq	56(%r12), %r13
	leaq	48(%r12), %rbx
	movsd	48(%r12), %xmm2
	subsd	%xmm2, %xmm1
	movsd	%xmm2, -96(%rbp)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm1
	mulsd	.LC3(%rip), %xmm0
	movsd	(%r12), %xmm4
	movsd	%xmm4, -112(%rbp)
	subsd	%xmm0, %xmm1
	addsd	-88(%rbp), %xmm1
	mulsd	.LC59(%rip), %xmm1
	divsd	.LC3(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-112(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	movapd	%xmm0, %xmm1
	unpcklpd	%xmm1, %xmm1
	movups	%xmm1, 32(%r12)
	movups	%xmm1, 48(%r12)
	movups	%xmm1, 64(%r12)
	movups	%xmm1, 80(%r12)
	movups	%xmm1, 96(%r12)
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L244:
	movsd	48(%r12), %xmm1
	movsd	%xmm1, -88(%rbp)
	subsd	-96(%rbp), %xmm1
	addsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -96(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-96(%rbp), %xmm1
	movsd	-80(%rbp), %xmm5
	mulsd	.LC3(%rip), %xmm0
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	divsd	%xmm1, %xmm5
	movapd	%xmm5, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-72(%rbp), %xmm3
	subsd	-88(%rbp), %xmm3
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, -96(%rbp)
	addsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm4
	divsd	.LC3(%rip), %xmm4
	movapd	%xmm4, %xmm0
	movsd	%xmm1, -104(%rbp)
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-104(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	mulsd	-96(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -104(%rbp)
	call	uprv_fabs_67@PLT
	movsd	%xmm0, -96(%rbp)
	movsd	-80(%rbp), %xmm0
	call	uprv_fabs_67@PLT
	movsd	-96(%rbp), %xmm2
	movsd	-104(%rbp), %xmm1
	comisd	%xmm0, %xmm2
	ja	.L344
	movsd	(%r12), %xmm4
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -96(%rbp)
	movsd	%xmm4, -80(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-80(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movsd	-96(%rbp), %xmm1
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_fabs_67@PLT
	comisd	.LC56(%rip), %xmm0
	jbe	.L330
	movsd	-88(%rbp), %xmm5
	movsd	48(%r12), %xmm0
	movsd	%xmm5, -96(%rbp)
.L253:
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	je	.L244
	movsd	32(%r12), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L245
	movsd	32(%r12), %xmm0
.L246:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L245:
	movsd	.LC7(%rip), %xmm0
	addsd	(%r12), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r12)
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L224:
	movsd	.LC7(%rip), %xmm0
	addsd	(%r12), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r12)
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L344:
	movq	.LC60(%rip), %rax
	movq	%rax, %xmm0
	call	uprv_ceil_67@PLT
	testb	%r14b, %r14b
	jne	.L249
	xorpd	.LC22(%rip), %xmm0
.L249:
	addsd	-112(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movsbl	%r14b, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	unpcklpd	%xmm0, %xmm0
	movq	.LC56(%rip), %rax
	movb	$0, 128(%r12)
	movups	%xmm0, 32(%r12)
	movsd	.LC61(%rip), %xmm1
	movups	%xmm0, 48(%r12)
	movq	%rax, %xmm2
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movsd	-72(%rbp), %xmm0
	call	_ZN6icu_6718CalendarAstronomer11timeOfAngleERNS0_9AngleFuncEddda
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L343:
	movsd	32(%r12), %xmm0
	leaq	56(%r12), %r15
	leaq	48(%r12), %r13
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L242
	movsd	32(%r12), %xmm0
.L243:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getSunLongitudeEdRdS1_
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L233:
	movsd	.LC7(%rip), %xmm0
	addsd	(%r12), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r12)
	jmp	.L234
.L242:
	movsd	.LC7(%rip), %xmm0
	addsd	(%r12), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm0, 32(%r12)
	jmp	.L243
.L334:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3379:
	.size	_ZN6icu_6718CalendarAstronomer10getSunTimeEda, .-_ZN6icu_6718CalendarAstronomer10getSunTimeEda
	.align 2
	.p2align 4
	.type	_ZN6icu_6718CalendarAstronomer11timeOfAngleERNS0_9AngleFuncEddda.constprop.0, @function
_ZN6icu_6718CalendarAstronomer11timeOfAngleERNS0_9AngleFuncEddda.constprop.0:
.LFB4362:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$72, %rsp
	movsd	%xmm0, -56(%rbp)
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%r12), %xmm1
	subsd	48(%r12), %xmm1
	movapd	%xmm1, %xmm4
	divsd	.LC3(%rip), %xmm4
	movapd	%xmm4, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-40(%rbp), %xmm1
	movapd	%xmm1, %xmm7
	movsd	-56(%rbp), %xmm1
	subsd	%xmm0, %xmm7
	subsd	%xmm7, %xmm1
	movsd	%xmm7, -48(%rbp)
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-40(%rbp), %xmm1
	pxor	%xmm4, %xmm4
	mulsd	.LC3(%rip), %xmm0
	movsd	%xmm4, -88(%rbp)
	subsd	%xmm0, %xmm1
	testb	%bl, %bl
	jne	.L346
	movsd	.LC57(%rip), %xmm4
	movsd	%xmm4, -88(%rbp)
.L346:
	movsd	(%r12), %xmm5
	addsd	-88(%rbp), %xmm1
	mulsd	.LC62(%rip), %xmm1
	divsd	.LC3(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	movsd	%xmm5, -96(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-96(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L391:
	movsd	(%r12), %xmm4
	movsd	-40(%rbp), %xmm0
	movsd	%xmm4, -64(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-64(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movsd	-40(%rbp), %xmm0
	call	uprv_fabs_67@PLT
	comisd	.LC56(%rip), %xmm0
	jbe	.L399
.L375:
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%r12), %xmm1
	subsd	48(%r12), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-64(%rbp), %xmm1
	movsd	-48(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -48(%rbp)
	subsd	%xmm2, %xmm1
	addsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-64(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	movsd	-40(%rbp), %xmm0
	divsd	%xmm1, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-56(%rbp), %xmm5
	subsd	-48(%rbp), %xmm5
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, -64(%rbp)
	addsd	%xmm5, %xmm1
	movapd	%xmm1, %xmm6
	divsd	.LC3(%rip), %xmm6
	movapd	%xmm6, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm1
	movsd	-40(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	movsd	%xmm2, -72(%rbp)
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	mulsd	-64(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	uprv_fabs_67@PLT
	movsd	-72(%rbp), %xmm2
	movsd	%xmm0, -64(%rbp)
	movapd	%xmm2, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-64(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L391
	movq	.LC63(%rip), %rax
	movq	%rax, %xmm0
	call	uprv_ceil_67@PLT
	testb	%bl, %bl
	je	.L400
.L349:
	addsd	-96(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movq	%r12, %rdi
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%r12), %xmm1
	subsd	48(%r12), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-40(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm7
	movsd	%xmm1, -48(%rbp)
	movsd	-56(%rbp), %xmm1
	subsd	%xmm7, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-40(%rbp), %xmm1
	mulsd	.LC3(%rip), %xmm0
	movsd	(%r12), %xmm4
	movsd	%xmm4, -96(%rbp)
	subsd	%xmm0, %xmm1
	addsd	-88(%rbp), %xmm1
	mulsd	.LC62(%rip), %xmm1
	divsd	.LC3(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-96(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L392:
	movsd	(%r12), %xmm6
	movsd	-40(%rbp), %xmm0
	movsd	%xmm6, -64(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-64(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movsd	-40(%rbp), %xmm0
	call	uprv_fabs_67@PLT
	comisd	.LC56(%rip), %xmm0
	jbe	.L399
.L374:
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%r12), %xmm1
	subsd	48(%r12), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-64(%rbp), %xmm1
	movsd	-48(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -48(%rbp)
	subsd	%xmm2, %xmm1
	addsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-64(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	movsd	-40(%rbp), %xmm0
	divsd	%xmm1, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-56(%rbp), %xmm7
	subsd	-48(%rbp), %xmm7
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, -64(%rbp)
	addsd	%xmm7, %xmm1
	movapd	%xmm1, %xmm3
	divsd	.LC3(%rip), %xmm3
	movapd	%xmm3, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm1
	movsd	-40(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	movsd	%xmm2, -72(%rbp)
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	mulsd	-64(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	uprv_fabs_67@PLT
	movsd	-72(%rbp), %xmm2
	movsd	%xmm0, -64(%rbp)
	movapd	%xmm2, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-64(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L392
	movq	.LC63(%rip), %rax
	movq	%rax, %xmm0
	call	uprv_ceil_67@PLT
	testb	%bl, %bl
	je	.L401
.L352:
	addsd	-96(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movq	%r12, %rdi
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%r12), %xmm1
	subsd	48(%r12), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-40(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm5
	movsd	%xmm1, -48(%rbp)
	movsd	-56(%rbp), %xmm1
	subsd	%xmm5, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-40(%rbp), %xmm1
	mulsd	.LC3(%rip), %xmm0
	movsd	(%r12), %xmm5
	movsd	%xmm5, -96(%rbp)
	subsd	%xmm0, %xmm1
	addsd	-88(%rbp), %xmm1
	mulsd	.LC62(%rip), %xmm1
	divsd	.LC3(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-96(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L393:
	movsd	(%r12), %xmm3
	movsd	-40(%rbp), %xmm0
	movsd	%xmm3, -64(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-64(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movsd	-40(%rbp), %xmm0
	call	uprv_fabs_67@PLT
	comisd	.LC56(%rip), %xmm0
	jbe	.L399
.L373:
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%r12), %xmm1
	subsd	48(%r12), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-64(%rbp), %xmm1
	movsd	-48(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -48(%rbp)
	subsd	%xmm2, %xmm1
	addsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-64(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	movsd	-40(%rbp), %xmm0
	divsd	%xmm1, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-56(%rbp), %xmm7
	subsd	-48(%rbp), %xmm7
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, -64(%rbp)
	addsd	%xmm7, %xmm1
	movapd	%xmm1, %xmm5
	divsd	.LC3(%rip), %xmm5
	movapd	%xmm5, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm1
	movsd	-40(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	movsd	%xmm2, -72(%rbp)
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	mulsd	-64(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	uprv_fabs_67@PLT
	movsd	-72(%rbp), %xmm2
	movsd	%xmm0, -64(%rbp)
	movapd	%xmm2, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-64(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L393
	movq	.LC63(%rip), %rax
	movq	%rax, %xmm0
	call	uprv_ceil_67@PLT
	testb	%bl, %bl
	jne	.L355
	xorpd	.LC22(%rip), %xmm0
.L355:
	addsd	-96(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movq	%r12, %rdi
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%r12), %xmm1
	subsd	48(%r12), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-40(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm5
	movsd	%xmm1, -48(%rbp)
	movsd	-56(%rbp), %xmm1
	subsd	%xmm5, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-40(%rbp), %xmm1
	mulsd	.LC3(%rip), %xmm0
	movsd	(%r12), %xmm3
	movsd	%xmm3, -96(%rbp)
	subsd	%xmm0, %xmm1
	addsd	-88(%rbp), %xmm1
	mulsd	.LC62(%rip), %xmm1
	divsd	.LC3(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-96(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L394:
	movsd	(%r12), %xmm6
	movsd	-40(%rbp), %xmm0
	movsd	%xmm6, -64(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-64(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movsd	-40(%rbp), %xmm0
	call	uprv_fabs_67@PLT
	comisd	.LC56(%rip), %xmm0
	jbe	.L399
.L372:
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%r12), %xmm1
	subsd	48(%r12), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-64(%rbp), %xmm1
	movsd	-48(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -48(%rbp)
	subsd	%xmm2, %xmm1
	addsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-64(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	movsd	-40(%rbp), %xmm0
	divsd	%xmm1, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-56(%rbp), %xmm5
	subsd	-48(%rbp), %xmm5
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, -64(%rbp)
	addsd	%xmm5, %xmm1
	movapd	%xmm1, %xmm6
	divsd	.LC3(%rip), %xmm6
	movapd	%xmm6, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm1
	movsd	-40(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	movsd	%xmm2, -72(%rbp)
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	mulsd	-64(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	uprv_fabs_67@PLT
	movsd	-72(%rbp), %xmm2
	movsd	%xmm0, -64(%rbp)
	movapd	%xmm2, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-64(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L394
	movq	.LC63(%rip), %rax
	movq	%rax, %xmm0
	call	uprv_ceil_67@PLT
	testb	%bl, %bl
	je	.L402
.L358:
	addsd	-96(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movq	%r12, %rdi
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%r12), %xmm1
	subsd	48(%r12), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-40(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm3
	movsd	%xmm1, -48(%rbp)
	movsd	-56(%rbp), %xmm1
	subsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-40(%rbp), %xmm1
	mulsd	.LC3(%rip), %xmm0
	movsd	(%r12), %xmm4
	movsd	%xmm4, -96(%rbp)
	subsd	%xmm0, %xmm1
	movsd	-88(%rbp), %xmm0
	addsd	%xmm1, %xmm0
	mulsd	.LC62(%rip), %xmm0
	divsd	.LC3(%rip), %xmm0
	movsd	%xmm0, -40(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-96(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L395:
	movsd	(%r12), %xmm6
	movsd	-40(%rbp), %xmm0
	movsd	%xmm6, -64(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-64(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movsd	-40(%rbp), %xmm0
	call	uprv_fabs_67@PLT
	comisd	.LC56(%rip), %xmm0
	jbe	.L399
.L371:
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%r12), %xmm1
	subsd	48(%r12), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-64(%rbp), %xmm1
	movsd	-48(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -48(%rbp)
	subsd	%xmm2, %xmm1
	addsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-64(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	movsd	-40(%rbp), %xmm0
	divsd	%xmm1, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-56(%rbp), %xmm5
	subsd	-48(%rbp), %xmm5
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, -64(%rbp)
	addsd	%xmm5, %xmm1
	movapd	%xmm1, %xmm7
	divsd	.LC3(%rip), %xmm7
	movapd	%xmm7, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm1
	movsd	-40(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	movsd	%xmm2, -72(%rbp)
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	mulsd	-64(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	uprv_fabs_67@PLT
	movsd	-72(%rbp), %xmm2
	movsd	%xmm0, -64(%rbp)
	movapd	%xmm2, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-64(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L395
	movq	.LC63(%rip), %rax
	movq	%rax, %xmm0
	call	uprv_ceil_67@PLT
	testb	%bl, %bl
	jne	.L361
	xorpd	.LC22(%rip), %xmm0
.L361:
	addsd	-96(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movq	%r12, %rdi
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%r12), %xmm1
	subsd	48(%r12), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-40(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm7
	movsd	%xmm1, -48(%rbp)
	movsd	-56(%rbp), %xmm1
	subsd	%xmm7, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-40(%rbp), %xmm1
	mulsd	.LC3(%rip), %xmm0
	movsd	(%r12), %xmm4
	movsd	%xmm4, -96(%rbp)
	subsd	%xmm0, %xmm1
	movsd	-88(%rbp), %xmm0
	addsd	%xmm1, %xmm0
	mulsd	.LC62(%rip), %xmm0
	divsd	.LC3(%rip), %xmm0
	movsd	%xmm0, -40(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-96(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L396:
	movsd	(%r12), %xmm6
	movsd	-40(%rbp), %xmm0
	movsd	%xmm6, -64(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-64(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movsd	-40(%rbp), %xmm0
	call	uprv_fabs_67@PLT
	comisd	.LC56(%rip), %xmm0
	jbe	.L399
.L370:
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%r12), %xmm1
	subsd	48(%r12), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-64(%rbp), %xmm1
	movsd	-48(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -48(%rbp)
	subsd	%xmm2, %xmm1
	addsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-64(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	movsd	-40(%rbp), %xmm0
	divsd	%xmm1, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-56(%rbp), %xmm7
	subsd	-48(%rbp), %xmm7
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, -64(%rbp)
	addsd	%xmm7, %xmm1
	movapd	%xmm1, %xmm3
	divsd	.LC3(%rip), %xmm3
	movapd	%xmm3, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm1
	movsd	-40(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	movsd	%xmm2, -72(%rbp)
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	mulsd	-64(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	uprv_fabs_67@PLT
	movsd	-72(%rbp), %xmm2
	movsd	%xmm0, -64(%rbp)
	movapd	%xmm2, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-64(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L396
	movq	.LC63(%rip), %rax
	movq	%rax, %xmm0
	call	uprv_ceil_67@PLT
	testb	%bl, %bl
	jne	.L364
	xorpd	.LC22(%rip), %xmm0
.L364:
	addsd	-96(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movq	%r12, %rdi
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%r12), %xmm1
	subsd	48(%r12), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-40(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm3
	movsd	%xmm1, -48(%rbp)
	movsd	-56(%rbp), %xmm1
	subsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-40(%rbp), %xmm1
	mulsd	.LC3(%rip), %xmm0
	movsd	(%r12), %xmm4
	subsd	%xmm0, %xmm1
	movsd	-88(%rbp), %xmm0
	movsd	%xmm4, -88(%rbp)
	addsd	%xmm1, %xmm0
	mulsd	.LC62(%rip), %xmm0
	divsd	.LC3(%rip), %xmm0
	movsd	%xmm0, -40(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-88(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L397:
	movsd	(%r12), %xmm5
	movsd	-40(%rbp), %xmm0
	movsd	%xmm5, -64(%rbp)
	call	uprv_ceil_67@PLT
	addsd	-64(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%r12)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movsd	-40(%rbp), %xmm0
	call	uprv_fabs_67@PLT
	comisd	.LC56(%rip), %xmm0
	jbe	.L399
.L368:
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomer15getMoonPositionEv
	movsd	72(%r12), %xmm1
	subsd	48(%r12), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-64(%rbp), %xmm1
	movsd	-48(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -48(%rbp)
	subsd	%xmm2, %xmm1
	addsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -64(%rbp)
	divsd	.LC3(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC3(%rip), %xmm0
	movsd	-64(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	movsd	-40(%rbp), %xmm0
	divsd	%xmm1, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-56(%rbp), %xmm7
	subsd	-48(%rbp), %xmm7
	movsd	.LC2(%rip), %xmm1
	movsd	%xmm0, -64(%rbp)
	addsd	%xmm7, %xmm1
	movapd	%xmm1, %xmm4
	divsd	.LC3(%rip), %xmm4
	movapd	%xmm4, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm1
	movsd	-40(%rbp), %xmm2
	mulsd	.LC3(%rip), %xmm0
	movsd	%xmm2, -72(%rbp)
	subsd	%xmm0, %xmm1
	subsd	.LC2(%rip), %xmm1
	mulsd	-64(%rbp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	uprv_fabs_67@PLT
	movsd	-72(%rbp), %xmm2
	movsd	%xmm0, -64(%rbp)
	movapd	%xmm2, %xmm0
	call	uprv_fabs_67@PLT
	movsd	-64(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L397
	movq	.LC63(%rip), %rax
	movq	%rax, %xmm0
	call	uprv_ceil_67@PLT
	testb	%bl, %bl
	jne	.L367
	xorpd	.LC22(%rip), %xmm0
.L367:
	addsd	-88(%rbp), %xmm0
	movsd	%xmm0, (%r12)
	call	uprv_getNaN_67@PLT
	movsbl	%bl, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	unpcklpd	%xmm0, %xmm0
	movq	.LC56(%rip), %rax
	movb	$0, 128(%r12)
	movups	%xmm0, 32(%r12)
	movsd	.LC64(%rip), %xmm1
	movups	%xmm0, 48(%r12)
	movq	%rax, %xmm2
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movsd	-56(%rbp), %xmm0
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6718CalendarAstronomer11timeOfAngleERNS0_9AngleFuncEddda
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	movsd	(%r12), %xmm0
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	xorpd	.LC22(%rip), %xmm0
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L401:
	xorpd	.LC22(%rip), %xmm0
	jmp	.L352
.L402:
	xorpd	.LC22(%rip), %xmm0
	jmp	.L358
	.cfi_endproc
.LFE4362:
	.size	_ZN6icu_6718CalendarAstronomer11timeOfAngleERNS0_9AngleFuncEddda.constprop.0, .-_ZN6icu_6718CalendarAstronomer11timeOfAngleERNS0_9AngleFuncEddda.constprop.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer11getMoonTimeEda
	.type	_ZN6icu_6718CalendarAstronomer11getMoonTimeEda, @function
_ZN6icu_6718CalendarAstronomer11getMoonTimeEda:
.LFB3412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsbl	%sil, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rsi
	leaq	16+_ZTVN6icu_6717MoonTimeAngleFuncE(%rip), %rax
	movq	%rax, -16(%rbp)
	call	_ZN6icu_6718CalendarAstronomer11timeOfAngleERNS0_9AngleFuncEddda.constprop.0
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L406
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L406:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3412:
	.size	_ZN6icu_6718CalendarAstronomer11getMoonTimeEda, .-_ZN6icu_6718CalendarAstronomer11getMoonTimeEda
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer11getMoonTimeERKNS0_7MoonAgeEa
	.type	_ZN6icu_6718CalendarAstronomer11getMoonTimeERKNS0_7MoonAgeEa, @function
_ZN6icu_6718CalendarAstronomer11getMoonTimeERKNS0_7MoonAgeEa:
.LFB3416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsbl	%dl, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movsd	(%rsi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rsi
	leaq	16+_ZTVN6icu_6717MoonTimeAngleFuncE(%rip), %rax
	movq	%rax, -16(%rbp)
	call	_ZN6icu_6718CalendarAstronomer11timeOfAngleERNS0_9AngleFuncEddda.constprop.0
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L410
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L410:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3416:
	.size	_ZN6icu_6718CalendarAstronomer11getMoonTimeERKNS0_7MoonAgeEa, .-_ZN6icu_6718CalendarAstronomer11getMoonTimeERKNS0_7MoonAgeEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer9riseOrSetERNS0_9CoordFuncEaddd
	.type	_ZN6icu_6718CalendarAstronomer9riseOrSetERNS0_9CoordFuncEaddd, @function
_ZN6icu_6718CalendarAstronomer9riseOrSetERNS0_9CoordFuncEaddd:
.LFB3427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	$5, %ebx
	subq	$88, %rsp
	movl	%edx, -108(%rbp)
	movsd	%xmm0, -120(%rbp)
	pxor	%xmm0, %xmm0
	movsd	%xmm1, -128(%rbp)
	movsd	%xmm2, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movsd	16(%rdi), %xmm0
	call	tan@PLT
	xorpd	.LC22(%rip), %xmm0
	movsd	%xmm0, -96(%rbp)
.L416:
	movq	(%r12), %rax
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*(%rax)
	movsd	-72(%rbp), %xmm0
	call	tan@PLT
	mulsd	-96(%rbp), %xmm0
	call	acos@PLT
	testb	%r14b, %r14b
	je	.L412
	movsd	.LC3(%rip), %xmm5
	subsd	%xmm0, %xmm5
	movapd	%xmm5, %xmm0
.L412:
	movsd	-80(%rbp), %xmm1
	movq	%r15, %rdi
	addsd	%xmm0, %xmm1
	mulsd	.LC4(%rip), %xmm1
	divsd	.LC3(%rip), %xmm1
	movsd	%xmm1, -88(%rbp)
	call	_ZN6icu_6718CalendarAstronomer17getSiderealOffsetEv
	movsd	-88(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	mulsd	.LC16(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -88(%rbp)
	divsd	.LC4(%rip), %xmm0
	call	uprv_floor_67@PLT
	mulsd	.LC4(%rip), %xmm0
	movsd	-88(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movsd	(%r15), %xmm0
	addsd	24(%r15), %xmm0
	divsd	.LC6(%rip), %xmm0
	movsd	%xmm1, -88(%rbp)
	call	uprv_floor_67@PLT
	movsd	-88(%rbp), %xmm1
	mulsd	.LC5(%rip), %xmm1
	mulsd	.LC6(%rip), %xmm0
	subsd	24(%r15), %xmm0
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm2
	subsd	(%r15), %xmm2
	movsd	%xmm1, (%r15)
	movsd	%xmm2, -88(%rbp)
	call	uprv_getNaN_67@PLT
	subl	$1, %ebx
	movsd	-88(%rbp), %xmm2
	movb	$0, 128(%r15)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%r15)
	movups	%xmm0, 48(%r15)
	movups	%xmm0, 64(%r15)
	movups	%xmm0, 80(%r15)
	movups	%xmm0, 96(%r15)
	je	.L417
	movapd	%xmm2, %xmm0
	call	uprv_fabs_67@PLT
	comisd	-104(%rbp), %xmm0
	ja	.L416
	.p2align 4,,10
	.p2align 3
.L417:
	movsd	-72(%rbp), %xmm0
	call	cos@PLT
	movsd	%xmm0, -88(%rbp)
	movsd	16(%r15), %xmm0
	call	sin@PLT
	divsd	-88(%rbp), %xmm0
	call	acos@PLT
	movsd	%xmm0, -104(%rbp)
	movsd	-120(%rbp), %xmm0
	mulsd	.LC10(%rip), %xmm0
	addsd	-128(%rbp), %xmm0
	call	sin@PLT
	movsd	-104(%rbp), %xmm2
	movsd	%xmm0, -96(%rbp)
	movapd	%xmm2, %xmm0
	call	sin@PLT
	movsd	-96(%rbp), %xmm1
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	call	asin@PLT
	cmpb	$0, -108(%rbp)
	movsd	(%r15), %xmm1
	mulsd	.LC38(%rip), %xmm0
	mulsd	.LC39(%rip), %xmm0
	divsd	-88(%rbp), %xmm0
	mulsd	.LC40(%rip), %xmm0
	cvttsd2siq	%xmm0, %rax
	je	.L426
	negq	%rax
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L418:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	addsd	%xmm1, %xmm0
	jne	.L427
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	jmp	.L418
.L427:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3427:
	.size	_ZN6icu_6718CalendarAstronomer9riseOrSetERNS0_9CoordFuncEaddd, .-_ZN6icu_6718CalendarAstronomer9riseOrSetERNS0_9CoordFuncEaddd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer17eclipticObliquityEv
	.type	_ZN6icu_6718CalendarAstronomer17eclipticObliquityEv, @function
_ZN6icu_6718CalendarAstronomer17eclipticObliquityEv:
.LFB3428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movsd	88(%rdi), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L429
	movsd	88(%rbx), %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	movsd	32(%rbx), %xmm0
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L431
	movsd	32(%rbx), %xmm1
.L432:
	movsd	.LC17(%rip), %xmm2
	subsd	.LC11(%rip), %xmm1
	divsd	.LC9(%rip), %xmm1
	movsd	.LC18(%rip), %xmm0
	mulsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm0
	movsd	.LC19(%rip), %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm0
	movsd	.LC20(%rip), %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm1
	addsd	%xmm1, %xmm0
	mulsd	.LC1(%rip), %xmm0
	movsd	%xmm0, 88(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	.cfi_restore_state
	movsd	.LC7(%rip), %xmm1
	addsd	(%rbx), %xmm1
	divsd	.LC6(%rip), %xmm1
	movsd	%xmm1, 32(%rbx)
	jmp	.L432
	.cfi_endproc
.LFE3428:
	.size	_ZN6icu_6718CalendarAstronomer17eclipticObliquityEv, .-_ZN6icu_6718CalendarAstronomer17eclipticObliquityEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CalendarAstronomer10clearCacheEv
	.type	_ZN6icu_6718CalendarAstronomer10clearCacheEv, @function
_ZN6icu_6718CalendarAstronomer10clearCacheEv:
.LFB3429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	uprv_getNaN_67@PLT
	movb	$0, 128(%rbx)
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3429:
	.size	_ZN6icu_6718CalendarAstronomer10clearCacheEv, .-_ZN6icu_6718CalendarAstronomer10clearCacheEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718CalendarAstronomer8Ecliptic8toStringEv
	.type	_ZNK6icu_6718CalendarAstronomer8Ecliptic8toStringEv, @function
_ZNK6icu_6718CalendarAstronomer8Ecliptic8toStringEv:
.LFB3430:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 8(%rdi)
	ret
	.cfi_endproc
.LFE3430:
	.size	_ZNK6icu_6718CalendarAstronomer8Ecliptic8toStringEv, .-_ZNK6icu_6718CalendarAstronomer8Ecliptic8toStringEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718CalendarAstronomer10Equatorial8toStringEv
	.type	_ZNK6icu_6718CalendarAstronomer10Equatorial8toStringEv, @function
_ZNK6icu_6718CalendarAstronomer10Equatorial8toStringEv:
.LFB4358:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 8(%rdi)
	ret
	.cfi_endproc
.LFE4358:
	.size	_ZNK6icu_6718CalendarAstronomer10Equatorial8toStringEv, .-_ZNK6icu_6718CalendarAstronomer10Equatorial8toStringEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718CalendarAstronomer7Horizon8toStringEv
	.type	_ZNK6icu_6718CalendarAstronomer7Horizon8toStringEv, @function
_ZNK6icu_6718CalendarAstronomer7Horizon8toStringEv:
.LFB4360:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 8(%rdi)
	ret
	.cfi_endproc
.LFE4360:
	.size	_ZNK6icu_6718CalendarAstronomer7Horizon8toStringEv, .-_ZNK6icu_6718CalendarAstronomer7Horizon8toStringEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CalendarCache11createCacheEPPS0_R10UErrorCode
	.type	_ZN6icu_6713CalendarCache11createCacheEPPS0_R10UErrorCode, @function
_ZN6icu_6713CalendarCache11createCacheEPPS0_R10UErrorCode:
.LFB3433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	calendar_astro_cleanup(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$11, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	testq	%rbx, %rbx
	je	.L452
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L442
	leaq	16+_ZTVN6icu_6713CalendarCacheE(%rip), %r14
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	movq	%r13, %r8
	xorl	%edx, %edx
	movq	%r14, (%rax)
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	movl	$32, %ecx
	call	uhash_openSize_67@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	movl	0(%r13), %eax
	movq	%r12, (%rbx)
	testl	%eax, %eax
	jle	.L439
	movq	(%r12), %rax
	leaq	_ZN6icu_6713CalendarCacheD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L444
	movq	%r14, (%r12)
	testq	%rdi, %rdi
	je	.L445
	call	uhash_close_67@PLT
.L445:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L446:
	movq	$0, (%rbx)
.L439:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	movl	$7, 0(%r13)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L444:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L446
.L442:
	cmpl	$0, 0(%r13)
	movq	$0, (%rbx)
	jg	.L446
	jmp	.L439
	.cfi_endproc
.LFE3433:
	.size	_ZN6icu_6713CalendarCache11createCacheEPPS0_R10UErrorCode, .-_ZN6icu_6713CalendarCache11createCacheEPPS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode
	.type	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode, @function
_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode:
.LFB3434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdx), %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%ecx, %ecx
	jle	.L460
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_restore_state
	movq	%rdi, %r12
	leaq	_ZL6ccLock(%rip), %rdi
	movl	%esi, %r13d
	movq	%rdx, %rbx
	call	umtx_lock_67@PLT
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L461
.L455:
	movq	8(%rax), %rdi
	movl	%r13d, %esi
	call	uhash_igeti_67@PLT
	movl	%eax, %r14d
.L459:
	leaq	_ZL6ccLock(%rip), %rdi
	call	umtx_unlock_67@PLT
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713CalendarCache11createCacheEPPS0_R10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L459
	movq	(%r12), %rax
	jmp	.L455
	.cfi_endproc
.LFE3434:
	.size	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode, .-_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CalendarCache3putEPPS0_iiR10UErrorCode
	.type	_ZN6icu_6713CalendarCache3putEPPS0_iiR10UErrorCode, @function
_ZN6icu_6713CalendarCache3putEPPS0_iiR10UErrorCode:
.LFB3435:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jle	.L470
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	_ZL6ccLock(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L471
.L464:
	movq	8(%rax), %rdi
	movq	%r12, %rcx
	movl	%r14d, %edx
	movl	%r13d, %esi
	call	uhash_iputi_67@PLT
.L469:
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	leaq	_ZL6ccLock(%rip), %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.p2align 4,,10
	.p2align 3
.L471:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713CalendarCache11createCacheEPPS0_R10UErrorCode
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L469
	movq	(%rbx), %rax
	jmp	.L464
	.cfi_endproc
.LFE3435:
	.size	_ZN6icu_6713CalendarCache3putEPPS0_iiR10UErrorCode, .-_ZN6icu_6713CalendarCache3putEPPS0_iiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CalendarCacheC2EiR10UErrorCode
	.type	_ZN6icu_6713CalendarCacheC2EiR10UErrorCode, @function
_ZN6icu_6713CalendarCacheC2EiR10UErrorCode:
.LFB3437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713CalendarCacheE(%rip), %rax
	movl	%esi, %ecx
	movq	%rdx, %r8
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	call	uhash_openSize_67@PLT
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3437:
	.size	_ZN6icu_6713CalendarCacheC2EiR10UErrorCode, .-_ZN6icu_6713CalendarCacheC2EiR10UErrorCode
	.globl	_ZN6icu_6713CalendarCacheC1EiR10UErrorCode
	.set	_ZN6icu_6713CalendarCacheC1EiR10UErrorCode,_ZN6icu_6713CalendarCacheC2EiR10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6718CalendarAstronomer9AngleFuncE
	.section	.rodata._ZTSN6icu_6718CalendarAstronomer9AngleFuncE,"aG",@progbits,_ZTSN6icu_6718CalendarAstronomer9AngleFuncE,comdat
	.align 32
	.type	_ZTSN6icu_6718CalendarAstronomer9AngleFuncE, @object
	.size	_ZTSN6icu_6718CalendarAstronomer9AngleFuncE, 40
_ZTSN6icu_6718CalendarAstronomer9AngleFuncE:
	.string	"N6icu_6718CalendarAstronomer9AngleFuncE"
	.weak	_ZTIN6icu_6718CalendarAstronomer9AngleFuncE
	.section	.data.rel.ro._ZTIN6icu_6718CalendarAstronomer9AngleFuncE,"awG",@progbits,_ZTIN6icu_6718CalendarAstronomer9AngleFuncE,comdat
	.align 8
	.type	_ZTIN6icu_6718CalendarAstronomer9AngleFuncE, @object
	.size	_ZTIN6icu_6718CalendarAstronomer9AngleFuncE, 24
_ZTIN6icu_6718CalendarAstronomer9AngleFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718CalendarAstronomer9AngleFuncE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6718CalendarAstronomer9CoordFuncE
	.section	.rodata._ZTSN6icu_6718CalendarAstronomer9CoordFuncE,"aG",@progbits,_ZTSN6icu_6718CalendarAstronomer9CoordFuncE,comdat
	.align 32
	.type	_ZTSN6icu_6718CalendarAstronomer9CoordFuncE, @object
	.size	_ZTSN6icu_6718CalendarAstronomer9CoordFuncE, 40
_ZTSN6icu_6718CalendarAstronomer9CoordFuncE:
	.string	"N6icu_6718CalendarAstronomer9CoordFuncE"
	.weak	_ZTIN6icu_6718CalendarAstronomer9CoordFuncE
	.section	.data.rel.ro._ZTIN6icu_6718CalendarAstronomer9CoordFuncE,"awG",@progbits,_ZTIN6icu_6718CalendarAstronomer9CoordFuncE,comdat
	.align 8
	.type	_ZTIN6icu_6718CalendarAstronomer9CoordFuncE, @object
	.size	_ZTIN6icu_6718CalendarAstronomer9CoordFuncE, 24
_ZTIN6icu_6718CalendarAstronomer9CoordFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718CalendarAstronomer9CoordFuncE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6713CalendarCacheE
	.section	.rodata._ZTSN6icu_6713CalendarCacheE,"aG",@progbits,_ZTSN6icu_6713CalendarCacheE,comdat
	.align 16
	.type	_ZTSN6icu_6713CalendarCacheE, @object
	.size	_ZTSN6icu_6713CalendarCacheE, 25
_ZTSN6icu_6713CalendarCacheE:
	.string	"N6icu_6713CalendarCacheE"
	.weak	_ZTIN6icu_6713CalendarCacheE
	.section	.data.rel.ro._ZTIN6icu_6713CalendarCacheE,"awG",@progbits,_ZTIN6icu_6713CalendarCacheE,comdat
	.align 8
	.type	_ZTIN6icu_6713CalendarCacheE, @object
	.size	_ZTIN6icu_6713CalendarCacheE, 24
_ZTIN6icu_6713CalendarCacheE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713CalendarCacheE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6716SunTimeAngleFuncE
	.section	.rodata._ZTSN6icu_6716SunTimeAngleFuncE,"aG",@progbits,_ZTSN6icu_6716SunTimeAngleFuncE,comdat
	.align 16
	.type	_ZTSN6icu_6716SunTimeAngleFuncE, @object
	.size	_ZTSN6icu_6716SunTimeAngleFuncE, 28
_ZTSN6icu_6716SunTimeAngleFuncE:
	.string	"N6icu_6716SunTimeAngleFuncE"
	.weak	_ZTIN6icu_6716SunTimeAngleFuncE
	.section	.data.rel.ro._ZTIN6icu_6716SunTimeAngleFuncE,"awG",@progbits,_ZTIN6icu_6716SunTimeAngleFuncE,comdat
	.align 8
	.type	_ZTIN6icu_6716SunTimeAngleFuncE, @object
	.size	_ZTIN6icu_6716SunTimeAngleFuncE, 24
_ZTIN6icu_6716SunTimeAngleFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6716SunTimeAngleFuncE
	.quad	_ZTIN6icu_6718CalendarAstronomer9AngleFuncE
	.weak	_ZTSN6icu_6716RiseSetCoordFuncE
	.section	.rodata._ZTSN6icu_6716RiseSetCoordFuncE,"aG",@progbits,_ZTSN6icu_6716RiseSetCoordFuncE,comdat
	.align 16
	.type	_ZTSN6icu_6716RiseSetCoordFuncE, @object
	.size	_ZTSN6icu_6716RiseSetCoordFuncE, 28
_ZTSN6icu_6716RiseSetCoordFuncE:
	.string	"N6icu_6716RiseSetCoordFuncE"
	.weak	_ZTIN6icu_6716RiseSetCoordFuncE
	.section	.data.rel.ro._ZTIN6icu_6716RiseSetCoordFuncE,"awG",@progbits,_ZTIN6icu_6716RiseSetCoordFuncE,comdat
	.align 8
	.type	_ZTIN6icu_6716RiseSetCoordFuncE, @object
	.size	_ZTIN6icu_6716RiseSetCoordFuncE, 24
_ZTIN6icu_6716RiseSetCoordFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6716RiseSetCoordFuncE
	.quad	_ZTIN6icu_6718CalendarAstronomer9CoordFuncE
	.weak	_ZTSN6icu_6717MoonTimeAngleFuncE
	.section	.rodata._ZTSN6icu_6717MoonTimeAngleFuncE,"aG",@progbits,_ZTSN6icu_6717MoonTimeAngleFuncE,comdat
	.align 16
	.type	_ZTSN6icu_6717MoonTimeAngleFuncE, @object
	.size	_ZTSN6icu_6717MoonTimeAngleFuncE, 29
_ZTSN6icu_6717MoonTimeAngleFuncE:
	.string	"N6icu_6717MoonTimeAngleFuncE"
	.weak	_ZTIN6icu_6717MoonTimeAngleFuncE
	.section	.data.rel.ro._ZTIN6icu_6717MoonTimeAngleFuncE,"awG",@progbits,_ZTIN6icu_6717MoonTimeAngleFuncE,comdat
	.align 8
	.type	_ZTIN6icu_6717MoonTimeAngleFuncE, @object
	.size	_ZTIN6icu_6717MoonTimeAngleFuncE, 24
_ZTIN6icu_6717MoonTimeAngleFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717MoonTimeAngleFuncE
	.quad	_ZTIN6icu_6718CalendarAstronomer9AngleFuncE
	.weak	_ZTSN6icu_6720MoonRiseSetCoordFuncE
	.section	.rodata._ZTSN6icu_6720MoonRiseSetCoordFuncE,"aG",@progbits,_ZTSN6icu_6720MoonRiseSetCoordFuncE,comdat
	.align 32
	.type	_ZTSN6icu_6720MoonRiseSetCoordFuncE, @object
	.size	_ZTSN6icu_6720MoonRiseSetCoordFuncE, 32
_ZTSN6icu_6720MoonRiseSetCoordFuncE:
	.string	"N6icu_6720MoonRiseSetCoordFuncE"
	.weak	_ZTIN6icu_6720MoonRiseSetCoordFuncE
	.section	.data.rel.ro._ZTIN6icu_6720MoonRiseSetCoordFuncE,"awG",@progbits,_ZTIN6icu_6720MoonRiseSetCoordFuncE,comdat
	.align 8
	.type	_ZTIN6icu_6720MoonRiseSetCoordFuncE, @object
	.size	_ZTIN6icu_6720MoonRiseSetCoordFuncE, 24
_ZTIN6icu_6720MoonRiseSetCoordFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6720MoonRiseSetCoordFuncE
	.quad	_ZTIN6icu_6718CalendarAstronomer9CoordFuncE
	.weak	_ZTVN6icu_6718CalendarAstronomer9AngleFuncE
	.section	.data.rel.ro._ZTVN6icu_6718CalendarAstronomer9AngleFuncE,"awG",@progbits,_ZTVN6icu_6718CalendarAstronomer9AngleFuncE,comdat
	.align 8
	.type	_ZTVN6icu_6718CalendarAstronomer9AngleFuncE, @object
	.size	_ZTVN6icu_6718CalendarAstronomer9AngleFuncE, 40
_ZTVN6icu_6718CalendarAstronomer9AngleFuncE:
	.quad	0
	.quad	_ZTIN6icu_6718CalendarAstronomer9AngleFuncE
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.weak	_ZTVN6icu_6716SunTimeAngleFuncE
	.section	.data.rel.ro.local._ZTVN6icu_6716SunTimeAngleFuncE,"awG",@progbits,_ZTVN6icu_6716SunTimeAngleFuncE,comdat
	.align 8
	.type	_ZTVN6icu_6716SunTimeAngleFuncE, @object
	.size	_ZTVN6icu_6716SunTimeAngleFuncE, 40
_ZTVN6icu_6716SunTimeAngleFuncE:
	.quad	0
	.quad	_ZTIN6icu_6716SunTimeAngleFuncE
	.quad	_ZN6icu_6716SunTimeAngleFunc4evalERNS_18CalendarAstronomerE
	.quad	_ZN6icu_6716SunTimeAngleFuncD1Ev
	.quad	_ZN6icu_6716SunTimeAngleFuncD0Ev
	.weak	_ZTVN6icu_6718CalendarAstronomer9CoordFuncE
	.section	.data.rel.ro._ZTVN6icu_6718CalendarAstronomer9CoordFuncE,"awG",@progbits,_ZTVN6icu_6718CalendarAstronomer9CoordFuncE,comdat
	.align 8
	.type	_ZTVN6icu_6718CalendarAstronomer9CoordFuncE, @object
	.size	_ZTVN6icu_6718CalendarAstronomer9CoordFuncE, 40
_ZTVN6icu_6718CalendarAstronomer9CoordFuncE:
	.quad	0
	.quad	_ZTIN6icu_6718CalendarAstronomer9CoordFuncE
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.weak	_ZTVN6icu_6716RiseSetCoordFuncE
	.section	.data.rel.ro.local._ZTVN6icu_6716RiseSetCoordFuncE,"awG",@progbits,_ZTVN6icu_6716RiseSetCoordFuncE,comdat
	.align 8
	.type	_ZTVN6icu_6716RiseSetCoordFuncE, @object
	.size	_ZTVN6icu_6716RiseSetCoordFuncE, 40
_ZTVN6icu_6716RiseSetCoordFuncE:
	.quad	0
	.quad	_ZTIN6icu_6716RiseSetCoordFuncE
	.quad	_ZN6icu_6716RiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_
	.quad	_ZN6icu_6716RiseSetCoordFuncD1Ev
	.quad	_ZN6icu_6716RiseSetCoordFuncD0Ev
	.weak	_ZTVN6icu_6717MoonTimeAngleFuncE
	.section	.data.rel.ro.local._ZTVN6icu_6717MoonTimeAngleFuncE,"awG",@progbits,_ZTVN6icu_6717MoonTimeAngleFuncE,comdat
	.align 8
	.type	_ZTVN6icu_6717MoonTimeAngleFuncE, @object
	.size	_ZTVN6icu_6717MoonTimeAngleFuncE, 40
_ZTVN6icu_6717MoonTimeAngleFuncE:
	.quad	0
	.quad	_ZTIN6icu_6717MoonTimeAngleFuncE
	.quad	_ZN6icu_6717MoonTimeAngleFunc4evalERNS_18CalendarAstronomerE
	.quad	_ZN6icu_6717MoonTimeAngleFuncD1Ev
	.quad	_ZN6icu_6717MoonTimeAngleFuncD0Ev
	.weak	_ZTVN6icu_6720MoonRiseSetCoordFuncE
	.section	.data.rel.ro.local._ZTVN6icu_6720MoonRiseSetCoordFuncE,"awG",@progbits,_ZTVN6icu_6720MoonRiseSetCoordFuncE,comdat
	.align 8
	.type	_ZTVN6icu_6720MoonRiseSetCoordFuncE, @object
	.size	_ZTVN6icu_6720MoonRiseSetCoordFuncE, 40
_ZTVN6icu_6720MoonRiseSetCoordFuncE:
	.quad	0
	.quad	_ZTIN6icu_6720MoonRiseSetCoordFuncE
	.quad	_ZN6icu_6720MoonRiseSetCoordFunc4evalERNS_18CalendarAstronomer10EquatorialERS1_
	.quad	_ZN6icu_6720MoonRiseSetCoordFuncD1Ev
	.quad	_ZN6icu_6720MoonRiseSetCoordFuncD0Ev
	.weak	_ZTVN6icu_6713CalendarCacheE
	.section	.data.rel.ro.local._ZTVN6icu_6713CalendarCacheE,"awG",@progbits,_ZTVN6icu_6713CalendarCacheE,comdat
	.align 8
	.type	_ZTVN6icu_6713CalendarCacheE, @object
	.size	_ZTVN6icu_6713CalendarCacheE, 32
_ZTVN6icu_6713CalendarCacheE:
	.quad	0
	.quad	_ZTIN6icu_6713CalendarCacheE
	.quad	_ZN6icu_6713CalendarCacheD1Ev
	.quad	_ZN6icu_6713CalendarCacheD0Ev
	.globl	_ZN6icu_6718CalendarAstronomer2PIE
	.section	.rodata
	.align 8
	.type	_ZN6icu_6718CalendarAstronomer2PIE, @object
	.size	_ZN6icu_6718CalendarAstronomer2PIE, 8
_ZN6icu_6718CalendarAstronomer2PIE:
	.long	1413754136
	.long	1074340347
	.globl	_ZN6icu_6718CalendarAstronomer13SYNODIC_MONTHE
	.align 8
	.type	_ZN6icu_6718CalendarAstronomer13SYNODIC_MONTHE, @object
	.size	_ZN6icu_6718CalendarAstronomer13SYNODIC_MONTHE, 8
_ZN6icu_6718CalendarAstronomer13SYNODIC_MONTHE:
	.long	2882224597
	.long	1077774292
	.local	_ZL6ccLock
	.comm	_ZL6ccLock,56,32
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	2723323193
	.long	1066524486
	.align 8
.LC2:
	.long	1413754136
	.long	1074340347
	.align 8
.LC3:
	.long	1413754136
	.long	1075388923
	.align 8
.LC4:
	.long	0
	.long	1077411840
	.align 8
.LC5:
	.long	0
	.long	1095464768
	.align 8
.LC6:
	.long	0
	.long	1100257648
	.align 8
.LC7:
	.long	3395567616
	.long	1122498823
	.align 8
.LC8:
	.long	0
	.long	1094872278
	.align 8
.LC9:
	.long	0
	.long	1088542112
	.align 8
.LC10:
	.long	0
	.long	1071644672
	.align 8
.LC11:
	.long	2147483648
	.long	1094890540
	.align 8
.LC12:
	.long	1219908151
	.long	1084407834
	.align 8
.LC13:
	.long	2388570264
	.long	1075497500
	.align 8
.LC14:
	.long	461049415
	.long	1056644679
	.align 8
.LC15:
	.long	3889812654
	.long	1072696118
	.align 8
.LC16:
	.long	3717316432
	.long	1072687521
	.align 8
.LC17:
	.long	3025947625
	.long	1066050029
	.align 8
.LC18:
	.long	1891984633
	.long	1077375093
	.align 8
.LC19:
	.long	2163380753
	.long	1048993439
	.align 8
.LC20:
	.long	110415401
	.long	1050730196
	.align 8
.LC21:
	.long	0
	.long	1076363264
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC22:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC23:
	.long	3221225472
	.long	1094888713
	.align 8
.LC24:
	.long	3453529090
	.long	1066507675
	.align 8
.LC25:
	.long	870182624
	.long	1075020171
	.align 8
.LC26:
	.long	2364443042
	.long	1075035567
	.align 8
.LC27:
	.long	1907102918
	.long	1066474806
	.align 8
.LC28:
	.long	0
	.long	1072693248
	.align 8
.LC29:
	.long	2296604913
	.long	1055193269
	.align 8
.LC30:
	.long	3332298709
	.long	1072710921
	.align 8
.LC31:
	.long	1413754136
	.long	1073291771
	.align 8
.LC32:
	.long	2134057426
	.long	1074977148
	.align 8
.LC33:
	.long	0
	.long	-1049323152
	.align 8
.LC34:
	.long	0
	.long	1098160496
	.align 8
.LC35:
	.long	0
	.long	1099209072
	.align 8
.LC36:
	.long	0
	.long	1085507584
	.align 8
.LC37:
	.long	2219644924
	.long	1066256298
	.align 8
.LC38:
	.long	0
	.long	1080950784
	.align 8
.LC39:
	.long	442745336
	.long	1078765020
	.align 8
.LC40:
	.long	0
	.long	1083129856
	.align 8
.LC41:
	.long	3436231821
	.long	1070428084
	.align 8
.LC42:
	.long	2694608354
	.long	1075198370
	.align 8
.LC43:
	.long	2635071848
	.long	1063246661
	.align 8
.LC44:
	.long	948003452
	.long	1071926235
	.align 8
.LC45:
	.long	2837881223
	.long	1066845297
	.align 8
.LC46:
	.long	2879716489
	.long	1063948464
	.align 8
.LC47:
	.long	2312531408
	.long	1064989544
	.align 8
.LC48:
	.long	546998494
	.long	1069291781
	.align 8
.LC49:
	.long	1398154162
	.long	1064212703
	.align 8
.LC50:
	.long	3937714634
	.long	1065846734
	.align 8
.LC51:
	.long	588189669
	.long	1062095083
	.align 8
.LC52:
	.long	2667318612
	.long	1075199095
	.align 8
.LC53:
	.long	1767866769
	.long	1063706714
	.align 8
.LC54:
	.long	1069647059
	.long	1072684797
	.align 8
.LC55:
	.long	3119694415
	.long	1068954997
	.align 8
.LC56:
	.long	0
	.long	1089293312
	.align 8
.LC57:
	.long	1413754136
	.long	-1072094725
	.align 8
.LC58:
	.long	0
	.long	1069547520
	.align 8
.LC59:
	.long	3722025369
	.long	1109222337
	.align 8
.LC60:
	.long	3722025369
	.long	1106076609
	.align 8
.LC61:
	.long	61572651
	.long	1081529312
	.align 8
.LC62:
	.long	3080504895
	.long	1105396349
	.align 8
.LC63:
	.long	3080504895
	.long	1102250621
	.align 8
.LC64:
	.long	2882224597
	.long	1077774292
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
