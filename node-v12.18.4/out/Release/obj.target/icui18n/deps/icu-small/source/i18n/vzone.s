	.file	"vzone.cpp"
	.text
	.p2align 4
	.globl	vzone_openID_67
	.type	vzone_openID_67, @function
vzone_openID_67:
.LFB2366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	leaq	-104(%rbp), %rdx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	movq	%rdi, -104(%rbp)
	movq	%r12, %rdi
	sete	%sil
	movzbl	%sil, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rdi
	call	_ZN6icu_679VTimeZone19createVTimeZoneByIDERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5
	addq	$96, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2366:
	.size	vzone_openID_67, .-vzone_openID_67
	.p2align 4
	.globl	vzone_openData_67
	.type	vzone_openData_67, @function
vzone_openData_67:
.LFB2367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	leaq	-104(%rbp), %rdx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	movq	%rdi, -104(%rbp)
	movq	%r12, %rdi
	sete	%sil
	movzbl	%sil, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_679VTimeZone15createVTimeZoneERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	addq	$96, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2367:
	.size	vzone_openData_67, .-vzone_openData_67
	.p2align 4
	.globl	vzone_close_67
	.type	vzone_close_67, @function
vzone_close_67:
.LFB2368:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L10
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L10:
	ret
	.cfi_endproc
.LFE2368:
	.size	vzone_close_67, .-vzone_close_67
	.p2align 4
	.globl	vzone_equals_67
	.type	vzone_equals_67, @function
vzone_equals_67:
.LFB2370:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE2370:
	.size	vzone_equals_67, .-vzone_equals_67
	.p2align 4
	.globl	vzone_getTZURL_67
	.type	vzone_getTZURL_67, @function
vzone_getTZURL_67:
.LFB2371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	%r14, %rsi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	call	_ZNK6icu_679VTimeZone8getTZURLERNS_13UnicodeStringE@PLT
	movzwl	-104(%rbp), %ecx
	movl	%eax, %r13d
	testw	%cx, %cx
	js	.L14
	movswl	%cx, %eax
	sarl	$5, %eax
	movl	%eax, (%r12)
	movslq	%eax, %rdx
	testb	$17, %cl
	jne	.L18
.L22:
	leaq	-102(%rbp), %rsi
	andl	$2, %ecx
	cmove	-88(%rbp), %rsi
.L16:
	movq	(%rbx), %rdi
	call	memcpy@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$80, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	-100(%rbp), %eax
	movl	%eax, (%r12)
	movslq	%eax, %rdx
	testb	$17, %cl
	je	.L22
.L18:
	xorl	%esi, %esi
	jmp	.L16
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2371:
	.size	vzone_getTZURL_67, .-vzone_getTZURL_67
	.p2align 4
	.globl	vzone_setTZURL_67
	.type	vzone_setTZURL_67, @function
vzone_setTZURL_67:
.LFB2372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -104(%rbp)
	xorl	%esi, %esi
	cmpl	$-1, %ecx
	sete	%sil
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_679VTimeZone8setTZURLERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L26:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2372:
	.size	vzone_setTZURL_67, .-vzone_setTZURL_67
	.p2align 4
	.globl	vzone_getLastModified_67
	.type	vzone_getLastModified_67, @function
vzone_getLastModified_67:
.LFB2373:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_679VTimeZone15getLastModifiedERd@PLT
	.cfi_endproc
.LFE2373:
	.size	vzone_getLastModified_67, .-vzone_getLastModified_67
	.p2align 4
	.globl	vzone_setLastModified_67
	.type	vzone_setLastModified_67, @function
vzone_setLastModified_67:
.LFB2374:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_679VTimeZone15setLastModifiedEd@PLT
	.cfi_endproc
.LFE2374:
	.size	vzone_setLastModified_67, .-vzone_setLastModified_67
	.p2align 4
	.globl	vzone_write_67
	.type	vzone_write_67, @function
vzone_write_67:
.LFB2375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r13
	movq	%rsi, %r12
	pushq	%rbx
	movq	%r13, %rsi
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	movq	%rcx, %rdx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	call	_ZNK6icu_679VTimeZone5writeERNS_13UnicodeStringER10UErrorCode@PLT
	movswl	-104(%rbp), %edi
	testw	%di, %di
	js	.L30
	sarl	$5, %edi
.L31:
	movl	%edi, (%rbx)
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movslq	(%rbx), %rdx
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movzwl	-104(%rbp), %eax
	testb	$17, %al
	jne	.L34
	leaq	-102(%rbp), %rsi
	testb	$2, %al
	cmove	-88(%rbp), %rsi
.L32:
	call	memcpy@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	-100(%rbp), %edi
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L34:
	xorl	%esi, %esi
	jmp	.L32
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2375:
	.size	vzone_write_67, .-vzone_write_67
	.p2align 4
	.globl	vzone_writeFromStart_67
	.type	vzone_writeFromStart_67, @function
vzone_writeFromStart_67:
.LFB2376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r13
	movq	%rsi, %r12
	pushq	%rbx
	movq	%r13, %rsi
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	movq	%rcx, %rdx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	call	_ZNK6icu_679VTimeZone5writeEdRNS_13UnicodeStringER10UErrorCode@PLT
	movswl	-104(%rbp), %edi
	testw	%di, %di
	js	.L39
	sarl	$5, %edi
.L40:
	movl	%edi, (%rbx)
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movslq	(%rbx), %rdx
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movzwl	-104(%rbp), %eax
	testb	$17, %al
	jne	.L43
	leaq	-102(%rbp), %rsi
	testb	$2, %al
	cmove	-88(%rbp), %rsi
.L41:
	call	memcpy@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L46
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movl	-100(%rbp), %edi
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%esi, %esi
	jmp	.L41
.L46:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2376:
	.size	vzone_writeFromStart_67, .-vzone_writeFromStart_67
	.p2align 4
	.globl	vzone_writeSimple_67
	.type	vzone_writeSimple_67, @function
vzone_writeSimple_67:
.LFB2377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r13
	movq	%rsi, %r12
	pushq	%rbx
	movq	%r13, %rsi
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	movq	%rcx, %rdx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	call	_ZNK6icu_679VTimeZone11writeSimpleEdRNS_13UnicodeStringER10UErrorCode@PLT
	movswl	-104(%rbp), %edi
	testw	%di, %di
	js	.L48
	sarl	$5, %edi
.L49:
	movl	%edi, (%rbx)
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movslq	(%rbx), %rdx
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movzwl	-104(%rbp), %eax
	testb	$17, %al
	jne	.L52
	leaq	-102(%rbp), %rsi
	testb	$2, %al
	cmove	-88(%rbp), %rsi
.L50:
	call	memcpy@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movl	-100(%rbp), %edi
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%esi, %esi
	jmp	.L50
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2377:
	.size	vzone_writeSimple_67, .-vzone_writeSimple_67
	.p2align 4
	.globl	vzone_getStaticClassID_67
	.type	vzone_getStaticClassID_67, @function
vzone_getStaticClassID_67:
.LFB2389:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_679VTimeZone16getStaticClassIDEv@PLT
	.cfi_endproc
.LFE2389:
	.size	vzone_getStaticClassID_67, .-vzone_getStaticClassID_67
	.p2align 4
	.globl	vzone_clone_67
	.type	vzone_clone_67, @function
vzone_clone_67:
.LFB2369:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_679VTimeZone5cloneEv@PLT
	.cfi_endproc
.LFE2369:
	.size	vzone_clone_67, .-vzone_clone_67
	.p2align 4
	.globl	vzone_getOffset_67
	.type	vzone_getOffset_67, @function
vzone_getOffset_67:
.LFB2378:
	.cfi_startproc
	endbr64
	movzbl	%sil, %esi
	movzbl	%r9b, %r9d
	jmp	_ZNK6icu_679VTimeZone9getOffsetEhiiihiR10UErrorCode@PLT
	.cfi_endproc
.LFE2378:
	.size	vzone_getOffset_67, .-vzone_getOffset_67
	.p2align 4
	.globl	vzone_getOffset2_67
	.type	vzone_getOffset2_67, @function
vzone_getOffset2_67:
.LFB2379:
	.cfi_startproc
	endbr64
	movzbl	%sil, %esi
	movzbl	%r9b, %r9d
	jmp	_ZNK6icu_679VTimeZone9getOffsetEhiiihiiR10UErrorCode@PLT
	.cfi_endproc
.LFE2379:
	.size	vzone_getOffset2_67, .-vzone_getOffset2_67
	.p2align 4
	.globl	vzone_getOffset3_67
	.type	vzone_getOffset3_67, @function
vzone_getOffset3_67:
.LFB2380:
	.cfi_startproc
	endbr64
	movsbl	%sil, %esi
	jmp	_ZNK6icu_679VTimeZone9getOffsetEdaRiS1_R10UErrorCode@PLT
	.cfi_endproc
.LFE2380:
	.size	vzone_getOffset3_67, .-vzone_getOffset3_67
	.p2align 4
	.globl	vzone_setRawOffset_67
	.type	vzone_setRawOffset_67, @function
vzone_setRawOffset_67:
.LFB2381:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_679VTimeZone12setRawOffsetEi@PLT
	.cfi_endproc
.LFE2381:
	.size	vzone_setRawOffset_67, .-vzone_setRawOffset_67
	.p2align 4
	.globl	vzone_getRawOffset_67
	.type	vzone_getRawOffset_67, @function
vzone_getRawOffset_67:
.LFB2382:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_679VTimeZone12getRawOffsetEv@PLT
	.cfi_endproc
.LFE2382:
	.size	vzone_getRawOffset_67, .-vzone_getRawOffset_67
	.p2align 4
	.globl	vzone_useDaylightTime_67
	.type	vzone_useDaylightTime_67, @function
vzone_useDaylightTime_67:
.LFB2383:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_679VTimeZone15useDaylightTimeEv@PLT
	.cfi_endproc
.LFE2383:
	.size	vzone_useDaylightTime_67, .-vzone_useDaylightTime_67
	.p2align 4
	.globl	vzone_inDaylightTime_67
	.type	vzone_inDaylightTime_67, @function
vzone_inDaylightTime_67:
.LFB2384:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_679VTimeZone14inDaylightTimeEdR10UErrorCode@PLT
	.cfi_endproc
.LFE2384:
	.size	vzone_inDaylightTime_67, .-vzone_inDaylightTime_67
	.p2align 4
	.globl	vzone_hasSameRules_67
	.type	vzone_hasSameRules_67, @function
vzone_hasSameRules_67:
.LFB2385:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_679VTimeZone12hasSameRulesERKNS_8TimeZoneE@PLT
	.cfi_endproc
.LFE2385:
	.size	vzone_hasSameRules_67, .-vzone_hasSameRules_67
	.p2align 4
	.globl	vzone_getNextTransition_67
	.type	vzone_getNextTransition_67, @function
vzone_getNextTransition_67:
.LFB2386:
	.cfi_startproc
	endbr64
	movsbl	%sil, %esi
	jmp	_ZNK6icu_679VTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE@PLT
	.cfi_endproc
.LFE2386:
	.size	vzone_getNextTransition_67, .-vzone_getNextTransition_67
	.p2align 4
	.globl	vzone_getPreviousTransition_67
	.type	vzone_getPreviousTransition_67, @function
vzone_getPreviousTransition_67:
.LFB2387:
	.cfi_startproc
	endbr64
	movsbl	%sil, %esi
	jmp	_ZNK6icu_679VTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE@PLT
	.cfi_endproc
.LFE2387:
	.size	vzone_getPreviousTransition_67, .-vzone_getPreviousTransition_67
	.p2align 4
	.globl	vzone_countTransitionRules_67
	.type	vzone_countTransitionRules_67, @function
vzone_countTransitionRules_67:
.LFB2388:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_679VTimeZone20countTransitionRulesER10UErrorCode@PLT
	.cfi_endproc
.LFE2388:
	.size	vzone_countTransitionRules_67, .-vzone_countTransitionRules_67
	.p2align 4
	.globl	vzone_getDynamicClassID_67
	.type	vzone_getDynamicClassID_67, @function
vzone_getDynamicClassID_67:
.LFB2390:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_679VTimeZone17getDynamicClassIDEv@PLT
	.cfi_endproc
.LFE2390:
	.size	vzone_getDynamicClassID_67, .-vzone_getDynamicClassID_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
