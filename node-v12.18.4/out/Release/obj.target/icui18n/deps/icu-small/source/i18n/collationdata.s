	.file	"collationdata.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713CollationData15getIndirectCE32Ej
	.type	_ZNK6icu_6713CollationData15getIndirectCE32Ej, @function
_ZNK6icu_6713CollationData15getIndirectCE32Ej:
.LFB3255:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	andl	$15, %eax
	cmpl	$10, %eax
	je	.L8
	movl	$-1, %r8d
	cmpl	$13, %eax
	je	.L1
	movl	%esi, %r8d
	cmpl	$11, %eax
	je	.L9
.L1:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	8(%rdi), %rax
	shrl	$13, %esi
	movl	(%rax,%rsi,4), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	8(%rdi), %rax
	movl	(%rax), %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3255:
	.size	_ZNK6icu_6713CollationData15getIndirectCE32Ej, .-_ZNK6icu_6713CollationData15getIndirectCE32Ej
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713CollationData12getFinalCE32Ej
	.type	_ZNK6icu_6713CollationData12getFinalCE32Ej, @function
_ZNK6icu_6713CollationData12getFinalCE32Ej:
.LFB3256:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	cmpb	$-65, %sil
	jbe	.L11
	movl	%esi, %edx
	andl	$15, %edx
	cmpl	$10, %edx
	je	.L14
	cmpl	$13, %edx
	je	.L13
	cmpl	$11, %edx
	je	.L15
.L11:
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	8(%rdi), %rdx
	shrl	$13, %eax
	movl	(%rdx,%rax,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	8(%rdi), %rax
	movl	(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3256:
	.size	_ZNK6icu_6713CollationData12getFinalCE32Ej, .-_ZNK6icu_6713CollationData12getFinalCE32Ej
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713CollationData11getSingleCEEiR10UErrorCode
	.type	_ZNK6icu_6713CollationData11getSingleCEEiR10UErrorCode, @function
_ZNK6icu_6713CollationData11getSingleCEEiR10UErrorCode:
.LFB3257:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L52
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movl	%esi, %edi
	movq	(%r8), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rax), %rsi
	cmpl	$55295, %edi
	ja	.L18
	movl	%edi, %ecx
	movq	(%rax), %rax
	movl	%edi, %r9d
	sarl	$5, %ecx
	andl	$31, %r9d
	movslq	%ecx, %rcx
	movzwl	(%rax,%rcx,2), %eax
	leal	(%r9,%rax,4), %eax
	cltq
	movl	(%rsi,%rax,4), %eax
	cmpl	$192, %eax
	je	.L57
.L20:
	cmpb	$-65, %al
	jbe	.L29
	leaq	.L32(%rip), %rsi
.L28:
	movl	%eax, %ecx
	andl	$15, %ecx
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	notrack jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L32:
	.long	.L30-.L32
	.long	.L40-.L32
	.long	.L39-.L32
	.long	.L30-.L32
	.long	.L41-.L32
	.long	.L38-.L32
	.long	.L37-.L32
	.long	.L41-.L32
	.long	.L41-.L32
	.long	.L41-.L32
	.long	.L36-.L32
	.long	.L35-.L32
	.long	.L41-.L32
	.long	.L41-.L32
	.long	.L33-.L32
	.long	.L31-.L32
	.text
	.p2align 4,,10
	.p2align 3
.L18:
	cmpl	$65535, %edi
	jbe	.L58
	cmpl	$1114111, %edi
	ja	.L24
	cmpl	44(%rax), %edi
	jge	.L59
	movq	(%rax), %r9
	movl	%edi, %eax
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%r9,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	movzwl	(%r9,%rax,2), %ecx
	movl	%edi, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
.L26:
	movl	(%rsi,%rax), %eax
	cmpl	$192, %eax
	jne	.L20
	movq	32(%r8), %r8
	movq	(%r8), %rax
	movq	16(%rax), %rsi
	cmpl	44(%rax), %edi
	jl	.L60
	movslq	48(%rax), %rax
	salq	$2, %rax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	%eax, %ecx
	shrl	$8, %ecx
	andl	$31, %ecx
	cmpl	$1, %ecx
	je	.L61
.L41:
	movl	$16, (%rdx)
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	cmpl	$56320, %edi
	movq	(%rax), %r10
	movl	$320, %ecx
	movl	$0, %eax
	cmovl	%ecx, %eax
	movl	%edi, %ecx
	movl	%edi, %r9d
	sarl	$5, %ecx
	andl	$31, %r9d
	addl	%eax, %ecx
	movslq	%ecx, %rcx
	movzwl	(%r10,%rcx,2), %eax
	leal	(%r9,%rax,4), %eax
	cltq
	movl	(%rsi,%rax,4), %eax
	cmpl	$192, %eax
	jne	.L20
.L57:
	movq	32(%r8), %r8
	movq	(%r8), %rax
	movq	16(%rax), %rsi
	movq	(%rax), %rax
	movzwl	(%rax,%rcx,2), %eax
	leal	(%r9,%rax,4), %eax
	cltq
	salq	$2, %rax
.L27:
	movl	(%rsi,%rax), %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L38:
	movl	%eax, %ecx
	shrl	$8, %ecx
	andl	$31, %ecx
	cmpl	$1, %ecx
	jne	.L41
.L36:
	movq	8(%r8), %rcx
	shrl	$13, %eax
	movl	(%rcx,%rax,4), %eax
.L42:
	cmpb	$-65, %al
	ja	.L28
.L29:
	movabsq	$-281474976710656, %rcx
	movq	%rax, %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	salq	$32, %rdx
	andq	%rcx, %rdx
	movl	%eax, %ecx
	sall	$8, %eax
	sall	$16, %ecx
	andl	$65280, %eax
	andl	$-16777216, %ecx
	orq	%rcx, %rdx
	orq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	8(%r8), %rax
	movl	(%rax), %eax
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L40:
	movabsq	$-1099511627776, %rdx
	salq	$32, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	andq	%rdx, %rax
	orq	$83887360, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	andl	$-256, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	call	_ZN6icu_679Collation30unassignedPrimaryFromCodePointEi@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	salq	$32, %rax
	orq	$83887360, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	16(%r8), %rdx
	shrl	$13, %eax
	movq	(%rdx,%rax,8), %rsi
	call	_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	salq	$32, %rax
	orq	$83887360, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	$5, (%rdx)
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movl	512(%rsi), %eax
	cmpl	$192, %eax
	jne	.L20
	movq	32(%r8), %r8
	movq	(%r8), %rax
	movq	16(%rax), %rsi
	movl	$512, %eax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L61:
	movq	16(%r8), %rdx
	shrl	$13, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movslq	48(%rax), %rax
	salq	$2, %rax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L60:
	movq	(%rax), %r9
	movl	%edi, %eax
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%r9,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	movzwl	(%r9,%rax,2), %ecx
	movl	%edi, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L27
	.cfi_endproc
.LFE3257:
	.size	_ZNK6icu_6713CollationData11getSingleCEEiR10UErrorCode, .-_ZNK6icu_6713CollationData11getSingleCEEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713CollationData23getFirstPrimaryForGroupEi
	.type	_ZNK6icu_6713CollationData23getFirstPrimaryForGroupEi, @function
_ZNK6icu_6713CollationData23getFirstPrimaryForGroupEi:
.LFB3258:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L62
	movl	100(%rdi), %edx
	cmpl	%edx, %esi
	jl	.L71
	cmpl	$4095, %esi
	jle	.L62
	subl	$4096, %esi
	cmpl	$7, %esi
	jg	.L62
	addl	%edx, %esi
.L71:
	movq	104(%rdi), %rax
	movslq	%esi, %rsi
	movzwl	(%rax,%rsi,2), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L62
	movq	112(%rdi), %rax
	movzwl	(%rax,%rdx,2), %eax
	sall	$16, %eax
.L62:
	ret
	.cfi_endproc
.LFE3258:
	.size	_ZNK6icu_6713CollationData23getFirstPrimaryForGroupEi, .-_ZNK6icu_6713CollationData23getFirstPrimaryForGroupEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713CollationData22getLastPrimaryForGroupEi
	.type	_ZNK6icu_6713CollationData22getLastPrimaryForGroupEi, @function
_ZNK6icu_6713CollationData22getLastPrimaryForGroupEi:
.LFB3259:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L72
	movl	100(%rdi), %edx
	cmpl	%edx, %esi
	jl	.L81
	cmpl	$4095, %esi
	jle	.L72
	subl	$4096, %esi
	cmpl	$7, %esi
	jg	.L72
	addl	%edx, %esi
.L81:
	movq	104(%rdi), %rax
	movslq	%esi, %rsi
	movzwl	(%rax,%rsi,2), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L72
	movq	112(%rdi), %rax
	movzwl	2(%rax,%rdx,2), %eax
	sall	$16, %eax
	subl	$1, %eax
.L72:
	ret
	.cfi_endproc
.LFE3259:
	.size	_ZNK6icu_6713CollationData22getLastPrimaryForGroupEi, .-_ZNK6icu_6713CollationData22getLastPrimaryForGroupEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713CollationData18getGroupForPrimaryEj
	.type	_ZNK6icu_6713CollationData18getGroupForPrimaryEj, @function
_ZNK6icu_6713CollationData18getGroupForPrimaryEj:
.LFB3260:
	.cfi_startproc
	endbr64
	movq	112(%rdi), %r8
	shrl	$16, %esi
	movzwl	2(%r8), %eax
	cmpl	%esi, %eax
	ja	.L91
	movslq	120(%rdi), %rax
	movzwl	-2(%r8,%rax,2), %eax
	cmpl	%esi, %eax
	jbe	.L91
	movzwl	4(%r8), %edx
	movl	$2, %eax
	cmpl	%edx, %esi
	jb	.L104
	.p2align 4,,10
	.p2align 3
.L84:
	movzwl	2(%r8,%rax,2), %ecx
	movl	%eax, %edx
	addq	$1, %rax
	cmpl	%esi, %ecx
	jbe	.L84
.L87:
	movl	100(%rdi), %r9d
	movq	104(%rdi), %rsi
	xorl	%eax, %eax
	leal	-1(%r9), %edi
	testl	%r9d, %r9d
	jg	.L88
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	1(%rax), %rcx
	cmpq	%rdi, %rax
	je	.L85
	movq	%rcx, %rax
.L88:
	movzwl	(%rsi,%rax,2), %ecx
	movl	%eax, %r8d
	cmpl	%ecx, %edx
	jne	.L105
.L82:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	movslq	%r9d, %rax
	movzwl	(%rsi,%rax,2), %eax
	cmpl	%eax, %edx
	je	.L94
	leal	1(%r9), %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	cmpl	%eax, %edx
	je	.L95
	leal	2(%r9), %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	cmpl	%eax, %edx
	je	.L96
	leal	3(%r9), %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	cmpl	%eax, %edx
	je	.L97
	leal	4(%r9), %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	cmpl	%eax, %edx
	je	.L98
	leal	5(%r9), %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	cmpl	%eax, %edx
	je	.L99
	leal	6(%r9), %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	cmpl	%eax, %edx
	je	.L100
	addl	$7, %r9d
	movl	$-1, %r8d
	movslq	%r9d, %r9
	movzwl	(%rsi,%r9,2), %eax
	cmpl	%eax, %edx
	jne	.L82
	movl	$7, %r8d
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L104:
	movl	$1, %edx
	jmp	.L87
.L94:
	xorl	%r8d, %r8d
.L89:
	addl	$4096, %r8d
	movl	%r8d, %eax
	ret
.L95:
	movl	$1, %r8d
	jmp	.L89
.L96:
	movl	$2, %r8d
	jmp	.L89
.L97:
	movl	$3, %r8d
	jmp	.L89
.L98:
	movl	$4, %r8d
	jmp	.L89
.L91:
	movl	$-1, %r8d
	jmp	.L82
.L99:
	movl	$5, %r8d
	jmp	.L89
.L100:
	movl	$6, %r8d
	jmp	.L89
	.cfi_endproc
.LFE3260:
	.size	_ZNK6icu_6713CollationData18getGroupForPrimaryEj, .-_ZNK6icu_6713CollationData18getGroupForPrimaryEj
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713CollationData14getScriptIndexEi
	.type	_ZNK6icu_6713CollationData14getScriptIndexEi, @function
_ZNK6icu_6713CollationData14getScriptIndexEi:
.LFB3261:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L106
	movl	100(%rdi), %edx
	cmpl	%esi, %edx
	jg	.L112
	cmpl	$4095, %esi
	jle	.L106
	subl	$4096, %esi
	cmpl	$7, %esi
	jg	.L106
	addl	%edx, %esi
.L112:
	movq	104(%rdi), %rax
	movslq	%esi, %rsi
	movzwl	(%rax,%rsi,2), %eax
.L106:
	ret
	.cfi_endproc
.LFE3261:
	.size	_ZNK6icu_6713CollationData14getScriptIndexEi, .-_ZNK6icu_6713CollationData14getScriptIndexEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713CollationData20getEquivalentScriptsEiPiiR10UErrorCode
	.type	_ZNK6icu_6713CollationData20getEquivalentScriptsEiPiiR10UErrorCode, @function
_ZNK6icu_6713CollationData20getEquivalentScriptsEiPiiR10UErrorCode:
.LFB3262:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L122
	testl	%esi, %esi
	js	.L122
	movl	100(%rdi), %r10d
	cmpl	%r10d, %esi
	jl	.L138
	xorl	%r9d, %r9d
	cmpl	$4095, %esi
	jle	.L132
	leal	-4096(%rsi), %eax
	cmpl	$7, %eax
	jg	.L132
	addl	%eax, %r10d
	movq	104(%rdi), %rax
	movslq	%r10d, %r10
	cmpw	$0, (%rax,%r10,2)
	je	.L132
	testl	%ecx, %ecx
	jle	.L135
	movl	%esi, (%rdx)
	movl	$1, %r9d
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L122:
	xorl	%r9d, %r9d
.L132:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	104(%rdi), %r11
	movslq	%esi, %rax
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movzwl	(%r11,%rax,2), %ebx
	testw	%bx, %bx
	je	.L113
	cmpl	$4095, %esi
	jg	.L139
	xorl	%eax, %eax
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L120:
	addq	$1, %rax
	cmpl	%eax, %r10d
	jle	.L140
.L118:
	cmpw	%bx, (%r11,%rax,2)
	jne	.L120
	cmpl	%r9d, %ecx
	jle	.L121
	movslq	%r9d, %rsi
	movl	%eax, (%rdx,%rsi,4)
	movl	100(%rdi), %r10d
.L121:
	addl	$1, %r9d
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L140:
	cmpl	%ecx, %r9d
	jle	.L113
	movl	$15, (%r8)
.L113:
	movl	%r9d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore 3
	.cfi_restore 6
	movl	$15, (%r8)
	movl	$1, %r9d
	jmp	.L132
.L139:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	testl	%ecx, %ecx
	jle	.L119
	movl	%esi, (%rdx)
	movl	$1, %r9d
	jmp	.L113
.L119:
	movl	$15, (%r8)
	movl	$1, %r9d
	jmp	.L113
	.cfi_endproc
.LFE3262:
	.size	_ZNK6icu_6713CollationData20getEquivalentScriptsEiPiiR10UErrorCode, .-_ZNK6icu_6713CollationData20getEquivalentScriptsEiPiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713CollationData17makeReorderRangesEPKiiaRNS_9UVector32ER10UErrorCode
	.type	_ZNK6icu_6713CollationData17makeReorderRangesEPKiiaRNS_9UVector32ER10UErrorCode, @function
_ZNK6icu_6713CollationData17makeReorderRangesEPKiiaRNS_9UVector32ER10UErrorCode:
.LFB3264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -328(%rbp)
	movl	(%r9), %eax
	movl	%ecx, -356(%rbp)
	movq	%r8, -336(%rbp)
	movq	%r9, -344(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.L282
.L141:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L281
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	%r8, %rdi
	movq	%rsi, %r13
	call	_ZN6icu_679UVector3217removeAllElementsEv@PLT
	movl	-328(%rbp), %eax
	testl	%eax, %eax
	je	.L141
	cmpl	$1, %eax
	jne	.L143
	cmpl	$103, 0(%r13)
	je	.L141
.L143:
	movl	100(%r12), %esi
	movl	$32, %ecx
	movq	104(%r12), %r8
	leaq	-320(%rbp), %rax
	movq	%rax, %rbx
	movq	%rax, -352(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	rep stosq
	leal	4110(%rsi), %eax
	cltq
	subq	$4096, %rax
	leaq	(%rax,%rax), %rdx
	movzwl	(%r8,%rax,2), %eax
	testl	%eax, %eax
	je	.L144
	movb	$-1, -320(%rbp,%rax)
.L144:
	movzwl	2(%r8,%rdx), %eax
	testl	%eax, %eax
	jne	.L283
.L145:
	movq	112(%r12), %r14
	movslq	120(%r12), %rdx
	movzwl	-2(%r14,%rdx,2), %ebx
	movzwl	2(%r14), %eax
	movl	%edx, -368(%rbp)
	movl	%ebx, -360(%rbp)
	movl	-328(%rbp), %ebx
	movw	%ax, -364(%rbp)
	testl	%ebx, %ebx
	jle	.L146
	leal	-1(%rbx), %edx
	movq	-352(%rbp), %rbx
	movq	%r13, %rdi
	movl	$1, %r10d
	leaq	4(%r13,%rdx,4), %r11
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L148:
	movl	(%rdi), %ecx
	movl	%r10d, %r15d
	movl	%edx, %r9d
	subl	$4096, %ecx
	sall	%cl, %r15d
	orl	%r15d, %edx
	cmpl	$8, %ecx
	cmovnb	%r9d, %edx
	addq	$4, %rdi
	cmpq	%r11, %rdi
	jne	.L148
	movslq	%esi, %rcx
	movq	%rbx, -352(%rbp)
	movzwl	(%r8,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L149
	testb	$1, %dl
	je	.L205
.L149:
	leal	1(%rsi), %ecx
	movslq	%ecx, %rcx
	movzwl	(%r8,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L152
	testb	$2, %dl
	je	.L206
.L152:
	leal	2(%rsi), %ecx
	movslq	%ecx, %rcx
	movzwl	(%r8,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L155
	testb	$4, %dl
	je	.L284
.L155:
	leal	3(%rsi), %ecx
	movslq	%ecx, %rcx
	movzwl	(%r8,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L158
	testb	$8, %dl
	je	.L285
.L158:
	leal	4(%rsi), %ecx
	movslq	%ecx, %rcx
	movzwl	(%r8,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L161
	testb	$16, %dl
	je	.L286
.L161:
	leal	5(%rsi), %ecx
	movslq	%ecx, %rcx
	movzwl	(%r8,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L164
	testb	$32, %dl
	je	.L287
.L164:
	leal	6(%rsi), %ecx
	movslq	%ecx, %rcx
	movzwl	(%r8,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L167
	testb	$64, %dl
	je	.L288
.L167:
	leal	7(%rsi), %ecx
	movslq	%ecx, %rcx
	movzwl	(%r8,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L170
	testb	$-128, %dl
	je	.L289
.L170:
	movl	$0, -364(%rbp)
	testl	%edx, %edx
	je	.L290
.L172:
	movl	-328(%rbp), %ebx
	testl	%ebx, %ebx
	jle	.L279
	leal	-1(%rbx), %r9d
	movq	-352(%rbp), %r15
	movl	$1, %edx
	movq	%r9, %r11
	addq	$2, %r9
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L175:
	cmpl	$-1, %ecx
	je	.L182
	testl	%ecx, %ecx
	js	.L184
	cmpl	%ecx, %esi
	jg	.L278
	cmpl	$4095, %ecx
	jle	.L184
	subl	$4096, %ecx
	cmpl	$7, %ecx
	jg	.L184
	addl	%esi, %ecx
	movslq	%ecx, %rcx
.L278:
	movzwl	(%r8,%rcx,2), %ecx
	testl	%ecx, %ecx
	je	.L184
	cmpb	$0, -320(%rbp,%rcx)
	jne	.L182
	movzwl	(%r14,%rcx,2), %r10d
	leal	256(%rax), %edi
	cmpb	%r10b, %al
	cmova	%edi, %eax
	movzwl	2(%r14,%rcx,2), %edi
	andl	$65280, %r10d
	movb	%ah, -320(%rbp,%rcx)
	movl	%edi, %ecx
	andl	$65280, %eax
	movzbl	%dil, %edi
	andl	$65280, %ecx
	subl	%r10d, %ecx
	addl	%ecx, %eax
	orl	%edi, %eax
.L184:
	addq	$1, %rdx
	cmpq	%r9, %rdx
	je	.L211
.L174:
	movslq	-4(%r13,%rdx,4), %rcx
	cmpl	$103, %ecx
	jne	.L175
	movl	-328(%rbp), %ebx
	movq	%r15, -352(%rbp)
	cmpl	%edx, %ebx
	jle	.L212
	movslq	%ebx, %rdi
	subl	%edx, %r11d
	movl	%eax, -356(%rbp)
	movl	-360(%rbp), %r9d
	salq	$2, %rdi
	leaq	0(,%r11,4), %rdx
	leaq	0(%r13,%rdi), %rcx
	leaq	-4(%r13,%rdi), %rdi
	subq	%rdx, %rdi
	movq	%rdi, %r15
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L177:
	testl	%eax, %eax
	js	.L179
	cmpl	%eax, %esi
	jg	.L277
	cmpl	$4095, %eax
	jle	.L179
	subl	$4096, %eax
	cmpl	$7, %eax
	jg	.L179
	addl	%esi, %eax
	cltq
.L277:
	movzwl	(%r8,%rax,2), %eax
	testl	%eax, %eax
	je	.L179
	cmpb	$0, -320(%rbp,%rax)
	jne	.L182
	leaq	1(%rax), %r10
	leal	-256(%r9), %r11d
	movzwl	(%r14,%r10,2), %edi
	movzwl	-2(%r14,%r10,2), %r10d
	cmpb	%dil, %r9b
	cmovb	%r11d, %r9d
	movl	%r10d, %r11d
	andl	$65280, %edi
	andl	$65280, %r11d
	subl	%r11d, %edi
	andl	$65280, %r9d
	subl	%edi, %r9d
	movl	%r9d, %edi
	movzbl	%r10b, %r9d
	movl	%edi, %ebx
	orl	%edi, %r9d
	movb	%bh, -320(%rbp,%rax)
.L179:
	subq	$4, %rcx
	cmpq	%r15, %rcx
	je	.L275
.L176:
	movslq	-4(%rcx), %rax
	cmpl	$103, %eax
	sete	%dil
	cmpl	$-1, %eax
	sete	%dl
	orb	%dil, %dl
	je	.L177
.L182:
	movq	-344(%rbp), %rax
	movl	$1, (%rax)
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L290:
	cmpl	$25, 0(%r13)
	jne	.L209
	cmpb	$0, -356(%rbp)
	je	.L291
.L209:
	movl	$0, -364(%rbp)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L283:
	movb	$-1, -320(%rbp,%rax)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L211:
	movq	%r15, -352(%rbp)
.L279:
	movl	$1, %edx
.L173:
	movl	-368(%rbp), %ebx
	cmpl	$2, %ebx
	jle	.L188
	leal	-3(%rbx), %ecx
	leaq	4(%r14), %r8
	movq	-352(%rbp), %rbx
	leaq	-319(%rbp), %rsi
	leaq	-318(%rbp,%rcx), %r10
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L293:
	testb	%dl, %dl
	je	.L218
.L190:
	movzwl	(%r8), %edi
	cmpb	%r9b, %cl
	leal	256(%rcx), %eax
	cmova	%eax, %ecx
	andl	$65280, %r9d
	movl	%edi, %eax
	andl	$65280, %eax
	movb	%ch, (%rsi)
	andl	$65280, %ecx
	subl	%r9d, %eax
	addl	%eax, %ecx
	movzbl	%dil, %eax
	orl	%ecx, %eax
.L189:
	addq	$1, %rsi
	addq	$2, %r8
	cmpq	%rsi, %r10
	je	.L292
.L193:
	cmpb	$0, (%rsi)
	jne	.L189
	movzwl	-2(%r8), %ecx
	movl	%ecx, %r9d
	cmpl	%eax, %ecx
	jg	.L293
.L218:
	movl	%eax, %ecx
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%rbx, -352(%rbp)
.L188:
	movl	-360(%rbp), %ebx
	cmpl	%eax, %ebx
	jge	.L213
	movl	-364(%rbp), %edx
	andl	$65280, %edx
	subl	%edx, %eax
	cmpl	%ebx, %eax
	jle	.L294
	movq	-344(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L141
.L213:
	movl	-368(%rbp), %eax
	movq	-336(%rbp), %r10
	xorl	%r15d, %r15d
	movq	%r12, %r14
	movq	-352(%rbp), %r8
	movl	$1, %r13d
	leal	-1(%rax), %edi
	.p2align 4,,10
	.p2align 3
.L194:
	cmpl	%r13d, %edi
	jle	.L214
	movslq	%r13d, %rax
	movl	%r15d, %r12d
	.p2align 4,,10
	.p2align 3
.L200:
	movzbl	(%r8,%rax), %edx
	movl	%eax, %ecx
	movl	%eax, %r13d
	cmpl	$255, %edx
	je	.L198
	movq	112(%r14), %rsi
	movzwl	(%rsi,%rax,2), %ebx
	movl	%ebx, %esi
	sarl	$8, %esi
	subl	%esi, %edx
	movl	%edx, %r12d
	cmpl	%edx, %r15d
	jne	.L199
.L198:
	addq	$1, %rax
	leal	1(%rcx), %r13d
	cmpl	%eax, %edi
	jg	.L200
.L197:
	testl	%r15d, %r15d
	jne	.L295
	movl	%r12d, %r15d
.L204:
	cmpl	%edi, %r13d
	je	.L141
	addl	$1, %r13d
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L295:
	movq	112(%r14), %rdx
	movslq	%r13d, %rax
	movzwl	(%rdx,%rax,2), %ebx
.L199:
	movslq	8(%r10), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L201
	cmpl	12(%r10), %esi
	jle	.L202
.L201:
	movq	-344(%rbp), %rdx
	movq	%r10, %rdi
	movq	%r8, -336(%rbp)
	movq	%r10, -328(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	-328(%rbp), %r10
	movq	-336(%rbp), %r8
	testb	%al, %al
	je	.L280
	movslq	8(%r10), %rax
.L202:
	movq	24(%r10), %rdx
	sall	$16, %ebx
	movzwl	%r15w, %r15d
	orl	%r15d, %ebx
	movl	%ebx, (%rdx,%rax,4)
	addl	$1, 8(%r10)
.L280:
	movl	120(%r14), %edi
	movl	%r12d, %r15d
	subl	$1, %edi
	jmp	.L204
.L287:
	movzwl	(%r14,%rcx,2), %edi
	leal	256(%rax), %r9d
	cmpb	%dil, %al
	cmova	%r9d, %eax
	andl	$65280, %edi
	movb	%ah, -320(%rbp,%rcx)
	movzwl	2(%r14,%rcx,2), %ecx
	andl	$65280, %eax
	movl	%ecx, %r9d
	movzbl	%cl, %ecx
	andl	$65280, %r9d
	subl	%edi, %r9d
	addl	%r9d, %eax
	orl	%ecx, %eax
	jmp	.L164
.L288:
	movzwl	(%r14,%rcx,2), %edi
	leal	256(%rax), %r9d
	cmpb	%dil, %al
	cmova	%r9d, %eax
	andl	$65280, %edi
	movb	%ah, -320(%rbp,%rcx)
	movzwl	2(%r14,%rcx,2), %ecx
	andl	$65280, %eax
	movl	%ecx, %r9d
	movzbl	%cl, %ecx
	andl	$65280, %r9d
	subl	%edi, %r9d
	addl	%r9d, %eax
	orl	%ecx, %eax
	jmp	.L167
.L284:
	movzwl	(%r14,%rcx,2), %edi
	leal	256(%rax), %r9d
	cmpb	%al, %dil
	cmovb	%r9d, %eax
	andl	$65280, %edi
	movb	%ah, -320(%rbp,%rcx)
	movzwl	2(%r14,%rcx,2), %ecx
	andl	$65280, %eax
	movl	%ecx, %r9d
	movzbl	%cl, %ecx
	andl	$65280, %r9d
	subl	%edi, %r9d
	addl	%r9d, %eax
	orl	%ecx, %eax
	jmp	.L155
.L285:
	movzwl	(%r14,%rcx,2), %edi
	leal	256(%rax), %r9d
	cmpb	%al, %dil
	cmovb	%r9d, %eax
	andl	$65280, %edi
	movb	%ah, -320(%rbp,%rcx)
	movzwl	2(%r14,%rcx,2), %ecx
	andl	$65280, %eax
	movl	%ecx, %r9d
	movzbl	%cl, %ecx
	andl	$65280, %r9d
	subl	%edi, %r9d
	addl	%r9d, %eax
	orl	%ecx, %eax
	jmp	.L158
.L286:
	movzwl	(%r14,%rcx,2), %edi
	leal	256(%rax), %r9d
	cmpb	%al, %dil
	cmovb	%r9d, %eax
	andl	$65280, %edi
	movb	%ah, -320(%rbp,%rcx)
	movzwl	2(%r14,%rcx,2), %ecx
	andl	$65280, %eax
	movl	%ecx, %r9d
	movzbl	%cl, %ecx
	andl	$65280, %r9d
	subl	%edi, %r9d
	addl	%r9d, %eax
	orl	%ecx, %eax
	jmp	.L161
.L289:
	movzwl	(%r14,%rcx,2), %edi
	leal	256(%rax), %r9d
	cmpb	%dil, %al
	cmova	%r9d, %eax
	andl	$65280, %edi
	movb	%ah, -320(%rbp,%rcx)
	movzwl	2(%r14,%rcx,2), %ecx
	andl	$65280, %eax
	movl	%ecx, %r9d
	movzbl	%cl, %ecx
	andl	$65280, %r9d
	subl	%edi, %r9d
	addl	%r9d, %eax
	orl	%ecx, %eax
	jmp	.L170
.L146:
	movslq	%esi, %rdx
	movzwl	(%r8,%rdx,2), %ecx
	xorl	%edx, %edx
	testw	%cx, %cx
	jne	.L205
	leal	1(%rsi), %ecx
	movslq	%ecx, %rcx
	movzwl	(%r8,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L152
.L206:
	movzwl	(%r14,%rcx,2), %edi
	leal	256(%rax), %r9d
	cmpb	%al, %dil
	cmovb	%r9d, %eax
	andl	$65280, %edi
	movb	%ah, -320(%rbp,%rcx)
	movzwl	2(%r14,%rcx,2), %ecx
	andl	$65280, %eax
	movl	%ecx, %r9d
	movzbl	%cl, %ecx
	andl	$65280, %r9d
	subl	%edi, %r9d
	addl	%r9d, %eax
	orl	%ecx, %eax
	jmp	.L152
.L205:
	movzwl	(%r14,%rcx,2), %edi
	leal	256(%rax), %r9d
	cmpb	-364(%rbp), %dil
	cmovb	%r9d, %eax
	andl	$65280, %edi
	movb	%ah, -320(%rbp,%rcx)
	movzwl	2(%r14,%rcx,2), %ecx
	andl	$65280, %eax
	movl	%ecx, %r9d
	movzbl	%cl, %ecx
	andl	$65280, %r9d
	subl	%edi, %r9d
	addl	%r9d, %eax
	orl	%ecx, %eax
	jmp	.L149
.L291:
	movzwl	50(%r8), %edx
	movzwl	(%r14,%rdx,2), %edx
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%edx, %eax
	movl	%ebx, -364(%rbp)
	jmp	.L172
.L294:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L281
	movq	-344(%rbp), %r9
	movq	-336(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	-328(%rbp), %edx
	addq	$328, %rsp
	movl	$1, %ecx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6713CollationData17makeReorderRangesEPKiiaRNS_9UVector32ER10UErrorCode
.L275:
	.cfi_restore_state
	movl	%r9d, -360(%rbp)
	movl	-356(%rbp), %eax
	jmp	.L173
.L214:
	movl	%r15d, %r12d
	jmp	.L197
.L212:
	xorl	%edx, %edx
	jmp	.L173
.L281:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3264:
	.size	_ZNK6icu_6713CollationData17makeReorderRangesEPKiiaRNS_9UVector32ER10UErrorCode, .-_ZNK6icu_6713CollationData17makeReorderRangesEPKiiaRNS_9UVector32ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713CollationData17makeReorderRangesEPKiiRNS_9UVector32ER10UErrorCode
	.type	_ZNK6icu_6713CollationData17makeReorderRangesEPKiiRNS_9UVector32ER10UErrorCode, @function
_ZNK6icu_6713CollationData17makeReorderRangesEPKiiRNS_9UVector32ER10UErrorCode:
.LFB3263:
	.cfi_startproc
	endbr64
	movq	%r8, %r9
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	jmp	_ZNK6icu_6713CollationData17makeReorderRangesEPKiiaRNS_9UVector32ER10UErrorCode
	.cfi_endproc
.LFE3263:
	.size	_ZNK6icu_6713CollationData17makeReorderRangesEPKiiRNS_9UVector32ER10UErrorCode, .-_ZNK6icu_6713CollationData17makeReorderRangesEPKiiRNS_9UVector32ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713CollationData17addLowScriptRangeEPhii
	.type	_ZNK6icu_6713CollationData17addLowScriptRangeEPhii, @function
_ZNK6icu_6713CollationData17addLowScriptRangeEPhii:
.LFB3265:
	.cfi_startproc
	endbr64
	movq	112(%rdi), %rax
	movslq	%edx, %rdx
	movq	%rsi, %r8
	movzwl	(%rax,%rdx,2), %esi
	leal	256(%rcx), %eax
	cmpb	%sil, %cl
	cmova	%eax, %ecx
	andl	$65280, %esi
	movzbl	%ch, %eax
	andl	$65280, %ecx
	movb	%al, (%r8,%rdx)
	movq	112(%rdi), %rax
	movzwl	2(%rax,%rdx,2), %edx
	movl	%edx, %eax
	andl	$65280, %eax
	subl	%esi, %eax
	addl	%eax, %ecx
	movzbl	%dl, %eax
	orl	%ecx, %eax
	ret
	.cfi_endproc
.LFE3265:
	.size	_ZNK6icu_6713CollationData17addLowScriptRangeEPhii, .-_ZNK6icu_6713CollationData17addLowScriptRangeEPhii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713CollationData18addHighScriptRangeEPhii
	.type	_ZNK6icu_6713CollationData18addHighScriptRangeEPhii, @function
_ZNK6icu_6713CollationData18addHighScriptRangeEPhii:
.LFB3266:
	.cfi_startproc
	endbr64
	movq	112(%rdi), %rax
	movslq	%edx, %rdx
	movq	%rsi, %r8
	leal	-256(%rcx), %r9d
	leaq	1(%rdx), %rdi
	movzwl	(%rax,%rdi,2), %esi
	movzwl	-2(%rax,%rdi,2), %eax
	cmpb	%sil, %cl
	movl	%eax, %edi
	movzbl	%al, %eax
	cmovb	%r9d, %ecx
	andl	$65280, %esi
	andl	$65280, %edi
	subl	%edi, %esi
	andl	$65280, %ecx
	subl	%esi, %ecx
	orl	%ecx, %eax
	movzbl	%ch, %ecx
	movb	%cl, (%r8,%rdx)
	ret
	.cfi_endproc
.LFE3266:
	.size	_ZNK6icu_6713CollationData18addHighScriptRangeEPhii, .-_ZNK6icu_6713CollationData18addHighScriptRangeEPhii
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
