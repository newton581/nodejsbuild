	.file	"measfmt.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat17getDynamicClassIDEv
	.type	_ZNK6icu_6713MeasureFormat17getDynamicClassIDEv, @function
_ZNK6icu_6713MeasureFormat17getDynamicClassIDEv:
.LFB4148:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713MeasureFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE4148:
	.size	_ZNK6icu_6713MeasureFormat17getDynamicClassIDEv, .-_ZNK6icu_6713MeasureFormat17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.type	_ZNK6icu_6713MeasureFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, @function
_ZNK6icu_6713MeasureFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE:
.LFB4197:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4197:
	.size	_ZNK6icu_6713MeasureFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, .-_ZNK6icu_6713MeasureFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"durationUnits"
.LC1:
	.string	"/"
	.text
	.p2align 4
	.type	_ZN6icu_67L31loadNumericDateFormatterPatternEPK15UResourceBundlePKcR10UErrorCode, @function
_ZN6icu_67L31loadNumericDateFormatterPatternEPK15UResourceBundlePKcR10UErrorCode:
.LFB4172:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r9w, 8(%rdi)
	movq	%rax, (%rdi)
	testl	%r10d, %r10d
	jle	.L22
.L4:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	xorl	%edi, %edi
	leaq	-115(%rbp), %rax
	movq	%rsi, %r14
	movq	%rcx, %rbx
	movw	%di, -116(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rdx, %r13
	leaq	-128(%rbp), %r15
	leaq	-176(%rbp), %rdi
	movq	%rax, -128(%rbp)
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-168(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	-176(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	leaq	-160(%rbp), %r8
	leaq	.LC1(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	movq	%rax, %r15
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-152(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	-160(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	leaq	-144(%rbp), %r9
	movq	%r13, %rsi
	movq	%r9, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, %r15
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-128(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movq	%r14, %rdi
	call	ures_getByKeyWithFallback_67@PLT
	movl	(%rbx), %r8d
	movq	%rax, %r13
	testl	%r8d, %r8d
	movq	-192(%rbp), %r8
	jle	.L24
.L7:
	testq	%r13, %r13
	je	.L14
	movq	%r13, %rdi
	call	ures_close_67@PLT
.L14:
	cmpb	$0, -116(%rbp)
	je	.L4
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r8, %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movl	$0, -160(%rbp)
	call	ures_getString_67@PLT
	movl	(%rbx), %esi
	movq	-184(%rbp), %r9
	testl	%esi, %esi
	jle	.L25
.L8:
	movswl	8(%r12), %r14d
	testw	%r14w, %r14w
	js	.L9
	sarl	$5, %r14d
.L10:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	testl	%r14d, %r14d
	jle	.L11
	leal	-1(%r14), %edx
	leaq	2(%rax,%rdx,2), %rdx
	.p2align 4,,10
	.p2align 3
.L13:
	cmpw	$104, (%rax)
	jne	.L12
	movl	$72, %ecx
	movw	%cx, (%rax)
.L12:
	addq	$2, %rax
	cmpq	%rdx, %rax
	jne	.L13
.L11:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	movl	12(%r12), %r14d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L25:
	movl	-160(%rbp), %ecx
	movq	%r9, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L8
.L23:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4172:
	.size	_ZN6icu_67L31loadNumericDateFormatterPatternEPK15UResourceBundlePKcR10UErrorCode, .-_ZN6icu_67L31loadNumericDateFormatterPatternEPK15UResourceBundlePKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MeasureFormatD2Ev
	.type	_ZN6icu_6713MeasureFormatD2Ev, @function
_ZN6icu_6713MeasureFormatD2Ev:
.LFB4191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713MeasureFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	328(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L27:
	movq	336(%r12), %rdi
	testq	%rdi, %rdi
	je	.L28
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L28:
	movq	344(%r12), %rdi
	testq	%rdi, %rdi
	je	.L29
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L29:
	movq	360(%r12), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	(%rdi), %rax
	call	*8(%rax)
.L30:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676FormatD2Ev@PLT
	.cfi_endproc
.LFE4191:
	.size	_ZN6icu_6713MeasureFormatD2Ev, .-_ZN6icu_6713MeasureFormatD2Ev
	.globl	_ZN6icu_6713MeasureFormatD1Ev
	.set	_ZN6icu_6713MeasureFormatD1Ev,_ZN6icu_6713MeasureFormatD2Ev
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE5cloneEv,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE5cloneEv
	.type	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE5cloneEv, @function
_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE5cloneEv:
.LFB5957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$240, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L44
	movl	8(%rbx), %eax
	movb	$0, 12(%r12)
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	movl	%eax, 8(%r12)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
.L44:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5957:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE5cloneEv, .-_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE5cloneEv
	.section	.text._ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci,"axG",@progbits,_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci
	.type	_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci, @function
_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci:
.LFB5962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	leaq	_ZTSN6icu_6722MeasureFormatCacheDataE(%rip), %rsi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edx, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	call	strncpy@PLT
	movb	$0, -1(%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5962:
	.size	_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci, .-_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci
	.type	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci, @function
_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci:
.LFB5959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edx, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	movq	56(%rdi), %rsi
	movq	%r8, %rdi
	call	strncpy@PLT
	movb	$0, -1(%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5959:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci, .-_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci
	.section	.text._ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE:
.LFB5961:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L59
	xorl	%eax, %eax
	cmpb	$42, (%rdi)
	je	.L59
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	strcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE5961:
	.size	_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE
	.section	.text._ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv,"axG",@progbits,_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv
	.type	_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv, @function
_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv:
.LFB5960:
	.cfi_startproc
	endbr64
	movl	$33, %esi
	leaq	_ZTSN6icu_6722MeasureFormatCacheDataE(%rip), %rdi
	jmp	ustr_hashCharsN_67@PLT
	.cfi_endproc
.LFE5960:
	.size	_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv, .-_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv
	.type	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv, @function
_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv:
.LFB5956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$33, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	_ZTSN6icu_6722MeasureFormatCacheDataE(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	call	ustr_hashCharsN_67@PLT
	leaq	16(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_676Locale8hashCodeEv@PLT
	movl	%eax, %r8d
	leal	(%rbx,%rbx,8), %eax
	leal	(%rbx,%rax,4), %eax
	popq	%rbx
	popq	%r12
	addl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5956:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv, .-_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722MeasureFormatCacheDataD2Ev
	.type	_ZN6icu_6722MeasureFormatCacheDataD2Ev, @function
_ZN6icu_6722MeasureFormatCacheDataD2Ev:
.LFB4167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722MeasureFormatCacheDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	64(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	40(%rdi), %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
.L67:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L66
	movq	(%rdi), %rax
	call	*8(%rax)
.L66:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L67
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L68
	movq	(%rdi), %rax
	call	*8(%rax)
.L68:
	movq	72(%r12), %r13
	testq	%r13, %r13
	je	.L69
	leaq	136(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L69:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712SharedObjectD2Ev@PLT
	.cfi_endproc
.LFE4167:
	.size	_ZN6icu_6722MeasureFormatCacheDataD2Ev, .-_ZN6icu_6722MeasureFormatCacheDataD2Ev
	.globl	_ZN6icu_6722MeasureFormatCacheDataD1Ev
	.set	_ZN6icu_6722MeasureFormatCacheDataD1Ev,_ZN6icu_6722MeasureFormatCacheDataD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722MeasureFormatCacheDataD0Ev
	.type	_ZN6icu_6722MeasureFormatCacheDataD0Ev, @function
_ZN6icu_6722MeasureFormatCacheDataD0Ev:
.LFB4169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6722MeasureFormatCacheDataD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4169:
	.size	_ZN6icu_6722MeasureFormatCacheDataD0Ev, .-_ZN6icu_6722MeasureFormatCacheDataD0Ev
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE:
.LFB5958:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L86
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L85
	cmpb	$42, (%rdi)
	je	.L88
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L88
.L85:
	leaq	16(%r12), %rsi
	leaq	16(%rbx), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676LocaleeqERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5958:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat5cloneEv
	.type	_ZNK6icu_6713MeasureFormat5cloneEv, @function
_ZNK6icu_6713MeasureFormat5cloneEv:
.LFB4195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$368, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L93
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_676FormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6713MeasureFormatE(%rip), %rax
	movdqu	328(%rbx), %xmm0
	movq	$0, 360(%r12)
	movq	%rax, (%r12)
	movq	344(%rbx), %rax
	movups	%xmm0, 328(%r12)
	movq	328(%rbx), %rdi
	movq	%rax, 344(%r12)
	movl	352(%rbx), %eax
	movl	%eax, 352(%r12)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	336(%r12), %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	344(%r12), %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	cmpq	$0, 360(%rbx)
	je	.L93
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L97
	movq	360(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713ListFormatterC1ERKS0_@PLT
.L97:
	movq	%r13, 360(%r12)
.L93:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4195:
	.size	_ZNK6icu_6713MeasureFormat5cloneEv, .-_ZNK6icu_6713MeasureFormat5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MeasureFormatD0Ev
	.type	_ZN6icu_6713MeasureFormatD0Ev, @function
_ZN6icu_6713MeasureFormatD0Ev:
.LFB4193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713MeasureFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	328(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L103
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L103:
	movq	336(%r12), %rdi
	testq	%rdi, %rdi
	je	.L104
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L104:
	movq	344(%r12), %rdi
	testq	%rdi, %rdi
	je	.L105
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L105:
	movq	360(%r12), %rdi
	testq	%rdi, %rdi
	je	.L106
	movq	(%rdi), %rax
	call	*8(%rax)
.L106:
	movq	%r12, %rdi
	call	_ZN6icu_676FormatD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4193:
	.size	_ZN6icu_6713MeasureFormatD0Ev, .-_ZN6icu_6713MeasureFormatD0Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB4745:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4745:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB4748:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L133
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L121
	cmpb	$0, 12(%rbx)
	jne	.L134
.L125:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L121:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L125
	.cfi_endproc
.LFE4748:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB4751:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L137
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4751:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB4754:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L140
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE4754:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB4756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L146
.L142:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L147
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L147:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4756:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB4757:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4757:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB4758:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4758:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB4759:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4759:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4760:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4760:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4761:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4761:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB4762:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L163
	testl	%edx, %edx
	jle	.L163
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L166
.L155:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L155
	.cfi_endproc
.LFE4762:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB4763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L170
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L170
	testl	%r12d, %r12d
	jg	.L177
	cmpb	$0, 12(%rbx)
	jne	.L178
.L172:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L172
.L178:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L170:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4763:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB4764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L180
	movq	(%rdi), %r8
.L181:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L184
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L184
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L184:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4764:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB4765:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L191
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4765:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB4766:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4766:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB4767:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4767:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB4768:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4768:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB4770:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4770:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB4772:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4772:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MeasureFormat16getStaticClassIDEv
	.type	_ZN6icu_6713MeasureFormat16getStaticClassIDEv, @function
_ZN6icu_6713MeasureFormat16getStaticClassIDEv:
.LFB4147:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713MeasureFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE4147:
	.size	_ZN6icu_6713MeasureFormat16getStaticClassIDEv, .-_ZN6icu_6713MeasureFormat16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722MeasureFormatCacheDataC2Ev
	.type	_ZN6icu_6722MeasureFormatCacheDataC2Ev, @function
_ZN6icu_6722MeasureFormatCacheDataC2Ev:
.LFB4164:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6722MeasureFormatCacheDataE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movabsq	$17179869188, %rax
	movups	%xmm0, 64(%rdi)
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rdi)
	movq	%rax, 24(%rdi)
	movl	$4, 32(%rdi)
	movq	$0, 56(%rdi)
	movups	%xmm0, 40(%rdi)
	ret
	.cfi_endproc
.LFE4164:
	.size	_ZN6icu_6722MeasureFormatCacheDataC2Ev, .-_ZN6icu_6722MeasureFormatCacheDataC2Ev
	.globl	_ZN6icu_6722MeasureFormatCacheDataC1Ev
	.set	_ZN6icu_6722MeasureFormatCacheDataC1Ev,_ZN6icu_6722MeasureFormatCacheDataC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MeasureFormatC2ERKS0_
	.type	_ZN6icu_6713MeasureFormatC2ERKS0_, @function
_ZN6icu_6713MeasureFormatC2ERKS0_:
.LFB4184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_676FormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6713MeasureFormatE(%rip), %rax
	movdqu	328(%r12), %xmm0
	movq	$0, 360(%rbx)
	movq	%rax, (%rbx)
	movq	344(%r12), %rax
	movq	328(%r12), %rdi
	movups	%xmm0, 328(%rbx)
	movq	%rax, 344(%rbx)
	movl	352(%r12), %eax
	movl	%eax, 352(%rbx)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	336(%rbx), %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	344(%rbx), %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	cmpq	$0, 360(%r12)
	je	.L199
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L201
	movq	360(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713ListFormatterC1ERKS0_@PLT
.L201:
	movq	%r13, 360(%rbx)
.L199:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4184:
	.size	_ZN6icu_6713MeasureFormatC2ERKS0_, .-_ZN6icu_6713MeasureFormatC2ERKS0_
	.globl	_ZN6icu_6713MeasureFormatC1ERKS0_
	.set	_ZN6icu_6713MeasureFormatC1ERKS0_,_ZN6icu_6713MeasureFormatC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MeasureFormataSERKS0_
	.type	_ZN6icu_6713MeasureFormataSERKS0_, @function
_ZN6icu_6713MeasureFormataSERKS0_:
.LFB4186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L207
	movq	%rsi, %rbx
	call	_ZN6icu_676FormataSERKS0_@PLT
	movq	328(%rbx), %r13
	movq	328(%r12), %rdi
	cmpq	%rdi, %r13
	je	.L209
	testq	%rdi, %rdi
	je	.L210
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L210:
	movq	%r13, 328(%r12)
	testq	%r13, %r13
	je	.L209
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L209:
	movq	336(%rbx), %r13
	movq	336(%r12), %rdi
	cmpq	%rdi, %r13
	je	.L213
	testq	%rdi, %rdi
	je	.L214
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L214:
	movq	%r13, 336(%r12)
	testq	%r13, %r13
	je	.L213
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L213:
	movq	344(%rbx), %r13
	movq	344(%r12), %rdi
	cmpq	%rdi, %r13
	je	.L217
	testq	%rdi, %rdi
	je	.L218
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L218:
	movq	%r13, 344(%r12)
	testq	%r13, %r13
	je	.L217
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L217:
	movl	352(%rbx), %eax
	movq	360(%r12), %rdi
	movl	%eax, 352(%r12)
	testq	%rdi, %rdi
	je	.L220
	movq	(%rdi), %rax
	call	*8(%rax)
.L220:
	cmpq	$0, 360(%rbx)
	je	.L221
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L222
	movq	360(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713ListFormatterC1ERKS0_@PLT
.L222:
	movq	%r13, 360(%r12)
.L207:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movq	$0, 360(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4186:
	.size	_ZN6icu_6713MeasureFormataSERKS0_, .-_ZN6icu_6713MeasureFormataSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MeasureFormatC2Ev
	.type	_ZN6icu_6713MeasureFormatC2Ev, @function
_ZN6icu_6713MeasureFormatC2Ev:
.LFB4188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6713MeasureFormatE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 344(%rbx)
	movq	%rax, (%rbx)
	movl	$1, 352(%rbx)
	movq	$0, 360(%rbx)
	movups	%xmm0, 328(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4188:
	.size	_ZN6icu_6713MeasureFormatC2Ev, .-_ZN6icu_6713MeasureFormatC2Ev
	.globl	_ZN6icu_6713MeasureFormatC1Ev
	.set	_ZN6icu_6713MeasureFormatC1Ev,_ZN6icu_6713MeasureFormatC2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat20formatMeasurePerUnitERKNS_7MeasureERKNS_11MeasureUnitERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6713MeasureFormat20formatMeasurePerUnitERKNS_7MeasureERKNS_11MeasureUnitERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6713MeasureFormat20formatMeasurePerUnitERKNS_7MeasureERKNS_11MeasureUnitERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB4198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1672, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -1680(%rbp)
	movq	%r8, -1672(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L251
	movq	336(%rdi), %rax
	movq	%rdi, -1688(%rbp)
	movq	%r9, %r12
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L252
	xorl	%ecx, %ecx
	movq	%rsi, %rbx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	-1688(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L252
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	leaq	-1664(%rbp), %r15
	movq	%r11, -1704(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	-1512(%rbp), %r9
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	movq	%r9, %rdi
	movq	%rax, -1664(%rbp)
	movq	%r9, -1696(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	leaq	-512(%rbp), %r13
	call	_ZNK6icu_6713DecimalFormat17toNumberFormatterER10UErrorCode@PLT
	testq	%rax, %rax
	movq	%rax, -1688(%rbp)
	je	.L254
	leaq	8(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	movq	-1696(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	movq	-1688(%rbp), %r8
	leaq	-1440(%rbp), %r9
	movq	120(%rbx), %rdx
	movq	%r9, %rdi
	movq	%r9, -1688(%rbp)
	leaq	-976(%rbp), %rbx
	movq	%r8, %rsi
	call	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE@PLT
	movq	-1688(%rbp), %r9
	movq	-1680(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r9, %rsi
	movq	%r9, -1680(%rbp)
	call	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE@PLT
	movq	-1704(%rbp), %r11
	movl	$2, %edx
	movq	-1680(%rbp), %r9
	movl	352(%r11), %eax
	testl	%eax, %eax
	je	.L255
	subl	$2, %eax
	xorl	%edx, %edx
	cmpl	$1, %eax
	seta	%dl
.L255:
	leaq	-512(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r9, -1680(%rbp)
	movq	%r13, %rdi
	call	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	-1680(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
.L254:
	movswl	8(%r14), %edx
	testw	%dx, %dx
	js	.L256
	sarl	$5, %edx
.L257:
	movq	-1672(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r15, %rdi
	call	_ZN6icu_6713DecimalFormat19fieldPositionHelperERKNS_6number4impl20UFormattedNumberDataERNS_13FieldPositionEiR10UErrorCode@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl12toTempStringER10UErrorCode@PLT
	movswl	-504(%rbp), %ecx
	testw	%cx, %cx
	js	.L258
	sarl	$5, %ecx
.L259:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
.L251:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L270
	addq	$1672, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movl	$16, (%r12)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L256:
	movl	12(%r14), %edx
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L258:
	movl	-500(%rbp), %ecx
	jmp	.L259
.L270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4198:
	.size	_ZNK6icu_6713MeasureFormat20formatMeasurePerUnitERKNS_7MeasureERKNS_11MeasureUnitERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6713MeasureFormat20formatMeasurePerUnitERKNS_7MeasureERKNS_11MeasureUnitERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MeasureFormat17adoptNumberFormatEPNS_12NumberFormatER10UErrorCode
	.type	_ZN6icu_6713MeasureFormat17adoptNumberFormatEPNS_12NumberFormatER10UErrorCode, @function
_ZN6icu_6713MeasureFormat17adoptNumberFormatEPNS_12NumberFormatER10UErrorCode:
.LFB4204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdx), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	testl	%eax, %eax
	jle	.L285
.L272:
	testq	%r12, %r12
	je	.L271
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	8(%rax), %rax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	movq	%rdi, %r14
	movl	$32, %edi
	movq	%rdx, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L273
	movq	336(%r14), %rdi
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6718SharedNumberFormatE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	%r12, 24(%r13)
	cmpq	%rdi, %r13
	je	.L271
	testq	%rdi, %rdi
	je	.L276
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L276:
	movq	%r13, 336(%r14)
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L273:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L272
	.cfi_endproc
.LFE4204:
	.size	_ZN6icu_6713MeasureFormat17adoptNumberFormatEPNS_12NumberFormatER10UErrorCode, .-_ZN6icu_6713MeasureFormat17adoptNumberFormatEPNS_12NumberFormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat23getNumberFormatInternalEv
	.type	_ZNK6icu_6713MeasureFormat23getNumberFormatInternalEv, @function
_ZNK6icu_6713MeasureFormat23getNumberFormatInternalEv:
.LFB4206:
	.cfi_startproc
	endbr64
	movq	336(%rdi), %rax
	movq	24(%rax), %rax
	ret
	.cfi_endproc
.LFE4206:
	.size	_ZNK6icu_6713MeasureFormat23getNumberFormatInternalEv, .-_ZNK6icu_6713MeasureFormat23getNumberFormatInternalEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat25getCurrencyFormatInternalEv
	.type	_ZNK6icu_6713MeasureFormat25getCurrencyFormatInternalEv, @function
_ZNK6icu_6713MeasureFormat25getCurrencyFormatInternalEv:
.LFB4207:
	.cfi_startproc
	endbr64
	movq	328(%rdi), %rax
	movq	56(%rax), %rax
	ret
	.cfi_endproc
.LFE4207:
	.size	_ZNK6icu_6713MeasureFormat25getCurrencyFormatInternalEv, .-_ZNK6icu_6713MeasureFormat25getCurrencyFormatInternalEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat14getPluralRulesEv
	.type	_ZNK6icu_6713MeasureFormat14getPluralRulesEv, @function
_ZNK6icu_6713MeasureFormat14getPluralRulesEv:
.LFB4208:
	.cfi_startproc
	endbr64
	movq	344(%rdi), %rax
	movq	24(%rax), %rax
	ret
	.cfi_endproc
.LFE4208:
	.size	_ZNK6icu_6713MeasureFormat14getPluralRulesEv, .-_ZNK6icu_6713MeasureFormat14getPluralRulesEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat9getLocaleER10UErrorCode
	.type	_ZNK6icu_6713MeasureFormat9getLocaleER10UErrorCode, @function
_ZNK6icu_6713MeasureFormat9getLocaleER10UErrorCode:
.LFB4209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_676Format9getLocaleE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L292
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L292:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4209:
	.size	_ZNK6icu_6713MeasureFormat9getLocaleER10UErrorCode, .-_ZNK6icu_6713MeasureFormat9getLocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat11getLocaleIDER10UErrorCode
	.type	_ZNK6icu_6713MeasureFormat11getLocaleIDER10UErrorCode, @function
_ZNK6icu_6713MeasureFormat11getLocaleIDER10UErrorCode:
.LFB4210:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movl	$1, %esi
	jmp	_ZNK6icu_676Format11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode@PLT
	.cfi_endproc
.LFE4210:
	.size	_ZNK6icu_6713MeasureFormat11getLocaleIDER10UErrorCode, .-_ZNK6icu_6713MeasureFormat11getLocaleIDER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MeasureFormat20createCurrencyFormatERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6713MeasureFormat20createCurrencyFormatERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6713MeasureFormat20createCurrencyFormatERKNS_6LocaleER10UErrorCode:
.LFB4229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L299
	movq	%rdi, %r14
	movl	$368, %edi
	movq	%rsi, %r12
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L297
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6714CurrencyFormatC1ERKNS_6LocaleER10UErrorCode@PLT
.L294:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L297:
	.cfi_restore_state
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L299
	movl	$7, (%r12)
	jmp	.L294
	.cfi_endproc
.LFE4229:
	.size	_ZN6icu_6713MeasureFormat20createCurrencyFormatERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6713MeasureFormat20createCurrencyFormatERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MeasureFormat20createCurrencyFormatER10UErrorCode
	.type	_ZN6icu_6713MeasureFormat20createCurrencyFormatER10UErrorCode, @function
_ZN6icu_6713MeasureFormat20createCurrencyFormatER10UErrorCode:
.LFB4230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L304
	movq	%rdi, %r12
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	(%r12), %edx
	movq	%rax, %r14
	testl	%edx, %edx
	jg	.L304
	movl	$368, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L305
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6714CurrencyFormatC1ERKNS_6LocaleER10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L305:
	.cfi_restore_state
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L308
	.p2align 4,,10
	.p2align 3
.L304:
	xorl	%r13d, %r13d
.L301:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L301
	.cfi_endproc
.LFE4230:
	.size	_ZN6icu_6713MeasureFormat20createCurrencyFormatER10UErrorCode, .-_ZN6icu_6713MeasureFormat20createCurrencyFormatER10UErrorCode
	.section	.rodata.str1.1
.LC2:
	.string	"icudt67l-unit"
.LC3:
	.string	"hm"
.LC4:
	.string	"ms"
.LC5:
	.string	"hms"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE12createObjectEPKvR10UErrorCode
	.type	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE12createObjectEPKvR10UErrorCode, @function
_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE12createObjectEPKvR10UErrorCode:
.LFB4174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$456, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	56(%rdi), %rax
	leaq	.LC2(%rip), %rdi
	movq	%rax, %rsi
	movq	%rax, -440(%rbp)
	call	ures_open_67@PLT
	movl	$80, %edi
	movq	%rax, %r14
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L310
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6722MeasureFormatCacheDataE(%rip), %rax
	movq	$0, 56(%r15)
	movups	%xmm0, 64(%r15)
	pxor	%xmm0, %xmm0
	movq	%rax, (%r15)
	movabsq	$17179869188, %rax
	movups	%xmm0, 40(%r15)
	movl	(%rbx), %r8d
	movq	%rax, 24(%r15)
	leaq	40(%r15), %rax
	movq	$0, 8(%r15)
	movq	$0, 16(%r15)
	movl	$4, 32(%r15)
	movq	%rax, -448(%rbp)
	testl	%r8d, %r8d
	jg	.L311
	leaq	-288(%rbp), %r13
	movq	%rbx, %rcx
	leaq	.LC3(%rip), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-352(%rbp), %r12
	call	_ZN6icu_67L31loadNumericDateFormatterPatternEPK15UResourceBundlePKcR10UErrorCode
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	.LC4(%rip), %rdx
	call	_ZN6icu_67L31loadNumericDateFormatterPatternEPK15UResourceBundlePKcR10UErrorCode
	leaq	-416(%rbp), %r10
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r10, %rdi
	leaq	.LC5(%rip), %rdx
	movq	%r10, -456(%rbp)
	call	_ZN6icu_67L31loadNumericDateFormatterPatternEPK15UResourceBundlePKcR10UErrorCode
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-456(%rbp), %r10
	testq	%rax, %rax
	je	.L314
	leaq	8(%rax), %r11
	movq	%r13, %rsi
	movq	%r10, -464(%rbp)
	movq	%r11, %rdi
	movq	%r11, -488(%rbp)
	movq	%rax, -456(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-456(%rbp), %rax
	movq	%r12, %rsi
	leaq	72(%rax), %r8
	movq	%r8, %rdi
	movq	%r8, -480(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-456(%rbp), %rax
	movq	-464(%rbp), %r10
	leaq	136(%rax), %r9
	movq	%r10, %rsi
	movq	%rax, -472(%rbp)
	movq	%r9, %rdi
	movq	%r9, -456(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-464(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %edi
	movq	-456(%rbp), %r9
	movq	-472(%rbp), %rax
	movq	-480(%rbp), %r8
	testl	%edi, %edi
	movq	-488(%rbp), %r11
	jg	.L352
.L315:
	movq	72(%r15), %r12
	testq	%r12, %r12
	je	.L316
	leaq	136(%r12), %rdi
	movq	%rax, -456(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-456(%rbp), %rax
.L316:
	movl	(%rbx), %esi
	movq	-448(%rbp), %r12
	movq	%rax, 72(%r15)
	leaq	_ZZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE12createObjectEPKvR10UErrorCodeE14currencyStyles(%rip), %rax
	movq	%rax, %rcx
	leaq	-420(%rbp), %rax
	movq	%rax, -448(%rbp)
	testl	%esi, %esi
	jg	.L351
	movq	%r15, -472(%rbp)
	movq	%r14, -464(%rbp)
	movq	%rcx, %r14
.L323:
	movq	-440(%rbp), %rsi
	movl	(%r14), %r15d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$0, -420(%rbp)
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-448(%rbp), %rdx
	movq	%r13, %rdi
	movl	%r15d, %esi
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L319
	movq	(%rdi), %rdx
	movq	%rax, -456(%rbp)
	call	*8(%rdx)
	movq	-456(%rbp), %rax
.L319:
	movq	%rax, (%r12)
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	-420(%rbp), %eax
	testl	%eax, %eax
	jne	.L320
	movl	(%rbx), %eax
.L321:
	testl	%eax, %eax
	jg	.L353
	addq	$4, %r14
	leaq	12+_ZZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE12createObjectEPKvR10UErrorCodeE14currencyStyles(%rip), %rax
	addq	$8, %r12
	cmpq	%r14, %rax
	jne	.L323
	movq	-440(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	-464(%rbp), %r14
	movq	-472(%rbp), %r15
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L351
	movq	(%r12), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*232(%rax)
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L325
	movq	(%rax), %rax
	movl	$2, %esi
	call	*280(%rax)
.L325:
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.L326
	movq	(%rdi), %rax
	call	*8(%rax)
.L326:
	movq	%r12, 64(%r15)
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L353:
	movq	-464(%rbp), %r14
	movq	-472(%rbp), %r15
.L351:
	movq	(%r15), %rax
	leaq	_ZN6icu_6722MeasureFormatCacheDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L327
.L311:
	movq	%r15, %rdi
	call	_ZN6icu_6722MeasureFormatCacheDataD1Ev
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L313:
	testq	%r14, %r14
	je	.L309
	movq	%r14, %rdi
	call	ures_close_67@PLT
.L309:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L354
	addq	$456, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	movl	%eax, (%rbx)
	jmp	.L321
.L310:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L313
	movl	$7, (%rbx)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	*%rax
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%r9, %rdi
	movq	%rax, -456(%rbp)
	movq	%r11, -464(%rbp)
	movq	%r8, -472(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-472(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-464(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-456(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	xorl	%eax, %eax
	jmp	.L315
.L354:
	call	__stack_chk_fail@PLT
.L314:
	movq	%r10, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-456(%rbp), %rax
	jmp	.L315
	.cfi_endproc
.LFE4174:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE12createObjectEPKvR10UErrorCode, .-_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE12createObjectEPKvR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat18getUnitDisplayNameERKNS_11MeasureUnitER10UErrorCode
	.type	_ZNK6icu_6713MeasureFormat18getUnitDisplayNameERKNS_11MeasureUnitER10UErrorCode, @function
_ZNK6icu_6713MeasureFormat18getUnitDisplayNameERKNS_11MeasureUnitER10UErrorCode:
.LFB4200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$2, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$248, %rsp
	movl	352(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L356
	subl	$2, %edx
	xorl	%r15d, %r15d
	cmpl	$1, %edx
	seta	%r15b
.L356:
	leaq	-288(%rbp), %r14
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZNK6icu_676Format9getLocaleE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	%rbx, %r8
	movl	%r15d, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15LongNameHandler18getUnitDisplayNameERKNS_6LocaleERKNS_11MeasureUnitE16UNumberUnitWidthR10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L362
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L362:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4200:
	.size	_ZNK6icu_6713MeasureFormat18getUnitDisplayNameERKNS_11MeasureUnitER10UErrorCode, .-_ZNK6icu_6713MeasureFormat18getUnitDisplayNameERKNS_11MeasureUnitER10UErrorCode
	.section	.text._ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED2Ev,"axG",@progbits,_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED2Ev
	.type	_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED2Ev, @function
_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED2Ev:
.LFB5241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	.cfi_endproc
.LFE5241:
	.size	_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED2Ev, .-_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED2Ev
	.weak	_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED1Ev
	.set	_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED1Ev,_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED2Ev
	.section	.rodata.str1.1
.LC6:
	.string	"unit-narrow"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode
	.type	_ZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode, @function
_ZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode:
.LFB4201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L417
.L366:
	testq	%r15, %r15
	je	.L365
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
.L365:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L418
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_restore_state
	movq	%rsi, %r14
	movq	40(%rsi), %rsi
	movq	%r8, %r12
	movslq	%edx, %r13
	movq	%rdi, %rbx
	movq	%rsi, %rdx
	call	_ZN6icu_676Format12setLocaleIDsEPKcS2_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%r12), %r9d
	testl	%r9d, %r9d
	jg	.L366
	leaq	-288(%rbp), %r11
	movq	%r14, %rsi
	movq	%rax, -344(%rbp)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%rax, -304(%rbp)
	movq	%r11, %rdi
	leaq	-304(%rbp), %rax
	movq	%rax, -328(%rbp)
	movq	%r11, -336(%rbp)
	movl	$0, -296(%rbp)
	movb	$0, -292(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%r12), %r8d
	movq	-336(%rbp), %r11
	movq	-344(%rbp), %rax
	testl	%r8d, %r8d
	jle	.L419
.L367:
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%r11, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-328(%rbp), %rdi
	leaq	16+_ZTVN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L366
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movl	(%r12), %ecx
	movq	%rax, %r8
	testl	%ecx, %ecx
	jg	.L366
	movq	344(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L376
	testq	%rdi, %rdi
	je	.L377
	movq	%rax, -328(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-328(%rbp), %r8
.L377:
	movq	%r8, 344(%rbx)
	testq	%r8, %r8
	je	.L376
	movq	%r8, %rdi
	movq	%r8, -328(%rbp)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-328(%rbp), %r8
.L376:
	movq	%r8, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	%r12, %rdx
	testq	%r15, %r15
	je	.L420
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713MeasureFormat17adoptNumberFormatEPNS_12NumberFormatER10UErrorCode
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L365
.L386:
	movq	360(%rbx), %rdi
	movl	%r13d, 352(%rbx)
	testq	%rdi, %rdi
	je	.L388
	movq	(%rdi), %rax
	call	*8(%rax)
	movslq	352(%rbx), %r13
.L388:
	leaq	.LC6(%rip), %rsi
	cmpl	$2, %r13d
	jg	.L389
	leaq	_ZZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCodeE10listStyles(%rip), %rax
	movq	(%rax,%r13,8), %rsi
.L389:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleEPKcR10UErrorCode@PLT
	movq	%rax, 360(%rbx)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L419:
	movq	-328(%rbp), %rsi
	leaq	-316(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	leaq	-312(%rbp), %rdx
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %eax
	movq	-312(%rbp), %r8
	movq	-336(%rbp), %r11
	testl	%eax, %eax
	jle	.L421
.L368:
	testq	%r8, %r8
	je	.L371
.L372:
	movq	%r8, %rdi
	movq	%r11, -336(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movl	-316(%rbp), %eax
	movq	-336(%rbp), %r11
.L371:
	movl	(%r12), %edi
	testl	%edi, %edi
	jne	.L422
.L373:
	movl	%eax, (%r12)
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L422:
	testl	%eax, %eax
	jg	.L373
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L420:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6712NumberFormat20createSharedInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode@PLT
	movl	(%r12), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	jg	.L365
	movq	336(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L383
	testq	%rdi, %rdi
	je	.L384
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L384:
	movq	%r15, 336(%rbx)
	testq	%r15, %r15
	je	.L383
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L383:
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L421:
	movq	328(%rbx), %rdi
	cmpq	%rdi, %r8
	je	.L368
	testq	%rdi, %rdi
	je	.L369
	movq	%r11, -344(%rbp)
	movq	%r8, -336(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-344(%rbp), %r11
	movq	-336(%rbp), %r8
.L369:
	movq	%r8, 328(%rbx)
	testq	%r8, %r8
	je	.L423
	movq	%r8, %rdi
	movq	%r11, -344(%rbp)
	movq	%r8, -336(%rbp)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-336(%rbp), %r8
	movq	-344(%rbp), %r11
	jmp	.L372
.L423:
	movl	-316(%rbp), %eax
	jmp	.L371
.L418:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4201:
	.size	_ZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode, .-_ZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode
	.section	.text._ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED0Ev,"axG",@progbits,_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED0Ev
	.type	_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED0Ev, @function
_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED0Ev:
.LFB5243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE5243:
	.size	_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED0Ev, .-_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MeasureFormatC2ERKNS_6LocaleE19UMeasureFormatWidthR10UErrorCode
	.type	_ZN6icu_6713MeasureFormatC2ERKNS_6LocaleE19UMeasureFormatWidthR10UErrorCode, @function
_ZN6icu_6713MeasureFormatC2ERKNS_6LocaleE19UMeasureFormatWidthR10UErrorCode:
.LFB4178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%edx, %rbx
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676FormatC2Ev@PLT
	movl	(%r12), %r9d
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6713MeasureFormatE(%rip), %rax
	movq	%rax, (%r15)
	movq	$0, 344(%r15)
	movl	%ebx, 352(%r15)
	movq	$0, 360(%r15)
	movups	%xmm0, 328(%r15)
	testl	%r9d, %r9d
	jle	.L473
.L426:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L474
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	movq	40(%r13), %rsi
	movq	%r15, %rdi
	movq	%rsi, %rdx
	call	_ZN6icu_676Format12setLocaleIDsEPKcS2_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%r12), %r8d
	testl	%r8d, %r8d
	jg	.L426
	movq	%rax, -336(%rbp)
	movq	%r13, %rsi
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	leaq	-288(%rbp), %r14
	movq	%rax, -304(%rbp)
	movq	%r14, %rdi
	leaq	-304(%rbp), %rax
	movq	%rax, -328(%rbp)
	movl	$0, -296(%rbp)
	movb	$0, -292(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%r12), %edi
	movq	-336(%rbp), %rax
	testl	%edi, %edi
	jle	.L475
.L430:
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-328(%rbp), %rdi
	leaq	16+_ZTVN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L426
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movl	(%r12), %edx
	movq	%rax, %r14
	testl	%edx, %edx
	jg	.L426
	movq	344(%r15), %rdi
	cmpq	%rdi, %rax
	je	.L439
	testq	%rdi, %rdi
	je	.L440
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L440:
	movq	%r14, 344(%r15)
	testq	%r14, %r14
	je	.L439
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L439:
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6712NumberFormat20createSharedInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode@PLT
	movq	%rax, %r14
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L426
	movq	336(%r15), %rdi
	cmpq	%rdi, %r14
	je	.L443
	testq	%rdi, %rdi
	je	.L444
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L444:
	movq	%r14, 336(%r15)
	testq	%r14, %r14
	je	.L443
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L443:
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	360(%r15), %rdi
	movl	%ebx, 352(%r15)
	testq	%rdi, %rdi
	je	.L446
	movq	(%rdi), %rax
	call	*8(%rax)
	movslq	352(%r15), %rbx
.L446:
	leaq	.LC6(%rip), %rsi
	cmpl	$2, %ebx
	jg	.L447
	leaq	_ZZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCodeE10listStyles(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
.L447:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleEPKcR10UErrorCode@PLT
	movq	%rax, 360(%r15)
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L475:
	movq	-328(%rbp), %rsi
	leaq	-316(%rbp), %r8
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	leaq	-312(%rbp), %rdx
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %eax
	movq	-312(%rbp), %r8
	testl	%eax, %eax
	jle	.L476
.L431:
	testq	%r8, %r8
	je	.L434
.L435:
	movq	%r8, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L472:
	movl	-316(%rbp), %eax
.L434:
	movl	(%r12), %esi
	testl	%esi, %esi
	jne	.L477
.L436:
	movl	%eax, (%r12)
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L477:
	testl	%eax, %eax
	jg	.L436
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L476:
	movq	328(%r15), %rdi
	cmpq	%rdi, %r8
	je	.L431
	testq	%rdi, %rdi
	je	.L432
	movq	%r8, -336(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-336(%rbp), %r8
.L432:
	movq	%r8, 328(%r15)
	testq	%r8, %r8
	je	.L472
	movq	%r8, %rdi
	movq	%r8, -336(%rbp)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-336(%rbp), %r8
	jmp	.L435
.L474:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4178:
	.size	_ZN6icu_6713MeasureFormatC2ERKNS_6LocaleE19UMeasureFormatWidthR10UErrorCode, .-_ZN6icu_6713MeasureFormatC2ERKNS_6LocaleE19UMeasureFormatWidthR10UErrorCode
	.globl	_ZN6icu_6713MeasureFormatC1ERKNS_6LocaleE19UMeasureFormatWidthR10UErrorCode
	.set	_ZN6icu_6713MeasureFormatC1ERKNS_6LocaleE19UMeasureFormatWidthR10UErrorCode,_ZN6icu_6713MeasureFormatC2ERKNS_6LocaleE19UMeasureFormatWidthR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MeasureFormatC2ERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode
	.type	_ZN6icu_6713MeasureFormatC2ERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode, @function
_ZN6icu_6713MeasureFormatC2ERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode:
.LFB4181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%edx, %rbx
	subq	$312, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676FormatC2Ev@PLT
	movl	(%r12), %r10d
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6713MeasureFormatE(%rip), %rax
	movq	%rax, (%r15)
	movq	$0, 344(%r15)
	movl	%ebx, 352(%r15)
	movq	$0, 360(%r15)
	movups	%xmm0, 328(%r15)
	testl	%r10d, %r10d
	jle	.L530
.L479:
	testq	%r13, %r13
	je	.L478
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L478:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L531
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L530:
	.cfi_restore_state
	movq	40(%r14), %rsi
	movq	%r15, %rdi
	movq	%rsi, %rdx
	call	_ZN6icu_676Format12setLocaleIDsEPKcS2_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%r12), %r9d
	testl	%r9d, %r9d
	jg	.L479
	leaq	-288(%rbp), %r10
	movq	%r14, %rsi
	movq	%rax, -344(%rbp)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%rax, -304(%rbp)
	movq	%r10, %rdi
	leaq	-304(%rbp), %rax
	movq	%rax, -328(%rbp)
	movq	%r10, -336(%rbp)
	movl	$0, -296(%rbp)
	movb	$0, -292(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%r12), %r8d
	movq	-336(%rbp), %r10
	movq	-344(%rbp), %rax
	testl	%r8d, %r8d
	jle	.L532
.L480:
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%r10, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-328(%rbp), %rdi
	leaq	16+_ZTVN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L479
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movl	(%r12), %ecx
	movq	%rax, %r8
	testl	%ecx, %ecx
	jg	.L479
	movq	344(%r15), %rdi
	cmpq	%rdi, %rax
	je	.L489
	testq	%rdi, %rdi
	je	.L490
	movq	%rax, -328(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-328(%rbp), %r8
.L490:
	movq	%r8, 344(%r15)
	testq	%r8, %r8
	je	.L489
	movq	%r8, %rdi
	movq	%r8, -328(%rbp)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-328(%rbp), %r8
.L489:
	movq	%r8, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	%r12, %rdx
	testq	%r13, %r13
	je	.L533
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713MeasureFormat17adoptNumberFormatEPNS_12NumberFormatER10UErrorCode
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L478
.L499:
	movq	360(%r15), %rdi
	movl	%ebx, 352(%r15)
	testq	%rdi, %rdi
	je	.L501
	movq	(%rdi), %rax
	call	*8(%rax)
	movslq	352(%r15), %rbx
.L501:
	leaq	.LC6(%rip), %rsi
	cmpl	$2, %ebx
	jg	.L502
	leaq	_ZZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCodeE10listStyles(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
.L502:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleEPKcR10UErrorCode@PLT
	movq	%rax, 360(%r15)
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L532:
	movq	-328(%rbp), %rsi
	leaq	-316(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	leaq	-312(%rbp), %rdx
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %eax
	movq	-312(%rbp), %r8
	movq	-336(%rbp), %r10
	testl	%eax, %eax
	jle	.L534
.L481:
	testq	%r8, %r8
	je	.L484
.L485:
	movq	%r8, %rdi
	movq	%r10, -336(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movl	-316(%rbp), %eax
	movq	-336(%rbp), %r10
.L484:
	movl	(%r12), %edi
	testl	%edi, %edi
	jne	.L535
.L486:
	movl	%eax, (%r12)
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L535:
	testl	%eax, %eax
	jg	.L486
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L533:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6712NumberFormat20createSharedInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode@PLT
	movl	(%r12), %edx
	movq	%rax, %r13
	testl	%edx, %edx
	jg	.L478
	movq	336(%r15), %rdi
	cmpq	%rdi, %rax
	je	.L496
	testq	%rdi, %rdi
	je	.L497
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L497:
	movq	%r13, 336(%r15)
	testq	%r13, %r13
	je	.L496
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L496:
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L534:
	movq	328(%r15), %rdi
	cmpq	%rdi, %r8
	je	.L481
	testq	%rdi, %rdi
	je	.L482
	movq	%r10, -344(%rbp)
	movq	%r8, -336(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-344(%rbp), %r10
	movq	-336(%rbp), %r8
.L482:
	movq	%r8, 328(%r15)
	testq	%r8, %r8
	je	.L536
	movq	%r8, %rdi
	movq	%r10, -344(%rbp)
	movq	%r8, -336(%rbp)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-336(%rbp), %r8
	movq	-344(%rbp), %r10
	jmp	.L485
.L536:
	movl	-316(%rbp), %eax
	jmp	.L484
.L531:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4181:
	.size	_ZN6icu_6713MeasureFormatC2ERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode, .-_ZN6icu_6713MeasureFormatC2ERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode
	.globl	_ZN6icu_6713MeasureFormatC1ERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode
	.set	_ZN6icu_6713MeasureFormatC1ERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode,_ZN6icu_6713MeasureFormatC2ERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MeasureFormat22setMeasureFormatLocaleERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6713MeasureFormat22setMeasureFormatLocaleERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6713MeasureFormat22setMeasureFormatLocaleERKNS_6LocaleER10UErrorCode:
.LFB4205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L587
.L537:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L588
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	leaq	-304(%rbp), %r13
	movq	%rdx, %rcx
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	%rdi, %rsi
	movq	%rdx, %r15
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZNK6icu_676Format9getLocaleE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	movq	%r13, %rdi
	testb	%al, %al
	jne	.L540
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	jle	.L589
.L541:
	testl	%edx, %edx
	setle	%al
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L540:
	call	_ZN6icu_676LocaleD1Ev@PLT
	xorl	%eax, %eax
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L589:
	movq	40(%r12), %rsi
	movl	352(%rbx), %eax
	movq	%rbx, %rdi
	movq	%rsi, %rdx
	movl	%eax, -324(%rbp)
	call	_ZN6icu_676Format12setLocaleIDsEPKcS2_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L541
	leaq	-288(%rbp), %r14
	movq	%r12, %rsi
	movq	%rax, -336(%rbp)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	movl	$0, -296(%rbp)
	movb	$0, -292(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%r15), %ecx
	movq	-336(%rbp), %rax
	testl	%ecx, %ecx
	jle	.L590
.L543:
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L541
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movl	(%r15), %edx
	movq	%rax, %r13
	testl	%edx, %edx
	jg	.L541
	movq	344(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L552
	testq	%rdi, %rdi
	je	.L553
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L553:
	movq	%r13, 344(%rbx)
	testq	%r13, %r13
	je	.L552
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L552:
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712NumberFormat20createSharedInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode@PLT
	movl	(%r15), %edx
	movq	%rax, %r13
	testl	%edx, %edx
	jg	.L541
	movq	336(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L556
	testq	%rdi, %rdi
	je	.L557
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L557:
	movq	%r13, 336(%rbx)
	testq	%r13, %r13
	je	.L556
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L556:
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movl	-324(%rbp), %eax
	movq	360(%rbx), %rdi
	movl	%eax, 352(%rbx)
	testq	%rdi, %rdi
	je	.L559
	movq	(%rdi), %rax
	call	*8(%rax)
	movl	352(%rbx), %eax
	movl	%eax, -324(%rbp)
.L559:
	movl	-324(%rbp), %eax
	leaq	.LC6(%rip), %rsi
	cmpl	$2, %eax
	jg	.L560
	movslq	%eax, %r14
	leaq	_ZZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCodeE10listStyles(%rip), %rax
	movq	(%rax,%r14,8), %rsi
.L560:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleEPKcR10UErrorCode@PLT
	movl	(%r15), %edx
	movq	%rax, 360(%rbx)
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L590:
	leaq	-316(%rbp), %r8
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	-312(%rbp), %rdx
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %eax
	movq	-312(%rbp), %r8
	testl	%eax, %eax
	jle	.L591
.L544:
	testq	%r8, %r8
	je	.L547
.L548:
	movq	%r8, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L586:
	movl	-316(%rbp), %eax
.L547:
	movl	(%r15), %edx
	testl	%edx, %edx
	je	.L549
	testl	%eax, %eax
	jle	.L543
.L549:
	movl	%eax, (%r15)
	jmp	.L543
.L591:
	movq	328(%rbx), %rdi
	cmpq	%rdi, %r8
	je	.L544
	testq	%rdi, %rdi
	je	.L545
	movq	%r8, -336(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-336(%rbp), %r8
.L545:
	movq	%r8, 328(%rbx)
	testq	%r8, %r8
	je	.L586
	movq	%r8, %rdi
	movq	%r8, -336(%rbp)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-336(%rbp), %r8
	jmp	.L548
.L588:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4205:
	.size	_ZN6icu_6713MeasureFormat22setMeasureFormatLocaleERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6713MeasureFormat22setMeasureFormatLocaleERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormateqERKNS_6FormatE
	.type	_ZNK6icu_6713MeasureFormateqERKNS_6FormatE, @function
_ZNK6icu_6713MeasureFormateqERKNS_6FormatE:
.LFB4194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L592
	movq	%rdi, %r12
	movq	%rsi, %rbx
	call	_ZNK6icu_676FormateqERKS0_@PLT
	testb	%al, %al
	je	.L592
	movl	352(%rbx), %edx
	xorl	%eax, %eax
	cmpl	%edx, 352(%r12)
	jne	.L592
	movq	328(%rbx), %rax
	cmpq	%rax, 328(%r12)
	je	.L594
	leaq	-44(%rbp), %r14
	movl	$1, %esi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	movq	%r14, %rdx
	call	_ZNK6icu_676Format11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode@PLT
	movl	$1, %esi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_ZNK6icu_676Format11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	%rax, %rsi
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L595
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L595
.L594:
	movq	336(%r12), %rcx
	movq	336(%rbx), %rdx
	movl	$1, %eax
	cmpq	%rdx, %rcx
	je	.L592
	movq	24(%rcx), %rdi
	movq	24(%rdx), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	setne	%al
	.p2align 4,,10
	.p2align 3
.L592:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L605
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L595:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L592
.L605:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4194:
	.size	_ZNK6icu_6713MeasureFormateqERKNS_6FormatE, .-_ZNK6icu_6713MeasureFormateqERKNS_6FormatE
	.section	.rodata.str1.1
.LC7:
	.string	"currency"
	.text
	.align 2
	.p2align 4
	.type	_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0, @function
_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0:
.LFB6010:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$1224, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -1224(%rbp)
	movq	120(%rsi), %rbx
	movq	%r8, -1232(%rbp)
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	8(%rsi), %rax
	movq	%rax, -1240(%rbp)
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	movl	$9, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L631
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	movq	%r14, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L632
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	leaq	-1200(%rbp), %r15
	movq	%rax, -1248(%rbp)
	leaq	-512(%rbp), %r14
	movq	%r15, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	-1048(%rbp), %r10
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	movq	%r10, %rdi
	movq	%rax, -1200(%rbp)
	movq	%r10, -1256(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	-1248(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZNK6icu_6713DecimalFormat17toNumberFormatterER10UErrorCode@PLT
	testq	%rax, %rax
	movq	%rax, -1248(%rbp)
	je	.L613
	movq	-1240(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	movq	-1256(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	movq	-1248(%rbp), %r9
	leaq	-976(%rbp), %r10
	movq	%rbx, %rdx
	movq	%r10, %rdi
	movq	%r10, -1240(%rbp)
	movq	%r9, %rsi
	call	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE@PLT
	movq	-1224(%rbp), %rax
	movl	$2, %edx
	movq	-1240(%rbp), %r10
	movl	352(%rax), %eax
	testl	%eax, %eax
	je	.L614
	subl	$2, %eax
	xorl	%edx, %edx
	cmpl	$1, %eax
	seta	%dl
.L614:
	leaq	-512(%rbp), %r14
	movq	%r10, %rsi
	movq	%r10, -1224(%rbp)
	movq	%r14, %rdi
	call	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	-1224(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
.L613:
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L615
	sarl	$5, %edx
.L616:
	movq	-1232(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r15, %rdi
	call	_ZN6icu_6713DecimalFormat19fieldPositionHelperERKNS_6number4impl20UFormattedNumberDataERNS_13FieldPositionEiR10UErrorCode@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl12toTempStringER10UErrorCode@PLT
	movswl	-504(%rbp), %ecx
	testw	%cx, %cx
	js	.L617
	sarl	$5, %ecx
.L618:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
.L606:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L633
	addq	$1224, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L632:
	.cfi_restore_state
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	movq	-1232(%rbp), %r8
	movq	-1240(%rbp), %rdi
	movq	%rax, -1200(%rbp)
	movq	-1224(%rbp), %rax
	movq	%r13, %r9
	movq	%r14, %rsi
	leaq	-1200(%rbp), %r15
	movw	%cx, -1192(%rbp)
	movq	344(%rax), %rax
	movq	%r15, %rcx
	movq	24(%rax), %rdx
	call	_ZN6icu_6717QuantityFormatter12selectPluralERKNS_11FormattableERKNS_12NumberFormatERKNS_11PluralRulesERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode@PLT
	movl	$2, %r11d
	movl	%eax, %r8d
	movq	-1224(%rbp), %rax
	movl	352(%rax), %edx
	testl	%edx, %edx
	je	.L611
	subl	$2, %edx
	xorl	%r11d, %r11d
	cmpl	$1, %edx
	seta	%r11b
.L611:
	leaq	-512(%rbp), %r14
	movq	-1224(%rbp), %rsi
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	%r11d, -1248(%rbp)
	movl	%r8d, -1240(%rbp)
	call	_ZNK6icu_676Format9getLocaleE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	%rbx, %rdx
	movq	%r13, %r9
	movq	%r14, %rsi
	movl	-1248(%rbp), %r11d
	leaq	-976(%rbp), %r10
	movl	-1240(%rbp), %r8d
	movq	%r10, %rdi
	movq	%r10, -1224(%rbp)
	movl	%r11d, %ecx
	call	_ZN6icu_676number4impl15LongNameHandler14getUnitPatternERKNS_6LocaleERKNS_11MeasureUnitE16UNumberUnitWidthNS_14StandardPlural4FormER10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	0(%r13), %edx
	movq	-1224(%rbp), %r10
	testl	%edx, %edx
	jle	.L634
.L612:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L631:
	movq	%rbx, %rdi
	leaq	-976(%rbp), %r15
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movl	$4, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	u_charsToUChars_67@PLT
	movl	$2, %eax
	movl	$128, %edi
	movq	-1224(%rbp), %rbx
	cmpl	$2, 352(%rbx)
	cmovle	352(%rbx), %eax
	movq	328(%rbx), %rdx
	cltq
	movq	40(%rdx,%rax,8), %r14
	movq	(%r14), %rax
	movq	%r15, -1208(%rbp)
	movq	40(%rax), %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L608
	movq	-1240(%rbp), %rsi
	leaq	-1208(%rbp), %rdx
	movq	%r13, %rcx
	movq	%rax, %rdi
	movq	%rax, -1224(%rbp)
	call	_ZN6icu_6714CurrencyAmountC1ERKNS_11FormattableENS_14ConstChar16PtrER10UErrorCode@PLT
	movq	-1224(%rbp), %r8
.L608:
	leaq	-512(%rbp), %r15
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711FormattableC1EPNS_7UObjectE@PLT
	movq	%r12, %rdx
	movq	%r13, %r8
	movq	%r15, %rsi
	movq	-1232(%rbp), %rcx
	movq	%r14, %rdi
	call	*%rbx
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6711FormattableD1Ev@PLT
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L634:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r10, %rsi
	movq	%r13, %r8
	xorl	%edx, %edx
	movq	%rax, -504(%rbp)
	movl	$1, %ecx
	movq	%r14, %rdi
	movl	$2, %eax
	movw	%ax, -496(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	%r12, %rdx
	movq	%r13, %r8
	movq	%r15, %rsi
	movq	-1232(%rbp), %rcx
	movq	%r14, %rdi
	call	_ZN6icu_6717QuantityFormatter6formatERKNS_15SimpleFormatterERKNS_13UnicodeStringERS4_RNS_13FieldPositionER10UErrorCode@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	-1224(%rbp), %r10
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L617:
	movl	-500(%rbp), %ecx
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L615:
	movl	12(%r12), %edx
	jmp	.L616
.L633:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6010:
	.size	_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0, .-_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB4211:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jle	.L637
	movq	%rcx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L637:
	jmp	_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	.cfi_endproc
.LFE4211:
	.size	_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6713MeasureFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6713MeasureFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB4196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jle	.L650
.L638:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L650:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movq	%rsi, %r12
	movq	%rcx, %r14
	movq	%r8, %rbx
	call	_ZNK6icu_6711Formattable7getTypeEv@PLT
	cmpl	$6, %eax
	je	.L651
.L641:
	movl	$1, (%rbx)
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L651:
	movq	%r12, %rdi
	call	_ZNK6icu_6711Formattable9getObjectEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L641
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_677MeasureE(%rip), %rdx
	leaq	_ZTIN6icu_677UObjectE(%rip), %rsi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L641
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L638
	movq	336(%r13), %rdx
	movq	%rbx, %r9
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	24(%rdx), %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	.cfi_endproc
.LFE4196:
	.size	_ZNK6icu_6713MeasureFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6713MeasureFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6713MeasureFormat23formatMeasuresSlowTrackEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0, @function
_ZNK6icu_6713MeasureFormat23formatMeasuresSlowTrackEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0:
.LFB6012:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movslq	%edx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	salq	$6, %rdi
	pushq	%rbx
	addq	$8, %rdi
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%rcx, -176(%rbp)
	movl	8(%r8), %edx
	movl	$4294967295, %ecx
	movl	%r14d, -132(%rbp)
	movq	%r8, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	%rcx, -104(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rax, -80(%rbp)
	movabsq	$144115188075855871, %rax
	cmpq	%rax, %r14
	movq	$-1, %rax
	movl	$0, -96(%rbp)
	cmova	%rax, %rdi
	movl	%edx, -72(%rbp)
	movq	$0, -68(%rbp)
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L654
	movq	%r14, %rdx
	movq	%r14, (%rax)
	addq	$8, %r12
	subq	$1, %rdx
	js	.L669
	movq	%r12, %rax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L656:
	movl	$2, %r11d
	subq	$1, %rdx
	movq	%rcx, (%rax)
	addq	$64, %rax
	movw	%r11w, -56(%rax)
	cmpq	$-1, %rdx
	jne	.L656
.L669:
	movl	-132(%rbp), %eax
	testl	%eax, %eax
	jle	.L672
	subl	$1, %eax
	movq	%rbx, %rsi
	movq	%r12, %rcx
	movl	$-1, %r14d
	movl	%eax, -136(%rbp)
	leaq	-80(%rbp), %rax
	movq	%r15, %r9
	xorl	%ebx, %ebx
	movq	%rax, -152(%rbp)
	leaq	-112(%rbp), %rax
	movq	%r13, %r15
	movq	%rsi, %r13
	movq	%r12, -168(%rbp)
	movl	%r14d, %r12d
	movq	%rcx, %r14
	movq	%rax, -160(%rbp)
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L659:
	movl	(%r9), %edi
	testl	%edi, %edi
	jg	.L663
	movq	-160(%rbp), %r8
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r9, -144(%rbp)
	call	_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	movq	-144(%rbp), %r9
.L663:
	addl	$1, %ebx
	subq	$-128, %r13
	addq	$64, %r14
	cmpl	%ebx, -132(%rbp)
	je	.L691
.L664:
	movq	328(%r15), %rax
	movq	64(%rax), %rdx
	cmpl	-136(%rbp), %ebx
	jne	.L658
	movq	336(%r15), %rax
	movq	24(%rax), %rdx
.L658:
	cmpl	$-1, %r12d
	jne	.L659
	movl	(%r9), %r10d
	testl	%r10d, %r10d
	jle	.L692
.L688:
	movq	-168(%rbp), %r12
.L661:
	testq	%r12, %r12
	je	.L665
	movq	-8(%r12), %rbx
	salq	$6, %rbx
	addq	%r12, %rbx
	cmpq	%r12, %rbx
	je	.L667
	.p2align 4,,10
	.p2align 3
.L666:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L666
.L667:
	leaq	-8(%r12), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L665:
	movq	-152(%rbp), %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	-160(%rbp), %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L693
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_restore_state
	movq	-152(%rbp), %r8
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r9, -144(%rbp)
	call	_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	movq	-144(%rbp), %r9
	movl	(%r9), %r8d
	testl	%r8d, %r8d
	jg	.L688
	movl	-68(%rbp), %eax
	orl	-64(%rbp), %eax
	cmovne	%ebx, %r12d
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L691:
	movl	%r12d, %r14d
	movq	-168(%rbp), %r12
	movq	%r15, %r13
	movq	%r9, %r15
.L657:
	subq	$8, %rsp
	movl	-132(%rbp), %edx
	movq	%r12, %rsi
	movl	%r14d, %r8d
	movq	360(%r13), %rdi
	pushq	%r15
	leaq	-116(%rbp), %r9
	movq	-176(%rbp), %rcx
	call	_ZNK6icu_6713ListFormatter6formatEPKNS_13UnicodeStringEiRS1_iRiR10UErrorCode@PLT
	movl	(%r15), %esi
	popq	%rdx
	popq	%rcx
	testl	%esi, %esi
	jg	.L661
	movl	-116(%rbp), %eax
	cmpl	$-1, %eax
	je	.L661
	movl	-68(%rbp), %edx
	movq	-184(%rbp), %rsi
	addl	%eax, %edx
	addl	-64(%rbp), %eax
	movl	%edx, 12(%rsi)
	movl	%eax, 16(%rsi)
	jmp	.L661
.L654:
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L669
	movl	$7, (%r15)
	jmp	.L669
.L672:
	leaq	-80(%rbp), %rax
	movl	$-1, %r14d
	movq	%rax, -152(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -160(%rbp)
	jmp	.L657
.L693:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6012:
	.size	_ZNK6icu_6713MeasureFormat23formatMeasuresSlowTrackEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0, .-_ZNK6icu_6713MeasureFormat23formatMeasuresSlowTrackEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat23formatMeasuresSlowTrackEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6713MeasureFormat23formatMeasuresSlowTrackEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6713MeasureFormat23formatMeasuresSlowTrackEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB4228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	subq	$8, %rsp
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L695
	call	_ZNK6icu_6713MeasureFormat23formatMeasuresSlowTrackEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
.L695:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4228:
	.size	_ZNK6icu_6713MeasureFormat23formatMeasuresSlowTrackEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6713MeasureFormat23formatMeasuresSlowTrackEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6713MeasureFormat13formatNumericEPKNS_11FormattableEiRNS_13UnicodeStringER10UErrorCode.part.0, @function
_ZNK6icu_6713MeasureFormat13formatNumericEPKNS_11FormattableEiRNS_13UnicodeStringER10UErrorCode.part.0:
.LFB6013:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%r8, %rsi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$1304, %rsp
	movq	%rcx, -1288(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%r10w, -1208(%rbp)
	movq	%r13, -1216(%rbp)
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	leaq	112(%r12), %rdi
	movq	%rbx, %rsi
	movsd	%xmm0, -1264(%rbp)
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	leaq	224(%r12), %rdi
	movq	%rbx, %rsi
	leaq	-1216(%rbp), %r12
	movsd	%xmm0, -1272(%rbp)
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	movl	(%rbx), %r11d
	movsd	%xmm0, -1280(%rbp)
	testl	%r11d, %r11d
	jg	.L698
	movl	%r15d, %eax
	andl	$-3, %eax
	cmpl	$5, %eax
	je	.L750
	cmpl	$3, %r15d
	je	.L751
	cmpl	$6, %r15d
	je	.L752
.L702:
	movl	$5, (%rbx)
.L698:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L753
	addq	$1304, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L750:
	.cfi_restore_state
	movq	328(%r14), %rax
	movq	%r12, %rdi
	movq	72(%rax), %rsi
	addq	$136, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movsd	-1264(%rbp), %xmm0
	call	uprv_trunc_67@PLT
	movsd	%xmm0, -1264(%rbp)
	movsd	-1272(%rbp), %xmm0
	call	uprv_trunc_67@PLT
	movsd	%xmm0, -1272(%rbp)
.L700:
	movq	336(%r14), %rax
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L702
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, -1304(%rbp)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L702
	leaq	-976(%rbp), %rax
	leaq	-960(%rbp), %rdi
	movl	$2, -972(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -968(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	-936(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %r8d
	pxor	%xmm0, %xmm0
	movl	$-1, %r9d
	leaq	-760(%rbp), %rdi
	movw	%r8w, -876(%rbp)
	movabsq	$30064771077, %rcx
	movw	%r9w, -852(%rbp)
	movq	%rcx, -824(%rbp)
	movaps	%xmm0, -784(%rbp)
	movl	$0, -912(%rbp)
	movl	$4, -880(%rbp)
	movl	$-2, -864(%rbp)
	movb	$0, -844(%rbp)
	movl	$0, -840(%rbp)
	movq	$0, -832(%rbp)
	movl	$2, -816(%rbp)
	movl	$0, -808(%rbp)
	movq	$0, -800(%rbp)
	movl	$0, -792(%rbp)
	movl	$3, -768(%rbp)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	call	_ZNK6icu_6713DecimalFormat17toNumberFormatterER10UErrorCode@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L703
	movl	$2, %edi
	leaq	-512(%rbp), %r14
	call	_ZN6icu_676number12IntegerWidth10zeroFillToEi@PLT
	leaq	-1248(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%edx, -1240(%rbp)
	movq	%rcx, %rdx
	movq	%rcx, -1336(%rbp)
	movq	%rax, -1248(%rbp)
	call	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE@PLT
	movq	-1296(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_676number24LocalizedNumberFormatteraSEOS1_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6722FormattedStringBuilderC1Ev@PLT
	movzwl	-1208(%rbp), %eax
	testw	%ax, %ax
	js	.L704
	movswl	%ax, %edx
	sarl	$5, %edx
	movl	%edx, -1252(%rbp)
.L705:
	movl	-1252(%rbp), %edi
	leaq	-1088(%rbp), %r15
	testl	%edi, %edi
	jle	.L706
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %ecx
	movb	$0, -1253(%rbp)
	xorl	%r9d, %r9d
	leaq	-1088(%rbp), %r15
	movq	%r12, -1328(%rbp)
	movb	%cl, -1254(%rbp)
	movb	%cl, -1255(%rbp)
	movb	%cl, -1317(%rbp)
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L755:
	movswl	%ax, %edx
	leal	1(%r9), %r12d
	sarl	$5, %edx
	cmpl	%r9d, %edx
	jbe	.L725
.L756:
	testb	$2, %al
	leaq	-1206(%rbp), %rcx
	movslq	%r9d, %rax
	cmove	-1192(%rbp), %rcx
	leaq	(%rax,%rax), %rdi
	movzwl	(%rcx,%rax,2), %eax
	cmpw	$109, %ax
	je	.L727
	cmpw	$115, %ax
	je	.L728
	cmpw	$72, %ax
	je	.L729
	movzwl	%ax, %r11d
	cmpw	$39, %ax
	jne	.L754
	cmpl	%r12d, -1252(%rbp)
	jle	.L718
	cmpl	%r12d, %edx
	jbe	.L718
	cmpw	$39, 2(%rcx,%rdi)
	je	.L719
.L718:
	xorb	$1, -1253(%rbp)
	movl	%r12d, %r9d
.L714:
	cmpl	%r9d, -1252(%rbp)
	jle	.L747
.L758:
	movzwl	-1208(%rbp), %eax
.L720:
	testw	%ax, %ax
	jns	.L755
	movl	-1204(%rbp), %edx
	leal	1(%r9), %r12d
	cmpl	%r9d, %edx
	ja	.L756
.L725:
	movl	$65535, %r11d
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L754:
	leal	-72(%rax), %r8d
	cmpw	$43, %r8w
	jbe	.L757
.L709:
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %ecx
	movq	%rbx, %r8
.L749:
	movl	-380(%rbp), %esi
	movl	%r11d, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode@PLT
	movl	%r12d, %r9d
	cmpl	%r9d, -1252(%rbp)
	jg	.L758
.L747:
	movq	-1328(%rbp), %r12
.L706:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder19toTempUnicodeStringEv@PLT
	movswl	-1080(%rbp), %ecx
	testw	%cx, %cx
	js	.L721
	sarl	$5, %ecx
.L722:
	movq	-1288(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6722FormattedStringBuilderD1Ev@PLT
.L703:
	movq	-1296(%rbp), %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L757:
	movabsq	$8933531975681, %rsi
	btq	%r8, %rsi
	jnc	.L709
	pxor	%xmm0, %xmm0
.L723:
	cmpb	$0, -1253(%rbp)
	jne	.L759
.L713:
	movl	$2, %esi
	movq	%r13, -1152(%rbp)
	movw	%si, -1144(%rbp)
	cmpl	%r12d, -1252(%rbp)
	jle	.L715
	cmpl	%r12d, %edx
	jbe	.L715
	cmpw	%ax, 2(%rcx,%rdi)
	je	.L716
.L715:
	movq	%r15, %rdi
	call	_ZN6icu_6711FormattableC1Ed@PLT
	leaq	-1152(%rbp), %r11
	movq	%rbx, %rcx
	movq	%r15, %rsi
	movq	-1304(%rbp), %rdi
	movq	%r11, %rdx
	movq	%r11, -1312(%rbp)
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	-1312(%rbp), %r11
	movl	%r12d, %r9d
.L717:
	movzbl	-1254(%rbp), %ecx
	movq	%r11, %rdx
	movq	%rbx, %r8
	movq	%r14, %rdi
	movl	-380(%rbp), %esi
	movl	%r9d, -1316(%rbp)
	movq	%r11, -1312(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	-1312(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-1316(%rbp), %r9d
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L729:
	cmpb	$0, -1253(%rbp)
	movsd	-1264(%rbp), %xmm0
	movl	$72, %r11d
	je	.L713
.L759:
	movzbl	-1255(%rbp), %ecx
	movq	%rbx, %r8
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L728:
	movsd	-1280(%rbp), %xmm0
	movl	$115, %r11d
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L727:
	movsd	-1272(%rbp), %xmm0
	movl	$109, %r11d
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L752:
	movq	328(%r14), %rax
	movq	%r12, %rdi
	movq	72(%rax), %rsi
	addq	$72, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movsd	-1272(%rbp), %xmm0
	call	uprv_trunc_67@PLT
	movsd	%xmm0, -1272(%rbp)
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L751:
	movq	328(%r14), %rax
	movq	%r12, %rdi
	movq	72(%rax), %rsi
	addq	$8, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movsd	-1264(%rbp), %xmm0
	call	uprv_trunc_67@PLT
	movsd	%xmm0, -1264(%rbp)
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L721:
	movl	-1076(%rbp), %ecx
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L704:
	movl	-1204(%rbp), %ecx
	movl	%ecx, -1252(%rbp)
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L719:
	movzbl	-1317(%rbp), %ecx
	movq	%rbx, %r8
	movl	$39, %edx
	movq	%r14, %rdi
	movl	-380(%rbp), %esi
	movl	%r9d, -1312(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode@PLT
	movl	-1312(%rbp), %r9d
	addl	$2, %r9d
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L716:
	movq	-1336(%rbp), %r12
	movq	-1296(%rbp), %rsi
	movq	%rbx, %rdx
	movl	%r9d, -1316(%rbp)
	movq	%r12, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter12formatDoubleEdR10UErrorCode@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_676number15FormattedNumber8toStringER10UErrorCode@PLT
	leaq	-1152(%rbp), %r11
	movq	%r15, %rsi
	movq	%r11, %rdi
	movq	%r11, -1312(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number15FormattedNumberD1Ev@PLT
	movl	-1316(%rbp), %r9d
	movq	-1312(%rbp), %r11
	addl	$2, %r9d
	jmp	.L717
.L753:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6013:
	.size	_ZNK6icu_6713MeasureFormat13formatNumericEPKNS_11FormattableEiRNS_13UnicodeStringER10UErrorCode.part.0, .-_ZNK6icu_6713MeasureFormat13formatNumericEPKNS_11FormattableEiRNS_13UnicodeStringER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat13formatNumericEPKNS_11FormattableEiRNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6713MeasureFormat13formatNumericEPKNS_11FormattableEiRNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6713MeasureFormat13formatNumericEPKNS_11FormattableEiRNS_13UnicodeStringER10UErrorCode:
.LFB4212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	subq	$8, %rsp
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L761
	call	_ZNK6icu_6713MeasureFormat13formatNumericEPKNS_11FormattableEiRNS_13UnicodeStringER10UErrorCode.part.0
.L761:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4212:
	.size	_ZNK6icu_6713MeasureFormat13formatNumericEPKNS_11FormattableEiRNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6713MeasureFormat13formatNumericEPKNS_11FormattableEiRNS_13UnicodeStringER10UErrorCode
	.section	.rodata.str1.1
.LC9:
	.string	"duration"
.LC10:
	.string	"hour"
.LC11:
	.string	"minute"
.LC12:
	.string	"second"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MeasureFormat14formatMeasuresEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6713MeasureFormat14formatMeasuresEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6713MeasureFormat14formatMeasuresEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB4199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -416(%rbp)
	movl	(%r9), %r10d
	movq	%r8, -408(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L817
	movl	%edx, %r15d
	testl	%edx, %edx
	je	.L817
	movq	%rdi, %r12
	movq	%r9, %rbx
	cmpl	$1, %edx
	je	.L818
	cmpl	$3, 352(%rdi)
	je	.L819
	cmpl	$-1, 8(%r8)
	jne	.L795
.L780:
	movabsq	$144115188075855871, %rax
	movslq	%r15d, %r13
	cmpq	%rax, %r13
	ja	.L782
	movq	%r13, %rdi
	salq	$6, %rdi
	addq	$8, %rdi
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, -432(%rbp)
	testq	%rax, %rax
	je	.L784
	movq	%r13, (%rax)
	addq	$8, %rax
	leaq	-1(%r13), %rdx
	movq	%rax, -424(%rbp)
.L785:
	movq	-424(%rbp), %rax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L790:
	movl	$2, %esi
	subq	$1, %rdx
	movq	%rcx, (%rax)
	addq	$64, %rax
	movw	%si, -56(%rax)
	cmpq	$-1, %rdx
	jne	.L790
.L789:
	testl	%r15d, %r15d
	jle	.L793
	movq	328(%r12), %rax
	leal	-1(%r15), %r10d
	xorl	%r13d, %r13d
	movl	%r15d, -440(%rbp)
	movq	%r14, -448(%rbp)
	movq	%rbx, %r15
	movq	%r12, %r14
	movq	%r10, %rbx
	movq	64(%rax), %rdx
	movl	%r10d, %r12d
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L806:
	movq	328(%r14), %rax
	movq	64(%rax), %rdx
	leal	1(%r13), %eax
	cmpl	%eax, %r12d
	jne	.L791
	movq	336(%r14), %rax
	movq	24(%rax), %rdx
.L791:
	addq	$1, %r13
.L796:
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L792
	movq	%r13, %rcx
	movq	%r13, %rsi
	movq	-408(%rbp), %r8
	movq	%r15, %r9
	salq	$6, %rcx
	salq	$7, %rsi
	movq	%r14, %rdi
	addq	-424(%rbp), %rcx
	addq	-416(%rbp), %rsi
	call	_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
.L792:
	cmpq	%r13, %rbx
	jne	.L806
	movq	%r14, %r12
	movq	%r15, %rbx
	movq	-448(%rbp), %r14
	movl	-440(%rbp), %r15d
.L793:
	movl	%r15d, %edx
	movq	-424(%rbp), %r15
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	360(%r12), %rdi
	movq	%r15, %rsi
	call	_ZNK6icu_6713ListFormatter6formatEPKNS_13UnicodeStringEiRS1_R10UErrorCode@PLT
	movq	-432(%rbp), %rax
	movq	-424(%rbp), %r12
	movq	(%rax), %rax
	movq	%rax, %rbx
	movq	%rax, -408(%rbp)
	salq	$6, %rbx
	addq	%r15, %rbx
	cmpq	%r15, %rbx
	je	.L787
	.p2align 4,,10
	.p2align 3
.L788:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L788
.L787:
	movq	-432(%rbp), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
	movq	%r14, %rax
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L819:
	leaq	-400(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN6icu_6711FormattableC1Ev@PLT
	leaq	-288(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN6icu_6711FormattableC1Ev@PLT
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L820
.L769:
	leaq	-64(%rbp), %r13
.L776:
	movq	-112(%r13), %rax
	subq	$112, %r13
	movq	%r13, %rdi
	call	*(%rax)
	cmpq	-424(%rbp), %r13
	jne	.L776
	movq	-408(%rbp), %rax
	cmpl	$-1, 8(%rax)
	je	.L780
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.L795
	.p2align 4,,10
	.p2align 3
.L817:
	movq	%r14, %rax
.L763:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L821
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L795:
	.cfi_restore_state
	movq	-408(%rbp), %r8
	movq	%rbx, %r9
	movq	%r14, %rcx
	movl	%r15d, %edx
	movq	-416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713MeasureFormat23formatMeasuresSlowTrackEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L818:
	movq	336(%rdi), %rax
	movq	24(%rax), %rdx
	call	_ZNK6icu_6713MeasureFormat13formatMeasureERKNS_7MeasureERKNS_12NumberFormatERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L782:
	movq	$-1, %rdi
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, -432(%rbp)
	testq	%rax, %rax
	je	.L784
	movq	%r13, (%rax)
	movq	%r13, %rdx
	addq	$8, %rax
	movq	%rax, -424(%rbp)
	subq	$1, %rdx
	jns	.L785
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L820:
	testl	%r15d, %r15d
	jle	.L769
	movq	-416(%rbp), %rdx
	leal	-1(%r15), %eax
	movq	%r14, -440(%rbp)
	salq	$7, %rax
	movq	%rbx, -448(%rbp)
	movq	-424(%rbp), %rbx
	leaq	136(%rdx,%rax), %rax
	leaq	8(%rdx), %r13
	xorl	%edx, %edx
	movq	%r12, -432(%rbp)
	movl	%r15d, -460(%rbp)
	movl	%edx, %r12d
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L773:
	movq	112(%r13), %r14
	movq	%r14, %rdi
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	movl	$9, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L822
.L770:
	movq	112(%r13), %r14
	movq	%r14, %rdi
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	movl	$9, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L823
.L772:
	movq	112(%r13), %r14
	movq	%r14, %rdi
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	movl	$9, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L824
.L814:
	movq	-432(%rbp), %r12
	movl	-460(%rbp), %r15d
	movq	-440(%rbp), %r14
	movq	-448(%rbp), %rbx
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L822:
	movq	%r14, %rdi
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movl	$5, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L770
	testl	%r12d, %r12d
	jne	.L814
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	pxor	%xmm0, %xmm0
	comisd	-392(%rbp), %xmm0
	ja	.L814
	movl	$1, %r12d
.L771:
	subq	$-128, %r13
	cmpq	%r15, %r13
	jne	.L773
	movq	-448(%rbp), %rbx
	movl	%r12d, %edx
	movq	-440(%rbp), %r14
	movq	-432(%rbp), %r12
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L774
	movq	-424(%rbp), %rsi
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r12, %rdi
	call	_ZNK6icu_6713MeasureFormat13formatNumericEPKNS_11FormattableEiRNS_13UnicodeStringER10UErrorCode.part.0
.L774:
	movq	-424(%rbp), %r12
	leaq	-64(%rbp), %rbx
.L778:
	movq	-112(%rbx), %rax
	subq	$112, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L778
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L823:
	movq	%r14, %rdi
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movl	$7, %ecx
	leaq	.LC11(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L772
	cmpl	$1, %r12d
	jg	.L814
	movq	-456(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	pxor	%xmm1, %xmm1
	comisd	-280(%rbp), %xmm1
	ja	.L814
	orl	$2, %r12d
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L824:
	movq	%r14, %rdi
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movl	$7, %ecx
	leaq	.LC12(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L814
	cmpl	$3, %r12d
	jg	.L814
	movq	-472(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	pxor	%xmm2, %xmm2
	comisd	-168(%rbp), %xmm2
	ja	.L814
	orl	$4, %r12d
	jmp	.L771
.L784:
	movl	$7, (%rbx)
	movq	%r14, %rax
	jmp	.L763
.L821:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4199:
	.size	_ZNK6icu_6713MeasureFormat14formatMeasuresEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6713MeasureFormat14formatMeasuresEPKNS_7MeasureEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.weak	_ZTSN6icu_6713MeasureFormatE
	.section	.rodata._ZTSN6icu_6713MeasureFormatE,"aG",@progbits,_ZTSN6icu_6713MeasureFormatE,comdat
	.align 16
	.type	_ZTSN6icu_6713MeasureFormatE, @object
	.size	_ZTSN6icu_6713MeasureFormatE, 25
_ZTSN6icu_6713MeasureFormatE:
	.string	"N6icu_6713MeasureFormatE"
	.weak	_ZTIN6icu_6713MeasureFormatE
	.section	.data.rel.ro._ZTIN6icu_6713MeasureFormatE,"awG",@progbits,_ZTIN6icu_6713MeasureFormatE,comdat
	.align 8
	.type	_ZTIN6icu_6713MeasureFormatE, @object
	.size	_ZTIN6icu_6713MeasureFormatE, 24
_ZTIN6icu_6713MeasureFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713MeasureFormatE
	.quad	_ZTIN6icu_676FormatE
	.weak	_ZTSN6icu_6722MeasureFormatCacheDataE
	.section	.rodata._ZTSN6icu_6722MeasureFormatCacheDataE,"aG",@progbits,_ZTSN6icu_6722MeasureFormatCacheDataE,comdat
	.align 32
	.type	_ZTSN6icu_6722MeasureFormatCacheDataE, @object
	.size	_ZTSN6icu_6722MeasureFormatCacheDataE, 34
_ZTSN6icu_6722MeasureFormatCacheDataE:
	.string	"N6icu_6722MeasureFormatCacheDataE"
	.weak	_ZTIN6icu_6722MeasureFormatCacheDataE
	.section	.data.rel.ro._ZTIN6icu_6722MeasureFormatCacheDataE,"awG",@progbits,_ZTIN6icu_6722MeasureFormatCacheDataE,comdat
	.align 8
	.type	_ZTIN6icu_6722MeasureFormatCacheDataE, @object
	.size	_ZTIN6icu_6722MeasureFormatCacheDataE, 24
_ZTIN6icu_6722MeasureFormatCacheDataE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722MeasureFormatCacheDataE
	.quad	_ZTIN6icu_6712SharedObjectE
	.weak	_ZTSN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE
	.section	.rodata._ZTSN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE,"aG",@progbits,_ZTSN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE,comdat
	.align 32
	.type	_ZTSN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE, @object
	.size	_ZTSN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE, 49
_ZTSN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE:
	.string	"N6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE"
	.weak	_ZTIN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE
	.section	.data.rel.ro._ZTIN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE,"awG",@progbits,_ZTIN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE,comdat
	.align 8
	.type	_ZTIN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE, @object
	.size	_ZTIN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE, 24
_ZTIN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE
	.quad	_ZTIN6icu_6712CacheKeyBaseE
	.weak	_ZTSN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE
	.section	.rodata._ZTSN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE,"aG",@progbits,_ZTSN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE,comdat
	.align 32
	.type	_ZTSN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE, @object
	.size	_ZTSN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE, 56
_ZTSN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE:
	.string	"N6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE"
	.weak	_ZTIN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE
	.section	.data.rel.ro._ZTIN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE,"awG",@progbits,_ZTIN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE,comdat
	.align 8
	.type	_ZTIN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE, @object
	.size	_ZTIN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE, 24
_ZTIN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE
	.quad	_ZTIN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE
	.weak	_ZTVN6icu_6722MeasureFormatCacheDataE
	.section	.data.rel.ro._ZTVN6icu_6722MeasureFormatCacheDataE,"awG",@progbits,_ZTVN6icu_6722MeasureFormatCacheDataE,comdat
	.align 8
	.type	_ZTVN6icu_6722MeasureFormatCacheDataE, @object
	.size	_ZTVN6icu_6722MeasureFormatCacheDataE, 40
_ZTVN6icu_6722MeasureFormatCacheDataE:
	.quad	0
	.quad	_ZTIN6icu_6722MeasureFormatCacheDataE
	.quad	_ZN6icu_6722MeasureFormatCacheDataD1Ev
	.quad	_ZN6icu_6722MeasureFormatCacheDataD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.weak	_ZTVN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE
	.section	.data.rel.ro._ZTVN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE,"awG",@progbits,_ZTVN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE,comdat
	.align 8
	.type	_ZTVN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE, @object
	.size	_ZTVN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE, 80
_ZTVN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE:
	.quad	0
	.quad	_ZTIN6icu_678CacheKeyINS_22MeasureFormatCacheDataEEE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci
	.weak	_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE
	.section	.data.rel.ro._ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE,"awG",@progbits,_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE,comdat
	.align 8
	.type	_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE, @object
	.size	_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE, 80
_ZTVN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE:
	.quad	0
	.quad	_ZTIN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEE
	.quad	_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED1Ev
	.quad	_ZN6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEED0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE8hashCodeEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE5cloneEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEEeqERKNS_12CacheKeyBaseE
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE12createObjectEPKvR10UErrorCode
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE16writeDescriptionEPci
	.weak	_ZTVN6icu_6713MeasureFormatE
	.section	.data.rel.ro._ZTVN6icu_6713MeasureFormatE,"awG",@progbits,_ZTVN6icu_6713MeasureFormatE,comdat
	.align 8
	.type	_ZTVN6icu_6713MeasureFormatE, @object
	.size	_ZTVN6icu_6713MeasureFormatE, 80
_ZTVN6icu_6713MeasureFormatE:
	.quad	0
	.quad	_ZTIN6icu_6713MeasureFormatE
	.quad	_ZN6icu_6713MeasureFormatD1Ev
	.quad	_ZN6icu_6713MeasureFormatD0Ev
	.quad	_ZNK6icu_6713MeasureFormat17getDynamicClassIDEv
	.quad	_ZNK6icu_6713MeasureFormateqERKNS_6FormatE
	.quad	_ZNK6icu_6713MeasureFormat5cloneEv
	.quad	_ZNK6icu_6713MeasureFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6713MeasureFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.section	.rodata.str1.1
.LC13:
	.string	"unit"
.LC14:
	.string	"unit-short"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCodeE10listStyles, @object
	.size	_ZZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCodeE10listStyles, 24
_ZZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCodeE10listStyles:
	.quad	.LC13
	.quad	.LC14
	.quad	.LC6
	.section	.rodata
	.align 8
	.type	_ZZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE12createObjectEPKvR10UErrorCodeE14currencyStyles, @object
	.size	_ZZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE12createObjectEPKvR10UErrorCodeE14currencyStyles, 12
_ZZNK6icu_6714LocaleCacheKeyINS_22MeasureFormatCacheDataEE12createObjectEPKvR10UErrorCodeE14currencyStyles:
	.long	11
	.long	10
	.long	2
	.local	_ZZN6icu_6713MeasureFormat16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6713MeasureFormat16getStaticClassIDEvE7classID,1,1
	.type	_ZN6icu_67L15kUndefinedFieldE, @object
	.size	_ZN6icu_67L15kUndefinedFieldE, 1
_ZN6icu_67L15kUndefinedFieldE:
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
