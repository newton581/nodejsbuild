	.file	"currunit.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712CurrencyUnit17getDynamicClassIDEv
	.type	_ZNK6icu_6712CurrencyUnit17getDynamicClassIDEv, @function
_ZNK6icu_6712CurrencyUnit17getDynamicClassIDEv:
.LFB2357:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712CurrencyUnit16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2357:
	.size	_ZNK6icu_6712CurrencyUnit17getDynamicClassIDEv, .-_ZNK6icu_6712CurrencyUnit17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712CurrencyUnit5cloneEv
	.type	_ZNK6icu_6712CurrencyUnit5cloneEv, @function
_ZNK6icu_6712CurrencyUnit5cloneEv:
.LFB2351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6711MeasureUnitC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6712CurrencyUnitE(%rip), %rax
	leaq	20(%rbx), %rsi
	movq	%rax, (%r12)
	leaq	20(%r12), %rdi
	call	u_strcpy_67@PLT
.L3:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2351:
	.size	_ZNK6icu_6712CurrencyUnit5cloneEv, .-_ZNK6icu_6712CurrencyUnit5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CurrencyUnitD2Ev
	.type	_ZN6icu_6712CurrencyUnitD2Ev, @function
_ZN6icu_6712CurrencyUnitD2Ev:
.LFB2353:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712CurrencyUnitE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6711MeasureUnitD2Ev@PLT
	.cfi_endproc
.LFE2353:
	.size	_ZN6icu_6712CurrencyUnitD2Ev, .-_ZN6icu_6712CurrencyUnitD2Ev
	.globl	_ZN6icu_6712CurrencyUnitD1Ev
	.set	_ZN6icu_6712CurrencyUnitD1Ev,_ZN6icu_6712CurrencyUnitD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CurrencyUnitD0Ev
	.type	_ZN6icu_6712CurrencyUnitD0Ev, @function
_ZN6icu_6712CurrencyUnitD0Ev:
.LFB2355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712CurrencyUnitE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6711MeasureUnitD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2355:
	.size	_ZN6icu_6712CurrencyUnitD0Ev, .-_ZN6icu_6712CurrencyUnitD0Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB2594:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2594:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB2597:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L25
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L13
	cmpb	$0, 12(%rbx)
	jne	.L26
.L17:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L17
	.cfi_endproc
.LFE2597:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB2600:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L29
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2600:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB2603:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L32
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE2603:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB2605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L38
.L34:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L39
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2605:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB2606:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2606:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB2607:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2607:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB2608:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2608:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2609:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2609:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2610:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2610:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB2611:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L55
	testl	%edx, %edx
	jle	.L55
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L58
.L47:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L47
	.cfi_endproc
.LFE2611:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB2612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L62
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L62
	testl	%r12d, %r12d
	jg	.L69
	cmpb	$0, 12(%rbx)
	jne	.L70
.L64:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L64
.L70:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L62:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2612:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB2613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L72
	movq	(%rdi), %r8
.L73:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L76
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L76
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2613:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB2614:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L83
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2614:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB2615:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2615:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB2616:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2616:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB2617:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2617:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB2619:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2619:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB2621:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2621:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CurrencyUnitC2ENS_14ConstChar16PtrER10UErrorCode
	.type	_ZN6icu_6712CurrencyUnitC2ENS_14ConstChar16PtrER10UErrorCode, @function
_ZN6icu_6712CurrencyUnitC2ENS_14ConstChar16PtrER10UErrorCode:
.LFB2336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711MeasureUnitC2Ev@PLT
	movl	(%rbx), %edx
	leaq	16+_ZTVN6icu_6712CurrencyUnitE(%rip), %rax
	movq	%rax, (%r12)
	testl	%edx, %edx
	jg	.L90
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L90
	cmpw	$0, (%rdi)
	je	.L90
	cmpw	$0, 2(%rdi)
	jne	.L101
.L91:
	movl	$1, (%rbx)
	.p2align 4,,10
	.p2align 3
.L90:
	movabsq	$377962889304, %rax
	movq	%rax, 20(%r12)
.L94:
	leaq	-44(%rbp), %r13
	movl	$4, %edx
	leaq	20(%r12), %rdi
	movq	%r13, %rsi
	call	u_UCharsToChars_67@PLT
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnit12initCurrencyENS_11StringPieceE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	cmpw	$0, 4(%rdi)
	je	.L91
	movl	$3, %esi
	call	uprv_isInvariantUString_67@PLT
	testb	%al, %al
	je	.L92
	xorl	%ebx, %ebx
.L93:
	movq	0(%r13), %rax
	movzwl	(%rax,%rbx), %edi
	call	u_asciiToUpper_67@PLT
	movw	%ax, 20(%r12,%rbx)
	addq	$2, %rbx
	cmpq	$6, %rbx
	jne	.L93
	xorl	%eax, %eax
	movw	%ax, 26(%r12)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$26, (%rbx)
	jmp	.L90
.L102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2336:
	.size	_ZN6icu_6712CurrencyUnitC2ENS_14ConstChar16PtrER10UErrorCode, .-_ZN6icu_6712CurrencyUnitC2ENS_14ConstChar16PtrER10UErrorCode
	.globl	_ZN6icu_6712CurrencyUnitC1ENS_14ConstChar16PtrER10UErrorCode
	.set	_ZN6icu_6712CurrencyUnitC1ENS_14ConstChar16PtrER10UErrorCode,_ZN6icu_6712CurrencyUnitC2ENS_14ConstChar16PtrER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CurrencyUnitC2ENS_11StringPieceER10UErrorCode
	.type	_ZN6icu_6712CurrencyUnitC2ENS_11StringPieceER10UErrorCode, @function
_ZN6icu_6712CurrencyUnitC2ENS_11StringPieceER10UErrorCode:
.LFB2339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711MeasureUnitC2Ev@PLT
	leaq	16+_ZTVN6icu_6712CurrencyUnitE(%rip), %rax
	movq	%rax, (%r12)
	cmpl	$3, %ebx
	je	.L112
.L104:
	movl	$1, 0(%r13)
	leaq	_ZN6icu_67L17kDefaultCurrency8E(%rip), %r13
.L106:
	movl	$4, %edx
	leaq	20(%r12), %rsi
	movq	%r13, %rdi
	call	u_charsToUChars_67@PLT
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnit12initCurrencyENS_11StringPieceE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$3, %edx
	movq	%r14, %rdi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L104
	movl	$3, %esi
	movq	%r14, %rdi
	call	uprv_isInvariantString_67@PLT
	testb	%al, %al
	je	.L114
	xorl	%ebx, %ebx
	leaq	-44(%rbp), %r13
.L107:
	movsbl	(%r14,%rbx), %edi
	call	uprv_toupper_67@PLT
	movb	%al, 0(%r13,%rbx)
	addq	$1, %rbx
	cmpq	$3, %rbx
	jne	.L107
	movb	$0, -41(%rbp)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$26, 0(%r13)
	leaq	_ZN6icu_67L17kDefaultCurrency8E(%rip), %r13
	jmp	.L106
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2339:
	.size	_ZN6icu_6712CurrencyUnitC2ENS_11StringPieceER10UErrorCode, .-_ZN6icu_6712CurrencyUnitC2ENS_11StringPieceER10UErrorCode
	.globl	_ZN6icu_6712CurrencyUnitC1ENS_11StringPieceER10UErrorCode
	.set	_ZN6icu_6712CurrencyUnitC1ENS_11StringPieceER10UErrorCode,_ZN6icu_6712CurrencyUnitC2ENS_11StringPieceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CurrencyUnitC2ERKS0_
	.type	_ZN6icu_6712CurrencyUnitC2ERKS0_, @function
_ZN6icu_6712CurrencyUnitC2ERKS0_:
.LFB2342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6711MeasureUnitC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6712CurrencyUnitE(%rip), %rax
	leaq	20(%r12), %rsi
	movq	%rax, (%rbx)
	leaq	20(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	u_strcpy_67@PLT
	.cfi_endproc
.LFE2342:
	.size	_ZN6icu_6712CurrencyUnitC2ERKS0_, .-_ZN6icu_6712CurrencyUnitC2ERKS0_
	.globl	_ZN6icu_6712CurrencyUnitC1ERKS0_
	.set	_ZN6icu_6712CurrencyUnitC1ERKS0_,_ZN6icu_6712CurrencyUnitC2ERKS0_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"currency"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CurrencyUnitC2ERKNS_11MeasureUnitER10UErrorCode
	.type	_ZN6icu_6712CurrencyUnitC2ERKNS_11MeasureUnitER10UErrorCode, @function
_ZN6icu_6712CurrencyUnitC2ERKNS_11MeasureUnitER10UErrorCode:
.LFB2345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6711MeasureUnitC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6712CurrencyUnitE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	movl	$9, %ecx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L118
	xorl	%edx, %edx
	movl	$1, (%r12)
	movw	%dx, 20(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	leaq	20(%rbx), %rsi
	movl	$4, %edx
	movq	%rax, %rdi
	call	u_charsToUChars_67@PLT
	xorl	%eax, %eax
	movw	%ax, 26(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2345:
	.size	_ZN6icu_6712CurrencyUnitC2ERKNS_11MeasureUnitER10UErrorCode, .-_ZN6icu_6712CurrencyUnitC2ERKNS_11MeasureUnitER10UErrorCode
	.globl	_ZN6icu_6712CurrencyUnitC1ERKNS_11MeasureUnitER10UErrorCode
	.set	_ZN6icu_6712CurrencyUnitC1ERKNS_11MeasureUnitER10UErrorCode,_ZN6icu_6712CurrencyUnitC2ERKNS_11MeasureUnitER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CurrencyUnitC2Ev
	.type	_ZN6icu_6712CurrencyUnitC2Ev, @function
_ZN6icu_6712CurrencyUnitC2Ev:
.LFB2348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-44(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	20(%r12), %r14
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711MeasureUnitC2Ev@PLT
	leaq	16+_ZTVN6icu_6712CurrencyUnitE(%rip), %rax
	leaq	_ZN6icu_67L16kDefaultCurrencyE(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, (%r12)
	call	u_strcpy_67@PLT
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	u_UCharsToChars_67@PLT
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnit12initCurrencyENS_11StringPieceE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L124
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L124:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2348:
	.size	_ZN6icu_6712CurrencyUnitC2Ev, .-_ZN6icu_6712CurrencyUnitC2Ev
	.globl	_ZN6icu_6712CurrencyUnitC1Ev
	.set	_ZN6icu_6712CurrencyUnitC1Ev,_ZN6icu_6712CurrencyUnitC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CurrencyUnitaSERKS0_
	.type	_ZN6icu_6712CurrencyUnitaSERKS0_, @function
_ZN6icu_6712CurrencyUnitaSERKS0_:
.LFB2350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L126
	movq	%rsi, %rbx
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	leaq	20(%rbx), %rsi
	leaq	20(%r12), %rdi
	call	u_strcpy_67@PLT
.L126:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2350:
	.size	_ZN6icu_6712CurrencyUnitaSERKS0_, .-_ZN6icu_6712CurrencyUnitaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CurrencyUnit16getStaticClassIDEv
	.type	_ZN6icu_6712CurrencyUnit16getStaticClassIDEv, @function
_ZN6icu_6712CurrencyUnit16getStaticClassIDEv:
.LFB2356:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712CurrencyUnit16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2356:
	.size	_ZN6icu_6712CurrencyUnit16getStaticClassIDEv, .-_ZN6icu_6712CurrencyUnit16getStaticClassIDEv
	.weak	_ZTSN6icu_6712CurrencyUnitE
	.section	.rodata._ZTSN6icu_6712CurrencyUnitE,"aG",@progbits,_ZTSN6icu_6712CurrencyUnitE,comdat
	.align 16
	.type	_ZTSN6icu_6712CurrencyUnitE, @object
	.size	_ZTSN6icu_6712CurrencyUnitE, 24
_ZTSN6icu_6712CurrencyUnitE:
	.string	"N6icu_6712CurrencyUnitE"
	.weak	_ZTIN6icu_6712CurrencyUnitE
	.section	.data.rel.ro._ZTIN6icu_6712CurrencyUnitE,"awG",@progbits,_ZTIN6icu_6712CurrencyUnitE,comdat
	.align 8
	.type	_ZTIN6icu_6712CurrencyUnitE, @object
	.size	_ZTIN6icu_6712CurrencyUnitE, 24
_ZTIN6icu_6712CurrencyUnitE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712CurrencyUnitE
	.quad	_ZTIN6icu_6711MeasureUnitE
	.weak	_ZTVN6icu_6712CurrencyUnitE
	.section	.data.rel.ro._ZTVN6icu_6712CurrencyUnitE,"awG",@progbits,_ZTVN6icu_6712CurrencyUnitE,comdat
	.align 8
	.type	_ZTVN6icu_6712CurrencyUnitE, @object
	.size	_ZTVN6icu_6712CurrencyUnitE, 56
_ZTVN6icu_6712CurrencyUnitE:
	.quad	0
	.quad	_ZTIN6icu_6712CurrencyUnitE
	.quad	_ZN6icu_6712CurrencyUnitD1Ev
	.quad	_ZN6icu_6712CurrencyUnitD0Ev
	.quad	_ZNK6icu_6712CurrencyUnit17getDynamicClassIDEv
	.quad	_ZNK6icu_6712CurrencyUnit5cloneEv
	.quad	_ZNK6icu_6711MeasureUniteqERKNS_7UObjectE
	.local	_ZZN6icu_6712CurrencyUnit16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6712CurrencyUnit16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.type	_ZN6icu_67L17kDefaultCurrency8E, @object
	.size	_ZN6icu_67L17kDefaultCurrency8E, 4
_ZN6icu_67L17kDefaultCurrency8E:
	.string	"XXX"
	.align 8
	.type	_ZN6icu_67L16kDefaultCurrencyE, @object
	.size	_ZN6icu_67L16kDefaultCurrencyE, 8
_ZN6icu_67L16kDefaultCurrencyE:
	.string	"X"
	.string	"X"
	.string	"X"
	.string	""
	.string	""
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
