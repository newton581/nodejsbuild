	.file	"numparse_parsednumber.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl12ParsedNumberC2Ev
	.type	_ZN6icu_678numparse4impl12ParsedNumberC2Ev, @function
_ZN6icu_678numparse4impl12ParsedNumberC2Ev:
.LFB3032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movl	$2, %ecx
	movq	%rax, 80(%rbx)
	leaq	80(%rbx), %rdi
	movw	%dx, 88(%rbx)
	movq	%rax, 144(%rbx)
	movw	%cx, 152(%rbx)
	movb	$1, 8(%rbx)
	movq	$0, 72(%rbx)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	144(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	xorl	%esi, %esi
	movw	%si, 208(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3032:
	.size	_ZN6icu_678numparse4impl12ParsedNumberC2Ev, .-_ZN6icu_678numparse4impl12ParsedNumberC2Ev
	.globl	_ZN6icu_678numparse4impl12ParsedNumberC1Ev
	.set	_ZN6icu_678numparse4impl12ParsedNumberC1Ev,_ZN6icu_678numparse4impl12ParsedNumberC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl12ParsedNumber5clearEv
	.type	_ZN6icu_678numparse4impl12ParsedNumber5clearEv, @function
_ZN6icu_678numparse4impl12ParsedNumber5clearEv:
.LFB3034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	80(%rdi), %rdi
	subq	$8, %rsp
	movb	$1, -72(%rdi)
	movq	$0, -8(%rdi)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	144(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	xorl	%eax, %eax
	movw	%ax, 208(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3034:
	.size	_ZN6icu_678numparse4impl12ParsedNumber5clearEv, .-_ZN6icu_678numparse4impl12ParsedNumber5clearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl12ParsedNumber16setCharsConsumedERKNS_13StringSegmentE
	.type	_ZN6icu_678numparse4impl12ParsedNumber16setCharsConsumedERKNS_13StringSegmentE, @function
_ZN6icu_678numparse4impl12ParsedNumber16setCharsConsumedERKNS_13StringSegmentE:
.LFB3035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	movl	%eax, 72(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3035:
	.size	_ZN6icu_678numparse4impl12ParsedNumber16setCharsConsumedERKNS_13StringSegmentE, .-_ZN6icu_678numparse4impl12ParsedNumber16setCharsConsumedERKNS_13StringSegmentE
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl12ParsedNumber11postProcessEv
	.type	_ZN6icu_678numparse4impl12ParsedNumber11postProcessEv, @function
_ZN6icu_678numparse4impl12ParsedNumber11postProcessEv:
.LFB3036:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	jne	.L8
	testb	$1, 76(%rdi)
	jne	.L13
.L8:
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	jmp	_ZN6icu_676number4impl15DecimalQuantity6negateEv@PLT
	.cfi_endproc
.LFE3036:
	.size	_ZN6icu_678numparse4impl12ParsedNumber11postProcessEv, .-_ZN6icu_678numparse4impl12ParsedNumber11postProcessEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl12ParsedNumber7successEv
	.type	_ZNK6icu_678numparse4impl12ParsedNumber7successEv, @function
_ZNK6icu_678numparse4impl12ParsedNumber7successEv:
.LFB3037:
	.cfi_startproc
	endbr64
	movl	72(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L14
	movl	76(%rdi), %eax
	shrl	$8, %eax
	xorl	$1, %eax
	andl	$1, %eax
.L14:
	ret
	.cfi_endproc
.LFE3037:
	.size	_ZNK6icu_678numparse4impl12ParsedNumber7successEv, .-_ZNK6icu_678numparse4impl12ParsedNumber7successEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl12ParsedNumber10seenNumberEv
	.type	_ZNK6icu_678numparse4impl12ParsedNumber10seenNumberEv, @function
_ZNK6icu_678numparse4impl12ParsedNumber10seenNumberEv:
.LFB3038:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	movl	$1, %eax
	je	.L17
	testb	$-64, 76(%rdi)
	setne	%al
.L17:
	ret
	.cfi_endproc
.LFE3038:
	.size	_ZNK6icu_678numparse4impl12ParsedNumber10seenNumberEv, .-_ZNK6icu_678numparse4impl12ParsedNumber10seenNumberEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl12ParsedNumber9getDoubleER10UErrorCode
	.type	_ZNK6icu_678numparse4impl12ParsedNumber9getDoubleER10UErrorCode, @function
_ZNK6icu_678numparse4impl12ParsedNumber9getDoubleER10UErrorCode:
.LFB3039:
	.cfi_startproc
	endbr64
	movl	76(%rdi), %eax
	testb	$64, %al
	jne	.L35
	testb	$-128, %al
	je	.L22
	movsd	.LC2(%rip), %xmm0
	testb	$1, %al
	je	.L36
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 8(%rdi)
	je	.L24
	movl	$27, (%rsi)
	pxor	%xmm0, %xmm0
.L20:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore 6
	.cfi_restore 12
	jmp	uprv_getNaN_67@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	movsd	.LC0(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	testb	%al, %al
	je	.L28
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity10isNegativeEv@PLT
	testb	%al, %al
	jne	.L37
.L28:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity10fitsInLongEb@PLT
	testb	%al, %al
	je	.L38
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity6toLongEb@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L38:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movsd	.LC3(%rip), %xmm0
	jmp	.L20
	.cfi_endproc
.LFE3039:
	.size	_ZNK6icu_678numparse4impl12ParsedNumber9getDoubleER10UErrorCode, .-_ZNK6icu_678numparse4impl12ParsedNumber9getDoubleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl12ParsedNumber19populateFormattableERNS_11FormattableEi
	.type	_ZNK6icu_678numparse4impl12ParsedNumber19populateFormattableERNS_11FormattableEi, @function
_ZNK6icu_678numparse4impl12ParsedNumber19populateFormattableERNS_11FormattableEi:
.LFB3040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	76(%rdi), %eax
	movl	%edx, %ebx
	movl	%eax, %edx
	andl	$128, %edx
	testb	$64, %al
	jne	.L59
	testl	%edx, %edx
	je	.L41
	movsd	.LC0(%rip), %xmm0
	testb	$1, %al
	je	.L58
	movsd	.LC2(%rip), %xmm0
.L58:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711Formattable9setDoubleEd@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	%rdi, %r12
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	testb	%al, %al
	jne	.L60
.L43:
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L44
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityC1ERKS2_@PLT
.L44:
	popq	%rbx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711Formattable20adoptDecimalQuantityEPNS_6number4impl15DecimalQuantityE@PLT
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	call	uprv_getNaN_67@PLT
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711Formattable9setDoubleEd@PLT
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity10isNegativeEv@PLT
	andl	$16, %ebx
	jne	.L43
	movsd	.LC3(%rip), %xmm0
	testb	%al, %al
	je	.L43
	jmp	.L58
	.cfi_endproc
.LFE3040:
	.size	_ZNK6icu_678numparse4impl12ParsedNumber19populateFormattableERNS_11FormattableEi, .-_ZNK6icu_678numparse4impl12ParsedNumber19populateFormattableERNS_11FormattableEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl12ParsedNumber12isBetterThanERKS2_
	.type	_ZN6icu_678numparse4impl12ParsedNumber12isBetterThanERKS2_, @function
_ZN6icu_678numparse4impl12ParsedNumber12isBetterThanERKS2_:
.LFB3041:
	.cfi_startproc
	endbr64
	movl	72(%rsi), %eax
	cmpl	%eax, 72(%rdi)
	setg	%al
	ret
	.cfi_endproc
.LFE3041:
	.size	_ZN6icu_678numparse4impl12ParsedNumber12isBetterThanERKS2_, .-_ZN6icu_678numparse4impl12ParsedNumber12isBetterThanERKS2_
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	2146435072
	.align 8
.LC2:
	.long	0
	.long	-1048576
	.align 8
.LC3:
	.long	0
	.long	-2147483648
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
