	.file	"number_decimalquantity.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity15hasIntegerValueEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity15hasIntegerValueEv, @function
_ZNK6icu_676number4impl15DecimalQuantity15hasIntegerValueEv:
.LFB3639:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	notl	%eax
	shrl	$31, %eax
	ret
	.cfi_endproc
.LFE3639:
	.size	_ZNK6icu_676number4impl15DecimalQuantity15hasIntegerValueEv, .-_ZNK6icu_676number4impl15DecimalQuantity15hasIntegerValueEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity10isInfiniteEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity10isInfiniteEv, @function
_ZNK6icu_676number4impl15DecimalQuantity10isInfiniteEv:
.LFB3647:
	.cfi_startproc
	endbr64
	movzbl	20(%rdi), %eax
	shrb	%al
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE3647:
	.size	_ZNK6icu_676number4impl15DecimalQuantity10isInfiniteEv, .-_ZNK6icu_676number4impl15DecimalQuantity10isInfiniteEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity5isNaNEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity5isNaNEv, @function
_ZNK6icu_676number4impl15DecimalQuantity5isNaNEv:
.LFB3648:
	.cfi_startproc
	endbr64
	movzbl	20(%rdi), %eax
	shrb	$2, %al
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE3648:
	.size	_ZNK6icu_676number4impl15DecimalQuantity5isNaNEv, .-_ZNK6icu_676number4impl15DecimalQuantity5isNaNEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantityD2Ev
	.type	_ZN6icu_676number4impl15DecimalQuantityD2Ev, @function
_ZN6icu_676number4impl15DecimalQuantityD2Ev:
.LFB3609:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl15DecimalQuantityE(%rip), %rax
	cmpb	$0, 64(%rdi)
	movq	%rax, (%rdi)
	jne	.L7
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	48(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3609:
	.size	_ZN6icu_676number4impl15DecimalQuantityD2Ev, .-_ZN6icu_676number4impl15DecimalQuantityD2Ev
	.globl	_ZN6icu_676number4impl15DecimalQuantityD1Ev
	.set	_ZN6icu_676number4impl15DecimalQuantityD1Ev,_ZN6icu_676number4impl15DecimalQuantityD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantityD0Ev
	.type	_ZN6icu_676number4impl15DecimalQuantityD0Ev, @function
_ZN6icu_676number4impl15DecimalQuantityD0Ev:
.LFB3611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl15DecimalQuantityE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 64(%rdi)
	movq	%rax, (%rdi)
	jne	.L11
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	48(%rdi), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3611:
	.size	_ZN6icu_676number4impl15DecimalQuantityD0Ev, .-_ZN6icu_676number4impl15DecimalQuantityD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0, @function
_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0:
.LFB4867:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 64(%rdi)
	jne	.L17
	movslq	%esi, %r13
	movq	%r13, %rdi
	call	uprv_malloc_67@PLT
	movl	%r12d, 56(%rbx)
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%rax, 48(%rbx)
	movq	%rax, %rdi
	call	memset@PLT
.L14:
	movb	$1, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	56(%rdi), %r13d
	cmpl	%esi, %r13d
	jge	.L14
	leal	(%rsi,%rsi), %ecx
	movslq	%r13d, %r15
	subl	%r13d, %r12d
	movslq	%ecx, %rdi
	movl	%ecx, -60(%rbp)
	call	uprv_malloc_67@PLT
	movq	48(%rbx), %r8
	movq	%r15, %rdx
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%r8, %rsi
	movq	%r8, -56(%rbp)
	call	memcpy@PLT
	movslq	%r12d, %rdx
	leaq	(%r14,%r15), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movl	-60(%rbp), %ecx
	movq	%r14, 48(%rbx)
	movl	%ecx, 56(%rbx)
	jmp	.L14
	.cfi_endproc
.LFE4867:
	.size	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0, .-_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB4021:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4021:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB4024:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L31
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L19
	cmpb	$0, 12(%rbx)
	jne	.L32
.L23:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L19:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L23
	.cfi_endproc
.LFE4024:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB4027:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L35
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4027:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB4030:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L38
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE4030:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB4032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L44
.L40:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L45
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4032:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB4033:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4033:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB4034:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4034:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB4035:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4035:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4036:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4036:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4037:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4037:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB4038:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L61
	testl	%edx, %edx
	jle	.L61
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L64
.L53:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L53
	.cfi_endproc
.LFE4038:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB4039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L68
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L68
	testl	%r12d, %r12d
	jg	.L75
	cmpb	$0, 12(%rbx)
	jne	.L76
.L70:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L70
.L76:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L68:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4039:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB4040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L78
	movq	(%rdi), %r8
.L79:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L82
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L82
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L82:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4040:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB4041:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L89
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4041:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB4042:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4042:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB4043:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4043:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB4044:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4044:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB4046:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4046:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB4048:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4048:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713IFixedDecimalD2Ev
	.type	_ZN6icu_6713IFixedDecimalD2Ev, @function
_ZN6icu_6713IFixedDecimalD2Ev:
.LFB3599:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3599:
	.size	_ZN6icu_6713IFixedDecimalD2Ev, .-_ZN6icu_6713IFixedDecimalD2Ev
	.globl	_ZN6icu_6713IFixedDecimalD1Ev
	.set	_ZN6icu_6713IFixedDecimalD1Ev,_ZN6icu_6713IFixedDecimalD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713IFixedDecimalD0Ev
	.type	_ZN6icu_6713IFixedDecimalD0Ev, @function
_ZN6icu_6713IFixedDecimalD0Ev:
.LFB3601:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3601:
	.size	_ZN6icu_6713IFixedDecimalD0Ev, .-_ZN6icu_6713IFixedDecimalD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantityC2Ev
	.type	_ZN6icu_676number4impl15DecimalQuantityC2Ev, @function
_ZN6icu_676number4impl15DecimalQuantityC2Ev:
.LFB3606:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl15DecimalQuantityE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movb	$0, 8(%rdi)
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movw	%ax, 64(%rdi)
	movq	$0, 48(%rdi)
	movq	$0x000000000, 24(%rdi)
	movq	$0, 12(%rdi)
	movw	%dx, 20(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE3606:
	.size	_ZN6icu_676number4impl15DecimalQuantityC2Ev, .-_ZN6icu_676number4impl15DecimalQuantityC2Ev
	.globl	_ZN6icu_676number4impl15DecimalQuantityC1Ev
	.set	_ZN6icu_676number4impl15DecimalQuantityC1Ev,_ZN6icu_676number4impl15DecimalQuantityC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantityC2ERKS2_
	.type	_ZN6icu_676number4impl15DecimalQuantityC2ERKS2_, @function
_ZN6icu_676number4impl15DecimalQuantityC2ERKS2_:
.LFB3613:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl15DecimalQuantityE(%rip), %rax
	movb	$0, 8(%rdi)
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movl	$0, 36(%rdi)
	movq	$0, 40(%rdi)
	movw	%ax, 64(%rdi)
	cmpq	%rsi, %rdi
	je	.L106
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 64(%r12)
	movq	%rdi, %rbx
	movq	$0, 48(%rdi)
	movl	16(%rsi), %esi
	movq	$0, 12(%rdi)
	movb	$0, 21(%rdi)
	movq	$0x000000000, 24(%rdi)
	movl	$0, 32(%rdi)
	je	.L100
	xorl	%edi, %edi
	testl	%esi, %esi
	je	.L101
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	16(%r12), %esi
	movq	48(%rbx), %rdi
.L101:
	movslq	%esi, %rdx
	movq	48(%r12), %rsi
	call	memcpy@PLT
	movl	16(%r12), %esi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L100:
	movq	48(%r12), %rax
	movq	%rax, 48(%rdi)
.L102:
	movzbl	8(%r12), %eax
	movsd	24(%r12), %xmm0
	movl	%esi, 16(%rbx)
	movdqu	32(%r12), %xmm1
	movb	%al, 8(%rbx)
	movl	12(%r12), %eax
	movsd	%xmm0, 24(%rbx)
	movl	%eax, 12(%rbx)
	movzwl	20(%r12), %eax
	movups	%xmm1, 32(%rbx)
	movw	%ax, 20(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE3613:
	.size	_ZN6icu_676number4impl15DecimalQuantityC2ERKS2_, .-_ZN6icu_676number4impl15DecimalQuantityC2ERKS2_
	.globl	_ZN6icu_676number4impl15DecimalQuantityC1ERKS2_
	.set	_ZN6icu_676number4impl15DecimalQuantityC1ERKS2_,_ZN6icu_676number4impl15DecimalQuantityC2ERKS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantityC2EOS2_
	.type	_ZN6icu_676number4impl15DecimalQuantityC2EOS2_, @function
_ZN6icu_676number4impl15DecimalQuantityC2EOS2_:
.LFB3617:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl15DecimalQuantityE(%rip), %rax
	movb	$0, 8(%rdi)
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movl	$0, 36(%rdi)
	movq	$0, 40(%rdi)
	movw	%ax, 64(%rdi)
	cmpq	%rsi, %rdi
	je	.L109
	cmpb	$0, 64(%rsi)
	movq	$0, 12(%rdi)
	movb	$0, 21(%rdi)
	movq	48(%rsi), %rax
	movq	$0x000000000, 24(%rdi)
	movl	$0, 32(%rdi)
	jne	.L113
	movq	%rax, 48(%rdi)
.L112:
	movzbl	8(%rsi), %eax
	movsd	24(%rsi), %xmm0
	movdqu	32(%rsi), %xmm1
	movb	%al, 8(%rdi)
	movq	12(%rsi), %rax
	movsd	%xmm0, 24(%rdi)
	movq	%rax, 12(%rdi)
	movzwl	20(%rsi), %eax
	movups	%xmm1, 32(%rdi)
	movw	%ax, 20(%rdi)
.L109:
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%rax, 48(%rdi)
	movl	56(%rsi), %eax
	movb	$1, 64(%rdi)
	movl	%eax, 56(%rdi)
	movq	$0, 48(%rsi)
	movb	$0, 64(%rsi)
	jmp	.L112
	.cfi_endproc
.LFE3617:
	.size	_ZN6icu_676number4impl15DecimalQuantityC2EOS2_, .-_ZN6icu_676number4impl15DecimalQuantityC2EOS2_
	.globl	_ZN6icu_676number4impl15DecimalQuantityC1EOS2_
	.set	_ZN6icu_676number4impl15DecimalQuantityC1EOS2_,_ZN6icu_676number4impl15DecimalQuantityC2EOS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantityaSERKS2_
	.type	_ZN6icu_676number4impl15DecimalQuantityaSERKS2_, @function
_ZN6icu_676number4impl15DecimalQuantityaSERKS2_:
.LFB3619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L115
	cmpb	$0, 64(%rdi)
	movq	%rsi, %rbx
	jne	.L123
.L116:
	movb	$0, 21(%r12)
	cmpb	$0, 64(%rbx)
	movq	$0, 48(%r12)
	movq	$0, 12(%r12)
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	movl	$0, 44(%r12)
	je	.L117
	movl	16(%rbx), %esi
	xorl	%edi, %edi
	testl	%esi, %esi
	je	.L118
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	16(%rbx), %esi
	movq	48(%r12), %rdi
.L118:
	movslq	%esi, %rdx
	movq	48(%rbx), %rsi
	call	memcpy@PLT
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L117:
	movq	48(%rbx), %rax
	movq	%rax, 48(%r12)
.L119:
	movzbl	8(%rbx), %eax
	movb	%al, 8(%r12)
	movl	36(%rbx), %eax
	movl	%eax, 36(%r12)
	movl	40(%rbx), %eax
	movl	%eax, 40(%r12)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	movl	16(%rbx), %eax
	movl	%eax, 16(%r12)
	movzbl	20(%rbx), %eax
	movb	%al, 20(%r12)
	movsd	24(%rbx), %xmm0
	movl	32(%rbx), %eax
	movsd	%xmm0, 24(%r12)
	movl	%eax, 32(%r12)
	movzbl	21(%rbx), %eax
	movb	%al, 21(%r12)
	movl	44(%rbx), %eax
	movl	%eax, 44(%r12)
.L115:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movq	48(%rdi), %rdi
	call	uprv_free_67@PLT
	movb	$0, 64(%r12)
	jmp	.L116
	.cfi_endproc
.LFE3619:
	.size	_ZN6icu_676number4impl15DecimalQuantityaSERKS2_, .-_ZN6icu_676number4impl15DecimalQuantityaSERKS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantityaSEOS2_
	.type	_ZN6icu_676number4impl15DecimalQuantityaSEOS2_, @function
_ZN6icu_676number4impl15DecimalQuantityaSEOS2_:
.LFB3620:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	cmpq	%rsi, %rdi
	je	.L130
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$24, %rsp
	cmpb	$0, 64(%rdi)
	jne	.L133
.L126:
	movb	$0, 21(%rax)
	cmpb	$0, 64(%rbx)
	movq	$0, 48(%rax)
	movq	$0, 12(%rax)
	movq	$0x000000000, 24(%rax)
	movl	$0, 32(%rax)
	movl	$0, 44(%rax)
	jne	.L134
	movq	48(%rbx), %rdx
	movq	%rdx, 48(%rax)
.L128:
	movzbl	8(%rbx), %edx
	movb	%dl, 8(%rax)
	movl	36(%rbx), %edx
	movl	%edx, 36(%rax)
	movl	40(%rbx), %edx
	movl	%edx, 40(%rax)
	movl	12(%rbx), %edx
	movl	%edx, 12(%rax)
	movl	16(%rbx), %edx
	movl	%edx, 16(%rax)
	movzbl	20(%rbx), %edx
	movb	%dl, 20(%rax)
	movsd	24(%rbx), %xmm0
	movl	32(%rbx), %edx
	movsd	%xmm0, 24(%rax)
	movl	%edx, 32(%rax)
	movzbl	21(%rbx), %edx
	movb	%dl, 21(%rax)
	movl	44(%rbx), %edx
	movl	%edx, 44(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	48(%rbx), %rdx
	movb	$1, 64(%rax)
	movq	%rdx, 48(%rax)
	movl	56(%rbx), %edx
	movl	%edx, 56(%rax)
	movq	$0, 48(%rbx)
	movb	$0, 64(%rbx)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L133:
	movq	48(%rdi), %rdi
	movq	%rax, -24(%rbp)
	call	uprv_free_67@PLT
	movq	-24(%rbp), %rax
	movb	$0, 64(%rax)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE3620:
	.size	_ZN6icu_676number4impl15DecimalQuantityaSEOS2_, .-_ZN6icu_676number4impl15DecimalQuantityaSEOS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity14copyFieldsFromERKS2_
	.type	_ZN6icu_676number4impl15DecimalQuantity14copyFieldsFromERKS2_, @function
_ZN6icu_676number4impl15DecimalQuantity14copyFieldsFromERKS2_:
.LFB3621:
	.cfi_startproc
	endbr64
	movzbl	8(%rsi), %eax
	movb	%al, 8(%rdi)
	movl	36(%rsi), %eax
	movl	%eax, 36(%rdi)
	movl	40(%rsi), %eax
	movl	%eax, 40(%rdi)
	movl	12(%rsi), %eax
	movl	%eax, 12(%rdi)
	movl	16(%rsi), %eax
	movl	%eax, 16(%rdi)
	movzbl	20(%rsi), %eax
	movb	%al, 20(%rdi)
	movsd	24(%rsi), %xmm0
	movl	32(%rsi), %eax
	movsd	%xmm0, 24(%rdi)
	movl	%eax, 32(%rdi)
	movzbl	21(%rsi), %eax
	movb	%al, 21(%rdi)
	movl	44(%rsi), %eax
	movl	%eax, 44(%rdi)
	ret
	.cfi_endproc
.LFE3621:
	.size	_ZN6icu_676number4impl15DecimalQuantity14copyFieldsFromERKS2_, .-_ZN6icu_676number4impl15DecimalQuantity14copyFieldsFromERKS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity5clearEv
	.type	_ZN6icu_676number4impl15DecimalQuantity5clearEv, @function
_ZN6icu_676number4impl15DecimalQuantity5clearEv:
.LFB3622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 64(%rdi)
	movq	$0, 36(%rdi)
	movb	$0, 20(%rdi)
	jne	.L139
.L137:
	movq	$0, 48(%rbx)
	movq	$0, 12(%rbx)
	movb	$0, 21(%rbx)
	movq	$0x000000000, 24(%rbx)
	movl	$0, 32(%rbx)
	movl	$0, 44(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	48(%rdi), %rdi
	call	uprv_free_67@PLT
	movb	$0, 64(%rbx)
	jmp	.L137
	.cfi_endproc
.LFE3622:
	.size	_ZN6icu_676number4impl15DecimalQuantity5clearEv, .-_ZN6icu_676number4impl15DecimalQuantity5clearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity13setMinIntegerEi
	.type	_ZN6icu_676number4impl15DecimalQuantity13setMinIntegerEi, @function
_ZN6icu_676number4impl15DecimalQuantity13setMinIntegerEi:
.LFB3623:
	.cfi_startproc
	endbr64
	cmpl	%esi, 36(%rdi)
	cmovge	36(%rdi), %esi
	movl	%esi, 36(%rdi)
	ret
	.cfi_endproc
.LFE3623:
	.size	_ZN6icu_676number4impl15DecimalQuantity13setMinIntegerEi, .-_ZN6icu_676number4impl15DecimalQuantity13setMinIntegerEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity14setMinFractionEi
	.type	_ZN6icu_676number4impl15DecimalQuantity14setMinFractionEi, @function
_ZN6icu_676number4impl15DecimalQuantity14setMinFractionEi:
.LFB3624:
	.cfi_startproc
	endbr64
	negl	%esi
	movl	%esi, 40(%rdi)
	ret
	.cfi_endproc
.LFE3624:
	.size	_ZN6icu_676number4impl15DecimalQuantity14setMinFractionEi, .-_ZN6icu_676number4impl15DecimalQuantity14setMinFractionEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity22getPositionFingerprintEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity22getPositionFingerprintEv, @function
_ZNK6icu_676number4impl15DecimalQuantity22getPositionFingerprintEv:
.LFB3626:
	.cfi_startproc
	endbr64
	movl	36(%rdi), %eax
	movslq	40(%rdi), %rdx
	sall	$16, %eax
	salq	$32, %rdx
	cltq
	xorq	%rdx, %rax
	ret
	.cfi_endproc
.LFE3626:
	.size	_ZNK6icu_676number4impl15DecimalQuantity22getPositionFingerprintEv, .-_ZNK6icu_676number4impl15DecimalQuantity22getPositionFingerprintEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity6negateEv
	.type	_ZN6icu_676number4impl15DecimalQuantity6negateEv, @function
_ZN6icu_676number4impl15DecimalQuantity6negateEv:
.LFB3633:
	.cfi_startproc
	endbr64
	xorb	$1, 20(%rdi)
	ret
	.cfi_endproc
.LFE3633:
	.size	_ZN6icu_676number4impl15DecimalQuantity6negateEv, .-_ZN6icu_676number4impl15DecimalQuantity6negateEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv, @function
_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv:
.LFB3634:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	addl	16(%rdi), %eax
	subl	$1, %eax
	ret
	.cfi_endproc
.LFE3634:
	.size	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv, .-_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi
	.type	_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi, @function
_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi:
.LFB3635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	16(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L152
.L145:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L153
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movq	%rdi, %rbx
	leaq	12(%rdi), %rdx
	movl	12(%rdi), %edi
	movl	%esi, -36(%rbp)
	call	uprv_add32_overflow_67@PLT
	movl	32(%rbx), %edi
	movl	-36(%rbp), %esi
	leaq	32(%rbx), %rdx
	movl	%eax, %r12d
	call	uprv_add32_overflow_67@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	orb	%r8b, %r12b
	jne	.L145
	movl	16(%rbx), %esi
	movl	12(%rbx), %edi
	leaq	-28(%rbp), %rdx
	call	uprv_add32_overflow_67@PLT
	testb	%al, %al
	setne	%al
	jmp	.L145
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3635:
	.size	_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi, .-_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity11getExponentEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity11getExponentEv, @function
_ZNK6icu_676number4impl15DecimalQuantity11getExponentEv:
.LFB3637:
	.cfi_startproc
	endbr64
	movl	44(%rdi), %eax
	ret
	.cfi_endproc
.LFE3637:
	.size	_ZNK6icu_676number4impl15DecimalQuantity11getExponentEv, .-_ZNK6icu_676number4impl15DecimalQuantity11getExponentEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity14adjustExponentEi
	.type	_ZN6icu_676number4impl15DecimalQuantity14adjustExponentEi, @function
_ZN6icu_676number4impl15DecimalQuantity14adjustExponentEi:
.LFB3638:
	.cfi_startproc
	endbr64
	addl	%esi, 44(%rdi)
	ret
	.cfi_endproc
.LFE3638:
	.size	_ZN6icu_676number4impl15DecimalQuantity14adjustExponentEi, .-_ZN6icu_676number4impl15DecimalQuantity14adjustExponentEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity24getUpperDisplayMagnitudeEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity24getUpperDisplayMagnitudeEv, @function
_ZNK6icu_676number4impl15DecimalQuantity24getUpperDisplayMagnitudeEv:
.LFB3640:
	.cfi_startproc
	endbr64
	movl	36(%rdi), %edx
	movl	16(%rdi), %eax
	addl	12(%rdi), %eax
	cmpl	%edx, %eax
	cmovl	%edx, %eax
	subl	$1, %eax
	ret
	.cfi_endproc
.LFE3640:
	.size	_ZNK6icu_676number4impl15DecimalQuantity24getUpperDisplayMagnitudeEv, .-_ZNK6icu_676number4impl15DecimalQuantity24getUpperDisplayMagnitudeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity24getLowerDisplayMagnitudeEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity24getLowerDisplayMagnitudeEv, @function
_ZNK6icu_676number4impl15DecimalQuantity24getLowerDisplayMagnitudeEv:
.LFB3641:
	.cfi_startproc
	endbr64
	movl	40(%rdi), %eax
	cmpl	%eax, 12(%rdi)
	cmovle	12(%rdi), %eax
	ret
	.cfi_endproc
.LFE3641:
	.size	_ZNK6icu_676number4impl15DecimalQuantity24getLowerDisplayMagnitudeEv, .-_ZNK6icu_676number4impl15DecimalQuantity24getLowerDisplayMagnitudeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity8getDigitEi
	.type	_ZNK6icu_676number4impl15DecimalQuantity8getDigitEi, @function
_ZNK6icu_676number4impl15DecimalQuantity8getDigitEi:
.LFB3642:
	.cfi_startproc
	endbr64
	subl	12(%rdi), %esi
	xorl	%eax, %eax
	cmpb	$0, 64(%rdi)
	je	.L159
	testl	%esi, %esi
	js	.L158
	cmpl	16(%rdi), %esi
	jl	.L164
.L158:
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	cmpl	$15, %esi
	ja	.L158
	movq	48(%rdi), %rax
	leal	0(,%rsi,4), %ecx
	shrq	%cl, %rax
	andl	$15, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	movq	48(%rdi), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %eax
	ret
	.cfi_endproc
.LFE3642:
	.size	_ZNK6icu_676number4impl15DecimalQuantity8getDigitEi, .-_ZNK6icu_676number4impl15DecimalQuantity8getDigitEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity13fractionCountEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity13fractionCountEv, @function
_ZNK6icu_676number4impl15DecimalQuantity13fractionCountEv:
.LFB3643:
	.cfi_startproc
	endbr64
	movl	40(%rdi), %eax
	cmpl	%eax, 12(%rdi)
	movl	$0, %edx
	cmovle	12(%rdi), %eax
	negl	%eax
	subl	44(%rdi), %eax
	cmovs	%edx, %eax
	ret
	.cfi_endproc
.LFE3643:
	.size	_ZNK6icu_676number4impl15DecimalQuantity13fractionCountEv, .-_ZNK6icu_676number4impl15DecimalQuantity13fractionCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity33fractionCountWithoutTrailingZerosEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity33fractionCountWithoutTrailingZerosEv, @function
_ZNK6icu_676number4impl15DecimalQuantity33fractionCountWithoutTrailingZerosEv:
.LFB3644:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	movl	$0, %edx
	negl	%eax
	subl	44(%rdi), %eax
	cmovs	%edx, %eax
	ret
	.cfi_endproc
.LFE3644:
	.size	_ZNK6icu_676number4impl15DecimalQuantity33fractionCountWithoutTrailingZerosEv, .-_ZNK6icu_676number4impl15DecimalQuantity33fractionCountWithoutTrailingZerosEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity10isNegativeEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity10isNegativeEv, @function
_ZNK6icu_676number4impl15DecimalQuantity10isNegativeEv:
.LFB3645:
	.cfi_startproc
	endbr64
	movzbl	20(%rdi), %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE3645:
	.size	_ZNK6icu_676number4impl15DecimalQuantity10isNegativeEv, .-_ZNK6icu_676number4impl15DecimalQuantity10isNegativeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity6signumEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity6signumEv, @function
_ZNK6icu_676number4impl15DecimalQuantity6signumEv:
.LFB3646:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	testl	%eax, %eax
	je	.L169
	movzbl	20(%rdi), %edx
	andl	$1, %edx
	cmpb	$1, %dl
	sbbl	%eax, %eax
	andl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	movq	(%rdi), %rax
	leaq	_ZNK6icu_676number4impl15DecimalQuantity10isInfiniteEv(%rip), %rdx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L188
	movzbl	20(%rdi), %edx
	movl	%edx, %eax
	shrb	%al
	andl	$1, %eax
	testb	%al, %al
	jne	.L189
	xorl	%eax, %eax
	andl	$1, %edx
	sete	%al
	addl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	call	*%rax
	movq	-8(%rbp), %rdi
	movzbl	20(%rdi), %edx
	testb	%al, %al
	jne	.L190
	leave
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	andl	$1, %edx
	sete	%al
	addl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore 6
	andl	$1, %edx
	cmpb	$1, %dl
	sbbl	%eax, %eax
	andl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	leave
	.cfi_def_cfa 7, 8
	andl	$1, %edx
	cmpb	$1, %dl
	sbbl	%eax, %eax
	andl	$3, %eax
	ret
	.cfi_endproc
.LFE3646:
	.size	_ZNK6icu_676number4impl15DecimalQuantity6signumEv, .-_ZNK6icu_676number4impl15DecimalQuantity6signumEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv, @function
_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv:
.LFB3649:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE3649:
	.size	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv, .-_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity9_setToIntEi
	.type	_ZN6icu_676number4impl15DecimalQuantity9_setToIntEi, @function
_ZN6icu_676number4impl15DecimalQuantity9_setToIntEi:
.LFB3651:
	.cfi_startproc
	endbr64
	cmpl	$-2147483648, %esi
	je	.L199
	testl	%esi, %esi
	je	.L197
	movl	$16, %r9d
	xorl	%eax, %eax
	movabsq	$-3689348814741910323, %r10
	.p2align 4,,10
	.p2align 3
.L196:
	movslq	%esi, %rcx
	shrq	$4, %rax
	subl	$1, %r9d
	movq	%rax, %r8
	movq	%rcx, %rax
	sarl	$31, %esi
	mulq	%r10
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	movq	%rcx, %rdx
	addq	%rax, %rax
	imulq	$1717986919, %rcx, %rcx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$60, %rax
	sarq	$34, %rcx
	addq	%r8, %rax
	subl	%esi, %ecx
	movl	%ecx, %esi
	jne	.L196
	leal	0(,%r9,4), %ecx
	movl	$16, %esi
	shrq	%cl, %rax
	subl	%r9d, %esi
.L195:
	movq	%rax, 48(%rdi)
	movl	$0, 12(%rdi)
	movl	%esi, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	movabsq	$142929835592, %rax
	movq	%rax, 48(%rdi)
	movabsq	$42949672960, %rax
	movq	%rax, 12(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	xorl	%eax, %eax
	jmp	.L195
	.cfi_endproc
.LFE3651:
	.size	_ZN6icu_676number4impl15DecimalQuantity9_setToIntEi, .-_ZN6icu_676number4impl15DecimalQuantity9_setToIntEi
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"9.223372036854775808E+18"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity10_setToLongEl
	.type	_ZN6icu_676number4impl15DecimalQuantity10_setToLongEl, @function
_ZN6icu_676number4impl15DecimalQuantity10_setToLongEl:
.LFB3653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movabsq	$-9223372036854775808, %rax
	cmpq	%rax, %rsi
	je	.L275
	movq	%rsi, %rbx
	cmpq	$2147483647, %rsi
	jg	.L212
	movl	%esi, %edi
	testl	%esi, %esi
	je	.L220
	movl	$16, %r8d
	xorl	%eax, %eax
	movabsq	$-3689348814741910323, %r9
	.p2align 4,,10
	.p2align 3
.L214:
	movslq	%edi, %rcx
	shrq	$4, %rax
	subl	$1, %r8d
	movq	%rax, %rsi
	movq	%rcx, %rax
	movq	%rcx, %rbx
	sarl	$31, %edi
	mulq	%r9
	imulq	$1717986919, %rcx, %rcx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	sarq	$34, %rcx
	addq	%rax, %rax
	subq	%rax, %rbx
	movq	%rbx, %rax
	salq	$60, %rax
	addq	%rsi, %rax
	subl	%edi, %ecx
	movl	%ecx, %edi
	jne	.L214
	leal	0(,%r8,4), %ecx
	movl	$16, %edi
	shrq	%cl, %rax
	subl	%r8d, %edi
.L213:
	movq	%rax, 48(%r12)
	movl	$0, 12(%r12)
	movl	%edi, 16(%r12)
.L200:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movabsq	$9999999999999999, %rax
	movl	$16, %edi
	xorl	%esi, %esi
	movabsq	$7378697629483820647, %rcx
	cmpq	%rax, %rbx
	jg	.L277
	.p2align 4,,10
	.p2align 3
.L215:
	movq	%rbx, %rax
	shrq	$4, %rsi
	subl	$1, %edi
	imulq	%rcx
	movq	%rbx, %rax
	sarq	$63, %rax
	sarq	$2, %rdx
	subq	%rax, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rbx
	salq	$60, %rbx
	addq	%rbx, %rsi
	movq	%rdx, %rbx
	testq	%rdx, %rdx
	jne	.L215
	leal	0(,%rdi,4), %ecx
	movl	$16, %eax
	movl	$0, 12(%r12)
	shrq	%cl, %rsi
	subl	%edi, %eax
	movq	%rsi, 48(%r12)
	movl	%eax, 16(%r12)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L275:
	leaq	-144(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl6DecNumC1Ev@PLT
	leaq	-148(%rbp), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movl	$0, -148(%rbp)
	call	_ZN6icu_676number4impl6DecNum5setToEPKcR10UErrorCode@PLT
	movl	-148(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L202
	cmpb	$0, -132(%rbp)
	je	.L200
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L277:
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	xorl	%ecx, %ecx
	movabsq	$7378697629483820647, %rsi
	.p2align 4,,10
	.p2align 3
.L216:
	movq	%rbx, %rax
	movq	48(%r12), %rdi
	imulq	%rsi
	movq	%rbx, %rax
	sarq	$63, %rax
	sarq	$2, %rdx
	subq	%rax, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rbx
	leal	1(%rcx), %eax
	movb	%bl, (%rdi,%rcx)
	movq	%rdx, %rbx
	addq	$1, %rcx
	testq	%rdx, %rdx
	jne	.L216
	movl	$0, 12(%r12)
	movl	%eax, 16(%r12)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L202:
	movq	-144(%rbp), %rbx
	orb	$1, 20(%r12)
	movl	(%rbx), %esi
	cmpl	$16, %esi
	jg	.L278
	testl	%esi, %esi
	jle	.L219
	movzbl	9(%rbx), %eax
	cmpl	$1, %esi
	je	.L209
	movzbl	10(%rbx), %edx
	salq	$4, %rdx
	orq	%rdx, %rax
	cmpl	$2, %esi
	je	.L209
	movzbl	11(%rbx), %edx
	salq	$8, %rdx
	orq	%rdx, %rax
	cmpl	$3, %esi
	je	.L209
	movzbl	12(%rbx), %edx
	salq	$12, %rdx
	orq	%rdx, %rax
	cmpl	$4, %esi
	je	.L209
	movzbl	13(%rbx), %edx
	salq	$16, %rdx
	orq	%rdx, %rax
	cmpl	$5, %esi
	je	.L209
	movzbl	14(%rbx), %edx
	salq	$20, %rdx
	orq	%rdx, %rax
	cmpl	$6, %esi
	je	.L209
	movzbl	15(%rbx), %edx
	salq	$24, %rdx
	orq	%rdx, %rax
	cmpl	$7, %esi
	je	.L209
	movzbl	16(%rbx), %edx
	salq	$28, %rdx
	orq	%rdx, %rax
	cmpl	$8, %esi
	je	.L209
	movzbl	17(%rbx), %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	cmpl	$9, %esi
	je	.L209
	movzbl	18(%rbx), %edx
	salq	$36, %rdx
	orq	%rdx, %rax
	cmpl	$10, %esi
	je	.L209
	movzbl	19(%rbx), %edx
	salq	$40, %rdx
	orq	%rdx, %rax
	cmpl	$11, %esi
	je	.L209
	movzbl	20(%rbx), %edx
	salq	$44, %rdx
	orq	%rdx, %rax
	cmpl	$12, %esi
	je	.L209
	movzbl	21(%rbx), %edx
	salq	$48, %rdx
	orq	%rdx, %rax
	cmpl	$13, %esi
	je	.L209
	movzbl	22(%rbx), %edx
	salq	$52, %rdx
	orq	%rdx, %rax
	cmpl	$14, %esi
	je	.L209
	movzbl	23(%rbx), %edx
	salq	$56, %rdx
	orq	%rdx, %rax
	cmpl	$15, %esi
	je	.L209
	movzbl	24(%rbx), %edx
	salq	$60, %rdx
	orq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L209:
	movq	%rax, 48(%r12)
	movq	%rbx, %rdi
.L208:
	movl	4(%rbx), %eax
	cmpb	$0, -132(%rbp)
	movl	%eax, 12(%r12)
	movl	(%rbx), %eax
	movl	%eax, 16(%r12)
	je	.L200
	call	uprv_free_67@PLT
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L220:
	xorl	%eax, %eax
	jmp	.L213
.L219:
	xorl	%eax, %eax
	jmp	.L209
.L278:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L274
	.p2align 4,,10
	.p2align 3
.L207:
	movzbl	9(%rbx,%rax), %ecx
	movq	48(%r12), %rdx
	movb	%cl, (%rdx,%rax)
	addq	$1, %rax
	cmpl	%eax, (%rbx)
	jg	.L207
.L274:
	movq	-144(%rbp), %rdi
	jmp	.L208
.L276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3653:
	.size	_ZN6icu_676number4impl15DecimalQuantity10_setToLongEl, .-_ZN6icu_676number4impl15DecimalQuantity10_setToLongEl
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity23convertToAccurateDoubleEv
	.type	_ZN6icu_676number4impl15DecimalQuantity23convertToAccurateDoubleEv, @function
_ZN6icu_676number4impl15DecimalQuantity23convertToAccurateDoubleEv:
.LFB3656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$18, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	leaq	-88(%rbp), %r9
	pushq	%r13
	movq	%r15, %rdx
	leaq	-89(%rbp), %r8
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$64, %rsp
	movl	32(%rdi), %r14d
	movsd	24(%rdi), %xmm0
	xorl	%edi, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-84(%rbp), %rax
	pushq	%rax
	call	_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_@PLT
	cmpb	$0, 64(%rbx)
	popq	%rax
	popq	%rdx
	jne	.L334
.L280:
	movl	-88(%rbp), %r12d
	movq	$0, 48(%rbx)
	movq	$0, 12(%rbx)
	movl	-84(%rbp), %r13d
	movb	$0, 21(%rbx)
	movq	$0x000000000, 24(%rbx)
	movl	$0, 32(%rbx)
	movl	$0, 44(%rbx)
	cmpl	$16, %r12d
	jg	.L335
	testl	%r12d, %r12d
	jle	.L287
	movslq	%r12d, %rax
	movsbl	-81(%rbp,%rax), %eax
	subl	$48, %eax
	cltq
	cmpl	$1, %r12d
	je	.L284
	leal	-1(%r12), %edx
	movslq	%edx, %rdx
	movsbl	-81(%rbp,%rdx), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	salq	$4, %rdx
	orq	%rdx, %rax
	cmpl	$2, %r12d
	je	.L284
	leal	-2(%r12), %edx
	movslq	%edx, %rdx
	movsbl	-81(%rbp,%rdx), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	salq	$8, %rdx
	orq	%rdx, %rax
	cmpl	$3, %r12d
	je	.L284
	leal	-3(%r12), %edx
	movslq	%edx, %rdx
	movsbl	-81(%rbp,%rdx), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	salq	$12, %rdx
	orq	%rdx, %rax
	cmpl	$4, %r12d
	je	.L284
	leal	-4(%r12), %edx
	movslq	%edx, %rdx
	movsbl	-81(%rbp,%rdx), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	orq	%rdx, %rax
	cmpl	$5, %r12d
	je	.L284
	leal	-5(%r12), %edx
	movslq	%edx, %rdx
	movsbl	-81(%rbp,%rdx), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	salq	$20, %rdx
	orq	%rdx, %rax
	cmpl	$6, %r12d
	je	.L284
	leal	-6(%r12), %edx
	movslq	%edx, %rdx
	movsbl	-81(%rbp,%rdx), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	salq	$24, %rdx
	orq	%rdx, %rax
	cmpl	$7, %r12d
	je	.L284
	leal	-7(%r12), %edx
	movslq	%edx, %rdx
	movsbl	-81(%rbp,%rdx), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	salq	$28, %rdx
	orq	%rdx, %rax
	cmpl	$8, %r12d
	je	.L284
	leal	-8(%r12), %edx
	movslq	%edx, %rdx
	movsbl	-81(%rbp,%rdx), %edx
	subl	$48, %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	cmpl	$9, %r12d
	je	.L284
	leal	-9(%r12), %edx
	movslq	%edx, %rdx
	movsbl	-81(%rbp,%rdx), %edx
	subl	$48, %edx
	salq	$36, %rdx
	orq	%rdx, %rax
	cmpl	$10, %r12d
	je	.L284
	leal	-10(%r12), %edx
	movslq	%edx, %rdx
	movsbl	-81(%rbp,%rdx), %edx
	subl	$48, %edx
	salq	$40, %rdx
	orq	%rdx, %rax
	cmpl	$11, %r12d
	je	.L284
	leal	-11(%r12), %edx
	movslq	%edx, %rdx
	movsbl	-81(%rbp,%rdx), %edx
	subl	$48, %edx
	salq	$44, %rdx
	orq	%rdx, %rax
	cmpl	$12, %r12d
	je	.L284
	leal	-12(%r12), %edx
	movslq	%edx, %rdx
	movsbl	-81(%rbp,%rdx), %edx
	subl	$48, %edx
	salq	$48, %rdx
	orq	%rdx, %rax
	cmpl	$13, %r12d
	je	.L284
	leal	-13(%r12), %edx
	movslq	%edx, %rdx
	movsbl	-81(%rbp,%rdx), %edx
	subl	$48, %edx
	salq	$52, %rdx
	orq	%rdx, %rax
	cmpl	$14, %r12d
	je	.L284
	leal	-14(%r12), %edx
	movslq	%edx, %rdx
	movsbl	-81(%rbp,%rdx), %edx
	subl	$48, %edx
	salq	$56, %rdx
	orq	%rdx, %rax
	cmpl	$15, %r12d
	je	.L284
	movsbl	-80(%rbp), %edx
	salq	$60, %rdx
	orq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L284:
	movq	%rax, 48(%rbx)
.L282:
	subl	%r12d, %r13d
	movl	%r12d, 16(%rbx)
	addl	%r14d, %r13d
	movb	$1, 65(%rbx)
	movl	%r13d, 12(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L336
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movslq	%r12d, %rdx
	movq	48(%rbx), %rdi
	addq	%rdx, %r15
	leaq	-81(%rbp,%rdx), %rsi
	leal	-1(%r12), %edx
	movq	%r15, %rax
	subq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L283:
	movzbl	-1(%rax), %edx
	movq	%r15, %rcx
	subq	%rax, %rcx
	subq	$1, %rax
	subl	$48, %edx
	movb	%dl, (%rdi,%rcx)
	cmpq	%rsi, %rax
	jne	.L283
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L334:
	movq	48(%rbx), %rdi
	call	uprv_free_67@PLT
	movb	$0, 64(%rbx)
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L287:
	xorl	%eax, %eax
	jmp	.L284
.L336:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3656:
	.size	_ZN6icu_676number4impl15DecimalQuantity23convertToAccurateDoubleEv, .-_ZN6icu_676number4impl15DecimalQuantity23convertToAccurateDoubleEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity16_setToDoubleFastEd
	.type	_ZN6icu_676number4impl15DecimalQuantity16_setToDoubleFastEd, @function
_ZN6icu_676number4impl15DecimalQuantity16_setToDoubleFastEd:
.LFB3655:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrq	$52, %rdx
	andl	$2047, %edx
	subl	$1023, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movb	$1, 21(%rdi)
	movl	$0, 32(%rdi)
	movsd	%xmm0, 24(%rdi)
	cmpl	$52, %edx
	jg	.L338
	cvttsd2siq	%xmm0, %rsi
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rsi, %xmm1
	ucomisd	%xmm0, %xmm1
	jnp	.L359
.L338:
	cmpl	$-1023, %edx
	je	.L352
	cmpl	$1024, %edx
	je	.L352
	movl	$52, %eax
	pxor	%xmm1, %xmm1
	subl	%edx, %eax
	cvtsi2sdl	%eax, %xmm1
	divsd	.LC2(%rip), %xmm1
	cvttsd2sil	%xmm1, %ebx
	movl	%ebx, %eax
	testl	%ebx, %ebx
	js	.L342
	cmpl	$21, %ebx
	jle	.L343
	movsd	.LC3(%rip), %xmm1
	.p2align 4,,10
	.p2align 3
.L344:
	mulsd	%xmm1, %xmm0
	subl	$22, %eax
	cmpl	$21, %eax
	jg	.L344
.L343:
	cltq
	leaq	_ZN12_GLOBAL__N_1L18DOUBLE_MULTIPLIERSE(%rip), %rdx
	mulsd	(%rdx,%rax,8), %xmm0
	call	uprv_round_67@PLT
	cvttsd2siq	%xmm0, %rsi
	testq	%rsi, %rsi
	jne	.L360
.L337:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	cmpl	$-21, %ebx
	jge	.L346
	movsd	.LC3(%rip), %xmm1
	.p2align 4,,10
	.p2align 3
.L347:
	divsd	%xmm1, %xmm0
	addl	$22, %eax
	cmpl	$-21, %eax
	jl	.L347
.L346:
	negl	%eax
	leaq	_ZN12_GLOBAL__N_1L18DOUBLE_MULTIPLIERSE(%rip), %rdx
	cltq
	divsd	(%rdx,%rax,8), %xmm0
	call	uprv_round_67@PLT
	cvttsd2siq	%xmm0, %rsi
	testq	%rsi, %rsi
	je	.L337
.L360:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity10_setToLongEl
	subl	%ebx, 12(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	jne	.L338
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity10_setToLongEl
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity23convertToAccurateDoubleEv
	.cfi_endproc
.LFE3655:
	.size	_ZN6icu_676number4impl15DecimalQuantity16_setToDoubleFastEd, .-_ZN6icu_676number4impl15DecimalQuantity16_setToDoubleFastEd
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity6toLongEb
	.type	_ZNK6icu_676number4impl15DecimalQuantity6toLongEb, @function
_ZNK6icu_676number4impl15DecimalQuantity6toLongEb:
.LFB3660:
	.cfi_startproc
	endbr64
	movslq	44(%rdi), %r10
	movslq	12(%rdi), %r8
	movl	%esi, %r11d
	movl	16(%rdi), %r9d
	leal	(%r10,%r8), %esi
	leal	-1(%r9,%rsi), %edx
	testb	%r11b, %r11b
	je	.L362
	cmpl	$17, %edx
	movl	$17, %eax
	cmovg	%eax, %edx
.L362:
	testl	%edx, %edx
	js	.L371
	cmpb	$0, 64(%rdi)
	je	.L373
	movl	%edx, %ecx
	movslq	%edx, %rdx
	xorl	%esi, %esi
	xorl	%eax, %eax
	subl	%r8d, %ecx
	movq	%rdx, %r11
	addq	%r10, %r8
	subl	%r10d, %ecx
	subq	%r8, %r11
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L372:
	movq	%r8, %rsi
.L368:
	leaq	(%rax,%rax,4), %rax
	addq	%rax, %rax
	testl	%ecx, %ecx
	js	.L367
	cmpl	%ecx, %r9d
	jle	.L367
	movq	48(%rdi), %r10
	movq	%r11, %r8
	subq	%rsi, %r8
	movsbq	(%r10,%r8), %r8
	addq	%r8, %rax
.L367:
	leaq	1(%rsi), %r8
	subl	$1, %ecx
	cmpq	%rdx, %rsi
	jne	.L372
	movq	%rax, %rdx
	negq	%rdx
	testb	$1, 20(%rdi)
	cmovne	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	subl	%r8d, %edx
	notl	%esi
	xorl	%eax, %eax
	subl	%r10d, %edx
	.p2align 4,,10
	.p2align 3
.L366:
	leaq	(%rax,%rax,4), %rax
	addq	%rax, %rax
	cmpl	$15, %edx
	ja	.L365
	movq	48(%rdi), %r11
	leal	0(,%rdx,4), %ecx
	shrq	%cl, %r11
	movq	%r11, %rcx
	andl	$15, %ecx
	addq	%rcx, %rax
.L365:
	subl	$1, %edx
	cmpl	%esi, %edx
	jne	.L366
	movq	%rax, %rdx
	negq	%rdx
	testb	$1, 20(%rdi)
	cmovne	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	xorl	%eax, %eax
	movq	%rax, %rdx
	negq	%rdx
	testb	$1, 20(%rdi)
	cmovne	%rdx, %rax
	ret
	.cfi_endproc
.LFE3660:
	.size	_ZNK6icu_676number4impl15DecimalQuantity6toLongEb, .-_ZNK6icu_676number4impl15DecimalQuantity6toLongEb
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity14toFractionLongEb
	.type	_ZNK6icu_676number4impl15DecimalQuantity14toFractionLongEb, @function
_ZNK6icu_676number4impl15DecimalQuantity14toFractionLongEb:
.LFB3662:
	.cfi_startproc
	endbr64
	movl	44(%rdi), %eax
	movl	12(%rdi), %edx
	notl	%eax
	testb	%sil, %sil
	je	.L375
	cmpl	%edx, 40(%rdi)
	movl	%edx, %r9d
	cmovle	40(%rdi), %r9d
	cmpl	%r9d, %eax
	jl	.L388
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	cmpb	$0, 64(%rdi)
	.cfi_offset 3, -24
	je	.L398
.L377:
	movl	%eax, %ecx
	movslq	%edx, %r8
	cltq
	movabsq	$1000000000000000000, %rbx
	subq	%r8, %rax
	subl	%edx, %ecx
	leal	-1(%rdx), %r10d
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L383:
	leaq	(%r8,%r8,4), %r8
	addq	%r8, %r8
	testl	%ecx, %ecx
	js	.L382
	cmpl	%ecx, 16(%rdi)
	jle	.L382
	movq	48(%rdi), %rdx
	movsbq	(%rdx,%rax), %rdx
	addq	%rdx, %r8
.L382:
	leal	(%r10,%rcx), %edx
	cmpl	%r9d, %edx
	setl	%r11b
	cmpq	%rbx, %r8
	seta	%dl
	subl	$1, %ecx
	subq	$1, %rax
	orb	%dl, %r11b
	je	.L383
.L379:
	testb	%sil, %sil
	jne	.L374
	testq	%r8, %r8
	je	.L374
	movabsq	$-3689348814741910323, %rsi
	movq	%r8, %rax
	movabsq	$1844674407370955161, %rdi
	mulq	%rsi
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	cmpq	%rax, %r8
	je	.L384
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L385:
	movq	%r8, %rax
	imulq	%rsi, %rax
	rorq	%rax
	cmpq	%rdi, %rax
	ja	.L374
.L384:
	movq	%r8, %rax
	movq	%r8, %rcx
	mulq	%rsi
	shrq	$3, %rdx
	movq	%rdx, %r8
	cmpq	$9, %rcx
	ja	.L385
	xorl	%r8d, %r8d
.L374:
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore 3
	.cfi_restore 6
	cmpl	%eax, %edx
	jg	.L388
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	cmpb	$0, 64(%rdi)
	jne	.L377
.L398:
	subl	%edx, %eax
	xorl	%r8d, %r8d
	leal	-1(%rdx), %r10d
	movabsq	$1000000000000000000, %r11
	.p2align 4,,10
	.p2align 3
.L381:
	leaq	(%r8,%r8,4), %r8
	addq	%r8, %r8
	cmpl	$15, %eax
	ja	.L378
	movq	48(%rdi), %rdx
	leal	0(,%rax,4), %ecx
	shrq	%cl, %rdx
	andl	$15, %edx
	addq	%rdx, %r8
.L378:
	leal	(%r10,%rax), %edx
	cmpl	%r9d, %edx
	setl	%cl
	cmpq	%r11, %r8
	seta	%dl
	subl	$1, %eax
	orb	%dl, %cl
	je	.L381
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE3662:
	.size	_ZNK6icu_676number4impl15DecimalQuantity14toFractionLongEb, .-_ZNK6icu_676number4impl15DecimalQuantity14toFractionLongEb
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity10fitsInLongEb
	.type	_ZNK6icu_676number4impl15DecimalQuantity10fitsInLongEb, @function
_ZNK6icu_676number4impl15DecimalQuantity10fitsInLongEb:
.LFB3663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_676number4impl15DecimalQuantity10isInfiniteEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L400
	movzbl	20(%rdi), %eax
	shrb	%al
	andl	$1, %eax
.L401:
	testb	%al, %al
	jne	.L406
	movq	(%rbx), %rax
	leaq	_ZNK6icu_676number4impl15DecimalQuantity5isNaNEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L404
	movzbl	20(%rbx), %eax
	shrb	$2, %al
	andl	$1, %eax
.L405:
	testb	%al, %al
	jne	.L406
	movl	16(%rbx), %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	je	.L399
	movslq	12(%rbx), %rsi
	movl	44(%rbx), %eax
	addl	%esi, %eax
	notl	%eax
	shrl	$31, %eax
	orb	%r12b, %al
	je	.L406
	leal	-1(%rsi,%rcx), %edx
	cmpl	$17, %edx
	jle	.L399
	cmpl	$18, %edx
	jne	.L406
	testl	%ecx, %ecx
	jle	.L407
	movzbl	64(%rbx), %r9d
	testb	%r9b, %r9b
	je	.L423
	movl	%edx, %eax
	leaq	1+_ZZNK6icu_676number4impl15DecimalQuantity10fitsInLongEbE9INT64_BCD(%rip), %rdi
	leal	-1(%rcx), %edx
	addq	%rdx, %rdi
	leaq	_ZZNK6icu_676number4impl15DecimalQuantity10fitsInLongEbE9INT64_BCD(%rip), %rdx
	subl	%esi, %eax
	leaq	18(%rdx), %r8
	subq	%rsi, %r8
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L424:
	testl	%eax, %eax
	js	.L415
	movq	48(%rbx), %r10
	movq	%r8, %rsi
	subq	%rdx, %rsi
	movzbl	(%r10,%rsi), %esi
.L411:
	cmpb	%sil, (%rdx)
	jg	.L416
	jl	.L406
	addq	$1, %rdx
	subl	$1, %eax
	cmpq	%rdx, %rdi
	je	.L407
.L412:
	cmpl	%eax, %ecx
	jg	.L424
.L415:
	xorl	%esi, %esi
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L423:
	subl	%esi, %edx
	leaq	_ZZNK6icu_676number4impl15DecimalQuantity10fitsInLongEbE9INT64_BCD(%rip), %rsi
	leal	-1(%rcx), %edi
	leaq	1(%rsi), %rcx
	addq	%rcx, %rdi
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L425:
	addq	$1, %rsi
	subl	$1, %edx
	cmpq	%rsi, %rdi
	je	.L407
.L410:
	xorl	%ecx, %ecx
	cmpl	$15, %edx
	ja	.L409
	movq	48(%rbx), %r11
	leal	0(,%rdx,4), %ecx
	shrq	%cl, %r11
	movq	%r11, %rcx
	andl	$15, %ecx
.L409:
	cmpb	%cl, (%rsi)
	jg	.L399
	jge	.L425
	.p2align 4,,10
	.p2align 3
.L406:
	xorl	%eax, %eax
.L399:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	call	*%rax
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L404:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L407:
	movzbl	20(%rbx), %eax
	andl	$1, %eax
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L416:
	movl	%r9d, %eax
	jmp	.L399
	.cfi_endproc
.LFE3663:
	.size	_ZNK6icu_676number4impl15DecimalQuantity10fitsInLongEb, .-_ZNK6icu_676number4impl15DecimalQuantity10fitsInLongEb
	.section	.rodata.str1.1
.LC4:
	.string	"0"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity8toDecNumERNS1_6DecNumER10UErrorCode
	.type	_ZNK6icu_676number4impl15DecimalQuantity8toDecNumERNS1_6DecNumER10UErrorCode, @function
_ZNK6icu_676number4impl15DecimalQuantity8toDecNumERNS1_6DecNumER10UErrorCode:
.LFB3665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movl	16(%rdi), %r12d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r12d, %r12d
	je	.L441
.L427:
	leaq	-67(%rbp), %rsi
	movl	$20, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movb	$0, -68(%rbp)
	cmpl	$20, %r12d
	jg	.L442
.L428:
	testl	%r12d, %r12d
	jle	.L431
	xorl	%edi, %edi
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L443:
	movq	48(%rbx), %rdx
	movzbl	(%rdx,%rdi), %ecx
.L433:
	subl	%eax, %r12d
	addq	$1, %rdi
	subl	$1, %r12d
	movslq	%r12d, %r12
	movb	%cl, (%rsi,%r12)
	movl	16(%rbx), %r12d
	movq	-80(%rbp), %rsi
	cmpl	%edi, %r12d
	jle	.L431
.L435:
	cmpb	$0, 64(%rbx)
	movl	%edi, %eax
	jne	.L443
	xorl	%ecx, %ecx
	cmpq	$15, %rdi
	ja	.L433
	movq	48(%rbx), %rdx
	leal	0(,%rdi,4), %ecx
	shrq	%cl, %rdx
	movq	%rdx, %rcx
	andl	$15, %ecx
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L431:
	movzbl	20(%rbx), %r8d
	movl	12(%rbx), %ecx
	movq	%r14, %r9
	movl	%r12d, %edx
	movq	%r13, %rdi
	andl	$1, %r8d
	call	_ZN6icu_676number4impl6DecNum5setToEPKhiibR10UErrorCode@PLT
	cmpb	$0, -68(%rbp)
	jne	.L444
.L426:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L445
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl6DecNum5setToEPKcR10UErrorCode@PLT
	movl	16(%rbx), %r12d
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L444:
	movq	-80(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L442:
	movslq	%r12d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L446
	cmpb	$0, -68(%rbp)
	jne	.L447
.L430:
	movl	%r12d, -72(%rbp)
	movl	16(%rbx), %r12d
	movq	%rsi, -80(%rbp)
	movb	$1, -68(%rbp)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L447:
	movq	-80(%rbp), %rdi
	movq	%rax, -88(%rbp)
	call	uprv_free_67@PLT
	movq	-88(%rbp), %rsi
	jmp	.L430
.L446:
	movl	16(%rbx), %r12d
	movq	-80(%rbp), %rsi
	jmp	.L428
.L445:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3665:
	.size	_ZNK6icu_676number4impl15DecimalQuantity8toDecNumERNS1_6DecNumER10UErrorCode, .-_ZNK6icu_676number4impl15DecimalQuantity8toDecNumERNS1_6DecNumER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity15roundToInfinityEv
	.type	_ZN6icu_676number4impl15DecimalQuantity15roundToInfinityEv, @function
_ZN6icu_676number4impl15DecimalQuantity15roundToInfinityEv:
.LFB3670:
	.cfi_startproc
	endbr64
	cmpb	$0, 21(%rdi)
	jne	.L450
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	jmp	_ZN6icu_676number4impl15DecimalQuantity23convertToAccurateDoubleEv
	.cfi_endproc
.LFE3670:
	.size	_ZN6icu_676number4impl15DecimalQuantity15roundToInfinityEv, .-_ZN6icu_676number4impl15DecimalQuantity15roundToInfinityEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity11appendDigitEaib
	.type	_ZN6icu_676number4impl15DecimalQuantity11appendDigitEaib, @function
_ZN6icu_676number4impl15DecimalQuantity11appendDigitEaib:
.LFB3671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	testb	%sil, %sil
	je	.L481
	movl	12(%rdi), %eax
	movl	%esi, %r13d
	movsbq	%sil, %r8
	testl	%eax, %eax
	jle	.L456
	addl	%eax, %r12d
	testb	%cl, %cl
	je	.L456
	movl	$0, 12(%rdi)
	xorl	%eax, %eax
.L456:
	movl	16(%rbx), %esi
	leal	1(%r12), %r14d
	addl	%r14d, %esi
	cmpb	$0, 64(%rbx)
	jne	.L457
	movq	48(%rbx), %rdx
	cmpl	$16, %esi
	jg	.L482
.L458:
	leal	0(,%r14,4), %ecx
	subl	%r14d, %eax
	movl	%esi, 16(%rbx)
	salq	%cl, %rdx
	movl	%eax, 12(%rbx)
	movq	%rdx, 48(%rbx)
.L468:
	movq	48(%rbx), %rax
	andq	$-16, %rax
	orq	%rax, %r8
	movq	%r8, 48(%rbx)
.L469:
	testb	%r15b, %r15b
	je	.L451
	addl	%r14d, 12(%rbx)
.L451:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	.cfi_restore_state
	testl	%esi, %esi
	je	.L461
	movq	%rbx, %rdi
	movb	%r8b, -49(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	16(%rbx), %edx
	movsbq	-49(%rbp), %r8
	addl	%r14d, %edx
	subl	$1, %edx
	cmpl	%edx, %r14d
	jg	.L466
.L462:
	movslq	%r12d, %rdi
	movslq	%edx, %rax
	notq	%rdi
	.p2align 4,,10
	.p2align 3
.L465:
	movq	48(%rbx), %rcx
	leaq	(%rcx,%rax), %rsi
	movzbl	(%rsi,%rdi), %esi
	movb	%sil, (%rcx,%rax)
	subq	$1, %rax
	cmpl	%eax, %r14d
	jle	.L465
	movl	%r12d, %eax
	movl	$0, %ecx
	subl	%edx, %eax
	addl	$1, %eax
	cmpl	%edx, %r12d
	cmovge	%ecx, %eax
	leal	-1(%rdx,%rax), %edx
.L466:
	movslq	%edx, %rcx
	leaq	-1(%rcx), %rax
	movq	%rax, %rsi
	subq	%rcx, %rsi
	testl	%edx, %edx
	jns	.L467
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L483:
	subq	$1, %rax
.L467:
	movq	48(%rbx), %rdx
	movb	$0, (%rdx,%rcx)
	movq	%rax, %rcx
	cmpq	%rax, %rsi
	jne	.L483
.L479:
	movl	16(%rbx), %ecx
	movl	12(%rbx), %eax
	movzbl	64(%rbx), %edx
	addl	%r14d, %ecx
	subl	%r14d, %eax
	movl	%eax, 12(%rbx)
	movl	%ecx, 16(%rbx)
	testb	%dl, %dl
	je	.L468
.L470:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movq	48(%rbx), %rax
	movb	%r13b, (%rax)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L481:
	testb	%cl, %cl
	je	.L451
	movl	16(%rdi), %eax
	testl	%eax, %eax
	je	.L451
	addl	$1, %r12d
	addl	%r12d, 12(%rdi)
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L461:
	movl	$-1, %edx
	testl	%r14d, %r14d
	js	.L462
	subl	%r14d, 12(%rbx)
	movl	$0, 16(%rbx)
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L482:
	movl	$40, %esi
	movq	%rbx, %rdi
	movb	%r8b, -49(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	16(%rbx), %esi
	movsbq	-49(%rbp), %r8
	testl	%esi, %esi
	jle	.L459
	movq	-64(%rbp), %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L460:
	movl	%edx, %esi
	movq	48(%rbx), %rcx
	shrq	$4, %rdx
	andl	$15, %esi
	movb	%sil, (%rcx,%rax)
	movl	16(%rbx), %esi
	addq	$1, %rax
	cmpl	%eax, %esi
	jg	.L460
.L459:
	addl	%r14d, %esi
	cmpb	$0, 64(%rbx)
	jne	.L457
	movq	48(%rbx), %rdx
	movl	12(%rbx), %eax
	jmp	.L458
	.cfi_endproc
.LFE3671:
	.size	_ZN6icu_676number4impl15DecimalQuantity11appendDigitEaib, .-_ZN6icu_676number4impl15DecimalQuantity11appendDigitEaib
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity13toPlainStringEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity13toPlainStringEv, @function
_ZNK6icu_676number4impl15DecimalQuantity13toPlainStringEv:
.LFB3672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r8w, 8(%rdi)
	movq	%rax, (%rdi)
	testb	$1, 20(%rsi)
	jne	.L514
.L485:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L515
	movl	12(%rbx), %edx
	movl	44(%rbx), %eax
	movl	36(%rbx), %esi
	addl	%edx, %ecx
	leal	(%rdx,%rax), %r13d
	addl	%eax, %ecx
	leal	-1(%rsi), %r12d
	cmpl	%esi, %ecx
	leal	-1(%rcx), %edi
	cmovge	%edi, %r12d
	cmpl	%r13d, 40(%rbx)
	cmovle	40(%rbx), %r13d
	testl	%r12d, %r12d
	jns	.L490
	movl	$48, %edx
	leaq	-42(%rbp), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movw	%dx, -42(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L491:
	testl	%r13d, %r13d
	js	.L516
.L484:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L517
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	testl	%ecx, %ecx
	js	.L493
	cmpl	16(%rbx), %ecx
	jge	.L493
	movq	48(%rbx), %rax
	movslq	%ecx, %rcx
	movsbl	(%rax,%rcx), %esi
	addl	$48, %esi
	.p2align 4,,10
	.p2align 3
.L493:
	movq	%r14, %rdi
	subl	$1, %r12d
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	$-1, %r12d
	je	.L491
	movl	12(%rbx), %edx
	movl	44(%rbx), %eax
.L490:
	movl	%r12d, %ecx
	movl	$48, %esi
	subl	%edx, %ecx
	subl	%eax, %ecx
	cmpb	$0, 64(%rbx)
	jne	.L518
	cmpl	$15, %ecx
	ja	.L493
	movq	48(%rbx), %rsi
	sall	$2, %ecx
	shrq	%cl, %rsi
	andl	$15, %esi
	addl	$48, %esi
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L516:
	movl	$46, %eax
	xorl	%edx, %edx
	leaq	-42(%rbp), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	%r12d, %r13d
	jg	.L484
	subl	$1, %r13d
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L519:
	testl	%ecx, %ecx
	js	.L497
	cmpl	16(%rbx), %ecx
	jge	.L497
	movq	48(%rbx), %rax
	movslq	%ecx, %rcx
	movsbl	(%rax,%rcx), %esi
	addl	$48, %esi
	.p2align 4,,10
	.p2align 3
.L497:
	movq	%r14, %rdi
	subl	$1, %r12d
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%r13d, %r12d
	je	.L484
.L498:
	movl	%r12d, %ecx
	subl	12(%rbx), %ecx
	subl	44(%rbx), %ecx
	movl	$48, %esi
	cmpb	$0, 64(%rbx)
	jne	.L519
	cmpl	$15, %ecx
	ja	.L497
	movq	48(%rbx), %rsi
	sall	$2, %ecx
	shrq	%cl, %rsi
	andl	$15, %esi
	addl	$48, %esi
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L515:
	movl	$48, %ecx
	leaq	-42(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movw	%cx, -42(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L514:
	movl	$45, %esi
	movl	$1, %ecx
	xorl	%edx, %edx
	movw	%si, -42(%rbp)
	leaq	-42(%rbp), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L485
.L517:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3672:
	.size	_ZNK6icu_676number4impl15DecimalQuantity13toPlainStringEv, .-_ZNK6icu_676number4impl15DecimalQuantity13toPlainStringEv
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC5:
	.string	"0"
	.string	"E"
	.string	"+"
	.string	"0"
	.string	""
	.string	""
	.align 2
.LC6:
	.string	"-"
	.string	"2"
	.string	"1"
	.string	"4"
	.string	"7"
	.string	"4"
	.string	"8"
	.string	"3"
	.string	"6"
	.string	"4"
	.string	"8"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity18toScientificStringEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity18toScientificStringEv, @function
_ZNK6icu_676number4impl15DecimalQuantity18toScientificStringEv:
.LFB3673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r9w, 8(%rdi)
	movq	%rax, (%rdi)
	testb	$1, 20(%rsi)
	jne	.L556
.L521:
	movl	16(%r14), %ebx
	testl	%ebx, %ebx
	jne	.L522
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC5(%rip), %rax
.L520:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L557
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	leal	-1(%rbx), %eax
	cmpb	$0, 64(%r14)
	movl	$48, %esi
	movl	%eax, -148(%rbp)
	je	.L524
	testl	%eax, %eax
	js	.L525
	movq	48(%r14), %rdx
	cltq
	movsbl	(%rdx,%rax), %esi
	addl	$48, %esi
.L525:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	subl	$2, %ebx
	jns	.L526
.L560:
	leaq	-130(%rbp), %r13
.L531:
	movl	$69, %edi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movw	%di, -130(%rbp)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-148(%rbp), %eax
	addl	12(%r14), %eax
	addl	44(%r14), %eax
	movl	%eax, %r14d
	cmpl	$-2147483648, %eax
	je	.L558
	testl	%eax, %eax
	js	.L559
	movl	$43, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movw	%dx, -130(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	testl	%r14d, %r14d
	jne	.L536
	movl	$48, %eax
	movw	%ax, -130(%rbp)
.L555:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L536:
	movswl	8(%r12), %r13d
	testw	%r13w, %r13w
	js	.L537
	sarl	$5, %r13d
.L538:
	testl	%r14d, %r14d
	je	.L520
	.p2align 4,,10
	.p2align 3
.L539:
	movl	%r14d, %edi
	movl	$10, %esi
	call	div@PLT
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, %rbx
	sarq	$32, %rcx
	movl	%ebx, %r14d
	addl	$48, %ecx
	call	_ZN6icu_6713UnicodeString7replaceEiii@PLT
	testl	%ebx, %ebx
	jg	.L539
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L524:
	cmpl	$15, %eax
	ja	.L525
	movq	48(%r14), %rsi
	leal	0(,%rax,4), %ecx
	movq	%r12, %rdi
	shrq	%cl, %rsi
	andl	$15, %esi
	addl	$48, %esi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	subl	$2, %ebx
	js	.L560
.L526:
	movl	$46, %esi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-130(%rbp), %r13
	movw	%si, -130(%rbp)
	movslq	%ebx, %r15
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L561:
	cmpl	%ebx, 16(%r14)
	jle	.L530
	movq	48(%r14), %rax
	movsbl	(%rax,%r15), %esi
	addl	$48, %esi
.L530:
	movq	%r12, %rdi
	subl	$1, %ebx
	subq	$1, %r15
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	$-1, %ebx
	je	.L531
.L532:
	cmpb	$0, 64(%r14)
	movl	$48, %esi
	jne	.L561
	cmpl	$15, %ebx
	jg	.L530
	movq	48(%r14), %rsi
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rsi
	andl	$15, %esi
	addl	$48, %esi
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L556:
	leaq	-130(%rbp), %r13
	movl	$45, %r8d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movw	%r8w, -130(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L537:
	movl	12(%r12), %r13d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L559:
	movl	$45, %ecx
	negl	%r14d
	movw	%cx, -130(%rbp)
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L558:
	leaq	-128(%rbp), %r13
	movl	$-1, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L533
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L534:
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L533:
	movl	-116(%rbp), %ecx
	jmp	.L534
.L557:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3673:
	.size	_ZNK6icu_676number4impl15DecimalQuantity18toScientificStringEv, .-_ZNK6icu_676number4impl15DecimalQuantity18toScientificStringEv
	.section	.rodata.str1.1
.LC10:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv, @function
_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv:
.LFB3664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_676number4impl15DecimalQuantity5isNaNEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L563
	movzbl	20(%rdi), %eax
	shrb	$2, %al
	andl	$1, %eax
.L564:
	movsd	.LC8(%rip), %xmm0
	testb	%al, %al
	jne	.L562
	movq	(%r12), %rax
	leaq	_ZNK6icu_676number4impl15DecimalQuantity10isInfiniteEv(%rip), %rdx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L566
	movzbl	20(%r12), %eax
	shrb	%al
	andl	$1, %eax
.L567:
	testb	%al, %al
	je	.L568
	movsd	.LC7(%rip), %xmm0
	testb	$1, 20(%r12)
	jne	.L562
	movsd	.LC9(%rip), %xmm0
.L562:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L578
	addq	$160, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L568:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	leaq	.LC10(%rip), %rax
	leaq	-96(%rbp), %r13
	movq	%r12, %rsi
	movups	%xmm0, -136(%rbp)
	movq	%rax, %xmm0
	movq	%r13, %rdi
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movw	%ax, -104(%rbp)
	movl	$0, -144(%rbp)
	movups	%xmm0, -120(%rbp)
	call	_ZNK6icu_676number4impl15DecimalQuantity18toScientificStringEv
	movzwl	-88(%rbp), %eax
	testw	%ax, %ax
	js	.L569
	movswl	%ax, %edx
	sarl	$5, %edx
.L570:
	testb	$17, %al
	jne	.L575
	leaq	-86(%rbp), %rsi
	testb	$2, %al
	cmove	-72(%rbp), %rsi
.L571:
	leaq	-148(%rbp), %rcx
	leaq	-144(%rbp), %rdi
	call	_ZNK6icu_6717double_conversion23StringToDoubleConverter14StringToDoubleEPKtiPi@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movsd	-168(%rbp), %xmm0
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L569:
	movl	-84(%rbp), %edx
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L563:
	call	*%rax
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L566:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L575:
	xorl	%esi, %esi
	jmp	.L571
.L578:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3664:
	.size	_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv, .-_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity16getPluralOperandENS_13PluralOperandE
	.type	_ZNK6icu_676number4impl15DecimalQuantity16getPluralOperandENS_13PluralOperandE, @function
_ZNK6icu_676number4impl15DecimalQuantity16getPluralOperandENS_13PluralOperandE:
.LFB3636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$6, %esi
	ja	.L580
	leaq	.L582(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L582:
	.long	.L580-.L582
	.long	.L587-.L582
	.long	.L586-.L582
	.long	.L585-.L582
	.long	.L584-.L582
	.long	.L583-.L582
	.long	.L581-.L582
	.text
	.p2align 4,,10
	.p2align 3
.L581:
	pxor	%xmm0, %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cvtsi2sdl	44(%rdi), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	movl	$1, %esi
	call	_ZNK6icu_676number4impl15DecimalQuantity6toLongEb
	testb	$1, 20(%rdi)
	je	.L596
	negq	%rax
	pxor	%xmm0, %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cvtsi2sdq	%rax, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	.cfi_restore_state
	movl	$1, %esi
	call	_ZNK6icu_676number4impl15DecimalQuantity14toFractionLongEb
	testq	%rax, %rax
	js	.L592
.L596:
	pxor	%xmm0, %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cvtsi2sdq	%rax, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L584:
	.cfi_restore_state
	movl	40(%rdi), %eax
	cmpl	%eax, 12(%rdi)
	cmovle	12(%rdi), %eax
.L597:
	negl	%eax
	movl	$0, %edx
	subl	44(%rdi), %eax
	cmovs	%edx, %eax
	pxor	%xmm0, %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cvtsi2sdl	%eax, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L585:
	.cfi_restore_state
	xorl	%esi, %esi
	call	_ZNK6icu_676number4impl15DecimalQuantity14toFractionLongEb
	testq	%rax, %rax
	jns	.L596
.L592:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L583:
	.cfi_restore_state
	movl	12(%rdi), %eax
	jmp	.L597
.L580:
	call	_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv
	popq	%rbp
	.cfi_def_cfa 7, 8
	andpd	.LC11(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3636:
	.size	_ZNK6icu_676number4impl15DecimalQuantity16getPluralOperandENS_13PluralOperandE, .-_ZNK6icu_676number4impl15DecimalQuantity16getPluralOperandENS_13PluralOperandE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity11getDigitPosEi
	.type	_ZNK6icu_676number4impl15DecimalQuantity11getDigitPosEi, @function
_ZNK6icu_676number4impl15DecimalQuantity11getDigitPosEi:
.LFB3674:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpb	$0, 64(%rdi)
	je	.L599
	testl	%esi, %esi
	js	.L598
	cmpl	16(%rdi), %esi
	jl	.L604
.L598:
	ret
	.p2align 4,,10
	.p2align 3
.L599:
	cmpl	$15, %esi
	ja	.L598
	movq	48(%rdi), %rax
	leal	0(,%rsi,4), %ecx
	shrq	%cl, %rax
	andl	$15, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L604:
	movq	48(%rdi), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %eax
	ret
	.cfi_endproc
.LFE3674:
	.size	_ZNK6icu_676number4impl15DecimalQuantity11getDigitPosEi, .-_ZNK6icu_676number4impl15DecimalQuantity11getDigitPosEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity11setDigitPosEia
	.type	_ZN6icu_676number4impl15DecimalQuantity11setDigitPosEia, @function
_ZN6icu_676number4impl15DecimalQuantity11setDigitPosEia:
.LFB3675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 64(%rdi)
	movslq	%esi, %rbx
	je	.L606
	movl	%ebx, %esi
	addl	$1, %esi
	je	.L607
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
.L607:
	movq	48(%r12), %rax
	movb	%r13b, (%rax,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_restore_state
	movq	48(%rdi), %r14
	cmpl	$15, %ebx
	jg	.L618
	movsbq	%dl, %rsi
	movl	$15, %edx
	leal	0(,%rbx,4), %ecx
	movq	%rdx, %rax
	salq	%cl, %rsi
	salq	%cl, %rax
	notq	%rax
	andq	%r14, %rax
	orq	%rsi, %rax
	movq	%rax, 48(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L618:
	.cfi_restore_state
	movl	$40, %esi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	16(%r12), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L611
.L610:
	movl	%r14d, %ecx
	movq	48(%r12), %rdx
	shrq	$4, %r14
	andl	$15, %ecx
	movb	%cl, (%rdx,%rax)
	addq	$1, %rax
	cmpl	%eax, 16(%r12)
	jg	.L610
.L611:
	leal	1(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movq	48(%r12), %rax
	movb	%r13b, (%rax,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3675:
	.size	_ZN6icu_676number4impl15DecimalQuantity11setDigitPosEia, .-_ZN6icu_676number4impl15DecimalQuantity11setDigitPosEia
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity9shiftLeftEi
	.type	_ZN6icu_676number4impl15DecimalQuantity9shiftLeftEi, @function
_ZN6icu_676number4impl15DecimalQuantity9shiftLeftEi:
.LFB3676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	addl	16(%rdi), %esi
	cmpb	$0, 64(%rdi)
	jne	.L620
	movq	48(%rdi), %r13
	cmpl	$16, %esi
	jg	.L638
.L621:
	leal	0(,%r12,4), %ecx
	salq	%cl, %r13
	movq	%r13, 48(%rbx)
.L627:
	subl	%r12d, 12(%rbx)
	movl	%esi, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L620:
	.cfi_restore_state
	testl	%esi, %esi
	je	.L624
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	16(%rbx), %esi
	addl	%r12d, %esi
	subl	$1, %esi
	cmpl	%esi, %r12d
	jg	.L629
.L625:
	movslq	%r12d, %rdi
	movslq	%esi, %rax
	movl	%esi, %r8d
	subq	%rdi, %rax
	subl	%eax, %r8d
	.p2align 4,,10
	.p2align 3
.L628:
	movq	48(%rbx), %rdx
	movzbl	(%rdx,%rax), %ecx
	addq	%rdi, %rdx
	movb	%cl, (%rdx,%rax)
	subq	$1, %rax
	leal	(%r8,%rax), %edx
	cmpl	%edx, %r12d
	jle	.L628
	movl	%r12d, %eax
	movl	$0, %edx
	subl	%esi, %eax
	cmpl	%esi, %r12d
	cmovg	%edx, %eax
	leal	-1(%rsi,%rax), %esi
.L629:
	movslq	%esi, %rdx
	leaq	-1(%rdx), %rax
	movq	%rax, %rdi
	subq	%rdx, %rdi
	testl	%esi, %esi
	jns	.L630
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L639:
	subq	$1, %rax
.L630:
	movq	48(%rbx), %rcx
	movb	$0, (%rcx,%rdx)
	movq	%rax, %rdx
	cmpq	%rax, %rdi
	jne	.L639
.L635:
	movl	16(%rbx), %esi
	addl	%r12d, %esi
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L624:
	testl	%r12d, %r12d
	jns	.L627
	movl	$-1, %esi
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L638:
	movl	$40, %esi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jle	.L622
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L623:
	movl	%r13d, %ecx
	movq	48(%rbx), %rdx
	shrq	$4, %r13
	andl	$15, %ecx
	movb	%cl, (%rdx,%rax)
	movl	16(%rbx), %esi
	addq	$1, %rax
	cmpl	%eax, %esi
	jg	.L623
.L622:
	addl	%r12d, %esi
	cmpb	$0, 64(%rbx)
	jne	.L620
	movq	48(%rbx), %r13
	jmp	.L621
	.cfi_endproc
.LFE3676:
	.size	_ZN6icu_676number4impl15DecimalQuantity9shiftLeftEi, .-_ZN6icu_676number4impl15DecimalQuantity9shiftLeftEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity10shiftRightEi
	.type	_ZN6icu_676number4impl15DecimalQuantity10shiftRightEi, @function
_ZN6icu_676number4impl15DecimalQuantity10shiftRightEi:
.LFB3677:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %ecx
	movl	%ecx, %r8d
	subl	%esi, %r8d
	cmpb	$0, 64(%rdi)
	je	.L641
	xorl	%eax, %eax
	movslq	%esi, %r9
	testl	%r8d, %r8d
	jle	.L651
	.p2align 4,,10
	.p2align 3
.L642:
	movq	48(%rdi), %rdx
	leaq	(%rdx,%rax), %rcx
	movzbl	(%rcx,%r9), %ecx
	movb	%cl, (%rdx,%rax)
	movl	16(%rdi), %ecx
	leal	1(%rax), %edx
	addq	$1, %rax
	movl	%ecx, %r8d
	subl	%esi, %r8d
	cmpl	%eax, %r8d
	jg	.L642
.L645:
	movslq	%edx, %rax
	cmpl	%ecx, %edx
	jge	.L643
	.p2align 4,,10
	.p2align 3
.L646:
	movq	48(%rdi), %rdx
	movb	$0, (%rdx,%rax)
	movl	16(%rdi), %r8d
	addq	$1, %rax
	cmpl	%eax, %r8d
	jg	.L646
	subl	%esi, %r8d
.L643:
	addl	%esi, 12(%rdi)
	movl	%r8d, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	leal	0(,%rsi,4), %ecx
	addl	%esi, 12(%rdi)
	movl	%r8d, 16(%rdi)
	shrq	%cl, 48(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L651:
	xorl	%edx, %edx
	jmp	.L645
	.cfi_endproc
.LFE3677:
	.size	_ZN6icu_676number4impl15DecimalQuantity10shiftRightEi, .-_ZN6icu_676number4impl15DecimalQuantity10shiftRightEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity11popFromLeftEi
	.type	_ZN6icu_676number4impl15DecimalQuantity11popFromLeftEi, @function
_ZN6icu_676number4impl15DecimalQuantity11popFromLeftEi:
.LFB3678:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	movl	%eax, %edx
	subl	%esi, %edx
	cmpb	$0, 64(%rdi)
	je	.L653
	subl	$1, %eax
	cmpl	%edx, %eax
	jl	.L654
	cltq
	.p2align 4,,10
	.p2align 3
.L655:
	movq	48(%rdi), %rdx
	movb	$0, (%rdx,%rax)
	movl	16(%rdi), %edx
	subq	$1, %rax
	subl	%esi, %edx
	cmpl	%eax, %edx
	jle	.L655
.L654:
	movl	%edx, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L653:
	leal	0(,%rdx,4), %ecx
	movq	$-1, %rax
	movl	%edx, 16(%rdi)
	salq	%cl, %rax
	notq	%rax
	andq	%rax, 48(%rdi)
	ret
	.cfi_endproc
.LFE3678:
	.size	_ZN6icu_676number4impl15DecimalQuantity11popFromLeftEi, .-_ZN6icu_676number4impl15DecimalQuantity11popFromLeftEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity12setBcdToZeroEv
	.type	_ZN6icu_676number4impl15DecimalQuantity12setBcdToZeroEv, @function
_ZN6icu_676number4impl15DecimalQuantity12setBcdToZeroEv:
.LFB3679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 64(%rdi)
	jne	.L659
.L657:
	movq	$0, 48(%rbx)
	movq	$0, 12(%rbx)
	movb	$0, 21(%rbx)
	movq	$0x000000000, 24(%rbx)
	movl	$0, 32(%rbx)
	movl	$0, 44(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L659:
	.cfi_restore_state
	movq	48(%rdi), %rdi
	call	uprv_free_67@PLT
	movb	$0, 64(%rbx)
	jmp	.L657
	.cfi_endproc
.LFE3679:
	.size	_ZN6icu_676number4impl15DecimalQuantity12setBcdToZeroEv, .-_ZN6icu_676number4impl15DecimalQuantity12setBcdToZeroEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity12readIntToBcdEi
	.type	_ZN6icu_676number4impl15DecimalQuantity12readIntToBcdEi, @function
_ZN6icu_676number4impl15DecimalQuantity12readIntToBcdEi:
.LFB3680:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L663
	movl	$16, %r9d
	xorl	%eax, %eax
	movabsq	$-3689348814741910323, %r10
	.p2align 4,,10
	.p2align 3
.L662:
	movslq	%esi, %rcx
	shrq	$4, %rax
	subl	$1, %r9d
	movq	%rax, %r8
	movq	%rcx, %rax
	sarl	$31, %esi
	mulq	%r10
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	movq	%rcx, %rdx
	addq	%rax, %rax
	imulq	$1717986919, %rcx, %rcx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$60, %rax
	sarq	$34, %rcx
	addq	%r8, %rax
	subl	%esi, %ecx
	movl	%ecx, %esi
	jne	.L662
	leal	0(,%r9,4), %ecx
	movl	$16, %esi
	movl	$0, 12(%rdi)
	shrq	%cl, %rax
	subl	%r9d, %esi
	movq	%rax, 48(%rdi)
	movl	%esi, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L663:
	xorl	%eax, %eax
	movl	$0, 12(%rdi)
	movq	%rax, 48(%rdi)
	movl	%esi, 16(%rdi)
	ret
	.cfi_endproc
.LFE3680:
	.size	_ZN6icu_676number4impl15DecimalQuantity12readIntToBcdEi, .-_ZN6icu_676number4impl15DecimalQuantity12readIntToBcdEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity13readLongToBcdEl
	.type	_ZN6icu_676number4impl15DecimalQuantity13readLongToBcdEl, @function
_ZN6icu_676number4impl15DecimalQuantity13readLongToBcdEl:
.LFB3681:
	.cfi_startproc
	endbr64
	movabsq	$9999999999999999, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	cmpq	%rax, %rsi
	jg	.L676
	testq	%rsi, %rsi
	je	.L671
	movl	$16, %edi
	xorl	%esi, %esi
	movabsq	$7378697629483820647, %rcx
	.p2align 4,,10
	.p2align 3
.L670:
	movq	%rbx, %rax
	shrq	$4, %rsi
	subl	$1, %edi
	imulq	%rcx
	movq	%rbx, %rax
	sarq	$63, %rax
	sarq	$2, %rdx
	subq	%rax, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rbx
	salq	$60, %rbx
	addq	%rbx, %rsi
	movq	%rdx, %rbx
	testq	%rdx, %rdx
	jne	.L670
	leal	0(,%rdi,4), %ecx
	movl	$16, %eax
	shrq	%cl, %rsi
	subl	%edi, %eax
.L669:
	movq	%rsi, 48(%r12)
.L675:
	popq	%rbx
	movl	%eax, 16(%r12)
	movl	$0, 12(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L676:
	.cfi_restore_state
	movl	$40, %esi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	xorl	%ecx, %ecx
	movabsq	$7378697629483820647, %rsi
	.p2align 4,,10
	.p2align 3
.L667:
	movq	%rbx, %rax
	movq	48(%r12), %rdi
	imulq	%rsi
	movq	%rbx, %rax
	sarq	$63, %rax
	sarq	$2, %rdx
	subq	%rax, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rbx
	leal	1(%rcx), %eax
	movb	%bl, (%rdi,%rcx)
	movq	%rdx, %rbx
	addq	$1, %rcx
	testq	%rdx, %rdx
	jne	.L667
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L671:
	xorl	%eax, %eax
	xorl	%esi, %esi
	jmp	.L669
	.cfi_endproc
.LFE3681:
	.size	_ZN6icu_676number4impl15DecimalQuantity13readLongToBcdEl, .-_ZN6icu_676number4impl15DecimalQuantity13readLongToBcdEl
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity18readDecNumberToBcdERKNS1_6DecNumE
	.type	_ZN6icu_676number4impl15DecimalQuantity18readDecNumberToBcdERKNS1_6DecNumE, @function
_ZN6icu_676number4impl15DecimalQuantity18readDecNumberToBcdERKNS1_6DecNumE:
.LFB3682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rsi), %rbx
	movl	(%rbx), %esi
	cmpl	$16, %esi
	jg	.L733
	testl	%esi, %esi
	jle	.L685
	movzbl	9(%rbx), %eax
	cmpl	$1, %esi
	je	.L682
	movzbl	10(%rbx), %edx
	salq	$4, %rdx
	orq	%rdx, %rax
	cmpl	$2, %esi
	je	.L682
	movzbl	11(%rbx), %edx
	salq	$8, %rdx
	orq	%rdx, %rax
	cmpl	$3, %esi
	je	.L682
	movzbl	12(%rbx), %edx
	salq	$12, %rdx
	orq	%rdx, %rax
	cmpl	$4, %esi
	je	.L682
	movzbl	13(%rbx), %edx
	salq	$16, %rdx
	orq	%rdx, %rax
	cmpl	$5, %esi
	je	.L682
	movzbl	14(%rbx), %edx
	salq	$20, %rdx
	orq	%rdx, %rax
	cmpl	$6, %esi
	je	.L682
	movzbl	15(%rbx), %edx
	salq	$24, %rdx
	orq	%rdx, %rax
	cmpl	$7, %esi
	je	.L682
	movzbl	16(%rbx), %edx
	salq	$28, %rdx
	orq	%rdx, %rax
	cmpl	$8, %esi
	je	.L682
	movzbl	17(%rbx), %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	cmpl	$9, %esi
	je	.L682
	movzbl	18(%rbx), %edx
	salq	$36, %rdx
	orq	%rdx, %rax
	cmpl	$10, %esi
	je	.L682
	movzbl	19(%rbx), %edx
	salq	$40, %rdx
	orq	%rdx, %rax
	cmpl	$11, %esi
	je	.L682
	movzbl	20(%rbx), %edx
	salq	$44, %rdx
	orq	%rdx, %rax
	cmpl	$12, %esi
	je	.L682
	movzbl	21(%rbx), %edx
	salq	$48, %rdx
	orq	%rdx, %rax
	cmpl	$13, %esi
	je	.L682
	movzbl	22(%rbx), %edx
	salq	$52, %rdx
	orq	%rdx, %rax
	cmpl	$14, %esi
	je	.L682
	movzbl	23(%rbx), %edx
	salq	$56, %rdx
	orq	%rdx, %rax
	cmpl	$15, %esi
	je	.L682
	movzbl	24(%rbx), %edx
	salq	$60, %rdx
	orq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L682:
	movq	%rax, 48(%r12)
.L680:
	movl	4(%rbx), %eax
	movl	%eax, 12(%r12)
	movl	(%rbx), %eax
	popq	%rbx
	movl	%eax, 16(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L733:
	.cfi_restore_state
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L680
	.p2align 4,,10
	.p2align 3
.L679:
	movzbl	9(%rbx,%rax), %ecx
	movq	48(%r12), %rdx
	movb	%cl, (%rdx,%rax)
	addq	$1, %rax
	cmpl	%eax, (%rbx)
	jg	.L679
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L685:
	xorl	%eax, %eax
	jmp	.L682
	.cfi_endproc
.LFE3682:
	.size	_ZN6icu_676number4impl15DecimalQuantity18readDecNumberToBcdERKNS1_6DecNumE, .-_ZN6icu_676number4impl15DecimalQuantity18readDecNumberToBcdERKNS1_6DecNumE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity25readDoubleConversionToBcdEPKcii
	.type	_ZN6icu_676number4impl15DecimalQuantity25readDoubleConversionToBcdEPKcii, @function
_ZN6icu_676number4impl15DecimalQuantity25readDoubleConversionToBcdEPKcii:
.LFB3683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	cmpl	$16, %edx
	jg	.L787
	testl	%edx, %edx
	jle	.L740
	movslq	%edx, %rax
	movsbl	-1(%rsi,%rax), %eax
	subl	$48, %eax
	cltq
	cmpl	$1, %edx
	je	.L738
	leal	-1(%rdx), %edx
	movslq	%edx, %rdx
	movsbl	-1(%rsi,%rdx), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	salq	$4, %rdx
	orq	%rdx, %rax
	cmpl	$2, %ebx
	je	.L738
	leal	-2(%rbx), %edx
	movslq	%edx, %rdx
	movsbl	-1(%rsi,%rdx), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	salq	$8, %rdx
	orq	%rdx, %rax
	cmpl	$3, %ebx
	je	.L738
	leal	-3(%rbx), %edx
	movslq	%edx, %rdx
	movsbl	-1(%rsi,%rdx), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	salq	$12, %rdx
	orq	%rdx, %rax
	cmpl	$4, %ebx
	je	.L738
	leal	-4(%rbx), %edx
	movslq	%edx, %rdx
	movsbl	-1(%rsi,%rdx), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	orq	%rdx, %rax
	cmpl	$5, %ebx
	je	.L738
	leal	-5(%rbx), %edx
	movslq	%edx, %rdx
	movsbl	-1(%rsi,%rdx), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	salq	$20, %rdx
	orq	%rdx, %rax
	cmpl	$6, %ebx
	je	.L738
	leal	-6(%rbx), %edx
	movslq	%edx, %rdx
	movsbl	-1(%rsi,%rdx), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	salq	$24, %rdx
	orq	%rdx, %rax
	cmpl	$7, %ebx
	je	.L738
	leal	-7(%rbx), %edx
	movslq	%edx, %rdx
	movsbl	-1(%rsi,%rdx), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	salq	$28, %rdx
	orq	%rdx, %rax
	cmpl	$8, %ebx
	je	.L738
	leal	-8(%rbx), %edx
	movslq	%edx, %rdx
	movsbl	-1(%rsi,%rdx), %edx
	subl	$48, %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	cmpl	$9, %ebx
	je	.L738
	leal	-9(%rbx), %edx
	movslq	%edx, %rdx
	movsbl	-1(%rsi,%rdx), %edx
	subl	$48, %edx
	salq	$36, %rdx
	orq	%rdx, %rax
	cmpl	$10, %ebx
	je	.L738
	leal	-10(%rbx), %edx
	movslq	%edx, %rdx
	movsbl	-1(%rsi,%rdx), %edx
	subl	$48, %edx
	salq	$40, %rdx
	orq	%rdx, %rax
	cmpl	$11, %ebx
	je	.L738
	leal	-11(%rbx), %edx
	movslq	%edx, %rdx
	movsbl	-1(%rsi,%rdx), %edx
	subl	$48, %edx
	salq	$44, %rdx
	orq	%rdx, %rax
	cmpl	$12, %ebx
	je	.L738
	leal	-12(%rbx), %edx
	movslq	%edx, %rdx
	movsbl	-1(%rsi,%rdx), %edx
	subl	$48, %edx
	salq	$48, %rdx
	orq	%rdx, %rax
	cmpl	$13, %ebx
	je	.L738
	leal	-13(%rbx), %edx
	movslq	%edx, %rdx
	movsbl	-1(%rsi,%rdx), %edx
	subl	$48, %edx
	salq	$52, %rdx
	orq	%rdx, %rax
	cmpl	$14, %ebx
	je	.L738
	leal	-14(%rbx), %edx
	movslq	%edx, %rdx
	movsbl	-1(%rsi,%rdx), %edx
	subl	$48, %edx
	salq	$56, %rdx
	orq	%rdx, %rax
	cmpl	$15, %ebx
	je	.L738
	movsbl	(%rsi), %edx
	salq	$60, %rdx
	orq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L738:
	movq	%rax, 48(%r12)
.L736:
	subl	%ebx, %r14d
	movl	%ebx, 16(%r12)
	popq	%rbx
	movl	%r14d, 12(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L787:
	.cfi_restore_state
	movl	%edx, %esi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movslq	%ebx, %rdx
	leal	-1(%rbx), %ecx
	leaq	-1(%r13,%rdx), %rax
	leaq	-2(%r13,%rdx), %rdi
	subq	%rcx, %rdi
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L737:
	movzbl	(%rax), %edx
	movq	48(%r12), %rsi
	movq	%r8, %rcx
	subq	%rax, %rcx
	subq	$1, %rax
	subl	$48, %edx
	movb	%dl, (%rsi,%rcx)
	cmpq	%rdi, %rax
	jne	.L737
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L740:
	xorl	%eax, %eax
	jmp	.L738
	.cfi_endproc
.LFE3683:
	.size	_ZN6icu_676number4impl15DecimalQuantity25readDoubleConversionToBcdEPKcii, .-_ZN6icu_676number4impl15DecimalQuantity25readDoubleConversionToBcdEPKcii
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	.type	_ZN6icu_676number4impl15DecimalQuantity7compactEv, @function
_ZN6icu_676number4impl15DecimalQuantity7compactEv:
.LFB3684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpb	$0, 64(%rdi)
	movq	%rdi, %rbx
	je	.L789
	movl	16(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L790
	movq	48(%rdi), %rdi
	leal	-1(%rcx), %r8d
	xorl	%eax, %eax
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L841:
	leal	1(%rax), %edx
	leaq	1(%rax), %rsi
	cmpq	%rax, %r8
	je	.L840
	movq	%rsi, %rax
.L792:
	cmpb	$0, (%rdi,%rax)
	movl	%eax, %edx
	je	.L841
.L791:
	subl	%edx, %ecx
	xorl	%eax, %eax
	movslq	%edx, %r8
	testl	%ecx, %ecx
	jg	.L799
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L843:
	movq	48(%rbx), %rdi
.L799:
	leaq	(%rdi,%rax), %rcx
	leal	1(%rax), %esi
	movzbl	(%rcx,%r8), %ecx
	movb	%cl, (%rdi,%rax)
	movl	16(%rbx), %edi
	addq	$1, %rax
	movl	%edi, %ecx
	subl	%edx, %ecx
	cmpl	%eax, %ecx
	jg	.L843
	cmpl	%esi, %edi
	jle	.L797
	movq	48(%rbx), %rdi
.L794:
	movslq	%esi, %rax
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L844:
	movq	48(%rbx), %rdi
.L800:
	movb	$0, (%rdi,%rax)
	movl	16(%rbx), %ecx
	addq	$1, %rax
	cmpl	%eax, %ecx
	jg	.L844
	subl	%edx, %ecx
.L797:
	movl	%ecx, %eax
	addl	%edx, 12(%rbx)
	movl	%ecx, 16(%rbx)
	subl	$1, %eax
	js	.L801
	movq	48(%rbx), %rdi
	movslq	%eax, %rdx
	addq	%rdi, %rdx
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L802:
	subl	$1, %eax
	subq	$1, %rdx
	cmpl	$-1, %eax
	je	.L845
.L804:
	cmpb	$0, (%rdx)
	je	.L802
	leal	1(%rax), %ecx
.L801:
	movl	%ecx, 16(%rbx)
	cmpl	$16, %ecx
	jg	.L788
	cmpb	$0, 64(%rbx)
	movq	48(%rbx), %rdi
	je	.L806
	testl	%eax, %eax
	js	.L825
	cltq
	xorl	%r12d, %r12d
	leaq	-1(%rdi,%rax), %rcx
	leaq	(%rdi,%rax), %rdx
	subq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L808:
	movsbq	(%rdx), %rax
	salq	$4, %r12
	subq	$1, %rdx
	orq	%rax, %r12
	cmpq	%rdx, %rcx
	jne	.L808
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L789:
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L838
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L816
	xorl	%edx, %edx
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L812:
	addl	$1, %edx
	cmpl	%eax, %edx
	je	.L846
.L814:
	cmpl	$15, %edx
	jg	.L812
	leal	0(,%rdx,4), %ecx
	movq	%rdi, %rsi
	shrq	%cl, %rsi
	testb	$15, %sil
	je	.L812
	addl	%edx, 12(%rbx)
	subl	$1, %eax
	movq	%rsi, 48(%rbx)
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L817:
	leal	-1(%rax), %edx
	testl	%eax, %eax
	je	.L816
	movl	%edx, %eax
.L818:
	cmpl	$15, %eax
	jg	.L817
	leal	0(,%rax,4), %ecx
	movq	%rsi, %rdx
	shrq	%cl, %rdx
	andl	$15, %edx
	je	.L817
	addl	$1, %eax
.L816:
	movl	%eax, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L847:
	.cfi_restore_state
	movq	48(%rdi), %rdi
	.p2align 4,,10
	.p2align 3
.L793:
	call	uprv_free_67@PLT
	movb	$0, 64(%rbx)
	movq	$0, 48(%rbx)
.L838:
	movq	$0, 12(%rbx)
	movb	$0, 21(%rbx)
	movq	$0x000000000, 24(%rbx)
	movl	$0, 32(%rbx)
	movl	$0, 44(%rbx)
.L788:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L840:
	.cfi_restore_state
	cmpl	%edx, %ecx
	jne	.L791
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L846:
	leal	0(,%rax,4), %ecx
	movq	%rdi, %rsi
	addl	%eax, 12(%rbx)
	subl	$1, %eax
	shrq	%cl, %rsi
	movq	%rsi, 48(%rbx)
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L845:
	cmpb	$0, 64(%rbx)
	movl	$0, 16(%rbx)
	jne	.L825
.L806:
	movq	%rdi, %r12
	movl	$40, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L788
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L809:
	movl	%r12d, %ecx
	movq	48(%rbx), %rdx
	shrq	$4, %r12
	andl	$15, %ecx
	movb	%cl, (%rdx,%rax)
	addq	$1, %rax
	cmpl	%eax, 16(%rbx)
	jg	.L809
	jmp	.L788
.L790:
	je	.L847
	xorl	%edx, %edx
	jmp	.L797
.L842:
	xorl	%esi, %esi
	jmp	.L794
.L825:
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L807:
	call	uprv_free_67@PLT
	movq	%r12, 48(%rbx)
	movb	$0, 64(%rbx)
	jmp	.L788
	.cfi_endproc
.LFE3684:
	.size	_ZN6icu_676number4impl15DecimalQuantity7compactEv, .-_ZN6icu_676number4impl15DecimalQuantity7compactEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity15applyMaxIntegerEi
	.type	_ZN6icu_676number4impl15DecimalQuantity15applyMaxIntegerEi, @function
_ZN6icu_676number4impl15DecimalQuantity15applyMaxIntegerEi:
.LFB3625:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	testl	%eax, %eax
	je	.L859
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	12(%rdi), %edx
	cmpl	%esi, %edx
	jge	.L862
	leal	-1(%rax,%rdx), %edx
	cmpl	%edx, %esi
	jle	.L863
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L862:
	.cfi_restore_state
	cmpb	$0, 64(%rdi)
	jne	.L864
.L851:
	movq	$0, 48(%r12)
	movq	$0, 12(%r12)
	movb	$0, 21(%r12)
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	movl	$0, 44(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L859:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L864:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	48(%rdi), %rdi
	call	uprv_free_67@PLT
	movb	$0, 64(%r12)
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L863:
	subl	%esi, %edx
	leal	1(%rdx), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	cmpb	$0, 64(%rdi)
	je	.L852
	subl	$1, %eax
	cmpl	%edx, %eax
	jl	.L853
	cltq
	.p2align 4,,10
	.p2align 3
.L854:
	movq	48(%r12), %rdx
	movb	$0, (%rdx,%rax)
	movl	16(%r12), %edx
	subq	$1, %rax
	subl	%ecx, %edx
	cmpl	%eax, %edx
	jle	.L854
.L853:
	movl	%edx, 16(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	.p2align 4,,10
	.p2align 3
.L852:
	.cfi_restore_state
	leal	0(,%rdx,4), %ecx
	movq	$-1, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%rax, 48(%rdi)
	jmp	.L853
	.cfi_endproc
.LFE3625:
	.size	_ZN6icu_676number4impl15DecimalQuantity15applyMaxIntegerEi, .-_ZN6icu_676number4impl15DecimalQuantity15applyMaxIntegerEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity8setToIntEi
	.type	_ZN6icu_676number4impl15DecimalQuantity8setToIntEi, @function
_ZN6icu_676number4impl15DecimalQuantity8setToIntEi:
.LFB3650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 64(%rdi)
	movl	%esi, %ebx
	jne	.L878
.L866:
	movq	$0, 48(%r12)
	xorl	%eax, %eax
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	movl	$0, 44(%r12)
	movq	$0, 12(%r12)
	movw	%ax, 20(%r12)
	cmpl	$-2147483648, %ebx
	je	.L879
	testl	%ebx, %ebx
	js	.L880
	jne	.L870
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L880:
	.cfi_restore_state
	movb	$1, 20(%r12)
	negl	%ebx
.L870:
	movl	$16, %edi
	xorl	%eax, %eax
	movl	$3435973837, %r8d
	movabsq	$-3689348814741910323, %r9
	.p2align 4,,10
	.p2align 3
.L872:
	movslq	%ebx, %rcx
	shrq	$4, %rax
	movl	%ebx, %ebx
	subl	$1, %edi
	movq	%rax, %rsi
	imulq	%r8, %rbx
	movq	%rcx, %rax
	mulq	%r9
	shrq	$35, %rbx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rcx
	salq	$60, %rcx
	leaq	(%rcx,%rsi), %rax
	testl	%ebx, %ebx
	jne	.L872
	leal	0(,%rdi,4), %ecx
	shrq	%cl, %rax
	movq	%rax, 48(%r12)
	movl	$16, %eax
	subl	%edi, %eax
	movl	%eax, 16(%r12)
.L868:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L879:
	.cfi_restore_state
	movabsq	$142929835592, %rax
	movb	$1, 20(%r12)
	movq	%rax, 48(%r12)
	movl	$10, 16(%r12)
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L878:
	movq	48(%rdi), %rdi
	call	uprv_free_67@PLT
	movb	$0, 64(%r12)
	jmp	.L866
	.cfi_endproc
.LFE3650:
	.size	_ZN6icu_676number4impl15DecimalQuantity8setToIntEi, .-_ZN6icu_676number4impl15DecimalQuantity8setToIntEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity8truncateEv
	.type	_ZN6icu_676number4impl15DecimalQuantity8truncateEv, @function
_ZN6icu_676number4impl15DecimalQuantity8truncateEv:
.LFB3666:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %ecx
	testl	%ecx, %ecx
	js	.L892
	ret
	.p2align 4,,10
	.p2align 3
.L892:
	movl	16(%rdi), %r8d
	cmpb	$0, 64(%rdi)
	leal	(%rcx,%r8), %edx
	je	.L883
	testl	%edx, %edx
	jle	.L893
	movl	%ecx, %esi
	xorl	%eax, %eax
	movslq	%ecx, %r9
	negl	%esi
	movslq	%esi, %rsi
	.p2align 4,,10
	.p2align 3
.L888:
	movq	48(%rdi), %rdx
	addl	$1, %eax
	movzbl	(%rdx,%rsi), %r8d
	addq	%rsi, %rdx
	addq	$1, %rsi
	movb	%r8b, (%rdx,%r9)
	movl	16(%rdi), %r8d
	leal	(%rcx,%r8), %edx
	cmpl	%eax, %edx
	jg	.L888
.L887:
	cmpl	%r8d, %eax
	jge	.L885
	cltq
	.p2align 4,,10
	.p2align 3
.L889:
	movq	48(%rdi), %rdx
	movb	$0, (%rdx,%rax)
	movl	16(%rdi), %edx
	addq	$1, %rax
	cmpl	%eax, %edx
	jg	.L889
	addl	%ecx, %edx
	movl	$0, 12(%rdi)
	movl	%edx, 16(%rdi)
	jmp	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	.p2align 4,,10
	.p2align 3
.L883:
	negl	%ecx
	sall	$2, %ecx
	shrq	%cl, 48(%rdi)
.L885:
	movl	%edx, 16(%rdi)
	movl	$0, 12(%rdi)
	jmp	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	.p2align 4,,10
	.p2align 3
.L893:
	xorl	%eax, %eax
	jmp	.L887
	.cfi_endproc
.LFE3666:
	.size	_ZN6icu_676number4impl15DecimalQuantity8truncateEv, .-_ZN6icu_676number4impl15DecimalQuantity8truncateEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModebR10UErrorCode
	.type	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModebR10UErrorCode, @function
_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModebR10UErrorCode:
.LFB3669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$40, %rsp
	movl	12(%rdi), %edx
	movzbl	64(%rdi), %eax
	subl	%edx, %ebx
	movl	%ebx, %r10d
	cmpl	%ebx, %esi
	jle	.L895
	testl	%edx, %edx
	js	.L1187
.L895:
	cmpl	%ebx, %r13d
	jge	.L1104
	testl	%edx, %edx
	jg	.L1028
.L1104:
	testb	%al, %al
	jne	.L1188
	xorl	%r9d, %r9d
	cmpl	$15, %ebx
	ja	.L901
	movq	48(%r12), %r9
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %r9
	andl	$15, %r9d
.L901:
	testl	%ebx, %ebx
	jle	.L1189
.L905:
	movl	16(%r12), %esi
	testl	%esi, %esi
	jne	.L1022
.L894:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1188:
	.cfi_restore_state
	testl	%ebx, %ebx
	js	.L898
	movl	16(%r12), %esi
	cmpl	%esi, %ebx
	jge	.L1029
	movq	48(%r12), %rcx
	movslq	%ebx, %rdx
	movzbl	(%rcx,%rdx), %r9d
	testl	%ebx, %ebx
	je	.L902
	leal	-1(%rbx), %edx
	cmpl	%edx, %ebx
	jl	.L1032
.L904:
	cmpl	%edx, %esi
	movl	%edx, %eax
	setle	%dil
	shrl	$31, %eax
	xorl	%ecx, %ecx
	orb	%dil, %al
	je	.L1190
.L903:
	cmpb	$0, 21(%r12)
	je	.L1026
.L908:
	subl	$2, %ebx
	cmpl	%r10d, %ebx
	jg	.L1191
.L919:
	subl	$14, %esi
	xorl	%edi, %edi
	movq	%r8, -72(%rbp)
	movb	%r9b, -58(%rbp)
	movl	%r10d, -64(%rbp)
	movb	%cl, -57(%rbp)
	movl	%edx, -56(%rbp)
	call	uprv_max_67@PLT
	movzbl	-57(%rbp), %ecx
	movl	-56(%rbp), %edx
	movl	-64(%rbp), %r10d
	movzbl	-58(%rbp), %r9d
	testb	%cl, %cl
	movq	-72(%rbp), %r8
	jne	.L920
	testb	%r9b, %r9b
	movl	16(%r12), %esi
	sete	%r11b
	cmpb	$5, %r9b
	sete	%cl
	orl	%ecx, %r11d
	movl	%r14d, %ecx
	xorl	$1, %ecx
	orb	%cl, %r11b
	je	.L921
	cmpl	%ebx, %eax
	jg	.L1192
	movzbl	64(%r12), %edi
	testb	%dil, %dil
	je	.L928
	movslq	%ebx, %rcx
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L929:
	subl	$1, %ebx
	subq	$1, %rcx
	cmpl	%ebx, %eax
	jg	.L1193
.L930:
	testl	%ebx, %ebx
	js	.L929
	cmpl	%esi, %ebx
	jge	.L929
	movq	48(%r12), %r11
	cmpb	$0, (%r11,%rcx)
	je	.L929
.L1048:
	movl	%edi, %r11d
.L1167:
	movl	$1, %edi
.L927:
	leal	-14(%rsi), %eax
	cmpl	$3, %r15d
	jbe	.L960
	cmpl	%edx, %eax
	jg	.L923
	cmpl	$2, %edi
	jne	.L961
.L923:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantity23convertToAccurateDoubleEv
	movq	-56(%rbp), %r8
	addq	$40, %rsp
	movl	%r15d, %edx
	popq	%rbx
	movzbl	%r14b, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModebR10UErrorCode
	.p2align 4,,10
	.p2align 3
.L1187:
	.cfi_restore_state
	movl	16(%rdi), %esi
	testl	%esi, %esi
	je	.L894
	testb	%al, %al
	je	.L1194
	movl	$2147483646, %edx
	movl	$2147483647, %ebx
	xorl	%r9d, %r9d
	movl	$2147483647, %r10d
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L902:
	cmpb	$0, 21(%r12)
	je	.L1023
	.p2align 4,,10
	.p2align 3
.L1022:
	leal	-1(%rbx), %edx
	cmpl	%r10d, %edx
	jg	.L1032
	testb	%al, %al
	jne	.L904
	xorl	%ecx, %ecx
	cmpl	$15, %edx
	ja	.L903
	movq	48(%r12), %rdi
	leal	0(,%rdx,4), %ecx
	shrq	%cl, %rdi
	movq	%rdi, %rcx
	andl	$15, %ecx
	cmpb	$0, 21(%r12)
	jne	.L908
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1194:
	movzbl	21(%rdi), %ecx
	xorl	%r9d, %r9d
	testb	%cl, %cl
	jne	.L1195
	movl	$2147483647, %ebx
	movl	$2147483647, %r10d
	.p2align 4,,10
	.p2align 3
.L1026:
	cmpb	$2, %r9b
	setne	%r11b
	andb	%r14b, %r11b
	je	.L909
	cmpb	$7, %r9b
	je	.L910
	cmpb	$1, %r9b
	jle	.L1035
	leal	-5(%r9), %eax
	cmpb	$1, %al
	jbe	.L1096
	movl	%r11d, %edx
	movl	$3, %edi
.L911:
	leal	-2(%r9), %eax
	cmpb	$5, %al
	ja	.L966
.L1009:
	cmpb	$7, %r9b
	sete	%r11b
	andl	%edx, %r11d
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L926:
	subl	$1, %ebx
	cmpl	%ebx, %eax
	jg	.L1168
.L928:
	cmpl	$15, %ebx
	ja	.L926
	movq	48(%r12), %rdi
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rdi
	movq	%rdi, %rcx
	andl	$15, %ecx
	je	.L926
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L909:
	movl	%r9d, %edx
	cmpb	$4, %cl
	jle	.L1068
	movl	$3, %edi
	cmpb	$5, %cl
	je	.L1196
.L913:
	testb	%r14b, %r14b
	jne	.L1011
	notl	%edx
	movl	%edx, %r11d
	andl	$1, %r11d
.L966:
	movzbl	20(%r12), %ebx
	cmpl	$6, %r15d
	ja	.L967
	leaq	.L969(%rip), %rcx
	movslq	(%rcx,%r15,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L969:
	.long	.L975-.L969
	.long	.L974-.L969
	.long	.L1075-.L969
	.long	.L1074-.L969
	.long	.L971-.L969
	.long	.L970-.L969
	.long	.L968-.L969
	.text
	.p2align 4,,10
	.p2align 3
.L1190:
	movq	48(%r12), %rcx
	movslq	%edx, %rax
	movzbl	(%rcx,%rax), %ecx
	movl	$1, %eax
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L1029:
	xorl	%r9d, %r9d
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L968:
	cmpl	$1, %edi
	je	.L1075
	subl	$2, %edi
	cmpl	$1, %edi
	ja	.L967
.L1074:
	xorl	%r11d, %r11d
.L972:
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L894
	movzbl	64(%r12), %eax
	cmpl	%r10d, %esi
	jg	.L976
	testb	%al, %al
	jne	.L1197
.L977:
	movq	$0, 48(%r12)
	xorl	%esi, %esi
	movl	$0, 16(%r12)
	movb	$0, 21(%r12)
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	movl	$0, 44(%r12)
	movl	%r13d, 12(%r12)
.L978:
	testb	%r14b, %r14b
	je	.L985
	movzbl	64(%r12), %eax
	cmpb	$4, %r9b
	jg	.L1152
	testb	%r11b, %r11b
	jne	.L1198
	cmpb	$4, %r9b
	jg	.L1152
.L989:
	testb	%al, %al
	je	.L993
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movq	48(%r12), %rax
	movb	$5, (%rax)
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L970:
	xorl	%r11d, %r11d
	cmpl	$3, %edi
	je	.L972
	testl	%edi, %edi
	jle	.L967
	.p2align 4,,10
	.p2align 3
.L1075:
	movl	$1, %r11d
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L1152:
	cmpb	$1, %r11b
	je	.L989
	testb	%al, %al
	je	.L990
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movq	48(%r12), %rax
	movb	$9, (%rax)
	movzbl	64(%r12), %edi
	movl	16(%r12), %esi
.L991:
	xorl	%eax, %eax
	testb	%dil, %dil
	je	.L992
.L995:
	movl	%eax, %edx
	cmpl	%eax, %esi
	jg	.L1199
.L998:
	movl	%esi, %ecx
	xorl	%eax, %eax
	movslq	%edx, %rdi
	subl	%edx, %ecx
	testl	%ecx, %ecx
	jle	.L1200
.L999:
	movq	48(%r12), %rcx
	leal	1(%rax), %r8d
	leaq	(%rcx,%rax), %rsi
	movzbl	(%rsi,%rdi), %esi
	movb	%sil, (%rcx,%rax)
	movl	16(%r12), %esi
	addq	$1, %rax
	movl	%esi, %ecx
	subl	%edx, %ecx
	cmpl	%eax, %ecx
	jg	.L999
.L1002:
	movslq	%r8d, %rax
	cmpl	%r8d, %esi
	jle	.L1201
	.p2align 4,,10
	.p2align 3
.L1003:
	movq	48(%r12), %rcx
	movb	$0, (%rcx,%rax)
	movl	16(%r12), %esi
	addq	$1, %rax
	cmpl	%esi, %eax
	jl	.L1003
	movzbl	64(%r12), %edi
	subl	%edx, %esi
.L1001:
	addl	%edx, 12(%r12)
	movl	%esi, 16(%r12)
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1189:
	cmpb	$0, 21(%r12)
	jne	.L905
.L1023:
	testb	%r9b, %r9b
	sete	%cl
	cmpb	$5, %r9b
	sete	%dl
	orb	%dl, %cl
	jne	.L894
	cmpb	$1, %r14b
	je	.L905
	jmp	.L894
.L1028:
	movl	$-2147483648, %ebx
	movl	$-2147483648, %r10d
	.p2align 4,,10
	.p2align 3
.L898:
	xorl	%r9d, %r9d
	cmpb	$0, 21(%r12)
	jne	.L905
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L920:
	movl	16(%r12), %esi
	cmpb	$4, %cl
	je	.L1202
	cmpb	$5, %cl
	jne	.L938
	cmpb	$2, %r9b
	sete	%dil
	cmpb	$7, %r9b
	sete	%cl
	orl	%ecx, %edi
	movl	%r14d, %ecx
	xorl	$1, %ecx
	orb	%cl, %dil
	movb	%dil, -56(%rbp)
	je	.L932
	cmpl	%ebx, %eax
	jg	.L1203
	movzbl	64(%r12), %r11d
	testb	%r11b, %r11b
	je	.L942
	movslq	%ebx, %rcx
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L943:
	subl	$1, %ebx
	subq	$1, %rcx
	cmpl	%ebx, %eax
	jg	.L1060
.L944:
	testl	%ebx, %ebx
	js	.L943
	cmpl	%ebx, %esi
	jle	.L943
	movq	48(%r12), %rdi
	cmpb	$0, (%rdi,%rcx)
	je	.L943
.L1066:
	xorl	%r11d, %r11d
.L1169:
	movl	$3, %edi
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L985:
	testb	%r11b, %r11b
	jne	.L988
	movzbl	64(%r12), %edi
	cmpb	$9, %r9b
	je	.L991
.L994:
	testb	%dil, %dil
	je	.L1004
	movl	$1, %ebx
	testl	%esi, %esi
	jle	.L1005
	movq	48(%r12), %rax
	movzbl	(%rax), %ebx
	addl	$1, %ebx
.L1005:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movq	48(%r12), %rax
	movb	%bl, (%rax)
	movl	16(%r12), %esi
.L1006:
	addl	$1, %esi
	movl	%esi, 16(%r12)
.L988:
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	.p2align 4,,10
	.p2align 3
.L960:
	.cfi_restore_state
	cmpl	%edx, %eax
	jg	.L923
	testl	%edi, %edi
	js	.L923
.L961:
	movb	$0, 21(%r12)
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	testl	%r10d, %r10d
	jle	.L962
	movl	%r9d, %edx
	cmpl	$-1, %edi
	je	.L1068
	.p2align 4,,10
	.p2align 3
.L1018:
	cmpl	$-2, %edi
	jne	.L913
.L1043:
	xorl	%r11d, %r11d
	movl	$3, %edi
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L1068:
	movl	$1, %r11d
	movl	$1, %edi
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L971:
	cmpl	$2, %edi
	je	.L972
	cmpl	$3, %edi
	je	.L1074
	cmpl	$1, %edi
	je	.L1075
.L967:
	movl	$65809, (%r8)
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L976:
	subl	%r10d, %esi
	testb	%al, %al
	je	.L1204
	xorl	%eax, %eax
	movslq	%r10d, %rdi
	.p2align 4,,10
	.p2align 3
.L979:
	movq	48(%r12), %rdx
	leaq	(%rdx,%rax), %rcx
	movzbl	(%rcx,%rdi), %ecx
	movb	%cl, (%rdx,%rax)
	movl	16(%r12), %ecx
	leal	1(%rax), %edx
	addq	$1, %rax
	movl	%ecx, %esi
	subl	%r10d, %esi
	cmpl	%eax, %esi
	jg	.L979
	cmpl	%edx, %ecx
	jle	.L981
	movslq	%edx, %rax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L984:
	movq	48(%r12), %rdx
	movb	$0, (%rdx,%rax)
	addq	$1, %rax
	movl	16(%r12), %esi
	leal	(%rcx,%rax), %edx
	cmpl	%edx, %esi
	jg	.L984
	subl	%r10d, %esi
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L974:
	notl	%ebx
	movl	%ebx, %r11d
	andl	$1, %r11d
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L975:
	andl	$1, %ebx
	movl	%ebx, %r11d
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L941:
	subl	$1, %ebx
	cmpl	%ebx, %eax
	jg	.L1205
.L942:
	cmpl	$15, %ebx
	ja	.L941
	movq	48(%r12), %rdi
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rdi
	movq	%rdi, %rcx
	andl	$15, %ecx
	je	.L941
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1035:
	xorl	%edx, %edx
	movl	$1, %edi
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L938:
	cmpb	$9, %cl
	jne	.L945
	cmpb	$4, %r9b
	sete	%dil
	cmpb	$9, %r9b
	sete	%cl
	orl	%ecx, %edi
	movl	%r14d, %ecx
	xorl	$1, %ecx
	orb	%cl, %dil
	movb	%dil, -56(%rbp)
	je	.L946
	cmpl	%ebx, %eax
	jg	.L1206
	movzbl	64(%r12), %r11d
	testb	%r11b, %r11b
	je	.L949
	movslq	%ebx, %rcx
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L1208:
	cmpl	%ebx, %esi
	jle	.L1066
	movq	48(%r12), %rdi
	cmpb	$9, (%rdi,%rcx)
	jne	.L1066
	subl	$1, %ebx
	subq	$1, %rcx
	cmpl	%ebx, %eax
	jg	.L1207
.L950:
	testl	%ebx, %ebx
	jns	.L1208
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1210:
	movq	48(%r12), %rdi
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rdi
	movq	%rdi, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jne	.L1169
	subl	$1, %ebx
	cmpl	%eax, %ebx
	jl	.L1209
.L949:
	cmpl	$15, %ebx
	ja	.L1169
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1195:
	movl	$2147483647, %r10d
	xorl	%ecx, %ecx
	movl	$2147483645, %ebx
	movl	$2147483646, %edx
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L1204:
	leal	0(,%r10,4), %ecx
	shrq	%cl, 48(%r12)
.L981:
	addl	%r10d, 12(%r12)
	movl	%esi, 16(%r12)
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L1196:
	subl	$2, %ebx
	cmpl	%r10d, %ebx
	jg	.L1042
	testl	%ebx, %ebx
	js	.L1042
.L1025:
	testb	%al, %al
	je	.L916
	movslq	%ebx, %rcx
	.p2align 4,,10
	.p2align 3
.L918:
	cmpl	%esi, %ebx
	jge	.L917
	movq	48(%r12), %rdi
	cmpb	$0, (%rdi,%rcx)
	jne	.L1043
.L917:
	subl	$1, %ebx
	subq	$1, %rcx
	cmpl	$-1, %ebx
	jne	.L918
	movl	%eax, %r11d
	movl	$2, %edi
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L916:
	cmpl	$15, %ebx
	jg	.L915
	movq	48(%r12), %rax
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rax
	testb	$15, %al
	jne	.L1043
.L915:
	subl	$1, %ebx
	cmpl	$-1, %ebx
	jne	.L916
.L1042:
	movl	$1, %r11d
	movl	$2, %edi
	jmp	.L913
.L1032:
	movl	$-2147483648, %edx
	xorl	%ecx, %ecx
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L1202:
	cmpb	$2, %r9b
	sete	%r11b
	cmpb	$7, %r9b
	sete	%cl
	orl	%ecx, %r11d
	movl	%r14d, %ecx
	xorl	$1, %ecx
	orb	%cl, %r11b
	je	.L932
	cmpl	%ebx, %eax
	jg	.L1211
	movzbl	64(%r12), %edi
	testb	%dil, %dil
	je	.L936
	movslq	%ebx, %rcx
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L1213:
	cmpl	%esi, %ebx
	jge	.L1048
	movq	48(%r12), %r11
	cmpb	$9, (%r11,%rcx)
	jne	.L1048
	subl	$1, %ebx
	subq	$1, %rcx
	cmpl	%ebx, %eax
	jg	.L1212
.L937:
	testl	%ebx, %ebx
	jns	.L1213
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1214:
	movq	48(%r12), %rdi
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rdi
	movq	%rdi, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jne	.L1167
	subl	$1, %ebx
	cmpl	%ebx, %eax
	jg	.L1060
.L936:
	cmpl	$15, %ebx
	ja	.L1167
	jmp	.L1214
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	48(%r12), %rcx
	movzbl	(%rcx,%rax), %ecx
	addq	$1, %rax
	cmpb	$9, %cl
	je	.L995
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L910:
	cmpb	$4, %cl
	jle	.L1096
	movl	$3, %edi
	cmpb	$5, %cl
	je	.L1215
.L912:
	cmpl	$3, %edi
	sete	%dl
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L921:
	cmpb	$2, %r9b
	je	.L952
	cmpb	$7, %r9b
	jne	.L932
.L952:
	leal	-14(%rsi), %eax
.L1185:
	cmpl	%edx, %eax
	jg	.L923
	movl	$1, %r11d
	movl	$1, %edi
.L934:
	movb	$0, 21(%r12)
	movl	%r9d, %edx
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	testl	%r10d, %r10d
	jg	.L1018
.L962:
	testb	%r9b, %r9b
	sete	%dl
	cmpb	$5, %r9b
	sete	%al
	orb	%al, %dl
	jne	.L894
	cmpb	$1, %r14b
	jne	.L894
	movl	%r9d, %edx
	cmpl	$-1, %edi
	je	.L1216
	cmpl	$-2, %edi
	je	.L1217
.L1011:
	subl	$2, %edx
	cmpb	$5, %dl
	jbe	.L1014
.L1170:
	movl	$1, %r11d
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	48(%r12), %rdi
	movb	%r9b, -57(%rbp)
	movb	%r11b, -56(%rbp)
	call	uprv_free_67@PLT
	movb	$0, 64(%r12)
	movzbl	-57(%rbp), %r9d
	movzbl	-56(%rbp), %r11d
	jmp	.L977
.L1218:
	movl	$2, %edi
.L1014:
	cmpb	$2, %r9b
	sete	%al
	andb	%al, %r11b
	jne	.L966
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L1193:
	movl	%edi, %r11d
.L1168:
	movl	$-1, %edi
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	48(%r12), %rax
	movq	%rax, %rdx
	andq	$-16, %rax
	andl	$15, %edx
	addq	$1, %rdx
	orq	%rdx, %rax
	movq	%rax, 48(%r12)
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1096:
	movl	$1, %edi
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L932:
	cmpb	$1, %r9b
	jle	.L952
	leal	-5(%r9), %eax
	cmpb	$1, %al
	leal	-14(%rsi), %eax
	jbe	.L1185
	cmpl	%edx, %eax
	jg	.L923
	xorl	%r11d, %r11d
	movl	$3, %edi
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L990:
	movq	48(%r12), %rax
	andq	$-16, %rax
	orq	$9, %rax
	movq	%rax, 48(%r12)
.L992:
	movq	48(%r12), %rax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1078
	movl	%eax, %edx
	shrb	$4, %dl
	cmpb	$9, %dl
	jne	.L1079
	movq	%rax, %rdx
	shrq	$8, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1080
	movq	%rax, %rdx
	shrq	$12, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1081
	movq	%rax, %rdx
	shrq	$16, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1082
	movq	%rax, %rdx
	shrq	$20, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1083
	movq	%rax, %rdx
	shrq	$24, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1084
	movq	%rax, %rdx
	shrq	$28, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1085
	movq	%rax, %rdx
	shrq	$32, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1086
	movq	%rax, %rdx
	shrq	$36, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1087
	movq	%rax, %rdx
	shrq	$40, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1088
	movq	%rax, %rdx
	shrq	$44, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1089
	movq	%rax, %rdx
	shrq	$48, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1090
	movq	%rax, %rdx
	shrq	$52, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1091
	movq	%rax, %rdx
	shrq	$56, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1092
	movq	%rax, %rdx
	shrq	$60, %rdx
	cmpb	$9, %dl
	jne	.L1093
	movl	$64, %ecx
	movl	$16, %edx
	shrq	%cl, %rax
.L997:
	movq	%rax, 48(%r12)
	subl	%edx, %esi
	xorl	%edi, %edi
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1198:
	testb	%al, %al
	je	.L987
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movq	48(%r12), %rax
	movb	$0, (%rax)
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1060:
	movl	$2, %edi
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L993:
	movq	48(%r12), %rax
	andq	$-16, %rax
	orq	$5, %rax
	movq	%rax, 48(%r12)
	jmp	.L894
.L946:
	cmpb	$7, %r9b
	setne	%r11b
	cmpb	$2, %r9b
	setne	%al
	andb	%al, %r11b
	jne	.L932
.L1186:
	leal	-14(%rsi), %eax
	cmpl	%edx, %eax
	jg	.L923
	movl	$3, %edi
	jmp	.L934
.L987:
	andq	$-16, 48(%r12)
	jmp	.L988
.L1212:
	movl	%edi, %r11d
	movl	$2, %edi
	jmp	.L927
.L1215:
	subl	$2, %ebx
	cmpl	%r10d, %ebx
	jg	.L1218
	movl	$7, %edx
	testl	%ebx, %ebx
	jns	.L1025
	movl	$2, %edi
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1205:
	movzbl	-56(%rbp), %r11d
	movl	$2, %edi
	jmp	.L927
.L1216:
	movl	$1, %r11d
	movl	$1, %edi
	jmp	.L1011
.L1209:
	movzbl	-56(%rbp), %r11d
	movl	$-2, %edi
	jmp	.L927
.L1207:
	movl	$-2, %edi
	jmp	.L927
.L1192:
	leal	-14(%rsi), %eax
	cmpl	$3, %r15d
	jbe	.L923
	cmpl	%edx, %eax
	jg	.L923
	movb	$0, 21(%r12)
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	testl	%r10d, %r10d
	jle	.L894
	movl	%r9d, %edx
	movl	$1, %edi
	jmp	.L913
.L1200:
	xorl	%r8d, %r8d
	jmp	.L1002
.L1217:
	leal	-2(%r9), %eax
	movl	$3, %edi
	cmpb	$5, %al
	ja	.L1170
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L1191:
	movl	$-2147483648, %ebx
	jmp	.L919
.L1211:
	leal	-14(%rsi), %eax
	cmpl	$3, %r15d
	ja	.L923
	cmpl	%edx, %eax
	jg	.L923
.L1174:
	movl	$2, %edi
	jmp	.L934
.L1203:
	leal	-14(%rsi), %eax
	cmpl	$3, %r15d
	ja	.L923
	cmpl	%edx, %eax
	jg	.L923
	movl	%edi, %r11d
	jmp	.L1174
.L1201:
	movzbl	64(%r12), %edi
	movl	%ecx, %esi
	jmp	.L1001
.L1206:
	leal	-14(%rsi), %eax
	cmpl	$3, %r15d
	jbe	.L923
	cmpl	%edx, %eax
	jg	.L923
	movl	%edi, %r11d
	movl	$-2, %edi
	jmp	.L934
.L945:
	cmpb	$2, %r9b
	setne	%r11b
	cmpb	$7, %r9b
	setne	%al
	andl	%eax, %r11d
	andb	%r14b, %r11b
	jne	.L932
	cmpb	$4, %cl
	jg	.L1186
	jmp	.L952
.L1081:
	movl	$3, %edx
.L996:
	movq	48(%r12), %rax
	leal	0(,%rdx,4), %ecx
	shrq	%cl, %rax
	jmp	.L997
.L1080:
	movl	$2, %edx
	jmp	.L996
.L1079:
	movl	$1, %edx
	jmp	.L996
.L1093:
	movl	$15, %edx
	jmp	.L996
.L1092:
	movl	$14, %edx
	jmp	.L996
.L1091:
	movl	$13, %edx
	jmp	.L996
.L1090:
	movl	$12, %edx
	jmp	.L996
.L1089:
	movl	$11, %edx
	jmp	.L996
.L1088:
	movl	$10, %edx
	jmp	.L996
.L1087:
	movl	$9, %edx
	jmp	.L996
.L1086:
	movl	$8, %edx
	jmp	.L996
.L1085:
	movl	$7, %edx
	jmp	.L996
.L1084:
	movl	$6, %edx
	jmp	.L996
.L1083:
	movl	$5, %edx
	jmp	.L996
.L1082:
	movl	$4, %edx
	jmp	.L996
.L1078:
	xorl	%edx, %edx
	jmp	.L996
	.cfi_endproc
.LFE3669:
	.size	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModebR10UErrorCode, .-_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModebR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity12_setToDecNumERKNS1_6DecNumER10UErrorCode
	.type	_ZN6icu_676number4impl15DecimalQuantity12_setToDecNumERKNS1_6DecNumER10UErrorCode, @function
_ZN6icu_676number4impl15DecimalQuantity12_setToDecNumERKNS1_6DecNumER10UErrorCode:
.LFB3659:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L1285
	ret
	.p2align 4,,10
	.p2align 3
.L1285:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK6icu_676number4impl6DecNum10isNegativeEv@PLT
	testb	%al, %al
	jne	.L1286
	movq	%rbx, %rdi
	call	_ZNK6icu_676number4impl6DecNum6isZeroEv@PLT
	testb	%al, %al
	je	.L1287
.L1219:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1286:
	.cfi_restore_state
	orb	$1, 20(%r12)
	movq	%rbx, %rdi
	call	_ZNK6icu_676number4impl6DecNum6isZeroEv@PLT
	testb	%al, %al
	jne	.L1219
.L1287:
	movq	(%rbx), %rbx
	movl	(%rbx), %esi
	cmpl	$16, %esi
	jg	.L1288
	testl	%esi, %esi
	jle	.L1231
	movzbl	9(%rbx), %eax
	cmpl	$1, %esi
	je	.L1228
	movzbl	10(%rbx), %edx
	salq	$4, %rdx
	orq	%rdx, %rax
	cmpl	$2, %esi
	je	.L1228
	movzbl	11(%rbx), %edx
	salq	$8, %rdx
	orq	%rdx, %rax
	cmpl	$3, %esi
	je	.L1228
	movzbl	12(%rbx), %edx
	salq	$12, %rdx
	orq	%rdx, %rax
	cmpl	$4, %esi
	je	.L1228
	movzbl	13(%rbx), %edx
	salq	$16, %rdx
	orq	%rdx, %rax
	cmpl	$5, %esi
	je	.L1228
	movzbl	14(%rbx), %edx
	salq	$20, %rdx
	orq	%rdx, %rax
	cmpl	$6, %esi
	je	.L1228
	movzbl	15(%rbx), %edx
	salq	$24, %rdx
	orq	%rdx, %rax
	cmpl	$7, %esi
	je	.L1228
	movzbl	16(%rbx), %edx
	salq	$28, %rdx
	orq	%rdx, %rax
	cmpl	$8, %esi
	je	.L1228
	movzbl	17(%rbx), %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	cmpl	$9, %esi
	je	.L1228
	movzbl	18(%rbx), %edx
	salq	$36, %rdx
	orq	%rdx, %rax
	cmpl	$10, %esi
	je	.L1228
	movzbl	19(%rbx), %edx
	salq	$40, %rdx
	orq	%rdx, %rax
	cmpl	$11, %esi
	je	.L1228
	movzbl	20(%rbx), %edx
	salq	$44, %rdx
	orq	%rdx, %rax
	cmpl	$12, %esi
	je	.L1228
	movzbl	21(%rbx), %edx
	salq	$48, %rdx
	orq	%rdx, %rax
	cmpl	$13, %esi
	je	.L1228
	movzbl	22(%rbx), %edx
	salq	$52, %rdx
	orq	%rdx, %rax
	cmpl	$14, %esi
	je	.L1228
	movzbl	23(%rbx), %edx
	salq	$56, %rdx
	orq	%rdx, %rax
	cmpl	$15, %esi
	je	.L1228
	movzbl	24(%rbx), %edx
	salq	$60, %rdx
	orq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L1228:
	movq	%rax, 48(%r12)
.L1226:
	movl	4(%rbx), %eax
	movq	%r12, %rdi
	movl	%eax, 12(%r12)
	movl	(%rbx), %eax
	movl	%eax, 16(%r12)
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	.p2align 4,,10
	.p2align 3
.L1288:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L1226
	.p2align 4,,10
	.p2align 3
.L1225:
	movzbl	9(%rbx,%rax), %ecx
	movq	48(%r12), %rdx
	movb	%cl, (%rdx,%rax)
	addq	$1, %rax
	cmpl	%eax, (%rbx)
	jg	.L1225
	jmp	.L1226
.L1231:
	xorl	%eax, %eax
	jmp	.L1228
	.cfi_endproc
.LFE3659:
	.size	_ZN6icu_676number4impl15DecimalQuantity12_setToDecNumERKNS1_6DecNumER10UErrorCode, .-_ZN6icu_676number4impl15DecimalQuantity12_setToDecNumERKNS1_6DecNumER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity9setToLongEl
	.type	_ZN6icu_676number4impl15DecimalQuantity9setToLongEl, @function
_ZN6icu_676number4impl15DecimalQuantity9setToLongEl:
.LFB3652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	cmpb	$0, 64(%rdi)
	movq	%rdi, %r12
	jne	.L1299
.L1290:
	movq	$0, 48(%r12)
	xorl	%eax, %eax
	movw	%ax, 20(%r12)
	movabsq	$-9223372036854775807, %rax
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	movl	$0, 44(%r12)
	movq	$0, 12(%r12)
	cmpq	%rax, %r13
	jb	.L1291
	movb	$1, 20(%r12)
	negq	%r13
.L1292:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity10_setToLongEl
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1291:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L1292
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1299:
	.cfi_restore_state
	movq	48(%rdi), %rdi
	call	uprv_free_67@PLT
	movb	$0, 64(%r12)
	jmp	.L1290
	.cfi_endproc
.LFE3652:
	.size	_ZN6icu_676number4impl15DecimalQuantity9setToLongEl, .-_ZN6icu_676number4impl15DecimalQuantity9setToLongEl
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd
	.type	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd, @function
_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd:
.LFB3654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	cmpb	$0, 64(%rdi)
	jne	.L1313
.L1301:
	movmskpd	%xmm0, %eax
	movb	$0, 21(%r12)
	movq	$0, 48(%r12)
	movq	$0, 12(%r12)
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	movl	$0, 44(%r12)
	testb	$1, %al
	jne	.L1302
	ucomisd	%xmm0, %xmm0
	movb	$0, 20(%r12)
	movl	$2, %eax
	movl	$4, %edx
	jp	.L1314
.L1304:
	movsd	.LC13(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC11(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L1315
	ucomisd	.LC0(%rip), %xmm0
	jp	.L1310
	jne	.L1310
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1302:
	.cfi_restore_state
	movb	$1, 20(%r12)
	movl	$3, %eax
	movl	$5, %edx
	xorpd	.LC12(%rip), %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.L1304
.L1314:
	movb	%dl, 20(%r12)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1315:
	.cfi_restore_state
	movb	%al, 20(%r12)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1313:
	.cfi_restore_state
	movq	48(%rdi), %rdi
	movsd	%xmm0, -24(%rbp)
	call	uprv_free_67@PLT
	movb	$0, 64(%r12)
	movsd	-24(%rbp), %xmm0
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1310:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity16_setToDoubleFastEd
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3654:
	.size	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd, .-_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity11setToDecNumERKNS1_6DecNumER10UErrorCode
	.type	_ZN6icu_676number4impl15DecimalQuantity11setToDecNumERKNS1_6DecNumER10UErrorCode, @function
_ZN6icu_676number4impl15DecimalQuantity11setToDecNumERKNS1_6DecNumER10UErrorCode:
.LFB3658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	cmpb	$0, 64(%rdi)
	jne	.L1380
.L1317:
	movq	$0, 48(%r12)
	xorl	%ecx, %ecx
	movw	%cx, 20(%r12)
	movl	(%rbx), %esi
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	movl	$0, 44(%r12)
	movq	$0, 12(%r12)
	testl	%esi, %esi
	jle	.L1381
.L1319:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1381:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK6icu_676number4impl6DecNum10isNegativeEv@PLT
	testb	%al, %al
	je	.L1320
	orb	$1, 20(%r12)
.L1320:
	movq	%r13, %rdi
	call	_ZNK6icu_676number4impl6DecNum6isZeroEv@PLT
	testb	%al, %al
	jne	.L1319
	movq	0(%r13), %rbx
	movl	(%rbx), %esi
	cmpl	$16, %esi
	jg	.L1382
	testl	%esi, %esi
	jle	.L1329
	movzbl	9(%rbx), %eax
	cmpl	$1, %esi
	je	.L1326
	movzbl	10(%rbx), %edx
	salq	$4, %rdx
	orq	%rdx, %rax
	cmpl	$2, %esi
	je	.L1326
	movzbl	11(%rbx), %edx
	salq	$8, %rdx
	orq	%rdx, %rax
	cmpl	$3, %esi
	je	.L1326
	movzbl	12(%rbx), %edx
	salq	$12, %rdx
	orq	%rdx, %rax
	cmpl	$4, %esi
	je	.L1326
	movzbl	13(%rbx), %edx
	salq	$16, %rdx
	orq	%rdx, %rax
	cmpl	$5, %esi
	je	.L1326
	movzbl	14(%rbx), %edx
	salq	$20, %rdx
	orq	%rdx, %rax
	cmpl	$6, %esi
	je	.L1326
	movzbl	15(%rbx), %edx
	salq	$24, %rdx
	orq	%rdx, %rax
	cmpl	$7, %esi
	je	.L1326
	movzbl	16(%rbx), %edx
	salq	$28, %rdx
	orq	%rdx, %rax
	cmpl	$8, %esi
	je	.L1326
	movzbl	17(%rbx), %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	cmpl	$9, %esi
	je	.L1326
	movzbl	18(%rbx), %edx
	salq	$36, %rdx
	orq	%rdx, %rax
	cmpl	$10, %esi
	je	.L1326
	movzbl	19(%rbx), %edx
	salq	$40, %rdx
	orq	%rdx, %rax
	cmpl	$11, %esi
	je	.L1326
	movzbl	20(%rbx), %edx
	salq	$44, %rdx
	orq	%rdx, %rax
	cmpl	$12, %esi
	je	.L1326
	movzbl	21(%rbx), %edx
	salq	$48, %rdx
	orq	%rdx, %rax
	cmpl	$13, %esi
	je	.L1326
	movzbl	22(%rbx), %edx
	salq	$52, %rdx
	orq	%rdx, %rax
	cmpl	$14, %esi
	je	.L1326
	movzbl	23(%rbx), %edx
	salq	$56, %rdx
	orq	%rdx, %rax
	cmpl	$15, %esi
	je	.L1326
	movzbl	24(%rbx), %edx
	salq	$60, %rdx
	orq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L1326:
	movq	%rax, 48(%r12)
.L1324:
	movl	4(%rbx), %eax
	movq	%r12, %rdi
	movl	%eax, 12(%r12)
	movl	(%rbx), %eax
	movl	%eax, 16(%r12)
	call	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1380:
	.cfi_restore_state
	movq	48(%rdi), %rdi
	call	uprv_free_67@PLT
	movb	$0, 64(%r12)
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1382:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L1324
	.p2align 4,,10
	.p2align 3
.L1323:
	movzbl	9(%rbx,%rax), %ecx
	movq	48(%r12), %rdx
	movb	%cl, (%rdx,%rax)
	addq	$1, %rax
	cmpl	%eax, (%rbx)
	jg	.L1323
	jmp	.L1324
.L1329:
	xorl	%eax, %eax
	jmp	.L1326
	.cfi_endproc
.LFE3658:
	.size	_ZN6icu_676number4impl15DecimalQuantity11setToDecNumERKNS1_6DecNumER10UErrorCode, .-_ZN6icu_676number4impl15DecimalQuantity11setToDecNumERKNS1_6DecNumER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity14setToDecNumberENS_11StringPieceER10UErrorCode
	.type	_ZN6icu_676number4impl15DecimalQuantity14setToDecNumberENS_11StringPieceER10UErrorCode, @function
_ZN6icu_676number4impl15DecimalQuantity14setToDecNumberENS_11StringPieceER10UErrorCode:
.LFB3657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 64(%rdi)
	jne	.L1449
.L1384:
	xorl	%ecx, %ecx
	leaq	-160(%rbp), %r15
	movq	$0, 48(%r12)
	movw	%cx, 20(%r12)
	movq	%r15, %rdi
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	movl	$0, 44(%r12)
	movq	$0, 12(%r12)
	call	_ZN6icu_676number4impl6DecNumC1Ev@PLT
	movq	%r14, %rsi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl6DecNum5setToENS_11StringPieceER10UErrorCode@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L1450
.L1386:
	cmpb	$0, -148(%rbp)
	jne	.L1451
.L1395:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1452
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1450:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl6DecNum10isNegativeEv@PLT
	testb	%al, %al
	je	.L1387
	orb	$1, 20(%r12)
.L1387:
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl6DecNum6isZeroEv@PLT
	testb	%al, %al
	jne	.L1386
	movq	-160(%rbp), %rbx
	movl	(%rbx), %esi
	cmpl	$16, %esi
	jg	.L1453
	testl	%esi, %esi
	jle	.L1398
	movzbl	9(%rbx), %eax
	cmpl	$1, %esi
	je	.L1393
	movzbl	10(%rbx), %edx
	salq	$4, %rdx
	orq	%rdx, %rax
	cmpl	$2, %esi
	je	.L1393
	movzbl	11(%rbx), %edx
	salq	$8, %rdx
	orq	%rdx, %rax
	cmpl	$3, %esi
	je	.L1393
	movzbl	12(%rbx), %edx
	salq	$12, %rdx
	orq	%rdx, %rax
	cmpl	$4, %esi
	je	.L1393
	movzbl	13(%rbx), %edx
	salq	$16, %rdx
	orq	%rdx, %rax
	cmpl	$5, %esi
	je	.L1393
	movzbl	14(%rbx), %edx
	salq	$20, %rdx
	orq	%rdx, %rax
	cmpl	$6, %esi
	je	.L1393
	movzbl	15(%rbx), %edx
	salq	$24, %rdx
	orq	%rdx, %rax
	cmpl	$7, %esi
	je	.L1393
	movzbl	16(%rbx), %edx
	salq	$28, %rdx
	orq	%rdx, %rax
	cmpl	$8, %esi
	je	.L1393
	movzbl	17(%rbx), %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	cmpl	$9, %esi
	je	.L1393
	movzbl	18(%rbx), %edx
	salq	$36, %rdx
	orq	%rdx, %rax
	cmpl	$10, %esi
	je	.L1393
	movzbl	19(%rbx), %edx
	salq	$40, %rdx
	orq	%rdx, %rax
	cmpl	$11, %esi
	je	.L1393
	movzbl	20(%rbx), %edx
	salq	$44, %rdx
	orq	%rdx, %rax
	cmpl	$12, %esi
	je	.L1393
	movzbl	21(%rbx), %edx
	salq	$48, %rdx
	orq	%rdx, %rax
	cmpl	$13, %esi
	je	.L1393
	movzbl	22(%rbx), %edx
	salq	$52, %rdx
	orq	%rdx, %rax
	cmpl	$14, %esi
	je	.L1393
	movzbl	23(%rbx), %edx
	salq	$56, %rdx
	orq	%rdx, %rax
	cmpl	$15, %esi
	je	.L1393
	movzbl	24(%rbx), %edx
	salq	$60, %rdx
	orq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L1393:
	movq	%rax, 48(%r12)
.L1391:
	movl	4(%rbx), %eax
	movq	%r12, %rdi
	movl	%eax, 12(%r12)
	movl	(%rbx), %eax
	movl	%eax, 16(%r12)
	call	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	cmpb	$0, -148(%rbp)
	je	.L1395
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	-160(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1449:
	movq	48(%rdi), %rdi
	call	uprv_free_67@PLT
	movb	$0, 64(%r12)
	jmp	.L1384
	.p2align 4,,10
	.p2align 3
.L1453:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L1391
	.p2align 4,,10
	.p2align 3
.L1390:
	movzbl	9(%rbx,%rax), %ecx
	movq	48(%r12), %rdx
	movb	%cl, (%rdx,%rax)
	addq	$1, %rax
	cmpl	%eax, (%rbx)
	jg	.L1390
	jmp	.L1391
.L1398:
	xorl	%eax, %eax
	jmp	.L1393
.L1452:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3657:
	.size	_ZN6icu_676number4impl15DecimalQuantity14setToDecNumberENS_11StringPieceER10UErrorCode, .-_ZN6icu_676number4impl15DecimalQuantity14setToDecNumberENS_11StringPieceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity10multiplyByERKNS1_6DecNumER10UErrorCode
	.type	_ZN6icu_676number4impl15DecimalQuantity10multiplyByERKNS1_6DecNumER10UErrorCode, @function
_ZN6icu_676number4impl15DecimalQuantity10multiplyByERKNS1_6DecNumER10UErrorCode:
.LFB3631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	16(%rdi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jne	.L1525
.L1454:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1526
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1525:
	.cfi_restore_state
	leaq	-144(%rbp), %r14
	movq	%rdx, %rbx
	movq	%rdi, %r12
	movq	%rsi, %r13
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl6DecNumC1Ev@PLT
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZNK6icu_676number4impl15DecimalQuantity8toDecNumERNS1_6DecNumER10UErrorCode
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.L1527
.L1457:
	cmpb	$0, -132(%rbp)
	je	.L1454
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L1527:
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl6DecNum10multiplyByERKS2_R10UErrorCode@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L1457
	cmpb	$0, 64(%r12)
	jne	.L1528
	movq	$0, 48(%r12)
	xorl	%eax, %eax
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	movl	$0, 44(%r12)
	movq	$0, 12(%r12)
	movw	%ax, 20(%r12)
.L1459:
	movq	%r14, %rdi
	call	_ZNK6icu_676number4impl6DecNum10isNegativeEv@PLT
	testb	%al, %al
	je	.L1461
	orb	$1, 20(%r12)
.L1461:
	movq	%r14, %rdi
	call	_ZNK6icu_676number4impl6DecNum6isZeroEv@PLT
	testb	%al, %al
	jne	.L1457
	movq	-144(%rbp), %rbx
	movl	(%rbx), %esi
	cmpl	$16, %esi
	jg	.L1529
	testl	%esi, %esi
	jle	.L1472
	movzbl	9(%rbx), %eax
	cmpl	$1, %esi
	je	.L1467
	movzbl	10(%rbx), %edx
	salq	$4, %rdx
	orq	%rdx, %rax
	cmpl	$2, %esi
	je	.L1467
	movzbl	11(%rbx), %edx
	salq	$8, %rdx
	orq	%rdx, %rax
	cmpl	$3, %esi
	je	.L1467
	movzbl	12(%rbx), %edx
	salq	$12, %rdx
	orq	%rdx, %rax
	cmpl	$4, %esi
	je	.L1467
	movzbl	13(%rbx), %edx
	salq	$16, %rdx
	orq	%rdx, %rax
	cmpl	$5, %esi
	je	.L1467
	movzbl	14(%rbx), %edx
	salq	$20, %rdx
	orq	%rdx, %rax
	cmpl	$6, %esi
	je	.L1467
	movzbl	15(%rbx), %edx
	salq	$24, %rdx
	orq	%rdx, %rax
	cmpl	$7, %esi
	je	.L1467
	movzbl	16(%rbx), %edx
	salq	$28, %rdx
	orq	%rdx, %rax
	cmpl	$8, %esi
	je	.L1467
	movzbl	17(%rbx), %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	cmpl	$9, %esi
	je	.L1467
	movzbl	18(%rbx), %edx
	salq	$36, %rdx
	orq	%rdx, %rax
	cmpl	$10, %esi
	je	.L1467
	movzbl	19(%rbx), %edx
	salq	$40, %rdx
	orq	%rdx, %rax
	cmpl	$11, %esi
	je	.L1467
	movzbl	20(%rbx), %edx
	salq	$44, %rdx
	orq	%rdx, %rax
	cmpl	$12, %esi
	je	.L1467
	movzbl	21(%rbx), %edx
	salq	$48, %rdx
	orq	%rdx, %rax
	cmpl	$13, %esi
	je	.L1467
	movzbl	22(%rbx), %edx
	salq	$52, %rdx
	orq	%rdx, %rax
	cmpl	$14, %esi
	je	.L1467
	movzbl	23(%rbx), %edx
	salq	$56, %rdx
	orq	%rdx, %rax
	cmpl	$15, %esi
	je	.L1467
	movzbl	24(%rbx), %edx
	salq	$60, %rdx
	orq	%rdx, %rax
.L1467:
	movq	%rax, 48(%r12)
.L1465:
	movl	4(%rbx), %eax
	movq	%r12, %rdi
	movl	%eax, 12(%r12)
	movl	(%rbx), %eax
	movl	%eax, 16(%r12)
	call	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1528:
	movq	48(%r12), %rdi
	call	uprv_free_67@PLT
	movl	(%rbx), %eax
	xorl	%ecx, %ecx
	movb	$0, 64(%r12)
	movq	$0, 48(%r12)
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	movl	$0, 44(%r12)
	movq	$0, 12(%r12)
	movw	%cx, 20(%r12)
	testl	%eax, %eax
	jg	.L1457
	jmp	.L1459
	.p2align 4,,10
	.p2align 3
.L1529:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L1465
	.p2align 4,,10
	.p2align 3
.L1464:
	movzbl	9(%rbx,%rax), %ecx
	movq	48(%r12), %rdx
	movb	%cl, (%rdx,%rax)
	addq	$1, %rax
	cmpl	%eax, (%rbx)
	jg	.L1464
	jmp	.L1465
.L1472:
	xorl	%eax, %eax
	jmp	.L1467
.L1526:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3631:
	.size	_ZN6icu_676number4impl15DecimalQuantity10multiplyByERKNS1_6DecNumER10UErrorCode, .-_ZN6icu_676number4impl15DecimalQuantity10multiplyByERKNS1_6DecNumER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity8divideByERKNS1_6DecNumER10UErrorCode
	.type	_ZN6icu_676number4impl15DecimalQuantity8divideByERKNS1_6DecNumER10UErrorCode, @function
_ZN6icu_676number4impl15DecimalQuantity8divideByERKNS1_6DecNumER10UErrorCode:
.LFB3632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	16(%rdi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jne	.L1601
.L1530:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1602
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1601:
	.cfi_restore_state
	leaq	-144(%rbp), %r14
	movq	%rdx, %rbx
	movq	%rdi, %r12
	movq	%rsi, %r13
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl6DecNumC1Ev@PLT
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZNK6icu_676number4impl15DecimalQuantity8toDecNumERNS1_6DecNumER10UErrorCode
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.L1603
.L1533:
	cmpb	$0, -132(%rbp)
	je	.L1530
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L1530
	.p2align 4,,10
	.p2align 3
.L1603:
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl6DecNum8divideByERKS2_R10UErrorCode@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L1533
	cmpb	$0, 64(%r12)
	jne	.L1604
	movq	$0, 48(%r12)
	xorl	%eax, %eax
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	movl	$0, 44(%r12)
	movq	$0, 12(%r12)
	movw	%ax, 20(%r12)
.L1535:
	movq	%r14, %rdi
	call	_ZNK6icu_676number4impl6DecNum10isNegativeEv@PLT
	testb	%al, %al
	je	.L1537
	orb	$1, 20(%r12)
.L1537:
	movq	%r14, %rdi
	call	_ZNK6icu_676number4impl6DecNum6isZeroEv@PLT
	testb	%al, %al
	jne	.L1533
	movq	-144(%rbp), %rbx
	movl	(%rbx), %esi
	cmpl	$16, %esi
	jg	.L1605
	testl	%esi, %esi
	jle	.L1548
	movzbl	9(%rbx), %eax
	cmpl	$1, %esi
	je	.L1543
	movzbl	10(%rbx), %edx
	salq	$4, %rdx
	orq	%rdx, %rax
	cmpl	$2, %esi
	je	.L1543
	movzbl	11(%rbx), %edx
	salq	$8, %rdx
	orq	%rdx, %rax
	cmpl	$3, %esi
	je	.L1543
	movzbl	12(%rbx), %edx
	salq	$12, %rdx
	orq	%rdx, %rax
	cmpl	$4, %esi
	je	.L1543
	movzbl	13(%rbx), %edx
	salq	$16, %rdx
	orq	%rdx, %rax
	cmpl	$5, %esi
	je	.L1543
	movzbl	14(%rbx), %edx
	salq	$20, %rdx
	orq	%rdx, %rax
	cmpl	$6, %esi
	je	.L1543
	movzbl	15(%rbx), %edx
	salq	$24, %rdx
	orq	%rdx, %rax
	cmpl	$7, %esi
	je	.L1543
	movzbl	16(%rbx), %edx
	salq	$28, %rdx
	orq	%rdx, %rax
	cmpl	$8, %esi
	je	.L1543
	movzbl	17(%rbx), %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	cmpl	$9, %esi
	je	.L1543
	movzbl	18(%rbx), %edx
	salq	$36, %rdx
	orq	%rdx, %rax
	cmpl	$10, %esi
	je	.L1543
	movzbl	19(%rbx), %edx
	salq	$40, %rdx
	orq	%rdx, %rax
	cmpl	$11, %esi
	je	.L1543
	movzbl	20(%rbx), %edx
	salq	$44, %rdx
	orq	%rdx, %rax
	cmpl	$12, %esi
	je	.L1543
	movzbl	21(%rbx), %edx
	salq	$48, %rdx
	orq	%rdx, %rax
	cmpl	$13, %esi
	je	.L1543
	movzbl	22(%rbx), %edx
	salq	$52, %rdx
	orq	%rdx, %rax
	cmpl	$14, %esi
	je	.L1543
	movzbl	23(%rbx), %edx
	salq	$56, %rdx
	orq	%rdx, %rax
	cmpl	$15, %esi
	je	.L1543
	movzbl	24(%rbx), %edx
	salq	$60, %rdx
	orq	%rdx, %rax
.L1543:
	movq	%rax, 48(%r12)
.L1541:
	movl	4(%rbx), %eax
	movq	%r12, %rdi
	movl	%eax, 12(%r12)
	movl	(%rbx), %eax
	movl	%eax, 16(%r12)
	call	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	jmp	.L1533
	.p2align 4,,10
	.p2align 3
.L1604:
	movq	48(%r12), %rdi
	call	uprv_free_67@PLT
	movl	(%rbx), %eax
	xorl	%ecx, %ecx
	movb	$0, 64(%r12)
	movq	$0, 48(%r12)
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	movl	$0, 44(%r12)
	movq	$0, 12(%r12)
	movw	%cx, 20(%r12)
	testl	%eax, %eax
	jg	.L1533
	jmp	.L1535
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L1541
	.p2align 4,,10
	.p2align 3
.L1540:
	movzbl	9(%rbx,%rax), %ecx
	movq	48(%r12), %rdx
	movb	%cl, (%rdx,%rax)
	addq	$1, %rax
	cmpl	%eax, (%rbx)
	jg	.L1540
	jmp	.L1541
.L1548:
	xorl	%eax, %eax
	jmp	.L1543
.L1602:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3632:
	.size	_ZN6icu_676number4impl15DecimalQuantity8divideByERKNS1_6DecNumER10UErrorCode, .-_ZN6icu_676number4impl15DecimalQuantity8divideByERKNS1_6DecNumER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity16roundToIncrementEd25UNumberFormatRoundingModeR10UErrorCode
	.type	_ZN6icu_676number4impl15DecimalQuantity16roundToIncrementEd25UNumberFormatRoundingModeR10UErrorCode, @function
_ZN6icu_676number4impl15DecimalQuantity16roundToIncrementEd25UNumberFormatRoundingModeR10UErrorCode:
.LFB3627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-144(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	addq	$-128, %rsp
	movsd	%xmm0, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676number4impl6DecNumC1Ev@PLT
	movsd	-152(%rbp), %xmm0
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl6DecNum5setToEdR10UErrorCode@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L1615
.L1607:
	cmpb	$0, -132(%rbp)
	jne	.L1616
.L1606:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1617
	subq	$-128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1615:
	.cfi_restore_state
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity8divideByERKNS1_6DecNumER10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L1607
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModebR10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1607
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity10multiplyByERKNS1_6DecNumER10UErrorCode
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L1606
.L1617:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3627:
	.size	_ZN6icu_676number4impl15DecimalQuantity16roundToIncrementEd25UNumberFormatRoundingModeR10UErrorCode, .-_ZN6icu_676number4impl15DecimalQuantity16roundToIncrementEd25UNumberFormatRoundingModeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity13roundToNickelEi25UNumberFormatRoundingModeR10UErrorCode
	.type	_ZN6icu_676number4impl15DecimalQuantity13roundToNickelEi25UNumberFormatRoundingModeR10UErrorCode, @function
_ZN6icu_676number4impl15DecimalQuantity13roundToNickelEi25UNumberFormatRoundingModeR10UErrorCode:
.LFB3667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movl	12(%rdi), %eax
	movzbl	64(%rdi), %edx
	subl	%eax, %ebx
	movl	%eax, %ecx
	shrl	$31, %ecx
	cmpl	%ebx, %esi
	setg	%sil
	andb	%cl, %sil
	jne	.L1887
	movl	%ebx, %r8d
	testl	%eax, %eax
	jle	.L1623
	cmpl	%ebx, %r13d
	jl	.L1888
.L1623:
	testb	%dl, %dl
	je	.L1626
	testl	%ebx, %ebx
	js	.L1624
	movl	16(%r12), %edi
	cmpl	%edi, %ebx
	jge	.L1889
	movq	48(%r12), %rcx
	movslq	%ebx, %rax
	movzbl	(%rcx,%rax), %r9d
	testl	%ebx, %ebx
	je	.L1630
	leal	-1(%rbx), %r10d
	cmpl	%r10d, %ebx
	jl	.L1724
.L1632:
	cmpl	%r10d, %edi
	movl	%r10d, %eax
	setle	%dl
	shrl	$31, %eax
	xorl	%ecx, %ecx
	orb	%al, %dl
	jne	.L1631
	movq	48(%r12), %rdx
	movslq	%r10d, %rax
	movzbl	(%rdx,%rax), %ecx
	movl	$1, %edx
.L1631:
	cmpb	$0, 21(%r12)
	jne	.L1638
.L1896:
	cmpb	$7, %r9b
	setne	%r11b
	cmpb	$2, %r9b
	setne	%al
	andb	%al, %r11b
	jne	.L1890
	cmpb	$4, %cl
	jle	.L1881
	cmpb	$5, %cl
	je	.L1891
.L1769:
	movl	$1, %r11d
.L1885:
	movl	$3, %eax
.L1640:
	leal	-2(%r9), %edx
	movl	$1, %esi
	cmpb	$5, %dl
	ja	.L1636
.L1884:
	cmpb	$7, %r9b
	sete	%sil
	andl	%r11d, %esi
.L1636:
	movzbl	20(%r12), %r10d
	cmpl	$6, %r14d
	ja	.L1679
	leaq	.L1681(%rip), %rcx
	movslq	(%rcx,%r14,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L1681:
	.long	.L1687-.L1681
	.long	.L1686-.L1681
	.long	.L1777-.L1681
	.long	.L1776-.L1681
	.long	.L1683-.L1681
	.long	.L1682-.L1681
	.long	.L1680-.L1681
	.text
	.p2align 4,,10
	.p2align 3
.L1626:
	cmpl	$15, %ebx
	ja	.L1892
	movq	48(%r12), %r9
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %r9
	andl	$15, %r9d
	testl	%ebx, %ebx
	jle	.L1630
.L1874:
	movl	16(%r12), %edi
.L1628:
	testl	%edi, %edi
	jne	.L1893
.L1618:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1887:
	.cfi_restore_state
	movl	16(%rdi), %edi
	testb	%dl, %dl
	je	.L1894
	movl	$2147483647, %ebx
	movl	$2147483647, %r8d
.L1620:
	testl	%edi, %edi
	je	.L1618
	leal	-1(%rbx), %r10d
	xorl	%r9d, %r9d
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1894:
	testl	%edi, %edi
	je	.L1618
	movzbl	21(%r12), %r9d
	movl	$2147483647, %r8d
	testb	%r9b, %r9b
	je	.L1726
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$2147483646, %r10d
	movl	$2147483645, %ebx
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1630:
	cmpb	$0, 21(%r12)
	jne	.L1874
	cmpb	$5, %r9b
	je	.L1618
	testb	%r9b, %r9b
	je	.L1618
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L1889:
	testl	%ebx, %ebx
	jne	.L1722
	cmpb	$0, 21(%r12)
	je	.L1618
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1680:
	cmpl	$1, %eax
	je	.L1777
	subl	$2, %eax
	cmpl	$1, %eax
	ja	.L1679
.L1776:
	xorl	%esi, %esi
.L1684:
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L1618
	movzbl	64(%r12), %eax
	cmpl	%r8d, %edi
	jg	.L1688
	testb	%al, %al
	jne	.L1895
.L1689:
	movq	$0, 48(%r12)
	movl	$0, 16(%r12)
	movb	$0, 21(%r12)
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	movl	$0, 44(%r12)
	movl	%r13d, 12(%r12)
	cmpb	$4, %r9b
	jg	.L1867
	testb	%sil, %sil
	jne	.L1690
	cmpb	$4, %r9b
	jg	.L1867
.L1703:
	movq	48(%r12), %rax
	andq	$-16, %rax
	orq	$5, %rax
	movq	%rax, 48(%r12)
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L1682:
	xorl	%esi, %esi
	cmpl	$3, %eax
	je	.L1684
	testl	%eax, %eax
	jle	.L1679
	.p2align 4,,10
	.p2align 3
.L1777:
	movl	$1, %esi
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L1893:
	leal	-1(%rbx), %r10d
	cmpl	%r10d, %r8d
	jl	.L1724
	testb	%dl, %dl
	jne	.L1632
	xorl	%ecx, %ecx
	cmpl	$15, %r10d
	ja	.L1631
	movq	48(%r12), %rax
	leal	0(,%r10,4), %ecx
	shrq	%cl, %rax
	movq	%rax, %rcx
	andl	$15, %ecx
	cmpb	$0, 21(%r12)
	je	.L1896
.L1638:
	subl	$2, %ebx
	cmpl	%r8d, %ebx
	jg	.L1897
.L1648:
	leal	-14(%rdi), %esi
	xorl	%edi, %edi
	movb	%r9b, -58(%rbp)
	movb	%cl, -57(%rbp)
	movl	%r10d, -56(%rbp)
	movl	%r8d, -52(%rbp)
	call	uprv_max_67@PLT
	movzbl	-57(%rbp), %ecx
	movl	-52(%rbp), %r8d
	movl	-56(%rbp), %r10d
	movzbl	-58(%rbp), %r9d
	testb	%cl, %cl
	jne	.L1649
	cmpb	$5, %r9b
	movl	16(%r12), %edi
	sete	%cl
	testb	%r9b, %r9b
	sete	%dl
	orb	%cl, %dl
	je	.L1650
	cmpl	%ebx, %eax
	jg	.L1880
	movzbl	64(%r12), %r11d
	testb	%r11b, %r11b
	je	.L1654
	movslq	%ebx, %rdx
	jmp	.L1656
	.p2align 4,,10
	.p2align 3
.L1655:
	subl	$1, %ebx
	subq	$1, %rdx
	cmpl	%ebx, %eax
	jg	.L1898
.L1656:
	testl	%ebx, %ebx
	js	.L1655
	cmpl	%edi, %ebx
	jge	.L1655
	movq	48(%r12), %rcx
	cmpb	$0, (%rcx,%rdx)
	je	.L1655
.L1750:
	movl	%r11d, %edx
	movl	$1, %eax
	xorl	%r11d, %r11d
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1653:
	subl	$1, %ebx
	cmpl	%ebx, %eax
	jg	.L1879
.L1654:
	cmpl	$15, %ebx
	ja	.L1653
	movq	48(%r12), %rsi
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rsi
	movq	%rsi, %rcx
	andl	$15, %ecx
	je	.L1653
.L1875:
	movl	$1, %eax
.L1651:
	leal	-14(%rdi), %ecx
	cmpl	$3, %r14d
	jbe	.L1674
	cmpl	%r10d, %ecx
	jg	.L1675
	cmpl	$2, %eax
	jne	.L1676
.L1675:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity23convertToAccurateDoubleEv
	addq	$24, %rsp
	movq	%r15, %r8
	movl	%r14d, %edx
	popq	%rbx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$1, %ecx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModebR10UErrorCode
	.p2align 4,,10
	.p2align 3
.L1892:
	.cfi_restore_state
	testl	%ebx, %ebx
	jg	.L1899
.L1625:
	cmpb	$0, 21(%r12)
	je	.L1618
	movl	16(%r12), %edi
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L1674:
	cmpl	%r10d, %ecx
	jg	.L1675
	testl	%eax, %eax
	js	.L1675
.L1676:
	movb	$0, 21(%r12)
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	testl	%r8d, %r8d
	jle	.L1900
.L1678:
	cmpl	$-1, %eax
	je	.L1768
	cmpl	$-2, %eax
	jne	.L1642
	jmp	.L1769
	.p2align 4,,10
	.p2align 3
.L1649:
	movl	16(%r12), %edi
	cmpb	$4, %cl
	je	.L1901
	cmpb	$5, %cl
	jne	.L1662
	cmpb	$2, %r9b
	sete	%r11b
	cmpb	$7, %r9b
	sete	%dl
	orb	%dl, %r11b
	je	.L1658
	cmpl	%ebx, %eax
	jg	.L1754
	movzbl	64(%r12), %edx
	testb	%dl, %dl
	je	.L1665
	movslq	%ebx, %rcx
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1666:
	subl	$1, %ebx
	subq	$1, %rcx
	cmpl	%ebx, %eax
	jg	.L1756
.L1667:
	testl	%ebx, %ebx
	js	.L1666
	cmpl	%ebx, %edi
	jle	.L1666
	movq	48(%r12), %rsi
	cmpb	$0, (%rsi,%rcx)
	je	.L1666
.L1763:
	movl	%edx, %r11d
	movl	$3, %eax
	xorl	%edx, %edx
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1768:
	xorl	%r11d, %r11d
.L1881:
	movl	$1, %edx
	movl	$1, %eax
.L1642:
	leal	-2(%r9), %ecx
	movl	$1, %esi
	cmpb	$5, %cl
	ja	.L1636
	cmpb	$2, %r9b
	sete	%sil
	andb	%dl, %sil
	je	.L1884
	movl	$2, %r9d
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1867:
	cmpb	$1, %sil
	je	.L1703
.L1701:
	movq	48(%r12), %rax
	movl	16(%r12), %edi
	andq	$-16, %rax
	orq	$9, %rax
	movq	%rax, 48(%r12)
.L1706:
	movq	48(%r12), %rax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1782
	movl	%eax, %edx
	shrb	$4, %dl
	cmpb	$9, %dl
	jne	.L1783
	movq	%rax, %rdx
	shrq	$8, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1784
	movq	%rax, %rdx
	shrq	$12, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1785
	movq	%rax, %rdx
	shrq	$16, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1786
	movq	%rax, %rdx
	shrq	$20, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1787
	movq	%rax, %rdx
	shrq	$24, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1788
	movq	%rax, %rdx
	shrq	$28, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1789
	movq	%rax, %rdx
	shrq	$32, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1790
	movq	%rax, %rdx
	shrq	$36, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1791
	movq	%rax, %rdx
	shrq	$40, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1792
	movq	%rax, %rdx
	shrq	$44, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1793
	movq	%rax, %rdx
	shrq	$48, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1794
	movq	%rax, %rdx
	shrq	$52, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1795
	movq	%rax, %rdx
	shrq	$56, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L1796
	movq	%rax, %rdx
	shrq	$60, %rdx
	cmpb	$9, %dl
	jne	.L1797
	movl	$64, %ecx
	movl	$16, %edx
	shrq	%cl, %rax
.L1717:
	addl	%edx, 12(%r12)
	movq	%rax, 48(%r12)
	movl	%edi, %eax
	subl	%edx, %eax
.L1713:
	movq	48(%r12), %rdx
	movq	%rdx, %rcx
	andq	$-16, %rdx
	andl	$15, %ecx
	addq	$1, %rcx
	orq	%rcx, %rdx
	movq	%rdx, 48(%r12)
.L1715:
	addl	$1, %eax
	movl	%eax, 16(%r12)
.L1886:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	.p2align 4,,10
	.p2align 3
.L1683:
	.cfi_restore_state
	cmpl	$2, %eax
	je	.L1684
	cmpl	$3, %eax
	je	.L1776
	cmpl	$1, %eax
	je	.L1777
.L1679:
	movl	$65809, (%r15)
	jmp	.L1618
.L1888:
	movl	$-2147483648, %ebx
	movl	$-2147483648, %r8d
	testb	%dl, %dl
	je	.L1625
	.p2align 4,,10
	.p2align 3
.L1624:
	cmpb	$0, 21(%r12)
	je	.L1618
	movl	16(%r12), %edi
	xorl	%r9d, %r9d
	movl	$1, %edx
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L1688:
	subl	%r8d, %edi
	testb	%al, %al
	jne	.L1778
	addl	%r8d, 12(%r12)
	leal	0(,%r8,4), %ecx
	shrq	%cl, 48(%r12)
	cmpb	$4, %r9b
	setle	%al
	movl	%edi, 16(%r12)
	andb	%sil, %al
	jne	.L1690
.L1699:
	cmpb	$4, %r9b
	jle	.L1704
.L1868:
	cmpb	$1, %sil
	je	.L1704
	testb	%al, %al
	je	.L1701
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movq	48(%r12), %rax
	movb	$9, (%rax)
	cmpb	$0, 64(%r12)
	movl	16(%r12), %edi
	je	.L1706
	xorl	%eax, %eax
.L1705:
	movl	%eax, %r8d
	cmpl	%eax, %edi
	jg	.L1902
.L1707:
	movl	%edi, %eax
	xorl	%edx, %edx
	movslq	%r8d, %rsi
	subl	%r8d, %eax
	testl	%eax, %eax
	jle	.L1903
.L1708:
	movq	48(%r12), %rax
	leaq	(%rax,%rdx), %rcx
	movzbl	(%rcx,%rsi), %ecx
	movb	%cl, (%rax,%rdx)
	movl	16(%r12), %edi
	leal	1(%rdx), %ecx
	addq	$1, %rdx
	movl	%edi, %eax
	subl	%r8d, %eax
	cmpl	%edx, %eax
	jg	.L1708
.L1711:
	movslq	%ecx, %rdx
	cmpl	%edi, %ecx
	jge	.L1904
	.p2align 4,,10
	.p2align 3
.L1712:
	movq	48(%r12), %rax
	movb	$0, (%rax,%rdx)
	movl	16(%r12), %eax
	addq	$1, %rdx
	cmpl	%eax, %edx
	jl	.L1712
	movzbl	64(%r12), %edx
	subl	%r8d, %eax
.L1710:
	addl	%r8d, 12(%r12)
	movl	%eax, 16(%r12)
	testb	%dl, %dl
	je	.L1713
	movl	$1, %ebx
	testl	%eax, %eax
	jle	.L1714
	movq	48(%r12), %rax
	movzbl	(%rax), %ebx
	addl	$1, %ebx
.L1714:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movq	48(%r12), %rax
	movb	%bl, (%rax)
	movl	16(%r12), %eax
	jmp	.L1715
	.p2align 4,,10
	.p2align 3
.L1686:
	movl	%r10d, %esi
	notl	%esi
	andl	$1, %esi
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L1687:
	movl	%r10d, %esi
	andl	$1, %esi
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L1664:
	subl	$1, %ebx
	cmpl	%ebx, %eax
	jg	.L1754
.L1665:
	cmpl	$15, %ebx
	ja	.L1664
	movq	48(%r12), %rsi
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rsi
	movq	%rsi, %rcx
	andl	$15, %ecx
	je	.L1664
	.p2align 4,,10
	.p2align 3
.L1878:
	movl	$3, %eax
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1900:
	cmpb	$5, %r9b
	je	.L1618
	testb	%r9b, %r9b
	jne	.L1678
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L1662:
	cmpb	$9, %cl
	jne	.L1668
	cmpb	$4, %r9b
	sete	%r11b
	cmpb	$9, %r9b
	sete	%dl
	orb	%dl, %r11b
	je	.L1669
	cmpl	%ebx, %eax
	jg	.L1760
	movzbl	64(%r12), %edx
	testb	%dl, %dl
	je	.L1671
	movslq	%ebx, %rcx
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1906:
	cmpl	%ebx, %edi
	jle	.L1763
	movq	48(%r12), %rsi
	cmpb	$9, (%rsi,%rcx)
	jne	.L1763
	subl	$1, %ebx
	subq	$1, %rcx
	cmpl	%ebx, %eax
	jg	.L1905
.L1672:
	testl	%ebx, %ebx
	jns	.L1906
	jmp	.L1763
	.p2align 4,,10
	.p2align 3
.L1907:
	movq	48(%r12), %rsi
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rsi
	movq	%rsi, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jne	.L1878
	subl	$1, %ebx
	cmpl	%ebx, %eax
	jg	.L1760
.L1671:
	cmpl	$15, %ebx
	ja	.L1878
	jmp	.L1907
	.p2align 4,,10
	.p2align 3
.L1726:
	movl	$1, %eax
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1778:
	xorl	%eax, %eax
	movslq	%r8d, %r10
	.p2align 4,,10
	.p2align 3
.L1693:
	movq	48(%r12), %rdx
	leal	1(%rax), %edi
	leaq	(%rdx,%rax), %rcx
	movzbl	(%rcx,%r10), %ecx
	movb	%cl, (%rdx,%rax)
	movl	16(%r12), %ecx
	addq	$1, %rax
	movl	%ecx, %edx
	subl	%r8d, %edx
	cmpl	%eax, %edx
	jg	.L1693
	movslq	%edi, %rax
	movl	%edi, %r10d
	subl	%eax, %r10d
	cmpl	%edi, %ecx
	jle	.L1908
	.p2align 4,,10
	.p2align 3
.L1698:
	movq	48(%r12), %rdx
	movb	$0, (%rdx,%rax)
	addq	$1, %rax
	movl	16(%r12), %edx
	leal	(%r10,%rax), %ecx
	cmpl	%ecx, %edx
	jg	.L1698
	movzbl	64(%r12), %eax
	subl	%r8d, %edx
.L1696:
	addl	%r8d, 12(%r12)
	movl	%edx, 16(%r12)
	cmpb	$4, %r9b
	jg	.L1868
	testb	%sil, %sil
	je	.L1699
	testb	%al, %al
	je	.L1690
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movq	48(%r12), %rax
	movb	$0, (%rax)
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1891:
	subl	$2, %ebx
	cmpl	%r8d, %ebx
	jg	.L1882
	testl	%ebx, %ebx
	js	.L1882
	testb	%dl, %dl
	je	.L1645
	movslq	%ebx, %rax
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L1646:
	subl	$1, %ebx
	subq	$1, %rax
	cmpl	$-1, %ebx
	je	.L1883
.L1647:
	cmpl	%ebx, %edi
	jle	.L1646
	movq	48(%r12), %rcx
	cmpb	$0, (%rcx,%rax)
	je	.L1646
	movl	%edx, %r11d
	movl	$3, %eax
	jmp	.L1640
	.p2align 4,,10
	.p2align 3
.L1644:
	subl	$1, %ebx
	cmpl	$-1, %ebx
	je	.L1909
.L1645:
	cmpl	$15, %ebx
	jg	.L1644
	movq	48(%r12), %rax
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rax
	testb	$15, %al
	je	.L1644
	jmp	.L1769
	.p2align 4,,10
	.p2align 3
.L1690:
	andq	$-16, 48(%r12)
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1902:
	movq	48(%r12), %rdx
	movzbl	(%rdx,%rax), %edx
	addq	$1, %rax
	cmpb	$9, %dl
	je	.L1705
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1901:
	cmpb	$2, %r9b
	sete	%cl
	cmpb	$7, %r9b
	sete	%dl
	orb	%cl, %dl
	je	.L1658
	cmpl	%ebx, %eax
	jg	.L1756
	movzbl	64(%r12), %r11d
	testb	%r11b, %r11b
	je	.L1660
	movslq	%ebx, %rdx
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1910:
	cmpl	%ebx, %edi
	jle	.L1750
	movq	48(%r12), %rcx
	cmpb	$9, (%rcx,%rdx)
	jne	.L1750
	subl	$1, %ebx
	subq	$1, %rdx
	cmpl	%ebx, %eax
	jg	.L1754
.L1661:
	testl	%ebx, %ebx
	jns	.L1910
	jmp	.L1750
	.p2align 4,,10
	.p2align 3
.L1911:
	movq	48(%r12), %rsi
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rsi
	movq	%rsi, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jne	.L1875
	subl	$1, %ebx
	cmpl	%ebx, %eax
	jg	.L1877
.L1660:
	cmpl	$15, %ebx
	ja	.L1875
	jmp	.L1911
	.p2align 4,,10
	.p2align 3
.L1890:
	cmpb	$1, %r9b
	jle	.L1728
	leal	-5(%r9), %eax
	cmpb	$1, %al
	ja	.L1885
	xorl	%esi, %esi
	movl	$1, %eax
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1899:
	movl	16(%r12), %edi
	xorl	%r9d, %r9d
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L1898:
	movl	%r11d, %edx
.L1880:
	xorl	%r11d, %r11d
.L1879:
	movl	$-1, %eax
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1650:
	cmpb	$7, %r9b
	setne	%r11b
	cmpb	$2, %r9b
	setne	%al
	andb	%al, %r11b
	jne	.L1658
.L1876:
	movl	$1, %edx
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1895:
	movq	48(%r12), %rdi
	movb	%r9b, -56(%rbp)
	movb	%sil, -52(%rbp)
	call	uprv_free_67@PLT
	movb	$0, 64(%r12)
	movzbl	-56(%rbp), %r9d
	movzbl	-52(%rbp), %esi
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1658:
	cmpb	$1, %r9b
	jle	.L1765
	leal	-5(%r9), %eax
	cmpb	$2, %al
	sbbl	%r11d, %r11d
	addl	$1, %r11d
	cmpb	$2, %al
	setb	%dl
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$3, %eax
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1722:
	xorl	%r9d, %r9d
	jmp	.L1628
.L1756:
	xorl	%r11d, %r11d
.L1877:
	movl	$2, %eax
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1754:
	movl	%r11d, %edx
	movl	$2, %eax
	xorl	%r11d, %r11d
	jmp	.L1651
.L1909:
	xorl	%r11d, %r11d
.L1882:
	movl	$1, %edx
.L1883:
	movl	$2, %eax
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1704:
	testb	%al, %al
	je	.L1703
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movq	48(%r12), %rax
	movb	$5, (%rax)
	jmp	.L1618
.L1669:
	cmpb	$2, %r9b
	setne	%dl
	cmpb	$7, %r9b
	setne	%al
	andb	%al, %dl
	jne	.L1658
	movl	$1, %r11d
	jmp	.L1878
.L1765:
	xorl	%r11d, %r11d
	jmp	.L1876
.L1903:
	xorl	%ecx, %ecx
	jmp	.L1711
.L1760:
	movl	%r11d, %edx
	movl	$-2, %eax
	xorl	%r11d, %r11d
	jmp	.L1651
.L1724:
	movl	$-2147483648, %r10d
	xorl	%ecx, %ecx
	jmp	.L1631
.L1905:
	xorl	%r11d, %r11d
	movl	$-2, %eax
	jmp	.L1651
.L1908:
	movzbl	64(%r12), %eax
	jmp	.L1696
.L1904:
	movzbl	64(%r12), %edx
	jmp	.L1710
.L1897:
	movl	$-2147483648, %ebx
	jmp	.L1648
.L1728:
	xorl	%r11d, %r11d
	movl	$1, %eax
	jmp	.L1640
.L1668:
	cmpb	$7, %r9b
	setne	%dl
	cmpb	$2, %r9b
	setne	%al
	testb	%al, %dl
	jne	.L1658
	cmpb	$5, %cl
	setge	%al
	setge	%r11b
	setl	%dl
	movzbl	%al, %eax
	leal	1(%rax,%rax), %eax
	jmp	.L1651
.L1797:
	movl	$15, %edx
.L1716:
	movq	48(%r12), %rax
	leal	0(,%rdx,4), %ecx
	shrq	%cl, %rax
	jmp	.L1717
.L1796:
	movl	$14, %edx
	jmp	.L1716
.L1795:
	movl	$13, %edx
	jmp	.L1716
.L1794:
	movl	$12, %edx
	jmp	.L1716
.L1793:
	movl	$11, %edx
	jmp	.L1716
.L1792:
	movl	$10, %edx
	jmp	.L1716
.L1791:
	movl	$9, %edx
	jmp	.L1716
.L1790:
	movl	$8, %edx
	jmp	.L1716
.L1789:
	movl	$7, %edx
	jmp	.L1716
.L1788:
	movl	$6, %edx
	jmp	.L1716
.L1787:
	movl	$5, %edx
	jmp	.L1716
.L1786:
	movl	$4, %edx
	jmp	.L1716
.L1785:
	movl	$3, %edx
	jmp	.L1716
.L1784:
	movl	$2, %edx
	jmp	.L1716
.L1783:
	movl	$1, %edx
	jmp	.L1716
.L1782:
	xorl	%edx, %edx
	jmp	.L1716
	.cfi_endproc
.LFE3667:
	.size	_ZN6icu_676number4impl15DecimalQuantity13roundToNickelEi25UNumberFormatRoundingModeR10UErrorCode, .-_ZN6icu_676number4impl15DecimalQuantity13roundToNickelEi25UNumberFormatRoundingModeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode
	.type	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode, @function
_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode:
.LFB3668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movl	12(%rdi), %eax
	movzbl	64(%rdi), %edi
	subl	%eax, %ebx
	movl	%ebx, %r8d
	testl	%eax, %eax
	jns	.L1913
	cmpl	%ebx, %esi
	jg	.L2147
.L1913:
	testl	%eax, %eax
	jle	.L2094
	cmpl	%ebx, %r13d
	jl	.L2032
.L2094:
	testb	%dil, %dil
	jne	.L2148
	xorl	%r9d, %r9d
	cmpl	$15, %ebx
	ja	.L1919
	movq	48(%r12), %r9
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %r9
	andl	$15, %r9d
.L1919:
	testl	%ebx, %ebx
	jle	.L1916
.L1922:
	movl	16(%r12), %esi
	testl	%esi, %esi
	jne	.L2149
.L1912:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2148:
	.cfi_restore_state
	testl	%ebx, %ebx
	js	.L2033
	movl	16(%r12), %esi
	cmpl	%esi, %ebx
	jge	.L2034
	movq	48(%r12), %rdx
	movslq	%ebx, %rax
	movzbl	(%rdx,%rax), %r9d
	testl	%ebx, %ebx
	je	.L2150
	leal	-1(%rbx), %edx
	movzbl	21(%r12), %r10d
	cmpl	%edx, %ebx
	jl	.L2151
.L2030:
	testl	%edx, %edx
	js	.L1925
.L2027:
	cmpl	%edx, %esi
	jle	.L1926
	movq	48(%r12), %rcx
	movslq	%edx, %rax
	movl	$1, %edi
	movzbl	(%rcx,%rax), %ecx
.L1931:
	testb	%r10b, %r10b
	je	.L2152
	subl	$2, %ebx
	subl	$14, %esi
	cmpl	%r8d, %ebx
	jg	.L2153
.L1945:
	xorl	%edi, %edi
	movl	%edx, -64(%rbp)
	movb	%cl, -60(%rbp)
	movb	%r9b, -53(%rbp)
	movl	%r8d, -52(%rbp)
	call	uprv_max_67@PLT
	movzbl	-60(%rbp), %ecx
	movl	-52(%rbp), %r8d
	movzbl	-53(%rbp), %r9d
	movl	-64(%rbp), %edx
	testb	%cl, %cl
	je	.L2022
	cmpb	$4, %cl
	je	.L2154
	cmpb	$5, %cl
	je	.L2155
	cmpb	$9, %cl
	je	.L2156
	xorl	%eax, %eax
	cmpb	$5, %cl
	setge	%al
	leal	1(%rax,%rax), %eax
.L1947:
	movl	16(%r12), %esi
	leal	-14(%rsi), %ecx
	cmpl	$3, %r14d
	jbe	.L1967
	.p2align 4,,10
	.p2align 3
.L2159:
	cmpl	%edx, %ecx
	jg	.L1968
	cmpl	$2, %eax
	jne	.L1969
.L1968:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity23convertToAccurateDoubleEv
	addq	$24, %rsp
	movq	%r15, %r8
	movl	%r14d, %edx
	popq	%rbx
	movl	%r13d, %esi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModebR10UErrorCode
	.p2align 4,,10
	.p2align 3
.L2147:
	.cfi_restore_state
	movl	16(%r12), %esi
	testl	%esi, %esi
	je	.L1912
	movzbl	21(%r12), %r10d
	testb	%dil, %dil
	je	.L2142
	movl	$2147483647, %r8d
	movl	$2147483647, %ebx
	movl	$2147483646, %edx
	xorl	%r9d, %r9d
	jmp	.L2027
	.p2align 4,,10
	.p2align 3
.L2033:
	xorl	%r9d, %r9d
.L1916:
	cmpb	$0, 21(%r12)
	jne	.L1922
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2034:
	.cfi_restore_state
	xorl	%r9d, %r9d
	jmp	.L1919
	.p2align 4,,10
	.p2align 3
.L2149:
	leal	-1(%rbx), %eax
	movzbl	21(%r12), %r10d
	movl	%eax, %edx
	cmpl	%r8d, %eax
	jg	.L2157
.L1924:
	testb	%dil, %dil
	jne	.L2030
	cmpl	$15, %eax
	ja	.L1926
	movq	48(%r12), %rax
	leal	0(,%rdx,4), %ecx
	shrq	%cl, %rax
	movq	%rax, %rcx
	andl	$15, %ecx
	jmp	.L1931
.L2157:
	movl	$-2147483648, %edx
	testb	%dil, %dil
	jne	.L1925
	.p2align 4,,10
	.p2align 3
.L1926:
	testb	%r10b, %r10b
	je	.L1929
.L2008:
	subl	$2, %ebx
	subl	$14, %esi
	cmpl	%r8d, %ebx
	jg	.L2158
.L2024:
	xorl	%edi, %edi
	movl	%edx, -60(%rbp)
	movb	%r9b, -53(%rbp)
	movl	%r8d, -52(%rbp)
	call	uprv_max_67@PLT
	movl	-52(%rbp), %r8d
	movzbl	-53(%rbp), %r9d
	movl	-60(%rbp), %edx
.L2022:
	cmpl	%ebx, %eax
	jg	.L2042
	cmpb	$0, 64(%r12)
	je	.L1950
	movslq	%ebx, %rcx
	jmp	.L1952
	.p2align 4,,10
	.p2align 3
.L1951:
	subl	$1, %ebx
	subq	$1, %rcx
	cmpl	%eax, %ebx
	jl	.L2042
.L1952:
	testl	%ebx, %ebx
	js	.L1951
	cmpl	%ebx, 16(%r12)
	jle	.L1951
	movq	48(%r12), %rsi
	cmpb	$0, (%rsi,%rcx)
	je	.L1951
	.p2align 4,,10
	.p2align 3
.L2049:
	movl	16(%r12), %esi
	movl	$1, %eax
	leal	-14(%rsi), %ecx
	cmpl	$3, %r14d
	ja	.L2159
.L1967:
	cmpl	%edx, %ecx
	jg	.L1968
	testl	%eax, %eax
	js	.L1968
.L1969:
	movb	$0, 21(%r12)
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	testl	%r8d, %r8d
	jle	.L1912
	movl	%r9d, %ebx
	notl	%ebx
	andl	$1, %ebx
	cmpl	$-1, %eax
	je	.L2065
	cmpl	$-2, %eax
	je	.L2160
.L1937:
	movzbl	20(%r12), %ecx
	cmpl	$6, %r14d
	ja	.L1972
	leaq	.L1974(%rip), %rdi
	movslq	(%rdi,%r14,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L1974:
	.long	.L1980-.L1974
	.long	.L1979-.L1974
	.long	.L2090-.L1974
	.long	.L2091-.L1974
	.long	.L1976-.L1974
	.long	.L1975-.L1974
	.long	.L1973-.L1974
	.text
	.p2align 4,,10
	.p2align 3
.L1949:
	subl	$1, %ebx
	cmpl	%ebx, %eax
	jg	.L2042
.L1950:
	cmpl	$15, %ebx
	ja	.L1949
	movq	48(%r12), %rdi
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rdi
	movq	%rdi, %rcx
	andl	$15, %ecx
	je	.L1949
	jmp	.L2049
	.p2align 4,,10
	.p2align 3
.L2142:
	testb	%r10b, %r10b
	jne	.L2136
	xorl	%r9d, %r9d
	movl	$2147483647, %r8d
	.p2align 4,,10
	.p2align 3
.L1929:
	movzbl	20(%r12), %ebx
	cmpl	$6, %r14d
	ja	.L1972
	leaq	.L2015(%rip), %rcx
	movslq	(%rcx,%r14,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2015:
	.long	.L2021-.L2015
	.long	.L2020-.L2015
	.long	.L2090-.L2015
	.long	.L2091-.L2015
	.long	.L2090-.L2015
	.long	.L2090-.L2015
	.long	.L2090-.L2015
	.text
	.p2align 4,,10
	.p2align 3
.L1975:
	cmpl	$3, %eax
	je	.L2091
	testl	%eax, %eax
	jle	.L1972
	.p2align 4,,10
	.p2align 3
.L2090:
	movl	$1, %ebx
.L1977:
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L1912
	movzbl	64(%r12), %eax
	cmpl	%r8d, %esi
	jg	.L1984
	testb	%al, %al
	jne	.L2161
.L1985:
	movq	$0, 48(%r12)
	xorl	%esi, %esi
	movl	$0, 16(%r12)
	movb	$0, 21(%r12)
	movq	$0x000000000, 24(%r12)
	movl	$0, 32(%r12)
	movl	$0, 44(%r12)
	movl	%r13d, 12(%r12)
.L1986:
	testb	%bl, %bl
	jne	.L1993
	movzbl	64(%r12), %edi
	cmpb	$9, %r9b
	je	.L2162
.L1994:
	testb	%dil, %dil
	je	.L2004
	movl	$1, %ebx
	testl	%esi, %esi
	jle	.L2005
	movq	48(%r12), %rax
	movzbl	(%rax), %ebx
	addl	$1, %ebx
.L2005:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movq	48(%r12), %rax
	movb	%bl, (%rax)
	movl	16(%r12), %esi
	jmp	.L2006
	.p2align 4,,10
	.p2align 3
.L1973:
	cmpl	$1, %eax
	je	.L2090
	subl	$2, %eax
	cmpl	$1, %eax
	ja	.L1972
.L2091:
	xorl	%ebx, %ebx
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L2004:
	movq	48(%r12), %rax
	movq	%rax, %rdx
	andq	$-16, %rax
	andl	$15, %edx
	addq	$1, %rdx
	orq	%rdx, %rax
	movq	%rax, 48(%r12)
.L2006:
	addl	$1, %esi
	movl	%esi, 16(%r12)
.L1993:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity7compactEv
	.p2align 4,,10
	.p2align 3
.L2152:
	.cfi_restore_state
	cmpb	$4, %cl
	jle	.L1929
	cmpb	$5, %cl
	jne	.L1935
	subl	$2, %ebx
	cmpl	%r8d, %ebx
	jg	.L1941
	testl	%ebx, %ebx
	js	.L1941
	testb	%dil, %dil
	je	.L1942
	movslq	%ebx, %rax
	jmp	.L1944
	.p2align 4,,10
	.p2align 3
.L1943:
	subl	$1, %ebx
	subq	$1, %rax
	cmpl	$-1, %ebx
	je	.L1941
.L1944:
	cmpl	%esi, %ebx
	jge	.L1943
	movq	48(%r12), %rdx
	cmpb	$0, (%rdx,%rax)
	je	.L1943
.L1935:
	movzbl	20(%r12), %ebx
	cmpl	$6, %r14d
	ja	.L1972
	leaq	.L2019(%rip), %rdx
	movslq	(%rdx,%r14,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2019:
	.long	.L2021-.L2019
	.long	.L2020-.L2019
	.long	.L2090-.L2019
	.long	.L2091-.L2019
	.long	.L2091-.L2019
	.long	.L2091-.L2019
	.long	.L2091-.L2019
	.text
	.p2align 4,,10
	.p2align 3
.L2150:
	cmpb	$0, 21(%r12)
	je	.L1912
	movzbl	21(%r12), %r10d
	movl	$-1, %edx
	movl	$-1, %eax
	jmp	.L1924
	.p2align 4,,10
	.p2align 3
.L1976:
	cmpl	$2, %eax
	je	.L1977
	cmpl	$3, %eax
	je	.L2091
	cmpl	$1, %eax
	je	.L2090
.L1972:
	movl	$65809, (%r15)
	jmp	.L1912
.L2151:
	movl	$-2147483648, %edx
	.p2align 4,,10
	.p2align 3
.L1925:
	testb	%r10b, %r10b
	jne	.L2008
	movl	%r9d, %ebx
	movl	$1, %eax
	notl	%ebx
	andl	$1, %ebx
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L1984:
	subl	%r8d, %esi
	testb	%al, %al
	je	.L2163
	xorl	%eax, %eax
	movslq	%r8d, %rdi
	.p2align 4,,10
	.p2align 3
.L1987:
	movq	48(%r12), %rdx
	leaq	(%rdx,%rax), %rcx
	movzbl	(%rcx,%rdi), %ecx
	movb	%cl, (%rdx,%rax)
	movl	16(%r12), %ecx
	leal	1(%rax), %edx
	addq	$1, %rax
	movl	%ecx, %esi
	subl	%r8d, %esi
	cmpl	%eax, %esi
	jg	.L1987
	cmpl	%edx, %ecx
	jle	.L1989
	movslq	%edx, %rax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1992:
	movq	48(%r12), %rdx
	movb	$0, (%rdx,%rax)
	addq	$1, %rax
	movl	16(%r12), %esi
	leal	(%rcx,%rax), %edx
	cmpl	%edx, %esi
	jg	.L1992
	subl	%r8d, %esi
	jmp	.L1989
	.p2align 4,,10
	.p2align 3
.L1940:
	subl	$1, %ebx
	cmpl	$-1, %ebx
	je	.L1941
.L1942:
	cmpl	$15, %ebx
	jg	.L1940
	movq	48(%r12), %rax
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rax
	testb	$15, %al
	je	.L1940
	jmp	.L1935
	.p2align 4,,10
	.p2align 3
.L2136:
	subl	$14, %esi
	xorl	%r9d, %r9d
	movl	$2147483645, %ebx
	movl	$2147483646, %edx
	movl	$2147483647, %r8d
	jmp	.L2024
	.p2align 4,,10
	.p2align 3
.L2163:
	leal	0(,%r8,4), %ecx
	shrq	%cl, 48(%r12)
.L1989:
	addl	%r8d, 12(%r12)
	movl	%esi, 16(%r12)
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L2065:
	movl	$1, %eax
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L2020:
	andl	$1, %ebx
.L2013:
	xorl	$1, %ebx
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L2021:
	andl	$1, %ebx
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L1979:
	andl	$1, %ecx
	movl	%ecx, %ebx
	jmp	.L2013
	.p2align 4,,10
	.p2align 3
.L1980:
	andl	$1, %ecx
	movl	%ecx, %ebx
	jmp	.L1977
.L2032:
	movl	$-2147483648, %ebx
	xorl	%r9d, %r9d
	movl	$-2147483648, %r8d
	jmp	.L1916
	.p2align 4,,10
	.p2align 3
.L2154:
	cmpl	%ebx, %eax
	jg	.L2055
	cmpb	$0, 64(%r12)
	je	.L1955
	movslq	%ebx, %rcx
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L2164:
	cmpl	%ebx, 16(%r12)
	jle	.L2049
	movq	48(%r12), %rsi
	cmpb	$9, (%rsi,%rcx)
	jne	.L2049
	subl	$1, %ebx
	subq	$1, %rcx
	cmpl	%ebx, %eax
	jg	.L2055
.L1956:
	testl	%ebx, %ebx
	jns	.L2164
	jmp	.L2049
	.p2align 4,,10
	.p2align 3
.L2165:
	movq	48(%r12), %rdi
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rdi
	movq	%rdi, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jne	.L2049
	subl	$1, %ebx
	cmpl	%ebx, %eax
	jg	.L2055
.L1955:
	cmpl	$15, %ebx
	ja	.L2049
	jmp	.L2165
	.p2align 4,,10
	.p2align 3
.L2042:
	movl	$-1, %eax
	jmp	.L1947
	.p2align 4,,10
	.p2align 3
.L2155:
	cmpl	%ebx, %eax
	jg	.L2055
	cmpb	$0, 64(%r12)
	je	.L1960
	movslq	%ebx, %rcx
	jmp	.L1962
	.p2align 4,,10
	.p2align 3
.L1961:
	subl	$1, %ebx
	subq	$1, %rcx
	cmpl	%ebx, %eax
	jg	.L2055
.L1962:
	testl	%ebx, %ebx
	js	.L1961
	cmpl	%ebx, 16(%r12)
	jle	.L1961
	movq	48(%r12), %rsi
	cmpb	$0, (%rsi,%rcx)
	je	.L1961
	.p2align 4,,10
	.p2align 3
.L2062:
	movl	$3, %eax
	jmp	.L1947
	.p2align 4,,10
	.p2align 3
.L1959:
	subl	$1, %ebx
	cmpl	%ebx, %eax
	jg	.L2055
.L1960:
	cmpl	$15, %ebx
	ja	.L1959
	movq	48(%r12), %rdi
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rdi
	movq	%rdi, %rcx
	andl	$15, %ecx
	je	.L1959
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2161:
	movq	48(%r12), %rdi
	movb	%r9b, -52(%rbp)
	call	uprv_free_67@PLT
	movb	$0, 64(%r12)
	movzbl	-52(%rbp), %r9d
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L2055:
	movl	$2, %eax
	jmp	.L1947
	.p2align 4,,10
	.p2align 3
.L2156:
	cmpl	%ebx, %eax
	jg	.L2063
	cmpb	$0, 64(%r12)
	je	.L1965
	movslq	%ebx, %rcx
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L2166:
	cmpl	16(%r12), %ebx
	jge	.L2062
	movq	48(%r12), %rsi
	cmpb	$9, (%rsi,%rcx)
	jne	.L2062
	subl	$1, %ebx
	subq	$1, %rcx
	cmpl	%ebx, %eax
	jg	.L2063
.L1966:
	testl	%ebx, %ebx
	jns	.L2166
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2167:
	movq	48(%r12), %rdi
	leal	0(,%rbx,4), %ecx
	shrq	%cl, %rdi
	movq	%rdi, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jne	.L2062
	subl	$1, %ebx
	cmpl	%ebx, %eax
	jg	.L2063
.L1965:
	cmpl	$15, %ebx
	ja	.L2062
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2162:
	testb	%dil, %dil
	jne	.L2070
	movq	48(%r12), %rax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L2071
	movl	%eax, %edx
	shrb	$4, %dl
	cmpb	$9, %dl
	jne	.L2072
	movq	%rax, %rdx
	shrq	$8, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L2073
	movq	%rax, %rdx
	shrq	$12, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L2074
	movq	%rax, %rdx
	shrq	$16, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L2075
	movq	%rax, %rdx
	shrq	$20, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L2076
	movq	%rax, %rdx
	shrq	$24, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L2077
	movq	%rax, %rdx
	shrq	$28, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L2078
	movq	%rax, %rdx
	shrq	$32, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L2079
	movq	%rax, %rdx
	shrq	$36, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L2080
	movq	%rax, %rdx
	shrq	$40, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L2081
	movq	%rax, %rdx
	shrq	$44, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L2082
	movq	%rax, %rdx
	shrq	$48, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L2083
	movq	%rax, %rdx
	shrq	$52, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L2084
	movq	%rax, %rdx
	shrq	$56, %rdx
	andl	$15, %edx
	cmpb	$9, %dl
	jne	.L2085
	movq	%rax, %rdx
	shrq	$60, %rdx
	cmpb	$9, %dl
	jne	.L2086
	movl	$64, %ecx
	movl	$16, %edx
	shrq	%cl, %rax
.L1997:
	movq	%rax, 48(%r12)
	subl	%edx, %esi
.L2001:
	addl	%edx, 12(%r12)
	movl	%esi, 16(%r12)
	jmp	.L1994
	.p2align 4,,10
	.p2align 3
.L2160:
	movzbl	20(%r12), %ebx
	cmpl	$6, %r14d
	ja	.L1972
	leaq	.L2010(%rip), %rdx
	movslq	(%rdx,%r14,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2010:
	.long	.L2021-.L2010
	.long	.L2020-.L2010
	.long	.L2090-.L2010
	.long	.L2091-.L2010
	.long	.L2091-.L2010
	.long	.L2091-.L2010
	.long	.L2091-.L2010
	.text
	.p2align 4,,10
	.p2align 3
.L2070:
	xorl	%eax, %eax
.L1995:
	movl	%eax, %edx
	cmpl	%eax, %esi
	jg	.L2168
.L1998:
	movl	%esi, %ecx
	xorl	%eax, %eax
	movslq	%edx, %rdi
	subl	%edx, %ecx
	testl	%ecx, %ecx
	jle	.L2169
.L1999:
	movq	48(%r12), %rcx
	leal	1(%rax), %r8d
	leaq	(%rcx,%rax), %rsi
	movzbl	(%rsi,%rdi), %esi
	movb	%sil, (%rcx,%rax)
	movl	16(%r12), %esi
	addq	$1, %rax
	movl	%esi, %ecx
	subl	%edx, %ecx
	cmpl	%eax, %ecx
	jg	.L1999
.L2002:
	movslq	%r8d, %rax
	cmpl	%esi, %r8d
	jge	.L2170
	.p2align 4,,10
	.p2align 3
.L2003:
	movq	48(%r12), %rcx
	movb	$0, (%rcx,%rax)
	movl	16(%r12), %esi
	addq	$1, %rax
	cmpl	%esi, %eax
	jl	.L2003
	movzbl	64(%r12), %edi
	subl	%edx, %esi
	jmp	.L2001
	.p2align 4,,10
	.p2align 3
.L2168:
	movq	48(%r12), %rcx
	movzbl	(%rcx,%rax), %ecx
	addq	$1, %rax
	cmpb	$9, %cl
	je	.L1995
	jmp	.L1998
	.p2align 4,,10
	.p2align 3
.L1941:
	movl	%r9d, %ebx
	movl	$2, %eax
	notl	%ebx
	andl	$1, %ebx
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L2063:
	movl	$-2, %eax
	jmp	.L1947
.L2169:
	xorl	%r8d, %r8d
	jmp	.L2002
.L2170:
	movzbl	64(%r12), %edi
	movl	%ecx, %esi
	jmp	.L2001
.L2158:
	xorl	%edi, %edi
	movl	%edx, -60(%rbp)
	movl	$-2147483648, %ebx
	movb	%r9b, -53(%rbp)
	movl	%r8d, -52(%rbp)
	call	uprv_max_67@PLT
	movl	-52(%rbp), %r8d
	movzbl	-53(%rbp), %r9d
	movl	-60(%rbp), %edx
	jmp	.L2022
.L2153:
	movl	$-2147483648, %ebx
	jmp	.L1945
.L2086:
	movl	$15, %edx
.L1996:
	movq	48(%r12), %rax
	leal	0(,%rdx,4), %ecx
	shrq	%cl, %rax
	jmp	.L1997
.L2071:
	xorl	%edx, %edx
	jmp	.L1996
.L2073:
	movl	$2, %edx
	jmp	.L1996
.L2072:
	movl	$1, %edx
	jmp	.L1996
.L2077:
	movl	$6, %edx
	jmp	.L1996
.L2076:
	movl	$5, %edx
	jmp	.L1996
.L2075:
	movl	$4, %edx
	jmp	.L1996
.L2074:
	movl	$3, %edx
	jmp	.L1996
.L2085:
	movl	$14, %edx
	jmp	.L1996
.L2084:
	movl	$13, %edx
	jmp	.L1996
.L2083:
	movl	$12, %edx
	jmp	.L1996
.L2082:
	movl	$11, %edx
	jmp	.L1996
.L2081:
	movl	$10, %edx
	jmp	.L1996
.L2080:
	movl	$9, %edx
	jmp	.L1996
.L2079:
	movl	$8, %edx
	jmp	.L1996
.L2078:
	movl	$7, %edx
	jmp	.L1996
	.cfi_endproc
.LFE3668:
	.size	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode, .-_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEv
	.type	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEv, @function
_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEv:
.LFB3685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 64(%rdi)
	jne	.L2176
	movl	$40, %edi
	call	uprv_malloc_67@PLT
	movl	$40, 56(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rax, 48(%rbx)
	movq	$0, 32(%rax)
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
.L2174:
	movb	$1, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2176:
	.cfi_restore_state
	movl	56(%rdi), %r12d
	cmpl	$39, %r12d
	jg	.L2174
	movl	$80, %edi
	movslq	%r12d, %r14
	call	uprv_malloc_67@PLT
	movq	48(%rbx), %r15
	movl	$80, %ecx
	movq	%r14, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%r15, %rsi
	call	__memcpy_chk@PLT
	movl	$40, %edx
	leaq	0(%r13,%r14), %rdi
	xorl	%esi, %esi
	subl	%r12d, %edx
	movslq	%edx, %rdx
	call	memset@PLT
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	movq	%r13, 48(%rbx)
	movl	$80, 56(%rbx)
	jmp	.L2174
	.cfi_endproc
.LFE3685:
	.size	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEv, .-_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi
	.type	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi, @function
_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi:
.LFB3686:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L2185
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	cmpb	$0, 64(%rdi)
	je	.L2179
	movl	56(%rdi), %r13d
	cmpl	%r13d, %esi
	jg	.L2188
.L2180:
	movb	$1, 64(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2179:
	.cfi_restore_state
	movslq	%esi, %r13
	movq	%r13, %rdi
	call	uprv_malloc_67@PLT
	movl	%ebx, 56(%r12)
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%rax, 48(%r12)
	movq	%rax, %rdi
	call	memset@PLT
	jmp	.L2180
	.p2align 4,,10
	.p2align 3
.L2185:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L2188:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leal	(%rsi,%rsi), %ecx
	movslq	%r13d, %r15
	subl	%r13d, %ebx
	movslq	%ecx, %rdi
	movl	%ecx, -60(%rbp)
	call	uprv_malloc_67@PLT
	movq	48(%r12), %r8
	movq	%r15, %rdx
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%r8, %rsi
	movq	%r8, -56(%rbp)
	call	memcpy@PLT
	movslq	%ebx, %rdx
	leaq	(%r14,%r15), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movl	-60(%rbp), %ecx
	movq	%r14, 48(%r12)
	movl	%ecx, 56(%r12)
	jmp	.L2180
	.cfi_endproc
.LFE3686:
	.size	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi, .-_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity13switchStorageEv
	.type	_ZN6icu_676number4impl15DecimalQuantity13switchStorageEv, @function
_ZN6icu_676number4impl15DecimalQuantity13switchStorageEv:
.LFB3687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpb	$0, 64(%rdi)
	movq	48(%rdi), %r13
	je	.L2190
	movslq	16(%rdi), %rdx
	movl	%edx, %esi
	subl	$1, %esi
	js	.L2196
	movslq	%esi, %rax
	leaq	-2(%r13,%rdx), %rcx
	movl	%esi, %esi
	xorl	%ebx, %ebx
	addq	%r13, %rax
	subq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L2192:
	movsbq	(%rax), %rdx
	salq	$4, %rbx
	subq	$1, %rax
	orq	%rdx, %rbx
	cmpq	%rax, %rcx
	jne	.L2192
.L2191:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	movq	%rbx, 48(%r12)
	movb	$0, 64(%r12)
.L2189:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2190:
	.cfi_restore_state
	movl	$40, %esi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	16(%r12), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L2189
	.p2align 4,,10
	.p2align 3
.L2194:
	movl	%r13d, %ecx
	movq	48(%r12), %rdx
	shrq	$4, %r13
	andl	$15, %ecx
	movb	%cl, (%rdx,%rax)
	addq	$1, %rax
	cmpl	%eax, 16(%r12)
	jg	.L2194
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2196:
	.cfi_restore_state
	xorl	%ebx, %ebx
	jmp	.L2191
	.cfi_endproc
.LFE3687:
	.size	_ZN6icu_676number4impl15DecimalQuantity13switchStorageEv, .-_ZN6icu_676number4impl15DecimalQuantity13switchStorageEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity11copyBcdFromERKS2_
	.type	_ZN6icu_676number4impl15DecimalQuantity11copyBcdFromERKS2_, @function
_ZN6icu_676number4impl15DecimalQuantity11copyBcdFromERKS2_:
.LFB3688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 64(%rdi)
	movq	%rdi, %rbx
	jne	.L2208
.L2202:
	movb	$0, 21(%rbx)
	cmpb	$0, 64(%r12)
	movq	$0, 48(%rbx)
	movq	$0, 12(%rbx)
	movq	$0x000000000, 24(%rbx)
	movl	$0, 32(%rbx)
	movl	$0, 44(%rbx)
	je	.L2203
	movl	16(%r12), %esi
	xorl	%edi, %edi
	testl	%esi, %esi
	je	.L2204
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14ensureCapacityEi.part.0
	movl	16(%r12), %esi
	movq	48(%rbx), %rdi
.L2204:
	movslq	%esi, %rdx
	movq	48(%r12), %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L2203:
	.cfi_restore_state
	movq	48(%r12), %rax
	movq	%rax, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2208:
	.cfi_restore_state
	movq	48(%rdi), %rdi
	call	uprv_free_67@PLT
	movb	$0, 64(%rbx)
	jmp	.L2202
	.cfi_endproc
.LFE3688:
	.size	_ZN6icu_676number4impl15DecimalQuantity11copyBcdFromERKS2_, .-_ZN6icu_676number4impl15DecimalQuantity11copyBcdFromERKS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15DecimalQuantity11moveBcdFromERS2_
	.type	_ZN6icu_676number4impl15DecimalQuantity11moveBcdFromERS2_, @function
_ZN6icu_676number4impl15DecimalQuantity11moveBcdFromERS2_:
.LFB3689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 64(%rdi)
	movq	%rdi, %rbx
	jne	.L2214
.L2210:
	movb	$0, 21(%rbx)
	cmpb	$0, 64(%r12)
	movq	$0, 48(%rbx)
	movq	$0, 12(%rbx)
	movq	$0x000000000, 24(%rbx)
	movl	$0, 32(%rbx)
	movl	$0, 44(%rbx)
	je	.L2211
	movq	48(%r12), %rax
	movb	$1, 64(%rbx)
	movq	%rax, 48(%rbx)
	movl	56(%r12), %eax
	movl	%eax, 56(%rbx)
	popq	%rbx
	movq	$0, 48(%r12)
	movb	$0, 64(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2211:
	.cfi_restore_state
	movq	48(%r12), %rax
	movq	%rax, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2214:
	.cfi_restore_state
	movq	48(%rdi), %rdi
	call	uprv_free_67@PLT
	movb	$0, 64(%rbx)
	jmp	.L2210
	.cfi_endproc
.LFE3689:
	.size	_ZN6icu_676number4impl15DecimalQuantity11moveBcdFromERS2_, .-_ZN6icu_676number4impl15DecimalQuantity11moveBcdFromERS2_
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC14:
	.string	"P"
	.string	"r"
	.string	"e"
	.string	"c"
	.string	"i"
	.string	"s"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	" "
	.string	"e"
	.string	"x"
	.string	"c"
	.string	"e"
	.string	"e"
	.string	"d"
	.string	"s"
	.string	" "
	.string	"l"
	.string	"e"
	.string	"n"
	.string	"g"
	.string	"t"
	.string	"h"
	.string	" "
	.string	"o"
	.string	"f"
	.string	" "
	.string	"l"
	.string	"o"
	.string	"n"
	.string	"g"
	.string	""
	.string	""
	.align 8
.LC15:
	.string	"N"
	.string	"o"
	.string	"n"
	.string	"z"
	.string	"e"
	.string	"r"
	.string	"o"
	.string	" "
	.string	"d"
	.string	"i"
	.string	"g"
	.string	"i"
	.string	"t"
	.string	"s"
	.string	" "
	.string	"o"
	.string	"u"
	.string	"t"
	.string	"s"
	.string	"i"
	.string	"d"
	.string	"e"
	.string	" "
	.string	"o"
	.string	"f"
	.string	" "
	.string	"r"
	.string	"a"
	.string	"n"
	.string	"g"
	.string	"e"
	.string	" "
	.string	"i"
	.string	"n"
	.string	" "
	.string	"l"
	.string	"o"
	.string	"n"
	.string	"g"
	.string	""
	.string	""
	.align 8
.LC16:
	.string	"M"
	.string	"o"
	.string	"s"
	.string	"t"
	.string	" "
	.string	"s"
	.string	"i"
	.string	"g"
	.string	"n"
	.string	"i"
	.string	"f"
	.string	"i"
	.string	"c"
	.string	"a"
	.string	"n"
	.string	"t"
	.string	" "
	.string	"d"
	.string	"i"
	.string	"g"
	.string	"i"
	.string	"t"
	.string	" "
	.string	"i"
	.string	"s"
	.string	" "
	.string	"z"
	.string	"e"
	.string	"r"
	.string	"o"
	.string	" "
	.string	"i"
	.string	"n"
	.string	" "
	.string	"b"
	.string	"y"
	.string	"t"
	.string	"e"
	.string	" "
	.string	"m"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	""
	.string	""
	.align 8
.LC17:
	.string	"D"
	.string	"i"
	.string	"g"
	.string	"i"
	.string	"t"
	.string	" "
	.string	"b"
	.string	"e"
	.string	"l"
	.string	"o"
	.string	"w"
	.string	" "
	.string	"0"
	.string	" "
	.string	"i"
	.string	"n"
	.string	" "
	.string	"b"
	.string	"y"
	.string	"t"
	.string	"e"
	.string	" "
	.string	"a"
	.string	"r"
	.string	"r"
	.string	"a"
	.string	"y"
	.string	""
	.string	""
	.align 8
.LC18:
	.string	"L"
	.string	"e"
	.string	"a"
	.string	"s"
	.string	"t"
	.string	" "
	.string	"s"
	.string	"i"
	.string	"g"
	.string	"n"
	.string	"i"
	.string	"f"
	.string	"i"
	.string	"c"
	.string	"a"
	.string	"n"
	.string	"t"
	.string	" "
	.string	"d"
	.string	"i"
	.string	"g"
	.string	"i"
	.string	"t"
	.string	" "
	.string	"i"
	.string	"s"
	.string	" "
	.string	"z"
	.string	"e"
	.string	"r"
	.string	"o"
	.string	" "
	.string	"i"
	.string	"n"
	.string	" "
	.string	"l"
	.string	"o"
	.string	"n"
	.string	"g"
	.string	" "
	.string	"m"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	""
	.string	""
	.align 8
.LC19:
	.string	"D"
	.string	"i"
	.string	"g"
	.string	"i"
	.string	"t"
	.string	" "
	.string	"e"
	.string	"x"
	.string	"c"
	.string	"e"
	.string	"e"
	.string	"d"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	" "
	.string	"1"
	.string	"0"
	.string	" "
	.string	"i"
	.string	"n"
	.string	" "
	.string	"b"
	.string	"y"
	.string	"t"
	.string	"e"
	.string	" "
	.string	"a"
	.string	"r"
	.string	"r"
	.string	"a"
	.string	"y"
	.string	""
	.string	""
	.align 8
.LC20:
	.string	"P"
	.string	"r"
	.string	"e"
	.string	"c"
	.string	"i"
	.string	"s"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	" "
	.string	"e"
	.string	"x"
	.string	"c"
	.string	"e"
	.string	"e"
	.string	"d"
	.string	"s"
	.string	" "
	.string	"l"
	.string	"e"
	.string	"n"
	.string	"g"
	.string	"t"
	.string	"h"
	.string	" "
	.string	"o"
	.string	"f"
	.string	" "
	.string	"b"
	.string	"y"
	.string	"t"
	.string	"e"
	.string	" "
	.string	"a"
	.string	"r"
	.string	"r"
	.string	"a"
	.string	"y"
	.string	""
	.string	""
	.align 8
.LC21:
	.string	"N"
	.string	"o"
	.string	"n"
	.string	"z"
	.string	"e"
	.string	"r"
	.string	"o"
	.string	" "
	.string	"d"
	.string	"i"
	.string	"g"
	.string	"i"
	.string	"t"
	.string	"s"
	.string	" "
	.string	"o"
	.string	"u"
	.string	"t"
	.string	"s"
	.string	"i"
	.string	"d"
	.string	"e"
	.string	" "
	.string	"o"
	.string	"f"
	.string	" "
	.string	"r"
	.string	"a"
	.string	"n"
	.string	"g"
	.string	"e"
	.string	" "
	.string	"i"
	.string	"n"
	.string	" "
	.string	"b"
	.string	"y"
	.string	"t"
	.string	"e"
	.string	" "
	.string	"a"
	.string	"r"
	.string	"r"
	.string	"a"
	.string	"y"
	.string	""
	.string	""
	.align 8
.LC22:
	.string	"V"
	.string	"a"
	.string	"l"
	.string	"u"
	.string	"e"
	.string	" "
	.string	"i"
	.string	"n"
	.string	" "
	.string	"b"
	.string	"c"
	.string	"d"
	.string	"L"
	.string	"o"
	.string	"n"
	.string	"g"
	.string	" "
	.string	"e"
	.string	"v"
	.string	"e"
	.string	"n"
	.string	" "
	.string	"t"
	.string	"h"
	.string	"o"
	.string	"u"
	.string	"g"
	.string	"h"
	.string	" "
	.string	"p"
	.string	"r"
	.string	"e"
	.string	"c"
	.string	"i"
	.string	"s"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	" "
	.string	"i"
	.string	"s"
	.string	" "
	.string	"z"
	.string	"e"
	.string	"r"
	.string	"o"
	.string	""
	.string	""
	.align 8
.LC23:
	.string	"D"
	.string	"i"
	.string	"g"
	.string	"i"
	.string	"t"
	.string	" "
	.string	"e"
	.string	"x"
	.string	"c"
	.string	"e"
	.string	"e"
	.string	"d"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	" "
	.string	"1"
	.string	"0"
	.string	" "
	.string	"i"
	.string	"n"
	.string	" "
	.string	"l"
	.string	"o"
	.string	"n"
	.string	"g"
	.string	""
	.string	""
	.align 8
.LC24:
	.string	"Z"
	.string	"e"
	.string	"r"
	.string	"o"
	.string	" "
	.string	"p"
	.string	"r"
	.string	"e"
	.string	"c"
	.string	"i"
	.string	"s"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	" "
	.string	"b"
	.string	"u"
	.string	"t"
	.string	" "
	.string	"w"
	.string	"e"
	.string	" "
	.string	"a"
	.string	"r"
	.string	"e"
	.string	" "
	.string	"i"
	.string	"n"
	.string	" "
	.string	"b"
	.string	"y"
	.string	"t"
	.string	"e"
	.string	" "
	.string	"m"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	""
	.string	""
	.align 8
.LC25:
	.string	"M"
	.string	"o"
	.string	"s"
	.string	"t"
	.string	" "
	.string	"s"
	.string	"i"
	.string	"g"
	.string	"n"
	.string	"i"
	.string	"f"
	.string	"i"
	.string	"c"
	.string	"a"
	.string	"n"
	.string	"t"
	.string	" "
	.string	"d"
	.string	"i"
	.string	"g"
	.string	"i"
	.string	"t"
	.string	" "
	.string	"i"
	.string	"s"
	.string	" "
	.string	"z"
	.string	"e"
	.string	"r"
	.string	"o"
	.string	" "
	.string	"i"
	.string	"n"
	.string	" "
	.string	"l"
	.string	"o"
	.string	"n"
	.string	"g"
	.string	" "
	.string	"m"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity11checkHealthEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity11checkHealthEv, @function
_ZNK6icu_676number4impl15DecimalQuantity11checkHealthEv:
.LFB3690:
	.cfi_startproc
	endbr64
	cmpb	$0, 64(%rdi)
	movl	16(%rdi), %eax
	je	.L2216
	leaq	.LC24(%rip), %r8
	testl	%eax, %eax
	je	.L2215
	movl	56(%rdi), %esi
	leaq	.LC20(%rip), %r8
	cmpl	%eax, %esi
	jl	.L2215
	movl	%eax, %ecx
	leaq	.LC16(%rip), %r8
	subl	$1, %ecx
	js	.L2215
	movq	48(%rdi), %r9
	movslq	%ecx, %rdx
	cmpb	$0, (%r9,%rdx)
	je	.L2215
	leaq	.LC18(%rip), %r8
	testl	%eax, %eax
	jle	.L2215
	movl	%ecx, %ecx
	cmpb	$0, (%r9)
	movq	%r9, %rdx
	leaq	1(%r9,%rcx), %rdi
	jne	.L2221
	jmp	.L2215
	.p2align 4,,10
	.p2align 3
.L2308:
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	je	.L2307
.L2221:
	movzbl	(%rdx), %ecx
	cmpb	$9, %cl
	jg	.L2239
	testb	%cl, %cl
	jns	.L2308
	leaq	.LC17(%rip), %r8
.L2215:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2216:
	testl	%eax, %eax
	jne	.L2224
	movq	48(%rdi), %rdx
	leaq	.LC22(%rip), %r8
	testq	%rdx, %rdx
	jne	.L2215
.L2229:
	cmpl	$15, %eax
	jbe	.L2309
.L2228:
	addl	$1, %eax
.L2306:
	cmpl	$16, %eax
	jne	.L2229
.L2262:
	xorl	%r8d, %r8d
	jmp	.L2215
	.p2align 4,,10
	.p2align 3
.L2309:
	leal	0(,%rax,4), %ecx
	movq	%rdx, %rsi
	leaq	.LC15(%rip), %r8
	shrq	%cl, %rsi
	movq	%rsi, %rcx
	andl	$15, %ecx
	je	.L2228
	jmp	.L2215
	.p2align 4,,10
	.p2align 3
.L2224:
	leaq	.LC14(%rip), %r8
	cmpl	$16, %eax
	jg	.L2215
	leal	-1(%rax), %ecx
	leaq	.LC25(%rip), %r8
	cmpl	$15, %ecx
	ja	.L2215
	movq	48(%rdi), %rdx
	sall	$2, %ecx
	movq	%rdx, %rsi
	shrq	%cl, %rsi
	movq	%rsi, %rcx
	andl	$15, %ecx
	je	.L2215
	movl	%edx, %ecx
	leaq	.LC18(%rip), %r8
	andl	$15, %ecx
	je	.L2215
	testl	%eax, %eax
	jle	.L2306
	leaq	.LC23(%rip), %r8
	cmpb	$9, %cl
	jg	.L2215
	cmpl	$1, %eax
	je	.L2229
	cmpb	$-97, %dl
	ja	.L2215
	cmpl	$2, %eax
	je	.L2229
	movq	%rdx, %rcx
	shrq	$8, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jg	.L2215
	cmpl	$3, %eax
	je	.L2229
	movq	%rdx, %rcx
	shrq	$12, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jg	.L2215
	cmpl	$4, %eax
	je	.L2229
	movq	%rdx, %rcx
	shrq	$16, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jg	.L2215
	cmpl	$5, %eax
	je	.L2229
	movq	%rdx, %rcx
	shrq	$20, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jg	.L2215
	cmpl	$6, %eax
	je	.L2229
	movq	%rdx, %rcx
	shrq	$24, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jg	.L2215
	cmpl	$7, %eax
	je	.L2229
	movq	%rdx, %rcx
	shrq	$28, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jg	.L2215
	cmpl	$8, %eax
	je	.L2229
	movq	%rdx, %rcx
	shrq	$32, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jg	.L2215
	cmpl	$9, %eax
	je	.L2229
	movq	%rdx, %rcx
	shrq	$36, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jg	.L2215
	cmpl	$10, %eax
	je	.L2229
	movq	%rdx, %rcx
	shrq	$40, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jg	.L2215
	cmpl	$11, %eax
	je	.L2229
	movq	%rdx, %rcx
	shrq	$44, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jg	.L2215
	cmpl	$12, %eax
	je	.L2229
	movq	%rdx, %rcx
	shrq	$48, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jg	.L2215
	cmpl	$13, %eax
	je	.L2229
	movq	%rdx, %rcx
	shrq	$52, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jg	.L2215
	cmpl	$14, %eax
	je	.L2229
	movq	%rdx, %rcx
	shrq	$56, %rcx
	andl	$15, %ecx
	cmpb	$9, %cl
	jg	.L2215
	cmpl	$15, %eax
	je	.L2229
	shrq	$60, %rdx
	movl	$0, %eax
	cmpb	$10, %dl
	cmovl	%rax, %r8
	jmp	.L2215
	.p2align 4,,10
	.p2align 3
.L2307:
	cmpl	%eax, %esi
	je	.L2262
	leal	1(%rax), %edx
	movslq	%edx, %rdx
	jmp	.L2219
	.p2align 4,,10
	.p2align 3
.L2222:
	addq	$1, %rdx
.L2219:
	movl	%edx, %ecx
	cmpl	%edx, %esi
	jle	.L2262
	cmpl	%edx, %eax
	jle	.L2222
	testl	%ecx, %ecx
	js	.L2222
	cmpb	$0, (%r9,%rdx)
	je	.L2222
	leaq	.LC21(%rip), %r8
	jmp	.L2215
	.p2align 4,,10
	.p2align 3
.L2239:
	leaq	.LC19(%rip), %r8
	jmp	.L2215
	.cfi_endproc
.LFE3690:
	.size	_ZNK6icu_676number4impl15DecimalQuantity11checkHealthEv, .-_ZNK6icu_676number4impl15DecimalQuantity11checkHealthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantityeqERKS2_
	.type	_ZNK6icu_676number4impl15DecimalQuantityeqERKS2_, @function
_ZNK6icu_676number4impl15DecimalQuantityeqERKS2_:
.LFB3691:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %edx
	movslq	12(%rsi), %rcx
	xorl	%r8d, %r8d
	cmpl	%ecx, %edx
	jne	.L2360
	movabsq	$1099511627775, %r9
	movq	16(%rdi), %rax
	xorq	16(%rsi), %rax
	andq	%r9, %rax
	jne	.L2360
	movl	36(%rdi), %r9d
	cmpl	36(%rsi), %r9d
	jne	.L2360
	movl	40(%rdi), %r10d
	cmpl	40(%rsi), %r10d
	jne	.L2360
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	21(%rdi), %ebx
	cmpb	21(%rsi), %bl
	jne	.L2310
	movl	16(%rdi), %r11d
	movl	$1, %r8d
	testl	%r11d, %r11d
	je	.L2310
	testb	%bl, %bl
	jne	.L2363
	leal	(%rdx,%r11), %ebx
	cmpl	%r9d, %ebx
	cmovge	%ebx, %r9d
	cmpl	%r10d, %edx
	cmovle	%edx, %r10d
	movl	%r9d, %r13d
	leal	-1(%r9), %r9d
	cmpl	%r10d, %r9d
	jl	.L2310
	movzbl	64(%rdi), %ebx
	movzbl	64(%rsi), %r8d
	testb	%bl, %bl
	jne	.L2314
	testb	%r8b, %r8b
	jne	.L2315
	movl	%ecx, %r11d
	movl	%r9d, %eax
	movl	%edx, %ebx
	sall	$30, %r11d
	subl	%edx, %eax
	subl	%ecx, %ebx
	subl	%ecx, %r11d
	addl	%edx, %r11d
	jmp	.L2318
	.p2align 4,,10
	.p2align 3
.L2365:
	movq	48(%rdi), %r9
	leal	0(,%rax,4), %ecx
	leal	(%rbx,%rax), %r12d
	shrq	%cl, %r9
	xorl	%ecx, %ecx
	andl	$15, %r9d
	cmpl	$15, %r12d
	ja	.L2317
.L2330:
	movq	48(%rsi), %r15
	leal	(%r11,%rax), %ecx
	sall	$2, %ecx
	shrq	%cl, %r15
	movq	%r15, %rcx
	andl	$15, %ecx
.L2317:
	cmpb	%r9b, %cl
	jne	.L2310
.L2329:
	subl	$1, %eax
	leal	(%rdx,%rax), %ecx
	cmpl	%ecx, %r10d
	jg	.L2364
.L2318:
	cmpl	$15, %eax
	jbe	.L2365
	leal	(%rbx,%rax), %ecx
	xorl	%r9d, %r9d
	cmpl	$15, %ecx
	jbe	.L2330
	jmp	.L2329
	.p2align 4,,10
	.p2align 3
.L2360:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	%r8d, %eax
	ret
.L2363:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movsd	24(%rdi), %xmm0
	ucomisd	24(%rsi), %xmm0
	jp	.L2352
	jne	.L2352
	movl	32(%rsi), %eax
	cmpl	%eax, 32(%rdi)
	sete	%r8b
.L2310:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2352:
	.cfi_restore_state
	xorl	%r8d, %r8d
	jmp	.L2310
.L2314:
	movl	%r9d, %eax
	subl	%edx, %eax
	testb	%r8b, %r8b
	jne	.L2322
	movl	%ecx, %r13d
	movslq	%r9d, %r9
	movslq	%edx, %r12
	movl	%edx, %r14d
	sall	$30, %r13d
	subq	%r12, %r9
	subl	%ecx, %r14d
	subl	%ecx, %r13d
	addl	%edx, %r13d
	jmp	.L2325
	.p2align 4,,10
	.p2align 3
.L2367:
	testl	%eax, %eax
	js	.L2323
	movq	48(%rdi), %rcx
	leal	(%r14,%rax), %r15d
	movzbl	(%rcx,%r9), %r12d
	xorl	%ecx, %ecx
	cmpl	$15, %r15d
	ja	.L2324
.L2334:
	movq	48(%rsi), %r15
	leal	(%rax,%r13), %ecx
	sall	$2, %ecx
	shrq	%cl, %r15
	movq	%r15, %rcx
	andl	$15, %ecx
.L2324:
	cmpb	%r12b, %cl
	jne	.L2310
.L2333:
	subl	$1, %eax
	subq	$1, %r9
	leal	(%rax,%rdx), %ecx
	cmpl	%ecx, %r10d
	jg	.L2366
.L2325:
	cmpl	%eax, %r11d
	jg	.L2367
.L2323:
	leal	(%r14,%rax), %ecx
	xorl	%r12d, %r12d
	cmpl	$15, %ecx
	ja	.L2333
	jmp	.L2334
.L2315:
	movl	%ecx, %r12d
	movl	%r9d, %r11d
	notq	%rcx
	movslq	%r13d, %rbx
	notl	%r12d
	subl	%edx, %r11d
	addq	%rcx, %rbx
	addl	%r13d, %r12d
	jmp	.L2321
	.p2align 4,,10
	.p2align 3
.L2368:
	movq	48(%rdi), %rdx
	leal	0(,%r11,4), %ecx
	movl	%eax, %r13d
	shrq	%cl, %rdx
	xorl	%ecx, %ecx
	andl	$15, %edx
	cmpl	%eax, %r12d
	js	.L2320
.L2332:
	movl	%r12d, %ecx
	subl	%r13d, %ecx
	movl	%ecx, %r13d
	xorl	%ecx, %ecx
	cmpl	%r13d, 16(%rsi)
	jle	.L2320
	movq	48(%rsi), %r13
	movq	%rbx, %rcx
	subq	%rax, %rcx
	movzbl	0(%r13,%rcx), %ecx
.L2320:
	cmpb	%dl, %cl
	jne	.L2352
.L2331:
	addq	$1, %rax
	movl	%r9d, %edx
	subl	$1, %r11d
	subl	%eax, %edx
	cmpl	%edx, %r10d
	jg	.L2310
.L2321:
	cmpl	$15, %r11d
	jbe	.L2368
	movl	%eax, %r13d
	xorl	%edx, %edx
	cmpl	%eax, %r12d
	jns	.L2332
	jmp	.L2331
.L2322:
	movl	%edx, %r13d
	movslq	%ecx, %r14
	movslq	%r9d, %r9
	subl	%ecx, %r13d
	movslq	%edx, %rcx
	subq	%r14, %r9
	subq	%rcx, %r14
	jmp	.L2328
	.p2align 4,,10
	.p2align 3
.L2369:
	cmpl	%eax, %r11d
	jle	.L2326
	movq	48(%rdi), %rcx
	movl	%r13d, %r12d
	addq	%r9, %rcx
	movzbl	(%rcx,%r14), %ebx
	xorl	%ecx, %ecx
	addl	%eax, %r12d
	js	.L2327
.L2336:
	xorl	%ecx, %ecx
	cmpl	%r12d, 16(%rsi)
	jle	.L2327
	movq	48(%rsi), %rcx
	movzbl	(%rcx,%r9), %ecx
.L2327:
	cmpb	%bl, %cl
	jne	.L2352
.L2335:
	subl	$1, %eax
	subq	$1, %r9
	leal	(%rax,%rdx), %ecx
	cmpl	%ecx, %r10d
	jg	.L2310
.L2328:
	testl	%eax, %eax
	jns	.L2369
.L2326:
	movl	%r13d, %r12d
	addl	%eax, %r12d
	js	.L2335
	xorl	%ebx, %ebx
	jmp	.L2336
.L2364:
	movl	$1, %r8d
	jmp	.L2310
.L2366:
	movl	%ebx, %r8d
	jmp	.L2310
	.cfi_endproc
.LFE3691:
	.size	_ZNK6icu_676number4impl15DecimalQuantityeqERKS2_, .-_ZNK6icu_676number4impl15DecimalQuantityeqERKS2_
	.section	.rodata.str1.1
.LC26:
	.string	"-"
.LC27:
	.string	"bytes"
.LC28:
	.string	"long"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"<DecimalQuantity %d:%d %s %s%s%s%d>"
	.section	.rodata.str1.1
.LC30:
	.string	"E"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15DecimalQuantity8toStringEv
	.type	_ZNK6icu_676number4impl15DecimalQuantity8toStringEv, @function
_ZNK6icu_676number4impl15DecimalQuantity8toStringEv:
.LFB3692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-179(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$160, %rsp
	movslq	16(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r13, -192(%rbp)
	leal	1(%rdi), %r14d
	movb	$0, -180(%rbp)
	movl	$30, -184(%rbp)
	cmpl	$30, %r14d
	jg	.L2391
.L2371:
	testl	%edi, %edi
	jle	.L2374
	xorl	%edx, %edx
	jmp	.L2378
	.p2align 4,,10
	.p2align 3
.L2392:
	testl	%ecx, %ecx
	js	.L2385
	cmpl	%ecx, %edi
	jle	.L2385
	movq	48(%rbx), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %edi
	addl	$48, %edi
	.p2align 4,,10
	.p2align 3
.L2376:
	movb	%dil, 0(%r13,%rdx)
	movslq	16(%rbx), %rdi
	addq	$1, %rdx
	movq	-192(%rbp), %r13
	cmpl	%edx, %edi
	jle	.L2374
.L2378:
	movl	%edi, %ecx
	subl	%edx, %ecx
	subl	$1, %ecx
	cmpb	$0, 64(%rbx)
	jne	.L2392
	movl	$48, %edi
	cmpl	$15, %ecx
	ja	.L2376
	movq	48(%rbx), %rdi
	sall	$2, %ecx
	shrq	%cl, %rdi
	andl	$15, %edi
	addl	$48, %edi
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2385:
	movl	$48, %edi
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2374:
	movb	$0, 0(%r13,%rdi)
	movl	16(%rbx), %eax
	leaq	.LC4(%rip), %rcx
	movl	12(%rbx), %esi
	testl	%eax, %eax
	je	.L2379
	movq	-192(%rbp), %rcx
.L2379:
	testb	$1, 20(%rbx)
	leaq	.LC10(%rip), %rax
	movl	36(%rbx), %r9d
	pushq	%rsi
	leaq	.LC26(%rip), %rdx
	leaq	.LC28(%rip), %rdi
	cmove	%rax, %rdx
	cmpb	$0, 64(%rbx)
	leaq	.LC27(%rip), %rax
	cmove	%rdi, %rax
	leaq	.LC30(%rip), %rsi
	pushq	%rsi
	leaq	-144(%rbp), %r13
	leaq	.LC29(%rip), %r8
	movl	$100, %esi
	pushq	%rcx
	movq	%r13, %rdi
	movl	$100, %ecx
	pushq	%rdx
	movl	$1, %edx
	pushq	%rax
	movl	40(%rbx), %eax
	pushq	%rax
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	addq	$48, %rsp
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	cmpb	$0, -180(%rbp)
	jne	.L2393
.L2370:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2394
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2391:
	.cfi_restore_state
	movslq	%r14d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2395
	cmpb	$0, -180(%rbp)
	jne	.L2396
.L2373:
	movq	%r13, -192(%rbp)
	movslq	16(%rbx), %rdi
	movl	%r14d, -184(%rbp)
	movb	$1, -180(%rbp)
	jmp	.L2371
	.p2align 4,,10
	.p2align 3
.L2393:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L2370
	.p2align 4,,10
	.p2align 3
.L2396:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L2373
.L2395:
	movslq	16(%rbx), %rdi
	movq	-192(%rbp), %r13
	jmp	.L2371
.L2394:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3692:
	.size	_ZNK6icu_676number4impl15DecimalQuantity8toStringEv, .-_ZNK6icu_676number4impl15DecimalQuantity8toStringEv
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6713IFixedDecimalE
	.section	.rodata._ZTSN6icu_6713IFixedDecimalE,"aG",@progbits,_ZTSN6icu_6713IFixedDecimalE,comdat
	.align 16
	.type	_ZTSN6icu_6713IFixedDecimalE, @object
	.size	_ZTSN6icu_6713IFixedDecimalE, 25
_ZTSN6icu_6713IFixedDecimalE:
	.string	"N6icu_6713IFixedDecimalE"
	.weak	_ZTIN6icu_6713IFixedDecimalE
	.section	.data.rel.ro._ZTIN6icu_6713IFixedDecimalE,"awG",@progbits,_ZTIN6icu_6713IFixedDecimalE,comdat
	.align 8
	.type	_ZTIN6icu_6713IFixedDecimalE, @object
	.size	_ZTIN6icu_6713IFixedDecimalE, 16
_ZTIN6icu_6713IFixedDecimalE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_6713IFixedDecimalE
	.weak	_ZTSN6icu_676number4impl15DecimalQuantityE
	.section	.rodata._ZTSN6icu_676number4impl15DecimalQuantityE,"aG",@progbits,_ZTSN6icu_676number4impl15DecimalQuantityE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl15DecimalQuantityE, @object
	.size	_ZTSN6icu_676number4impl15DecimalQuantityE, 39
_ZTSN6icu_676number4impl15DecimalQuantityE:
	.string	"N6icu_676number4impl15DecimalQuantityE"
	.weak	_ZTIN6icu_676number4impl15DecimalQuantityE
	.section	.data.rel.ro._ZTIN6icu_676number4impl15DecimalQuantityE,"awG",@progbits,_ZTIN6icu_676number4impl15DecimalQuantityE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl15DecimalQuantityE, @object
	.size	_ZTIN6icu_676number4impl15DecimalQuantityE, 56
_ZTIN6icu_676number4impl15DecimalQuantityE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl15DecimalQuantityE
	.long	0
	.long	2
	.quad	_ZTIN6icu_6713IFixedDecimalE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTVN6icu_6713IFixedDecimalE
	.section	.data.rel.ro._ZTVN6icu_6713IFixedDecimalE,"awG",@progbits,_ZTVN6icu_6713IFixedDecimalE,comdat
	.align 8
	.type	_ZTVN6icu_6713IFixedDecimalE, @object
	.size	_ZTVN6icu_6713IFixedDecimalE, 64
_ZTVN6icu_6713IFixedDecimalE:
	.quad	0
	.quad	_ZTIN6icu_6713IFixedDecimalE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_676number4impl15DecimalQuantityE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl15DecimalQuantityE,"awG",@progbits,_ZTVN6icu_676number4impl15DecimalQuantityE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl15DecimalQuantityE, @object
	.size	_ZTVN6icu_676number4impl15DecimalQuantityE, 64
_ZTVN6icu_676number4impl15DecimalQuantityE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl15DecimalQuantityE
	.quad	_ZN6icu_676number4impl15DecimalQuantityD1Ev
	.quad	_ZN6icu_676number4impl15DecimalQuantityD0Ev
	.quad	_ZNK6icu_676number4impl15DecimalQuantity16getPluralOperandENS_13PluralOperandE
	.quad	_ZNK6icu_676number4impl15DecimalQuantity5isNaNEv
	.quad	_ZNK6icu_676number4impl15DecimalQuantity10isInfiniteEv
	.quad	_ZNK6icu_676number4impl15DecimalQuantity15hasIntegerValueEv
	.section	.rodata
	.align 16
	.type	_ZZNK6icu_676number4impl15DecimalQuantity10fitsInLongEbE9INT64_BCD, @object
	.size	_ZZNK6icu_676number4impl15DecimalQuantity10fitsInLongEbE9INT64_BCD, 19
_ZZNK6icu_676number4impl15DecimalQuantity10fitsInLongEbE9INT64_BCD:
	.string	"\t\002\002\003\003\007\002"
	.string	"\003\006\b\005\004\007\007\005\b"
	.ascii	"\b"
	.align 32
	.type	_ZN12_GLOBAL__N_1L18DOUBLE_MULTIPLIERSE, @object
	.size	_ZN12_GLOBAL__N_1L18DOUBLE_MULTIPLIERSE, 176
_ZN12_GLOBAL__N_1L18DOUBLE_MULTIPLIERSE:
	.long	0
	.long	1072693248
	.long	0
	.long	1076101120
	.long	0
	.long	1079574528
	.long	0
	.long	1083129856
	.long	0
	.long	1086556160
	.long	0
	.long	1090021888
	.long	0
	.long	1093567616
	.long	0
	.long	1097011920
	.long	0
	.long	1100470148
	.long	0
	.long	1104006501
	.long	536870912
	.long	1107468383
	.long	3892314112
	.long	1110919286
	.long	2717908992
	.long	1114446484
	.long	3846176768
	.long	1117925532
	.long	512753664
	.long	1121369284
	.long	640942080
	.long	1124887541
	.long	937459712
	.long	1128383353
	.long	2245566464
	.long	1131820119
	.long	1733216256
	.long	1135329645
	.long	1620131072
	.long	1138841828
	.long	2025163840
	.long	1142271773
	.long	3605196624
	.long	1145772772
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC2:
	.long	158966641
	.long	1074434895
	.align 8
.LC3:
	.long	105764242
	.long	1149300943
	.align 8
.LC7:
	.long	0
	.long	-1048576
	.align 8
.LC8:
	.long	0
	.long	2146959360
	.align 8
.LC9:
	.long	0
	.long	2146435072
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC11:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.align 16
.LC12:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC13:
	.long	4294967295
	.long	2146435071
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
