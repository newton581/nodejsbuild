	.file	"number_modifiers.cpp"
	.text
	.section	.text._ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE,"axG",@progbits,_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.type	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE, @function
_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE:
.LFB3642:
	.cfi_startproc
	endbr64
	leal	(%rsi,%rdx,4), %eax
	cltq
	movq	8(%rdi,%rax,8), %rax
	testq	%rax, %rax
	jne	.L1
	cmpl	$5, %edx
	je	.L1
	addl	$20, %esi
	movslq	%esi, %rsi
	movq	8(%rdi,%rsi,8), %rax
.L1:
	ret
	.cfi_endproc
.LFE3642:
	.size	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE, .-_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl21AdoptingModifierStoreD2Ev
	.type	_ZN6icu_676number4impl21AdoptingModifierStoreD2Ev, @function
_ZN6icu_676number4impl21AdoptingModifierStoreD2Ev:
.LFB3662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl21AdoptingModifierStoreE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	200(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	8(%rdi), %rbx
	movq	%rax, (%rdi)
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L11
	movq	(%rdi), %rax
	call	*8(%rax)
.L11:
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jne	.L12
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3662:
	.size	_ZN6icu_676number4impl21AdoptingModifierStoreD2Ev, .-_ZN6icu_676number4impl21AdoptingModifierStoreD2Ev
	.globl	_ZN6icu_676number4impl21AdoptingModifierStoreD1Ev
	.set	_ZN6icu_676number4impl21AdoptingModifierStoreD1Ev,_ZN6icu_676number4impl21AdoptingModifierStoreD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl21ConstantAffixModifier15getPrefixLengthEv
	.type	_ZNK6icu_676number4impl21ConstantAffixModifier15getPrefixLengthEv, @function
_ZNK6icu_676number4impl21ConstantAffixModifier15getPrefixLengthEv:
.LFB3666:
	.cfi_startproc
	endbr64
	movswl	24(%rdi), %eax
	testw	%ax, %ax
	js	.L19
	sarl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movl	28(%rdi), %eax
	ret
	.cfi_endproc
.LFE3666:
	.size	_ZNK6icu_676number4impl21ConstantAffixModifier15getPrefixLengthEv, .-_ZNK6icu_676number4impl21ConstantAffixModifier15getPrefixLengthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl21ConstantAffixModifier8isStrongEv
	.type	_ZNK6icu_676number4impl21ConstantAffixModifier8isStrongEv, @function
_ZNK6icu_676number4impl21ConstantAffixModifier8isStrongEv:
.LFB3668:
	.cfi_startproc
	endbr64
	movzbl	145(%rdi), %eax
	ret
	.cfi_endproc
.LFE3668:
	.size	_ZNK6icu_676number4impl21ConstantAffixModifier8isStrongEv, .-_ZNK6icu_676number4impl21ConstantAffixModifier8isStrongEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl14SimpleModifier15getPrefixLengthEv
	.type	_ZNK6icu_676number4impl14SimpleModifier15getPrefixLengthEv, @function
_ZNK6icu_676number4impl14SimpleModifier15getPrefixLengthEv:
.LFB3682:
	.cfi_startproc
	endbr64
	movl	76(%rdi), %eax
	ret
	.cfi_endproc
.LFE3682:
	.size	_ZNK6icu_676number4impl14SimpleModifier15getPrefixLengthEv, .-_ZNK6icu_676number4impl14SimpleModifier15getPrefixLengthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl14SimpleModifier8isStrongEv
	.type	_ZNK6icu_676number4impl14SimpleModifier8isStrongEv, @function
_ZNK6icu_676number4impl14SimpleModifier8isStrongEv:
.LFB3684:
	.cfi_startproc
	endbr64
	movzbl	73(%rdi), %eax
	ret
	.cfi_endproc
.LFE3684:
	.size	_ZNK6icu_676number4impl14SimpleModifier8isStrongEv, .-_ZNK6icu_676number4impl14SimpleModifier8isStrongEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl14SimpleModifier13getParametersERNS1_8Modifier10ParametersE
	.type	_ZNK6icu_676number4impl14SimpleModifier13getParametersERNS1_8Modifier10ParametersE, @function
_ZNK6icu_676number4impl14SimpleModifier13getParametersERNS1_8Modifier10ParametersE:
.LFB3686:
	.cfi_startproc
	endbr64
	movdqu	88(%rdi), %xmm0
	movups	%xmm0, (%rsi)
	ret
	.cfi_endproc
.LFE3686:
	.size	_ZNK6icu_676number4impl14SimpleModifier13getParametersERNS1_8Modifier10ParametersE, .-_ZNK6icu_676number4impl14SimpleModifier13getParametersERNS1_8Modifier10ParametersE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl26ConstantMultiFieldModifier8isStrongEv
	.type	_ZNK6icu_676number4impl26ConstantMultiFieldModifier8isStrongEv, @function
_ZNK6icu_676number4impl26ConstantMultiFieldModifier8isStrongEv:
.LFB3693:
	.cfi_startproc
	endbr64
	movzbl	281(%rdi), %eax
	ret
	.cfi_endproc
.LFE3693:
	.size	_ZNK6icu_676number4impl26ConstantMultiFieldModifier8isStrongEv, .-_ZNK6icu_676number4impl26ConstantMultiFieldModifier8isStrongEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl26ConstantMultiFieldModifier13getParametersERNS1_8Modifier10ParametersE
	.type	_ZNK6icu_676number4impl26ConstantMultiFieldModifier13getParametersERNS1_8Modifier10ParametersE, @function
_ZNK6icu_676number4impl26ConstantMultiFieldModifier13getParametersERNS1_8Modifier10ParametersE:
.LFB3695:
	.cfi_startproc
	endbr64
	movdqu	288(%rdi), %xmm0
	movups	%xmm0, (%rsi)
	ret
	.cfi_endproc
.LFE3695:
	.size	_ZNK6icu_676number4impl26ConstantMultiFieldModifier13getParametersERNS1_8Modifier10ParametersE, .-_ZNK6icu_676number4impl26ConstantMultiFieldModifier13getParametersERNS1_8Modifier10ParametersE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl21AdoptingModifierStoreD0Ev
	.type	_ZN6icu_676number4impl21AdoptingModifierStoreD0Ev, @function
_ZN6icu_676number4impl21AdoptingModifierStoreD0Ev:
.LFB3664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl21AdoptingModifierStoreE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	200(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	8(%rdi), %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	.p2align 4,,10
	.p2align 3
.L29:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L28
	movq	(%rdi), %rax
	call	*8(%rax)
.L28:
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jne	.L29
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3664:
	.size	_ZN6icu_676number4impl21AdoptingModifierStoreD0Ev, .-_ZN6icu_676number4impl21AdoptingModifierStoreD0Ev
	.section	.text._ZN6icu_676number4impl14SimpleModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl14SimpleModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl14SimpleModifierD2Ev
	.type	_ZN6icu_676number4impl14SimpleModifierD2Ev, @function
_ZN6icu_676number4impl14SimpleModifierD2Ev:
.LFB4868:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	addq	$8, %rdi
	movq	%rax, -8(%rdi)
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE4868:
	.size	_ZN6icu_676number4impl14SimpleModifierD2Ev, .-_ZN6icu_676number4impl14SimpleModifierD2Ev
	.weak	_ZN6icu_676number4impl14SimpleModifierD1Ev
	.set	_ZN6icu_676number4impl14SimpleModifierD1Ev,_ZN6icu_676number4impl14SimpleModifierD2Ev
	.section	.text._ZN6icu_676number4impl14SimpleModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl14SimpleModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl14SimpleModifierD0Ev
	.type	_ZN6icu_676number4impl14SimpleModifierD0Ev, @function
_ZN6icu_676number4impl14SimpleModifierD0Ev:
.LFB4870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4870:
	.size	_ZN6icu_676number4impl14SimpleModifierD0Ev, .-_ZN6icu_676number4impl14SimpleModifierD0Ev
	.section	.text._ZN6icu_676number4impl26ConstantMultiFieldModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl26ConstantMultiFieldModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl26ConstantMultiFieldModifierD2Ev
	.type	_ZN6icu_676number4impl26ConstantMultiFieldModifierD2Ev, @function
_ZN6icu_676number4impl26ConstantMultiFieldModifierD2Ev:
.LFB3699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl26ConstantMultiFieldModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	144(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -144(%rdi)
	call	_ZN6icu_6722FormattedStringBuilderD1Ev@PLT
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6722FormattedStringBuilderD1Ev@PLT
	.cfi_endproc
.LFE3699:
	.size	_ZN6icu_676number4impl26ConstantMultiFieldModifierD2Ev, .-_ZN6icu_676number4impl26ConstantMultiFieldModifierD2Ev
	.weak	_ZN6icu_676number4impl26ConstantMultiFieldModifierD1Ev
	.set	_ZN6icu_676number4impl26ConstantMultiFieldModifierD1Ev,_ZN6icu_676number4impl26ConstantMultiFieldModifierD2Ev
	.section	.text._ZN6icu_676number4impl26ConstantMultiFieldModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl26ConstantMultiFieldModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl26ConstantMultiFieldModifierD0Ev
	.type	_ZN6icu_676number4impl26ConstantMultiFieldModifierD0Ev, @function
_ZN6icu_676number4impl26ConstantMultiFieldModifierD0Ev:
.LFB3701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl26ConstantMultiFieldModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	144(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -144(%rdi)
	call	_ZN6icu_6722FormattedStringBuilderD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6722FormattedStringBuilderD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3701:
	.size	_ZN6icu_676number4impl26ConstantMultiFieldModifierD0Ev, .-_ZN6icu_676number4impl26ConstantMultiFieldModifierD0Ev
	.section	.text._ZN6icu_676number4impl21ConstantAffixModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl21ConstantAffixModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl21ConstantAffixModifierD2Ev
	.type	_ZN6icu_676number4impl21ConstantAffixModifierD2Ev, @function
_ZN6icu_676number4impl21ConstantAffixModifierD2Ev:
.LFB4872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	104+_ZTVN6icu_676number4impl21ConstantAffixModifierE(%rip), %rax
	leaq	-88(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	80(%rdi), %rdi
	subq	$8, %rsp
	movups	%xmm0, -80(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE4872:
	.size	_ZN6icu_676number4impl21ConstantAffixModifierD2Ev, .-_ZN6icu_676number4impl21ConstantAffixModifierD2Ev
	.weak	_ZN6icu_676number4impl21ConstantAffixModifierD1Ev
	.set	_ZN6icu_676number4impl21ConstantAffixModifierD1Ev,_ZN6icu_676number4impl21ConstantAffixModifierD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl21ConstantAffixModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.type	_ZNK6icu_676number4impl21ConstantAffixModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, @function
_ZNK6icu_676number4impl21ConstantAffixModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode:
.LFB3665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	leaq	80(%rdi), %rdx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	%ecx, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	144(%rdi), %ecx
	movq	%r13, %rdi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	leaq	16(%rbx), %rdx
	movq	%r14, %r8
	movl	%r15d, %esi
	movzbl	144(%rbx), %ecx
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	addq	$8, %rsp
	popq	%rbx
	addl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3665:
	.size	_ZNK6icu_676number4impl21ConstantAffixModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, .-_ZNK6icu_676number4impl21ConstantAffixModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl21ConstantAffixModifier17getCodePointCountEv
	.type	_ZNK6icu_676number4impl21ConstantAffixModifier17getCodePointCountEv, @function
_ZNK6icu_676number4impl21ConstantAffixModifier17getCodePointCountEv:
.LFB3667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2147483647, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	call	_ZNK6icu_6713UnicodeString11countChar32Eii@PLT
	leaq	80(%rbx), %rdi
	movl	$2147483647, %edx
	xorl	%esi, %esi
	movl	%eax, %r12d
	call	_ZNK6icu_6713UnicodeString11countChar32Eii@PLT
	popq	%rbx
	addl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3667:
	.size	_ZNK6icu_676number4impl21ConstantAffixModifier17getCodePointCountEv, .-_ZNK6icu_676number4impl21ConstantAffixModifier17getCodePointCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl14SimpleModifier17getCodePointCountEv
	.type	_ZNK6icu_676number4impl14SimpleModifier17getCodePointCountEv, @function
_ZNK6icu_676number4impl14SimpleModifier17getCodePointCountEv:
.LFB3683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movl	76(%rdi), %edx
	movq	%rdi, %rbx
	testl	%edx, %edx
	jle	.L49
	leaq	8(%rdi), %rdi
	movl	$2, %esi
	call	_ZNK6icu_6713UnicodeString11countChar32Eii@PLT
	movl	%eax, %r12d
.L49:
	movl	84(%rbx), %edx
	testl	%edx, %edx
	jle	.L48
	movl	80(%rbx), %eax
	leaq	8(%rbx), %rdi
	leal	1(%rax), %esi
	call	_ZNK6icu_6713UnicodeString11countChar32Eii@PLT
	addl	%eax, %r12d
.L48:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3683:
	.size	_ZNK6icu_676number4impl14SimpleModifier17getCodePointCountEv, .-_ZNK6icu_676number4impl14SimpleModifier17getCodePointCountEv
	.section	.text.unlikely,"ax",@progbits
	.align 2
	.globl	_ZNK6icu_676number4impl21ConstantAffixModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.type	_ZNK6icu_676number4impl21ConstantAffixModifier13containsFieldENS_22FormattedStringBuilder5FieldE, @function
_ZNK6icu_676number4impl21ConstantAffixModifier13containsFieldENS_22FormattedStringBuilder5FieldE:
.LFB3669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	abort@PLT
	.cfi_endproc
.LFE3669:
	.size	_ZNK6icu_676number4impl21ConstantAffixModifier13containsFieldENS_22FormattedStringBuilder5FieldE, .-_ZNK6icu_676number4impl21ConstantAffixModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.globl	_ZNK6icu_676number4impl14SimpleModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.set	_ZNK6icu_676number4impl14SimpleModifier13containsFieldENS_22FormattedStringBuilder5FieldE,_ZNK6icu_676number4impl21ConstantAffixModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.align 2
	.globl	_ZNK6icu_676number4impl21ConstantAffixModifier13getParametersERNS1_8Modifier10ParametersE
	.type	_ZNK6icu_676number4impl21ConstantAffixModifier13getParametersERNS1_8Modifier10ParametersE, @function
_ZNK6icu_676number4impl21ConstantAffixModifier13getParametersERNS1_8Modifier10ParametersE:
.LFB3670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	abort@PLT
	.cfi_endproc
.LFE3670:
	.size	_ZNK6icu_676number4impl21ConstantAffixModifier13getParametersERNS1_8Modifier10ParametersE, .-_ZNK6icu_676number4impl21ConstantAffixModifier13getParametersERNS1_8Modifier10ParametersE
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl26ConstantMultiFieldModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.type	_ZNK6icu_676number4impl26ConstantMultiFieldModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, @function
_ZNK6icu_676number4impl26ConstantMultiFieldModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode:
.LFB3690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	%edx, %esi
	leaq	8(%rdi), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	movq	%r8, %rcx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%esi, -136(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKS0_R10UErrorCode@PLT
	movl	-136(%rbp), %esi
	cmpb	$0, 280(%r12)
	movl	%eax, %ebx
	jne	.L62
.L59:
	leal	(%rbx,%r13), %esi
	leaq	144(%r12), %rdx
	movq	%r15, %rcx
	movq	%r14, %rdi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKS0_R10UErrorCode@PLT
	addl	%ebx, %eax
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L63
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	pushq	%r15
	leaq	-128(%rbp), %r10
	addl	%ebx, %esi
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	leal	(%rbx,%r13), %edx
	movq	%r10, %rcx
	movw	%ax, -120(%rbp)
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %eax
	movq	%r10, -136(%rbp)
	pushq	%rax
	call	_ZN6icu_6722FormattedStringBuilder6spliceEiiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode@PLT
	movq	-136(%rbp), %r10
	addl	%eax, %ebx
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	popq	%rdx
	popq	%rcx
	jmp	.L59
.L63:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3690:
	.size	_ZNK6icu_676number4impl26ConstantMultiFieldModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, .-_ZNK6icu_676number4impl26ConstantMultiFieldModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl26ConstantMultiFieldModifier15getPrefixLengthEv
	.type	_ZNK6icu_676number4impl26ConstantMultiFieldModifier15getPrefixLengthEv, @function
_ZNK6icu_676number4impl26ConstantMultiFieldModifier15getPrefixLengthEv:
.LFB3691:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	jmp	_ZNK6icu_6722FormattedStringBuilder6lengthEv@PLT
	.cfi_endproc
.LFE3691:
	.size	_ZNK6icu_676number4impl26ConstantMultiFieldModifier15getPrefixLengthEv, .-_ZNK6icu_676number4impl26ConstantMultiFieldModifier15getPrefixLengthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl26ConstantMultiFieldModifier17getCodePointCountEv
	.type	_ZNK6icu_676number4impl26ConstantMultiFieldModifier17getCodePointCountEv, @function
_ZNK6icu_676number4impl26ConstantMultiFieldModifier17getCodePointCountEv:
.LFB3692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	call	_ZNK6icu_6722FormattedStringBuilder14codePointCountEv@PLT
	leaq	144(%rbx), %rdi
	movl	%eax, %r12d
	call	_ZNK6icu_6722FormattedStringBuilder14codePointCountEv@PLT
	popq	%rbx
	addl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3692:
	.size	_ZNK6icu_676number4impl26ConstantMultiFieldModifier17getCodePointCountEv, .-_ZNK6icu_676number4impl26ConstantMultiFieldModifier17getCodePointCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl26ConstantMultiFieldModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.type	_ZNK6icu_676number4impl26ConstantMultiFieldModifier13containsFieldENS_22FormattedStringBuilder5FieldE, @function
_ZNK6icu_676number4impl26ConstantMultiFieldModifier13containsFieldENS_22FormattedStringBuilder5FieldE:
.LFB3694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$8, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder13containsFieldENS0_5FieldE@PLT
	testb	%al, %al
	je	.L70
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	leaq	144(%rbx), %rdi
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6722FormattedStringBuilder13containsFieldENS0_5FieldE@PLT
	.cfi_endproc
.LFE3694:
	.size	_ZNK6icu_676number4impl26ConstantMultiFieldModifier13containsFieldENS_22FormattedStringBuilder5FieldE, .-_ZNK6icu_676number4impl26ConstantMultiFieldModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl26ConstantMultiFieldModifier22semanticallyEquivalentERKNS1_8ModifierE
	.type	_ZNK6icu_676number4impl26ConstantMultiFieldModifier22semanticallyEquivalentERKNS1_8ModifierE, @function
_ZNK6icu_676number4impl26ConstantMultiFieldModifier22semanticallyEquivalentERKNS1_8ModifierE:
.LFB3696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_676number4impl26ConstantMultiFieldModifierE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	_ZTIN6icu_676number4impl8ModifierE(%rip), %rsi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L75
	movq	%rax, %rbx
	movq	288(%r12), %rax
	testq	%rax, %rax
	je	.L74
	cmpq	%rax, 288(%rbx)
	sete	%al
.L71:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	leaq	8(%rbx), %rsi
	leaq	8(%r12), %rdi
	call	_ZNK6icu_6722FormattedStringBuilder13contentEqualsERKS0_@PLT
	testb	%al, %al
	jne	.L83
.L75:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	leaq	144(%rbx), %rsi
	leaq	144(%r12), %rdi
	call	_ZNK6icu_6722FormattedStringBuilder13contentEqualsERKS0_@PLT
	testb	%al, %al
	je	.L75
	movzwl	280(%rbx), %eax
	cmpw	%ax, 280(%r12)
	sete	%al
	jmp	.L71
	.cfi_endproc
.LFE3696:
	.size	_ZNK6icu_676number4impl26ConstantMultiFieldModifier22semanticallyEquivalentERKNS1_8ModifierE, .-_ZNK6icu_676number4impl26ConstantMultiFieldModifier22semanticallyEquivalentERKNS1_8ModifierE
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"["
	.string	":"
	.string	"d"
	.string	"i"
	.string	"g"
	.string	"i"
	.string	"t"
	.string	":"
	.string	"]"
	.string	""
	.string	""
	.align 2
.LC1:
	.string	"["
	.string	":"
	.string	"^"
	.string	"S"
	.string	":"
	.string	"]"
	.string	""
	.string	""
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_126initDefaultCurrencySpacingER10UErrorCode, @function
_ZN12_GLOBAL__N_126initDefaultCurrencySpacingER10UErrorCode:
.LFB3646:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN12_GLOBAL__N_129cleanupDefaultCurrencySpacingEv(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$2, %edi
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	ucln_i18n_registerCleanup_67@PLT
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L85
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
.L85:
	movq	%r12, %rdi
	movq	%rbx, _ZN12_GLOBAL__N_112UNISET_DIGITE(%rip)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L86
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
.L86:
	movq	%r12, %rdi
	movq	%rbx, _ZN12_GLOBAL__N_111UNISET_NOTSE(%rip)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	_ZN12_GLOBAL__N_112UNISET_DIGITE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L87
	cmpq	$0, _ZN12_GLOBAL__N_111UNISET_NOTSE(%rip)
	je	.L87
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	_ZN12_GLOBAL__N_111UNISET_NOTSE(%rip), %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
.L84:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movl	$7, 0(%r13)
	jmp	.L84
.L101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3646:
	.size	_ZN12_GLOBAL__N_126initDefaultCurrencySpacingER10UErrorCode, .-_ZN12_GLOBAL__N_126initDefaultCurrencySpacingER10UErrorCode
	.section	.text._ZN6icu_676number4impl21ConstantAffixModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl21ConstantAffixModifierD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N6icu_676number4impl21ConstantAffixModifierD0Ev
	.type	_ZThn8_N6icu_676number4impl21ConstantAffixModifierD0Ev, @function
_ZThn8_N6icu_676number4impl21ConstantAffixModifierD0Ev:
.LFB4899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	104+_ZTVN6icu_676number4impl21ConstantAffixModifierE(%rip), %rax
	leaq	-88(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 13, -24
	leaq	-8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	72(%rdi), %rdi
	movups	%xmm0, -80(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4899:
	.size	_ZThn8_N6icu_676number4impl21ConstantAffixModifierD0Ev, .-_ZThn8_N6icu_676number4impl21ConstantAffixModifierD0Ev
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl21ConstantAffixModifierD0Ev
	.type	_ZN6icu_676number4impl21ConstantAffixModifierD0Ev, @function
_ZN6icu_676number4impl21ConstantAffixModifierD0Ev:
.LFB4874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	104+_ZTVN6icu_676number4impl21ConstantAffixModifierE(%rip), %rax
	leaq	-88(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	80(%rdi), %rdi
	subq	$8, %rsp
	movups	%xmm0, -80(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4874:
	.size	_ZN6icu_676number4impl21ConstantAffixModifierD0Ev, .-_ZN6icu_676number4impl21ConstantAffixModifierD0Ev
	.section	.text._ZN6icu_676number4impl21ConstantAffixModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl21ConstantAffixModifierD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N6icu_676number4impl21ConstantAffixModifierD1Ev
	.type	_ZThn8_N6icu_676number4impl21ConstantAffixModifierD1Ev, @function
_ZThn8_N6icu_676number4impl21ConstantAffixModifierD1Ev:
.LFB4900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	104+_ZTVN6icu_676number4impl21ConstantAffixModifierE(%rip), %rax
	leaq	-88(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	movups	%xmm0, -80(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE4900:
	.size	_ZThn8_N6icu_676number4impl21ConstantAffixModifierD1Ev, .-_ZThn8_N6icu_676number4impl21ConstantAffixModifierD1Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl21ConstantAffixModifier22semanticallyEquivalentERKNS1_8ModifierE
	.type	_ZNK6icu_676number4impl21ConstantAffixModifier22semanticallyEquivalentERKNS1_8ModifierE, @function
_ZNK6icu_676number4impl21ConstantAffixModifier22semanticallyEquivalentERKNS1_8ModifierE:
.LFB3671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_676number4impl21ConstantAffixModifierE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	_ZTIN6icu_676number4impl8ModifierE(%rip), %rsi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L116
	movq	%rax, %rbx
	movswl	24(%rax), %eax
	movswl	24(%r12), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L111
	testw	%dx, %dx
	js	.L112
	sarl	$5, %edx
.L113:
	testw	%ax, %ax
	js	.L114
	sarl	$5, %eax
.L115:
	testb	%cl, %cl
	jne	.L116
	cmpl	%edx, %eax
	je	.L141
	.p2align 4,,10
	.p2align 3
.L116:
	xorl	%eax, %eax
.L108:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L111:
	testb	%cl, %cl
	je	.L116
	movswl	88(%rbx), %eax
	movswl	88(%r12), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L117
	testw	%dx, %dx
	js	.L118
	sarl	$5, %edx
.L119:
	testw	%ax, %ax
	js	.L120
	sarl	$5, %eax
.L121:
	cmpl	%edx, %eax
	jne	.L116
	testb	%cl, %cl
	jne	.L116
	leaq	80(%rbx), %rsi
	leaq	80(%r12), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L117:
	testb	%cl, %cl
	je	.L116
	movzbl	144(%rbx), %eax
	cmpb	%al, 144(%r12)
	jne	.L116
	movzbl	145(%rbx), %eax
	cmpb	%al, 145(%r12)
	sete	%al
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L114:
	movl	28(%rbx), %eax
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L112:
	movl	28(%r12), %edx
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L120:
	movl	92(%rbx), %eax
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L118:
	movl	92(%r12), %edx
	jmp	.L119
	.cfi_endproc
.LFE3671:
	.size	_ZNK6icu_676number4impl21ConstantAffixModifier22semanticallyEquivalentERKNS1_8ModifierE, .-_ZNK6icu_676number4impl21ConstantAffixModifier22semanticallyEquivalentERKNS1_8ModifierE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl14SimpleModifier22semanticallyEquivalentERKNS1_8ModifierE
	.type	_ZNK6icu_676number4impl14SimpleModifier22semanticallyEquivalentERKNS1_8ModifierE, @function
_ZNK6icu_676number4impl14SimpleModifier22semanticallyEquivalentERKNS1_8ModifierE:
.LFB3687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_676number4impl14SimpleModifierE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	_ZTIN6icu_676number4impl8ModifierE(%rip), %rsi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L151
	movq	%rax, %rbx
	movq	88(%r12), %rax
	testq	%rax, %rax
	je	.L145
	cmpq	%rax, 88(%rbx)
	sete	%al
.L142:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movswl	16(%rbx), %eax
	movswl	16(%r12), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L146
	testw	%dx, %dx
	js	.L147
	sarl	$5, %edx
.L148:
	testw	%ax, %ax
	js	.L149
	sarl	$5, %eax
.L150:
	testb	%cl, %cl
	jne	.L151
	cmpl	%edx, %eax
	je	.L162
.L151:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	leaq	8(%rbx), %rsi
	leaq	8(%r12), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L146:
	testb	%cl, %cl
	je	.L151
	movzbl	72(%rbx), %eax
	cmpb	%al, 72(%r12)
	jne	.L151
	movzbl	73(%rbx), %eax
	cmpb	%al, 73(%r12)
	sete	%al
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L149:
	movl	20(%rbx), %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L147:
	movl	20(%r12), %edx
	jmp	.L148
	.cfi_endproc
.LFE3687:
	.size	_ZNK6icu_676number4impl14SimpleModifier22semanticallyEquivalentERKNS1_8ModifierE, .-_ZNK6icu_676number4impl14SimpleModifier22semanticallyEquivalentERKNS1_8ModifierE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl14SimpleModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.type	_ZNK6icu_676number4impl14SimpleModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, @function
_ZNK6icu_676number4impl14SimpleModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode:
.LFB3681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	%edx, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpl	$-1, 80(%rdi)
	movq	%rdi, %rbx
	movl	76(%rdi), %eax
	jne	.L164
	movl	84(%rdi), %edx
	addl	%eax, %edx
	testl	%edx, %edx
	jle	.L164
	pushq	%r8
	movzbl	72(%rdi), %edx
	leaq	8(%rdi), %rcx
	leal	2(%rax), %r9d
	movl	$2, %r8d
	movq	%r13, %rdi
	pushq	%rdx
	movl	%r14d, %edx
	call	_ZN6icu_6722FormattedStringBuilder6spliceEiiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode@PLT
	popq	%r8
	popq	%r9
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	testl	%eax, %eax
	jg	.L169
	movl	84(%rbx), %esi
	testl	%esi, %esi
	jg	.L170
.L167:
	leaq	-32(%rbp), %rsp
	addl	%esi, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	subq	$8, %rsp
	movl	80(%rbx), %edi
	addl	%eax, %r14d
	movzbl	72(%rbx), %r9d
	pushq	%r12
	leaq	8(%rbx), %rdx
	leal	1(%rdi), %ecx
	movq	%r13, %rdi
	leal	(%rsi,%rcx), %r8d
	movl	%r14d, %esi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode@PLT
	popq	%rdx
	movl	76(%rbx), %eax
	movl	84(%rbx), %esi
	popq	%rcx
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L169:
	subq	$8, %rsp
	movzbl	72(%rbx), %r9d
	movq	%r13, %rdi
	leaq	8(%rbx), %rdx
	pushq	%r12
	leal	2(%rax), %r8d
	movl	$2, %ecx
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode@PLT
	popq	%rsi
	movl	84(%rbx), %esi
	movl	76(%rbx), %eax
	popq	%rdi
	testl	%esi, %esi
	jle	.L167
	jmp	.L170
	.cfi_endproc
.LFE3681:
	.size	_ZNK6icu_676number4impl14SimpleModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, .-_ZNK6icu_676number4impl14SimpleModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl8ModifierD2Ev
	.type	_ZN6icu_676number4impl8ModifierD2Ev, @function
_ZN6icu_676number4impl8ModifierD2Ev:
.LFB3648:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3648:
	.size	_ZN6icu_676number4impl8ModifierD2Ev, .-_ZN6icu_676number4impl8ModifierD2Ev
	.globl	_ZN6icu_676number4impl8ModifierD1Ev
	.set	_ZN6icu_676number4impl8ModifierD1Ev,_ZN6icu_676number4impl8ModifierD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl8ModifierD0Ev
	.type	_ZN6icu_676number4impl8ModifierD0Ev, @function
_ZN6icu_676number4impl8ModifierD0Ev:
.LFB3650:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3650:
	.size	_ZN6icu_676number4impl8ModifierD0Ev, .-_ZN6icu_676number4impl8ModifierD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl8Modifier10ParametersC2Ev
	.type	_ZN6icu_676number4impl8Modifier10ParametersC2Ev, @function
_ZN6icu_676number4impl8Modifier10ParametersC2Ev:
.LFB3652:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	ret
	.cfi_endproc
.LFE3652:
	.size	_ZN6icu_676number4impl8Modifier10ParametersC2Ev, .-_ZN6icu_676number4impl8Modifier10ParametersC2Ev
	.globl	_ZN6icu_676number4impl8Modifier10ParametersC1Ev
	.set	_ZN6icu_676number4impl8Modifier10ParametersC1Ev,_ZN6icu_676number4impl8Modifier10ParametersC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl8Modifier10ParametersC2EPKNS1_13ModifierStoreENS1_6SignumENS_14StandardPlural4FormE
	.type	_ZN6icu_676number4impl8Modifier10ParametersC2EPKNS1_13ModifierStoreENS1_6SignumENS_14StandardPlural4FormE, @function
_ZN6icu_676number4impl8Modifier10ParametersC2EPKNS1_13ModifierStoreENS1_6SignumENS_14StandardPlural4FormE:
.LFB3655:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	%edx, 8(%rdi)
	movl	%ecx, 12(%rdi)
	ret
	.cfi_endproc
.LFE3655:
	.size	_ZN6icu_676number4impl8Modifier10ParametersC2EPKNS1_13ModifierStoreENS1_6SignumENS_14StandardPlural4FormE, .-_ZN6icu_676number4impl8Modifier10ParametersC2EPKNS1_13ModifierStoreENS1_6SignumENS_14StandardPlural4FormE
	.globl	_ZN6icu_676number4impl8Modifier10ParametersC1EPKNS1_13ModifierStoreENS1_6SignumENS_14StandardPlural4FormE
	.set	_ZN6icu_676number4impl8Modifier10ParametersC1EPKNS1_13ModifierStoreENS1_6SignumENS_14StandardPlural4FormE,_ZN6icu_676number4impl8Modifier10ParametersC2EPKNS1_13ModifierStoreENS1_6SignumENS_14StandardPlural4FormE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl13ModifierStoreD2Ev
	.type	_ZN6icu_676number4impl13ModifierStoreD2Ev, @function
_ZN6icu_676number4impl13ModifierStoreD2Ev:
.LFB3658:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3658:
	.size	_ZN6icu_676number4impl13ModifierStoreD2Ev, .-_ZN6icu_676number4impl13ModifierStoreD2Ev
	.globl	_ZN6icu_676number4impl13ModifierStoreD1Ev
	.set	_ZN6icu_676number4impl13ModifierStoreD1Ev,_ZN6icu_676number4impl13ModifierStoreD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl13ModifierStoreD0Ev
	.type	_ZN6icu_676number4impl13ModifierStoreD0Ev, @function
_ZN6icu_676number4impl13ModifierStoreD0Ev:
.LFB3660:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3660:
	.size	_ZN6icu_676number4impl13ModifierStoreD0Ev, .-_ZN6icu_676number4impl13ModifierStoreD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SimpleModifierC2ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEb
	.type	_ZN6icu_676number4impl14SimpleModifierC2ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEb, @function
_ZN6icu_676number4impl14SimpleModifierC2ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEb:
.LFB3673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	addq	$8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$4294967295, %eax
	movb	%r13b, 72(%rbx)
	movq	%rax, 80(%rbx)
	movzwl	16(%rbx), %eax
	movb	%r12b, 73(%rbx)
	movl	$0, 76(%rbx)
	movq	$0, 88(%rbx)
	movl	$0, 96(%rbx)
	movl	$0, 100(%rbx)
	testw	%ax, %ax
	js	.L178
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	$17, %al
	jne	.L195
.L207:
	leaq	18(%rbx), %rcx
	testb	$2, %al
	je	.L204
.L180:
	testl	%edx, %edx
	je	.L197
	cmpw	$0, (%rcx)
	jne	.L183
	cmpl	$1, %edx
	je	.L197
	testb	$2, %al
	jne	.L205
	movq	32(%rbx), %rax
.L185:
	movzwl	2(%rax), %eax
	subl	$256, %eax
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L197:
	movl	$65279, %eax
.L182:
	movl	%eax, 76(%rbx)
.L177:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	movq	32(%rbx), %rcx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L205:
	leaq	18(%rbx), %rax
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L183:
	cmpl	$1, %edx
	je	.L198
	testb	$2, %al
	je	.L188
	movzwl	20(%rbx), %ecx
	testw	%cx, %cx
	jne	.L189
.L190:
	movl	$2, 80(%rbx)
	movl	$2, %esi
	movl	$3, %ecx
.L191:
	cmpl	%ecx, %edx
	jle	.L177
	movl	$65279, %ecx
	cmpl	%edx, %esi
	jnb	.L192
	testb	$2, %al
	jne	.L206
	movq	32(%rbx), %rax
.L194:
	movzwl	(%rax,%rsi,2), %ecx
	subl	$256, %ecx
.L192:
	movl	%ecx, 84(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movl	20(%rbx), %edx
	testb	$17, %al
	je	.L207
.L195:
	xorl	%ecx, %ecx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L198:
	movl	$65282, %ecx
	movl	$65279, %esi
.L187:
	movl	%esi, 76(%rbx)
	movslq	%ecx, %rsi
	movl	%ecx, 80(%rbx)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L188:
	movq	32(%rbx), %rcx
	movzwl	2(%rcx), %ecx
	testw	%cx, %cx
	je	.L190
.L189:
	leal	-256(%rcx), %esi
	subl	$253, %ecx
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L206:
	leaq	18(%rbx), %rax
	jmp	.L194
	.cfi_endproc
.LFE3673:
	.size	_ZN6icu_676number4impl14SimpleModifierC2ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEb, .-_ZN6icu_676number4impl14SimpleModifierC2ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEb
	.globl	_ZN6icu_676number4impl14SimpleModifierC1ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEb
	.set	_ZN6icu_676number4impl14SimpleModifierC1ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEb,_ZN6icu_676number4impl14SimpleModifierC2ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEb
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SimpleModifierC2ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEbNS1_8Modifier10ParametersE
	.type	_ZN6icu_676number4impl14SimpleModifierC2ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEbNS1_8Modifier10ParametersE, @function
_ZN6icu_676number4impl14SimpleModifierC2ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEbNS1_8Modifier10ParametersE:
.LFB3676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	addq	$8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$4294967295, %eax
	movb	%r15b, 72(%rbx)
	movq	%rax, 80(%rbx)
	movzwl	16(%rbx), %eax
	movb	%r14b, 73(%rbx)
	movl	$0, 76(%rbx)
	movq	%r12, 88(%rbx)
	movq	%r13, 96(%rbx)
	testw	%ax, %ax
	js	.L209
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	$17, %al
	jne	.L226
.L238:
	leaq	18(%rbx), %rcx
	testb	$2, %al
	je	.L235
.L211:
	testl	%edx, %edx
	je	.L228
	cmpw	$0, (%rcx)
	jne	.L214
	cmpl	$1, %edx
	je	.L228
	testb	$2, %al
	jne	.L236
	movq	32(%rbx), %rax
.L216:
	movzwl	2(%rax), %eax
	subl	$256, %eax
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L228:
	movl	$65279, %eax
.L213:
	movl	%eax, 76(%rbx)
.L208:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	movq	32(%rbx), %rcx
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L236:
	leaq	18(%rbx), %rax
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L214:
	cmpl	$1, %edx
	je	.L229
	testb	$2, %al
	je	.L219
	movzwl	20(%rbx), %ecx
	testw	%cx, %cx
	jne	.L220
.L221:
	movl	$2, 80(%rbx)
	movl	$2, %esi
	movl	$3, %ecx
.L222:
	cmpl	%ecx, %edx
	jle	.L208
	movl	$65279, %ecx
	cmpl	%edx, %esi
	jnb	.L223
	testb	$2, %al
	jne	.L237
	movq	32(%rbx), %rax
.L225:
	movzwl	(%rax,%rsi,2), %ecx
	subl	$256, %ecx
.L223:
	movl	%ecx, 84(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movl	20(%rbx), %edx
	testb	$17, %al
	je	.L238
.L226:
	xorl	%ecx, %ecx
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$65282, %ecx
	movl	$65279, %esi
.L218:
	movl	%esi, 76(%rbx)
	movslq	%ecx, %rsi
	movl	%ecx, 80(%rbx)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L219:
	movq	32(%rbx), %rcx
	movzwl	2(%rcx), %ecx
	testw	%cx, %cx
	je	.L221
.L220:
	leal	-256(%rcx), %esi
	subl	$253, %ecx
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L237:
	leaq	18(%rbx), %rax
	jmp	.L225
	.cfi_endproc
.LFE3676:
	.size	_ZN6icu_676number4impl14SimpleModifierC2ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEbNS1_8Modifier10ParametersE, .-_ZN6icu_676number4impl14SimpleModifierC2ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEbNS1_8Modifier10ParametersE
	.globl	_ZN6icu_676number4impl14SimpleModifierC1ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEbNS1_8Modifier10ParametersE
	.set	_ZN6icu_676number4impl14SimpleModifierC1ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEbNS1_8Modifier10ParametersE,_ZN6icu_676number4impl14SimpleModifierC2ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEbNS1_8Modifier10ParametersE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SimpleModifierC2Ev
	.type	_ZN6icu_676number4impl14SimpleModifierC2Ev, @function
_ZN6icu_676number4impl14SimpleModifierC2Ev:
.LFB3679:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rdx
	movl	$0, 76(%rdi)
	movq	%rdx, %xmm0
	movq	%rax, %xmm1
	movl	$2, %eax
	xorl	%edx, %edx
	movw	%ax, 16(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movl	$4294967295, %eax
	movw	%dx, 72(%rdi)
	movq	%rax, 80(%rdi)
	movq	$0, 88(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE3679:
	.size	_ZN6icu_676number4impl14SimpleModifierC2Ev, .-_ZN6icu_676number4impl14SimpleModifierC2Ev
	.globl	_ZN6icu_676number4impl14SimpleModifierC1Ev
	.set	_ZN6icu_676number4impl14SimpleModifierC1Ev,_ZN6icu_676number4impl14SimpleModifierC2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl14SimpleModifier20formatAsPrefixSuffixERNS_22FormattedStringBuilderEiiR10UErrorCode
	.type	_ZNK6icu_676number4impl14SimpleModifier20formatAsPrefixSuffixERNS_22FormattedStringBuilderEiiR10UErrorCode, @function
_ZNK6icu_676number4impl14SimpleModifier20formatAsPrefixSuffixERNS_22FormattedStringBuilderEiiR10UErrorCode:
.LFB3688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	%edx, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpl	$-1, 80(%rdi)
	movq	%rdi, %rbx
	movl	76(%rdi), %eax
	jne	.L241
	movl	84(%rdi), %edx
	addl	%eax, %edx
	testl	%edx, %edx
	jle	.L241
	pushq	%r8
	movzbl	72(%rdi), %edx
	leaq	8(%rdi), %rcx
	leal	2(%rax), %r9d
	movl	$2, %r8d
	movq	%r13, %rdi
	pushq	%rdx
	movl	%r14d, %edx
	call	_ZN6icu_6722FormattedStringBuilder6spliceEiiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode@PLT
	popq	%r8
	popq	%r9
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	testl	%eax, %eax
	jg	.L246
	movl	84(%rbx), %esi
	testl	%esi, %esi
	jg	.L247
.L244:
	leaq	-32(%rbp), %rsp
	addl	%esi, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	subq	$8, %rsp
	movl	80(%rbx), %edi
	addl	%eax, %r14d
	movzbl	72(%rbx), %r9d
	pushq	%r12
	leaq	8(%rbx), %rdx
	leal	1(%rdi), %ecx
	movq	%r13, %rdi
	leal	(%rsi,%rcx), %r8d
	movl	%r14d, %esi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode@PLT
	popq	%rdx
	movl	76(%rbx), %eax
	movl	84(%rbx), %esi
	popq	%rcx
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L246:
	subq	$8, %rsp
	movzbl	72(%rbx), %r9d
	movq	%r13, %rdi
	leaq	8(%rbx), %rdx
	pushq	%r12
	leal	2(%rax), %r8d
	movl	$2, %ecx
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode@PLT
	popq	%rsi
	movl	84(%rbx), %esi
	movl	76(%rbx), %eax
	popq	%rdi
	testl	%esi, %esi
	jle	.L244
	jmp	.L247
	.cfi_endproc
.LFE3688:
	.size	_ZNK6icu_676number4impl14SimpleModifier20formatAsPrefixSuffixERNS_22FormattedStringBuilderEiiR10UErrorCode, .-_ZNK6icu_676number4impl14SimpleModifier20formatAsPrefixSuffixERNS_22FormattedStringBuilderEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SimpleModifier19formatTwoArgPatternERKNS_15SimpleFormatterERNS_22FormattedStringBuilderEiPiS8_NS6_5FieldER10UErrorCode
	.type	_ZN6icu_676number4impl14SimpleModifier19formatTwoArgPatternERKNS_15SimpleFormatterERNS_22FormattedStringBuilderEiPiS8_NS6_5FieldER10UErrorCode, @function
_ZN6icu_676number4impl14SimpleModifier19formatTwoArgPatternERKNS_15SimpleFormatterERNS_22FormattedStringBuilderEiPiS8_NS6_5FieldER10UErrorCode:
.LFB3689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$40, %rsp
	movzwl	16(%rbx), %eax
	testw	%ax, %ax
	js	.L249
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testb	$17, %al
	jne	.L272
.L286:
	leaq	18(%rbx), %rdx
	testb	$2, %al
	je	.L283
.L251:
	testl	%ecx, %ecx
	je	.L253
	cmpw	$2, (%rdx)
	je	.L254
.L253:
	movq	16(%rbp), %rax
	xorl	%r15d, %r15d
	movl	$5, (%rax)
.L248:
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movq	32(%rbx), %rdx
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L254:
	leaq	8(%rbx), %rdx
	cmpl	$1, %ecx
	je	.L273
	leaq	18(%rbx), %rcx
	testb	$2, %al
	jne	.L258
	movq	32(%rbx), %rcx
.L258:
	movzwl	2(%rcx), %r10d
	cmpl	$255, %r10d
	jg	.L284
	movl	$0, -52(%rbp)
	movl	$3, %r10d
	movl	$2, %r11d
	movl	$2, %r15d
.L259:
	testw	%ax, %ax
	js	.L260
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L261:
	cmpl	%r11d, %ecx
	jbe	.L275
	leaq	18(%rbx), %rcx
	testb	$2, %al
	jne	.L264
	movq	32(%rbx), %rcx
.L264:
	movslq	%r15d, %r15
	movzwl	(%rcx,%r15,2), %ecx
	movl	-52(%rbp), %r15d
	cmpl	$255, %ecx
	jg	.L285
.L265:
	testw	%ax, %ax
	js	.L266
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L267:
	xorl	%r11d, %r11d
	cmpl	%ecx, %r10d
	je	.L268
	movl	$65279, %r11d
	cmpl	%r10d, %ecx
	jbe	.L269
	testb	$2, %al
	je	.L270
	addq	$18, %rbx
.L271:
	movslq	%r10d, %rax
	movzwl	(%rbx,%rax,2), %r11d
	subl	$256, %r11d
.L269:
	subq	$8, %rsp
	pushq	16(%rbp)
	leal	1(%r10), %ecx
	leal	(%r15,%r12), %esi
	leal	(%rcx,%r11), %r8d
	movl	%r11d, -64(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode@PLT
	movl	-64(%rbp), %r11d
	popq	%rax
	popq	%rdx
	addl	%r11d, %r15d
.L268:
	movl	-52(%rbp), %eax
	movl	%eax, (%r14)
	movl	%r11d, 0(%r13)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L249:
	movl	20(%rbx), %ecx
	testb	$17, %al
	je	.L286
.L272:
	xorl	%edx, %edx
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L273:
	movl	$65279, -52(%rbp)
	movl	$65283, %r10d
	movl	$65282, %r11d
	movl	$65282, %r15d
	movl	$65281, %r8d
.L256:
	subq	$8, %rsp
	pushq	16(%rbp)
	movl	$2, %ecx
	movl	%r12d, %esi
	movl	%r11d, -80(%rbp)
	movl	%r10d, -76(%rbp)
	movb	%r9b, -56(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode@PLT
	popq	%rdi
	movzwl	16(%rbx), %eax
	movq	-64(%rbp), %rdi
	movq	-72(%rbp), %rdx
	movzbl	-56(%rbp), %r9d
	movl	-76(%rbp), %r10d
	movl	-80(%rbp), %r11d
	popq	%r8
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L266:
	movl	20(%rbx), %ecx
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L260:
	movl	20(%rbx), %ecx
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L270:
	movq	32(%rbx), %rbx
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L275:
	movl	$65279, %r15d
.L262:
	subq	$8, %rsp
	movl	-52(%rbp), %eax
	pushq	16(%rbp)
	leal	(%r10,%r15), %r8d
	movl	%r10d, %ecx
	movb	%r9b, -76(%rbp)
	leal	(%rax,%r12), %esi
	movl	%r8d, -56(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode@PLT
	movl	-56(%rbp), %r8d
	popq	%rcx
	movzwl	16(%rbx), %eax
	movzbl	-76(%rbp), %r9d
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	leal	1(%r8), %r10d
	addl	-52(%rbp), %r15d
	popq	%rsi
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L284:
	leal	-256(%r10), %eax
	leal	-253(%r10), %r15d
	leal	-254(%r10), %r8d
	movl	%eax, -52(%rbp)
	movl	%r15d, %r11d
	subl	$252, %r10d
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L285:
	leal	-256(%rcx), %r15d
	jmp	.L262
	.cfi_endproc
.LFE3689:
	.size	_ZN6icu_676number4impl14SimpleModifier19formatTwoArgPatternERKNS_15SimpleFormatterERNS_22FormattedStringBuilderEiPiS8_NS6_5FieldER10UErrorCode, .-_ZN6icu_676number4impl14SimpleModifier19formatTwoArgPatternERKNS_15SimpleFormatterERNS_22FormattedStringBuilderEiPiS8_NS6_5FieldER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier13getUnicodeSetERKNS_20DecimalFormatSymbolsENS2_9EPositionENS2_6EAffixER10UErrorCode
	.type	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier13getUnicodeSetERKNS_20DecimalFormatSymbolsENS2_9EPositionENS2_6EAffixER10UErrorCode, @function
_ZN6icu_676number4impl30CurrencySpacingEnabledModifier13getUnicodeSetERKNS_20DecimalFormatSymbolsENS2_9EPositionENS2_6EAffixER10UErrorCode:
.LFB3707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %edi
	testl	%edi, %edi
	jle	.L305
.L288:
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
.L287:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movl	_ZN12_GLOBAL__N_131gDefaultCurrencySpacingInitOnceE(%rip), %eax
	movq	%rsi, %r15
	movl	%edx, %r14d
	movl	%ecx, %ebx
	movq	%r8, %r12
	cmpl	$2, %eax
	jne	.L306
.L289:
	movl	4+_ZN12_GLOBAL__N_131gDefaultCurrencySpacingInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L290
	movl	%eax, (%r12)
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L306:
	leaq	_ZN12_GLOBAL__N_131gDefaultCurrencySpacingInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L289
	movq	%r12, %rdi
	call	_ZN12_GLOBAL__N_126initDefaultCurrencySpacingER10UErrorCode
	movl	(%r12), %eax
	leaq	_ZN12_GLOBAL__N_131gDefaultCurrencySpacingInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN12_GLOBAL__N_131gDefaultCurrencySpacingInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L290:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L288
	xorl	%edx, %edx
	cmpl	$1, %ebx
	movq	%r12, %rcx
	movq	%r15, %rdi
	sete	%dl
	xorl	%esi, %esi
	testl	%r14d, %r14d
	setne	%sil
	call	_ZNK6icu_6720DecimalFormatSymbols28getPatternForCurrencySpacingE16UCurrencySpacingaR10UErrorCode@PLT
	movswl	8(%rax), %edx
	movq	%rax, %r14
	testw	%dx, %dx
	js	.L291
	sarl	$5, %edx
.L292:
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	.LC0(%rip), %rdx
	testb	%al, %al
	jne	.L293
	movq	_ZN12_GLOBAL__N_112UNISET_DIGITE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKS0_@PLT
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L293:
	movswl	8(%r14), %edx
	testw	%dx, %dx
	js	.L295
	sarl	$5, %edx
.L296:
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	.LC1(%rip), %rdx
	testb	%al, %al
	jne	.L297
	movq	_ZN12_GLOBAL__N_111UNISET_NOTSE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKS0_@PLT
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L295:
	movl	12(%r14), %edx
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L291:
	movl	12(%rax), %edx
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
	jmp	.L287
	.cfi_endproc
.LFE3707:
	.size	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier13getUnicodeSetERKNS_20DecimalFormatSymbolsENS2_9EPositionENS2_6EAffixER10UErrorCode, .-_ZN6icu_676number4impl30CurrencySpacingEnabledModifier13getUnicodeSetERKNS_20DecimalFormatSymbolsENS2_9EPositionENS2_6EAffixER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier15getInsertStringERKNS_20DecimalFormatSymbolsENS2_6EAffixER10UErrorCode
	.type	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier15getInsertStringERKNS_20DecimalFormatSymbolsENS2_6EAffixER10UErrorCode, @function
_ZN6icu_676number4impl30CurrencySpacingEnabledModifier15getInsertStringERKNS_20DecimalFormatSymbolsENS2_6EAffixER10UErrorCode:
.LFB3708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	$2, %esi
	subq	$8, %rsp
	cmpl	$1, %edx
	sete	%dl
	movzbl	%dl, %edx
	call	_ZNK6icu_6720DecimalFormatSymbols28getPatternForCurrencySpacingE16UCurrencySpacingaR10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3708:
	.size	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier15getInsertStringERKNS_20DecimalFormatSymbolsENS2_6EAffixER10UErrorCode, .-_ZN6icu_676number4impl30CurrencySpacingEnabledModifier15getInsertStringERKNS_20DecimalFormatSymbolsENS2_6EAffixER10UErrorCode
	.section	.text._ZN6icu_676number4impl30CurrencySpacingEnabledModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD2Ev
	.type	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD2Ev, @function
_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD2Ev:
.LFB4864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl30CurrencySpacingEnabledModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	768(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -768(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	568(%rbx), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	504(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	304(%rbx), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl26ConstantMultiFieldModifierE(%rip), %rax
	leaq	144(%rbx), %rdi
	movq	%rax, (%rbx)
	call	_ZN6icu_6722FormattedStringBuilderD1Ev@PLT
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6722FormattedStringBuilderD1Ev@PLT
	.cfi_endproc
.LFE4864:
	.size	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD2Ev, .-_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD2Ev
	.weak	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD1Ev
	.set	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD1Ev,_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD2Ev
	.section	.text._ZN6icu_676number4impl30CurrencySpacingEnabledModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD0Ev
	.type	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD0Ev, @function
_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD0Ev:
.LFB4866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl30CurrencySpacingEnabledModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	768(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -768(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	568(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	504(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	304(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl26ConstantMultiFieldModifierE(%rip), %rax
	leaq	144(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6722FormattedStringBuilderD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6722FormattedStringBuilderD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4866:
	.size	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD0Ev, .-_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierC2ERKNS_22FormattedStringBuilderES5_bbRKNS_20DecimalFormatSymbolsER10UErrorCode
	.type	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierC2ERKNS_22FormattedStringBuilderES5_bbRKNS_20DecimalFormatSymbolsER10UErrorCode, @function
_ZN6icu_676number4impl30CurrencySpacingEnabledModifierC2ERKNS_22FormattedStringBuilderES5_bbRKNS_20DecimalFormatSymbolsER10UErrorCode:
.LFB3702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$472, %rsp
	movq	16(%rbp), %rax
	movq	%r9, -488(%rbp)
	movq	%rax, -496(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_676number4impl26ConstantMultiFieldModifierE(%rip), %rax
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6722FormattedStringBuilderC1ERKS0_@PLT
	movq	%r12, %rsi
	leaq	144(%rbx), %rdi
	call	_ZN6icu_6722FormattedStringBuilderC1ERKS0_@PLT
	leaq	16+_ZTVN6icu_676number4impl30CurrencySpacingEnabledModifierE(%rip), %rax
	movb	%r14b, 280(%rbx)
	leaq	304(%rbx), %r9
	movq	%rax, (%rbx)
	movq	%r9, %rdi
	leaq	768(%rbx), %r14
	movb	%r13b, 281(%rbx)
	leaq	568(%rbx), %r13
	movq	$0, 288(%rbx)
	movq	%r9, -472(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	504(%rbx), %rax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%r13, %rdi
	movq	%rax, -480(%rbp)
	movl	$2, %eax
	movq	%rdx, 504(%rbx)
	movw	%ax, 512(%rbx)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%r15, %rdi
	movq	%rdx, 768(%rbx)
	movl	$2, %edx
	movw	%dx, 776(%rbx)
	call	_ZNK6icu_6722FormattedStringBuilder6lengthEv@PLT
	movq	-472(%rbp), %r9
	testl	%eax, %eax
	jle	.L314
	movq	%r15, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder6lengthEv@PLT
	movq	-472(%rbp), %r9
	leaq	88(%r15), %rdx
	subl	$1, %eax
	cmpb	$0, (%r15)
	je	.L316
	movq	88(%r15), %rdx
.L316:
	addl	128(%r15), %eax
	cltq
	cmpb	$39, (%rdx,%rax)
	je	.L331
.L314:
	movq	%r9, %rdi
	call	_ZN6icu_6710UnicodeSet10setToBogusEv@PLT
	movq	-480(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L319:
	movq	%r12, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder6lengthEv@PLT
	testl	%eax, %eax
	jle	.L321
	cmpb	$0, (%r12)
	leaq	88(%r12), %rax
	jne	.L332
	movslq	128(%r12), %rdx
	cmpb	$39, (%rax,%rdx)
	je	.L324
.L321:
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet10setToBogusEv@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L313:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L333
	addq	$472, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	movq	88(%r12), %rax
	movslq	128(%r12), %rdx
	cmpb	$39, (%rax,%rdx)
	jne	.L321
.L324:
	movq	%r12, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder17getFirstCodePointEv@PLT
	movq	-496(%rbp), %r15
	movq	-488(%rbp), %rbx
	xorl	%edx, %edx
	leaq	-464(%rbp), %r10
	movl	$1, %ecx
	movl	%eax, %r12d
	movq	%r10, %rdi
	movq	%r15, %r8
	movq	%rbx, %rsi
	movq	%r10, -472(%rbp)
	call	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier13getUnicodeSetERKNS_20DecimalFormatSymbolsENS2_9EPositionENS2_6EAffixER10UErrorCode
	movq	-472(%rbp), %r10
	movl	%r12d, %esi
	movq	%r10, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movq	-472(%rbp), %r10
	testb	%al, %al
	je	.L326
	movq	%r15, %r8
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rbx, %rsi
	leaq	-256(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier13getUnicodeSetERKNS_20DecimalFormatSymbolsENS2_9EPositionENS2_6EAffixER10UErrorCode
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rbx, %rdi
	movl	$2, %esi
	call	_ZNK6icu_6720DecimalFormatSymbols28getPatternForCurrencySpacingE16UCurrencySpacingaR10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-472(%rbp), %r10
.L327:
	movq	%r10, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L331:
	movq	%r15, %rdi
	movq	%r9, -512(%rbp)
	call	_ZNK6icu_6722FormattedStringBuilder16getLastCodePointEv@PLT
	leaq	-464(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-496(%rbp), %r8
	movq	-488(%rbp), %rsi
	movq	%r10, %rdi
	movl	%eax, -504(%rbp)
	movq	%r10, -472(%rbp)
	call	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier13getUnicodeSetERKNS_20DecimalFormatSymbolsENS2_9EPositionENS2_6EAffixER10UErrorCode
	movq	-472(%rbp), %r10
	movl	-504(%rbp), %r11d
	movq	%r10, %rdi
	movl	%r11d, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movq	-472(%rbp), %r10
	movq	-512(%rbp), %r9
	testb	%al, %al
	je	.L334
	movq	-496(%rbp), %r15
	movq	-488(%rbp), %rbx
	xorl	%ecx, %ecx
	movl	$1, %edx
	leaq	-256(%rbp), %r11
	movq	%r10, -512(%rbp)
	movq	%r15, %r8
	movq	%r11, %rdi
	movq	%rbx, %rsi
	movq	%r9, -504(%rbp)
	movq	%r11, -472(%rbp)
	call	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier13getUnicodeSetERKNS_20DecimalFormatSymbolsENS2_9EPositionENS2_6EAffixER10UErrorCode
	movq	-504(%rbp), %r9
	movq	-472(%rbp), %r11
	movq	%r9, %rdi
	movq	%r11, %rsi
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movq	-472(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-504(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r15, %rcx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6720DecimalFormatSymbols28getPatternForCurrencySpacingE16UCurrencySpacingaR10UErrorCode@PLT
	movq	-472(%rbp), %r11
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-472(%rbp), %r11
	movq	-480(%rbp), %rdi
	movq	%r11, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-472(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-512(%rbp), %r10
.L320:
	movq	%r10, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L326:
	movq	%r13, %rdi
	movq	%r10, -472(%rbp)
	call	_ZN6icu_6710UnicodeSet10setToBogusEv@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movq	-472(%rbp), %r10
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L334:
	movq	%r9, %rdi
	movq	%r10, -472(%rbp)
	call	_ZN6icu_6710UnicodeSet10setToBogusEv@PLT
	movq	-480(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movq	-472(%rbp), %r10
	jmp	.L320
.L333:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3702:
	.size	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierC2ERKNS_22FormattedStringBuilderES5_bbRKNS_20DecimalFormatSymbolsER10UErrorCode, .-_ZN6icu_676number4impl30CurrencySpacingEnabledModifierC2ERKNS_22FormattedStringBuilderES5_bbRKNS_20DecimalFormatSymbolsER10UErrorCode
	.globl	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierC1ERKNS_22FormattedStringBuilderES5_bbRKNS_20DecimalFormatSymbolsER10UErrorCode
	.set	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierC1ERKNS_22FormattedStringBuilderES5_bbRKNS_20DecimalFormatSymbolsER10UErrorCode,_ZN6icu_676number4impl30CurrencySpacingEnabledModifierC2ERKNS_22FormattedStringBuilderES5_bbRKNS_20DecimalFormatSymbolsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl30CurrencySpacingEnabledModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.type	_ZNK6icu_676number4impl30CurrencySpacingEnabledModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, @function
_ZNK6icu_676number4impl30CurrencySpacingEnabledModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode:
.LFB3704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%ecx, %eax
	subl	%edx, %eax
	testl	%eax, %eax
	jle	.L342
	testb	$1, 336(%rdi)
	je	.L351
.L337:
	movl	%ebx, -132(%rbp)
	movl	$0, -136(%rbp)
.L338:
	testb	$1, 600(%r12)
	je	.L352
.L336:
	leaq	8(%r12), %rdx
	movq	%r15, %rcx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKS0_R10UErrorCode@PLT
	cmpb	$0, 280(%r12)
	movl	%eax, %ebx
	jne	.L353
.L339:
	movl	-132(%rbp), %esi
	movq	%r15, %rcx
	leaq	144(%r12), %rdx
	movq	%r14, %rdi
	addl	%ebx, %esi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKS0_R10UErrorCode@PLT
	addl	%eax, %ebx
	movl	-136(%rbp), %eax
	addl	%ebx, %eax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L354
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	movl	%ecx, -132(%rbp)
	movl	$0, -136(%rbp)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L353:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	pushq	%r15
	leaq	-128(%rbp), %r10
	xorl	%r9d, %r9d
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movq	%r10, %rcx
	xorl	%r8d, %r8d
	movw	%ax, -120(%rbp)
	movl	-132(%rbp), %eax
	leal	0(%r13,%rbx), %esi
	movq	%r14, %rdi
	movq	%r10, -144(%rbp)
	leal	(%rbx,%rax), %edx
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %eax
	pushq	%rax
	call	_ZN6icu_6722FormattedStringBuilder6spliceEiiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode@PLT
	movq	-144(%rbp), %r10
	addl	%eax, %ebx
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	popq	%rdx
	popq	%rcx
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L352:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder15codePointBeforeEi@PLT
	leaq	568(%r12), %rdi
	movl	%eax, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L336
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %ecx
	movl	-132(%rbp), %esi
	movq	%r15, %r8
	movq	%r14, %rdi
	leaq	768(%r12), %rdx
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	addl	%eax, -136(%rbp)
	movl	-136(%rbp), %eax
	addl	%eax, %ebx
	movl	%ebx, -132(%rbp)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L351:
	movl	%edx, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder11codePointAtEi@PLT
	leaq	304(%r12), %rdi
	movl	%eax, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L337
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %ecx
	movq	%r15, %r8
	movl	%r13d, %esi
	movq	%r14, %rdi
	leaq	504(%r12), %rdx
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movl	%eax, -136(%rbp)
	addl	%ebx, %eax
	movl	%eax, -132(%rbp)
	jmp	.L338
.L354:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3704:
	.size	_ZNK6icu_676number4impl30CurrencySpacingEnabledModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, .-_ZNK6icu_676number4impl30CurrencySpacingEnabledModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier25applyCurrencySpacingAffixERNS_22FormattedStringBuilderEiNS2_6EAffixERKNS_20DecimalFormatSymbolsER10UErrorCode.part.0, @function
_ZN6icu_676number4impl30CurrencySpacingEnabledModifier25applyCurrencySpacingAffixERNS_22FormattedStringBuilderEiNS2_6EAffixERKNS_20DecimalFormatSymbolsER10UErrorCode.part.0:
.LFB4898:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$520, %rsp
	movq	%rcx, -544(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L356
	call	_ZNK6icu_6722FormattedStringBuilder15codePointBeforeEi@PLT
	movl	(%rbx), %edx
	movl	%eax, -536(%rbp)
	testl	%edx, %edx
	jle	.L385
.L358:
	leaq	-464(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
.L364:
	movl	-536(%rbp), %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	$0, -536(%rbp)
	testb	%al, %al
	je	.L368
	movl	%r14d, %esi
	movq	%r13, %rdi
	testl	%r12d, %r12d
	je	.L386
	call	_ZNK6icu_6722FormattedStringBuilder15codePointBeforeEi@PLT
	movl	%eax, %r10d
.L370:
	leaq	-256(%rbp), %r9
	movq	-544(%rbp), %rsi
	movq	%rbx, %r8
	movl	%r12d, %ecx
	movq	%r9, %rdi
	movl	$1, %edx
	movl	%r10d, -552(%rbp)
	movq	%r9, -536(%rbp)
	call	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier13getUnicodeSetERKNS_20DecimalFormatSymbolsENS2_9EPositionENS2_6EAffixER10UErrorCode
	movq	-536(%rbp), %r9
	movl	-552(%rbp), %r10d
	movq	%r9, %rdi
	movl	%r10d, %esi
	movq	%r9, -552(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movq	-552(%rbp), %r9
	movl	$0, -536(%rbp)
	testb	%al, %al
	je	.L371
	movq	-544(%rbp), %rdi
	xorl	%edx, %edx
	cmpl	$1, %r12d
	movq	%rbx, %rcx
	sete	%dl
	movl	$2, %esi
	leaq	-528(%rbp), %r12
	call	_ZNK6icu_6720DecimalFormatSymbols28getPatternForCurrencySpacingE16UCurrencySpacingaR10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%rbx, %r8
	movq	%r12, %rdx
	movl	%r14d, %esi
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %ecx
	movq	%r13, %rdi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	%r12, %rdi
	movl	%eax, -536(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-552(%rbp), %r9
.L371:
	movq	%r9, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
.L368:
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	movl	-536(%rbp), %eax
	addq	$520, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	call	_ZNK6icu_6722FormattedStringBuilder11codePointAtEi@PLT
	movl	(%rbx), %edx
	movl	%eax, -536(%rbp)
	testl	%edx, %edx
	jg	.L358
.L385:
	movl	_ZN12_GLOBAL__N_131gDefaultCurrencySpacingInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L388
.L359:
	movl	4+_ZN12_GLOBAL__N_131gDefaultCurrencySpacingInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L360
	movl	%eax, (%rbx)
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L386:
	call	_ZNK6icu_6722FormattedStringBuilder11codePointAtEi@PLT
	movl	%eax, %r10d
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L388:
	leaq	_ZN12_GLOBAL__N_131gDefaultCurrencySpacingInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L359
	movq	%rbx, %rdi
	call	_ZN12_GLOBAL__N_126initDefaultCurrencySpacingER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN12_GLOBAL__N_131gDefaultCurrencySpacingInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN12_GLOBAL__N_131gDefaultCurrencySpacingInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L360:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L358
	xorl	%edx, %edx
	movq	-544(%rbp), %rdi
	cmpl	$1, %r12d
	movq	%rbx, %rcx
	sete	%dl
	xorl	%esi, %esi
	call	_ZNK6icu_6720DecimalFormatSymbols28getPatternForCurrencySpacingE16UCurrencySpacingaR10UErrorCode@PLT
	movswl	8(%rax), %edx
	movq	%rax, %r10
	testw	%dx, %dx
	js	.L361
	sarl	$5, %edx
.L362:
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r10, %rdi
	leaq	.LC0(%rip), %rcx
	movq	%r10, -552(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	.LC0(%rip), %rdx
	testb	%al, %al
	movq	-552(%rbp), %r10
	jne	.L363
	leaq	-464(%rbp), %r15
	movq	_ZN12_GLOBAL__N_112UNISET_DIGITE(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKS0_@PLT
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L363:
	movswl	8(%r10), %edx
	testw	%dx, %dx
	js	.L365
	sarl	$5, %edx
.L366:
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r10, %rdi
	leaq	.LC1(%rip), %rcx
	movq	%r10, -552(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	.LC1(%rip), %rdx
	testb	%al, %al
	movq	-552(%rbp), %r10
	leaq	-464(%rbp), %r15
	jne	.L367
	movq	_ZN12_GLOBAL__N_111UNISET_NOTSE(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKS0_@PLT
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L365:
	movl	12(%r10), %edx
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L361:
	movl	12(%rax), %edx
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L367:
	movq	%rbx, %rdx
	movq	%r10, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
	jmp	.L364
.L387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4898:
	.size	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier25applyCurrencySpacingAffixERNS_22FormattedStringBuilderEiNS2_6EAffixERKNS_20DecimalFormatSymbolsER10UErrorCode.part.0, .-_ZN6icu_676number4impl30CurrencySpacingEnabledModifier25applyCurrencySpacingAffixERNS_22FormattedStringBuilderEiNS2_6EAffixERKNS_20DecimalFormatSymbolsER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier25applyCurrencySpacingAffixERNS_22FormattedStringBuilderEiNS2_6EAffixERKNS_20DecimalFormatSymbolsER10UErrorCode
	.type	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier25applyCurrencySpacingAffixERNS_22FormattedStringBuilderEiNS2_6EAffixERKNS_20DecimalFormatSymbolsER10UErrorCode, @function
_ZN6icu_676number4impl30CurrencySpacingEnabledModifier25applyCurrencySpacingAffixERNS_22FormattedStringBuilderEiNS2_6EAffixERKNS_20DecimalFormatSymbolsER10UErrorCode:
.LFB3706:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %r9d
	testl	%edx, %edx
	jne	.L390
	leal	-1(%rsi), %eax
	leaq	88(%rdi), %r10
	testb	%r9b, %r9b
	jne	.L400
.L399:
	addl	128(%rdi), %eax
	cltq
	movzbl	(%r10,%rax), %eax
	cmpb	$39, %al
	jne	.L396
	jmp	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier25applyCurrencySpacingAffixERNS_22FormattedStringBuilderEiNS2_6EAffixERKNS_20DecimalFormatSymbolsER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L390:
	leaq	88(%rdi), %r10
	testb	%r9b, %r9b
	je	.L395
	movq	88(%rdi), %r10
.L395:
	movl	%esi, %eax
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L396:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	movq	88(%rdi), %r10
	jmp	.L399
	.cfi_endproc
.LFE3706:
	.size	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier25applyCurrencySpacingAffixERNS_22FormattedStringBuilderEiNS2_6EAffixERKNS_20DecimalFormatSymbolsER10UErrorCode, .-_ZN6icu_676number4impl30CurrencySpacingEnabledModifier25applyCurrencySpacingAffixERNS_22FormattedStringBuilderEiNS2_6EAffixERKNS_20DecimalFormatSymbolsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier20applyCurrencySpacingERNS_22FormattedStringBuilderEiiiiRKNS_20DecimalFormatSymbolsER10UErrorCode
	.type	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier20applyCurrencySpacingERNS_22FormattedStringBuilderEiiiiRKNS_20DecimalFormatSymbolsER10UErrorCode, @function
_ZN6icu_676number4impl30CurrencySpacingEnabledModifier20applyCurrencySpacingERNS_22FormattedStringBuilderEiiiiRKNS_20DecimalFormatSymbolsER10UErrorCode:
.LFB3705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movl	%ecx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 3, -48
	movl	%ecx, %ebx
	subq	$16, %rsp
	testl	%r8d, %r8d
	setg	%r13b
	subl	%eax, %esi
	subl	%edx, %esi
	testl	%esi, %esi
	setg	%r15b
	testl	%edx, %edx
	jle	.L409
	testb	%r15b, %r15b
	jne	.L424
.L409:
	xorl	%r9d, %r9d
.L402:
	testb	%r13b, %r13b
	je	.L401
	testb	%r15b, %r15b
	jne	.L425
.L401:
	addq	$16, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	cmpb	$0, (%rdi)
	leal	(%r9,%rbx), %esi
	leaq	88(%rdi), %rdx
	je	.L407
	movq	88(%rdi), %rdx
.L407:
	movl	128(%rdi), %eax
	addl	%esi, %eax
	cltq
	cmpb	$39, (%rdx,%rax)
	jne	.L401
	movq	16(%rbp), %r8
	movq	%r14, %rcx
	movl	$1, %edx
	movl	%r9d, -40(%rbp)
	call	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier25applyCurrencySpacingAffixERNS_22FormattedStringBuilderEiNS2_6EAffixERKNS_20DecimalFormatSymbolsER10UErrorCode.part.0
	movl	-40(%rbp), %r9d
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	addl	%eax, %r9d
	popq	%r14
	popq	%r15
	movl	%r9d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	leal	(%rdx,%rax), %esi
	cmpb	$0, (%rdi)
	leaq	88(%rdi), %rdx
	leal	-1(%rsi), %eax
	je	.L404
	movq	88(%rdi), %rdx
.L404:
	addl	128(%rdi), %eax
	xorl	%r9d, %r9d
	cltq
	cmpb	$39, (%rdx,%rax)
	jne	.L402
	movq	16(%rbp), %r8
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%rdi, -40(%rbp)
	call	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier25applyCurrencySpacingAffixERNS_22FormattedStringBuilderEiNS2_6EAffixERKNS_20DecimalFormatSymbolsER10UErrorCode.part.0
	movq	-40(%rbp), %rdi
	movl	%eax, %r9d
	jmp	.L402
	.cfi_endproc
.LFE3705:
	.size	_ZN6icu_676number4impl30CurrencySpacingEnabledModifier20applyCurrencySpacingERNS_22FormattedStringBuilderEiiiiRKNS_20DecimalFormatSymbolsER10UErrorCode, .-_ZN6icu_676number4impl30CurrencySpacingEnabledModifier20applyCurrencySpacingERNS_22FormattedStringBuilderEiiiiRKNS_20DecimalFormatSymbolsER10UErrorCode
	.p2align 4
	.type	_ZN12_GLOBAL__N_129cleanupDefaultCurrencySpacingEv, @function
_ZN12_GLOBAL__N_129cleanupDefaultCurrencySpacingEv:
.LFB3645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZN12_GLOBAL__N_112UNISET_DIGITE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L427
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L427:
	movq	_ZN12_GLOBAL__N_111UNISET_NOTSE(%rip), %rdi
	movq	$0, _ZN12_GLOBAL__N_112UNISET_DIGITE(%rip)
	testq	%rdi, %rdi
	je	.L428
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L428:
	movq	$0, _ZN12_GLOBAL__N_111UNISET_NOTSE(%rip)
	movl	$1, %eax
	movl	$0, _ZN12_GLOBAL__N_131gDefaultCurrencySpacingInitOnceE(%rip)
	mfence
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3645:
	.size	_ZN12_GLOBAL__N_129cleanupDefaultCurrencySpacingEv, .-_ZN12_GLOBAL__N_129cleanupDefaultCurrencySpacingEv
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_676number4impl8ModifierE
	.section	.rodata._ZTSN6icu_676number4impl8ModifierE,"aG",@progbits,_ZTSN6icu_676number4impl8ModifierE,comdat
	.align 16
	.type	_ZTSN6icu_676number4impl8ModifierE, @object
	.size	_ZTSN6icu_676number4impl8ModifierE, 31
_ZTSN6icu_676number4impl8ModifierE:
	.string	"N6icu_676number4impl8ModifierE"
	.weak	_ZTIN6icu_676number4impl8ModifierE
	.section	.data.rel.ro._ZTIN6icu_676number4impl8ModifierE,"awG",@progbits,_ZTIN6icu_676number4impl8ModifierE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl8ModifierE, @object
	.size	_ZTIN6icu_676number4impl8ModifierE, 16
_ZTIN6icu_676number4impl8ModifierE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl8ModifierE
	.weak	_ZTSN6icu_676number4impl13ModifierStoreE
	.section	.rodata._ZTSN6icu_676number4impl13ModifierStoreE,"aG",@progbits,_ZTSN6icu_676number4impl13ModifierStoreE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl13ModifierStoreE, @object
	.size	_ZTSN6icu_676number4impl13ModifierStoreE, 37
_ZTSN6icu_676number4impl13ModifierStoreE:
	.string	"N6icu_676number4impl13ModifierStoreE"
	.weak	_ZTIN6icu_676number4impl13ModifierStoreE
	.section	.data.rel.ro._ZTIN6icu_676number4impl13ModifierStoreE,"awG",@progbits,_ZTIN6icu_676number4impl13ModifierStoreE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl13ModifierStoreE, @object
	.size	_ZTIN6icu_676number4impl13ModifierStoreE, 16
_ZTIN6icu_676number4impl13ModifierStoreE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl13ModifierStoreE
	.weak	_ZTSN6icu_676number4impl21ConstantAffixModifierE
	.section	.rodata._ZTSN6icu_676number4impl21ConstantAffixModifierE,"aG",@progbits,_ZTSN6icu_676number4impl21ConstantAffixModifierE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl21ConstantAffixModifierE, @object
	.size	_ZTSN6icu_676number4impl21ConstantAffixModifierE, 45
_ZTSN6icu_676number4impl21ConstantAffixModifierE:
	.string	"N6icu_676number4impl21ConstantAffixModifierE"
	.weak	_ZTIN6icu_676number4impl21ConstantAffixModifierE
	.section	.data.rel.ro._ZTIN6icu_676number4impl21ConstantAffixModifierE,"awG",@progbits,_ZTIN6icu_676number4impl21ConstantAffixModifierE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl21ConstantAffixModifierE, @object
	.size	_ZTIN6icu_676number4impl21ConstantAffixModifierE, 56
_ZTIN6icu_676number4impl21ConstantAffixModifierE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl21ConstantAffixModifierE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl8ModifierE
	.quad	2
	.quad	_ZTIN6icu_677UObjectE
	.quad	2050
	.weak	_ZTSN6icu_676number4impl14SimpleModifierE
	.section	.rodata._ZTSN6icu_676number4impl14SimpleModifierE,"aG",@progbits,_ZTSN6icu_676number4impl14SimpleModifierE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl14SimpleModifierE, @object
	.size	_ZTSN6icu_676number4impl14SimpleModifierE, 38
_ZTSN6icu_676number4impl14SimpleModifierE:
	.string	"N6icu_676number4impl14SimpleModifierE"
	.weak	_ZTIN6icu_676number4impl14SimpleModifierE
	.section	.data.rel.ro._ZTIN6icu_676number4impl14SimpleModifierE,"awG",@progbits,_ZTIN6icu_676number4impl14SimpleModifierE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl14SimpleModifierE, @object
	.size	_ZTIN6icu_676number4impl14SimpleModifierE, 56
_ZTIN6icu_676number4impl14SimpleModifierE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl14SimpleModifierE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl8ModifierE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTSN6icu_676number4impl26ConstantMultiFieldModifierE
	.section	.rodata._ZTSN6icu_676number4impl26ConstantMultiFieldModifierE,"aG",@progbits,_ZTSN6icu_676number4impl26ConstantMultiFieldModifierE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl26ConstantMultiFieldModifierE, @object
	.size	_ZTSN6icu_676number4impl26ConstantMultiFieldModifierE, 50
_ZTSN6icu_676number4impl26ConstantMultiFieldModifierE:
	.string	"N6icu_676number4impl26ConstantMultiFieldModifierE"
	.weak	_ZTIN6icu_676number4impl26ConstantMultiFieldModifierE
	.section	.data.rel.ro._ZTIN6icu_676number4impl26ConstantMultiFieldModifierE,"awG",@progbits,_ZTIN6icu_676number4impl26ConstantMultiFieldModifierE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl26ConstantMultiFieldModifierE, @object
	.size	_ZTIN6icu_676number4impl26ConstantMultiFieldModifierE, 56
_ZTIN6icu_676number4impl26ConstantMultiFieldModifierE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl26ConstantMultiFieldModifierE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl8ModifierE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTSN6icu_676number4impl30CurrencySpacingEnabledModifierE
	.section	.rodata._ZTSN6icu_676number4impl30CurrencySpacingEnabledModifierE,"aG",@progbits,_ZTSN6icu_676number4impl30CurrencySpacingEnabledModifierE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl30CurrencySpacingEnabledModifierE, @object
	.size	_ZTSN6icu_676number4impl30CurrencySpacingEnabledModifierE, 54
_ZTSN6icu_676number4impl30CurrencySpacingEnabledModifierE:
	.string	"N6icu_676number4impl30CurrencySpacingEnabledModifierE"
	.weak	_ZTIN6icu_676number4impl30CurrencySpacingEnabledModifierE
	.section	.data.rel.ro._ZTIN6icu_676number4impl30CurrencySpacingEnabledModifierE,"awG",@progbits,_ZTIN6icu_676number4impl30CurrencySpacingEnabledModifierE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl30CurrencySpacingEnabledModifierE, @object
	.size	_ZTIN6icu_676number4impl30CurrencySpacingEnabledModifierE, 24
_ZTIN6icu_676number4impl30CurrencySpacingEnabledModifierE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl30CurrencySpacingEnabledModifierE
	.quad	_ZTIN6icu_676number4impl26ConstantMultiFieldModifierE
	.weak	_ZTSN6icu_676number4impl21AdoptingModifierStoreE
	.section	.rodata._ZTSN6icu_676number4impl21AdoptingModifierStoreE,"aG",@progbits,_ZTSN6icu_676number4impl21AdoptingModifierStoreE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl21AdoptingModifierStoreE, @object
	.size	_ZTSN6icu_676number4impl21AdoptingModifierStoreE, 45
_ZTSN6icu_676number4impl21AdoptingModifierStoreE:
	.string	"N6icu_676number4impl21AdoptingModifierStoreE"
	.weak	_ZTIN6icu_676number4impl21AdoptingModifierStoreE
	.section	.data.rel.ro._ZTIN6icu_676number4impl21AdoptingModifierStoreE,"awG",@progbits,_ZTIN6icu_676number4impl21AdoptingModifierStoreE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl21AdoptingModifierStoreE, @object
	.size	_ZTIN6icu_676number4impl21AdoptingModifierStoreE, 56
_ZTIN6icu_676number4impl21AdoptingModifierStoreE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl21AdoptingModifierStoreE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl13ModifierStoreE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTVN6icu_676number4impl8ModifierE
	.section	.data.rel.ro._ZTVN6icu_676number4impl8ModifierE,"awG",@progbits,_ZTVN6icu_676number4impl8ModifierE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl8ModifierE, @object
	.size	_ZTVN6icu_676number4impl8ModifierE, 88
_ZTVN6icu_676number4impl8ModifierE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl8ModifierE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_676number4impl13ModifierStoreE
	.section	.data.rel.ro._ZTVN6icu_676number4impl13ModifierStoreE,"awG",@progbits,_ZTVN6icu_676number4impl13ModifierStoreE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl13ModifierStoreE, @object
	.size	_ZTVN6icu_676number4impl13ModifierStoreE, 40
_ZTVN6icu_676number4impl13ModifierStoreE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl13ModifierStoreE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_676number4impl21AdoptingModifierStoreE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl21AdoptingModifierStoreE,"awG",@progbits,_ZTVN6icu_676number4impl21AdoptingModifierStoreE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl21AdoptingModifierStoreE, @object
	.size	_ZTVN6icu_676number4impl21AdoptingModifierStoreE, 40
_ZTVN6icu_676number4impl21AdoptingModifierStoreE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl21AdoptingModifierStoreE
	.quad	_ZN6icu_676number4impl21AdoptingModifierStoreD1Ev
	.quad	_ZN6icu_676number4impl21AdoptingModifierStoreD0Ev
	.quad	_ZNK6icu_676number4impl21AdoptingModifierStore11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.weak	_ZTVN6icu_676number4impl21ConstantAffixModifierE
	.section	.data.rel.ro._ZTVN6icu_676number4impl21ConstantAffixModifierE,"awG",@progbits,_ZTVN6icu_676number4impl21ConstantAffixModifierE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl21ConstantAffixModifierE, @object
	.size	_ZTVN6icu_676number4impl21ConstantAffixModifierE, 128
_ZTVN6icu_676number4impl21ConstantAffixModifierE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl21ConstantAffixModifierE
	.quad	_ZN6icu_676number4impl21ConstantAffixModifierD1Ev
	.quad	_ZN6icu_676number4impl21ConstantAffixModifierD0Ev
	.quad	_ZNK6icu_676number4impl21ConstantAffixModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.quad	_ZNK6icu_676number4impl21ConstantAffixModifier15getPrefixLengthEv
	.quad	_ZNK6icu_676number4impl21ConstantAffixModifier17getCodePointCountEv
	.quad	_ZNK6icu_676number4impl21ConstantAffixModifier8isStrongEv
	.quad	_ZNK6icu_676number4impl21ConstantAffixModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.quad	_ZNK6icu_676number4impl21ConstantAffixModifier13getParametersERNS1_8Modifier10ParametersE
	.quad	_ZNK6icu_676number4impl21ConstantAffixModifier22semanticallyEquivalentERKNS1_8ModifierE
	.quad	-8
	.quad	_ZTIN6icu_676number4impl21ConstantAffixModifierE
	.quad	_ZThn8_N6icu_676number4impl21ConstantAffixModifierD1Ev
	.quad	_ZThn8_N6icu_676number4impl21ConstantAffixModifierD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.weak	_ZTVN6icu_676number4impl14SimpleModifierE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl14SimpleModifierE,"awG",@progbits,_ZTVN6icu_676number4impl14SimpleModifierE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl14SimpleModifierE, @object
	.size	_ZTVN6icu_676number4impl14SimpleModifierE, 88
_ZTVN6icu_676number4impl14SimpleModifierE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl14SimpleModifierE
	.quad	_ZN6icu_676number4impl14SimpleModifierD1Ev
	.quad	_ZN6icu_676number4impl14SimpleModifierD0Ev
	.quad	_ZNK6icu_676number4impl14SimpleModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.quad	_ZNK6icu_676number4impl14SimpleModifier15getPrefixLengthEv
	.quad	_ZNK6icu_676number4impl14SimpleModifier17getCodePointCountEv
	.quad	_ZNK6icu_676number4impl14SimpleModifier8isStrongEv
	.quad	_ZNK6icu_676number4impl14SimpleModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.quad	_ZNK6icu_676number4impl14SimpleModifier13getParametersERNS1_8Modifier10ParametersE
	.quad	_ZNK6icu_676number4impl14SimpleModifier22semanticallyEquivalentERKNS1_8ModifierE
	.weak	_ZTVN6icu_676number4impl26ConstantMultiFieldModifierE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl26ConstantMultiFieldModifierE,"awG",@progbits,_ZTVN6icu_676number4impl26ConstantMultiFieldModifierE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl26ConstantMultiFieldModifierE, @object
	.size	_ZTVN6icu_676number4impl26ConstantMultiFieldModifierE, 88
_ZTVN6icu_676number4impl26ConstantMultiFieldModifierE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl26ConstantMultiFieldModifierE
	.quad	_ZN6icu_676number4impl26ConstantMultiFieldModifierD1Ev
	.quad	_ZN6icu_676number4impl26ConstantMultiFieldModifierD0Ev
	.quad	_ZNK6icu_676number4impl26ConstantMultiFieldModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.quad	_ZNK6icu_676number4impl26ConstantMultiFieldModifier15getPrefixLengthEv
	.quad	_ZNK6icu_676number4impl26ConstantMultiFieldModifier17getCodePointCountEv
	.quad	_ZNK6icu_676number4impl26ConstantMultiFieldModifier8isStrongEv
	.quad	_ZNK6icu_676number4impl26ConstantMultiFieldModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.quad	_ZNK6icu_676number4impl26ConstantMultiFieldModifier13getParametersERNS1_8Modifier10ParametersE
	.quad	_ZNK6icu_676number4impl26ConstantMultiFieldModifier22semanticallyEquivalentERKNS1_8ModifierE
	.weak	_ZTVN6icu_676number4impl30CurrencySpacingEnabledModifierE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl30CurrencySpacingEnabledModifierE,"awG",@progbits,_ZTVN6icu_676number4impl30CurrencySpacingEnabledModifierE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl30CurrencySpacingEnabledModifierE, @object
	.size	_ZTVN6icu_676number4impl30CurrencySpacingEnabledModifierE, 88
_ZTVN6icu_676number4impl30CurrencySpacingEnabledModifierE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl30CurrencySpacingEnabledModifierE
	.quad	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD1Ev
	.quad	_ZN6icu_676number4impl30CurrencySpacingEnabledModifierD0Ev
	.quad	_ZNK6icu_676number4impl30CurrencySpacingEnabledModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.quad	_ZNK6icu_676number4impl26ConstantMultiFieldModifier15getPrefixLengthEv
	.quad	_ZNK6icu_676number4impl26ConstantMultiFieldModifier17getCodePointCountEv
	.quad	_ZNK6icu_676number4impl26ConstantMultiFieldModifier8isStrongEv
	.quad	_ZNK6icu_676number4impl26ConstantMultiFieldModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.quad	_ZNK6icu_676number4impl26ConstantMultiFieldModifier13getParametersERNS1_8Modifier10ParametersE
	.quad	_ZNK6icu_676number4impl26ConstantMultiFieldModifier22semanticallyEquivalentERKNS1_8ModifierE
	.local	_ZN12_GLOBAL__N_111UNISET_NOTSE
	.comm	_ZN12_GLOBAL__N_111UNISET_NOTSE,8,8
	.local	_ZN12_GLOBAL__N_112UNISET_DIGITE
	.comm	_ZN12_GLOBAL__N_112UNISET_DIGITE,8,8
	.local	_ZN12_GLOBAL__N_131gDefaultCurrencySpacingInitOnceE
	.comm	_ZN12_GLOBAL__N_131gDefaultCurrencySpacingInitOnceE,8,8
	.section	.rodata
	.type	_ZN6icu_67L15kUndefinedFieldE, @object
	.size	_ZN6icu_67L15kUndefinedFieldE, 1
_ZN6icu_67L15kUndefinedFieldE:
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
