	.file	"csmatch.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CharsetMatchC2Ev
	.type	_ZN6icu_6712CharsetMatchC2Ev, @function
_ZN6icu_6712CharsetMatchC2Ev:
.LFB2237:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, (%rdi)
	movl	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE2237:
	.size	_ZN6icu_6712CharsetMatchC2Ev, .-_ZN6icu_6712CharsetMatchC2Ev
	.globl	_ZN6icu_6712CharsetMatchC1Ev
	.set	_ZN6icu_6712CharsetMatchC1Ev,_ZN6icu_6712CharsetMatchC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_
	.type	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_, @function
_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_:
.LFB2239:
	.cfi_startproc
	endbr64
	movq	%r8, %xmm0
	movq	%r9, %xmm1
	movq	%rsi, (%rdi)
	punpcklqdq	%xmm1, %xmm0
	movl	%ecx, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	testq	%rdx, %rdx
	je	.L9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%r8, %r8
	je	.L12
	testq	%r9, %r9
	je	.L13
.L3:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	(%rdx), %rax
	movq	%rdx, %rdi
	call	*(%rax)
	movq	24(%rbx), %r9
	movq	%rax, 16(%rbx)
	testq	%r9, %r9
	jne	.L3
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE2239:
	.size	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_, .-_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712CharsetMatch7getNameEv
	.type	_ZNK6icu_6712CharsetMatch7getNameEv, @function
_ZNK6icu_6712CharsetMatch7getNameEv:
.LFB2240:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE2240:
	.size	_ZNK6icu_6712CharsetMatch7getNameEv, .-_ZNK6icu_6712CharsetMatch7getNameEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712CharsetMatch11getLanguageEv
	.type	_ZNK6icu_6712CharsetMatch11getLanguageEv, @function
_ZNK6icu_6712CharsetMatch11getLanguageEv:
.LFB2241:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE2241:
	.size	_ZNK6icu_6712CharsetMatch11getLanguageEv, .-_ZNK6icu_6712CharsetMatch11getLanguageEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712CharsetMatch13getConfidenceEv
	.type	_ZNK6icu_6712CharsetMatch13getConfidenceEv, @function
_ZNK6icu_6712CharsetMatch13getConfidenceEv:
.LFB2242:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2242:
	.size	_ZNK6icu_6712CharsetMatch13getConfidenceEv, .-_ZNK6icu_6712CharsetMatch13getConfidenceEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712CharsetMatch9getUCharsEPDsiP10UErrorCode
	.type	_ZNK6icu_6712CharsetMatch9getUCharsEPDsiP10UErrorCode, @function
_ZNK6icu_6712CharsetMatch9getUCharsEPDsiP10UErrorCode:
.LFB2243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rcx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	ucnv_open_67@PLT
	movq	%rbx, %r9
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%rax, %r12
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	40(%rax), %rcx
	movl	48(%rax), %r8d
	call	ucnv_toUChars_67@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	ucnv_close_67@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2243:
	.size	_ZNK6icu_6712CharsetMatch9getUCharsEPDsiP10UErrorCode, .-_ZNK6icu_6712CharsetMatch9getUCharsEPDsiP10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
