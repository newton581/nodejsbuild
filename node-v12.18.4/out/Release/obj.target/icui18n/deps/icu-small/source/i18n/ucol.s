	.file	"ucol.cpp"
	.text
	.p2align 4
	.globl	ucol_openBinary_67
	.type	ucol_openBinary_67, @function
ucol_openBinary_67:
.LFB2437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L1
	movq	%rdi, %r13
	movl	%esi, %r14d
	movq	%rdx, %r12
	movq	%rcx, %rbx
	testq	%rdx, %rdx
	je	.L3
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717RuleBasedCollatorE(%rip), %rdx
	leaq	_ZTIN6icu_678CollatorE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %r12
.L3:
	movl	$272, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L4
	movq	%rax, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6717RuleBasedCollatorC1EPKhiPKS0_R10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L12
.L1:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_6717RuleBasedCollatorD0Ev@PLT
	jmp	.L1
.L4:
	movl	$7, (%rbx)
	jmp	.L1
	.cfi_endproc
.LFE2437:
	.size	ucol_openBinary_67, .-ucol_openBinary_67
	.p2align 4
	.globl	ucol_cloneBinary_67
	.type	ucol_cloneBinary_67, @function
ucol_cloneBinary_67:
.LFB2438:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L21
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L15
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717RuleBasedCollatorE(%rip), %rdx
	leaq	_ZTIN6icu_678CollatorE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L15
	movl	$16, (%r12)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6717RuleBasedCollator11cloneBinaryEPhiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2438:
	.size	ucol_cloneBinary_67, .-ucol_cloneBinary_67
	.p2align 4
	.globl	ucol_safeClone_67
	.type	ucol_safeClone_67, @function
ucol_safeClone_67:
.LFB2439:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L40
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rcx, %rbx
	subq	$8, %rsp
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L39
	testq	%rdi, %rdi
	je	.L41
	testq	%rdx, %rdx
	je	.L32
	movl	(%rdx), %eax
	movl	$1, (%rdx)
	testl	%eax, %eax
	je	.L39
.L32:
	movq	(%rdi), %rax
	call	*40(%rax)
	cmpq	$1, %rax
	sbbl	%edx, %edx
	andl	$133, %edx
	subl	$126, %edx
	movl	%edx, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	$1, (%rcx)
.L39:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2439:
	.size	ucol_safeClone_67, .-ucol_safeClone_67
	.p2align 4
	.globl	ucol_close_67
	.type	ucol_close_67, @function
ucol_close_67:
.LFB2440:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L42
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L42:
	ret
	.cfi_endproc
.LFE2440:
	.size	ucol_close_67, .-ucol_close_67
	.p2align 4
	.globl	ucol_mergeSortkeys_67
	.type	ucol_mergeSortkeys_67, @function
ucol_mergeSortkeys_67:
.LFB2441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	cmpl	$-1, %esi
	setl	%dil
	testl	%esi, %esi
	sete	%al
	orb	%al, %dil
	jne	.L45
	testq	%rbx, %rbx
	je	.L45
	testl	%esi, %esi
	jle	.L46
	movslq	%esi, %rax
	cmpb	$0, -1(%rbx,%rax)
	jne	.L45
.L46:
	cmpl	$-1, %ecx
	setl	%dil
	testl	%ecx, %ecx
	sete	%al
	orb	%al, %dil
	jne	.L45
	testq	%rdx, %rdx
	je	.L45
	testl	%ecx, %ecx
	jle	.L47
	movslq	%ecx, %rax
	cmpb	$0, -1(%rdx,%rax)
	jne	.L45
.L47:
	xorl	%eax, %eax
	testl	%r9d, %r9d
	js	.L44
	jle	.L50
	testq	%r8, %r8
	je	.L44
.L50:
	cmpl	$-1, %esi
	je	.L80
.L51:
	cmpl	$-1, %ecx
	je	.L81
.L52:
	leal	(%rsi,%rcx), %eax
	cmpl	%eax, %r9d
	jl	.L44
	movq	%rbx, %rdi
	movq	%r8, %r9
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L82:
	movb	%al, (%r9)
	movq	%rcx, %r9
.L55:
	addq	$1, %rdi
.L53:
	movzbl	(%rdi), %eax
	leaq	1(%r9), %rcx
	cmpb	$1, %al
	ja	.L82
	movb	$2, (%r9)
	movzbl	(%rdx), %esi
	cmpb	$1, %sil
	jbe	.L56
	.p2align 4,,10
	.p2align 3
.L57:
	addq	$1, %rdx
	movb	%sil, (%rcx)
	addq	$1, %rcx
	movzbl	(%rdx), %esi
	cmpb	$1, %sil
	ja	.L57
.L56:
	movzbl	(%rdi), %r10d
	leaq	1(%rcx), %rax
	movq	%rax, %r9
	cmpb	$1, %r10b
	jne	.L58
	cmpb	$1, %sil
	jne	.L64
	movb	$1, (%rcx)
	addq	$1, %rdx
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L45:
	testq	%r8, %r8
	je	.L63
	testl	%r9d, %r9d
	jle	.L63
	movb	$0, (%r8)
	xorl	%eax, %eax
.L44:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	movq	%rdx, %rdi
	movl	%r9d, -32(%rbp)
	movq	%r8, -40(%rbp)
	movl	%esi, -28(%rbp)
	movq	%rdx, -24(%rbp)
	call	strlen@PLT
	movl	-32(%rbp), %r9d
	movq	-40(%rbp), %r8
	movl	-28(%rbp), %esi
	movq	-24(%rbp), %rdx
	leal	1(%rax), %ecx
	jmp	.L52
.L80:
	movq	%rbx, %rdi
	movl	%r9d, -32(%rbp)
	movq	%r8, -40(%rbp)
	movl	%ecx, -28(%rbp)
	movq	%rdx, -24(%rbp)
	call	strlen@PLT
	movl	-32(%rbp), %r9d
	movq	-40(%rbp), %r8
	movl	-28(%rbp), %ecx
	movq	-24(%rbp), %rdx
	leal	1(%rax), %esi
	jmp	.L51
.L58:
	testb	%r10b, %r10b
	cmovne	%rdi, %rdx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L83:
	addq	$1, %rax
.L60:
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	movb	%cl, -1(%rax)
	testb	%cl, %cl
	jne	.L83
	subl	%r8d, %eax
	jmp	.L44
.L64:
	movq	%rdi, %rdx
	jmp	.L60
	.cfi_endproc
.LFE2441:
	.size	ucol_mergeSortkeys_67, .-ucol_mergeSortkeys_67
	.p2align 4
	.globl	ucol_getSortKey_67
	.type	ucol_getSortKey_67, @function
ucol_getSortKey_67:
.LFB2442:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*272(%rax)
	.cfi_endproc
.LFE2442:
	.size	ucol_getSortKey_67, .-ucol_getSortKey_67
	.p2align 4
	.globl	ucol_nextSortKeyPart_67
	.type	ucol_nextSortKeyPart_67, @function
ucol_nextSortKeyPart_67:
.LFB2443:
	.cfi_startproc
	endbr64
	testq	%r9, %r9
	je	.L85
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L85
	movq	(%rdi), %rax
	jmp	*304(%rax)
	.p2align 4,,10
	.p2align 3
.L85:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2443:
	.size	ucol_nextSortKeyPart_67, .-ucol_nextSortKeyPart_67
	.p2align 4
	.globl	ucol_getBound_67
	.type	ucol_getBound_67, @function
ucol_getBound_67:
.LFB2444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r15
	testq	%r15, %r15
	je	.L90
	movl	(%r15), %r10d
	testl	%r10d, %r10d
	jg	.L90
	movl	%edx, %r13d
	movl	$1, %ebx
	testq	%rdi, %rdi
	jne	.L92
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L93:
	testl	%ecx, %ecx
	je	.L94
	cmpl	%ebx, %esi
	jg	.L95
	testb	%al, %al
	je	.L96
.L95:
	addq	$1, %rbx
.L92:
	movzbl	(%rdi,%rbx), %eax
	movl	%ebx, %r14d
	movl	%ebx, %r12d
	cmpb	$1, %al
	jne	.L93
	subl	$1, %ecx
	jne	.L95
.L94:
	leal	0(%r13,%r12), %eax
	testq	%r8, %r8
	je	.L98
	cmpl	%r9d, %eax
	jle	.L125
.L98:
	addl	$1, %eax
.L90:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	%rdi, %rsi
	movq	%rbx, %rdx
	movq	%r8, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	cmpl	$1, %r13d
	je	.L99
	cmpl	$2, %r13d
	je	.L100
	testl	%r13d, %r13d
	je	.L101
	movl	$1, (%r15)
	xorl	%eax, %eax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$-123, (%r15)
	jmp	.L94
.L99:
	leal	1(%r14), %r12d
	movb	$2, (%rax,%rbx)
	movslq	%r12d, %rbx
.L101:
	movb	$0, (%r8,%rbx)
	addq	$8, %rsp
	leal	1(%r12), %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movl	$1, (%r15)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L100:
	movb	$-1, (%rax,%rbx)
	leal	1(%r14), %eax
	leal	2(%r14), %r12d
	cltq
	movslq	%r12d, %rbx
	movb	$-1, (%r8,%rax)
	jmp	.L101
	.cfi_endproc
.LFE2444:
	.size	ucol_getBound_67, .-ucol_getBound_67
	.p2align 4
	.globl	ucol_setMaxVariable_67
	.type	ucol_setMaxVariable_67, @function
ucol_setMaxVariable_67:
.LFB2445:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L126
	movq	(%rdi), %rax
	jmp	*200(%rax)
	.p2align 4,,10
	.p2align 3
.L126:
	ret
	.cfi_endproc
.LFE2445:
	.size	ucol_setMaxVariable_67, .-ucol_setMaxVariable_67
	.p2align 4
	.globl	ucol_getMaxVariable_67
	.type	ucol_getMaxVariable_67, @function
ucol_getMaxVariable_67:
.LFB2446:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*208(%rax)
	.cfi_endproc
.LFE2446:
	.size	ucol_getMaxVariable_67, .-ucol_getMaxVariable_67
	.p2align 4
	.globl	ucol_setVariableTop_67
	.type	ucol_setVariableTop_67, @function
ucol_setVariableTop_67:
.LFB2447:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L130
	testq	%rdi, %rdi
	je	.L130
	movq	(%rdi), %rax
	jmp	*216(%rax)
	.p2align 4,,10
	.p2align 3
.L130:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2447:
	.size	ucol_setVariableTop_67, .-ucol_setVariableTop_67
	.p2align 4
	.globl	ucol_getVariableTop_67
	.type	ucol_getVariableTop_67, @function
ucol_getVariableTop_67:
.LFB2448:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L132
	testq	%rdi, %rdi
	je	.L132
	movq	(%rdi), %rax
	jmp	*240(%rax)
	.p2align 4,,10
	.p2align 3
.L132:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2448:
	.size	ucol_getVariableTop_67, .-ucol_getVariableTop_67
	.p2align 4
	.globl	ucol_restoreVariableTop_67
	.type	ucol_restoreVariableTop_67, @function
ucol_restoreVariableTop_67:
.LFB2449:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L133
	testq	%rdi, %rdi
	je	.L133
	movq	(%rdi), %rax
	jmp	*232(%rax)
	.p2align 4,,10
	.p2align 3
.L133:
	ret
	.cfi_endproc
.LFE2449:
	.size	ucol_restoreVariableTop_67, .-ucol_restoreVariableTop_67
	.p2align 4
	.globl	ucol_setAttribute_67
	.type	ucol_setAttribute_67, @function
ucol_setAttribute_67:
.LFB2450:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L135
	testq	%rdi, %rdi
	je	.L135
	movq	(%rdi), %rax
	jmp	*184(%rax)
	.p2align 4,,10
	.p2align 3
.L135:
	ret
	.cfi_endproc
.LFE2450:
	.size	ucol_setAttribute_67, .-ucol_setAttribute_67
	.p2align 4
	.globl	ucol_getAttribute_67
	.type	ucol_getAttribute_67, @function
ucol_getAttribute_67:
.LFB2451:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L138
	testq	%rdi, %rdi
	je	.L138
	movq	(%rdi), %rax
	jmp	*192(%rax)
	.p2align 4,,10
	.p2align 3
.L138:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2451:
	.size	ucol_getAttribute_67, .-ucol_getAttribute_67
	.p2align 4
	.globl	ucol_setStrength_67
	.type	ucol_setStrength_67, @function
ucol_setStrength_67:
.LFB2452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$0, -12(%rbp)
	testq	%rdi, %rdi
	je	.L139
	movq	(%rdi), %rax
	movl	%esi, %edx
	leaq	-12(%rbp), %rcx
	movl	$5, %esi
	call	*184(%rax)
.L139:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L146
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L146:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2452:
	.size	ucol_setStrength_67, .-ucol_setStrength_67
	.p2align 4
	.globl	ucol_getStrength_67
	.type	ucol_getStrength_67, @function
ucol_getStrength_67:
.LFB2453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$0, -12(%rbp)
	testq	%rdi, %rdi
	je	.L150
	movq	(%rdi), %rax
	leaq	-12(%rbp), %rdx
	movl	$5, %esi
	call	*192(%rax)
.L147:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L152
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L150:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L147
.L152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2453:
	.size	ucol_getStrength_67, .-ucol_getStrength_67
	.p2align 4
	.globl	ucol_getReorderCodes_67
	.type	ucol_getReorderCodes_67, @function
ucol_getReorderCodes_67:
.LFB2454:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L154
	movq	(%rdi), %rax
	jmp	*160(%rax)
	.p2align 4,,10
	.p2align 3
.L154:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2454:
	.size	ucol_getReorderCodes_67, .-ucol_getReorderCodes_67
	.p2align 4
	.globl	ucol_setReorderCodes_67
	.type	ucol_setReorderCodes_67, @function
ucol_setReorderCodes_67:
.LFB2455:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L155
	movq	(%rdi), %rax
	jmp	*168(%rax)
	.p2align 4,,10
	.p2align 3
.L155:
	ret
	.cfi_endproc
.LFE2455:
	.size	ucol_setReorderCodes_67, .-ucol_setReorderCodes_67
	.p2align 4
	.globl	ucol_getEquivalentReorderCodes_67
	.type	ucol_getEquivalentReorderCodes_67, @function
ucol_getEquivalentReorderCodes_67:
.LFB2456:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_678Collator25getEquivalentReorderCodesEiPiiR10UErrorCode@PLT
	.cfi_endproc
.LFE2456:
	.size	ucol_getEquivalentReorderCodes_67, .-ucol_getEquivalentReorderCodes_67
	.p2align 4
	.globl	ucol_getVersion_67
	.type	ucol_getVersion_67, @function
ucol_getVersion_67:
.LFB2457:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*176(%rax)
	.cfi_endproc
.LFE2457:
	.size	ucol_getVersion_67, .-ucol_getVersion_67
	.p2align 4
	.globl	ucol_strcollIter_67
	.type	ucol_strcollIter_67, @function
ucol_strcollIter_67:
.LFB2458:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L159
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L159
	testq	%rdx, %rdx
	sete	%r8b
	testq	%rdi, %rdi
	sete	%al
	orb	%al, %r8b
	jne	.L165
	testq	%rsi, %rsi
	je	.L165
	movq	(%rdi), %rax
	jmp	*96(%rax)
	.p2align 4,,10
	.p2align 3
.L165:
	movl	$1, (%rcx)
.L159:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2458:
	.size	ucol_strcollIter_67, .-ucol_strcollIter_67
	.p2align 4
	.globl	ucol_strcoll_67
	.type	ucol_strcoll_67, @function
ucol_strcoll_67:
.LFB2459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -12(%rbp)
	leaq	-12(%rbp), %r9
	call	*88(%rax)
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L170
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L170:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2459:
	.size	ucol_strcoll_67, .-ucol_strcoll_67
	.p2align 4
	.globl	ucol_strcollUTF8_67
	.type	ucol_strcollUTF8_67, @function
ucol_strcollUTF8_67:
.LFB2460:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L172
	movq	(%rdi), %rax
	jmp	*296(%rax)
	.p2align 4,,10
	.p2align 3
.L172:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2460:
	.size	ucol_strcollUTF8_67, .-ucol_strcollUTF8_67
	.p2align 4
	.globl	ucol_greater_67
	.type	ucol_greater_67, @function
ucol_greater_67:
.LFB2461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -12(%rbp)
	leaq	-12(%rbp), %r9
	call	*88(%rax)
	cmpl	$1, %eax
	sete	%al
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L176
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L176:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2461:
	.size	ucol_greater_67, .-ucol_greater_67
	.p2align 4
	.globl	ucol_greaterOrEqual_67
	.type	ucol_greaterOrEqual_67, @function
ucol_greaterOrEqual_67:
.LFB2462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -12(%rbp)
	leaq	-12(%rbp), %r9
	call	*88(%rax)
	cmpl	$-1, %eax
	setne	%al
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L180
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L180:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2462:
	.size	ucol_greaterOrEqual_67, .-ucol_greaterOrEqual_67
	.p2align 4
	.globl	ucol_equal_67
	.type	ucol_equal_67, @function
ucol_equal_67:
.LFB2463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -12(%rbp)
	leaq	-12(%rbp), %r9
	call	*88(%rax)
	testl	%eax, %eax
	sete	%al
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L184
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L184:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2463:
	.size	ucol_equal_67, .-ucol_equal_67
	.p2align 4
	.globl	ucol_getUCAVersion_67
	.type	ucol_getUCAVersion_67, @function
ucol_getUCAVersion_67:
.LFB2464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L185
	movq	(%rdi), %rax
	movq	%rsi, %rbx
	leaq	-28(%rbp), %rsi
	call	*176(%rax)
	movzbl	-27(%rbp), %edx
	movb	$0, 3(%rbx)
	movl	%edx, %eax
	sarl	$3, %edx
	andl	$7, %eax
	movb	%dl, (%rbx)
	movb	%al, 1(%rbx)
	movzbl	-26(%rbp), %eax
	sarl	$6, %eax
	movb	%al, 2(%rbx)
.L185:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L192
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L192:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2464:
	.size	ucol_getUCAVersion_67, .-ucol_getUCAVersion_67
	.p2align 4
	.globl	ucol_getRules_67
	.type	ucol_getRules_67, @function
ucol_getRules_67:
.LFB2465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L194
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717RuleBasedCollatorE(%rip), %rdx
	leaq	_ZTIN6icu_678CollatorE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L194
	movl	$0, (%rbx)
	leaq	_ZZ16ucol_getRules_67E4_NUL(%rip), %rax
.L193:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	call	_ZNK6icu_6717RuleBasedCollator8getRulesEv@PLT
	movswl	8(%rax), %edx
	testw	%dx, %dx
	js	.L196
	sarl	$5, %edx
.L197:
	movl	%edx, (%rbx)
	movzwl	8(%rax), %edx
	testb	$17, %dl
	jne	.L200
	andl	$2, %edx
	je	.L199
	addq	$8, %rsp
	addq	$10, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movl	12(%rax), %edx
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L199:
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L193
	.cfi_endproc
.LFE2465:
	.size	ucol_getRules_67, .-ucol_getRules_67
	.p2align 4
	.globl	ucol_getRulesEx_67
	.type	ucol_getRulesEx_67, @function
ucol_getRulesEx_67:
.LFB2466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	testq	%rdx, %rdx
	setne	%bl
	movq	%rax, -128(%rbp)
	testl	%ecx, %ecx
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	setg	%al
	andl	%eax, %ebx
	testq	%rdi, %rdi
	je	.L206
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717RuleBasedCollatorE(%rip), %rdx
	leaq	_ZTIN6icu_678CollatorE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L206
	leaq	-128(%rbp), %r14
	testb	%bl, %bl
	jne	.L211
	movl	$2, %r12d
	leaq	-128(%rbp), %r14
.L212:
	sarl	$5, %r12d
.L209:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L221
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	leaq	-140(%rbp), %rcx
	leaq	-136(%rbp), %rsi
	movl	%r15d, %edx
	movq	%r14, %rdi
	movq	%r12, -136(%rbp)
	movl	$0, -140(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L206:
	leaq	-128(%rbp), %r14
	movl	%r13d, %esi
	movq	%r14, %rdx
	call	_ZNK6icu_6717RuleBasedCollator8getRulesE14UColRuleOptionRNS_13UnicodeStringE@PLT
	testb	%bl, %bl
	jne	.L211
	movswl	-120(%rbp), %r12d
	testw	%r12w, %r12w
	jns	.L212
	movl	-116(%rbp), %r12d
	jmp	.L209
.L221:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2466:
	.size	ucol_getRulesEx_67, .-ucol_getRulesEx_67
	.p2align 4
	.globl	ucol_getLocale_67
	.type	ucol_getLocale_67, @function
ucol_getLocale_67:
.LFB2467:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L228
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	testq	%rdi, %rdi
	je	.L226
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717RuleBasedCollatorE(%rip), %rdx
	leaq	_ZTIN6icu_678CollatorE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L224
	movl	$16, (%r12)
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	xorl	%edi, %edi
.L224:
	movq	%r12, %rdx
	movl	%r13d, %esi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6717RuleBasedCollator19internalGetLocaleIDE18ULocDataLocaleTypeR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L228:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2467:
	.size	ucol_getLocale_67, .-ucol_getLocale_67
	.p2align 4
	.globl	ucol_getLocaleByType_67
	.type	ucol_getLocaleByType_67, @function
ucol_getLocaleByType_67:
.LFB2468:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L237
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	testq	%rdi, %rdi
	je	.L235
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717RuleBasedCollatorE(%rip), %rdx
	leaq	_ZTIN6icu_678CollatorE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L233
	movl	$16, (%r12)
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	xorl	%edi, %edi
.L233:
	movq	%r12, %rdx
	movl	%r13d, %esi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6717RuleBasedCollator19internalGetLocaleIDE18ULocDataLocaleTypeR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L237:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2468:
	.size	ucol_getLocaleByType_67, .-ucol_getLocaleByType_67
	.p2align 4
	.globl	ucol_equals_67
	.type	ucol_equals_67, @function
ucol_equals_67:
.LFB2470:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L243
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*24(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2470:
	.size	ucol_equals_67, .-ucol_equals_67
	.p2align 4
	.globl	ucol_getTailoredSet_67
	.type	ucol_getTailoredSet_67, @function
ucol_getTailoredSet_67:
.LFB2469:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L259
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*248(%rax)
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L260
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L258
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L258:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2469:
	.size	ucol_getTailoredSet_67, .-ucol_getTailoredSet_67
	.section	.rodata
	.align 2
	.type	_ZZ16ucol_getRules_67E4_NUL, @object
	.size	_ZZ16ucol_getRules_67E4_NUL, 2
_ZZ16ucol_getRules_67E4_NUL:
	.zero	2
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
