	.file	"funcrepl.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716FunctionReplacer17getDynamicClassIDEv
	.type	_ZNK6icu_6716FunctionReplacer17getDynamicClassIDEv, @function
_ZNK6icu_6716FunctionReplacer17getDynamicClassIDEv:
.LFB2274:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6716FunctionReplacer16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2274:
	.size	_ZNK6icu_6716FunctionReplacer17getDynamicClassIDEv, .-_ZNK6icu_6716FunctionReplacer17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716FunctionReplacer10toReplacerEv
	.type	_ZNK6icu_6716FunctionReplacer10toReplacerEv, @function
_ZNK6icu_6716FunctionReplacer10toReplacerEv:
.LFB2301:
	.cfi_startproc
	endbr64
	leaq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE2301:
	.size	_ZNK6icu_6716FunctionReplacer10toReplacerEv, .-_ZNK6icu_6716FunctionReplacer10toReplacerEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716FunctionReplacer7setDataEPKNS_23TransliterationRuleDataE
	.type	_ZN6icu_6716FunctionReplacer7setDataEPKNS_23TransliterationRuleDataE, @function
_ZN6icu_6716FunctionReplacer7setDataEPKNS_23TransliterationRuleDataE:
.LFB2305:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*48(%rax)
	.cfi_endproc
.LFE2305:
	.size	_ZN6icu_6716FunctionReplacer7setDataEPKNS_23TransliterationRuleDataE, .-_ZN6icu_6716FunctionReplacer7setDataEPKNS_23TransliterationRuleDataE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716FunctionReplacerD2Ev
	.type	_ZN6icu_6716FunctionReplacerD2Ev, @function
_ZN6icu_6716FunctionReplacerD2Ev:
.LFB2297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	112+_ZTVN6icu_6716FunctionReplacerE(%rip), %rax
	leaq	-96(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L6
	movq	(%rdi), %rax
	call	*8(%rax)
.L6:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L7
	movq	(%rdi), %rax
	call	*8(%rax)
.L7:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6715UnicodeReplacerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	.cfi_endproc
.LFE2297:
	.size	_ZN6icu_6716FunctionReplacerD2Ev, .-_ZN6icu_6716FunctionReplacerD2Ev
	.globl	_ZN6icu_6716FunctionReplacerD1Ev
	.set	_ZN6icu_6716FunctionReplacerD1Ev,_ZN6icu_6716FunctionReplacerD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716FunctionReplacer5cloneEv
	.type	_ZNK6icu_6716FunctionReplacer5cloneEv, @function
_ZNK6icu_6716FunctionReplacer5cloneEv:
.LFB2300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L15
	movq	16(%rbx), %rdi
	leaq	112+_ZTVN6icu_6716FunctionReplacerE(%rip), %rax
	leaq	-96(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	(%rdi), %rax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	call	*24(%rax)
	movq	24(%rbx), %rdi
	movq	%rax, 16(%r12)
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 24(%r12)
.L15:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2300:
	.size	_ZNK6icu_6716FunctionReplacer5cloneEv, .-_ZNK6icu_6716FunctionReplacer5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi
	.type	_ZN6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi, @function
_ZN6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi:
.LFB2302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	leaq	_ZThn8_N6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L22
	subq	$8, %rdi
	movq	%r15, %r8
	movl	%r14d, %ecx
	movl	%ebx, %edx
	movq	%r13, %rsi
	call	.LTHUNK2
.L23:
	movq	16(%r12), %rdi
	leal	(%rbx,%rax), %ecx
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	addq	$8, %rsp
	subl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	%r15, %r8
	movl	%r14d, %ecx
	movl	%ebx, %edx
	movq	%r13, %rsi
	call	*%rax
	jmp	.L23
	.cfi_endproc
.LFE2302:
	.size	_ZN6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi, .-_ZN6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi
	.set	.LTHUNK2,_ZN6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716FunctionReplacer17toReplacerPatternERNS_13UnicodeStringEa
	.type	_ZNK6icu_6716FunctionReplacer17toReplacerPatternERNS_13UnicodeStringEa, @function
_ZNK6icu_6716FunctionReplacer17toReplacerPatternERNS_13UnicodeStringEa:
.LFB2303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	movl	$2, %edx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -104(%rbp)
	movq	%rax, -112(%rbp)
	movzwl	8(%rsi), %eax
	testb	$1, %al
	je	.L26
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L27:
	leaq	-114(%rbp), %rsi
	movl	$1, %ecx
	movl	$38, %eax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%ax, -114(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	call	*104(%rax)
	movswl	8(%rax), %ecx
	movq	%rax, %rsi
	testw	%cx, %cx
	js	.L30
	sarl	$5, %ecx
.L31:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$2, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	_ZL4OPEN(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	_ZL4OPEN(%rip), %rax
	movq	24(%r13), %rdi
	leaq	-112(%rbp), %r13
	movq	(%rdi), %rax
	call	*40(%rax)
	leaq	_ZThn8_NK6icu_6716FunctionReplacer17toReplacerPatternERNS_13UnicodeStringEa(%rip), %rcx
	movsbl	%bl, %edx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L32
	movq	%r13, %rsi
	subq	$8, %rdi
	call	.LTHUNK3
	movq	%rax, %rsi
.L33:
	movswl	8(%rsi), %ecx
	testw	%cx, %cx
	js	.L34
	sarl	$5, %ecx
.L35:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$2, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	_ZL5CLOSE(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	_ZL5CLOSE(%rip), %rax
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L28
	movswl	%ax, %edx
	sarl	$5, %edx
.L29:
	testl	%edx, %edx
	je	.L27
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	movl	12(%rsi), %edx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	movl	12(%rax), %ecx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L34:
	movl	12(%rsi), %ecx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%r13, %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L33
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2303:
	.size	_ZNK6icu_6716FunctionReplacer17toReplacerPatternERNS_13UnicodeStringEa, .-_ZNK6icu_6716FunctionReplacer17toReplacerPatternERNS_13UnicodeStringEa
	.set	.LTHUNK3,_ZNK6icu_6716FunctionReplacer17toReplacerPatternERNS_13UnicodeStringEa
	.p2align 4
	.globl	_ZThn8_NK6icu_6716FunctionReplacer17toReplacerPatternERNS_13UnicodeStringEa
	.type	_ZThn8_NK6icu_6716FunctionReplacer17toReplacerPatternERNS_13UnicodeStringEa, @function
_ZThn8_NK6icu_6716FunctionReplacer17toReplacerPatternERNS_13UnicodeStringEa:
.LFB2966:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK3
	.cfi_endproc
.LFE2966:
	.size	_ZThn8_NK6icu_6716FunctionReplacer17toReplacerPatternERNS_13UnicodeStringEa, .-_ZThn8_NK6icu_6716FunctionReplacer17toReplacerPatternERNS_13UnicodeStringEa
	.p2align 4
	.globl	_ZThn8_N6icu_6716FunctionReplacerD0Ev
	.type	_ZThn8_N6icu_6716FunctionReplacerD0Ev, @function
_ZThn8_N6icu_6716FunctionReplacerD0Ev:
.LFB2960:
	.cfi_startproc
	endbr64
	leaq	112+_ZTVN6icu_6716FunctionReplacerE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-96(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	-8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movups	%xmm0, -8(%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L43
	movq	(%rdi), %rax
	call	*8(%rax)
.L43:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L44
	movq	(%rdi), %rax
	call	*8(%rax)
.L44:
	movq	%r12, %rdi
	call	_ZN6icu_6715UnicodeReplacerD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2960:
	.size	_ZThn8_N6icu_6716FunctionReplacerD0Ev, .-_ZThn8_N6icu_6716FunctionReplacerD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716FunctionReplacerD0Ev
	.type	_ZN6icu_6716FunctionReplacerD0Ev, @function
_ZN6icu_6716FunctionReplacerD0Ev:
.LFB2299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	112+_ZTVN6icu_6716FunctionReplacerE(%rip), %rax
	leaq	-96(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L53
	movq	(%rdi), %rax
	call	*8(%rax)
.L53:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L54
	movq	(%rdi), %rax
	call	*8(%rax)
.L54:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6715UnicodeReplacerD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2299:
	.size	_ZN6icu_6716FunctionReplacerD0Ev, .-_ZN6icu_6716FunctionReplacerD0Ev
	.p2align 4
	.globl	_ZThn8_N6icu_6716FunctionReplacerD1Ev
	.type	_ZThn8_N6icu_6716FunctionReplacerD1Ev, @function
_ZThn8_N6icu_6716FunctionReplacerD1Ev:
.LFB2962:
	.cfi_startproc
	endbr64
	leaq	112+_ZTVN6icu_6716FunctionReplacerE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-96(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movups	%xmm0, -8(%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L63
	movq	(%rdi), %rax
	call	*8(%rax)
.L63:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L64
	movq	(%rdi), %rax
	call	*8(%rax)
.L64:
	leaq	-8(%r12), %r13
	movq	%r12, %rdi
	call	_ZN6icu_6715UnicodeReplacerD2Ev@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	.cfi_endproc
.LFE2962:
	.size	_ZThn8_N6icu_6716FunctionReplacerD1Ev, .-_ZThn8_N6icu_6716FunctionReplacerD1Ev
	.p2align 4
	.globl	_ZThn8_N6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi
	.type	_ZThn8_N6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi, @function
_ZThn8_N6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi:
.LFB2963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	leaq	_ZThn8_N6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L76
	movq	%r15, %r8
	movl	%r14d, %ecx
	movl	%ebx, %edx
	movq	%r13, %rsi
	call	*%rax
.L74:
	movq	8(%r12), %rdi
	leal	(%rbx,%rax), %ecx
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	addq	$8, %rsp
	subl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	subq	$8, %rdi
	movq	%r15, %r8
	movl	%r14d, %ecx
	movl	%ebx, %edx
	movq	%r13, %rsi
	call	.LTHUNK2
	jmp	.L74
	.cfi_endproc
.LFE2963:
	.size	_ZThn8_N6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi, .-_ZThn8_N6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716FunctionReplacer16getStaticClassIDEv
	.type	_ZN6icu_6716FunctionReplacer16getStaticClassIDEv, @function
_ZN6icu_6716FunctionReplacer16getStaticClassIDEv:
.LFB2273:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6716FunctionReplacer16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2273:
	.size	_ZN6icu_6716FunctionReplacer16getStaticClassIDEv, .-_ZN6icu_6716FunctionReplacer16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716FunctionReplacerC2EPNS_14TransliteratorEPNS_14UnicodeFunctorE
	.type	_ZN6icu_6716FunctionReplacerC2EPNS_14TransliteratorEPNS_14UnicodeFunctorE, @function
_ZN6icu_6716FunctionReplacerC2EPNS_14TransliteratorEPNS_14UnicodeFunctorE:
.LFB2282:
	.cfi_startproc
	endbr64
	leaq	112+_ZTVN6icu_6716FunctionReplacerE(%rip), %rax
	movq	%rdx, %xmm2
	leaq	-96(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE2282:
	.size	_ZN6icu_6716FunctionReplacerC2EPNS_14TransliteratorEPNS_14UnicodeFunctorE, .-_ZN6icu_6716FunctionReplacerC2EPNS_14TransliteratorEPNS_14UnicodeFunctorE
	.globl	_ZN6icu_6716FunctionReplacerC1EPNS_14TransliteratorEPNS_14UnicodeFunctorE
	.set	_ZN6icu_6716FunctionReplacerC1EPNS_14TransliteratorEPNS_14UnicodeFunctorE,_ZN6icu_6716FunctionReplacerC2EPNS_14TransliteratorEPNS_14UnicodeFunctorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716FunctionReplacerC2ERKS0_
	.type	_ZN6icu_6716FunctionReplacerC2ERKS0_, @function
_ZN6icu_6716FunctionReplacerC2ERKS0_:
.LFB2294:
	.cfi_startproc
	endbr64
	leaq	112+_ZTVN6icu_6716FunctionReplacerE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-96(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movups	%xmm0, (%rdi)
	movq	16(%rsi), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	24(%r12), %rdi
	movq	%rax, 16(%rbx)
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2294:
	.size	_ZN6icu_6716FunctionReplacerC2ERKS0_, .-_ZN6icu_6716FunctionReplacerC2ERKS0_
	.globl	_ZN6icu_6716FunctionReplacerC1ERKS0_
	.set	_ZN6icu_6716FunctionReplacerC1ERKS0_,_ZN6icu_6716FunctionReplacerC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716FunctionReplacer19addReplacementSetToERNS_10UnicodeSetE
	.type	_ZNK6icu_6716FunctionReplacer19addReplacementSetToERNS_10UnicodeSetE, @function
_ZNK6icu_6716FunctionReplacer19addReplacementSetToERNS_10UnicodeSetE:
.LFB2304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-240(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*128(%rax)
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L84:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2304:
	.size	_ZNK6icu_6716FunctionReplacer19addReplacementSetToERNS_10UnicodeSetE, .-_ZNK6icu_6716FunctionReplacer19addReplacementSetToERNS_10UnicodeSetE
	.p2align 4
	.globl	_ZThn8_NK6icu_6716FunctionReplacer19addReplacementSetToERNS_10UnicodeSetE
	.type	_ZThn8_NK6icu_6716FunctionReplacer19addReplacementSetToERNS_10UnicodeSetE, @function
_ZThn8_NK6icu_6716FunctionReplacer19addReplacementSetToERNS_10UnicodeSetE:
.LFB2961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-240(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*128(%rax)
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L88:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2961:
	.size	_ZThn8_NK6icu_6716FunctionReplacer19addReplacementSetToERNS_10UnicodeSetE, .-_ZThn8_NK6icu_6716FunctionReplacer19addReplacementSetToERNS_10UnicodeSetE
	.weak	_ZTSN6icu_6716FunctionReplacerE
	.section	.rodata._ZTSN6icu_6716FunctionReplacerE,"aG",@progbits,_ZTSN6icu_6716FunctionReplacerE,comdat
	.align 16
	.type	_ZTSN6icu_6716FunctionReplacerE, @object
	.size	_ZTSN6icu_6716FunctionReplacerE, 28
_ZTSN6icu_6716FunctionReplacerE:
	.string	"N6icu_6716FunctionReplacerE"
	.weak	_ZTIN6icu_6716FunctionReplacerE
	.section	.data.rel.ro._ZTIN6icu_6716FunctionReplacerE,"awG",@progbits,_ZTIN6icu_6716FunctionReplacerE,comdat
	.align 8
	.type	_ZTIN6icu_6716FunctionReplacerE, @object
	.size	_ZTIN6icu_6716FunctionReplacerE, 56
_ZTIN6icu_6716FunctionReplacerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_6716FunctionReplacerE
	.long	0
	.long	2
	.quad	_ZTIN6icu_6714UnicodeFunctorE
	.quad	2
	.quad	_ZTIN6icu_6715UnicodeReplacerE
	.quad	2050
	.weak	_ZTVN6icu_6716FunctionReplacerE
	.section	.data.rel.ro._ZTVN6icu_6716FunctionReplacerE,"awG",@progbits,_ZTVN6icu_6716FunctionReplacerE,comdat
	.align 8
	.type	_ZTVN6icu_6716FunctionReplacerE, @object
	.size	_ZTVN6icu_6716FunctionReplacerE, 152
_ZTVN6icu_6716FunctionReplacerE:
	.quad	0
	.quad	_ZTIN6icu_6716FunctionReplacerE
	.quad	_ZN6icu_6716FunctionReplacerD1Ev
	.quad	_ZN6icu_6716FunctionReplacerD0Ev
	.quad	_ZNK6icu_6716FunctionReplacer17getDynamicClassIDEv
	.quad	_ZNK6icu_6716FunctionReplacer5cloneEv
	.quad	_ZNK6icu_6714UnicodeFunctor9toMatcherEv
	.quad	_ZNK6icu_6716FunctionReplacer10toReplacerEv
	.quad	_ZN6icu_6716FunctionReplacer7setDataEPKNS_23TransliterationRuleDataE
	.quad	_ZN6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi
	.quad	_ZNK6icu_6716FunctionReplacer17toReplacerPatternERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6716FunctionReplacer19addReplacementSetToERNS_10UnicodeSetE
	.quad	-8
	.quad	_ZTIN6icu_6716FunctionReplacerE
	.quad	_ZThn8_N6icu_6716FunctionReplacerD1Ev
	.quad	_ZThn8_N6icu_6716FunctionReplacerD0Ev
	.quad	_ZThn8_N6icu_6716FunctionReplacer7replaceERNS_11ReplaceableEiiRi
	.quad	_ZThn8_NK6icu_6716FunctionReplacer17toReplacerPatternERNS_13UnicodeStringEa
	.quad	_ZThn8_NK6icu_6716FunctionReplacer19addReplacementSetToERNS_10UnicodeSetE
	.local	_ZZN6icu_6716FunctionReplacer16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6716FunctionReplacer16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.align 2
	.type	_ZL5CLOSE, @object
	.size	_ZL5CLOSE, 6
_ZL5CLOSE:
	.value	32
	.value	41
	.value	0
	.align 2
	.type	_ZL4OPEN, @object
	.size	_ZL4OPEN, 6
_ZL4OPEN:
	.value	40
	.value	32
	.value	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
