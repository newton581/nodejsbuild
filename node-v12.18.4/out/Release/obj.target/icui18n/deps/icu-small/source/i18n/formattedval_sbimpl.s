	.file	"formattedval_sbimpl.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev
	.type	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev, @function
_ZN6icu_6731FormattedValueStringBuilderImplD2Ev:
.LFB3363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6731FormattedValueStringBuilderImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6722FormattedStringBuilderD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714FormattedValueD2Ev@PLT
	.cfi_endproc
.LFE3363:
	.size	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev, .-_ZN6icu_6731FormattedValueStringBuilderImplD2Ev
	.globl	_ZN6icu_6731FormattedValueStringBuilderImplD1Ev
	.set	_ZN6icu_6731FormattedValueStringBuilderImplD1Ev,_ZN6icu_6731FormattedValueStringBuilderImplD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6731FormattedValueStringBuilderImplD0Ev
	.type	_ZN6icu_6731FormattedValueStringBuilderImplD0Ev, @function
_ZN6icu_6731FormattedValueStringBuilderImplD0Ev:
.LFB3365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6731FormattedValueStringBuilderImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6722FormattedStringBuilderD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714FormattedValueD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3365:
	.size	_ZN6icu_6731FormattedValueStringBuilderImplD0Ev, .-_ZN6icu_6731FormattedValueStringBuilderImplD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6731FormattedValueStringBuilderImpl8toStringER10UErrorCode
	.type	_ZNK6icu_6731FormattedValueStringBuilderImpl8toStringER10UErrorCode, @function
_ZNK6icu_6731FormattedValueStringBuilderImpl8toStringER10UErrorCode:
.LFB3366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6722FormattedStringBuilder15toUnicodeStringEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3366:
	.size	_ZNK6icu_6731FormattedValueStringBuilderImpl8toStringER10UErrorCode, .-_ZNK6icu_6731FormattedValueStringBuilderImpl8toStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6731FormattedValueStringBuilderImpl12toTempStringER10UErrorCode
	.type	_ZNK6icu_6731FormattedValueStringBuilderImpl12toTempStringER10UErrorCode, @function
_ZNK6icu_6731FormattedValueStringBuilderImpl12toTempStringER10UErrorCode:
.LFB3367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6722FormattedStringBuilder19toTempUnicodeStringEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L13
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L13:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3367:
	.size	_ZNK6icu_6731FormattedValueStringBuilderImpl12toTempStringER10UErrorCode, .-_ZNK6icu_6731FormattedValueStringBuilderImpl12toTempStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode
	.type	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode, @function
_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode:
.LFB3368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	8(%rdi), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	40(%rax), %rbx
	call	_ZNK6icu_6722FormattedStringBuilder6lengthEv@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	_ZNK6icu_6722FormattedStringBuilder5charsEv@PLT
	movl	%r14d, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	*%rbx
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3368:
	.size	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode, .-_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3851:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3851:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3854:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L29
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L17
	cmpb	$0, 12(%rbx)
	jne	.L30
.L21:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L17:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L21
	.cfi_endproc
.LFE3854:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3857:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L33
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3857:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3860:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L36
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3860:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L42
.L38:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L43
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L43:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3862:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3863:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3863:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3864:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3864:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3865:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3865:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3866:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3866:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3867:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3867:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3868:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L59
	testl	%edx, %edx
	jle	.L59
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L62
.L51:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L51
	.cfi_endproc
.LFE3868:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L66
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L66
	testl	%r12d, %r12d
	jg	.L73
	cmpb	$0, 12(%rbx)
	jne	.L74
.L68:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L68
.L74:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3869:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L76
	movq	(%rdi), %r8
.L77:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L80
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L80
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L80:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3870:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3871:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L87
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3871:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3872:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3872:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3873:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3873:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3874:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3874:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3876:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3876:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3878:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3878:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE
	.type	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE, @function
_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE:
.LFB3360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6731FormattedValueStringBuilderImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6722FormattedStringBuilderC1Ev@PLT
	movb	%r12b, 144(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3360:
	.size	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE, .-_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE
	.globl	_ZN6icu_6731FormattedValueStringBuilderImplC1ENS_22FormattedStringBuilder5FieldE
	.set	_ZN6icu_6731FormattedValueStringBuilderImplC1ENS_22FormattedStringBuilder5FieldE,_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6731FormattedValueStringBuilderImpl16nextPositionImplERNS_24ConstrainedFieldPositionENS_22FormattedStringBuilder5FieldER10UErrorCode
	.type	_ZNK6icu_6731FormattedValueStringBuilderImpl16nextPositionImplERNS_24ConstrainedFieldPositionENS_22FormattedStringBuilder5FieldER10UErrorCode, @function
_ZNK6icu_6731FormattedValueStringBuilderImpl16nextPositionImplERNS_24ConstrainedFieldPositionENS_22FormattedStringBuilder5FieldER10UErrorCode:
.LFB3372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movb	%dl, -53(%rbp)
	movl	16(%rsi), %ecx
	movl	136(%rdi), %edx
	movl	140(%rdi), %esi
	leal	(%rdx,%rcx), %r15d
	leal	(%rdx,%rsi), %eax
	cmpl	%esi, %ecx
	jg	.L130
	movzbl	-53(%rbp), %r14d
	movq	%rdi, %r13
	movl	$-1, -52(%rbp)
	xorl	%ebx, %ebx
	movl	%r14d, %ecx
	sarl	$4, %r14d
	movl	%ecx, %edi
	movl	%r14d, -64(%rbp)
	andl	$15, %edi
	movl	%edi, -60(%rbp)
.L131:
	cmpl	%eax, %r15d
	jge	.L98
	cmpb	$0, 8(%r13)
	leaq	96(%r13), %rax
	je	.L100
	movq	96(%r13), %rax
.L100:
	movslq	%r15d, %rcx
	movzbl	(%rax,%rcx), %r14d
	testb	%bl, %bl
	je	.L101
	cmpb	%r14b, %bl
	je	.L164
.L102:
	movl	%r15d, %r14d
	subl	%edx, %r14d
	cmpb	$38, %bl
	je	.L104
	movl	$1, %edi
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	cmpb	$0, 8(%r13)
	movq	%rax, %rdi
	leaq	16(%r13), %rax
	jne	.L165
.L106:
	movslq	136(%r13), %rdx
	movl	$1, %ecx
	leaq	(%rax,%rdx,2), %rsi
	movl	%r14d, %edx
	call	_ZNK6icu_6710UnicodeSet8spanBackEPKDsi17USetSpanCondition@PLT
	movl	%eax, %r14d
.L104:
	cmpl	-52(%rbp), %r14d
	jg	.L107
	movl	$-1, -52(%rbp)
	movl	136(%r13), %edx
	xorl	%ebx, %ebx
	movl	140(%r13), %esi
.L103:
	leal	(%rdx,%rsi), %eax
	cmpl	%r15d, %eax
	jge	.L131
.L130:
	xorl	%eax, %eax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$-1, %r14d
	testb	%bl, %bl
	jne	.L102
.L101:
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6724ConstrainedFieldPosition12matchesFieldEii@PLT
	testb	%al, %al
	je	.L111
	movl	136(%r13), %edx
	cmpl	%r15d, %edx
	jl	.L166
.L111:
	cmpb	$0, -53(%rbp)
	jne	.L167
.L117:
	leal	1(%r15), %ebx
	cmpb	$32, %r14b
	je	.L162
	movzbl	%r14b, %esi
	sarl	$4, %esi
	je	.L162
	cmpb	$-1, %r14b
	jne	.L127
.L162:
	movl	136(%r13), %edx
.L163:
	movl	%ebx, %r15d
	movl	140(%r13), %esi
	xorl	%ebx, %ebx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L107:
	cmpb	$38, %bl
	jne	.L168
.L108:
	movl	-52(%rbp), %ecx
	movl	%ebx, %edx
	movzbl	%bl, %esi
	movl	%r14d, %r8d
	andl	$15, %edx
	sarl	$4, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6724ConstrainedFieldPosition8setStateEiiii@PLT
	movl	$1, %eax
.L95:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movl	$1, %edi
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movl	140(%r13), %edx
	subl	-52(%rbp), %edx
	leaq	16(%r13), %rcx
	cmpb	$0, 8(%r13)
	movq	%rax, %rdi
	je	.L110
	movq	16(%r13), %rcx
.L110:
	movslq	-52(%rbp), %rsi
	movslq	136(%r13), %rax
	addq	%rsi, %rax
	movq	%rsi, %r15
	leaq	(%rcx,%rax,2), %rsi
	movl	$1, %ecx
	call	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition@PLT
	addl	%eax, %r15d
	movl	%r15d, -52(%rbp)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L165:
	movq	16(%r13), %rax
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L166:
	movl	%r15d, %r8d
	subl	%edx, %r8d
	cmpl	16(%r12), %r8d
	jle	.L111
	cmpb	$0, 8(%r13)
	leaq	96(%r13), %rsi
	je	.L113
	movq	96(%r13), %rsi
.L113:
	movslq	%r15d, %rax
	movzbl	-1(%rsi,%rax), %eax
	cmpb	$32, %al
	sete	%dil
	cmpb	$38, %al
	sete	%al
	orb	%dil, %al
	je	.L111
	cmpb	$32, %r14b
	je	.L111
	cmpb	$38, %r14b
	je	.L111
	leal	-1(%r15), %r11d
	movslq	%r11d, %rcx
	cmpl	%r11d, %edx
	jg	.L115
.L119:
	movzbl	(%rsi,%rcx), %edi
	movl	%ecx, %r9d
	movl	%ecx, %r11d
	cmpb	$32, %dil
	je	.L118
	cmpb	$38, %dil
	je	.L118
.L115:
	subl	%edx, %r11d
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	leal	1(%r11), %ecx
	movb	%al, -52(%rbp)
	call	_ZN6icu_6724ConstrainedFieldPosition8setStateEiiii@PLT
	movzbl	-52(%rbp), %eax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L127:
	movl	%r14d, %edx
	movq	%r12, %rdi
	andl	$15, %edx
	call	_ZNK6icu_6724ConstrainedFieldPosition12matchesFieldEii@PLT
	movl	136(%r13), %edx
	testb	%al, %al
	je	.L163
	subl	%edx, %r15d
	movl	140(%r13), %esi
	movl	%r15d, -52(%rbp)
	movl	%ebx, %r15d
	movl	%r14d, %ebx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L164:
	addl	$1, %r15d
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L167:
	movl	-64(%rbp), %ebx
	movl	-60(%rbp), %edx
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	_ZNK6icu_6724ConstrainedFieldPosition12matchesFieldEii@PLT
	testb	%al, %al
	je	.L117
	movl	136(%r13), %edx
	cmpl	%r15d, %edx
	jge	.L117
	movl	%r15d, %r8d
	subl	%edx, %r8d
	cmpl	16(%r12), %r8d
	jg	.L120
	cmpl	%ebx, 20(%r12)
	jne	.L120
	movl	-60(%rbp), %eax
	cmpl	%eax, 8(%r12)
	je	.L117
	.p2align 4,,10
	.p2align 3
.L120:
	cmpb	$0, 8(%r13)
	leaq	96(%r13), %rsi
	je	.L122
	movq	96(%r13), %rsi
.L122:
	movslq	%r15d, %rax
	movzbl	-1(%rsi,%rax), %eax
	movl	%eax, %edi
	sarl	$4, %eax
	cmpl	$2, %eax
	sete	%r10b
	cmpb	$1, %dil
	sete	%al
	orb	%r10b, %al
	je	.L117
	movzbl	%r14b, %edi
	sarl	$4, %edi
	cmpl	$2, %edi
	je	.L117
	cmpb	$1, %r14b
	je	.L169
	leal	-1(%r15), %r11d
	cmpl	%r11d, %edx
	jg	.L124
	movslq	%r11d, %rcx
.L126:
	movzbl	(%rsi,%rcx), %edi
	movl	%ecx, %r10d
	movl	%ecx, %r11d
	movl	%edi, %r9d
	sarl	$4, %edi
	cmpl	$2, %edi
	je	.L125
	cmpb	$1, %r9b
	je	.L125
.L124:
	subl	%edx, %r11d
	movl	-64(%rbp), %esi
	movl	-60(%rbp), %edx
	movq	%r12, %rdi
	leal	1(%r11), %ecx
	movb	%al, -52(%rbp)
	call	_ZN6icu_6724ConstrainedFieldPosition8setStateEiiii@PLT
	movzbl	-52(%rbp), %eax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L118:
	subq	$1, %rcx
	leal	-1(%r9), %r11d
	cmpl	%ecx, %edx
	jle	.L119
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L125:
	subq	$1, %rcx
	leal	-1(%r10), %r11d
	cmpl	%ecx, %edx
	jle	.L126
	jmp	.L124
.L169:
	movl	140(%r13), %esi
	addl	$1, %r15d
	xorl	%ebx, %ebx
	jmp	.L103
	.cfi_endproc
.LFE3372:
	.size	_ZNK6icu_6731FormattedValueStringBuilderImpl16nextPositionImplERNS_24ConstrainedFieldPositionENS_22FormattedStringBuilder5FieldER10UErrorCode, .-_ZNK6icu_6731FormattedValueStringBuilderImpl16nextPositionImplERNS_24ConstrainedFieldPositionENS_22FormattedStringBuilder5FieldER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6731FormattedValueStringBuilderImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.type	_ZNK6icu_6731FormattedValueStringBuilderImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode, @function
_ZNK6icu_6731FormattedValueStringBuilderImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode:
.LFB3369:
	.cfi_startproc
	endbr64
	movq	%rdx, %rcx
	movzbl	144(%rdi), %edx
	jmp	_ZNK6icu_6731FormattedValueStringBuilderImpl16nextPositionImplERNS_24ConstrainedFieldPositionENS_22FormattedStringBuilder5FieldER10UErrorCode
	.cfi_endproc
.LFE3369:
	.size	_ZNK6icu_6731FormattedValueStringBuilderImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode, .-_ZNK6icu_6731FormattedValueStringBuilderImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6731FormattedValueStringBuilderImpl17nextFieldPositionERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6731FormattedValueStringBuilderImpl17nextFieldPositionERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6731FormattedValueStringBuilderImpl17nextFieldPositionERNS_13FieldPositionER10UErrorCode:
.LFB3370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rsi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %r13d
	je	.L171
	cmpl	$12, %r13d
	jbe	.L173
	movl	$1, (%rdx)
.L171:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	addq	$72, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	leaq	-96(%rbp), %r14
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%rdx, -104(%rbp)
	movq	%r14, %rdi
	call	_ZN6icu_6724ConstrainedFieldPositionC1Ev@PLT
	movl	%r13d, %edx
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6724ConstrainedFieldPosition14constrainFieldEii@PLT
	movl	12(%rbx), %ecx
	movl	16(%rbx), %r8d
	movl	%r13d, %edx
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6724ConstrainedFieldPosition8setStateEiiii@PLT
	movq	-104(%rbp), %r9
	movq	%r14, %rsi
	movq	%r12, %rdi
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %edx
	movq	%r9, %rcx
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl16nextPositionImplERNS_24ConstrainedFieldPositionENS_22FormattedStringBuilder5FieldER10UErrorCode
	testb	%al, %al
	jne	.L198
	movl	%eax, %r15d
	cmpl	$1, %r13d
	je	.L199
.L175:
	movq	%r14, %rdi
	call	_ZN6icu_6724ConstrainedFieldPositionD1Ev@PLT
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L199:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jne	.L175
	movl	136(%r12), %r10d
	movl	140(%r12), %r8d
	addl	%r10d, %r8d
	cmpl	%r8d, %r10d
	jge	.L188
	movzbl	8(%r12), %edi
	testb	%dil, %dil
	jne	.L177
	movslq	%r10d, %rax
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L189:
	movl	%edx, %edi
.L178:
	addq	$1, %rax
	leal	1(%r11), %esi
	cmpl	%eax, %r8d
	jle	.L180
.L181:
	movzbl	96(%r12,%rax), %ecx
	movl	%eax, %r11d
	movl	%eax, %esi
	movl	%ecx, %edx
	andl	$-3, %edx
	cmpb	$32, %dl
	sete	%dl
	cmpb	$38, %cl
	sete	%cl
	orb	%cl, %dl
	jne	.L189
	testb	%dil, %dil
	je	.L178
	.p2align 4,,10
	.p2align 3
.L180:
	movl	%esi, %edx
	subl	%r10d, %edx
.L176:
	movl	%edx, 12(%rbx)
	subl	136(%r12), %esi
	xorl	%r15d, %r15d
	movl	%esi, 16(%rbx)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L198:
	movq	-84(%rbp), %rax
	movq	%r14, %rdi
	movl	$1, %r15d
	movq	%rax, 12(%rbx)
	call	_ZN6icu_6724ConstrainedFieldPositionD1Ev@PLT
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L177:
	movq	96(%r12), %r13
	movslq	%r10d, %rdx
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L190:
	movl	%ecx, %eax
.L182:
	addq	$1, %rdx
	leal	1(%r11), %esi
	cmpl	%edx, %r8d
	jle	.L180
.L183:
	movzbl	0(%r13,%rdx), %edi
	movl	%edx, %r11d
	movl	%edx, %esi
	movl	%edi, %ecx
	andl	$-3, %ecx
	cmpb	$32, %cl
	sete	%cl
	cmpb	$38, %dil
	sete	%dil
	orb	%dil, %cl
	jne	.L190
	testb	%al, %al
	je	.L182
	jmp	.L180
.L188:
	movl	%r10d, %esi
	jmp	.L176
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3370:
	.size	_ZNK6icu_6731FormattedValueStringBuilderImpl17nextFieldPositionERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6731FormattedValueStringBuilderImpl17nextFieldPositionERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6731FormattedValueStringBuilderImpl20getAllFieldPositionsERNS_28FieldPositionIteratorHandlerER10UErrorCode
	.type	_ZNK6icu_6731FormattedValueStringBuilderImpl20getAllFieldPositionsERNS_28FieldPositionIteratorHandlerER10UErrorCode, @function
_ZNK6icu_6731FormattedValueStringBuilderImpl20getAllFieldPositionsERNS_28FieldPositionIteratorHandlerER10UErrorCode:
.LFB3371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6724ConstrainedFieldPositionC1Ev@PLT
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %r15d
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L205:
	movq	(%rbx), %rax
	movl	-80(%rbp), %ecx
	movq	%rbx, %rdi
	movl	-84(%rbp), %edx
	movl	-88(%rbp), %esi
	call	*16(%rax)
.L202:
	movq	%r13, %rcx
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl16nextPositionImplERNS_24ConstrainedFieldPositionENS_22FormattedStringBuilder5FieldER10UErrorCode
	testb	%al, %al
	jne	.L205
	movq	%r12, %rdi
	call	_ZN6icu_6724ConstrainedFieldPositionD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L206:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3371:
	.size	_ZNK6icu_6731FormattedValueStringBuilderImpl20getAllFieldPositionsERNS_28FieldPositionIteratorHandlerER10UErrorCode, .-_ZNK6icu_6731FormattedValueStringBuilderImpl20getAllFieldPositionsERNS_28FieldPositionIteratorHandlerER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6731FormattedValueStringBuilderImpl12isIntOrGroupENS_22FormattedStringBuilder5FieldE
	.type	_ZN6icu_6731FormattedValueStringBuilderImpl12isIntOrGroupENS_22FormattedStringBuilder5FieldE, @function
_ZN6icu_6731FormattedValueStringBuilderImpl12isIntOrGroupENS_22FormattedStringBuilder5FieldE:
.LFB3373:
	.cfi_startproc
	endbr64
	cmpb	$38, %dil
	sete	%al
	cmpb	$32, %dil
	sete	%dl
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE3373:
	.size	_ZN6icu_6731FormattedValueStringBuilderImpl12isIntOrGroupENS_22FormattedStringBuilder5FieldE, .-_ZN6icu_6731FormattedValueStringBuilderImpl12isIntOrGroupENS_22FormattedStringBuilder5FieldE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6731FormattedValueStringBuilderImpl8trimBackEi
	.type	_ZNK6icu_6731FormattedValueStringBuilderImpl8trimBackEi, @function
_ZNK6icu_6731FormattedValueStringBuilderImpl8trimBackEi:
.LFB3374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$1, %edi
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	cmpb	$0, 8(%rbx)
	movq	%rax, %rdi
	leaq	16(%rbx), %rax
	je	.L210
	movq	16(%rbx), %rax
.L210:
	movslq	136(%rbx), %rdx
	movl	$1, %ecx
	popq	%rbx
	leaq	(%rax,%rdx,2), %rsi
	movl	%r12d, %edx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6710UnicodeSet8spanBackEPKDsi17USetSpanCondition@PLT
	.cfi_endproc
.LFE3374:
	.size	_ZNK6icu_6731FormattedValueStringBuilderImpl8trimBackEi, .-_ZNK6icu_6731FormattedValueStringBuilderImpl8trimBackEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6731FormattedValueStringBuilderImpl9trimFrontEi
	.type	_ZNK6icu_6731FormattedValueStringBuilderImpl9trimFrontEi, @function
_ZNK6icu_6731FormattedValueStringBuilderImpl9trimFrontEi:
.LFB3375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$1, %edi
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movl	140(%rbx), %edx
	leaq	16(%rbx), %rcx
	movq	%rax, %rdi
	subl	%r12d, %edx
	cmpb	$0, 8(%rbx)
	je	.L215
	movq	16(%rbx), %rcx
.L215:
	movslq	136(%rbx), %rax
	movslq	%r12d, %rsi
	addq	%rsi, %rax
	leaq	(%rcx,%rax,2), %rsi
	movl	$1, %ecx
	call	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition@PLT
	popq	%rbx
	addl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3375:
	.size	_ZNK6icu_6731FormattedValueStringBuilderImpl9trimFrontEi, .-_ZNK6icu_6731FormattedValueStringBuilderImpl9trimFrontEi
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6731FormattedValueStringBuilderImplE
	.section	.rodata._ZTSN6icu_6731FormattedValueStringBuilderImplE,"aG",@progbits,_ZTSN6icu_6731FormattedValueStringBuilderImplE,comdat
	.align 32
	.type	_ZTSN6icu_6731FormattedValueStringBuilderImplE, @object
	.size	_ZTSN6icu_6731FormattedValueStringBuilderImplE, 43
_ZTSN6icu_6731FormattedValueStringBuilderImplE:
	.string	"N6icu_6731FormattedValueStringBuilderImplE"
	.weak	_ZTIN6icu_6731FormattedValueStringBuilderImplE
	.section	.data.rel.ro._ZTIN6icu_6731FormattedValueStringBuilderImplE,"awG",@progbits,_ZTIN6icu_6731FormattedValueStringBuilderImplE,comdat
	.align 8
	.type	_ZTIN6icu_6731FormattedValueStringBuilderImplE, @object
	.size	_ZTIN6icu_6731FormattedValueStringBuilderImplE, 56
_ZTIN6icu_6731FormattedValueStringBuilderImplE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_6731FormattedValueStringBuilderImplE
	.long	0
	.long	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.quad	_ZTIN6icu_6714FormattedValueE
	.quad	2
	.weak	_ZTVN6icu_6731FormattedValueStringBuilderImplE
	.section	.data.rel.ro.local._ZTVN6icu_6731FormattedValueStringBuilderImplE,"awG",@progbits,_ZTVN6icu_6731FormattedValueStringBuilderImplE,comdat
	.align 8
	.type	_ZTVN6icu_6731FormattedValueStringBuilderImplE, @object
	.size	_ZTVN6icu_6731FormattedValueStringBuilderImplE, 64
_ZTVN6icu_6731FormattedValueStringBuilderImplE:
	.quad	0
	.quad	_ZTIN6icu_6731FormattedValueStringBuilderImplE
	.quad	_ZN6icu_6731FormattedValueStringBuilderImplD1Ev
	.quad	_ZN6icu_6731FormattedValueStringBuilderImplD0Ev
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl8toStringER10UErrorCode
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl12toTempStringER10UErrorCode
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.section	.rodata
	.type	_ZN6icu_67L15kUndefinedFieldE, @object
	.size	_ZN6icu_67L15kUndefinedFieldE, 1
_ZN6icu_67L15kUndefinedFieldE:
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
