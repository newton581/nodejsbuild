	.file	"choicfmt.cpp"
	.text
	.section	.text._ZNK6icu_6712NumberFormat9isLenientEv,"axG",@progbits,_ZNK6icu_6712NumberFormat9isLenientEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6712NumberFormat9isLenientEv
	.type	_ZNK6icu_6712NumberFormat9isLenientEv, @function
_ZNK6icu_6712NumberFormat9isLenientEv:
.LFB2381:
	.cfi_startproc
	endbr64
	movzbl	341(%rdi), %eax
	ret
	.cfi_endproc
.LFE2381:
	.size	_ZNK6icu_6712NumberFormat9isLenientEv, .-_ZNK6icu_6712NumberFormat9isLenientEv
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712ChoiceFormat17getDynamicClassIDEv
	.type	_ZNK6icu_6712ChoiceFormat17getDynamicClassIDEv, @function
_ZNK6icu_6712ChoiceFormat17getDynamicClassIDEv:
.LFB2481:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712ChoiceFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2481:
	.size	_ZNK6icu_6712ChoiceFormat17getDynamicClassIDEv, .-_ZNK6icu_6712ChoiceFormat17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712ChoiceFormat9getLimitsERi
	.type	_ZNK6icu_6712ChoiceFormat9getLimitsERi, @function
_ZNK6icu_6712ChoiceFormat9getLimitsERi:
.LFB2510:
	.cfi_startproc
	endbr64
	movl	$0, (%rsi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2510:
	.size	_ZNK6icu_6712ChoiceFormat9getLimitsERi, .-_ZNK6icu_6712ChoiceFormat9getLimitsERi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712ChoiceFormat11getClosuresERi
	.type	_ZNK6icu_6712ChoiceFormat11getClosuresERi, @function
_ZNK6icu_6712ChoiceFormat11getClosuresERi:
.LFB2511:
	.cfi_startproc
	endbr64
	movl	$0, (%rsi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2511:
	.size	_ZNK6icu_6712ChoiceFormat11getClosuresERi, .-_ZNK6icu_6712ChoiceFormat11getClosuresERi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712ChoiceFormat10getFormatsERi
	.type	_ZNK6icu_6712ChoiceFormat10getFormatsERi, @function
_ZNK6icu_6712ChoiceFormat10getFormatsERi:
.LFB2512:
	.cfi_startproc
	endbr64
	movl	$0, (%rsi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2512:
	.size	_ZNK6icu_6712ChoiceFormat10getFormatsERi, .-_ZNK6icu_6712ChoiceFormat10getFormatsERi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712ChoiceFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712ChoiceFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode:
.LFB2504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	360(%rdi), %rdi
	call	_ZN6icu_6714MessagePattern16parseChoiceStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%r12), %eax
	movl	%eax, 356(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2504:
	.size	_ZN6icu_6712ChoiceFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712ChoiceFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6712ChoiceFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, @function
_ZN6icu_6712ChoiceFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode:
.LFB2505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	360(%rdi), %rdi
	call	_ZN6icu_6714MessagePattern16parseChoiceStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%r12), %eax
	movl	%eax, 356(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2505:
	.size	_ZN6icu_6712ChoiceFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, .-_ZN6icu_6712ChoiceFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712ChoiceFormat9toPatternERNS_13UnicodeStringE
	.type	_ZNK6icu_6712ChoiceFormat9toPatternERNS_13UnicodeStringE, @function
_ZNK6icu_6712ChoiceFormat9toPatternERNS_13UnicodeStringE:
.LFB2506:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	leaq	376(%r8), %rsi
	jmp	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	.cfi_endproc
.LFE2506:
	.size	_ZNK6icu_6712ChoiceFormat9toPatternERNS_13UnicodeStringE, .-_ZNK6icu_6712ChoiceFormat9toPatternERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712ChoiceFormat5cloneEv
	.type	_ZNK6icu_6712ChoiceFormat5cloneEv, @function
_ZNK6icu_6712ChoiceFormat5cloneEv:
.LFB2521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$488, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L12
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6712NumberFormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6712ChoiceFormatE(%rip), %rax
	leaq	360(%rbx), %rsi
	movq	%rax, (%r12)
	movl	356(%rbx), %eax
	leaq	360(%r12), %rdi
	movl	%eax, 356(%r12)
	call	_ZN6icu_6714MessagePatternC1ERKS0_@PLT
.L12:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2521:
	.size	_ZNK6icu_6712ChoiceFormat5cloneEv, .-_ZNK6icu_6712ChoiceFormat5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormat16getStaticClassIDEv
	.type	_ZN6icu_6712ChoiceFormat16getStaticClassIDEv, @function
_ZN6icu_6712ChoiceFormat16getStaticClassIDEv:
.LFB2480:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712ChoiceFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2480:
	.size	_ZN6icu_6712ChoiceFormat16getStaticClassIDEv, .-_ZN6icu_6712ChoiceFormat16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712ChoiceFormatC2ERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712ChoiceFormatC2ERKNS_13UnicodeStringER10UErrorCode:
.LFB2483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	360(%rbx), %r14
	call	_ZN6icu_6712NumberFormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712ChoiceFormatE(%rip), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, (%rbx)
	movl	(%r12), %eax
	movl	%eax, 356(%rbx)
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6714MessagePattern16parseChoiceStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%r12), %eax
	movl	%eax, 356(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2483:
	.size	_ZN6icu_6712ChoiceFormatC2ERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712ChoiceFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6712ChoiceFormatC1ERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6712ChoiceFormatC1ERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6712ChoiceFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormatC2ERKS0_
	.type	_ZN6icu_6712ChoiceFormatC2ERKS0_, @function
_ZN6icu_6712ChoiceFormatC2ERKS0_:
.LFB2492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6712NumberFormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6712ChoiceFormatE(%rip), %rax
	leaq	360(%r12), %rsi
	movq	%rax, (%rbx)
	movl	356(%r12), %eax
	leaq	360(%rbx), %rdi
	movl	%eax, 356(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714MessagePatternC1ERKS0_@PLT
	.cfi_endproc
.LFE2492:
	.size	_ZN6icu_6712ChoiceFormatC2ERKS0_, .-_ZN6icu_6712ChoiceFormatC2ERKS0_
	.globl	_ZN6icu_6712ChoiceFormatC1ERKS0_
	.set	_ZN6icu_6712ChoiceFormatC1ERKS0_,_ZN6icu_6712ChoiceFormatC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormatC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6712ChoiceFormatC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, @function
_ZN6icu_6712ChoiceFormatC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode:
.LFB2495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	360(%rbx), %r15
	subq	$8, %rsp
	call	_ZN6icu_6712NumberFormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712ChoiceFormatE(%rip), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, (%rbx)
	movl	(%r12), %eax
	movl	%eax, 356(%rbx)
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6714MessagePattern16parseChoiceStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%r12), %eax
	movl	%eax, 356(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2495:
	.size	_ZN6icu_6712ChoiceFormatC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, .-_ZN6icu_6712ChoiceFormatC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.globl	_ZN6icu_6712ChoiceFormatC1ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.set	_ZN6icu_6712ChoiceFormatC1ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode,_ZN6icu_6712ChoiceFormatC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormataSERKS0_
	.type	_ZN6icu_6712ChoiceFormataSERKS0_, @function
_ZN6icu_6712ChoiceFormataSERKS0_:
.LFB2498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L26
	movq	%rsi, %rbx
	call	_ZN6icu_6712NumberFormataSERKS0_@PLT
	movl	356(%rbx), %eax
	leaq	360(%rbx), %rsi
	leaq	360(%r12), %rdi
	movl	%eax, 356(%r12)
	call	_ZN6icu_6714MessagePatternaSERKS0_@PLT
.L26:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2498:
	.size	_ZN6icu_6712ChoiceFormataSERKS0_, .-_ZN6icu_6712ChoiceFormataSERKS0_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%.*g"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormat4dtosEdRNS_13UnicodeStringE
	.type	_ZN6icu_6712ChoiceFormat4dtosEdRNS_13UnicodeStringE, @function
_ZN6icu_6712ChoiceFormat4dtosEdRNS_13UnicodeStringE:
.LFB2503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$31, %edx
	movl	$15, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	call	__sprintf_chk@PLT
	movzbl	-80(%rbp), %edx
	testb	%dl, %dl
	je	.L29
	movq	%r14, %rax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L42:
	movzbl	1(%rax), %edx
	addq	$1, %rax
	testb	%dl, %dl
	je	.L29
.L32:
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	jbe	.L42
	cmpb	$45, %dl
	je	.L42
	cmpb	$101, %dl
	je	.L33
	movb	$46, (%rax)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L70:
	cmpb	$101, %dl
	je	.L66
.L69:
	addq	$1, %rax
.L33:
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L70
	cmpb	$101, %dl
	je	.L66
.L29:
	leaq	-144(%rbp), %r13
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movzbl	1(%rax), %edx
	leal	-43(%rdx), %ecx
	andl	$253, %ecx
	leaq	1(%rax), %rcx
	je	.L72
.L38:
	cmpb	$48, %dl
	jne	.L29
	movq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L39:
	movzbl	1(%rax), %edx
	addq	$1, %rax
	cmpb	$48, %dl
	je	.L39
	cmpq	%rax, %rcx
	je	.L29
	testb	%dl, %dl
	je	.L29
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$1, %rax
	movb	%dl, (%rcx)
	addq	$1, %rcx
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L40
	movb	$0, (%rcx)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L72:
	movzbl	2(%rax), %edx
	leaq	2(%rax), %rcx
	jmp	.L38
.L71:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2503:
	.size	_ZN6icu_6712ChoiceFormat4dtosEdRNS_13UnicodeStringE, .-_ZN6icu_6712ChoiceFormat4dtosEdRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.type	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode.part.0, @function
_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode.part.0:
.LFB3184:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -264(%rbp)
	movq	%rsi, -232(%rbp)
	movq	%rdx, -248(%rbp)
	movq	%r9, -272(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -192(%rbp)
	movl	$2, %eax
	movw	%ax, -184(%rbp)
	testl	%r8d, %r8d
	jle	.L74
	leal	-1(%r8), %eax
	leaq	10(%rcx), %rbx
	movq	$0, -224(%rbp)
	movq	-224(%rbp), %rsi
	movq	%rax, -256(%rbp)
	leaq	-128(%rbp), %rax
	leaq	-194(%rbp), %r12
	movq	%rax, -240(%rbp)
	leaq	-192(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r11d
	movq	%rax, -128(%rbp)
	movq	-232(%rbp), %rax
	movw	%r11w, -120(%rbp)
	movsd	(%rax,%rsi,8), %xmm0
	call	uprv_isPositiveInfinity_67@PLT
	testb	%al, %al
	jne	.L128
	movq	-232(%rbp), %rax
	movq	-224(%rbp), %rsi
	movsd	(%rax,%rsi,8), %xmm0
	call	uprv_isNegativeInfinity_67@PLT
	testb	%al, %al
	je	.L80
	movl	$45, %r10d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movw	%r10w, -194(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L128:
	movl	$8734, %r9d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movw	%r9w, -194(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L79:
	movq	-248(%rbp), %rax
	testq	%rax, %rax
	je	.L83
	movq	-224(%rbp), %rcx
	cmpb	$0, (%rax,%rcx)
	jne	.L130
.L83:
	movl	$35, %edi
	movw	%di, -194(%rbp)
.L129:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	-2(%rbx), %eax
	testw	%ax, %ax
	js	.L85
	movswl	%ax, %edx
	sarl	$5, %edx
.L86:
	testl	%edx, %edx
	jle	.L87
	leal	-1(%rdx), %esi
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movq	%rsi, -216(%rbp)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L131:
	movswl	%ax, %edx
	sarl	$5, %edx
	cmpl	%r14d, %edx
	jbe	.L102
.L132:
	movq	%rbx, %rdx
	testb	$2, %al
	jne	.L91
	movq	14(%rbx), %rdx
.L91:
	movzwl	(%rdx,%r14,2), %eax
	testl	%r15d, %r15d
	sete	%dl
	cmpw	$39, %ax
	jne	.L92
	testb	%dl, %dl
	je	.L92
	movl	$39, %esi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%si, -194(%rbp)
	movq	%r12, %rsi
	xorl	%r15d, %r15d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$39, %eax
.L90:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movw	%ax, -194(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	1(%r14), %rdx
	cmpq	-216(%rbp), %r14
	je	.L87
.L135:
	movzwl	-2(%rbx), %eax
	movq	%rdx, %r14
.L98:
	testw	%ax, %ax
	jns	.L131
	movl	2(%rbx), %edx
	cmpl	%r14d, %edx
	ja	.L132
.L102:
	movl	$-1, %eax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L92:
	cmpw	$124, %ax
	jne	.L93
	testb	%dl, %dl
	jne	.L133
.L93:
	cmpw	$123, %ax
	jne	.L134
	addl	$1, %r15d
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L134:
	cmpw	$125, %ax
	jne	.L90
	testl	%r15d, %r15d
	setg	%dl
	cmpb	$1, %dl
	adcl	$-1, %r15d
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L133:
	movl	$39, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movw	%ax, -194(%rbp)
	xorl	%r15d, %r15d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$124, %edx
	movl	$1, %ecx
	movq	%r12, %rsi
	movw	%dx, -194(%rbp)
	movq	%rax, %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$39, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movw	%cx, -194(%rbp)
	movq	%rax, %rdi
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	1(%r14), %rdx
	cmpq	-216(%rbp), %r14
	jne	.L135
	.p2align 4,,10
	.p2align 3
.L87:
	movq	-240(%rbp), %rdi
	addq	$64, %rbx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-224(%rbp), %rax
	leaq	1(%rax), %r14
	cmpq	%rax, -256(%rbp)
	je	.L74
	movq	%r12, %rsi
	movl	$124, %r15d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%r15w, -194(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%r14, -224(%rbp)
	movq	%r14, %rsi
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L85:
	movl	2(%rbx), %edx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L130:
	movl	$60, %r8d
	movw	%r8w, -194(%rbp)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L80:
	movq	-232(%rbp), %rax
	movq	-224(%rbp), %rcx
	movq	-240(%rbp), %rdi
	movsd	(%rax,%rcx,8), %xmm0
	call	_ZN6icu_6712ChoiceFormat4dtosEdRNS_13UnicodeStringE
	movzwl	8(%rax), %edx
	movq	%rax, %rsi
	testw	%dx, %dx
	js	.L81
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L82:
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L74:
	movq	-264(%rbp), %rax
	leaq	_ZN6icu_6712ChoiceFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode(%rip), %rdx
	movq	(%rax), %rax
	movq	296(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L136
	movq	-264(%rbp), %rbx
	movq	-272(%rbp), %r15
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	360(%rbx), %rdi
	movq	%r15, %rcx
	call	_ZN6icu_6714MessagePattern16parseChoiceStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%r15), %eax
	movl	%eax, 356(%rbx)
.L100:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L137
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movl	12(%rax), %ecx
	jmp	.L82
.L136:
	movq	-272(%rbp), %rdx
	movq	-264(%rbp), %rdi
	movq	%r13, %rsi
	call	*%rax
	jmp	.L100
.L137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3184:
	.size	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode.part.0, .-_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode
	.type	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode, @function
_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode:
.LFB2509:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L138
	testq	%rsi, %rsi
	je	.L142
	testq	%rcx, %rcx
	je	.L142
	jmp	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L138:
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$1, (%r9)
	ret
	.cfi_endproc
.LFE2509:
	.size	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode, .-_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEi
	.type	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEi, @function
_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEi:
.LFB2508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode(%rip), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -12(%rbp)
	movq	368(%rax), %rax
	cmpq	%r9, %rax
	jne	.L144
	testq	%rsi, %rsi
	je	.L143
	testq	%rcx, %rcx
	je	.L143
	leaq	-12(%rbp), %r9
	call	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode.part.0
.L143:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L148
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	leaq	-12(%rbp), %r9
	call	*%rax
	jmp	.L143
.L148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2508:
	.size	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEi, .-_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKNS_13UnicodeStringEi
	.type	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKNS_13UnicodeStringEi, @function
_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKNS_13UnicodeStringEi:
.LFB2507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r8d
	leaq	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -12(%rbp)
	movq	368(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L150
	testq	%rsi, %rsi
	je	.L149
	testq	%rdx, %rdx
	je	.L149
	movq	%rdx, %rcx
	leaq	-12(%rbp), %r9
	xorl	%edx, %edx
	call	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode.part.0
.L149:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	movq	%rdx, %rcx
	leaq	-12(%rbp), %r9
	xorl	%edx, %edx
	call	*%rax
	jmp	.L149
.L154:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2507:
	.size	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKNS_13UnicodeStringEi, .-_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKNS_13UnicodeStringEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormatC2EPKdPKNS_13UnicodeStringEi
	.type	_ZN6icu_6712ChoiceFormatC2EPKdPKNS_13UnicodeStringEi, @function
_ZN6icu_6712ChoiceFormatC2EPKdPKNS_13UnicodeStringEi:
.LFB2486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	leaq	356(%r12), %r15
	subq	$8, %rsp
	call	_ZN6icu_6712NumberFormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712ChoiceFormatE(%rip), %rax
	leaq	360(%r12), %rdi
	movq	%r15, %rsi
	movq	%rax, (%r12)
	movl	$0, 356(%r12)
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	movl	356(%r12), %eax
	testl	%eax, %eax
	jg	.L155
	testq	%r13, %r13
	je	.L159
	testq	%r14, %r14
	je	.L159
	addq	$8, %rsp
	movq	%r15, %r9
	movl	%ebx, %r8d
	movq	%r14, %rcx
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movl	$1, 356(%r12)
.L155:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2486:
	.size	_ZN6icu_6712ChoiceFormatC2EPKdPKNS_13UnicodeStringEi, .-_ZN6icu_6712ChoiceFormatC2EPKdPKNS_13UnicodeStringEi
	.globl	_ZN6icu_6712ChoiceFormatC1EPKdPKNS_13UnicodeStringEi
	.set	_ZN6icu_6712ChoiceFormatC1EPKdPKNS_13UnicodeStringEi,_ZN6icu_6712ChoiceFormatC2EPKdPKNS_13UnicodeStringEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormatC2EPKdPKaPKNS_13UnicodeStringEi
	.type	_ZN6icu_6712ChoiceFormatC2EPKdPKaPKNS_13UnicodeStringEi, @function
_ZN6icu_6712ChoiceFormatC2EPKdPKaPKNS_13UnicodeStringEi:
.LFB2489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	356(%r12), %rbx
	subq	$24, %rsp
	movl	%r8d, -52(%rbp)
	call	_ZN6icu_6712NumberFormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712ChoiceFormatE(%rip), %rax
	leaq	360(%r12), %rdi
	movq	%rbx, %rsi
	movq	%rax, (%r12)
	movl	$0, 356(%r12)
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	movl	356(%r12), %eax
	testl	%eax, %eax
	jg	.L161
	testq	%r13, %r13
	je	.L165
	testq	%r14, %r14
	je	.L165
	movl	-52(%rbp), %r8d
	addq	$24, %rsp
	movq	%rbx, %r9
	movq	%r14, %rcx
	popq	%rbx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movl	$1, 356(%r12)
.L161:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2489:
	.size	_ZN6icu_6712ChoiceFormatC2EPKdPKaPKNS_13UnicodeStringEi, .-_ZN6icu_6712ChoiceFormatC2EPKdPKaPKNS_13UnicodeStringEi
	.globl	_ZN6icu_6712ChoiceFormatC1EPKdPKaPKNS_13UnicodeStringEi
	.set	_ZN6icu_6712ChoiceFormatC1EPKdPKaPKNS_13UnicodeStringEi,_ZN6icu_6712ChoiceFormatC2EPKdPKaPKNS_13UnicodeStringEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormat14findSubMessageERKNS_14MessagePatternEid
	.type	_ZN6icu_6712ChoiceFormat14findSubMessageERKNS_14MessagePatternEid, @function
_ZN6icu_6712ChoiceFormat14findSubMessageERKNS_14MessagePatternEid:
.LFB2516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	26(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leal	2(%rsi), %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	96(%rdi), %r12d
	movq	88(%rdi), %rsi
	movq	%rax, -64(%rbp)
	movsd	%xmm0, -56(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L179:
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L171:
	cmpl	%eax, %ecx
	jbe	.L172
	andl	$2, %edx
	je	.L173
	movq	-64(%rbp), %rdx
	cmpw	$60, (%rdx,%rax,2)
	je	.L178
.L172:
	movsd	-56(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	setb	%al
	testb	%al, %al
	jne	.L167
.L180:
	movl	%r14d, %r15d
.L176:
	movslq	%r15d, %rax
	movl	%r15d, %r14d
	salq	$4, %rax
	addq	%rsi, %rax
	cmpl	%r15d, 12(%rax)
	cmovge	12(%rax), %r14d
	leal	1(%r14), %eax
	cmpl	%r12d, %eax
	jge	.L167
	movslq	%eax, %rbx
	salq	$4, %rbx
	addq	%rbx, %rsi
	cmpl	$6, (%rsi)
	je	.L167
	movq	%r13, %rdi
	addl	$3, %r14d
	call	_ZNK6icu_6714MessagePattern15getNumericValueERKNS0_4PartE@PLT
	movq	88(%r13), %rsi
	movzwl	24(%r13), %edx
	movslq	20(%rsi,%rbx), %rax
	testw	%dx, %dx
	jns	.L179
	movl	28(%r13), %ecx
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L173:
	movq	40(%r13), %rdx
	cmpw	$60, (%rdx,%rax,2)
	jne	.L172
.L178:
	movsd	-56(%rbp), %xmm2
	comisd	%xmm0, %xmm2
	setbe	%al
	testb	%al, %al
	je	.L180
.L167:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2516:
	.size	_ZN6icu_6712ChoiceFormat14findSubMessageERKNS_14MessagePatternEid, .-_ZN6icu_6712ChoiceFormat14findSubMessageERKNS_14MessagePatternEid
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712ChoiceFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE
	.type	_ZNK6icu_6712ChoiceFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE, @function
_ZNK6icu_6712ChoiceFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE:
.LFB2515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	456(%rdi), %eax
	testl	%eax, %eax
	jne	.L185
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	leaq	360(%rdi), %r13
	xorl	%esi, %esi
	movq	%rdi, %rbx
	movq	%r13, %rdi
	call	_ZN6icu_6712ChoiceFormat14findSubMessageERKNS_14MessagePatternEid
	cmpl	$1, 368(%rbx)
	movl	%eax, %esi
	je	.L183
	movq	448(%rbx), %rcx
	cltq
	movq	%r12, %rdi
	salq	$4, %rax
	addq	%rcx, %rax
	movzwl	8(%rax), %edx
	addl	4(%rax), %edx
	cmpl	%esi, 12(%rax)
	cmovge	12(%rax), %esi
	movslq	%esi, %rsi
	salq	$4, %rsi
	movl	4(%rcx,%rsi), %ecx
	leaq	376(%rbx), %rsi
	subl	%edx, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdx
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711MessageImpl33appendSubMessageWithoutSkipSyntaxERKNS_14MessagePatternEiRNS_13UnicodeStringE@PLT
	.cfi_endproc
.LFE2515:
	.size	_ZNK6icu_6712ChoiceFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE, .-_ZNK6icu_6712ChoiceFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712ChoiceFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE
	.type	_ZNK6icu_6712ChoiceFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE, @function
_ZNK6icu_6712ChoiceFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE:
.LFB2513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	leaq	_ZNK6icu_6712ChoiceFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE(%rip), %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L187
	movl	456(%rdi), %eax
	testl	%eax, %eax
	jne	.L191
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	leaq	360(%rdi), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6712ChoiceFormat14findSubMessageERKNS_14MessagePatternEid
	cmpl	$1, 368(%r12)
	movl	%eax, %esi
	je	.L189
	movq	448(%r12), %rcx
	cltq
	movq	%r13, %rdi
	salq	$4, %rax
	addq	%rcx, %rax
	movzwl	8(%rax), %edx
	addl	4(%rax), %edx
	cmpl	%esi, 12(%rax)
	cmovge	12(%rax), %esi
	movslq	%esi, %rsi
	salq	$4, %rsi
	movl	4(%rcx,%rsi), %ecx
	leaq	376(%r12), %rsi
	subl	%edx, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%rcx, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711MessageImpl33appendSubMessageWithoutSkipSyntaxERKNS_14MessagePatternEiRNS_13UnicodeStringE@PLT
	.cfi_endproc
.LFE2513:
	.size	_ZNK6icu_6712ChoiceFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE, .-_ZNK6icu_6712ChoiceFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712ChoiceFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionE
	.type	_ZNK6icu_6712ChoiceFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionE, @function
_ZNK6icu_6712ChoiceFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionE:
.LFB2514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	leaq	_ZNK6icu_6712ChoiceFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE(%rip), %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L193
	movl	456(%rdi), %eax
	testl	%eax, %eax
	jne	.L197
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	leaq	360(%rdi), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6712ChoiceFormat14findSubMessageERKNS_14MessagePatternEid
	cmpl	$1, 368(%r12)
	movl	%eax, %esi
	je	.L195
	movq	448(%r12), %rcx
	cltq
	movq	%r13, %rdi
	salq	$4, %rax
	addq	%rcx, %rax
	movzwl	8(%rax), %edx
	addl	4(%rax), %edx
	cmpl	%esi, 12(%rax)
	cmovge	12(%rax), %esi
	movslq	%esi, %rsi
	salq	$4, %rsi
	movl	4(%rcx,%rsi), %ecx
	leaq	376(%r12), %rsi
	subl	%edx, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%rcx, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711MessageImpl33appendSubMessageWithoutSkipSyntaxERKNS_14MessagePatternEiRNS_13UnicodeStringE@PLT
	.cfi_endproc
.LFE2514:
	.size	_ZNK6icu_6712ChoiceFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionE, .-_ZNK6icu_6712ChoiceFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712ChoiceFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6712ChoiceFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6712ChoiceFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB2517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -56(%rbp)
	movq	%r8, -64(%rbp)
	testl	%edx, %edx
	js	.L212
	movl	456(%rdi), %ecx
	movq	%rdi, %r12
	testl	%ecx, %ecx
	je	.L213
	movl	%edx, %r13d
	testl	%edx, %edx
	je	.L200
	movq	%rsi, %r15
	xorl	%ebx, %ebx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L203:
	addl	$1, %ebx
	addq	$112, %r15
	cmpl	%ebx, %r13d
	jle	.L200
.L206:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L203
	movq	(%r12), %rax
	leaq	_ZNK6icu_6712ChoiceFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE(%rip), %rcx
	movq	64(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L204
	movl	456(%r12), %eax
	testl	%eax, %eax
	je	.L203
	leaq	360(%r12), %rdi
	xorl	%esi, %esi
	movq	%rdi, -72(%rbp)
	call	_ZN6icu_6712ChoiceFormat14findSubMessageERKNS_14MessagePatternEid
	movq	-72(%rbp), %rdi
	cmpl	$1, 368(%r12)
	movl	%eax, %esi
	je	.L205
	movq	448(%r12), %rcx
	cltq
	movq	-56(%rbp), %rdi
	salq	$4, %rax
	addq	%rcx, %rax
	movzwl	8(%rax), %edx
	addl	4(%rax), %edx
	cmpl	%esi, 12(%rax)
	cmovge	12(%rax), %esi
	movslq	%esi, %rsi
	salq	$4, %rsi
	movl	4(%rcx,%rsi), %ecx
	leaq	376(%r12), %rsi
	subl	%edx, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L213:
	movl	$27, (%r9)
.L200:
	movq	-56(%rbp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L205:
	movq	-56(%rbp), %rdx
	call	_ZN6icu_6711MessageImpl33appendSubMessageWithoutSkipSyntaxERKNS_14MessagePatternEiRNS_13UnicodeStringE@PLT
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L212:
	movl	$1, (%r9)
	jmp	.L200
	.cfi_endproc
.LFE2517:
	.size	_ZNK6icu_6712ChoiceFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6712ChoiceFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormat25matchStringUntilLimitPartERKNS_14MessagePatternEiiRKNS_13UnicodeStringEi
	.type	_ZN6icu_6712ChoiceFormat25matchStringUntilLimitPartERKNS_14MessagePatternEiiRKNS_13UnicodeStringEi, @function
_ZN6icu_6712ChoiceFormat25matchStringUntilLimitPartERKNS_14MessagePatternEiiRKNS_13UnicodeStringEi:
.LFB2520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rcx, %rdi
	xorl	%r11d, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movslq	%esi, %r12
	movl	%r8d, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r12, %rbx
	salq	$4, %r12
	subq	$40, %rsp
	movq	88(%r10), %rcx
	movzwl	8(%rcx,%r12), %eax
	addl	4(%rcx,%r12), %eax
	addq	$16, %r12
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L218:
	addq	$16, %r12
.L215:
	addl	$1, %ebx
	leaq	(%rcx,%r12), %r13
	cmpl	%r15d, %ebx
	je	.L216
	cmpl	$2, 0(%r13)
	jne	.L218
	movl	4(%r13), %edx
	movl	%edx, %r14d
	subl	%eax, %r14d
	jne	.L228
.L229:
	movzwl	8(%r13), %eax
	addl	%edx, %eax
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L216:
	movl	4(%r13), %r14d
	subl	%eax, %r14d
	je	.L214
.L228:
	movzwl	24(%r10), %edx
	testb	$1, %dl
	je	.L219
	movzbl	8(%rdi), %eax
	notl	%eax
	andl	$1, %eax
	testb	%al, %al
	jne	.L232
.L235:
	addl	%r14d, %r11d
	cmpl	%r15d, %ebx
	je	.L214
	movl	4(%r13), %edx
	movq	88(%r10), %rcx
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L219:
	testw	%dx, %dx
	js	.L221
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L222:
	xorl	%r8d, %r8d
	testl	%eax, %eax
	js	.L223
	cmpl	%ecx, %eax
	cmovg	%ecx, %eax
	movl	%eax, %r8d
.L223:
	xorl	%r9d, %r9d
	testl	%r14d, %r14d
	js	.L224
	subl	%r8d, %ecx
	cmpl	%r14d, %ecx
	cmovg	%r14d, %ecx
	movl	%ecx, %r9d
.L224:
	andl	$2, %edx
	leaq	26(%r10), %rcx
	jne	.L226
	movq	40(%r10), %rcx
.L226:
	movl	%r14d, %edx
	movq	%r10, -72(%rbp)
	movl	%r11d, -64(%rbp)
	movl	%esi, -60(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movq	-72(%rbp), %r10
	movl	-64(%rbp), %r11d
	movl	-60(%rbp), %esi
	movq	-56(%rbp), %rdi
	testb	%al, %al
	je	.L235
.L232:
	movl	$-1, %r11d
.L214:
	addq	$40, %rsp
	movl	%r11d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movl	28(%r10), %ecx
	jmp	.L222
	.cfi_endproc
.LFE2520:
	.size	_ZN6icu_6712ChoiceFormat25matchStringUntilLimitPartERKNS_14MessagePatternEiiRKNS_13UnicodeStringEi, .-_ZN6icu_6712ChoiceFormat25matchStringUntilLimitPartERKNS_14MessagePatternEiiRKNS_13UnicodeStringEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormat13parseArgumentERKNS_14MessagePatternEiRKNS_13UnicodeStringERNS_13ParsePositionE
	.type	_ZN6icu_6712ChoiceFormat13parseArgumentERKNS_14MessagePatternEiRKNS_13UnicodeStringERNS_13ParsePositionE, @function
_ZN6icu_6712ChoiceFormat13parseArgumentERKNS_14MessagePatternEiRKNS_13UnicodeStringERNS_13ParsePositionE:
.LFB2519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$40, %rsp
	movq	%rcx, -80(%rbp)
	movl	8(%rcx), %r13d
	call	uprv_getNaN_67@PLT
	movl	96(%r15), %eax
	movsd	%xmm0, -72(%rbp)
	movl	%eax, -56(%rbp)
	cmpl	%eax, %ebx
	jge	.L237
	movl	%r13d, -52(%rbp)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L248:
	sarl	$5, %r12d
	movl	%r12d, -52(%rbp)
	cmpl	-52(%rbp), %eax
	je	.L245
.L249:
	movsd	-64(%rbp), %xmm1
	movl	%eax, -52(%rbp)
	movsd	%xmm1, -72(%rbp)
.L239:
	addl	$1, %ebx
	cmpl	-56(%rbp), %ebx
	jge	.L238
.L242:
	movq	88(%r15), %rsi
	movslq	%ebx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rsi
	movq	%rdx, %r12
	cmpl	$6, (%rsi)
	je	.L238
	movq	%r15, %rdi
	call	_ZNK6icu_6714MessagePattern15getNumericValueERKNS0_4PartE@PLT
	movq	88(%r15), %rax
	leal	2(%rbx), %esi
	movl	%r13d, %r8d
	movl	%esi, %ebx
	movq	%r14, %rcx
	movq	%r15, %rdi
	movsd	%xmm0, -64(%rbp)
	cmpl	%esi, 44(%rax,%r12)
	cmovge	44(%rax,%r12), %ebx
	movl	%ebx, %edx
	call	_ZN6icu_6712ChoiceFormat25matchStringUntilLimitPartERKNS_14MessagePatternEiiRKNS_13UnicodeStringEi
	testl	%eax, %eax
	js	.L239
	addl	%r13d, %eax
	cmpl	-52(%rbp), %eax
	jle	.L239
	movswl	8(%r14), %r12d
	testw	%r12w, %r12w
	jns	.L248
	movl	12(%r14), %ecx
	movl	%ecx, -52(%rbp)
	cmpl	-52(%rbp), %eax
	jne	.L249
.L245:
	movsd	-64(%rbp), %xmm2
	movsd	%xmm2, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L238:
	cmpl	%r13d, -52(%rbp)
	je	.L237
	movq	-80(%rbp), %rax
	movl	-52(%rbp), %ecx
	movl	%ecx, 8(%rax)
.L236:
	movsd	-72(%rbp), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movq	-80(%rbp), %rax
	movl	%r13d, 12(%rax)
	jmp	.L236
	.cfi_endproc
.LFE2519:
	.size	_ZN6icu_6712ChoiceFormat13parseArgumentERKNS_14MessagePatternEiRKNS_13UnicodeStringERNS_13ParsePositionE, .-_ZN6icu_6712ChoiceFormat13parseArgumentERKNS_14MessagePatternEiRKNS_13UnicodeStringERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712ChoiceFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.type	_ZNK6icu_6712ChoiceFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, @function
_ZNK6icu_6712ChoiceFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE:
.LFB2518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$360, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	movq	%rsi, %rdx
	xorl	%esi, %esi
	subq	$8, %rsp
	call	_ZN6icu_6712ChoiceFormat13parseArgumentERKNS_14MessagePatternEiRKNS_13UnicodeStringERNS_13ParsePositionE
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711Formattable9setDoubleEd@PLT
	.cfi_endproc
.LFE2518:
	.size	_ZNK6icu_6712ChoiceFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, .-_ZNK6icu_6712ChoiceFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712ChoiceFormateqERKNS_6FormatE
	.type	_ZNK6icu_6712ChoiceFormateqERKNS_6FormatE, @function
_ZNK6icu_6712ChoiceFormateqERKNS_6FormatE:
.LFB2497:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L254
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZNK6icu_6712NumberFormateqERKNS_6FormatE@PLT
	testb	%al, %al
	jne	.L262
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	leaq	360(%r12), %rsi
	leaq	360(%rbx), %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6714MessagePatterneqERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L254:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2497:
	.size	_ZNK6icu_6712ChoiceFormateqERKNS_6FormatE, .-_ZNK6icu_6712ChoiceFormateqERKNS_6FormatE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormatD2Ev
	.type	_ZN6icu_6712ChoiceFormatD2Ev, @function
_ZN6icu_6712ChoiceFormatD2Ev:
.LFB2500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712ChoiceFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	360(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -360(%rdi)
	call	_ZN6icu_6714MessagePatternD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712NumberFormatD2Ev@PLT
	.cfi_endproc
.LFE2500:
	.size	_ZN6icu_6712ChoiceFormatD2Ev, .-_ZN6icu_6712ChoiceFormatD2Ev
	.globl	_ZN6icu_6712ChoiceFormatD1Ev
	.set	_ZN6icu_6712ChoiceFormatD1Ev,_ZN6icu_6712ChoiceFormatD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ChoiceFormatD0Ev
	.type	_ZN6icu_6712ChoiceFormatD0Ev, @function
_ZN6icu_6712ChoiceFormatD0Ev:
.LFB2502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712ChoiceFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	360(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -360(%rdi)
	call	_ZN6icu_6714MessagePatternD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6712NumberFormatD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2502:
	.size	_ZN6icu_6712ChoiceFormatD0Ev, .-_ZN6icu_6712ChoiceFormatD0Ev
	.weak	_ZTSN6icu_6712ChoiceFormatE
	.section	.rodata._ZTSN6icu_6712ChoiceFormatE,"aG",@progbits,_ZTSN6icu_6712ChoiceFormatE,comdat
	.align 16
	.type	_ZTSN6icu_6712ChoiceFormatE, @object
	.size	_ZTSN6icu_6712ChoiceFormatE, 24
_ZTSN6icu_6712ChoiceFormatE:
	.string	"N6icu_6712ChoiceFormatE"
	.weak	_ZTIN6icu_6712ChoiceFormatE
	.section	.data.rel.ro._ZTIN6icu_6712ChoiceFormatE,"awG",@progbits,_ZTIN6icu_6712ChoiceFormatE,comdat
	.align 8
	.type	_ZTIN6icu_6712ChoiceFormatE, @object
	.size	_ZTIN6icu_6712ChoiceFormatE, 24
_ZTIN6icu_6712ChoiceFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712ChoiceFormatE
	.quad	_ZTIN6icu_6712NumberFormatE
	.weak	_ZTVN6icu_6712ChoiceFormatE
	.section	.data.rel.ro._ZTVN6icu_6712ChoiceFormatE,"awG",@progbits,_ZTVN6icu_6712ChoiceFormatE,comdat
	.align 8
	.type	_ZTVN6icu_6712ChoiceFormatE, @object
	.size	_ZTVN6icu_6712ChoiceFormatE, 392
_ZTVN6icu_6712ChoiceFormatE:
	.quad	0
	.quad	_ZTIN6icu_6712ChoiceFormatE
	.quad	_ZN6icu_6712ChoiceFormatD1Ev
	.quad	_ZN6icu_6712ChoiceFormatD0Ev
	.quad	_ZNK6icu_6712ChoiceFormat17getDynamicClassIDEv
	.quad	_ZNK6icu_6712ChoiceFormateqERKNS_6FormatE
	.quad	_ZNK6icu_6712ChoiceFormat5cloneEv
	.quad	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.quad	_ZNK6icu_6712ChoiceFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE
	.quad	_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712ChoiceFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionE
	.quad	_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712ChoiceFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE
	.quad	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatENS_11StringPieceERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6712ChoiceFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.quad	_ZNK6icu_6712NumberFormat5parseERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE
	.quad	_ZN6icu_6712NumberFormat19setParseIntegerOnlyEa
	.quad	_ZN6icu_6712NumberFormat10setLenientEa
	.quad	_ZNK6icu_6712NumberFormat9isLenientEv
	.quad	_ZN6icu_6712NumberFormat15setGroupingUsedEa
	.quad	_ZN6icu_6712NumberFormat23setMaximumIntegerDigitsEi
	.quad	_ZN6icu_6712NumberFormat23setMinimumIntegerDigitsEi
	.quad	_ZN6icu_6712NumberFormat24setMaximumFractionDigitsEi
	.quad	_ZN6icu_6712NumberFormat24setMinimumFractionDigitsEi
	.quad	_ZN6icu_6712NumberFormat11setCurrencyEPKDsR10UErrorCode
	.quad	_ZN6icu_6712NumberFormat10setContextE15UDisplayContextR10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat10getContextE19UDisplayContextTypeR10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat15getRoundingModeEv
	.quad	_ZN6icu_6712NumberFormat15setRoundingModeENS0_13ERoundingModeE
	.quad	_ZNK6icu_6712NumberFormat20getEffectiveCurrencyEPDsR10UErrorCode
	.quad	_ZN6icu_6712ChoiceFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6712ChoiceFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.quad	_ZNK6icu_6712ChoiceFormat9toPatternERNS_13UnicodeStringE
	.quad	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKNS_13UnicodeStringEi
	.quad	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEi
	.quad	_ZNK6icu_6712ChoiceFormat9getLimitsERi
	.quad	_ZNK6icu_6712ChoiceFormat11getClosuresERi
	.quad	_ZNK6icu_6712ChoiceFormat10getFormatsERi
	.quad	_ZNK6icu_6712ChoiceFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZN6icu_6712ChoiceFormat10setChoicesEPKdPKaPKNS_13UnicodeStringEiR10UErrorCode
	.local	_ZZN6icu_6712ChoiceFormat16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6712ChoiceFormat16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
