	.file	"reldtfmt.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718RelativeDateFormat17getDynamicClassIDEv
	.type	_ZNK6icu_6718RelativeDateFormat17getDynamicClassIDEv, @function
_ZNK6icu_6718RelativeDateFormat17getDynamicClassIDEv:
.LFB2789:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6718RelativeDateFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2789:
	.size	_ZNK6icu_6718RelativeDateFormat17getDynamicClassIDEv, .-_ZNK6icu_6718RelativeDateFormat17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718RelativeDateFormat20getDateFormatSymbolsEv
	.type	_ZNK6icu_6718RelativeDateFormat20getDateFormatSymbolsEv, @function
_ZNK6icu_6718RelativeDateFormat20getDateFormatSymbolsEv:
.LFB2812:
	.cfi_startproc
	endbr64
	movq	352(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*272(%rax)
	.cfi_endproc
.LFE2812:
	.size	_ZNK6icu_6718RelativeDateFormat20getDateFormatSymbolsEv, .-_ZNK6icu_6718RelativeDateFormat20getDateFormatSymbolsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringERNS_8CalendarERNS_13ParsePositionE
	.type	_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringERNS_8CalendarERNS_13ParsePositionE, @function
_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringERNS_8CalendarERNS_13ParsePositionE:
.LFB2804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$280, %rsp
	movq	%rdx, -256(%rbp)
	movq	%rcx, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movswl	368(%rdi), %eax
	shrl	$5, %eax
	jne	.L5
	movq	352(%rdi), %rdi
	movq	%rdx, %r15
	leaq	424(%rbx), %rsi
	movq	(%rdi), %rax
	call	*256(%rax)
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	352(%rbx), %rdi
	movq	(%rdi), %rax
	call	*88(%rax)
.L4:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movswl	432(%rdi), %eax
	movl	8(%rcx), %r14d
	shrl	$5, %eax
	jne	.L7
.L10:
	movl	728(%rbx), %edi
	testl	%edi, %edi
	jle	.L8
	movq	736(%rbx), %rdx
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%r15, %r12
	salq	$4, %r12
	leaq	(%rdx,%r12), %rax
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	je	.L54
	movl	4(%rax), %edx
	xorl	%r8d, %r8d
	movl	%r14d, %esi
	movq	%r13, %rdi
	movl	%edx, %r9d
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	je	.L15
	leal	1(%r15), %eax
	cmpl	728(%rbx), %eax
	jge	.L8
	movq	736(%rbx), %rdx
.L14:
	addq	$1, %r15
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L7:
	cmpq	$0, 488(%rdi)
	je	.L10
	leaq	-192(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movl	$0, -208(%rbp)
	movq	%rax, -224(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -216(%rbp)
	movl	728(%rbx), %eax
	movl	$0, -228(%rbp)
	testl	%eax, %eax
	jle	.L29
	xorl	%r10d, %r10d
	movq	%r10, %r12
	testl	%r14d, %r14d
	jns	.L25
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L20:
	movswl	%ax, %r9d
	movl	%r14d, %r8d
	sarl	$5, %r9d
	cmpl	%r14d, %r9d
	cmovle	%r9d, %r8d
.L23:
	subl	%r8d, %r9d
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	cmpl	%eax, %r14d
	jle	.L51
	movl	728(%rbx), %eax
.L19:
	addq	$1, %r12
	cmpl	%r12d, %eax
	jle	.L29
.L25:
	movq	736(%rbx), %rdx
	movq	%r12, %r13
	salq	$4, %r13
	addq	%r13, %rdx
	movq	8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L19
	movzwl	-184(%rbp), %eax
	movl	4(%rdx), %ecx
	testw	%ax, %ax
	jns	.L20
	cmpl	%r14d, -180(%rbp)
	movl	%r14d, %r8d
	movl	-180(%rbp), %r9d
	cmovle	-180(%rbp), %r8d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L54:
	leal	1(%r15), %eax
	cmpl	728(%rbx), %eax
	jl	.L14
.L8:
	movq	352(%rbx), %rdi
	leaq	360(%rbx), %rsi
	movq	(%rdi), %rax
	call	*256(%rax)
	movq	352(%rbx), %rdi
	movq	-248(%rbp), %rcx
	movq	%r13, %rsi
	movq	-256(%rbp), %rdx
	movq	(%rdi), %rax
	call	*88(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$0, -224(%rbp)
	leaq	-224(%rbp), %r15
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	-256(%rbp), %r13
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movl	$5, %esi
	movq	%r13, %rdi
	movq	%r15, %rcx
	movq	736(%rbx), %rax
	movl	(%rax,%r12), %edx
	movq	0(%r13), %rax
	call	*56(%rax)
	movl	-224(%rbp), %esi
	testl	%esi, %esi
	jle	.L16
	movq	-248(%rbp), %rax
	movl	%r14d, 12(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L16:
	movq	736(%rbx), %rax
	addl	4(%rax,%r12), %r14d
	movq	-248(%rbp), %rax
	movl	%r14d, 8(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L55:
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L28:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	cmpl	%eax, %r14d
	jle	.L51
	movl	728(%rbx), %eax
.L26:
	addq	$1, %r12
	cmpl	%r12d, %eax
	jle	.L29
.L18:
	movq	736(%rbx), %rdx
	movq	%r12, %r13
	salq	$4, %r13
	addq	%r13, %rdx
	movq	8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L26
	movzwl	-184(%rbp), %eax
	movl	4(%rdx), %ecx
	testw	%ax, %ax
	jns	.L55
	movl	-180(%rbp), %r9d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%r13d, %r13d
	movl	$0, -288(%rbp)
	leaq	360(%rbx), %r10
	movl	$0, -280(%rbp)
	leaq	-224(%rbp), %r12
	leaq	-128(%rbp), %r14
	movq	%rax, -264(%rbp)
	leaq	-228(%rbp), %r11
	movl	$0, -268(%rbp)
.L12:
	movq	488(%rbx), %rdi
	movq	%r10, %rdx
	movq	%r11, %r8
	movq	%r14, %rcx
	leaq	424(%rbx), %rsi
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	call	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_RS1_R10UErrorCode@PLT
	movq	352(%rbx), %rdi
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*256(%rax)
	movq	352(%rbx), %rdi
	movq	-248(%rbp), %rbx
	movq	%r15, %rsi
	movq	-256(%rbp), %rdx
	movq	(%rdi), %rax
	movq	%rbx, %rcx
	call	*88(%rax)
	movl	12(%rbx), %edx
	movl	%edx, %eax
	testl	%edx, %edx
	jns	.L33
	movl	8(%rbx), %eax
.L33:
	cmpl	%r13d, %eax
	jl	.L34
	movl	-288(%rbp), %ecx
	subl	-280(%rbp), %ecx
	subl	%ecx, %eax
.L35:
	movq	-248(%rbp), %rsi
	testl	%edx, %edx
	js	.L56
	movl	%eax, 12(%rsi)
.L37:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L34:
	movl	-268(%rbp), %esi
	cmpl	%eax, %esi
	cmovle	%esi, %eax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L56:
	movl	%eax, 8(%rsi)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L51:
	movq	-256(%rbp), %rdi
	movl	%eax, -268(%rbp)
	movl	$2, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -264(%rbp)
	movq	%rax, -128(%rbp)
	movq	(%rdi), %rax
	movw	%dx, -120(%rbp)
	call	*24(%rax)
	movq	%rax, %r12
	movq	%rax, -296(%rbp)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	leaq	-228(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -280(%rbp)
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	736(%rbx), %rax
	movq	%r12, %rdi
	movq	-280(%rbp), %r11
	movl	$5, %esi
	movl	(%rax,%r13), %edx
	movq	(%r12), %rax
	movq	%r11, %rcx
	call	*56(%rax)
	movl	-228(%rbp), %ecx
	movq	-280(%rbp), %r11
	testl	%ecx, %ecx
	jg	.L57
	movq	352(%rbx), %rdi
	leaq	360(%rbx), %r10
	leaq	-128(%rbp), %r14
	movq	%r11, -304(%rbp)
	movq	%r10, %rsi
	movq	%r10, -288(%rbp)
	leaq	-224(%rbp), %r12
	movq	(%rdi), %rax
	call	*256(%rax)
	movq	352(%rbx), %rdi
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	-296(%rbp), %rsi
	movq	(%rdi), %rax
	call	*64(%rax)
	movq	736(%rbx), %rax
	movq	-288(%rbp), %r10
	movq	-304(%rbp), %r11
	movl	4(%rax,%r13), %eax
	movl	%eax, -280(%rbp)
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L31
	sarl	$5, %eax
	movl	%eax, -288(%rbp)
	movl	%eax, %r13d
.L32:
	movl	-280(%rbp), %edx
	movl	%r13d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	-268(%rbp), %esi
	movq	%r15, %rdi
	movq	%r11, -312(%rbp)
	movq	%r10, -304(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	-296(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-312(%rbp), %r11
	movq	-304(%rbp), %r10
	movq	-264(%rbp), %rax
	addl	-268(%rbp), %r13d
	jmp	.L12
.L57:
	movq	-248(%rbp), %rax
	movq	%r12, %rdi
	movl	%r14d, 12(%rax)
	movq	(%r12), %rax
	call	*8(%rax)
	leaq	-128(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-224(%rbp), %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L4
.L31:
	movl	-116(%rbp), %eax
	movl	%eax, -288(%rbp)
	movl	%eax, %r13d
	jmp	.L32
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2804:
	.size	_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringERNS_8CalendarERNS_13ParsePositionE, .-_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringERNS_8CalendarERNS_13ParsePositionE
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, @function
_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode:
.LFB2818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	leaq	-120(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movq	%r8, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$120, %rsp
	movq	%rsi, -120(%rbp)
	movq	%r12, %rsi
	movq	%r8, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	call	*88(%rax)
	leaq	-100(%rbp), %rax
	movl	$0, -100(%rbp)
	movq	%rax, -152(%rbp)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L60:
	addl	$1, %ebx
.L61:
	movq	%r12, %rcx
	movq	%r15, %rdx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L58
	movq	-120(%rbp), %rdi
	movl	$10, %edx
	xorl	%esi, %esi
	call	strtol@PLT
	leal	2(%rax), %edx
	cmpl	%edx, 16(%r13)
	jle	.L60
	movq	8(%r13), %r8
	movslq	%edx, %rcx
	salq	$4, %rcx
	addq	%rcx, %r8
	cmpq	$0, 8(%r8)
	jne	.L60
	movl	%eax, (%r8)
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%rcx, -136(%rbp)
	movq	-144(%rbp), %rdx
	movq	%r8, -128(%rbp)
	movq	-152(%rbp), %rsi
	call	*32(%rax)
	movq	-128(%rbp), %r8
	movl	-100(%rbp), %edx
	movq	-136(%rbp), %rcx
	movq	%rax, 8(%r8)
	movq	8(%r13), %rax
	movl	%edx, 4(%rax,%rcx)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L58:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L64:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2818:
	.size	_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, .-_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkD2Ev, @function
_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkD2Ev:
.LFB2820:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE2820:
	.size	_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkD2Ev, .-_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkD1Ev,_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkD0Ev, @function
_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkD0Ev:
.LFB2822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2822:
	.size	_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkD0Ev, .-_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718RelativeDateFormat13applyPatternsERKNS_13UnicodeStringES3_R10UErrorCode
	.type	_ZN6icu_6718RelativeDateFormat13applyPatternsERKNS_13UnicodeStringES3_R10UErrorCode, @function
_ZN6icu_6718RelativeDateFormat13applyPatternsERKNS_13UnicodeStringES3_R10UErrorCode:
.LFB2811:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L73
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	360(%rdi), %rdi
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	leaq	424(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	.cfi_restore 3
	xorl	%edx, %edx
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	.cfi_endproc
.LFE2811:
	.size	_ZN6icu_6718RelativeDateFormat13applyPatternsERKNS_13UnicodeStringES3_R10UErrorCode, .-_ZN6icu_6718RelativeDateFormat13applyPatternsERKNS_13UnicodeStringES3_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718RelativeDateFormat9toPatternERNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6718RelativeDateFormat9toPatternERNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6718RelativeDateFormat9toPatternERNS_13UnicodeStringER10UErrorCode:
.LFB2808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L87
.L75:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movzwl	8(%rsi), %ecx
	movl	%ecx, %eax
	andl	$31, %eax
	andl	$1, %ecx
	movl	$2, %ecx
	cmovne	%ecx, %eax
	movw	%ax, 8(%rsi)
	movswl	368(%rdi), %eax
	shrl	$5, %eax
	je	.L88
	movswl	432(%rdi), %eax
	leaq	360(%rdi), %r9
	shrl	$5, %eax
	je	.L78
	movq	488(%rdi), %r10
	testq	%r10, %r10
	je	.L78
	leaq	424(%rdi), %rsi
	movq	%rdx, %r8
	movq	%r12, %rcx
	movq	%r9, %rdx
	movq	%r10, %rdi
	call	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_RS1_R10UErrorCode@PLT
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r9, %rsi
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	leaq	424(%rdi), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2808:
	.size	_ZNK6icu_6718RelativeDateFormat9toPatternERNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6718RelativeDateFormat9toPatternERNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718RelativeDateFormat13toPatternDateERNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6718RelativeDateFormat13toPatternDateERNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6718RelativeDateFormat13toPatternDateERNS_13UnicodeStringER10UErrorCode:
.LFB2809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L90
	movzwl	8(%rsi), %edx
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	xorl	%edx, %edx
	movw	%ax, 8(%rsi)
	leaq	360(%rdi), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
.L90:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2809:
	.size	_ZNK6icu_6718RelativeDateFormat13toPatternDateERNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6718RelativeDateFormat13toPatternDateERNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718RelativeDateFormat13toPatternTimeERNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6718RelativeDateFormat13toPatternTimeERNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6718RelativeDateFormat13toPatternTimeERNS_13UnicodeStringER10UErrorCode:
.LFB2810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L95
	movzwl	8(%rsi), %edx
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	xorl	%edx, %edx
	movw	%ax, 8(%rsi)
	leaq	424(%rdi), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
.L95:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2810:
	.size	_ZNK6icu_6718RelativeDateFormat13toPatternTimeERNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6718RelativeDateFormat13toPatternTimeERNS_13UnicodeStringER10UErrorCode
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"'"
	.string	"'"
	.string	""
	.string	""
	.align 2
.LC1:
	.string	"'"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718RelativeDateFormat6formatERNS_8CalendarERNS_13UnicodeStringERNS_13FieldPositionE
	.type	_ZNK6icu_6718RelativeDateFormat6formatERNS_8CalendarERNS_13UnicodeStringERNS_13FieldPositionE, @function
_ZNK6icu_6718RelativeDateFormat6formatERNS_8CalendarERNS_13UnicodeStringERNS_13FieldPositionE:
.LFB2802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$1, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-340(%rbp), %rbx
	movq	%rbx, %rdx
	subq	$344, %rsp
	movq	%rcx, -368(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$0, -340(%rbp)
	movq	%rax, -320(%rbp)
	movq	(%rdi), %rax
	movw	%r8w, -312(%rbp)
	call	*208(%rax)
	movl	-340(%rbp), %r9d
	movl	%eax, -360(%rbp)
	testl	%r9d, %r9d
	jg	.L101
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	%rax, %r14
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%rbx, %rdx
	movl	$20, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movq	%rbx, %rdx
	movl	$20, %esi
	movq	%r14, %rdi
	movl	%eax, -376(%rbp)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	-376(%rbp), %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	testq	%r14, %r14
	je	.L102
	movq	(%r14), %rdx
	movl	%ecx, -376(%rbp)
	movq	%r14, %rdi
	call	*8(%rdx)
	movl	-376(%rbp), %eax
.L102:
	movl	-340(%rbp), %edi
	leaq	-320(%rbp), %r14
	testl	%edi, %edi
	jg	.L101
	movl	%eax, %edx
	addl	$2, %edx
	js	.L101
	cmpl	%edx, 728(%r15)
	jle	.L101
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	736(%r15), %rdx
	cmpl	%eax, (%rdx)
	jne	.L101
	movq	8(%rdx), %rcx
	testq	%rcx, %rcx
	movq	%rcx, -376(%rbp)
	je	.L101
	movl	4(%rdx), %r9d
	movq	%r14, %rdi
	movl	%r9d, -384(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-312(%rbp), %edx
	movl	-384(%rbp), %r9d
	movq	-376(%rbp), %rcx
	testw	%dx, %dx
	js	.L104
	sarl	$5, %edx
.L105:
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	.p2align 4,,10
	.p2align 3
.L101:
	movswl	-312(%rbp), %eax
	testw	%ax, %ax
	js	.L106
	sarl	$5, %eax
.L107:
	testl	%eax, %eax
	jle	.L108
	movswl	368(%r15), %eax
	shrl	$5, %eax
	jne	.L163
.L108:
	movq	352(%r15), %rdi
	movl	-360(%rbp), %esi
	movq	%rbx, %rdx
	movq	(%rdi), %rax
	call	*200(%rax)
.L110:
	movswl	368(%r15), %eax
	shrl	$5, %eax
	jne	.L114
	movq	352(%r15), %rdi
	leaq	424(%r15), %rsi
.L162:
	movq	(%rdi), %rax
	call	*256(%rax)
	movq	352(%r15), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	-368(%rbp), %rcx
	movq	(%rdi), %rax
	call	*64(%rax)
.L115:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movswl	432(%r15), %eax
	movswl	-312(%rbp), %ecx
	shrl	$5, %eax
	jne	.L165
.L116:
	testw	%cx, %cx
	js	.L119
	sarl	$5, %ecx
	testl	%ecx, %ecx
	jg	.L166
.L121:
	movq	352(%r15), %rdi
	leaq	360(%r15), %rsi
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L163:
	movswl	432(%r15), %eax
	shrl	$5, %eax
	je	.L109
	cmpq	$0, 488(%r15)
	je	.L109
	cmpb	$0, 744(%r15)
	je	.L108
.L109:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	call	u_islower_67@PLT
	testb	%al, %al
	je	.L111
	movq	752(%r15), %rsi
	testq	%rsi, %rsi
	je	.L111
	movl	-360(%rbp), %eax
	cmpl	$258, %eax
	je	.L112
	cmpl	$259, %eax
	je	.L167
	cmpl	$260, -360(%rbp)
	jne	.L111
	cmpb	$0, 747(%r15)
	je	.L111
.L112:
	leaq	504(%r15), %rdx
	movl	$768, %ecx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleEj@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	movq	352(%r15), %rdi
	movq	%rbx, %rdx
	movl	$256, %esi
	movq	(%rdi), %rax
	call	*200(%rax)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L165:
	cmpq	$0, 488(%r15)
	je	.L116
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %esi
	movq	%rax, -256(%rbp)
	movswl	%cx, %eax
	sarl	$5, %eax
	testw	%cx, %cx
	cmovs	-308(%rbp), %eax
	movw	%si, -248(%rbp)
	testl	%eax, %eax
	jle	.L168
	leaq	.LC0(%rip), %rax
	leaq	-128(%rbp), %r10
	movl	$2, %ecx
	movl	$1, %esi
	movq	%rax, -328(%rbp)
	leaq	-328(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, %rdx
	movq	%r10, -384(%rbp)
	movq	%rax, -360(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$1, %ecx
	movl	$1, %esi
	leaq	-192(%rbp), %r11
	leaq	.LC1(%rip), %rax
	movq	%r11, %rdi
	leaq	-336(%rbp), %rdx
	movq	%r11, -376(%rbp)
	movq	%rax, -336(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	movq	-376(%rbp), %r11
	movq	-384(%rbp), %r10
	testw	%ax, %ax
	js	.L124
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L125:
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L126
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L127:
	movzwl	-312(%rbp), %eax
	testw	%ax, %ax
	js	.L128
	movswl	%ax, %edx
	sarl	$5, %edx
.L129:
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r14, %rdi
	pushq	%rcx
	movq	%r11, %rcx
	pushq	$0
	pushq	%r10
	movq	%r10, -384(%rbp)
	movq	%r11, -376(%rbp)
	call	_ZN6icu_6713UnicodeString14findAndReplaceEiiRKS0_iiS2_ii@PLT
	movq	-376(%rbp), %r11
	addq	$32, %rsp
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-384(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -376(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$39, %edx
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	-360(%rbp), %rcx
	movw	%dx, -328(%rbp)
	movq	%r14, %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movl	$39, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	-360(%rbp), %rsi
	movw	%cx, -328(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	-256(%rbp), %r9
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r9, %rdi
	movq	%r9, -360(%rbp)
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	movq	-360(%rbp), %r9
	movq	-376(%rbp), %r10
.L130:
	movq	488(%r15), %rdi
	movq	%r9, %rdx
	movq	%rbx, %r8
	movq	%r10, %rcx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	424(%r15), %rsi
	movq	%r9, -376(%rbp)
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movq	%r10, -360(%rbp)
	movw	%ax, -120(%rbp)
	call	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_RS1_R10UErrorCode@PLT
	movq	352(%r15), %rdi
	movq	-360(%rbp), %r10
	movq	(%rdi), %rax
	movq	%r10, %rsi
	call	*256(%rax)
	movq	352(%r15), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	-368(%rbp), %rcx
	movq	(%rdi), %rax
	call	*64(%rax)
	movq	-360(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-376(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L106:
	movl	-308(%rbp), %eax
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L119:
	movl	-308(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L121
.L166:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L104:
	movl	-308(%rbp), %edx
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	-256(%rbp), %r9
	leaq	360(%r15), %rsi
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%r9, -360(%rbp)
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	movq	-360(%rbp), %r9
	leaq	-128(%rbp), %r10
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L167:
	cmpb	$0, 746(%r15)
	jne	.L112
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L128:
	movl	-308(%rbp), %edx
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L126:
	movl	-180(%rbp), %r9d
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L124:
	movl	-116(%rbp), %ecx
	jmp	.L125
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2802:
	.size	_ZNK6icu_6718RelativeDateFormat6formatERNS_8CalendarERNS_13UnicodeStringERNS_13FieldPositionE, .-_ZNK6icu_6718RelativeDateFormat6formatERNS_8CalendarERNS_13UnicodeStringERNS_13FieldPositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718RelativeDateFormat5cloneEv
	.type	_ZNK6icu_6718RelativeDateFormat5cloneEv, @function
_ZNK6icu_6718RelativeDateFormat5cloneEv:
.LFB2800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$760, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L169
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6710DateFormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6718RelativeDateFormatE(%rip), %rax
	movq	$0, 352(%r12)
	leaq	360(%rbx), %rsi
	movq	%rax, (%r12)
	leaq	360(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	424(%rbx), %rsi
	leaq	424(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	496(%rbx), %eax
	movq	$0, 488(%r12)
	leaq	504(%r12), %rdi
	leaq	504(%rbx), %rsi
	movl	%eax, 496(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	728(%rbx), %eax
	movq	352(%rbx), %rdi
	movq	$0, 736(%r12)
	movq	$0, 752(%r12)
	movl	%eax, 728(%r12)
	movl	744(%rbx), %eax
	movl	%eax, 744(%r12)
	testq	%rdi, %rdi
	je	.L172
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rax, 352(%r12)
.L172:
	cmpq	$0, 488(%rbx)
	je	.L173
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L174
	movq	488(%rbx), %rax
	leaq	8(%r13), %rdi
	leaq	8(%rax), %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L174:
	movq	%r13, 488(%r12)
.L173:
	movslq	728(%r12), %rdi
	testl	%edi, %edi
	jle	.L175
	salq	$4, %rdi
	call	uprv_malloc_67@PLT
	movslq	728(%r12), %rdx
	movq	736(%rbx), %rsi
	movq	%rax, 736(%r12)
	movq	%rax, %rdi
	salq	$4, %rdx
	call	memcpy@PLT
.L175:
	movq	752(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L169
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rax, 752(%r12)
.L169:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2800:
	.size	_ZNK6icu_6718RelativeDateFormat5cloneEv, .-_ZNK6icu_6718RelativeDateFormat5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718RelativeDateFormat16getStaticClassIDEv
	.type	_ZN6icu_6718RelativeDateFormat16getStaticClassIDEv, @function
_ZN6icu_6718RelativeDateFormat16getStaticClassIDEv:
.LFB2788:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6718RelativeDateFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2788:
	.size	_ZN6icu_6718RelativeDateFormat16getStaticClassIDEv, .-_ZN6icu_6718RelativeDateFormat16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718RelativeDateFormatC2ERKS0_
	.type	_ZN6icu_6718RelativeDateFormatC2ERKS0_, @function
_ZN6icu_6718RelativeDateFormatC2ERKS0_:
.LFB2791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6710DateFormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6718RelativeDateFormatE(%rip), %rax
	movq	$0, 352(%rbx)
	leaq	360(%r12), %rsi
	movq	%rax, (%rbx)
	leaq	360(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	424(%r12), %rsi
	leaq	424(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	496(%r12), %eax
	movq	$0, 488(%rbx)
	leaq	504(%rbx), %rdi
	leaq	504(%r12), %rsi
	movl	%eax, 496(%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	728(%r12), %eax
	movq	352(%r12), %rdi
	movq	$0, 736(%rbx)
	movq	$0, 752(%rbx)
	movl	%eax, 728(%rbx)
	movl	744(%r12), %eax
	movl	%eax, 744(%rbx)
	testq	%rdi, %rdi
	je	.L189
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rax, 352(%rbx)
.L189:
	cmpq	$0, 488(%r12)
	je	.L190
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L191
	movq	488(%r12), %rax
	leaq	8(%r13), %rdi
	leaq	8(%rax), %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L191:
	movq	%r13, 488(%rbx)
.L190:
	movslq	728(%rbx), %rdi
	testl	%edi, %edi
	jle	.L192
	salq	$4, %rdi
	call	uprv_malloc_67@PLT
	movslq	728(%rbx), %rdx
	movq	736(%r12), %rsi
	movq	%rax, 736(%rbx)
	movq	%rax, %rdi
	salq	$4, %rdx
	call	memcpy@PLT
.L192:
	movq	752(%r12), %rdi
	testq	%rdi, %rdi
	je	.L188
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rax, 752(%rbx)
.L188:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2791:
	.size	_ZN6icu_6718RelativeDateFormatC2ERKS0_, .-_ZN6icu_6718RelativeDateFormatC2ERKS0_
	.globl	_ZN6icu_6718RelativeDateFormatC1ERKS0_
	.set	_ZN6icu_6718RelativeDateFormatC1ERKS0_,_ZN6icu_6718RelativeDateFormatC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionE
	.type	_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionE, @function
_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionE:
.LFB2805:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710DateFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionE@PLT
	.cfi_endproc
.LFE2805:
	.size	_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionE, .-_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718RelativeDateFormat15getStringForDayEiRiR10UErrorCode
	.type	_ZNK6icu_6718RelativeDateFormat15getStringForDayEiRiR10UErrorCode, @function
_ZNK6icu_6718RelativeDateFormat15getStringForDayEiRiR10UErrorCode:
.LFB2807:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jg	.L205
	movl	%esi, %eax
	addl	$2, %eax
	js	.L205
	cmpl	%eax, 728(%rdi)
	jle	.L205
	cltq
	salq	$4, %rax
	addq	736(%rdi), %rax
	cmpl	%esi, (%rax)
	jne	.L205
	movq	8(%rax), %r8
	testq	%r8, %r8
	je	.L205
	movl	4(%rax), %eax
	movl	%eax, (%rdx)
.L205:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE2807:
	.size	_ZNK6icu_6718RelativeDateFormat15getStringForDayEiRiR10UErrorCode, .-_ZNK6icu_6718RelativeDateFormat15getStringForDayEiRiR10UErrorCode
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"calendar/gregorian/DateTimePatterns"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"fields/day/relative"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718RelativeDateFormat9loadDatesER10UErrorCode
	.type	_ZN6icu_6718RelativeDateFormat9loadDatesER10UErrorCode, @function
_ZN6icu_6718RelativeDateFormat9loadDatesER10UErrorCode:
.LFB2823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$504, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	%rbx, %rdx
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	ures_open_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ures_getByKeyWithFallback_67@PLT
	movl	(%rbx), %edi
	movq	%rax, %r14
	testl	%edi, %edi
	jle	.L250
.L216:
	movl	$6, 728(%r12)
	movl	$96, %edi
	call	uprv_malloc_67@PLT
	movl	728(%r12), %edi
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE(%rip), %r8
	movq	%rax, 736(%r12)
	movq	%r8, -160(%rbp)
	movq	%rax, -152(%rbp)
	movl	%edi, -144(%rbp)
	testl	%edi, %edi
	jle	.L229
	leal	-1(%rdi), %edx
	cmpl	$3, %edx
	jbe	.L237
	movabsq	$-4294967296, %rcx
	movl	%edi, %esi
	movq	%rax, %rdx
	shrl	$2, %esi
	salq	$6, %rsi
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L228:
	movq	$0, 8(%rdx)
	addq	$64, %rdx
	movq	$0, -40(%rdx)
	movq	$0, -24(%rdx)
	movq	$0, -8(%rdx)
	movq	%rcx, -64(%rdx)
	movq	%rcx, -48(%rdx)
	movq	%rcx, -32(%rdx)
	movq	%rcx, -16(%rdx)
	cmpq	%rsi, %rdx
	jne	.L228
	movl	%edi, %edx
	andl	$-4, %edx
	testb	$3, %dil
	je	.L229
.L226:
	movabsq	$-4294967296, %rcx
	movslq	%edx, %rsi
	salq	$4, %rsi
	addq	%rax, %rsi
	movq	%rcx, (%rsi)
	movq	$0, 8(%rsi)
	leal	1(%rdx), %esi
	cmpl	%esi, %edi
	jle	.L229
	movslq	%esi, %rsi
	salq	$4, %rsi
	addq	%rax, %rsi
	movq	%rcx, (%rsi)
	movq	$0, 8(%rsi)
	leal	2(%rdx), %esi
	cmpl	%esi, %edi
	jle	.L229
	movslq	%esi, %rsi
	addl	$3, %edx
	salq	$4, %rsi
	addq	%rax, %rsi
	movq	%rcx, (%rsi)
	movq	$0, 8(%rsi)
	cmpl	%edx, %edi
	jle	.L229
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
.L229:
	movq	%rbx, %rcx
	movq	%r15, %rdx
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	ures_getAllItemsWithFallback_67@PLT
	movq	%r13, %rdi
	call	ures_close_67@PLT
	movl	(%rbx), %eax
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE(%rip), %r8
	testl	%eax, %eax
	jle	.L245
	movl	$0, 728(%r12)
.L245:
	movq	%r15, %rdi
	movq	%r8, -160(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	testq	%r14, %r14
	je	.L214
	movq	%r14, %rdi
	call	ures_close_67@PLT
.L214:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L251
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	movq	%rax, %rdi
	leaq	-160(%rbp), %r15
	call	ures_getSize_67@PLT
	cmpl	$8, %eax
	jle	.L216
	movl	$0, -164(%rbp)
	movl	$8, %esi
	cmpl	$12, %eax
	jle	.L218
	movl	496(%r12), %eax
	andb	$127, %al
	leal	9(%rax), %edx
	cmpl	$132, %eax
	cmovb	%edx, %esi
.L218:
	movq	%rbx, %rcx
	leaq	-164(%rbp), %rdx
	movq	%r14, %rdi
	call	ures_getStringByIndex_67@PLT
	movl	(%rbx), %esi
	movl	-164(%rbp), %ecx
	movq	%rax, %r15
	testl	%esi, %esi
	jle	.L219
.L220:
	leaq	-128(%rbp), %r9
	movq	%r15, -160(%rbp)
	movl	$1, %esi
	leaq	-160(%rbp), %r15
	movq	%r9, %rdi
	movq	%r15, %rdx
	movq	%r9, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L222
	movl	$2, %edx
	movq	%r9, %rsi
	movq	%rbx, %r8
	movl	$2, %ecx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, 16(%rdi)
	movl	$2, %edx
	movq	%rax, 8(%rdi)
	movq	%r9, -192(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	-192(%rbp), %r9
	movq	-184(%rbp), %rdi
.L222:
	movq	%rdi, 488(%r12)
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L216
.L237:
	xorl	%edx, %edx
	jmp	.L226
.L219:
	cmpl	$2, %ecx
	jle	.L220
	movl	$3, %edx
	leaq	_ZN6icu_67L8patItem1E(%rip), %rsi
	movq	%rax, %rdi
	call	u_strncmp_67@PLT
	testl	%eax, %eax
	jne	.L247
	movb	$1, 744(%r12)
.L247:
	movl	-164(%rbp), %ecx
	jmp	.L220
.L251:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2823:
	.size	_ZN6icu_6718RelativeDateFormat9loadDatesER10UErrorCode, .-_ZN6icu_6718RelativeDateFormat9loadDatesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718RelativeDateFormatC2E16UDateFormatStyleS1_RKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6718RelativeDateFormatC2E16UDateFormatStyleS1_RKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6718RelativeDateFormatC2E16UDateFormatStyleS1_RKNS_6LocaleER10UErrorCode:
.LFB2794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN6icu_6710DateFormatC2Ev@PLT
	movl	$2, %esi
	leaq	16+_ZTVN6icu_6718RelativeDateFormatE(%rip), %rax
	movl	$2, %edi
	movq	%rax, (%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%si, 368(%rbx)
	movq	%r14, %rsi
	movw	%di, 432(%rbx)
	leaq	504(%rbx), %rdi
	movq	$0, 352(%rbx)
	movq	%rax, 360(%rbx)
	movq	%rax, 424(%rbx)
	movq	$0, 488(%rbx)
	movl	%r13d, 496(%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%r12), %r8d
	movl	$0, 728(%rbx)
	movq	$0, 736(%rbx)
	movl	$0, 744(%rbx)
	movq	$0, 752(%rbx)
	testl	%r8d, %r8d
	jg	.L252
	leal	1(%r15), %eax
	cmpl	$4, %eax
	ja	.L278
	leaq	424(%rbx), %rax
	movq	%rax, -56(%rbp)
	cmpl	$3, %r13d
	jle	.L255
	andb	$127, %r13b
.L256:
	movl	%r13d, %edi
	movq	%r14, %rsi
	call	_ZN6icu_6710DateFormat18createDateInstanceENS0_6EStyleERKNS_6LocaleE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L277
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, 352(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L276
	movq	(%rax), %rax
	leaq	360(%rbx), %rsi
	call	*240(%rax)
	cmpl	$-1, %r15d
	je	.L262
	movl	%r15d, %edi
	movq	%r14, %rsi
	call	_ZN6icu_6710DateFormat18createTimeInstanceENS0_6EStyleERKNS_6LocaleE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L262
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L262
	movq	(%rax), %rax
	movq	%r13, %rdi
	leaq	424(%rbx), %rsi
	call	*240(%rax)
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L277:
	movq	$0, 352(%rbx)
.L276:
	movl	$16, (%r12)
.L252:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	movl	$1, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	cmpl	$-1, %r13d
	jne	.L256
	movq	%r14, %rsi
	movl	%r15d, %edi
	call	_ZN6icu_6710DateFormat18createTimeInstanceENS0_6EStyleERKNS_6LocaleE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L277
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, 352(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L265
	movq	(%rax), %rax
	leaq	424(%rbx), %rsi
	call	*240(%rax)
.L262:
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L279
.L267:
	addq	$24, %rsp
	movq	%r12, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6718RelativeDateFormat9loadDatesER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_678Calendar14createInstanceEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	movl	(%r12), %edx
	movq	%rax, 328(%rbx)
	testl	%edx, %edx
	jg	.L267
	testq	%rax, %rax
	jne	.L267
	movl	$7, (%r12)
	jmp	.L267
.L265:
	movq	0(%r13), %rax
	movl	$16, (%r12)
	movq	%r13, %rdi
	movq	8(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE2794:
	.size	_ZN6icu_6718RelativeDateFormatC2E16UDateFormatStyleS1_RKNS_6LocaleER10UErrorCode, .-_ZN6icu_6718RelativeDateFormatC2E16UDateFormatStyleS1_RKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6718RelativeDateFormatC1E16UDateFormatStyleS1_RKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6718RelativeDateFormatC1E16UDateFormatStyleS1_RKNS_6LocaleER10UErrorCode,_ZN6icu_6718RelativeDateFormatC2E16UDateFormatStyleS1_RKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718RelativeDateFormat18initializeCalendarEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6718RelativeDateFormat18initializeCalendarEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6718RelativeDateFormat18initializeCalendarEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode:
.LFB2824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movl	(%rcx), %edi
	testl	%edi, %edi
	jle	.L281
	movq	328(%r12), %rax
.L280:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	movq	%rsi, %rdi
	movq	%rcx, %rbx
	movq	%rdx, %rsi
	testq	%rdi, %rdi
	je	.L285
.L283:
	movq	%rbx, %rdx
	call	_ZN6icu_678Calendar14createInstanceEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %edx
	movq	%rax, 328(%r12)
	testl	%edx, %edx
	jg	.L280
	testq	%rax, %rax
	jne	.L280
	movl	$7, (%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	movq	%rdx, -24(%rbp)
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	-24(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L283
	.cfi_endproc
.LFE2824:
	.size	_ZN6icu_6718RelativeDateFormat18initializeCalendarEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6718RelativeDateFormat18initializeCalendarEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718RelativeDateFormat13dayDifferenceERNS_8CalendarER10UErrorCode
	.type	_ZN6icu_6718RelativeDateFormat13dayDifferenceERNS_8CalendarER10UErrorCode, @function
_ZN6icu_6718RelativeDateFormat13dayDifferenceERNS_8CalendarER10UErrorCode:
.LFB2825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L286
	movq	(%rdi), %rax
	movq	%rsi, %r12
	movq	%rdi, %r14
	call	*24(%rax)
	movq	%rax, %r15
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r12, %rdx
	movl	$20, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movq	%r12, %rdx
	movl	$20, %esi
	movq	%r15, %rdi
	movl	%eax, %r13d
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	subl	%eax, %r13d
	testq	%r15, %r15
	je	.L286
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
.L286:
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2825:
	.size	_ZN6icu_6718RelativeDateFormat13dayDifferenceERNS_8CalendarER10UErrorCode, .-_ZN6icu_6718RelativeDateFormat13dayDifferenceERNS_8CalendarER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718RelativeDateFormatD2Ev
	.type	_ZN6icu_6718RelativeDateFormatD2Ev, @function
_ZN6icu_6718RelativeDateFormatD2Ev:
.LFB2797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718RelativeDateFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	movq	352(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L294
	movq	(%rdi), %rax
	call	*8(%rax)
.L294:
	movq	488(%r12), %r13
	testq	%r13, %r13
	je	.L295
	movq	%r13, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L295:
	movq	736(%r12), %rdi
	call	uprv_free_67@PLT
	movq	752(%r12), %rdi
	testq	%rdi, %rdi
	je	.L296
	movq	(%rdi), %rax
	call	*8(%rax)
.L296:
	leaq	504(%r12), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	424(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	360(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710DateFormatD2Ev@PLT
	.cfi_endproc
.LFE2797:
	.size	_ZN6icu_6718RelativeDateFormatD2Ev, .-_ZN6icu_6718RelativeDateFormatD2Ev
	.globl	_ZN6icu_6718RelativeDateFormatD1Ev
	.set	_ZN6icu_6718RelativeDateFormatD1Ev,_ZN6icu_6718RelativeDateFormatD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718RelativeDateFormatD0Ev
	.type	_ZN6icu_6718RelativeDateFormatD0Ev, @function
_ZN6icu_6718RelativeDateFormatD0Ev:
.LFB2799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6718RelativeDateFormatD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2799:
	.size	_ZN6icu_6718RelativeDateFormatD0Ev, .-_ZN6icu_6718RelativeDateFormatD0Ev
	.section	.rodata.str1.1
.LC4:
	.string	"contextTransforms/relative"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718RelativeDateFormat29initCapitalizationContextInfoERKNS_6LocaleE
	.type	_ZN6icu_6718RelativeDateFormat29initCapitalizationContextInfoERKNS_6LocaleE, @function
_ZN6icu_6718RelativeDateFormat29initCapitalizationContextInfoERKNS_6LocaleE:
.LFB2814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-272(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	je	.L331
.L310:
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	xorl	%edi, %edi
	movl	$0, -280(%rbp)
	call	ures_open_67@PLT
	movq	%r13, %rcx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ures_getByKeyWithFallback_67@PLT
	movl	-280(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L332
	testq	%r12, %r12
	je	.L309
.L314:
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L309:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L333
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L309
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-276(%rbp), %rsi
	movl	$0, -276(%rbp)
	call	ures_getIntVector_67@PLT
	movl	-280(%rbp), %edx
	testl	%edx, %edx
	jg	.L314
	testq	%rax, %rax
	je	.L314
	cmpl	$1, -276(%rbp)
	jle	.L314
	movl	(%rax), %edx
	movb	%dl, 746(%rbx)
	movl	4(%rax), %eax
	movb	%al, 747(%rbx)
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L331:
	movq	%r12, %rdi
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	%rax, %r14
	jmp	.L310
.L333:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2814:
	.size	_ZN6icu_6718RelativeDateFormat29initCapitalizationContextInfoERKNS_6LocaleE, .-_ZN6icu_6718RelativeDateFormat29initCapitalizationContextInfoERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718RelativeDateFormateqERKNS_6FormatE
	.type	_ZNK6icu_6718RelativeDateFormateqERKNS_6FormatE, @function
_ZNK6icu_6718RelativeDateFormateqERKNS_6FormatE:
.LFB2801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZNK6icu_6710DateFormateqERKNS_6FormatE@PLT
	testb	%al, %al
	je	.L334
	movl	496(%r12), %eax
	cmpl	%eax, 496(%rbx)
	je	.L336
.L343:
	xorl	%eax, %eax
.L334:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	movswl	368(%r12), %ecx
	movzwl	368(%rbx), %eax
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L338
	testw	%ax, %ax
	js	.L339
	movswl	%ax, %edx
	sarl	$5, %edx
.L340:
	testw	%cx, %cx
	js	.L341
	sarl	$5, %ecx
.L342:
	testb	%sil, %sil
	jne	.L343
	cmpl	%edx, %ecx
	jne	.L343
	leaq	360(%r12), %rsi
	leaq	360(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L338:
	testb	%sil, %sil
	je	.L343
	movswl	432(%r12), %ecx
	movzwl	432(%rbx), %eax
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L344
	testw	%ax, %ax
	js	.L345
	movswl	%ax, %edx
	sarl	$5, %edx
.L346:
	testw	%cx, %cx
	js	.L347
	sarl	$5, %ecx
.L348:
	cmpl	%edx, %ecx
	jne	.L343
	testb	%sil, %sil
	jne	.L343
	leaq	424(%r12), %rsi
	leaq	424(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L344:
	testb	%sil, %sil
	je	.L343
	leaq	504(%r12), %rsi
	leaq	504(%rbx), %rdi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	setne	%al
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L339:
	movl	372(%rbx), %edx
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L341:
	movl	372(%r12), %ecx
	jmp	.L342
.L347:
	movl	436(%r12), %ecx
	jmp	.L348
.L345:
	movl	436(%rbx), %edx
	jmp	.L346
	.cfi_endproc
.LFE2801:
	.size	_ZNK6icu_6718RelativeDateFormateqERKNS_6FormatE, .-_ZNK6icu_6718RelativeDateFormateqERKNS_6FormatE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718RelativeDateFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6718RelativeDateFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6718RelativeDateFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB2803:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710DateFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode@PLT
	.cfi_endproc
.LFE2803:
	.size	_ZNK6icu_6718RelativeDateFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6718RelativeDateFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringER10UErrorCode:
.LFB2806:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710DateFormat5parseERKNS_13UnicodeStringER10UErrorCode@PLT
	.cfi_endproc
.LFE2806:
	.size	_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718RelativeDateFormat10setContextE15UDisplayContextR10UErrorCode
	.type	_ZN6icu_6718RelativeDateFormat10setContextE15UDisplayContextR10UErrorCode, @function
_ZN6icu_6718RelativeDateFormat10setContextE15UDisplayContextR10UErrorCode:
.LFB2813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6710DateFormat10setContextE15UDisplayContextR10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L389
.L373:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	cmpb	$0, 745(%rbx)
	jne	.L376
	leal	-259(%r13), %eax
	cmpl	$1, %eax
	jbe	.L390
.L376:
	cmpq	$0, 752(%rbx)
	jne	.L373
	cmpl	$258, %r13d
	je	.L382
.L380:
	cmpl	$259, %r13d
	je	.L391
	cmpl	$260, %r13d
	jne	.L373
	cmpb	$0, 747(%rbx)
	je	.L373
.L382:
	movl	$0, (%r12)
	leaq	504(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713BreakIterator22createSentenceInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 752(%rbx)
	movq	%rax, %rdi
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L373
	testq	%rdi, %rdi
	je	.L383
	movq	(%rdi), %rax
	call	*8(%rax)
.L383:
	movq	$0, 752(%rbx)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L390:
	leaq	504(%rbx), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6718RelativeDateFormat29initCapitalizationContextInfoERKNS_6LocaleE
	cmpq	$0, 752(%rbx)
	movb	$1, 745(%rbx)
	jne	.L373
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L391:
	cmpb	$0, 746(%rbx)
	jne	.L382
	jmp	.L373
	.cfi_endproc
.LFE2813:
	.size	_ZN6icu_6718RelativeDateFormat10setContextE15UDisplayContextR10UErrorCode, .-_ZN6icu_6718RelativeDateFormat10setContextE15UDisplayContextR10UErrorCode
	.weak	_ZTSN6icu_6718RelativeDateFormatE
	.section	.rodata._ZTSN6icu_6718RelativeDateFormatE,"aG",@progbits,_ZTSN6icu_6718RelativeDateFormatE,comdat
	.align 16
	.type	_ZTSN6icu_6718RelativeDateFormatE, @object
	.size	_ZTSN6icu_6718RelativeDateFormatE, 30
_ZTSN6icu_6718RelativeDateFormatE:
	.string	"N6icu_6718RelativeDateFormatE"
	.weak	_ZTIN6icu_6718RelativeDateFormatE
	.section	.data.rel.ro._ZTIN6icu_6718RelativeDateFormatE,"awG",@progbits,_ZTIN6icu_6718RelativeDateFormatE,comdat
	.align 8
	.type	_ZTIN6icu_6718RelativeDateFormatE, @object
	.size	_ZTIN6icu_6718RelativeDateFormatE, 24
_ZTIN6icu_6718RelativeDateFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718RelativeDateFormatE
	.quad	_ZTIN6icu_6710DateFormatE
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE, 24
_ZTIN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE, 45
_ZTSN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE:
	.string	"*N6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE"
	.weak	_ZTVN6icu_6718RelativeDateFormatE
	.section	.data.rel.ro._ZTVN6icu_6718RelativeDateFormatE,"awG",@progbits,_ZTVN6icu_6718RelativeDateFormatE,comdat
	.align 8
	.type	_ZTVN6icu_6718RelativeDateFormatE, @object
	.size	_ZTVN6icu_6718RelativeDateFormatE, 288
_ZTVN6icu_6718RelativeDateFormatE:
	.quad	0
	.quad	_ZTIN6icu_6718RelativeDateFormatE
	.quad	_ZN6icu_6718RelativeDateFormatD1Ev
	.quad	_ZN6icu_6718RelativeDateFormatD0Ev
	.quad	_ZNK6icu_6718RelativeDateFormat17getDynamicClassIDEv
	.quad	_ZNK6icu_6718RelativeDateFormateqERKNS_6FormatE
	.quad	_ZNK6icu_6718RelativeDateFormat5cloneEv
	.quad	_ZNK6icu_6718RelativeDateFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6710DateFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6710DateFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.quad	_ZNK6icu_6718RelativeDateFormat6formatERNS_8CalendarERNS_13UnicodeStringERNS_13FieldPositionE
	.quad	_ZNK6icu_6710DateFormat6formatERNS_8CalendarERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6718RelativeDateFormat5parseERKNS_13UnicodeStringERNS_8CalendarERNS_13ParsePositionE
	.quad	_ZNK6icu_6710DateFormat9isLenientEv
	.quad	_ZN6icu_6710DateFormat10setLenientEa
	.quad	_ZNK6icu_6710DateFormat17isCalendarLenientEv
	.quad	_ZN6icu_6710DateFormat18setCalendarLenientEa
	.quad	_ZNK6icu_6710DateFormat11getCalendarEv
	.quad	_ZN6icu_6710DateFormat13adoptCalendarEPNS_8CalendarE
	.quad	_ZN6icu_6710DateFormat11setCalendarERKNS_8CalendarE
	.quad	_ZNK6icu_6710DateFormat15getNumberFormatEv
	.quad	_ZN6icu_6710DateFormat17adoptNumberFormatEPNS_12NumberFormatE
	.quad	_ZN6icu_6710DateFormat15setNumberFormatERKNS_12NumberFormatE
	.quad	_ZNK6icu_6710DateFormat11getTimeZoneEv
	.quad	_ZN6icu_6710DateFormat13adoptTimeZoneEPNS_8TimeZoneE
	.quad	_ZN6icu_6710DateFormat11setTimeZoneERKNS_8TimeZoneE
	.quad	_ZN6icu_6718RelativeDateFormat10setContextE15UDisplayContextR10UErrorCode
	.quad	_ZNK6icu_6710DateFormat10getContextE19UDisplayContextTypeR10UErrorCode
	.quad	_ZN6icu_6710DateFormat19setBooleanAttributeE27UDateFormatBooleanAttributeaR10UErrorCode
	.quad	_ZNK6icu_6710DateFormat19getBooleanAttributeE27UDateFormatBooleanAttributeR10UErrorCode
	.quad	_ZNK6icu_6718RelativeDateFormat9toPatternERNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6718RelativeDateFormat13toPatternDateERNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6718RelativeDateFormat13toPatternTimeERNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6718RelativeDateFormat13applyPatternsERKNS_13UnicodeStringES3_R10UErrorCode
	.quad	_ZNK6icu_6718RelativeDateFormat20getDateFormatSymbolsEv
	.section	.data.rel.ro
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE, 48
_ZTVN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkE
	.quad	_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6712_GLOBAL__N_118RelDateFmtDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.section	.rodata
	.align 2
	.type	_ZN6icu_67L8patItem1E, @object
	.size	_ZN6icu_67L8patItem1E, 6
_ZN6icu_67L8patItem1E:
	.value	123
	.value	49
	.value	125
	.local	_ZZN6icu_6718RelativeDateFormat16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6718RelativeDateFormat16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
