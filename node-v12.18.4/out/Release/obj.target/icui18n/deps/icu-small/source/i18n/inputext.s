	.file	"inputext.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_679InputTextC2ER10UErrorCode
	.type	_ZN6icu_679InputTextC2ER10UErrorCode, @function
_ZN6icu_679InputTextC2ER10UErrorCode:
.LFB2054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$8192, %edi
	call	uprv_malloc_67@PLT
	movl	$512, %edi
	movq	%rax, (%rbx)
	call	uprv_malloc_67@PLT
	pxor	%xmm0, %xmm0
	cmpq	$0, (%rbx)
	movl	$0, 48(%rbx)
	movq	%rax, 16(%rbx)
	movups	%xmm0, 32(%rbx)
	je	.L4
	testq	%rax, %rax
	je	.L4
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	popq	%rbx
	movl	$7, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2054:
	.size	_ZN6icu_679InputTextC2ER10UErrorCode, .-_ZN6icu_679InputTextC2ER10UErrorCode
	.globl	_ZN6icu_679InputTextC1ER10UErrorCode
	.set	_ZN6icu_679InputTextC1ER10UErrorCode,_ZN6icu_679InputTextC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679InputTextD2Ev
	.type	_ZN6icu_679InputTextD2Ev, @function
_ZN6icu_679InputTextD2Ev:
.LFB2057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	16(%rbx), %rdi
	call	uprv_free_67@PLT
	movq	(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2057:
	.size	_ZN6icu_679InputTextD2Ev, .-_ZN6icu_679InputTextD2Ev
	.globl	_ZN6icu_679InputTextD1Ev
	.set	_ZN6icu_679InputTextD1Ev,_ZN6icu_679InputTextD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679InputText7setTextEPKci
	.type	_ZN6icu_679InputText7setTextEPKci, @function
_ZN6icu_679InputText7setTextEPKci:
.LFB2059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	$0, 8(%rdi)
	movb	$0, 24(%rdi)
	movq	%rsi, 40(%rdi)
	cmpl	$-1, %edx
	jne	.L13
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, %edx
.L13:
	movl	%edx, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2059:
	.size	_ZN6icu_679InputText7setTextEPKci, .-_ZN6icu_679InputText7setTextEPKci
	.align 2
	.p2align 4
	.globl	_ZN6icu_679InputText19setDeclaredEncodingEPKci
	.type	_ZN6icu_679InputText19setDeclaredEncodingEPKci, @function
_ZN6icu_679InputText19setDeclaredEncodingEPKci:
.LFB2060:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L15
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	cmpl	$-1, %edx
	je	.L21
.L17:
	movq	32(%r14), %rdi
	addl	$1, %ebx
	movslq	%ebx, %r12
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, 32(%r14)
	movq	%rax, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	strncpy@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, %ebx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE2060:
	.size	_ZN6icu_679InputText19setDeclaredEncodingEPKci, .-_ZN6icu_679InputText19setDeclaredEncodingEPKci
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679InputText5isSetEv
	.type	_ZNK6icu_679InputText5isSetEv, @function
_ZNK6icu_679InputText5isSetEv:
.LFB2061:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE2061:
	.size	_ZNK6icu_679InputText5isSetEv, .-_ZNK6icu_679InputText5isSetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679InputText10MungeInputEa
	.type	_ZN6icu_679InputText10MungeInputEa, @function
_ZN6icu_679InputText10MungeInputEa:
.LFB2062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	48(%rdi), %r8d
	testb	%sil, %sil
	je	.L24
	testl	%r8d, %r8d
	jle	.L44
	xorl	%eax, %eax
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L26:
	testb	%sil, %sil
	jne	.L29
	movq	(%rdx), %r9
	movslq	%edi, %r8
	addl	$1, %edi
	movb	%cl, (%r9,%r8)
	movl	48(%rdx), %r8d
.L29:
	cmpb	$62, %cl
	cmove	%ebx, %esi
.L28:
	leal	1(%rax), %ecx
	cmpl	%r8d, %ecx
	setge	%r9b
	cmpl	$8191, %edi
	setg	%cl
	addq	$1, %rax
	orb	%cl, %r9b
	jne	.L55
.L30:
	movq	40(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	cmpb	$60, %cl
	jne	.L26
	cmpb	$1, %sil
	movl	$1, %esi
	sbbl	$-1, %r11d
	addl	$1, %r10d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L55:
	movl	%edi, 8(%rdx)
	cmpl	$4, %r10d
	jle	.L24
	movl	$3435973837, %eax
	imulq	%rax, %r10
	shrq	$34, %r10
	cmpl	%r11d, %r10d
	jge	.L56
	.p2align 4,,10
	.p2align 3
.L24:
	cmpl	$8192, %r8d
	movl	$8192, %edi
	cmovle	%r8d, %edi
	testl	%r8d, %r8d
	jle	.L44
.L41:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L34:
	movq	40(%rdx), %rcx
	movzbl	(%rcx,%rax), %esi
	movq	(%rdx), %rcx
	movb	%sil, (%rcx,%rax)
	addq	$1, %rax
	cmpl	%eax, %edi
	jg	.L34
	testl	%edi, %edi
	movl	$1, %eax
	cmovle	%eax, %edi
.L25:
	movl	%edi, 8(%rdx)
.L33:
	movq	16(%rdx), %rax
	leaq	8(%rax), %rdi
	movq	$0, (%rax)
	movq	$0, 504(%rax)
	andq	$-8, %rdi
	subq	%rdi, %rax
	leal	512(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movl	8(%rdx), %edi
	movq	16(%rdx), %rcx
	testl	%edi, %edi
	jle	.L36
	movq	(%rdx), %rsi
	subl	$1, %edi
	leaq	1(%rsi), %rax
	addq	%rax, %rdi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L57:
	addq	$1, %rax
.L37:
	movzbl	(%rsi), %esi
	addw	$1, (%rcx,%rsi,2)
	movq	%rax, %rsi
	cmpq	%rax, %rdi
	jne	.L57
.L36:
	leaq	256(%rcx), %rax
	addq	$320, %rcx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L38:
	addq	$2, %rax
	cmpq	%rcx, %rax
	je	.L23
.L40:
	cmpw	$0, (%rax)
	je	.L38
	movb	$1, 24(%rdx)
.L23:
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	cmpl	$600, %r8d
	jle	.L33
	cmpl	$99, %edi
	jg	.L33
	cmpl	$8192, %r8d
	movl	$8192, %edi
	cmovle	%r8d, %edi
	jmp	.L41
.L44:
	xorl	%edi, %edi
	jmp	.L25
	.cfi_endproc
.LFE2062:
	.size	_ZN6icu_679InputText10MungeInputEa, .-_ZN6icu_679InputText10MungeInputEa
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
