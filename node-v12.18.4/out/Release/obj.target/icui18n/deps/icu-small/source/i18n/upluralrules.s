	.file	"upluralrules.cpp"
	.text
	.p2align 4
	.globl	uplrules_close_67
	.type	uplrules_close_67, @function
uplrules_close_67:
.LFB2824:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.cfi_endproc
.LFE2824:
	.size	uplrules_close_67, .-uplrules_close_67
	.p2align 4
	.globl	uplrules_select_67
	.type	uplrules_select_67, @function
uplrules_select_67:
.LFB2825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 15, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L4
	movl	%edx, %eax
	movq	%rsi, %rbx
	movl	%edx, %r13d
	movq	%rcx, %r12
	shrl	$31, %eax
	testq	%rsi, %rsi
	je	.L12
	testb	%al, %al
	je	.L8
.L14:
	movl	$1, (%r12)
	xorl	%eax, %eax
.L4:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L13
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	leaq	-112(%rbp), %r15
	movq	%rdi, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6711PluralRules6selectEd@PLT
	leaq	-120(%rbp), %rsi
	movq	%r12, %rcx
	movl	%r13d, %edx
	movq	%r15, %rdi
	movq	%rbx, -120(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -132(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-132(%rbp), %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	testl	%edx, %edx
	setne	%al
	testb	%al, %al
	jne	.L14
	jmp	.L8
.L13:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2825:
	.size	uplrules_select_67, .-uplrules_select_67
	.p2align 4
	.globl	uplrules_selectFormatted_67
	.type	uplrules_selectFormatted_67, @function
uplrules_selectFormatted_67:
.LFB2826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L24
	movl	%ecx, %eax
	movq	%rdi, %r14
	movq	%rdx, %rbx
	movq	%rsi, %rdi
	movl	%ecx, %r13d
	movq	%r8, %r12
	shrl	$31, %eax
	testq	%rdx, %rdx
	je	.L25
	testb	%al, %al
	je	.L20
.L27:
	movl	$1, (%r12)
.L24:
	xorl	%eax, %eax
.L15:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L26
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	%r12, %rsi
	call	_ZN6icu_676number4impl41validateUFormattedNumberToDecimalQuantityEPK16UFormattedNumberR10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L24
	leaq	-128(%rbp), %r15
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE@PLT
	movq	%r12, %rcx
	movl	%r13d, %edx
	movq	%r15, %rdi
	leaq	-136(%rbp), %rsi
	movq	%rbx, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -148(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-148(%rbp), %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L25:
	testl	%ecx, %ecx
	setne	%al
	testb	%al, %al
	jne	.L27
	jmp	.L20
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2826:
	.size	uplrules_selectFormatted_67, .-uplrules_selectFormatted_67
	.p2align 4
	.globl	uplrules_getKeywords_67
	.type	uplrules_getKeywords_67, @function
uplrules_getKeywords_67:
.LFB2828:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L35
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L38
	call	_ZNK6icu_6711PluralRules11getKeywordsER10UErrorCode@PLT
	movq	%rax, %rdi
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L28
	testq	%rdi, %rdi
	je	.L39
	addq	$8, %rsp
	movq	%r12, %rsi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uenum_openFromStringEnumeration_67@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movl	$1, (%rsi)
.L28:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$7, (%r12)
	jmp	.L28
	.cfi_endproc
.LFE2828:
	.size	uplrules_getKeywords_67, .-uplrules_getKeywords_67
	.p2align 4
	.globl	uplrules_openForType_67
	.type	uplrules_openForType_67, @function
uplrules_openForType_67:
.LFB2823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-272(%rbp), %r12
	movq	%r12, %rdi
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	%r13d, %esi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	addq	$248, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L43:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2823:
	.size	uplrules_openForType_67, .-uplrules_openForType_67
	.p2align 4
	.globl	uplrules_open_67
	.type	uplrules_open_67, @function
uplrules_open_67:
.LFB2822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-256(%rbp), %r12
	movq	%r12, %rdi
	subq	$240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$240, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L47:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2822:
	.size	uplrules_open_67, .-uplrules_open_67
	.p2align 4
	.globl	uplrules_selectWithFormat_67
	.type	uplrules_selectWithFormat_67, @function
uplrules_selectWithFormat_67:
.LFB2827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L48
	movq	%rdi, %r9
	movq	%r8, %r12
	testq	%rdi, %rdi
	je	.L50
	movq	%rsi, %r10
	testq	%rsi, %rsi
	je	.L50
	movq	%rdx, %rbx
	movl	%ecx, %r15d
	testq	%rdx, %rdx
	je	.L63
	testl	%ecx, %ecx
	js	.L50
.L52:
	leaq	-176(%rbp), %r14
	movq	%r10, -336(%rbp)
	leaq	-320(%rbp), %r13
	movq	%r14, %rdi
	movq	%r9, -328(%rbp)
	call	_ZN6icu_6711FormattableC1Ed@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jle	.L64
.L57:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-240(%rbp), %r8
	movq	%rax, -240(%rbp)
	movl	$2, %eax
	movw	%ax, -232(%rbp)
.L56:
	movq	%r12, %rcx
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%rbx, -320(%rbp)
	movq	%r8, -336(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -328(%rbp)
	movq	-336(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movl	-328(%rbp), %eax
.L48:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L65
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	testl	%ecx, %ecx
	je	.L52
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L64:
	movq	-336(%rbp), %r10
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	movq	%r10, %rdi
	call	__dynamic_cast@PLT
	movq	-328(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, -336(%rbp)
	je	.L54
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	-336(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK6icu_6713DecimalFormat23formatToDecimalQuantityERKNS_11FormattableERNS_6number4impl15DecimalQuantityER10UErrorCode@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L66
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L66:
	movq	-328(%rbp), %r9
	leaq	-240(%rbp), %r8
	movq	%r13, %rdx
	movq	%r8, %rdi
	movq	%r8, -328(%rbp)
	movq	%r9, %rsi
	call	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	-328(%rbp), %r8
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r9, -328(%rbp)
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	movl	(%r12), %edx
	movq	-328(%rbp), %r9
	testl	%edx, %edx
	jg	.L57
	leaq	-240(%rbp), %r8
	movq	%r9, %rsi
	movq	%r8, %rdi
	movq	%r8, -328(%rbp)
	call	_ZNK6icu_6711PluralRules6selectEd@PLT
	movq	-328(%rbp), %r8
	jmp	.L56
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2827:
	.size	uplrules_selectWithFormat_67, .-uplrules_selectWithFormat_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
