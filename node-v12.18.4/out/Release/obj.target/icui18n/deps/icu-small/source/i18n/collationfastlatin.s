	.file	"collationfastlatin.cpp"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_.constprop.0, @function
_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_.constprop.0:
.LFB4153:
	.cfi_startproc
	leal	-1024(%rdx), %eax
	movl	%edx, %r10d
	cmpl	$2047, %eax
	ja	.L33
	cmpl	$2047, %edx
	ja	.L37
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%r8), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	(%r9), %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	testl	%esi, %esi
	jne	.L4
	testl	%r12d, %r12d
	js	.L38
.L4:
	andl	$1023, %edx
	addl	$448, %edx
	movl	%edx, %r10d
	cmpl	%eax, %r12d
	je	.L39
	movslq	%eax, %rsi
	leal	1(%rax), %ebx
	movzbl	(%rcx,%rsi), %r11d
	cmpl	$127, %r11d
	jle	.L7
	leal	-194(%r11), %r13d
	leal	2(%rax), %esi
	cmpl	$3, %r13d
	ja	.L8
	cmpl	%ebx, %r12d
	jne	.L40
.L8:
	cmpl	%esi, %r12d
	jg	.L9
	testl	%r12d, %r12d
	jns	.L11
.L9:
	cmpl	$226, %r11d
	je	.L41
	cmpl	$239, %r11d
	jne	.L11
	movslq	%ebx, %rbx
	cmpb	$-65, (%rcx,%rbx)
	jne	.L11
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %ecx
	cmpb	$-66, %cl
	je	.L35
	cmpb	$-65, %cl
	jne	.L11
.L35:
	leal	3(%rax), %ebx
	movl	$-1, %r11d
	.p2align 4,,10
	.p2align 3
.L14:
	movzwl	(%rdi,%rdx,2), %eax
	.p2align 4,,10
	.p2align 3
.L16:
	sarl	$9, %eax
	addl	%eax, %r10d
	movslq	%r10d, %rsi
	movzwl	(%rdi,%rsi,2), %eax
	movl	%eax, %ecx
	andl	$511, %ecx
	cmpl	%r11d, %ecx
	jl	.L16
	je	.L17
	movzwl	(%rdi,%rdx,2), %eax
	movq	%rdx, %rsi
.L6:
	sarl	$9, %eax
	cmpl	$1, %eax
	je	.L11
	leaq	1(%rsi), %rdx
	movzwl	(%rdi,%rdx,2), %r10d
	leaq	(%rdx,%rdx), %rcx
	cmpl	$2, %eax
	je	.L1
	movzwl	2(%rdi,%rcx), %eax
	sall	$16, %eax
	orl	%eax, %r10d
.L1:
	popq	%rbx
	movl	%r10d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	andl	$1023, %r10d
	movzwl	898(%rdi,%r10,2), %eax
	movzwl	896(%rdi,%r10,2), %r10d
	sall	$16, %eax
	orl	%eax, %r10d
.L33:
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movslq	%ebx, %r14
	movzbl	(%rcx,%r14), %r14d
	leal	-128(%r14), %r15d
	cmpb	$63, %r15b
	ja	.L8
	sall	$6, %r13d
	movzbl	%r14b, %r11d
	movl	%esi, %ebx
	addl	%r13d, %r11d
	.p2align 4,,10
	.p2align 3
.L7:
	testl	%r11d, %r11d
	jne	.L14
	testl	%r12d, %r12d
	jns	.L14
	movl	%eax, (%r9)
	movl	$-1, %r11d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$1, %r10d
	popq	%rbx
	popq	%r12
	movl	%r10d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movl	%edx, %esi
	movzwl	(%rdi,%rsi,2), %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L17:
	movl	%ebx, (%r8)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L38:
	subl	$1, %eax
	movl	$2, %r10d
	movl	%eax, (%r9)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L41:
	movslq	%ebx, %rbx
	cmpb	$-128, (%rcx,%rbx)
	jne	.L11
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %ecx
	leal	-128(%rcx), %esi
	cmpb	$63, %sil
	ja	.L11
	leal	256(%rcx), %r11d
	leal	3(%rax), %ebx
	jmp	.L14
	.cfi_endproc
.LFE4153:
	.size	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_.constprop.0, .-_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_.constprop.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationFastLatin10getOptionsEPKNS_13CollationDataERKNS_17CollationSettingsEPti
	.type	_ZN6icu_6718CollationFastLatin10getOptionsEPKNS_13CollationDataERKNS_17CollationSettingsEPti, @function
_ZN6icu_6718CollationFastLatin10getOptionsEPKNS_13CollationDataERKNS_17CollationSettingsEPti:
.LFB3154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	88(%rdi), %rax
	movq	%rax, -72(%rbp)
	cmpl	$384, %ecx
	jne	.L47
	testq	%rax, %rax
	je	.L47
	movl	24(%rsi), %eax
	movq	%rdi, %r12
	movq	%rsi, %r14
	movq	%rdx, %r15
	testb	$12, %al
	je	.L69
	movq	-72(%rbp), %rbx
	movl	%eax, %ecx
	sarl	$4, %ecx
	andl	$7, %ecx
	movzbl	(%rbx), %esi
	addl	$1, %ecx
	cmpl	%ecx, %esi
	jle	.L47
	movslq	%ecx, %rcx
	cmpq	$0, 32(%r14)
	movzwl	(%rbx,%rcx,2), %ebx
	je	.L70
.L96:
	movl	$0, -60(%rbp)
	movl	$4096, %r13d
	movl	$0, -56(%rbp)
	movl	$0, -64(%rbp)
	movl	$0, -52(%rbp)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L94:
	cmpl	$1, %esi
	jbe	.L75
	movq	%r14, %rdi
	call	_ZNK6icu_6717CollationSettings9reorderExEj@PLT
	cmpl	$4100, %r13d
	je	.L71
.L95:
	testl	%eax, %eax
	je	.L53
	movl	-52(%rbp), %edx
	cmpl	%edx, %eax
	jb	.L47
	movl	-56(%rbp), %esi
	testl	%esi, %esi
	je	.L72
	movl	-60(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L72
	movl	-64(%rbp), %esi
	cmpl	%esi, %edx
	je	.L92
	movl	%eax, -52(%rbp)
	movl	$0, -60(%rbp)
.L53:
	cmpl	$4103, %r13d
	je	.L93
.L52:
	addl	$1, %r13d
.L55:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713CollationData23getFirstPrimaryForGroupEi@PLT
	movq	32(%r14), %rdi
	movl	%eax, %esi
	shrl	$24, %eax
	movzbl	(%rdi,%rax), %eax
	testb	%al, %al
	je	.L94
.L75:
	sall	$24, %eax
	andl	$16777215, %esi
	orl	%esi, %eax
	cmpl	$4100, %r13d
	jne	.L95
.L71:
	movl	%eax, -56(%rbp)
	movl	-52(%rbp), %eax
	movl	%eax, -64(%rbp)
	jmp	.L52
.L69:
	cmpq	$0, 32(%r14)
	movl	$3071, %ebx
	jne	.L96
.L70:
	xorl	%r10d, %r10d
	jmp	.L48
.L72:
	movl	%eax, -52(%rbp)
	cmpl	$4103, %r13d
	jne	.L52
.L93:
	movl	$25, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713CollationData23getFirstPrimaryForGroupEi@PLT
	movq	32(%r14), %rcx
	movl	%eax, %esi
	shrl	$24, %eax
	movzbl	(%rcx,%rax), %eax
	testb	%al, %al
	jne	.L76
	cmpl	$1, %esi
	jbe	.L76
	movq	%r14, %rdi
	call	_ZNK6icu_6717CollationSettings9reorderExEj@PLT
.L58:
	cmpl	%eax, -52(%rbp)
	ja	.L47
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	cmovne	%edx, %eax
	movl	-56(%rbp), %edx
	cmpl	%edx, -64(%rbp)
	setnb	%r10b
	cmpl	%edx, %eax
	setbe	%al
	orl	%eax, %r10d
	movl	24(%r14), %eax
.L48:
	movq	-72(%rbp), %rdx
	leaq	16(%r15), %r8
	movzbl	(%rdx), %ecx
	leaq	(%rdx,%rcx,2), %rsi
	leaq	16(%rcx,%rcx), %rdi
	cmpq	%r8, %rsi
	jnb	.L77
	addq	%rdx, %rdi
	cmpq	%rdi, %r15
	jb	.L60
.L77:
	movd	%ebx, %xmm5
	pxor	%xmm14, %xmm14
	pxor	%xmm6, %xmm6
	xorl	%ecx, %ecx
	movdqa	.LC2(%rip), %xmm12
	pshufd	$0, %xmm5, %xmm11
	movdqa	.LC1(%rip), %xmm13
	psubd	%xmm12, %xmm11
	psubd	%xmm12, %xmm13
	.p2align 4,,10
	.p2align 3
.L63:
	movdqu	(%rsi,%rcx), %xmm3
	movdqa	.LC0(%rip), %xmm5
	movdqa	%xmm3, %xmm0
	movdqa	%xmm3, %xmm4
	pand	%xmm3, %xmm5
	punpcklwd	%xmm14, %xmm0
	punpckhwd	%xmm14, %xmm4
	pand	.LC3(%rip), %xmm3
	psubd	%xmm12, %xmm0
	psubd	%xmm12, %xmm4
	movdqa	%xmm0, %xmm1
	pcmpgtd	%xmm11, %xmm0
	movdqa	%xmm4, %xmm8
	pcmpgtd	%xmm13, %xmm1
	pcmpgtd	%xmm13, %xmm8
	pcmpgtd	%xmm11, %xmm4
	movdqa	%xmm0, %xmm2
	movdqa	%xmm1, %xmm10
	pcmpeqd	%xmm6, %xmm2
	movdqa	%xmm8, %xmm9
	pcmpeqd	%xmm6, %xmm10
	pcmpeqd	%xmm6, %xmm9
	movdqa	%xmm4, %xmm7
	pcmpeqd	%xmm6, %xmm7
	pand	%xmm10, %xmm2
	pand	%xmm9, %xmm4
	pand	%xmm10, %xmm0
	pand	%xmm9, %xmm7
	movdqa	%xmm2, %xmm15
	punpcklwd	%xmm7, %xmm2
	punpckhwd	%xmm7, %xmm15
	movdqa	%xmm2, %xmm7
	punpcklwd	%xmm15, %xmm2
	punpckhwd	%xmm15, %xmm7
	punpcklwd	%xmm7, %xmm2
	movdqa	%xmm0, %xmm7
	punpcklwd	%xmm4, %xmm0
	punpckhwd	%xmm4, %xmm7
	movdqa	%xmm0, %xmm4
	punpckhwd	%xmm7, %xmm4
	punpcklwd	%xmm7, %xmm0
	punpcklwd	%xmm4, %xmm0
	movdqa	%xmm1, %xmm4
	punpcklwd	%xmm8, %xmm1
	punpckhwd	%xmm8, %xmm4
	movdqa	%xmm1, %xmm7
	punpckhwd	%xmm4, %xmm7
	punpcklwd	%xmm4, %xmm1
	punpcklwd	%xmm7, %xmm1
	pand	%xmm1, %xmm3
	pandn	%xmm5, %xmm1
	pand	%xmm0, %xmm5
	por	%xmm3, %xmm1
	pandn	%xmm1, %xmm0
	por	%xmm5, %xmm0
	pandn	%xmm0, %xmm2
	movups	%xmm2, (%r15,%rcx)
	addq	$16, %rcx
	cmpq	$768, %rcx
	jne	.L63
.L62:
	testb	%r10b, %r10b
	jne	.L67
	testb	$2, %al
	je	.L68
.L67:
	movl	$0, 112(%r15)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 96(%r15)
.L68:
	addq	$40, %rsp
	sall	$16, %ebx
	orl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	sall	$24, %eax
	andl	$16777215, %esi
	orl	%esi, %eax
	jmp	.L58
.L92:
	movl	%eax, -60(%rbp)
	movl	%eax, -52(%rbp)
	jmp	.L53
.L60:
	leaq	(%rdx,%rcx,2), %r9
	xorl	%esi, %esi
	xorl	%r11d, %r11d
	.p2align 4,,10
	.p2align 3
.L66:
	movzwl	(%r9,%rsi), %r8d
	movl	%r8d, %edi
	movl	%r8d, %ecx
	andw	$-1024, %di
	cmpw	$4095, %r8w
	ja	.L65
	andl	$-8, %ecx
	cmpl	%r8d, %ebx
	movl	%ecx, %edi
	cmovnb	%r11d, %edi
.L65:
	movw	%di, (%r15,%rsi)
	addq	$2, %rsi
	cmpq	$768, %rsi
	jne	.L66
	jmp	.L62
.L47:
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3154:
	.size	_ZN6icu_6718CollationFastLatin10getOptionsEPKNS_13CollationDataERKNS_17CollationSettingsEPti, .-_ZN6icu_6718CollationFastLatin10getOptionsEPKNS_13CollationDataERKNS_17CollationSettingsEPti
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationFastLatin6lookupEPKti
	.type	_ZN6icu_6718CollationFastLatin6lookupEPKti, @function
_ZN6icu_6718CollationFastLatin6lookupEPKti:
.LFB3157:
	.cfi_startproc
	endbr64
	leal	-8192(%rsi), %eax
	cmpl	$63, %eax
	jbe	.L102
	cmpl	$65534, %esi
	je	.L100
	cmpl	$65535, %esi
	movl	$1, %edx
	movl	$64680, %eax
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	cltq
	movzwl	768(%rdi,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE3157:
	.size	_ZN6icu_6718CollationFastLatin6lookupEPKti, .-_ZN6icu_6718CollationFastLatin6lookupEPKti
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationFastLatin10lookupUTF8EPKtiPKhRii
	.type	_ZN6icu_6718CollationFastLatin10lookupUTF8EPKtiPKhRii, @function
_ZN6icu_6718CollationFastLatin10lookupUTF8EPKtiPKhRii:
.LFB3158:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	leal	1(%rax), %r9d
	cmpl	%r8d, %r9d
	jl	.L104
	testl	%r8d, %r8d
	js	.L104
.L107:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	movslq	%eax, %r8
	movslq	%r9d, %r9
	addl	$2, %eax
	movzbl	(%rdx,%r8), %r8d
	movzbl	(%rdx,%r9), %edx
	movl	%eax, (%rcx)
	cmpl	$226, %esi
	jne	.L106
	cmpb	$-128, %r8b
	jne	.L106
	leal	-128(%rdx), %eax
	cmpb	$63, %al
	ja	.L107
	movzwl	512(%rdi,%rdx,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	cmpl	$239, %esi
	jne	.L107
	cmpb	$-65, %r8b
	jne	.L107
	cmpb	$-66, %dl
	je	.L108
	cmpb	$-65, %dl
	jne	.L107
	movl	$64680, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE3158:
	.size	_ZN6icu_6718CollationFastLatin10lookupUTF8EPKtiPKhRii, .-_ZN6icu_6718CollationFastLatin10lookupUTF8EPKtiPKhRii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationFastLatin16lookupUTF8UnsafeEPKtiPKhRi
	.type	_ZN6icu_6718CollationFastLatin16lookupUTF8UnsafeEPKtiPKhRi, @function
_ZN6icu_6718CollationFastLatin16lookupUTF8UnsafeEPKtiPKhRi:
.LFB3159:
	.cfi_startproc
	endbr64
	movslq	(%rcx), %r8
	movq	%r8, %rax
	cmpl	$197, %esi
	jg	.L122
	addl	$1, %eax
	subl	$194, %esi
	movl	%eax, (%rcx)
	movzbl	(%rdx,%r8), %eax
	sall	$6, %esi
	addl	%eax, %esi
	movslq	%esi, %rsi
	movzwl	(%rdi,%rsi,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	addl	$2, %eax
	movzbl	1(%rdx,%r8), %edx
	movl	%eax, (%rcx)
	cmpl	$226, %esi
	je	.L126
	cmpb	$-66, %dl
	movl	$3, %eax
	movl	$64680, %edx
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	movzwl	512(%rdi,%rdx,2), %eax
	ret
	.cfi_endproc
.LFE3159:
	.size	_ZN6icu_6718CollationFastLatin16lookupUTF8UnsafeEPKtiPKhRi, .-_ZN6icu_6718CollationFastLatin16lookupUTF8UnsafeEPKtiPKhRi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_
	.type	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_, @function
_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_:
.LFB3160:
	.cfi_startproc
	endbr64
	leal	-1024(%rdx), %r10d
	movl	%edx, %eax
	cmpl	$2047, %r10d
	ja	.L163
	cmpl	$2047, %edx
	ja	.L166
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	movq	16(%rbp), %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rax), %ebx
	movl	(%r9), %eax
	testl	%esi, %esi
	jne	.L130
	testl	%ebx, %ebx
	js	.L167
.L130:
	andl	$1023, %edx
	addl	$448, %edx
	movl	%edx, %esi
	cmpl	%eax, %ebx
	je	.L168
	leal	1(%rax), %r11d
	movslq	%eax, %r10
	testq	%rcx, %rcx
	je	.L133
	movzwl	(%rcx,%r10,2), %r10d
	cmpl	$383, %r10d
	jle	.L134
	leal	-8192(%r10), %ecx
	cmpl	$63, %ecx
	ja	.L135
	subl	$7808, %r10d
.L134:
	testl	%r10d, %r10d
	jne	.L136
	testl	%ebx, %ebx
	jns	.L136
	movq	16(%rbp), %rbx
	movl	$-1, %r10d
	movl	%eax, (%rbx)
	.p2align 4,,10
	.p2align 3
.L136:
	movl	%edx, %r8d
	movzwl	(%rdi,%r8,2), %edx
	.p2align 4,,10
	.p2align 3
.L144:
	sarl	$9, %edx
	addl	%edx, %esi
	movslq	%esi, %rcx
	movzwl	(%rdi,%rcx,2), %edx
	movl	%edx, %eax
	andl	$511, %eax
	cmpl	%r10d, %eax
	jl	.L144
	je	.L145
	movzwl	(%rdi,%r8,2), %edx
	movq	%r8, %rcx
.L132:
	sarl	$9, %edx
	cmpl	$1, %edx
	je	.L138
	addq	$1, %rcx
	movzwl	(%rdi,%rcx,2), %eax
	leaq	(%rcx,%rcx), %rsi
	cmpl	$2, %edx
	je	.L127
	movzwl	2(%rdi,%rsi), %edx
	sall	$16, %edx
	orl	%edx, %eax
.L127:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	andl	$1023, %eax
	movzwl	898(%rdi,%rax,2), %edx
	movzwl	896(%rdi,%rax,2), %eax
	sall	$16, %edx
	orl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movzbl	(%r8,%r10), %r10d
	cmpl	$127, %r10d
	jle	.L134
	leal	-194(%r10), %r12d
	leal	2(%rax), %ecx
	cmpl	$3, %r12d
	ja	.L137
	cmpl	%r11d, %ebx
	je	.L137
	movslq	%r11d, %r13
	movzbl	(%r8,%r13), %r13d
	leal	-128(%r13), %r14d
	cmpb	$63, %r14b
	jbe	.L169
.L137:
	cmpl	%ecx, %ebx
	jg	.L148
	testl	%ebx, %ebx
	jns	.L138
.L148:
	cmpl	$226, %r10d
	je	.L170
	cmpl	$239, %r10d
	jne	.L138
	movslq	%r11d, %r11
	cmpb	$-65, (%r8,%r11)
	jne	.L138
	movslq	%ecx, %rcx
	movzbl	(%r8,%rcx), %ecx
	cmpb	$-66, %cl
	je	.L165
	cmpb	$-65, %cl
	jne	.L138
.L165:
	leal	3(%rax), %r11d
	movl	$-1, %r10d
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L168:
	movl	%edx, %ecx
	movzwl	(%rdi,%rcx,2), %edx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L135:
	leal	-65534(%r10), %eax
	movl	$-1, %r10d
	cmpl	$1, %eax
	jbe	.L136
.L138:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movl	%r11d, (%r9)
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L167:
	movq	16(%rbp), %rbx
	subl	$1, %eax
	movl	%eax, (%rbx)
	movl	$2, %eax
	jmp	.L127
.L170:
	movslq	%r11d, %r11
	cmpb	$-128, (%r8,%r11)
	jne	.L138
	movslq	%ecx, %rcx
	movzbl	(%r8,%rcx), %ecx
	leal	-128(%rcx), %r8d
	cmpb	$63, %r8b
	ja	.L138
	leal	256(%rcx), %r10d
	leal	3(%rax), %r11d
	jmp	.L136
.L169:
	movl	%r12d, %r10d
	movl	%ecx, %r11d
	sall	$6, %r10d
	addl	%r13d, %r10d
	jmp	.L134
	.cfi_endproc
.LFE3160:
	.size	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_, .-_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationFastLatin14getSecondariesEjj
	.type	_ZN6icu_6718CollationFastLatin14getSecondariesEjj, @function
_ZN6icu_6718CollationFastLatin14getSecondariesEjj:
.LFB3161:
	.cfi_startproc
	endbr64
	cmpl	$65535, %esi
	ja	.L172
	cmpl	$4095, %esi
	jbe	.L173
	andl	$992, %esi
	leal	32(%rsi), %eax
	movl	%eax, %edx
	sall	$16, %edx
	orb	$-64, %dl
	cmpl	$384, %esi
	cmovnb	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	movl	%esi, %eax
	andl	$61440, %eax
	je	.L175
	andl	$65012704, %esi
	leal	2097184(%rsi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	movzwl	%si, %esi
	movl	$12583104, %edx
	cmpl	%edi, %esi
	cmova	%edx, %eax
.L171:
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$192, %eax
	cmpl	%edi, %esi
	ja	.L171
	cmpl	$3072, %esi
	movl	$0, %eax
	cmovb	%esi, %eax
	ret
	.cfi_endproc
.LFE3161:
	.size	_ZN6icu_6718CollationFastLatin14getSecondariesEjj, .-_ZN6icu_6718CollationFastLatin14getSecondariesEjj
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationFastLatin8getCasesEjaj
	.type	_ZN6icu_6718CollationFastLatin8getCasesEjaj, @function
_ZN6icu_6718CollationFastLatin8getCasesEjaj:
.LFB3162:
	.cfi_startproc
	endbr64
	cmpl	$65535, %edx
	ja	.L179
	cmpl	$4095, %edx
	jbe	.L180
	movl	%edx, %eax
	andl	$24, %eax
	testb	%sil, %sil
	jne	.L178
	andl	$992, %edx
	movl	%eax, %ecx
	orl	$524288, %ecx
	cmpl	$383, %edx
	cmova	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	movl	%edx, %eax
	andl	$61440, %eax
	je	.L182
	movl	%edx, %eax
	testb	%sil, %sil
	je	.L183
	andl	$-67108864, %edx
	jne	.L183
	andl	$24, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	movzwl	%dx, %edx
	cmpl	%edi, %edx
	movl	$524296, %edx
	cmova	%edx, %eax
.L178:
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	movl	$8, %eax
	cmpl	%edi, %edx
	ja	.L178
	cmpl	$3072, %edx
	movl	$0, %eax
	cmovb	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	andl	$1572888, %eax
	ret
	.cfi_endproc
.LFE3162:
	.size	_ZN6icu_6718CollationFastLatin8getCasesEjaj, .-_ZN6icu_6718CollationFastLatin8getCasesEjaj
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationFastLatin13getTertiariesEjaj
	.type	_ZN6icu_6718CollationFastLatin13getTertiariesEjaj, @function
_ZN6icu_6718CollationFastLatin13getTertiariesEjaj:
.LFB3163:
	.cfi_startproc
	endbr64
	cmpl	$65535, %edx
	ja	.L187
	cmpl	$4095, %edx
	jbe	.L188
	movl	%edx, %ecx
	andl	$992, %ecx
	testb	%sil, %sil
	je	.L189
	andl	$31, %edx
	leal	32(%rdx), %eax
	movl	%eax, %edx
	orl	$2621440, %edx
	cmpl	$383, %ecx
	cmova	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	movl	%edx, %eax
	andl	$61440, %eax
	je	.L192
	movl	%edx, %eax
	andl	$458759, %edx
	andl	$2031647, %eax
	testb	%sil, %sil
	cmovne	%eax, %edx
	leal	2097184(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	movzwl	%dx, %ecx
	cmpl	%edi, %ecx
	jbe	.L186
	andl	$458759, %edx
	leal	2097184(%rdx), %eax
	movl	%eax, %edx
	orl	$524296, %edx
	testb	%sil, %sil
	cmovne	%edx, %eax
.L186:
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	cmpl	%edi, %edx
	jbe	.L191
	andl	$7, %edx
	leal	32(%rdx), %eax
	movl	%eax, %edx
	orl	$8, %edx
	testb	%sil, %sil
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	andl	$7, %edx
	leal	32(%rdx), %eax
	movl	%eax, %edx
	orl	$2097152, %edx
	cmpl	$383, %ecx
	cmova	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	cmpl	$3072, %edx
	movl	$0, %eax
	cmovb	%edx, %eax
	ret
	.cfi_endproc
.LFE3163:
	.size	_ZN6icu_6718CollationFastLatin13getTertiariesEjaj, .-_ZN6icu_6718CollationFastLatin13getTertiariesEjaj
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationFastLatin12compareUTF16EPKtS2_iPKDsiS4_i
	.type	_ZN6icu_6718CollationFastLatin12compareUTF16EPKtS2_iPKDsiS4_i, @function
_ZN6icu_6718CollationFastLatin12compareUTF16EPKtS2_iPKDsiS4_i:
.LFB3155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	shrl	$16, %r15d
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	%edx, -72(%rbp)
	andl	$2, %edx
	movq	%rcx, -80(%rbp)
	xorl	%ecx, %ecx
	movl	%r8d, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rdi), %eax
	movl	$0, -64(%rbp)
	movl	$0, -60(%rbp)
	leaq	(%rdi,%rax,2), %rbx
	leaq	-64(%rbp), %rax
	movl	%edx, -88(%rbp)
	movq	%rax, -96(%rbp)
.L197:
	testl	%ecx, %ecx
	jne	.L198
.L212:
	movslq	-64(%rbp), %rdx
	movl	$2, %ecx
	cmpl	-68(%rbp), %edx
	je	.L198
	leal	1(%rdx), %eax
	movl	%eax, -64(%rbp)
	movq	-80(%rbp), %rax
	movzwl	(%rax,%rdx,2), %esi
	movq	%rsi, %rax
	cmpl	$383, %esi
	jg	.L199
	movzwl	(%r14,%rsi,2), %ecx
	testl	%ecx, %ecx
	jne	.L198
	leal	-48(%rsi), %edx
	cmpl	$9, %edx
	ja	.L200
	movl	-88(%rbp), %edx
	testl	%edx, %edx
	jne	.L432
.L200:
	movzwl	(%rbx,%rax,2), %edx
	cmpl	$4095, %edx
	jbe	.L204
.L592:
	movl	%edx, %ecx
	andl	$64512, %ecx
	.p2align 4,,10
	.p2align 3
.L198:
	testl	%r12d, %r12d
	jne	.L207
	movq	%r14, %r11
	movl	%ecx, %r14d
	.p2align 4,,10
	.p2align 3
.L208:
	movl	-60(%rbp), %edx
	movl	16(%rbp), %r8d
.L245:
	cmpl	%r8d, %edx
	je	.L395
	movslq	%edx, %rax
	leal	1(%rdx), %edi
	movzwl	0(%r13,%rax,2), %ecx
	leaq	(%rax,%rax), %rsi
	movl	%edi, -60(%rbp)
	movq	%rcx, %rax
	cmpl	$383, %ecx
	jg	.L213
	movzwl	(%r11,%rcx,2), %r12d
	testl	%r12d, %r12d
	jne	.L552
	leal	-48(%rcx), %r9d
	cmpl	$9, %r9d
	ja	.L214
	movl	-88(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L432
.L214:
	movzwl	(%rbx,%rax,2), %eax
.L215:
	cmpl	$4095, %eax
	ja	.L586
	cmpl	%r15d, %eax
	ja	.L587
	leal	-1024(%rax), %r9d
	cmpl	$2047, %r9d
	ja	.L223
	cmpl	$2047, %eax
	ja	.L588
	movl	%r8d, %r10d
	shrl	$31, %r10d
	testl	%ecx, %ecx
	jne	.L226
	testb	%r10b, %r10b
	jne	.L589
.L226:
	andl	$1023, %eax
	addl	$448, %eax
	movl	%eax, %ecx
	cmpl	%r8d, %edi
	je	.L590
	leal	2(%rdx), %r9d
	movslq	%edi, %r12
	testq	%r13, %r13
	je	.L230
	movzwl	2(%r13,%rsi), %esi
	cmpl	$383, %esi
	jle	.L231
	leal	-8192(%rsi), %edx
	cmpl	$63, %edx
	jbe	.L591
	subl	$65534, %esi
	cmpl	$1, %esi
	ja	.L432
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L233:
	movzwl	(%rbx,%rax,2), %edx
	.p2align 4,,10
	.p2align 3
.L240:
	sarl	$9, %edx
	addl	%edx, %ecx
	movslq	%ecx, %r12
	movzwl	(%rbx,%r12,2), %edx
	movl	%edx, %r10d
	andl	$511, %r10d
	cmpl	%esi, %r10d
	jl	.L240
	je	.L241
	movzwl	(%rbx,%rax,2), %edx
	movq	%rax, %r12
.L229:
	sarl	$9, %edx
	cmpl	$1, %edx
	je	.L432
	leaq	1(%r12), %rax
	movzwl	(%rbx,%rax,2), %r12d
	leaq	(%rax,%rax), %rcx
	cmpl	$2, %edx
	je	.L225
	movzwl	2(%rbx,%rcx), %eax
	sall	$16, %eax
	orl	%eax, %r12d
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L199:
	leal	-8192(%rsi), %eax
	cmpl	$63, %eax
	ja	.L203
	cltq
	movzwl	768(%rbx,%rax,2), %edx
	cmpl	$4095, %edx
	ja	.L592
.L204:
	cmpl	%edx, %r15d
	jb	.L593
.L206:
	subq	$8, %rsp
	leaq	-68(%rbp), %rax
	movq	-80(%rbp), %rcx
	movq	-96(%rbp), %r9
	pushq	%rax
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_
	popq	%r10
	popq	%r11
	movl	%eax, %ecx
	cmpl	$1, %eax
	je	.L432
	testb	$-16, %ah
	jne	.L594
	movzwl	%ax, %eax
	cmpl	%eax, %r15d
	jb	.L595
	cmpl	$3071, %eax
	jbe	.L197
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L203:
	cmpl	$65534, %esi
	je	.L392
	cmpl	$65535, %esi
	je	.L393
	movl	$1, %edx
	cmpl	%edx, %r15d
	jnb	.L206
.L593:
	movl	%edx, %ecx
	andl	$65528, %ecx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L213:
	leal	-8192(%rcx), %eax
	cmpl	$63, %eax
	ja	.L216
	cltq
	movzwl	768(%rbx,%rax,2), %eax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L216:
	movl	%ecx, %esi
	movl	%r14d, %ecx
	movq	%r11, %r14
	cmpl	$65534, %esi
	je	.L217
	cmpl	$65535, %esi
	je	.L596
	testl	%r15d, %r15d
	je	.L597
.L432:
	movl	$-2, %eax
.L196:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L598
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	cmpl	$1, %eax
	je	.L432
	movl	%eax, %r12d
.L244:
	cmpl	$3071, %eax
	ja	.L599
.L243:
	testl	%r12d, %r12d
	je	.L208
	.p2align 4,,10
	.p2align 3
.L552:
	movl	%r14d, %ecx
	movq	%r11, %r14
	.p2align 4,,10
	.p2align 3
.L207:
	cmpl	%ecx, %r12d
	je	.L600
.L246:
	movzwl	%cx, %eax
	movzwl	%r12w, %edx
	cmpl	%edx, %eax
	jne	.L577
	cmpl	$2, %ecx
	je	.L247
	shrl	$16, %ecx
	shrl	$16, %r12d
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L588:
	andl	$1023, %eax
	movzwl	898(%rbx,%rax,2), %r12d
	movzwl	896(%rbx,%rax,2), %eax
	sall	$16, %r12d
	orl	%eax, %r12d
.L225:
	cmpl	$1, %r12d
	je	.L432
	testl	$61440, %r12d
	je	.L242
	andl	$-67044352, %r12d
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L242:
	movzwl	%r12w, %eax
	cmpl	%eax, %r15d
	jnb	.L244
	andl	$-458760, %r12d
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L230:
	movzbl	(%r12), %esi
	cmpl	$127, %esi
	jle	.L231
	leal	3(%rdx), %r12d
	movl	%r12d, -100(%rbp)
	leal	-194(%rsi), %r12d
	movl	%r12d, -104(%rbp)
	cmpl	$3, %r12d
	ja	.L234
	cmpl	%r8d, %r9d
	je	.L234
	movslq	%r9d, %r12
	movzbl	(%r12), %r12d
	movb	%r12b, -105(%rbp)
	addl	$-128, %r12d
	cmpb	$63, %r12b
	ja	.L234
	movl	-104(%rbp), %esi
	movzbl	-105(%rbp), %edx
	movl	-100(%rbp), %r9d
	sall	$6, %esi
	addl	%edx, %esi
	.p2align 4,,10
	.p2align 3
.L231:
	testl	%esi, %esi
	jne	.L233
	testl	%r8d, %r8d
	jns	.L233
	movl	%edi, 16(%rbp)
	movl	%edi, %r8d
	movl	$-1, %esi
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L599:
	movl	%edi, %edx
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L590:
	movl	%eax, %r12d
	movzwl	(%rbx,%r12,2), %edx
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L591:
	subl	$7808, %esi
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L595:
	andl	$-458760, %ecx
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L234:
	cmpl	%r8d, -100(%rbp)
	jl	.L434
	testb	%r10b, %r10b
	je	.L432
.L434:
	cmpl	$226, %esi
	je	.L601
	cmpl	$239, %esi
	jne	.L432
	movslq	%r9d, %r9
	cmpb	$-65, (%r9)
	jne	.L432
	movslq	-100(%rbp), %rsi
	movzbl	(%rsi), %esi
	cmpb	$-66, %sil
	je	.L576
	cmpb	$-65, %sil
	jne	.L432
.L576:
	leal	4(%rdx), %r9d
	movl	$-1, %esi
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L241:
	movl	%r9d, -60(%rbp)
	movl	%r9d, %edi
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L596:
	movl	$64512, %r12d
	cmpl	%ecx, %r12d
	jne	.L246
.L600:
	cmpl	$2, %r12d
	je	.L247
	xorl	%r12d, %r12d
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L594:
	andl	$-67044352, %ecx
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L589:
	movl	%edx, 16(%rbp)
	cmpl	$1, %r15d
	jbe	.L208
.L395:
	movl	%r14d, %ecx
	movl	$2, %r12d
	movq	%r11, %r14
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L217:
	cmpl	$3, %r15d
	sbbl	%r12d, %r12d
	notl	%r12d
	andl	$3, %r12d
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%r13, %r14
	movl	-72(%rbp), %r13d
	sarl	$12, %r13d
	movl	%r13d, %eax
	andl	$15, %eax
	movl	%eax, -88(%rbp)
	jne	.L602
.L252:
	testl	$1024, -72(%rbp)
	je	.L288
	movl	$0, -60(%rbp)
	movq	%rbx, %rdi
	movl	-88(%rbp), %ebx
	xorl	%r10d, %r10d
	movl	$0, -64(%rbp)
	xorl	%r12d, %r12d
.L289:
	testl	%r12d, %r12d
	je	.L603
.L290:
	testl	%r10d, %r10d
	jne	.L604
.L293:
	leaq	-60(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L317:
	movslq	-60(%rbp), %rdx
	movl	$2, %eax
	cmpl	16(%rbp), %edx
	je	.L295
	movzwl	(%r14,%rdx,2), %esi
	leal	1(%rdx), %eax
	movl	%eax, -60(%rbp)
	cmpl	$383, %esi
	jg	.L309
	movzwl	(%rdi,%rsi,2), %edx
.L310:
	cmpl	$3071, %edx
	jbe	.L312
.L314:
	cmpl	$4095, %edx
	jbe	.L316
	movl	%edx, %eax
	andl	$24, %eax
	testl	%ebx, %ebx
	je	.L313
	movl	%eax, %ecx
	andl	$992, %edx
	orl	$524288, %ecx
	cmpl	$383, %edx
	cmova	%ecx, %eax
.L313:
	testl	%eax, %eax
	je	.L317
	.p2align 4,,10
	.p2align 3
.L295:
	cmpl	%r12d, %eax
	je	.L605
	movzwl	%r12w, %edx
	movzwl	%ax, %r10d
	cmpl	%r10d, %edx
	jne	.L294
	cmpl	$2, %r12d
	je	.L570
	shrl	$16, %eax
	shrl	$16, %r12d
	movl	%eax, %r10d
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L393:
	movl	$64512, %ecx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L602:
	movl	$0, -60(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	movl	$0, -64(%rbp)
	movl	%eax, %ebx
.L253:
	testl	%ebx, %ebx
	je	.L606
.L254:
	testl	%r12d, %r12d
	jne	.L262
	.p2align 4,,10
	.p2align 3
.L279:
	movslq	-60(%rbp), %rax
	movl	$2, %r12d
	cmpl	16(%rbp), %eax
	je	.L262
	movzwl	(%r14,%rax,2), %esi
	leal	1(%rax), %edx
	movl	%edx, -60(%rbp)
	cmpl	$383, %esi
	jg	.L270
	movzwl	(%rdi,%rsi,2), %edx
.L271:
	cmpl	$4095, %edx
	ja	.L607
.L273:
	movl	$192, %r12d
	cmpl	%edx, %r15d
	jb	.L262
	subq	$8, %rsp
	leaq	16(%rbp), %rax
	leaq	-60(%rbp), %r9
	xorl	%r8d, %r8d
	pushq	%rax
	movq	%r14, %rcx
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_
	popq	%r9
	popq	%r10
	cmpl	$65535, %eax
	ja	.L276
	cmpl	$4095, %eax
	jbe	.L277
	andl	$992, %eax
	movl	%eax, %ecx
	leal	32(%rax), %eax
	movl	%eax, %edx
	sall	$16, %edx
	orb	$-64, %dl
	cmpl	$384, %ecx
	cmovnb	%edx, %eax
.L278:
	testl	%eax, %eax
	je	.L279
	movl	%eax, %r12d
	.p2align 4,,10
	.p2align 3
.L262:
	cmpl	%ebx, %r12d
	je	.L608
	movzwl	%bx, %eax
	movzwl	%r12w, %edx
	cmpl	%edx, %eax
	jne	.L609
	cmpl	$2, %ebx
	je	.L563
	shrl	$16, %ebx
	shrl	$16, %r12d
	jmp	.L253
.L601:
	movslq	%r9d, %r9
	cmpb	$-128, (%r9)
	jne	.L432
	movslq	-100(%rbp), %rsi
	movzbl	(%rsi), %esi
	leal	-128(%rsi), %r9d
	cmpb	$63, %r9b
	ja	.L432
	addl	$256, %esi
	leal	4(%rdx), %r9d
	jmp	.L233
.L597:
	xorl	%r12d, %r12d
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L309:
	leal	-8192(%rsi), %eax
	cmpl	$63, %eax
	jbe	.L610
	cmpl	$65534, %esi
	je	.L413
	cmpl	$65535, %esi
	je	.L414
	movl	$1, %edx
.L312:
	subq	$8, %rsp
	leaq	16(%rbp), %rax
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	pushq	%rax
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_
	movl	%eax, %edx
	popq	%rax
	popq	%rcx
	cmpl	$65535, %edx
	jbe	.L314
	testb	$-16, %dh
	jne	.L611
	movzwl	%dx, %edx
	movl	$524296, %eax
	cmpl	%edx, %r15d
	jnb	.L317
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L270:
	leal	-8192(%rsi), %eax
	cmpl	$63, %eax
	ja	.L272
	cltq
	movzwl	768(%rdi,%rax,2), %edx
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L276:
	testb	$-16, %ah
	je	.L280
	andl	$65012704, %eax
	addl	$2097184, %eax
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L316:
	movl	$8, %eax
	cmpl	%edx, %r15d
	jb	.L295
	movl	%edx, %eax
	cmpl	$3071, %edx
	jbe	.L313
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L272:
	cmpl	$65534, %esi
	je	.L405
	cmpl	$65535, %esi
	je	.L406
	movl	$1, %edx
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L277:
	cmpl	%eax, %r15d
	jb	.L262
	cmpl	$3071, %eax
	ja	.L279
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L280:
	movzwl	%ax, %eax
	movl	$12583104, %r12d
	cmpl	%eax, %r15d
	jnb	.L279
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L610:
	cltq
	movzwl	768(%rdi,%rax,2), %edx
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L586:
	andl	$64512, %eax
	movl	%r14d, %ecx
	movq	%r11, %r14
	movl	%eax, %r12d
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L587:
	andl	$65528, %eax
	movl	%r14d, %ecx
	movq	%r11, %r14
	movl	%eax, %r12d
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L611:
	movl	%edx, %ecx
	movl	%edx, %eax
	andl	$1572888, %edx
	andl	$-67108864, %ecx
	andl	$24, %eax
	orl	%ebx, %ecx
	cmovne	%edx, %eax
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L392:
	movl	$3, %edx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L603:
	movslq	-64(%rbp), %rax
	cmpl	%eax, -68(%rbp)
	je	.L612
.L323:
	movq	%r14, %r12
	leaq	-64(%rbp), %r9
	movl	%r13d, %r14d
	movl	%ebx, %r13d
	movl	%r10d, %ebx
.L292:
	movq	-80(%rbp), %rsi
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movzwl	(%rsi,%rax,2), %esi
	cmpl	$383, %esi
	jg	.L297
	movzwl	(%rdi,%rsi,2), %edx
.L298:
	cmpl	$3071, %edx
	jbe	.L300
.L302:
	cmpl	$4095, %edx
	jbe	.L304
	movl	%ebx, %r10d
	movl	%r13d, %ebx
	movl	%r14d, %r13d
	movq	%r12, %r14
	movl	%edx, %r12d
	andl	$24, %r12d
	testl	%ebx, %ebx
	je	.L289
	movl	%r12d, %eax
	andl	$992, %edx
	orl	$524288, %eax
	cmpl	$383, %edx
	cmova	%eax, %r12d
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L606:
	movslq	-64(%rbp), %rax
	cmpl	%eax, -68(%rbp)
	je	.L613
.L285:
	movq	-80(%rbp), %rsi
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movzwl	(%rsi,%rax,2), %esi
	cmpl	$383, %esi
	jg	.L256
	movzwl	(%rdi,%rsi,2), %edx
.L257:
	cmpl	$4095, %edx
	jbe	.L259
	andl	$992, %edx
	leal	32(%rdx), %ebx
	movl	%ebx, %eax
	sall	$16, %eax
	orb	$-64, %al
	cmpl	$384, %edx
	cmovnb	%eax, %ebx
	jmp	.L254
.L570:
	movq	%rdi, %rbx
.L288:
	andl	$14, %r13d
	je	.L351
	movl	-72(%rbp), %eax
	movl	$0, -60(%rbp)
	leaq	-60(%rbp), %r12
	movl	$0, -64(%rbp)
	andl	$1536, %eax
	cmpl	$512, %eax
	sete	-96(%rbp)
	xorl	%r10d, %r10d
	xorl	%r13d, %r13d
.L327:
	testl	%r13d, %r13d
	je	.L614
	testl	%r10d, %r10d
	jne	.L333
	movq	%r12, %r9
	movzbl	-96(%rbp), %r12d
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L615:
	movzwl	(%rbx,%rsi,2), %edx
.L341:
	cmpl	$3071, %edx
	ja	.L344
.L343:
	subq	$8, %rsp
	leaq	16(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	pushq	%rax
	movq	%rbx, %rdi
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_
	popq	%r8
	popq	%r10
	movl	%eax, %edx
.L344:
	movzbl	%r12b, %esi
	movl	%r15d, %edi
	call	_ZN6icu_6718CollationFastLatin13getTertiariesEjaj
	testl	%eax, %eax
	jne	.L572
.L345:
	movslq	-60(%rbp), %rdx
	movl	$2, %eax
	cmpl	16(%rbp), %edx
	je	.L572
	movzwl	(%r14,%rdx,2), %esi
	leal	1(%rdx), %eax
	movl	%eax, -60(%rbp)
	cmpl	$383, %esi
	jle	.L615
	leal	-8192(%rsi), %eax
	cmpl	$63, %eax
	jbe	.L616
	cmpl	$65534, %esi
	je	.L419
	movl	$64680, %edx
	cmpl	$65535, %esi
	je	.L344
	movl	$1, %edx
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L572:
	movl	%eax, %r10d
	movq	%r9, %r12
.L333:
	cmpl	%r13d, %r10d
	je	.L617
	movzwl	%r13w, %eax
	movzwl	%r10w, %edx
	cmpl	%edx, %eax
	jne	.L618
	cmpl	$2, %r13d
	je	.L331
	shrl	$16, %r13d
	shrl	$16, %r10d
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L616:
	cltq
	movzwl	768(%rbx,%rax,2), %edx
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L614:
	movslq	-64(%rbp), %rax
	cmpl	-68(%rbp), %eax
	je	.L619
.L329:
	movq	-80(%rbp), %rdi
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movzwl	(%rdi,%rax,2), %esi
	cmpl	$383, %esi
	jg	.L334
	movzwl	(%rbx,%rsi,2), %edx
.L335:
	cmpl	$3071, %edx
	ja	.L338
.L337:
	subq	$8, %rsp
	leaq	-68(%rbp), %rax
	movq	-80(%rbp), %rcx
	leaq	-64(%rbp), %r9
	pushq	%rax
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movl	%r10d, -100(%rbp)
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_
	movl	-100(%rbp), %r10d
	popq	%r11
	movl	%eax, %edx
	popq	%r13
.L338:
	movzbl	-96(%rbp), %esi
	movl	%r15d, %edi
	movl	%r10d, -100(%rbp)
	call	_ZN6icu_6718CollationFastLatin13getTertiariesEjaj
	movl	-100(%rbp), %r10d
	movl	%eax, %r13d
	jmp	.L327
.L414:
	movl	$8, %eax
	jmp	.L295
.L297:
	leal	-8192(%rsi), %eax
	cmpl	$63, %eax
	jbe	.L620
	cmpl	$65534, %esi
	je	.L411
	cmpl	$65535, %esi
	je	.L412
	movl	$1, %edx
.L300:
	subq	$8, %rsp
	leaq	-68(%rbp), %rax
	movq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	pushq	%rax
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_
	popq	%rsi
	popq	%r8
	movl	%eax, %edx
	cmpl	$65535, %eax
	jbe	.L302
	testb	$-16, %ah
	jne	.L621
	movzwl	%ax, %edx
	movl	$524296, %ecx
	cmpl	%edx, %r15d
	jb	.L564
.L305:
	movslq	-64(%rbp), %rax
	movl	$2, %ecx
	cmpl	-68(%rbp), %eax
	jne	.L292
.L564:
	movl	%ebx, %r10d
	movl	%r13d, %ebx
	movl	%r14d, %r13d
	movq	%r12, %r14
	movl	%ecx, %r12d
	jmp	.L290
.L258:
	cmpl	$65534, %esi
	je	.L401
	cmpl	$65535, %esi
	je	.L403
	movl	$1, %edx
.L259:
	movl	$192, %ebx
	cmpl	%edx, %r15d
	jb	.L254
	subq	$8, %rsp
	leaq	-68(%rbp), %rax
	movq	-80(%rbp), %rcx
	leaq	-64(%rbp), %r9
	pushq	%rax
	xorl	%r8d, %r8d
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_
	popq	%r11
	movl	%eax, %ebx
	popq	%rax
	cmpl	$65535, %ebx
	ja	.L264
	cmpl	$4095, %ebx
	jbe	.L265
	andl	$992, %ebx
	movl	%ebx, %edx
	leal	32(%rbx), %ebx
	movl	%ebx, %eax
	sall	$16, %eax
	orb	$-64, %al
	cmpl	$384, %edx
	cmovnb	%eax, %ebx
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L413:
	movl	$3, %edx
	jmp	.L312
.L334:
	leal	-8192(%rsi), %eax
	cmpl	$63, %eax
	jbe	.L622
	cmpl	$65534, %esi
	je	.L417
	movl	$64680, %edx
	cmpl	$65535, %esi
	je	.L338
	movl	$1, %edx
	jmp	.L337
.L419:
	movl	$3, %edx
	jmp	.L343
.L406:
	movl	$192, %r12d
	jmp	.L262
.L256:
	leal	-8192(%rsi), %eax
	cmpl	$63, %eax
	ja	.L258
	cltq
	movzwl	768(%rdi,%rax,2), %edx
	jmp	.L257
.L619:
	testl	%r10d, %r10d
	jne	.L623
.L430:
	movq	%r12, %r9
	movl	$2, %r13d
	movzbl	-96(%rbp), %r12d
	jmp	.L345
.L613:
	testl	%r12d, %r12d
	jne	.L624
.L408:
	movl	$2, %ebx
	jmp	.L279
.L612:
	testl	%r10d, %r10d
	jne	.L625
.L431:
	movl	$2, %r12d
	jmp	.L293
.L607:
	andl	$992, %edx
	leal	32(%rdx), %r12d
	cmpl	$383, %edx
	jbe	.L262
	sall	$16, %r12d
	orb	$-64, %r12b
	jmp	.L262
.L351:
	xorl	%eax, %eax
	jmp	.L196
.L405:
	movl	$3, %edx
	jmp	.L273
.L304:
	movl	$8, %ecx
	cmpl	%edx, %r15d
	jb	.L564
	cmpl	$3071, %edx
	ja	.L305
	movl	%ebx, %r10d
	movl	%r13d, %ebx
	movl	%r14d, %r13d
	movq	%r12, %r14
	movl	%edx, %r12d
	jmp	.L289
.L623:
	cmpl	$2, %r10d
	je	.L331
	movl	-72(%rbp), %eax
	andl	$1792, %eax
	cmpl	$768, %eax
	je	.L433
	movl	$2, %eax
.L349:
	cmpl	%r10d, %eax
.L577:
	jnb	.L287
.L250:
	movl	$-1, %eax
	jmp	.L196
.L605:
	cmpl	$2, %eax
	je	.L570
	movslq	-64(%rbp), %rax
	xorl	%r10d, %r10d
	cmpl	-68(%rbp), %eax
	jne	.L323
	jmp	.L431
.L617:
	cmpl	$2, %r10d
	je	.L331
	movslq	-64(%rbp), %rax
	xorl	%r10d, %r10d
	cmpl	%eax, -68(%rbp)
	jne	.L329
	jmp	.L430
.L608:
	cmpl	$2, %r12d
	je	.L563
	movslq	-64(%rbp), %rax
	xorl	%r12d, %r12d
	cmpl	-68(%rbp), %eax
	jne	.L285
	jmp	.L408
.L620:
	cltq
	movzwl	768(%rdi,%rax,2), %edx
	jmp	.L298
.L622:
	cltq
	movzwl	768(%rbx,%rax,2), %edx
	jmp	.L335
.L609:
	movl	%edx, %r12d
.L284:
	testl	$2048, -72(%rbp)
	jne	.L432
	cmpl	%eax, %r12d
	ja	.L250
.L287:
	movl	$1, %eax
	jmp	.L196
.L331:
	cmpl	$2, -88(%rbp)
	je	.L351
	leaq	-64(%rbp), %rax
	xorl	%r13d, %r13d
	leaq	-60(%rbp), %r12
	xorl	%r10d, %r10d
	movl	$0, -60(%rbp)
	movl	$0, -64(%rbp)
	movq	%rax, -88(%rbp)
.L352:
	testl	%r13d, %r13d
	jne	.L626
.L387:
	movslq	-64(%rbp), %rax
	cmpl	-68(%rbp), %eax
	je	.L627
	movq	-80(%rbp), %rdi
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movzwl	(%rdi,%rax,2), %esi
	cmpl	$383, %esi
	jg	.L363
	movzwl	(%rbx,%rsi,2), %r13d
.L364:
	cmpl	$3071, %r13d
	jbe	.L366
.L368:
	cmpl	$4095, %r13d
	jbe	.L370
	andl	$992, %r13d
	cmpl	$383, %r13d
	jbe	.L371
.L585:
	testl	%r10d, %r10d
	jne	.L376
	movl	$-67044352, %r13d
	.p2align 4,,10
	.p2align 3
.L362:
	movslq	-60(%rbp), %rax
	movl	$2, %r10d
	cmpl	16(%rbp), %eax
	je	.L361
	movzwl	(%r14,%rax,2), %esi
	leal	1(%rax), %edx
	movl	%edx, -60(%rbp)
	cmpl	$383, %esi
	jg	.L378
	movzwl	(%rbx,%rsi,2), %edx
.L379:
	cmpl	$3071, %edx
	jbe	.L381
.L382:
	cmpl	$4095, %edx
	jbe	.L384
	andl	$992, %edx
	cmpl	$384, %edx
	sbbl	%r10d, %r10d
	andl	$67108864, %r10d
	subl	$67044352, %r10d
.L361:
	cmpl	%r13d, %r10d
	je	.L628
	movzwl	%r13w, %eax
	movzwl	%r10w, %edx
	cmpl	%edx, %eax
	jne	.L629
	cmpl	$2, %r13d
	je	.L351
.L389:
	shrl	$16, %r13d
	shrl	$16, %r10d
	testl	%r13d, %r13d
	je	.L387
.L626:
	testl	%r10d, %r10d
	je	.L362
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L378:
	leal	-8192(%rsi), %eax
	cmpl	$63, %eax
	jbe	.L630
	cmpl	$65534, %esi
	je	.L427
	movl	$64512, %r10d
	cmpl	$65535, %esi
	je	.L361
	movl	$1, %edx
.L381:
	subq	$8, %rsp
	leaq	16(%rbp), %rax
	movq	%r14, %rcx
	movq	%r12, %r9
	pushq	%rax
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_
	movl	%eax, %edx
	popq	%rax
	popq	%rcx
	cmpl	$65535, %edx
	jbe	.L382
	movzwl	%dx, %eax
	movl	$-67044352, %r10d
	cmpl	%eax, %r15d
	jb	.L361
	andl	$-458760, %edx
.L385:
	testl	%edx, %edx
	je	.L362
	movl	%edx, %r10d
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L384:
	movl	$64512, %r10d
	cmpl	%edx, %r15d
	jb	.L361
	movl	%edx, %eax
	andl	$65528, %eax
	cmpl	$3071, %edx
	cmova	%eax, %edx
	jmp	.L385
.L630:
	cltq
	movzwl	768(%rbx,%rax,2), %edx
	jmp	.L379
.L563:
	movq	%rdi, %rbx
	jmp	.L252
.L621:
	movl	%ebx, %r10d
	andl	$-67108864, %eax
	movl	%r13d, %ebx
	movl	%r14d, %r13d
	movq	%r12, %r14
	movl	%edx, %r12d
	andl	$1572888, %edx
	andl	$24, %r12d
	orl	%ebx, %eax
	cmovne	%edx, %r12d
	jmp	.L289
.L424:
	movl	$3, %r13d
.L366:
	subq	$8, %rsp
	leaq	-68(%rbp), %rax
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rcx
	pushq	%rax
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movq	%rbx, %rdi
	movl	%r10d, -96(%rbp)
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_
	popq	%rsi
	movl	-96(%rbp), %r10d
	cmpl	$65535, %eax
	movl	%eax, %r13d
	popq	%rdi
	jbe	.L368
	movzwl	%ax, %eax
	andl	$-458760, %r13d
	cmpl	%eax, %r15d
	jnb	.L352
	jmp	.L585
.L627:
	testl	%r10d, %r10d
	jne	.L631
	movl	$2, %r13d
	jmp	.L362
.L363:
	leal	-8192(%rsi), %eax
	cmpl	$63, %eax
	jbe	.L632
	cmpl	$65534, %esi
	je	.L424
	movl	$1, %r13d
	cmpl	$65535, %esi
	jne	.L366
.L371:
	testl	%r10d, %r10d
	jne	.L579
	movl	$64512, %r13d
	jmp	.L362
.L427:
	movl	$3, %edx
	jmp	.L381
.L264:
	testb	$-16, %bh
	je	.L268
	andl	$65012704, %ebx
	addl	$2097184, %ebx
	jmp	.L253
.L412:
	movl	%ebx, %r10d
	movl	%r13d, %ebx
	movl	%r14d, %r13d
	movq	%r12, %r14
	movl	$8, %r12d
	jmp	.L290
.L403:
	movl	$192, %ebx
	jmp	.L254
.L265:
	cmpl	%ebx, %r15d
	jb	.L403
	cmpl	$3071, %ebx
	jbe	.L253
.L267:
	movslq	-64(%rbp), %rax
	movl	$2, %ebx
	cmpl	-68(%rbp), %eax
	jne	.L285
	jmp	.L254
.L625:
	movq	%rdi, %rbx
	cmpl	$2, %r10d
	je	.L288
	movl	$2, %edx
.L294:
	testl	$256, -72(%rbp)
	jne	.L325
	cmpl	%edx, %r10d
	ja	.L250
	jmp	.L287
.L411:
	movl	$3, %edx
	jmp	.L300
.L268:
	movzwl	%bx, %eax
	movl	$12583104, %ebx
	cmpl	%eax, %r15d
	jnb	.L267
	jmp	.L254
.L370:
	cmpl	%r13d, %r15d
	jb	.L371
	cmpl	$3071, %r13d
	jbe	.L352
	andl	$65528, %r13d
	jmp	.L352
.L417:
	movl	$3, %edx
	jmp	.L337
.L628:
	cmpl	$2, %r10d
	je	.L351
.L355:
	xorl	%r10d, %r10d
	jmp	.L387
.L632:
	cltq
	movzwl	768(%rbx,%rax,2), %r13d
	jmp	.L364
.L401:
	movl	$3, %edx
	jmp	.L259
.L325:
	cmpl	%r10d, %edx
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
	jmp	.L196
.L618:
	movl	-72(%rbp), %ecx
	andl	$1792, %ecx
	cmpl	$768, %ecx
	je	.L633
	movl	%edx, %r10d
	jmp	.L349
.L633:
	andl	$65532, %r13d
	je	.L350
	xorl	$24, %eax
.L350:
	movl	%r10d, %ecx
	movl	%edx, %r10d
	andl	$65532, %ecx
	je	.L349
	xorl	$24, %r10d
	jmp	.L349
.L629:
	movl	%edx, %r13d
.L388:
	cmpl	%eax, %r13d
	ja	.L250
	jmp	.L287
.L433:
	movl	%r10d, %edx
	movl	$2, %eax
	jmp	.L350
.L376:
	cmpl	$64512, %r10d
	jne	.L429
	movl	$-67044352, %r13d
	jmp	.L389
.L598:
	call	__stack_chk_fail@PLT
.L579:
	cmpl	$64512, %r10d
	je	.L355
.L429:
	movl	%r10d, %r13d
	movl	$64512, %eax
	jmp	.L388
.L604:
	movl	%r10d, %eax
	jmp	.L295
.L624:
	movq	%rdi, %rbx
	cmpl	$2, %r12d
	je	.L252
	movl	$2, %eax
	jmp	.L284
.L631:
	movl	%r10d, %r13d
	cmpl	$2, %r10d
	je	.L351
	movl	$2, %eax
	jmp	.L388
	.cfi_endproc
.LFE3155:
	.size	_ZN6icu_6718CollationFastLatin12compareUTF16EPKtS2_iPKDsiS4_i, .-_ZN6icu_6718CollationFastLatin12compareUTF16EPKtS2_iPKDsiS4_i
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationFastLatin11compareUTF8EPKtS2_iPKhiS4_i
	.type	_ZN6icu_6718CollationFastLatin11compareUTF8EPKtS2_iPKhiS4_i, @function
_ZN6icu_6718CollationFastLatin11compareUTF8EPKtS2_iPKhiS4_i:
.LFB3156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	shrl	$16, %r15d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r14d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	%edx, -108(%rbp)
	andl	$2, %edx
	movq	%rsi, -80(%rbp)
	movq	%rcx, -104(%rbp)
	movl	%r8d, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rdi), %eax
	movl	$0, -64(%rbp)
	movl	$0, -60(%rbp)
	movl	%edx, -96(%rbp)
	leaq	(%rdi,%rax,2), %rbx
	movl	$0, -88(%rbp)
.L635:
	testl	%r13d, %r13d
	jne	.L636
.L651:
	movl	-64(%rbp), %edx
	movl	-68(%rbp), %ecx
	movl	$2, %r13d
	cmpl	%ecx, %edx
	je	.L636
	movq	-104(%rbp), %rsi
	movslq	%edx, %rax
	leal	1(%rdx), %edi
	movl	%edi, -64(%rbp)
	movzbl	(%rsi,%rax), %esi
	movq	%rsi, %rax
	cmpl	$127, %esi
	jg	.L637
	movq	-80(%rbp), %rcx
	movzwl	(%rcx,%rsi,2), %r13d
	testl	%r13d, %r13d
	jne	.L636
	leal	-48(%rsi), %edx
	cmpl	$9, %edx
	jbe	.L1144
.L1092:
	movzwl	(%rbx,%rax,2), %r13d
.L640:
	cmpl	$4095, %r13d
	jbe	.L643
	andl	$64512, %r13d
.L636:
	movl	-88(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L652
	movl	-60(%rbp), %edx
	movl	16(%rbp), %esi
	xorl	%r14d, %r14d
	xorl	%edi, %edi
.L701:
	cmpl	%esi, %edx
	je	.L1145
.L656:
	movslq	%edx, %rax
	leal	1(%rdx), %ecx
	movzbl	(%r12,%rax), %edi
	movq	%rdi, %rax
	cmpl	$127, %edi
	jg	.L659
	movq	-80(%rbp), %rdx
	movzwl	(%rdx,%rdi,2), %edx
	testl	%edx, %edx
	jne	.L1146
	leal	-48(%rdi), %edx
	cmpl	$9, %edx
	ja	.L662
	movl	-96(%rbp), %edx
	testl	%edx, %edx
	jne	.L858
.L662:
	movzwl	(%rbx,%rax,2), %eax
.L664:
	cmpl	$4095, %eax
	ja	.L1147
.L678:
	cmpl	%r15d, %eax
	ja	.L1148
	leal	-1024(%rax), %edx
	cmpl	$2047, %edx
	ja	.L682
	cmpl	$2047, %eax
	ja	.L1149
	movl	%esi, %r8d
	shrl	$31, %r8d
	testl	%edi, %edi
	sete	%dl
	movl	%edx, %edi
	andb	%r8b, %dil
	jne	.L1150
	andl	$1023, %eax
	leal	448(%rax), %r11d
	movl	%r11d, %eax
	cmpl	%esi, %ecx
	je	.L1151
	movslq	%ecx, %rdi
	leal	1(%rcx), %edx
	movzbl	(%r12,%rdi), %edi
	cmpl	$127, %edi
	jle	.L689
	leal	-194(%rdi), %r9d
	leal	2(%rcx), %r10d
	movl	%r9d, -72(%rbp)
	cmpl	$3, %r9d
	ja	.L690
	cmpl	%esi, %edx
	je	.L690
	movslq	%edx, %r9
	movzbl	(%r12,%r9), %r9d
	movb	%r9b, -109(%rbp)
	addl	$-128, %r9d
	cmpb	$63, %r9b
	jbe	.L1152
.L690:
	cmpl	%r10d, %esi
	jg	.L900
	testb	%r8b, %r8b
	je	.L858
.L900:
	cmpl	$226, %edi
	je	.L1153
	cmpl	$239, %edi
	jne	.L858
	movslq	%edx, %rdx
	cmpb	$-65, (%r12,%rdx)
	jne	.L858
	movslq	%r10d, %rdx
	movzbl	(%r12,%rdx), %edx
	cmpb	$-66, %dl
	je	.L696
	cmpb	$-65, %dl
	jne	.L858
.L696:
	leal	3(%rcx), %edx
	movl	$-1, %edi
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L659:
	leal	-194(%rdi), %eax
	leal	2(%rdx), %r8d
	cmpl	$3, %eax
	ja	.L665
	cmpl	%esi, %ecx
	je	.L665
	movslq	%ecx, %r9
	movzbl	(%r12,%r9), %r9d
	leal	-128(%r9), %r10d
	cmpb	$63, %r10b
	jbe	.L666
.L665:
	cmpl	%r8d, %esi
	jg	.L899
	testl	%esi, %esi
	js	.L899
.L670:
	movl	%ecx, -60(%rbp)
	testb	%r14b, %r14b
	je	.L860
	movl	%esi, 16(%rbp)
.L860:
	testl	%r15d, %r15d
	je	.L652
.L858:
	movl	$-2, %eax
.L634:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1154
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L899:
	.cfi_restore_state
	movslq	%ecx, %rcx
	movslq	%r8d, %r8
	movzbl	(%r12,%rcx), %eax
	movzbl	(%r12,%r8), %r8d
	leal	3(%rdx), %ecx
	cmpl	$226, %edi
	jne	.L669
	cmpb	$-128, %al
	jne	.L669
	leal	-128(%r8), %eax
	cmpb	$63, %al
	ja	.L670
	movzwl	512(%rbx,%r8,2), %eax
	cmpl	$4095, %eax
	jbe	.L678
	.p2align 4,,10
	.p2align 3
.L1147:
	movl	%ecx, -60(%rbp)
	testb	%r14b, %r14b
	je	.L679
	movl	%esi, 16(%rbp)
.L679:
	andl	$64512, %eax
.L1127:
	movl	%eax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L652:
	cmpl	%r13d, -88(%rbp)
	je	.L1155
.L702:
	movzwl	-88(%rbp), %edx
	movzwl	%r13w, %eax
	cmpl	%edx, %eax
	jne	.L1156
	cmpl	$2, %r13d
	je	.L703
	shrl	$16, %r13d
	shrl	$16, -88(%rbp)
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L1149:
	andl	$1023, %eax
	movzwl	898(%rbx,%rax,2), %edx
	movzwl	896(%rbx,%rax,2), %eax
	sall	$16, %edx
	orl	%edx, %eax
	movl	%ecx, %edx
.L684:
	cmpl	$1, %eax
	je	.L858
	testb	$-16, %ah
	jne	.L1157
	movzwl	%ax, %ecx
	cmpl	%ecx, %r15d
	jb	.L1158
.L700:
	movl	$1, %edi
	cmpl	$3071, %ecx
	ja	.L701
.L699:
	testl	%eax, %eax
	jne	.L1159
.L654:
	movl	$1, %edi
	cmpl	%esi, %edx
	jne	.L656
.L1145:
	testb	%dil, %dil
	je	.L657
	movl	%esi, -60(%rbp)
.L657:
	testb	%r14b, %r14b
	jne	.L1141
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1157:
	andl	$-67044352, %eax
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L1158:
	andl	$-458760, %eax
	jmp	.L699
.L1152:
	movl	-72(%rbp), %r9d
	movzbl	-109(%rbp), %edi
	movl	%r10d, %edx
	sall	$6, %r9d
	addl	%r9d, %edi
	.p2align 4,,10
	.p2align 3
.L689:
	testl	%edi, %edi
	jne	.L695
	testl	%esi, %esi
	jns	.L695
	movl	%ecx, %esi
	movl	$1, %r14d
	movl	$-1, %edi
.L695:
	movzwl	(%rbx,%r11,2), %r8d
	.p2align 4,,10
	.p2align 3
.L697:
	sarl	$9, %r8d
	addl	%r8d, %eax
	movslq	%eax, %r10
	movzwl	(%rbx,%r10,2), %r8d
	movl	%r8d, %r9d
	andl	$511, %r9d
	cmpl	%edi, %r9d
	jl	.L697
	je	.L688
	movzwl	(%rbx,%r11,2), %r8d
	movq	%r11, %r10
	movl	%ecx, %edx
.L688:
	sarl	$9, %r8d
	cmpl	$1, %r8d
	je	.L858
	addq	$1, %r10
	movzwl	(%rbx,%r10,2), %eax
	leaq	(%r10,%r10), %rcx
	cmpl	$2, %r8d
	je	.L684
	movzwl	2(%rbx,%rcx), %ecx
	sall	$16, %ecx
	orl	%ecx, %eax
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L1151:
	movl	%r11d, %r10d
	movl	%esi, %edx
	movzwl	(%rbx,%r10,2), %r8d
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L669:
	cmpl	$239, %edi
	jne	.L670
	cmpb	$-65, %al
	jne	.L670
	cmpb	$-66, %r8b
	je	.L672
	movl	%ecx, -60(%rbp)
	testb	%r14b, %r14b
	je	.L673
	movl	%esi, 16(%rbp)
.L673:
	cmpb	$-65, %r8b
	jne	.L860
	movl	$64512, -88(%rbp)
	cmpl	%r13d, -88(%rbp)
	jne	.L702
	.p2align 4,,10
	.p2align 3
.L1155:
	cmpl	$2, -88(%rbp)
	jne	.L1160
.L703:
	movl	-108(%rbp), %eax
	sarl	$12, %eax
	movl	%eax, -88(%rbp)
	andl	$15, %eax
	movl	%eax, -80(%rbp)
	je	.L707
	leaq	-68(%rbp), %rax
	movl	$0, -60(%rbp)
	xorl	%r10d, %r10d
	xorl	%r13d, %r13d
	movl	$0, -64(%rbp)
	movq	%rax, -96(%rbp)
.L708:
	testl	%r13d, %r13d
	je	.L1161
.L709:
	testl	%r10d, %r10d
	jne	.L1162
	.p2align 4,,10
	.p2align 3
.L736:
	movl	-60(%rbp), %eax
	movl	$2, %r14d
	cmpl	16(%rbp), %eax
	je	.L718
	movslq	%eax, %rdx
	leal	1(%rax), %ecx
	movzbl	(%r12,%rdx), %esi
	movl	%ecx, -60(%rbp)
	cmpl	$127, %esi
	jg	.L726
	movzwl	(%rbx,%rsi,2), %edx
.L727:
	cmpl	$4095, %edx
	ja	.L1163
.L730:
	movl	$192, %r14d
	cmpl	%edx, %r15d
	jb	.L718
	leaq	16(%rbp), %r9
	leaq	-60(%rbp), %r8
	movq	%r12, %rcx
	movq	%rbx, %rdi
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_.constprop.0
	cmpl	$65535, %eax
	ja	.L733
	cmpl	$4095, %eax
	jbe	.L734
	andl	$992, %eax
	movl	%eax, %ecx
	leal	32(%rax), %eax
	movl	%eax, %edx
	sall	$16, %edx
	orb	$-64, %dl
	cmpl	$384, %ecx
	cmovnb	%edx, %eax
.L735:
	testl	%eax, %eax
	je	.L736
	movl	%eax, %r14d
	.p2align 4,,10
	.p2align 3
.L718:
	cmpl	%r13d, %r14d
	je	.L1164
	movzwl	%r13w, %eax
	movzwl	%r14w, %r10d
	cmpl	%r10d, %eax
	jne	.L743
	cmpl	$2, %r13d
	je	.L707
	shrl	$16, %r13d
	shrl	$16, %r14d
.L1130:
	movl	%r14d, %r10d
	testl	%r13d, %r13d
	jne	.L709
.L1161:
	movl	-64(%rbp), %eax
	movl	%r10d, %r14d
	cmpl	-68(%rbp), %eax
	je	.L1165
.L741:
	movq	-104(%rbp), %rsi
	movslq	%eax, %rcx
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movzbl	(%rsi,%rcx), %esi
	cmpl	$127, %esi
	jg	.L711
	movzwl	(%rbx,%rsi,2), %edx
.L712:
	cmpl	$4095, %edx
	jbe	.L715
	andl	$992, %edx
	movl	%r14d, %r10d
	leal	32(%rdx), %r13d
	movl	%r13d, %eax
	sall	$16, %eax
	orb	$-64, %al
	cmpl	$384, %edx
	cmovnb	%eax, %r13d
	jmp	.L709
.L645:
	cmpl	$239, %esi
	jne	.L867
	cmpb	$-65, %cl
	jne	.L867
	cmpb	$-66, %al
	je	.L868
	cmpb	$-65, %al
	je	.L869
.L867:
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L643:
	cmpl	%r13d, %r15d
	jb	.L1166
	movq	-104(%rbp), %rcx
	movl	%r13d, %edx
	leaq	-68(%rbp), %r9
	leaq	-64(%rbp), %r8
	movq	%rbx, %rdi
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_.constprop.0
	movl	%eax, %r13d
	cmpl	$1, %eax
	je	.L858
	testb	$-16, %ah
	jne	.L1167
	movzwl	%ax, %eax
	cmpl	%eax, %r15d
	jb	.L1168
	cmpl	$3071, %eax
	jbe	.L635
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L666:
	sall	$6, %eax
	movzbl	%r9b, %edi
	movq	-80(%rbp), %rcx
	addl	%eax, %edi
	movslq	%edi, %rax
	movzwl	(%rcx,%rax,2), %edx
	testl	%edx, %edx
	jne	.L1169
	movzwl	(%rbx,%rax,2), %eax
	movl	%r8d, %ecx
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L637:
	leal	-194(%rsi), %r8d
	leal	2(%rdx), %eax
	cmpl	$3, %r8d
	ja	.L641
	cmpl	%edi, %ecx
	je	.L641
	movq	-104(%rbp), %r11
	movslq	%edi, %r9
	movzbl	(%r11,%r9), %r9d
	leal	-128(%r9), %r10d
	cmpb	$63, %r10b
	jbe	.L1170
.L641:
	cmpl	%eax, %ecx
	jg	.L898
	movl	$1, %r13d
	testl	%ecx, %ecx
	jns	.L643
.L898:
	movq	-104(%rbp), %r11
	movslq	%edi, %rdi
	cltq
	addl	$3, %edx
	movl	%edx, -64(%rbp)
	movzbl	(%r11,%rdi), %ecx
	movzbl	(%r11,%rax), %eax
	cmpl	$226, %esi
	jne	.L645
	cmpb	$-128, %cl
	jne	.L645
	leal	-128(%rax), %edx
	movl	$1, %r13d
	cmpb	$63, %dl
	ja	.L643
	movzwl	512(%rbx,%rax,2), %r13d
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L1150:
	leal	-1(%rcx), %esi
	cmpl	$1, %r15d
	jbe	.L1171
	movl	%ecx, -60(%rbp)
.L1141:
	movl	%esi, 16(%rbp)
.L1126:
	movl	$2, -88(%rbp)
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L1153:
	movslq	%edx, %rdx
	cmpb	$-128, (%r12,%rdx)
	jne	.L858
	movslq	%r10d, %rdx
	movzbl	(%r12,%rdx), %edx
	leal	-128(%rdx), %edi
	cmpb	$63, %dil
	ja	.L858
	leal	256(%rdx), %edi
	leal	3(%rcx), %edx
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L1166:
	andl	$65528, %r13d
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L1144:
	movl	-96(%rbp), %edi
	testl	%edi, %edi
	je	.L1092
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L1160:
	movl	$0, -88(%rbp)
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L1167:
	andl	$-67044352, %r13d
	jmp	.L635
.L707:
	testl	$1024, -108(%rbp)
	je	.L746
	movl	$0, -60(%rbp)
	movq	%rbx, %rdi
	movl	-80(%rbp), %ebx
	xorl	%r14d, %r14d
	movl	$0, -64(%rbp)
	xorl	%r13d, %r13d
.L747:
	testl	%r13d, %r13d
	je	.L1172
.L748:
	testl	%r14d, %r14d
	jne	.L1173
.L751:
	leaq	-60(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L777:
	movl	-60(%rbp), %edx
	movl	$2, %eax
	cmpl	16(%rbp), %edx
	je	.L753
	movslq	%edx, %rax
	leal	1(%rdx), %ecx
	movzbl	(%r12,%rax), %esi
	movl	%ecx, -60(%rbp)
	movq	%rsi, %rax
	cmpl	$127, %esi
	jg	.L768
.L1131:
	movzwl	(%rdi,%rax,2), %edx
.L769:
	cmpl	$3071, %edx
	jbe	.L772
.L774:
	cmpl	$4095, %edx
	ja	.L1174
	movl	$8, %eax
	cmpl	%edx, %r15d
	jb	.L753
	movl	%edx, %eax
	cmpl	$3071, %edx
	ja	.L777
	.p2align 4,,10
	.p2align 3
.L773:
	testl	%eax, %eax
	je	.L777
	.p2align 4,,10
	.p2align 3
.L753:
	cmpl	%r13d, %eax
	je	.L1175
	movzwl	%r13w, %edx
	movzwl	%ax, %r14d
	cmpl	%r14d, %edx
	jne	.L752
	cmpl	$2, %r13d
	je	.L1120
	shrl	$16, %eax
	shrl	$16, %r13d
	movl	%eax, %r14d
	testl	%r13d, %r13d
	jne	.L748
.L1172:
	movl	-64(%rbp), %eax
	cmpl	%eax, -68(%rbp)
	je	.L1176
.L783:
	movq	-104(%rbp), %r13
	leaq	-68(%rbp), %r9
.L750:
	movslq	%eax, %rdx
	leal	1(%rax), %ecx
	movzbl	0(%r13,%rdx), %esi
	movl	%ecx, -64(%rbp)
	cmpl	$127, %esi
	jg	.L755
	movzwl	(%rdi,%rsi,2), %edx
.L756:
	cmpl	$3071, %edx
	jbe	.L759
.L761:
	cmpl	$4095, %edx
	ja	.L1177
	movl	$8, %ecx
	cmpl	%edx, %r15d
	jb	.L1114
	cmpl	$3071, %edx
	ja	.L764
	movl	%edx, %r13d
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L672:
	movl	$3, %eax
	cmpl	$2, %r15d
	jbe	.L1178
.L861:
	movl	%ecx, -60(%rbp)
	testb	%r14b, %r14b
	je	.L1127
	movl	%esi, 16(%rbp)
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1171:
	movl	%ecx, %edx
	movl	%edi, %r14d
	jmp	.L654
.L1146:
	movl	%ecx, -60(%rbp)
	testb	%r14b, %r14b
	je	.L677
.L1140:
	movl	%esi, 16(%rbp)
.L677:
	movl	%edx, -88(%rbp)
	jmp	.L652
.L1120:
	movq	%rdi, %rbx
.L746:
	testb	$14, -88(%rbp)
	je	.L813
	movl	-108(%rbp), %eax
	movl	$0, -60(%rbp)
	movl	$0, -64(%rbp)
	andl	$1536, %eax
	cmpl	$512, %eax
	leaq	-60(%rbp), %rax
	movq	%rax, -88(%rbp)
	sete	%r13b
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
.L787:
	testl	%r14d, %r14d
	je	.L1179
.L788:
	movq	-88(%rbp), %r8
	testl	%r10d, %r10d
	je	.L807
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L1132:
	movzwl	(%rbx,%rax,2), %edx
.L802:
	cmpl	$3071, %edx
	jbe	.L805
.L806:
	movzbl	%r13b, %esi
	movl	%r15d, %edi
	call	_ZN6icu_6718CollationFastLatin13getTertiariesEjaj
	testl	%eax, %eax
	jne	.L1122
.L807:
	movl	-60(%rbp), %edx
	movl	$2, %eax
	cmpl	16(%rbp), %edx
	je	.L1122
	movslq	%edx, %rax
	leal	1(%rdx), %ecx
	movzbl	(%r12,%rax), %esi
	movl	%ecx, -60(%rbp)
	movq	%rsi, %rax
	cmpl	$127, %esi
	jle	.L1132
	movslq	%ecx, %rcx
	cmpl	$197, %esi
	jg	.L803
	addl	$2, %edx
	leal	-194(%rsi), %eax
	movl	%edx, -60(%rbp)
	movzbl	(%r12,%rcx), %edx
	sall	$6, %eax
	addl	%edx, %eax
	cltq
	jmp	.L1132
.L1122:
	movl	%eax, %r10d
.L793:
	cmpl	%r14d, %r10d
	je	.L1180
	movzwl	%r14w, %eax
	movzwl	%r10w, %edx
	cmpl	%edx, %eax
	jne	.L1181
	cmpl	$2, %r14d
	je	.L791
	shrl	$16, %r14d
	shrl	$16, %r10d
	testl	%r14d, %r14d
	jne	.L788
.L1179:
	movl	-64(%rbp), %eax
	cmpl	-68(%rbp), %eax
	je	.L1182
.L789:
	movq	-104(%rbp), %rsi
	movslq	%eax, %rdx
	leal	1(%rax), %ecx
	movl	%ecx, -64(%rbp)
	movzbl	(%rsi,%rdx), %esi
	cmpl	$127, %esi
	jg	.L794
	movzwl	(%rbx,%rsi,2), %edx
.L795:
	cmpl	$3071, %edx
	jbe	.L798
.L799:
	movzbl	%r13b, %esi
	movl	%r15d, %edi
	movl	%r10d, -96(%rbp)
	call	_ZN6icu_6718CollationFastLatin13getTertiariesEjaj
	movl	-96(%rbp), %r10d
	movl	%eax, %r14d
	jmp	.L787
.L1170:
	sall	$6, %r8d
	movzbl	%r9b, %esi
	movq	-80(%rbp), %rcx
	movl	%eax, -64(%rbp)
	addl	%r8d, %esi
	movslq	%esi, %rax
	movzwl	(%rcx,%rax,2), %r13d
	testl	%r13d, %r13d
	je	.L1092
	jmp	.L636
.L881:
	movl	$3, %edx
	.p2align 4,,10
	.p2align 3
.L772:
	leaq	16(%rbp), %r9
	movq	%r12, %rcx
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_.constprop.0
	movl	%eax, %edx
	cmpl	$65535, %eax
	jbe	.L774
	testb	$-16, %ah
	jne	.L1183
	movzwl	%dx, %edx
	movl	$524296, %eax
	cmpl	%edx, %r15d
	jnb	.L777
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L726:
	movslq	%ecx, %rcx
	cmpl	$197, %esi
	jg	.L728
	addl	$2, %eax
	movzbl	(%r12,%rcx), %edx
	movl	%eax, -60(%rbp)
	leal	-194(%rsi), %eax
	sall	$6, %eax
	addl	%edx, %eax
	cltq
	movzwl	(%rbx,%rax,2), %edx
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L733:
	testb	$-16, %ah
	je	.L737
	andl	$65012704, %eax
	addl	$2097184, %eax
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L768:
	movslq	%ecx, %rcx
	cmpl	$197, %esi
	jg	.L770
	addl	$2, %edx
	leal	-194(%rsi), %eax
	movl	%edx, -60(%rbp)
	movzbl	(%r12,%rcx), %edx
	sall	$6, %eax
	addl	%edx, %eax
	cltq
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L803:
	addl	$3, %edx
	movzbl	1(%r12,%rcx), %eax
	movl	%edx, -60(%rbp)
	cmpl	$226, %esi
	je	.L1184
	movl	$64680, %edx
	cmpb	$-66, %al
	jne	.L806
	movl	$3, %edx
	.p2align 4,,10
	.p2align 3
.L805:
	leaq	16(%rbp), %r9
	movq	%r12, %rcx
	movq	%rbx, %rdi
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_.constprop.0
	movl	%eax, %edx
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L1174:
	movl	%edx, %eax
	andl	$24, %eax
	testl	%ebx, %ebx
	je	.L773
	movl	%eax, %ecx
	andl	$992, %edx
	orl	$524288, %ecx
	cmpl	$383, %edx
	cmova	%ecx, %eax
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L737:
	movzwl	%ax, %eax
	movl	$12583104, %r14d
	cmpl	%eax, %r15d
	jnb	.L736
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L734:
	cmpl	%eax, %r15d
	jb	.L718
	cmpl	$3071, %eax
	ja	.L736
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L770:
	addl	$3, %edx
	movzbl	1(%r12,%rcx), %eax
	movl	%edx, -60(%rbp)
	cmpl	$226, %esi
	je	.L1185
	cmpb	$-66, %al
	je	.L881
	movl	$8, %eax
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L1183:
	movl	%eax, %ecx
	andl	$1572888, %edx
	andl	$24, %eax
	andl	$-67108864, %ecx
	orl	%ebx, %ecx
	cmovne	%edx, %eax
	jmp	.L773
.L1168:
	andl	$-458760, %r13d
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L728:
	addl	$3, %eax
	movzbl	1(%r12,%rcx), %edx
	movl	%eax, -60(%rbp)
	cmpl	$226, %esi
	je	.L1186
	cmpb	$-66, %dl
	je	.L876
	movl	$192, %r14d
	jmp	.L718
.L880:
	movl	$3, %edx
.L759:
	leaq	-64(%rbp), %r8
	movq	%r13, %rcx
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_.constprop.0
	movl	%eax, %edx
	cmpl	$65535, %eax
	jbe	.L761
	testb	$-16, %ah
	jne	.L1187
	movzwl	%ax, %edx
	movl	$524296, %ecx
	cmpl	%edx, %r15d
	jb	.L1114
.L764:
	movl	-64(%rbp), %eax
	movl	$2, %ecx
	cmpl	-68(%rbp), %eax
	jne	.L750
.L1114:
	movl	%ecx, %r13d
	jmp	.L748
.L1182:
	testl	%r10d, %r10d
	jne	.L1188
.L894:
	movq	-88(%rbp), %r8
	movl	$2, %r14d
	jmp	.L807
.L711:
	movslq	%edx, %rdx
	cmpl	$197, %esi
	jg	.L713
	movq	-104(%rbp), %rcx
	addl	$2, %eax
	movl	%eax, -64(%rbp)
	leal	-194(%rsi), %eax
	movzbl	(%rcx,%rdx), %edx
	sall	$6, %eax
	addl	%edx, %eax
	cltq
	movzwl	(%rbx,%rax,2), %edx
	jmp	.L712
.L1185:
	movzwl	512(%rdi,%rax,2), %edx
	jmp	.L769
.L872:
	movl	$3, %edx
.L715:
	movl	$192, %r13d
	cmpl	%edx, %r15d
	jnb	.L1189
.L1106:
	movl	%r14d, %r10d
	jmp	.L709
.L1176:
	testl	%r14d, %r14d
	jne	.L1190
.L895:
	movl	$2, %r13d
	jmp	.L751
.L1178:
	movl	%ecx, -60(%rbp)
	testb	%r14b, %r14b
	je	.L652
	movl	%esi, 16(%rbp)
	jmp	.L652
.L1165:
	testl	%r10d, %r10d
	jne	.L1191
.L896:
	movl	$2, %r13d
	jmp	.L736
.L1184:
	movzwl	512(%rbx,%rax,2), %edx
	jmp	.L802
.L755:
	movslq	%ecx, %rcx
	cmpl	$197, %esi
	jle	.L1192
	addl	$3, %eax
	movzbl	1(%r13,%rcx), %edx
	movl	%eax, -64(%rbp)
	cmpl	$226, %esi
	je	.L1193
	cmpb	$-66, %dl
	je	.L880
	movl	$8, %r13d
	jmp	.L748
.L1177:
	movl	%edx, %r13d
	andl	$24, %r13d
	testl	%ebx, %ebx
	je	.L747
	movl	%r13d, %eax
	andl	$992, %edx
	orl	$524288, %eax
	cmpl	$383, %edx
	cmova	%eax, %r13d
	jmp	.L747
.L1187:
	movl	%edx, %r13d
	andl	$-67108864, %eax
	andl	$1572888, %edx
	andl	$24, %r13d
	orl	%ebx, %eax
	cmovne	%edx, %r13d
	jmp	.L747
.L1192:
	addl	$2, %eax
	movzbl	0(%r13,%rcx), %edx
	movl	%eax, -64(%rbp)
	leal	-194(%rsi), %eax
	sall	$6, %eax
	addl	%edx, %eax
	cltq
	movzwl	(%rdi,%rax,2), %edx
	jmp	.L756
.L1189:
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %rcx
	leaq	-64(%rbp), %r8
	movq	%rbx, %rdi
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_.constprop.0
	movl	%eax, %r13d
	cmpl	$65535, %eax
	ja	.L720
	cmpl	$4095, %eax
	jbe	.L721
	movl	%eax, %edx
	movl	%r14d, %r10d
	andl	$992, %edx
	leal	32(%rdx), %r13d
	movl	%r13d, %eax
	sall	$16, %eax
	orb	$-64, %al
	cmpl	$384, %edx
	cmovnb	%eax, %r13d
	jmp	.L708
.L713:
	movq	-104(%rbp), %rcx
	addl	$3, %eax
	movl	%eax, -64(%rbp)
	movzbl	1(%rcx,%rdx), %edx
	cmpl	$226, %esi
	je	.L1194
	cmpb	$-66, %dl
	je	.L872
.L874:
	movl	%r14d, %r10d
	movl	$192, %r13d
	jmp	.L709
.L883:
	movl	$3, %edx
.L798:
	movq	-104(%rbp), %rcx
	leaq	-68(%rbp), %r9
	leaq	-64(%rbp), %r8
	movq	%rbx, %rdi
	movl	%r10d, -96(%rbp)
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_.constprop.0
	movl	-96(%rbp), %r10d
	movl	%eax, %edx
	jmp	.L799
.L794:
	movslq	%ecx, %rcx
	cmpl	$197, %esi
	jg	.L796
	movq	-104(%rbp), %rdi
	addl	$2, %eax
	movl	%eax, -64(%rbp)
	leal	-194(%rsi), %eax
	movzbl	(%rdi,%rcx), %edx
	sall	$6, %eax
	addl	%edx, %eax
	cltq
	movzwl	(%rbx,%rax,2), %edx
	jmp	.L795
.L1198:
	cmpl	$2, %ebx
	jne	.L817
.L813:
	xorl	%eax, %eax
	jmp	.L634
.L876:
	movl	$3, %edx
	jmp	.L730
.L1175:
	cmpl	$2, %eax
	je	.L1120
	movl	-64(%rbp), %eax
	xorl	%r14d, %r14d
	cmpl	-68(%rbp), %eax
	jne	.L783
	jmp	.L895
.L1164:
	cmpl	$2, %r14d
	je	.L707
	movl	-64(%rbp), %eax
	cmpl	-68(%rbp), %eax
	je	.L896
	xorl	%r10d, %r10d
	movl	%r10d, %r14d
	jmp	.L741
.L1180:
	cmpl	$2, %r10d
	je	.L791
	movl	-64(%rbp), %eax
	xorl	%r10d, %r10d
	cmpl	%eax, -68(%rbp)
	jne	.L789
	jmp	.L894
.L1156:
	jnb	.L706
.L745:
	movl	$-1, %eax
	jmp	.L634
.L1186:
	movzwl	512(%rbx,%rdx,2), %edx
	jmp	.L727
.L868:
	movl	$239, %esi
	movl	$3, %r13d
	jmp	.L643
.L1188:
	cmpl	$2, %r10d
	je	.L791
	movl	-108(%rbp), %eax
	andl	$1792, %eax
	cmpl	$768, %eax
	je	.L897
	movl	$2, %eax
.L811:
	cmpl	%r10d, %eax
	jb	.L745
.L706:
	movl	$1, %eax
	jmp	.L634
.L796:
	movq	-104(%rbp), %rdi
	addl	$3, %eax
	movl	%eax, -64(%rbp)
	movzbl	1(%rdi,%rcx), %edx
	cmpl	$226, %esi
	je	.L1195
	cmpb	$-66, %dl
	je	.L883
	movl	$64680, %edx
	jmp	.L799
.L791:
	cmpl	$2, -80(%rbp)
	je	.L813
	leaq	-68(%rbp), %rax
	xorl	%r11d, %r11d
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	movl	$0, -60(%rbp)
	leaq	-60(%rbp), %r13
	movl	%r11d, %ebx
	movl	$0, -64(%rbp)
	movq	%rax, -80(%rbp)
.L814:
	testl	%r14d, %r14d
	jne	.L1196
.L851:
	movl	-64(%rbp), %eax
	cmpl	-68(%rbp), %eax
	je	.L1197
	movq	-104(%rbp), %rsi
	movslq	%eax, %rcx
	leal	1(%rax), %edx
	movl	%edx, -64(%rbp)
	movzbl	(%rsi,%rcx), %esi
	cmpl	$127, %esi
	jg	.L825
	movzwl	(%rdi,%rsi,2), %r14d
.L826:
	cmpl	$3071, %r14d
	jbe	.L829
.L831:
	cmpl	$4095, %r14d
	jbe	.L833
	andl	$992, %r14d
	cmpl	$383, %r14d
	jbe	.L834
.L1143:
	testl	%ebx, %ebx
	jne	.L839
	movl	$-67044352, %r14d
.L824:
	movl	-60(%rbp), %eax
	movl	$2, %ebx
	cmpl	16(%rbp), %eax
	je	.L823
	movslq	%eax, %rdx
	leal	1(%rax), %ecx
	movzbl	(%r12,%rdx), %esi
	movl	%ecx, -60(%rbp)
	cmpl	$127, %esi
	jg	.L841
	movzwl	(%rdi,%rsi,2), %edx
.L842:
	cmpl	$3071, %edx
	jbe	.L845
.L846:
	cmpl	$4095, %edx
	jbe	.L848
	andl	$992, %edx
	cmpl	$384, %edx
	sbbl	%ebx, %ebx
	andl	$67108864, %ebx
	subl	$67044352, %ebx
.L823:
	cmpl	%r14d, %ebx
	je	.L1198
	movzwl	%r14w, %eax
	movzwl	%bx, %edx
	cmpl	%edx, %eax
	jne	.L1199
	cmpl	$2, %r14d
	je	.L813
.L853:
	shrl	$16, %r14d
	shrl	$16, %ebx
	testl	%r14d, %r14d
	je	.L851
.L1196:
	testl	%ebx, %ebx
	je	.L824
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L843:
	addl	$3, %eax
	movzbl	1(%r12,%rcx), %edx
	movl	%eax, -60(%rbp)
	cmpl	$226, %esi
	je	.L1200
	movl	$64512, %ebx
	cmpb	$-66, %dl
	jne	.L823
	movl	$3, %edx
.L845:
	leaq	16(%rbp), %r9
	movq	%r13, %r8
	movq	%r12, %rcx
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_.constprop.0
	movl	%eax, %edx
	cmpl	$65535, %eax
	jbe	.L846
	movzwl	%ax, %eax
	movl	$-67044352, %ebx
	cmpl	%eax, %r15d
	jb	.L823
	andl	$-458760, %edx
.L849:
	testl	%edx, %edx
	je	.L824
	movl	%edx, %ebx
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L841:
	movslq	%ecx, %rcx
	cmpl	$197, %esi
	jg	.L843
	addl	$2, %eax
	movzbl	(%r12,%rcx), %edx
	movl	%eax, -60(%rbp)
	leal	-194(%rsi), %eax
	sall	$6, %eax
	addl	%edx, %eax
	cltq
	movzwl	(%rdi,%rax,2), %edx
	jmp	.L842
.L848:
	movl	$64512, %ebx
	cmpl	%edx, %r15d
	jb	.L823
	movl	%edx, %eax
	andl	$65528, %eax
	cmpl	$3071, %edx
	cmova	%eax, %edx
	jmp	.L849
.L1163:
	andl	$992, %edx
	leal	32(%rdx), %r14d
	cmpl	$383, %edx
	jbe	.L718
	sall	$16, %r14d
	orb	$-64, %r14b
	jmp	.L718
.L1169:
	movl	%r8d, -60(%rbp)
	testb	%r14b, %r14b
	jne	.L1140
	jmp	.L677
.L869:
	movl	$64512, %r13d
	jmp	.L636
.L827:
	movq	-104(%rbp), %rcx
	addl	$3, %eax
	movl	%eax, -64(%rbp)
	movzbl	1(%rcx,%rdx), %edx
	cmpl	$226, %esi
	je	.L1201
	cmpb	$-66, %dl
	jne	.L834
	movl	$3, %r14d
.L829:
	movq	-80(%rbp), %r9
	movq	-104(%rbp), %rcx
	movl	%r14d, %edx
	leaq	-64(%rbp), %r8
	call	_ZN6icu_6718CollationFastLatin8nextPairEPKtijPKDsPKhRiS7_.constprop.0
	movl	%eax, %r14d
	cmpl	$65535, %eax
	jbe	.L831
	movzwl	%ax, %eax
	andl	$-458760, %r14d
	cmpl	%eax, %r15d
	jnb	.L814
	jmp	.L1143
.L1197:
	testl	%ebx, %ebx
	jne	.L1202
	movl	$2, %r14d
	jmp	.L824
.L1200:
	movzwl	512(%rdi,%rdx,2), %edx
	jmp	.L842
.L720:
	testb	$-16, %ah
	je	.L724
	andl	$65012704, %r13d
	movl	%r14d, %r10d
	addl	$2097184, %r13d
	jmp	.L708
.L1190:
	movq	%rdi, %rbx
	cmpl	$2, %r14d
	je	.L746
	movl	$2, %edx
.L752:
	testl	$256, -108(%rbp)
	jne	.L785
	cmpl	%edx, %r14d
	ja	.L745
	jmp	.L706
.L825:
	movslq	%edx, %rdx
	cmpl	$197, %esi
	jg	.L827
	movq	-104(%rbp), %rcx
	addl	$2, %eax
	movl	%eax, -64(%rbp)
	leal	-194(%rsi), %eax
	movzbl	(%rcx,%rdx), %edx
	sall	$6, %eax
	addl	%edx, %eax
	cltq
	movzwl	(%rdi,%rax,2), %r14d
	jmp	.L826
.L834:
	testl	%ebx, %ebx
	jne	.L1134
	movl	$64512, %r14d
	jmp	.L824
.L833:
	cmpl	%r14d, %r15d
	jb	.L834
	movl	%r14d, %eax
	andl	$65528, %eax
	cmpl	$3071, %r14d
	cmova	%eax, %r14d
	jmp	.L814
.L724:
	movzwl	%ax, %eax
	movl	$12583104, %r13d
	cmpl	%eax, %r15d
	jb	.L1106
.L723:
	movl	-64(%rbp), %eax
	movl	$2, %r13d
	cmpl	-68(%rbp), %eax
	jne	.L741
	jmp	.L1106
.L1193:
	movzwl	512(%rdi,%rdx,2), %edx
	jmp	.L756
.L721:
	cmpl	%eax, %r15d
	jb	.L874
	cmpl	$3071, %eax
	ja	.L723
	jmp	.L1130
.L1195:
	movzwl	512(%rbx,%rdx,2), %edx
	jmp	.L795
.L1191:
	cmpl	$2, %r10d
	je	.L707
	movl	$2, %eax
.L743:
	testl	$2048, -108(%rbp)
	jne	.L858
	cmpl	%eax, %r10d
	ja	.L745
	jmp	.L706
.L1134:
	cmpl	$64512, %ebx
	jne	.L892
.L817:
	xorl	%ebx, %ebx
	jmp	.L851
.L785:
	cmpl	%edx, %r14d
	jbe	.L745
	jmp	.L706
.L1181:
	movl	-108(%rbp), %ecx
	andl	$1792, %ecx
	cmpl	$768, %ecx
	je	.L1203
	movl	%edx, %r10d
	jmp	.L811
.L1194:
	movzwl	512(%rbx,%rdx,2), %edx
	jmp	.L712
.L1199:
	movl	%edx, %r11d
.L852:
	cmpl	%r11d, %eax
	jb	.L745
	jmp	.L706
.L839:
	cmpl	$64512, %ebx
	jne	.L892
	movl	$-67044352, %r14d
	jmp	.L853
.L1173:
	movl	%r14d, %eax
	jmp	.L753
.L897:
	movl	%r10d, %edx
	movl	$2, %eax
.L812:
	movl	%edx, %ecx
	xorl	$24, %ecx
	andl	$65532, %r10d
	cmovne	%ecx, %edx
	movl	%edx, %r10d
	jmp	.L811
.L892:
	movl	%ebx, %r11d
	movl	$64512, %eax
	jmp	.L852
.L1159:
	movl	%edx, %ecx
	jmp	.L861
.L682:
	cmpl	$1, %eax
	je	.L858
	movl	%ecx, %edx
	movl	%eax, %ecx
	jmp	.L700
.L1148:
	movl	%ecx, -60(%rbp)
	testb	%r14b, %r14b
	je	.L681
	movl	%esi, 16(%rbp)
.L681:
	andl	$65528, %eax
	movl	%eax, -88(%rbp)
	jmp	.L652
.L1202:
	movl	%ebx, %r11d
	cmpl	$2, %ebx
	je	.L813
	movl	$2, %eax
	jmp	.L852
.L1154:
	call	__stack_chk_fail@PLT
.L1162:
	movl	%r10d, %r14d
	jmp	.L718
.L1201:
	movzwl	512(%rdi,%rdx,2), %r14d
	jmp	.L826
.L1203:
	andl	$65532, %r14d
	je	.L812
	xorl	$24, %eax
	jmp	.L812
	.cfi_endproc
.LFE3156:
	.size	_ZN6icu_6718CollationFastLatin11compareUTF8EPKtS2_iPKhiS4_i, .-_ZN6icu_6718CollationFastLatin11compareUTF8EPKtS2_iPKhiS4_i
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationFastLatin15getQuaternariesEjj
	.type	_ZN6icu_6718CollationFastLatin15getQuaternariesEjj, @function
_ZN6icu_6718CollationFastLatin15getQuaternariesEjj:
.LFB3164:
	.cfi_startproc
	endbr64
	cmpl	$65535, %esi
	ja	.L1205
	cmpl	$4095, %esi
	jbe	.L1206
	andl	$992, %esi
	cmpl	$384, %esi
	sbbl	%eax, %eax
	andl	$67108864, %eax
	subl	$67044352, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1205:
	movzwl	%si, %eax
	andl	$-458760, %esi
	cmpl	%edi, %eax
	movl	$-67044352, %eax
	cmovbe	%esi, %eax
.L1204:
	ret
	.p2align 4,,10
	.p2align 3
.L1206:
	movl	$64512, %eax
	cmpl	%edi, %esi
	ja	.L1204
	movl	%esi, %eax
	andl	$65528, %eax
	cmpl	$3071, %esi
	cmovbe	%esi, %eax
	ret
	.cfi_endproc
.LFE3164:
	.size	_ZN6icu_6718CollationFastLatin15getQuaternariesEjj, .-_ZN6icu_6718CollationFastLatin15getQuaternariesEjj
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.value	-8
	.value	-8
	.value	-8
	.value	-8
	.value	-8
	.value	-8
	.value	-8
	.value	-8
	.align 16
.LC1:
	.long	4095
	.long	4095
	.long	4095
	.long	4095
	.align 16
.LC2:
	.long	-2147483648
	.long	-2147483648
	.long	-2147483648
	.long	-2147483648
	.align 16
.LC3:
	.value	-1024
	.value	-1024
	.value	-1024
	.value	-1024
	.value	-1024
	.value	-1024
	.value	-1024
	.value	-1024
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
