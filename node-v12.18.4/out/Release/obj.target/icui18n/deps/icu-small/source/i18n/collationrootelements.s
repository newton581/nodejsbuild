	.file	"collationrootelements.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CollationRootElements15getPrimaryAfterEjia
	.type	_ZNK6icu_6721CollationRootElements15getPrimaryAfterEjia, @function
_ZNK6icu_6721CollationRootElements15getPrimaryAfterEjia:
.LFB74:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addl	$1, %edx
	movl	%esi, %r9d
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r8d
	leaq	0(,%rdx,4), %rsi
	testb	$-128, %r8b
	jne	.L2
	movl	%r8d, %edx
	andl	$127, %edx
	je	.L1
	movsbl	%cl, %esi
	movl	%r9d, %edi
	testw	%r9w, %r9w
	jne	.L4
	jmp	_ZN6icu_679Collation25incTwoBytePrimaryByOffsetEjai@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	4(%rax,%rsi), %rax
	.p2align 4,,10
	.p2align 3
.L5:
	movl	(%rax), %r8d
	addq	$4, %rax
	testb	$-128, %r8b
	jne	.L5
.L1:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	jmp	_ZN6icu_679Collation27incThreeBytePrimaryByOffsetEjai@PLT
	.cfi_endproc
.LFE74:
	.size	_ZNK6icu_6721CollationRootElements15getPrimaryAfterEjia, .-_ZNK6icu_6721CollationRootElements15getPrimaryAfterEjia
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CollationRootElements17getSecondaryAfterEij
	.type	_ZNK6icu_6721CollationRootElements17getSecondaryAfterEij, @function
_ZNK6icu_6721CollationRootElements17getSecondaryAfterEij:
.LFB75:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	testl	%esi, %esi
	jne	.L12
	movslq	4(%rcx), %rsi
	movl	$65536, %edi
	movl	(%rcx,%rsi,4), %eax
.L13:
	leaq	(%rcx,%rsi,4), %rcx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L21:
	movl	4(%rcx), %eax
	addq	$4, %rcx
	testb	$-128, %al
	je	.L20
.L16:
	shrl	$16, %eax
	cmpl	%edx, %eax
	jbe	.L21
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	leal	1(%rsi), %eax
	cltq
	movl	(%rcx,%rax,4), %edi
	movl	$83887360, %eax
	testb	$-128, %dil
	je	.L14
	movl	%edi, %eax
	movl	$83887360, %edi
	andb	$127, %al
	cmpl	$83887360, %eax
	cmova	%edi, %eax
.L14:
	movl	16(%rcx), %edi
	movslq	%esi, %rsi
	shrl	$8, %edi
	andl	$65280, %edi
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L20:
	movl	%edi, %eax
	ret
	.cfi_endproc
.LFE75:
	.size	_ZNK6icu_6721CollationRootElements17getSecondaryAfterEij, .-_ZNK6icu_6721CollationRootElements17getSecondaryAfterEij
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CollationRootElements16getTertiaryAfterEijj
	.type	_ZNK6icu_6721CollationRootElements16getTertiaryAfterEijj, @function
_ZNK6icu_6721CollationRootElements16getTertiaryAfterEijj:
.LFB76:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movl	%ecx, %r9d
	testl	%esi, %esi
	jne	.L23
	testl	%edx, %edx
	je	.L38
	movl	16(%rdi), %r8d
	movl	4(%rdi), %esi
	sall	$8, %r8d
	movzwl	%r8w, %r8d
.L25:
	movslq	%esi, %rax
	movl	(%rdi,%rax,4), %eax
	andb	$127, %al
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L23:
	leal	1(%rsi), %eax
	cltq
	movl	(%rdi,%rax,4), %ecx
	movl	$83887360, %eax
	testb	$-128, %cl
	je	.L27
	movl	%ecx, %eax
	movl	$83887360, %ecx
	andb	$127, %al
	cmpl	$83887360, %eax
	cmova	%ecx, %eax
.L27:
	movl	16(%rdi), %r8d
	sall	$8, %r8d
	movzwl	%r8w, %r8d
.L26:
	movl	%edx, %ecx
	movslq	%esi, %rsi
	sall	$16, %ecx
	leaq	(%rdi,%rsi,4), %rsi
	orl	%r9d, %ecx
	cmpl	%ecx, %eax
	jbe	.L31
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L39:
	movl	%eax, %edi
	shrl	$16, %edi
	cmpl	%edx, %edi
	ja	.L22
	andb	$127, %al
	addq	$4, %rsi
	cmpl	%eax, %ecx
	jb	.L30
.L31:
	movl	4(%rsi), %eax
	testb	$-128, %al
	jne	.L39
.L22:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movl	(%rdi), %esi
	movl	$16384, %r8d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L30:
	movzwl	%ax, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE76:
	.size	_ZNK6icu_6721CollationRootElements16getTertiaryAfterEijj, .-_ZNK6icu_6721CollationRootElements16getTertiaryAfterEijj
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CollationRootElements24getFirstSecTerForPrimaryEi
	.type	_ZNK6icu_6721CollationRootElements24getFirstSecTerForPrimaryEi, @function
_ZNK6icu_6721CollationRootElements24getFirstSecTerForPrimaryEi:
.LFB77:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %edx
	movl	$83887360, %eax
	testb	$-128, %dl
	je	.L40
	movl	%edx, %eax
	movl	$83887360, %edx
	andb	$127, %al
	cmpl	$83887360, %eax
	cmova	%edx, %eax
.L40:
	ret
	.cfi_endproc
.LFE77:
	.size	_ZNK6icu_6721CollationRootElements24getFirstSecTerForPrimaryEi, .-_ZNK6icu_6721CollationRootElements24getFirstSecTerForPrimaryEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CollationRootElements5findPEj
	.type	_ZNK6icu_6721CollationRootElements5findPEj, @function
_ZNK6icu_6721CollationRootElements5findPEj:
.LFB79:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	movl	8(%rdi), %edi
	movl	8(%r8), %r9d
	subl	$1, %edi
.L51:
	leal	1(%r9), %r10d
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L46:
	xorb	%dl, %dl
	cmpl	%esi, %edx
	jbe	.L64
	movl	%eax, %edi
.L50:
	cmpl	%edi, %r10d
	jge	.L44
	leal	(%r9,%rdi), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rdx
	movl	%ecx, %eax
	movl	(%r8,%rdx,4), %edx
	testb	$-128, %dl
	je	.L46
	leal	1(%rcx), %eax
	cmpl	%eax, %edi
	je	.L47
	cltq
	.p2align 4,,10
	.p2align 3
.L48:
	movl	(%r8,%rax,4), %edx
	testb	$-128, %dl
	je	.L46
	addq	$1, %rax
	cmpl	%eax, %edi
	jne	.L48
.L47:
	subl	$1, %ecx
	cmpl	%ecx, %r9d
	je	.L44
	movslq	%ecx, %rcx
	.p2align 4,,10
	.p2align 3
.L49:
	movl	(%r8,%rcx,4), %edx
	movl	%ecx, %eax
	testb	$-128, %dl
	je	.L46
	subq	$1, %rcx
	cmpl	%ecx, %r9d
	jne	.L49
.L44:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	movl	%eax, %r9d
	jmp	.L51
	.cfi_endproc
.LFE79:
	.size	_ZNK6icu_6721CollationRootElements5findPEj, .-_ZNK6icu_6721CollationRootElements5findPEj
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CollationRootElements23lastCEWithPrimaryBeforeEj
	.type	_ZNK6icu_6721CollationRootElements23lastCEWithPrimaryBeforeEj, @function
_ZNK6icu_6721CollationRootElements23lastCEWithPrimaryBeforeEj:
.LFB69:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L87
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6721CollationRootElements5findPEj
	movl	$83887360, %ecx
	movq	(%r11), %r9
	movl	%eax, %r8d
	cltq
	salq	$2, %rax
	leaq	(%r9,%rax), %rdx
	movl	(%rdx), %edi
	xorb	%dil, %dil
	cmpl	%esi, %edi
	je	.L88
	.p2align 4,,10
	.p2align 3
.L73:
	movl	%ecx, %eax
	movl	4(%rdx), %ecx
	addq	$4, %rdx
	testb	$-128, %cl
	jne	.L73
	movl	%eax, %edx
	andl	$-129, %edx
.L69:
	movq	%rdi, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	salq	$32, %rax
	orq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movl	-4(%r9,%rax), %eax
	testb	$-128, %al
	jne	.L68
	xorb	%al, %al
	movl	$83887360, %edx
	movl	%eax, %edi
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L68:
	subl	$2, %r8d
	movslq	%r8d, %r8
	movl	(%r9,%r8,4), %edi
	leaq	-4(%r9,%r8,4), %rdx
	testb	$-128, %dil
	je	.L71
	.p2align 4,,10
	.p2align 3
.L72:
	movl	(%rdx), %edi
	subq	$4, %rdx
	testb	$-128, %dil
	jne	.L72
.L71:
	movl	%eax, %edx
	xorb	%dil, %dil
	andl	$-129, %edx
	jmp	.L69
	.cfi_endproc
.LFE69:
	.size	_ZNK6icu_6721CollationRootElements23lastCEWithPrimaryBeforeEj, .-_ZNK6icu_6721CollationRootElements23lastCEWithPrimaryBeforeEj
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CollationRootElements25firstCEWithPrimaryAtLeastEj
	.type	_ZNK6icu_6721CollationRootElements25firstCEWithPrimaryAtLeastEj, @function
_ZNK6icu_6721CollationRootElements25firstCEWithPrimaryAtLeastEj:
.LFB70:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L101
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6721CollationRootElements5findPEj
	movq	(%r11), %rdx
	cltq
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax), %edx
	xorb	%dl, %dl
	cmpl	%esi, %edx
	je	.L91
	.p2align 4,,10
	.p2align 3
.L92:
	movl	4(%rax), %esi
	addq	$4, %rax
	testb	$-128, %sil
	jne	.L92
.L91:
	salq	$32, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rsi, %rax
	orq	$83887360, %rax
	ret
	.cfi_endproc
.LFE70:
	.size	_ZNK6icu_6721CollationRootElements25firstCEWithPrimaryAtLeastEj, .-_ZNK6icu_6721CollationRootElements25firstCEWithPrimaryAtLeastEj
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CollationRootElements11findPrimaryEj
	.type	_ZNK6icu_6721CollationRootElements11findPrimaryEj, @function
_ZNK6icu_6721CollationRootElements11findPrimaryEj:
.LFB78:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6721CollationRootElements5findPEj
	.cfi_endproc
.LFE78:
	.size	_ZNK6icu_6721CollationRootElements11findPrimaryEj, .-_ZNK6icu_6721CollationRootElements11findPrimaryEj
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CollationRootElements16getPrimaryBeforeEja
	.type	_ZNK6icu_6721CollationRootElements16getPrimaryBeforeEja, @function
_ZNK6icu_6721CollationRootElements16getPrimaryBeforeEja:
.LFB71:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	call	_ZNK6icu_6721CollationRootElements5findPEj
	movq	(%r12), %rsi
	cltq
	salq	$2, %rax
	leaq	(%rsi,%rax), %rcx
	movl	(%rcx), %edx
	movl	%edx, %edi
	xorb	%dil, %dil
	cmpl	%r11d, %edi
	je	.L112
	movl	4(%rsi,%rax), %edx
	andl	$127, %edx
.L105:
	movsbl	%bl, %esi
	movl	%r11d, %edi
	testw	%r11w, %r11w
	jne	.L108
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679Collation26decTwoBytePrimaryByOneStepEjai@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679Collation28decThreeBytePrimaryByOneStepEjai@PLT
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	andl	$127, %edx
	jne	.L105
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L106:
	movl	-4(%rdx), %eax
	subq	$4, %rdx
	testb	$-128, %al
	jne	.L106
	popq	%rbx
	xorb	%al, %al
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE71:
	.size	_ZNK6icu_6721CollationRootElements16getPrimaryBeforeEja, .-_ZNK6icu_6721CollationRootElements16getPrimaryBeforeEja
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CollationRootElements18getSecondaryBeforeEjj
	.type	_ZNK6icu_6721CollationRootElements18getSecondaryBeforeEjj, @function
_ZNK6icu_6721CollationRootElements18getSecondaryBeforeEjj:
.LFB72:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	%esi, %esi
	jne	.L114
	movq	(%rdi), %rdx
	movslq	4(%rdx), %rax
	movzwl	2(%rdx,%rax,4), %ecx
	leaq	(%rdx,%rax,4), %rax
	cmpl	%ecx, %r11d
	jbe	.L126
	.p2align 4,,10
	.p2align 3
.L119:
	movl	%ecx, %r8d
	movl	(%rax), %ecx
	addq	$4, %rax
	shrl	$16, %ecx
	cmpl	%ecx, %r11d
	ja	.L119
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	call	_ZNK6icu_6721CollationRootElements5findPEj
	movq	(%rbx), %rdx
	movl	$1280, %ecx
	addl	$1, %eax
	cltq
	movl	(%rdx,%rax,4), %esi
	testb	$-128, %sil
	je	.L117
	andb	$127, %sil
	movl	%esi, %ecx
	movl	$83887360, %esi
	cmpl	$83887360, %ecx
	cmova	%esi, %ecx
	shrl	$16, %ecx
.L117:
	movl	$256, %esi
	leaq	(%rdx,%rax,4), %rax
	cmpl	%ecx, %r11d
	ja	.L119
.L126:
	addq	$8, %rsp
	movl	%esi, %r8d
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE72:
	.size	_ZNK6icu_6721CollationRootElements18getSecondaryBeforeEjj, .-_ZNK6icu_6721CollationRootElements18getSecondaryBeforeEjj
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CollationRootElements17getTertiaryBeforeEjjj
	.type	_ZNK6icu_6721CollationRootElements17getTertiaryBeforeEjjj, @function
_ZNK6icu_6721CollationRootElements17getTertiaryBeforeEjjj:
.LFB73:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testl	%esi, %esi
	jne	.L128
	movq	(%rdi), %rsi
	testl	%edx, %edx
	je	.L138
	movslq	4(%rsi), %rdx
	movl	$256, %r8d
.L130:
	movl	(%rsi,%rdx,4), %eax
	andb	$127, %al
.L131:
	movl	%r11d, %ecx
	sall	$16, %ecx
	orl	%r12d, %ecx
	cmpl	%ecx, %eax
	jnb	.L127
	leaq	(%rsi,%rdx,4), %rsi
	.p2align 4,,10
	.p2align 3
.L134:
	movl	%eax, %edi
	shrl	$16, %edi
	cmpl	%edi, %r11d
	cmove	%eax, %r8d
	movl	(%rsi), %eax
	addq	$4, %rsi
	andb	$127, %al
	cmpl	%eax, %ecx
	ja	.L134
	movzwl	%r8w, %r8d
.L127:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movslq	(%rsi), %rdx
	xorl	%r8d, %r8d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L128:
	call	_ZNK6icu_6721CollationRootElements5findPEj
	movq	(%rbx), %rsi
	leal	1(%rax), %edx
	movslq	%edx, %rdx
	movl	(%rsi,%rdx,4), %eax
	testb	$-128, %al
	je	.L135
	andb	$127, %al
	movl	$83887360, %ecx
	movl	$256, %r8d
	cmpl	$83887360, %eax
	cmova	%ecx, %eax
	jmp	.L131
.L135:
	movl	$83887360, %eax
	movl	$256, %r8d
	jmp	.L131
	.cfi_endproc
.LFE73:
	.size	_ZNK6icu_6721CollationRootElements17getTertiaryBeforeEjjj, .-_ZNK6icu_6721CollationRootElements17getTertiaryBeforeEjjj
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
