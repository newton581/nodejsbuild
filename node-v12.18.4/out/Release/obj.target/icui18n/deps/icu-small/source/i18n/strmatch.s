	.file	"strmatch.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringMatcher17getDynamicClassIDEv
	.type	_ZNK6icu_6713StringMatcher17getDynamicClassIDEv, @function
_ZNK6icu_6713StringMatcher17getDynamicClassIDEv:
.LFB2423:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713StringMatcher16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2423:
	.size	_ZNK6icu_6713StringMatcher17getDynamicClassIDEv, .-_ZNK6icu_6713StringMatcher17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringMatcher9toMatcherEv
	.type	_ZNK6icu_6713StringMatcher9toMatcherEv, @function
_ZNK6icu_6713StringMatcher9toMatcherEv:
.LFB2456:
	.cfi_startproc
	endbr64
	leaq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE2456:
	.size	_ZNK6icu_6713StringMatcher9toMatcherEv, .-_ZNK6icu_6713StringMatcher9toMatcherEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringMatcher10toReplacerEv
	.type	_ZNK6icu_6713StringMatcher10toReplacerEv, @function
_ZNK6icu_6713StringMatcher10toReplacerEv:
.LFB2457:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE2457:
	.size	_ZNK6icu_6713StringMatcher10toReplacerEv, .-_ZNK6icu_6713StringMatcher10toReplacerEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringMatcher19addReplacementSetToERNS_10UnicodeSetE
	.type	_ZNK6icu_6713StringMatcher19addReplacementSetToERNS_10UnicodeSetE, @function
_ZNK6icu_6713StringMatcher19addReplacementSetToERNS_10UnicodeSetE:
.LFB2465:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2465:
	.size	_ZNK6icu_6713StringMatcher19addReplacementSetToERNS_10UnicodeSetE, .-_ZNK6icu_6713StringMatcher19addReplacementSetToERNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringMatcher7replaceERNS_11ReplaceableEiiRi
	.type	_ZN6icu_6713StringMatcher7replaceERNS_11ReplaceableEiiRi, @function
_ZN6icu_6713StringMatcher7replaceERNS_11ReplaceableEiiRi:
.LFB2462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	100(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	testl	%esi, %esi
	js	.L7
	movl	104(%rdi), %edx
	movq	%rdi, %rbx
	cmpl	%edx, %esi
	je	.L7
	movq	%r12, %rdi
	call	*40(%rax)
	movq	(%r12), %rax
	movl	104(%rbx), %r15d
	subl	100(%rbx), %r15d
.L7:
	movl	$2, %edx
	leaq	-128(%rbp), %rbx
	movl	%r14d, %esi
	movq	%r12, %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movw	%dx, -120(%rbp)
	movl	%r13d, %edx
	movq	%rcx, -128(%rbp)
	movq	%rbx, %rcx
	call	*32(%rax)
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L12
	addq	$88, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L12:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2462:
	.size	_ZN6icu_6713StringMatcher7replaceERNS_11ReplaceableEiiRi, .-_ZN6icu_6713StringMatcher7replaceERNS_11ReplaceableEiiRi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringMatcherD2Ev
	.type	_ZN6icu_6713StringMatcherD2Ev, @function
_ZN6icu_6713StringMatcherD2Ev:
.LFB2452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	144+_ZTVN6icu_6713StringMatcherE(%rip), %rax
	leaq	-128(%rax), %rdx
	movq	%rax, %xmm1
	addq	$64, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	24(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_6715UnicodeReplacerD2Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	.cfi_endproc
.LFE2452:
	.size	_ZN6icu_6713StringMatcherD2Ev, .-_ZN6icu_6713StringMatcherD2Ev
	.globl	_ZN6icu_6713StringMatcherD1Ev
	.set	_ZN6icu_6713StringMatcherD1Ev,_ZN6icu_6713StringMatcherD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringMatcher5cloneEv
	.type	_ZNK6icu_6713StringMatcher5cloneEv, @function
_ZNK6icu_6713StringMatcher5cloneEv:
.LFB2455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$112, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L15
	leaq	144+_ZTVN6icu_6713StringMatcherE(%rip), %rax
	leaq	24(%rbx), %rsi
	leaq	-128(%rax), %rcx
	movq	%rax, %xmm1
	addq	$64, %rax
	movq	%rax, 16(%r12)
	movq	%rcx, %xmm0
	leaq	24(%r12), %rdi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	88(%rbx), %rdx
	movl	104(%rbx), %eax
	movq	%rdx, 88(%r12)
	movq	96(%rbx), %rdx
	movl	%eax, 104(%r12)
	movq	%rdx, 96(%r12)
.L15:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2455:
	.size	_ZNK6icu_6713StringMatcher5cloneEv, .-_ZNK6icu_6713StringMatcher5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringMatcher7setDataEPKNS_23TransliterationRuleDataE
	.type	_ZN6icu_6713StringMatcher7setDataEPKNS_23TransliterationRuleDataE, @function
_ZN6icu_6713StringMatcher7setDataEPKNS_23TransliterationRuleDataE:
.LFB2466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, 88(%rdi)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L33:
	sarl	$5, %eax
	cmpl	%eax, %r14d
	jge	.L21
.L34:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	88(%rbx), %rdi
	movl	%eax, %esi
	movl	%eax, %r13d
	call	_ZNK6icu_6723TransliterationRuleData6lookupEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L25
	movq	(%rax), %rax
	movq	88(%rbx), %rsi
	call	*48(%rax)
.L25:
	xorl	%eax, %eax
	cmpl	$65535, %r13d
	seta	%al
	leal	1(%r14,%rax), %r14d
.L27:
	movswl	32(%rbx), %eax
	testw	%ax, %ax
	jns	.L33
	movl	36(%rbx), %eax
	cmpl	%eax, %r14d
	jl	.L34
.L21:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2466:
	.size	_ZN6icu_6713StringMatcher7setDataEPKNS_23TransliterationRuleDataE, .-_ZN6icu_6713StringMatcher7setDataEPKNS_23TransliterationRuleDataE
	.p2align 4
	.globl	_ZThn16_NK6icu_6713StringMatcher19addReplacementSetToERNS_10UnicodeSetE
	.type	_ZThn16_NK6icu_6713StringMatcher19addReplacementSetToERNS_10UnicodeSetE, @function
_ZThn16_NK6icu_6713StringMatcher19addReplacementSetToERNS_10UnicodeSetE:
.LFB3183:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3183:
	.size	_ZThn16_NK6icu_6713StringMatcher19addReplacementSetToERNS_10UnicodeSetE, .-_ZThn16_NK6icu_6713StringMatcher19addReplacementSetToERNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia
	.type	_ZN6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia, @function
_ZN6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia:
.LFB2458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -80(%rbp)
	movl	(%rdx), %edx
	movb	%r8b, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edx, -60(%rbp)
	movzwl	32(%rdi), %eax
	cmpl	%ecx, %edx
	jle	.L68
	testw	%ax, %ax
	js	.L38
	movswl	%ax, %ebx
	sarl	$5, %ebx
.L39:
	subl	$1, %ebx
	js	.L40
	movslq	%ebx, %r14
	addq	%r14, %r14
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L78:
	movswl	%ax, %edx
	sarl	$5, %edx
	cmpl	%ebx, %edx
	jbe	.L69
.L79:
	testb	$2, %al
	je	.L44
	leaq	34(%r15), %rax
.L45:
	movzwl	(%rax,%r14), %esi
	movw	%si, -70(%rbp)
.L43:
	movq	88(%r15), %rdi
	call	_ZNK6icu_6723TransliterationRuleData13lookupMatcherEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L76
	movq	(%rax), %rax
	leaq	_ZThn8_N6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia(%rip), %rcx
	movsbl	-68(%rbp), %r8d
	leaq	-60(%rbp), %rdx
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L51
	subq	$8, %rdi
	movl	%r12d, %ecx
	movq	%r13, %rsi
	call	.LTHUNK4
.L52:
	cmpl	$2, %eax
	jne	.L36
	subl	$1, %ebx
	subq	$2, %r14
	cmpl	$-1, %ebx
	je	.L77
.L53:
	movzwl	32(%r15), %eax
.L54:
	testw	%ax, %ax
	jns	.L78
	movl	36(%r15), %edx
	cmpl	%ebx, %edx
	ja	.L79
.L69:
	movl	$-1, %edx
	movl	$65535, %esi
	movw	%dx, -70(%rbp)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L68:
	xorl	%r14d, %r14d
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L81:
	movswl	%ax, %edx
	sarl	$5, %edx
	cmpl	%r14d, %edx
	jle	.L58
.L82:
	cmpb	$0, -68(%rbp)
	je	.L59
	cmpl	%r12d, -60(%rbp)
	je	.L70
.L59:
	cmpl	%r14d, %edx
	jbe	.L71
	testb	$2, %al
	je	.L61
	leaq	34(%r15), %rax
.L62:
	movzwl	(%rax,%r14,2), %esi
	movl	%esi, %ebx
.L60:
	movq	88(%r15), %rdi
	call	_ZNK6icu_6723TransliterationRuleData13lookupMatcherEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L80
	movq	(%rax), %rax
	leaq	_ZThn8_N6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia(%rip), %rcx
	movsbl	-68(%rbp), %r8d
	leaq	-60(%rbp), %rdx
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L65
	subq	$8, %rdi
	movl	%r12d, %ecx
	movq	%r13, %rsi
	call	.LTHUNK4
.L66:
	cmpl	$2, %eax
	jne	.L36
.L64:
	movzwl	32(%r15), %eax
	addq	$1, %r14
.L37:
	testw	%ax, %ax
	jns	.L81
	movl	36(%r15), %edx
	cmpl	%r14d, %edx
	jg	.L82
.L58:
	movq	-80(%rbp), %rax
	movl	-60(%rbp), %edx
	movl	(%rax), %eax
	movl	%edx, 104(%r15)
	movl	%eax, 100(%r15)
.L55:
	movq	-80(%rbp), %rax
	movl	%edx, (%rax)
	movl	$2, %eax
.L36:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L83
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	48(%r15), %rax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L80:
	movl	-60(%rbp), %esi
	cmpl	%r12d, %esi
	jge	.L49
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*72(%rax)
	cmpw	%ax, %bx
	jne	.L49
	addl	$1, -60(%rbp)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$65535, %esi
	movl	$-1, %ebx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L65:
	movl	%r12d, %ecx
	movq	%r13, %rsi
	call	*%rax
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L38:
	movl	36(%rdi), %ebx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L44:
	movq	48(%r15), %rax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L76:
	movl	-60(%rbp), %esi
	cmpl	%r12d, %esi
	jle	.L49
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*72(%rax)
	cmpw	%ax, -70(%rbp)
	jne	.L49
	subl	$1, %ebx
	subl	$1, -60(%rbp)
	subq	$2, %r14
	cmpl	$-1, %ebx
	jne	.L53
.L77:
	movl	-60(%rbp), %edx
.L40:
	movl	100(%r15), %eax
	testl	%eax, %eax
	jns	.L55
	leal	1(%rdx), %eax
	movl	%eax, 100(%r15)
	movq	-80(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -68(%rbp)
	addl	$1, %eax
	movl	%eax, 104(%r15)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L51:
	movl	%r12d, %ecx
	movq	%r13, %rsi
	call	*%rax
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%eax, %eax
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$1, %eax
	jmp	.L36
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2458:
	.size	_ZN6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia, .-_ZN6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia
	.set	.LTHUNK4,_ZN6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia
	.p2align 4
	.globl	_ZThn8_N6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia
	.type	_ZThn8_N6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia, @function
_ZThn8_N6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia:
.LFB3194:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK4
	.cfi_endproc
.LFE3194:
	.size	_ZThn8_N6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia, .-_ZThn8_N6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringMatcher9toPatternERNS_13UnicodeStringEa
	.type	_ZNK6icu_6713StringMatcher9toPatternERNS_13UnicodeStringEa, @function
_ZNK6icu_6713StringMatcher9toPatternERNS_13UnicodeStringEa:
.LFB2459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rsi), %eax
	testb	$1, %al
	je	.L85
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L86:
	movl	96(%r12), %r8d
	movl	$2, %esi
	movl	$2, %edi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -192(%rbp)
	movw	%si, -184(%rbp)
	movq	%rax, -128(%rbp)
	movw	%di, -120(%rbp)
	testl	%r8d, %r8d
	jle	.L89
	movl	$40, %ecx
	leaq	-194(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movw	%cx, -194(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L89:
	leaq	-192(%rbp), %rax
	movsbl	%r13b, %r13d
	leaq	-128(%rbp), %r15
	xorl	%ebx, %ebx
	movq	%rax, -224(%rbp)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L109:
	movswl	%ax, %edx
	sarl	$5, %edx
	cmpl	%ebx, %edx
	jle	.L92
.L110:
	movl	$65535, %esi
	jbe	.L93
	testb	$2, %al
	je	.L94
	leaq	34(%r12), %rax
.L95:
	movzwl	(%rax,%rbx,2), %esi
.L93:
	movq	88(%r12), %rdi
	movl	%esi, -212(%rbp)
	call	_ZNK6icu_6723TransliterationRuleData13lookupMatcherEi@PLT
	movl	-212(%rbp), %esi
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L108
	movq	(%rax), %rax
	leaq	_ZThn8_NK6icu_6713StringMatcher9toPatternERNS_13UnicodeStringEa(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L98
	movq	-224(%rbp), %rsi
	subq	$8, %rdi
	movl	%r13d, %edx
	call	.LTHUNK5
	movq	%rax, %rsi
.L99:
	movq	%r15, %r8
	movl	%r13d, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringERKS1_aaS2_@PLT
.L97:
	addq	$1, %rbx
.L100:
	movzwl	32(%r12), %eax
	testw	%ax, %ax
	jns	.L109
	movl	36(%r12), %edx
	cmpl	%ebx, %edx
	jg	.L110
.L92:
	movl	96(%r12), %edx
	testl	%edx, %edx
	jle	.L101
	movl	$41, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	leaq	-194(%rbp), %rsi
	movw	%ax, -194(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L101:
	movq	%r15, %r8
	movl	%r13d, %ecx
	movl	$1, %edx
	movl	$-1, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-224(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	48(%r12), %rax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%r15, %r8
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	movq	-224(%rbp), %rsi
	movl	%r13d, %edx
	call	*%rax
	movq	%rax, %rsi
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L85:
	testw	%ax, %ax
	js	.L87
	movswl	%ax, %edx
	sarl	$5, %edx
.L88:
	testl	%edx, %edx
	je	.L86
	andl	$31, %eax
	movw	%ax, 8(%r14)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L87:
	movl	12(%rsi), %edx
	jmp	.L88
.L111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2459:
	.size	_ZNK6icu_6713StringMatcher9toPatternERNS_13UnicodeStringEa, .-_ZNK6icu_6713StringMatcher9toPatternERNS_13UnicodeStringEa
	.set	.LTHUNK5,_ZNK6icu_6713StringMatcher9toPatternERNS_13UnicodeStringEa
	.p2align 4
	.globl	_ZThn8_NK6icu_6713StringMatcher9toPatternERNS_13UnicodeStringEa
	.type	_ZThn8_NK6icu_6713StringMatcher9toPatternERNS_13UnicodeStringEa, @function
_ZThn8_NK6icu_6713StringMatcher9toPatternERNS_13UnicodeStringEa:
.LFB3195:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK5
	.cfi_endproc
.LFE3195:
	.size	_ZThn8_NK6icu_6713StringMatcher9toPatternERNS_13UnicodeStringEa, .-_ZThn8_NK6icu_6713StringMatcher9toPatternERNS_13UnicodeStringEa
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringMatcher17matchesIndexValueEh
	.type	_ZNK6icu_6713StringMatcher17matchesIndexValueEh, @function
_ZNK6icu_6713StringMatcher17matchesIndexValueEh:
.LFB2460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movswl	32(%rdi), %eax
	testw	%ax, %ax
	js	.L113
	sarl	$5, %eax
.L114:
	movl	$1, %r8d
	testl	%eax, %eax
	jne	.L121
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	leaq	24(%rbx), %rdi
	movzbl	%sil, %r12d
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	88(%rbx), %rdi
	movl	%eax, %esi
	movl	%eax, %r13d
	call	_ZNK6icu_6723TransliterationRuleData13lookupMatcherEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L122
	movq	(%rax), %rax
	leaq	_ZThn8_NK6icu_6713StringMatcher17matchesIndexValueEh(%rip), %rdx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L117
	addq	$8, %rsp
	movl	%r12d, %esi
	subq	$8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	.LTHUNK6
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movl	36(%rdi), %eax
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L122:
	movzbl	%r13b, %r13d
	cmpl	%r12d, %r13d
	sete	%r8b
	addq	$8, %rsp
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	addq	$8, %rsp
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE2460:
	.size	_ZNK6icu_6713StringMatcher17matchesIndexValueEh, .-_ZNK6icu_6713StringMatcher17matchesIndexValueEh
	.set	.LTHUNK6,_ZNK6icu_6713StringMatcher17matchesIndexValueEh
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringMatcher13addMatchSetToERNS_10UnicodeSetE
	.type	_ZNK6icu_6713StringMatcher13addMatchSetToERNS_10UnicodeSetE, @function
_ZNK6icu_6713StringMatcher13addMatchSetToERNS_10UnicodeSetE:
.LFB2461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZThn8_NK6icu_6713StringMatcher13addMatchSetToERNS_10UnicodeSetE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L135:
	sarl	$5, %eax
	cmpl	%eax, %r15d
	jge	.L123
.L136:
	leaq	24(%r12), %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	88(%r12), %rdi
	movl	%eax, %esi
	movl	%eax, %ebx
	call	_ZNK6icu_6723TransliterationRuleData13lookupMatcherEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L134
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%r14, %rax
	jne	.L129
	subq	$8, %rdi
	movq	%r13, %rsi
	call	.LTHUNK7
.L128:
	xorl	%eax, %eax
	cmpl	$65535, %ebx
	seta	%al
	leal	1(%r15,%rax), %r15d
.L131:
	movswl	32(%r12), %eax
	testw	%ax, %ax
	jns	.L135
	movl	36(%r12), %eax
	cmpl	%eax, %r15d
	jl	.L136
.L123:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%r13, %rsi
	call	*%rax
	jmp	.L128
	.cfi_endproc
.LFE2461:
	.size	_ZNK6icu_6713StringMatcher13addMatchSetToERNS_10UnicodeSetE, .-_ZNK6icu_6713StringMatcher13addMatchSetToERNS_10UnicodeSetE
	.set	.LTHUNK7,_ZNK6icu_6713StringMatcher13addMatchSetToERNS_10UnicodeSetE
	.p2align 4
	.globl	_ZThn8_NK6icu_6713StringMatcher13addMatchSetToERNS_10UnicodeSetE
	.type	_ZThn8_NK6icu_6713StringMatcher13addMatchSetToERNS_10UnicodeSetE, @function
_ZThn8_NK6icu_6713StringMatcher13addMatchSetToERNS_10UnicodeSetE:
.LFB3197:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK7
	.cfi_endproc
.LFE3197:
	.size	_ZThn8_NK6icu_6713StringMatcher13addMatchSetToERNS_10UnicodeSetE, .-_ZThn8_NK6icu_6713StringMatcher13addMatchSetToERNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringMatcher17toReplacerPatternERNS_13UnicodeStringEa
	.type	_ZNK6icu_6713StringMatcher17toReplacerPatternERNS_13UnicodeStringEa, @function
_ZNK6icu_6713StringMatcher17toReplacerPatternERNS_13UnicodeStringEa:
.LFB2463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rsi), %eax
	testb	$1, %al
	je	.L138
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L139:
	xorl	%edx, %edx
	movl	$36, %eax
	leaq	-26(%rbp), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%ax, -26(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	96(%rbx), %esi
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$10, %edx
	call	_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L147
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L140
	movswl	%ax, %edx
	sarl	$5, %edx
.L141:
	testl	%edx, %edx
	je	.L139
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L140:
	movl	12(%rsi), %edx
	jmp	.L141
.L147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2463:
	.size	_ZNK6icu_6713StringMatcher17toReplacerPatternERNS_13UnicodeStringEa, .-_ZNK6icu_6713StringMatcher17toReplacerPatternERNS_13UnicodeStringEa
	.p2align 4
	.globl	_ZThn8_N6icu_6713StringMatcherD0Ev
	.type	_ZThn8_N6icu_6713StringMatcherD0Ev, @function
_ZThn8_N6icu_6713StringMatcherD0Ev:
.LFB3185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	144+_ZTVN6icu_6713StringMatcherE(%rip), %rax
	leaq	-128(%rax), %rdx
	movq	%rax, %xmm1
	addq	$64, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 13, -24
	leaq	-8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	movq	%rax, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6715UnicodeReplacerD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3185:
	.size	_ZThn8_N6icu_6713StringMatcherD0Ev, .-_ZThn8_N6icu_6713StringMatcherD0Ev
	.p2align 4
	.globl	_ZThn16_N6icu_6713StringMatcherD0Ev
	.type	_ZThn16_N6icu_6713StringMatcherD0Ev, @function
_ZThn16_N6icu_6713StringMatcherD0Ev:
.LFB3184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	144+_ZTVN6icu_6713StringMatcherE(%rip), %rax
	leaq	-128(%rax), %rdx
	movq	%rax, %xmm1
	addq	$64, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	movq	%rax, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6715UnicodeReplacerD2Ev@PLT
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3184:
	.size	_ZThn16_N6icu_6713StringMatcherD0Ev, .-_ZThn16_N6icu_6713StringMatcherD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringMatcherD0Ev
	.type	_ZN6icu_6713StringMatcherD0Ev, @function
_ZN6icu_6713StringMatcherD0Ev:
.LFB2454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	144+_ZTVN6icu_6713StringMatcherE(%rip), %rax
	leaq	-128(%rax), %rdx
	movq	%rax, %xmm1
	addq	$64, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	24(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_6715UnicodeReplacerD2Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2454:
	.size	_ZN6icu_6713StringMatcherD0Ev, .-_ZN6icu_6713StringMatcherD0Ev
	.p2align 4
	.globl	_ZThn8_N6icu_6713StringMatcherD1Ev
	.type	_ZThn8_N6icu_6713StringMatcherD1Ev, @function
_ZThn8_N6icu_6713StringMatcherD1Ev:
.LFB3186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	144+_ZTVN6icu_6713StringMatcherE(%rip), %rax
	leaq	-128(%rax), %rdx
	movq	%rax, %xmm1
	addq	$64, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 13, -24
	leaq	-8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	movq	%rax, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6715UnicodeReplacerD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	.cfi_endproc
.LFE3186:
	.size	_ZThn8_N6icu_6713StringMatcherD1Ev, .-_ZThn8_N6icu_6713StringMatcherD1Ev
	.p2align 4
	.globl	_ZThn16_N6icu_6713StringMatcherD1Ev
	.type	_ZThn16_N6icu_6713StringMatcherD1Ev, @function
_ZThn16_N6icu_6713StringMatcherD1Ev:
.LFB3187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	144+_ZTVN6icu_6713StringMatcherE(%rip), %rax
	leaq	-128(%rax), %rdx
	movq	%rax, %xmm1
	addq	$64, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	movq	%rax, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6715UnicodeReplacerD2Ev@PLT
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_6714UnicodeMatcherD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	.cfi_endproc
.LFE3187:
	.size	_ZThn16_N6icu_6713StringMatcherD1Ev, .-_ZThn16_N6icu_6713StringMatcherD1Ev
	.p2align 4
	.globl	_ZThn16_NK6icu_6713StringMatcher17toReplacerPatternERNS_13UnicodeStringEa
	.type	_ZThn16_NK6icu_6713StringMatcher17toReplacerPatternERNS_13UnicodeStringEa, @function
_ZThn16_NK6icu_6713StringMatcher17toReplacerPatternERNS_13UnicodeStringEa:
.LFB3188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rsi), %eax
	testb	$1, %al
	je	.L159
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L160:
	xorl	%edx, %edx
	movl	$36, %eax
	leaq	-26(%rbp), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%ax, -26(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	80(%rbx), %esi
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$10, %edx
	call	_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L168
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L161
	movswl	%ax, %edx
	sarl	$5, %edx
.L162:
	testl	%edx, %edx
	je	.L160
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L161:
	movl	12(%rsi), %edx
	jmp	.L162
.L168:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3188:
	.size	_ZThn16_NK6icu_6713StringMatcher17toReplacerPatternERNS_13UnicodeStringEa, .-_ZThn16_NK6icu_6713StringMatcher17toReplacerPatternERNS_13UnicodeStringEa
	.p2align 4
	.globl	_ZThn16_N6icu_6713StringMatcher7replaceERNS_11ReplaceableEiiRi
	.type	_ZThn16_N6icu_6713StringMatcher7replaceERNS_11ReplaceableEiiRi, @function
_ZThn16_N6icu_6713StringMatcher7replaceERNS_11ReplaceableEiiRi:
.LFB3189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	84(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	testl	%esi, %esi
	js	.L170
	movl	88(%rdi), %edx
	movq	%rdi, %rbx
	cmpl	%edx, %esi
	je	.L170
	movq	%r12, %rdi
	call	*40(%rax)
	movq	(%r12), %rax
	movl	88(%rbx), %r15d
	subl	84(%rbx), %r15d
.L170:
	movl	$2, %edx
	leaq	-128(%rbp), %rbx
	movl	%r14d, %esi
	movq	%r12, %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movw	%dx, -120(%rbp)
	movl	%r13d, %edx
	movq	%rcx, -128(%rbp)
	movq	%rbx, %rcx
	call	*32(%rax)
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L175
	addq	$88, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L175:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3189:
	.size	_ZThn16_N6icu_6713StringMatcher7replaceERNS_11ReplaceableEiiRi, .-_ZThn16_N6icu_6713StringMatcher7replaceERNS_11ReplaceableEiiRi
	.p2align 4
	.globl	_ZThn8_NK6icu_6713StringMatcher17matchesIndexValueEh
	.type	_ZThn8_NK6icu_6713StringMatcher17matchesIndexValueEh, @function
_ZThn8_NK6icu_6713StringMatcher17matchesIndexValueEh:
.LFB3190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movswl	24(%rdi), %eax
	testw	%ax, %ax
	js	.L177
	sarl	$5, %eax
.L178:
	movl	$1, %r8d
	testl	%eax, %eax
	jne	.L185
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	leaq	16(%rbx), %rdi
	movzbl	%sil, %r12d
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	80(%rbx), %rdi
	movl	%eax, %esi
	movl	%eax, %r13d
	call	_ZNK6icu_6723TransliterationRuleData13lookupMatcherEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L186
	movq	(%rax), %rax
	leaq	_ZThn8_NK6icu_6713StringMatcher17matchesIndexValueEh(%rip), %rdx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L187
	addq	$8, %rsp
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movl	28(%rdi), %eax
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L186:
	movzbl	%r13b, %r13d
	cmpl	%r12d, %r13d
	sete	%r8b
	addq	$8, %rsp
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L187:
	.cfi_restore_state
	popq	%rax
	movl	%r12d, %esi
	popq	%rbx
	subq	$8, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	.LTHUNK6
	.cfi_endproc
.LFE3190:
	.size	_ZThn8_NK6icu_6713StringMatcher17matchesIndexValueEh, .-_ZThn8_NK6icu_6713StringMatcher17matchesIndexValueEh
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringMatcher16getStaticClassIDEv
	.type	_ZN6icu_6713StringMatcher16getStaticClassIDEv, @function
_ZN6icu_6713StringMatcher16getStaticClassIDEv:
.LFB2422:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713StringMatcher16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2422:
	.size	_ZN6icu_6713StringMatcher16getStaticClassIDEv, .-_ZN6icu_6713StringMatcher16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringMatcherC2ERKNS_13UnicodeStringEiiiRKNS_23TransliterationRuleDataE
	.type	_ZN6icu_6713StringMatcherC2ERKNS_13UnicodeStringEiiiRKNS_23TransliterationRuleDataE, @function
_ZN6icu_6713StringMatcherC2ERKNS_13UnicodeStringEiiiRKNS_23TransliterationRuleDataE:
.LFB2434:
	.cfi_startproc
	endbr64
	leaq	144+_ZTVN6icu_6713StringMatcherE(%rip), %rax
	movq	%rsi, %r10
	movl	%edx, %esi
	movl	%ecx, %edx
	leaq	-128(%rax), %rcx
	movq	%rax, %xmm1
	movq	%r9, 88(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm2
	addq	$192, %rcx
	movl	$2, %eax
	punpcklqdq	%xmm1, %xmm0
	movw	%ax, 32(%rdi)
	movq	(%r10), %rax
	movups	%xmm0, (%rdi)
	movq	%rcx, %xmm0
	leaq	24(%rdi), %rcx
	punpcklqdq	%xmm2, %xmm0
	movl	%r8d, 96(%rdi)
	movq	24(%rax), %rax
	movq	$-1, 100(%rdi)
	movups	%xmm0, 16(%rdi)
	movq	%r10, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE2434:
	.size	_ZN6icu_6713StringMatcherC2ERKNS_13UnicodeStringEiiiRKNS_23TransliterationRuleDataE, .-_ZN6icu_6713StringMatcherC2ERKNS_13UnicodeStringEiiiRKNS_23TransliterationRuleDataE
	.globl	_ZN6icu_6713StringMatcherC1ERKNS_13UnicodeStringEiiiRKNS_23TransliterationRuleDataE
	.set	_ZN6icu_6713StringMatcherC1ERKNS_13UnicodeStringEiiiRKNS_23TransliterationRuleDataE,_ZN6icu_6713StringMatcherC2ERKNS_13UnicodeStringEiiiRKNS_23TransliterationRuleDataE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringMatcherC2ERKS0_
	.type	_ZN6icu_6713StringMatcherC2ERKS0_, @function
_ZN6icu_6713StringMatcherC2ERKS0_:
.LFB2449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	144+_ZTVN6icu_6713StringMatcherE(%rip), %rax
	leaq	-128(%rax), %rcx
	movq	%rax, %xmm1
	addq	$64, %rax
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	24(%rsi), %rsi
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	movq	%rax, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	88(%r12), %rdx
	movl	104(%r12), %eax
	movq	%rdx, 88(%rbx)
	movq	96(%r12), %rdx
	movl	%eax, 104(%rbx)
	movq	%rdx, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2449:
	.size	_ZN6icu_6713StringMatcherC2ERKS0_, .-_ZN6icu_6713StringMatcherC2ERKS0_
	.globl	_ZN6icu_6713StringMatcherC1ERKS0_
	.set	_ZN6icu_6713StringMatcherC1ERKS0_,_ZN6icu_6713StringMatcherC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringMatcher10resetMatchEv
	.type	_ZN6icu_6713StringMatcher10resetMatchEv, @function
_ZN6icu_6713StringMatcher10resetMatchEv:
.LFB2464:
	.cfi_startproc
	endbr64
	movq	$-1, 100(%rdi)
	ret
	.cfi_endproc
.LFE2464:
	.size	_ZN6icu_6713StringMatcher10resetMatchEv, .-_ZN6icu_6713StringMatcher10resetMatchEv
	.weak	_ZTSN6icu_6713StringMatcherE
	.section	.rodata._ZTSN6icu_6713StringMatcherE,"aG",@progbits,_ZTSN6icu_6713StringMatcherE,comdat
	.align 16
	.type	_ZTSN6icu_6713StringMatcherE, @object
	.size	_ZTSN6icu_6713StringMatcherE, 25
_ZTSN6icu_6713StringMatcherE:
	.string	"N6icu_6713StringMatcherE"
	.weak	_ZTIN6icu_6713StringMatcherE
	.section	.data.rel.ro._ZTIN6icu_6713StringMatcherE,"awG",@progbits,_ZTIN6icu_6713StringMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_6713StringMatcherE, @object
	.size	_ZTIN6icu_6713StringMatcherE, 72
_ZTIN6icu_6713StringMatcherE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_6713StringMatcherE
	.long	0
	.long	3
	.quad	_ZTIN6icu_6714UnicodeFunctorE
	.quad	2
	.quad	_ZTIN6icu_6714UnicodeMatcherE
	.quad	2050
	.quad	_ZTIN6icu_6715UnicodeReplacerE
	.quad	4098
	.weak	_ZTVN6icu_6713StringMatcherE
	.section	.data.rel.ro.local._ZTVN6icu_6713StringMatcherE,"awG",@progbits,_ZTVN6icu_6713StringMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_6713StringMatcherE, @object
	.size	_ZTVN6icu_6713StringMatcherE, 248
_ZTVN6icu_6713StringMatcherE:
	.quad	0
	.quad	_ZTIN6icu_6713StringMatcherE
	.quad	_ZN6icu_6713StringMatcherD1Ev
	.quad	_ZN6icu_6713StringMatcherD0Ev
	.quad	_ZNK6icu_6713StringMatcher17getDynamicClassIDEv
	.quad	_ZNK6icu_6713StringMatcher5cloneEv
	.quad	_ZNK6icu_6713StringMatcher9toMatcherEv
	.quad	_ZNK6icu_6713StringMatcher10toReplacerEv
	.quad	_ZN6icu_6713StringMatcher7setDataEPKNS_23TransliterationRuleDataE
	.quad	_ZN6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia
	.quad	_ZNK6icu_6713StringMatcher9toPatternERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6713StringMatcher17matchesIndexValueEh
	.quad	_ZNK6icu_6713StringMatcher13addMatchSetToERNS_10UnicodeSetE
	.quad	_ZN6icu_6713StringMatcher7replaceERNS_11ReplaceableEiiRi
	.quad	_ZNK6icu_6713StringMatcher17toReplacerPatternERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6713StringMatcher19addReplacementSetToERNS_10UnicodeSetE
	.quad	-8
	.quad	_ZTIN6icu_6713StringMatcherE
	.quad	_ZThn8_N6icu_6713StringMatcherD1Ev
	.quad	_ZThn8_N6icu_6713StringMatcherD0Ev
	.quad	_ZThn8_N6icu_6713StringMatcher7matchesERKNS_11ReplaceableERiia
	.quad	_ZThn8_NK6icu_6713StringMatcher9toPatternERNS_13UnicodeStringEa
	.quad	_ZThn8_NK6icu_6713StringMatcher17matchesIndexValueEh
	.quad	_ZThn8_NK6icu_6713StringMatcher13addMatchSetToERNS_10UnicodeSetE
	.quad	-16
	.quad	_ZTIN6icu_6713StringMatcherE
	.quad	_ZThn16_N6icu_6713StringMatcherD1Ev
	.quad	_ZThn16_N6icu_6713StringMatcherD0Ev
	.quad	_ZThn16_N6icu_6713StringMatcher7replaceERNS_11ReplaceableEiiRi
	.quad	_ZThn16_NK6icu_6713StringMatcher17toReplacerPatternERNS_13UnicodeStringEa
	.quad	_ZThn16_NK6icu_6713StringMatcher19addReplacementSetToERNS_10UnicodeSetE
	.local	_ZZN6icu_6713StringMatcher16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6713StringMatcher16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
