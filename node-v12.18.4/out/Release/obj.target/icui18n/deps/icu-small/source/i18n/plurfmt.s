	.file	"plurfmt.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712PluralFormat17getDynamicClassIDEv
	.type	_ZNK6icu_6712PluralFormat17getDynamicClassIDEv, @function
_ZNK6icu_6712PluralFormat17getDynamicClassIDEv:
.LFB3453:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712PluralFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3453:
	.size	_ZNK6icu_6712PluralFormat17getDynamicClassIDEv, .-_ZNK6icu_6712PluralFormat17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712PluralFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.type	_ZNK6icu_6712PluralFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, @function
_ZNK6icu_6712PluralFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE:
.LFB3507:
	.cfi_startproc
	endbr64
	movl	8(%rcx), %eax
	movl	%eax, 12(%rcx)
	ret
	.cfi_endproc
.LFE3507:
	.size	_ZNK6icu_6712PluralFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, .-_ZNK6icu_6712PluralFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormat21PluralSelectorAdapterD2Ev
	.type	_ZN6icu_6712PluralFormat21PluralSelectorAdapterD2Ev, @function
_ZN6icu_6712PluralFormat21PluralSelectorAdapterD2Ev:
.LFB3515:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L4:
	ret
	.cfi_endproc
.LFE3515:
	.size	_ZN6icu_6712PluralFormat21PluralSelectorAdapterD2Ev, .-_ZN6icu_6712PluralFormat21PluralSelectorAdapterD2Ev
	.globl	_ZN6icu_6712PluralFormat21PluralSelectorAdapterD1Ev
	.set	_ZN6icu_6712PluralFormat21PluralSelectorAdapterD1Ev,_ZN6icu_6712PluralFormat21PluralSelectorAdapterD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormat21PluralSelectorAdapterD0Ev
	.type	_ZN6icu_6712PluralFormat21PluralSelectorAdapterD0Ev, @function
_ZN6icu_6712PluralFormat21PluralSelectorAdapterD0Ev:
.LFB3517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L7
	movq	(%rdi), %rax
	call	*8(%rax)
.L7:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3517:
	.size	_ZN6icu_6712PluralFormat21PluralSelectorAdapterD0Ev, .-_ZN6icu_6712PluralFormat21PluralSelectorAdapterD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712PluralFormat21PluralSelectorAdapter6selectEPvdR10UErrorCode
	.type	_ZNK6icu_6712PluralFormat21PluralSelectorAdapter6selectEPvdR10UErrorCode, @function
_ZNK6icu_6712PluralFormat21PluralSelectorAdapter6selectEPvdR10UErrorCode:
.LFB3518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3518:
	.size	_ZNK6icu_6712PluralFormat21PluralSelectorAdapter6selectEPvdR10UErrorCode, .-_ZNK6icu_6712PluralFormat21PluralSelectorAdapter6selectEPvdR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6712PluralFormat14findSubMessageERKNS_14MessagePatternEiRKNS0_14PluralSelectorEPvdR10UErrorCode.part.0, @function
_ZN6icu_6712PluralFormat14findSubMessageERKNS_14MessagePatternEiRKNS0_14PluralSelectorEPvdR10UErrorCode.part.0:
.LFB4794:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm2, %xmm2
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -320(%rbp)
	movq	%rcx, -328(%rbp)
	movq	%r8, -352(%rbp)
	movsd	%xmm0, -304(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	96(%rdi), %eax
	movsd	%xmm2, -344(%rbp)
	movl	%eax, -276(%rbp)
	movslq	%esi, %rax
	movq	%rax, %rsi
	salq	$4, %rsi
	addq	88(%rdi), %rsi
	movl	(%rsi), %edi
	leal	-12(%rdi), %edx
	cmpl	$1, %edx
	jbe	.L78
.L17:
	movq	%rax, -288(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	xorl	%esi, %esi
	movq	%rax, -256(%rbp)
	leaq	_ZN6icu_67L12OTHER_STRINGE(%rip), %rax
	leaq	-264(%rbp), %rdx
	movq	%rax, -264(%rbp)
	leaq	-192(%rbp), %rax
	movw	%cx, -248(%rbp)
	movq	%rax, %rdi
	movl	$5, %ecx
	movq	%rax, -336(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-256(%rbp), %rax
	movq	88(%r14), %rdi
	xorl	%ecx, %ecx
	movl	$0, -280(%rbp)
	movq	%rax, -312(%rbp)
	movq	-288(%rbp), %rax
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L19:
	testb	%cl, %cl
	jne	.L21
	movzwl	-184(%rbp), %eax
	leaq	16(%r14), %r15
	movl	%eax, %edx
	andl	$1, %edx
	testw	%ax, %ax
	js	.L22
	testb	%dl, %dl
	jne	.L23
	movswl	%ax, %r9d
	xorl	%r8d, %r8d
	sarl	$5, %r9d
.L61:
	movl	%r9d, %edx
	subl	%r8d, %edx
	cmpl	%r9d, %edx
	cmovle	%edx, %r9d
.L27:
	testb	$2, %al
	movzwl	8(%r12), %edx
	movl	4(%r12), %esi
	movq	%r15, %rdi
	leaq	-182(%rbp), %rcx
	cmove	-168(%rbp), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
.L26:
	testb	%al, %al
	jne	.L29
	movl	-280(%rbp), %edx
	testl	%edx, %edx
	je	.L30
	movq	88(%r14), %rdi
	xorl	%ecx, %ecx
	leaq	(%rdi,%r13), %rsi
	.p2align 4,,10
	.p2align 3
.L21:
	cmpl	%ebx, 12(%rsi)
	cmovge	12(%rsi), %ebx
	leal	1(%rbx), %r15d
	cmpl	%r15d, -276(%rbp)
	jle	.L18
	movslq	%r15d, %rax
.L59:
	salq	$4, %rax
	leal	1(%r15), %ebx
	leaq	(%rdi,%rax), %r12
	cmpl	$6, (%r12)
	je	.L18
	leaq	16(%rax), %r13
	leaq	(%rdi,%r13), %rsi
	movl	(%rsi), %edx
	subl	$12, %edx
	cmpl	$1, %edx
	ja	.L19
	movq	%r14, %rdi
	movb	%cl, -296(%rbp)
	leal	2(%r15), %ebx
	movq	%rax, -288(%rbp)
	call	_ZNK6icu_6714MessagePattern15getNumericValueERKNS0_4PartE@PLT
	ucomisd	-304(%rbp), %xmm0
	movq	-288(%rbp), %rax
	movzbl	-296(%rbp), %ecx
	jnp	.L79
.L72:
	movq	88(%r14), %rdi
	leaq	32(%rdi,%rax), %rsi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L29:
	movswl	-248(%rbp), %edx
	movl	%edx, %eax
	sarl	$5, %edx
	movl	%edx, -288(%rbp)
	jne	.L39
	movq	-320(%rbp), %rcx
	leaq	_ZNK6icu_6712PluralFormat21PluralSelectorAdapter6selectEPvdR10UErrorCode(%rip), %rdi
	movq	(%rcx), %rax
	movq	16(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L40
	leaq	-128(%rbp), %r8
	movq	8(%rcx), %rsi
	movq	-328(%rbp), %rdx
	movq	%r8, %rdi
	movq	%r8, -296(%rbp)
	call	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE@PLT
	movq	-296(%rbp), %r8
.L41:
	movq	-312(%rbp), %rdi
	movq	%r8, %rsi
	movq	%r8, -296(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-296(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-280(%rbp), %eax
	testl	%eax, %eax
	jne	.L42
.L51:
	movzwl	-248(%rbp), %eax
.L39:
	movl	%eax, %edx
	andl	$1, %edx
	testw	%ax, %ax
	js	.L52
	testb	%dl, %dl
	jne	.L53
	movswl	%ax, %r9d
	xorl	%r8d, %r8d
	sarl	$5, %r9d
.L60:
	movl	%r9d, %edx
	subl	%r8d, %edx
	cmpl	%r9d, %edx
	cmovle	%edx, %r9d
.L57:
	testb	$2, %al
	movzwl	8(%r12), %edx
	movl	4(%r12), %esi
	movq	%r15, %rdi
	leaq	-246(%rbp), %rcx
	cmove	-232(%rbp), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
.L56:
	movl	-280(%rbp), %ecx
	testb	%al, %al
	movq	88(%r14), %rdi
	cmove	%ebx, %ecx
	leaq	(%rdi,%r13), %rsi
	movl	%ecx, -280(%rbp)
	sete	%cl
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L23:
	movzbl	24(%r14), %eax
	notl	%eax
	andl	$1, %eax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L22:
	movl	-180(%rbp), %r9d
	testb	%dl, %dl
	jne	.L23
	testl	%r9d, %r9d
	movl	$0, %r8d
	cmovle	%r9d, %r8d
	jns	.L61
	xorl	%r9d, %r9d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L52:
	movl	-244(%rbp), %r9d
	testb	%dl, %dl
	je	.L55
.L53:
	movzbl	24(%r14), %eax
	notl	%eax
	andl	$1, %eax
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L79:
	jne	.L72
	movl	%ebx, -280(%rbp)
	.p2align 4,,10
	.p2align 3
.L18:
	movq	-336(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-312(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	movl	-280(%rbp), %eax
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movzwl	-184(%rbp), %esi
	testw	%si, %si
	js	.L31
	movzwl	-248(%rbp), %eax
	movswl	%si, %ecx
	sarl	$5, %ecx
	testw	%ax, %ax
	js	.L33
.L81:
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	$1, %sil
	je	.L35
.L82:
	notl	%eax
	andl	$1, %eax
.L36:
	movq	88(%r14), %rdi
	testb	%al, %al
	movl	%ebx, -280(%rbp)
	sete	%cl
	leaq	(%rdi,%r13), %rsi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L55:
	testl	%r9d, %r9d
	movl	$0, %r8d
	cmovle	%r9d, %r8d
	jns	.L60
	xorl	%r9d, %r9d
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L31:
	movzwl	-248(%rbp), %eax
	movl	-180(%rbp), %ecx
	testw	%ax, %ax
	jns	.L81
.L33:
	movl	-244(%rbp), %edx
	testb	$1, %sil
	jne	.L82
.L35:
	testl	%ecx, %ecx
	movl	$0, %r8d
	cmovle	%ecx, %r8d
	js	.L37
	movl	%ecx, %eax
	subl	%r8d, %eax
	cmpl	%ecx, %eax
	cmovle	%eax, %ecx
	movl	%ecx, -280(%rbp)
.L37:
	andl	$2, %esi
	leaq	-182(%rbp), %rcx
	movl	-280(%rbp), %r9d
	cmove	-168(%rbp), %rcx
	movq	-312(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L42:
	movzwl	-184(%rbp), %esi
	testw	%si, %si
	js	.L43
	movswl	%si, %ecx
	sarl	$5, %ecx
.L44:
	movzwl	-248(%rbp), %eax
	testw	%ax, %ax
	js	.L45
	movswl	%ax, %edx
	sarl	$5, %edx
.L46:
	testb	$1, %sil
	jne	.L83
	testl	%ecx, %ecx
	movl	$0, %r8d
	cmovle	%ecx, %r8d
	js	.L49
	movl	%ecx, %eax
	subl	%r8d, %eax
	cmpl	%ecx, %eax
	cmovle	%eax, %ecx
	movl	%ecx, -288(%rbp)
.L49:
	andl	$2, %esi
	leaq	-182(%rbp), %rcx
	movl	-288(%rbp), %r9d
	cmove	-168(%rbp), %rcx
	movq	-312(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
.L48:
	testb	%al, %al
	jne	.L51
	movq	88(%r14), %rdi
	movl	$1, %ecx
	leaq	(%rdi,%r13), %rsi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%r14, %rdi
	addl	$1, %r15d
	call	_ZNK6icu_6714MessagePattern15getNumericValueERKNS0_4PartE@PLT
	movslq	%r15d, %rax
	movsd	%xmm0, -344(%rbp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	-128(%rbp), %r8
	movsd	-304(%rbp), %xmm0
	movq	%rcx, %rsi
	subsd	-344(%rbp), %xmm0
	movq	%r8, -296(%rbp)
	movq	%r8, %rdi
	movq	-352(%rbp), %rcx
	movq	-328(%rbp), %rdx
	call	*%rax
	movq	-296(%rbp), %r8
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L83:
	notl	%eax
	andl	$1, %eax
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L45:
	movl	-244(%rbp), %edx
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L43:
	movl	-180(%rbp), %ecx
	jmp	.L44
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4794:
	.size	_ZN6icu_6712PluralFormat14findSubMessageERKNS_14MessagePatternEiRKNS0_14PluralSelectorEPvdR10UErrorCode.part.0, .-_ZN6icu_6712PluralFormat14findSubMessageERKNS_14MessagePatternEiRKNS0_14PluralSelectorEPvdR10UErrorCode.part.0
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB4041:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4041:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB4044:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L97
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L85
	cmpb	$0, 12(%rbx)
	jne	.L98
.L89:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L85:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L89
	.cfi_endproc
.LFE4044:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB4047:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L101
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4047:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB4050:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L104
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE4050:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB4052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L110
.L106:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L111
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4052:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB4053:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4053:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB4054:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4054:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB4055:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4055:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4056:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4056:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4057:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4057:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB4058:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L127
	testl	%edx, %edx
	jle	.L127
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L130
.L119:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L119
	.cfi_endproc
.LFE4058:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB4059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L134
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L134
	testl	%r12d, %r12d
	jg	.L141
	cmpb	$0, 12(%rbx)
	jne	.L142
.L136:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L136
.L142:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L134:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4059:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB4060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L144
	movq	(%rdi), %r8
.L145:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L148
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L148
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L148:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4060:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB4061:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L155
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4061:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB4062:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4062:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB4063:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4063:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB4064:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4064:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB4066:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4066:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB4068:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4068:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormat16getStaticClassIDEv
	.type	_ZN6icu_6712PluralFormat16getStaticClassIDEv, @function
_ZN6icu_6712PluralFormat16getStaticClassIDEv:
.LFB3452:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712PluralFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3452:
	.size	_ZN6icu_6712PluralFormat16getStaticClassIDEv, .-_ZN6icu_6712PluralFormat16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormatC2ER10UErrorCode
	.type	_ZN6icu_6712PluralFormatC2ER10UErrorCode, @function
_ZN6icu_6712PluralFormatC2ER10UErrorCode:
.LFB3455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	328(%rbx), %r13
	subq	$8, %rsp
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712PluralFormatE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	552(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	$0, 680(%rbx)
	movq	%rax, 696(%rbx)
	movl	(%r12), %eax
	movq	$0x000000000, 688(%rbx)
	movq	$0, 704(%rbx)
	testl	%eax, %eax
	jg	.L162
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, 704(%rbx)
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 680(%rbx)
.L162:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3455:
	.size	_ZN6icu_6712PluralFormatC2ER10UErrorCode, .-_ZN6icu_6712PluralFormatC2ER10UErrorCode
	.globl	_ZN6icu_6712PluralFormatC1ER10UErrorCode
	.set	_ZN6icu_6712PluralFormatC1ER10UErrorCode,_ZN6icu_6712PluralFormatC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6712PluralFormatC2ERKNS_6LocaleER10UErrorCode:
.LFB3458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	328(%rbx), %r14
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712PluralFormatE(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, (%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	552(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	$0, 680(%rbx)
	movq	%rax, 696(%rbx)
	movl	(%r12), %eax
	movq	$0x000000000, 688(%rbx)
	movq	$0, 704(%rbx)
	testl	%eax, %eax
	jg	.L165
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, 704(%rbx)
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 680(%rbx)
.L165:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3458:
	.size	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6712PluralFormatC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6712PluralFormatC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6712PluralFormatC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6712PluralFormatC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormatC2ERKNS_11PluralRulesER10UErrorCode
	.type	_ZN6icu_6712PluralFormatC2ERKNS_11PluralRulesER10UErrorCode, @function
_ZN6icu_6712PluralFormatC2ERKNS_11PluralRulesER10UErrorCode:
.LFB3461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	328(%rbx), %r14
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712PluralFormatE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	552(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	$0, 680(%rbx)
	movq	%rax, 696(%rbx)
	movl	(%r12), %eax
	movq	$0x000000000, 688(%rbx)
	movq	$0, 704(%rbx)
	testl	%eax, %eax
	jg	.L168
	movq	%r13, %rdi
	call	_ZNK6icu_6711PluralRules5cloneEv@PLT
	movq	%rax, 704(%rbx)
	testq	%rax, %rax
	je	.L172
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 680(%rbx)
.L168:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L168
	.cfi_endproc
.LFE3461:
	.size	_ZN6icu_6712PluralFormatC2ERKNS_11PluralRulesER10UErrorCode, .-_ZN6icu_6712PluralFormatC2ERKNS_11PluralRulesER10UErrorCode
	.globl	_ZN6icu_6712PluralFormatC1ERKNS_11PluralRulesER10UErrorCode
	.set	_ZN6icu_6712PluralFormatC1ERKNS_11PluralRulesER10UErrorCode,_ZN6icu_6712PluralFormatC2ERKNS_11PluralRulesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_11PluralRulesER10UErrorCode
	.type	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_11PluralRulesER10UErrorCode, @function
_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_11PluralRulesER10UErrorCode:
.LFB3464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	328(%rbx), %r15
	subq	$8, %rsp
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712PluralFormatE(%rip), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, (%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	552(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	$0, 680(%rbx)
	movq	%rax, 696(%rbx)
	movl	(%r12), %eax
	movq	$0x000000000, 688(%rbx)
	movq	$0, 704(%rbx)
	testl	%eax, %eax
	jg	.L173
	movq	%r14, %rdi
	call	_ZNK6icu_6711PluralRules5cloneEv@PLT
	movq	%rax, 704(%rbx)
	testq	%rax, %rax
	je	.L177
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 680(%rbx)
.L173:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L173
	.cfi_endproc
.LFE3464:
	.size	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_11PluralRulesER10UErrorCode, .-_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_11PluralRulesER10UErrorCode
	.globl	_ZN6icu_6712PluralFormatC1ERKNS_6LocaleERKNS_11PluralRulesER10UErrorCode
	.set	_ZN6icu_6712PluralFormatC1ERKNS_6LocaleERKNS_11PluralRulesER10UErrorCode,_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_11PluralRulesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleE11UPluralTypeR10UErrorCode
	.type	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleE11UPluralTypeR10UErrorCode, @function
_ZN6icu_6712PluralFormatC2ERKNS_6LocaleE11UPluralTypeR10UErrorCode:
.LFB3467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	328(%rbx), %r15
	subq	$8, %rsp
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712PluralFormatE(%rip), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, (%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	552(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	$0, 680(%rbx)
	movq	%rax, 696(%rbx)
	movl	(%r12), %eax
	movq	$0x000000000, 688(%rbx)
	movq	$0, 704(%rbx)
	testl	%eax, %eax
	jg	.L178
	movq	%r12, %rdx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, 704(%rbx)
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 680(%rbx)
.L178:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3467:
	.size	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleE11UPluralTypeR10UErrorCode, .-_ZN6icu_6712PluralFormatC2ERKNS_6LocaleE11UPluralTypeR10UErrorCode
	.globl	_ZN6icu_6712PluralFormatC1ERKNS_6LocaleE11UPluralTypeR10UErrorCode
	.set	_ZN6icu_6712PluralFormatC1ERKNS_6LocaleE11UPluralTypeR10UErrorCode,_ZN6icu_6712PluralFormatC2ERKNS_6LocaleE11UPluralTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712PluralFormatC2ERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712PluralFormatC2ERKNS_13UnicodeStringER10UErrorCode:
.LFB3470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	328(%rbx), %r14
	leaq	552(%rbx), %r15
	subq	$8, %rsp
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712PluralFormatE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	movl	(%r12), %edx
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	$0, 680(%rbx)
	movq	$0x000000000, 688(%rbx)
	movq	%rax, 696(%rbx)
	movq	$0, 704(%rbx)
	testl	%edx, %edx
	jg	.L182
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, 704(%rbx)
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 680(%rbx)
.L182:
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6714MessagePattern16parsePluralStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L186
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6714MessagePattern15getPluralOffsetEi@PLT
	movsd	%xmm0, 688(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	$0x000000000, 688(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3470:
	.size	_ZN6icu_6712PluralFormatC2ERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712PluralFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6712PluralFormatC1ERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6712PluralFormatC1ERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6712PluralFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_13UnicodeStringER10UErrorCode:
.LFB3473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	328(%rbx), %r15
	subq	$8, %rsp
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712PluralFormatE(%rip), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, (%rbx)
	leaq	552(%rbx), %r14
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	movl	(%r12), %edx
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	$0, 680(%rbx)
	movq	$0x000000000, 688(%rbx)
	movq	%rax, 696(%rbx)
	movq	$0, 704(%rbx)
	testl	%edx, %edx
	jg	.L188
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, 704(%rbx)
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 680(%rbx)
.L188:
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern16parsePluralStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L192
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6714MessagePattern15getPluralOffsetEi@PLT
	movsd	%xmm0, 688(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	$0x000000000, 688(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3473:
	.size	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6712PluralFormatC1ERKNS_6LocaleERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6712PluralFormatC1ERKNS_6LocaleERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormatC2ERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712PluralFormatC2ERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712PluralFormatC2ERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode:
.LFB3476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	328(%rbx), %r15
	leaq	552(%rbx), %r14
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712PluralFormatE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	movl	(%r12), %edx
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	$0, 680(%rbx)
	movq	$0x000000000, 688(%rbx)
	movq	%rax, 696(%rbx)
	movq	$0, 704(%rbx)
	testl	%edx, %edx
	jg	.L194
	movq	-56(%rbp), %rdi
	call	_ZNK6icu_6711PluralRules5cloneEv@PLT
	movq	%rax, 704(%rbx)
	testq	%rax, %rax
	je	.L199
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 680(%rbx)
.L194:
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern16parsePluralStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L200
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6714MessagePattern15getPluralOffsetEi@PLT
	movsd	%xmm0, 688(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	$0x000000000, 688(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L194
	.cfi_endproc
.LFE3476:
	.size	_ZN6icu_6712PluralFormatC2ERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712PluralFormatC2ERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6712PluralFormatC1ERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6712PluralFormatC1ERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6712PluralFormatC2ERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode:
.LFB3479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	328(%rbx), %r15
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712PluralFormatE(%rip), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, (%rbx)
	leaq	552(%rbx), %r14
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	movl	(%r12), %edx
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	$0, 680(%rbx)
	movq	$0x000000000, 688(%rbx)
	movq	%rax, 696(%rbx)
	movq	$0, 704(%rbx)
	testl	%edx, %edx
	jg	.L202
	movq	-56(%rbp), %rdi
	call	_ZNK6icu_6711PluralRules5cloneEv@PLT
	movq	%rax, 704(%rbx)
	testq	%rax, %rax
	je	.L207
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 680(%rbx)
.L202:
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern16parsePluralStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L208
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6714MessagePattern15getPluralOffsetEi@PLT
	movsd	%xmm0, 688(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	$0x000000000, 688(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L202
	.cfi_endproc
.LFE3479:
	.size	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6712PluralFormatC1ERKNS_6LocaleERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6712PluralFormatC1ERKNS_6LocaleERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6712PluralFormatC2ERKNS_6LocaleERKNS_11PluralRulesERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleE11UPluralTypeRKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleE11UPluralTypeRKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712PluralFormatC2ERKNS_6LocaleE11UPluralTypeRKNS_13UnicodeStringER10UErrorCode:
.LFB3482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	328(%rbx), %r15
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712PluralFormatE(%rip), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, (%rbx)
	leaq	552(%rbx), %r14
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	movl	(%r12), %edx
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	$0, 680(%rbx)
	movq	$0x000000000, 688(%rbx)
	movq	%rax, 696(%rbx)
	movq	$0, 704(%rbx)
	testl	%edx, %edx
	jg	.L210
	movl	-52(%rbp), %esi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, 704(%rbx)
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 680(%rbx)
.L210:
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern16parsePluralStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L214
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6714MessagePattern15getPluralOffsetEi@PLT
	movsd	%xmm0, 688(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	$0x000000000, 688(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3482:
	.size	_ZN6icu_6712PluralFormatC2ERKNS_6LocaleE11UPluralTypeRKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712PluralFormatC2ERKNS_6LocaleE11UPluralTypeRKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6712PluralFormatC1ERKNS_6LocaleE11UPluralTypeRKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6712PluralFormatC1ERKNS_6LocaleE11UPluralTypeRKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6712PluralFormatC2ERKNS_6LocaleE11UPluralTypeRKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormat11copyObjectsERKS0_
	.type	_ZN6icu_6712PluralFormat11copyObjectsERKS0_, @function
_ZN6icu_6712PluralFormat11copyObjectsERKS0_:
.LFB3487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	680(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	testq	%rdi, %rdi
	je	.L216
	movq	(%rdi), %rax
	call	*8(%rax)
.L216:
	movq	704(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L217
	movq	(%rdi), %rax
	call	*8(%rax)
.L217:
	movq	680(%r12), %rdi
	testq	%rdi, %rdi
	je	.L230
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rax, 680(%rbx)
.L219:
	movq	704(%r12), %rdi
	testq	%rdi, %rdi
	je	.L231
	call	_ZNK6icu_6711PluralRules5cloneEv@PLT
	movq	%rax, 704(%rbx)
.L215:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L232
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	leaq	-28(%rbp), %rsi
	leaq	328(%rbx), %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 704(%rbx)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L230:
	leaq	-28(%rbp), %rsi
	leaq	328(%rbx), %rdi
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 680(%rbx)
	jmp	.L219
.L232:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3487:
	.size	_ZN6icu_6712PluralFormat11copyObjectsERKS0_, .-_ZN6icu_6712PluralFormat11copyObjectsERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormatC2ERKS0_
	.type	_ZN6icu_6712PluralFormatC2ERKS0_, @function
_ZN6icu_6712PluralFormatC2ERKS0_:
.LFB3485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_676FormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6712PluralFormatE(%rip), %rax
	leaq	328(%r13), %rsi
	movq	%rax, (%r12)
	leaq	328(%r12), %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	552(%r13), %rsi
	leaq	552(%r12), %rdi
	call	_ZN6icu_6714MessagePatternC1ERKS0_@PLT
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movsd	688(%r13), %xmm0
	movq	$0, 680(%r12)
	movq	%rax, 696(%r12)
	movq	$0, 704(%r12)
	movsd	%xmm0, 688(%r12)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712PluralFormat11copyObjectsERKS0_
	.cfi_endproc
.LFE3485:
	.size	_ZN6icu_6712PluralFormatC2ERKS0_, .-_ZN6icu_6712PluralFormatC2ERKS0_
	.globl	_ZN6icu_6712PluralFormatC1ERKS0_
	.set	_ZN6icu_6712PluralFormatC1ERKS0_,_ZN6icu_6712PluralFormatC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712PluralFormat5cloneEv
	.type	_ZNK6icu_6712PluralFormat5cloneEv, @function
_ZNK6icu_6712PluralFormat5cloneEv:
.LFB3503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$712, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L235
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676FormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6712PluralFormatE(%rip), %rax
	leaq	328(%r13), %rsi
	movq	%rax, (%r12)
	leaq	328(%r12), %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	552(%r13), %rsi
	leaq	552(%r12), %rdi
	call	_ZN6icu_6714MessagePatternC1ERKS0_@PLT
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movsd	688(%r13), %xmm0
	movq	$0, 680(%r12)
	movq	%rax, 696(%r12)
	movq	$0, 704(%r12)
	movsd	%xmm0, 688(%r12)
	call	_ZN6icu_6712PluralFormat11copyObjectsERKS0_
.L235:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3503:
	.size	_ZNK6icu_6712PluralFormat5cloneEv, .-_ZNK6icu_6712PluralFormat5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormat4initEPKNS_11PluralRulesE11UPluralTypeR10UErrorCode
	.type	_ZN6icu_6712PluralFormat4initEPKNS_11PluralRulesE11UPluralTypeR10UErrorCode, @function
_ZN6icu_6712PluralFormat4initEPKNS_11PluralRulesE11UPluralTypeR10UErrorCode:
.LFB3492:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L247
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L250
	call	_ZNK6icu_6711PluralRules5cloneEv@PLT
	leaq	328(%rbx), %r13
	movq	%rax, 704(%rbx)
	testq	%rax, %rax
	je	.L251
.L244:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 680(%rbx)
.L241:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	328(%rbx), %r13
	movl	%edx, %esi
	movq	%rcx, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movq	%rax, 704(%rbx)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L251:
	movl	$7, (%r12)
	jmp	.L241
	.cfi_endproc
.LFE3492:
	.size	_ZN6icu_6712PluralFormat4initEPKNS_11PluralRulesE11UPluralTypeR10UErrorCode, .-_ZN6icu_6712PluralFormat4initEPKNS_11PluralRulesE11UPluralTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712PluralFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712PluralFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode:
.LFB3493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	552(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$8, %rsp
	call	_ZN6icu_6714MessagePattern16parsePluralStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L256
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6714MessagePattern15getPluralOffsetEi@PLT
	movsd	%xmm0, 688(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	$0x000000000, 688(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3493:
	.size	_ZN6icu_6712PluralFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712PluralFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormat9toPatternERNS_13UnicodeStringE
	.type	_ZN6icu_6712PluralFormat9toPatternERNS_13UnicodeStringE, @function
_ZN6icu_6712PluralFormat9toPatternERNS_13UnicodeStringE:
.LFB3500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movl	648(%rdi), %eax
	testl	%eax, %eax
	je	.L263
	movswl	576(%rdi), %ecx
	leaq	568(%rdi), %rsi
	testw	%cx, %cx
	js	.L260
	sarl	$5, %ecx
.L261:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	movl	580(%rdi), %ecx
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L263:
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3500:
	.size	_ZN6icu_6712PluralFormat9toPatternERNS_13UnicodeStringE, .-_ZN6icu_6712PluralFormat9toPatternERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormat9setLocaleERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6712PluralFormat9setLocaleERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6712PluralFormat9setLocaleERKNS_6LocaleER10UErrorCode:
.LFB3501:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L280
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	328(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	552(%rbx), %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	680(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L267
	movq	(%rdi), %rax
	call	*8(%rax)
.L267:
	movq	704(%rbx), %rdi
	movq	$0x000000000, 688(%rbx)
	movq	$0, 680(%rbx)
	testq	%rdi, %rdi
	je	.L268
	movq	(%rdi), %rax
	call	*8(%rax)
.L268:
	movq	$0, 704(%rbx)
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L264
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, 704(%rbx)
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 680(%rbx)
.L264:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3501:
	.size	_ZN6icu_6712PluralFormat9setLocaleERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6712PluralFormat9setLocaleERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormat15setNumberFormatEPKNS_12NumberFormatER10UErrorCode
	.type	_ZN6icu_6712PluralFormat15setNumberFormatEPKNS_12NumberFormatER10UErrorCode, @function
_ZN6icu_6712PluralFormat15setNumberFormatEPKNS_12NumberFormatER10UErrorCode:
.LFB3502:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L289
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	call	*32(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L283
	movq	680(%r12), %rdi
	testq	%rdi, %rdi
	je	.L284
	movq	(%rdi), %rax
	call	*8(%rax)
.L284:
	movq	%r13, 680(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$7, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3502:
	.size	_ZN6icu_6712PluralFormat15setNumberFormatEPKNS_12NumberFormatER10UErrorCode, .-_ZN6icu_6712PluralFormat15setNumberFormatEPKNS_12NumberFormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormataSERKS0_
	.type	_ZN6icu_6712PluralFormataSERKS0_, @function
_ZN6icu_6712PluralFormataSERKS0_:
.LFB3504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	cmpq	%rsi, %rdi
	je	.L293
	movq	%rsi, %r13
	leaq	328(%rdi), %rdi
	leaq	328(%rsi), %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	552(%r13), %rsi
	leaq	552(%r12), %rdi
	call	_ZN6icu_6714MessagePatternaSERKS0_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movsd	688(%r13), %xmm0
	movsd	%xmm0, 688(%r12)
	call	_ZN6icu_6712PluralFormat11copyObjectsERKS0_
.L293:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3504:
	.size	_ZN6icu_6712PluralFormataSERKS0_, .-_ZN6icu_6712PluralFormataSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormat14findSubMessageERKNS_14MessagePatternEiRKNS0_14PluralSelectorEPvdR10UErrorCode
	.type	_ZN6icu_6712PluralFormat14findSubMessageERKNS_14MessagePatternEiRKNS0_14PluralSelectorEPvdR10UErrorCode, @function
_ZN6icu_6712PluralFormat14findSubMessageERKNS_14MessagePatternEiRKNS0_14PluralSelectorEPvdR10UErrorCode:
.LFB3508:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L296
	jmp	_ZN6icu_6712PluralFormat14findSubMessageERKNS_14MessagePatternEiRKNS0_14PluralSelectorEPvdR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L296:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3508:
	.size	_ZN6icu_6712PluralFormat14findSubMessageERKNS_14MessagePatternEiRKNS0_14PluralSelectorEPvdR10UErrorCode, .-_ZN6icu_6712PluralFormat14findSubMessageERKNS_14MessagePatternEiRKNS0_14PluralSelectorEPvdR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712PluralFormat9parseTypeERKNS_13UnicodeStringEPKNS_6NFRuleERNS_11FormattableERNS_13FieldPositionE
	.type	_ZNK6icu_6712PluralFormat9parseTypeERKNS_13UnicodeStringEPKNS_6NFRuleERNS_11FormattableERNS_13FieldPositionE, @function
_ZNK6icu_6712PluralFormat9parseTypeERKNS_13UnicodeStringEPKNS_6NFRuleERNS_11FormattableERNS_13FieldPositionE:
.LFB3509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -352(%rbp)
	movl	648(%rdi), %r13d
	movq	%rdx, -360(%rbp)
	movq	%rcx, -384(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r13d, %r13d
	je	.L334
	movq	%rdi, %r12
	movl	12(%r8), %edi
	movl	$0, %eax
	movl	$2, %r9d
	movw	%r9w, -248(%rbp)
	testl	%edi, %edi
	cmovns	12(%r8), %eax
	movl	$2, %r8d
	movw	%r8w, -312(%rbp)
	movl	%eax, -344(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	%rax, -256(%rbp)
	leaq	568(%r12), %rax
	movq	%rax, -392(%rbp)
	testl	%r13d, %r13d
	jle	.L335
	leaq	-256(%rbp), %rax
	movq	%r14, -408(%rbp)
	xorl	%edx, %edx
	movq	%r12, %r14
	movq	%rax, -368(%rbp)
	leaq	-320(%rbp), %rax
	leaq	-192(%rbp), %r15
	movl	$-1, -340(%rbp)
	movq	%rax, -376(%rbp)
	.p2align 4,,10
	.p2align 3
.L301:
	movq	640(%r14), %rcx
	movslq	%edx, %rax
	leal	1(%rdx), %ebx
	salq	$4, %rax
	cmpl	$11, (%rcx,%rax)
	jne	.L303
	leaq	16(%rcx,%rax), %r12
	leal	2(%rdx), %ebx
	movl	(%r12), %esi
	testl	%esi, %esi
	jne	.L303
	leaq	32(%rcx,%rax), %r11
	leal	3(%rdx), %ebx
	cmpl	$1, (%r11)
	je	.L336
.L303:
	movl	%ebx, %edx
.L316:
	cmpl	%ebx, %r13d
	jg	.L301
	movl	-340(%rbp), %eax
	movq	-408(%rbp), %r14
	cmpl	$-1, %eax
	je	.L300
	movl	%eax, 12(%r14)
	movswl	-248(%rbp), %eax
	testw	%ax, %ax
	js	.L317
	sarl	$5, %eax
.L318:
	addl	-340(%rbp), %eax
	movq	-376(%rbp), %rbx
	movl	%eax, 16(%r14)
	movq	-384(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6711Formattable9setStringERKNS_13UnicodeStringE@PLT
	movq	-368(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L297:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L337
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	movl	4(%r11), %ecx
	movzwl	8(%r12), %edx
	movq	%r15, %rdi
	movq	%r11, -400(%rbp)
	addl	4(%r12), %edx
	movq	-392(%rbp), %rsi
	subl	%edx, %ecx
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	-360(%rbp), %rdi
	movq	-400(%rbp), %r11
	testq	%rdi, %rdi
	je	.L304
	movl	-344(%rbp), %ecx
	movq	-352(%rbp), %rsi
	leaq	-324(%rbp), %r8
	movq	%r15, %rdx
	movl	$-1, -324(%rbp)
	call	_ZNK6icu_676NFRule15findTextLenientERKNS_13UnicodeStringES3_iPi@PLT
	movq	-400(%rbp), %r11
	movl	%eax, %r8d
.L305:
	movl	-340(%rbp), %esi
	movl	$0, %eax
	testl	%esi, %esi
	cmovns	%esi, %eax
	cmpl	%eax, %r8d
	jl	.L310
	movswl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L312
	movswl	-248(%rbp), %edx
	sarl	$5, %eax
	testw	%dx, %dx
	js	.L314
.L338:
	sarl	$5, %edx
.L315:
	cmpl	%eax, %edx
	jge	.L310
	movq	-368(%rbp), %rdi
	movq	%r15, %rsi
	movl	%r8d, -340(%rbp)
	movq	%r11, -400(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-400(%rbp), %r11
	movzwl	8(%r12), %edx
	movq	-392(%rbp), %rsi
	addl	4(%r12), %edx
	leaq	-128(%rbp), %r12
	movl	4(%r11), %ecx
	movq	%r12, %rdi
	subl	%edx, %ecx
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	-376(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L310:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	%ebx, %edx
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L335:
	leaq	-256(%rbp), %rax
	movq	%rax, -368(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, -376(%rbp)
.L300:
	movq	$-1, 12(%r14)
	movq	-368(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-376(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L334:
	movq	$-1, 12(%r8)
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L304:
	movq	-352(%rbp), %rax
	movswl	8(%rax), %r9d
	testw	%r9w, %r9w
	js	.L306
	movl	-344(%rbp), %eax
	sarl	$5, %r9d
	cmpl	%r9d, %eax
	movl	%eax, %r8d
	cmovg	%r9d, %r8d
.L307:
	movzwl	-184(%rbp), %eax
	movl	%eax, %edx
	andl	$1, %edx
	testw	%ax, %ax
	js	.L308
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testb	%dl, %dl
	jne	.L310
.L309:
	movq	%r11, -400(%rbp)
	testl	%ecx, %ecx
	je	.L310
	testb	$2, %al
	movq	-352(%rbp), %rdi
	leaq	-182(%rbp), %rsi
	cmove	-168(%rbp), %rsi
	subl	%r8d, %r9d
	xorl	%edx, %edx
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movq	-400(%rbp), %r11
	movl	%eax, %r8d
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L312:
	movswl	-248(%rbp), %edx
	movl	-180(%rbp), %eax
	testw	%dx, %dx
	jns	.L338
.L314:
	movl	-244(%rbp), %edx
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L317:
	movl	-244(%rbp), %eax
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L308:
	movl	-180(%rbp), %ecx
	testb	%dl, %dl
	jne	.L310
	testl	%ecx, %ecx
	js	.L310
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L306:
	movl	12(%rax), %r9d
	movl	-344(%rbp), %eax
	cmpl	%r9d, %eax
	movl	%eax, %r8d
	cmovg	%r9d, %r8d
	jmp	.L307
.L337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3509:
	.size	_ZNK6icu_6712PluralFormat9parseTypeERKNS_13UnicodeStringEPKNS_6NFRuleERNS_11FormattableERNS_13FieldPositionE, .-_ZNK6icu_6712PluralFormat9parseTypeERKNS_13UnicodeStringEPKNS_6NFRuleERNS_11FormattableERNS_13FieldPositionE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormat14PluralSelectorD2Ev
	.type	_ZN6icu_6712PluralFormat14PluralSelectorD2Ev, @function
_ZN6icu_6712PluralFormat14PluralSelectorD2Ev:
.LFB3511:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3511:
	.size	_ZN6icu_6712PluralFormat14PluralSelectorD2Ev, .-_ZN6icu_6712PluralFormat14PluralSelectorD2Ev
	.globl	_ZN6icu_6712PluralFormat14PluralSelectorD1Ev
	.set	_ZN6icu_6712PluralFormat14PluralSelectorD1Ev,_ZN6icu_6712PluralFormat14PluralSelectorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormat14PluralSelectorD0Ev
	.type	_ZN6icu_6712PluralFormat14PluralSelectorD0Ev, @function
_ZN6icu_6712PluralFormat14PluralSelectorD0Ev:
.LFB3513:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3513:
	.size	_ZN6icu_6712PluralFormat14PluralSelectorD0Ev, .-_ZN6icu_6712PluralFormat14PluralSelectorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormat21PluralSelectorAdapter5resetEv
	.type	_ZN6icu_6712PluralFormat21PluralSelectorAdapter5resetEv, @function
_ZN6icu_6712PluralFormat21PluralSelectorAdapter5resetEv:
.LFB3519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L342
	movq	(%rdi), %rax
	call	*8(%rax)
.L342:
	movq	$0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3519:
	.size	_ZN6icu_6712PluralFormat21PluralSelectorAdapter5resetEv, .-_ZN6icu_6712PluralFormat21PluralSelectorAdapter5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormatD2Ev
	.type	_ZN6icu_6712PluralFormatD2Ev, @function
_ZN6icu_6712PluralFormatD2Ev:
.LFB3489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712PluralFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	680(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L348
	movq	(%rdi), %rax
	call	*8(%rax)
.L348:
	movq	704(%r12), %rdi
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	%rax, 696(%r12)
	testq	%rdi, %rdi
	je	.L349
	movq	(%rdi), %rax
	call	*8(%rax)
.L349:
	leaq	552(%r12), %rdi
	call	_ZN6icu_6714MessagePatternD1Ev@PLT
	leaq	328(%r12), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676FormatD2Ev@PLT
	.cfi_endproc
.LFE3489:
	.size	_ZN6icu_6712PluralFormatD2Ev, .-_ZN6icu_6712PluralFormatD2Ev
	.globl	_ZN6icu_6712PluralFormatD1Ev
	.set	_ZN6icu_6712PluralFormatD1Ev,_ZN6icu_6712PluralFormatD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PluralFormatD0Ev
	.type	_ZN6icu_6712PluralFormatD0Ev, @function
_ZN6icu_6712PluralFormatD0Ev:
.LFB3491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712PluralFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	680(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L358
	movq	(%rdi), %rax
	call	*8(%rax)
.L358:
	movq	704(%r12), %rdi
	leaq	16+_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE(%rip), %rax
	movq	%rax, 696(%r12)
	testq	%rdi, %rdi
	je	.L359
	movq	(%rdi), %rax
	call	*8(%rax)
.L359:
	leaq	552(%r12), %rdi
	call	_ZN6icu_6714MessagePatternD1Ev@PLT
	leaq	328(%r12), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676FormatD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3491:
	.size	_ZN6icu_6712PluralFormatD0Ev, .-_ZN6icu_6712PluralFormatD0Ev
	.align 2
	.p2align 4
	.type	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0, @function
_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0:
.LFB4795:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-288(%rbp), %r15
	leaq	-136(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$456, %rsp
	movq	%rsi, -480(%rbp)
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	movsd	%xmm0, -496(%rbp)
	subsd	688(%rdi), %xmm0
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movsd	%xmm0, -472(%rbp)
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	pxor	%xmm1, %xmm1
	ucomisd	688(%rbx), %xmm1
	jp	.L368
	jne	.L368
	movq	-480(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZNK6icu_6711Formattable23populateDecimalQuantityERNS_6number4impl15DecimalQuantityER10UErrorCode@PLT
.L370:
	movl	$2, %r8d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r8w, -456(%rbp)
	movq	680(%rbx), %r8
	movq	%rax, -464(%rbp)
	testq	%r8, %r8
	je	.L371
	movq	%r8, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	movq	%r8, -488(%rbp)
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	-488(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L371
	movq	%r13, %rsi
	call	_ZNK6icu_6713DecimalFormat17toNumberFormatterER10UErrorCode@PLT
	movl	0(%r13), %edi
	leaq	-464(%rbp), %rsi
	movq	%rsi, -472(%rbp)
	testl	%edi, %edi
	jle	.L407
.L378:
	movq	-472(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L408
	addq	$456, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	pxor	%xmm2, %xmm2
	ucomisd	688(%rbx), %xmm2
	jp	.L374
	jne	.L374
	leaq	-464(%rbp), %rax
	movq	-480(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r8, %rdi
	movq	%rax, %rdx
	movq	%rax, -472(%rbp)
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
.L373:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L378
	movsd	-496(%rbp), %xmm0
	movq	%r13, %r8
	movq	%r14, %rcx
	xorl	%esi, %esi
	leaq	696(%rbx), %rdx
	leaq	552(%rbx), %rdi
	call	_ZN6icu_6712PluralFormat14findSubMessageERKNS_14MessagePatternEiRKNS0_14PluralSelectorEPvdR10UErrorCode.part.0
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L378
	movq	640(%rbx), %rcx
	movslq	%eax, %r13
	leaq	568(%rbx), %r10
	leal	1(%rax), %r14d
	salq	$4, %r13
	movzwl	8(%rcx,%r13), %edx
	addl	4(%rcx,%r13), %edx
	addq	$16, %r13
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L380:
	cmpl	$5, %eax
	je	.L409
.L382:
	addl	$1, %r14d
	movslq	%r14d, %r13
	salq	$4, %r13
.L406:
	leaq	(%rcx,%r13), %r8
	movl	(%r8), %eax
	movl	4(%r8), %r9d
	cmpl	$1, %eax
	je	.L410
	cmpl	$4, %eax
	je	.L379
	cmpl	$2, %eax
	jne	.L380
	cmpl	$1, 560(%rbx)
	jne	.L382
	movl	%r9d, %ecx
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	%r8, -488(%rbp)
	subl	%edx, %ecx
	movq	%r10, -480(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-480(%rbp), %r10
	movq	-488(%rbp), %r8
.L385:
	movzwl	8(%r8), %edx
	movq	640(%rbx), %rcx
	addl	4(%r8), %edx
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L368:
	movsd	-472(%rbp), %xmm0
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	-400(%rbp), %r9
	movsd	-472(%rbp), %xmm0
	movq	%r8, -488(%rbp)
	movq	%r9, %rdi
	movq	%r9, -480(%rbp)
	call	_ZN6icu_6711FormattableC1Ed@PLT
	movq	-480(%rbp), %r9
	movq	-488(%rbp), %r8
	movq	%r13, %rcx
	leaq	-464(%rbp), %rax
	movq	%r9, %rsi
	movq	%r8, %rdi
	movq	%rax, %rdx
	movq	%rax, -472(%rbp)
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
	movq	-480(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L409:
	movl	%r9d, %ecx
	movq	%r10, %rsi
	movq	%r12, %rdi
	movl	%r9d, -496(%rbp)
	subl	%edx, %ecx
	movq	%r10, -480(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	640(%rbx), %rcx
	movq	-480(%rbp), %r10
	movl	-496(%rbp), %r9d
	movl	12(%rcx,%r13), %eax
	movq	%r10, %rdi
	movl	%r9d, %esi
	cmpl	%eax, %r14d
	cmovl	%eax, %r14d
	movslq	%r14d, %rax
	salq	$4, %rax
	movzwl	8(%rcx,%rax), %edx
	addl	4(%rcx,%rax), %edx
	movq	%r12, %rcx
	movl	%edx, -488(%rbp)
	call	_ZN6icu_6711MessageImpl24appendReducedApostrophesERKNS_13UnicodeStringEiiRS1_@PLT
	movq	640(%rbx), %rcx
	movq	-480(%rbp), %r10
	movl	-488(%rbp), %edx
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L410:
	movl	%r9d, %ecx
	movq	%r12, %rdi
	movq	%r10, %rsi
	subl	%edx, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%rax, %r12
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L379:
	movl	%r9d, %ecx
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	%r8, -488(%rbp)
	subl	%edx, %ecx
	movq	%r10, -480(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	-456(%rbp), %ecx
	movq	-472(%rbp), %rsi
	movq	%r12, %rdi
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	cmovs	-452(%rbp), %ecx
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-480(%rbp), %r10
	movq	-488(%rbp), %r8
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L407:
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L378
	leaq	-400(%rbp), %r8
	leaq	-280(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -480(%rbp)
	call	_ZNK6icu_6722FormattedStringBuilder15toUnicodeStringEv@PLT
	movq	-480(%rbp), %r8
	movq	-472(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-480(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L373
.L408:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4795:
	.size	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0, .-_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB3499:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L412
	movl	648(%rdi), %eax
	testl	%eax, %eax
	jne	.L413
	movq	680(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L412:
	movq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	movq	%r8, %rcx
	jmp	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	.cfi_endproc
.LFE3499:
	.size	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB3494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jle	.L421
.L414:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movq	%rdi, %r15
	movq	%rsi, %rdi
	movq	%rsi, %rbx
	movq	%rcx, %r13
	movq	%r8, %r12
	call	_ZNK6icu_6711Formattable9isNumericEv@PLT
	testb	%al, %al
	jne	.L422
	movl	$1, (%r12)
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L422:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L414
	movl	648(%r15), %eax
	testl	%eax, %eax
	jne	.L419
	movq	680(%r15), %rdi
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	movsd	8(%rbx), %xmm0
	addq	$8, %rsp
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	.cfi_endproc
.LFE3494:
	.size	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712PluralFormat6formatEiR10UErrorCode
	.type	_ZNK6icu_6712PluralFormat6formatEiR10UErrorCode, @function
_ZNK6icu_6712PluralFormat6formatEiR10UErrorCode:
.LFB3495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r15
	movq	%rsi, %r14
	movl	%edx, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$248, %rsp
	movq	%rcx, -280(%rbp)
	movl	$2, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movw	%cx, -232(%rbp)
	movq	%rax, -272(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -264(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$0, -256(%rbp)
	movq	%rax, -240(%rbp)
	call	_ZN6icu_6711FormattableC1Ei@PLT
	movq	-280(%rbp), %r8
	movl	(%r8), %esi
	testl	%esi, %esi
	jg	.L427
	movl	648(%r14), %eax
	testl	%eax, %eax
	jne	.L425
	movq	680(%r14), %rdi
	leaq	-240(%rbp), %r13
	leaq	-272(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	%rax, %rsi
.L424:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L429
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L427:
	.cfi_restore_state
	leaq	-240(%rbp), %r13
	leaq	-272(%rbp), %r14
	movq	%r13, %rsi
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L425:
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r8, %rcx
	cvtsi2sdl	%ebx, %xmm0
	leaq	-240(%rbp), %r13
	movq	%r13, %rdx
	leaq	-272(%rbp), %r14
	call	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	movq	%rax, %rsi
	jmp	.L424
.L429:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3495:
	.size	_ZNK6icu_6712PluralFormat6formatEiR10UErrorCode, .-_ZNK6icu_6712PluralFormat6formatEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712PluralFormat6formatEdR10UErrorCode
	.type	_ZNK6icu_6712PluralFormat6formatEdR10UErrorCode, @function
_ZNK6icu_6712PluralFormat6formatEdR10UErrorCode:
.LFB3496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$2, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movl	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -264(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -240(%rbp)
	movw	%dx, -232(%rbp)
	movsd	%xmm0, -280(%rbp)
	call	_ZN6icu_6711FormattableC1Ed@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L434
	movl	648(%r14), %eax
	movsd	-280(%rbp), %xmm0
	testl	%eax, %eax
	jne	.L432
	movq	680(%r14), %rdi
	leaq	-240(%rbp), %rbx
	movq	%r15, %rsi
	movq	%r13, %r8
	leaq	-272(%rbp), %r14
	movq	%rbx, %rdx
	movq	(%rdi), %rax
	movq	%r14, %rcx
	call	*40(%rax)
	movq	%rax, %rsi
.L431:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L436
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	leaq	-240(%rbp), %rbx
	leaq	-272(%rbp), %r14
	movq	%rbx, %rsi
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L432:
	leaq	-240(%rbp), %rbx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	leaq	-272(%rbp), %r14
	call	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	movq	%rax, %rsi
	jmp	.L431
.L436:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3496:
	.size	_ZNK6icu_6712PluralFormat6formatEdR10UErrorCode, .-_ZNK6icu_6712PluralFormat6formatEdR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712PluralFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6712PluralFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6712PluralFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB3497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	movq	%r15, %rdi
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$152, %rsp
	movq	%rcx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711FormattableC1Ei@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L438
	movl	648(%r14), %eax
	testl	%eax, %eax
	jne	.L439
	movq	680(%r14), %rdi
	movq	%r12, %rdx
	movq	%r13, %r8
	movq	%r15, %rsi
	movq	-184(%rbp), %rcx
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	%rax, %r12
.L438:
	movq	%r15, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L443
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movq	%r13, %rcx
	movq	%r15, %rsi
	cvtsi2sdl	%ebx, %xmm0
	movq	%r14, %rdi
	call	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	movq	%rax, %r12
	jmp	.L438
.L443:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3497:
	.size	_ZNK6icu_6712PluralFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6712PluralFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712PluralFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6712PluralFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6712PluralFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB3498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-176(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movsd	%xmm0, -184(%rbp)
	call	_ZN6icu_6711FormattableC1Ed@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L445
	movl	648(%r15), %eax
	movsd	-184(%rbp), %xmm0
	testl	%eax, %eax
	jne	.L446
	movq	680(%r15), %rdi
	movq	%r12, %rdx
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	%rax, %r12
.L445:
	movq	%rbx, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L450
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L446:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	movq	%rax, %r12
	jmp	.L445
.L450:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3498:
	.size	_ZNK6icu_6712PluralFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6712PluralFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712PluralFormateqERKNS_6FormatE
	.type	_ZNK6icu_6712PluralFormateqERKNS_6FormatE, @function
_ZNK6icu_6712PluralFormateqERKNS_6FormatE:
.LFB3505:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L469
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZNK6icu_676FormateqERKS0_@PLT
	testb	%al, %al
	jne	.L472
.L451:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	leaq	328(%r12), %rsi
	leaq	328(%rbx), %rdi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	jne	.L453
.L455:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	leaq	552(%r12), %rsi
	leaq	552(%rbx), %rdi
	call	_ZNK6icu_6714MessagePatterneqERKS0_@PLT
	testb	%al, %al
	je	.L455
	movq	680(%rbx), %rdi
	movq	680(%r12), %rsi
	testq	%rdi, %rdi
	sete	%dl
	testq	%rsi, %rsi
	sete	%al
	cmpb	%al, %dl
	jne	.L455
	testq	%rdi, %rdi
	je	.L458
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	je	.L455
.L458:
	movq	704(%rbx), %rdi
	movq	704(%r12), %rsi
	testq	%rdi, %rdi
	sete	%dl
	testq	%rsi, %rsi
	sete	%al
	cmpb	%al, %dl
	jne	.L455
	movl	$1, %eax
	testq	%rdi, %rdi
	je	.L451
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	setne	%al
	jmp	.L451
	.cfi_endproc
.LFE3505:
	.size	_ZNK6icu_6712PluralFormateqERKNS_6FormatE, .-_ZNK6icu_6712PluralFormateqERKNS_6FormatE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712PluralFormatneERKNS_6FormatE
	.type	_ZNK6icu_6712PluralFormatneERKNS_6FormatE, @function
_ZNK6icu_6712PluralFormatneERKNS_6FormatE:
.LFB3506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_6712PluralFormateqERKNS_6FormatE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L474
	xorl	%eax, %eax
	cmpq	%rdi, %rsi
	je	.L473
	call	_ZNK6icu_676FormateqERKS0_@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	jne	.L491
.L473:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L491:
	.cfi_restore_state
	leaq	328(%r13), %rsi
	leaq	328(%r12), %rdi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	jne	.L476
.L478:
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore_state
	call	*%rax
	popq	%r12
	popq	%r13
	testb	%al, %al
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	leaq	552(%r13), %rsi
	leaq	552(%r12), %rdi
	call	_ZNK6icu_6714MessagePatterneqERKS0_@PLT
	testb	%al, %al
	je	.L478
	movq	680(%r12), %rdi
	movq	680(%r13), %rsi
	testq	%rdi, %rdi
	sete	%dl
	testq	%rsi, %rsi
	sete	%al
	cmpb	%al, %dl
	jne	.L478
	testq	%rdi, %rdi
	je	.L481
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	je	.L478
.L481:
	movq	704(%r12), %rdi
	movq	704(%r13), %rsi
	testq	%rdi, %rdi
	sete	%dl
	testq	%rsi, %rsi
	sete	%al
	cmpb	%al, %dl
	jne	.L478
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L473
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	sete	%al
	jmp	.L473
	.cfi_endproc
.LFE3506:
	.size	_ZNK6icu_6712PluralFormatneERKNS_6FormatE, .-_ZNK6icu_6712PluralFormatneERKNS_6FormatE
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6712PluralFormat14PluralSelectorE
	.section	.rodata._ZTSN6icu_6712PluralFormat14PluralSelectorE,"aG",@progbits,_ZTSN6icu_6712PluralFormat14PluralSelectorE,comdat
	.align 32
	.type	_ZTSN6icu_6712PluralFormat14PluralSelectorE, @object
	.size	_ZTSN6icu_6712PluralFormat14PluralSelectorE, 40
_ZTSN6icu_6712PluralFormat14PluralSelectorE:
	.string	"N6icu_6712PluralFormat14PluralSelectorE"
	.weak	_ZTIN6icu_6712PluralFormat14PluralSelectorE
	.section	.data.rel.ro._ZTIN6icu_6712PluralFormat14PluralSelectorE,"awG",@progbits,_ZTIN6icu_6712PluralFormat14PluralSelectorE,comdat
	.align 8
	.type	_ZTIN6icu_6712PluralFormat14PluralSelectorE, @object
	.size	_ZTIN6icu_6712PluralFormat14PluralSelectorE, 24
_ZTIN6icu_6712PluralFormat14PluralSelectorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712PluralFormat14PluralSelectorE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6712PluralFormat21PluralSelectorAdapterE
	.section	.rodata._ZTSN6icu_6712PluralFormat21PluralSelectorAdapterE,"aG",@progbits,_ZTSN6icu_6712PluralFormat21PluralSelectorAdapterE,comdat
	.align 32
	.type	_ZTSN6icu_6712PluralFormat21PluralSelectorAdapterE, @object
	.size	_ZTSN6icu_6712PluralFormat21PluralSelectorAdapterE, 47
_ZTSN6icu_6712PluralFormat21PluralSelectorAdapterE:
	.string	"N6icu_6712PluralFormat21PluralSelectorAdapterE"
	.weak	_ZTIN6icu_6712PluralFormat21PluralSelectorAdapterE
	.section	.data.rel.ro._ZTIN6icu_6712PluralFormat21PluralSelectorAdapterE,"awG",@progbits,_ZTIN6icu_6712PluralFormat21PluralSelectorAdapterE,comdat
	.align 8
	.type	_ZTIN6icu_6712PluralFormat21PluralSelectorAdapterE, @object
	.size	_ZTIN6icu_6712PluralFormat21PluralSelectorAdapterE, 24
_ZTIN6icu_6712PluralFormat21PluralSelectorAdapterE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712PluralFormat21PluralSelectorAdapterE
	.quad	_ZTIN6icu_6712PluralFormat14PluralSelectorE
	.weak	_ZTSN6icu_6712PluralFormatE
	.section	.rodata._ZTSN6icu_6712PluralFormatE,"aG",@progbits,_ZTSN6icu_6712PluralFormatE,comdat
	.align 16
	.type	_ZTSN6icu_6712PluralFormatE, @object
	.size	_ZTSN6icu_6712PluralFormatE, 24
_ZTSN6icu_6712PluralFormatE:
	.string	"N6icu_6712PluralFormatE"
	.weak	_ZTIN6icu_6712PluralFormatE
	.section	.data.rel.ro._ZTIN6icu_6712PluralFormatE,"awG",@progbits,_ZTIN6icu_6712PluralFormatE,comdat
	.align 8
	.type	_ZTIN6icu_6712PluralFormatE, @object
	.size	_ZTIN6icu_6712PluralFormatE, 24
_ZTIN6icu_6712PluralFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712PluralFormatE
	.quad	_ZTIN6icu_676FormatE
	.weak	_ZTVN6icu_6712PluralFormatE
	.section	.data.rel.ro._ZTVN6icu_6712PluralFormatE,"awG",@progbits,_ZTVN6icu_6712PluralFormatE,comdat
	.align 8
	.type	_ZTVN6icu_6712PluralFormatE, @object
	.size	_ZTVN6icu_6712PluralFormatE, 88
_ZTVN6icu_6712PluralFormatE:
	.quad	0
	.quad	_ZTIN6icu_6712PluralFormatE
	.quad	_ZN6icu_6712PluralFormatD1Ev
	.quad	_ZN6icu_6712PluralFormatD0Ev
	.quad	_ZNK6icu_6712PluralFormat17getDynamicClassIDEv
	.quad	_ZNK6icu_6712PluralFormateqERKNS_6FormatE
	.quad	_ZNK6icu_6712PluralFormat5cloneEv
	.quad	_ZNK6icu_6712PluralFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712PluralFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.quad	_ZNK6icu_6712PluralFormatneERKNS_6FormatE
	.weak	_ZTVN6icu_6712PluralFormat14PluralSelectorE
	.section	.data.rel.ro._ZTVN6icu_6712PluralFormat14PluralSelectorE,"awG",@progbits,_ZTVN6icu_6712PluralFormat14PluralSelectorE,comdat
	.align 8
	.type	_ZTVN6icu_6712PluralFormat14PluralSelectorE, @object
	.size	_ZTVN6icu_6712PluralFormat14PluralSelectorE, 40
_ZTVN6icu_6712PluralFormat14PluralSelectorE:
	.quad	0
	.quad	_ZTIN6icu_6712PluralFormat14PluralSelectorE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE
	.section	.data.rel.ro.local._ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE,"awG",@progbits,_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE,comdat
	.align 8
	.type	_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE, @object
	.size	_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE, 40
_ZTVN6icu_6712PluralFormat21PluralSelectorAdapterE:
	.quad	0
	.quad	_ZTIN6icu_6712PluralFormat21PluralSelectorAdapterE
	.quad	_ZN6icu_6712PluralFormat21PluralSelectorAdapterD1Ev
	.quad	_ZN6icu_6712PluralFormat21PluralSelectorAdapterD0Ev
	.quad	_ZNK6icu_6712PluralFormat21PluralSelectorAdapter6selectEPvdR10UErrorCode
	.local	_ZZN6icu_6712PluralFormat16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6712PluralFormat16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.align 8
	.type	_ZN6icu_67L12OTHER_STRINGE, @object
	.size	_ZN6icu_67L12OTHER_STRINGE, 12
_ZN6icu_67L12OTHER_STRINGE:
	.value	111
	.value	116
	.value	104
	.value	101
	.value	114
	.value	0
	.type	_ZN6icu_67L15kUndefinedFieldE, @object
	.size	_ZN6icu_67L15kUndefinedFieldE, 1
_ZN6icu_67L15kUndefinedFieldE:
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
