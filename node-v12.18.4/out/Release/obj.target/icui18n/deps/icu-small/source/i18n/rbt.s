	.file	"rbt.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723RuleBasedTransliterator17getDynamicClassIDEv
	.type	_ZNK6icu_6723RuleBasedTransliterator17getDynamicClassIDEv, @function
_ZNK6icu_6723RuleBasedTransliterator17getDynamicClassIDEv:
.LFB3144:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6723RuleBasedTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3144:
	.size	_ZNK6icu_6723RuleBasedTransliterator17getDynamicClassIDEv, .-_ZNK6icu_6723RuleBasedTransliterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723RuleBasedTransliteratorD2Ev
	.type	_ZN6icu_6723RuleBasedTransliteratorD2Ev, @function
_ZN6icu_6723RuleBasedTransliteratorD2Ev:
.LFB3159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6723RuleBasedTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	cmpb	$0, 96(%rdi)
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	je	.L4
	movq	88(%rdi), %r13
	testq	%r13, %r13
	je	.L4
	movq	%r13, %rdi
	call	_ZN6icu_6723TransliterationRuleDataD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L4:
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714TransliteratorD2Ev@PLT
	.cfi_endproc
.LFE3159:
	.size	_ZN6icu_6723RuleBasedTransliteratorD2Ev, .-_ZN6icu_6723RuleBasedTransliteratorD2Ev
	.globl	_ZN6icu_6723RuleBasedTransliteratorD1Ev
	.set	_ZN6icu_6723RuleBasedTransliteratorD1Ev,_ZN6icu_6723RuleBasedTransliteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723RuleBasedTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.type	_ZNK6icu_6723RuleBasedTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, @function
_ZNK6icu_6723RuleBasedTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona:
.LFB3163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	12(%rdx), %eax
	subl	8(%rdx), %eax
	movq	%rdi, -56(%rbp)
	movl	%eax, %r14d
	sall	$4, %r14d
	cmpl	$268435456, %eax
	movl	$-1, %eax
	cmovnb	%eax, %r14d
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	_ZN6icu_67L11gLockedTextE(%rip), %r15
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	cmpq	%rbx, %r15
	je	.L32
	leaq	_ZZNK6icu_6723RuleBasedTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositionaE23transliteratorDataMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	xorl	%edi, %edi
	movq	%rbx, _ZN6icu_67L11gLockedTextE(%rip)
	call	umtx_unlock_67@PLT
	movq	-56(%rbp), %rax
	movq	88(%rax), %rdi
	testq	%rdi, %rdi
	je	.L16
	movb	$1, -57(%rbp)
	movl	8(%r13), %eax
	cmpl	%eax, 12(%r13)
	jle	.L16
.L15:
	movsbl	%r12b, %r12d
	xorl	%r15d, %r15d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L33:
	movl	12(%r13), %eax
	addl	$1, %r15d
	cmpl	%eax, 8(%r13)
	jge	.L18
	cmpl	%r15d, %r14d
	jb	.L18
	movq	-56(%rbp), %rax
	movq	88(%rax), %rdi
.L19:
	addq	$8, %rdi
	movl	%r12d, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN6icu_6722TransliterationRuleSet13transliterateERNS_11ReplaceableER14UTransPositiona@PLT
	testb	%al, %al
	jne	.L33
.L18:
	cmpb	$0, -57(%rbp)
	jne	.L16
.L9:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	xorl	%edi, %edi
	movq	$0, _ZN6icu_67L11gLockedTextE(%rip)
	call	umtx_unlock_67@PLT
	addq	$24, %rsp
	leaq	_ZZNK6icu_6723RuleBasedTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositionaE23transliteratorDataMutex(%rip), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	movq	88(%rax), %rdi
	testq	%rdi, %rdi
	je	.L9
	movl	12(%r13), %eax
	cmpl	%eax, 8(%r13)
	jge	.L9
	movb	$0, -57(%rbp)
	jmp	.L15
	.cfi_endproc
.LFE3163:
	.size	_ZNK6icu_6723RuleBasedTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, .-_ZNK6icu_6723RuleBasedTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723RuleBasedTransliterator18handleGetSourceSetERNS_10UnicodeSetE
	.type	_ZNK6icu_6723RuleBasedTransliterator18handleGetSourceSetERNS_10UnicodeSetE, @function
_ZNK6icu_6723RuleBasedTransliterator18handleGetSourceSetERNS_10UnicodeSetE:
.LFB3171:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rdi
	xorl	%edx, %edx
	addq	$8, %rdi
	jmp	_ZNK6icu_6722TransliterationRuleSet18getSourceTargetSetERNS_10UnicodeSetEa@PLT
	.cfi_endproc
.LFE3171:
	.size	_ZNK6icu_6723RuleBasedTransliterator18handleGetSourceSetERNS_10UnicodeSetE, .-_ZNK6icu_6723RuleBasedTransliterator18handleGetSourceSetERNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723RuleBasedTransliterator12getTargetSetERNS_10UnicodeSetE
	.type	_ZNK6icu_6723RuleBasedTransliterator12getTargetSetERNS_10UnicodeSetE, @function
_ZNK6icu_6723RuleBasedTransliterator12getTargetSetERNS_10UnicodeSetE:
.LFB3172:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rdi
	movl	$1, %edx
	addq	$8, %rdi
	jmp	_ZNK6icu_6722TransliterationRuleSet18getSourceTargetSetERNS_10UnicodeSetEa@PLT
	.cfi_endproc
.LFE3172:
	.size	_ZNK6icu_6723RuleBasedTransliterator12getTargetSetERNS_10UnicodeSetE, .-_ZNK6icu_6723RuleBasedTransliterator12getTargetSetERNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723RuleBasedTransliteratorD0Ev
	.type	_ZN6icu_6723RuleBasedTransliteratorD0Ev, @function
_ZN6icu_6723RuleBasedTransliteratorD0Ev:
.LFB3161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6723RuleBasedTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	cmpb	$0, 96(%rdi)
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	je	.L37
	movq	88(%rdi), %r13
	testq	%r13, %r13
	je	.L37
	movq	%r13, %rdi
	call	_ZN6icu_6723TransliterationRuleDataD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L37:
	movq	%r12, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3161:
	.size	_ZN6icu_6723RuleBasedTransliteratorD0Ev, .-_ZN6icu_6723RuleBasedTransliteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723RuleBasedTransliterator5cloneEv
	.type	_ZNK6icu_6723RuleBasedTransliterator5cloneEv, @function
_ZNK6icu_6723RuleBasedTransliterator5cloneEv:
.LFB3162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$104, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L42
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6723RuleBasedTransliteratorE(%rip), %rax
	movq	%rax, (%r12)
	movq	88(%rbx), %rax
	movq	%rax, 88(%r12)
	movzbl	96(%rbx), %eax
	movb	%al, 96(%r12)
	testb	%al, %al
	jne	.L54
.L42:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movl	$1168, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L46
	movq	88(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6723TransliterationRuleDataC1ERKS0_@PLT
.L46:
	movq	%r13, 88(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3162:
	.size	_ZNK6icu_6723RuleBasedTransliterator5cloneEv, .-_ZNK6icu_6723RuleBasedTransliterator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723RuleBasedTransliterator16getStaticClassIDEv
	.type	_ZN6icu_6723RuleBasedTransliterator16getStaticClassIDEv, @function
_ZN6icu_6723RuleBasedTransliterator16getStaticClassIDEv:
.LFB3143:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6723RuleBasedTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3143:
	.size	_ZN6icu_6723RuleBasedTransliterator16getStaticClassIDEv, .-_ZN6icu_6723RuleBasedTransliterator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723RuleBasedTransliteratorC2ERKS0_
	.type	_ZN6icu_6723RuleBasedTransliteratorC2ERKS0_, @function
_ZN6icu_6723RuleBasedTransliteratorC2ERKS0_:
.LFB3156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6723RuleBasedTransliteratorE(%rip), %rax
	movq	%rax, (%rbx)
	movq	88(%r12), %rax
	movq	%rax, 88(%rbx)
	movzbl	96(%r12), %eax
	movb	%al, 96(%rbx)
	testb	%al, %al
	jne	.L66
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	$1168, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L58
	movq	88(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6723TransliterationRuleDataC1ERKS0_@PLT
.L58:
	movq	%r13, 88(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3156:
	.size	_ZN6icu_6723RuleBasedTransliteratorC2ERKS0_, .-_ZN6icu_6723RuleBasedTransliteratorC2ERKS0_
	.globl	_ZN6icu_6723RuleBasedTransliteratorC1ERKS0_
	.set	_ZN6icu_6723RuleBasedTransliteratorC1ERKS0_,_ZN6icu_6723RuleBasedTransliteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723RuleBasedTransliterator10_constructERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode
	.type	_ZN6icu_6723RuleBasedTransliterator10_constructERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode, @function
_ZN6icu_6723RuleBasedTransliterator10_constructERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode:
.LFB3145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$1, 96(%rdi)
	movl	(%r8), %r9d
	movq	$0, 88(%rdi)
	testl	%r9d, %r9d
	jle	.L75
.L67:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	leaq	-560(%rbp), %r9
	movq	%rsi, %r13
	movq	%rdi, %r12
	movq	%r8, %rsi
	movq	%r9, %rdi
	movq	%r8, %rbx
	movq	%r9, -568(%rbp)
	movl	%edx, %r14d
	movq	%rcx, %r15
	call	_ZN6icu_6720TransliteratorParserC1ER10UErrorCode@PLT
	movq	%r15, %rcx
	movq	%rbx, %r8
	movl	%r14d, %edx
	movq	-568(%rbp), %r9
	movq	%r13, %rsi
	movq	%r9, %rdi
	call	_ZN6icu_6720TransliteratorParser5parseERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode@PLT
	movl	(%rbx), %ecx
	movq	-568(%rbp), %r9
	testl	%ecx, %ecx
	jg	.L69
	movl	-504(%rbp), %edx
	testl	%edx, %edx
	jne	.L70
	cmpq	$0, -472(%rbp)
	je	.L77
.L70:
	movl	$65560, (%rbx)
.L69:
	movq	%r9, %rdi
	call	_ZN6icu_6720TransliteratorParserD1Ev@PLT
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L77:
	movl	-544(%rbp), %eax
	testl	%eax, %eax
	je	.L70
	xorl	%esi, %esi
	leaq	-552(%rbp), %rdi
	movq	%r9, -568(%rbp)
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	movq	%rax, 88(%r12)
	leaq	8(%rax), %rdi
	call	_ZNK6icu_6722TransliterationRuleSet23getMaximumContextLengthEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6714Transliterator23setMaximumContextLengthEi@PLT
	movq	-568(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6720TransliteratorParserD1Ev@PLT
	jmp	.L67
.L76:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3145:
	.size	_ZN6icu_6723RuleBasedTransliterator10_constructERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode, .-_ZN6icu_6723RuleBasedTransliterator10_constructERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringES3_15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringES3_15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode, @function
_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringES3_15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode:
.LFB3147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movq	%r8, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movq	%r15, %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	leaq	16+_ZTVN6icu_6723RuleBasedTransliteratorE(%rip), %rax
	movq	%rbx, %r8
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6723RuleBasedTransliterator10_constructERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode
	.cfi_endproc
.LFE3147:
	.size	_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringES3_15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode, .-_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringES3_15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode
	.globl	_ZN6icu_6723RuleBasedTransliteratorC1ERKNS_13UnicodeStringES3_15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode
	.set	_ZN6icu_6723RuleBasedTransliteratorC1ERKNS_13UnicodeStringES3_15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode,_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringES3_15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataEPNS_13UnicodeFilterE
	.type	_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataEPNS_13UnicodeFilterE, @function
_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataEPNS_13UnicodeFilterE:
.LFB3150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	movq	%rcx, %rdx
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	leaq	16+_ZTVN6icu_6723RuleBasedTransliteratorE(%rip), %rax
	movq	%rbx, 88(%r12)
	leaq	8(%rbx), %rdi
	movq	%rax, (%r12)
	movb	$0, 96(%r12)
	call	_ZNK6icu_6722TransliterationRuleSet23getMaximumContextLengthEv@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movl	%eax, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714Transliterator23setMaximumContextLengthEi@PLT
	.cfi_endproc
.LFE3150:
	.size	_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataEPNS_13UnicodeFilterE, .-_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataEPNS_13UnicodeFilterE
	.globl	_ZN6icu_6723RuleBasedTransliteratorC1ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataEPNS_13UnicodeFilterE
	.set	_ZN6icu_6723RuleBasedTransliteratorC1ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataEPNS_13UnicodeFilterE,_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataEPNS_13UnicodeFilterE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringEPNS_23TransliterationRuleDataEa
	.type	_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringEPNS_23TransliterationRuleDataEa, @function
_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringEPNS_23TransliterationRuleDataEa:
.LFB3153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$8, %rsp
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	leaq	16+_ZTVN6icu_6723RuleBasedTransliteratorE(%rip), %rax
	movq	%rbx, 88(%r12)
	leaq	8(%rbx), %rdi
	movb	%r13b, 96(%r12)
	movq	%rax, (%r12)
	call	_ZNK6icu_6722TransliterationRuleSet23getMaximumContextLengthEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movl	%eax, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714Transliterator23setMaximumContextLengthEi@PLT
	.cfi_endproc
.LFE3153:
	.size	_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringEPNS_23TransliterationRuleDataEa, .-_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringEPNS_23TransliterationRuleDataEa
	.globl	_ZN6icu_6723RuleBasedTransliteratorC1ERKNS_13UnicodeStringEPNS_23TransliterationRuleDataEa
	.set	_ZN6icu_6723RuleBasedTransliteratorC1ERKNS_13UnicodeStringEPNS_23TransliterationRuleDataEa,_ZN6icu_6723RuleBasedTransliteratorC2ERKNS_13UnicodeStringEPNS_23TransliterationRuleDataEa
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723RuleBasedTransliterator7toRulesERNS_13UnicodeStringEa
	.type	_ZNK6icu_6723RuleBasedTransliterator7toRulesERNS_13UnicodeStringEa, @function
_ZNK6icu_6723RuleBasedTransliterator7toRulesERNS_13UnicodeStringEa:
.LFB3170:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rdi
	movsbl	%dl, %edx
	addq	$8, %rdi
	jmp	_ZNK6icu_6722TransliterationRuleSet7toRulesERNS_13UnicodeStringEa@PLT
	.cfi_endproc
.LFE3170:
	.size	_ZNK6icu_6723RuleBasedTransliterator7toRulesERNS_13UnicodeStringEa, .-_ZNK6icu_6723RuleBasedTransliterator7toRulesERNS_13UnicodeStringEa
	.weak	_ZTSN6icu_6723RuleBasedTransliteratorE
	.section	.rodata._ZTSN6icu_6723RuleBasedTransliteratorE,"aG",@progbits,_ZTSN6icu_6723RuleBasedTransliteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6723RuleBasedTransliteratorE, @object
	.size	_ZTSN6icu_6723RuleBasedTransliteratorE, 35
_ZTSN6icu_6723RuleBasedTransliteratorE:
	.string	"N6icu_6723RuleBasedTransliteratorE"
	.weak	_ZTIN6icu_6723RuleBasedTransliteratorE
	.section	.data.rel.ro._ZTIN6icu_6723RuleBasedTransliteratorE,"awG",@progbits,_ZTIN6icu_6723RuleBasedTransliteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6723RuleBasedTransliteratorE, @object
	.size	_ZTIN6icu_6723RuleBasedTransliteratorE, 24
_ZTIN6icu_6723RuleBasedTransliteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6723RuleBasedTransliteratorE
	.quad	_ZTIN6icu_6714TransliteratorE
	.weak	_ZTVN6icu_6723RuleBasedTransliteratorE
	.section	.data.rel.ro._ZTVN6icu_6723RuleBasedTransliteratorE,"awG",@progbits,_ZTVN6icu_6723RuleBasedTransliteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6723RuleBasedTransliteratorE, @object
	.size	_ZTVN6icu_6723RuleBasedTransliteratorE, 152
_ZTVN6icu_6723RuleBasedTransliteratorE:
	.quad	0
	.quad	_ZTIN6icu_6723RuleBasedTransliteratorE
	.quad	_ZN6icu_6723RuleBasedTransliteratorD1Ev
	.quad	_ZN6icu_6723RuleBasedTransliteratorD0Ev
	.quad	_ZNK6icu_6723RuleBasedTransliterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6723RuleBasedTransliterator5cloneEv
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.quad	_ZNK6icu_6723RuleBasedTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.quad	_ZNK6icu_6714Transliterator5getIDEv
	.quad	_ZNK6icu_6723RuleBasedTransliterator7toRulesERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6723RuleBasedTransliterator18handleGetSourceSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6723RuleBasedTransliterator12getTargetSetERNS_10UnicodeSetE
	.local	_ZZNK6icu_6723RuleBasedTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositionaE23transliteratorDataMutex
	.comm	_ZZNK6icu_6723RuleBasedTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositionaE23transliteratorDataMutex,56,32
	.local	_ZN6icu_67L11gLockedTextE
	.comm	_ZN6icu_67L11gLockedTextE,8,8
	.local	_ZZN6icu_6723RuleBasedTransliterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6723RuleBasedTransliterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
