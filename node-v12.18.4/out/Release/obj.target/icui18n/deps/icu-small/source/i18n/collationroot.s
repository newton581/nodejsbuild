	.file	"collationroot.cpp"
	.text
	.p2align 4
	.type	uprv_collation_root_cleanup, @function
uprv_collation_root_cleanup:
.LFB3301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZN6icu_6712_GLOBAL__N_1L13rootSingletonE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L2
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	$0, _ZN6icu_6712_GLOBAL__N_1L13rootSingletonE(%rip)
.L2:
	movl	$1, %eax
	movl	$0, _ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip)
	mfence
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3301:
	.size	uprv_collation_root_cleanup, .-uprv_collation_root_cleanup
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ucadata"
.LC1:
	.string	"icu"
.LC2:
	.string	"icudt67l-coll"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CollationRoot4loadER10UErrorCode
	.type	_ZN6icu_6713CollationRoot4loadER10UErrorCode, @function
_ZN6icu_6713CollationRoot4loadER10UErrorCode:
.LFB3302:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L19
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$400, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L10
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6718CollationTailoringC1EPKNS_17CollationSettingsE@PLT
	cmpq	$0, 32(%r12)
	je	.L11
	movq	_ZN6icu_6719CollationDataReader12isAcceptableEPvPKcS3_PK9UDataInfo@GOTPCREL(%rip), %rcx
	leaq	.LC0(%rip), %rdx
	movq	%rbx, %r9
	leaq	.LC2(%rip), %rdi
	leaq	328(%r12), %r8
	leaq	.LC1(%rip), %rsi
	call	udata_openChoice_67@PLT
	movl	(%rbx), %edx
	movq	%rax, 352(%r12)
	movq	%rax, %rdi
	testl	%edx, %edx
	jle	.L20
.L13:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	call	udata_getMemory_67@PLT
	movq	352(%r12), %rdi
	movq	%rax, %r13
	call	udata_getLength_67@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rsi
	movl	%eax, %edx
	xorl	%edi, %edi
	call	_ZN6icu_6719CollationDataReader4readEPKNS_18CollationTailoringEPKhiRS1_R10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L13
	leaq	uprv_collation_root_cleanup(%rip), %rsi
	movl	$30, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	call	_ZN6icu_676Locale7getRootEv@PLT
	movl	$256, %edi
	movq	%rax, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L13
	movq	$0, 8(%rax)
	movq	%r13, %rsi
	leaq	24(%rbx), %rdi
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6719CollationCacheEntryE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r12, 248(%rbx)
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	%rbx, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	%rbx, _ZN6icu_6712_GLOBAL__N_1L13rootSingletonE(%rip)
.L8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L13
.L10:
	movl	$7, (%rbx)
	jmp	.L8
	.cfi_endproc
.LFE3302:
	.size	_ZN6icu_6713CollationRoot4loadER10UErrorCode, .-_ZN6icu_6713CollationRoot4loadER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CollationRoot17getRootCacheEntryER10UErrorCode
	.type	_ZN6icu_6713CollationRoot17getRootCacheEntryER10UErrorCode, @function
_ZN6icu_6713CollationRoot17getRootCacheEntryER10UErrorCode:
.LFB3305:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	testl	%edx, %edx
	jle	.L37
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L38
.L23:
	movl	4+_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L24
	movl	%eax, (%rbx)
.L22:
	xorl	%eax, %eax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L23
	movq	%rbx, %rdi
	call	_ZN6icu_6713CollationRoot4loadER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L24:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L22
	movq	_ZN6icu_6712_GLOBAL__N_1L13rootSingletonE(%rip), %rax
.L21:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3305:
	.size	_ZN6icu_6713CollationRoot17getRootCacheEntryER10UErrorCode, .-_ZN6icu_6713CollationRoot17getRootCacheEntryER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CollationRoot7getRootER10UErrorCode
	.type	_ZN6icu_6713CollationRoot7getRootER10UErrorCode, @function
_ZN6icu_6713CollationRoot7getRootER10UErrorCode:
.LFB3306:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	testl	%edx, %edx
	jle	.L55
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L56
.L41:
	movl	4+_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L42
	movl	%eax, (%rbx)
.L40:
	xorl	%eax, %eax
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L41
	movq	%rbx, %rdi
	call	_ZN6icu_6713CollationRoot4loadER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L42:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L40
	movq	_ZN6icu_6712_GLOBAL__N_1L13rootSingletonE(%rip), %rax
	movq	248(%rax), %rax
.L39:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3306:
	.size	_ZN6icu_6713CollationRoot7getRootER10UErrorCode, .-_ZN6icu_6713CollationRoot7getRootER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CollationRoot7getDataER10UErrorCode
	.type	_ZN6icu_6713CollationRoot7getDataER10UErrorCode, @function
_ZN6icu_6713CollationRoot7getDataER10UErrorCode:
.LFB3307:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	testl	%edx, %edx
	jle	.L73
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L74
.L59:
	movl	4+_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L60
	movl	%eax, (%rbx)
.L58:
	xorl	%eax, %eax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L74:
	leaq	_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L59
	movq	%rbx, %rdi
	call	_ZN6icu_6713CollationRoot4loadER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L60:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L58
	movq	_ZN6icu_6712_GLOBAL__N_1L13rootSingletonE(%rip), %rax
	movq	248(%rax), %rax
	movq	24(%rax), %rax
.L57:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3307:
	.size	_ZN6icu_6713CollationRoot7getDataER10UErrorCode, .-_ZN6icu_6713CollationRoot7getDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CollationRoot11getSettingsER10UErrorCode
	.type	_ZN6icu_6713CollationRoot11getSettingsER10UErrorCode, @function
_ZN6icu_6713CollationRoot11getSettingsER10UErrorCode:
.LFB3308:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	testl	%edx, %edx
	jle	.L91
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L92
.L77:
	movl	4+_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L78
	movl	%eax, (%rbx)
.L76:
	xorl	%eax, %eax
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L92:
	leaq	_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L77
	movq	%rbx, %rdi
	call	_ZN6icu_6713CollationRoot4loadER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_6712_GLOBAL__N_1L8initOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L78:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L76
	movq	_ZN6icu_6712_GLOBAL__N_1L13rootSingletonE(%rip), %rax
	movq	248(%rax), %rax
	movq	32(%rax), %rax
.L75:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3308:
	.size	_ZN6icu_6713CollationRoot11getSettingsER10UErrorCode, .-_ZN6icu_6713CollationRoot11getSettingsER10UErrorCode
	.local	_ZN6icu_6712_GLOBAL__N_1L8initOnceE
	.comm	_ZN6icu_6712_GLOBAL__N_1L8initOnceE,8,8
	.local	_ZN6icu_6712_GLOBAL__N_1L13rootSingletonE
	.comm	_ZN6icu_6712_GLOBAL__N_1L13rootSingletonE,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
