	.file	"currpinf.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718CurrencyPluralInfo17getDynamicClassIDEv
	.type	_ZNK6icu_6718CurrencyPluralInfo17getDynamicClassIDEv, @function
_ZNK6icu_6718CurrencyPluralInfo17getDynamicClassIDEv:
.LFB2621:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6718CurrencyPluralInfo16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2621:
	.size	_ZNK6icu_6718CurrencyPluralInfo17getDynamicClassIDEv, .-_ZNK6icu_6718CurrencyPluralInfo17getDynamicClassIDEv
	.p2align 4
	.type	ValueComparator, @function
ValueComparator:
.LFB2619:
	.cfi_startproc
	endbr64
	movswl	8(%rdi), %edx
	movswl	8(%rsi), %eax
	movl	%edx, %ecx
	movl	%eax, %r8d
	andl	$1, %r8d
	andl	$1, %ecx
	jne	.L17
	testw	%dx, %dx
	js	.L5
	sarl	$5, %edx
	testw	%ax, %ax
	js	.L7
.L22:
	sarl	$5, %eax
.L8:
	andl	$1, %r8d
	jne	.L18
	cmpl	%edx, %eax
	je	.L21
.L18:
	movl	%ecx, %r8d
.L17:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	12(%rdi), %edx
	testw	%ax, %ax
	jns	.L22
.L7:
	movl	12(%rsi), %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L21:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	setne	%cl
	movl	%ecx, %eax
	ret
	.cfi_endproc
.LFE2619:
	.size	ValueComparator, .-ValueComparator
	.align 2
	.p2align 4
	.type	_ZN6icu_6718CurrencyPluralInfo8initHashER10UErrorCode.part.0, @function
_ZN6icu_6718CurrencyPluralInfo8initHashER10UErrorCode.part.0:
.LFB3618:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$88, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L24
	movl	(%rbx), %esi
	movq	$0, (%rax)
	testl	%esi, %esi
	jle	.L34
.L25:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L23:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	leaq	8(%rax), %r13
	movq	uhash_compareCaselessUnicodeString_67@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movq	uhash_hashCaselessUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	uhash_init_67@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L26
	movq	(%r12), %rdi
.L27:
	testq	%rdi, %rdi
	je	.L25
	call	uhash_close_67@PLT
	jmp	.L25
.L24:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L23
	movl	$7, (%rbx)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	%r13, (%r12)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	uhash_setKeyDeleter_67@PLT
	movl	(%rbx), %edx
	movq	(%r12), %rdi
	testl	%edx, %edx
	jg	.L27
	leaq	ValueComparator(%rip), %rsi
	call	uhash_setValueComparator_67@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3618:
	.size	_ZN6icu_6718CurrencyPluralInfo8initHashER10UErrorCode.part.0, .-_ZN6icu_6718CurrencyPluralInfo8initHashER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfoD0Ev
	.type	_ZN6icu_6718CurrencyPluralInfoD0Ev, @function
_ZN6icu_6718CurrencyPluralInfoD0Ev:
.LFB2635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6718CurrencyPluralInfoE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L37
	movl	$-1, -44(%rbp)
	leaq	-44(%rbp), %rbx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L58:
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L57
.L39:
	movq	0(%r13), %rdi
	movq	%rbx, %rsi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	jne	.L58
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	uhash_close_67@PLT
.L41:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L37:
	movq	$0, 8(%r12)
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L42
	movq	(%rdi), %rax
	call	*8(%rax)
.L42:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L43
	movq	(%rdi), %rax
	call	*8(%rax)
.L43:
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, 16(%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L39
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2635:
	.size	_ZN6icu_6718CurrencyPluralInfoD0Ev, .-_ZN6icu_6718CurrencyPluralInfoD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfoD2Ev
	.type	_ZN6icu_6718CurrencyPluralInfoD2Ev, @function
_ZN6icu_6718CurrencyPluralInfoD2Ev:
.LFB2633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6718CurrencyPluralInfoE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L62
	movl	$-1, -44(%rbp)
	leaq	-44(%rbp), %rbx
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L83:
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L82
.L64:
	movq	0(%r13), %rdi
	movq	%rbx, %rsi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	jne	.L83
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L66
	call	uhash_close_67@PLT
.L66:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L62:
	movq	$0, 8(%r12)
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L67
	movq	(%rdi), %rax
	call	*8(%rax)
.L67:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L68
	movq	(%rdi), %rax
	call	*8(%rax)
.L68:
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, 16(%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L64
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2633:
	.size	_ZN6icu_6718CurrencyPluralInfoD2Ev, .-_ZN6icu_6718CurrencyPluralInfoD2Ev
	.globl	_ZN6icu_6718CurrencyPluralInfoD1Ev
	.set	_ZN6icu_6718CurrencyPluralInfoD1Ev,_ZN6icu_6718CurrencyPluralInfoD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfo16getStaticClassIDEv
	.type	_ZN6icu_6718CurrencyPluralInfo16getStaticClassIDEv, @function
_ZN6icu_6718CurrencyPluralInfo16getStaticClassIDEv:
.LFB2620:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6718CurrencyPluralInfo16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2620:
	.size	_ZN6icu_6718CurrencyPluralInfo16getStaticClassIDEv, .-_ZN6icu_6718CurrencyPluralInfo16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718CurrencyPluralInfoeqERKS0_
	.type	_ZNK6icu_6718CurrencyPluralInfoeqERKS0_, @function
_ZNK6icu_6718CurrencyPluralInfoeqERKS0_:
.LFB2636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	movq	16(%rsi), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	jne	.L87
.L89:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movq	24(%r12), %rsi
	movq	24(%rbx), %rdi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	je	.L89
	movq	8(%r12), %rdx
	movq	8(%rbx), %rax
	movq	(%rdx), %rsi
	movq	(%rax), %rdi
	call	uhash_equals_67@PLT
	popq	%rbx
	popq	%r12
	testb	%al, %al
	popq	%rbp
	.cfi_def_cfa 7, 8
	setne	%al
	ret
	.cfi_endproc
.LFE2636:
	.size	_ZNK6icu_6718CurrencyPluralInfoeqERKS0_, .-_ZNK6icu_6718CurrencyPluralInfoeqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718CurrencyPluralInfo14getPluralRulesEv
	.type	_ZNK6icu_6718CurrencyPluralInfo14getPluralRulesEv, @function
_ZNK6icu_6718CurrencyPluralInfo14getPluralRulesEv:
.LFB2638:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE2638:
	.size	_ZNK6icu_6718CurrencyPluralInfo14getPluralRulesEv, .-_ZNK6icu_6718CurrencyPluralInfo14getPluralRulesEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718CurrencyPluralInfo9getLocaleEv
	.type	_ZNK6icu_6718CurrencyPluralInfo9getLocaleEv, @function
_ZNK6icu_6718CurrencyPluralInfo9getLocaleEv:
.LFB2640:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE2640:
	.size	_ZNK6icu_6718CurrencyPluralInfo9getLocaleEv, .-_ZNK6icu_6718CurrencyPluralInfo9getLocaleEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfo14setPluralRulesERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6718CurrencyPluralInfo14setPluralRulesERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6718CurrencyPluralInfo14setPluralRulesERKNS_13UnicodeStringER10UErrorCode:
.LFB2641:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L106
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L98
	movq	(%rdi), %rax
	call	*8(%rax)
.L98:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711PluralRules11createRulesERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rax, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2641:
	.size	_ZN6icu_6718CurrencyPluralInfo14setPluralRulesERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6718CurrencyPluralInfo14setPluralRulesERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfo24setCurrencyPluralPatternERKNS_13UnicodeStringES3_R10UErrorCode
	.type	_ZN6icu_6718CurrencyPluralInfo24setCurrencyPluralPatternERKNS_13UnicodeStringES3_R10UErrorCode, @function
_ZN6icu_6718CurrencyPluralInfo24setCurrencyPluralPatternERKNS_13UnicodeStringES3_R10UErrorCode:
.LFB2642:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jle	.L128
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	(%rax), %rdi
	call	uhash_get_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L110
	movq	(%rax), %rax
	call	*8(%rax)
.L110:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L111
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L129
	addq	$8, %rsp
	movq	%r15, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD0Ev@PLT
.L111:
	.cfi_restore_state
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L116
.L107:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L129:
	.cfi_restore_state
	movl	$64, %edi
	movq	8(%rbx), %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L115
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L115:
	movq	(%rbx), %rdi
	addq	$8, %rsp
	movq	%r12, %rcx
	movq	%r15, %rdx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r14, %rsi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uhash_put_67@PLT
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L107
	.cfi_endproc
.LFE2642:
	.size	_ZN6icu_6718CurrencyPluralInfo24setCurrencyPluralPatternERKNS_13UnicodeStringES3_R10UErrorCode, .-_ZN6icu_6718CurrencyPluralInfo24setCurrencyPluralPatternERKNS_13UnicodeStringES3_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfo10deleteHashEPNS_9HashtableE
	.type	_ZN6icu_6718CurrencyPluralInfo10deleteHashEPNS_9HashtableE, @function
_ZN6icu_6718CurrencyPluralInfo10deleteHashEPNS_9HashtableE:
.LFB2646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L130
	movl	$-1, -28(%rbp)
	movq	%rsi, %r12
	leaq	-28(%rbp), %rbx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L145:
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L144
.L134:
	movq	(%r12), %rdi
	movq	%rbx, %rsi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	jne	.L145
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L136
	call	uhash_close_67@PLT
.L136:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L130:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L146
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L134
.L146:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2646:
	.size	_ZN6icu_6718CurrencyPluralInfo10deleteHashEPNS_9HashtableE, .-_ZN6icu_6718CurrencyPluralInfo10deleteHashEPNS_9HashtableE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfo8initHashER10UErrorCode
	.type	_ZN6icu_6718CurrencyPluralInfo8initHashER10UErrorCode, @function
_ZN6icu_6718CurrencyPluralInfo8initHashER10UErrorCode:
.LFB2647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rsi), %edi
	testl	%edi, %edi
	jg	.L147
	movl	$88, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L149
	movl	(%rbx), %esi
	movq	$0, (%rax)
	testl	%esi, %esi
	jle	.L160
.L150:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L147:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	leaq	8(%rax), %r13
	movq	uhash_compareCaselessUnicodeString_67@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movq	uhash_hashCaselessUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	uhash_init_67@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L151
	movq	(%r12), %rdi
.L152:
	testq	%rdi, %rdi
	je	.L150
	call	uhash_close_67@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%r13, (%r12)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	uhash_setKeyDeleter_67@PLT
	movl	(%rbx), %edx
	movq	(%r12), %rdi
	testl	%edx, %edx
	jg	.L152
	leaq	ValueComparator(%rip), %rsi
	call	uhash_setValueComparator_67@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L149:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L147
	movl	$7, (%rbx)
	jmp	.L147
	.cfi_endproc
.LFE2647:
	.size	_ZN6icu_6718CurrencyPluralInfo8initHashER10UErrorCode, .-_ZN6icu_6718CurrencyPluralInfo8initHashER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718CurrencyPluralInfo24getCurrencyPluralPatternERKNS_13UnicodeStringERS1_
	.type	_ZNK6icu_6718CurrencyPluralInfo24getCurrencyPluralPatternERKNS_13UnicodeStringERS1_, @function
_ZNK6icu_6718CurrencyPluralInfo24getCurrencyPluralPatternERKNS_13UnicodeStringERS1_:
.LFB2639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	(%rax), %rdi
	call	uhash_get_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L174
.L162:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L166:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L175
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	movswl	8(%r13), %edx
	testw	%dx, %dx
	js	.L163
	sarl	$5, %edx
.L164:
	movl	$5, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	_ZN6icu_67L17gPluralCountOtherE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L17gPluralCountOtherE(%rip), %rdx
	testb	%al, %al
	jne	.L165
	leaq	-112(%rbp), %r13
.L167:
	movq	%r13, %rdi
	leaq	_ZN6icu_67L29gDefaultCurrencyPluralPatternE(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L163:
	movl	12(%r13), %edx
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L165:
	leaq	-112(%rbp), %r13
	movq	8(%rbx), %rbx
	movl	$5, %ecx
	movq	%rdx, -120(%rbp)
	movq	%r13, %rdi
	leaq	-120(%rbp), %rdx
	movl	$1, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	uhash_get_67@PLT
	movq	%r13, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-136(%rbp), %rsi
	testq	%rsi, %rsi
	jne	.L162
	jmp	.L167
.L175:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2639:
	.size	_ZNK6icu_6718CurrencyPluralInfo24getCurrencyPluralPatternERKNS_13UnicodeStringERS1_, .-_ZNK6icu_6718CurrencyPluralInfo24getCurrencyPluralPatternERKNS_13UnicodeStringERS1_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"icudt67l-curr"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfo26setupCurrencyPluralPatternERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6718CurrencyPluralInfo26setupCurrencyPluralPatternERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6718CurrencyPluralInfo26setupCurrencyPluralPatternERKNS_6LocaleER10UErrorCode:
.LFB2645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L306
.L176:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L307
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movq	8(%rdi), %r12
	movq	%rdi, %r15
	movq	%rsi, %rbx
	movq	%rdx, %r14
	testq	%r12, %r12
	je	.L180
	movl	$-1, -264(%rbp)
	leaq	-264(%rbp), %r13
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L309:
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L308
.L182:
	movq	(%r12), %rdi
	movq	%r13, %rsi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	jne	.L309
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L184
	call	uhash_close_67@PLT
.L184:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	(%r14), %edi
	testl	%edi, %edi
	jg	.L185
.L180:
	movq	%r14, %rdi
	call	_ZN6icu_6718CurrencyPluralInfo8initHashER10UErrorCode.part.0
	movq	%rax, %r8
	movl	(%r14), %eax
	movq	%r8, 8(%r15)
	testl	%eax, %eax
	jg	.L176
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, -296(%rbp)
	testq	%rax, %rax
	je	.L310
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L311
.L237:
	movq	-296(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L308:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L185:
	movq	$0, 8(%r15)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L311:
	leaq	-288(%rbp), %r12
	movq	40(%rbx), %rsi
	xorl	%edi, %edi
	movl	$0, -288(%rbp)
	movq	%r12, %rdx
	call	ures_open_67@PLT
	movq	%r12, %rcx
	xorl	%edx, %edx
	leaq	_ZN6icu_67L18gNumberElementsTagE(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ures_getByKeyWithFallback_67@PLT
	movq	-296(%rbp), %rdi
	movq	%rax, -304(%rbp)
	call	_ZNK6icu_6715NumberingSystem7getNameEv@PLT
	movq	-304(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	ures_getByKeyWithFallback_67@PLT
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r13, %rdi
	leaq	_ZN6icu_67L12gPatternsTagE(%rip), %rsi
	call	ures_getByKeyWithFallback_67@PLT
	leaq	-284(%rbp), %r8
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r8, %rdx
	leaq	_ZN6icu_67L17gDecimalFormatTagE(%rip), %rsi
	movq	%r8, -312(%rbp)
	call	ures_getStringByKeyWithFallback_67@PLT
	movq	%rax, -344(%rbp)
	movl	-288(%rbp), %eax
	cmpl	$2, %eax
	je	.L312
.L188:
	testl	%eax, %eax
	jle	.L313
	cmpl	$7, %eax
	jne	.L235
	movl	$7, (%r14)
.L235:
	movq	-304(%rbp), %rax
	testq	%rax, %rax
	je	.L236
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L236:
	testq	%r13, %r13
	je	.L237
	movq	%r13, %rdi
	call	ures_close_67@PLT
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L313:
	movl	-284(%rbp), %r10d
	movl	%r10d, -352(%rbp)
	testl	%r10d, %r10d
	jle	.L247
	movq	-344(%rbp), %r8
	leal	-1(%r10), %ecx
	xorl	%eax, %eax
	xorl	%r9d, %r9d
	movl	$0, -348(%rbp)
	movq	%rcx, %rsi
	xorl	%r11d, %r11d
	leaq	2(%r8), %rdi
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%rdx, %rax
.L193:
	cmpw	$59, (%r8,%rax,2)
	jne	.L192
	movl	%esi, %edx
	leaq	(%rdi,%rax,2), %r11
	movl	%eax, %r10d
	movl	$1, %r9d
	subl	%eax, %edx
	movl	%edx, -348(%rbp)
.L192:
	leaq	1(%rax), %rdx
	cmpq	%rax, %rcx
	jne	.L314
	movb	%r9b, -361(%rbp)
	movl	%r10d, -352(%rbp)
	movq	%r11, -392(%rbp)
.L191:
	movq	40(%rbx), %rsi
	movq	%r12, %rdx
	leaq	.LC0(%rip), %rdi
	call	ures_open_67@PLT
	movq	%r12, %rcx
	xorl	%edx, %edx
	leaq	_ZN6icu_67L15gCurrUnitPtnTagE(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -360(%rbp)
	call	ures_getByKeyWithFallback_67@PLT
	movq	16(%r15), %rdi
	movq	%r12, %rsi
	movq	%rax, -312(%rbp)
	call	_ZNK6icu_6711PluralRules11getKeywordsER10UErrorCode@PLT
	movq	%rax, %rbx
	movl	-288(%rbp), %eax
	testq	%rbx, %rbx
	je	.L238
	leaq	-276(%rbp), %rcx
	movq	%rcx, -328(%rbp)
	testl	%eax, %eax
	jg	.L293
	movq	%r13, -416(%rbp)
	movq	%r15, -400(%rbp)
	movq	%r14, -376(%rbp)
	.p2align 4,,10
	.p2align 3
.L232:
	movq	(%rbx), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*40(%rax)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L198
	movl	-288(%rbp), %eax
	testl	%eax, %eax
	jg	.L296
	movq	-328(%rbp), %rcx
	movq	-312(%rbp), %rdi
	leaq	-280(%rbp), %rdx
	movq	%r14, %rsi
	movl	$0, -276(%rbp)
	call	ures_getStringByKeyWithFallback_67@PLT
	movq	%rax, %r11
	movl	-276(%rbp), %eax
	cmpl	$7, %eax
	je	.L200
	testq	%r11, %r11
	je	.L200
	movq	%r11, -336(%rbp)
	testl	%eax, %eax
	jg	.L232
	movl	-280(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L232
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -320(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L203
	movq	-336(%rbp), %r11
	movl	-280(%rbp), %edx
	leaq	-128(%rbp), %r13
	leaq	-192(%rbp), %r15
	movq	%r11, %rsi
	movq	%r11, -384(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movl	-352(%rbp), %edx
	movq	-344(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	leaq	_ZN6icu_67L6gPart0E(%rip), %rax
	movl	$3, %ecx
	movq	%r15, %rdi
	movq	%rax, -264(%rbp)
	leaq	-264(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdx
	movq	%rax, -336(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	movq	-384(%rbp), %r11
	testw	%ax, %ax
	js	.L204
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L205:
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L206
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L207:
	movq	-320(%rbp), %rax
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	js	.L208
	movswl	%ax, %edx
	sarl	$5, %edx
.L209:
	subq	$8, %rsp
	movq	-320(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	pushq	%rcx
	movq	%r15, %rcx
	pushq	$0
	pushq	%r13
	movq	%r11, -384(%rbp)
	call	_ZN6icu_6713UnicodeString14findAndReplaceEiiRKS0_iiS2_ii@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-336(%rbp), %rdx
	movl	$3, %ecx
	movq	%r13, %rdi
	leaq	_ZN6icu_67L19gTripleCurrencySignE(%rip), %rax
	movl	$1, %esi
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	_ZN6icu_67L6gPart1E(%rip), %rax
	movl	$3, %ecx
	movq	%r15, %rdi
	movq	%rax, -272(%rbp)
	leaq	-272(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdx
	movq	%rax, -408(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	movq	-384(%rbp), %r11
	testw	%ax, %ax
	js	.L210
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L211:
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L212
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L213:
	movq	-320(%rbp), %rax
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	js	.L214
	movswl	%ax, %edx
	sarl	$5, %edx
.L215:
	subq	$8, %rsp
	movq	-320(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	pushq	%rcx
	movq	%r15, %rcx
	pushq	$0
	pushq	%r13
	movq	%r11, -384(%rbp)
	call	_ZN6icu_6713UnicodeString14findAndReplaceEiiRKS0_iiS2_ii@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -361(%rbp)
	movq	-384(%rbp), %r11
	jne	.L315
.L216:
	movq	-400(%rbp), %rax
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	$-1, %edx
	movq	8(%rax), %r15
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L231
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-336(%rbp), %r8
.L231:
	movq	(%r15), %rdi
	movq	-376(%rbp), %rcx
	movq	%r8, %rsi
	movq	-320(%rbp), %rdx
	call	uhash_put_67@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L310:
	movl	(%r14), %esi
	testl	%esi, %esi
	jg	.L176
	movl	$7, (%r14)
	jmp	.L176
.L296:
	movq	-416(%rbp), %r13
	movq	-376(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L293:
	cmpl	$7, %eax
	je	.L197
.L244:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*8(%rax)
.L233:
	movq	-312(%rbp), %rax
	testq	%rax, %rax
	je	.L234
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L234:
	movq	-360(%rbp), %rax
	testq	%rax, %rax
	je	.L235
	movq	%rax, %rdi
	call	ures_close_67@PLT
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L312:
	movq	-296(%rbp), %rdi
	call	_ZNK6icu_6715NumberingSystem7getNameEv@PLT
	leaq	_ZN6icu_67L8gLatnTagE(%rip), %rsi
	movq	%rax, %rdi
	call	strcmp@PLT
	movq	-312(%rbp), %r8
	testl	%eax, %eax
	jne	.L189
.L299:
	movl	-288(%rbp), %eax
	jmp	.L188
.L203:
	movq	-416(%rbp), %r13
	movq	-376(%rbp), %r14
.L301:
	movl	$7, -288(%rbp)
.L197:
	movl	$7, (%r14)
	testq	%rbx, %rbx
	jne	.L244
	jmp	.L233
.L214:
	movq	-320(%rbp), %rax
	movl	12(%rax), %edx
	jmp	.L215
.L212:
	movl	-180(%rbp), %r9d
	jmp	.L213
.L210:
	movl	-116(%rbp), %ecx
	jmp	.L211
.L208:
	movq	-320(%rbp), %rax
	movl	12(%rax), %edx
	jmp	.L209
.L206:
	movl	-180(%rbp), %r9d
	jmp	.L207
.L204:
	movl	-116(%rbp), %ecx
	jmp	.L205
.L238:
	testl	%eax, %eax
	jle	.L301
	cmpl	$7, %eax
	jne	.L233
	jmp	.L197
.L315:
	leaq	-256(%rbp), %r10
	movl	-280(%rbp), %edx
	movq	%r11, %rsi
	movq	%r10, %rdi
	movq	%r10, -384(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movl	-348(%rbp), %edx
	movq	-392(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movq	-336(%rbp), %rdx
	movl	$3, %ecx
	movq	%r15, %rdi
	leaq	_ZN6icu_67L6gPart0E(%rip), %rax
	movl	$1, %esi
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	movq	-384(%rbp), %r10
	testw	%ax, %ax
	js	.L217
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L218:
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L219
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L220:
	movzwl	-248(%rbp), %eax
	testw	%ax, %ax
	js	.L221
	movswl	%ax, %edx
	sarl	$5, %edx
.L222:
	subq	$8, %rsp
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	pushq	%rcx
	movq	%r15, %rcx
	pushq	$0
	pushq	%r13
	movq	%r10, -384(%rbp)
	call	_ZN6icu_6713UnicodeString14findAndReplaceEiiRKS0_iiS2_ii@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-336(%rbp), %rdx
	movl	$3, %ecx
	movq	%r13, %rdi
	leaq	_ZN6icu_67L19gTripleCurrencySignE(%rip), %rax
	movl	$1, %esi
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-408(%rbp), %rdx
	movl	$3, %ecx
	movq	%r15, %rdi
	leaq	_ZN6icu_67L6gPart1E(%rip), %rax
	movl	$1, %esi
	movq	%rax, -272(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	movq	-384(%rbp), %r10
	testw	%ax, %ax
	js	.L223
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L224:
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L225
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L226:
	movzwl	-248(%rbp), %eax
	testw	%ax, %ax
	js	.L227
	movswl	%ax, %edx
	sarl	$5, %edx
.L228:
	subq	$8, %rsp
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	pushq	%rcx
	movq	%r15, %rcx
	pushq	$0
	pushq	%r13
	movq	%r10, -384(%rbp)
	call	_ZN6icu_6713UnicodeString14findAndReplaceEiiRKS0_iiS2_ii@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$59, %edx
	movq	-336(%rbp), %rsi
	movl	$1, %ecx
	movq	-320(%rbp), %rdi
	movw	%dx, -264(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	-248(%rbp), %eax
	movq	-384(%rbp), %r10
	testw	%ax, %ax
	js	.L229
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L230:
	movq	-320(%rbp), %rdi
	movq	%r10, %rsi
	xorl	%edx, %edx
	movq	%r10, -336(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-336(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L216
.L200:
	movl	%eax, -288(%rbp)
	movq	-416(%rbp), %r13
	movq	-376(%rbp), %r14
	jmp	.L293
.L189:
	movq	-304(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r13, %rdx
	leaq	_ZN6icu_67L8gLatnTagE(%rip), %rsi
	movq	%r8, -312(%rbp)
	movl	$0, -288(%rbp)
	call	ures_getByKeyWithFallback_67@PLT
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r13, %rdi
	leaq	_ZN6icu_67L12gPatternsTagE(%rip), %rsi
	call	ures_getByKeyWithFallback_67@PLT
	movq	-312(%rbp), %r8
	movq	%r12, %rcx
	movq	%r13, %rdi
	leaq	_ZN6icu_67L17gDecimalFormatTagE(%rip), %rsi
	movq	%r8, %rdx
	call	ures_getStringByKeyWithFallback_67@PLT
	movq	%rax, -344(%rbp)
	jmp	.L299
.L247:
	movb	$0, -361(%rbp)
	movl	$0, -348(%rbp)
	movq	$0, -392(%rbp)
	jmp	.L191
.L198:
	movq	-416(%rbp), %r13
	movq	-376(%rbp), %r14
	movl	-288(%rbp), %eax
	jmp	.L293
.L229:
	movl	-244(%rbp), %ecx
	jmp	.L230
.L227:
	movl	-244(%rbp), %edx
	jmp	.L228
.L225:
	movl	-180(%rbp), %r9d
	jmp	.L226
.L223:
	movl	-116(%rbp), %ecx
	jmp	.L224
.L221:
	movl	-244(%rbp), %edx
	jmp	.L222
.L219:
	movl	-180(%rbp), %r9d
	jmp	.L220
.L217:
	movl	-116(%rbp), %ecx
	jmp	.L218
.L307:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2645:
	.size	_ZN6icu_6718CurrencyPluralInfo26setupCurrencyPluralPatternERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6718CurrencyPluralInfo26setupCurrencyPluralPatternERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfoC2ER10UErrorCode
	.type	_ZN6icu_6718CurrencyPluralInfoC2ER10UErrorCode, @function
_ZN6icu_6718CurrencyPluralInfoC2ER10UErrorCode:
.LFB2623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718CurrencyPluralInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movl	$0, 32(%rdi)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L316
	movq	24(%r12), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L318
	movq	(%rdi), %rax
	call	*8(%rax)
.L318:
	movq	$0, 24(%r12)
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L319
	movq	(%rdi), %rax
	call	*8(%rax)
.L319:
	movq	$0, 16(%r12)
	movq	%r14, %rdi
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L322
	cmpb	$0, 216(%r14)
	jne	.L321
	cmpb	$0, 216(%rax)
	jne	.L322
.L321:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6718CurrencyPluralInfo26setupCurrencyPluralPatternERKNS_6LocaleER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	movl	$7, 0(%r13)
.L316:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2623:
	.size	_ZN6icu_6718CurrencyPluralInfoC2ER10UErrorCode, .-_ZN6icu_6718CurrencyPluralInfoC2ER10UErrorCode
	.globl	_ZN6icu_6718CurrencyPluralInfoC1ER10UErrorCode
	.set	_ZN6icu_6718CurrencyPluralInfoC1ER10UErrorCode,_ZN6icu_6718CurrencyPluralInfoC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfo9setLocaleERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6718CurrencyPluralInfo9setLocaleERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6718CurrencyPluralInfo9setLocaleERKNS_6LocaleER10UErrorCode:
.LFB2643:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L344
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L332
	movq	(%rdi), %rax
	call	*8(%rax)
.L332:
	movq	$0, 24(%r12)
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L333
	movq	(%rdi), %rax
	call	*8(%rax)
.L333:
	movq	$0, 16(%r12)
	movq	%r13, %rdi
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L336
	cmpb	$0, 216(%r13)
	jne	.L335
	cmpb	$0, 216(%rax)
	jne	.L336
.L335:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6718CurrencyPluralInfo26setupCurrencyPluralPatternERKNS_6LocaleER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	movl	$7, (%r14)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE2643:
	.size	_ZN6icu_6718CurrencyPluralInfo9setLocaleERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6718CurrencyPluralInfo9setLocaleERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfo10initializeERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6718CurrencyPluralInfo10initializeERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6718CurrencyPluralInfo10initializeERKNS_6LocaleER10UErrorCode:
.LFB2644:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L361
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L349
	movq	(%rdi), %rax
	call	*8(%rax)
.L349:
	movq	$0, 24(%r12)
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L350
	movq	(%rdi), %rax
	call	*8(%rax)
.L350:
	movq	$0, 16(%r12)
	movq	%r13, %rdi
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L353
	cmpb	$0, 216(%r13)
	jne	.L352
	cmpb	$0, 216(%rax)
	jne	.L353
.L352:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6718CurrencyPluralInfo26setupCurrencyPluralPatternERKNS_6LocaleER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	movl	$7, (%r14)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE2644:
	.size	_ZN6icu_6718CurrencyPluralInfo10initializeERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6718CurrencyPluralInfo10initializeERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfoC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6718CurrencyPluralInfoC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6718CurrencyPluralInfoC2ERKNS_6LocaleER10UErrorCode:
.LFB2626:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6718CurrencyPluralInfoE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	(%rdx), %eax
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movl	$0, 32(%rdi)
	testl	%eax, %eax
	jg	.L370
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L368
	cmpb	$0, 216(%r14)
	jne	.L367
	cmpb	$0, 216(%rax)
	jne	.L368
.L367:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6718CurrencyPluralInfo26setupCurrencyPluralPatternERKNS_6LocaleER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movl	$7, 0(%r13)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE2626:
	.size	_ZN6icu_6718CurrencyPluralInfoC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6718CurrencyPluralInfoC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6718CurrencyPluralInfoC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6718CurrencyPluralInfoC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6718CurrencyPluralInfoC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfo8copyHashEPKNS_9HashtableEPS1_R10UErrorCode
	.type	_ZN6icu_6718CurrencyPluralInfo8copyHashEPKNS_9HashtableEPS1_R10UErrorCode, @function
_ZN6icu_6718CurrencyPluralInfo8copyHashEPKNS_9HashtableEPS1_R10UErrorCode:
.LFB2648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -160(%rbp)
	movl	(%rcx), %edi
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L373
	movl	$-1, -132(%rbp)
	testq	%rsi, %rsi
	je	.L373
	movq	%rcx, %rbx
	leaq	-132(%rbp), %r15
	leaq	-128(%rbp), %r12
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L395:
	movq	8(%rax), %rsi
	movl	$64, %edi
	movq	16(%rax), %r14
	movq	%rsi, -152(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L375
	movq	-152(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L394
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L378
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
.L393:
	movq	-168(%rbp), %rax
	movq	(%rax), %rdi
	call	uhash_put_67@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L373
.L380:
	movq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	(%rax), %rdi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	jne	.L395
	.p2align 4,,10
	.p2align 3
.L373:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L396
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L375:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L373
	movl	$7, (%rbx)
	jmp	.L373
.L394:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	jmp	.L373
.L378:
	movq	%rbx, %rcx
	movq	%r13, %rdx
	xorl	%esi, %esi
	jmp	.L393
.L396:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2648:
	.size	_ZN6icu_6718CurrencyPluralInfo8copyHashEPKNS_9HashtableEPS1_R10UErrorCode, .-_ZN6icu_6718CurrencyPluralInfo8copyHashEPKNS_9HashtableEPS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfoaSERKS0_
	.type	_ZN6icu_6718CurrencyPluralInfoaSERKS0_, @function
_ZN6icu_6718CurrencyPluralInfoaSERKS0_:
.LFB2631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L399
	movl	32(%rsi), %eax
	movq	%rsi, %rbx
	movl	%eax, 32(%rdi)
	testl	%eax, %eax
	jle	.L434
.L399:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L435
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	movq	8(%rdi), %r14
	testq	%r14, %r14
	je	.L436
	movl	$-1, -60(%rbp)
	leaq	-60(%rbp), %r15
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L438:
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L437
.L404:
	movq	(%r14), %rdi
	movq	%r15, %rsi
	call	uhash_nextElement_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L438
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L406
	call	uhash_close_67@PLT
.L406:
	movq	%r14, %rdi
	leaq	32(%r12), %r14
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	32(%r12), %edx
	testl	%edx, %edx
	jg	.L407
.L402:
	movq	%r14, %rdi
	call	_ZN6icu_6718CurrencyPluralInfo8initHashER10UErrorCode.part.0
	movq	%rax, %r13
.L407:
	movq	%r13, 8(%r12)
	movq	8(%rbx), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6718CurrencyPluralInfo8copyHashEPKNS_9HashtableEPS1_R10UErrorCode
	movl	32(%r12), %eax
	testl	%eax, %eax
	jg	.L399
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L408
	movq	(%rdi), %rax
	call	*8(%rax)
.L408:
	movq	$0, 16(%r12)
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L409
	movq	(%rdi), %rax
	call	*8(%rax)
.L409:
	movq	$0, 24(%r12)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L410
	call	_ZNK6icu_6711PluralRules5cloneEv@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L411
.L410:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L399
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L411
	movq	24(%rbx), %rdx
	cmpb	$0, 216(%rdx)
	jne	.L399
	cmpb	$0, 216(%rax)
	je	.L399
.L411:
	movl	$7, 32(%r12)
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L437:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L436:
	leaq	32(%rdi), %r14
	jmp	.L402
.L435:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2631:
	.size	_ZN6icu_6718CurrencyPluralInfoaSERKS0_, .-_ZN6icu_6718CurrencyPluralInfoaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CurrencyPluralInfoC2ERKS0_
	.type	_ZN6icu_6718CurrencyPluralInfoC2ERKS0_, @function
_ZN6icu_6718CurrencyPluralInfoC2ERKS0_:
.LFB2629:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6718CurrencyPluralInfoE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movl	$0, 32(%rdi)
	cmpq	%rsi, %rdi
	je	.L463
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	32(%rsi), %eax
	movl	%eax, 32(%rdi)
	testl	%eax, %eax
	jle	.L466
.L439:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	leaq	32(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN6icu_6718CurrencyPluralInfo8initHashER10UErrorCode.part.0
	movq	8(%r12), %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	%rax, 8(%rbx)
	movq	%rax, %rdx
	call	_ZN6icu_6718CurrencyPluralInfo8copyHashEPKNS_9HashtableEPS1_R10UErrorCode
	movl	32(%rbx), %eax
	testl	%eax, %eax
	jg	.L439
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L443
	movq	(%rdi), %rax
	call	*8(%rax)
.L443:
	movq	24(%rbx), %rdi
	movq	$0, 16(%rbx)
	testq	%rdi, %rdi
	je	.L444
	movq	(%rdi), %rax
	call	*8(%rax)
.L444:
	movq	16(%r12), %rdi
	movq	$0, 24(%rbx)
	testq	%rdi, %rdi
	je	.L445
	call	_ZNK6icu_6711PluralRules5cloneEv@PLT
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L446
.L445:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L439
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.L446
	movq	24(%r12), %rdx
	cmpb	$0, 216(%rdx)
	jne	.L439
	cmpb	$0, 216(%rax)
	je	.L439
.L446:
	movl	$7, 32(%rbx)
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE2629:
	.size	_ZN6icu_6718CurrencyPluralInfoC2ERKS0_, .-_ZN6icu_6718CurrencyPluralInfoC2ERKS0_
	.globl	_ZN6icu_6718CurrencyPluralInfoC1ERKS0_
	.set	_ZN6icu_6718CurrencyPluralInfoC1ERKS0_,_ZN6icu_6718CurrencyPluralInfoC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718CurrencyPluralInfo5cloneEv
	.type	_ZNK6icu_6718CurrencyPluralInfo5cloneEv, @function
_ZNK6icu_6718CurrencyPluralInfo5cloneEv:
.LFB2637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$40, %edi
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L467
	leaq	16+_ZTVN6icu_6718CurrencyPluralInfoE(%rip), %rbx
	movq	$0, 8(%rax)
	movq	%rbx, (%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movl	$0, 32(%rax)
	cmpq	%r13, %rax
	je	.L467
	movl	32(%r13), %eax
	movl	%eax, 32(%r12)
	testl	%eax, %eax
	jle	.L518
.L469:
	movq	8(%r12), %r13
	movq	%rbx, (%r12)
	testq	%r13, %r13
	je	.L482
	movl	$-1, -44(%rbp)
	leaq	-44(%rbp), %rbx
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L520:
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L519
.L484:
	movq	0(%r13), %rdi
	movq	%rbx, %rsi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	jne	.L520
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L486
	call	uhash_close_67@PLT
.L486:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L482:
	movq	$0, 8(%r12)
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L487
	movq	(%rdi), %rax
	call	*8(%rax)
.L487:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L488
	movq	(%rdi), %rax
	call	*8(%rax)
.L488:
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, 16(%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L467:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L521
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	leaq	32(%r12), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6718CurrencyPluralInfo8initHashER10UErrorCode.part.0
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, 8(%r12)
	movq	8(%r13), %rsi
	movq	%rax, %rdx
	call	_ZN6icu_6718CurrencyPluralInfo8copyHashEPKNS_9HashtableEPS1_R10UErrorCode
	movl	32(%r12), %eax
	testl	%eax, %eax
	jg	.L489
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L472
	movq	(%rdi), %rax
	call	*8(%rax)
.L472:
	movq	$0, 16(%r12)
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L473
	movq	(%rdi), %rax
	call	*8(%rax)
.L473:
	movq	$0, 24(%r12)
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L474
	call	_ZNK6icu_6711PluralRules5cloneEv@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L479
.L474:
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L517
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L479
	movq	24(%r13), %rdx
	cmpb	$0, 216(%rdx)
	jne	.L517
	cmpb	$0, 216(%rax)
	je	.L517
.L479:
	movl	$7, 32(%r12)
	movq	(%r12), %rax
	movq	8(%rax), %rax
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L519:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L517:
	movl	32(%r12), %eax
	testl	%eax, %eax
	jle	.L467
	.p2align 4,,10
	.p2align 3
.L489:
	movq	(%r12), %rax
	movq	8(%rax), %rax
.L490:
	leaq	_ZN6icu_6718CurrencyPluralInfoD0Ev(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L469
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*%rax
	jmp	.L467
.L521:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2637:
	.size	_ZNK6icu_6718CurrencyPluralInfo5cloneEv, .-_ZNK6icu_6718CurrencyPluralInfo5cloneEv
	.weak	_ZTSN6icu_6718CurrencyPluralInfoE
	.section	.rodata._ZTSN6icu_6718CurrencyPluralInfoE,"aG",@progbits,_ZTSN6icu_6718CurrencyPluralInfoE,comdat
	.align 16
	.type	_ZTSN6icu_6718CurrencyPluralInfoE, @object
	.size	_ZTSN6icu_6718CurrencyPluralInfoE, 30
_ZTSN6icu_6718CurrencyPluralInfoE:
	.string	"N6icu_6718CurrencyPluralInfoE"
	.weak	_ZTIN6icu_6718CurrencyPluralInfoE
	.section	.data.rel.ro._ZTIN6icu_6718CurrencyPluralInfoE,"awG",@progbits,_ZTIN6icu_6718CurrencyPluralInfoE,comdat
	.align 8
	.type	_ZTIN6icu_6718CurrencyPluralInfoE, @object
	.size	_ZTIN6icu_6718CurrencyPluralInfoE, 24
_ZTIN6icu_6718CurrencyPluralInfoE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718CurrencyPluralInfoE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6718CurrencyPluralInfoE
	.section	.data.rel.ro.local._ZTVN6icu_6718CurrencyPluralInfoE,"awG",@progbits,_ZTVN6icu_6718CurrencyPluralInfoE,comdat
	.align 8
	.type	_ZTVN6icu_6718CurrencyPluralInfoE, @object
	.size	_ZTVN6icu_6718CurrencyPluralInfoE, 40
_ZTVN6icu_6718CurrencyPluralInfoE:
	.quad	0
	.quad	_ZTIN6icu_6718CurrencyPluralInfoE
	.quad	_ZN6icu_6718CurrencyPluralInfoD1Ev
	.quad	_ZN6icu_6718CurrencyPluralInfoD0Ev
	.quad	_ZNK6icu_6718CurrencyPluralInfo17getDynamicClassIDEv
	.section	.rodata
	.align 16
	.type	_ZN6icu_67L15gCurrUnitPtnTagE, @object
	.size	_ZN6icu_67L15gCurrUnitPtnTagE, 21
_ZN6icu_67L15gCurrUnitPtnTagE:
	.string	"CurrencyUnitPatterns"
	.align 8
	.type	_ZN6icu_67L17gDecimalFormatTagE, @object
	.size	_ZN6icu_67L17gDecimalFormatTagE, 14
_ZN6icu_67L17gDecimalFormatTagE:
	.string	"decimalFormat"
	.align 8
	.type	_ZN6icu_67L12gPatternsTagE, @object
	.size	_ZN6icu_67L12gPatternsTagE, 9
_ZN6icu_67L12gPatternsTagE:
	.string	"patterns"
	.type	_ZN6icu_67L8gLatnTagE, @object
	.size	_ZN6icu_67L8gLatnTagE, 5
_ZN6icu_67L8gLatnTagE:
	.string	"latn"
	.align 8
	.type	_ZN6icu_67L18gNumberElementsTagE, @object
	.size	_ZN6icu_67L18gNumberElementsTagE, 15
_ZN6icu_67L18gNumberElementsTagE:
	.string	"NumberElements"
	.align 8
	.type	_ZN6icu_67L6gPart1E, @object
	.size	_ZN6icu_67L6gPart1E, 8
_ZN6icu_67L6gPart1E:
	.value	123
	.value	49
	.value	125
	.value	0
	.align 8
	.type	_ZN6icu_67L6gPart0E, @object
	.size	_ZN6icu_67L6gPart0E, 8
_ZN6icu_67L6gPart0E:
	.value	123
	.value	48
	.value	125
	.value	0
	.align 8
	.type	_ZN6icu_67L17gPluralCountOtherE, @object
	.size	_ZN6icu_67L17gPluralCountOtherE, 12
_ZN6icu_67L17gPluralCountOtherE:
	.value	111
	.value	116
	.value	104
	.value	101
	.value	114
	.value	0
	.align 8
	.type	_ZN6icu_67L19gTripleCurrencySignE, @object
	.size	_ZN6icu_67L19gTripleCurrencySignE, 8
_ZN6icu_67L19gTripleCurrencySignE:
	.value	164
	.value	164
	.value	164
	.value	0
	.align 16
	.type	_ZN6icu_67L29gDefaultCurrencyPluralPatternE, @object
	.size	_ZN6icu_67L29gDefaultCurrencyPluralPatternE, 18
_ZN6icu_67L29gDefaultCurrencyPluralPatternE:
	.value	48
	.value	46
	.value	35
	.value	35
	.value	32
	.value	164
	.value	164
	.value	164
	.value	0
	.local	_ZZN6icu_6718CurrencyPluralInfo16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6718CurrencyPluralInfo16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
