	.file	"gender.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710GenderInfoD2Ev
	.type	_ZN6icu_6710GenderInfoD2Ev, @function
_ZN6icu_6710GenderInfoD2Ev:
.LFB3046:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6710GenderInfoE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3046:
	.size	_ZN6icu_6710GenderInfoD2Ev, .-_ZN6icu_6710GenderInfoD2Ev
	.globl	_ZN6icu_6710GenderInfoD1Ev
	.set	_ZN6icu_6710GenderInfoD1Ev,_ZN6icu_6710GenderInfoD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710GenderInfoD0Ev
	.type	_ZN6icu_6710GenderInfoD0Ev, @function
_ZN6icu_6710GenderInfoD0Ev:
.LFB3048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6710GenderInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3048:
	.size	_ZN6icu_6710GenderInfoD0Ev, .-_ZN6icu_6710GenderInfoD0Ev
	.p2align 4
	.type	gender_cleanup, @function
gender_cleanup:
.LFB3040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	_ZL16gGenderInfoCache(%rip), %rdi
	testq	%rdi, %rdi
	je	.L7
	call	uhash_close_67@PLT
	movq	_ZL5gObjs(%rip), %rax
	movq	$0, _ZL16gGenderInfoCache(%rip)
	testq	%rax, %rax
	je	.L7
	movq	-8(%rax), %rbx
	salq	$4, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L9
	leaq	_ZN6icu_6710GenderInfoD2Ev(%rip), %r12
	leaq	16+_ZTVN6icu_6710GenderInfoE(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L10:
	movq	-16(%rbx), %rax
	subq	$16, %rbx
	movq	(%rax), %rax
	cmpq	%r12, %rax
	jne	.L11
	movq	%r13, (%rbx)
	movq	%rbx, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	cmpq	%rbx, _ZL5gObjs(%rip)
	jne	.L10
.L9:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L7:
	movl	$1, %eax
	movl	$0, _ZL15gGenderInitOnce(%rip)
	mfence
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	*%rax
	cmpq	%rbx, _ZL5gObjs(%rip)
	jne	.L10
	jmp	.L9
	.cfi_endproc
.LFE3040:
	.size	gender_cleanup, .-gender_cleanup
	.p2align 4
	.globl	_ZN6icu_6720GenderInfo_initCacheER10UErrorCode
	.type	_ZN6icu_6720GenderInfo_initCacheER10UErrorCode, @function
_ZN6icu_6720GenderInfo_initCacheER10UErrorCode:
.LFB3041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	gender_cleanup(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$31, %edi
	subq	$8, %rsp
	call	ucln_i18n_registerCleanup_67@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L33
.L18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movl	$56, %edi
	call	_ZN6icu_677UMemorynaEm@PLT
	testq	%rax, %rax
	je	.L21
	movq	$3, (%rax)
	leaq	8(%rax), %rdx
	leaq	16+_ZTVN6icu_6710GenderInfoE(%rip), %r12
	movq	uhash_compareChars_67@GOTPCREL(%rip), %rsi
	movq	%r12, 8(%rax)
	movq	uhash_hashChars_67@GOTPCREL(%rip), %rdi
	movq	%rbx, %rcx
	movq	%r12, 24(%rax)
	movq	%r12, 40(%rax)
	movl	$0, 16(%rax)
	movl	$1, 32(%rax)
	movl	$2, 48(%rax)
	movq	%rdx, _ZL5gObjs(%rip)
	xorl	%edx, %edx
	call	uhash_open_67@PLT
	movl	(%rbx), %edx
	movq	%rax, _ZL16gGenderInfoCache(%rip)
	testl	%edx, %edx
	jle	.L22
	movq	_ZL5gObjs(%rip), %rax
	testq	%rax, %rax
	je	.L18
	movq	-8(%rax), %rbx
	salq	$4, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L24
	leaq	_ZN6icu_6710GenderInfoD2Ev(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L25:
	movq	-16(%rbx), %rax
	subq	$16, %rbx
	movq	(%rax), %rax
	cmpq	%r13, %rax
	jne	.L26
	movq	%r12, (%rbx)
	movq	%rbx, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	cmpq	_ZL5gObjs(%rip), %rbx
	jne	.L25
.L24:
	addq	$8, %rsp
	leaq	-8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydaEPv@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	*%rax
	cmpq	%rbx, _ZL5gObjs(%rip)
	jne	.L25
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L22:
	movq	uprv_free_67@GOTPCREL(%rip), %rsi
	addq	$8, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uhash_setKeyDeleter_67@PLT
.L21:
	.cfi_restore_state
	movq	$0, _ZL5gObjs(%rip)
	movl	$7, (%rbx)
	jmp	.L18
	.cfi_endproc
.LFE3041:
	.size	_ZN6icu_6720GenderInfo_initCacheER10UErrorCode, .-_ZN6icu_6720GenderInfo_initCacheER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710GenderInfoC2Ev
	.type	_ZN6icu_6710GenderInfoC2Ev, @function
_ZN6icu_6710GenderInfoC2Ev:
.LFB3043:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6710GenderInfoE(%rip), %rax
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE3043:
	.size	_ZN6icu_6710GenderInfoC2Ev, .-_ZN6icu_6710GenderInfoC2Ev
	.globl	_ZN6icu_6710GenderInfoC1Ev
	.set	_ZN6icu_6710GenderInfoC1Ev,_ZN6icu_6710GenderInfoC2Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"genderList"
.LC1:
	.string	"neutral"
.LC2:
	.string	"mixedNeutral"
.LC3:
	.string	"maleTaints"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710GenderInfo12loadInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6710GenderInfo12loadInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6710GenderInfo12loadInstanceERKNS_6LocaleER10UErrorCode:
.LFB3056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	xorl	%edi, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC0(%rip), %rsi
	subq	$312, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ures_openDirect_67@PLT
	movl	(%rbx), %edx
	movq	%rax, %r12
	testl	%edx, %edx
	jg	.L36
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	call	ures_getByKey_67@PLT
	movq	%rax, %r13
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L58
.L37:
	testq	%r13, %r13
	je	.L36
.L63:
	movq	%r13, %rdi
	call	ures_close_67@PLT
.L36:
	testq	%r12, %r12
	je	.L35
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L35:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	addq	$312, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	40(%r14), %rsi
	leaq	-324(%rbp), %rbx
	movq	%r13, %rdi
	leaq	-328(%rbp), %r15
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	$0, -328(%rbp)
	leaq	-320(%rbp), %r14
	movq	%rsi, -344(%rbp)
	movl	$0, -324(%rbp)
	call	ures_getStringByKey_67@PLT
	movq	-344(%rbp), %rsi
	testq	%rax, %rax
	je	.L60
.L38:
	movl	-328(%rbp), %ecx
	pxor	%xmm0, %xmm0
	movq	%rax, %rdi
	movq	%r14, %rsi
	movaps	%xmm0, -320(%rbp)
	leal	1(%rcx), %edx
	movaps	%xmm0, -304(%rbp)
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -272(%rbp)
	movaps	%xmm0, -256(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	u_UCharsToChars_67@PLT
	movl	$8, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%r14, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L39
	movl	$13, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%r14, %rsi
	movq	_ZL5gObjs(%rip), %r15
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L61
	movl	$11, %ecx
	leaq	.LC3(%rip), %rdi
	leaq	32(%r15), %rdx
	movq	%r14, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	cmove	%rdx, %r15
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$157, %edx
	movq	%r14, %rdi
	movl	$0, -324(%rbp)
	call	__strcpy_chk@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	$0, -324(%rbp)
	movl	$0, -328(%rbp)
	call	ures_getStringByKey_67@PLT
	movl	$0, -324(%rbp)
	testq	%rax, %rax
	jne	.L38
.L40:
	movq	%rbx, %rcx
	movl	$157, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	uloc_getParent_67@PLT
	testl	%eax, %eax
	jg	.L62
.L39:
	movq	_ZL5gObjs(%rip), %r15
	testq	%r13, %r13
	jne	.L63
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L61:
	addq	$16, %r15
	jmp	.L37
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3056:
	.size	_ZN6icu_6710GenderInfo12loadInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6710GenderInfo12loadInstanceERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCode:
.LFB3049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	jle	.L78
.L65:
	xorl	%r12d, %r12d
.L64:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movl	_ZL15gGenderInitOnce(%rip), %eax
	movq	%rdi, %r14
	movq	%rsi, %rbx
	cmpl	$2, %eax
	jne	.L79
.L66:
	movl	4+_ZL15gGenderInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L67
	movl	%eax, (%rbx)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L79:
	leaq	_ZL15gGenderInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L66
	movq	%rbx, %rdi
	call	_ZN6icu_6720GenderInfo_initCacheER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL15gGenderInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL15gGenderInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L67:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L65
	movq	40(%r14), %r13
	leaq	_ZZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCodeE15gGenderMetaLock(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	_ZL16gGenderInfoCache(%rip), %rdi
	movq	%r13, %rsi
	call	uhash_get_67@PLT
	leaq	_ZZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCodeE15gGenderMetaLock(%rip), %rdi
	movq	%rax, %r12
	call	umtx_unlock_67@PLT
	testq	%r12, %r12
	jne	.L64
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6710GenderInfo12loadInstanceERKNS_6LocaleER10UErrorCode
	movl	(%rbx), %edx
	movq	%rax, %r14
	testl	%edx, %edx
	jg	.L65
	leaq	_ZZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCodeE15gGenderMetaLock(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	_ZL16gGenderInfoCache(%rip), %rdi
	movq	%r13, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L80
.L69:
	leaq	_ZZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCodeE15gGenderMetaLock(%rip), %rdi
	call	umtx_unlock_67@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	%r13, %rdi
	call	uprv_strdup_67@PLT
	movq	_ZL16gGenderInfoCache(%rip), %rdi
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%rax, %rsi
	call	uhash_put_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L69
	movq	%r14, %r12
	jmp	.L69
	.cfi_endproc
.LFE3049:
	.size	_ZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710GenderInfo13getListGenderEPK7UGenderiR10UErrorCode
	.type	_ZNK6icu_6710GenderInfo13getListGenderEPK7UGenderiR10UErrorCode, @function
_ZNK6icu_6710GenderInfo13getListGenderEPK7UGenderiR10UErrorCode:
.LFB3057:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L95
	testl	%edx, %edx
	je	.L95
	cmpl	$1, %edx
	je	.L103
	movl	8(%rdi), %eax
	cmpl	$1, %eax
	je	.L84
	cmpl	$2, %eax
	jne	.L95
	testl	%edx, %edx
	jle	.L96
	leal	-1(%rdx), %eax
	leaq	4(%rsi,%rax,4), %rdx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L105:
	addq	$4, %rsi
	cmpq	%rdx, %rsi
	je	.L104
.L91:
	movl	(%rsi), %eax
	cmpl	$1, %eax
	je	.L105
	xorl	%eax, %eax
.L81:
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	movl	(%rsi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	testl	%edx, %edx
	jle	.L96
	leal	-1(%rdx), %eax
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	leaq	4(%rsi,%rax,4), %rdx
.L90:
	movl	(%rsi), %eax
	cmpl	$1, %eax
	je	.L87
	cmpl	$2, %eax
	je	.L81
	testl	%eax, %eax
	je	.L106
.L89:
	addq	$4, %rsi
	cmpq	%rdx, %rsi
	jne	.L90
	xorl	$1, %ecx
	movsbl	%cl, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	testb	%cl, %cl
	jne	.L95
	movl	$1, %edi
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L106:
	testb	%dil, %dil
	jne	.L95
	movl	$1, %ecx
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L104:
	ret
.L96:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3057:
	.size	_ZNK6icu_6710GenderInfo13getListGenderEPK7UGenderiR10UErrorCode, .-_ZNK6icu_6710GenderInfo13getListGenderEPK7UGenderiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710GenderInfo18getNeutralInstanceEv
	.type	_ZN6icu_6710GenderInfo18getNeutralInstanceEv, @function
_ZN6icu_6710GenderInfo18getNeutralInstanceEv:
.LFB3058:
	.cfi_startproc
	endbr64
	movq	_ZL5gObjs(%rip), %rax
	ret
	.cfi_endproc
.LFE3058:
	.size	_ZN6icu_6710GenderInfo18getNeutralInstanceEv, .-_ZN6icu_6710GenderInfo18getNeutralInstanceEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710GenderInfo23getMixedNeutralInstanceEv
	.type	_ZN6icu_6710GenderInfo23getMixedNeutralInstanceEv, @function
_ZN6icu_6710GenderInfo23getMixedNeutralInstanceEv:
.LFB3059:
	.cfi_startproc
	endbr64
	movq	_ZL5gObjs(%rip), %rax
	addq	$16, %rax
	ret
	.cfi_endproc
.LFE3059:
	.size	_ZN6icu_6710GenderInfo23getMixedNeutralInstanceEv, .-_ZN6icu_6710GenderInfo23getMixedNeutralInstanceEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710GenderInfo21getMaleTaintsInstanceEv
	.type	_ZN6icu_6710GenderInfo21getMaleTaintsInstanceEv, @function
_ZN6icu_6710GenderInfo21getMaleTaintsInstanceEv:
.LFB3060:
	.cfi_startproc
	endbr64
	movq	_ZL5gObjs(%rip), %rax
	addq	$32, %rax
	ret
	.cfi_endproc
.LFE3060:
	.size	_ZN6icu_6710GenderInfo21getMaleTaintsInstanceEv, .-_ZN6icu_6710GenderInfo21getMaleTaintsInstanceEv
	.p2align 4
	.globl	ugender_getListGender_67
	.type	ugender_getListGender_67, @function
ugender_getListGender_67:
.LFB3062:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L124
	testl	%edx, %edx
	je	.L124
	cmpl	$1, %edx
	je	.L132
	movl	8(%rdi), %eax
	cmpl	$1, %eax
	je	.L113
	cmpl	$2, %eax
	jne	.L124
	testl	%edx, %edx
	jle	.L125
	leal	-1(%rdx), %eax
	leaq	4(%rsi,%rax,4), %rdx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L134:
	addq	$4, %rsi
	cmpq	%rdx, %rsi
	je	.L133
.L120:
	movl	(%rsi), %eax
	cmpl	$1, %eax
	je	.L134
	xorl	%eax, %eax
.L110:
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	movl	(%rsi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	testl	%edx, %edx
	jle	.L125
	leal	-1(%rdx), %eax
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	leaq	4(%rsi,%rax,4), %rdx
.L119:
	movl	(%rsi), %eax
	cmpl	$1, %eax
	je	.L116
	cmpl	$2, %eax
	je	.L110
	testl	%eax, %eax
	je	.L135
.L118:
	addq	$4, %rsi
	cmpq	%rdx, %rsi
	jne	.L119
	xorl	$1, %ecx
	movsbl	%cl, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	testb	%cl, %cl
	jne	.L124
	movl	$1, %edi
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L135:
	testb	%dil, %dil
	jne	.L124
	movl	$1, %ecx
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L133:
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3062:
	.size	ugender_getListGender_67, .-ugender_getListGender_67
	.p2align 4
	.globl	ugender_getInstance_67
	.type	ugender_getInstance_67, @function
ugender_getInstance_67:
.LFB3061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-288(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L152
.L137:
	xorl	%r12d, %r12d
.L140:
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movl	_ZL15gGenderInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L154
.L138:
	movl	4+_ZL15gGenderInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L139
	movl	%eax, (%rbx)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L154:
	leaq	_ZL15gGenderInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L138
	movq	%rbx, %rdi
	call	_ZN6icu_6720GenderInfo_initCacheER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL15gGenderInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL15gGenderInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L139:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L137
	movq	-248(%rbp), %r14
	leaq	_ZZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCodeE15gGenderMetaLock(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	_ZL16gGenderInfoCache(%rip), %rdi
	movq	%r14, %rsi
	call	uhash_get_67@PLT
	leaq	_ZZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCodeE15gGenderMetaLock(%rip), %rdi
	movq	%rax, %r12
	call	umtx_unlock_67@PLT
	testq	%r12, %r12
	jne	.L140
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710GenderInfo12loadInstanceERKNS_6LocaleER10UErrorCode
	movl	(%rbx), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	jg	.L137
	leaq	_ZZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCodeE15gGenderMetaLock(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	_ZL16gGenderInfoCache(%rip), %rdi
	movq	%r14, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L155
.L141:
	leaq	_ZZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCodeE15gGenderMetaLock(%rip), %rdi
	call	umtx_unlock_67@PLT
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%r14, %rdi
	call	uprv_strdup_67@PLT
	movq	_ZL16gGenderInfoCache(%rip), %rdi
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%rax, %rsi
	call	uhash_put_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L141
	movq	%r15, %r12
	jmp	.L141
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3061:
	.size	ugender_getInstance_67, .-ugender_getInstance_67
	.weak	_ZTSN6icu_6710GenderInfoE
	.section	.rodata._ZTSN6icu_6710GenderInfoE,"aG",@progbits,_ZTSN6icu_6710GenderInfoE,comdat
	.align 16
	.type	_ZTSN6icu_6710GenderInfoE, @object
	.size	_ZTSN6icu_6710GenderInfoE, 22
_ZTSN6icu_6710GenderInfoE:
	.string	"N6icu_6710GenderInfoE"
	.weak	_ZTIN6icu_6710GenderInfoE
	.section	.data.rel.ro._ZTIN6icu_6710GenderInfoE,"awG",@progbits,_ZTIN6icu_6710GenderInfoE,comdat
	.align 8
	.type	_ZTIN6icu_6710GenderInfoE, @object
	.size	_ZTIN6icu_6710GenderInfoE, 24
_ZTIN6icu_6710GenderInfoE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6710GenderInfoE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6710GenderInfoE
	.section	.data.rel.ro._ZTVN6icu_6710GenderInfoE,"awG",@progbits,_ZTVN6icu_6710GenderInfoE,comdat
	.align 8
	.type	_ZTVN6icu_6710GenderInfoE, @object
	.size	_ZTVN6icu_6710GenderInfoE, 40
_ZTVN6icu_6710GenderInfoE:
	.quad	0
	.quad	_ZTIN6icu_6710GenderInfoE
	.quad	_ZN6icu_6710GenderInfoD1Ev
	.quad	_ZN6icu_6710GenderInfoD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.local	_ZZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCodeE15gGenderMetaLock
	.comm	_ZZN6icu_6710GenderInfo11getInstanceERKNS_6LocaleER10UErrorCodeE15gGenderMetaLock,56,32
	.local	_ZL15gGenderInitOnce
	.comm	_ZL15gGenderInitOnce,8,8
	.local	_ZL5gObjs
	.comm	_ZL5gObjs,8,8
	.local	_ZL16gGenderInfoCache
	.comm	_ZL16gGenderInfoCache,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
