	.file	"numrange_fluent.cpp"
	.text
	.section	.text._ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.type	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, @function
_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode:
.LFB3509:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3509:
	.size	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, .-_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.section	.text._ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv
	.type	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv, @function
_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv:
.LFB3510:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3510:
	.size	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv, .-_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv
	.section	.text._ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv
	.type	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv, @function
_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv:
.LFB3511:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3511:
	.size	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv, .-_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv
	.section	.text._ZNK6icu_676number4impl13EmptyModifier8isStrongEv,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier8isStrongEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv
	.type	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv, @function
_ZNK6icu_676number4impl13EmptyModifier8isStrongEv:
.LFB3512:
	.cfi_startproc
	endbr64
	movzbl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3512:
	.size	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv, .-_ZNK6icu_676number4impl13EmptyModifier8isStrongEv
	.section	.text._ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.type	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE, @function
_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE:
.LFB3513:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3513:
	.size	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE, .-_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.section	.text._ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE
	.type	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE, @function
_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE:
.LFB3514:
	.cfi_startproc
	endbr64
	movq	$0, (%rsi)
	ret
	.cfi_endproc
.LFE3514:
	.size	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE, .-_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE
	.section	.text._ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE
	.type	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE, @function
_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE:
.LFB3515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*32(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE3515:
	.size	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE, .-_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE
	.section	.text._ZN6icu_676number4impl13EmptyModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl13EmptyModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl13EmptyModifierD2Ev
	.type	_ZN6icu_676number4impl13EmptyModifierD2Ev, @function
_ZN6icu_676number4impl13EmptyModifierD2Ev:
.LFB5128:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	.cfi_endproc
.LFE5128:
	.size	_ZN6icu_676number4impl13EmptyModifierD2Ev, .-_ZN6icu_676number4impl13EmptyModifierD2Ev
	.weak	_ZN6icu_676number4impl13EmptyModifierD1Ev
	.set	_ZN6icu_676number4impl13EmptyModifierD1Ev,_ZN6icu_676number4impl13EmptyModifierD2Ev
	.section	.text._ZN6icu_676number4impl13EmptyModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl13EmptyModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl13EmptyModifierD0Ev
	.type	_ZN6icu_676number4impl13EmptyModifierD0Ev, @function
_ZN6icu_676number4impl13EmptyModifierD0Ev:
.LFB5130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE5130:
	.size	_ZN6icu_676number4impl13EmptyModifierD0Ev, .-_ZN6icu_676number4impl13EmptyModifierD0Ev
	.section	.text._ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode,"axG",@progbits,_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode
	.type	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode, @function
_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode:
.LFB3620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpq	%rdx, %rdi
	je	.L17
	movdqu	8(%rdi), %xmm0
	movq	%rdx, %r12
	leaq	200(%rdi), %rsi
	movups	%xmm0, 8(%rdx)
	movdqu	24(%rdi), %xmm1
	movups	%xmm1, 24(%rdx)
	movl	40(%rdi), %eax
	movl	%eax, 40(%rdx)
	movzbl	44(%rdi), %eax
	movb	%al, 44(%rdx)
	movq	48(%rdi), %rax
	movq	%rax, 48(%rdx)
	movl	56(%rdi), %eax
	movl	%eax, 56(%rdx)
	movq	60(%rdi), %rax
	movq	%rax, 60(%rdx)
	movl	68(%rdi), %eax
	movl	%eax, 68(%rdx)
	movq	72(%rdi), %rax
	movq	%rax, 72(%rdx)
	movzbl	80(%rdi), %eax
	movb	%al, 80(%rdx)
	movl	84(%rdi), %eax
	movl	%eax, 84(%rdx)
	movl	88(%rdi), %eax
	movl	%eax, 88(%rdx)
	movzbl	92(%rdi), %eax
	movb	%al, 92(%rdx)
	movzbl	93(%rdi), %eax
	movb	%al, 93(%rdx)
	movzbl	94(%rdi), %eax
	movb	%al, 94(%rdx)
	movzbl	95(%rdi), %eax
	movb	%al, 95(%rdx)
	movzbl	96(%rdi), %eax
	movb	%al, 96(%rdx)
	movzbl	97(%rdi), %eax
	movb	%al, 97(%rdx)
	movzbl	98(%rdi), %eax
	movb	%al, 98(%rdx)
	movzbl	99(%rdi), %eax
	movb	%al, 99(%rdx)
	movzbl	100(%rdi), %eax
	movb	%al, 100(%rdx)
	movzbl	101(%rdi), %eax
	movb	%al, 101(%rdx)
	movq	104(%rdi), %rax
	movq	%rax, 104(%rdx)
	movq	112(%rdi), %rax
	movq	%rax, 112(%rdx)
	movq	120(%rdi), %rax
	movq	%rax, 120(%rdx)
	movq	128(%rdi), %rax
	movq	%rax, 128(%rdx)
	movq	152(%rdi), %rax
	movl	144(%rdi), %edx
	movq	%rax, 152(%r12)
	movzbl	168(%rdi), %eax
	movl	%edx, 144(%r12)
	movb	%al, 168(%r12)
	movzbl	184(%rdi), %eax
	leaq	200(%r12), %rdi
	movb	%al, 184(%r12)
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	movq	224(%rbx), %rax
	movq	%rax, 224(%r12)
	movzbl	232(%rbx), %eax
	movb	%al, 232(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movb	$1, 232(%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3620:
	.size	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode, .-_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number20FormattedNumberRange8toStringER10UErrorCode
	.type	_ZNK6icu_676number20FormattedNumberRange8toStringER10UErrorCode, @function
_ZNK6icu_676number20FormattedNumberRange8toStringER10UErrorCode:
.LFB3813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L24
	movq	8(%rsi), %r8
	testq	%r8, %r8
	je	.L25
	movq	(%r8), %rax
	movq	%r8, %rsi
	call	*16(%rax)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L25:
	movl	16(%rsi), %eax
	movl	%eax, (%rdx)
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L18:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L26:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3813:
	.size	_ZNK6icu_676number20FormattedNumberRange8toStringER10UErrorCode, .-_ZNK6icu_676number20FormattedNumberRange8toStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number20FormattedNumberRange12toTempStringER10UErrorCode
	.type	_ZNK6icu_676number20FormattedNumberRange12toTempStringER10UErrorCode, @function
_ZNK6icu_676number20FormattedNumberRange12toTempStringER10UErrorCode:
.LFB3814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L33
	movq	8(%rsi), %r8
	testq	%r8, %r8
	je	.L34
	movq	(%r8), %rax
	movq	%r8, %rsi
	call	*24(%rax)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L34:
	movl	16(%rsi), %eax
	movl	%eax, (%rdx)
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L27:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L35
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L35:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3814:
	.size	_ZNK6icu_676number20FormattedNumberRange12toTempStringER10UErrorCode, .-_ZNK6icu_676number20FormattedNumberRange12toTempStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number20FormattedNumberRange8appendToERNS_10AppendableER10UErrorCode
	.type	_ZNK6icu_676number20FormattedNumberRange8appendToERNS_10AppendableER10UErrorCode, @function
_ZNK6icu_676number20FormattedNumberRange8appendToERNS_10AppendableER10UErrorCode:
.LFB3815:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L36
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L40
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	32(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L40:
	movl	16(%rdi), %eax
	movl	%eax, (%rdx)
.L36:
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3815:
	.size	_ZNK6icu_676number20FormattedNumberRange8appendToERNS_10AppendableER10UErrorCode, .-_ZNK6icu_676number20FormattedNumberRange8appendToERNS_10AppendableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number20FormattedNumberRange12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.type	_ZNK6icu_676number20FormattedNumberRange12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode, @function
_ZNK6icu_676number20FormattedNumberRange12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode:
.LFB3816:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L41
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L45
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	40(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L45:
	movl	16(%rdi), %eax
	movl	%eax, (%rdx)
.L41:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3816:
	.size	_ZNK6icu_676number20FormattedNumberRange12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode, .-_ZNK6icu_676number20FormattedNumberRange12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.section	.text._ZN6icu_676number4impl24ImmutablePatternModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl24ImmutablePatternModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev
	.type	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev, @function
_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev:
.LFB4421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L47
	movq	(%rdi), %rax
	call	*8(%rax)
.L47:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4421:
	.size	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev, .-_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev
	.section	.text._ZN6icu_676number4impl10MicroPropsD2Ev,"axG",@progbits,_ZN6icu_676number4impl10MicroPropsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl10MicroPropsD2Ev
	.type	_ZN6icu_676number4impl10MicroPropsD2Ev, @function
_ZN6icu_676number4impl10MicroPropsD2Ev:
.LFB5147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	192(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	200(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	subq	$8, %rsp
	movq	%rax, -200(%rdi)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rax, -8(%rdi)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%rbx, 176(%r12)
	leaq	176(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 160(%r12)
	leaq	160(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	136(%r12), %rdi
	movq	%rax, 136(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.cfi_endproc
.LFE5147:
	.size	_ZN6icu_676number4impl10MicroPropsD2Ev, .-_ZN6icu_676number4impl10MicroPropsD2Ev
	.weak	_ZN6icu_676number4impl10MicroPropsD1Ev
	.set	_ZN6icu_676number4impl10MicroPropsD1Ev,_ZN6icu_676number4impl10MicroPropsD2Ev
	.section	.text._ZN6icu_676number4impl10MicroPropsD0Ev,"axG",@progbits,_ZN6icu_676number4impl10MicroPropsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl10MicroPropsD0Ev
	.type	_ZN6icu_676number4impl10MicroPropsD0Ev, @function
_ZN6icu_676number4impl10MicroPropsD0Ev:
.LFB5149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	192(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	200(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	subq	$8, %rsp
	movq	%rax, -200(%rdi)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rax, -8(%rdi)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%rbx, 176(%r12)
	leaq	176(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 160(%r12)
	leaq	160(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	136(%r12), %rdi
	movq	%rax, 136(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$240, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5149:
	.size	_ZN6icu_676number4impl10MicroPropsD0Ev, .-_ZN6icu_676number4impl10MicroPropsD0Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB4318:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4318:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB4321:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L69
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L57
	cmpb	$0, 12(%rbx)
	jne	.L70
.L61:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L57:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L61
	.cfi_endproc
.LFE4321:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB4324:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L73
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4324:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB4327:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L76
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE4327:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB4329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L82
.L78:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L83
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L83:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4329:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB4330:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4330:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB4331:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4331:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB4332:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4332:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4333:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4333:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4334:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4334:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB4335:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L99
	testl	%edx, %edx
	jle	.L99
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L102
.L91:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L91
	.cfi_endproc
.LFE4335:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB4336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L106
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L106
	testl	%r12d, %r12d
	jg	.L113
	cmpb	$0, 12(%rbx)
	jne	.L114
.L108:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L108
.L114:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L106:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4336:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB4337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L116
	movq	(%rdi), %r8
.L117:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L120
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L120
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L120:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4337:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB4338:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L127
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4338:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB4339:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4339:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB4340:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4340:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB4341:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4341:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB4343:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4343:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB4345:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4345:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.p2align 4
	.globl	_ZN6icu_676number4impl17touchRangeLocalesERNS1_15RangeMacroPropsE
	.type	_ZN6icu_676number4impl17touchRangeLocalesERNS1_15RangeMacroPropsE, @function
_ZN6icu_676number4impl17touchRangeLocalesERNS1_15RangeMacroPropsE:
.LFB3658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	920(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%r12, %rsi
	movq	%rdi, %rbx
	leaq	224(%rdi), %rdi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleaSERKS0_@PLT
	.cfi_endproc
.LFE3658:
	.size	_ZN6icu_676number4impl17touchRangeLocalesERNS1_15RangeMacroPropsE, .-_ZN6icu_676number4impl17touchRangeLocalesERNS1_15RangeMacroPropsE
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE:
.LFB4346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$1, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4346:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE:
.LFB4347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$1, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4347:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE:
.LFB4348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$1, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4348:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE:
.LFB4350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$1, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4350:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE:
.LFB4351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4351:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE:
.LFB4352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4352:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE:
.LFB4353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4353:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE:
.LFB4354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4354:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE:
.LFB4355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	leaq	456(%r12), %r15
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4355:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE:
.LFB4356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	leaq	456(%r12), %r15
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	456(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4356:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE:
.LFB4357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	leaq	456(%r12), %r15
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4357:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE:
.LFB4358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	leaq	456(%r12), %r15
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	456(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4358:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse:
.LFB4359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	leaq	920(%r12), %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	%r13d, 908(%r12)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4359:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse:
.LFB4360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	leaq	920(%r12), %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movl	%r13d, 908(%r12)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4360:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback:
.LFB4361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	leaq	920(%r12), %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	%r13d, 912(%r12)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4361:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback:
.LFB4362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	leaq	920(%r12), %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movl	%r13d, 912(%r12)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4362:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE5cloneEv,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE5cloneEv
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE5cloneEv, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE5cloneEv:
.LFB4363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$1152, %edi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L168
	leaq	8(%rax), %rdi
	leaq	8(%r12), %rsi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%r12), %rsi
	leaq	456(%rbx), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	920(%r12), %rsi
	movzbl	904(%r12), %eax
	leaq	920(%rbx), %rdi
	movb	%al, 904(%rbx)
	movq	908(%r12), %rax
	movq	%rax, 908(%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
.L168:
	movq	%rbx, 0(%r13)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4363:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE5cloneEv, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE5cloneEv
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE5cloneEv,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE5cloneEv
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE5cloneEv, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE5cloneEv:
.LFB4366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$1152, %edi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L174
	leaq	8(%rax), %rdi
	leaq	8(%r12), %rsi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	456(%r12), %rsi
	leaq	456(%rbx), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	920(%r12), %rsi
	movzbl	904(%r12), %eax
	leaq	920(%rbx), %rdi
	movb	%al, 904(%rbx)
	movq	908(%r12), %rax
	movq	%rax, 908(%rbx)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
.L174:
	movq	%rbx, 0(%r13)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4366:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE5cloneEv, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE5cloneEv
	.section	.text._ZNK6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE11copyErrorToER10UErrorCode,"axG",@progbits,_ZNK6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE11copyErrorToER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE11copyErrorToER10UErrorCode
	.type	_ZNK6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE11copyErrorToER10UErrorCode, @function
_ZNK6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE11copyErrorToER10UErrorCode:
.LFB4367:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jle	.L207
.L179:
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	cmpl	$3, 12(%rdi)
	je	.L181
	cmpl	$9, 72(%rdi)
	je	.L208
	cmpl	$-3, 120(%rdi)
	je	.L209
	cmpb	$0, 140(%rdi)
	jne	.L210
	movl	144(%rdi), %ecx
	leal	-1(%rcx), %edx
	cmpl	$1, %edx
	jbe	.L211
.L187:
	movl	192(%rdi), %edx
	testl	%edx, %edx
	jne	.L212
.L189:
	cmpl	$3, 460(%rdi)
	je	.L190
	cmpl	$9, 520(%rdi)
	je	.L213
	cmpl	$-3, 568(%rdi)
	je	.L214
	cmpb	$0, 588(%rdi)
	jne	.L215
	movl	592(%rdi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L216
.L196:
	movl	640(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L179
	movl	%edx, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	movl	124(%rdi), %edx
	movl	%edx, (%rsi)
	.p2align 4,,10
	.p2align 3
.L184:
	movl	$1, %eax
	testl	%edx, %edx
	jle	.L189
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	cmpq	$0, 152(%rdi)
	jne	.L187
	movl	$7, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	movl	16(%rdi), %edx
	movl	%edx, (%rsi)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L210:
	movl	132(%rdi), %edx
	movl	%edx, (%rsi)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L208:
	movl	80(%rdi), %edx
	movl	%edx, (%rsi)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L213:
	movl	528(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	movl	%edx, (%rsi)
	jmp	.L184
.L216:
	cmpq	$0, 600(%rdi)
	jne	.L196
	movl	$7, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	movl	464(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	movl	580(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	movl	572(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.cfi_endproc
.LFE4367:
	.size	_ZNK6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE11copyErrorToER10UErrorCode, .-_ZNK6icu_676number28NumberRangeFormatterSettingsINS0_31UnlocalizedNumberRangeFormatterEE11copyErrorToER10UErrorCode
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE:
.LFB4368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	$0, 1144(%r12)
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$1, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4368:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE:
.LFB4370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	$0, 1144(%r12)
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$1, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4370:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE:
.LFB4372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	$0, 1144(%r12)
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4372:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE:
.LFB4374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	$0, 1144(%r12)
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4374:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE:
.LFB4376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	leaq	456(%r12), %r15
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	$0, 1144(%r12)
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4376:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE:
.LFB4378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	leaq	456(%r12), %r15
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	leaq	920(%r12), %r13
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	movq	%r13, %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	$0, 1144(%r12)
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4378:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse:
.LFB4380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	leaq	920(%r12), %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	%r13d, 908(%r12)
	movq	%r12, %rax
	movq	$0, 1144(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4380:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback:
.LFB4382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	leaq	920(%r12), %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	%r13d, 912(%r12)
	movq	%r12, %rax
	movq	$0, 1144(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4382:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback
	.section	.text._ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE5cloneEv,"axG",@progbits,_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE5cloneEv
	.type	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE5cloneEv, @function
_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE5cloneEv:
.LFB4384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$1152, %edi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L234
	leaq	8(%rax), %rdi
	leaq	8(%r12), %rsi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%r12), %rsi
	leaq	456(%rbx), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	920(%r12), %rsi
	movzbl	904(%r12), %eax
	leaq	920(%rbx), %rdi
	movb	%al, 904(%rbx)
	movq	908(%r12), %rax
	movq	%rax, 908(%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	$0, 1144(%rbx)
.L234:
	movq	%rbx, 0(%r13)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4384:
	.size	_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE5cloneEv, .-_ZNKR6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE5cloneEv
	.section	.text._ZNK6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE11copyErrorToER10UErrorCode,"axG",@progbits,_ZNK6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE11copyErrorToER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE11copyErrorToER10UErrorCode
	.type	_ZNK6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE11copyErrorToER10UErrorCode, @function
_ZNK6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE11copyErrorToER10UErrorCode:
.LFB4388:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jle	.L267
.L239:
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	cmpl	$3, 12(%rdi)
	je	.L241
	cmpl	$9, 72(%rdi)
	je	.L268
	cmpl	$-3, 120(%rdi)
	je	.L269
	cmpb	$0, 140(%rdi)
	jne	.L270
	movl	144(%rdi), %ecx
	leal	-1(%rcx), %edx
	cmpl	$1, %edx
	jbe	.L271
.L247:
	movl	192(%rdi), %edx
	testl	%edx, %edx
	jne	.L272
.L249:
	cmpl	$3, 460(%rdi)
	je	.L250
	cmpl	$9, 520(%rdi)
	je	.L273
	cmpl	$-3, 568(%rdi)
	je	.L274
	cmpb	$0, 588(%rdi)
	jne	.L275
	movl	592(%rdi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L276
.L256:
	movl	640(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L239
	movl	%edx, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	movl	124(%rdi), %edx
	movl	%edx, (%rsi)
	.p2align 4,,10
	.p2align 3
.L244:
	movl	$1, %eax
	testl	%edx, %edx
	jle	.L249
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	cmpq	$0, 152(%rdi)
	jne	.L247
	movl	$7, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	movl	16(%rdi), %edx
	movl	%edx, (%rsi)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L270:
	movl	132(%rdi), %edx
	movl	%edx, (%rsi)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L268:
	movl	80(%rdi), %edx
	movl	%edx, (%rsi)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L273:
	movl	528(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	movl	%edx, (%rsi)
	jmp	.L244
.L276:
	cmpq	$0, 600(%rdi)
	jne	.L256
	movl	$7, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	movl	464(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	movl	580(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	movl	572(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.cfi_endproc
.LFE4388:
	.size	_ZNK6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE11copyErrorToER10UErrorCode, .-_ZNK6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE11copyErrorToER10UErrorCode
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number20NumberRangeFormatter4withEv
	.type	_ZN6icu_676number20NumberRangeFormatter4withEv, @function
_ZN6icu_676number20NumberRangeFormatter4withEv:
.LFB3677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	24(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movabsq	$30064771077, %rbx
	movq	$2, -12(%rdi)
	movl	$0, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	48(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %eax
	movl	$-1, %edx
	movq	%rbx, 160(%r12)
	movw	%ax, 108(%r12)
	pxor	%xmm0, %xmm0
	leaq	224(%r12), %rdi
	movw	%dx, 132(%r12)
	movl	$0, 72(%r12)
	movl	$4, 104(%r12)
	movl	$-2, 120(%r12)
	movb	$0, 140(%r12)
	movl	$0, 144(%r12)
	movq	$0, 152(%r12)
	movl	$2, 168(%r12)
	movl	$0, 176(%r12)
	movq	$0, 184(%r12)
	movl	$0, 192(%r12)
	movl	$3, 216(%r12)
	movups	%xmm0, 200(%r12)
	call	_ZN6icu_676LocaleC1Ev@PLT
	leaq	472(%r12), %rdi
	movq	$2, 460(%r12)
	movl	$0, 468(%r12)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	496(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %ecx
	movl	$-1, %esi
	movq	%rbx, 608(%r12)
	movw	%cx, 556(%r12)
	pxor	%xmm0, %xmm0
	leaq	672(%r12), %rdi
	movw	%si, 580(%r12)
	movl	$0, 520(%r12)
	movl	$4, 552(%r12)
	movl	$-2, 568(%r12)
	movb	$0, 588(%r12)
	movl	$0, 592(%r12)
	movq	$0, 600(%r12)
	movl	$2, 616(%r12)
	movl	$0, 624(%r12)
	movq	$0, 632(%r12)
	movl	$0, 640(%r12)
	movl	$3, 664(%r12)
	movups	%xmm0, 648(%r12)
	call	_ZN6icu_676LocaleC1Ev@PLT
	leaq	920(%r12), %rdi
	movabsq	$8589934592, %rax
	movb	$1, 904(%r12)
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3677:
	.size	_ZN6icu_676number20NumberRangeFormatter4withEv, .-_ZN6icu_676number20NumberRangeFormatter4withEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2ERKS1_
	.type	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2ERKS1_, @function
_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2ERKS1_:
.LFB3716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	8(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%r12), %rsi
	leaq	456(%rbx), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	920(%r12), %rsi
	movzbl	904(%r12), %eax
	leaq	920(%rbx), %rdi
	movb	%al, 904(%rbx)
	movq	908(%r12), %rax
	movq	%rax, 908(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleC1ERKS0_@PLT
	.cfi_endproc
.LFE3716:
	.size	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2ERKS1_, .-_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2ERKS1_
	.globl	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC1ERKS1_
	.set	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC1ERKS1_,_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2ERKS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2ERKNS0_28NumberRangeFormatterSettingsIS1_EE
	.type	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2ERKNS0_28NumberRangeFormatterSettingsIS1_EE, @function
_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2ERKNS0_28NumberRangeFormatterSettingsIS1_EE:
.LFB3725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	8(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%r12), %rsi
	leaq	456(%rbx), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	920(%r12), %rsi
	movzbl	904(%r12), %eax
	leaq	920(%rbx), %rdi
	movb	%al, 904(%rbx)
	movq	908(%r12), %rax
	movq	%rax, 908(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleC1ERKS0_@PLT
	.cfi_endproc
.LFE3725:
	.size	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2ERKNS0_28NumberRangeFormatterSettingsIS1_EE, .-_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2ERKNS0_28NumberRangeFormatterSettingsIS1_EE
	.globl	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC1ERKNS0_28NumberRangeFormatterSettingsIS1_EE
	.set	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC1ERKNS0_28NumberRangeFormatterSettingsIS1_EE,_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2ERKNS0_28NumberRangeFormatterSettingsIS1_EE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2EOS1_
	.type	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2EOS1_, @function
_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2EOS1_:
.LFB3728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	8(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	456(%r12), %rsi
	leaq	456(%rbx), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	920(%r12), %rsi
	movzbl	904(%r12), %eax
	leaq	920(%rbx), %rdi
	movb	%al, 904(%rbx)
	movq	908(%r12), %rax
	movq	%rax, 908(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleC1EOS0_@PLT
	.cfi_endproc
.LFE3728:
	.size	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2EOS1_, .-_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2EOS1_
	.globl	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC1EOS1_
	.set	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC1EOS1_,_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2EOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2EONS0_28NumberRangeFormatterSettingsIS1_EE
	.type	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2EONS0_28NumberRangeFormatterSettingsIS1_EE, @function
_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2EONS0_28NumberRangeFormatterSettingsIS1_EE:
.LFB3738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	8(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	456(%r12), %rsi
	leaq	456(%rbx), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	920(%r12), %rsi
	movzbl	904(%r12), %eax
	leaq	920(%rbx), %rdi
	movb	%al, 904(%rbx)
	movq	908(%r12), %rax
	movq	%rax, 908(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleC1EOS0_@PLT
	.cfi_endproc
.LFE3738:
	.size	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2EONS0_28NumberRangeFormatterSettingsIS1_EE, .-_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2EONS0_28NumberRangeFormatterSettingsIS1_EE
	.globl	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC1EONS0_28NumberRangeFormatterSettingsIS1_EE
	.set	_ZN6icu_676number31UnlocalizedNumberRangeFormatterC1EONS0_28NumberRangeFormatterSettingsIS1_EE,_ZN6icu_676number31UnlocalizedNumberRangeFormatterC2EONS0_28NumberRangeFormatterSettingsIS1_EE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number31UnlocalizedNumberRangeFormatteraSERKS1_
	.type	_ZN6icu_676number31UnlocalizedNumberRangeFormatteraSERKS1_, @function
_ZN6icu_676number31UnlocalizedNumberRangeFormatteraSERKS1_:
.LFB3740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	leaq	920(%r12), %rdi
	movb	%al, 904(%r12)
	movl	908(%rbx), %eax
	movl	%eax, 908(%r12)
	movl	912(%rbx), %eax
	movl	%eax, 912(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3740:
	.size	_ZN6icu_676number31UnlocalizedNumberRangeFormatteraSERKS1_, .-_ZN6icu_676number31UnlocalizedNumberRangeFormatteraSERKS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number31UnlocalizedNumberRangeFormatteraSEOS1_
	.type	_ZN6icu_676number31UnlocalizedNumberRangeFormatteraSEOS1_, @function
_ZN6icu_676number31UnlocalizedNumberRangeFormatteraSEOS1_:
.LFB3743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	leaq	920(%r12), %rdi
	movb	%al, 904(%r12)
	movl	908(%rbx), %eax
	movl	%eax, 908(%r12)
	movl	912(%rbx), %eax
	movl	%eax, 912(%r12)
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3743:
	.size	_ZN6icu_676number31UnlocalizedNumberRangeFormatteraSEOS1_, .-_ZN6icu_676number31UnlocalizedNumberRangeFormatteraSEOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKS1_
	.type	_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKS1_, @function
_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKS1_:
.LFB3747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	leaq	920(%r12), %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movq	$0, 1144(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3747:
	.size	_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKS1_, .-_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKS1_
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatterC1ERKS1_
	.set	_ZN6icu_676number29LocalizedNumberRangeFormatterC1ERKS1_,_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKNS0_28NumberRangeFormatterSettingsIS1_EE
	.type	_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKNS0_28NumberRangeFormatterSettingsIS1_EE, @function
_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKNS0_28NumberRangeFormatterSettingsIS1_EE:
.LFB3756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	8(%rsi), %rsi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r12), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	leaq	920(%r12), %rdi
	movb	%al, 904(%r12)
	movq	908(%rbx), %rax
	movq	%rax, 908(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movq	$0, 1144(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3756:
	.size	_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKNS0_28NumberRangeFormatterSettingsIS1_EE, .-_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKNS0_28NumberRangeFormatterSettingsIS1_EE
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatterC1ERKNS0_28NumberRangeFormatterSettingsIS1_EE
	.set	_ZN6icu_676number29LocalizedNumberRangeFormatterC1ERKNS0_28NumberRangeFormatterSettingsIS1_EE,_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKNS0_28NumberRangeFormatterSettingsIS1_EE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKNS0_4impl15RangeMacroPropsERKNS_6LocaleE
	.type	_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKNS0_4impl15RangeMacroPropsERKNS_6LocaleE, @function
_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKNS0_4impl15RangeMacroPropsERKNS_6LocaleE:
.LFB3794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$30064771077, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	672(%rbx), %r15
	subq	$40, %rsp
	movq	$2, -12(%rdi)
	movl	$0, -4(%rdi)
	movq	%r10, -72(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	48(%rbx), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-1, %edx
	pxor	%xmm0, %xmm0
	movl	$-3, %eax
	movw	%dx, 132(%rbx)
	leaq	224(%rbx), %r8
	movw	%ax, 108(%rbx)
	movq	%r8, %rdi
	movq	%r13, 160(%rbx)
	movl	$0, 72(%rbx)
	movl	$4, 104(%rbx)
	movl	$-2, 120(%rbx)
	movb	$0, 140(%rbx)
	movl	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movl	$2, 168(%rbx)
	movl	$0, 176(%rbx)
	movq	$0, 184(%rbx)
	movl	$0, 192(%rbx)
	movl	$3, 216(%rbx)
	movups	%xmm0, 200(%rbx)
	movq	%r8, -64(%rbp)
	call	_ZN6icu_676LocaleC1Ev@PLT
	leaq	456(%rbx), %r9
	movq	$2, 460(%rbx)
	leaq	472(%rbx), %rdi
	movl	$0, 468(%rbx)
	movq	%r9, -56(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	496(%rbx), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %ecx
	pxor	%xmm0, %xmm0
	movl	$-1, %esi
	movw	%cx, 556(%rbx)
	movq	%r15, %rdi
	movw	%si, 580(%rbx)
	movq	%r13, 608(%rbx)
	leaq	920(%rbx), %r13
	movl	$0, 520(%rbx)
	movl	$4, 552(%rbx)
	movl	$-2, 568(%rbx)
	movb	$0, 588(%rbx)
	movl	$0, 592(%rbx)
	movq	$0, 600(%rbx)
	movl	$2, 616(%rbx)
	movl	$0, 624(%rbx)
	movq	$0, 632(%rbx)
	movl	$0, 640(%rbx)
	movl	$3, 664(%rbx)
	movups	%xmm0, 648(%rbx)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movb	$1, 904(%rbx)
	movq	%r13, %rdi
	movabsq	$8589934592, %rax
	movq	%rax, 908(%rbx)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	-72(%rbp), %r10
	leaq	8(%r12), %rsi
	movq	$0, 1144(%rbx)
	movq	%r10, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	movq	-56(%rbp), %r9
	leaq	456(%r12), %rsi
	movq	%r9, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	920(%r12), %rsi
	movq	%r13, %rdi
	movzbl	904(%r12), %eax
	movb	%al, 904(%rbx)
	movq	908(%r12), %rax
	movq	%rax, 908(%rbx)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	-64(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$40, %rsp
	movq	%r13, %rsi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleaSERKS0_@PLT
	.cfi_endproc
.LFE3794:
	.size	_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKNS0_4impl15RangeMacroPropsERKNS_6LocaleE, .-_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKNS0_4impl15RangeMacroPropsERKNS_6LocaleE
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatterC1ERKNS0_4impl15RangeMacroPropsERKNS_6LocaleE
	.set	_ZN6icu_676number29LocalizedNumberRangeFormatterC1ERKNS0_4impl15RangeMacroPropsERKNS_6LocaleE,_ZN6icu_676number29LocalizedNumberRangeFormatterC2ERKNS0_4impl15RangeMacroPropsERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatterC2EONS0_4impl15RangeMacroPropsERKNS_6LocaleE
	.type	_ZN6icu_676number29LocalizedNumberRangeFormatterC2EONS0_4impl15RangeMacroPropsERKNS_6LocaleE, @function
_ZN6icu_676number29LocalizedNumberRangeFormatterC2EONS0_4impl15RangeMacroPropsERKNS_6LocaleE:
.LFB3798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$30064771077, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	672(%rbx), %r15
	subq	$40, %rsp
	movq	$2, -12(%rdi)
	movl	$0, -4(%rdi)
	movq	%r10, -72(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	48(%rbx), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-1, %edx
	pxor	%xmm0, %xmm0
	movl	$-3, %eax
	movw	%dx, 132(%rbx)
	leaq	224(%rbx), %r8
	movw	%ax, 108(%rbx)
	movq	%r8, %rdi
	movq	%r13, 160(%rbx)
	movl	$0, 72(%rbx)
	movl	$4, 104(%rbx)
	movl	$-2, 120(%rbx)
	movb	$0, 140(%rbx)
	movl	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movl	$2, 168(%rbx)
	movl	$0, 176(%rbx)
	movq	$0, 184(%rbx)
	movl	$0, 192(%rbx)
	movl	$3, 216(%rbx)
	movups	%xmm0, 200(%rbx)
	movq	%r8, -64(%rbp)
	call	_ZN6icu_676LocaleC1Ev@PLT
	leaq	456(%rbx), %r9
	movq	$2, 460(%rbx)
	leaq	472(%rbx), %rdi
	movl	$0, 468(%rbx)
	movq	%r9, -56(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	496(%rbx), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %ecx
	pxor	%xmm0, %xmm0
	movl	$-1, %esi
	movw	%cx, 556(%rbx)
	movq	%r15, %rdi
	movw	%si, 580(%rbx)
	movq	%r13, 608(%rbx)
	leaq	920(%rbx), %r13
	movl	$0, 520(%rbx)
	movl	$4, 552(%rbx)
	movl	$-2, 568(%rbx)
	movb	$0, 588(%rbx)
	movl	$0, 592(%rbx)
	movq	$0, 600(%rbx)
	movl	$2, 616(%rbx)
	movl	$0, 624(%rbx)
	movq	$0, 632(%rbx)
	movl	$0, 640(%rbx)
	movl	$3, 664(%rbx)
	movups	%xmm0, 648(%rbx)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movb	$1, 904(%rbx)
	movq	%r13, %rdi
	movabsq	$8589934592, %rax
	movq	%rax, 908(%rbx)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	-72(%rbp), %r10
	leaq	8(%r12), %rsi
	movq	$0, 1144(%rbx)
	movq	%r10, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	movq	-56(%rbp), %r9
	leaq	456(%r12), %rsi
	movq	%r9, %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	920(%r12), %rsi
	movq	%r13, %rdi
	movzbl	904(%r12), %eax
	movb	%al, 904(%rbx)
	movq	908(%r12), %rax
	movq	%rax, 908(%rbx)
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	-64(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$40, %rsp
	movq	%r13, %rsi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleaSERKS0_@PLT
	.cfi_endproc
.LFE3798:
	.size	_ZN6icu_676number29LocalizedNumberRangeFormatterC2EONS0_4impl15RangeMacroPropsERKNS_6LocaleE, .-_ZN6icu_676number29LocalizedNumberRangeFormatterC2EONS0_4impl15RangeMacroPropsERKNS_6LocaleE
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_4impl15RangeMacroPropsERKNS_6LocaleE
	.set	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_4impl15RangeMacroPropsERKNS_6LocaleE,_ZN6icu_676number29LocalizedNumberRangeFormatterC2EONS0_4impl15RangeMacroPropsERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZNKR6icu_676number31UnlocalizedNumberRangeFormatter6localeERKNS_6LocaleE
	.type	_ZNKR6icu_676number31UnlocalizedNumberRangeFormatter6localeERKNS_6LocaleE, @function
_ZNKR6icu_676number31UnlocalizedNumberRangeFormatter6localeERKNS_6LocaleE:
.LFB3800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_676number29LocalizedNumberRangeFormatterC1ERKNS0_4impl15RangeMacroPropsERKNS_6LocaleE
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3800:
	.size	_ZNKR6icu_676number31UnlocalizedNumberRangeFormatter6localeERKNS_6LocaleE, .-_ZNKR6icu_676number31UnlocalizedNumberRangeFormatter6localeERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZNO6icu_676number31UnlocalizedNumberRangeFormatter6localeERKNS_6LocaleE
	.type	_ZNO6icu_676number31UnlocalizedNumberRangeFormatter6localeERKNS_6LocaleE, @function
_ZNO6icu_676number31UnlocalizedNumberRangeFormatter6localeERKNS_6LocaleE:
.LFB3801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_4impl15RangeMacroPropsERKNS_6LocaleE
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3801:
	.size	_ZNO6icu_676number31UnlocalizedNumberRangeFormatter6localeERKNS_6LocaleE, .-_ZNO6icu_676number31UnlocalizedNumberRangeFormatter6localeERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number20FormattedNumberRangeC2EOS1_
	.type	_ZN6icu_676number20FormattedNumberRangeC2EOS1_, @function
_ZN6icu_676number20FormattedNumberRangeC2EOS1_:
.LFB3806:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number20FormattedNumberRangeE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rsi), %rax
	movq	$0, 8(%rsi)
	movq	%rax, 8(%rdi)
	movl	16(%rsi), %eax
	movl	$27, 16(%rsi)
	movl	%eax, 16(%rdi)
	ret
	.cfi_endproc
.LFE3806:
	.size	_ZN6icu_676number20FormattedNumberRangeC2EOS1_, .-_ZN6icu_676number20FormattedNumberRangeC2EOS1_
	.globl	_ZN6icu_676number20FormattedNumberRangeC1EOS1_
	.set	_ZN6icu_676number20FormattedNumberRangeC1EOS1_,_ZN6icu_676number20FormattedNumberRangeC2EOS1_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number20FormattedNumberRange15getFirstDecimalER10UErrorCode
	.type	_ZNK6icu_676number20FormattedNumberRange15getFirstDecimalER10UErrorCode, @function
_ZNK6icu_676number20FormattedNumberRange15getFirstDecimalER10UErrorCode:
.LFB3817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L310
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L311
	leaq	152(%rax), %rsi
	call	_ZNK6icu_676number4impl15DecimalQuantity18toScientificStringEv@PLT
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L311:
	movl	16(%rsi), %eax
	movl	%eax, (%rdx)
.L310:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L304:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L312
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L312:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3817:
	.size	_ZNK6icu_676number20FormattedNumberRange15getFirstDecimalER10UErrorCode, .-_ZNK6icu_676number20FormattedNumberRange15getFirstDecimalER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number20FormattedNumberRange16getSecondDecimalER10UErrorCode
	.type	_ZNK6icu_676number20FormattedNumberRange16getSecondDecimalER10UErrorCode, @function
_ZNK6icu_676number20FormattedNumberRange16getSecondDecimalER10UErrorCode:
.LFB3818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L319
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L320
	leaq	224(%rax), %rsi
	call	_ZNK6icu_676number4impl15DecimalQuantity18toScientificStringEv@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L320:
	movl	16(%rsi), %eax
	movl	%eax, (%rdx)
.L319:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L313:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L321
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L321:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3818:
	.size	_ZNK6icu_676number20FormattedNumberRange16getSecondDecimalER10UErrorCode, .-_ZNK6icu_676number20FormattedNumberRange16getSecondDecimalER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number20FormattedNumberRange17getIdentityResultER10UErrorCode
	.type	_ZNK6icu_676number20FormattedNumberRange17getIdentityResultER10UErrorCode, @function
_ZNK6icu_676number20FormattedNumberRange17getIdentityResultER10UErrorCode:
.LFB3819:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movl	$2, %eax
	testl	%edx, %edx
	jg	.L322
	movq	8(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L326
	movl	296(%rdx), %eax
.L322:
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	movl	16(%rdi), %edx
	movl	%edx, (%rsi)
	ret
	.cfi_endproc
.LFE3819:
	.size	_ZNK6icu_676number20FormattedNumberRange17getIdentityResultER10UErrorCode, .-_ZNK6icu_676number20FormattedNumberRange17getIdentityResultER10UErrorCode
	.section	.text._ZN6icu_676number4impl24ImmutablePatternModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl24ImmutablePatternModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.type	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev, @function
_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev:
.LFB4419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L328
	movq	(%rdi), %rax
	call	*8(%rax)
.L328:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.cfi_endproc
.LFE4419:
	.size	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev, .-_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD1Ev
	.set	_ZN6icu_676number4impl24ImmutablePatternModifierD1Ev,_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number20NumberRangeFormatter10withLocaleERKNS_6LocaleE
	.type	_ZN6icu_676number20NumberRangeFormatter10withLocaleERKNS_6LocaleE, @function
_ZN6icu_676number20NumberRangeFormatter10withLocaleERKNS_6LocaleE:
.LFB3714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1192(%rbp), %r14
	leaq	-1168(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	-1216(%rbp), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-992(%rbp), %rbx
	subq	$1224, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -1256(%rbp)
	movl	$2, -1204(%rbp)
	movq	$0, -1200(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %eax
	movl	$-1, %edx
	movq	%rbx, %rdi
	pxor	%xmm0, %xmm0
	movw	%ax, -1108(%rbp)
	movabsq	$30064771077, %rax
	movw	%dx, -1084(%rbp)
	movq	%rax, -1056(%rbp)
	movups	%xmm0, -1016(%rbp)
	movl	$0, -1144(%rbp)
	movl	$4, -1112(%rbp)
	movl	$-2, -1096(%rbp)
	movb	$0, -1076(%rbp)
	movl	$0, -1072(%rbp)
	movq	$0, -1064(%rbp)
	movl	$2, -1048(%rbp)
	movl	$0, -1040(%rbp)
	movq	$0, -1032(%rbp)
	movl	$0, -1024(%rbp)
	movl	$3, -1000(%rbp)
	call	_ZN6icu_676LocaleC1Ev@PLT
	leaq	-744(%rbp), %r8
	movl	$2, -756(%rbp)
	movq	%r8, %rdi
	movq	%r8, -1248(%rbp)
	movq	$0, -752(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	-720(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -1240(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	-544(%rbp), %r10
	movl	$-3, %ecx
	pxor	%xmm0, %xmm0
	movq	%r10, %rdi
	movl	$-1, %esi
	movabsq	$30064771077, %rax
	movw	%cx, -660(%rbp)
	movups	%xmm0, -568(%rbp)
	movq	%r10, -1232(%rbp)
	movw	%si, -636(%rbp)
	movq	%rax, -608(%rbp)
	movl	$0, -696(%rbp)
	movl	$4, -664(%rbp)
	movl	$-2, -648(%rbp)
	movb	$0, -628(%rbp)
	movl	$0, -624(%rbp)
	movq	$0, -616(%rbp)
	movl	$2, -600(%rbp)
	movl	$0, -592(%rbp)
	movq	$0, -584(%rbp)
	movl	$0, -576(%rbp)
	movl	$3, -552(%rbp)
	call	_ZN6icu_676LocaleC1Ev@PLT
	leaq	-296(%rbp), %r11
	movabsq	$8589934592, %rax
	movb	$1, -312(%rbp)
	movq	%r11, %rdi
	movq	%rax, -308(%rbp)
	movq	%r11, -1224(%rbp)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	-1256(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_4impl15RangeMacroPropsERKNS_6LocaleE
	movq	-1224(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-1232(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-592(%rbp), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-624(%rbp), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	movq	-1240(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	-1248(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-1040(%rbp), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-1072(%rbp), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L336
	addq	$1224, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L336:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3714:
	.size	_ZN6icu_676number20NumberRangeFormatter10withLocaleERKNS_6LocaleE, .-_ZN6icu_676number20NumberRangeFormatter10withLocaleERKNS_6LocaleE
	.section	.text._ZN6icu_676number4impl22MutablePatternModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl22MutablePatternModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl22MutablePatternModifierD2Ev
	.type	_ZN6icu_676number4impl22MutablePatternModifierD2Ev, @function
_ZN6icu_676number4impl22MutablePatternModifierD2Ev:
.LFB4412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$328, %rdi
	subq	$8, %rsp
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r12)
	jne	.L340
.L338:
	leaq	80(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	movq	112(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L338
	.cfi_endproc
.LFE4412:
	.size	_ZN6icu_676number4impl22MutablePatternModifierD2Ev, .-_ZN6icu_676number4impl22MutablePatternModifierD2Ev
	.weak	_ZN6icu_676number4impl22MutablePatternModifierD1Ev
	.set	_ZN6icu_676number4impl22MutablePatternModifierD1Ev,_ZN6icu_676number4impl22MutablePatternModifierD2Ev
	.section	.text._ZN6icu_676number4impl22MutablePatternModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl22MutablePatternModifierD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev
	.type	_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev, @function
_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev:
.LFB5168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 13, -24
	leaq	-8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$320, %rdi
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	232(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	168(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 116(%r12)
	jne	.L344
.L342:
	leaq	72(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	movq	104(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L342
	.cfi_endproc
.LFE5168:
	.size	_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev, .-_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev
	.p2align 4
	.weak	_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev
	.type	_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev, @function
_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev:
.LFB5167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$312, %rdi
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	224(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	160(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 108(%rbx)
	jne	.L348
.L346:
	leaq	64(%rbx), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L346
	.cfi_endproc
.LFE5167:
	.size	_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev, .-_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl22MutablePatternModifierD0Ev
	.type	_ZN6icu_676number4impl22MutablePatternModifierD0Ev, @function
_ZN6icu_676number4impl22MutablePatternModifierD0Ev:
.LFB4414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$328, %rdi
	subq	$8, %rsp
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r12)
	jne	.L352
.L350:
	leaq	80(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movq	112(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L350
	.cfi_endproc
.LFE4414:
	.size	_ZN6icu_676number4impl22MutablePatternModifierD0Ev, .-_ZN6icu_676number4impl22MutablePatternModifierD0Ev
	.section	.text._ZN6icu_676number4impl22MutablePatternModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl22MutablePatternModifierD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev
	.type	_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev, @function
_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev:
.LFB5169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$320, %rdi
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	232(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	168(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 116(%r12)
	jne	.L356
.L354:
	leaq	72(%r12), %rdi
	leaq	-8(%r12), %r13
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	movq	104(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L354
	.cfi_endproc
.LFE5169:
	.size	_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev, .-_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev
	.p2align 4
	.weak	_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev
	.type	_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev, @function
_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev:
.LFB5170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$312, %rdi
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	224(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	160(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 108(%rbx)
	jne	.L360
.L358:
	leaq	64(%rbx), %rdi
	leaq	-16(%rbx), %r12
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L358
	.cfi_endproc
.LFE5170:
	.size	_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev, .-_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev
	.section	.text._ZN6icu_676number4impl17ParsedPatternInfoD2Ev,"axG",@progbits,_ZN6icu_676number4impl17ParsedPatternInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl17ParsedPatternInfoD2Ev
	.type	_ZN6icu_676number4impl17ParsedPatternInfoD2Ev, @function
_ZN6icu_676number4impl17ParsedPatternInfoD2Ev:
.LFB4398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	296(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -296(%rdi)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r12), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	.cfi_endproc
.LFE4398:
	.size	_ZN6icu_676number4impl17ParsedPatternInfoD2Ev, .-_ZN6icu_676number4impl17ParsedPatternInfoD2Ev
	.weak	_ZN6icu_676number4impl17ParsedPatternInfoD1Ev
	.set	_ZN6icu_676number4impl17ParsedPatternInfoD1Ev,_ZN6icu_676number4impl17ParsedPatternInfoD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl25UFormattedNumberRangeDataD2Ev
	.type	_ZN6icu_676number4impl25UFormattedNumberRangeDataD2Ev, @function
_ZN6icu_676number4impl25UFormattedNumberRangeDataD2Ev:
.LFB3821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl25UFormattedNumberRangeDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	224(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -224(%rdi)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	152(%r12), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	.cfi_endproc
.LFE3821:
	.size	_ZN6icu_676number4impl25UFormattedNumberRangeDataD2Ev, .-_ZN6icu_676number4impl25UFormattedNumberRangeDataD2Ev
	.globl	_ZN6icu_676number4impl25UFormattedNumberRangeDataD1Ev
	.set	_ZN6icu_676number4impl25UFormattedNumberRangeDataD1Ev,_ZN6icu_676number4impl25UFormattedNumberRangeDataD2Ev
	.section	.text._ZN6icu_676number4impl17ParsedPatternInfoD0Ev,"axG",@progbits,_ZN6icu_676number4impl17ParsedPatternInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev
	.type	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev, @function
_ZN6icu_676number4impl17ParsedPatternInfoD0Ev:
.LFB4400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	296(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -296(%rdi)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r12), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4400:
	.size	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev, .-_ZN6icu_676number4impl17ParsedPatternInfoD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl25UFormattedNumberRangeDataD0Ev
	.type	_ZN6icu_676number4impl25UFormattedNumberRangeDataD0Ev, @function
_ZN6icu_676number4impl25UFormattedNumberRangeDataD0Ev:
.LFB3823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl25UFormattedNumberRangeDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	224(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -224(%rdi)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	152(%r12), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3823:
	.size	_ZN6icu_676number4impl25UFormattedNumberRangeDataD0Ev, .-_ZN6icu_676number4impl25UFormattedNumberRangeDataD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number20FormattedNumberRangeD2Ev
	.type	_ZN6icu_676number20FormattedNumberRangeD2Ev, @function
_ZN6icu_676number20FormattedNumberRangeD2Ev:
.LFB3809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number20FormattedNumberRangeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L370
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl25UFormattedNumberRangeDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L371
	leaq	16+_ZTVN6icu_676number4impl25UFormattedNumberRangeDataE(%rip), %rax
	leaq	224(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	152(%r13), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L370:
	movq	$0, 8(%r12)
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714FormattedValueD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L370
	.cfi_endproc
.LFE3809:
	.size	_ZN6icu_676number20FormattedNumberRangeD2Ev, .-_ZN6icu_676number20FormattedNumberRangeD2Ev
	.globl	_ZN6icu_676number20FormattedNumberRangeD1Ev
	.set	_ZN6icu_676number20FormattedNumberRangeD1Ev,_ZN6icu_676number20FormattedNumberRangeD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number20FormattedNumberRangeaSEOS1_
	.type	_ZN6icu_676number20FormattedNumberRangeaSEOS1_, @function
_ZN6icu_676number20FormattedNumberRangeaSEOS1_:
.LFB3812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L377
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl25UFormattedNumberRangeDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L378
	leaq	16+_ZTVN6icu_676number4impl25UFormattedNumberRangeDataE(%rip), %rax
	leaq	224(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	152(%r13), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L377:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	movl	16(%rbx), %eax
	movq	$0, 8(%rbx)
	movl	%eax, 16(%r12)
	movq	%r12, %rax
	movl	$27, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L377
	.cfi_endproc
.LFE3812:
	.size	_ZN6icu_676number20FormattedNumberRangeaSEOS1_, .-_ZN6icu_676number20FormattedNumberRangeaSEOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number20FormattedNumberRangeD0Ev
	.type	_ZN6icu_676number20FormattedNumberRangeD0Ev, @function
_ZN6icu_676number20FormattedNumberRangeD0Ev:
.LFB3811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number20FormattedNumberRangeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L384
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl25UFormattedNumberRangeDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L385
	leaq	16+_ZTVN6icu_676number4impl25UFormattedNumberRangeDataE(%rip), %rax
	leaq	224(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	152(%r13), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L384:
	movq	$0, 8(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_6714FormattedValueD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L384
	.cfi_endproc
.LFE3811:
	.size	_ZN6icu_676number20FormattedNumberRangeD0Ev, .-_ZN6icu_676number20FormattedNumberRangeD0Ev
	.section	.text._ZN6icu_676number4impl24NumberRangeFormatterImplD2Ev,"axG",@progbits,_ZN6icu_676number4impl24NumberRangeFormatterImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl24NumberRangeFormatterImplD2Ev
	.type	_ZN6icu_676number4impl24NumberRangeFormatterImplD2Ev, @function
_ZN6icu_676number4impl24NumberRangeFormatterImplD2Ev:
.LFB3779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 1300(%rdi)
	jne	.L475
.L391:
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	leaq	1192(%rbx), %rdi
	movq	%rax, 1184(%rbx)
	leaq	1184(%rbx), %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	1112(%rbx), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	1032(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	968(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 916(%rbx)
	jne	.L476
.L392:
	leaq	872(%rbx), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	856(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L393
	movq	(%rdi), %rax
	call	*8(%rax)
.L393:
	movq	848(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L394
	movq	(%rdi), %rax
	call	*8(%rax)
.L394:
	movq	840(%rbx), %r12
	testq	%r12, %r12
	je	.L395
	movq	(%r12), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L396
	movq	8(%r12), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L397
	movq	(%rdi), %rax
	call	*8(%rax)
.L397:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L395:
	movq	832(%rbx), %r12
	testq	%r12, %r12
	je	.L398
	movq	(%r12), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L399
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r12), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r12)
	jne	.L477
.L400:
	leaq	80(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L398:
	movq	824(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L401
	movq	(%rdi), %rax
	call	*8(%rax)
.L401:
	movq	816(%rbx), %r12
	testq	%r12, %r12
	je	.L402
	movq	(%r12), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L403
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r12), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L402:
	movq	808(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L404
	movq	(%rdi), %rax
	call	*8(%rax)
.L404:
	movq	800(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L405
	movq	(%rdi), %rax
	call	*8(%rax)
.L405:
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %r15
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %r14
	movq	%r15, 560(%rbx)
	leaq	752(%rbx), %r12
	leaq	760(%rbx), %rdi
	movq	%r14, 752(%rbx)
	leaq	560(%rbx), %r13
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r12, %rdi
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %r12
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r12, 736(%rbx)
	leaq	736(%rbx), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r12, 720(%rbx)
	leaq	720(%rbx), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	696(%rbx), %rdi
	movq	%rax, 696(%rbx)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	leaq	488(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	424(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 372(%rbx)
	jne	.L478
.L406:
	leaq	328(%rbx), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	312(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L407
	movq	(%rdi), %rax
	call	*8(%rax)
.L407:
	movq	304(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L408
	movq	(%rdi), %rax
	call	*8(%rax)
.L408:
	movq	296(%rbx), %r13
	testq	%r13, %r13
	je	.L409
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L410
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L411
	movq	(%rdi), %rax
	call	*8(%rax)
.L411:
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L409:
	movq	288(%rbx), %r13
	testq	%r13, %r13
	je	.L412
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L413
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r13), %rdi
	leaq	-104(%rax), %rsi
	movq	%rax, %xmm2
	addq	$88, %rax
	movq	%rsi, %xmm0
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 0(%r13)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r13)
	jne	.L479
.L414:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r13), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L412:
	movq	280(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L415
	movq	(%rdi), %rax
	call	*8(%rax)
.L415:
	movq	272(%rbx), %r13
	testq	%r13, %r13
	je	.L416
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L417
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r13), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L416:
	movq	264(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L418
	movq	(%rdi), %rax
	call	*8(%rax)
.L418:
	movq	256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L419
	movq	(%rdi), %rax
	call	*8(%rax)
.L419:
	movq	%r15, 16(%rbx)
	leaq	216(%rbx), %rdi
	movq	%r14, 208(%rbx)
	leaq	208(%rbx), %r14
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r12, 192(%rbx)
	leaq	192(%rbx), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r12, 176(%rbx)
	leaq	16(%rbx), %r12
	leaq	176(%rbx), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	152(%rbx), %rdi
	movq	%rax, 152(%rbx)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	movq	904(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L475:
	movq	1288(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L478:
	movq	360(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L477:
	movq	112(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L479:
	movq	112(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L399:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L410:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L403:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L413:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L417:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L395
	.cfi_endproc
.LFE3779:
	.size	_ZN6icu_676number4impl24NumberRangeFormatterImplD2Ev, .-_ZN6icu_676number4impl24NumberRangeFormatterImplD2Ev
	.weak	_ZN6icu_676number4impl24NumberRangeFormatterImplD1Ev
	.set	_ZN6icu_676number4impl24NumberRangeFormatterImplD1Ev,_ZN6icu_676number4impl24NumberRangeFormatterImplD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number29LocalizedNumberRangeFormatter10formatImplERNS0_4impl25UFormattedNumberRangeDataEbR10UErrorCode
	.type	_ZNK6icu_676number29LocalizedNumberRangeFormatter10formatImplERNS0_4impl25UFormattedNumberRangeDataEbR10UErrorCode, @function
_ZNK6icu_676number29LocalizedNumberRangeFormatter10formatImplERNS0_4impl25UFormattedNumberRangeDataEbR10UErrorCode:
.LFB3803:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L493
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	1144(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rax, -64(%rbp)
	movq	1144(%rdi), %r15
	movq	%rdi, -56(%rbp)
	movq	%r15, %r13
	testq	%r15, %r15
	je	.L496
.L486:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L480
.L488:
	movzbl	%r14b, %edx
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_676number4impl24NumberRangeFormatterImpl6formatERNS1_25UFormattedNumberRangeDataEbR10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L497
.L480:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	movl	$1352, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L483
	movq	-56(%rbp), %r9
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%r9, %rsi
	call	_ZN6icu_676number4impl24NumberRangeFormatterImplC1ERKNS1_15RangeMacroPropsER10UErrorCode@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L480
	movq	-64(%rbp), %rcx
	movq	%r15, %rax
	lock cmpxchgq	%r13, (%rcx)
	movq	%rax, %r15
	je	.L486
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl24NumberRangeFormatterImplD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	cmpl	$0, (%r12)
	jg	.L480
	testq	%r15, %r15
	jne	.L487
	movl	$5, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	addq	$24, %rsp
	leaq	8(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6722FormattedStringBuilder15writeTerminatorER10UErrorCode@PLT
.L483:
	.cfi_restore_state
	cmpl	$0, (%r12)
	jg	.L480
	movl	$7, (%r12)
	jmp	.L480
.L487:
	movq	%r15, %r13
	jmp	.L488
	.cfi_endproc
.LFE3803:
	.size	_ZNK6icu_676number29LocalizedNumberRangeFormatter10formatImplERNS0_4impl25UFormattedNumberRangeDataEbR10UErrorCode, .-_ZNK6icu_676number29LocalizedNumberRangeFormatter10formatImplERNS0_4impl25UFormattedNumberRangeDataEbR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number29LocalizedNumberRangeFormatter22formatFormattableRangeERKNS_11FormattableES4_R10UErrorCode
	.type	_ZNK6icu_676number29LocalizedNumberRangeFormatter22formatFormattableRangeERKNS_11FormattableES4_R10UErrorCode, @function
_ZNK6icu_676number29LocalizedNumberRangeFormatter22formatFormattableRangeERKNS_11FormattableES4_R10UErrorCode:
.LFB3802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movl	(%r8), %esi
	testl	%esi, %esi
	jle	.L499
	leaq	16+_ZTVN6icu_676number20FormattedNumberRangeE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$1, 16(%rdi)
.L498:
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	movl	$304, %edi
	movq	%rdx, %r12
	movq	%rcx, %r13
	movq	%r8, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L501
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	movq	%rax, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl25UFormattedNumberRangeDataE(%rip), %rax
	movq	%rax, (%r15)
	leaq	152(%r15), %rax
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	leaq	224(%r15), %r10
	movq	%r10, %rdi
	movq	%r10, -72(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	-64(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	$3, 296(%r15)
	call	_ZNK6icu_6711Formattable23populateDecimalQuantityERNS_6number4impl15DecimalQuantityER10UErrorCode@PLT
	movl	(%rbx), %eax
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	jle	.L502
.L508:
	leaq	16+_ZTVN6icu_676number20FormattedNumberRangeE(%rip), %rbx
	movq	$0, 8(%r14)
	movq	%rbx, (%r14)
	movl	%eax, 16(%r14)
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L502:
	movq	%r10, %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%r10, -72(%rbp)
	call	_ZNK6icu_6711Formattable23populateDecimalQuantityERNS_6number4impl15DecimalQuantityER10UErrorCode@PLT
	movl	(%rbx), %eax
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	jle	.L503
	leaq	16+_ZTVN6icu_676number20FormattedNumberRangeE(%rip), %rcx
	movq	$0, 8(%r14)
	movq	%rcx, (%r14)
	movl	%eax, 16(%r14)
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L503:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, -72(%rbp)
	call	_ZNK6icu_6711FormattableeqERKS0_@PLT
	xorl	%edx, %edx
	movq	-56(%rbp), %rdi
	movq	%rbx, %rcx
	testb	%al, %al
	movq	%r15, %rsi
	setne	%dl
	call	_ZNK6icu_676number29LocalizedNumberRangeFormatter10formatImplERNS0_4impl25UFormattedNumberRangeDataEbR10UErrorCode
	movl	(%rbx), %eax
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	jle	.L509
	movq	(%r15), %rax
	leaq	_ZN6icu_676number4impl25UFormattedNumberRangeDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L505
	leaq	16+_ZTVN6icu_676number4impl25UFormattedNumberRangeDataE(%rip), %rax
	movq	%r10, %rdi
	movq	%rax, (%r15)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	-64(%rbp), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	(%rbx), %eax
	jmp	.L508
.L509:
	leaq	16+_ZTVN6icu_676number20FormattedNumberRangeE(%rip), %rax
	movq	%r15, 8(%r14)
	movq	%rax, (%r14)
	movl	$0, 16(%r14)
	jmp	.L498
.L505:
	movq	%r15, %rdi
	call	*%rax
	movl	(%rbx), %eax
	jmp	.L508
.L501:
	leaq	16+_ZTVN6icu_676number20FormattedNumberRangeE(%rip), %rax
	movl	$7, (%rbx)
	movq	%rax, (%r14)
	movq	$0, 8(%r14)
	movl	$7, 16(%r14)
	jmp	.L498
	.cfi_endproc
.LFE3802:
	.size	_ZNK6icu_676number29LocalizedNumberRangeFormatter22formatFormattableRangeERKNS_11FormattableES4_R10UErrorCode, .-_ZNK6icu_676number29LocalizedNumberRangeFormatter22formatFormattableRangeERKNS_11FormattableES4_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number29LocalizedNumberRangeFormatter12getFormatterER10UErrorCode
	.type	_ZNK6icu_676number29LocalizedNumberRangeFormatter12getFormatterER10UErrorCode, @function
_ZNK6icu_676number29LocalizedNumberRangeFormatter12getFormatterER10UErrorCode:
.LFB3804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L515
	movq	%rdi, %r12
	leaq	1144(%rdi), %r14
	movq	1144(%rdi), %r15
	testq	%r15, %r15
	je	.L513
.L521:
	movq	%r15, %r13
.L510:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore_state
	movl	$1352, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L514
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_676number4impl24NumberRangeFormatterImplC1ERKNS1_15RangeMacroPropsER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L515
	movq	%r15, %rax
	lock cmpxchgq	%r13, (%r14)
	movq	%rax, %r15
	je	.L510
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl24NumberRangeFormatterImplD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L515:
	xorl	%r13d, %r13d
	jmp	.L510
.L514:
	cmpl	$0, (%rbx)
	jg	.L515
	movl	$7, (%rbx)
	jmp	.L510
	.cfi_endproc
.LFE3804:
	.size	_ZNK6icu_676number29LocalizedNumberRangeFormatter12getFormatterER10UErrorCode, .-_ZNK6icu_676number29LocalizedNumberRangeFormatter12getFormatterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatterD2Ev
	.type	_ZN6icu_676number29LocalizedNumberRangeFormatterD2Ev, @function
_ZN6icu_676number29LocalizedNumberRangeFormatterD2Ev:
.LFB3788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	xchgq	1144(%rdi), %r12
	testq	%r12, %r12
	jne	.L611
.L523:
	leaq	920(%rbx), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	672(%rbx), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	624(%rbx), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	592(%rbx), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	leaq	496(%rbx), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	472(%rbx), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	224(%rbx), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	176(%rbx), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	144(%rbx), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	leaq	48(%rbx), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	addq	$8, %rsp
	leaq	24(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711MeasureUnitD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_restore_state
	cmpb	$0, 1300(%r12)
	jne	.L612
.L524:
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	leaq	1192(%r12), %rdi
	movq	%rax, 1184(%r12)
	leaq	1184(%r12), %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	1112(%r12), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	1032(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	968(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 916(%r12)
	jne	.L613
.L525:
	leaq	872(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	856(%r12), %rdi
	testq	%rdi, %rdi
	je	.L526
	movq	(%rdi), %rax
	call	*8(%rax)
.L526:
	movq	848(%r12), %rdi
	testq	%rdi, %rdi
	je	.L527
	movq	(%rdi), %rax
	call	*8(%rax)
.L527:
	movq	840(%r12), %r13
	testq	%r13, %r13
	je	.L528
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L529
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L530
	movq	(%rdi), %rax
	call	*8(%rax)
.L530:
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L528:
	movq	832(%r12), %r13
	testq	%r13, %r13
	je	.L531
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L532
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r13), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r13)
	jne	.L614
.L533:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r13), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L531:
	movq	824(%r12), %rdi
	testq	%rdi, %rdi
	je	.L534
	movq	(%rdi), %rax
	call	*8(%rax)
.L534:
	movq	816(%r12), %r13
	testq	%r13, %r13
	je	.L535
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L536
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r13), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L535:
	movq	808(%r12), %rdi
	testq	%rdi, %rdi
	je	.L537
	movq	(%rdi), %rax
	call	*8(%rax)
.L537:
	movq	800(%r12), %rdi
	testq	%rdi, %rdi
	je	.L538
	movq	(%rdi), %rax
	call	*8(%rax)
.L538:
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %r15
	movq	%rax, 752(%r12)
	leaq	752(%r12), %r13
	leaq	760(%r12), %rdi
	movq	%r15, 560(%r12)
	leaq	560(%r12), %r14
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r13, %rdi
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %r13
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, 736(%r12)
	leaq	736(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, 720(%r12)
	leaq	720(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	696(%r12), %rdi
	movq	%rax, 696(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	leaq	488(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	424(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 372(%r12)
	jne	.L615
.L539:
	leaq	328(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	312(%r12), %rdi
	testq	%rdi, %rdi
	je	.L540
	movq	(%rdi), %rax
	call	*8(%rax)
.L540:
	movq	304(%r12), %rdi
	testq	%rdi, %rdi
	je	.L541
	movq	(%rdi), %rax
	call	*8(%rax)
.L541:
	movq	296(%r12), %r14
	testq	%r14, %r14
	je	.L542
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L543
	movq	8(%r14), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L544
	movq	(%rdi), %rax
	call	*8(%rax)
.L544:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L542:
	movq	288(%r12), %r14
	testq	%r14, %r14
	je	.L545
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L546
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r14), %rdi
	leaq	-104(%rax), %rsi
	movq	%rax, %xmm2
	addq	$88, %rax
	movq	%rsi, %xmm0
	movq	%rax, 16(%r14)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r14)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r14)
	jne	.L616
.L547:
	leaq	80(%r14), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r14), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L545:
	movq	280(%r12), %rdi
	testq	%rdi, %rdi
	je	.L548
	movq	(%rdi), %rax
	call	*8(%rax)
.L548:
	movq	272(%r12), %r14
	testq	%r14, %r14
	je	.L549
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L550
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r14), %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r14), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L549:
	movq	264(%r12), %rdi
	testq	%rdi, %rdi
	je	.L551
	movq	(%rdi), %rax
	call	*8(%rax)
.L551:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L552
	movq	(%rdi), %rax
	call	*8(%rax)
.L552:
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%r15, 16(%r12)
	leaq	216(%r12), %rdi
	movq	%rax, 208(%r12)
	leaq	208(%r12), %r14
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, 192(%r12)
	leaq	192(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, 176(%r12)
	leaq	176(%r12), %rdi
	leaq	16(%r12), %r13
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	152(%r12), %rdi
	movq	%rax, 152(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L612:
	movq	1288(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L615:
	movq	360(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L613:
	movq	904(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L616:
	movq	112(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L614:
	movq	112(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L529:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L550:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L543:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L536:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L532:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L546:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L545
	.cfi_endproc
.LFE3788:
	.size	_ZN6icu_676number29LocalizedNumberRangeFormatterD2Ev, .-_ZN6icu_676number29LocalizedNumberRangeFormatterD2Ev
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatterD1Ev
	.set	_ZN6icu_676number29LocalizedNumberRangeFormatterD1Ev,_ZN6icu_676number29LocalizedNumberRangeFormatterD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatteraSERKS1_
	.type	_ZN6icu_676number29LocalizedNumberRangeFormatteraSERKS1_, @function
_ZN6icu_676number29LocalizedNumberRangeFormatteraSERKS1_:
.LFB3783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	addq	$8, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$8, %rsi
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r13), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	leaq	920(%r13), %rdi
	movb	%al, 904(%r13)
	movl	908(%rbx), %eax
	movl	%eax, 908(%r13)
	movl	912(%rbx), %eax
	movl	%eax, 912(%r13)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	xchgq	1144(%r13), %r12
	testq	%r12, %r12
	jne	.L706
.L618:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L706:
	.cfi_restore_state
	cmpb	$0, 1300(%r12)
	jne	.L707
.L619:
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	leaq	1192(%r12), %rdi
	movq	%rax, 1184(%r12)
	leaq	1184(%r12), %r14
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	1112(%r12), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	1032(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	968(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 916(%r12)
	jne	.L708
.L620:
	leaq	872(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	856(%r12), %rdi
	testq	%rdi, %rdi
	je	.L621
	movq	(%rdi), %rax
	call	*8(%rax)
.L621:
	movq	848(%r12), %rdi
	testq	%rdi, %rdi
	je	.L622
	movq	(%rdi), %rax
	call	*8(%rax)
.L622:
	movq	840(%r12), %r14
	testq	%r14, %r14
	je	.L623
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L624
	movq	8(%r14), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L625
	movq	(%rdi), %rax
	call	*8(%rax)
.L625:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L623:
	movq	832(%r12), %r14
	testq	%r14, %r14
	je	.L626
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L627
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r14), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r14)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r14)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r14)
	jne	.L709
.L628:
	leaq	80(%r14), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r14), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L626:
	movq	824(%r12), %rdi
	testq	%rdi, %rdi
	je	.L629
	movq	(%rdi), %rax
	call	*8(%rax)
.L629:
	movq	816(%r12), %r14
	testq	%r14, %r14
	je	.L630
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L631
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r14), %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r14), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L630:
	movq	808(%r12), %rdi
	testq	%rdi, %rdi
	je	.L632
	movq	(%rdi), %rax
	call	*8(%rax)
.L632:
	movq	800(%r12), %rdi
	testq	%rdi, %rdi
	je	.L633
	movq	(%rdi), %rax
	call	*8(%rax)
.L633:
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %r15
	movq	%rax, 752(%r12)
	leaq	752(%r12), %r14
	leaq	760(%r12), %rdi
	movq	%r15, 560(%r12)
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r14, %rdi
	leaq	560(%r12), %r14
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%rbx, 736(%r12)
	leaq	736(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 720(%r12)
	leaq	720(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	696(%r12), %rdi
	movq	%rax, 696(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	leaq	488(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	424(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 372(%r12)
	jne	.L710
.L634:
	leaq	328(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	312(%r12), %rdi
	testq	%rdi, %rdi
	je	.L635
	movq	(%rdi), %rax
	call	*8(%rax)
.L635:
	movq	304(%r12), %rdi
	testq	%rdi, %rdi
	je	.L636
	movq	(%rdi), %rax
	call	*8(%rax)
.L636:
	movq	296(%r12), %r14
	testq	%r14, %r14
	je	.L637
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L638
	movq	8(%r14), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L639
	movq	(%rdi), %rax
	call	*8(%rax)
.L639:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L637:
	movq	288(%r12), %r14
	testq	%r14, %r14
	je	.L640
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L641
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r14), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm2
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r14)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r14)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r14)
	jne	.L711
.L642:
	leaq	80(%r14), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r14), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L640:
	movq	280(%r12), %rdi
	testq	%rdi, %rdi
	je	.L643
	movq	(%rdi), %rax
	call	*8(%rax)
.L643:
	movq	272(%r12), %r14
	testq	%r14, %r14
	je	.L644
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L645
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r14), %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r14), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L644:
	movq	264(%r12), %rdi
	testq	%rdi, %rdi
	je	.L646
	movq	(%rdi), %rax
	call	*8(%rax)
.L646:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L647
	movq	(%rdi), %rax
	call	*8(%rax)
.L647:
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%r15, 16(%r12)
	leaq	208(%r12), %r14
	movq	%rax, 208(%r12)
	leaq	216(%r12), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r14, %rdi
	leaq	16(%r12), %r14
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%rbx, 192(%r12)
	leaq	192(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 176(%r12)
	leaq	176(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	152(%r12), %rdi
	movq	%rax, 152(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L707:
	movq	1288(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L710:
	movq	360(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L708:
	movq	904(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L711:
	movq	112(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L709:
	movq	112(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L624:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L645:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L638:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L631:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L627:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L641:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L640
	.cfi_endproc
.LFE3783:
	.size	_ZN6icu_676number29LocalizedNumberRangeFormatteraSERKS1_, .-_ZN6icu_676number29LocalizedNumberRangeFormatteraSERKS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatteraSEOS1_
	.type	_ZN6icu_676number29LocalizedNumberRangeFormatteraSEOS1_, @function
_ZN6icu_676number29LocalizedNumberRangeFormatteraSEOS1_:
.LFB3785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	addq	$8, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$8, %rsi
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	456(%rbx), %rsi
	leaq	456(%r13), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	movzbl	904(%rbx), %eax
	leaq	920(%rbx), %rsi
	leaq	920(%r13), %rdi
	movb	%al, 904(%r13)
	movl	908(%rbx), %eax
	movl	%eax, 908(%r13)
	movl	912(%rbx), %eax
	movl	%eax, 912(%r13)
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	xchgq	1144(%rbx), %r12
	xchgq	1144(%r13), %r12
	testq	%r12, %r12
	jne	.L801
.L713:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	.cfi_restore_state
	cmpb	$0, 1300(%r12)
	jne	.L802
.L714:
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	leaq	1192(%r12), %rdi
	movq	%rax, 1184(%r12)
	leaq	1184(%r12), %r14
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	1112(%r12), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	1032(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	968(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 916(%r12)
	jne	.L803
.L715:
	leaq	872(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	856(%r12), %rdi
	testq	%rdi, %rdi
	je	.L716
	movq	(%rdi), %rax
	call	*8(%rax)
.L716:
	movq	848(%r12), %rdi
	testq	%rdi, %rdi
	je	.L717
	movq	(%rdi), %rax
	call	*8(%rax)
.L717:
	movq	840(%r12), %r14
	testq	%r14, %r14
	je	.L718
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L719
	movq	8(%r14), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L720
	movq	(%rdi), %rax
	call	*8(%rax)
.L720:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L718:
	movq	832(%r12), %r14
	testq	%r14, %r14
	je	.L721
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L722
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r14), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r14)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r14)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r14)
	jne	.L804
.L723:
	leaq	80(%r14), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r14), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L721:
	movq	824(%r12), %rdi
	testq	%rdi, %rdi
	je	.L724
	movq	(%rdi), %rax
	call	*8(%rax)
.L724:
	movq	816(%r12), %r14
	testq	%r14, %r14
	je	.L725
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L726
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r14), %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r14), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L725:
	movq	808(%r12), %rdi
	testq	%rdi, %rdi
	je	.L727
	movq	(%rdi), %rax
	call	*8(%rax)
.L727:
	movq	800(%r12), %rdi
	testq	%rdi, %rdi
	je	.L728
	movq	(%rdi), %rax
	call	*8(%rax)
.L728:
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %r15
	movq	%rax, 752(%r12)
	leaq	752(%r12), %r14
	leaq	760(%r12), %rdi
	movq	%r15, 560(%r12)
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r14, %rdi
	leaq	560(%r12), %r14
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%rbx, 736(%r12)
	leaq	736(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 720(%r12)
	leaq	720(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	696(%r12), %rdi
	movq	%rax, 696(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	leaq	488(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	424(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 372(%r12)
	jne	.L805
.L729:
	leaq	328(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	312(%r12), %rdi
	testq	%rdi, %rdi
	je	.L730
	movq	(%rdi), %rax
	call	*8(%rax)
.L730:
	movq	304(%r12), %rdi
	testq	%rdi, %rdi
	je	.L731
	movq	(%rdi), %rax
	call	*8(%rax)
.L731:
	movq	296(%r12), %r14
	testq	%r14, %r14
	je	.L732
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L733
	movq	8(%r14), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L734
	movq	(%rdi), %rax
	call	*8(%rax)
.L734:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L732:
	movq	288(%r12), %r14
	testq	%r14, %r14
	je	.L735
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L736
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r14), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm2
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r14)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r14)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r14)
	jne	.L806
.L737:
	leaq	80(%r14), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r14), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L735:
	movq	280(%r12), %rdi
	testq	%rdi, %rdi
	je	.L738
	movq	(%rdi), %rax
	call	*8(%rax)
.L738:
	movq	272(%r12), %r14
	testq	%r14, %r14
	je	.L739
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L740
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r14), %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r14), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L739:
	movq	264(%r12), %rdi
	testq	%rdi, %rdi
	je	.L741
	movq	(%rdi), %rax
	call	*8(%rax)
.L741:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L742
	movq	(%rdi), %rax
	call	*8(%rax)
.L742:
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%r15, 16(%r12)
	leaq	208(%r12), %r14
	movq	%rax, 208(%r12)
	leaq	216(%r12), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r14, %rdi
	leaq	16(%r12), %r14
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%rbx, 192(%r12)
	leaq	192(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 176(%r12)
	leaq	176(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	152(%r12), %rdi
	movq	%rax, 152(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L802:
	movq	1288(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L805:
	movq	360(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L803:
	movq	904(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L806:
	movq	112(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L804:
	movq	112(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L719:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L740:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L733:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L726:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L722:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L736:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L735
	.cfi_endproc
.LFE3785:
	.size	_ZN6icu_676number29LocalizedNumberRangeFormatteraSEOS1_, .-_ZN6icu_676number29LocalizedNumberRangeFormatteraSEOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatterC2EONS0_28NumberRangeFormatterSettingsIS1_EE
	.type	_ZN6icu_676number29LocalizedNumberRangeFormatterC2EONS0_28NumberRangeFormatterSettingsIS1_EE, @function
_ZN6icu_676number29LocalizedNumberRangeFormatterC2EONS0_28NumberRangeFormatterSettingsIS1_EE:
.LFB3781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	addq	$8, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$8, %rsp
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	leaq	456(%r13), %rsi
	leaq	456(%rbx), %rdi
	call	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_@PLT
	movzbl	904(%r13), %eax
	leaq	920(%r13), %rsi
	leaq	920(%rbx), %rdi
	movb	%al, 904(%rbx)
	movq	908(%r13), %rax
	movq	%rax, 908(%rbx)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	$0, 1144(%rbx)
	xchgq	1144(%r13), %r12
	xchgq	1144(%rbx), %r12
	testq	%r12, %r12
	jne	.L893
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L893:
	.cfi_restore_state
	cmpb	$0, 1300(%r12)
	jne	.L894
.L809:
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	leaq	1192(%r12), %rdi
	movq	%rax, 1184(%r12)
	leaq	1184(%r12), %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	1112(%r12), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	leaq	1032(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	968(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 916(%r12)
	jne	.L895
.L810:
	leaq	872(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	856(%r12), %rdi
	testq	%rdi, %rdi
	je	.L811
	movq	(%rdi), %rax
	call	*8(%rax)
.L811:
	movq	848(%r12), %rdi
	testq	%rdi, %rdi
	je	.L812
	movq	(%rdi), %rax
	call	*8(%rax)
.L812:
	movq	840(%r12), %r13
	testq	%r13, %r13
	je	.L813
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L814
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L815
	movq	(%rdi), %rax
	call	*8(%rax)
.L815:
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L813:
	movq	832(%r12), %r13
	testq	%r13, %r13
	je	.L816
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L817
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r13), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r13)
	jne	.L896
.L818:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r13), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L816:
	movq	824(%r12), %rdi
	testq	%rdi, %rdi
	je	.L819
	movq	(%rdi), %rax
	call	*8(%rax)
.L819:
	movq	816(%r12), %r13
	testq	%r13, %r13
	je	.L820
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L821
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r13), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L820:
	movq	808(%r12), %rdi
	testq	%rdi, %rdi
	je	.L822
	movq	(%rdi), %rax
	call	*8(%rax)
.L822:
	movq	800(%r12), %rdi
	testq	%rdi, %rdi
	je	.L823
	movq	(%rdi), %rax
	call	*8(%rax)
.L823:
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %r15
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %r14
	movq	%r15, 560(%r12)
	leaq	752(%r12), %r13
	leaq	760(%r12), %rdi
	movq	%r14, 752(%r12)
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r13, %rdi
	leaq	560(%r12), %r13
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%rbx, 736(%r12)
	leaq	736(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 720(%r12)
	leaq	720(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	696(%r12), %rdi
	movq	%rax, 696(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	leaq	488(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	424(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 372(%r12)
	jne	.L897
.L824:
	leaq	328(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	312(%r12), %rdi
	testq	%rdi, %rdi
	je	.L825
	movq	(%rdi), %rax
	call	*8(%rax)
.L825:
	movq	304(%r12), %rdi
	testq	%rdi, %rdi
	je	.L826
	movq	(%rdi), %rax
	call	*8(%rax)
.L826:
	movq	296(%r12), %r13
	testq	%r13, %r13
	je	.L827
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L828
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L829
	movq	(%rdi), %rax
	call	*8(%rax)
.L829:
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L827:
	movq	288(%r12), %r13
	testq	%r13, %r13
	je	.L830
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L831
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r13), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm2
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 0(%r13)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r13)
	jne	.L898
.L832:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r13), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L830:
	movq	280(%r12), %rdi
	testq	%rdi, %rdi
	je	.L833
	movq	(%rdi), %rax
	call	*8(%rax)
.L833:
	movq	272(%r12), %r13
	testq	%r13, %r13
	je	.L834
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L835
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r13), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L834:
	movq	264(%r12), %rdi
	testq	%rdi, %rdi
	je	.L836
	movq	(%rdi), %rax
	call	*8(%rax)
.L836:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L837
	movq	(%rdi), %rax
	call	*8(%rax)
.L837:
	movq	%r15, 16(%r12)
	leaq	216(%r12), %rdi
	leaq	16(%r12), %r13
	movq	%r14, 208(%r12)
	leaq	208(%r12), %r14
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%rbx, 192(%r12)
	leaq	192(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 176(%r12)
	leaq	176(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	152(%r12), %rdi
	movq	%rax, 152(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L894:
	.cfi_restore_state
	movq	1288(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L897:
	movq	360(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L895:
	movq	904(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L898:
	movq	112(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L896:
	movq	112(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L814:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L835:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L828:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L821:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L817:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L831:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L830
	.cfi_endproc
.LFE3781:
	.size	_ZN6icu_676number29LocalizedNumberRangeFormatterC2EONS0_28NumberRangeFormatterSettingsIS1_EE, .-_ZN6icu_676number29LocalizedNumberRangeFormatterC2EONS0_28NumberRangeFormatterSettingsIS1_EE
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_28NumberRangeFormatterSettingsIS1_EE
	.set	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_28NumberRangeFormatterSettingsIS1_EE,_ZN6icu_676number29LocalizedNumberRangeFormatterC2EONS0_28NumberRangeFormatterSettingsIS1_EE
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE:
.LFB4369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_28NumberRangeFormatterSettingsIS1_EE
	leaq	8(%r12), %rdi
	movq	%r13, %rsi
	leaq	920(%r12), %r13
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$1, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4369:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothERKNS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE:
.LFB4371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_28NumberRangeFormatterSettingsIS1_EE
	leaq	8(%r12), %rdi
	movq	%r13, %rsi
	leaq	920(%r12), %r13
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$1, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4371:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE19numberFormatterBothEONS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE:
.LFB4373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_28NumberRangeFormatterSettingsIS1_EE
	leaq	8(%r12), %rdi
	movq	%r13, %rsi
	leaq	920(%r12), %r13
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4373:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstERKNS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE:
.LFB4375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_28NumberRangeFormatterSettingsIS1_EE
	leaq	8(%r12), %rdi
	movq	%r13, %rsi
	leaq	920(%r12), %r13
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4375:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE20numberFormatterFirstEONS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE:
.LFB4377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_28NumberRangeFormatterSettingsIS1_EE
	leaq	456(%r12), %rdi
	movq	%r13, %rsi
	leaq	920(%r12), %r13
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4377:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondERKNS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE:
.LFB4379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_28NumberRangeFormatterSettingsIS1_EE
	leaq	456(%r12), %rdi
	movq	%r13, %rsi
	leaq	920(%r12), %r13
	call	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_@PLT
	leaq	224(%r12), %rdi
	movq	%r13, %rsi
	movb	$0, 904(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	leaq	672(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4379:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE21numberFormatterSecondEONS0_26UnlocalizedNumberFormatterE
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse:
.LFB4381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	call	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_28NumberRangeFormatterSettingsIS1_EE
	movl	%ebx, 908(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4381:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE8collapseE20UNumberRangeCollapse
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback:
.LFB4383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	call	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_28NumberRangeFormatterSettingsIS1_EE
	movl	%ebx, 912(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4383:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE16identityFallbackE28UNumberRangeIdentityFallback
	.section	.text._ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE5cloneEv,"axG",@progbits,_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE5cloneEv
	.type	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE5cloneEv, @function
_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE5cloneEv:
.LFB4387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$1152, %edi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L916
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_28NumberRangeFormatterSettingsIS1_EE
.L916:
	movq	%rbx, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4387:
	.size	_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE5cloneEv, .-_ZNO6icu_676number28NumberRangeFormatterSettingsINS0_29LocalizedNumberRangeFormatterEE5cloneEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatterC2EOS1_
	.type	_ZN6icu_676number29LocalizedNumberRangeFormatterC2EOS1_, @function
_ZN6icu_676number29LocalizedNumberRangeFormatterC2EOS1_:
.LFB3759:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EONS0_28NumberRangeFormatterSettingsIS1_EE
	.cfi_endproc
.LFE3759:
	.size	_ZN6icu_676number29LocalizedNumberRangeFormatterC2EOS1_, .-_ZN6icu_676number29LocalizedNumberRangeFormatterC2EOS1_
	.globl	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EOS1_
	.set	_ZN6icu_676number29LocalizedNumberRangeFormatterC1EOS1_,_ZN6icu_676number29LocalizedNumberRangeFormatterC2EOS1_
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_676number4impl10MicroPropsE
	.section	.rodata._ZTSN6icu_676number4impl10MicroPropsE,"aG",@progbits,_ZTSN6icu_676number4impl10MicroPropsE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl10MicroPropsE, @object
	.size	_ZTSN6icu_676number4impl10MicroPropsE, 34
_ZTSN6icu_676number4impl10MicroPropsE:
	.string	"N6icu_676number4impl10MicroPropsE"
	.weak	_ZTSN6icu_676number20FormattedNumberRangeE
	.section	.rodata._ZTSN6icu_676number20FormattedNumberRangeE,"aG",@progbits,_ZTSN6icu_676number20FormattedNumberRangeE,comdat
	.align 32
	.type	_ZTSN6icu_676number20FormattedNumberRangeE, @object
	.size	_ZTSN6icu_676number20FormattedNumberRangeE, 39
_ZTSN6icu_676number20FormattedNumberRangeE:
	.string	"N6icu_676number20FormattedNumberRangeE"
	.weak	_ZTIN6icu_676number20FormattedNumberRangeE
	.section	.data.rel.ro._ZTIN6icu_676number20FormattedNumberRangeE,"awG",@progbits,_ZTIN6icu_676number20FormattedNumberRangeE,comdat
	.align 8
	.type	_ZTIN6icu_676number20FormattedNumberRangeE, @object
	.size	_ZTIN6icu_676number20FormattedNumberRangeE, 56
_ZTIN6icu_676number20FormattedNumberRangeE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number20FormattedNumberRangeE
	.long	0
	.long	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.quad	_ZTIN6icu_6714FormattedValueE
	.quad	2
	.weak	_ZTSN6icu_676number4impl13EmptyModifierE
	.section	.rodata._ZTSN6icu_676number4impl13EmptyModifierE,"aG",@progbits,_ZTSN6icu_676number4impl13EmptyModifierE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl13EmptyModifierE, @object
	.size	_ZTSN6icu_676number4impl13EmptyModifierE, 37
_ZTSN6icu_676number4impl13EmptyModifierE:
	.string	"N6icu_676number4impl13EmptyModifierE"
	.weak	_ZTIN6icu_676number4impl13EmptyModifierE
	.section	.data.rel.ro._ZTIN6icu_676number4impl13EmptyModifierE,"awG",@progbits,_ZTIN6icu_676number4impl13EmptyModifierE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl13EmptyModifierE, @object
	.size	_ZTIN6icu_676number4impl13EmptyModifierE, 56
_ZTIN6icu_676number4impl13EmptyModifierE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl13EmptyModifierE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl8ModifierE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTIN6icu_676number4impl10MicroPropsE
	.section	.data.rel.ro._ZTIN6icu_676number4impl10MicroPropsE,"awG",@progbits,_ZTIN6icu_676number4impl10MicroPropsE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl10MicroPropsE, @object
	.size	_ZTIN6icu_676number4impl10MicroPropsE, 24
_ZTIN6icu_676number4impl10MicroPropsE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl10MicroPropsE
	.quad	_ZTIN6icu_676number4impl19MicroPropsGeneratorE
	.weak	_ZTSN6icu_676number4impl25UFormattedNumberRangeDataE
	.section	.rodata._ZTSN6icu_676number4impl25UFormattedNumberRangeDataE,"aG",@progbits,_ZTSN6icu_676number4impl25UFormattedNumberRangeDataE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl25UFormattedNumberRangeDataE, @object
	.size	_ZTSN6icu_676number4impl25UFormattedNumberRangeDataE, 49
_ZTSN6icu_676number4impl25UFormattedNumberRangeDataE:
	.string	"N6icu_676number4impl25UFormattedNumberRangeDataE"
	.weak	_ZTIN6icu_676number4impl25UFormattedNumberRangeDataE
	.section	.data.rel.ro._ZTIN6icu_676number4impl25UFormattedNumberRangeDataE,"awG",@progbits,_ZTIN6icu_676number4impl25UFormattedNumberRangeDataE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl25UFormattedNumberRangeDataE, @object
	.size	_ZTIN6icu_676number4impl25UFormattedNumberRangeDataE, 24
_ZTIN6icu_676number4impl25UFormattedNumberRangeDataE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl25UFormattedNumberRangeDataE
	.quad	_ZTIN6icu_6731FormattedValueStringBuilderImplE
	.weak	_ZTVN6icu_676number4impl13EmptyModifierE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl13EmptyModifierE,"awG",@progbits,_ZTVN6icu_676number4impl13EmptyModifierE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl13EmptyModifierE, @object
	.size	_ZTVN6icu_676number4impl13EmptyModifierE, 88
_ZTVN6icu_676number4impl13EmptyModifierE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl13EmptyModifierE
	.quad	_ZN6icu_676number4impl13EmptyModifierD1Ev
	.quad	_ZN6icu_676number4impl13EmptyModifierD0Ev
	.quad	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.quad	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv
	.quad	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv
	.quad	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv
	.quad	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.quad	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE
	.quad	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE
	.weak	_ZTVN6icu_676number4impl10MicroPropsE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl10MicroPropsE,"awG",@progbits,_ZTVN6icu_676number4impl10MicroPropsE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl10MicroPropsE, @object
	.size	_ZTVN6icu_676number4impl10MicroPropsE, 40
_ZTVN6icu_676number4impl10MicroPropsE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl10MicroPropsE
	.quad	_ZN6icu_676number4impl10MicroPropsD1Ev
	.quad	_ZN6icu_676number4impl10MicroPropsD0Ev
	.quad	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode
	.weak	_ZTVN6icu_676number20FormattedNumberRangeE
	.section	.data.rel.ro.local._ZTVN6icu_676number20FormattedNumberRangeE,"awG",@progbits,_ZTVN6icu_676number20FormattedNumberRangeE,comdat
	.align 8
	.type	_ZTVN6icu_676number20FormattedNumberRangeE, @object
	.size	_ZTVN6icu_676number20FormattedNumberRangeE, 64
_ZTVN6icu_676number20FormattedNumberRangeE:
	.quad	0
	.quad	_ZTIN6icu_676number20FormattedNumberRangeE
	.quad	_ZN6icu_676number20FormattedNumberRangeD1Ev
	.quad	_ZN6icu_676number20FormattedNumberRangeD0Ev
	.quad	_ZNK6icu_676number20FormattedNumberRange8toStringER10UErrorCode
	.quad	_ZNK6icu_676number20FormattedNumberRange12toTempStringER10UErrorCode
	.quad	_ZNK6icu_676number20FormattedNumberRange8appendToERNS_10AppendableER10UErrorCode
	.quad	_ZNK6icu_676number20FormattedNumberRange12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.weak	_ZTVN6icu_676number4impl25UFormattedNumberRangeDataE
	.section	.data.rel.ro._ZTVN6icu_676number4impl25UFormattedNumberRangeDataE,"awG",@progbits,_ZTVN6icu_676number4impl25UFormattedNumberRangeDataE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl25UFormattedNumberRangeDataE, @object
	.size	_ZTVN6icu_676number4impl25UFormattedNumberRangeDataE, 64
_ZTVN6icu_676number4impl25UFormattedNumberRangeDataE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl25UFormattedNumberRangeDataE
	.quad	_ZN6icu_676number4impl25UFormattedNumberRangeDataD1Ev
	.quad	_ZN6icu_676number4impl25UFormattedNumberRangeDataD0Ev
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl8toStringER10UErrorCode
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl12toTempStringER10UErrorCode
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.section	.rodata
	.type	_ZN6icu_67L15kUndefinedFieldE, @object
	.size	_ZN6icu_67L15kUndefinedFieldE, 1
_ZN6icu_67L15kUndefinedFieldE:
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
