	.file	"indiancal.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"indian"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714IndianCalendar7getTypeEv
	.type	_ZNK6icu_6714IndianCalendar7getTypeEv, @function
_ZNK6icu_6714IndianCalendar7getTypeEv:
.LFB3116:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE3116:
	.size	_ZNK6icu_6714IndianCalendar7getTypeEv, .-_ZNK6icu_6714IndianCalendar7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714IndianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.type	_ZNK6icu_6714IndianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, @function
_ZNK6icu_6714IndianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE:
.LFB3117:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	leaq	_ZN6icu_67L6LIMITSE(%rip), %rax
	leaq	(%rdx,%rsi,4), %rdx
	movl	(%rax,%rdx,4), %eax
	ret
	.cfi_endproc
.LFE3117:
	.size	_ZNK6icu_6714IndianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, .-_ZNK6icu_6714IndianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714IndianCalendar19handleGetYearLengthEi
	.type	_ZNK6icu_6714IndianCalendar19handleGetYearLengthEi, @function
_ZNK6icu_6714IndianCalendar19handleGetYearLengthEi:
.LFB3120:
	.cfi_startproc
	endbr64
	addl	$78, %esi
	movl	$365, %eax
	testb	$3, %sil
	jne	.L4
	imull	$-1030792151, %esi, %esi
	movl	$366, %eax
	addl	$85899344, %esi
	movl	%esi, %edx
	rorl	$2, %edx
	cmpl	$42949672, %edx
	jbe	.L9
.L4:
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	rorl	$4, %esi
	xorl	%eax, %eax
	cmpl	$10737419, %esi
	setb	%al
	addl	$365, %eax
	ret
	.cfi_endproc
.LFE3120:
	.size	_ZNK6icu_6714IndianCalendar19handleGetYearLengthEi, .-_ZNK6icu_6714IndianCalendar19handleGetYearLengthEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714IndianCalendar18haveDefaultCenturyEv
	.type	_ZNK6icu_6714IndianCalendar18haveDefaultCenturyEv, @function
_ZNK6icu_6714IndianCalendar18haveDefaultCenturyEv:
.LFB3128:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3128:
	.size	_ZNK6icu_6714IndianCalendar18haveDefaultCenturyEv, .-_ZNK6icu_6714IndianCalendar18haveDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714IndianCalendar17getDynamicClassIDEv
	.type	_ZNK6icu_6714IndianCalendar17getDynamicClassIDEv, @function
_ZNK6icu_6714IndianCalendar17getDynamicClassIDEv:
.LFB3133:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714IndianCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3133:
	.size	_ZNK6icu_6714IndianCalendar17getDynamicClassIDEv, .-_ZNK6icu_6714IndianCalendar17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714IndianCalendar5cloneEv
	.type	_ZNK6icu_6714IndianCalendar5cloneEv, @function
_ZNK6icu_6714IndianCalendar5cloneEv:
.LFB3105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$616, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L12
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_678CalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714IndianCalendarE(%rip), %rax
	movq	%rax, (%r12)
.L12:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3105:
	.size	_ZNK6icu_6714IndianCalendar5cloneEv, .-_ZNK6icu_6714IndianCalendar5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714IndianCalendarD2Ev
	.type	_ZN6icu_6714IndianCalendarD2Ev, @function
_ZN6icu_6714IndianCalendarD2Ev:
.LFB3113:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714IndianCalendarE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678CalendarD2Ev@PLT
	.cfi_endproc
.LFE3113:
	.size	_ZN6icu_6714IndianCalendarD2Ev, .-_ZN6icu_6714IndianCalendarD2Ev
	.globl	_ZN6icu_6714IndianCalendarD1Ev
	.set	_ZN6icu_6714IndianCalendarD1Ev,_ZN6icu_6714IndianCalendarD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714IndianCalendarD0Ev
	.type	_ZN6icu_6714IndianCalendarD0Ev, @function
_ZN6icu_6714IndianCalendarD0Ev:
.LFB3115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714IndianCalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678CalendarD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3115:
	.size	_ZN6icu_6714IndianCalendarD0Ev, .-_ZN6icu_6714IndianCalendarD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714IndianCalendar20handleGetMonthLengthEii
	.type	_ZNK6icu_6714IndianCalendar20handleGetMonthLengthEii, @function
_ZNK6icu_6714IndianCalendar20handleGetMonthLengthEii:
.LFB3119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$24, %rsp
	movl	%edx, -20(%rbp)
	cmpl	$11, %edx
	ja	.L30
.L22:
	addl	$78, %ebx
	testb	$3, %bl
	jne	.L23
	imull	$-1030792151, %ebx, %ebx
	addl	$85899344, %ebx
	movl	%ebx, %edx
	rorl	$2, %edx
	cmpl	$42949672, %edx
	jbe	.L31
	movl	$31, %r8d
	testl	%eax, %eax
	je	.L21
	.p2align 4,,10
	.p2align 3
.L23:
	subl	$1, %eax
	xorl	%r8d, %r8d
	cmpl	$5, %eax
	setb	%r8b
	addl	$30, %r8d
.L21:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	rorl	$4, %ebx
	cmpl	$10737418, %ebx
	ja	.L23
	movl	$31, %r8d
	testl	%eax, %eax
	jne	.L23
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L30:
	pxor	%xmm0, %xmm0
	leaq	-20(%rbp), %rsi
	movl	$12, %edi
	cvtsi2sdl	%edx, %xmm0
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	addl	%eax, %ebx
	movl	-20(%rbp), %eax
	jmp	.L22
	.cfi_endproc
.LFE3119:
	.size	_ZNK6icu_6714IndianCalendar20handleGetMonthLengthEii, .-_ZNK6icu_6714IndianCalendar20handleGetMonthLengthEii
	.p2align 4
	.type	_ZN6icu_67L13gregorianToJDEiii, @function
_ZN6icu_67L13gregorianToJDEiii:
.LFB3121:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	2(%rdi), %eax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	subl	$1, %r15d
	cmovns	%r15d, %eax
	sarl	$2, %eax
	cvtsi2sdl	%eax, %xmm0
	call	uprv_floor_67@PLT
	imull	$365, %r15d, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movslq	%r15d, %rax
	addsd	.LC1(%rip), %xmm1
	sarl	$31, %r15d
	imulq	$1374389535, %rax, %rax
	addsd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	sarq	$37, %rax
	subl	%r15d, %eax
	shrq	$32, %r12
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm1, -56(%rbp)
	sarl	$7, %r12d
	subl	%r15d, %r12d
	call	uprv_floor_67@PLT
	movsd	-56(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	movsd	%xmm1, -56(%rbp)
	call	uprv_floor_67@PLT
	imull	$367, %r14d, %eax
	addsd	-56(%rbp), %xmm0
	subl	$362, %eax
	movsd	%xmm0, -56(%rbp)
	movslq	%eax, %rdx
	sarl	$31, %eax
	imulq	$715827883, %rdx, %rdx
	sarq	$33, %rdx
	subl	%eax, %edx
	cmpl	$2, %r14d
	jle	.L33
	testb	$3, %r13b
	jne	.L38
	imull	$-1030792151, %r13d, %r13d
	addl	$85899344, %r13d
	movl	%r13d, %eax
	rorl	$2, %eax
	cmpl	$42949672, %eax
	jbe	.L35
	subl	$1, %edx
.L33:
	addl	%ebx, %edx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	call	uprv_floor_67@PLT
	addsd	-56(%rbp), %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	subl	$2, %edx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L35:
	rorl	$4, %r13d
	cmpl	$10737419, %r13d
	adcl	$-2, %edx
	jmp	.L33
	.cfi_endproc
.LFE3121:
	.size	_ZN6icu_67L13gregorianToJDEiii, .-_ZN6icu_67L13gregorianToJDEiii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714IndianCalendar14inDaylightTimeER10UErrorCode
	.type	_ZNK6icu_6714IndianCalendar14inDaylightTimeER10UErrorCode, @function
_ZNK6icu_6714IndianCalendar14inDaylightTimeER10UErrorCode:
.LFB3127:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.L40
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK6icu_678Calendar11getTimeZoneEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*72(%rax)
	testb	%al, %al
	jne	.L51
	xorl	%eax, %eax
.L39:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode@PLT
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L39
	movl	76(%r12), %eax
	testl	%eax, %eax
	setne	%al
	jmp	.L39
	.cfi_endproc
.LFE3127:
	.size	_ZNK6icu_6714IndianCalendar14inDaylightTimeER10UErrorCode, .-_ZNK6icu_6714IndianCalendar14inDaylightTimeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714IndianCalendar21handleGetExtendedYearEv
	.type	_ZN6icu_6714IndianCalendar21handleGetExtendedYearEv, @function
_ZN6icu_6714IndianCalendar21handleGetExtendedYearEv:
.LFB3125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movl	$19, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_@PLT
	cmpl	$19, %eax
	movl	$1, %eax
	je	.L58
	movl	132(%rbx), %edx
	testl	%edx, %edx
	jle	.L52
	movl	16(%rbx), %eax
.L52:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movl	204(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L52
	movl	88(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3125:
	.size	_ZN6icu_6714IndianCalendar21handleGetExtendedYearEv, .-_ZN6icu_6714IndianCalendar21handleGetExtendedYearEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714IndianCalendar23handleComputeMonthStartEiia
	.type	_ZNK6icu_6714IndianCalendar23handleComputeMonthStartEiia, @function
_ZNK6icu_6714IndianCalendar23handleComputeMonthStartEiia:
.LFB3124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$16, %rsp
	movl	%edx, -20(%rbp)
	cmpl	$11, %edx
	ja	.L70
.L60:
	leal	1(%rax), %r12d
.L61:
	leal	78(%rbx), %edi
	testb	$3, %dil
	jne	.L62
	imull	$-1030792151, %edi, %eax
	addl	$85899344, %eax
	movl	%eax, %edx
	rorl	$2, %edx
	cmpl	$42949672, %edx
	jbe	.L71
.L63:
	movl	$21, %edx
	movl	$3, %esi
	call	_ZN6icu_67L13gregorianToJDEiii
	movsd	.LC3(%rip), %xmm1
	cmpl	$1, %r12d
	jne	.L65
.L67:
	addsd	.LC4(%rip), %xmm0
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cvttsd2sil	%xmm0, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	rorl	$4, %eax
	cmpl	$10737418, %eax
	jbe	.L63
.L62:
	movl	$22, %edx
	movl	$3, %esi
	call	_ZN6icu_67L13gregorianToJDEiii
	movsd	.LC2(%rip), %xmm1
	cmpl	$1, %r12d
	je	.L67
.L65:
	leal	-2(%r12), %edx
	movl	$5, %eax
	addsd	%xmm0, %xmm1
	cmpl	$5, %edx
	pxor	%xmm0, %xmm0
	cmovg	%eax, %edx
	movl	%edx, %eax
	sall	$5, %eax
	subl	%edx, %eax
	cvtsi2sdl	%eax, %xmm0
	addsd	%xmm1, %xmm0
	cmpl	$7, %r12d
	jle	.L67
	leal	-7(%r12), %eax
	pxor	%xmm1, %xmm1
	imull	$30, %eax, %eax
	cvtsi2sdl	%eax, %xmm1
	addsd	%xmm1, %xmm0
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L70:
	pxor	%xmm0, %xmm0
	leaq	-20(%rbp), %rsi
	movl	$12, %edi
	cvtsi2sdl	%edx, %xmm0
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	addl	%eax, %ebx
	movl	-20(%rbp), %eax
	cmpl	$12, %eax
	jne	.L60
	movl	$1, %r12d
	jmp	.L61
	.cfi_endproc
.LFE3124:
	.size	_ZNK6icu_6714IndianCalendar23handleComputeMonthStartEiia, .-_ZNK6icu_6714IndianCalendar23handleComputeMonthStartEiia
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714IndianCalendar19handleComputeFieldsEiR10UErrorCode
	.type	_ZN6icu_6714IndianCalendar19handleComputeFieldsEiR10UErrorCode, @function
_ZN6icu_6714IndianCalendar19handleComputeFieldsEiR10UErrorCode:
.LFB3126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm5, %xmm5
	cvtsi2sdl	%esi, %xmm5
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	movapd	%xmm5, %xmm0
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	subsd	.LC7(%rip), %xmm0
	movsd	%xmm5, -72(%rbp)
	call	uprv_floor_67@PLT
	addsd	.LC7(%rip), %xmm0
	movapd	%xmm0, %xmm2
	subsd	.LC8(%rip), %xmm2
	movsd	%xmm0, -56(%rbp)
	movsd	%xmm2, -64(%rbp)
	movapd	%xmm2, %xmm0
	divsd	.LC9(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-64(%rbp), %xmm2
	movsd	%xmm0, -96(%rbp)
	movapd	%xmm2, %xmm0
	call	uprv_floor_67@PLT
	pxor	%xmm2, %xmm2
	cvttsd2sil	%xmm0, %eax
	movslq	%eax, %rdx
	movl	%eax, %ecx
	imulq	$963315389, %rdx, %rdx
	sarl	$31, %ecx
	sarq	$47, %rdx
	subl	%ecx, %edx
	imull	$146097, %edx, %edx
	subl	%edx, %eax
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm2, %xmm0
	movsd	%xmm2, -64(%rbp)
	divsd	.LC10(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-64(%rbp), %xmm2
	movsd	%xmm0, -88(%rbp)
	movapd	%xmm2, %xmm0
	call	uprv_floor_67@PLT
	pxor	%xmm2, %xmm2
	cvttsd2sil	%xmm0, %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$-441679365, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$15, %eax
	subl	%ecx, %eax
	imull	$36524, %eax, %eax
	subl	%eax, %edx
	cvtsi2sdl	%edx, %xmm2
	movapd	%xmm2, %xmm0
	movsd	%xmm2, -80(%rbp)
	divsd	.LC11(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm2
	movsd	%xmm0, -64(%rbp)
	movapd	%xmm2, %xmm0
	call	uprv_floor_67@PLT
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm0, %xmm0
	movslq	%eax, %rdx
	movl	%eax, %ecx
	imulq	$376287347, %rdx, %rdx
	sarl	$31, %ecx
	sarq	$39, %rdx
	subl	%ecx, %edx
	imull	$1461, %edx, %edx
	subl	%edx, %eax
	cvtsi2sdl	%eax, %xmm0
	divsd	.LC12(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-88(%rbp), %xmm3
	movsd	-96(%rbp), %xmm1
	movl	$1, %ecx
	movsd	.LC14(%rip), %xmm2
	mulsd	.LC13(%rip), %xmm1
	movsd	-64(%rbp), %xmm4
	mulsd	%xmm3, %xmm2
	addsd	%xmm2, %xmm1
	movsd	.LC15(%rip), %xmm2
	mulsd	%xmm2, %xmm4
	ucomisd	%xmm2, %xmm3
	setp	%dl
	cmovne	%ecx, %edx
	addsd	%xmm4, %xmm1
	addsd	%xmm0, %xmm1
	cvttsd2sil	%xmm1, %eax
	testb	%dl, %dl
	je	.L94
	ucomisd	%xmm2, %xmm0
	leal	1(%rax), %r12d
	movl	%eax, %r14d
	setp	%dl
	cmovne	%ecx, %edx
	testb	%dl, %dl
	je	.L94
.L75:
	imull	$365, %r14d, %eax
	pxor	%xmm0, %xmm0
	testl	%r14d, %r14d
	movl	%r14d, %r15d
	pxor	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm0
	leal	3(%r14), %eax
	cmovns	%r14d, %eax
	addsd	.LC1(%rip), %xmm0
	sarl	$31, %r15d
	sarl	$2, %eax
	cvtsi2sdl	%eax, %xmm3
	movsd	%xmm0, -64(%rbp)
	movapd	%xmm3, %xmm0
	movsd	%xmm3, -104(%rbp)
	call	uprv_floor_67@PLT
	movslq	%r14d, %rdx
	pxor	%xmm1, %xmm1
	movsd	-64(%rbp), %xmm2
	imulq	$1374389535, %rdx, %rdx
	addsd	%xmm0, %xmm2
	movq	%rdx, %r13
	sarq	$37, %rdx
	subl	%r15d, %edx
	movsd	%xmm2, -80(%rbp)
	shrq	$32, %r13
	cvtsi2sdl	%edx, %xmm1
	sarl	$7, %r13d
	subl	%r15d, %r13d
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -96(%rbp)
	call	uprv_floor_67@PLT
	movsd	-80(%rbp), %xmm2
	subsd	%xmm0, %xmm2
	movsd	%xmm2, -80(%rbp)
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%r13d, %xmm2
	movapd	%xmm2, %xmm0
	movsd	%xmm2, -88(%rbp)
	call	uprv_floor_67@PLT
	addsd	-80(%rbp), %xmm0
	movq	.LC5(%rip), %rax
	movsd	%xmm0, -80(%rbp)
	movq	%rax, %xmm0
	call	uprv_floor_67@PLT
	addsd	-80(%rbp), %xmm0
	movsd	-56(%rbp), %xmm7
	movl	%r12d, %edi
	movl	$1, %edx
	movl	$3, %esi
	subsd	%xmm0, %xmm7
	movsd	%xmm7, -80(%rbp)
	call	_ZN6icu_67L13gregorianToJDEiii
	movsd	-88(%rbp), %xmm2
	movsd	-96(%rbp), %xmm1
	movapd	%xmm0, %xmm4
	comisd	-56(%rbp), %xmm4
	movsd	-104(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	ja	.L76
	movsd	.LC6(%rip), %xmm0
	testb	$3, %r12b
	je	.L99
.L76:
	addsd	-80(%rbp), %xmm0
	mulsd	.LC16(%rip), %xmm0
	movsd	%xmm2, -96(%rbp)
	movsd	%xmm1, -88(%rbp)
	addsd	.LC17(%rip), %xmm0
	divsd	.LC18(%rip), %xmm0
	movsd	%xmm3, -56(%rbp)
	call	uprv_floor_67@PLT
	movl	$1, %edx
	movl	%r12d, %edi
	cvttsd2sil	%xmm0, %esi
	call	_ZN6icu_67L13gregorianToJDEiii
	movsd	-56(%rbp), %xmm3
	movapd	%xmm3, %xmm0
	call	uprv_floor_67@PLT
	movsd	-64(%rbp), %xmm3
	movsd	-88(%rbp), %xmm1
	addsd	%xmm0, %xmm3
	movapd	%xmm1, %xmm0
	movsd	%xmm3, -56(%rbp)
	call	uprv_floor_67@PLT
	movsd	-56(%rbp), %xmm3
	movsd	-96(%rbp), %xmm2
	subsd	%xmm0, %xmm3
	movapd	%xmm2, %xmm0
	movsd	%xmm3, -56(%rbp)
	call	uprv_floor_67@PLT
	addsd	-56(%rbp), %xmm0
	movq	.LC5(%rip), %rax
	movsd	%xmm0, -56(%rbp)
	movq	%rax, %xmm0
	call	uprv_floor_67@PLT
	addsd	-56(%rbp), %xmm0
	movsd	-72(%rbp), %xmm5
	subsd	%xmm0, %xmm5
	cvttsd2sil	%xmm5, %edx
	cmpl	$79, %edx
	jg	.L77
	subl	$79, %r12d
	testb	$3, %r14b
	je	.L100
	movl	$285, %esi
	movl	$30, %ecx
.L78:
	leal	(%rdx,%rsi), %eax
	leal	1(%rax), %r15d
	cmpl	%eax, %ecx
	jle	.L101
.L93:
	movl	%r15d, %eax
	xorl	%edx, %edx
.L81:
	movl	%edx, 20(%rbx)
	movl	$257, %ecx
	movl	$257, %esi
	movabsq	$4294967297, %rdx
	movl	%r12d, 88(%rbx)
	movl	%r12d, 16(%rbx)
	movl	%r15d, 36(%rbx)
	movl	$0, 12(%rbx)
	movl	$1, 204(%rbx)
	movb	$1, 123(%rbx)
	movq	%rdx, 128(%rbx)
	movl	$1, 136(%rbx)
	movw	%cx, 104(%rbx)
	movb	$1, 106(%rbx)
	movl	%eax, 32(%rbx)
	movq	%rdx, 148(%rbx)
	movw	%si, 109(%rbx)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	leal	-1(%rax), %r14d
	movl	%eax, %r12d
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$30, %ecx
	testb	$3, %r12b
	je	.L102
.L80:
	leal	-80(%rdx), %eax
	subl	$78, %r12d
	leal	1(%rax), %r15d
	cmpl	%eax, %ecx
	jg	.L93
.L101:
	subl	%ecx, %eax
	movl	%eax, %r13d
	cmpl	$154, %eax
	jg	.L82
	movslq	%eax, %r14
	pxor	%xmm0, %xmm0
	imulq	$-2078209981, %r14, %r14
	shrq	$32, %r14
	addl	%eax, %r14d
	sarl	$31, %eax
	sarl	$4, %r14d
	subl	%eax, %r14d
	cvtsi2sdl	%r14d, %xmm0
	call	uprv_floor_67@PLT
	movl	%r14d, %eax
	cvttsd2sil	%xmm0, %edx
	sall	$5, %eax
	subl	%r14d, %eax
	subl	%eax, %r13d
	movl	%r13d, %eax
	addl	$1, %edx
	addl	$1, %eax
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L82:
	leal	-155(%rax), %r14d
	movl	$2290649225, %eax
	pxor	%xmm0, %xmm0
	movq	%r14, %r13
	imulq	%rax, %r14
	shrq	$36, %r14
	cvtsi2sdl	%r14d, %xmm0
	imull	$30, %r14d, %r14d
	call	uprv_floor_67@PLT
	subl	%r14d, %r13d
	cvttsd2sil	%xmm0, %edx
	leal	1(%r13), %eax
	addl	$6, %edx
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L102:
	imull	$-1030792151, %r12d, %eax
	movl	$31, %ecx
	addl	$85899344, %eax
	movl	%eax, %esi
	rorl	$2, %esi
	cmpl	$42949672, %esi
	ja	.L80
	rorl	$4, %eax
	xorl	%ecx, %ecx
	cmpl	$10737419, %eax
	setb	%cl
	addl	$30, %ecx
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L100:
	imull	$-1030792151, %r14d, %eax
	addl	$85899344, %eax
	movl	%eax, %ecx
	rorl	$2, %ecx
	cmpl	$42949672, %ecx
	jbe	.L103
	movl	$286, %esi
	movl	$31, %ecx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L99:
	imull	$-1030792151, %r12d, %eax
	movq	.LC5(%rip), %rdi
	movq	%rdi, %xmm0
	addl	$85899344, %eax
	movl	%eax, %edx
	rorl	$2, %edx
	cmpl	$42949672, %edx
	ja	.L76
	rorl	$4, %eax
	movsd	.LC6(%rip), %xmm0
	cmpl	$10737418, %eax
	ja	.L76
	movq	%rdi, %xmm0
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L103:
	rorl	$4, %eax
	cmpl	$10737419, %eax
	sbbl	%esi, %esi
	notl	%esi
	addl	$286, %esi
	cmpl	$10737419, %eax
	sbbl	%ecx, %ecx
	notl	%ecx
	addl	$31, %ecx
	jmp	.L78
	.cfi_endproc
.LFE3126:
	.size	_ZN6icu_6714IndianCalendar19handleComputeFieldsEiR10UErrorCode, .-_ZN6icu_6714IndianCalendar19handleComputeFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714IndianCalendarC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6714IndianCalendarC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6714IndianCalendarC2ERKNS_6LocaleER10UErrorCode:
.LFB3107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r12, %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6714IndianCalendarE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE3107:
	.size	_ZN6icu_6714IndianCalendarC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6714IndianCalendarC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6714IndianCalendarC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6714IndianCalendarC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6714IndianCalendarC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714IndianCalendarC2ERKS0_
	.type	_ZN6icu_6714IndianCalendarC2ERKS0_, @function
_ZN6icu_6714IndianCalendarC2ERKS0_:
.LFB3110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678CalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714IndianCalendarE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3110:
	.size	_ZN6icu_6714IndianCalendarC2ERKS0_, .-_ZN6icu_6714IndianCalendarC2ERKS0_
	.globl	_ZN6icu_6714IndianCalendarC1ERKS0_
	.set	_ZN6icu_6714IndianCalendarC1ERKS0_,_ZN6icu_6714IndianCalendarC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714IndianCalendar16getStaticClassIDEv
	.type	_ZN6icu_6714IndianCalendar16getStaticClassIDEv, @function
_ZN6icu_6714IndianCalendar16getStaticClassIDEv:
.LFB3132:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714IndianCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3132:
	.size	_ZN6icu_6714IndianCalendar16getStaticClassIDEv, .-_ZN6icu_6714IndianCalendar16getStaticClassIDEv
	.section	.rodata.str1.1
.LC19:
	.string	"@calendar=Indian"
	.text
	.p2align 4
	.type	_ZN6icu_67L30initializeSystemDefaultCenturyEv, @function
_ZN6icu_67L30initializeSystemDefaultCenturyEv:
.LFB3129:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	.LC19(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-880(%rbp), %r14
	leaq	-884(%rbp), %r13
	pushq	%r12
	movq	%r14, %rdi
	.cfi_offset 12, -40
	leaq	-656(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVN6icu_6714IndianCalendarE(%rip), %rbx
	subq	$880, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -884(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	movq	%rbx, -656(%rbp)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	-884(%rbp), %eax
	testl	%eax, %eax
	jle	.L113
.L110:
	movq	%r12, %rdi
	movq	%rbx, -656(%rbp)
	call	_ZN6icu_678CalendarD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$880, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r13, %rcx
	movl	$-80, %edx
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movsd	%xmm0, -904(%rbp)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movsd	-904(%rbp), %xmm0
	movl	%eax, _ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip)
	movsd	%xmm0, _ZN6icu_67L26gSystemDefaultCenturyStartE(%rip)
	jmp	.L110
.L114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3129:
	.size	_ZN6icu_67L30initializeSystemDefaultCenturyEv, .-_ZN6icu_67L30initializeSystemDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714IndianCalendar19defaultCenturyStartEv
	.type	_ZNK6icu_6714IndianCalendar19defaultCenturyStartEv, @function
_ZNK6icu_6714IndianCalendar19defaultCenturyStartEv:
.LFB3130:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %eax
	cmpl	$2, %eax
	je	.L123
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L117
	call	_ZN6icu_67L30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L117:
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore 6
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3130:
	.size	_ZNK6icu_6714IndianCalendar19defaultCenturyStartEv, .-_ZNK6icu_6714IndianCalendar19defaultCenturyStartEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714IndianCalendar23defaultCenturyStartYearEv
	.type	_ZNK6icu_6714IndianCalendar23defaultCenturyStartYearEv, @function
_ZNK6icu_6714IndianCalendar23defaultCenturyStartYearEv:
.LFB3131:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %eax
	cmpl	$2, %eax
	je	.L134
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L128
	call	_ZN6icu_67L30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L128:
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore 6
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	ret
	.cfi_endproc
.LFE3131:
	.size	_ZNK6icu_6714IndianCalendar23defaultCenturyStartYearEv, .-_ZNK6icu_6714IndianCalendar23defaultCenturyStartYearEv
	.weak	_ZTSN6icu_6714IndianCalendarE
	.section	.rodata._ZTSN6icu_6714IndianCalendarE,"aG",@progbits,_ZTSN6icu_6714IndianCalendarE,comdat
	.align 16
	.type	_ZTSN6icu_6714IndianCalendarE, @object
	.size	_ZTSN6icu_6714IndianCalendarE, 26
_ZTSN6icu_6714IndianCalendarE:
	.string	"N6icu_6714IndianCalendarE"
	.weak	_ZTIN6icu_6714IndianCalendarE
	.section	.data.rel.ro._ZTIN6icu_6714IndianCalendarE,"awG",@progbits,_ZTIN6icu_6714IndianCalendarE,comdat
	.align 8
	.type	_ZTIN6icu_6714IndianCalendarE, @object
	.size	_ZTIN6icu_6714IndianCalendarE, 24
_ZTIN6icu_6714IndianCalendarE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714IndianCalendarE
	.quad	_ZTIN6icu_678CalendarE
	.weak	_ZTVN6icu_6714IndianCalendarE
	.section	.data.rel.ro._ZTVN6icu_6714IndianCalendarE,"awG",@progbits,_ZTVN6icu_6714IndianCalendarE,comdat
	.align 8
	.type	_ZTVN6icu_6714IndianCalendarE, @object
	.size	_ZTVN6icu_6714IndianCalendarE, 416
_ZTVN6icu_6714IndianCalendarE:
	.quad	0
	.quad	_ZTIN6icu_6714IndianCalendarE
	.quad	_ZN6icu_6714IndianCalendarD1Ev
	.quad	_ZN6icu_6714IndianCalendarD0Ev
	.quad	_ZNK6icu_6714IndianCalendar17getDynamicClassIDEv
	.quad	_ZNK6icu_6714IndianCalendar5cloneEv
	.quad	_ZNK6icu_678CalendareqERKS0_
	.quad	_ZNK6icu_678Calendar14isEquivalentToERKS0_
	.quad	_ZN6icu_678Calendar3addENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar4rollENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6714IndianCalendar14inDaylightTimeER10UErrorCode
	.quad	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6714IndianCalendar7getTypeEv
	.quad	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEv
	.quad	_ZN6icu_678Calendar11computeTimeER10UErrorCode
	.quad	_ZN6icu_678Calendar13computeFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode
	.quad	_ZNK6icu_6714IndianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.quad	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE
	.quad	_ZNK6icu_6714IndianCalendar23handleComputeMonthStartEiia
	.quad	_ZNK6icu_6714IndianCalendar20handleGetMonthLengthEii
	.quad	_ZNK6icu_6714IndianCalendar19handleGetYearLengthEi
	.quad	_ZN6icu_6714IndianCalendar21handleGetExtendedYearEv
	.quad	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields
	.quad	_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii
	.quad	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar23getFieldResolutionTableEv
	.quad	_ZN6icu_6714IndianCalendar19handleComputeFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar21getDefaultMonthInYearEi
	.quad	_ZN6icu_678Calendar20getDefaultDayInMonthEii
	.quad	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6714IndianCalendar18haveDefaultCenturyEv
	.quad	_ZNK6icu_6714IndianCalendar19defaultCenturyStartEv
	.quad	_ZNK6icu_6714IndianCalendar23defaultCenturyStartYearEv
	.quad	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode
	.quad	_ZN6icu_678Calendar14setRelatedYearEi
	.local	_ZZN6icu_6714IndianCalendar16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6714IndianCalendar16getStaticClassIDEvE7classID,1,1
	.local	_ZN6icu_67L25gSystemDefaultCenturyInitE
	.comm	_ZN6icu_67L25gSystemDefaultCenturyInitE,8,8
	.data
	.align 4
	.type	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, @object
	.size	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, 4
_ZN6icu_67L30gSystemDefaultCenturyStartYearE:
	.long	-1
	.align 8
	.type	_ZN6icu_67L26gSystemDefaultCenturyStartE, @object
	.size	_ZN6icu_67L26gSystemDefaultCenturyStartE, 8
_ZN6icu_67L26gSystemDefaultCenturyStartE:
	.long	0
	.long	1048576
	.section	.rodata
	.align 32
	.type	_ZN6icu_67L6LIMITSE, @object
	.size	_ZN6icu_67L6LIMITSE, 368
_ZN6icu_67L6LIMITSE:
	.zero	16
	.long	-5000000
	.long	-5000000
	.long	5000000
	.long	5000000
	.long	0
	.long	0
	.long	11
	.long	11
	.long	1
	.long	1
	.long	52
	.long	53
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	1
	.long	1
	.long	30
	.long	31
	.long	1
	.long	1
	.long	365
	.long	366
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	5
	.long	5
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-5000000
	.long	-5000000
	.long	5000000
	.long	5000000
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-5000000
	.long	-5000000
	.long	5000000
	.long	5000000
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	2147483648
	.long	1094337616
	.align 8
.LC2:
	.long	0
	.long	1077805056
	.align 8
.LC3:
	.long	0
	.long	1077870592
	.align 8
.LC4:
	.long	0
	.long	0
	.align 8
.LC5:
	.long	0
	.long	1072693248
	.align 8
.LC6:
	.long	0
	.long	1073741824
	.align 8
.LC7:
	.long	0
	.long	1071644672
	.align 8
.LC8:
	.long	2147483648
	.long	1094337617
	.align 8
.LC9:
	.long	0
	.long	1090639240
	.align 8
.LC10:
	.long	0
	.long	1088542080
	.align 8
.LC11:
	.long	0
	.long	1083626496
	.align 8
.LC12:
	.long	0
	.long	1081528320
	.align 8
.LC13:
	.long	0
	.long	1081671680
	.align 8
.LC14:
	.long	0
	.long	1079574528
	.align 8
.LC15:
	.long	0
	.long	1074790400
	.align 8
.LC16:
	.long	0
	.long	1076363264
	.align 8
.LC17:
	.long	0
	.long	1081561088
	.align 8
.LC18:
	.long	0
	.long	1081536512
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
