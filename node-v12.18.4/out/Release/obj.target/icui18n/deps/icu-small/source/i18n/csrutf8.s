	.file	"csrutf8.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"UTF-8"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CharsetRecog_UTF87getNameEv
	.type	_ZNK6icu_6717CharsetRecog_UTF87getNameEv, @function
_ZNK6icu_6717CharsetRecog_UTF87getNameEv:
.LFB8:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE8:
	.size	_ZNK6icu_6717CharsetRecog_UTF87getNameEv, .-_ZNK6icu_6717CharsetRecog_UTF87getNameEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharsetRecog_UTF8D2Ev
	.type	_ZN6icu_6717CharsetRecog_UTF8D2Ev, @function
_ZN6icu_6717CharsetRecog_UTF8D2Ev:
.LFB5:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_UTF8E(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE5:
	.size	_ZN6icu_6717CharsetRecog_UTF8D2Ev, .-_ZN6icu_6717CharsetRecog_UTF8D2Ev
	.globl	_ZN6icu_6717CharsetRecog_UTF8D1Ev
	.set	_ZN6icu_6717CharsetRecog_UTF8D1Ev,_ZN6icu_6717CharsetRecog_UTF8D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharsetRecog_UTF8D0Ev
	.type	_ZN6icu_6717CharsetRecog_UTF8D0Ev, @function
_ZN6icu_6717CharsetRecog_UTF8D0Ev:
.LFB7:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_UTF8E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE7:
	.size	_ZN6icu_6717CharsetRecog_UTF8D0Ev, .-_ZN6icu_6717CharsetRecog_UTF8D0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CharsetRecog_UTF85matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6717CharsetRecog_UTF85matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6717CharsetRecog_UTF85matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB9:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	48(%rsi), %ecx
	movq	40(%rsi), %r8
	cmpl	$2, %ecx
	jle	.L7
	movzbl	(%r8), %edx
	xorl	%r12d, %r12d
	cmpb	$-17, %dl
	je	.L59
.L23:
	xorl	%eax, %eax
	xorl	%r11d, %r11d
	xorl	%ebx, %ebx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L12:
	addl	$1, %r11d
.L9:
	addl	$1, %eax
	cmpl	%eax, %ecx
	jle	.L15
.L60:
	movslq	%eax, %rdx
	movzbl	(%r8,%rdx), %edx
.L16:
	testb	%dl, %dl
	jns	.L9
	movl	%edx, %r9d
	andl	$-32, %r9d
	cmpb	$-64, %r9b
	je	.L28
	movl	%edx, %r9d
	andl	$-16, %r9d
	cmpb	$-32, %r9b
	je	.L29
	andl	$-8, %edx
	cmpb	$-16, %dl
	jne	.L12
	movl	$3, %r9d
.L10:
	leal	1(%rax), %r13d
	cmpl	%r13d, %ecx
	jle	.L34
	movslq	%r13d, %rdx
	movzbl	(%r8,%rdx), %edx
	andl	$-64, %edx
	cmpb	$-128, %dl
	jne	.L35
	subl	$1, %r9d
	je	.L36
	leal	2(%rax), %r13d
	cmpl	%r13d, %ecx
	jle	.L34
	movslq	%r13d, %rdx
	movzbl	(%r8,%rdx), %edx
	andl	$-64, %edx
	cmpb	$-128, %dl
	jne	.L35
	cmpl	$1, %r9d
	je	.L36
	addl	$3, %eax
	cmpl	%eax, %ecx
	jle	.L9
	movslq	%eax, %rdx
	movzbl	(%r8,%rdx), %edx
	andl	$-64, %edx
	cmpb	$-128, %dl
	jne	.L12
.L13:
	addl	$1, %eax
	addl	$1, %ebx
	cmpl	%eax, %ecx
	jg	.L60
	.p2align 4,,10
	.p2align 3
.L15:
	testl	%r11d, %r11d
	sete	%al
	jne	.L43
	movl	$1, %r13d
	movl	$100, %ecx
	testb	%r12b, %r12b
	je	.L19
.L17:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r10, %rdx
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	$1, %r9d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$2, %r9d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%ebx, %ebx
	xorl	%r11d, %r11d
	testl	%ecx, %ecx
	jle	.L25
	movzbl	(%r8), %edx
	xorl	%r12d, %r12d
	jmp	.L23
.L59:
	cmpb	$-69, 1(%r8)
	jne	.L23
	cmpb	$-65, 2(%r8)
	sete	%r12b
	jmp	.L23
.L19:
	cmpl	$3, %ebx
	jle	.L44
	movl	$1, %r13d
	movl	$100, %ecx
	testb	%al, %al
	jne	.L17
.L44:
	testl	%ebx, %ebx
	jle	.L25
	testb	%al, %al
	jne	.L40
	.p2align 4,,10
	.p2align 3
.L25:
	movl	%ebx, %eax
	orl	%r11d, %eax
	jne	.L61
	movl	$1, %r13d
	movl	$15, %ecx
	jmp	.L17
.L43:
	testb	%r12b, %r12b
	je	.L19
	leal	(%r11,%r11,4), %eax
	addl	%eax, %eax
	cmpl	%ebx, %eax
	jge	.L25
.L40:
	movl	$1, %r13d
	movl	$80, %ecx
	jmp	.L17
.L34:
	movl	%r13d, %eax
	jmp	.L9
.L35:
	movl	%r13d, %eax
	jmp	.L12
.L36:
	movl	%r13d, %eax
	jmp	.L13
.L61:
	leal	(%r11,%r11,4), %eax
	movl	$0, %ecx
	addl	%eax, %eax
	cmpl	%eax, %ebx
	movl	$25, %eax
	cmovg	%eax, %ecx
	setg	%r13b
	jmp	.L17
	.cfi_endproc
.LFE9:
	.size	_ZNK6icu_6717CharsetRecog_UTF85matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6717CharsetRecog_UTF85matchEPNS_9InputTextEPNS_12CharsetMatchE
	.weak	_ZTSN6icu_6717CharsetRecog_UTF8E
	.section	.rodata._ZTSN6icu_6717CharsetRecog_UTF8E,"aG",@progbits,_ZTSN6icu_6717CharsetRecog_UTF8E,comdat
	.align 16
	.type	_ZTSN6icu_6717CharsetRecog_UTF8E, @object
	.size	_ZTSN6icu_6717CharsetRecog_UTF8E, 29
_ZTSN6icu_6717CharsetRecog_UTF8E:
	.string	"N6icu_6717CharsetRecog_UTF8E"
	.weak	_ZTIN6icu_6717CharsetRecog_UTF8E
	.section	.data.rel.ro._ZTIN6icu_6717CharsetRecog_UTF8E,"awG",@progbits,_ZTIN6icu_6717CharsetRecog_UTF8E,comdat
	.align 8
	.type	_ZTIN6icu_6717CharsetRecog_UTF8E, @object
	.size	_ZTIN6icu_6717CharsetRecog_UTF8E, 24
_ZTIN6icu_6717CharsetRecog_UTF8E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717CharsetRecog_UTF8E
	.quad	_ZTIN6icu_6717CharsetRecognizerE
	.weak	_ZTVN6icu_6717CharsetRecog_UTF8E
	.section	.data.rel.ro._ZTVN6icu_6717CharsetRecog_UTF8E,"awG",@progbits,_ZTVN6icu_6717CharsetRecog_UTF8E,comdat
	.align 8
	.type	_ZTVN6icu_6717CharsetRecog_UTF8E, @object
	.size	_ZTVN6icu_6717CharsetRecog_UTF8E, 56
_ZTVN6icu_6717CharsetRecog_UTF8E:
	.quad	0
	.quad	_ZTIN6icu_6717CharsetRecog_UTF8E
	.quad	_ZNK6icu_6717CharsetRecog_UTF87getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	_ZNK6icu_6717CharsetRecog_UTF85matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6717CharsetRecog_UTF8D1Ev
	.quad	_ZN6icu_6717CharsetRecog_UTF8D0Ev
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
