	.file	"double-conversion-strtod.cpp"
	.text
	.p2align 4
	.type	_ZN6icu_6717double_conversionL22CompareBufferWithDiyFpENS0_6VectorIKcEEiNS0_5DiyFpE.isra.0, @function
_ZN6icu_6717double_conversionL22CompareBufferWithDiyFpENS0_6VectorIKcEEiNS0_5DiyFpE.isra.0:
.LFB214:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1104(%rbp), %r14
	leaq	-576(%rbp), %r15
	pushq	%r13
	movq	%r14, %rdi
	.cfi_offset 13, -40
	movl	%edx, %r13d
	movq	%r9, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$1064, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -1104(%rbp)
	movl	$0, -576(%rbp)
	call	_ZN6icu_6717double_conversion6Bignum19AssignDecimalStringENS0_6VectorIKcEE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6717double_conversion6Bignum12AssignUInt64Em@PLT
	movl	%r13d, %esi
	testl	%r13d, %r13d
	js	.L2
	movq	%r14, %rdi
	call	_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi@PLT
	movl	%r12d, %esi
	testl	%r12d, %r12d
	jle	.L4
.L10:
	movq	%r15, %rdi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
.L5:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717double_conversion6Bignum7CompareERKS1_S3_@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L9
	addq	$1064, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	negl	%esi
	movq	%r15, %rdi
	call	_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi@PLT
	movl	%r12d, %esi
	testl	%r12d, %r12d
	jg	.L10
.L4:
	negl	%esi
	movq	%r14, %rdi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	jmp	.L5
.L9:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE214:
	.size	_ZN6icu_6717double_conversionL22CompareBufferWithDiyFpENS0_6VectorIKcEEiNS0_5DiyFpE.isra.0, .-_ZN6icu_6717double_conversionL22CompareBufferWithDiyFpENS0_6VectorIKcEEiNS0_5DiyFpE.isra.0
	.section	.text.unlikely,"ax",@progbits
.LCOLDB2:
	.text
.LHOTB2:
	.p2align 4
	.globl	_ZN6icu_6717double_conversion13StrtodTrimmedENS0_6VectorIKcEEi
	.type	_ZN6icu_6717double_conversion13StrtodTrimmedENS0_6VectorIKcEEi, @function
_ZN6icu_6717double_conversion13StrtodTrimmedENS0_6VectorIKcEEi:
.LFB187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L368
	movslq	%edx, %r14
	movq	%rsi, %r12
	leal	(%r14,%rsi), %eax
	cmpl	$309, %eax
	jg	.L367
	pxor	%xmm0, %xmm0
	cmpl	$-323, %eax
	jl	.L11
	movq	%rdi, %r13
	movq	%rdi, %rdx
	cmpl	$15, %esi
	jle	.L371
.L14:
	testl	%r12d, %r12d
	jle	.L81
	movl	$1, %edx
	xorl	%ebx, %ebx
	movabsq	$1844674407370955160, %r8
	.p2align 4,,10
	.p2align 3
.L29:
	movsbl	-1(%r13,%rdx), %eax
	leaq	(%rbx,%rbx,4), %rcx
	movl	%edx, %edi
	subl	$48, %eax
	cmpl	%edx, %esi
	cltq
	leaq	(%rax,%rcx,2), %rbx
	setg	%cl
	cmpq	%r8, %rbx
	setbe	%al
	addq	$1, %rdx
	testb	%al, %cl
	jne	.L29
	cmpl	%edi, %r12d
	je	.L82
	movl	%r12d, %eax
	movslq	%edi, %rdx
	subl	%edi, %eax
	addq	%r13, %rdx
	addl	%r14d, %eax
.L28:
	cmpb	$52, (%rdx)
	movl	$4, %r8d
	setg	%dl
	movzbl	%dl, %edx
	addq	%rdx, %rbx
.L30:
	movabsq	$-18014398509481984, %rcx
	xorl	%r15d, %r15d
	movq	%rcx, %rdx
	testq	%rcx, %rbx
	jne	.L35
	.p2align 4,,10
	.p2align 3
.L32:
	salq	$10, %rbx
	subl	$10, %r15d
	testq	%rdx, %rbx
	je	.L32
.L35:
	testq	%rbx, %rbx
	js	.L33
	.p2align 4,,10
	.p2align 3
.L34:
	subl	$1, %r15d
	addq	%rbx, %rbx
	jns	.L34
.L33:
	pxor	%xmm0, %xmm0
	cmpl	$-348, %eax
	jl	.L11
	movl	%r15d, %ecx
	leaq	-84(%rbp), %rdx
	movl	%eax, %edi
	movl	%eax, -104(%rbp)
	negl	%ecx
	leaq	-80(%rbp), %rsi
	movq	$0, -80(%rbp)
	salq	%cl, %r8
	movl	$0, -72(%rbp)
	movq	%r8, -112(%rbp)
	call	_ZN6icu_6717double_conversion16PowersOfTenCache32GetCachedPowerForDecimalExponentEiPNS0_5DiyFpEPi@PLT
	movl	-84(%rbp), %edx
	movl	-104(%rbp), %eax
	movq	-112(%rbp), %r8
	cmpl	%edx, %eax
	jne	.L372
.L37:
	movq	-80(%rbp), %rax
	movq	%rbx, %rsi
	movl	%ebx, %ebx
	movl	$2147483648, %r10d
	shrq	$32, %rsi
	movq	%rbx, %rdi
	movl	-72(%rbp), %ecx
	movq	%rax, %r9
	movq	%rsi, %rdx
	movl	%eax, %eax
	imulq	%rax, %rdx
	shrq	$32, %r9
	leal	64(%r15,%rcx), %ecx
	imulq	%rax, %rbx
	imulq	%r9, %rdi
	movl	%edx, %eax
	imulq	%r9, %rsi
	shrq	$32, %rdx
	shrq	$32, %rbx
	addq	%rbx, %rax
	addq	%r10, %rax
	movl	%edi, %r10d
	shrq	$32, %rdi
	addq	%rdi, %rdx
	addq	%r10, %rax
	movabsq	$-18014398509481984, %rdi
	addq	%rdx, %rsi
	shrq	$32, %rax
	xorl	%edx, %edx
	addq	%rsi, %rax
	testq	%r8, %r8
	movq	%rdi, %rsi
	setne	%dl
	addl	$8, %edx
	movslq	%edx, %rdx
	addq	%rdx, %r8
	movl	%ecx, %edx
	testq	%rdi, %rax
	jne	.L51
	.p2align 4,,10
	.p2align 3
.L48:
	salq	$10, %rax
	subl	$10, %edx
	testq	%rsi, %rax
	je	.L48
.L51:
	testq	%rax, %rax
	js	.L49
	.p2align 4,,10
	.p2align 3
.L50:
	subl	$1, %edx
	addq	%rax, %rax
	jns	.L50
.L49:
	subl	%edx, %ecx
	salq	%cl, %r8
	leal	64(%rdx), %ecx
	cmpl	$-1021, %ecx
	jge	.L86
	cmpl	$-1073, %ecx
	jl	.L87
	leal	1138(%rdx), %r9d
	movl	$64, %edi
	subl	%r9d, %edi
	cmpl	$60, %edi
	jg	.L54
	movl	%edi, %ecx
	movq	$-1, %rsi
	salq	%cl, %rsi
	movl	$63, %ecx
	subl	%r9d, %ecx
	movl	$8, %r9d
	notq	%rsi
	salq	%cl, %r9
.L52:
	andq	%rax, %rsi
	movl	%edi, %ecx
	addl	%edi, %edx
	shrq	%cl, %rax
	salq	$3, %rsi
	leaq	(%r8,%r9), %rcx
	cmpq	%rcx, %rsi
	jnb	.L373
	subq	%r8, %r9
	cmpq	%r9, %rsi
	seta	%dil
	cmpl	$971, %edx
	jg	.L57
	.p2align 4,,10
	.p2align 3
.L74:
	cmpl	$-1074, %edx
	jl	.L58
	movabsq	$4503599627370496, %rsi
	movq	%rax, %rcx
	andq	%rsi, %rcx
	cmpl	$-1074, %edx
	je	.L59
	testq	%rcx, %rcx
	je	.L61
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L363:
	testq	%rcx, %rcx
	jne	.L374
.L61:
	addq	%rax, %rax
	movl	%edx, %r15d
	subl	$1, %edx
	movq	%rax, %rcx
	andq	%rsi, %rcx
	cmpl	$-1074, %edx
	jne	.L363
.L59:
	movabsq	$4503599627370496, %rdx
	testq	%rcx, %rcx
	cmovne	%rdx, %rcx
.L70:
	movabsq	$4503599627370495, %r15
	andq	%r15, %rax
	orq	%rcx, %rax
	movq	%rax, %r15
	movq	%rax, %xmm0
	testb	%dil, %dil
	je	.L11
	movsd	.LC1(%rip), %xmm1
	ucomisd	%xmm1, %xmm0
	jnp	.L375
.L67:
	movabsq	$4503599627370495, %rbx
	movabsq	$9218868437227405312, %rax
	andq	%r15, %rbx
	testq	%rax, %r15
	je	.L88
	movabsq	$4503599627370496, %rax
	movq	%r15, %r8
	shrq	$52, %r8
	addq	%rax, %rbx
	subl	$1076, %r8d
.L68:
	leaq	1(%rbx,%rbx), %rcx
	movl	%r14d, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movsd	%xmm0, -104(%rbp)
	call	_ZN6icu_6717double_conversionL22CompareBufferWithDiyFpENS0_6VectorIKcEEiNS0_5DiyFpE.isra.0
	movsd	-104(%rbp), %xmm0
	testl	%eax, %eax
	js	.L11
	jne	.L370
	andl	$1, %ebx
	je	.L11
.L370:
	movabsq	$9218868437227405312, %rax
	cmpq	%rax, %r15
	je	.L367
	leaq	1(%r15), %rax
	movq	%rax, %xmm0
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L367:
	movsd	.LC1(%rip), %xmm1
.L65:
	movapd	%xmm1, %xmm0
.L11:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L376
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L371:
	cmpl	$-22, %r14d
	jnb	.L377
	cmpl	$22, %r14d
	jbe	.L378
	testl	%r14d, %r14d
	js	.L14
	movl	$15, %ecx
	movl	%r14d, %edi
	subl	%esi, %ecx
	subl	%ecx, %edi
	cmpl	$22, %edi
	jg	.L14
	testl	%esi, %esi
	jle	.L80
	movsbl	0(%r13), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	cmpl	$1, %esi
	jle	.L25
	movabsq	$1844674407370955160, %rsi
	cmpq	%rsi, %rdx
	ja	.L25
	movsbl	1(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$2, %r12d
	jle	.L25
	cmpq	%rsi, %rdx
	ja	.L25
	movsbl	2(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$3, %r12d
	jle	.L25
	cmpq	%rsi, %rdx
	ja	.L25
	movsbl	3(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$4, %r12d
	jle	.L25
	cmpq	%rsi, %rdx
	ja	.L25
	movsbl	4(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$5, %r12d
	jle	.L25
	cmpq	%rsi, %rdx
	ja	.L25
	movsbl	5(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$6, %r12d
	jle	.L25
	cmpq	%rsi, %rdx
	ja	.L25
	movsbl	6(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$7, %r12d
	jle	.L25
	cmpq	%rsi, %rdx
	ja	.L25
	movsbl	7(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$8, %r12d
	jle	.L25
	cmpq	%rsi, %rdx
	ja	.L25
	movsbl	8(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$9, %r12d
	jle	.L25
	cmpq	%rsi, %rdx
	ja	.L25
	movsbl	9(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$10, %r12d
	jle	.L25
	cmpq	%rsi, %rdx
	ja	.L25
	movsbl	10(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$11, %r12d
	jle	.L25
	cmpq	%rsi, %rdx
	ja	.L25
	movsbl	11(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$12, %r12d
	jle	.L25
	cmpq	%rsi, %rdx
	ja	.L25
	movsbl	12(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$13, %r12d
	jle	.L25
	cmpq	%rsi, %rdx
	ja	.L25
	movsbl	13(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$15, %r12d
	jne	.L25
	cmpq	%rsi, %rdx
	ja	.L25
	movsbl	14(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
.L25:
	testq	%rdx, %rdx
	js	.L26
.L362:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L27:
	leaq	_ZN6icu_6717double_conversionL19exact_powers_of_tenE(%rip), %rdx
	movslq	%ecx, %rcx
	movslq	%edi, %rax
	mulsd	(%rdx,%rcx,8), %xmm0
	mulsd	(%rdx,%rax,8), %xmm0
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L372:
	subl	%edx, %eax
	cmpl	$7, %eax
	ja	.L38
	leaq	.L40(%rip), %rsi
	movl	%eax, %ecx
	movslq	(%rsi,%rcx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L40:
	.long	.L38-.L40
	.long	.L46-.L40
	.long	.L84-.L40
	.long	.L44-.L40
	.long	.L43-.L40
	.long	.L42-.L40
	.long	.L41-.L40
	.long	.L39-.L40
	.text
	.p2align 4,,10
	.p2align 3
.L378:
	testl	%esi, %esi
	jle	.L21
	movsbl	(%rdi), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	cmpl	$1, %esi
	jle	.L22
	movabsq	$1844674407370955160, %rax
	cmpq	%rax, %rdx
	ja	.L22
	movsbl	1(%rdi), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$2, %esi
	jle	.L22
	movabsq	$1844674407370955160, %rcx
	cmpq	%rcx, %rdx
	ja	.L22
	movsbl	2(%rdi), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$3, %esi
	jle	.L22
	cmpq	%rcx, %rdx
	ja	.L22
	movsbl	3(%rdi), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$4, %esi
	jle	.L22
	cmpq	%rcx, %rdx
	ja	.L22
	movsbl	4(%rdi), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$5, %esi
	jle	.L22
	cmpq	%rcx, %rdx
	ja	.L22
	movsbl	5(%rdi), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$6, %esi
	jle	.L22
	cmpq	%rcx, %rdx
	ja	.L22
	movsbl	6(%rdi), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$7, %esi
	jle	.L22
	cmpq	%rcx, %rdx
	ja	.L22
	movsbl	7(%rdi), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$8, %esi
	jle	.L22
	cmpq	%rcx, %rdx
	ja	.L22
	movsbl	8(%rdi), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$9, %esi
	jle	.L22
	cmpq	%rcx, %rdx
	ja	.L22
	movsbl	9(%rdi), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$10, %esi
	jle	.L22
	cmpq	%rcx, %rdx
	ja	.L22
	movsbl	10(%rdi), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$11, %esi
	jle	.L22
	cmpq	%rcx, %rdx
	ja	.L22
	movsbl	11(%rdi), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$12, %esi
	jle	.L22
	cmpq	%rcx, %rdx
	ja	.L22
	movsbl	12(%rdi), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$13, %esi
	jle	.L22
	cmpq	%rcx, %rdx
	ja	.L22
	movsbl	13(%rdi), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$15, %esi
	jne	.L22
	cmpq	%rcx, %rdx
	ja	.L22
	movsbl	14(%rdi), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
.L22:
	testq	%rdx, %rdx
	js	.L23
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L21:
	leaq	_ZN6icu_6717double_conversionL19exact_powers_of_tenE(%rip), %rax
	mulsd	(%rax,%r14,8), %xmm0
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$8192, %r9d
	movl	$2047, %esi
	movl	$11, %edi
	jmp	.L52
.L39:
	movl	$24, %esi
	movl	$2560000000, %ecx
	.p2align 4,,10
	.p2align 3
.L45:
	movl	%ebx, %edx
	shrq	$32, %rbx
	addl	%esi, %r15d
	imulq	%rcx, %rdx
	imulq	%rcx, %rbx
	movq	%rdx, %rcx
	movl	%edx, %edx
	shrq	$32, %rcx
	addq	%rbx, %rcx
	movl	$2147483648, %ebx
	addq	%rdx, %rbx
	movl	$19, %edx
	shrq	$32, %rbx
	subl	%r12d, %edx
	leaq	(%rcx,%rbx), %rbx
	cmpl	%edx, %eax
	leaq	4(%r8), %rcx
	cmovg	%rcx, %r8
	jmp	.L37
.L41:
	movl	$20, %esi
	movl	$4096000000, %ecx
	jmp	.L45
.L42:
	movl	$17, %esi
	movl	$3276800000, %ecx
	jmp	.L45
.L43:
	movl	$14, %esi
	movl	$2621440000, %ecx
	jmp	.L45
.L44:
	movl	$10, %esi
	movl	$4194304000, %ecx
	jmp	.L45
.L84:
	movl	$7, %esi
	movl	$3355443200, %ecx
	jmp	.L45
.L46:
	movl	$4, %esi
	movl	$2684354560, %ecx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L377:
	testl	%esi, %esi
	jle	.L16
	movsbl	(%rdi), %eax
	subl	$48, %eax
	cltq
	cmpl	$1, %esi
	jle	.L17
	movabsq	$1844674407370955160, %rcx
	cmpq	%rcx, %rax
	ja	.L17
	movsbl	1(%rdi), %edx
	leaq	(%rax,%rax,4), %rax
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rax,2), %rax
	cmpl	$2, %esi
	jle	.L17
	cmpq	%rcx, %rax
	ja	.L17
	leaq	(%rax,%rax,4), %rdx
	movsbl	2(%rdi), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rax
	cmpl	$3, %esi
	jle	.L17
	movq	%rcx, %rdx
	cmpq	%rcx, %rax
	ja	.L17
	leaq	(%rax,%rax,4), %rcx
	movsbl	3(%rdi), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$4, %esi
	jle	.L17
	cmpq	%rdx, %rax
	ja	.L17
	leaq	(%rax,%rax,4), %rcx
	movsbl	4(%rdi), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$5, %esi
	jle	.L17
	cmpq	%rdx, %rax
	ja	.L17
	leaq	(%rax,%rax,4), %rcx
	movsbl	5(%rdi), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$6, %esi
	jle	.L17
	cmpq	%rdx, %rax
	ja	.L17
	leaq	(%rax,%rax,4), %rcx
	movsbl	6(%rdi), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$7, %esi
	jle	.L17
	cmpq	%rdx, %rax
	ja	.L17
	leaq	(%rax,%rax,4), %rdx
	movsbl	7(%rdi), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rax
	cmpl	$8, %esi
	jle	.L17
	movabsq	$1844674407370955160, %rdx
	cmpq	%rdx, %rax
	ja	.L17
	leaq	(%rax,%rax,4), %rcx
	movsbl	8(%rdi), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$9, %esi
	jle	.L17
	cmpq	%rdx, %rax
	ja	.L17
	leaq	(%rax,%rax,4), %rcx
	movsbl	9(%rdi), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$10, %esi
	jle	.L17
	cmpq	%rdx, %rax
	ja	.L17
	leaq	(%rax,%rax,4), %rcx
	movsbl	10(%rdi), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$11, %esi
	jle	.L17
	cmpq	%rdx, %rax
	ja	.L17
	leaq	(%rax,%rax,4), %rcx
	movsbl	11(%rdi), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$12, %esi
	jle	.L17
	cmpq	%rdx, %rax
	ja	.L17
	leaq	(%rax,%rax,4), %rdx
	movsbl	12(%rdi), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rax
	cmpl	$13, %esi
	jle	.L17
	movabsq	$1844674407370955160, %rdx
	cmpq	%rdx, %rax
	ja	.L17
	leaq	(%rax,%rax,4), %rcx
	movsbl	13(%rdi), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$15, %esi
	jne	.L17
	cmpq	%rdx, %rax
	ja	.L17
	leaq	(%rax,%rax,4), %rdx
	movsbl	14(%rdi), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rax
.L17:
	testq	%rax, %rax
	js	.L18
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L16:
	negl	%r14d
	leaq	_ZN6icu_6717double_conversionL19exact_powers_of_tenE(%rip), %rax
	movslq	%r14d, %r14
	divsd	(%rax,%r14,8), %xmm0
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L82:
	movl	%r14d, %eax
	xorl	%r8d, %r8d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L373:
	movabsq	$9007199254740992, %rdi
	addq	$1, %rax
	cmpq	%rdi, %rax
	jne	.L56
	movabsq	$4503599627370496, %rax
	addl	$1, %edx
.L56:
	subq	%r8, %r9
	cmpq	%r9, %rsi
	seta	%dil
	cmpq	%rcx, %rsi
	setb	%cl
	andl	%ecx, %edi
	cmpl	$971, %edx
	jle	.L74
.L57:
	movsd	.LC1(%rip), %xmm0
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L375:
	je	.L65
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L58:
	testb	%dil, %dil
	je	.L368
	xorl	%r15d, %r15d
	pxor	%xmm0, %xmm0
	movl	$-1075, %r8d
	xorl	%ebx, %ebx
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$4, %ecx
.L53:
	shrq	%cl, %r8
	shrq	%cl, %rax
	addl	%ecx, %edx
	movl	$60, %edi
	movabsq	$4611686018427387904, %r9
	addq	$9, %r8
	movabsq	$1152921504606846975, %rsi
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$4, %ecx
	subl	%r9d, %ecx
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L374:
	addl	$1074, %r15d
	movq	%r15, %rcx
	salq	$52, %rcx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L81:
	xorl	%ebx, %ebx
	jmp	.L28
.L23:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L21
.L26:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L27
.L88:
	movl	$-1075, %r8d
	jmp	.L68
.L60:
	addl	$1075, %edx
	movq	%rdx, %rcx
	salq	$52, %rcx
	jmp	.L70
.L80:
	xorl	%edx, %edx
	jmp	.L362
.L376:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion13StrtodTrimmedENS0_6VectorIKcEEi.cold, @function
_ZN6icu_6717double_conversion13StrtodTrimmedENS0_6VectorIKcEEi.cold:
.LFSB187:
.L38:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE187:
	.text
	.size	_ZN6icu_6717double_conversion13StrtodTrimmedENS0_6VectorIKcEEi, .-_ZN6icu_6717double_conversion13StrtodTrimmedENS0_6VectorIKcEEi
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion13StrtodTrimmedENS0_6VectorIKcEEi.cold, .-_ZN6icu_6717double_conversion13StrtodTrimmedENS0_6VectorIKcEEi.cold
.LCOLDE2:
	.text
.LHOTE2:
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6StrtodENS0_6VectorIKcEEi
	.type	_ZN6icu_6717double_conversion6StrtodENS0_6VectorIKcEEi, @function
_ZN6icu_6717double_conversion6StrtodENS0_6VectorIKcEEi:
.LFB188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$800, %rsp
	movq	%fs:40, %rsi
	movq	%rsi, -8(%rbp)
	xorl	%esi, %esi
	testl	%eax, %eax
	jle	.L380
	leal	-1(%rax), %r10d
	xorl	%ecx, %ecx
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L381:
	leaq	1(%rcx), %rsi
	cmpq	%rcx, %r10
	je	.L380
	movq	%rsi, %rcx
.L383:
	cmpb	$48, (%rdi,%rcx)
	leaq	(%rdi,%rcx), %rsi
	je	.L381
	subl	%ecx, %eax
	movl	%eax, %ecx
	subl	$1, %ecx
	js	.L394
	movslq	%ecx, %rcx
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L384:
	subq	$1, %rcx
	testl	%ecx, %ecx
	js	.L394
.L387:
	cmpb	$48, (%rsi,%rcx)
	movl	%ecx, %r8d
	je	.L384
	leal	1(%rcx), %ecx
	subl	%ecx, %eax
	addl	%eax, %edx
	cmpl	$780, %ecx
	jle	.L395
	leaq	-800(%rbp), %rax
	movl	$97, %ecx
	leal	-779(%r8,%rdx), %edx
	movq	%rax, %rdi
	rep movsq
	movzwl	(%rsi), %ecx
	movw	%cx, (%rdi)
	movzbl	2(%rsi), %ecx
	movl	$780, %esi
	movb	%cl, 2(%rdi)
	movq	%rax, %rdi
	movb	$49, -21(%rbp)
.L388:
	call	_ZN6icu_6717double_conversion13StrtodTrimmedENS0_6VectorIKcEEi
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L396
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	addl	%eax, %edx
	movq	%rsi, %r8
.L380:
	xorl	%ecx, %ecx
.L386:
	movq	%r8, %rdi
	movl	%ecx, %esi
	jmp	.L388
.L396:
	call	__stack_chk_fail@PLT
.L395:
	movq	%rsi, %r8
	jmp	.L386
	.cfi_endproc
.LFE188:
	.size	_ZN6icu_6717double_conversion6StrtodENS0_6VectorIKcEEi, .-_ZN6icu_6717double_conversion6StrtodENS0_6VectorIKcEEi
	.section	.text.unlikely
.LCOLDB10:
	.text
.LHOTB10:
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6StrtofENS0_6VectorIKcEEi
	.type	_ZN6icu_6717double_conversion6StrtofENS0_6VectorIKcEEi, @function
_ZN6icu_6717double_conversion6StrtofENS0_6VectorIKcEEi:
.LFB190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1912, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L398
	leal	-1(%rsi), %r8d
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L399:
	leaq	1(%rax), %rcx
	cmpq	%rax, %r8
	je	.L398
	movq	%rcx, %rax
.L401:
	cmpb	$48, (%rdi,%rax)
	leaq	(%rdi,%rax), %r13
	je	.L399
	subl	%eax, %esi
	movl	%esi, %eax
	subl	$1, %eax
	js	.L398
	cltq
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L402:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L398
.L405:
	cmpb	$48, 0(%r13,%rax)
	movl	%eax, %r8d
	je	.L402
	leal	1(%rax), %r14d
	subl	%r14d, %esi
	leal	(%rsi,%rdx), %r12d
	cmpl	$780, %r14d
	jle	.L802
	leaq	-848(%rbp), %rax
	movl	$97, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	leal	-779(%r8,%r12), %r12d
	rep movsq
	movzwl	(%rsi), %edx
	movw	%dx, (%rdi)
	movzbl	2(%rsi), %edx
	movb	%dl, 2(%rdi)
	leal	780(%r12), %edx
	movb	$49, -69(%rbp)
	cmpl	$309, %edx
	jg	.L492
	cmpl	$-323, %edx
	jge	.L803
	.p2align 4,,10
	.p2align 3
.L398:
	pxor	%xmm1, %xmm1
.L397:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L804
	addq	$1912, %rsp
	movaps	%xmm1, %xmm0
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	movss	.LC3(%rip), %xmm1
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L803:
	movl	$780, %r14d
	movq	%rax, %r13
.L407:
	movl	$1, %edx
	xorl	%ebx, %ebx
	movabsq	$1844674407370955160, %rdi
	.p2align 4,,10
	.p2align 3
.L420:
	movsbl	-1(%r13,%rdx), %eax
	leaq	(%rbx,%rbx,4), %rcx
	movl	%edx, %esi
	subl	$48, %eax
	cmpl	%edx, %r14d
	cltq
	leaq	(%rax,%rcx,2), %rbx
	setg	%cl
	cmpq	%rdi, %rbx
	setbe	%al
	addq	$1, %rdx
	testb	%al, %cl
	jne	.L420
	cmpl	%r14d, %esi
	je	.L493
	movslq	%esi, %rax
	movl	%r14d, %edi
	movl	$4, %r8d
	cmpb	$52, 0(%r13,%rax)
	setg	%al
	subl	%esi, %edi
	movzbl	%al, %eax
	addl	%r12d, %edi
	addq	%rax, %rbx
.L421:
	movabsq	$-18014398509481984, %rdx
	xorl	%r15d, %r15d
	movq	%rdx, %rax
	testq	%rdx, %rbx
	jne	.L426
	.p2align 4,,10
	.p2align 3
.L423:
	salq	$10, %rbx
	subl	$10, %r15d
	testq	%rax, %rbx
	je	.L423
.L426:
	testq	%rbx, %rbx
	js	.L424
	.p2align 4,,10
	.p2align 3
.L425:
	subl	$1, %r15d
	addq	%rbx, %rbx
	jns	.L425
.L424:
	cmpl	$-348, %edi
	jl	.L398
	movl	%r15d, %ecx
	leaq	-1924(%rbp), %rdx
	leaq	-1920(%rbp), %rsi
	movl	%edi, -1940(%rbp)
	movq	$0, -1920(%rbp)
	negl	%ecx
	movl	$0, -1912(%rbp)
	salq	%cl, %r8
	movq	%r8, -1952(%rbp)
	call	_ZN6icu_6717double_conversion16PowersOfTenCache32GetCachedPowerForDecimalExponentEiPNS0_5DiyFpEPi@PLT
	movl	-1924(%rbp), %eax
	movl	-1940(%rbp), %edi
	movq	-1952(%rbp), %r8
	cmpl	%edi, %eax
	jne	.L805
.L428:
	movq	-1920(%rbp), %rax
	movq	%rbx, %rsi
	movl	%ebx, %ebx
	movl	$2147483648, %r10d
	shrq	$32, %rsi
	movq	%rbx, %rdi
	movl	-1912(%rbp), %ecx
	movq	%rax, %r9
	movq	%rsi, %rdx
	movl	%eax, %eax
	imulq	%rax, %rdx
	shrq	$32, %r9
	leal	64(%r15,%rcx), %ecx
	imulq	%rax, %rbx
	imulq	%r9, %rdi
	movl	%edx, %eax
	imulq	%r9, %rsi
	shrq	$32, %rdx
	shrq	$32, %rbx
	addq	%rbx, %rax
	addq	%r10, %rax
	movl	%edi, %r10d
	shrq	$32, %rdi
	addq	%rdi, %rdx
	addq	%r10, %rax
	movabsq	$-18014398509481984, %rdi
	addq	%rdx, %rsi
	shrq	$32, %rax
	xorl	%edx, %edx
	addq	%rsi, %rax
	testq	%r8, %r8
	movl	%ecx, %esi
	setne	%dl
	addl	$8, %edx
	movslq	%edx, %rdx
	addq	%rdx, %r8
	movq	%rdi, %rdx
	testq	%rdi, %rax
	jne	.L442
	.p2align 4,,10
	.p2align 3
.L439:
	salq	$10, %rax
	subl	$10, %esi
	testq	%rdx, %rax
	je	.L439
.L442:
	testq	%rax, %rax
	js	.L440
	.p2align 4,,10
	.p2align 3
.L441:
	subl	$1, %esi
	addq	%rax, %rax
	jns	.L441
.L440:
	subl	%esi, %ecx
	leal	64(%rsi), %edx
	salq	%cl, %r8
	cmpl	$-1021, %edx
	jge	.L443
	cmpl	$-1073, %edx
	jl	.L497
	leal	1138(%rsi), %edi
	movl	$64, %edx
	subl	%edi, %edx
	cmpl	$60, %edx
	jg	.L445
	movl	%edx, %ecx
	movq	$-1, %r10
	movl	$8, %r9d
	salq	%cl, %r10
	movl	$63, %ecx
	subl	%edi, %ecx
	notq	%r10
	salq	%cl, %r9
.L446:
	andq	%rax, %r10
	leaq	(%r9,%r8), %r11
	leal	(%rsi,%rdx), %edi
	movl	%edx, %ecx
	shrq	%cl, %rax
	salq	$3, %r10
	movq	%rax, %rdx
	cmpq	%r10, %r11
	jbe	.L806
.L447:
	cmpl	$971, %edi
	jg	.L500
	cmpl	$-1074, %edi
	jl	.L501
	.p2align 4,,10
	.p2align 3
.L487:
	movabsq	$4503599627370496, %rax
	movq	%rdx, %rcx
	andq	%rax, %rcx
	cmpl	$-1074, %edi
	je	.L450
	testq	%rcx, %rcx
	je	.L452
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L798:
	testq	%rcx, %rcx
	jne	.L807
.L452:
	addq	%rdx, %rdx
	movl	%edi, %esi
	subl	$1, %edi
	movq	%rdx, %rcx
	andq	%rax, %rcx
	cmpl	$-1074, %edi
	jne	.L798
.L450:
	movabsq	$4503599627370496, %rax
	testq	%rcx, %rcx
	cmovne	%rax, %rcx
.L483:
	movabsq	$4503599627370495, %rax
	andq	%rdx, %rax
	orq	%rcx, %rax
	movq	%rax, %xmm0
.L449:
	subq	%r8, %r9
	cmpq	%r10, %r9
	jnb	.L485
	cmpq	%r10, %r11
	jbe	.L485
	ucomisd	.LC1(%rip), %xmm0
	movl	$0, %eax
	setnp	%cl
	cmovne	%eax, %ecx
	jmp	.L412
.L802:
	leal	(%r14,%r12), %eax
	cmpl	$309, %eax
	jg	.L492
	cmpl	$-323, %eax
	jl	.L398
	cmpl	$15, %r14d
	jg	.L407
	cmpl	$-22, %r12d
	jnb	.L808
	cmpl	$22, %r12d
	jbe	.L809
	testl	%r12d, %r12d
	js	.L407
	movl	$15, %ecx
	movl	%r12d, %esi
	subl	%r14d, %ecx
	subl	%ecx, %esi
	cmpl	$22, %esi
	jg	.L407
	movsbl	0(%r13), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	cmpl	$1, %r14d
	jle	.L417
	movabsq	$1844674407370955160, %rdi
	cmpq	%rdi, %rdx
	ja	.L417
	movsbl	1(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$2, %r14d
	jle	.L417
	cmpq	%rdi, %rdx
	ja	.L417
	movsbl	2(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$3, %r14d
	jle	.L417
	cmpq	%rdi, %rdx
	ja	.L417
	movsbl	3(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$4, %r14d
	jle	.L417
	cmpq	%rdi, %rdx
	ja	.L417
	movsbl	4(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$5, %r14d
	jle	.L417
	cmpq	%rdi, %rdx
	ja	.L417
	movsbl	5(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$6, %r14d
	jle	.L417
	cmpq	%rdi, %rdx
	ja	.L417
	movsbl	6(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$7, %r14d
	jle	.L417
	cmpq	%rdi, %rdx
	ja	.L417
	movsbl	7(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$8, %r14d
	jle	.L417
	cmpq	%rdi, %rdx
	ja	.L417
	movsbl	8(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$9, %r14d
	jle	.L417
	cmpq	%rdi, %rdx
	ja	.L417
	movsbl	9(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$10, %r14d
	jle	.L417
	cmpq	%rdi, %rdx
	ja	.L417
	movsbl	10(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$11, %r14d
	jle	.L417
	cmpq	%rdi, %rdx
	ja	.L417
	movsbl	11(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$12, %r14d
	jle	.L417
	cmpq	%rdi, %rdx
	ja	.L417
	movsbl	12(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$13, %r14d
	jle	.L417
	cmpq	%rdi, %rdx
	ja	.L417
	movsbl	13(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$15, %r14d
	jne	.L417
	cmpq	%rdi, %rdx
	ja	.L417
	movsbl	14(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
.L417:
	testq	%rdx, %rdx
	js	.L418
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L419:
	leaq	_ZN6icu_6717double_conversionL19exact_powers_of_tenE(%rip), %rdx
	movslq	%ecx, %rcx
	movslq	%esi, %rax
	mulsd	(%rdx,%rcx,8), %xmm0
	movl	$1, %ecx
	mulsd	(%rdx,%rax,8), %xmm0
	.p2align 4,,10
	.p2align 3
.L412:
	movsd	.LC4(%rip), %xmm2
	comisd	%xmm2, %xmm0
	jb	.L810
	comisd	.LC9(%rip), %xmm0
	jnb	.L502
	movss	.LC5(%rip), %xmm1
	movapd	%xmm2, %xmm3
.L458:
	ucomisd	%xmm3, %xmm0
	jnp	.L811
.L519:
	movabsq	$9218868437227405312, %rdx
	movq	%xmm0, %rax
	cmpq	%rdx, %rax
	je	.L503
	testq	%rax, %rax
	js	.L812
	leaq	1(%rax), %rbx
	movq	%rbx, %xmm0
	jne	.L460
	movss	.LC6(%rip), %xmm5
	movss	%xmm5, -1940(%rbp)
.L465:
	comisd	%xmm2, %xmm0
	jb	.L799
	comisd	.LC9(%rip), %xmm0
	movss	.LC3(%rip), %xmm7
	movss	%xmm7, -1952(%rbp)
	jnb	.L468
	movss	.LC5(%rip), %xmm5
	movss	%xmm5, -1952(%rbp)
.L468:
	testb	%cl, %cl
	jne	.L469
	movss	.LC3(%rip), %xmm7
	movq	%xmm0, %rax
	movabsq	$9218868437227405312, %rdx
	movss	%xmm7, -1952(%rbp)
	cmpq	%rdx, %rax
	je	.L469
	leaq	1(%rax), %rbx
	movq	%rbx, %xmm0
	testq	%rax, %rax
	js	.L813
.L472:
	comisd	%xmm2, %xmm0
	jb	.L814
	comisd	.LC9(%rip), %xmm0
	movss	.LC3(%rip), %xmm4
	movss	%xmm4, -1952(%rbp)
	jnb	.L469
	movss	.LC5(%rip), %xmm4
	movss	%xmm4, -1952(%rbp)
	.p2align 4,,10
	.p2align 3
.L469:
	movss	-1952(%rbp), %xmm4
	ucomiss	-1940(%rbp), %xmm4
	jnp	.L815
.L521:
	movss	-1940(%rbp), %xmm6
	ucomiss	.LC8(%rip), %xmm6
	jp	.L522
	jne	.L522
	movabsq	$4503599627370496, %rbx
	movl	$-202, %r15d
.L475:
	movl	$0, -1904(%rbp)
	movl	%r14d, %edx
	movq	%r13, %rsi
	leaq	-1904(%rbp), %r14
	movq	%r14, %rdi
	leaq	-1376(%rbp), %r13
	movl	$0, -1376(%rbp)
	call	_ZN6icu_6717double_conversion6Bignum19AssignDecimalStringENS0_6VectorIKcEE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717double_conversion6Bignum12AssignUInt64Em@PLT
	movl	%r12d, %esi
	testl	%r12d, %r12d
	js	.L478
	movq	%r14, %rdi
	call	_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi@PLT
.L479:
	movl	%r15d, %esi
	testl	%r15d, %r15d
	jle	.L480
	movq	%r13, %rdi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
.L481:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717double_conversion6Bignum7CompareERKS1_S3_@PLT
	movss	-1940(%rbp), %xmm1
	testl	%eax, %eax
	js	.L397
	jne	.L514
	movd	%xmm1, %eax
	testb	$1, %al
	je	.L397
.L514:
	movss	-1952(%rbp), %xmm1
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L805:
	subl	%eax, %edi
	cmpl	$7, %edi
	ja	.L429
	leaq	.L431(%rip), %rcx
	movl	%edi, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L431:
	.long	.L429-.L431
	.long	.L437-.L431
	.long	.L495-.L431
	.long	.L435-.L431
	.long	.L434-.L431
	.long	.L433-.L431
	.long	.L432-.L431
	.long	.L430-.L431
	.text
	.p2align 4,,10
	.p2align 3
.L809:
	movsbl	0(%r13), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	cmpl	$1, %r14d
	jle	.L414
	movabsq	$1844674407370955160, %rax
	cmpq	%rax, %rdx
	ja	.L414
	movsbl	1(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$2, %r14d
	jle	.L414
	movabsq	$1844674407370955160, %rcx
	cmpq	%rcx, %rdx
	ja	.L414
	movsbl	2(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$3, %r14d
	jle	.L414
	cmpq	%rcx, %rdx
	ja	.L414
	movsbl	3(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$4, %r14d
	jle	.L414
	cmpq	%rcx, %rdx
	ja	.L414
	movsbl	4(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$5, %r14d
	jle	.L414
	cmpq	%rcx, %rdx
	ja	.L414
	movsbl	5(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$6, %r14d
	jle	.L414
	cmpq	%rcx, %rdx
	ja	.L414
	movsbl	6(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$7, %r14d
	jle	.L414
	cmpq	%rcx, %rdx
	ja	.L414
	movsbl	7(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$8, %r14d
	jle	.L414
	cmpq	%rcx, %rdx
	ja	.L414
	movsbl	8(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$9, %r14d
	jle	.L414
	cmpq	%rcx, %rdx
	ja	.L414
	movsbl	9(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$10, %r14d
	jle	.L414
	cmpq	%rcx, %rdx
	ja	.L414
	movsbl	10(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$11, %r14d
	jle	.L414
	cmpq	%rcx, %rdx
	ja	.L414
	movsbl	11(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$12, %r14d
	jle	.L414
	cmpq	%rcx, %rdx
	ja	.L414
	movsbl	12(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$13, %r14d
	jle	.L414
	cmpq	%rcx, %rdx
	ja	.L414
	movsbl	13(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$15, %r14d
	jne	.L414
	cmpq	%rcx, %rdx
	ja	.L414
	movsbl	14(%r13), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
.L414:
	movslq	%r12d, %rcx
	testq	%rdx, %rdx
	js	.L415
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L416:
	leaq	_ZN6icu_6717double_conversionL19exact_powers_of_tenE(%rip), %rax
	mulsd	(%rax,%rcx,8), %xmm0
	movl	$1, %ecx
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%rax, %r10
	leaq	8192(%r8), %r11
	shrq	$11, %rax
	leal	11(%rsi), %edi
	andl	$2047, %r10d
	movq	%rax, %rdx
	salq	$3, %r10
	cmpq	%r11, %r10
	jb	.L498
	movabsq	$9007199254740992, %rax
	addq	$1, %rdx
	cmpq	%rax, %rdx
	jne	.L499
	leal	12(%rsi), %edi
	movl	$8192, %r9d
	movabsq	$4503599627370496, %rdx
.L448:
	cmpl	$971, %edi
	jg	.L816
	cmpl	$-1074, %edi
	jge	.L487
	pxor	%xmm0, %xmm0
.L485:
	movl	$1, %ecx
	jmp	.L412
.L430:
	movl	$24, %ecx
	movl	$2560000000, %edx
	.p2align 4,,10
	.p2align 3
.L436:
	movl	%ebx, %eax
	shrq	$32, %rbx
	addl	%ecx, %r15d
	imulq	%rdx, %rax
	imulq	%rdx, %rbx
	movq	%rax, %rdx
	movl	%eax, %eax
	shrq	$32, %rdx
	addq	%rbx, %rdx
	movl	$2147483648, %ebx
	addq	%rbx, %rax
	shrq	$32, %rax
	leaq	(%rdx,%rax), %rbx
	movl	$19, %eax
	leaq	4(%r8), %rdx
	subl	%r14d, %eax
	cmpl	%eax, %edi
	cmovg	%rdx, %r8
	jmp	.L428
.L432:
	movl	$20, %ecx
	movl	$4096000000, %edx
	jmp	.L436
.L433:
	movl	$17, %ecx
	movl	$3276800000, %edx
	jmp	.L436
.L434:
	movl	$14, %ecx
	movl	$2621440000, %edx
	jmp	.L436
.L435:
	movl	$10, %ecx
	movl	$4194304000, %edx
	jmp	.L436
.L495:
	movl	$7, %ecx
	movl	$3355443200, %edx
	jmp	.L436
.L437:
	movl	$4, %ecx
	movl	$2684354560, %edx
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L815:
	je	.L397
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L799:
	pxor	%xmm4, %xmm4
	cvtsd2ss	%xmm0, %xmm4
	movss	%xmm4, -1952(%rbp)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L503:
	movsd	.LC1(%rip), %xmm0
.L460:
	subq	$1, %rax
	movq	%rax, %xmm3
.L484:
	comisd	%xmm2, %xmm3
	jb	.L817
	comisd	.LC9(%rip), %xmm3
	movss	.LC3(%rip), %xmm6
	movss	%xmm6, -1940(%rbp)
	jnb	.L465
	movss	.LC5(%rip), %xmm6
	movss	%xmm6, -1940(%rbp)
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L522:
	movl	-1940(%rbp), %r15d
	movl	%r15d, %eax
	andl	$8388607, %eax
	testl	$2139095040, %r15d
	je	.L512
	shrl	$23, %r15d
	addl	$8388608, %eax
	movzbl	%r15b, %r15d
	subl	$151, %r15d
.L477:
	leal	1(%rax,%rax), %ebx
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L502:
	movsd	.LC1(%rip), %xmm3
	movss	.LC3(%rip), %xmm1
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L480:
	negl	%esi
	movq	%r14, %rdi
	call	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi@PLT
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L478:
	negl	%esi
	movq	%r13, %rdi
	call	_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi@PLT
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L812:
	movq	%xmm0, %rdx
	pxor	%xmm0, %xmm0
	btrq	$63, %rdx
	testq	%rdx, %rdx
	je	.L462
	leaq	-1(%rax), %rbx
	movq	%rbx, %xmm0
.L462:
	movabsq	$-4503599627370496, %rdx
	cmpq	%rdx, %rax
	jne	.L818
	movss	.LC7(%rip), %xmm7
	movss	%xmm7, -1940(%rbp)
	jmp	.L465
.L818:
	addq	$1, %rax
	movq	%rax, %xmm3
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L813:
	movq	%rax, %rdx
	btrq	$63, %rdx
	testq	%rdx, %rdx
	je	.L509
	subq	$1, %rax
	movq	%rax, %xmm0
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L814:
	pxor	%xmm7, %xmm7
	cvtsd2ss	%xmm0, %xmm7
	movss	%xmm7, -1952(%rbp)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L493:
	movl	%r12d, %edi
	xorl	%r8d, %r8d
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L808:
	movsbl	0(%r13), %eax
	subl	$48, %eax
	cltq
	cmpl	$1, %r14d
	jle	.L409
	movabsq	$1844674407370955160, %rcx
	cmpq	%rcx, %rax
	ja	.L409
	movsbl	1(%r13), %edx
	leaq	(%rax,%rax,4), %rax
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rax,2), %rax
	cmpl	$2, %r14d
	jle	.L409
	cmpq	%rcx, %rax
	ja	.L409
	leaq	(%rax,%rax,4), %rdx
	movsbl	2(%r13), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rax
	cmpl	$3, %r14d
	jle	.L409
	movq	%rcx, %rdx
	cmpq	%rcx, %rax
	ja	.L409
	leaq	(%rax,%rax,4), %rcx
	movsbl	3(%r13), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$4, %r14d
	jle	.L409
	cmpq	%rdx, %rax
	ja	.L409
	leaq	(%rax,%rax,4), %rcx
	movsbl	4(%r13), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$5, %r14d
	jle	.L409
	cmpq	%rdx, %rax
	ja	.L409
	leaq	(%rax,%rax,4), %rcx
	movsbl	5(%r13), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$6, %r14d
	jle	.L409
	cmpq	%rdx, %rax
	ja	.L409
	leaq	(%rax,%rax,4), %rcx
	movsbl	6(%r13), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$7, %r14d
	jle	.L409
	cmpq	%rdx, %rax
	ja	.L409
	leaq	(%rax,%rax,4), %rdx
	movsbl	7(%r13), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rax
	cmpl	$8, %r14d
	jle	.L409
	movabsq	$1844674407370955160, %rdx
	cmpq	%rdx, %rax
	ja	.L409
	leaq	(%rax,%rax,4), %rcx
	movsbl	8(%r13), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$9, %r14d
	jle	.L409
	cmpq	%rdx, %rax
	ja	.L409
	leaq	(%rax,%rax,4), %rcx
	movsbl	9(%r13), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$10, %r14d
	jle	.L409
	cmpq	%rdx, %rax
	ja	.L409
	leaq	(%rax,%rax,4), %rcx
	movsbl	10(%r13), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$11, %r14d
	jle	.L409
	cmpq	%rdx, %rax
	ja	.L409
	leaq	(%rax,%rax,4), %rcx
	movsbl	11(%r13), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$12, %r14d
	jle	.L409
	cmpq	%rdx, %rax
	ja	.L409
	leaq	(%rax,%rax,4), %rdx
	movsbl	12(%r13), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rax
	cmpl	$13, %r14d
	jle	.L409
	movabsq	$1844674407370955160, %rdx
	cmpq	%rdx, %rax
	ja	.L409
	leaq	(%rax,%rax,4), %rcx
	movsbl	13(%r13), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	$15, %r14d
	jne	.L409
	cmpq	%rdx, %rax
	ja	.L409
	leaq	(%rax,%rax,4), %rdx
	movsbl	14(%r13), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rax
.L409:
	testq	%rax, %rax
	js	.L410
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L411:
	movl	%r12d, %eax
	leaq	_ZN6icu_6717double_conversionL19exact_powers_of_tenE(%rip), %rdx
	movl	$1, %ecx
	negl	%eax
	cltq
	divsd	(%rdx,%rax,8), %xmm0
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L509:
	pxor	%xmm6, %xmm6
	movss	%xmm6, -1952(%rbp)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L410:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L497:
	movl	$4, %ecx
.L444:
	shrq	%cl, %r8
	shrq	%cl, %rax
	addl	%ecx, %esi
	movl	$60, %edx
	movabsq	$4611686018427387904, %r9
	addq	$9, %r8
	movabsq	$1152921504606846975, %r10
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L445:
	movl	$4, %ecx
	subl	%edi, %ecx
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L807:
	leal	1074(%rsi), %ecx
	salq	$52, %rcx
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L816:
	movsd	.LC1(%rip), %xmm0
	jmp	.L485
.L415:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L416
.L811:
	jne	.L519
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L418:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L419
.L512:
	movl	$-150, %r15d
	jmp	.L477
.L806:
	addq	$1, %rdx
	jmp	.L448
.L451:
	leal	1075(%rdi), %ecx
	salq	$52, %rcx
	jmp	.L483
.L804:
	call	__stack_chk_fail@PLT
.L817:
	pxor	%xmm5, %xmm5
	cvtsd2ss	%xmm3, %xmm5
	movss	%xmm5, -1940(%rbp)
	jmp	.L465
.L500:
	movsd	.LC1(%rip), %xmm0
	jmp	.L449
.L499:
	movl	$8192, %r9d
	jmp	.L448
.L498:
	movl	$8192, %r9d
	jmp	.L447
.L810:
	pxor	%xmm1, %xmm1
	pxor	%xmm3, %xmm3
	cvtsd2ss	%xmm0, %xmm1
	cvtss2sd	%xmm1, %xmm3
	jmp	.L458
.L501:
	pxor	%xmm0, %xmm0
	jmp	.L449
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion6StrtofENS0_6VectorIKcEEi.cold, @function
_ZN6icu_6717double_conversion6StrtofENS0_6VectorIKcEEi.cold:
.LFSB190:
.L429:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE190:
	.text
	.size	_ZN6icu_6717double_conversion6StrtofENS0_6VectorIKcEEi, .-_ZN6icu_6717double_conversion6StrtofENS0_6VectorIKcEEi
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion6StrtofENS0_6VectorIKcEEi.cold, .-_ZN6icu_6717double_conversion6StrtofENS0_6VectorIKcEEi.cold
.LCOLDE10:
	.text
.LHOTE10:
	.section	.rodata
	.align 32
	.type	_ZN6icu_6717double_conversionL19exact_powers_of_tenE, @object
	.size	_ZN6icu_6717double_conversionL19exact_powers_of_tenE, 184
_ZN6icu_6717double_conversionL19exact_powers_of_tenE:
	.long	0
	.long	1072693248
	.long	0
	.long	1076101120
	.long	0
	.long	1079574528
	.long	0
	.long	1083129856
	.long	0
	.long	1086556160
	.long	0
	.long	1090021888
	.long	0
	.long	1093567616
	.long	0
	.long	1097011920
	.long	0
	.long	1100470148
	.long	0
	.long	1104006501
	.long	536870912
	.long	1107468383
	.long	3892314112
	.long	1110919286
	.long	2717908992
	.long	1114446484
	.long	3846176768
	.long	1117925532
	.long	512753664
	.long	1121369284
	.long	640942080
	.long	1124887541
	.long	937459712
	.long	1128383353
	.long	2245566464
	.long	1131820119
	.long	1733216256
	.long	1135329645
	.long	1620131072
	.long	1138841828
	.long	2025163840
	.long	1142271773
	.long	3605196624
	.long	1145772772
	.long	105764242
	.long	1149300943
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	2146435072
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC3:
	.long	2139095040
	.section	.rodata.cst8
	.align 8
.LC4:
	.long	3758096384
	.long	1206910975
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	2139095039
	.align 4
.LC6:
	.long	2147483648
	.align 4
.LC7:
	.long	4286578688
	.align 4
.LC8:
	.long	0
	.section	.rodata.cst8
	.align 8
.LC9:
	.long	4026531840
	.long	1206910975
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
