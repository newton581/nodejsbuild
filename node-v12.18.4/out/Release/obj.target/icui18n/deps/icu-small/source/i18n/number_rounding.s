	.file	"number_rounding.cpp"
	.text
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB0:
	.text
.LHOTB0:
	.align 2
	.p2align 4
	.type	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode.part.0, @function
_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode.part.0:
.LFB3977:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$9, (%rdi)
	ja	.L2
	movl	(%rdi), %eax
	movq	%rsi, %r12
	leaq	.L4(%rip), %rsi
	movq	%rdi, %rbx
	movq	%rdx, %r15
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L3-.L4
	.long	.L11-.L4
	.long	.L10-.L4
	.long	.L9-.L4
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L2-.L4
	.long	.L3-.L4
	.text
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$5, (%rdx)
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity15roundToInfinityEv@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movswl	10(%rdi), %esi
	movl	32(%rdi), %edx
	cmpl	$-1, %esi
	je	.L25
	negl	%esi
.L13:
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode@PLT
	movswl	8(%rbx), %esi
	movl	$-2147483647, %eax
	testl	%esi, %esi
	cmove	%eax, %esi
	xorl	%edi, %edi
	call	uprv_max_67@PLT
	movl	%eax, %esi
.L37:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity14setMinFractionEi@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movswl	14(%rdi), %r13d
	movl	32(%rdi), %r14d
	cmpl	$-1, %r13d
	je	.L26
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L40
.L16:
	subl	%r13d, %eax
	leal	1(%rax), %esi
.L15:
	movq	%r15, %rcx
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode@PLT
	movq	%r12, %rdi
	movswl	12(%rbx), %r13d
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L41
.L17:
	subl	%r13d, %eax
	xorl	%edi, %edi
	notl	%eax
	movl	%eax, %esi
	call	uprv_max_67@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN6icu_676number4impl15DecimalQuantity14setMinFractionEi@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	testb	%al, %al
	je	.L1
	cmpw	$0, 12(%rbx)
	jle	.L1
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity13setMinIntegerEi@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movswl	8(%rdi), %r13d
	movswl	10(%rdi), %r14d
	movl	$-2147483647, %eax
	testl	%r13d, %r13d
	cmove	%eax, %r13d
	cmpl	$-1, %r14d
	je	.L29
	negl	%r14d
.L19:
	movswl	12(%rbx), %eax
	cmpw	$-1, %ax
	je	.L42
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L43
.L24:
	subl	-52(%rbp), %eax
	movl	%r14d, %edi
	leal	1(%rax), %esi
	call	uprv_min_67@PLT
	movl	%eax, %esi
.L23:
	movl	32(%rbx), %edx
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode@PLT
	movl	%r13d, %esi
	xorl	%edi, %edi
	call	uprv_max_67@PLT
	movl	%eax, %esi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L7:
	movl	32(%rdi), %esi
	movsd	8(%rdi), %xmm0
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity16roundToIncrementEd25UNumberFormatRoundingModeR10UErrorCode@PLT
.L38:
	movswl	16(%rbx), %esi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L6:
	movswl	18(%rdi), %esi
	movl	32(%rdi), %edx
	movq	%r15, %rcx
	movq	%r12, %rdi
	negl	%esi
	call	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L5:
	movswl	18(%rdi), %esi
	movl	32(%rdi), %edx
	movq	%r15, %rcx
	movq	%r12, %rdi
	negl	%esi
	call	_ZN6icu_676number4impl15DecimalQuantity13roundToNickelEi25UNumberFormatRoundingModeR10UErrorCode@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L42:
	movswl	14(%rbx), %edx
	cmpl	$-1, %edx
	movl	%edx, -52(%rbp)
	je	.L30
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	movl	-52(%rbp), %edx
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L44
.L22:
	subl	%edx, %eax
	leal	1(%rax), %esi
.L21:
	movl	%r14d, %edi
	call	uprv_max_67@PLT
	movl	%eax, %esi
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	movl	-52(%rbp), %edx
	jmp	.L22
.L26:
	movl	$-2147483648, %esi
	jmp	.L15
.L25:
	movl	$-2147483648, %esi
	jmp	.L13
.L29:
	movl	$-2147483648, %r14d
	jmp	.L19
.L30:
	movl	$-2147483648, %esi
	jmp	.L21
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode.part.0.cold, @function
_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode.part.0.cold:
.LFSB3977:
.L2:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3977:
	.text
	.size	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode.part.0, .-_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode.part.0
	.section	.text.unlikely
	.size	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode.part.0.cold, .-_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode.part.0.cold
.LCOLDE0:
	.text
.LHOTE0:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl18MultiplierProducerD2Ev
	.type	_ZN6icu_676number4impl18MultiplierProducerD2Ev, @function
_ZN6icu_676number4impl18MultiplierProducerD2Ev:
.LFB2834:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2834:
	.size	_ZN6icu_676number4impl18MultiplierProducerD2Ev, .-_ZN6icu_676number4impl18MultiplierProducerD2Ev
	.globl	_ZN6icu_676number4impl18MultiplierProducerD1Ev
	.set	_ZN6icu_676number4impl18MultiplierProducerD1Ev,_ZN6icu_676number4impl18MultiplierProducerD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl18MultiplierProducerD0Ev
	.type	_ZN6icu_676number4impl18MultiplierProducerD0Ev, @function
_ZN6icu_676number4impl18MultiplierProducerD0Ev:
.LFB2836:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE2836:
	.size	_ZN6icu_676number4impl18MultiplierProducerD0Ev, .-_ZN6icu_676number4impl18MultiplierProducerD0Ev
	.p2align 4
	.globl	_ZN6icu_676number4impl13roundingutils20doubleFractionLengthEdPa
	.type	_ZN6icu_676number4impl13roundingutils20doubleFractionLengthEdPa, @function
_ZN6icu_676number4impl13roundingutils20doubleFractionLengthEdPa:
.LFB2837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$18, %ecx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	xorl	%edi, %edi
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %r9
	leaq	-57(%rbp), %r8
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	-52(%rbp), %rax
	pushq	%rax
	call	_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_@PLT
	popq	%rax
	movl	-56(%rbp), %eax
	popq	%rdx
	testq	%rbx, %rbx
	je	.L49
	cmpl	$1, %eax
	je	.L53
	movb	$-1, (%rbx)
.L49:
	subw	-52(%rbp), %ax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L54
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movzbl	-48(%rbp), %esi
	leal	-48(%rsi), %edx
	movb	%dl, (%rbx)
	jmp	.L49
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2837:
	.size	_ZN6icu_676number4impl13roundingutils20doubleFractionLengthEdPa, .-_ZN6icu_676number4impl13roundingutils20doubleFractionLengthEdPa
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision9unlimitedEv
	.type	_ZN6icu_676number9Precision9unlimitedEv, @function
_ZN6icu_676number9Precision9unlimitedEv:
.LFB2838:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1, (%rdi)
	movq	%rdi, %rax
	movl	$4, 24(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE2838:
	.size	_ZN6icu_676number9Precision9unlimitedEv, .-_ZN6icu_676number9Precision9unlimitedEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision7integerEv
	.type	_ZN6icu_676number9Precision7integerEv, @function
_ZN6icu_676number9Precision7integerEv:
.LFB2839:
	.cfi_startproc
	endbr64
	movabsq	$-4294967296, %rdx
	movl	$2, (%rdi)
	movq	%rdi, %rax
	movq	%rdx, 8(%rdi)
	movl	$4, 24(%rdi)
	ret
	.cfi_endproc
.LFE2839:
	.size	_ZN6icu_676number9Precision7integerEv, .-_ZN6icu_676number9Precision7integerEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision13fixedFractionEi
	.type	_ZN6icu_676number9Precision13fixedFractionEi, @function
_ZN6icu_676number9Precision13fixedFractionEi:
.LFB2840:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	cmpl	$999, %esi
	ja	.L58
	movl	$2, (%rdi)
	movw	%si, 8(%rdi)
	movw	%si, 10(%rdi)
	movl	$-1, 12(%rdi)
	movl	$4, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$9, (%rdi)
	movl	$65810, 8(%rdi)
	ret
	.cfi_endproc
.LFE2840:
	.size	_ZN6icu_676number9Precision13fixedFractionEi, .-_ZN6icu_676number9Precision13fixedFractionEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision11minFractionEi
	.type	_ZN6icu_676number9Precision11minFractionEi, @function
_ZN6icu_676number9Precision11minFractionEi:
.LFB2844:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	cmpl	$999, %esi
	ja	.L61
	movl	$-1, %edx
	movl	$2, (%rdi)
	movw	%si, 8(%rdi)
	movw	%dx, 10(%rdi)
	movl	$-1, 12(%rdi)
	movl	$4, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$9, (%rdi)
	movl	$65810, 8(%rdi)
	ret
	.cfi_endproc
.LFE2844:
	.size	_ZN6icu_676number9Precision11minFractionEi, .-_ZN6icu_676number9Precision11minFractionEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision11maxFractionEi
	.type	_ZN6icu_676number9Precision11maxFractionEi, @function
_ZN6icu_676number9Precision11maxFractionEi:
.LFB2845:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	cmpl	$999, %esi
	ja	.L64
	xorl	%edx, %edx
	movl	$2, (%rdi)
	movw	%dx, 8(%rdi)
	movw	%si, 10(%rdi)
	movl	$-1, 12(%rdi)
	movl	$4, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$9, (%rdi)
	movl	$65810, 8(%rdi)
	ret
	.cfi_endproc
.LFE2845:
	.size	_ZN6icu_676number9Precision11maxFractionEi, .-_ZN6icu_676number9Precision11maxFractionEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision14minMaxFractionEii
	.type	_ZN6icu_676number9Precision14minMaxFractionEii, @function
_ZN6icu_676number9Precision14minMaxFractionEii:
.LFB2846:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	movq	%rdi, %rax
	notl	%ecx
	shrl	$31, %ecx
	cmpl	%edx, %esi
	setle	%dil
	testb	%cl, %dil
	je	.L67
	cmpl	$999, %edx
	jle	.L75
.L67:
	movl	$9, (%rax)
	movl	$65810, 8(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$2, (%rax)
	movw	%si, 8(%rax)
	movw	%dx, 10(%rax)
	movl	$-1, 12(%rax)
	movl	$4, 24(%rax)
	ret
	.cfi_endproc
.LFE2846:
	.size	_ZN6icu_676number9Precision14minMaxFractionEii, .-_ZN6icu_676number9Precision14minMaxFractionEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision22fixedSignificantDigitsEi
	.type	_ZN6icu_676number9Precision22fixedSignificantDigitsEi, @function
_ZN6icu_676number9Precision22fixedSignificantDigitsEi:
.LFB2847:
	.cfi_startproc
	endbr64
	leal	-1(%rsi), %edx
	movq	%rdi, %rax
	cmpl	$998, %edx
	ja	.L77
	movl	$3, (%rdi)
	movl	$-1, 8(%rdi)
	movw	%si, 12(%rdi)
	movw	%si, 14(%rdi)
	movl	$4, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$9, (%rdi)
	movl	$65810, 8(%rdi)
	ret
	.cfi_endproc
.LFE2847:
	.size	_ZN6icu_676number9Precision22fixedSignificantDigitsEi, .-_ZN6icu_676number9Precision22fixedSignificantDigitsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision20minSignificantDigitsEi
	.type	_ZN6icu_676number9Precision20minSignificantDigitsEi, @function
_ZN6icu_676number9Precision20minSignificantDigitsEi:
.LFB2848:
	.cfi_startproc
	endbr64
	leal	-1(%rsi), %edx
	movq	%rdi, %rax
	cmpl	$998, %edx
	ja	.L80
	movl	$-1, %edx
	movl	$3, (%rdi)
	movl	$-1, 8(%rdi)
	movw	%si, 12(%rdi)
	movw	%dx, 14(%rdi)
	movl	$4, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$9, (%rdi)
	movl	$65810, 8(%rdi)
	ret
	.cfi_endproc
.LFE2848:
	.size	_ZN6icu_676number9Precision20minSignificantDigitsEi, .-_ZN6icu_676number9Precision20minSignificantDigitsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision20maxSignificantDigitsEi
	.type	_ZN6icu_676number9Precision20maxSignificantDigitsEi, @function
_ZN6icu_676number9Precision20maxSignificantDigitsEi:
.LFB2849:
	.cfi_startproc
	endbr64
	leal	-1(%rsi), %edx
	movq	%rdi, %rax
	cmpl	$998, %edx
	ja	.L83
	movl	$1, %edx
	movl	$3, (%rdi)
	movl	$-1, 8(%rdi)
	movw	%dx, 12(%rdi)
	movw	%si, 14(%rdi)
	movl	$4, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$9, (%rdi)
	movl	$65810, 8(%rdi)
	ret
	.cfi_endproc
.LFE2849:
	.size	_ZN6icu_676number9Precision20maxSignificantDigitsEi, .-_ZN6icu_676number9Precision20maxSignificantDigitsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision23minMaxSignificantDigitsEii
	.type	_ZN6icu_676number9Precision23minMaxSignificantDigitsEii, @function
_ZN6icu_676number9Precision23minMaxSignificantDigitsEii:
.LFB2850:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	movq	%rdi, %rax
	setg	%dil
	cmpl	%edx, %esi
	setle	%cl
	testb	%cl, %dil
	je	.L86
	cmpl	$999, %edx
	jle	.L94
.L86:
	movl	$9, (%rax)
	movl	$65810, 8(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$3, (%rax)
	movl	$-1, 8(%rax)
	movw	%si, 12(%rax)
	movw	%dx, 14(%rax)
	movl	$4, 24(%rax)
	ret
	.cfi_endproc
.LFE2850:
	.size	_ZN6icu_676number9Precision23minMaxSignificantDigitsEii, .-_ZN6icu_676number9Precision23minMaxSignificantDigitsEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision9incrementEd
	.type	_ZN6icu_676number9Precision9incrementEd, @function
_ZN6icu_676number9Precision9incrementEd:
.LFB2851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	comisd	.LC1(%rip), %xmm0
	ja	.L106
	movl	$9, (%rdi)
	movl	$65810, 8(%rdi)
.L95:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	movq	%r12, %rax
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	subq	$8, %rsp
	leaq	-52(%rbp), %rax
	xorl	%edi, %edi
	xorl	%esi, %esi
	pushq	%rax
	leaq	-57(%rbp), %r8
	leaq	-48(%rbp), %rdx
	movl	$18, %ecx
	leaq	-56(%rbp), %r9
	movsd	%xmm0, -72(%rbp)
	call	_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_@PLT
	movl	-56(%rbp), %eax
	popq	%rdi
	movsd	-72(%rbp), %xmm0
	popq	%r8
	cmpl	$1, %eax
	je	.L98
	subw	-52(%rbp), %ax
.L99:
	xorl	%edx, %edx
	movw	%ax, 18(%r12)
	movl	$5, (%r12)
	movw	%dx, 16(%r12)
	movl	$4, 24(%r12)
	movsd	%xmm0, 8(%r12)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L98:
	movzbl	-48(%rbp), %edx
	movl	$1, %eax
	subw	-52(%rbp), %ax
	leal	-48(%rdx), %ecx
	cmpb	$49, %dl
	jne	.L100
	xorl	%esi, %esi
	movw	%ax, 18(%r12)
	movl	$6, (%r12)
	movw	%si, 16(%r12)
	movl	$4, 24(%r12)
	movsd	%xmm0, 8(%r12)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L100:
	cmpb	$5, %cl
	jne	.L99
	xorl	%ecx, %ecx
	movw	%ax, 18(%r12)
	movl	$7, (%r12)
	movw	%cx, 16(%r12)
	movl	$4, 24(%r12)
	movsd	%xmm0, 8(%r12)
	jmp	.L95
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2851:
	.size	_ZN6icu_676number9Precision9incrementEd, .-_ZN6icu_676number9Precision9incrementEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision8currencyE14UCurrencyUsage
	.type	_ZN6icu_676number9Precision8currencyE14UCurrencyUsage, @function
_ZN6icu_676number9Precision8currencyE14UCurrencyUsage:
.LFB2855:
	.cfi_startproc
	endbr64
	movl	$8, (%rdi)
	movq	%rdi, %rax
	movl	%esi, 8(%rdi)
	movl	$4, 24(%rdi)
	ret
	.cfi_endproc
.LFE2855:
	.size	_ZN6icu_676number9Precision8currencyE14UCurrencyUsage, .-_ZN6icu_676number9Precision8currencyE14UCurrencyUsage
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number17FractionPrecision13withMinDigitsEi
	.type	_ZNK6icu_676number17FractionPrecision13withMinDigitsEi, @function
_ZNK6icu_676number17FractionPrecision13withMinDigitsEi:
.LFB2856:
	.cfi_startproc
	endbr64
	cmpl	$9, (%rsi)
	movq	%rdi, %rax
	je	.L113
	leal	-1(%rdx), %ecx
	cmpl	$998, %ecx
	ja	.L112
	movl	8(%rsi), %ecx
	movw	%dx, 12(%rdi)
	movl	$-1, %edx
	movl	$4, (%rdi)
	movl	%ecx, 8(%rdi)
	movw	%dx, 14(%rdi)
	movl	$4, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$9, (%rdi)
	movl	$65810, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	movq	16(%rsi), %rdx
	movdqu	(%rsi), %xmm0
	movq	%rdx, 16(%rdi)
	movl	24(%rsi), %edx
	movups	%xmm0, (%rdi)
	movl	%edx, 24(%rdi)
	ret
	.cfi_endproc
.LFE2856:
	.size	_ZNK6icu_676number17FractionPrecision13withMinDigitsEi, .-_ZNK6icu_676number17FractionPrecision13withMinDigitsEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number17FractionPrecision13withMaxDigitsEi
	.type	_ZNK6icu_676number17FractionPrecision13withMaxDigitsEi, @function
_ZNK6icu_676number17FractionPrecision13withMaxDigitsEi:
.LFB2857:
	.cfi_startproc
	endbr64
	cmpl	$9, (%rsi)
	movq	%rdi, %rax
	je	.L118
	leal	-1(%rdx), %ecx
	cmpl	$998, %ecx
	ja	.L117
	movl	8(%rsi), %ecx
	movl	$4, (%rdi)
	movw	%dx, 14(%rdi)
	movl	%ecx, 8(%rdi)
	movl	$-1, %ecx
	movw	%cx, 12(%rdi)
	movl	$4, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$9, (%rdi)
	movl	$65810, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	movq	16(%rsi), %rdx
	movdqu	(%rsi), %xmm0
	movq	%rdx, 16(%rdi)
	movl	24(%rsi), %edx
	movups	%xmm0, (%rdi)
	movl	%edx, 24(%rdi)
	ret
	.cfi_endproc
.LFE2857:
	.size	_ZNK6icu_676number17FractionPrecision13withMaxDigitsEi, .-_ZNK6icu_676number17FractionPrecision13withMaxDigitsEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number9Precision12withCurrencyERKNS_12CurrencyUnitER10UErrorCode
	.type	_ZNK6icu_676number9Precision12withCurrencyERKNS_12CurrencyUnitER10UErrorCode, @function
_ZNK6icu_676number9Precision12withCurrencyERKNS_12CurrencyUnitER10UErrorCode:
.LFB2858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$9, (%rsi)
	jne	.L120
	movdqu	(%rsi), %xmm1
	movdqu	16(%rsi), %xmm2
	movups	%xmm1, (%rdi)
	movups	%xmm2, 16(%rdi)
.L119:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L134
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	leaq	20(%rdx), %r13
	movl	8(%rsi), %esi
	movq	%rcx, %rdx
	movq	%rcx, %r14
	movq	%r13, %rdi
	call	ucurr_getRoundingIncrementForUsage_67@PLT
	movl	8(%rbx), %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movsd	%xmm0, -88(%rbp)
	call	ucurr_getDefaultFractionDigitsForUsage_67@PLT
	movsd	-88(%rbp), %xmm0
	ucomisd	.LC1(%rip), %xmm0
	movl	%eax, %ebx
	jp	.L130
	jne	.L130
	movl	$2, (%r12)
	movw	%ax, 8(%r12)
	movw	%ax, 10(%r12)
	movl	$-1, 12(%r12)
	movl	$4, 24(%r12)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L130:
	subq	$8, %rsp
	leaq	-68(%rbp), %rax
	leaq	-64(%rbp), %rdx
	xorl	%esi, %esi
	pushq	%rax
	movl	$18, %ecx
	xorl	%edi, %edi
	leaq	-72(%rbp), %r9
	leaq	-73(%rbp), %r8
	movsd	%xmm0, -88(%rbp)
	call	_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_@PLT
	movl	-72(%rbp), %eax
	popq	%rdx
	movsd	-88(%rbp), %xmm0
	popq	%rcx
	cmpl	$1, %eax
	je	.L124
	subw	-68(%rbp), %ax
.L125:
	movl	$5, %edx
.L126:
	movl	%edx, (%r12)
	movw	%bx, 16(%r12)
	movw	%ax, 18(%r12)
	movl	$4, 24(%r12)
	movsd	%xmm0, 8(%r12)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L124:
	movzbl	-64(%rbp), %ecx
	movl	$1, %eax
	movl	$6, %edx
	subw	-68(%rbp), %ax
	leal	-48(%rcx), %esi
	cmpb	$49, %cl
	je	.L126
	cmpb	$5, %sil
	jne	.L125
	movl	$7, %edx
	jmp	.L126
.L134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2858:
	.size	_ZNK6icu_676number9Precision12withCurrencyERKNS_12CurrencyUnitER10UErrorCode, .-_ZNK6icu_676number9Precision12withCurrencyERKNS_12CurrencyUnitER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number17CurrencyPrecision12withCurrencyERKNS_12CurrencyUnitE
	.type	_ZNK6icu_676number17CurrencyPrecision12withCurrencyERKNS_12CurrencyUnitE, @function
_ZNK6icu_676number17CurrencyPrecision12withCurrencyERKNS_12CurrencyUnitE:
.LFB2859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$96, %rsp
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -104(%rbp)
	cmpl	$9, %r8d
	jne	.L136
	movdqu	(%rsi), %xmm1
	movdqu	16(%rsi), %xmm2
	movzwl	8(%rsi), %edi
	movzwl	10(%rsi), %eax
	movzwl	14(%rbx), %edx
	movzwl	12(%rsi), %esi
	movaps	%xmm1, -96(%rbp)
	movl	24(%rbx), %ecx
	movaps	%xmm2, -80(%rbp)
.L137:
	movl	%r8d, -96(%rbp)
	movw	%di, -88(%rbp)
	movw	%ax, -86(%rbp)
	movw	%si, -84(%rbp)
	movw	%dx, -82(%rbp)
	movdqa	-96(%rbp), %xmm3
	movl	%ecx, -72(%rbp)
	movdqa	-80(%rbp), %xmm4
	movups	%xmm3, (%r12)
	movups	%xmm4, 16(%r12)
.L135:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	leaq	20(%rdx), %r13
	movl	8(%rsi), %esi
	leaq	-104(%rbp), %r14
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	ucurr_getRoundingIncrementForUsage_67@PLT
	movl	8(%rbx), %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movsd	%xmm0, -120(%rbp)
	call	ucurr_getDefaultFractionDigitsForUsage_67@PLT
	movsd	-120(%rbp), %xmm0
	ucomisd	.LC1(%rip), %xmm0
	movl	%eax, %ebx
	jp	.L149
	jne	.L149
	movl	%eax, %edi
	movl	$-1, %edx
	movl	$-1, %esi
	movl	$2, %r8d
.L143:
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L148
	movl	$9, (%r12)
	movl	%ecx, 8(%r12)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L149:
	subq	$8, %rsp
	leaq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	xorl	%esi, %esi
	pushq	%rax
	movl	$18, %ecx
	xorl	%edi, %edi
	leaq	-100(%rbp), %r9
	leaq	-105(%rbp), %r8
	movsd	%xmm0, -120(%rbp)
	call	_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_@PLT
	movl	-100(%rbp), %eax
	popq	%rdx
	movsd	-120(%rbp), %xmm0
	popq	%rcx
	cmpl	$1, %eax
	je	.L140
	subw	-96(%rbp), %ax
.L141:
	movl	$5, %r8d
.L142:
	movq	%xmm0, %rsi
	movq	%xmm0, %rdx
	movw	%ax, -78(%rbp)
	movd	%xmm0, %eax
	salq	$16, %rsi
	movl	%edx, %edi
	movl	%r8d, -96(%rbp)
	sarl	$16, %eax
	movw	%bx, -80(%rbp)
	shrq	$48, %rsi
	shrq	$48, %rdx
	movl	$4, -72(%rbp)
	movsd	%xmm0, -88(%rbp)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L140:
	movzbl	-64(%rbp), %edx
	movl	$1, %eax
	movl	$6, %r8d
	subw	-96(%rbp), %ax
	leal	-48(%rdx), %ecx
	cmpb	$49, %dl
	je	.L142
	cmpb	$5, %cl
	jne	.L141
	movl	$7, %r8d
	jmp	.L142
.L153:
	call	__stack_chk_fail@PLT
.L148:
	movl	$4, %ecx
	jmp	.L137
	.cfi_endproc
.LFE2859:
	.size	_ZNK6icu_676number17CurrencyPrecision12withCurrencyERKNS_12CurrencyUnitE, .-_ZNK6icu_676number17CurrencyPrecision12withCurrencyERKNS_12CurrencyUnitE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number18IncrementPrecision15withMinFractionEi
	.type	_ZNK6icu_676number18IncrementPrecision15withMinFractionEi, @function
_ZNK6icu_676number18IncrementPrecision15withMinFractionEi:
.LFB2860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rcx
	movq	%rcx, -24(%rbp)
	xorl	%ecx, %ecx
	cmpl	$9, (%rsi)
	je	.L166
	movl	%edx, %ebx
	cmpl	$999, %edx
	jbe	.L167
	movl	$9, (%rdi)
	movl	$65810, 8(%rdi)
.L154:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L168
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	subq	$8, %rsp
	leaq	-52(%rbp), %rcx
	movq	%rdi, -80(%rbp)
	leaq	-48(%rbp), %rdx
	movsd	8(%rsi), %xmm0
	pushq	%rcx
	xorl	%esi, %esi
	movl	$18, %ecx
	xorl	%edi, %edi
	leaq	-56(%rbp), %r9
	leaq	-57(%rbp), %r8
	movsd	%xmm0, -72(%rbp)
	call	_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_@PLT
	movl	-56(%rbp), %edx
	popq	%rax
	movsd	-72(%rbp), %xmm0
	movq	-80(%rbp), %rax
	cmpl	$1, %edx
	popq	%rcx
	je	.L158
	subw	-52(%rbp), %dx
.L159:
	movl	$5, %ecx
.L160:
	movl	%ecx, (%rax)
	movw	%bx, 16(%rax)
	movw	%dx, 18(%rax)
	movl	$4, 24(%rax)
	movsd	%xmm0, 8(%rax)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L166:
	movq	16(%rsi), %rdx
	movdqu	(%rsi), %xmm1
	movq	%rdx, 16(%rdi)
	movl	24(%rsi), %edx
	movups	%xmm1, (%rdi)
	movl	%edx, 24(%rdi)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L158:
	movzbl	-48(%rbp), %esi
	movl	$1, %edx
	movl	$6, %ecx
	subw	-52(%rbp), %dx
	leal	-48(%rsi), %edi
	cmpb	$49, %sil
	je	.L160
	cmpb	$5, %dil
	jne	.L159
	movl	$7, %ecx
	jmp	.L160
.L168:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2860:
	.size	_ZNK6icu_676number18IncrementPrecision15withMinFractionEi, .-_ZNK6icu_676number18IncrementPrecision15withMinFractionEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision17constructFractionEii
	.type	_ZN6icu_676number9Precision17constructFractionEii, @function
_ZN6icu_676number9Precision17constructFractionEii:
.LFB2861:
	.cfi_startproc
	endbr64
	movl	$2, (%rdi)
	movq	%rdi, %rax
	movw	%si, 8(%rdi)
	movw	%dx, 10(%rdi)
	movl	$-1, 12(%rdi)
	movl	$4, 24(%rdi)
	ret
	.cfi_endproc
.LFE2861:
	.size	_ZN6icu_676number9Precision17constructFractionEii, .-_ZN6icu_676number9Precision17constructFractionEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision20constructSignificantEii
	.type	_ZN6icu_676number9Precision20constructSignificantEii, @function
_ZN6icu_676number9Precision20constructSignificantEii:
.LFB2865:
	.cfi_startproc
	endbr64
	movl	$3, (%rdi)
	movq	%rdi, %rax
	movl	$-1, 8(%rdi)
	movw	%si, 12(%rdi)
	movw	%dx, 14(%rdi)
	movl	$4, 24(%rdi)
	ret
	.cfi_endproc
.LFE2865:
	.size	_ZN6icu_676number9Precision20constructSignificantEii, .-_ZN6icu_676number9Precision20constructSignificantEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision28constructFractionSignificantERKNS0_17FractionPrecisionEii
	.type	_ZN6icu_676number9Precision28constructFractionSignificantERKNS0_17FractionPrecisionEii, @function
_ZN6icu_676number9Precision28constructFractionSignificantERKNS0_17FractionPrecisionEii:
.LFB2866:
	.cfi_startproc
	endbr64
	movl	8(%rsi), %esi
	movl	$4, (%rdi)
	movq	%rdi, %rax
	movw	%dx, 12(%rdi)
	movl	%esi, 8(%rdi)
	movw	%cx, 14(%rdi)
	movl	$4, 24(%rdi)
	ret
	.cfi_endproc
.LFE2866:
	.size	_ZN6icu_676number9Precision28constructFractionSignificantERKNS0_17FractionPrecisionEii, .-_ZN6icu_676number9Precision28constructFractionSignificantERKNS0_17FractionPrecisionEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision18constructIncrementEdi
	.type	_ZN6icu_676number9Precision18constructIncrementEdi, @function
_ZN6icu_676number9Precision18constructIncrementEdi:
.LFB2867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$18, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	xorl	%edi, %edi
	pushq	%rbx
	leaq	-48(%rbp), %rdx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	leaq	-56(%rbp), %r9
	xorl	%esi, %esi
	leaq	-57(%rbp), %r8
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	-52(%rbp), %rax
	movsd	%xmm0, -72(%rbp)
	pushq	%rax
	call	_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_@PLT
	movl	-56(%rbp), %eax
	popq	%rdx
	movsd	-72(%rbp), %xmm0
	popq	%rcx
	cmpl	$1, %eax
	je	.L173
	subw	-52(%rbp), %ax
.L174:
	movl	$5, (%r12)
	movw	%bx, 16(%r12)
	movw	%ax, 18(%r12)
	movl	$4, 24(%r12)
	movsd	%xmm0, 8(%r12)
.L172:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movzbl	-48(%rbp), %edx
	movl	$1, %eax
	subw	-52(%rbp), %ax
	leal	-48(%rdx), %ecx
	cmpb	$49, %dl
	jne	.L175
	movl	$6, (%r12)
	movw	%bx, 16(%r12)
	movw	%ax, 18(%r12)
	movl	$4, 24(%r12)
	movsd	%xmm0, 8(%r12)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L175:
	cmpb	$5, %cl
	jne	.L174
	movl	$7, (%r12)
	movw	%bx, 16(%r12)
	movw	%ax, 18(%r12)
	movl	$4, 24(%r12)
	movsd	%xmm0, 8(%r12)
	jmp	.L172
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2867:
	.size	_ZN6icu_676number9Precision18constructIncrementEdi, .-_ZN6icu_676number9Precision18constructIncrementEdi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number9Precision17constructCurrencyE14UCurrencyUsage
	.type	_ZN6icu_676number9Precision17constructCurrencyE14UCurrencyUsage, @function
_ZN6icu_676number9Precision17constructCurrencyE14UCurrencyUsage:
.LFB3979:
	.cfi_startproc
	endbr64
	movl	$8, (%rdi)
	movq	%rdi, %rax
	movl	%esi, 8(%rdi)
	movl	$4, 24(%rdi)
	ret
	.cfi_endproc
.LFE3979:
	.size	_ZN6icu_676number9Precision17constructCurrencyE14UCurrencyUsage, .-_ZN6icu_676number9Precision17constructCurrencyE14UCurrencyUsage
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl12RoundingImplC2ERKNS0_9PrecisionE25UNumberFormatRoundingModeRKNS_12CurrencyUnitER10UErrorCode
	.type	_ZN6icu_676number4impl12RoundingImplC2ERKNS0_9PrecisionE25UNumberFormatRoundingModeRKNS_12CurrencyUnitER10UErrorCode, @function
_ZN6icu_676number4impl12RoundingImplC2ERKNS0_9PrecisionE25UNumberFormatRoundingModeRKNS_12CurrencyUnitER10UErrorCode:
.LFB2876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movdqu	(%rsi), %xmm1
	movdqu	16(%rsi), %xmm2
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$8, (%rsi)
	movl	%edx, 32(%rdi)
	movb	$0, 36(%rdi)
	movups	%xmm1, (%rdi)
	movups	%xmm2, 16(%rdi)
	je	.L196
.L181:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	leaq	20(%rcx), %r13
	movq	%rsi, %r12
	movl	8(%rsi), %esi
	movq	%r8, %rdx
	movq	%rdi, %rbx
	movq	%r13, %rdi
	movq	%r8, %r14
	call	ucurr_getRoundingIncrementForUsage_67@PLT
	movl	8(%r12), %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movsd	%xmm0, -120(%rbp)
	call	ucurr_getDefaultFractionDigitsForUsage_67@PLT
	movsd	-120(%rbp), %xmm0
	ucomisd	.LC1(%rip), %xmm0
	movl	%eax, %r12d
	jp	.L192
	jne	.L192
	movl	%eax, %ecx
	movl	$-1, %edx
	movl	$-1, %eax
	movl	$2, %esi
.L188:
	movw	%ax, -84(%rbp)
	movq	-80(%rbp), %rax
	movl	%esi, -96(%rbp)
	movw	%cx, -88(%rbp)
	movw	%r12w, -86(%rbp)
	movw	%dx, -82(%rbp)
	movdqa	-96(%rbp), %xmm3
	movl	$4, -72(%rbp)
	movq	%rax, 16(%rbx)
	movl	$4, 24(%rbx)
	movups	%xmm3, (%rbx)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L192:
	subq	$8, %rsp
	leaq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	xorl	%esi, %esi
	pushq	%rax
	movl	$18, %ecx
	xorl	%edi, %edi
	leaq	-100(%rbp), %r9
	leaq	-101(%rbp), %r8
	movsd	%xmm0, -120(%rbp)
	call	_ZN6icu_6717double_conversion23DoubleToStringConverter13DoubleToAsciiEdNS1_8DtoaModeEiPciPbPiS5_@PLT
	movl	-100(%rbp), %eax
	popq	%rdx
	movsd	-120(%rbp), %xmm0
	popq	%rcx
	cmpl	$1, %eax
	je	.L185
	subw	-96(%rbp), %ax
.L186:
	movl	$5, %esi
.L187:
	movw	%ax, -78(%rbp)
	movq	%xmm0, %rax
	movq	%xmm0, %rdx
	movw	%r12w, -80(%rbp)
	salq	$16, %rax
	movd	%xmm0, %r12d
	movl	%edx, %ecx
	sarl	$16, %r12d
	shrq	$48, %rax
	shrq	$48, %rdx
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L185:
	movzbl	-64(%rbp), %edx
	movl	$1, %eax
	movl	$6, %esi
	subw	-96(%rbp), %ax
	leal	-48(%rdx), %ecx
	cmpb	$49, %dl
	je	.L187
	cmpb	$5, %cl
	jne	.L186
	movl	$7, %esi
	jmp	.L187
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2876:
	.size	_ZN6icu_676number4impl12RoundingImplC2ERKNS0_9PrecisionE25UNumberFormatRoundingModeRKNS_12CurrencyUnitER10UErrorCode, .-_ZN6icu_676number4impl12RoundingImplC2ERKNS0_9PrecisionE25UNumberFormatRoundingModeRKNS_12CurrencyUnitER10UErrorCode
	.globl	_ZN6icu_676number4impl12RoundingImplC1ERKNS0_9PrecisionE25UNumberFormatRoundingModeRKNS_12CurrencyUnitER10UErrorCode
	.set	_ZN6icu_676number4impl12RoundingImplC1ERKNS0_9PrecisionE25UNumberFormatRoundingModeRKNS_12CurrencyUnitER10UErrorCode,_ZN6icu_676number4impl12RoundingImplC2ERKNS0_9PrecisionE25UNumberFormatRoundingModeRKNS_12CurrencyUnitER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl12RoundingImpl11passThroughEv
	.type	_ZN6icu_676number4impl12RoundingImpl11passThroughEv, @function
_ZN6icu_676number4impl12RoundingImpl11passThroughEv:
.LFB2878:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$0, (%rdi)
	movq	%rdi, %rax
	movups	%xmm0, 24(%rdi)
	movb	$1, 36(%rdi)
	ret
	.cfi_endproc
.LFE2878:
	.size	_ZN6icu_676number4impl12RoundingImpl11passThroughEv, .-_ZN6icu_676number4impl12RoundingImpl11passThroughEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl12RoundingImpl19isSignificantDigitsEv
	.type	_ZNK6icu_676number4impl12RoundingImpl19isSignificantDigitsEv, @function
_ZNK6icu_676number4impl12RoundingImpl19isSignificantDigitsEv:
.LFB2882:
	.cfi_startproc
	endbr64
	cmpl	$3, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE2882:
	.size	_ZNK6icu_676number4impl12RoundingImpl19isSignificantDigitsEv, .-_ZNK6icu_676number4impl12RoundingImpl19isSignificantDigitsEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl12RoundingImpl24chooseMultiplierAndApplyERNS1_15DecimalQuantityERKNS1_18MultiplierProducerER10UErrorCode
	.type	_ZN6icu_676number4impl12RoundingImpl24chooseMultiplierAndApplyERNS1_15DecimalQuantityERKNS1_18MultiplierProducerER10UErrorCode, @function
_ZN6icu_676number4impl12RoundingImpl24chooseMultiplierAndApplyERNS1_15DecimalQuantityERKNS1_18MultiplierProducerER10UErrorCode:
.LFB2883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rsi, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -56(%rbp)
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	movq	(%r14), %rax
	movl	%ebx, %esi
	call	*16(%rax)
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	%eax, %r13d
	call	_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi@PLT
	cmpb	$0, 36(%r15)
	jne	.L201
	movq	-56(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode.part.0
.L201:
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	testb	%al, %al
	jne	.L200
	movq	-56(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L206
.L200:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	movl	%eax, %r8d
	leal	(%rbx,%r13), %eax
	cmpl	%eax, %r8d
	je	.L200
	movq	(%r14), %rax
	leal	1(%rbx), %esi
	movq	%r14, %rdi
	call	*16(%rax)
	movl	%eax, %ebx
	cmpl	%eax, %r13d
	je	.L200
	movl	%eax, %esi
	movq	%r12, %rdi
	subl	%r13d, %esi
	movl	%ebx, %r13d
	call	_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi@PLT
	cmpb	$0, 36(%r15)
	jne	.L200
	movq	-56(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode.part.0
	jmp	.L200
	.cfi_endproc
.LFE2883:
	.size	_ZN6icu_676number4impl12RoundingImpl24chooseMultiplierAndApplyERNS1_15DecimalQuantityERKNS1_18MultiplierProducerER10UErrorCode, .-_ZN6icu_676number4impl12RoundingImpl24chooseMultiplierAndApplyERNS1_15DecimalQuantityERKNS1_18MultiplierProducerER10UErrorCode
	.section	.text.unlikely
	.align 2
.LCOLDB2:
	.text
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode
	.type	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode, @function
_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode:
.LFB2884:
	.cfi_startproc
	endbr64
	cmpb	$0, 36(%rdi)
	jne	.L244
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$9, (%rdi)
	ja	.L209
	movl	(%rdi), %eax
	leaq	.L211(%rip), %rcx
	movq	%rsi, %r12
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L211:
	.long	.L210-.L211
	.long	.L218-.L211
	.long	.L217-.L211
	.long	.L216-.L211
	.long	.L215-.L211
	.long	.L214-.L211
	.long	.L213-.L211
	.long	.L212-.L211
	.long	.L209-.L211
	.long	.L210-.L211
	.text
	.p2align 4,,10
	.p2align 3
.L210:
	movl	$5, (%rdx)
.L207:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%rsi, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity15roundToInfinityEv@PLT
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movswl	10(%rdi), %esi
	movl	32(%rdi), %r8d
	cmpl	$-1, %esi
	je	.L232
	negl	%esi
.L220:
	movq	%rdx, %rcx
	movq	%r12, %rdi
	movl	%r8d, %edx
	call	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode@PLT
	movswl	8(%rbx), %esi
	movl	$-2147483647, %eax
	testl	%esi, %esi
	cmove	%eax, %esi
	xorl	%edi, %edi
	call	uprv_max_67@PLT
	movl	%eax, %esi
.L247:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity14setMinFractionEi@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movswl	14(%rdi), %r13d
	movl	32(%rdi), %r14d
	cmpl	$-1, %r13d
	je	.L233
	movq	%rsi, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	movq	-56(%rbp), %rdx
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L249
.L223:
	subl	%r13d, %eax
	leal	1(%rax), %esi
.L222:
	movq	%rdx, %rcx
	movq	%r12, %rdi
	movl	%r14d, %edx
	call	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode@PLT
	movq	%r12, %rdi
	movswl	12(%rbx), %r13d
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L250
.L224:
	subl	%r13d, %eax
	xorl	%edi, %edi
	notl	%eax
	movl	%eax, %esi
	call	uprv_max_67@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN6icu_676number4impl15DecimalQuantity14setMinFractionEi@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	testb	%al, %al
	je	.L207
	cmpw	$0, 12(%rbx)
	jle	.L207
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity13setMinIntegerEi@PLT
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movswl	8(%rdi), %r14d
	movswl	10(%rdi), %r13d
	movl	$-2147483647, %eax
	testl	%r14d, %r14d
	cmove	%eax, %r14d
	cmpl	$-1, %r13d
	je	.L236
	negl	%r13d
.L226:
	movswl	12(%rbx), %r15d
	cmpw	$-1, %r15w
	je	.L251
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	movq	-56(%rbp), %rdx
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L252
.L231:
	subl	%r15d, %eax
	movl	%r13d, %edi
	movq	%rdx, -56(%rbp)
	leal	1(%rax), %esi
	call	uprv_min_67@PLT
	movq	-56(%rbp), %rdx
	movl	%eax, %esi
.L230:
	movl	32(%rbx), %r8d
	movq	%rdx, %rcx
	movq	%r12, %rdi
	movl	%r8d, %edx
	call	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode@PLT
	movl	%r14d, %esi
	xorl	%edi, %edi
	call	uprv_max_67@PLT
	movl	%eax, %esi
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L214:
	movl	32(%rdi), %esi
	movsd	8(%rdi), %xmm0
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity16roundToIncrementEd25UNumberFormatRoundingModeR10UErrorCode@PLT
.L248:
	movswl	16(%rbx), %esi
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L213:
	movl	32(%rdi), %r8d
	movswl	18(%rdi), %esi
	movq	%rdx, %rcx
	movq	%r12, %rdi
	negl	%esi
	movl	%r8d, %edx
	call	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode@PLT
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L212:
	movl	32(%rdi), %r8d
	movswl	18(%rdi), %esi
	movq	%rdx, %rcx
	movq	%r12, %rdi
	negl	%esi
	movl	%r8d, %edx
	call	_ZN6icu_676number4impl15DecimalQuantity13roundToNickelEi25UNumberFormatRoundingModeR10UErrorCode@PLT
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L251:
	movswl	14(%rbx), %r15d
	cmpl	$-1, %r15d
	je	.L237
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	movq	-56(%rbp), %rdx
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L253
.L229:
	subl	%r15d, %eax
	leal	1(%rax), %esi
.L228:
	movl	%r13d, %edi
	movq	%rdx, -56(%rbp)
	call	uprv_max_67@PLT
	movq	-56(%rbp), %rdx
	movl	%eax, %esi
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L250:
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L249:
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L223
.L253:
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L229
.L233:
	movl	$-2147483648, %esi
	jmp	.L222
.L232:
	movl	$-2147483648, %esi
	jmp	.L220
.L236:
	movl	$-2147483648, %r13d
	jmp	.L226
.L237:
	movl	$-2147483648, %esi
	jmp	.L228
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode.cold, @function
_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode.cold:
.LFSB2884:
.L209:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE2884:
	.text
	.size	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode, .-_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode
	.section	.text.unlikely
	.size	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode.cold, .-_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode.cold
.LCOLDE2:
	.text
.LHOTE2:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityEi10UErrorCode
	.type	_ZN6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityEi10UErrorCode, @function
_ZN6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityEi10UErrorCode:
.LFB2885:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movswl	12(%r8), %esi
	subl	%edx, %esi
	jmp	_ZN6icu_676number4impl15DecimalQuantity14setMinFractionEi@PLT
	.cfi_endproc
.LFE2885:
	.size	_ZN6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityEi10UErrorCode, .-_ZN6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityEi10UErrorCode
	.weak	_ZTSN6icu_676number4impl18MultiplierProducerE
	.section	.rodata._ZTSN6icu_676number4impl18MultiplierProducerE,"aG",@progbits,_ZTSN6icu_676number4impl18MultiplierProducerE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl18MultiplierProducerE, @object
	.size	_ZTSN6icu_676number4impl18MultiplierProducerE, 42
_ZTSN6icu_676number4impl18MultiplierProducerE:
	.string	"N6icu_676number4impl18MultiplierProducerE"
	.weak	_ZTIN6icu_676number4impl18MultiplierProducerE
	.section	.data.rel.ro._ZTIN6icu_676number4impl18MultiplierProducerE,"awG",@progbits,_ZTIN6icu_676number4impl18MultiplierProducerE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl18MultiplierProducerE, @object
	.size	_ZTIN6icu_676number4impl18MultiplierProducerE, 16
_ZTIN6icu_676number4impl18MultiplierProducerE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl18MultiplierProducerE
	.weak	_ZTVN6icu_676number4impl18MultiplierProducerE
	.section	.data.rel.ro._ZTVN6icu_676number4impl18MultiplierProducerE,"awG",@progbits,_ZTVN6icu_676number4impl18MultiplierProducerE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl18MultiplierProducerE, @object
	.size	_ZTVN6icu_676number4impl18MultiplierProducerE, 40
_ZTVN6icu_676number4impl18MultiplierProducerE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl18MultiplierProducerE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
