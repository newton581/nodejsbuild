	.file	"udat.cpp"
	.text
	.p2align 4
	.globl	udat_toCalendarDateField_67
	.type	udat_toCalendarDateField_67, @function
udat_toCalendarDateField_67:
.LFB3275:
	.cfi_startproc
	endbr64
	movslq	%edi, %rdi
	leaq	_ZL17gDateFieldMapping(%rip), %rax
	movl	(%rax,%rdi,4), %eax
	ret
	.cfi_endproc
.LFE3275:
	.size	udat_toCalendarDateField_67, .-udat_toCalendarDateField_67
	.p2align 4
	.globl	udat_registerOpener_67
	.type	udat_registerOpener_67, @function
udat_registerOpener_67:
.LFB3276:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L10
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	xorl	%edi, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	umtx_lock_67@PLT
	cmpq	$0, _ZL7gOpener(%rip)
	je	.L11
	movl	$1, (%rbx)
	xorl	%edi, %edi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	popq	%rbx
	.cfi_restore 3
	movq	%r12, _ZL7gOpener(%rip)
	xorl	%edi, %edi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.cfi_endproc
.LFE3276:
	.size	udat_registerOpener_67, .-udat_registerOpener_67
	.p2align 4
	.globl	udat_unregisterOpener_67
	.type	udat_unregisterOpener_67, @function
udat_unregisterOpener_67:
.LFB3277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L12
	movq	%rdi, %r13
	xorl	%edi, %edi
	movq	%rsi, %rbx
	call	umtx_lock_67@PLT
	movq	_ZL7gOpener(%rip), %r12
	testq	%r12, %r12
	je	.L18
	cmpq	%r13, %r12
	je	.L14
.L18:
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
.L16:
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
.L12:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	$0, _ZL7gOpener(%rip)
	jmp	.L16
	.cfi_endproc
.LFE3277:
	.size	udat_unregisterOpener_67, .-udat_unregisterOpener_67
	.p2align 4
	.globl	udat_close_67
	.type	udat_close_67, @function
udat_close_67:
.LFB3279:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L21
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L21:
	ret
	.cfi_endproc
.LFE3279:
	.size	udat_close_67, .-udat_close_67
	.p2align 4
	.globl	udat_clone_67
	.type	udat_clone_67, @function
udat_clone_67:
.LFB3280:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L27
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*32(%rax)
	testq	%rax, %rax
	je	.L30
.L23:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L23
	.cfi_endproc
.LFE3280:
	.size	udat_clone_67, .-udat_clone_67
	.p2align 4
	.globl	udat_format_67
	.type	udat_format_67, @function
udat_format_67:
.LFB3281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L41
	movq	%rdi, %r9
	movq	%rsi, %rbx
	movl	%edx, %r12d
	movq	%rcx, %r13
	testq	%rsi, %rsi
	je	.L44
	testl	%edx, %edx
	js	.L34
	movl	$2, %edx
	leaq	-128(%rbp), %r15
	movl	%r12d, %ecx
	movq	%rdi, -184(%rbp)
	movw	%dx, -120(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r8, -200(%rbp)
	movsd	%xmm0, -192(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movq	-184(%rbp), %r9
	movsd	-192(%rbp), %xmm0
	movq	-200(%rbp), %r8
.L39:
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	%r8, -184(%rbp)
	movq	%rax, -160(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -152(%rbp)
	movl	$0, -144(%rbp)
	testq	%r13, %r13
	je	.L37
	movl	0(%r13), %eax
	leaq	-160(%rbp), %r14
	movq	%r15, %rsi
	movq	%r9, %rdi
	movq	%r14, %rdx
	movl	%eax, -152(%rbp)
	call	_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE@PLT
	movq	-148(%rbp), %rax
	movq	-184(%rbp), %r8
	movq	%rax, 4(%r13)
.L38:
	movl	%r12d, %edx
	leaq	-168(%rbp), %rsi
	movq	%r8, %rcx
	movq	%r15, %rdi
	movq	%rbx, -168(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	%r14, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L31:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$168, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	testl	%edx, %edx
	jne	.L34
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r15
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	-160(%rbp), %r14
	movq	%r15, %rsi
	movq	%r9, %rdi
	movq	%r14, %rdx
	call	_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE@PLT
	movq	-184(%rbp), %r8
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$-1, %r12d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$1, (%r8)
	movl	$-1, %r12d
	jmp	.L31
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3281:
	.size	udat_format_67, .-udat_format_67
	.p2align 4
	.globl	udat_formatCalendar_67
	.type	udat_formatCalendar_67, @function
udat_formatCalendar_67:
.LFB3282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L56
	movq	%rdi, %r15
	movq	%rsi, %r10
	movq	%rdx, %rbx
	movl	%ecx, %r12d
	movq	%r8, %r13
	testq	%rdx, %rdx
	je	.L59
	testl	%ecx, %ecx
	js	.L49
	movl	$2, %edx
	leaq	-128(%rbp), %r14
	movq	%rsi, -184(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -120(%rbp)
	movq	%rbx, %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r9, -192(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movq	-184(%rbp), %r10
	movq	-192(%rbp), %r9
.L54:
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	%r9, -192(%rbp)
	movq	%rax, -160(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -152(%rbp)
	movq	(%r15), %rax
	movl	$0, -144(%rbp)
	movq	64(%rax), %rax
	testq	%r13, %r13
	je	.L52
	movl	0(%r13), %edx
	leaq	-160(%rbp), %r8
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	%r8, -184(%rbp)
	movq	%r8, %rcx
	movl	%edx, -152(%rbp)
	movq	%r14, %rdx
	call	*%rax
	movq	-148(%rbp), %rax
	movq	-192(%rbp), %r9
	movq	-184(%rbp), %r8
	movq	%rax, 4(%r13)
.L53:
	movl	%r12d, %edx
	leaq	-168(%rbp), %rsi
	movq	%r9, %rcx
	movq	%r14, %rdi
	movq	%r8, -184(%rbp)
	movq	%rbx, -168(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	-184(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L46:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	addq	$152, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	testl	%ecx, %ecx
	jne	.L49
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r14
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	-160(%rbp), %r8
	movq	%r14, %rdx
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	%r8, -184(%rbp)
	movq	%r8, %rcx
	call	*%rax
	movq	-184(%rbp), %r8
	movq	-192(%rbp), %r9
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$-1, %r12d
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$1, (%r9)
	movl	$-1, %r12d
	jmp	.L46
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3282:
	.size	udat_formatCalendar_67, .-udat_formatCalendar_67
	.p2align 4
	.globl	udat_formatForFields_67
	.type	udat_formatForFields_67, @function
udat_formatForFields_67:
.LFB3283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L69
	movq	%rdi, %r15
	movq	%rsi, %rbx
	movl	%edx, %r13d
	movq	%rcx, %r9
	movq	%r8, %r12
	testq	%rsi, %rsi
	je	.L72
	testl	%edx, %edx
	js	.L64
	movl	$2, %edx
	leaq	-128(%rbp), %r14
	movq	%rcx, -160(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -120(%rbp)
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movsd	%xmm0, -152(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movsd	-152(%rbp), %xmm0
	movq	-160(%rbp), %r9
.L67:
	movq	%r9, %rdx
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode@PLT
	movq	%r12, %rcx
	movl	%r13d, %edx
	movq	%r14, %rdi
	leaq	-136(%rbp), %rsi
	movq	%rbx, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L61:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	testl	%edx, %edx
	jne	.L64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r14
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L69:
	movl	$-1, %r12d
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$1, (%r12)
	movl	$-1, %r12d
	jmp	.L61
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3283:
	.size	udat_formatForFields_67, .-udat_formatForFields_67
	.p2align 4
	.globl	udat_formatCalendarForFields_67
	.type	udat_formatCalendarForFields_67, @function
udat_formatCalendarForFields_67:
.LFB3284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L82
	movq	%rdi, %r14
	movq	%rsi, %r11
	movq	%rdx, %rbx
	movl	%ecx, %r13d
	movq	%r8, %r10
	movq	%r9, %r12
	testq	%rdx, %rdx
	je	.L85
	testl	%ecx, %ecx
	js	.L77
	movl	$2, %edx
	leaq	-128(%rbp), %r15
	movq	%rsi, -152(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -120(%rbp)
	movq	%rbx, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r8, -160(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movq	-152(%rbp), %r11
	movq	-160(%rbp), %r10
.L80:
	movq	(%r14), %rax
	movq	%r12, %r8
	movq	%r10, %rcx
	movq	%r11, %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	*72(%rax)
	movq	%r12, %rcx
	leaq	-136(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r15, %rdi
	movq	%rbx, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L74:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	testl	%ecx, %ecx
	jne	.L77
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r15
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$-1, %r12d
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$1, (%r12)
	movl	$-1, %r12d
	jmp	.L74
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3284:
	.size	udat_formatCalendarForFields_67, .-udat_formatCalendarForFields_67
	.p2align 4
	.globl	udat_parse_67
	.type	udat_parse_67, @function
udat_parse_67:
.LFB3285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L97
.L87:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movq	%rsi, -144(%rbp)
	leaq	-128(%rbp), %r14
	xorl	%esi, %esi
	cmpl	$-1, %edx
	leaq	-144(%rbp), %r15
	movq	%rdi, %r13
	movq	%rcx, %r12
	movq	%r14, %rdi
	movl	%edx, %ecx
	sete	%sil
	movq	%r15, %rdx
	movq	%r8, %rbx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movl	$-1, -132(%rbp)
	movq	%rax, -144(%rbp)
	movl	$0, -148(%rbp)
	testq	%r12, %r12
	je	.L94
	movl	(%r12), %eax
.L89:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, -136(%rbp)
	call	_ZNK6icu_6710DateFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionE@PLT
	movl	-132(%rbp), %eax
	cmpl	$-1, %eax
	je	.L99
	movl	%eax, (%r12)
	movl	$9, (%rbx)
.L91:
	movq	%r15, %rdi
	movsd	%xmm0, -168(%rbp)
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movsd	-168(%rbp), %xmm0
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L99:
	movl	-136(%rbp), %eax
	movl	%eax, (%r12)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L94:
	xorl	%eax, %eax
	leaq	-148(%rbp), %r12
	jmp	.L89
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3285:
	.size	udat_parse_67, .-udat_parse_67
	.p2align 4
	.globl	udat_parseCalendar_67
	.type	udat_parseCalendar_67, @function
udat_parseCalendar_67:
.LFB3286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	testl	%eax, %eax
	jle	.L109
.L100:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	%rsi, %r13
	movq	%r8, %r14
	xorl	%esi, %esi
	cmpl	$-1, %ecx
	leaq	-128(%rbp), %r15
	movq	%rdi, %r12
	sete	%sil
	movq	%r9, %rbx
	leaq	-144(%rbp), %r8
	movq	%rdx, -144(%rbp)
	movq	%r15, %rdi
	movq	%r8, %rdx
	movq	%r8, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	testq	%r14, %r14
	movl	$-1, -132(%rbp)
	movq	-168(%rbp), %r8
	movq	%rax, -144(%rbp)
	movl	$0, -148(%rbp)
	je	.L107
	movl	(%r14), %eax
.L103:
	movl	%eax, -136(%rbp)
	movq	(%r12), %rax
	movq	%r8, %rcx
	movq	%r13, %rdx
	movq	%r8, -168(%rbp)
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*88(%rax)
	movl	-132(%rbp), %eax
	movq	-168(%rbp), %r8
	cmpl	$-1, %eax
	je	.L111
	movl	%eax, (%r14)
	movl	$9, (%rbx)
.L105:
	movq	%r8, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L111:
	movl	-136(%rbp), %eax
	movl	%eax, (%r14)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L107:
	xorl	%eax, %eax
	leaq	-148(%rbp), %r14
	jmp	.L103
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3286:
	.size	udat_parseCalendar_67, .-udat_parseCalendar_67
	.p2align 4
	.globl	udat_isLenient_67
	.type	udat_isLenient_67, @function
udat_isLenient_67:
.LFB3287:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*96(%rax)
	.cfi_endproc
.LFE3287:
	.size	udat_isLenient_67, .-udat_isLenient_67
	.p2align 4
	.globl	udat_setLenient_67
	.type	udat_setLenient_67, @function
udat_setLenient_67:
.LFB3288:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movsbl	%sil, %esi
	jmp	*104(%rax)
	.cfi_endproc
.LFE3288:
	.size	udat_setLenient_67, .-udat_setLenient_67
	.p2align 4
	.globl	udat_getBooleanAttribute_67
	.type	udat_getBooleanAttribute_67, @function
udat_getBooleanAttribute_67:
.LFB3289:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L115
	movq	(%rdi), %rax
	jmp	*224(%rax)
	.p2align 4,,10
	.p2align 3
.L115:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3289:
	.size	udat_getBooleanAttribute_67, .-udat_getBooleanAttribute_67
	.p2align 4
	.globl	udat_setBooleanAttribute_67
	.type	udat_setBooleanAttribute_67, @function
udat_setBooleanAttribute_67:
.LFB3290:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L116
	movq	(%rdi), %rax
	movsbl	%dl, %edx
	jmp	*216(%rax)
	.p2align 4,,10
	.p2align 3
.L116:
	ret
	.cfi_endproc
.LFE3290:
	.size	udat_setBooleanAttribute_67, .-udat_setBooleanAttribute_67
	.p2align 4
	.globl	udat_getCalendar_67
	.type	udat_getCalendar_67, @function
udat_getCalendar_67:
.LFB3291:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*128(%rax)
	.cfi_endproc
.LFE3291:
	.size	udat_getCalendar_67, .-udat_getCalendar_67
	.p2align 4
	.globl	udat_setCalendar_67
	.type	udat_setCalendar_67, @function
udat_setCalendar_67:
.LFB3292:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*144(%rax)
	.cfi_endproc
.LFE3292:
	.size	udat_setCalendar_67, .-udat_setCalendar_67
	.p2align 4
	.globl	udat_getNumberFormatForField_67
	.type	udat_getNumberFormatForField_67, @function
udat_getNumberFormatForField_67:
.LFB3293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L121
	movl	%esi, %ebx
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L121
	movzwl	%bx, %esi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6716SimpleDateFormat23getNumberFormatForFieldEDs@PLT
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	152(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE3293:
	.size	udat_getNumberFormatForField_67, .-udat_getNumberFormatForField_67
	.p2align 4
	.globl	udat_getNumberFormat_67
	.type	udat_getNumberFormat_67, @function
udat_getNumberFormat_67:
.LFB3294:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*152(%rax)
	.cfi_endproc
.LFE3294:
	.size	udat_getNumberFormat_67, .-udat_getNumberFormat_67
	.p2align 4
	.globl	udat_adoptNumberFormatForFields_67
	.type	udat_adoptNumberFormatForFields_67, @function
udat_adoptNumberFormatForFields_67:
.LFB3295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L143
.L130:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L144
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	%rcx, %r12
	testq	%rdi, %rdi
	je	.L132
	movq	%rsi, %r14
	movq	%rdx, %r15
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L132
	testq	%r14, %r14
	je	.L130
	leaq	-128(%rbp), %rbx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rdi
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN6icu_6716SimpleDateFormat17adoptNumberFormatERKNS_13UnicodeStringEPNS_12NumberFormatER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L132:
	movl	$1, (%r12)
	jmp	.L130
.L144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3295:
	.size	udat_adoptNumberFormatForFields_67, .-udat_adoptNumberFormatForFields_67
	.p2align 4
	.globl	udat_setNumberFormat_67
	.type	udat_setNumberFormat_67, @function
udat_setNumberFormat_67:
.LFB3296:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*168(%rax)
	.cfi_endproc
.LFE3296:
	.size	udat_setNumberFormat_67, .-udat_setNumberFormat_67
	.p2align 4
	.globl	udat_adoptNumberFormat_67
	.type	udat_adoptNumberFormat_67, @function
udat_adoptNumberFormat_67:
.LFB3297:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*160(%rax)
	.cfi_endproc
.LFE3297:
	.size	udat_adoptNumberFormat_67, .-udat_adoptNumberFormat_67
	.p2align 4
	.globl	udat_getAvailable_67
	.type	udat_getAvailable_67, @function
udat_getAvailable_67:
.LFB3298:
	.cfi_startproc
	endbr64
	jmp	uloc_getAvailable_67@PLT
	.cfi_endproc
.LFE3298:
	.size	udat_getAvailable_67, .-udat_getAvailable_67
	.p2align 4
	.globl	udat_countAvailable_67
	.type	udat_countAvailable_67, @function
udat_countAvailable_67:
.LFB3299:
	.cfi_startproc
	endbr64
	jmp	uloc_countAvailable_67@PLT
	.cfi_endproc
.LFE3299:
	.size	udat_countAvailable_67, .-udat_countAvailable_67
	.p2align 4
	.globl	udat_get2DigitYearStart_67
	.type	udat_get2DigitYearStart_67, @function
udat_get2DigitYearStart_67:
.LFB3300:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L162
	pxor	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L151
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L151
	movsd	784(%rbx), %xmm0
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movl	$1, (%r12)
	pxor	%xmm0, %xmm0
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3300:
	.size	udat_get2DigitYearStart_67, .-udat_get2DigitYearStart_67
	.p2align 4
	.globl	udat_set2DigitYearStart_67
	.type	udat_set2DigitYearStart_67, @function
udat_set2DigitYearStart_67:
.LFB3301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	subq	$24, %rsp
	.cfi_offset 13, -24
	movl	(%rsi), %eax
	movsd	%xmm0, -24(%rbp)
	testl	%eax, %eax
	jle	.L172
	addq	$24, %rsp
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movq	%rsi, %r13
	testq	%rdi, %rdi
	je	.L165
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	movq	%rdi, -32(%rbp)
	call	__dynamic_cast@PLT
	movq	-32(%rbp), %rdi
	movsd	-24(%rbp), %xmm0
	testq	%rax, %rax
	je	.L165
	movq	(%rdi), %rax
	movq	%r13, %rsi
	movq	232(%rax), %rax
	addq	$24, %rsp
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movl	$1, 0(%r13)
	addq	$24, %rsp
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3301:
	.size	udat_set2DigitYearStart_67, .-udat_set2DigitYearStart_67
	.p2align 4
	.globl	udat_toPattern_67
	.type	udat_toPattern_67, @function
udat_toPattern_67:
.LFB3302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -148(%rbp)
	movl	(%r8), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jg	.L188
	movq	%rdi, %r15
	movq	%rdx, %rbx
	movl	%ecx, %r13d
	movq	%r8, %r12
	testq	%rdx, %rdx
	je	.L194
	testl	%ecx, %ecx
	js	.L176
	movl	$2, %edx
	leaq	-128(%rbp), %r14
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rbx, %rsi
	movw	%dx, -120(%rbp)
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	testq	%r15, %r15
	je	.L179
.L196:
	movq	%r15, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L180
	cmpb	$0, -148(%rbp)
	movq	(%rax), %rax
	je	.L181
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	*248(%rax)
.L182:
	movq	%r12, %rcx
	leaq	-136(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%rbx, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
.L184:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L173:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L195
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	cmpb	$0, -148(%rbp)
	jne	.L179
	movq	%r15, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6718RelativeDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L179
	movq	(%rax), %rax
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	*232(%rax)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L194:
	testl	%ecx, %ecx
	jne	.L176
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r14
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	testq	%r15, %r15
	jne	.L196
.L179:
	movl	$1, (%r12)
	movl	$-1, %r12d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%r14, %rsi
	call	*240(%rax)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$-1, %r12d
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$1, (%r12)
	movl	$-1, %r12d
	jmp	.L173
.L195:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3302:
	.size	udat_toPattern_67, .-udat_toPattern_67
	.p2align 4
	.globl	udat_applyPattern_67
	.type	udat_applyPattern_67, @function
udat_applyPattern_67:
.LFB3303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-120(%rbp), %r14
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	xorl	%esi, %esi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %ecx
	movq	%rdx, -120(%rbp)
	movq	%r14, %rdx
	sete	%sil
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$0, -120(%rbp)
	testq	%r12, %r12
	je	.L198
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	movq	%r12, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L198
	movq	(%r12), %rax
	testb	%bl, %bl
	jne	.L202
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*256(%rax)
.L201:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%r13, %rdi
	movl	$1, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L197:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L210
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*264(%rax)
	jmp	.L201
.L210:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3303:
	.size	udat_applyPattern_67, .-udat_applyPattern_67
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4
	.globl	udat_getSymbols_67
	.type	udat_getSymbols_67, @function
udat_getSymbols_67:
.LFB3304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	$-1, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L211
	xorl	%ecx, %ecx
	movl	%esi, %ebx
	movl	%edx, %r12d
	movq	%rdi, -152(%rbp)
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	movq	%r9, %r15
	movl	%r8d, %r14d
	call	__dynamic_cast@PLT
	movq	-152(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L213
	movq	(%rax), %rax
	movq	%r9, %rdi
	call	*272(%rax)
	movq	%rax, %r13
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L213:
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6718RelativeDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L211
	movq	(%rax), %rax
	call	*264(%rax)
	movq	%rax, %r13
.L214:
	movl	$0, -140(%rbp)
	cmpl	$27, %ebx
	ja	.L250
	leaq	.L217(%rip), %rax
	movslq	(%rax,%rbx,4), %rdx
	addq	%rax, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L217:
	.long	.L244-.L217
	.long	.L243-.L217
	.long	.L242-.L217
	.long	.L241-.L217
	.long	.L240-.L217
	.long	.L239-.L217
	.long	.L238-.L217
	.long	.L237-.L217
	.long	.L236-.L217
	.long	.L235-.L217
	.long	.L234-.L217
	.long	.L233-.L217
	.long	.L232-.L217
	.long	.L231-.L217
	.long	.L230-.L217
	.long	.L229-.L217
	.long	.L228-.L217
	.long	.L227-.L217
	.long	.L226-.L217
	.long	.L225-.L217
	.long	.L224-.L217
	.long	.L223-.L217
	.long	.L222-.L217
	.long	.L221-.L217
	.long	.L220-.L217
	.long	.L219-.L217
	.long	.L218-.L217
	.long	.L216-.L217
	.text
	.p2align 4,,10
	.p2align 3
.L218:
	xorl	%edx, %edx
	leaq	-140(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols14getZodiacNamesERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	.p2align 4,,10
	.p2align 3
.L215:
	xorl	%r13d, %r13d
	cmpl	%edx, %r12d
	jge	.L211
	movslq	%r12d, %rdi
	movq	-160(%rbp), %rbx
	movq	%r15, %rcx
	movl	%r14d, %edx
	salq	$6, %rdi
	leaq	-136(%rbp), %rsi
	leaq	(%rax,%rdi), %rdi
	movq	%rbx, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r13d
.L211:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L258
	addq	$120, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	xorl	%edx, %edx
	leaq	-140(%rbp), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols14getZodiacNamesERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L220:
	xorl	%edx, %edx
	leaq	-140(%rbp), %rsi
	movl	$2, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols12getYearNamesERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L221:
	xorl	%edx, %edx
	leaq	-140(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols12getYearNamesERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L222:
	xorl	%edx, %edx
	leaq	-140(%rbp), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols12getYearNamesERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L223:
	movl	$1, %edx
	leaq	-140(%rbp), %rsi
	movl	$3, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L224:
	xorl	%edx, %edx
	leaq	-140(%rbp), %rsi
	movl	$3, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L225:
	movl	$1, %edx
	leaq	-140(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols11getQuartersERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L226:
	movl	$1, %edx
	leaq	-140(%rbp), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols11getQuartersERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L227:
	xorl	%edx, %edx
	leaq	-140(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols11getQuartersERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L228:
	xorl	%edx, %edx
	leaq	-140(%rbp), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols11getQuartersERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$1, %edx
	leaq	-140(%rbp), %rsi
	movl	$2, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L230:
	movl	$1, %edx
	leaq	-140(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$1, %edx
	leaq	-140(%rbp), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L232:
	movl	$1, %edx
	leaq	-140(%rbp), %rsi
	movl	$2, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols9getMonthsERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L233:
	movl	$1, %edx
	leaq	-140(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols9getMonthsERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$1, %edx
	leaq	-140(%rbp), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols9getMonthsERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L235:
	xorl	%edx, %edx
	leaq	-140(%rbp), %rsi
	movl	$2, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L236:
	xorl	%edx, %edx
	leaq	-140(%rbp), %rsi
	movl	$2, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols9getMonthsERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L237:
	leaq	-140(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols11getEraNamesERi@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L238:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	cmpq	$0, -160(%rbp)
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	jne	.L252
	leaq	-128(%rbp), %r12
	testl	%r14d, %r14d
	jne	.L252
.L245:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols20getLocalPatternCharsERNS_13UnicodeStringE@PLT
	movq	%r15, %rcx
	movl	%r14d, %edx
	movq	%r12, %rdi
	movq	-160(%rbp), %rax
	leaq	-136(%rbp), %rsi
	movq	%rax, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r13d
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L239:
	leaq	-140(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols14getAmPmStringsERi@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L240:
	leaq	-140(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols16getShortWeekdaysERi@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L241:
	leaq	-140(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERi@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L242:
	leaq	-140(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols14getShortMonthsERi@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L243:
	leaq	-140(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols9getMonthsERi@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L244:
	leaq	-140(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols7getErasERi@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L216:
	xorl	%edx, %edx
	leaq	-140(%rbp), %rsi
	movl	$2, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6717DateFormatSymbols14getZodiacNamesERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-140(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L252:
	leaq	-128(%rbp), %r12
	movq	-160(%rbp), %rsi
	movl	%r14d, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	jmp	.L245
.L258:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	udat_getSymbols_67.cold, @function
udat_getSymbols_67.cold:
.LFSB3304:
.L250:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L215
	.cfi_endproc
.LFE3304:
	.text
	.size	udat_getSymbols_67, .-udat_getSymbols_67
	.section	.text.unlikely
	.size	udat_getSymbols_67.cold, .-udat_getSymbols_67.cold
.LCOLDE1:
	.text
.LHOTE1:
	.section	.text.unlikely
.LCOLDB2:
	.text
.LHOTB2:
	.p2align 4
	.globl	udat_countSymbols_67
	.type	udat_countSymbols_67, @function
udat_countSymbols_67:
.LFB3305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L259
	movl	%esi, %ebx
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	movq	%rdi, %r12
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L261
	movq	(%rax), %rax
	call	*272(%rax)
	movq	%rax, %rdi
.L262:
	movl	$0, -44(%rbp)
	cmpl	$27, %ebx
	ja	.L296
	leaq	.L265(%rip), %rax
	movslq	(%rax,%rbx,4), %rdx
	addq	%rax, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L265:
	.long	.L292-.L265
	.long	.L291-.L265
	.long	.L290-.L265
	.long	.L289-.L265
	.long	.L288-.L265
	.long	.L287-.L265
	.long	.L286-.L265
	.long	.L285-.L265
	.long	.L284-.L265
	.long	.L283-.L265
	.long	.L282-.L265
	.long	.L281-.L265
	.long	.L280-.L265
	.long	.L279-.L265
	.long	.L278-.L265
	.long	.L277-.L265
	.long	.L276-.L265
	.long	.L275-.L265
	.long	.L274-.L265
	.long	.L273-.L265
	.long	.L272-.L265
	.long	.L271-.L265
	.long	.L270-.L265
	.long	.L269-.L265
	.long	.L268-.L265
	.long	.L267-.L265
	.long	.L266-.L265
	.long	.L264-.L265
	.text
	.p2align 4,,10
	.p2align 3
.L286:
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L259:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L299
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6718RelativeDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L259
	movq	(%rax), %rax
	call	*264(%rax)
	movq	%rax, %rdi
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L285:
	leaq	-44(%rbp), %rsi
	call	_ZNK6icu_6717DateFormatSymbols11getEraNamesERi@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L284:
	leaq	-44(%rbp), %rsi
	movl	$2, %ecx
	xorl	%edx, %edx
	call	_ZNK6icu_6717DateFormatSymbols9getMonthsERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L283:
	leaq	-44(%rbp), %rsi
	movl	$2, %ecx
	xorl	%edx, %edx
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L282:
	leaq	-44(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	call	_ZNK6icu_6717DateFormatSymbols9getMonthsERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L281:
	leaq	-44(%rbp), %rsi
	xorl	%ecx, %ecx
	movl	$1, %edx
	call	_ZNK6icu_6717DateFormatSymbols9getMonthsERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L280:
	leaq	-44(%rbp), %rsi
	movl	$2, %ecx
	movl	$1, %edx
	call	_ZNK6icu_6717DateFormatSymbols9getMonthsERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	-44(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L278:
	leaq	-44(%rbp), %rsi
	xorl	%ecx, %ecx
	movl	$1, %edx
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L277:
	leaq	-44(%rbp), %rsi
	movl	$2, %ecx
	movl	$1, %edx
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L276:
	leaq	-44(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZNK6icu_6717DateFormatSymbols11getQuartersERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L275:
	leaq	-44(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZNK6icu_6717DateFormatSymbols11getQuartersERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L274:
	leaq	-44(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	call	_ZNK6icu_6717DateFormatSymbols11getQuartersERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L273:
	leaq	-44(%rbp), %rsi
	xorl	%ecx, %ecx
	movl	$1, %edx
	call	_ZNK6icu_6717DateFormatSymbols11getQuartersERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L272:
	leaq	-44(%rbp), %rsi
	movl	$3, %ecx
	xorl	%edx, %edx
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L271:
	leaq	-44(%rbp), %rsi
	movl	$3, %ecx
	movl	$1, %edx
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L270:
	leaq	-44(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZNK6icu_6717DateFormatSymbols12getYearNamesERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L269:
	leaq	-44(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZNK6icu_6717DateFormatSymbols12getYearNamesERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L268:
	leaq	-44(%rbp), %rsi
	movl	$2, %ecx
	xorl	%edx, %edx
	call	_ZNK6icu_6717DateFormatSymbols12getYearNamesERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L267:
	leaq	-44(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZNK6icu_6717DateFormatSymbols14getZodiacNamesERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L266:
	leaq	-44(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZNK6icu_6717DateFormatSymbols14getZodiacNamesERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L264:
	leaq	-44(%rbp), %rsi
	movl	$2, %ecx
	xorl	%edx, %edx
	call	_ZNK6icu_6717DateFormatSymbols14getZodiacNamesERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L292:
	leaq	-44(%rbp), %rsi
	call	_ZNK6icu_6717DateFormatSymbols7getErasERi@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L291:
	leaq	-44(%rbp), %rsi
	call	_ZNK6icu_6717DateFormatSymbols9getMonthsERi@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L290:
	leaq	-44(%rbp), %rsi
	call	_ZNK6icu_6717DateFormatSymbols14getShortMonthsERi@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L289:
	leaq	-44(%rbp), %rsi
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERi@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L288:
	leaq	-44(%rbp), %rsi
	call	_ZNK6icu_6717DateFormatSymbols16getShortWeekdaysERi@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L287:
	leaq	-44(%rbp), %rsi
	call	_ZNK6icu_6717DateFormatSymbols14getAmPmStringsERi@PLT
	movl	-44(%rbp), %r13d
	jmp	.L259
.L299:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	udat_countSymbols_67.cold, @function
udat_countSymbols_67.cold:
.LFSB3305:
.L296:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	jmp	.L259
	.cfi_endproc
.LFE3305:
	.text
	.size	udat_countSymbols_67, .-udat_countSymbols_67
	.section	.text.unlikely
	.size	udat_countSymbols_67.cold, .-udat_countSymbols_67.cold
.LCOLDE2:
	.text
.LHOTE2:
	.p2align 4
	.globl	udat_setSymbols_67
	.type	udat_setSymbols_67, @function
udat_setSymbols_67:
.LFB3331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %eax
	movl	%r8d, -52(%rbp)
	testl	%eax, %eax
	jle	.L549
.L300:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L549:
	.cfi_restore_state
	movq	%r9, %r15
	testq	%rdi, %rdi
	je	.L312
	movl	%esi, %ebx
	movl	%edx, %r13d
	movq	%rcx, %r14
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	movq	%rdi, -64(%rbp)
	call	__dynamic_cast@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	je	.L312
	movq	(%rdi), %rax
	call	*272(%rax)
	cmpl	$26, %ebx
	ja	.L381
	leaq	.L382(%rip), %rcx
	movslq	(%rcx,%rbx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L382:
	.long	.L304-.L382
	.long	.L315-.L382
	.long	.L318-.L382
	.long	.L333-.L382
	.long	.L336-.L382
	.long	.L375-.L382
	.long	.L378-.L382
	.long	.L310-.L382
	.long	.L321-.L382
	.long	.L342-.L382
	.long	.L324-.L382
	.long	.L327-.L382
	.long	.L330-.L382
	.long	.L345-.L382
	.long	.L348-.L382
	.long	.L354-.L382
	.long	.L357-.L382
	.long	.L360-.L382
	.long	.L363-.L382
	.long	.L366-.L382
	.long	.L339-.L382
	.long	.L351-.L382
	.long	.L381-.L382
	.long	.L369-.L382
	.long	.L381-.L382
	.long	.L381-.L382
	.long	.L372-.L382
	.text
	.p2align 4,,10
	.p2align 3
.L312:
	movl	$1, (%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	.cfi_restore_state
	movl	$16, (%r15)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L304:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	16(%rax), %r13d
	jl	.L305
.L311:
	movl	$8, (%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	64(%rax), %r13d
	jge	.L311
	.p2align 4,,10
	.p2align 3
.L305:
	testq	%r14, %r14
	je	.L312
	movslq	%r13d, %r12
	salq	$6, %r12
	addq	%rdx, %r12
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L376
	movswl	%ax, %edx
	sarl	$5, %edx
.L380:
	movl	-52(%rbp), %r9d
	addq	$24, %rsp
	movq	%r14, %rcx
	movq	%r12, %rdi
	popq	%rbx
	xorl	%r8d, %r8d
	popq	%r12
	xorl	%esi, %esi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore_state
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	80(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L333:
	movq	152(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	160(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L336:
	movq	168(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	176(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L375:
	movq	280(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	288(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L378:
	testq	%r14, %r14
	je	.L312
	leaq	736(%rax), %r12
	movq	%rax, -64(%rbp)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movq	-64(%rbp), %rax
	movswl	744(%rax), %edx
	testw	%dx, %dx
	js	.L379
	sarl	$5, %edx
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L310:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	32(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L321:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	96(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L342:
	movq	200(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	208(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L324:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	112(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L327:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	128(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L330:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	144(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L345:
	movq	216(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	224(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L348:
	movq	232(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	240(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L354:
	movq	264(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	272(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L357:
	movq	376(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	384(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L360:
	movq	392(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	400(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L363:
	movq	408(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	416(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L366:
	movq	424(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	432(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L339:
	movq	184(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	192(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L351:
	movq	248(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	256(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L369:
	movq	456(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	464(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L372:
	movq	472(%rax), %rdx
	testq	%rdx, %rdx
	je	.L300
	cmpl	480(%rax), %r13d
	jge	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L376:
	movl	12(%r12), %edx
	jmp	.L380
.L379:
	movl	748(%rax), %edx
	jmp	.L380
	.cfi_endproc
.LFE3331:
	.size	udat_setSymbols_67, .-udat_setSymbols_67
	.p2align 4
	.globl	udat_getLocaleByType_67
	.type	udat_getLocaleByType_67, @function
udat_getLocaleByType_67:
.LFB3332:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L554
	jmp	_ZNK6icu_676Format11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L554:
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L550
	movl	$1, (%rdx)
.L550:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3332:
	.size	udat_getLocaleByType_67, .-udat_getLocaleByType_67
	.p2align 4
	.globl	udat_setContext_67
	.type	udat_setContext_67, @function
udat_setContext_67:
.LFB3333:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L555
	movq	(%rdi), %rax
	jmp	*200(%rax)
	.p2align 4,,10
	.p2align 3
.L555:
	ret
	.cfi_endproc
.LFE3333:
	.size	udat_setContext_67, .-udat_setContext_67
	.p2align 4
	.globl	udat_getContext_67
	.type	udat_getContext_67, @function
udat_getContext_67:
.LFB3334:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L558
	movq	(%rdi), %rax
	jmp	*208(%rax)
	.p2align 4,,10
	.p2align 3
.L558:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3334:
	.size	udat_getContext_67, .-udat_getContext_67
	.p2align 4
	.globl	udat_toPatternRelativeDate_67
	.type	udat_toPatternRelativeDate_67, @function
udat_toPatternRelativeDate_67:
.LFB3336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L577
.L560:
	movl	$-1, %r12d
	.p2align 4,,10
	.p2align 3
.L559:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L578
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	%rcx, %r12
	testq	%rdi, %rdi
	je	.L561
	movq	%rsi, %rbx
	movl	%edx, %r14d
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6718RelativeDateFormatE(%rip), %rdx
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L561
	testq	%rbx, %rbx
	je	.L579
	testl	%r14d, %r14d
	js	.L563
	movl	$2, %edx
	leaq	-128(%rbp), %r15
	movl	%r14d, %ecx
	movq	%rbx, %rsi
	movw	%dx, -120(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
.L567:
	movq	0(%r13), %rax
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*240(%rax)
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%r15, %rdi
	leaq	-136(%rbp), %rsi
	movq	%rbx, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L579:
	testl	%r14d, %r14d
	jne	.L563
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r15
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L561:
	movl	$1, (%r12)
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L563:
	movl	$1, (%r12)
	movl	$-1, %r12d
	jmp	.L559
.L578:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3336:
	.size	udat_toPatternRelativeDate_67, .-udat_toPatternRelativeDate_67
	.p2align 4
	.globl	udat_toPatternRelativeTime_67
	.type	udat_toPatternRelativeTime_67, @function
udat_toPatternRelativeTime_67:
.LFB3337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L598
.L581:
	movl	$-1, %r12d
	.p2align 4,,10
	.p2align 3
.L580:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L599
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L598:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	%rcx, %r12
	testq	%rdi, %rdi
	je	.L582
	movq	%rsi, %rbx
	movl	%edx, %r14d
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6718RelativeDateFormatE(%rip), %rdx
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L582
	testq	%rbx, %rbx
	je	.L600
	testl	%r14d, %r14d
	js	.L584
	movl	$2, %edx
	leaq	-128(%rbp), %r15
	movl	%r14d, %ecx
	movq	%rbx, %rsi
	movw	%dx, -120(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
.L588:
	movq	0(%r13), %rax
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*248(%rax)
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%r15, %rdi
	leaq	-136(%rbp), %rsi
	movq	%rbx, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L600:
	testl	%r14d, %r14d
	jne	.L584
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r15
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L582:
	movl	$1, (%r12)
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L584:
	movl	$1, (%r12)
	movl	$-1, %r12d
	jmp	.L580
.L599:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3337:
	.size	udat_toPatternRelativeTime_67, .-udat_toPatternRelativeTime_67
	.p2align 4
	.globl	udat_applyPatternRelative_67
	.type	udat_applyPatternRelative_67, @function
udat_applyPatternRelative_67:
.LFB3338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	testl	%eax, %eax
	jle	.L611
.L601:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L612
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	%r9, %r12
	testq	%rdi, %rdi
	je	.L603
	movq	%rsi, %r15
	movl	%edx, %r14d
	leaq	_ZTIN6icu_6710DateFormatE(%rip), %rsi
	movq	%rcx, %rbx
	leaq	_ZTIN6icu_6718RelativeDateFormatE(%rip), %rdx
	xorl	%ecx, %ecx
	call	__dynamic_cast@PLT
	movl	-216(%rbp), %r8d
	testq	%rax, %rax
	je	.L603
	movq	%r15, -200(%rbp)
	xorl	%esi, %esi
	leaq	-192(%rbp), %r15
	cmpl	$-1, %r14d
	leaq	-200(%rbp), %rdx
	sete	%sil
	movl	%r14d, %ecx
	movq	%r15, %rdi
	movl	%r8d, -220(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	-220(%rbp), %r8d
	movq	-216(%rbp), %rdx
	xorl	%esi, %esi
	leaq	-128(%rbp), %r14
	movq	%r14, %rdi
	movq	%rbx, -200(%rbp)
	cmpl	$-1, %r8d
	movl	%r8d, %ecx
	sete	%sil
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	0(%r13), %rax
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*256(%rax)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L603:
	movl	$1, (%r12)
	jmp	.L601
.L612:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3338:
	.size	udat_applyPatternRelative_67, .-udat_applyPatternRelative_67
	.p2align 4
	.globl	udat_open_67
	.type	udat_open_67, @function
udat_open_67:
.LFB3278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %r15
	movl	%r8d, -372(%rbp)
	movl	(%r15), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L613
	movq	_ZL7gOpener(%rip), %rax
	movl	%edi, %r13d
	movl	%esi, %r11d
	movq	%rdx, %r14
	movq	%rcx, %rbx
	testq	%rax, %rax
	je	.L618
	movl	16(%rbp), %ecx
	pushq	%r15
	movq	%r9, -392(%rbp)
	pushq	%rcx
	movq	%rbx, %rcx
	movl	%esi, -384(%rbp)
	call	*%rax
	popq	%rdx
	popq	%rcx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L640
.L613:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L641
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L640:
	.cfi_restore_state
	movl	-384(%rbp), %r11d
	movq	-392(%rbp), %r9
.L618:
	cmpl	$-2, %r13d
	je	.L642
	testq	%r14, %r14
	je	.L643
	leaq	-288(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%r14, %rsi
	movl	%r11d, -392(%rbp)
	movq	%r9, -384(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-384(%rbp), %r9
	movl	-392(%rbp), %r11d
	movl	%r13d, %esi
	movq	%r9, %rdx
	movl	%r11d, %edi
	call	_ZN6icu_6710DateFormat22createDateTimeInstanceENS0_6EStyleES1_RKNS_6LocaleE@PLT
	movq	-384(%rbp), %r9
	movq	%rax, %r12
	movq	%r9, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
.L620:
	testq	%r12, %r12
	je	.L623
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L639
	testq	%rbx, %rbx
	je	.L613
	movl	-372(%rbp), %ecx
	leaq	-288(%rbp), %r14
	xorl	%esi, %esi
	leaq	-360(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rbx, -360(%rbp)
	cmpl	$-1, %ecx
	sete	%sil
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r14, %rdi
	call	_ZN6icu_678TimeZone14createTimeZoneERKNS_13UnicodeStringE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r13, %r13
	je	.L644
	movq	(%r12), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*184(%rax)
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L644:
	movl	$7, (%r15)
	.p2align 4,,10
	.p2align 3
.L639:
	movq	(%r12), %rax
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*8(%rax)
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L643:
	movl	%r11d, -384(%rbp)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	-384(%rbp), %r11d
	movl	%r13d, %esi
	movq	%rax, %rdx
	movl	%r11d, %edi
	call	_ZN6icu_6710DateFormat22createDateTimeInstanceENS0_6EStyleES1_RKNS_6LocaleE@PLT
	movq	%rax, %r12
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L642:
	movl	16(%rbp), %ecx
	xorl	%esi, %esi
	leaq	-352(%rbp), %r13
	cmpl	$-1, 16(%rbp)
	leaq	-360(%rbp), %rdx
	sete	%sil
	movq	%r13, %rdi
	movq	%r9, -360(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	testq	%r14, %r14
	je	.L645
	leaq	-288(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%r14, %rsi
	movq	%r9, -384(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	$864, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-384(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L625
	movq	%r9, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6716SimpleDateFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode@PLT
	movq	-384(%rbp), %r9
.L625:
	movq	%r9, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
.L624:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L620
.L646:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L623:
	movl	$7, (%r15)
	xorl	%r12d, %r12d
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L645:
	movl	$864, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L646
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6716SimpleDateFormatC1ERKNS_13UnicodeStringER10UErrorCode@PLT
	jmp	.L624
.L641:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3278:
	.size	udat_open_67, .-udat_open_67
	.local	_ZL7gOpener
	.comm	_ZL7gOpener,8,8
	.section	.rodata
	.align 32
	.type	_ZL17gDateFieldMapping, @object
	.size	_ZL17gDateFieldMapping, 144
_ZL17gDateFieldMapping:
	.long	0
	.long	1
	.long	2
	.long	5
	.long	11
	.long	11
	.long	12
	.long	13
	.long	14
	.long	7
	.long	6
	.long	8
	.long	3
	.long	4
	.long	9
	.long	10
	.long	10
	.long	15
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	15
	.long	15
	.long	18
	.long	2
	.long	2
	.long	2
	.long	15
	.long	1
	.long	15
	.long	15
	.long	15
	.long	19
	.long	23
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
