	.file	"persncal.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"persian"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715PersianCalendar7getTypeEv
	.type	_ZNK6icu_6715PersianCalendar7getTypeEv, @function
_ZNK6icu_6715PersianCalendar7getTypeEv:
.LFB2978:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE2978:
	.size	_ZNK6icu_6715PersianCalendar7getTypeEv, .-_ZNK6icu_6715PersianCalendar7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715PersianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.type	_ZNK6icu_6715PersianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, @function
_ZNK6icu_6715PersianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE:
.LFB2990:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	leaq	_ZL22kPersianCalendarLimits(%rip), %rax
	leaq	(%rdx,%rsi,4), %rdx
	movl	(%rax,%rdx,4), %eax
	ret
	.cfi_endproc
.LFE2990:
	.size	_ZNK6icu_6715PersianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, .-_ZNK6icu_6715PersianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715PersianCalendar18haveDefaultCenturyEv
	.type	_ZNK6icu_6715PersianCalendar18haveDefaultCenturyEv, @function
_ZNK6icu_6715PersianCalendar18haveDefaultCenturyEv:
.LFB3000:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3000:
	.size	_ZNK6icu_6715PersianCalendar18haveDefaultCenturyEv, .-_ZNK6icu_6715PersianCalendar18haveDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715PersianCalendar17getDynamicClassIDEv
	.type	_ZNK6icu_6715PersianCalendar17getDynamicClassIDEv, @function
_ZNK6icu_6715PersianCalendar17getDynamicClassIDEv:
.LFB3005:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6715PersianCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3005:
	.size	_ZNK6icu_6715PersianCalendar17getDynamicClassIDEv, .-_ZNK6icu_6715PersianCalendar17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715PersianCalendar5cloneEv
	.type	_ZNK6icu_6715PersianCalendar5cloneEv, @function
_ZNK6icu_6715PersianCalendar5cloneEv:
.LFB2979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$616, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L6
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_678CalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6715PersianCalendarE(%rip), %rax
	movq	%rax, (%r12)
.L6:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2979:
	.size	_ZNK6icu_6715PersianCalendar5cloneEv, .-_ZNK6icu_6715PersianCalendar5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715PersianCalendarD2Ev
	.type	_ZN6icu_6715PersianCalendarD2Ev, @function
_ZN6icu_6715PersianCalendarD2Ev:
.LFB2987:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6715PersianCalendarE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678CalendarD2Ev@PLT
	.cfi_endproc
.LFE2987:
	.size	_ZN6icu_6715PersianCalendarD2Ev, .-_ZN6icu_6715PersianCalendarD2Ev
	.globl	_ZN6icu_6715PersianCalendarD1Ev
	.set	_ZN6icu_6715PersianCalendarD1Ev,_ZN6icu_6715PersianCalendarD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715PersianCalendarD0Ev
	.type	_ZN6icu_6715PersianCalendarD0Ev, @function
_ZN6icu_6715PersianCalendarD0Ev:
.LFB2989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715PersianCalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678CalendarD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2989:
	.size	_ZN6icu_6715PersianCalendarD0Ev, .-_ZN6icu_6715PersianCalendarD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715PersianCalendar20handleGetMonthLengthEii
	.type	_ZNK6icu_6715PersianCalendar20handleGetMonthLengthEii, @function
_ZNK6icu_6715PersianCalendar20handleGetMonthLengthEii:
.LFB2994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$40, %rsp
	movl	%edx, -36(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -24(%rbp)
	xorl	%ecx, %ecx
	cmpl	$11, %edx
	ja	.L21
.L16:
	leal	(%rbx,%rbx,4), %eax
	pxor	%xmm0, %xmm0
	leaq	-28(%rbp), %rsi
	movl	$33, %edi
	leal	11(%rax,%rax,4), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	cmpl	$7, -28(%rbp)
	movslq	-36(%rbp), %rax
	jg	.L17
	leaq	_ZL23kPersianLeapMonthLength(%rip), %rdx
	movsbl	(%rdx,%rax), %eax
.L15:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L22
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	leaq	_ZL19kPersianMonthLength(%rip), %rdx
	movsbl	(%rdx,%rax), %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L21:
	pxor	%xmm0, %xmm0
	leaq	-36(%rbp), %rsi
	movl	$12, %edi
	cvtsi2sdl	%edx, %xmm0
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	addl	%eax, %ebx
	jmp	.L16
.L22:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2994:
	.size	_ZNK6icu_6715PersianCalendar20handleGetMonthLengthEii, .-_ZNK6icu_6715PersianCalendar20handleGetMonthLengthEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715PersianCalendar19handleGetYearLengthEi
	.type	_ZNK6icu_6715PersianCalendar19handleGetYearLengthEi, @function
_ZNK6icu_6715PersianCalendar19handleGetYearLengthEi:
.LFB2995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movl	$33, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leal	(%rsi,%rsi,4), %eax
	leaq	-12(%rbp), %r8
	leal	11(%rax,%rax,4), %eax
	movq	%r8, %rsi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	xorl	%eax, %eax
	cmpl	$7, -12(%rbp)
	setle	%al
	addl	$365, %eax
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L28
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L28:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2995:
	.size	_ZNK6icu_6715PersianCalendar19handleGetYearLengthEi, .-_ZNK6icu_6715PersianCalendar19handleGetYearLengthEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715PersianCalendar23handleComputeMonthStartEiia
	.type	_ZNK6icu_6715PersianCalendar23handleComputeMonthStartEiia, @function
_ZNK6icu_6715PersianCalendar23handleComputeMonthStartEiia:
.LFB2996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$24, %rsp
	movl	%edx, -20(%rbp)
	cmpl	$11, %edx
	ja	.L36
.L30:
	leal	21(,%rbx,8), %edi
	subl	$1, %ebx
	movl	$33, %esi
	call	_ZN6icu_679ClockMath11floorDivideEii@PLT
	imull	$365, %ebx, %ebx
	leal	1948319(%rbx,%rax), %r8d
	movslq	-20(%rbp), %rax
	testl	%eax, %eax
	je	.L29
	leaq	_ZL15kPersianNumDays(%rip), %rdx
	movswl	(%rdx,%rax,2), %eax
	addl	%eax, %r8d
.L29:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	leaq	-20(%rbp), %rsi
	movl	$12, %edi
	cvtsi2sdl	%edx, %xmm0
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	addl	%eax, %ebx
	jmp	.L30
	.cfi_endproc
.LFE2996:
	.size	_ZNK6icu_6715PersianCalendar23handleComputeMonthStartEiia, .-_ZNK6icu_6715PersianCalendar23handleComputeMonthStartEiia
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715PersianCalendar19handleComputeFieldsEiR10UErrorCode
	.type	_ZN6icu_6715PersianCalendar19handleComputeFieldsEiR10UErrorCode, @function
_ZN6icu_6715PersianCalendar19handleComputeFieldsEiR10UErrorCode:
.LFB2998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leal	-1948320(%rsi), %r14d
	movl	$12053, %esi
	movslq	%r14d, %rax
	pushq	%r13
	movq	%rax, %rdx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	salq	$5, %rdx
	movq	%rdi, %rbx
	leaq	3(%rax,%rdx), %rdi
	call	_ZN6icu_679ClockMath11floorDivideEll@PLT
	movl	$33, %esi
	movq	%rax, %r13
	leal	29(,%rax,8), %edi
	leal	1(%rax), %r12d
	call	_ZN6icu_679ClockMath11floorDivideEii@PLT
	imull	$365, %r13d, %r13d
	movl	%r14d, %esi
	addl	%eax, %r13d
	subl	%r13d, %esi
	cmpl	$215, %esi
	jg	.L38
	movslq	%esi, %rax
	movl	%esi, %edx
	imulq	$-2078209981, %rax, %rax
	sarl	$31, %edx
	shrq	$32, %rax
	addl	%esi, %eax
	sarl	$4, %eax
	subl	%edx, %eax
.L39:
	movslq	%eax, %rcx
	leaq	_ZL15kPersianNumDays(%rip), %rdx
	movl	%eax, 20(%rbx)
	movabsq	$4294967297, %rax
	movswl	(%rdx,%rcx,2), %edx
	movl	%esi, %ecx
	addl	$1, %esi
	movl	%r12d, 16(%rbx)
	movl	%esi, 36(%rbx)
	movl	$257, %esi
	subl	%edx, %ecx
	movl	%r12d, 88(%rbx)
	movl	%ecx, %edx
	movl	$257, %ecx
	movb	$1, 123(%rbx)
	addl	$1, %edx
	movl	$0, 12(%rbx)
	movl	$1, 204(%rbx)
	movq	%rax, 128(%rbx)
	movl	$1, 136(%rbx)
	movw	%cx, 104(%rbx)
	movb	$1, 106(%rbx)
	movl	%edx, 32(%rbx)
	movq	%rax, 148(%rbx)
	movw	%si, 109(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	leal	-6(%rsi), %eax
	movl	$2290649225, %edx
	imulq	%rdx, %rax
	shrq	$36, %rax
	jmp	.L39
	.cfi_endproc
.LFE2998:
	.size	_ZN6icu_6715PersianCalendar19handleComputeFieldsEiR10UErrorCode, .-_ZN6icu_6715PersianCalendar19handleComputeFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715PersianCalendar14inDaylightTimeER10UErrorCode
	.type	_ZNK6icu_6715PersianCalendar14inDaylightTimeER10UErrorCode, @function
_ZNK6icu_6715PersianCalendar14inDaylightTimeER10UErrorCode:
.LFB2999:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.L42
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK6icu_678Calendar11getTimeZoneEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*72(%rax)
	testb	%al, %al
	jne	.L53
	xorl	%eax, %eax
.L41:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode@PLT
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L41
	movl	76(%r12), %eax
	testl	%eax, %eax
	setne	%al
	jmp	.L41
	.cfi_endproc
.LFE2999:
	.size	_ZNK6icu_6715PersianCalendar14inDaylightTimeER10UErrorCode, .-_ZNK6icu_6715PersianCalendar14inDaylightTimeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715PersianCalendar21handleGetExtendedYearEv
	.type	_ZN6icu_6715PersianCalendar21handleGetExtendedYearEv, @function
_ZN6icu_6715PersianCalendar21handleGetExtendedYearEv:
.LFB2997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movl	$19, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_@PLT
	cmpl	$19, %eax
	movl	$1, %eax
	je	.L60
	movl	132(%rbx), %edx
	testl	%edx, %edx
	jle	.L54
	movl	16(%rbx), %eax
.L54:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	204(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L54
	movl	88(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2997:
	.size	_ZN6icu_6715PersianCalendar21handleGetExtendedYearEv, .-_ZN6icu_6715PersianCalendar21handleGetExtendedYearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715PersianCalendarC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6715PersianCalendarC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6715PersianCalendarC2ERKNS_6LocaleER10UErrorCode:
.LFB2981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r12, %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6715PersianCalendarE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE2981:
	.size	_ZN6icu_6715PersianCalendarC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6715PersianCalendarC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6715PersianCalendarC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6715PersianCalendarC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6715PersianCalendarC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715PersianCalendarC2ERKS0_
	.type	_ZN6icu_6715PersianCalendarC2ERKS0_, @function
_ZN6icu_6715PersianCalendarC2ERKS0_:
.LFB2984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678CalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6715PersianCalendarE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2984:
	.size	_ZN6icu_6715PersianCalendarC2ERKS0_, .-_ZN6icu_6715PersianCalendarC2ERKS0_
	.globl	_ZN6icu_6715PersianCalendarC1ERKS0_
	.set	_ZN6icu_6715PersianCalendarC1ERKS0_,_ZN6icu_6715PersianCalendarC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715PersianCalendar10isLeapYearEi
	.type	_ZN6icu_6715PersianCalendar10isLeapYearEi, @function
_ZN6icu_6715PersianCalendar10isLeapYearEi:
.LFB2991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leal	(%rdi,%rdi,4), %eax
	leaq	-12(%rbp), %rsi
	movl	$33, %edi
	leal	11(%rax,%rax,4), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	cmpl	$7, -12(%rbp)
	setle	%al
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L68
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L68:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2991:
	.size	_ZN6icu_6715PersianCalendar10isLeapYearEi, .-_ZN6icu_6715PersianCalendar10isLeapYearEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715PersianCalendar9yearStartEi
	.type	_ZN6icu_6715PersianCalendar9yearStartEi, @function
_ZN6icu_6715PersianCalendar9yearStartEi:
.LFB2992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_6715PersianCalendar23handleComputeMonthStartEiia(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	264(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L70
	subl	$1, %r12d
	leal	21(,%rsi,8), %edi
	movl	$33, %esi
	call	_ZN6icu_679ClockMath11floorDivideEii@PLT
	imull	$365, %r12d, %r12d
	addq	$8, %rsp
	leal	1948319(%r12,%rax), %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE2992:
	.size	_ZN6icu_6715PersianCalendar9yearStartEi, .-_ZN6icu_6715PersianCalendar9yearStartEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715PersianCalendar10monthStartEii
	.type	_ZNK6icu_6715PersianCalendar10monthStartEii, @function
_ZNK6icu_6715PersianCalendar10monthStartEii:
.LFB2993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_6715PersianCalendar23handleComputeMonthStartEiia(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	264(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L75
	movl	%edx, -28(%rbp)
	cmpl	$11, %edx
	ja	.L84
.L76:
	leal	21(,%r12,8), %edi
	subl	$1, %r12d
	movl	$33, %esi
	call	_ZN6icu_679ClockMath11floorDivideEii@PLT
	imull	$365, %r12d, %r12d
	movslq	-28(%rbp), %rdx
	leal	1948319(%r12,%rax), %eax
	testl	%edx, %edx
	jne	.L85
.L74:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L86
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	leaq	_ZL15kPersianNumDays(%rip), %rcx
	movswl	(%rcx,%rdx,2), %edx
	addl	%edx, %eax
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L84:
	pxor	%xmm0, %xmm0
	leaq	-28(%rbp), %rsi
	movl	$12, %edi
	cvtsi2sdl	%edx, %xmm0
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	addl	%eax, %r12d
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$1, %ecx
	call	*%rax
	jmp	.L74
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2993:
	.size	_ZNK6icu_6715PersianCalendar10monthStartEii, .-_ZNK6icu_6715PersianCalendar10monthStartEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715PersianCalendar16getStaticClassIDEv
	.type	_ZN6icu_6715PersianCalendar16getStaticClassIDEv, @function
_ZN6icu_6715PersianCalendar16getStaticClassIDEv:
.LFB3004:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6715PersianCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3004:
	.size	_ZN6icu_6715PersianCalendar16getStaticClassIDEv, .-_ZN6icu_6715PersianCalendar16getStaticClassIDEv
	.section	.rodata.str1.1
.LC1:
	.string	"@calendar=persian"
	.text
	.p2align 4
	.type	_ZN6icu_67L30initializeSystemDefaultCenturyEv, @function
_ZN6icu_67L30initializeSystemDefaultCenturyEv:
.LFB3001:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-880(%rbp), %r14
	leaq	-884(%rbp), %r13
	pushq	%r12
	movq	%r14, %rdi
	.cfi_offset 12, -40
	leaq	-656(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVN6icu_6715PersianCalendarE(%rip), %rbx
	subq	$864, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -884(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	movq	%rbx, -656(%rbp)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	-884(%rbp), %eax
	testl	%eax, %eax
	jle	.L92
.L89:
	movq	%r12, %rdi
	movq	%rbx, -656(%rbp)
	call	_ZN6icu_678CalendarD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$864, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r13, %rcx
	movl	$-80, %edx
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movsd	%xmm0, _ZN6icu_67L26gSystemDefaultCenturyStartE(%rip)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	%eax, _ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip)
	jmp	.L89
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3001:
	.size	_ZN6icu_67L30initializeSystemDefaultCenturyEv, .-_ZN6icu_67L30initializeSystemDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715PersianCalendar19defaultCenturyStartEv
	.type	_ZNK6icu_6715PersianCalendar19defaultCenturyStartEv, @function
_ZNK6icu_6715PersianCalendar19defaultCenturyStartEv:
.LFB3002:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %eax
	cmpl	$2, %eax
	je	.L102
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L96
	call	_ZN6icu_67L30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L96:
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore 6
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3002:
	.size	_ZNK6icu_6715PersianCalendar19defaultCenturyStartEv, .-_ZNK6icu_6715PersianCalendar19defaultCenturyStartEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715PersianCalendar23defaultCenturyStartYearEv
	.type	_ZNK6icu_6715PersianCalendar23defaultCenturyStartYearEv, @function
_ZNK6icu_6715PersianCalendar23defaultCenturyStartYearEv:
.LFB3003:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %eax
	cmpl	$2, %eax
	je	.L113
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L107
	call	_ZN6icu_67L30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L107:
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore 6
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	ret
	.cfi_endproc
.LFE3003:
	.size	_ZNK6icu_6715PersianCalendar23defaultCenturyStartYearEv, .-_ZNK6icu_6715PersianCalendar23defaultCenturyStartYearEv
	.weak	_ZTSN6icu_6715PersianCalendarE
	.section	.rodata._ZTSN6icu_6715PersianCalendarE,"aG",@progbits,_ZTSN6icu_6715PersianCalendarE,comdat
	.align 16
	.type	_ZTSN6icu_6715PersianCalendarE, @object
	.size	_ZTSN6icu_6715PersianCalendarE, 27
_ZTSN6icu_6715PersianCalendarE:
	.string	"N6icu_6715PersianCalendarE"
	.weak	_ZTIN6icu_6715PersianCalendarE
	.section	.data.rel.ro._ZTIN6icu_6715PersianCalendarE,"awG",@progbits,_ZTIN6icu_6715PersianCalendarE,comdat
	.align 8
	.type	_ZTIN6icu_6715PersianCalendarE, @object
	.size	_ZTIN6icu_6715PersianCalendarE, 24
_ZTIN6icu_6715PersianCalendarE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715PersianCalendarE
	.quad	_ZTIN6icu_678CalendarE
	.weak	_ZTVN6icu_6715PersianCalendarE
	.section	.data.rel.ro._ZTVN6icu_6715PersianCalendarE,"awG",@progbits,_ZTVN6icu_6715PersianCalendarE,comdat
	.align 8
	.type	_ZTVN6icu_6715PersianCalendarE, @object
	.size	_ZTVN6icu_6715PersianCalendarE, 416
_ZTVN6icu_6715PersianCalendarE:
	.quad	0
	.quad	_ZTIN6icu_6715PersianCalendarE
	.quad	_ZN6icu_6715PersianCalendarD1Ev
	.quad	_ZN6icu_6715PersianCalendarD0Ev
	.quad	_ZNK6icu_6715PersianCalendar17getDynamicClassIDEv
	.quad	_ZNK6icu_6715PersianCalendar5cloneEv
	.quad	_ZNK6icu_678CalendareqERKS0_
	.quad	_ZNK6icu_678Calendar14isEquivalentToERKS0_
	.quad	_ZN6icu_678Calendar3addENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar4rollENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6715PersianCalendar14inDaylightTimeER10UErrorCode
	.quad	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6715PersianCalendar7getTypeEv
	.quad	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEv
	.quad	_ZN6icu_678Calendar11computeTimeER10UErrorCode
	.quad	_ZN6icu_678Calendar13computeFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode
	.quad	_ZNK6icu_6715PersianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.quad	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE
	.quad	_ZNK6icu_6715PersianCalendar23handleComputeMonthStartEiia
	.quad	_ZNK6icu_6715PersianCalendar20handleGetMonthLengthEii
	.quad	_ZNK6icu_6715PersianCalendar19handleGetYearLengthEi
	.quad	_ZN6icu_6715PersianCalendar21handleGetExtendedYearEv
	.quad	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields
	.quad	_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii
	.quad	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar23getFieldResolutionTableEv
	.quad	_ZN6icu_6715PersianCalendar19handleComputeFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar21getDefaultMonthInYearEi
	.quad	_ZN6icu_678Calendar20getDefaultDayInMonthEii
	.quad	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6715PersianCalendar18haveDefaultCenturyEv
	.quad	_ZNK6icu_6715PersianCalendar19defaultCenturyStartEv
	.quad	_ZNK6icu_6715PersianCalendar23defaultCenturyStartYearEv
	.quad	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode
	.quad	_ZN6icu_678Calendar14setRelatedYearEi
	.local	_ZZN6icu_6715PersianCalendar16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6715PersianCalendar16getStaticClassIDEvE7classID,1,1
	.local	_ZN6icu_67L25gSystemDefaultCenturyInitE
	.comm	_ZN6icu_67L25gSystemDefaultCenturyInitE,8,8
	.data
	.align 4
	.type	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, @object
	.size	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, 4
_ZN6icu_67L30gSystemDefaultCenturyStartYearE:
	.long	-1
	.align 8
	.type	_ZN6icu_67L26gSystemDefaultCenturyStartE, @object
	.size	_ZN6icu_67L26gSystemDefaultCenturyStartE, 8
_ZN6icu_67L26gSystemDefaultCenturyStartE:
	.long	0
	.long	1048576
	.section	.rodata
	.align 32
	.type	_ZL22kPersianCalendarLimits, @object
	.size	_ZL22kPersianCalendarLimits, 368
_ZL22kPersianCalendarLimits:
	.zero	16
	.long	-5000000
	.long	-5000000
	.long	5000000
	.long	5000000
	.long	0
	.long	0
	.long	11
	.long	11
	.long	1
	.long	1
	.long	52
	.long	53
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	1
	.long	1
	.long	29
	.long	31
	.long	1
	.long	1
	.long	365
	.long	366
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	1
	.long	1
	.long	5
	.long	5
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-5000000
	.long	-5000000
	.long	5000000
	.long	5000000
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-5000000
	.long	-5000000
	.long	5000000
	.long	5000000
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.align 8
	.type	_ZL23kPersianLeapMonthLength, @object
	.size	_ZL23kPersianLeapMonthLength, 12
_ZL23kPersianLeapMonthLength:
	.ascii	"\037\037\037\037\037\037\036\036\036\036\036\036"
	.align 8
	.type	_ZL19kPersianMonthLength, @object
	.size	_ZL19kPersianMonthLength, 12
_ZL19kPersianMonthLength:
	.ascii	"\037\037\037\037\037\037\036\036\036\036\036\035"
	.align 16
	.type	_ZL15kPersianNumDays, @object
	.size	_ZL15kPersianNumDays, 24
_ZL15kPersianNumDays:
	.value	0
	.value	31
	.value	62
	.value	93
	.value	124
	.value	155
	.value	186
	.value	216
	.value	246
	.value	276
	.value	306
	.value	336
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
