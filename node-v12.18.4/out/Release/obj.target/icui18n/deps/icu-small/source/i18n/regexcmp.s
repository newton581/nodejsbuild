	.file	"regexcmp.cpp"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0, @function
_ZN6icu_6712RegexCompile10nextCharLLEv.part.0:
.LFB4193:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	movl	40(%rdi), %edx
	cmpl	44(%rdi), %edx
	jge	.L2
	movq	48(%rdi), %rcx
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %eax
	cmpw	$-10241, %ax
	jbe	.L13
.L2:
	call	utext_next32_67@PLT
	cmpl	$-1, %eax
	je	.L1
.L3:
	cmpl	$13, %eax
	sete	%cl
	cmpl	$133, %eax
	sete	%dl
	orb	%dl, %cl
	jne	.L5
	cmpl	$8232, %eax
	jne	.L14
.L5:
	addq	$1, 48(%rbx)
	movq	$0, 56(%rbx)
.L7:
	movl	%eax, 64(%rbx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	addl	$1, %edx
	movl	%edx, 40(%rdi)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L14:
	cmpl	$10, %eax
	je	.L15
	addq	$1, 56(%rbx)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$13, 64(%rbx)
	jne	.L5
	jmp	.L7
	.cfi_endproc
.LFE4193:
	.size	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0, .-_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	.section	.text._ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi,"axG",@progbits,_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	.type	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi, @function
_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi:
.LFB1194:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %eax
	movq	(%rsi), %rcx
	movl	%edx, %r9d
	testw	%ax, %ax
	js	.L17
	movswl	%ax, %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	sarl	$5, %edx
	jmp	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	movl	12(%rdi), %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	jmp	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	.cfi_endproc
.LFE1194:
	.size	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi, .-_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3510:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3510:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3513:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L32
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L20
	cmpb	$0, 12(%rbx)
	jne	.L33
.L24:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L24
	.cfi_endproc
.LFE3513:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3516:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L36
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3516:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3519:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L39
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3519:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L45
.L41:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L46
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3521:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3522:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3522:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3523:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3523:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3524:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3524:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3525:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3525:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3526:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3526:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3527:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L62
	testl	%edx, %edx
	jle	.L62
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L65
.L54:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L54
	.cfi_endproc
.LFE3527:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3528:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L69
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L69
	testl	%r12d, %r12d
	jg	.L76
	cmpb	$0, 12(%rbx)
	jne	.L77
.L71:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L71
.L77:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L69:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3528:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L79
	movq	(%rdi), %r8
.L80:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L83
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L83
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L83:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3529:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3530:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L90
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3530:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3531:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3531:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3532:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3532:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3533:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3533:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3535:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3535:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3537:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3537:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompileC2EPNS_12RegexPatternER10UErrorCode
	.type	_ZN6icu_6712RegexCompileC2EPNS_12RegexPatternER10UErrorCode, @function
_ZN6icu_6712RegexCompileC2EPNS_12RegexPatternER10UErrorCode:
.LFB3165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm0
	movq	%rsi, %xmm1
	leaq	16+_ZTVN6icu_6712RegexCompileE(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$376, %rdi
	subq	$24, %rsp
	movq	%rax, -376(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -72(%rdi)
	movl	$2, %eax
	movw	%ax, -64(%rdi)
	movaps	%xmm0, -48(%rbp)
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	leaq	432(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_676UStackC1ER10UErrorCode@PLT
	leaq	472(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_676UStackC1ER10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6715RegexStaticSets11initGlobalsEP10UErrorCode@PLT
	movdqa	-48(%rbp), %xmm0
	movl	24(%r13), %eax
	xorl	%edx, %edx
	movl	(%r12), %ecx
	movq	$-1, 64(%rbx)
	movups	%xmm0, 8(%rbx)
	movdqa	.LC0(%rip), %xmm0
	orl	$-2147483648, %eax
	movl	%eax, 292(%rbx)
	movq	$0, 32(%rbx)
	movw	%dx, 40(%rbx)
	movb	$1, 42(%rbx)
	movq	$-1, 408(%rbx)
	movq	$0, 520(%rbx)
	movl	$-1, 512(%rbx)
	movups	%xmm0, 48(%rbx)
	testl	%ecx, %ecx
	jle	.L99
.L96:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movl	120(%r13), %eax
	testl	%eax, %eax
	jle	.L96
	movl	%eax, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3165:
	.size	_ZN6icu_6712RegexCompileC2EPNS_12RegexPatternER10UErrorCode, .-_ZN6icu_6712RegexCompileC2EPNS_12RegexPatternER10UErrorCode
	.globl	_ZN6icu_6712RegexCompileC1EPNS_12RegexPatternER10UErrorCode
	.set	_ZN6icu_6712RegexCompileC1EPNS_12RegexPatternER10UErrorCode,_ZN6icu_6712RegexCompileC2EPNS_12RegexPatternER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile11literalCharEi
	.type	_ZN6icu_6712RegexCompile11literalCharEi, @function
_ZN6icu_6712RegexCompile11literalCharEi:
.LFB3175:
	.cfi_startproc
	endbr64
	addq	$304, %rdi
	jmp	_ZN6icu_6713UnicodeString6appendEi@PLT
	.cfi_endproc
.LFE3175:
	.size	_ZN6icu_6712RegexCompile11literalCharEi, .-_ZN6icu_6712RegexCompile11literalCharEi
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB1:
	.text
.LHOTB1:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile7buildOpEii
	.type	_ZN6icu_6712RegexCompile7buildOpEii, @function
_ZN6icu_6712RegexCompile7buildOpEii:
.LFB3177:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L108
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$255, %esi
	ja	.L104
	cmpl	$16777215, %edx
	jg	.L104
	movl	%esi, %eax
	sall	$24, %eax
	testl	%edx, %edx
	js	.L117
	orl	%edx, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	cmpl	$255, %esi
	jne	.L118
.L109:
	movl	%edx, %eax
	shrl	$24, %eax
	cmpl	$255, %eax
	jne	.L104
	movl	$-16777216, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	orl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	testl	%esi, %esi
	jne	.L104
	jmp	.L109
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexCompile7buildOpEii.cold, @function
_ZN6icu_6712RegexCompile7buildOpEii.cold:
.LFSB3177:
.L104:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3177:
	.text
	.size	_ZN6icu_6712RegexCompile7buildOpEii, .-_ZN6icu_6712RegexCompile7buildOpEii
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexCompile7buildOpEii.cold, .-_ZN6icu_6712RegexCompile7buildOpEii.cold
.LCOLDE1:
	.text
.LHOTE1:
	.section	.text.unlikely
	.align 2
.LCOLDB2:
	.text
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile8insertOpEi
	.type	_ZN6icu_6712RegexCompile8insertOpEi, @function
_ZN6icu_6712RegexCompile8insertOpEi:
.LFB3180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movl	$0, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movabsq	$71672569920, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	movl	(%rcx), %edx
	movq	32(%rax), %r15
	movl	$117440512, %eax
	testl	%edx, %edx
	movq	%r15, %rdi
	movl	%r13d, %edx
	cmovle	%rax, %rsi
	xorl	%ebx, %ebx
	call	_ZN6icu_679UVector6415insertElementAtEliR10UErrorCode@PLT
	movl	8(%r15), %esi
	testl	%esi, %esi
	jg	.L121
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L146:
	leal	1(%rcx), %esi
	cmpl	$16777215, %ecx
	je	.L143
	sall	$24, %edx
	orl	%esi, %edx
	movslq	%edx, %rsi
.L126:
	movl	%r8d, %edx
	movq	%r15, %rdi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movl	8(%r15), %esi
.L125:
	addq	$1, %rbx
	cmpl	%ebx, %esi
	jle	.L128
.L121:
	movq	24(%r15), %rax
	movl	%ebx, %r8d
	movq	(%rax,%rbx,8), %rax
	movl	%eax, %edx
	movl	%eax, %ecx
	shrl	$24, %edx
	andl	$16777215, %ecx
	cmpl	$620756991, %eax
	ja	.L125
	btq	%rdx, %r12
	jnc	.L125
	cmpl	%ecx, %r13d
	jge	.L125
	movq	8(%r14), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L146
	xorl	%esi, %esi
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L128:
	movl	384(%r14), %eax
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.L132
	.p2align 4,,10
	.p2align 3
.L122:
	movl	%ebx, %edx
	xorl	%esi, %esi
	testl	%eax, %eax
	jle	.L129
	movl	%eax, %ecx
	subl	%ebx, %ecx
	testl	%ecx, %ecx
	jle	.L129
	movq	400(%r14), %rcx
	movl	(%rcx,%rbx,4), %esi
.L129:
	cmpl	%esi, %r13d
	jl	.L147
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L122
.L132:
	movl	412(%r14), %eax
	cmpl	%r13d, %eax
	jle	.L124
	addl	$1, %eax
	movl	%eax, 412(%r14)
.L124:
	movl	408(%r14), %eax
	cmpl	%r13d, %eax
	jle	.L119
	addl	$1, %eax
	movl	%eax, 408(%r14)
.L119:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	addl	$1, %esi
	leaq	376(%r14), %rdi
	addq	$1, %rbx
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movl	384(%r14), %eax
	cmpl	%ebx, %eax
	jg	.L122
	jmp	.L132
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexCompile8insertOpEi.cold, @function
_ZN6icu_6712RegexCompile8insertOpEi.cold:
.LFSB3180:
.L143:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3180:
	.text
	.size	_ZN6icu_6712RegexCompile8insertOpEi, .-_ZN6icu_6712RegexCompile8insertOpEi
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexCompile8insertOpEi.cold, .-_ZN6icu_6712RegexCompile8insertOpEi.cold
.LCOLDE2:
	.text
.LHOTE2:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	.type	_ZN6icu_6712RegexCompile5errorE10UErrorCode, @function
_ZN6icu_6712RegexCompile5errorE10UErrorCode:
.LFB3194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jle	.L155
	cmpl	$7, %esi
	je	.L155
.L148:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L160
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movq	48(%rbx), %rcx
	movl	$2147483648, %edx
	movl	%esi, (%rax)
	movq	24(%rbx), %rax
	cmpq	%rdx, %rcx
	jl	.L151
	movabsq	$-4294967296, %rcx
	movq	%rcx, (%rax)
.L152:
	pxor	%xmm0, %xmm0
	leaq	-28(%rbp), %r12
	movl	$16, %r8d
	movl	$0, -28(%rbp)
	movups	%xmm0, 8(%rax)
	movq	%r12, %r9
	movups	%xmm0, 24(%rax)
	movq	24(%rbx), %rax
	movups	%xmm0, 40(%rax)
	movups	%xmm0, 56(%rax)
	movq	24(%rbx), %rax
	movq	32(%rbx), %rdx
	leaq	8(%rax), %rcx
	movq	16(%rbx), %rax
	leaq	-15(%rdx), %rsi
	movq	8(%rax), %rdi
	call	utext_extract_67@PLT
	movq	24(%rbx), %rax
	movq	32(%rbx), %rsi
	movq	%r12, %r9
	movl	$16, %r8d
	leaq	40(%rax), %rcx
	movq	16(%rbx), %rax
	leaq	15(%rsi), %rdx
	movq	8(%rax), %rdi
	call	utext_extract_67@PLT
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L151:
	movq	56(%rbx), %rsi
	movl	%ecx, (%rax)
	cmpq	%rdx, %rsi
	jl	.L153
	movl	$-1, 4(%rax)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L153:
	movl	%esi, 4(%rax)
	jmp	.L152
.L160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3194:
	.size	_ZN6icu_6712RegexCompile5errorE10UErrorCode, .-_ZN6icu_6712RegexCompile5errorE10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile8appendOpEi
	.type	_ZN6icu_6712RegexCompile8appendOpEi, @function
_ZN6icu_6712RegexCompile8appendOpEi:
.LFB3178:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L174
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %rax
	movq	32(%rax), %rbx
	movslq	8(%rbx), %rax
	movl	%eax, %r8d
	addl	$1, %r8d
	js	.L164
	cmpl	12(%rbx), %r8d
	jle	.L169
.L164:
	movl	%r8d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L166
	movq	16(%r12), %rax
	movq	32(%rax), %rdx
.L167:
	cmpl	$16777200, 8(%rdx)
	jle	.L161
	movq	8(%r12), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L175
.L161:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	16(%r12), %rdx
	movslq	8(%rbx), %rax
	movq	32(%rdx), %rdx
	leal	1(%rax), %r8d
.L165:
	movq	24(%rbx), %rcx
	movslq	%r13d, %rsi
	movq	%rsi, (%rcx,%rax,8)
	movl	%r8d, 8(%rbx)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%rbx, %rdx
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L175:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$66324, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	.cfi_endproc
.LFE3178:
	.size	_ZN6icu_6712RegexCompile8appendOpEi, .-_ZN6icu_6712RegexCompile8appendOpEi
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexCompile12allocateDataEi.part.0, @function
_ZN6icu_6712RegexCompile12allocateDataEi.part.0:
.LFB4191:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leal	-1(%rsi), %eax
	cmpl	$255, %eax
	ja	.L177
	movq	16(%rdi), %rdx
	movl	132(%rdx), %eax
	testl	%eax, %eax
	js	.L177
	addl	%eax, %esi
	movl	%esi, 132(%rdx)
	cmpl	$16777199, %esi
	jg	.L186
.L176:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L187
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	8(%r12), %rdx
	xorl	%eax, %eax
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L176
	movl	$66304, (%rdx)
	movq	48(%r12), %rdx
	movl	$2147483648, %ecx
	movq	24(%r12), %rax
	cmpq	%rcx, %rdx
	jl	.L180
	movabsq	$-4294967296, %rcx
	movq	%rcx, (%rax)
.L181:
	pxor	%xmm0, %xmm0
	leaq	-28(%rbp), %rbx
	movl	$16, %r8d
	movl	$0, -28(%rbp)
	movups	%xmm0, 8(%rax)
	movq	%rbx, %r9
	movups	%xmm0, 24(%rax)
	movq	24(%r12), %rax
	movups	%xmm0, 40(%rax)
	movups	%xmm0, 56(%rax)
	movq	24(%r12), %rax
	movq	32(%r12), %rdx
	leaq	8(%rax), %rcx
	movq	16(%r12), %rax
	leaq	-15(%rdx), %rsi
	movq	8(%rax), %rdi
	call	utext_extract_67@PLT
	movq	24(%r12), %rax
	movq	32(%r12), %rsi
	movq	%rbx, %r9
	movl	$16, %r8d
	leaq	40(%rax), %rcx
	movq	16(%r12), %rax
	leaq	15(%rsi), %rdx
	movq	8(%rax), %rdi
	call	utext_extract_67@PLT
	xorl	%eax, %eax
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$66304, %esi
	movl	%eax, -36(%rbp)
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movl	-36(%rbp), %eax
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L180:
	movq	56(%r12), %rsi
	movl	%edx, (%rax)
	cmpq	%rcx, %rsi
	jl	.L182
	movl	$-1, 4(%rax)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L182:
	movl	%esi, 4(%rax)
	jmp	.L181
.L187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4191:
	.size	_ZN6icu_6712RegexCompile12allocateDataEi.part.0, .-_ZN6icu_6712RegexCompile12allocateDataEi.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexCompile17allocateStackDataEi.part.0, @function
_ZN6icu_6712RegexCompile17allocateStackDataEi.part.0:
.LFB4192:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leal	-1(%rsi), %eax
	cmpl	$255, %eax
	ja	.L189
	movq	16(%rdi), %rdx
	movl	128(%rdx), %eax
	testl	%eax, %eax
	js	.L189
	addl	%eax, %esi
	movl	%esi, 128(%rdx)
	cmpl	$16777199, %esi
	jg	.L198
.L188:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L199
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	8(%r12), %rdx
	xorl	%eax, %eax
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L188
	movl	$66304, (%rdx)
	movq	48(%r12), %rdx
	movl	$2147483648, %ecx
	movq	24(%r12), %rax
	cmpq	%rcx, %rdx
	jl	.L192
	movabsq	$-4294967296, %rcx
	movq	%rcx, (%rax)
.L193:
	pxor	%xmm0, %xmm0
	leaq	-28(%rbp), %rbx
	movl	$16, %r8d
	movl	$0, -28(%rbp)
	movups	%xmm0, 8(%rax)
	movq	%rbx, %r9
	movups	%xmm0, 24(%rax)
	movq	24(%r12), %rax
	movups	%xmm0, 40(%rax)
	movups	%xmm0, 56(%rax)
	movq	24(%r12), %rax
	movq	32(%r12), %rdx
	leaq	8(%rax), %rcx
	movq	16(%r12), %rax
	leaq	-15(%rdx), %rsi
	movq	8(%rax), %rdi
	call	utext_extract_67@PLT
	movq	24(%r12), %rax
	movq	32(%r12), %rsi
	movq	%rbx, %r9
	movl	$16, %r8d
	leaq	40(%rax), %rcx
	movq	16(%r12), %rax
	leaq	15(%rsi), %rdx
	movq	8(%rax), %rdi
	call	utext_extract_67@PLT
	xorl	%eax, %eax
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L198:
	movl	$66324, %esi
	movl	%eax, -36(%rbp)
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movl	-36(%rbp), %eax
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L192:
	movq	56(%r12), %rsi
	movl	%edx, (%rax)
	cmpq	%rcx, %rsi
	jl	.L194
	movl	$-1, 4(%rax)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L194:
	movl	%esi, 4(%rax)
	jmp	.L193
.L199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4192:
	.size	_ZN6icu_6712RegexCompile17allocateStackDataEi.part.0, .-_ZN6icu_6712RegexCompile17allocateStackDataEi.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile12allocateDataEi
	.type	_ZN6icu_6712RegexCompile12allocateDataEi, @function
_ZN6icu_6712RegexCompile12allocateDataEi:
.LFB3181:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	movl	(%rdx), %edx
	testl	%edx, %edx
	jg	.L206
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-1(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	cmpl	$255, %eax
	ja	.L202
	movq	16(%rdi), %rdx
	movl	132(%rdx), %eax
	testl	%eax, %eax
	js	.L202
	addl	%eax, %esi
	movl	%esi, 132(%rdx)
	cmpl	$16777199, %esi
	jg	.L209
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movl	$66304, %esi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	xorl	%eax, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	$66304, %esi
	movl	%eax, -4(%rbp)
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movl	-4(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3181:
	.size	_ZN6icu_6712RegexCompile12allocateDataEi, .-_ZN6icu_6712RegexCompile12allocateDataEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile17allocateStackDataEi
	.type	_ZN6icu_6712RegexCompile17allocateStackDataEi, @function
_ZN6icu_6712RegexCompile17allocateStackDataEi:
.LFB3182:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	movl	(%rdx), %edx
	testl	%edx, %edx
	jg	.L216
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-1(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	cmpl	$255, %eax
	ja	.L212
	movq	16(%rdi), %rdx
	movl	128(%rdx), %eax
	testl	%eax, %eax
	js	.L212
	addl	%eax, %esi
	movl	%esi, 128(%rdx)
	cmpl	$16777199, %esi
	jg	.L219
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movl	$66304, %esi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	xorl	%eax, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	$66324, %esi
	movl	%eax, -4(%rbp)
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movl	-4(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3182:
	.size	_ZN6icu_6712RegexCompile17allocateStackDataEi, .-_ZN6icu_6712RegexCompile17allocateStackDataEi
	.section	.text.unlikely
	.align 2
.LCOLDB3:
	.text
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile8appendOpEii
	.type	_ZN6icu_6712RegexCompile8appendOpEii, @function
_ZN6icu_6712RegexCompile8appendOpEii:
.LFB3179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	movq	8(%rdi), %rdx
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L220
	cmpl	$255, %esi
	ja	.L224
	cmpl	$16777215, %ebx
	jg	.L224
	movl	%esi, %r14d
	movq	%rdi, %r12
	sall	$24, %r14d
	testl	%ebx, %ebx
	js	.L239
.L227:
	movq	16(%r12), %rax
	movq	32(%rax), %r13
	movslq	8(%r13), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L228
	cmpl	12(%r13), %esi
	jle	.L233
.L228:
	movq	%r13, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L230
	movq	16(%r12), %rax
	movq	32(%rax), %rdx
.L231:
	cmpl	$16777200, 8(%rdx)
	jle	.L220
	movq	8(%r12), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L220
	popq	%rbx
	movq	%r12, %rdi
	movl	$66324, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	cmpl	$255, %esi
	jne	.L240
.L234:
	movl	%ebx, %eax
	shrl	$24, %eax
	cmpl	$255, %eax
	jne	.L224
	movl	$-16777216, %r14d
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L230:
	movq	16(%r12), %rdx
	movslq	8(%r13), %rax
	movq	32(%rdx), %rdx
	leal	1(%rax), %esi
.L229:
	movq	24(%r13), %rcx
	orl	%r14d, %ebx
	movslq	%ebx, %rbx
	movq	%rbx, (%rcx,%rax,8)
	movl	%esi, 8(%r13)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L240:
	testl	%esi, %esi
	je	.L234
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%r13, %rdx
	jmp	.L229
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexCompile8appendOpEii.cold, @function
_ZN6icu_6712RegexCompile8appendOpEii.cold:
.LFSB3179:
.L224:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	call	abort@PLT
	.cfi_endproc
.LFE3179:
	.text
	.size	_ZN6icu_6712RegexCompile8appendOpEii, .-_ZN6icu_6712RegexCompile8appendOpEii
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexCompile8appendOpEii.cold, .-_ZN6icu_6712RegexCompile8appendOpEii.cold
.LCOLDE3:
	.text
.LHOTE3:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile11fixLiteralsEa
	.type	_ZN6icu_6712RegexCompile11fixLiteralsEa, @function
_ZN6icu_6712RegexCompile11fixLiteralsEa:
.LFB3176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	movswl	312(%rdi), %esi
	testw	%si, %si
	js	.L242
	sarl	$5, %esi
.L243:
	testl	%esi, %esi
	jne	.L293
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	leaq	304(%r12), %r14
	movl	$-1, %edx
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	movl	%eax, %r13d
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r15d
	testb	%bl, %bl
	jne	.L294
	testb	$2, 292(%r12)
	jne	.L295
	testl	%r13d, %r13d
	je	.L255
.L254:
	cmpw	$0, 312(%r12)
	js	.L296
.L257:
	movq	16(%r12), %rdx
	movzwl	48(%rdx), %eax
	testw	%ax, %ax
	js	.L297
	testb	$2, 292(%r12)
	jne	.L272
.L273:
	movswl	%ax, %edx
	sarl	$5, %edx
.L266:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
.L264:
	movswl	312(%r12), %edx
	testw	%dx, %dx
	js	.L267
	sarl	$5, %edx
.L268:
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	16(%r12), %rax
	movswl	312(%r12), %ecx
	leaq	40(%rax), %rdi
	testw	%cx, %cx
	js	.L269
	sarl	$5, %ecx
.L270:
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L242:
	movl	316(%rdi), %esi
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L295:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8foldCaseEj@PLT
	movswl	312(%r12), %esi
	testw	%si, %si
	js	.L252
	sarl	$5, %esi
.L253:
	movl	$-1, %edx
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	movl	%eax, %ebx
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r15d
	testl	%ebx, %ebx
	jne	.L254
	testb	$2, 292(%r12)
	je	.L255
	movl	$34, %esi
	movl	%eax, %edi
	call	u_hasBinaryProperty_67@PLT
	testb	%al, %al
	jne	.L298
.L255:
	movl	%r15d, %edx
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
.L256:
	movzwl	312(%r12), %edx
	movl	$2, %eax
	movl	%edx, %ecx
	andl	$31, %ecx
	andl	$1, %edx
	cmove	%ecx, %eax
	movw	%ax, 312(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	cmpl	$16777215, 316(%r12)
	jle	.L257
.L258:
	movl	$66324, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	16(%r12), %rdx
	testb	$2, 292(%r12)
	movzwl	48(%rdx), %eax
	je	.L261
	testw	%ax, %ax
	js	.L262
.L272:
	movswl	%ax, %edx
	sarl	$5, %edx
.L263:
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L294:
	movzwl	312(%r12), %eax
	testl	%r13d, %r13d
	jne	.L246
	testb	$1, %al
	jne	.L299
.L246:
	testw	%ax, %ax
	js	.L248
	movswl	%ax, %edx
	sarl	$5, %edx
.L249:
	cmpl	%r13d, %edx
	jbe	.L247
	cmpl	$1023, %r13d
	jg	.L250
	andl	$31, %eax
	sall	$5, %r13d
	orl	%eax, %r13d
	movw	%r13w, 312(%r12)
.L247:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexCompile11fixLiteralsEa
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	movl	316(%r12), %ecx
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L267:
	movl	316(%r12), %edx
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L297:
	cmpl	$16777215, 52(%rdx)
	jg	.L258
	testb	$2, 292(%r12)
	je	.L265
	.p2align 4,,10
	.p2align 3
.L262:
	movl	52(%rdx), %edx
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L261:
	testw	%ax, %ax
	jns	.L273
.L265:
	movl	52(%rdx), %edx
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L252:
	movl	316(%r12), %esi
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L298:
	movl	%r15d, %edx
	movl	$39, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L299:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L248:
	movl	316(%r12), %edx
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L250:
	orl	$-32, %eax
	movl	%r13d, 316(%r12)
	movw	%ax, 312(%r12)
	jmp	.L247
	.cfi_endproc
.LFE3176:
	.size	_ZN6icu_6712RegexCompile11fixLiteralsEa, .-_ZN6icu_6712RegexCompile11fixLiteralsEa
	.section	.text.unlikely
	.align 2
.LCOLDB4:
	.text
.LHOTB4:
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexCompile21compileInlineIntervalEv.part.0, @function
_ZN6icu_6712RegexCompile21compileInlineIntervalEv.part.0:
.LFB4190:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
	movl	8(%rdi), %eax
	cmpl	412(%rbx), %eax
	je	.L346
	movl	%eax, %r12d
	subl	$1, %r12d
	js	.L303
	movq	24(%rdi), %rsi
	movslq	%r12d, %r8
	cmpb	$5, 3(%rsi,%r8,8)
	jne	.L304
	movl	420(%rbx), %edx
	leal	-2(%rax), %r12d
	testl	%edx, %edx
	je	.L324
.L305:
	leal	-1(%rax), %ecx
	cmpl	$1, %edx
	je	.L330
	xorl	%r8d, %r8d
	cmpl	%r12d, %ecx
	je	.L330
.L300:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movl	420(%rbx), %edx
	testl	%edx, %edx
	je	.L324
	movl	%r12d, %ecx
	xorl	%r14d, %r14d
.L310:
	movq	8(%rbx), %rsi
	movl	416(%rbx), %eax
	movl	(%rsi), %esi
	testl	%esi, %esi
	jg	.L328
	leal	(%rcx,%rdx,2), %r13d
	subl	%eax, %r13d
	cmpl	$16777215, %r13d
	ja	.L343
	orl	$100663296, %r13d
	testl	%eax, %eax
	je	.L347
.L313:
	cmpl	$1, %edx
	jle	.L345
	movl	$1, %r12d
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L317:
	addl	$1, %r12d
	cmpl	%r12d, 420(%rbx)
	jle	.L345
.L323:
	cmpl	416(%rbx), %r12d
	jge	.L348
.L315:
	movq	8(%rbx), %rdx
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L317
	movq	16(%rbx), %rax
	movq	32(%rax), %r15
	movslq	8(%r15), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L318
	cmpl	12(%r15), %esi
	jle	.L329
.L318:
	movq	%r15, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L320
	movq	16(%rbx), %rax
	movq	32(%rax), %rdx
.L321:
	cmpl	$16777200, 8(%rdx)
	jle	.L317
	movq	8(%rbx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L317
	movl	$66324, %esi
	movq	%rbx, %rdi
	addl	$1, %r12d
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	cmpl	%r12d, 420(%rbx)
	jg	.L323
	.p2align 4,,10
	.p2align 3
.L345:
	movl	$1, %r8d
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L348:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEi
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L320:
	movq	16(%rbx), %rdx
	movslq	8(%r15), %rax
	movq	32(%rdx), %rdx
	leal	1(%rax), %esi
.L319:
	movq	24(%r15), %rcx
	movq	%r14, (%rcx,%rax,8)
	movl	%esi, 8(%r15)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L329:
	movq	%r15, %rdx
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L346:
	movl	420(%rbx), %edx
	movl	408(%rbx), %r12d
	testl	%edx, %edx
	jne	.L305
.L324:
	movl	%r12d, %esi
	call	_ZN6icu_679UVector647setSizeEi@PLT
	cmpl	%r12d, 408(%rbx)
	jl	.L306
	movl	$-1, 408(%rbx)
.L306:
	cmpl	%r12d, 412(%rbx)
	jl	.L345
	movl	$-1, 412(%rbx)
	movl	$1, %r8d
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L328:
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jne	.L313
.L347:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8insertOpEi
	movq	16(%rbx), %rax
	movl	%r12d, %edx
	movslq	%r13d, %rsi
	movq	32(%rax), %rdi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movl	420(%rbx), %edx
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L330:
	cmpl	%r12d, %eax
	jle	.L327
	testl	%r12d, %r12d
	jns	.L349
.L327:
	xorl	%r14d, %r14d
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L304:
	movl	420(%rbx), %edx
	testl	%edx, %edx
	je	.L324
	movl	%r12d, %ecx
.L325:
	movslq	(%rsi,%r8,8), %r14
	jmp	.L310
.L349:
	movq	24(%rdi), %rsi
	movslq	%r12d, %r8
	jmp	.L325
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexCompile21compileInlineIntervalEv.part.0.cold, @function
_ZN6icu_6712RegexCompile21compileInlineIntervalEv.part.0.cold:
.LFSB4190:
.L343:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE4190:
	.text
	.size	_ZN6icu_6712RegexCompile21compileInlineIntervalEv.part.0, .-_ZN6icu_6712RegexCompile21compileInlineIntervalEv.part.0
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexCompile21compileInlineIntervalEv.part.0.cold, .-_ZN6icu_6712RegexCompile21compileInlineIntervalEv.part.0.cold
.LCOLDE4:
	.text
.LHOTE4:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile21compileInlineIntervalEv
	.type	_ZN6icu_6712RegexCompile21compileInlineIntervalEv, @function
_ZN6icu_6712RegexCompile21compileInlineIntervalEv:
.LFB3187:
	.cfi_startproc
	endbr64
	movl	420(%rdi), %eax
	cmpl	$10, %eax
	jg	.L350
	cmpl	416(%rdi), %eax
	jge	.L354
.L350:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	jmp	_ZN6icu_6712RegexCompile21compileInlineIntervalEv.part.0
	.cfi_endproc
.LFE3187:
	.size	_ZN6icu_6712RegexCompile21compileInlineIntervalEv, .-_ZN6icu_6712RegexCompile21compileInlineIntervalEv
	.section	.text.unlikely
	.align 2
.LCOLDB5:
	.text
.LHOTB5:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile15compileIntervalEii
	.type	_ZN6icu_6712RegexCompile15compileIntervalEii, @function
_ZN6icu_6712RegexCompile15compileIntervalEii:
.LFB3186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movl	$1, %esi
	subq	$24, %rsp
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movq	16(%r12), %rax
	movq	32(%rax), %rdi
	movl	8(%rdi), %eax
	cmpl	412(%r12), %eax
	je	.L393
	movl	%eax, %r13d
	subl	$1, %r13d
	js	.L358
	movq	24(%rdi), %rdx
	movslq	%r13d, %rcx
	subl	$2, %eax
	cmpb	$5, 3(%rdx,%rcx,8)
	cmove	%eax, %r13d
.L358:
	movq	8(%r12), %rcx
	movl	$0, %esi
	movl	$117440512, %eax
	movl	%r13d, %edx
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	cmovle	%rax, %rsi
	call	_ZN6icu_679UVector6415insertElementAtEliR10UErrorCode@PLT
.L357:
	movl	%r13d, %esi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_6712RegexCompile8insertOpEi
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8insertOpEi
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8insertOpEi
	movq	8(%r12), %rcx
	movq	16(%r12), %rdx
	movl	420(%r12), %eax
	movl	(%rcx), %esi
	shrl	$31, %eax
	addl	$1, %eax
	testl	%esi, %esi
	jle	.L394
.L365:
	movq	32(%rdx), %rdi
	movq	%r15, %rsi
	movl	%r13d, %edx
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movq	16(%r12), %rax
	movq	32(%rax), %rdi
	movq	8(%r12), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L378
	movl	8(%rdi), %esi
	cmpl	$16777215, %esi
	ja	.L363
	orl	$520093696, %esi
	movslq	%esi, %rsi
.L368:
	leal	1(%r13), %edx
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movq	16(%r12), %rax
	leal	2(%r13), %edx
	movslq	416(%r12), %rsi
	movq	32(%rax), %rdi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movq	16(%r12), %rax
	leal	3(%r13), %edx
	movslq	420(%r12), %rsi
	movq	32(%rax), %rdi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movl	416(%r12), %edx
	testl	$-16777216, %edx
	jne	.L369
	movl	420(%r12), %eax
	testl	%eax, %eax
	jle	.L370
	testl	$-16777216, %eax
	jne	.L369
	.p2align 4,,10
	.p2align 3
.L370:
	cmpl	$-1, %eax
	je	.L355
	cmpl	%edx, %eax
	jl	.L395
.L355:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore_state
	movl	$66311, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movl	416(%r12), %edx
	movl	420(%r12), %eax
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L395:
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$66313, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	movl	128(%rdx), %ecx
	testl	%ecx, %ecx
	js	.L396
	addl	%ecx, %eax
	movl	%eax, 128(%rdx)
	cmpl	$16777199, %eax
	jg	.L397
.L364:
	cmpl	$255, %ebx
	ja	.L363
	cmpl	$16777215, %ecx
	jg	.L398
	sall	$24, %ebx
	movq	16(%r12), %rdx
	orl	%ecx, %ebx
	movslq	%ebx, %r15
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L378:
	xorl	%esi, %esi
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L393:
	movl	408(%r12), %r13d
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L396:
	movl	$66304, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%r12), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L361
	cmpl	$255, %ebx
	ja	.L399
	xorl	%ecx, %ecx
	sall	$24, %ebx
	movq	16(%r12), %rdx
	orl	%ecx, %ebx
	movslq	%ebx, %r15
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L397:
	movl	$66324, %esi
	movq	%r12, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%r12), %rax
	movl	-52(%rbp), %ecx
	movl	(%rax), %edx
	testl	%edx, %edx
	jle	.L364
.L361:
	movq	16(%r12), %rdx
	jmp	.L365
.L399:
	jmp	.L363
.L398:
	jmp	.L363
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexCompile15compileIntervalEii.cold, @function
_ZN6icu_6712RegexCompile15compileIntervalEii.cold:
.LFSB3186:
.L363:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3186:
	.text
	.size	_ZN6icu_6712RegexCompile15compileIntervalEii, .-_ZN6icu_6712RegexCompile15compileIntervalEii
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexCompile15compileIntervalEii.cold, .-_ZN6icu_6712RegexCompile15compileIntervalEii.cold
.LCOLDE5:
	.text
.LHOTE5:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile11blockTopLocEa
	.type	_ZN6icu_6712RegexCompile11blockTopLocEa, @function
_ZN6icu_6712RegexCompile11blockTopLocEa:
.LFB3183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movswl	312(%rdi), %esi
	testw	%si, %si
	js	.L401
	sarl	$5, %esi
.L402:
	testl	%esi, %esi
	jne	.L424
.L403:
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
	movl	8(%rdi), %eax
	cmpl	%eax, 412(%rbx)
	je	.L425
	movl	%eax, %r13d
	subl	$1, %r13d
	js	.L411
	movq	24(%rdi), %rdx
	movslq	%r13d, %rcx
	subl	$2, %eax
	cmpb	$5, 3(%rdx,%rcx,8)
	cmove	%eax, %r13d
.L411:
	testb	%r12b, %r12b
	je	.L400
	movq	8(%rbx), %rcx
	movl	$0, %esi
	movl	%r13d, %edx
	movl	(%rcx), %eax
	testl	%eax, %eax
	movl	$117440512, %eax
	cmovle	%rax, %rsi
	call	_ZN6icu_679UVector6415insertElementAtEliR10UErrorCode@PLT
.L400:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	leaq	304(%rbx), %r14
	movl	$-1, %edx
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	movl	%eax, %r13d
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movzwl	312(%rbx), %edx
	movl	%eax, %r15d
	testl	%r13d, %r13d
	jne	.L404
	testb	$1, %dl
	jne	.L426
.L404:
	testw	%dx, %dx
	js	.L406
	movswl	%dx, %eax
	sarl	$5, %eax
.L407:
	cmpl	%r13d, %eax
	jbe	.L405
	cmpl	$1023, %r13d
	jg	.L408
	andl	$31, %edx
	sall	$5, %r13d
	orl	%edx, %r13d
	movw	%r13w, 312(%rbx)
.L405:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L401:
	movl	316(%rdi), %esi
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L425:
	movl	408(%rbx), %r13d
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L408:
	orl	$-32, %edx
	movl	%r13d, 316(%rbx)
	movw	%dx, 312(%rbx)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L406:
	movl	316(%rbx), %eax
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L426:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L405
	.cfi_endproc
.LFE3183:
	.size	_ZN6icu_6712RegexCompile11blockTopLocEa, .-_ZN6icu_6712RegexCompile11blockTopLocEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile10nextCharLLEv
	.type	_ZN6icu_6712RegexCompile10nextCharLLEv, @function
_ZN6icu_6712RegexCompile10nextCharLLEv:
.LFB3195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	68(%rdi), %eax
	cmpl	$-1, %eax
	je	.L428
	movl	$-1, 68(%rdi)
.L427:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	movl	40(%rdi), %edx
	cmpl	44(%rdi), %edx
	jge	.L430
	movq	48(%rdi), %rcx
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %eax
	cmpw	$-10241, %ax
	jbe	.L439
.L430:
	call	utext_next32_67@PLT
	cmpl	$-1, %eax
	je	.L427
.L431:
	cmpl	$13, %eax
	sete	%cl
	cmpl	$133, %eax
	sete	%dl
	orb	%dl, %cl
	jne	.L432
	cmpl	$8232, %eax
	jne	.L440
.L432:
	addq	$1, 48(%rbx)
	movq	$0, 56(%rbx)
.L434:
	movl	%eax, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	addl	$1, %edx
	movl	%edx, 40(%rdi)
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L440:
	cmpl	$10, %eax
	je	.L441
	addq	$1, 56(%rbx)
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L441:
	cmpl	$13, 64(%rbx)
	jne	.L432
	jmp	.L434
	.cfi_endproc
.LFE3195:
	.size	_ZN6icu_6712RegexCompile10nextCharLLEv, .-_ZN6icu_6712RegexCompile10nextCharLLEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile10peekCharLLEv
	.type	_ZN6icu_6712RegexCompile10peekCharLLEv, @function
_ZN6icu_6712RegexCompile10peekCharLLEv:
.LFB3196:
	.cfi_startproc
	endbr64
	movl	68(%rdi), %eax
	cmpl	$-1, %eax
	je	.L457
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	movl	40(%rdi), %edx
	cmpl	44(%rdi), %edx
	jge	.L444
	movq	48(%rdi), %rcx
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %eax
	cmpw	$-10241, %ax
	ja	.L444
	addl	$1, %edx
	movl	%edx, 40(%rdi)
.L445:
	cmpl	$13, %eax
	sete	%cl
	cmpl	$133, %eax
	sete	%dl
	orb	%dl, %cl
	jne	.L447
	cmpl	$8232, %eax
	je	.L447
	cmpl	$10, %eax
	je	.L458
	addq	$1, 56(%rbx)
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L444:
	call	utext_next32_67@PLT
	cmpl	$-1, %eax
	jne	.L445
	movl	%eax, 68(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	cmpl	$13, 64(%rbx)
	je	.L449
	.p2align 4,,10
	.p2align 3
.L447:
	addq	$1, 48(%rbx)
	movq	$0, 56(%rbx)
.L449:
	movl	%eax, 64(%rbx)
	movl	%eax, 68(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3196:
	.size	_ZN6icu_6712RegexCompile10peekCharLLEv, .-_ZN6icu_6712RegexCompile10peekCharLLEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompileD2Ev
	.type	_ZN6icu_6712RegexCompileD2Ev, @function
_ZN6icu_6712RegexCompileD2Ev:
.LFB3168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712RegexCompileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	520(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L460
	movq	(%rdi), %rax
	call	*8(%rax)
.L460:
	leaq	472(%rbx), %rdi
	call	_ZN6icu_676UStackD1Ev@PLT
	leaq	432(%rbx), %rdi
	call	_ZN6icu_676UStackD1Ev@PLT
	leaq	376(%rbx), %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	addq	$8, %rsp
	leaq	304(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE3168:
	.size	_ZN6icu_6712RegexCompileD2Ev, .-_ZN6icu_6712RegexCompileD2Ev
	.globl	_ZN6icu_6712RegexCompileD1Ev
	.set	_ZN6icu_6712RegexCompileD1Ev,_ZN6icu_6712RegexCompileD2Ev
	.section	.text.unlikely
	.align 2
.LCOLDB6:
	.text
.LHOTB6:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile14minMatchLengthEii
	.type	_ZN6icu_6712RegexCompile14minMatchLengthEii, @function
_ZN6icu_6712RegexCompile14minMatchLengthEii:
.LFB3191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L558
.L465:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L559
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L558:
	.cfi_restore_state
	leal	2(%r13), %eax
	leaq	-96(%rbp), %r15
	movq	%rdi, %r14
	movl	%esi, %ebx
	movl	%eax, %r12d
	movl	%eax, %esi
	movq	%r15, %rdi
	movl	%eax, -104(%rbp)
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_679UVector327setSizeEi@PLT
	leal	1(%r13), %eax
	movl	%eax, -116(%rbp)
	cmpl	%eax, %ebx
	jg	.L470
	movl	%ebx, -112(%rbp)
	movl	-104(%rbp), %r12d
	.p2align 4,,10
	.p2align 3
.L471:
	movl	%ebx, %edx
	movl	$2147483647, %esi
	movq	%r15, %rdi
	addl	$1, %ebx
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	cmpl	%r12d, %ebx
	jne	.L471
	movl	-112(%rbp), %ebx
.L470:
	xorl	%r12d, %r12d
	cmpl	%ebx, %r13d
	jl	.L468
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %r9
	movl	%r13d, %ecx
	movl	%r12d, %r15d
	.p2align 4,,10
	.p2align 3
.L469:
	testl	%ebx, %ebx
	js	.L472
	movq	16(%r9), %rax
	movq	32(%rax), %rsi
	movl	8(%rsi), %r11d
	cmpl	%r11d, %ebx
	jge	.L473
	movq	24(%rsi), %rax
	movslq	%ebx, %r12
	movq	(%rax,%r12,8), %r10
	movl	-88(%rbp), %eax
	movl	%r10d, %edx
	shrl	$24, %r10d
	testl	%eax, %eax
	jle	.L474
	movl	%eax, %r14d
	xorl	%r13d, %r13d
	subl	%ebx, %r14d
	testl	%r14d, %r14d
	jle	.L475
.L516:
	movq	-72(%rbp), %r13
	movl	0(%r13,%r12,4), %r13d
.L475:
	cmpl	%r13d, %r15d
	cmovg	%r13d, %r15d
	cmpl	$58, %r10d
	ja	.L505
	leaq	.L515(%rip), %r14
	movl	%r10d, %r13d
	movslq	(%r14,%r13,4), %r13
	addq	%r14, %r13
	notrack jmp	*%r13
	.section	.rodata
	.align 4
	.align 4
.L515:
	.long	.L556-.L515
	.long	.L513-.L515
	.long	.L556-.L515
	.long	.L476-.L515
	.long	.L484-.L515
	.long	.L556-.L515
	.long	.L482-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L476-.L515
	.long	.L476-.L515
	.long	.L476-.L515
	.long	.L542-.L515
	.long	.L505-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L476-.L515
	.long	.L556-.L515
	.long	.L476-.L515
	.long	.L476-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L487-.L515
	.long	.L487-.L515
	.long	.L476-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L478-.L515
	.long	.L491-.L515
	.long	.L556-.L515
	.long	.L476-.L515
	.long	.L486-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L491-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L476-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L556-.L515
	.long	.L476-.L515
	.long	.L476-.L515
	.long	.L476-.L515
	.text
	.p2align 4,,10
	.p2align 3
.L472:
	testl	%r15d, %r15d
	cmovg	%r8d, %r15d
.L556:
	addl	$1, %ebx
.L477:
	cmpl	%ebx, %ecx
	jge	.L469
	movl	%r15d, %r12d
	movq	%rdi, %r15
.L468:
	movl	-116(%rbp), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	js	.L506
	movl	-88(%rbp), %edx
	testl	%edx, %edx
	jle	.L506
	subl	%ecx, %edx
	testl	%edx, %edx
	jle	.L506
	movq	-72(%rbp), %rdx
	movslq	%ecx, %rax
	movl	(%rdx,%rax,4), %eax
.L506:
	cmpl	%eax, %r12d
	movq	%r15, %rdi
	cmovg	%eax, %r12d
	call	_ZN6icu_679UVector32D1Ev@PLT
	jmp	.L465
.L491:
	xorl	%r13d, %r13d
	cmpl	$37, %r10d
	movl	%ecx, -120(%rbp)
	movq	24(%rsi), %r10
	sete	%r13b
	leaq	8(,%r12,8), %r12
	addl	$1, %r13d
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L495:
	addl	$2, %r13d
	.p2align 4,,10
	.p2align 3
.L500:
	addq	$8, %r12
	movl	%r14d, %ebx
.L493:
	leal	1(%rbx), %r14d
	cmpl	8(%rsi), %r14d
	jge	.L500
	movq	(%r10,%r12), %rdx
	movl	%edx, %eax
	shrl	$24, %eax
	cmpl	$37, %eax
	je	.L495
	cmpl	$44, %eax
	jne	.L560
	addl	$1, %r13d
	jmp	.L500
.L487:
	leal	1(%rbx), %eax
	xorl	%edx, %edx
	cmpl	%r11d, %eax
	jge	.L488
	movq	24(%rsi), %rdx
	cltq
	movq	(%rdx,%rax,8), %rdx
	andl	$16777215, %edx
.L488:
	leal	2(%rbx), %eax
	cmpl	%r11d, %eax
	jge	.L553
	movq	24(%rsi), %rsi
	cltq
	movl	(%rsi,%rax,8), %eax
	testl	%eax, %eax
	je	.L553
	addl	$4, %ebx
	jmp	.L477
.L478:
	leal	1(%rbx), %esi
.L479:
	andl	$16777215, %edx
	leal	1(%rsi), %ebx
	cmpl	%edx, %esi
	jle	.L480
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.L477
	subl	%ebx, %eax
	testl	%eax, %eax
	jle	.L477
.L514:
	movq	-72(%rbp), %rax
	movslq	%ebx, %r12
	movl	(%rax,%r12,4), %r15d
	jmp	.L477
.L486:
	movl	$2147483647, %eax
	addl	$2, %ebx
	movl	%eax, %edx
	subl	%r15d, %edx
	addl	$1, %r15d
	cmpl	$1, %edx
	cmovle	%eax, %r15d
	jmp	.L477
.L482:
	andl	$16777215, %edx
	leal	1(%rbx), %esi
	cmpl	%edx, %ebx
	jge	.L527
	xorl	%r10d, %r10d
	testl	%eax, %eax
	jle	.L483
	subl	%edx, %eax
	testl	%eax, %eax
	jle	.L483
	movq	-72(%rbp), %r10
	movslq	%edx, %rax
	movl	(%r10,%rax,4), %r10d
.L483:
	movl	%esi, %ebx
	cmpl	%r15d, %r10d
	jle	.L477
.L554:
	movl	%r15d, %esi
	movl	%ecx, -120(%rbp)
	movq	%r9, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movq	-104(%rbp), %rdi
	movq	-112(%rbp), %r9
	xorl	%r8d, %r8d
	movl	-120(%rbp), %ecx
	jmp	.L477
.L484:
	leal	1(%rbx), %edx
	xorl	%eax, %eax
	cmpl	%r11d, %edx
	jge	.L485
	movq	24(%rsi), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
	andl	$16777215, %eax
.L485:
	movl	$2147483647, %edx
	addl	$2, %ebx
	movl	%edx, %esi
	subl	%r15d, %esi
	addl	%eax, %r15d
	cmpl	%eax, %esi
	cmovle	%edx, %r15d
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L473:
	movl	-88(%rbp), %eax
	testl	%eax, %eax
	jle	.L472
	movl	%eax, %edx
	subl	%ebx, %edx
	testl	%edx, %edx
	jle	.L472
	xorl	%r10d, %r10d
	xorl	%edx, %edx
	movslq	%ebx, %r12
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L474:
	testl	%r15d, %r15d
	cmovg	%r8d, %r15d
	cmpl	$58, %r10d
	ja	.L505
	leaq	.L512(%rip), %r14
	movl	%r10d, %r13d
	movslq	(%r14,%r13,4), %r13
	addq	%r14, %r13
	notrack jmp	*%r13
	.section	.rodata
	.align 4
	.align 4
.L512:
	.long	.L556-.L512
	.long	.L507-.L512
	.long	.L556-.L512
	.long	.L510-.L512
	.long	.L484-.L512
	.long	.L556-.L512
	.long	.L482-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L510-.L512
	.long	.L510-.L512
	.long	.L510-.L512
	.long	.L542-.L512
	.long	.L505-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L510-.L512
	.long	.L556-.L512
	.long	.L510-.L512
	.long	.L510-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L487-.L512
	.long	.L487-.L512
	.long	.L510-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L478-.L512
	.long	.L491-.L512
	.long	.L556-.L512
	.long	.L510-.L512
	.long	.L486-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L491-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L510-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L556-.L512
	.long	.L510-.L512
	.long	.L510-.L512
	.long	.L510-.L512
	.text
	.p2align 4,,10
	.p2align 3
.L480:
	xorl	%esi, %esi
	testl	%eax, %eax
	jle	.L481
	subl	%edx, %eax
	testl	%eax, %eax
	jle	.L481
	movq	-72(%rbp), %rsi
	movslq	%edx, %rax
	movl	(%rsi,%rax,4), %esi
.L481:
	cmpl	%r15d, %esi
	jle	.L477
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L476:
	movl	$2147483647, %eax
	addl	$1, %ebx
	subl	%r15d, %eax
	cmpl	$1, %eax
	jg	.L511
	movl	$2147483647, %r15d
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L527:
	movl	%esi, %ebx
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L553:
	leal	1(%rdx), %ebx
	jmp	.L477
.L513:
	addl	$1, %ebx
	xorl	%r15d, %r15d
	subl	%ebx, %eax
	testl	%eax, %eax
	jg	.L514
	jmp	.L477
.L507:
	addl	$1, %ebx
	xorl	%r15d, %r15d
	jmp	.L477
.L510:
	addl	$1, %ebx
.L511:
	addl	$1, %r15d
	jmp	.L477
.L542:
	movl	%ebx, %esi
	jmp	.L479
.L559:
	call	__stack_chk_fail@PLT
.L560:
	cmpl	$38, %eax
	jne	.L561
.L501:
	subl	$1, %r13d
	jne	.L500
	movl	-120(%rbp), %ecx
	addl	$2, %ebx
	jmp	.L477
.L561:
	cmpl	$48, %eax
	je	.L501
	cmpl	$6, %eax
	jne	.L500
	andl	$16777215, %edx
	cmpl	%edx, %r14d
	jge	.L500
	movl	-88(%rbp), %eax
	xorl	%r11d, %r11d
	testl	%eax, %eax
	jle	.L504
	subl	%edx, %eax
	testl	%eax, %eax
	jle	.L504
	movq	-72(%rbp), %r11
	movslq	%edx, %rax
	movl	(%r11,%rax,4), %r11d
.L504:
	cmpl	%r15d, %r11d
	jle	.L500
	movl	%r15d, %esi
	movq	%r9, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %rdi
	xorl	%r8d, %r8d
	movq	16(%r9), %rax
	movq	32(%rax), %rsi
	movq	24(%rsi), %r10
	jmp	.L500
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexCompile14minMatchLengthEii.cold, @function
_ZN6icu_6712RegexCompile14minMatchLengthEii.cold:
.LFSB3191:
.L505:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3191:
	.text
	.size	_ZN6icu_6712RegexCompile14minMatchLengthEii, .-_ZN6icu_6712RegexCompile14minMatchLengthEii
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexCompile14minMatchLengthEii.cold, .-_ZN6icu_6712RegexCompile14minMatchLengthEii.cold
.LCOLDE6:
	.text
.LHOTE6:
	.section	.text.unlikely
	.align 2
.LCOLDB7:
	.text
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile14maxMatchLengthEii
	.type	_ZN6icu_6712RegexCompile14maxMatchLengthEii, @function
_ZN6icu_6712RegexCompile14maxMatchLengthEii:
.LFB3192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	%edx, -104(%rbp)
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L642
.L562:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L643
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	.cfi_restore_state
	movl	-104(%rbp), %eax
	leaq	-96(%rbp), %r13
	movl	%esi, %ebx
	movq	%rdi, %r14
	movq	%r13, %rdi
	leal	1(%rax), %r15d
	movl	%r15d, %esi
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_679UVector327setSizeEi@PLT
	cmpl	%ebx, -104(%rbp)
	jl	.L564
	movl	%ebx, %r12d
	.p2align 4,,10
	.p2align 3
.L565:
	movl	%r12d, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	addl	$1, %r12d
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	cmpl	%r15d, %r12d
	jne	.L565
	movq	%r13, %r11
	movl	-104(%rbp), %r13d
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	leaq	.L601(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L594:
	movq	16(%r14), %rax
	movq	32(%rax), %rsi
	testl	%ebx, %ebx
	js	.L566
	movl	8(%rsi), %edi
	cmpl	%edi, %ebx
	jge	.L567
	movq	24(%rsi), %rax
	movslq	%ebx, %r10
	movl	-88(%rbp), %r9d
	movq	(%rax,%r10,8), %rax
	movl	%eax, %edx
	shrl	$24, %eax
	testl	%r9d, %r9d
	jg	.L644
	testl	%r12d, %r12d
	cmovs	%r15d, %r12d
	cmpl	$58, %eax
	ja	.L570
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L601:
	.long	.L595-.L601
	.long	.L600-.L601
	.long	.L595-.L601
	.long	.L622-.L601
	.long	.L575-.L601
	.long	.L595-.L601
	.long	.L578-.L601
	.long	.L595-.L601
	.long	.L595-.L601
	.long	.L595-.L601
	.long	.L571-.L601
	.long	.L571-.L601
	.long	.L571-.L601
	.long	.L576-.L601
	.long	.L570-.L601
	.long	.L576-.L601
	.long	.L595-.L601
	.long	.L595-.L601
	.long	.L576-.L601
	.long	.L623-.L601
	.long	.L595-.L601
	.long	.L571-.L601
	.long	.L571-.L601
	.long	.L595-.L601
	.long	.L595-.L601
	.long	.L577-.L601
	.long	.L577-.L601
	.long	.L571-.L601
	.long	.L570-.L601
	.long	.L570-.L601
	.long	.L595-.L601
	.long	.L595-.L601
	.long	.L595-.L601
	.long	.L595-.L601
	.long	.L623-.L601
	.long	.L595-.L601
	.long	.L576-.L601
	.long	.L595-.L601
	.long	.L595-.L601
	.long	.L571-.L601
	.long	.L575-.L601
	.long	.L623-.L601
	.long	.L595-.L601
	.long	.L595-.L601
	.long	.L574-.L601
	.long	.L595-.L601
	.long	.L595-.L601
	.long	.L595-.L601
	.long	.L595-.L601
	.long	.L571-.L601
	.long	.L623-.L601
	.long	.L623-.L601
	.long	.L623-.L601
	.long	.L595-.L601
	.long	.L595-.L601
	.long	.L595-.L601
	.long	.L571-.L601
	.long	.L571-.L601
	.long	.L571-.L601
	.text
	.p2align 4,,10
	.p2align 3
.L571:
	movl	$2147483647, %eax
	subl	%r12d, %eax
	cmpl	$2, %eax
	jle	.L623
	addl	$2, %r12d
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L577:
	leal	1(%rbx), %eax
	cmpl	%edi, %eax
	jl	.L645
	leal	4(%rbx), %r10d
	xorl	%r9d, %r9d
.L597:
	addl	$3, %ebx
	cmpl	%edi, %ebx
	jge	.L619
	movq	24(%rsi), %rax
	movslq	%ebx, %rbx
	movq	(%rax,%rbx,8), %rbx
	cmpl	$-1, %ebx
	jne	.L646
	.p2align 4,,10
	.p2align 3
.L623:
	movq	%r11, %r13
	movl	$2147483647, %r12d
.L564:
	movq	%r13, %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L576:
	andl	$16777215, %edx
	cmpl	%ebx, %edx
	jl	.L623
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L584
	subl	%edx, %r9d
	testl	%r9d, %r9d
	jle	.L584
	movq	-72(%rbp), %rsi
	movslq	%edx, %rax
	movl	(%rsi,%rax,4), %eax
.L584:
	addl	$1, %ebx
	cmpl	%r12d, %eax
	jl	.L647
.L638:
	xorl	%r12d, %r12d
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L575:
	addl	$1, %ebx
	xorl	%edx, %edx
	cmpl	%edi, %ebx
	jge	.L588
	movq	24(%rsi), %rdx
	movslq	%ebx, %rax
	movq	(%rdx,%rax,8), %rdx
	andl	$16777215, %edx
.L588:
	movl	$2147483647, %eax
	subl	%r12d, %eax
	cmpl	%edx, %eax
	jle	.L623
	addl	%edx, %r12d
	.p2align 4,,10
	.p2align 3
.L573:
	cmpl	$2147483647, %r12d
	je	.L623
.L595:
	addl	$1, %ebx
.L585:
	cmpl	%ebx, %r13d
	jge	.L594
	movq	%r11, %r13
	jmp	.L564
.L578:
	andl	$16777215, %edx
	cmpl	%ebx, %edx
	jle	.L623
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L586
	subl	%edx, %r9d
	testl	%r9d, %r9d
	jle	.L586
	movq	-72(%rbp), %rsi
	movslq	%edx, %rax
	movl	(%rsi,%rax,4), %eax
.L586:
	cmpl	%r12d, %eax
	jge	.L573
	movq	%r11, %rdi
	movl	%r12d, %esi
	movq	%r11, -104(%rbp)
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movq	-104(%rbp), %r11
	leaq	.L601(%rip), %rcx
	jmp	.L573
.L574:
	addl	$1, %ebx
	andl	$16777215, %edx
	cmpl	%ebx, %r13d
	jle	.L573
	movslq	%ebx, %rax
	salq	$3, %rax
	.p2align 4,,10
	.p2align 3
.L593:
	testl	%ebx, %ebx
	js	.L591
	cmpl	%ebx, %edi
	jle	.L591
	movq	24(%rsi), %r9
	movq	(%r9,%rax), %r9
	movl	%r9d, %r10d
	shrl	$24, %r10d
	cmpl	$38, %r10d
	je	.L624
	cmpl	$48, %r10d
	jne	.L591
.L624:
	andl	$16777215, %r9d
	cmpl	%edx, %r9d
	je	.L573
.L591:
	addl	$1, %ebx
	addq	$8, %rax
	cmpl	%ebx, %r13d
	jne	.L593
	movl	%r13d, %ebx
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L566:
	testl	%r12d, %r12d
	cmovs	%r15d, %r12d
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L567:
	movl	-88(%rbp), %r9d
	testl	%r9d, %r9d
	jle	.L566
	movl	%r9d, %eax
	subl	%ebx, %eax
	testl	%eax, %eax
	jg	.L604
	xorl	%eax, %eax
	xorl	%edx, %edx
.L569:
	testl	%r12d, %r12d
	cmovs	%r15d, %r12d
	cmpl	$58, %eax
	ja	.L570
	leaq	.L603(%rip), %r8
	movslq	(%r8,%rax,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L603:
	.long	.L595-.L603
	.long	.L602-.L603
	.long	.L595-.L603
	.long	.L622-.L603
	.long	.L575-.L603
	.long	.L595-.L603
	.long	.L578-.L603
	.long	.L595-.L603
	.long	.L595-.L603
	.long	.L595-.L603
	.long	.L571-.L603
	.long	.L571-.L603
	.long	.L571-.L603
	.long	.L576-.L603
	.long	.L570-.L603
	.long	.L576-.L603
	.long	.L595-.L603
	.long	.L595-.L603
	.long	.L576-.L603
	.long	.L623-.L603
	.long	.L595-.L603
	.long	.L571-.L603
	.long	.L571-.L603
	.long	.L595-.L603
	.long	.L595-.L603
	.long	.L577-.L603
	.long	.L577-.L603
	.long	.L571-.L603
	.long	.L570-.L603
	.long	.L570-.L603
	.long	.L595-.L603
	.long	.L595-.L603
	.long	.L595-.L603
	.long	.L595-.L603
	.long	.L623-.L603
	.long	.L595-.L603
	.long	.L576-.L603
	.long	.L595-.L603
	.long	.L595-.L603
	.long	.L571-.L603
	.long	.L575-.L603
	.long	.L623-.L603
	.long	.L595-.L603
	.long	.L595-.L603
	.long	.L574-.L603
	.long	.L595-.L603
	.long	.L595-.L603
	.long	.L595-.L603
	.long	.L595-.L603
	.long	.L571-.L603
	.long	.L623-.L603
	.long	.L623-.L603
	.long	.L623-.L603
	.long	.L595-.L603
	.long	.L595-.L603
	.long	.L595-.L603
	.long	.L571-.L603
	.long	.L571-.L603
	.long	.L571-.L603
	.text
	.p2align 4,,10
	.p2align 3
.L644:
	movl	%r9d, %r8d
	subl	%ebx, %r8d
	testl	%r8d, %r8d
	jle	.L569
	movq	-72(%rbp), %r8
	movl	(%r8,%r10,4), %r10d
	cmpl	%r10d, %r12d
	cmovl	%r10d, %r12d
	cmpl	$58, %eax
	ja	.L570
	leaq	.L572(%rip), %r8
	movslq	(%r8,%rax,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L572:
	.long	.L573-.L572
	.long	.L602-.L572
	.long	.L573-.L572
	.long	.L622-.L572
	.long	.L575-.L572
	.long	.L573-.L572
	.long	.L578-.L572
	.long	.L573-.L572
	.long	.L573-.L572
	.long	.L573-.L572
	.long	.L571-.L572
	.long	.L571-.L572
	.long	.L571-.L572
	.long	.L576-.L572
	.long	.L570-.L572
	.long	.L576-.L572
	.long	.L573-.L572
	.long	.L573-.L572
	.long	.L576-.L572
	.long	.L623-.L572
	.long	.L573-.L572
	.long	.L571-.L572
	.long	.L571-.L572
	.long	.L573-.L572
	.long	.L573-.L572
	.long	.L577-.L572
	.long	.L577-.L572
	.long	.L571-.L572
	.long	.L570-.L572
	.long	.L570-.L572
	.long	.L573-.L572
	.long	.L573-.L572
	.long	.L573-.L572
	.long	.L573-.L572
	.long	.L623-.L572
	.long	.L573-.L572
	.long	.L576-.L572
	.long	.L573-.L572
	.long	.L573-.L572
	.long	.L571-.L572
	.long	.L575-.L572
	.long	.L623-.L572
	.long	.L573-.L572
	.long	.L573-.L572
	.long	.L574-.L572
	.long	.L573-.L572
	.long	.L573-.L572
	.long	.L573-.L572
	.long	.L573-.L572
	.long	.L571-.L572
	.long	.L623-.L572
	.long	.L623-.L572
	.long	.L623-.L572
	.long	.L573-.L572
	.long	.L573-.L572
	.long	.L573-.L572
	.long	.L571-.L572
	.long	.L571-.L572
	.long	.L571-.L572
	.text
	.p2align 4,,10
	.p2align 3
.L604:
	movq	-72(%rbp), %rdx
	movslq	%ebx, %rax
	movl	(%rdx,%rax,4), %eax
	cmpl	%eax, %r12d
	cmovl	%eax, %r12d
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L645:
	movq	24(%rsi), %rdx
	cltq
	leal	4(%rbx), %r10d
	movq	(%rdx,%rax,8), %r9
	andl	$16777215, %r9d
	cmpl	%r9d, %r10d
	jne	.L597
	movl	%r10d, %ebx
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L647:
	movl	%r12d, %esi
	movq	%r11, %rdi
	movq	%r11, -104(%rbp)
	xorl	%r12d, %r12d
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movq	-104(%rbp), %r11
	leaq	.L601(%rip), %rcx
	jmp	.L585
.L602:
	leal	1(%rbx), %eax
	subl	%eax, %r9d
	testl	%r9d, %r9d
	jle	.L613
	movq	-72(%rbp), %rdx
	cltq
	movl	(%rdx,%rax,4), %r12d
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L619:
	xorl	%ebx, %ebx
.L590:
	leal	-1(%r9), %edx
	movl	%r10d, %esi
	movq	%r14, %rdi
	movq	%r11, -104(%rbp)
	movl	%r9d, -108(%rbp)
	movslq	%r12d, %r12
	call	_ZN6icu_6712RegexCompile14maxMatchLengthEii
	movq	-104(%rbp), %r11
	cltq
	imulq	%rax, %rbx
	addq	%rbx, %r12
	cmpq	$2147483646, %r12
	jg	.L623
	movl	-108(%rbp), %r9d
	leaq	.L601(%rip), %rcx
	movl	%r9d, %ebx
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L646:
	movslq	%ebx, %rbx
	jmp	.L590
.L600:
	addl	$1, %ebx
	xorl	%r12d, %r12d
	jmp	.L585
.L622:
	movl	$2147483647, %eax
	movl	%r12d, %edi
	movl	%eax, %esi
	subl	%r12d, %esi
	cmpl	$1, %esi
	jle	.L623
	andl	$16777215, %edx
	addl	$1, %r12d
	cmpl	$65536, %edx
	jle	.L573
	subl	%r12d, %eax
	cmpl	$1, %eax
	jle	.L623
	leal	2(%rdi), %r12d
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L613:
	movl	%eax, %ebx
	jmp	.L638
.L643:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexCompile14maxMatchLengthEii.cold, @function
_ZN6icu_6712RegexCompile14maxMatchLengthEii.cold:
.LFSB3192:
.L570:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3192:
	.text
	.size	_ZN6icu_6712RegexCompile14maxMatchLengthEii, .-_ZN6icu_6712RegexCompile14maxMatchLengthEii
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexCompile14maxMatchLengthEii.cold, .-_ZN6icu_6712RegexCompile14maxMatchLengthEii.cold
.LCOLDE7:
	.text
.LHOTE7:
	.section	.text.unlikely
	.align 2
.LCOLDB8:
	.text
.LHOTB8:
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexCompile16handleCloseParenEv.part.0, @function
_ZN6icu_6712RegexCompile16handleCloseParenEv.part.0:
.LFB4197:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L649:
	leal	-1(%rax), %edx
	movq	400(%rbx), %rcx
	movslq	%edx, %rsi
	movl	%edx, 384(%rbx)
	movl	(%rcx,%rsi,4), %r12d
	testl	%r12d, %r12d
	js	.L651
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
.L650:
	movl	8(%rdi), %esi
	cmpl	%r12d, %esi
	jle	.L652
	movq	24(%rdi), %rdx
	movslq	%r12d, %rax
	orl	(%rdx,%rax,8), %esi
.L652:
	movslq	%esi, %rsi
	movl	%r12d, %edx
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movl	%r12d, 408(%rbx)
.L653:
	movl	384(%rbx), %eax
	testl	%eax, %eax
	jg	.L649
	movq	16(%rbx), %rax
	xorl	%r12d, %r12d
	movq	32(%rax), %rdi
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L651:
	testl	%edx, %edx
	je	.L654
	subl	$2, %eax
	movl	%eax, 384(%rbx)
	cltq
	movl	(%rcx,%rax,4), %edx
.L654:
	movl	%edx, 292(%rbx)
	leal	8(%r12), %edx
	cmpl	$-8, %r12d
	jb	.L655
	leaq	.L657(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L657:
	.long	.L663-.L657
	.long	.L662-.L657
	.long	.L656-.L657
	.long	.L661-.L657
	.long	.L660-.L657
	.long	.L659-.L657
	.long	.L658-.L657
	.long	.L656-.L657
	.text
.L661:
	movl	408(%rbx), %eax
	xorl	%r12d, %r12d
	subl	$1, %eax
	js	.L667
	movq	16(%rbx), %rdx
	movq	32(%rdx), %rdx
	cmpl	8(%rdx), %eax
	jge	.L667
	movq	24(%rdx), %rdx
	cltq
	movq	(%rdx,%rax,8), %r12
	andl	$16777215, %r12d
.L667:
	movl	%r12d, %edx
	movl	$38, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movl	%r12d, %edx
	movq	%rbx, %rdi
	movl	$38, %esi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	16(%rbx), %rax
	movl	408(%rbx), %edx
	movq	32(%rax), %rdi
	movq	8(%rbx), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L686
	movl	8(%rdi), %eax
	leal	-1(%rax), %esi
	cmpl	$16777215, %esi
	ja	.L677
	orl	$100663296, %esi
	movslq	%esi, %rsi
.L702:
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
.L656:
	movq	16(%rbx), %rax
	movq	32(%rax), %rax
	movl	8(%rax), %eax
	movl	%eax, 412(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L659:
	.cfi_restore_state
	movl	408(%rbx), %eax
	xorl	%edx, %edx
	addl	$1, %eax
	js	.L665
	movq	16(%rbx), %rcx
	movq	32(%rcx), %rcx
	cmpl	8(%rcx), %eax
	jge	.L665
	movq	24(%rcx), %rdx
	cltq
	movq	(%rdx,%rax,8), %rdx
	andl	$16777215, %edx
.L665:
	movl	$33, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	jmp	.L656
.L658:
	movl	408(%rbx), %eax
	xorl	%edx, %edx
	addl	$1, %eax
	js	.L664
	movq	16(%rbx), %rcx
	movq	32(%rcx), %rcx
	cmpl	8(%rcx), %eax
	jge	.L664
	movq	24(%rcx), %rdx
	cltq
	movq	(%rdx,%rax,8), %rdx
	andl	$16777215, %edx
.L664:
	movl	$9, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	jmp	.L656
.L663:
	movl	408(%rbx), %eax
	xorl	%edx, %edx
	subl	$5, %eax
	js	.L674
	movq	16(%rbx), %rcx
	movq	32(%rcx), %rcx
	cmpl	8(%rcx), %eax
	jl	.L703
.L674:
	movl	$48, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	16(%rbx), %rax
	movl	408(%rbx), %esi
	movq	%rbx, %rdi
	movq	32(%rax), %rax
	movl	8(%rax), %r12d
	subl	$1, %r12d
	movl	%r12d, %edx
	call	_ZN6icu_6712RegexCompile14minMatchLengthEii
	movl	408(%rbx), %esi
	movl	%r12d, %edx
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	_ZN6icu_6712RegexCompile14maxMatchLengthEii
	movl	%eax, %r12d
	shrl	$24, %eax
	jne	.L672
	cmpl	$2147483647, %r12d
	je	.L672
	cmpl	$2147483647, %r13d
	cmove	%eax, %r13d
	movl	408(%rbx), %eax
	leal	-3(%rax), %edx
	movq	16(%rbx), %rax
	movslq	%r13d, %rsi
	movq	32(%rax), %rdi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movl	408(%rbx), %eax
	movslq	%r12d, %rsi
	leal	-2(%rax), %edx
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
	movq	8(%rbx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L691
	movl	8(%rdi), %esi
	cmpl	$16777215, %esi
	ja	.L677
	orl	$520093696, %esi
	movslq	%esi, %rsi
.L676:
	movl	408(%rbx), %eax
	leal	-1(%rax), %edx
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	jmp	.L656
.L662:
	movl	408(%rbx), %eax
	xorl	%r12d, %r12d
	subl	$4, %eax
	js	.L670
	movq	16(%rbx), %rdx
	movq	32(%rdx), %rdx
	cmpl	8(%rdx), %eax
	jl	.L704
.L670:
	movl	%r12d, %edx
	movl	$46, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movl	%r12d, %edx
	movl	$38, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	16(%rbx), %rax
	movl	408(%rbx), %esi
	movq	%rbx, %rdi
	movq	32(%rax), %rax
	movl	8(%rax), %r12d
	subl	$1, %r12d
	movl	%r12d, %edx
	call	_ZN6icu_6712RegexCompile14minMatchLengthEii
	movl	408(%rbx), %esi
	movl	%r12d, %edx
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	_ZN6icu_6712RegexCompile14maxMatchLengthEii
	movl	%eax, %r12d
	shrl	$24, %eax
	jne	.L672
	cmpl	$2147483647, %r12d
	je	.L672
	cmpl	$2147483647, %r13d
	cmove	%eax, %r13d
	movl	408(%rbx), %eax
	leal	-2(%rax), %edx
	movq	16(%rbx), %rax
	movslq	%r13d, %rsi
	movq	32(%rax), %rdi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movl	408(%rbx), %eax
	movslq	%r12d, %rsi
	leal	-1(%rax), %edx
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
	jmp	.L702
.L660:
	movl	408(%rbx), %eax
	xorl	%edx, %edx
	subl	$5, %eax
	js	.L666
	movq	16(%rbx), %rcx
	movq	32(%rcx), %rcx
	cmpl	8(%rcx), %eax
	jge	.L666
	movq	24(%rcx), %rdx
	cltq
	movq	(%rdx,%rax,8), %rdx
	andl	$16777215, %edx
.L666:
	movl	$38, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	jmp	.L656
.L672:
	movl	$66316, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	jmp	.L656
.L686:
	xorl	%esi, %esi
	jmp	.L702
.L704:
	movq	24(%rdx), %rdx
	cltq
	movq	(%rdx,%rax,8), %r12
	andl	$16777215, %r12d
	jmp	.L670
.L703:
	movq	24(%rcx), %rdx
	cltq
	movq	(%rdx,%rax,8), %rdx
	andl	$16777215, %edx
	jmp	.L674
.L691:
	xorl	%esi, %esi
	jmp	.L676
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexCompile16handleCloseParenEv.part.0.cold, @function
_ZN6icu_6712RegexCompile16handleCloseParenEv.part.0.cold:
.LFSB4197:
.L655:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	call	abort@PLT
.L677:
	call	abort@PLT
	.cfi_endproc
.LFE4197:
	.text
	.size	_ZN6icu_6712RegexCompile16handleCloseParenEv.part.0, .-_ZN6icu_6712RegexCompile16handleCloseParenEv.part.0
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexCompile16handleCloseParenEv.part.0.cold, .-_ZN6icu_6712RegexCompile16handleCloseParenEv.part.0.cold
.LCOLDE8:
	.text
.LHOTE8:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile16handleCloseParenEv
	.type	_ZN6icu_6712RegexCompile16handleCloseParenEv, @function
_ZN6icu_6712RegexCompile16handleCloseParenEv:
.LFB3184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	384(%rdi), %eax
	testl	%eax, %eax
	jle	.L746
	movzwl	312(%rdi), %eax
	testw	%ax, %ax
	js	.L707
	movswl	%ax, %esi
	sarl	$5, %esi
	testl	%esi, %esi
	je	.L709
.L750:
	leaq	304(%r12), %r14
	movl	$-1, %edx
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	movl	%eax, %ebx
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r13d
	testb	$2, 292(%r12)
	jne	.L747
	testl	%ebx, %ebx
	je	.L714
.L713:
	cmpw	$0, 312(%r12)
	js	.L748
.L716:
	movq	16(%r12), %rdx
	movzwl	48(%rdx), %eax
	testw	%ax, %ax
	js	.L749
	testb	$2, 292(%r12)
	jne	.L731
.L732:
	movswl	%ax, %edx
	sarl	$5, %edx
.L725:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
.L723:
	movzwl	312(%r12), %eax
	testw	%ax, %ax
	js	.L726
	movswl	%ax, %edx
	sarl	$5, %edx
.L727:
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	16(%r12), %rax
	leaq	40(%rax), %rdi
	movzwl	312(%r12), %eax
	testw	%ax, %ax
	js	.L728
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L729:
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L707:
	movl	316(%rdi), %esi
	testl	%esi, %esi
	jne	.L750
.L709:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexCompile16handleCloseParenEv.part.0
	.p2align 4,,10
	.p2align 3
.L746:
	.cfi_restore_state
	popq	%rbx
	movl	$66310, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	.p2align 4,,10
	.p2align 3
.L747:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8foldCaseEj@PLT
	movzwl	312(%r12), %eax
	testw	%ax, %ax
	js	.L711
	movswl	%ax, %esi
	sarl	$5, %esi
.L712:
	movl	$-1, %edx
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	movl	%eax, %ebx
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r13d
	testl	%ebx, %ebx
	jne	.L713
	testb	$2, 292(%r12)
	je	.L714
	movl	$34, %esi
	movl	%eax, %edi
	call	u_hasBinaryProperty_67@PLT
	testb	%al, %al
	jne	.L751
.L714:
	movl	%r13d, %edx
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
.L715:
	movzwl	312(%r12), %edx
	movl	$2, %eax
	popq	%rbx
	movq	%r12, %rdi
	movl	%edx, %ecx
	andl	$31, %ecx
	andl	$1, %edx
	cmove	%ecx, %eax
	movw	%ax, 312(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexCompile16handleCloseParenEv.part.0
.L748:
	.cfi_restore_state
	cmpl	$16777215, 316(%r12)
	jle	.L716
.L717:
	movl	$66324, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	16(%r12), %rdx
	testb	$2, 292(%r12)
	movzwl	48(%rdx), %eax
	je	.L720
	testw	%ax, %ax
	js	.L721
.L731:
	movswl	%ax, %edx
	sarl	$5, %edx
.L722:
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	jmp	.L723
.L728:
	movl	316(%r12), %ecx
	jmp	.L729
.L726:
	movl	316(%r12), %edx
	jmp	.L727
.L749:
	cmpl	$16777215, 52(%rdx)
	jg	.L717
	testb	$2, 292(%r12)
	je	.L724
.L721:
	movl	52(%rdx), %edx
	jmp	.L722
.L720:
	testw	%ax, %ax
	jns	.L732
.L724:
	movl	52(%rdx), %edx
	jmp	.L725
.L751:
	movl	%r13d, %edx
	movl	$39, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	jmp	.L715
.L711:
	movl	316(%r12), %esi
	jmp	.L712
	.cfi_endproc
.LFE3184:
	.size	_ZN6icu_6712RegexCompile16handleCloseParenEv, .-_ZN6icu_6712RegexCompile16handleCloseParenEv
	.section	.text.unlikely
	.align 2
.LCOLDB9:
	.text
.LHOTB9:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile9stripNOPsEv
	.type	_ZN6icu_6712RegexCompile9stripNOPsEv, @function
_ZN6icu_6712RegexCompile9stripNOPsEv:
.LFB3193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L813
.L752:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L814
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L813:
	.cfi_restore_state
	movq	16(%rdi), %rax
	leaq	-160(%rbp), %r12
	movq	%rdi, %r14
	movq	%r12, %rdi
	movq	32(%rax), %rax
	movl	8(%rax), %ebx
	movl	%ebx, %esi
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	testl	%ebx, %ebx
	jle	.L755
	subl	$1, %ebx
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L815:
	movq	%rax, %r13
.L761:
	movslq	-152(%rbp), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L756
	cmpl	-148(%rbp), %esi
	jle	.L757
.L756:
	movq	8(%r14), %rdx
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L758
	movslq	-152(%rbp), %rax
.L757:
	movq	-136(%rbp), %rdx
	movl	%r15d, (%rdx,%rax,4)
	addl	$1, -152(%rbp)
.L758:
	movq	16(%r14), %rcx
	movq	32(%rcx), %rdi
	cmpl	%r13d, 8(%rdi)
	jle	.L759
	movq	24(%rdi), %rax
	cmpb	$7, 3(%rax,%r13,8)
	sete	%al
	movzbl	%al, %eax
	addl	%eax, %r15d
.L759:
	leaq	1(%r13), %rax
	cmpq	%r13, %rbx
	jne	.L815
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	xorl	%r13d, %r13d
	xorl	%r8d, %r8d
	movq	%rax, -128(%rbp)
	leaq	.L766(%rip), %r15
	movw	%dx, -120(%rbp)
	cmpl	%r13d, 8(%rdi)
	jle	.L789
	.p2align 4,,10
	.p2align 3
.L817:
	movq	24(%rdi), %rax
	movq	(%rax,%r13,8), %rsi
	movl	%esi, %edx
	shrl	$24, %edx
	cmpl	$989855743, %esi
	ja	.L763
	leal	-6(%rdx), %eax
	cmpl	$35, %eax
	ja	.L812
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L766:
	.long	.L767-.L766
	.long	.L768-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L767-.L766
	.long	.L812-.L766
	.long	.L767-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L767-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L767-.L766
	.long	.L767-.L766
	.long	.L812-.L766
	.long	.L767-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L765-.L766
	.long	.L812-.L766
	.long	.L767-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L812-.L766
	.long	.L765-.L766
	.text
.L765:
	movq	136(%rcx), %r9
	andl	$16777215, %esi
	movl	8(%r9), %eax
	cmpl	%eax, %esi
	jg	.L816
	movq	8(%r14), %rcx
	leal	-1(%rsi), %r10d
	movl	(%rcx), %ecx
	testl	%esi, %esi
	je	.L810
	testl	%eax, %eax
	jg	.L781
.L810:
	xorl	%esi, %esi
	testl	%ecx, %ecx
	jg	.L779
	sall	$24, %edx
	movl	%edx, %esi
.L782:
	movslq	%esi, %rsi
.L779:
	movl	%r8d, %edx
	movl	%r8d, -164(%rbp)
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movq	16(%r14), %rcx
	movl	-164(%rbp), %r8d
	movb	$1, 184(%rcx)
	movq	32(%rcx), %rdi
	addl	$1, %r8d
.L768:
	leaq	1(%r13), %rax
	cmpq	%rbx, %r13
	je	.L787
.L795:
	movq	%rax, %r13
	cmpl	%r13d, 8(%rdi)
	jg	.L817
.L789:
	xorl	%esi, %esi
	jmp	.L762
.L767:
	movl	%esi, %eax
	movl	-152(%rbp), %ecx
	movq	8(%r14), %rsi
	andl	$16777215, %eax
	movl	(%rsi), %r9d
	testl	%ecx, %ecx
	jle	.L770
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L770
	movq	-136(%rbp), %rcx
	movslq	%eax, %rsi
	subl	(%rcx,%rsi,4), %eax
	testl	%r9d, %r9d
	jg	.L789
	cmpl	$16777215, %eax
	jg	.L774
	testl	%eax, %eax
	jns	.L773
	cmpl	$255, %edx
	je	.L797
	testl	%edx, %edx
	jne	.L774
.L797:
	movl	%eax, %edx
	shrl	$24, %edx
	cmpl	$255, %edx
	jne	.L774
	movl	$-16777216, %esi
	orl	%eax, %esi
.L812:
	movslq	%esi, %rsi
	.p2align 4,,10
	.p2align 3
.L762:
	movl	%r8d, %edx
	movl	%r8d, -164(%rbp)
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movl	-164(%rbp), %r8d
	movq	16(%r14), %rcx
	leaq	1(%r13), %rax
	movq	32(%rcx), %rdi
	addl	$1, %r8d
	cmpq	%rbx, %r13
	jne	.L795
.L787:
	movl	%r8d, %esi
	call	_ZN6icu_679UVector647setSizeEi@PLT
	leaq	-128(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L770:
	xorl	%esi, %esi
	testl	%r9d, %r9d
	jg	.L762
.L773:
	sall	$24, %edx
	movl	%edx, %esi
	orl	%eax, %esi
	jmp	.L812
.L763:
	cmpl	$255, %edx
	je	.L812
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L816:
	movq	%r14, %rdi
	movl	$66314, %esi
	movl	%r8d, -164(%rbp)
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	16(%r14), %rcx
	movl	-164(%rbp), %r8d
	movq	32(%rcx), %rdi
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L781:
	subl	%r10d, %eax
	testl	%eax, %eax
	jle	.L810
	movq	24(%r9), %rax
	movslq	%r10d, %r10
	movl	(%rax,%r10,4), %esi
	testl	%ecx, %ecx
	jg	.L794
	cmpl	$16777215, %esi
	jg	.L774
	testl	%esi, %esi
	jns	.L818
	cmpl	$255, %edx
	je	.L798
	testl	%edx, %edx
	jne	.L774
.L798:
	movl	%esi, %eax
	shrl	$24, %eax
	cmpl	$255, %eax
	jne	.L774
	orl	$-16777216, %esi
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L794:
	xorl	%esi, %esi
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L755:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%r8d, %r8d
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	movq	16(%r14), %rax
	movq	32(%rax), %rdi
	jmp	.L787
.L814:
	call	__stack_chk_fail@PLT
.L818:
	sall	$24, %edx
	orl	%edx, %esi
	jmp	.L782
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexCompile9stripNOPsEv.cold, @function
_ZN6icu_6712RegexCompile9stripNOPsEv.cold:
.LFSB3193:
.L774:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
.L769:
	call	abort@PLT
	.cfi_endproc
.LFE3193:
	.text
	.size	_ZN6icu_6712RegexCompile9stripNOPsEv, .-_ZN6icu_6712RegexCompile9stripNOPsEv
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexCompile9stripNOPsEv.cold, .-_ZN6icu_6712RegexCompile9stripNOPsEv.cold
.LCOLDE9:
	.text
.LHOTE9:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompileD0Ev
	.type	_ZN6icu_6712RegexCompileD0Ev, @function
_ZN6icu_6712RegexCompileD0Ev:
.LFB3170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712RegexCompileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	520(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L820
	movq	(%rdi), %rax
	call	*8(%rax)
.L820:
	leaq	472(%r12), %rdi
	call	_ZN6icu_676UStackD1Ev@PLT
	leaq	432(%r12), %rdi
	call	_ZN6icu_676UStackD1Ev@PLT
	leaq	376(%r12), %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	leaq	304(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3170:
	.size	_ZN6icu_6712RegexCompileD0Ev, .-_ZN6icu_6712RegexCompileD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE
	.type	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE, @function
_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE:
.LFB3197:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	$18433, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L826:
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L827
	addq	32(%rdi), %rax
.L828:
	movl	68(%r13), %edi
	movq	%rax, 32(%r13)
	cmpl	$-1, %edi
	je	.L829
	movl	$-1, 68(%r13)
.L830:
	movl	%edi, (%rbx)
	movb	$0, 4(%rbx)
	cmpb	$0, 40(%r13)
	je	.L831
	movb	$1, 4(%rbx)
	cmpl	$92, %edi
	je	.L918
.L832:
	cmpl	$-1, %edi
	jne	.L840
	cmpl	$-1, 68(%r13)
	movb	$0, 40(%r13)
	jne	.L914
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L831:
	cmpb	$0, 41(%r13)
	jne	.L919
	testb	$4, 292(%r13)
	jne	.L916
.L841:
	cmpl	$92, %edi
	jne	.L840
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movslq	40(%rdi), %r14
	cmpl	28(%rdi), %r14d
	jg	.L853
	addq	32(%rdi), %r14
.L854:
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	movl	68(%r13), %esi
	leaq	3624(%rax), %r15
	cmpl	$-1, %esi
	je	.L920
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L921
.L856:
	movl	68(%r13), %eax
	cmpl	$-1, %eax
	je	.L922
	cmpl	$48, %eax
	je	.L885
.L883:
	cmpl	$81, %eax
	jne	.L884
.L934:
	movb	$1, 40(%r13)
.L914:
	movl	$-1, 68(%r13)
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L850:
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	movl	%eax, (%rbx)
	movl	%eax, %edi
.L916:
	cmpl	$-1, %edi
	je	.L840
	cmpl	$35, %edi
	je	.L923
.L843:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	je	.L924
.L849:
	movl	68(%r13), %edi
	cmpl	$-1, %edi
	je	.L850
	movl	$-1, 68(%r13)
	movl	%edi, (%rbx)
	cmpl	$35, %edi
	jne	.L843
.L923:
	cmpb	$1, 42(%r13)
	jne	.L843
	.p2align 4,,10
	.p2align 3
.L903:
	movl	68(%r13), %edi
	cmpl	$-1, %edi
	je	.L844
.L925:
	leal	1(%rdi), %eax
	movl	$-1, 68(%r13)
	movl	%edi, (%rbx)
	cmpl	$14, %eax
	ja	.L846
.L926:
	btq	%rax, %r12
	jc	.L843
	movl	68(%r13), %edi
	cmpl	$-1, %edi
	jne	.L925
.L844:
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	movl	%eax, %edi
	leal	1(%rdi), %eax
	movl	%edi, (%rbx)
	cmpl	$14, %eax
	jbe	.L926
.L846:
	cmpl	$133, %edi
	je	.L843
	cmpl	$8232, %edi
	jne	.L903
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	jne	.L849
	.p2align 4,,10
	.p2align 3
.L924:
	movl	(%rbx), %edi
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L829:
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	movl	%eax, %edi
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L827:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L853:
	movq	56(%rdi), %rax
	call	*64(%rax)
	movq	%rax, %r14
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L918:
	movl	68(%r13), %eax
	cmpl	$-1, %eax
	je	.L927
	cmpl	$69, %eax
	jne	.L840
.L834:
	testb	$16, 292(%r13)
	je	.L835
	movl	(%rbx), %edi
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L920:
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	movq	%r15, %rdi
	movl	%eax, 68(%r13)
	movl	%eax, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L856
.L921:
	cmpl	$-1, 68(%r13)
	je	.L857
	movl	$-1, 68(%r13)
.L858:
	movb	$1, 4(%rbx)
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movq	32(%rdi), %rax
	testq	%rax, %rax
	jne	.L859
	movq	368(%r13), %rdx
	cmpq	16(%rdi), %rdx
	je	.L928
.L859:
	movq	%r14, %rcx
	movl	$0, -84(%rbp)
	movl	$0, -68(%rbp)
	movl	$-1, -72(%rbp)
	movq	%rdi, -80(%rbp)
	subq	%rax, %rcx
	js	.L863
	movslq	28(%rdi), %rdx
	cmpq	%rcx, %rdx
	jle	.L863
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rcx,2)
	jbe	.L929
.L863:
	movq	%r14, %rsi
	call	utext_setNativeIndex_67@PLT
.L864:
	movq	uregex_utext_unescape_charAt_67@GOTPCREL(%rip), %rdi
	leaq	-80(%rbp), %rcx
	leaq	-84(%rbp), %rsi
	movl	$2147483647, %edx
	call	u_unescapeAt_67@PLT
	movl	%eax, (%rbx)
	movslq	-84(%rbp), %rax
	testl	%eax, %eax
	je	.L930
	movl	-72(%rbp), %edx
	cmpl	%edx, %eax
	je	.L931
	leal	-1(%rax), %ecx
	cmpl	%ecx, %edx
	jne	.L932
.L866:
	addq	%rax, 56(%r13)
	.p2align 4,,10
	.p2align 3
.L840:
	movb	$1, 42(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L933
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L922:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	movl	%eax, 68(%r13)
	cmpl	$48, %eax
	je	.L885
	cmpl	$-1, %eax
	jne	.L883
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	movl	%eax, 68(%r13)
	cmpl	$81, %eax
	je	.L934
.L884:
	movb	$1, 41(%r13)
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L927:
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	movl	%eax, 68(%r13)
	cmpl	$69, %eax
	je	.L834
	movl	(%rbx), %edi
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L835:
	movb	$0, 40(%r13)
	movl	$-1, 68(%r13)
	jmp	.L826
.L919:
	movb	$0, 41(%r13)
	jmp	.L840
.L885:
	movl	$0, (%rbx)
	movq	%r13, %rdi
	movl	$-1, 68(%r13)
	call	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	leal	-48(%rax), %edx
	movl	%eax, 68(%r13)
	cmpl	$7, %edx
	ja	.L935
	movl	(%rbx), %edx
	andl	$7, %eax
	leal	(%rax,%rdx,8), %eax
	movl	%eax, (%rbx)
	cmpl	$255, %eax
	jle	.L873
	sarl	$3, %eax
	movl	%eax, (%rbx)
	movl	68(%r13), %eax
.L874:
	leal	-48(%rax), %edx
	cmpl	$7, %edx
	ja	.L872
	movl	(%rbx), %edx
	andl	$7, %eax
	leal	(%rax,%rdx,8), %eax
	movl	%eax, (%rbx)
	cmpl	$255, %eax
	jg	.L936
	movl	$-1, 68(%r13)
.L877:
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	movl	%eax, 68(%r13)
.L878:
	leal	-48(%rax), %edx
	cmpl	$7, %edx
	ja	.L872
	movl	(%rbx), %edx
	andl	$7, %eax
	leal	(%rax,%rdx,8), %eax
	movl	%eax, (%rbx)
	cmpl	$255, %eax
	jg	.L881
	movl	$-1, 68(%r13)
.L872:
	movb	$1, 4(%rbx)
	jmp	.L840
.L936:
	sarl	$3, %eax
	movl	%eax, (%rbx)
	movl	68(%r13), %eax
	cmpl	$-1, %eax
	jne	.L878
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L857:
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	jmp	.L858
.L873:
	movl	$-1, 68(%r13)
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	movl	%eax, 68(%r13)
	jmp	.L874
.L881:
	sarl	$3, %eax
	movl	%eax, (%rbx)
	jmp	.L872
.L930:
	movl	$66307, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movslq	-84(%rbp), %rax
	jmp	.L866
.L932:
	subl	%edx, %eax
	leal	-1(%rax), %esi
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	call	utext_moveIndex32_67@PLT
	movslq	-84(%rbp), %rax
	jmp	.L866
.L929:
	movl	%ecx, 40(%rdi)
	jmp	.L864
.L931:
	movq	16(%r13), %rdx
	movq	8(%rdx), %rdi
	movl	40(%rdi), %edx
	testl	%edx, %edx
	jle	.L868
	movq	48(%rdi), %rsi
	movslq	%edx, %rcx
	cmpw	$-10241, -2(%rsi,%rcx,2)
	ja	.L868
	subl	$1, %edx
	movl	%edx, 40(%rdi)
	jmp	.L866
.L928:
	movslq	28(%rdi), %rcx
	cmpq	%rcx, %rdx
	jne	.L859
	movq	48(%rdi), %rcx
	movq	uregex_ucstr_unescape_charAt_67@GOTPCREL(%rip), %rdi
	leaq	-80(%rbp), %rsi
	movl	%r14d, -80(%rbp)
	call	u_unescapeAt_67@PLT
	movslq	-80(%rbp), %rsi
	movl	%eax, (%rbx)
	cmpq	%r14, %rsi
	je	.L937
.L860:
	movq	%rsi, %rax
	subq	%r14, %rax
	addq	%rax, 56(%r13)
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movq	%rsi, %rax
	subq	32(%rdi), %rax
	js	.L861
	movslq	28(%rdi), %rdx
	cmpq	%rax, %rdx
	jle	.L861
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L861
	movl	%eax, 40(%rdi)
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L868:
	call	utext_previous32_67@PLT
	movslq	-84(%rbp), %rax
	jmp	.L866
.L935:
	movl	$66307, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	jmp	.L872
.L861:
	call	utext_setNativeIndex_67@PLT
	jmp	.L840
.L937:
	movl	$66307, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movslq	-80(%rbp), %rsi
	jmp	.L860
.L933:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3197:
	.size	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE, .-_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexCompile13scanNamedCharEv.part.0, @function
_ZN6icu_6712RegexCompile13scanNamedCharEv.part.0:
.LFB4199:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	72(%rdi), %r13
	pushq	%r12
	movq	%r13, %rsi
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$176, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE
	cmpl	$123, 72(%r12)
	jne	.L959
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	leaq	-208(%rbp), %r14
	movq	%rax, -208(%rbp)
	movw	%dx, -200(%rbp)
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L960:
	cmpl	$-1, %esi
	je	.L948
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
.L944:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE
	movl	72(%r12), %esi
	cmpl	$125, %esi
	jne	.L960
	movzwl	-200(%rbp), %eax
	testw	%ax, %ax
	js	.L945
	movswl	%ax, %esi
	sarl	$5, %esi
.L946:
	testb	$17, %al
	jne	.L953
	leaq	-198(%rbp), %rdi
	testb	$2, %al
	cmove	-184(%rbp), %rdi
.L947:
	call	uprv_isInvariantUString_67@PLT
	testb	%al, %al
	je	.L948
	movzwl	-200(%rbp), %eax
	testw	%ax, %ax
	js	.L949
	movswl	%ax, %edx
	sarl	$5, %edx
.L950:
	cmpl	$99, %edx
	ja	.L948
	leaq	-144(%rbp), %r15
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r15, %rcx
	movl	$100, %r8d
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movq	8(%r12), %rdx
	movq	%r15, %rsi
	xorl	%edi, %edi
	call	u_charFromName_67@PLT
	movl	%eax, %r15d
	movq	8(%r12), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L951
	movl	$66308, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
.L951:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L948:
	movl	$66308, %esi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
.L943:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L938:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L961
	addq	$176, %rsp
	movl	%r15d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L959:
	.cfi_restore_state
	movl	$66308, %esi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L949:
	movl	-196(%rbp), %edx
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L945:
	movl	-196(%rbp), %esi
	jmp	.L946
.L953:
	xorl	%edi, %edi
	jmp	.L947
.L961:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4199:
	.size	_ZN6icu_6712RegexCompile13scanNamedCharEv.part.0, .-_ZN6icu_6712RegexCompile13scanNamedCharEv.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile13scanNamedCharEv
	.type	_ZN6icu_6712RegexCompile13scanNamedCharEv, @function
_ZN6icu_6712RegexCompile13scanNamedCharEv:
.LFB3198:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L964
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L964:
	jmp	_ZN6icu_6712RegexCompile13scanNamedCharEv.part.0
	.cfi_endproc
.LFE3198:
	.size	_ZN6icu_6712RegexCompile13scanNamedCharEv, .-_ZN6icu_6712RegexCompile13scanNamedCharEv
	.p2align 4
	.type	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode, @function
_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode:
.LFB3171:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-240(%rbp), %r12
	movq	%r12, %rdi
	subq	$208, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r15, %rcx
	movl	%r14d, %edx
	movl	$8192, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L968
	addq	$208, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L968:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3171:
	.size	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode, .-_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	.p2align 4
	.type	_ZN6icu_67L22addIdentifierIgnorableEPNS_10UnicodeSetER10UErrorCode, @function
_ZN6icu_67L22addIdentifierIgnorableEPNS_10UnicodeSetER10UErrorCode:
.LFB3201:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-240(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$27, %edx
	movl	$14, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$159, %edx
	movl	$127, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r14, %rcx
	movl	$65536, %edx
	movq	%r13, %rdi
	movl	$8192, %esi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L972
	addq	$216, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L972:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3201:
	.size	_ZN6icu_67L22addIdentifierIgnorableEPNS_10UnicodeSetER10UErrorCode, .-_ZN6icu_67L22addIdentifierIgnorableEPNS_10UnicodeSetER10UErrorCode
	.section	.text.unlikely
	.align 2
.LCOLDB10:
	.text
.LHOTB10:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetE
	.type	_ZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetE, @function
_ZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetE:
.LFB3188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$1114111, %edi
	ja	.L985
	movq	%rsi, %r13
	movl	$34, %esi
	movl	%edi, %r12d
	call	u_hasBinaryProperty_67@PLT
	testb	%al, %al
	je	.L975
	xorl	%esi, %esi
	movl	%r12d, %edi
	call	u_foldCase_67@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	movl	%eax, %edx
	call	_ZN6icu_6710UnicodeSet3setEii@PLT
	cmpl	$97, %r12d
	jle	.L976
	movl	$1, %eax
	leaq	-4+_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE19RECaseFixCodePoints(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L977:
	movslq	%eax, %rdx
	addq	$1, %rax
	cmpl	(%rcx,%rax,4), %r12d
	jg	.L977
	je	.L987
.L978:
	movq	%r13, %rdi
	movl	$2, %esi
	call	_ZN6icu_6710UnicodeSet9closeOverEi@PLT
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSet16removeAllStringsEv@PLT
	.p2align 4,,10
	.p2align 3
.L975:
	.cfi_restore_state
	addq	$24, %rsp
	movl	%r12d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSet3setEii@PLT
	.p2align 4,,10
	.p2align 3
.L976:
	.cfi_restore_state
	jne	.L978
	movl	$1, -52(%rbp)
	movl	$7834, %eax
	xorl	%r15d, %r15d
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L987:
	leaq	_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE22RECaseFixStringOffsets(%rip), %rax
	movswl	(%rax,%rdx,2), %r15d
	leaq	_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE15RECaseFixCounts(%rip), %rax
	movswl	(%rax,%rdx,2), %eax
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	jle	.L978
	movslq	%r15d, %rax
	leaq	_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE13RECaseFixData(%rip), %rdx
	movzwl	(%rdx,%rax,2), %eax
.L982:
	xorl	%ebx, %ebx
	leaq	_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE13RECaseFixData(%rip), %r12
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L979:
	movq	%r13, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	cmpl	-52(%rbp), %ebx
	jge	.L978
.L980:
	movslq	%r14d, %rax
	movl	%r14d, %r15d
	movzwl	(%r12,%rax,2), %eax
.L981:
	movzwl	%ax, %esi
	andl	$64512, %eax
	leal	1(%r15), %r14d
	cmpl	$55296, %eax
	jne	.L979
	movslq	%r14d, %r14
	sall	$10, %esi
	movq	%r13, %rdi
	addl	$2, %r15d
	movzwl	(%r12,%r14,2), %eax
	addl	$1, %ebx
	leal	-56613888(%rsi,%rax), %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	cmpl	%ebx, -52(%rbp)
	jle	.L978
	movl	%r15d, %r14d
	jmp	.L980
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetE.cold, @function
_ZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetE.cold:
.LFSB3188:
.L985:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3188:
	.text
	.size	_ZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetE, .-_ZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetE
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetE.cold, .-_ZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetE.cold
.LCOLDE10:
	.text
.LHOTE10:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile10compileSetEPNS_10UnicodeSetE
	.type	_ZN6icu_6712RegexCompile10compileSetEPNS_10UnicodeSetE, @function
_ZN6icu_6712RegexCompile10compileSetEPNS_10UnicodeSetE:
.LFB3185:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L988
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6710UnicodeSet16removeAllStringsEv@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet4sizeEv@PLT
	testl	%eax, %eax
	je	.L990
	cmpl	$1, %eax
	jne	.L998
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet6charAtEi@PLT
	leaq	304(%r13), %rdi
	movl	%eax, %esi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
.L997:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSetD0Ev@PLT
	.p2align 4,,10
	.p2align 3
.L998:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rdx
	movq	%r12, %rsi
	movq	104(%rax), %rdi
	movl	8(%rdi), %r14d
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$11, %esi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	movl	%r14d, %edx
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexCompile8appendOpEii
	.p2align 4,,10
	.p2align 3
.L990:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L988:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE3185:
	.size	_ZN6icu_6712RegexCompile10compileSetEPNS_10UnicodeSetE, .-_ZN6icu_6712RegexCompile10compileSetEPNS_10UnicodeSetE
	.section	.text.unlikely
	.align 2
.LCOLDB11:
	.text
.LHOTB11:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile14matchStartTypeEv
	.type	_ZN6icu_6712RegexCompile14matchStartTypeEv, @function
_ZN6icu_6712RegexCompile14matchStartTypeEv:
.LFB3190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %r11d
	testl	%r11d, %r11d
	jle	.L1179
.L999:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1180
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1179:
	.cfi_restore_state
	movq	16(%rdi), %rax
	leaq	-288(%rbp), %r15
	movq	%rdi, %r14
	movq	%r15, %rdi
	movq	32(%rax), %rax
	movl	8(%rax), %r13d
	leal	1(%r13), %r12d
	movl	%r12d, %esi
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_679UVector327setSizeEi@PLT
	cmpl	$3, %r13d
	jle	.L1001
	movl	$3, %ebx
	.p2align 4,,10
	.p2align 3
.L1002:
	movl	%ebx, %edx
	movl	$2147483647, %esi
	movq	%r15, %rdi
	addl	$1, %ebx
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	cmpl	%r13d, %ebx
	jne	.L1002
	movq	%r15, -304(%rbp)
	movq	16(%r14), %rdi
	xorl	%r12d, %r12d
	movl	$1, %r11d
	movl	$0, -292(%rbp)
	movl	$3, %ebx
	.p2align 4,,10
	.p2align 3
.L1075:
	movq	32(%rdi), %rcx
	movl	8(%rcx), %eax
	cmpl	%ebx, %eax
	jle	.L1003
	movq	24(%rcx), %rdx
	movslq	%ebx, %rsi
	movl	-280(%rbp), %r8d
	movq	(%rdx,%rsi,8), %rdx
	movl	%edx, %r15d
	shrl	$24, %edx
	testl	%r8d, %r8d
	jle	.L1004
	movl	%r8d, %r10d
	xorl	%r9d, %r9d
	subl	%ebx, %r10d
	testl	%r10d, %r10d
	jle	.L1005
	.p2align 4,,10
	.p2align 3
.L1095:
	movq	-264(%rbp), %r9
	movl	(%r9,%rsi,4), %r9d
.L1005:
	cmpl	%r9d, %r12d
	cmovg	%r9d, %r12d
	cmpl	$58, %edx
	ja	.L1073
	leaq	.L1093(%rip), %r10
	movl	%edx, %r9d
	movslq	(%r10,%r9,4), %r9
	addq	%r10, %r9
	notrack jmp	*%r9
	.section	.rodata
	.align 4
	.align 4
.L1093:
	.long	.L1094-.L1093
	.long	.L1091-.L1093
	.long	.L1094-.L1093
	.long	.L1009-.L1093
	.long	.L1050-.L1093
	.long	.L1094-.L1093
	.long	.L1048-.L1093
	.long	.L1094-.L1093
	.long	.L1094-.L1093
	.long	.L1094-.L1093
	.long	.L1017-.L1093
	.long	.L1012-.L1093
	.long	.L1040-.L1093
	.long	.L1130-.L1093
	.long	.L1094-.L1093
	.long	.L1063-.L1093
	.long	.L1094-.L1093
	.long	.L1094-.L1093
	.long	.L1063-.L1093
	.long	.L1040-.L1093
	.long	.L1094-.L1093
	.long	.L1040-.L1093
	.long	.L1023-.L1093
	.long	.L1006-.L1093
	.long	.L1094-.L1093
	.long	.L1058-.L1093
	.long	.L1058-.L1093
	.long	.L1040-.L1093
	.long	.L1063-.L1093
	.long	.L1063-.L1093
	.long	.L1008-.L1093
	.long	.L1094-.L1093
	.long	.L1094-.L1093
	.long	.L1094-.L1093
	.long	.L1094-.L1093
	.long	.L1094-.L1093
	.long	.L1043-.L1093
	.long	.L1064-.L1093
	.long	.L1073-.L1093
	.long	.L1035-.L1093
	.long	.L1054-.L1093
	.long	.L1094-.L1093
	.long	.L1094-.L1093
	.long	.L1008-.L1093
	.long	.L1064-.L1093
	.long	.L1073-.L1093
	.long	.L1073-.L1093
	.long	.L1073-.L1093
	.long	.L1073-.L1093
	.long	.L1020-.L1093
	.long	.L1015-.L1093
	.long	.L1063-.L1093
	.long	.L1016-.L1093
	.long	.L1094-.L1093
	.long	.L1094-.L1093
	.long	.L1094-.L1093
	.long	.L1027-.L1093
	.long	.L1031-.L1093
	.long	.L1031-.L1093
	.text
	.p2align 4,,10
	.p2align 3
.L1063:
	addl	$1, %ebx
	xorl	%r11d, %r11d
	.p2align 4,,10
	.p2align 3
.L1007:
	cmpl	%ebx, %r13d
	jg	.L1075
	movq	-304(%rbp), %r15
.L1074:
	movq	160(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L1076
	movq	176(%rdi), %r12
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1078:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L1077
	movl	%r13d, %eax
	movl	%r13d, %ecx
	movl	$1, %edx
	sarl	$3, %eax
	andl	$7, %ecx
	cltq
	sall	%cl, %edx
	orb	%dl, (%r12,%rax)
.L1077:
	addl	$1, %r13d
	cmpl	$256, %r13d
	jne	.L1078
	movq	16(%r14), %rdi
.L1076:
	movl	144(%rdi), %eax
	cmpl	$3, %eax
	je	.L1079
	cmpl	$1, -292(%rbp)
	je	.L1181
.L1080:
	cmpl	$4, %eax
	je	.L1079
	movl	124(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L1081
	movl	$0, 144(%rdi)
.L1079:
	movq	%r15, %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1003:
	movl	-280(%rbp), %r8d
	testl	%r8d, %r8d
	jle	.L1161
	movl	%r8d, %edx
	subl	%ebx, %edx
	testl	%edx, %edx
	jle	.L1161
	xorl	%r15d, %r15d
	xorl	%edx, %edx
	movslq	%ebx, %rsi
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1004:
	testl	%r12d, %r12d
	movl	$0, %r10d
	cmovg	%r10d, %r12d
	cmpl	$58, %edx
	ja	.L1073
	leaq	.L1088(%rip), %r10
	movl	%edx, %r9d
	movslq	(%r10,%r9,4), %r9
	addq	%r10, %r9
	notrack jmp	*%r9
	.section	.rodata
	.align 4
	.align 4
.L1088:
	.long	.L1094-.L1088
	.long	.L1087-.L1088
	.long	.L1094-.L1088
	.long	.L1009-.L1088
	.long	.L1050-.L1088
	.long	.L1094-.L1088
	.long	.L1048-.L1088
	.long	.L1094-.L1088
	.long	.L1094-.L1088
	.long	.L1094-.L1088
	.long	.L1017-.L1088
	.long	.L1012-.L1088
	.long	.L1040-.L1088
	.long	.L1130-.L1088
	.long	.L1094-.L1088
	.long	.L1063-.L1088
	.long	.L1094-.L1088
	.long	.L1094-.L1088
	.long	.L1063-.L1088
	.long	.L1040-.L1088
	.long	.L1094-.L1088
	.long	.L1040-.L1088
	.long	.L1023-.L1088
	.long	.L1006-.L1088
	.long	.L1094-.L1088
	.long	.L1058-.L1088
	.long	.L1058-.L1088
	.long	.L1040-.L1088
	.long	.L1063-.L1088
	.long	.L1063-.L1088
	.long	.L1008-.L1088
	.long	.L1094-.L1088
	.long	.L1094-.L1088
	.long	.L1094-.L1088
	.long	.L1094-.L1088
	.long	.L1094-.L1088
	.long	.L1043-.L1088
	.long	.L1064-.L1088
	.long	.L1073-.L1088
	.long	.L1035-.L1088
	.long	.L1054-.L1088
	.long	.L1094-.L1088
	.long	.L1094-.L1088
	.long	.L1008-.L1088
	.long	.L1064-.L1088
	.long	.L1073-.L1088
	.long	.L1073-.L1088
	.long	.L1073-.L1088
	.long	.L1073-.L1088
	.long	.L1020-.L1088
	.long	.L1015-.L1088
	.long	.L1063-.L1088
	.long	.L1016-.L1088
	.long	.L1094-.L1088
	.long	.L1094-.L1088
	.long	.L1094-.L1088
	.long	.L1027-.L1088
	.long	.L1031-.L1088
	.long	.L1031-.L1088
	.text
	.p2align 4,,10
	.p2align 3
.L1161:
	testl	%r12d, %r12d
	movl	$0, %eax
	cmovg	%eax, %r12d
.L1094:
	addl	$1, %ebx
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1040:
	addl	$1, %ebx
	testl	%r12d, %r12d
	je	.L1182
	.p2align 4,,10
	.p2align 3
.L1041:
	movl	$2147483647, %eax
	subl	%r12d, %eax
	cmpl	$1, %eax
	jg	.L1042
.L1122:
	xorl	%r11d, %r11d
	movl	$2147483647, %r12d
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1181:
	movl	124(%rdi), %esi
	testl	%esi, %esi
	jle	.L1080
	movl	148(%rdi), %esi
	addq	$40, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r8d
	movq	16(%r14), %rax
	movl	$5, 144(%rax)
	movl	%r8d, 168(%rax)
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1064:
	xorl	%r15d, %r15d
	cmpl	$37, %edx
	movq	24(%rcx), %r9
	movl	%r12d, %r10d
	sete	%r15b
	leaq	8(,%rsi,8), %r8
	movl	%r13d, -312(%rbp)
	movl	%ebx, %ecx
	addl	$1, %r15d
	movl	%r11d, %r13d
	movq	%r8, %r12
	movl	%r15d, %ebx
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1068:
	addl	$2, %ebx
.L1067:
	addq	$8, %r12
	movl	%r15d, %ecx
.L1066:
	leal	1(%rcx), %r15d
	cmpl	%eax, %r15d
	jge	.L1067
	movq	(%r9,%r12), %rdx
	movl	%edx, %esi
	shrl	$24, %esi
	cmpl	$37, %esi
	je	.L1068
	cmpl	$44, %esi
	jne	.L1183
	addl	$1, %ebx
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1008:
	addl	$1, %ebx
	testb	%r11b, %r11b
	je	.L1007
	movl	$4, 144(%rdi)
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1058:
	leal	1(%rbx), %edx
	xorl	%r9d, %r9d
	cmpl	%eax, %edx
	jge	.L1059
	movq	24(%rcx), %rsi
	movslq	%edx, %rdx
	movq	(%rsi,%rdx,8), %r9
	andl	$16777215, %r9d
.L1059:
	leal	2(%rbx), %edx
	cmpl	%edx, %eax
	jle	.L1086
	movq	24(%rcx), %rax
	movslq	%edx, %rdx
	movl	(%rax,%rdx,8), %r10d
	testl	%r10d, %r10d
	je	.L1086
.L1060:
	addl	$4, %ebx
	xorl	%r11d, %r11d
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1031:
	addl	$1, %ebx
	testl	%r12d, %r12d
	jne	.L1041
	leaq	-256(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -312(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	$13, %edx
	movl	$10, %esi
	movq	-312(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movq	-312(%rbp), %r8
	movl	$133, %esi
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$8233, %edx
	movl	$8232, %esi
	movq	-312(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	andl	$16777215, %r15d
	movq	-312(%rbp), %r8
	je	.L1025
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1086:
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L1061
	subl	%r9d, %r8d
	testl	%r8d, %r8d
	jle	.L1061
	movq	-264(%rbp), %rdx
	movslq	%r9d, %rax
	movl	(%rdx,%rax,4), %eax
.L1061:
	cmpl	%r12d, %eax
	jle	.L1060
	movq	-304(%rbp), %rdi
	movl	%r9d, %edx
	movl	%r12d, %esi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movq	16(%r14), %rdi
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1006:
	addl	$1, %ebx
	testb	%r11b, %r11b
	je	.L1007
	movl	$3, 144(%rdi)
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1023:
	addl	$1, %ebx
	testl	%r12d, %r12d
	jne	.L1041
	leaq	-256(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -312(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	-312(%rbp), %r8
	movq	8(%r14), %rcx
	movl	$512, %edx
	movl	$8192, %esi
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	andl	$16777215, %r15d
	movq	-312(%rbp), %r8
	je	.L1025
.L1178:
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	movq	-312(%rbp), %r8
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1027:
	addl	$1, %ebx
	testl	%r12d, %r12d
	jne	.L1041
	leaq	-256(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -312(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	-312(%rbp), %r8
	movq	8(%r14), %rcx
	movl	$4096, %edx
	movl	$8192, %esi
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	movq	-312(%rbp), %r8
	movl	$9, %esi
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	andl	$16777215, %r15d
	movq	-312(%rbp), %r8
	jne	.L1178
.L1025:
	movq	16(%r14), %rax
	movq	%r8, %rsi
	movq	160(%rax), %rdi
	movq	%r8, -312(%rbp)
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1043:
	leal	1(%rbx), %eax
.L1044:
	andl	$16777215, %r15d
	leal	1(%rax), %ebx
	cmpl	%r15d, %eax
	jle	.L1045
	testl	%r8d, %r8d
	jle	.L1164
.L1176:
	subl	%ebx, %r8d
	testl	%r8d, %r8d
	jle	.L1164
	movq	-264(%rbp), %rax
	movslq	%ebx, %rsi
	xorl	%r11d, %r11d
	movl	(%rax,%rsi,4), %r12d
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1015:
	addl	$1, %ebx
	xorl	%r11d, %r11d
	testl	%r12d, %r12d
	jne	.L1007
	movq	104(%rdi), %rdi
	movl	%r15d, %esi
	movb	%r11b, -312(%rbp)
	andl	$16777215, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rsi
	movq	16(%r14), %rax
	movq	160(%rax), %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	addl	$2, -292(%rbp)
	movq	16(%r14), %rdi
	movzbl	-312(%rbp), %r11d
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1020:
	addl	$1, %ebx
	testl	%r12d, %r12d
	jne	.L1041
	leaq	-256(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -312(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	%r15d, %esi
	movq	-312(%rbp), %r8
	andl	$16777215, %esi
	leaq	(%rsi,%rsi,4), %rax
	movq	%r8, %rdi
	leaq	(%rax,%rax,4), %rdx
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	leaq	8(%rax,%rdx,8), %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	movq	16(%r14), %rax
	movq	-312(%rbp), %r8
	movq	160(%rax), %rdi
	movq	%r8, %rsi
.L1169:
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	-312(%rbp), %r8
	addl	$2, -292(%rbp)
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	16(%r14), %rdi
.L1042:
	addl	$1, %r12d
	xorl	%r11d, %r11d
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1012:
	addl	$1, %ebx
	testl	%r12d, %r12d
	jne	.L1041
	movq	104(%rdi), %rdi
	movl	%r15d, %esi
	andl	$16777215, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rsi
	movq	16(%r14), %rax
	movq	160(%rax), %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	addl	$2, -292(%rbp)
	movq	16(%r14), %rdi
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1017:
	addl	$1, %ebx
	testl	%r12d, %r12d
	jne	.L1041
	movl	%r15d, %esi
	movq	160(%rdi), %rdi
	andl	$16777215, %esi
	leaq	(%rsi,%rsi,4), %rax
	leaq	(%rax,%rax,4), %rdx
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	leaq	8(%rax,%rdx,8), %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	addl	$2, -292(%rbp)
	movq	16(%r14), %rdi
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1048:
	andl	$16777215, %r15d
	leal	1(%rbx), %eax
	cmpl	%ebx, %r15d
	jle	.L1115
	xorl	%edx, %edx
	testl	%r8d, %r8d
	jle	.L1049
	subl	%r15d, %r8d
	testl	%r8d, %r8d
	jle	.L1049
	movq	-264(%rbp), %rcx
	movslq	%r15d, %rdx
	movl	(%rcx,%rdx,4), %edx
.L1049:
	movl	%eax, %ebx
	xorl	%r11d, %r11d
	cmpl	%r12d, %edx
	jle	.L1007
.L1163:
	movq	-304(%rbp), %rdi
	movl	%r15d, %edx
	movl	%r12d, %esi
	movb	%r11b, -312(%rbp)
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movq	16(%r14), %rdi
	movzbl	-312(%rbp), %r11d
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1050:
	leal	1(%rbx), %edx
	xorl	%r8d, %r8d
	cmpl	%eax, %edx
	jge	.L1051
	movq	24(%rcx), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %r8
	andl	$16777215, %r8d
.L1051:
	addl	$2, %ebx
	testl	%r12d, %r12d
	je	.L1184
.L1056:
	movl	$2147483647, %eax
	subl	%r12d, %eax
	cmpl	%r8d, %eax
	jle	.L1122
.L1057:
	addl	%r8d, %r12d
	xorl	%r11d, %r11d
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1016:
	addl	$1, %ebx
	xorl	%r11d, %r11d
	testl	%r12d, %r12d
	jne	.L1007
	movq	160(%rdi), %rdi
	movb	%r11b, -312(%rbp)
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	movq	16(%r14), %rax
	movq	160(%rax), %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	addl	$2, -292(%rbp)
	movq	16(%r14), %rdi
	movzbl	-312(%rbp), %r11d
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1054:
	leal	1(%rbx), %edx
	xorl	%r8d, %r8d
	cmpl	%eax, %edx
	jge	.L1055
	movq	24(%rcx), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %r8
	andl	$16777215, %r8d
.L1055:
	addl	$2, %ebx
	testl	%r12d, %r12d
	jne	.L1056
	movl	%r15d, %esi
	addq	$40, %rdi
	leaq	-256(%rbp), %r15
	movl	%r8d, -296(%rbp)
	andl	$16777215, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	%r15, %rdi
	movl	%eax, -312(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	-312(%rbp), %r9d
	movq	%r15, %rsi
	movl	%r9d, %edi
	call	_ZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetE
	movq	16(%r14), %rax
	movq	%r15, %rsi
	movq	160(%rax), %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r15, %rdi
	addl	$2, -292(%rbp)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	16(%r14), %rdi
	movl	-296(%rbp), %r8d
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1035:
	addl	$1, %ebx
	testl	%r12d, %r12d
	jne	.L1041
	andl	$16777215, %r15d
	movl	$34, %esi
	movl	%r15d, %edi
	call	u_hasBinaryProperty_67@PLT
	testb	%al, %al
	je	.L1037
	leaq	-256(%rbp), %r8
	movl	%r15d, %edx
	movl	%r15d, %esi
	movq	%r8, %rdi
	movq	%r8, -312(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
	movq	-312(%rbp), %r8
	movl	$2, %esi
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSet9closeOverEi@PLT
	movq	16(%r14), %rax
	movq	-312(%rbp), %r8
	movq	160(%rax), %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	-312(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
.L1170:
	addl	$2, -292(%rbp)
	movq	16(%r14), %rdi
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1009:
	addl	$1, %ebx
	testl	%r12d, %r12d
	jne	.L1041
	movq	160(%rdi), %rdi
	movl	%r15d, %esi
	andl	$16777215, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	addl	$2, -292(%rbp)
	movq	16(%r14), %rdi
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1130:
	movl	%ebx, %eax
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1045:
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L1046
	subl	%r15d, %r8d
	testl	%r8d, %r8d
	jle	.L1046
	movq	-264(%rbp), %rdx
	movslq	%r15d, %rax
	movl	(%rdx,%rax,4), %eax
.L1046:
	xorl	%r11d, %r11d
	cmpl	%r12d, %eax
	jle	.L1007
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1164:
	xorl	%r11d, %r11d
	xorl	%r12d, %r12d
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1115:
	movl	%eax, %ebx
	xorl	%r11d, %r11d
	jmp	.L1007
.L1087:
	addl	$1, %ebx
	xorl	%r11d, %r11d
	xorl	%r12d, %r12d
	jmp	.L1007
.L1091:
	addl	$1, %ebx
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	160(%rdi), %rdi
	call	_ZNK6icu_6710UnicodeSet4sizeEv@PLT
	cmpl	$1, %eax
	je	.L1185
	movq	16(%r14), %rax
	xorl	%esi, %esi
	movl	$1114111, %edx
	movq	160(%rax), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEii@PLT
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L1084
	movl	124(%rax), %edx
	testl	%edx, %edx
	jle	.L1084
	movl	$2, 144(%rax)
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1001:
	movl	$0, -292(%rbp)
	movq	16(%r14), %rdi
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	160(%rdi), %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	movq	16(%r14), %rax
	movq	160(%rax), %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1184:
	andl	$16777215, %r15d
	addq	$40, %rdi
	movl	%r8d, -312(%rbp)
	movl	%r15d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %esi
	movq	16(%r14), %rax
	movq	160(%rax), %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	16(%r14), %rdi
	movl	-312(%rbp), %r8d
	addl	$1, -292(%rbp)
	movl	%r15d, 148(%rdi)
	movl	%r8d, 152(%rdi)
	jmp	.L1057
.L1084:
	movl	$0, 144(%rax)
	jmp	.L1079
.L1037:
	movq	16(%r14), %rax
	movl	%r15d, %esi
	movq	160(%rax), %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	jmp	.L1170
.L1185:
	movq	16(%r14), %rbx
	xorl	%esi, %esi
	movl	$1, 144(%rbx)
	movq	160(%rbx), %rdi
	call	_ZNK6icu_6710UnicodeSet6charAtEi@PLT
	movl	%eax, 168(%rbx)
	jmp	.L1079
.L1180:
	call	__stack_chk_fail@PLT
.L1183:
	cmpl	$38, %esi
	je	.L1070
	cmpl	$48, %esi
	jne	.L1071
.L1070:
	subl	$1, %ebx
	je	.L1186
.L1071:
	cmpl	$6, %esi
	jne	.L1067
	andl	$16777215, %edx
	cmpl	%edx, %r15d
	jge	.L1067
	movl	-280(%rbp), %ecx
	xorl	%esi, %esi
	testl	%ecx, %ecx
	jle	.L1072
	subl	%edx, %ecx
	testl	%ecx, %ecx
	jle	.L1072
	movq	-264(%rbp), %rsi
	movslq	%edx, %rcx
	movl	(%rsi,%rcx,4), %esi
.L1072:
	cmpl	%r10d, %esi
	jle	.L1067
	movq	-304(%rbp), %rdi
	movl	%r10d, %esi
	movl	%r10d, -296(%rbp)
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movq	16(%r14), %rdi
	movl	-296(%rbp), %r10d
	movq	32(%rdi), %rdx
	movl	8(%rdx), %eax
	movq	24(%rdx), %r9
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1186:
	movl	%ecx, %ebx
	movl	%r13d, %r11d
	movl	%r10d, %r12d
	movl	-312(%rbp), %r13d
	addl	$2, %ebx
	jmp	.L1007
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexCompile14matchStartTypeEv.cold, @function
_ZN6icu_6712RegexCompile14matchStartTypeEv.cold:
.LFSB3190:
.L1073:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3190:
	.text
	.size	_ZN6icu_6712RegexCompile14matchStartTypeEv, .-_ZN6icu_6712RegexCompile14matchStartTypeEv
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexCompile14matchStartTypeEv.cold, .-_ZN6icu_6712RegexCompile14matchStartTypeEv.cold
.LCOLDE11:
	.text
.LHOTE11:
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC12:
	.string	"["
	.string	"\\"
	.string	"p"
	.string	"{"
	.string	""
	.string	""
	.align 2
.LC13:
	.string	"}"
	.string	"]"
	.string	""
	.string	""
	.align 2
.LC14:
	.string	"w"
	.string	"o"
	.string	"r"
	.string	"d"
	.string	""
	.string	""
	.align 2
.LC15:
	.string	"a"
	.string	"l"
	.string	"l"
	.string	""
	.string	""
	.align 2
.LC16:
	.string	"I"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC17:
	.string	"B"
	.string	"l"
	.string	"o"
	.string	"c"
	.string	"k"
	.string	""
	.string	""
	.align 2
.LC18:
	.string	"I"
	.string	"s"
	.string	""
	.string	""
	.align 2
.LC19:
	.string	"a"
	.string	"s"
	.string	"s"
	.string	"i"
	.string	"g"
	.string	"n"
	.string	"e"
	.string	"d"
	.string	""
	.string	""
	.align 2
.LC20:
	.string	"u"
	.string	"n"
	.string	"a"
	.string	"s"
	.string	"s"
	.string	"i"
	.string	"g"
	.string	"n"
	.string	"e"
	.string	"d"
	.string	""
	.string	""
	.align 2
.LC21:
	.string	"T"
	.string	"i"
	.string	"t"
	.string	"l"
	.string	"e"
	.string	"C"
	.string	"a"
	.string	"s"
	.string	"e"
	.string	""
	.string	""
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC22:
	.string	"T"
	.string	"i"
	.string	"t"
	.string	"l"
	.string	"e"
	.string	"c"
	.string	"a"
	.string	"s"
	.string	"e"
	.string	"_"
	.string	"L"
	.string	"e"
	.string	"t"
	.string	"t"
	.string	"e"
	.string	"r"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC23:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	""
	.string	""
	.align 2
.LC24:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"D"
	.string	"e"
	.string	"f"
	.string	"i"
	.string	"n"
	.string	"e"
	.string	"d"
	.string	""
	.string	""
	.align 2
.LC25:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"D"
	.string	"i"
	.string	"g"
	.string	"i"
	.string	"t"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC26:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"I"
	.string	"d"
	.string	"e"
	.string	"n"
	.string	"t"
	.string	"i"
	.string	"f"
	.string	"i"
	.string	"e"
	.string	"r"
	.string	"I"
	.string	"g"
	.string	"n"
	.string	"o"
	.string	"r"
	.string	"a"
	.string	"b"
	.string	"l"
	.string	"e"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC27:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"I"
	.string	"S"
	.string	"O"
	.string	"C"
	.string	"o"
	.string	"n"
	.string	"t"
	.string	"r"
	.string	"o"
	.string	"l"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC28:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"J"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"I"
	.string	"d"
	.string	"e"
	.string	"n"
	.string	"t"
	.string	"i"
	.string	"f"
	.string	"i"
	.string	"e"
	.string	"r"
	.string	"P"
	.string	"a"
	.string	"r"
	.string	"t"
	.string	""
	.string	""
	.align 8
.LC29:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"J"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"I"
	.string	"d"
	.string	"e"
	.string	"n"
	.string	"t"
	.string	"i"
	.string	"f"
	.string	"i"
	.string	"e"
	.string	"r"
	.string	"S"
	.string	"t"
	.string	"a"
	.string	"r"
	.string	"t"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC30:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"L"
	.string	"e"
	.string	"t"
	.string	"t"
	.string	"e"
	.string	"r"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC31:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"L"
	.string	"e"
	.string	"t"
	.string	"t"
	.string	"e"
	.string	"r"
	.string	"O"
	.string	"r"
	.string	"D"
	.string	"i"
	.string	"g"
	.string	"i"
	.string	"t"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC32:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"L"
	.string	"o"
	.string	"w"
	.string	"e"
	.string	"r"
	.string	"C"
	.string	"a"
	.string	"s"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC33:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"M"
	.string	"i"
	.string	"r"
	.string	"r"
	.string	"o"
	.string	"r"
	.string	"e"
	.string	"d"
	.string	""
	.string	""
	.align 2
.LC34:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"S"
	.string	"p"
	.string	"a"
	.string	"c"
	.string	"e"
	.string	"C"
	.string	"h"
	.string	"a"
	.string	"r"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC35:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"S"
	.string	"u"
	.string	"p"
	.string	"p"
	.string	"l"
	.string	"e"
	.string	"m"
	.string	"e"
	.string	"n"
	.string	"t"
	.string	"a"
	.string	"r"
	.string	"y"
	.string	"C"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	"P"
	.string	"o"
	.string	"i"
	.string	"n"
	.string	"t"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC36:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"T"
	.string	"i"
	.string	"t"
	.string	"l"
	.string	"e"
	.string	"C"
	.string	"a"
	.string	"s"
	.string	"e"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC37:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"U"
	.string	"n"
	.string	"i"
	.string	"c"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	"I"
	.string	"d"
	.string	"e"
	.string	"n"
	.string	"t"
	.string	"i"
	.string	"f"
	.string	"i"
	.string	"e"
	.string	"r"
	.string	"S"
	.string	"t"
	.string	"a"
	.string	"r"
	.string	"t"
	.string	""
	.string	""
	.align 8
.LC38:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"U"
	.string	"n"
	.string	"i"
	.string	"c"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	"I"
	.string	"d"
	.string	"e"
	.string	"n"
	.string	"t"
	.string	"i"
	.string	"f"
	.string	"i"
	.string	"e"
	.string	"r"
	.string	"P"
	.string	"a"
	.string	"r"
	.string	"t"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC39:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"U"
	.string	"p"
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"C"
	.string	"a"
	.string	"s"
	.string	"e"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC40:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"V"
	.string	"a"
	.string	"l"
	.string	"i"
	.string	"d"
	.string	"C"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	"P"
	.string	"o"
	.string	"i"
	.string	"n"
	.string	"t"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC41:
	.string	"j"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	"W"
	.string	"h"
	.string	"i"
	.string	"t"
	.string	"e"
	.string	"s"
	.string	"p"
	.string	"a"
	.string	"c"
	.string	"e"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexCompile20createSetForPropertyERKNS_13UnicodeStringEa.part.0, @function
_ZN6icu_6712RegexCompile20createSetForPropertyERKNS_13UnicodeStringEa.part.0:
.LFB4204:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$2, %edi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-448(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	.LC12(%rip), %rsi
	pushq	%rbx
	subq	$456, %rsp
	.cfi_offset 3, -56
	movl	%edx, -488(%rbp)
	movb	%dl, -473(%rbp)
	xorl	%edx, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%di, -440(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r13, %rdi
	movl	$0, -460(%rbp)
	movq	%rax, -448(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC12(%rip), %rax
	movswl	8(%r12), %ecx
	testw	%cx, %cx
	js	.L1188
	sarl	$5, %ecx
.L1189:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	leaq	.LC13(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC13(%rip), %rax
	movl	292(%r14), %edx
	movl	$200, %edi
	andl	$2, %edx
	cmpl	$1, %edx
	movl	%edx, -472(%rbp)
	sbbl	%ebx, %ebx
	call	_ZN6icu_677UMemorynwEm@PLT
	addl	$1, %ebx
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1191
	movl	-472(%rbp), %edx
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	leaq	-460(%rbp), %r8
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode@PLT
	movl	-460(%rbp), %esi
	testl	%esi, %esi
	jg	.L1317
.L1192:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1264:
	movl	-460(%rbp), %esi
	testl	%esi, %esi
	jg	.L1265
	cmpb	$0, -473(%rbp)
	jne	.L1318
.L1187:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1319
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1265:
	.cfi_restore_state
	cmpl	$1, %esi
	jne	.L1268
	movl	$66308, -460(%rbp)
	movl	$66308, %esi
.L1268:
	movq	%r14, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	testq	%r15, %r15
	je	.L1187
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1188:
	movl	12(%r12), %ecx
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movl	-460(%rbp), %eax
.L1273:
	cmpl	$7, %eax
	je	.L1276
	testl	%eax, %eax
	jle	.L1276
	movl	$0, -460(%rbp)
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L1194
	sarl	$5, %edx
.L1195:
	subq	$8, %rsp
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString13doCaseCompareEiiPKDsiij@PLT
	leaq	.LC14(%rip), %rdx
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	je	.L1320
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L1198
	sarl	$5, %edx
.L1199:
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	.LC15(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	.LC15(%rip), %rdx
	testb	%al, %al
	jne	.L1200
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1201
	movq	%rax, %rdi
	movl	$1114111, %edx
	xorl	%esi, %esi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
	movl	-460(%rbp), %eax
	testl	%eax, %eax
	jle	.L1192
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1276:
	xorl	%r15d, %r15d
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1318:
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1320:
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	leaq	208(%rax), %rdi
	call	_ZNK6icu_6710UnicodeSet13cloneAsThawedEv@PLT
	movq	%rax, %r15
	movl	-460(%rbp), %eax
	testl	%eax, %eax
	jle	.L1321
	testq	%r15, %r15
	je	.L1192
.L1310:
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1194:
	movl	12(%r12), %edx
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1200:
	leaq	-384(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	$2, %r9d
	leaq	.LC16(%rip), %rcx
	movl	$2, %edx
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	je	.L1202
	leaq	.LC16(%rip), %rax
.L1203:
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	$2, %r9d
	movl	$2, %edx
	leaq	.LC18(%rip), %rcx
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jne	.L1213
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L1214
	sarl	$5, %eax
.L1215:
	cmpl	$2, %eax
	leaq	.LC18(%rip), %rax
	jle	.L1307
	movq	-472(%rbp), %r15
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movswl	-376(%rbp), %ecx
	movl	$61, %esi
	movq	%r15, %rdi
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	cmovs	-372(%rbp), %ecx
	xorl	%edx, %edx
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	js	.L1322
	movl	$66308, -460(%rbp)
	xorl	%r15d, %r15d
.L1212:
	movq	-472(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1321:
	testq	%r15, %r15
	jne	.L1192
	movl	$7, -460(%rbp)
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1198:
	movl	12(%r12), %edx
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1202:
	movswl	-376(%rbp), %eax
	testw	%ax, %ax
	js	.L1204
	sarl	$5, %eax
.L1205:
	cmpl	$2, %eax
	leaq	.LC16(%rip), %rax
	jg	.L1206
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1206:
	movl	$0, -460(%rbp)
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1237
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	-460(%rbp), %r12d
	testl	%r12d, %r12d
	jg	.L1323
.L1208:
	leaq	-320(%rbp), %r12
	movq	-472(%rbp), %rsi
	movl	$2, %edx
	leaq	-256(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_i@PLT
	movq	%rbx, %rdi
	leaq	.LC17(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	-460(%rbp), %rcx
	call	_ZN6icu_6710UnicodeSet18applyPropertyAliasERKNS_13UnicodeStringES3_R10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1213:
	leaq	.LC18(%rip), %rax
.L1307:
	leaq	.LC23(%rip), %r15
	movq	%r15, %rdi
	call	u_strlen_67@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	xorl	%esi, %esi
	movl	%eax, %edx
	movl	%eax, %r9d
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jne	.L1236
	movl	$0, -460(%rbp)
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1237
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	-460(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L1324
.L1238:
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L1241
	sarl	$5, %eax
	movl	%eax, %edx
.L1242:
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	.LC24(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	.LC24(%rip), %rdx
	testb	%al, %al
	jne	.L1243
	leaq	-256(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	-460(%rbp), %rcx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$8192, %esi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
.L1244:
	movl	-460(%rbp), %eax
	testl	%eax, %eax
	jg	.L1212
.L1316:
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet7isEmptyEv@PLT
	testb	%al, %al
	jne	.L1212
	testb	%bl, %bl
	je	.L1212
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet9closeOverEi@PLT
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1323:
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movl	-460(%rbp), %ebx
	testl	%ebx, %ebx
	jg	.L1212
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1204:
	movl	-372(%rbp), %eax
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	-472(%rbp), %rdi
	xorl	%r15d, %r15d
	movl	$66308, -460(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1264
.L1214:
	movl	12(%r12), %eax
	jmp	.L1215
.L1324:
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movl	-460(%rbp), %edx
	testl	%edx, %edx
	jg	.L1212
	jmp	.L1238
.L1322:
	movzwl	-376(%rbp), %eax
	testw	%ax, %ax
	js	.L1222
	movswl	%ax, %edx
	sarl	$5, %edx
.L1223:
	subq	$8, %rsp
	movq	-472(%rbp), %r15
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	pushq	$0
	movl	$-1, %r9d
	leaq	.LC19(%rip), %rcx
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString13doCaseCompareEiiPKDsiij@PLT
	leaq	.LC19(%rip), %rdx
	popq	%r10
	popq	%r11
	testb	%al, %al
	jne	.L1224
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-376(%rbp), %eax
	testw	%ax, %ax
	js	.L1225
	sarl	$5, %eax
	movl	%eax, %edx
.L1226:
	movq	-472(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	$-1, %r9d
	leaq	.LC20(%rip), %rcx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	cmpb	$0, -488(%rbp)
	sete	-473(%rbp)
.L1227:
	movq	-472(%rbp), %r12
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$-1, %r9d
	leaq	.LC12(%rip), %rcx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	leaq	.LC12(%rip), %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC13(%rip), %rax
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1237
	movq	8(%r14), %rdx
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	-460(%rbp), %edi
	testl	%edi, %edi
	jle	.L1316
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movl	-460(%rbp), %esi
	testl	%esi, %esi
	jg	.L1212
	jmp	.L1316
.L1224:
	movswl	-376(%rbp), %eax
	testw	%ax, %ax
	js	.L1228
	sarl	$5, %eax
	movl	%eax, %edx
.L1229:
	subq	$8, %rsp
	movq	-472(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	pushq	$0
	movl	$-1, %r9d
	leaq	.LC21(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString13doCaseCompareEiiPKDsiij@PLT
	leaq	.LC21(%rip), %rdx
	popq	%r8
	popq	%r9
	testb	%al, %al
	jne	.L1227
	movq	-472(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-376(%rbp), %eax
	testw	%ax, %ax
	js	.L1230
	sarl	$5, %eax
	movl	%eax, %edx
.L1231:
	movq	-472(%rbp), %rdi
	orl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	leaq	.LC22(%rip), %rcx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L1227
.L1222:
	movl	-372(%rbp), %edx
	jmp	.L1223
.L1243:
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L1245
	sarl	$5, %eax
	movl	%eax, %edx
.L1246:
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	.LC25(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	.LC25(%rip), %rdx
	testb	%al, %al
	jne	.L1247
	leaq	-460(%rbp), %rdx
	movl	$512, %esi
	movq	%r15, %rdi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	jmp	.L1244
.L1225:
	movl	-372(%rbp), %edx
	jmp	.L1226
.L1241:
	movl	12(%r12), %edx
	jmp	.L1242
.L1228:
	movl	-372(%rbp), %edx
	jmp	.L1229
.L1247:
	leaq	.LC26(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -488(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1248
	leaq	-460(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_67L22addIdentifierIgnorableEPNS_10UnicodeSetER10UErrorCode
	jmp	.L1244
.L1245:
	movl	12(%r12), %edx
	jmp	.L1246
.L1319:
	call	__stack_chk_fail@PLT
.L1191:
	movl	-460(%rbp), %eax
	testl	%eax, %eax
	jg	.L1273
.L1269:
	movl	$7, -460(%rbp)
	xorl	%r15d, %r15d
	jmp	.L1192
.L1230:
	movl	-372(%rbp), %edx
	jmp	.L1231
.L1248:
	movq	-488(%rbp), %rsi
	leaq	.LC27(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1249
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	$31, %edx
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$159, %edx
	movl	$127, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	jmp	.L1244
.L1249:
	movq	-488(%rbp), %rsi
	leaq	.LC28(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1250
	leaq	-460(%rbp), %r12
	movl	$62, %esi
	movq	%r15, %rdi
	movq	%r12, %rdx
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	movq	%r12, %rdx
	movl	$33554432, %esi
.L1308:
	movq	%r15, %rdi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	$4194304, %esi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	$512, %esi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	$1024, %esi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	$256, %esi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	movq	%r15, %rdi
	movq	%r12, %rdx
	movl	$64, %esi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_67L22addIdentifierIgnorableEPNS_10UnicodeSetER10UErrorCode
	jmp	.L1244
.L1250:
	movq	-488(%rbp), %rsi
	leaq	.LC29(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1251
	leaq	-460(%rbp), %r12
	movq	%r15, %rdi
	movl	$62, %esi
	movq	%r12, %rdx
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	$1024, %esi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	$33554432, %esi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	movq	%r12, %rdx
	movl	$4194304, %esi
	movq	%r15, %rdi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	jmp	.L1244
.L1201:
	cmpl	$0, -460(%rbp)
	jg	.L1192
	jmp	.L1269
.L1237:
	cmpl	$0, -460(%rbp)
	jg	.L1212
	movl	$7, -460(%rbp)
	jmp	.L1212
.L1251:
	movq	-488(%rbp), %rsi
	leaq	.LC30(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1252
	leaq	-460(%rbp), %rdx
	movl	$62, %esi
	movq	%r15, %rdi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	jmp	.L1244
.L1252:
	movq	-488(%rbp), %rsi
	leaq	.LC31(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1253
	leaq	-460(%rbp), %r12
	movq	%r15, %rdi
	movl	$62, %esi
	movq	%r12, %rdx
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	movq	%r12, %rdx
	movl	$512, %esi
	movq	%r15, %rdi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	jmp	.L1244
.L1253:
	movq	-488(%rbp), %rsi
	leaq	.LC32(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1254
	leaq	-460(%rbp), %rdx
	movl	$4, %esi
	movq	%r15, %rdi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	jmp	.L1244
.L1254:
	movq	-488(%rbp), %rsi
	leaq	.LC33(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1255
	leaq	-460(%rbp), %rcx
	movl	$1, %edx
	movl	$3, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	jmp	.L1244
.L1255:
	movq	-488(%rbp), %rsi
	leaq	.LC34(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1256
	leaq	-460(%rbp), %rdx
	movl	$28672, %esi
	movq	%r15, %rdi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	jmp	.L1244
.L1256:
	movq	-488(%rbp), %rsi
	leaq	.LC35(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1257
	movl	$1114111, %edx
	movl	$65536, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	jmp	.L1244
.L1257:
	movq	-488(%rbp), %rsi
	leaq	.LC36(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1258
	leaq	-460(%rbp), %rdx
	movl	$8, %esi
	movq	%r15, %rdi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	jmp	.L1244
.L1258:
	movq	-488(%rbp), %rsi
	leaq	.LC37(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1259
	leaq	-460(%rbp), %r12
	movq	%r15, %rdi
	movl	$62, %esi
	movq	%r12, %rdx
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	movq	%r12, %rdx
	movl	$1024, %esi
	movq	%r15, %rdi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	jmp	.L1244
.L1259:
	movq	-488(%rbp), %rsi
	leaq	.LC38(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1260
	leaq	-460(%rbp), %r12
	movl	$62, %esi
	movq	%r12, %rdx
	jmp	.L1308
.L1260:
	movq	-488(%rbp), %rsi
	leaq	.LC39(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1261
	leaq	-460(%rbp), %rdx
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	jmp	.L1244
.L1261:
	movq	-488(%rbp), %rsi
	leaq	.LC40(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	jne	.L1262
	movl	$1114111, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	jmp	.L1244
.L1262:
	movq	-488(%rbp), %rsi
	leaq	.LC41(%rip), %rax
	orl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	je	.L1325
	movl	$66308, -460(%rbp)
	jmp	.L1212
.L1325:
	leaq	-460(%rbp), %rdx
	leaq	-256(%rbp), %r12
	movl	$28672, %esi
	movq	%r15, %rdi
	call	_ZN6icu_67L11addCategoryEPNS_10UnicodeSetEiR10UErrorCode
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r12, %rdi
	movl	$160, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$8199, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$8239, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r15, %rdi
	movl	$13, %edx
	movl	$9, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$31, %edx
	movl	$28, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	jmp	.L1244
	.cfi_endproc
.LFE4204:
	.size	_ZN6icu_6712RegexCompile20createSetForPropertyERKNS_13UnicodeStringEa.part.0, .-_ZN6icu_6712RegexCompile20createSetForPropertyERKNS_13UnicodeStringEa.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile20createSetForPropertyERKNS_13UnicodeStringEa
	.type	_ZN6icu_6712RegexCompile20createSetForPropertyERKNS_13UnicodeStringEa, @function
_ZN6icu_6712RegexCompile20createSetForPropertyERKNS_13UnicodeStringEa:
.LFB3202:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1327
	movsbl	%dl, %edx
	jmp	_ZN6icu_6712RegexCompile20createSetForPropertyERKNS_13UnicodeStringEa.part.0
	.p2align 4,,10
	.p2align 3
.L1327:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3202:
	.size	_ZN6icu_6712RegexCompile20createSetForPropertyERKNS_13UnicodeStringEa, .-_ZN6icu_6712RegexCompile20createSetForPropertyERKNS_13UnicodeStringEa
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexCompile8scanPropEv.part.0, @function
_ZN6icu_6712RegexCompile8scanPropEv.part.0:
.LFB4205:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	72(%rdi), %r13
	pushq	%r12
	movq	%r13, %rsi
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	72(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -120(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE
	cmpl	$123, 72(%r12)
	je	.L1329
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1342:
	cmpl	$-1, %esi
	je	.L1332
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
.L1329:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE
	movl	72(%r12), %esi
	cmpl	$125, %esi
	jne	.L1342
	movq	8(%r12), %rax
	xorl	%r15d, %r15d
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1333
	xorl	%edx, %edx
	cmpl	$80, %ebx
	movq	%r14, %rsi
	movq	%r12, %rdi
	sete	%dl
	call	_ZN6icu_6712RegexCompile20createSetForPropertyERKNS_13UnicodeStringEa.part.0
	movq	%rax, %r15
.L1333:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1332:
	movl	$66308, %esi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
.L1330:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1343
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1343:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4205:
	.size	_ZN6icu_6712RegexCompile8scanPropEv.part.0, .-_ZN6icu_6712RegexCompile8scanPropEv.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile8scanPropEv
	.type	_ZN6icu_6712RegexCompile8scanPropEv, @function
_ZN6icu_6712RegexCompile8scanPropEv:
.LFB3199:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1345
	jmp	_ZN6icu_6712RegexCompile8scanPropEv.part.0
	.p2align 4,,10
	.p2align 3
.L1345:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3199:
	.size	_ZN6icu_6712RegexCompile8scanPropEv, .-_ZN6icu_6712RegexCompile8scanPropEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile13scanPosixPropEv
	.type	_ZN6icu_6712RegexCompile13scanPosixPropEv, @function
_ZN6icu_6712RegexCompile13scanPosixPropEv:
.LFB3200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L1346
	movq	32(%rdi), %rax
	movq	%rdi, %r15
	movq	%rax, -144(%rbp)
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	movslq	40(%rdi), %r13
	cmpl	28(%rdi), %r13d
	jle	.L1367
	movq	56(%rdi), %rax
	call	*64(%rax)
	movq	%rax, %r13
.L1349:
	movzwl	40(%r15), %eax
	leaq	72(%r15), %r12
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movw	%dx, -120(%rbp)
	movzbl	42(%r15), %ebx
	movw	%ax, -164(%rbp)
	movq	48(%r15), %rax
	movq	%rax, -152(%rbp)
	movq	56(%r15), %rax
	movq	%rax, -160(%rbp)
	movl	72(%r15), %eax
	movl	%eax, -168(%rbp)
	movzbl	76(%r15), %eax
	movb	%al, -161(%rbp)
	movq	64(%r15), %rax
	movq	%rax, -136(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE
	movl	72(%r15), %esi
	movl	$0, -172(%rbp)
	cmpl	$94, %esi
	je	.L1368
.L1350:
	leaq	-128(%rbp), %r14
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1370:
	movl	72(%r15), %esi
	cmpl	$-1, %esi
	je	.L1351
	cmpl	$58, %esi
	je	.L1369
.L1352:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE
	cmpb	$0, 76(%r15)
	je	.L1370
.L1351:
	movq	-144(%rbp), %rax
	movb	%bl, 42(%r15)
	movq	-152(%rbp), %xmm0
	movq	%rax, 32(%r15)
	movzwl	-164(%rbp), %eax
	movhps	-160(%rbp), %xmm0
	movw	%ax, 40(%r15)
	movq	-136(%rbp), %rax
	movups	%xmm0, 48(%r15)
	movq	%rax, 64(%r15)
	movl	-168(%rbp), %eax
	movl	%eax, 72(%r15)
	movzbl	-161(%rbp), %eax
	movb	%al, 76(%r15)
	movq	16(%r15), %rax
	movq	8(%rax), %rdi
	movq	%r13, %rax
	subq	32(%rdi), %rax
	js	.L1354
	movslq	28(%rdi), %rdx
	cmpq	%rax, %rdx
	jle	.L1354
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	jbe	.L1371
.L1354:
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	call	utext_setNativeIndex_67@PLT
.L1353:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1346:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1372
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1367:
	.cfi_restore_state
	addq	32(%rdi), %r13
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1369:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE
	cmpl	$93, 72(%r15)
	jne	.L1351
	movq	8(%r15), %rax
	xorl	%r12d, %r12d
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1353
	movl	-172(%rbp), %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6712RegexCompile20createSetForPropertyERKNS_13UnicodeStringEa.part.0
	movq	%rax, %r12
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1371:
	movl	%eax, 40(%rdi)
	xorl	%r12d, %r12d
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1368:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE
	movl	72(%r15), %esi
	movl	$1, -172(%rbp)
	jmp	.L1350
.L1372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3200:
	.size	_ZN6icu_6712RegexCompile13scanPosixPropEv, .-_ZN6icu_6712RegexCompile13scanPosixPropEv
	.section	.text.unlikely
	.align 2
.LCOLDB42:
	.text
.LHOTB42:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile7setEvalEi
	.type	_ZN6icu_6712RegexCompile7setEvalEi, @function
_ZN6icu_6712RegexCompile7setEvalEi:
.LFB3203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorw	%si, %si
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	472(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	432(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	%esi, -52(%rbp)
	.p2align 4,,10
	.p2align 3
.L1374:
	movl	480(%r12), %eax
	movq	%r14, %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector10elementAtiEi@PLT
	movl	%eax, %ebx
	xorw	%ax, %ax
	cmpl	-52(%rbp), %eax
	jb	.L1373
	movq	%r14, %rdi
	call	_ZN6icu_676UStack4popiEv@PLT
	movl	440(%r12), %eax
	movq	%r13, %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r15
	cmpl	$196612, %ebx
	je	.L1376
	jle	.L1409
	cmpl	$262150, %ebx
	je	.L1381
	jle	.L1410
	cmpl	$262151, %ebx
	jne	.L1411
.L1376:
	movq	%r13, %rdi
	call	_ZN6icu_676UStack3popEv@PLT
	movl	440(%r12), %eax
	movq	%r13, %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	testq	%r15, %r15
	je	.L1374
.L1408:
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1411:
	cmpl	$262152, %ebx
	jne	.L1412
.L1383:
	movq	%r13, %rdi
	call	_ZN6icu_676UStack3popEv@PLT
	movl	440(%r12), %eax
	movq	%r13, %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet9retainAllERKS0_@PLT
	testq	%r15, %r15
	jne	.L1408
	jmp	.L1374
.L1410:
	cmpl	$196613, %ebx
	je	.L1383
	jmp	.L1380
	.p2align 4,,10
	.p2align 3
.L1409:
	cmpl	$131075, %ebx
	jne	.L1413
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1413:
	cmpl	$131081, %ebx
	jne	.L1414
	movl	$2, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet9closeOverEi@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet16removeAllStringsEv@PLT
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	%r13, %rdi
	call	_ZN6icu_676UStack3popEv@PLT
	movl	440(%r12), %eax
	movq	%r13, %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	testq	%r15, %r15
	jne	.L1408
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1373:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1412:
	.cfi_restore_state
	jmp	.L1380
.L1414:
	jmp	.L1380
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexCompile7setEvalEi.cold, @function
_ZN6icu_6712RegexCompile7setEvalEi.cold:
.LFSB3203:
.L1380:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3203:
	.text
	.size	_ZN6icu_6712RegexCompile7setEvalEi, .-_ZN6icu_6712RegexCompile7setEvalEi
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexCompile7setEvalEi.cold, .-_ZN6icu_6712RegexCompile7setEvalEi.cold
.LCOLDE42:
	.text
.LHOTE42:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile9setPushOpEi
	.type	_ZN6icu_6712RegexCompile9setPushOpEi, @function
_ZN6icu_6712RegexCompile9setPushOpEi:
.LFB3204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6712RegexCompile7setEvalEi
	movq	8(%rbx), %rdx
	movl	%r12d, %esi
	leaq	472(%rbx), %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movl	$200, %edi
	movq	8(%rbx), %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1416
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
.L1416:
	addq	$8, %rsp
	leaq	432(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	.cfi_endproc
.LFE3204:
	.size	_ZN6icu_6712RegexCompile9setPushOpEi, .-_ZN6icu_6712RegexCompile9setPushOpEi
	.section	.text.unlikely
	.align 2
.LCOLDB43:
	.text
.LHOTB43:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile14doParseActionsEi
	.type	_ZN6icu_6712RegexCompile14doParseActionsEi, @function
_ZN6icu_6712RegexCompile14doParseActionsEi:
.LFB3174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$104, %esi
	ja	.L1422
	leaq	.L1424(%rip), %rdx
	movl	%esi, %esi
	movq	%rdi, %rbx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1424:
	.long	.L1523-.L1424
	.long	.L1522-.L1424
	.long	.L1521-.L1424
	.long	.L1520-.L1424
	.long	.L1519-.L1424
	.long	.L1518-.L1424
	.long	.L1517-.L1424
	.long	.L1516-.L1424
	.long	.L1515-.L1424
	.long	.L1514-.L1424
	.long	.L1513-.L1424
	.long	.L1512-.L1424
	.long	.L1511-.L1424
	.long	.L1496-.L1424
	.long	.L1510-.L1424
	.long	.L1509-.L1424
	.long	.L1508-.L1424
	.long	.L1507-.L1424
	.long	.L1506-.L1424
	.long	.L1491-.L1424
	.long	.L1505-.L1424
	.long	.L1504-.L1424
	.long	.L1503-.L1424
	.long	.L1502-.L1424
	.long	.L1501-.L1424
	.long	.L1500-.L1424
	.long	.L1499-.L1424
	.long	.L1498-.L1424
	.long	.L1447-.L1424
	.long	.L1497-.L1424
	.long	.L1496-.L1424
	.long	.L1495-.L1424
	.long	.L1494-.L1424
	.long	.L1493-.L1424
	.long	.L1492-.L1424
	.long	.L1491-.L1424
	.long	.L1490-.L1424
	.long	.L1447-.L1424
	.long	.L1489-.L1424
	.long	.L1488-.L1424
	.long	.L1487-.L1424
	.long	.L1486-.L1424
	.long	.L1485-.L1424
	.long	.L1484-.L1424
	.long	.L1483-.L1424
	.long	.L1482-.L1424
	.long	.L1481-.L1424
	.long	.L1480-.L1424
	.long	.L1479-.L1424
	.long	.L1478-.L1424
	.long	.L1477-.L1424
	.long	.L1476-.L1424
	.long	.L1475-.L1424
	.long	.L1474-.L1424
	.long	.L1473-.L1424
	.long	.L1472-.L1424
	.long	.L1471-.L1424
	.long	.L1470-.L1424
	.long	.L1469-.L1424
	.long	.L1468-.L1424
	.long	.L1467-.L1424
	.long	.L1466-.L1424
	.long	.L1465-.L1424
	.long	.L1464-.L1424
	.long	.L1463-.L1424
	.long	.L1462-.L1424
	.long	.L1461-.L1424
	.long	.L1460-.L1424
	.long	.L1459-.L1424
	.long	.L1422-.L1424
	.long	.L1458-.L1424
	.long	.L1457-.L1424
	.long	.L1456-.L1424
	.long	.L1455-.L1424
	.long	.L1454-.L1424
	.long	.L1453-.L1424
	.long	.L1452-.L1424
	.long	.L1451-.L1424
	.long	.L1450-.L1424
	.long	.L1449-.L1424
	.long	.L1448-.L1424
	.long	.L1447-.L1424
	.long	.L1446-.L1424
	.long	.L1445-.L1424
	.long	.L1444-.L1424
	.long	.L1443-.L1424
	.long	.L1442-.L1424
	.long	.L1441-.L1424
	.long	.L1440-.L1424
	.long	.L1439-.L1424
	.long	.L1438-.L1424
	.long	.L1437-.L1424
	.long	.L1436-.L1424
	.long	.L1435-.L1424
	.long	.L1434-.L1424
	.long	.L1433-.L1424
	.long	.L1432-.L1424
	.long	.L1431-.L1424
	.long	.L1430-.L1424
	.long	.L1429-.L1424
	.long	.L1428-.L1424
	.long	.L1427-.L1424
	.long	.L1426-.L1424
	.long	.L1425-.L1424
	.long	.L1423-.L1424
	.text
	.p2align 4,,10
	.p2align 3
.L1447:
	movl	$66305, %esi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1524:
	movl	(%rdx), %edx
	testl	%edx, %edx
	jle	.L1421
.L1779:
	xorl	%eax, %eax
.L1421:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1886
	addq	$208, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1496:
	.cfi_restore_state
	movl	72(%rdi), %esi
	movq	520(%rdi), %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
	.p2align 4,,10
	.p2align 3
.L1491:
	movl	$66309, %esi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1509:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	xorl	%edx, %edx
	movl	$57, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1510:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	xorl	%edx, %edx
	movl	$17, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1431:
	movb	$0, 42(%rdi)
	movq	8(%rdi), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1432:
	movl	72(%rdi), %esi
	leaq	304(%rdi), %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1433:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	$4, %edx
	movl	$10, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1434:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	$1, %edx
	movl	$10, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1435:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	384(%rbx), %eax
	xorl	%edx, %edx
	testl	%eax, %eax
	jle	.L1528
	subl	$1, %eax
	movq	400(%rbx), %rdx
	movl	%eax, 384(%rbx)
	cltq
	movl	(%rdx,%rax,4), %edx
.L1528:
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
	movq	8(%rbx), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L1783
	movl	8(%rdi), %eax
	leal	1(%rax), %esi
	cmpl	$16777215, %esi
	ja	.L1671
	orl	$100663296, %esi
	movslq	%esi, %rsi
.L1529:
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movl	$13, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	16(%rbx), %rax
	leaq	376(%rbx), %r13
	movq	32(%rax), %rax
	movl	8(%rax), %r12d
	movslq	384(%rbx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1531
	cmpl	388(%rbx), %esi
	jle	.L1532
.L1531:
	movq	8(%rbx), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L1533
	movslq	384(%rbx), %rax
.L1532:
	movq	400(%rbx), %rdx
	subl	$1, %r12d
	movl	%r12d, (%rdx,%rax,4)
	addl	$1, 384(%rbx)
.L1533:
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	32(%rax), %rax
	movl	8(%rax), %r12d
	movslq	384(%rbx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1534
	cmpl	388(%rbx), %esi
	jle	.L1535
.L1534:
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1536
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1436:
	movl	$1, %esi
	call	_ZN6icu_6712RegexCompile11blockTopLocEa
	movq	%rbx, %rdi
	movl	%eax, %esi
	movl	%eax, %r12d
	call	_ZN6icu_6712RegexCompile8insertOpEi
	movq	8(%rbx), %rax
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jg	.L1814
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile12allocateDataEi.part.0
	movl	%eax, %r13d
	movq	8(%rbx), %rax
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L1815
	cmpl	$16777215, %r13d
	ja	.L1671
	movl	%r13d, %esi
	orl	$536870912, %esi
	movslq	%esi, %rsi
.L1731:
	movq	16(%rbx), %rax
	movl	%r12d, %edx
	movq	32(%rax), %rdi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
	movq	8(%rbx), %rax
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jg	.L1816
	movl	8(%rdi), %eax
	leal	1(%rax), %esi
	cmpl	$16777215, %esi
	ja	.L1671
	orl	$100663296, %esi
	movslq	%esi, %rsi
.L1732:
	leal	1(%r12), %edx
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movl	%r13d, %edx
	movl	$33, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1437:
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movq	8(%rbx), %rax
	movl	(%rax), %r13d
	testl	%r13d, %r13d
	jg	.L1625
	movl	$5, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile12allocateDataEi.part.0
	movl	%eax, %r12d
.L1625:
	movl	%r12d, %edx
	movl	$44, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movl	%r12d, %edx
	movl	$45, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
	leaq	376(%rbx), %r12
	movl	292(%rbx), %r13d
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1626
	cmpl	388(%rbx), %esi
	jle	.L1627
.L1626:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1628
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1629:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1630
	cmpl	388(%rbx), %esi
	jle	.L1631
.L1630:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	je	.L1633
.L1631:
	movq	400(%rbx), %rcx
	cltq
	movl	$-7, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1633:
	movq	16(%rbx), %rcx
	movl	%eax, %esi
	movq	32(%rcx), %rcx
	movl	8(%rcx), %r13d
	addl	$1, %esi
	js	.L1634
	cmpl	388(%rbx), %esi
	jle	.L1635
.L1634:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	movq	32(%rcx), %rcx
	je	.L1637
.L1635:
	movq	400(%rbx), %rdi
	cltq
	leal	-2(%r13), %esi
	movl	%esi, (%rdi,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1637:
	movl	%eax, %esi
	movl	8(%rcx), %r13d
	addl	$1, %esi
	js	.L1638
	cmpl	388(%rbx), %esi
	jle	.L1639
.L1638:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1640
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1438:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	xorl	%edx, %edx
	movl	$23, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1448:
	movl	$29, %edx
	movl	$26, %esi
	call	_ZN6icu_6712RegexCompile15compileIntervalEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1449:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1537
	movl	$2, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	%rcx, (%rax)
	movq	%rax, 520(%rbx)
	movl	$1, %eax
	jmp	.L1524
.L1450:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	292(%rbx), %eax
	andl	$9, %eax
	je	.L1887
	cmpl	$8, %eax
	je	.L1888
	cmpl	$1, %eax
	je	.L1889
	cmpl	$9, %eax
	je	.L1711
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1451:
	movl	$1, %esi
	call	_ZN6icu_6712RegexCompile11blockTopLocEa
	movl	%eax, %edx
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
	movq	8(%rbx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1793
	movl	8(%rdi), %esi
	cmpl	$16777215, %esi
	ja	.L1671
	orl	$100663296, %esi
	movslq	%esi, %rsi
.L1672:
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1452:
	movq	16(%rdi), %rax
	movl	72(%rdi), %r12d
	xorl	%r13d, %r13d
	movq	136(%rax), %rax
	movl	8(%rax), %r14d
	.p2align 4,,10
	.p2align 3
.L1716:
	movl	%r12d, %edi
	call	u_charDigitValue_67@PLT
	leal	0(%r13,%r13,4), %edx
	leal	(%rax,%rdx,2), %r13d
	cmpl	%r14d, %r13d
	jge	.L1717
	movl	68(%rbx), %r12d
	cmpl	$-1, %r12d
	je	.L1890
.L1718:
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	movl	%r12d, %esi
	movq	3824(%rax), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L1717
	cmpl	$-1, 68(%rbx)
	je	.L1719
	movl	$-1, 68(%rbx)
	jmp	.L1716
.L1439:
	movl	72(%rdi), %edx
	cmpl	$45, %edx
	je	.L1733
	subl	$100, %edx
	cmpl	$20, %edx
	ja	.L1422
	leaq	.L1735(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1735:
	.long	.L1741-.L1735
	.long	.L1422-.L1735
	.long	.L1422-.L1735
	.long	.L1422-.L1735
	.long	.L1422-.L1735
	.long	.L1740-.L1735
	.long	.L1422-.L1735
	.long	.L1422-.L1735
	.long	.L1422-.L1735
	.long	.L1817-.L1735
	.long	.L1422-.L1735
	.long	.L1422-.L1735
	.long	.L1422-.L1735
	.long	.L1422-.L1735
	.long	.L1422-.L1735
	.long	.L1738-.L1735
	.long	.L1422-.L1735
	.long	.L1737-.L1735
	.long	.L1422-.L1735
	.long	.L1736-.L1735
	.long	.L1734-.L1735
	.text
.L1817:
	movl	$-9, %ecx
	movl	$8, %eax
.L1739:
	cmpb	$0, 300(%rbx)
	movl	296(%rbx), %esi
	movq	8(%rbx), %rdx
	je	.L1743
	orl	%esi, %eax
	movl	%eax, 296(%rbx)
	movl	$1, %eax
	jmp	.L1524
.L1741:
	movl	$-2, %ecx
	movl	$1, %eax
	jmp	.L1739
.L1740:
	movl	$-3, %ecx
	movl	$2, %eax
	jmp	.L1739
.L1734:
	movl	$-5, %ecx
	movl	$4, %eax
	jmp	.L1739
.L1736:
	movl	$-257, %ecx
	movl	$256, %eax
	jmp	.L1739
.L1737:
	movl	$-1, %ecx
	xorl	%eax, %eax
	jmp	.L1739
.L1738:
	movl	$-33, %ecx
	movl	$32, %eax
	jmp	.L1739
.L1441:
	movl	440(%rdi), %eax
	leaq	-240(%rbp), %r12
	leaq	432(%rdi), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	8(%rbx), %rcx
	movl	$4096, %edx
	movq	%r12, %rdi
	movl	$8192, %esi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1442:
	movl	72(%rdi), %edi
	call	u_charDigitValue_67@PLT
	movslq	416(%rbx), %rdx
	cltq
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rax,%rdx,2), %rax
	cmpq	$2147483647, %rax
	jg	.L1891
	movl	%eax, 416(%rbx)
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1456:
	movl	440(%rdi), %eax
	leaq	432(%rdi), %rdi
	leaq	-240(%rbp), %r12
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	8(%rbx), %r14
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	$512, %edx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	$8192, %esi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1443:
	movl	$262150, %esi
	leaq	472(%rbx), %r13
	call	_ZN6icu_6712RegexCompile7setEvalEi
	movq	8(%rbx), %rdx
	movl	$262150, %esi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movl	$200, %edi
	movq	8(%rbx), %r14
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1766
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
.L1766:
	leaq	432(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	testb	$2, 292(%rbx)
	movq	8(%rbx), %rdx
	movl	$1, %eax
	je	.L1524
	movl	$131081, %esi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1444:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	292(%rbx), %eax
	movq	%rbx, %rdi
	andl	$256, %eax
	cmpl	$1, %eax
	sbbl	%esi, %esi
	xorl	%edx, %edx
	andl	$-37, %esi
	addl	$53, %esi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1445:
	movl	$262152, %esi
	leaq	472(%rbx), %r13
	call	_ZN6icu_6712RegexCompile7setEvalEi
	movq	8(%rbx), %rdx
	movl	$262152, %esi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movl	$200, %edi
	movq	8(%rbx), %r14
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1764
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
.L1764:
	leaq	432(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	testb	$2, 292(%rbx)
	movq	8(%rbx), %rdx
	movl	$1, %eax
	je	.L1524
	movl	$131081, %esi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1446:
	call	_ZN6icu_6712RegexCompile13scanPosixPropEv
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1892
	movl	440(%rbx), %eax
	leaq	432(%rbx), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1440:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11blockTopLocEa
	movl	%eax, %edx
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
	movl	8(%rdi), %eax
	leal	-1(%rax), %r8d
	cmpl	%edx, %r8d
	jne	.L1674
	cmpl	%eax, %edx
	jge	.L1674
	testl	%edx, %edx
	jns	.L1893
.L1674:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile11blockTopLocEa
	movl	%eax, %r14d
	leal	1(%rax), %r13d
	movq	8(%rbx), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L1799
	cmpl	$16777215, %r13d
	ja	.L1671
	movl	%r13d, %r12d
	orl	$251658240, %r12d
.L1682:
	movq	16(%rbx), %rax
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movq	32(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	call	_ZN6icu_6712RegexCompile14minMatchLengthEii
	testl	%eax, %eax
	je	.L1894
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
	movq	8(%rbx), %rax
	movl	(%rax), %r13d
	testl	%r13d, %r13d
	jg	.L1800
	movl	8(%rdi), %eax
	leal	1(%rax), %esi
.L1688:
	cmpl	$16777215, %esi
	ja	.L1671
	orl	$100663296, %esi
	movslq	%esi, %rsi
.L1689:
	movl	%r14d, %edx
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEi
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1454:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movq	8(%rbx), %rax
	xorl	%esi, %esi
	movl	(%rax), %edi
	testl	%edi, %edi
	jg	.L1714
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8scanPropEv.part.0
	movq	%rax, %rsi
.L1714:
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile10compileSetEPNS_10UnicodeSetE
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1453:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1722
	movl	$2, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	%rcx, (%rax)
	movq	%rax, 520(%rbx)
	movl	$1, %eax
	jmp	.L1524
.L1460:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	292(%rbx), %eax
	xorl	%edx, %edx
	testb	$32, %al
	jne	.L1895
	testb	$1, %al
	je	.L1703
	movl	$27, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1455:
	leaq	432(%rdi), %rdi
	call	_ZN6icu_676UStack3popEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6712RegexCompile10compileSetEPNS_10UnicodeSetE
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1458:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rax
	xorl	%edx, %edx
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1577
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile12allocateDataEi.part.0
	movl	%eax, %edx
.L1577:
	movl	$32, %esi
	movq	%rbx, %rdi
	leaq	376(%rbx), %r12
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
	movl	292(%rbx), %r13d
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1578
	cmpl	388(%rbx), %esi
	jle	.L1579
.L1578:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1580
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1581:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1582
	cmpl	388(%rbx), %esi
	jle	.L1583
.L1582:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	je	.L1585
.L1583:
	movq	400(%rbx), %rcx
	cltq
	movl	$-3, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1585:
	movq	16(%rbx), %rcx
	movl	%eax, %esi
	movq	32(%rcx), %rcx
	movl	8(%rcx), %r13d
	addl	$1, %esi
	js	.L1586
	cmpl	388(%rbx), %esi
	jle	.L1587
.L1586:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	movq	32(%rcx), %rcx
	je	.L1589
.L1587:
	movq	400(%rbx), %rdi
	cltq
	leal	-3(%r13), %esi
	movl	%esi, (%rdi,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1589:
	movl	%eax, %esi
	movl	8(%rcx), %r13d
	addl	$1, %esi
	js	.L1590
	cmpl	388(%rbx), %esi
	jle	.L1591
.L1590:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1592
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1459:
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1538
	movl	$3, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile17allocateStackDataEi.part.0
	movl	%eax, %r12d
.L1538:
	movl	%r12d, %edx
	movl	$8, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
	leaq	376(%rbx), %r13
	movl	292(%rbx), %r14d
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1539
	cmpl	388(%rbx), %esi
	jle	.L1540
.L1539:
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1541
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1542:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1543
	cmpl	388(%rbx), %esi
	jle	.L1544
.L1543:
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	je	.L1546
.L1544:
	movq	400(%rbx), %rcx
	cltq
	movl	$-2, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1546:
	movq	16(%rbx), %rdi
	movl	%eax, %esi
	movq	32(%rdi), %rcx
	movl	8(%rcx), %r14d
	addl	$1, %esi
	js	.L1547
	cmpl	388(%rbx), %esi
	jle	.L1548
.L1547:
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	16(%rbx), %rdi
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	movq	32(%rdi), %rcx
	je	.L1550
.L1548:
	movq	400(%rbx), %r8
	cltq
	leal	-3(%r14), %esi
	movl	%esi, (%r8,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1550:
	movl	%eax, %esi
	movl	8(%rcx), %r14d
	addl	$1, %esi
	js	.L1551
	cmpl	388(%rbx), %esi
	jle	.L1552
.L1551:
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1553
	movq	16(%rbx), %rdi
	movq	8(%rbx), %rdx
.L1554:
	movq	136(%rdi), %r13
	movslq	8(%r13), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1555
	cmpl	12(%r13), %esi
	jle	.L1556
.L1555:
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L1557
	movslq	8(%r13), %rax
.L1556:
	movq	24(%r13), %rdx
	movl	%r12d, (%rdx,%rax,4)
	addl	$1, 8(%r13)
.L1557:
	cmpq	$0, 520(%rbx)
	je	.L1896
	movq	16(%rbx), %rdi
	call	_ZN6icu_6712RegexPattern19initNamedCaptureMapEv@PLT
	testb	%al, %al
	jne	.L1559
	movq	8(%rbx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1779
	movq	16(%rbx), %rax
	movq	%rbx, %rdi
	movl	120(%rax), %esi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
	.p2align 4,,10
	.p2align 3
.L1457:
	movl	$66315, %esi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1513:
	movq	8(%rdi), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1514:
	movl	$1, %esi
	call	_ZN6icu_6712RegexCompile11blockTopLocEa
	movl	%eax, %r12d
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
	movq	8(%rbx), %rax
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jg	.L1801
	movl	8(%rdi), %esi
	cmpl	$16777215, %esi
	ja	.L1671
	orl	$218103808, %esi
	movslq	%esi, %rsi
.L1690:
	movl	%r12d, %edx
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	leal	1(%r12), %edx
	movl	$6, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1512:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	xorl	%edx, %edx
	movl	$19, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1428:
	movl	$1, %esi
	call	_ZN6icu_6712RegexCompile11blockTopLocEa
	movl	%eax, %r12d
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
	movq	8(%rbx), %rax
	movl	(%rax), %r14d
	movl	8(%rdi), %r13d
	testl	%r14d, %r14d
	jg	.L1794
	leal	1(%r13), %esi
	cmpl	$16777215, %esi
	ja	.L1671
	orl	$218103808, %esi
	movslq	%esi, %rsi
.L1673:
	movl	%r12d, %edx
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	leal	2(%r13), %edx
	movl	$13, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	leal	1(%r12), %edx
	movl	$6, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1429:
	movl	416(%rdi), %eax
	movq	8(%rdi), %rdx
	movl	%eax, 420(%rdi)
	movl	$1, %eax
	jmp	.L1524
.L1430:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	292(%rbx), %eax
	andl	$9, %eax
	je	.L1706
	cmpl	$8, %eax
	je	.L1897
	cmpl	$1, %eax
	je	.L1706
	cmpl	$9, %eax
	je	.L1707
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1489:
	movl	$1, %esi
	call	_ZN6icu_6712RegexCompile11blockTopLocEa
	movq	%rbx, %rdi
	movl	%eax, %esi
	movl	%eax, %r12d
	call	_ZN6icu_6712RegexCompile8insertOpEi
	movq	8(%rbx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1811
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile12allocateDataEi.part.0
	movl	%eax, %r13d
	movq	8(%rbx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1812
	cmpl	$16777215, %r13d
	ja	.L1671
	movl	%r13d, %esi
	orl	$536870912, %esi
	movslq	%esi, %rsi
.L1729:
	movq	16(%rbx), %rax
	movl	%r12d, %edx
	movq	32(%rax), %rdi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
	movq	8(%rbx), %rax
	movl	(%rax), %r14d
	testl	%r14d, %r14d
	jg	.L1813
	movl	8(%rdi), %eax
	leal	1(%rax), %esi
	cmpl	$16777215, %esi
	ja	.L1671
	orl	$100663296, %esi
	movslq	%esi, %rsi
.L1730:
	addl	$1, %r12d
	movl	%r12d, %edx
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movl	%r12d, %edx
	movl	$13, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movl	%r13d, %edx
	movl	$33, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1492:
	movl	440(%rdi), %eax
	leaq	-240(%rbp), %r12
	leaq	432(%rdi), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	$13, %edx
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$133, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$8233, %edx
	movl	$8232, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1493:
	movq	8(%rdi), %rax
	xorl	%r12d, %r12d
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L1771
	call	_ZN6icu_6712RegexCompile13scanNamedCharEv.part.0
	movl	%eax, %r12d
.L1771:
	movl	$262150, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile7setEvalEi
	movl	440(%rbx), %eax
	leaq	432(%rbx), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	%r12d, 512(%rbx)
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1515:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	$1, %edx
	movl	$22, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1516:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	$4, %edx
	movl	$49, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1517:
	movl	$2, %edx
	movl	$6, %esi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movl	$3, %edx
	movl	$13, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$14, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movl	$31, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile14doParseActionsEi
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1518:
	movq	16(%rdi), %rax
	movq	192(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1725
	movq	520(%rbx), %rsi
	call	uhash_geti_67@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L1898
.L1725:
	movl	$66325, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
.L1724:
	movq	520(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1727
	movq	(%rdi), %rax
	call	*8(%rax)
.L1727:
	movq	$0, 520(%rbx)
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1519:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movq	8(%rbx), %rax
	xorl	%edx, %edx
	movl	(%rax), %r14d
	testl	%r14d, %r14d
	jg	.L1609
	movl	$4, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile12allocateDataEi.part.0
	movl	%eax, %edx
.L1609:
	movl	$37, %esi
	movq	%rbx, %rdi
	leaq	376(%rbx), %r12
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
	movl	292(%rbx), %r13d
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1610
	cmpl	388(%rbx), %esi
	jle	.L1611
.L1610:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1612
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1613:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1614
	cmpl	388(%rbx), %esi
	jle	.L1615
.L1614:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	je	.L1617
.L1615:
	movq	400(%rbx), %rcx
	cltq
	movl	$-5, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1617:
	movq	16(%rbx), %rcx
	movl	%eax, %esi
	movq	32(%rcx), %rcx
	movl	8(%rcx), %r13d
	addl	$1, %esi
	js	.L1618
	cmpl	388(%rbx), %esi
	jle	.L1619
.L1618:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	movq	32(%rcx), %rcx
	je	.L1621
.L1619:
	movq	400(%rbx), %rdi
	cltq
	leal	-2(%r13), %esi
	movl	%esi, (%rdi,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1621:
	movl	%eax, %esi
	movl	8(%rcx), %r13d
	addl	$1, %esi
	js	.L1622
	cmpl	388(%rbx), %esi
	jle	.L1623
.L1622:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1624
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1520:
	testb	$2, 293(%rdi)
	je	.L1770
	movl	72(%rdi), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpl	$25, %eax
	jbe	.L1899
.L1770:
	movl	$262150, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile7setEvalEi
	movl	440(%rbx), %eax
	leaq	432(%rbx), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	72(%rbx), %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	72(%rbx), %eax
	movq	8(%rbx), %rdx
	movl	%eax, 512(%rbx)
	movl	$1, %eax
	jmp	.L1524
.L1521:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	$1, %edx
	movl	$56, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1522:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	xorl	%edx, %edx
	movl	$56, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1523:
	movl	440(%rdi), %eax
	leaq	-240(%rbp), %r12
	leaq	432(%rdi), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	8(%rbx), %rcx
	movl	$512, %edx
	movq	%r12, %rdi
	movl	$8192, %esi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1423:
	movl	440(%rdi), %eax
	leaq	432(%rdi), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rdi
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	leaq	808(%rax), %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1425:
	movl	$66310, %esi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1426:
	movl	440(%rdi), %eax
	leaq	-240(%rbp), %r12
	leaq	432(%rdi), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	movq	%r12, %rdi
	leaq	208(%rax), %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1427:
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movq	8(%rbx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1593
	movl	$4, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile12allocateDataEi.part.0
	movl	%eax, %r12d
.L1593:
	movl	%r12d, %edx
	movl	$37, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	16(%rbx), %rax
	movl	$6, %esi
	movq	%rbx, %rdi
	movq	32(%rax), %rax
	movl	8(%rax), %edx
	addl	$2, %edx
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	16(%rbx), %rax
	movl	$13, %esi
	movq	%rbx, %rdi
	movq	32(%rax), %rax
	movl	8(%rax), %edx
	addl	$3, %edx
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movl	%r12d, %edx
	movl	$38, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
	leaq	376(%rbx), %r12
	movl	292(%rbx), %r13d
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1594
	cmpl	388(%rbx), %esi
	jle	.L1595
.L1594:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1596
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1597:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1598
	cmpl	388(%rbx), %esi
	jle	.L1599
.L1598:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	je	.L1601
.L1599:
	movq	400(%rbx), %rcx
	cltq
	movl	$-4, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1601:
	movq	16(%rbx), %rcx
	movl	%eax, %esi
	movq	32(%rcx), %rcx
	movl	8(%rcx), %r13d
	addl	$1, %esi
	js	.L1602
	cmpl	388(%rbx), %esi
	jle	.L1603
.L1602:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	movq	32(%rcx), %rcx
	je	.L1605
.L1603:
	movq	400(%rbx), %rdi
	cltq
	leal	-2(%r13), %esi
	movl	%esi, (%rdi,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1605:
	movl	%eax, %esi
	movl	8(%rcx), %r13d
	addl	$1, %esi
	js	.L1606
	cmpl	388(%rbx), %esi
	jle	.L1607
.L1606:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1608
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1502:
	movl	$66312, %esi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1503:
	movl	$262151, %esi
	leaq	472(%rbx), %r13
	call	_ZN6icu_6712RegexCompile7setEvalEi
	movq	8(%rbx), %rdx
	movl	$262151, %esi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movl	$200, %edi
	movq	8(%rbx), %r14
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1762
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
.L1762:
	leaq	432(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	testb	$2, 292(%rbx)
	movq	8(%rbx), %rdx
	movl	$1, %eax
	je	.L1524
	movl	$131081, %esi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1504:
	movl	440(%rdi), %eax
	leaq	432(%rdi), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	$38, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1505:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	xorl	%edx, %edx
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1498:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11blockTopLocEa
	movl	$28, %edx
	movl	$25, %esi
	movq	%rbx, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6712RegexCompile15compileIntervalEii
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8insertOpEi
	movq	8(%rbx), %rax
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jg	.L1802
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile12allocateDataEi.part.0
	movl	%eax, %r13d
	movq	8(%rbx), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L1803
	cmpl	$16777215, %r13d
	ja	.L1671
	movl	%r13d, %esi
	orl	$536870912, %esi
	movslq	%esi, %rsi
.L1695:
	movq	16(%rbx), %rax
	movl	%r12d, %edx
	movq	32(%rax), %rdi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movq	16(%rbx), %rax
	movq	32(%rax), %r14
	movl	8(%r14), %esi
	testl	%esi, %esi
	jle	.L1900
	leal	-1(%rsi), %eax
	movq	24(%r14), %rdx
	movslq	%eax, %rcx
	movl	%eax, 8(%r14)
	movq	(%rdx,%rcx,8), %r12
	addl	$1, %r12d
	movslq	%r12d, %r12
.L1697:
	cmpl	%esi, 12(%r14)
	jge	.L1699
.L1698:
	movq	8(%rbx), %rdx
	movq	%r14, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L1700
	movl	8(%r14), %eax
	leal	1(%rax), %esi
.L1699:
	movq	24(%r14), %rdx
	cltq
	movq	%r12, (%rdx,%rax,8)
	movl	%esi, 8(%r14)
.L1700:
	movl	%r13d, %edx
	movl	$33, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1499:
	movl	$196613, %esi
	call	_ZN6icu_6712RegexCompile7setEvalEi
	movq	8(%rbx), %rdx
	movl	$196613, %esi
	leaq	472(%rbx), %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movl	$200, %edi
	movq	8(%rbx), %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1769
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
.L1769:
	movq	%r13, %rdx
	leaq	432(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1500:
	movabsq	$-4294967296, %rax
	movq	8(%rdi), %rdx
	movq	%rax, 416(%rdi)
	movl	$1, %eax
	jmp	.L1524
.L1501:
	movl	480(%rdi), %eax
	leaq	472(%rdi), %r12
	movq	%r12, %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector10elementAtiEi@PLT
	cmpl	$131081, %eax
	je	.L1901
	movq	8(%rbx), %rdx
	movl	$131075, %esi
	movq	%r12, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1511:
	movl	$262150, %esi
	call	_ZN6icu_6712RegexCompile7setEvalEi
	movl	440(%rbx), %eax
	leaq	432(%rbx), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	72(%rbx), %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	72(%rbx), %eax
	movq	8(%rbx), %rdx
	movl	%eax, 512(%rbx)
	movl	$1, %eax
	jmp	.L1524
.L1465:
	movq	8(%rdi), %rax
	xorl	%esi, %esi
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L1715
	call	_ZN6icu_6712RegexCompile13scanNamedCharEv.part.0
	movl	%eax, %esi
.L1715:
	leaq	304(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1466:
	movl	384(%rdi), %edi
	testl	%edi, %edi
	jle	.L1902
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile16handleCloseParenEv.part.0
.L1526:
	movl	384(%rbx), %esi
	testl	%esi, %esi
	jle	.L1527
	movl	$66310, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
.L1527:
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	xorl	%eax, %eax
	jmp	.L1524
.L1467:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	xorl	%edx, %edx
	movl	$22, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1468:
	movl	292(%rdi), %eax
	movq	8(%rdi), %rdx
	movb	$1, 300(%rdi)
	movl	%eax, 296(%rdi)
	movl	$1, %eax
	jmp	.L1524
.L1469:
	movl	$66325, %esi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1470:
	movl	420(%rdi), %eax
	cmpl	$10, %eax
	jg	.L1694
	cmpl	416(%rdi), %eax
	jl	.L1694
	call	_ZN6icu_6712RegexCompile21compileInlineIntervalEv.part.0
	testb	%al, %al
	je	.L1694
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1471:
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movq	8(%rbx), %rax
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jg	.L1641
	movl	$5, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile12allocateDataEi.part.0
	movl	%eax, %r12d
.L1641:
	movl	%r12d, %edx
	movl	$44, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movl	%r12d, %edx
	movl	$47, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
	leaq	376(%rbx), %r12
	movl	292(%rbx), %r13d
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1642
	cmpl	388(%rbx), %esi
	jle	.L1643
.L1642:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1644
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1645:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1646
	cmpl	388(%rbx), %esi
	jle	.L1647
.L1646:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	je	.L1649
.L1647:
	movq	400(%rbx), %rcx
	cltq
	movl	$-8, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1649:
	movq	16(%rbx), %rcx
	movl	%eax, %esi
	movq	32(%rcx), %rcx
	movl	8(%rcx), %r13d
	addl	$1, %esi
	js	.L1650
	cmpl	388(%rbx), %esi
	jle	.L1651
.L1650:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	movq	32(%rcx), %rcx
	je	.L1653
.L1651:
	movq	400(%rbx), %rdi
	cltq
	leal	-2(%r13), %esi
	movl	%esi, (%rdi,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1653:
	movl	%eax, %esi
	movl	8(%rcx), %r13d
	addl	$1, %esi
	js	.L1654
	cmpl	388(%rbx), %esi
	jle	.L1655
.L1654:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1656
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1481:
	movq	8(%rdi), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L1779
	call	_ZN6icu_6712RegexCompile8scanPropEv.part.0
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1903
	movl	440(%rbx), %eax
	leaq	432(%rbx), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1473:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	xorl	%edx, %edx
	movl	$20, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1490:
	movl	$66307, %esi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1485:
	movl	420(%rdi), %r10d
	testl	%r10d, %r10d
	jns	.L1692
	movl	$0, 420(%rdi)
.L1692:
	movl	72(%rbx), %edi
	call	u_charDigitValue_67@PLT
	movslq	420(%rbx), %rdx
	cltq
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rax,%rdx,2), %rax
	cmpq	$2147483647, %rax
	jg	.L1904
	movl	%eax, 420(%rbx)
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1486:
	movl	440(%rdi), %eax
	leaq	432(%rdi), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rdi
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	leaq	208(%rax), %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1472:
	movq	8(%rdi), %rax
	xorl	%r12d, %r12d
	movl	(%rax), %edi
	testl	%edi, %edi
	jle	.L1905
.L1772:
	movl	440(%rbx), %eax
	leaq	432(%rbx), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	512(%rbx), %esi
	movl	%r12d, %edx
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	%r12d, 512(%rbx)
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1477:
	xorl	%esi, %esi
	leaq	376(%rbx), %r12
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
	movl	292(%rbx), %r13d
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1744
	cmpl	388(%rbx), %esi
	jle	.L1745
.L1744:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1746
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1747:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1748
	cmpl	388(%rbx), %esi
	jle	.L1749
.L1748:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	je	.L1751
.L1749:
	movq	400(%rbx), %rcx
	cltq
	movl	$-6, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1751:
	movq	16(%rbx), %rcx
	movl	%eax, %esi
	movq	32(%rcx), %rcx
	movl	8(%rcx), %r13d
	addl	$1, %esi
	js	.L1752
	cmpl	388(%rbx), %esi
	jle	.L1753
.L1752:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	movq	32(%rcx), %rcx
	je	.L1755
.L1753:
	movq	400(%rbx), %rdi
	cltq
	leal	-2(%r13), %esi
	movl	%esi, (%rdi,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1755:
	movl	%eax, %esi
	movl	8(%rcx), %r13d
	addl	$1, %esi
	js	.L1756
	cmpl	388(%rbx), %esi
	jle	.L1757
.L1756:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1758
	movq	8(%rbx), %rdx
.L1759:
	movl	296(%rbx), %eax
	movl	%eax, 292(%rbx)
	movl	$1, %eax
	jmp	.L1524
.L1483:
	movl	440(%rdi), %eax
	leaq	-240(%rbp), %r12
	leaq	432(%rdi), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	movq	%r12, %rdi
	leaq	808(%rax), %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1475:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	$1, %edx
	movl	$58, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1476:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11blockTopLocEa
	movl	%eax, %r12d
	movq	16(%rbx), %rax
	movq	32(%rax), %rcx
	movl	8(%rcx), %eax
	leal	-1(%rax), %edx
	cmpl	%r12d, %edx
	jne	.L1660
	cmpl	%eax, %edx
	jge	.L1660
	testl	%edx, %edx
	jns	.L1906
.L1660:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile14minMatchLengthEii
	testl	%eax, %eax
	jne	.L1667
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8insertOpEi
	movq	8(%rbx), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L1670
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile17allocateStackDataEi.part.0
	movq	8(%rbx), %rdx
	movl	(%rdx), %edx
	testl	%edx, %edx
	jg	.L1670
	cmpl	$16777215, %eax
	ja	.L1671
	orl	$587202560, %eax
	movslq	%eax, %rsi
.L1669:
	movq	16(%rbx), %rax
	movl	%r12d, %edx
	movq	32(%rax), %rdi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	leal	1(%r12), %edx
	movl	$18, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1474:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	296(%rbx), %eax
	movq	8(%rbx), %rdx
	movl	%eax, 292(%rbx)
	movl	$1, %eax
	jmp	.L1524
.L1487:
	movl	72(%rdi), %esi
	testb	$2, 293(%rdi)
	je	.L1701
	movl	%esi, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpl	$25, %eax
	jbe	.L1907
.L1701:
	leaq	304(%rbx), %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1488:
	movl	440(%rdi), %eax
	leaq	432(%rdi), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	$45, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1482:
	movl	$66319, %esi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1484:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	xorl	%edx, %edx
	movl	$58, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1479:
	movl	$65538, %esi
	call	_ZN6icu_6712RegexCompile7setEvalEi
	leaq	472(%rbx), %rdi
	call	_ZN6icu_676UStack4popiEv@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1480:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	292(%rbx), %eax
	movl	$1, %edx
	movq	%rbx, %rdi
	andl	$256, %eax
	cmpl	$1, %eax
	sbbl	%esi, %esi
	andl	$-37, %esi
	addl	$53, %esi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1478:
	movl	512(%rdi), %eax
	cmpl	$-1, %eax
	je	.L1777
	cmpl	72(%rdi), %eax
	jle	.L1778
.L1777:
	movl	$66320, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
.L1778:
	movl	440(%rbx), %eax
	leaq	432(%rbx), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	72(%rbx), %edx
	movl	512(%rbx), %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1508:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	$200, %edi
	movq	8(%rbx), %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1760
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
.L1760:
	movq	%r12, %rsi
	leaq	432(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	leaq	472(%rbx), %r12
	movq	8(%rbx), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	testb	$2, 292(%rbx)
	movq	8(%rbx), %rdx
	movl	$1, %eax
	je	.L1524
	movl	$131081, %esi
	movq	%r12, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1494:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movq	8(%rbx), %rdx
	xorl	%eax, %eax
	jmp	.L1524
.L1495:
	xorl	%esi, %esi
	leaq	376(%rbx), %r12
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
	movl	292(%rbx), %r13d
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1562
	cmpl	388(%rbx), %esi
	jle	.L1563
.L1562:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1564
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1565:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1566
	cmpl	388(%rbx), %esi
	jle	.L1567
.L1566:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	je	.L1569
.L1567:
	movq	400(%rbx), %rcx
	cltq
	movl	$-1, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1569:
	movq	16(%rbx), %rcx
	movl	%eax, %esi
	movq	32(%rcx), %rcx
	movl	8(%rcx), %r13d
	addl	$1, %esi
	js	.L1570
	cmpl	388(%rbx), %esi
	jle	.L1571
.L1570:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdx
	testb	%al, %al
	movl	384(%rbx), %eax
	movq	32(%rcx), %rcx
	je	.L1573
.L1571:
	movq	400(%rbx), %rdi
	cltq
	leal	-2(%r13), %esi
	movl	%esi, (%rdi,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
.L1573:
	movl	%eax, %esi
	movl	8(%rcx), %r13d
	addl	$1, %esi
	js	.L1574
	cmpl	388(%rbx), %esi
	jle	.L1575
.L1574:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1576
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1497:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	$1, %edx
	movl	$49, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1461:
	movl	384(%rdi), %r10d
	testl	%r10d, %r10d
	jle	.L1908
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile16handleCloseParenEv.part.0
.L1658:
	movl	384(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L1659
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1462:
	movl	440(%rdi), %eax
	leaq	-240(%rbp), %r12
	leaq	432(%rdi), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	8(%rbx), %rcx
	movl	$4096, %edx
	movq	%r12, %rdi
	movl	$8192, %esi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1463:
	movl	$196612, %esi
	call	_ZN6icu_6712RegexCompile7setEvalEi
	movq	8(%rbx), %rdx
	movl	$196612, %esi
	leaq	472(%rbx), %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movl	$200, %edi
	movq	8(%rbx), %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1768
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
.L1768:
	movq	%r13, %rdx
	leaq	432(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1464:
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexCompile11blockTopLocEa
	movl	$6, %esi
	movq	%rbx, %rdi
	movl	%eax, %edx
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1506:
	movl	$1, %esi
	call	_ZN6icu_6712RegexCompile11blockTopLocEa
	movl	%eax, %r12d
	movq	8(%rbx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1809
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile12allocateDataEi.part.0
	movl	%eax, %r13d
	movq	8(%rbx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1810
	cmpl	$16777215, %r13d
	ja	.L1671
	movl	%r13d, %esi
	orl	$536870912, %esi
	movslq	%esi, %rsi
.L1728:
	movq	16(%rbx), %rax
	movl	%r12d, %edx
	movq	32(%rax), %rdi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movq	16(%rbx), %rax
	movl	$6, %esi
	movq	%rbx, %rdi
	movq	32(%rax), %rax
	movl	8(%rax), %edx
	addl	$2, %edx
	call	_ZN6icu_6712RegexCompile8appendOpEii
	leal	1(%r12), %edx
	movl	$13, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movl	%r13d, %edx
	movl	$33, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1507:
	movl	440(%rdi), %eax
	leaq	432(%rdi), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	$13, %edx
	movl	$10, %esi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$133, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$8233, %edx
	movl	$8232, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
	.p2align 4,,10
	.p2align 3
.L1719:
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1890:
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile10nextCharLLEv.part.0
	movl	%eax, 68(%rbx)
	movl	%eax, %r12d
	jmp	.L1718
.L1717:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	%r13d, %edx
	testb	$2, 292(%rbx)
	je	.L1721
	movl	$41, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1694:
	movl	$28, %edx
	movl	$25, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile15compileIntervalEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1758:
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1757:
	movq	400(%rbx), %rcx
	cltq
	subl	$1, %r13d
	movl	%r13d, (%rcx,%rax,4)
	addl	$1, 384(%rbx)
	jmp	.L1759
.L1608:
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1607:
	movq	400(%rbx), %rcx
	cltq
	subl	$1, %r13d
	movl	%r13d, (%rcx,%rax,4)
	movl	$1, %eax
	addl	$1, 384(%rbx)
	jmp	.L1524
.L1596:
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
.L1595:
	movq	400(%rbx), %rcx
	movl	%r13d, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
	jmp	.L1597
.L1644:
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
.L1643:
	movq	400(%rbx), %rcx
	movl	%r13d, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
	jmp	.L1645
.L1746:
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
.L1745:
	movq	400(%rbx), %rcx
	movl	%r13d, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
	jmp	.L1747
.L1656:
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1655:
	movq	400(%rbx), %rcx
	cltq
	subl	$1, %r13d
	movl	%r13d, (%rcx,%rax,4)
	movl	$1, %eax
	addl	$1, 384(%rbx)
	jmp	.L1524
.L1541:
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
.L1540:
	movq	400(%rbx), %rcx
	movl	%r14d, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
	jmp	.L1542
.L1553:
	movl	384(%rbx), %eax
	movq	16(%rbx), %rdi
	movq	8(%rbx), %rdx
.L1552:
	movq	400(%rbx), %rsi
	cltq
	leal	-1(%r14), %ecx
	movl	%ecx, (%rsi,%rax,4)
	addl	$1, 384(%rbx)
	jmp	.L1554
.L1536:
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
.L1535:
	movq	400(%rbx), %rcx
	subl	$1, %r12d
	movl	%r12d, (%rcx,%rax,4)
	movl	$1, %eax
	addl	$1, 384(%rbx)
	jmp	.L1524
.L1628:
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
.L1627:
	movq	400(%rbx), %rcx
	movl	%r13d, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
	jmp	.L1629
.L1640:
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1639:
	movq	400(%rbx), %rcx
	cltq
	subl	$1, %r13d
	movl	%r13d, (%rcx,%rax,4)
	movl	$1, %eax
	addl	$1, 384(%rbx)
	jmp	.L1524
.L1576:
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1575:
	movq	400(%rbx), %rcx
	cltq
	subl	$1, %r13d
	movl	%r13d, (%rcx,%rax,4)
	movl	$1, %eax
	addl	$1, 384(%rbx)
	jmp	.L1524
.L1564:
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
.L1563:
	movq	400(%rbx), %rcx
	movl	%r13d, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
	jmp	.L1565
.L1592:
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1591:
	movq	400(%rbx), %rcx
	cltq
	subl	$1, %r13d
	movl	%r13d, (%rcx,%rax,4)
	movl	$1, %eax
	addl	$1, 384(%rbx)
	jmp	.L1524
.L1580:
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
.L1579:
	movq	400(%rbx), %rcx
	movl	%r13d, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
	jmp	.L1581
.L1624:
	movl	384(%rbx), %eax
	movq	8(%rbx), %rdx
.L1623:
	movq	400(%rbx), %rcx
	cltq
	subl	$1, %r13d
	movl	%r13d, (%rcx,%rax,4)
	movl	$1, %eax
	addl	$1, 384(%rbx)
	jmp	.L1524
.L1612:
	movslq	384(%rbx), %rax
	movq	8(%rbx), %rdx
.L1611:
	movq	400(%rbx), %rcx
	movl	%r13d, (%rcx,%rax,4)
	movl	384(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 384(%rbx)
	jmp	.L1613
.L1721:
	movl	$34, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1896:
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1892:
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1706:
	xorl	%edx, %edx
	movl	$23, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1802:
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	jmp	.L1695
.L1783:
	xorl	%esi, %esi
	jmp	.L1529
.L1814:
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	jmp	.L1731
.L1816:
	xorl	%esi, %esi
	jmp	.L1732
.L1794:
	xorl	%esi, %esi
	jmp	.L1673
.L1811:
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	jmp	.L1729
.L1809:
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	jmp	.L1728
.L1813:
	xorl	%esi, %esi
	jmp	.L1730
.L1793:
	xorl	%esi, %esi
	jmp	.L1672
.L1801:
	xorl	%esi, %esi
	jmp	.L1690
.L1900:
	movl	%esi, %edx
	addl	$1, %edx
	jns	.L1804
	movl	%edx, %esi
	movl	$1, %r12d
	jmp	.L1698
.L1905:
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile13scanNamedCharEv.part.0
	movl	%eax, %r12d
	movq	8(%rbx), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L1772
	movl	512(%rbx), %eax
	cmpl	$-1, %eax
	je	.L1820
	cmpl	%r12d, %eax
	jle	.L1772
.L1820:
	movl	$66320, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	jmp	.L1772
.L1743:
	andl	%esi, %ecx
.L1742:
	movl	%ecx, 296(%rbx)
	movl	$1, %eax
	jmp	.L1524
.L1901:
	movq	%r12, %rdi
	call	_ZN6icu_676UStack4popiEv@PLT
	movq	8(%rbx), %rdx
	movl	$131075, %esi
	movq	%r12, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	movl	$131081, %esi
	movq	%r12, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1703:
	movl	$12, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1895:
	movl	$21, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1904:
	movl	$66311, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1891:
	movl	$66311, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1887:
	xorl	%edx, %edx
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1667:
	movl	%r12d, %edx
	movl	$15, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1903:
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1902:
	movl	$66310, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	jmp	.L1526
.L1908:
	movl	$66310, %esi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	jmp	.L1658
.L1659:
	movl	$66310, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1799:
	xorl	%r12d, %r12d
	jmp	.L1682
.L1803:
	xorl	%esi, %esi
	jmp	.L1695
.L1812:
	xorl	%esi, %esi
	jmp	.L1729
.L1815:
	xorl	%esi, %esi
	jmp	.L1731
.L1810:
	xorl	%esi, %esi
	jmp	.L1728
.L1898:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile11fixLiteralsEa
	movl	%r12d, %edx
	testb	$2, 292(%rbx)
	je	.L1726
	movl	$41, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	jmp	.L1724
.L1670:
	xorl	%esi, %esi
	jmp	.L1669
.L1894:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8insertOpEi
	movq	8(%rbx), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L1686
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile17allocateStackDataEi.part.0
	movq	8(%rbx), %rdx
	movl	(%rdx), %edx
	testl	%edx, %edx
	jg	.L1686
	cmpl	$16777215, %eax
	ja	.L1671
	orl	$587202560, %eax
.L1685:
	movslq	%eax, %rsi
	movq	16(%rbx), %rax
	movl	%r13d, %edx
	movq	32(%rax), %rdi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movq	8(%rbx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1687
	leal	2(%r14), %r12d
	cmpl	$16777215, %r12d
	ja	.L1671
	movq	16(%rbx), %rax
	orl	$301989888, %r12d
	movq	32(%rax), %rdi
	movl	8(%rdi), %eax
	leal	1(%rax), %esi
	jmp	.L1688
.L1559:
	movq	16(%rbx), %rax
	movq	8(%rbx), %rcx
	movq	520(%rbx), %rsi
	movq	136(%rax), %rdx
	movq	192(%rax), %rdi
	movl	8(%rdx), %edx
	call	uhash_puti_67@PLT
	movq	$0, 520(%rbx)
	testl	%eax, %eax
	jle	.L1909
	movq	8(%rbx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1779
	movl	$66325, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1893:
	movq	24(%rdi), %rcx
	movslq	%edx, %rax
	movq	(%rcx,%rax,8), %rsi
	movl	%esi, %ecx
	shrl	$24, %ecx
	cmpl	$11, %ecx
	jne	.L1675
	movq	8(%rbx), %rcx
	xorl	%eax, %eax
	movl	(%rcx), %r12d
	testl	%r12d, %r12d
	jg	.L1676
	movl	%esi, %eax
	andl	$16777215, %eax
	orl	$838860800, %eax
.L1676:
	movslq	%eax, %rsi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movq	8(%rbx), %rax
	xorl	%edx, %edx
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jg	.L1677
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile17allocateStackDataEi.part.0
	movl	%eax, %edx
.L1677:
	movl	$51, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1675:
	cmpl	$27, %ecx
	ja	.L1674
	movl	$136318976, %eax
	btq	%rcx, %rax
	jnc	.L1674
	movq	8(%rbx), %rax
	movl	$872415232, %edx
	movl	(%rax), %r9d
	movl	$0, %eax
	testl	%r9d, %r9d
	cmovle	%edx, %eax
	movl	%eax, %edx
	orl	$1, %edx
	cmpl	$21, %ecx
	cmove	%edx, %eax
	movl	%eax, %edx
	orl	$2, %edx
	testb	$1, 292(%rbx)
	cmovne	%edx, %eax
	movl	%r8d, %edx
	movslq	%eax, %rsi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movq	8(%rbx), %rax
	xorl	%edx, %edx
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L1681
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile17allocateStackDataEi.part.0
	movl	%eax, %edx
.L1681:
	movl	$51, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1800:
	xorl	%esi, %esi
	jmp	.L1689
.L1899:
	movl	$66307, %esi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	jmp	.L1770
.L1907:
	movl	$66307, %esi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movl	72(%rbx), %esi
	jmp	.L1701
.L1906:
	movq	24(%rcx), %rcx
	movslq	%edx, %rax
	movq	(%rcx,%rax,8), %rcx
	movl	%ecx, %eax
	shrl	$24, %eax
	cmpl	$11, %eax
	jne	.L1661
	movl	%ecx, %edx
	movl	$50, %esi
	movq	%rbx, %rdi
	andl	$16777215, %edx
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rax
	xorl	%edx, %edx
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L1662
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile17allocateStackDataEi.part.0
	movl	%eax, %edx
.L1662:
	movl	$51, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1661:
	cmpl	$27, %eax
	ja	.L1660
	movl	$136318976, %ecx
	btq	%rax, %rcx
	jnc	.L1660
	movq	8(%rbx), %rdx
	movq	%rbx, %rdi
	movl	(%rdx), %esi
	movl	$872415232, %edx
	testl	%esi, %esi
	movl	$0, %esi
	cmovle	%edx, %esi
	movl	%esi, %edx
	orl	$1, %edx
	cmpl	$21, %eax
	cmove	%edx, %esi
	movl	%esi, %eax
	orl	$2, %eax
	testb	$1, 292(%rbx)
	cmovne	%eax, %esi
	call	_ZN6icu_6712RegexCompile8appendOpEi
	movq	8(%rbx), %rax
	xorl	%edx, %edx
	movl	(%rax), %edi
	testl	%edi, %edi
	jg	.L1666
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile17allocateStackDataEi.part.0
	movl	%eax, %edx
.L1666:
	movl	$51, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1804:
	movl	%esi, %eax
	movl	$1, %r12d
	movl	%edx, %esi
	jmp	.L1697
.L1888:
	xorl	%edx, %edx
	movl	$42, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1897:
	xorl	%edx, %edx
	movl	$43, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1686:
	xorl	%eax, %eax
	jmp	.L1685
.L1733:
	movb	$0, 300(%rdi)
	movl	296(%rdi), %ecx
	movq	8(%rdi), %rdx
	jmp	.L1742
.L1726:
	movl	$34, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	jmp	.L1724
.L1889:
	xorl	%edx, %edx
	movl	$54, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1711:
	xorl	%edx, %edx
	movl	$55, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1707:
	xorl	%edx, %edx
	movl	$30, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile8appendOpEii
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1687:
	movq	16(%rbx), %rax
	xorl	%r12d, %r12d
	xorl	%esi, %esi
	movq	32(%rax), %rdi
	jmp	.L1689
.L1909:
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1886:
	call	__stack_chk_fail@PLT
.L1722:
	movq	$0, 520(%rbx)
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
.L1537:
	movq	$0, 520(%rbx)
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movq	8(%rbx), %rdx
	movl	$1, %eax
	jmp	.L1524
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexCompile14doParseActionsEi.cold, @function
_ZN6icu_6712RegexCompile14doParseActionsEi.cold:
.LFSB3174:
.L1422:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	call	abort@PLT
.L1671:
	call	abort@PLT
	.cfi_endproc
.LFE3174:
	.text
	.size	_ZN6icu_6712RegexCompile14doParseActionsEi, .-_ZN6icu_6712RegexCompile14doParseActionsEi
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexCompile14doParseActionsEi.cold, .-_ZN6icu_6712RegexCompile14doParseActionsEi.cold
.LCOLDE43:
	.text
.LHOTE43:
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexCompile7compileEP5UTextR11UParseErrorR10UErrorCode.part.0, @function
_ZN6icu_6712RegexCompile7compileEP5UTextR11UParseErrorR10UErrorCode.part.0:
.LFB4206:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rsi, %rdi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	utext_nativeLength_67@PLT
	movq	%rax, 368(%r15)
	testb	$16, 292(%r15)
	je	.L1911
	movb	$1, 40(%r15)
.L1911:
	leaq	72(%r15), %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE
	movq	8(%r15), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L1927
	movl	$1, %r14d
	leaq	_ZN6icu_67L20gRuleParseStateTableE(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1913:
	movzwl	%r14w, %eax
	leaq	(%rbx,%rax,8), %r14
	jmp	.L1919
	.p2align 4,,10
	.p2align 3
.L1976:
	cmpb	$0, 76(%r15)
	jne	.L1915
	cmpl	72(%r15), %eax
	je	.L1916
	.p2align 4,,10
	.p2align 3
.L1915:
	addq	$8, %r14
.L1919:
	movzbl	4(%r14), %eax
	cmpb	$126, %al
	jbe	.L1976
	cmpb	$-1, %al
	je	.L1916
	cmpb	$-2, %al
	jne	.L1917
	cmpb	$0, 76(%r15)
	je	.L1915
	.p2align 4,,10
	.p2align 3
.L1916:
	movl	(%r14), %esi
	movq	%r15, %rdi
	call	_ZN6icu_6712RegexCompile14doParseActionsEi
	testb	%al, %al
	je	.L1920
.L1982:
	movzbl	6(%r14), %edx
	testb	%dl, %dl
	je	.L1921
	movl	288(%r15), %eax
	addl	$1, %eax
	movl	%eax, 288(%r15)
	cmpl	$99, %eax
	jg	.L1977
.L1922:
	cltq
	movw	%dx, 88(%r15,%rax,2)
.L1921:
	cmpb	$0, 7(%r14)
	jne	.L1978
.L1923:
	movzbl	5(%r14), %eax
	movzbl	%al, %r14d
	cmpb	$-1, %al
	je	.L1979
.L1925:
	movq	8(%r15), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L1913
.L1927:
	leaq	432(%r15), %rbx
	jmp	.L1930
	.p2align 4,,10
	.p2align 3
.L1981:
	movq	%rbx, %rdi
	call	_ZN6icu_676UStack3popEv@PLT
	testq	%rax, %rax
	jne	.L1980
.L1930:
	movl	440(%r15), %eax
	testl	%eax, %eax
	jne	.L1981
.L1910:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1917:
	.cfi_restore_state
	cmpb	$-3, %al
	jne	.L1918
	cmpl	$-1, 72(%r15)
	jne	.L1915
	movl	(%r14), %esi
	movq	%r15, %rdi
	call	_ZN6icu_6712RegexCompile14doParseActionsEi
	testb	%al, %al
	jne	.L1982
.L1920:
	movq	8(%r15), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L1927
	movq	16(%r15), %rdx
	movl	128(%rdx), %eax
	testl	%eax, %eax
	js	.L1983
	addl	$2, %eax
	movl	%eax, 128(%rdx)
	cmpl	$16777199, %eax
	jg	.L1984
.L1933:
	movq	%r15, %rdi
	call	_ZN6icu_6712RegexCompile9stripNOPsEv
	movq	16(%r15), %rbx
	movl	$3, %esi
	movq	%r15, %rdi
	movq	32(%rbx), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	call	_ZN6icu_6712RegexCompile14minMatchLengthEii
	movq	%r15, %rdi
	movl	%eax, 124(%rbx)
	call	_ZN6icu_6712RegexCompile14matchStartTypeEv
	movq	16(%r15), %rax
	movq	104(%rax), %rax
	movslq	8(%rax), %rbx
	movabsq	$288230376151711743, %rax
	movq	%rbx, %rdi
	movq	%rbx, %r12
	salq	$5, %rdi
	cmpq	%rax, %rbx
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1935
	subq	$1, %rbx
	movq	%rbx, %rdx
	js	.L1936
	.p2align 4,,10
	.p2align 3
.L1937:
	pxor	%xmm0, %xmm0
	subq	$1, %rdx
	addq	$32, %rax
	movups	%xmm0, -32(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	$-1, %rdx
	jne	.L1937
.L1936:
	movq	16(%r15), %rax
	movq	%rcx, 112(%rax)
	testl	%r12d, %r12d
	jle	.L1910
	leal	-1(%r12), %ebx
	movq	$0, -56(%rbp)
	movl	$1, %r13d
	movq	%rbx, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L1943:
	movq	104(%rax), %rdi
	movl	-56(%rbp), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1941
	movq	16(%r15), %rax
	movq	-56(%rbp), %rbx
	xorl	%r14d, %r14d
	salq	$5, %rbx
	addq	112(%rax), %rbx
	.p2align 4,,10
	.p2align 3
.L1942:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L1940
	movl	%r14d, %eax
	movl	%r14d, %ecx
	movl	%r13d, %edx
	sarl	$3, %eax
	andl	$7, %ecx
	cltq
	sall	%cl, %edx
	orb	%dl, (%rbx,%rax)
.L1940:
	addl	$1, %r14d
	cmpl	$256, %r14d
	jne	.L1942
.L1941:
	movq	-56(%rbp), %rax
	leaq	1(%rax), %rdx
	cmpq	%rax, -64(%rbp)
	je	.L1910
	movq	%rdx, -56(%rbp)
	movq	16(%r15), %rax
	jmp	.L1943
.L1918:
	leal	-128(%rax), %edx
	cmpb	$111, %dl
	ja	.L1915
	cmpb	$0, 76(%r15)
	jne	.L1915
	movl	72(%r15), %esi
	cmpl	$-1, %esi
	je	.L1915
	addl	$-128, %eax
	cltq
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rdx
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	leaq	3024(%rax,%rdx,8), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L1915
	jmp	.L1916
	.p2align 4,,10
	.p2align 3
.L1979:
	movslq	288(%r15), %rdx
	movq	%rdx, %rax
	movzwl	88(%r15,%rdx,2), %r14d
	subl	$1, %eax
	js	.L1985
	movl	%eax, 288(%r15)
	jmp	.L1925
	.p2align 4,,10
	.p2align 3
.L1978:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6712RegexCompile8nextCharERNS0_16RegexPatternCharE
	jmp	.L1923
.L1977:
	movl	$66304, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	movl	288(%r15), %eax
	subl	$1, %eax
	movl	%eax, 288(%r15)
	movzbl	6(%r14), %edx
	jmp	.L1922
.L1985:
	movl	$66310, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	jmp	.L1925
.L1980:
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L1930
.L1984:
	movl	$66324, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	jmp	.L1933
.L1983:
	movl	$66304, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6712RegexCompile5errorE10UErrorCode
	jmp	.L1933
.L1935:
	movq	16(%r15), %rax
	movq	$0, 112(%rax)
	movq	8(%r15), %rax
	movl	$7, (%rax)
	movl	$7, 0(%r13)
	jmp	.L1910
	.cfi_endproc
.LFE4206:
	.size	_ZN6icu_6712RegexCompile7compileEP5UTextR11UParseErrorR10UErrorCode.part.0, .-_ZN6icu_6712RegexCompile7compileEP5UTextR11UParseErrorR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile7compileEP5UTextR11UParseErrorR10UErrorCode
	.type	_ZN6icu_6712RegexCompile7compileEP5UTextR11UParseErrorR10UErrorCode, @function
_ZN6icu_6712RegexCompile7compileEP5UTextR11UParseErrorR10UErrorCode:
.LFB3173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, 24(%rdi)
	xorl	%edx, %edx
	movq	%rcx, 8(%rdi)
	movl	$0, 288(%rdi)
	movw	%dx, 88(%rdi)
	movl	(%rcx), %edi
	testl	%edi, %edi
	jle	.L1991
.L1986:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1991:
	.cfi_restore_state
	movq	16(%r12), %rbx
	movq	%rcx, %r8
	movq	%rcx, %r13
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%rsi, %r14
	movq	8(%rbx), %rdi
	call	utext_clone_67@PLT
	movq	%rax, 8(%rbx)
	movq	8(%r12), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1986
	popq	%rbx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexCompile7compileEP5UTextR11UParseErrorR10UErrorCode.part.0
	.cfi_endproc
.LFE3173:
	.size	_ZN6icu_6712RegexCompile7compileEP5UTextR11UParseErrorR10UErrorCode, .-_ZN6icu_6712RegexCompile7compileEP5UTextR11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexCompile7compileERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6712RegexCompile7compileERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, @function
_ZN6icu_6712RegexCompile7compileERKNS_13UnicodeStringER11UParseErrorR10UErrorCode:
.LFB3172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$64, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1993
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L1993:
	movq	16(%r13), %rax
	movl	$18, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	leaq	-208(%rbp), %rdi
	leaq	-208(%rbp), %r15
	movq	%r12, 16(%rax)
	xorl	%eax, %eax
	rep stosq
	movq	%r15, %rdi
	movl	$878368812, -208(%rbp)
	movl	$144, -196(%rbp)
	call	utext_openConstUnicodeString_67@PLT
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jle	.L2001
.L1992:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2002
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2001:
	.cfi_restore_state
	movq	%rbx, 24(%r13)
	xorl	%eax, %eax
	movq	16(%r13), %rbx
	xorl	%edx, %edx
	movq	%r14, 8(%r13)
	movq	%r14, %r8
	movl	$1, %ecx
	movq	%r15, %rsi
	movl	$0, 288(%r13)
	movq	8(%rbx), %rdi
	movw	%ax, 88(%r13)
	call	utext_clone_67@PLT
	movq	%rax, 8(%rbx)
	movq	8(%r13), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jle	.L2003
.L1995:
	movq	%r15, %rdi
	call	utext_close_67@PLT
	jmp	.L1992
	.p2align 4,,10
	.p2align 3
.L2003:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexCompile7compileEP5UTextR11UParseErrorR10UErrorCode.part.0
	jmp	.L1995
.L2002:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3172:
	.size	_ZN6icu_6712RegexCompile7compileERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, .-_ZN6icu_6712RegexCompile7compileERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6712RegexCompileE
	.section	.rodata._ZTSN6icu_6712RegexCompileE,"aG",@progbits,_ZTSN6icu_6712RegexCompileE,comdat
	.align 16
	.type	_ZTSN6icu_6712RegexCompileE, @object
	.size	_ZTSN6icu_6712RegexCompileE, 24
_ZTSN6icu_6712RegexCompileE:
	.string	"N6icu_6712RegexCompileE"
	.weak	_ZTIN6icu_6712RegexCompileE
	.section	.data.rel.ro._ZTIN6icu_6712RegexCompileE,"awG",@progbits,_ZTIN6icu_6712RegexCompileE,comdat
	.align 8
	.type	_ZTIN6icu_6712RegexCompileE, @object
	.size	_ZTIN6icu_6712RegexCompileE, 24
_ZTIN6icu_6712RegexCompileE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712RegexCompileE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTVN6icu_6712RegexCompileE
	.section	.data.rel.ro.local._ZTVN6icu_6712RegexCompileE,"awG",@progbits,_ZTVN6icu_6712RegexCompileE,comdat
	.align 8
	.type	_ZTVN6icu_6712RegexCompileE, @object
	.size	_ZTVN6icu_6712RegexCompileE, 32
_ZTVN6icu_6712RegexCompileE:
	.quad	0
	.quad	_ZTIN6icu_6712RegexCompileE
	.quad	_ZN6icu_6712RegexCompileD1Ev
	.quad	_ZN6icu_6712RegexCompileD0Ev
	.section	.rodata
	.align 32
	.type	_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE13RECaseFixData, @object
	.size	_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE13RECaseFixData, 210
_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE13RECaseFixData:
	.value	7834
	.value	-1280
	.value	-1279
	.value	-1278
	.value	-1277
	.value	-1276
	.value	7830
	.value	304
	.value	496
	.value	223
	.value	7838
	.value	-1275
	.value	-1274
	.value	7831
	.value	7832
	.value	7833
	.value	329
	.value	8116
	.value	8132
	.value	8115
	.value	8118
	.value	8119
	.value	8124
	.value	8131
	.value	8134
	.value	8135
	.value	8140
	.value	912
	.value	8146
	.value	8147
	.value	8150
	.value	8151
	.value	8164
	.value	944
	.value	8016
	.value	8018
	.value	8020
	.value	8022
	.value	8162
	.value	8163
	.value	8166
	.value	8167
	.value	8179
	.value	8182
	.value	8183
	.value	8188
	.value	8180
	.value	1415
	.value	-1261
	.value	-1260
	.value	-1259
	.value	-1257
	.value	-1258
	.value	8064
	.value	8072
	.value	8065
	.value	8073
	.value	8066
	.value	8074
	.value	8067
	.value	8075
	.value	8068
	.value	8076
	.value	8069
	.value	8077
	.value	8070
	.value	8078
	.value	8071
	.value	8079
	.value	8080
	.value	8088
	.value	8081
	.value	8089
	.value	8082
	.value	8090
	.value	8083
	.value	8091
	.value	8084
	.value	8092
	.value	8085
	.value	8093
	.value	8086
	.value	8094
	.value	8087
	.value	8095
	.value	8096
	.value	8104
	.value	8097
	.value	8105
	.value	8098
	.value	8106
	.value	8099
	.value	8107
	.value	8100
	.value	8108
	.value	8101
	.value	8109
	.value	8102
	.value	8110
	.value	8103
	.value	8111
	.value	8114
	.value	8130
	.value	8178
	.value	0
	.align 32
	.type	_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE15RECaseFixCounts, @object
	.size	_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE15RECaseFixCounts, 100
_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE15RECaseFixCounts:
	.value	1
	.value	5
	.value	1
	.value	1
	.value	1
	.value	4
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	4
	.value	4
	.value	5
	.value	1
	.value	9
	.value	4
	.value	1
	.value	1
	.value	4
	.value	1
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	1
	.value	1
	.value	1
	.value	0
	.align 32
	.type	_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE22RECaseFixStringOffsets, @object
	.size	_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE22RECaseFixStringOffsets, 100
_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE22RECaseFixStringOffsets:
	.value	0
	.value	1
	.value	6
	.value	7
	.value	8
	.value	9
	.value	13
	.value	14
	.value	15
	.value	16
	.value	17
	.value	18
	.value	19
	.value	23
	.value	27
	.value	32
	.value	33
	.value	42
	.value	46
	.value	47
	.value	48
	.value	52
	.value	53
	.value	55
	.value	57
	.value	59
	.value	61
	.value	63
	.value	65
	.value	67
	.value	69
	.value	71
	.value	73
	.value	75
	.value	77
	.value	79
	.value	81
	.value	83
	.value	85
	.value	87
	.value	89
	.value	91
	.value	93
	.value	95
	.value	97
	.value	99
	.value	101
	.value	102
	.value	103
	.value	0
	.align 32
	.type	_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE19RECaseFixCodePoints, @object
	.size	_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE19RECaseFixCodePoints, 200
_ZZN6icu_6712RegexCompile27findCaseInsensitiveStartersEiPNS_10UnicodeSetEE19RECaseFixCodePoints:
	.long	97
	.long	102
	.long	104
	.long	105
	.long	106
	.long	115
	.long	116
	.long	119
	.long	121
	.long	700
	.long	940
	.long	942
	.long	945
	.long	951
	.long	953
	.long	961
	.long	965
	.long	969
	.long	974
	.long	1381
	.long	1396
	.long	1406
	.long	7936
	.long	7937
	.long	7938
	.long	7939
	.long	7940
	.long	7941
	.long	7942
	.long	7943
	.long	7968
	.long	7969
	.long	7970
	.long	7971
	.long	7972
	.long	7973
	.long	7974
	.long	7975
	.long	8032
	.long	8033
	.long	8034
	.long	8035
	.long	8036
	.long	8037
	.long	8038
	.long	8039
	.long	8048
	.long	8052
	.long	8060
	.long	1114112
	.align 32
	.type	_ZN6icu_67L20gRuleParseStateTableE, @object
	.size	_ZN6icu_67L20gRuleParseStateTableE, 1656
_ZN6icu_67L20gRuleParseStateTableE:
	.long	10
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.long	6
	.byte	-1
	.byte	2
	.byte	0
	.byte	0
	.long	96
	.byte	-2
	.byte	14
	.byte	0
	.byte	1
	.long	96
	.byte	-126
	.byte	14
	.byte	0
	.byte	1
	.long	16
	.byte	91
	.byte	123
	.byte	-51
	.byte	1
	.long	10
	.byte	40
	.byte	27
	.byte	0
	.byte	1
	.long	67
	.byte	46
	.byte	14
	.byte	0
	.byte	1
	.long	98
	.byte	94
	.byte	14
	.byte	0
	.byte	1
	.long	78
	.byte	36
	.byte	14
	.byte	0
	.byte	1
	.long	10
	.byte	92
	.byte	89
	.byte	0
	.byte	1
	.long	93
	.byte	124
	.byte	2
	.byte	0
	.byte	1
	.long	66
	.byte	41
	.byte	-1
	.byte	0
	.byte	1
	.long	61
	.byte	-3
	.byte	2
	.byte	0
	.byte	0
	.long	28
	.byte	-1
	.byte	-50
	.byte	0
	.byte	0
	.long	10
	.byte	42
	.byte	68
	.byte	0
	.byte	1
	.long	10
	.byte	43
	.byte	71
	.byte	0
	.byte	1
	.long	10
	.byte	63
	.byte	74
	.byte	0
	.byte	1
	.long	25
	.byte	123
	.byte	77
	.byte	0
	.byte	1
	.long	10
	.byte	40
	.byte	23
	.byte	0
	.byte	1
	.long	10
	.byte	-1
	.byte	20
	.byte	0
	.byte	0
	.long	93
	.byte	124
	.byte	2
	.byte	0
	.byte	1
	.long	66
	.byte	41
	.byte	-1
	.byte	0
	.byte	1
	.long	10
	.byte	-1
	.byte	2
	.byte	0
	.byte	0
	.long	97
	.byte	63
	.byte	25
	.byte	0
	.byte	1
	.long	10
	.byte	-1
	.byte	27
	.byte	0
	.byte	0
	.long	10
	.byte	35
	.byte	50
	.byte	14
	.byte	1
	.long	10
	.byte	-1
	.byte	29
	.byte	0
	.byte	0
	.long	97
	.byte	63
	.byte	29
	.byte	0
	.byte	1
	.long	68
	.byte	-1
	.byte	2
	.byte	14
	.byte	0
	.long	31
	.byte	58
	.byte	2
	.byte	14
	.byte	1
	.long	70
	.byte	62
	.byte	2
	.byte	14
	.byte	1
	.long	101
	.byte	61
	.byte	2
	.byte	20
	.byte	1
	.long	4
	.byte	33
	.byte	2
	.byte	20
	.byte	1
	.long	10
	.byte	60
	.byte	46
	.byte	0
	.byte	1
	.long	10
	.byte	35
	.byte	50
	.byte	2
	.byte	1
	.long	59
	.byte	105
	.byte	53
	.byte	0
	.byte	0
	.long	59
	.byte	100
	.byte	53
	.byte	0
	.byte	0
	.long	59
	.byte	109
	.byte	53
	.byte	0
	.byte	0
	.long	59
	.byte	115
	.byte	53
	.byte	0
	.byte	0
	.long	59
	.byte	117
	.byte	53
	.byte	0
	.byte	0
	.long	59
	.byte	119
	.byte	53
	.byte	0
	.byte	0
	.long	59
	.byte	120
	.byte	53
	.byte	0
	.byte	0
	.long	59
	.byte	45
	.byte	53
	.byte	0
	.byte	0
	.long	35
	.byte	40
	.byte	-50
	.byte	0
	.byte	1
	.long	19
	.byte	123
	.byte	-50
	.byte	0
	.byte	1
	.long	37
	.byte	-1
	.byte	-50
	.byte	0
	.byte	0
	.long	91
	.byte	61
	.byte	2
	.byte	20
	.byte	1
	.long	56
	.byte	33
	.byte	2
	.byte	20
	.byte	1
	.long	79
	.byte	-127
	.byte	64
	.byte	0
	.byte	0
	.long	37
	.byte	-1
	.byte	-50
	.byte	0
	.byte	0
	.long	10
	.byte	41
	.byte	-1
	.byte	0
	.byte	1
	.long	103
	.byte	-3
	.byte	-50
	.byte	0
	.byte	0
	.long	10
	.byte	-1
	.byte	50
	.byte	0
	.byte	1
	.long	89
	.byte	105
	.byte	53
	.byte	0
	.byte	1
	.long	89
	.byte	100
	.byte	53
	.byte	0
	.byte	1
	.long	89
	.byte	109
	.byte	53
	.byte	0
	.byte	1
	.long	89
	.byte	115
	.byte	53
	.byte	0
	.byte	1
	.long	89
	.byte	117
	.byte	53
	.byte	0
	.byte	1
	.long	89
	.byte	119
	.byte	53
	.byte	0
	.byte	1
	.long	89
	.byte	120
	.byte	53
	.byte	0
	.byte	1
	.long	89
	.byte	45
	.byte	53
	.byte	0
	.byte	1
	.long	53
	.byte	41
	.byte	2
	.byte	0
	.byte	1
	.long	50
	.byte	58
	.byte	2
	.byte	14
	.byte	1
	.long	71
	.byte	-1
	.byte	-50
	.byte	0
	.byte	0
	.long	13
	.byte	-127
	.byte	64
	.byte	0
	.byte	1
	.long	13
	.byte	-128
	.byte	64
	.byte	0
	.byte	1
	.long	68
	.byte	62
	.byte	2
	.byte	14
	.byte	1
	.long	58
	.byte	-1
	.byte	-50
	.byte	0
	.byte	0
	.long	9
	.byte	63
	.byte	20
	.byte	0
	.byte	1
	.long	38
	.byte	43
	.byte	20
	.byte	0
	.byte	1
	.long	88
	.byte	-1
	.byte	20
	.byte	0
	.byte	0
	.long	63
	.byte	63
	.byte	20
	.byte	0
	.byte	1
	.long	18
	.byte	43
	.byte	20
	.byte	0
	.byte	1
	.long	51
	.byte	-1
	.byte	20
	.byte	0
	.byte	0
	.long	100
	.byte	63
	.byte	20
	.byte	0
	.byte	1
	.long	92
	.byte	43
	.byte	20
	.byte	0
	.byte	1
	.long	77
	.byte	-1
	.byte	20
	.byte	0
	.byte	0
	.long	10
	.byte	-128
	.byte	79
	.byte	0
	.byte	0
	.long	23
	.byte	-1
	.byte	-50
	.byte	0
	.byte	0
	.long	86
	.byte	-128
	.byte	79
	.byte	0
	.byte	1
	.long	10
	.byte	44
	.byte	83
	.byte	0
	.byte	1
	.long	99
	.byte	125
	.byte	86
	.byte	0
	.byte	1
	.long	23
	.byte	-1
	.byte	-50
	.byte	0
	.byte	0
	.long	42
	.byte	-128
	.byte	83
	.byte	0
	.byte	1
	.long	10
	.byte	125
	.byte	86
	.byte	0
	.byte	1
	.long	23
	.byte	-1
	.byte	-50
	.byte	0
	.byte	0
	.long	80
	.byte	63
	.byte	20
	.byte	0
	.byte	1
	.long	27
	.byte	43
	.byte	20
	.byte	0
	.byte	1
	.long	57
	.byte	-1
	.byte	20
	.byte	0
	.byte	0
	.long	90
	.byte	65
	.byte	2
	.byte	0
	.byte	1
	.long	47
	.byte	66
	.byte	2
	.byte	0
	.byte	1
	.long	84
	.byte	98
	.byte	2
	.byte	0
	.byte	1
	.long	60
	.byte	100
	.byte	14
	.byte	0
	.byte	1
	.long	8
	.byte	68
	.byte	14
	.byte	0
	.byte	1
	.long	14
	.byte	71
	.byte	2
	.byte	0
	.byte	1
	.long	1
	.byte	104
	.byte	14
	.byte	0
	.byte	1
	.long	2
	.byte	72
	.byte	14
	.byte	0
	.byte	1
	.long	10
	.byte	107
	.byte	115
	.byte	0
	.byte	1
	.long	62
	.byte	78
	.byte	14
	.byte	0
	.byte	0
	.long	74
	.byte	112
	.byte	14
	.byte	0
	.byte	0
	.long	74
	.byte	80
	.byte	14
	.byte	0
	.byte	0
	.long	15
	.byte	82
	.byte	14
	.byte	0
	.byte	1
	.long	69
	.byte	81
	.byte	2
	.byte	0
	.byte	1
	.long	7
	.byte	83
	.byte	14
	.byte	0
	.byte	1
	.long	95
	.byte	115
	.byte	14
	.byte	0
	.byte	1
	.long	43
	.byte	118
	.byte	14
	.byte	0
	.byte	1
	.long	52
	.byte	86
	.byte	14
	.byte	0
	.byte	1
	.long	29
	.byte	87
	.byte	14
	.byte	0
	.byte	1
	.long	94
	.byte	119
	.byte	14
	.byte	0
	.byte	1
	.long	11
	.byte	88
	.byte	14
	.byte	0
	.byte	1
	.long	20
	.byte	90
	.byte	2
	.byte	0
	.byte	1
	.long	54
	.byte	122
	.byte	2
	.byte	0
	.byte	1
	.long	76
	.byte	-128
	.byte	14
	.byte	0
	.byte	1
	.long	36
	.byte	-3
	.byte	-50
	.byte	0
	.byte	0
	.long	40
	.byte	-1
	.byte	14
	.byte	0
	.byte	1
	.long	75
	.byte	60
	.byte	117
	.byte	0
	.byte	1
	.long	58
	.byte	-1
	.byte	-50
	.byte	0
	.byte	0
	.long	30
	.byte	-127
	.byte	119
	.byte	0
	.byte	1
	.long	58
	.byte	-1
	.byte	-50
	.byte	0
	.byte	0
	.long	30
	.byte	-127
	.byte	119
	.byte	0
	.byte	1
	.long	30
	.byte	-128
	.byte	119
	.byte	0
	.byte	1
	.long	5
	.byte	62
	.byte	14
	.byte	0
	.byte	1
	.long	58
	.byte	-1
	.byte	-50
	.byte	0
	.byte	0
	.long	24
	.byte	94
	.byte	126
	.byte	0
	.byte	1
	.long	82
	.byte	58
	.byte	-128
	.byte	0
	.byte	0
	.long	10
	.byte	-1
	.byte	126
	.byte	0
	.byte	0
	.long	12
	.byte	93
	.byte	-115
	.byte	0
	.byte	1
	.long	10
	.byte	-1
	.byte	-125
	.byte	0
	.byte	0
	.long	48
	.byte	93
	.byte	-1
	.byte	0
	.byte	1
	.long	10
	.byte	58
	.byte	-125
	.byte	0
	.byte	0
	.long	28
	.byte	-1
	.byte	-50
	.byte	0
	.byte	0
	.long	48
	.byte	93
	.byte	-1
	.byte	0
	.byte	1
	.long	85
	.byte	91
	.byte	123
	.byte	-108
	.byte	1
	.long	10
	.byte	92
	.byte	-65
	.byte	0
	.byte	1
	.long	10
	.byte	45
	.byte	-119
	.byte	0
	.byte	1
	.long	10
	.byte	38
	.byte	-117
	.byte	0
	.byte	1
	.long	12
	.byte	-1
	.byte	-115
	.byte	0
	.byte	1
	.long	28
	.byte	45
	.byte	-50
	.byte	0
	.byte	0
	.long	39
	.byte	-1
	.byte	-115
	.byte	0
	.byte	0
	.long	28
	.byte	38
	.byte	-50
	.byte	0
	.byte	0
	.long	21
	.byte	-1
	.byte	-115
	.byte	0
	.byte	0
	.long	48
	.byte	93
	.byte	-1
	.byte	0
	.byte	1
	.long	85
	.byte	91
	.byte	123
	.byte	-108
	.byte	1
	.long	10
	.byte	45
	.byte	-78
	.byte	0
	.byte	1
	.long	10
	.byte	38
	.byte	-87
	.byte	0
	.byte	1
	.long	10
	.byte	92
	.byte	-65
	.byte	0
	.byte	1
	.long	45
	.byte	-3
	.byte	-50
	.byte	0
	.byte	0
	.long	12
	.byte	-1
	.byte	-115
	.byte	0
	.byte	1
	.long	48
	.byte	93
	.byte	-1
	.byte	0
	.byte	1
	.long	85
	.byte	91
	.byte	123
	.byte	-108
	.byte	1
	.long	10
	.byte	45
	.byte	-85
	.byte	0
	.byte	1
	.long	10
	.byte	38
	.byte	-90
	.byte	0
	.byte	1
	.long	10
	.byte	92
	.byte	-65
	.byte	0
	.byte	1
	.long	45
	.byte	-3
	.byte	-50
	.byte	0
	.byte	0
	.long	12
	.byte	-1
	.byte	-115
	.byte	0
	.byte	1
	.long	48
	.byte	93
	.byte	-1
	.byte	0
	.byte	1
	.long	85
	.byte	91
	.byte	123
	.byte	-108
	.byte	1
	.long	10
	.byte	45
	.byte	-82
	.byte	0
	.byte	1
	.long	10
	.byte	38
	.byte	-80
	.byte	0
	.byte	1
	.long	10
	.byte	92
	.byte	-65
	.byte	0
	.byte	1
	.long	45
	.byte	-3
	.byte	-50
	.byte	0
	.byte	0
	.long	12
	.byte	-1
	.byte	-115
	.byte	0
	.byte	1
	.long	85
	.byte	91
	.byte	123
	.byte	-108
	.byte	1
	.long	81
	.byte	93
	.byte	-50
	.byte	0
	.byte	0
	.long	10
	.byte	92
	.byte	-65
	.byte	0
	.byte	1
	.long	12
	.byte	-1
	.byte	-115
	.byte	0
	.byte	1
	.long	83
	.byte	91
	.byte	123
	.byte	-108
	.byte	1
	.long	26
	.byte	38
	.byte	-94
	.byte	0
	.byte	1
	.long	21
	.byte	-1
	.byte	-115
	.byte	0
	.byte	0
	.long	26
	.byte	38
	.byte	-94
	.byte	0
	.byte	1
	.long	21
	.byte	-1
	.byte	-115
	.byte	0
	.byte	0
	.long	22
	.byte	91
	.byte	123
	.byte	-108
	.byte	1
	.long	64
	.byte	45
	.byte	-94
	.byte	0
	.byte	1
	.long	39
	.byte	-1
	.byte	-115
	.byte	0
	.byte	0
	.long	64
	.byte	45
	.byte	-94
	.byte	0
	.byte	1
	.long	39
	.byte	-1
	.byte	-115
	.byte	0
	.byte	0
	.long	26
	.byte	38
	.byte	-94
	.byte	0
	.byte	1
	.long	21
	.byte	-1
	.byte	-115
	.byte	0
	.byte	0
	.long	64
	.byte	45
	.byte	-94
	.byte	0
	.byte	1
	.long	39
	.byte	91
	.byte	-115
	.byte	0
	.byte	0
	.long	39
	.byte	93
	.byte	-115
	.byte	0
	.byte	0
	.long	10
	.byte	92
	.byte	-73
	.byte	0
	.byte	1
	.long	49
	.byte	-1
	.byte	-101
	.byte	0
	.byte	1
	.long	81
	.byte	115
	.byte	-50
	.byte	0
	.byte	0
	.long	81
	.byte	83
	.byte	-50
	.byte	0
	.byte	0
	.long	81
	.byte	119
	.byte	-50
	.byte	0
	.byte	0
	.long	81
	.byte	87
	.byte	-50
	.byte	0
	.byte	0
	.long	81
	.byte	100
	.byte	-50
	.byte	0
	.byte	0
	.long	81
	.byte	68
	.byte	-50
	.byte	0
	.byte	0
	.long	55
	.byte	78
	.byte	-101
	.byte	0
	.byte	0
	.long	49
	.byte	-1
	.byte	-101
	.byte	0
	.byte	1
	.long	46
	.byte	112
	.byte	-108
	.byte	0
	.byte	0
	.long	46
	.byte	80
	.byte	-108
	.byte	0
	.byte	0
	.long	33
	.byte	78
	.byte	-115
	.byte	0
	.byte	0
	.long	104
	.byte	115
	.byte	-101
	.byte	0
	.byte	1
	.long	44
	.byte	83
	.byte	-101
	.byte	0
	.byte	1
	.long	41
	.byte	119
	.byte	-101
	.byte	0
	.byte	1
	.long	102
	.byte	87
	.byte	-101
	.byte	0
	.byte	1
	.long	72
	.byte	100
	.byte	-101
	.byte	0
	.byte	1
	.long	0
	.byte	68
	.byte	-101
	.byte	0
	.byte	1
	.long	87
	.byte	104
	.byte	-101
	.byte	0
	.byte	1
	.long	65
	.byte	72
	.byte	-101
	.byte	0
	.byte	1
	.long	17
	.byte	118
	.byte	-101
	.byte	0
	.byte	1
	.long	34
	.byte	86
	.byte	-101
	.byte	0
	.byte	1
	.long	3
	.byte	-1
	.byte	-115
	.byte	0
	.byte	1
	.long	73
	.byte	-1
	.byte	14
	.byte	0
	.byte	0
	.long	32
	.byte	-1
	.byte	-50
	.byte	0
	.byte	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	1
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
