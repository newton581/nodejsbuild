	.file	"dayperiodrules.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722DayPeriodRulesDataSinkD2Ev
	.type	_ZN6icu_6722DayPeriodRulesDataSinkD2Ev, @function
_ZN6icu_6722DayPeriodRulesDataSinkD2Ev:
.LFB3157:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6722DayPeriodRulesDataSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE3157:
	.size	_ZN6icu_6722DayPeriodRulesDataSinkD2Ev, .-_ZN6icu_6722DayPeriodRulesDataSinkD2Ev
	.globl	_ZN6icu_6722DayPeriodRulesDataSinkD1Ev
	.set	_ZN6icu_6722DayPeriodRulesDataSinkD1Ev,_ZN6icu_6722DayPeriodRulesDataSinkD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723DayPeriodRulesCountSinkD2Ev
	.type	_ZN6icu_6723DayPeriodRulesCountSinkD2Ev, @function
_ZN6icu_6723DayPeriodRulesCountSinkD2Ev:
.LFB3161:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6723DayPeriodRulesCountSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE3161:
	.size	_ZN6icu_6723DayPeriodRulesCountSinkD2Ev, .-_ZN6icu_6723DayPeriodRulesCountSinkD2Ev
	.globl	_ZN6icu_6723DayPeriodRulesCountSinkD1Ev
	.set	_ZN6icu_6723DayPeriodRulesCountSinkD1Ev,_ZN6icu_6723DayPeriodRulesCountSinkD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722DayPeriodRulesDataSinkD0Ev
	.type	_ZN6icu_6722DayPeriodRulesDataSinkD0Ev, @function
_ZN6icu_6722DayPeriodRulesDataSinkD0Ev:
.LFB3159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722DayPeriodRulesDataSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3159:
	.size	_ZN6icu_6722DayPeriodRulesDataSinkD0Ev, .-_ZN6icu_6722DayPeriodRulesDataSinkD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723DayPeriodRulesCountSinkD0Ev
	.type	_ZN6icu_6723DayPeriodRulesCountSinkD0Ev, @function
_ZN6icu_6723DayPeriodRulesCountSinkD0Ev:
.LFB3163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6723DayPeriodRulesCountSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3163:
	.size	_ZN6icu_6723DayPeriodRulesCountSinkD0Ev, .-_ZN6icu_6723DayPeriodRulesCountSinkD0Ev
	.p2align 4
	.globl	dayPeriodRulesCleanup_67
	.type	dayPeriodRulesCleanup_67, @function
dayPeriodRulesCleanup_67:
.LFB3164:
	.cfi_startproc
	endbr64
	movq	_ZN6icu_6712_GLOBAL__N_14dataE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L9
	call	_ZN6icu_677UMemorydaEPv@PLT
	movq	_ZN6icu_6712_GLOBAL__N_14dataE(%rip), %rax
.L9:
	movq	(%rax), %rdi
	call	uhash_close_67@PLT
	movq	_ZN6icu_6712_GLOBAL__N_14dataE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZN6icu_677UMemorydlEPv@PLT
.L10:
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, _ZN6icu_6712_GLOBAL__N_14dataE(%rip)
	ret
	.cfi_endproc
.LFE3164:
	.size	dayPeriodRulesCleanup_67, .-dayPeriodRulesCleanup_67
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dayPeriods"
.LC1:
	.string	"rules"
.LC2:
	.string	""
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6714DayPeriodRules4loadER10UErrorCode.part.0, @function
_ZN6icu_6714DayPeriodRules4loadER10UErrorCode.part.0:
.LFB4138:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L19
	movl	$0, 16(%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
.L19:
	movq	uhash_compareChars_67@GOTPCREL(%rip), %rsi
	movq	uhash_hashChars_67@GOTPCREL(%rip), %rdi
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%rbx, _ZN6icu_6712_GLOBAL__N_14dataE(%rip)
	leaq	-184(%rbp), %r14
	leaq	-176(%rbp), %r15
	call	uhash_open_67@PLT
	xorl	%edi, %edi
	movq	%r12, %rdx
	leaq	.LC0(%rip), %rsi
	movq	%rax, (%rbx)
	leaq	16+_ZTVN6icu_6723DayPeriodRulesCountSinkE(%rip), %rbx
	call	ures_openDirect_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%rbx, -184(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	leaq	-168(%rbp), %rdx
	xorl	%eax, %eax
	movl	$12, %ecx
	movq	%rdx, %rdi
	leaq	16+_ZTVN6icu_6722DayPeriodRulesDataSinkE(%rip), %r8
	movq	%r15, %rdx
	rep stosq
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rcx
	movq	%r8, -176(%rbp)
	movl	$0, (%rdi)
	movq	%r13, %rdi
	call	ures_getAllItemsWithFallback_67@PLT
	leaq	dayPeriodRulesCleanup_67(%rip), %rsi
	movl	$24, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	leaq	16+_ZTVN6icu_6722DayPeriodRulesDataSinkE(%rip), %r8
	movq	%r15, %rdi
	movq	%r8, -176(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	movq	%r14, %rdi
	movq	%rbx, -184(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	testq	%r13, %r13
	je	.L18
	movq	%r13, %rdi
	call	ures_close_67@PLT
.L18:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L29:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4138:
	.size	_ZN6icu_6714DayPeriodRules4loadER10UErrorCode.part.0, .-_ZN6icu_6714DayPeriodRules4loadER10UErrorCode.part.0
	.section	.text._ZN6icu_6723DayPeriodRulesCountSink3putEPKcRNS_13ResourceValueEaR10UErrorCode,"axG",@progbits,_ZN6icu_6723DayPeriodRulesCountSink3putEPKcRNS_13ResourceValueEaR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6723DayPeriodRulesCountSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.type	_ZN6icu_6723DayPeriodRulesCountSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, @function
_ZN6icu_6723DayPeriodRulesCountSink3putEPKcRNS_13ResourceValueEaR10UErrorCode:
.LFB3155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	movq	%rsi, -104(%rbp)
	movq	%r14, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%r8, %rdx
	call	*88(%rax)
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L30
	xorl	%r15d, %r15d
	leaq	-104(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L30
	movl	(%rbx), %eax
	movq	-104(%rbp), %rdx
	testl	%eax, %eax
	jg	.L41
	cmpb	$115, (%rdx)
	jne	.L36
	cmpb	$101, 1(%rdx)
	jne	.L36
	movzbl	2(%rdx), %ecx
	subl	$116, %ecx
	jne	.L36
	movsbl	3(%rdx), %eax
	testb	%al, %al
	je	.L36
	addq	$4, %rdx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L55:
	leal	(%rcx,%rcx,4), %ecx
	addq	$1, %rdx
	leal	(%rax,%rcx,2), %ecx
	movsbl	-1(%rdx), %eax
	testb	%al, %al
	je	.L54
.L37:
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L55
.L36:
	movl	$3, (%rbx)
	movl	$-1, %ecx
.L33:
	movq	_ZN6icu_6712_GLOBAL__N_14dataE(%rip), %rax
	cmpl	%ecx, 16(%rax)
	jge	.L38
	movl	%ecx, 16(%rax)
.L38:
	addl	$1, %r15d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L54:
	testl	%ecx, %ecx
	jne	.L33
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L30:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	$-1, %ecx
	jmp	.L33
.L56:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3155:
	.size	_ZN6icu_6723DayPeriodRulesCountSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, .-_ZN6icu_6723DayPeriodRulesCountSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3484:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3484:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3487:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L70
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L58
	cmpb	$0, 12(%rbx)
	jne	.L71
.L62:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L58:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L62
	.cfi_endproc
.LFE3487:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3490:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L74
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3490:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3493:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L77
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3493:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L83
.L79:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L84
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3495:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3496:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3496:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3497:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3497:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3498:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3498:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3499:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3499:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3500:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3500:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3501:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L100
	testl	%edx, %edx
	jle	.L100
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L103
.L92:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L92
	.cfi_endproc
.LFE3501:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L107
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L107
	testl	%r12d, %r12d
	jg	.L114
	cmpb	$0, 12(%rbx)
	jne	.L115
.L109:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L109
.L115:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L107:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3502:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L117
	movq	(%rdi), %r8
.L118:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L121
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L121
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L121:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3503:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3504:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L128
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3504:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3505:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3505:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3506:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3506:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3507:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3507:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3509:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3509:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3511:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3511:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714DayPeriodRules4loadER10UErrorCode
	.type	_ZN6icu_6714DayPeriodRules4loadER10UErrorCode, @function
_ZN6icu_6714DayPeriodRules4loadER10UErrorCode:
.LFB3165:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L136
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	jmp	_ZN6icu_6714DayPeriodRules4loadER10UErrorCode.part.0
	.cfi_endproc
.LFE3165:
	.size	_ZN6icu_6714DayPeriodRules4loadER10UErrorCode, .-_ZN6icu_6714DayPeriodRules4loadER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714DayPeriodRules11getInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6714DayPeriodRules11getInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6714DayPeriodRules11getInstanceERKNS_6LocaleER10UErrorCode:
.LFB3169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L155
.L138:
	xorl	%eax, %eax
.L137:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L156
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movl	_ZN6icu_6712_GLOBAL__N_18initOnceE(%rip), %eax
	movq	%rdi, %r13
	movq	%rsi, %r12
	cmpl	$2, %eax
	jne	.L157
.L139:
	movl	4+_ZN6icu_6712_GLOBAL__N_18initOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L141
	movl	%eax, (%r12)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L157:
	leaq	_ZN6icu_6712_GLOBAL__N_18initOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L139
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L140
	movq	%r12, %rdi
	call	_ZN6icu_6714DayPeriodRules4loadER10UErrorCode.part.0
	movl	(%r12), %eax
.L140:
	leaq	_ZN6icu_6712_GLOBAL__N_18initOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_6712_GLOBAL__N_18initOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L141:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L138
	movq	%r13, %rdi
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	strlen@PLT
	cmpq	$156, %rax
	ja	.L142
	leaq	-368(%rbp), %rbx
	leaq	1(%rax), %rdx
	movl	$157, %ecx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__memcpy_chk@PLT
	cmpb	$0, -368(%rbp)
	je	.L158
.L143:
	leaq	-208(%rbp), %r13
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r12, %rcx
	movl	$157, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	uloc_getParent_67@PLT
	cmpb	$0, -208(%rbp)
	je	.L138
	movl	$157, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__strcpy_chk@PLT
	cmpb	$0, -368(%rbp)
	je	.L138
.L146:
	movq	_ZN6icu_6712_GLOBAL__N_14dataE(%rip), %rax
	movq	%rbx, %rsi
	movq	(%rax), %rdi
	call	uhash_geti_67@PLT
	testl	%eax, %eax
	je	.L159
	jle	.L138
	cltq
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rdx
	movq	_ZN6icu_6712_GLOBAL__N_14dataE(%rip), %rax
	movq	8(%rax), %rax
	leaq	(%rax,%rdx,4), %rax
	cmpl	$-1, 4(%rax)
	jne	.L137
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$15, (%r12)
	xorl	%eax, %eax
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L158:
	movl	$1953460082, -368(%rbp)
	movb	$0, -364(%rbp)
	jmp	.L143
.L156:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3169:
	.size	_ZN6icu_6714DayPeriodRules11getInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6714DayPeriodRules11getInstanceERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714DayPeriodRulesC2Ev
	.type	_ZN6icu_6714DayPeriodRulesC2Ev, @function
_ZN6icu_6714DayPeriodRulesC2Ev:
.LFB3171:
	.cfi_startproc
	endbr64
	pcmpeqd	%xmm0, %xmm0
	xorl	%eax, %eax
	movw	%ax, (%rdi)
	movups	%xmm0, 4(%rdi)
	movups	%xmm0, 20(%rdi)
	movups	%xmm0, 36(%rdi)
	movups	%xmm0, 52(%rdi)
	movups	%xmm0, 68(%rdi)
	movups	%xmm0, 84(%rdi)
	ret
	.cfi_endproc
.LFE3171:
	.size	_ZN6icu_6714DayPeriodRulesC2Ev, .-_ZN6icu_6714DayPeriodRulesC2Ev
	.globl	_ZN6icu_6714DayPeriodRulesC1Ev
	.set	_ZN6icu_6714DayPeriodRulesC1Ev,_ZN6icu_6714DayPeriodRulesC2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714DayPeriodRules23getMidPointForDayPeriodENS0_9DayPeriodER10UErrorCode
	.type	_ZNK6icu_6714DayPeriodRules23getMidPointForDayPeriodENS0_9DayPeriodER10UErrorCode, @function
_ZNK6icu_6714DayPeriodRules23getMidPointForDayPeriodENS0_9DayPeriodER10UErrorCode:
.LFB3173:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L162
	pxor	%xmm0, %xmm0
	testl	%esi, %esi
	je	.L161
	cmpl	$1, %esi
	je	.L178
	xorl	%eax, %eax
	cmpl	4(%rdi), %esi
	jne	.L169
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L195:
	addq	$1, %rax
	cmpq	$24, %rax
	je	.L167
.L169:
	movl	%eax, %r8d
	cmpl	4(%rdi,%rax,4), %esi
	jne	.L195
.L166:
	movl	$23, %eax
	cmpl	4(%rdi), %esi
	jne	.L174
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L173:
	subq	$1, %rax
	cmpq	$-1, %rax
	je	.L167
.L174:
	movl	%eax, %ecx
	cmpl	4(%rdi,%rax,4), %esi
	jne	.L173
	addl	$1, %ecx
.L171:
	leal	(%rcx,%r8), %eax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LC6(%rip), %xmm0
	cmpl	%r8d, %ecx
	jge	.L161
	addsd	.LC4(%rip), %xmm0
	movsd	.LC7(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jb	.L161
	subsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	cmpl	96(%rdi), %esi
	jne	.L174
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L172:
	movl	%eax, %ecx
	cmpl	4(%rdi,%rax,4), %esi
	jne	.L171
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L172
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$1, (%rdx)
.L162:
	movsd	.LC3(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	cmpl	96(%rdi), %esi
	jne	.L169
	movl	$22, %eax
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L165:
	subq	$1, %rax
	je	.L167
.L168:
	movl	%eax, %r8d
	cmpl	4(%rdi,%rax,4), %esi
	je	.L165
	addl	$1, %r8d
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L178:
	movsd	.LC4(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3173:
	.size	_ZNK6icu_6714DayPeriodRules23getMidPointForDayPeriodENS0_9DayPeriodER10UErrorCode, .-_ZNK6icu_6714DayPeriodRules23getMidPointForDayPeriodENS0_9DayPeriodER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714DayPeriodRules24getStartHourForDayPeriodENS0_9DayPeriodER10UErrorCode
	.type	_ZNK6icu_6714DayPeriodRules24getStartHourForDayPeriodENS0_9DayPeriodER10UErrorCode, @function
_ZNK6icu_6714DayPeriodRules24getStartHourForDayPeriodENS0_9DayPeriodER10UErrorCode:
.LFB3174:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L203
	xorl	%r8d, %r8d
	testl	%esi, %esi
	je	.L196
	cmpl	$1, %esi
	je	.L205
	xorl	%eax, %eax
	cmpl	4(%rdi), %esi
	jne	.L202
	cmpl	96(%rdi), %esi
	jne	.L202
	movl	$22, %eax
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L199:
	subq	$1, %rax
	je	.L200
.L201:
	movl	%eax, %r8d
	cmpl	4(%rdi,%rax,4), %esi
	je	.L199
	addl	$1, %r8d
.L196:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	addq	$1, %rax
	cmpq	$24, %rax
	je	.L200
.L202:
	movl	%eax, %r8d
	cmpl	4(%rdi,%rax,4), %esi
	jne	.L213
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L200:
	movl	$-1, %r8d
	movl	$1, (%rdx)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	movl	$12, %r8d
	jmp	.L196
.L203:
	movl	$-1, %r8d
	jmp	.L196
	.cfi_endproc
.LFE3174:
	.size	_ZNK6icu_6714DayPeriodRules24getStartHourForDayPeriodENS0_9DayPeriodER10UErrorCode, .-_ZNK6icu_6714DayPeriodRules24getStartHourForDayPeriodENS0_9DayPeriodER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714DayPeriodRules22getEndHourForDayPeriodENS0_9DayPeriodER10UErrorCode
	.type	_ZNK6icu_6714DayPeriodRules22getEndHourForDayPeriodENS0_9DayPeriodER10UErrorCode, @function
_ZNK6icu_6714DayPeriodRules22getEndHourForDayPeriodENS0_9DayPeriodER10UErrorCode:
.LFB3175:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L221
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L214
	cmpl	$1, %esi
	je	.L223
	movl	$23, %eax
	cmpl	4(%rdi), %esi
	jne	.L220
	cmpl	96(%rdi), %esi
	jne	.L220
	movl	$1, %ecx
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L231:
	addq	$1, %rcx
	cmpq	$23, %rcx
	je	.L217
.L218:
	movl	%ecx, %eax
	cmpl	4(%rdi,%rcx,4), %esi
	je	.L231
.L214:
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	subq	$1, %rax
	cmpq	$-1, %rax
	je	.L217
.L220:
	cmpl	4(%rdi,%rax,4), %esi
	jne	.L219
	leal	1(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	movl	$1, (%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	movl	$12, %eax
	ret
.L221:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3175:
	.size	_ZNK6icu_6714DayPeriodRules22getEndHourForDayPeriodENS0_9DayPeriodER10UErrorCode, .-_ZNK6icu_6714DayPeriodRules22getEndHourForDayPeriodENS0_9DayPeriodER10UErrorCode
	.section	.rodata.str1.1
.LC8:
	.string	"midnight"
.LC9:
	.string	"noon"
.LC10:
	.string	"morning1"
.LC11:
	.string	"afternoon1"
.LC12:
	.string	"evening1"
.LC13:
	.string	"night1"
.LC14:
	.string	"morning2"
.LC15:
	.string	"afternoon2"
.LC16:
	.string	"evening2"
.LC17:
	.string	"night2"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714DayPeriodRules22getDayPeriodFromStringEPKc
	.type	_ZN6icu_6714DayPeriodRules22getDayPeriodFromStringEPKc, @function
_ZN6icu_6714DayPeriodRules22getDayPeriodFromStringEPKc:
.LFB3176:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movl	$9, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	testl	%eax, %eax
	je	.L232
	movl	$5, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rdx, %rsi
	movl	$1, %eax
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L232
	movl	$9, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%rdx, %rsi
	movl	$2, %eax
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L232
	movl	$11, %ecx
	leaq	.LC11(%rip), %rdi
	movq	%rdx, %rsi
	movl	$3, %eax
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L232
	movl	$9, %ecx
	leaq	.LC12(%rip), %rdi
	movq	%rdx, %rsi
	movl	$4, %eax
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L232
	movl	$7, %ecx
	leaq	.LC13(%rip), %rdi
	movq	%rdx, %rsi
	movl	$5, %eax
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L232
	movl	$9, %ecx
	leaq	.LC14(%rip), %rdi
	movq	%rdx, %rsi
	movl	$6, %eax
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L232
	movl	$11, %ecx
	leaq	.LC15(%rip), %rdi
	movq	%rdx, %rsi
	movl	$7, %eax
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L232
	movl	$9, %ecx
	leaq	.LC16(%rip), %rdi
	movq	%rdx, %rsi
	movl	$8, %eax
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L232
	movl	$7, %ecx
	leaq	.LC17(%rip), %rdi
	movq	%rdx, %rsi
	movl	$9, %eax
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L232
	movzbl	(%rdx), %ecx
	cmpl	$97, %ecx
	je	.L263
.L247:
	cmpl	$112, %ecx
	je	.L264
.L248:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	cmpb	$109, 1(%rdx)
	jne	.L248
	cmpb	$0, 2(%rdx)
	jne	.L248
	movl	$11, %eax
.L232:
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	cmpb	$109, 1(%rdx)
	jne	.L247
	cmpb	$0, 2(%rdx)
	movl	$10, %eax
	jne	.L247
	ret
	.cfi_endproc
.LFE3176:
	.size	_ZN6icu_6714DayPeriodRules22getDayPeriodFromStringEPKc, .-_ZN6icu_6714DayPeriodRules22getDayPeriodFromStringEPKc
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714DayPeriodRules3addEiiNS0_9DayPeriodE
	.type	_ZN6icu_6714DayPeriodRules3addEiiNS0_9DayPeriodE, @function
_ZN6icu_6714DayPeriodRules3addEiiNS0_9DayPeriodE:
.LFB3177:
	.cfi_startproc
	endbr64
	cmpl	%edx, %esi
	jne	.L266
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L267:
	movl	%ecx, 4(%rdi)
	cmpl	$1, %edx
	je	.L265
	movl	$1, %eax
	leal	1(%rax), %esi
	movl	%ecx, 4(%rdi,%rax,4)
	cmpl	%esi, %edx
	je	.L274
.L266:
	cmpl	$24, %esi
	je	.L267
	movslq	%esi, %rax
	leal	1(%rax), %esi
	movl	%ecx, 4(%rdi,%rax,4)
	cmpl	%esi, %edx
	jne	.L266
.L274:
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	ret
	.cfi_endproc
.LFE3177:
	.size	_ZN6icu_6714DayPeriodRules3addEiiNS0_9DayPeriodE, .-_ZN6icu_6714DayPeriodRules3addEiiNS0_9DayPeriodE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714DayPeriodRules14allHoursAreSetEv
	.type	_ZN6icu_6714DayPeriodRules14allHoursAreSetEv, @function
_ZN6icu_6714DayPeriodRules14allHoursAreSetEv:
.LFB3178:
	.cfi_startproc
	endbr64
	leaq	4(%rdi), %rax
	addq	$100, %rdi
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L281:
	addq	$4, %rax
	cmpq	%rdi, %rax
	je	.L280
.L277:
	cmpl	$-1, (%rax)
	jne	.L281
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3178:
	.size	_ZN6icu_6714DayPeriodRules14allHoursAreSetEv, .-_ZN6icu_6714DayPeriodRules14allHoursAreSetEv
	.section	.rodata._ZN6icu_6722DayPeriodRulesDataSink12processRulesERKNS_13ResourceTableEPKcRNS_13ResourceValueER10UErrorCode.str1.1,"aMS",@progbits,1
.LC18:
	.string	"from"
.LC19:
	.string	"before"
.LC20:
	.string	"after"
	.section	.text._ZN6icu_6722DayPeriodRulesDataSink12processRulesERKNS_13ResourceTableEPKcRNS_13ResourceValueER10UErrorCode,"axG",@progbits,_ZN6icu_6722DayPeriodRulesDataSink12processRulesERKNS_13ResourceTableEPKcRNS_13ResourceValueER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6722DayPeriodRulesDataSink12processRulesERKNS_13ResourceTableEPKcRNS_13ResourceValueER10UErrorCode
	.type	_ZN6icu_6722DayPeriodRulesDataSink12processRulesERKNS_13ResourceTableEPKcRNS_13ResourceValueER10UErrorCode, @function
_ZN6icu_6722DayPeriodRulesDataSink12processRulesERKNS_13ResourceTableEPKcRNS_13ResourceValueER10UErrorCode:
.LFB3148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -288(%rbp)
	movl	(%r8), %edi
	movq	%rsi, -352(%rbp)
	movq	%rdx, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-280(%rbp), %rax
	movl	$0, -340(%rbp)
	movq	%rax, -320(%rbp)
	leaq	-118(%rbp), %rax
	movq	%rax, -336(%rbp)
	testl	%edi, %edi
	jg	.L282
	movq	%rcx, %r15
	movq	%r8, %r14
	movq	%rsi, %rdi
.L283:
	movq	-320(%rbp), %rdx
	movl	-340(%rbp), %esi
	movq	%r15, %rcx
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L282
	movl	(%r14), %ecx
	movq	-280(%rbp), %rdx
	testl	%ecx, %ecx
	jg	.L345
	cmpb	$115, (%rdx)
	je	.L399
.L289:
	movl	$3, (%r14)
	movl	$-1, %ecx
.L286:
	movq	-288(%rbp), %rax
	movq	%r14, %rdx
	movq	%r15, %rsi
	movl	%ecx, 108(%rax)
	leaq	-224(%rbp), %rax
	movq	%rax, -360(%rbp)
	movq	%rax, %rdi
	movq	(%r15), %rax
	call	*88(%rax)
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L282
	movl	$0, -344(%rbp)
.L338:
	movq	-320(%rbp), %rdx
	movl	-344(%rbp), %esi
	movq	%r15, %rcx
	movq	-360(%rbp), %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L292
	movq	-280(%rbp), %rdi
	call	_ZN6icu_6714DayPeriodRules22getDayPeriodFromStringEPKc
	movq	-288(%rbp), %rbx
	movl	%eax, 112(%rbx)
	cmpl	$-1, %eax
	je	.L339
	leaq	-176(%rbp), %rax
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rax, -328(%rbp)
	movq	%rax, %rdi
	movq	(%r15), %rax
	call	*88(%rax)
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L282
	movl	$0, -312(%rbp)
.L324:
	movq	-320(%rbp), %rdx
	movl	-312(%rbp), %esi
	movq	%r15, %rcx
	movq	-328(%rbp), %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L296
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L297
	movq	-280(%rbp), %rdx
	movl	$5, %ecx
	leaq	.LC18(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L346
	movl	$7, %ecx
	movq	%rdx, %rsi
	leaq	.LC19(%rip), %rdi
	repz cmpsb
	seta	%r8b
	sbbb	$0, %r8b
	movsbl	%r8b, %r8d
	testl	%r8d, %r8d
	je	.L347
	movl	$6, %ecx
	leaq	.LC20(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L348
	cmpb	$97, (%rdx)
	je	.L400
.L356:
	movl	$-1, %r8d
	.p2align 4,,10
	.p2align 3
.L298:
	movq	(%r15), %rax
	leaq	-264(%rbp), %r13
	movq	%r14, %rdx
	movq	%r15, %rdi
	movl	%r8d, -296(%rbp)
	movq	%r13, %rsi
	leaq	-128(%rbp), %r12
	movl	$0, -264(%rbp)
	call	*32(%rax)
	movl	-264(%rbp), %ecx
	leaq	-256(%rbp), %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -256(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L302
	movl	-296(%rbp), %r8d
	cmpl	$-1, %r8d
	je	.L305
	movzwl	-120(%rbp), %edx
	testw	%dx, %dx
	js	.L303
	movswl	%dx, %eax
	sarl	$5, %eax
.L304:
	leal	-4(%rax), %esi
	leal	-3(%rax), %ecx
	cmpl	$1, %esi
	ja	.L305
	cmpl	%eax, %ecx
	jnb	.L305
	andl	$2, %edx
	movq	-336(%rbp), %rsi
	cmove	-104(%rbp), %rsi
	movslq	%ecx, %rdx
	leaq	(%rdx,%rdx), %rdi
	cmpw	$58, (%rsi,%rdx,2)
	jne	.L305
	cmpw	$48, 2(%rsi,%rdi)
	jne	.L305
	testl	%eax, %eax
	je	.L305
	cmpw	$48, 4(%rsi,%rdi)
	jne	.L305
	movzwl	(%rsi), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L305
	cmpl	$2, %ecx
	je	.L401
.L308:
	movq	-288(%rbp), %rsi
	cltq
	orl	%ebx, 8(%rsi,%rax,4)
.L302:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L282
.L309:
	addl	$1, -312(%rbp)
	jmp	.L324
.L292:
	movq	-288(%rbp), %rax
	movslq	108(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rdx
	movq	_ZN6icu_6712_GLOBAL__N_14dataE(%rip), %rax
	movq	8(%rax), %rax
	leaq	(%rax,%rdx,4), %rdx
	leaq	4(%rdx), %rax
	addq	$100, %rdx
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L403:
	addq	$4, %rax
	cmpq	%rax, %rdx
	je	.L402
.L341:
	cmpl	$-1, (%rax)
	jne	.L403
.L339:
	movl	$3, (%r14)
	.p2align 4,,10
	.p2align 3
.L282:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L404
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	cmpb	$101, 1(%rdx)
	jne	.L289
	movzbl	2(%rdx), %ecx
	subl	$116, %ecx
	jne	.L289
	movsbl	3(%rdx), %eax
	testb	%al, %al
	je	.L289
	addq	$4, %rdx
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L406:
	leal	(%rcx,%rcx,4), %ecx
	addq	$1, %rdx
	leal	(%rax,%rcx,2), %ecx
	movsbl	-1(%rdx), %eax
	testb	%al, %al
	je	.L405
.L290:
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L406
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L297:
	movq	-280(%rbp), %r8
	movl	$5, %ecx
	movl	$2, %eax
	leaq	.LC18(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L310
	movl	$7, %ecx
	movq	%r8, %rsi
	leaq	.LC19(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	testl	%eax, %eax
	je	.L310
	movl	$6, %ecx
	leaq	.LC20(%rip), %rdi
	movq	%r8, %rsi
	movl	$1, %eax
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L310
	cmpb	$97, (%r8)
	jne	.L357
	cmpb	$116, 1(%r8)
	je	.L407
.L357:
	movl	$-1, %eax
	.p2align 4,,10
	.p2align 3
.L310:
	movq	-288(%rbp), %rbx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movl	%eax, 116(%rbx)
	leaq	-256(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	%rax, %rdi
	movq	(%r15), %rax
	call	*80(%rax)
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L282
	movl	-240(%rbp), %eax
	movl	%eax, -308(%rbp)
	testl	%eax, %eax
	jle	.L309
	leaq	-268(%rbp), %rax
	xorl	%ebx, %ebx
	leaq	-264(%rbp), %r13
	movq	%rax, -304(%rbp)
	leaq	-128(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L313:
	movq	-296(%rbp), %rdi
	movq	%r15, %rdx
	movl	%ebx, %esi
	call	_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE@PLT
	movq	(%r15), %rax
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	-304(%rbp), %rsi
	movl	$0, -268(%rbp)
	call	*32(%rax)
	movl	-268(%rbp), %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L316
	movq	-288(%rbp), %rax
	movl	116(%rax), %ecx
	cmpl	$-1, %ecx
	je	.L319
	movzwl	-120(%rbp), %edx
	testw	%dx, %dx
	js	.L317
	movswl	%dx, %eax
	sarl	$5, %eax
.L318:
	leal	-4(%rax), %edi
	leal	-3(%rax), %esi
	cmpl	$1, %edi
	ja	.L319
	cmpl	%eax, %esi
	jnb	.L319
	andl	$2, %edx
	movq	-336(%rbp), %rdi
	cmove	-104(%rbp), %rdi
	movslq	%esi, %rdx
	leaq	(%rdx,%rdx), %r9
	cmpw	$58, (%rdi,%rdx,2)
	jne	.L319
	cmpw	$48, 2(%rdi,%r9)
	jne	.L319
	testl	%eax, %eax
	je	.L319
	cmpw	$48, 4(%rdi,%r9)
	jne	.L319
	movzwl	(%rdi), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L319
	cmpl	$2, %esi
	je	.L408
.L322:
	movq	-288(%rbp), %rsi
	cltq
	movl	$1, %edx
	sall	%cl, %edx
	orl	%edx, 8(%rsi,%rax,4)
	jmp	.L316
.L408:
	movzwl	2(%rdi), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L319
	leal	(%rax,%rax,4), %eax
	leal	(%rdx,%rax,2), %eax
	cmpl	$24, %eax
	jle	.L322
	.p2align 4,,10
	.p2align 3
.L319:
	movl	$3, (%r14)
.L316:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r14), %r11d
	testl	%r11d, %r11d
	jg	.L282
	addl	$1, %ebx
	cmpl	-308(%rbp), %ebx
	jne	.L313
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L317:
	movl	-116(%rbp), %eax
	jmp	.L318
.L401:
	movzwl	2(%rsi), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L305
	leal	(%rax,%rax,4), %eax
	leal	(%rdx,%rax,2), %eax
	cmpl	$24, %eax
	jle	.L308
	.p2align 4,,10
	.p2align 3
.L305:
	movl	$3, (%r14)
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L347:
	movl	$1, %ebx
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L346:
	movl	$4, %ebx
	movl	$2, %r8d
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L348:
	movl	$2, %ebx
	movl	$1, %r8d
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L400:
	cmpb	$116, 1(%rdx)
	jne	.L356
	cmpb	$0, 2(%rdx)
	jne	.L356
	movl	$8, %ebx
	movl	$3, %r8d
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L303:
	movl	-116(%rbp), %eax
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L405:
	testl	%ecx, %ecx
	jne	.L286
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L407:
	cmpb	$0, 2(%r8)
	jne	.L357
	movl	$3, %eax
	jmp	.L310
.L296:
	movq	-288(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movslq	108(%rsi), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rdx
	movq	_ZN6icu_6712_GLOBAL__N_14dataE(%rip), %rax
	movq	8(%rax), %rax
	leaq	(%rax,%rdx,4), %r9
.L337:
	movl	8(%rsi,%rcx,4), %edx
	movl	%ecx, %eax
	movl	%ecx, %edi
	testb	$8, %dl
	je	.L325
	testq	%rcx, %rcx
	je	.L409
	cmpl	$12, %ecx
	jne	.L327
	cmpl	$1, 112(%rsi)
	jne	.L327
	movb	$1, 1(%r9)
.L328:
	addl	$1, %eax
	andl	$6, %edx
	jne	.L336
.L330:
	addq	$1, %rcx
	cmpq	$25, %rcx
	jne	.L337
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L325:
	andl	$6, %edx
	leal	1(%rcx), %eax
	je	.L330
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L332:
	addl	$1, %eax
	cmpl	%ecx, %eax
	je	.L327
.L336:
	cmpl	$25, %eax
	cmove	%r8d, %eax
	movslq	%eax, %rdx
	movl	8(%rsi,%rdx,4), %edx
	andl	$1, %edx
	je	.L332
	movl	112(%rsi), %r11d
	cmpl	%ecx, %eax
	jne	.L334
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L333:
	movl	%r11d, 4(%r9)
	cmpl	$1, %eax
	je	.L330
	movslq	%edx, %r10
.L335:
	leal	1(%r10), %edi
	movl	%r11d, 4(%r9,%r10,4)
	cmpl	%eax, %edi
	je	.L330
.L334:
	cmpl	$24, %edi
	je	.L333
	movslq	%edi, %r10
	jmp	.L335
.L327:
	movl	$3, (%r14)
.L329:
	movq	-288(%rbp), %rax
	leaq	16(%rax), %rdi
	movq	$0, 8(%rax)
	andq	$-8, %rdi
	movq	$0, 100(%rax)
	subl	%edi, %eax
	leal	108(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	addl	$1, -344(%rbp)
	jmp	.L338
.L409:
	movl	112(%rsi), %r10d
	testl	%r10d, %r10d
	jne	.L327
	movb	$1, (%r9)
	jmp	.L328
.L402:
	addl	$1, -340(%rbp)
	movq	-352(%rbp), %rdi
	jmp	.L283
.L345:
	movl	$-1, %ecx
	jmp	.L286
.L404:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3148:
	.size	_ZN6icu_6722DayPeriodRulesDataSink12processRulesERKNS_13ResourceTableEPKcRNS_13ResourceValueER10UErrorCode, .-_ZN6icu_6722DayPeriodRulesDataSink12processRulesERKNS_13ResourceTableEPKcRNS_13ResourceValueER10UErrorCode
	.section	.rodata._ZN6icu_6722DayPeriodRulesDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode.str1.1,"aMS",@progbits,1
.LC21:
	.string	"locales"
	.section	.text._ZN6icu_6722DayPeriodRulesDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode,"axG",@progbits,_ZN6icu_6722DayPeriodRulesDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6722DayPeriodRulesDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.type	_ZN6icu_6722DayPeriodRulesDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, @function
_ZN6icu_6722DayPeriodRulesDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode:
.LFB3147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -352(%rbp)
	movq	%rsi, -312(%rbp)
	movq	%r13, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-288(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	movq	(%rdx), %rax
	movq	%r8, %rdx
	call	*88(%rax)
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L410
	leaq	-300(%rbp), %rax
	leaq	-312(%rbp), %r15
	movl	$0, -332(%rbp)
	movq	%rax, -328(%rbp)
	.p2align 4,,10
	.p2align 3
.L426:
	movl	-332(%rbp), %esi
	movq	-344(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L410
	movq	-312(%rbp), %rax
	movl	$8, %ecx
	leaq	.LC21(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%bl
	sbbb	$0, %bl
	movsbl	%bl, %ebx
	testl	%ebx, %ebx
	je	.L453
	movq	%rax, %rsi
	movl	$6, %ecx
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L421
	movq	_ZN6icu_6712_GLOBAL__N_14dataE(%rip), %rax
	movq	$-1, %rdi
	movl	16(%rax), %edx
	movabsq	$92233720368547758, %rax
	addl	$1, %edx
	movslq	%edx, %rbx
	cmpq	%rax, %rbx
	ja	.L422
	leaq	(%rbx,%rbx,4), %rax
	leaq	(%rax,%rax,4), %rdi
	salq	$2, %rdi
.L422:
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L423
	subq	$1, %rbx
	movq	%rbx, %rdx
	js	.L424
	pcmpeqd	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L425:
	subq	$1, %rdx
	movb	$0, (%rax)
	addq	$100, %rax
	movb	$0, -99(%rax)
	movups	%xmm0, -96(%rax)
	movups	%xmm0, -80(%rax)
	movups	%xmm0, -64(%rax)
	movups	%xmm0, -48(%rax)
	movups	%xmm0, -32(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	$-1, %rdx
	jne	.L425
.L424:
	movq	_ZN6icu_6712_GLOBAL__N_14dataE(%rip), %rax
	leaq	-240(%rbp), %r14
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, 8(%rax)
	movq	0(%r13), %rax
	call	*88(%rax)
	movq	-312(%rbp), %rdx
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	-352(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6722DayPeriodRulesDataSink12processRulesERKNS_13ResourceTableEPKcRNS_13ResourceValueER10UErrorCode
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L410
.L421:
	addl	$1, -332(%rbp)
	jmp	.L426
.L453:
	leaq	-240(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%rax, -320(%rbp)
	movq	%rax, %rdi
	movq	0(%r13), %rax
	call	*88(%rax)
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L410
	.p2align 4,,10
	.p2align 3
.L413:
	movq	-320(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movl	%ebx, %esi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L421
	movq	0(%r13), %rax
	movq	-328(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$0, -300(%rbp)
	leaq	-192(%rbp), %r14
	call	*32(%rax)
	movl	-300(%rbp), %ecx
	leaq	-296(%rbp), %rdx
	movq	%r14, %rdi
	movl	$1, %esi
	movq	%rax, -296(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	leaq	-128(%rbp), %rdi
	leaq	-115(%rbp), %rax
	movq	%r14, %rsi
	movw	%dx, -116(%rbp)
	movq	%r12, %rdx
	movq	%rax, -128(%rbp)
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	(%r12), %ecx
	movq	-128(%rbp), %rdi
	testl	%ecx, %ecx
	jg	.L430
	cmpb	$115, (%rdi)
	jne	.L418
	cmpb	$101, 1(%rdi)
	jne	.L418
	movzbl	2(%rdi), %edx
	subl	$116, %edx
	jne	.L418
	movsbl	3(%rdi), %eax
	testb	%al, %al
	je	.L418
	leaq	4(%rdi), %rcx
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L455:
	leal	(%rdx,%rdx,4), %edx
	addq	$1, %rcx
	leal	(%rax,%rdx,2), %edx
	movsbl	-1(%rcx), %eax
	testb	%al, %al
	je	.L454
.L419:
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L455
.L418:
	movl	$3, (%r12)
	movl	$-1, %edx
.L415:
	cmpb	$0, -116(%rbp)
	jne	.L456
.L420:
	movq	_ZN6icu_6712_GLOBAL__N_14dataE(%rip), %rax
	movq	-312(%rbp), %rsi
	movq	%r12, %rcx
	addl	$1, %ebx
	movq	(%rax), %rdi
	call	uhash_puti_67@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L454:
	testl	%edx, %edx
	je	.L418
	cmpb	$0, -116(%rbp)
	je	.L420
.L456:
	movl	%edx, -336(%rbp)
	call	uprv_free_67@PLT
	movl	-336(%rbp), %edx
	jmp	.L420
.L423:
	movq	_ZN6icu_6712_GLOBAL__N_14dataE(%rip), %rax
	movq	$0, 8(%rax)
	movl	$7, (%r12)
.L410:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L457
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L430:
	.cfi_restore_state
	movl	$-1, %edx
	jmp	.L415
.L457:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3147:
	.size	_ZN6icu_6722DayPeriodRulesDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, .-_ZN6icu_6722DayPeriodRulesDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.weak	_ZTSN6icu_6722DayPeriodRulesDataSinkE
	.section	.rodata._ZTSN6icu_6722DayPeriodRulesDataSinkE,"aG",@progbits,_ZTSN6icu_6722DayPeriodRulesDataSinkE,comdat
	.align 32
	.type	_ZTSN6icu_6722DayPeriodRulesDataSinkE, @object
	.size	_ZTSN6icu_6722DayPeriodRulesDataSinkE, 34
_ZTSN6icu_6722DayPeriodRulesDataSinkE:
	.string	"N6icu_6722DayPeriodRulesDataSinkE"
	.weak	_ZTIN6icu_6722DayPeriodRulesDataSinkE
	.section	.data.rel.ro._ZTIN6icu_6722DayPeriodRulesDataSinkE,"awG",@progbits,_ZTIN6icu_6722DayPeriodRulesDataSinkE,comdat
	.align 8
	.type	_ZTIN6icu_6722DayPeriodRulesDataSinkE, @object
	.size	_ZTIN6icu_6722DayPeriodRulesDataSinkE, 24
_ZTIN6icu_6722DayPeriodRulesDataSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722DayPeriodRulesDataSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.weak	_ZTSN6icu_6723DayPeriodRulesCountSinkE
	.section	.rodata._ZTSN6icu_6723DayPeriodRulesCountSinkE,"aG",@progbits,_ZTSN6icu_6723DayPeriodRulesCountSinkE,comdat
	.align 32
	.type	_ZTSN6icu_6723DayPeriodRulesCountSinkE, @object
	.size	_ZTSN6icu_6723DayPeriodRulesCountSinkE, 35
_ZTSN6icu_6723DayPeriodRulesCountSinkE:
	.string	"N6icu_6723DayPeriodRulesCountSinkE"
	.weak	_ZTIN6icu_6723DayPeriodRulesCountSinkE
	.section	.data.rel.ro._ZTIN6icu_6723DayPeriodRulesCountSinkE,"awG",@progbits,_ZTIN6icu_6723DayPeriodRulesCountSinkE,comdat
	.align 8
	.type	_ZTIN6icu_6723DayPeriodRulesCountSinkE, @object
	.size	_ZTIN6icu_6723DayPeriodRulesCountSinkE, 24
_ZTIN6icu_6723DayPeriodRulesCountSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6723DayPeriodRulesCountSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.weak	_ZTVN6icu_6722DayPeriodRulesDataSinkE
	.section	.data.rel.ro._ZTVN6icu_6722DayPeriodRulesDataSinkE,"awG",@progbits,_ZTVN6icu_6722DayPeriodRulesDataSinkE,comdat
	.align 8
	.type	_ZTVN6icu_6722DayPeriodRulesDataSinkE, @object
	.size	_ZTVN6icu_6722DayPeriodRulesDataSinkE, 48
_ZTVN6icu_6722DayPeriodRulesDataSinkE:
	.quad	0
	.quad	_ZTIN6icu_6722DayPeriodRulesDataSinkE
	.quad	_ZN6icu_6722DayPeriodRulesDataSinkD1Ev
	.quad	_ZN6icu_6722DayPeriodRulesDataSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6722DayPeriodRulesDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.weak	_ZTVN6icu_6723DayPeriodRulesCountSinkE
	.section	.data.rel.ro._ZTVN6icu_6723DayPeriodRulesCountSinkE,"awG",@progbits,_ZTVN6icu_6723DayPeriodRulesCountSinkE,comdat
	.align 8
	.type	_ZTVN6icu_6723DayPeriodRulesCountSinkE, @object
	.size	_ZTVN6icu_6723DayPeriodRulesCountSinkE, 48
_ZTVN6icu_6723DayPeriodRulesCountSinkE:
	.quad	0
	.quad	_ZTIN6icu_6723DayPeriodRulesCountSinkE
	.quad	_ZN6icu_6723DayPeriodRulesCountSinkD1Ev
	.quad	_ZN6icu_6723DayPeriodRulesCountSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6723DayPeriodRulesCountSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.local	_ZN6icu_6712_GLOBAL__N_18initOnceE
	.comm	_ZN6icu_6712_GLOBAL__N_18initOnceE,8,8
	.local	_ZN6icu_6712_GLOBAL__N_14dataE
	.comm	_ZN6icu_6712_GLOBAL__N_14dataE,8,8
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	0
	.long	-1074790400
	.align 8
.LC4:
	.long	0
	.long	1076363264
	.align 8
.LC6:
	.long	0
	.long	1071644672
	.align 8
.LC7:
	.long	0
	.long	1077411840
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
