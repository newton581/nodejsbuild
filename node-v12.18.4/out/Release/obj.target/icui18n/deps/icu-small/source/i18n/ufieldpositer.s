	.file	"ufieldpositer.cpp"
	.text
	.p2align 4
	.globl	ufieldpositer_open_67
	.type	ufieldpositer_open_67, @function
ufieldpositer_open_67:
.LFB2014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L1
	movq	%rdi, %rbx
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3
	movq	%rax, %rdi
	call	_ZN6icu_6721FieldPositionIteratorC1Ev@PLT
.L1:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L1
	.cfi_endproc
.LFE2014:
	.size	ufieldpositer_open_67, .-ufieldpositer_open_67
	.p2align 4
	.globl	ufieldpositer_close_67
	.type	ufieldpositer_close_67, @function
ufieldpositer_close_67:
.LFB2015:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L7
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L7:
	ret
	.cfi_endproc
.LFE2015:
	.size	ufieldpositer_close_67, .-ufieldpositer_close_67
	.p2align 4
	.globl	ufieldpositer_next_67
	.type	ufieldpositer_next_67, @function
ufieldpositer_next_67:
.LFB2016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%r13, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movl	$0, -48(%rbp)
	movq	%rax, -64(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -56(%rbp)
	call	_ZN6icu_6721FieldPositionIterator4nextERNS_13FieldPositionE@PLT
	testb	%al, %al
	je	.L13
	movl	-56(%rbp), %r14d
	testq	%r12, %r12
	je	.L11
	movl	-52(%rbp), %eax
	movl	%eax, (%r12)
.L11:
	testq	%rbx, %rbx
	je	.L10
	movl	-48(%rbp), %eax
	movl	%eax, (%rbx)
.L10:
	movq	%r13, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$32, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movl	$-1, %r14d
	jmp	.L10
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2016:
	.size	ufieldpositer_next_67, .-ufieldpositer_next_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
