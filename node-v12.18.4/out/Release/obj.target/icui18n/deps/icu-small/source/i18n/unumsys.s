	.file	"unumsys.cpp"
	.text
	.p2align 4
	.globl	unumsys_openByName_67
	.type	unumsys_openByName_67, @function
unumsys_openByName_67:
.LFB2331:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L2
	jmp	_ZN6icu_6715NumberingSystem20createInstanceByNameEPKcR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2331:
	.size	unumsys_openByName_67, .-unumsys_openByName_67
	.p2align 4
	.globl	unumsys_close_67
	.type	unumsys_close_67, @function
unumsys_close_67:
.LFB2332:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L4
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L4:
	ret
	.cfi_endproc
.LFE2332:
	.size	unumsys_close_67, .-unumsys_close_67
	.p2align 4
	.globl	unumsys_openAvailableNames_67
	.type	unumsys_openAvailableNames_67, @function
unumsys_openAvailableNames_67:
.LFB2333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6715NumberingSystem17getAvailableNamesER10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uenum_openFromStringEnumeration_67@PLT
	.cfi_endproc
.LFE2333:
	.size	unumsys_openAvailableNames_67, .-unumsys_openAvailableNames_67
	.p2align 4
	.globl	unumsys_getName_67
	.type	unumsys_getName_67, @function
unumsys_getName_67:
.LFB2334:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6715NumberingSystem7getNameEv@PLT
	.cfi_endproc
.LFE2334:
	.size	unumsys_getName_67, .-unumsys_getName_67
	.p2align 4
	.globl	unumsys_getRadix_67
	.type	unumsys_getRadix_67, @function
unumsys_getRadix_67:
.LFB2335:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6715NumberingSystem8getRadixEv@PLT
	.cfi_endproc
.LFE2335:
	.size	unumsys_getRadix_67, .-unumsys_getRadix_67
	.p2align 4
	.globl	unumsys_isAlgorithmic_67
	.type	unumsys_isAlgorithmic_67, @function
unumsys_isAlgorithmic_67:
.LFB2336:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6715NumberingSystem13isAlgorithmicEv@PLT
	.cfi_endproc
.LFE2336:
	.size	unumsys_isAlgorithmic_67, .-unumsys_isAlgorithmic_67
	.p2align 4
	.globl	unumsys_open_67
	.type	unumsys_open_67, @function
unumsys_open_67:
.LFB2330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-256(%rbp), %r12
	movq	%r12, %rdi
	subq	$240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L14
	addq	$240, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2330:
	.size	unumsys_open_67, .-unumsys_open_67
	.p2align 4
	.globl	unumsys_getDescription_67
	.type	unumsys_getDescription_67, @function
unumsys_getDescription_67:
.LFB2337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L18
	movq	(%rdi), %rax
	movl	%edx, %r13d
	movq	%rcx, %r12
	leaq	-112(%rbp), %r14
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	*24(%rax)
	movq	%r12, %rcx
	leaq	-120(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%rbx, -120(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	addq	$96, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movl	$-1, %r12d
	jmp	.L15
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2337:
	.size	unumsys_getDescription_67, .-unumsys_getDescription_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
