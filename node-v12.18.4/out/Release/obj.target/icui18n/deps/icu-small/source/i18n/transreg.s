	.file	"transreg.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliteratorRegistry11Enumeration5countER10UErrorCode
	.type	_ZNK6icu_6722TransliteratorRegistry11Enumeration5countER10UErrorCode, @function
_ZNK6icu_6722TransliteratorRegistry11Enumeration5countER10UErrorCode:
.LFB2601:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rax
	movl	232(%rax), %eax
	ret
	.cfi_endproc
.LFE2601:
	.size	_ZNK6icu_6722TransliteratorRegistry11Enumeration5countER10UErrorCode, .-_ZNK6icu_6722TransliteratorRegistry11Enumeration5countER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry11Enumeration5resetER10UErrorCode
	.type	_ZN6icu_6722TransliteratorRegistry11Enumeration5resetER10UErrorCode, @function
_ZN6icu_6722TransliteratorRegistry11Enumeration5resetER10UErrorCode:
.LFB2603:
	.cfi_startproc
	endbr64
	movl	$0, 116(%rdi)
	ret
	.cfi_endproc
.LFE2603:
	.size	_ZN6icu_6722TransliteratorRegistry11Enumeration5resetER10UErrorCode, .-_ZN6icu_6722TransliteratorRegistry11Enumeration5resetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliteratorRegistry11Enumeration17getDynamicClassIDEv
	.type	_ZNK6icu_6722TransliteratorRegistry11Enumeration17getDynamicClassIDEv, @function
_ZNK6icu_6722TransliteratorRegistry11Enumeration17getDynamicClassIDEv:
.LFB2605:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6722TransliteratorRegistry11Enumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2605:
	.size	_ZNK6icu_6722TransliteratorRegistry11Enumeration17getDynamicClassIDEv, .-_ZNK6icu_6722TransliteratorRegistry11Enumeration17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry11Enumeration5snextER10UErrorCode
	.type	_ZN6icu_6722TransliteratorRegistry11Enumeration5snextER10UErrorCode, @function
_ZN6icu_6722TransliteratorRegistry11Enumeration5snextER10UErrorCode:
.LFB2602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L5
	movq	%rdi, %rbx
	movq	120(%rdi), %rdi
	movl	116(%rbx), %r8d
	cmpl	232(%rdi), %r8d
	jg	.L11
	jge	.L5
	leal	1(%r8), %eax
	addq	$224, %rdi
	leaq	8(%rbx), %r12
	movl	%r8d, %esi
	movl	%eax, 116(%rbx)
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L5:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$25, (%rsi)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2602:
	.size	_ZN6icu_6722TransliteratorRegistry11Enumeration5snextER10UErrorCode, .-_ZN6icu_6722TransliteratorRegistry11Enumeration5snextER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry11EnumerationD2Ev
	.type	_ZN6icu_6722TransliteratorRegistry11EnumerationD2Ev, @function
_ZN6icu_6722TransliteratorRegistry11EnumerationD2Ev:
.LFB2598:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6722TransliteratorRegistry11EnumerationE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717StringEnumerationD2Ev@PLT
	.cfi_endproc
.LFE2598:
	.size	_ZN6icu_6722TransliteratorRegistry11EnumerationD2Ev, .-_ZN6icu_6722TransliteratorRegistry11EnumerationD2Ev
	.globl	_ZN6icu_6722TransliteratorRegistry11EnumerationD1Ev
	.set	_ZN6icu_6722TransliteratorRegistry11EnumerationD1Ev,_ZN6icu_6722TransliteratorRegistry11EnumerationD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry11EnumerationD0Ev
	.type	_ZN6icu_6722TransliteratorRegistry11EnumerationD0Ev, @function
_ZN6icu_6722TransliteratorRegistry11EnumerationD0Ev:
.LFB2600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722TransliteratorRegistry11EnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2600:
	.size	_ZN6icu_6722TransliteratorRegistry11EnumerationD0Ev, .-_ZN6icu_6722TransliteratorRegistry11EnumerationD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6718TransliteratorSpec9setupNextEv.part.0, @function
_ZN6icu_6718TransliteratorSpec9setupNextEv.part.0:
.LFB3566:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	72(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	136(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	144(%rbx), %ecx
	testw	%cx, %cx
	js	.L16
	sarl	$5, %ecx
.L17:
	xorl	%edx, %edx
	movl	$95, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString13doLastIndexOfEDsii@PLT
	testl	%eax, %eax
	jle	.L18
	movzwl	144(%rbx), %edx
	testw	%dx, %dx
	js	.L19
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L20:
	cmpl	%eax, %ecx
	jbe	.L21
	cmpl	$1023, %eax
	jg	.L22
	andl	$31, %edx
	sall	$5, %eax
	orl	%edx, %eax
	movw	%ax, 144(%rbx)
.L21:
	movb	$1, 265(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	148(%rbx), %ecx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	200(%rbx), %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movl	148(%rbx), %ecx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L22:
	orl	$-32, %edx
	movl	%eax, 148(%rbx)
	movw	%dx, 144(%rbx)
	movb	$1, 265(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3566:
	.size	_ZN6icu_6718TransliteratorSpec9setupNextEv.part.0, .-_ZN6icu_6718TransliteratorSpec9setupNextEv.part.0
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3014:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3014:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3017:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L39
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L27
	cmpb	$0, 12(%rbx)
	jne	.L40
.L31:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L27:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L31
	.cfi_endproc
.LFE3017:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3020:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L43
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3020:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3023:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L46
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3023:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L52
.L48:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L53
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3025:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3026:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3026:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3027:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3027:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3028:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3028:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3029:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3029:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3030:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3030:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3031:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L69
	testl	%edx, %edx
	jle	.L69
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L72
.L61:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L61
	.cfi_endproc
.LFE3031:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L76
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L76
	testl	%r12d, %r12d
	jg	.L83
	cmpb	$0, 12(%rbx)
	jne	.L84
.L78:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L78
.L84:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3032:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L86
	movq	(%rdi), %r8
.L87:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L90
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L90
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3033:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3034:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L97
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3034:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3035:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3035:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3036:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3036:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3037:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3037:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3039:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3039:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3041:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3041:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringEPKNS_10UnicodeSetE
	.type	_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringEPKNS_10UnicodeSetE, @function
_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringEPKNS_10UnicodeSetE:
.LFB2534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	72(%rdi), %rdi
	movq	%rax, -64(%rdi)
	movl	$2, %eax
	movw	%ax, -56(%rdi)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r12, 144(%rbx)
	movq	$0, 136(%rbx)
	movq	$0, 152(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2534:
	.size	_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringEPKNS_10UnicodeSetE, .-_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringEPKNS_10UnicodeSetE
	.globl	_ZN6icu_6719TransliteratorAliasC1ERKNS_13UnicodeStringEPKNS_10UnicodeSetE
	.set	_ZN6icu_6719TransliteratorAliasC1ERKNS_13UnicodeStringEPKNS_10UnicodeSetE,_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringEPKNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringES3_PNS_7UVectorEPKNS_10UnicodeSetE
	.type	_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringES3_PNS_7UVectorEPKNS_10UnicodeSetE, @function
_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringES3_PNS_7UVectorEPKNS_10UnicodeSetE:
.LFB2537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	subq	$16, %rsp
	movq	%r8, -32(%rbp)
	movhps	-32(%rbp), %xmm0
	movaps	%xmm0, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	72(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movdqa	-32(%rbp), %xmm0
	movabsq	$4294967296, %rax
	movq	%rax, 152(%rbx)
	movups	%xmm0, 136(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2537:
	.size	_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringES3_PNS_7UVectorEPKNS_10UnicodeSetE, .-_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringES3_PNS_7UVectorEPKNS_10UnicodeSetE
	.globl	_ZN6icu_6719TransliteratorAliasC1ERKNS_13UnicodeStringES3_PNS_7UVectorEPKNS_10UnicodeSetE
	.set	_ZN6icu_6719TransliteratorAliasC1ERKNS_13UnicodeStringES3_PNS_7UVectorEPKNS_10UnicodeSetE,_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringES3_PNS_7UVectorEPKNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringES3_15UTransDirection
	.type	_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringES3_15UTransDirection, @function
_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringES3_15UTransDirection:
.LFB2540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	72(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	pxor	%xmm0, %xmm0
	movl	%r12d, 152(%rbx)
	movl	$2, 156(%rbx)
	movups	%xmm0, 136(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2540:
	.size	_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringES3_15UTransDirection, .-_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringES3_15UTransDirection
	.globl	_ZN6icu_6719TransliteratorAliasC1ERKNS_13UnicodeStringES3_15UTransDirection
	.set	_ZN6icu_6719TransliteratorAliasC1ERKNS_13UnicodeStringES3_15UTransDirection,_ZN6icu_6719TransliteratorAliasC2ERKNS_13UnicodeStringES3_15UTransDirection
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719TransliteratorAliasD2Ev
	.type	_ZN6icu_6719TransliteratorAliasD2Ev, @function
_ZN6icu_6719TransliteratorAliasD2Ev:
.LFB2543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	136(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L110
	movq	(%rdi), %rax
	call	*8(%rax)
.L110:
	leaq	72(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE2543:
	.size	_ZN6icu_6719TransliteratorAliasD2Ev, .-_ZN6icu_6719TransliteratorAliasD2Ev
	.globl	_ZN6icu_6719TransliteratorAliasD1Ev
	.set	_ZN6icu_6719TransliteratorAliasD1Ev,_ZN6icu_6719TransliteratorAliasD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719TransliteratorAlias11isRuleBasedEv
	.type	_ZNK6icu_6719TransliteratorAlias11isRuleBasedEv, @function
_ZNK6icu_6719TransliteratorAlias11isRuleBasedEv:
.LFB2546:
	.cfi_startproc
	endbr64
	cmpl	$2, 156(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE2546:
	.size	_ZNK6icu_6719TransliteratorAlias11isRuleBasedEv, .-_ZNK6icu_6719TransliteratorAlias11isRuleBasedEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719TransliteratorAlias5parseERNS_20TransliteratorParserER11UParseErrorR10UErrorCode
	.type	_ZNK6icu_6719TransliteratorAlias5parseERNS_20TransliteratorParserER11UParseErrorR10UErrorCode, @function
_ZNK6icu_6719TransliteratorAlias5parseERNS_20TransliteratorParserER11UParseErrorR10UErrorCode:
.LFB2547:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	movq	%rsi, %r9
	testl	%eax, %eax
	jle	.L118
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	movl	152(%rdi), %r10d
	leaq	72(%rdi), %rsi
	movq	%rcx, %r8
	movq	%r9, %rdi
	movq	%rdx, %rcx
	movl	%r10d, %edx
	jmp	_ZN6icu_6720TransliteratorParser5parseERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode@PLT
	.cfi_endproc
.LFE2547:
	.size	_ZNK6icu_6719TransliteratorAlias5parseERNS_20TransliteratorParserER11UParseErrorR10UErrorCode, .-_ZNK6icu_6719TransliteratorAlias5parseERNS_20TransliteratorParserER11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TransliteratorSpecD2Ev
	.type	_ZN6icu_6718TransliteratorSpecD2Ev, @function
_ZN6icu_6718TransliteratorSpecD2Ev:
.LFB2554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	272(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L120
	movq	(%rdi), %rax
	call	*8(%rax)
.L120:
	leaq	200(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE2554:
	.size	_ZN6icu_6718TransliteratorSpecD2Ev, .-_ZN6icu_6718TransliteratorSpecD2Ev
	.globl	_ZN6icu_6718TransliteratorSpecD1Ev
	.set	_ZN6icu_6718TransliteratorSpecD1Ev,_ZN6icu_6718TransliteratorSpecD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718TransliteratorSpec11hasFallbackEv
	.type	_ZNK6icu_6718TransliteratorSpec11hasFallbackEv, @function
_ZNK6icu_6718TransliteratorSpec11hasFallbackEv:
.LFB2556:
	.cfi_startproc
	endbr64
	movswl	144(%rdi), %eax
	testw	%ax, %ax
	js	.L126
	sarl	$5, %eax
	testl	%eax, %eax
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	movl	148(%rdi), %eax
	testl	%eax, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE2556:
	.size	_ZNK6icu_6718TransliteratorSpec11hasFallbackEv, .-_ZNK6icu_6718TransliteratorSpec11hasFallbackEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TransliteratorSpec5resetEv
	.type	_ZN6icu_6718TransliteratorSpec5resetEv, @function
_ZN6icu_6718TransliteratorSpec5resetEv:
.LFB2557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$72, %rdi
	subq	$24, %rsp
	movswl	16(%r12), %eax
	movswl	80(%r12), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L129
	testw	%dx, %dx
	js	.L130
	sarl	$5, %edx
	testw	%ax, %ax
	js	.L132
.L156:
	sarl	$5, %eax
.L133:
	cmpl	%edx, %eax
	jne	.L134
	testb	%cl, %cl
	je	.L154
.L134:
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	272(%r12), %rax
	movb	$0, 265(%r12)
	testq	%rax, %rax
	setne	264(%r12)
	jne	.L155
	movzwl	144(%r12), %eax
	testb	$1, %al
	je	.L137
	addq	$24, %rsp
	leaq	136(%r12), %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movq	%rsi, -32(%rbp)
	movq	%rdi, -24(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-32(%rbp), %rsi
	movq	-24(%rbp), %rdi
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L129:
	testb	%cl, %cl
	je	.L134
.L128:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movl	84(%r12), %edx
	testw	%ax, %ax
	jns	.L156
.L132:
	movl	20(%r12), %eax
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L155:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6718TransliteratorSpec9setupNextEv.part.0
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L138
	movswl	%ax, %edx
	sarl	$5, %edx
.L139:
	testl	%edx, %edx
	je	.L128
	andl	$31, %eax
	movw	%ax, 144(%r12)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L138:
	movl	148(%r12), %edx
	jmp	.L139
	.cfi_endproc
.LFE2557:
	.size	_ZN6icu_6718TransliteratorSpec5resetEv, .-_ZN6icu_6718TransliteratorSpec5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TransliteratorSpec9setupNextEv
	.type	_ZN6icu_6718TransliteratorSpec9setupNextEv, @function
_ZN6icu_6718TransliteratorSpec9setupNextEv:
.LFB2558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	136(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 264(%rdi)
	movq	%rdi, %rbx
	movb	$0, 265(%rdi)
	je	.L158
	leaq	72(%rdi), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	144(%rbx), %ecx
	testw	%cx, %cx
	js	.L159
	sarl	$5, %ecx
.L160:
	xorl	%edx, %edx
	movl	$95, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString13doLastIndexOfEDsii@PLT
	testl	%eax, %eax
	jle	.L161
	movzwl	144(%rbx), %edx
	testw	%dx, %dx
	js	.L162
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L163:
	cmpl	%eax, %ecx
	jbe	.L164
	cmpl	$1023, %eax
	jg	.L165
	andl	$31, %edx
	sall	$5, %eax
	orl	%edx, %eax
	movw	%ax, 144(%rbx)
.L164:
	movb	$1, 265(%rbx)
.L157:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movzwl	144(%rdi), %eax
	testb	$1, %al
	je	.L167
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L168
	movswl	%ax, %edx
	sarl	$5, %edx
.L169:
	testl	%edx, %edx
	je	.L157
	andl	$31, %eax
	movw	%ax, 144(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movl	148(%rbx), %ecx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	200(%rbx), %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movl	148(%rbx), %ecx
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L168:
	movl	148(%rdi), %edx
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L165:
	orl	$-32, %edx
	movl	%eax, 148(%rbx)
	movw	%dx, 144(%rbx)
	jmp	.L164
	.cfi_endproc
.LFE2558:
	.size	_ZN6icu_6718TransliteratorSpec9setupNextEv, .-_ZN6icu_6718TransliteratorSpec9setupNextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TransliteratorSpec4nextEv
	.type	_ZN6icu_6718TransliteratorSpec4nextEv, @function
_ZN6icu_6718TransliteratorSpec4nextEv:
.LFB2559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	136(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	72(%rdi), %r13
	movq	%r14, %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzbl	265(%r12), %eax
	movb	$0, 265(%r12)
	movb	%al, 264(%r12)
	testb	%al, %al
	je	.L175
	movq	%r12, %rdi
	call	_ZN6icu_6718TransliteratorSpec9setupNextEv.part.0
.L174:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movzwl	144(%r12), %eax
	testb	$1, %al
	je	.L177
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L178
	movswl	%ax, %edx
	sarl	$5, %edx
.L179:
	testl	%edx, %edx
	je	.L174
	andl	$31, %eax
	movw	%ax, 144(%r12)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movl	148(%r12), %edx
	jmp	.L179
	.cfi_endproc
.LFE2559:
	.size	_ZN6icu_6718TransliteratorSpec4nextEv, .-_ZN6icu_6718TransliteratorSpec4nextEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718TransliteratorSpec3getEv
	.type	_ZNK6icu_6718TransliteratorSpec3getEv, @function
_ZNK6icu_6718TransliteratorSpec3getEv:
.LFB2560:
	.cfi_startproc
	endbr64
	leaq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE2560:
	.size	_ZNK6icu_6718TransliteratorSpec3getEv, .-_ZNK6icu_6718TransliteratorSpec3getEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718TransliteratorSpec8isLocaleEv
	.type	_ZNK6icu_6718TransliteratorSpec8isLocaleEv, @function
_ZNK6icu_6718TransliteratorSpec8isLocaleEv:
.LFB2561:
	.cfi_startproc
	endbr64
	movzbl	264(%rdi), %eax
	ret
	.cfi_endproc
.LFE2561:
	.size	_ZNK6icu_6718TransliteratorSpec8isLocaleEv, .-_ZNK6icu_6718TransliteratorSpec8isLocaleEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718TransliteratorSpec9getBundleEv
	.type	_ZNK6icu_6718TransliteratorSpec9getBundleEv, @function
_ZNK6icu_6718TransliteratorSpec9getBundleEv:
.LFB2562:
	.cfi_startproc
	endbr64
	movq	272(%rdi), %rax
	ret
	.cfi_endproc
.LFE2562:
	.size	_ZNK6icu_6718TransliteratorSpec9getBundleEv, .-_ZNK6icu_6718TransliteratorSpec9getBundleEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719TransliteratorEntryC2Ev
	.type	_ZN6icu_6719TransliteratorEntryC2Ev, @function
_ZN6icu_6719TransliteratorEntryC2Ev:
.LFB2564:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$8, (%rdi)
	movq	%rax, 8(%rdi)
	movl	$2, %eax
	movw	%ax, 16(%rdi)
	movups	%xmm0, 80(%rdi)
	ret
	.cfi_endproc
.LFE2564:
	.size	_ZN6icu_6719TransliteratorEntryC2Ev, .-_ZN6icu_6719TransliteratorEntryC2Ev
	.globl	_ZN6icu_6719TransliteratorEntryC1Ev
	.set	_ZN6icu_6719TransliteratorEntryC1Ev,_ZN6icu_6719TransliteratorEntryC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719TransliteratorEntry14adoptPrototypeEPNS_14TransliteratorE
	.type	_ZN6icu_6719TransliteratorEntry14adoptPrototypeEPNS_14TransliteratorE, @function
_ZN6icu_6719TransliteratorEntry14adoptPrototypeEPNS_14TransliteratorE:
.LFB2569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpl	$3, (%rdi)
	movq	%rdi, %rbx
	je	.L194
.L189:
	movq	%r12, 88(%rbx)
	movl	$3, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movq	88(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L189
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r12, 88(%rbx)
	movl	$3, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2569:
	.size	_ZN6icu_6719TransliteratorEntry14adoptPrototypeEPNS_14TransliteratorE, .-_ZN6icu_6719TransliteratorEntry14adoptPrototypeEPNS_14TransliteratorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719TransliteratorEntry10setFactoryEPFPNS_14TransliteratorERKNS_13UnicodeStringENS1_5TokenEES6_
	.type	_ZN6icu_6719TransliteratorEntry10setFactoryEPFPNS_14TransliteratorERKNS_13UnicodeStringENS1_5TokenEES6_, @function
_ZN6icu_6719TransliteratorEntry10setFactoryEPFPNS_14TransliteratorERKNS_13UnicodeStringENS1_5TokenEES6_:
.LFB2570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$3, (%rdi)
	je	.L201
.L196:
	movq	%r13, 88(%rbx)
	movq	%r12, 96(%rbx)
	movl	$7, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movq	88(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L196
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L196
	.cfi_endproc
.LFE2570:
	.size	_ZN6icu_6719TransliteratorEntry10setFactoryEPFPNS_14TransliteratorERKNS_13UnicodeStringENS1_5TokenEES6_, .-_ZN6icu_6719TransliteratorEntry10setFactoryEPFPNS_14TransliteratorERKNS_13UnicodeStringENS1_5TokenEES6_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistryC2ER10UErrorCode
	.type	_ZN6icu_6722TransliteratorRegistryC2ER10UErrorCode, @function
_ZN6icu_6722TransliteratorRegistryC2ER10UErrorCode:
.LFB2573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	$0, 8(%rdi)
	movl	(%rsi), %edi
	testl	%edi, %edi
	jle	.L212
	movq	uhash_compareCaselessUnicodeString_67@GOTPCREL(%rip), %r13
.L203:
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rax
	movq	$0, 96(%rbx)
	movq	%rax, -56(%rbp)
.L205:
	leaq	184(%rbx), %r15
	movq	%r12, %rdx
	movl	$11, %esi
	movq	%r15, %rdi
	leaq	224(%rbx), %r14
	call	_ZN6icu_677UVectorC1EiR10UErrorCode@PLT
	movq	%r12, %rdx
	movl	$641, %esi
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorC1EiR10UErrorCode@PLT
	movq	8(%rbx), %rdi
	leaq	deleteEntry(%rip), %rsi
	call	uhash_setValueDeleter_67@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_677UVector11setComparerEPFa8UElementS1_E@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L207
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, (%rsi)
	movl	$2, %eax
	movw	%ax, 8(%rsi)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L207:
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_677UVector11setComparerEPFa8UElementS1_E@PLT
	movq	96(%rbx), %rdi
	movq	uhash_deleteHashtable_67@GOTPCREL(%rip), %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uhash_setValueDeleter_67@PLT
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movq	uhash_compareCaselessUnicodeString_67@GOTPCREL(%rip), %r13
	leaq	16(%rbx), %r14
	movq	%rsi, %r8
	xorl	%ecx, %ecx
	movq	uhash_hashCaselessUnicodeString_67@GOTPCREL(%rip), %r15
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	uhash_init_67@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L203
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rax
	movq	%r14, 8(%rbx)
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	call	uhash_setKeyDeleter_67@PLT
	movl	(%r12), %ecx
	movq	$0, 96(%rbx)
	testl	%ecx, %ecx
	jg	.L205
	leaq	104(%rbx), %r14
	movq	%r13, %rdx
	movq	%r12, %r9
	movl	$149, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	uhash_initSize_67@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L205
	movq	%r14, 96(%rbx)
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	call	uhash_setKeyDeleter_67@PLT
	jmp	.L205
	.cfi_endproc
.LFE2573:
	.size	_ZN6icu_6722TransliteratorRegistryC2ER10UErrorCode, .-_ZN6icu_6722TransliteratorRegistryC2ER10UErrorCode
	.globl	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode
	.set	_ZN6icu_6722TransliteratorRegistryC1ER10UErrorCode,_ZN6icu_6722TransliteratorRegistryC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliteratorRegistry17countAvailableIDsEv
	.type	_ZNK6icu_6722TransliteratorRegistry17countAvailableIDsEv, @function
_ZNK6icu_6722TransliteratorRegistry17countAvailableIDsEv:
.LFB2585:
	.cfi_startproc
	endbr64
	movl	232(%rdi), %eax
	ret
	.cfi_endproc
.LFE2585:
	.size	_ZNK6icu_6722TransliteratorRegistry17countAvailableIDsEv, .-_ZNK6icu_6722TransliteratorRegistry17countAvailableIDsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliteratorRegistry14getAvailableIDEi
	.type	_ZNK6icu_6722TransliteratorRegistry14getAvailableIDEi, @function
_ZNK6icu_6722TransliteratorRegistry14getAvailableIDEi:
.LFB2586:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	js	.L217
	cmpl	232(%rdi), %esi
	movl	$0, %eax
	cmovge	%eax, %esi
	addq	$224, %rdi
	jmp	_ZNK6icu_677UVector9elementAtEi@PLT
	.p2align 4,,10
	.p2align 3
.L217:
	xorl	%esi, %esi
	addq	$224, %rdi
	jmp	_ZNK6icu_677UVector9elementAtEi@PLT
	.cfi_endproc
.LFE2586:
	.size	_ZNK6icu_6722TransliteratorRegistry14getAvailableIDEi, .-_ZNK6icu_6722TransliteratorRegistry14getAvailableIDEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliteratorRegistry15getAvailableIDsEv
	.type	_ZNK6icu_6722TransliteratorRegistry15getAvailableIDsEv, @function
_ZNK6icu_6722TransliteratorRegistry15getAvailableIDsEv:
.LFB2587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$128, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L218
	movq	%rax, %rdi
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	leaq	16+_ZTVN6icu_6722TransliteratorRegistry11EnumerationE(%rip), %rax
	movq	%rbx, 120(%r12)
	movq	%rax, (%r12)
	movl	$0, 116(%r12)
.L218:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2587:
	.size	_ZNK6icu_6722TransliteratorRegistry15getAvailableIDsEv, .-_ZNK6icu_6722TransliteratorRegistry15getAvailableIDsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliteratorRegistry21countAvailableSourcesEv
	.type	_ZNK6icu_6722TransliteratorRegistry21countAvailableSourcesEv, @function
_ZNK6icu_6722TransliteratorRegistry21countAvailableSourcesEv:
.LFB2588:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rdi
	jmp	uhash_count_67@PLT
	.cfi_endproc
.LFE2588:
	.size	_ZNK6icu_6722TransliteratorRegistry21countAvailableSourcesEv, .-_ZNK6icu_6722TransliteratorRegistry21countAvailableSourcesEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliteratorRegistry18getAvailableSourceEiRNS_13UnicodeStringE
	.type	_ZNK6icu_6722TransliteratorRegistry18getAvailableSourceEiRNS_13UnicodeStringE, @function
_ZNK6icu_6722TransliteratorRegistry18getAvailableSourceEiRNS_13UnicodeStringE:
.LFB2589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-44(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$-1, -44(%rbp)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L246:
	movq	96(%r14), %rdi
	movq	%r13, %rsi
	subl	$1, %ebx
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	je	.L230
.L227:
	testl	%ebx, %ebx
	jns	.L246
	testq	%rax, %rax
	je	.L230
	movq	16(%rax), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L232:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L247
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L248
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L248:
	testw	%ax, %ax
	js	.L233
	movswl	%ax, %edx
	sarl	$5, %edx
.L234:
	testl	%edx, %edx
	je	.L232
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L232
.L233:
	movl	12(%r12), %edx
	jmp	.L234
.L247:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2589:
	.size	_ZNK6icu_6722TransliteratorRegistry18getAvailableSourceEiRNS_13UnicodeStringE, .-_ZNK6icu_6722TransliteratorRegistry18getAvailableSourceEiRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliteratorRegistry21countAvailableTargetsERKNS_13UnicodeStringE
	.type	_ZNK6icu_6722TransliteratorRegistry21countAvailableTargetsERKNS_13UnicodeStringE, @function
_ZNK6icu_6722TransliteratorRegistry21countAvailableTargetsERKNS_13UnicodeStringE:
.LFB2590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	96(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L250
	movq	(%rax), %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uhash_count_67@PLT
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2590:
	.size	_ZNK6icu_6722TransliteratorRegistry21countAvailableTargetsERKNS_13UnicodeStringE, .-_ZNK6icu_6722TransliteratorRegistry21countAvailableTargetsERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliteratorRegistry18getAvailableTargetEiRKNS_13UnicodeStringERS1_
	.type	_ZNK6icu_6722TransliteratorRegistry18getAvailableTargetEiRKNS_13UnicodeStringERS1_, @function
_ZNK6icu_6722TransliteratorRegistry18getAvailableTargetEiRKNS_13UnicodeStringERS1_:
.LFB2591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	96(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L283
	movq	%rax, %rbx
	movl	$-1, -44(%rbp)
	xorl	%eax, %eax
	leaq	-44(%rbp), %r12
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L284:
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	subl	$1, %r14d
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	je	.L283
.L260:
	testl	%r14d, %r14d
	jns	.L284
	testq	%rax, %rax
	je	.L283
	movq	16(%rax), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L255:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L285
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movzwl	8(%r13), %eax
	testb	$1, %al
	jne	.L286
	testw	%ax, %ax
	js	.L265
	movswl	%ax, %edx
	sarl	$5, %edx
.L266:
	testl	%edx, %edx
	je	.L255
	andl	$31, %eax
	movw	%ax, 8(%r13)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L286:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L255
.L265:
	movl	12(%r13), %edx
	jmp	.L266
.L285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2591:
	.size	_ZNK6icu_6722TransliteratorRegistry18getAvailableTargetEiRKNS_13UnicodeStringERS1_, .-_ZNK6icu_6722TransliteratorRegistry18getAvailableTargetEiRKNS_13UnicodeStringERS1_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliteratorRegistry22countAvailableVariantsERKNS_13UnicodeStringES3_
	.type	_ZNK6icu_6722TransliteratorRegistry22countAvailableVariantsERKNS_13UnicodeStringES3_, @function
_ZNK6icu_6722TransliteratorRegistry22countAvailableVariantsERKNS_13UnicodeStringES3_:
.LFB2592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movq	96(%rdi), %rdi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L288
	movq	(%rax), %rdi
	movq	%r12, %rsi
	call	uhash_geti_67@PLT
	testl	%eax, %eax
	je	.L288
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L291:
	movl	%eax, %edx
	andl	$1, %edx
	cmpl	$1, %edx
	sbbl	$-1, %r8d
	shrl	%eax
	jne	.L291
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2592:
	.size	_ZNK6icu_6722TransliteratorRegistry22countAvailableVariantsERKNS_13UnicodeStringES3_, .-_ZNK6icu_6722TransliteratorRegistry22countAvailableVariantsERKNS_13UnicodeStringES3_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliteratorRegistry19getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_
	.type	_ZNK6icu_6722TransliteratorRegistry19getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_, @function
_ZNK6icu_6722TransliteratorRegistry19getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_:
.LFB2593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	96(%rdi), %rdi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L338
	movq	(%rax), %rdi
	movq	%r14, %rsi
	call	uhash_geti_67@PLT
	testl	%eax, %eax
	je	.L338
	xorl	%esi, %esi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L314:
	testb	$1, %al
	je	.L312
	cmpl	%edx, %r13d
	je	.L339
	addl	$1, %edx
.L312:
	addl	$1, %esi
	shrl	%eax
	jne	.L314
.L338:
	movzwl	8(%r12), %eax
	testb	$1, %al
	jne	.L340
	testw	%ax, %ax
	js	.L316
	movswl	%ax, %edx
	sarl	$5, %edx
.L317:
	testl	%edx, %edx
	je	.L307
	andl	$31, %eax
	movw	%ax, 8(%r12)
.L307:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	leaq	184(%rbx), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	testq	%rax, %rax
	je	.L338
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L316:
	movl	12(%r12), %edx
	jmp	.L317
	.cfi_endproc
.LFE2593:
	.size	_ZNK6icu_6722TransliteratorRegistry19getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_, .-_ZNK6icu_6722TransliteratorRegistry19getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry11EnumerationC2ERKS0_
	.type	_ZN6icu_6722TransliteratorRegistry11EnumerationC2ERKS0_, @function
_ZN6icu_6722TransliteratorRegistry11EnumerationC2ERKS0_:
.LFB2595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	leaq	16+_ZTVN6icu_6722TransliteratorRegistry11EnumerationE(%rip), %rax
	movq	%r12, 120(%rbx)
	movq	%rax, (%rbx)
	movl	$0, 116(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2595:
	.size	_ZN6icu_6722TransliteratorRegistry11EnumerationC2ERKS0_, .-_ZN6icu_6722TransliteratorRegistry11EnumerationC2ERKS0_
	.globl	_ZN6icu_6722TransliteratorRegistry11EnumerationC1ERKS0_
	.set	_ZN6icu_6722TransliteratorRegistry11EnumerationC1ERKS0_,_ZN6icu_6722TransliteratorRegistry11EnumerationC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry11Enumeration16getStaticClassIDEv
	.type	_ZN6icu_6722TransliteratorRegistry11Enumeration16getStaticClassIDEv, @function
_ZN6icu_6722TransliteratorRegistry11Enumeration16getStaticClassIDEv:
.LFB2604:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6722TransliteratorRegistry11Enumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2604:
	.size	_ZN6icu_6722TransliteratorRegistry11Enumeration16getStaticClassIDEv, .-_ZN6icu_6722TransliteratorRegistry11Enumeration16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry11registerSTVERKNS_13UnicodeStringES3_S3_
	.type	_ZN6icu_6722TransliteratorRegistry11registerSTVERKNS_13UnicodeStringES3_S3_, @function
_ZN6icu_6722TransliteratorRegistry11registerSTVERKNS_13UnicodeStringES3_S3_:
.LFB2609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	96(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	call	uhash_get_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L376
.L345:
	leaq	184(%r12), %r8
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	%r8, -72(%rbp)
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	movq	-72(%rbp), %r8
	testl	%eax, %eax
	movl	%eax, %r13d
	jns	.L377
	cmpl	$30, 192(%r12)
	movq	%r8, -80(%rbp)
	jle	.L378
.L344:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L379
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_restore_state
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L344
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-80(%rbp), %r8
	leaq	-60(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%rax, -72(%rbp)
	movq	%r8, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L344
	movl	192(%r12), %r13d
	subl	$1, %r13d
	jns	.L353
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L377:
	leaq	-60(%rbp), %rax
	movq	%rax, -72(%rbp)
.L353:
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	uhash_geti_67@PLT
	movl	$64, %edi
	btsl	%r13d, %eax
	movl	%eax, %r13d
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L354
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L354:
	movq	-72(%rbp), %rcx
	movq	(%rbx), %rdi
	movl	%r13d, %edx
	movq	%r12, %rsi
	call	uhash_puti_67@PLT
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L376:
	movswl	8(%r13), %edx
	testw	%dx, %dx
	js	.L346
	sarl	$5, %edx
.L347:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	_ZL3ANY(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZL3ANY(%rip), %rdx
	movl	$125, %r8d
	testb	%al, %al
	jne	.L380
.L348:
	movl	$88, %edi
	movl	%r8d, -72(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L344
	movl	-60(%rbp), %esi
	movl	-72(%rbp), %r8d
	movq	$0, (%rax)
	testl	%esi, %esi
	jg	.L344
	leaq	8(%rax), %rdi
	movq	uhash_compareCaselessUnicodeString_67@GOTPCREL(%rip), %rdx
	leaq	-60(%rbp), %rax
	xorl	%ecx, %ecx
	movq	uhash_hashCaselessUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%rax, %r9
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	uhash_initSize_67@PLT
	movl	-60(%rbp), %ecx
	movq	-80(%rbp), %rdi
	testl	%ecx, %ecx
	jg	.L344
	movq	%rdi, (%rbx)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	call	uhash_setKeyDeleter_67@PLT
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jg	.L344
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L352
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-80(%rbp), %r8
.L352:
	movq	96(%r12), %rdi
	movq	-72(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r8, %rsi
	call	uhash_put_67@PLT
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L346:
	movl	12(%r13), %edx
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L380:
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L349
	movswl	%ax, %edx
	sarl	$5, %edx
.L350:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	_ZL3LAT(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZL3LAT(%rip), %rdx
	cmpb	$1, %al
	sbbl	%r8d, %r8d
	andl	$20, %r8d
	addl	$3, %r8d
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L349:
	movl	12(%r13), %edx
	jmp	.L350
.L379:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2609:
	.size	_ZN6icu_6722TransliteratorRegistry11registerSTVERKNS_13UnicodeStringES3_S3_, .-_ZN6icu_6722TransliteratorRegistry11registerSTVERKNS_13UnicodeStringES3_S3_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry9removeSTVERKNS_13UnicodeStringES3_S3_
	.type	_ZN6icu_6722TransliteratorRegistry9removeSTVERKNS_13UnicodeStringES3_S3_, @function
_ZN6icu_6722TransliteratorRegistry9removeSTVERKNS_13UnicodeStringES3_S3_:
.LFB2610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	96(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L381
	movq	(%rax), %rdi
	movq	%r14, %rsi
	movq	%rax, %rbx
	call	uhash_geti_67@PLT
	testl	%eax, %eax
	movl	%eax, -68(%rbp)
	jne	.L397
.L381:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L398
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L397:
	.cfi_restore_state
	xorl	%edx, %edx
	leaq	184(%r12), %rdi
	movq	%r15, %rsi
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	testl	%eax, %eax
	js	.L381
	movl	-68(%rbp), %ecx
	btrl	%eax, %ecx
	movl	%ecx, %r15d
	testl	%ecx, %ecx
	je	.L383
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L384
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L384:
	movq	(%rbx), %rdi
	leaq	-60(%rbp), %rcx
	movl	%r15d, %edx
	movq	%r12, %rsi
	call	uhash_puti_67@PLT
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L383:
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	uhash_remove_67@PLT
	movq	(%rbx), %rdi
	call	uhash_count_67@PLT
	testl	%eax, %eax
	jne	.L381
	movq	96(%r12), %rdi
	movq	%r13, %rsi
	call	uhash_remove_67@PLT
	jmp	.L381
.L398:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2610:
	.size	_ZN6icu_6722TransliteratorRegistry9removeSTVERKNS_13UnicodeStringES3_S3_, .-_ZN6icu_6722TransliteratorRegistry9removeSTVERKNS_13UnicodeStringES3_S3_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry6removeERKNS_13UnicodeStringE
	.type	_ZN6icu_6722TransliteratorRegistry6removeERKNS_13UnicodeStringE, @function
_ZN6icu_6722TransliteratorRegistry6removeERKNS_13UnicodeStringE:
.LFB2584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movl	$2, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	leaq	-321(%rbp), %r8
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-256(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%r12, %rsi
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$2, %eax
	movw	%dx, -248(%rbp)
	movq	%r13, %rdx
	movw	%cx, -184(%rbp)
	movq	%r14, %rcx
	movw	%ax, -312(%rbp)
	movq	%r15, -320(%rbp)
	movq	%r15, -256(%rbp)
	movq	%r15, -192(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser7IDtoSTVERKNS_13UnicodeStringERS1_S4_S4_Ra@PLT
	movq	%r15, -128(%rbp)
	leaq	-128(%rbp), %r15
	movq	%r14, %rdx
	movl	$2, %esi
	movq	%r15, %rcx
	movq	%r12, %rdi
	movw	%si, -120(%rbp)
	movq	%r13, %rsi
	call	_ZN6icu_6722TransliteratorIDParser7STVtoIDERKNS_13UnicodeStringES3_S3_RS1_@PLT
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	call	uhash_remove_67@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6722TransliteratorRegistry9removeSTVERKNS_13UnicodeStringES3_S3_
	movq	%r15, %rsi
	leaq	224(%rbx), %rdi
	call	_ZN6icu_677UVector13removeElementEPv@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L402
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L402:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2584:
	.size	_ZN6icu_6722TransliteratorRegistry6removeERKNS_13UnicodeStringE, .-_ZN6icu_6722TransliteratorRegistry6removeERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_S3_PNS_19TransliteratorEntryEa
	.type	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_S3_PNS_19TransliteratorEntryEa, @function
_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_S3_PNS_19TransliteratorEntryEa:
.LFB2608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$64, %edi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	16(%rbp), %eax
	movq	%rdx, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%eax, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L404
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-96(%rbp), %r8
.L404:
	movq	8(%r12), %rdi
	leaq	-60(%rbp), %rbx
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%rbx, %rcx
	leaq	224(%r12), %r15
	call	uhash_put_67@PLT
	cmpb	$0, -84(%rbp)
	movq	-80(%rbp), %rdx
	movq	%r14, %rcx
	movq	-72(%rbp), %rsi
	movq	%r12, %rdi
	je	.L405
	call	_ZN6icu_6722TransliteratorRegistry11registerSTVERKNS_13UnicodeStringES3_S3_
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	testl	%eax, %eax
	js	.L417
.L403:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L418
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore_state
	call	_ZN6icu_6722TransliteratorRegistry9removeSTVERKNS_13UnicodeStringES3_S3_
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_677UVector13removeElementEPv@PLT
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L417:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*56(%rax)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L403
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	jmp	.L403
.L418:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2608:
	.size	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_S3_PNS_19TransliteratorEntryEa, .-_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_S3_PNS_19TransliteratorEntryEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_PNS_19TransliteratorEntryEa
	.type	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_PNS_19TransliteratorEntryEa, @function
_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_PNS_19TransliteratorEntryEa:
.LFB2606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	movl	$2, %ecx
	subq	$200, %rsp
	movq	%rdi, -216(%rbp)
	movq	%r13, %rdi
	movl	%r9d, -220(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rsi, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movswl	-120(%rbp), %eax
	movq	-232(%rbp), %r10
	testw	%ax, %ax
	js	.L420
	sarl	$5, %eax
	testl	%eax, %eax
	je	.L425
.L422:
	leaq	-192(%rbp), %r14
	movq	%r10, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rcx
	call	_ZN6icu_6722TransliteratorIDParser7STVtoIDERKNS_13UnicodeStringES3_S3_RS1_@PLT
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%rbx, %r8
	movsbl	-220(%rbp), %r9d
	movq	-216(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r14, %rsi
	pushq	%r9
	movq	%r15, %r9
	call	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_S3_PNS_19TransliteratorEntryEa
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L426
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	jne	.L422
.L425:
	leaq	_ZL3ANY(%rip), %rax
	leaq	-200(%rbp), %rdx
	movl	$3, %ecx
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%r10, -232(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	-232(%rbp), %r10
	jmp	.L422
.L426:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2606:
	.size	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_PNS_19TransliteratorEntryEa, .-_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_PNS_19TransliteratorEntryEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryEa
	.type	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryEa, @function
_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryEa:
.LFB2607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	leaq	-321(%rbp), %r8
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-256(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	movl	$2, %ecx
	movsbl	%bl, %ebx
	subq	$312, %rsp
	movq	%rdi, -352(%rbp)
	movq	%rsi, %rdi
	movq	%r12, %rsi
	movq	%rdx, -344(%rbp)
	movl	$2, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$2, %eax
	movw	%dx, -248(%rbp)
	movq	%r13, %rdx
	movw	%cx, -184(%rbp)
	movq	%r14, %rcx
	movw	%ax, -312(%rbp)
	movq	%r15, -320(%rbp)
	movq	%r15, -256(%rbp)
	movq	%r15, -192(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser7IDtoSTVERKNS_13UnicodeStringERS1_S4_S4_Ra@PLT
	movq	%r15, -128(%rbp)
	leaq	-128(%rbp), %r15
	movq	%r14, %rdx
	movl	$2, %esi
	movq	%r15, %rcx
	movq	%r12, %rdi
	movw	%si, -120(%rbp)
	movq	%r13, %rsi
	call	_ZN6icu_6722TransliteratorIDParser7STVtoIDERKNS_13UnicodeStringES3_S3_RS1_@PLT
	subq	$8, %rsp
	movq	%r14, %r8
	movq	%r13, %rcx
	pushq	%rbx
	movq	-352(%rbp), %r10
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	-344(%rbp), %r9
	movq	%r10, %rdi
	call	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_S3_PNS_19TransliteratorEntryEa
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	popq	%rdi
	popq	%r8
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L430
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L430:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2607:
	.size	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryEa, .-_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry3putEPNS_14TransliteratorEaR10UErrorCode
	.type	_ZN6icu_6722TransliteratorRegistry3putEPNS_14TransliteratorEaR10UErrorCode, @function
_ZN6icu_6722TransliteratorRegistry3putEPNS_14TransliteratorEaR10UErrorCode:
.LFB2580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$104, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L432
	movq	%rax, %r12
	pxor	%xmm0, %xmm0
	movl	$2, %edx
	movq	%r15, %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movups	%xmm0, 80(%r12)
	movq	%rax, 8(%r12)
	movq	(%r15), %rax
	movw	%dx, 16(%r12)
	movq	%r15, 88(%r12)
	movl	$3, (%r12)
	call	*104(%rax)
	addq	$8, %rsp
	movsbl	%bl, %ecx
	movq	%r12, %rdx
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryEa
.L432:
	.cfi_restore_state
	movl	$7, (%r14)
	popq	%rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2580:
	.size	_ZN6icu_6722TransliteratorRegistry3putEPNS_14TransliteratorEaR10UErrorCode, .-_ZN6icu_6722TransliteratorRegistry3putEPNS_14TransliteratorEaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringEPFPNS_14TransliteratorES3_NS4_5TokenEES6_aR10UErrorCode
	.type	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringEPFPNS_14TransliteratorES3_NS4_5TokenEES6_aR10UErrorCode, @function
_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringEPFPNS_14TransliteratorES3_NS4_5TokenEES6_aR10UErrorCode:
.LFB2581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$104, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	movq	%r9, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L436
	movq	%rax, %rdx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movsbl	%bl, %ecx
	movq	%r14, %rsi
	movq	%rax, 8(%rdx)
	pxor	%xmm0, %xmm0
	movl	$2, %eax
	movq	%r15, %rdi
	movups	%xmm0, 80(%rdx)
	movw	%ax, 16(%rdx)
	movl	$7, (%rdx)
	movq	%r13, 88(%rdx)
	movq	%r12, 96(%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryEa
.L436:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	movl	$7, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2581:
	.size	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringEPFPNS_14TransliteratorES3_NS4_5TokenEES6_aR10UErrorCode, .-_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringEPFPNS_14TransliteratorES3_NS4_5TokenEES6_aR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringES3_15UTransDirectionaaR10UErrorCode
	.type	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringES3_15UTransDirectionaaR10UErrorCode, @function
_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringES3_15UTransDirectionaaR10UErrorCode:
.LFB2582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$104, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$40, %rsp
	movq	16(%rbp), %rax
	movq	%rdx, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L440
	movq	%rax, %r8
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	-72(%rbp), %rdx
	movq	%rax, 8(%r8)
	movl	$2, %eax
	leaq	8(%r8), %rdi
	movw	%ax, 16(%r8)
	xorl	%eax, %eax
	testl	%r15d, %r15d
	setne	%al
	testb	%r14b, %r14b
	movups	%xmm0, 80(%r8)
	movl	%eax, (%r8)
	je	.L441
	movzwl	8(%rdx), %eax
	testb	$17, %al
	jne	.L447
	leaq	10(%rdx), %rsi
	testb	$2, %al
	je	.L449
.L442:
	movq	%rsi, -64(%rbp)
	leaq	-64(%rbp), %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%r8, -72(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	-72(%rbp), %r8
.L444:
	movsbl	%bl, %ecx
	movq	%r8, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryEa
.L439:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L450
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	movq	24(%rdx), %rsi
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L441:
	movq	%rdx, %rsi
	movq	%r8, -72(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-72(%rbp), %r8
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L447:
	xorl	%esi, %esi
	jmp	.L442
.L450:
	call	__stack_chk_fail@PLT
.L440:
	movq	-80(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L439
	.cfi_endproc
.LFE2582:
	.size	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringES3_15UTransDirectionaaR10UErrorCode, .-_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringES3_15UTransDirectionaaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringES3_aaR10UErrorCode
	.type	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringES3_aaR10UErrorCode, @function
_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringES3_aaR10UErrorCode:
.LFB2583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$104, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$40, %rsp
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L451
	movq	%rax, %r12
	pxor	%xmm0, %xmm0
	testb	%r15b, %r15b
	movq	-72(%rbp), %rdx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movups	%xmm0, 80(%r12)
	leaq	8(%r12), %rdi
	movq	%rax, 8(%r12)
	movl	$2, %eax
	movw	%ax, 16(%r12)
	movl	$6, (%r12)
	je	.L453
	movzwl	8(%rdx), %eax
	testb	$17, %al
	jne	.L458
	leaq	10(%rdx), %rsi
	testb	$2, %al
	je	.L463
.L454:
	movq	%rsi, -64(%rbp)
	leaq	-64(%rbp), %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
.L456:
	movsbl	%bl, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryEa
.L451:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L464
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	movq	24(%rdx), %rsi
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L453:
	movq	%rdx, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L458:
	xorl	%esi, %esi
	jmp	.L454
.L464:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2583:
	.size	_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringES3_aaR10UErrorCode, .-_ZN6icu_6722TransliteratorRegistry3putERKNS_13UnicodeStringES3_aaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722TransliteratorRegistry18findInDynamicStoreERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE
	.type	_ZNK6icu_6722TransliteratorRegistry18findInDynamicStoreERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE, @function
_ZNK6icu_6722TransliteratorRegistry18findInDynamicStoreERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE:
.LFB2611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	leaq	72(%rdx), %r8
	movq	%r9, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rcx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	72(%rsi), %rdi
	movq	%r8, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser7STVtoIDERKNS_13UnicodeStringES3_S3_RS1_@PLT
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	call	uhash_get_67@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L468
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L468:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2611:
	.size	_ZNK6icu_6722TransliteratorRegistry18findInDynamicStoreERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE, .-_ZNK6icu_6722TransliteratorRegistry18findInDynamicStoreERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB0:
	.text
.LHOTB0:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry16instantiateEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryERPNS_19TransliteratorAliasER10UErrorCode
	.type	_ZN6icu_6722TransliteratorRegistry16instantiateEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryERPNS_19TransliteratorAliasER10UErrorCode, @function
_ZN6icu_6722TransliteratorRegistry16instantiateEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryERPNS_19TransliteratorAliasER10UErrorCode:
.LFB2616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$696, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -712(%rbp)
	movq	%rcx, -720(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$7, (%rdx)
	ja	.L470
	movl	(%rdx), %eax
	movq	%rdx, %r13
	leaq	.L472(%rip), %rdx
	movq	%r8, %rbx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L472:
	.long	.L478-.L472
	.long	.L478-.L472
	.long	.L477-.L472
	.long	.L476-.L472
	.long	.L475-.L472
	.long	.L474-.L472
	.long	.L473-.L472
	.long	.L471-.L472
	.text
	.p2align 4,,10
	.p2align 3
.L478:
	leaq	-560(%rbp), %r12
	movq	%r8, %rsi
	leaq	-624(%rbp), %r15
	movq	%r12, %rdi
	call	_ZN6icu_6720TransliteratorParserC1ER10UErrorCode@PLT
	leaq	8(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L486
	movq	-712(%rbp), %rsi
	leaq	8(%rax), %rdi
	xorl	%ebx, %ebx
	cmpl	$1, 0(%r13)
	sete	%bl
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	72(%r14), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-720(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	%ebx, 152(%r14)
	movl	$2, 156(%r14)
	movups	%xmm0, 136(%r14)
	movq	%r14, (%rax)
.L487:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6720TransliteratorParserD1Ev@PLT
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L469:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L506
	addq	$696, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L481
	movq	-712(%rbp), %rsi
	leaq	8(%rax), %rdi
	movl	72(%r13), %ebx
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	8(%r13), %rsi
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-720(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	%ebx, 152(%r12)
	movl	$2, 156(%r12)
	movups	%xmm0, 136(%r12)
	movq	%r12, (%rax)
	xorl	%eax, %eax
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L476:
	movq	88(%r13), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	testq	%rax, %rax
	jne	.L469
.L505:
	movl	$7, (%rbx)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L475:
	movl	$104, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L479
	movq	88(%r13), %rdx
	movq	-712(%rbp), %rsi
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	movq	%rax, -696(%rbp)
	call	_ZN6icu_6723RuleBasedTransliteratorC1ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataEPNS_13UnicodeFilterE@PLT
	movq	-696(%rbp), %rax
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L473:
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L481
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	movq	80(%r13), %rbx
	leaq	8(%r13), %rsi
	movq	%rax, 8(%r12)
	leaq	72(%r12), %rdi
	movw	%cx, 16(%r12)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-720(%rbp), %rax
	movq	$0, 136(%r12)
	movq	%rbx, 144(%r12)
	movq	$0, 152(%r12)
	movq	%r12, (%rax)
	xorl	%eax, %eax
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L471:
	movq	96(%r13), %rsi
	movq	-712(%rbp), %rdi
	call	*88(%r13)
	testq	%rax, %rax
	jne	.L469
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L474:
	movq	88(%r13), %rax
	movl	$40, %edi
	movl	8(%rax), %r12d
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -728(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L479
	movq	%rbx, %rdx
	movl	%r12d, %esi
	call	_ZN6icu_677UVectorC1EiR10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L488
	xorl	%r8d, %r8d
	leaq	-624(%rbp), %r12
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L507:
	movl	%ecx, %esi
	movq	%r12, %rdi
	leaq	-688(%rbp), %r15
	movl	%ecx, -700(%rbp)
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	leaq	-560(%rbp), %r14
	leaq	_ZN6icu_6722CompoundTransliterator11PASS_STRINGE(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movl	-696(%rbp), %r8d
	movq	88(%r13), %rdi
	movl	%r8d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	$104, %edi
	movq	%rax, -696(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L484
	movq	-696(%rbp), %rdx
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, -696(%rbp)
	call	_ZN6icu_6723RuleBasedTransliteratorC1ERKNS_13UnicodeStringEPNS_23TransliterationRuleDataEa@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-696(%rbp), %r8
	movq	-728(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r8, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%rbx), %eax
	movl	-700(%rbp), %r8d
	testl	%eax, %eax
	jg	.L488
.L485:
	movq	88(%r13), %rax
	movl	%r8d, -696(%rbp)
	leal	1(%r8), %ecx
	cmpl	%r8d, 8(%rax)
	jg	.L507
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L481
	movq	-712(%rbp), %rsi
	leaq	8(%r12), %rdi
	movq	80(%r13), %rbx
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	8(%r13), %rsi
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%rbx, %xmm1
	movabsq	$4294967296, %rax
	movq	-728(%rbp), %xmm0
	movq	%rax, 152(%r12)
	movq	-720(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 136(%r12)
	movq	%r12, (%rax)
	xorl	%eax, %eax
	jmp	.L469
.L484:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$7, (%rbx)
	.p2align 4,,10
	.p2align 3
.L488:
	movq	-728(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	xorl	%eax, %eax
	jmp	.L469
.L506:
	call	__stack_chk_fail@PLT
.L479:
	movl	$7, (%rbx)
	xorl	%eax, %eax
	jmp	.L469
.L481:
	movq	-720(%rbp), %rax
	movq	$0, (%rax)
	xorl	%eax, %eax
	movl	$7, (%rbx)
	jmp	.L469
.L486:
	movq	-720(%rbp), %rax
	movq	$0, (%rax)
	movl	$7, (%rbx)
	jmp	.L487
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6722TransliteratorRegistry16instantiateEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryERPNS_19TransliteratorAliasER10UErrorCode.cold, @function
_ZN6icu_6722TransliteratorRegistry16instantiateEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryERPNS_19TransliteratorAliasER10UErrorCode.cold:
.LFSB2616:
.L470:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE2616:
	.text
	.size	_ZN6icu_6722TransliteratorRegistry16instantiateEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryERPNS_19TransliteratorAliasER10UErrorCode, .-_ZN6icu_6722TransliteratorRegistry16instantiateEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryERPNS_19TransliteratorAliasER10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_6722TransliteratorRegistry16instantiateEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryERPNS_19TransliteratorAliasER10UErrorCode.cold, .-_ZN6icu_6722TransliteratorRegistry16instantiateEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryERPNS_19TransliteratorAliasER10UErrorCode.cold
.LCOLDE0:
	.text
.LHOTE0:
	.section	.text.unlikely
	.align 2
.LCOLDB1:
	.text
.LHOTB1:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719TransliteratorAlias6createER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6719TransliteratorAlias6createER11UParseErrorR10UErrorCode, @function
_ZN6icu_6719TransliteratorAlias6createER11UParseErrorR10UErrorCode:
.LFB2545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -256(%rbp)
	movl	(%rdx), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r13d, %r13d
	jg	.L509
	movl	156(%rdi), %eax
	movq	%rdi, %rbx
	movq	%rdx, %r12
	cmpl	$1, %eax
	je	.L510
	cmpl	$2, %eax
	je	.L511
	xorl	%r13d, %r13d
	testl	%eax, %eax
	je	.L592
.L508:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L593
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L592:
	.cfi_restore_state
	movq	%rdx, %rcx
	leaq	72(%rdi), %rdi
	movq	%rsi, %rdx
	xorl	%esi, %esi
	call	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode@PLT
	movl	(%r12), %r12d
	movq	%rax, %r13
	testl	%r12d, %r12d
	jg	.L509
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L508
	call	_ZNK6icu_6710UnicodeSet5cloneEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6714Transliterator11adoptFilterEPNS_13UnicodeFilterE@PLT
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L509:
	xorl	%r13d, %r13d
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L510:
	movq	136(%rdi), %rax
	movl	$65535, %esi
	leaq	-240(%rbp), %r14
	leaq	72(%rbx), %r13
	movl	8(%rax), %eax
	movl	%eax, -268(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, %r15
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movl	$-1, %r11d
	xorl	%edx, %edx
	movq	%r14, %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	movw	%r11w, -240(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	80(%rbx), %r9d
	testw	%r9w, %r9w
	js	.L513
	sarl	$5, %r9d
.L514:
	movzwl	-184(%rbp), %esi
	movl	%esi, %eax
	andl	$1, %eax
	testw	%si, %si
	js	.L515
	movswl	%si, %ecx
	sarl	$5, %ecx
	testb	%al, %al
	jne	.L517
.L516:
	testl	%ecx, %ecx
	je	.L594
	andl	$2, %esi
	leaq	-182(%rbp), %rax
	movq	%r13, %rdi
	cmove	-168(%rbp), %rax
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movzwl	-184(%rbp), %esi
	movl	-180(%rbp), %ecx
	movl	%eax, %r8d
.L518:
	xorl	%r15d, %r15d
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L596:
	sarl	$5, %r9d
	cmpl	%r9d, %r8d
	cmovg	%r9d, %r8d
	testw	%si, %si
	js	.L595
.L523:
	testl	%r10d, %r10d
	je	.L517
	andl	$2, %esi
	leaq	-182(%rbp), %rax
	movl	%r10d, %ecx
	movq	%r13, %rdi
	cmove	-168(%rbp), %rax
	subl	%r8d, %r9d
	movq	%rax, %rsi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movzwl	-184(%rbp), %esi
	movl	-180(%rbp), %ecx
	movl	%eax, %r8d
.L525:
	testl	%ecx, %ecx
	movl	%r15d, %eax
	movswl	%si, %edi
	movl	%r15d, %edx
	cmovle	%ecx, %eax
	sarl	$5, %edi
	testl	%edi, %edi
	cmovle	%edi, %edx
	testb	$1, %sil
	jne	.L517
	testw	%si, %si
	cmovs	%ecx, %edi
	cmovs	%eax, %edx
	movl	%edi, %r10d
	subl	%edx, %r10d
	cmpl	%edi, %r10d
	cmovg	%edi, %r10d
	testl	%r8d, %r8d
	js	.L517
	movswl	80(%rbx), %r9d
	addl	$1, %r8d
	testw	%r9w, %r9w
	jns	.L596
	movl	84(%rbx), %r9d
	cmpl	%r9d, %r8d
	cmovg	%r9d, %r8d
	testw	%si, %si
	jns	.L523
.L595:
	testl	%ecx, %ecx
	jns	.L523
.L517:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movswl	80(%rbx), %ecx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r10d
	movq	%rax, -128(%rbp)
	movw	%r10w, -120(%rbp)
	testw	%cx, %cx
	js	.L526
	sarl	$5, %ecx
.L527:
	xorl	%edx, %edx
	movl	$65535, %esi
	leaq	-128(%rbp), %r15
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %r9d
	testl	%eax, %eax
	jns	.L528
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L599:
	sarl	$5, %edx
.L532:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%r9d, -248(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movl	-248(%rbp), %r9d
	leal	1(%r9), %edx
	cmpl	$2147483646, %r9d
	jne	.L533
	movzwl	80(%rbx), %edx
	movl	$2, %ecx
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	cmovne	%ecx, %eax
	movw	%ax, 80(%rbx)
.L535:
	movswl	-120(%rbp), %eax
	shrl	$5, %eax
	jne	.L597
.L536:
	movq	136(%rbx), %rdi
	movl	8(%rdi), %r9d
	testl	%r9d, %r9d
	jne	.L598
.L537:
	movswl	80(%rbx), %ecx
	testw	%cx, %cx
	js	.L538
	sarl	$5, %ecx
.L588:
	xorl	%edx, %edx
	movl	$65535, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %r9d
	testl	%eax, %eax
	js	.L539
.L528:
	movswl	-120(%rbp), %edx
	testw	%dx, %dx
	jns	.L599
	movl	-116(%rbp), %edx
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L513:
	movl	84(%rbx), %r9d
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L538:
	movl	84(%rbx), %ecx
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L598:
	xorl	%esi, %esi
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L597:
	movq	-256(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r12, %rcx
	call	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L533:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L539:
	movswl	80(%rbx), %eax
	shrl	$5, %eax
	je	.L530
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L541:
	xorl	%esi, %esi
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
.L590:
	movq	%rax, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L530:
	movq	136(%rbx), %rdi
	movl	8(%rdi), %r8d
	testl	%r8d, %r8d
	jne	.L541
	movl	(%r12), %esi
	testl	%esi, %esi
	jle	.L601
	movl	-232(%rbp), %eax
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.L551
	.p2align 4,,10
	.p2align 3
.L548:
	movq	%r14, %rdi
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L549
	movq	(%rax), %rax
	addl	$1, %ebx
	call	*8(%rax)
	cmpl	-232(%rbp), %ebx
	jl	.L548
.L551:
	xorl	%r13d, %r13d
.L591:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	-264(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L549:
	addl	$1, %ebx
	cmpl	-232(%rbp), %ebx
	jl	.L548
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L601:
	movq	144(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L545
	movq	%rcx, %rdi
	call	_ZNK6icu_6710UnicodeSet5cloneEv@PLT
	movq	%rax, %rcx
.L545:
	movl	$104, %edi
	movq	%rcx, -248(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L546
	subq	$8, %rsp
	movq	%r14, %rdx
	leaq	8(%rbx), %rsi
	movq	%rax, %rdi
	movq	-248(%rbp), %rcx
	movq	-256(%rbp), %r9
	pushq	%r12
	movl	-268(%rbp), %r8d
	call	_ZN6icu_6722CompoundTransliteratorC1ERKNS_13UnicodeStringERNS_7UVectorEPNS_13UnicodeFilterEiR11UParseErrorR10UErrorCode@PLT
	popq	%rdx
	popq	%rcx
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L526:
	movl	84(%rbx), %ecx
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L515:
	movl	-180(%rbp), %ecx
	testb	%al, %al
	jne	.L517
	testl	%ecx, %ecx
	jns	.L516
	movl	$-1, %r8d
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L600:
	movq	-256(%rbp), %rdx
	movq	%r12, %rcx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode@PLT
	jmp	.L590
.L594:
	movl	-180(%rbp), %ecx
	movl	$-1, %r8d
	jmp	.L518
.L593:
	call	__stack_chk_fail@PLT
.L546:
	movl	$7, (%r12)
	jmp	.L591
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6719TransliteratorAlias6createER11UParseErrorR10UErrorCode.cold, @function
_ZN6icu_6719TransliteratorAlias6createER11UParseErrorR10UErrorCode.cold:
.LFSB2545:
.L511:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE2545:
	.text
	.size	_ZN6icu_6719TransliteratorAlias6createER11UParseErrorR10UErrorCode, .-_ZN6icu_6719TransliteratorAlias6createER11UParseErrorR10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_6719TransliteratorAlias6createER11UParseErrorR10UErrorCode.cold, .-_ZN6icu_6719TransliteratorAlias6createER11UParseErrorR10UErrorCode.cold
.LCOLDE1:
	.text
.LHOTE1:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistryD2Ev
	.type	_ZN6icu_6722TransliteratorRegistryD2Ev, @function
_ZN6icu_6722TransliteratorRegistryD2Ev:
.LFB2576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$224, %rdi
	subq	$8, %rsp
	call	_ZN6icu_677UVectorD1Ev@PLT
	leaq	184(%rbx), %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L603
	call	uhash_close_67@PLT
.L603:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L602
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uhash_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L602:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2576:
	.size	_ZN6icu_6722TransliteratorRegistryD2Ev, .-_ZN6icu_6722TransliteratorRegistryD2Ev
	.globl	_ZN6icu_6722TransliteratorRegistryD1Ev
	.set	_ZN6icu_6722TransliteratorRegistryD1Ev,_ZN6icu_6722TransliteratorRegistryD2Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	""
.LC3:
	.string	"icudt67l-translit"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TransliteratorSpecC2ERKNS_13UnicodeStringE
	.type	_ZN6icu_6718TransliteratorSpecC2ERKNS_13UnicodeStringE, @function
_ZN6icu_6718TransliteratorSpecC2ERKNS_13UnicodeStringE:
.LFB2551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-288(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	subq	$392, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	8(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -432(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$2, %ecx
	movq	%rbx, 72(%r15)
	xorl	%edx, %edx
	movl	$2, %esi
	movl	$2, %edi
	movw	%cx, 80(%r15)
	xorl	%ecx, %ecx
	movw	%si, 144(%r15)
	leaq	200(%r15), %rax
	xorl	%r8d, %r8d
	leaq	.LC2(%rip), %rsi
	movw	%di, 208(%r15)
	movq	%r13, %rdi
	movq	%rbx, 136(%r15)
	movq	%rbx, 200(%r15)
	movq	$0, 272(%r15)
	movq	%rax, -424(%rbp)
	movl	$0, -404(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713LocaleUtility18initLocaleFromNameERKNS_13UnicodeStringERNS_6LocaleE@PLT
	cmpb	$0, -72(%rbp)
	leaq	-404(%rbp), %rcx
	jne	.L611
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L612
	leaq	-404(%rbp), %rcx
	movq	%rax, %rdi
	movq	%r13, %rdx
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -424(%rbp)
	call	_ZN6icu_6714ResourceBundleC1EPKcRKNS_6LocaleER10UErrorCode@PLT
	movl	-404(%rbp), %eax
	movq	%r14, 272(%r15)
	movq	-424(%rbp), %rcx
	testl	%eax, %eax
	jg	.L624
	cmpl	$-127, %eax
	jne	.L611
.L624:
	movq	%r14, %rdi
	movq	%rcx, -424(%rbp)
	call	_ZN6icu_6714ResourceBundleD0Ev@PLT
	movq	-424(%rbp), %rcx
	movq	$0, 272(%r15)
.L611:
	xorl	%edx, %edx
	leaq	-352(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movw	%dx, -340(%rbp)
	movq	%r14, %rdi
	movq	%rcx, %rdx
	leaq	-339(%rbp), %rax
	movaps	%xmm0, -400(%rbp)
	movq	%rcx, -424(%rbp)
	movaps	%xmm0, -384(%rbp)
	movl	$0, -404(%rbp)
	movq	$0, -368(%rbp)
	movl	$-1, -400(%rbp)
	movq	%rax, -352(%rbp)
	movl	$0, -296(%rbp)
	movl	$40, -344(%rbp)
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	-424(%rbp), %rcx
	movl	$10, %edx
	leaq	-400(%rbp), %rsi
	movq	(%rax), %rdi
	call	uscript_getCode_67@PLT
	cmpb	$0, -340(%rbp)
	movl	%eax, %r12d
	jne	.L635
.L615:
	testl	%r12d, %r12d
	jle	.L616
	movl	-400(%rbp), %edi
	cmpl	$-1, %edi
	jne	.L636
.L616:
	cmpq	$0, 272(%r15)
	je	.L617
.L639:
	movl	$2, %eax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rbx, -352(%rbp)
	movw	%ax, -344(%rbp)
	call	_ZN6icu_6713LocaleUtility18initNameFromLocaleERKNS_6LocaleERNS_13UnicodeStringE@PLT
	testb	$1, -344(%rbp)
	je	.L637
.L618:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L619:
	movq	%r15, %rdi
	call	_ZN6icu_6718TransliteratorSpec5resetEv
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
.L609:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L638
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L636:
	.cfi_restore_state
	call	uscript_getName_67@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movq	%r14, %rsi
	leaq	200(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpq	$0, 272(%r15)
	jne	.L639
.L617:
	movswl	208(%r15), %eax
	testw	%ax, %ax
	js	.L620
	sarl	$5, %eax
.L621:
	testl	%eax, %eax
	je	.L619
	movq	-432(%rbp), %rdi
	leaq	200(%r15), %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L635:
	movq	-352(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L637:
	movq	-432(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L620:
	movl	212(%r15), %eax
	jmp	.L621
.L612:
	movq	$0, 272(%r15)
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L609
.L638:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2551:
	.size	_ZN6icu_6718TransliteratorSpecC2ERKNS_13UnicodeStringE, .-_ZN6icu_6718TransliteratorSpecC2ERKNS_13UnicodeStringE
	.globl	_ZN6icu_6718TransliteratorSpecC1ERKNS_13UnicodeStringE
	.set	_ZN6icu_6718TransliteratorSpecC1ERKNS_13UnicodeStringE,_ZN6icu_6718TransliteratorSpecC2ERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719TransliteratorEntryD2Ev
	.type	_ZN6icu_6719TransliteratorEntryD2Ev, @function
_ZN6icu_6719TransliteratorEntryD2Ev:
.LFB2567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	cmpl	$3, %eax
	je	.L664
	cmpl	$4, %eax
	je	.L665
	cmpl	$5, %eax
	je	.L646
.L643:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L648
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L648:
	addq	$16, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L667:
	.cfi_restore_state
	xorl	%esi, %esi
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	testq	%rax, %rax
	jne	.L666
.L646:
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L643
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jne	.L667
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L665:
	movq	88(%rdi), %r12
	testq	%r12, %r12
	je	.L643
	movq	%r12, %rdi
	call	_ZN6icu_6723TransliterationRuleDataD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L664:
	movq	88(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L643
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L666:
	movq	%rax, %rdi
	movq	%rax, -24(%rbp)
	call	_ZN6icu_6723TransliterationRuleDataD1Ev@PLT
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L646
	.cfi_endproc
.LFE2567:
	.size	_ZN6icu_6719TransliteratorEntryD2Ev, .-_ZN6icu_6719TransliteratorEntryD2Ev
	.globl	_ZN6icu_6719TransliteratorEntryD1Ev
	.set	_ZN6icu_6719TransliteratorEntryD1Ev,_ZN6icu_6719TransliteratorEntryD2Ev
	.p2align 4
	.type	deleteEntry, @function
deleteEntry:
.LFB2571:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L668
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6719TransliteratorEntryD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L668:
	ret
	.cfi_endproc
.LFE2571:
	.size	deleteEntry, .-deleteEntry
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry12findInBundleERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE15UTransDirection
	.type	_ZN6icu_6722TransliteratorRegistry12findInBundleERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE15UTransDirection, @function
_ZN6icu_6722TransliteratorRegistry12findInBundleERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE15UTransDirection:
.LFB2613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r9d
	movl	$2, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-580(%rbp), %r15
	leaq	-576(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-416(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-544(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-288(%rbp), %rbx
	subq	$632, %rsp
	movq	%rdx, -656(%rbp)
	leaq	_ZN6icu_67L16TRANSLITERATE_TOE(%rip), %rdx
	movq	%rdi, -616(%rbp)
	movl	%ecx, -600(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	testl	%ecx, %ecx
	movw	%r9w, -536(%rbp)
	movq	%rax, -544(%rbp)
	movq	%rax, -480(%rbp)
	leaq	_ZN6icu_67L18TRANSLITERATE_FROME(%rip), %rax
	cmove	%rdx, %rax
	leaq	-275(%rbp), %rdx
	movw	%r10w, -472(%rbp)
	movq	%rdx, -648(%rbp)
	leaq	-480(%rbp), %rdx
	movq	%rdx, -624(%rbp)
	leaq	-352(%rbp), %rdx
	movl	$0, -596(%rbp)
	movq	%rdx, -664(%rbp)
	movq	%rax, -632(%rbp)
	leaq	72(%rsi), %rax
	movq	%rax, -640(%rbp)
	movl	$2, %eax
.L703:
	testb	$1, %al
	je	.L674
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L675:
	movl	-596(%rbp), %r8d
	movl	$-1, %ecx
	xorl	%edx, %edx
	testl	%r8d, %r8d
	jne	.L678
	movq	-632(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L679:
	movq	-640(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7toUpperERKNS_6LocaleE@PLT
	movswl	8(%rax), %ecx
	movq	%rax, %rsi
	testw	%cx, %cx
	js	.L680
	sarl	$5, %ecx
.L681:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	xorl	%edi, %edi
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	-616(%rbp), %rax
	movw	%di, -276(%rbp)
	movq	%rbx, %rdi
	movl	$0, -580(%rbp)
	movq	272(%rax), %r8
	movq	-648(%rbp), %rax
	movl	$0, -232(%rbp)
	movl	$40, -280(%rbp)
	movq	%r8, -608(%rbp)
	movq	%rax, -288(%rbp)
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	-608(%rbp), %r8
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	(%rax), %rdx
	movq	%r8, %rsi
	call	_ZNK6icu_6714ResourceBundle3getEPKcR10UErrorCode@PLT
	cmpb	$0, -276(%rbp)
	jne	.L729
.L682:
	movl	-580(%rbp), %eax
	cmpl	$-127, %eax
	je	.L683
	testl	%eax, %eax
	jg	.L683
	movzwl	-408(%rbp), %eax
	testb	$1, %al
	je	.L684
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L685:
	movq	%r14, %rdi
	call	_ZNK6icu_6714ResourceBundle9getLocaleEv@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713LocaleUtility18initNameFromLocaleERKNS_6LocaleERNS_13UnicodeStringE@PLT
	movq	-616(%rbp), %rsi
	movswl	80(%rsi), %edx
	testb	$1, %dl
	je	.L688
	movzwl	8(%rax), %eax
	andl	$1, %eax
.L689:
	testb	%al, %al
	je	.L683
	movq	-656(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L694
	sarl	$5, %eax
.L695:
	movl	$0, -580(%rbp)
	testl	%eax, %eax
	je	.L696
	xorl	%esi, %esi
	movq	-664(%rbp), %rdi
	leaq	-339(%rbp), %rax
	movq	%r15, %rdx
	movw	%si, -340(%rbp)
	movq	-656(%rbp), %rsi
	movq	%rax, -352(%rbp)
	movl	$0, -296(%rbp)
	movl	$40, -344(%rbp)
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	(%rax), %rdx
	call	_ZNK6icu_6714ResourceBundle11getStringExEPKcR10UErrorCode@PLT
	movq	-624(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -340(%rbp)
	jne	.L730
.L728:
	movl	-580(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L698
	.p2align 4,,10
	.p2align 3
.L683:
	movq	%r14, %rdi
	call	_ZN6icu_6714ResourceBundleD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	$1, -596(%rbp)
	jne	.L731
.L699:
	xorl	%r13d, %r13d
.L702:
	movq	-624(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L732
	addq	$632, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L676
	movswl	%ax, %edx
	sarl	$5, %edx
.L677:
	testl	%edx, %edx
	je	.L675
	andl	$31, %eax
	movw	%ax, -536(%rbp)
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L731:
	movl	$1, -596(%rbp)
	movzwl	-536(%rbp), %eax
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L678:
	leaq	_ZN6icu_67L13TRANSLITERATEE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	_ZN6icu_67L13TRANSLITERATEE(%rip), %rax
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L680:
	movl	12(%rax), %ecx
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L729:
	movq	-288(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L684:
	testw	%ax, %ax
	js	.L686
	movswl	%ax, %edx
	sarl	$5, %edx
.L687:
	testl	%edx, %edx
	je	.L685
	andl	$31, %eax
	movw	%ax, -408(%rbp)
	jmp	.L685
.L698:
	movq	%r14, %rdi
	call	_ZN6icu_6714ResourceBundleD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$104, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L699
	movl	-596(%rbp), %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$2, 0(%r13)
	movq	%rax, 8(%r13)
	movl	$2, %eax
	movq	-624(%rbp), %rsi
	leaq	8(%r13), %rdi
	movw	%ax, 16(%r13)
	testl	%edx, %edx
	movl	$0, %eax
	cmovne	-600(%rbp), %eax
	movups	%xmm0, 80(%r13)
	movl	%eax, %ebx
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	%ebx, 72(%r13)
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L676:
	movl	-532(%rbp), %edx
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L688:
	testw	%dx, %dx
	js	.L690
	movzwl	8(%rax), %ecx
	sarl	$5, %edx
	testw	%cx, %cx
	js	.L692
.L733:
	movswl	%cx, %esi
	sarl	$5, %esi
.L693:
	andl	$1, %ecx
	jne	.L683
	cmpl	%edx, %esi
	jne	.L683
	movq	-616(%rbp), %rcx
	movq	%rax, %rsi
	leaq	72(%rcx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L696:
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6714ResourceBundle11getStringExEiR10UErrorCode@PLT
	movq	-624(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-580(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L683
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L694:
	movq	-656(%rbp), %rax
	movl	12(%rax), %eax
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L730:
	movq	-352(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L690:
	movzwl	8(%rax), %ecx
	movl	84(%rsi), %edx
	testw	%cx, %cx
	jns	.L733
.L692:
	movl	12(%rax), %esi
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L686:
	movl	-404(%rbp), %edx
	jmp	.L687
.L732:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2613:
	.size	_ZN6icu_6722TransliteratorRegistry12findInBundleERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE15UTransDirection, .-_ZN6icu_6722TransliteratorRegistry12findInBundleERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE15UTransDirection
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry17findInStaticStoreERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE
	.type	_ZN6icu_6722TransliteratorRegistry17findInStaticStoreERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE, @function
_ZN6icu_6722TransliteratorRegistry17findInStaticStoreERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE:
.LFB2612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, 264(%rsi)
	jne	.L743
	xorl	%r13d, %r13d
	cmpb	$0, 264(%rdx)
	je	.L734
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6722TransliteratorRegistry12findInBundleERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE15UTransDirection
	movq	%rax, %r13
.L736:
	testq	%r13, %r13
	je	.L734
	leaq	8(%r12), %rdx
	leaq	8(%rbx), %rsi
	xorl	%r9d, %r9d
	movq	%r13, %r8
	movq	%r15, %rcx
	movq	%r14, %rdi
	call	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_PNS_19TransliteratorEntryEa
.L734:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L743:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6722TransliteratorRegistry12findInBundleERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE15UTransDirection
	movq	%rax, %r13
	jmp	.L736
	.cfi_endproc
.LFE2612:
	.size	_ZN6icu_6722TransliteratorRegistry17findInStaticStoreERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE, .-_ZN6icu_6722TransliteratorRegistry17findInStaticStoreERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry4findERNS_13UnicodeStringES2_S2_
	.type	_ZN6icu_6722TransliteratorRegistry4findERNS_13UnicodeStringES2_S2_, @function
_ZN6icu_6722TransliteratorRegistry4findERNS_13UnicodeStringES2_S2_:
.LFB2615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-624(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	subq	$840, %rsp
	movq	%rdi, -832(%rbp)
	movq	%r15, %rdi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN6icu_6718TransliteratorSpecC1ERKNS_13UnicodeStringE
	leaq	-336(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN6icu_6718TransliteratorSpecC1ERKNS_13UnicodeStringE
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	leaq	-816(%rbp), %rax
	movl	$2, %r8d
	movq	%rbx, -816(%rbp)
	movq	%rax, %rcx
	movq	%rax, -864(%rbp)
	movw	%r8w, -808(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser7STVtoIDERKNS_13UnicodeStringES3_S3_RS1_@PLT
	movq	-832(%rbp), %rax
	movq	-864(%rbp), %rsi
	movq	8(%rax), %rdi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L792
	leaq	-264(%rbp), %rcx
	leaq	-552(%rbp), %r13
	movq	%rcx, -848(%rbp)
.L797:
	leaq	-488(%rbp), %rcx
	leaq	-328(%rbp), %r15
	movq	%rcx, -840(%rbp)
	leaq	-200(%rbp), %rcx
	leaq	-616(%rbp), %rbx
	movq	%rcx, -872(%rbp)
.L745:
	movq	-864(%rbp), %rdi
	movq	%rax, -824(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-64(%rbp), %rdi
	movq	-824(%rbp), %rax
	testq	%rdi, %rdi
	je	.L769
	movq	(%rdi), %rdx
	call	*8(%rdx)
	movq	-824(%rbp), %rax
.L769:
	leaq	-136(%rbp), %rdi
	movq	%rax, -824(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-872(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-848(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-352(%rbp), %rdi
	movq	-824(%rbp), %rax
	testq	%rdi, %rdi
	je	.L770
	movq	(%rdi), %rdx
	call	*8(%rdx)
	movq	-824(%rbp), %rax
.L770:
	leaq	-424(%rbp), %rdi
	movq	%rax, -824(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-840(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movq	-824(%rbp), %rax
	jne	.L798
	addq	$840, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L792:
	.cfi_restore_state
	movswl	8(%r14), %eax
	testw	%ax, %ax
	js	.L746
	sarl	$5, %eax
	testl	%eax, %eax
	jne	.L748
.L801:
	leaq	-264(%rbp), %rax
	leaq	-688(%rbp), %r12
	movq	%rax, -848(%rbp)
	leaq	-552(%rbp), %r13
.L752:
	leaq	-488(%rbp), %rax
	leaq	-752(%rbp), %r14
	movq	%rax, -840(%rbp)
	leaq	-200(%rbp), %rax
	movq	%rax, -872(%rbp)
.L749:
	movq	%r15, %rdi
	call	_ZN6icu_6718TransliteratorSpec5resetEv
	.p2align 4,,10
	.p2align 3
.L753:
	movl	$2, %esi
	movl	$2, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movw	%si, -680(%rbp)
	movq	-848(%rbp), %rsi
	movw	%cx, -744(%rbp)
	movq	%r12, %rcx
	movq	%rbx, -752(%rbp)
	movq	%rbx, -688(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser7STVtoIDERKNS_13UnicodeStringES3_S3_RS1_@PLT
	movq	-832(%rbp), %rax
	movq	%r12, %rsi
	movq	8(%rax), %rdi
	call	uhash_get_67@PLT
	movq	%r12, %rdi
	movq	%rax, -824(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-824(%rbp), %rax
	testq	%rax, %rax
	jne	.L796
	movl	$2, %edx
	cmpb	$0, -360(%rbp)
	movq	%rbx, -688(%rbp)
	movw	%dx, -680(%rbp)
	jne	.L799
	cmpb	$0, -72(%rbp)
	je	.L759
	movq	-856(%rbp), %rdi
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN6icu_6722TransliteratorRegistry12findInBundleERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE15UTransDirection
.L755:
	testq	%rax, %rax
	je	.L759
	movq	-832(%rbp), %rdi
	movq	%rax, %r8
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	leaq	-328(%rbp), %r15
	leaq	-616(%rbp), %rbx
	movq	%rax, -824(%rbp)
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_PNS_19TransliteratorEntryEa
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-824(%rbp), %rax
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L759:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movswl	-480(%rbp), %eax
	movl	%eax, %edx
	sarl	$5, %eax
	testw	%dx, %dx
	cmovs	-476(%rbp), %eax
	testl	%eax, %eax
	je	.L800
	movq	-840(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzbl	-359(%rbp), %eax
	movb	$0, -359(%rbp)
	movb	%al, -360(%rbp)
	testb	%al, %al
	je	.L762
	movq	%r15, %rdi
	call	_ZN6icu_6718TransliteratorSpec9setupNextEv.part.0
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L762:
	movzwl	-480(%rbp), %eax
	testb	$1, %al
	je	.L764
	movq	-840(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L799:
	movq	-856(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6722TransliteratorRegistry12findInBundleERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE15UTransDirection
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L764:
	testw	%ax, %ax
	js	.L765
	movswl	%ax, %edx
	sarl	$5, %edx
.L766:
	testl	%edx, %edx
	je	.L753
	andl	$31, %eax
	movw	%ax, -480(%rbp)
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L765:
	movl	-476(%rbp), %edx
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L746:
	movl	12(%r14), %eax
	testl	%eax, %eax
	je	.L801
.L748:
	leaq	-264(%rbp), %rax
	movl	$2, %edi
	movq	%r14, %rdx
	movq	%rbx, -688(%rbp)
	leaq	-688(%rbp), %r12
	leaq	-552(%rbp), %r13
	movq	%rax, %rsi
	movw	%di, -680(%rbp)
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%rax, -848(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser7STVtoIDERKNS_13UnicodeStringES3_S3_RS1_@PLT
	movq	-832(%rbp), %rax
	movq	%r12, %rsi
	movq	8(%rax), %rdi
	call	uhash_get_67@PLT
	movq	%r12, %rdi
	movq	%rax, -824(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-824(%rbp), %rax
	testq	%rax, %rax
	jne	.L797
	cmpb	$0, -360(%rbp)
	jne	.L802
	cmpb	$0, -72(%rbp)
	je	.L752
	movq	-856(%rbp), %rdi
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN6icu_6722TransliteratorRegistry12findInBundleERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE15UTransDirection
.L751:
	testq	%rax, %rax
	je	.L752
	movq	-832(%rbp), %rdi
	movq	%rax, %r8
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	leaq	-328(%rbp), %r15
	leaq	-616(%rbp), %rbx
	movq	%rax, -824(%rbp)
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN6icu_6722TransliteratorRegistry13registerEntryERKNS_13UnicodeStringES3_S3_PNS_19TransliteratorEntryEa
	leaq	-488(%rbp), %rax
	movq	%rax, -840(%rbp)
	leaq	-200(%rbp), %rax
	movq	%rax, -872(%rbp)
	movq	-824(%rbp), %rax
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L767:
	movl	-188(%rbp), %eax
	testl	%eax, %eax
	jne	.L768
.L774:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L796:
	leaq	-328(%rbp), %r15
	leaq	-616(%rbp), %rbx
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L800:
	movswl	-192(%rbp), %eax
	testw	%ax, %ax
	js	.L767
	shrl	$5, %eax
	je	.L774
.L768:
	movq	-848(%rbp), %rdi
	movq	-872(%rbp), %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzbl	-71(%rbp), %eax
	movq	-856(%rbp), %rdi
	movb	%al, -72(%rbp)
	call	_ZN6icu_6718TransliteratorSpec9setupNextEv
	jmp	.L749
.L802:
	movq	-856(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6722TransliteratorRegistry12findInBundleERKNS_18TransliteratorSpecES3_RKNS_13UnicodeStringE15UTransDirection
	jmp	.L751
.L798:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2615:
	.size	_ZN6icu_6722TransliteratorRegistry4findERNS_13UnicodeStringES2_S2_, .-_ZN6icu_6722TransliteratorRegistry4findERNS_13UnicodeStringES2_S2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry4findERKNS_13UnicodeStringE
	.type	_ZN6icu_6722TransliteratorRegistry4findERKNS_13UnicodeStringE, @function
_ZN6icu_6722TransliteratorRegistry4findERKNS_13UnicodeStringE:
.LFB2614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movl	$2, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rsi, %rdi
	pushq	%r14
	movl	$2, %esi
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	leaq	-241(%rbp), %r8
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-176(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-240(%rbp), %r12
	subq	$224, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -232(%rbp)
	movq	%r13, %rdx
	movw	%cx, -168(%rbp)
	movq	%r14, %rcx
	movw	%si, -104(%rbp)
	movq	%r12, %rsi
	movq	%rax, -240(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser7IDtoSTVERKNS_13UnicodeStringERS1_S4_S4_Ra@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6722TransliteratorRegistry4findERNS_13UnicodeStringES2_S2_
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L806
	addq	$224, %rsp
	movq	%r15, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L806:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2614:
	.size	_ZN6icu_6722TransliteratorRegistry4findERKNS_13UnicodeStringE, .-_ZN6icu_6722TransliteratorRegistry4findERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry3getERKNS_13UnicodeStringERPNS_19TransliteratorAliasER10UErrorCode
	.type	_ZN6icu_6722TransliteratorRegistry3getERKNS_13UnicodeStringERPNS_19TransliteratorAliasER10UErrorCode, @function
_ZN6icu_6722TransliteratorRegistry3getERKNS_13UnicodeStringERPNS_19TransliteratorAliasER10UErrorCode:
.LFB2578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r15
	movq	%rsi, %r14
	movl	$2, %esi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	leaq	-257(%rbp), %r8
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-192(%rbp), %rbx
	subq	$264, %rsp
	movq	%rdx, -296(%rbp)
	movl	$2, %edx
	movq	%rcx, -288(%rbp)
	movl	$2, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -248(%rbp)
	movq	%rbx, %rdx
	movw	%cx, -184(%rbp)
	movq	%r13, %rcx
	movw	%si, -120(%rbp)
	movq	%r15, %rsi
	movq	%rax, -256(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser7IDtoSTVERKNS_13UnicodeStringERS1_S4_S4_Ra@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r12, -280(%rbp)
	call	_ZN6icu_6722TransliteratorRegistry4findERNS_13UnicodeStringES2_S2_
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r12, %r12
	je	.L807
	movq	-288(%rbp), %r10
	movq	-296(%rbp), %r9
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	-280(%rbp), %rdi
	movq	%r10, %r8
	movq	%r9, %rcx
	call	_ZN6icu_6722TransliteratorRegistry16instantiateEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryERPNS_19TransliteratorAliasER10UErrorCode
	movq	%rax, %r12
.L807:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L814
	addq	$264, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L814:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2578:
	.size	_ZN6icu_6722TransliteratorRegistry3getERKNS_13UnicodeStringERPNS_19TransliteratorAliasER10UErrorCode, .-_ZN6icu_6722TransliteratorRegistry3getERKNS_13UnicodeStringERPNS_19TransliteratorAliasER10UErrorCode
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC4:
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"-"
	.string	"N"
	.string	"U"
	.string	"L"
	.string	"L"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorRegistry5regetERKNS_13UnicodeStringERNS_20TransliteratorParserERPNS_19TransliteratorAliasER10UErrorCode
	.type	_ZN6icu_6722TransliteratorRegistry5regetERKNS_13UnicodeStringERNS_20TransliteratorParserERPNS_19TransliteratorAliasER10UErrorCode, @function
_ZN6icu_6722TransliteratorRegistry5regetERKNS_13UnicodeStringERNS_20TransliteratorParserERPNS_19TransliteratorAliasER10UErrorCode:
.LFB2579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-192(%rbp), %r11
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	pushq	%r13
	leaq	-256(%rbp), %r9
	leaq	-264(%rbp), %r14
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r11, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	movl	$2, %r8d
	subq	$280, %rsp
	movq	%rdi, -296(%rbp)
	movl	$2, %edi
	movq	%rsi, -304(%rbp)
	movl	$2, %esi
	movq	%rcx, -312(%rbp)
	movq	%r15, %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%si, -248(%rbp)
	movq	%r9, %rsi
	movw	%di, -184(%rbp)
	movq	%r10, %rdi
	movw	%r8w, -120(%rbp)
	movq	%r14, %r8
	movq	%rax, -256(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -128(%rbp)
	movq	%r11, -288(%rbp)
	movq	%r9, -280(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser7IDtoSTVERKNS_13UnicodeStringERS1_S4_S4_Ra@PLT
	movq	-280(%rbp), %r9
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-288(%rbp), %r11
	movq	%r9, %rsi
	movq	%r11, %rdx
	call	_ZN6icu_6722TransliteratorRegistry4findERNS_13UnicodeStringES2_S2_
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-288(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-280(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r12, %r12
	je	.L815
	cmpl	$2, (%r12)
	jbe	.L844
.L820:
	movq	-312(%rbp), %rcx
	movq	-304(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rbx, %r8
	movq	-296(%rbp), %rdi
	call	_ZN6icu_6722TransliteratorRegistry16instantiateEntryERKNS_13UnicodeStringEPNS_19TransliteratorEntryERPNS_19TransliteratorAliasER10UErrorCode
	movq	%rax, %r12
.L815:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L845
	addq	$280, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L844:
	.cfi_restore_state
	movl	56(%r13), %eax
	testl	%eax, %eax
	je	.L846
	leaq	8(%r12), %rcx
	movq	%rcx, -280(%rbp)
	cmpl	$1, %eax
	jne	.L822
	movl	16(%r13), %ecx
	testl	%ecx, %ecx
	jne	.L822
	leaq	48(%r13), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	-280(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6720TransliteratorParser20orphanCompoundFilterEv@PLT
	movl	$6, (%r12)
	movq	%rax, 80(%r12)
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L846:
	movl	16(%r13), %eax
	testl	%eax, %eax
	je	.L847
	cmpl	$1, %eax
	je	.L821
	leaq	8(%r12), %rax
	movq	%rax, -280(%rbp)
.L822:
	movl	$5, (%r12)
	movq	%r13, %rdi
	call	_ZN6icu_6720TransliteratorParser20orphanCompoundFilterEv@PLT
	movl	$40, %edi
	movq	%rax, 80(%r12)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L826
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
.L826:
	movzwl	16(%r12), %edx
	movq	%r15, 88(%r12)
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 16(%r12)
	movl	56(%r13), %eax
	cmpl	%eax, 16(%r13)
	movl	%eax, %edx
	cmovge	16(%r13), %edx
	testl	%edx, %edx
	jle	.L820
	leaq	48(%r13), %rcx
	movq	%r14, -320(%rbp)
	xorl	%r15d, %r15d
	movq	%r13, %r14
	movq	%rcx, -288(%rbp)
	movl	%edx, %r13d
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L829:
	movl	16(%r14), %edx
	testl	%edx, %edx
	jne	.L848
.L832:
	addl	$1, %r15d
	cmpl	%r15d, %r13d
	je	.L820
	movl	56(%r14), %eax
.L833:
	cmpl	%eax, %r15d
	jge	.L829
	movq	-288(%rbp), %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movswl	8(%rax), %ecx
	movq	%rax, %rsi
	movl	%ecx, %eax
	sarl	$5, %ecx
	je	.L829
	testw	%ax, %ax
	jns	.L831
	movl	12(%rsi), %ecx
.L831:
	movq	-280(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	16(%r14), %edx
	testl	%edx, %edx
	je	.L832
	.p2align 4,,10
	.p2align 3
.L848:
	leaq	8(%r14), %rdi
	xorl	%esi, %esi
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	movq	88(%r12), %rdi
	movq	%rbx, %rdx
	movq	%rax, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	-320(%rbp), %rsi
	movl	$-1, %eax
	xorl	%edx, %edx
	movq	-280(%rbp), %rdi
	movl	$1, %ecx
	movw	%ax, -264(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L847:
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r15, %rdi
	movl	$1, %esi
	movq	$0, 88(%r12)
	leaq	.LC4(%rip), %rax
	movl	$6, (%r12)
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	8(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L821:
	leaq	8(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	movl	$4, (%r12)
	movq	%rax, 88(%r12)
	jmp	.L820
.L845:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2579:
	.size	_ZN6icu_6722TransliteratorRegistry5regetERKNS_13UnicodeStringERNS_20TransliteratorParserERPNS_19TransliteratorAliasER10UErrorCode, .-_ZN6icu_6722TransliteratorRegistry5regetERKNS_13UnicodeStringERNS_20TransliteratorParserERPNS_19TransliteratorAliasER10UErrorCode
	.weak	_ZTSN6icu_6722TransliteratorRegistry11EnumerationE
	.section	.rodata._ZTSN6icu_6722TransliteratorRegistry11EnumerationE,"aG",@progbits,_ZTSN6icu_6722TransliteratorRegistry11EnumerationE,comdat
	.align 32
	.type	_ZTSN6icu_6722TransliteratorRegistry11EnumerationE, @object
	.size	_ZTSN6icu_6722TransliteratorRegistry11EnumerationE, 47
_ZTSN6icu_6722TransliteratorRegistry11EnumerationE:
	.string	"N6icu_6722TransliteratorRegistry11EnumerationE"
	.weak	_ZTIN6icu_6722TransliteratorRegistry11EnumerationE
	.section	.data.rel.ro._ZTIN6icu_6722TransliteratorRegistry11EnumerationE,"awG",@progbits,_ZTIN6icu_6722TransliteratorRegistry11EnumerationE,comdat
	.align 8
	.type	_ZTIN6icu_6722TransliteratorRegistry11EnumerationE, @object
	.size	_ZTIN6icu_6722TransliteratorRegistry11EnumerationE, 24
_ZTIN6icu_6722TransliteratorRegistry11EnumerationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722TransliteratorRegistry11EnumerationE
	.quad	_ZTIN6icu_6717StringEnumerationE
	.weak	_ZTVN6icu_6722TransliteratorRegistry11EnumerationE
	.section	.data.rel.ro._ZTVN6icu_6722TransliteratorRegistry11EnumerationE,"awG",@progbits,_ZTVN6icu_6722TransliteratorRegistry11EnumerationE,comdat
	.align 8
	.type	_ZTVN6icu_6722TransliteratorRegistry11EnumerationE, @object
	.size	_ZTVN6icu_6722TransliteratorRegistry11EnumerationE, 104
_ZTVN6icu_6722TransliteratorRegistry11EnumerationE:
	.quad	0
	.quad	_ZTIN6icu_6722TransliteratorRegistry11EnumerationE
	.quad	_ZN6icu_6722TransliteratorRegistry11EnumerationD1Ev
	.quad	_ZN6icu_6722TransliteratorRegistry11EnumerationD0Ev
	.quad	_ZNK6icu_6722TransliteratorRegistry11Enumeration17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringEnumeration5cloneEv
	.quad	_ZNK6icu_6722TransliteratorRegistry11Enumeration5countER10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration4nextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode
	.quad	_ZN6icu_6722TransliteratorRegistry11Enumeration5snextER10UErrorCode
	.quad	_ZN6icu_6722TransliteratorRegistry11Enumeration5resetER10UErrorCode
	.quad	_ZNK6icu_6717StringEnumerationeqERKS0_
	.quad	_ZNK6icu_6717StringEnumerationneERKS0_
	.section	.rodata
	.align 16
	.type	_ZN6icu_67L13TRANSLITERATEE, @object
	.size	_ZN6icu_67L13TRANSLITERATEE, 28
_ZN6icu_67L13TRANSLITERATEE:
	.value	84
	.value	114
	.value	97
	.value	110
	.value	115
	.value	108
	.value	105
	.value	116
	.value	101
	.value	114
	.value	97
	.value	116
	.value	101
	.value	0
	.align 32
	.type	_ZN6icu_67L18TRANSLITERATE_FROME, @object
	.size	_ZN6icu_67L18TRANSLITERATE_FROME, 36
_ZN6icu_67L18TRANSLITERATE_FROME:
	.value	84
	.value	114
	.value	97
	.value	110
	.value	115
	.value	108
	.value	105
	.value	116
	.value	101
	.value	114
	.value	97
	.value	116
	.value	101
	.value	70
	.value	114
	.value	111
	.value	109
	.value	0
	.align 32
	.type	_ZN6icu_67L16TRANSLITERATE_TOE, @object
	.size	_ZN6icu_67L16TRANSLITERATE_TOE, 32
_ZN6icu_67L16TRANSLITERATE_TOE:
	.value	84
	.value	114
	.value	97
	.value	110
	.value	115
	.value	108
	.value	105
	.value	116
	.value	101
	.value	114
	.value	97
	.value	116
	.value	101
	.value	84
	.value	111
	.value	0
	.local	_ZZN6icu_6722TransliteratorRegistry11Enumeration16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6722TransliteratorRegistry11Enumeration16getStaticClassIDEvE7classID,1,1
	.align 8
	.type	_ZL3LAT, @object
	.size	_ZL3LAT, 8
_ZL3LAT:
	.value	76
	.value	97
	.value	116
	.value	0
	.align 8
	.type	_ZL3ANY, @object
	.size	_ZL3ANY, 8
_ZL3ANY:
	.value	65
	.value	110
	.value	121
	.value	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
