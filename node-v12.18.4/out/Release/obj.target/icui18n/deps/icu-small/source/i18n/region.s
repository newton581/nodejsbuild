	.file	"region.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721RegionNameEnumeration17getDynamicClassIDEv
	.type	_ZNK6icu_6721RegionNameEnumeration17getDynamicClassIDEv, @function
_ZNK6icu_6721RegionNameEnumeration17getDynamicClassIDEv:
.LFB3151:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6721RegionNameEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3151:
	.size	_ZNK6icu_6721RegionNameEnumeration17getDynamicClassIDEv, .-_ZNK6icu_6721RegionNameEnumeration17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721RegionNameEnumeration5resetER10UErrorCode
	.type	_ZN6icu_6721RegionNameEnumeration5resetER10UErrorCode, @function
_ZN6icu_6721RegionNameEnumeration5resetER10UErrorCode:
.LFB3181:
	.cfi_startproc
	endbr64
	movl	$0, 116(%rdi)
	ret
	.cfi_endproc
.LFE3181:
	.size	_ZN6icu_6721RegionNameEnumeration5resetER10UErrorCode, .-_ZN6icu_6721RegionNameEnumeration5resetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721RegionNameEnumeration5countER10UErrorCode
	.type	_ZNK6icu_6721RegionNameEnumeration5countER10UErrorCode, @function
_ZNK6icu_6721RegionNameEnumeration5countER10UErrorCode:
.LFB3182:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L4
	movl	8(%rdx), %eax
.L4:
	ret
	.cfi_endproc
.LFE3182:
	.size	_ZNK6icu_6721RegionNameEnumeration5countER10UErrorCode, .-_ZNK6icu_6721RegionNameEnumeration5countER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721RegionNameEnumeration5snextER10UErrorCode
	.type	_ZN6icu_6721RegionNameEnumeration5snextER10UErrorCode, @function
_ZN6icu_6721RegionNameEnumeration5snextER10UErrorCode:
.LFB3180:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L18
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	120(%rdi), %rax
	testq	%rax, %rax
	je	.L8
	movl	116(%rdi), %esi
	movq	%rax, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	testq	%rax, %rax
	je	.L8
	addl	$1, 116(%rbx)
.L8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE3180:
	.size	_ZN6icu_6721RegionNameEnumeration5snextER10UErrorCode, .-_ZN6icu_6721RegionNameEnumeration5snextER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676RegionD2Ev
	.type	_ZN6icu_676RegionD2Ev, @function
_ZN6icu_676RegionD2Ev:
.LFB3160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676RegionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	96(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	104(%r12), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3160:
	.size	_ZN6icu_676RegionD2Ev, .-_ZN6icu_676RegionD2Ev
	.globl	_ZN6icu_676RegionD1Ev
	.set	_ZN6icu_676RegionD1Ev,_ZN6icu_676RegionD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721RegionNameEnumerationD2Ev
	.type	_ZN6icu_6721RegionNameEnumerationD2Ev, @function
_ZN6icu_6721RegionNameEnumerationD2Ev:
.LFB3184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721RegionNameEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L32
	movq	(%rdi), %rax
	call	*8(%rax)
.L32:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717StringEnumerationD2Ev@PLT
	.cfi_endproc
.LFE3184:
	.size	_ZN6icu_6721RegionNameEnumerationD2Ev, .-_ZN6icu_6721RegionNameEnumerationD2Ev
	.globl	_ZN6icu_6721RegionNameEnumerationD1Ev
	.set	_ZN6icu_6721RegionNameEnumerationD1Ev,_ZN6icu_6721RegionNameEnumerationD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721RegionNameEnumerationD0Ev
	.type	_ZN6icu_6721RegionNameEnumerationD0Ev, @function
_ZN6icu_6721RegionNameEnumerationD0Ev:
.LFB3186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721RegionNameEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L38
	movq	(%rdi), %rax
	call	*8(%rax)
.L38:
	movq	%r12, %rdi
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3186:
	.size	_ZN6icu_6721RegionNameEnumerationD0Ev, .-_ZN6icu_6721RegionNameEnumerationD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676RegionD0Ev
	.type	_ZN6icu_676RegionD0Ev, @function
_ZN6icu_676RegionD0Ev:
.LFB3162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676RegionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	96(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L44
	movq	(%rdi), %rax
	call	*8(%rax)
.L44:
	movq	104(%r12), %rdi
	testq	%rdi, %rdi
	je	.L45
	movq	(%rdi), %rax
	call	*8(%rax)
.L45:
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3162:
	.size	_ZN6icu_676RegionD0Ev, .-_ZN6icu_676RegionD0Ev
	.p2align 4
	.type	deleteRegion, @function
deleteRegion:
.LFB3148:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L53
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_676RegionD0Ev(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L55
	leaq	16+_ZTVN6icu_676RegionE(%rip), %rax
	movq	%rax, (%rdi)
	movq	96(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L56
	movq	(%rdi), %rax
	call	*8(%rax)
.L56:
	movq	104(%r12), %rdi
	testq	%rdi, %rdi
	je	.L57
	movq	(%rdi), %rax
	call	*8(%rax)
.L57:
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE3148:
	.size	deleteRegion, .-deleteRegion
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"metadata"
.LC1:
	.string	"alias"
.LC2:
	.string	"territory"
.LC3:
	.string	"supplementalData"
.LC4:
	.string	"codeMappings"
.LC5:
	.string	"idValidity"
.LC6:
	.string	"region"
.LC7:
	.string	"regular"
.LC8:
	.string	"macroregion"
.LC9:
	.string	"unknown"
.LC10:
	.string	"territoryContainment"
.LC11:
	.string	"001"
.LC12:
	.string	"grouping"
.LC13:
	.string	"replacement"
.LC14:
	.string	"containedGroupings"
.LC15:
	.string	"deprecated"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676Region14loadRegionDataER10UErrorCode
	.type	_ZN6icu_676Region14loadRegionDataER10UErrorCode, @function
_ZN6icu_676Region14loadRegionDataER10UErrorCode:
.LFB3152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$552, %rsp
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %r12
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	uhash_open_67@PLT
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movq	%rax, -408(%rbp)
	call	uhash_open_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%rax, -480(%rbp)
	call	uhash_open_67@PLT
	movl	$40, %edi
	movq	%rax, -488(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -512(%rbp)
	testq	%rax, %rax
	je	.L68
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	movq	%rbx, %rcx
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
.L69:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -464(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L70
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%rbx, %rcx
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
.L71:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L72
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%rbx, %rcx
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
.L72:
	movq	%rbx, %rdx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	movq	%r12, _ZN6icu_67L10allRegionsE(%rip)
	call	ures_openDirect_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -552(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -560(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rbx, %rdx
	leaq	.LC3(%rip), %rsi
	xorl	%edi, %edi
	movq	%rax, -448(%rbp)
	call	ures_openDirect_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%rax, -568(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, -544(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -576(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	movq	%rax, -584(%rbp)
	call	ures_getByKey_67@PLT
	movq	%r15, %rdi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, -432(%rbp)
	call	ures_getByKey_67@PLT
	movq	%r15, %rdi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, -440(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, -520(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	movq	%rax, -504(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r15, %rdi
	leaq	.LC12(%rip), %rsi
	movq	%rax, -528(%rbp)
	call	ures_getByKey_67@PLT
	movl	(%rbx), %r10d
	movq	%rax, -536(%rbp)
	testl	%r10d, %r10d
	jle	.L487
.L73:
	movq	-536(%rbp), %rax
	testq	%rax, %rax
	je	.L248
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L248:
	movq	-528(%rbp), %rax
	testq	%rax, %rax
	je	.L249
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L249:
	movq	-504(%rbp), %rax
	testq	%rax, %rax
	je	.L250
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L250:
	movq	-520(%rbp), %rax
	testq	%rax, %rax
	je	.L251
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L251:
	movq	-440(%rbp), %rax
	testq	%rax, %rax
	je	.L252
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L252:
	movq	-432(%rbp), %rax
	testq	%rax, %rax
	je	.L253
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L253:
	movq	-584(%rbp), %rax
	testq	%rax, %rax
	je	.L254
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L254:
	movq	-576(%rbp), %rax
	testq	%rax, %rax
	je	.L255
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L255:
	movq	-544(%rbp), %rax
	testq	%rax, %rax
	je	.L256
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L256:
	movq	-568(%rbp), %rax
	testq	%rax, %rax
	je	.L257
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L257:
	movq	-448(%rbp), %rax
	testq	%rax, %rax
	je	.L258
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L258:
	movq	-560(%rbp), %rax
	testq	%rax, %rax
	je	.L259
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L259:
	movq	-552(%rbp), %rax
	testq	%rax, %rax
	je	.L260
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L260:
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L261
	movq	(%rdi), %rax
	call	*8(%rax)
.L261:
	movq	-512(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L262
	movq	(%rdi), %rax
	call	*8(%rax)
.L262:
	movq	-488(%rbp), %rax
	testq	%rax, %rax
	je	.L263
	movq	%rax, %rdi
	call	uhash_close_67@PLT
.L263:
	movq	-480(%rbp), %rax
	testq	%rax, %rax
	je	.L264
	movq	%rax, %rdi
	call	uhash_close_67@PLT
.L264:
	movq	-408(%rbp), %rax
	testq	%rax, %rax
	je	.L67
	movq	%rax, %rdi
	call	uhash_close_67@PLT
.L67:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L488
	addq	$552, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	movq	-408(%rbp), %rdi
	leaq	deleteRegion(%rip), %rsi
	call	uhash_setValueDeleter_67@PLT
	movq	-488(%rbp), %rdi
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	call	uhash_setKeyDeleter_67@PLT
	movq	-432(%rbp), %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L74
	.p2align 4,,10
	.p2align 3
.L490:
	movq	-432(%rbp), %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movl	$2, %r8d
	movq	%rax, -128(%rbp)
	leaq	-396(%rbp), %rsi
	movw	%r8w, -120(%rbp)
	movl	$0, -396(%rbp)
	call	ures_getNextString_67@PLT
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L489
	leaq	-128(%rbp), %r14
	leaq	-392(%rbp), %r15
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L76:
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L77
	sarl	$5, %ecx
.L78:
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$126, %esi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movq	%rbx, %rcx
	movl	$6, %edx
	movq	%r15, %rsi
	leaq	-192(%rbp), %r13
	movq	%r14, %rdi
	movslq	%eax, %r12
	movq	%r13, -392(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	testl	%r12d, %r12d
	jle	.L79
	movzwl	-120(%rbp), %edx
	leal	1(%r12), %eax
	testw	%dx, %dx
	js	.L80
	movswl	%dx, %ecx
	leal	-1(%r12), %esi
	sarl	$5, %ecx
	cmpl	%ecx, %eax
	jnb	.L82
.L495:
	andl	$2, %edx
	leaq	-118(%rbp), %rcx
	cmove	-104(%rbp), %rcx
	cltq
	xorl	%edi, %edi
	movzwl	(%rcx,%rax,2), %eax
	movw	%di, -192(%rbp,%r12,2)
	movslq	%esi, %r12
	cmpw	-192(%rbp,%r12,2), %ax
	jb	.L85
	movq	%r14, -416(%rbp)
	movl	%eax, %r15d
	.p2align 4,,10
	.p2align 3
.L89:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L86
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
.L87:
	movq	_ZN6icu_67L10allRegionsE(%rip), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movzwl	-192(%rbp,%r12,2), %eax
	addl	$1, %eax
	movw	%ax, -192(%rbp,%r12,2)
	cmpw	%r15w, %ax
	jbe	.L89
	movq	-416(%rbp), %r14
.L85:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-432(%rbp), %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	jne	.L490
.L74:
	movq	-440(%rbp), %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L491
	.p2align 4,,10
	.p2align 3
.L467:
	movl	$2, %ecx
	movq	-440(%rbp), %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%edx, %edx
	movw	%cx, -120(%rbp)
	movq	%rbx, %rcx
	leaq	-396(%rbp), %rsi
	movq	%rax, -128(%rbp)
	movl	$0, -396(%rbp)
	call	ures_getNextString_67@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L492
	leaq	-128(%rbp), %r14
	leaq	-392(%rbp), %r15
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L95:
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L96
	sarl	$5, %ecx
.L97:
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$126, %esi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movq	%rbx, %rcx
	movl	$6, %edx
	movq	%r15, %rsi
	leaq	-192(%rbp), %r13
	movq	%r14, %rdi
	movslq	%eax, %r12
	movq	%r13, -392(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	testl	%r12d, %r12d
	jle	.L98
	movzwl	-120(%rbp), %edx
	leal	1(%r12), %eax
	testw	%dx, %dx
	js	.L99
	movswl	%dx, %ecx
	leal	-1(%r12), %esi
	sarl	$5, %ecx
	cmpl	%eax, %ecx
	jbe	.L101
.L494:
	andl	$2, %edx
	leaq	-118(%rbp), %rcx
	cmove	-104(%rbp), %rcx
	cltq
	xorl	%edx, %edx
	movzwl	(%rcx,%rax,2), %eax
	movw	%dx, -192(%rbp,%r12,2)
	movslq	%esi, %r12
	cmpw	-192(%rbp,%r12,2), %ax
	jb	.L104
	movq	%r14, -416(%rbp)
	movl	%eax, %r15d
	.p2align 4,,10
	.p2align 3
.L108:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L105
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
.L106:
	movq	_ZN6icu_67L10allRegionsE(%rip), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movzwl	-192(%rbp,%r12,2), %eax
	addl	$1, %eax
	movw	%ax, -192(%rbp,%r12,2)
	cmpw	%r15w, %ax
	jbe	.L108
	movq	-416(%rbp), %r14
.L104:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-440(%rbp), %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	jne	.L467
.L491:
	movq	-520(%rbp), %r14
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r12
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L112:
	leaq	-128(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L113:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L114
	movl	(%rbx), %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_i@PLT
.L114:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	_ZN6icu_67L10allRegionsE(%rip), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L93:
	movq	%r14, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L493
	movl	$2, %r13d
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	leaq	-396(%rbp), %rsi
	movq	%r12, -128(%rbp)
	movw	%r13w, -120(%rbp)
	movl	$0, -396(%rbp)
	call	ures_getNextString_67@PLT
	movl	(%rbx), %r15d
	testl	%r15d, %r15d
	jg	.L112
	movl	-396(%rbp), %ecx
	leaq	-128(%rbp), %r15
	leaq	-392(%rbp), %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rax, -392(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L113
.L86:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L87
	movl	$7, (%rbx)
	jmp	.L87
.L105:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L106
	movl	$7, (%rbx)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L90
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L91:
	movq	_ZN6icu_67L10allRegionsE(%rip), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L109
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L110:
	movq	_ZN6icu_67L10allRegionsE(%rip), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L77:
	movl	-116(%rbp), %ecx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L489:
	movl	-396(%rbp), %ecx
	leaq	-392(%rbp), %r15
	leaq	-128(%rbp), %r14
	movl	$1, %esi
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%rax, -392(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L96:
	movl	-116(%rbp), %ecx
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L492:
	movl	-396(%rbp), %ecx
	leaq	-392(%rbp), %r15
	leaq	-128(%rbp), %r14
	movl	$1, %esi
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%rax, -392(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L99:
	movl	-116(%rbp), %ecx
	leal	-1(%r12), %esi
	cmpl	%eax, %ecx
	ja	.L494
.L101:
	movslq	%r12d, %rax
	xorl	%edx, %edx
	movq	%r14, -416(%rbp)
	movslq	%esi, %r12
	movw	%dx, -192(%rbp,%rax,2)
	movl	$-1, %r15d
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L80:
	movl	-116(%rbp), %ecx
	leal	-1(%r12), %esi
	cmpl	%ecx, %eax
	jb	.L495
.L82:
	movslq	%r12d, %rax
	xorl	%ecx, %ecx
	movq	%r14, -416(%rbp)
	movslq	%esi, %r12
	movw	%cx, -192(%rbp,%rax,2)
	movl	$-1, %r15d
	jmp	.L89
.L90:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L91
	movl	$7, (%rbx)
	jmp	.L91
.L109:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L110
	movl	$7, (%rbx)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L493:
	movq	-528(%rbp), %r14
	movq	-512(%rbp), %r15
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r12
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L116:
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L111:
	movq	%r14, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L291
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L116
	movl	$2, %r10d
	movq	%r12, (%rax)
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movw	%r10w, 8(%rax)
	leaq	-396(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -396(%rbp)
	call	ures_getNextString_67@PLT
	movl	(%rbx), %r11d
	testl	%r11d, %r11d
	jg	.L117
	movl	-396(%rbp), %ecx
	leaq	-392(%rbp), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -392(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L291:
	movq	-536(%rbp), %r14
	xorl	%r12d, %r12d
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L496:
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	ures_getNextResource_67@PLT
	movl	(%rbx), %r9d
	movq	%rax, %r12
	testl	%r9d, %r9d
	jg	.L119
	movq	%rax, %rdi
	call	ures_getKey_67@PLT
	movl	$64, %edi
	movq	%rax, %r15
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L115
	movl	$-1, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movq	-464(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L115:
	movq	%r14, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	jne	.L496
.L119:
	movq	%r12, %rdi
	leaq	-392(%rbp), %r15
	xorl	%r12d, %r12d
	call	ures_close_67@PLT
	movq	_ZN6icu_67L10allRegionsE(%rip), %rax
	movl	8(%rax), %r8d
	testl	%r8d, %r8d
	jle	.L133
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$112, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L123
	leaq	16+_ZTVN6icu_676RegionE(%rip), %rax
	pxor	%xmm1, %xmm1
	movb	$0, 8(%r13)
	movl	$2, %esi
	movq	%rax, 0(%r13)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 16(%r13)
	movl	$4294967295, %eax
	movq	%rax, 80(%r13)
	movups	%xmm1, 88(%r13)
	movl	(%rbx), %edi
	movw	%si, 24(%r13)
	movq	$0, 104(%r13)
	testl	%edi, %edi
	jg	.L497
	movq	_ZN6icu_67L10allRegionsE(%rip), %rdi
	movl	%r12d, %esi
	leaq	16(%r13), %r14
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	24(%r13), %edx
	leaq	8(%r13), %rcx
	testw	%dx, %dx
	js	.L129
	sarl	$5, %edx
.L130:
	xorl	%r9d, %r9d
	movl	$4, %r8d
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movl	$1, 84(%r13)
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	$0, -392(%rbp)
	call	_ZN6icu_6711ICU_Utility17parseAsciiIntegerERKNS_13UnicodeStringERi@PLT
	movl	-392(%rbp), %ecx
	movl	%eax, %esi
	testl	%ecx, %ecx
	jg	.L498
	movq	-408(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movl	$-1, 80(%r13)
	addl	$1, %r12d
	call	uhash_put_67@PLT
	movq	_ZN6icu_67L10allRegionsE(%rip), %rax
	cmpl	8(%rax), %r12d
	jl	.L121
.L133:
	leaq	-396(%rbp), %rax
	movq	%rax, -496(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -416(%rbp)
	.p2align 4,,10
	.p2align 3
.L122:
	movq	-448(%rbp), %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L499
.L470:
	movq	-448(%rbp), %rdi
	movq	%rbx, %rdx
	xorl	%esi, %esi
	call	ures_getNextResource_67@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ures_getKey_67@PLT
	movl	$64, %edi
	movq	%rax, %r14
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L135
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
.L136:
	movq	-496(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -192(%rbp)
	movl	$2, %eax
	leaq	.LC13(%rip), %rsi
	movw	%ax, -184(%rbp)
	movl	$0, -396(%rbp)
	call	ures_getStringByKey_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L500
	movq	-416(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L138:
	testq	%r12, %r12
	je	.L139
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L139:
	movq	-408(%rbp), %r15
	movq	-416(%rbp), %rsi
	movq	%r15, %rdi
	call	uhash_get_67@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	uhash_get_67@PLT
	movq	%rax, %r12
	testq	%r14, %r14
	je	.L140
	testq	%rax, %rax
	jne	.L142
	movq	-488(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	uhash_put_67@PLT
	movq	-416(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-448(%rbp), %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	jne	.L470
.L499:
	leaq	-256(%rbp), %rax
	movq	-544(%rbp), %r12
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r14
	movq	%rax, -456(%rbp)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L179:
	movq	%r15, %rdi
	call	ures_close_67@PLT
.L134:
	movq	%r12, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L177
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	call	ures_getNextResource_67@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	ures_getType_67@PLT
	cmpl	$8, %eax
	jne	.L179
	movq	%r15, %rdi
	call	ures_getSize_67@PLT
	cmpl	$3, %eax
	jne	.L179
	movl	$2, %edi
	leaq	-396(%rbp), %r13
	movq	%rbx, %rcx
	xorl	%esi, %esi
	movw	%di, -248(%rbp)
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%r14, -256(%rbp)
	movl	$0, -396(%rbp)
	call	ures_getStringByIndex_67@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L501
	movq	-456(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L182:
	movl	$2, %ecx
	movl	$1, %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	movw	%cx, -184(%rbp)
	movq	%rbx, %rcx
	movq	%r14, -192(%rbp)
	movl	$0, -396(%rbp)
	call	ures_getStringByIndex_67@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L502
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L184:
	movl	$2, %eax
	movq	%r13, %rdx
	movq	%rbx, %rcx
	movl	$2, %esi
	movq	%r15, %rdi
	movq	%r14, -128(%rbp)
	movw	%ax, -120(%rbp)
	movl	$0, -396(%rbp)
	call	ures_getStringByIndex_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L503
	leaq	-128(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L186:
	movq	-456(%rbp), %rsi
	movq	-408(%rbp), %rdi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	movq	%rax, -424(%rbp)
	je	.L187
	movq	-416(%rbp), %rdi
	leaq	-392(%rbp), %rsi
	movl	$0, -392(%rbp)
	call	_ZN6icu_6711ICU_Utility17parseAsciiIntegerERKNS_13UnicodeStringERi@PLT
	movq	-424(%rbp), %rdx
	movl	%eax, %esi
	movl	-392(%rbp), %eax
	testl	%eax, %eax
	jle	.L188
	movl	%esi, 80(%rdx)
	movq	-480(%rbp), %rdi
	movq	%rbx, %rcx
	call	uhash_iput_67@PLT
	movq	-424(%rbp), %rdx
.L188:
	movl	$64, %edi
	movq	%rdx, -424(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-424(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L189
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rax, -424(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-424(%rbp), %r9
	movq	-472(%rbp), %rdx
.L190:
	movq	-488(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r9, %rsi
	call	uhash_put_67@PLT
.L187:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-416(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-456(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L140:
	testq	%rax, %rax
	jne	.L142
	movl	$112, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L143
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_676RegionE(%rip), %rax
	movb	$0, 8(%r12)
	movl	$2, %r14d
	movq	%rax, (%r12)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 16(%r12)
	movl	$4294967295, %eax
	movq	%rax, 80(%r12)
	movups	%xmm0, 88(%r12)
	movl	(%rbx), %r15d
	movw	%r14w, 24(%r12)
	movq	$0, 104(%r12)
	testl	%r15d, %r15d
	jg	.L504
	leaq	16(%r12), %r14
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	movzwl	24(%r12), %eax
	leaq	8(%r12), %rcx
	testw	%ax, %ax
	js	.L147
	movswl	%ax, %edx
	sarl	$5, %edx
.L148:
	xorl	%r9d, %r9d
	movl	$4, %r8d
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	-408(%rbp), %rdi
	call	uhash_put_67@PLT
	leaq	-392(%rbp), %rdx
	movq	%r14, %rdi
	movl	$0, -392(%rbp)
	movq	%rdx, %rsi
	call	_ZN6icu_6711ICU_Utility17parseAsciiIntegerERKNS_13UnicodeStringERi@PLT
	movl	-392(%rbp), %r11d
	movl	%eax, %esi
	testl	%r11d, %r11d
	jg	.L505
	movl	$-1, 80(%r12)
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$6, 84(%r12)
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L156
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	movq	%rbx, %rcx
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	movl	(%rbx), %eax
	movq	%r14, 104(%r12)
	testl	%eax, %eax
	jg	.L153
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r10d
	xorl	%r15d, %r15d
	movq	%r12, -456(%rbp)
	leaq	-392(%rbp), %rcx
	movq	%rax, -128(%rbp)
	leaq	-128(%rbp), %r14
	movzwl	-184(%rbp), %eax
	movq	%r13, -472(%rbp)
	movq	%rcx, %r12
	movq	%r15, %r13
	movw	%r10w, -120(%rbp)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L506:
	movswl	%ax, %edx
	sarl	$5, %edx
	cmpl	%r13d, %edx
	jle	.L160
.L507:
	movl	%r13d, %r15d
	jbe	.L293
	testb	$2, %al
	jne	.L162
	movq	-168(%rbp), %rdx
	movzwl	(%rdx,%r13,2), %edx
	cmpw	$32, %dx
	je	.L163
.L161:
	movw	%dx, -392(%rbp)
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	-184(%rbp), %eax
.L163:
	testw	%ax, %ax
	js	.L164
	movswl	%ax, %edx
	sarl	$5, %edx
.L165:
	cmpl	%edx, %r15d
	jnb	.L166
	testb	$2, %al
	leaq	-182(%rbp), %rcx
	cmove	-168(%rbp), %rcx
	cmpw	$32, (%rcx,%r13,2)
	je	.L168
.L166:
	leal	1(%r15), %r9d
	cmpl	%r9d, %edx
	je	.L168
.L169:
	addq	$1, %r13
.L174:
	testw	%ax, %ax
	jns	.L506
	movl	-180(%rbp), %edx
	cmpl	%r13d, %edx
	jg	.L507
.L160:
	movq	%r14, %rdi
	movq	-472(%rbp), %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-416(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r13, %r13
	je	.L122
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L168:
	movq	-408(%rbp), %rdi
	movq	%r14, %rsi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	movq	%rax, -424(%rbp)
	je	.L170
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L171
	movq	-424(%rbp), %rsi
	movq	%rax, %rdi
	addq	$16, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L172:
	movq	-456(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	104(%rax), %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L170:
	movzwl	-120(%rbp), %eax
	testb	$1, %al
	je	.L173
	movl	$2, %r9d
	movzwl	-184(%rbp), %eax
	movw	%r9w, -120(%rbp)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L164:
	movl	-180(%rbp), %edx
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L162:
	movq	-416(%rbp), %rcx
	movzwl	10(%rcx,%r13,2), %edx
	cmpw	$32, %dx
	jne	.L161
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L173:
	andl	$31, %eax
	movw	%ax, -120(%rbp)
	movzwl	-184(%rbp), %eax
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L293:
	movl	$-1, %edx
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L498:
	movl	%eax, 80(%r13)
	movq	%rbx, %rcx
	movq	%r13, %rdx
	addl	$1, %r12d
	movq	-480(%rbp), %rdi
	call	uhash_iput_67@PLT
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movl	$4, 84(%r13)
	movq	-408(%rbp), %rdi
	call	uhash_put_67@PLT
	movq	_ZN6icu_67L10allRegionsE(%rip), %rax
	cmpl	%r12d, 8(%rax)
	jg	.L121
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L129:
	movl	28(%r13), %edx
	jmp	.L130
.L171:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L172
	movl	$7, (%rbx)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L500:
	movl	-396(%rbp), %ecx
	movq	-416(%rbp), %rdi
	leaq	-392(%rbp), %rdx
	movl	$1, %esi
	movq	%rax, -392(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L138
.L123:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L73
	movl	$7, (%rbx)
	jmp	.L73
.L135:
	movl	(%rbx), %r14d
	testl	%r14d, %r14d
	jg	.L136
	movl	$7, (%rbx)
	jmp	.L136
.L156:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L508
	movq	$0, 104(%r12)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L70:
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L71
	movl	$7, (%rbx)
	jmp	.L71
.L68:
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L69
	movl	$7, (%rbx)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L508:
	movl	$7, (%rbx)
	movq	$0, 104(%r12)
.L153:
	movq	-416(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r13, %r13
	je	.L73
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	jmp	.L73
.L505:
	movl	%eax, 80(%r12)
	movq	-480(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r12, %rdx
	call	uhash_iput_67@PLT
	jmp	.L142
.L147:
	movl	28(%r12), %edx
	jmp	.L148
.L497:
	movq	24+_ZTVN6icu_676RegionE(%rip), %rax
	leaq	_ZN6icu_676RegionD0Ev(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L509
	movq	96(%r13), %rdi
	testq	%rdi, %rdi
	je	.L127
	movq	(%rdi), %rax
	call	*8(%rax)
.L127:
	movq	104(%r13), %rdi
	testq	%rdi, %rdi
	je	.L128
	movq	(%rdi), %rax
	call	*8(%rax)
.L128:
	leaq	16(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L73
.L177:
	leaq	-384(%rbp), %rax
	leaq	_ZN6icu_67L8WORLD_IDE(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	movq	%rax, -496(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-408(%rbp), %rdi
	movq	%r15, %rsi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L191
	movl	$2, 84(%rax)
.L191:
	leaq	-320(%rbp), %rax
	leaq	_ZN6icu_67L17UNKNOWN_REGION_IDE(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	movq	%rax, -592(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-408(%rbp), %rdi
	movq	%r15, %rsi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L192
	movl	$0, 84(%rax)
.L192:
	movq	-512(%rbp), %rax
	movq	-408(%rbp), %r14
	xorl	%r12d, %r12d
	movl	8(%rax), %r15d
	movq	%rax, %r13
	testl	%r15d, %r15d
	jle	.L199
	.p2align 4,,10
	.p2align 3
.L193:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L197
	movl	$3, 84(%rax)
	addl	$1, %r12d
	cmpl	8(%r13), %r12d
	jl	.L193
.L199:
	movq	-464(%rbp), %rax
	xorl	%r12d, %r12d
	movl	8(%rax), %r14d
	testl	%r14d, %r14d
	jle	.L202
	movq	-408(%rbp), %r14
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L194:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L200
	movl	$5, 84(%rax)
	addl	$1, %r12d
	cmpl	8(%r13), %r12d
	jl	.L194
.L202:
	movq	-456(%rbp), %r15
	leaq	_ZN6icu_67L26OUTLYING_OCEANIA_REGION_IDE(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-408(%rbp), %rdi
	movq	%r15, %rsi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L209
	movl	$4, 84(%rax)
	.p2align 4,,10
	.p2align 3
.L209:
	movq	-504(%rbp), %r15
	movq	%r15, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L203
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	ures_getNextResource_67@PLT
	movl	(%rbx), %r13d
	movq	%rax, %r12
	testl	%r13d, %r13d
	jg	.L204
	movq	%rax, %rdi
	call	ures_getKey_67@PLT
	movl	$19, %ecx
	leaq	.LC14(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L486
	movl	$11, %ecx
	movq	%rax, %rsi
	leaq	.LC15(%rip), %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L206
.L486:
	testq	%r12, %r12
	je	.L209
	movq	%r12, %rdi
	call	ures_close_67@PLT
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L200:
	addl	$1, %r12d
	cmpl	8(%r13), %r12d
	jl	.L194
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L197:
	addl	$1, %r12d
	cmpl	8(%r13), %r12d
	jl	.L193
	jmp	.L199
.L206:
	leaq	-192(%rbp), %rcx
	movq	%rax, %rsi
	movl	$-1, %edx
	movq	%rcx, %r15
	movq	%rcx, -472(%rbp)
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movq	-408(%rbp), %rdi
	movq	%r15, %rsi
	xorl	%r15d, %r15d
	call	uhash_get_67@PLT
	movq	%rax, %r13
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L211:
	leaq	-128(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L212:
	movq	-408(%rbp), %rdi
	movq	%r14, %rsi
	call	uhash_get_67@PLT
	movq	%rax, -416(%rbp)
	testq	%r13, %r13
	je	.L215
	testq	%rax, %rax
	je	.L215
	cmpq	$0, 96(%r13)
	je	.L510
.L216:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L218
	movl	(%rbx), %r9d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edi
	movq	%rax, (%r8)
	movw	%di, 8(%r8)
	testl	%r9d, %r9d
	jg	.L511
	movq	-416(%rbp), %rax
	movq	%r8, %rdi
	movq	%r8, -424(%rbp)
	leaq	16(%rax), %rsi
	call	_ZN6icu_6713UnicodeString12fastCopyFromERKS0_@PLT
	movq	-424(%rbp), %r8
	movq	96(%r13), %rdi
	movq	%rbx, %rdx
	movq	%r8, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	cmpl	$5, 84(%r13)
	je	.L215
	movq	-416(%rbp), %rax
	movq	%r13, 88(%rax)
.L215:
	movq	%r14, %rdi
	addl	$1, %r15d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L222:
	movq	%r12, %rdi
	call	ures_getSize_67@PLT
	cmpl	%r15d, %eax
	jle	.L210
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rbx, %rcx
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	$2, %r10d
	movq	%rax, -128(%rbp)
	leaq	-396(%rbp), %rdx
	movw	%r10w, -120(%rbp)
	movl	$0, -396(%rbp)
	call	ures_getStringByIndex_67@PLT
	movl	(%rbx), %r11d
	testl	%r11d, %r11d
	jg	.L211
	movl	-396(%rbp), %ecx
	leaq	-128(%rbp), %r14
	leaq	-392(%rbp), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, -392(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L212
.L218:
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L278
	movl	$7, (%rbx)
.L278:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-472(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L204:
	testq	%r12, %r12
	je	.L225
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L225:
	movq	-456(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-592(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-496(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L73
.L510:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L217
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	movq	%rbx, %rcx
	movq	%rax, -424(%rbp)
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	movq	-424(%rbp), %rax
.L217:
	movq	%rax, 96(%r13)
	jmp	.L216
.L143:
	movl	(%rbx), %r15d
	testl	%r15d, %r15d
	jg	.L153
	movl	$7, (%rbx)
	jmp	.L153
.L210:
	movq	-472(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L486
.L511:
	movq	%r8, %rdi
	call	*24+_ZTVN6icu_6713UnicodeStringE(%rip)
	jmp	.L278
.L203:
	leaq	-392(%rbp), %r14
	leaq	_ZN6icu_67L16availableRegionsE(%rip), %r13
	movl	$-1, -392(%rbp)
	movq	%r14, %r15
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L227:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L230
	leaq	16(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L512
	movslq	84(%r12), %rax
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	0(%r13,%rax,8), %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L233:
	movq	-408(%rbp), %rdi
	movq	%r15, %rsi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	je	.L226
	movq	8(%rax), %r12
	movslq	84(%r12), %rax
	cmpq	$0, 0(%r13,%rax,8)
	jne	.L227
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L228
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%rbx, %rcx
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
.L229:
	movslq	84(%r12), %rdx
	movq	%r14, 0(%r13,%rdx,8)
	jmp	.L227
.L230:
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jg	.L225
	movl	$7, (%rbx)
	jmp	.L225
.L509:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L73
.L226:
	leaq	region_cleanup(%rip), %rsi
	movl	$33, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movq	-480(%rbp), %rax
	movq	-456(%rbp), %rdi
	movq	%rax, _ZN6icu_67L14numericCodeMapE(%rip)
	movq	-408(%rbp), %rax
	movq	%rax, _ZN6icu_67L11regionIDMapE(%rip)
	movq	-488(%rbp), %rax
	movq	%rax, _ZN6icu_67L13regionAliasesE(%rip)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-592(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-496(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-536(%rbp), %rax
	testq	%rax, %rax
	je	.L234
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L234:
	movq	-528(%rbp), %rax
	testq	%rax, %rax
	je	.L235
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L235:
	movq	-504(%rbp), %rax
	testq	%rax, %rax
	je	.L236
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L236:
	movq	-520(%rbp), %rax
	testq	%rax, %rax
	je	.L237
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L237:
	movq	-440(%rbp), %rax
	testq	%rax, %rax
	je	.L238
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L238:
	movq	-432(%rbp), %rax
	testq	%rax, %rax
	je	.L239
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L239:
	movq	-584(%rbp), %rax
	testq	%rax, %rax
	je	.L240
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L240:
	movq	-576(%rbp), %rax
	testq	%rax, %rax
	je	.L241
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L241:
	movq	-544(%rbp), %rax
	testq	%rax, %rax
	je	.L242
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L242:
	movq	-568(%rbp), %rax
	testq	%rax, %rax
	je	.L243
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L243:
	movq	-448(%rbp), %rax
	testq	%rax, %rax
	je	.L244
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L244:
	movq	-560(%rbp), %rax
	testq	%rax, %rax
	je	.L245
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L245:
	movq	-552(%rbp), %rax
	testq	%rax, %rax
	je	.L246
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L246:
	movq	-464(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-512(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L67
.L503:
	leaq	-392(%rbp), %rsi
	movl	-396(%rbp), %ecx
	leaq	-128(%rbp), %r13
	movq	%rax, -392(%rbp)
	movq	%rsi, %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L186
.L502:
	movq	%rax, -392(%rbp)
	leaq	-392(%rbp), %rsi
	leaq	-192(%rbp), %rax
	movl	-396(%rbp), %ecx
	movq	%rsi, %rdx
	movq	%rax, %rdi
	movl	$1, %esi
	movq	%rax, -416(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L184
.L501:
	leaq	-392(%rbp), %rsi
	movl	-396(%rbp), %ecx
	movq	-456(%rbp), %rdi
	movq	%rax, -392(%rbp)
	movq	%rsi, %rdx
	movl	$1, %esi
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L182
.L228:
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L229
	movl	$7, (%rbx)
	jmp	.L229
.L504:
	movq	24+_ZTVN6icu_676RegionE(%rip), %rax
	leaq	_ZN6icu_676RegionD0Ev(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L513
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L151
	movq	(%rdi), %rax
	call	*8(%rax)
.L151:
	movq	104(%r12), %rdi
	testq	%rdi, %rdi
	je	.L152
	movq	(%rdi), %rax
	call	*8(%rax)
.L152:
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L153
.L189:
	movl	(%rbx), %r11d
	testl	%r11d, %r11d
	jg	.L190
	movl	$7, (%rbx)
	jmp	.L190
.L513:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L153
.L488:
	call	__stack_chk_fail@PLT
.L512:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	jmp	.L225
	.cfi_endproc
.LFE3152:
	.size	_ZN6icu_676Region14loadRegionDataER10UErrorCode, .-_ZN6icu_676Region14loadRegionDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721RegionNameEnumeration16getStaticClassIDEv
	.type	_ZN6icu_6721RegionNameEnumeration16getStaticClassIDEv, @function
_ZN6icu_6721RegionNameEnumeration16getStaticClassIDEv:
.LFB3150:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6721RegionNameEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3150:
	.size	_ZN6icu_6721RegionNameEnumeration16getStaticClassIDEv, .-_ZN6icu_6721RegionNameEnumeration16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676Region17cleanupRegionDataEv
	.type	_ZN6icu_676Region17cleanupRegionDataEv, @function
_ZN6icu_676Region17cleanupRegionDataEv:
.LFB3155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	_ZN6icu_67L16availableRegionsE(%rip), %rbx
	leaq	56(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L517:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L516
	movq	(%rdi), %rax
	call	*8(%rax)
.L516:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L517
	movq	_ZN6icu_67L13regionAliasesE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L518
	call	uhash_close_67@PLT
.L518:
	movq	_ZN6icu_67L14numericCodeMapE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L519
	call	uhash_close_67@PLT
.L519:
	movq	_ZN6icu_67L11regionIDMapE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L520
	call	uhash_close_67@PLT
.L520:
	movq	_ZN6icu_67L10allRegionsE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L521
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	movq	_ZN6icu_67L10allRegionsE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L522
	movq	(%rdi), %rax
	call	*8(%rax)
.L522:
	movq	$0, _ZN6icu_67L10allRegionsE(%rip)
.L521:
	movq	$0, _ZN6icu_67L11regionIDMapE(%rip)
	movq	$0, _ZN6icu_67L14numericCodeMapE(%rip)
	movq	$0, _ZN6icu_67L13regionAliasesE(%rip)
	movl	$0, _ZN6icu_67L19gRegionDataInitOnceE(%rip)
	mfence
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3155:
	.size	_ZN6icu_676Region17cleanupRegionDataEv, .-_ZN6icu_676Region17cleanupRegionDataEv
	.p2align 4
	.type	region_cleanup, @function
region_cleanup:
.LFB3149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676Region17cleanupRegionDataEv
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3149:
	.size	region_cleanup, .-region_cleanup
	.align 2
	.p2align 4
	.globl	_ZN6icu_676RegionC2Ev
	.type	_ZN6icu_676RegionC2Ev, @function
_ZN6icu_676RegionC2Ev:
.LFB3157:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676RegionE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 104(%rdi)
	movq	%rax, (%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 16(%rdi)
	movl	$2, %eax
	movw	%ax, 24(%rdi)
	movl	$4294967295, %eax
	movq	%rax, 80(%rdi)
	movb	$0, 8(%rdi)
	movups	%xmm0, 88(%rdi)
	ret
	.cfi_endproc
.LFE3157:
	.size	_ZN6icu_676RegionC2Ev, .-_ZN6icu_676RegionC2Ev
	.globl	_ZN6icu_676RegionC1Ev
	.set	_ZN6icu_676RegionC1Ev,_ZN6icu_676RegionC2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676RegioneqERKS0_
	.type	_ZNK6icu_676RegioneqERKS0_, @function
_ZNK6icu_676RegioneqERKS0_:
.LFB3163:
	.cfi_startproc
	endbr64
	movswl	24(%rdi), %edx
	movswl	24(%rsi), %eax
	movl	%edx, %ecx
	movl	%eax, %r8d
	andl	$1, %r8d
	andl	$1, %ecx
	jne	.L560
	testw	%dx, %dx
	js	.L548
	sarl	$5, %edx
	testw	%ax, %ax
	js	.L550
.L565:
	sarl	$5, %eax
.L551:
	andl	$1, %r8d
	jne	.L561
	cmpl	%edx, %eax
	je	.L564
.L561:
	movl	%ecx, %r8d
.L560:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L548:
	movl	28(%rdi), %edx
	testw	%ax, %ax
	jns	.L565
.L550:
	movl	28(%rsi), %eax
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L564:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rsi
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	setne	%cl
	movl	%ecx, %eax
	ret
	.cfi_endproc
.LFE3163:
	.size	_ZNK6icu_676RegioneqERKS0_, .-_ZNK6icu_676RegioneqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676RegionneERKS0_
	.type	_ZNK6icu_676RegionneERKS0_, @function
_ZNK6icu_676RegionneERKS0_:
.LFB3164:
	.cfi_startproc
	endbr64
	movswl	24(%rsi), %ecx
	movswl	24(%rdi), %edx
	movl	%ecx, %eax
	andl	$1, %eax
	testb	$1, %dl
	je	.L567
	xorl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	testw	%dx, %dx
	js	.L569
	sarl	$5, %edx
	testw	%cx, %cx
	js	.L571
.L586:
	sarl	$5, %ecx
.L572:
	testb	%al, %al
	jne	.L574
	cmpl	%edx, %ecx
	je	.L585
.L574:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	movl	28(%rdi), %edx
	testw	%cx, %cx
	jns	.L586
.L571:
	movl	28(%rsi), %ecx
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L585:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rsi
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	sete	%al
	ret
	.cfi_endproc
.LFE3164:
	.size	_ZNK6icu_676RegionneERKS0_, .-_ZNK6icu_676RegionneERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Region19getContainingRegionEv
	.type	_ZNK6icu_676Region19getContainingRegionEv, @function
_ZNK6icu_676Region19getContainingRegionEv:
.LFB3168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	movl	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L598
.L589:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	movq	88(%rbx), %rax
	jne	.L599
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L598:
	.cfi_restore_state
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L589
	leaq	-28(%rbp), %rdi
	call	_ZN6icu_676Region14loadRegionDataER10UErrorCode
	movl	-28(%rbp), %eax
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L19gRegionDataInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L589
.L599:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3168:
	.size	_ZNK6icu_676Region19getContainingRegionEv, .-_ZNK6icu_676Region19getContainingRegionEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Region19getContainingRegionE11URegionType
	.type	_ZNK6icu_676Region19getContainingRegionE11URegionType, @function
_ZNK6icu_676Region19getContainingRegionE11URegionType:
.LFB3169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	movl	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L615
.L601:
	movl	4+_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L602
	movl	%eax, -28(%rbp)
.L602:
	movq	88(%rbx), %rax
	testq	%rax, %rax
	je	.L600
	cmpl	%r12d, 84(%rax)
	jne	.L616
.L600:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L617
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L616:
	.cfi_restore_state
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZNK6icu_676Region19getContainingRegionE11URegionType
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L615:
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L601
	leaq	-28(%rbp), %rdi
	call	_ZN6icu_676Region14loadRegionDataER10UErrorCode
	movl	-28(%rbp), %eax
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L19gRegionDataInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L602
.L617:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3169:
	.size	_ZNK6icu_676Region19getContainingRegionE11URegionType, .-_ZNK6icu_676Region19getContainingRegionE11URegionType
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Region8containsERKS0_
	.type	_ZNK6icu_676Region8containsERKS0_, @function
_ZNK6icu_676Region8containsERKS0_:
.LFB3172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	movl	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L640
.L619:
	movl	4+_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L620
	movl	%eax, -44(%rbp)
.L620:
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L621
	xorl	%edx, %edx
	leaq	16(%r12), %rsi
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	testl	%eax, %eax
	jns	.L626
	movq	96(%rbx), %rdi
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L621
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L625:
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	_ZN6icu_67L11regionIDMapE(%rip), %rdi
	movq	%rax, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L627
	movq	%r12, %rsi
	call	_ZNK6icu_676Region8containsERKS0_
	testb	%al, %al
	jne	.L626
.L627:
	movq	96(%rbx), %rdi
	addl	$1, %r13d
	cmpl	8(%rdi), %r13d
	jl	.L625
.L621:
	xorl	%eax, %eax
.L618:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L641
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L626:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L640:
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L619
	leaq	-44(%rbp), %rdi
	call	_ZN6icu_676Region14loadRegionDataER10UErrorCode
	movl	-44(%rbp), %eax
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L19gRegionDataInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L620
.L641:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3172:
	.size	_ZNK6icu_676Region8containsERKS0_, .-_ZNK6icu_676Region8containsERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Region13getRegionCodeEv
	.type	_ZNK6icu_676Region13getRegionCodeEv, @function
_ZNK6icu_676Region13getRegionCodeEv:
.LFB3174:
	.cfi_startproc
	endbr64
	leaq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE3174:
	.size	_ZNK6icu_676Region13getRegionCodeEv, .-_ZNK6icu_676Region13getRegionCodeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Region14getNumericCodeEv
	.type	_ZNK6icu_676Region14getNumericCodeEv, @function
_ZNK6icu_676Region14getNumericCodeEv:
.LFB3175:
	.cfi_startproc
	endbr64
	movl	80(%rdi), %eax
	ret
	.cfi_endproc
.LFE3175:
	.size	_ZNK6icu_676Region14getNumericCodeEv, .-_ZNK6icu_676Region14getNumericCodeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Region7getTypeEv
	.type	_ZNK6icu_676Region7getTypeEv, @function
_ZNK6icu_676Region7getTypeEv:
.LFB3176:
	.cfi_startproc
	endbr64
	movl	84(%rdi), %eax
	ret
	.cfi_endproc
.LFE3176:
	.size	_ZNK6icu_676Region7getTypeEv, .-_ZNK6icu_676Region7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721RegionNameEnumerationC2EPNS_7UVectorER10UErrorCode
	.type	_ZN6icu_6721RegionNameEnumerationC2EPNS_7UVectorER10UErrorCode, @function
_ZN6icu_6721RegionNameEnumerationC2EPNS_7UVectorER10UErrorCode:
.LFB3178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	leaq	16+_ZTVN6icu_6721RegionNameEnumerationE(%rip), %rax
	movl	$0, 116(%rbx)
	movq	%rax, (%rbx)
	testq	%r13, %r13
	je	.L646
	movl	(%r14), %edx
	testl	%edx, %edx
	jle	.L658
.L646:
	movq	$0, 120(%rbx)
.L645:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L658:
	.cfi_restore_state
	movl	$40, %edi
	movl	8(%r13), %r15d
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L648
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	%r14, %r8
	movl	%r15d, %ecx
	movq	%rax, %rdi
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
.L648:
	movl	8(%r13), %eax
	movq	%r12, 120(%rbx)
	testl	%eax, %eax
	jle	.L645
	xorl	%r15d, %r15d
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L659:
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	addl	$1, %r15d
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	120(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	cmpl	%r15d, 8(%r13)
	jle	.L645
.L653:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	$64, %edi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L659
	movq	120(%rbx), %rdi
	movq	%r14, %rdx
	xorl	%esi, %esi
	addl	$1, %r15d
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	cmpl	%r15d, 8(%r13)
	jg	.L653
	jmp	.L645
	.cfi_endproc
.LFE3178:
	.size	_ZN6icu_6721RegionNameEnumerationC2EPNS_7UVectorER10UErrorCode, .-_ZN6icu_6721RegionNameEnumerationC2EPNS_7UVectorER10UErrorCode
	.globl	_ZN6icu_6721RegionNameEnumerationC1EPNS_7UVectorER10UErrorCode
	.set	_ZN6icu_6721RegionNameEnumerationC1EPNS_7UVectorER10UErrorCode,_ZN6icu_6721RegionNameEnumerationC2EPNS_7UVectorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676Region12getAvailableE11URegionTypeR10UErrorCode
	.type	_ZN6icu_676Region12getAvailableE11URegionTypeR10UErrorCode, @function
_ZN6icu_676Region12getAvailableE11URegionTypeR10UErrorCode:
.LFB3167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L675
.L661:
	xorl	%r13d, %r13d
.L660:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L675:
	.cfi_restore_state
	movl	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	movl	%edi, %ebx
	movq	%rsi, %r12
	cmpl	$2, %eax
	jne	.L676
.L662:
	movl	4+_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L663
	movl	%eax, (%r12)
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L676:
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L662
	movq	%r12, %rdi
	call	_ZN6icu_676Region14loadRegionDataER10UErrorCode
	movl	(%r12), %eax
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L19gRegionDataInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L663:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L661
	movl	$128, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L660
	movslq	%ebx, %rdi
	leaq	_ZN6icu_67L16availableRegionsE(%rip), %rax
	movq	%r12, %rdx
	movq	(%rax,%rdi,8), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6721RegionNameEnumerationC1EPNS_7UVectorER10UErrorCode
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3167:
	.size	_ZN6icu_676Region12getAvailableE11URegionTypeR10UErrorCode, .-_ZN6icu_676Region12getAvailableE11URegionTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Region19getContainedRegionsER10UErrorCode
	.type	_ZNK6icu_676Region19getContainedRegionsER10UErrorCode, @function
_ZNK6icu_676Region19getContainedRegionsER10UErrorCode:
.LFB3170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L692
.L678:
	xorl	%r13d, %r13d
.L677:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_restore_state
	movl	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	cmpl	$2, %eax
	jne	.L693
.L679:
	movl	4+_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L680
	movl	%eax, (%r12)
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L693:
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L679
	movq	%r12, %rdi
	call	_ZN6icu_676Region14loadRegionDataER10UErrorCode
	movl	(%r12), %eax
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L19gRegionDataInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L680:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L678
	movl	$128, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L677
	movq	96(%rbx), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6721RegionNameEnumerationC1EPNS_7UVectorER10UErrorCode
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3170:
	.size	_ZNK6icu_676Region19getContainedRegionsER10UErrorCode, .-_ZNK6icu_676Region19getContainedRegionsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Region18getPreferredValuesER10UErrorCode
	.type	_ZNK6icu_676Region18getPreferredValuesER10UErrorCode, @function
_ZNK6icu_676Region18getPreferredValuesER10UErrorCode:
.LFB3173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L709
.L695:
	xorl	%r13d, %r13d
.L694:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L709:
	.cfi_restore_state
	movl	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	cmpl	$2, %eax
	jne	.L710
.L696:
	movl	4+_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L697
	movl	%eax, (%r12)
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L710:
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L696
	movq	%r12, %rdi
	call	_ZN6icu_676Region14loadRegionDataER10UErrorCode
	movl	(%r12), %eax
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L19gRegionDataInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L697:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L695
	cmpl	$6, 84(%rbx)
	jne	.L695
	movl	$128, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L694
	movq	104(%rbx), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6721RegionNameEnumerationC1EPNS_7UVectorER10UErrorCode
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3173:
	.size	_ZNK6icu_676Region18getPreferredValuesER10UErrorCode, .-_ZNK6icu_676Region18getPreferredValuesER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_676Region11getInstanceEPKcR10UErrorCode.part.0, @function
_ZN6icu_676Region11getInstanceEPKcR10UErrorCode.part.0:
.LFB4227:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	subq	$80, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movq	_ZN6icu_67L11regionIDMapE(%rip), %rdi
	movq	%r13, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L724
.L712:
	cmpl	$6, 84(%r12)
	je	.L725
.L713:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L726
	addq	$80, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L725:
	.cfi_restore_state
	movq	104(%r12), %rax
	cmpl	$1, 8(%rax)
	jne	.L713
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_676Region18getPreferredValuesER10UErrorCode
	leaq	_ZN6icu_6721RegionNameEnumeration5resetER10UErrorCode(%rip), %rcx
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L714
	movl	$0, 116(%r15)
.L715:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*56(%rax)
	movq	_ZN6icu_67L11regionIDMapE(%rip), %rdi
	movq	%rax, %rsi
	call	uhash_get_67@PLT
	leaq	_ZN6icu_6721RegionNameEnumerationD0Ev(%rip), %rdx
	movq	%rax, %r12
	movq	(%r15), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L716
	movq	120(%r15), %rdi
	leaq	16+_ZTVN6icu_6721RegionNameEnumerationE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L717
	movq	(%rdi), %rax
	call	*8(%rax)
.L717:
	movq	%r15, %rdi
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L724:
	movq	_ZN6icu_67L13regionAliasesE(%rip), %rdi
	movq	%r13, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L712
	movl	$1, (%r14)
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L714:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%rdx
	movq	(%r15), %rax
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L716:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L713
.L726:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4227:
	.size	_ZN6icu_676Region11getInstanceEPKcR10UErrorCode.part.0, .-_ZN6icu_676Region11getInstanceEPKcR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_676Region11getInstanceEiR10UErrorCode
	.type	_ZN6icu_676Region11getInstanceEiR10UErrorCode, @function
_ZN6icu_676Region11getInstanceEiR10UErrorCode:
.LFB3166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$104, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L749
.L728:
	xorl	%eax, %eax
.L727:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L750
	addq	$104, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L749:
	.cfi_restore_state
	movl	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	movl	%edi, %r13d
	movq	%rsi, %r12
	cmpl	$2, %eax
	jne	.L751
.L729:
	movl	4+_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L730
	movl	%eax, (%r12)
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L751:
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L729
	movq	%r12, %rdi
	call	_ZN6icu_676Region14loadRegionDataER10UErrorCode
	movl	(%r12), %eax
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L19gRegionDataInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L730:
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L728
	movq	_ZN6icu_67L14numericCodeMapE(%rip), %rdi
	movl	%r13d, %esi
	call	uhash_iget_67@PLT
	testq	%rax, %rax
	je	.L752
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L728
.L732:
	cmpl	$6, 84(%rax)
	jne	.L727
	movq	104(%rax), %rdx
	cmpl	$1, 8(%rdx)
	jne	.L727
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_676Region18getPreferredValuesER10UErrorCode
	leaq	_ZN6icu_6721RegionNameEnumeration5resetER10UErrorCode(%rip), %rcx
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L734
	movl	$0, 116(%r13)
.L735:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*56(%rax)
	movq	_ZN6icu_67L11regionIDMapE(%rip), %rdi
	movq	%rax, %rsi
	call	uhash_get_67@PLT
	movq	0(%r13), %rdx
	leaq	_ZN6icu_6721RegionNameEnumerationD0Ev(%rip), %rcx
	movq	8(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L736
	movq	120(%r13), %rdi
	leaq	16+_ZTVN6icu_6721RegionNameEnumerationE(%rip), %rdx
	movq	%rdx, 0(%r13)
	testq	%rdi, %rdi
	je	.L737
	movq	(%rdi), %rdx
	movq	%rax, -120(%rbp)
	call	*8(%rdx)
	movq	-120(%rbp), %rax
.L737:
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-120(%rbp), %rax
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L752:
	leaq	-112(%rbp), %r14
	movl	$2, %ecx
	movl	$10, %edx
	movl	%r13d, %esi
	movw	%cx, -104(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii@PLT
	movq	_ZN6icu_67L13regionAliasesE(%rip), %rdi
	movq	%r14, %rsi
	call	uhash_get_67@PLT
	movq	%r14, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L728
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	jne	.L732
	movl	$1, (%r12)
	jmp	.L727
.L736:
	movq	%rax, -120(%rbp)
	movq	%r13, %rdi
	call	*%rdx
	movq	-120(%rbp), %rax
	jmp	.L727
.L734:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*%rdx
	movq	0(%r13), %rax
	jmp	.L735
.L750:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3166:
	.size	_ZN6icu_676Region11getInstanceEiR10UErrorCode, .-_ZN6icu_676Region11getInstanceEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676Region11getInstanceEPKcR10UErrorCode
	.type	_ZN6icu_676Region11getInstanceEPKcR10UErrorCode, @function
_ZN6icu_676Region11getInstanceEPKcR10UErrorCode:
.LFB3165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L776
.L754:
	xorl	%r13d, %r13d
.L753:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L777
	addq	$80, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L776:
	.cfi_restore_state
	movl	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	movq	%rdi, %r13
	movq	%rsi, %r12
	cmpl	$2, %eax
	jne	.L778
.L755:
	movl	4+_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L756
	movl	%eax, (%r12)
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L778:
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L755
	movq	%r12, %rdi
	call	_ZN6icu_676Region14loadRegionDataER10UErrorCode
	movl	(%r12), %eax
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L19gRegionDataInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L756:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L754
	testq	%r13, %r13
	je	.L779
	leaq	-112(%rbp), %r14
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movq	_ZN6icu_67L11regionIDMapE(%rip), %rdi
	movq	%r14, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L780
.L759:
	cmpl	$6, 84(%r13)
	je	.L781
.L760:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L781:
	movq	104(%r13), %rax
	cmpl	$1, 8(%rax)
	jne	.L760
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_676Region18getPreferredValuesER10UErrorCode
	leaq	_ZN6icu_6721RegionNameEnumeration5resetER10UErrorCode(%rip), %rcx
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L761
	movl	$0, 116(%r15)
.L762:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*56(%rax)
	movq	_ZN6icu_67L11regionIDMapE(%rip), %rdi
	movq	%rax, %rsi
	call	uhash_get_67@PLT
	leaq	_ZN6icu_6721RegionNameEnumerationD0Ev(%rip), %rdx
	movq	%rax, %r13
	movq	(%r15), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L763
	movq	120(%r15), %rdi
	leaq	16+_ZTVN6icu_6721RegionNameEnumerationE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L764
	movq	(%rdi), %rax
	call	*8(%rax)
.L764:
	movq	%r15, %rdi
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L779:
	movl	$1, (%r12)
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L780:
	movq	_ZN6icu_67L13regionAliasesE(%rip), %rdi
	movq	%r14, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L759
	movl	$1, (%r12)
	jmp	.L760
.L763:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L760
.L761:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*%rdx
	movq	(%r15), %rax
	jmp	.L762
.L777:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3165:
	.size	_ZN6icu_676Region11getInstanceEPKcR10UErrorCode, .-_ZN6icu_676Region11getInstanceEPKcR10UErrorCode
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB16:
	.text
.LHOTB16:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Region19getContainedRegionsE11URegionTypeR10UErrorCode
	.type	_ZNK6icu_676Region19getContainedRegionsE11URegionTypeR10UErrorCode, @function
_ZNK6icu_676Region19getContainedRegionsE11URegionTypeR10UErrorCode:
.LFB3171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r10d
	movl	%esi, -56(%rbp)
	testl	%r10d, %r10d
	jle	.L869
.L783:
	xorl	%r12d, %r12d
.L782:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L869:
	.cfi_restore_state
	movl	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	movq	%rdi, %rbx
	movq	%rdx, %r15
	cmpl	$2, %eax
	jne	.L870
.L784:
	movl	4+_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L785
	movl	%eax, (%r15)
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L870:
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L784
	movq	%r15, %rdi
	call	_ZN6icu_676Region14loadRegionDataER10UErrorCode
	movl	(%r15), %eax
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L19gRegionDataInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L785:
	movl	(%r15), %r9d
	testl	%r9d, %r9d
	jg	.L783
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L786
	movq	uhash_compareChars_67@GOTPCREL(%rip), %rdx
	movq	%r15, %rcx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
.L786:
	movl	(%r15), %r8d
	testl	%r8d, %r8d
	jle	.L871
.L787:
	xorl	%r14d, %r14d
.L790:
	movl	$0, -52(%rbp)
.L812:
	movq	(%r14), %rax
	leaq	_ZNK6icu_6721RegionNameEnumeration5countER10UErrorCode(%rip), %rbx
	movq	32(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.L791
	movq	120(%r14), %rcx
	testq	%rcx, %rcx
	je	.L792
	movl	8(%rcx), %ecx
.L793:
	cmpl	-52(%rbp), %ecx
	jle	.L792
	xorl	%esi, %esi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	*40(%rax)
	movl	(%r15), %esi
	movq	%rax, %r12
	testl	%esi, %esi
	jg	.L798
	movl	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L795
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L795
	movq	%r15, %rdi
	call	_ZN6icu_676Region14loadRegionDataER10UErrorCode
	movl	(%r15), %eax
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L19gRegionDataInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L796:
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L798
	testq	%r12, %r12
	je	.L867
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_676Region11getInstanceEPKcR10UErrorCode.part.0
	movq	%rax, %rdi
	movl	-56(%rbp), %eax
	cmpl	84(%rdi), %eax
	jne	.L799
	leaq	16(%rdi), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L800:
	addl	$1, -52(%rbp)
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L799:
	movl	%eax, %esi
	movq	%r15, %rdx
	xorl	%ebx, %ebx
	call	_ZNK6icu_676Region19getContainedRegionsE11URegionTypeR10UErrorCode
	movq	%r14, -64(%rbp)
	movq	%r13, %r14
	movl	%ebx, %r13d
	movq	%rax, %rbx
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L804:
	xorl	%r12d, %r12d
.L808:
	leaq	16(%r12), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	addl	$1, %r13d
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L809:
	movq	(%rbx), %rax
	leaq	_ZNK6icu_6721RegionNameEnumeration5countER10UErrorCode(%rip), %rdx
	movq	32(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L801
	movq	120(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L802
	movl	8(%rcx), %ecx
.L803:
	cmpl	%r13d, %ecx
	jle	.L802
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*40(%rax)
	movl	(%r15), %edx
	movq	%rax, %r12
	testl	%edx, %edx
	jg	.L804
	movl	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L805
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L805
	movq	%r15, %rdi
	call	_ZN6icu_676Region14loadRegionDataER10UErrorCode
	movl	(%r15), %eax
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L19gRegionDataInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L806:
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L804
	testq	%r12, %r12
	je	.L872
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_676Region11getInstanceEPKcR10UErrorCode.part.0
	movq	%rax, %r12
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L805:
	movl	4+_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L806
	movl	%eax, (%r15)
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L801:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	*%rcx
	movl	%eax, %ecx
	movq	(%rbx), %rax
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L802:
	movq	8(%rax), %rax
	movq	%rbx, %r12
	leaq	_ZN6icu_6721RegionNameEnumerationD0Ev(%rip), %rbx
	movq	%r14, %r13
	movq	-64(%rbp), %r14
	cmpq	%rbx, %rax
	jne	.L810
	movq	120(%r12), %rdi
	leaq	16+_ZTVN6icu_6721RegionNameEnumerationE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L811
	movq	(%rdi), %rax
	call	*8(%rax)
.L811:
	movq	%r12, %rdi
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L872:
	movl	$1, (%r15)
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L795:
	movl	4+_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L796
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L792:
	movq	8(%rax), %rax
	leaq	_ZN6icu_6721RegionNameEnumerationD0Ev(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L813
	movq	120(%r14), %rdi
	leaq	16+_ZTVN6icu_6721RegionNameEnumerationE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L814
	movq	(%rdi), %rax
	call	*8(%rax)
.L814:
	movq	%r14, %rdi
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L815:
	movl	$128, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L816
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6721RegionNameEnumerationC1EPNS_7UVectorER10UErrorCode
.L816:
	testq	%r13, %r13
	je	.L782
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L791:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*%rcx
	movl	%eax, %ecx
	movq	(%r14), %rax
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L871:
	movl	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L873
.L788:
	movl	4+_ZN6icu_67L19gRegionDataInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L789
	movl	%eax, (%r15)
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L873:
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L788
	movq	%r15, %rdi
	call	_ZN6icu_676Region14loadRegionDataER10UErrorCode
	movl	(%r15), %eax
	leaq	_ZN6icu_67L19gRegionDataInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L19gRegionDataInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L789:
	movl	(%r15), %edi
	testl	%edi, %edi
	jg	.L787
	movl	$128, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L790
	movq	96(%rbx), %rsi
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6721RegionNameEnumerationC1EPNS_7UVectorER10UErrorCode
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L810:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L813:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L815
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_676Region19getContainedRegionsE11URegionTypeR10UErrorCode.cold, @function
_ZNK6icu_676Region19getContainedRegionsE11URegionTypeR10UErrorCode.cold:
.LFSB3171:
.L866:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	%eax, (%r15)
.L798:
	movl	84, %eax
	ud2
.L867:
	movl	$1, (%r15)
	jmp	.L798
	.cfi_endproc
.LFE3171:
	.text
	.size	_ZNK6icu_676Region19getContainedRegionsE11URegionTypeR10UErrorCode, .-_ZNK6icu_676Region19getContainedRegionsE11URegionTypeR10UErrorCode
	.section	.text.unlikely
	.size	_ZNK6icu_676Region19getContainedRegionsE11URegionTypeR10UErrorCode.cold, .-_ZNK6icu_676Region19getContainedRegionsE11URegionTypeR10UErrorCode.cold
.LCOLDE16:
	.text
.LHOTE16:
	.weak	_ZTSN6icu_676RegionE
	.section	.rodata._ZTSN6icu_676RegionE,"aG",@progbits,_ZTSN6icu_676RegionE,comdat
	.align 16
	.type	_ZTSN6icu_676RegionE, @object
	.size	_ZTSN6icu_676RegionE, 17
_ZTSN6icu_676RegionE:
	.string	"N6icu_676RegionE"
	.weak	_ZTIN6icu_676RegionE
	.section	.data.rel.ro._ZTIN6icu_676RegionE,"awG",@progbits,_ZTIN6icu_676RegionE,comdat
	.align 8
	.type	_ZTIN6icu_676RegionE, @object
	.size	_ZTIN6icu_676RegionE, 24
_ZTIN6icu_676RegionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676RegionE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6721RegionNameEnumerationE
	.section	.rodata._ZTSN6icu_6721RegionNameEnumerationE,"aG",@progbits,_ZTSN6icu_6721RegionNameEnumerationE,comdat
	.align 32
	.type	_ZTSN6icu_6721RegionNameEnumerationE, @object
	.size	_ZTSN6icu_6721RegionNameEnumerationE, 33
_ZTSN6icu_6721RegionNameEnumerationE:
	.string	"N6icu_6721RegionNameEnumerationE"
	.weak	_ZTIN6icu_6721RegionNameEnumerationE
	.section	.data.rel.ro._ZTIN6icu_6721RegionNameEnumerationE,"awG",@progbits,_ZTIN6icu_6721RegionNameEnumerationE,comdat
	.align 8
	.type	_ZTIN6icu_6721RegionNameEnumerationE, @object
	.size	_ZTIN6icu_6721RegionNameEnumerationE, 24
_ZTIN6icu_6721RegionNameEnumerationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6721RegionNameEnumerationE
	.quad	_ZTIN6icu_6717StringEnumerationE
	.weak	_ZTVN6icu_676RegionE
	.section	.data.rel.ro._ZTVN6icu_676RegionE,"awG",@progbits,_ZTVN6icu_676RegionE,comdat
	.align 8
	.type	_ZTVN6icu_676RegionE, @object
	.size	_ZTVN6icu_676RegionE, 40
_ZTVN6icu_676RegionE:
	.quad	0
	.quad	_ZTIN6icu_676RegionE
	.quad	_ZN6icu_676RegionD1Ev
	.quad	_ZN6icu_676RegionD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.weak	_ZTVN6icu_6721RegionNameEnumerationE
	.section	.data.rel.ro._ZTVN6icu_6721RegionNameEnumerationE,"awG",@progbits,_ZTVN6icu_6721RegionNameEnumerationE,comdat
	.align 8
	.type	_ZTVN6icu_6721RegionNameEnumerationE, @object
	.size	_ZTVN6icu_6721RegionNameEnumerationE, 104
_ZTVN6icu_6721RegionNameEnumerationE:
	.quad	0
	.quad	_ZTIN6icu_6721RegionNameEnumerationE
	.quad	_ZN6icu_6721RegionNameEnumerationD1Ev
	.quad	_ZN6icu_6721RegionNameEnumerationD0Ev
	.quad	_ZNK6icu_6721RegionNameEnumeration17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringEnumeration5cloneEv
	.quad	_ZNK6icu_6721RegionNameEnumeration5countER10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration4nextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode
	.quad	_ZN6icu_6721RegionNameEnumeration5snextER10UErrorCode
	.quad	_ZN6icu_6721RegionNameEnumeration5resetER10UErrorCode
	.quad	_ZNK6icu_6717StringEnumerationeqERKS0_
	.quad	_ZNK6icu_6717StringEnumerationneERKS0_
	.local	_ZZN6icu_6721RegionNameEnumeration16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6721RegionNameEnumeration16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.align 8
	.type	_ZN6icu_67L8WORLD_IDE, @object
	.size	_ZN6icu_67L8WORLD_IDE, 8
_ZN6icu_67L8WORLD_IDE:
	.value	48
	.value	48
	.value	49
	.value	0
	.align 2
	.type	_ZN6icu_67L26OUTLYING_OCEANIA_REGION_IDE, @object
	.size	_ZN6icu_67L26OUTLYING_OCEANIA_REGION_IDE, 6
_ZN6icu_67L26OUTLYING_OCEANIA_REGION_IDE:
	.value	81
	.value	79
	.value	0
	.align 2
	.type	_ZN6icu_67L17UNKNOWN_REGION_IDE, @object
	.size	_ZN6icu_67L17UNKNOWN_REGION_IDE, 6
_ZN6icu_67L17UNKNOWN_REGION_IDE:
	.value	90
	.value	90
	.value	0
	.local	_ZN6icu_67L10allRegionsE
	.comm	_ZN6icu_67L10allRegionsE,8,8
	.local	_ZN6icu_67L14numericCodeMapE
	.comm	_ZN6icu_67L14numericCodeMapE,8,8
	.local	_ZN6icu_67L11regionIDMapE
	.comm	_ZN6icu_67L11regionIDMapE,8,8
	.local	_ZN6icu_67L13regionAliasesE
	.comm	_ZN6icu_67L13regionAliasesE,8,8
	.local	_ZN6icu_67L16availableRegionsE
	.comm	_ZN6icu_67L16availableRegionsE,56,32
	.local	_ZN6icu_67L19gRegionDataInitOnceE
	.comm	_ZN6icu_67L19gRegionDataInitOnceE,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
