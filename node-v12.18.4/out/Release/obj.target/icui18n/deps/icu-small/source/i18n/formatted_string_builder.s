	.file	"formatted_string_builder.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilderC2Ev
	.type	_ZN6icu_6722FormattedStringBuilderC2Ev, @function
_ZN6icu_6722FormattedStringBuilderC2Ev:
.LFB2436:
	.cfi_startproc
	endbr64
	movb	$0, (%rdi)
	movq	$20, 128(%rdi)
	ret
	.cfi_endproc
.LFE2436:
	.size	_ZN6icu_6722FormattedStringBuilderC2Ev, .-_ZN6icu_6722FormattedStringBuilderC2Ev
	.globl	_ZN6icu_6722FormattedStringBuilderC1Ev
	.set	_ZN6icu_6722FormattedStringBuilderC1Ev,_ZN6icu_6722FormattedStringBuilderC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilderD2Ev
	.type	_ZN6icu_6722FormattedStringBuilderD2Ev, @function
_ZN6icu_6722FormattedStringBuilderD2Ev:
.LFB2439:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	jne	.L8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	88(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2439:
	.size	_ZN6icu_6722FormattedStringBuilderD2Ev, .-_ZN6icu_6722FormattedStringBuilderD2Ev
	.globl	_ZN6icu_6722FormattedStringBuilderD1Ev
	.set	_ZN6icu_6722FormattedStringBuilderD1Ev,_ZN6icu_6722FormattedStringBuilderD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilderaSERKS0_
	.type	_ZN6icu_6722FormattedStringBuilderaSERKS0_, @function
_ZN6icu_6722FormattedStringBuilderaSERKS0_:
.LFB2444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L10
	cmpb	$0, (%rdi)
	movq	%rsi, %rbx
	jne	.L40
.L11:
	movzbl	(%rbx), %r13d
	testb	%r13b, %r13b
	je	.L12
	movslq	16(%rbx), %r14
	leaq	(%r14,%r14), %r15
	cmpl	$40, %r14d
	jg	.L41
	movq	8(%rbx), %rsi
.L26:
	leaq	8(%r12), %rdi
	xorl	%eax, %eax
.L19:
	testq	%r15, %r15
	jne	.L42
.L20:
	leaq	88(%rbx), %rsi
	testb	%r13b, %r13b
	je	.L22
	movq	88(%rbx), %rsi
.L22:
	leaq	88(%r12), %rdi
	testb	%al, %al
	je	.L24
	movq	88(%r12), %rdi
.L24:
	testq	%r14, %r14
	jne	.L43
.L25:
	movl	128(%rbx), %eax
	movl	%eax, 128(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
.L10:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	leaq	8(%rbx), %rsi
	movl	$40, %r14d
	movl	$80, %r15d
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%r15, %rdx
	call	memcpy@PLT
	movzbl	(%r12), %eax
	movzbl	(%rbx), %r13d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%r14, %rdx
	call	memcpy@PLT
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L40:
	movq	8(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	88(%r12), %rdi
	call	uprv_free_67@PLT
	movb	$0, (%r12)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%r15, %rdi
	movl	%r14d, -204(%rbp)
	call	uprv_malloc_67@PLT
	movq	%r14, %rdi
	movq	%rax, -200(%rbp)
	call	uprv_malloc_67@PLT
	movq	-200(%rbp), %r8
	testq	%r8, %r8
	je	.L28
	testq	%rax, %rax
	movl	-204(%rbp), %edx
	je	.L28
	movb	$1, (%r12)
	leaq	8(%rbx), %rsi
	movl	%edx, 16(%r12)
	movl	%edx, 96(%r12)
	movzbl	(%rbx), %edx
	movq	%r8, 8(%r12)
	movq	%rax, 88(%r12)
	testb	%dl, %dl
	jne	.L45
.L18:
	movl	%r13d, %eax
	movq	8(%r12), %rdi
	movl	%edx, %r13d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L45:
	movq	8(%rbx), %rsi
	jmp	.L18
.L44:
	call	__stack_chk_fail@PLT
.L28:
	movq	%r8, %rdi
	movq	%rax, -200(%rbp)
	call	uprv_free_67@PLT
	movq	-200(%rbp), %rax
	movq	%rax, %rdi
	call	uprv_free_67@PLT
	leaq	-192(%rbp), %rsi
	movq	%r12, %rdi
	movb	$0, -192(%rbp)
	movq	$20, -64(%rbp)
	call	_ZN6icu_6722FormattedStringBuilderaSERKS0_
	cmpb	$0, -192(%rbp)
	je	.L10
	movq	-184(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-104(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L10
	.cfi_endproc
.LFE2444:
	.size	_ZN6icu_6722FormattedStringBuilderaSERKS0_, .-_ZN6icu_6722FormattedStringBuilderaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilderC2ERKS0_
	.type	_ZN6icu_6722FormattedStringBuilderC2ERKS0_, @function
_ZN6icu_6722FormattedStringBuilderC2ERKS0_:
.LFB2442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, (%rdi)
	movq	$20, 128(%rdi)
	cmpq	%rsi, %rdi
	je	.L46
	movzbl	(%rsi), %r13d
	movq	%rdi, %r12
	movq	%rsi, %rbx
	testb	%r13b, %r13b
	je	.L48
	movslq	16(%rsi), %r14
	leaq	(%r14,%r14), %r15
	cmpl	$40, %r14d
	jg	.L75
	movq	8(%rsi), %rsi
.L62:
	leaq	8(%r12), %rdi
	xorl	%eax, %eax
.L55:
	testq	%r15, %r15
	jne	.L76
.L56:
	leaq	88(%rbx), %rsi
	testb	%r13b, %r13b
	je	.L58
	movq	88(%rbx), %rsi
.L58:
	leaq	88(%r12), %rdi
	testb	%al, %al
	je	.L60
	movq	88(%r12), %rdi
.L60:
	testq	%r14, %r14
	jne	.L77
.L61:
	movq	128(%rbx), %rax
	movq	%rax, 128(%r12)
.L46:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	leaq	8(%rsi), %rsi
	movl	$40, %r14d
	movl	$80, %r15d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r15, %rdx
	call	memcpy@PLT
	movzbl	(%rbx), %r13d
	movzbl	(%r12), %eax
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%r14, %rdx
	call	memcpy@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r15, %rdi
	movl	%r14d, -204(%rbp)
	call	uprv_malloc_67@PLT
	movq	%r14, %rdi
	movq	%rax, -200(%rbp)
	call	uprv_malloc_67@PLT
	movq	-200(%rbp), %r8
	testq	%r8, %r8
	je	.L64
	testq	%rax, %rax
	movl	-204(%rbp), %edx
	je	.L64
	movl	%edx, 16(%r12)
	leaq	8(%rbx), %rsi
	movl	%edx, 96(%r12)
	movzbl	(%rbx), %edx
	movb	$1, (%r12)
	movq	%r8, 8(%r12)
	movq	%rax, 88(%r12)
	testb	%dl, %dl
	jne	.L79
.L54:
	movl	%r13d, %eax
	movq	8(%r12), %rdi
	movl	%edx, %r13d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L79:
	movq	8(%rbx), %rsi
	jmp	.L54
.L78:
	call	__stack_chk_fail@PLT
.L64:
	movq	%r8, %rdi
	movq	%rax, -200(%rbp)
	call	uprv_free_67@PLT
	movq	-200(%rbp), %rax
	movq	%rax, %rdi
	call	uprv_free_67@PLT
	leaq	-192(%rbp), %rsi
	movq	%r12, %rdi
	movb	$0, -192(%rbp)
	movq	$20, -64(%rbp)
	call	_ZN6icu_6722FormattedStringBuilderaSERKS0_
	cmpb	$0, -192(%rbp)
	je	.L46
	movq	-184(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-104(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L46
	.cfi_endproc
.LFE2442:
	.size	_ZN6icu_6722FormattedStringBuilderC2ERKS0_, .-_ZN6icu_6722FormattedStringBuilderC2ERKS0_
	.globl	_ZN6icu_6722FormattedStringBuilderC1ERKS0_
	.set	_ZN6icu_6722FormattedStringBuilderC1ERKS0_,_ZN6icu_6722FormattedStringBuilderC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722FormattedStringBuilder6lengthEv
	.type	_ZNK6icu_6722FormattedStringBuilder6lengthEv, @function
_ZNK6icu_6722FormattedStringBuilder6lengthEv:
.LFB2445:
	.cfi_startproc
	endbr64
	movl	132(%rdi), %eax
	ret
	.cfi_endproc
.LFE2445:
	.size	_ZNK6icu_6722FormattedStringBuilder6lengthEv, .-_ZNK6icu_6722FormattedStringBuilder6lengthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722FormattedStringBuilder14codePointCountEv
	.type	_ZNK6icu_6722FormattedStringBuilder14codePointCountEv, @function
_ZNK6icu_6722FormattedStringBuilder14codePointCountEv:
.LFB2446:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	movl	132(%rdi), %esi
	leaq	8(%rdi), %rax
	je	.L83
	movq	8(%rdi), %rax
.L83:
	movslq	128(%rdi), %rdx
	leaq	(%rax,%rdx,2), %rdi
	jmp	u_countChar32_67@PLT
	.cfi_endproc
.LFE2446:
	.size	_ZNK6icu_6722FormattedStringBuilder14codePointCountEv, .-_ZNK6icu_6722FormattedStringBuilder14codePointCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722FormattedStringBuilder17getFirstCodePointEv
	.type	_ZNK6icu_6722FormattedStringBuilder17getFirstCodePointEv, @function
_ZNK6icu_6722FormattedStringBuilder17getFirstCodePointEv:
.LFB2447:
	.cfi_startproc
	endbr64
	movl	132(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L88
	cmpb	$0, (%rdi)
	leaq	8(%rdi), %rdx
	jne	.L95
.L87:
	movslq	128(%rdi), %rax
	movzwl	(%rdx,%rax,2), %r8d
	leaq	(%rax,%rax), %rdi
	movl	%r8d, %esi
	movl	%r8d, %eax
	andl	$63488, %esi
	cmpl	$55296, %esi
	je	.L96
.L84:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	movq	8(%rdi), %rdx
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L96:
	testb	$4, %ah
	jne	.L84
	cmpl	$1, %ecx
	je	.L84
	movzwl	2(%rdx,%rdi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L84
	sall	$10, %r8d
	leal	-56613888(%rax,%r8), %r8d
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L88:
	movl	$-1, %r8d
	jmp	.L84
	.cfi_endproc
.LFE2447:
	.size	_ZNK6icu_6722FormattedStringBuilder17getFirstCodePointEv, .-_ZNK6icu_6722FormattedStringBuilder17getFirstCodePointEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722FormattedStringBuilder16getLastCodePointEv
	.type	_ZNK6icu_6722FormattedStringBuilder16getLastCodePointEv, @function
_ZNK6icu_6722FormattedStringBuilder16getLastCodePointEv:
.LFB2448:
	.cfi_startproc
	endbr64
	movl	132(%rdi), %esi
	testl	%esi, %esi
	je	.L106
	cmpb	$0, (%rdi)
	leaq	8(%rdi), %r8
	jne	.L116
.L100:
	movslq	128(%rdi), %r9
	leal	-1(%rsi), %ecx
	movslq	%ecx, %rax
	addq	%r9, %rax
	movzwl	(%r8,%rax,2), %edi
	leaq	(%rax,%rax), %r11
	movl	%edi, %r10d
	movl	%edi, %edx
	andl	$64512, %r10d
	cmpl	$56320, %r10d
	jne	.L101
	testl	%ecx, %ecx
	jg	.L117
.L101:
	movl	%edi, %r10d
	movl	%edi, %r9d
	andl	$-2048, %r10d
	cmpl	$55296, %r10d
	je	.L118
.L97:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	movq	8(%rdi), %r8
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L118:
	andb	$4, %dh
	jne	.L103
.L104:
	addl	$1, %ecx
	cmpl	%ecx, %esi
	je	.L97
	movzwl	2(%r8,%rax,2), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L97
	sall	$10, %edi
	leal	-56613888(%rax,%rdi), %r9d
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L117:
	movzwl	-2(%r8,%r11), %r10d
	andl	$-1024, %r10d
	cmpl	$55296, %r10d
	je	.L119
	movl	%edi, %r9d
	andb	$4, %dh
	je	.L104
.L105:
	movzwl	-2(%r8,%r11), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L97
	sall	$10, %eax
	leal	-56613888(%rax,%rdi), %r9d
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L103:
	testl	%ecx, %ecx
	jle	.L97
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L119:
	leal	-2(%rsi), %ecx
	movslq	%ecx, %rax
	addq	%r9, %rax
	movzwl	(%r8,%rax,2), %edi
	leaq	(%rax,%rax), %r11
	movl	%edi, %edx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L106:
	movl	$-1, %r9d
	jmp	.L97
	.cfi_endproc
.LFE2448:
	.size	_ZNK6icu_6722FormattedStringBuilder16getLastCodePointEv, .-_ZNK6icu_6722FormattedStringBuilder16getLastCodePointEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722FormattedStringBuilder11codePointAtEi
	.type	_ZNK6icu_6722FormattedStringBuilder11codePointAtEi, @function
_ZNK6icu_6722FormattedStringBuilder11codePointAtEi:
.LFB2449:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	leaq	8(%rdi), %rdx
	je	.L122
	movq	8(%rdi), %rdx
.L122:
	movslq	128(%rdi), %rax
	movslq	%esi, %rcx
	addq	%rcx, %rax
	movzwl	(%rdx,%rax,2), %r8d
	leaq	(%rax,%rax), %r9
	movl	%r8d, %ecx
	movl	%r8d, %eax
	andl	$63488, %ecx
	cmpl	$55296, %ecx
	je	.L125
.L120:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	testb	$4, %ah
	jne	.L124
	addl	$1, %esi
	cmpl	132(%rdi), %esi
	je	.L120
	movzwl	2(%rdx,%r9), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L120
	sall	$10, %r8d
	leal	-56613888(%rax,%r8), %r8d
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L124:
	testl	%esi, %esi
	jle	.L120
	movzwl	-2(%rdx,%r9), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L120
	sall	$10, %eax
	leal	-56613888(%r8,%rax), %r8d
	jmp	.L120
	.cfi_endproc
.LFE2449:
	.size	_ZNK6icu_6722FormattedStringBuilder11codePointAtEi, .-_ZNK6icu_6722FormattedStringBuilder11codePointAtEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722FormattedStringBuilder15codePointBeforeEi
	.type	_ZNK6icu_6722FormattedStringBuilder15codePointBeforeEi, @function
_ZNK6icu_6722FormattedStringBuilder15codePointBeforeEi:
.LFB2450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, (%rdi)
	leaq	8(%rdi), %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	je	.L128
	movq	8(%rdi), %r11
.L128:
	movslq	128(%rdi), %r8
	leal	-1(%rsi), %r10d
	movslq	%r10d, %rax
	addq	%r8, %rax
	movzwl	(%r11,%rax,2), %ecx
	leaq	(%rax,%rax), %rbx
	movl	%ecx, %r9d
	movl	%ecx, %edx
	andl	$64512, %r9d
	cmpl	$56320, %r9d
	jne	.L129
	testl	%r10d, %r10d
	jg	.L146
.L129:
	movl	%ecx, %esi
	movl	%ecx, %r8d
	andl	$-2048, %esi
	cmpl	$55296, %esi
	je	.L147
.L126:
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movzwl	-2(%r11,%rbx), %r9d
	andl	$-1024, %r9d
	cmpl	$55296, %r9d
	je	.L148
	movl	%ecx, %r8d
	andb	$4, %dh
	je	.L133
.L134:
	movzwl	-2(%r11,%rbx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L126
	sall	$10, %eax
	leal	-56613888(%rax,%rcx), %r8d
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L147:
	andb	$4, %dh
	jne	.L132
.L133:
	addl	$1, %r10d
	cmpl	132(%rdi), %r10d
	je	.L126
	movzwl	2(%r11,%rax,2), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L126
	sall	$10, %ecx
	leal	-56613888(%rax,%rcx), %r8d
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L132:
	testl	%r10d, %r10d
	jle	.L126
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L148:
	leal	-2(%rsi), %r10d
	movslq	%r10d, %rax
	addq	%r8, %rax
	movzwl	(%r11,%rax,2), %ecx
	leaq	(%rax,%rax), %rbx
	movl	%ecx, %edx
	jmp	.L129
	.cfi_endproc
.LFE2450:
	.size	_ZNK6icu_6722FormattedStringBuilder15codePointBeforeEi, .-_ZNK6icu_6722FormattedStringBuilder15codePointBeforeEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilder5clearEv
	.type	_ZN6icu_6722FormattedStringBuilder5clearEv, @function
_ZN6icu_6722FormattedStringBuilder5clearEv:
.LFB2451:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	movq	%rdi, %rax
	movl	$20, %edx
	je	.L150
	movl	16(%rdi), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
.L150:
	movl	%edx, 128(%rax)
	movl	$0, 132(%rax)
	ret
	.cfi_endproc
.LFE2451:
	.size	_ZN6icu_6722FormattedStringBuilder5clearEv, .-_ZN6icu_6722FormattedStringBuilder5clearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode
	.type	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode, @function
_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode:
.LFB2452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	cmpl	$65535, %edx
	movl	132(%rdi), %eax
	movb	%cl, -57(%rbp)
	seta	%r13b
	movq	%r8, -56(%rbp)
	movslq	128(%rdi), %rcx
	addl	$1, %r13d
	leal	0(%r13,%rax), %esi
	testl	%r14d, %r14d
	jne	.L154
	movl	%ecx, %r12d
	subl	%r13d, %r12d
	jns	.L210
.L154:
	movzbl	(%rbx), %edi
	cmpl	%eax, %r14d
	jne	.L156
	leal	(%rcx,%r14), %edx
	addl	%r13d, %edx
	testb	%dil, %dil
	je	.L157
	movl	16(%rbx), %edi
	cmpl	%edi, %edx
	jge	.L158
.L180:
	movq	-56(%rbp), %rax
	leal	(%rcx,%rsi), %r12d
	movl	%esi, 132(%rbx)
	subl	%r13d, %r12d
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L211
.L152:
	addq	$120, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	testb	%dil, %dil
	je	.L159
	movl	16(%rbx), %edi
.L158:
	movq	88(%rbx), %rdx
	movq	8(%rbx), %r9
	movq	%rdx, -72(%rbp)
.L179:
	movl	%esi, %r12d
	shrl	$31, %r12d
	addl	%esi, %r12d
	movl	%r12d, %edx
	sarl	%edx
	negl	%edx
	cmpl	%edi, %esi
	jg	.L212
	movl	%edi, %r12d
	shrl	$31, %r12d
	addl	%r12d, %edi
	sarl	%edi
	leal	(%rdi,%rdx), %r12d
	movslq	%eax, %rdx
	movslq	%r12d, %r11
	addq	%rdx, %rdx
	jne	.L213
.L170:
	movl	%eax, %edx
	movslq	%r14d, %r8
	movslq	%r13d, %r10
	subl	%r14d, %edx
	addq	%r11, %r8
	movslq	%edx, %rdx
	addq	%r8, %r10
	addq	%rdx, %rdx
	jne	.L214
	movslq	%eax, %rdx
	xorl	%esi, %esi
	testq	%rdx, %rdx
	jne	.L215
.L172:
	movl	%esi, %edx
	subl	%r14d, %edx
	movslq	%edx, %rdx
	testq	%rdx, %rdx
	jne	.L216
.L173:
	addl	%r13d, %esi
	movl	%r12d, 128(%rbx)
	movl	%esi, 132(%rbx)
.L169:
	addl	%r14d, %r12d
.L155:
	movq	-56(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L152
.L211:
	movslq	%r12d, %r12
	movzbl	(%rbx), %esi
	leaq	(%r12,%r12), %rcx
	cmpl	$1, %r13d
	je	.L217
	movl	%r15d, %edx
	sarl	$10, %r15d
	leaq	1(%r12), %rax
	andw	$1023, %dx
	movl	%r15d, %r9d
	orw	$-9216, %dx
	subw	$10304, %r9w
	testb	%sil, %sil
	jne	.L218
	movw	%r9w, 8(%rbx,%rcx)
	addq	$88, %rbx
	movw	%dx, -78(%rbx,%rcx)
	movzbl	-57(%rbp), %ecx
	addq	%rbx, %rax
	movb	%cl, (%rax)
.L178:
	movzbl	(%rax), %eax
	movb	%al, (%rbx,%r12)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L210:
	movl	%r12d, 128(%rdi)
	movl	%esi, 132(%rdi)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L157:
	cmpl	$39, %edx
	jle	.L180
.L159:
	leaq	88(%rbx), %rdi
	leaq	8(%rbx), %r9
	movq	%rdi, -72(%rbp)
	movl	$40, %edi
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L212:
	leal	(%rsi,%rsi), %eax
	movq	%r9, -112(%rbp)
	leal	(%rsi,%rdx), %r12d
	movslq	%eax, %r8
	movl	%ecx, -96(%rbp)
	leaq	(%r8,%r8), %rdi
	movq	%r8, -88(%rbp)
	movl	%eax, -104(%rbp)
	call	uprv_malloc_67@PLT
	movq	-88(%rbp), %r8
	movq	%rax, -80(%rbp)
	movq	%r8, %rdi
	call	uprv_malloc_67@PLT
	cmpq	$0, -80(%rbp)
	movq	%rax, -88(%rbp)
	je	.L183
	testq	%rax, %rax
	movslq	-96(%rbp), %rcx
	movq	-112(%rbp), %r9
	je	.L183
	movslq	%r14d, %r8
	movslq	%r12d, %r11
	movq	%r8, %rdx
	addq	%rdx, %rdx
	jne	.L219
.L164:
	movl	132(%rbx), %eax
	movslq	%r13d, %rdi
	addq	%r8, %rdi
	movl	%eax, %r10d
	movl	%eax, -96(%rbp)
	leaq	(%r8,%rcx), %rax
	subl	%r14d, %r10d
	movq	%rax, -112(%rbp)
	leaq	(%rdi,%r11), %rax
	movslq	%r10d, %r10
	movq	%rax, -120(%rbp)
	movq	%r10, %rdx
	addq	%rdx, %rdx
	jne	.L220
.L165:
	testq	%r8, %r8
	jne	.L221
.L166:
	testq	%r10, %r10
	jne	.L222
.L167:
	cmpb	$0, (%rbx)
	jne	.L223
.L168:
	movq	-80(%rbp), %rax
	movl	-96(%rbp), %r11d
	movb	$1, (%rbx)
	movq	-88(%rbp), %rcx
	movl	%r12d, 128(%rbx)
	movq	%rax, 8(%rbx)
	movl	-104(%rbp), %eax
	addl	%r13d, %r11d
	movq	%rcx, 88(%rbx)
	movl	%eax, 16(%rbx)
	movl	%eax, 96(%rbx)
	movl	%r11d, 132(%rbx)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L218:
	movq	8(%rbx), %rsi
	addq	88(%rbx), %rax
	movw	%r9w, (%rsi,%r12,2)
	movq	8(%rbx), %rsi
	movw	%dx, 2(%rsi,%rcx)
	movzbl	-57(%rbp), %ecx
	movb	%cl, (%rax)
	movq	88(%rbx), %rbx
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%r9, %rdi
	call	uprv_free_67@PLT
	movq	-72(%rbp), %rdi
	call	uprv_free_67@PLT
	movl	132(%rbx), %eax
	movl	%eax, -96(%rbp)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L222:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movq	%r10, %rdx
	movq	%r9, -128(%rbp)
	addq	-88(%rbp), %rdi
	addq	-72(%rbp), %rsi
	call	memcpy@PLT
	movq	-128(%rbp), %r9
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L221:
	movq	-88(%rbp), %rax
	movq	%r8, %rdx
	movq	%r10, -136(%rbp)
	movq	%r9, -128(%rbp)
	leaq	(%rax,%r11), %rdi
	movq	-72(%rbp), %rax
	leaq	(%rax,%rcx), %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %r9
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L220:
	movq	-80(%rbp), %rax
	movq	%rcx, -144(%rbp)
	movq	-120(%rbp), %rcx
	movq	%r10, -160(%rbp)
	movq	%r11, -152(%rbp)
	leaq	(%rax,%rcx,2), %rdi
	movq	-112(%rbp), %rax
	movq	%r8, -136(%rbp)
	movq	%r9, -128(%rbp)
	leaq	(%r9,%rax,2), %rsi
	call	memcpy@PLT
	movq	-160(%rbp), %r10
	movq	-128(%rbp), %r9
	movq	-152(%rbp), %r11
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %r8
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L219:
	movq	-80(%rbp), %rax
	leaq	(%r9,%rcx,2), %rsi
	movq	%r8, -128(%rbp)
	movq	%r11, -120(%rbp)
	leaq	(%rax,%r11,2), %rdi
	movq	%r9, -112(%rbp)
	movq	%rcx, -96(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %r11
	movq	-112(%rbp), %r9
	movq	-96(%rbp), %rcx
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L217:
	testb	%sil, %sil
	je	.L175
	movq	8(%rbx), %rax
	movq	88(%rbx), %rbx
	movw	%r15w, (%rax,%r12,2)
.L176:
	movzbl	-57(%rbp), %eax
	movb	%al, (%rbx,%r12)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L216:
	movq	-72(%rbp), %rax
	leaq	(%rax,%r8), %rsi
	leaq	(%rax,%r10), %rdi
	call	memmove@PLT
	movl	132(%rbx), %esi
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L214:
	leaq	(%r9,%r8,2), %rsi
	leaq	(%r9,%r10,2), %rdi
	movq	%r11, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	memmove@PLT
	movq	-104(%rbp), %r11
	movq	-96(%rbp), %rcx
	xorl	%esi, %esi
	movl	132(%rbx), %eax
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %r10
	movslq	%eax, %rdx
	testq	%rdx, %rdx
	je	.L172
.L215:
	movq	-72(%rbp), %rax
	movq	%r10, -88(%rbp)
	movq	%r8, -80(%rbp)
	leaq	(%rax,%rcx), %rsi
	leaq	(%rax,%r11), %rdi
	call	memmove@PLT
	movl	132(%rbx), %esi
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %r8
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L213:
	leaq	(%r9,%rcx,2), %rsi
	leaq	(%r9,%r11,2), %rdi
	movq	%rcx, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r11, -80(%rbp)
	call	memmove@PLT
	movl	132(%rbx), %eax
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %r11
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L175:
	movw	%r15w, 8(%rbx,%rcx)
	addq	$88, %rbx
	jmp	.L176
.L183:
	movq	-80(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-88(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-56(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L152
	.cfi_endproc
.LFE2452:
	.size	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode, .-_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilder15writeTerminatorER10UErrorCode
	.type	_ZN6icu_6722FormattedStringBuilder15writeTerminatorER10UErrorCode, @function
_ZN6icu_6722FormattedStringBuilder15writeTerminatorER10UErrorCode:
.LFB2458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movl	132(%rdi), %r15d
	movl	128(%rdi), %esi
	testl	%r15d, %r15d
	je	.L271
.L225:
	cmpb	$0, (%rbx)
	leal	1(%r15,%rsi), %eax
	je	.L227
	movl	16(%rbx), %edi
	cmpl	%edi, %eax
	jge	.L228
.L247:
	leal	1(%r15), %ecx
	movl	%ecx, 132(%rbx)
	leal	-1(%rcx,%rsi), %r12d
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L271:
	movl	%esi, %r12d
	subl	$1, %r12d
	js	.L225
	movl	%r12d, 128(%rdi)
	movl	$1, 132(%rdi)
.L226:
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L224
	movslq	%r12d, %r12
	cmpb	$0, (%rbx)
	leaq	(%r12,%r12), %rax
	je	.L244
	movq	8(%rbx), %rax
	xorl	%ecx, %ecx
	movw	%cx, (%rax,%r12,2)
	movq	88(%rbx), %rax
.L245:
	movb	$0, (%rax,%r12)
	subl	$1, 132(%rbx)
.L224:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	8(%rbx), %r8
	movq	88(%rbx), %r14
.L246:
	leal	1(%r15), %edx
	movl	%edx, %r12d
	shrl	$31, %r12d
	addl	%edx, %r12d
	movl	%r12d, %eax
	sarl	%eax
	negl	%eax
	cmpl	%edi, %edx
	jg	.L272
	movl	%edi, %r12d
	movslq	%r15d, %rdx
	movslq	%esi, %r11
	shrl	$31, %r12d
	addl	%edi, %r12d
	sarl	%r12d
	addl	%eax, %r12d
	movslq	%r12d, %r9
	leaq	(%rdx,%r9), %r10
	leaq	1(%r10), %rax
	movq	%rax, -56(%rbp)
	movq	%rdx, %rax
	addq	%rax, %rax
	jne	.L273
	movl	%r15d, %eax
	testq	%rdx, %rdx
	jne	.L249
.L243:
	addl	$1, %eax
	movl	%r12d, 128(%rbx)
	movl	%eax, 132(%rbx)
.L238:
	addl	%r15d, %r12d
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L244:
	xorl	%edx, %edx
	movw	%dx, 8(%rbx,%rax)
	leaq	88(%rbx), %rax
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L272:
	leal	(%rdx,%rdx), %ecx
	leal	(%rdx,%rax), %r12d
	movl	%esi, -96(%rbp)
	movslq	%ecx, %rdx
	movq	%r8, -88(%rbp)
	leaq	(%rdx,%rdx), %rdi
	movl	%ecx, -80(%rbp)
	movq	%rdx, -64(%rbp)
	call	uprv_malloc_67@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, -56(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -72(%rbp)
	call	uprv_malloc_67@PLT
	cmpq	$0, -56(%rbp)
	movq	%rax, -64(%rbp)
	je	.L250
	testq	%rax, %rax
	movq	-72(%rbp), %rdx
	movq	-88(%rbp), %r8
	movl	-96(%rbp), %esi
	je	.L250
	movslq	%r15d, %r11
	movslq	%esi, %rcx
	movslq	%r12d, %r10
	subq	$2, %rdx
	jne	.L274
.L233:
	movl	132(%rbx), %eax
	movl	%eax, %r9d
	movl	%eax, -72(%rbp)
	leaq	(%r11,%rcx), %rax
	subl	%r15d, %r9d
	movq	%rax, -88(%rbp)
	leaq	1(%r11,%r10), %rax
	movslq	%r9d, %r9
	movq	%rax, -96(%rbp)
	movq	%r9, %rdx
	addq	%rdx, %rdx
	jne	.L275
.L234:
	testq	%r11, %r11
	jne	.L276
.L235:
	testq	%r9, %r9
	jne	.L277
.L236:
	cmpb	$0, (%rbx)
	jne	.L278
.L237:
	movq	-56(%rbp), %rax
	movl	-72(%rbp), %r10d
	movb	$1, (%rbx)
	movq	-64(%rbp), %rcx
	movl	%r12d, 128(%rbx)
	movq	%rax, 8(%rbx)
	movl	-80(%rbp), %eax
	addl	$1, %r10d
	movq	%rcx, 88(%rbx)
	movl	%eax, 16(%rbx)
	movl	%eax, 96(%rbx)
	movl	%r10d, 132(%rbx)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L227:
	cmpl	$39, %eax
	jle	.L247
	leaq	8(%rbx), %r8
	leaq	88(%rbx), %r14
	movl	$40, %edi
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L273:
	leaq	(%r8,%r11,2), %rsi
	leaq	(%r8,%r9,2), %rdi
	movq	%rax, %rdx
	movq	%r10, -88(%rbp)
	movq	%r11, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	movl	132(%rbx), %eax
	movq	-88(%rbp), %r10
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
	movl	%eax, %edx
	leaq	(%r10,%r10), %rdi
	movq	-80(%rbp), %r11
	subl	%r15d, %edx
	leaq	(%r8,%rdi), %rsi
	leaq	2(%r8,%rdi), %rdi
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	je	.L279
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	memmove@PLT
	movslq	132(%rbx), %rdx
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r11
	movq	%rdx, %rax
.L241:
	testq	%rdx, %rdx
	je	.L242
.L249:
	leaq	(%r14,%r11), %rsi
	leaq	(%r14,%r9), %rdi
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	movl	132(%rbx), %eax
	movq	-64(%rbp), %r10
.L242:
	movl	%eax, %edx
	subl	%r15d, %edx
	movslq	%edx, %rdx
	testq	%rdx, %rdx
	je	.L243
	movq	-56(%rbp), %rdi
	leaq	(%r14,%r10), %rsi
	addq	%r14, %rdi
	call	memmove@PLT
	movl	132(%rbx), %eax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L274:
	movq	-56(%rbp), %rax
	leaq	(%r8,%rcx,2), %rsi
	movq	%r11, -104(%rbp)
	movq	%r10, -96(%rbp)
	leaq	(%rax,%r10,2), %rdi
	movq	%r8, -88(%rbp)
	movq	%rcx, -72(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %r11
	movq	-96(%rbp), %r10
	movq	-88(%rbp), %r8
	movq	-72(%rbp), %rcx
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	movl	132(%rbx), %eax
	movl	%eax, -72(%rbp)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L277:
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%r9, %rdx
	movq	%r8, -104(%rbp)
	addq	-64(%rbp), %rdi
	addq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r8
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L276:
	movq	-64(%rbp), %rax
	leaq	(%r14,%rcx), %rsi
	movq	%r11, %rdx
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	leaq	(%rax,%r10), %rdi
	call	memcpy@PLT
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %r8
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L275:
	movq	-56(%rbp), %rax
	movq	%rcx, -120(%rbp)
	movq	-96(%rbp), %rcx
	movq	%r9, -136(%rbp)
	movq	%r10, -128(%rbp)
	leaq	(%rax,%rcx,2), %rdi
	movq	-88(%rbp), %rax
	movq	%r11, -112(%rbp)
	movq	%r8, -104(%rbp)
	leaq	(%r8,%rax,2), %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %r10
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %r8
	jmp	.L234
.L250:
	movq	-56(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-64(%rbp), %rdi
	call	uprv_free_67@PLT
	movl	$7, 0(%r13)
	jmp	.L224
.L279:
	movslq	%eax, %rdx
	jmp	.L241
	.cfi_endproc
.LFE2458:
	.size	_ZN6icu_6722FormattedStringBuilder15writeTerminatorER10UErrorCode, .-_ZN6icu_6722FormattedStringBuilder15writeTerminatorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilder16prepareForInsertEiiR10UErrorCode
	.type	_ZN6icu_6722FormattedStringBuilder16prepareForInsertEiiR10UErrorCode, @function
_ZN6icu_6722FormattedStringBuilder16prepareForInsertEiiR10UErrorCode:
.LFB2459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movl	128(%rdi), %esi
	movl	132(%rdi), %edi
	testl	%r13d, %r13d
	jne	.L281
	movl	%esi, %eax
	subl	%edx, %eax
	jns	.L330
.L281:
	movzbl	(%rbx), %edx
	cmpl	%edi, %r13d
	jne	.L283
	leal	(%rsi,%r13), %eax
	addl	%r14d, %eax
	testb	%dl, %dl
	je	.L284
	movl	16(%rbx), %r11d
	cmpl	%r11d, %eax
	jge	.L285
.L301:
	addl	%r14d, %edi
	movl	%edi, 132(%rbx)
	leal	(%rdi,%rsi), %eax
	addq	$104, %rsp
	popq	%rbx
	subl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	testb	%dl, %dl
	je	.L286
	movl	16(%rbx), %r11d
.L285:
	movq	88(%rbx), %rax
	movq	8(%rbx), %r9
	movq	%rax, -56(%rbp)
.L300:
	leal	(%r14,%rdi), %r10d
	movl	%r10d, %r12d
	shrl	$31, %r12d
	addl	%r10d, %r12d
	movl	%r12d, %edx
	sarl	%edx
	negl	%edx
	cmpl	%r11d, %r10d
	jg	.L331
	movl	%r11d, %r12d
	movslq	%esi, %r15
	shrl	$31, %r12d
	addl	%r12d, %r11d
	sarl	%r11d
	leal	(%r11,%rdx), %r12d
	movslq	%edi, %rdx
	movslq	%r12d, %r11
	addq	%rdx, %rdx
	jne	.L332
.L296:
	movl	%edi, %edx
	movslq	%r13d, %rcx
	movslq	%r14d, %r10
	subl	%r13d, %edx
	addq	%r11, %rcx
	movslq	%edx, %rdx
	addq	%rcx, %r10
	addq	%rdx, %rdx
	jne	.L333
.L297:
	movslq	%edi, %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jne	.L334
.L298:
	movl	%eax, %edx
	subl	%r13d, %edx
	movslq	%edx, %rdx
	testq	%rdx, %rdx
	jne	.L335
.L299:
	leal	(%r14,%rax), %r8d
	movl	%r12d, 128(%rbx)
	movl	%r8d, 132(%rbx)
.L295:
	leal	0(%r13,%r12), %eax
.L280:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	leal	(%rdx,%rdi), %r8d
	movl	%eax, 128(%rbx)
	movl	%r8d, 132(%rbx)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	cmpl	$39, %eax
	jle	.L301
.L286:
	leaq	88(%rbx), %rax
	leaq	8(%rbx), %r9
	movl	$40, %r11d
	movq	%rax, -56(%rbp)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L331:
	leal	(%r10,%r10), %r15d
	leal	(%r10,%rdx), %r12d
	movq	%rcx, -96(%rbp)
	movslq	%r15d, %r10
	movq	%r9, -88(%rbp)
	leaq	(%r10,%r10), %rdi
	movl	%esi, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	uprv_malloc_67@PLT
	movq	-72(%rbp), %r10
	movq	%rax, -64(%rbp)
	movq	%r10, %rdi
	call	uprv_malloc_67@PLT
	cmpq	$0, -64(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rax, -72(%rbp)
	je	.L303
	testq	%rax, %rax
	movl	-80(%rbp), %esi
	movq	-88(%rbp), %r9
	je	.L303
	movslq	%r13d, %r11
	movslq	%esi, %rcx
	movslq	%r12d, %r8
	movq	%r11, %rdx
	addq	%rdx, %rdx
	jne	.L336
.L290:
	movl	132(%rbx), %eax
	movslq	%r14d, %rdi
	addq	%r11, %rdi
	movl	%eax, %r10d
	movl	%eax, -80(%rbp)
	leaq	(%r11,%rcx), %rax
	subl	%r13d, %r10d
	movq	%rax, -88(%rbp)
	leaq	(%rdi,%r8), %rax
	movslq	%r10d, %r10
	movq	%rax, -96(%rbp)
	movq	%r10, %rdx
	addq	%rdx, %rdx
	jne	.L337
.L291:
	testq	%r11, %r11
	jne	.L338
.L292:
	testq	%r10, %r10
	jne	.L339
.L293:
	cmpb	$0, (%rbx)
	jne	.L340
.L294:
	movq	-64(%rbp), %rax
	movl	-80(%rbp), %r8d
	movb	$1, (%rbx)
	movl	%r15d, 16(%rbx)
	movq	%rax, 8(%rbx)
	movq	-72(%rbp), %rax
	addl	%r14d, %r8d
	movl	%r15d, 96(%rbx)
	movq	%rax, 88(%rbx)
	movl	%r12d, 128(%rbx)
	movl	%r8d, 132(%rbx)
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%r9, %rdi
	call	uprv_free_67@PLT
	movq	-56(%rbp), %rdi
	call	uprv_free_67@PLT
	movl	132(%rbx), %eax
	movl	%eax, -80(%rbp)
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L339:
	movq	-96(%rbp), %rdi
	movq	-88(%rbp), %rsi
	movq	%r10, %rdx
	movq	%r9, -104(%rbp)
	addq	-72(%rbp), %rdi
	addq	-56(%rbp), %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r9
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L338:
	movq	-72(%rbp), %rax
	movq	%r11, %rdx
	movq	%r10, -112(%rbp)
	movq	%r9, -104(%rbp)
	leaq	(%rax,%r8), %rdi
	movq	-56(%rbp), %rax
	leaq	(%rax,%rcx), %rsi
	call	memcpy@PLT
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %r9
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L337:
	movq	-64(%rbp), %rax
	movq	%rcx, -120(%rbp)
	movq	-96(%rbp), %rcx
	movq	%r10, -136(%rbp)
	movq	%r8, -128(%rbp)
	leaq	(%rax,%rcx,2), %rdi
	movq	-88(%rbp), %rax
	movq	%r11, -112(%rbp)
	movq	%r9, -104(%rbp)
	leaq	(%r9,%rax,2), %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %r9
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L336:
	movq	-64(%rbp), %rax
	leaq	(%r9,%rcx,2), %rsi
	movq	%r11, -104(%rbp)
	movq	%r8, -96(%rbp)
	leaq	(%rax,%r8,2), %rdi
	movq	%r9, -88(%rbp)
	movq	%rcx, -80(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %r11
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rcx
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L335:
	movq	-56(%rbp), %rax
	leaq	(%rax,%rcx), %rsi
	leaq	(%rax,%r10), %rdi
	call	memmove@PLT
	movl	132(%rbx), %eax
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L334:
	movq	-56(%rbp), %rax
	movq	%r10, -72(%rbp)
	movq	%rcx, -64(%rbp)
	leaq	(%rax,%r15), %rsi
	leaq	(%rax,%r11), %rdi
	call	memmove@PLT
	movl	132(%rbx), %eax
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %rcx
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L333:
	leaq	(%r9,%r10,2), %rdi
	leaq	(%r9,%rcx,2), %rsi
	movq	%r11, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	movl	132(%rbx), %edi
	movq	-80(%rbp), %r11
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %r10
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L332:
	leaq	(%r9,%r11,2), %rdi
	leaq	(%r9,%r15,2), %rsi
	movq	%r9, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	memmove@PLT
	movl	132(%rbx), %edi
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r11
	jmp	.L296
.L303:
	movq	-64(%rbp), %rdi
	movq	%rcx, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-72(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-56(%rbp), %rcx
	orl	$-1, %eax
	movl	$7, (%rcx)
	jmp	.L280
	.cfi_endproc
.LFE2459:
	.size	_ZN6icu_6722FormattedStringBuilder16prepareForInsertEiiR10UErrorCode, .-_ZN6icu_6722FormattedStringBuilder16prepareForInsertEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilder22prepareForInsertHelperEiiR10UErrorCode
	.type	_ZN6icu_6722FormattedStringBuilder22prepareForInsertHelperEiiR10UErrorCode, @function
_ZN6icu_6722FormattedStringBuilder22prepareForInsertHelperEiiR10UErrorCode:
.LFB2460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	cmpb	$0, (%rdi)
	movl	128(%rdi), %esi
	je	.L342
	movq	88(%rdi), %rax
	movl	16(%rdi), %r10d
	movq	8(%rdi), %r9
	movq	%rax, -56(%rbp)
.L357:
	movl	132(%rbx), %edi
	leal	(%rdi,%r14), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	leal	(%rax,%rdx), %r12d
	movl	%r12d, %eax
	sarl	%eax
	negl	%eax
	cmpl	%r10d, %edx
	jg	.L383
	movl	%r10d, %r12d
	movslq	%edi, %rdx
	movslq	%esi, %r11
	shrl	$31, %r12d
	addl	%r10d, %r12d
	sarl	%r12d
	addl	%eax, %r12d
	movslq	%r12d, %r10
	addq	%rdx, %rdx
	jne	.L384
.L353:
	movl	%edi, %edx
	movslq	%r13d, %r15
	movslq	%r14d, %rcx
	subl	%r13d, %edx
	addq	%r10, %r15
	movslq	%edx, %rdx
	addq	%r15, %rcx
	addq	%rdx, %rdx
	jne	.L385
	movslq	%edi, %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jne	.L386
.L355:
	movl	%eax, %edx
	subl	%r13d, %edx
	movslq	%edx, %rdx
	testq	%rdx, %rdx
	jne	.L387
.L356:
	leal	(%r14,%rax), %r8d
	movl	%r12d, 128(%rbx)
	movl	%r8d, 132(%rbx)
.L352:
	leal	0(%r13,%r12), %eax
.L341:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	leaq	88(%rdi), %rax
	leaq	8(%rdi), %r9
	movl	$40, %r10d
	movq	%rax, -56(%rbp)
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L387:
	movq	-56(%rbp), %rax
	leaq	(%rax,%r15), %rsi
	leaq	(%rax,%rcx), %rdi
	call	memmove@PLT
	movl	132(%rbx), %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L386:
	movq	-56(%rbp), %rax
	movq	%rcx, -64(%rbp)
	leaq	(%rax,%r11), %rsi
	leaq	(%rax,%r10), %rdi
	call	memmove@PLT
	movl	132(%rbx), %eax
	movq	-64(%rbp), %rcx
	movl	%eax, %edx
	subl	%r13d, %edx
	movslq	%edx, %rdx
	testq	%rdx, %rdx
	je	.L356
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L385:
	leaq	(%r9,%rcx,2), %rdi
	leaq	(%r9,%r15,2), %rsi
	movq	%r10, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r11
	xorl	%eax, %eax
	movl	132(%rbx), %edi
	movq	-64(%rbp), %rcx
	movslq	%edi, %rdx
	testq	%rdx, %rdx
	je	.L355
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L384:
	leaq	(%r9,%r10,2), %rdi
	leaq	(%r9,%r11,2), %rsi
	movq	%r11, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	movl	132(%rbx), %edi
	movq	-80(%rbp), %r11
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r10
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L383:
	leal	(%rdx,%rdx), %r15d
	movq	%rcx, -80(%rbp)
	leal	(%rdx,%rax), %r12d
	movslq	%r15d, %r10
	movl	%esi, -96(%rbp)
	leaq	(%r10,%r10), %rdi
	movq	%r9, -88(%rbp)
	movq	%r10, -72(%rbp)
	call	uprv_malloc_67@PLT
	movq	-72(%rbp), %r10
	movq	%rax, -64(%rbp)
	movq	%r10, %rdi
	call	uprv_malloc_67@PLT
	cmpq	$0, -64(%rbp)
	movq	-80(%rbp), %rcx
	movq	%rax, -72(%rbp)
	je	.L359
	testq	%rax, %rax
	movq	-88(%rbp), %r9
	movl	-96(%rbp), %esi
	je	.L359
	movslq	%r13d, %r11
	movslq	%esi, %rcx
	movslq	%r12d, %r8
	movq	%r11, %rdx
	addq	%rdx, %rdx
	jne	.L388
.L347:
	movl	132(%rbx), %eax
	movslq	%r14d, %rdi
	addq	%r11, %rdi
	movl	%eax, %r10d
	movl	%eax, -80(%rbp)
	leaq	(%r11,%rcx), %rax
	subl	%r13d, %r10d
	movq	%rax, -88(%rbp)
	leaq	(%rdi,%r8), %rax
	movslq	%r10d, %r10
	movq	%rax, -96(%rbp)
	movq	%r10, %rdx
	addq	%rdx, %rdx
	jne	.L389
.L348:
	testq	%r11, %r11
	jne	.L390
.L349:
	testq	%r10, %r10
	jne	.L391
.L350:
	cmpb	$0, (%rbx)
	jne	.L392
.L351:
	movq	-64(%rbp), %rax
	movl	-80(%rbp), %r8d
	movb	$1, (%rbx)
	movl	%r15d, 16(%rbx)
	movq	%rax, 8(%rbx)
	movq	-72(%rbp), %rax
	addl	%r14d, %r8d
	movl	%r15d, 96(%rbx)
	movq	%rax, 88(%rbx)
	movl	%r12d, 128(%rbx)
	movl	%r8d, 132(%rbx)
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L388:
	movq	-64(%rbp), %rax
	leaq	(%r9,%rcx,2), %rsi
	movq	%r11, -104(%rbp)
	movq	%r8, -96(%rbp)
	leaq	(%rax,%r8,2), %rdi
	movq	%r9, -88(%rbp)
	movq	%rcx, -80(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %r11
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rcx
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L392:
	movq	%r9, %rdi
	call	uprv_free_67@PLT
	movq	-56(%rbp), %rdi
	call	uprv_free_67@PLT
	movl	132(%rbx), %eax
	movl	%eax, -80(%rbp)
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L391:
	movq	-96(%rbp), %rdi
	movq	-88(%rbp), %rsi
	movq	%r10, %rdx
	movq	%r9, -104(%rbp)
	addq	-72(%rbp), %rdi
	addq	-56(%rbp), %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r9
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L390:
	movq	-72(%rbp), %rax
	movq	%r11, %rdx
	movq	%r9, -112(%rbp)
	movq	%r10, -104(%rbp)
	leaq	(%rax,%r8), %rdi
	movq	-56(%rbp), %rax
	leaq	(%rax,%rcx), %rsi
	call	memcpy@PLT
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %r10
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L389:
	movq	-64(%rbp), %rax
	movq	%rcx, -120(%rbp)
	movq	-96(%rbp), %rcx
	movq	%r10, -136(%rbp)
	movq	%r8, -128(%rbp)
	leaq	(%rax,%rcx,2), %rdi
	movq	-88(%rbp), %rax
	movq	%r11, -112(%rbp)
	movq	%r9, -104(%rbp)
	leaq	(%r9,%rax,2), %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %r9
	jmp	.L348
.L359:
	movq	-64(%rbp), %rdi
	movq	%rcx, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-72(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-56(%rbp), %rcx
	orl	$-1, %eax
	movl	$7, (%rcx)
	jmp	.L341
	.cfi_endproc
.LFE2460:
	.size	_ZN6icu_6722FormattedStringBuilder22prepareForInsertHelperEiiR10UErrorCode, .-_ZN6icu_6722FormattedStringBuilder22prepareForInsertHelperEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode
	.type	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode, @function
_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode:
.LFB2454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	%ecx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$24, %rsp
	movq	16(%rbp), %r15
	movl	132(%rdi), %edx
	testl	%esi, %esi
	jne	.L394
	movl	128(%rdi), %eax
	subl	%r8d, %eax
	jns	.L421
.L394:
	cmpl	%edx, %esi
	jne	.L396
	movl	128(%r12), %eax
	movl	$40, %edi
	leal	(%rax,%rsi), %ecx
	addl	%r8d, %ecx
	cmpb	$0, (%r12)
	je	.L397
	movl	16(%r12), %edi
.L397:
	cmpl	%edi, %ecx
	jl	.L422
.L396:
	movl	%r8d, %edx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movl	%r8d, -52(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder22prepareForInsertHelperEiiR10UErrorCode
	movl	-52(%rbp), %r8d
.L395:
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L393
	testl	%r8d, %r8d
	jle	.L393
	cmpb	$0, (%r12)
	cltq
	jne	.L400
	movslq	%ebx, %rsi
	leaq	88(%r12,%rax), %rdi
	leaq	10(%r13), %rbx
	subq	%rsi, %rax
	leaq	(%rsi,%rsi), %rdx
	leal	(%r8,%rsi), %r10d
	movq	%rsi, %rcx
	leaq	(%r12,%rax,2), %r11
	.p2align 4,,10
	.p2align 3
.L408:
	movswl	8(%r13), %eax
	movl	%eax, %esi
	sarl	$5, %eax
	testw	%si, %si
	jns	.L402
	movl	12(%r13), %eax
.L402:
	movl	$-1, %r9d
	cmpl	%ecx, %eax
	jbe	.L403
	andl	$2, %esi
	movq	%rbx, %rax
	jne	.L406
	movq	24(%r13), %rax
.L406:
	movzwl	(%rax,%rdx), %r9d
.L403:
	addl	$1, %ecx
	movw	%r9w, 8(%r11,%rdx)
	addq	$1, %rdi
	addq	$2, %rdx
	movb	%r14b, -1(%rdi)
	cmpl	%r10d, %ecx
	jne	.L408
.L393:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movslq	%ebx, %rdi
	leal	-1(%r8), %edx
	leaq	10(%r13), %r11
	subl	%eax, %ebx
	addq	%rdi, %rdi
	leaq	1(%rax,%rdx), %r10
	movl	%ebx, %ecx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L423:
	movswl	%dx, %esi
	sarl	$5, %esi
.L410:
	leal	(%rcx,%rax), %r15d
	movl	$-1, %r9d
	cmpl	%r15d, %esi
	jbe	.L411
	andl	$2, %edx
	movq	%r11, %rdx
	jne	.L413
	movq	24(%r13), %rdx
.L413:
	movzwl	(%rdx,%rdi), %r9d
.L411:
	movq	88(%r12), %rdx
	movw	%r9w, (%rbx)
	addq	$2, %rdi
	movb	%r14b, (%rdx,%rax)
	addq	$1, %rax
	cmpq	%r10, %rax
	je	.L393
.L414:
	movq	8(%r12), %rdx
	leaq	(%rdx,%rax,2), %rbx
	movzwl	8(%r13), %edx
	testw	%dx, %dx
	jns	.L423
	movl	12(%r13), %esi
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L421:
	addl	%r8d, %edx
	movl	%eax, 128(%rdi)
	movl	%edx, 132(%rdi)
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L422:
	addl	%r8d, %edx
	addl	%edx, %eax
	movl	%edx, 132(%r12)
	subl	%r8d, %eax
	jmp	.L395
	.cfi_endproc
.LFE2454:
	.size	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode, .-_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilder6appendERKS0_R10UErrorCode
	.type	_ZN6icu_6722FormattedStringBuilder6appendERKS0_R10UErrorCode, @function
_ZN6icu_6722FormattedStringBuilder6appendERKS0_R10UErrorCode:
.LFB2456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rsi, %rdi
	je	.L456
	movl	132(%rsi), %r13d
	movq	%rsi, %r12
	testl	%r13d, %r13d
	jne	.L457
.L424:
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	.cfi_restore_state
	movl	132(%rdi), %esi
	movl	128(%rdi), %edx
	movq	%rdi, %rbx
	testl	%esi, %esi
	je	.L458
.L427:
	leal	(%rsi,%rdx), %eax
	movl	$40, %ecx
	addl	%r13d, %eax
	cmpb	$0, (%rbx)
	je	.L429
	movl	16(%rbx), %ecx
.L429:
	cmpl	%ecx, %eax
	jl	.L459
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6722FormattedStringBuilder22prepareForInsertHelperEiiR10UErrorCode
.L428:
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L424
	testl	%r13d, %r13d
	jle	.L424
	cmpb	$0, (%rbx)
	movzbl	(%r12), %ecx
	movslq	128(%r12), %rdx
	jne	.L431
	testb	%cl, %cl
	jne	.L432
	movslq	%eax, %r9
	movslq	%r13d, %rsi
	leal	-1(%r13), %r14d
	movslq	%edx, %r11
	movq	%rsi, -56(%rbp)
	leaq	88(%r9), %rsi
	leaq	88(%r12,%r11), %r8
	leaq	(%rbx,%rsi), %r10
	movq	%rsi, -64(%rbp)
	leaq	104(%r12,%r11), %rsi
	cmpq	%rsi, %r10
	movq	%r10, -72(%rbp)
	leaq	104(%rbx,%r9), %r10
	leaq	40(%r11,%r11), %rdi
	setnb	%sil
	cmpq	%r10, %r8
	leaq	8(%r9,%r9), %r15
	movl	%r14d, -84(%rbp)
	setnb	%r10b
	leaq	-32(%r12,%rdi), %rcx
	movq	%rdi, -80(%rbp)
	orl	%esi, %r10d
	cmpl	$14, %r14d
	seta	%sil
	andl	%esi, %r10d
	leaq	32(%rbx,%r15), %rsi
	cmpq	%rsi, %rcx
	leaq	(%rbx,%r15), %rsi
	setnb	%r14b
	addq	%r12, %rdi
	cmpq	%rdi, %rsi
	setnb	-56(%rbp)
	movzbl	-56(%rbp), %edi
	orl	%edi, %r14d
	testb	%r14b, %r10b
	je	.L433
	movslq	%r13d, %rdi
	leaq	4(%rdi,%r9), %r10
	addq	%r10, %r10
	cmpq	-64(%rbp), %r10
	leaq	88(%rdi,%r9), %r10
	setle	%r14b
	cmpq	%r15, %r10
	setle	%r10b
	orb	%r10b, %r14b
	je	.L433
	movl	%r13d, %r10d
	movq	-72(%rbp), %rdi
	xorl	%r9d, %r9d
	shrl	$4, %r10d
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L435:
	movdqu	16(%rcx,%r9,2), %xmm0
	movdqu	(%rcx,%r9,2), %xmm1
	movdqu	(%r8,%r9), %xmm2
	movups	%xmm1, (%rsi,%r9,2)
	movups	%xmm0, 16(%rsi,%r9,2)
	movups	%xmm2, (%rdi,%r9)
	addq	$16, %r9
	cmpq	%r10, %r9
	jne	.L435
	movl	%r13d, %ecx
	andl	$-16, %ecx
	testb	$15, %r13b
	je	.L424
	leal	(%rdx,%rcx), %r9d
	leaq	8(%r12), %rdi
	movslq	%r9d, %r9
	leal	(%rax,%rcx), %r8d
	leaq	8(%rbx), %rsi
	movzwl	(%rdi,%r9,2), %r10d
	movzbl	88(%r12,%r9), %r9d
	movslq	%r8d, %r8
	movw	%r10w, (%rsi,%r8,2)
	movb	%r9b, 88(%rbx,%r8)
	leal	1(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L424
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	2(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L424
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	3(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L424
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	4(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L424
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	5(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L424
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	6(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L424
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	7(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L424
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	8(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L424
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	9(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L424
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	10(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L424
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	11(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L424
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	12(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L424
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	13(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L424
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	addl	$14, %ecx
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	cmpl	%ecx, %r13d
	jle	.L424
	addl	%ecx, %eax
	addl	%edx, %ecx
	movslq	%eax, %r8
	movslq	%ecx, %rax
	movzwl	(%rdi,%rax,2), %edx
	movzbl	88(%r12,%rax), %eax
	movw	%dx, (%rsi,%r8,2)
	movb	%al, 88(%rbx,%r8)
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L458:
	movl	%edx, %eax
	subl	%r13d, %eax
	js	.L427
	movl	%eax, 128(%rdi)
	movl	%r13d, 132(%rdi)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L456:
	movl	$1, (%rdx)
	xorl	%r13d, %r13d
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L431:
	testb	%cl, %cl
	cltq
	leal	-1(%r13), %ecx
	jne	.L441
	leaq	1(%rax,%rcx), %rsi
	movq	%rdx, %rcx
	subq	%rax, %rcx
	leaq	(%r12,%rcx,2), %rdi
	addq	%rcx, %r12
	.p2align 4,,10
	.p2align 3
.L442:
	movzwl	8(%rdi,%rax,2), %ecx
	movq	8(%rbx), %rdx
	movw	%cx, (%rdx,%rax,2)
	movzbl	88(%r12,%rax), %ecx
	movq	88(%rbx), %rdx
	movb	%cl, (%rdx,%rax)
	addq	$1, %rax
	cmpq	%rsi, %rax
	jne	.L442
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L459:
	addl	%r13d, %esi
	leal	(%rsi,%rdx), %eax
	movl	%esi, 132(%rbx)
	subl	%r13d, %eax
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L432:
	cltq
	leal	-1(%r13), %ecx
	movq	%rax, %rsi
	subq	%rdx, %rax
	leaq	1(%rdx,%rcx), %rcx
	subq	%rdx, %rsi
	leaq	(%rbx,%rsi,2), %rsi
	addq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L440:
	movq	8(%r12), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 8(%rsi,%rdx,2)
	movq	88(%r12), %rax
	movzbl	(%rax,%rdx), %eax
	movb	%al, 88(%rbx,%rdx)
	addq	$1, %rdx
	cmpq	%rcx, %rdx
	jne	.L440
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L441:
	leaq	1(%rax,%rcx), %rdi
	.p2align 4,,10
	.p2align 3
.L443:
	movq	8(%r12), %rcx
	movzwl	(%rcx,%rdx,2), %esi
	movq	8(%rbx), %rcx
	movw	%si, (%rcx,%rax,2)
	movq	88(%r12), %rcx
	movzbl	(%rcx,%rdx), %esi
	movq	88(%rbx), %rcx
	addq	$1, %rdx
	movb	%sil, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%rdi, %rax
	jne	.L443
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L433:
	movq	-80(%rbp), %rax
	movl	-84(%rbp), %edi
	leaq	-8(%rbx,%r15), %r10
	leaq	(%r12,%r11), %rsi
	leaq	(%rbx,%r9), %rcx
	leaq	-40(%r12,%rax), %r8
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L439:
	movzwl	8(%r8,%rax,2), %edx
	movw	%dx, 8(%r10,%rax,2)
	movzbl	88(%rsi,%rax), %edx
	movb	%dl, 88(%rcx,%rax)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rdi, %rdx
	jne	.L439
	jmp	.L424
	.cfi_endproc
.LFE2456:
	.size	_ZN6icu_6722FormattedStringBuilder6appendERKS0_R10UErrorCode, .-_ZN6icu_6722FormattedStringBuilder6appendERKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode
	.type	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode, @function
_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode:
.LFB2453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$8, %rsp
	movzwl	8(%rdx), %ecx
	testw	%cx, %cx
	js	.L461
	movswl	%cx, %r12d
	sarl	$5, %r12d
.L462:
	testl	%r12d, %r12d
	je	.L460
	cmpl	$1, %r12d
	jne	.L464
	andl	$2, %ecx
	jne	.L479
	movq	24(%rdx), %rdx
.L466:
	movzwl	(%rdx), %r14d
	movl	132(%r13), %edx
	testl	%esi, %esi
	jne	.L467
	movl	128(%r13), %eax
	subl	$1, %eax
	jns	.L480
.L467:
	cmpl	%edx, %esi
	jne	.L469
	movl	128(%r13), %ecx
	cmpb	$0, 0(%r13)
	movl	$40, %eax
	leal	1(%rcx,%rsi), %edi
	je	.L470
	movl	16(%r13), %eax
.L470:
	cmpl	%eax, %edi
	jl	.L481
.L469:
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6722FormattedStringBuilder22prepareForInsertHelperEiiR10UErrorCode
.L468:
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L460
	cltq
	cmpb	$0, 0(%r13)
	leaq	(%rax,%rax), %rdx
	jne	.L482
	movw	%r14w, 8(%r13,%rdx)
	addq	$88, %r13
.L473:
	movb	%bl, 0(%r13,%rax)
.L460:
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	subq	$8, %rsp
	movl	%r12d, %r8d
	movl	%ebx, %r9d
	xorl	%ecx, %ecx
	pushq	%r15
	movq	%r13, %rdi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode
	movl	%eax, %r12d
	popq	%rax
	popq	%rdx
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L461:
	movl	12(%rdx), %r12d
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L479:
	addq	$10, %rdx
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L480:
	addl	$1, %edx
	movl	%eax, 128(%r13)
	movl	%edx, 132(%r13)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L482:
	movq	8(%r13), %rdx
	movq	88(%r13), %r13
	movw	%r14w, (%rdx,%rax,2)
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L481:
	addl	$1, %edx
	movl	%edx, 132(%r13)
	leal	-1(%rcx,%rdx), %eax
	jmp	.L468
	.cfi_endproc
.LFE2453:
	.size	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode, .-_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilder6insertEiRKS0_R10UErrorCode
	.type	_ZN6icu_6722FormattedStringBuilder6insertEiRKS0_R10UErrorCode, @function
_ZN6icu_6722FormattedStringBuilder6insertEiRKS0_R10UErrorCode:
.LFB2457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rdx, %rdi
	je	.L515
	movl	132(%rdx), %r13d
	movq	%rdx, %r12
	testl	%r13d, %r13d
	jne	.L516
.L483:
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore_state
	movl	132(%rdi), %edx
	movq	%rdi, %rbx
	testl	%esi, %esi
	jne	.L486
	movl	128(%rdi), %eax
	subl	%r13d, %eax
	jns	.L517
.L486:
	cmpl	%edx, %esi
	jne	.L488
	movl	128(%rbx), %eax
	movl	$40, %edi
	leal	(%rax,%rsi), %ecx
	addl	%r13d, %ecx
	cmpb	$0, (%rbx)
	je	.L489
	movl	16(%rbx), %edi
.L489:
	cmpl	%edi, %ecx
	jge	.L488
	addl	%r13d, %edx
	addl	%edx, %eax
	movl	%edx, 132(%rbx)
	subl	%r13d, %eax
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6722FormattedStringBuilder22prepareForInsertHelperEiiR10UErrorCode
.L487:
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L483
	testl	%r13d, %r13d
	jle	.L483
	cmpb	$0, (%rbx)
	movzbl	(%r12), %ecx
	movslq	128(%r12), %rdx
	je	.L518
	testb	%cl, %cl
	cltq
	leal	-1(%r13), %ecx
	jne	.L500
	leaq	1(%rax,%rcx), %rsi
	movq	%rdx, %rcx
	subq	%rax, %rcx
	leaq	(%r12,%rcx,2), %rdi
	addq	%rcx, %r12
	.p2align 4,,10
	.p2align 3
.L501:
	movzwl	8(%rdi,%rax,2), %ecx
	movq	8(%rbx), %rdx
	movw	%cx, (%rdx,%rax,2)
	movzbl	88(%r12,%rax), %ecx
	movq	88(%rbx), %rdx
	movb	%cl, (%rdx,%rax)
	addq	$1, %rax
	cmpq	%rsi, %rax
	jne	.L501
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L518:
	testb	%cl, %cl
	jne	.L491
	movslq	%eax, %r9
	movslq	%r13d, %rsi
	leal	-1(%r13), %r14d
	movslq	%edx, %r11
	movq	%rsi, -56(%rbp)
	leaq	88(%r9), %rsi
	leaq	88(%r12,%r11), %r8
	leaq	(%rbx,%rsi), %r10
	movq	%rsi, -64(%rbp)
	leaq	104(%r12,%r11), %rsi
	cmpq	%rsi, %r10
	movq	%r10, -72(%rbp)
	leaq	104(%rbx,%r9), %r10
	leaq	40(%r11,%r11), %rdi
	setnb	%sil
	cmpq	%r10, %r8
	leaq	8(%r9,%r9), %r15
	movl	%r14d, -84(%rbp)
	setnb	%r10b
	leaq	-32(%r12,%rdi), %rcx
	movq	%rdi, -80(%rbp)
	orl	%esi, %r10d
	cmpl	$14, %r14d
	seta	%sil
	andl	%esi, %r10d
	leaq	32(%rbx,%r15), %rsi
	cmpq	%rsi, %rcx
	leaq	(%rbx,%r15), %rsi
	setnb	%r14b
	addq	%r12, %rdi
	cmpq	%rdi, %rsi
	setnb	-56(%rbp)
	movzbl	-56(%rbp), %edi
	orl	%edi, %r14d
	testb	%r14b, %r10b
	je	.L492
	movslq	%r13d, %rdi
	leaq	4(%r9,%rdi), %r10
	addq	%r10, %r10
	cmpq	-64(%rbp), %r10
	leaq	88(%r9,%rdi), %r10
	setle	%r14b
	cmpq	%r15, %r10
	setle	%r10b
	orb	%r10b, %r14b
	je	.L492
	movl	%r13d, %r10d
	movq	-72(%rbp), %rdi
	xorl	%r9d, %r9d
	shrl	$4, %r10d
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L494:
	movdqu	16(%rcx,%r9,2), %xmm0
	movdqu	(%rcx,%r9,2), %xmm1
	movdqu	(%r8,%r9), %xmm2
	movups	%xmm1, (%rsi,%r9,2)
	movups	%xmm0, 16(%rsi,%r9,2)
	movups	%xmm2, (%rdi,%r9)
	addq	$16, %r9
	cmpq	%r10, %r9
	jne	.L494
	movl	%r13d, %ecx
	andl	$-16, %ecx
	testb	$15, %r13b
	je	.L483
	leal	(%rdx,%rcx), %r9d
	leaq	8(%r12), %rdi
	movslq	%r9d, %r9
	leal	(%rax,%rcx), %r8d
	leaq	8(%rbx), %rsi
	movzwl	(%rdi,%r9,2), %r10d
	movzbl	88(%r12,%r9), %r9d
	movslq	%r8d, %r8
	movw	%r10w, (%rsi,%r8,2)
	movb	%r9b, 88(%rbx,%r8)
	leal	1(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L483
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	2(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L483
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	3(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L483
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	4(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L483
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	5(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L483
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	6(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L483
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	7(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L483
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	8(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L483
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	9(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L483
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	10(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L483
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	11(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L483
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	12(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L483
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	leal	13(%rcx), %r8d
	cmpl	%r8d, %r13d
	jle	.L483
	leal	(%rax,%r8), %r9d
	addl	%edx, %r8d
	addl	$14, %ecx
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	movzwl	(%rdi,%r8,2), %r10d
	movzbl	88(%r12,%r8), %r8d
	movw	%r10w, (%rsi,%r9,2)
	movb	%r8b, 88(%rbx,%r9)
	cmpl	%ecx, %r13d
	jle	.L483
	addl	%ecx, %eax
	addl	%edx, %ecx
	movslq	%eax, %r8
	movslq	%ecx, %rax
	movzwl	(%rdi,%rax,2), %edx
	movzbl	88(%r12,%rax), %eax
	movw	%dx, (%rsi,%r8,2)
	movb	%al, 88(%rbx,%r8)
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L517:
	addl	%r13d, %edx
	movl	%eax, 128(%rdi)
	movl	%edx, 132(%rdi)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L515:
	movl	$1, (%rcx)
	xorl	%r13d, %r13d
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L500:
	leaq	1(%rax,%rcx), %rdi
	.p2align 4,,10
	.p2align 3
.L502:
	movq	8(%r12), %rcx
	movzwl	(%rcx,%rdx,2), %esi
	movq	8(%rbx), %rcx
	movw	%si, (%rcx,%rax,2)
	movq	88(%r12), %rcx
	movzbl	(%rcx,%rdx), %esi
	movq	88(%rbx), %rcx
	addq	$1, %rdx
	movb	%sil, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%rdi, %rax
	jne	.L502
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L491:
	cltq
	leal	-1(%r13), %ecx
	movq	%rax, %rsi
	subq	%rdx, %rax
	leaq	1(%rdx,%rcx), %rcx
	subq	%rdx, %rsi
	leaq	(%rbx,%rsi,2), %rsi
	addq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L499:
	movq	8(%r12), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 8(%rsi,%rdx,2)
	movq	88(%r12), %rax
	movzbl	(%rax,%rdx), %eax
	movb	%al, 88(%rbx,%rdx)
	addq	$1, %rdx
	cmpq	%rcx, %rdx
	jne	.L499
	jmp	.L483
.L492:
	movq	-80(%rbp), %rax
	movl	-84(%rbp), %edi
	leaq	-8(%rbx,%r15), %r10
	leaq	(%r12,%r11), %rsi
	leaq	(%rbx,%r9), %rcx
	leaq	-40(%r12,%rax), %r8
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L498:
	movzwl	8(%r8,%rax,2), %edx
	movw	%dx, 8(%r10,%rax,2)
	movzbl	88(%rsi,%rax), %edx
	movb	%dl, 88(%rcx,%rax)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rdi, %rdx
	jne	.L498
	jmp	.L483
	.cfi_endproc
.LFE2457:
	.size	_ZN6icu_6722FormattedStringBuilder6insertEiRKS0_R10UErrorCode, .-_ZN6icu_6722FormattedStringBuilder6insertEiRKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilder6removeEii
	.type	_ZN6icu_6722FormattedStringBuilder6removeEii, @function
_ZN6icu_6722FormattedStringBuilder6removeEii:
.LFB2461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r14, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	128(%rdi), %r8d
	movl	132(%rdi), %edx
	addl	%esi, %r8d
	subl	%esi, %edx
	subl	%r14d, %edx
	movslq	%r8d, %r15
	addq	%r15, %r14
	movslq	%edx, %rdx
	leaq	(%r15,%r15), %rdi
	cmpb	$0, (%rbx)
	leaq	(%rdx,%rdx), %r9
	leaq	(%r14,%r14), %rsi
	je	.L520
	movq	8(%rbx), %rax
	testq	%r9, %r9
	jne	.L536
.L521:
	movq	88(%rbx), %rdi
	testq	%rdx, %rdx
	jne	.L538
.L525:
	subl	%r12d, 132(%rbx)
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L539
	leaq	88(%rbx), %rdi
.L540:
	testq	%rdx, %rdx
	je	.L525
.L538:
	leaq	(%rdi,%r14), %rsi
	addq	%r15, %rdi
	movl	%r8d, -52(%rbp)
	call	memmove@PLT
	movl	-52(%rbp), %r8d
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L539:
	leaq	8(%rbx), %rax
.L536:
	addq	%rax, %rsi
	addq	%rax, %rdi
	movq	%r9, %rdx
	movl	%r8d, -52(%rbp)
	call	memmove@PLT
	movl	132(%rbx), %edx
	movl	-52(%rbp), %r8d
	subl	%r13d, %edx
	subl	%r12d, %edx
	cmpb	$0, (%rbx)
	movslq	%edx, %rdx
	jne	.L521
	leaq	88(%rbx), %rdi
	jmp	.L540
	.cfi_endproc
.LFE2461:
	.size	_ZN6icu_6722FormattedStringBuilder6removeEii, .-_ZN6icu_6722FormattedStringBuilder6removeEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722FormattedStringBuilder6spliceEiiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode
	.type	_ZN6icu_6722FormattedStringBuilder6spliceEiiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode, @function
_ZN6icu_6722FormattedStringBuilder6spliceEiiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode:
.LFB2455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r9d, %r15d
	pushq	%r14
	subl	%r8d, %r15d
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%r15d, %r14d
	pushq	%r12
	subl	%edx, %r14d
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	movzbl	16(%rbp), %r13d
	testl	%r14d, %r14d
	jle	.L542
	movl	132(%rdi), %edx
	testl	%esi, %esi
	je	.L570
.L543:
	cmpl	%edx, %esi
	jne	.L545
	movl	128(%rdi), %eax
	movl	$40, %r8d
	leal	(%rax,%rsi), %ecx
	addl	%r14d, %ecx
	cmpb	$0, (%rdi)
	je	.L546
	movl	16(%rdi), %r8d
.L546:
	cmpl	%r8d, %ecx
	jl	.L571
.L545:
	movq	24(%rbp), %rcx
	movl	%r14d, %edx
	movq	%rdi, -56(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder22prepareForInsertHelperEiiR10UErrorCode
	movq	-56(%rbp), %rdi
.L544:
	movq	24(%rbp), %rsi
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L541
	testl	%r15d, %r15d
	jle	.L541
	cmpb	$0, (%rdi)
	cltq
	jne	.L549
	movslq	%ebx, %rcx
	leaq	88(%rdi,%rax), %rsi
	leaq	10(%r12), %r11
	subq	%rcx, %rax
	leaq	(%rcx,%rcx), %rdx
	leal	(%r15,%rcx), %r9d
	movq	%rcx, %r8
	leaq	(%rdi,%rax,2), %r10
	.p2align 4,,10
	.p2align 3
.L557:
	movswl	8(%r12), %eax
	movl	%eax, %ecx
	sarl	$5, %eax
	testw	%cx, %cx
	jns	.L551
	movl	12(%r12), %eax
.L551:
	movl	$-1, %edi
	cmpl	%r8d, %eax
	jbe	.L552
	andl	$2, %ecx
	movq	%r11, %rax
	jne	.L555
	movq	24(%r12), %rax
.L555:
	movzwl	(%rax,%rdx), %edi
.L552:
	addl	$1, %r8d
	movw	%di, 8(%r10,%rdx)
	addq	$1, %rsi
	addq	$2, %rdx
	movb	%r13b, -1(%rsi)
	cmpl	%r9d, %r8d
	jne	.L557
.L541:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L570:
	.cfi_restore_state
	movl	128(%rdi), %eax
	subl	%r14d, %eax
	js	.L543
	addl	%r14d, %edx
	movl	%eax, 128(%rdi)
	movl	%edx, 132(%rdi)
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L542:
	subl	%r15d, %edx
	movq	%rdi, -56(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder6removeEii
	movq	-56(%rbp), %rdi
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L549:
	movslq	%ebx, %rsi
	leal	-1(%r15), %edx
	leaq	10(%r12), %r11
	subl	%eax, %ebx
	addq	%rsi, %rsi
	leaq	1(%rax,%rdx), %r10
	movl	%ebx, %r8d
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L572:
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L559:
	leal	(%r8,%rax), %r15d
	movl	$-1, %r9d
	cmpl	%r15d, %ecx
	jbe	.L560
	andl	$2, %edx
	movq	%r11, %rdx
	jne	.L562
	movq	24(%r12), %rdx
.L562:
	movzwl	(%rdx,%rsi), %r9d
.L560:
	movq	88(%rdi), %rdx
	movw	%r9w, (%rbx)
	addq	$2, %rsi
	movb	%r13b, (%rdx,%rax)
	addq	$1, %rax
	cmpq	%r10, %rax
	je	.L541
.L563:
	movq	8(%rdi), %rdx
	leaq	(%rdx,%rax,2), %rbx
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	jns	.L572
	movl	12(%r12), %ecx
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L571:
	addl	%r14d, %edx
	addl	%edx, %eax
	movl	%edx, 132(%rdi)
	subl	%r14d, %eax
	jmp	.L544
	.cfi_endproc
.LFE2455:
	.size	_ZN6icu_6722FormattedStringBuilder6spliceEiiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode, .-_ZN6icu_6722FormattedStringBuilder6spliceEiiRKNS_13UnicodeStringEiiNS0_5FieldER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722FormattedStringBuilder15toUnicodeStringEv
	.type	_ZNK6icu_6722FormattedStringBuilder15toUnicodeStringEv, @function
_ZNK6icu_6722FormattedStringBuilder15toUnicodeStringEv:
.LFB2462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, (%rsi)
	movl	132(%rsi), %edx
	je	.L575
	movq	8(%rsi), %rax
.L575:
	movslq	128(%rsi), %rcx
	movq	%r12, %rdi
	leaq	(%rax,%rcx,2), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2462:
	.size	_ZNK6icu_6722FormattedStringBuilder15toUnicodeStringEv, .-_ZNK6icu_6722FormattedStringBuilder15toUnicodeStringEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722FormattedStringBuilder19toTempUnicodeStringEv
	.type	_ZNK6icu_6722FormattedStringBuilder19toTempUnicodeStringEv, @function
_ZNK6icu_6722FormattedStringBuilder19toTempUnicodeStringEv:
.LFB2463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	132(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	leaq	8(%rsi), %rax
	je	.L580
	movq	8(%rsi), %rax
.L580:
	movslq	128(%rsi), %rdx
	movq	%r12, %rdi
	xorl	%esi, %esi
	leaq	(%rax,%rdx,2), %rax
	leaq	-32(%rbp), %rdx
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L584
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L584:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2463:
	.size	_ZNK6icu_6722FormattedStringBuilder19toTempUnicodeStringEv, .-_ZNK6icu_6722FormattedStringBuilder19toTempUnicodeStringEv
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC0:
	.string	"<"
	.string	"F"
	.string	"o"
	.string	"r"
	.string	"m"
	.string	"a"
	.string	"t"
	.string	"t"
	.string	"e"
	.string	"d"
	.string	"S"
	.string	"t"
	.string	"r"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	"B"
	.string	"u"
	.string	"i"
	.string	"l"
	.string	"d"
	.string	"e"
	.string	"r"
	.string	" "
	.string	"["
	.string	""
	.string	""
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC1:
	.string	"]"
	.string	" "
	.string	"["
	.string	""
	.string	""
	.align 2
.LC2:
	.string	"]"
	.string	">"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722FormattedStringBuilder13toDebugStringEv
	.type	_ZNK6icu_6722FormattedStringBuilder13toDebugStringEv, @function
_ZNK6icu_6722FormattedStringBuilder13toDebugStringEv:
.LFB2464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	leaq	.LC0(%rip), %rsi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, 8(%rdi)
	movl	$-1, %ecx
	movq	%rax, (%rdi)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC0(%rip), %rax
	cmpb	$0, (%rbx)
	movl	132(%rbx), %edx
	leaq	8(%rbx), %rax
	je	.L587
	movq	8(%rbx), %rax
.L587:
	movslq	128(%rbx), %rcx
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	leaq	(%rax,%rcx,2), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movswl	-104(%rbp), %ecx
	testw	%cx, %cx
	js	.L588
	sarl	$5, %ecx
.L589:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC1(%rip), %rax
	movl	132(%rbx), %edx
	testl	%edx, %edx
	jle	.L590
	xorl	%r13d, %r13d
	leaq	CSWTCH.135(%rip), %r12
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L593:
	movzbl	%al, %esi
	sarl	$4, %esi
	cmpl	$2, %esi
	jne	.L595
	andl	$15, %eax
	cmpb	$10, %al
	ja	.L596
	andl	$15, %eax
	movzwl	(%r12,%rax,2), %eax
.L597:
	movw	%ax, -114(%rbp)
.L603:
	leaq	-114(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	addl	$1, %r13d
	cmpl	%r13d, 132(%rbx)
	jle	.L590
.L598:
	cmpb	$0, (%rbx)
	leaq	88(%rbx), %rdx
	je	.L592
	movq	88(%rbx), %rdx
.L592:
	movl	128(%rbx), %eax
	addl	%r13d, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	jne	.L593
	movl	$110, %eax
	movw	%ax, -114(%rbp)
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L595:
	addl	$48, %esi
	movq	%r14, %rdi
	addl	$1, %r13d
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%r13d, 132(%rbx)
	jg	.L598
.L590:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC2(%rip), %rax
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L604
	addq	$96, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L596:
	.cfi_restore_state
	movzbl	%al, %eax
	addl	$48, %eax
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L588:
	movl	-100(%rbp), %ecx
	jmp	.L589
.L604:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2464:
	.size	_ZNK6icu_6722FormattedStringBuilder13toDebugStringEv, .-_ZNK6icu_6722FormattedStringBuilder13toDebugStringEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722FormattedStringBuilder5charsEv
	.type	_ZNK6icu_6722FormattedStringBuilder5charsEv, @function
_ZNK6icu_6722FormattedStringBuilder5charsEv:
.LFB2465:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	leaq	8(%rdi), %rax
	je	.L607
	movq	8(%rdi), %rax
.L607:
	movslq	128(%rdi), %rdx
	leaq	(%rax,%rdx,2), %rax
	ret
	.cfi_endproc
.LFE2465:
	.size	_ZNK6icu_6722FormattedStringBuilder5charsEv, .-_ZNK6icu_6722FormattedStringBuilder5charsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722FormattedStringBuilder13contentEqualsERKS0_
	.type	_ZNK6icu_6722FormattedStringBuilder13contentEqualsERKS0_, @function
_ZNK6icu_6722FormattedStringBuilder13contentEqualsERKS0_:
.LFB2466:
	.cfi_startproc
	endbr64
	movl	132(%rdi), %ecx
	xorl	%r9d, %r9d
	cmpl	132(%rsi), %ecx
	jne	.L627
	testl	%ecx, %ecx
	jle	.L619
	movzbl	(%rdi), %r10d
	movslq	128(%rdi), %rax
	movzbl	(%rsi), %r9d
	movslq	128(%rsi), %rdx
	testb	%r10b, %r10b
	jne	.L610
	testb	%r9b, %r9b
	jne	.L611
	leal	-1(%rcx), %r11d
	leaq	(%rdi,%rax,2), %r10
	xorl	%ecx, %ecx
	addq	%rax, %rdi
	leaq	(%rsi,%rdx,2), %r8
	addq	%rdx, %rsi
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L629:
	movzbl	88(%rcx,%rdi), %eax
	cmpb	%al, 88(%rcx,%rsi)
	jne	.L627
	leaq	1(%rcx), %rax
	cmpq	%r11, %rcx
	je	.L619
	movq	%rax, %rcx
.L612:
	movzwl	8(%r8,%rcx,2), %eax
	cmpw	%ax, 8(%r10,%rcx,2)
	je	.L629
.L627:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	testb	%r9b, %r9b
	jne	.L614
	subl	$1, %ecx
	movq	8(%rdi), %r8
	leaq	1(%rax,%rcx), %r11
	movq	%rdx, %rcx
	subq	%rax, %rdx
	subq	%rax, %rcx
	leaq	(%rsi,%rcx,2), %rcx
	addq	%rdx, %rsi
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L631:
	movq	88(%rdi), %rdx
	movzbl	(%rdx,%rax), %ebx
	cmpb	%bl, 88(%rax,%rsi)
	jne	.L608
	addq	$1, %rax
	cmpq	%r11, %rax
	je	.L630
.L615:
	movzwl	8(%rcx,%rax,2), %ebx
	cmpw	%bx, (%r8,%rax,2)
	je	.L631
.L608:
	movl	%r9d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	subl	$1, %ecx
	movq	8(%rdi), %r11
	movq	8(%rsi), %r10
	leaq	1(%rdx,%rcx), %rbx
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L632:
	movq	88(%rsi), %rcx
	movq	88(%rdi), %r8
	movzbl	(%rcx,%rdx), %ecx
	cmpb	%cl, (%r8,%rax)
	jne	.L624
	addq	$1, %rdx
	addq	$1, %rax
	cmpq	%rbx, %rdx
	je	.L608
.L616:
	movzwl	(%r10,%rdx,2), %ecx
	cmpw	%cx, (%r11,%rax,2)
	je	.L632
.L624:
	xorl	%r9d, %r9d
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	subl	$1, %ecx
	movq	8(%rsi), %r8
	leaq	1(%rdx,%rcx), %r10
	movq	%rax, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	leaq	(%rdi,%rcx,2), %rcx
	addq	%rax, %rdi
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L633:
	movq	88(%rsi), %rax
	movzbl	(%rax,%rdx), %eax
	cmpb	%al, 88(%rdx,%rdi)
	jne	.L621
	addq	$1, %rdx
	cmpq	%rdx, %r10
	je	.L627
.L613:
	movzwl	8(%rcx,%rdx,2), %eax
	cmpw	%ax, (%r8,%rdx,2)
	je	.L633
.L621:
	xorl	%r9d, %r9d
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L619:
	movl	$1, %r9d
	jmp	.L627
.L630:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	%r10d, %r9d
	jmp	.L608
	.cfi_endproc
.LFE2466:
	.size	_ZNK6icu_6722FormattedStringBuilder13contentEqualsERKS0_, .-_ZNK6icu_6722FormattedStringBuilder13contentEqualsERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722FormattedStringBuilder13containsFieldENS0_5FieldE
	.type	_ZNK6icu_6722FormattedStringBuilder13containsFieldENS0_5FieldE, @function
_ZNK6icu_6722FormattedStringBuilder13containsFieldENS0_5FieldE:
.LFB2467:
	.cfi_startproc
	endbr64
	movl	132(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L639
	movzbl	(%rdi), %r8d
	movslq	128(%rdi), %rax
	testb	%r8b, %r8b
	jne	.L636
	leaq	88(%rdi,%rax), %rdx
	subl	$1, %ecx
	leaq	89(%rdi,%rax), %rax
	addq	%rcx, %rax
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L642:
	addq	$1, %rdx
	cmpq	%rax, %rdx
	je	.L634
.L637:
	cmpb	(%rdx), %sil
	jne	.L642
	movl	$1, %r8d
.L634:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L636:
	addq	88(%rdi), %rax
	leal	-1(%rcx), %edx
	leaq	1(%rax,%rdx), %rdx
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L643:
	addq	$1, %rax
	cmpq	%rdx, %rax
	je	.L639
.L638:
	cmpb	(%rax), %sil
	jne	.L643
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L639:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2467:
	.size	_ZNK6icu_6722FormattedStringBuilder13containsFieldENS0_5FieldE, .-_ZNK6icu_6722FormattedStringBuilder13containsFieldENS0_5FieldE
	.section	.rodata
	.align 16
	.type	CSWTCH.135, @object
	.size	CSWTCH.135, 22
CSWTCH.135:
	.value	105
	.value	102
	.value	46
	.value	69
	.value	43
	.value	101
	.value	44
	.value	36
	.value	37
	.value	8240
	.value	45
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
