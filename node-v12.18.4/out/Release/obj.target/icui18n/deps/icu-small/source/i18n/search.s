	.file	"search.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIterator14setMatchLengthEi
	.type	_ZN6icu_6714SearchIterator14setMatchLengthEi, @function
_ZN6icu_6714SearchIterator14setMatchLengthEi:
.LFB2507:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	%esi, 36(%rax)
	ret
	.cfi_endproc
.LFE2507:
	.size	_ZN6icu_6714SearchIterator14setMatchLengthEi, .-_ZN6icu_6714SearchIterator14setMatchLengthEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIterator13setMatchStartEi
	.type	_ZN6icu_6714SearchIterator13setMatchStartEi, @function
_ZN6icu_6714SearchIterator13setMatchStartEi:
.LFB2508:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	%esi, 32(%rax)
	ret
	.cfi_endproc
.LFE2508:
	.size	_ZN6icu_6714SearchIterator13setMatchStartEi, .-_ZN6icu_6714SearchIterator13setMatchStartEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SearchIteratoreqERKS0_
	.type	_ZNK6icu_6714SearchIteratoreqERKS0_, @function
_ZNK6icu_6714SearchIteratoreqERKS0_:
.LFB2489:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rsi), %rax
	cmpq	%rax, 16(%rdi)
	je	.L6
.L8:
	xorl	%eax, %eax
.L4:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	8(%rsi), %rax
	movq	8(%rdi), %rdx
	movzwl	12(%rax), %ecx
	cmpw	%cx, 12(%rdx)
	jne	.L8
	movzwl	14(%rax), %ecx
	cmpw	%cx, 14(%rdx)
	jne	.L8
	movq	32(%rax), %rcx
	cmpq	%rcx, 32(%rdx)
	jne	.L8
	movl	8(%rax), %eax
	cmpl	%eax, 8(%rdx)
	jne	.L8
	movq	(%rdi), %rax
	movq	%rsi, -24(%rbp)
	call	*32(%rax)
	movq	-24(%rbp), %rsi
	movl	%eax, %r12d
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	call	*32(%rax)
	movq	-24(%rbp), %rsi
	cmpl	%eax, %r12d
	jne	.L8
	movq	8(%rbx), %rax
	movq	8(%rsi), %rcx
	movslq	8(%rax), %rdx
	movq	(%rcx), %rsi
	movq	(%rax), %rdi
	addq	%rdx, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2489:
	.size	_ZNK6icu_6714SearchIteratoreqERKS0_, .-_ZNK6icu_6714SearchIteratoreqERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIterator7setTextERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6714SearchIterator7setTextERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6714SearchIterator7setTextERKNS_13UnicodeStringER10UErrorCode:
.LFB2486:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L28
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movswl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L16
	sarl	$5, %eax
	testl	%eax, %eax
	jne	.L18
.L29:
	movl	$1, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movl	12(%rsi), %eax
	testl	%eax, %eax
	je	.L29
.L18:
	leaq	24(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	32(%rbx), %eax
	movq	8(%rbx), %rdx
	testb	$17, %al
	jne	.L23
	leaq	34(%rbx), %rcx
	testb	$2, %al
	jne	.L19
	movq	48(%rbx), %rcx
.L19:
	movq	%rcx, (%rdx)
	testw	%ax, %ax
	js	.L21
	sarl	$5, %eax
.L22:
	movl	%eax, 8(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movl	36(%rbx), %eax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L23:
	xorl	%ecx, %ecx
	jmp	.L19
	.cfi_endproc
.LFE2486:
	.size	_ZN6icu_6714SearchIterator7setTextERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6714SearchIterator7setTextERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIterator7setTextERNS_17CharacterIteratorER10UErrorCode
	.type	_ZN6icu_6714SearchIterator7setTextERNS_17CharacterIteratorER10UErrorCode, @function
_ZN6icu_6714SearchIterator7setTextERNS_17CharacterIteratorER10UErrorCode:
.LFB2487:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L47
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	leaq	24(%r13), %r14
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	%r14, %rsi
	call	*208(%rax)
	movq	0(%r13), %rax
	leaq	_ZN6icu_6714SearchIterator7setTextERKNS_13UnicodeStringER10UErrorCode(%rip), %rdx
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L33
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L48
.L30:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movswl	32(%r13), %eax
	testw	%ax, %ax
	js	.L35
	sarl	$5, %eax
.L36:
	testl	%eax, %eax
	jne	.L37
	movl	$1, (%r12)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L33:
	addq	$8, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movl	36(%r13), %eax
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	32(%r13), %eax
	movq	8(%r13), %rdx
	testb	$17, %al
	jne	.L42
	leaq	34(%r13), %rcx
	testb	$2, %al
	jne	.L38
	movq	48(%r13), %rcx
.L38:
	movq	%rcx, (%rdx)
	testw	%ax, %ax
	js	.L40
	sarl	$5, %eax
.L41:
	movl	%eax, 8(%rdx)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L40:
	movl	36(%r13), %eax
	jmp	.L41
.L42:
	xorl	%ecx, %ecx
	jmp	.L38
	.cfi_endproc
.LFE2487:
	.size	_ZN6icu_6714SearchIterator7setTextERNS_17CharacterIteratorER10UErrorCode, .-_ZN6icu_6714SearchIterator7setTextERNS_17CharacterIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIterator5resetEv
	.type	_ZN6icu_6714SearchIterator5resetEv, @function
_ZN6icu_6714SearchIterator5resetEv:
.LFB2496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6714SearchIterator13setMatchStartEi(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -32(%rbp)
	movq	104(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L50
	movq	8(%rdi), %rdx
	leaq	_ZN6icu_6714SearchIterator14setMatchLengthEi(%rip), %rcx
	movl	$-1, 32(%rdx)
	movq	96(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L52
.L59:
	movq	8(%rbx), %rdx
	movl	$0, 36(%rdx)
.L53:
	cmpb	$0, 40(%rdx)
	movq	24(%rax), %rax
	movl	$0, -28(%rbp)
	je	.L54
	leaq	-28(%rbp), %r8
	movl	8(%rdx), %esi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	*%rax
.L55:
	movq	(%rbx), %rax
	leaq	-32(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*24(%rax)
	movq	8(%rbx), %rax
	movl	$257, %edx
	movl	$0, 12(%rax)
	movw	%dx, 40(%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	leaq	-28(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$-1, %esi
	call	*%rdx
	movq	(%rbx), %rax
	leaq	_ZN6icu_6714SearchIterator14setMatchLengthEi(%rip), %rcx
	movq	96(%rax), %rdx
	cmpq	%rcx, %rdx
	je	.L59
.L52:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rdx
	movq	8(%rbx), %rdx
	movq	(%rbx), %rax
	jmp	.L53
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2496:
	.size	_ZN6icu_6714SearchIterator5resetEv, .-_ZN6icu_6714SearchIterator5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIteratorC2ERKS0_
	.type	_ZN6icu_6714SearchIteratorC2ERKS0_, @function
_ZN6icu_6714SearchIteratorC2ERKS0_:
.LFB2473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714SearchIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	24(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	movq	%rax, -24(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movq	-8(%rsi), %rax
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	$48, %edi
	call	uprv_malloc_67@PLT
	movq	8(%r12), %rdx
	movq	%rax, 8(%rbx)
	movq	24(%rdx), %rcx
	movq	%rcx, 24(%rax)
	movzbl	13(%rdx), %ecx
	movb	%cl, 13(%rax)
	movzbl	12(%rdx), %ecx
	movb	%cl, 12(%rax)
	movzwl	14(%rdx), %ecx
	movw	%cx, 14(%rax)
	movl	32(%rdx), %ecx
	movl	%ecx, 32(%rax)
	movl	36(%rdx), %ecx
	movl	%ecx, 36(%rax)
	movq	(%rdx), %rcx
	movl	8(%rdx), %edx
	movq	%rcx, (%rax)
	movl	%edx, 8(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2473:
	.size	_ZN6icu_6714SearchIteratorC2ERKS0_, .-_ZN6icu_6714SearchIteratorC2ERKS0_
	.globl	_ZN6icu_6714SearchIteratorC1ERKS0_
	.set	_ZN6icu_6714SearchIteratorC1ERKS0_,_ZN6icu_6714SearchIteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIteratorD2Ev
	.type	_ZN6icu_6714SearchIteratorD2Ev, @function
_ZN6icu_6714SearchIteratorD2Ev:
.LFB2476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714SearchIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L63
	call	uprv_free_67@PLT
.L63:
	leaq	24(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2476:
	.size	_ZN6icu_6714SearchIteratorD2Ev, .-_ZN6icu_6714SearchIteratorD2Ev
	.globl	_ZN6icu_6714SearchIteratorD1Ev
	.set	_ZN6icu_6714SearchIteratorD1Ev,_ZN6icu_6714SearchIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIteratorD0Ev
	.type	_ZN6icu_6714SearchIteratorD0Ev, @function
_ZN6icu_6714SearchIteratorD0Ev:
.LFB2478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714SearchIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L69
	call	uprv_free_67@PLT
.L69:
	leaq	24(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2478:
	.size	_ZN6icu_6714SearchIteratorD0Ev, .-_ZN6icu_6714SearchIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIterator12setAttributeE16USearchAttribute21USearchAttributeValueR10UErrorCode
	.type	_ZN6icu_6714SearchIterator12setAttributeE16USearchAttribute21USearchAttributeValueR10UErrorCode, @function
_ZN6icu_6714SearchIterator12setAttributeE16USearchAttribute21USearchAttributeValueR10UErrorCode:
.LFB2479:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jle	.L81
.L75:
	cmpl	$5, %edx
	je	.L82
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	cmpl	$1, %esi
	je	.L76
	cmpl	$2, %esi
	je	.L77
	testl	%esi, %esi
	je	.L83
	movl	$1, (%rcx)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L76:
	movq	8(%rdi), %rax
	cmpl	$1, %edx
	sete	13(%rax)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L83:
	movq	8(%rdi), %rax
	cmpl	$1, %edx
	sete	12(%rax)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L77:
	leal	-3(%rdx), %esi
	movq	8(%rdi), %rax
	cmpl	$1, %esi
	jbe	.L84
	xorl	%esi, %esi
	movw	%si, 14(%rax)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L84:
	movw	%dx, 14(%rax)
	ret
	.cfi_endproc
.LFE2479:
	.size	_ZN6icu_6714SearchIterator12setAttributeE16USearchAttribute21USearchAttributeValueR10UErrorCode, .-_ZN6icu_6714SearchIterator12setAttributeE16USearchAttribute21USearchAttributeValueR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SearchIterator12getAttributeE16USearchAttribute
	.type	_ZNK6icu_6714SearchIterator12getAttributeE16USearchAttribute, @function
_ZNK6icu_6714SearchIterator12getAttributeE16USearchAttribute:
.LFB2480:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	je	.L86
	cmpl	$2, %esi
	je	.L87
	movl	$-1, %eax
	testl	%esi, %esi
	je	.L91
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	movq	8(%rdi), %rax
	cmpb	$1, 12(%rax)
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	movq	8(%rdi), %rax
	movswl	14(%rax), %eax
	leal	-3(%rax), %edx
	cmpw	$2, %dx
	cmovnb	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	movq	8(%rdi), %rax
	cmpb	$1, 13(%rax)
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE2480:
	.size	_ZNK6icu_6714SearchIterator12getAttributeE16USearchAttribute, .-_ZNK6icu_6714SearchIterator12getAttributeE16USearchAttribute
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SearchIterator15getMatchedStartEv
	.type	_ZNK6icu_6714SearchIterator15getMatchedStartEv, @function
_ZNK6icu_6714SearchIterator15getMatchedStartEv:
.LFB2481:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	32(%rax), %eax
	ret
	.cfi_endproc
.LFE2481:
	.size	_ZNK6icu_6714SearchIterator15getMatchedStartEv, .-_ZNK6icu_6714SearchIterator15getMatchedStartEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SearchIterator16getMatchedLengthEv
	.type	_ZNK6icu_6714SearchIterator16getMatchedLengthEv, @function
_ZNK6icu_6714SearchIterator16getMatchedLengthEv:
.LFB2482:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	36(%rax), %eax
	ret
	.cfi_endproc
.LFE2482:
	.size	_ZNK6icu_6714SearchIterator16getMatchedLengthEv, .-_ZNK6icu_6714SearchIterator16getMatchedLengthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SearchIterator14getMatchedTextERNS_13UnicodeStringE
	.type	_ZNK6icu_6714SearchIterator14getMatchedTextERNS_13UnicodeStringE, @function
_ZNK6icu_6714SearchIterator14getMatchedTextERNS_13UnicodeStringE:
.LFB2483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %rax
	movslq	32(%rax), %rdx
	movl	36(%rax), %ebx
	cmpl	$-1, %edx
	je	.L95
	testl	%ebx, %ebx
	jne	.L107
.L95:
	movzwl	8(%r12), %edx
	movl	$2, %eax
	movl	%edx, %ecx
	andl	$31, %ecx
	andl	$1, %edx
	cmove	%ecx, %eax
	movw	%ax, 8(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movq	(%rax), %rax
	movq	%rsi, %rdi
	leaq	(%rax,%rdx,2), %r13
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L96
	sarl	$5, %edx
.L97:
	addq	$8, %rsp
	movl	%ebx, %r9d
	movq	%r13, %rcx
	movq	%r12, %rdi
	popq	%rbx
	xorl	%r8d, %r8d
	popq	%r12
	xorl	%esi, %esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movl	12(%r12), %edx
	jmp	.L97
	.cfi_endproc
.LFE2483:
	.size	_ZNK6icu_6714SearchIterator14getMatchedTextERNS_13UnicodeStringE, .-_ZNK6icu_6714SearchIterator14getMatchedTextERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIterator16setBreakIteratorEPNS_13BreakIteratorER10UErrorCode
	.type	_ZN6icu_6714SearchIterator16setBreakIteratorEPNS_13BreakIteratorER10UErrorCode, @function
_ZN6icu_6714SearchIterator16setBreakIteratorEPNS_13BreakIteratorER10UErrorCode:
.LFB2484:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L110
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	movq	8(%rdi), %rax
	movq	%rsi, 24(%rax)
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE2484:
	.size	_ZN6icu_6714SearchIterator16setBreakIteratorEPNS_13BreakIteratorER10UErrorCode, .-_ZN6icu_6714SearchIterator16setBreakIteratorEPNS_13BreakIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SearchIterator16getBreakIteratorEv
	.type	_ZNK6icu_6714SearchIterator16getBreakIteratorEv, @function
_ZNK6icu_6714SearchIterator16getBreakIteratorEv:
.LFB2485:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE2485:
	.size	_ZNK6icu_6714SearchIterator16getBreakIteratorEv, .-_ZNK6icu_6714SearchIterator16getBreakIteratorEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SearchIterator7getTextEv
	.type	_ZNK6icu_6714SearchIterator7getTextEv, @function
_ZNK6icu_6714SearchIterator7getTextEv:
.LFB2488:
	.cfi_startproc
	endbr64
	leaq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE2488:
	.size	_ZNK6icu_6714SearchIterator7getTextEv, .-_ZNK6icu_6714SearchIterator7getTextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIterator5firstER10UErrorCode
	.type	_ZN6icu_6714SearchIterator5firstER10UErrorCode, @function
_ZN6icu_6714SearchIterator5firstER10UErrorCode:
.LFB2490:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L114
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rdi, %r12
	call	*24(%rax)
	movq	(%r12), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	movq	80(%rax), %rax
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2490:
	.size	_ZN6icu_6714SearchIterator5firstER10UErrorCode, .-_ZN6icu_6714SearchIterator5firstER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIterator9followingEiR10UErrorCode
	.type	_ZN6icu_6714SearchIterator9followingEiR10UErrorCode, @function
_ZN6icu_6714SearchIterator9followingEiR10UErrorCode:
.LFB2491:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L119
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	(%r12), %rax
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	80(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2491:
	.size	_ZN6icu_6714SearchIterator9followingEiR10UErrorCode, .-_ZN6icu_6714SearchIterator9followingEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIterator4lastER10UErrorCode
	.type	_ZN6icu_6714SearchIterator4lastER10UErrorCode, @function
_ZN6icu_6714SearchIterator4lastER10UErrorCode:
.LFB2492:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L124
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	8(%rdi), %rax
	movq	%rdi, %r12
	movq	%r13, %rdx
	movl	8(%rax), %esi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	8(%r12), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	8(%rax), %esi
	movq	(%r12), %rax
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	movq	88(%rax), %rax
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2492:
	.size	_ZN6icu_6714SearchIterator4lastER10UErrorCode, .-_ZN6icu_6714SearchIterator4lastER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIterator9precedingEiR10UErrorCode
	.type	_ZN6icu_6714SearchIterator9precedingEiR10UErrorCode, @function
_ZN6icu_6714SearchIterator9precedingEiR10UErrorCode:
.LFB2493:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L129
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	(%r12), %rax
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	88(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L129:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2493:
	.size	_ZN6icu_6714SearchIterator9precedingEiR10UErrorCode, .-_ZN6icu_6714SearchIterator9precedingEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIteratorC2Ev
	.type	_ZN6icu_6714SearchIteratorC2Ev, @function
_ZN6icu_6714SearchIteratorC2Ev:
.LFB2498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714SearchIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 24(%rdi)
	movl	$2, %eax
	movw	%ax, 32(%rdi)
	movl	$48, %edi
	call	uprv_malloc_67@PLT
	movl	$4294967295, %edx
	movq	$0, 16(%rbx)
	movq	%rdx, 32(%rax)
	movl	$257, %edx
	movq	$0, 24(%rax)
	movw	%dx, 40(%rax)
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2498:
	.size	_ZN6icu_6714SearchIteratorC2Ev, .-_ZN6icu_6714SearchIteratorC2Ev
	.globl	_ZN6icu_6714SearchIteratorC1Ev
	.set	_ZN6icu_6714SearchIteratorC1Ev,_ZN6icu_6714SearchIteratorC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIteratorC2ERKNS_13UnicodeStringEPNS_13BreakIteratorE
	.type	_ZN6icu_6714SearchIteratorC2ERKNS_13UnicodeStringEPNS_13BreakIteratorE, @function
_ZN6icu_6714SearchIteratorC2ERKNS_13UnicodeStringEPNS_13BreakIteratorE:
.LFB2501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714SearchIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$24, %rdi
	movq	%rdx, -8(%rdi)
	movq	%rax, -24(%rdi)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$48, %edi
	call	uprv_malloc_67@PLT
	movl	$257, %edx
	movl	$4294967295, %ecx
	movw	%dx, 40(%rax)
	movzwl	32(%rbx), %edx
	movq	%rax, 8(%rbx)
	movl	$0, 12(%rax)
	movq	$0, 24(%rax)
	movq	%rcx, 32(%rax)
	testb	$17, %dl
	jne	.L140
	andl	$2, %edx
	jne	.L142
	movq	48(%rbx), %rbx
.L136:
	movswl	8(%r12), %edx
	movq	%rbx, (%rax)
	testw	%dx, %dx
	js	.L138
.L143:
	sarl	$5, %edx
	movl	%edx, 8(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movswl	8(%r12), %edx
	addq	$34, %rbx
	movq	%rbx, (%rax)
	testw	%dx, %dx
	jns	.L143
.L138:
	movl	12(%r12), %edx
	movl	%edx, 8(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	xorl	%ebx, %ebx
	jmp	.L136
	.cfi_endproc
.LFE2501:
	.size	_ZN6icu_6714SearchIteratorC2ERKNS_13UnicodeStringEPNS_13BreakIteratorE, .-_ZN6icu_6714SearchIteratorC2ERKNS_13UnicodeStringEPNS_13BreakIteratorE
	.globl	_ZN6icu_6714SearchIteratorC1ERKNS_13UnicodeStringEPNS_13BreakIteratorE
	.set	_ZN6icu_6714SearchIteratorC1ERKNS_13UnicodeStringEPNS_13BreakIteratorE,_ZN6icu_6714SearchIteratorC2ERKNS_13UnicodeStringEPNS_13BreakIteratorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIteratorC2ERNS_17CharacterIteratorEPNS_13BreakIteratorE
	.type	_ZN6icu_6714SearchIteratorC2ERNS_17CharacterIteratorEPNS_13BreakIteratorE, @function
_ZN6icu_6714SearchIteratorC2ERNS_17CharacterIteratorEPNS_13BreakIteratorE:
.LFB2504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714SearchIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 24(%rdi)
	movl	$2, %eax
	movq	%rdx, 16(%rdi)
	movw	%ax, 32(%rdi)
	movl	$48, %edi
	call	uprv_malloc_67@PLT
	movl	$257, %edx
	movl	$4294967295, %ecx
	movq	%r13, %rdi
	movw	%dx, 40(%rax)
	leaq	24(%rbx), %rsi
	movq	%rax, 8(%rbx)
	movl	$0, 12(%rax)
	movq	$0, 24(%rax)
	movq	%rcx, 32(%rax)
	movq	0(%r13), %rax
	call	*208(%rax)
	movswl	32(%rbx), %eax
	movq	8(%rbx), %rdx
	testb	$17, %al
	jne	.L149
	leaq	34(%rbx), %rcx
	testb	$2, %al
	je	.L151
.L145:
	movq	%rcx, (%rdx)
	testw	%ax, %ax
	js	.L147
.L152:
	sarl	$5, %eax
.L148:
	movl	%eax, 8(%rdx)
	movq	%r12, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movq	48(%rbx), %rcx
	movq	%rcx, (%rdx)
	testw	%ax, %ax
	jns	.L152
.L147:
	movl	36(%rbx), %eax
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L149:
	xorl	%ecx, %ecx
	jmp	.L145
	.cfi_endproc
.LFE2504:
	.size	_ZN6icu_6714SearchIteratorC2ERNS_17CharacterIteratorEPNS_13BreakIteratorE, .-_ZN6icu_6714SearchIteratorC2ERNS_17CharacterIteratorEPNS_13BreakIteratorE
	.globl	_ZN6icu_6714SearchIteratorC1ERNS_17CharacterIteratorEPNS_13BreakIteratorE
	.set	_ZN6icu_6714SearchIteratorC1ERNS_17CharacterIteratorEPNS_13BreakIteratorE,_ZN6icu_6714SearchIteratorC2ERNS_17CharacterIteratorEPNS_13BreakIteratorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIteratoraSERKS0_
	.type	_ZN6icu_6714SearchIteratoraSERKS0_, @function
_ZN6icu_6714SearchIteratoraSERKS0_:
.LFB2506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L154
	movq	16(%rsi), %rax
	movq	%rsi, %rbx
	leaq	24(%rdi), %rdi
	leaq	24(%rsi), %rsi
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	8(%rbx), %rdx
	movq	8(%r12), %rax
	movq	24(%rdx), %rcx
	movq	%rcx, 24(%rax)
	movzbl	13(%rdx), %ecx
	movb	%cl, 13(%rax)
	movzbl	12(%rdx), %ecx
	movb	%cl, 12(%rax)
	movzwl	14(%rdx), %ecx
	movw	%cx, 14(%rax)
	movl	32(%rdx), %ecx
	movl	%ecx, 32(%rax)
	movl	36(%rdx), %ecx
	movl	%ecx, 36(%rax)
	movq	(%rdx), %rcx
	movl	8(%rdx), %edx
	movq	%rcx, (%rax)
	movl	%edx, 8(%rax)
.L154:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2506:
	.size	_ZN6icu_6714SearchIteratoraSERKS0_, .-_ZN6icu_6714SearchIteratoraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIterator16setMatchNotFoundEv
	.type	_ZN6icu_6714SearchIterator16setMatchNotFoundEv, @function
_ZN6icu_6714SearchIterator16setMatchNotFoundEv:
.LFB2509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6714SearchIterator13setMatchStartEi(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	104(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L157
	movq	8(%rdi), %rdx
	leaq	_ZN6icu_6714SearchIterator14setMatchLengthEi(%rip), %rcx
	movl	$-1, 32(%rdx)
	movq	96(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L159
.L166:
	movq	8(%rdi), %rdx
	movl	$0, 36(%rdx)
.L160:
	cmpb	$0, 40(%rdx)
	movq	24(%rax), %rax
	movl	$0, -12(%rbp)
	je	.L161
	leaq	-12(%rbp), %r8
	movl	8(%rdx), %esi
	movq	%r8, %rdx
	call	*%rax
.L156:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	leaq	-12(%rbp), %rdx
	xorl	%esi, %esi
	call	*%rax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%rdi, -24(%rbp)
	movl	$-1, %esi
	call	*%rdx
	movq	-24(%rbp), %rdi
	leaq	_ZN6icu_6714SearchIterator14setMatchLengthEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	96(%rax), %rdx
	cmpq	%rcx, %rdx
	je	.L166
.L159:
	movq	%rdi, -24(%rbp)
	xorl	%esi, %esi
	call	*%rdx
	movq	-24(%rbp), %rdi
	movq	8(%rdi), %rdx
	movq	(%rdi), %rax
	jmp	.L160
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2509:
	.size	_ZN6icu_6714SearchIterator16setMatchNotFoundEv, .-_ZN6icu_6714SearchIterator16setMatchNotFoundEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIterator4nextER10UErrorCode
	.type	_ZN6icu_6714SearchIterator4nextER10UErrorCode, @function
_ZN6icu_6714SearchIterator4nextER10UErrorCode:
.LFB2494:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L176
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rdi, %r12
	call	*32(%rax)
	movq	8(%r12), %rcx
	movl	%eax, %esi
	cmpb	$1, 40(%rcx)
	movl	32(%rcx), %eax
	movb	$0, 41(%rcx)
	movl	36(%rcx), %edx
	je	.L181
	movb	$1, 40(%rcx)
	cmpl	$-1, %eax
	je	.L174
.L167:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movl	8(%rcx), %edi
	cmpl	%edi, %esi
	je	.L170
	cmpl	%edi, %eax
	je	.L170
	cmpl	$-1, %eax
	je	.L174
	addl	%edx, %eax
	cmpl	%edi, %eax
	jge	.L170
.L174:
	testl	%edx, %edx
	jle	.L173
	cmpb	$0, 12(%rcx)
	je	.L175
	addl	$1, %esi
.L173:
	movq	(%r12), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	80(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	addl	%edx, %esi
	jmp	.L173
.L170:
	movq	%r12, %rdi
	call	_ZN6icu_6714SearchIterator16setMatchNotFoundEv
	movl	$-1, %eax
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2494:
	.size	_ZN6icu_6714SearchIterator4nextER10UErrorCode, .-_ZN6icu_6714SearchIterator4nextER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SearchIterator8previousER10UErrorCode
	.type	_ZN6icu_6714SearchIterator8previousER10UErrorCode, @function
_ZN6icu_6714SearchIterator8previousER10UErrorCode:
.LFB2495:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L191
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	(%rdi), %rcx
	cmpb	$0, 41(%rax)
	jne	.L201
	call	*32(%rcx)
	movq	8(%r12), %rdx
	movl	%eax, %r14d
	cmpb	$1, 40(%rdx)
	movl	32(%rdx), %esi
	je	.L202
.L186:
	testl	%r14d, %r14d
	je	.L192
	testl	%esi, %esi
	je	.L192
	cmpl	$-1, %esi
	je	.L187
	cmpb	$0, 12(%rdx)
	je	.L190
	movl	36(%rdx), %eax
	leal	-2(%rsi,%rax), %esi
.L190:
	movq	(%r12), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	88(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movl	8(%rax), %r14d
	xorl	%edx, %edx
	movw	%dx, 40(%rax)
	movq	%rsi, %rdx
	movl	%r14d, %esi
	call	*24(%rcx)
	movq	8(%r12), %rdx
	cmpb	$1, 40(%rdx)
	movl	32(%rdx), %esi
	jne	.L186
.L202:
	movb	$0, 40(%rdx)
	cmpl	$-1, %esi
	je	.L187
.L182:
	addq	$8, %rsp
	movl	%esi, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	88(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$-1, %esi
	movl	%esi, %eax
	ret
.L192:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	%r12, %rdi
	call	_ZN6icu_6714SearchIterator16setMatchNotFoundEv
	movl	$-1, %esi
	jmp	.L182
	.cfi_endproc
.LFE2495:
	.size	_ZN6icu_6714SearchIterator8previousER10UErrorCode, .-_ZN6icu_6714SearchIterator8previousER10UErrorCode
	.weak	_ZTSN6icu_6714SearchIteratorE
	.section	.rodata._ZTSN6icu_6714SearchIteratorE,"aG",@progbits,_ZTSN6icu_6714SearchIteratorE,comdat
	.align 16
	.type	_ZTSN6icu_6714SearchIteratorE, @object
	.size	_ZTSN6icu_6714SearchIteratorE, 26
_ZTSN6icu_6714SearchIteratorE:
	.string	"N6icu_6714SearchIteratorE"
	.weak	_ZTIN6icu_6714SearchIteratorE
	.section	.data.rel.ro._ZTIN6icu_6714SearchIteratorE,"awG",@progbits,_ZTIN6icu_6714SearchIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6714SearchIteratorE, @object
	.size	_ZTIN6icu_6714SearchIteratorE, 24
_ZTIN6icu_6714SearchIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714SearchIteratorE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6714SearchIteratorE
	.section	.data.rel.ro._ZTVN6icu_6714SearchIteratorE,"awG",@progbits,_ZTVN6icu_6714SearchIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6714SearchIteratorE, @object
	.size	_ZTVN6icu_6714SearchIteratorE, 128
_ZTVN6icu_6714SearchIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6714SearchIteratorE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_6714SearchIterator7setTextERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6714SearchIterator7setTextERNS_17CharacterIteratorER10UErrorCode
	.quad	_ZNK6icu_6714SearchIteratoreqERKS0_
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_6714SearchIterator5resetEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_6714SearchIterator14setMatchLengthEi
	.quad	_ZN6icu_6714SearchIterator13setMatchStartEi
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
