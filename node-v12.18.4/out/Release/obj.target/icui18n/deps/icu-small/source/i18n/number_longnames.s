	.file	"number_longnames.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15LongNameHandler11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.type	_ZNK6icu_676number4impl15LongNameHandler11getModifierENS1_6SignumENS_14StandardPlural4FormE, @function
_ZNK6icu_676number4impl15LongNameHandler11getModifierENS1_6SignumENS_14StandardPlural4FormE:
.LFB3450:
	.cfi_startproc
	endbr64
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rdx,%rax,4), %rax
	leaq	16(%rdi,%rax,8), %rax
	ret
	.cfi_endproc
.LFE3450:
	.size	_ZNK6icu_676number4impl15LongNameHandler11getModifierENS1_6SignumENS_14StandardPlural4FormE, .-_ZNK6icu_676number4impl15LongNameHandler11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dnam"
.LC1:
	.string	"per"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_115PluralTableSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode, @function
_ZN12_GLOBAL__N_115PluralTableSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode:
.LFB3437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$184, %rsp
	movq	%rsi, -200(%rbp)
	movq	%r15, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%r8, %rdx
	call	*88(%rax)
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L3
	leaq	-200(%rbp), %rax
	xorl	%r12d, %r12d
	movq	%rax, -208(%rbp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L19:
	movq	-200(%rbp), %r8
	movl	$5, %ecx
	movl	$6, %eax
	leaq	.LC0(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L6
	movl	$4, %ecx
	movq	%r8, %rsi
	movl	$7, %eax
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L6
	movq	%rbx, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6714StandardPlural15indexFromStringEPKcR10UErrorCode@PLT
.L6:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L3
	movslq	%eax, %r8
	movq	8(%r13), %rax
	salq	$6, %r8
	testb	$1, 8(%rax,%r8)
	je	.L9
	movq	(%r15), %rax
	movq	%r8, -224(%rbp)
	movq	%rbx, %rdx
	movq	%r15, %rdi
	leaq	-188(%rbp), %rsi
	movl	$0, -188(%rbp)
	call	*32(%rax)
	leaq	-128(%rbp), %r10
	movl	-188(%rbp), %ecx
	leaq	-184(%rbp), %rdx
	movl	$1, %esi
	movq	%r10, %rdi
	movq	%rax, -184(%rbp)
	movq	%r10, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-224(%rbp), %r8
	movq	-216(%rbp), %r10
	movq	8(%r13), %rdi
	movq	%r10, %rsi
	addq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-216(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L3
.L9:
	addl	$1, %r12d
.L8:
	movq	-208(%rbp), %rdx
	movq	%r15, %rcx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	jne	.L19
.L3:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L20:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3437:
	.size	_ZN12_GLOBAL__N_115PluralTableSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode, .-_ZN12_GLOBAL__N_115PluralTableSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_115PluralTableSinkD2Ev, @function
_ZN12_GLOBAL__N_115PluralTableSinkD2Ev:
.LFB4646:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_115PluralTableSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE4646:
	.size	_ZN12_GLOBAL__N_115PluralTableSinkD2Ev, .-_ZN12_GLOBAL__N_115PluralTableSinkD2Ev
	.set	_ZN12_GLOBAL__N_115PluralTableSinkD1Ev,_ZN12_GLOBAL__N_115PluralTableSinkD2Ev
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_115PluralTableSinkD0Ev, @function
_ZN12_GLOBAL__N_115PluralTableSinkD0Ev:
.LFB4648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_115PluralTableSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4648:
	.size	_ZN12_GLOBAL__N_115PluralTableSinkD0Ev, .-_ZN12_GLOBAL__N_115PluralTableSinkD0Ev
	.section	.rodata.str1.1
.LC2:
	.string	"icudt67l-unit"
.LC3:
	.string	"-person"
.LC4:
	.string	"units"
.LC5:
	.string	"Narrow"
.LC6:
	.string	"Short"
.LC7:
	.string	"/"
.LC8:
	.string	"unitsShort/"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_114getMeasureDataERKN6icu_676LocaleERKNS0_11MeasureUnitERK16UNumberUnitWidthPNS0_13UnicodeStringER10UErrorCode, @function
_ZN12_GLOBAL__N_114getMeasureDataERKN6icu_676LocaleERKNS0_11MeasureUnitERK16UNumberUnitWidthPNS0_13UnicodeStringER10UErrorCode:
.LFB3438:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	512(%rcx), %rbx
	subq	$168, %rsp
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN12_GLOBAL__N_115PluralTableSinkE(%rip), %rax
	movq	%rcx, -152(%rbp)
	movq	%rax, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r15, %rdi
	addq	$64, %r15
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	cmpq	%rbx, %r15
	jne	.L25
	movq	40(%r13), %rsi
	movq	%r12, %rdx
	leaq	.LC2(%rip), %rdi
	call	ures_open_67@PLT
	movl	(%r12), %edx
	leaq	-160(%rbp), %r8
	movq	%rax, %r13
	testl	%edx, %edx
	jle	.L49
.L34:
	testq	%r13, %r13
	je	.L36
	movq	%r13, %rdi
	movq	%r8, -184(%rbp)
	call	ures_close_67@PLT
	movq	-184(%rbp), %r8
.L36:
	leaq	16+_ZTVN12_GLOBAL__N_115PluralTableSinkE(%rip), %rax
	movq	%r8, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%rax, %rbx
	cmpl	$7, %eax
	jg	.L51
.L27:
	movq	%r14, %rdi
	leaq	-144(%rbp), %rbx
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-144(%rbp), %rax
	movq	%rax, -200(%rbp)
	movl	-136(%rbp), %eax
	movl	%eax, -188(%rbp)
.L28:
	leaq	-115(%rbp), %rax
	movq	%rbx, %rdi
	leaq	.LC4(%rip), %rsi
	movl	$0, -72(%rbp)
	movq	%rax, -128(%rbp)
	xorl	%eax, %eax
	leaq	-128(%rbp), %r15
	movw	%ax, -116(%rbp)
	movl	$40, -120(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-144(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r15, %rdi
	movl	-136(%rbp), %edx
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-184(%rbp), %rax
	leaq	.LC5(%rip), %rsi
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L46
	cmpl	$1, %eax
	je	.L52
.L30:
	leaq	.LC7(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	leaq	.LC7(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-188(%rbp), %edx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-200(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	leaq	-160(%rbp), %r8
	movq	-128(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	leaq	-164(%rbp), %rcx
	movq	%r8, -208(%rbp)
	movl	$0, -164(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	movq	-184(%rbp), %rax
	movq	-208(%rbp), %r8
	cmpl	$1, (%rax)
	je	.L53
	movq	-128(%rbp), %rax
	movl	$0, -72(%rbp)
	movq	%rbx, %rdi
	leaq	.LC8(%rip), %rsi
	movq	%r8, -184(%rbp)
	movb	$0, (%rax)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	leaq	.LC7(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-188(%rbp), %edx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-200(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-128(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	-184(%rbp), %r8
	movq	%r8, %rdx
	call	ures_getAllItemsWithFallback_67@PLT
	cmpb	$0, -116(%rbp)
	movq	-184(%rbp), %r8
	je	.L34
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-184(%rbp), %r8
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%r14, %rdi
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movslq	%ebx, %rdx
	movl	$8, %ecx
	leaq	.LC3(%rip), %rdi
	leaq	-7(%rdx,%rax), %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L27
	movq	%r14, %rdi
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movq	%rax, -200(%rbp)
	leal	-7(%rbx), %eax
	leaq	-144(%rbp), %rbx
	movl	%eax, -188(%rbp)
	jmp	.L28
.L52:
	leaq	.LC6(%rip), %rsi
.L46:
	movq	%rbx, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L53:
	movl	-164(%rbp), %eax
	testl	%eax, %eax
	jle	.L32
	movl	%eax, (%r12)
.L32:
	cmpb	$0, -116(%rbp)
	je	.L34
	movq	-128(%rbp), %rdi
	movq	%r8, -184(%rbp)
	call	uprv_free_67@PLT
	movq	-184(%rbp), %r8
	jmp	.L34
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3438:
	.size	_ZN12_GLOBAL__N_114getMeasureDataERKN6icu_676LocaleERKNS0_11MeasureUnitERK16UNumberUnitWidthPNS0_13UnicodeStringER10UErrorCode, .-_ZN12_GLOBAL__N_114getMeasureDataERKN6icu_676LocaleERKNS0_11MeasureUnitERK16UNumberUnitWidthPNS0_13UnicodeStringER10UErrorCode
	.section	.text._ZN6icu_676number4impl15LongNameHandlerD2Ev,"axG",@progbits,_ZN6icu_676number4impl15LongNameHandlerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl15LongNameHandlerD2Ev
	.type	_ZN6icu_676number4impl15LongNameHandlerD2Ev, @function
_ZN6icu_676number4impl15LongNameHandlerD2Ev:
.LFB4642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN6icu_676number4impl15LongNameHandlerE(%rip), %rax
	leaq	-48(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 13, -24
	leaq	-88(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	536(%rdi), %rbx
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	.p2align 4,,10
	.p2align 3
.L55:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$104, %rbx
	call	*(%rax)
	cmpq	%r13, %rbx
	jne	.L55
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl13ModifierStoreD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.cfi_endproc
.LFE4642:
	.size	_ZN6icu_676number4impl15LongNameHandlerD2Ev, .-_ZN6icu_676number4impl15LongNameHandlerD2Ev
	.weak	_ZN6icu_676number4impl15LongNameHandlerD1Ev
	.set	_ZN6icu_676number4impl15LongNameHandlerD1Ev,_ZN6icu_676number4impl15LongNameHandlerD2Ev
	.text
	.p2align 4
	.globl	_ZThn8_NK6icu_676number4impl15LongNameHandler11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.type	_ZThn8_NK6icu_676number4impl15LongNameHandler11getModifierENS1_6SignumENS_14StandardPlural4FormE, @function
_ZThn8_NK6icu_676number4impl15LongNameHandler11getModifierENS1_6SignumENS_14StandardPlural4FormE:
.LFB4670:
	.cfi_startproc
	endbr64
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rdx,%rax,4), %rax
	leaq	8(%rdi,%rax,8), %rax
	ret
	.cfi_endproc
.LFE4670:
	.size	_ZThn8_NK6icu_676number4impl15LongNameHandler11getModifierENS1_6SignumENS_14StandardPlural4FormE, .-_ZThn8_NK6icu_676number4impl15LongNameHandler11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.section	.text._ZN6icu_676number4impl15LongNameHandlerD0Ev,"axG",@progbits,_ZN6icu_676number4impl15LongNameHandlerD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N6icu_676number4impl15LongNameHandlerD0Ev
	.type	_ZThn8_N6icu_676number4impl15LongNameHandlerD0Ev, @function
_ZThn8_N6icu_676number4impl15LongNameHandlerD0Ev:
.LFB4671:
	.cfi_startproc
	endbr64
	leaq	64+_ZTVN6icu_676number4impl15LongNameHandlerE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-48(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	-8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	632(%rdi), %rbx
	movups	%xmm0, -8(%rdi)
	.p2align 4,,10
	.p2align 3
.L60:
	movq	-104(%rbx), %rax
	subq	$104, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%r13, %rbx
	jne	.L60
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl13ModifierStoreD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4671:
	.size	_ZThn8_N6icu_676number4impl15LongNameHandlerD0Ev, .-_ZThn8_N6icu_676number4impl15LongNameHandlerD0Ev
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl15LongNameHandlerD0Ev
	.type	_ZN6icu_676number4impl15LongNameHandlerD0Ev, @function
_ZN6icu_676number4impl15LongNameHandlerD0Ev:
.LFB4644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN6icu_676number4impl15LongNameHandlerE(%rip), %rax
	leaq	-48(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 13, -24
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	640(%rdi), %rbx
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	.p2align 4,,10
	.p2align 3
.L64:
	movq	-104(%rbx), %rax
	subq	$104, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%r13, %rbx
	jne	.L64
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl13ModifierStoreD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4644:
	.size	_ZN6icu_676number4impl15LongNameHandlerD0Ev, .-_ZN6icu_676number4impl15LongNameHandlerD0Ev
	.section	.text._ZN6icu_676number4impl15LongNameHandlerD2Ev,"axG",@progbits,_ZN6icu_676number4impl15LongNameHandlerD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N6icu_676number4impl15LongNameHandlerD1Ev
	.type	_ZThn8_N6icu_676number4impl15LongNameHandlerD1Ev, @function
_ZThn8_N6icu_676number4impl15LongNameHandlerD1Ev:
.LFB4672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN6icu_676number4impl15LongNameHandlerE(%rip), %rax
	leaq	-48(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	632(%rdi), %rbx
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	.p2align 4,,10
	.p2align 3
.L68:
	movq	-104(%rbx), %rax
	subq	$104, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%r13, %rbx
	jne	.L68
	movq	%r12, %rdi
	leaq	-8(%r12), %r13
	call	_ZN6icu_676number4impl13ModifierStoreD2Ev@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.cfi_endproc
.LFE4672:
	.size	_ZThn8_N6icu_676number4impl15LongNameHandlerD1Ev, .-_ZThn8_N6icu_676number4impl15LongNameHandlerD1Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3756:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3756:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3759:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L84
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L72
	cmpb	$0, 12(%rbx)
	jne	.L85
.L76:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L72:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L76
	.cfi_endproc
.LFE3759:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3762:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L88
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3762:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3765:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L91
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3765:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L97
.L93:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L98
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L98:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3767:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3768:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3768:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3769:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3769:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3770:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3770:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3771:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3771:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3772:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3772:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3773:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L114
	testl	%edx, %edx
	jle	.L114
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L117
.L106:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L106
	.cfi_endproc
.LFE3773:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L121
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L121
	testl	%r12d, %r12d
	jg	.L128
	cmpb	$0, 12(%rbx)
	jne	.L129
.L123:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L123
.L129:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L121:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3774:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L131
	movq	(%rdi), %r8
.L132:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L135
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L135
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L135:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3775:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3776:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L142
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3776:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3777:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3777:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3778:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3778:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3779:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3779:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3781:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3781:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3783:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3783:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15LongNameHandler18getUnitDisplayNameERKNS_6LocaleERKNS_11MeasureUnitE16UNumberUnitWidthR10UErrorCode
	.type	_ZN6icu_676number4impl15LongNameHandler18getUnitDisplayNameERKNS_6LocaleERKNS_11MeasureUnitE16UNumberUnitWidthR10UErrorCode, @function
_ZN6icu_676number4impl15LongNameHandler18getUnitDisplayNameERKNS_6LocaleERKNS_11MeasureUnitE16UNumberUnitWidthR10UErrorCode:
.LFB3443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$544, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%ecx, -564(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L155
	movl	$2, %r13d
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movl	$2, %ecx
	movw	%r13w, -104(%rbp)
	movl	$2, %edx
	leaq	-560(%rbp), %r13
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r9d
	movl	$2, %r10d
	movl	$2, %r11d
	movw	%dx, -552(%rbp)
	movl	$2, %ebx
	movw	%cx, -488(%rbp)
	leaq	-564(%rbp), %rdx
	movq	%r13, %rcx
	movl	$2, %r12d
	movq	%rax, -560(%rbp)
	movq	%rax, -496(%rbp)
	movq	%rax, -432(%rbp)
	movw	%r9w, -424(%rbp)
	movq	%rax, -368(%rbp)
	movw	%r10w, -360(%rbp)
	movq	%rax, -304(%rbp)
	movw	%r11w, -296(%rbp)
	movq	%rax, -240(%rbp)
	movw	%bx, -232(%rbp)
	leaq	-112(%rbp), %rbx
	movq	%rax, -176(%rbp)
	movq	%rax, -112(%rbp)
	movw	%r12w, -168(%rbp)
	call	_ZN12_GLOBAL__N_114getMeasureDataERKN6icu_676LocaleERKNS0_11MeasureUnitERK16UNumberUnitWidthPNS0_13UnicodeStringER10UErrorCode
	leaq	-176(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L151:
	movq	(%rbx), %rax
	movq	%rbx, %r12
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%r13, %r12
	jne	.L151
.L148:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L156
	addq	$544, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L148
.L156:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3443:
	.size	_ZN6icu_676number4impl15LongNameHandler18getUnitDisplayNameERKNS_6LocaleERKNS_11MeasureUnitE16UNumberUnitWidthR10UErrorCode, .-_ZN6icu_676number4impl15LongNameHandler18getUnitDisplayNameERKNS_6LocaleERKNS_11MeasureUnitE16UNumberUnitWidthR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15LongNameHandler14getUnitPatternERKNS_6LocaleERKNS_11MeasureUnitE16UNumberUnitWidthNS_14StandardPlural4FormER10UErrorCode
	.type	_ZN6icu_676number4impl15LongNameHandler14getUnitPatternERKNS_6LocaleERKNS_11MeasureUnitE16UNumberUnitWidthNS_14StandardPlural4FormER10UErrorCode, @function
_ZN6icu_676number4impl15LongNameHandler14getUnitPatternERKNS_6LocaleERKNS_11MeasureUnitE16UNumberUnitWidthNS_14StandardPlural4FormER10UErrorCode:
.LFB3444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$552, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -580(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L168
	movl	$2, %r14d
	movq	%r9, %r12
	movq	%rsi, %rdi
	movl	%r8d, %r15d
	movw	%r14w, -184(%rbp)
	movq	%rdx, %rsi
	movl	$2, %ecx
	movl	$2, %edx
	movl	$2, %r8d
	movl	$2, %eax
	leaq	-576(%rbp), %r14
	movw	%dx, -568(%rbp)
	movl	$2, %r9d
	movl	$2, %r10d
	movw	%ax, -120(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	movw	%cx, -504(%rbp)
	movl	$2, %r11d
	movq	%r14, %rcx
	leaq	-580(%rbp), %rdx
	movw	%r8w, -440(%rbp)
	movq	%r12, %r8
	movq	%rbx, -576(%rbp)
	movq	%rbx, -512(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rbx, -384(%rbp)
	movw	%r9w, -376(%rbp)
	movq	%rbx, -320(%rbp)
	movw	%r10w, -312(%rbp)
	movq	%rbx, -256(%rbp)
	movw	%r11w, -248(%rbp)
	movq	%rbx, -192(%rbp)
	movq	%rbx, -128(%rbp)
	call	_ZN12_GLOBAL__N_114getMeasureDataERKN6icu_676LocaleERKNS0_11MeasureUnitERK16UNumberUnitWidthPNS0_13UnicodeStringER10UErrorCode
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L169
	movslq	%r15d, %rsi
	leaq	-256(%rbp), %rax
	movq	%r13, %rdi
	salq	$6, %rsi
	addq	%r14, %rsi
	testb	$1, 8(%rsi)
	cmovne	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L161:
	leaq	-128(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L163:
	movq	(%rbx), %rax
	movq	%rbx, %r12
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%r14, %r12
	jne	.L163
.L157:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L170
	addq	$552, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movl	$2, %eax
	movq	%rbx, 0(%r13)
	movq	%r13, %rdi
	movw	%ax, 8(%r13)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L157
.L170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3444:
	.size	_ZN6icu_676number4impl15LongNameHandler14getUnitPatternERKNS_6LocaleERKNS_11MeasureUnitE16UNumberUnitWidthNS_14StandardPlural4FormER10UErrorCode, .-_ZN6icu_676number4impl15LongNameHandler14getUnitPatternERKNS_6LocaleERKNS_11MeasureUnitE16UNumberUnitWidthNS_14StandardPlural4FormER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15LongNameHandler24simpleFormatsToModifiersEPKNS_13UnicodeStringENS_22FormattedStringBuilder5FieldER10UErrorCode
	.type	_ZN6icu_676number4impl15LongNameHandler24simpleFormatsToModifiersEPKNS_13UnicodeStringENS_22FormattedStringBuilder5FieldER10UErrorCode, @function
_ZN6icu_676number4impl15LongNameHandler24simpleFormatsToModifiersEPKNS_13UnicodeStringENS_22FormattedStringBuilder5FieldER10UErrorCode:
.LFB3446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-240(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-304(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	24(%rdi), %rbx
	subq	$328, %rsp
	movq	%rdi, -352(%rbp)
	movq	%rsi, -344(%rbp)
	movb	%dl, -353(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	320(%rsi), %rax
	movq	$0, -328(%rbp)
	movq	%rax, -368(%rbp)
.L176:
	movq	-328(%rbp), %rsi
	movq	%r13, %rdi
	movl	%esi, %r15d
	salq	$6, %rsi
	addq	-344(%rbp), %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	testb	$1, -296(%rbp)
	jne	.L189
.L172:
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L173
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%edx, %edx
	movq	%r12, %r8
	movq	%r13, %rsi
	movq	%rax, -232(%rbp)
	movl	$1, %ecx
	movq	%r14, %rdi
	movl	$2, %eax
	movw	%ax, -224(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L190
	movq	-352(%rbp), %rax
	movl	%r15d, %ecx
	movl	$2, %edx
	leaq	-320(%rbp), %rdi
	leaq	-160(%rbp), %r15
	leaq	8(%rax), %rsi
	call	_ZN6icu_676number4impl8Modifier10ParametersC1EPKNS1_13ModifierStoreENS1_6SignumENS_14StandardPlural4FormE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-312(%rbp), %r9
	movzbl	-353(%rbp), %edx
	movq	-320(%rbp), %r8
	call	_ZN6icu_676number4impl14SimpleModifierC1ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEbNS1_8Modifier10ParametersE@PLT
	leaq	-152(%rbp), %r8
	movq	%rbx, %rdi
	addq	$104, %rbx
	movq	%r8, %rsi
	movq	%r8, -336(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movzbl	-88(%rbp), %eax
	movdqu	-72(%rbp), %xmm0
	movq	-336(%rbp), %r8
	movb	%al, -40(%rbx)
	movzbl	-87(%rbp), %eax
	movq	%r8, %rdi
	movups	%xmm0, -24(%rbx)
	movb	%al, -39(%rbx)
	movl	-84(%rbp), %eax
	movl	%eax, -36(%rbx)
	movl	-80(%rbp), %eax
	movl	%eax, -32(%rbx)
	movl	-76(%rbp), %eax
	movl	%eax, -28(%rbx)
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$1, -328(%rbp)
	movq	-328(%rbp), %rax
	cmpq	$6, %rax
	jne	.L176
.L171:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L191
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	-368(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	testb	$1, -296(%rbp)
	je	.L172
	movl	$5, (%r12)
.L173:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%r14, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	jmp	.L173
.L191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3446:
	.size	_ZN6icu_676number4impl15LongNameHandler24simpleFormatsToModifiersEPKNS_13UnicodeStringENS_22FormattedStringBuilder5FieldER10UErrorCode, .-_ZN6icu_676number4impl15LongNameHandler24simpleFormatsToModifiersEPKNS_13UnicodeStringENS_22FormattedStringBuilder5FieldER10UErrorCode
	.section	.rodata.str1.1
.LC9:
	.string	"icudt67l-curr"
.LC10:
	.string	"CurrencyUnitPatterns"
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC11:
	.string	"{"
	.string	"1"
	.string	"}"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15LongNameHandler20forCurrencyLongNamesERKNS_6LocaleERKNS_12CurrencyUnitEPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode
	.type	_ZN6icu_676number4impl15LongNameHandler20forCurrencyLongNamesERKNS_6LocaleERKNS_12CurrencyUnitEPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode, @function
_ZN6icu_676number4impl15LongNameHandler20forCurrencyLongNamesERKNS_6LocaleERKNS_12CurrencyUnitEPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode:
.LFB3445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	subq	$760, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -744(%rbp)
	movl	$656, %edi
	movq	%rsi, -768(%rbp)
	movq	%rdx, -752(%rbp)
	movq	%rcx, -760(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L193
	leaq	64+_ZTVN6icu_676number4impl15LongNameHandlerE(%rip), %rax
	leaq	16(%r12), %r14
	leaq	-48(%rax), %rcx
	movq	%rax, %xmm1
	leaq	640(%r12), %rbx
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r14, %rdi
	addq	$104, %r14
	call	_ZN6icu_676number4impl14SimpleModifierC1Ev@PLT
	cmpq	%rbx, %r14
	jne	.L194
	movl	$2, %ebx
	movl	$2, %esi
	movl	$2, %edi
	movq	-752(%rbp), %xmm0
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%bx, -184(%rbp)
	movl	$2, %r8d
	leaq	-576(%rbp), %rbx
	movhps	-760(%rbp), %xmm0
	movl	$2, %r9d
	movq	%rbx, %xmm2
	movl	$2, %r10d
	movl	$2, %r11d
	movq	%rax, -576(%rbp)
	movl	$2, %r14d
	movq	%rbx, %r15
	movups	%xmm0, 640(%r12)
	movq	.LC12(%rip), %xmm0
	movw	%si, -568(%rbp)
	movq	%rax, -512(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movw	%di, -504(%rbp)
	movq	%rax, -448(%rbp)
	movw	%r8w, -440(%rbp)
	movq	%rax, -384(%rbp)
	movw	%r9w, -376(%rbp)
	movq	%rax, -320(%rbp)
	movw	%r10w, -312(%rbp)
	movq	%rax, -256(%rbp)
	movw	%r11w, -248(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -128(%rbp)
	movw	%r14w, -120(%rbp)
	leaq	-64(%rbp), %r14
	movaps	%xmm0, -720(%rbp)
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%r15, %rdi
	addq	$64, %r15
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	cmpq	%r14, %r15
	jne	.L195
	movq	-744(%rbp), %rax
	movq	%r13, %rdx
	leaq	.LC9(%rip), %rdi
	movq	40(%rax), %rsi
	call	ures_open_67@PLT
	movl	0(%r13), %ecx
	movq	%rax, -760(%rbp)
	leaq	-720(%rbp), %rax
	movq	%rax, -752(%rbp)
	testl	%ecx, %ecx
	jle	.L228
.L227:
	movq	-760(%rbp), %rax
	testq	%rax, %rax
	je	.L207
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L207:
	movq	-752(%rbp), %rdi
	leaq	16+_ZTVN12_GLOBAL__N_115PluralTableSinkE(%rip), %rax
	movq	%rax, -720(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L229
	xorl	%r12d, %r12d
.L208:
	leaq	-128(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L211:
	movq	0(%r13), %rax
	movq	%r13, %r14
	movq	%r13, %rdi
	subq	$64, %r13
	call	*(%rax)
	cmpq	%rbx, %r14
	jne	.L211
.L192:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	-760(%rbp), %rdi
	movq	%rax, %rdx
	movq	%r13, %rcx
	leaq	.LC10(%rip), %rsi
	call	ures_getAllItemsWithFallback_67@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L227
	xorl	%r10d, %r10d
	leaq	-724(%rbp), %rax
	movq	%rbx, %r14
	movq	%rbx, -792(%rbp)
	movq	%rax, -776(%rbp)
	movl	%r10d, %ebx
	movq	%r12, -784(%rbp)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L232:
	movzwl	-696(%rbp), %eax
	sarl	$5, %esi
	testw	%ax, %ax
	js	.L201
.L233:
	movswl	%ax, %r9d
	movzwl	8(%r14), %eax
	sarl	$5, %r9d
	testw	%ax, %ax
	js	.L203
.L234:
	movswl	%ax, %edx
	sarl	$5, %edx
.L204:
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r14, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	pushq	$0
	pushq	%r12
	call	_ZN6icu_6713UnicodeString14findAndReplaceEiiRKS0_iiS2_ii@PLT
	movq	%r15, %rdi
	addq	$32, %rsp
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L198:
	addl	$1, %ebx
	addq	$64, %r14
	cmpl	$6, %ebx
	je	.L231
.L205:
	testb	$1, 8(%r14)
	jne	.L198
	movl	%ebx, %edi
	leaq	-640(%rbp), %r12
	movl	$0, -724(%rbp)
	leaq	-704(%rbp), %r15
	call	_ZN6icu_6714StandardPlural10getKeywordENS0_4FormE@PLT
	movq	-776(%rbp), %r8
	movq	%r13, %r9
	xorl	%edx, %edx
	movq	%rax, %rcx
	movq	-744(%rbp), %rax
	movq	40(%rax), %rsi
	movq	-768(%rbp), %rax
	leaq	20(%rax), %rdi
	call	ucurr_getPluralName_67@PLT
	movl	-724(%rbp), %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	leaq	.LC11(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movswl	-632(%rbp), %esi
	testw	%si, %si
	jns	.L232
	movzwl	-696(%rbp), %eax
	movl	-628(%rbp), %esi
	testw	%ax, %ax
	jns	.L233
.L201:
	movzwl	8(%r14), %eax
	movl	-692(%rbp), %r9d
	testw	%ax, %ax
	jns	.L234
.L203:
	movl	12(%r14), %edx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%r13, %rcx
	movl	$39, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15LongNameHandler24simpleFormatsToModifiersEPKNS_13UnicodeStringENS_22FormattedStringBuilder5FieldER10UErrorCode
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L231:
	movq	-784(%rbp), %r12
	movq	-792(%rbp), %rbx
	jmp	.L227
.L193:
	movl	$7, 0(%r13)
	jmp	.L192
.L230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3445:
	.size	_ZN6icu_676number4impl15LongNameHandler20forCurrencyLongNamesERKNS_6LocaleERKNS_12CurrencyUnitEPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode, .-_ZN6icu_676number4impl15LongNameHandler20forCurrencyLongNamesERKNS_6LocaleERKNS_12CurrencyUnitEPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15LongNameHandler29multiSimpleFormatsToModifiersEPKNS_13UnicodeStringES3_NS_22FormattedStringBuilder5FieldER10UErrorCode
	.type	_ZN6icu_676number4impl15LongNameHandler29multiSimpleFormatsToModifiersEPKNS_13UnicodeStringES3_NS_22FormattedStringBuilder5FieldER10UErrorCode, @function
_ZN6icu_676number4impl15LongNameHandler29multiSimpleFormatsToModifiersEPKNS_13UnicodeStringES3_NS_22FormattedStringBuilder5FieldER10UErrorCode:
.LFB3448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$488, %rsp
	movq	%rdi, -512(%rbp)
	movq	%rsi, -480(%rbp)
	movq	%rdx, %rsi
	movl	$1, %edx
	movb	%cl, -513(%rbp)
	movl	$1, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r9w, -304(%rbp)
	movq	%rax, -312(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -504(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movl	(%r12), %r10d
	testl	%r10d, %r10d
	jg	.L236
	movq	$0, -472(%rbp)
	addq	$24, %rbx
	leaq	-448(%rbp), %r14
.L241:
	movq	-472(%rbp), %rsi
	movq	%r14, %rdi
	movl	%esi, %r13d
	salq	$6, %rsi
	addq	-480(%rbp), %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	testb	$1, -440(%rbp)
	jne	.L255
.L237:
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L238
	movl	$2, %ecx
	movq	-504(%rbp), %rdi
	movq	%r14, %rsi
	leaq	-384(%rbp), %r15
	movw	%cx, -376(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%rax, -384(%rbp)
	call	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringERS1_R10UErrorCode@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L253
	leaq	-240(%rbp), %r10
	xorl	%edx, %edx
	movq	%r12, %r8
	movq	%r15, %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r10, %rdi
	movl	$1, %ecx
	movq	%r10, -488(%rbp)
	movq	%rax, -232(%rbp)
	movl	$2, %eax
	movw	%ax, -224(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movl	(%r12), %edx
	movq	-488(%rbp), %r10
	testl	%edx, %edx
	jg	.L256
	movq	-512(%rbp), %rax
	movl	%r13d, %ecx
	leaq	-464(%rbp), %rdi
	movl	$2, %edx
	movq	%r10, -488(%rbp)
	leaq	-160(%rbp), %r13
	leaq	8(%rax), %rsi
	call	_ZN6icu_676number4impl8Modifier10ParametersC1EPKNS1_13ModifierStoreENS1_6SignumENS_14StandardPlural4FormE@PLT
	movq	-488(%rbp), %r10
	xorl	%ecx, %ecx
	movq	-456(%rbp), %r9
	movzbl	-513(%rbp), %edx
	movq	-464(%rbp), %r8
	movq	%r13, %rdi
	movq	%r10, %rsi
	movq	%r10, -496(%rbp)
	call	_ZN6icu_676number4impl14SimpleModifierC1ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEbNS1_8Modifier10ParametersE@PLT
	leaq	-152(%rbp), %r8
	movq	%rbx, %rdi
	addq	$104, %rbx
	movq	%r8, %rsi
	movq	%r8, -488(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movzbl	-88(%rbp), %eax
	movdqu	-72(%rbp), %xmm0
	movq	-488(%rbp), %r8
	movb	%al, -40(%rbx)
	movzbl	-87(%rbp), %eax
	movq	%r8, %rdi
	movups	%xmm0, -24(%rbx)
	movb	%al, -39(%rbx)
	movl	-84(%rbp), %eax
	movl	%eax, -36(%rbx)
	movl	-80(%rbp), %eax
	movl	%eax, -32(%rbx)
	movl	-76(%rbp), %eax
	movl	%eax, -28(%rbx)
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	-496(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$1, -472(%rbp)
	movq	-472(%rbp), %rax
	cmpq	$6, %rax
	jne	.L241
.L236:
	movq	-504(%rbp), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L257
	addq	$488, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	-480(%rbp), %rax
	movq	%r14, %rdi
	leaq	320(%rax), %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	testb	$1, -440(%rbp)
	je	.L237
	movl	$5, (%r12)
.L238:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L253:
	movq	%r15, %r13
.L239:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%r10, %rdi
	movq	%r15, %r13
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	jmp	.L239
.L257:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3448:
	.size	_ZN6icu_676number4impl15LongNameHandler29multiSimpleFormatsToModifiersEPKNS_13UnicodeStringES3_NS_22FormattedStringBuilder5FieldER10UErrorCode, .-_ZN6icu_676number4impl15LongNameHandler29multiSimpleFormatsToModifiersEPKNS_13UnicodeStringES3_NS_22FormattedStringBuilder5FieldER10UErrorCode
	.section	.rodata.str1.1
.LC13:
	.string	"/compound/per"
	.section	.rodata.str2.2
	.align 2
.LC14:
	.string	"{"
	.string	"0"
	.string	"}"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15LongNameHandler15forCompoundUnitERKNS_6LocaleERKNS_11MeasureUnitES8_RK16UNumberUnitWidthPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode
	.type	_ZN6icu_676number4impl15LongNameHandler15forCompoundUnitERKNS_6LocaleERKNS_11MeasureUnitES8_RK16UNumberUnitWidthPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode, @function
_ZN6icu_676number4impl15LongNameHandler15forCompoundUnitERKNS_6LocaleERKNS_11MeasureUnitES8_RK16UNumberUnitWidthPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode:
.LFB3442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$656, %edi
	pushq	%rbx
	subq	$1592, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -1600(%rbp)
	movq	16(%rbp), %r15
	movq	%rdx, -1624(%rbp)
	movq	%r8, -1608(%rbp)
	movq	%r9, -1616(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -1592(%rbp)
	testq	%rax, %rax
	je	.L259
	movq	%rax, %rcx
	leaq	64+_ZTVN6icu_676number4impl15LongNameHandlerE(%rip), %rax
	leaq	-48(%rax), %rdx
	movq	%rax, %xmm1
	leaq	16(%rcx), %r14
	movq	%rdx, %xmm0
	leaq	640(%rcx), %rbx
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rcx)
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%r14, %rdi
	addq	$104, %r14
	call	_ZN6icu_676number4impl14SimpleModifierC1Ev@PLT
	cmpq	%rbx, %r14
	jne	.L260
	movq	-1592(%rbp), %rax
	movl	$2, %ebx
	movq	%r15, %r8
	movq	-1608(%rbp), %xmm0
	movw	%bx, -952(%rbp)
	movq	-1600(%rbp), %rsi
	movq	%r13, %rdx
	leaq	-1088(%rbp), %rbx
	movhps	-1616(%rbp), %xmm0
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r9
	movq	%rbx, %rcx
	movl	$2, %r10d
	movups	%xmm0, 640(%rax)
	movl	$2, %eax
	movl	$2, %r11d
	movq	%r12, %rdi
	movw	%ax, -824(%rbp)
	movl	$2, %eax
	movl	$2, %r14d
	movw	%ax, -760(%rbp)
	movl	$2, %eax
	movw	%ax, -696(%rbp)
	movl	$2, %eax
	movw	%ax, -632(%rbp)
	movq	%r9, -1088(%rbp)
	movw	%r10w, -1080(%rbp)
	movq	%r9, -1024(%rbp)
	movw	%r11w, -1016(%rbp)
	movq	%r9, -960(%rbp)
	movq	%r9, -896(%rbp)
	movw	%r14w, -888(%rbp)
	movq	%r9, -832(%rbp)
	movq	%r9, -768(%rbp)
	movq	%r9, -704(%rbp)
	movq	%r9, -640(%rbp)
	call	_ZN12_GLOBAL__N_114getMeasureDataERKN6icu_676LocaleERKNS0_11MeasureUnitERK16UNumberUnitWidthPNS0_13UnicodeStringER10UErrorCode
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L261
	movl	$2, %eax
	movl	$2, %esi
	movl	$2, %edx
	movl	$2, %ecx
	movw	%ax, -568(%rbp)
	movl	$2, %edi
	movl	$2, %eax
	movl	$2, %r8d
	movw	%si, -248(%rbp)
	leaq	-576(%rbp), %r14
	movq	-1624(%rbp), %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r9
	movw	%ax, -504(%rbp)
	movl	$2, %eax
	movw	%dx, -376(%rbp)
	movq	%r13, %rdx
	movw	%cx, -312(%rbp)
	movq	%r14, %rcx
	movw	%di, -184(%rbp)
	movq	%r12, %rdi
	movw	%r8w, -120(%rbp)
	movq	%r15, %r8
	movq	%r9, -576(%rbp)
	movq	%r9, -512(%rbp)
	movq	%r9, -448(%rbp)
	movq	%r9, -384(%rbp)
	movq	%r9, -320(%rbp)
	movq	%r9, -256(%rbp)
	movq	%r9, -192(%rbp)
	movq	%r9, -128(%rbp)
	movw	%ax, -440(%rbp)
	call	_ZN12_GLOBAL__N_114getMeasureDataERKN6icu_676LocaleERKNS0_11MeasureUnitERK16UNumberUnitWidthPNS0_13UnicodeStringER10UErrorCode
	movl	(%r15), %r9d
	leaq	-128(%rbp), %r10
	testl	%r9d, %r9d
	jg	.L262
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r9
	movl	$2, %eax
	movq	%r9, -1568(%rbp)
	movw	%ax, -1560(%rbp)
	testb	$1, -120(%rbp)
	je	.L306
	movq	40(%r12), %rsi
	movq	%r15, %rdx
	leaq	.LC2(%rip), %rdi
	call	ures_open_67@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r9
	movq	%rax, %r8
	movl	(%r15), %eax
	testl	%eax, %eax
	jle	.L265
	movl	$2, %r12d
	leaq	-1504(%rbp), %rax
	movq	%r9, -1504(%rbp)
	movw	%r12w, -1496(%rbp)
	movq	%rax, -1600(%rbp)
.L266:
	testq	%r8, %r8
	je	.L270
	movq	%r8, %rdi
	call	ures_close_67@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r9
.L270:
	movl	(%r15), %r10d
	leaq	-1568(%rbp), %r13
	testl	%r10d, %r10d
	jle	.L307
.L271:
	movq	-1600(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-128(%rbp), %r10
.L280:
	movq	%r13, %rdi
	movq	%r10, -1600(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-1600(%rbp), %r10
.L262:
	movq	%r10, %r15
	.p2align 4,,10
	.p2align 3
.L281:
	movq	(%r15), %rax
	movq	%r15, %r12
	movq	%r15, %rdi
	subq	$64, %r15
	call	*(%rax)
	cmpq	%r12, %r14
	jne	.L281
.L261:
	leaq	-640(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L282:
	movq	(%r12), %rax
	movq	%r12, %r13
	movq	%r12, %rdi
	subq	$64, %r12
	call	*(%rax)
	cmpq	%rbx, %r13
	jne	.L282
.L258:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L308
	movq	-1592(%rbp), %rax
	addq	$1592, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	leaq	-1568(%rbp), %r13
	movq	%r10, %rsi
	movq	%r10, -1600(%rbp)
	leaq	-1168(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-1600(%rbp), %r10
.L264:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, -1600(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r15, %r8
	movl	$43, %ecx
	movq	%r12, %rdx
	movq	-1592(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_676number4impl15LongNameHandler29multiSimpleFormatsToModifiersEPKNS_13UnicodeStringES3_NS_22FormattedStringBuilder5FieldER10UErrorCode
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-1600(%rbp), %r10
	jmp	.L280
.L307:
	leaq	-1248(%rbp), %rax
	movl	$2, %edi
	movq	%r15, %r8
	movl	$2, %ecx
	movq	-1600(%rbp), %rsi
	movw	%di, -1232(%rbp)
	movl	$2, %edx
	movq	%rax, %rdi
	movq	%r9, -1240(%rbp)
	movq	%rax, -1608(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movl	(%r15), %r8d
	testl	%r8d, %r8d
	jle	.L309
.L272:
	movq	-1608(%rbp), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	jmp	.L271
.L309:
	leaq	-1440(%rbp), %rax
	leaq	-512(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -1616(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	testb	$1, -1432(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r9
	jne	.L310
.L273:
	movl	(%r15), %esi
	leaq	-1568(%rbp), %r13
	testl	%esi, %esi
	jg	.L274
	movl	$2, %edx
	movq	-1616(%rbp), %rsi
	movl	$1, %ecx
	movq	%r15, %r8
	leaq	-1168(%rbp), %r12
	movw	%dx, -1152(%rbp)
	xorl	%edx, %edx
	leaq	-1568(%rbp), %r13
	movq	%r12, %rdi
	movq	%r9, -1160(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jle	.L311
.L275:
	movq	%r12, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
.L274:
	movq	-1616(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L272
.L265:
	leaq	-1584(%rbp), %r10
	xorl	%r11d, %r11d
	leaq	-1155(%rbp), %rax
	movq	%r8, -1608(%rbp)
	movq	%r10, %rdi
	leaq	.LC4(%rip), %rsi
	movq	%r10, -1600(%rbp)
	leaq	-1168(%rbp), %r12
	movq	%rax, -1168(%rbp)
	movw	%r11w, -1156(%rbp)
	movl	$0, -1112(%rbp)
	movl	$40, -1160(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-1576(%rbp), %edx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-1584(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	0(%r13), %eax
	movq	-1600(%rbp), %r10
	movq	-1608(%rbp), %r8
	testl	%eax, %eax
	je	.L312
	cmpl	$1, %eax
	je	.L313
.L268:
	movq	%r10, %rdi
	leaq	.LC13(%rip), %rsi
	movq	%r8, -1608(%rbp)
	movq	%r10, -1600(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-1576(%rbp), %edx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-1584(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-1608(%rbp), %r8
	movq	-1600(%rbp), %r10
	movq	%r15, %rcx
	movq	-1168(%rbp), %rsi
	movl	$0, -1584(%rbp)
	movq	%r8, %rdi
	movq	%r10, %rdx
	call	ures_getStringByKeyWithFallback_67@PLT
	movl	-1584(%rbp), %edx
	movq	%rax, %rsi
	leaq	-1504(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1600(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	cmpb	$0, -1156(%rbp)
	movq	-1608(%rbp), %r8
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r9
	je	.L266
	movq	-1168(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-1608(%rbp), %r8
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r9
	jmp	.L266
.L310:
	movq	-1616(%rbp), %rdi
	leaq	-256(%rbp), %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	testb	$1, -1432(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r9
	je	.L273
	movl	$5, (%r15)
	jmp	.L274
.L312:
	movq	%r10, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-1576(%rbp), %edx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-1584(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-1600(%rbp), %r10
	movq	-1608(%rbp), %r8
	jmp	.L268
.L313:
	movq	%r10, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%r8, -1608(%rbp)
	movq	%r10, -1600(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-1576(%rbp), %edx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-1584(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-1608(%rbp), %r8
	movq	-1600(%rbp), %r10
	jmp	.L268
.L311:
	movzwl	-1152(%rbp), %eax
	testw	%ax, %ax
	js	.L276
	movswl	%ax, %edx
	sarl	$5, %edx
.L277:
	testb	$17, %al
	jne	.L285
	leaq	-1150(%rbp), %rsi
	testb	$2, %al
	cmove	-1136(%rbp), %rsi
.L278:
	leaq	-1312(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r9, %rdi
	movq	%r9, -1624(%rbp)
	leaq	-1568(%rbp), %r13
	call	_ZN6icu_6715SimpleFormatter22getTextWithNoArgumentsEPKDsiPii@PLT
	movq	-1624(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeString4trimEv@PLT
	leaq	-1376(%rbp), %r10
	movq	%rax, %rsi
	movq	%r10, %rdi
	movq	%r10, -1632(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-1624(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-1624(%rbp), %r9
	leaq	.LC14(%rip), %rsi
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-1632(%rbp), %r10
	movq	%r15, %r8
	movq	%r13, %rcx
	movq	-1624(%rbp), %r9
	movq	-1608(%rbp), %rdi
	movq	%r10, %rdx
	movq	%r9, %rsi
	call	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_RS1_R10UErrorCode@PLT
	movq	-1624(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r15), %eax
	movq	-1632(%rbp), %r10
	testl	%eax, %eax
	movq	%r10, %rdi
	jg	.L314
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	-1616(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-1608(%rbp), %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	-1600(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-128(%rbp), %r10
	jmp	.L264
.L276:
	movl	-1148(%rbp), %edx
	jmp	.L277
.L314:
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L275
.L285:
	xorl	%esi, %esi
	jmp	.L278
.L259:
	movl	$7, (%r15)
	jmp	.L258
.L308:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3442:
	.size	_ZN6icu_676number4impl15LongNameHandler15forCompoundUnitERKNS_6LocaleERKNS_11MeasureUnitES8_RK16UNumberUnitWidthPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode, .-_ZN6icu_676number4impl15LongNameHandler15forCompoundUnitERKNS_6LocaleERKNS_11MeasureUnitES8_RK16UNumberUnitWidthPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode
	.section	.rodata.str1.1
.LC15:
	.string	"none"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl15LongNameHandler14forMeasureUnitERKNS_6LocaleERKNS_11MeasureUnitES8_RK16UNumberUnitWidthPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode
	.type	_ZN6icu_676number4impl15LongNameHandler14forMeasureUnitERKNS_6LocaleERKNS_11MeasureUnitES8_RK16UNumberUnitWidthPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode, @function
_ZN6icu_676number4impl15LongNameHandler14forMeasureUnitERKNS_6LocaleERKNS_11MeasureUnitES8_RK16UNumberUnitWidthPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode:
.LFB3441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$648, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -664(%rbp)
	movq	%rsi, %rdi
	movq	16(%rbp), %r14
	movq	%rcx, -672(%rbp)
	movq	%r8, -680(%rbp)
	movq	%r9, -688(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	cmpb	$0, (%rax)
	je	.L318
	movq	%r12, %rdi
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	cmpb	$0, (%rax)
	je	.L318
	leaq	-640(%rbp), %r15
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	movl	$5, %ecx
	leaq	.LC15(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L319
	leaq	-608(%rbp), %r13
	leaq	-641(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movb	$0, -641(%rbp)
	call	_ZN6icu_6711MeasureUnit18resolveUnitPerUnitERKS0_S2_Pb@PLT
	cmpb	$0, -641(%rbp)
	je	.L320
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
.L319:
	movl	$656, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L322
	leaq	64+_ZTVN6icu_676number4impl15LongNameHandlerE(%rip), %rax
	leaq	16(%r12), %r13
	leaq	-48(%rax), %rdx
	movq	%rax, %xmm1
	leaq	640(%r12), %rbx
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%r13, %rdi
	addq	$104, %r13
	call	_ZN6icu_676number4impl14SimpleModifierC1Ev@PLT
	cmpq	%r13, %rbx
	jne	.L323
	movl	$2, %edx
	movl	$2, %edi
	movl	$2, %ecx
	movq	-680(%rbp), %xmm0
	movw	%dx, -568(%rbp)
	movl	$2, %esi
	movl	$2, %r8d
	movq	-672(%rbp), %rdx
	movw	%di, -376(%rbp)
	movhps	-688(%rbp), %xmm0
	movq	-664(%rbp), %rdi
	leaq	-576(%rbp), %rbx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, -504(%rbp)
	movl	$2, %r9d
	movl	$2, %r10d
	movw	%si, -440(%rbp)
	movq	%rbx, %rcx
	movq	%r15, %rsi
	movl	$2, %r11d
	movw	%r8w, -312(%rbp)
	movq	%r14, %r8
	movups	%xmm0, 640(%r12)
	movq	%rax, -576(%rbp)
	movq	%rax, -512(%rbp)
	movq	%rax, -448(%rbp)
	movq	%rax, -384(%rbp)
	movq	%rax, -320(%rbp)
	movq	%rax, -256(%rbp)
	movw	%r9w, -248(%rbp)
	movq	%rax, -192(%rbp)
	movw	%r10w, -184(%rbp)
	movq	%rax, -128(%rbp)
	movw	%r11w, -120(%rbp)
	call	_ZN12_GLOBAL__N_114getMeasureDataERKN6icu_676LocaleERKNS0_11MeasureUnitERK16UNumberUnitWidthPNS0_13UnicodeStringER10UErrorCode
	movl	(%r14), %r13d
	testl	%r13d, %r13d
	jg	.L324
	movq	%r14, %rcx
	movl	$43, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15LongNameHandler24simpleFormatsToModifiersEPKNS_13UnicodeStringENS_22FormattedStringBuilder5FieldER10UErrorCode
.L324:
	leaq	-128(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L325:
	movq	0(%r13), %rax
	movq	%r13, %r14
	movq	%r13, %rdi
	subq	$64, %r13
	call	*(%rax)
	cmpq	%r14, %rbx
	jne	.L325
.L321:
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L318:
	movl	$16, (%r14)
	xorl	%r12d, %r12d
.L315:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L333
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	subq	$8, %rsp
	movq	-688(%rbp), %r9
	movq	-680(%rbp), %r8
	movq	%r12, %rdx
	pushq	%r14
	movq	-672(%rbp), %rcx
	movq	%r15, %rsi
	movq	-664(%rbp), %rdi
	call	_ZN6icu_676number4impl15LongNameHandler15forCompoundUnitERKNS_6LocaleERKNS_11MeasureUnitES8_RK16UNumberUnitWidthPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	popq	%r14
	movq	%r15, %rdi
	popq	%rax
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	jmp	.L315
.L333:
	call	__stack_chk_fail@PLT
.L322:
	movl	$7, (%r14)
	jmp	.L321
	.cfi_endproc
.LFE3441:
	.size	_ZN6icu_676number4impl15LongNameHandler14forMeasureUnitERKNS_6LocaleERKNS_11MeasureUnitES8_RK16UNumberUnitWidthPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode, .-_ZN6icu_676number4impl15LongNameHandler14forMeasureUnitERKNS_6LocaleERKNS_11MeasureUnitES8_RK16UNumberUnitWidthPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl15LongNameHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.type	_ZNK6icu_676number4impl15LongNameHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode, @function
_ZNK6icu_676number4impl15LongNameHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode:
.LFB3449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	648(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	640(%rbx), %r8
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r8, -216(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1ERKS2_@PLT
	leaq	8(%r12), %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L338
	movq	-216(%rbp), %r8
	testq	%r8, %r8
	je	.L338
	leaq	-128(%rbp), %r14
	movq	%r15, %rdx
	movq	%r8, %rsi
	movl	$536, %r13d
	movq	%r14, %rdi
	call	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringERKNS_13UnicodeStringE@PLT
	testl	%eax, %eax
	js	.L336
	cltq
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rax,%rdx,4), %rax
	leaq	16(,%rax,8), %r13
.L336:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L338:
	movl	$536, %r13d
.L335:
	addq	%r13, %rbx
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	%rbx, 112(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L341
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L341:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3449:
	.size	_ZNK6icu_676number4impl15LongNameHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode, .-_ZNK6icu_676number4impl15LongNameHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_676number4impl15LongNameHandlerE
	.section	.rodata._ZTSN6icu_676number4impl15LongNameHandlerE,"aG",@progbits,_ZTSN6icu_676number4impl15LongNameHandlerE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl15LongNameHandlerE, @object
	.size	_ZTSN6icu_676number4impl15LongNameHandlerE, 39
_ZTSN6icu_676number4impl15LongNameHandlerE:
	.string	"N6icu_676number4impl15LongNameHandlerE"
	.weak	_ZTIN6icu_676number4impl15LongNameHandlerE
	.section	.data.rel.ro._ZTIN6icu_676number4impl15LongNameHandlerE,"awG",@progbits,_ZTIN6icu_676number4impl15LongNameHandlerE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl15LongNameHandlerE, @object
	.size	_ZTIN6icu_676number4impl15LongNameHandlerE, 72
_ZTIN6icu_676number4impl15LongNameHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl15LongNameHandlerE
	.long	0
	.long	3
	.quad	_ZTIN6icu_676number4impl19MicroPropsGeneratorE
	.quad	2
	.quad	_ZTIN6icu_676number4impl13ModifierStoreE
	.quad	2050
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTIN12_GLOBAL__N_115PluralTableSinkE, @object
	.size	_ZTIN12_GLOBAL__N_115PluralTableSinkE, 24
_ZTIN12_GLOBAL__N_115PluralTableSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN12_GLOBAL__N_115PluralTableSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.section	.rodata
	.align 32
	.type	_ZTSN12_GLOBAL__N_115PluralTableSinkE, @object
	.size	_ZTSN12_GLOBAL__N_115PluralTableSinkE, 35
_ZTSN12_GLOBAL__N_115PluralTableSinkE:
	.string	"*N12_GLOBAL__N_115PluralTableSinkE"
	.section	.data.rel.ro
	.align 8
	.type	_ZTVN12_GLOBAL__N_115PluralTableSinkE, @object
	.size	_ZTVN12_GLOBAL__N_115PluralTableSinkE, 48
_ZTVN12_GLOBAL__N_115PluralTableSinkE:
	.quad	0
	.quad	_ZTIN12_GLOBAL__N_115PluralTableSinkE
	.quad	_ZN12_GLOBAL__N_115PluralTableSinkD1Ev
	.quad	_ZN12_GLOBAL__N_115PluralTableSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN12_GLOBAL__N_115PluralTableSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode
	.weak	_ZTVN6icu_676number4impl15LongNameHandlerE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl15LongNameHandlerE,"awG",@progbits,_ZTVN6icu_676number4impl15LongNameHandlerE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl15LongNameHandlerE, @object
	.size	_ZTVN6icu_676number4impl15LongNameHandlerE, 88
_ZTVN6icu_676number4impl15LongNameHandlerE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl15LongNameHandlerE
	.quad	_ZN6icu_676number4impl15LongNameHandlerD1Ev
	.quad	_ZN6icu_676number4impl15LongNameHandlerD0Ev
	.quad	_ZNK6icu_676number4impl15LongNameHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.quad	_ZNK6icu_676number4impl15LongNameHandler11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.quad	-8
	.quad	_ZTIN6icu_676number4impl15LongNameHandlerE
	.quad	_ZThn8_N6icu_676number4impl15LongNameHandlerD1Ev
	.quad	_ZThn8_N6icu_676number4impl15LongNameHandlerD0Ev
	.quad	_ZThn8_NK6icu_676number4impl15LongNameHandler11getModifierENS1_6SignumENS_14StandardPlural4FormE
	.section	.data.rel.ro.local,"aw"
	.align 8
.LC12:
	.quad	_ZTVN12_GLOBAL__N_115PluralTableSinkE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
