	.file	"curramt.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714CurrencyAmount17getDynamicClassIDEv
	.type	_ZNK6icu_6714CurrencyAmount17getDynamicClassIDEv, @function
_ZNK6icu_6714CurrencyAmount17getDynamicClassIDEv:
.LFB2255:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714CurrencyAmount16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2255:
	.size	_ZNK6icu_6714CurrencyAmount17getDynamicClassIDEv, .-_ZNK6icu_6714CurrencyAmount17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714CurrencyAmount5cloneEv
	.type	_ZNK6icu_6714CurrencyAmount5cloneEv, @function
_ZNK6icu_6714CurrencyAmount5cloneEv:
.LFB2249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$128, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_677MeasureC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714CurrencyAmountE(%rip), %rax
	movq	%rax, (%r12)
.L3:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2249:
	.size	_ZNK6icu_6714CurrencyAmount5cloneEv, .-_ZNK6icu_6714CurrencyAmount5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CurrencyAmountD2Ev
	.type	_ZN6icu_6714CurrencyAmountD2Ev, @function
_ZN6icu_6714CurrencyAmountD2Ev:
.LFB2251:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714CurrencyAmountE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677MeasureD2Ev@PLT
	.cfi_endproc
.LFE2251:
	.size	_ZN6icu_6714CurrencyAmountD2Ev, .-_ZN6icu_6714CurrencyAmountD2Ev
	.globl	_ZN6icu_6714CurrencyAmountD1Ev
	.set	_ZN6icu_6714CurrencyAmountD1Ev,_ZN6icu_6714CurrencyAmountD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CurrencyAmountD0Ev
	.type	_ZN6icu_6714CurrencyAmountD0Ev, @function
_ZN6icu_6714CurrencyAmountD0Ev:
.LFB2253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714CurrencyAmountE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677MeasureD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2253:
	.size	_ZN6icu_6714CurrencyAmountD0Ev, .-_ZN6icu_6714CurrencyAmountD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CurrencyAmountC2ERKNS_11FormattableENS_14ConstChar16PtrER10UErrorCode
	.type	_ZN6icu_6714CurrencyAmountC2ERKNS_11FormattableENS_14ConstChar16PtrER10UErrorCode, @function
_ZN6icu_6714CurrencyAmountC2ERKNS_11FormattableENS_14ConstChar16PtrER10UErrorCode:
.LFB2240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$32, %edi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rax, -48(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L13
	leaq	-48(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6712CurrencyUnitC1ENS_14ConstChar16PtrER10UErrorCode@PLT
.L13:
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_677MeasureC2ERKNS_11FormattableEPNS_11MeasureUnitER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6714CurrencyAmountE(%rip), %rax
	movq	%rax, (%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L19
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2240:
	.size	_ZN6icu_6714CurrencyAmountC2ERKNS_11FormattableENS_14ConstChar16PtrER10UErrorCode, .-_ZN6icu_6714CurrencyAmountC2ERKNS_11FormattableENS_14ConstChar16PtrER10UErrorCode
	.globl	_ZN6icu_6714CurrencyAmountC1ERKNS_11FormattableENS_14ConstChar16PtrER10UErrorCode
	.set	_ZN6icu_6714CurrencyAmountC1ERKNS_11FormattableENS_14ConstChar16PtrER10UErrorCode,_ZN6icu_6714CurrencyAmountC2ERKNS_11FormattableENS_14ConstChar16PtrER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CurrencyAmountC2ERKS0_
	.type	_ZN6icu_6714CurrencyAmountC2ERKS0_, @function
_ZN6icu_6714CurrencyAmountC2ERKS0_:
.LFB2246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_677MeasureC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714CurrencyAmountE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2246:
	.size	_ZN6icu_6714CurrencyAmountC2ERKS0_, .-_ZN6icu_6714CurrencyAmountC2ERKS0_
	.globl	_ZN6icu_6714CurrencyAmountC1ERKS0_
	.set	_ZN6icu_6714CurrencyAmountC1ERKS0_,_ZN6icu_6714CurrencyAmountC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CurrencyAmountaSERKS0_
	.type	_ZN6icu_6714CurrencyAmountaSERKS0_, @function
_ZN6icu_6714CurrencyAmountaSERKS0_:
.LFB2248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_677MeasureaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2248:
	.size	_ZN6icu_6714CurrencyAmountaSERKS0_, .-_ZN6icu_6714CurrencyAmountaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CurrencyAmount16getStaticClassIDEv
	.type	_ZN6icu_6714CurrencyAmount16getStaticClassIDEv, @function
_ZN6icu_6714CurrencyAmount16getStaticClassIDEv:
.LFB2254:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714CurrencyAmount16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2254:
	.size	_ZN6icu_6714CurrencyAmount16getStaticClassIDEv, .-_ZN6icu_6714CurrencyAmount16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CurrencyAmountC2EdNS_14ConstChar16PtrER10UErrorCode
	.type	_ZN6icu_6714CurrencyAmountC2EdNS_14ConstChar16PtrER10UErrorCode, @function
_ZN6icu_6714CurrencyAmountC2EdNS_14ConstChar16PtrER10UErrorCode:
.LFB2243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$32, %edi
	subq	$160, %rsp
	movsd	%xmm0, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -168(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L26
	leaq	-168(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6712CurrencyUnitC1ENS_14ConstChar16PtrER10UErrorCode@PLT
.L26:
	movsd	-184(%rbp), %xmm0
	leaq	-160(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6711FormattableC1Ed@PLT
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_677MeasureC2ERKNS_11FormattableEPNS_11MeasureUnitER10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	leaq	16+_ZTVN6icu_6714CurrencyAmountE(%rip), %rax
	movq	%rax, (%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L32
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L32:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2243:
	.size	_ZN6icu_6714CurrencyAmountC2EdNS_14ConstChar16PtrER10UErrorCode, .-_ZN6icu_6714CurrencyAmountC2EdNS_14ConstChar16PtrER10UErrorCode
	.globl	_ZN6icu_6714CurrencyAmountC1EdNS_14ConstChar16PtrER10UErrorCode
	.set	_ZN6icu_6714CurrencyAmountC1EdNS_14ConstChar16PtrER10UErrorCode,_ZN6icu_6714CurrencyAmountC2EdNS_14ConstChar16PtrER10UErrorCode
	.weak	_ZTSN6icu_6714CurrencyAmountE
	.section	.rodata._ZTSN6icu_6714CurrencyAmountE,"aG",@progbits,_ZTSN6icu_6714CurrencyAmountE,comdat
	.align 16
	.type	_ZTSN6icu_6714CurrencyAmountE, @object
	.size	_ZTSN6icu_6714CurrencyAmountE, 26
_ZTSN6icu_6714CurrencyAmountE:
	.string	"N6icu_6714CurrencyAmountE"
	.weak	_ZTIN6icu_6714CurrencyAmountE
	.section	.data.rel.ro._ZTIN6icu_6714CurrencyAmountE,"awG",@progbits,_ZTIN6icu_6714CurrencyAmountE,comdat
	.align 8
	.type	_ZTIN6icu_6714CurrencyAmountE, @object
	.size	_ZTIN6icu_6714CurrencyAmountE, 24
_ZTIN6icu_6714CurrencyAmountE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714CurrencyAmountE
	.quad	_ZTIN6icu_677MeasureE
	.weak	_ZTVN6icu_6714CurrencyAmountE
	.section	.data.rel.ro.local._ZTVN6icu_6714CurrencyAmountE,"awG",@progbits,_ZTVN6icu_6714CurrencyAmountE,comdat
	.align 8
	.type	_ZTVN6icu_6714CurrencyAmountE, @object
	.size	_ZTVN6icu_6714CurrencyAmountE, 48
_ZTVN6icu_6714CurrencyAmountE:
	.quad	0
	.quad	_ZTIN6icu_6714CurrencyAmountE
	.quad	_ZN6icu_6714CurrencyAmountD1Ev
	.quad	_ZN6icu_6714CurrencyAmountD0Ev
	.quad	_ZNK6icu_6714CurrencyAmount17getDynamicClassIDEv
	.quad	_ZNK6icu_6714CurrencyAmount5cloneEv
	.local	_ZZN6icu_6714CurrencyAmount16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6714CurrencyAmount16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
