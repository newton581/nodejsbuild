	.file	"cecal.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710CECalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.type	_ZNK6icu_6710CECalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, @function
_ZNK6icu_6710CECalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE:
.LFB2319:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	leaq	_ZN6icu_67L6LIMITSE(%rip), %rax
	leaq	(%rdx,%rsi,4), %rdx
	movl	(%rax,%rdx,4), %eax
	ret
	.cfi_endproc
.LFE2319:
	.size	_ZNK6icu_6710CECalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, .-_ZNK6icu_6710CECalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710CECalendar18haveDefaultCenturyEv
	.type	_ZNK6icu_6710CECalendar18haveDefaultCenturyEv, @function
_ZNK6icu_6710CECalendar18haveDefaultCenturyEv:
.LFB2321:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2321:
	.size	_ZNK6icu_6710CECalendar18haveDefaultCenturyEv, .-_ZNK6icu_6710CECalendar18haveDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710CECalendar14inDaylightTimeER10UErrorCode
	.type	_ZNK6icu_6710CECalendar14inDaylightTimeER10UErrorCode, @function
_ZNK6icu_6710CECalendar14inDaylightTimeER10UErrorCode:
.LFB2320:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.L5
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK6icu_678Calendar11getTimeZoneEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*72(%rax)
	testb	%al, %al
	jne	.L16
	xorl	%eax, %eax
.L4:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode@PLT
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L4
	movl	76(%r12), %eax
	testl	%eax, %eax
	setne	%al
	jmp	.L4
	.cfi_endproc
.LFE2320:
	.size	_ZNK6icu_6710CECalendar14inDaylightTimeER10UErrorCode, .-_ZNK6icu_6710CECalendar14inDaylightTimeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710CECalendar23handleComputeMonthStartEiia
	.type	_ZNK6icu_6710CECalendar23handleComputeMonthStartEiia, @function
_ZNK6icu_6710CECalendar23handleComputeMonthStartEiia:
.LFB2318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*400(%rax)
	testl	%ebx, %ebx
	js	.L18
	movslq	%ebx, %rdx
	movl	%ebx, %ecx
	imulq	$1321528399, %rdx, %rdx
	sarl	$31, %ecx
	sarq	$34, %rdx
	subl	%ecx, %edx
	leal	(%rdx,%rdx,2), %ecx
	leal	(%rdx,%r12), %edi
	leal	(%rdx,%rcx,4), %edx
	subl	%edx, %ebx
.L19:
	imull	$365, %edi, %r13d
	movl	$4, %esi
	imull	$30, %ebx, %ebx
	addl	%eax, %r13d
	call	_ZN6icu_679ClockMath11floorDivideEii@PLT
	addq	$8, %rsp
	addl	%eax, %r13d
	leal	-1(%r13,%rbx), %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	addl	$1, %ebx
	movslq	%ebx, %rdx
	movl	%ebx, %ecx
	imulq	$1321528399, %rdx, %rdx
	sarl	$31, %ecx
	sarq	$34, %rdx
	subl	%ecx, %edx
	leal	(%rdx,%rdx,2), %ecx
	leal	-1(%r12,%rdx), %edi
	leal	(%rdx,%rcx,4), %edx
	subl	%edx, %ebx
	addl	$12, %ebx
	jmp	.L19
	.cfi_endproc
.LFE2318:
	.size	_ZNK6icu_6710CECalendar23handleComputeMonthStartEiia, .-_ZNK6icu_6710CECalendar23handleComputeMonthStartEiia
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CECalendarC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6710CECalendarC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6710CECalendarC2ERKNS_6LocaleER10UErrorCode:
.LFB2308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r12, %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6710CECalendarE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE2308:
	.size	_ZN6icu_6710CECalendarC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6710CECalendarC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6710CECalendarC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6710CECalendarC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6710CECalendarC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CECalendarC2ERKS0_
	.type	_ZN6icu_6710CECalendarC2ERKS0_, @function
_ZN6icu_6710CECalendarC2ERKS0_:
.LFB2311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678CalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6710CECalendarE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2311:
	.size	_ZN6icu_6710CECalendarC2ERKS0_, .-_ZN6icu_6710CECalendarC2ERKS0_
	.globl	_ZN6icu_6710CECalendarC1ERKS0_
	.set	_ZN6icu_6710CECalendarC1ERKS0_,_ZN6icu_6710CECalendarC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CECalendarD2Ev
	.type	_ZN6icu_6710CECalendarD2Ev, @function
_ZN6icu_6710CECalendarD2Ev:
.LFB2314:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6710CECalendarE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678CalendarD2Ev@PLT
	.cfi_endproc
.LFE2314:
	.size	_ZN6icu_6710CECalendarD2Ev, .-_ZN6icu_6710CECalendarD2Ev
	.globl	_ZN6icu_6710CECalendarD1Ev
	.set	_ZN6icu_6710CECalendarD1Ev,_ZN6icu_6710CECalendarD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CECalendarD0Ev
	.type	_ZN6icu_6710CECalendarD0Ev, @function
_ZN6icu_6710CECalendarD0Ev:
.LFB2316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6710CECalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678CalendarD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2316:
	.size	_ZN6icu_6710CECalendarD0Ev, .-_ZN6icu_6710CECalendarD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CECalendaraSERKS0_
	.type	_ZN6icu_6710CECalendaraSERKS0_, @function
_ZN6icu_6710CECalendaraSERKS0_:
.LFB2317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_678CalendaraSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2317:
	.size	_ZN6icu_6710CECalendaraSERKS0_, .-_ZN6icu_6710CECalendaraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CECalendar6ceToJDEiiii
	.type	_ZN6icu_6710CECalendar6ceToJDEiiii, @function
_ZN6icu_6710CECalendar6ceToJDEiiii:
.LFB2322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testl	%esi, %esi
	js	.L31
	movslq	%esi, %rdx
	movl	%esi, %eax
	movl	%esi, %ebx
	imulq	$1321528399, %rdx, %rdx
	sarl	$31, %eax
	sarq	$34, %rdx
	subl	%eax, %edx
	leal	(%rdx,%rdx,2), %eax
	addl	%edx, %edi
	leal	(%rdx,%rax,4), %eax
	subl	%eax, %ebx
.L32:
	imull	$365, %edi, %r12d
	movl	$4, %esi
	addl	%ecx, %r12d
	call	_ZN6icu_679ClockMath11floorDivideEii@PLT
	imull	$30, %ebx, %esi
	addq	$8, %rsp
	addl	%eax, %r12d
	popq	%rbx
	addl	%r12d, %esi
	popq	%r12
	leal	-1(%r13,%rsi), %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	addl	$1, %esi
	movslq	%esi, %rdx
	movl	%esi, %eax
	imulq	$1321528399, %rdx, %rdx
	sarl	$31, %eax
	sarq	$34, %rdx
	subl	%eax, %edx
	leal	(%rdx,%rdx,2), %eax
	leal	-1(%rdi,%rdx), %edi
	leal	(%rdx,%rax,4), %eax
	subl	%eax, %esi
	leal	12(%rsi), %ebx
	jmp	.L32
	.cfi_endproc
.LFE2322:
	.size	_ZN6icu_6710CECalendar6ceToJDEiiii, .-_ZN6icu_6710CECalendar6ceToJDEiiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CECalendar6jdToCEEiiRiS1_S1_
	.type	_ZN6icu_6710CECalendar6jdToCEEiiRiS1_S1_, @function
_ZN6icu_6710CECalendar6jdToCEEiiRiS1_S1_:
.LFB2323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r9d
	pxor	%xmm0, %xmm0
	subl	%r9d, %edi
	cvtsi2sdl	%edi, %xmm0
	movl	$1461, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	leaq	-44(%rbp), %rsi
	.cfi_offset 12, -32
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	movslq	-44(%rbp), %rsi
	movl	%eax, %r8d
	movq	%rsi, %rax
	imulq	$-1282606671, %rsi, %rsi
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrq	$32, %rsi
	addl	%eax, %esi
	movl	%esi, %edx
	sarl	$10, %esi
	sarl	$8, %edx
	movl	%edx, %edi
	subl	%esi, %edx
	leal	(%rdx,%r8,4), %edx
	subl	%ecx, %edi
	movl	%edx, 0(%r13)
	cmpl	$1460, %eax
	je	.L37
	imull	$365, %edi, %ecx
	subl	%ecx, %eax
	movslq	%eax, %rdx
	movl	%eax, %ecx
	imulq	$-2004318071, %rdx, %rdx
	sarl	$31, %ecx
	shrq	$32, %rdx
	addl	%eax, %edx
	sarl	$4, %edx
	subl	%ecx, %edx
	imull	$30, %edx, %ecx
	subl	%ecx, %eax
	addl	$1, %eax
.L35:
	movl	%edx, (%rbx)
	movl	%eax, (%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L39
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movl	$6, %eax
	movl	$12, %edx
	jmp	.L35
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2323:
	.size	_ZN6icu_6710CECalendar6jdToCEEiiRiS1_S1_, .-_ZN6icu_6710CECalendar6jdToCEEiiRiS1_S1_
	.weak	_ZTSN6icu_6710CECalendarE
	.section	.rodata._ZTSN6icu_6710CECalendarE,"aG",@progbits,_ZTSN6icu_6710CECalendarE,comdat
	.align 16
	.type	_ZTSN6icu_6710CECalendarE, @object
	.size	_ZTSN6icu_6710CECalendarE, 22
_ZTSN6icu_6710CECalendarE:
	.string	"N6icu_6710CECalendarE"
	.weak	_ZTIN6icu_6710CECalendarE
	.section	.data.rel.ro._ZTIN6icu_6710CECalendarE,"awG",@progbits,_ZTIN6icu_6710CECalendarE,comdat
	.align 8
	.type	_ZTIN6icu_6710CECalendarE, @object
	.size	_ZTIN6icu_6710CECalendarE, 24
_ZTIN6icu_6710CECalendarE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6710CECalendarE
	.quad	_ZTIN6icu_678CalendarE
	.weak	_ZTVN6icu_6710CECalendarE
	.section	.data.rel.ro._ZTVN6icu_6710CECalendarE,"awG",@progbits,_ZTVN6icu_6710CECalendarE,comdat
	.align 8
	.type	_ZTVN6icu_6710CECalendarE, @object
	.size	_ZTVN6icu_6710CECalendarE, 424
_ZTVN6icu_6710CECalendarE:
	.quad	0
	.quad	_ZTIN6icu_6710CECalendarE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CalendareqERKS0_
	.quad	_ZNK6icu_678Calendar14isEquivalentToERKS0_
	.quad	_ZN6icu_678Calendar3addENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar4rollENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6710CECalendar14inDaylightTimeER10UErrorCode
	.quad	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEv
	.quad	_ZN6icu_678Calendar11computeTimeER10UErrorCode
	.quad	_ZN6icu_678Calendar13computeFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode
	.quad	_ZNK6icu_6710CECalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.quad	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE
	.quad	_ZNK6icu_6710CECalendar23handleComputeMonthStartEiia
	.quad	_ZNK6icu_678Calendar20handleGetMonthLengthEii
	.quad	_ZNK6icu_678Calendar19handleGetYearLengthEi
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields
	.quad	_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii
	.quad	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar23getFieldResolutionTableEv
	.quad	_ZN6icu_678Calendar19handleComputeFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar21getDefaultMonthInYearEi
	.quad	_ZN6icu_678Calendar20getDefaultDayInMonthEii
	.quad	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6710CECalendar18haveDefaultCenturyEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode
	.quad	_ZN6icu_678Calendar14setRelatedYearEi
	.quad	__cxa_pure_virtual
	.section	.rodata
	.align 32
	.type	_ZN6icu_67L6LIMITSE, @object
	.size	_ZN6icu_67L6LIMITSE, 368
_ZN6icu_67L6LIMITSE:
	.long	0
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	5000000
	.long	5000000
	.long	0
	.long	0
	.long	12
	.long	12
	.long	1
	.long	1
	.long	52
	.long	53
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	1
	.long	1
	.long	5
	.long	30
	.long	1
	.long	1
	.long	365
	.long	366
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	1
	.long	5
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-5000000
	.long	-5000000
	.long	5000000
	.long	5000000
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-5000000
	.long	-5000000
	.long	5000000
	.long	5000000
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
