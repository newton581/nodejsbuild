	.file	"number_fluent.cpp"
	.text
	.section	.text._ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.type	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, @function
_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode:
.LFB3281:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3281:
	.size	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, .-_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.section	.text._ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv
	.type	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv, @function
_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv:
.LFB3282:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3282:
	.size	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv, .-_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv
	.section	.text._ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv
	.type	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv, @function
_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv:
.LFB3283:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3283:
	.size	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv, .-_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv
	.section	.text._ZNK6icu_676number4impl13EmptyModifier8isStrongEv,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier8isStrongEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv
	.type	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv, @function
_ZNK6icu_676number4impl13EmptyModifier8isStrongEv:
.LFB3284:
	.cfi_startproc
	endbr64
	movzbl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3284:
	.size	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv, .-_ZNK6icu_676number4impl13EmptyModifier8isStrongEv
	.section	.text._ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.type	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE, @function
_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE:
.LFB3285:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3285:
	.size	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE, .-_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.section	.text._ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE
	.type	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE, @function
_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE:
.LFB3286:
	.cfi_startproc
	endbr64
	movq	$0, (%rsi)
	ret
	.cfi_endproc
.LFE3286:
	.size	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE, .-_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE
	.section	.text._ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE
	.type	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE, @function
_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE:
.LFB3287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*32(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE3287:
	.size	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE, .-_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE
	.section	.text._ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode,"axG",@progbits,_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode
	.type	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode, @function
_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode:
.LFB3392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpq	%rdx, %rdi
	je	.L14
	movdqu	8(%rdi), %xmm0
	movq	%rdx, %r12
	leaq	200(%rdi), %rsi
	movups	%xmm0, 8(%rdx)
	movdqu	24(%rdi), %xmm1
	movups	%xmm1, 24(%rdx)
	movl	40(%rdi), %eax
	movl	%eax, 40(%rdx)
	movzbl	44(%rdi), %eax
	movb	%al, 44(%rdx)
	movq	48(%rdi), %rax
	movq	%rax, 48(%rdx)
	movl	56(%rdi), %eax
	movl	%eax, 56(%rdx)
	movq	60(%rdi), %rax
	movq	%rax, 60(%rdx)
	movl	68(%rdi), %eax
	movl	%eax, 68(%rdx)
	movq	72(%rdi), %rax
	movq	%rax, 72(%rdx)
	movzbl	80(%rdi), %eax
	movb	%al, 80(%rdx)
	movl	84(%rdi), %eax
	movl	%eax, 84(%rdx)
	movl	88(%rdi), %eax
	movl	%eax, 88(%rdx)
	movzbl	92(%rdi), %eax
	movb	%al, 92(%rdx)
	movzbl	93(%rdi), %eax
	movb	%al, 93(%rdx)
	movzbl	94(%rdi), %eax
	movb	%al, 94(%rdx)
	movzbl	95(%rdi), %eax
	movb	%al, 95(%rdx)
	movzbl	96(%rdi), %eax
	movb	%al, 96(%rdx)
	movzbl	97(%rdi), %eax
	movb	%al, 97(%rdx)
	movzbl	98(%rdi), %eax
	movb	%al, 98(%rdx)
	movzbl	99(%rdi), %eax
	movb	%al, 99(%rdx)
	movzbl	100(%rdi), %eax
	movb	%al, 100(%rdx)
	movzbl	101(%rdi), %eax
	movb	%al, 101(%rdx)
	movq	104(%rdi), %rax
	movq	%rax, 104(%rdx)
	movq	112(%rdi), %rax
	movq	%rax, 112(%rdx)
	movq	120(%rdi), %rax
	movq	%rax, 120(%rdx)
	movq	128(%rdi), %rax
	movq	%rax, 128(%rdx)
	movq	152(%rdi), %rax
	movl	144(%rdi), %edx
	movq	%rax, 152(%r12)
	movzbl	168(%rdi), %eax
	movl	%edx, 144(%r12)
	movb	%al, 168(%r12)
	movzbl	184(%rdi), %eax
	leaq	200(%r12), %rdi
	movb	%al, 184(%r12)
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	movq	224(%rbx), %rax
	movq	%rax, 224(%r12)
	movzbl	232(%rbx), %eax
	movb	%al, 232(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movb	$1, 232(%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3392:
	.size	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode, .-_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode
	.section	.text._ZN6icu_676number4impl13EmptyModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl13EmptyModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl13EmptyModifierD2Ev
	.type	_ZN6icu_676number4impl13EmptyModifierD2Ev, @function
_ZN6icu_676number4impl13EmptyModifierD2Ev:
.LFB5596:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	.cfi_endproc
.LFE5596:
	.size	_ZN6icu_676number4impl13EmptyModifierD2Ev, .-_ZN6icu_676number4impl13EmptyModifierD2Ev
	.weak	_ZN6icu_676number4impl13EmptyModifierD1Ev
	.set	_ZN6icu_676number4impl13EmptyModifierD1Ev,_ZN6icu_676number4impl13EmptyModifierD2Ev
	.section	.text._ZN6icu_676number4impl13EmptyModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl13EmptyModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl13EmptyModifierD0Ev
	.type	_ZN6icu_676number4impl13EmptyModifierD0Ev, @function
_ZN6icu_676number4impl13EmptyModifierD0Ev:
.LFB5598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE5598:
	.size	_ZN6icu_676number4impl13EmptyModifierD0Ev, .-_ZN6icu_676number4impl13EmptyModifierD0Ev
	.section	.text._ZN6icu_676number4impl24ImmutablePatternModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl24ImmutablePatternModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev
	.type	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev, @function
_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev:
.LFB4774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L19
	movq	(%rdi), %rax
	call	*8(%rax)
.L19:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4774:
	.size	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev, .-_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev
	.section	.text._ZN6icu_676number4impl10MicroPropsD2Ev,"axG",@progbits,_ZN6icu_676number4impl10MicroPropsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl10MicroPropsD2Ev
	.type	_ZN6icu_676number4impl10MicroPropsD2Ev, @function
_ZN6icu_676number4impl10MicroPropsD2Ev:
.LFB5615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	192(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	200(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	subq	$8, %rsp
	movq	%rax, -200(%rdi)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rax, -8(%rdi)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%rbx, 176(%r12)
	leaq	176(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 160(%r12)
	leaq	160(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	136(%r12), %rdi
	movq	%rax, 136(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.cfi_endproc
.LFE5615:
	.size	_ZN6icu_676number4impl10MicroPropsD2Ev, .-_ZN6icu_676number4impl10MicroPropsD2Ev
	.weak	_ZN6icu_676number4impl10MicroPropsD1Ev
	.set	_ZN6icu_676number4impl10MicroPropsD1Ev,_ZN6icu_676number4impl10MicroPropsD2Ev
	.section	.text._ZN6icu_676number4impl10MicroPropsD0Ev,"axG",@progbits,_ZN6icu_676number4impl10MicroPropsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl10MicroPropsD0Ev
	.type	_ZN6icu_676number4impl10MicroPropsD0Ev, @function
_ZN6icu_676number4impl10MicroPropsD0Ev:
.LFB5617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	192(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	200(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	subq	$8, %rsp
	movq	%rax, -200(%rdi)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rax, -8(%rdi)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%rbx, 176(%r12)
	leaq	176(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 160(%r12)
	leaq	160(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	136(%r12), %rdi
	movq	%rax, 136(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$240, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5617:
	.size	_ZN6icu_676number4impl10MicroPropsD0Ev, .-_ZN6icu_676number4impl10MicroPropsD0Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB4557:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4557:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB4560:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L41
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L29
	cmpb	$0, 12(%rbx)
	jne	.L42
.L33:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L29:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L33
	.cfi_endproc
.LFE4560:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB4563:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L45
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4563:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB4566:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L48
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE4566:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB4568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L54
.L50:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L55
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4568:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB4569:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4569:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB4570:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4570:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB4571:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4571:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4572:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4572:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4573:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4573:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB4574:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L71
	testl	%edx, %edx
	jle	.L71
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L74
.L63:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L63
	.cfi_endproc
.LFE4574:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB4575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L78
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L78
	testl	%r12d, %r12d
	jg	.L85
	cmpb	$0, 12(%rbx)
	jne	.L86
.L80:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L80
.L86:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L78:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4575:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB4576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L88
	movq	(%rdi), %r8
.L89:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L92
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L92
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L92:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4576:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB4577:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L99
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4577:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB4578:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4578:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB4579:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4579:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB4580:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4580:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB4582:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4582:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB4584:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4584:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8notationERKNS0_8NotationE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8notationERKNS0_8NotationE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8notationERKNS0_8NotationE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8notationERKNS0_8NotationE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8notationERKNS0_8NotationE:
.LFB4656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L106
	cmpl	$2, %eax
	jne	.L108
	cmpq	$0, 144(%rbx)
	je	.L111
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L112
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L108:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	0(%r13), %rax
	popq	%rbx
	movq	%rax, 4(%r12)
	movl	8(%r13), %eax
	movl	%eax, 12(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L120
.L111:
	movq	$0, 144(%r12)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L112
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L112:
	movq	%r14, 144(%r12)
	jmp	.L108
	.cfi_endproc
.LFE4656:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8notationERKNS0_8NotationE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8notationERKNS0_8NotationE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8notationERKNS0_8NotationE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8notationERKNS0_8NotationE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8notationERKNS0_8NotationE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8notationERKNS0_8NotationE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8notationERKNS0_8NotationE:
.LFB4657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L123
	cmpl	$2, %eax
	jne	.L124
.L123:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L124:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	0(%r13), %rax
	movq	%rax, 4(%r12)
	movl	8(%r13), %eax
	movl	%eax, 12(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4657:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8notationERKNS0_8NotationE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8notationERKNS0_8NotationE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE:
.LFB4658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, 4(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, 12(%rdi)
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L127
	cmpl	$2, %eax
	jne	.L129
	cmpq	$0, 144(%rbx)
	je	.L132
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L133
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r15, 144(%r12)
.L129:
	movq	152(%rbx), %rdx
	movl	160(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L141
.L132:
	movq	$0, 144(%r12)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L141:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L133
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L133:
	movq	%r15, 144(%r12)
	jmp	.L129
	.cfi_endproc
.LFE4658:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE:
.LFB4659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	16(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, 4(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, 12(%rdi)
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L144
	cmpl	$2, %eax
	jne	.L145
.L144:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L145:
	movq	152(%rbx), %rdx
	movl	160(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4659:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE:
.LFB4660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, 4(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, 12(%rdi)
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L148
	cmpl	$2, %eax
	jne	.L150
	cmpq	$0, 144(%rbx)
	je	.L153
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L154
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r15, 144(%r12)
	.p2align 4,,10
	.p2align 3
.L150:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 160(%r12)
	movq	%rdx, 152(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	testq	%r13, %r13
	je	.L147
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L147:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L166
.L153:
	movq	$0, 144(%r12)
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L166:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L154
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L154:
	movq	%r15, 144(%r12)
	jmp	.L150
	.cfi_endproc
.LFE4660:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE:
.LFB4662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	16(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, 4(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, 12(%rdi)
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L169
	cmpl	$2, %eax
	jne	.L170
.L169:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L170:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 160(%r12)
	movq	%rdx, 152(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	testq	%r13, %r13
	je	.L167
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L167:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4662:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE:
.LFB4663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	leaq	40(%r12), %r14
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L177
	cmpl	$2, %eax
	jne	.L179
	cmpq	$0, 144(%rbx)
	je	.L182
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L183
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r15, 144(%r12)
.L179:
	movq	152(%rbx), %rdx
	movl	160(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L191
.L182:
	movq	$0, 144(%r12)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L183
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L183:
	movq	%r15, 144(%r12)
	jmp	.L179
	.cfi_endproc
.LFE4663:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE:
.LFB4664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	leaq	40(%r12), %r14
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L194
	cmpl	$2, %eax
	jne	.L195
.L194:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L195:
	movq	152(%rbx), %rdx
	movl	160(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4664:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE:
.LFB4665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	leaq	40(%r12), %r14
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L198
	cmpl	$2, %eax
	jne	.L200
	cmpq	$0, 144(%rbx)
	je	.L203
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L204
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r15, 144(%r12)
	.p2align 4,,10
	.p2align 3
.L200:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 160(%r12)
	movq	%rdx, 152(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	testq	%r13, %r13
	je	.L197
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L197:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L216
.L203:
	movq	$0, 144(%r12)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L216:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L204
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L204:
	movq	%r15, 144(%r12)
	jmp	.L200
	.cfi_endproc
.LFE4665:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE:
.LFB4666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	leaq	40(%r12), %r14
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L219
	cmpl	$2, %eax
	jne	.L220
.L219:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L220:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 160(%r12)
	movq	%rdx, 152(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	testq	%r13, %r13
	je	.L217
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L217:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4666:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE:
.LFB4667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L227
	cmpl	$2, %eax
	jne	.L229
	cmpq	$0, 144(%rbx)
	je	.L232
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L233
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L229:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movdqu	0(%r13), %xmm3
	popq	%rbx
	movups	%xmm3, 64(%r12)
	movq	16(%r13), %rax
	movq	%rax, 80(%r12)
	movl	24(%r13), %eax
	movl	%eax, 88(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L241
.L232:
	movq	$0, 144(%r12)
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L241:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L233
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L233:
	movq	%r14, 144(%r12)
	jmp	.L229
	.cfi_endproc
.LFE4667:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE:
.LFB4668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L244
	cmpl	$2, %eax
	jne	.L245
.L244:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L245:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movdqu	0(%r13), %xmm3
	movups	%xmm3, 64(%r12)
	movq	16(%r13), %rax
	movq	%rax, 80(%r12)
	movl	24(%r13), %eax
	movl	%eax, 88(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4668:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode:
.LFB4669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L248
	cmpl	$2, %eax
	jne	.L250
	cmpq	$0, 144(%rbx)
	je	.L253
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L254
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L250:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movl	%r13d, 96(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L262
.L253:
	movq	$0, 144(%r12)
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L262:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L254
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L254:
	movq	%r14, 144(%r12)
	jmp	.L250
	.cfi_endproc
.LFE4669:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode:
.LFB4670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L265
	cmpl	$2, %eax
	jne	.L266
.L265:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L266:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movl	%r13d, 96(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4670:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy:
.LFB4671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L269
	cmpl	$2, %eax
	jne	.L271
	cmpq	$0, 144(%rbx)
	je	.L274
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L275
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L271:
	movq	152(%rbx), %rdx
	movl	160(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	%r13d, %edi
	call	_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy@PLT
	popq	%rbx
	movq	%rax, 100(%r12)
	movq	%r12, %rax
	movl	%edx, 108(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L283
.L274:
	movq	$0, 144(%r12)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L275
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L275:
	movq	%r14, 144(%r12)
	jmp	.L271
	.cfi_endproc
.LFE4671:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy:
.LFB4672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L286
	cmpl	$2, %eax
	jne	.L287
.L286:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L287:
	movq	152(%rbx), %rdx
	movl	160(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movl	%r13d, %edi
	call	_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy@PLT
	movq	%rax, 100(%r12)
	movq	%r12, %rax
	movl	%edx, 108(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4672:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE:
.LFB4673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L290
	cmpl	$2, %eax
	jne	.L292
	cmpq	$0, 144(%rbx)
	je	.L295
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L296
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L292:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	0(%r13), %rax
	popq	%rbx
	movq	%rax, 124(%r12)
	movzbl	8(%r13), %eax
	movb	%al, 132(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L304
.L295:
	movq	$0, 144(%r12)
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L304:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L296
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L296:
	movq	%r14, 144(%r12)
	jmp	.L292
	.cfi_endproc
.LFE4673:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE:
.LFB4674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L307
	cmpl	$2, %eax
	jne	.L308
.L307:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L308:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	0(%r13), %rax
	movq	%rax, 124(%r12)
	movzbl	8(%r13), %eax
	movb	%al, 132(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4674:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE:
.LFB4675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L311
	cmpl	$2, %eax
	jne	.L313
	cmpq	$0, 144(%rbx)
	je	.L316
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L317
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
	.p2align 4,,10
	.p2align 3
.L313:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L319
	cmpl	$2, %eax
	je	.L319
.L320:
	movl	$1, 136(%r12)
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L321
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L321:
	movq	%rbx, 144(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L320
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L311:
	cmpq	$0, 144(%rbx)
	jne	.L340
.L316:
	movq	$0, 144(%r12)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L340:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L317
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L317:
	movq	%r14, 144(%r12)
	jmp	.L313
	.cfi_endproc
.LFE4675:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE:
.LFB4676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L343
	cmpl	$2, %eax
	jne	.L344
.L343:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L344:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L346
	cmpl	$2, %eax
	je	.L346
.L347:
	movl	$1, 136(%r12)
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L348
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L348:
	movq	%rbx, 144(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L347
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L347
	.cfi_endproc
.LFE4676:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE:
.LFB4677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L362
	cmpl	$2, %eax
	jne	.L364
	cmpq	$0, 144(%rbx)
	je	.L367
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L368
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
	.p2align 4,,10
	.p2align 3
.L364:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L370
	cmpl	$2, %eax
	je	.L370
.L371:
	popq	%rbx
	movq	%r12, %rax
	movq	%r13, 144(%r12)
	movl	$2, 136(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L371
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L362:
	cmpq	$0, 144(%rbx)
	jne	.L387
.L367:
	movq	$0, 144(%r12)
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L387:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L368
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L368:
	movq	%r14, 144(%r12)
	jmp	.L364
	.cfi_endproc
.LFE4677:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE:
.LFB4678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L390
	cmpl	$2, %eax
	jne	.L391
.L390:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L391:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L393
	cmpl	$2, %eax
	je	.L393
.L394:
	movq	%r13, 144(%r12)
	movq	%r12, %rax
	movl	$2, 136(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L394
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L394
	.cfi_endproc
.LFE4678:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth:
.LFB4679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L405
	cmpl	$2, %eax
	jne	.L407
	cmpq	$0, 144(%rbx)
	je	.L410
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L411
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L407:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movl	%r13d, 152(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L419
.L410:
	movq	$0, 144(%r12)
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L419:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L411
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L411:
	movq	%r14, 144(%r12)
	jmp	.L407
	.cfi_endproc
.LFE4679:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth:
.LFB4680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L422
	cmpl	$2, %eax
	jne	.L423
.L422:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L423:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movl	%r13d, 152(%r12)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4680:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4signE18UNumberSignDisplay,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4signE18UNumberSignDisplay,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4signE18UNumberSignDisplay
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4signE18UNumberSignDisplay, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4signE18UNumberSignDisplay:
.LFB4681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L426
	cmpl	$2, %eax
	jne	.L428
	cmpq	$0, 144(%rbx)
	je	.L431
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L432
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L428:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movl	%r13d, 156(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L440
.L431:
	movq	$0, 144(%r12)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L440:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L432
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L432:
	movq	%r14, 144(%r12)
	jmp	.L428
	.cfi_endproc
.LFE4681:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4signE18UNumberSignDisplay, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4signE18UNumberSignDisplay
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4signE18UNumberSignDisplay,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4signE18UNumberSignDisplay,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4signE18UNumberSignDisplay
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4signE18UNumberSignDisplay, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4signE18UNumberSignDisplay:
.LFB4682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L443
	cmpl	$2, %eax
	jne	.L444
.L443:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L444:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movl	%r13d, 156(%r12)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4682:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4signE18UNumberSignDisplay, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE4signE18UNumberSignDisplay
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay:
.LFB4683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L447
	cmpl	$2, %eax
	jne	.L449
	cmpq	$0, 144(%rbx)
	je	.L452
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L453
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L449:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movl	%r13d, 160(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L461
.L452:
	movq	$0, 144(%r12)
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L461:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L453
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L453:
	movq	%r14, 144(%r12)
	jmp	.L449
	.cfi_endproc
.LFE4683:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay:
.LFB4684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L464
	cmpl	$2, %eax
	jne	.L465
.L464:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L465:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movl	%r13d, 160(%r12)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4684:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5scaleERKNS0_5ScaleE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5scaleERKNS0_5ScaleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5scaleERKNS0_5ScaleE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5scaleERKNS0_5ScaleE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5scaleERKNS0_5ScaleE:
.LFB4685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L468
	cmpl	$2, %eax
	jne	.L470
	cmpq	$0, 144(%rbx)
	je	.L473
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L474
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L470:
	movq	152(%rbx), %rdx
	movl	160(%rbx), %eax
	leaq	168(%r12), %r14
	leaq	168(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L482
.L473:
	movq	$0, 144(%r12)
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L482:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L474
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L474:
	movq	%r14, 144(%r12)
	jmp	.L470
	.cfi_endproc
.LFE4685:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5scaleERKNS0_5ScaleE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5scaleERKNS0_5ScaleE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5scaleERKNS0_5ScaleE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5scaleERKNS0_5ScaleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5scaleERKNS0_5ScaleE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5scaleERKNS0_5ScaleE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5scaleERKNS0_5ScaleE:
.LFB4686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L485
	cmpl	$2, %eax
	jne	.L486
.L485:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L486:
	movq	152(%rbx), %rdx
	movl	160(%rbx), %eax
	leaq	168(%r12), %r14
	leaq	168(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4686:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5scaleERKNS0_5ScaleE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5scaleERKNS0_5ScaleE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE:
.LFB4687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L489
	cmpl	$2, %eax
	jne	.L491
	cmpq	$0, 144(%rbx)
	je	.L494
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L495
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L491:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	0(%r13), %rax
	popq	%rbx
	movq	%rax, 112(%r12)
	movl	8(%r13), %eax
	movl	%eax, 120(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L489:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L503
.L494:
	movq	$0, 144(%r12)
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L503:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L495
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L495:
	movq	%r14, 144(%r12)
	jmp	.L491
	.cfi_endproc
.LFE4687:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE:
.LFB4688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L506
	cmpl	$2, %eax
	jne	.L507
.L506:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L507:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	0(%r13), %rax
	movq	%rax, 112(%r12)
	movl	8(%r13), %eax
	movl	%eax, 120(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4688:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9thresholdEi,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9thresholdEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9thresholdEi
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9thresholdEi, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9thresholdEi:
.LFB4689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L510
	cmpl	$2, %eax
	jne	.L512
	cmpq	$0, 144(%rbx)
	je	.L515
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L516
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L512:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movl	%r13d, 208(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L524
.L515:
	movq	$0, 144(%r12)
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L524:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L516
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L516:
	movq	%r14, 144(%r12)
	jmp	.L512
	.cfi_endproc
.LFE4689:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9thresholdEi, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9thresholdEi
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9thresholdEi,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9thresholdEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9thresholdEi
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9thresholdEi, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9thresholdEi:
.LFB4690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L527
	cmpl	$2, %eax
	jne	.L528
.L527:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L528:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movl	%r13d, 208(%r12)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4690:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9thresholdEi, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE9thresholdEi
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE:
.LFB4691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	addq	$16, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, 4(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, 12(%rdi)
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%r12), %rax
	leaq	40(%r13), %rsi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%r13), %eax
	movdqu	64(%r13), %xmm0
	movdqu	80(%r13), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%r13), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%r13), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%r13), %rax
	movq	%rax, 112(%r12)
	movl	120(%r13), %eax
	movl	%eax, 120(%r12)
	movq	124(%r13), %rax
	movq	%rax, 124(%r12)
	movl	132(%r13), %eax
	movl	%eax, 132(%r12)
	movl	136(%r13), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L531
	cmpl	$2, %eax
	jne	.L533
	cmpq	$0, 144(%r13)
	je	.L536
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L537
	movq	144(%r13), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
	.p2align 4,,10
	.p2align 3
.L533:
	movq	152(%r13), %rdx
	movl	160(%r13), %eax
	leaq	168(%r12), %r8
	leaq	168(%r13), %rsi
	movq	%r8, %rdi
	movq	%r8, -64(%rbp)
	leaq	216(%r12), %r14
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%r13), %eax
	leaq	216(%r13), %rsi
	movq	%r14, %rdi
	movdqu	192(%r13), %xmm2
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	4(%rbx), %rax
	leaq	16(%rbx), %rsi
	movq	%r15, %rdi
	movq	%rax, 4(%r12)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movq	-56(%rbp), %rdi
	leaq	40(%rbx), %rsi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movdqu	64(%rbx), %xmm3
	cmpq	%rbx, %r12
	movq	-64(%rbp), %r8
	movups	%xmm3, 64(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 132(%r12)
	je	.L538
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L540
	cmpl	$2, %eax
	jne	.L541
.L540:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L541
	movq	(%rdi), %rax
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-56(%rbp), %r8
.L541:
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L542
	cmpl	$2, %eax
	je	.L543
.L538:
	movl	152(%rbx), %eax
	leaq	168(%rbx), %rsi
	movq	%r8, %rdi
	movl	%eax, 152(%r12)
	movl	156(%rbx), %eax
	movl	%eax, 156(%r12)
	movl	160(%rbx), %eax
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	movdqu	192(%rbx), %xmm4
	leaq	216(%rbx), %rsi
	movq	%r14, %rdi
	movups	%xmm4, 192(%r12)
	movl	208(%rbx), %eax
	movl	%eax, 208(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	je	.L546
	movl	$88, %edi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L547
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	-56(%rbp), %r8
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L542:
	cmpq	$0, 144(%rbx)
	je	.L546
	movl	$2816, %edi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L547
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
	movq	-56(%rbp), %r8
.L547:
	movq	%r13, 144(%r12)
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L531:
	cmpq	$0, 144(%r13)
	jne	.L569
.L536:
	movq	$0, 144(%r12)
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L546:
	movq	$0, 144(%r12)
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L569:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L537
	movq	144(%r13), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L537:
	movq	%r14, 144(%r12)
	jmp	.L533
	.cfi_endproc
.LFE4691:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE:
.LFB4692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	addq	$16, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	leaq	40(%r12), %r15
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	-12(%rsi), %rax
	movq	%r8, -56(%rbp)
	movq	%rax, 4(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, 12(%rdi)
	movq	%r8, %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%r13), %eax
	movdqu	64(%r13), %xmm0
	movdqu	80(%r13), %xmm1
	movq	-56(%rbp), %r8
	movl	%eax, 96(%r12)
	movq	100(%r13), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%r13), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%r13), %rax
	movq	%rax, 112(%r12)
	movl	120(%r13), %eax
	movl	%eax, 120(%r12)
	movq	124(%r13), %rax
	movq	%rax, 124(%r12)
	movl	132(%r13), %eax
	movl	%eax, 132(%r12)
	movl	136(%r13), %eax
	cmpl	$1, %eax
	movl	%eax, 136(%r12)
	je	.L572
	cmpl	$2, %eax
	jne	.L573
.L572:
	movq	144(%r13), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%r13)
.L573:
	movq	152(%r13), %rdx
	movl	160(%r13), %eax
	movq	%r8, -64(%rbp)
	leaq	168(%r12), %r9
	movq	%r9, %rdi
	leaq	168(%r13), %rsi
	movq	%r9, -56(%rbp)
	leaq	216(%r12), %r14
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%r13), %eax
	leaq	216(%r13), %rsi
	movq	%r14, %rdi
	movdqu	192(%r13), %xmm2
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	4(%rbx), %rax
	movq	-64(%rbp), %r8
	leaq	16(%rbx), %rsi
	movq	%rax, 4(%r12)
	movl	12(%rbx), %eax
	movq	%r8, %rdi
	movl	%eax, 12(%r12)
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	leaq	40(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movdqu	64(%rbx), %xmm3
	cmpq	%rbx, %r12
	movq	-56(%rbp), %r9
	movups	%xmm3, 64(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 132(%r12)
	je	.L574
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L576
	cmpl	$2, %eax
	jne	.L577
.L576:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L577
	movq	(%rdi), %rax
	movq	%r9, -56(%rbp)
	call	*8(%rax)
	movq	-56(%rbp), %r9
.L577:
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L578
	cmpl	$2, %eax
	je	.L579
.L574:
	movl	152(%rbx), %eax
	leaq	168(%rbx), %rsi
	movq	%r9, %rdi
	movl	%eax, 152(%r12)
	movl	156(%rbx), %eax
	movl	%eax, 156(%r12)
	movl	160(%rbx), %eax
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	movdqu	192(%rbx), %xmm4
	leaq	216(%rbx), %rsi
	movq	%r14, %rdi
	movups	%xmm4, 192(%r12)
	movl	208(%rbx), %eax
	movl	%eax, 208(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	je	.L582
	movl	$88, %edi
	movq	%r9, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L583
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	-56(%rbp), %r9
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L578:
	cmpq	$0, 144(%rbx)
	je	.L582
	movl	$2816, %edi
	movq	%r9, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L583
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
	movq	-56(%rbp), %r9
.L583:
	movq	%r13, 144(%r12)
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L582:
	movq	$0, 144(%r12)
	jmp	.L574
	.cfi_endproc
.LFE4692:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE:
.LFB4693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	addq	$16, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, 4(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, 12(%rdi)
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%r12), %rax
	leaq	40(%r13), %rsi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%r13), %eax
	movdqu	64(%r13), %xmm0
	movdqu	80(%r13), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%r13), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%r13), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%r13), %rax
	movq	%rax, 112(%r12)
	movl	120(%r13), %eax
	movl	%eax, 120(%r12)
	movq	124(%r13), %rax
	movq	%rax, 124(%r12)
	movl	132(%r13), %eax
	movl	%eax, 132(%r12)
	movl	136(%r13), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L600
	cmpl	$2, %eax
	jne	.L602
	cmpq	$0, 144(%r13)
	je	.L605
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L606
	movq	144(%r13), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
	.p2align 4,,10
	.p2align 3
.L602:
	movq	152(%r13), %rdx
	movl	160(%r13), %eax
	leaq	168(%r12), %r8
	leaq	168(%r13), %rsi
	movq	%r8, %rdi
	movq	%r8, -64(%rbp)
	leaq	216(%r12), %r14
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%r13), %eax
	leaq	216(%r13), %rsi
	movq	%r14, %rdi
	movdqu	192(%r13), %xmm2
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	4(%rbx), %rax
	leaq	16(%rbx), %rsi
	movq	%r15, %rdi
	movq	%rax, 4(%r12)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	-56(%rbp), %rdi
	leaq	40(%rbx), %rsi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movdqu	64(%rbx), %xmm3
	cmpq	%rbx, %r12
	movq	-64(%rbp), %r8
	movups	%xmm3, 64(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 132(%r12)
	je	.L607
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L609
	cmpl	$2, %eax
	jne	.L610
.L609:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L610
	movq	(%rdi), %rax
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-56(%rbp), %r8
.L610:
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L612
	cmpl	$2, %eax
	je	.L612
.L607:
	movl	152(%rbx), %eax
	leaq	168(%rbx), %rsi
	movq	%r8, %rdi
	movl	%eax, 152(%r12)
	movl	156(%rbx), %eax
	movl	%eax, 156(%r12)
	movl	160(%rbx), %eax
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleaSEOS1_@PLT
	movdqu	192(%rbx), %xmm4
	leaq	216(%rbx), %rsi
	movq	%r14, %rdi
	movups	%xmm4, 192(%r12)
	movl	208(%rbx), %eax
	movl	%eax, 208(%r12)
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L612:
	.cfi_restore_state
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L600:
	cmpq	$0, 144(%r13)
	jne	.L628
.L605:
	movq	$0, 144(%r12)
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L628:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L606
	movq	144(%r13), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L606:
	movq	%r14, 144(%r12)
	jmp	.L602
	.cfi_endproc
.LFE4693:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE:
.LFB4694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	addq	$16, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	leaq	40(%r12), %r15
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	-12(%rsi), %rax
	movq	%r8, -56(%rbp)
	movq	%rax, 4(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, 12(%rdi)
	movq	%r8, %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%r13), %eax
	movdqu	64(%r13), %xmm0
	movdqu	80(%r13), %xmm1
	movq	-56(%rbp), %r8
	movl	%eax, 96(%r12)
	movq	100(%r13), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%r13), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%r13), %rax
	movq	%rax, 112(%r12)
	movl	120(%r13), %eax
	movl	%eax, 120(%r12)
	movq	124(%r13), %rax
	movq	%rax, 124(%r12)
	movl	132(%r13), %eax
	movl	%eax, 132(%r12)
	movl	136(%r13), %eax
	cmpl	$1, %eax
	movl	%eax, 136(%r12)
	je	.L631
	cmpl	$2, %eax
	jne	.L632
.L631:
	movq	144(%r13), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%r13)
.L632:
	movq	152(%r13), %rdx
	movl	160(%r13), %eax
	movq	%r8, -64(%rbp)
	leaq	168(%r12), %r9
	movq	%r9, %rdi
	leaq	168(%r13), %rsi
	movq	%r9, -56(%rbp)
	leaq	216(%r12), %r14
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%r13), %eax
	leaq	216(%r13), %rsi
	movq	%r14, %rdi
	movdqu	192(%r13), %xmm2
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	movq	4(%rbx), %rax
	movq	-64(%rbp), %r8
	leaq	16(%rbx), %rsi
	movq	%rax, 4(%r12)
	movl	12(%rbx), %eax
	movq	%r8, %rdi
	movl	%eax, 12(%r12)
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	leaq	40(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movdqu	64(%rbx), %xmm3
	cmpq	%rbx, %r12
	movq	-56(%rbp), %r9
	movups	%xmm3, 64(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 132(%r12)
	je	.L633
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L635
	cmpl	$2, %eax
	jne	.L636
.L635:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L636
	movq	(%rdi), %rax
	movq	%r9, -56(%rbp)
	call	*8(%rax)
	movq	-56(%rbp), %r9
.L636:
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L638
	cmpl	$2, %eax
	je	.L638
.L633:
	movl	152(%rbx), %eax
	leaq	168(%rbx), %rsi
	movq	%r9, %rdi
	movl	%eax, 152(%r12)
	movl	156(%rbx), %eax
	movl	%eax, 156(%r12)
	movl	160(%rbx), %eax
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleaSEOS1_@PLT
	movdqu	192(%rbx), %xmm4
	leaq	216(%rbx), %rsi
	movq	%r14, %rdi
	movups	%xmm4, 192(%r12)
	movl	208(%rbx), %eax
	movl	%eax, 208(%r12)
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L638:
	.cfi_restore_state
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
	jmp	.L633
	.cfi_endproc
.LFE4694:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5cloneEv,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5cloneEv
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5cloneEv, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5cloneEv:
.LFB4695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$448, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L649
	movq	4(%rbx), %rax
	leaq	16(%rbx), %rsi
	leaq	16(%r13), %rdi
	movq	%rax, 4(%r13)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r13)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r13), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r13)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r13)
	movq	%rax, 100(%r13)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r13)
	movl	%eax, 108(%r13)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r13)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r13)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r13)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r13)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r13)
	cmpl	$1, %eax
	je	.L650
	cmpl	$2, %eax
	je	.L651
.L652:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r13), %rdi
	movl	%eax, 160(%r13)
	movq	%rdx, 152(%r13)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r13), %rdi
	movl	%eax, 208(%r13)
	movups	%xmm2, 192(%r13)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
.L649:
	popq	%rbx
	movq	%r13, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	je	.L655
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L656
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L650:
	cmpq	$0, 144(%rbx)
	jne	.L667
.L655:
	movq	$0, 144(%r13)
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L667:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L656
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L656:
	movq	%r14, 144(%r13)
	jmp	.L652
	.cfi_endproc
.LFE4695:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5cloneEv, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5cloneEv
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5cloneEv,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5cloneEv
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5cloneEv, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5cloneEv:
.LFB4698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$448, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L669
	movq	4(%rbx), %rax
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	movq	%rax, 4(%r12)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L671
	cmpl	$2, %eax
	je	.L671
.L672:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 160(%r12)
	movq	%rdx, 152(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
.L669:
	movq	%r12, 0(%r13)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L671:
	.cfi_restore_state
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
	jmp	.L672
	.cfi_endproc
.LFE4698:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5cloneEv, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE5cloneEv
	.section	.text._ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE11copyErrorToER10UErrorCode,"axG",@progbits,_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE11copyErrorToER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE11copyErrorToER10UErrorCode
	.type	_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE11copyErrorToER10UErrorCode, @function
_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE11copyErrorToER10UErrorCode:
.LFB4699:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jle	.L690
.L677:
	ret
	.p2align 4,,10
	.p2align 3
.L690:
	cmpl	$3, 4(%rdi)
	je	.L679
	cmpl	$9, 64(%rdi)
	je	.L691
	cmpl	$-3, 112(%rdi)
	je	.L692
	cmpb	$0, 132(%rdi)
	jne	.L693
	movl	136(%rdi), %ecx
	leal	-1(%rcx), %edx
	cmpl	$1, %edx
	jbe	.L694
.L685:
	movl	184(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L677
	movl	%edx, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	movl	116(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L694:
	cmpq	$0, 144(%rdi)
	jne	.L685
	movl	$7, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L679:
	movl	8(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L693:
	movl	124(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L691:
	movl	72(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.cfi_endproc
.LFE4699:
	.size	_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE11copyErrorToER10UErrorCode, .-_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE11copyErrorToER10UErrorCode
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8notationERKNS0_8NotationE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8notationERKNS0_8NotationE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8notationERKNS0_8NotationE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8notationERKNS0_8NotationE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8notationERKNS0_8NotationE:
.LFB4700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L696
	cmpl	$2, %eax
	jne	.L698
	cmpq	$0, 144(%rbx)
	je	.L701
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L702
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L698:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movq	$0, 448(%r12)
	movq	$0, 440(%r12)
	movq	0(%r13), %rax
	movq	%rax, 4(%r12)
	movl	8(%r13), %eax
	movl	%eax, 12(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L696:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L710
.L701:
	movq	$0, 144(%r12)
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L710:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L702
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L702:
	movq	%r14, 144(%r12)
	jmp	.L698
	.cfi_endproc
.LFE4700:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8notationERKNS0_8NotationE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8notationERKNS0_8NotationE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE:
.LFB4702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, 4(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, 12(%rdi)
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L712
	cmpl	$2, %eax
	jne	.L714
	cmpq	$0, 144(%rbx)
	je	.L717
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L718
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r15, 144(%r12)
.L714:
	movq	152(%rbx), %rdx
	movl	160(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L726
.L717:
	movq	$0, 144(%r12)
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L726:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L718
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L718:
	movq	%r15, 144(%r12)
	jmp	.L714
	.cfi_endproc
.LFE4702:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE:
.LFB4704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, 4(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, 12(%rdi)
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L728
	cmpl	$2, %eax
	jne	.L730
	cmpq	$0, 144(%rbx)
	je	.L733
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L734
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r15, 144(%r12)
	.p2align 4,,10
	.p2align 3
.L730:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	testq	%r13, %r13
	je	.L727
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L727:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L728:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L746
.L733:
	movq	$0, 144(%r12)
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L746:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L734
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L734:
	movq	%r15, 144(%r12)
	jmp	.L730
	.cfi_endproc
.LFE4704:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE:
.LFB4706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	leaq	40(%r12), %r14
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L748
	cmpl	$2, %eax
	jne	.L750
	cmpq	$0, 144(%rbx)
	je	.L753
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L754
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r15, 144(%r12)
.L750:
	movq	152(%rbx), %rdx
	movl	160(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L748:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L762
.L753:
	movq	$0, 144(%r12)
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L762:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L754
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L754:
	movq	%r15, 144(%r12)
	jmp	.L750
	.cfi_endproc
.LFE4706:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE:
.LFB4708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	leaq	40(%r12), %r14
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L764
	cmpl	$2, %eax
	jne	.L766
	cmpq	$0, 144(%rbx)
	je	.L769
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L770
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r15, 144(%r12)
	.p2align 4,,10
	.p2align 3
.L766:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	testq	%r13, %r13
	je	.L763
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L763:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L764:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L782
.L769:
	movq	$0, 144(%r12)
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L782:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L770
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L770:
	movq	%r15, 144(%r12)
	jmp	.L766
	.cfi_endproc
.LFE4708:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE:
.LFB4710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L784
	cmpl	$2, %eax
	jne	.L786
	cmpq	$0, 144(%rbx)
	je	.L789
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L790
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L786:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movq	$0, 448(%r12)
	movdqu	0(%r13), %xmm3
	movq	$0, 440(%r12)
	movups	%xmm3, 64(%r12)
	movq	16(%r13), %rax
	movq	%rax, 80(%r12)
	movl	24(%r13), %eax
	movl	%eax, 88(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L784:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L798
.L789:
	movq	$0, 144(%r12)
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L798:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L790
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L790:
	movq	%r14, 144(%r12)
	jmp	.L786
	.cfi_endproc
.LFE4710:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode:
.LFB4712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L800
	cmpl	$2, %eax
	jne	.L802
	cmpq	$0, 144(%rbx)
	je	.L805
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L806
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L802:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movl	%r13d, 96(%r12)
	movq	%r12, %rax
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L800:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L814
.L805:
	movq	$0, 144(%r12)
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L814:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L806
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L806:
	movq	%r14, 144(%r12)
	jmp	.L802
	.cfi_endproc
.LFE4712:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy:
.LFB4714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L816
	cmpl	$2, %eax
	jne	.L818
	cmpq	$0, 144(%rbx)
	je	.L821
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L822
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L818:
	movq	152(%rbx), %rdx
	movl	160(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	%r13d, %edi
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	call	_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy@PLT
	popq	%rbx
	movq	%rax, 100(%r12)
	movq	%r12, %rax
	movl	%edx, 108(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L816:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L830
.L821:
	movq	$0, 144(%r12)
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L830:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L822
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L822:
	movq	%r14, 144(%r12)
	jmp	.L818
	.cfi_endproc
.LFE4714:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE:
.LFB4716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L832
	cmpl	$2, %eax
	jne	.L834
	cmpq	$0, 144(%rbx)
	je	.L837
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L838
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L834:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movq	$0, 448(%r12)
	movq	$0, 440(%r12)
	movq	0(%r13), %rax
	movq	%rax, 124(%r12)
	movzbl	8(%r13), %eax
	movb	%al, 132(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L832:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L846
.L837:
	movq	$0, 144(%r12)
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L846:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L838
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L838:
	movq	%r14, 144(%r12)
	jmp	.L834
	.cfi_endproc
.LFE4716:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE:
.LFB4718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L848
	cmpl	$2, %eax
	jne	.L850
	cmpq	$0, 144(%rbx)
	je	.L853
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L854
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
	.p2align 4,,10
	.p2align 3
.L850:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	136(%r12), %eax
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	cmpl	$1, %eax
	je	.L856
	cmpl	$2, %eax
	je	.L856
.L857:
	movl	$1, 136(%r12)
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L858
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L858:
	movq	%rbx, 144(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L856:
	.cfi_restore_state
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L857
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L848:
	cmpq	$0, 144(%rbx)
	jne	.L877
.L853:
	movq	$0, 144(%r12)
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L877:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L854
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L854:
	movq	%r14, 144(%r12)
	jmp	.L850
	.cfi_endproc
.LFE4718:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE:
.LFB4720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L879
	cmpl	$2, %eax
	jne	.L881
	cmpq	$0, 144(%rbx)
	je	.L884
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L885
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
	.p2align 4,,10
	.p2align 3
.L881:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	136(%r12), %eax
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	cmpl	$1, %eax
	je	.L887
	cmpl	$2, %eax
	je	.L887
.L888:
	popq	%rbx
	movq	%r12, %rax
	movq	%r13, 144(%r12)
	movl	$2, 136(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L887:
	.cfi_restore_state
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L888
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L879:
	cmpq	$0, 144(%rbx)
	jne	.L904
.L884:
	movq	$0, 144(%r12)
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L904:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L885
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L885:
	movq	%r14, 144(%r12)
	jmp	.L881
	.cfi_endproc
.LFE4720:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth:
.LFB4722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L906
	cmpl	$2, %eax
	jne	.L908
	cmpq	$0, 144(%rbx)
	je	.L911
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L912
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L908:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movl	%r13d, 152(%r12)
	movq	%r12, %rax
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L906:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L920
.L911:
	movq	$0, 144(%r12)
	jmp	.L908
	.p2align 4,,10
	.p2align 3
.L920:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L912
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L912:
	movq	%r14, 144(%r12)
	jmp	.L908
	.cfi_endproc
.LFE4722:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4signE18UNumberSignDisplay,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4signE18UNumberSignDisplay,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4signE18UNumberSignDisplay
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4signE18UNumberSignDisplay, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4signE18UNumberSignDisplay:
.LFB4724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L922
	cmpl	$2, %eax
	jne	.L924
	cmpq	$0, 144(%rbx)
	je	.L927
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L928
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L924:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movl	%r13d, 156(%r12)
	movq	%r12, %rax
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L922:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L936
.L927:
	movq	$0, 144(%r12)
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L936:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L928
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L928:
	movq	%r14, 144(%r12)
	jmp	.L924
	.cfi_endproc
.LFE4724:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4signE18UNumberSignDisplay, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4signE18UNumberSignDisplay
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay:
.LFB4726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L938
	cmpl	$2, %eax
	jne	.L940
	cmpq	$0, 144(%rbx)
	je	.L943
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L944
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L940:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movl	%r13d, 160(%r12)
	movq	%r12, %rax
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L938:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L952
.L943:
	movq	$0, 144(%r12)
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L952:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L944
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L944:
	movq	%r14, 144(%r12)
	jmp	.L940
	.cfi_endproc
.LFE4726:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5scaleERKNS0_5ScaleE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5scaleERKNS0_5ScaleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5scaleERKNS0_5ScaleE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5scaleERKNS0_5ScaleE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5scaleERKNS0_5ScaleE:
.LFB4728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L954
	cmpl	$2, %eax
	jne	.L956
	cmpq	$0, 144(%rbx)
	je	.L959
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L960
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L956:
	movq	152(%rbx), %rdx
	movl	160(%rbx), %eax
	leaq	168(%r12), %r14
	leaq	168(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L954:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L968
.L959:
	movq	$0, 144(%r12)
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L968:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L960
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L960:
	movq	%r14, 144(%r12)
	jmp	.L956
	.cfi_endproc
.LFE4728:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5scaleERKNS0_5ScaleE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5scaleERKNS0_5ScaleE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE:
.LFB4730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L970
	cmpl	$2, %eax
	jne	.L972
	cmpq	$0, 144(%rbx)
	je	.L975
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L976
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L972:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movq	$0, 448(%r12)
	movq	$0, 440(%r12)
	movq	0(%r13), %rax
	movq	%rax, 112(%r12)
	movl	8(%r13), %eax
	movl	%eax, 120(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L970:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L984
.L975:
	movq	$0, 144(%r12)
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L984:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L976
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L976:
	movq	%r14, 144(%r12)
	jmp	.L972
	.cfi_endproc
.LFE4730:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9thresholdEi,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9thresholdEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9thresholdEi
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9thresholdEi, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9thresholdEi:
.LFB4732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L986
	cmpl	$2, %eax
	jne	.L988
	cmpq	$0, 144(%rbx)
	je	.L991
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L992
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
.L988:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	popq	%rbx
	movl	%r13d, 208(%r12)
	movq	%r12, %rax
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L986:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L1000
.L991:
	movq	$0, 144(%r12)
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1000:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L992
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L992:
	movq	%r14, 144(%r12)
	jmp	.L988
	.cfi_endproc
.LFE4732:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9thresholdEi, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9thresholdEi
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE:
.LFB4734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	addq	$16, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, 4(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, 12(%rdi)
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%r12), %rax
	leaq	40(%r13), %rsi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%r13), %eax
	movdqu	64(%r13), %xmm0
	movdqu	80(%r13), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%r13), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%r13), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%r13), %rax
	movq	%rax, 112(%r12)
	movl	120(%r13), %eax
	movl	%eax, 120(%r12)
	movq	124(%r13), %rax
	movq	%rax, 124(%r12)
	movl	132(%r13), %eax
	movl	%eax, 132(%r12)
	movl	136(%r13), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1002
	cmpl	$2, %eax
	jne	.L1004
	cmpq	$0, 144(%r13)
	je	.L1007
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1008
	movq	144(%r13), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	152(%r13), %rdx
	movl	160(%r13), %eax
	leaq	168(%r12), %r8
	leaq	168(%r13), %rsi
	movq	%r8, %rdi
	movq	%r8, -64(%rbp)
	leaq	216(%r12), %r14
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%r13), %eax
	leaq	216(%r13), %rsi
	movq	%r14, %rdi
	movdqu	192(%r13), %xmm2
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	16(%rbx), %rsi
	movq	%r15, %rdi
	movq	$0, 448(%r12)
	movq	$0, 440(%r12)
	movq	4(%rbx), %rax
	movq	%rax, 4(%r12)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movq	-56(%rbp), %rdi
	leaq	40(%rbx), %rsi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movdqu	64(%rbx), %xmm3
	cmpq	%rbx, %r12
	movq	-64(%rbp), %r8
	movups	%xmm3, 64(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 132(%r12)
	je	.L1009
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L1011
	cmpl	$2, %eax
	jne	.L1012
.L1011:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1012
	movq	(%rdi), %rax
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-56(%rbp), %r8
.L1012:
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1013
	cmpl	$2, %eax
	je	.L1014
.L1009:
	movl	152(%rbx), %eax
	leaq	168(%rbx), %rsi
	movq	%r8, %rdi
	movl	%eax, 152(%r12)
	movl	156(%rbx), %eax
	movl	%eax, 156(%r12)
	movl	160(%rbx), %eax
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	movdqu	192(%rbx), %xmm4
	leaq	216(%rbx), %rsi
	movq	%r14, %rdi
	movups	%xmm4, 192(%r12)
	movl	208(%rbx), %eax
	movl	%eax, 208(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1014:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	je	.L1017
	movl	$88, %edi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1018
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	-56(%rbp), %r8
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1013:
	cmpq	$0, 144(%rbx)
	je	.L1017
	movl	$2816, %edi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1018
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
	movq	-56(%rbp), %r8
.L1018:
	movq	%r13, 144(%r12)
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1002:
	cmpq	$0, 144(%r13)
	jne	.L1040
.L1007:
	movq	$0, 144(%r12)
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	$0, 144(%r12)
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1040:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1008
	movq	144(%r13), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1008:
	movq	%r14, 144(%r12)
	jmp	.L1004
	.cfi_endproc
.LFE4734:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE:
.LFB4736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	addq	$16, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, 4(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, 12(%rdi)
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%r12), %rax
	leaq	40(%r13), %rsi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%r13), %eax
	movdqu	64(%r13), %xmm0
	movdqu	80(%r13), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%r13), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%r13), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%r13), %rax
	movq	%rax, 112(%r12)
	movl	120(%r13), %eax
	movl	%eax, 120(%r12)
	movq	124(%r13), %rax
	movq	%rax, 124(%r12)
	movl	132(%r13), %eax
	movl	%eax, 132(%r12)
	movl	136(%r13), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1042
	cmpl	$2, %eax
	jne	.L1044
	cmpq	$0, 144(%r13)
	je	.L1047
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1048
	movq	144(%r13), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r14, 144(%r12)
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	152(%r13), %rdx
	movl	160(%r13), %eax
	leaq	168(%r12), %r8
	leaq	168(%r13), %rsi
	movq	%r8, %rdi
	movq	%r8, -64(%rbp)
	leaq	216(%r12), %r14
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%r13), %eax
	leaq	216(%r13), %rsi
	movq	%r14, %rdi
	movdqu	192(%r13), %xmm2
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	16(%rbx), %rsi
	movq	%r15, %rdi
	movq	$0, 448(%r12)
	movq	$0, 440(%r12)
	movq	4(%rbx), %rax
	movq	%rax, 4(%r12)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	-56(%rbp), %rdi
	leaq	40(%rbx), %rsi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movdqu	64(%rbx), %xmm3
	cmpq	%rbx, %r12
	movq	-64(%rbp), %r8
	movups	%xmm3, 64(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 132(%r12)
	je	.L1049
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L1051
	cmpl	$2, %eax
	jne	.L1052
.L1051:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1052
	movq	(%rdi), %rax
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-56(%rbp), %r8
.L1052:
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1054
	cmpl	$2, %eax
	je	.L1054
.L1049:
	movl	152(%rbx), %eax
	leaq	168(%rbx), %rsi
	movq	%r8, %rdi
	movl	%eax, 152(%r12)
	movl	156(%rbx), %eax
	movl	%eax, 156(%r12)
	movl	160(%rbx), %eax
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleaSEOS1_@PLT
	movdqu	192(%rbx), %xmm4
	leaq	216(%rbx), %rsi
	movq	%r14, %rdi
	movups	%xmm4, 192(%r12)
	movl	208(%rbx), %eax
	movl	%eax, 208(%r12)
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1054:
	.cfi_restore_state
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1042:
	cmpq	$0, 144(%r13)
	jne	.L1070
.L1047:
	movq	$0, 144(%r12)
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1070:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1048
	movq	144(%r13), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1048:
	movq	%r14, 144(%r12)
	jmp	.L1044
	.cfi_endproc
.LFE4736:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE
	.section	.text._ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5cloneEv,"axG",@progbits,_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5cloneEv
	.type	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5cloneEv, @function
_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5cloneEv:
.LFB4738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$456, %edi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1072
	movq	4(%r12), %rax
	leaq	16(%r12), %rsi
	leaq	16(%rbx), %rdi
	movq	%rax, 4(%rbx)
	movl	12(%r12), %eax
	movl	%eax, 12(%rbx)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%r12), %rsi
	leaq	40(%rbx), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%r12), %eax
	movdqu	64(%r12), %xmm0
	movdqu	80(%r12), %xmm1
	movl	%eax, 96(%rbx)
	movq	100(%r12), %rax
	movups	%xmm0, 64(%rbx)
	movq	%rax, 100(%rbx)
	movl	108(%r12), %eax
	movups	%xmm1, 80(%rbx)
	movl	%eax, 108(%rbx)
	movq	112(%r12), %rax
	movq	%rax, 112(%rbx)
	movl	120(%r12), %eax
	movl	%eax, 120(%rbx)
	movq	124(%r12), %rax
	movq	%rax, 124(%rbx)
	movl	132(%r12), %eax
	movl	%eax, 132(%rbx)
	movl	136(%r12), %eax
	movl	%eax, 136(%rbx)
	cmpl	$1, %eax
	je	.L1073
	cmpl	$2, %eax
	je	.L1074
.L1075:
	movl	160(%r12), %eax
	movq	152(%r12), %rdx
	leaq	168(%r12), %rsi
	leaq	168(%rbx), %rdi
	movq	%rdx, 152(%rbx)
	movl	%eax, 160(%rbx)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%r12), %eax
	movdqu	192(%r12), %xmm2
	leaq	216(%r12), %rsi
	leaq	216(%rbx), %rdi
	movl	%eax, 208(%rbx)
	movups	%xmm2, 192(%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	$0, 440(%rbx)
	movq	$0, 448(%rbx)
.L1072:
	movq	%rbx, 0(%r13)
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1074:
	.cfi_restore_state
	cmpq	$0, 144(%r12)
	je	.L1078
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1079
	movq	144(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1073:
	cmpq	$0, 144(%r12)
	jne	.L1090
.L1078:
	movq	$0, 144(%rbx)
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1090:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1079
	movq	144(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1079:
	movq	%r14, 144(%rbx)
	jmp	.L1075
	.cfi_endproc
.LFE4738:
	.size	_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5cloneEv, .-_ZNKR6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5cloneEv
	.section	.text._ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE11copyErrorToER10UErrorCode,"axG",@progbits,_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE11copyErrorToER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE11copyErrorToER10UErrorCode
	.type	_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE11copyErrorToER10UErrorCode, @function
_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE11copyErrorToER10UErrorCode:
.LFB4742:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jle	.L1104
.L1091:
	ret
	.p2align 4,,10
	.p2align 3
.L1104:
	cmpl	$3, 4(%rdi)
	je	.L1093
	cmpl	$9, 64(%rdi)
	je	.L1105
	cmpl	$-3, 112(%rdi)
	je	.L1106
	cmpb	$0, 132(%rdi)
	jne	.L1107
	movl	136(%rdi), %ecx
	leal	-1(%rcx), %edx
	cmpl	$1, %edx
	jbe	.L1108
.L1099:
	movl	184(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L1091
	movl	%edx, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1106:
	movl	116(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1108:
	cmpq	$0, 144(%rdi)
	jne	.L1099
	movl	$7, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L1093:
	movl	8(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1107:
	movl	124(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1105:
	movl	72(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.cfi_endproc
.LFE4742:
	.size	_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE11copyErrorToER10UErrorCode, .-_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE11copyErrorToER10UErrorCode
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number15NumberFormatter4withEv
	.type	_ZN6icu_676number15NumberFormatter4withEv, @function
_ZN6icu_676number15NumberFormatter4withEv:
.LFB3949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	$2, -12(%rdi)
	movl	$0, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %eax
	movl	$-1, %edx
	movl	$0, 64(%r12)
	movw	%ax, 100(%r12)
	pxor	%xmm0, %xmm0
	movabsq	$30064771077, %rax
	leaq	216(%r12), %rdi
	movq	%rax, 152(%r12)
	movl	$4, 96(%r12)
	movl	$-2, 112(%r12)
	movb	$0, 132(%r12)
	movw	%dx, 124(%r12)
	movl	$0, 136(%r12)
	movq	$0, 144(%r12)
	movl	$2, 160(%r12)
	movl	$0, 168(%r12)
	movq	$0, 176(%r12)
	movl	$0, 184(%r12)
	movl	$3, 208(%r12)
	movups	%xmm0, 192(%r12)
	call	_ZN6icu_676LocaleC1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3949:
	.size	_ZN6icu_676number15NumberFormatter4withEv, .-_ZN6icu_676number15NumberFormatter4withEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number26UnlocalizedNumberFormatterC2ERKS1_
	.type	_ZN6icu_676number26UnlocalizedNumberFormatterC2ERKS1_, @function
_ZN6icu_676number26UnlocalizedNumberFormatterC2ERKS1_:
.LFB3970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1112
	cmpl	$2, %eax
	jne	.L1114
	cmpq	$0, 144(%rbx)
	je	.L1117
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1118
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r13, 144(%r12)
.L1114:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 160(%r12)
	movq	%rdx, 152(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleC1ERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L1112:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L1126
.L1117:
	movq	$0, 144(%r12)
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1126:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1118
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1118:
	movq	%r13, 144(%r12)
	jmp	.L1114
	.cfi_endproc
.LFE3970:
	.size	_ZN6icu_676number26UnlocalizedNumberFormatterC2ERKS1_, .-_ZN6icu_676number26UnlocalizedNumberFormatterC2ERKS1_
	.globl	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_
	.set	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKS1_,_ZN6icu_676number26UnlocalizedNumberFormatterC2ERKS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number26UnlocalizedNumberFormatterC2ERKNS0_23NumberFormatterSettingsIS1_EE
	.type	_ZN6icu_676number26UnlocalizedNumberFormatterC2ERKNS0_23NumberFormatterSettingsIS1_EE, @function
_ZN6icu_676number26UnlocalizedNumberFormatterC2ERKNS0_23NumberFormatterSettingsIS1_EE:
.LFB3979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1128
	cmpl	$2, %eax
	jne	.L1130
	cmpq	$0, 144(%rbx)
	je	.L1133
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1134
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r13, 144(%r12)
.L1130:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 160(%r12)
	movq	%rdx, 152(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleC1ERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L1128:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L1142
.L1133:
	movq	$0, 144(%r12)
	jmp	.L1130
	.p2align 4,,10
	.p2align 3
.L1142:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1134
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1134:
	movq	%r13, 144(%r12)
	jmp	.L1130
	.cfi_endproc
.LFE3979:
	.size	_ZN6icu_676number26UnlocalizedNumberFormatterC2ERKNS0_23NumberFormatterSettingsIS1_EE, .-_ZN6icu_676number26UnlocalizedNumberFormatterC2ERKNS0_23NumberFormatterSettingsIS1_EE
	.globl	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKNS0_23NumberFormatterSettingsIS1_EE
	.set	_ZN6icu_676number26UnlocalizedNumberFormatterC1ERKNS0_23NumberFormatterSettingsIS1_EE,_ZN6icu_676number26UnlocalizedNumberFormatterC2ERKNS0_23NumberFormatterSettingsIS1_EE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number26UnlocalizedNumberFormatterC2EOS1_
	.type	_ZN6icu_676number26UnlocalizedNumberFormatterC2EOS1_, @function
_ZN6icu_676number26UnlocalizedNumberFormatterC2EOS1_:
.LFB3982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1145
	cmpl	$2, %eax
	jne	.L1146
.L1145:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L1146:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 160(%r12)
	movq	%rdx, 152(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	popq	%rbx
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleC1EOS0_@PLT
	.cfi_endproc
.LFE3982:
	.size	_ZN6icu_676number26UnlocalizedNumberFormatterC2EOS1_, .-_ZN6icu_676number26UnlocalizedNumberFormatterC2EOS1_
	.globl	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_
	.set	_ZN6icu_676number26UnlocalizedNumberFormatterC1EOS1_,_ZN6icu_676number26UnlocalizedNumberFormatterC2EOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number26UnlocalizedNumberFormatterC2EONS0_23NumberFormatterSettingsIS1_EE
	.type	_ZN6icu_676number26UnlocalizedNumberFormatterC2EONS0_23NumberFormatterSettingsIS1_EE, @function
_ZN6icu_676number26UnlocalizedNumberFormatterC2EONS0_23NumberFormatterSettingsIS1_EE:
.LFB3992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1150
	cmpl	$2, %eax
	jne	.L1151
.L1150:
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
.L1151:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 160(%r12)
	movq	%rdx, 152(%r12)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	popq	%rbx
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleC1EOS0_@PLT
	.cfi_endproc
.LFE3992:
	.size	_ZN6icu_676number26UnlocalizedNumberFormatterC2EONS0_23NumberFormatterSettingsIS1_EE, .-_ZN6icu_676number26UnlocalizedNumberFormatterC2EONS0_23NumberFormatterSettingsIS1_EE
	.globl	_ZN6icu_676number26UnlocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	.set	_ZN6icu_676number26UnlocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE,_ZN6icu_676number26UnlocalizedNumberFormatterC2EONS0_23NumberFormatterSettingsIS1_EE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_
	.type	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_, @function
_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_:
.LFB3994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movdqu	64(%rbx), %xmm0
	movups	%xmm0, 64(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 132(%r12)
	cmpq	%rbx, %r12
	je	.L1154
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L1156
	cmpl	$2, %eax
	jne	.L1157
.L1156:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1157
	movq	(%rdi), %rax
	call	*8(%rax)
.L1157:
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1158
	cmpl	$2, %eax
	je	.L1159
.L1154:
	movl	152(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 152(%r12)
	movl	156(%rbx), %eax
	movl	%eax, 156(%r12)
	movl	160(%rbx), %eax
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	movdqu	192(%rbx), %xmm1
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movups	%xmm1, 192(%r12)
	movl	208(%rbx), %eax
	movl	%eax, 208(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1159:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	je	.L1162
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1163
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1158:
	cmpq	$0, 144(%rbx)
	je	.L1162
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1163
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1163:
	movq	%r13, 144(%r12)
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	$0, 144(%r12)
	jmp	.L1154
	.cfi_endproc
.LFE3994:
	.size	_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_, .-_ZN6icu_676number26UnlocalizedNumberFormatteraSERKS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_
	.type	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_, @function
_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_:
.LFB3997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movdqu	64(%rbx), %xmm0
	movups	%xmm0, 64(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 132(%r12)
	cmpq	%rbx, %r12
	je	.L1180
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L1182
	cmpl	$2, %eax
	jne	.L1183
.L1182:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1183
	movq	(%rdi), %rax
	call	*8(%rax)
.L1183:
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1185
	cmpl	$2, %eax
	je	.L1185
.L1180:
	movl	152(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 152(%r12)
	movl	156(%rbx), %eax
	movl	%eax, 156(%r12)
	movl	160(%rbx), %eax
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleaSEOS1_@PLT
	movdqu	192(%rbx), %xmm1
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movups	%xmm1, 192(%r12)
	movl	208(%rbx), %eax
	movl	%eax, 208(%r12)
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1185:
	.cfi_restore_state
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
	jmp	.L1180
	.cfi_endproc
.LFE3997:
	.size	_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_, .-_ZN6icu_676number26UnlocalizedNumberFormatteraSEOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number24LocalizedNumberFormatterC2ERKS1_
	.type	_ZN6icu_676number24LocalizedNumberFormatterC2ERKS1_, @function
_ZN6icu_676number24LocalizedNumberFormatterC2ERKS1_:
.LFB4001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1196
	cmpl	$2, %eax
	jne	.L1198
	cmpq	$0, 144(%rbx)
	je	.L1201
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1202
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r13, 144(%r12)
.L1198:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1196:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L1210
.L1201:
	movq	$0, 144(%r12)
	jmp	.L1198
	.p2align 4,,10
	.p2align 3
.L1210:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1202
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1202:
	movq	%r13, 144(%r12)
	jmp	.L1198
	.cfi_endproc
.LFE4001:
	.size	_ZN6icu_676number24LocalizedNumberFormatterC2ERKS1_, .-_ZN6icu_676number24LocalizedNumberFormatterC2ERKS1_
	.globl	_ZN6icu_676number24LocalizedNumberFormatterC1ERKS1_
	.set	_ZN6icu_676number24LocalizedNumberFormatterC1ERKS1_,_ZN6icu_676number24LocalizedNumberFormatterC2ERKS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number24LocalizedNumberFormatterC2ERKNS0_23NumberFormatterSettingsIS1_EE
	.type	_ZN6icu_676number24LocalizedNumberFormatterC2ERKNS0_23NumberFormatterSettingsIS1_EE, @function
_ZN6icu_676number24LocalizedNumberFormatterC2ERKNS0_23NumberFormatterSettingsIS1_EE:
.LFB4010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-12(%rsi), %rax
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movl	96(%rbx), %eax
	movdqu	64(%rbx), %xmm0
	movdqu	80(%rbx), %xmm1
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movups	%xmm1, 80(%r12)
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movl	132(%rbx), %eax
	movl	%eax, 132(%r12)
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1212
	cmpl	$2, %eax
	jne	.L1214
	cmpq	$0, 144(%rbx)
	je	.L1217
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1218
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r13, 144(%r12)
.L1214:
	movl	160(%rbx), %eax
	movq	152(%rbx), %rdx
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movq	%rdx, 152(%r12)
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	movl	208(%rbx), %eax
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movl	%eax, 208(%r12)
	movups	%xmm2, 192(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1212:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	jne	.L1226
.L1217:
	movq	$0, 144(%r12)
	jmp	.L1214
	.p2align 4,,10
	.p2align 3
.L1226:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1218
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1218:
	movq	%r13, 144(%r12)
	jmp	.L1214
	.cfi_endproc
.LFE4010:
	.size	_ZN6icu_676number24LocalizedNumberFormatterC2ERKNS0_23NumberFormatterSettingsIS1_EE, .-_ZN6icu_676number24LocalizedNumberFormatterC2ERKNS0_23NumberFormatterSettingsIS1_EE
	.globl	_ZN6icu_676number24LocalizedNumberFormatterC1ERKNS0_23NumberFormatterSettingsIS1_EE
	.set	_ZN6icu_676number24LocalizedNumberFormatterC1ERKNS0_23NumberFormatterSettingsIS1_EE,_ZN6icu_676number24LocalizedNumberFormatterC2ERKNS0_23NumberFormatterSettingsIS1_EE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number24LocalizedNumberFormatterC2ERKNS0_4impl10MacroPropsERKNS_6LocaleE
	.type	_ZN6icu_676number24LocalizedNumberFormatterC2ERKNS0_4impl10MacroPropsERKNS_6LocaleE, @function
_ZN6icu_676number24LocalizedNumberFormatterC2ERKNS0_4impl10MacroPropsERKNS_6LocaleE:
.LFB4044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	40(%rbx), %r15
	leaq	216(%rbx), %r14
	subq	$24, %rsp
	movq	$2, 4(%rdi)
	movl	$0, 12(%rdi)
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %eax
	pxor	%xmm0, %xmm0
	movl	$-1, %edx
	movw	%ax, 100(%rbx)
	movq	%r14, %rdi
	movabsq	$30064771077, %rax
	movw	%dx, 124(%rbx)
	movq	%rax, 152(%rbx)
	movl	$0, 64(%rbx)
	movl	$4, 96(%rbx)
	movl	$-2, 112(%rbx)
	movb	$0, 132(%rbx)
	movl	$0, 136(%rbx)
	movq	$0, 144(%rbx)
	movl	$2, 160(%rbx)
	movl	$0, 168(%rbx)
	movq	$0, 176(%rbx)
	movl	$0, 184(%rbx)
	movl	$3, 208(%rbx)
	movups	%xmm0, 192(%rbx)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	4(%r12), %rax
	movq	-56(%rbp), %r8
	movq	$0, 440(%rbx)
	movq	$0, 448(%rbx)
	leaq	16(%r12), %rsi
	movq	%rax, 4(%rbx)
	movl	12(%r12), %eax
	movq	%r8, %rdi
	movl	%eax, 12(%rbx)
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	leaq	40(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movq	80(%r12), %rax
	movdqu	64(%r12), %xmm1
	movq	%rax, 80(%rbx)
	movl	88(%r12), %eax
	movups	%xmm1, 64(%rbx)
	movl	%eax, 88(%rbx)
	movl	96(%r12), %eax
	movl	%eax, 96(%rbx)
	movq	100(%r12), %rax
	movq	%rax, 100(%rbx)
	movl	108(%r12), %eax
	movl	%eax, 108(%rbx)
	movq	112(%r12), %rax
	movq	%rax, 112(%rbx)
	movl	120(%r12), %eax
	movl	%eax, 120(%rbx)
	movq	124(%r12), %rax
	movq	%rax, 124(%rbx)
	movzbl	132(%r12), %eax
	movb	%al, 132(%rbx)
	cmpq	%r12, %rbx
	je	.L1228
	movl	136(%rbx), %eax
	cmpl	$1, %eax
	je	.L1230
	cmpl	$2, %eax
	jne	.L1231
.L1230:
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1231
	movq	(%rdi), %rax
	call	*8(%rax)
.L1231:
	movl	136(%r12), %eax
	movl	%eax, 136(%rbx)
	cmpl	$1, %eax
	je	.L1232
	cmpl	$2, %eax
	je	.L1233
.L1228:
	movq	152(%r12), %rdx
	movl	160(%r12), %eax
	leaq	168(%r12), %rsi
	leaq	168(%rbx), %rdi
	movq	%rdx, 152(%rbx)
	movl	%eax, 160(%rbx)
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	movl	208(%r12), %eax
	leaq	216(%r12), %rsi
	movq	%r14, %rdi
	movdqu	192(%r12), %xmm2
	movl	%eax, 208(%rbx)
	movups	%xmm2, 192(%rbx)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleaSERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L1233:
	.cfi_restore_state
	cmpq	$0, 144(%r12)
	je	.L1236
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1237
	movq	144(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1232:
	cmpq	$0, 144(%r12)
	je	.L1236
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1237
	movq	144(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1237:
	movq	%r15, 144(%rbx)
	jmp	.L1228
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	$0, 144(%rbx)
	jmp	.L1228
	.cfi_endproc
.LFE4044:
	.size	_ZN6icu_676number24LocalizedNumberFormatterC2ERKNS0_4impl10MacroPropsERKNS_6LocaleE, .-_ZN6icu_676number24LocalizedNumberFormatterC2ERKNS0_4impl10MacroPropsERKNS_6LocaleE
	.globl	_ZN6icu_676number24LocalizedNumberFormatterC1ERKNS0_4impl10MacroPropsERKNS_6LocaleE
	.set	_ZN6icu_676number24LocalizedNumberFormatterC1ERKNS0_4impl10MacroPropsERKNS_6LocaleE,_ZN6icu_676number24LocalizedNumberFormatterC2ERKNS0_4impl10MacroPropsERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number24LocalizedNumberFormatterC2EONS0_4impl10MacroPropsERKNS_6LocaleE
	.type	_ZN6icu_676number24LocalizedNumberFormatterC2EONS0_4impl10MacroPropsERKNS_6LocaleE, @function
_ZN6icu_676number24LocalizedNumberFormatterC2EONS0_4impl10MacroPropsERKNS_6LocaleE:
.LFB4048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	40(%rbx), %r15
	leaq	216(%rbx), %r14
	subq	$24, %rsp
	movq	$2, 4(%rdi)
	movl	$0, 12(%rdi)
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %eax
	pxor	%xmm0, %xmm0
	movl	$-1, %edx
	movw	%ax, 100(%rbx)
	movq	%r14, %rdi
	movabsq	$30064771077, %rax
	movw	%dx, 124(%rbx)
	movq	%rax, 152(%rbx)
	movl	$0, 64(%rbx)
	movl	$4, 96(%rbx)
	movl	$-2, 112(%rbx)
	movb	$0, 132(%rbx)
	movl	$0, 136(%rbx)
	movq	$0, 144(%rbx)
	movl	$2, 160(%rbx)
	movl	$0, 168(%rbx)
	movq	$0, 176(%rbx)
	movl	$0, 184(%rbx)
	movl	$3, 208(%rbx)
	movups	%xmm0, 192(%rbx)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	4(%r12), %rax
	movq	-56(%rbp), %r8
	movq	$0, 440(%rbx)
	movq	$0, 448(%rbx)
	leaq	16(%r12), %rsi
	movq	%rax, 4(%rbx)
	movl	12(%r12), %eax
	movq	%r8, %rdi
	movl	%eax, 12(%rbx)
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	leaq	40(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	80(%r12), %rax
	movdqu	64(%r12), %xmm1
	movq	%rax, 80(%rbx)
	movl	88(%r12), %eax
	movups	%xmm1, 64(%rbx)
	movl	%eax, 88(%rbx)
	movl	96(%r12), %eax
	movl	%eax, 96(%rbx)
	movq	100(%r12), %rax
	movq	%rax, 100(%rbx)
	movl	108(%r12), %eax
	movl	%eax, 108(%rbx)
	movq	112(%r12), %rax
	movq	%rax, 112(%rbx)
	movl	120(%r12), %eax
	movl	%eax, 120(%rbx)
	movq	124(%r12), %rax
	movq	%rax, 124(%rbx)
	movzbl	132(%r12), %eax
	movb	%al, 132(%rbx)
	cmpq	%r12, %rbx
	je	.L1254
	movl	136(%rbx), %eax
	cmpl	$1, %eax
	je	.L1256
	cmpl	$2, %eax
	jne	.L1257
.L1256:
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1257
	movq	(%rdi), %rax
	call	*8(%rax)
.L1257:
	movl	136(%r12), %eax
	movl	%eax, 136(%rbx)
	cmpl	$1, %eax
	je	.L1259
	cmpl	$2, %eax
	je	.L1259
.L1254:
	movq	152(%r12), %rdx
	movl	160(%r12), %eax
	leaq	168(%r12), %rsi
	leaq	168(%rbx), %rdi
	movq	%rdx, 152(%rbx)
	movl	%eax, 160(%rbx)
	call	_ZN6icu_676number5ScaleaSEOS1_@PLT
	movl	208(%r12), %eax
	leaq	216(%r12), %rsi
	movq	%r14, %rdi
	movdqu	192(%r12), %xmm2
	movl	%eax, 208(%rbx)
	movups	%xmm2, 192(%rbx)
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleaSERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L1259:
	.cfi_restore_state
	movq	144(%r12), %rax
	movq	%rax, 144(%rbx)
	movq	$0, 144(%r12)
	jmp	.L1254
	.cfi_endproc
.LFE4048:
	.size	_ZN6icu_676number24LocalizedNumberFormatterC2EONS0_4impl10MacroPropsERKNS_6LocaleE, .-_ZN6icu_676number24LocalizedNumberFormatterC2EONS0_4impl10MacroPropsERKNS_6LocaleE
	.globl	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_4impl10MacroPropsERKNS_6LocaleE
	.set	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_4impl10MacroPropsERKNS_6LocaleE,_ZN6icu_676number24LocalizedNumberFormatterC2EONS0_4impl10MacroPropsERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZNKR6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE
	.type	_ZNKR6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE, @function
_ZNKR6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE:
.LFB4050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	leaq	40(%r12), %r15
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	216(%r12), %r14
	subq	$24, %rsp
	movq	$2, 4(%rdi)
	movl	$0, 12(%rdi)
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %eax
	movl	$-1, %edx
	movq	%r14, %rdi
	movw	%ax, 100(%r12)
	pxor	%xmm0, %xmm0
	movabsq	$30064771077, %rax
	movw	%dx, 124(%r12)
	movq	%rax, 152(%r12)
	movl	$0, 64(%r12)
	movl	$4, 96(%r12)
	movl	$-2, 112(%r12)
	movb	$0, 132(%r12)
	movl	$0, 136(%r12)
	movq	$0, 144(%r12)
	movl	$2, 160(%r12)
	movl	$0, 168(%r12)
	movq	$0, 176(%r12)
	movl	$0, 184(%r12)
	movl	$3, 208(%r12)
	movups	%xmm0, 192(%r12)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	-56(%rbp), %r8
	leaq	16(%rbx), %rsi
	movq	$0, 448(%r12)
	movq	$0, 440(%r12)
	movq	4(%rbx), %rax
	movq	%r8, %rdi
	movq	%rax, 4(%r12)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	leaq	40(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movdqu	64(%rbx), %xmm1
	movups	%xmm1, 64(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 132(%r12)
	cmpq	%r12, %rbx
	je	.L1270
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L1272
	cmpl	$2, %eax
	jne	.L1273
.L1272:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1273
	movq	(%rdi), %rax
	call	*8(%rax)
.L1273:
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1274
	cmpl	$2, %eax
	je	.L1275
.L1270:
	movl	152(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 152(%r12)
	movl	156(%rbx), %eax
	movl	%eax, 156(%r12)
	movl	160(%rbx), %eax
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	movq	%r14, %rdi
	movups	%xmm2, 192(%r12)
	movl	208(%rbx), %eax
	movl	%eax, 208(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1275:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	je	.L1278
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1279
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1274:
	cmpq	$0, 144(%rbx)
	je	.L1278
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1279
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1279:
	movq	%r15, 144(%r12)
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	$0, 144(%r12)
	jmp	.L1270
	.cfi_endproc
.LFE4050:
	.size	_ZNKR6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE, .-_ZNKR6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZNO6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE
	.type	_ZNO6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE, @function
_ZNO6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE:
.LFB4051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	leaq	40(%r12), %r15
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	216(%r12), %r14
	subq	$24, %rsp
	movq	$2, 4(%rdi)
	movl	$0, 12(%rdi)
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %eax
	movl	$-1, %edx
	movq	%r14, %rdi
	movw	%ax, 100(%r12)
	pxor	%xmm0, %xmm0
	movabsq	$30064771077, %rax
	movw	%dx, 124(%r12)
	movq	%rax, 152(%r12)
	movl	$0, 64(%r12)
	movl	$4, 96(%r12)
	movl	$-2, 112(%r12)
	movb	$0, 132(%r12)
	movl	$0, 136(%r12)
	movq	$0, 144(%r12)
	movl	$2, 160(%r12)
	movl	$0, 168(%r12)
	movq	$0, 176(%r12)
	movl	$0, 184(%r12)
	movl	$3, 208(%r12)
	movups	%xmm0, 192(%r12)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	-56(%rbp), %r8
	leaq	16(%rbx), %rsi
	movq	$0, 448(%r12)
	movq	$0, 440(%r12)
	movq	4(%rbx), %rax
	movq	%r8, %rdi
	movq	%rax, 4(%r12)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	leaq	40(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movdqu	64(%rbx), %xmm1
	movups	%xmm1, 64(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 132(%r12)
	cmpq	%r12, %rbx
	je	.L1296
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L1298
	cmpl	$2, %eax
	jne	.L1299
.L1298:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1299
	movq	(%rdi), %rax
	call	*8(%rax)
.L1299:
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1301
	cmpl	$2, %eax
	je	.L1301
.L1296:
	movl	152(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 152(%r12)
	movl	156(%rbx), %eax
	movl	%eax, 156(%r12)
	movl	160(%rbx), %eax
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleaSEOS1_@PLT
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	movq	%r14, %rdi
	movups	%xmm2, 192(%r12)
	movl	208(%rbx), %eax
	movl	%eax, 208(%r12)
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1301:
	.cfi_restore_state
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
	jmp	.L1296
	.cfi_endproc
.LFE4051:
	.size	_ZNO6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE, .-_ZNO6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SymbolsWrapperC2ERKS2_
	.type	_ZN6icu_676number4impl14SymbolsWrapperC2ERKS2_, @function
_ZN6icu_676number4impl14SymbolsWrapperC2ERKS2_:
.LFB4053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	cmpl	$1, %eax
	je	.L1312
	cmpl	$2, %eax
	jne	.L1311
	cmpq	$0, 8(%rsi)
	je	.L1317
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1318
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r13, 8(%r12)
.L1311:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1312:
	.cfi_restore_state
	cmpq	$0, 8(%rsi)
	jne	.L1326
.L1317:
	movq	$0, 8(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1326:
	.cfi_restore_state
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1318
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1318:
	movq	%r13, 8(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4053:
	.size	_ZN6icu_676number4impl14SymbolsWrapperC2ERKS2_, .-_ZN6icu_676number4impl14SymbolsWrapperC2ERKS2_
	.globl	_ZN6icu_676number4impl14SymbolsWrapperC1ERKS2_
	.set	_ZN6icu_676number4impl14SymbolsWrapperC1ERKS2_,_ZN6icu_676number4impl14SymbolsWrapperC2ERKS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SymbolsWrapperC2EOS2_
	.type	_ZN6icu_676number4impl14SymbolsWrapperC2EOS2_, @function
_ZN6icu_676number4impl14SymbolsWrapperC2EOS2_:
.LFB4057:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	cmpl	$1, %eax
	je	.L1329
	cmpl	$2, %eax
	je	.L1329
	ret
	.p2align 4,,10
	.p2align 3
.L1329:
	movq	8(%rsi), %rax
	movq	%rax, 8(%rdi)
	movq	$0, 8(%rsi)
	ret
	.cfi_endproc
.LFE4057:
	.size	_ZN6icu_676number4impl14SymbolsWrapperC2EOS2_, .-_ZN6icu_676number4impl14SymbolsWrapperC2EOS2_
	.globl	_ZN6icu_676number4impl14SymbolsWrapperC1EOS2_
	.set	_ZN6icu_676number4impl14SymbolsWrapperC1EOS2_,_ZN6icu_676number4impl14SymbolsWrapperC2EOS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SymbolsWrapperaSERKS2_
	.type	_ZN6icu_676number4impl14SymbolsWrapperaSERKS2_, @function
_ZN6icu_676number4impl14SymbolsWrapperaSERKS2_:
.LFB4059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L1332
	movl	(%rdi), %eax
	movq	%rsi, %rbx
	cmpl	$1, %eax
	je	.L1334
	cmpl	$2, %eax
	jne	.L1335
.L1334:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1335
	movq	(%rdi), %rax
	call	*8(%rax)
.L1335:
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	cmpl	$1, %eax
	je	.L1336
	cmpl	$2, %eax
	je	.L1337
.L1332:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1337:
	.cfi_restore_state
	cmpq	$0, 8(%rbx)
	je	.L1340
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1341
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1336:
	cmpq	$0, 8(%rbx)
	je	.L1340
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1341
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1341:
	movq	%r13, 8(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1340:
	.cfi_restore_state
	movq	$0, 8(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4059:
	.size	_ZN6icu_676number4impl14SymbolsWrapperaSERKS2_, .-_ZN6icu_676number4impl14SymbolsWrapperaSERKS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SymbolsWrapperaSEOS2_
	.type	_ZN6icu_676number4impl14SymbolsWrapperaSEOS2_, @function
_ZN6icu_676number4impl14SymbolsWrapperaSEOS2_:
.LFB4060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L1358
	movl	(%rdi), %eax
	movq	%rsi, %rbx
	cmpl	$1, %eax
	je	.L1360
	cmpl	$2, %eax
	jne	.L1361
.L1360:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1361
	movq	(%rdi), %rax
	call	*8(%rax)
.L1361:
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	cmpl	$1, %eax
	je	.L1363
	cmpl	$2, %eax
	je	.L1363
.L1358:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1363:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	movq	%r12, %rax
	movq	$0, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4060:
	.size	_ZN6icu_676number4impl14SymbolsWrapperaSEOS2_, .-_ZN6icu_676number4impl14SymbolsWrapperaSEOS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SymbolsWrapperD2Ev
	.type	_ZN6icu_676number4impl14SymbolsWrapperD2Ev, @function
_ZN6icu_676number4impl14SymbolsWrapperD2Ev:
.LFB4062:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L1375
	cmpl	$2, %eax
	je	.L1375
	ret
	.p2align 4,,10
	.p2align 3
.L1375:
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1373
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1373:
	ret
	.cfi_endproc
.LFE4062:
	.size	_ZN6icu_676number4impl14SymbolsWrapperD2Ev, .-_ZN6icu_676number4impl14SymbolsWrapperD2Ev
	.globl	_ZN6icu_676number4impl14SymbolsWrapperD1Ev
	.set	_ZN6icu_676number4impl14SymbolsWrapperD1Ev,_ZN6icu_676number4impl14SymbolsWrapperD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SymbolsWrapper5setToERKNS_20DecimalFormatSymbolsE
	.type	_ZN6icu_676number4impl14SymbolsWrapper5setToERKNS_20DecimalFormatSymbolsE, @function
_ZN6icu_676number4impl14SymbolsWrapper5setToERKNS_20DecimalFormatSymbolsE:
.LFB4064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L1387
	cmpl	$2, %eax
	jne	.L1388
.L1387:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1388
	movq	(%rdi), %rax
	call	*8(%rax)
.L1388:
	movl	$1, (%rbx)
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1389
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1389:
	movq	%r12, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4064:
	.size	_ZN6icu_676number4impl14SymbolsWrapper5setToERKNS_20DecimalFormatSymbolsE, .-_ZN6icu_676number4impl14SymbolsWrapper5setToERKNS_20DecimalFormatSymbolsE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SymbolsWrapper5setToEPKNS_15NumberingSystemE
	.type	_ZN6icu_676number4impl14SymbolsWrapper5setToEPKNS_15NumberingSystemE, @function
_ZN6icu_676number4impl14SymbolsWrapper5setToEPKNS_15NumberingSystemE:
.LFB4065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rdi), %eax
	movq	%rdi, %rbx
	cmpl	$1, %eax
	je	.L1404
	cmpl	$2, %eax
	jne	.L1405
.L1404:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1405
	movq	(%rdi), %rax
	call	*8(%rax)
.L1405:
	movq	%r12, 8(%rbx)
	movl	$2, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4065:
	.size	_ZN6icu_676number4impl14SymbolsWrapper5setToEPKNS_15NumberingSystemE, .-_ZN6icu_676number4impl14SymbolsWrapper5setToEPKNS_15NumberingSystemE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SymbolsWrapper10doCopyFromERKS2_
	.type	_ZN6icu_676number4impl14SymbolsWrapper10doCopyFromERKS2_, @function
_ZN6icu_676number4impl14SymbolsWrapper10doCopyFromERKS2_:
.LFB4066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	cmpl	$1, %eax
	je	.L1416
	cmpl	$2, %eax
	jne	.L1415
	cmpq	$0, 8(%rsi)
	je	.L1421
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1422
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	movq	%r13, 8(%r12)
.L1415:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1416:
	.cfi_restore_state
	cmpq	$0, 8(%rsi)
	jne	.L1430
.L1421:
	movq	$0, 8(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1430:
	.cfi_restore_state
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1422
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1422:
	movq	%r13, 8(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4066:
	.size	_ZN6icu_676number4impl14SymbolsWrapper10doCopyFromERKS2_, .-_ZN6icu_676number4impl14SymbolsWrapper10doCopyFromERKS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SymbolsWrapper10doMoveFromEOS2_
	.type	_ZN6icu_676number4impl14SymbolsWrapper10doMoveFromEOS2_, @function
_ZN6icu_676number4impl14SymbolsWrapper10doMoveFromEOS2_:
.LFB4067:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	cmpl	$1, %eax
	je	.L1433
	cmpl	$2, %eax
	je	.L1433
	ret
	.p2align 4,,10
	.p2align 3
.L1433:
	movq	8(%rsi), %rax
	movq	%rax, 8(%rdi)
	movq	$0, 8(%rsi)
	ret
	.cfi_endproc
.LFE4067:
	.size	_ZN6icu_676number4impl14SymbolsWrapper10doMoveFromEOS2_, .-_ZN6icu_676number4impl14SymbolsWrapper10doMoveFromEOS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SymbolsWrapper9doCleanupEv
	.type	_ZN6icu_676number4impl14SymbolsWrapper9doCleanupEv, @function
_ZN6icu_676number4impl14SymbolsWrapper9doCleanupEv:
.LFB4068:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L1437
	cmpl	$2, %eax
	je	.L1437
	ret
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1435
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1435:
	ret
	.cfi_endproc
.LFE4068:
	.size	_ZN6icu_676number4impl14SymbolsWrapper9doCleanupEv, .-_ZN6icu_676number4impl14SymbolsWrapper9doCleanupEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl14SymbolsWrapper22isDecimalFormatSymbolsEv
	.type	_ZNK6icu_676number4impl14SymbolsWrapper22isDecimalFormatSymbolsEv, @function
_ZNK6icu_676number4impl14SymbolsWrapper22isDecimalFormatSymbolsEv:
.LFB4069:
	.cfi_startproc
	endbr64
	cmpl	$1, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE4069:
	.size	_ZNK6icu_676number4impl14SymbolsWrapper22isDecimalFormatSymbolsEv, .-_ZNK6icu_676number4impl14SymbolsWrapper22isDecimalFormatSymbolsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl14SymbolsWrapper17isNumberingSystemEv
	.type	_ZNK6icu_676number4impl14SymbolsWrapper17isNumberingSystemEv, @function
_ZNK6icu_676number4impl14SymbolsWrapper17isNumberingSystemEv:
.LFB4070:
	.cfi_startproc
	endbr64
	cmpl	$2, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE4070:
	.size	_ZNK6icu_676number4impl14SymbolsWrapper17isNumberingSystemEv, .-_ZNK6icu_676number4impl14SymbolsWrapper17isNumberingSystemEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl14SymbolsWrapper23getDecimalFormatSymbolsEv
	.type	_ZNK6icu_676number4impl14SymbolsWrapper23getDecimalFormatSymbolsEv, @function
_ZNK6icu_676number4impl14SymbolsWrapper23getDecimalFormatSymbolsEv:
.LFB4071:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE4071:
	.size	_ZNK6icu_676number4impl14SymbolsWrapper23getDecimalFormatSymbolsEv, .-_ZNK6icu_676number4impl14SymbolsWrapper23getDecimalFormatSymbolsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl14SymbolsWrapper18getNumberingSystemEv
	.type	_ZNK6icu_676number4impl14SymbolsWrapper18getNumberingSystemEv, @function
_ZNK6icu_676number4impl14SymbolsWrapper18getNumberingSystemEv:
.LFB4072:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE4072:
	.size	_ZNK6icu_676number4impl14SymbolsWrapper18getNumberingSystemEv, .-_ZNK6icu_676number4impl14SymbolsWrapper18getNumberingSystemEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode
	.type	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode, @function
_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode:
.LFB4077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	448(%rdi), %eax
	movq	%rsi, %rbx
	movl	208(%rdi), %edx
	testl	%eax, %eax
	js	.L1452
	cmpl	%edx, %eax
	jg	.L1453
	testl	%edx, %edx
	jne	.L1484
.L1453:
	testl	%edx, %edx
	jle	.L1460
	cmpl	%edx, %eax
	je	.L1461
.L1460:
	leaq	8(%rbx), %r13
	leaq	152(%rbx), %rsi
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN6icu_676number4impl19NumberFormatterImpl12formatStaticERKNS1_10MacroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderER10UErrorCode@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L1485
.L1451:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1452:
	.cfi_restore_state
	testl	%edx, %edx
	jle	.L1456
	cmpl	%eax, %edx
	je	.L1461
.L1456:
	movq	440(%r12), %rdi
	leaq	8(%rbx), %r13
	leaq	152(%rbx), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	_ZNK6icu_676number4impl19NumberFormatterImpl6formatERNS1_15DecimalQuantityERNS_22FormattedStringBuilderER10UErrorCode@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L1451
.L1485:
	popq	%rbx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6722FormattedStringBuilder15writeTerminatorER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L1484:
	.cfi_restore_state
	leaq	448(%rdi), %rcx
	movl	$1, %eax
	lock xaddl	%eax, (%rcx)
	movl	208(%rdi), %edx
	addl	$1, %eax
	cmpl	%edx, %eax
	jne	.L1454
	testl	%edx, %edx
	jle	.L1454
	.p2align 4,,10
	.p2align 3
.L1461:
	movl	$544, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1455
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_676number4impl19NumberFormatterImplC1ERKNS1_10MacroPropsER10UErrorCode@PLT
	movq	%r13, 440(%r12)
	movl	$-2147483648, 448(%r12)
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1454:
	testl	%eax, %eax
	jns	.L1460
	jmp	.L1456
.L1455:
	movl	$7, (%r14)
	jmp	.L1460
	.cfi_endproc
.LFE4077:
	.size	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode, .-_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number24LocalizedNumberFormatter9formatIntElR10UErrorCode
	.type	_ZNK6icu_676number24LocalizedNumberFormatter9formatIntElR10UErrorCode, @function
_ZNK6icu_676number24LocalizedNumberFormatter9formatIntElR10UErrorCode:
.LFB4073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %edi
	testl	%edi, %edi
	jle	.L1487
	movq	$0, 8(%r12)
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movq	%rax, (%r12)
	movl	$1, 16(%r12)
.L1486:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1487:
	.cfi_restore_state
	movl	$224, %edi
	movq	%rsi, %r13
	movq	%rdx, %r14
	movq	%rcx, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1489
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	movq	%rax, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	152(%r15), %rdi
	movq	%rax, (%r15)
	movq	%rdi, -56(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	-56(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_676number4impl15DecimalQuantity9setToLongEl@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1492
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movl	(%rbx), %eax
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rbx
	movq	$0, 8(%r12)
	movq	%rbx, (%r12)
	movl	%eax, 16(%r12)
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1492:
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movq	%r15, 8(%r12)
	movq	%rax, (%r12)
	movl	$0, 16(%r12)
	jmp	.L1486
.L1489:
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movl	$7, (%rbx)
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movl	$7, 16(%r12)
	jmp	.L1486
	.cfi_endproc
.LFE4073:
	.size	_ZNK6icu_676number24LocalizedNumberFormatter9formatIntElR10UErrorCode, .-_ZNK6icu_676number24LocalizedNumberFormatter9formatIntElR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number24LocalizedNumberFormatter12formatDoubleEdR10UErrorCode
	.type	_ZNK6icu_676number24LocalizedNumberFormatter12formatDoubleEdR10UErrorCode, @function
_ZNK6icu_676number24LocalizedNumberFormatter12formatDoubleEdR10UErrorCode:
.LFB4074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L1494
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$1, 16(%rdi)
.L1493:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1494:
	.cfi_restore_state
	movl	$224, %edi
	movq	%rsi, %r13
	movq	%rdx, %rbx
	movsd	%xmm0, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1496
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	movq	%rax, %rdi
	leaq	152(%r14), %r15
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movsd	-56(%rbp), %xmm0
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1499
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	movl	(%rbx), %eax
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rcx
	movq	$0, 8(%r12)
	movq	%rcx, (%r12)
	movl	%eax, 16(%r12)
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1499:
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movq	%r14, 8(%r12)
	movq	%rax, (%r12)
	movl	$0, 16(%r12)
	jmp	.L1493
.L1496:
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movl	$7, (%rbx)
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movl	$7, 16(%r12)
	jmp	.L1493
	.cfi_endproc
.LFE4074:
	.size	_ZNK6icu_676number24LocalizedNumberFormatter12formatDoubleEdR10UErrorCode, .-_ZNK6icu_676number24LocalizedNumberFormatter12formatDoubleEdR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number24LocalizedNumberFormatter13formatDecimalENS_11StringPieceER10UErrorCode
	.type	_ZNK6icu_676number24LocalizedNumberFormatter13formatDecimalENS_11StringPieceER10UErrorCode, @function
_ZNK6icu_676number24LocalizedNumberFormatter13formatDecimalENS_11StringPieceER10UErrorCode:
.LFB4075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%r8), %edx
	testl	%edx, %edx
	jle	.L1501
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$1, 16(%rdi)
.L1500:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1501:
	.cfi_restore_state
	movl	$224, %edi
	movq	%rax, -56(%rbp)
	movq	%rsi, %r14
	movq	%rcx, %r15
	movq	%r8, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1503
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	movq	%rax, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	152(%r13), %rdi
	movq	%rax, 0(%r13)
	movq	%rdi, -64(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%r9, %rsi
	call	_ZN6icu_676number4impl15DecimalQuantity14setToDecNumberENS_11StringPieceER10UErrorCode@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1506
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	movl	(%rbx), %eax
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rcx
	movq	$0, 8(%r12)
	movq	%rcx, (%r12)
	movl	%eax, 16(%r12)
	jmp	.L1500
	.p2align 4,,10
	.p2align 3
.L1506:
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movq	%r13, 8(%r12)
	movq	%rax, (%r12)
	movl	$0, 16(%r12)
	jmp	.L1500
.L1503:
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movl	$7, (%rbx)
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movl	$7, 16(%r12)
	jmp	.L1500
	.cfi_endproc
.LFE4075:
	.size	_ZNK6icu_676number24LocalizedNumberFormatter13formatDecimalENS_11StringPieceER10UErrorCode, .-_ZNK6icu_676number24LocalizedNumberFormatter13formatDecimalENS_11StringPieceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number24LocalizedNumberFormatter21formatDecimalQuantityERKNS0_4impl15DecimalQuantityER10UErrorCode
	.type	_ZNK6icu_676number24LocalizedNumberFormatter21formatDecimalQuantityERKNS0_4impl15DecimalQuantityER10UErrorCode, @function
_ZNK6icu_676number24LocalizedNumberFormatter21formatDecimalQuantityERKNS0_4impl15DecimalQuantityER10UErrorCode:
.LFB4076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %edi
	testl	%edi, %edi
	jle	.L1508
	movq	$0, 8(%r12)
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movq	%rax, (%r12)
	movl	$1, 16(%r12)
.L1507:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1508:
	.cfi_restore_state
	movl	$224, %edi
	movq	%rsi, %r13
	movq	%rdx, %r14
	movq	%rcx, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1510
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	movq	%rax, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	152(%r15), %rdi
	movq	%rax, (%r15)
	movq	%rdi, -56(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	-56(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_676number4impl15DecimalQuantityaSERKS2_@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1513
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movl	(%rbx), %eax
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rbx
	movq	$0, 8(%r12)
	movq	%rbx, (%r12)
	movl	%eax, 16(%r12)
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1513:
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movq	%r15, 8(%r12)
	movq	%rax, (%r12)
	movl	$0, 16(%r12)
	jmp	.L1507
.L1510:
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movl	$7, (%rbx)
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movl	$7, 16(%r12)
	jmp	.L1507
	.cfi_endproc
.LFE4076:
	.size	_ZNK6icu_676number24LocalizedNumberFormatter21formatDecimalQuantityERKNS0_4impl15DecimalQuantityER10UErrorCode, .-_ZNK6icu_676number24LocalizedNumberFormatter21formatDecimalQuantityERKNS0_4impl15DecimalQuantityER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number24LocalizedNumberFormatter15computeCompiledER10UErrorCode
	.type	_ZNK6icu_676number24LocalizedNumberFormatter15computeCompiledER10UErrorCode, @function
_ZNK6icu_676number24LocalizedNumberFormatter15computeCompiledER10UErrorCode:
.LFB4079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	448(%rdi), %eax
	testl	%eax, %eax
	js	.L1527
	leaq	448(%rdi), %rdx
	movl	208(%rdi), %edi
	testl	%edi, %edi
	je	.L1516
	cmpl	%eax, %edi
	jge	.L1528
.L1516:
	testl	%edi, %edi
	setg	%cl
	cmpl	%edi, %eax
	sete	%r12b
	andb	%cl, %r12b
	jne	.L1529
.L1517:
	shrl	$31, %eax
	movl	%eax, %r12d
.L1514:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1528:
	.cfi_restore_state
	movl	$1, %eax
	lock xaddl	%eax, (%rdx)
	addl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1527:
	movl	208(%rbx), %edi
	testl	%edi, %edi
	setg	%cl
	cmpl	%edi, %eax
	sete	%r12b
	andb	%cl, %r12b
	je	.L1517
.L1529:
	movl	$544, %edi
	movq	%rsi, -40(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-40(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1518
	movq	%rsi, %rdx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_676number4impl19NumberFormatterImplC1ERKNS1_10MacroPropsER10UErrorCode@PLT
	movq	%r13, 440(%rbx)
	movl	%r12d, %eax
	movl	$-2147483648, 448(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1518:
	.cfi_restore_state
	movl	$7, (%rsi)
	xorl	%r12d, %r12d
	jmp	.L1514
	.cfi_endproc
.LFE4079:
	.size	_ZNK6icu_676number24LocalizedNumberFormatter15computeCompiledER10UErrorCode, .-_ZNK6icu_676number24LocalizedNumberFormatter15computeCompiledER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number24LocalizedNumberFormatter11getCompiledEv
	.type	_ZNK6icu_676number24LocalizedNumberFormatter11getCompiledEv, @function
_ZNK6icu_676number24LocalizedNumberFormatter11getCompiledEv:
.LFB4080:
	.cfi_startproc
	endbr64
	movq	440(%rdi), %rax
	ret
	.cfi_endproc
.LFE4080:
	.size	_ZNK6icu_676number24LocalizedNumberFormatter11getCompiledEv, .-_ZNK6icu_676number24LocalizedNumberFormatter11getCompiledEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number24LocalizedNumberFormatter12getCallCountEv
	.type	_ZNK6icu_676number24LocalizedNumberFormatter12getCallCountEv, @function
_ZNK6icu_676number24LocalizedNumberFormatter12getCallCountEv:
.LFB4081:
	.cfi_startproc
	endbr64
	movl	448(%rdi), %eax
	ret
	.cfi_endproc
.LFE4081:
	.size	_ZNK6icu_676number24LocalizedNumberFormatter12getCallCountEv, .-_ZNK6icu_676number24LocalizedNumberFormatter12getCallCountEv
	.section	.text._ZN6icu_676number4impl24ImmutablePatternModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl24ImmutablePatternModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.type	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev, @function
_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev:
.LFB4772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1533
	movq	(%rdi), %rax
	call	*8(%rax)
.L1533:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.cfi_endproc
.LFE4772:
	.size	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev, .-_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD1Ev
	.set	_ZN6icu_676number4impl24ImmutablePatternModifierD1Ev,_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number15NumberFormatter10withLocaleERKNS_6LocaleE
	.type	_ZN6icu_676number15NumberFormatter10withLocaleERKNS_6LocaleE, @function
_ZN6icu_676number15NumberFormatter10withLocaleERKNS_6LocaleE:
.LFB3968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-296(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-472(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-496(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movabsq	$30064771077, %rbx
	subq	$504, %rsp
	movq	%rsi, -536(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$2, -508(%rbp)
	movq	$0, -504(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-1, %edx
	movl	$-3, %eax
	movq	%r15, %rdi
	pxor	%xmm0, %xmm0
	movw	%dx, -388(%rbp)
	movw	%ax, -412(%rbp)
	movaps	%xmm0, -320(%rbp)
	movq	%rbx, -360(%rbp)
	movl	$0, -448(%rbp)
	movl	$4, -416(%rbp)
	movl	$-2, -400(%rbp)
	movb	$0, -380(%rbp)
	movl	$0, -376(%rbp)
	movq	$0, -368(%rbp)
	movl	$2, -352(%rbp)
	movl	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movl	$0, -328(%rbp)
	movl	$3, -304(%rbp)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	$2, 4(%r14)
	leaq	16(%r14), %r10
	movl	$0, 12(%r14)
	movq	%r10, %rdi
	movq	%r10, -528(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	40(%r14), %r9
	movq	%r9, %rdi
	movq	%r9, -520(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %ecx
	movl	$-1, %esi
	movq	%rbx, 152(%r14)
	movw	%cx, 100(%r14)
	pxor	%xmm0, %xmm0
	leaq	216(%r14), %rbx
	movw	%si, 124(%r14)
	movq	%rbx, %rdi
	movl	$0, 64(%r14)
	movl	$4, 96(%r14)
	movl	$-2, 112(%r14)
	movb	$0, 132(%r14)
	movl	$0, 136(%r14)
	movq	$0, 144(%r14)
	movl	$2, 160(%r14)
	movl	$0, 168(%r14)
	movq	$0, 176(%r14)
	movl	$0, 184(%r14)
	movl	$3, 208(%r14)
	movups	%xmm0, 192(%r14)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	-508(%rbp), %rax
	movq	-528(%rbp), %r10
	movq	%r12, %rsi
	movq	$0, 440(%r14)
	movq	%rax, 4(%r14)
	movl	-500(%rbp), %eax
	movq	%r10, %rdi
	movq	$0, 448(%r14)
	movl	%eax, 12(%r14)
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	-520(%rbp), %r9
	movq	%r13, %rsi
	movq	%r9, %rdi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	-432(%rbp), %rax
	movdqa	-448(%rbp), %xmm1
	movq	%rax, 80(%r14)
	movl	-424(%rbp), %eax
	movups	%xmm1, 64(%r14)
	movl	%eax, 88(%r14)
	movl	-416(%rbp), %eax
	movl	%eax, 96(%r14)
	movq	-412(%rbp), %rax
	movq	%rax, 100(%r14)
	movl	-404(%rbp), %eax
	movl	%eax, 108(%r14)
	movq	-400(%rbp), %rax
	movq	%rax, 112(%r14)
	movl	-392(%rbp), %eax
	movl	%eax, 120(%r14)
	movq	-388(%rbp), %rax
	movq	%rax, 124(%r14)
	movzbl	-380(%rbp), %eax
	movb	%al, 132(%r14)
	movl	136(%r14), %eax
	cmpl	$1, %eax
	je	.L1540
	cmpl	$2, %eax
	jne	.L1541
.L1540:
	movq	144(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1541
	movq	(%rdi), %rax
	call	*8(%rax)
.L1541:
	movl	-376(%rbp), %eax
	movl	%eax, 136(%r14)
	cmpl	$1, %eax
	je	.L1543
	cmpl	$2, %eax
	je	.L1543
.L1544:
	movq	-360(%rbp), %rax
	leaq	-344(%rbp), %r9
	leaq	168(%r14), %rdi
	movq	%r9, %rsi
	movq	%r9, -520(%rbp)
	movq	%rax, 152(%r14)
	movl	-352(%rbp), %eax
	movl	%eax, 160(%r14)
	call	_ZN6icu_676number5ScaleaSEOS1_@PLT
	movl	-304(%rbp), %eax
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movdqa	-320(%rbp), %xmm2
	movl	%eax, 208(%r14)
	movups	%xmm2, 192(%r14)
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	-536(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-520(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movl	-376(%rbp), %eax
	cmpl	$1, %eax
	je	.L1546
	cmpl	$2, %eax
	je	.L1546
.L1547:
	movq	%r13, %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1566
	addq	$504, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1546:
	.cfi_restore_state
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1547
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1547
	.p2align 4,,10
	.p2align 3
.L1543:
	movq	-368(%rbp), %rax
	movq	$0, -368(%rbp)
	movq	%rax, 144(%r14)
	jmp	.L1544
.L1566:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3968:
	.size	_ZN6icu_676number15NumberFormatter10withLocaleERKNS_6LocaleE, .-_ZN6icu_676number15NumberFormatter10withLocaleERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number24LocalizedNumberFormatter12getAffixImplEbbRNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_676number24LocalizedNumberFormatter12getAffixImplEbbRNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_676number24LocalizedNumberFormatter12getAffixImplEbbRNS_13UnicodeStringER10UErrorCode:
.LFB4078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$296, %rsp
	movl	%esi, -324(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6722FormattedStringBuilderC1Ev@PLT
	cmpb	$1, %r15b
	movq	%rbx, %rsi
	movq	%r14, %rdi
	sbbl	%r15d, %r15d
	andl	$3, %r15d
	call	_ZNK6icu_676number24LocalizedNumberFormatter15computeCompiledER10UErrorCode
	testb	%al, %al
	je	.L1569
	movq	440(%r14), %rdi
	movq	%rbx, %r8
	movq	%r13, %rcx
	movl	%r15d, %esi
	movl	$5, %edx
	call	_ZNK6icu_676number4impl19NumberFormatterImpl15getPrefixSuffixENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode@PLT
	movl	%eax, %r14d
.L1570:
	movzwl	8(%r12), %edx
	leaq	-320(%rbp), %r15
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	cmpb	$0, -324(%rbp)
	movw	%ax, 8(%r12)
	je	.L1572
	call	_ZNK6icu_6722FormattedStringBuilder19toTempUnicodeStringEv@PLT
	leaq	-256(%rbp), %rbx
	movl	%r14d, %ecx
	xorl	%edx, %edx
.L1584:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movswl	-248(%rbp), %ecx
	testw	%cx, %cx
	js	.L1576
	sarl	$5, %ecx
.L1577:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6722FormattedStringBuilderD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1585
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1569:
	.cfi_restore_state
	movq	%r14, %rdi
	movq	%rbx, %r8
	movq	%r13, %rcx
	movl	$5, %edx
	movl	%r15d, %esi
	call	_ZN6icu_676number4impl19NumberFormatterImpl21getPrefixSuffixStaticERKNS1_10MacroPropsENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode@PLT
	movl	%eax, %r14d
	jmp	.L1570
	.p2align 4,,10
	.p2align 3
.L1572:
	call	_ZNK6icu_6722FormattedStringBuilder19toTempUnicodeStringEv@PLT
	movq	%r13, %rdi
	leaq	-256(%rbp), %rbx
	call	_ZNK6icu_6722FormattedStringBuilder6lengthEv@PLT
	movl	%r14d, %edx
	subl	%r14d, %eax
	movl	%eax, %ecx
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1576:
	movl	-244(%rbp), %ecx
	jmp	.L1577
.L1585:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4078:
	.size	_ZNK6icu_676number24LocalizedNumberFormatter12getAffixImplEbbRNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_676number24LocalizedNumberFormatter12getAffixImplEbbRNS_13UnicodeStringER10UErrorCode
	.section	.text._ZN6icu_676number4impl22MutablePatternModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl22MutablePatternModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl22MutablePatternModifierD2Ev
	.type	_ZN6icu_676number4impl22MutablePatternModifierD2Ev, @function
_ZN6icu_676number4impl22MutablePatternModifierD2Ev:
.LFB4765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$328, %rdi
	subq	$8, %rsp
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r12)
	jne	.L1589
.L1587:
	leaq	80(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1589:
	.cfi_restore_state
	movq	112(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L1587
	.cfi_endproc
.LFE4765:
	.size	_ZN6icu_676number4impl22MutablePatternModifierD2Ev, .-_ZN6icu_676number4impl22MutablePatternModifierD2Ev
	.weak	_ZN6icu_676number4impl22MutablePatternModifierD1Ev
	.set	_ZN6icu_676number4impl22MutablePatternModifierD1Ev,_ZN6icu_676number4impl22MutablePatternModifierD2Ev
	.section	.text._ZN6icu_676number4impl22MutablePatternModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl22MutablePatternModifierD5Ev,comdat
	.p2align 4
	.weak	_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev
	.type	_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev, @function
_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev:
.LFB5650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$312, %rdi
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	224(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	160(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 108(%rbx)
	jne	.L1593
.L1591:
	leaq	64(%rbx), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L1593:
	.cfi_restore_state
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L1591
	.cfi_endproc
.LFE5650:
	.size	_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev, .-_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev
	.p2align 4
	.weak	_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev
	.type	_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev, @function
_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev:
.LFB5651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 13, -24
	leaq	-8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$320, %rdi
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	232(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	168(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 116(%r12)
	jne	.L1597
.L1595:
	leaq	72(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L1597:
	.cfi_restore_state
	movq	104(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L1595
	.cfi_endproc
.LFE5651:
	.size	_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev, .-_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl22MutablePatternModifierD0Ev
	.type	_ZN6icu_676number4impl22MutablePatternModifierD0Ev, @function
_ZN6icu_676number4impl22MutablePatternModifierD0Ev:
.LFB4767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$328, %rdi
	subq	$8, %rsp
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r12)
	jne	.L1601
.L1599:
	leaq	80(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L1601:
	.cfi_restore_state
	movq	112(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L1599
	.cfi_endproc
.LFE4767:
	.size	_ZN6icu_676number4impl22MutablePatternModifierD0Ev, .-_ZN6icu_676number4impl22MutablePatternModifierD0Ev
	.section	.text._ZN6icu_676number4impl22MutablePatternModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl22MutablePatternModifierD5Ev,comdat
	.p2align 4
	.weak	_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev
	.type	_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev, @function
_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev:
.LFB5652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$312, %rdi
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	224(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	160(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 108(%rbx)
	jne	.L1605
.L1603:
	leaq	64(%rbx), %rdi
	leaq	-16(%rbx), %r12
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1605:
	.cfi_restore_state
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L1603
	.cfi_endproc
.LFE5652:
	.size	_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev, .-_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev
	.p2align 4
	.weak	_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev
	.type	_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev, @function
_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev:
.LFB5653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$320, %rdi
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	232(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	168(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 116(%r12)
	jne	.L1609
.L1607:
	leaq	72(%r12), %rdi
	leaq	-8(%r12), %r13
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1609:
	.cfi_restore_state
	movq	104(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L1607
	.cfi_endproc
.LFE5653:
	.size	_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev, .-_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev
	.section	.text._ZN6icu_676number4impl17ParsedPatternInfoD2Ev,"axG",@progbits,_ZN6icu_676number4impl17ParsedPatternInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl17ParsedPatternInfoD2Ev
	.type	_ZN6icu_676number4impl17ParsedPatternInfoD2Ev, @function
_ZN6icu_676number4impl17ParsedPatternInfoD2Ev:
.LFB4751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	296(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -296(%rdi)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r12), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	.cfi_endproc
.LFE4751:
	.size	_ZN6icu_676number4impl17ParsedPatternInfoD2Ev, .-_ZN6icu_676number4impl17ParsedPatternInfoD2Ev
	.weak	_ZN6icu_676number4impl17ParsedPatternInfoD1Ev
	.set	_ZN6icu_676number4impl17ParsedPatternInfoD1Ev,_ZN6icu_676number4impl17ParsedPatternInfoD2Ev
	.section	.text._ZN6icu_676number4impl17ParsedPatternInfoD0Ev,"axG",@progbits,_ZN6icu_676number4impl17ParsedPatternInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev
	.type	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev, @function
_ZN6icu_676number4impl17ParsedPatternInfoD0Ev:
.LFB4753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	296(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -296(%rdi)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r12), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4753:
	.size	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev, .-_ZN6icu_676number4impl17ParsedPatternInfoD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number24LocalizedNumberFormatteraSERKS1_
	.type	_ZN6icu_676number24LocalizedNumberFormatteraSERKS1_, @function
_ZN6icu_676number24LocalizedNumberFormatteraSERKS1_:
.LFB4022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movdqu	64(%rbx), %xmm1
	movups	%xmm1, 64(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 132(%r12)
	cmpq	%rbx, %r12
	je	.L1615
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L1617
	cmpl	$2, %eax
	jne	.L1618
.L1617:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1618
	movq	(%rdi), %rax
	call	*8(%rax)
.L1618:
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1619
	cmpl	$2, %eax
	je	.L1620
.L1615:
	movl	152(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 152(%r12)
	movl	156(%rbx), %eax
	movl	%eax, 156(%r12)
	movl	160(%rbx), %eax
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movups	%xmm2, 192(%r12)
	movl	208(%rbx), %eax
	movl	%eax, 208(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movl	$0, 448(%r12)
	movq	440(%r12), %r13
	testq	%r13, %r13
	je	.L1625
	leaq	480(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	416(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 364(%r13)
	jne	.L1685
.L1626:
	leaq	320(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	304(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1627
	movq	(%rdi), %rax
	call	*8(%rax)
.L1627:
	movq	296(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1628
	movq	(%rdi), %rax
	call	*8(%rax)
.L1628:
	movq	288(%r13), %r14
	testq	%r14, %r14
	je	.L1629
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1630
	movq	8(%r14), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L1631
	movq	(%rdi), %rax
	call	*8(%rax)
.L1631:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1629:
	movq	280(%r13), %r14
	testq	%r14, %r14
	je	.L1632
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1633
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r14), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm3
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r14)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r14)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r14)
	jne	.L1686
.L1634:
	leaq	80(%r14), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r14), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1632:
	movq	272(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1635
	movq	(%rdi), %rax
	call	*8(%rax)
.L1635:
	movq	264(%r13), %r14
	testq	%r14, %r14
	je	.L1636
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1637
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r14), %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r14), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1636:
	movq	256(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1638
	movq	(%rdi), %rax
	call	*8(%rax)
.L1638:
	movq	248(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1639
	movq	(%rdi), %rax
	call	*8(%rax)
.L1639:
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	leaq	200(%r13), %r14
	movq	%rax, 8(%r13)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	leaq	208(%r13), %rdi
	movq	%rax, 200(%r13)
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r14, %rdi
	leaq	8(%r13), %r14
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%rbx, 184(%r13)
	leaq	184(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 168(%r13)
	leaq	168(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	144(%r13), %rdi
	movq	%rax, 144(%r13)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1625:
	movq	$0, 440(%r12)
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1620:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	je	.L1623
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1624
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1619:
	cmpq	$0, 144(%rbx)
	je	.L1623
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1624
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L1624:
	movq	%r13, 144(%r12)
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1623:
	movq	$0, 144(%r12)
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1685:
	movq	352(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L1626
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	112(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L1634
	.p2align 4,,10
	.p2align 3
.L1630:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1633:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1637:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1636
	.cfi_endproc
.LFE4022:
	.size	_ZN6icu_676number24LocalizedNumberFormatteraSERKS1_, .-_ZN6icu_676number24LocalizedNumberFormatteraSERKS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number24LocalizedNumberFormatterD2Ev
	.type	_ZN6icu_676number24LocalizedNumberFormatterD2Ev, @function
_ZN6icu_676number24LocalizedNumberFormatterD2Ev:
.LFB4038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	440(%rdi), %r12
	testq	%r12, %r12
	je	.L1688
	leaq	480(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	416(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 364(%r12)
	jne	.L1745
.L1689:
	leaq	320(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	304(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1690
	movq	(%rdi), %rax
	call	*8(%rax)
.L1690:
	movq	296(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1691
	movq	(%rdi), %rax
	call	*8(%rax)
.L1691:
	movq	288(%r12), %r13
	testq	%r13, %r13
	je	.L1692
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1693
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L1694
	movq	(%rdi), %rax
	call	*8(%rax)
.L1694:
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1692:
	movq	280(%r12), %r13
	testq	%r13, %r13
	je	.L1695
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1696
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r13), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r13)
	jne	.L1746
.L1697:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r13), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1695:
	movq	272(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1698
	movq	(%rdi), %rax
	call	*8(%rax)
.L1698:
	movq	264(%r12), %r13
	testq	%r13, %r13
	je	.L1699
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1700
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r13), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1699:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1701
	movq	(%rdi), %rax
	call	*8(%rax)
.L1701:
	movq	248(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1702
	movq	(%rdi), %rax
	call	*8(%rax)
.L1702:
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	leaq	200(%r12), %r13
	movq	%rax, 8(%r12)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	leaq	208(%r12), %rdi
	movq	%rax, 200(%r12)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r13, %rdi
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %r13
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, 184(%r12)
	leaq	184(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, 168(%r12)
	leaq	168(%r12), %rdi
	leaq	8(%r12), %r13
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	144(%r12), %rdi
	movq	%rax, 144(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1688:
	leaq	216(%rbx), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	168(%rbx), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movl	136(%rbx), %eax
	cmpl	$1, %eax
	je	.L1704
	cmpl	$2, %eax
	jne	.L1705
.L1704:
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1705
	movq	(%rdi), %rax
	call	*8(%rax)
.L1705:
	leaq	40(%rbx), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	addq	$8, %rsp
	leaq	16(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711MeasureUnitD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1745:
	.cfi_restore_state
	movq	352(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1746:
	movq	112(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L1697
	.p2align 4,,10
	.p2align 3
.L1693:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L1696:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1695
	.p2align 4,,10
	.p2align 3
.L1700:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1699
	.cfi_endproc
.LFE4038:
	.size	_ZN6icu_676number24LocalizedNumberFormatterD2Ev, .-_ZN6icu_676number24LocalizedNumberFormatterD2Ev
	.globl	_ZN6icu_676number24LocalizedNumberFormatterD1Ev
	.set	_ZN6icu_676number24LocalizedNumberFormatterD1Ev,_ZN6icu_676number24LocalizedNumberFormatterD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number24LocalizedNumberFormatter5clearEv
	.type	_ZN6icu_676number24LocalizedNumberFormatter5clearEv, @function
_ZN6icu_676number24LocalizedNumberFormatter5clearEv:
.LFB4026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	$0, 448(%rdi)
	movq	440(%rdi), %r12
	testq	%r12, %r12
	je	.L1748
	leaq	480(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	416(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 364(%r12)
	jne	.L1794
.L1749:
	leaq	320(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	304(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1750
	movq	(%rdi), %rax
	call	*8(%rax)
.L1750:
	movq	296(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1751
	movq	(%rdi), %rax
	call	*8(%rax)
.L1751:
	movq	288(%r12), %r13
	testq	%r13, %r13
	je	.L1752
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1753
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L1754
	movq	(%rdi), %rax
	call	*8(%rax)
.L1754:
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1752:
	movq	280(%r12), %r13
	testq	%r13, %r13
	je	.L1755
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1756
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r13), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r13)
	jne	.L1795
.L1757:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r13), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1755:
	movq	272(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1758
	movq	(%rdi), %rax
	call	*8(%rax)
.L1758:
	movq	264(%r12), %r13
	testq	%r13, %r13
	je	.L1759
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1760
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r13), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1759:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1761
	movq	(%rdi), %rax
	call	*8(%rax)
.L1761:
	movq	248(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1762
	movq	(%rdi), %rax
	call	*8(%rax)
.L1762:
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	leaq	200(%r12), %r13
	movq	%rax, 8(%r12)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	leaq	208(%r12), %rdi
	movq	%rax, 200(%r12)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r13, %rdi
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %r13
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, 184(%r12)
	leaq	184(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, 168(%r12)
	leaq	168(%r12), %rdi
	leaq	8(%r12), %r13
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	144(%r12), %rdi
	movq	%rax, 144(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1748:
	movq	$0, 440(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1794:
	.cfi_restore_state
	movq	352(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L1795:
	movq	112(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L1757
	.p2align 4,,10
	.p2align 3
.L1753:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1752
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1755
	.p2align 4,,10
	.p2align 3
.L1760:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1759
	.cfi_endproc
.LFE4026:
	.size	_ZN6icu_676number24LocalizedNumberFormatter5clearEv, .-_ZN6icu_676number24LocalizedNumberFormatter5clearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number24LocalizedNumberFormatter13lnfMoveHelperEOS1_
	.type	_ZN6icu_676number24LocalizedNumberFormatter13lnfMoveHelperEOS1_, @function
_ZN6icu_676number24LocalizedNumberFormatter13lnfMoveHelperEOS1_:
.LFB4036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movl	$-2147483648, 448(%rdi)
	movq	440(%rdi), %r12
	testq	%r12, %r12
	je	.L1797
	leaq	480(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	416(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 364(%r12)
	jne	.L1843
.L1798:
	leaq	320(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	304(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1799
	movq	(%rdi), %rax
	call	*8(%rax)
.L1799:
	movq	296(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1800
	movq	(%rdi), %rax
	call	*8(%rax)
.L1800:
	movq	288(%r12), %r14
	testq	%r14, %r14
	je	.L1801
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1802
	movq	8(%r14), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L1803
	movq	(%rdi), %rax
	call	*8(%rax)
.L1803:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1801:
	movq	280(%r12), %r14
	testq	%r14, %r14
	je	.L1804
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1805
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r14), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r14)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r14)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r14)
	jne	.L1844
.L1806:
	leaq	80(%r14), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r14), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1804:
	movq	272(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1807
	movq	(%rdi), %rax
	call	*8(%rax)
.L1807:
	movq	264(%r12), %r14
	testq	%r14, %r14
	je	.L1808
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1809
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r14), %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r14), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1808:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1810
	movq	(%rdi), %rax
	call	*8(%rax)
.L1810:
	movq	248(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1811
	movq	(%rdi), %rax
	call	*8(%rax)
.L1811:
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	leaq	200(%r12), %r14
	movq	%rax, 8(%r12)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	leaq	208(%r12), %rdi
	movq	%rax, 200(%r12)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r14, %rdi
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %r14
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, 184(%r12)
	leaq	184(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, 168(%r12)
	leaq	168(%r12), %rdi
	leaq	8(%r12), %r14
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	144(%r12), %rdi
	movq	%rax, 144(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1797:
	movq	440(%rbx), %rax
	movq	%rax, 440(%r13)
	movl	$0, 448(%rbx)
	movq	$0, 440(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1843:
	.cfi_restore_state
	movq	352(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1844:
	movq	112(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1802:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1805:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1809:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1808
	.cfi_endproc
.LFE4036:
	.size	_ZN6icu_676number24LocalizedNumberFormatter13lnfMoveHelperEOS1_, .-_ZN6icu_676number24LocalizedNumberFormatter13lnfMoveHelperEOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number24LocalizedNumberFormatteraSEOS1_
	.type	_ZN6icu_676number24LocalizedNumberFormatteraSEOS1_, @function
_ZN6icu_676number24LocalizedNumberFormatteraSEOS1_:
.LFB4024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	4(%rsi), %rax
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movdqu	64(%rbx), %xmm1
	movups	%xmm1, 64(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 132(%r12)
	cmpq	%rbx, %r12
	je	.L1846
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L1848
	cmpl	$2, %eax
	jne	.L1849
.L1848:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1849
	movq	(%rdi), %rax
	call	*8(%rax)
.L1849:
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L1851
	cmpl	$2, %eax
	je	.L1851
.L1846:
	movl	152(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 152(%r12)
	movl	156(%rbx), %eax
	movl	%eax, 156(%r12)
	movl	160(%rbx), %eax
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleaSEOS1_@PLT
	movdqu	192(%rbx), %xmm2
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movups	%xmm2, 192(%r12)
	movl	208(%rbx), %eax
	movl	%eax, 208(%r12)
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	cmpq	$0, 440(%rbx)
	je	.L1852
	movl	$-2147483648, 448(%r12)
	movq	440(%r12), %r13
	testq	%r13, %r13
	je	.L1853
	leaq	480(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	416(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 364(%r13)
	jne	.L1953
.L1854:
	leaq	320(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	304(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1855
	movq	(%rdi), %rax
	call	*8(%rax)
.L1855:
	movq	296(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1856
	movq	(%rdi), %rax
	call	*8(%rax)
.L1856:
	movq	288(%r13), %r14
	testq	%r14, %r14
	je	.L1857
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1858
	movq	8(%r14), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L1859
	movq	(%rdi), %rax
	call	*8(%rax)
.L1859:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1857:
	movq	280(%r13), %r14
	testq	%r14, %r14
	je	.L1860
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1861
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r14), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm3
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r14)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r14)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r14)
	jne	.L1954
.L1862:
	leaq	80(%r14), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r14), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1860:
	movq	272(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1863
	movq	(%rdi), %rax
	call	*8(%rax)
.L1863:
	movq	264(%r13), %r14
	testq	%r14, %r14
	je	.L1864
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1865
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r14), %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r14), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1864:
	movq	256(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1866
	movq	(%rdi), %rax
	call	*8(%rax)
.L1866:
	movq	248(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1867
	movq	(%rdi), %rax
	call	*8(%rax)
.L1867:
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	leaq	200(%r13), %r14
	movq	%rax, 8(%r13)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	leaq	208(%r13), %rdi
	movq	%rax, 200(%r13)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r14, %rdi
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %r14
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, 184(%r13)
	leaq	184(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, 168(%r13)
	leaq	8(%r13), %r14
	leaq	168(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	144(%r13), %rdi
	movq	%rax, 144(%r13)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1853:
	movq	440(%rbx), %rax
	movq	%rax, 440(%r12)
	movq	%r12, %rax
	movl	$0, 448(%rbx)
	movq	$0, 440(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1851:
	.cfi_restore_state
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
	jmp	.L1846
	.p2align 4,,10
	.p2align 3
.L1852:
	movl	$0, 448(%r12)
	movq	440(%r12), %r13
	testq	%r13, %r13
	je	.L1869
	leaq	480(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	416(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 364(%r13)
	jne	.L1955
.L1870:
	leaq	320(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	304(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1871
	movq	(%rdi), %rax
	call	*8(%rax)
.L1871:
	movq	296(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1872
	movq	(%rdi), %rax
	call	*8(%rax)
.L1872:
	movq	288(%r13), %r14
	testq	%r14, %r14
	je	.L1873
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1874
	movq	8(%r14), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L1875
	movq	(%rdi), %rax
	call	*8(%rax)
.L1875:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1873:
	movq	280(%r13), %r14
	testq	%r14, %r14
	je	.L1876
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1877
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r14), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm4
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r14)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r14)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r14)
	jne	.L1956
.L1878:
	leaq	80(%r14), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r14), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1876:
	movq	272(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1879
	movq	(%rdi), %rax
	call	*8(%rax)
.L1879:
	movq	264(%r13), %r14
	testq	%r14, %r14
	je	.L1880
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1881
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r14), %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r14), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1880:
	movq	256(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1882
	movq	(%rdi), %rax
	call	*8(%rax)
.L1882:
	movq	248(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1883
	movq	(%rdi), %rax
	call	*8(%rax)
.L1883:
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	leaq	200(%r13), %r14
	movq	%rax, 8(%r13)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	leaq	208(%r13), %rdi
	movq	%rax, 200(%r13)
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r14, %rdi
	leaq	8(%r13), %r14
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%rbx, 184(%r13)
	leaq	184(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 168(%r13)
	leaq	168(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	144(%r13), %rdi
	movq	%rax, 144(%r13)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1869:
	movq	$0, 440(%r12)
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1954:
	.cfi_restore_state
	movq	112(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L1862
	.p2align 4,,10
	.p2align 3
.L1953:
	movq	352(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L1854
	.p2align 4,,10
	.p2align 3
.L1955:
	movq	352(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L1870
	.p2align 4,,10
	.p2align 3
.L1861:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1864
	.p2align 4,,10
	.p2align 3
.L1858:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1857
	.p2align 4,,10
	.p2align 3
.L1956:
	movq	112(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L1878
	.p2align 4,,10
	.p2align 3
.L1877:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1876
	.p2align 4,,10
	.p2align 3
.L1874:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1873
	.p2align 4,,10
	.p2align 3
.L1881:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1880
	.cfi_endproc
.LFE4024:
	.size	_ZN6icu_676number24LocalizedNumberFormatteraSEOS1_, .-_ZN6icu_676number24LocalizedNumberFormatteraSEOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number24LocalizedNumberFormatterC2EONS0_23NumberFormatterSettingsIS1_EE
	.type	_ZN6icu_676number24LocalizedNumberFormatterC2EONS0_23NumberFormatterSettingsIS1_EE, @function
_ZN6icu_676number24LocalizedNumberFormatterC2EONS0_23NumberFormatterSettingsIS1_EE:
.LFB4020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	addq	$16, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	-12(%rsi), %rax
	movq	%rdi, %rbx
	addq	$16, %rdi
	movq	%rax, -12(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	leaq	40(%r12), %rsi
	leaq	40(%rbx), %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movl	96(%r12), %eax
	movdqu	64(%r12), %xmm1
	movdqu	80(%r12), %xmm2
	movl	%eax, 96(%rbx)
	movq	100(%r12), %rax
	movups	%xmm1, 64(%rbx)
	movq	%rax, 100(%rbx)
	movl	108(%r12), %eax
	movups	%xmm2, 80(%rbx)
	movl	%eax, 108(%rbx)
	movq	112(%r12), %rax
	movq	%rax, 112(%rbx)
	movl	120(%r12), %eax
	movl	%eax, 120(%rbx)
	movq	124(%r12), %rax
	movq	%rax, 124(%rbx)
	movl	132(%r12), %eax
	movl	%eax, 132(%rbx)
	movl	136(%r12), %eax
	movl	%eax, 136(%rbx)
	cmpl	$1, %eax
	je	.L1959
	cmpl	$2, %eax
	jne	.L1960
.L1959:
	movq	144(%r12), %rax
	movq	%rax, 144(%rbx)
	movq	$0, 144(%r12)
.L1960:
	movl	160(%r12), %eax
	movq	152(%r12), %rdx
	leaq	168(%r12), %rsi
	leaq	168(%rbx), %rdi
	movq	%rdx, 152(%rbx)
	movl	%eax, 160(%rbx)
	call	_ZN6icu_676number5ScaleC1EOS1_@PLT
	movl	208(%r12), %eax
	movdqu	192(%r12), %xmm3
	leaq	216(%r12), %rsi
	leaq	216(%rbx), %rdi
	movl	%eax, 208(%rbx)
	movups	%xmm3, 192(%rbx)
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	cmpq	$0, 440(%r12)
	movq	$0, 440(%rbx)
	movq	$0, 448(%rbx)
	je	.L1957
	movl	$-2147483648, 448(%rbx)
	movq	440(%rbx), %r13
	testq	%r13, %r13
	je	.L1962
	leaq	480(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	416(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 364(%r13)
	jne	.L2008
.L1963:
	leaq	320(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	304(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1964
	movq	(%rdi), %rax
	call	*8(%rax)
.L1964:
	movq	296(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1965
	movq	(%rdi), %rax
	call	*8(%rax)
.L1965:
	movq	288(%r13), %r14
	testq	%r14, %r14
	je	.L1966
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1967
	movq	8(%r14), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L1968
	movq	(%rdi), %rax
	call	*8(%rax)
.L1968:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1966:
	movq	280(%r13), %r14
	testq	%r14, %r14
	je	.L1969
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1970
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r14), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm4
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r14)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r14)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r14)
	jne	.L2009
.L1971:
	leaq	80(%r14), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r14), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1969:
	movq	272(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1972
	movq	(%rdi), %rax
	call	*8(%rax)
.L1972:
	movq	264(%r13), %r14
	testq	%r14, %r14
	je	.L1973
	movq	(%r14), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1974
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r14), %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r14), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1973:
	movq	256(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1975
	movq	(%rdi), %rax
	call	*8(%rax)
.L1975:
	movq	248(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1976
	movq	(%rdi), %rax
	call	*8(%rax)
.L1976:
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	leaq	200(%r13), %r14
	movq	%rax, 8(%r13)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	leaq	208(%r13), %rdi
	movq	%rax, 200(%r13)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r14, %rdi
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %r14
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r14, 184(%r13)
	leaq	184(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, 168(%r13)
	leaq	8(%r13), %r14
	leaq	168(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	144(%r13), %rdi
	movq	%rax, 144(%r13)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1962:
	movq	440(%r12), %rax
	movq	%rax, 440(%rbx)
	movl	$0, 448(%r12)
	movq	$0, 440(%r12)
.L1957:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2008:
	.cfi_restore_state
	movq	352(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L1963
	.p2align 4,,10
	.p2align 3
.L2009:
	movq	112(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L1971
	.p2align 4,,10
	.p2align 3
.L1970:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1969
	.p2align 4,,10
	.p2align 3
.L1967:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L1974:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1973
	.cfi_endproc
.LFE4020:
	.size	_ZN6icu_676number24LocalizedNumberFormatterC2EONS0_23NumberFormatterSettingsIS1_EE, .-_ZN6icu_676number24LocalizedNumberFormatterC2EONS0_23NumberFormatterSettingsIS1_EE
	.globl	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	.set	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE,_ZN6icu_676number24LocalizedNumberFormatterC2EONS0_23NumberFormatterSettingsIS1_EE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8notationERKNS0_8NotationE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8notationERKNS0_8NotationE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8notationERKNS0_8NotationE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8notationERKNS0_8NotationE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8notationERKNS0_8NotationE:
.LFB4701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	movq	(%rbx), %rax
	movq	%rax, 4(%r12)
	movl	8(%rbx), %eax
	popq	%rbx
	movl	%eax, 12(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4701:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8notationERKNS0_8NotationE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8notationERKNS0_8NotationE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE:
.LFB4703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	leaq	16(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4703:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4unitERKNS_11MeasureUnitE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE:
.LFB4705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	testq	%r12, %r12
	je	.L2014
	leaq	16(%r13), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L2014:
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4705:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9adoptUnitEPNS_11MeasureUnitE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE:
.LFB4707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	leaq	40(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4707:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7perUnitERKNS_11MeasureUnitE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE:
.LFB4709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	testq	%r12, %r12
	je	.L2022
	leaq	40(%r13), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L2022:
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4709:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptPerUnitEPNS_11MeasureUnitE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE:
.LFB4711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	movdqu	(%rbx), %xmm0
	movups	%xmm0, 64(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	24(%rbx), %eax
	popq	%rbx
	movl	%eax, 88(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4711:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9precisionERKNS0_9PrecisionE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode:
.LFB4713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	movl	%ebx, 96(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4713:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy:
.LFB4715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	movl	%r13d, %edi
	call	_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy@PLT
	movq	%rax, 100(%r12)
	movq	%r12, %rax
	movl	%edx, 108(%r12)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4715:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE8groupingE23UNumberGroupingStrategy
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE:
.LFB4717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	movq	(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	8(%rbx), %eax
	popq	%rbx
	movb	%al, 132(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4717:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12integerWidthERKNS0_12IntegerWidthE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE:
.LFB4719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L2038
	cmpl	$2, %eax
	jne	.L2039
.L2038:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2039
	movq	(%rdi), %rax
	call	*8(%rax)
.L2039:
	movl	$1, 136(%r12)
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2040
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L2040:
	movq	%rbx, 144(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4719:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7symbolsERKNS_20DecimalFormatSymbolsE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE:
.LFB4721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L2055
	cmpl	$2, %eax
	jne	.L2056
.L2055:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2056
	movq	(%rdi), %rax
	call	*8(%rax)
.L2056:
	movq	%rbx, 144(%r12)
	movq	%r12, %rax
	popq	%rbx
	movl	$2, 136(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4721:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12adoptSymbolsEPNS_15NumberingSystemE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth:
.LFB4723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	movl	%ebx, 152(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4723:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9unitWidthE16UNumberUnitWidth
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4signE18UNumberSignDisplay,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4signE18UNumberSignDisplay,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4signE18UNumberSignDisplay
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4signE18UNumberSignDisplay, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4signE18UNumberSignDisplay:
.LFB4725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	movl	%ebx, 156(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4725:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4signE18UNumberSignDisplay, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE4signE18UNumberSignDisplay
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay:
.LFB4727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	movl	%ebx, 160(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4727:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7decimalE30UNumberDecimalSeparatorDisplay
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5scaleERKNS0_5ScaleE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5scaleERKNS0_5ScaleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5scaleERKNS0_5ScaleE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5scaleERKNS0_5ScaleE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5scaleERKNS0_5ScaleE:
.LFB4729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	leaq	168(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4729:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5scaleERKNS0_5ScaleE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5scaleERKNS0_5ScaleE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE:
.LFB4731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	movq	(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	8(%rbx), %eax
	popq	%rbx
	movl	%eax, 120(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4731:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE7paddingERKNS0_4impl6PadderE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9thresholdEi,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9thresholdEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9thresholdEi
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9thresholdEi, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9thresholdEi:
.LFB4733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	movl	%ebx, 208(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4733:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9thresholdEi, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE9thresholdEi
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE:
.LFB4735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	movq	4(%rbx), %rax
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	movq	%rax, 4(%r12)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movdqu	64(%rbx), %xmm0
	movups	%xmm0, 64(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 132(%r12)
	cmpq	%rbx, %r12
	je	.L2079
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L2081
	cmpl	$2, %eax
	jne	.L2082
.L2081:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2082
	movq	(%rdi), %rax
	call	*8(%rax)
.L2082:
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L2083
	cmpl	$2, %eax
	je	.L2084
.L2079:
	movl	152(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 152(%r12)
	movl	156(%rbx), %eax
	movl	%eax, 156(%r12)
	movl	160(%rbx), %eax
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	movdqu	192(%rbx), %xmm1
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movups	%xmm1, 192(%r12)
	movl	208(%rbx), %eax
	movl	%eax, 208(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2084:
	.cfi_restore_state
	cmpq	$0, 144(%rbx)
	je	.L2087
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2088
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715NumberingSystemC1ERKS0_@PLT
	jmp	.L2088
	.p2align 4,,10
	.p2align 3
.L2083:
	cmpq	$0, 144(%rbx)
	je	.L2087
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2088
	movq	144(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
.L2088:
	movq	%r13, 144(%r12)
	jmp	.L2079
	.p2align 4,,10
	.p2align 3
.L2087:
	movq	$0, 144(%r12)
	jmp	.L2079
	.cfi_endproc
.LFE4735:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE:
.LFB4737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	movq	4(%rbx), %rax
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	movq	%rax, 4(%r12)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	leaq	40(%rbx), %rsi
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movdqu	64(%rbx), %xmm0
	movups	%xmm0, 64(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	movl	88(%rbx), %eax
	movl	%eax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movq	100(%rbx), %rax
	movq	%rax, 100(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 120(%r12)
	movq	124(%rbx), %rax
	movq	%rax, 124(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 132(%r12)
	cmpq	%rbx, %r12
	je	.L2105
	movl	136(%r12), %eax
	cmpl	$1, %eax
	je	.L2107
	cmpl	$2, %eax
	jne	.L2108
.L2107:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2108
	movq	(%rdi), %rax
	call	*8(%rax)
.L2108:
	movl	136(%rbx), %eax
	movl	%eax, 136(%r12)
	cmpl	$1, %eax
	je	.L2110
	cmpl	$2, %eax
	je	.L2110
.L2105:
	movl	152(%rbx), %eax
	leaq	168(%rbx), %rsi
	leaq	168(%r12), %rdi
	movl	%eax, 152(%r12)
	movl	156(%rbx), %eax
	movl	%eax, 156(%r12)
	movl	160(%rbx), %eax
	movl	%eax, 160(%r12)
	call	_ZN6icu_676number5ScaleaSEOS1_@PLT
	movdqu	192(%rbx), %xmm1
	leaq	216(%rbx), %rsi
	leaq	216(%r12), %rdi
	movups	%xmm1, 192(%r12)
	movl	208(%rbx), %eax
	movl	%eax, 208(%r12)
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2110:
	.cfi_restore_state
	movq	144(%rbx), %rax
	movq	%rax, 144(%r12)
	movq	$0, 144(%rbx)
	jmp	.L2105
	.cfi_endproc
.LFE4737:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE
	.section	.text._ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5cloneEv,"axG",@progbits,_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5cloneEv
	.type	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5cloneEv, @function
_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5cloneEv:
.LFB4741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$456, %edi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2121
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
.L2121:
	movq	%rbx, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4741:
	.size	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5cloneEv, .-_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE5cloneEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number24LocalizedNumberFormatterC2EOS1_
	.type	_ZN6icu_676number24LocalizedNumberFormatterC2EOS1_, @function
_ZN6icu_676number24LocalizedNumberFormatterC2EOS1_:
.LFB4013:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_676number24LocalizedNumberFormatterC1EONS0_23NumberFormatterSettingsIS1_EE
	.cfi_endproc
.LFE4013:
	.size	_ZN6icu_676number24LocalizedNumberFormatterC2EOS1_, .-_ZN6icu_676number24LocalizedNumberFormatterC2EOS1_
	.globl	_ZN6icu_676number24LocalizedNumberFormatterC1EOS1_
	.set	_ZN6icu_676number24LocalizedNumberFormatterC1EOS1_,_ZN6icu_676number24LocalizedNumberFormatterC2EOS1_
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_676number4impl10MicroPropsE
	.section	.rodata._ZTSN6icu_676number4impl10MicroPropsE,"aG",@progbits,_ZTSN6icu_676number4impl10MicroPropsE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl10MicroPropsE, @object
	.size	_ZTSN6icu_676number4impl10MicroPropsE, 34
_ZTSN6icu_676number4impl10MicroPropsE:
	.string	"N6icu_676number4impl10MicroPropsE"
	.weak	_ZTSN6icu_676number4impl13EmptyModifierE
	.section	.rodata._ZTSN6icu_676number4impl13EmptyModifierE,"aG",@progbits,_ZTSN6icu_676number4impl13EmptyModifierE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl13EmptyModifierE, @object
	.size	_ZTSN6icu_676number4impl13EmptyModifierE, 37
_ZTSN6icu_676number4impl13EmptyModifierE:
	.string	"N6icu_676number4impl13EmptyModifierE"
	.weak	_ZTIN6icu_676number4impl13EmptyModifierE
	.section	.data.rel.ro._ZTIN6icu_676number4impl13EmptyModifierE,"awG",@progbits,_ZTIN6icu_676number4impl13EmptyModifierE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl13EmptyModifierE, @object
	.size	_ZTIN6icu_676number4impl13EmptyModifierE, 56
_ZTIN6icu_676number4impl13EmptyModifierE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl13EmptyModifierE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl8ModifierE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTIN6icu_676number4impl10MicroPropsE
	.section	.data.rel.ro._ZTIN6icu_676number4impl10MicroPropsE,"awG",@progbits,_ZTIN6icu_676number4impl10MicroPropsE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl10MicroPropsE, @object
	.size	_ZTIN6icu_676number4impl10MicroPropsE, 24
_ZTIN6icu_676number4impl10MicroPropsE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl10MicroPropsE
	.quad	_ZTIN6icu_676number4impl19MicroPropsGeneratorE
	.weak	_ZTVN6icu_676number4impl13EmptyModifierE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl13EmptyModifierE,"awG",@progbits,_ZTVN6icu_676number4impl13EmptyModifierE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl13EmptyModifierE, @object
	.size	_ZTVN6icu_676number4impl13EmptyModifierE, 88
_ZTVN6icu_676number4impl13EmptyModifierE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl13EmptyModifierE
	.quad	_ZN6icu_676number4impl13EmptyModifierD1Ev
	.quad	_ZN6icu_676number4impl13EmptyModifierD0Ev
	.quad	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.quad	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv
	.quad	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv
	.quad	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv
	.quad	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.quad	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE
	.quad	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE
	.weak	_ZTVN6icu_676number4impl10MicroPropsE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl10MicroPropsE,"awG",@progbits,_ZTVN6icu_676number4impl10MicroPropsE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl10MicroPropsE, @object
	.size	_ZTVN6icu_676number4impl10MicroPropsE, 40
_ZTVN6icu_676number4impl10MicroPropsE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl10MicroPropsE
	.quad	_ZN6icu_676number4impl10MicroPropsD1Ev
	.quad	_ZN6icu_676number4impl10MicroPropsD0Ev
	.quad	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode
	.section	.rodata
	.type	_ZN6icu_67L15kUndefinedFieldE, @object
	.size	_ZN6icu_67L15kUndefinedFieldE, 1
_ZN6icu_67L15kUndefinedFieldE:
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
