	.file	"collationruleparser.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser4Sink20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser4Sink20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode, @function
_ZN6icu_6719CollationRuleParser4Sink20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode:
.LFB3325:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3325:
	.size	_ZN6icu_6719CollationRuleParser4Sink20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode, .-_ZN6icu_6719CollationRuleParser4Sink20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode
	.globl	_ZN6icu_6719CollationRuleParser4Sink8optimizeERKNS_10UnicodeSetERPKcR10UErrorCode
	.set	_ZN6icu_6719CollationRuleParser4Sink8optimizeERKNS_10UnicodeSetERPKcR10UErrorCode,_ZN6icu_6719CollationRuleParser4Sink20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6719CollationRuleParser13setParseErrorEPKcR10UErrorCode.part.0, @function
_ZN6icu_6719CollationRuleParser13setParseErrorEPKcR10UErrorCode.part.0:
.LFB4444:
	.cfi_startproc
	movq	40(%rdi), %rax
	movl	$3, (%rdx)
	movq	%rsi, 48(%rdi)
	testq	%rax, %rax
	je	.L24
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	72(%rdi), %r13
	movl	$0, (%rax)
	movq	16(%rdi), %rdi
	leal	-15(%r13), %esi
	movl	%r13d, 4(%rax)
	testl	%esi, %esi
	jg	.L27
	xorl	%esi, %esi
.L5:
	leaq	8(%rax), %r12
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movq	%r12, %rcx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	40(%rbx), %rax
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	movw	%cx, 8(%rax,%r13,2)
	movzwl	8(%rdi), %ecx
	testw	%cx, %cx
	js	.L10
	movswl	%cx, %edx
	sarl	$5, %edx
.L11:
	movl	72(%rbx), %esi
	movl	%edx, %r12d
	subl	%esi, %r12d
	cmpl	$15, %r12d
	jle	.L12
	leal	14(%rsi), %r8d
	movl	$15, %r12d
	cmpl	%r8d, %edx
	jbe	.L12
	andl	$2, %ecx
	leaq	10(%rdi), %rdx
	jne	.L14
	movq	24(%rdi), %rdx
.L14:
	movslq	%r8d, %r8
	xorl	%r12d, %r12d
	movzwl	(%rdx,%r8,2), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	setne	%r12b
	addl	$14, %r12d
.L12:
	leaq	40(%rax), %r13
	xorl	%r8d, %r8d
	movl	%r12d, %edx
	movq	%r13, %rcx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	40(%rbx), %rax
	movslq	%r12d, %r12
	xorl	%edx, %edx
	movw	%dx, 40(%rax,%r12,2)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movzwl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L6
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L7:
	cmpl	%esi, %ecx
	jbe	.L17
	andl	$2, %edx
	leaq	10(%rdi), %rcx
	jne	.L9
	movq	24(%rdi), %rcx
.L9:
	movslq	%esi, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L28
.L17:
	movl	$15, %r13d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L10:
	movl	12(%rdi), %edx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L6:
	movl	12(%rdi), %ecx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L28:
	leal	-14(%r13), %esi
	movl	$14, %r13d
	jmp	.L5
	.cfi_endproc
.LFE4444:
	.size	_ZN6icu_6719CollationRuleParser13setParseErrorEPKcR10UErrorCode.part.0, .-_ZN6icu_6719CollationRuleParser13setParseErrorEPKcR10UErrorCode.part.0
	.section	.text._ZNK6icu_6713UnicodeStringeqERKS0_,"axG",@progbits,_ZNK6icu_6713UnicodeStringeqERKS0_,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6713UnicodeStringeqERKS0_
	.type	_ZNK6icu_6713UnicodeStringeqERKS0_, @function
_ZNK6icu_6713UnicodeStringeqERKS0_:
.LFB1186:
	.cfi_startproc
	endbr64
	movswl	8(%rdi), %edx
	movswl	8(%rsi), %eax
	movl	%edx, %ecx
	movl	%eax, %r8d
	andl	$1, %r8d
	andl	$1, %ecx
	jne	.L43
	testw	%dx, %dx
	js	.L31
	sarl	$5, %edx
	testw	%ax, %ax
	js	.L33
.L48:
	sarl	$5, %eax
.L34:
	andl	$1, %r8d
	jne	.L44
	cmpl	%edx, %eax
	je	.L47
.L44:
	movl	%ecx, %r8d
.L43:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movl	12(%rdi), %edx
	testw	%ax, %ax
	jns	.L48
.L33:
	movl	12(%rsi), %eax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L47:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	setne	%cl
	movl	%ecx, %eax
	ret
	.cfi_endproc
.LFE1186:
	.size	_ZNK6icu_6713UnicodeStringeqERKS0_, .-_ZNK6icu_6713UnicodeStringeqERKS0_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3686:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3686:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3689:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L62
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L50
	cmpb	$0, 12(%rbx)
	jne	.L63
.L54:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L50:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L54
	.cfi_endproc
.LFE3689:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3692:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L66
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3692:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3695:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L69
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3695:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L75
.L71:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L76
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3697:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3698:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3698:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3699:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3699:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3700:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3700:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3701:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3701:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3702:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3702:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3703:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L92
	testl	%edx, %edx
	jle	.L92
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L95
.L84:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L84
	.cfi_endproc
.LFE3703:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L99
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L99
	testl	%r12d, %r12d
	jg	.L106
	cmpb	$0, 12(%rbx)
	jne	.L107
.L101:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L101
.L107:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L99:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3704:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L109
	movq	(%rdi), %r8
.L110:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L113
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L113
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L113:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3705:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3706:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L120
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3706:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3707:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3707:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3708:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3708:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3709:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3709:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3711:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3711:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3713:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3713:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser4SinkD2Ev
	.type	_ZN6icu_6719CollationRuleParser4SinkD2Ev, @function
_ZN6icu_6719CollationRuleParser4SinkD2Ev:
.LFB3322:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6719CollationRuleParser4SinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3322:
	.size	_ZN6icu_6719CollationRuleParser4SinkD2Ev, .-_ZN6icu_6719CollationRuleParser4SinkD2Ev
	.globl	_ZN6icu_6719CollationRuleParser4SinkD1Ev
	.set	_ZN6icu_6719CollationRuleParser4SinkD1Ev,_ZN6icu_6719CollationRuleParser4SinkD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser4SinkD0Ev
	.type	_ZN6icu_6719CollationRuleParser4SinkD0Ev, @function
_ZN6icu_6719CollationRuleParser4SinkD0Ev:
.LFB3324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6719CollationRuleParser4SinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3324:
	.size	_ZN6icu_6719CollationRuleParser4SinkD0Ev, .-_ZN6icu_6719CollationRuleParser4SinkD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser8ImporterD2Ev
	.type	_ZN6icu_6719CollationRuleParser8ImporterD2Ev, @function
_ZN6icu_6719CollationRuleParser8ImporterD2Ev:
.LFB3328:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6719CollationRuleParser8ImporterE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3328:
	.size	_ZN6icu_6719CollationRuleParser8ImporterD2Ev, .-_ZN6icu_6719CollationRuleParser8ImporterD2Ev
	.globl	_ZN6icu_6719CollationRuleParser8ImporterD1Ev
	.set	_ZN6icu_6719CollationRuleParser8ImporterD1Ev,_ZN6icu_6719CollationRuleParser8ImporterD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser8ImporterD0Ev
	.type	_ZN6icu_6719CollationRuleParser8ImporterD0Ev, @function
_ZN6icu_6719CollationRuleParser8ImporterD0Ev:
.LFB3330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6719CollationRuleParser8ImporterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3330:
	.size	_ZN6icu_6719CollationRuleParser8ImporterD0Ev, .-_ZN6icu_6719CollationRuleParser8ImporterD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParserC2EPKNS_13CollationDataER10UErrorCode
	.type	_ZN6icu_6719CollationRuleParserC2EPKNS_13CollationDataER10UErrorCode, @function
_ZN6icu_6719CollationRuleParserC2EPKNS_13CollationDataER10UErrorCode:
.LFB3332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$8, %rsp
	call	_ZN6icu_6711Normalizer214getNFDInstanceER10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	call	_ZN6icu_6711Normalizer214getNFCInstanceER10UErrorCode@PLT
	movq	%r13, 24(%rbx)
	movq	%rax, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3332:
	.size	_ZN6icu_6719CollationRuleParserC2EPKNS_13CollationDataER10UErrorCode, .-_ZN6icu_6719CollationRuleParserC2EPKNS_13CollationDataER10UErrorCode
	.globl	_ZN6icu_6719CollationRuleParserC1EPKNS_13CollationDataER10UErrorCode
	.set	_ZN6icu_6719CollationRuleParserC1EPKNS_13CollationDataER10UErrorCode,_ZN6icu_6719CollationRuleParserC2EPKNS_13CollationDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParserD2Ev
	.type	_ZN6icu_6719CollationRuleParserD2Ev, @function
_ZN6icu_6719CollationRuleParserD2Ev:
.LFB3335:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3335:
	.size	_ZN6icu_6719CollationRuleParserD2Ev, .-_ZN6icu_6719CollationRuleParserD2Ev
	.globl	_ZN6icu_6719CollationRuleParserD1Ev
	.set	_ZN6icu_6719CollationRuleParserD1Ev,_ZN6icu_6719CollationRuleParserD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser21parseRelationOperatorER10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser21parseRelationOperatorER10UErrorCode, @function
_ZN6icu_6719CollationRuleParser21parseRelationOperatorER10UErrorCode:
.LFB3341:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L183
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movslq	72(%rdi), %r13
	movq	%r13, %rbx
	addq	%r13, %r13
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L185:
	movswl	%dx, %ecx
	sarl	$5, %ecx
	cmpl	%ecx, %ebx
	jge	.L140
.L186:
	movl	$65535, %edi
	cmpl	%ebx, %ecx
	jbe	.L141
	andl	$2, %edx
	je	.L142
	addq	$10, %rax
.L143:
	movzwl	(%rax,%r13), %edi
.L141:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %r13
	testb	%al, %al
	je	.L184
	addl	$1, %ebx
.L145:
	movq	16(%r12), %rax
	movzwl	8(%rax), %edx
	testw	%dx, %dx
	jns	.L185
	movl	12(%rax), %ecx
	cmpl	%ecx, %ebx
	jl	.L186
.L140:
	movl	%ebx, 72(%r12)
	testw	%dx, %dx
	js	.L146
.L188:
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L147:
	cmpl	%ebx, %ecx
	jle	.L182
	leal	1(%rbx), %esi
	jbe	.L182
	andl	$2, %edx
	jne	.L187
	movq	24(%rax), %rax
.L150:
	movslq	%ebx, %rdx
	leaq	(%rdx,%rdx), %rdi
	movzwl	(%rax,%rdx,2), %edx
	cmpw	$60, %dx
	je	.L151
	ja	.L152
	movl	$258, %r8d
	cmpw	$44, %dx
	je	.L135
	cmpw	$59, %dx
	movl	$257, %r8d
	movl	$-1, %eax
	cmovne	%eax, %r8d
.L135:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movq	24(%rax), %rax
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L187:
	addq	$10, %rax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L152:
	cmpw	$61, %dx
	jne	.L182
	movl	$271, %r8d
	cmpl	%ecx, %esi
	jge	.L135
	cmpl	%esi, %ecx
	jbe	.L135
	cmpw	$42, 2(%rax,%rdi)
	movl	$543, %eax
	cmove	%eax, %r8d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L182:
	addq	$8, %rsp
	movl	$-1, %r8d
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	16(%r12), %rax
	movzwl	8(%rax), %edx
	movl	%ebx, 72(%r12)
	testw	%dx, %dx
	jns	.L188
.L146:
	movl	12(%rax), %ecx
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$256, %r8d
	cmpl	%ecx, %esi
	jge	.L135
	cmpl	%esi, %ecx
	jbe	.L165
	xorl	%r8d, %r8d
	cmpw	$60, 2(%rax,%rdi)
	je	.L189
.L158:
	movslq	%esi, %rdx
	cmpw	$42, (%rax,%rdx,2)
	je	.L159
	subl	%ebx, %esi
	sall	$8, %esi
	orl	%esi, %r8d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L159:
	leal	1(%rsi), %eax
	subl	%ebx, %eax
	sall	$8, %eax
	orl	%eax, %r8d
	orl	$16, %r8d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L189:
	leal	2(%rbx), %esi
	movl	$513, %r8d
	cmpl	%ecx, %esi
	jge	.L135
	cmpl	%esi, %ecx
	jbe	.L168
	cmpw	$60, 4(%rax,%rdi)
	movl	$1, %r8d
	jne	.L158
	leal	3(%rbx), %esi
	movl	$770, %r8d
	cmpl	%esi, %ecx
	jle	.L135
	jbe	.L171
	cmpw	$60, 6(%rax,%rdi)
	movl	$2, %r8d
	jne	.L158
	leal	4(%rbx), %esi
	cmpl	%esi, %ecx
	jle	.L173
	jbe	.L190
	movl	$3, %r8d
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L165:
	xorl	%eax, %eax
.L160:
	subl	%ebx, %esi
	movl	%esi, %r8d
	sall	$8, %r8d
	orl	%eax, %r8d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$-1, %r8d
	movl	%r8d, %eax
	ret
.L168:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$1, %eax
	jmp	.L160
.L171:
	movl	$2, %eax
	jmp	.L160
.L190:
	movl	$3, %eax
	jmp	.L160
.L173:
	movl	$1027, %r8d
	jmp	.L135
	.cfi_endproc
.LFE3341:
	.size	_ZN6icu_6719CollationRuleParser21parseRelationOperatorER10UErrorCode, .-_ZN6icu_6719CollationRuleParser21parseRelationOperatorER10UErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"others"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser14getReorderCodeEPKc
	.type	_ZN6icu_6719CollationRuleParser14getReorderCodeEPKc, @function
_ZN6icu_6719CollationRuleParser14getReorderCodeEPKc:
.LFB3349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	_ZN6icu_67L20gSpecialReorderCodesE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
.L194:
	movq	(%r14,%rbx,8), %rsi
	movq	%r13, %rdi
	call	uprv_stricmp_67@PLT
	testl	%eax, %eax
	je	.L198
	addq	$1, %rbx
	cmpq	$5, %rbx
	jne	.L194
	movq	%r13, %rsi
	movl	$4106, %edi
	call	u_getPropertyValueEnum_67@PLT
	testl	%eax, %eax
	js	.L199
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	.LC0(%rip), %rsi
	call	uprv_stricmp_67@PLT
	popq	%rbx
	popq	%r12
	cmpl	$1, %eax
	popq	%r13
	popq	%r14
	sbbl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	andl	$104, %eax
	subl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	leal	4096(%rbx), %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3349:
	.size	_ZN6icu_6719CollationRuleParser14getReorderCodeEPKc, .-_ZN6icu_6719CollationRuleParser14getReorderCodeEPKc
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC1:
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC2:
	.string	"o"
	.string	"f"
	.string	"f"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser13getOnOffValueERKNS_13UnicodeStringE
	.type	_ZN6icu_6719CollationRuleParser13getOnOffValueERKNS_13UnicodeStringE, @function
_ZN6icu_6719CollationRuleParser13getOnOffValueERKNS_13UnicodeStringE:
.LFB3350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-120(%rbp), %r14
	leaq	-112(%rbp), %r13
	pushq	%r12
	movq	%r14, %rdx
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rax
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	8(%r12), %edx
	testb	$1, %dl
	je	.L201
	movzbl	-104(%rbp), %ebx
	andl	$1, %ebx
.L202:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$17, %eax
	testb	%bl, %bl
	je	.L209
.L200:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L231
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	testw	%dx, %dx
	js	.L203
	movzwl	-104(%rbp), %eax
	sarl	$5, %edx
	testw	%ax, %ax
	js	.L205
.L232:
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L206:
	testb	$1, %al
	jne	.L222
	cmpl	%edx, %ecx
	je	.L207
.L222:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L209:
	movq	%r14, %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rax
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	8(%r12), %edx
	testb	$1, %dl
	je	.L211
	movzbl	-104(%rbp), %ebx
	andl	$1, %ebx
.L212:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$1, %bl
	sbbl	%eax, %eax
	orl	$16, %eax
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L211:
	testw	%dx, %dx
	js	.L213
	movzwl	-104(%rbp), %ecx
	sarl	$5, %edx
	testw	%cx, %cx
	js	.L215
.L233:
	movswl	%cx, %eax
	sarl	$5, %eax
.L216:
	cmpl	%edx, %eax
	jne	.L223
	andl	$1, %ecx
	je	.L217
.L223:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %eax
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L203:
	movzwl	-104(%rbp), %eax
	movl	12(%r12), %edx
	testw	%ax, %ax
	jns	.L232
.L205:
	movl	-100(%rbp), %ecx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L213:
	movzwl	-104(%rbp), %ecx
	movl	12(%r12), %edx
	testw	%cx, %cx
	jns	.L233
.L215:
	movl	-100(%rbp), %eax
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L207:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%bl
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%bl
	jmp	.L212
.L231:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3350:
	.size	_ZN6icu_6719CollationRuleParser13getOnOffValueERKNS_13UnicodeStringE, .-_ZN6icu_6719CollationRuleParser13getOnOffValueERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CollationRuleParser9readWordsEiRNS_13UnicodeStringE
	.type	_ZNK6icu_6719CollationRuleParser9readWordsEiRNS_13UnicodeStringE, @function
_ZNK6icu_6719CollationRuleParser9readWordsEiRNS_13UnicodeStringE:
.LFB3352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%esi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r14, %rbx
	subq	$24, %rsp
	movzwl	8(%rdx), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	addq	%r14, %r14
	movw	%ax, 8(%r13)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L301:
	movswl	%cx, %edx
	sarl	$5, %edx
	cmpl	%edx, %ebx
	jge	.L240
.L302:
	movl	$65535, %edi
	cmpl	%ebx, %edx
	jbe	.L241
	andl	$2, %ecx
	je	.L242
	addq	$10, %rax
.L243:
	movzwl	(%rax,%r14), %edi
.L241:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %r14
	testb	%al, %al
	je	.L300
	addl	$1, %ebx
.L236:
	movq	16(%r12), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	jns	.L301
	movl	12(%rax), %edx
	cmpl	%edx, %ebx
	jl	.L302
.L240:
	leaq	-58(%rbp), %r14
.L245:
	testw	%cx, %cx
	js	.L246
.L306:
	movswl	%cx, %edx
	sarl	$5, %edx
	cmpl	%edx, %ebx
	jge	.L277
.L307:
	cmpl	%ebx, %edx
	jbe	.L278
	andl	$2, %ecx
	je	.L250
	addq	$10, %rax
.L251:
	movslq	%ebx, %rdx
	movzwl	(%rax,%rdx,2), %edi
	leal	-33(%rdi), %eax
	movl	%edi, %r15d
	cmpl	$93, %eax
	ja	.L249
	leal	-58(%rdi), %eax
	cmpl	$6, %eax
	jbe	.L252
	cmpl	$47, %edi
	jle	.L252
	leal	-91(%rdi), %eax
	cmpl	$5, %eax
	jbe	.L252
	cmpl	$122, %edi
	jle	.L249
.L252:
	cmpw	$45, %r15w
	je	.L249
	cmpw	$95, %r15w
	jne	.L303
.L249:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addl	$1, %ebx
	testb	%al, %al
	je	.L304
	movl	$32, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movslq	%ebx, %r15
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	addq	%r15, %r15
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L305:
	movswl	%cx, %edx
	sarl	$5, %edx
.L266:
	cmpl	%edx, %ebx
	jge	.L245
	movl	$65535, %edi
	cmpl	%ebx, %edx
	jbe	.L268
	andl	$2, %ecx
	je	.L269
	addq	$10, %rax
.L270:
	movzwl	(%rax,%r15), %edi
.L268:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %r15
	testb	%al, %al
	je	.L299
	addl	$1, %ebx
.L272:
	movq	16(%r12), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	jns	.L305
	movl	12(%rax), %edx
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L242:
	movq	24(%rax), %rax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L269:
	movq	24(%rax), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L304:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movw	%r15w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L299:
	movq	16(%r12), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	jns	.L306
.L246:
	movl	12(%rax), %edx
	cmpl	%edx, %ebx
	jl	.L307
.L277:
	xorl	%eax, %eax
.L234:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L308
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	movq	24(%rax), %rax
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L278:
	movl	$-1, %r15d
	movl	$65535, %edi
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L300:
	movq	16(%r12), %rax
	movzwl	8(%rax), %ecx
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L303:
	movswl	8(%r13), %edx
	movl	%ebx, %eax
	movl	%edx, %ecx
	sarl	$5, %edx
	je	.L234
	testw	%cx, %cx
	jns	.L258
	movl	12(%r13), %edx
.L258:
	leal	-1(%rdx), %esi
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	leaq	_ZZNK6icu_6719CollationRuleParser9readWordsEiRNS_13UnicodeStringEE2sp(%rip), %rcx
	movl	$1, %edx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movl	%eax, %edx
	leaq	_ZZNK6icu_6719CollationRuleParser9readWordsEiRNS_13UnicodeStringEE2sp(%rip), %rax
	movl	%ebx, %eax
	testb	%dl, %dl
	jne	.L234
	movzwl	8(%r13), %edx
	movl	%edx, %edi
	andl	$1, %edi
	testw	%dx, %dx
	js	.L259
	movswl	%dx, %esi
	sarl	$5, %esi
	leal	-1(%rsi), %ecx
	testb	%dil, %dil
	je	.L309
	testl	%ecx, %ecx
	je	.L274
	cmpl	%ecx, %esi
	jbe	.L234
.L273:
	movl	%edx, %eax
	sall	$5, %ecx
	andl	$31, %eax
	orl	%ecx, %eax
	movw	%ax, 8(%r13)
	movl	%ebx, %eax
	jmp	.L234
.L259:
	movl	12(%r13), %esi
	leal	-1(%rsi), %ecx
	testb	%dil, %dil
	je	.L263
	testl	%ecx, %ecx
	je	.L274
.L263:
	movl	%ebx, %eax
	cmpl	%ecx, %esi
	jbe	.L234
	cmpl	$1023, %ecx
	jle	.L273
	orl	$-32, %edx
	movl	%ecx, 12(%r13)
	movw	%dx, 8(%r13)
	jmp	.L234
.L309:
	cmpl	%esi, %ecx
	jb	.L273
	jmp	.L234
.L274:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movl	%ebx, %eax
	jmp	.L234
.L308:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3352:
	.size	_ZNK6icu_6719CollationRuleParser9readWordsEiRNS_13UnicodeStringE, .-_ZNK6icu_6719CollationRuleParser9readWordsEiRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CollationRuleParser11skipCommentEi
	.type	_ZNK6icu_6719CollationRuleParser11skipCommentEi, @function
_ZNK6icu_6719CollationRuleParser11skipCommentEi:
.LFB3353:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	movswl	8(%rdi), %ecx
	movl	%ecx, %eax
	sarl	$5, %ecx
	movl	%eax, %edx
	andl	$2, %edx
	testw	%ax, %ax
	js	.L345
	testw	%dx, %dx
	jne	.L320
	movslq	%esi, %r8
	addq	%r8, %r8
	.p2align 4,,10
	.p2align 3
.L325:
	cmpl	%esi, %ecx
	jle	.L330
	leal	1(%rsi), %eax
	cmpl	%ecx, %esi
	jnb	.L346
	movq	24(%rdi), %rdx
	movzwl	(%rdx,%r8), %edx
	leal	-12(%rdx), %esi
	cmpw	$1, %si
	jbe	.L310
	cmpw	$10, %dx
	je	.L347
	cmpw	$133, %dx
	je	.L310
	subw	$8232, %dx
	addq	$2, %r8
	cmpw	$1, %dx
	jbe	.L348
	movl	%eax, %esi
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L320:
	movslq	%esi, %rsi
	.p2align 4,,10
	.p2align 3
.L328:
	movl	%esi, %eax
	cmpl	%esi, %ecx
	jle	.L310
	leal	1(%rsi), %eax
	jbe	.L349
	movzwl	10(%rdi,%rsi,2), %edx
	leal	-12(%rdx), %r8d
	cmpw	$1, %r8w
	jbe	.L310
	cmpw	$10, %dx
	je	.L310
	cmpw	$133, %dx
	je	.L310
	subw	$8232, %dx
	addq	$1, %rsi
	cmpw	$1, %dx
	ja	.L328
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	movl	%esi, %eax
.L310:
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	addq	$1, %rsi
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L346:
	addq	$2, %r8
	movl	%eax, %esi
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L345:
	testw	%dx, %dx
	jne	.L312
	movslq	%esi, %rcx
	movl	12(%rdi), %r8d
	addq	%rcx, %rcx
	.p2align 4,,10
	.p2align 3
.L316:
	cmpl	%esi, %r8d
	jle	.L330
	leal	1(%rsi), %eax
	jbe	.L350
	movq	24(%rdi), %rdx
	movzwl	(%rdx,%rcx), %edx
	leal	-12(%rdx), %esi
	cmpw	$1, %si
	jbe	.L310
	cmpw	$10, %dx
	je	.L310
	cmpw	$133, %dx
	je	.L310
	subw	$8232, %dx
	addq	$2, %rcx
	cmpw	$1, %dx
	jbe	.L310
	movl	%eax, %esi
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L350:
	addq	$2, %rcx
	movl	%eax, %esi
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L312:
	movl	12(%rdi), %ecx
	movslq	%esi, %rsi
	.p2align 4,,10
	.p2align 3
.L319:
	movl	%esi, %eax
	cmpl	%esi, %ecx
	jle	.L310
	leal	1(%rsi), %eax
	cmpl	%ecx, %esi
	jnb	.L351
	movzwl	10(%rdi,%rsi,2), %edx
	leal	-12(%rdx), %r8d
	cmpw	$1, %r8w
	jbe	.L310
	cmpw	$10, %dx
	je	.L310
	cmpw	$133, %dx
	je	.L310
	subw	$8232, %dx
	addq	$1, %rsi
	cmpw	$1, %dx
	ja	.L319
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	addq	$1, %rsi
	jmp	.L319
.L348:
	ret
.L347:
	ret
	.cfi_endproc
.LFE3353:
	.size	_ZNK6icu_6719CollationRuleParser11skipCommentEi, .-_ZNK6icu_6719CollationRuleParser11skipCommentEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser13setParseErrorEPKcR10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser13setParseErrorEPKcR10UErrorCode, @function
_ZN6icu_6719CollationRuleParser13setParseErrorEPKcR10UErrorCode:
.LFB3354:
	.cfi_startproc
	endbr64
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L375
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %rax
	movl	$3, (%rdx)
	movq	%rsi, 48(%rdi)
	testq	%rax, %rax
	je	.L352
	movslq	72(%rdi), %r13
	movq	16(%rdi), %rdi
	movl	$0, (%rax)
	leal	-15(%r13), %esi
	movl	%r13d, 4(%rax)
	testl	%esi, %esi
	jg	.L378
	xorl	%esi, %esi
.L356:
	leaq	8(%rax), %r12
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movq	%r12, %rcx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	40(%rbx), %rax
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	movw	%cx, 8(%rax,%r13,2)
	movzwl	8(%rdi), %ecx
	testw	%cx, %cx
	js	.L361
	movswl	%cx, %edx
	sarl	$5, %edx
.L362:
	movl	72(%rbx), %esi
	movl	%edx, %r12d
	subl	%esi, %r12d
	cmpl	$15, %r12d
	jle	.L363
	leal	14(%rsi), %r8d
	movl	$15, %r12d
	cmpl	%r8d, %edx
	jbe	.L363
	andl	$2, %ecx
	leaq	10(%rdi), %rdx
	je	.L379
.L365:
	movslq	%r8d, %r8
	xorl	%r12d, %r12d
	movzwl	(%rdx,%r8,2), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	setne	%r12b
	addl	$14, %r12d
.L363:
	leaq	40(%rax), %r13
	xorl	%r8d, %r8d
	movl	%r12d, %edx
	movq	%r13, %rcx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	40(%rbx), %rax
	movslq	%r12d, %r12
	xorl	%edx, %edx
	movw	%dx, 40(%rax,%r12,2)
.L352:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movzwl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L357
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L358:
	cmpl	%esi, %ecx
	jbe	.L368
	andl	$2, %edx
	leaq	10(%rdi), %rdx
	jne	.L360
	movq	24(%rdi), %rdx
.L360:
	movslq	%esi, %rcx
	movzwl	(%rdx,%rcx,2), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L380
.L368:
	movl	$15, %r13d
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L361:
	movl	12(%rdi), %edx
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L379:
	movq	24(%rdi), %rdx
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L357:
	movl	12(%rdi), %ecx
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L380:
	leal	-14(%r13), %esi
	movl	$14, %r13d
	jmp	.L356
	.cfi_endproc
.LFE3354:
	.size	_ZN6icu_6719CollationRuleParser13setParseErrorEPKcR10UErrorCode, .-_ZN6icu_6719CollationRuleParser13setParseErrorEPKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser15setErrorContextEv
	.type	_ZN6icu_6719CollationRuleParser15setErrorContextEv, @function
_ZN6icu_6719CollationRuleParser15setErrorContextEv:
.LFB3355:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L402
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	72(%rdi), %r13
	movl	$0, (%rax)
	movq	16(%rdi), %rdi
	leal	-15(%r13), %esi
	movl	%r13d, 4(%rax)
	testl	%esi, %esi
	jg	.L405
	xorl	%esi, %esi
.L383:
	leaq	8(%rax), %r12
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movq	%r12, %rcx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	40(%rbx), %rax
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	movw	%cx, 8(%rax,%r13,2)
	movzwl	8(%rdi), %ecx
	testw	%cx, %cx
	js	.L388
	movswl	%cx, %edx
	sarl	$5, %edx
.L389:
	movl	72(%rbx), %esi
	movl	%edx, %r12d
	subl	%esi, %r12d
	cmpl	$15, %r12d
	jle	.L390
	leal	14(%rsi), %r8d
	movl	$15, %r12d
	cmpl	%r8d, %edx
	jbe	.L390
	andl	$2, %ecx
	leaq	10(%rdi), %rdx
	je	.L406
.L392:
	movslq	%r8d, %r8
	xorl	%r12d, %r12d
	movzwl	(%rdx,%r8,2), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	setne	%r12b
	addl	$14, %r12d
.L390:
	leaq	40(%rax), %r13
	xorl	%r8d, %r8d
	movl	%r12d, %edx
	movq	%r13, %rcx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	40(%rbx), %rax
	movslq	%r12d, %r12
	xorl	%edx, %edx
	movw	%dx, 40(%rax,%r12,2)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_restore_state
	movl	12(%rdi), %edx
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L405:
	movzwl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L384
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L385:
	cmpl	%esi, %ecx
	jbe	.L395
	andl	$2, %edx
	leaq	10(%rdi), %rcx
	jne	.L387
	movq	24(%rdi), %rcx
.L387:
	movslq	%esi, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L407
.L395:
	movl	$15, %r13d
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L406:
	movq	24(%rdi), %rdx
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L384:
	movl	12(%rdi), %ecx
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leal	-14(%r13), %esi
	movl	$14, %r13d
	jmp	.L383
	.cfi_endproc
.LFE3355:
	.size	_ZN6icu_6719CollationRuleParser15setErrorContextEv, .-_ZN6icu_6719CollationRuleParser15setErrorContextEv
	.section	.rodata.str2.2
	.align 2
.LC3:
	.string	"t"
	.string	"o"
	.string	"p"
	.string	""
	.string	""
	.align 2
.LC4:
	.string	"v"
	.string	"a"
	.string	"r"
	.string	"i"
	.string	"a"
	.string	"b"
	.string	"l"
	.string	"e"
	.string	" "
	.string	"t"
	.string	"o"
	.string	"p"
	.string	""
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"not a valid special reset position"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6719CollationRuleParser20parseSpecialPositionEiRNS_13UnicodeStringER10UErrorCode.part.0, @function
_ZN6icu_6719CollationRuleParser20parseSpecialPositionEiRNS_13UnicodeStringER10UErrorCode.part.0:
.LFB4446:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	addl	$1, %esi
	subq	$200, %rsp
	movq	%rdx, -216(%rbp)
	movq	%r15, %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r9w, -184(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZNK6icu_6719CollationRuleParser9readWordsEiRNS_13UnicodeStringE
	cmpl	%eax, %ebx
	jge	.L409
	movq	16(%r14), %rdx
	movzwl	8(%rdx), %ecx
	testw	%cx, %cx
	js	.L410
	movswl	%cx, %esi
	sarl	$5, %esi
.L411:
	cmpl	%eax, %esi
	jbe	.L409
	andl	$2, %ecx
	jne	.L478
	movq	24(%rdx), %rdx
.L413:
	movslq	%eax, %rcx
	cmpw	$93, (%rdx,%rcx,2)
	jne	.L409
	movswl	-184(%rbp), %edx
	shrl	$5, %edx
	je	.L409
	leal	1(%rax), %r12d
	xorl	%r10d, %r10d
	leaq	-128(%rbp), %r9
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L481:
	movzbl	-120(%rbp), %eax
	andl	$1, %eax
	movb	%al, -224(%rbp)
.L415:
	movq	%r9, %rdi
	movq	%r10, -240(%rbp)
	movq	%r9, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -224(%rbp)
	movq	-232(%rbp), %r9
	movq	-240(%rbp), %r10
	jne	.L479
.L422:
	addq	$1, %r10
	cmpq	$14, %r10
	je	.L480
.L426:
	leaq	_ZN6icu_6712_GLOBAL__N_1L9positionsE(%rip), %rax
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r9, %rdi
	movq	(%rax,%r10,8), %rsi
	movq	%r10, -232(%rbp)
	movq	%r9, -224(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movswl	-184(%rbp), %edx
	movq	-224(%rbp), %r9
	movq	-232(%rbp), %r10
	testb	$1, %dl
	jne	.L481
	testw	%dx, %dx
	js	.L416
	sarl	$5, %edx
.L417:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L418
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L419:
	testb	$1, %al
	jne	.L451
	cmpl	%edx, %ecx
	je	.L420
.L451:
	movq	%r9, %rdi
	movq	%r10, -232(%rbp)
	movq	%r9, -224(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-224(%rbp), %r9
	movq	-232(%rbp), %r10
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L438:
	testw	%ax, %ax
	js	.L440
	movswl	%ax, %edx
	sarl	$5, %edx
.L441:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L442
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L443:
	testb	$1, %al
	jne	.L453
	cmpl	%edx, %ecx
	je	.L444
.L453:
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L409:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L477
	leaq	.LC5(%rip), %rax
	cmpq	$0, 40(%r14)
	movl	$3, 0(%r13)
	movq	%rax, 48(%r14)
	je	.L477
	movq	%r14, %rdi
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
.L477:
	movl	%ebx, %r12d
.L425:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L482
	addq	$200, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	movl	12(%rdx), %esi
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L478:
	addq	$10, %rdx
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L418:
	movl	-116(%rbp), %ecx
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L416:
	movl	-180(%rbp), %edx
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L480:
	leaq	.LC3(%rip), %rax
	movq	%r9, %rdi
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rax, -200(%rbp)
	leaq	-200(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -224(%rbp)
	movq	%r9, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-184(%rbp), %eax
	movq	-232(%rbp), %r9
	testb	$1, %al
	je	.L427
	movzbl	-120(%rbp), %eax
	andl	$1, %eax
	movb	%al, -240(%rbp)
.L428:
	movq	%r9, %rdi
	movq	%r9, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -240(%rbp)
	movq	-232(%rbp), %r9
	je	.L435
	movq	-216(%rbp), %rbx
	movl	$-2, %edi
	movw	%di, -200(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L436
	movswl	%ax, %edx
	sarl	$5, %edx
.L437:
	movq	-224(%rbp), %rbx
	movq	-216(%rbp), %rdi
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	%rbx, %rcx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movl	$10249, %esi
	movl	$1, %ecx
	xorl	%edx, %edx
	movw	%si, -200(%rbp)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L479:
	movq	-216(%rbp), %rbx
	movl	$-2, %r8d
	movq	%r10, -224(%rbp)
	movw	%r8w, -200(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	8(%rbx), %eax
	movq	-224(%rbp), %r10
	testw	%ax, %ax
	js	.L423
	movswl	%ax, %edx
	sarl	$5, %edx
.L424:
	movq	-216(%rbp), %rdi
	leaq	-200(%rbp), %r13
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %r9d
	movq	%r10, -224(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	-224(%rbp), %r10
	movq	%rax, %rdi
	addw	$10240, %r10w
	movw	%r10w, -200(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L420:
	movq	%r9, %rsi
	movq	%r15, %rdi
	movq	%r10, -240(%rbp)
	movq	%r9, -232(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-240(%rbp), %r10
	movq	-232(%rbp), %r9
	testb	%al, %al
	setne	-224(%rbp)
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L427:
	testw	%ax, %ax
	js	.L429
	sarl	$5, %eax
.L430:
	movzwl	-120(%rbp), %edx
	testw	%dx, %dx
	js	.L431
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L432:
	andl	$1, %edx
	jne	.L452
	cmpl	%eax, %ecx
	je	.L433
.L452:
	movq	%r9, %rdi
	movq	%r9, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-232(%rbp), %r9
.L435:
	movq	-224(%rbp), %rdx
	leaq	.LC4(%rip), %rax
	movq	%r9, %rdi
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rax, -200(%rbp)
	movq	%r9, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-184(%rbp), %eax
	movq	-232(%rbp), %r9
	testb	$1, %al
	je	.L438
	movzbl	-120(%rbp), %eax
	andl	$1, %eax
	movb	%al, -232(%rbp)
.L439:
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -232(%rbp)
	je	.L409
	movq	-216(%rbp), %rbx
	movl	$-2, %ecx
	movw	%cx, -200(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L446
	movswl	%ax, %edx
	sarl	$5, %edx
.L447:
	movq	-224(%rbp), %rbx
	movq	-216(%rbp), %rdi
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	%rbx, %rcx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movl	$10247, %edx
	movl	$1, %ecx
	movq	%rbx, %rsi
	movw	%dx, -200(%rbp)
	movq	%rax, %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L423:
	movq	-216(%rbp), %rax
	movl	12(%rax), %edx
	jmp	.L424
.L431:
	movl	-116(%rbp), %ecx
	jmp	.L432
.L429:
	movl	-180(%rbp), %eax
	jmp	.L430
.L436:
	movq	-216(%rbp), %rax
	movl	12(%rax), %edx
	jmp	.L437
.L442:
	movl	-116(%rbp), %ecx
	jmp	.L443
.L440:
	movl	-180(%rbp), %edx
	jmp	.L441
.L446:
	movq	-216(%rbp), %rax
	movl	12(%rax), %edx
	jmp	.L447
.L433:
	movq	%r9, %rsi
	movl	%eax, %edx
	movq	%r15, %rdi
	movq	%r9, -232(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-232(%rbp), %r9
	testb	%al, %al
	setne	-240(%rbp)
	jmp	.L428
.L444:
	movq	%r9, %rsi
	movq	%r15, %rdi
	movq	%r9, -240(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-240(%rbp), %r9
	testb	%al, %al
	setne	-232(%rbp)
	jmp	.L439
.L482:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4446:
	.size	_ZN6icu_6719CollationRuleParser20parseSpecialPositionEiRNS_13UnicodeStringER10UErrorCode.part.0, .-_ZN6icu_6719CollationRuleParser20parseSpecialPositionEiRNS_13UnicodeStringER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser20parseSpecialPositionEiRNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser20parseSpecialPositionEiRNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6719CollationRuleParser20parseSpecialPositionEiRNS_13UnicodeStringER10UErrorCode:
.LFB3346:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L485
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L485:
	jmp	_ZN6icu_6719CollationRuleParser20parseSpecialPositionEiRNS_13UnicodeStringER10UErrorCode.part.0
	.cfi_endproc
.LFE3346:
	.size	_ZN6icu_6719CollationRuleParser20parseSpecialPositionEiRNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6719CollationRuleParser20parseSpecialPositionEiRNS_13UnicodeStringER10UErrorCode
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"unbalanced UnicodeSet pattern brackets"
	.align 8
.LC7:
	.string	"not a valid UnicodeSet pattern"
	.align 8
.LC8:
	.string	"missing option-terminating ']' after UnicodeSet pattern"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser15parseUnicodeSetEiRNS_10UnicodeSetER10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser15parseUnicodeSetEiRNS_10UnicodeSetER10UErrorCode, @function
_ZN6icu_6719CollationRuleParser15parseUnicodeSetEiRNS_10UnicodeSetER10UErrorCode:
.LFB3351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$88, %rsp
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movswl	8(%rsi), %edx
	movl	%edx, %eax
	sarl	$5, %edx
	movl	%eax, %ecx
	andl	$2, %ecx
	testw	%ax, %ax
	js	.L487
	testw	%cx, %cx
	jne	.L535
	movslq	%r8d, %rcx
	movl	%r8d, %eax
	xorl	%edi, %edi
	addq	%rcx, %rcx
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L532:
	cmpw	$93, %ax
	jne	.L492
	subl	$1, %edi
	je	.L493
.L492:
	addq	$2, %rcx
	movl	%r12d, %eax
.L496:
	cmpl	%eax, %edx
	je	.L523
	leal	1(%rax), %r12d
	cmpl	%edx, %eax
	jnb	.L492
	movq	24(%rsi), %rax
	movzwl	(%rax,%rcx), %eax
	cmpw	$91, %ax
	jne	.L532
	addl	$1, %edi
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L535:
	movslq	%r8d, %rax
	xorl	%edi, %edi
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L507:
	cmpw	$93, %cx
	jne	.L506
	subl	$1, %edi
	je	.L493
.L506:
	addq	$1, %rax
.L489:
	movl	%eax, %r12d
	cmpl	%eax, %edx
	je	.L490
	leal	1(%rax), %r12d
	jbe	.L506
	movzwl	10(%rsi,%rax,2), %ecx
	cmpw	$91, %cx
	jne	.L507
	addl	$1, %edi
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L523:
	movl	%edx, %r12d
.L490:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L486
	leaq	.LC6(%rip), %rax
	cmpq	$0, 40(%r13)
	movl	$3, (%rbx)
	movq	%rax, 48(%r13)
	je	.L486
.L534:
	movq	%r13, %rdi
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
.L486:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L536
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	testw	%cx, %cx
	jne	.L497
	movslq	%r8d, %rdx
	movl	12(%rsi), %r12d
	movl	%r8d, %eax
	xorl	%edi, %edi
	addq	%rdx, %rdx
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L499:
	cmpw	$93, %ax
	jne	.L498
	subl	$1, %edi
	je	.L524
.L498:
	addq	$2, %rdx
	movl	%ecx, %eax
.L500:
	cmpl	%eax, %r12d
	je	.L490
	leal	1(%rax), %ecx
	jbe	.L498
	movq	24(%rsi), %rax
	movzwl	(%rax,%rdx), %eax
	cmpw	$91, %ax
	jne	.L499
	addl	$1, %edi
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L497:
	movl	12(%rsi), %edx
	movslq	%r8d, %rax
	xorl	%edi, %edi
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L537:
	cmpw	$93, %cx
	jne	.L501
	subl	$1, %edi
	je	.L493
.L501:
	addq	$1, %rax
.L503:
	movl	%eax, %r12d
	cmpl	%eax, %edx
	je	.L490
	leal	1(%rax), %r12d
	jbe	.L501
	movzwl	10(%rsi,%rax,2), %ecx
	cmpw	$91, %cx
	jne	.L537
	addl	$1, %edi
	jmp	.L501
.L524:
	movl	%ecx, %r12d
.L493:
	leaq	-128(%rbp), %r15
	movl	%r12d, %ecx
	movl	%r8d, %edx
	subl	%r8d, %ecx
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L508
	movslq	%r12d, %r14
	addq	%r14, %r14
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L539:
	movswl	%cx, %edx
	sarl	$5, %edx
.L511:
	cmpl	%edx, %r12d
	jge	.L512
	movl	$65535, %edi
	cmpl	%r12d, %edx
	jbe	.L513
	andl	$2, %ecx
	je	.L514
	addq	$10, %rax
.L515:
	movzwl	(%rax,%r14), %edi
.L513:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %r14
	testb	%al, %al
	je	.L538
	addl	$1, %r12d
.L509:
	movq	16(%r13), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	jns	.L539
	movl	12(%rax), %edx
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L514:
	movq	24(%rax), %rax
	jmp	.L515
.L538:
	movq	16(%r13), %rax
	movzwl	8(%rax), %ecx
.L512:
	testw	%cx, %cx
	js	.L517
	movswl	%cx, %edx
	sarl	$5, %edx
.L518:
	cmpl	%r12d, %edx
	je	.L519
	jbe	.L519
	andl	$2, %ecx
	jne	.L540
	movq	24(%rax), %rax
.L521:
	movslq	%r12d, %rdx
	cmpw	$93, (%rax,%rdx,2)
	jne	.L519
	addl	$1, %r12d
	jmp	.L486
.L508:
	leaq	.LC7(%rip), %rax
	cmpq	$0, 40(%r13)
	movl	$3, (%rbx)
	movq	%rax, 48(%r13)
	jne	.L534
	jmp	.L486
.L519:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L486
	leaq	.LC8(%rip), %rax
	cmpq	$0, 40(%r13)
	movl	$3, (%rbx)
	movq	%rax, 48(%r13)
	jne	.L534
	jmp	.L486
.L540:
	addq	$10, %rax
	jmp	.L521
.L517:
	movl	12(%rax), %edx
	jmp	.L518
.L536:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3351:
	.size	_ZN6icu_6719CollationRuleParser15parseUnicodeSetEiRNS_10UnicodeSetER10UErrorCode, .-_ZN6icu_6719CollationRuleParser15parseUnicodeSetEiRNS_10UnicodeSetER10UErrorCode
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"quoted literal text missing terminating apostrophe"
	.align 8
.LC10:
	.string	"backslash escape at the end of the rule string"
	.align 8
.LC11:
	.string	"string contains an unpaired surrogate"
	.align 8
.LC12:
	.string	"string contains U+FFFD, U+FFFE or U+FFFF"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6719CollationRuleParser11parseStringEiRNS_13UnicodeStringER10UErrorCode.part.0, @function
_ZN6icu_6719CollationRuleParser11parseStringEiRNS_13UnicodeStringER10UErrorCode.part.0:
.LFB4445:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-58(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movzwl	8(%rdx), %edx
	movq	16(%rdi), %rdi
	movq	%rcx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 8(%rbx)
	movzwl	8(%rdi), %edx
	.p2align 4,,10
	.p2align 3
.L543:
	testw	%dx, %dx
	js	.L545
.L590:
	movswl	%dx, %eax
	sarl	$5, %eax
	cmpl	%eax, %r14d
	jge	.L554
.L547:
	leal	1(%r14), %r15d
	cmpl	%r14d, %eax
	jbe	.L574
	leaq	10(%rdi), %rsi
	testb	$2, %dl
	jne	.L551
	movq	24(%rdi), %rsi
.L551:
	movslq	%r14d, %rcx
	movzwl	(%rsi,%rcx,2), %r9d
	leaq	(%rcx,%rcx), %r10
	leal	-33(%r9), %r11d
	movl	%r9d, %ecx
	cmpl	$93, %r11d
	ja	.L549
	leal	-58(%r9), %r11d
	cmpl	$6, %r11d
	jbe	.L552
	cmpl	$47, %r9d
	jle	.L552
	leal	-91(%r9), %edx
	cmpl	$5, %edx
	jbe	.L553
	cmpl	$122, %r9d
	jle	.L549
.L553:
	cmpl	$92, %r9d
	jne	.L554
	cmpl	%r15d, %eax
	je	.L589
	movl	%r15d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	movl	%eax, %r14d
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movq	16(%r12), %rdi
	xorl	%eax, %eax
	cmpl	$65535, %r14d
	seta	%al
	movzwl	8(%rdi), %edx
	leal	1(%r15,%rax), %r14d
	testw	%dx, %dx
	jns	.L590
.L545:
	movl	12(%rdi), %eax
	cmpl	%eax, %r14d
	jl	.L547
.L554:
	xorl	%r13d, %r13d
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L593:
	sarl	$5, %eax
	cmpl	%eax, %r13d
	jge	.L579
.L594:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edx
	andl	$-2048, %edx
	cmpl	$55296, %edx
	je	.L591
	leal	-65533(%rax), %edx
	cmpl	$2, %edx
	jbe	.L592
	cmpl	$65535, %eax
	seta	%al
	movzbl	%al, %eax
	leal	1(%r13,%rax), %r13d
.L548:
	movswl	8(%rbx), %eax
	testw	%ax, %ax
	jns	.L593
	movl	12(%rbx), %eax
	cmpl	%eax, %r13d
	jl	.L594
.L579:
	movl	%r14d, %eax
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L574:
	movl	$65535, %r9d
	movl	$-1, %ecx
.L549:
	movl	%r9d, %edi
	movl	%ecx, -68(%rbp)
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	movl	-68(%rbp), %ecx
	testb	%al, %al
	jne	.L554
	movw	%cx, -58(%rbp)
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %ecx
	movq	%r13, %rsi
	movl	%r15d, %r14d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	16(%r12), %rdi
	movzwl	8(%rdi), %edx
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L552:
	cmpl	$39, %r9d
	jne	.L553
	cmpl	%r15d, %eax
	jle	.L565
	jbe	.L565
	cmpw	$39, 2(%rsi,%r10)
	jne	.L565
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L598:
	movswl	%dx, %eax
	sarl	$5, %eax
	cmpl	%eax, %r15d
	je	.L596
.L560:
	leal	1(%r15), %r14d
	cmpl	%r15d, %eax
	jbe	.L575
	leaq	10(%rdi), %rsi
	testb	$2, %dl
	jne	.L564
	movq	24(%rdi), %rsi
.L564:
	movslq	%r15d, %rcx
	leaq	(%rcx,%rcx), %r9
	movzwl	(%rsi,%rcx,2), %ecx
	cmpw	$39, %cx
	je	.L597
.L562:
	movw	%cx, -58(%rbp)
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %ecx
	movq	%r13, %rsi
	movl	%r14d, %r15d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	16(%r12), %rdi
	movzwl	8(%rdi), %edx
.L565:
	testw	%dx, %dx
	jns	.L598
	movl	12(%rdi), %eax
	cmpl	%eax, %r15d
	jne	.L560
.L596:
	movq	-80(%rbp), %rbx
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L541
	movl	$3, (%rbx)
	cmpq	$0, 40(%r12)
	leaq	.LC9(%rip), %rbx
	movq	%rbx, 48(%r12)
	je	.L541
.L588:
	movq	%r12, %rdi
	movl	%eax, -68(%rbp)
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
	movl	-68(%rbp), %eax
.L541:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L599
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	.cfi_restore_state
	cmpl	%r14d, %eax
	jle	.L543
	jbe	.L543
	movzwl	2(%rsi,%r9), %ecx
	cmpw	$39, %cx
	jne	.L543
	leal	2(%r15), %r14d
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L575:
	movl	$-1, %ecx
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L595:
	movl	$39, %r8d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %ecx
	movq	%r13, %rsi
	movw	%r8w, -58(%rbp)
	addl	$2, %r14d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	16(%r12), %rdi
	movzwl	8(%rdi), %edx
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L591:
	movq	-80(%rbp), %rbx
	movl	%r14d, %eax
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L541
	movl	$3, (%rbx)
	cmpq	$0, 40(%r12)
	leaq	.LC11(%rip), %rbx
	movq	%rbx, 48(%r12)
	jne	.L588
	jmp	.L541
.L592:
	movq	-80(%rbp), %rbx
	movl	%r14d, %eax
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L541
	movl	$3, (%rbx)
	cmpq	$0, 40(%r12)
	leaq	.LC12(%rip), %rbx
	movq	%rbx, 48(%r12)
	jne	.L588
	jmp	.L541
.L589:
	movq	-80(%rbp), %rbx
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L541
	movl	$3, (%rbx)
	cmpq	$0, 40(%r12)
	leaq	.LC10(%rip), %rbx
	movq	%rbx, 48(%r12)
	jne	.L588
	jmp	.L541
.L599:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4445:
	.size	_ZN6icu_6719CollationRuleParser11parseStringEiRNS_13UnicodeStringER10UErrorCode.part.0, .-_ZN6icu_6719CollationRuleParser11parseStringEiRNS_13UnicodeStringER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser11parseStringEiRNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser11parseStringEiRNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6719CollationRuleParser11parseStringEiRNS_13UnicodeStringER10UErrorCode:
.LFB3345:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L601
	jmp	_ZN6icu_6719CollationRuleParser11parseStringEiRNS_13UnicodeStringER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L601:
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE3345:
	.size	_ZN6icu_6719CollationRuleParser11parseStringEiRNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6719CollationRuleParser11parseStringEiRNS_13UnicodeStringER10UErrorCode
	.section	.rodata.str1.1
.LC13:
	.string	"missing relation string"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser20parseTailoringStringEiRNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser20parseTailoringStringEiRNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6719CollationRuleParser20parseTailoringStringEiRNS_13UnicodeStringER10UErrorCode:
.LFB3344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movslq	%esi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r14, %r12
	addq	%r14, %r14
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L628:
	movswl	%cx, %edx
	sarl	$5, %edx
	cmpl	%edx, %r12d
	jge	.L605
.L629:
	movl	$65535, %edi
	cmpl	%r12d, %edx
	jbe	.L606
	andl	$2, %ecx
	je	.L607
	addq	$10, %rax
.L608:
	movzwl	(%rax,%r14), %edi
.L606:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %r14
	testb	%al, %al
	je	.L605
	addl	$1, %r12d
.L609:
	movq	16(%rbx), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	jns	.L628
	movl	12(%rax), %edx
	cmpl	%edx, %r12d
	jl	.L629
.L605:
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L610
	movl	%r12d, %esi
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_6719CollationRuleParser11parseStringEiRNS_13UnicodeStringER10UErrorCode.part.0
	movl	%eax, %r12d
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L610
	movswl	8(%r13), %eax
	shrl	$5, %eax
	je	.L630
.L610:
	movslq	%r12d, %r13
	addq	%r13, %r13
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L631:
	movswl	%cx, %edx
	sarl	$5, %edx
	cmpl	%edx, %r12d
	jge	.L602
.L632:
	movl	$65535, %edi
	cmpl	%r12d, %edx
	jbe	.L614
	andl	$2, %ecx
	je	.L615
	addq	$10, %rax
.L616:
	movzwl	(%rax,%r13), %edi
.L614:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %r13
	testb	%al, %al
	je	.L602
	addl	$1, %r12d
.L617:
	movq	16(%rbx), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	jns	.L631
	movl	12(%rax), %edx
	cmpl	%edx, %r12d
	jl	.L632
.L602:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	movq	24(%rax), %rax
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L615:
	movq	24(%rax), %rax
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L630:
	leaq	.LC13(%rip), %rax
	cmpq	$0, 40(%rbx)
	movl	$3, (%r15)
	movq	%rax, 48(%rbx)
	je	.L610
	movq	%rbx, %rdi
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
	jmp	.L610
	.cfi_endproc
.LFE3344:
	.size	_ZN6icu_6719CollationRuleParser20parseTailoringStringEiRNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6719CollationRuleParser20parseTailoringStringEiRNS_13UnicodeStringER10UErrorCode
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"in 'prefix|str', prefix and str must each start with an NFC boundary"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser20parseRelationStringsEiiR10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser20parseRelationStringsEiiR10UErrorCode, @function
_ZN6icu_6719CollationRuleParser20parseRelationStringsEiiR10UErrorCode:
.LFB3342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$2, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movslq	%edx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r14, %r12
	addq	%r14, %r14
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$248, %rsp
	movl	%esi, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r11w, -248(%rbp)
	movq	%rax, -256(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%r15w, -184(%rbp)
	movw	%ax, -120(%rbp)
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L698:
	movswl	%cx, %edx
	sarl	$5, %edx
	cmpl	%edx, %r12d
	jge	.L636
.L699:
	movl	$65535, %edi
	cmpl	%r12d, %edx
	jbe	.L637
	andl	$2, %ecx
	je	.L638
	addq	$10, %rax
.L639:
	movzwl	(%rax,%r14), %edi
.L637:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %r14
	testb	%al, %al
	je	.L636
	addl	$1, %r12d
.L640:
	movq	16(%r13), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	jns	.L698
	movl	12(%rax), %edx
	cmpl	%edx, %r12d
	jl	.L699
.L636:
	movl	(%rbx), %r10d
	leaq	-192(%rbp), %r14
	testl	%r10d, %r10d
	jg	.L641
	movl	%r12d, %esi
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6719CollationRuleParser11parseStringEiRNS_13UnicodeStringER10UErrorCode.part.0
	movl	(%rbx), %r9d
	movl	%eax, %r12d
	testl	%r9d, %r9d
	jg	.L641
	movswl	-184(%rbp), %eax
	shrl	$5, %eax
	je	.L700
.L641:
	movslq	%r12d, %r15
	addq	%r15, %r15
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L701:
	movswl	%cx, %edx
	sarl	$5, %edx
	cmpl	%edx, %r12d
	jge	.L644
.L702:
	movl	$65535, %edi
	cmpl	%r12d, %edx
	jbe	.L645
	andl	$2, %ecx
	je	.L646
	addq	$10, %rax
.L647:
	movzwl	(%rax,%r15), %edi
.L645:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %r15
	testb	%al, %al
	je	.L644
	addl	$1, %r12d
.L648:
	movq	16(%r13), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	jns	.L701
	movl	12(%rax), %edx
	cmpl	%edx, %r12d
	jl	.L702
.L644:
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L703
	movq	16(%r13), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	js	.L650
	movswl	%cx, %edx
	sarl	$5, %edx
	cmpl	%r12d, %edx
	jle	.L697
.L692:
	jbe	.L697
	andl	$2, %ecx
	je	.L653
	addq	$10, %rax
.L654:
	movslq	%r12d, %rdx
	leaq	-256(%rbp), %r15
	movzwl	(%rax,%rdx,2), %eax
	cmpw	$124, %ax
	je	.L704
.L655:
	leaq	-128(%rbp), %r8
	cmpw	$47, %ax
	jne	.L652
	leal	1(%r12), %esi
	movq	%r8, %rdx
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN6icu_6719CollationRuleParser20parseTailoringStringEiRNS_13UnicodeStringER10UErrorCode
	movq	-272(%rbp), %r8
	movl	%eax, %r12d
	movswl	-248(%rbp), %eax
	shrl	$5, %eax
	je	.L660
.L705:
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r8, -288(%rbp)
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	%eax, -276(%rbp)
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	8(%r13), %rdi
	movl	-276(%rbp), %r9d
	movl	%eax, -272(%rbp)
	movq	(%rdi), %rax
	movl	%r9d, %esi
	call	*120(%rax)
	movl	-272(%rbp), %r11d
	movq	-288(%rbp), %r8
	testb	%al, %al
	je	.L663
	movq	8(%r13), %rdi
	movq	%r8, -272(%rbp)
	movl	%r11d, %esi
	movq	(%rdi), %rax
	call	*120(%rax)
	movq	-272(%rbp), %r8
	testb	%al, %al
	jne	.L660
.L663:
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L664
	leaq	.LC14(%rip), %rax
	cmpq	$0, 40(%r13)
	movl	$3, (%rbx)
	movq	%rax, 48(%r13)
	je	.L664
	movq	%r13, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
	movq	-264(%rbp), %r8
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L638:
	movq	24(%rax), %rax
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L646:
	movq	24(%rax), %rax
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L650:
	movl	12(%rax), %edx
	cmpl	%r12d, %edx
	jg	.L692
.L697:
	leaq	-256(%rbp), %r15
	leaq	-128(%rbp), %r8
.L652:
	movswl	-248(%rbp), %eax
	shrl	$5, %eax
	jne	.L705
.L660:
	movq	56(%r13), %rdi
	subq	$8, %rsp
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r8, -272(%rbp)
	movl	-264(%rbp), %esi
	leaq	48(%r13), %r9
	movq	(%rdi), %rax
	pushq	%rbx
	call	*32(%rax)
	movl	(%rbx), %ecx
	popq	%rax
	movq	-272(%rbp), %r8
	popq	%rdx
	testl	%ecx, %ecx
	jle	.L665
	movq	%r13, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
	movq	-264(%rbp), %r8
.L665:
	movl	%r12d, 72(%r13)
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L703:
	leaq	-256(%rbp), %r15
	leaq	-128(%rbp), %r8
.L664:
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L706
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L700:
	.cfi_restore_state
	leaq	.LC13(%rip), %rax
	cmpq	$0, 40(%r13)
	movl	$3, (%rbx)
	movq	%rax, 48(%r13)
	je	.L641
	movq	%r13, %rdi
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L653:
	movq	24(%rax), %rax
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L704:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leal	1(%r12), %esi
	movq	%r13, %rdi
	movq	%rbx, %rcx
	movq	%r14, %rdx
	call	_ZN6icu_6719CollationRuleParser20parseTailoringStringEiRNS_13UnicodeStringER10UErrorCode
	movl	(%rbx), %edi
	leaq	-128(%rbp), %r8
	movl	%eax, %r12d
	testl	%edi, %edi
	jg	.L664
	movq	16(%r13), %rdx
	movzwl	8(%rdx), %ecx
	testw	%cx, %cx
	js	.L656
	movswl	%cx, %eax
	sarl	$5, %eax
.L657:
	leaq	-128(%rbp), %r8
	cmpl	%eax, %r12d
	jge	.L652
	cmpl	%r12d, %eax
	jbe	.L652
	andl	$2, %ecx
	je	.L658
	addq	$10, %rdx
.L659:
	movslq	%r12d, %rax
	movzwl	(%rdx,%rax,2), %eax
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L656:
	movl	12(%rdx), %eax
	jmp	.L657
.L658:
	movq	24(%rdx), %rdx
	jmp	.L659
.L706:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3342:
	.size	_ZN6icu_6719CollationRuleParser20parseRelationStringsEiiR10UErrorCode, .-_ZN6icu_6719CollationRuleParser20parseRelationStringsEiiR10UErrorCode
	.section	.rodata.str1.1
.LC15:
	.string	"reset without position"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser21parseResetAndPositionER10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser21parseResetAndPositionER10UErrorCode, @function
_ZN6icu_6719CollationRuleParser21parseResetAndPositionER10UErrorCode:
.LFB3340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L774
	movl	72(%rdi), %eax
	movq	%rdi, %r13
	movq	%rsi, %rbx
	leal	1(%rax), %r12d
	movslq	%r12d, %r14
	addq	%r14, %r14
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L776:
	movswl	%dx, %eax
	sarl	$5, %eax
	cmpl	%eax, %r12d
	jge	.L712
.L777:
	movl	$65535, %r8d
	cmpl	%r12d, %eax
	jbe	.L713
	andl	$2, %edx
	je	.L714
	addq	$10, %rdi
.L715:
	movzwl	(%rdi,%r14), %r8d
.L713:
	movl	%r8d, %edi
	addq	$2, %r14
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	je	.L775
	addl	$1, %r12d
.L717:
	movq	16(%r13), %rdi
	movzwl	8(%rdi), %edx
	testw	%dx, %dx
	jns	.L776
	movl	12(%rdi), %eax
	cmpl	%eax, %r12d
	jl	.L777
.L712:
	xorl	%r8d, %r8d
	movl	$7, %r9d
	movl	$7, %edx
	movl	%r12d, %esi
	leaq	_ZN6icu_6712_GLOBAL__N_1L6BEFOREE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jne	.L718
	movq	16(%r13), %rax
	leal	7(%r12), %edx
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	js	.L719
	movswl	%cx, %esi
	sarl	$5, %esi
	cmpl	%esi, %edx
	jge	.L773
.L720:
	movl	$65535, %edi
	cmpl	%edx, %esi
	jbe	.L723
	andl	$2, %ecx
	je	.L724
	addq	$10, %rax
.L725:
	movslq	%edx, %rdx
	movzwl	(%rax,%rdx,2), %edi
.L723:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	jne	.L726
.L718:
	movq	16(%r13), %rax
	movl	$15, %r14d
	movzwl	8(%rax), %ecx
.L727:
	testw	%cx, %cx
	js	.L748
	movswl	%cx, %edx
.L760:
	movl	%edx, %esi
	sarl	$5, %esi
.L749:
	cmpl	%r12d, %esi
	jle	.L778
	movl	$2, %ecx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rsi
	movw	%cx, -120(%rbp)
	movzwl	8(%rax), %edx
	movq	%rsi, -128(%rbp)
	testw	%dx, %dx
	js	.L752
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L753:
	cmpl	%r12d, %ecx
	jbe	.L754
	andl	$2, %edx
	je	.L755
	addq	$10, %rax
	movslq	%r12d, %rdx
	cmpw	$91, (%rax,%rdx,2)
	je	.L779
.L754:
	leaq	-128(%rbp), %r15
	movq	%rbx, %rcx
	movl	%r12d, %esi
	movq	%r13, %rdi
	movq	%r15, %rdx
	call	_ZN6icu_6719CollationRuleParser20parseTailoringStringEiRNS_13UnicodeStringER10UErrorCode
	movl	%eax, -132(%rbp)
.L757:
	movq	56(%r13), %rdi
	leaq	48(%r13), %rcx
	movq	%rbx, %r8
	movq	%r15, %rdx
	movl	%r14d, %esi
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L758
	movq	%r13, %rdi
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
.L758:
	movl	-132(%rbp), %eax
	movq	%r15, %rdi
	movl	%eax, 72(%r13)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L707:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L780
	addq	$104, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L714:
	.cfi_restore_state
	movq	24(%rdi), %rdi
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L736:
	movl	12(%rax), %esi
	cmpl	%edi, %esi
	jle	.L773
	movl	$15, %r14d
	cmpl	%esi, %r15d
	jb	.L759
	.p2align 4,,10
	.p2align 3
.L748:
	movl	12(%rax), %esi
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L775:
	movq	16(%r13), %rdi
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L752:
	movl	12(%rax), %ecx
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L755:
	movq	24(%rax), %rax
	movslq	%r12d, %rdx
	cmpw	$91, (%rax,%rdx,2)
	jne	.L754
.L779:
	movl	$0, -132(%rbp)
	movl	(%rbx), %edx
	leaq	-128(%rbp), %r15
	testl	%edx, %edx
	jg	.L757
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6719CollationRuleParser20parseSpecialPositionEiRNS_13UnicodeStringER10UErrorCode.part.0
	movl	%eax, -132(%rbp)
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L773:
	movl	$15, %r14d
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L726:
	leal	8(%r12), %r15d
	movslq	%r15d, %r14
	addq	%r14, %r14
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L782:
	movswl	%cx, %edx
	sarl	$5, %edx
.L729:
	cmpl	%edx, %r15d
	jge	.L730
	movl	$65535, %edi
	cmpl	%r15d, %edx
	jbe	.L731
	andl	$2, %ecx
	je	.L732
	addq	$10, %rax
.L733:
	movzwl	(%rax,%r14), %edi
.L731:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %r14
	leal	1(%r15), %edi
	testb	%al, %al
	je	.L781
	movl	%edi, %r15d
.L734:
	movq	16(%r13), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	jns	.L782
	movl	12(%rax), %edx
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L732:
	movq	24(%rax), %rax
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L719:
	movl	12(%rax), %esi
	movl	$15, %r14d
	cmpl	%esi, %edx
	jge	.L749
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L724:
	movq	24(%rax), %rax
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L730:
	leal	1(%r15), %edi
.L735:
	testw	%cx, %cx
	js	.L736
	movswl	%cx, %edx
	movl	%edx, %esi
	sarl	$5, %esi
	cmpl	%edi, %esi
	jle	.L773
	movl	$15, %r14d
	cmpl	%esi, %r15d
	jnb	.L760
.L759:
	leaq	10(%rax), %r9
	testb	$2, %cl
	jne	.L740
	movq	24(%rax), %r9
.L740:
	movslq	%r15d, %rdx
	movl	$15, %r14d
	leaq	(%rdx,%rdx), %r8
	movzwl	(%r9,%rdx,2), %edx
	leal	-49(%rdx), %r10d
	cmpw	$2, %r10w
	ja	.L727
	cmpl	%esi, %edi
	jnb	.L727
	movslq	%edi, %rdi
	cmpw	$93, (%r9,%rdi,2)
	jne	.L727
	movzwl	%dx, %r14d
	leal	2(%r15), %r12d
	leaq	4(%r8), %r15
	subl	$49, %r14d
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L784:
	movswl	%cx, %edx
	sarl	$5, %edx
.L742:
	cmpl	%edx, %r12d
	jge	.L727
	movl	$65535, %edi
	cmpl	%r12d, %edx
	jbe	.L743
	andl	$2, %ecx
	je	.L744
	addq	$10, %rax
.L745:
	movzwl	(%rax,%r15), %edi
.L743:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %r15
	testb	%al, %al
	je	.L783
	movq	16(%r13), %rax
	addl	$1, %r12d
	movzwl	8(%rax), %ecx
.L747:
	testw	%cx, %cx
	jns	.L784
	movl	12(%rax), %edx
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L744:
	movq	24(%rax), %rax
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L783:
	movq	16(%r13), %rax
	movzwl	8(%rax), %ecx
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L778:
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L774
	leaq	.LC15(%rip), %rax
	cmpq	$0, 40(%r13)
	movl	$3, (%rbx)
	movq	%rax, 48(%r13)
	je	.L774
	movq	%r13, %rdi
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
	.p2align 4,,10
	.p2align 3
.L774:
	movl	$-1, %r14d
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L781:
	movq	16(%r13), %rax
	movzwl	8(%rax), %ecx
	jmp	.L735
.L780:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3340:
	.size	_ZN6icu_6719CollationRuleParser21parseResetAndPositionER10UErrorCode, .-_ZN6icu_6719CollationRuleParser21parseResetAndPositionER10UErrorCode
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"missing starred-relation string"
	.align 8
.LC17:
	.string	"starred-relation string is not all NFD-inert"
	.align 8
.LC18:
	.string	"range without start in starred-relation string"
	.align 8
.LC19:
	.string	"range without end in starred-relation string"
	.align 8
.LC20:
	.string	"range start greater than end in starred-relation string"
	.align 8
.LC21:
	.string	"starred-relation string range is not all NFD-inert"
	.align 8
.LC22:
	.string	"starred-relation string range contains a surrogate"
	.align 8
.LC23:
	.string	"starred-relation string range contains U+FFFD, U+FFFE or U+FFFF"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser22parseStarredCharactersEiiR10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser22parseStarredCharactersEiiR10UErrorCode, @function
_ZN6icu_6719CollationRuleParser22parseStarredCharactersEiiR10UErrorCode:
.LFB3343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r9d
	movl	$2, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r14, %r13
	addq	%r14, %r14
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movl	%esi, -268(%rbp)
	movq	%rcx, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r9w, -248(%rbp)
	movq	%rax, -256(%rbp)
	movq	%rax, -192(%rbp)
	movw	%r10w, -184(%rbp)
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L864:
	movswl	%cx, %edx
	sarl	$5, %edx
	cmpl	%edx, %r13d
	jge	.L788
.L865:
	movl	$65535, %edi
	jnb	.L789
	andl	$2, %ecx
	je	.L790
	addq	$10, %rax
.L791:
	movzwl	(%rax,%r14), %edi
.L789:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %r14
	testb	%al, %al
	je	.L788
	addl	$1, %r13d
.L792:
	movq	16(%r12), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	jns	.L864
	movl	12(%rax), %edx
	cmpl	%edx, %r13d
	jl	.L865
.L788:
	movq	-264(%rbp), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L866
	movq	%rax, %rbx
	leaq	-192(%rbp), %rax
	movq	%r12, %rdi
	movl	%r13d, %esi
	movq	%rbx, %rcx
	movq	%rax, %rdx
	movq	%rax, -296(%rbp)
	call	_ZN6icu_6719CollationRuleParser11parseStringEiRNS_13UnicodeStringER10UErrorCode.part.0
	movl	(%rbx), %edi
	movl	%eax, -272(%rbp)
	testl	%edi, %edi
	jg	.L867
	movswl	-184(%rbp), %edx
	movl	%edx, %eax
	shrl	$5, %edx
	je	.L868
	xorl	%ebx, %ebx
	leaq	48(%r12), %rsi
	leaq	-128(%rbp), %r13
	movl	$-1, %r15d
	movq	%rsi, -280(%rbp)
	leaq	-256(%rbp), %r14
	movl	%ebx, %r10d
	testw	%ax, %ax
	js	.L797
.L870:
	sarl	$5, %eax
	cmpl	%eax, %r10d
	jge	.L799
.L871:
	movq	-296(%rbp), %rdi
	movl	%r10d, %esi
	movl	%r10d, -288(%rbp)
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	(%r12), %rdi
	movl	%eax, %r15d
	movq	(%rdi), %rax
	movl	%r15d, %esi
	call	*136(%rax)
	movl	-288(%rbp), %r10d
	testb	%al, %al
	je	.L869
	movq	56(%r12), %r11
	movl	%r15d, %esi
	movq	%r13, %rdi
	movl	%r10d, -308(%rbp)
	movq	(%r11), %rax
	movq	%r11, -304(%rbp)
	movq	32(%rax), %rax
	movq	%rax, -288(%rbp)
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	subq	$8, %rsp
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	-264(%rbp), %rbx
	movl	-268(%rbp), %esi
	movq	%r14, %r8
	movq	-304(%rbp), %r11
	movq	-288(%rbp), %rax
	pushq	%rbx
	movq	-280(%rbp), %r9
	movq	%r11, %rdi
	call	*%rax
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L804
	xorl	%eax, %eax
	movl	-308(%rbp), %r10d
	cmpl	$65535, %r15d
	seta	%al
	leal	1(%r10,%rax), %r10d
.L806:
	movswl	-184(%rbp), %eax
	testw	%ax, %ax
	jns	.L870
.L797:
	movl	-180(%rbp), %eax
	cmpl	%eax, %r10d
	jl	.L871
.L799:
	movq	16(%r12), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	js	.L807
	movswl	%cx, %edx
	sarl	$5, %edx
.L808:
	cmpl	%edx, -272(%rbp)
	jge	.L861
	cmpl	-272(%rbp), %edx
	jbe	.L861
	andl	$2, %ecx
	leaq	10(%rax), %rcx
	je	.L872
.L811:
	movslq	-272(%rbp), %rdx
	cmpw	$45, (%rcx,%rdx,2)
	jne	.L809
	movq	-264(%rbp), %rax
	movl	(%rax), %edx
	testl	%r15d, %r15d
	js	.L873
	movl	-272(%rbp), %esi
	addl	$1, %esi
	testl	%edx, %edx
	jg	.L794
	movq	-296(%rbp), %rdx
	movq	%rax, %rbx
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_6719CollationRuleParser11parseStringEiRNS_13UnicodeStringER10UErrorCode.part.0
	movl	(%rbx), %r11d
	movl	%eax, -272(%rbp)
	testl	%r11d, %r11d
	jg	.L794
	movswl	-184(%rbp), %eax
	shrl	$5, %eax
	je	.L874
	movq	-296(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, -288(%rbp)
	cmpl	%eax, %r15d
	jg	.L875
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r9d
	movq	%rax, -128(%rbp)
	movw	%r9w, -120(%rbp)
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L879:
	sarl	$5, %edx
	movl	%ebx, %ecx
.L863:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7replaceEiii@PLT
	movq	56(%r12), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	-264(%rbp), %r15
	subq	$8, %rsp
	movq	-280(%rbp), %r9
	movq	%r14, %r8
	movq	(%rdi), %rax
	movl	-268(%rbp), %esi
	pushq	%r15
	call	*32(%rax)
	movl	(%r15), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L829
	movl	%ebx, %r15d
.L833:
	leal	1(%r15), %ebx
	cmpl	%ebx, -288(%rbp)
	jl	.L824
	movq	(%r12), %rdi
	movl	%ebx, %esi
	movq	(%rdi), %rax
	call	*136(%rax)
	testb	%al, %al
	je	.L876
	movl	%ebx, %eax
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L877
	leal	-65532(%r15), %edx
	cmpl	$2, %edx
	jbe	.L878
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-120(%rbp), %edx
	testw	%dx, %dx
	jns	.L879
	movl	-116(%rbp), %edx
	movl	%ebx, %ecx
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L790:
	movq	24(%rax), %rax
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L868:
	movq	-264(%rbp), %rax
	cmpq	$0, 40(%r12)
	leaq	-256(%rbp), %r14
	movl	$3, (%rax)
	leaq	.LC16(%rip), %rax
	movq	%rax, 48(%r12)
	je	.L794
.L804:
	movq	%r12, %rdi
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
	.p2align 4,,10
	.p2align 3
.L794:
	movq	-296(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L785:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L880
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_restore_state
	leaq	-192(%rbp), %rax
	leaq	-256(%rbp), %r14
	movq	%rax, -296(%rbp)
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L807:
	movl	12(%rax), %edx
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L872:
	movq	24(%rax), %rcx
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L877:
	movq	-264(%rbp), %rax
	movl	(%rax), %edi
	testl	%edi, %edi
	jg	.L827
	movl	$3, (%rax)
	cmpq	$0, 40(%r12)
	leaq	.LC22(%rip), %rax
	movq	%rax, 48(%r12)
	je	.L827
	.p2align 4,,10
	.p2align 3
.L829:
	movq	%r12, %rdi
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
.L827:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L861:
	movslq	-272(%rbp), %rdx
.L809:
	movl	-272(%rbp), %r13d
	leaq	(%rdx,%rdx), %rbx
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L881:
	movswl	%cx, %edx
	sarl	$5, %edx
.L815:
	cmpl	%edx, %r13d
	jge	.L816
	movl	$65535, %edi
	cmpl	%r13d, %edx
	jbe	.L817
	andl	$2, %ecx
	je	.L818
	addq	$10, %rax
.L819:
	movzwl	(%rax,%rbx), %edi
.L817:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %rbx
	testb	%al, %al
	je	.L816
	movq	16(%r12), %rax
	addl	$1, %r13d
.L820:
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	jns	.L881
	movl	12(%rax), %edx
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L818:
	movq	24(%rax), %rax
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L816:
	movl	%r13d, 72(%r12)
	movq	-296(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L824:
	xorl	%r10d, %r10d
	movq	%r13, %rdi
	movl	$-1, %r15d
	cmpl	$65535, -288(%rbp)
	seta	%r10b
	addl	$1, %r10d
	movl	%r10d, -288(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-288(%rbp), %r10d
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L876:
	movq	-264(%rbp), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L827
	movl	$3, (%rax)
	cmpq	$0, 40(%r12)
	leaq	.LC21(%rip), %rax
	movq	%rax, 48(%r12)
	jne	.L829
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L878:
	movq	-264(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L827
	movl	$3, (%rax)
	cmpq	$0, 40(%r12)
	leaq	.LC23(%rip), %rax
	movq	%rax, 48(%r12)
	jne	.L829
	jmp	.L827
.L869:
	movq	-264(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L794
	movl	$3, (%rax)
	cmpq	$0, 40(%r12)
	leaq	.LC17(%rip), %rax
	movq	%rax, 48(%r12)
	jne	.L804
	jmp	.L794
.L873:
	testl	%edx, %edx
	jg	.L794
	movl	$3, (%rax)
	cmpq	$0, 40(%r12)
	leaq	.LC18(%rip), %rax
	movq	%rax, 48(%r12)
	jne	.L804
	jmp	.L794
.L874:
	movq	-264(%rbp), %rax
	cmpq	$0, 40(%r12)
	movl	$3, (%rax)
	leaq	.LC19(%rip), %rax
	movq	%rax, 48(%r12)
	jne	.L804
	jmp	.L794
.L875:
	movq	-264(%rbp), %rax
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L794
	movl	$3, (%rax)
	cmpq	$0, 40(%r12)
	leaq	.LC20(%rip), %rax
	movq	%rax, 48(%r12)
	jne	.L804
	jmp	.L794
.L880:
	call	__stack_chk_fail@PLT
.L867:
	leaq	-256(%rbp), %r14
	jmp	.L794
	.cfi_endproc
.LFE3343:
	.size	_ZN6icu_6719CollationRuleParser22parseStarredCharactersEiiR10UErrorCode, .-_ZN6icu_6719CollationRuleParser22parseStarredCharactersEiiR10UErrorCode
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"reset not followed by a relation"
	.align 8
.LC25:
	.string	"reset-before strength differs from its first relation"
	.align 8
.LC26:
	.string	"reset-before strength followed by a stronger relation"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser14parseRuleChainER10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser14parseRuleChainER10UErrorCode, @function
_ZN6icu_6719CollationRuleParser14parseRuleChainER10UErrorCode:
.LFB3339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_ZN6icu_6719CollationRuleParser21parseResetAndPositionER10UErrorCode
	movl	(%r15), %edi
	movl	%eax, -52(%rbp)
.L883:
	testl	%edi, %edi
	jg	.L882
	movslq	72(%r14), %rsi
	movq	%rsi, %rbx
	leaq	(%rsi,%rsi), %r12
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L968:
	movswl	%cx, %edx
	sarl	$5, %edx
	cmpl	%edx, %ebx
	jge	.L888
.L969:
	movl	$65535, %edi
	cmpl	%ebx, %edx
	jbe	.L889
	andl	$2, %ecx
	je	.L890
	addq	$10, %rax
.L891:
	movzwl	(%rax,%r12), %edi
.L889:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %r12
	testb	%al, %al
	je	.L967
	addl	$1, %ebx
.L893:
	movq	16(%r14), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	jns	.L968
	movl	12(%rax), %edx
	cmpl	%edx, %ebx
	jl	.L969
.L888:
	movl	%ebx, 72(%r14)
	testw	%cx, %cx
	js	.L894
.L975:
	movswl	%cx, %esi
	sarl	$5, %esi
.L895:
	movl	(%r15), %edi
	cmpl	%ebx, %esi
	jle	.L970
	leal	1(%rbx), %edx
	movl	%edx, %r9d
	jbe	.L907
	leaq	10(%rax), %r10
	testb	$2, %cl
	jne	.L902
	movq	24(%rax), %r10
.L902:
	movslq	%ebx, %r8
	leaq	(%r8,%r8), %r11
	movzwl	(%r10,%r8,2), %r8d
	cmpw	$60, %r8w
	je	.L903
	ja	.L904
	cmpw	$44, %r8w
	je	.L905
	cmpw	$59, %r8w
	jne	.L907
	testl	%edi, %edi
	jg	.L882
	movl	$257, %edx
.L921:
	movl	%edx, %esi
	andl	$15, %esi
	cmpl	$14, -52(%rbp)
	jg	.L971
.L939:
	testb	%r13b, %r13b
	jne	.L972
.L934:
	cmpl	%esi, -52(%rbp)
	jg	.L973
.L933:
	movl	%edx, %r8d
	sarl	$8, %r8d
	addl	%ebx, %r8d
	andl	$16, %edx
	je	.L938
.L936:
	movq	%r15, %rcx
	movl	%r8d, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6719CollationRuleParser22parseStarredCharactersEiiR10UErrorCode
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L890:
	movq	24(%rax), %rax
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L904:
	cmpw	$61, %r8w
	jne	.L907
	cmpl	%edx, %esi
	jg	.L974
.L918:
	testl	%edi, %edi
	jg	.L882
	movl	$271, %edx
	movl	%edx, %esi
	andl	$15, %esi
	cmpl	$14, -52(%rbp)
	jle	.L939
	.p2align 4,,10
	.p2align 3
.L971:
	sarl	$8, %edx
	leal	(%rdx,%rbx), %r8d
.L938:
	movq	%r15, %rcx
	movl	%r8d, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6719CollationRuleParser20parseRelationStringsEiiR10UErrorCode
.L937:
	movl	(%r15), %edi
	testl	%edi, %edi
	jg	.L882
	xorl	%r13d, %r13d
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L967:
	movq	16(%r14), %rax
	movzwl	8(%rax), %ecx
	movl	%ebx, 72(%r14)
	testw	%cx, %cx
	jns	.L975
.L894:
	movl	12(%rax), %esi
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L907:
	testl	%edi, %edi
	jg	.L882
	cmpl	%ebx, %esi
	jbe	.L898
	andl	$2, %ecx
	jne	.L976
	movq	24(%rax), %rax
.L925:
	movslq	%ebx, %rbx
	cmpw	$35, (%rax,%rbx,2)
	jne	.L898
	movslq	%edx, %rdx
	.p2align 4,,10
	.p2align 3
.L931:
	movl	%edx, %r8d
	cmpl	%edx, %esi
	jle	.L928
	leal	1(%rdx), %r8d
	jbe	.L977
	movzwl	(%rax,%rdx,2), %ecx
	leal	-12(%rcx), %r9d
	cmpw	$1, %r9w
	jbe	.L928
	cmpw	$10, %cx
	je	.L928
	cmpw	$133, %cx
	je	.L928
	subw	$8232, %cx
	addq	$1, %rdx
	cmpw	$1, %cx
	ja	.L931
.L928:
	movl	%r8d, 72(%r14)
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L970:
	testl	%edi, %edi
	jg	.L882
	.p2align 4,,10
	.p2align 3
.L898:
	testb	%r13b, %r13b
	jne	.L978
.L882:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L976:
	.cfi_restore_state
	addq	$10, %rax
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L972:
	cmpl	%esi, -52(%rbp)
	je	.L933
.L944:
	leaq	.LC25(%rip), %rax
	cmpq	$0, 40(%r14)
	movl	$3, (%r15)
	movq	%rax, 48(%r14)
	je	.L882
.L935:
	addq	$24, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6719CollationRuleParser15setErrorContextEv
	.p2align 4,,10
	.p2align 3
.L974:
	.cfi_restore_state
	jbe	.L918
	cmpw	$42, 2(%r10,%r11)
	jne	.L918
	testl	%edi, %edi
	jg	.L882
	cmpl	$14, -52(%rbp)
	jg	.L979
	testb	%r13b, %r13b
	jne	.L944
	movl	$15, %esi
	movl	$543, %edx
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L977:
	addq	$1, %rdx
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L903:
	cmpl	%edx, %esi
	jg	.L980
	testl	%edi, %edi
	jg	.L882
	movl	$256, %edx
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L978:
	leaq	.LC24(%rip), %rax
	cmpq	$0, 40(%r14)
	movl	$3, (%r15)
	movq	%rax, 48(%r14)
	jne	.L935
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L980:
	movl	$0, %eax
	jbe	.L964
	cmpw	$60, 2(%r10,%r11)
	je	.L981
.L915:
	movslq	%r9d, %rdx
	cmpw	$42, (%r10,%rdx,2)
	je	.L917
.L964:
	movl	%r9d, %edx
	subl	%ebx, %edx
	sall	$8, %edx
	orl	%eax, %edx
.L916:
	testl	%edi, %edi
	jg	.L882
	movl	%edx, %esi
	andl	$15, %esi
	cmpl	$14, -52(%rbp)
	jg	.L933
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L917:
	leal	1(%r9), %edx
	subl	%ebx, %edx
	sall	$8, %edx
	orl	%eax, %edx
	orl	$16, %edx
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L981:
	leal	2(%rbx), %r9d
	cmpl	%r9d, %esi
	jle	.L912
	movl	$1, %eax
	jbe	.L964
	cmpw	$60, 4(%r10,%r11)
	jne	.L915
	leal	3(%rbx), %r9d
	cmpl	%r9d, %esi
	jle	.L913
	movl	$2, %eax
	jbe	.L964
	cmpw	$60, 6(%r10,%r11)
	jne	.L915
	leal	4(%rbx), %r9d
	cmpl	%r9d, %esi
	jle	.L914
	movl	$3, %eax
	ja	.L915
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L912:
	testl	%edi, %edi
	jg	.L882
	movl	$513, %edx
	jmp	.L921
.L973:
	leaq	.LC26(%rip), %rax
	cmpq	$0, 40(%r14)
	movl	$3, (%r15)
	movq	%rax, 48(%r14)
	jne	.L935
	jmp	.L882
.L913:
	testl	%edi, %edi
	jg	.L882
	movl	$770, %edx
	jmp	.L921
.L914:
	testl	%edi, %edi
	jg	.L882
	movl	$1027, %edx
	jmp	.L921
.L905:
	testl	%edi, %edi
	jg	.L882
	movl	$258, %edx
	jmp	.L921
.L979:
	leal	2(%rbx), %r8d
	movl	$15, %esi
	jmp	.L936
	.cfi_endproc
.LFE3339:
	.size	_ZN6icu_6719CollationRuleParser14parseRuleChainER10UErrorCode, .-_ZN6icu_6719CollationRuleParser14parseRuleChainER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser12isSyntaxCharEi
	.type	_ZN6icu_6719CollationRuleParser12isSyntaxCharEi, @function
_ZN6icu_6719CollationRuleParser12isSyntaxCharEi:
.LFB3356:
	.cfi_startproc
	endbr64
	leal	-33(%rdi), %edx
	xorl	%eax, %eax
	cmpl	$93, %edx
	ja	.L982
	leal	-58(%rdi), %eax
	cmpl	$6, %eax
	jbe	.L985
	leal	-48(%rdi), %eax
	cmpl	$74, %eax
	ja	.L985
	subl	$91, %edi
	cmpl	$5, %edi
	setbe	%al
	ret
	.p2align 4,,10
	.p2align 3
.L985:
	movl	$1, %eax
.L982:
	ret
	.cfi_endproc
.LFE3356:
	.size	_ZN6icu_6719CollationRuleParser12isSyntaxCharEi, .-_ZN6icu_6719CollationRuleParser12isSyntaxCharEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CollationRuleParser14skipWhiteSpaceEi
	.type	_ZNK6icu_6719CollationRuleParser14skipWhiteSpaceEi, @function
_ZNK6icu_6719CollationRuleParser14skipWhiteSpaceEi:
.LFB3357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movslq	%esi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%r13, %r12
	addq	%r13, %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L999:
	movswl	%cx, %edx
	sarl	$5, %edx
	cmpl	%edx, %r12d
	jge	.L989
.L1000:
	movl	$65535, %edi
	cmpl	%r12d, %edx
	jbe	.L990
	andl	$2, %ecx
	je	.L991
	addq	$10, %rax
.L992:
	movzwl	(%rax,%r13), %edi
.L990:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %r13
	testb	%al, %al
	je	.L989
	addl	$1, %r12d
.L993:
	movq	16(%rbx), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	jns	.L999
	movl	12(%rax), %edx
	cmpl	%edx, %r12d
	jl	.L1000
.L989:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L991:
	.cfi_restore_state
	movq	24(%rax), %rax
	jmp	.L992
	.cfi_endproc
.LFE3357:
	.size	_ZNK6icu_6719CollationRuleParser14skipWhiteSpaceEi, .-_ZNK6icu_6719CollationRuleParser14skipWhiteSpaceEi
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"unknown script or reorder code"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser15parseReorderingERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser15parseReorderingERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6719CollationRuleParser15parseReorderingERKNS_13UnicodeStringER10UErrorCode:
.LFB3348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -248(%rbp)
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L1001
	movswl	8(%rsi), %eax
	movq	%rsi, %r13
	movq	%rdx, %r12
	testw	%ax, %ax
	js	.L1003
	sarl	$5, %eax
	cmpl	$7, %eax
	je	.L1034
.L1005:
	leaq	-224(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L1006
	leaq	-179(%rbp), %rax
	xorl	%esi, %esi
	movl	$0, -136(%rbp)
	movl	$7, %ebx
	movq	%rax, -192(%rbp)
	movl	$40, -184(%rbp)
	movw	%si, -180(%rbp)
	.p2align 4,,10
	.p2align 3
.L1021:
	movswl	8(%r13), %ecx
	testw	%cx, %cx
	js	.L1007
	sarl	$5, %ecx
.L1008:
	cmpl	%ecx, %ebx
	jge	.L1009
	xorl	%edx, %edx
	addl	$1, %ebx
	movl	%ebx, %r8d
	js	.L1010
	cmpl	%ecx, %ebx
	movl	%ecx, %edx
	cmovle	%ebx, %edx
	subl	%edx, %ecx
.L1010:
	movl	$32, %esi
	movq	%r13, %rdi
	movl	%r8d, -232(%rbp)
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	-232(%rbp), %r8d
	testl	%eax, %eax
	movl	%eax, %ebx
	js	.L1035
.L1011:
	movq	-192(%rbp), %rax
	leaq	-128(%rbp), %r15
	movl	%ebx, %ecx
	movl	%r8d, %edx
	movl	$0, -136(%rbp)
	subl	%r8d, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movb	$0, (%rax)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	leaq	-192(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L1017
	movq	-192(%rbp), %r15
	xorl	%edx, %edx
.L1016:
	leaq	_ZN6icu_67L20gSpecialReorderCodesE(%rip), %rax
	movq	%r15, %rdi
	movl	%edx, -236(%rbp)
	movq	(%rax,%rdx,8), %rsi
	movq	%rdx, -232(%rbp)
	call	uprv_stricmp_67@PLT
	movq	-232(%rbp), %rdx
	testl	%eax, %eax
	je	.L1036
	addq	$1, %rdx
	cmpq	$5, %rdx
	jne	.L1016
	movq	%r15, %rsi
	movl	$4106, %edi
	call	u_getPropertyValueEnum_67@PLT
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L1037
.L1015:
	movslq	-216(%rbp), %rdx
	movl	%edx, %esi
	addl	$1, %esi
	js	.L1018
	cmpl	-212(%rbp), %esi
	jle	.L1019
.L1018:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movl	%ecx, -232(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L1020
	movslq	-216(%rbp), %rdx
	movl	-232(%rbp), %ecx
.L1019:
	movq	-200(%rbp), %rax
	movl	%ecx, (%rax,%rdx,4)
	addl	$1, -216(%rbp)
.L1020:
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L1021
.L1017:
	cmpb	$0, -180(%rbp)
	je	.L1006
.L1033:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	%r14, %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
.L1001:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1038
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1003:
	.cfi_restore_state
	movl	12(%rsi), %eax
	cmpl	$7, %eax
	jne	.L1005
.L1034:
	movq	-248(%rbp), %rax
	movq	32(%rax), %rdi
	call	_ZN6icu_6717CollationSettings15resetReorderingEv@PLT
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1007:
	movl	12(%r13), %ecx
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1035:
	movswl	8(%r13), %ebx
	testw	%bx, %bx
	js	.L1012
	sarl	$5, %ebx
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1037:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	uprv_stricmp_67@PLT
	testl	%eax, %eax
	jne	.L1039
	movl	$103, %ecx
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1036:
	movl	-236(%rbp), %ecx
	addl	$4096, %ecx
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1012:
	movl	12(%r13), %ebx
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	-248(%rbp), %rax
	movl	-216(%rbp), %ecx
	movq	%r12, %r8
	movq	-200(%rbp), %rdx
	movq	24(%rax), %rsi
	movq	32(%rax), %rdi
	call	_ZN6icu_6717CollationSettings13setReorderingERKNS_13CollationDataEPKiiR10UErrorCode@PLT
	cmpb	$0, -180(%rbp)
	je	.L1006
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1039:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L1017
	movq	-248(%rbp), %rax
	leaq	.LC27(%rip), %rbx
	movl	$3, (%r12)
	cmpq	$0, 40(%rax)
	movq	%rbx, 48(%rax)
	je	.L1017
	movq	%rax, %rdi
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
	jmp	.L1017
.L1038:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3348:
	.size	_ZN6icu_6719CollationRuleParser15parseReorderingERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6719CollationRuleParser15parseReorderingERKNS_13UnicodeStringER10UErrorCode
	.section	.rodata.str1.1
.LC28:
	.string	"standard"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"expected a setting/option at '['"
	.section	.rodata.str2.2
	.align 2
.LC30:
	.string	"r"
	.string	"e"
	.string	"o"
	.string	"r"
	.string	"d"
	.string	"e"
	.string	"r"
	.string	""
	.string	""
	.align 2
.LC31:
	.string	"b"
	.string	"a"
	.string	"c"
	.string	"k"
	.string	"w"
	.string	"a"
	.string	"r"
	.string	"d"
	.string	"s"
	.string	" "
	.string	"2"
	.string	""
	.string	""
	.align 2
.LC32:
	.string	"s"
	.string	"t"
	.string	"r"
	.string	"e"
	.string	"n"
	.string	"g"
	.string	"t"
	.string	"h"
	.string	""
	.string	""
	.align 2
.LC33:
	.string	"a"
	.string	"l"
	.string	"t"
	.string	"e"
	.string	"r"
	.string	"n"
	.string	"a"
	.string	"t"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC34:
	.string	"n"
	.string	"o"
	.string	"n"
	.string	"-"
	.string	"i"
	.string	"g"
	.string	"n"
	.string	"o"
	.string	"r"
	.string	"a"
	.string	"b"
	.string	"l"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC35:
	.string	"s"
	.string	"h"
	.string	"i"
	.string	"f"
	.string	"t"
	.string	"e"
	.string	"d"
	.string	""
	.string	""
	.align 2
.LC36:
	.string	"m"
	.string	"a"
	.string	"x"
	.string	"V"
	.string	"a"
	.string	"r"
	.string	"i"
	.string	"a"
	.string	"b"
	.string	"l"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC37:
	.string	"s"
	.string	"p"
	.string	"a"
	.string	"c"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC38:
	.string	"p"
	.string	"u"
	.string	"n"
	.string	"c"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC39:
	.string	"s"
	.string	"y"
	.string	"m"
	.string	"b"
	.string	"o"
	.string	"l"
	.string	""
	.string	""
	.align 2
.LC40:
	.string	"c"
	.string	"u"
	.string	"r"
	.string	"r"
	.string	"e"
	.string	"n"
	.string	"c"
	.string	"y"
	.string	""
	.string	""
	.align 2
.LC41:
	.string	"c"
	.string	"a"
	.string	"s"
	.string	"e"
	.string	"F"
	.string	"i"
	.string	"r"
	.string	"s"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC42:
	.string	"l"
	.string	"o"
	.string	"w"
	.string	"e"
	.string	"r"
	.string	""
	.string	""
	.align 2
.LC43:
	.string	"u"
	.string	"p"
	.string	"p"
	.string	"e"
	.string	"r"
	.string	""
	.string	""
	.align 2
.LC44:
	.string	"c"
	.string	"a"
	.string	"s"
	.string	"e"
	.string	"L"
	.string	"e"
	.string	"v"
	.string	"e"
	.string	"l"
	.string	""
	.string	""
	.align 2
.LC45:
	.string	"n"
	.string	"o"
	.string	"r"
	.string	"m"
	.string	"a"
	.string	"l"
	.string	"i"
	.string	"z"
	.string	"a"
	.string	"t"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC46:
	.string	"n"
	.string	"u"
	.string	"m"
	.string	"e"
	.string	"r"
	.string	"i"
	.string	"c"
	.string	"O"
	.string	"r"
	.string	"d"
	.string	"e"
	.string	"r"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC47:
	.string	"h"
	.string	"i"
	.string	"r"
	.string	"a"
	.string	"g"
	.string	"a"
	.string	"n"
	.string	"a"
	.string	"Q"
	.string	""
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"[hiraganaQ on] is not supported"
	.section	.rodata.str2.2
	.align 2
.LC49:
	.string	"i"
	.string	"m"
	.string	"p"
	.string	"o"
	.string	"r"
	.string	"t"
	.string	""
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"expected language tag in [import langTag]"
	.section	.rodata.str1.1
.LC51:
	.string	"collation"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"[import langTag] is not supported"
	.section	.rodata.str1.1
.LC53:
	.string	"[import langTag] failed"
	.section	.rodata.str2.2
	.align 2
.LC54:
	.string	"o"
	.string	"p"
	.string	"t"
	.string	"i"
	.string	"m"
	.string	"i"
	.string	"z"
	.string	"e"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC55:
	.string	"s"
	.string	"u"
	.string	"p"
	.string	"p"
	.string	"r"
	.string	"e"
	.string	"s"
	.string	"s"
	.string	"C"
	.string	"o"
	.string	"n"
	.string	"t"
	.string	"r"
	.string	"a"
	.string	"c"
	.string	"t"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	"s"
	.string	""
	.string	""
	.section	.rodata.str1.1
.LC56:
	.string	"not a valid setting/option"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser12parseSettingER10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser12parseSettingER10UErrorCode, @function
_ZN6icu_6719CollationRuleParser12parseSettingER10UErrorCode:
.LFB3347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$888, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L1362
.L1040:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1363
	addq	$888, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1362:
	.cfi_restore_state
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-880(%rbp), %r12
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	%rax, -880(%rbp)
	movl	72(%rdi), %eax
	movl	$2, %r8d
	movq	%r12, %rdx
	movw	%r8w, -872(%rbp)
	leal	1(%rax), %r13d
	movl	%r13d, %esi
	call	_ZNK6icu_6719CollationRuleParser9readWordsEiRNS_13UnicodeStringE
	movl	%eax, %r14d
	cmpl	%eax, %r13d
	jl	.L1364
.L1042:
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L1043
	leaq	.LC29(%rip), %rax
	cmpq	$0, 40(%r15)
	movl	$3, (%rbx)
	movq	%rax, 48(%r15)
	je	.L1043
	movq	%r15, %rdi
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
.L1043:
	movq	16(%r15), %rdx
	movzwl	8(%rdx), %eax
	testw	%ax, %ax
	js	.L1045
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1046:
	cmpl	%r14d, %ecx
	jbe	.L1047
	movslq	%r14d, %rcx
	leaq	(%rcx,%rcx), %rsi
	testb	$2, %al
	jne	.L1365
	movq	24(%rdx), %rax
	movzwl	(%rax,%rcx,2), %eax
	cmpw	$93, %ax
	je	.L1049
.L1050:
	cmpw	$91, %ax
	jne	.L1047
	leaq	-688(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6719CollationRuleParser15parseUnicodeSetEiRNS_10UnicodeSetER10UErrorCode
	movl	(%rbx), %ecx
	movl	%eax, -900(%rbp)
	testl	%ecx, %ecx
	jle	.L1366
.L1202:
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1201:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1040
.L1214:
	testw	%ax, %ax
	js	.L1216
	movswl	%ax, %edx
	sarl	$5, %edx
.L1217:
	movzwl	-744(%rbp), %ecx
	testw	%cx, %cx
	js	.L1218
	movswl	%cx, %eax
	sarl	$5, %eax
.L1219:
	cmpl	%edx, %eax
	jne	.L1255
	andl	$1, %ecx
	je	.L1220
.L1255:
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1222:
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1047:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1201
	leaq	.LC56(%rip), %rax
	cmpq	$0, 40(%r15)
	movl	$3, (%rbx)
	movq	%rax, 48(%r15)
	je	.L1201
	movq	%r15, %rdi
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1364:
	movswl	-872(%rbp), %eax
	shrl	$5, %eax
	jne	.L1043
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1365:
	movzwl	10(%rdx,%rsi), %eax
	cmpw	$93, %ax
	jne	.L1050
.L1049:
	leal	1(%r14), %eax
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	-888(%rbp), %r14
	leaq	-688(%rbp), %r13
	movl	%eax, -900(%rbp)
	movq	%r14, %rdx
	leaq	.LC30(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-680(%rbp), %eax
	testw	%ax, %ax
	js	.L1051
	testb	$1, %al
	jne	.L1052
	movswl	%ax, %edx
	xorl	%r8d, %r8d
	sarl	$5, %edx
.L1234:
	movl	%edx, %r9d
	subl	%r8d, %r9d
	cmpl	%edx, %r9d
	cmovg	%edx, %r9d
.L1056:
	testb	$2, %al
	leaq	-678(%rbp), %rcx
	movq	%r12, %rdi
	cmove	-664(%rbp), %rcx
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
.L1055:
	testb	%al, %al
	jne	.L1058
	movzwl	-872(%rbp), %edx
	testw	%dx, %dx
	js	.L1059
	movswl	%dx, %eax
	sarl	$5, %eax
.L1060:
	cmpl	$7, %eax
	je	.L1061
	jbe	.L1058
	andl	$2, %edx
	leaq	-870(%rbp), %rax
	movq	%r13, %rdi
	cmove	-856(%rbp), %rax
	cmpw	$32, 14(%rax)
	je	.L1358
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1066:
	movl	$-1, %ecx
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	.LC31(%rip), %rax
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-872(%rbp), %eax
	testb	$1, %al
	je	.L1068
	movzbl	-680(%rbp), %eax
	andl	$1, %eax
	movb	%al, -912(%rbp)
.L1069:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -912(%rbp)
	je	.L1076
	movq	32(%r15), %rdi
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movl	$17, %edx
	movl	$2048, %esi
	call	_ZN6icu_6717CollationSettings7setFlagEi18UColAttributeValueiR10UErrorCode@PLT
	movl	-900(%rbp), %eax
	movl	%eax, 72(%r15)
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1045:
	movl	12(%rdx), %ecx
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1051:
	movl	-676(%rbp), %edx
	testb	$1, %al
	jne	.L1052
	testl	%edx, %edx
	movl	$0, %r8d
	cmovle	%edx, %r8d
	jns	.L1234
	xorl	%r9d, %r9d
	jmp	.L1056
	.p2align 4,,10
	.p2align 3
.L1366:
	leaq	-752(%rbp), %r8
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	-888(%rbp), %r14
	leaq	.LC54(%rip), %rax
	movq	%r8, %rdi
	movq	%r8, -912(%rbp)
	movq	%r14, %rdx
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-872(%rbp), %eax
	movq	-912(%rbp), %r8
	testb	$1, %al
	je	.L1203
	movzbl	-744(%rbp), %eax
	andl	$1, %eax
	movb	%al, -912(%rbp)
.L1204:
	movq	%r8, %rdi
	movq	%r8, -920(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -912(%rbp)
	movq	-920(%rbp), %r8
	je	.L1211
	movq	56(%r15), %rdi
	leaq	_ZN6icu_6719CollationRuleParser4Sink20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1361
.L1223:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L1224
	movq	%r15, %rdi
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
.L1224:
	movl	-900(%rbp), %eax
	movl	%eax, 72(%r15)
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1068:
	testw	%ax, %ax
	js	.L1070
	movswl	%ax, %edx
	sarl	$5, %edx
.L1071:
	movzwl	-680(%rbp), %ecx
	testw	%cx, %cx
	js	.L1072
	movswl	%cx, %eax
	sarl	$5, %eax
.L1073:
	cmpl	%edx, %eax
	jne	.L1247
	andl	$1, %ecx
	je	.L1074
.L1247:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1076:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %esi
	movq	%rax, -816(%rbp)
	movzwl	-872(%rbp), %eax
	movw	%si, -808(%rbp)
	testw	%ax, %ax
	js	.L1077
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1078:
	xorl	%edx, %edx
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString13doLastIndexOfEDsii@PLT
	movl	%eax, %r10d
	testl	%eax, %eax
	jns	.L1232
	leaq	-816(%rbp), %rax
	movq	%rax, -912(%rbp)
.L1083:
	movl	$-1, %ecx
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	.LC32(%rip), %rax
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-872(%rbp), %eax
	testb	$1, %al
	je	.L1088
	movzbl	-680(%rbp), %eax
	andl	$1, %eax
.L1089:
	testb	%al, %al
	je	.L1094
	movswl	-808(%rbp), %eax
	testw	%ax, %ax
	js	.L1097
	sarl	$5, %eax
.L1098:
	movq	%r13, %rdi
	cmpl	$1, %eax
	je	.L1099
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1096:
	movl	$-1, %ecx
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	.LC33(%rip), %rax
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-872(%rbp), %eax
	testb	$1, %al
	je	.L1107
	movzbl	-680(%rbp), %eax
	andl	$1, %eax
	movb	%al, -920(%rbp)
.L1108:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -920(%rbp)
	je	.L1115
	movl	$-1, %ecx
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	.LC34(%rip), %rax
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-808(%rbp), %eax
	testb	$1, %al
	je	.L1116
	movzbl	-680(%rbp), %eax
	andl	$1, %eax
	movb	%al, -920(%rbp)
.L1117:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -920(%rbp)
	movl	$21, %esi
	je	.L1124
.L1125:
	movq	32(%r15), %rdi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	call	_ZN6icu_6717CollationSettings20setAlternateHandlingE18UColAttributeValueiR10UErrorCode@PLT
	movl	-900(%rbp), %eax
	movl	%eax, 72(%r15)
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	-912(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1052:
	movzbl	-872(%rbp), %eax
	notl	%eax
	andl	$1, %eax
	jmp	.L1055
.L1099:
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzwl	-808(%rbp), %eax
	testw	%ax, %ax
	js	.L1100
	movswl	%ax, %edx
	sarl	$5, %edx
.L1101:
	testl	%edx, %edx
	je	.L1105
	leaq	-806(%rbp), %rdx
	testb	$2, %al
	movq	%rdx, %rax
	cmove	-792(%rbp), %rax
	movzwl	(%rax), %eax
	leal	-49(%rax), %edx
	leal	-49(%rax), %esi
	cmpw	$3, %dx
	jbe	.L1104
	cmpw	$73, %ax
	je	.L1367
.L1105:
	movq	-912(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1094:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1088:
	testw	%ax, %ax
	js	.L1090
	movswl	%ax, %edx
	sarl	$5, %edx
.L1091:
	movzwl	-680(%rbp), %eax
	testw	%ax, %ax
	js	.L1092
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1093:
	cmpl	%edx, %ecx
	jne	.L1094
	testb	$1, %al
	jne	.L1094
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	%r13, %rdi
.L1358:
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6719CollationRuleParser15parseReorderingERKNS_13UnicodeStringER10UErrorCode
	movl	-900(%rbp), %eax
	movl	%eax, 72(%r15)
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1077:
	movl	-868(%rbp), %ecx
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1059:
	movl	-868(%rbp), %eax
	jmp	.L1060
.L1367:
	movl	$15, %esi
.L1104:
	movq	32(%r15), %rdi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	call	_ZN6icu_6717CollationSettings11setStrengthEiiR10UErrorCode@PLT
	movl	-900(%rbp), %eax
	movl	%eax, 72(%r15)
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1203:
	testw	%ax, %ax
	js	.L1205
	movswl	%ax, %edx
	sarl	$5, %edx
.L1206:
	movzwl	-744(%rbp), %eax
	testw	%ax, %ax
	js	.L1207
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1208:
	testb	$1, %al
	jne	.L1254
	cmpl	%edx, %ecx
	je	.L1209
.L1254:
	movq	%r8, %rdi
	movq	%r8, -912(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-912(%rbp), %r8
.L1211:
	movq	%r8, %rdi
	movl	$-1, %ecx
	movq	%r14, %rdx
	movl	$1, %esi
	leaq	.LC55(%rip), %rax
	movq	%r8, -912(%rbp)
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-872(%rbp), %eax
	movq	-912(%rbp), %r8
	testb	$1, %al
	je	.L1214
	movzbl	-744(%rbp), %r14d
	andl	$1, %r14d
.L1215:
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testb	%r14b, %r14b
	je	.L1222
	movq	56(%r15), %rdi
	leaq	_ZN6icu_6719CollationRuleParser4Sink20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode(%rip), %rdx
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1223
.L1361:
	leaq	48(%r15), %rdx
	movq	%rbx, %rcx
	movq	%r13, %rsi
	call	*%rax
	jmp	.L1223
.L1100:
	movl	-804(%rbp), %edx
	jmp	.L1101
.L1072:
	movl	-676(%rbp), %eax
	jmp	.L1073
.L1070:
	movl	-868(%rbp), %edx
	jmp	.L1071
.L1232:
	leaq	-816(%rbp), %rax
	leal	1(%r10), %r8d
	movl	%r10d, -904(%rbp)
	movq	%rax, %rdi
	movl	%r8d, -920(%rbp)
	movq	%rax, -912(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	-872(%rbp), %eax
	movl	-920(%rbp), %r8d
	movl	-904(%rbp), %r10d
	testw	%ax, %ax
	js	.L1368
	movswl	%ax, %r9d
	sarl	$5, %r9d
	cmpl	%r8d, %r9d
	cmovle	%r9d, %r8d
.L1079:
	movzwl	-808(%rbp), %eax
	subl	%r8d, %r9d
	testw	%ax, %ax
	js	.L1080
	movswl	%ax, %edx
	sarl	$5, %edx
.L1081:
	movq	-912(%rbp), %rdi
	movq	%r12, %rcx
	xorl	%esi, %esi
	movl	%r10d, -920(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movl	-920(%rbp), %r10d
	movzwl	-872(%rbp), %eax
	testl	%r10d, %r10d
	jne	.L1082
	testb	$1, %al
	jne	.L1369
.L1082:
	testw	%ax, %ax
	js	.L1084
	movswl	%ax, %edx
	sarl	$5, %edx
.L1085:
	cmpl	%r10d, %edx
	jbe	.L1083
	cmpl	$1023, %r10d
	jg	.L1087
	movl	%r10d, %edx
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, -872(%rbp)
	jmp	.L1083
.L1097:
	movl	-804(%rbp), %eax
	jmp	.L1098
.L1092:
	movl	-676(%rbp), %ecx
	jmp	.L1093
.L1090:
	movl	-868(%rbp), %edx
	jmp	.L1091
.L1080:
	movl	-804(%rbp), %edx
	jmp	.L1081
.L1368:
	movl	-868(%rbp), %r9d
	cmpl	%r8d, %r9d
	cmovle	%r9d, %r8d
	jmp	.L1079
.L1107:
	testw	%ax, %ax
	js	.L1109
	movswl	%ax, %edx
	sarl	$5, %edx
.L1110:
	movzwl	-680(%rbp), %eax
	testw	%ax, %ax
	js	.L1111
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1112:
	testb	$1, %al
	jne	.L1248
	cmpl	%edx, %ecx
	je	.L1113
.L1248:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1115:
	movl	$-1, %ecx
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	.LC36(%rip), %rax
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-872(%rbp), %eax
	testb	$1, %al
	je	.L1134
	movzbl	-680(%rbp), %eax
	andl	$1, %eax
	movb	%al, -920(%rbp)
.L1135:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -920(%rbp)
	je	.L1142
	movl	$-1, %ecx
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	.LC37(%rip), %rax
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-808(%rbp), %eax
	testb	$1, %al
	je	.L1143
	movzbl	-680(%rbp), %eax
	andl	$1, %eax
.L1144:
	movq	%r13, %rdi
	movb	%al, -920(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-920(%rbp), %eax
	xorl	%esi, %esi
	testb	%al, %al
	je	.L1151
.L1152:
	movq	32(%r15), %rdi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movl	%esi, -920(%rbp)
	call	_ZN6icu_6717CollationSettings14setMaxVariableEiiR10UErrorCode@PLT
	movl	-920(%rbp), %esi
	movq	24(%r15), %rdi
	movq	32(%r15), %rbx
	addl	$4096, %esi
	call	_ZNK6icu_6713CollationData22getLastPrimaryForGroupEi@PLT
	movl	%eax, 28(%rbx)
	movl	-900(%rbp), %eax
	movl	%eax, 72(%r15)
	jmp	.L1106
.L1084:
	movl	-868(%rbp), %edx
	jmp	.L1085
.L1205:
	movl	-868(%rbp), %edx
	jmp	.L1206
.L1207:
	movl	-740(%rbp), %ecx
	jmp	.L1208
.L1116:
	testw	%ax, %ax
	js	.L1118
	sarl	$5, %eax
	movl	%eax, %edx
.L1119:
	movzwl	-680(%rbp), %eax
	testw	%ax, %ax
	js	.L1120
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1121:
	testb	$1, %al
	jne	.L1249
	cmpl	%edx, %ecx
	je	.L1122
.L1249:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1124:
	movl	$-1, %ecx
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	.LC35(%rip), %rax
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-808(%rbp), %eax
	testb	$1, %al
	je	.L1126
	movzbl	-680(%rbp), %r14d
	andl	$1, %r14d
.L1127:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testb	%r14b, %r14b
	je	.L1105
	movl	$20, %esi
	jmp	.L1125
.L1087:
	orl	$-32, %eax
	movl	%r10d, -868(%rbp)
	movw	%ax, -872(%rbp)
	jmp	.L1083
.L1134:
	testw	%ax, %ax
	js	.L1136
	sarl	$5, %eax
	movl	%eax, %edx
.L1137:
	movzwl	-680(%rbp), %eax
	testw	%ax, %ax
	js	.L1138
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1139:
	testb	$1, %al
	jne	.L1251
	cmpl	%edx, %ecx
	je	.L1140
.L1251:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1142:
	movl	$-1, %ecx
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	.LC41(%rip), %rax
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-872(%rbp), %eax
	testb	$1, %al
	je	.L1160
	movzbl	-680(%rbp), %eax
	andl	$1, %eax
.L1161:
	movq	%r13, %rdi
	movb	%al, -920(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-920(%rbp), %eax
	testb	%al, %al
	je	.L1168
	movl	$-1, %ecx
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rax
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-808(%rbp), %eax
	testb	$1, %al
	je	.L1169
	movzbl	-680(%rbp), %eax
	andl	$1, %eax
.L1170:
	movq	%r13, %rdi
	movb	%al, -920(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-920(%rbp), %eax
	testb	%al, %al
	jne	.L1244
	orl	$-1, %ecx
	movq	%r14, %rdx
	leaq	.LC42(%rip), %rax
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-912(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	movq	%r13, %rdi
	movb	%al, -920(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-920(%rbp), %eax
	movl	$24, %esi
	testb	%al, %al
	jne	.L1176
	movq	%r14, %rdx
	orl	$-1, %ecx
	leaq	.LC43(%rip), %rax
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-912(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$25, %esi
	testb	%r14b, %r14b
	je	.L1105
.L1176:
	movq	32(%r15), %rdi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	call	_ZN6icu_6717CollationSettings12setCaseFirstE18UColAttributeValueiR10UErrorCode@PLT
	movl	-900(%rbp), %eax
	movl	%eax, 72(%r15)
	jmp	.L1106
.L1111:
	movl	-676(%rbp), %ecx
	jmp	.L1112
.L1109:
	movl	-868(%rbp), %edx
	jmp	.L1110
.L1218:
	movl	-740(%rbp), %eax
	jmp	.L1219
.L1216:
	movl	-868(%rbp), %edx
	jmp	.L1217
.L1074:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	-912(%rbp)
	jmp	.L1069
.L1126:
	testw	%ax, %ax
	js	.L1128
	sarl	$5, %eax
	movl	%eax, %edx
.L1129:
	movzwl	-680(%rbp), %ecx
	testw	%cx, %cx
	js	.L1130
	movswl	%cx, %eax
	sarl	$5, %eax
.L1131:
	andl	$1, %ecx
	jne	.L1250
	cmpl	%edx, %eax
	je	.L1132
.L1250:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1105
.L1143:
	testw	%ax, %ax
	js	.L1145
	sarl	$5, %eax
	movl	%eax, %edx
.L1146:
	movzwl	-680(%rbp), %ecx
	testw	%cx, %cx
	js	.L1147
	movswl	%cx, %eax
	sarl	$5, %eax
.L1148:
	cmpl	%edx, %eax
	jne	.L1252
	andb	$1, %cl
	je	.L1149
.L1252:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1151:
	movl	$-1, %ecx
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	.LC38(%rip), %rax
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-808(%rbp), %eax
	testb	$1, %al
	je	.L1153
	movzbl	-680(%rbp), %eax
	andl	$1, %eax
.L1154:
	movq	%r13, %rdi
	movb	%al, -920(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-920(%rbp), %eax
	movl	$1, %esi
	testb	%al, %al
	jne	.L1152
	orl	$-1, %ecx
	movq	%r14, %rdx
	leaq	.LC39(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-912(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	movq	%r13, %rdi
	movb	%al, -920(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-920(%rbp), %eax
	movl	$2, %esi
	testb	%al, %al
	jne	.L1152
	movq	%r14, %rdx
	orl	$-1, %ecx
	leaq	.LC40(%rip), %rax
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-912(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testb	%r14b, %r14b
	je	.L1105
	movl	$3, %esi
	jmp	.L1152
.L1160:
	testw	%ax, %ax
	js	.L1162
	sarl	$5, %eax
	movl	%eax, %edx
.L1163:
	movzwl	-680(%rbp), %ecx
	testw	%cx, %cx
	js	.L1164
	movswl	%cx, %eax
	sarl	$5, %eax
.L1165:
	cmpl	%edx, %eax
	jne	.L1253
	andb	$1, %cl
	je	.L1166
.L1253:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1168:
	movl	$-1, %ecx
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	.LC44(%rip), %rax
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-872(%rbp), %eax
	testb	$1, %al
	je	.L1177
	movzbl	-680(%rbp), %eax
	andl	$1, %eax
.L1178:
	movq	%r13, %rdi
	movb	%al, -920(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-920(%rbp), %eax
	testb	%al, %al
	je	.L1184
	movq	-912(%rbp), %rdi
	call	_ZN6icu_6719CollationRuleParser13getOnOffValueERKNS_13UnicodeStringE
	movl	%eax, %edx
	cmpl	$-1, %eax
	je	.L1105
	movq	32(%r15), %rdi
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movl	$1024, %esi
	call	_ZN6icu_6717CollationSettings7setFlagEi18UColAttributeValueiR10UErrorCode@PLT
	movl	-900(%rbp), %eax
	movl	%eax, 72(%r15)
	jmp	.L1106
.L1369:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L1083
.L1209:
	movq	%r8, %rsi
	movq	%r12, %rdi
	movq	%r8, -920(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-920(%rbp), %r8
	testb	%al, %al
	setne	-912(%rbp)
	jmp	.L1204
.L1138:
	movl	-676(%rbp), %ecx
	jmp	.L1139
.L1120:
	movl	-676(%rbp), %ecx
	jmp	.L1121
.L1118:
	movl	-804(%rbp), %edx
	jmp	.L1119
.L1136:
	movl	-868(%rbp), %edx
	jmp	.L1137
.L1153:
	testw	%ax, %ax
	js	.L1155
	sarl	$5, %eax
	movl	%eax, %edx
.L1156:
	movzwl	-680(%rbp), %eax
	testw	%ax, %ax
	js	.L1157
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1158:
	movl	%eax, %esi
	cmpl	%edx, %ecx
	notl	%esi
	sete	%al
	andb	%sil, %al
	je	.L1154
	movq	-912(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L1154
.L1184:
	orl	$-1, %ecx
	movq	%r14, %rdx
	leaq	.LC45(%rip), %rax
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	movq	%r13, %rdi
	movb	%al, -920(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-920(%rbp), %eax
	testb	%al, %al
	je	.L1185
	movq	-912(%rbp), %rdi
	call	_ZN6icu_6719CollationRuleParser13getOnOffValueERKNS_13UnicodeStringE
	movl	%eax, %edx
	cmpl	$-1, %eax
	je	.L1105
	movq	32(%r15), %rdi
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movl	$1, %esi
	call	_ZN6icu_6717CollationSettings7setFlagEi18UColAttributeValueiR10UErrorCode@PLT
	movl	-900(%rbp), %eax
	movl	%eax, 72(%r15)
	jmp	.L1106
.L1177:
	testw	%ax, %ax
	js	.L1179
	sarl	$5, %eax
	movl	%eax, %edx
.L1180:
	movzwl	-680(%rbp), %eax
	testw	%ax, %ax
	js	.L1181
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1182:
	cmpl	%edx, %ecx
	notl	%eax
	sete	%cl
	andb	%cl, %al
	je	.L1178
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L1178
.L1130:
	movl	-676(%rbp), %eax
	jmp	.L1131
.L1128:
	movl	-804(%rbp), %edx
	jmp	.L1129
.L1169:
	testw	%ax, %ax
	js	.L1171
	sarl	$5, %eax
	movl	%eax, %edx
.L1172:
	movzwl	-680(%rbp), %eax
	testw	%ax, %ax
	js	.L1173
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1174:
	movl	%eax, %esi
	cmpl	%edx, %ecx
	notl	%esi
	sete	%al
	andb	%sil, %al
	je	.L1170
	movq	-912(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L1170
.L1147:
	movl	-676(%rbp), %eax
	jmp	.L1148
.L1145:
	movl	-804(%rbp), %edx
	jmp	.L1146
.L1164:
	movl	-676(%rbp), %eax
	jmp	.L1165
.L1162:
	movl	-868(%rbp), %edx
	jmp	.L1163
.L1113:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	-920(%rbp)
	jmp	.L1108
.L1220:
	movq	%r8, %rsi
	movq	%r12, %rdi
	movq	%r8, -912(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-912(%rbp), %r8
	testb	%al, %al
	setne	%r14b
	jmp	.L1215
.L1244:
	movl	$16, %esi
	jmp	.L1176
.L1363:
	call	__stack_chk_fail@PLT
.L1157:
	movl	-676(%rbp), %ecx
	jmp	.L1158
.L1155:
	movl	-804(%rbp), %edx
	jmp	.L1156
.L1122:
	movq	-912(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	-920(%rbp)
	jmp	.L1117
.L1185:
	orl	$-1, %ecx
	movq	%r14, %rdx
	leaq	.LC46(%rip), %rax
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	movq	%r13, %rdi
	movb	%al, -920(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-920(%rbp), %eax
	testb	%al, %al
	je	.L1186
	movq	-912(%rbp), %rdi
	call	_ZN6icu_6719CollationRuleParser13getOnOffValueERKNS_13UnicodeStringE
	movl	%eax, %edx
	cmpl	$-1, %eax
	je	.L1105
	movq	32(%r15), %rdi
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movl	$2, %esi
	call	_ZN6icu_6717CollationSettings7setFlagEi18UColAttributeValueiR10UErrorCode@PLT
	movl	-900(%rbp), %eax
	movl	%eax, 72(%r15)
	jmp	.L1106
.L1173:
	movl	-676(%rbp), %ecx
	jmp	.L1174
.L1140:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	-920(%rbp)
	jmp	.L1135
.L1181:
	movl	-676(%rbp), %ecx
	jmp	.L1182
.L1179:
	movl	-868(%rbp), %edx
	jmp	.L1180
.L1171:
	movl	-804(%rbp), %edx
	jmp	.L1172
.L1186:
	orl	$-1, %ecx
	movq	%r14, %rdx
	leaq	.LC47(%rip), %rax
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	movq	%r13, %rdi
	movb	%al, -920(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-920(%rbp), %eax
	testb	%al, %al
	je	.L1187
	movq	-912(%rbp), %rdi
	call	_ZN6icu_6719CollationRuleParser13getOnOffValueERKNS_13UnicodeStringE
	cmpl	$-1, %eax
	je	.L1105
	cmpl	$17, %eax
	je	.L1370
.L1188:
	movl	-900(%rbp), %eax
	movl	%eax, 72(%r15)
	jmp	.L1106
.L1132:
	movq	-912(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r14b
	jmp	.L1127
.L1149:
	movq	-912(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L1144
.L1166:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L1161
.L1187:
	orl	$-1, %ecx
	movq	%r14, %rdx
	leaq	.LC49(%rip), %rax
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeStringeqERKS0_
	movq	%r13, %rdi
	movb	%al, -920(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-920(%rbp), %eax
	testb	%al, %al
	je	.L1105
	movq	-912(%rbp), %rsi
	leaq	-739(%rbp), %rax
	leaq	-752(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%rax, -752(%rbp)
	movl	$0, -696(%rbp)
	movl	$40, -744(%rbp)
	movw	$0, -740(%rbp)
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	cmpl	$7, (%rbx)
	je	.L1189
	leaq	-384(%rbp), %r9
	movq	-752(%rbp), %rdi
	movq	%rbx, %r8
	movq	%r14, %rcx
	movl	$157, %edx
	movq	%r9, %rsi
	movq	%r9, -920(%rbp)
	call	uloc_forLanguageTag_67@PLT
	cmpl	$0, (%rbx)
	jg	.L1190
	movl	-696(%rbp), %ecx
	cmpl	%ecx, -888(%rbp)
	jne	.L1190
	cmpl	$156, %eax
	movq	-920(%rbp), %r9
	jle	.L1371
.L1190:
	movl	$0, (%rbx)
	movq	%rbx, %rdx
	leaq	.LC50(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6719CollationRuleParser13setParseErrorEPKcR10UErrorCode.part.0
.L1189:
	cmpb	$0, -740(%rbp)
	je	.L1106
	movq	-752(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L1106
.L1370:
	cmpl	$0, (%rbx)
	jg	.L1188
	movq	%rbx, %rdx
	leaq	.LC48(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6719CollationRuleParser13setParseErrorEPKcR10UErrorCode.part.0
	jmp	.L1188
.L1371:
	leaq	-224(%rbp), %r14
	movq	%rbx, %rcx
	movl	$157, %edx
	movq	%r9, %rdi
	movq	%r14, %rsi
	call	uloc_getBaseName_67@PLT
	cmpl	$0, (%rbx)
	jg	.L1190
	cmpl	$95, %eax
	movq	-920(%rbp), %r9
	jg	.L1190
	testl	%eax, %eax
	je	.L1372
	cmpb	$95, -224(%rbp)
	je	.L1373
.L1194:
	leaq	-480(%rbp), %rdx
	movl	$96, %ecx
	movq	%rbx, %r8
	movq	%r9, %rdi
	leaq	.LC51(%rip), %rsi
	movq	%rdx, -920(%rbp)
	call	uloc_getKeywordValue_67@PLT
	cmpl	$0, (%rbx)
	movl	%eax, %ecx
	jg	.L1190
	cmpl	$95, %eax
	jg	.L1190
	movq	64(%r15), %rdi
	movq	-920(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1374
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	testl	%ecx, %ecx
	movq	%rbx, %r9
	movq	%r14, %rsi
	movq	%rax, -688(%rbp)
	movq	(%rdi), %rax
	leaq	.LC28(%rip), %rcx
	leaq	48(%r15), %r8
	movw	$2, -680(%rbp)
	cmovle	%rcx, %rdx
	movq	%r13, %rcx
	call	*24(%rax)
	cmpl	$0, (%rbx)
	jle	.L1197
	cmpq	$0, 48(%r15)
	je	.L1375
.L1198:
	movq	%r15, %rdi
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1189
.L1372:
	movl	$1953460082, -224(%rbp)
	movb	$0, -220(%rbp)
	jmp	.L1194
.L1375:
	leaq	.LC53(%rip), %rax
	movq	%rax, 48(%r15)
	jmp	.L1198
.L1197:
	movq	16(%r15), %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	72(%r15), %r14d
	movq	%rcx, -920(%rbp)
	call	_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringER10UErrorCode
	cmpl	$0, (%rbx)
	movq	-920(%rbp), %rcx
	jle	.L1199
	movq	40(%r15), %rax
	testq	%rax, %rax
	je	.L1199
	movl	%r14d, 4(%rax)
.L1199:
	movl	-900(%rbp), %eax
	movq	%rcx, 16(%r15)
	movq	%r13, %rdi
	movl	%eax, 72(%r15)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1189
.L1374:
	movq	%rbx, %rdx
	leaq	.LC52(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6719CollationRuleParser13setParseErrorEPKcR10UErrorCode.part.0
	jmp	.L1189
.L1373:
	addl	$1, %eax
	leaq	-221(%rbp), %rdi
	movl	$154, %ecx
	movq	%r14, %rsi
	movslq	%eax, %rdx
	movq	%r9, -920(%rbp)
	call	__memmove_chk@PLT
	movb	$100, -222(%rbp)
	movq	-920(%rbp), %r9
	movw	$28277, -224(%rbp)
	jmp	.L1194
	.cfi_endproc
.LFE3347:
	.size	_ZN6icu_6719CollationRuleParser12parseSettingER10UErrorCode, .-_ZN6icu_6719CollationRuleParser12parseSettingER10UErrorCode
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"expected a reset or setting or comment"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringER10UErrorCode.part.0, @function
_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringER10UErrorCode.part.0:
.LFB4447:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	.LC57(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	movzwl	8(%rsi), %edx
	movq	%rsi, 16(%rdi)
	movl	$0, 72(%rdi)
	testw	%dx, %dx
	js	.L1378
	.p2align 4,,10
	.p2align 3
.L1438:
	movswl	%dx, %ecx
	sarl	$5, %ecx
	cmpl	%eax, %ecx
	jle	.L1376
.L1439:
	jbe	.L1435
	andl	$2, %edx
	je	.L1384
	addq	$10, %rsi
.L1385:
	cltq
	movzwl	(%rsi,%rax,2), %edi
	movl	%edi, %r14d
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	jne	.L1386
	cmpw	$38, %r14w
	je	.L1387
	ja	.L1388
	cmpw	$33, %r14w
	je	.L1389
	cmpw	$35, %r14w
	jne	.L1382
	movq	16(%r13), %rdi
	movl	72(%r13), %eax
	movswl	8(%rdi), %esi
	addl	$1, %eax
	movl	%esi, %edx
	sarl	$5, %esi
	movl	%edx, %ecx
	andl	$2, %ecx
	testw	%dx, %dx
	js	.L1436
	testw	%cx, %cx
	jne	.L1403
	movslq	%eax, %rdx
	addq	%rdx, %rdx
	.p2align 4,,10
	.p2align 3
.L1408:
	cmpl	%eax, %esi
	jle	.L1415
	leal	1(%rax), %ecx
	cmpl	%esi, %eax
	jnb	.L1437
	movq	24(%rdi), %rax
	movzwl	(%rax,%rdx), %eax
	leal	-12(%rax), %r8d
	cmpw	$1, %r8w
	jbe	.L1396
	cmpw	$10, %ax
	je	.L1396
	cmpw	$133, %ax
	je	.L1396
	subw	$8232, %ax
	addq	$2, %rdx
	cmpw	$1, %ax
	jbe	.L1396
	movl	%ecx, %eax
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1388:
	cmpw	$64, %r14w
	je	.L1391
	cmpw	$91, %r14w
	jne	.L1382
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6719CollationRuleParser12parseSettingER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L1393:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1376
	movl	72(%r13), %eax
.L1383:
	movq	16(%r13), %rsi
	movzwl	8(%rsi), %edx
	testw	%dx, %dx
	jns	.L1438
.L1378:
	movl	12(%rsi), %ecx
	cmpl	%eax, %ecx
	jg	.L1439
.L1376:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1384:
	.cfi_restore_state
	movq	24(%rsi), %rsi
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1386:
	movl	72(%r13), %eax
	addl	$1, %eax
	movl	%eax, 72(%r13)
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1435:
	movl	$65535, %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	jne	.L1386
.L1382:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L1376
	cmpq	$0, 40(%r13)
	movl	$3, (%rbx)
	movq	%r12, 48(%r13)
	je	.L1376
	movq	%r13, %rdi
	call	_ZN6icu_6719CollationRuleParser15setErrorContextEv
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1387:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6719CollationRuleParser14parseRuleChainER10UErrorCode
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1391:
	movq	32(%r13), %rdi
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movl	$17, %edx
	movl	$2048, %esi
	call	_ZN6icu_6717CollationSettings7setFlagEi18UColAttributeValueiR10UErrorCode@PLT
	addl	$1, 72(%r13)
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1389:
	addl	$1, 72(%r13)
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1403:
	cltq
	.p2align 4,,10
	.p2align 3
.L1411:
	movl	%eax, %ecx
	cmpl	%eax, %esi
	jle	.L1396
	leal	1(%rax), %ecx
	jbe	.L1440
	movzwl	10(%rdi,%rax,2), %edx
	leal	-12(%rdx), %r8d
	cmpw	$1, %r8w
	jbe	.L1396
	cmpw	$10, %dx
	je	.L1396
	cmpw	$133, %dx
	je	.L1396
	subw	$8232, %dx
	addq	$1, %rax
	cmpw	$1, %dx
	ja	.L1411
	.p2align 4,,10
	.p2align 3
.L1396:
	movl	%ecx, 72(%r13)
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1440:
	addq	$1, %rax
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1437:
	addq	$2, %rdx
	movl	%ecx, %eax
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1436:
	movl	12(%rdi), %esi
	testw	%cx, %cx
	jne	.L1395
	movslq	%eax, %rdx
	addq	%rdx, %rdx
	.p2align 4,,10
	.p2align 3
.L1399:
	cmpl	%esi, %eax
	jge	.L1415
	leal	1(%rax), %ecx
	cmpl	%eax, %esi
	jbe	.L1441
	movq	24(%rdi), %rax
	movzwl	(%rax,%rdx), %eax
	leal	-12(%rax), %r8d
	cmpw	$1, %r8w
	jbe	.L1396
	cmpw	$10, %ax
	je	.L1396
	cmpw	$133, %ax
	je	.L1396
	subw	$8232, %ax
	addq	$2, %rdx
	cmpw	$1, %ax
	jbe	.L1396
	movl	%ecx, %eax
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1441:
	addq	$2, %rdx
	movl	%ecx, %eax
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1395:
	cltq
	.p2align 4,,10
	.p2align 3
.L1402:
	movl	%eax, %ecx
	cmpl	%eax, %esi
	jle	.L1396
	leal	1(%rax), %ecx
	jbe	.L1442
	movzwl	10(%rdi,%rax,2), %edx
	leal	-12(%rdx), %r8d
	cmpw	$1, %r8w
	jbe	.L1396
	cmpw	$10, %dx
	je	.L1396
	cmpw	$133, %dx
	je	.L1396
	subw	$8232, %dx
	addq	$1, %rax
	cmpw	$1, %dx
	ja	.L1402
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1442:
	addq	$1, %rax
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1415:
	movl	%eax, %ecx
	movl	%ecx, 72(%r13)
	jmp	.L1393
	.cfi_endproc
.LFE4447:
	.size	_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringER10UErrorCode.part.0, .-_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringER10UErrorCode:
.LFB3338:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L1443
	jmp	_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1443:
	ret
	.cfi_endproc
.LFE3338:
	.size	_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringERNS_17CollationSettingsEP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringERNS_17CollationSettingsEP11UParseErrorR10UErrorCode, @function
_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringERNS_17CollationSettingsEP11UParseErrorR10UErrorCode:
.LFB3337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdx, -8(%rbp)
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L1445
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 32(%rdi)
	testq	%rcx, %rcx
	je	.L1447
	movabsq	$-4294967296, %rax
	xorl	%edx, %edx
	movq	%rax, (%rcx)
	xorl	%eax, %eax
	movw	%ax, 8(%rcx)
	movw	%dx, 40(%rcx)
.L1447:
	movq	$0, 48(%rdi)
	movq	%r8, %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1445:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3337:
	.size	_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringERNS_17CollationSettingsEP11UParseErrorR10UErrorCode, .-_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringERNS_17CollationSettingsEP11UParseErrorR10UErrorCode
	.weak	_ZTSN6icu_6719CollationRuleParser4SinkE
	.section	.rodata._ZTSN6icu_6719CollationRuleParser4SinkE,"aG",@progbits,_ZTSN6icu_6719CollationRuleParser4SinkE,comdat
	.align 32
	.type	_ZTSN6icu_6719CollationRuleParser4SinkE, @object
	.size	_ZTSN6icu_6719CollationRuleParser4SinkE, 36
_ZTSN6icu_6719CollationRuleParser4SinkE:
	.string	"N6icu_6719CollationRuleParser4SinkE"
	.weak	_ZTIN6icu_6719CollationRuleParser4SinkE
	.section	.data.rel.ro._ZTIN6icu_6719CollationRuleParser4SinkE,"awG",@progbits,_ZTIN6icu_6719CollationRuleParser4SinkE,comdat
	.align 8
	.type	_ZTIN6icu_6719CollationRuleParser4SinkE, @object
	.size	_ZTIN6icu_6719CollationRuleParser4SinkE, 24
_ZTIN6icu_6719CollationRuleParser4SinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CollationRuleParser4SinkE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6719CollationRuleParser8ImporterE
	.section	.rodata._ZTSN6icu_6719CollationRuleParser8ImporterE,"aG",@progbits,_ZTSN6icu_6719CollationRuleParser8ImporterE,comdat
	.align 32
	.type	_ZTSN6icu_6719CollationRuleParser8ImporterE, @object
	.size	_ZTSN6icu_6719CollationRuleParser8ImporterE, 40
_ZTSN6icu_6719CollationRuleParser8ImporterE:
	.string	"N6icu_6719CollationRuleParser8ImporterE"
	.weak	_ZTIN6icu_6719CollationRuleParser8ImporterE
	.section	.data.rel.ro._ZTIN6icu_6719CollationRuleParser8ImporterE,"awG",@progbits,_ZTIN6icu_6719CollationRuleParser8ImporterE,comdat
	.align 8
	.type	_ZTIN6icu_6719CollationRuleParser8ImporterE, @object
	.size	_ZTIN6icu_6719CollationRuleParser8ImporterE, 24
_ZTIN6icu_6719CollationRuleParser8ImporterE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CollationRuleParser8ImporterE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6719CollationRuleParser4SinkE
	.section	.data.rel.ro._ZTVN6icu_6719CollationRuleParser4SinkE,"awG",@progbits,_ZTVN6icu_6719CollationRuleParser4SinkE,comdat
	.align 8
	.type	_ZTVN6icu_6719CollationRuleParser4SinkE, @object
	.size	_ZTVN6icu_6719CollationRuleParser4SinkE, 72
_ZTVN6icu_6719CollationRuleParser4SinkE:
	.quad	0
	.quad	_ZTIN6icu_6719CollationRuleParser4SinkE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_6719CollationRuleParser4Sink20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode
	.quad	_ZN6icu_6719CollationRuleParser4Sink8optimizeERKNS_10UnicodeSetERPKcR10UErrorCode
	.weak	_ZTVN6icu_6719CollationRuleParser8ImporterE
	.section	.data.rel.ro._ZTVN6icu_6719CollationRuleParser8ImporterE,"awG",@progbits,_ZTVN6icu_6719CollationRuleParser8ImporterE,comdat
	.align 8
	.type	_ZTVN6icu_6719CollationRuleParser8ImporterE, @object
	.size	_ZTVN6icu_6719CollationRuleParser8ImporterE, 48
_ZTVN6icu_6719CollationRuleParser8ImporterE:
	.quad	0
	.quad	_ZTIN6icu_6719CollationRuleParser8ImporterE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.section	.rodata
	.align 2
	.type	_ZZNK6icu_6719CollationRuleParser9readWordsEiRNS_13UnicodeStringEE2sp, @object
	.size	_ZZNK6icu_6719CollationRuleParser9readWordsEiRNS_13UnicodeStringEE2sp, 2
_ZZNK6icu_6719CollationRuleParser9readWordsEiRNS_13UnicodeStringEE2sp:
	.value	32
	.section	.rodata.str1.1
.LC58:
	.string	"space"
.LC59:
	.string	"punct"
.LC60:
	.string	"symbol"
.LC61:
	.string	"currency"
.LC62:
	.string	"digit"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZN6icu_67L20gSpecialReorderCodesE, @object
	.size	_ZN6icu_67L20gSpecialReorderCodesE, 40
_ZN6icu_67L20gSpecialReorderCodesE:
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.quad	.LC62
	.section	.rodata.str1.1
.LC63:
	.string	"first tertiary ignorable"
.LC64:
	.string	"last tertiary ignorable"
.LC65:
	.string	"first secondary ignorable"
.LC66:
	.string	"last secondary ignorable"
.LC67:
	.string	"first primary ignorable"
.LC68:
	.string	"last primary ignorable"
.LC69:
	.string	"first variable"
.LC70:
	.string	"last variable"
.LC71:
	.string	"first regular"
.LC72:
	.string	"last regular"
.LC73:
	.string	"first implicit"
.LC74:
	.string	"last implicit"
.LC75:
	.string	"first trailing"
.LC76:
	.string	"last trailing"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZN6icu_6712_GLOBAL__N_1L9positionsE, @object
	.size	_ZN6icu_6712_GLOBAL__N_1L9positionsE, 112
_ZN6icu_6712_GLOBAL__N_1L9positionsE:
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.section	.rodata
	.align 16
	.type	_ZN6icu_6712_GLOBAL__N_1L6BEFOREE, @object
	.size	_ZN6icu_6712_GLOBAL__N_1L6BEFOREE, 16
_ZN6icu_6712_GLOBAL__N_1L6BEFOREE:
	.value	91
	.value	98
	.value	101
	.value	102
	.value	111
	.value	114
	.value	101
	.value	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
