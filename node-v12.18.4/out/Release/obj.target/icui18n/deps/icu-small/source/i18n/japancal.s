	.file	"japancal.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716JapaneseCalendar17getDynamicClassIDEv
	.type	_ZNK6icu_6716JapaneseCalendar17getDynamicClassIDEv, @function
_ZNK6icu_6716JapaneseCalendar17getDynamicClassIDEv:
.LFB3058:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6716JapaneseCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3058:
	.size	_ZNK6icu_6716JapaneseCalendar17getDynamicClassIDEv, .-_ZNK6icu_6716JapaneseCalendar17getDynamicClassIDEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"japanese"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716JapaneseCalendar7getTypeEv
	.type	_ZNK6icu_6716JapaneseCalendar7getTypeEv, @function
_ZNK6icu_6716JapaneseCalendar7getTypeEv:
.LFB3075:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE3075:
	.size	_ZNK6icu_6716JapaneseCalendar7getTypeEv, .-_ZNK6icu_6716JapaneseCalendar7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716JapaneseCalendar14internalGetEraEv
	.type	_ZNK6icu_6716JapaneseCalendar14internalGetEraEv, @function
_ZNK6icu_6716JapaneseCalendar14internalGetEraEv:
.LFB3078:
	.cfi_startproc
	endbr64
	movl	128(%rdi), %edx
	movl	_ZL11gCurrentEra(%rip), %eax
	testl	%edx, %edx
	jle	.L4
	movl	12(%rdi), %eax
.L4:
	ret
	.cfi_endproc
.LFE3078:
	.size	_ZNK6icu_6716JapaneseCalendar14internalGetEraEv, .-_ZNK6icu_6716JapaneseCalendar14internalGetEraEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716JapaneseCalendar18haveDefaultCenturyEv
	.type	_ZNK6icu_6716JapaneseCalendar18haveDefaultCenturyEv, @function
_ZNK6icu_6716JapaneseCalendar18haveDefaultCenturyEv:
.LFB3081:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3081:
	.size	_ZNK6icu_6716JapaneseCalendar18haveDefaultCenturyEv, .-_ZNK6icu_6716JapaneseCalendar18haveDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716JapaneseCalendar19defaultCenturyStartEv
	.type	_ZNK6icu_6716JapaneseCalendar19defaultCenturyStartEv, @function
_ZNK6icu_6716JapaneseCalendar19defaultCenturyStartEv:
.LFB3082:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE3082:
	.size	_ZNK6icu_6716JapaneseCalendar19defaultCenturyStartEv, .-_ZNK6icu_6716JapaneseCalendar19defaultCenturyStartEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716JapaneseCalendar23defaultCenturyStartYearEv
	.type	_ZNK6icu_6716JapaneseCalendar23defaultCenturyStartYearEv, @function
_ZNK6icu_6716JapaneseCalendar23defaultCenturyStartYearEv:
.LFB3083:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3083:
	.size	_ZNK6icu_6716JapaneseCalendar23defaultCenturyStartYearEv, .-_ZNK6icu_6716JapaneseCalendar23defaultCenturyStartYearEv
	.p2align 4
	.type	japanese_calendar_cleanup, @function
japanese_calendar_cleanup:
.LFB3056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	_ZL17gJapaneseEraRules(%rip), %r12
	testq	%r12, %r12
	je	.L10
	movq	%r12, %rdi
	call	_ZN6icu_678EraRulesD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	$0, _ZL17gJapaneseEraRules(%rip)
.L10:
	movl	$0, _ZL11gCurrentEra(%rip)
	movl	$1, %eax
	movl	$0, _ZL25gJapaneseEraRulesInitOnce(%rip)
	mfence
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3056:
	.size	japanese_calendar_cleanup, .-japanese_calendar_cleanup
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716JapaneseCalendarD2Ev
	.type	_ZN6icu_6716JapaneseCalendarD2Ev, @function
_ZN6icu_6716JapaneseCalendarD2Ev:
.LFB3067:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6716JapaneseCalendarE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717GregorianCalendarD2Ev@PLT
	.cfi_endproc
.LFE3067:
	.size	_ZN6icu_6716JapaneseCalendarD2Ev, .-_ZN6icu_6716JapaneseCalendarD2Ev
	.globl	_ZN6icu_6716JapaneseCalendarD1Ev
	.set	_ZN6icu_6716JapaneseCalendarD1Ev,_ZN6icu_6716JapaneseCalendarD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716JapaneseCalendarD0Ev
	.type	_ZN6icu_6716JapaneseCalendarD0Ev, @function
_ZN6icu_6716JapaneseCalendarD0Ev:
.LFB3069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6716JapaneseCalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717GregorianCalendarD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3069:
	.size	_ZN6icu_6716JapaneseCalendarD0Ev, .-_ZN6icu_6716JapaneseCalendarD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716JapaneseCalendar21handleGetExtendedYearEv
	.type	_ZN6icu_6716JapaneseCalendar21handleGetExtendedYearEv, @function
_ZN6icu_6716JapaneseCalendar21handleGetExtendedYearEv:
.LFB3079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movl	$19, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_@PLT
	cmpl	$19, %eax
	je	.L27
.L19:
	movl	128(%rbx), %r8d
	movl	$0, -28(%rbp)
	movq	_ZL17gJapaneseEraRules(%rip), %rdi
	movl	_ZL11gCurrentEra(%rip), %esi
	testl	%r8d, %r8d
	jle	.L21
	movl	12(%rbx), %esi
.L21:
	leaq	-28(%rbp), %rdx
	call	_ZNK6icu_678EraRules12getStartYearEiR10UErrorCode@PLT
	movl	132(%rbx), %ecx
	movl	$1, %edx
	testl	%ecx, %ecx
	jle	.L22
	movl	16(%rbx), %edx
.L22:
	leal	-1(%rax,%rdx), %eax
.L18:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L28
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$19, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_@PLT
	cmpl	$19, %eax
	jne	.L19
	movl	204(%rbx), %r9d
	movl	$1970, %eax
	testl	%r9d, %r9d
	jle	.L18
	movl	88(%rbx), %eax
	jmp	.L18
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3079:
	.size	_ZN6icu_6716JapaneseCalendar21handleGetExtendedYearEv, .-_ZN6icu_6716JapaneseCalendar21handleGetExtendedYearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716JapaneseCalendar21getDefaultMonthInYearEi
	.type	_ZN6icu_6716JapaneseCalendar21getDefaultMonthInYearEi, @function
_ZN6icu_6716JapaneseCalendar21getDefaultMonthInYearEi:
.LFB3076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_6716JapaneseCalendar14internalGetEraEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	400(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L30
	movl	128(%rdi), %eax
	movl	_ZL11gCurrentEra(%rip), %esi
	testl	%eax, %eax
	jle	.L32
	movl	12(%rdi), %esi
.L32:
	movq	_ZL17gJapaneseEraRules(%rip), %rdi
	leaq	-40(%rbp), %rcx
	leaq	-36(%rbp), %rdx
	movq	$0, -36(%rbp)
	movl	$0, -28(%rbp)
	movl	$0, -40(%rbp)
	call	_ZNK6icu_678EraRules12getStartDateEiRA3_iR10UErrorCode@PLT
	xorl	%eax, %eax
	cmpl	%ebx, -36(%rbp)
	je	.L37
.L29:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L38
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movl	-32(%rbp), %eax
	subl	$1, %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	call	*%rax
	movl	%eax, %esi
	jmp	.L32
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3076:
	.size	_ZN6icu_6716JapaneseCalendar21getDefaultMonthInYearEi, .-_ZN6icu_6716JapaneseCalendar21getDefaultMonthInYearEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716JapaneseCalendar20getDefaultDayInMonthEii
	.type	_ZN6icu_6716JapaneseCalendar20getDefaultDayInMonthEii, @function
_ZN6icu_6716JapaneseCalendar20getDefaultDayInMonthEii:
.LFB3077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	leaq	_ZNK6icu_6716JapaneseCalendar14internalGetEraEv(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	400(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L40
	movl	128(%rdi), %eax
	movl	_ZL11gCurrentEra(%rip), %esi
	testl	%eax, %eax
	jle	.L42
	movl	12(%rdi), %esi
.L42:
	movq	_ZL17gJapaneseEraRules(%rip), %rdi
	leaq	-40(%rbp), %rcx
	leaq	-36(%rbp), %rdx
	movq	$0, -36(%rbp)
	movl	$0, -28(%rbp)
	movl	$0, -40(%rbp)
	call	_ZNK6icu_678EraRules12getStartDateEiRA3_iR10UErrorCode@PLT
	movl	$1, %eax
	cmpl	%ebx, -36(%rbp)
	jne	.L39
	movl	-32(%rbp), %ecx
	leal	-1(%rcx), %edx
	cmpl	%r12d, %edx
	cmove	-28(%rbp), %eax
.L39:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L48
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	call	*%rax
	movl	%eax, %esi
	jmp	.L42
.L48:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3077:
	.size	_ZN6icu_6716JapaneseCalendar20getDefaultDayInMonthEii, .-_ZN6icu_6716JapaneseCalendar20getDefaultDayInMonthEii
	.section	.rodata.str1.1
.LC2:
	.string	"ICU_ENABLE_TENTATIVE_ERA"
.LC3:
	.string	"true"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716JapaneseCalendar5cloneEv
	.type	_ZNK6icu_6716JapaneseCalendar5cloneEv, @function
_ZNK6icu_6716JapaneseCalendar5cloneEv:
.LFB3074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$656, %edi
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L49
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6717GregorianCalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6716JapaneseCalendarE(%rip), %rax
	movq	%rax, (%r12)
	movl	$0, -28(%rbp)
	movl	_ZL25gJapaneseEraRulesInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L68
.L51:
	movl	4+_ZL25gJapaneseEraRulesInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L54
	movl	%eax, -28(%rbp)
.L54:
	leaq	japanese_calendar_cleanup(%rip), %rsi
	movl	$7, %edi
	call	ucln_i18n_registerCleanup_67@PLT
.L49:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	leaq	_ZL25gJapaneseEraRulesInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L51
	leaq	.LC2(%rip), %rdi
	call	getenv@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L52
	leaq	.LC3(%rip), %rsi
	call	uprv_stricmp_67@PLT
	xorl	%esi, %esi
	testl	%eax, %eax
	sete	%sil
.L52:
	leaq	-28(%rbp), %rdx
	leaq	.LC0(%rip), %rdi
	call	_ZN6icu_678EraRules14createInstanceEPKcaR10UErrorCode@PLT
	movl	-28(%rbp), %edx
	movq	%rax, _ZL17gJapaneseEraRules(%rip)
	testl	%edx, %edx
	jg	.L53
	movl	12(%rax), %eax
	movl	%eax, _ZL11gCurrentEra(%rip)
.L53:
	leaq	_ZL25gJapaneseEraRulesInitOnce(%rip), %rdi
	movl	%edx, 4+_ZL25gJapaneseEraRulesInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L54
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3074:
	.size	_ZNK6icu_6716JapaneseCalendar5cloneEv, .-_ZNK6icu_6716JapaneseCalendar5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716JapaneseCalendar16getStaticClassIDEv
	.type	_ZN6icu_6716JapaneseCalendar16getStaticClassIDEv, @function
_ZN6icu_6716JapaneseCalendar16getStaticClassIDEv:
.LFB3057:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6716JapaneseCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3057:
	.size	_ZN6icu_6716JapaneseCalendar16getStaticClassIDEv, .-_ZN6icu_6716JapaneseCalendar16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716JapaneseCalendar18enableTentativeEraEv
	.type	_ZN6icu_6716JapaneseCalendar18enableTentativeEraEv, @function
_ZN6icu_6716JapaneseCalendar18enableTentativeEraEv:
.LFB3059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	getenv@PLT
	movq	%rax, %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L71
	leaq	.LC3(%rip), %rsi
	call	uprv_stricmp_67@PLT
	testl	%eax, %eax
	sete	%al
.L71:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3059:
	.size	_ZN6icu_6716JapaneseCalendar18enableTentativeEraEv, .-_ZN6icu_6716JapaneseCalendar18enableTentativeEraEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716JapaneseCalendar13getCurrentEraEv
	.type	_ZN6icu_6716JapaneseCalendar13getCurrentEraEv, @function
_ZN6icu_6716JapaneseCalendar13getCurrentEraEv:
.LFB3062:
	.cfi_startproc
	endbr64
	movl	_ZL11gCurrentEra(%rip), %eax
	ret
	.cfi_endproc
.LFE3062:
	.size	_ZN6icu_6716JapaneseCalendar13getCurrentEraEv, .-_ZN6icu_6716JapaneseCalendar13getCurrentEraEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716JapaneseCalendarC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6716JapaneseCalendarC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6716JapaneseCalendarC2ERKNS_6LocaleER10UErrorCode:
.LFB3064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	call	_ZN6icu_6717GregorianCalendarC2ERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6716JapaneseCalendarE(%rip), %rax
	movq	%rax, 0(%r13)
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L94
.L80:
	leaq	japanese_calendar_cleanup(%rip), %rsi
	movl	$7, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movl	_ZL25gJapaneseEraRulesInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L95
.L81:
	movl	4+_ZL25gJapaneseEraRulesInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L80
	movl	%eax, (%r12)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	_ZL25gJapaneseEraRulesInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L81
	leaq	.LC2(%rip), %rdi
	call	getenv@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L82
	leaq	.LC3(%rip), %rsi
	call	uprv_stricmp_67@PLT
	xorl	%esi, %esi
	testl	%eax, %eax
	sete	%sil
.L82:
	movq	%r12, %rdx
	leaq	.LC0(%rip), %rdi
	call	_ZN6icu_678EraRules14createInstanceEPKcaR10UErrorCode@PLT
	movl	(%r12), %edx
	movq	%rax, _ZL17gJapaneseEraRules(%rip)
	testl	%edx, %edx
	jg	.L83
	movl	12(%rax), %eax
	movl	%eax, _ZL11gCurrentEra(%rip)
.L83:
	leaq	_ZL25gJapaneseEraRulesInitOnce(%rip), %rdi
	movl	%edx, 4+_ZL25gJapaneseEraRulesInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L80
	.cfi_endproc
.LFE3064:
	.size	_ZN6icu_6716JapaneseCalendarC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6716JapaneseCalendarC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6716JapaneseCalendarC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6716JapaneseCalendarC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6716JapaneseCalendarC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716JapaneseCalendarC2ERKS0_
	.type	_ZN6icu_6716JapaneseCalendarC2ERKS0_, @function
_ZN6icu_6716JapaneseCalendarC2ERKS0_:
.LFB3071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6717GregorianCalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6716JapaneseCalendarE(%rip), %rax
	movl	$0, -28(%rbp)
	movq	%rax, (%rbx)
	movl	_ZL25gJapaneseEraRulesInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L111
.L97:
	movl	4+_ZL25gJapaneseEraRulesInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L100
	movl	%eax, -28(%rbp)
.L100:
	leaq	japanese_calendar_cleanup(%rip), %rsi
	movl	$7, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L112
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	leaq	_ZL25gJapaneseEraRulesInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L97
	leaq	.LC2(%rip), %rdi
	call	getenv@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L98
	leaq	.LC3(%rip), %rsi
	call	uprv_stricmp_67@PLT
	xorl	%esi, %esi
	testl	%eax, %eax
	sete	%sil
.L98:
	leaq	-28(%rbp), %rdx
	leaq	.LC0(%rip), %rdi
	call	_ZN6icu_678EraRules14createInstanceEPKcaR10UErrorCode@PLT
	movl	-28(%rbp), %edx
	movq	%rax, _ZL17gJapaneseEraRules(%rip)
	testl	%edx, %edx
	jg	.L99
	movl	12(%rax), %eax
	movl	%eax, _ZL11gCurrentEra(%rip)
.L99:
	leaq	_ZL25gJapaneseEraRulesInitOnce(%rip), %rdi
	movl	%edx, 4+_ZL25gJapaneseEraRulesInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L100
.L112:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3071:
	.size	_ZN6icu_6716JapaneseCalendarC2ERKS0_, .-_ZN6icu_6716JapaneseCalendarC2ERKS0_
	.globl	_ZN6icu_6716JapaneseCalendarC1ERKS0_
	.set	_ZN6icu_6716JapaneseCalendarC1ERKS0_,_ZN6icu_6716JapaneseCalendarC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716JapaneseCalendaraSERKS0_
	.type	_ZN6icu_6716JapaneseCalendaraSERKS0_, @function
_ZN6icu_6716JapaneseCalendaraSERKS0_:
.LFB3073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6717GregorianCalendaraSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3073:
	.size	_ZN6icu_6716JapaneseCalendaraSERKS0_, .-_ZN6icu_6716JapaneseCalendaraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716JapaneseCalendar19handleComputeFieldsEiR10UErrorCode
	.type	_ZN6icu_6716JapaneseCalendar19handleComputeFieldsEiR10UErrorCode, @function
_ZN6icu_6716JapaneseCalendar19handleComputeFieldsEiR10UErrorCode:
.LFB3080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6717GregorianCalendar19handleComputeFieldsEiR10UErrorCode@PLT
	movl	88(%rbx), %r12d
	movl	20(%rbx), %eax
	movq	%r13, %r8
	movl	32(%rbx), %ecx
	movq	_ZL17gJapaneseEraRules(%rip), %rdi
	movl	%r12d, %esi
	leal	1(%rax), %edx
	call	_ZNK6icu_678EraRules11getEraIndexEiiiR10UErrorCode@PLT
	movb	$1, 104(%rbx)
	movq	_ZL17gJapaneseEraRules(%rip), %rdi
	movq	%r13, %rdx
	movl	%eax, 12(%rbx)
	movl	%eax, %esi
	movl	$1, 128(%rbx)
	call	_ZNK6icu_678EraRules12getStartYearEiR10UErrorCode@PLT
	movb	$1, 105(%rbx)
	movl	$1, 132(%rbx)
	subl	%eax, %r12d
	addl	$1, %r12d
	movl	%r12d, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3080:
	.size	_ZN6icu_6716JapaneseCalendar19handleComputeFieldsEiR10UErrorCode, .-_ZN6icu_6716JapaneseCalendar19handleComputeFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716JapaneseCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.type	_ZNK6icu_6716JapaneseCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, @function
_ZNK6icu_6716JapaneseCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE:
.LFB3084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L118
	movq	%rdi, %r12
	cmpl	$1, %esi
	je	.L119
	call	_ZNK6icu_6717GregorianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE@PLT
.L117:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L127
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	xorl	%eax, %eax
	cmpl	$1, %edx
	jbe	.L117
	movq	_ZL17gJapaneseEraRules(%rip), %rax
	movl	8(%rax), %eax
	subl	$1, %eax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L119:
	subl	$3, %edx
	movl	$1, %eax
	cmpl	$1, %edx
	ja	.L117
	movl	_ZL11gCurrentEra(%rip), %esi
	movq	_ZL17gJapaneseEraRules(%rip), %rdi
	leaq	-28(%rbp), %rdx
	movl	$0, -28(%rbp)
	call	_ZNK6icu_678EraRules12getStartYearEiR10UErrorCode@PLT
	movl	$3, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_6717GregorianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE@PLT
	subl	%ebx, %eax
	jmp	.L117
.L127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3084:
	.size	_ZNK6icu_6716JapaneseCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, .-_ZNK6icu_6716JapaneseCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716JapaneseCalendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.type	_ZNK6icu_6716JapaneseCalendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode, @function
_ZNK6icu_6716JapaneseCalendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode:
.LFB3085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %esi
	je	.L142
	call	_ZNK6icu_6717GregorianCalendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode@PLT
.L128:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L143
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	xorl	%esi, %esi
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	(%r12), %edx
	movl	%eax, %r15d
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L128
	movq	_ZL17gJapaneseEraRules(%rip), %rdi
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	cmpl	%edx, %r15d
	jne	.L131
	movq	0(%r13), %rax
	leaq	_ZNK6icu_6716JapaneseCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE(%rip), %rdx
	movq	248(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L132
	movl	_ZL11gCurrentEra(%rip), %esi
	leaq	-72(%rbp), %rdx
	movl	$0, -72(%rbp)
	call	_ZNK6icu_678EraRules12getStartYearEiR10UErrorCode@PLT
	movl	$3, %edx
	movl	$1, %esi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_6717GregorianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE@PLT
	subl	%ebx, %eax
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	-68(%rbp), %rdx
	leal	1(%r15), %esi
	movq	%r12, %rcx
	movq	$0, -68(%rbp)
	movl	$0, -60(%rbp)
	call	_ZNK6icu_678EraRules12getStartDateEiRA3_iR10UErrorCode@PLT
	movl	-68(%rbp), %r13d
	movq	%r12, %rdx
	movl	%r15d, %esi
	movq	_ZL17gJapaneseEraRules(%rip), %rdi
	movl	-64(%rbp), %ebx
	movl	-60(%rbp), %r14d
	call	_ZNK6icu_678EraRules12getStartYearEiR10UErrorCode@PLT
	subl	%eax, %r13d
	movl	%r13d, %eax
	cmpl	$1, %ebx
	jne	.L137
	cmpl	$1, %r14d
	je	.L128
.L137:
	addl	$1, %eax
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L132:
	movl	$3, %edx
	movl	$1, %esi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L128
.L143:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3085:
	.size	_ZNK6icu_6716JapaneseCalendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode, .-_ZNK6icu_6716JapaneseCalendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.weak	_ZTSN6icu_6716JapaneseCalendarE
	.section	.rodata._ZTSN6icu_6716JapaneseCalendarE,"aG",@progbits,_ZTSN6icu_6716JapaneseCalendarE,comdat
	.align 16
	.type	_ZTSN6icu_6716JapaneseCalendarE, @object
	.size	_ZTSN6icu_6716JapaneseCalendarE, 28
_ZTSN6icu_6716JapaneseCalendarE:
	.string	"N6icu_6716JapaneseCalendarE"
	.weak	_ZTIN6icu_6716JapaneseCalendarE
	.section	.data.rel.ro._ZTIN6icu_6716JapaneseCalendarE,"awG",@progbits,_ZTIN6icu_6716JapaneseCalendarE,comdat
	.align 8
	.type	_ZTIN6icu_6716JapaneseCalendarE, @object
	.size	_ZTIN6icu_6716JapaneseCalendarE, 24
_ZTIN6icu_6716JapaneseCalendarE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6716JapaneseCalendarE
	.quad	_ZTIN6icu_6717GregorianCalendarE
	.weak	_ZTVN6icu_6716JapaneseCalendarE
	.section	.data.rel.ro._ZTVN6icu_6716JapaneseCalendarE,"awG",@progbits,_ZTVN6icu_6716JapaneseCalendarE,comdat
	.align 8
	.type	_ZTVN6icu_6716JapaneseCalendarE, @object
	.size	_ZTVN6icu_6716JapaneseCalendarE, 448
_ZTVN6icu_6716JapaneseCalendarE:
	.quad	0
	.quad	_ZTIN6icu_6716JapaneseCalendarE
	.quad	_ZN6icu_6716JapaneseCalendarD1Ev
	.quad	_ZN6icu_6716JapaneseCalendarD0Ev
	.quad	_ZNK6icu_6716JapaneseCalendar17getDynamicClassIDEv
	.quad	_ZNK6icu_6716JapaneseCalendar5cloneEv
	.quad	_ZNK6icu_678CalendareqERKS0_
	.quad	_ZNK6icu_6717GregorianCalendar14isEquivalentToERKNS_8CalendarE
	.quad	_ZN6icu_678Calendar3addENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_6717GregorianCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_6717GregorianCalendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6717GregorianCalendar14inDaylightTimeER10UErrorCode
	.quad	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_6717GregorianCalendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6716JapaneseCalendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6716JapaneseCalendar7getTypeEv
	.quad	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEv
	.quad	_ZN6icu_678Calendar11computeTimeER10UErrorCode
	.quad	_ZN6icu_678Calendar13computeFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode
	.quad	_ZNK6icu_6716JapaneseCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.quad	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE
	.quad	_ZNK6icu_6717GregorianCalendar23handleComputeMonthStartEiia
	.quad	_ZNK6icu_6717GregorianCalendar20handleGetMonthLengthEii
	.quad	_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi
	.quad	_ZN6icu_6716JapaneseCalendar21handleGetExtendedYearEv
	.quad	_ZN6icu_6717GregorianCalendar22handleComputeJulianDayE19UCalendarDateFields
	.quad	_ZN6icu_6717GregorianCalendar35handleGetExtendedYearFromWeekFieldsEii
	.quad	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar23getFieldResolutionTableEv
	.quad	_ZN6icu_6716JapaneseCalendar19handleComputeFieldsEiR10UErrorCode
	.quad	_ZN6icu_6716JapaneseCalendar21getDefaultMonthInYearEi
	.quad	_ZN6icu_6716JapaneseCalendar20getDefaultDayInMonthEii
	.quad	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6716JapaneseCalendar18haveDefaultCenturyEv
	.quad	_ZNK6icu_6716JapaneseCalendar19defaultCenturyStartEv
	.quad	_ZNK6icu_6716JapaneseCalendar23defaultCenturyStartYearEv
	.quad	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode
	.quad	_ZN6icu_678Calendar14setRelatedYearEi
	.quad	_ZNK6icu_6716JapaneseCalendar14internalGetEraEv
	.quad	_ZNK6icu_6717GregorianCalendar11monthLengthEi
	.quad	_ZNK6icu_6717GregorianCalendar11monthLengthEii
	.quad	_ZN6icu_6717GregorianCalendar11getEpochDayER10UErrorCode
	.local	_ZZN6icu_6716JapaneseCalendar16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6716JapaneseCalendar16getStaticClassIDEvE7classID,1,1
	.local	_ZL11gCurrentEra
	.comm	_ZL11gCurrentEra,4,4
	.local	_ZL25gJapaneseEraRulesInitOnce
	.comm	_ZL25gJapaneseEraRulesInitOnce,8,8
	.local	_ZL17gJapaneseEraRules
	.comm	_ZL17gJapaneseEraRules,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
