	.file	"collationbuilder.cpp"
	.text
	.section	.text._ZNK6icu_6711CEFinalizer8modifyCEEl,"axG",@progbits,_ZNK6icu_6711CEFinalizer8modifyCEEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6711CEFinalizer8modifyCEEl
	.type	_ZNK6icu_6711CEFinalizer8modifyCEEl, @function
_ZNK6icu_6711CEFinalizer8modifyCEEl:
.LFB3514:
	.cfi_startproc
	endbr64
	movabsq	$4311744768, %rax
	movl	%esi, %edx
	shrl	$24, %edx
	subl	$6, %edx
	cmpl	$63, %edx
	ja	.L1
	movabsq	$-4629700417037541376, %rcx
	addq	%rsi, %rcx
	andl	$49152, %esi
	movq	%rcx, %rdx
	movq	%rcx, %rax
	sarq	$24, %rcx
	sarq	$43, %rdx
	sarq	$42, %rax
	andl	$63, %ecx
	andl	$8128, %eax
	andl	$1040384, %edx
	orl	%eax, %edx
	movq	8(%rdi), %rax
	orl	%ecx, %edx
	movslq	%edx, %rdx
	orq	(%rax,%rdx,8), %rsi
	movq	%rsi, %rax
.L1:
	ret
	.cfi_endproc
.LFE3514:
	.size	_ZNK6icu_6711CEFinalizer8modifyCEEl, .-_ZNK6icu_6711CEFinalizer8modifyCEEl
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_114BundleImporter8getRulesEPKcS3_RNS_13UnicodeStringERS3_R10UErrorCode, @function
_ZN6icu_6712_GLOBAL__N_114BundleImporter8getRulesEPKcS3_RNS_13UnicodeStringERS3_R10UErrorCode:
.LFB3447:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%r9, %rcx
	jmp	_ZN6icu_6715CollationLoader9loadRulesEPKcS2_RNS_13UnicodeStringER10UErrorCode@PLT
	.cfi_endproc
.LFE3447:
	.size	_ZN6icu_6712_GLOBAL__N_114BundleImporter8getRulesEPKcS3_RNS_13UnicodeStringERS3_R10UErrorCode, .-_ZN6icu_6712_GLOBAL__N_114BundleImporter8getRulesEPKcS3_RNS_13UnicodeStringERS3_R10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_114BundleImporterD2Ev, @function
_ZN6icu_6712_GLOBAL__N_114BundleImporterD2Ev:
.LFB3444:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_114BundleImporterE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6719CollationRuleParser8ImporterD2Ev@PLT
	.cfi_endproc
.LFE3444:
	.size	_ZN6icu_6712_GLOBAL__N_114BundleImporterD2Ev, .-_ZN6icu_6712_GLOBAL__N_114BundleImporterD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_114BundleImporterD1Ev,_ZN6icu_6712_GLOBAL__N_114BundleImporterD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_114BundleImporterD0Ev, @function
_ZN6icu_6712_GLOBAL__N_114BundleImporterD0Ev:
.LFB3446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_114BundleImporterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6719CollationRuleParser8ImporterD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3446:
	.size	_ZN6icu_6712_GLOBAL__N_114BundleImporterD0Ev, .-_ZN6icu_6712_GLOBAL__N_114BundleImporterD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711CEFinalizerD2Ev
	.type	_ZN6icu_6711CEFinalizerD2Ev, @function
_ZN6icu_6711CEFinalizerD2Ev:
.LFB3516:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711CEFinalizerE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6720CollationDataBuilder10CEModifierD2Ev@PLT
	.cfi_endproc
.LFE3516:
	.size	_ZN6icu_6711CEFinalizerD2Ev, .-_ZN6icu_6711CEFinalizerD2Ev
	.globl	_ZN6icu_6711CEFinalizerD1Ev
	.set	_ZN6icu_6711CEFinalizerD1Ev,_ZN6icu_6711CEFinalizerD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711CEFinalizerD0Ev
	.type	_ZN6icu_6711CEFinalizerD0Ev, @function
_ZN6icu_6711CEFinalizerD0Ev:
.LFB3518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711CEFinalizerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6720CollationDataBuilder10CEModifierD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3518:
	.size	_ZN6icu_6711CEFinalizerD0Ev, .-_ZN6icu_6711CEFinalizerD0Ev
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"application of [suppressContractions [set]] failed"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode, @function
_ZN6icu_6716CollationBuilder20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode:
.LFB3493:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jle	.L20
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	movq	%rcx, %rdx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	72(%rdi), %rdi
	movq	%rcx, %rbx
	call	_ZN6icu_6720CollationDataBuilder20suppressContractionsERKNS_10UnicodeSetER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L12
	leaq	.LC0(%rip), %rax
	movq	%rax, (%r12)
.L12:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3493:
	.size	_ZN6icu_6716CollationBuilder20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode, .-_ZN6icu_6716CollationBuilder20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode
	.section	.text._ZNK6icu_6711CEFinalizer10modifyCE32Ej,"axG",@progbits,_ZNK6icu_6711CEFinalizer10modifyCE32Ej,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6711CEFinalizer10modifyCE32Ej
	.type	_ZNK6icu_6711CEFinalizer10modifyCE32Ej, @function
_ZNK6icu_6711CEFinalizer10modifyCE32Ej:
.LFB3513:
	.cfi_startproc
	endbr64
	movabsq	$4311744768, %r8
	movl	%esi, %eax
	testb	$-2, %al
	je	.L21
	movzbl	%ah, %edx
	subl	$6, %edx
	cmpl	$63, %edx
	jbe	.L26
.L21:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	leal	-1077937696(%rsi), %ecx
	sall	$8, %eax
	movl	%ecx, %edx
	movl	%ecx, %esi
	shrl	$8, %ecx
	andl	$49152, %eax
	shrl	$11, %edx
	shrl	$10, %esi
	andl	$63, %ecx
	andl	$1040384, %edx
	andl	$8128, %esi
	orl	%esi, %edx
	orl	%ecx, %edx
	movq	8(%rdi), %rcx
	movslq	%edx, %rdx
	orq	(%rcx,%rdx,8), %rax
	movq	%rax, %r8
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE3513:
	.size	_ZNK6icu_6711CEFinalizer10modifyCE32Ej, .-_ZNK6icu_6711CEFinalizer10modifyCE32Ej
	.section	.rodata.str1.8
	.align 8
.LC1:
	.string	"fetching root CEs for tailored string"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6716CollationBuilder11setCaseBitsERKNS_13UnicodeStringERPKcR10UErrorCode.part.0, @function
_ZN6icu_6716CollationBuilder11setCaseBitsERKNS_13UnicodeStringERPKcR10UErrorCode.part.0:
.LFB4710:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	544(%rdi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L27
	movq	%rdx, %r13
	leal	-1(%r8), %edx
	movq	%rcx, %r15
	movq	%rdi, %rbx
	leaq	296(%rdi), %rax
	xorl	%r12d, %r12d
	leaq	304(%rdi,%rdx,8), %r9
	movabsq	$-72057594037927936, %rcx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L77:
	andl	$768, %edi
	jne	.L32
.L31:
	addl	$1, %r12d
.L32:
	addq	$8, %rax
	cmpq	%rax, %r9
	je	.L76
.L33:
	movq	(%rax), %rdi
	movl	%edi, %edx
	shrl	$24, %edx
	subl	$6, %edx
	cmpl	$63, %edx
	jbe	.L77
	testq	%rcx, %rdi
	jne	.L31
	addq	$8, %rax
	cmpq	%rax, %r9
	jne	.L33
.L76:
	testl	%r12d, %r12d
	je	.L53
	movswl	8(%rsi), %eax
	testb	$17, %al
	jne	.L54
	leaq	10(%rsi), %rcx
	testb	$2, %al
	jne	.L35
	movq	24(%rsi), %rcx
.L35:
	testw	%ax, %ax
	js	.L37
	sarl	$5, %eax
.L38:
	movq	40(%rbx), %rdx
	movq	%rcx, %xmm0
	leaq	-480(%rbp), %r14
	cltq
	punpcklqdq	%xmm0, %xmm0
	leaq	(%rcx,%rax,2), %rax
	movq	%r14, %rdi
	movq	(%rdx), %rsi
	movq	%rdx, -464(%rbp)
	leaq	-432(%rbp), %rdx
	movq	%rdx, -448(%rbp)
	movq	%rsi, -472(%rbp)
	leaq	16+_ZTVN6icu_6722UTF16CollationIteratorE(%rip), %rsi
	movq	%rsi, -480(%rbp)
	movq	%r15, %rsi
	movl	$0, -456(%rbp)
	movl	$40, -440(%rbp)
	movb	$0, -436(%rbp)
	movl	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movl	$-1, -96(%rbp)
	movb	$0, -92(%rbp)
	movq	%rax, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	call	_ZN6icu_6717CollationIterator8fetchCEsER10UErrorCode@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L78
	cmpl	$1, %eax
	jle	.L55
	movq	-448(%rbp), %rdx
	subl	$2, %eax
	xorl	%r8d, %r8d
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	leaq	8(%rdx,%rax,8), %rdi
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L80:
	addl	%ecx, %ecx
	salq	%cl, %rax
	movl	%esi, %ecx
	orq	%rax, %r13
.L41:
	addq	$8, %rdx
	cmpq	%rdi, %rdx
	je	.L79
.L44:
	movq	(%rdx), %rax
	movq	%rax, %rsi
	shrq	$32, %rsi
	je	.L41
	shrl	$14, %eax
	leal	1(%rcx), %esi
	andl	$3, %eax
	cmpl	%r12d, %esi
	jl	.L80
	je	.L56
	cmpl	%r8d, %eax
	jne	.L57
	addq	$8, %rdx
	movl	%esi, %ecx
	cmpq	%rdi, %rdx
	jne	.L44
	.p2align 4,,10
	.p2align 3
.L79:
	cmpl	%r12d, %ecx
	jge	.L43
.L40:
	movq	%r14, %rdi
	call	_ZN6icu_6722UTF16CollationIteratorD1Ev@PLT
	movl	544(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L27
.L34:
	movabsq	$-72057594037927936, %rdi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L51:
	movq	296(%rbx,%rdx,8), %rax
	andb	$63, %ah
	movl	%eax, %ecx
	movl	%eax, %esi
	shrl	$24, %ecx
	subl	$6, %ecx
	cmpl	$63, %ecx
	ja	.L46
	movl	%eax, %ecx
	sarl	$8, %ecx
	andl	$3, %ecx
	je	.L47
	cmpl	$2, %ecx
	jne	.L49
	orb	$-128, %ah
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rax, 296(%rbx,%rdx,8)
	addq	$1, %rdx
	cmpl	%edx, %r8d
	jg	.L51
.L27:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movl	%r12d, %ecx
	movl	%eax, %r8d
	jmp	.L41
.L57:
	movl	$1, %r8d
.L43:
	leal	-2(%r12,%r12), %ecx
	salq	%cl, %r8
	orq	%r8, %r13
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%r13, %rcx
	sarq	$2, %r13
	salq	$14, %rcx
	movzwl	%cx, %ecx
	orq	%rcx, %rax
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L46:
	testq	%rdi, %rax
	jne	.L47
	andl	$-16777216, %esi
	jne	.L49
	testq	%rax, %rax
	je	.L49
	orb	$-128, %ah
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L53:
	xorl	%r13d, %r13d
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L37:
	movl	12(%rsi), %eax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L54:
	xorl	%ecx, %ecx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	.LC1(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_6722UTF16CollationIteratorD1Ev@PLT
	jmp	.L27
.L55:
	xorl	%r13d, %r13d
	jmp	.L40
.L81:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4710:
	.size	_ZN6icu_6716CollationBuilder11setCaseBitsERKNS_13UnicodeStringERPKcR10UErrorCode.part.0, .-_ZN6icu_6716CollationBuilder11setCaseBitsERKNS_13UnicodeStringERPKcR10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode.part.0, @function
_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode.part.0:
.LFB4699:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	560(%rbx), %r12d
	movq	608(%rdi), %rdi
	movq	576(%rbx), %rcx
	testl	%r12d, %r12d
	je	.L83
	movl	%r12d, %eax
	shrl	$31, %eax
	addl	%r12d, %eax
	sarl	%eax
	movslq	%eax, %rdx
	movslq	(%rcx,%rdx,4), %rdx
	movslq	4(%rdi,%rdx,8), %rdx
	movl	%edx, %r8d
	cmpl	%edx, %esi
	je	.L84
	movl	%r12d, %r9d
	xorl	%edx, %edx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L106:
	cmpl	%edx, %eax
	je	.L105
	movl	%eax, %r9d
.L87:
	leal	(%rdx,%r9), %esi
	movl	%esi, %eax
	shrl	$31, %eax
	addl	%esi, %eax
	sarl	%eax
	movslq	%eax, %rsi
	movslq	(%rcx,%rsi,4), %rsi
	movslq	4(%rdi,%rsi,8), %rsi
	movl	%esi, %r8d
	cmpl	%esi, %r13d
	je	.L84
.L85:
	cmpl	%r8d, %r13d
	jb	.L106
	cmpl	%edx, %eax
	je	.L107
	movl	%eax, %edx
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L107:
	addl	$1, %eax
.L105:
	notl	%eax
.L84:
	testl	%eax, %eax
	js	.L108
	xorl	%r15d, %r15d
	testl	%r12d, %r12d
	jle	.L82
	subl	%eax, %r12d
	testl	%r12d, %r12d
	jle	.L82
	cltq
	movl	(%rcx,%rax,4), %r15d
.L82:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	notl	%eax
	movl	%eax, %r12d
.L83:
	movl	592(%rbx), %r15d
	movl	%r15d, %r8d
	addl	$1, %r8d
	js	.L90
	cmpl	596(%rbx), %r8d
	jle	.L97
.L90:
	leaq	584(%rbx), %rdi
	movq	%r14, %rdx
	movl	%r8d, %esi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L92
	movslq	592(%rbx), %rax
	movq	608(%rbx), %rdi
	leal	1(%rax), %r8d
.L91:
	movq	%r13, %rsi
	salq	$32, %rsi
	movq	%rsi, (%rdi,%rax,8)
	movl	%r8d, 592(%rbx)
.L92:
	leaq	552(%rbx), %rdi
	movq	%r14, %rcx
	movl	%r12d, %edx
	movl	%r15d, %esi
	call	_ZN6icu_679UVector3215insertElementAtEiiR10UErrorCode@PLT
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L97:
	movslq	%r15d, %rax
	jmp	.L91
	.cfi_endproc
.LFE4699:
	.size	_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode.part.0, .-_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZNK6icu_6716CollationBuilder24mergeCompositeIntoStringERKNS_13UnicodeStringEiiS3_RS1_S4_R10UErrorCode.part.0, @function
_ZNK6icu_6716CollationBuilder24mergeCompositeIntoStringERKNS_13UnicodeStringEiiS3_RS1_S4_R10UErrorCode.part.0:
.LFB4704:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%r8, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	$1, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$40, %rsp
	movq	%rsi, -64(%rbp)
	xorl	%esi, %esi
	movq	%r9, -72(%rbp)
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movl	%eax, %r12d
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L110
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L111:
	cmpl	%r9d, %r12d
	je	.L119
	testb	$1, %al
	je	.L114
	movq	-64(%rbp), %rax
	movzwl	8(%rax), %eax
	movw	%ax, -52(%rbp)
	movzbl	-52(%rbp), %eax
	notl	%eax
	andl	$1, %eax
.L115:
	testb	%al, %al
	je	.L119
	movq	-72(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movq	-72(%rbp), %rax
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	js	.L120
	movswl	%ax, %edx
	sarl	$5, %edx
.L121:
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	%r14d, %r9d
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	16(%rbp), %rdi
	movl	%r14d, %r9d
	subl	%r12d, %r9d
	movl	%r9d, -52(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movq	16(%rbp), %rax
	movl	-52(%rbp), %r9d
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	js	.L122
	movswl	%ax, %edx
	sarl	$5, %edx
.L123:
	movq	-64(%rbp), %rcx
	movq	16(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movl	%ebx, %esi
	movl	$-1, %ebx
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movb	$0, -53(%rbp)
	xorl	%edx, %edx
.L133:
	testl	%ebx, %ebx
	js	.L135
	movswl	8(%r13), %ecx
	testw	%cx, %cx
	js	.L128
.L148:
	movswl	%cx, %eax
	sarl	$5, %eax
.L129:
	cmpl	%r12d, %eax
	jle	.L130
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	8(%r15), %rdi
	movl	%eax, %ecx
	movq	(%rdi), %rax
	movl	%ecx, -52(%rbp)
	movl	%ecx, %esi
	call	*80(%rax)
	movl	%eax, %edx
	testb	%al, %al
	je	.L119
	movzbl	-53(%rbp), %eax
	cmpb	%al, %dl
	ja	.L119
	movl	-52(%rbp), %ecx
	jb	.L147
	movb	%dl, -52(%rbp)
	cmpl	%ebx, %ecx
	jne	.L119
	movq	-72(%rbp), %rdi
	movl	%ebx, %esi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	xorl	%eax, %eax
	cmpl	$65535, %ebx
	movzbl	-52(%rbp), %edx
	seta	%al
	addl	$1, %eax
	addl	%eax, %r12d
	addl	%eax, %r14d
.L135:
	movq	-64(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L125
	sarl	$5, %eax
.L126:
	movb	%dl, -52(%rbp)
	cmpl	%r14d, %eax
	jle	.L127
	movq	-64(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	8(%r15), %rdi
	movl	%eax, %ebx
	movq	(%rdi), %rax
	movl	%ebx, %esi
	call	*80(%rax)
	movswl	8(%r13), %ecx
	movzbl	-52(%rbp), %edx
	movb	%al, -53(%rbp)
	testw	%cx, %cx
	jns	.L148
.L128:
	movl	12(%r13), %eax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L130:
	testl	%ebx, %ebx
	js	.L136
	cmpb	%dl, -53(%rbp)
	jnb	.L149
	.p2align 4,,10
	.p2align 3
.L119:
	xorl	%r14d, %r14d
.L109:
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	xorl	%r8d, %r8d
	testl	%r12d, %r12d
	js	.L116
	cmpl	%r9d, %r12d
	movl	%r9d, %r8d
	cmovle	%r12d, %r8d
	subl	%r8d, %r9d
.L116:
	leaq	10(%r13), %rcx
	testb	$2, %al
	jne	.L118
	movq	24(%r13), %rcx
.L118:
	movq	-64(%rbp), %rdi
	movl	$2147483647, %edx
	movl	%r14d, %esi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L110:
	movl	12(%r13), %r9d
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L120:
	movq	-72(%rbp), %rax
	movl	12(%rax), %edx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L147:
	movq	-72(%rbp), %rdi
	movl	%ecx, %esi
	movb	%dl, -54(%rbp)
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	-52(%rbp), %ecx
	xorl	%eax, %eax
	movzbl	-54(%rbp), %edx
	cmpl	$65535, %ecx
	seta	%al
	leal	1(%rax,%r12), %r12d
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L125:
	movq	-64(%rbp), %rax
	movl	12(%rax), %eax
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L122:
	movq	16(%rbp), %rax
	movl	12(%rax), %edx
	jmp	.L123
.L149:
	movq	-64(%rbp), %rbx
	movq	-72(%rbp), %rdi
	movl	%r14d, %edx
	movl	$2147483647, %ecx
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	16(%rbp), %rdi
	movl	%r14d, %edx
	movq	%rbx, %rsi
	movl	$2147483647, %ecx
	movl	$1, %r14d
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L109
.L127:
	movswl	8(%r13), %ecx
.L136:
	testw	%cx, %cx
	js	.L137
	sarl	$5, %ecx
.L138:
	movl	$1, %r14d
	cmpl	%r12d, %ecx
	jle	.L109
	movq	-72(%rbp), %rdi
	movl	$2147483647, %ecx
	movl	%r12d, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L109
.L137:
	movl	12(%r13), %ecx
	jmp	.L138
	.cfi_endproc
.LFE4704:
	.size	_ZNK6icu_6716CollationBuilder24mergeCompositeIntoStringERKNS_13UnicodeStringEiiS3_RS1_S4_R10UErrorCode.part.0, .-_ZNK6icu_6716CollationBuilder24mergeCompositeIntoStringERKNS_13UnicodeStringEiiS3_RS1_S4_R10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6716CollationBuilder17insertNodeBetweenEiilR10UErrorCode.part.0, @function
_ZN6icu_6716CollationBuilder17insertNodeBetweenEiilR10UErrorCode.part.0:
.LFB4700:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$584, %rdi
	subq	$24, %rsp
	movl	8(%rdi), %r12d
	movl	%r12d, %esi
	addl	$1, %esi
	js	.L151
	cmpl	596(%rbx), %esi
	jle	.L157
.L151:
	movq	%r14, %rdx
	movq	%rdi, -56(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-56(%rbp), %rdi
	testb	%al, %al
	je	.L153
	movslq	592(%rbx), %rdx
	movq	-64(%rbp), %rcx
	leal	1(%rdx), %esi
.L152:
	movl	%r15d, %eax
	movq	608(%rbx), %r8
	sall	$8, %eax
	cltq
	orq	%rcx, %rax
	movslq	%r13d, %rcx
	salq	$28, %rcx
	orq	%rax, %rcx
	movq	%rcx, (%r8,%rdx,8)
	movl	%esi, 592(%rbx)
.L153:
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L158
	xorl	%edx, %edx
	testl	%r13d, %r13d
	js	.L155
	cmpl	592(%rbx), %r13d
	jge	.L155
	movq	608(%rbx), %rdx
	movslq	%r13d, %rax
	movq	(%rdx,%rax,8), %rsi
	movq	%rsi, %rdx
	andq	$-268435201, %rdx
.L155:
	movl	%r12d, %eax
	movq	%rdi, -56(%rbp)
	sall	$8, %eax
	movslq	%eax, %rsi
	orq	%rdx, %rsi
	movl	%r13d, %edx
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	testl	%r15d, %r15d
	je	.L150
	movq	-56(%rbp), %rdi
	movl	$0, %eax
	js	.L156
	cmpl	592(%rbx), %r15d
	jge	.L156
	movq	608(%rbx), %rax
	movslq	%r15d, %rdx
	movabsq	$-281474708275201, %rsi
	andq	(%rax,%rdx,8), %rsi
	movq	%rsi, %rax
.L156:
	movslq	%r12d, %rsi
	movl	%r15d, %edx
	salq	$28, %rsi
	orq	%rax, %rsi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
.L150:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movslq	%r12d, %rdx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L158:
	xorl	%r12d, %r12d
	jmp	.L150
	.cfi_endproc
.LFE4700:
	.size	_ZN6icu_6716CollationBuilder17insertNodeBetweenEiilR10UErrorCode.part.0, .-_ZN6icu_6716CollationBuilder17insertNodeBetweenEiilR10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode.part.0, @function
_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode.part.0:
.LFB4701:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	subl	$1, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testl	%esi, %esi
	js	.L171
	cmpl	592(%rdi), %esi
	jge	.L171
	movq	608(%rdi), %r10
	movslq	%esi, %rax
	movq	(%r10,%rax,8), %rax
	cmpl	$1278, %edx
	jbe	.L212
.L172:
	sarl	$8, %eax
	andl	$1048575, %eax
	movl	%eax, %r9d
	je	.L177
	movl	592(%r14), %esi
	testl	%ecx, %ecx
	jns	.L176
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L215:
	movslq	%r9d, %rax
	movq	(%r10,%rax,8), %rdx
	movl	%edx, %edi
	movl	%edx, %eax
	andl	$3, %edi
	cmpl	%edi, %ecx
	jge	.L214
.L179:
	sarl	$8, %eax
	movl	%r9d, %r12d
	andl	$1048575, %eax
	je	.L193
	movl	%eax, %r9d
.L176:
	cmpl	%esi, %r9d
	jl	.L215
	testl	%ecx, %ecx
	jle	.L216
.L177:
	addq	$24, %rsp
	salq	$48, %r13
	movslq	%ecx, %rcx
	movl	%r12d, %esi
	popq	%rbx
	orq	%r13, %rcx
	popq	%r12
	movq	%r14, %rdi
	popq	%r13
	movl	%r9d, %edx
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6716CollationBuilder17insertNodeBetweenEiilR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	cmpl	$1278, %edx
	ja	.L193
	xorl	%eax, %eax
	cmpl	$1, %ecx
	je	.L181
.L183:
	movslq	%ecx, %rbx
	movl	$32, %esi
	movabsq	$360287970189639680, %r15
	orq	%rbx, %r15
.L180:
	orq	%rax, %rsi
	movl	%r12d, %edx
	leaq	584(%r14), %rdi
	movq	%r8, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	movq	-64(%rbp), %r8
	xorl	%r9d, %r9d
	movq	-56(%rbp), %rax
	movl	(%r8), %edx
	testl	%edx, %edx
	jg	.L170
	sarl	$8, %eax
	movq	%r13, %rcx
	movl	%r12d, %esi
	movq	%r14, %rdi
	andl	$1048575, %eax
	salq	$48, %rcx
	movl	%eax, %edx
	orq	%rbx, %rcx
	movl	%eax, -56(%rbp)
	call	_ZN6icu_6716CollationBuilder17insertNodeBetweenEiilR10UErrorCode.part.0
	movq	-64(%rbp), %r8
	movl	%eax, %r9d
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L170
	movl	-56(%rbp), %edx
	movl	%r9d, %esi
	movq	%r15, %rcx
	movq	%r14, %rdi
	movl	%r9d, -56(%rbp)
	call	_ZN6icu_6716CollationBuilder17insertNodeBetweenEiilR10UErrorCode.part.0
	movl	-56(%rbp), %r9d
.L170:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	testl	%r13d, %r13d
	je	.L170
	xorl	%eax, %eax
	jmp	.L179
.L213:
	movl	%eax, %r12d
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L217:
	movslq	%r12d, %rax
	movq	(%r10,%rax,8), %rax
	sarl	$8, %eax
	andl	$1048575, %eax
	je	.L193
	movl	%eax, %r12d
.L185:
	cmpl	%r12d, %esi
	jg	.L217
.L193:
	xorl	%r9d, %r9d
	jmp	.L177
.L181:
	movq	%rax, %r15
	movl	$1, %ebx
	andq	$-33, %rax
	movabsq	$360287970189639681, %rdx
	andl	$32, %r15d
	movl	$64, %esi
	orq	%rdx, %r15
	jmp	.L180
.L212:
	cmpl	$1, %ecx
	je	.L218
	testb	$32, %al
	jne	.L172
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L218:
	testb	$64, %al
	je	.L181
	sarl	$8, %eax
	andl	$1048575, %eax
	movl	%eax, %r9d
	je	.L177
	movl	592(%r14), %esi
	jmp	.L176
.L214:
	jg	.L177
	testb	$8, %dl
	jne	.L179
	shrq	$48, %rdx
	cmpl	%edx, %r13d
	je	.L170
	jb	.L177
	jmp	.L179
	.cfi_endproc
.LFE4701:
	.size	_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode.part.0, .-_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode.part.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollatorC2Ev
	.type	_ZN6icu_6717RuleBasedCollatorC2Ev, @function
_ZN6icu_6717RuleBasedCollatorC2Ev:
.LFB3449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678CollatorC2Ev@PLT
	leaq	16+_ZTVN6icu_6717RuleBasedCollatorE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, (%rbx)
	leaq	40(%rbx), %rdi
	xorl	%r8d, %r8d
	leaq	.LC2(%rip), %rsi
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movb	$0, 268(%rbx)
	movl	$0, 264(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3449:
	.size	_ZN6icu_6717RuleBasedCollatorC2Ev, .-_ZN6icu_6717RuleBasedCollatorC2Ev
	.globl	_ZN6icu_6717RuleBasedCollatorC1Ev
	.set	_ZN6icu_6717RuleBasedCollatorC1Ev,_ZN6icu_6717RuleBasedCollatorC2Ev
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"CollationBuilder fields initialization failed"
	.align 8
.LC4:
	.string	"CollationBuilder initialization failed"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilderC2EPKNS_18CollationTailoringER10UErrorCode
	.type	_ZN6icu_6716CollationBuilderC2EPKNS_18CollationTailoringER10UErrorCode, @function
_ZN6icu_6716CollationBuilderC2EPKNS_18CollationTailoringER10UErrorCode:
.LFB3473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6716CollationBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	%rdx, %rdi
	call	_ZN6icu_6711Normalizer214getNFDInstanceER10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%rax, 8(%rbx)
	call	_ZN6icu_6718Normalizer2Factory14getFCDInstanceER10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%rax, 16(%rbx)
	call	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode@PLT
	movq	%r13, %xmm1
	movl	$632, %edi
	movq	%rax, %xmm0
	movq	24(%r13), %rax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	movl	136(%rax), %edx
	movq	%rax, %xmm0
	movhps	128(%rax), %xmm0
	movl	$0, 64(%rbx)
	movl	%edx, 56(%rbx)
	movups	%xmm0, 40(%rbx)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L222
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720CollationDataBuilderC1ER10UErrorCode@PLT
.L222:
	movq	%r13, 72(%rbx)
	leaq	88(%rbx), %rdi
	movb	$1, 80(%rbx)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	552(%rbx), %rdi
	movq	%r12, %rsi
	movq	$0, 288(%rbx)
	movl	$0, 544(%rbx)
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	leaq	584(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_679UVector64C1ER10UErrorCode@PLT
	movq	24(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK6icu_6715Normalizer2Impl19ensureCanonIterDataER10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L230
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L231
	movq	40(%rbx), %rsi
	movq	%r12, %rdx
	call	_ZN6icu_6720CollationDataBuilder16initForTailoringEPKNS_13CollationDataER10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L221
	leaq	.LC4(%rip), %rax
	movq	%rax, 288(%rbx)
.L221:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	leaq	.LC3(%rip), %rax
	movq	%rax, 288(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	movl	$7, (%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3473:
	.size	_ZN6icu_6716CollationBuilderC2EPKNS_18CollationTailoringER10UErrorCode, .-_ZN6icu_6716CollationBuilderC2EPKNS_18CollationTailoringER10UErrorCode
	.globl	_ZN6icu_6716CollationBuilderC1EPKNS_18CollationTailoringER10UErrorCode
	.set	_ZN6icu_6716CollationBuilderC1EPKNS_18CollationTailoringER10UErrorCode,_ZN6icu_6716CollationBuilderC2EPKNS_18CollationTailoringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder17getWeight16BeforeEili
	.type	_ZN6icu_6716CollationBuilder17getWeight16BeforeEili, @function
_ZN6icu_6716CollationBuilder17getWeight16BeforeEili:
.LFB3481:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	movq	%rdx, %rsi
	andl	$3, %eax
	shrq	$48, %rsi
	movq	%rsi, %r8
	cmpl	$2, %eax
	movl	$1280, %eax
	cmovne	%eax, %r8d
	.p2align 4,,10
	.p2align 3
.L235:
	movl	%edx, %eax
	testb	$2, %dl
	je	.L234
.L249:
	sarq	$28, %rdx
	movl	%edx, %esi
	xorl	%edx, %edx
	andl	$1048575, %esi
	cmpl	592(%rdi), %esi
	jge	.L235
	movq	608(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rdx
	movl	%edx, %eax
	testb	$2, %dl
	jne	.L249
.L234:
	testb	$8, %dl
	jne	.L232
	andl	$3, %eax
	movq	%rdx, %rsi
	movl	$1280, %r9d
	shrq	$48, %rsi
	cmpl	$1, %eax
	cmove	%esi, %r9d
	.p2align 4,,10
	.p2align 3
.L240:
	testb	$3, %dl
	je	.L239
.L250:
	sarq	$28, %rdx
	movl	%edx, %esi
	xorl	%edx, %edx
	andl	$1048575, %esi
	cmpl	592(%rdi), %esi
	jge	.L240
	movq	608(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rdx
	testb	$3, %dl
	jne	.L250
.L239:
	testb	$8, %dl
	jne	.L232
	sarq	$32, %rdx
	addq	$48, %rdi
	movq	%rdx, %rsi
	cmpl	$1, %ecx
	je	.L251
	movl	%r8d, %ecx
	movl	%r9d, %edx
	jmp	_ZNK6icu_6721CollationRootElements17getTertiaryBeforeEjjj@PLT
	.p2align 4,,10
	.p2align 3
.L251:
	movl	%r9d, %edx
	jmp	_ZNK6icu_6721CollationRootElements18getSecondaryBeforeEjj@PLT
	.p2align 4,,10
	.p2align 3
.L232:
	movl	$256, %eax
	ret
	.cfi_endproc
.LFE3481:
	.size	_ZN6icu_6716CollationBuilder17getWeight16BeforeEili, .-_ZN6icu_6716CollationBuilder17getWeight16BeforeEili
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode, @function
_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode:
.LFB3487:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L254
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	jmp	_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode.part.0
	.cfi_endproc
.LFE3487:
	.size	_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode, .-_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder17insertNodeBetweenEiilR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder17insertNodeBetweenEiilR10UErrorCode, @function
_ZN6icu_6716CollationBuilder17insertNodeBetweenEiilR10UErrorCode:
.LFB3490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L256
.L262:
	xorl	%r15d, %r15d
.L255:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movl	592(%rdi), %r15d
	movl	%esi, %r13d
	movq	%rdi, %rbx
	movl	%edx, %r14d
	movq	%r8, %r12
	leaq	584(%rdi), %rdi
	movl	%r15d, %esi
	addl	$1, %esi
	js	.L258
	cmpl	596(%rbx), %esi
	jle	.L265
.L258:
	movq	%r12, %rdx
	movq	%rcx, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rcx
	testb	%al, %al
	jne	.L260
	movl	(%r12), %eax
.L261:
	testl	%eax, %eax
	jg	.L262
	xorl	%edx, %edx
	testl	%r13d, %r13d
	js	.L263
	cmpl	592(%rbx), %r13d
	jge	.L263
	movq	608(%rbx), %rdx
	movslq	%r13d, %rax
	movq	(%rdx,%rax,8), %rsi
	movq	%rsi, %rdx
	andq	$-268435201, %rdx
.L263:
	movl	%r15d, %eax
	movq	%rdi, -56(%rbp)
	sall	$8, %eax
	movslq	%eax, %rsi
	orq	%rdx, %rsi
	movl	%r13d, %edx
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	testl	%r14d, %r14d
	je	.L255
	movq	-56(%rbp), %rdi
	movl	$0, %eax
	js	.L264
	cmpl	592(%rbx), %r14d
	jge	.L264
	movq	608(%rbx), %rax
	movslq	%r14d, %rdx
	movabsq	$-281474708275201, %rsi
	andq	(%rax,%rdx,8), %rsi
	movq	%rsi, %rax
.L264:
	movslq	%r15d, %rsi
	movl	%r14d, %edx
	salq	$28, %rsi
	orq	%rax, %rsi
	call	_ZN6icu_679UVector6412setElementAtEli@PLT
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L260:
	movslq	592(%rbx), %r8
	movl	(%r12), %eax
	leal	1(%r8), %esi
.L259:
	movl	%r14d, %edx
	movq	608(%rbx), %r9
	sall	$8, %edx
	movslq	%edx, %rdx
	orq	%rdx, %rcx
	movslq	%r13d, %rdx
	salq	$28, %rdx
	orq	%rdx, %rcx
	movq	%rcx, (%r9,%r8,8)
	movl	%esi, 592(%rbx)
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L265:
	movslq	%r15d, %r8
	jmp	.L259
	.cfi_endproc
.LFE3490:
	.size	_ZN6icu_6716CollationBuilder17insertNodeBetweenEiilR10UErrorCode, .-_ZN6icu_6716CollationBuilder17insertNodeBetweenEiilR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716CollationBuilder14findCommonNodeEii
	.type	_ZNK6icu_6716CollationBuilder14findCommonNodeEii, @function
_ZNK6icu_6716CollationBuilder14findCommonNodeEii:
.LFB3491:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	js	.L283
	cmpl	592(%rdi), %esi
	jl	.L286
.L283:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L275:
	cmpl	%edx, %r8d
	jge	.L276
	cmpl	$1, %edx
	je	.L287
	shrq	$5, %rax
	xorq	$1, %rax
	andl	$1, %eax
.L278:
	testb	%al, %al
	jne	.L276
	sarl	$8, %ecx
	movl	%ecx, %eax
	movl	592(%rdi), %ecx
	andl	$1048575, %eax
	cmpl	%ecx, %eax
	jge	.L284
	movq	608(%rdi), %r8
	cltq
	movq	(%r8,%rax,8), %rax
	.p2align 4,,10
	.p2align 3
.L280:
	sarl	$8, %eax
	andl	$1048575, %eax
	movl	%eax, %esi
	cmpl	%ecx, %eax
	jge	.L285
	cltq
	movq	(%r8,%rax,8), %rax
	testb	$8, %al
	jne	.L280
	movl	%eax, %edi
	andl	$3, %edi
	cmpl	%edi, %edx
	jl	.L280
	movq	%rax, %rdi
	shrq	$48, %rdi
	cmpl	$1279, %edi
	jbe	.L280
.L276:
	movl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	movq	608(%rdi), %r8
	movslq	%esi, %rax
	movq	(%r8,%rax,8), %rax
	movl	%eax, %r8d
	movl	%eax, %ecx
	andl	$3, %r8d
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L287:
	shrq	$6, %rax
	xorq	$1, %rax
	andl	$1, %eax
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L285:
	xorl	%eax, %eax
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L284:
	movq	608(%rdi), %r8
	xorl	%eax, %eax
	jmp	.L280
	.cfi_endproc
.LFE3491:
	.size	_ZNK6icu_6716CollationBuilder14findCommonNodeEii, .-_ZNK6icu_6716CollationBuilder14findCommonNodeEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode, @function
_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode:
.LFB3488:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L289
	cmpl	$1280, %edx
	je	.L291
	jmp	_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L289:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	movl	%ecx, %edx
	jmp	_ZNK6icu_6716CollationBuilder14findCommonNodeEii
	.cfi_endproc
.LFE3488:
	.size	_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode, .-_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6716CollationBuilder23insertTailoredNodeAfterEiiR10UErrorCode.part.0, @function
_ZN6icu_6716CollationBuilder23insertTailoredNodeAfterEiiR10UErrorCode.part.0:
.LFB4703:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movl	%edx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rcx, %rbx
	subq	$8, %rsp
	testl	%edx, %edx
	jg	.L307
.L293:
	xorl	%r9d, %r9d
	testl	%esi, %esi
	js	.L294
	movl	592(%r11), %eax
	cmpl	%eax, %esi
	jge	.L294
	movq	608(%r11), %rdi
	movslq	%esi, %rdx
	movq	(%rdi,%rdx,8), %rdx
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L308:
	movslq	%edx, %rdx
	movq	(%rdi,%rdx,8), %rdx
	movl	%edx, %ecx
	andl	$3, %ecx
	cmpl	%ecx, %r10d
	jge	.L294
.L296:
	movl	%r9d, %esi
.L297:
	sarl	$8, %edx
	andl	$1048575, %edx
	movl	%edx, %r9d
	je	.L294
	cmpl	%eax, %edx
	jl	.L308
	testl	%r10d, %r10d
	jns	.L294
	xorl	%edx, %edx
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L294:
	addq	$8, %rsp
	orl	$8, %r10d
	movq	%rbx, %r8
	movl	%r9d, %edx
	popq	%rbx
	movslq	%r10d, %rcx
	movq	%r11, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6716CollationBuilder17insertNodeBetweenEiilR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	movl	$1, %edx
	call	_ZNK6icu_6716CollationBuilder14findCommonNodeEii
	movl	%eax, %esi
	cmpl	$1, %r10d
	je	.L293
	movl	$2, %edx
	movq	%r11, %rdi
	call	_ZNK6icu_6716CollationBuilder14findCommonNodeEii
	movl	%eax, %esi
	jmp	.L293
	.cfi_endproc
.LFE4703:
	.size	_ZN6icu_6716CollationBuilder23insertTailoredNodeAfterEiiR10UErrorCode.part.0, .-_ZN6icu_6716CollationBuilder23insertTailoredNodeAfterEiiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder23insertTailoredNodeAfterEiiR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder23insertTailoredNodeAfterEiiR10UErrorCode, @function
_ZN6icu_6716CollationBuilder23insertTailoredNodeAfterEiiR10UErrorCode:
.LFB3489:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L310
	jmp	_ZN6icu_6716CollationBuilder23insertTailoredNodeAfterEiiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L310:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3489:
	.size	_ZN6icu_6716CollationBuilder23insertTailoredNodeAfterEiiR10UErrorCode, .-_ZN6icu_6716CollationBuilder23insertTailoredNodeAfterEiiR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6716CollationBuilder25findOrInsertNodeForRootCEEliR10UErrorCode.part.0, @function
_ZN6icu_6716CollationBuilder25findOrInsertNodeForRootCEEliR10UErrorCode.part.0:
.LFB4702:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	movq	%rcx, %rdx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	sarq	$32, %rsi
	call	_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode.part.0
	testl	%r12d, %r12d
	jg	.L325
.L311:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L318
	movl	%ebx, %edx
	shrl	$16, %edx
	cmpl	$1280, %edx
	je	.L326
	movq	%r13, %r8
	movl	$1, %ecx
	movl	%eax, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode.part.0
.L313:
	cmpl	$1, %r12d
	je	.L311
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L324
	xorl	%eax, %eax
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L318:
	xorl	%eax, %eax
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L326:
	movl	$1, %edx
	movl	%eax, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6716CollationBuilder14findCommonNodeEii
	cmpl	$1, %r12d
	je	.L311
.L324:
	movl	%ebx, %edx
	andl	$16191, %edx
	cmpl	$1280, %edx
	je	.L327
	popq	%rbx
	movq	%r13, %r8
	popq	%r12
	movq	%r14, %rdi
	popq	%r13
	movl	$2, %ecx
	popq	%r14
	movl	%eax, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	movl	$2, %edx
	popq	%r13
	movl	%eax, %esi
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6716CollationBuilder14findCommonNodeEii
	.cfi_endproc
.LFE4702:
	.size	_ZN6icu_6716CollationBuilder25findOrInsertNodeForRootCEEliR10UErrorCode.part.0, .-_ZN6icu_6716CollationBuilder25findOrInsertNodeForRootCEEliR10UErrorCode.part.0
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"tailoring relative to an unassigned code point not supported"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6716CollationBuilder22findOrInsertNodeForCEsEiRPKcR10UErrorCode.part.0, @function
_ZN6icu_6716CollationBuilder22findOrInsertNodeForCEsEiRPKcR10UErrorCode.part.0:
.LFB4707:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	544(%rdi), %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	testl	%r11d, %r11d
	je	.L334
	movabsq	$-72057594037927936, %r13
	leal	-1(%r11), %esi
	subq	$2, %r11
	movslq	%esi, %rax
	movl	%esi, %esi
	subq	%rsi, %r11
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L342:
	movl	%esi, %r9d
	sarl	$8, %r9d
	andl	$3, %r9d
.L332:
	cmpl	%r9d, %r12d
	jge	.L333
.L343:
	subq	$1, %rax
	movl	%r10d, 544(%rdi)
	cmpq	%rax, %r11
	je	.L334
.L335:
	movq	296(%rdi,%rax,8), %rsi
	movl	%eax, %r10d
	movl	%esi, %r8d
	movl	%esi, %ebx
	shrl	$24, %r8d
	subl	$6, %r8d
	cmpl	$63, %r8d
	jbe	.L342
	xorl	%r9d, %r9d
	testq	%r13, %rsi
	jne	.L332
	andl	$-16777216, %ebx
	movl	$1, %r9d
	jne	.L332
	cmpq	$1, %rsi
	sbbl	%r9d, %r9d
	andl	$13, %r9d
	addl	$2, %r9d
	cmpl	%r9d, %r12d
	jl	.L343
	.p2align 4,,10
	.p2align 3
.L333:
	cmpl	$63, %r8d
	ja	.L336
	movabsq	$-4629700417037541376, %rax
	popq	%rbx
	popq	%r12
	addq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rsi, %rax
	movq	%rsi, %rdx
	sarq	$24, %rsi
	sarq	$43, %rax
	sarq	$42, %rdx
	andl	$63, %esi
	andl	$1040384, %eax
	andl	$8128, %edx
	orl	%edx, %eax
	orl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movq	$0, 296(%rdi)
	xorl	%esi, %esi
	movl	$1, 544(%rdi)
.L330:
	popq	%rbx
	movl	%r12d, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6716CollationBuilder25findOrInsertNodeForRootCEEliR10UErrorCode.part.0
.L336:
	.cfi_restore_state
	movq	%rsi, %rax
	sarq	$56, %rax
	cmpb	$-2, %al
	jne	.L330
	leaq	.LC5(%rip), %rax
	movl	$16, (%rcx)
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4707:
	.size	_ZN6icu_6716CollationBuilder22findOrInsertNodeForCEsEiRPKcR10UErrorCode.part.0, .-_ZN6icu_6716CollationBuilder22findOrInsertNodeForCEsEiRPKcR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder22findOrInsertNodeForCEsEiRPKcR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder22findOrInsertNodeForCEsEiRPKcR10UErrorCode, @function
_ZN6icu_6716CollationBuilder22findOrInsertNodeForCEsEiRPKcR10UErrorCode:
.LFB3484:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L354
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	544(%rdi), %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	testl	%r11d, %r11d
	je	.L351
	movabsq	$-72057594037927936, %r13
	leal	-1(%r11), %esi
	subq	$2, %r11
	movslq	%esi, %rax
	movl	%esi, %esi
	subq	%rsi, %r11
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L362:
	movl	%esi, %r9d
	sarl	$8, %r9d
	andl	$3, %r9d
.L349:
	cmpl	%r9d, %r12d
	jge	.L350
.L363:
	subq	$1, %rax
	movl	%r10d, 544(%rdi)
	cmpq	%rax, %r11
	je	.L351
.L352:
	movq	296(%rdi,%rax,8), %rsi
	movl	%eax, %r10d
	movl	%esi, %r8d
	movl	%esi, %ebx
	shrl	$24, %r8d
	subl	$6, %r8d
	cmpl	$63, %r8d
	jbe	.L362
	xorl	%r9d, %r9d
	testq	%r13, %rsi
	jne	.L349
	andl	$-16777216, %ebx
	movl	$1, %r9d
	jne	.L349
	cmpq	$1, %rsi
	sbbl	%r9d, %r9d
	andl	$13, %r9d
	addl	$2, %r9d
	cmpl	%r9d, %r12d
	jl	.L363
	.p2align 4,,10
	.p2align 3
.L350:
	cmpl	$63, %r8d
	ja	.L353
	movabsq	$-4629700417037541376, %rax
	popq	%rbx
	popq	%r12
	addq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rsi, %rax
	movq	%rsi, %rdx
	sarq	$24, %rsi
	sarq	$43, %rax
	sarq	$42, %rdx
	andl	$63, %esi
	andl	$1040384, %eax
	andl	$8128, %edx
	orl	%edx, %eax
	orl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	$0, 296(%rdi)
	xorl	%esi, %esi
	movl	$1, 544(%rdi)
.L347:
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movl	%r12d, %edx
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6716CollationBuilder25findOrInsertNodeForRootCEEliR10UErrorCode.part.0
.L353:
	.cfi_restore_state
	movq	%rsi, %rax
	sarq	$56, %rax
	cmpb	$-2, %al
	jne	.L347
	leaq	.LC5(%rip), %rax
	movl	$16, (%rcx)
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3484:
	.size	_ZN6icu_6716CollationBuilder22findOrInsertNodeForCEsEiRPKcR10UErrorCode, .-_ZN6icu_6716CollationBuilder22findOrInsertNodeForCEsEiRPKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder25findOrInsertNodeForRootCEEliR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder25findOrInsertNodeForRootCEEliR10UErrorCode, @function
_ZN6icu_6716CollationBuilder25findOrInsertNodeForRootCEEliR10UErrorCode:
.LFB3485:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jle	.L365
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	movq	%rcx, %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	sarq	$32, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	call	_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode.part.0
	testl	%r13d, %r13d
	jg	.L381
.L364:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	.cfi_restore_state
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L380
	movl	%r12d, %edx
	shrl	$16, %edx
	cmpl	$1280, %edx
	je	.L382
	movq	%rbx, %r8
	movl	$1, %ecx
	movl	%eax, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode.part.0
	cmpl	$1, %r13d
	je	.L364
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L380
.L379:
	movl	%r12d, %edx
	andl	$16191, %edx
	cmpl	$1280, %edx
	je	.L383
	movq	%rbx, %r8
	movq	%r14, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movl	$2, %ecx
	popq	%r12
	.cfi_restore 12
	movl	%eax, %esi
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	.cfi_restore_state
	movl	$1, %edx
	movl	%eax, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6716CollationBuilder14findCommonNodeEii
	cmpl	$1, %r13d
	je	.L364
	jmp	.L379
.L383:
	popq	%rbx
	.cfi_restore 3
	movq	%r14, %rdi
	popq	%r12
	.cfi_restore 12
	movl	$2, %edx
	popq	%r13
	.cfi_restore 13
	movl	%eax, %esi
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6716CollationBuilder14findCommonNodeEii
	.cfi_endproc
.LFE3485:
	.size	_ZN6icu_6716CollationBuilder25findOrInsertNodeForRootCEEliR10UErrorCode, .-_ZN6icu_6716CollationBuilder25findOrInsertNodeForRootCEEliR10UErrorCode
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"reset to [last implicit] not supported"
	.align 8
.LC7:
	.string	"LDML forbids tailoring to U+FFFF"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB8:
	.text
.LHOTB8:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder23getSpecialResetPositionERKNS_13UnicodeStringERPKcR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder23getSpecialResetPositionERKNS_13UnicodeStringERPKcR10UErrorCode, @function
_ZN6icu_6716CollationBuilder23getSpecialResetPositionERKNS_13UnicodeStringERPKcR10UErrorCode:
.LFB3482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L385
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L386:
	cmpl	$1, %ecx
	jbe	.L387
	testb	$2, %al
	jne	.L473
	movq	24(%rsi), %rsi
.L389:
	movzwl	2(%rsi), %ebx
	subl	$10240, %ebx
	cmpl	$13, %ebx
	ja	.L387
	leaq	.L391(%rip), %rsi
	movl	%ebx, %ecx
	movslq	(%rsi,%rcx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L391:
	.long	.L472-.L391
	.long	.L472-.L391
	.long	.L402-.L391
	.long	.L401-.L391
	.long	.L400-.L391
	.long	.L399-.L391
	.long	.L398-.L391
	.long	.L397-.L391
	.long	.L396-.L391
	.long	.L395-.L391
	.long	.L394-.L391
	.long	.L393-.L391
	.long	.L392-.L391
	.long	.L390-.L391
	.text
	.p2align 4,,10
	.p2align 3
.L473:
	addq	$10, %rsi
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L393:
	leaq	.LC6(%rip), %rax
	movl	$16, (%r14)
	movq	%rax, (%rdx)
	.p2align 4,,10
	.p2align 3
.L472:
	xorl	%r12d, %r12d
.L384:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	movl	12(%rsi), %ecx
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L392:
	movl	(%r14), %eax
	xorl	%r15d, %r15d
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movabsq	$-71492444977363712, %r12
	.p2align 4,,10
	.p2align 3
.L407:
	testl	%eax, %eax
	jg	.L472
.L413:
	movq	%r12, %rsi
	movq	%r14, %rcx
	movl	%r8d, %edx
	movq	%r13, %rdi
	movb	%r9b, -53(%rbp)
	movl	%r8d, -52(%rbp)
	call	_ZN6icu_6716CollationBuilder25findOrInsertNodeForRootCEEliR10UErrorCode.part.0
	movl	(%r14), %esi
	testl	%esi, %esi
	jg	.L472
	andl	$1, %ebx
	testl	%eax, %eax
	movl	-52(%rbp), %r8d
	movzbl	-53(%rbp), %r9d
	jns	.L474
.L414:
	testl	%ebx, %ebx
	jne	.L384
	testb	%r9b, %r9b
	je	.L384
.L418:
	sarq	$32, %r12
	leaq	48(%r13), %rbx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6721CollationRootElements11findPrimaryEj@PLT
	movq	40(%r13), %rcx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movl	%eax, %edx
	movl	%r12d, %eax
	movq	72(%rcx), %rcx
	shrl	$24, %eax
	movsbl	(%rcx,%rax), %ecx
	call	_ZNK6icu_6721CollationRootElements15getPrimaryAfterEjia@PLT
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L472
	salq	$32, %rax
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %r12
	sarq	$32, %rax
	movl	%eax, %esi
	orq	$83887360, %r12
	call	_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode.part.0
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L472
	testl	%eax, %eax
	js	.L384
	cmpl	592(%r13), %eax
	jge	.L384
	movq	608(%r13), %r10
	movslq	%eax, %rdx
	movq	(%r10,%rdx,8), %r8
	movq	%r8, %rdi
	andl	$96, %edi
	.p2align 4,,10
	.p2align 3
.L420:
	testq	%rdi, %rdi
	je	.L384
	movq	%r8, %rcx
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L395:
	movq	40(%r13), %rdi
	movl	$17, %esi
	call	_ZNK6icu_6713CollationData23getFirstPrimaryForGroupEi@PLT
	leaq	48(%r13), %rdi
	movl	%eax, %esi
	call	_ZNK6icu_6721CollationRootElements25firstCEWithPrimaryAtLeastEj@PLT
.L469:
	movq	%rax, %r12
	xorl	%r15d, %r15d
	movl	(%r14), %eax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L396:
	movl	64(%r13), %eax
	leaq	48(%r13), %rdi
	leal	1(%rax), %esi
	call	_ZNK6icu_6721CollationRootElements25firstCEWithPrimaryAtLeastEj@PLT
.L470:
	movq	%rax, %r12
	xorl	%r15d, %r15d
	movl	(%r14), %eax
	movl	$1, %r9d
	xorl	%r8d, %r8d
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L394:
	movq	40(%r13), %rdi
	movq	%r14, %rdx
	movl	$19968, %esi
	call	_ZNK6icu_6713CollationData11getSingleCEEiR10UErrorCode@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L397:
	movl	64(%r13), %eax
	leaq	48(%r13), %rdi
	leal	1(%rax), %esi
	call	_ZNK6icu_6721CollationRootElements23lastCEWithPrimaryBeforeEj@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L398:
	movq	48(%r13), %rax
	movl	8(%rax), %edx
	movl	(%rax,%rdx,4), %eax
	salq	$32, %rax
	orq	$83887360, %rax
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L399:
	movq	48(%r13), %rax
	movl	$256, %r15d
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	(%rax,%rdx,4), %eax
	movl	%eax, %r12d
	movl	(%r14), %eax
	andl	$-129, %r12d
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L400:
	movl	(%r14), %r9d
	testl	%r9d, %r9d
	jg	.L472
	xorl	%esi, %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode.part.0
	movl	(%r14), %r8d
	movl	%eax, %esi
	testl	%r8d, %r8d
	jg	.L472
	movq	%r13, %rdi
	movq	%r14, %r8
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode.part.0
	movl	(%r14), %edi
	testl	%edi, %edi
	jg	.L472
	testl	%eax, %eax
	jns	.L475
	.p2align 4,,10
	.p2align 3
.L408:
	movq	48(%r13), %rax
	movl	$256, %r15d
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movl	4(%rax), %edx
	movl	(%rax,%rdx,4), %eax
	movl	%eax, %r12d
	andl	$-129, %r12d
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L401:
	movq	48(%r13), %rax
	movl	$512, %r15d
	xorl	%r9d, %r9d
	movl	$2, %r8d
	movl	4(%rax), %edx
	subl	$1, %edx
	movl	(%rax,%rdx,4), %eax
	movl	%eax, %r12d
	movl	(%r14), %eax
	andl	$-129, %r12d
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L402:
	movl	(%r14), %r11d
	testl	%r11d, %r11d
	jg	.L472
	movq	%r14, %rcx
	movl	$2, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6716CollationBuilder25findOrInsertNodeForRootCEEliR10UErrorCode.part.0
	movl	(%r14), %r10d
	testl	%r10d, %r10d
	jg	.L472
	testl	%eax, %eax
	jns	.L476
.L406:
	movq	48(%r13), %rax
	movl	(%rax), %edx
	movl	(%rax,%rdx,4), %eax
	movl	%eax, %r12d
	andl	$-129, %r12d
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L390:
	leaq	.LC7(%rip), %rax
	movl	$1, (%r14)
	xorl	%r12d, %r12d
	movq	%rax, (%rdx)
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L474:
	movl	592(%r13), %edi
	cmpl	%edi, %eax
	jge	.L414
	movq	608(%r13), %r10
	movslq	%eax, %rdx
	movq	(%r10,%rdx,8), %rcx
	testl	%ebx, %ebx
	jne	.L415
	movq	%rcx, %r8
	andl	$96, %r8d
	je	.L477
.L417:
	testb	$64, %cl
	je	.L421
	sarl	$8, %ecx
	movl	592(%r13), %edx
	xorl	%eax, %eax
	andl	$1048575, %ecx
	cmpl	%edx, %ecx
	jge	.L422
	movslq	%ecx, %rcx
	movq	(%r10,%rcx,8), %rax
	sarl	$8, %eax
	andl	$1048575, %eax
.L422:
	cmpl	%eax, %edx
	jg	.L423
	movabsq	$9151314442816847872, %rcx
	movq	%rax, %rdx
	salq	$43, %rdx
	andq	%rcx, %rdx
	movabsq	$4629700417037541376, %rcx
	addq	%rdx, %rcx
	movq	%rax, %rdx
.L471:
	salq	$42, %rdx
	sall	$24, %eax
	movabsq	$35747322042253312, %rsi
	andq	%rsi, %rdx
	andl	$1056964608, %eax
	addq	%rcx, %rdx
	addq	%rdx, %rax
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L428:
	testl	%r8d, %r8d
	jne	.L427
	xorl	%ecx, %ecx
	movl	%edx, %eax
.L415:
	movl	%ecx, %edx
	sarl	$8, %edx
	andl	$1048575, %edx
	je	.L427
	cmpl	%edi, %edx
	jge	.L428
	movslq	%edx, %rsi
	movq	(%r10,%rsi,8), %rsi
	movl	%esi, %r9d
	andl	$3, %r9d
	cmpl	%r9d, %r8d
	jg	.L427
	movq	%rsi, %rcx
	movl	%edx, %eax
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L423:
	movslq	%eax, %rdx
	movq	(%r10,%rdx,8), %rcx
.L421:
	testb	$32, %cl
	je	.L478
	movabsq	$4629700417037541376, %rax
	sarl	$8, %ecx
	andl	$1048575, %ecx
	cmpl	592(%r13), %ecx
	jge	.L424
	movabsq	$9151314442816847872, %rsi
	movslq	%ecx, %rcx
	movq	(%r10,%rcx,8), %r12
	sarl	$8, %r12d
	movl	%r12d, %ecx
	sall	$24, %r12d
	andl	$1048575, %ecx
	andl	$1056964608, %r12d
	movq	%rcx, %rdx
	salq	$42, %rcx
	salq	$43, %rdx
	andq	%rsi, %rdx
	addq	%rax, %rdx
	movabsq	$35747322042253312, %rax
	andq	%rax, %rcx
	leaq	(%rdx,%rcx), %rax
	addq	%r12, %rax
.L424:
	leaq	(%r15,%rax), %r12
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L427:
	andl	$8, %ecx
	testq	%rcx, %rcx
	je	.L384
	movabsq	$9151314442816847872, %rdx
	movq	%rax, %r12
	movabsq	$35747322042253312, %rcx
	salq	$43, %r12
	andq	%rdx, %r12
	movabsq	$4629700417037541376, %rdx
	addq	%r12, %rdx
	movq	%rax, %r12
	sall	$24, %eax
	salq	$42, %r12
	andl	$1056964608, %eax
	andq	%rcx, %r12
	addq	%rdx, %r12
	addq	%r12, %rax
	leaq	(%rax,%r15), %r12
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L478:
	movabsq	$9151314442816847872, %rsi
	movq	%rdx, %rcx
	salq	$43, %rcx
	andq	%rsi, %rcx
	movabsq	$4629700417037541376, %rsi
	addq	%rsi, %rcx
	jmp	.L471
.L476:
	movl	592(%r13), %edx
	cmpl	%edx, %eax
	jge	.L406
	movq	608(%r13), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	sarl	$8, %eax
	andl	$1048575, %eax
	je	.L406
	cmpl	%edx, %eax
	jge	.L406
	movslq	%eax, %rsi
	movq	(%rcx,%rsi,8), %rdx
	testb	$8, %dl
	je	.L406
	andl	$3, %edx
	cmpl	$2, %edx
	jne	.L406
	movabsq	$9151314442816847872, %rcx
	movq	%rsi, %rdx
	sall	$24, %eax
	salq	$43, %rdx
	salq	$42, %rsi
	andl	$1056964608, %eax
	andq	%rcx, %rdx
	movabsq	$4629700417037541376, %rcx
	addq	%rcx, %rdx
	movabsq	$35747322042253312, %rcx
	andq	%rcx, %rsi
	addq	%rdx, %rsi
	leaq	512(%rsi,%rax), %r12
	jmp	.L384
.L475:
	movl	592(%r13), %edi
	cmpl	%edi, %eax
	jge	.L408
	movq	608(%r13), %r8
	cltq
	movq	(%r8,%rax,8), %rax
	.p2align 4,,10
	.p2align 3
.L409:
	sarl	$8, %eax
	andl	$1048575, %eax
	movl	%eax, %edx
	je	.L408
	cmpl	%edi, %eax
	jge	.L408
	movslq	%eax, %rcx
	movq	(%r8,%rcx,8), %rax
	movl	%eax, %esi
	movl	%eax, %r9d
	andl	$3, %esi
	je	.L408
	cmpl	$1, %esi
	jne	.L409
	testb	$8, %al
	je	.L408
	testb	$32, %al
	jne	.L410
	movabsq	$9151314442816847872, %rsi
	movq	%rcx, %rax
	salq	$42, %rcx
	salq	$43, %rax
	andq	%rsi, %rax
	movabsq	$4629700417037541376, %rsi
	addq	%rsi, %rax
	movabsq	$35747322042253312, %rsi
	andq	%rsi, %rcx
	addq	%rax, %rcx
	movl	%edx, %eax
	sall	$24, %eax
	andl	$1056964608, %eax
	leaq	256(%rcx,%rax), %r12
	jmp	.L384
.L410:
	movabsq	$4629700417037541632, %r12
	sarl	$8, %r9d
	andl	$1048575, %r9d
	cmpl	%edi, %r9d
	jge	.L384
	movabsq	$9151314442816847872, %rsi
	movslq	%r9d, %r9
	movq	(%r8,%r9,8), %rax
	sarl	$8, %eax
	movl	%eax, %ecx
	sall	$24, %eax
	andl	$1048575, %ecx
	andl	$1056964608, %eax
	movq	%rcx, %rdx
	salq	$42, %rcx
	salq	$43, %rdx
	andq	%rsi, %rdx
	movabsq	$4629700417037541376, %rsi
	addq	%rsi, %rdx
	movabsq	$35747322042253312, %rsi
	andq	%rsi, %rcx
	addq	%rcx, %rdx
	leaq	256(%rdx,%rax), %r12
	jmp	.L384
.L477:
	testb	%r9b, %r9b
	je	.L384
	movl	%ecx, %eax
	sarl	$8, %eax
	andl	$1048575, %eax
	je	.L418
	cmpl	%edi, %eax
	jge	.L436
	movslq	%eax, %rdx
	movq	(%r10,%rdx,8), %r8
	movq	%r8, %rdi
	andl	$96, %edi
.L419:
	movabsq	$9151314442816847872, %rsi
	movq	%rdx, %rcx
	movq	%rdx, %r12
	salq	$43, %rcx
	salq	$42, %r12
	andq	%rsi, %rcx
	movabsq	$4629700417037541376, %rsi
	addq	%rcx, %rsi
	movabsq	$35747322042253312, %rcx
	andq	%rcx, %r12
	leaq	(%rsi,%r12), %rcx
	movl	%eax, %r12d
	sall	$24, %r12d
	andl	$1056964608, %r12d
	addq	%rcx, %r12
	addq	%r15, %r12
	jmp	.L420
.L436:
	xorl	%edi, %edi
	movslq	%eax, %rdx
	jmp	.L419
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6716CollationBuilder23getSpecialResetPositionERKNS_13UnicodeStringERPKcR10UErrorCode.cold, @function
_ZN6icu_6716CollationBuilder23getSpecialResetPositionERKNS_13UnicodeStringERPKcR10UErrorCode.cold:
.LFSB3482:
.L387:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3482:
	.text
	.size	_ZN6icu_6716CollationBuilder23getSpecialResetPositionERKNS_13UnicodeStringERPKcR10UErrorCode, .-_ZN6icu_6716CollationBuilder23getSpecialResetPositionERKNS_13UnicodeStringERPKcR10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_6716CollationBuilder23getSpecialResetPositionERKNS_13UnicodeStringERPKcR10UErrorCode.cold, .-_ZN6icu_6716CollationBuilder23getSpecialResetPositionERKNS_13UnicodeStringERPKcR10UErrorCode.cold
.LCOLDE8:
	.text
.LHOTE8:
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"normalizing the reset position"
	.align 8
.LC10:
	.string	"reset position maps to too many collation elements (more than 31)"
	.align 8
.LC11:
	.string	"reset primary-before ignorable not possible"
	.align 8
.LC12:
	.string	"reset primary-before first non-ignorable not supported"
	.align 8
.LC13:
	.string	"reset primary-before [first trailing] not supported"
	.align 8
.LC14:
	.string	"reset secondary-before secondary ignorable not possible"
	.align 8
.LC15:
	.string	"reset tertiary-before completely ignorable not possible"
	.align 8
.LC16:
	.string	"inserting reset position for &[before n]"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder8addResetEiRKNS_13UnicodeStringERPKcR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder8addResetEiRKNS_13UnicodeStringERPKcR10UErrorCode, @function
_ZN6icu_6716CollationBuilder8addResetEiRKNS_13UnicodeStringERPKcR10UErrorCode:
.LFB3480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r11d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r11d, %r11d
	jg	.L479
	movzwl	8(%rdx), %eax
	movslq	%esi, %r13
	movq	%rdi, %r12
	movq	%rdx, %rsi
	movq	%rcx, %r14
	movq	%r8, %rbx
	testw	%ax, %ax
	js	.L482
	movswl	%ax, %edx
	sarl	$5, %edx
.L483:
	testl	%edx, %edx
	je	.L484
	testb	$2, %al
	je	.L485
	leaq	10(%rsi), %rax
.L486:
	cmpw	$-2, (%rax)
	je	.L557
.L484:
	movq	8(%r12), %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r8d
	movq	%rbx, %rcx
	movq	%rax, -128(%rbp)
	leaq	-128(%rbp), %r15
	movq	(%rdi), %rax
	movw	%r8w, -120(%rbp)
	movq	%r15, %rdx
	call	*24(%rax)
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L489
	leaq	.LC9(%rip), %rax
	movq	%rax, (%r14)
.L490:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L479:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L558
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	.cfi_restore_state
	movl	12(%rdx), %edx
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L485:
	movq	24(%rsi), %rax
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L489:
	movq	72(%r12), %rdi
	leaq	296(%r12), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	call	_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringEPli@PLT
	movl	%eax, 544(%r12)
	cmpl	$31, %eax
	jle	.L491
	leaq	.LC10(%rip), %rax
	movl	$1, (%rbx)
	movq	%rax, (%r14)
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L491:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	$15, %r13d
	je	.L479
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L479
.L522:
	movq	%r14, %rdx
	movq	%rbx, %rcx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6716CollationBuilder22findOrInsertNodeForCEsEiRPKcR10UErrorCode.part.0
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L479
	xorl	%edx, %edx
	xorl	%esi, %esi
	testl	%eax, %eax
	jns	.L559
.L493:
	cmpl	%edx, %r13d
	jge	.L494
	movl	592(%r12), %ecx
	.p2align 4,,10
	.p2align 3
.L495:
	sarq	$28, %rsi
	movl	%esi, %eax
	andl	$1048575, %eax
	cmpl	%ecx, %eax
	jge	.L497
	movq	608(%r12), %rsi
	movslq	%eax, %rdx
	movq	(%rsi,%rdx,8), %rsi
	movl	%esi, %edx
	andl	$3, %edx
	cmpl	%edx, %r13d
	jl	.L495
.L494:
	cmpl	%edx, %r13d
	je	.L560
.L499:
	testl	%r13d, %r13d
	jne	.L502
	sarq	$32, %rsi
	testl	%esi, %esi
	je	.L523
	movq	48(%r12), %rax
	movl	8(%rax), %edx
	cmpl	(%rax,%rdx,4), %esi
	jbe	.L561
	cmpl	$-16645632, %esi
	je	.L562
	movq	40(%r12), %rdx
	movl	%esi, %eax
	leaq	48(%r12), %rdi
	shrl	$24, %eax
	movq	72(%rdx), %rdx
	movsbl	(%rdx,%rax), %edx
	call	_ZNK6icu_6721CollationRootElements16getPrimaryBeforeEja@PLT
	movl	(%rbx), %r10d
	xorl	%r9d, %r9d
	testl	%r10d, %r10d
	jg	.L509
	movq	%rbx, %rdx
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6716CollationBuilder26findOrInsertNodeForPrimaryEjR10UErrorCode.part.0
	movl	(%rbx), %r10d
	movl	%eax, %r9d
	.p2align 4,,10
	.p2align 3
.L509:
	testl	%r9d, %r9d
	js	.L508
	cmpl	592(%r12), %r9d
	jl	.L563
.L508:
	testl	%r10d, %r10d
	jle	.L503
	leaq	.LC16(%rip), %rax
	movq	%rax, (%r14)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L557:
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6716CollationBuilder23getSpecialResetPositionERKNS_13UnicodeStringERPKcR10UErrorCode
	movl	(%rbx), %r10d
	movl	$1, 544(%r12)
	movq	%rax, 296(%r12)
	testl	%r10d, %r10d
	jg	.L479
	cmpl	$15, %r13d
	jne	.L522
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L497:
	testl	%r13d, %r13d
	jns	.L496
	xorl	%esi, %esi
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L563:
	movq	608(%r12), %rdx
	movslq	%r9d, %rax
	movq	(%rdx,%rax,8), %rax
	sarl	$8, %eax
	andl	$1048575, %eax
	je	.L528
	movl	%eax, %r9d
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L559:
	cmpl	592(%r12), %eax
	jge	.L493
	movq	608(%r12), %rcx
	movslq	%eax, %rdx
	movq	(%rcx,%rdx,8), %rsi
	movl	%esi, %edx
	andl	$3, %edx
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L496:
	je	.L523
.L502:
	movl	$1, %edx
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6716CollationBuilder14findCommonNodeEii
	movl	%eax, %r15d
	cmpl	$1, %r13d
	je	.L510
	movl	$2, %edx
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6716CollationBuilder14findCommonNodeEii
	movl	%eax, %r15d
.L510:
	testl	%r15d, %r15d
	js	.L530
	cmpl	592(%r12), %r15d
	jge	.L530
	movq	608(%r12), %rdx
	movslq	%r15d, %rax
	movq	(%rdx,%rax,8), %rdx
	movl	%edx, %eax
	andl	$3, %eax
	cmpl	%eax, %r13d
	jne	.L511
	movq	%rdx, %rax
	shrq	$48, %rax
	je	.L564
	movl	%r13d, %ecx
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	%rdx, -136(%rbp)
	call	_ZN6icu_6716CollationBuilder17getWeight16BeforeEili
	movq	-136(%rbp), %rdx
	movl	%eax, %ecx
	sarq	$28, %rdx
	movl	%edx, %r9d
	movl	592(%r12), %edx
	andl	$1048575, %r9d
	movl	%r9d, %eax
	cmpl	%edx, %eax
	jge	.L532
.L565:
	movq	608(%r12), %rsi
	cltq
	movq	(%rsi,%rax,8), %rax
	movl	%eax, %esi
	andl	$3, %esi
	cmpl	%esi, %r13d
	jg	.L532
	jne	.L556
	testb	$8, %al
	je	.L517
.L556:
	sarq	$28, %rax
	andl	$1048575, %eax
	cmpl	%edx, %eax
	jl	.L565
.L532:
	movl	$1280, %eax
.L514:
	movl	(%rbx), %r10d
	cmpl	%eax, %ecx
	je	.L519
	testl	%r10d, %r10d
	jle	.L566
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L519:
	movl	544(%r12), %eax
	subl	$1, %eax
	cltq
	movq	296(%r12,%rax,8), %rax
	movl	%eax, %edx
	shrl	$24, %edx
	subl	$6, %edx
	cmpl	$63, %edx
	ja	.L521
	movl	%eax, %r13d
	sarl	$8, %r13d
	andl	$3, %r13d
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L560:
	movq	%rsi, %r9
	sarq	$28, %r9
	andl	$1048575, %r9d
	testb	$8, %sil
	je	.L499
.L503:
	movl	544(%r12), %eax
	sall	$8, %r13d
	movabsq	$9151314442816847872, %rdx
	movabsq	$35747322042253312, %rsi
	leal	-1(%rax), %ecx
	movq	%r9, %rax
	salq	$43, %rax
	movslq	%ecx, %rcx
	andq	%rdx, %rax
	movabsq	$4629700417037541376, %rdx
	addq	%rax, %rdx
	movq	%r9, %rax
	sall	$24, %r9d
	salq	$42, %rax
	andl	$1056964608, %r9d
	andq	%rsi, %rax
	addq	%rdx, %rax
	addq	%r9, %rax
	movslq	%r13d, %r9
	addq	%r9, %rax
	movq	%rax, 296(%r12,%rcx,8)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L530:
	xorl	%edx, %edx
.L511:
	movl	%r13d, %ecx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6716CollationBuilder17getWeight16BeforeEili
	movl	(%rbx), %r10d
	xorl	%r9d, %r9d
	movl	%eax, %edx
	testl	%r10d, %r10d
	jg	.L519
	cmpl	$1280, %eax
	je	.L567
	movq	%rbx, %r8
	movl	%r13d, %ecx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6716CollationBuilder20findOrInsertWeakNodeEijiR10UErrorCode.part.0
	movl	(%rbx), %r10d
	movl	%eax, %r9d
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L523:
	leaq	.LC11(%rip), %rax
	movl	$16, (%rbx)
	movq	%rax, (%r14)
	jmp	.L479
.L561:
	leaq	.LC12(%rip), %rax
	movl	$16, (%rbx)
	movq	%rax, (%r14)
	jmp	.L479
.L521:
	movabsq	$-72057594037927936, %rdx
	xorl	%r13d, %r13d
	testq	%rdx, %rax
	jne	.L508
	movl	$1, %r13d
	testl	$-16777216, %eax
	jne	.L508
	cmpq	$1, %rax
	sbbl	%r13d, %r13d
	andl	$13, %r13d
	addl	$2, %r13d
	jmp	.L508
.L562:
	leaq	.LC13(%rip), %rax
	movl	$16, (%rbx)
	movq	%rax, (%r14)
	jmp	.L479
.L564:
	cmpl	$1, %r13d
	leaq	.LC14(%rip), %rax
	leaq	.LC15(%rip), %rdx
	movl	$16, (%rbx)
	cmovne	%rdx, %rax
	movq	%rax, (%r14)
	jmp	.L479
.L566:
	salq	$48, %rcx
	movl	%r9d, %esi
	movq	%rbx, %r8
	movl	%r15d, %edx
	orq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_6716CollationBuilder17insertNodeBetweenEiilR10UErrorCode.part.0
	movl	(%rbx), %r10d
	movl	%eax, %r9d
	jmp	.L519
.L517:
	shrq	$48, %rax
	jmp	.L514
.L567:
	movl	%r13d, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6716CollationBuilder14findCommonNodeEii
	movl	%eax, %r9d
	jmp	.L519
.L558:
	call	__stack_chk_fail@PLT
.L528:
	xorl	%r13d, %r13d
	jmp	.L508
	.cfi_endproc
.LFE3480:
	.size	_ZN6icu_6716CollationBuilder8addResetEiRKNS_13UnicodeStringERPKcR10UErrorCode, .-_ZN6icu_6716CollationBuilder8addResetEiRKNS_13UnicodeStringERPKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder11setCaseBitsERKNS_13UnicodeStringERPKcR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder11setCaseBitsERKNS_13UnicodeStringERPKcR10UErrorCode, @function
_ZN6icu_6716CollationBuilder11setCaseBitsERKNS_13UnicodeStringERPKcR10UErrorCode:
.LFB3492:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L568
	jmp	_ZN6icu_6716CollationBuilder11setCaseBitsERKNS_13UnicodeStringERPKcR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L568:
	ret
	.cfi_endproc
.LFE3492:
	.size	_ZN6icu_6716CollationBuilder11setCaseBitsERKNS_13UnicodeStringERPKcR10UErrorCode, .-_ZN6icu_6716CollationBuilder11setCaseBitsERKNS_13UnicodeStringERPKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716CollationBuilder24mergeCompositeIntoStringERKNS_13UnicodeStringEiiS3_RS1_S4_R10UErrorCode
	.type	_ZNK6icu_6716CollationBuilder24mergeCompositeIntoStringERKNS_13UnicodeStringEiiS3_RS1_S4_R10UErrorCode, @function
_ZNK6icu_6716CollationBuilder24mergeCompositeIntoStringERKNS_13UnicodeStringEiiS3_RS1_S4_R10UErrorCode:
.LFB3498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	24(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L573
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6716CollationBuilder24mergeCompositeIntoStringERKNS_13UnicodeStringEiiS3_RS1_S4_R10UErrorCode.part.0
	.cfi_endproc
.LFE3498:
	.size	_ZNK6icu_6716CollationBuilder24mergeCompositeIntoStringERKNS_13UnicodeStringEiiS3_RS1_S4_R10UErrorCode, .-_ZNK6icu_6716CollationBuilder24mergeCompositeIntoStringERKNS_13UnicodeStringEiiS3_RS1_S4_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716CollationBuilder12ignorePrefixERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6716CollationBuilder12ignorePrefixERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6716CollationBuilder12ignorePrefixERKNS_13UnicodeStringER10UErrorCode:
.LFB3499:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L582
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L582:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rdi
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*88(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	sete	%al
	ret
	.cfi_endproc
.LFE3499:
	.size	_ZNK6icu_6716CollationBuilder12ignorePrefixERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6716CollationBuilder12ignorePrefixERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716CollationBuilder12ignoreStringERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6716CollationBuilder12ignoreStringERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6716CollationBuilder12ignoreStringERKNS_13UnicodeStringER10UErrorCode:
.LFB3500:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L584
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L584:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	movq	(%rdi), %rax
	call	*88(%rax)
	testb	%al, %al
	je	.L586
	movzwl	8(%rbx), %edx
	testw	%dx, %dx
	js	.L587
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L588:
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L583
	andl	$2, %edx
	leaq	10(%rbx), %rsi
	jne	.L590
	movq	24(%rbx), %rsi
.L590:
	movzwl	(%rsi), %eax
	subl	$44032, %eax
	cmpl	$11171, %eax
	setbe	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	.cfi_restore_state
	movl	$1, %eax
.L583:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	movl	12(%rbx), %ecx
	jmp	.L588
	.cfi_endproc
.LFE3500:
	.size	_ZNK6icu_6716CollationBuilder12ignoreStringERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6716CollationBuilder12ignoreStringERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716CollationBuilder5isFCDERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6716CollationBuilder5isFCDERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6716CollationBuilder5isFCDERKNS_13UnicodeStringER10UErrorCode:
.LFB3501:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L607
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rdi
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*88(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	setne	%al
	ret
	.cfi_endproc
.LFE3501:
	.size	_ZNK6icu_6716CollationBuilder5isFCDERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6716CollationBuilder5isFCDERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder14addIfDifferentERKNS_13UnicodeStringES3_PKlijR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder14addIfDifferentERKNS_13UnicodeStringES3_PKlijR10UErrorCode, @function
_ZN6icu_6716CollationBuilder14addIfDifferentERKNS_13UnicodeStringES3_PKlijR10UErrorCode:
.LFB3503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %r11
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r11), %eax
	testl	%eax, %eax
	jle	.L617
.L608:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L618
	addq	$296, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L617:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	72(%rdi), %rdi
	movl	%r8d, %r14d
	movq	%rcx, %r13
	xorl	%r8d, %r8d
	leaq	-304(%rbp), %rcx
	movq	%rsi, %r15
	movq	%r11, -320(%rbp)
	movq	%rcx, -328(%rbp)
	movq	%rdx, -312(%rbp)
	call	_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringES3_Pli@PLT
	movq	-312(%rbp), %r10
	movq	-320(%rbp), %r11
	cmpl	%r14d, %eax
	jne	.L611
	testl	%r14d, %r14d
	jle	.L608
	movq	-328(%rbp), %rcx
	leal	-1(%r14), %esi
	xorl	%edx, %edx
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L619:
	leaq	1(%rdx), %rax
	cmpq	%rdx, %rsi
	je	.L608
	movq	%rax, %rdx
.L613:
	movq	(%rcx,%rdx,8), %rax
	cmpq	%rax, 0(%r13,%rdx,8)
	je	.L619
.L611:
	movq	72(%rbx), %rdi
	cmpl	$-1, %r12d
	je	.L620
.L614:
	movq	%r11, %r8
	movl	%r12d, %ecx
	movq	%r10, %rdx
	movq	%r15, %rsi
	call	_ZN6icu_6720CollationDataBuilder7addCE32ERKNS_13UnicodeStringES3_jR10UErrorCode@PLT
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L620:
	movq	(%rdi), %rax
	movq	%r10, -320(%rbp)
	movq	%r11, %rcx
	movl	%r14d, %edx
	movq	%r11, -312(%rbp)
	movq	%r13, %rsi
	call	*32(%rax)
	movq	72(%rbx), %rdi
	movq	-320(%rbp), %r10
	movq	-312(%rbp), %r11
	movl	%eax, %r12d
	jmp	.L614
.L618:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3503:
	.size	_ZN6icu_6716CollationBuilder14addIfDifferentERKNS_13UnicodeStringES3_PKlijR10UErrorCode, .-_ZN6icu_6716CollationBuilder14addIfDifferentERKNS_13UnicodeStringES3_PKlijR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder7sameCEsEPKliS2_i
	.type	_ZN6icu_6716CollationBuilder7sameCEsEPKliS2_i, @function
_ZN6icu_6716CollationBuilder7sameCEsEPKliS2_i:
.LFB3504:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	%ecx, %esi
	jne	.L621
	testl	%esi, %esi
	jle	.L625
	subl	$1, %esi
	xorl	%eax, %eax
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L628:
	leaq	1(%rax), %rcx
	cmpq	%rsi, %rax
	je	.L625
	movq	%rcx, %rax
.L623:
	movq	(%rdx,%rax,8), %rcx
	cmpq	%rcx, (%rdi,%rax,8)
	je	.L628
	xorl	%eax, %eax
.L621:
	ret
	.p2align 4,,10
	.p2align 3
.L625:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3504:
	.size	_ZN6icu_6716CollationBuilder7sameCEsEPKliS2_i, .-_ZN6icu_6716CollationBuilder7sameCEsEPKliS2_i
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"quaternary tailoring gap too small"
	.align 8
.LC18:
	.string	"tertiary tailoring gap too small"
	.align 8
.LC19:
	.string	"secondary tailoring gap too small"
	.align 8
.LC20:
	.string	"primary tailoring gap too small"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder15makeTailoredCEsER10UErrorCode
	.type	_ZN6icu_6716CollationBuilder15makeTailoredCEsER10UErrorCode, @function
_ZN6icu_6716CollationBuilder15makeTailoredCEsER10UErrorCode:
.LFB3505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$616, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -592(%rbp)
	movl	(%rsi), %eax
	movq	%rsi, -656(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	testl	%eax, %eax
	jle	.L716
.L629:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L717
	addq	$616, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L716:
	.cfi_restore_state
	leaq	-576(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6716CollationWeightsC1Ev@PLT
	leaq	-400(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -632(%rbp)
	call	_ZN6icu_6716CollationWeightsC1Ev@PLT
	leaq	-224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN6icu_6716CollationWeightsC1Ev@PLT
	movq	-592(%rbp), %rsi
	movl	560(%rsi), %eax
	movq	608(%rsi), %r15
	testl	%eax, %eax
	jle	.L629
	movq	$0, -648(%rbp)
	leaq	48(%rsi), %rdi
	movq	%rdi, -624(%rbp)
.L666:
	movq	%r15, %rdx
	testl	%eax, %eax
	jle	.L631
	movq	-648(%rbp), %rdi
	subl	%edi, %eax
	testl	%eax, %eax
	jle	.L631
	movq	-592(%rbp), %rax
	movq	576(%rax), %rax
	movslq	(%rax,%rdi,4), %rax
	leaq	(%r15,%rax,8), %rdx
.L631:
	movq	(%rdx), %rbx
	movq	%rbx, %rsi
	sarq	$32, %rsi
	movl	%esi, %r9d
	testl	%esi, %esi
	je	.L718
	movq	-624(%rbp), %rdi
	movl	%esi, -580(%rbp)
	call	_ZNK6icu_6721CollationRootElements11findPrimaryEj@PLT
	movl	-580(%rbp), %r9d
	movl	$1280, %r8d
	movl	%eax, -612(%rbp)
.L667:
	sarl	$8, %ebx
	andl	$1048575, %ebx
	je	.L633
	movb	$0, -580(%rbp)
	movl	%r8d, %r10d
	xorl	%esi, %esi
	xorl	%r11d, %r11d
	xorl	%edx, %edx
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L635:
	cmpl	$2, %ecx
	je	.L719
	cmpl	$1, %ecx
	je	.L720
	cmpb	$0, -580(%rbp)
	je	.L721
.L662:
	movq	%r14, %rdi
	andl	$8, %r12d
	call	_ZN6icu_6716CollationWeights10nextWeightEv@PLT
	movb	$1, -580(%rbp)
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%eax, %r9d
	xorl	%r11d, %r11d
	movl	$1280, %r8d
	movl	$1280, %r10d
.L638:
	testq	%r12, %r12
	je	.L649
.L660:
	movl	%r10d, %eax
	movl	%edx, %ecx
	movl	%r8d, %edi
	sall	$16, %eax
	sall	$6, %ecx
.L648:
	orq	%rdi, %rax
	orq	%rcx, %rax
	movq	%r9, %rcx
	salq	$32, %rcx
	orq	%rcx, %rax
	movq	%rax, 0(%r13)
.L649:
	testl	%ebx, %ebx
	je	.L633
.L634:
	movslq	%ebx, %rbx
	leaq	(%r15,%rbx,8), %r13
	movq	0(%r13), %r12
	movl	%r12d, %ebx
	movl	%r12d, %ecx
	sarl	$8, %ebx
	andl	$3, %ecx
	andl	$1048575, %ebx
	cmpl	$3, %ecx
	jne	.L635
	cmpl	$3, %edx
	je	.L722
	addl	$1, %edx
	andl	$8, %r12d
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L719:
	testb	$8, %r12b
	je	.L640
	testb	%sil, %sil
	je	.L723
.L641:
	movq	-608(%rbp), %rdi
	movb	%r11b, -600(%rbp)
	movl	%r10d, -596(%rbp)
	movl	%r9d, -584(%rbp)
	call	_ZN6icu_6716CollationWeights10nextWeightEv@PLT
	movl	-596(%rbp), %r10d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%eax, %edi
	movl	-584(%rbp), %r9d
	movl	$1, %esi
	movzbl	-600(%rbp), %r11d
	movl	%r10d, %eax
	movq	%rdi, %r8
	sall	$16, %eax
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L720:
	testb	$8, %r12b
	je	.L651
	testb	%r11b, %r11b
	je	.L724
.L652:
	movq	-632(%rbp), %rdi
	movl	%r9d, -584(%rbp)
	call	_ZN6icu_6716CollationWeights10nextWeightEv@PLT
	movl	-584(%rbp), %r9d
	testl	%eax, %eax
	movl	%eax, %r10d
	jne	.L725
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
	xorl	%esi, %esi
	movl	$1, %r11d
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L721:
	testl	%ebx, %ebx
	je	.L675
	movl	%ebx, %eax
	xorl	%edx, %edx
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L664:
	sarl	$8, %eax
	andl	$1048575, %eax
	je	.L663
.L665:
	cltq
	leal	1(%rdx), %r8d
	movq	(%r15,%rax,8), %rax
	testb	$3, %al
	jne	.L664
	testb	$8, %al
	je	.L663
	leal	2(%rdx), %ecx
	sarl	$8, %eax
	movl	%r8d, %edx
	movl	%ecx, %r8d
	andl	$1048575, %eax
	jne	.L665
.L663:
	movq	-592(%rbp), %rax
	movq	-624(%rbp), %rdi
	movl	%r9d, %esi
	movl	%r8d, -600(%rbp)
	movl	%r9d, -584(%rbp)
	movq	40(%rax), %rdx
	movl	%r9d, %eax
	shrl	$24, %eax
	movq	72(%rdx), %rdx
	movsbl	(%rdx,%rax), %r10d
	movl	-612(%rbp), %edx
	movl	%r10d, %ecx
	movl	%r10d, -596(%rbp)
	call	_ZNK6icu_6721CollationRootElements15getPrimaryAfterEjia@PLT
	movl	-596(%rbp), %r10d
	movq	%r14, %rdi
	movl	%eax, -580(%rbp)
	movl	%r10d, %esi
	call	_ZN6icu_6716CollationWeights14initForPrimaryEa@PLT
	movl	-600(%rbp), %r8d
	movl	-584(%rbp), %r9d
	movq	%r14, %rdi
	movl	-580(%rbp), %edx
	movl	%r8d, %ecx
	movl	%r9d, %esi
	call	_ZN6icu_6716CollationWeights12allocWeightsEjji@PLT
	testb	%al, %al
	jne	.L662
	movq	-656(%rbp), %rax
	leaq	.LC20(%rip), %rdi
	movl	$15, (%rax)
	movq	-592(%rbp), %rax
	movq	%rdi, 288(%rax)
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L640:
	shrq	$48, %r12
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r12, %r8
	testl	%ebx, %ebx
	jne	.L634
.L633:
	movq	-592(%rbp), %rax
	addq	$1, -648(%rbp)
	movq	-648(%rbp), %rdi
	movl	560(%rax), %eax
	cmpl	%edi, %eax
	jg	.L666
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L651:
	shrq	$48, %r12
	xorl	%esi, %esi
	xorl	%r11d, %r11d
	xorl	%edx, %edx
	cmpl	$1, %r12d
	movq	%r12, %r10
	sbbl	%r8d, %r8d
	notl	%r8d
	andl	$1280, %r8d
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L723:
	testl	%ebx, %ebx
	je	.L671
	movl	%ebx, %eax
	xorl	%edx, %edx
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L643:
	sarl	$8, %eax
	andl	$1048575, %eax
	je	.L642
.L644:
	cltq
	leal	1(%rdx), %r12d
	movq	(%r15,%rax,8), %rax
	movl	%eax, %ecx
	andl	$3, %ecx
	testb	$2, %al
	je	.L642
	cmpl	$2, %ecx
	jne	.L643
	testb	$8, %al
	je	.L642
	leal	2(%rdx), %ecx
	sarl	$8, %eax
	movl	%r12d, %edx
	movl	%ecx, %r12d
	andl	$1048575, %eax
	jne	.L644
	.p2align 4,,10
	.p2align 3
.L642:
	testl	%r8d, %r8d
	jne	.L645
	movq	-592(%rbp), %rax
	movq	48(%rax), %rax
	movl	16(%rax), %r8d
	movl	(%rax), %edx
	sall	$8, %r8d
	movl	(%rax,%rdx,4), %edx
	movzwl	%r8w, %r8d
	subl	$256, %r8d
	andl	$16191, %edx
.L646:
	movq	-608(%rbp), %rdi
	movb	%r11b, -633(%rbp)
	movl	%r10d, -600(%rbp)
	movl	%r9d, -596(%rbp)
	movl	%r8d, -616(%rbp)
	movl	%edx, -584(%rbp)
	call	_ZN6icu_6716CollationWeights15initForTertiaryEv@PLT
	movl	-616(%rbp), %r8d
	movl	-584(%rbp), %edx
	movl	%r12d, %ecx
	movq	-608(%rbp), %rdi
	movl	%r8d, %esi
	call	_ZN6icu_6716CollationWeights12allocWeightsEjji@PLT
	movl	-596(%rbp), %r9d
	movl	-600(%rbp), %r10d
	testb	%al, %al
	movzbl	-633(%rbp), %r11d
	jne	.L641
	movq	-656(%rbp), %rax
	leaq	.LC18(%rip), %rdi
	movl	$15, (%rax)
	movq	-592(%rbp), %rax
	movq	%rdi, 288(%rax)
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L724:
	testl	%ebx, %ebx
	je	.L653
	movl	%ebx, %eax
	xorl	%edx, %edx
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L654:
	sarl	$8, %eax
	andl	$1048575, %eax
	je	.L653
.L655:
	cltq
	leal	1(%rdx), %ecx
	movq	(%r15,%rax,8), %rax
	movl	%eax, %esi
	andl	$3, %esi
	je	.L653
	cmpl	$1, %esi
	jne	.L654
	testb	$8, %al
	je	.L653
	leal	2(%rdx), %esi
	sarl	$8, %eax
	movl	%ecx, %edx
	movl	%esi, %ecx
	andl	$1048575, %eax
	jne	.L655
	.p2align 4,,10
	.p2align 3
.L653:
	testl	%r10d, %r10d
	jne	.L656
	movq	-592(%rbp), %rax
	movq	48(%rax), %rax
	movl	16(%rax), %r10d
	movl	4(%rax), %edx
	shrl	$8, %r10d
	movzwl	2(%rax,%rdx,4), %r12d
	andl	$65280, %r10d
	subl	$256, %r10d
.L657:
	cmpl	$1280, %r10d
	jne	.L659
	movq	-592(%rbp), %rax
	movq	48(%rax), %rax
	movzwl	18(%rax), %r10d
	andl	$65280, %r10d
.L659:
	movq	-632(%rbp), %rdi
	movl	%r9d, -596(%rbp)
	movl	%r10d, -600(%rbp)
	movl	%ecx, -584(%rbp)
	call	_ZN6icu_6716CollationWeights16initForSecondaryEv@PLT
	movl	-600(%rbp), %r10d
	movl	-584(%rbp), %ecx
	movl	%r12d, %edx
	movq	-632(%rbp), %rdi
	movl	%r10d, %esi
	call	_ZN6icu_6716CollationWeights12allocWeightsEjji@PLT
	movl	-596(%rbp), %r9d
	testb	%al, %al
	jne	.L652
	movq	-656(%rbp), %rax
	leaq	.LC19(%rip), %rdi
	movl	$15, (%rax)
	movq	-592(%rbp), %rax
	movq	%rdi, 288(%rax)
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L645:
	movzbl	-580(%rbp), %eax
	orb	%r11b, %al
	je	.L726
	movl	$1280, %edx
	cmpl	$256, %r8d
	je	.L646
	movq	-592(%rbp), %rax
	movq	48(%rax), %rax
	movl	16(%rax), %edx
	sall	$8, %edx
	movzwl	%dx, %edx
	jmp	.L646
.L718:
	movl	$0, -612(%rbp)
	xorl	%r8d, %r8d
	jmp	.L667
.L656:
	cmpb	$0, -580(%rbp)
	je	.L727
	movl	$1280, %r12d
	cmpl	$256, %r10d
	je	.L659
	movq	-592(%rbp), %rax
	movq	48(%rax), %rax
	movl	16(%rax), %r12d
	shrl	$8, %r12d
	andl	$65280, %r12d
	jmp	.L657
.L725:
	xorl	%esi, %esi
	movl	$1, %r11d
	xorl	%edx, %edx
	movl	$1280, %r8d
	jmp	.L660
.L726:
	movl	-612(%rbp), %esi
	movq	-624(%rbp), %rdi
	movl	%r10d, %edx
	movl	%r8d, %ecx
	movb	%r11b, -616(%rbp)
	movl	%r9d, -600(%rbp)
	movl	%r8d, -596(%rbp)
	movl	%r10d, -584(%rbp)
	call	_ZNK6icu_6721CollationRootElements16getTertiaryAfterEijj@PLT
	movl	-584(%rbp), %r10d
	movl	-596(%rbp), %r8d
	movl	-600(%rbp), %r9d
	movzbl	-616(%rbp), %r11d
	movl	%eax, %edx
	jmp	.L646
.L722:
	movq	-656(%rbp), %rax
	leaq	.LC17(%rip), %rdi
	movl	$15, (%rax)
	movq	-592(%rbp), %rax
	movq	%rdi, 288(%rax)
	jmp	.L629
.L727:
	movl	-612(%rbp), %esi
	movl	%r10d, %edx
	movq	-624(%rbp), %rdi
	movl	%r9d, -600(%rbp)
	movl	%ecx, -596(%rbp)
	movl	%r10d, -584(%rbp)
	call	_ZNK6icu_6721CollationRootElements17getSecondaryAfterEij@PLT
	movl	-584(%rbp), %r10d
	movl	-596(%rbp), %ecx
	movl	-600(%rbp), %r9d
	movl	%eax, %r12d
	jmp	.L657
.L675:
	movl	$1, %r8d
	jmp	.L663
.L671:
	movl	$1, %r12d
	jmp	.L642
.L717:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3505:
	.size	_ZN6icu_6716CollationBuilder15makeTailoredCEsER10UErrorCode, .-_ZN6icu_6716CollationBuilder15makeTailoredCEsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder18countTailoredNodesEPKlii
	.type	_ZN6icu_6716CollationBuilder18countTailoredNodesEPKlii, @function
_ZN6icu_6716CollationBuilder18countTailoredNodesEPKlii:
.LFB3506:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	testl	%esi, %esi
	jne	.L731
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L730:
	sarl	$8, %esi
	andl	$1048575, %esi
	je	.L728
.L731:
	movslq	%esi, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movl	%esi, %eax
	andl	$3, %eax
	cmpl	%eax, %edx
	jg	.L728
	jne	.L730
	testb	$8, %sil
	je	.L728
	sarl	$8, %esi
	addl	$1, %r8d
	andl	$1048575, %esi
	jne	.L731
.L728:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3506:
	.size	_ZN6icu_6716CollationBuilder18countTailoredNodesEPKlii, .-_ZN6icu_6716CollationBuilder18countTailoredNodesEPKlii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder11finalizeCEsER10UErrorCode
	.type	_ZN6icu_6716CollationBuilder11finalizeCEsER10UErrorCode, @function
_ZN6icu_6716CollationBuilder11finalizeCEsER10UErrorCode:
.LFB3519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L756
.L737:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L757
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L756:
	.cfi_restore_state
	movq	%rdi, %r13
	movl	$632, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L740
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720CollationDataBuilderC1ER10UErrorCode@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L758
.L741:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L737
.L740:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L737
	movl	$7, (%rbx)
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L758:
	movq	40(%r13), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	leaq	-80(%rbp), %r15
	leaq	16+_ZTVN6icu_6711CEFinalizerE(%rip), %r14
	call	_ZN6icu_6720CollationDataBuilder16initForTailoringEPKNS_13CollationDataER10UErrorCode@PLT
	movq	72(%r13), %rsi
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	608(%r13), %rax
	movq	%r12, %rdi
	movq	%r14, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN6icu_6720CollationDataBuilder8copyFromERKS0_RKNS0_10CEModifierER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L759
	movq	72(%r13), %rdi
	testq	%rdi, %rdi
	je	.L744
	movq	(%rdi), %rax
	call	*8(%rax)
.L744:
	movq	%r12, 72(%r13)
	movq	%r15, %rdi
	movq	%r14, -80(%rbp)
	call	_ZN6icu_6720CollationDataBuilder10CEModifierD2Ev@PLT
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L759:
	movq	%r15, %rdi
	movq	%r14, -80(%rbp)
	call	_ZN6icu_6720CollationDataBuilder10CEModifierD2Ev@PLT
	jmp	.L741
.L757:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3519:
	.size	_ZN6icu_6716CollationBuilder11finalizeCEsER10UErrorCode, .-_ZN6icu_6716CollationBuilder11finalizeCEsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder10ceStrengthEl
	.type	_ZN6icu_6716CollationBuilder10ceStrengthEl, @function
_ZN6icu_6716CollationBuilder10ceStrengthEl:
.LFB3520:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	shrl	$24, %eax
	subl	$6, %eax
	cmpl	$63, %eax
	ja	.L761
	sarl	$8, %edi
	movl	%edi, %eax
	andl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L761:
	movabsq	$-72057594037927936, %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdi
	jne	.L760
	movl	$1, %eax
	testl	$-16777216, %edi
	jne	.L760
	cmpq	$1, %rdi
	sbbl	%eax, %eax
	andl	$13, %eax
	addl	$2, %eax
.L760:
	ret
	.cfi_endproc
.LFE3520:
	.size	_ZN6icu_6716CollationBuilder10ceStrengthEl, .-_ZN6icu_6716CollationBuilder10ceStrengthEl
	.p2align 4
	.globl	ucol_getUnsafeSet_67
	.type	ucol_getUnsafeSet_67, @function
ucol_getUnsafeSet_67:
.LFB3522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rsi, %rdi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1080, %rsp
	movq	%rdx, -1104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uset_clear_67@PLT
	movq	%rbx, %r8
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	leaq	_ZZ20ucol_getUnsafeSet_67E10cccpattern(%rip), %rsi
	call	uset_applyPattern_67@PLT
	movl	$57343, %edx
	movl	$55296, %esi
	movq	%r12, %rdi
	call	uset_addRange_67@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uset_open_67@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, %r13
	movq	%rbx, %r8
	movq	%rax, %rsi
	call	ucol_getContractionsAndExpansions_67@PLT
	movq	%r13, %rdi
	call	uset_size_67@PLT
	testl	%eax, %eax
	jle	.L767
	movl	%eax, %ebx
	leaq	-1088(%rbp), %rax
	xorl	%r14d, %r14d
	movq	%rax, -1112(%rbp)
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L768:
	addl	$1, %r14d
	cmpl	%r14d, %ebx
	je	.L767
.L771:
	subq	$8, %rsp
	pushq	-1104(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-1112(%rbp), %r8
	movl	$512, %r9d
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	uset_getItem_67@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jle	.L768
	xorl	%ecx, %ecx
.L770:
	movslq	%ecx, %rsi
	leal	1(%rcx), %r15d
	movzwl	-1088(%rbp,%rsi,2), %esi
	movl	%esi, %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	je	.L775
.L769:
	cmpl	%eax, %r15d
	jge	.L768
	movq	%r12, %rdi
	movl	%eax, -1092(%rbp)
	call	uset_add_67@PLT
	movl	-1092(%rbp), %eax
	movl	%r15d, %ecx
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L775:
	cmpl	%r15d, %eax
	je	.L768
	movslq	%r15d, %rdi
	movzwl	-1088(%rbp,%rdi,2), %edi
	movl	%edi, %r8d
	andl	$-1024, %r8d
	cmpl	$56320, %r8d
	jne	.L769
	sall	$10, %esi
	leal	2(%rcx), %r15d
	leal	-56613888(%rdi,%rsi), %esi
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L767:
	movq	%r13, %rdi
	call	uset_close_67@PLT
	movq	%r12, %rdi
	call	uset_size_67@PLT
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L776
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L776:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3522:
	.size	ucol_getUnsafeSet_67, .-ucol_getUnsafeSet_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilderD2Ev
	.type	_ZN6icu_6716CollationBuilderD2Ev, @function
_ZN6icu_6716CollationBuilderD2Ev:
.LFB3476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6716CollationBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L778
	movq	(%rdi), %rax
	call	*8(%rax)
.L778:
	leaq	584(%r12), %rdi
	call	_ZN6icu_679UVector64D1Ev@PLT
	leaq	552(%r12), %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	leaq	88(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6719CollationRuleParser4SinkD2Ev@PLT
	.cfi_endproc
.LFE3476:
	.size	_ZN6icu_6716CollationBuilderD2Ev, .-_ZN6icu_6716CollationBuilderD2Ev
	.globl	_ZN6icu_6716CollationBuilderD1Ev
	.set	_ZN6icu_6716CollationBuilderD1Ev,_ZN6icu_6716CollationBuilderD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilderD0Ev
	.type	_ZN6icu_6716CollationBuilderD0Ev, @function
_ZN6icu_6716CollationBuilderD0Ev:
.LFB3478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6716CollationBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L784
	movq	(%rdi), %rax
	call	*8(%rax)
.L784:
	leaq	584(%r12), %rdi
	call	_ZN6icu_679UVector64D1Ev@PLT
	leaq	552(%r12), %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	leaq	88(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6719CollationRuleParser4SinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3478:
	.size	_ZN6icu_6716CollationBuilderD0Ev, .-_ZN6icu_6716CollationBuilderD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder8optimizeERKNS_10UnicodeSetERPKcR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder8optimizeERKNS_10UnicodeSetERPKcR10UErrorCode, @function
_ZN6icu_6716CollationBuilder8optimizeERKNS_10UnicodeSetERPKcR10UErrorCode:
.LFB3494:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L791
	ret
	.p2align 4,,10
	.p2align 3
.L791:
	addq	$88, %rdi
	jmp	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	.cfi_endproc
.LFE3494:
	.size	_ZN6icu_6716CollationBuilder8optimizeERKNS_10UnicodeSetERPKcR10UErrorCode, .-_ZN6icu_6716CollationBuilder8optimizeERKNS_10UnicodeSetERPKcR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6716CollationBuilder14addOnlyClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode.part.0, @function
_ZN6icu_6716CollationBuilder14addOnlyClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode.part.0:
.LFB4716:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$616, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -632(%rbp)
	movq	16(%rbp), %rbx
	movq	%rdx, -608(%rbp)
	movq	%rcx, -616(%rbp)
	movl	%r8d, -600(%rbp)
	movl	%r9d, -596(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movswl	8(%rsi), %eax
	shrl	$5, %eax
	jne	.L793
	movq	%rdx, %rdi
	leaq	-256(%rbp), %r14
	movq	%rbx, %rdx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717CanonicalIteratorC1ERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L794
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-464(%rbp), %r13
	movq	%rax, -528(%rbp)
	movl	$2, %eax
	leaq	-528(%rbp), %r12
	movw	%ax, -520(%rbp)
	leaq	-454(%rbp), %rax
	movq	%rax, -624(%rbp)
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L808:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L795:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717CanonicalIterator4nextEv@PLT
	testb	$1, -456(%rbp)
	jne	.L796
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L808
	movq	16(%r15), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*88(%rax)
	testb	%al, %al
	je	.L808
	movzwl	-456(%rbp), %ecx
	testw	%cx, %cx
	js	.L800
	movswl	%cx, %edx
	sarl	$5, %edx
.L801:
	testl	%edx, %edx
	je	.L802
	testb	$2, %cl
	movq	-624(%rbp), %rax
	cmove	-440(%rbp), %rax
	movzwl	(%rax), %eax
	subl	$44032, %eax
	cmpl	$11171, %eax
	jbe	.L808
.L802:
	movq	-608(%rbp), %rax
	movswl	8(%rax), %eax
	movl	%eax, %esi
	andl	$1, %esi
	andl	$1, %ecx
	jne	.L804
	testw	%ax, %ax
	js	.L805
	sarl	$5, %eax
.L806:
	cmpl	%eax, %edx
	jne	.L807
	testb	%sil, %sil
	je	.L880
.L807:
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	-596(%rbp), %r9d
	movl	-600(%rbp), %r8d
	pushq	%rbx
	movq	-616(%rbp), %rcx
	call	_ZN6icu_6716CollationBuilder14addIfDifferentERKNS_13UnicodeStringES3_PKlijR10UErrorCode
	movl	(%rbx), %r11d
	popq	%r9
	movl	%eax, -596(%rbp)
	popq	%r10
	testl	%r11d, %r11d
	jle	.L808
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L794:
	movq	%r14, %rdi
	call	_ZN6icu_6717CanonicalIteratorD1Ev@PLT
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L793:
	leaq	-464(%rbp), %r13
	movq	%rbx, %rdx
	leaq	-256(%rbp), %r14
	movq	%r13, %rdi
	call	_ZN6icu_6717CanonicalIteratorC1ERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	-608(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6717CanonicalIteratorC1ERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L810
	leaq	-592(%rbp), %rax
	leaq	-518(%rbp), %rcx
	movq	%r13, -640(%rbp)
	movq	%rax, -656(%rbp)
	leaq	-528(%rbp), %r12
	movq	%rax, %r13
	movq	%rcx, -648(%rbp)
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L879:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L811:
	movq	-640(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717CanonicalIterator4nextEv@PLT
	testb	$1, -584(%rbp)
	jne	.L812
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L879
	movq	16(%r15), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*88(%rax)
	testb	%al, %al
	je	.L879
	movq	-632(%rbp), %rcx
	movzwl	-584(%rbp), %eax
	movswl	8(%rcx), %ecx
	movl	%eax, %esi
	movl	%ecx, %edx
	andl	$1, %edx
	movb	%dl, -624(%rbp)
	andl	$1, %esi
	jne	.L822
	testw	%ax, %ax
	js	.L817
	movswl	%ax, %edx
	sarl	$5, %edx
	testw	%cx, %cx
	js	.L819
.L885:
	sarl	$5, %ecx
.L820:
	cmpl	%edx, %ecx
	jne	.L821
	testb	$1, -624(%rbp)
	je	.L881
.L821:
	movb	%sil, -624(%rbp)
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L824:
	movq	16(%r15), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*88(%rax)
	testb	%al, %al
	je	.L835
	movzwl	-520(%rbp), %ecx
	testw	%cx, %cx
	js	.L827
	movswl	%cx, %edx
	sarl	$5, %edx
.L828:
	testl	%edx, %edx
	je	.L829
	testb	$2, %cl
	movq	-648(%rbp), %rax
	cmove	-504(%rbp), %rax
	movzwl	(%rax), %eax
	subl	$44032, %eax
	cmpl	$11171, %eax
	jbe	.L835
.L829:
	cmpb	$0, -624(%rbp)
	je	.L831
	movq	-608(%rbp), %rax
	movswl	8(%rax), %eax
	movl	%eax, %esi
	andl	$1, %esi
	andl	$1, %ecx
	jne	.L832
	testw	%ax, %ax
	js	.L833
	sarl	$5, %eax
.L834:
	testb	%sil, %sil
	jne	.L831
	cmpl	%eax, %edx
	je	.L882
	.p2align 4,,10
	.p2align 3
.L831:
	subq	$8, %rsp
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-616(%rbp), %rcx
	movl	-596(%rbp), %r9d
	pushq	%rbx
	movl	-600(%rbp), %r8d
	call	_ZN6icu_6716CollationBuilder14addIfDifferentERKNS_13UnicodeStringES3_PKlijR10UErrorCode
	movl	(%rbx), %ecx
	movl	%eax, -596(%rbp)
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L883
.L835:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L822:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6717CanonicalIterator4nextEv@PLT
	testb	$1, -520(%rbp)
	jne	.L823
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L835
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L883:
	movq	%r12, %rdi
	movq	-640(%rbp), %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-656(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L810:
	movq	%r14, %rdi
	call	_ZN6icu_6717CanonicalIteratorD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6717CanonicalIteratorD1Ev@PLT
.L792:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L884
	movl	-596(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L880:
	.cfi_restore_state
	movq	-608(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L804:
	testb	%sil, %sil
	jne	.L808
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L882:
	movq	-608(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L832:
	testb	%sil, %sil
	jne	.L835
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L805:
	movq	-608(%rbp), %rax
	movl	12(%rax), %eax
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L827:
	movl	-516(%rbp), %edx
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L833:
	movq	-608(%rbp), %rax
	movl	12(%rax), %eax
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L823:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6717CanonicalIterator5resetEv@PLT
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L800:
	movl	-452(%rbp), %edx
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L796:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6717CanonicalIteratorD1Ev@PLT
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L817:
	movl	-580(%rbp), %edx
	testw	%cx, %cx
	jns	.L885
.L819:
	movq	-632(%rbp), %rax
	movl	12(%rax), %ecx
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L812:
	movq	-656(%rbp), %rdi
	movq	-640(%rbp), %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6717CanonicalIteratorD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6717CanonicalIteratorD1Ev@PLT
	jmp	.L792
.L881:
	movq	-632(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	jmp	.L821
.L884:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4716:
	.size	_ZN6icu_6716CollationBuilder14addOnlyClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode.part.0, .-_ZN6icu_6716CollationBuilder14addOnlyClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder14addOnlyClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder14addOnlyClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode, @function
_ZN6icu_6716CollationBuilder14addOnlyClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode:
.LFB3496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L887
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6716CollationBuilder14addOnlyClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L887:
	.cfi_restore_state
	movl	%r9d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3496:
	.size	_ZN6icu_6716CollationBuilder14addOnlyClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode, .-_ZN6icu_6716CollationBuilder14addOnlyClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder17addTailCompositesERKNS_13UnicodeStringES3_R10UErrorCode
	.type	_ZN6icu_6716CollationBuilder17addTailCompositesERKNS_13UnicodeStringES3_R10UErrorCode, @function
_ZN6icu_6716CollationBuilder17addTailCompositesERKNS_13UnicodeStringES3_R10UErrorCode:
.LFB3497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$792, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -784(%rbp)
	movl	(%rax), %eax
	movq	%rcx, -776(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testl	%eax, %eax
	jg	.L889
	movswl	8(%rdx), %ebx
	movq	%rdi, %r12
	movq	%rdx, %r13
	testw	%bx, %bx
	js	.L891
	sarl	$5, %ebx
.L893:
	testl	%ebx, %ebx
	jne	.L892
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L926:
	subl	$2, %ebx
	movl	%ebx, %r14d
	je	.L889
.L896:
	movl	%r14d, %ebx
.L892:
	leal	-1(%rbx), %r14d
	movq	%r13, %rdi
	movl	%r14d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	8(%r12), %rdi
	movl	%eax, %r15d
	movq	(%rdi), %rax
	movl	%r15d, %esi
	call	*80(%rax)
	testb	%al, %al
	je	.L894
	cmpl	$65535, %r15d
	ja	.L926
	testl	%r14d, %r14d
	jne	.L896
	.p2align 4,,10
	.p2align 3
.L889:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L927
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L891:
	.cfi_restore_state
	movl	12(%rdx), %ebx
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L894:
	leal	-4352(%r15), %eax
	cmpl	$18, %eax
	jbe	.L889
	leaq	-512(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, %r14
	movq	%rax, -816(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	24(%r12), %rdi
	movq	%r14, %rdx
	movl	%r15d, %esi
	call	_ZNK6icu_6715Normalizer2Impl16getCanonStartSetEiRNS_10UnicodeSetE@PLT
	testb	%al, %al
	je	.L925
	movl	$2, %r15d
	movq	-816(%rbp), %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r11d
	movw	%r15w, -568(%rbp)
	leaq	-768(%rbp), %r15
	movl	$2, %r14d
	movq	%r15, %rdi
	movq	%rax, -704(%rbp)
	movq	%rax, -640(%rbp)
	movq	%rax, -576(%rbp)
	movw	%r11w, -696(%rbp)
	movw	%r14w, -632(%rbp)
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	leaq	-704(%rbp), %rax
	movq	%rax, -792(%rbp)
	leaq	-640(%rbp), %rax
	movq	%rax, -800(%rbp)
	leaq	-576(%rbp), %rax
	movq	%rax, -808(%rbp)
	leaq	-304(%rbp), %rax
	movq	%rax, -824(%rbp)
	.p2align 4,,10
	.p2align 3
.L901:
	movq	%r15, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	je	.L900
	movq	8(%r12), %rdi
	movl	-760(%rbp), %r14d
	movq	-792(%rbp), %rdx
	movq	(%rdi), %rax
	movl	%r14d, %esi
	call	*56(%rax)
	movq	-776(%rbp), %rax
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L901
	subq	$8, %rsp
	pushq	-808(%rbp)
	movl	%r14d, %ecx
	movl	%ebx, %edx
	movq	-800(%rbp), %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-792(%rbp), %r8
	call	_ZNK6icu_6716CollationBuilder24mergeCompositeIntoStringERKNS_13UnicodeStringEiiS3_RS1_S4_R10UErrorCode.part.0
	popq	%r8
	popq	%r9
	testb	%al, %al
	je	.L901
	movq	-800(%rbp), %r14
	movq	72(%r12), %rdi
	xorl	%r8d, %r8d
	movq	-824(%rbp), %rcx
	movq	-784(%rbp), %rsi
	movq	%r14, %rdx
	call	_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringES3_Pli@PLT
	movl	%eax, %r8d
	cmpl	$31, %eax
	jg	.L901
	subq	$8, %rsp
	pushq	-776(%rbp)
	movq	-784(%rbp), %rsi
	movq	%r12, %rdi
	movq	-824(%rbp), %rcx
	movl	$-1, %r9d
	movl	%eax, -828(%rbp)
	movq	-808(%rbp), %rdx
	call	_ZN6icu_6716CollationBuilder14addIfDifferentERKNS_13UnicodeStringES3_PKlijR10UErrorCode
	popq	%rsi
	popq	%rdi
	cmpl	$-1, %eax
	je	.L901
	movq	-776(%rbp), %rdx
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L901
	subq	$8, %rsp
	movl	%eax, %r9d
	movq	%r12, %rdi
	movl	-828(%rbp), %r8d
	movq	-824(%rbp), %rcx
	movq	-784(%rbp), %rsi
	pushq	%rdx
	movq	%r14, %rdx
	call	_ZN6icu_6716CollationBuilder14addOnlyClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode.part.0
	popq	%rax
	popq	%rdx
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L900:
	movq	%r15, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	movq	-808(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-800(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-792(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L925:
	movq	-816(%rbp), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	jmp	.L889
.L927:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3497:
	.size	_ZN6icu_6716CollationBuilder17addTailCompositesERKNS_13UnicodeStringES3_R10UErrorCode, .-_ZN6icu_6716CollationBuilder17addTailCompositesERKNS_13UnicodeStringES3_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder14addWithClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder14addWithClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode, @function
_ZN6icu_6716CollationBuilder14addWithClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode:
.LFB3495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	%rcx, -56(%rbp)
	movl	%r8d, -60(%rbp)
	pushq	%rbx
	call	_ZN6icu_6716CollationBuilder14addIfDifferentERKNS_13UnicodeStringES3_PKlijR10UErrorCode
	movl	(%rbx), %edi
	popq	%rcx
	movl	%eax, %r12d
	popq	%rsi
	testl	%edi, %edi
	jg	.L929
	subq	$8, %rsp
	movl	-60(%rbp), %r8d
	movq	-56(%rbp), %rcx
	movq	%r15, %rdx
	pushq	%rbx
	movl	%eax, %r9d
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6716CollationBuilder14addOnlyClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode.part.0
	movl	%eax, %r12d
	popq	%rax
	popq	%rdx
.L929:
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6716CollationBuilder17addTailCompositesERKNS_13UnicodeStringES3_R10UErrorCode
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3495:
	.size	_ZN6icu_6716CollationBuilder14addWithClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode, .-_ZN6icu_6716CollationBuilder14addWithClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"normalizing the relation prefix"
	.align 8
.LC22:
	.string	"normalizing the relation string"
	.align 8
.LC23:
	.string	"contractions starting with conjoining Jamo L or V not supported"
	.align 8
.LC24:
	.string	"contractions ending with conjoining Jamo L or L+V not supported"
	.align 8
.LC25:
	.string	"tailoring primary after ignorables not supported"
	.align 8
.LC26:
	.string	"tailoring quaternary after tertiary ignorables not supported"
	.section	.rodata.str1.1
.LC27:
	.string	"modifying collation elements"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"normalizing the relation extension"
	.align 8
.LC29:
	.string	"extension string adds too many collation elements (more than 31 total)"
	.section	.rodata.str1.1
.LC30:
	.string	"writing collation elements"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder11addRelationEiRKNS_13UnicodeStringES3_S3_RPKcR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder11addRelationEiRKNS_13UnicodeStringES3_S3_RPKcR10UErrorCode, @function
_ZN6icu_6716CollationBuilder11addRelationEiRKNS_13UnicodeStringES3_S3_RPKcR10UErrorCode:
.LFB3483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r12
	movl	%esi, -272(%rbp)
	movq	%r8, -280(%rbp)
	movq	%r9, -264(%rbp)
	movl	(%r12), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L931
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movq	%rdx, %r13
	movswl	8(%rdx), %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r9d
	movq	%rcx, %r14
	movq	%rax, -256(%rbp)
	movq	(%rdi), %rax
	shrl	$5, %edx
	leaq	-256(%rbp), %r15
	movw	%r9w, -248(%rbp)
	movq	24(%rax), %rax
	jne	.L1021
.L933:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rsi
	leaq	-192(%rbp), %r10
	movq	%r12, %rcx
	movq	%rsi, -192(%rbp)
	movl	$2, %esi
	movq	%r10, %rdx
	movw	%si, -184(%rbp)
	movq	%r14, %rsi
	movq	%r10, -288(%rbp)
	call	*%rax
	movl	(%r12), %edi
	movq	-288(%rbp), %r10
	testl	%edi, %edi
	jg	.L1024
	movzwl	-184(%rbp), %edx
	testw	%dx, %dx
	js	.L938
	movswl	%dx, %eax
	sarl	$5, %eax
	cmpl	$1, %eax
	jle	.L940
.L1027:
	andl	$2, %edx
	leaq	-182(%rbp), %rcx
	cmove	-168(%rbp), %rcx
	movq	%rcx, %rdx
	movzwl	(%rcx), %ecx
	leal	-4352(%rcx), %esi
	cmpl	$18, %esi
	jbe	.L942
	subl	$4449, %ecx
	cmpl	$20, %ecx
	jbe	.L942
	leal	-1(%rax), %ecx
	testl	%eax, %eax
	je	.L940
	movslq	%ecx, %rcx
	leaq	(%rcx,%rcx), %rdi
	movzwl	(%rdx,%rcx,2), %ecx
	leal	-4352(%rcx), %esi
	cmpl	$18, %esi
	jbe	.L944
	subl	$4449, %ecx
	cmpl	$20, %ecx
	ja	.L940
	cmpl	$1, %eax
	jbe	.L940
	movzwl	-2(%rdx,%rdi), %eax
	subl	$4352, %eax
	cmpl	$18, %eax
	ja	.L940
.L944:
	movq	-264(%rbp), %rax
	leaq	.LC24(%rip), %rbx
	movl	$16, (%r12)
	movq	%rbx, (%rax)
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	*%rax
	movl	(%r12), %r8d
	testl	%r8d, %r8d
	jle	.L1025
	movq	-264(%rbp), %rax
	leaq	.LC21(%rip), %rbx
	movq	%rbx, (%rax)
.L935:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L931:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1026
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L938:
	movl	-180(%rbp), %eax
	cmpl	$1, %eax
	jg	.L1027
.L940:
	movl	-272(%rbp), %eax
	cmpl	$15, %eax
	jne	.L1028
.L945:
	movq	-264(%rbp), %rdx
	movq	%r10, %rsi
	movq	%r12, %rcx
	movq	%rbx, %rdi
	movq	%r10, -272(%rbp)
	call	_ZN6icu_6716CollationBuilder11setCaseBitsERKNS_13UnicodeStringERPKcR10UErrorCode.part.0
	movl	(%r12), %eax
	movq	-272(%rbp), %r10
	testl	%eax, %eax
	jg	.L937
	movl	544(%rbx), %eax
	movq	-280(%rbp), %rsi
	movl	%eax, -288(%rbp)
	movswl	8(%rsi), %eax
	shrl	$5, %eax
	je	.L955
	movq	8(%rbx), %rdi
	leaq	-128(%rbp), %r8
	movq	%r12, %rcx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movq	%r8, %rdx
	movw	%ax, -120(%rbp)
	movq	(%rdi), %rax
	movq	%r10, -280(%rbp)
	movq	%r8, -272(%rbp)
	call	*24(%rax)
	movl	(%r12), %eax
	movq	-272(%rbp), %r8
	movq	-280(%rbp), %r10
	testl	%eax, %eax
	jle	.L956
	movq	-264(%rbp), %rax
	leaq	.LC28(%rip), %rbx
	movq	%rbx, (%rax)
.L957:
	movq	%r8, %rdi
	movq	%r10, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-264(%rbp), %r10
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L1024:
	movq	-264(%rbp), %rax
	leaq	.LC22(%rip), %rbx
	movq	%rbx, (%rax)
.L937:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L942:
	movq	-264(%rbp), %rax
	leaq	.LC23(%rip), %rbx
	movl	$16, (%r12)
	movq	%rbx, (%rax)
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L1028:
	movq	-264(%rbp), %rdx
	movq	%r12, %rcx
	movl	%eax, %esi
	movq	%rbx, %rdi
	movq	%r10, -288(%rbp)
	call	_ZN6icu_6716CollationBuilder22findOrInsertNodeForCEsEiRPKcR10UErrorCode.part.0
	movl	544(%rbx), %ecx
	movq	-288(%rbp), %r10
	leal	-1(%rcx), %edx
	movl	-272(%rbp), %ecx
	movslq	%edx, %rdx
	testl	%ecx, %ecx
	movq	296(%rbx,%rdx,8), %r8
	jne	.L946
	movl	%r8d, %edx
	shrl	$24, %edx
	subl	$6, %edx
	cmpl	$63, %edx
	jbe	.L947
	movq	%r8, %rdx
	sarq	$32, %rdx
	testl	%edx, %edx
	je	.L1029
.L947:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L952
	movl	-272(%rbp), %edx
	movq	%r12, %rcx
	movl	%eax, %esi
	movq	%rbx, %rdi
	movq	%r10, -296(%rbp)
	movq	%r8, -288(%rbp)
	call	_ZN6icu_6716CollationBuilder23insertTailoredNodeAfterEiiR10UErrorCode.part.0
	movq	-296(%rbp), %r10
	movl	%eax, %r11d
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L952
	movq	-288(%rbp), %r8
	movl	%r8d, %eax
	shrl	$24, %eax
	subl	$6, %eax
	cmpl	$63, %eax
	ja	.L953
	movl	%r8d, %eax
	sarl	$8, %eax
	andl	$3, %eax
	movl	%eax, %r9d
.L954:
	movl	544(%rbx), %eax
	movq	%r11, %rcx
	movq	%r11, %rdx
	sall	$24, %r11d
	salq	$43, %rcx
	salq	$42, %rdx
	movabsq	$9151314442816847872, %rdi
	andl	$1056964608, %r11d
	andq	%rdi, %rcx
	leal	-1(%rax), %esi
	movl	-272(%rbp), %eax
	movabsq	$4629700417037541376, %rdi
	addq	%rcx, %rdi
	movslq	%esi, %rsi
	movabsq	$35747322042253312, %rcx
	andq	%rcx, %rdx
	cmpl	%r9d, %eax
	cmovg	%r9d, %eax
	leaq	(%rdi,%rdx), %rcx
	leaq	(%rcx,%r11), %rdx
	sall	$8, %eax
	cltq
	addq	%rdx, %rax
	movq	%rax, 296(%rbx,%rsi,8)
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L946:
	cmpl	$3, -272(%rbp)
	jne	.L947
	testq	%r8, %r8
	jne	.L947
	movq	-264(%rbp), %rax
	leaq	.LC26(%rip), %rbx
	movl	$16, (%r12)
	movq	%rbx, (%rax)
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L955:
	leaq	296(%rbx), %r11
.L959:
	movswl	-248(%rbp), %ecx
	movzwl	8(%r13), %eax
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L960
	testw	%ax, %ax
	js	.L961
	movswl	%ax, %edx
	sarl	$5, %edx
.L962:
	testw	%cx, %cx
	js	.L963
	sarl	$5, %ecx
.L964:
	testb	%sil, %sil
	jne	.L965
	cmpl	%edx, %ecx
	je	.L1030
.L965:
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L1031
.L968:
	movl	$-1, %r9d
.L974:
	subq	$8, %rsp
	movq	%r10, %rdx
	movq	%rbx, %rdi
	movq	%r11, %rcx
	movl	544(%rbx), %r13d
	pushq	%r12
	movq	%r15, %rsi
	movq	%r10, -272(%rbp)
	movl	%r13d, %r8d
	movq	%r11, -280(%rbp)
	call	_ZN6icu_6716CollationBuilder14addIfDifferentERKNS_13UnicodeStringES3_PKlijR10UErrorCode
	movl	(%r12), %r10d
	popq	%rdi
	movl	%eax, %r9d
	popq	%r8
	testl	%r10d, %r10d
	movq	-272(%rbp), %r10
	jg	.L980
	subq	$8, %rsp
	movq	%r10, %rdx
	movq	%r15, %rsi
	movl	%r13d, %r8d
	pushq	%r12
	movq	-280(%rbp), %r11
	movq	%rbx, %rdi
	movq	%r11, %rcx
	call	_ZN6icu_6716CollationBuilder14addOnlyClosureERKNS_13UnicodeStringES3_PKlijR10UErrorCode.part.0
	movq	-272(%rbp), %r10
	popq	%rcx
	popq	%rsi
.L980:
	movq	%r10, %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r10, -272(%rbp)
	call	_ZN6icu_6716CollationBuilder17addTailCompositesERKNS_13UnicodeStringES3_R10UErrorCode
	movl	(%r12), %edx
	movq	-272(%rbp), %r10
	testl	%edx, %edx
	jle	.L981
	movq	-264(%rbp), %rax
	leaq	.LC30(%rip), %rbx
	movq	%rbx, (%rax)
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L952:
	movq	-264(%rbp), %rax
	leaq	.LC27(%rip), %rbx
	movq	%rbx, (%rax)
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L956:
	leaq	296(%rbx), %r11
	movl	544(%rbx), %ecx
	movq	72(%rbx), %rdi
	movq	%r8, %rsi
	movq	%r11, %rdx
	movq	%r10, -296(%rbp)
	movq	%r11, -280(%rbp)
	movq	%r8, -272(%rbp)
	call	_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringEPli@PLT
	movq	-272(%rbp), %r8
	movq	-280(%rbp), %r11
	cmpl	$31, %eax
	movl	%eax, 544(%rbx)
	movq	-296(%rbp), %r10
	jle	.L958
	movq	-264(%rbp), %rax
	leaq	.LC29(%rip), %rbx
	movl	$1, (%r12)
	movq	%rbx, (%rax)
	jmp	.L957
.L1030:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r10, -280(%rbp)
	movq	%r11, -272(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-280(%rbp), %r10
	movq	-272(%rbp), %r11
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L960:
	testb	%sil, %sil
	je	.L965
	movswl	-184(%rbp), %ecx
	movzwl	8(%r14), %eax
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L969
	testw	%ax, %ax
	js	.L970
	movswl	%ax, %edx
	sarl	$5, %edx
.L971:
	testw	%cx, %cx
	js	.L972
	sarl	$5, %ecx
.L973:
	cmpl	%edx, %ecx
	jne	.L965
	testb	%sil, %sil
	jne	.L965
	movq	%r10, %rsi
	movq	%r14, %rdi
	movq	%r11, -280(%rbp)
	movq	%r10, -272(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-280(%rbp), %r11
	movq	-272(%rbp), %r10
	testb	%al, %al
	setne	%sil
.L969:
	testb	%sil, %sil
	jne	.L968
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L953:
	movabsq	$-72057594037927936, %rax
	xorl	%r9d, %r9d
	testq	%rax, %r8
	jne	.L954
	movl	$1, %r9d
	testl	$-16777216, %r8d
	jne	.L954
	cmpq	$1, %r8
	sbbl	%r9d, %r9d
	andl	$13, %r9d
	addl	$2, %r9d
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L981:
	movl	-288(%rbp), %eax
	movq	%r10, %rdi
	movl	%eax, 544(%rbx)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L931
.L1031:
	movq	16(%rbx), %rdi
	movq	%r10, -280(%rbp)
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, -272(%rbp)
	movq	(%rdi), %rax
	call	*88(%rax)
	movq	-272(%rbp), %r11
	movq	-280(%rbp), %r10
	testb	%al, %al
	je	.L968
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L968
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*88(%rax)
	movq	-272(%rbp), %r11
	movq	-280(%rbp), %r10
	testb	%al, %al
	je	.L968
	movzwl	8(%r14), %edx
	testw	%dx, %dx
	js	.L975
	movswl	%dx, %eax
	sarl	$5, %eax
.L976:
	testl	%eax, %eax
	je	.L977
	andl	$2, %edx
	leaq	10(%r14), %rax
	jne	.L979
	movq	24(%r14), %rax
.L979:
	movzwl	(%rax), %eax
	subl	$44032, %eax
	cmpl	$11171, %eax
	jbe	.L968
.L977:
	subq	$8, %rsp
	movl	$-1, %r9d
	movq	%r11, %rcx
	movq	%r13, %rsi
	pushq	%r12
	movl	544(%rbx), %r8d
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%r10, -280(%rbp)
	movq	%r11, -272(%rbp)
	call	_ZN6icu_6716CollationBuilder14addIfDifferentERKNS_13UnicodeStringES3_PKlijR10UErrorCode
	popq	%r11
	movq	-280(%rbp), %r10
	movq	-272(%rbp), %r11
	movl	%eax, %r9d
	popq	%r13
	jmp	.L974
.L958:
	movq	%r8, %rdi
	movq	%r10, -280(%rbp)
	movq	%r11, -272(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-272(%rbp), %r11
	movq	-280(%rbp), %r10
	jmp	.L959
.L1029:
	movq	-264(%rbp), %rax
	leaq	.LC25(%rip), %rbx
	movl	$16, (%r12)
	movq	%rbx, (%rax)
	jmp	.L937
.L961:
	movl	12(%r13), %edx
	jmp	.L962
.L963:
	movl	-244(%rbp), %ecx
	jmp	.L964
.L972:
	movl	-180(%rbp), %ecx
	jmp	.L973
.L970:
	movl	12(%r14), %edx
	jmp	.L971
.L975:
	movl	12(%r14), %eax
	jmp	.L976
.L1026:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3483:
	.size	_ZN6icu_6716CollationBuilder11addRelationEiRKNS_13UnicodeStringES3_S3_RPKcR10UErrorCode, .-_ZN6icu_6716CollationBuilder11addRelationEiRKNS_13UnicodeStringES3_S3_RPKcR10UErrorCode
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC31:
	.string	"["
	.string	":"
	.string	"N"
	.string	"F"
	.string	"D"
	.string	"_"
	.string	"Q"
	.string	"C"
	.string	"="
	.string	"N"
	.string	":"
	.string	"]"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder19closeOverCompositesER10UErrorCode
	.type	_ZN6icu_6716CollationBuilder19closeOverCompositesER10UErrorCode, @function
_ZN6icu_6716CollationBuilder19closeOverCompositesER10UErrorCode:
.LFB3502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-448(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	movq	%r13, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$424, %rsp
	movq	%rsi, -464(%rbp)
	movl	$1, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC31(%rip), %rax
	movq	%rax, -448(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r14), %edi
	testl	%edi, %edi
	jg	.L1041
	movl	$55203, %edx
	movl	$44032, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet6removeEii@PLT
	movl	$2, %esi
	movl	$2, %ecx
	movq	%r13, %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%si, -312(%rbp)
	movq	%r15, %rsi
	leaq	296(%rbx), %r14
	movq	%rax, -384(%rbp)
	movq	%rax, -320(%rbp)
	movw	%cx, -376(%rbp)
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	leaq	-384(%rbp), %rax
	movq	%rax, -456(%rbp)
	.p2align 4,,10
	.p2align 3
.L1036:
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	je	.L1035
.L1042:
	movq	8(%rbx), %rdi
	movl	-440(%rbp), %esi
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	72(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_6720CollationDataBuilder6getCEsERKNS_13UnicodeStringEPli@PLT
	movl	%eax, 544(%rbx)
	cmpl	$31, %eax
	jg	.L1036
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIterator9getStringEv@PLT
	subq	$8, %rsp
	movq	%rbx, %rdi
	movq	%r14, %rcx
	pushq	-464(%rbp)
	movl	544(%rbx), %r8d
	movq	%rax, %rdx
	movl	$-1, %r9d
	movq	-456(%rbp), %rsi
	call	_ZN6icu_6716CollationBuilder14addIfDifferentERKNS_13UnicodeStringES3_PKlijR10UErrorCode
	popq	%rax
	movq	%r13, %rdi
	popq	%rdx
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	jne	.L1042
.L1035:
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-456(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1041:
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1043
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1043:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3502:
	.size	_ZN6icu_6716CollationBuilder19closeOverCompositesER10UErrorCode, .-_ZN6icu_6716CollationBuilder19closeOverCompositesER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6716CollationBuilder13parseAndBuildERKNS_13UnicodeStringEPKhPNS_19CollationRuleParser8ImporterEP11UParseErrorR10UErrorCode.part.0, @function
_ZN6icu_6716CollationBuilder13parseAndBuildERKNS_13UnicodeStringEPKhPNS_19CollationRuleParser8ImporterEP11UParseErrorR10UErrorCode.part.0:
.LFB4717:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$400, %edi
	subq	$136, %rsp
	movq	%rsi, -152(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%r8, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1045
	movq	32(%rbx), %rax
	movq	%r13, %rdi
	movq	32(%rax), %rsi
	call	_ZN6icu_6718CollationTailoringC1EPKNS_17CollationSettingsE@PLT
	cmpq	$0, 32(%r13)
	je	.L1046
	movq	40(%rbx), %rsi
	leaq	-144(%rbp), %r14
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6719CollationRuleParserC1EPKNS_13CollationDataER10UErrorCode@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jle	.L1057
.L1047:
	movq	%r14, %rdi
	call	_ZN6icu_6719CollationRuleParserD1Ev@PLT
.L1053:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	*8(%rax)
.L1044:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1058
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1057:
	.cfi_restore_state
	movq	32(%rbx), %rax
	movq	32(%rax), %rax
	movl	28(%rax), %eax
	movq	%r15, -80(%rbp)
	movq	32(%r13), %r15
	movq	%rbx, -88(%rbp)
	movl	%eax, 64(%rbx)
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	cmpl	$1, %eax
	jle	.L1048
	movl	$856, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1055
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN6icu_6717CollationSettingsC1ERKS0_@PLT
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-176(%rbp), %rax
	movq	%rax, 32(%r13)
	movq	%rax, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-176(%rbp), %rax
	movq	%rax, %r15
.L1048:
	movq	-160(%rbp), %rcx
	movq	%r12, %r8
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	-152(%rbp), %rsi
	call	_ZN6icu_6719CollationRuleParser5parseERKNS_13UnicodeStringERNS_17CollationSettingsEP11UParseErrorR10UErrorCode@PLT
	movq	-96(%rbp), %rax
	movl	(%r12), %ecx
	movq	%rax, 288(%rbx)
	testl	%ecx, %ecx
	jg	.L1047
	movq	72(%rbx), %rax
	cmpb	$0, 608(%rax)
	jne	.L1059
	movq	40(%rbx), %rdi
	movq	%rdi, 24(%r13)
.L1051:
	leaq	84(%r15), %rdx
	movl	$384, %ecx
	leaq	40(%r13), %r12
	movq	%r15, %rsi
	call	_ZN6icu_6718CollationFastLatin10getOptionsEPKNS_13CollationDataERKNS_17CollationSettingsEPti@PLT
	movq	-152(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, 80(%r15)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	32(%rbx), %rsi
	movq	-168(%rbp), %rdx
	movq	%r13, %rdi
	addq	$328, %rsi
	call	_ZN6icu_6718CollationTailoring10setVersionEPKhS2_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6719CollationRuleParserD1Ev@PLT
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1046:
	movl	$7, (%r12)
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6716CollationBuilder15makeTailoredCEsER10UErrorCode
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6716CollationBuilder19closeOverCompositesER10UErrorCode
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6716CollationBuilder11finalizeCEsER10UErrorCode
	leaq	88(%rbx), %r8
	movl	$127, %edx
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -160(%rbp)
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$255, %edx
	movl	$192, %esi
	movq	-160(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$55203, %edx
	movl	$44032, %esi
	movq	-160(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSet6removeEii@PLT
	movq	-160(%rbp), %r8
	movq	72(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r8, %rsi
	call	_ZN6icu_6720CollationDataBuilder8optimizeERKNS_10UnicodeSetER10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6718CollationTailoring15ensureOwnedDataER10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L1047
	cmpb	$0, 80(%rbx)
	movq	72(%rbx), %rdi
	je	.L1050
	movb	$1, 609(%rdi)
	movq	72(%rbx), %rdi
.L1050:
	movq	(%rdi), %rax
	movq	336(%r13), %rsi
	movq	%r12, %rdx
	call	*40(%rax)
	movq	72(%rbx), %rax
	movq	%rax, 344(%r13)
	movl	(%r12), %eax
	movq	$0, 72(%rbx)
	testl	%eax, %eax
	jg	.L1047
	movq	24(%r13), %rdi
	jmp	.L1051
.L1045:
	movl	$7, (%r12)
	jmp	.L1044
.L1058:
	call	__stack_chk_fail@PLT
.L1055:
	xorl	%r15d, %r15d
	jmp	.L1048
	.cfi_endproc
.LFE4717:
	.size	_ZN6icu_6716CollationBuilder13parseAndBuildERKNS_13UnicodeStringEPKhPNS_19CollationRuleParser8ImporterEP11UParseErrorR10UErrorCode.part.0, .-_ZN6icu_6716CollationBuilder13parseAndBuildERKNS_13UnicodeStringEPKhPNS_19CollationRuleParser8ImporterEP11UParseErrorR10UErrorCode.part.0
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"missing root elements data, tailoring not supported"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationBuilder13parseAndBuildERKNS_13UnicodeStringEPKhPNS_19CollationRuleParser8ImporterEP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6716CollationBuilder13parseAndBuildERKNS_13UnicodeStringEPKhPNS_19CollationRuleParser8ImporterEP11UParseErrorR10UErrorCode, @function
_ZN6icu_6716CollationBuilder13parseAndBuildERKNS_13UnicodeStringEPKhPNS_19CollationRuleParser8ImporterEP11UParseErrorR10UErrorCode:
.LFB3479:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L1060
	movq	40(%rdi), %rax
	cmpq	$0, 128(%rax)
	je	.L1064
	jmp	_ZN6icu_6716CollationBuilder13parseAndBuildERKNS_13UnicodeStringEPKhPNS_19CollationRuleParser8ImporterEP11UParseErrorR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1064:
	leaq	.LC32(%rip), %rax
	movl	$2, (%r9)
	movq	%rax, 288(%rdi)
.L1060:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3479:
	.size	_ZN6icu_6716CollationBuilder13parseAndBuildERKNS_13UnicodeStringEPKhPNS_19CollationRuleParser8ImporterEP11UParseErrorR10UErrorCode, .-_ZN6icu_6716CollationBuilder13parseAndBuildERKNS_13UnicodeStringEPKhPNS_19CollationRuleParser8ImporterEP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollator22internalBuildTailoringERKNS_13UnicodeStringEi18UColAttributeValueP11UParseErrorPS1_R10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollator22internalBuildTailoringERKNS_13UnicodeStringEi18UColAttributeValueP11UParseErrorPS1_R10UErrorCode, @function
_ZN6icu_6717RuleBasedCollator22internalBuildTailoringERKNS_13UnicodeStringEi18UColAttributeValueP11UParseErrorPS1_R10UErrorCode:
.LFB3466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	subq	$760, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -792(%rbp)
	movq	16(%rbp), %r15
	movq	%rsi, -776(%rbp)
	movl	%edx, -788(%rbp)
	movq	%r15, %rdi
	movq	%r8, -784(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713CollationRoot7getRootER10UErrorCode@PLT
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L1065
	movq	%rax, %rsi
	testq	%r12, %r12
	je	.L1067
	movzwl	8(%r12), %edx
	movl	$2, %eax
	movl	%edx, %ecx
	andl	$31, %ecx
	andl	$1, %edx
	cmove	%ecx, %eax
	movw	%ax, 8(%r12)
.L1067:
	leaq	-688(%rbp), %r14
	movq	%r15, %rdx
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_114BundleImporterE(%rip), %rbx
	movq	%r14, %rdi
	call	_ZN6icu_6716CollationBuilderC1EPKNS_18CollationTailoringER10UErrorCode
	movl	(%r15), %edx
	movl	$0, -60(%rbp)
	movq	%rbx, -760(%rbp)
	testl	%edx, %edx
	jg	.L1069
	movq	-648(%rbp), %rax
	cmpq	$0, 128(%rax)
	je	.L1114
	leaq	-760(%rbp), %rax
	leaq	-60(%rbp), %rdx
	movq	%r15, %r9
	movq	%r14, %rdi
	movq	-784(%rbp), %r8
	movq	-776(%rbp), %rsi
	movq	%rax, %rcx
	movq	%rax, -800(%rbp)
	call	_ZN6icu_6716CollationBuilder13parseAndBuildERKNS_13UnicodeStringEPKhPNS_19CollationRuleParser8ImporterEP11UParseErrorR10UErrorCode.part.0
	movq	%rax, %r8
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L1115
	leaq	104(%r8), %rdi
	movq	%r8, -776(%rbp)
	call	_ZN6icu_676Locale10setToBogusEv@PLT
	movq	-776(%rbp), %r8
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6717RuleBasedCollator14adoptTailoringEPNS_18CollationTailoringER10UErrorCode@PLT
	movl	-788(%rbp), %edx
	cmpl	$-1, %edx
	je	.L1076
	movq	0(%r13), %rax
	movq	%r15, %rcx
	movl	$5, %esi
	movq	%r13, %rdi
	call	*184(%rax)
.L1076:
	movl	-792(%rbp), %edx
	cmpl	$-1, %edx
	je	.L1077
	movq	0(%r13), %rax
	movq	%r15, %rcx
	movl	$4, %esi
	movq	%r13, %rdi
	call	*184(%rax)
.L1077:
	movq	-800(%rbp), %rdi
	movq	%rbx, -760(%rbp)
	call	_ZN6icu_6719CollationRuleParser8ImporterD2Ev@PLT
	movq	-616(%rbp), %rdi
	leaq	16+_ZTVN6icu_6716CollationBuilderE(%rip), %rax
	movq	%rax, -688(%rbp)
	testq	%rdi, %rdi
	je	.L1078
	movq	(%rdi), %rax
	call	*8(%rax)
.L1078:
	leaq	-104(%rbp), %rdi
	call	_ZN6icu_679UVector64D1Ev@PLT
	leaq	-136(%rbp), %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	leaq	-600(%rbp), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6719CollationRuleParser4SinkD2Ev@PLT
.L1065:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1116
	addq	$760, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1069:
	.cfi_restore_state
	movq	-400(%rbp), %rsi
	testq	%rsi, %rsi
	setne	%al
.L1071:
	leaq	-760(%rbp), %rcx
	movq	%rcx, -800(%rbp)
	testq	%r12, %r12
	je	.L1077
	xorl	%r8d, %r8d
	testb	%al, %al
	je	.L1077
.L1080:
	leaq	-752(%rbp), %r13
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r8, -776(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-776(%rbp), %r8
.L1073:
	testq	%r8, %r8
	je	.L1077
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	-400(%rbp), %rsi
	testq	%r12, %r12
	je	.L1073
	testq	%rsi, %rsi
	je	.L1073
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1114:
	leaq	.LC32(%rip), %rsi
	movl	$2, (%r15)
	movl	$1, %eax
	movq	%rsi, -400(%rbp)
	jmp	.L1071
.L1116:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3466:
	.size	_ZN6icu_6717RuleBasedCollator22internalBuildTailoringERKNS_13UnicodeStringEi18UColAttributeValueP11UParseErrorPS1_R10UErrorCode, .-_ZN6icu_6717RuleBasedCollator22internalBuildTailoringERKNS_13UnicodeStringEi18UColAttributeValueP11UParseErrorPS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringER10UErrorCode:
.LFB3452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678CollatorC2Ev@PLT
	leaq	16+_ZTVN6icu_6717RuleBasedCollatorE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, (%r12)
	leaq	40(%r12), %rdi
	xorl	%r8d, %r8d
	leaq	.LC2(%rip), %rsi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	%rbx
	movl	$-1, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movb	$0, 268(%r12)
	movl	$-1, %ecx
	movl	$0, 264(%r12)
	call	_ZN6icu_6717RuleBasedCollator22internalBuildTailoringERKNS_13UnicodeStringEi18UColAttributeValueP11UParseErrorPS1_R10UErrorCode
	popq	%rax
	popq	%rdx
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3452:
	.size	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6717RuleBasedCollatorC1ERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6717RuleBasedCollatorC1ERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthER10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthER10UErrorCode, @function
_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthER10UErrorCode:
.LFB3455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	call	_ZN6icu_678CollatorC2Ev@PLT
	leaq	16+_ZTVN6icu_6717RuleBasedCollatorE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, (%r12)
	leaq	40(%r12), %rdi
	xorl	%r8d, %r8d
	leaq	.LC2(%rip), %rsi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	subq	$8, %rsp
	movl	%r14d, %edx
	movq	%r13, %rsi
	pushq	%rbx
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movb	$0, 268(%r12)
	movl	$-1, %ecx
	movl	$0, 264(%r12)
	call	_ZN6icu_6717RuleBasedCollator22internalBuildTailoringERKNS_13UnicodeStringEi18UColAttributeValueP11UParseErrorPS1_R10UErrorCode
	popq	%rax
	popq	%rdx
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3455:
	.size	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthER10UErrorCode, .-_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthER10UErrorCode
	.globl	_ZN6icu_6717RuleBasedCollatorC1ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthER10UErrorCode
	.set	_ZN6icu_6717RuleBasedCollatorC1ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthER10UErrorCode,_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringE18UColAttributeValueR10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringE18UColAttributeValueR10UErrorCode, @function
_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringE18UColAttributeValueR10UErrorCode:
.LFB3458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	call	_ZN6icu_678CollatorC2Ev@PLT
	leaq	16+_ZTVN6icu_6717RuleBasedCollatorE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, (%r12)
	leaq	40(%r12), %rdi
	xorl	%r8d, %r8d
	leaq	.LC2(%rip), %rsi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	subq	$8, %rsp
	movl	%r14d, %ecx
	movq	%r13, %rsi
	pushq	%rbx
	movl	$-1, %edx
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movl	$0, 264(%r12)
	xorl	%r8d, %r8d
	movb	$0, 268(%r12)
	call	_ZN6icu_6717RuleBasedCollator22internalBuildTailoringERKNS_13UnicodeStringEi18UColAttributeValueP11UParseErrorPS1_R10UErrorCode
	popq	%rax
	popq	%rdx
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3458:
	.size	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringE18UColAttributeValueR10UErrorCode, .-_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringE18UColAttributeValueR10UErrorCode
	.globl	_ZN6icu_6717RuleBasedCollatorC1ERKNS_13UnicodeStringE18UColAttributeValueR10UErrorCode
	.set	_ZN6icu_6717RuleBasedCollatorC1ERKNS_13UnicodeStringE18UColAttributeValueR10UErrorCode,_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringE18UColAttributeValueR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthE18UColAttributeValueR10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthE18UColAttributeValueR10UErrorCode, @function
_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthE18UColAttributeValueR10UErrorCode:
.LFB3461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678CollatorC2Ev@PLT
	leaq	16+_ZTVN6icu_6717RuleBasedCollatorE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, (%r12)
	leaq	40(%r12), %rdi
	xorl	%r8d, %r8d
	leaq	.LC2(%rip), %rsi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	subq	$8, %rsp
	movl	%r14d, %edx
	movl	%r15d, %ecx
	pushq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movl	$0, 264(%r12)
	xorl	%r8d, %r8d
	movb	$0, 268(%r12)
	call	_ZN6icu_6717RuleBasedCollator22internalBuildTailoringERKNS_13UnicodeStringEi18UColAttributeValueP11UParseErrorPS1_R10UErrorCode
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3461:
	.size	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthE18UColAttributeValueR10UErrorCode, .-_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthE18UColAttributeValueR10UErrorCode
	.globl	_ZN6icu_6717RuleBasedCollatorC1ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthE18UColAttributeValueR10UErrorCode
	.set	_ZN6icu_6717RuleBasedCollatorC1ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthE18UColAttributeValueR10UErrorCode,_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringENS_8Collator18ECollationStrengthE18UColAttributeValueR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringER11UParseErrorRS1_R10UErrorCode
	.type	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringER11UParseErrorRS1_R10UErrorCode, @function
_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringER11UParseErrorRS1_R10UErrorCode:
.LFB3464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678CollatorC2Ev@PLT
	leaq	16+_ZTVN6icu_6717RuleBasedCollatorE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, (%r12)
	leaq	40(%r12), %rdi
	xorl	%r8d, %r8d
	leaq	.LC2(%rip), %rsi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	subq	$8, %rsp
	movq	%r14, %r9
	movq	%rbx, %r8
	pushq	%r15
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$0, 268(%r12)
	movl	$-1, %ecx
	movl	$0, 264(%r12)
	call	_ZN6icu_6717RuleBasedCollator22internalBuildTailoringERKNS_13UnicodeStringEi18UColAttributeValueP11UParseErrorPS1_R10UErrorCode
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3464:
	.size	_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringER11UParseErrorRS1_R10UErrorCode, .-_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringER11UParseErrorRS1_R10UErrorCode
	.globl	_ZN6icu_6717RuleBasedCollatorC1ERKNS_13UnicodeStringER11UParseErrorRS1_R10UErrorCode
	.set	_ZN6icu_6717RuleBasedCollatorC1ERKNS_13UnicodeStringER11UParseErrorRS1_R10UErrorCode,_ZN6icu_6717RuleBasedCollatorC2ERKNS_13UnicodeStringER11UParseErrorRS1_R10UErrorCode
	.p2align 4
	.globl	ucol_openRules_67
	.type	ucol_openRules_67, @function
ucol_openRules_67:
.LFB3521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movl	%edx, -148(%rbp)
	movl	(%r9), %r10d
	movl	%ecx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L1127
	movq	%rdi, %r13
	movl	%esi, %r14d
	movq	%r8, %r15
	movq	%r9, %rbx
	testq	%rdi, %rdi
	jne	.L1129
	testl	%esi, %esi
	je	.L1129
	movl	$1, (%r9)
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1129:
	movl	$272, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1130
	movq	%rax, %rdi
	call	_ZN6icu_678CollatorC2Ev@PLT
	leaq	16+_ZTVN6icu_6717RuleBasedCollatorE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, (%r12)
	leaq	40(%r12), %rdi
	xorl	%r8d, %r8d
	leaq	.LC2(%rip), %rsi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r13, -136(%rbp)
	movl	%r14d, %esi
	leaq	-128(%rbp), %r13
	leaq	-136(%rbp), %rdx
	shrl	$31, %esi
	movl	%r14d, %ecx
	movq	%r13, %rdi
	movl	$0, 264(%r12)
	movb	$0, 268(%r12)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	subq	$8, %rsp
	movl	-148(%rbp), %ecx
	xorl	%r9d, %r9d
	movq	%r15, %r8
	pushq	%rbx
	movl	-152(%rbp), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6717RuleBasedCollator22internalBuildTailoringERKNS_13UnicodeStringEi18UColAttributeValueP11UParseErrorPS1_R10UErrorCode
	movl	(%rbx), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L1141
.L1131:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1127:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1142
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1141:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*8(%rax)
	jmp	.L1131
.L1142:
	call	__stack_chk_fail@PLT
.L1130:
	movl	$7, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L1127
	.cfi_endproc
.LFE3521:
	.size	ucol_openRules_67, .-ucol_openRules_67
	.weak	_ZTSN6icu_6716CollationBuilderE
	.section	.rodata._ZTSN6icu_6716CollationBuilderE,"aG",@progbits,_ZTSN6icu_6716CollationBuilderE,comdat
	.align 16
	.type	_ZTSN6icu_6716CollationBuilderE, @object
	.size	_ZTSN6icu_6716CollationBuilderE, 28
_ZTSN6icu_6716CollationBuilderE:
	.string	"N6icu_6716CollationBuilderE"
	.weak	_ZTIN6icu_6716CollationBuilderE
	.section	.data.rel.ro._ZTIN6icu_6716CollationBuilderE,"awG",@progbits,_ZTIN6icu_6716CollationBuilderE,comdat
	.align 8
	.type	_ZTIN6icu_6716CollationBuilderE, @object
	.size	_ZTIN6icu_6716CollationBuilderE, 24
_ZTIN6icu_6716CollationBuilderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6716CollationBuilderE
	.quad	_ZTIN6icu_6719CollationRuleParser4SinkE
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_114BundleImporterE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_114BundleImporterE, 24
_ZTIN6icu_6712_GLOBAL__N_114BundleImporterE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_114BundleImporterE
	.quad	_ZTIN6icu_6719CollationRuleParser8ImporterE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_114BundleImporterE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_114BundleImporterE, 41
_ZTSN6icu_6712_GLOBAL__N_114BundleImporterE:
	.string	"*N6icu_6712_GLOBAL__N_114BundleImporterE"
	.weak	_ZTSN6icu_6711CEFinalizerE
	.section	.rodata._ZTSN6icu_6711CEFinalizerE,"aG",@progbits,_ZTSN6icu_6711CEFinalizerE,comdat
	.align 16
	.type	_ZTSN6icu_6711CEFinalizerE, @object
	.size	_ZTSN6icu_6711CEFinalizerE, 23
_ZTSN6icu_6711CEFinalizerE:
	.string	"N6icu_6711CEFinalizerE"
	.weak	_ZTIN6icu_6711CEFinalizerE
	.section	.data.rel.ro._ZTIN6icu_6711CEFinalizerE,"awG",@progbits,_ZTIN6icu_6711CEFinalizerE,comdat
	.align 8
	.type	_ZTIN6icu_6711CEFinalizerE, @object
	.size	_ZTIN6icu_6711CEFinalizerE, 24
_ZTIN6icu_6711CEFinalizerE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6711CEFinalizerE
	.quad	_ZTIN6icu_6720CollationDataBuilder10CEModifierE
	.section	.data.rel.ro
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_114BundleImporterE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_114BundleImporterE, 48
_ZTVN6icu_6712_GLOBAL__N_114BundleImporterE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_114BundleImporterE
	.quad	_ZN6icu_6712_GLOBAL__N_114BundleImporterD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_114BundleImporterD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6712_GLOBAL__N_114BundleImporter8getRulesEPKcS3_RNS_13UnicodeStringERS3_R10UErrorCode
	.weak	_ZTVN6icu_6716CollationBuilderE
	.section	.data.rel.ro._ZTVN6icu_6716CollationBuilderE,"awG",@progbits,_ZTVN6icu_6716CollationBuilderE,comdat
	.align 8
	.type	_ZTVN6icu_6716CollationBuilderE, @object
	.size	_ZTVN6icu_6716CollationBuilderE, 72
_ZTVN6icu_6716CollationBuilderE:
	.quad	0
	.quad	_ZTIN6icu_6716CollationBuilderE
	.quad	_ZN6icu_6716CollationBuilderD1Ev
	.quad	_ZN6icu_6716CollationBuilderD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6716CollationBuilder8addResetEiRKNS_13UnicodeStringERPKcR10UErrorCode
	.quad	_ZN6icu_6716CollationBuilder11addRelationEiRKNS_13UnicodeStringES3_S3_RPKcR10UErrorCode
	.quad	_ZN6icu_6716CollationBuilder20suppressContractionsERKNS_10UnicodeSetERPKcR10UErrorCode
	.quad	_ZN6icu_6716CollationBuilder8optimizeERKNS_10UnicodeSetERPKcR10UErrorCode
	.weak	_ZTVN6icu_6711CEFinalizerE
	.section	.data.rel.ro._ZTVN6icu_6711CEFinalizerE,"awG",@progbits,_ZTVN6icu_6711CEFinalizerE,comdat
	.align 8
	.type	_ZTVN6icu_6711CEFinalizerE, @object
	.size	_ZTVN6icu_6711CEFinalizerE, 56
_ZTVN6icu_6711CEFinalizerE:
	.quad	0
	.quad	_ZTIN6icu_6711CEFinalizerE
	.quad	_ZN6icu_6711CEFinalizerD1Ev
	.quad	_ZN6icu_6711CEFinalizerD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6711CEFinalizer10modifyCE32Ej
	.quad	_ZNK6icu_6711CEFinalizer8modifyCEEl
	.section	.rodata
	.align 32
	.type	_ZZ20ucol_getUnsafeSet_67E10cccpattern, @object
	.size	_ZZ20ucol_getUnsafeSet_67E10cccpattern, 50
_ZZ20ucol_getUnsafeSet_67E10cccpattern:
	.value	91
	.value	91
	.value	58
	.value	94
	.value	116
	.value	99
	.value	99
	.value	99
	.value	61
	.value	48
	.value	58
	.value	93
	.value	91
	.value	58
	.value	94
	.value	108
	.value	99
	.value	99
	.value	99
	.value	61
	.value	48
	.value	58
	.value	93
	.value	93
	.zero	2
	.globl	_ZN6icu_6716CollationBuilder11HAS_BEFORE3E
	.align 4
	.type	_ZN6icu_6716CollationBuilder11HAS_BEFORE3E, @object
	.size	_ZN6icu_6716CollationBuilder11HAS_BEFORE3E, 4
_ZN6icu_6716CollationBuilder11HAS_BEFORE3E:
	.long	32
	.globl	_ZN6icu_6716CollationBuilder11HAS_BEFORE2E
	.align 4
	.type	_ZN6icu_6716CollationBuilder11HAS_BEFORE2E, @object
	.size	_ZN6icu_6716CollationBuilder11HAS_BEFORE2E, 4
_ZN6icu_6716CollationBuilder11HAS_BEFORE2E:
	.long	64
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
