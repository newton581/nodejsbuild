	.file	"number_mapper.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider6charAtEii
	.type	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider6charAtEii, @function
_ZNK6icu_676number4impl30PropertiesAffixPatternProvider6charAtEii:
.LFB3646:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	andl	$512, %eax
	andl	$256, %esi
	je	.L4
	testl	%eax, %eax
	je	.L2
	addq	$136, %rdi
.L3:
	movzwl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L6
.L19:
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L7:
	movl	$-1, %r8d
	cmpl	%edx, %ecx
	jbe	.L1
	testb	$2, %al
	je	.L9
	addq	$10, %rdi
.L10:
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %r8d
.L1:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testl	%esi, %esi
	je	.L4
	movzwl	16(%rdi), %eax
	addq	$8, %rdi
	testw	%ax, %ax
	jns	.L19
.L6:
	movl	12(%rdi), %ecx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	200(%rdi), %rcx
	addq	$72, %rdi
	testl	%eax, %eax
	cmovne	%rcx, %rdi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	movq	24(%rdi), %rdi
	jmp	.L10
	.cfi_endproc
.LFE3646:
	.size	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider6charAtEii, .-_ZNK6icu_676number4impl30PropertiesAffixPatternProvider6charAtEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider6lengthEi
	.type	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider6lengthEi, @function
_ZNK6icu_676number4impl30PropertiesAffixPatternProvider6lengthEi:
.LFB3647:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	andl	$512, %eax
	andl	$256, %esi
	je	.L23
	testl	%eax, %eax
	je	.L21
	addq	$136, %rdi
.L22:
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L25
.L33:
	sarl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	testl	%esi, %esi
	je	.L23
	movswl	16(%rdi), %eax
	addq	$8, %rdi
	testw	%ax, %ax
	jns	.L33
.L25:
	movl	12(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	leaq	200(%rdi), %rdx
	addq	$72, %rdi
	testl	%eax, %eax
	cmovne	%rdx, %rdi
	jmp	.L22
	.cfi_endproc
.LFE3647:
	.size	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider6lengthEi, .-_ZNK6icu_676number4impl30PropertiesAffixPatternProvider6lengthEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider15hasCurrencySignEv
	.type	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider15hasCurrencySignEv, @function
_ZNK6icu_676number4impl30PropertiesAffixPatternProvider15hasCurrencySignEv:
.LFB3653:
	.cfi_startproc
	endbr64
	movzbl	264(%rdi), %eax
	ret
	.cfi_endproc
.LFE3653:
	.size	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider15hasCurrencySignEv, .-_ZNK6icu_676number4impl30PropertiesAffixPatternProvider15hasCurrencySignEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider7hasBodyEv
	.type	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider7hasBodyEv, @function
_ZNK6icu_676number4impl30PropertiesAffixPatternProvider7hasBodyEv:
.LFB3655:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3655:
	.size	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider7hasBodyEv, .-_ZNK6icu_676number4impl30PropertiesAffixPatternProvider7hasBodyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider9getStringEi
	.type	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider9getStringEi, @function
_ZNK6icu_676number4impl30PropertiesAffixPatternProvider9getStringEi:
.LFB3648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	andl	$512, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	andl	$256, %edx
	je	.L39
	testl	%eax, %eax
	je	.L37
	addq	$136, %rsi
.L38:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L39
	addq	$8, %rsi
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	200(%rsi), %rdx
	addq	$72, %rsi
	testl	%eax, %eax
	cmovne	%rdx, %rsi
	jmp	.L38
	.cfi_endproc
.LFE3648:
	.size	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider9getStringEi, .-_ZNK6icu_676number4impl30PropertiesAffixPatternProvider9getStringEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider19positiveHasPlusSignEv
	.type	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider19positiveHasPlusSignEv, @function
_ZNK6icu_676number4impl30PropertiesAffixPatternProvider19positiveHasPlusSignEv:
.LFB3650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-56(%rbp), %r13
	pushq	%rbx
	movq	%r13, %rdx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	movl	$0, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L53
.L49:
	leaq	-64(%rbp), %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	leaq	72(%rbx), %rdi
	movq	%r13, %rdx
	movl	$-2, %esi
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	movl	%eax, %r12d
	jmp	.L49
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3650:
	.size	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider19positiveHasPlusSignEv, .-_ZNK6icu_676number4impl30PropertiesAffixPatternProvider19positiveHasPlusSignEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider20negativeHasMinusSignEv
	.type	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider20negativeHasMinusSignEv, @function
_ZNK6icu_676number4impl30PropertiesAffixPatternProvider20negativeHasMinusSignEv:
.LFB3652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-56(%rbp), %r13
	pushq	%rbx
	movq	%r13, %rdx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$136, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	movl	$0, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L60
.L56:
	leaq	-64(%rbp), %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	leaq	200(%rbx), %rdi
	movq	%r13, %rdx
	movl	$-1, %esi
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	movl	%eax, %r12d
	jmp	.L56
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3652:
	.size	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider20negativeHasMinusSignEv, .-_ZNK6icu_676number4impl30PropertiesAffixPatternProvider20negativeHasMinusSignEv
	.section	.text._ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD2Ev,"axG",@progbits,_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD2Ev
	.type	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD2Ev, @function
_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD2Ev:
.LFB4833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-264(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	1368(%rdi), %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	.p2align 4,,10
	.p2align 3
.L63:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$272, %rbx
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L63
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	.cfi_endproc
.LFE4833:
	.size	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD2Ev, .-_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD2Ev
	.weak	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD1Ev
	.set	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD1Ev,_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD2Ev
	.section	.text._ZN6icu_676number4impl30PropertiesAffixPatternProviderD2Ev,"axG",@progbits,_ZN6icu_676number4impl30PropertiesAffixPatternProviderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl30PropertiesAffixPatternProviderD2Ev
	.type	_ZN6icu_676number4impl30PropertiesAffixPatternProviderD2Ev, @function
_ZN6icu_676number4impl30PropertiesAffixPatternProviderD2Ev:
.LFB4837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	200(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -200(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	.cfi_endproc
.LFE4837:
	.size	_ZN6icu_676number4impl30PropertiesAffixPatternProviderD2Ev, .-_ZN6icu_676number4impl30PropertiesAffixPatternProviderD2Ev
	.weak	_ZN6icu_676number4impl30PropertiesAffixPatternProviderD1Ev
	.set	_ZN6icu_676number4impl30PropertiesAffixPatternProviderD1Ev,_ZN6icu_676number4impl30PropertiesAffixPatternProviderD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode
	.type	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode, @function
_ZNK6icu_676number4impl30PropertiesAffixPatternProvider18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode:
.LFB3654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$8, %rsp
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	testb	%al, %al
	je	.L73
.L70:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	leaq	72(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	testb	%al, %al
	jne	.L70
	leaq	136(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	testb	%al, %al
	jne	.L70
	addq	$8, %rsp
	leaq	200(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	.cfi_endproc
.LFE3654:
	.size	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode, .-_ZNK6icu_676number4impl30PropertiesAffixPatternProvider18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider7hasBodyEv
	.type	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider7hasBodyEv, @function
_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider7hasBodyEv:
.LFB3665:
	.cfi_startproc
	endbr64
	movq	1368(%rdi), %rax
	leaq	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider7hasBodyEv(%rip), %rdx
	leaq	1368(%rdi), %r8
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L76
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE3665:
	.size	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider7hasBodyEv, .-_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider7hasBodyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider15hasCurrencySignEv
	.type	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider15hasCurrencySignEv, @function
_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider15hasCurrencySignEv:
.LFB3663:
	.cfi_startproc
	endbr64
	movq	1368(%rdi), %rax
	leaq	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider15hasCurrencySignEv(%rip), %rdx
	leaq	1368(%rdi), %r8
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L78
	movzbl	1632(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE3663:
	.size	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider15hasCurrencySignEv, .-_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider15hasCurrencySignEv
	.section	.text._ZN6icu_676number4impl30PropertiesAffixPatternProviderD0Ev,"axG",@progbits,_ZN6icu_676number4impl30PropertiesAffixPatternProviderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl30PropertiesAffixPatternProviderD0Ev
	.type	_ZN6icu_676number4impl30PropertiesAffixPatternProviderD0Ev, @function
_ZN6icu_676number4impl30PropertiesAffixPatternProviderD0Ev:
.LFB4839:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	200(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -200(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4839:
	.size	_ZN6icu_676number4impl30PropertiesAffixPatternProviderD0Ev, .-_ZN6icu_676number4impl30PropertiesAffixPatternProviderD0Ev
	.section	.text._ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD0Ev,"axG",@progbits,_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD0Ev
	.type	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD0Ev, @function
_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD0Ev:
.LFB4835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	1640(%rdi), %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	.p2align 4,,10
	.p2align 3
.L84:
	movq	-272(%rbx), %rax
	subq	$272, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, %r13
	jne	.L84
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4835:
	.size	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD0Ev, .-_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider9getStringEi
	.type	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider9getStringEi, @function
_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider9getStringEi:
.LFB3659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider9getStringEi(%rip), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	%dl, %eax
	movq	%rax, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	salq	$4, %rcx
	movq	8(%rsi,%rcx), %rax
	leaq	8(%rsi,%rcx), %r8
	movq	32(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L88
	movl	%edx, %eax
	andl	$512, %eax
	andl	$256, %edx
	je	.L91
	testl	%eax, %eax
	je	.L89
	leaq	144(%rsi,%rcx), %rsi
.L90:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L87:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L91
	leaq	16(%rsi,%rcx), %rsi
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	208(%rsi,%rcx), %rdx
	testl	%eax, %eax
	leaq	80(%rsi,%rcx), %rsi
	cmovne	%rdx, %rsi
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L87
.L102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3659:
	.size	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider9getStringEi, .-_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider9getStringEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider21hasNegativeSubpatternEv
	.type	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider21hasNegativeSubpatternEv, @function
_ZNK6icu_676number4impl30PropertiesAffixPatternProvider21hasNegativeSubpatternEv:
.LFB3651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$88, %rsp
	movswl	208(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movswl	80(%rdi), %eax
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L104
	testw	%dx, %dx
	js	.L105
	sarl	$5, %edx
	testw	%ax, %ax
	js	.L107
.L144:
	sarl	$5, %eax
.L108:
	cmpl	%edx, %eax
	jne	.L122
	testb	%cl, %cl
	je	.L142
.L122:
	movl	$1, %r12d
.L103:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L143
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	leaq	72(%rbx), %rsi
	leaq	200(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L104:
	movl	$1, %r12d
	testb	%cl, %cl
	je	.L103
	leaq	-112(%rbp), %r13
	movl	$1, %edx
	movl	$2147483647, %ecx
	leaq	136(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movswl	-104(%rbp), %edx
	testb	$1, %dl
	je	.L110
	movzbl	16(%rbx), %eax
	andl	$1, %eax
.L111:
	testb	%al, %al
	je	.L125
	movzwl	144(%rbx), %eax
	testw	%ax, %ax
	js	.L117
	movswl	%ax, %edx
	sarl	$5, %edx
.L118:
	movl	$1, %r12d
	testl	%edx, %edx
	je	.L116
	testb	$2, %al
	je	.L119
	addq	$146, %rbx
.L120:
	cmpw	$45, (%rbx)
	setne	%r12b
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L105:
	movl	212(%rdi), %edx
	testw	%ax, %ax
	jns	.L144
.L107:
	movl	84(%rbx), %eax
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L110:
	testw	%dx, %dx
	js	.L112
	sarl	$5, %edx
.L113:
	movzwl	16(%rbx), %ecx
	testw	%cx, %cx
	js	.L114
	movswl	%cx, %eax
	sarl	$5, %eax
.L115:
	cmpl	%edx, %eax
	jne	.L125
	andl	$1, %ecx
	je	.L145
.L125:
	movl	$1, %r12d
.L116:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L114:
	movl	20(%rbx), %eax
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L112:
	movl	-100(%rbp), %edx
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L117:
	movl	148(%rbx), %edx
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L119:
	movq	160(%rbx), %rbx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	8(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L111
.L143:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3651:
	.size	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider21hasNegativeSubpatternEv, .-_ZNK6icu_676number4impl30PropertiesAffixPatternProvider21hasNegativeSubpatternEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider6lengthEi
	.type	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider6lengthEi, @function
_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider6lengthEi:
.LFB3658:
	.cfi_startproc
	endbr64
	movzbl	%sil, %edx
	leaq	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider6lengthEi(%rip), %rcx
	movq	%rdx, %rax
	salq	$4, %rax
	addq	%rdx, %rax
	salq	$4, %rax
	movq	8(%rdi,%rax), %rdx
	leaq	8(%rdi,%rax), %r8
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L147
	movl	%esi, %ecx
	andl	$512, %ecx
	andl	$256, %esi
	je	.L150
	leaq	144(%rdi,%rax), %rdx
	testl	%ecx, %ecx
	je	.L160
.L149:
	movswl	8(%rdx), %eax
	testw	%ax, %ax
	js	.L152
.L161:
	sarl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	movl	12(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	16(%rdi,%rax), %rdx
	testl	%esi, %esi
	jne	.L149
.L150:
	leaq	208(%rdi,%rax), %rsi
	leaq	80(%rdi,%rax), %rdx
	testl	%ecx, %ecx
	cmovne	%rsi, %rdx
	movswl	8(%rdx), %eax
	testw	%ax, %ax
	jns	.L161
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L147:
	movq	%r8, %rdi
	jmp	*%rdx
	.cfi_endproc
.LFE3658:
	.size	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider6lengthEi, .-_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider6lengthEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider20negativeHasMinusSignEv
	.type	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider20negativeHasMinusSignEv, @function
_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider20negativeHasMinusSignEv:
.LFB3662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider20negativeHasMinusSignEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	1368(%rdi), %rax
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L163
	leaq	-56(%rbp), %r13
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	movl	$-1, %esi
	movl	$0, -56(%rbp)
	leaq	1504(%rdi), %rdi
	movq	%r13, %rdx
	movq	%rax, -64(%rbp)
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L168
.L164:
	leaq	-64(%rbp), %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
.L162:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L169
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	leaq	1568(%rbx), %rdi
	movq	%r13, %rdx
	movl	$-1, %esi
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	movl	%eax, %r12d
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L163:
	addq	$1368, %rdi
	call	*%rax
	movl	%eax, %r12d
	jmp	.L162
.L169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3662:
	.size	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider20negativeHasMinusSignEv, .-_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider20negativeHasMinusSignEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider19positiveHasPlusSignEv
	.type	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider19positiveHasPlusSignEv, @function
_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider19positiveHasPlusSignEv:
.LFB3660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider19positiveHasPlusSignEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	1368(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L171
	leaq	-56(%rbp), %r13
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	movl	$-2, %esi
	movl	$0, -56(%rbp)
	leaq	1376(%rdi), %rdi
	movq	%r13, %rdx
	movq	%rax, -64(%rbp)
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L176
.L172:
	leaq	-64(%rbp), %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
.L170:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L177
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	leaq	1440(%rbx), %rdi
	movq	%r13, %rdx
	movl	$-2, %esi
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	movl	%eax, %r12d
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L171:
	addq	$1368, %rdi
	call	*%rax
	movl	%eax, %r12d
	jmp	.L170
.L177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3660:
	.size	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider19positiveHasPlusSignEv, .-_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider19positiveHasPlusSignEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider6charAtEii
	.type	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider6charAtEii, @function
_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider6charAtEii:
.LFB3657:
	.cfi_startproc
	endbr64
	movzbl	%sil, %ecx
	leaq	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider6charAtEii(%rip), %r8
	movq	%rcx, %rax
	salq	$4, %rax
	addq	%rcx, %rax
	salq	$4, %rax
	movq	8(%rdi,%rax), %rcx
	leaq	8(%rdi,%rax), %r9
	movq	16(%rcx), %rcx
	cmpq	%r8, %rcx
	jne	.L179
	movl	%esi, %ecx
	andl	$512, %ecx
	andl	$256, %esi
	je	.L182
	testl	%ecx, %ecx
	je	.L180
	leaq	144(%rdi,%rax), %rax
.L181:
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	js	.L184
.L196:
	movswl	%cx, %esi
	sarl	$5, %esi
.L185:
	movl	$-1, %r8d
	cmpl	%edx, %esi
	jbe	.L178
	andl	$2, %ecx
	je	.L187
	addq	$10, %rax
.L188:
	movslq	%edx, %rdx
	movzwl	(%rax,%rdx,2), %r8d
.L178:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	testl	%esi, %esi
	je	.L182
	leaq	16(%rdi,%rax), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	jns	.L196
.L184:
	movl	12(%rax), %esi
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L182:
	leaq	208(%rdi,%rax), %rsi
	testl	%ecx, %ecx
	leaq	80(%rdi,%rax), %rax
	cmovne	%rsi, %rax
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L187:
	movq	24(%rax), %rax
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L179:
	movq	%r9, %rdi
	jmp	*%rcx
	.cfi_endproc
.LFE3657:
	.size	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider6charAtEii, .-_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider6charAtEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode
	.type	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode, @function
_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode:
.LFB3664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	leaq	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode(%rip), %rdx
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	1368(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L198
	leaq	1376(%rdi), %rdi
	movq	%r13, %rdx
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	testb	%al, %al
	je	.L203
.L200:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	leaq	1440(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	testb	%al, %al
	jne	.L200
	leaq	1504(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	testb	%al, %al
	jne	.L200
	addq	$8, %rsp
	leaq	1568(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rdx
	addq	$1368, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE3664:
	.size	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode, .-_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider21hasNegativeSubpatternEv
	.type	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider21hasNegativeSubpatternEv, @function
_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider21hasNegativeSubpatternEv:
.LFB3661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider21hasNegativeSubpatternEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	1368(%rdi), %rax
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L205
	movswl	1448(%rdi), %eax
	movswl	1576(%rdi), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L206
	testw	%dx, %dx
	js	.L207
	sarl	$5, %edx
.L208:
	testw	%ax, %ax
	js	.L209
	sarl	$5, %eax
.L210:
	cmpl	%edx, %eax
	jne	.L225
	testb	%cl, %cl
	je	.L245
.L225:
	movl	$1, %r12d
.L204:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L246
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	leaq	1440(%rbx), %rsi
	leaq	1568(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L206:
	movl	$1, %r12d
	testb	%cl, %cl
	je	.L204
	leaq	-112(%rbp), %r13
	movl	$1, %edx
	movl	$2147483647, %ecx
	leaq	1504(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movswl	-104(%rbp), %edx
	testb	$1, %dl
	je	.L212
	movzbl	1384(%rbx), %eax
	andl	$1, %eax
.L213:
	testb	%al, %al
	je	.L228
	movzwl	1512(%rbx), %eax
	testw	%ax, %ax
	js	.L219
	movswl	%ax, %edx
	sarl	$5, %edx
.L220:
	movl	$1, %r12d
	testl	%edx, %edx
	je	.L218
	testb	$2, %al
	je	.L221
	addq	$1514, %rbx
.L222:
	cmpw	$45, (%rbx)
	setne	%r12b
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L212:
	testw	%dx, %dx
	js	.L214
	sarl	$5, %edx
.L215:
	movzwl	1384(%rbx), %eax
	testw	%ax, %ax
	js	.L216
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L217:
	testb	$1, %al
	jne	.L228
	cmpl	%edx, %ecx
	je	.L247
.L228:
	movl	$1, %r12d
.L218:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L205:
	addq	$1368, %rdi
	call	*%rax
	movl	%eax, %r12d
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L209:
	movl	1452(%rbx), %eax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L207:
	movl	1580(%rdi), %edx
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L216:
	movl	1388(%rbx), %ecx
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L214:
	movl	-100(%rbp), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L219:
	movl	1516(%rbx), %edx
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L221:
	movq	1528(%rbx), %rbx
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L247:
	leaq	1376(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L213
.L246:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3661:
	.size	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider21hasNegativeSubpatternEv, .-_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider21hasNegativeSubpatternEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB4159:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4159:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB4162:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L261
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L249
	cmpb	$0, 12(%rbx)
	jne	.L262
.L253:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L249:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L253
	.cfi_endproc
.LFE4162:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB4165:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L265
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4165:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB4168:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L268
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE4168:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB4170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L274
.L270:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L275
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L275:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4170:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB4171:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4171:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB4172:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4172:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB4173:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4173:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4174:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4174:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4175:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4175:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB4176:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L291
	testl	%edx, %edx
	jle	.L291
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L294
.L283:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L283
	.cfi_endproc
.LFE4176:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB4177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L298
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L298
	testl	%r12d, %r12d
	jg	.L305
	cmpb	$0, 12(%rbx)
	jne	.L306
.L300:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L300
.L306:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L298:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4177:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB4178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L308
	movq	(%rdi), %r8
.L309:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L312
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L312
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L312:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4178:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB4179:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L319
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4179:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB4180:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4180:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB4181:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4181:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB4182:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4182:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB4184:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4184:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB4186:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4186:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	""
	.string	""
	.align 2
.LC1:
	.string	"-"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl30PropertiesAffixPatternProvider5setToERKNS1_23DecimalFormatPropertiesER10UErrorCode
	.type	_ZN6icu_676number4impl30PropertiesAffixPatternProvider5setToERKNS1_23DecimalFormatPropertiesER10UErrorCode, @function
_ZN6icu_676number4impl30PropertiesAffixPatternProvider5setToERKNS1_23DecimalFormatPropertiesER10UErrorCode:
.LFB3645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	leaq	-256(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-384(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	$480, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$488, %rsp
	movq	%rdx, -456(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, 265(%rdi)
	leaq	-448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -464(%rbp)
	call	_ZN6icu_676number4impl10AffixUtils6escapeERKNS_13UnicodeStringE@PLT
	leaq	608(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl10AffixUtils6escapeERKNS_13UnicodeStringE@PLT
	leaq	128(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl10AffixUtils6escapeERKNS_13UnicodeStringE@PLT
	leaq	256(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl10AffixUtils6escapeERKNS_13UnicodeStringE@PLT
	leaq	544(%r12), %rax
	leaq	8(%rbx), %r8
	movq	%rax, -472(%rbp)
	leaq	672(%r12), %rax
	movq	%rax, -480(%rbp)
	leaq	192(%r12), %rax
	movq	%rax, -496(%rbp)
	leaq	320(%r12), %rax
	movq	%rax, -488(%rbp)
	testb	$1, 488(%r12)
	je	.L347
	testb	$1, 552(%r12)
	je	.L348
	leaq	-128(%rbp), %r9
	leaq	.LC0(%rip), %rsi
	movq	%r8, -512(%rbp)
	movq	%r9, %rdi
	movq	%r9, -504(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-504(%rbp), %r9
	movq	-512(%rbp), %r8
	movq	%r9, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-504(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L327:
	leaq	72(%rbx), %r8
	testb	$1, 616(%r12)
	je	.L349
.L329:
	testb	$1, 680(%r12)
	jne	.L331
	leaq	672(%r12), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L330:
	leaq	136(%rbx), %r8
	testb	$1, 136(%r12)
	je	.L350
	testb	$1, 200(%r12)
	je	.L351
	testb	$1, 552(%r12)
	je	.L335
	leaq	-128(%rbp), %r9
	leaq	.LC1(%rip), %rsi
	movq	%r8, -512(%rbp)
	movq	%r9, %rdi
	movq	%r9, -504(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-504(%rbp), %r9
	movq	-512(%rbp), %r8
	movq	%r9, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-504(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L333:
	leaq	200(%rbx), %r8
	testb	$1, 264(%r12)
	je	.L352
	testb	$1, 328(%r12)
	je	.L353
	testb	$1, 680(%r12)
	movq	%r8, -512(%rbp)
	leaq	-128(%rbp), %r9
	je	.L340
	movq	%r9, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%r9, -504(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-504(%rbp), %r9
	movq	-512(%rbp), %r8
.L341:
	movq	%r9, %rsi
	movq	%r8, %rdi
	movq	%r9, -504(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-504(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L338:
	movq	-456(%rbp), %rsi
	movq	-472(%rbp), %rdi
	call	_ZN6icu_676number4impl10AffixUtils18hasCurrencySymbolsERKNS_13UnicodeStringER10UErrorCode@PLT
	testb	%al, %al
	je	.L342
.L344:
	movl	$1, %eax
.L343:
	movb	%al, 264(%rbx)
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-464(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L354
	addq	$488, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	movq	%r8, %rdi
	leaq	544(%r12), %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	72(%rbx), %r8
	testb	$1, 616(%r12)
	jne	.L329
.L349:
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L331:
	leaq	-128(%rbp), %r9
	leaq	.LC0(%rip), %rsi
	movq	%r8, -512(%rbp)
	movq	%r9, %rdi
	movq	%r9, -504(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-504(%rbp), %r9
	movq	-512(%rbp), %r8
	movq	%r9, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-504(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L347:
	movq	-464(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L342:
	movq	-456(%rbp), %rsi
	movq	-480(%rbp), %rdi
	call	_ZN6icu_676number4impl10AffixUtils18hasCurrencySymbolsERKNS_13UnicodeStringER10UErrorCode@PLT
	testb	%al, %al
	jne	.L344
	movq	-456(%rbp), %rsi
	movq	-496(%rbp), %rdi
	call	_ZN6icu_676number4impl10AffixUtils18hasCurrencySymbolsERKNS_13UnicodeStringER10UErrorCode@PLT
	testb	%al, %al
	jne	.L344
	movq	-456(%rbp), %rsi
	movq	-488(%rbp), %rdi
	call	_ZN6icu_676number4impl10AffixUtils18hasCurrencySymbolsERKNS_13UnicodeStringER10UErrorCode@PLT
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L350:
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L335:
	leaq	-192(%rbp), %r9
	leaq	.LC1(%rip), %rsi
	movq	%r8, -520(%rbp)
	movq	%r9, %rdi
	movq	%r9, -504(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-504(%rbp), %r9
	leaq	-128(%rbp), %r10
	leaq	544(%r12), %rdx
	movq	%r10, %rdi
	movq	%r10, -504(%rbp)
	movq	%r9, %rsi
	movq	%r9, -512(%rbp)
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movq	-504(%rbp), %r10
	movq	-520(%rbp), %r8
	movq	%r10, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-504(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-512(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L340:
	movq	-480(%rbp), %rsi
	movq	%r9, %rdi
	movq	%r9, -504(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-512(%rbp), %r8
	movq	-504(%rbp), %r9
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L351:
	leaq	192(%r12), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L353:
	movq	-488(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L338
.L354:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3645:
	.size	_ZN6icu_676number4impl30PropertiesAffixPatternProvider5setToERKNS1_23DecimalFormatPropertiesER10UErrorCode, .-_ZN6icu_676number4impl30PropertiesAffixPatternProvider5setToERKNS1_23DecimalFormatPropertiesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider17getStringInternalEi
	.type	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider17getStringInternalEi, @function
_ZNK6icu_676number4impl30PropertiesAffixPatternProvider17getStringInternalEi:
.LFB3649:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	andl	$512, %edx
	andl	$256, %esi
	je	.L358
	testl	%edx, %edx
	je	.L356
	leaq	136(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	testl	%esi, %esi
	je	.L358
	leaq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	leaq	200(%rdi), %rax
	addq	$72, %rdi
	testl	%edx, %edx
	cmove	%rdi, %rax
	ret
	.cfi_endproc
.LFE3649:
	.size	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider17getStringInternalEi, .-_ZNK6icu_676number4impl30PropertiesAffixPatternProvider17getStringInternalEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProvider5setToERKNS_18CurrencyPluralInfoERKNS1_23DecimalFormatPropertiesER10UErrorCode
	.type	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProvider5setToERKNS_18CurrencyPluralInfoERKNS1_23DecimalFormatPropertiesER10UErrorCode, @function
_ZN6icu_676number4impl31CurrencyPluralInfoAffixProvider5setToERKNS_18CurrencyPluralInfoERKNS1_23DecimalFormatPropertiesER10UErrorCode:
.LFB3656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1000, %rsp
	movq	%rsi, -960(%rbp)
	leaq	16(%rdx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movb	$0, 1640(%rdi)
	movq	%rax, -816(%rbp)
	movzbl	8(%rdx), %eax
	movb	%al, -808(%rbp)
	leaq	-800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1008(%rbp)
	call	_ZN6icu_6712CurrencyUnitC1ERKS0_@PLT
	movq	48(%rbx), %r15
	movq	$0, -768(%rbp)
	testq	%r15, %r15
	je	.L367
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L368
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6718CurrencyPluralInfoC1ERKS0_@PLT
.L368:
	movq	-768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L369
	movq	(%rdi), %rax
	call	*8(%rax)
.L369:
	movq	%r13, -768(%rbp)
.L367:
	movq	56(%rbx), %rax
	movl	72(%rbx), %edx
	addq	$8, %r12
	leaq	128(%rbx), %rsi
	movq	64(%rbx), %rcx
	movdqu	80(%rbx), %xmm1
	leaq	-880(%rbp), %r13
	leaq	-816(%rbp), %r15
	movq	%rax, -760(%rbp)
	movzbl	76(%rbx), %eax
	movdqu	96(%rbx), %xmm2
	movl	%edx, -744(%rbp)
	movb	%al, -740(%rbp)
	movl	120(%rbx), %eax
	movq	112(%rbx), %rdx
	movq	%rcx, -752(%rbp)
	movl	%eax, -696(%rbp)
	leaq	-688(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -704(%rbp)
	movaps	%xmm1, -736(%rbp)
	movaps	%xmm2, -720(%rbp)
	movq	%rax, -992(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	-624(%rbp), %rax
	leaq	192(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -984(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	-560(%rbp), %rax
	leaq	256(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -976(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	-496(%rbp), %rax
	leaq	320(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -968(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	384(%rbx), %rax
	leaq	392(%rbx), %rsi
	movq	%rax, -432(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1000(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	456(%rbx), %eax
	leaq	480(%rbx), %rsi
	movw	%ax, -360(%rbp)
	movq	460(%rbx), %rax
	movq	%rax, -356(%rbp)
	movzwl	468(%rbx), %eax
	movw	%ax, -348(%rbp)
	movl	472(%rbx), %eax
	movl	%eax, -344(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1040(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	-272(%rbp), %rax
	leaq	544(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -1032(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	-208(%rbp), %rax
	leaq	608(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -1024(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	-144(%rbp), %rax
	leaq	672(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -1016(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	744(%rbx), %rax
	movsd	736(%rbx), %xmm0
	xorl	%r8d, %r8d
	movq	%rax, -72(%rbp)
	movl	752(%rbx), %eax
	movsd	%xmm0, -80(%rbp)
	movl	%eax, -64(%rbp)
	movzbl	756(%rbx), %eax
	leaq	-944(%rbp), %rbx
	movb	%al, -60(%rbp)
	.p2align 4,,10
	.p2align 3
.L370:
	movl	%r8d, %edi
	movl	%r8d, -948(%rbp)
	call	_ZN6icu_6714StandardPlural10getKeywordENS0_4FormE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -944(%rbp)
	movl	$2, %eax
	movw	%ax, -936(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKc@PLT
	movq	-960(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	_ZNK6icu_6718CurrencyPluralInfo24getCurrencyPluralPatternERKNS_13UnicodeStringERS1_@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN6icu_676number4impl30PropertiesAffixPatternProvider5setToERKNS1_23DecimalFormatPropertiesER10UErrorCode
	movq	%rbx, %rdi
	addq	$272, %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-948(%rbp), %r8d
	addl	$1, %r8d
	cmpl	$6, %r8d
	jne	.L370
	movq	-1016(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-1024(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-1032(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-1040(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-1000(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-968(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-976(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-984(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-992(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L371
	movq	(%rdi), %rax
	call	*8(%rax)
.L371:
	movq	-1008(%rbp), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	addq	$1000, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L387:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3656:
	.size	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProvider5setToERKNS_18CurrencyPluralInfoERKNS1_23DecimalFormatPropertiesER10UErrorCode, .-_ZN6icu_676number4impl31CurrencyPluralInfoAffixProvider5setToERKNS_18CurrencyPluralInfoERKNS1_23DecimalFormatPropertiesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl20NumberPropertyMapper8oldToNewERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseEPS3_R10UErrorCode
	.type	_ZN6icu_676number4impl20NumberPropertyMapper8oldToNewERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseEPS3_R10UErrorCode, @function
_ZN6icu_676number4impl20NumberPropertyMapper8oldToNewERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseEPS3_R10UErrorCode:
.LFB3639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$456, %rsp
	movq	%r8, -464(%rbp)
	movq	%r9, -432(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$2, 4(%rdi)
	movl	$0, 12(%rdi)
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %ecx
	movl	$-1, %esi
	movabsq	$30064771077, %rax
	movw	%cx, 100(%r12)
	pxor	%xmm0, %xmm0
	leaq	216(%r12), %rdi
	movw	%si, 124(%r12)
	movq	%rax, 152(%r12)
	movl	$0, 64(%r12)
	movl	$4, 96(%r12)
	movl	$-2, 112(%r12)
	movb	$0, 132(%r12)
	movl	$0, 136(%r12)
	movq	$0, 144(%r12)
	movl	$2, 160(%r12)
	movl	$0, 168(%r12)
	movq	$0, 176(%r12)
	movl	$0, 184(%r12)
	movl	$3, 208(%r12)
	movups	%xmm0, 192(%r12)
	call	_ZN6icu_676LocaleC1Ev@PLT
	leaq	-288(%rbp), %rax
	leaq	1872(%r13), %rsi
	movq	%rax, %rdi
	movq	%rax, -448(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	136(%r12), %rdi
	movq	%r13, %rsi
	movq	%rbx, %r13
	call	_ZN6icu_676number4impl14SymbolsWrapper5setToERKNS_20DecimalFormatSymbolsE@PLT
	movq	48(%r14), %rdi
	testq	%rdi, %rdi
	je	.L391
	call	_ZNK6icu_6718CurrencyPluralInfo14getPluralRulesEv@PLT
	movq	48(%r14), %rsi
	movq	%rax, 200(%r12)
	testq	%rsi, %rsi
	je	.L391
	movb	$1, 265(%rbx)
	movq	-432(%rbp), %rcx
	leaq	272(%rbx), %rbx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProvider5setToERKNS_18CurrencyPluralInfoERKNS1_23DecimalFormatPropertiesER10UErrorCode
	cmpb	$0, 1912(%r13)
	cmove	%rbx, %r13
.L390:
	cmpb	$0, 8(%r14)
	movq	%r13, 192(%r12)
	je	.L392
	cmpq	$0, 48(%r14)
	je	.L503
.L392:
	leaq	-320(%rbp), %rax
	movq	-432(%rbp), %rcx
	movq	-448(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN6icu_676number4impl15resolveCurrencyERKNS1_23DecimalFormatPropertiesERKNS_6LocaleER10UErrorCode@PLT
	xorl	%r8d, %r8d
	cmpb	$0, 56(%r14)
	jne	.L395
	movl	60(%r14), %r8d
.L395:
	movq	-440(%rbp), %rsi
	movq	%r15, %rdi
	movl	%r8d, -456(%rbp)
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movl	100(%r14), %eax
	movl	88(%r14), %ebx
	cmpb	$0, 744(%r14)
	movl	108(%r14), %r13d
	movl	%eax, -420(%rbp)
	movl	112(%r14), %eax
	movl	84(%r14), %r15d
	movl	-456(%rbp), %r8d
	movsd	736(%r14), %xmm0
	movl	%eax, -424(%rbp)
	movl	92(%r14), %eax
	movl	%eax, -452(%rbp)
	jne	.L398
	movl	-420(%rbp), %r9d
	movl	748(%r14), %eax
	movl	-424(%rbp), %ecx
	andl	-452(%rbp), %ecx
	andl	%r15d, %r9d
	cmpl	$-1, -420(%rbp)
	movl	%eax, -468(%rbp)
	je	.L400
.L512:
	cmpl	$-1, %r15d
	jne	.L445
	movq	-432(%rbp), %rdx
	movl	%r8d, %esi
	leaq	-300(%rbp), %rdi
	movl	%r9d, -472(%rbp)
	movl	%ecx, -480(%rbp)
	movl	%r8d, -456(%rbp)
	movsd	%xmm0, -488(%rbp)
	call	ucurr_getDefaultFractionDigitsForUsage_67@PLT
	movl	-420(%rbp), %edx
	movl	-456(%rbp), %r8d
	movl	-480(%rbp), %ecx
	movsd	-488(%rbp), %xmm0
	cmpl	%edx, %eax
	movl	-472(%rbp), %r9d
	cmovl	%edx, %eax
	movl	%eax, %r15d
.L445:
	movzbl	56(%r14), %edx
	movl	$1, %eax
.L396:
	testl	%r13d, %r13d
	jne	.L402
.L515:
	movl	-420(%rbp), %esi
	testl	%r15d, %r15d
	je	.L403
	testl	%esi, %esi
	js	.L453
	orl	%ebx, %esi
	jne	.L404
	movl	$1, -420(%rbp)
	testl	%r15d, %r15d
	js	.L504
.L444:
	movl	-420(%rbp), %edi
	cmpl	%edi, %r15d
	cmovl	%edi, %r15d
	movl	%r15d, -456(%rbp)
.L406:
	cmpl	$1000, %ebx
	movl	$-1, %esi
	cmovnb	%esi, %ebx
.L407:
	movl	$0, -416(%rbp)
	testb	%dl, %dl
	je	.L505
.L410:
	ucomisd	.LC2(%rip), %xmm0
	jp	.L466
	jne	.L466
	cmpl	$-1, %ecx
	je	.L415
	movl	-424(%rbp), %ecx
	movl	$999, %eax
	movl	$1, %edx
	cmpl	$999, %ecx
	cmovg	%eax, %ecx
	testl	%ecx, %ecx
	cmovg	%ecx, %edx
	movl	-452(%rbp), %ecx
	movl	%edx, -424(%rbp)
	testl	%ecx, %ecx
	js	.L458
	cmpl	%ecx, %edx
	jg	.L459
	cmpl	$999, %ecx
	movl	%edx, %esi
	cmovle	%ecx, %eax
	movl	%eax, -452(%rbp)
.L416:
	movl	-452(%rbp), %edx
	leaq	-352(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_676number9Precision20constructSignificantEii@PLT
	movq	-336(%rbp), %rax
	movdqa	-352(%rbp), %xmm4
	movq	%rax, -400(%rbp)
	movl	-328(%rbp), %eax
	movaps	%xmm4, -416(%rbp)
	movl	%eax, -392(%rbp)
.L411:
	movl	-416(%rbp), %edx
	testl	%edx, %edx
	je	.L418
	movl	-468(%rbp), %edx
	movq	-400(%rbp), %rax
	movdqa	-416(%rbp), %xmm1
	movl	%edx, -392(%rbp)
	movq	%rax, 80(%r12)
	movl	%edx, 88(%r12)
	movups	%xmm1, 64(%r12)
.L418:
	movzbl	67(%r14), %ecx
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_676number12IntegerWidthC1Essb@PLT
	movq	-352(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, 124(%r12)
	movzbl	-344(%rbp), %eax
	movb	%al, 132(%r12)
	call	_ZN6icu_676number4impl7Grouper13forPropertiesERKNS1_23DecimalFormatPropertiesE@PLT
	movq	%rax, 100(%r12)
	movl	68(%r14), %eax
	movl	%edx, 108(%r12)
	testl	%eax, %eax
	jle	.L419
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl6Padder13forPropertiesERKNS1_23DecimalFormatPropertiesE@PLT
	movq	%rax, 112(%r12)
	movl	%edx, 120(%r12)
.L419:
	movzbl	65(%r14), %eax
	movl	96(%r14), %ecx
	movl	%eax, 160(%r12)
	movzbl	756(%r14), %eax
	movl	%eax, 156(%r12)
	cmpl	$-1, %ecx
	je	.L420
	cmpl	$8, %ebx
	jg	.L506
	cmpl	%r13d, %ebx
	jle	.L467
	cmpl	$1, %r13d
	jle	.L467
	movl	$1, %edi
	movl	$1, %r13d
	call	_ZN6icu_676number12IntegerWidth10zeroFillToEi@PLT
	leaq	-384(%rbp), %rdi
	movl	%ebx, %esi
	movl	%edx, -376(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZN6icu_676number12IntegerWidth10truncateAtEi@PLT
	movl	96(%r14), %ecx
	movq	%rax, -352(%rbp)
	movq	%rax, 124(%r12)
	movl	%edx, -344(%rbp)
	movb	%dl, 132(%r12)
	xorl	%edx, %edx
	cmpl	$1, %ebx
	sete	%dl
.L422:
	movzbl	66(%r14), %r8d
	movswl	%cx, %ecx
	movsbl	%bl, %esi
	movq	%r15, %rdi
	call	_ZN6icu_676number18ScientificNotationC1Eabs18UNumberSignDisplay@PLT
	movq	-352(%rbp), %rax
	cmpl	$2, 64(%r12)
	movq	%rax, 4(%r12)
	movl	-344(%rbp), %eax
	movl	%eax, 12(%r12)
	je	.L507
.L420:
	cmpb	$0, (%r14)
	jne	.L430
	cmpl	$1, 4(%r14)
	je	.L508
	call	_ZN6icu_676number8Notation12compactShortEv@PLT
	movq	%rax, 4(%r12)
	movl	%edx, 12(%r12)
.L432:
	movq	$0, 192(%r12)
.L430:
	movl	120(%r14), %esi
	movl	116(%r14), %eax
	addl	80(%r14), %esi
	je	.L433
	cmpl	$1, %eax
	je	.L433
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN6icu_676number5Scale21byDoubleAndPowerOfTenEdi@PLT
.L434:
	leaq	168(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_676number5ScaleaSEOS1_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	-464(%rbp), %r14
	testq	%r14, %r14
	je	.L437
	movq	-440(%rbp), %rsi
	leaq	16(%r14), %rdi
	call	_ZN6icu_6712CurrencyUnitaSERKS0_@PLT
	movl	$2147483647, %eax
	cmpl	$-1, %ebx
	movl	-468(%rbp), %edx
	cmove	%eax, %ebx
	movl	-416(%rbp), %eax
	movb	$0, 8(%r14)
	movl	%edx, 748(%r14)
	movb	$0, 744(%r14)
	movl	%r13d, 108(%r14)
	movl	%ebx, 88(%r14)
	cmpl	$8, %eax
	je	.L509
	movq	-400(%rbp), %rdx
	movdqa	-416(%rbp), %xmm2
	movq	%rdx, -368(%rbp)
	movl	-392(%rbp), %edx
	movaps	%xmm2, -384(%rbp)
	movl	%edx, -360(%rbp)
	movzwl	-400(%rbp), %edx
.L440:
	cmpl	$2, %eax
	je	.L510
	leal	-5(%rax), %ecx
	cmpl	$2, %ecx
	ja	.L443
	movswl	%dx, %eax
	movsd	-376(%rbp), %xmm0
	movl	%eax, -420(%rbp)
	movl	%eax, -456(%rbp)
.L442:
	movq	-464(%rbp), %rax
	movl	-420(%rbp), %ebx
	movl	%ebx, 100(%rax)
	movl	-456(%rbp), %ebx
	movsd	%xmm0, 736(%rax)
	movl	%ebx, 84(%rax)
	movl	-424(%rbp), %ebx
	movl	%ebx, 112(%rax)
	movl	-452(%rbp), %ebx
	movl	%ebx, 92(%rax)
.L437:
	movq	-440(%rbp), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	-448(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L511
	addq	$456, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	movq	-432(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl30PropertiesAffixPatternProvider5setToERKNS1_23DecimalFormatPropertiesER10UErrorCode
	movb	$1, 1912(%rbx)
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L398:
	movl	-420(%rbp), %r9d
	movl	-424(%rbp), %ecx
	movl	$4, -468(%rbp)
	andl	-452(%rbp), %ecx
	andl	%r15d, %r9d
	cmpl	$-1, -420(%rbp)
	jne	.L512
.L400:
	movq	-432(%rbp), %rdx
	movl	%r8d, %esi
	leaq	-300(%rbp), %rdi
	movl	%r9d, -472(%rbp)
	movl	%ecx, -480(%rbp)
	movl	%r8d, -456(%rbp)
	movsd	%xmm0, -488(%rbp)
	call	ucurr_getDefaultFractionDigitsForUsage_67@PLT
	cmpl	$-1, %r15d
	movl	-456(%rbp), %r8d
	movl	-480(%rbp), %ecx
	movl	%eax, -420(%rbp)
	movsd	-488(%rbp), %xmm0
	movl	-472(%rbp), %r9d
	je	.L513
	cmpl	%r15d, %eax
	cmovg	%r15d, %eax
	movl	%eax, -420(%rbp)
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L503:
	cmpb	$0, 56(%r14)
	je	.L392
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L392
	movb	%al, -420(%rbp)
	movq	-448(%rbp), %rdx
	leaq	-320(%rbp), %rax
	movq	%r14, %rsi
	movq	-432(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN6icu_676number4impl15resolveCurrencyERKNS1_23DecimalFormatPropertiesERKNS_6LocaleER10UErrorCode@PLT
	movzbl	56(%r14), %edx
	movzbl	-420(%rbp), %eax
	testb	%dl, %dl
	je	.L514
	xorl	%r8d, %r8d
.L394:
	movl	100(%r14), %esi
	movl	112(%r14), %ecx
	movl	$4, -468(%rbp)
	movl	92(%r14), %edi
	movl	84(%r14), %r15d
	movl	88(%r14), %ebx
	movl	108(%r14), %r13d
	movl	%esi, -420(%rbp)
	movl	%ecx, -424(%rbp)
	andl	%r15d, %esi
	andl	%edi, %ecx
	cmpb	$0, 744(%r14)
	movl	%edi, -452(%rbp)
	movl	%esi, %r9d
	movsd	736(%r14), %xmm0
	jne	.L396
	movl	748(%r14), %eax
	movl	%eax, -468(%rbp)
	xorl	%eax, %eax
	testl	%r13d, %r13d
	je	.L515
	.p2align 4,,10
	.p2align 3
.L402:
	movl	-420(%rbp), %edi
	movl	$0, %esi
	movl	$-1, -456(%rbp)
	testl	%edi, %edi
	cmovns	%edi, %esi
	movl	%esi, -420(%rbp)
	testl	%r15d, %r15d
	jns	.L450
.L408:
	leal	-1(%r13), %esi
	cmpl	$999, %esi
	movl	$1, %esi
	cmovnb	%esi, %r13d
	testl	%ebx, %ebx
	js	.L456
	cmpl	%ebx, %r13d
	jg	.L457
	cmpl	$1000, %ebx
	movl	$-1, %esi
	movl	$0, -416(%rbp)
	cmovge	%esi, %ebx
	testb	%dl, %dl
	jne	.L410
.L505:
	leaq	-384(%rbp), %r9
	movl	%r8d, %esi
	movq	%r9, %rdi
	movq	%r9, -480(%rbp)
	leaq	-352(%rbp), %r15
	call	_ZN6icu_676number9Precision17constructCurrencyE14UCurrencyUsage@PLT
	movq	-480(%rbp), %r9
	movq	-440(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r9, %rsi
	call	_ZNK6icu_676number17CurrencyPrecision12withCurrencyERKNS_12CurrencyUnitE@PLT
	movq	-336(%rbp), %rax
	movdqa	-352(%rbp), %xmm3
	movq	%rax, -400(%rbp)
	movl	-328(%rbp), %eax
	movaps	%xmm3, -416(%rbp)
	movl	%eax, -392(%rbp)
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L433:
	testl	%esi, %esi
	jne	.L516
	cmpl	$1, %eax
	je	.L436
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN6icu_676number5Scale8byDoubleEd@PLT
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L403:
	testl	%esi, %esi
	cmovs	%r15d, %esi
	movl	%esi, -420(%rbp)
.L450:
	movl	-420(%rbp), %esi
	cmpl	%esi, %r15d
	cmovl	%esi, %r15d
	movl	%r15d, -456(%rbp)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L456:
	movl	$-1, %ebx
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L467:
	xorl	%edx, %edx
	cmpl	%r13d, %ebx
	sete	%dl
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L507:
	movl	108(%r14), %esi
	movl	84(%r14), %edx
	movl	%esi, %eax
	orl	%edx, %eax
	je	.L517
	movl	100(%r14), %eax
	movl	%esi, %ecx
	orl	%eax, %ecx
	je	.L518
	addl	%esi, %edx
	cmpl	%esi, 88(%r14)
	jle	.L428
	cmpl	$1, %esi
	movl	$1, %ecx
	cmovg	%ecx, %esi
.L428:
	addl	%eax, %esi
	movq	%r15, %rdi
	call	_ZN6icu_676number9Precision20constructSignificantEii@PLT
	movq	-336(%rbp), %rax
	movdqa	-352(%rbp), %xmm7
	movq	%rax, 80(%r12)
	movl	-328(%rbp), %eax
	movups	%xmm7, 64(%r12)
	movl	%eax, 88(%r12)
.L426:
	movl	-468(%rbp), %eax
	movl	%eax, 88(%r12)
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L457:
	movl	%r13d, %ebx
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L516:
	movq	%r15, %rdi
	call	_ZN6icu_676number5Scale10powerOfTenEi@PLT
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L466:
	movl	-456(%rbp), %edi
	movsd	%xmm0, -480(%rbp)
	leaq	-352(%rbp), %r15
	call	_ZN6icu_676number4impl18PatternStringUtils23ignoreRoundingIncrementEdi@PLT
	movsd	-480(%rbp), %xmm0
	testb	%al, %al
	jne	.L501
	movl	-420(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN6icu_676number9Precision18constructIncrementEdi@PLT
.L502:
	movq	-336(%rbp), %rax
	movdqa	-352(%rbp), %xmm6
	movq	%rax, -400(%rbp)
	movl	-328(%rbp), %eax
	movaps	%xmm6, -416(%rbp)
	movl	%eax, -392(%rbp)
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L506:
	movl	%r13d, %edi
	movl	%r13d, %ebx
	call	_ZN6icu_676number12IntegerWidth10zeroFillToEi@PLT
	leaq	-384(%rbp), %rdi
	movl	%r13d, %esi
	movl	%edx, -376(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZN6icu_676number12IntegerWidth10truncateAtEi@PLT
	movl	96(%r14), %ecx
	movl	%edx, -344(%rbp)
	movb	%dl, 132(%r12)
	movl	$1, %edx
	movq	%rax, -352(%rbp)
	movq	%rax, 124(%r12)
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L501:
	movl	-456(%rbp), %edx
	movl	-420(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN6icu_676number9Precision17constructFractionEii@PLT
	movq	-336(%rbp), %rax
	movdqa	-352(%rbp), %xmm5
	movq	%rax, -400(%rbp)
	movl	-328(%rbp), %eax
	movaps	%xmm5, -416(%rbp)
	movl	%eax, -392(%rbp)
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L510:
	movswl	-376(%rbp), %eax
	pxor	%xmm0, %xmm0
	movl	%eax, -420(%rbp)
	movswl	-374(%rbp), %eax
	movl	%eax, -456(%rbp)
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L443:
	pxor	%xmm0, %xmm0
	cmpl	$3, %eax
	jne	.L442
	movswl	-372(%rbp), %eax
	movl	%eax, -424(%rbp)
	movswl	-370(%rbp), %eax
	movl	%eax, -452(%rbp)
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L453:
	movl	$1, -420(%rbp)
.L404:
	movl	$-1, -456(%rbp)
	testl	%r15d, %r15d
	jns	.L444
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L508:
	call	_ZN6icu_676number8Notation11compactLongEv@PLT
	movq	%rax, 4(%r12)
	movl	%edx, 12(%r12)
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L436:
	movq	%r15, %rdi
	call	_ZN6icu_676number5Scale4noneEv@PLT
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L415:
	leaq	-352(%rbp), %r15
	cmpl	$-1, %r9d
	jne	.L501
	testb	%al, %al
	je	.L418
	movl	%r8d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_676number9Precision17constructCurrencyE14UCurrencyUsage@PLT
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L458:
	movl	$999, -452(%rbp)
	movl	%edx, %esi
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L509:
	movq	-432(%rbp), %rcx
	movq	-440(%rbp), %rdx
	leaq	-416(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_676number9Precision12withCurrencyERKNS_12CurrencyUnitER10UErrorCode@PLT
	movq	-336(%rbp), %rcx
	movdqa	-352(%rbp), %xmm7
	movl	-352(%rbp), %eax
	movzwl	-336(%rbp), %edx
	movq	%rcx, -368(%rbp)
	movl	-328(%rbp), %ecx
	movaps	%xmm7, -384(%rbp)
	movl	%ecx, -360(%rbp)
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L459:
	movl	%edx, -452(%rbp)
	movl	%edx, %esi
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L517:
	movq	%r15, %rdi
	call	_ZN6icu_676number9Precision9unlimitedEv@PLT
	movq	-336(%rbp), %rax
	movdqa	-352(%rbp), %xmm6
	movq	%rax, 80(%r12)
	movl	-328(%rbp), %eax
	movups	%xmm6, 64(%r12)
	movl	%eax, 88(%r12)
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L504:
	movl	$-1, -456(%rbp)
	xorl	%ebx, %ebx
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L518:
	addl	$1, %edx
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN6icu_676number9Precision20constructSignificantEii@PLT
	movq	-336(%rbp), %rax
	movdqa	-352(%rbp), %xmm5
	movq	%rax, 80(%r12)
	movl	-328(%rbp), %eax
	movups	%xmm5, 64(%r12)
	movl	%eax, 88(%r12)
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L513:
	movl	%eax, %r15d
	jmp	.L445
.L511:
	call	__stack_chk_fail@PLT
.L514:
	movl	60(%r14), %r8d
	jmp	.L394
	.cfi_endproc
.LFE3639:
	.size	_ZN6icu_676number4impl20NumberPropertyMapper8oldToNewERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseEPS3_R10UErrorCode, .-_ZN6icu_676number4impl20NumberPropertyMapper8oldToNewERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseEPS3_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl20NumberPropertyMapper6createERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseER10UErrorCode
	.type	_ZN6icu_676number4impl20NumberPropertyMapper6createERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseER10UErrorCode, @function
_ZN6icu_676number4impl20NumberPropertyMapper6createERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseER10UErrorCode:
.LFB3631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	leaq	-512(%rbp), %r10
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r10, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$936, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r10, -976(%rbp)
	call	_ZN6icu_676number15NumberFormatter4withEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %r9
	movq	%r15, %rcx
	leaq	-960(%rbp), %r11
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	movq	%r11, -968(%rbp)
	call	_ZN6icu_676number4impl20NumberPropertyMapper8oldToNewERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseEPS3_R10UErrorCode
	movq	-968(%rbp), %r11
	movq	-976(%rbp), %r10
	movq	%r12, %rdi
	movq	%r11, %rdx
	movq	%r10, %rsi
	call	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE@PLT
	leaq	-744(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-792(%rbp), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-824(%rbp), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	leaq	-920(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	-944(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	-296(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-344(%rbp), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-376(%rbp), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	leaq	-472(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	-496(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L522
	addq	$936, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L522:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3631:
	.size	_ZN6icu_676number4impl20NumberPropertyMapper6createERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseER10UErrorCode, .-_ZN6icu_676number4impl20NumberPropertyMapper6createERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl20NumberPropertyMapper6createERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseERS3_R10UErrorCode
	.type	_ZN6icu_676number4impl20NumberPropertyMapper6createERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseERS3_R10UErrorCode, @function
_ZN6icu_676number4impl20NumberPropertyMapper6createERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseERS3_R10UErrorCode:
.LFB3638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	leaq	-512(%rbp), %r10
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r10, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$936, %rsp
	movq	%r9, -968(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r10, -976(%rbp)
	call	_ZN6icu_676number15NumberFormatter4withEv@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	-968(%rbp), %r9
	leaq	-960(%rbp), %r11
	movq	%r13, %rsi
	movq	%r11, %rdi
	movq	%r11, -968(%rbp)
	call	_ZN6icu_676number4impl20NumberPropertyMapper8oldToNewERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseEPS3_R10UErrorCode
	movq	-968(%rbp), %r11
	movq	-976(%rbp), %r10
	movq	%r12, %rdi
	movq	%r11, %rdx
	movq	%r10, %rsi
	call	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosEONS0_4impl10MacroPropsE@PLT
	leaq	-744(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-792(%rbp), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-824(%rbp), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	leaq	-920(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	-944(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	-296(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-344(%rbp), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-376(%rbp), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	leaq	-472(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	-496(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L526
	addq	$936, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L526:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3638:
	.size	_ZN6icu_676number4impl20NumberPropertyMapper6createERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseERS3_R10UErrorCode, .-_ZN6icu_676number4impl20NumberPropertyMapper6createERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseERS3_R10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_676number4impl30PropertiesAffixPatternProviderE
	.section	.rodata._ZTSN6icu_676number4impl30PropertiesAffixPatternProviderE,"aG",@progbits,_ZTSN6icu_676number4impl30PropertiesAffixPatternProviderE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl30PropertiesAffixPatternProviderE, @object
	.size	_ZTSN6icu_676number4impl30PropertiesAffixPatternProviderE, 54
_ZTSN6icu_676number4impl30PropertiesAffixPatternProviderE:
	.string	"N6icu_676number4impl30PropertiesAffixPatternProviderE"
	.weak	_ZTIN6icu_676number4impl30PropertiesAffixPatternProviderE
	.section	.data.rel.ro._ZTIN6icu_676number4impl30PropertiesAffixPatternProviderE,"awG",@progbits,_ZTIN6icu_676number4impl30PropertiesAffixPatternProviderE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl30PropertiesAffixPatternProviderE, @object
	.size	_ZTIN6icu_676number4impl30PropertiesAffixPatternProviderE, 56
_ZTIN6icu_676number4impl30PropertiesAffixPatternProviderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl30PropertiesAffixPatternProviderE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl20AffixPatternProviderE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTSN6icu_676number4impl31CurrencyPluralInfoAffixProviderE
	.section	.rodata._ZTSN6icu_676number4impl31CurrencyPluralInfoAffixProviderE,"aG",@progbits,_ZTSN6icu_676number4impl31CurrencyPluralInfoAffixProviderE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl31CurrencyPluralInfoAffixProviderE, @object
	.size	_ZTSN6icu_676number4impl31CurrencyPluralInfoAffixProviderE, 55
_ZTSN6icu_676number4impl31CurrencyPluralInfoAffixProviderE:
	.string	"N6icu_676number4impl31CurrencyPluralInfoAffixProviderE"
	.weak	_ZTIN6icu_676number4impl31CurrencyPluralInfoAffixProviderE
	.section	.data.rel.ro._ZTIN6icu_676number4impl31CurrencyPluralInfoAffixProviderE,"awG",@progbits,_ZTIN6icu_676number4impl31CurrencyPluralInfoAffixProviderE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl31CurrencyPluralInfoAffixProviderE, @object
	.size	_ZTIN6icu_676number4impl31CurrencyPluralInfoAffixProviderE, 56
_ZTIN6icu_676number4impl31CurrencyPluralInfoAffixProviderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl31CurrencyPluralInfoAffixProviderE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl20AffixPatternProviderE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE,"awG",@progbits,_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE, @object
	.size	_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE, 104
_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl30PropertiesAffixPatternProviderE
	.quad	_ZN6icu_676number4impl30PropertiesAffixPatternProviderD1Ev
	.quad	_ZN6icu_676number4impl30PropertiesAffixPatternProviderD0Ev
	.quad	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider6charAtEii
	.quad	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider6lengthEi
	.quad	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider9getStringEi
	.quad	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider15hasCurrencySignEv
	.quad	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider19positiveHasPlusSignEv
	.quad	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider21hasNegativeSubpatternEv
	.quad	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider20negativeHasMinusSignEv
	.quad	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode
	.quad	_ZNK6icu_676number4impl30PropertiesAffixPatternProvider7hasBodyEv
	.weak	_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE,"awG",@progbits,_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE, @object
	.size	_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE, 104
_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl31CurrencyPluralInfoAffixProviderE
	.quad	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD1Ev
	.quad	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProviderD0Ev
	.quad	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider6charAtEii
	.quad	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider6lengthEi
	.quad	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider9getStringEi
	.quad	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider15hasCurrencySignEv
	.quad	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider19positiveHasPlusSignEv
	.quad	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider21hasNegativeSubpatternEv
	.quad	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider20negativeHasMinusSignEv
	.quad	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode
	.quad	_ZNK6icu_676number4impl31CurrencyPluralInfoAffixProvider7hasBodyEv
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
