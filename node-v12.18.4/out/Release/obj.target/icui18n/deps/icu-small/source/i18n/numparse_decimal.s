	.file	"numparse_decimal.cpp"
	.text
	.section	.text._ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv,"axG",@progbits,_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.type	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv, @function
_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv:
.LFB2799:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2799:
	.size	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv, .-_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.section	.text._ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE,"axG",@progbits,_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.type	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE, @function
_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE:
.LFB2800:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2800:
	.size	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE, .-_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"<"
	.string	"D"
	.string	"e"
	.string	"c"
	.string	"i"
	.string	"m"
	.string	"a"
	.string	"l"
	.string	">"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl14DecimalMatcher8toStringEv
	.type	_ZNK6icu_678numparse4impl14DecimalMatcher8toStringEv, @function
_ZNK6icu_678numparse4impl14DecimalMatcher8toStringEv:
.LFB2815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2815:
	.size	_ZNK6icu_678numparse4impl14DecimalMatcher8toStringEv, .-_ZNK6icu_678numparse4impl14DecimalMatcher8toStringEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl14DecimalMatcher9smokeTestERKNS_13StringSegmentE
	.type	_ZNK6icu_678numparse4impl14DecimalMatcher9smokeTestERKNS_13StringSegmentE, @function
_ZNK6icu_678numparse4impl14DecimalMatcher9smokeTestERKNS_13StringSegmentE:
.LFB2814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	$0, 192(%rdi)
	movq	%rdi, %rbx
	je	.L21
.L7:
	movq	160(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment10startsWithERKNS_10UnicodeSetE@PLT
	movl	%eax, %r14d
	testb	%al, %al
	je	.L22
.L8:
	movl	$1, %r14d
.L6:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment12getCodePointEv@PLT
	movl	%eax, %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	jne	.L8
	movq	192(%rbx), %rsi
	xorl	%r13d, %r13d
	testq	%rsi, %rsi
	je	.L6
	addq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment10startsWithERKNS_13UnicodeStringE@PLT
	testb	%al, %al
	jne	.L8
	.p2align 4,,10
	.p2align 3
.L23:
	addq	$64, %r13
	cmpq	$640, %r13
	je	.L6
	movq	192(%rbx), %rsi
	movq	%r12, %rdi
	addq	%r13, %rsi
	call	_ZNK6icu_6713StringSegment10startsWithERKNS_13UnicodeStringE@PLT
	testb	%al, %al
	je	.L23
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L21:
	movq	168(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L7
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6713StringSegment10startsWithERKNS_10UnicodeSetE@PLT
	.cfi_endproc
.LFE2814:
	.size	_ZNK6icu_678numparse4impl14DecimalMatcher9smokeTestERKNS_13StringSegmentE, .-_ZNK6icu_678numparse4impl14DecimalMatcher9smokeTestERKNS_13StringSegmentE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl14DecimalMatcher13validateGroupEiib
	.type	_ZNK6icu_678numparse4impl14DecimalMatcher13validateGroupEiib, @function
_ZNK6icu_678numparse4impl14DecimalMatcher13validateGroupEiib:
.LFB2813:
	.cfi_startproc
	endbr64
	movzbl	8(%rdi), %eax
	testb	%al, %al
	je	.L25
	cmpl	$-1, %esi
	je	.L24
	testl	%esi, %esi
	jne	.L27
	movl	%ecx, %eax
	testb	%cl, %cl
	jne	.L24
	testl	%edx, %edx
	je	.L24
	movswl	14(%rdi), %eax
	cmpl	%edx, %eax
	setge	%al
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$1, %eax
	cmpl	$1, %esi
	jne	.L24
	cmpl	$1, %edx
	setne	%al
.L24:
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	cmpl	$1, %esi
	jne	.L24
	testb	%cl, %cl
	je	.L28
	movswl	12(%rdi), %eax
	cmpl	%edx, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movswl	14(%rdi), %eax
	cmpl	%edx, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE2813:
	.size	_ZNK6icu_678numparse4impl14DecimalMatcher13validateGroupEiib, .-_ZNK6icu_678numparse4impl14DecimalMatcher13validateGroupEiib
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl14DecimalMatcherC2ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperEi
	.type	_ZN6icu_678numparse4impl14DecimalMatcherC2ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperEi, @function
_ZN6icu_678numparse4impl14DecimalMatcherC2ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperEi:
.LFB2809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r8d
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$2, %esi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$16, %rdi
	leaq	80(%rbx), %r12
	subq	$120, %rsp
	movl	%ecx, -132(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_678numparse4impl14DecimalMatcherE(%rip), %rax
	movq	%r12, -144(%rbp)
	movq	%rax, -16(%rdi)
	andl	$2, %ecx
	movq	%r13, 16(%rbx)
	movw	%si, 24(%rbx)
	movq	%r13, 80(%rbx)
	movw	%r8w, 88(%rbx)
	movq	$0, 192(%rbx)
	movups	%xmm0, 176(%rbx)
	je	.L37
	leaq	1096(%r14), %r15
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	648(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L38:
	testb	$4, -132(%rbp)
	je	.L83
	movl	$10, %edi
	leaq	-128(%rbp), %r15
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movq	-144(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, 144(%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r15, %rdi
	movl	$6, %edx
	movl	$5, %esi
	call	_ZN6icu_677unisets10chooseFromENS_13UnicodeStringENS0_3KeyES2_@PLT
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testl	%r12d, %r12d
	js	.L61
	movl	%r12d, %edi
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movl	$22, %edi
	movq	%rax, 152(%rbx)
	movq	144(%rbx), %rax
	movq	%rax, 160(%rbx)
.L62:
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movq	%rax, 168(%rbx)
.L45:
	movl	1864(%r14), %r15d
	cmpl	$-1, %r15d
	je	.L50
	movl	%r15d, %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	jne	.L84
.L50:
	movl	$648, %edi
	call	_ZN6icu_677UMemorynaEm@PLT
	testq	%rax, %rax
	je	.L85
	movl	$2, %edx
	movl	$2, %ecx
	movl	$2, %esi
	movl	$2, %edi
	movw	%dx, 16(%rax)
	movl	$2, %r8d
	leaq	8(%rax), %r12
	movl	$2, %r9d
	movl	$2, %r10d
	movl	$2, %r11d
	movq	%r13, 8(%rax)
	movl	$2, %r15d
	movl	$2, %edx
	movq	$10, (%rax)
	movq	%r13, 72(%rax)
	movw	%cx, 80(%rax)
	movq	%r13, 136(%rax)
	movw	%si, 144(%rax)
	movq	%r13, 200(%rax)
	movw	%di, 208(%rax)
	movq	%r13, 264(%rax)
	movw	%r8w, 272(%rax)
	movq	%r13, 328(%rax)
	movw	%r9w, 336(%rax)
	movq	%r13, 392(%rax)
	movw	%r10w, 400(%rax)
	movq	%r13, 456(%rax)
	movw	%r11w, 464(%rax)
	movq	%r13, 520(%rax)
	movw	%r15w, 528(%rax)
	movq	%r13, 584(%rax)
	movw	%dx, 592(%rax)
.L49:
	movq	192(%rbx), %rax
	testq	%rax, %rax
	je	.L52
	movq	-8(%rax), %r13
	salq	$6, %r13
	addq	%rax, %r13
	cmpq	%r13, %rax
	je	.L53
	.p2align 4,,10
	.p2align 3
.L54:
	movq	-64(%r13), %rax
	subq	$64, %r13
	movq	%r13, %rdi
	call	*(%rax)
	cmpq	%r13, 192(%rbx)
	jne	.L54
.L53:
	leaq	-8(%r13), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L52:
	leaq	1096(%r14), %r15
	movq	%r12, 192(%rbx)
	xorl	%r13d, %r13d
	addq	$264, %r14
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%r13, %rsi
	movq	%r12, %rdi
	salq	$6, %rsi
	addq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	cmpl	$9, %r13d
	je	.L51
.L56:
	addq	$1, %r13
	addq	$64, %r12
.L57:
	testq	%r13, %r13
	jne	.L55
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$9, %edi
	leaq	-128(%rbp), %r15
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movq	-144(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, 144(%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r15, %rdi
	movl	$4, %edx
	movl	$3, %esi
	call	_ZN6icu_677unisets10chooseFromENS_13UnicodeStringENS0_3KeyES2_@PLT
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testl	%r12d, %r12d
	jns	.L86
.L61:
	movswl	88(%rbx), %eax
	shrl	$5, %eax
	jne	.L87
	xorl	%edi, %edi
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movq	%rax, 152(%rbx)
.L44:
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L59
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
.L59:
	movq	144(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	152(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	184(%rbx), %rdi
	movq	%r15, 160(%rbx)
	testq	%rdi, %rdi
	je	.L46
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L46:
	movq	%r15, 184(%rbx)
	movq	$0, 168(%rbx)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	72(%r14), %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-144(%rbp), %rdi
	leaq	8(%r14), %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L42
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
.L42:
	movq	-144(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	176(%rbx), %rdi
	movq	%r15, 152(%rbx)
	testq	%rdi, %rdi
	je	.L43
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L43:
	movq	%r15, 176(%rbx)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$10, %esi
	movl	%r15d, %edi
	call	u_digit_67@PLT
	testl	%eax, %eax
	jne	.L50
	.p2align 4,,10
	.p2align 3
.L51:
	movl	-132(%rbp), %ecx
	movq	-152(%rbp), %r14
	movl	%ecx, %eax
	movq	%r14, %rdi
	shrl	$3, %eax
	andl	$1, %eax
	movb	%al, 8(%rbx)
	movl	%ecx, %eax
	shrl	$4, %ecx
	movl	%ecx, %r12d
	shrl	$5, %eax
	andl	$1, %eax
	andl	$1, %r12d
	movb	%al, 9(%rbx)
	movb	%r12b, 10(%rbx)
	call	_ZNK6icu_676number4impl7Grouper10getPrimaryEv@PLT
	movq	%r14, %rdi
	movw	%ax, 12(%rbx)
	call	_ZNK6icu_676number4impl7Grouper12getSecondaryEv@PLT
	movw	%ax, 14(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L86:
	.cfi_restore_state
	movl	%r12d, %edi
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movl	$23, %edi
	movq	%rax, 152(%rbx)
	movq	144(%rbx), %rax
	movq	%rax, 160(%rbx)
	jmp	.L62
.L88:
	call	__stack_chk_fail@PLT
.L85:
	xorl	%r12d, %r12d
	jmp	.L49
	.cfi_endproc
.LFE2809:
	.size	_ZN6icu_678numparse4impl14DecimalMatcherC2ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperEi, .-_ZN6icu_678numparse4impl14DecimalMatcherC2ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperEi
	.globl	_ZN6icu_678numparse4impl14DecimalMatcherC1ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperEi
	.set	_ZN6icu_678numparse4impl14DecimalMatcherC1ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperEi,_ZN6icu_678numparse4impl14DecimalMatcherC2ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperEi
	.section	.text._ZN6icu_678numparse4impl14DecimalMatcherD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl14DecimalMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl14DecimalMatcherD2Ev
	.type	_ZN6icu_678numparse4impl14DecimalMatcherD2Ev, @function
_ZN6icu_678numparse4impl14DecimalMatcherD2Ev:
.LFB3928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl14DecimalMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	192(%rdi), %rax
	testq	%rax, %rax
	je	.L90
	movq	-8(%rax), %rbx
	salq	$6, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L91
	.p2align 4,,10
	.p2align 3
.L92:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, 192(%r12)
	jne	.L92
.L91:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L90:
	movq	184(%r12), %rdi
	testq	%rdi, %rdi
	je	.L93
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L93:
	movq	176(%r12), %rdi
	testq	%rdi, %rdi
	je	.L94
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L94:
	leaq	80(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	.cfi_endproc
.LFE3928:
	.size	_ZN6icu_678numparse4impl14DecimalMatcherD2Ev, .-_ZN6icu_678numparse4impl14DecimalMatcherD2Ev
	.weak	_ZN6icu_678numparse4impl14DecimalMatcherD1Ev
	.set	_ZN6icu_678numparse4impl14DecimalMatcherD1Ev,_ZN6icu_678numparse4impl14DecimalMatcherD2Ev
	.section	.text._ZN6icu_678numparse4impl14DecimalMatcherD0Ev,"axG",@progbits,_ZN6icu_678numparse4impl14DecimalMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl14DecimalMatcherD0Ev
	.type	_ZN6icu_678numparse4impl14DecimalMatcherD0Ev, @function
_ZN6icu_678numparse4impl14DecimalMatcherD0Ev:
.LFB3930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl14DecimalMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	192(%rdi), %rax
	testq	%rax, %rax
	je	.L107
	movq	-8(%rax), %rbx
	salq	$6, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L108
	.p2align 4,,10
	.p2align 3
.L109:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, 192(%r12)
	jne	.L109
.L108:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L107:
	movq	184(%r12), %rdi
	testq	%rdi, %rdi
	je	.L110
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L110:
	movq	176(%r12), %rdi
	testq	%rdi, %rdi
	je	.L111
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L111:
	leaq	80(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3930:
	.size	_ZN6icu_678numparse4impl14DecimalMatcherD0Ev, .-_ZN6icu_678numparse4impl14DecimalMatcherD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl14DecimalMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberEaR10UErrorCode
	.type	_ZNK6icu_678numparse4impl14DecimalMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberEaR10UErrorCode, @function
_ZNK6icu_678numparse4impl14DecimalMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberEaR10UErrorCode:
.LFB2812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$392, %rsp
	movq	%rdx, -408(%rbp)
	movl	%ecx, -376(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_678numparse4impl12ParsedNumber10seenNumberEv@PLT
	testb	%r14b, %r14b
	sete	%dl
	xorl	%r14d, %r14d
	testb	%al, %dl
	jne	.L123
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	movl	%eax, -412(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r10d
	movl	$2, %r11d
	movq	%rax, -256(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, %rdi
	movw	%r10w, -248(%rbp)
	movw	%r11w, -184(%rbp)
	movq	%rax, -392(%rbp)
	movb	$1, -328(%rbp)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -400(%rbp)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movl	$-1, -356(%rbp)
	movl	$-1, -352(%rbp)
	movl	$-1, -360(%rbp)
	movl	$0, -340(%rbp)
	movl	$0, -344(%rbp)
	movl	$0, -348(%rbp)
	movl	$0, -372(%rbp)
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	testl	%eax, %eax
	jle	.L439
.L443:
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment12getCodePointEv@PLT
	movl	%eax, %edi
	movl	%eax, %r13d
	call	u_isdigit_67@PLT
	testb	%al, %al
	je	.L428
	xorl	%esi, %esi
	cmpl	$65535, %r13d
	movq	%r12, %rdi
	seta	%sil
	addl	$1, %esi
	call	_ZN6icu_6713StringSegment12adjustOffsetEi@PLT
	movl	$10, %esi
	movl	%r13d, %edi
	call	u_digit_67@PLT
	cmpb	$-1, %al
	je	.L428
	testb	%al, %al
	js	.L129
	movsbl	%al, %r13d
	xorl	%r14d, %r14d
.L181:
	cmpb	$0, -328(%rbp)
	jne	.L442
.L182:
	movq	-368(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movl	%r13d, %esi
	call	_ZN6icu_676number4impl15DecimalQuantity11appendDigitEaib@PLT
	movzbl	-184(%rbp), %eax
	movq	%r12, %rdi
	addl	$1, -340(%rbp)
	andl	$1, %eax
	cmpb	$1, %al
	adcl	$0, -372(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	testl	%eax, %eax
	jg	.L443
.L439:
	movl	-340(%rbp), %eax
	testl	%eax, %eax
	sete	%dl
.L211:
	cmpl	$2, -344(%rbp)
	je	.L213
	testb	%dl, %dl
	jne	.L234
.L213:
	movzbl	8(%rbx), %eax
	testb	%al, %al
	je	.L243
	movl	-352(%rbp), %edx
	cmpl	$-1, %edx
	je	.L248
	testl	%edx, %edx
	jne	.L245
	movl	-356(%rbp), %edi
	testl	%edi, %edi
	jne	.L246
	xorl	%eax, %eax
.L248:
	cmpl	$1, -344(%rbp)
	je	.L444
	cmpl	$2, -344(%rbp)
	je	.L252
.L247:
	testb	%al, %al
	jne	.L252
	movb	$1, -328(%rbp)
.L255:
	testb	%r14b, %r14b
	jne	.L257
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	testl	%eax, %eax
	sete	%r14b
.L257:
	movl	-412(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment9setOffsetEi@PLT
.L258:
	movq	-400(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-392(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-368(%rbp), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
.L123:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L445
	addq	$392, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	movq	192(%rbx), %r15
	testq	%r15, %r15
	jne	.L446
.L129:
	xorl	%r14d, %r14d
.L177:
	xorl	%ecx, %ecx
	testb	$1, -184(%rbp)
	je	.L184
	movswl	88(%rbx), %eax
	shrl	$5, %eax
	jne	.L447
.L184:
	movzwl	-248(%rbp), %eax
	testb	$1, %al
	je	.L448
	movzbl	-184(%rbp), %esi
	andl	$1, %esi
	cmpb	$0, 9(%rbx)
	jne	.L281
.L282:
	testb	%sil, %sil
	je	.L195
	movswl	24(%rbx), %eax
	shrl	$5, %eax
	je	.L288
	xorl	%edx, %edx
.L293:
	leaq	16(%rbx), %rsi
	movq	%r12, %rdi
	movb	%dl, -417(%rbp)
	movb	%cl, -416(%rbp)
	movq	%rsi, -384(%rbp)
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	testb	%r14b, %r14b
	movq	-384(%rbp), %rsi
	movzbl	-416(%rbp), %ecx
	movzbl	-417(%rbp), %edx
	movl	%eax, %r8d
	je	.L449
.L197:
	movswl	24(%rbx), %eax
	testw	%ax, %ax
	js	.L198
	sarl	$5, %eax
.L199:
	cmpl	%eax, %r8d
	je	.L450
	testb	%dl, %dl
	je	.L451
	movzbl	9(%rbx), %edx
.L201:
	testb	%dl, %dl
	jne	.L210
	movzwl	-248(%rbp), %eax
	movl	$1, %edx
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L446:
	movswl	8(%r15), %eax
	shrl	$5, %eax
	je	.L298
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movl	%eax, %esi
	movswl	8(%r15), %eax
	movl	%eax, %ecx
	sarl	$5, %eax
	testw	%cx, %cx
	jns	.L136
	movl	12(%r15), %eax
.L136:
	cmpl	%esi, %eax
	je	.L299
	movq	%r12, %rdi
	movl	%esi, -384(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-384(%rbp), %esi
	movq	192(%rbx), %r15
	cmpl	%esi, %eax
	sete	%r14b
.L132:
	movswl	72(%r15), %eax
	shrl	$5, %eax
	je	.L137
	leaq	64(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movl	%eax, %esi
	movswl	72(%r15), %eax
	movl	%eax, %ecx
	sarl	$5, %eax
	testw	%cx, %cx
	jns	.L141
	movl	76(%r15), %eax
.L141:
	cmpl	%esi, %eax
	je	.L300
	testb	%r14b, %r14b
	je	.L140
.L429:
	movq	192(%rbx), %r15
.L137:
	movswl	136(%r15), %eax
	shrl	$5, %eax
	je	.L142
	leaq	128(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movswl	136(%r15), %esi
	movl	%eax, %ecx
	movl	%esi, %edi
	sarl	$5, %esi
	testw	%di, %di
	jns	.L146
	movl	140(%r15), %esi
.L146:
	cmpl	%esi, %ecx
	je	.L301
	testb	%r14b, %r14b
	je	.L145
.L430:
	movq	192(%rbx), %r15
.L142:
	movswl	200(%r15), %eax
	shrl	$5, %eax
	je	.L147
	leaq	192(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movswl	200(%r15), %esi
	movl	%eax, %ecx
	movl	%esi, %edi
	sarl	$5, %esi
	testw	%di, %di
	jns	.L151
	movl	204(%r15), %esi
.L151:
	cmpl	%esi, %ecx
	je	.L302
	testb	%r14b, %r14b
	je	.L150
.L431:
	movq	192(%rbx), %r15
.L147:
	movswl	264(%r15), %eax
	shrl	$5, %eax
	je	.L152
	leaq	256(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movswl	264(%r15), %esi
	movl	%eax, %ecx
	movl	%esi, %edi
	sarl	$5, %esi
	testw	%di, %di
	jns	.L156
	movl	268(%r15), %esi
.L156:
	cmpl	%esi, %ecx
	je	.L303
	testb	%r14b, %r14b
	je	.L155
.L432:
	movq	192(%rbx), %r15
.L152:
	movswl	328(%r15), %eax
	shrl	$5, %eax
	je	.L157
	leaq	320(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movswl	328(%r15), %esi
	movl	%eax, %ecx
	movl	%esi, %edi
	sarl	$5, %esi
	testw	%di, %di
	jns	.L161
	movl	332(%r15), %esi
.L161:
	cmpl	%esi, %ecx
	je	.L304
	testb	%r14b, %r14b
	je	.L160
.L433:
	movq	192(%rbx), %r15
.L157:
	movswl	392(%r15), %eax
	shrl	$5, %eax
	je	.L162
	leaq	384(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movswl	392(%r15), %esi
	movl	%eax, %ecx
	movl	%esi, %edi
	sarl	$5, %esi
	testw	%di, %di
	jns	.L166
	movl	396(%r15), %esi
.L166:
	cmpl	%esi, %ecx
	je	.L305
	testb	%r14b, %r14b
	je	.L165
.L434:
	movq	192(%rbx), %r15
.L162:
	movswl	456(%r15), %eax
	shrl	$5, %eax
	je	.L167
	leaq	448(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movswl	456(%r15), %esi
	movl	%eax, %ecx
	movl	%esi, %edi
	sarl	$5, %esi
	testw	%di, %di
	jns	.L171
	movl	460(%r15), %esi
.L171:
	cmpl	%esi, %ecx
	je	.L306
	testb	%r14b, %r14b
	je	.L170
.L435:
	movq	192(%rbx), %r15
.L167:
	movswl	520(%r15), %eax
	shrl	$5, %eax
	je	.L172
	leaq	512(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movswl	520(%r15), %esi
	movl	%eax, %ecx
	movl	%esi, %edi
	sarl	$5, %esi
	testw	%di, %di
	jns	.L176
	movl	524(%r15), %esi
.L176:
	cmpl	%esi, %ecx
	je	.L307
	testb	%r14b, %r14b
	je	.L175
.L436:
	movq	192(%rbx), %r15
.L172:
	movswl	584(%r15), %eax
	shrl	$5, %eax
	je	.L177
	leaq	576(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movswl	584(%r15), %esi
	movl	%eax, %ecx
	movl	%esi, %edi
	sarl	$5, %esi
	testw	%di, %di
	jns	.L180
	movl	588(%r15), %esi
.L180:
	cmpl	%esi, %ecx
	je	.L308
	testb	%r14b, %r14b
	jne	.L177
	movq	%r12, %rdi
	movl	%ecx, -384(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-384(%rbp), %ecx
	cmpl	%eax, %ecx
	sete	%r14b
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L281:
	testb	%sil, %sil
	je	.L314
.L288:
	movq	152(%rbx), %rdi
	movl	%r13d, %esi
	movb	%cl, -384(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L204
	movzbl	9(%rbx), %eax
	movzbl	-384(%rbp), %ecx
.L205:
	testb	%al, %al
	jne	.L314
	movzwl	-248(%rbp), %eax
.L195:
	testb	$1, %al
	je	.L314
	xorl	%edx, %edx
	testb	$1, -184(%rbp)
	jne	.L208
.L437:
	movl	%edx, %eax
	orl	%ecx, %eax
.L207:
	testb	%al, %al
	je	.L439
.L210:
	testb	%cl, %cl
	je	.L212
	cmpb	$0, 10(%rbx)
	jne	.L439
.L212:
	cmpl	$2, -344(%rbp)
	jne	.L286
	testb	%dl, %dl
	jne	.L452
.L286:
	movzbl	8(%rbx), %eax
	testb	%al, %al
	je	.L215
	movl	-352(%rbp), %edi
	cmpl	$-1, %edi
	je	.L220
	testl	%edi, %edi
	jne	.L217
	movl	-356(%rbp), %edi
	testl	%edi, %edi
	je	.L218
	movswl	14(%rbx), %esi
	cmpl	%esi, %edi
	setle	%sil
	cmpl	$1, -344(%rbp)
	je	.L453
.L418:
	testb	%sil, %sil
	je	.L454
	.p2align 4,,10
	.p2align 3
.L233:
	movl	$-1, -352(%rbp)
	testb	%cl, %cl
	je	.L419
.L236:
	movl	-340(%rbp), %r8d
	movl	-348(%rbp), %r13d
	testl	%r8d, %r8d
	jne	.L455
.L237:
	testb	%dl, %dl
	je	.L456
	movswl	-248(%rbp), %esi
	movq	%r12, %rdi
	movl	%esi, %eax
	sarl	$5, %esi
	testw	%ax, %ax
	cmovs	-244(%rbp), %esi
	call	_ZN6icu_6713StringSegment12adjustOffsetEi@PLT
	movl	-340(%rbp), %eax
	movl	$1, -344(%rbp)
	movl	$0, -340(%rbp)
	movl	%eax, -356(%rbp)
	movl	-348(%rbp), %eax
	movl	%r13d, -348(%rbp)
	movl	%eax, -360(%rbp)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L215:
	cmpl	$1, -352(%rbp)
	je	.L457
	cmpl	$1, -344(%rbp)
	jne	.L233
	cmpl	$1, -340(%rbp)
	jne	.L233
	testb	%cl, %cl
	jne	.L458
	.p2align 4,,10
	.p2align 3
.L419:
	movl	-344(%rbp), %eax
	movl	%eax, -352(%rbp)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L456:
	movswl	-184(%rbp), %esi
	movq	%r12, %rdi
	movl	%esi, %eax
	sarl	$5, %esi
	testw	%ax, %ax
	cmovs	-180(%rbp), %esi
	call	_ZN6icu_6713StringSegment12adjustOffsetEi@PLT
	movl	-340(%rbp), %eax
	movl	$2, -344(%rbp)
	movl	$0, -340(%rbp)
	movl	%eax, -356(%rbp)
	movl	-348(%rbp), %eax
	movl	%r13d, -348(%rbp)
	movl	%eax, -360(%rbp)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L314:
	movl	%ecx, %eax
	xorl	%edx, %edx
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L448:
	movq	-392(%rbp), %rsi
	movq	%r12, %rdi
	movb	%cl, -384(%rbp)
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	testb	%r14b, %r14b
	movzbl	-384(%rbp), %ecx
	movl	%eax, %esi
	je	.L459
.L189:
	movzwl	-248(%rbp), %eax
	testw	%ax, %ax
	js	.L190
	movswl	%ax, %edi
	movzbl	9(%rbx), %edx
	sarl	$5, %edi
	cmpl	%edi, %esi
	je	.L460
.L411:
	movzbl	-184(%rbp), %esi
	andl	$1, %esi
	testb	%dl, %dl
	jne	.L281
	testb	$1, %al
	jne	.L282
.L289:
	testb	%sil, %sil
	jne	.L288
	movzbl	9(%rbx), %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L455:
	movq	%r12, %rdi
	movb	%dl, -344(%rbp)
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	movzbl	-344(%rbp), %edx
	movl	%eax, %r13d
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L298:
	xorl	%r14d, %r14d
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L442:
	movq	-368(%rbp), %rdi
	movb	$0, -328(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantity5clearEv@PLT
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L220:
	cmpl	$1, -344(%rbp)
	jne	.L233
	movswl	12(%rbx), %eax
	cmpl	-340(%rbp), %eax
	sete	%al
	xorl	$1, %eax
	andb	%cl, %al
	jne	.L230
.L285:
	movl	-340(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L233
.L234:
	movl	-348(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment9setOffsetEi@PLT
	movzbl	8(%rbx), %r14d
	testb	%r14b, %r14b
	je	.L461
	cmpw	$0, 14(%rbx)
	setg	%al
	cmpl	$-1, -352(%rbp)
	je	.L247
	movl	-356(%rbp), %edx
	movl	%edx, -340(%rbp)
	movl	-352(%rbp), %edx
	movl	%edx, -344(%rbp)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L217:
	cmpl	$1, -352(%rbp)
	jne	.L220
	movswl	14(%rbx), %esi
	cmpl	-356(%rbp), %esi
	sete	%sil
	cmpl	$1, -344(%rbp)
	jne	.L418
.L453:
	movswl	12(%rbx), %edi
	cmpl	-340(%rbp), %edi
	sete	%dil
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L447:
	leaq	80(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rsi, -384(%rbp)
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	testb	%r14b, %r14b
	movq	-384(%rbp), %rsi
	movl	%eax, %edx
	je	.L462
.L185:
	movswl	88(%rbx), %eax
	testw	%ax, %ax
	js	.L186
	sarl	$5, %eax
.L187:
	xorl	%ecx, %ecx
	cmpl	%eax, %edx
	jne	.L184
	movq	-400(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	$1, %ecx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L457:
	cmpl	$1, -344(%rbp)
	je	.L463
	cmpl	$1, -356(%rbp)
	jne	.L233
	movl	-356(%rbp), %ecx
	movl	%ecx, -352(%rbp)
.L230:
	cmpl	$0, -340(%rbp)
	sete	%r13b
	andb	%r13b, %dl
	je	.L464
.L321:
	movl	$0, -340(%rbp)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L190:
	movl	-244(%rbp), %edi
	movzbl	9(%rbx), %edx
	cmpl	%edi, %esi
	jne	.L411
.L460:
	testb	%dl, %dl
	jne	.L210
	testb	$1, %al
	jne	.L465
.L323:
	movl	$1, %edx
.L196:
	testb	$1, %al
	je	.L210
	testb	$1, -184(%rbp)
	je	.L437
.L208:
	movq	144(%rbx), %rdi
	movl	%r13d, %esi
	movb	%cl, -384(%rbp)
	movb	%dl, -416(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movzbl	-384(%rbp), %ecx
	testb	%al, %al
	jne	.L209
	movzbl	-416(%rbp), %edx
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L459:
	movq	%r12, %rdi
	movb	%cl, -416(%rbp)
	movl	%eax, -384(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-384(%rbp), %esi
	movzbl	-416(%rbp), %ecx
	cmpl	%esi, %eax
	sete	%r14b
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L218:
	movl	-340(%rbp), %esi
	testl	%esi, %esi
	sete	%r13b
	andl	%r13d, %edx
	cmpl	$1, -344(%rbp)
	je	.L274
	movl	%edi, -352(%rbp)
.L275:
	testb	%dl, %dl
	jne	.L321
.L438:
	movl	-352(%rbp), %edx
	movl	-356(%rbp), %eax
	movl	%edx, -356(%rbp)
	jmp	.L226
.L470:
	cmpl	$1, -340(%rbp)
	jne	.L252
	movl	-348(%rbp), %eax
	movl	%eax, -360(%rbp)
.L284:
	movl	-360(%rbp), %esi
	movq	%r12, %rdi
	movl	$1, %r15d
	movl	$1, %r14d
	call	_ZN6icu_6713StringSegment9setOffsetEi@PLT
.L294:
	movq	-368(%rbp), %rbx
	movl	%r15d, %esi
	negl	%esi
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity8truncateEv@PLT
	.p2align 4,,10
	.p2align 3
.L252:
	cmpb	$0, -328(%rbp)
	jne	.L255
.L256:
	movl	-372(%rbp), %esi
	movq	-368(%rbp), %rdi
	negl	%esi
	call	_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi@PLT
	cmpb	$0, -376(%rbp)
	je	.L261
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	cmpl	-412(%rbp), %eax
	je	.L261
	movq	-368(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_676number4impl15DecimalQuantity10fitsInLongEb@PLT
	testb	%al, %al
	jne	.L262
.L265:
	cmpb	$-1, -376(%rbp)
	je	.L466
	movq	-408(%rbp), %rax
	orl	$128, 76(%rax)
	movb	$1, 8(%rax)
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L261:
	movq	-368(%rbp), %rsi
	movq	-408(%rbp), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityaSERKS2_@PLT
.L260:
	testb	$1, -184(%rbp)
	jne	.L266
	movq	-408(%rbp), %rax
	orl	$32, 76(%rax)
.L266:
	movq	-408(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_678numparse4impl12ParsedNumber16setCharsConsumedERKNS_13StringSegmentE@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	testl	%eax, %eax
	sete	%al
	orl	%eax, %r14d
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%r12, %rdi
	movl	%esi, -384(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-384(%rbp), %esi
	cmpl	%eax, %esi
	sete	%r14b
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r12, %rdi
	movl	%ecx, -384(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-384(%rbp), %ecx
	cmpl	%eax, %ecx
	sete	%r14b
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L150:
	movq	%r12, %rdi
	movl	%ecx, -384(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-384(%rbp), %ecx
	cmpl	%eax, %ecx
	sete	%r14b
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%r12, %rdi
	movl	%ecx, -384(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-384(%rbp), %ecx
	cmpl	%eax, %ecx
	sete	%r14b
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%r12, %rdi
	movl	%ecx, -384(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-384(%rbp), %ecx
	cmpl	%eax, %ecx
	sete	%r14b
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r12, %rdi
	movl	%ecx, -384(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-384(%rbp), %ecx
	cmpl	%eax, %ecx
	sete	%r14b
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L170:
	movq	%r12, %rdi
	movl	%ecx, -384(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-384(%rbp), %ecx
	cmpl	%eax, %ecx
	sete	%r14b
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r12, %rdi
	movl	%ecx, -384(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-384(%rbp), %ecx
	cmpl	%eax, %ecx
	sete	%r14b
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L186:
	movl	92(%rbx), %eax
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L463:
	cmpl	$1, -356(%rbp)
	setne	%sil
	cmpl	$1, -340(%rbp)
	setne	%dil
.L229:
	testb	%sil, %sil
	je	.L230
	testb	%dil, %dil
	jne	.L231
	testb	%cl, %cl
	jne	.L230
.L231:
	testb	%al, %al
	jne	.L285
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L462:
	movq	%r12, %rdi
	movl	%eax, -416(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-416(%rbp), %edx
	movq	-384(%rbp), %rsi
	cmpl	%edx, %eax
	sete	%r14b
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L204:
	leaq	-128(%rbp), %r8
	movl	%r13d, %esi
	movq	%r8, %rdi
	movq	%r8, -384(%rbp)
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	movq	-384(%rbp), %r8
	movq	-400(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-384(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	9(%rbx), %edx
	testb	%dl, %dl
	je	.L467
	cmpb	$0, 10(%rbx)
	jne	.L439
	movl	%edx, %ecx
	xorl	%edx, %edx
	jmp	.L286
.L458:
	movl	$1, -340(%rbp)
	.p2align 4,,10
	.p2align 3
.L243:
	cmpl	$1, -352(%rbp)
	jne	.L319
	cmpl	$1, -344(%rbp)
	je	.L468
	cmpl	$1, -356(%rbp)
	jne	.L252
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L467:
	movzwl	-248(%rbp), %eax
	movl	$1, %ecx
	jmp	.L196
.L274:
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L469
.L226:
	movq	-368(%rbp), %rdi
	movl	%eax, -384(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantity5clearEv@PLT
	movl	-356(%rbp), %eax
	movb	$1, -328(%rbp)
	movl	%r13d, %edx
	movl	%eax, -352(%rbp)
	movl	-384(%rbp), %eax
	movl	%eax, -356(%rbp)
	jmp	.L211
.L198:
	movl	28(%rbx), %eax
	jmp	.L199
.L461:
	movl	$1, %eax
	xorl	%edx, %edx
	movl	$1, %r14d
.L228:
	cmpl	$1, -352(%rbp)
	jne	.L252
	cmpl	$1, -356(%rbp)
	jne	.L252
	orl	%edx, %eax
	je	.L252
	jmp	.L284
.L319:
	movl	-340(%rbp), %ebx
	movl	-356(%rbp), %eax
	movl	-352(%rbp), %edx
	movl	%ebx, -356(%rbp)
	movl	-344(%rbp), %ebx
	movl	%ebx, -352(%rbp)
	movl	-348(%rbp), %ebx
	movl	%ebx, -360(%rbp)
	jmp	.L228
.L449:
	movq	%r12, %rdi
	movb	%dl, -418(%rbp)
	movb	%cl, -417(%rbp)
	movl	%eax, -416(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-416(%rbp), %r8d
	movzbl	-418(%rbp), %edx
	movzbl	-417(%rbp), %ecx
	movq	-384(%rbp), %rsi
	cmpl	%r8d, %eax
	sete	%r14b
	jmp	.L197
.L444:
	movswl	12(%rbx), %edx
	cmpl	-340(%rbp), %edx
	sete	%dl
	andl	%edx, %eax
	jmp	.L247
.L209:
	leaq	-128(%rbp), %r8
	movl	%r13d, %esi
	movb	%cl, -416(%rbp)
	movq	%r8, %rdi
	movq	%r8, -384(%rbp)
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	movq	-384(%rbp), %r8
	movq	-392(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-384(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-416(%rbp), %ecx
	movl	$1, %edx
	jmp	.L210
.L452:
	movl	$2, -344(%rbp)
	jmp	.L213
.L245:
	cmpl	$1, -352(%rbp)
	jne	.L248
	movswl	14(%rbx), %eax
	cmpl	-356(%rbp), %eax
	sete	%al
	jmp	.L248
.L299:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment12adjustOffsetEi@PLT
	jmp	.L181
.L300:
	movl	$1, %r13d
	jmp	.L135
.L302:
	movl	$3, %r13d
	jmp	.L135
.L301:
	movl	$2, %r13d
	jmp	.L135
.L303:
	movl	$4, %r13d
	jmp	.L135
.L304:
	movl	$5, %r13d
	jmp	.L135
.L306:
	movl	$7, %r13d
	jmp	.L135
.L305:
	movl	$6, %r13d
	jmp	.L135
.L469:
	movl	-348(%rbp), %esi
	movq	%r12, %rdi
	movb	%dl, -340(%rbp)
	call	_ZN6icu_6713StringSegment9setOffsetEi@PLT
	movzbl	8(%rbx), %r14d
	testb	%r14b, %r14b
	jne	.L291
	movzbl	-328(%rbp), %r14d
	movzbl	-340(%rbp), %edx
	testb	%r14b, %r14b
	jne	.L257
	movl	%edx, %r14d
	jmp	.L256
.L307:
	movl	$8, %r13d
	jmp	.L135
.L308:
	movl	$9, %r13d
	jmp	.L135
.L291:
	cmpw	$0, 14(%rbx)
	setg	%al
	jmp	.L247
.L464:
	cmpl	$2, -344(%rbp)
	setne	%dl
	andl	%r13d, %edx
	testb	%al, %al
	jne	.L438
	testb	%dl, %dl
	je	.L243
	jmp	.L234
.L450:
	movq	-392(%rbp), %rdi
	movb	%cl, -384(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzbl	9(%rbx), %edx
	movzbl	-384(%rbp), %ecx
	jmp	.L201
.L468:
	cmpl	$1, -356(%rbp)
	jne	.L470
.L253:
	movl	-360(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment9setOffsetEi@PLT
	movl	-340(%rbp), %r15d
	addl	$1, %r15d
	jmp	.L294
.L262:
	movq	-368(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_676number4impl15DecimalQuantity6toLongEb@PLT
	cmpq	$2147483647, %rax
	jg	.L265
	movsbl	-376(%rbp), %esi
	movq	-408(%rbp), %rdi
	imull	%eax, %esi
	call	_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi@PLT
	testb	%al, %al
	jne	.L265
	jmp	.L260
.L246:
	movswl	14(%rbx), %eax
	cmpl	-356(%rbp), %eax
	setge	%al
	jmp	.L248
.L466:
	movq	-408(%rbp), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity5clearEv@PLT
	jmp	.L260
.L454:
	movl	-340(%rbp), %ecx
	testl	%ecx, %ecx
	sete	%r13b
	andl	%r13d, %edx
	jmp	.L275
.L451:
	movzbl	-184(%rbp), %esi
	andl	$1, %esi
	jmp	.L289
.L465:
	testb	$1, -184(%rbp)
	je	.L323
	movswl	24(%rbx), %esi
	movl	$1, %edx
	shrl	$5, %esi
	je	.L196
	jmp	.L293
.L445:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2812:
	.size	_ZNK6icu_678numparse4impl14DecimalMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberEaR10UErrorCode, .-_ZNK6icu_678numparse4impl14DecimalMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberEaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl14DecimalMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.type	_ZNK6icu_678numparse4impl14DecimalMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, @function
_ZNK6icu_678numparse4impl14DecimalMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode:
.LFB2811:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	jmp	_ZNK6icu_678numparse4impl14DecimalMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberEaR10UErrorCode
	.cfi_endproc
.LFE2811:
	.size	_ZNK6icu_678numparse4impl14DecimalMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, .-_ZNK6icu_678numparse4impl14DecimalMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_678numparse4impl14DecimalMatcherE
	.section	.rodata._ZTSN6icu_678numparse4impl14DecimalMatcherE,"aG",@progbits,_ZTSN6icu_678numparse4impl14DecimalMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl14DecimalMatcherE, @object
	.size	_ZTSN6icu_678numparse4impl14DecimalMatcherE, 40
_ZTSN6icu_678numparse4impl14DecimalMatcherE:
	.string	"N6icu_678numparse4impl14DecimalMatcherE"
	.weak	_ZTIN6icu_678numparse4impl14DecimalMatcherE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl14DecimalMatcherE,"awG",@progbits,_ZTIN6icu_678numparse4impl14DecimalMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl14DecimalMatcherE, @object
	.size	_ZTIN6icu_678numparse4impl14DecimalMatcherE, 56
_ZTIN6icu_678numparse4impl14DecimalMatcherE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl14DecimalMatcherE
	.long	0
	.long	2
	.quad	_ZTIN6icu_678numparse4impl18NumberParseMatcherE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTVN6icu_678numparse4impl14DecimalMatcherE
	.section	.data.rel.ro.local._ZTVN6icu_678numparse4impl14DecimalMatcherE,"awG",@progbits,_ZTVN6icu_678numparse4impl14DecimalMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl14DecimalMatcherE, @object
	.size	_ZTVN6icu_678numparse4impl14DecimalMatcherE, 72
_ZTVN6icu_678numparse4impl14DecimalMatcherE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl14DecimalMatcherE
	.quad	_ZN6icu_678numparse4impl14DecimalMatcherD1Ev
	.quad	_ZN6icu_678numparse4impl14DecimalMatcherD0Ev
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl14DecimalMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl14DecimalMatcher9smokeTestERKNS_13StringSegmentE
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.quad	_ZNK6icu_678numparse4impl14DecimalMatcher8toStringEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
