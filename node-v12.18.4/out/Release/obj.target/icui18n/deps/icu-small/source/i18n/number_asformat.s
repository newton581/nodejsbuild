	.file	"number_asformat.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat17getDynamicClassIDEv
	.type	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat17getDynamicClassIDEv, @function
_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat17getDynamicClassIDEv:
.LFB3592:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_676number4impl32LocalizedNumberFormatterAsFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3592:
	.size	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat17getDynamicClassIDEv, .-_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.type	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, @function
_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE:
.LFB3607:
	.cfi_startproc
	endbr64
	movl	$0, 12(%rcx)
	ret
	.cfi_endproc
.LFE3607:
	.size	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, .-_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat5cloneEv
	.type	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat5cloneEv, @function
_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat5cloneEv:
.LFB3601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$1008, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_676FormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_676number4impl32LocalizedNumberFormatterAsFormatE(%rip), %rax
	leaq	328(%rbx), %rsi
	movq	%rax, (%r12)
	leaq	328(%r12), %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterC1ERKS1_@PLT
	leaq	784(%rbx), %rsi
	leaq	784(%r12), %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
.L4:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3601:
	.size	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat5cloneEv, .-_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat5cloneEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB4085:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4085:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB4088:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L23
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L11
	cmpb	$0, 12(%rbx)
	jne	.L24
.L15:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L15
	.cfi_endproc
.LFE4088:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB4091:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L27
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4091:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB4094:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L30
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE4094:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB4096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L36
.L32:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L37
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4096:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB4097:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4097:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB4098:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4098:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB4099:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4099:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4100:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4100:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4101:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4101:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB4102:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L53
	testl	%edx, %edx
	jle	.L53
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L56
.L45:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L45
	.cfi_endproc
.LFE4102:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB4103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L60
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L60
	testl	%r12d, %r12d
	jg	.L67
	cmpb	$0, 12(%rbx)
	jne	.L68
.L62:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L62
.L68:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L60:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4103:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB4104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L70
	movq	(%rdi), %r8
.L71:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L74
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L74
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L74:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4104:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB4105:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L81
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4105:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB4106:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4106:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB4107:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4107:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB4108:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4108:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB4110:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4110:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB4112:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4112:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormat16getStaticClassIDEv
	.type	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormat16getStaticClassIDEv, @function
_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormat16getStaticClassIDEv:
.LFB3591:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_676number4impl32LocalizedNumberFormatterAsFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3591:
	.size	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormat16getStaticClassIDEv, .-_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormat16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatC2ERKNS0_24LocalizedNumberFormatterERKNS_6LocaleE
	.type	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatC2ERKNS0_24LocalizedNumberFormatterERKNS_6LocaleE, @function
_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatC2ERKNS0_24LocalizedNumberFormatterERKNS_6LocaleE:
.LFB3594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl32LocalizedNumberFormatterAsFormatE(%rip), %rax
	leaq	328(%r12), %rdi
	movq	%r13, %rsi
	movq	%rax, (%r12)
	call	_ZN6icu_676number24LocalizedNumberFormatterC1ERKS1_@PLT
	leaq	784(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	40(%rbx), %rsi
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	movq	%rsi, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676Format12setLocaleIDsEPKcS2_@PLT
	.cfi_endproc
.LFE3594:
	.size	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatC2ERKNS0_24LocalizedNumberFormatterERKNS_6LocaleE, .-_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatC2ERKNS0_24LocalizedNumberFormatterERKNS_6LocaleE
	.globl	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatC1ERKNS0_24LocalizedNumberFormatterERKNS_6LocaleE
	.set	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatC1ERKNS0_24LocalizedNumberFormatterERKNS_6LocaleE,_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatC2ERKNS0_24LocalizedNumberFormatterERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat18getNumberFormatterEv
	.type	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat18getNumberFormatterEv, @function
_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat18getNumberFormatterEv:
.LFB3608:
	.cfi_startproc
	endbr64
	leaq	328(%rdi), %rax
	ret
	.cfi_endproc
.LFE3608:
	.size	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat18getNumberFormatterEv, .-_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat18getNumberFormatterEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number24LocalizedNumberFormatter8toFormatER10UErrorCode
	.type	_ZNK6icu_676number24LocalizedNumberFormatter8toFormatER10UErrorCode, @function
_ZNK6icu_676number24LocalizedNumberFormatter8toFormatER10UErrorCode:
.LFB3609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L96
	movq	%rdi, %rbx
	movl	$1008, %edi
	movq	%rsi, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L94
	movq	%rax, %rdi
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl32LocalizedNumberFormatterAsFormatE(%rip), %rax
	leaq	328(%r12), %rdi
	movq	%rbx, %rsi
	movq	%rax, (%r12)
	call	_ZN6icu_676number24LocalizedNumberFormatterC1ERKS1_@PLT
	leaq	216(%rbx), %rsi
	leaq	784(%r12), %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	256(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rsi, %rdx
	call	_ZN6icu_676Format12setLocaleIDsEPKcS2_@PLT
.L91:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L94:
	.cfi_restore_state
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L96
	movl	$7, 0(%r13)
	jmp	.L91
	.cfi_endproc
.LFE3609:
	.size	_ZNK6icu_676number24LocalizedNumberFormatter8toFormatER10UErrorCode, .-_ZNK6icu_676number24LocalizedNumberFormatter8toFormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD2Ev
	.type	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD2Ev, @function
_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD2Ev:
.LFB3597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl32LocalizedNumberFormatterAsFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	784(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -784(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	328(%r12), %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676FormatD2Ev@PLT
	.cfi_endproc
.LFE3597:
	.size	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD2Ev, .-_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD2Ev
	.globl	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD1Ev
	.set	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD1Ev,_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD0Ev
	.type	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD0Ev, @function
_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD0Ev:
.LFB3599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl32LocalizedNumberFormatterAsFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	784(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -784(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	328(%r12), %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676FormatD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3599:
	.size	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD0Ev, .-_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormateqERKNS_6FormatE
	.type	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormateqERKNS_6FormatE, @function
_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormateqERKNS_6FormatE:
.LFB3600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_676number4impl32LocalizedNumberFormatterAsFormatE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	_ZTIN6icu_676FormatE(%rip), %rsi
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L102
	leaq	-180(%rbp), %r12
	leaq	-112(%rbp), %r13
	movl	$0, -180(%rbp)
	leaq	328(%rax), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE10toSkeletonER10UErrorCode@PLT
	leaq	-176(%rbp), %r14
	movq	%r12, %rdx
	leaq	328(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE10toSkeletonER10UErrorCode@PLT
	movswl	-168(%rbp), %edx
	movl	%edx, %r12d
	andl	$1, %r12d
	jne	.L122
	testw	%dx, %dx
	js	.L106
	sarl	$5, %edx
.L107:
	movzwl	-104(%rbp), %eax
	testw	%ax, %ax
	js	.L108
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L109:
	testb	$1, %al
	jne	.L105
	cmpl	%edx, %ecx
	jne	.L105
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r12b
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L122:
	movzbl	-104(%rbp), %r12d
	andl	$1, %r12d
.L105:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L102:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L123
	addq	$160, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movl	-100(%rbp), %ecx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L106:
	movl	-164(%rbp), %edx
	jmp	.L107
.L123:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3600:
	.size	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormateqERKNS_6FormatE, .-_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormateqERKNS_6FormatE
	.align 2
	.p2align 4
	.type	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0, @function
_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0:
.LFB4765:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-288(%rbp), %r14
	leaq	-136(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$328, %rsp
	movq	%rcx, -368(%rbp)
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	movq	%rdi, -360(%rbp)
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6711Formattable23populateDecimalQuantityERNS_6number4impl15DecimalQuantityER10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L143
.L126:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L144
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	-360(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	addq	$328, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L126
	movq	-368(%rbp), %rax
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	$0, 12(%rax)
	movq	%rax, %rsi
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl17nextFieldPositionERNS_13FieldPositionER10UErrorCode@PLT
	testb	%al, %al
	je	.L128
	movswl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L129
	sarl	$5, %eax
.L130:
	testl	%eax, %eax
	je	.L128
	movq	-368(%rbp), %rcx
	addl	%eax, 12(%rcx)
	movswl	8(%rbx), %eax
	movl	16(%rcx), %edx
	testw	%ax, %ax
	js	.L131
	sarl	$5, %eax
.L132:
	addl	%edx, %eax
	movl	%eax, 16(%rcx)
.L128:
	leaq	-352(%rbp), %r13
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl12toTempStringER10UErrorCode@PLT
	movswl	-344(%rbp), %ecx
	testw	%cx, %cx
	js	.L133
	sarl	$5, %ecx
.L134:
	movq	%rbx, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L133:
	movl	-340(%rbp), %ecx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L129:
	movl	12(%rbx), %eax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L131:
	movl	12(%rbx), %eax
	jmp	.L132
.L144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4765:
	.size	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0, .-_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB3605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L146
	call	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
.L146:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3605:
	.size	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode.part.0, @function
_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode.part.0:
.LFB4766:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-288(%rbp), %r15
	leaq	-136(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$376, %rsp
	movq	%rdx, -408(%rbp)
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	movq	%rcx, -416(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6711Formattable23populateDecimalQuantityERNS_6number4impl15DecimalQuantityER10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L159
.L150:
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L160
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	leaq	328(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L150
	leaq	-352(%rbp), %r13
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl12toTempStringER10UErrorCode@PLT
	movswl	-344(%rbp), %ecx
	testw	%cx, %cx
	js	.L152
	sarl	$5, %ecx
.L153:
	movq	-408(%rbp), %rdi
	movq	%r13, %rsi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-416(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L150
	leaq	-400(%rbp), %r13
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6728FieldPositionIteratorHandlerC1EPNS_21FieldPositionIteratorER10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl20getAllFieldPositionsERNS_28FieldPositionIteratorHandlerER10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6728FieldPositionIteratorHandlerD1Ev@PLT
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L152:
	movl	-340(%rbp), %ecx
	jmp	.L153
.L160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4766:
	.size	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode.part.0, .-_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB3606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L162
	call	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode.part.0
.L162:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3606:
	.size	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.weak	_ZTSN6icu_676number4impl32LocalizedNumberFormatterAsFormatE
	.section	.rodata._ZTSN6icu_676number4impl32LocalizedNumberFormatterAsFormatE,"aG",@progbits,_ZTSN6icu_676number4impl32LocalizedNumberFormatterAsFormatE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl32LocalizedNumberFormatterAsFormatE, @object
	.size	_ZTSN6icu_676number4impl32LocalizedNumberFormatterAsFormatE, 56
_ZTSN6icu_676number4impl32LocalizedNumberFormatterAsFormatE:
	.string	"N6icu_676number4impl32LocalizedNumberFormatterAsFormatE"
	.weak	_ZTIN6icu_676number4impl32LocalizedNumberFormatterAsFormatE
	.section	.data.rel.ro._ZTIN6icu_676number4impl32LocalizedNumberFormatterAsFormatE,"awG",@progbits,_ZTIN6icu_676number4impl32LocalizedNumberFormatterAsFormatE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl32LocalizedNumberFormatterAsFormatE, @object
	.size	_ZTIN6icu_676number4impl32LocalizedNumberFormatterAsFormatE, 24
_ZTIN6icu_676number4impl32LocalizedNumberFormatterAsFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl32LocalizedNumberFormatterAsFormatE
	.quad	_ZTIN6icu_676FormatE
	.weak	_ZTVN6icu_676number4impl32LocalizedNumberFormatterAsFormatE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl32LocalizedNumberFormatterAsFormatE,"awG",@progbits,_ZTVN6icu_676number4impl32LocalizedNumberFormatterAsFormatE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl32LocalizedNumberFormatterAsFormatE, @object
	.size	_ZTVN6icu_676number4impl32LocalizedNumberFormatterAsFormatE, 80
_ZTVN6icu_676number4impl32LocalizedNumberFormatterAsFormatE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl32LocalizedNumberFormatterAsFormatE
	.quad	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD1Ev
	.quad	_ZN6icu_676number4impl32LocalizedNumberFormatterAsFormatD0Ev
	.quad	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat17getDynamicClassIDEv
	.quad	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormateqERKNS_6FormatE
	.quad	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat5cloneEv
	.quad	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_676number4impl32LocalizedNumberFormatterAsFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.local	_ZZN6icu_676number4impl32LocalizedNumberFormatterAsFormat16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_676number4impl32LocalizedNumberFormatterAsFormat16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.type	_ZN6icu_67L15kUndefinedFieldE, @object
	.size	_ZN6icu_67L15kUndefinedFieldE, 1
_ZN6icu_67L15kUndefinedFieldE:
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
