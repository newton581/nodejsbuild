	.file	"hebrwcal.cpp"
	.text
	.p2align 4
	.type	calendar_hebrew_cleanup, @function
calendar_hebrew_cleanup:
.LFB3099:
	.cfi_startproc
	endbr64
	movq	_ZL6gCache(%rip), %rdi
	testq	%rdi, %rdi
	je	.L7
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*8(%rax)
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, _ZL6gCache(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore 6
	movq	$0, _ZL6gCache(%rip)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3099:
	.size	calendar_hebrew_cleanup, .-calendar_hebrew_cleanup
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"hebrew"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714HebrewCalendar7getTypeEv
	.type	_ZNK6icu_6714HebrewCalendar7getTypeEv, @function
_ZNK6icu_6714HebrewCalendar7getTypeEv:
.LFB3107:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE3107:
	.size	_ZNK6icu_6714HebrewCalendar7getTypeEv, .-_ZNK6icu_6714HebrewCalendar7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714HebrewCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.type	_ZNK6icu_6714HebrewCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, @function
_ZNK6icu_6714HebrewCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE:
.LFB3121:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	leaq	_ZL6LIMITS(%rip), %rax
	leaq	(%rdx,%rsi,4), %rdx
	movl	(%rax,%rdx,4), %eax
	ret
	.cfi_endproc
.LFE3121:
	.size	_ZNK6icu_6714HebrewCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, .-_ZNK6icu_6714HebrewCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714HebrewCalendar18haveDefaultCenturyEv
	.type	_ZNK6icu_6714HebrewCalendar18haveDefaultCenturyEv, @function
_ZNK6icu_6714HebrewCalendar18haveDefaultCenturyEv:
.LFB3129:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3129:
	.size	_ZNK6icu_6714HebrewCalendar18haveDefaultCenturyEv, .-_ZNK6icu_6714HebrewCalendar18haveDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714HebrewCalendar17getDynamicClassIDEv
	.type	_ZNK6icu_6714HebrewCalendar17getDynamicClassIDEv, @function
_ZNK6icu_6714HebrewCalendar17getDynamicClassIDEv:
.LFB3134:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714HebrewCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3134:
	.size	_ZNK6icu_6714HebrewCalendar17getDynamicClassIDEv, .-_ZNK6icu_6714HebrewCalendar17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendarD2Ev
	.type	_ZN6icu_6714HebrewCalendarD2Ev, @function
_ZN6icu_6714HebrewCalendarD2Ev:
.LFB3104:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714HebrewCalendarE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678CalendarD2Ev@PLT
	.cfi_endproc
.LFE3104:
	.size	_ZN6icu_6714HebrewCalendarD2Ev, .-_ZN6icu_6714HebrewCalendarD2Ev
	.globl	_ZN6icu_6714HebrewCalendarD1Ev
	.set	_ZN6icu_6714HebrewCalendarD1Ev,_ZN6icu_6714HebrewCalendarD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendarD0Ev
	.type	_ZN6icu_6714HebrewCalendarD0Ev, @function
_ZN6icu_6714HebrewCalendarD0Ev:
.LFB3106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714HebrewCalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678CalendarD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3106:
	.size	_ZN6icu_6714HebrewCalendarD0Ev, .-_ZN6icu_6714HebrewCalendarD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714HebrewCalendar5cloneEv
	.type	_ZNK6icu_6714HebrewCalendar5cloneEv, @function
_ZNK6icu_6714HebrewCalendar5cloneEv:
.LFB3108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$616, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L18
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_678CalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714HebrewCalendarE(%rip), %rax
	movq	%rax, (%r12)
.L18:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3108:
	.size	_ZNK6icu_6714HebrewCalendar5cloneEv, .-_ZNK6icu_6714HebrewCalendar5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714HebrewCalendar14inDaylightTimeER10UErrorCode
	.type	_ZNK6icu_6714HebrewCalendar14inDaylightTimeER10UErrorCode, @function
_ZNK6icu_6714HebrewCalendar14inDaylightTimeER10UErrorCode:
.LFB3128:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.L25
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK6icu_678Calendar11getTimeZoneEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*72(%rax)
	testb	%al, %al
	jne	.L36
	xorl	%eax, %eax
.L24:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode@PLT
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L24
	movl	76(%r12), %eax
	testl	%eax, %eax
	setne	%al
	jmp	.L24
	.cfi_endproc
.LFE3128:
	.size	_ZNK6icu_6714HebrewCalendar14inDaylightTimeER10UErrorCode, .-_ZNK6icu_6714HebrewCalendar14inDaylightTimeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendar21handleGetExtendedYearEv
	.type	_ZN6icu_6714HebrewCalendar21handleGetExtendedYearEv, @function
_ZN6icu_6714HebrewCalendar21handleGetExtendedYearEv:
.LFB3126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movl	$19, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_@PLT
	cmpl	$19, %eax
	movl	$1, %eax
	je	.L43
	movl	132(%rbx), %edx
	testl	%edx, %edx
	jle	.L37
	movl	16(%rbx), %eax
.L37:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	204(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L37
	movl	88(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3126:
	.size	_ZN6icu_6714HebrewCalendar21handleGetExtendedYearEv, .-_ZN6icu_6714HebrewCalendar21handleGetExtendedYearEv
	.align 2
	.p2align 4
	.type	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0, @function
_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0:
.LFB4053:
	.cfi_startproc
	imull	$235, %edi, %eax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movl	%edi, %r8d
	movabsq	$5830082077616845943, %rdx
	subl	$234, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movslq	%eax, %r12
	sarl	$31, %eax
	imulq	$1808407283, %r12, %r12
	subq	$8, %rsp
	sarq	$35, %r12
	subl	%eax, %r12d
	movslq	%r12d, %rax
	imull	$29, %r12d, %r12d
	imulq	$13753, %rax, %rax
	leaq	12084(%rax), %rsi
	movq	%rsi, %rax
	imulq	%rdx
	movq	%rsi, %rax
	sarq	$63, %rax
	sarq	$13, %rdx
	subq	%rax, %rdx
	movq	%rsi, %rax
	addl	%edx, %r12d
	imulq	$25920, %rdx, %rdx
	movl	%r12d, %esi
	movl	%r12d, %edi
	sarl	$31, %esi
	subq	%rdx, %rax
	movslq	%r12d, %rdx
	imulq	$-1840700269, %rdx, %rdx
	shrq	$32, %rdx
	addl	%r12d, %edx
	sarl	$2, %edx
	subl	%esi, %edx
	leal	0(,%rdx,8), %esi
	subl	%edx, %esi
	subl	%esi, %edi
	movl	%edi, %esi
	andl	$-5, %esi
	cmpl	$2, %esi
	je	.L53
	movl	%edi, %edx
	cmpl	$4, %edi
	jne	.L45
.L53:
	addl	$1, %r12d
	movslq	%r12d, %rdx
	movl	%r12d, %esi
	movl	%r12d, %edi
	imulq	$-1840700269, %rdx, %rdx
	sarl	$31, %esi
	shrq	$32, %rdx
	addl	%r12d, %edx
	sarl	$2, %edx
	subl	%esi, %edx
	leal	0(,%rdx,8), %esi
	subl	%edx, %esi
	subl	%esi, %edi
	movl	%edi, %edx
.L45:
	cmpl	$1, %edx
	jne	.L47
	cmpq	$16404, %rax
	jle	.L47
	leal	(%r8,%r8,2), %eax
	leal	17(,%rax,4), %edx
	movslq	%edx, %rax
	movl	%edx, %esi
	imulq	$1808407283, %rax, %rax
	sarl	$31, %esi
	sarq	$35, %rax
	subl	%esi, %eax
	leal	(%rax,%rax,8), %esi
	leal	(%rax,%rsi,2), %eax
	leal	2(%r12), %esi
	subl	%eax, %edx
	movl	%edx, %eax
	cltd
	andl	$-19, %edx
	addl	$12, %edx
	cmpl	%edx, %eax
	cmovl	%esi, %r12d
.L49:
	movl	%r12d, %edx
	leaq	_ZL6gCache(%rip), %rdi
	movl	%r8d, %esi
	call	_ZN6icu_6713CalendarCache3putEPPS0_iiR10UErrorCode@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	testl	%edx, %edx
	jne	.L49
	cmpq	$23269, %rax
	jle	.L49
	leal	-3(%r8,%r8,2), %eax
	leal	17(,%rax,4), %edx
	movslq	%edx, %rax
	movl	%edx, %esi
	imulq	$1808407283, %rax, %rax
	sarl	$31, %esi
	sarq	$35, %rax
	subl	%esi, %eax
	leal	(%rax,%rax,8), %esi
	leal	(%rax,%rsi,2), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	cltd
	andl	$-19, %edx
	addl	$12, %edx
	cmpl	%edx, %eax
	setge	%al
	movzbl	%al, %eax
	addl	%eax, %r12d
	jmp	.L49
	.cfi_endproc
.LFE4053:
	.size	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0, .-_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714HebrewCalendar19handleGetYearLengthEi
	.type	_ZNK6icu_6714HebrewCalendar19handleGetYearLengthEi, @function
_ZNK6icu_6714HebrewCalendar19handleGetYearLengthEi:
.LFB3123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$10, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leal	1(%rsi), %r14d
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-44(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%r13, %rdx
	movl	%r14d, %esi
	leaq	_ZL6gCache(%rip), %rdi
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L71
	movq	%r13, %rsi
	movl	%r14d, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	movl	%eax, %ebx
.L71:
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	movl	$10, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%r13, %rdx
	movl	%r12d, %esi
	leaq	_ZL6gCache(%rip), %rdi
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	testl	%eax, %eax
	jne	.L72
	movq	%r13, %rsi
	movl	%r12d, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
.L72:
	subl	%eax, %ebx
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	movl	%ebx, %eax
	jne	.L76
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3123:
	.size	_ZNK6icu_6714HebrewCalendar19handleGetYearLengthEi, .-_ZNK6icu_6714HebrewCalendar19handleGetYearLengthEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendar19handleComputeFieldsEiR10UErrorCode
	.type	_ZN6icu_6714HebrewCalendar19handleComputeFieldsEiR10UErrorCode, @function
_ZN6icu_6714HebrewCalendar19handleComputeFieldsEiR10UErrorCode:
.LFB3125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leal	-347997(%rsi), %r14d
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	pushq	%r13
	cvtsi2sdl	%r14d, %xmm0
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$10, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	mulsd	.LC1(%rip), %xmm0
	divsd	.LC2(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	addsd	.LC4(%rip), %xmm0
	divsd	.LC5(%rip), %xmm0
	addsd	.LC6(%rip), %xmm0
	cvttsd2sil	%xmm0, %ebx
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%r12, %rdx
	leaq	_ZL6gCache(%rip), %rdi
	movl	%ebx, %esi
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	testl	%eax, %eax
	jne	.L78
	movq	%r12, %rsi
	movl	%ebx, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
.L78:
	movl	%r14d, %ecx
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jg	.L79
	leaq	calendar_hebrew_cleanup(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L82:
	subl	$1, %ebx
	movq	%r15, %rsi
	movl	$10, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%r12, %rdx
	movl	%ebx, %esi
	leaq	_ZL6gCache(%rip), %rdi
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	testl	%eax, %eax
	jne	.L80
	movq	%r12, %rsi
	movl	%ebx, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	movl	%r14d, %ecx
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L82
.L79:
	movq	0(%r13), %rax
	leaq	_ZNK6icu_6714HebrewCalendar19handleGetYearLengthEi(%rip), %rdx
	movl	%ecx, -68(%rbp)
	movq	280(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L83
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	movl	$10, %edi
	leal	1(%rbx), %r15d
	movl	$0, -60(%rbp)
	call	ucln_i18n_registerCleanup_67@PLT
	leaq	-60(%rbp), %r14
	movl	%r15d, %esi
	leaq	_ZL6gCache(%rip), %rdi
	movq	%r14, %rdx
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	movl	-68(%rbp), %ecx
	testl	%eax, %eax
	movl	%eax, %r8d
	jne	.L84
	movq	%r14, %rsi
	movl	%r15d, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	movl	-68(%rbp), %ecx
	movl	%eax, %r8d
.L84:
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	movl	$10, %edi
	movl	%r8d, -72(%rbp)
	movl	%ecx, -68(%rbp)
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%r14, %rdx
	movl	%ebx, %esi
	leaq	_ZL6gCache(%rip), %rdi
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	movl	-68(%rbp), %ecx
	movl	-72(%rbp), %r8d
	testl	%eax, %eax
	je	.L139
.L85:
	subl	%eax, %r8d
	movl	%r8d, %eax
.L86:
	leal	-30(%rax), %edx
	cmpl	$380, %eax
	cmovg	%edx, %eax
	leal	-353(%rax), %edx
	movl	$1, %eax
	cmpl	$3, %edx
	cmovnb	%eax, %edx
	leal	(%rbx,%rbx,2), %eax
	leal	17(,%rax,4), %esi
	movslq	%esi, %rax
	movl	%esi, %edi
	imulq	$1808407283, %rax, %rax
	sarl	$31, %edi
	sarq	$35, %rax
	subl	%edi, %eax
	leal	(%rax,%rax,8), %edi
	leal	(%rax,%rdi,2), %eax
	subl	%eax, %esi
	movl	%esi, %eax
	sarl	$31, %esi
	andl	$-19, %esi
	addl	$12, %esi
	cmpl	%esi, %eax
	jge	.L90
	leaq	_ZL11MONTH_START(%rip), %rsi
	movslq	%edx, %rax
	movswl	(%rsi,%rax,2), %edx
	cmpl	%ecx, %edx
	jge	.L91
	movswl	6(%rsi,%rax,2), %edx
	cmpl	%ecx, %edx
	jge	.L98
	movswl	12(%rsi,%rax,2), %edx
	cmpl	%ecx, %edx
	jge	.L99
	movswl	18(%rsi,%rax,2), %edx
	cmpl	%ecx, %edx
	jl	.L138
.L100:
	movl	$3, %edx
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L80:
	movl	%r14d, %ecx
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L82
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	_ZL16LEAP_MONTH_START(%rip), %rsi
	movslq	%edx, %rax
	movswl	(%rsi,%rax,2), %edx
	cmpl	%edx, %ecx
	jle	.L91
	movswl	6(%rsi,%rax,2), %edx
	cmpl	%edx, %ecx
	jle	.L98
	movswl	12(%rsi,%rax,2), %edx
	cmpl	%edx, %ecx
	jle	.L99
	movswl	18(%rsi,%rax,2), %edx
	cmpl	%edx, %ecx
	jle	.L100
.L138:
	movswl	24(%rsi,%rax,2), %edx
	cmpl	%edx, %ecx
	jle	.L101
	movswl	30(%rsi,%rax,2), %edx
	cmpl	%edx, %ecx
	jle	.L102
	movswl	36(%rsi,%rax,2), %edx
	cmpl	%edx, %ecx
	jle	.L103
	movswl	42(%rsi,%rax,2), %edx
	cmpl	%edx, %ecx
	jle	.L104
	movswl	48(%rsi,%rax,2), %edx
	cmpl	%edx, %ecx
	jle	.L105
	movswl	54(%rsi,%rax,2), %edx
	cmpl	%edx, %ecx
	jle	.L106
	movswl	60(%rsi,%rax,2), %edx
	cmpl	%edx, %ecx
	jle	.L107
	movswl	66(%rsi,%rax,2), %edx
	cmpl	%edx, %ecx
	jle	.L108
	movswl	72(%rsi,%rax,2), %edx
	cmpl	%edx, %ecx
	jle	.L109
	movswl	78(%rsi,%rax,2), %edx
	cmpl	%ecx, %edx
	jge	.L110
.L91:
	movl	$1, (%r12)
.L77:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L140
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	%r14, %rsi
	movl	%ebx, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	movl	-72(%rbp), %r8d
	movl	-68(%rbp), %ecx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L83:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	*%rax
	movl	-68(%rbp), %ecx
	jmp	.L86
.L98:
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L92:
	subl	$1, %edx
	movl	%ecx, 36(%r13)
	movslq	%edx, %rdi
	movl	%edx, 20(%r13)
	movl	$257, %edx
	leaq	(%rdi,%rdi,2), %rdi
	movw	%dx, 104(%r13)
	movl	%ecx, %edx
	movl	$257, %ecx
	addq	%rdi, %rax
	movl	$0, 12(%r13)
	movswl	(%rsi,%rax,2), %esi
	movl	%ebx, 16(%r13)
	movabsq	$4294967297, %rax
	movl	%ebx, 88(%r13)
	subl	%esi, %edx
	movb	$1, 123(%r13)
	movl	$1, 204(%r13)
	movq	%rax, 128(%r13)
	movl	$1, 136(%r13)
	movb	$1, 106(%r13)
	movl	%edx, 32(%r13)
	movq	%rax, 148(%r13)
	movw	%cx, 109(%r13)
	jmp	.L77
.L99:
	movl	$2, %edx
	jmp	.L92
.L101:
	movl	$4, %edx
	jmp	.L92
.L102:
	movl	$5, %edx
	jmp	.L92
.L103:
	movl	$6, %edx
	jmp	.L92
.L104:
	movl	$7, %edx
	jmp	.L92
.L105:
	movl	$8, %edx
	jmp	.L92
.L106:
	movl	$9, %edx
	jmp	.L92
.L107:
	movl	$10, %edx
	jmp	.L92
.L108:
	movl	$11, %edx
	jmp	.L92
.L109:
	movl	$12, %edx
	jmp	.L92
.L110:
	movl	$13, %edx
	jmp	.L92
.L140:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3125:
	.size	_ZN6icu_6714HebrewCalendar19handleComputeFieldsEiR10UErrorCode, .-_ZN6icu_6714HebrewCalendar19handleComputeFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714HebrewCalendar20handleGetMonthLengthEii
	.type	_ZNK6icu_6714HebrewCalendar20handleGetMonthLengthEii, @function
_ZNK6icu_6714HebrewCalendar20handleGetMonthLengthEii:
.LFB3122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jns	.L166
	leal	-1(%rsi), %esi
	leal	(%rsi,%rsi,2), %eax
	leal	17(,%rax,4), %ecx
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L167:
	addl	$13, %ebx
	jns	.L143
.L147:
	subl	$1, %esi
.L148:
	movslq	%ecx, %rax
	movl	%ecx, %edx
	movl	%esi, %r12d
	imulq	$1808407283, %rax, %rax
	sarl	$31, %edx
	sarq	$35, %rax
	subl	%edx, %eax
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,2), %eax
	movl	%ecx, %edx
	subl	$12, %ecx
	subl	%eax, %edx
	movl	%edx, %eax
	cltd
	andl	$-19, %edx
	addl	$12, %edx
	cmpl	%eax, %edx
	jle	.L167
	addl	$12, %ebx
	js	.L147
.L143:
	leal	-1(%rbx), %eax
	cmpl	$1, %eax
	jbe	.L168
.L153:
	movslq	%ebx, %rbx
	leaq	_ZL12MONTH_LENGTH(%rip), %rdx
	leaq	(%rbx,%rbx,2), %rax
	movsbl	(%rdx,%rax), %eax
.L141:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L169
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	cmpl	$12, %edx
	jle	.L143
	leal	(%rsi,%rsi,2), %eax
	leal	17(,%rax,4), %ecx
	.p2align 4,,10
	.p2align 3
.L152:
	movslq	%ecx, %rax
	movl	%ecx, %edx
	movl	%ecx, %esi
	addl	$1, %r12d
	imulq	$1808407283, %rax, %rax
	sarl	$31, %edx
	sarq	$35, %rax
	subl	%edx, %eax
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,2), %eax
	subl	%eax, %esi
	movl	%esi, %eax
	cltd
	andl	$-19, %edx
	addl	$12, %edx
	cmpl	%esi, %edx
	jg	.L150
	subl	$13, %ebx
	addl	$12, %ecx
	cmpl	$12, %ebx
	jg	.L152
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L150:
	subl	$12, %ebx
	addl	$12, %ecx
	cmpl	$12, %ebx
	jg	.L152
	leal	-1(%rbx), %eax
	cmpl	$1, %eax
	ja	.L153
.L168:
	movq	(%rdi), %rax
	leaq	_ZNK6icu_6714HebrewCalendar19handleGetYearLengthEi(%rip), %rdx
	movq	280(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L154
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	movl	$10, %edi
	leal	1(%r12), %r15d
	movl	$0, -60(%rbp)
	call	ucln_i18n_registerCleanup_67@PLT
	leaq	-60(%rbp), %r14
	movl	%r15d, %esi
	leaq	_ZL6gCache(%rip), %rdi
	movq	%r14, %rdx
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L155
	movq	%r14, %rsi
	movl	%r15d, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	movl	%eax, %r13d
.L155:
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	movl	$10, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%r14, %rdx
	movl	%r12d, %esi
	leaq	_ZL6gCache(%rip), %rdi
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	testl	%eax, %eax
	je	.L170
.L156:
	subl	%eax, %r13d
	movl	%r13d, %eax
.L157:
	cmpl	$380, %eax
	leal	-30(%rax), %edx
	movslq	%ebx, %rbx
	cmovg	%edx, %eax
	movl	$1, %edx
	leaq	(%rbx,%rbx,2), %rcx
	subl	$353, %eax
	cmpl	$3, %eax
	cmovnb	%edx, %eax
	leaq	_ZL12MONTH_LENGTH(%rip), %rdx
	addq	%rcx, %rdx
	cltq
	movsbl	(%rdx,%rax), %eax
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L170:
	movq	%r14, %rsi
	movl	%r12d, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	jmp	.L156
.L154:
	movl	%r12d, %esi
	call	*%rax
	jmp	.L157
.L169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3122:
	.size	_ZNK6icu_6714HebrewCalendar20handleGetMonthLengthEii, .-_ZNK6icu_6714HebrewCalendar20handleGetMonthLengthEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714HebrewCalendar23handleComputeMonthStartEiia
	.type	_ZNK6icu_6714HebrewCalendar23handleComputeMonthStartEiia, @function
_ZNK6icu_6714HebrewCalendar23handleComputeMonthStartEiia:
.LFB3127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -64(%rbp)
	testl	%edx, %edx
	jns	.L210
	leal	-1(%rsi), %esi
	leal	(%rsi,%rsi,2), %eax
	leal	17(,%rax,4), %ecx
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L211:
	addl	$13, %ebx
	jns	.L173
.L177:
	subl	$1, %esi
.L178:
	movslq	%ecx, %rax
	movl	%ecx, %edx
	movl	%ecx, %edi
	movl	%esi, %r13d
	imulq	$1808407283, %rax, %rax
	sarl	$31, %edx
	subl	$12, %ecx
	sarq	$35, %rax
	subl	%edx, %eax
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,2), %eax
	subl	%eax, %edi
	movl	%edi, %eax
	cltd
	andl	$-19, %edx
	addl	$12, %edx
	cmpl	%edi, %edx
	jle	.L211
	addl	$12, %ebx
	js	.L177
.L173:
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	movl	$10, %edi
	leaq	-64(%rbp), %r15
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%r15, %rdx
	movl	%r13d, %esi
	leaq	_ZL6gCache(%rip), %rdi
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L183
	movq	%r15, %rsi
	movl	%r13d, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	movl	%eax, %r14d
.L183:
	movl	-64(%rbp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L171
	testl	%ebx, %ebx
	jne	.L212
.L185:
	leal	347997(%r14), %eax
.L171:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L213
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	cmpl	$12, %edx
	jle	.L173
	leal	(%rsi,%rsi,2), %eax
	leal	17(,%rax,4), %ecx
	.p2align 4,,10
	.p2align 3
.L182:
	movslq	%ecx, %rax
	movl	%ecx, %edx
	movl	%ecx, %esi
	addl	$1, %r13d
	imulq	$1808407283, %rax, %rax
	sarl	$31, %edx
	sarq	$35, %rax
	subl	%edx, %eax
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,2), %eax
	subl	%eax, %esi
	movl	%esi, %eax
	cltd
	andl	$-19, %edx
	addl	$12, %edx
	cmpl	%esi, %edx
	jg	.L180
	subl	$13, %ebx
	addl	$12, %ecx
	cmpl	$12, %ebx
	jg	.L182
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L180:
	subl	$12, %ebx
	addl	$12, %ecx
	cmpl	$12, %ebx
	jg	.L182
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L212:
	leal	0(%r13,%r13,2), %eax
	leal	17(,%rax,4), %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$1808407283, %rax, %rax
	sarl	$31, %ecx
	sarq	$35, %rax
	subl	%ecx, %eax
	leal	(%rax,%rax,8), %ecx
	leal	(%rax,%rcx,2), %eax
	movq	(%r12), %rcx
	subl	%eax, %edx
	movl	%edx, %eax
	movq	280(%rcx), %rcx
	cltd
	andl	$-19, %edx
	addl	$12, %edx
	cmpl	%edx, %eax
	leaq	_ZNK6icu_6714HebrewCalendar19handleGetYearLengthEi(%rip), %rax
	jl	.L187
	cmpq	%rax, %rcx
	jne	.L188
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	movl	$10, %edi
	leal	1(%r13), %r15d
	movl	$0, -60(%rbp)
	call	ucln_i18n_registerCleanup_67@PLT
	leaq	-60(%rbp), %r12
	movl	%r15d, %esi
	leaq	_ZL6gCache(%rip), %rdi
	movq	%r12, %rdx
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	movl	%eax, %ecx
	testl	%eax, %eax
	jne	.L189
	movq	%r12, %rsi
	movl	%r15d, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	movl	%eax, %ecx
.L189:
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	movl	$10, %edi
	movl	%ecx, -68(%rbp)
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%r12, %rdx
	movl	%r13d, %esi
	leaq	_ZL6gCache(%rip), %rdi
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	movl	-68(%rbp), %ecx
	testl	%eax, %eax
	jne	.L190
	movq	%r12, %rsi
	movl	%r13d, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	movl	-68(%rbp), %ecx
.L190:
	subl	%eax, %ecx
	movl	%ecx, %eax
.L191:
	cmpl	$380, %eax
	leal	-30(%rax), %edx
	movslq	%ebx, %rbx
	cmovg	%edx, %eax
	movl	$1, %edx
	subl	$353, %eax
	cmpl	$3, %eax
	cmovnb	%edx, %eax
	leaq	(%rbx,%rbx,2), %rdx
	cltq
	addq	%rdx, %rax
	leaq	_ZL16LEAP_MONTH_START(%rip), %rdx
	movswl	(%rdx,%rax,2), %eax
	addl	%eax, %r14d
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L187:
	cmpq	%rax, %rcx
	jne	.L194
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	movl	$10, %edi
	leal	1(%r13), %r15d
	movl	$0, -60(%rbp)
	call	ucln_i18n_registerCleanup_67@PLT
	leaq	-60(%rbp), %r12
	movl	%r15d, %esi
	leaq	_ZL6gCache(%rip), %rdi
	movq	%r12, %rdx
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	movl	%eax, %ecx
	testl	%eax, %eax
	jne	.L195
	movq	%r12, %rsi
	movl	%r15d, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	movl	%eax, %ecx
.L195:
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	movl	$10, %edi
	movl	%ecx, -68(%rbp)
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%r12, %rdx
	movl	%r13d, %esi
	leaq	_ZL6gCache(%rip), %rdi
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	movl	-68(%rbp), %ecx
	testl	%eax, %eax
	jne	.L196
	movq	%r12, %rsi
	movl	%r13d, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	movl	-68(%rbp), %ecx
.L196:
	subl	%eax, %ecx
	movl	%ecx, %eax
.L197:
	cmpl	$380, %eax
	leal	-30(%rax), %edx
	movslq	%ebx, %rbx
	cmovg	%edx, %eax
	movl	$1, %edx
	subl	$353, %eax
	cmpl	$3, %eax
	cmovnb	%edx, %eax
	leaq	(%rbx,%rbx,2), %rdx
	cltq
	addq	%rdx, %rax
	leaq	_ZL11MONTH_START(%rip), %rdx
	movswl	(%rdx,%rax,2), %eax
	addl	%eax, %r14d
	jmp	.L185
.L194:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rcx
	jmp	.L197
.L188:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rcx
	jmp	.L191
.L213:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3127:
	.size	_ZNK6icu_6714HebrewCalendar23handleComputeMonthStartEiia, .-_ZNK6icu_6714HebrewCalendar23handleComputeMonthStartEiia
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendarC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6714HebrewCalendarC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6714HebrewCalendarC2ERKNS_6LocaleER10UErrorCode:
.LFB3101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r12, %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6714HebrewCalendarE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE3101:
	.size	_ZN6icu_6714HebrewCalendarC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6714HebrewCalendarC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6714HebrewCalendarC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6714HebrewCalendarC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6714HebrewCalendarC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendarC2ERKS0_
	.type	_ZN6icu_6714HebrewCalendarC2ERKS0_, @function
_ZN6icu_6714HebrewCalendarC2ERKS0_:
.LFB3110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678CalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714HebrewCalendarE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3110:
	.size	_ZN6icu_6714HebrewCalendarC2ERKS0_, .-_ZN6icu_6714HebrewCalendarC2ERKS0_
	.globl	_ZN6icu_6714HebrewCalendarC1ERKS0_
	.set	_ZN6icu_6714HebrewCalendarC1ERKS0_,_ZN6icu_6714HebrewCalendarC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode
	.type	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode, @function
_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode:
.LFB3116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edi, %r12d
	movl	$10, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%r13, %rdx
	movl	%r12d, %esi
	leaq	_ZL6gCache(%rip), %rdi
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	testl	%eax, %eax
	jne	.L218
	movq	%r13, %rsi
	movl	%r12d, %edi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3116:
	.size	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode, .-_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendar22absoluteDayToDayOfWeekEi
	.type	_ZN6icu_6714HebrewCalendar22absoluteDayToDayOfWeekEi, @function
_ZN6icu_6714HebrewCalendar22absoluteDayToDayOfWeekEi:
.LFB3117:
	.cfi_startproc
	endbr64
	movslq	%edi, %rdx
	movl	%edi, %eax
	imulq	$-1840700269, %rdx, %rdx
	sarl	$31, %eax
	shrq	$32, %rdx
	addl	%edi, %edx
	sarl	$2, %edx
	subl	%eax, %edx
	leal	0(,%rdx,8), %eax
	subl	%edx, %eax
	subl	%eax, %edi
	leal	1(%rdi), %eax
	ret
	.cfi_endproc
.LFE3117:
	.size	_ZN6icu_6714HebrewCalendar22absoluteDayToDayOfWeekEi, .-_ZN6icu_6714HebrewCalendar22absoluteDayToDayOfWeekEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714HebrewCalendar8yearTypeEi
	.type	_ZNK6icu_6714HebrewCalendar8yearTypeEi, @function
_ZNK6icu_6714HebrewCalendar8yearTypeEi:
.LFB3118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_6714HebrewCalendar19handleGetYearLengthEi(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	280(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L223
	leal	1(%rsi), %r14d
	movl	$10, %edi
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	movl	$0, -44(%rbp)
	call	ucln_i18n_registerCleanup_67@PLT
	leaq	-44(%rbp), %r13
	movl	%r14d, %esi
	leaq	_ZL6gCache(%rip), %rdi
	movq	%r13, %rdx
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L224
	movq	%r13, %rsi
	movl	%r14d, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	movl	%eax, %ebx
.L224:
	leaq	calendar_hebrew_cleanup(%rip), %rsi
	movl	$10, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%r13, %rdx
	movl	%r12d, %esi
	leaq	_ZL6gCache(%rip), %rdi
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	testl	%eax, %eax
	je	.L231
.L225:
	subl	%eax, %ebx
	movl	%ebx, %eax
.L226:
	cmpl	$380, %eax
	leal	-30(%rax), %edx
	cmovg	%edx, %eax
	movl	$1, %edx
	subl	$353, %eax
	cmpl	$3, %eax
	cmovnb	%edx, %eax
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L232
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	movq	%r13, %rsi
	movl	%r12d, %edi
	call	_ZN6icu_6714HebrewCalendar11startOfYearEiR10UErrorCode.part.0
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L223:
	call	*%rax
	jmp	.L226
.L232:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3118:
	.size	_ZNK6icu_6714HebrewCalendar8yearTypeEi, .-_ZNK6icu_6714HebrewCalendar8yearTypeEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendar10isLeapYearEi
	.type	_ZN6icu_6714HebrewCalendar10isLeapYearEi, @function
_ZN6icu_6714HebrewCalendar10isLeapYearEi:
.LFB3119:
	.cfi_startproc
	endbr64
	leal	(%rdi,%rdi,2), %eax
	leal	17(,%rax,4), %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$1808407283, %rax, %rax
	sarl	$31, %ecx
	sarq	$35, %rax
	subl	%ecx, %eax
	leal	(%rax,%rax,8), %ecx
	leal	(%rax,%rcx,2), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	cltd
	andl	$-19, %edx
	addl	$12, %edx
	cmpl	%eax, %edx
	setle	%al
	ret
	.cfi_endproc
.LFE3119:
	.size	_ZN6icu_6714HebrewCalendar10isLeapYearEi, .-_ZN6icu_6714HebrewCalendar10isLeapYearEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendar12monthsInYearEi
	.type	_ZN6icu_6714HebrewCalendar12monthsInYearEi, @function
_ZN6icu_6714HebrewCalendar12monthsInYearEi:
.LFB3120:
	.cfi_startproc
	endbr64
	leal	(%rdi,%rdi,2), %eax
	leal	17(,%rax,4), %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$1808407283, %rax, %rax
	sarl	$31, %ecx
	sarq	$35, %rax
	subl	%ecx, %eax
	leal	(%rax,%rax,8), %ecx
	leal	(%rax,%rcx,2), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	cltd
	andl	$-19, %edx
	addl	$12, %edx
	cmpl	%edx, %eax
	setge	%al
	movzbl	%al, %eax
	addl	$12, %eax
	ret
	.cfi_endproc
.LFE3120:
	.size	_ZN6icu_6714HebrewCalendar12monthsInYearEi, .-_ZN6icu_6714HebrewCalendar12monthsInYearEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendar16getStaticClassIDEv
	.type	_ZN6icu_6714HebrewCalendar16getStaticClassIDEv, @function
_ZN6icu_6714HebrewCalendar16getStaticClassIDEv:
.LFB3133:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714HebrewCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3133:
	.size	_ZN6icu_6714HebrewCalendar16getStaticClassIDEv, .-_ZN6icu_6714HebrewCalendar16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendar3addE19UCalendarDateFieldsiR10UErrorCode
	.type	_ZN6icu_6714HebrewCalendar3addE19UCalendarDateFieldsiR10UErrorCode, @function
_ZN6icu_6714HebrewCalendar3addE19UCalendarDateFieldsiR10UErrorCode:
.LFB3112:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L242
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$2, %esi
	je	.L270
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L242:
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rcx, %rdx
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	%eax, %r12d
	testl	%r15d, %r15d
	jle	.L245
	cmpl	$4, %ebx
	leal	(%r15,%rbx), %edx
	setle	%al
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L271:
	subl	$13, %edx
	addl	$1, %r12d
	movl	$1, %eax
.L249:
	cmpl	$4, %edx
	jle	.L246
	testb	%al, %al
	je	.L246
	leal	(%r12,%r12,2), %eax
	leal	17(,%rax,4), %eax
	movslq	%eax, %rsi
	movl	%eax, %ecx
	imulq	$1808407283, %rsi, %rsi
	sarl	$31, %ecx
	sarq	$35, %rsi
	subl	%ecx, %esi
	leal	(%rsi,%rsi,8), %ecx
	leal	(%rsi,%rcx,2), %ecx
	subl	%ecx, %eax
	movl	%eax, %esi
	sarl	$31, %eax
	andl	$-19, %eax
	addl	$12, %eax
	cmpl	%eax, %esi
	setl	%al
	movzbl	%al, %eax
	addl	%eax, %edx
.L246:
	cmpl	$12, %edx
	jg	.L271
.L248:
	movq	%r14, %rdi
	movl	$2, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movl	%r12d, %edx
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movq	(%r14), %rax
	movq	%r13, %rdx
	movq	%r14, %rdi
	movl	$5, %esi
	movq	352(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	cmpl	$5, %ebx
	leal	(%r15,%rbx), %edx
	setg	%al
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L272:
	addl	$13, %edx
	subl	$1, %r12d
	movl	$1, %eax
.L252:
	cmpl	$5, %edx
	jg	.L250
	testb	%al, %al
	je	.L250
	leal	(%r12,%r12,2), %eax
	leal	17(,%rax,4), %ecx
	movslq	%ecx, %rax
	movl	%ecx, %esi
	imulq	$1808407283, %rax, %rax
	sarl	$31, %esi
	sarq	$35, %rax
	subl	%esi, %eax
	leal	(%rax,%rax,8), %esi
	leal	(%rax,%rsi,2), %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	sarl	$31, %ecx
	andl	$-19, %ecx
	addl	$12, %ecx
	cmpl	%ecx, %eax
	setl	%al
	movzbl	%al, %eax
	subl	%eax, %edx
.L250:
	testl	%edx, %edx
	js	.L272
	jmp	.L248
	.cfi_endproc
.LFE3112:
	.size	_ZN6icu_6714HebrewCalendar3addE19UCalendarDateFieldsiR10UErrorCode, .-_ZN6icu_6714HebrewCalendar3addE19UCalendarDateFieldsiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendar3addENS_8Calendar11EDateFieldsEiR10UErrorCode
	.type	_ZN6icu_6714HebrewCalendar3addENS_8Calendar11EDateFieldsEiR10UErrorCode, @function
_ZN6icu_6714HebrewCalendar3addENS_8Calendar11EDateFieldsEiR10UErrorCode:
.LFB3113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	leaq	_ZN6icu_6714HebrewCalendar3addE19UCalendarDateFieldsiR10UErrorCode(%rip), %rcx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L274
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L273
	cmpl	$2, %esi
	je	.L300
	addq	$8, %rsp
	movq	%r13, %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	movq	%r13, %rdx
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	%eax, %r14d
	testl	%r15d, %r15d
	jle	.L277
	cmpl	$4, %ebx
	leal	(%r15,%rbx), %edx
	setle	%al
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L301:
	subl	$13, %edx
	addl	$1, %r14d
	movl	$1, %eax
.L281:
	cmpl	$4, %edx
	jle	.L278
	testb	%al, %al
	je	.L278
	leal	(%r14,%r14,2), %eax
	leal	17(,%rax,4), %eax
	movslq	%eax, %rsi
	movl	%eax, %ecx
	imulq	$1808407283, %rsi, %rsi
	sarl	$31, %ecx
	sarq	$35, %rsi
	subl	%ecx, %esi
	leal	(%rsi,%rsi,8), %ecx
	leal	(%rsi,%rcx,2), %ecx
	subl	%ecx, %eax
	movl	%eax, %esi
	sarl	$31, %eax
	andl	$-19, %eax
	addl	$12, %eax
	cmpl	%eax, %esi
	setl	%al
	movzbl	%al, %eax
	addl	%eax, %edx
.L278:
	cmpl	$12, %edx
	jg	.L301
.L280:
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movl	%r14d, %edx
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movq	(%r12), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	movq	352(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	cmpl	$5, %ebx
	leal	(%r15,%rbx), %edx
	setg	%al
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L302:
	addl	$13, %edx
	subl	$1, %r14d
	movl	$1, %eax
.L284:
	cmpl	$5, %edx
	jg	.L282
	testb	%al, %al
	je	.L282
	leal	(%r14,%r14,2), %eax
	leal	17(,%rax,4), %ecx
	movslq	%ecx, %rax
	movl	%ecx, %esi
	imulq	$1808407283, %rax, %rax
	sarl	$31, %esi
	sarq	$35, %rax
	subl	%esi, %eax
	leal	(%rax,%rax,8), %esi
	leal	(%rax,%rsi,2), %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	sarl	$31, %ecx
	andl	$-19, %ecx
	addl	$12, %ecx
	cmpl	%ecx, %eax
	setl	%al
	movzbl	%al, %eax
	subl	%eax, %edx
.L282:
	testl	%edx, %edx
	js	.L302
	jmp	.L280
	.cfi_endproc
.LFE3113:
	.size	_ZN6icu_6714HebrewCalendar3addENS_8Calendar11EDateFieldsEiR10UErrorCode, .-_ZN6icu_6714HebrewCalendar3addENS_8Calendar11EDateFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.type	_ZN6icu_6714HebrewCalendar4rollE19UCalendarDateFieldsiR10UErrorCode, @function
_ZN6icu_6714HebrewCalendar4rollE19UCalendarDateFieldsiR10UErrorCode:
.LFB3114:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L303
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpl	$2, %esi
	je	.L326
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L303:
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	%rcx, %rdx
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	leal	(%rax,%rax,2), %eax
	leal	17(,%rax,4), %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$1808407283, %rax, %rax
	sarl	$31, %ecx
	sarq	$35, %rax
	subl	%ecx, %eax
	leal	(%rax,%rax,8), %ecx
	leal	(%rax,%rcx,2), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	cltd
	andl	$-19, %edx
	addl	$12, %edx
	cmpl	%edx, %eax
	jl	.L327
	movslq	%r14d, %rax
	movl	%r14d, %edx
	imulq	$1321528399, %rax, %rax
	sarl	$31, %edx
	sarq	$34, %rax
	subl	%edx, %eax
	leal	(%rax,%rax,2), %edx
	leal	(%rax,%rdx,4), %eax
	subl	%eax, %r14d
	leal	(%r14,%rbx), %edx
.L309:
	addl	$13, %edx
	movq	%r13, %rdi
	movl	$2, %esi
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$1321528399, %rax, %rax
	sarl	$31, %ecx
	sarq	$34, %rax
	subl	%ecx, %eax
	leal	(%rax,%rax,2), %ecx
	leal	(%rax,%rcx,4), %eax
	subl	%eax, %edx
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movq	0(%r13), %rax
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r12, %rdx
	movq	%r13, %rdi
	popq	%r12
	.cfi_restore 12
	movl	$5, %esi
	popq	%r13
	.cfi_restore 13
	movq	352(%rax), %rax
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movslq	%r14d, %rax
	movl	%r14d, %edx
	imulq	$715827883, %rax, %rax
	sarl	$31, %edx
	sarq	$33, %rax
	subl	%edx, %eax
	movl	%r14d, %edx
	leal	(%rax,%rax,2), %eax
	sall	$2, %eax
	subl	%eax, %edx
	addl	%ebx, %edx
	testl	%r14d, %r14d
	jle	.L312
	cmpl	$4, %ebx
	jg	.L312
	xorl	%eax, %eax
	cmpl	$4, %edx
	setg	%al
	addl	%eax, %edx
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L312:
	shrl	$31, %r14d
	cmpl	$5, %ebx
	setg	%al
	testb	%r14b, %al
	je	.L309
	cmpl	$5, %edx
	setle	%al
	cmpb	$1, %al
	adcl	$-1, %edx
	jmp	.L309
	.cfi_endproc
.LFE3114:
	.size	_ZN6icu_6714HebrewCalendar4rollE19UCalendarDateFieldsiR10UErrorCode, .-_ZN6icu_6714HebrewCalendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode
	.type	_ZN6icu_6714HebrewCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode, @function
_ZN6icu_6714HebrewCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode:
.LFB3115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	leaq	_ZN6icu_6714HebrewCalendar4rollE19UCalendarDateFieldsiR10UErrorCode(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L329
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L328
	cmpl	$2, %esi
	je	.L350
	popq	%rbx
	movl	%r14d, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	popq	%rbx
	movl	%r14d, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	movq	%rcx, %rdx
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	leal	(%rax,%rax,2), %eax
	leal	17(,%rax,4), %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$1808407283, %rax, %rax
	sarl	$31, %ecx
	sarq	$35, %rax
	subl	%ecx, %eax
	leal	(%rax,%rax,8), %ecx
	leal	(%rax,%rcx,2), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	cltd
	andl	$-19, %edx
	addl	$12, %edx
	cmpl	%edx, %eax
	jl	.L351
	movslq	%r14d, %rax
	movl	%r14d, %edx
	imulq	$1321528399, %rax, %rax
	sarl	$31, %edx
	sarq	$34, %rax
	subl	%edx, %eax
	leal	(%rax,%rax,2), %edx
	leal	(%rax,%rdx,4), %eax
	subl	%eax, %r14d
	leal	(%r14,%rbx), %edx
.L335:
	addl	$13, %edx
	movq	%r12, %rdi
	movl	$2, %esi
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$1321528399, %rax, %rax
	sarl	$31, %ecx
	sarq	$34, %rax
	subl	%ecx, %eax
	leal	(%rax,%rax,2), %ecx
	leal	(%rax,%rcx,4), %eax
	subl	%eax, %edx
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movq	(%r12), %rax
	popq	%rbx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	popq	%r12
	popq	%r13
	movq	352(%rax), %rax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	movslq	%r14d, %rax
	movl	%r14d, %edx
	imulq	$715827883, %rax, %rax
	sarl	$31, %edx
	sarq	$33, %rax
	subl	%edx, %eax
	movl	%r14d, %edx
	leal	(%rax,%rax,2), %eax
	sall	$2, %eax
	subl	%eax, %edx
	addl	%ebx, %edx
	testl	%r14d, %r14d
	jle	.L338
	cmpl	$4, %ebx
	jg	.L338
	xorl	%eax, %eax
	cmpl	$4, %edx
	setg	%al
	addl	%eax, %edx
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L338:
	shrl	$31, %r14d
	cmpl	$5, %ebx
	setg	%al
	testb	%r14b, %al
	je	.L335
	cmpl	$5, %edx
	setle	%al
	cmpb	$1, %al
	adcl	$-1, %edx
	jmp	.L335
	.cfi_endproc
.LFE3115:
	.size	_ZN6icu_6714HebrewCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode, .-_ZN6icu_6714HebrewCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714HebrewCalendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.type	_ZN6icu_6714HebrewCalendar13validateFieldE19UCalendarDateFieldsR10UErrorCode, @function
_ZN6icu_6714HebrewCalendar13validateFieldE19UCalendarDateFieldsR10UErrorCode:
.LFB3124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$2, %esi
	jne	.L353
	movq	(%rdi), %rax
	leaq	_ZN6icu_6714HebrewCalendar21handleGetExtendedYearEv(%rip), %rdx
	movq	288(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L354
	movl	$1, %edx
	movl	$19, %esi
	call	_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_@PLT
	cmpl	$19, %eax
	je	.L365
	movl	132(%r12), %eax
	testl	%eax, %eax
	jle	.L359
	movl	16(%r12), %eax
.L358:
	leal	(%rax,%rax,2), %eax
	leal	17(,%rax,4), %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$1808407283, %rax, %rax
	sarl	$31, %ecx
	sarq	$35, %rax
	subl	%ecx, %eax
	leal	(%rax,%rax,8), %ecx
	leal	(%rax,%rcx,2), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	cltd
	andl	$-19, %edx
	addl	$12, %edx
	cmpl	%edx, %eax
	jl	.L359
.L353:
	addq	$8, %rsp
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	cmpl	$5, 20(%r12)
	jne	.L353
	movl	$1, (%r14)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	call	*%rax
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L365:
	movl	204(%r12), %edx
	testl	%edx, %edx
	jle	.L359
	movl	88(%r12), %eax
	jmp	.L358
	.cfi_endproc
.LFE3124:
	.size	_ZN6icu_6714HebrewCalendar13validateFieldE19UCalendarDateFieldsR10UErrorCode, .-_ZN6icu_6714HebrewCalendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.section	.rodata.str1.1
.LC7:
	.string	"@calendar=hebrew"
	.text
	.p2align 4
	.type	_ZN6icu_67L30initializeSystemDefaultCenturyEv, @function
_ZN6icu_67L30initializeSystemDefaultCenturyEv:
.LFB3130:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	.LC7(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-880(%rbp), %r13
	leaq	-884(%rbp), %r14
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -40
	leaq	-656(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVN6icu_6714HebrewCalendarE(%rip), %rbx
	subq	$864, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -884(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	movq	%rbx, -656(%rbp)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	-884(%rbp), %edx
	testl	%edx, %edx
	jle	.L371
.L367:
	movq	%r12, %rdi
	movq	%rbx, -656(%rbp)
	call	_ZN6icu_678CalendarD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L372
	addq	$864, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movl	-884(%rbp), %eax
	testl	%eax, %eax
	jg	.L368
	movq	%r14, %rcx
	movl	$-80, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode@PLT
.L368:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movsd	%xmm0, _ZN6icu_67L26gSystemDefaultCenturyStartE(%rip)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	%eax, _ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip)
	jmp	.L367
.L372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3130:
	.size	_ZN6icu_67L30initializeSystemDefaultCenturyEv, .-_ZN6icu_67L30initializeSystemDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714HebrewCalendar19defaultCenturyStartEv
	.type	_ZNK6icu_6714HebrewCalendar19defaultCenturyStartEv, @function
_ZNK6icu_6714HebrewCalendar19defaultCenturyStartEv:
.LFB3131:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %eax
	cmpl	$2, %eax
	je	.L381
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L375
	call	_ZN6icu_67L30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L375:
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	.cfi_restore 6
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3131:
	.size	_ZNK6icu_6714HebrewCalendar19defaultCenturyStartEv, .-_ZNK6icu_6714HebrewCalendar19defaultCenturyStartEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714HebrewCalendar23defaultCenturyStartYearEv
	.type	_ZNK6icu_6714HebrewCalendar23defaultCenturyStartYearEv, @function
_ZNK6icu_6714HebrewCalendar23defaultCenturyStartYearEv:
.LFB3132:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %eax
	cmpl	$2, %eax
	je	.L392
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L386
	call	_ZN6icu_67L30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L386:
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore 6
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	ret
	.cfi_endproc
.LFE3132:
	.size	_ZNK6icu_6714HebrewCalendar23defaultCenturyStartYearEv, .-_ZNK6icu_6714HebrewCalendar23defaultCenturyStartYearEv
	.weak	_ZTSN6icu_6714HebrewCalendarE
	.section	.rodata._ZTSN6icu_6714HebrewCalendarE,"aG",@progbits,_ZTSN6icu_6714HebrewCalendarE,comdat
	.align 16
	.type	_ZTSN6icu_6714HebrewCalendarE, @object
	.size	_ZTSN6icu_6714HebrewCalendarE, 26
_ZTSN6icu_6714HebrewCalendarE:
	.string	"N6icu_6714HebrewCalendarE"
	.weak	_ZTIN6icu_6714HebrewCalendarE
	.section	.data.rel.ro._ZTIN6icu_6714HebrewCalendarE,"awG",@progbits,_ZTIN6icu_6714HebrewCalendarE,comdat
	.align 8
	.type	_ZTIN6icu_6714HebrewCalendarE, @object
	.size	_ZTIN6icu_6714HebrewCalendarE, 24
_ZTIN6icu_6714HebrewCalendarE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714HebrewCalendarE
	.quad	_ZTIN6icu_678CalendarE
	.weak	_ZTVN6icu_6714HebrewCalendarE
	.section	.data.rel.ro._ZTVN6icu_6714HebrewCalendarE,"awG",@progbits,_ZTVN6icu_6714HebrewCalendarE,comdat
	.align 8
	.type	_ZTVN6icu_6714HebrewCalendarE, @object
	.size	_ZTVN6icu_6714HebrewCalendarE, 416
_ZTVN6icu_6714HebrewCalendarE:
	.quad	0
	.quad	_ZTIN6icu_6714HebrewCalendarE
	.quad	_ZN6icu_6714HebrewCalendarD1Ev
	.quad	_ZN6icu_6714HebrewCalendarD0Ev
	.quad	_ZNK6icu_6714HebrewCalendar17getDynamicClassIDEv
	.quad	_ZNK6icu_6714HebrewCalendar5cloneEv
	.quad	_ZNK6icu_678CalendareqERKS0_
	.quad	_ZNK6icu_678Calendar14isEquivalentToERKS0_
	.quad	_ZN6icu_6714HebrewCalendar3addENS_8Calendar11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_6714HebrewCalendar3addE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_6714HebrewCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_6714HebrewCalendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6714HebrewCalendar14inDaylightTimeER10UErrorCode
	.quad	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6714HebrewCalendar7getTypeEv
	.quad	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEv
	.quad	_ZN6icu_678Calendar11computeTimeER10UErrorCode
	.quad	_ZN6icu_678Calendar13computeFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode
	.quad	_ZNK6icu_6714HebrewCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.quad	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE
	.quad	_ZNK6icu_6714HebrewCalendar23handleComputeMonthStartEiia
	.quad	_ZNK6icu_6714HebrewCalendar20handleGetMonthLengthEii
	.quad	_ZNK6icu_6714HebrewCalendar19handleGetYearLengthEi
	.quad	_ZN6icu_6714HebrewCalendar21handleGetExtendedYearEv
	.quad	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields
	.quad	_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii
	.quad	_ZN6icu_6714HebrewCalendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar23getFieldResolutionTableEv
	.quad	_ZN6icu_6714HebrewCalendar19handleComputeFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar21getDefaultMonthInYearEi
	.quad	_ZN6icu_678Calendar20getDefaultDayInMonthEii
	.quad	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6714HebrewCalendar18haveDefaultCenturyEv
	.quad	_ZNK6icu_6714HebrewCalendar19defaultCenturyStartEv
	.quad	_ZNK6icu_6714HebrewCalendar23defaultCenturyStartYearEv
	.quad	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode
	.quad	_ZN6icu_678Calendar14setRelatedYearEi
	.local	_ZZN6icu_6714HebrewCalendar16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6714HebrewCalendar16getStaticClassIDEvE7classID,1,1
	.local	_ZN6icu_67L25gSystemDefaultCenturyInitE
	.comm	_ZN6icu_67L25gSystemDefaultCenturyInitE,8,8
	.data
	.align 4
	.type	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, @object
	.size	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, 4
_ZN6icu_67L30gSystemDefaultCenturyStartYearE:
	.long	-1
	.align 8
	.type	_ZN6icu_67L26gSystemDefaultCenturyStartE, @object
	.size	_ZN6icu_67L26gSystemDefaultCenturyStartE, 8
_ZN6icu_67L26gSystemDefaultCenturyStartE:
	.long	0
	.long	1048576
	.local	_ZL6gCache
	.comm	_ZL6gCache,8,8
	.section	.rodata
	.align 32
	.type	_ZL16LEAP_MONTH_START, @object
	.size	_ZL16LEAP_MONTH_START, 84
_ZL16LEAP_MONTH_START:
	.zero	6
	.value	30
	.value	30
	.value	30
	.value	59
	.value	59
	.value	60
	.value	88
	.value	89
	.value	90
	.value	117
	.value	118
	.value	119
	.value	147
	.value	148
	.value	149
	.value	177
	.value	178
	.value	179
	.value	206
	.value	207
	.value	208
	.value	236
	.value	237
	.value	238
	.value	265
	.value	266
	.value	267
	.value	295
	.value	296
	.value	297
	.value	324
	.value	325
	.value	326
	.value	354
	.value	355
	.value	356
	.value	383
	.value	384
	.value	385
	.align 32
	.type	_ZL11MONTH_START, @object
	.size	_ZL11MONTH_START, 84
_ZL11MONTH_START:
	.zero	6
	.value	30
	.value	30
	.value	30
	.value	59
	.value	59
	.value	60
	.value	88
	.value	89
	.value	90
	.value	117
	.value	118
	.value	119
	.value	147
	.value	148
	.value	149
	.value	147
	.value	148
	.value	149
	.value	176
	.value	177
	.value	178
	.value	206
	.value	207
	.value	208
	.value	235
	.value	236
	.value	237
	.value	265
	.value	266
	.value	267
	.value	294
	.value	295
	.value	296
	.value	324
	.value	325
	.value	326
	.value	353
	.value	354
	.value	355
	.align 32
	.type	_ZL12MONTH_LENGTH, @object
	.size	_ZL12MONTH_LENGTH, 39
_ZL12MONTH_LENGTH:
	.ascii	"\036\036\036"
	.ascii	"\035\035\036"
	.ascii	"\035\036\036"
	.ascii	"\035\035\035"
	.ascii	"\036\036\036"
	.ascii	"\036\036\036"
	.ascii	"\035\035\035"
	.ascii	"\036\036\036"
	.ascii	"\035\035\035"
	.ascii	"\036\036\036"
	.ascii	"\035\035\035"
	.ascii	"\036\036\036"
	.ascii	"\035\035\035"
	.align 32
	.type	_ZL6LIMITS, @object
	.size	_ZL6LIMITS, 368
_ZL6LIMITS:
	.zero	16
	.long	-5000000
	.long	-5000000
	.long	5000000
	.long	5000000
	.long	0
	.long	0
	.long	12
	.long	12
	.long	1
	.long	1
	.long	51
	.long	56
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	1
	.long	1
	.long	29
	.long	30
	.long	1
	.long	1
	.long	353
	.long	385
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	5
	.long	5
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-5000000
	.long	-5000000
	.long	5000000
	.long	5000000
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-5000000
	.long	-5000000
	.long	5000000
	.long	5000000
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1087983616
	.align 8
.LC2:
	.long	0
	.long	1093098482
	.align 8
.LC3:
	.long	0
	.long	1077084160
	.align 8
.LC4:
	.long	0
	.long	1080901632
	.align 8
.LC5:
	.long	0
	.long	1080909824
	.align 8
.LC6:
	.long	0
	.long	1072693248
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
