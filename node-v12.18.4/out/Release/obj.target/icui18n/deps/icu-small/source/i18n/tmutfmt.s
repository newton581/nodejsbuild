	.file	"tmutfmt.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TimeUnitFormat17getDynamicClassIDEv
	.type	_ZNK6icu_6714TimeUnitFormat17getDynamicClassIDEv, @function
_ZNK6icu_6714TimeUnitFormat17getDynamicClassIDEv:
.LFB2755:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714TimeUnitFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2755:
	.size	_ZNK6icu_6714TimeUnitFormat17getDynamicClassIDEv, .-_ZNK6icu_6714TimeUnitFormat17getDynamicClassIDEv
	.p2align 4
	.type	tmutfmtHashTableValueComparator, @function
tmutfmtHashTableValueComparator:
.LFB2793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movq	%rsi, %rbx
	movq	(%rsi), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	je	.L3
	movq	8(%r12), %rdi
	movq	8(%rbx), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	setne	%al
.L3:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2793:
	.size	tmutfmtHashTableValueComparator, .-tmutfmtHashTableValueComparator
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TimeUnitFormatReadSinkD2Ev
	.type	_ZN6icu_6722TimeUnitFormatReadSinkD2Ev, @function
_ZN6icu_6722TimeUnitFormatReadSinkD2Ev:
.LFB2783:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6722TimeUnitFormatReadSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE2783:
	.size	_ZN6icu_6722TimeUnitFormatReadSinkD2Ev, .-_ZN6icu_6722TimeUnitFormatReadSinkD2Ev
	.globl	_ZN6icu_6722TimeUnitFormatReadSinkD1Ev
	.set	_ZN6icu_6722TimeUnitFormatReadSinkD1Ev,_ZN6icu_6722TimeUnitFormatReadSinkD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TimeUnitFormatReadSinkD0Ev
	.type	_ZN6icu_6722TimeUnitFormatReadSinkD0Ev, @function
_ZN6icu_6722TimeUnitFormatReadSinkD0Ev:
.LFB2785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722TimeUnitFormatReadSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2785:
	.size	_ZN6icu_6722TimeUnitFormatReadSinkD0Ev, .-_ZN6icu_6722TimeUnitFormatReadSinkD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6714TimeUnitFormat8initHashER10UErrorCode.part.0, @function
_ZN6icu_6714TimeUnitFormat8initHashER10UErrorCode.part.0:
.LFB3813:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$88, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L13
	movl	(%rbx), %ecx
	movq	$0, (%rax)
	testl	%ecx, %ecx
	jle	.L24
.L14:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L12:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	uhash_compareCaselessUnicodeString_67@GOTPCREL(%rip), %rdx
	leaq	8(%rax), %r13
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movq	uhash_hashCaselessUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	uhash_init_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L25
	movq	(%r12), %rdi
.L16:
	testq	%rdi, %rdi
	je	.L14
	call	uhash_close_67@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r13, (%r12)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	uhash_setKeyDeleter_67@PLT
	movl	(%rbx), %eax
	movq	(%r12), %rdi
	testl	%eax, %eax
	jg	.L16
	leaq	tmutfmtHashTableValueComparator(%rip), %rsi
	call	uhash_setValueComparator_67@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L13:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L12
	.cfi_endproc
.LFE3813:
	.size	_ZN6icu_6714TimeUnitFormat8initHashER10UErrorCode.part.0, .-_ZN6icu_6714TimeUnitFormat8initHashER10UErrorCode.part.0
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3170:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3170:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3173:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L39
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L27
	cmpb	$0, 12(%rbx)
	jne	.L40
.L31:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L27:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L31
	.cfi_endproc
.LFE3173:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3176:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L43
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3176:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3179:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L46
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3179:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L52
.L48:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L53
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3181:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3182:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3182:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3183:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3183:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3184:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3184:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3185:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3185:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3186:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3186:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3187:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L69
	testl	%edx, %edx
	jle	.L69
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L72
.L61:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L61
	.cfi_endproc
.LFE3187:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L76
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L76
	testl	%r12d, %r12d
	jg	.L83
	cmpb	$0, 12(%rbx)
	jne	.L84
.L78:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L78
.L84:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3188:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L86
	movq	(%rdi), %r8
.L87:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L90
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L90
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3189:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3190:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L97
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3190:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3191:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3191:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3192:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3192:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3193:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3193:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3195:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3195:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3197:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3197:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormat16getStaticClassIDEv
	.type	_ZN6icu_6714TimeUnitFormat16getStaticClassIDEv, @function
_ZN6icu_6714TimeUnitFormat16getStaticClassIDEv:
.LFB2754:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714TimeUnitFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2754:
	.size	_ZN6icu_6714TimeUnitFormat16getStaticClassIDEv, .-_ZN6icu_6714TimeUnitFormat16getStaticClassIDEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"icudt67l-unit"
.LC1:
	.string	"duration"
.LC2:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormat21readFromCurrentLocaleE20UTimeUnitFormatStylePKcRKNS_7UVectorER10UErrorCode
	.type	_ZN6icu_6714TimeUnitFormat21readFromCurrentLocaleE20UTimeUnitFormatStylePKcRKNS_7UVectorER10UErrorCode, @function
_ZN6icu_6714TimeUnitFormat21readFromCurrentLocaleE20UTimeUnitFormatStylePKcRKNS_7UVectorER10UErrorCode:
.LFB2786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L129
.L104:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	leaq	-100(%rbp), %r15
	movl	%esi, %r12d
	movq	%rdx, %r13
	movq	%rcx, -120(%rbp)
	movq	%r15, %rsi
	movq	%rdi, %rbx
	movl	$0, -100(%rbp)
	call	_ZNK6icu_6713MeasureFormat11getLocaleIDER10UErrorCode@PLT
	movq	%r15, %rdx
	leaq	.LC0(%rip), %rdi
	movq	%rax, %rsi
	call	ures_open_67@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rcx
	movq	%rax, %rdi
	movq	%rax, %r14
	call	ures_getByKey_67@PLT
	movq	%r15, %rcx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ures_getByKey_67@PLT
	movl	-100(%rbp), %eax
	movq	-120(%rbp), %r9
	testl	%eax, %eax
	jg	.L128
	movl	%r12d, -72(%rbp)
	leaq	-96(%rbp), %r12
	movq	%r13, %rdi
	movq	%r15, %rcx
	leaq	16+_ZTVN6icu_6722TimeUnitFormatReadSinkE(%rip), %rax
	movq	%r12, %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rbx, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%r9, -80(%rbp)
	movb	$0, -68(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	leaq	16+_ZTVN6icu_6722TimeUnitFormatReadSinkE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
.L128:
	testq	%r13, %r13
	je	.L109
	movq	%r13, %rdi
	call	ures_close_67@PLT
.L109:
	testq	%r14, %r14
	je	.L104
	movq	%r14, %rdi
	call	ures_close_67@PLT
	jmp	.L104
.L130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2786:
	.size	_ZN6icu_6714TimeUnitFormat21readFromCurrentLocaleE20UTimeUnitFormatStylePKcRKNS_7UVectorER10UErrorCode, .-_ZN6icu_6714TimeUnitFormat21readFromCurrentLocaleE20UTimeUnitFormatStylePKcRKNS_7UVectorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormat15setNumberFormatERKNS_12NumberFormatER10UErrorCode
	.type	_ZN6icu_6714TimeUnitFormat15setNumberFormatERKNS_12NumberFormatER10UErrorCode, @function
_ZN6icu_6714TimeUnitFormat15setNumberFormatERKNS_12NumberFormatER10UErrorCode:
.LFB2790:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L131
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rax
	movq	%rdx, %r12
	call	*32(%rax)
	movq	%r12, %rdx
	movq	%r13, %rdi
	popq	%r12
	.cfi_restore 12
	movq	%rax, %rsi
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713MeasureFormat17adoptNumberFormatEPNS_12NumberFormatER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L131:
	ret
	.cfi_endproc
.LFE2790:
	.size	_ZN6icu_6714TimeUnitFormat15setNumberFormatERKNS_12NumberFormatER10UErrorCode, .-_ZN6icu_6714TimeUnitFormat15setNumberFormatERKNS_12NumberFormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormat10deleteHashEPNS_9HashtableE
	.type	_ZN6icu_6714TimeUnitFormat10deleteHashEPNS_9HashtableE, @function
_ZN6icu_6714TimeUnitFormat10deleteHashEPNS_9HashtableE:
.LFB2791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$-1, -44(%rbp)
	testq	%rsi, %rsi
	je	.L136
	movq	%rsi, %r13
	leaq	-44(%rbp), %rbx
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L155:
	movq	8(%rax), %r12
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L139
	movq	(%rdi), %rax
	call	*8(%rax)
.L139:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L140
	movq	(%rdi), %rax
	call	*8(%rax)
.L140:
	movq	%r12, %rdi
	call	uprv_free_67@PLT
.L141:
	movq	0(%r13), %rdi
	movq	%rbx, %rsi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	jne	.L155
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L143
	call	uhash_close_67@PLT
.L143:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L136:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L156
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L156:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2791:
	.size	_ZN6icu_6714TimeUnitFormat10deleteHashEPNS_9HashtableE, .-_ZN6icu_6714TimeUnitFormat10deleteHashEPNS_9HashtableE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormatD2Ev
	.type	_ZN6icu_6714TimeUnitFormatD2Ev, @function
_ZN6icu_6714TimeUnitFormatD2Ev:
.LFB2769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714TimeUnitFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	424(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	368(%rdi), %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	.p2align 4,,10
	.p2align 3
.L158:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN6icu_6714TimeUnitFormat10deleteHashEPNS_9HashtableE
	movq	$0, -8(%rbx)
	cmpq	%r13, %rbx
	jne	.L158
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713MeasureFormatD2Ev@PLT
	.cfi_endproc
.LFE2769:
	.size	_ZN6icu_6714TimeUnitFormatD2Ev, .-_ZN6icu_6714TimeUnitFormatD2Ev
	.globl	_ZN6icu_6714TimeUnitFormatD1Ev
	.set	_ZN6icu_6714TimeUnitFormatD1Ev,_ZN6icu_6714TimeUnitFormatD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormat15initDataMembersER10UErrorCode
	.type	_ZN6icu_6714TimeUnitFormat15initDataMembersER10UErrorCode, @function
_ZN6icu_6714TimeUnitFormat15initDataMembersER10UErrorCode:
.LFB2777:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L167
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	424(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	368(%rdi), %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L164:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN6icu_6714TimeUnitFormat10deleteHashEPNS_9HashtableE
	movq	$0, -8(%rbx)
	cmpq	%r13, %rbx
	jne	.L164
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE2777:
	.size	_ZN6icu_6714TimeUnitFormat15initDataMembersER10UErrorCode, .-_ZN6icu_6714TimeUnitFormat15initDataMembersER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormatD0Ev
	.type	_ZN6icu_6714TimeUnitFormatD0Ev, @function
_ZN6icu_6714TimeUnitFormatD0Ev:
.LFB2771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714TimeUnitFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	424(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	368(%rdi), %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	.p2align 4,,10
	.p2align 3
.L171:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN6icu_6714TimeUnitFormat10deleteHashEPNS_9HashtableE
	movq	$0, -8(%rbx)
	cmpq	%r13, %rbx
	jne	.L171
	movq	%r12, %rdi
	call	_ZN6icu_6713MeasureFormatD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2771:
	.size	_ZN6icu_6714TimeUnitFormatD0Ev, .-_ZN6icu_6714TimeUnitFormatD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormat8copyHashEPKNS_9HashtableEPS1_R10UErrorCode
	.type	_ZN6icu_6714TimeUnitFormat8copyHashEPKNS_9HashtableEPS1_R10UErrorCode, @function
_ZN6icu_6714TimeUnitFormat8copyHashEPKNS_9HashtableEPS1_R10UErrorCode:
.LFB2792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -160(%rbp)
	movl	(%rcx), %edx
	movq	%rsi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L174
	movl	$-1, -132(%rbp)
	testq	%rsi, %rsi
	je	.L174
	leaq	-132(%rbp), %rax
	movq	%rcx, %rbx
	leaq	-128(%rbp), %r13
	movq	%rax, -168(%rbp)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L200:
	movq	8(%rax), %r15
	movl	$16, %edi
	movq	16(%rax), %r14
	call	uprv_malloc_67@PLT
	movq	(%r15), %rdi
	movq	%rax, %r12
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rax, (%r12)
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, 8(%r12)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L176
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
.L198:
	movq	-160(%rbp), %rax
	movq	(%rax), %rdi
	call	uhash_put_67@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L199
.L179:
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %rsi
	movq	(%rax), %rdi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	jne	.L200
.L174:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L201
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L180
	movq	(%rdi), %rax
	call	*8(%rax)
.L180:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L181
	movq	(%rdi), %rax
	call	*8(%rax)
.L181:
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	jmp	.L174
.L176:
	movq	%rbx, %rcx
	movq	%r12, %rdx
	xorl	%esi, %esi
	jmp	.L198
.L201:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2792:
	.size	_ZN6icu_6714TimeUnitFormat8copyHashEPKNS_9HashtableEPS1_R10UErrorCode, .-_ZN6icu_6714TimeUnitFormat8copyHashEPKNS_9HashtableEPS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormatC2ERKS0_
	.type	_ZN6icu_6714TimeUnitFormatC2ERKS0_, @function
_ZN6icu_6714TimeUnitFormatC2ERKS0_:
.LFB2766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$368, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713MeasureFormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714TimeUnitFormatE(%rip), %rax
	movq	%rax, 0(%r13)
	movl	424(%r14), %eax
	movl	%eax, 424(%r13)
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-60(%rbp), %edx
	movq	$0, 0(%r13,%rbx)
	testl	%edx, %edx
	jle	.L226
.L204:
	movq	$0, 0(%r13,%rbx)
.L212:
	addq	$8, %rbx
	cmpq	$424, %rbx
	je	.L227
.L214:
	movl	$88, %edi
	movl	$0, -60(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L204
	movl	-60(%rbp), %edi
	movq	$0, (%rax)
	testl	%edi, %edi
	jg	.L205
	movq	uhash_hashCaselessUnicodeString_67@GOTPCREL(%rip), %rsi
	leaq	-60(%rbp), %r15
	leaq	8(%rax), %rdi
	xorl	%ecx, %ecx
	movq	uhash_compareCaselessUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	%r15, %r8
	movq	%rdi, -72(%rbp)
	call	uhash_init_67@PLT
	movl	-60(%rbp), %esi
	movq	-72(%rbp), %rdi
	testl	%esi, %esi
	jle	.L228
	movq	(%r12), %rdi
.L207:
	testq	%rdi, %rdi
	je	.L205
	call	uhash_close_67@PLT
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L227:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L229
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	%rdi, (%r12)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	call	uhash_setKeyDeleter_67@PLT
	movl	-60(%rbp), %ecx
	movq	(%r12), %rdi
	testl	%ecx, %ecx
	jg	.L207
	leaq	tmutfmtHashTableValueComparator(%rip), %rsi
	call	uhash_setValueComparator_67@PLT
	movl	-60(%rbp), %eax
	movq	%r12, 0(%r13,%rbx)
	testl	%eax, %eax
	jle	.L209
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L213
	call	uhash_close_67@PLT
.L213:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L226:
	xorl	%r12d, %r12d
	leaq	-60(%rbp), %r15
.L209:
	movq	(%r14,%rbx), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6714TimeUnitFormat8copyHashEPKNS_9HashtableEPS1_R10UErrorCode
	jmp	.L212
.L229:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2766:
	.size	_ZN6icu_6714TimeUnitFormatC2ERKS0_, .-_ZN6icu_6714TimeUnitFormatC2ERKS0_
	.globl	_ZN6icu_6714TimeUnitFormatC1ERKS0_
	.set	_ZN6icu_6714TimeUnitFormatC1ERKS0_,_ZN6icu_6714TimeUnitFormatC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormataSERKS0_
	.type	_ZN6icu_6714TimeUnitFormataSERKS0_, @function
_ZN6icu_6714TimeUnitFormataSERKS0_:
.LFB2773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L231
	movq	%rsi, %rbx
	leaq	368(%r13), %r12
	leaq	424(%r13), %r14
	call	_ZN6icu_6713MeasureFormataSERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L232:
	movq	(%r12), %rsi
	movq	%r13, %rdi
	addq	$8, %r12
	call	_ZN6icu_6714TimeUnitFormat10deleteHashEPNS_9HashtableE
	movq	$0, -8(%r12)
	cmpq	%r14, %r12
	jne	.L232
	movl	$368, %r15d
	leaq	-60(%rbp), %r14
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L233:
	testq	%r12, %r12
	je	.L235
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L236
	call	uhash_close_67@PLT
.L236:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L235:
	movq	$0, 0(%r13,%r15)
	addq	$8, %r15
	cmpq	$424, %r15
	je	.L248
.L237:
	movq	%r14, %rdi
	movl	$0, -60(%rbp)
	call	_ZN6icu_6714TimeUnitFormat8initHashER10UErrorCode.part.0
	movq	%rax, 0(%r13,%r15)
	movq	%rax, %r12
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L233
	movq	(%rbx,%r15), %rsi
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	addq	$8, %r15
	call	_ZN6icu_6714TimeUnitFormat8copyHashEPKNS_9HashtableEPS1_R10UErrorCode
	cmpq	$424, %r15
	jne	.L237
.L248:
	movl	424(%rbx), %eax
	movl	%eax, 424(%r13)
.L231:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L249
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L249:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2773:
	.size	_ZN6icu_6714TimeUnitFormataSERKS0_, .-_ZN6icu_6714TimeUnitFormataSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TimeUnitFormat5cloneEv
	.type	_ZNK6icu_6714TimeUnitFormat5cloneEv, @function
_ZNK6icu_6714TimeUnitFormat5cloneEv:
.LFB2772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$432, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L250
	movq	%rax, %rdi
	movq	%r14, %rsi
	movl	$368, %ebx
	call	_ZN6icu_6713MeasureFormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714TimeUnitFormatE(%rip), %rax
	movq	%rax, 0(%r13)
	movl	424(%r14), %eax
	movl	%eax, 424(%r13)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L254:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-60(%rbp), %edx
	movq	$0, 0(%r13,%rbx)
	testl	%edx, %edx
	jle	.L278
.L253:
	movq	$0, 0(%r13,%rbx)
.L261:
	addq	$8, %rbx
	cmpq	$424, %rbx
	je	.L250
.L264:
	movl	$88, %edi
	movl	$0, -60(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L253
	movl	-60(%rbp), %edi
	movq	$0, (%rax)
	testl	%edi, %edi
	jg	.L254
	movq	uhash_hashCaselessUnicodeString_67@GOTPCREL(%rip), %rsi
	leaq	-60(%rbp), %r15
	leaq	8(%rax), %rdi
	xorl	%ecx, %ecx
	movq	uhash_compareCaselessUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	%r15, %r8
	movq	%rdi, -72(%rbp)
	call	uhash_init_67@PLT
	movl	-60(%rbp), %esi
	movq	-72(%rbp), %rdi
	testl	%esi, %esi
	jle	.L279
	movq	(%r12), %rdi
.L256:
	testq	%rdi, %rdi
	je	.L254
	call	uhash_close_67@PLT
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L250:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L280
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	movq	%rdi, (%r12)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	call	uhash_setKeyDeleter_67@PLT
	movl	-60(%rbp), %ecx
	movq	(%r12), %rdi
	testl	%ecx, %ecx
	jg	.L256
	leaq	tmutfmtHashTableValueComparator(%rip), %rsi
	call	uhash_setValueComparator_67@PLT
	movl	-60(%rbp), %eax
	movq	%r12, 0(%r13,%rbx)
	testl	%eax, %eax
	jle	.L258
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L262
	call	uhash_close_67@PLT
.L262:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L278:
	xorl	%r12d, %r12d
	leaq	-60(%rbp), %r15
.L258:
	movq	(%r14,%rbx), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6714TimeUnitFormat8copyHashEPKNS_9HashtableEPS1_R10UErrorCode
	jmp	.L261
.L280:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2772:
	.size	_ZNK6icu_6714TimeUnitFormat5cloneEv, .-_ZNK6icu_6714TimeUnitFormat5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormat8initHashER10UErrorCode
	.type	_ZN6icu_6714TimeUnitFormat8initHashER10UErrorCode, @function
_ZN6icu_6714TimeUnitFormat8initHashER10UErrorCode:
.LFB2794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rsi), %edi
	testl	%edi, %edi
	jg	.L281
	movl	$88, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L283
	movl	(%rbx), %ecx
	movq	$0, (%rax)
	testl	%ecx, %ecx
	jle	.L294
.L284:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L281:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	movq	uhash_compareCaselessUnicodeString_67@GOTPCREL(%rip), %rdx
	leaq	8(%rax), %r13
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movq	uhash_hashCaselessUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	uhash_init_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L295
	movq	(%r12), %rdi
.L286:
	testq	%rdi, %rdi
	je	.L284
	call	uhash_close_67@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%r13, (%r12)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	uhash_setKeyDeleter_67@PLT
	movl	(%rbx), %eax
	movq	(%r12), %rdi
	testl	%eax, %eax
	jg	.L286
	leaq	tmutfmtHashTableValueComparator(%rip), %rsi
	call	uhash_setValueComparator_67@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L283:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L281
	.cfi_endproc
.LFE2794:
	.size	_ZN6icu_6714TimeUnitFormat8initHashER10UErrorCode, .-_ZN6icu_6714TimeUnitFormat8initHashER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormat15getTimeUnitNameENS_8TimeUnit15UTimeUnitFieldsER10UErrorCode
	.type	_ZN6icu_6714TimeUnitFormat15getTimeUnitNameENS_8TimeUnit15UTimeUnitFieldsER10UErrorCode, @function
_ZN6icu_6714TimeUnitFormat15getTimeUnitNameENS_8TimeUnit15UTimeUnitFieldsER10UErrorCode:
.LFB2795:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L296
	cmpl	$6, %edi
	ja	.L298
	leaq	.L300(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L300:
	.long	.L307-.L300
	.long	.L305-.L300
	.long	.L304-.L300
	.long	.L303-.L300
	.long	.L302-.L300
	.long	.L301-.L300
	.long	.L299-.L300
	.text
.L305:
	leaq	_ZN6icu_67L14gTimeUnitMonthE(%rip), %rax
	ret
.L307:
	leaq	_ZN6icu_67L13gTimeUnitYearE(%rip), %rax
.L296:
	ret
.L301:
	leaq	_ZN6icu_67L15gTimeUnitMinuteE(%rip), %rax
	ret
.L299:
	leaq	_ZN6icu_67L15gTimeUnitSecondE(%rip), %rax
	ret
.L304:
	leaq	_ZN6icu_67L12gTimeUnitDayE(%rip), %rax
	ret
.L303:
	leaq	_ZN6icu_67L13gTimeUnitWeekE(%rip), %rax
	ret
.L302:
	leaq	_ZN6icu_67L13gTimeUnitHourE(%rip), %rax
	ret
.L298:
	movl	$1, (%rsi)
	ret
	.cfi_endproc
.LFE2795:
	.size	_ZN6icu_6714TimeUnitFormat15getTimeUnitNameENS_8TimeUnit15UTimeUnitFieldsER10UErrorCode, .-_ZN6icu_6714TimeUnitFormat15getTimeUnitNameENS_8TimeUnit15UTimeUnitFieldsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TimeUnitFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.type	_ZNK6icu_6714TimeUnitFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, @function
_ZNK6icu_6714TimeUnitFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE:
.LFB2774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -568(%rbp)
	movq	%rsi, -504(%rbp)
	movq	%rdx, -608(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-400(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN6icu_6711FormattableC1Ed@PLT
	movl	8(%r14), %eax
	movb	$0, -549(%rbp)
	movq	$0, -536(%rbp)
	movl	%eax, -484(%rbp)
	leaq	-472(%rbp), %rax
	movq	$0, -560(%rbp)
	movl	$0, -488(%rbp)
	movl	$-1, -576(%rbp)
	movl	$7, -572(%rbp)
	movq	%rax, -528(%rbp)
	.p2align 4,,10
	.p2align 3
.L320:
	movq	-536(%rbp), %rax
	movq	-568(%rbp), %rcx
	movl	$-1, -472(%rbp)
	movl	%eax, -548(%rbp)
	movq	368(%rcx,%rax,8), %rax
	movq	%rax, -512(%rbp)
	.p2align 4,,10
	.p2align 3
.L318:
	movq	-512(%rbp), %rax
	movq	-528(%rbp), %rsi
	movq	(%rax), %rdi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	je	.L309
	movq	16(%rax), %rcx
	movq	8(%rax), %rax
	xorl	%r13d, %r13d
	leaq	-288(%rbp), %rbx
	leaq	-176(%rbp), %r15
	movq	%rcx, -520(%rbp)
	movq	%rax, -496(%rbp)
.L319:
	movq	-496(%rbp), %rax
	movq	%rbx, %rdi
	movq	(%rax,%r13,8), %r12
	movl	-484(%rbp), %eax
	movl	$-1, 12(%r14)
	movl	%eax, 8(%r14)
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	(%r12), %rax
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	-504(%rbp), %rsi
	movq	%r12, %rdi
	call	*56(%rax)
	cmpl	$-1, 12(%r14)
	jne	.L310
	movl	-484(%rbp), %eax
	cmpl	8(%r14), %eax
	je	.L310
	movq	%r15, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN6icu_6711FormattableC1Ed@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_6713MessageFormat15getArgTypeCountEv@PLT
	testl	%eax, %eax
	jne	.L359
.L311:
	movl	8(%r14), %edx
	subl	-484(%rbp), %edx
	cmpl	-488(%rbp), %edx
	jg	.L360
.L314:
	movq	%r15, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
.L310:
	movq	%rbx, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	cmpq	$1, %r13
	je	.L318
	movl	$1, %r13d
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L359:
	movq	-280(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -544(%rbp)
	call	_ZNK6icu_6711Formattable7getTypeEv@PLT
	movq	-544(%rbp), %rsi
	cmpl	$3, %eax
	je	.L361
	movq	%rsi, %rdi
	movq	%rsi, -544(%rbp)
	call	_ZNK6icu_6711Formattable9isNumericEv@PLT
	testb	%al, %al
	je	.L314
	movq	-544(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L360:
	movq	%r12, %rdi
	movl	%edx, -488(%rbp)
	call	_ZNK6icu_6713MessageFormat15getArgTypeCountEv@PLT
	movb	$0, -549(%rbp)
	movl	-488(%rbp), %edx
	testl	%eax, %eax
	jne	.L362
.L316:
	movl	8(%r14), %eax
	movl	%edx, -488(%rbp)
	movl	%eax, -576(%rbp)
	movq	-520(%rbp), %rax
	movq	%rax, -560(%rbp)
	movl	-548(%rbp), %eax
	movl	%eax, -572(%rbp)
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L309:
	addq	$1, -536(%rbp)
	movq	-536(%rbp), %rax
	cmpq	$7, %rax
	jne	.L320
	cmpb	$0, -549(%rbp)
	jne	.L321
	movl	-488(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L363
.L333:
	movl	-484(%rbp), %eax
	movl	$0, 12(%r14)
	movl	%eax, 8(%r14)
.L332:
	movq	-600(%rbp), %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L364
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movq	-568(%rbp), %rdi
	movl	$2, %esi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$0, -468(%rbp)
	movw	%si, -456(%rbp)
	movq	%rax, -464(%rbp)
	call	_ZNK6icu_6713MeasureFormat23getNumberFormatInternalEv@PLT
	movq	-544(%rbp), %rsi
	leaq	-464(%rbp), %r9
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	%r9, %rdi
	movq	%r9, -544(%rbp)
	movq	8(%rsi), %rsi
	movq	%r8, -592(%rbp)
	movq	168(%rax), %r10
	movq	%r10, -584(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-544(%rbp), %r9
	movq	%r15, %rdx
	movq	-592(%rbp), %r8
	movq	-584(%rbp), %r10
	leaq	-468(%rbp), %rcx
	movq	%r9, %rsi
	movq	%r8, %rdi
	call	*%r10
	movl	-468(%rbp), %edi
	movq	-544(%rbp), %r9
	testl	%edi, %edi
	movq	%r9, %rdi
	jg	.L365
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L362:
	movq	-600(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	movb	$1, -549(%rbp)
	movl	-488(%rbp), %edx
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L365:
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L314
.L363:
	movq	-560(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L322
	sarl	$5, %eax
	movl	%eax, %edx
.L323:
	movq	-560(%rbp), %rdi
	movl	$4, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	leaq	_ZN6icu_67L17PLURAL_COUNT_ZEROE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L17PLURAL_COUNT_ZEROE(%rip), %rdx
	testb	%al, %al
	jne	.L324
	leaq	-176(%rbp), %r12
	pxor	%xmm0, %xmm0
.L358:
	movq	%r12, %rdi
	call	_ZN6icu_6711FormattableC1Ed@PLT
	movq	-600(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
.L325:
	movl	$0, -468(%rbp)
	movl	$128, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L333
	movl	-572(%rbp), %edx
	movq	-600(%rbp), %rsi
	movq	%rax, %rdi
	leaq	-468(%rbp), %rcx
	call	_ZN6icu_6714TimeUnitAmountC1ERKNS_11FormattableENS_8TimeUnit15UTimeUnitFieldsER10UErrorCode@PLT
	movl	-468(%rbp), %eax
	testl	%eax, %eax
	jle	.L366
	movl	-484(%rbp), %eax
	movl	$0, 12(%r14)
	movq	%rbx, %rdi
	movl	%eax, 8(%r14)
	call	_ZN6icu_6714TimeUnitAmountD0Ev@PLT
	jmp	.L332
.L321:
	movl	-488(%rbp), %edx
	testl	%edx, %edx
	je	.L333
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L322:
	movq	-560(%rbp), %rax
	movl	12(%rax), %edx
	jmp	.L323
.L324:
	movq	-560(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L326
	sarl	$5, %eax
	movl	%eax, %edx
.L327:
	movq	-560(%rbp), %rdi
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	leaq	_ZN6icu_67L16PLURAL_COUNT_ONEE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L16PLURAL_COUNT_ONEE(%rip), %rdx
	testb	%al, %al
	jne	.L328
	movsd	.LC4(%rip), %xmm0
	leaq	-176(%rbp), %r12
	jmp	.L358
.L366:
	movq	-608(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6711Formattable11adoptObjectEPNS_7UObjectE@PLT
	movl	-576(%rbp), %eax
	movl	$-1, 12(%r14)
	movl	%eax, 8(%r14)
	jmp	.L332
.L328:
	movq	-560(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L329
	sarl	$5, %eax
	movl	%eax, %edx
.L330:
	movq	-560(%rbp), %rdi
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	leaq	_ZN6icu_67L16PLURAL_COUNT_TWOE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L16PLURAL_COUNT_TWOE(%rip), %rdx
	movsd	.LC5(%rip), %xmm0
	leaq	-176(%rbp), %r12
	testb	%al, %al
	je	.L358
	movsd	.LC6(%rip), %xmm0
	jmp	.L358
.L326:
	movq	-560(%rbp), %rax
	movl	12(%rax), %edx
	jmp	.L327
.L329:
	movq	-560(%rbp), %rax
	movl	12(%rax), %edx
	jmp	.L330
.L364:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2774:
	.size	_ZNK6icu_6714TimeUnitFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, .-_ZNK6icu_6714TimeUnitFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormat19searchInLocaleChainE20UTimeUnitFormatStylePKcS3_NS_8TimeUnit15UTimeUnitFieldsERKNS_13UnicodeStringES3_PNS_9HashtableER10UErrorCode
	.type	_ZN6icu_6714TimeUnitFormat19searchInLocaleChainE20UTimeUnitFormatStylePKcS3_NS_8TimeUnit15UTimeUnitFieldsERKNS_13UnicodeStringES3_PNS_9HashtableER10UErrorCode, @function
_ZN6icu_6714TimeUnitFormat19searchInLocaleChainE20UTimeUnitFormatStylePKcS3_NS_8TimeUnit15UTimeUnitFieldsERKNS_13UnicodeStringES3_PNS_9HashtableER10UErrorCode:
.LFB2788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movl	%esi, -544(%rbp)
	movq	%rdi, -584(%rbp)
	movq	%rax, -552(%rbp)
	movq	24(%rbp), %rax
	movq	%rdx, -560(%rbp)
	movq	%rax, -600(%rbp)
	movq	32(%rbp), %rax
	movq	%rcx, -576(%rbp)
	movl	%r8d, -540(%rbp)
	movl	(%rax), %esi
	movq	%r9, -592(%rbp)
	movq	%rax, -568(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testl	%esi, %esi
	jle	.L473
.L367:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L474
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	leaq	-224(%rbp), %r14
	movl	$157, %edx
	movq	%rcx, %rsi
	movl	$0, -528(%rbp)
	movq	%r14, %rdi
	leaq	-528(%rbp), %rbx
	call	__strcpy_chk@PLT
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%rbx, %rcx
	movl	$157, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	uloc_getParent_67@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	js	.L402
	movq	%rbx, %rdx
	movq	%r14, %rsi
	leaq	.LC0(%rip), %rdi
	call	ures_open_67@PLT
	movq	-560(%rbp), %rsi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ures_getByKey_67@PLT
	movl	-528(%rbp), %ecx
	xorl	%esi, %esi
	movq	%rax, %r15
	testl	%ecx, %ecx
	jg	.L370
	movl	-540(%rbp), %eax
	cmpl	$6, %eax
	ja	.L371
	leaq	.L373(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L373:
	.long	.L421-.L373
	.long	.L378-.L373
	.long	.L377-.L373
	.long	.L376-.L373
	.long	.L375-.L373
	.long	.L374-.L373
	.long	.L372-.L373
	.text
.L378:
	leaq	_ZN6icu_67L14gTimeUnitMonthE(%rip), %rsi
.L370:
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movq	%r15, %rdi
	call	ures_getByKey_67@PLT
	movq	-552(%rbp), %rsi
	leaq	-524(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%rax, %rdi
	movq	%rax, -536(%rbp)
	call	ures_getStringByKeyWithFallback_67@PLT
	movl	-528(%rbp), %edx
	movq	-536(%rbp), %rdi
	testl	%edx, %edx
	jle	.L475
	movl	$0, -528(%rbp)
	testl	%r13d, %r13d
	je	.L391
	testq	%rdi, %rdi
	je	.L392
	call	ures_close_67@PLT
.L392:
	testq	%r15, %r15
	je	.L393
	movq	%r15, %rdi
	call	ures_close_67@PLT
.L393:
	testq	%r12, %r12
	je	.L395
	movq	%r12, %rdi
	call	ures_close_67@PLT
	jmp	.L395
.L421:
	leaq	_ZN6icu_67L13gTimeUnitYearE(%rip), %rsi
	jmp	.L370
.L372:
	leaq	_ZN6icu_67L15gTimeUnitSecondE(%rip), %rsi
	jmp	.L370
.L374:
	leaq	_ZN6icu_67L15gTimeUnitMinuteE(%rip), %rsi
	jmp	.L370
.L375:
	leaq	_ZN6icu_67L13gTimeUnitHourE(%rip), %rsi
	jmp	.L370
.L376:
	leaq	_ZN6icu_67L13gTimeUnitWeekE(%rip), %rsi
	jmp	.L370
.L377:
	leaq	_ZN6icu_67L12gTimeUnitDayE(%rip), %rsi
	jmp	.L370
.L371:
	movl	$1, -528(%rbp)
	jmp	.L370
.L477:
	movq	-600(%rbp), %rax
	movq	-592(%rbp), %rsi
	movq	(%rax), %rdi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L400
	movslq	-544(%rbp), %rdx
	cmpq	$0, (%rax,%rdx,8)
	jne	.L399
.L400:
	cmpb	$0, -436(%rbp)
	je	.L402
	movq	-448(%rbp), %rdi
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L402:
	movq	-552(%rbp), %rdi
	leaq	_ZN6icu_67L17gPluralCountOtherE(%rip), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L404
	movl	-540(%rbp), %eax
	cmpl	$6, %eax
	je	.L422
	cmpl	$5, %eax
	je	.L423
	cmpl	$4, %eax
	je	.L424
	cmpl	$3, %eax
	je	.L425
	cmpl	$2, %eax
	je	.L426
	cmpl	$1, %eax
	je	.L427
	testl	%eax, %eax
	jne	.L406
	leaq	_ZN6icu_67L24DEFAULT_PATTERN_FOR_YEARE(%rip), %rax
	.p2align 4,,10
	.p2align 3
.L405:
	leaq	-512(%rbp), %r13
	movl	$-1, %ecx
	leaq	-520(%rbp), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -520(%rbp)
	leaq	-448(%rbp), %r14
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-568(%rbp), %rbx
	movq	-584(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	call	_ZNK6icu_6713MeasureFormat9getLocaleER10UErrorCode@PLT
	movl	$816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L407
	movq	%rax, %rdi
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN6icu_6713MessageFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.L408
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6713MessageFormatD0Ev@PLT
.L408:
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-568(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jle	.L418
.L409:
	testq	%r12, %r12
	je	.L367
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L404:
	subq	$8, %rsp
	leaq	_ZN6icu_67L17gPluralCountOtherE(%rip), %rax
	pushq	-568(%rbp)
	movq	-592(%rbp), %r9
	movl	-540(%rbp), %r8d
	pushq	-600(%rbp)
	movq	-576(%rbp), %rcx
	pushq	%rax
	movq	-560(%rbp), %rdx
	movl	-544(%rbp), %esi
	movq	-584(%rbp), %rdi
	call	_ZN6icu_6714TimeUnitFormat19searchInLocaleChainE20UTimeUnitFormatStylePKcS3_NS_8TimeUnit15UTimeUnitFieldsERKNS_13UnicodeStringES3_PNS_9HashtableER10UErrorCode
	addq	$32, %rsp
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L475:
	movl	-524(%rbp), %ecx
	leaq	-512(%rbp), %r13
	leaq	-520(%rbp), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -520(%rbp)
	leaq	-448(%rbp), %r14
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-568(%rbp), %rdx
	movq	-584(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713MeasureFormat9getLocaleER10UErrorCode@PLT
	movl	$816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-536(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L380
	movq	-568(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713MessageFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode@PLT
	movq	-536(%rbp), %r9
.L381:
	movq	%r14, %rdi
	movq	%r9, -536(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-568(%rbp), %rax
	movq	-536(%rbp), %r9
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jle	.L476
.L382:
	testq	%rbx, %rbx
	je	.L386
	movq	(%rbx), %rax
	movq	%r9, -536(%rbp)
	movq	%rbx, %rdi
	call	*8(%rax)
	movq	-536(%rbp), %r9
.L386:
	testq	%r9, %r9
	je	.L387
	movq	%r9, %rdi
	call	ures_close_67@PLT
.L387:
	testq	%r15, %r15
	je	.L388
	movq	%r15, %rdi
	call	ures_close_67@PLT
.L388:
	testq	%r12, %r12
	je	.L367
	movq	%r12, %rdi
	call	ures_close_67@PLT
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L391:
	testq	%rdi, %rdi
	je	.L396
	call	ures_close_67@PLT
.L396:
	testq	%r15, %r15
	je	.L397
	movq	%r15, %rdi
	call	ures_close_67@PLT
.L397:
	testq	%r12, %r12
	je	.L398
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L398:
	movq	-560(%rbp), %rdi
	leaq	_ZN6icu_67L14gShortUnitsTagE(%rip), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L402
	movq	-568(%rbp), %rbx
	movq	-576(%rbp), %rsi
	xorl	%r8d, %r8d
	leaq	-448(%rbp), %r12
	leaq	-435(%rbp), %rax
	movl	$-1, %edx
	movq	%r12, %rdi
	movw	%r8w, -436(%rbp)
	movq	%rbx, %rcx
	movq	%rax, -448(%rbp)
	movl	$0, -392(%rbp)
	movl	$40, -440(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%rbx, %rdx
	movl	$95, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	subq	$8, %rsp
	movq	-592(%rbp), %r9
	movl	-540(%rbp), %r8d
	pushq	%rbx
	movq	-448(%rbp), %rcx
	leaq	_ZN6icu_67L9gUnitsTagE(%rip), %rdx
	pushq	-600(%rbp)
	movl	-544(%rbp), %esi
	movq	-584(%rbp), %rdi
	pushq	-552(%rbp)
	call	_ZN6icu_6714TimeUnitFormat19searchInLocaleChainE20UTimeUnitFormatStylePKcS3_NS_8TimeUnit15UTimeUnitFieldsERKNS_13UnicodeStringES3_PNS_9HashtableER10UErrorCode
	movl	(%rbx), %r9d
	addq	$32, %rsp
	testl	%r9d, %r9d
	jle	.L477
.L399:
	cmpb	$0, -436(%rbp)
	je	.L367
	movq	-448(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L476:
	movq	-600(%rbp), %rax
	movq	-592(%rbp), %rsi
	movq	(%rax), %rdi
	call	uhash_get_67@PLT
	movq	-536(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L478
.L383:
	movslq	-544(%rbp), %rax
	movq	%rbx, (%r14,%rax,8)
	jmp	.L386
.L425:
	leaq	_ZN6icu_67L24DEFAULT_PATTERN_FOR_WEEKE(%rip), %rax
	jmp	.L405
.L478:
	movl	$16, %edi
	call	uprv_malloc_67@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movups	%xmm0, (%rax)
	movq	%rax, %r14
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-536(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L384
	movq	-592(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-536(%rbp), %r9
.L384:
	movq	-600(%rbp), %rdi
	movq	-568(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r9, -536(%rbp)
	movq	(%rdi), %rdi
	call	uhash_put_67@PLT
	movq	-568(%rbp), %rax
	movq	-536(%rbp), %r9
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jle	.L385
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	movq	-536(%rbp), %r9
	jmp	.L382
.L406:
	movq	-568(%rbp), %rax
	xorl	%r12d, %r12d
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L367
	.p2align 4,,10
	.p2align 3
.L418:
	movq	-600(%rbp), %rax
	movq	-592(%rbp), %rsi
	movq	(%rax), %rdi
	call	uhash_get_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L479
.L410:
	movq	-568(%rbp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L409
	movslq	-544(%rbp), %rax
	movq	%r12, (%rbx,%rax,8)
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L423:
	leaq	_ZN6icu_67L26DEFAULT_PATTERN_FOR_MINUTEE(%rip), %rax
	jmp	.L405
.L380:
	movq	-568(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L381
	movl	$7, (%rax)
	jmp	.L381
.L422:
	leaq	_ZN6icu_67L26DEFAULT_PATTERN_FOR_SECONDE(%rip), %rax
	jmp	.L405
.L424:
	leaq	_ZN6icu_67L24DEFAULT_PATTERN_FOR_HOURE(%rip), %rax
	jmp	.L405
.L426:
	leaq	_ZN6icu_67L23DEFAULT_PATTERN_FOR_DAYE(%rip), %rax
	jmp	.L405
.L479:
	movl	$16, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L480
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movups	%xmm0, (%rax)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L412
	movq	-592(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L412:
	movq	-600(%rbp), %rax
	movq	-568(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	(%rax), %rdi
	call	uhash_put_67@PLT
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	jmp	.L410
.L427:
	leaq	_ZN6icu_67L25DEFAULT_PATTERN_FOR_MONTHE(%rip), %rax
	jmp	.L405
.L385:
	xorl	%edi, %edi
	movq	%r9, -536(%rbp)
	call	uprv_free_67@PLT
	movq	-536(%rbp), %r9
	jmp	.L383
.L474:
	call	__stack_chk_fail@PLT
.L407:
	movq	-568(%rbp), %rax
	cmpl	$0, (%rax)
	jg	.L408
	movl	$7, (%rax)
	jmp	.L408
.L480:
	movq	-568(%rbp), %rax
	xorl	%edi, %edi
	movl	$7, (%rax)
	call	uprv_free_67@PLT
	jmp	.L409
	.cfi_endproc
.LFE2788:
	.size	_ZN6icu_6714TimeUnitFormat19searchInLocaleChainE20UTimeUnitFormatStylePKcS3_NS_8TimeUnit15UTimeUnitFieldsERKNS_13UnicodeStringES3_PNS_9HashtableER10UErrorCode, .-_ZN6icu_6714TimeUnitFormat19searchInLocaleChainE20UTimeUnitFormatStylePKcS3_NS_8TimeUnit15UTimeUnitFieldsERKNS_13UnicodeStringES3_PNS_9HashtableER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6714TimeUnitFormat16checkConsistencyE20UTimeUnitFormatStylePKcR10UErrorCode.part.0, @function
_ZN6icu_6714TimeUnitFormat16checkConsistencyE20UTimeUnitFormatStylePKcR10UErrorCode.part.0:
.LFB3826:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movl	%esi, -148(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6713MeasureFormat14getPluralRulesEv@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_6711PluralRules11getKeywordsER10UErrorCode@PLT
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L503
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L485
	movslq	-148(%rbp), %rax
	salq	$3, %rax
	movq	%rax, -136(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -168(%rbp)
.L484:
	movq	-176(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L485
	xorl	%ebx, %ebx
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L486:
	movq	(%r15), %rdi
	movq	%r13, %rsi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L488
	movq	-136(%rbp), %rdx
	cmpq	$0, (%rax,%rdx)
	je	.L488
.L489:
	addq	$1, %rbx
	cmpq	$7, %rbx
	je	.L504
.L492:
	movq	368(%r14,%rbx,8), %r15
	testq	%r15, %r15
	jne	.L486
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L485
	movq	%r12, %rdi
	call	_ZN6icu_6714TimeUnitFormat8initHashER10UErrorCode.part.0
	movq	%rax, %r15
	movl	(%r12), %eax
	movq	%r15, 368(%r14,%rbx,8)
	testl	%eax, %eax
	jle	.L486
	.p2align 4,,10
	.p2align 3
.L485:
	movq	-176(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
.L481:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L505
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713MeasureFormat11getLocaleIDER10UErrorCode@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	-168(%rbp), %rdi
	movq	%rax, -144(%rbp)
	leaq	-115(%rbp), %rax
	movw	%dx, -116(%rbp)
	movq	%r12, %rdx
	movq	%rax, -128(%rbp)
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	subq	$8, %rsp
	movq	%r13, %r9
	movl	%ebx, %r8d
	pushq	%r12
	movq	-144(%rbp), %rcx
	movq	%r14, %rdi
	movq	-160(%rbp), %rdx
	movl	-148(%rbp), %esi
	pushq	%r15
	pushq	-128(%rbp)
	call	_ZN6icu_6714TimeUnitFormat19searchInLocaleChainE20UTimeUnitFormatStylePKcS3_NS_8TimeUnit15UTimeUnitFieldsERKNS_13UnicodeStringES3_PNS_9HashtableER10UErrorCode
	addq	$32, %rsp
	cmpb	$0, -116(%rbp)
	je	.L489
	movq	-128(%rbp), %rdi
	addq	$1, %rbx
	call	uprv_free_67@PLT
	cmpq	$7, %rbx
	jne	.L492
	.p2align 4,,10
	.p2align 3
.L504:
	movl	(%r12), %esi
	testl	%esi, %esi
	jle	.L484
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L503:
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L481
	movl	$7, (%r12)
	jmp	.L481
.L505:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3826:
	.size	_ZN6icu_6714TimeUnitFormat16checkConsistencyE20UTimeUnitFormatStylePKcR10UErrorCode.part.0, .-_ZN6icu_6714TimeUnitFormat16checkConsistencyE20UTimeUnitFormatStylePKcR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormat16checkConsistencyE20UTimeUnitFormatStylePKcR10UErrorCode
	.type	_ZN6icu_6714TimeUnitFormat16checkConsistencyE20UTimeUnitFormatStylePKcR10UErrorCode, @function
_ZN6icu_6714TimeUnitFormat16checkConsistencyE20UTimeUnitFormatStylePKcR10UErrorCode:
.LFB2787:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L508
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	jmp	_ZN6icu_6714TimeUnitFormat16checkConsistencyE20UTimeUnitFormatStylePKcR10UErrorCode.part.0
	.cfi_endproc
.LFE2787:
	.size	_ZN6icu_6714TimeUnitFormat16checkConsistencyE20UTimeUnitFormatStylePKcR10UErrorCode, .-_ZN6icu_6714TimeUnitFormat16checkConsistencyE20UTimeUnitFormatStylePKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormat5setupER10UErrorCode
	.type	_ZN6icu_6714TimeUnitFormat5setupER10UErrorCode, @function
_ZN6icu_6714TimeUnitFormat5setupER10UErrorCode:
.LFB2776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	424(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	368(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$48, %rsp
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L513
	.p2align 4,,10
	.p2align 3
.L514:
	movq	0(%r13), %rsi
	movq	%r14, %rdi
	addq	$8, %r13
	call	_ZN6icu_6714TimeUnitFormat10deleteHashEPNS_9HashtableE
	movq	$0, -8(%r13)
	cmpq	%r13, %r15
	jne	.L514
.L513:
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	leaq	-80(%rbp), %r15
	movq	%r12, %r8
	xorl	%esi, %esi
	movl	$6, %ecx
	movq	%r15, %rdi
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_6713MeasureFormat14getPluralRulesEv@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_6711PluralRules11getKeywordsER10UErrorCode@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L527
	movl	(%r12), %edx
	movq	(%rax), %rax
	testl	%edx, %edx
	jle	.L518
	movq	%r13, %rdi
	call	*8(%rax)
.L515:
	movq	%r15, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L528:
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	0(%r13), %rax
.L518:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*56(%rax)
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L528
	xorl	%esi, %esi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r14, %rdi
	leaq	_ZN6icu_67L9gUnitsTagE(%rip), %rdx
	call	_ZN6icu_6714TimeUnitFormat21readFromCurrentLocaleE20UTimeUnitFormatStylePKcRKNS_7UVectorER10UErrorCode
	movl	(%r12), %esi
	testl	%esi, %esi
	jle	.L529
.L519:
	movq	%r15, %rcx
	movq	%r12, %r8
	leaq	_ZN6icu_67L14gShortUnitsTagE(%rip), %rdx
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN6icu_6714TimeUnitFormat21readFromCurrentLocaleE20UTimeUnitFormatStylePKcRKNS_7UVectorER10UErrorCode
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L530
.L520:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	movq	%r15, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
.L509:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L531
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L530:
	.cfi_restore_state
	movq	%r12, %rcx
	leaq	_ZN6icu_67L14gShortUnitsTagE(%rip), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6714TimeUnitFormat16checkConsistencyE20UTimeUnitFormatStylePKcR10UErrorCode.part.0
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L529:
	movq	%r12, %rcx
	leaq	_ZN6icu_67L9gUnitsTagE(%rip), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6714TimeUnitFormat16checkConsistencyE20UTimeUnitFormatStylePKcR10UErrorCode.part.0
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L527:
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L515
	movl	$7, (%r12)
	jmp	.L515
.L531:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2776:
	.size	_ZN6icu_6714TimeUnitFormat5setupER10UErrorCode, .-_ZN6icu_6714TimeUnitFormat5setupER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormatC2ERKNS_6LocaleE20UTimeUnitFormatStyleR10UErrorCode
	.type	_ZN6icu_6714TimeUnitFormatC2ERKNS_6LocaleE20UTimeUnitFormatStyleR10UErrorCode, @function
_ZN6icu_6714TimeUnitFormatC2ERKNS_6LocaleE20UTimeUnitFormatStyleR10UErrorCode:
.LFB2763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	_ZN6icu_6713MeasureFormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6714TimeUnitFormatE(%rip), %rax
	movq	%rax, (%r12)
	testl	%ebx, %ebx
	je	.L533
	cmpl	$1, %ebx
	je	.L534
	xorl	%edx, %edx
	movq	%r13, %r8
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode@PLT
	movl	0(%r13), %edx
	pxor	%xmm0, %xmm0
	movq	$0, 416(%r12)
	movups	%xmm0, 368(%r12)
	movups	%xmm0, 384(%r12)
	movups	%xmm0, 400(%r12)
	testl	%edx, %edx
	jg	.L532
	movl	$1, 0(%r13)
.L532:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L533:
	.cfi_restore_state
	movq	%r13, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode@PLT
.L536:
	movl	0(%r13), %eax
	pxor	%xmm0, %xmm0
	movq	$0, 416(%r12)
	movups	%xmm0, 368(%r12)
	movups	%xmm0, 384(%r12)
	movups	%xmm0, 400(%r12)
	testl	%eax, %eax
	jg	.L532
	movl	%ebx, 424(%r12)
	movq	%r13, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714TimeUnitFormat5setupER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	movq	%r13, %r8
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode@PLT
	jmp	.L536
	.cfi_endproc
.LFE2763:
	.size	_ZN6icu_6714TimeUnitFormatC2ERKNS_6LocaleE20UTimeUnitFormatStyleR10UErrorCode, .-_ZN6icu_6714TimeUnitFormatC2ERKNS_6LocaleE20UTimeUnitFormatStyleR10UErrorCode
	.globl	_ZN6icu_6714TimeUnitFormatC1ERKNS_6LocaleE20UTimeUnitFormatStyleR10UErrorCode
	.set	_ZN6icu_6714TimeUnitFormatC1ERKNS_6LocaleE20UTimeUnitFormatStyleR10UErrorCode,_ZN6icu_6714TimeUnitFormatC2ERKNS_6LocaleE20UTimeUnitFormatStyleR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormat9setLocaleERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6714TimeUnitFormat9setLocaleERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6714TimeUnitFormat9setLocaleERKNS_6LocaleER10UErrorCode:
.LFB2789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_6713MeasureFormat22setMeasureFormatLocaleERKNS_6LocaleER10UErrorCode@PLT
	testb	%al, %al
	jne	.L547
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L547:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714TimeUnitFormat5setupER10UErrorCode
	.cfi_endproc
.LFE2789:
	.size	_ZN6icu_6714TimeUnitFormat9setLocaleERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6714TimeUnitFormat9setLocaleERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormatC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6714TimeUnitFormatC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6714TimeUnitFormatC2ERKNS_6LocaleER10UErrorCode:
.LFB2760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713MeasureFormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6714TimeUnitFormatE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, (%r12)
	movq	%r13, %r8
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode@PLT
	movl	0(%r13), %eax
	pxor	%xmm0, %xmm0
	movq	$0, 416(%r12)
	movups	%xmm0, 368(%r12)
	movups	%xmm0, 384(%r12)
	movups	%xmm0, 400(%r12)
	testl	%eax, %eax
	jg	.L548
	movl	$0, 424(%r12)
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714TimeUnitFormat5setupER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L548:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2760:
	.size	_ZN6icu_6714TimeUnitFormatC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6714TimeUnitFormatC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6714TimeUnitFormatC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6714TimeUnitFormatC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6714TimeUnitFormatC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormatC2ER10UErrorCode
	.type	_ZN6icu_6714TimeUnitFormatC2ER10UErrorCode, @function
_ZN6icu_6714TimeUnitFormatC2ER10UErrorCode:
.LFB2757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_6713MeasureFormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6714TimeUnitFormatE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%r13, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713MeasureFormat17initMeasureFormatERKNS_6LocaleE19UMeasureFormatWidthPNS_12NumberFormatER10UErrorCode@PLT
	movl	0(%r13), %eax
	pxor	%xmm0, %xmm0
	movq	$0, 416(%r12)
	movups	%xmm0, 368(%r12)
	movups	%xmm0, 384(%r12)
	movups	%xmm0, 400(%r12)
	testl	%eax, %eax
	jg	.L551
	movl	$0, 424(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714TimeUnitFormat5setupER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L551:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2757:
	.size	_ZN6icu_6714TimeUnitFormatC2ER10UErrorCode, .-_ZN6icu_6714TimeUnitFormatC2ER10UErrorCode
	.globl	_ZN6icu_6714TimeUnitFormatC1ER10UErrorCode
	.set	_ZN6icu_6714TimeUnitFormatC1ER10UErrorCode,_ZN6icu_6714TimeUnitFormatC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitFormat6createE20UTimeUnitFormatStyleR10UErrorCode
	.type	_ZN6icu_6714TimeUnitFormat6createE20UTimeUnitFormatStyleR10UErrorCode, @function
_ZN6icu_6714TimeUnitFormat6createE20UTimeUnitFormatStyleR10UErrorCode:
.LFB2775:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	pxor	%xmm0, %xmm0
	movq	$0, 416(%rdi)
	movups	%xmm0, 368(%rdi)
	movups	%xmm0, 384(%rdi)
	movups	%xmm0, 400(%rdi)
	testl	%ecx, %ecx
	jg	.L554
	cmpl	$1, %esi
	jbe	.L556
	movl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	ret
	.p2align 4,,10
	.p2align 3
.L556:
	movl	%esi, 424(%rdi)
	movq	%rdx, %rsi
	jmp	_ZN6icu_6714TimeUnitFormat5setupER10UErrorCode
	.cfi_endproc
.LFE2775:
	.size	_ZN6icu_6714TimeUnitFormat6createE20UTimeUnitFormatStyleR10UErrorCode, .-_ZN6icu_6714TimeUnitFormat6createE20UTimeUnitFormatStyleR10UErrorCode
	.section	.text._ZN6icu_6722TimeUnitFormatReadSink3putEPKcRNS_13ResourceValueEaR10UErrorCode,"axG",@progbits,_ZN6icu_6722TimeUnitFormatReadSink3putEPKcRNS_13ResourceValueEaR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6722TimeUnitFormatReadSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.type	_ZN6icu_6722TimeUnitFormatReadSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, @function
_ZN6icu_6722TimeUnitFormatReadSink3putEPKcRNS_13ResourceValueEaR10UErrorCode:
.LFB2781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -544(%rbp)
	movq	%rsi, -536(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 28(%rdi)
	jne	.L557
	leaq	-512(%rbp), %rax
	movb	$1, 28(%rdi)
	movq	%rdx, %r12
	movq	%r8, %rbx
	movq	%rax, -624(%rbp)
	movq	%rax, %rdi
	movq	(%rdx), %rax
	movq	%r12, %rsi
	movq	%r8, %rdx
	call	*88(%rax)
	movl	(%rbx), %r14d
	testl	%r14d, %r14d
	jg	.L557
	leaq	-536(%rbp), %rax
	movl	$0, -604(%rbp)
	movq	%rax, -552(%rbp)
	leaq	-464(%rbp), %rax
	movq	%rax, -560(%rbp)
	leaq	-524(%rbp), %rax
	movq	%rax, -568(%rbp)
	.p2align 4,,10
	.p2align 3
.L588:
	movq	-552(%rbp), %rdx
	movl	-604(%rbp), %esi
	movq	%r12, %rcx
	movq	-624(%rbp), %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L557
	movq	-536(%rbp), %r13
	testq	%r13, %r13
	je	.L571
	leaq	_ZN6icu_67L13gTimeUnitYearE(%rip), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L561
	leaq	_ZN6icu_67L14gTimeUnitMonthE(%rip), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L595
	leaq	_ZN6icu_67L12gTimeUnitDayE(%rip), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L596
	leaq	_ZN6icu_67L13gTimeUnitHourE(%rip), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L597
	leaq	_ZN6icu_67L15gTimeUnitMinuteE(%rip), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L598
	leaq	_ZN6icu_67L15gTimeUnitSecondE(%rip), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L599
	leaq	_ZN6icu_67L13gTimeUnitWeekE(%rip), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L571
	movl	$3, %eax
	.p2align 4,,10
	.p2align 3
.L561:
	movq	-544(%rbp), %rcx
	cltq
	movq	%rax, -632(%rbp)
	movq	8(%rcx), %rdx
	movq	368(%rdx,%rax,8), %rax
	movq	%rax, -592(%rbp)
	testq	%rax, %rax
	je	.L641
.L633:
	movq	(%r12), %rax
	movq	-560(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	*88(%rax)
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L571
	movq	$0, -600(%rbp)
.L592:
	xorl	%r13d, %r13d
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L573:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L582:
	addl	$1, %r13d
.L569:
	movq	-552(%rbp), %rdx
	movq	-560(%rbp), %rdi
	movq	%r12, %rcx
	movl	%r13d, %esi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L572
	movq	(%r12), %rax
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	$0, (%rbx)
	movq	-568(%rbp), %rsi
	leaq	-416(%rbp), %r14
	movl	$0, -524(%rbp)
	call	*32(%rax)
	movl	-524(%rbp), %ecx
	leaq	-520(%rbp), %rdx
	movq	%r14, %rdi
	movl	$1, %esi
	movq	%rax, -520(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jg	.L573
	movq	-536(%rbp), %rsi
	leaq	-352(%rbp), %r15
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movq	-544(%rbp), %rax
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	16(%rax), %rdi
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	testl	%eax, %eax
	js	.L642
	movq	-544(%rbp), %rax
	leaq	-288(%rbp), %r8
	movq	%rbx, %rdx
	movq	%r8, %rdi
	movq	%r8, -576(%rbp)
	movq	8(%rax), %rsi
	call	_ZNK6icu_6713MeasureFormat9getLocaleER10UErrorCode@PLT
	movl	$816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-576(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, -584(%rbp)
	movq	%rax, %rdi
	je	.L575
	movq	%r8, %rdx
	movq	%rbx, %rcx
	movq	%r14, %rsi
	call	_ZN6icu_6713MessageFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode@PLT
	movq	-576(%rbp), %r8
.L576:
	movq	%r8, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L577
.L646:
	movq	-592(%rbp), %rax
	movq	%r15, %rsi
	movq	(%rax), %rdi
	call	uhash_get_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L643
.L578:
	movq	-544(%rbp), %rax
	movq	-584(%rbp), %rcx
	movq	%r15, %rdi
	movslq	24(%rax), %rax
	movq	%rcx, (%r8,%rax,8)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L582
.L649:
	movl	$7, (%rbx)
	.p2align 4,,10
	.p2align 3
.L557:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L644
	addq	$600, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	movq	-544(%rbp), %rax
	movq	-632(%rbp), %rcx
	movq	8(%rax), %rax
	leaq	(%rax,%rcx,8), %rax
	cmpq	$0, 368(%rax)
	je	.L645
	movq	-600(%rbp), %rax
	testq	%rax, %rax
	je	.L571
.L640:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L587
	call	uhash_close_67@PLT
.L587:
	movq	-600(%rbp), %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L571:
	addl	$1, -604(%rbp)
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L642:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L573
.L575:
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L576
	movl	$7, (%rbx)
	movq	%r8, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L646
	.p2align 4,,10
	.p2align 3
.L577:
	movq	-584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L583
	movq	(%rdi), %rax
	call	*8(%rax)
.L583:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpq	$0, -600(%rbp)
	je	.L557
	movq	-600(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L589
	call	uhash_close_67@PLT
.L589:
	movq	-600(%rbp), %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L643:
	movl	$16, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L647
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	%rax, -576(%rbp)
	movups	%xmm0, (%rax)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-576(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L581
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%r8, -616(%rbp)
	movq	%rax, -576(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-616(%rbp), %r8
	movq	-576(%rbp), %r9
.L581:
	movq	-592(%rbp), %rax
	movq	%r8, %rdx
	movq	%rbx, %rcx
	movq	%r9, %rsi
	movq	%r8, -576(%rbp)
	movq	(%rax), %rdi
	call	uhash_put_67@PLT
	movl	(%rbx), %edi
	movq	-576(%rbp), %r8
	testl	%edi, %edi
	jg	.L580
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	movq	-576(%rbp), %r8
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L641:
	movl	(%rbx), %r13d
	testl	%r13d, %r13d
	jg	.L557
	movq	%rbx, %rdi
	call	_ZN6icu_6714TimeUnitFormat8initHashER10UErrorCode.part.0
	movl	(%rbx), %r11d
	movq	%rax, -600(%rbp)
	testl	%r11d, %r11d
	jle	.L648
	testq	%rax, %rax
	je	.L557
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L567
	call	uhash_close_67@PLT
.L567:
	movq	-600(%rbp), %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jle	.L633
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L595:
	movl	$1, %eax
	jmp	.L561
.L596:
	movl	$2, %eax
	jmp	.L561
.L645:
	movq	-600(%rbp), %rcx
	addl	$1, -604(%rbp)
	movq	%rcx, 368(%rax)
	jmp	.L588
.L597:
	movl	$4, %eax
	jmp	.L561
.L648:
	testq	%rax, %rax
	je	.L649
	movq	(%r12), %rax
	movq	%rbx, %rdx
	movq	-560(%rbp), %rdi
	movq	%r12, %rsi
	call	*88(%rax)
	movl	(%rbx), %edx
	movq	-600(%rbp), %rax
	testl	%edx, %edx
	jg	.L640
	movq	%rax, -592(%rbp)
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L647:
	movl	$7, (%rbx)
.L580:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L577
.L598:
	movl	$5, %eax
	jmp	.L561
.L599:
	movl	$6, %eax
	jmp	.L561
.L644:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2781:
	.size	_ZN6icu_6722TimeUnitFormatReadSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, .-_ZN6icu_6722TimeUnitFormatReadSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.weak	_ZTSN6icu_6714TimeUnitFormatE
	.section	.rodata._ZTSN6icu_6714TimeUnitFormatE,"aG",@progbits,_ZTSN6icu_6714TimeUnitFormatE,comdat
	.align 16
	.type	_ZTSN6icu_6714TimeUnitFormatE, @object
	.size	_ZTSN6icu_6714TimeUnitFormatE, 26
_ZTSN6icu_6714TimeUnitFormatE:
	.string	"N6icu_6714TimeUnitFormatE"
	.weak	_ZTIN6icu_6714TimeUnitFormatE
	.section	.data.rel.ro._ZTIN6icu_6714TimeUnitFormatE,"awG",@progbits,_ZTIN6icu_6714TimeUnitFormatE,comdat
	.align 8
	.type	_ZTIN6icu_6714TimeUnitFormatE, @object
	.size	_ZTIN6icu_6714TimeUnitFormatE, 24
_ZTIN6icu_6714TimeUnitFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714TimeUnitFormatE
	.quad	_ZTIN6icu_6713MeasureFormatE
	.weak	_ZTSN6icu_6722TimeUnitFormatReadSinkE
	.section	.rodata._ZTSN6icu_6722TimeUnitFormatReadSinkE,"aG",@progbits,_ZTSN6icu_6722TimeUnitFormatReadSinkE,comdat
	.align 32
	.type	_ZTSN6icu_6722TimeUnitFormatReadSinkE, @object
	.size	_ZTSN6icu_6722TimeUnitFormatReadSinkE, 34
_ZTSN6icu_6722TimeUnitFormatReadSinkE:
	.string	"N6icu_6722TimeUnitFormatReadSinkE"
	.weak	_ZTIN6icu_6722TimeUnitFormatReadSinkE
	.section	.data.rel.ro._ZTIN6icu_6722TimeUnitFormatReadSinkE,"awG",@progbits,_ZTIN6icu_6722TimeUnitFormatReadSinkE,comdat
	.align 8
	.type	_ZTIN6icu_6722TimeUnitFormatReadSinkE, @object
	.size	_ZTIN6icu_6722TimeUnitFormatReadSinkE, 24
_ZTIN6icu_6722TimeUnitFormatReadSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722TimeUnitFormatReadSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.weak	_ZTVN6icu_6714TimeUnitFormatE
	.section	.data.rel.ro._ZTVN6icu_6714TimeUnitFormatE,"awG",@progbits,_ZTVN6icu_6714TimeUnitFormatE,comdat
	.align 8
	.type	_ZTVN6icu_6714TimeUnitFormatE, @object
	.size	_ZTVN6icu_6714TimeUnitFormatE, 80
_ZTVN6icu_6714TimeUnitFormatE:
	.quad	0
	.quad	_ZTIN6icu_6714TimeUnitFormatE
	.quad	_ZN6icu_6714TimeUnitFormatD1Ev
	.quad	_ZN6icu_6714TimeUnitFormatD0Ev
	.quad	_ZNK6icu_6714TimeUnitFormat17getDynamicClassIDEv
	.quad	_ZNK6icu_6713MeasureFormateqERKNS_6FormatE
	.quad	_ZNK6icu_6714TimeUnitFormat5cloneEv
	.quad	_ZNK6icu_6713MeasureFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6714TimeUnitFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.weak	_ZTVN6icu_6722TimeUnitFormatReadSinkE
	.section	.data.rel.ro._ZTVN6icu_6722TimeUnitFormatReadSinkE,"awG",@progbits,_ZTVN6icu_6722TimeUnitFormatReadSinkE,comdat
	.align 8
	.type	_ZTVN6icu_6722TimeUnitFormatReadSinkE, @object
	.size	_ZTVN6icu_6722TimeUnitFormatReadSinkE, 48
_ZTVN6icu_6722TimeUnitFormatReadSinkE:
	.quad	0
	.quad	_ZTIN6icu_6722TimeUnitFormatReadSinkE
	.quad	_ZN6icu_6722TimeUnitFormatReadSinkD1Ev
	.quad	_ZN6icu_6722TimeUnitFormatReadSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6722TimeUnitFormatReadSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.section	.rodata
	.align 8
	.type	_ZN6icu_67L16PLURAL_COUNT_TWOE, @object
	.size	_ZN6icu_67L16PLURAL_COUNT_TWOE, 8
_ZN6icu_67L16PLURAL_COUNT_TWOE:
	.value	116
	.value	119
	.value	111
	.value	0
	.align 8
	.type	_ZN6icu_67L16PLURAL_COUNT_ONEE, @object
	.size	_ZN6icu_67L16PLURAL_COUNT_ONEE, 8
_ZN6icu_67L16PLURAL_COUNT_ONEE:
	.value	111
	.value	110
	.value	101
	.value	0
	.align 8
	.type	_ZN6icu_67L17PLURAL_COUNT_ZEROE, @object
	.size	_ZN6icu_67L17PLURAL_COUNT_ZEROE, 10
_ZN6icu_67L17PLURAL_COUNT_ZEROE:
	.value	122
	.value	101
	.value	114
	.value	111
	.value	0
	.align 8
	.type	_ZN6icu_67L24DEFAULT_PATTERN_FOR_YEARE, @object
	.size	_ZN6icu_67L24DEFAULT_PATTERN_FOR_YEARE, 12
_ZN6icu_67L24DEFAULT_PATTERN_FOR_YEARE:
	.value	123
	.value	48
	.value	125
	.value	32
	.value	121
	.value	0
	.align 8
	.type	_ZN6icu_67L25DEFAULT_PATTERN_FOR_MONTHE, @object
	.size	_ZN6icu_67L25DEFAULT_PATTERN_FOR_MONTHE, 12
_ZN6icu_67L25DEFAULT_PATTERN_FOR_MONTHE:
	.value	123
	.value	48
	.value	125
	.value	32
	.value	109
	.value	0
	.align 8
	.type	_ZN6icu_67L23DEFAULT_PATTERN_FOR_DAYE, @object
	.size	_ZN6icu_67L23DEFAULT_PATTERN_FOR_DAYE, 12
_ZN6icu_67L23DEFAULT_PATTERN_FOR_DAYE:
	.value	123
	.value	48
	.value	125
	.value	32
	.value	100
	.value	0
	.align 8
	.type	_ZN6icu_67L24DEFAULT_PATTERN_FOR_WEEKE, @object
	.size	_ZN6icu_67L24DEFAULT_PATTERN_FOR_WEEKE, 12
_ZN6icu_67L24DEFAULT_PATTERN_FOR_WEEKE:
	.value	123
	.value	48
	.value	125
	.value	32
	.value	119
	.value	0
	.align 8
	.type	_ZN6icu_67L24DEFAULT_PATTERN_FOR_HOURE, @object
	.size	_ZN6icu_67L24DEFAULT_PATTERN_FOR_HOURE, 12
_ZN6icu_67L24DEFAULT_PATTERN_FOR_HOURE:
	.value	123
	.value	48
	.value	125
	.value	32
	.value	104
	.value	0
	.align 16
	.type	_ZN6icu_67L26DEFAULT_PATTERN_FOR_MINUTEE, @object
	.size	_ZN6icu_67L26DEFAULT_PATTERN_FOR_MINUTEE, 16
_ZN6icu_67L26DEFAULT_PATTERN_FOR_MINUTEE:
	.value	123
	.value	48
	.value	125
	.value	32
	.value	109
	.value	105
	.value	110
	.value	0
	.align 8
	.type	_ZN6icu_67L26DEFAULT_PATTERN_FOR_SECONDE, @object
	.size	_ZN6icu_67L26DEFAULT_PATTERN_FOR_SECONDE, 12
_ZN6icu_67L26DEFAULT_PATTERN_FOR_SECONDE:
	.value	123
	.value	48
	.value	125
	.value	32
	.value	115
	.value	0
	.type	_ZN6icu_67L17gPluralCountOtherE, @object
	.size	_ZN6icu_67L17gPluralCountOtherE, 6
_ZN6icu_67L17gPluralCountOtherE:
	.string	"other"
	.type	_ZN6icu_67L15gTimeUnitSecondE, @object
	.size	_ZN6icu_67L15gTimeUnitSecondE, 7
_ZN6icu_67L15gTimeUnitSecondE:
	.string	"second"
	.type	_ZN6icu_67L15gTimeUnitMinuteE, @object
	.size	_ZN6icu_67L15gTimeUnitMinuteE, 7
_ZN6icu_67L15gTimeUnitMinuteE:
	.string	"minute"
	.type	_ZN6icu_67L13gTimeUnitHourE, @object
	.size	_ZN6icu_67L13gTimeUnitHourE, 5
_ZN6icu_67L13gTimeUnitHourE:
	.string	"hour"
	.type	_ZN6icu_67L13gTimeUnitWeekE, @object
	.size	_ZN6icu_67L13gTimeUnitWeekE, 5
_ZN6icu_67L13gTimeUnitWeekE:
	.string	"week"
	.type	_ZN6icu_67L12gTimeUnitDayE, @object
	.size	_ZN6icu_67L12gTimeUnitDayE, 4
_ZN6icu_67L12gTimeUnitDayE:
	.string	"day"
	.type	_ZN6icu_67L14gTimeUnitMonthE, @object
	.size	_ZN6icu_67L14gTimeUnitMonthE, 6
_ZN6icu_67L14gTimeUnitMonthE:
	.string	"month"
	.type	_ZN6icu_67L13gTimeUnitYearE, @object
	.size	_ZN6icu_67L13gTimeUnitYearE, 5
_ZN6icu_67L13gTimeUnitYearE:
	.string	"year"
	.align 8
	.type	_ZN6icu_67L14gShortUnitsTagE, @object
	.size	_ZN6icu_67L14gShortUnitsTagE, 11
_ZN6icu_67L14gShortUnitsTagE:
	.string	"unitsShort"
	.type	_ZN6icu_67L9gUnitsTagE, @object
	.size	_ZN6icu_67L9gUnitsTagE, 6
_ZN6icu_67L9gUnitsTagE:
	.string	"units"
	.local	_ZZN6icu_6714TimeUnitFormat16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6714TimeUnitFormat16getStaticClassIDEvE7classID,1,1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	0
	.long	1072693248
	.align 8
.LC5:
	.long	0
	.long	1073741824
	.align 8
.LC6:
	.long	0
	.long	1074266112
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
