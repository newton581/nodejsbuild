	.file	"plurrule.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PluralRules17getDynamicClassIDEv
	.type	_ZNK6icu_6711PluralRules17getDynamicClassIDEv, @function
_ZNK6icu_6711PluralRules17getDynamicClassIDEv:
.LFB3786:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6711PluralRules16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3786:
	.size	_ZNK6icu_6711PluralRules17getDynamicClassIDEv, .-_ZNK6icu_6711PluralRules17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724PluralKeywordEnumeration17getDynamicClassIDEv
	.type	_ZNK6icu_6724PluralKeywordEnumeration17getDynamicClassIDEv, @function
_ZNK6icu_6724PluralKeywordEnumeration17getDynamicClassIDEv:
.LFB3788:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6724PluralKeywordEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3788:
	.size	_ZNK6icu_6724PluralKeywordEnumeration17getDynamicClassIDEv, .-_ZNK6icu_6724PluralKeywordEnumeration17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724PluralKeywordEnumeration5resetER10UErrorCode
	.type	_ZN6icu_6724PluralKeywordEnumeration5resetER10UErrorCode, @function
_ZN6icu_6724PluralKeywordEnumeration5resetER10UErrorCode:
.LFB3887:
	.cfi_startproc
	endbr64
	movl	$0, 116(%rdi)
	ret
	.cfi_endproc
.LFE3887:
	.size	_ZN6icu_6724PluralKeywordEnumeration5resetER10UErrorCode, .-_ZN6icu_6724PluralKeywordEnumeration5resetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724PluralKeywordEnumeration5countER10UErrorCode
	.type	_ZNK6icu_6724PluralKeywordEnumeration5countER10UErrorCode, @function
_ZNK6icu_6724PluralKeywordEnumeration5countER10UErrorCode:
.LFB3888:
	.cfi_startproc
	endbr64
	movl	128(%rdi), %eax
	ret
	.cfi_endproc
.LFE3888:
	.size	_ZNK6icu_6724PluralKeywordEnumeration5countER10UErrorCode, .-_ZNK6icu_6724PluralKeywordEnumeration5countER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712FixedDecimal5isNaNEv
	.type	_ZNK6icu_6712FixedDecimal5isNaNEv, @function
_ZNK6icu_6712FixedDecimal5isNaNEv:
.LFB3926:
	.cfi_startproc
	endbr64
	cmpb	$0, 58(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE3926:
	.size	_ZNK6icu_6712FixedDecimal5isNaNEv, .-_ZNK6icu_6712FixedDecimal5isNaNEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712FixedDecimal10isInfiniteEv
	.type	_ZNK6icu_6712FixedDecimal10isInfiniteEv, @function
_ZNK6icu_6712FixedDecimal10isInfiniteEv:
.LFB3927:
	.cfi_startproc
	endbr64
	cmpb	$0, 59(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE3927:
	.size	_ZNK6icu_6712FixedDecimal10isInfiniteEv, .-_ZNK6icu_6712FixedDecimal10isInfiniteEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712FixedDecimal15hasIntegerValueEv
	.type	_ZNK6icu_6712FixedDecimal15hasIntegerValueEv, @function
_ZNK6icu_6712FixedDecimal15hasIntegerValueEv:
.LFB3928:
	.cfi_startproc
	endbr64
	cmpb	$0, 56(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE3928:
	.size	_ZNK6icu_6712FixedDecimal15hasIntegerValueEv, .-_ZNK6icu_6712FixedDecimal15hasIntegerValueEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716PluralRuleParserD2Ev
	.type	_ZN6icu_6716PluralRuleParserD2Ev, @function
_ZN6icu_6716PluralRuleParserD2Ev:
.LFB3875:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6716PluralRuleParserE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE3875:
	.size	_ZN6icu_6716PluralRuleParserD2Ev, .-_ZN6icu_6716PluralRuleParserD2Ev
	.globl	_ZN6icu_6716PluralRuleParserD1Ev
	.set	_ZN6icu_6716PluralRuleParserD1Ev,_ZN6icu_6716PluralRuleParserD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716PluralRuleParserD0Ev
	.type	_ZN6icu_6716PluralRuleParserD0Ev, @function
_ZN6icu_6716PluralRuleParserD0Ev:
.LFB3877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6716PluralRuleParserE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	24(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -24(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3877:
	.size	_ZN6icu_6716PluralRuleParserD0Ev, .-_ZN6icu_6716PluralRuleParserD0Ev
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE5cloneEv,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE5cloneEv
	.type	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE5cloneEv, @function
_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE5cloneEv:
.LFB5337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$240, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L12
	movl	8(%rbx), %eax
	movb	$0, 12(%r12)
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	movl	%eax, 8(%r12)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
.L12:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5337:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE5cloneEv, .-_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE5cloneEv
	.section	.text._ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci,"axG",@progbits,_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci
	.type	_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci, @function
_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci:
.LFB5342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	leaq	_ZTSN6icu_6717SharedPluralRulesE(%rip), %rsi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edx, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	call	strncpy@PLT
	movb	$0, -1(%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5342:
	.size	_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci, .-_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci
	.type	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci, @function
_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci:
.LFB5339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edx, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	movq	56(%rdi), %rsi
	movq	%r8, %rdi
	call	strncpy@PLT
	movb	$0, -1(%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5339:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci, .-_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci
	.section	.text._ZNK6icu_678CacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE:
.LFB5341:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L27
	xorl	%eax, %eax
	cmpb	$42, (%rdi)
	je	.L27
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	strcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE5341:
	.size	_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE
	.section	.text._ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE8hashCodeEv,"axG",@progbits,_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE8hashCodeEv
	.type	_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE8hashCodeEv, @function
_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE8hashCodeEv:
.LFB5340:
	.cfi_startproc
	endbr64
	movl	$28, %esi
	leaq	_ZTSN6icu_6717SharedPluralRulesE(%rip), %rdi
	jmp	ustr_hashCharsN_67@PLT
	.cfi_endproc
.LFE5340:
	.size	_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE8hashCodeEv, .-_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE8hashCodeEv
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE8hashCodeEv,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE8hashCodeEv
	.type	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE8hashCodeEv, @function
_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE8hashCodeEv:
.LFB5336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$28, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	_ZTSN6icu_6717SharedPluralRulesE(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	call	ustr_hashCharsN_67@PLT
	leaq	16(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_676Locale8hashCodeEv@PLT
	movl	%eax, %r8d
	leal	(%rbx,%rbx,8), %eax
	leal	(%rbx,%rax,4), %eax
	popq	%rbx
	popq	%r12
	addl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5336:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE8hashCodeEv, .-_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE8hashCodeEv
	.text
	.p2align 4
	.type	_ZN6icu_67L11tokenStringENS_9tokenTypeE, @function
_ZN6icu_67L11tokenStringENS_9tokenTypeE:
.LFB3867:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r9d
	subl	$21, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r9w, 8(%rdi)
	movq	%rax, (%rdi)
	cmpl	$4, %esi
	ja	.L34
	leaq	.L36(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L36:
	.long	.L40-.L36
	.long	.L39-.L36
	.long	.L38-.L36
	.long	.L37-.L36
	.long	.L35-.L36
	.text
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$118, %ecx
	movw	%cx, -26(%rbp)
	.p2align 4,,10
	.p2align 3
.L44:
	xorl	%edx, %edx
	leaq	-26(%rbp), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movl	$116, %edx
	movw	%dx, -26(%rbp)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$110, %r8d
	movw	%r8w, -26(%rbp)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$105, %edi
	movw	%di, -26(%rbp)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$102, %esi
	movw	%si, -26(%rbp)
	jmp	.L44
.L34:
	movl	$126, %eax
	movw	%ax, -26(%rbp)
	jmp	.L44
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3867:
	.size	_ZN6icu_67L11tokenStringENS_9tokenTypeE, .-_ZN6icu_67L11tokenStringENS_9tokenTypeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6733PluralAvailableLocalesEnumerationD2Ev
	.type	_ZN6icu_6733PluralAvailableLocalesEnumerationD2Ev, @function
_ZN6icu_6733PluralAvailableLocalesEnumerationD2Ev:
.LFB3935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6733PluralAvailableLocalesEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	call	ures_close_67@PLT
	movq	128(%r12), %rdi
	call	ures_close_67@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, 120(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717StringEnumerationD2Ev@PLT
	.cfi_endproc
.LFE3935:
	.size	_ZN6icu_6733PluralAvailableLocalesEnumerationD2Ev, .-_ZN6icu_6733PluralAvailableLocalesEnumerationD2Ev
	.globl	_ZN6icu_6733PluralAvailableLocalesEnumerationD1Ev
	.set	_ZN6icu_6733PluralAvailableLocalesEnumerationD1Ev,_ZN6icu_6733PluralAvailableLocalesEnumerationD2Ev
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB1:
	.text
.LHOTB1:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712FixedDecimal16getPluralOperandENS_13PluralOperandE
	.type	_ZNK6icu_6712FixedDecimal16getPluralOperandENS_13PluralOperandE, @function
_ZNK6icu_6712FixedDecimal16getPluralOperandENS_13PluralOperandE:
.LFB3925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$6, %esi
	ja	.L49
	leaq	.L51(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L51:
	.long	.L56-.L51
	.long	.L55-.L51
	.long	.L54-.L51
	.long	.L53-.L51
	.long	.L52-.L51
	.long	.L49-.L51
	.long	.L57-.L51
	.text
	.p2align 4,,10
	.p2align 3
.L52:
	pxor	%xmm0, %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cvtsi2sdl	24(%rdi), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movsd	16(%rdi), %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cvtsi2sdq	48(%rdi), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cvtsi2sdq	32(%rdi), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cvtsi2sdq	40(%rdi), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_6712FixedDecimal16getPluralOperandENS_13PluralOperandE.cold, @function
_ZNK6icu_6712FixedDecimal16getPluralOperandENS_13PluralOperandE.cold:
.LFSB3925:
.L49:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3925:
	.text
	.size	_ZNK6icu_6712FixedDecimal16getPluralOperandENS_13PluralOperandE, .-_ZNK6icu_6712FixedDecimal16getPluralOperandENS_13PluralOperandE
	.section	.text.unlikely
	.size	_ZNK6icu_6712FixedDecimal16getPluralOperandENS_13PluralOperandE.cold, .-_ZNK6icu_6712FixedDecimal16getPluralOperandENS_13PluralOperandE.cold
.LCOLDE1:
	.text
.LHOTE1:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FixedDecimalD2Ev
	.type	_ZN6icu_6712FixedDecimalD2Ev, @function
_ZN6icu_6712FixedDecimalD2Ev:
.LFB3916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713IFixedDecimalD2Ev@PLT
	.cfi_endproc
.LFE3916:
	.size	_ZN6icu_6712FixedDecimalD2Ev, .-_ZN6icu_6712FixedDecimalD2Ev
	.globl	_ZN6icu_6712FixedDecimalD1Ev
	.set	_ZN6icu_6712FixedDecimalD1Ev,_ZN6icu_6712FixedDecimalD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FixedDecimalD0Ev
	.type	_ZN6icu_6712FixedDecimalD0Ev, @function
_ZN6icu_6712FixedDecimalD0Ev:
.LFB3918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713IFixedDecimalD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3918:
	.size	_ZN6icu_6712FixedDecimalD0Ev, .-_ZN6icu_6712FixedDecimalD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6733PluralAvailableLocalesEnumeration5countER10UErrorCode
	.type	_ZNK6icu_6733PluralAvailableLocalesEnumeration5countER10UErrorCode, @function
_ZNK6icu_6733PluralAvailableLocalesEnumeration5countER10UErrorCode:
.LFB3940:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L63
	movl	116(%rdi), %eax
	testl	%eax, %eax
	jle	.L65
	movl	%eax, (%rsi)
.L63:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	movq	120(%rdi), %rdi
	jmp	ures_getSize_67@PLT
	.cfi_endproc
.LFE3940:
	.size	_ZNK6icu_6733PluralAvailableLocalesEnumeration5countER10UErrorCode, .-_ZNK6icu_6733PluralAvailableLocalesEnumeration5countER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6733PluralAvailableLocalesEnumeration4nextEPiR10UErrorCode
	.type	_ZN6icu_6733PluralAvailableLocalesEnumeration4nextEPiR10UErrorCode, @function
_ZN6icu_6733PluralAvailableLocalesEnumeration4nextEPiR10UErrorCode:
.LFB3938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L79
	movl	116(%rdi), %eax
	movq	%rdi, %r12
	movq	%rdx, %rbx
	testl	%eax, %eax
	jg	.L80
	movq	%rsi, %r13
	movq	128(%rdi), %rsi
	movq	120(%rdi), %rdi
	call	ures_getNextResource_67@PLT
	movq	%rax, %rdi
	movq	%rax, 128(%r12)
	movl	(%rbx), %eax
	testq	%rdi, %rdi
	je	.L72
	testl	%eax, %eax
	jg	.L72
	call	ures_getKey_67@PLT
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L67
	movq	%rax, %rdi
	call	strlen@PLT
	movl	%eax, 0(%r13)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movl	%eax, (%rdx)
.L79:
	xorl	%r12d, %r12d
.L67:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	cmpl	$8, %eax
	jne	.L79
	movl	$0, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L67
	.cfi_endproc
.LFE3938:
	.size	_ZN6icu_6733PluralAvailableLocalesEnumeration4nextEPiR10UErrorCode, .-_ZN6icu_6733PluralAvailableLocalesEnumeration4nextEPiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6733PluralAvailableLocalesEnumeration5resetER10UErrorCode
	.type	_ZN6icu_6733PluralAvailableLocalesEnumeration5resetER10UErrorCode, @function
_ZN6icu_6733PluralAvailableLocalesEnumeration5resetER10UErrorCode:
.LFB3939:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L81
	movl	116(%rdi), %eax
	testl	%eax, %eax
	jle	.L83
	movl	%eax, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	movq	120(%rdi), %rdi
	jmp	ures_resetIterator_67@PLT
	.cfi_endproc
.LFE3939:
	.size	_ZN6icu_6733PluralAvailableLocalesEnumeration5resetER10UErrorCode, .-_ZN6icu_6733PluralAvailableLocalesEnumeration5resetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724PluralKeywordEnumeration5snextER10UErrorCode
	.type	_ZN6icu_6724PluralKeywordEnumeration5snextER10UErrorCode, @function
_ZN6icu_6724PluralKeywordEnumeration5snextER10UErrorCode:
.LFB3886:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L84
	movl	116(%rdi), %esi
	cmpl	128(%rdi), %esi
	jge	.L84
	leal	1(%rsi), %eax
	addq	$120, %rdi
	movl	%eax, -4(%rdi)
	jmp	_ZNK6icu_677UVector9elementAtEi@PLT
	.p2align 4,,10
	.p2align 3
.L84:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3886:
	.size	_ZN6icu_6724PluralKeywordEnumeration5snextER10UErrorCode, .-_ZN6icu_6724PluralKeywordEnumeration5snextER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi.part.0, @function
_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi.part.0:
.LFB5378:
	.cfi_startproc
	movq	.LC2(%rip), %xmm3
	movsd	.LC3(%rip), %xmm5
	andpd	%xmm3, %xmm0
	movapd	%xmm0, %xmm4
	movapd	%xmm0, %xmm1
	andpd	%xmm3, %xmm4
	ucomisd	%xmm4, %xmm5
	jbe	.L89
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	movapd	%xmm3, %xmm2
	andnpd	%xmm0, %xmm2
	cvtsi2sdq	%rax, %xmm1
	orpd	%xmm2, %xmm1
.L89:
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	cmpl	$2, %edi
	je	.L90
	cmpl	$3, %edi
	je	.L91
	cmpl	$1, %edi
	je	.L100
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movsd	.LC4(%rip), %xmm0
	movsd	%xmm2, -8(%rbp)
	call	pow@PLT
	movsd	-8(%rbp), %xmm2
	movq	.LC2(%rip), %xmm3
	movsd	.LC3(%rip), %xmm6
	mulsd	%xmm2, %xmm0
	addsd	.LC5(%rip), %xmm0
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm2
	andpd	%xmm3, %xmm1
	ucomisd	%xmm1, %xmm6
	ja	.L101
.L94:
	comisd	.LC9(%rip), %xmm0
	movabsq	$9223372036854775807, %rax
	ja	.L88
	cvttsd2siq	%xmm0, %rax
.L88:
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore 6
	mulsd	.LC6(%rip), %xmm2
	movsd	.LC5(%rip), %xmm0
	addsd	%xmm2, %xmm0
	cvttsd2siq	%xmm0, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	mulsd	.LC4(%rip), %xmm2
	movsd	.LC5(%rip), %xmm0
	addsd	%xmm2, %xmm0
	cvttsd2siq	%xmm0, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	mulsd	.LC7(%rip), %xmm2
	movsd	.LC5(%rip), %xmm0
	addsd	%xmm2, %xmm0
	cvttsd2siq	%xmm0, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC8(%rip), %xmm4
	andnpd	%xmm2, %xmm3
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm7
	cmpnlesd	%xmm0, %xmm7
	movapd	%xmm7, %xmm0
	andpd	%xmm4, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	orpd	%xmm3, %xmm0
	jmp	.L94
	.cfi_endproc
.LFE5378:
	.size	_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi.part.0, .-_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi.part.0
	.p2align 4
	.globl	_ZThn8_N6icu_6712FixedDecimalD1Ev
	.type	_ZThn8_N6icu_6712FixedDecimalD1Ev, @function
_ZThn8_N6icu_6712FixedDecimalD1Ev:
.LFB5401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713IFixedDecimalD2Ev@PLT
	.cfi_endproc
.LFE5401:
	.size	_ZThn8_N6icu_6712FixedDecimalD1Ev, .-_ZThn8_N6icu_6712FixedDecimalD1Ev
	.p2align 4
	.globl	_ZThn8_N6icu_6712FixedDecimalD0Ev
	.type	_ZThn8_N6icu_6712FixedDecimalD0Ev, @function
_ZThn8_N6icu_6712FixedDecimalD0Ev:
.LFB5402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713IFixedDecimalD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE5402:
	.size	_ZThn8_N6icu_6712FixedDecimalD0Ev, .-_ZThn8_N6icu_6712FixedDecimalD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6733PluralAvailableLocalesEnumerationD0Ev
	.type	_ZN6icu_6733PluralAvailableLocalesEnumerationD0Ev, @function
_ZN6icu_6733PluralAvailableLocalesEnumerationD0Ev:
.LFB3937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6733PluralAvailableLocalesEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	call	ures_close_67@PLT
	movq	128(%r12), %rdi
	call	ures_close_67@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, 120(%r12)
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3937:
	.size	_ZN6icu_6733PluralAvailableLocalesEnumerationD0Ev, .-_ZN6icu_6733PluralAvailableLocalesEnumerationD0Ev
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE:
.LFB5338:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L111
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L110
	cmpb	$42, (%rdi)
	je	.L113
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L113
.L110:
	leaq	16(%r12), %rsi
	leaq	16(%rbx), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676LocaleeqERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5338:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713AndConstraintD2Ev
	.type	_ZN6icu_6713AndConstraintD2Ev, @function
_ZN6icu_6713AndConstraintD2Ev:
.LFB3842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713AndConstraintE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L119
	movq	(%rdi), %rax
	call	*8(%rax)
.L119:
	movq	40(%rbx), %r12
	movq	$0, 24(%rbx)
	testq	%r12, %r12
	je	.L118
	movq	(%r12), %rax
	leaq	_ZN6icu_6713AndConstraintD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L121
	call	_ZN6icu_6713AndConstraintD1Ev
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE3842:
	.size	_ZN6icu_6713AndConstraintD2Ev, .-_ZN6icu_6713AndConstraintD2Ev
	.globl	_ZN6icu_6713AndConstraintD1Ev
	.set	_ZN6icu_6713AndConstraintD1Ev,_ZN6icu_6713AndConstraintD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713AndConstraintD0Ev
	.type	_ZN6icu_6713AndConstraintD0Ev, @function
_ZN6icu_6713AndConstraintD0Ev:
.LFB3844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN6icu_6713AndConstraintE(%rip), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%r15, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L127
	movq	(%rdi), %rax
	call	*8(%rax)
.L127:
	movq	$0, 24(%r12)
	movq	40(%r12), %r13
	testq	%r13, %r13
	je	.L128
	movq	0(%r13), %rax
	leaq	_ZN6icu_6713AndConstraintD0Ev(%rip), %rbx
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L129
	movq	24(%r13), %rdi
	movq	%r15, 0(%r13)
	testq	%rdi, %rdi
	je	.L130
	movq	(%rdi), %rax
	call	*8(%rax)
.L130:
	movq	40(%r13), %r14
	movq	$0, 24(%r13)
	testq	%r14, %r14
	je	.L131
	movq	(%r14), %rax
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L132
	movq	24(%r14), %rdi
	movq	%r15, (%r14)
	testq	%rdi, %rdi
	je	.L133
	movq	(%rdi), %rax
	call	*8(%rax)
.L133:
	movq	40(%r14), %r8
	movq	$0, 24(%r14)
	testq	%r8, %r8
	je	.L134
	movq	(%r8), %rax
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L135
	movq	24(%r8), %rdi
	movq	%r15, (%r8)
	testq	%rdi, %rdi
	je	.L136
	movq	(%rdi), %rax
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-56(%rbp), %r8
.L136:
	movq	40(%r8), %r9
	movq	$0, 24(%r8)
	testq	%r9, %r9
	je	.L137
	movq	(%r9), %rax
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L138
	movq	24(%r9), %rdi
	movq	%r15, (%r9)
	testq	%rdi, %rdi
	je	.L139
	movq	(%rdi), %rax
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
.L139:
	movq	40(%r9), %r10
	movq	$0, 24(%r9)
	testq	%r10, %r10
	je	.L140
	movq	(%r10), %rax
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L141
	movq	24(%r10), %rdi
	movq	%r15, (%r10)
	testq	%rdi, %rdi
	je	.L142
	movq	(%rdi), %rax
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
.L142:
	movq	40(%r10), %r15
	movq	$0, 24(%r10)
	testq	%r15, %r15
	je	.L143
	movq	(%r15), %rax
	movq	%r10, -72(%rbp)
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	movq	8(%rax), %rax
	movq	%r8, -56(%rbp)
	cmpq	%rbx, %rax
	jne	.L144
	call	_ZN6icu_6713AndConstraintD1Ev
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r10
.L143:
	movq	%r10, %rdi
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
.L140:
	movq	%r9, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r8
.L137:
	movq	%r8, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L134:
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L131:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L128:
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r8, -56(%rbp)
	movq	%r9, %rdi
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%r9, -64(%rbp)
	movq	%r10, %rdi
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
	jmp	.L140
.L144:
	call	*%rax
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
	jmp	.L143
	.cfi_endproc
.LFE3844:
	.size	_ZN6icu_6713AndConstraintD0Ev, .-_ZN6icu_6713AndConstraintD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6716PluralRuleParser12getNextTokenER10UErrorCode.part.0, @function
_ZN6icu_6716PluralRuleParser12getNextTokenER10UErrorCode.part.0:
.LFB5375:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	leaq	.L194(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	8(%rdi), %rsi
	movslq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rsi), %eax
	movq	%rdi, %rdx
	leaq	10(%rsi), %r10
	addq	%rdi, %rdi
	testw	%ax, %ax
	js	.L183
	.p2align 4,,10
	.p2align 3
.L247:
	movswl	%ax, %ecx
	sarl	$5, %ecx
	cmpl	%edx, %ecx
	jle	.L186
.L184:
	cmpl	%ecx, %edx
	jnb	.L191
	movq	%r10, %r9
	testb	$2, %al
	jne	.L187
	movq	24(%rsi), %r9
.L187:
	movzwl	(%r9,%rdi), %eax
	leal	-48(%rax), %r12d
	cmpw	$9, %r12w
	jbe	.L188
	leal	-97(%rax), %r12d
	cmpw	$25, %r12w
	jbe	.L189
	cmpw	$64, %ax
	ja	.L190
	cmpw	$31, %ax
	jbe	.L191
	subl	$32, %eax
	cmpw	$32, %ax
	ja	.L192
	movzwl	%ax, %eax
	movslq	(%r8,%rax,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L194:
	.long	.L202-.L194
	.long	.L201-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L200-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L199-.L194
	.long	.L192-.L194
	.long	.L198-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L197-.L194
	.long	.L196-.L194
	.long	.L192-.L194
	.long	.L195-.L194
	.long	.L192-.L194
	.long	.L192-.L194
	.long	.L193-.L194
	.text
.L202:
	addl	$1, %edx
	addq	$2, %rdi
	movl	%edx, 16(%rbx)
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	jns	.L247
.L183:
	movl	12(%rsi), %ecx
	cmpl	%edx, %ecx
	jg	.L184
.L186:
	movl	$28, 88(%rbx)
.L182:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L248
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	cmpw	$126, %ax
	je	.L203
	cmpw	$8230, %ax
	jne	.L192
	movl	$9, 88(%rbx)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$1, 88(%rbx)
	cmpl	%ecx, %edx
	jge	.L186
	leal	1(%rdx), %r12d
	movl	$1, %r8d
	movslq	%r12d, %rax
	cmpl	%ecx, %r12d
	jge	.L221
	.p2align 4,,10
	.p2align 3
.L219:
	movl	%eax, %r12d
	cmpl	%ecx, %eax
	jnb	.L243
	movzwl	(%r9,%rax,2), %edi
	subl	$48, %edi
	cmpw	$9, %di
	jbe	.L249
.L243:
	movl	%r12d, %r8d
	subl	%edx, %r8d
.L221:
	movl	$1, 88(%rbx)
	.p2align 4,,10
	.p2align 3
.L213:
	leaq	-112(%rbp), %r13
	movl	%r8d, %ecx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	leaq	24(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	%r12d, 16(%rbx)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L249:
	addq	$1, %rax
	addl	$1, %r12d
	cmpl	%eax, %ecx
	jg	.L219
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$10, 88(%rbx)
	cmpl	%ecx, %edx
	jge	.L186
	leal	1(%rdx), %r12d
	movl	$1, %r8d
	movslq	%r12d, %rax
	cmpl	%ecx, %r12d
	jge	.L217
	.p2align 4,,10
	.p2align 3
.L215:
	movl	%eax, %r12d
	cmpl	%ecx, %eax
	jnb	.L242
	movzwl	(%r9,%rax,2), %edi
	leal	-48(%rdi), %r8d
	cmpw	$9, %r8w
	jbe	.L242
	subl	$97, %edi
	cmpw	$25, %di
	jbe	.L250
.L242:
	movl	%r12d, %r8d
	subl	%edx, %r8d
.L217:
	movl	$10, 88(%rbx)
	jmp	.L213
.L193:
	movl	$6, 88(%rbx)
.L205:
	leal	1(%rdx), %r12d
	movl	$1, %r8d
	cmpl	%ecx, %edx
	jl	.L213
	jmp	.L186
.L195:
	movl	$16, 88(%rbx)
	jmp	.L205
.L196:
	movl	$3, 88(%rbx)
	jmp	.L205
.L198:
	movl	$7, 88(%rbx)
	cmpl	%ecx, %edx
	jge	.L186
	leal	1(%rdx), %r12d
	movl	$1, %r8d
	cmpl	%ecx, %r12d
	jge	.L213
	jnb	.L213
	movslq	%r12d, %rax
	cmpw	$46, (%r9,%rax,2)
	leaq	(%rax,%rax), %rdi
	jne	.L213
	leal	2(%rdx), %r12d
	cmpl	%ecx, %r12d
	jge	.L222
	jnb	.L222
	cmpw	$46, 2(%r9,%rdi)
	je	.L251
.L222:
	movl	$8, 88(%rbx)
	movl	$2, %r8d
	jmp	.L213
.L199:
	movl	$2, 88(%rbx)
	jmp	.L205
.L200:
	movl	$13, 88(%rbx)
	jmp	.L205
.L201:
	movl	$17, 88(%rbx)
	cmpl	%ecx, %edx
	jge	.L186
	leal	1(%rdx), %r12d
	cmpl	%ecx, %r12d
	jnb	.L212
	movslq	%r12d, %rax
	cmpw	$61, (%r9,%rax,2)
	je	.L252
.L212:
	movl	$0, 88(%rbx)
	movl	$1, %r8d
	jmp	.L213
.L197:
	movl	$5, 88(%rbx)
	jmp	.L205
.L192:
	movl	$0, 88(%rbx)
	cmpl	%ecx, %edx
	jge	.L186
	.p2align 4,,10
	.p2align 3
.L208:
	movl	$65792, (%r11)
	leal	1(%rdx), %r12d
	movl	$1, %r8d
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L250:
	addq	$1, %rax
	addl	$1, %r12d
	cmpl	%eax, %ecx
	jg	.L215
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L203:
	movl	$18, 88(%rbx)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$0, 88(%rbx)
	jmp	.L208
.L252:
	leal	2(%rdx), %r12d
	movl	$2, %r8d
	jmp	.L213
.L251:
	movl	$9, 88(%rbx)
	leal	3(%rdx), %r12d
	movl	$3, %r8d
	jmp	.L213
.L248:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5375:
	.size	_ZN6icu_6716PluralRuleParser12getNextTokenER10UErrorCode.part.0, .-_ZN6icu_6716PluralRuleParser12getNextTokenER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712OrConstraintD2Ev
	.type	_ZN6icu_6712OrConstraintD2Ev, @function
_ZN6icu_6712OrConstraintD2Ev:
.LFB3854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712OrConstraintE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %r14
	movq	%rax, (%rdi)
	testq	%r14, %r14
	je	.L254
	movq	(%r14), %rax
	leaq	_ZN6icu_6713AndConstraintD0Ev(%rip), %r13
	movq	8(%rax), %rax
	cmpq	%r13, %rax
	jne	.L255
	movq	24(%r14), %rdi
	leaq	16+_ZTVN6icu_6713AndConstraintE(%rip), %r15
	movq	%r15, (%r14)
	testq	%rdi, %rdi
	je	.L256
	movq	(%rdi), %rax
	call	*8(%rax)
.L256:
	movq	40(%r14), %r12
	movq	$0, 24(%r14)
	testq	%r12, %r12
	je	.L257
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r13, %rax
	jne	.L258
	movq	24(%r12), %rdi
	movq	%r15, (%r12)
	testq	%rdi, %rdi
	je	.L259
	movq	(%rdi), %rax
	call	*8(%rax)
.L259:
	movq	$0, 24(%r12)
	movq	40(%r12), %r8
	testq	%r8, %r8
	je	.L260
	movq	(%r8), %rax
	movq	8(%rax), %rax
	cmpq	%r13, %rax
	jne	.L261
	movq	24(%r8), %rdi
	movq	%r15, (%r8)
	testq	%rdi, %rdi
	je	.L262
	movq	(%rdi), %rax
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-56(%rbp), %r8
.L262:
	movq	40(%r8), %r9
	movq	$0, 24(%r8)
	testq	%r9, %r9
	je	.L263
	movq	(%r9), %rax
	movq	8(%rax), %rax
	cmpq	%r13, %rax
	jne	.L264
	movq	24(%r9), %rdi
	movq	%r15, (%r9)
	testq	%rdi, %rdi
	je	.L265
	movq	(%rdi), %rax
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
.L265:
	movq	40(%r9), %r10
	movq	$0, 24(%r9)
	testq	%r10, %r10
	je	.L266
	movq	(%r10), %rax
	movq	8(%rax), %rax
	cmpq	%r13, %rax
	jne	.L267
	movq	24(%r10), %rdi
	movq	%r15, (%r10)
	testq	%rdi, %rdi
	je	.L268
	movq	(%rdi), %rax
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
.L268:
	movq	40(%r10), %r11
	movq	$0, 24(%r10)
	testq	%r11, %r11
	je	.L269
	movq	(%r11), %rax
	movq	8(%rax), %rax
	cmpq	%r13, %rax
	jne	.L270
	movq	24(%r11), %rdi
	movq	%r15, (%r11)
	testq	%rdi, %rdi
	je	.L271
	movq	(%rdi), %rax
	movq	%r11, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-80(%rbp), %r11
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
.L271:
	movq	40(%r11), %r15
	movq	$0, 24(%r11)
	testq	%r15, %r15
	je	.L272
	movq	(%r15), %rax
	movq	%r11, -80(%rbp)
	movq	%r15, %rdi
	movq	%r10, -72(%rbp)
	movq	8(%rax), %rax
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	cmpq	%r13, %rax
	jne	.L273
	call	_ZN6icu_6713AndConstraintD1Ev
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r11
.L272:
	movq	%r11, %rdi
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r10
.L269:
	movq	%r10, %rdi
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
.L266:
	movq	%r9, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r8
.L263:
	movq	%r8, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L260:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L257:
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L254:
	movq	16(%rbx), %r12
	movq	$0, 8(%rbx)
	testq	%r12, %r12
	je	.L253
	movq	(%r12), %rax
	leaq	_ZN6icu_6712OrConstraintD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L275
	call	_ZN6icu_6712OrConstraintD1Ev
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	%r14, %rdi
	call	*%rax
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L275:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L264:
	movq	%r8, -56(%rbp)
	movq	%r9, %rdi
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%r9, -64(%rbp)
	movq	%r10, %rdi
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
	jmp	.L266
.L270:
	movq	%r10, -72(%rbp)
	movq	%r11, %rdi
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
	jmp	.L269
.L273:
	call	*%rax
	movq	-80(%rbp), %r11
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
	jmp	.L272
	.cfi_endproc
.LFE3854:
	.size	_ZN6icu_6712OrConstraintD2Ev, .-_ZN6icu_6712OrConstraintD2Ev
	.globl	_ZN6icu_6712OrConstraintD1Ev
	.set	_ZN6icu_6712OrConstraintD1Ev,_ZN6icu_6712OrConstraintD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679RuleChainD2Ev
	.type	_ZN6icu_679RuleChainD2Ev, @function
_ZN6icu_679RuleChainD2Ev:
.LFB3863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679RuleChainE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	72(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L317
	movq	(%r12), %rax
	leaq	_ZN6icu_679RuleChainD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L318
	call	_ZN6icu_679RuleChainD1Ev
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L317:
	movq	80(%rbx), %r12
	testq	%r12, %r12
	je	.L319
	movq	(%r12), %rax
	leaq	_ZN6icu_6712OrConstraintD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L320
	call	_ZN6icu_6712OrConstraintD1Ev
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L319:
	leaq	152(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	88(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore_state
	call	*%rax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L320:
	call	*%rax
	jmp	.L319
	.cfi_endproc
.LFE3863:
	.size	_ZN6icu_679RuleChainD2Ev, .-_ZN6icu_679RuleChainD2Ev
	.globl	_ZN6icu_679RuleChainD1Ev
	.set	_ZN6icu_679RuleChainD1Ev,_ZN6icu_679RuleChainD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679RuleChainD0Ev
	.type	_ZN6icu_679RuleChainD0Ev, @function
_ZN6icu_679RuleChainD0Ev:
.LFB3865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_679RuleChainD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3865:
	.size	_ZN6icu_679RuleChainD0Ev, .-_ZN6icu_679RuleChainD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRulesD2Ev
	.type	_ZN6icu_6711PluralRulesD2Ev, @function
_ZN6icu_6711PluralRulesD2Ev:
.LFB3796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711PluralRulesE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L331
	movq	0(%r13), %rax
	leaq	_ZN6icu_679RuleChainD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L332
	call	_ZN6icu_679RuleChainD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L331:
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	call	*%rax
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3796:
	.size	_ZN6icu_6711PluralRulesD2Ev, .-_ZN6icu_6711PluralRulesD2Ev
	.globl	_ZN6icu_6711PluralRulesD1Ev
	.set	_ZN6icu_6711PluralRulesD1Ev,_ZN6icu_6711PluralRulesD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRulesD0Ev
	.type	_ZN6icu_6711PluralRulesD0Ev, @function
_ZN6icu_6711PluralRulesD0Ev:
.LFB3798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711PluralRulesE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L338
	movq	0(%r13), %rax
	leaq	_ZN6icu_679RuleChainD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L339
	call	_ZN6icu_679RuleChainD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L338:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	call	*%rax
	jmp	.L338
	.cfi_endproc
.LFE3798:
	.size	_ZN6icu_6711PluralRulesD0Ev, .-_ZN6icu_6711PluralRulesD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717SharedPluralRulesD0Ev
	.type	_ZN6icu_6717SharedPluralRulesD0Ev, @function
_ZN6icu_6717SharedPluralRulesD0Ev:
.LFB3802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717SharedPluralRulesE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	24(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L345
	movq	0(%r13), %rax
	leaq	_ZN6icu_6711PluralRulesD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L346
	movq	8(%r13), %r14
	leaq	16+_ZTVN6icu_6711PluralRulesE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L347
	movq	(%r14), %rax
	leaq	_ZN6icu_679RuleChainD0Ev(%rip), %rdx
	movq	%r14, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L348
	call	_ZN6icu_679RuleChainD1Ev
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L347:
	movq	%r13, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L345:
	movq	%r12, %rdi
	call	_ZN6icu_6712SharedObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L348:
	call	*%rax
	jmp	.L347
	.cfi_endproc
.LFE3802:
	.size	_ZN6icu_6717SharedPluralRulesD0Ev, .-_ZN6icu_6717SharedPluralRulesD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717SharedPluralRulesD2Ev
	.type	_ZN6icu_6717SharedPluralRulesD2Ev, @function
_ZN6icu_6717SharedPluralRulesD2Ev:
.LFB3800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717SharedPluralRulesE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	24(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L357
	movq	0(%r13), %rax
	leaq	_ZN6icu_6711PluralRulesD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L358
	movq	8(%r13), %r14
	leaq	16+_ZTVN6icu_6711PluralRulesE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L359
	movq	(%r14), %rax
	leaq	_ZN6icu_679RuleChainD0Ev(%rip), %rdx
	movq	%r14, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L360
	call	_ZN6icu_679RuleChainD1Ev
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L359:
	movq	%r13, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L357:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712SharedObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L360:
	call	*%rax
	jmp	.L359
	.cfi_endproc
.LFE3800:
	.size	_ZN6icu_6717SharedPluralRulesD2Ev, .-_ZN6icu_6717SharedPluralRulesD2Ev
	.globl	_ZN6icu_6717SharedPluralRulesD1Ev
	.set	_ZN6icu_6717SharedPluralRulesD1Ev,_ZN6icu_6717SharedPluralRulesD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712OrConstraintD0Ev
	.type	_ZN6icu_6712OrConstraintD0Ev, @function
_ZN6icu_6712OrConstraintD0Ev:
.LFB3856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712OrConstraintE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L369
	movq	0(%r13), %rax
	leaq	_ZN6icu_6713AndConstraintD0Ev(%rip), %rbx
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L370
	movq	24(%r13), %rdi
	leaq	16+_ZTVN6icu_6713AndConstraintE(%rip), %r15
	movq	%r15, 0(%r13)
	testq	%rdi, %rdi
	je	.L371
	movq	(%rdi), %rax
	call	*8(%rax)
.L371:
	movq	40(%r13), %r14
	movq	$0, 24(%r13)
	testq	%r14, %r14
	je	.L372
	movq	(%r14), %rax
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L373
	movq	24(%r14), %rdi
	movq	%r15, (%r14)
	testq	%rdi, %rdi
	je	.L374
	movq	(%rdi), %rax
	call	*8(%rax)
.L374:
	movq	40(%r14), %r8
	movq	$0, 24(%r14)
	testq	%r8, %r8
	je	.L375
	movq	(%r8), %rax
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L376
	movq	24(%r8), %rdi
	movq	%r15, (%r8)
	testq	%rdi, %rdi
	je	.L377
	movq	(%rdi), %rax
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-56(%rbp), %r8
.L377:
	movq	40(%r8), %r9
	movq	$0, 24(%r8)
	testq	%r9, %r9
	je	.L378
	movq	(%r9), %rax
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L379
	movq	24(%r9), %rdi
	movq	%r15, (%r9)
	testq	%rdi, %rdi
	je	.L380
	movq	(%rdi), %rax
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
.L380:
	movq	40(%r9), %r10
	movq	$0, 24(%r9)
	testq	%r10, %r10
	je	.L381
	movq	(%r10), %rax
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L382
	movq	24(%r10), %rdi
	movq	%r15, (%r10)
	testq	%rdi, %rdi
	je	.L383
	movq	(%rdi), %rax
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
.L383:
	movq	40(%r10), %r11
	movq	$0, 24(%r10)
	testq	%r11, %r11
	je	.L384
	movq	(%r11), %rax
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L385
	movq	24(%r11), %rdi
	movq	%r15, (%r11)
	testq	%rdi, %rdi
	je	.L386
	movq	(%rdi), %rax
	movq	%r11, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	*8(%rax)
	movq	-80(%rbp), %r11
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
.L386:
	movq	40(%r11), %r15
	movq	$0, 24(%r11)
	testq	%r15, %r15
	je	.L387
	movq	(%r15), %rax
	movq	%r11, -80(%rbp)
	movq	%r15, %rdi
	movq	%r10, -72(%rbp)
	movq	8(%rax), %rax
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	cmpq	%rbx, %rax
	jne	.L388
	call	_ZN6icu_6713AndConstraintD1Ev
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r11
.L387:
	movq	%r11, %rdi
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r10
.L384:
	movq	%r10, %rdi
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
.L381:
	movq	%r9, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-56(%rbp), %r8
.L378:
	movq	%r8, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L375:
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L372:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L369:
	movq	$0, 8(%r12)
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L389
	movq	0(%r13), %rax
	leaq	_ZN6icu_6712OrConstraintD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L390
	call	_ZN6icu_6712OrConstraintD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L389:
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L390:
	call	*%rax
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L373:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L376:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%r8, -56(%rbp)
	movq	%r9, %rdi
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%r9, -64(%rbp)
	movq	%r10, %rdi
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
	jmp	.L381
.L385:
	movq	%r10, -72(%rbp)
	movq	%r11, %rdi
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
	jmp	.L384
.L388:
	call	*%rax
	movq	-80(%rbp), %r11
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
	jmp	.L387
	.cfi_endproc
.LFE3856:
	.size	_ZN6icu_6712OrConstraintD0Ev, .-_ZN6icu_6712OrConstraintD0Ev
	.section	.text._ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi,"axG",@progbits,_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	.type	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi, @function
_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi:
.LFB2319:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %eax
	movq	(%rsi), %rcx
	movl	%edx, %r9d
	testw	%ax, %ax
	js	.L435
	movswl	%ax, %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	sarl	$5, %edx
	jmp	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	.p2align 4,,10
	.p2align 3
.L435:
	movl	12(%rdi), %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	jmp	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	.cfi_endproc
.LFE2319:
	.size	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi, .-_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB4292:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4292:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB4295:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L450
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L438
	cmpb	$0, 12(%rbx)
	jne	.L451
.L442:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L438:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L451:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L442
	.cfi_endproc
.LFE4295:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB4298:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L454
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4298:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB4301:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L457
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE4301:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB4303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L463
.L459:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L464
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L464:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4303:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB4304:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4304:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB4305:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4305:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB4306:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4306:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4307:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4307:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4308:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4308:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB4309:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L480
	testl	%edx, %edx
	jle	.L480
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L483
.L472:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L480:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L472
	.cfi_endproc
.LFE4309:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB4310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L487
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L487
	testl	%r12d, %r12d
	jg	.L494
	cmpb	$0, 12(%rbx)
	jne	.L495
.L489:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L489
.L495:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L487:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4310:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB4311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L497
	movq	(%rdi), %r8
.L498:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L501
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L501
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L501:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4311:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB4312:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L508
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4312:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB4313:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4313:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB4314:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4314:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB4315:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4315:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB4317:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4317:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB4319:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4319:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRules16getStaticClassIDEv
	.type	_ZN6icu_6711PluralRules16getStaticClassIDEv, @function
_ZN6icu_6711PluralRules16getStaticClassIDEv:
.LFB3785:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6711PluralRules16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3785:
	.size	_ZN6icu_6711PluralRules16getStaticClassIDEv, .-_ZN6icu_6711PluralRules16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724PluralKeywordEnumeration16getStaticClassIDEv
	.type	_ZN6icu_6724PluralKeywordEnumeration16getStaticClassIDEv, @function
_ZN6icu_6724PluralKeywordEnumeration16getStaticClassIDEv:
.LFB3787:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6724PluralKeywordEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3787:
	.size	_ZN6icu_6724PluralKeywordEnumeration16getStaticClassIDEv, .-_ZN6icu_6724PluralKeywordEnumeration16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRulesC2ER10UErrorCode
	.type	_ZN6icu_6711PluralRulesC2ER10UErrorCode, @function
_ZN6icu_6711PluralRulesC2ER10UErrorCode:
.LFB3790:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711PluralRulesE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$0, 16(%rdi)
	ret
	.cfi_endproc
.LFE3790:
	.size	_ZN6icu_6711PluralRulesC2ER10UErrorCode, .-_ZN6icu_6711PluralRulesC2ER10UErrorCode
	.globl	_ZN6icu_6711PluralRulesC1ER10UErrorCode
	.set	_ZN6icu_6711PluralRulesC1ER10UErrorCode,_ZN6icu_6711PluralRulesC2ER10UErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC10:
	.string	"plurals"
.LC11:
	.string	"locales"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRules19getAvailableLocalesER10UErrorCode
	.type	_ZN6icu_6711PluralRules19getAvailableLocalesER10UErrorCode, @function
_ZN6icu_6711PluralRules19getAvailableLocalesER10UErrorCode:
.LFB3805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L530
	movq	%rdi, %rbx
	movl	$136, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L519
	movq	%rax, %rdi
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	leaq	16+_ZTVN6icu_6733PluralAvailableLocalesEnumerationE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%r12)
	movl	(%rbx), %eax
	movups	%xmm0, 120(%r12)
	testl	%eax, %eax
	jle	.L531
	movl	%eax, 116(%r12)
.L522:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L517
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L530:
	xorl	%r12d, %r12d
.L517:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	.cfi_restore_state
	movl	$0, 116(%r12)
	leaq	116(%r12), %r14
	leaq	.LC10(%rip), %rsi
	xorl	%edi, %edi
	movq	%r14, %rdx
	call	ures_openDirect_67@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ures_getByKey_67@PLT
	movq	%rax, 120(%r12)
	testq	%r13, %r13
	je	.L522
	movq	%r13, %rdi
	call	ures_close_67@PLT
	jmp	.L522
.L519:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L530
	movl	$7, (%rbx)
	jmp	.L530
	.cfi_endproc
.LFE3805:
	.size	_ZN6icu_6711PluralRules19getAvailableLocalesER10UErrorCode, .-_ZN6icu_6711PluralRules19getAvailableLocalesER10UErrorCode
	.section	.rodata.str1.1
.LC12:
	.string	"%1.15e"
	.section	.text.unlikely
	.align 2
.LCOLDB13:
	.text
.LHOTB13:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PluralRules6selectEi
	.type	_ZNK6icu_6711PluralRules6selectEi, @function
_ZNK6icu_6711PluralRules6selectEi:
.LFB3813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	cvtsi2sdl	%ebx, %xmm0
	subq	$168, %rsp
	andpd	.LC2(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	leaq	-64(%rax), %rdx
	ucomisd	%xmm0, %xmm0
	movq	%rax, %xmm4
	movq	%rdx, %xmm3
	punpcklqdq	%xmm4, %xmm3
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm3, -160(%rbp)
	jnp	.L628
.L604:
	movsd	.LC4(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.L629
.L605:
	movsd	.LC6(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.L630
.L606:
	movsd	.LC7(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.L631
.L607:
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	leaq	-96(%rbp), %rdi
	movl	$30, %edx
	movw	%ax, -68(%rbp)
	leaq	.LC12(%rip), %rcx
	movl	$1, %esi
	movl	$1, %eax
	movsd	%xmm0, -200(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	$0, -80(%rbp)
	movl	$0, -72(%rbp)
	call	__sprintf_chk@PLT
	xorl	%esi, %esi
	leaq	-78(%rbp), %rdi
	movl	$10, %edx
	call	strtol@PLT
	cmpb	$48, -80(%rbp)
	movsd	-200(%rbp), %xmm0
	jne	.L583
	cmpb	$48, -81(%rbp)
	jne	.L584
	cmpb	$48, -82(%rbp)
	jne	.L585
	cmpb	$48, -83(%rbp)
	jne	.L586
	cmpb	$48, -84(%rbp)
	jne	.L587
	cmpb	$48, -85(%rbp)
	jne	.L588
	cmpb	$48, -86(%rbp)
	jne	.L589
	cmpb	$48, -87(%rbp)
	jne	.L590
	cmpb	$48, -88(%rbp)
	jne	.L591
	cmpb	$48, -89(%rbp)
	jne	.L592
	cmpb	$48, -90(%rbp)
	jne	.L593
	cmpb	$48, -91(%rbp)
	jne	.L594
	cmpb	$48, -92(%rbp)
	jne	.L595
	cmpb	$48, -93(%rbp)
	jne	.L596
	cmpb	$48, -94(%rbp)
	jne	.L597
	xorl	%r14d, %r14d
	cmpb	$48, -95(%rbp)
	sete	%r14b
	negl	%r14d
	.p2align 4,,10
	.p2align 3
.L538:
	subl	%eax, %r14d
.L533:
	shrl	$31, %ebx
	movsd	%xmm0, -144(%rbp)
	movb	%bl, -103(%rbp)
	call	uprv_isNaN_67@PLT
	movsd	-144(%rbp), %xmm0
	movb	%al, -102(%rbp)
	call	uprv_isInfinite_67@PLT
	movb	%al, -101(%rbp)
	cmpw	$0, -102(%rbp)
	jne	.L598
	movsd	-144(%rbp), %xmm0
	pxor	%xmm1, %xmm1
	movl	$0, %esi
	cvttsd2siq	%xmm0, %rcx
	cvtsi2sdq	%rcx, %xmm1
	ucomisd	%xmm0, %xmm1
	setnp	%dl
	cmovne	%esi, %edx
.L539:
	movq	8(%r13), %r15
	pxor	%xmm0, %xmm0
	movb	%dl, -104(%rbp)
	movq	%rcx, -112(%rbp)
	movl	%r14d, -136(%rbp)
	movaps	%xmm0, -128(%rbp)
	testq	%r15, %r15
	je	.L632
	orb	-102(%rbp), %al
	je	.L542
.L570:
	leaq	_ZN6icu_67L20PLURAL_KEYWORD_OTHERE(%rip), %rax
	leaq	-168(%rbp), %rdx
	movl	$5, %ecx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
.L541:
	movdqa	-192(%rbp), %xmm5
	leaq	-152(%rbp), %rdi
	leaq	-160(%rbp), %r13
	movaps	%xmm5, -160(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713IFixedDecimalD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L633
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L555:
	.cfi_restore_state
	movapd	%xmm2, %xmm0
	movsd	%xmm2, -200(%rbp)
	call	uprv_floor_67@PLT
	movsd	-200(%rbp), %xmm2
	ucomisd	%xmm2, %xmm0
	jp	.L558
	je	.L559
.L558:
	movzbl	32(%rbx), %eax
	xorl	%edx, %edx
	testb	%al, %al
	je	.L577
	movl	$1, %eax
.L566:
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L567
	testb	%dl, %dl
	je	.L544
.L567:
	movl	%eax, %edx
	andl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L577:
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.L608
	testb	%dl, %dl
	je	.L568
.L608:
	testb	%al, %al
	jne	.L545
	.p2align 4,,10
	.p2align 3
.L543:
	movq	72(%r15), %r15
	testq	%r15, %r15
	je	.L570
.L542:
	movq	80(%r15), %r14
	testq	%r14, %r14
	je	.L543
	leaq	CSWTCH.347(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L568:
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.L545
	.p2align 4,,10
	.p2align 3
.L544:
	movl	36(%rbx), %eax
	testl	%eax, %eax
	je	.L546
	subl	$21, %eax
	cmpl	$4, %eax
	ja	.L547
	cmpl	$6, 0(%r13,%rax,4)
	ja	.L547
	movl	0(%r13,%rax,4), %eax
	leaq	.L549(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L549:
	.long	.L554-.L549
	.long	.L553-.L549
	.long	.L552-.L549
	.long	.L551-.L549
	.long	.L550-.L549
	.long	.L547-.L549
	.long	.L599-.L549
	.text
	.p2align 4,,10
	.p2align 3
.L599:
	pxor	%xmm2, %xmm2
	.p2align 4,,10
	.p2align 3
.L548:
	cmpb	$0, 33(%rbx)
	jne	.L555
.L559:
	cmpl	$1, 8(%rbx)
	je	.L634
.L557:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L635
	movl	8(%rsi), %edx
	testl	%edx, %edx
	jle	.L558
	leal	-1(%rdx), %eax
	leal	-3(%rdx), %r8d
	movl	%eax, %edi
	andl	$-2, %edi
	subl	%edi, %r8d
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L564:
	leal	1(%rax), %r9d
	pxor	%xmm0, %xmm0
	testl	%r9d, %r9d
	jle	.L563
	movq	24(%rsi), %r9
	pxor	%xmm0, %xmm0
	cvtsi2sdl	(%r9,%rdi), %xmm0
.L563:
	comisd	%xmm0, %xmm2
	jnb	.L636
.L574:
	subl	$2, %eax
	addq	$8, %rdi
	cmpl	%r8d, %eax
	jne	.L564
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L550:
	pxor	%xmm2, %xmm2
	cvtsi2sdl	-136(%rbp), %xmm2
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L551:
	pxor	%xmm2, %xmm2
	cvtsi2sdq	-120(%rbp), %xmm2
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L552:
	pxor	%xmm2, %xmm2
	cvtsi2sdq	-128(%rbp), %xmm2
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L553:
	pxor	%xmm2, %xmm2
	cvtsi2sdq	-112(%rbp), %xmm2
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L554:
	movsd	-144(%rbp), %xmm2
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L587:
	movl	$11, %r14d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L546:
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L544
	.p2align 4,,10
	.p2align 3
.L545:
	leaq	8(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L635:
	movl	16(%rbx), %eax
	cmpl	$-1, %eax
	je	.L600
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	cvtsi2sdl	%eax, %xmm0
	movl	$0, %eax
	ucomisd	%xmm2, %xmm0
	setnp	%dl
	cmovne	%eax, %edx
	setp	%al
	cmovne	%edi, %eax
	movl	%edx, %esi
.L562:
	cmpb	$0, 32(%rbx)
	jne	.L566
	andl	$1, %eax
	movl	%eax, %edx
	movl	%esi, %eax
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L634:
	pxor	%xmm1, %xmm1
	movapd	%xmm2, %xmm0
	cvtsi2sdl	12(%rbx), %xmm1
	call	fmod@PLT
	movapd	%xmm0, %xmm2
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L600:
	xorl	%eax, %eax
	movl	$1, %esi
	movl	$1, %edx
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L636:
	movl	%edx, %r9d
	pxor	%xmm0, %xmm0
	subl	%eax, %r9d
	testl	%eax, %eax
	jle	.L572
	movq	24(%rsi), %r10
	movslq	%r9d, %r9
	pxor	%xmm0, %xmm0
	cvtsi2sdl	(%r10,%r9,4), %xmm0
.L572:
	comisd	%xmm2, %xmm0
	jb	.L574
	xorl	%eax, %eax
	movl	$1, %edx
	movl	$1, %esi
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L598:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r14d, %r14d
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L632:
	leaq	_ZN6icu_67L19PLURAL_DEFAULT_RULEE(%rip), %rax
	leaq	-168(%rbp), %rdx
	movl	$-1, %ecx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L583:
	movl	$15, %r14d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L584:
	movl	$14, %r14d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L585:
	movl	$13, %r14d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L628:
	movl	$0, %r14d
	je	.L533
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L586:
	movl	$12, %r14d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L629:
	movl	$1, %r14d
	je	.L533
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L630:
	movl	$2, %r14d
	je	.L533
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L631:
	movl	$3, %r14d
	je	.L533
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L588:
	movl	$10, %r14d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L595:
	movl	$3, %r14d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L589:
	movl	$9, %r14d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L590:
	movl	$8, %r14d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L591:
	movl	$7, %r14d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L592:
	movl	$6, %r14d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L593:
	movl	$5, %r14d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L594:
	movl	$4, %r14d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L596:
	movl	$2, %r14d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L597:
	movl	$1, %r14d
	jmp	.L538
.L633:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_6711PluralRules6selectEi.cold, @function
_ZNK6icu_6711PluralRules6selectEi.cold:
.LFSB3813:
.L547:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3813:
	.text
	.size	_ZNK6icu_6711PluralRules6selectEi, .-_ZNK6icu_6711PluralRules6selectEi
	.section	.text.unlikely
	.size	_ZNK6icu_6711PluralRules6selectEi.cold, .-_ZNK6icu_6711PluralRules6selectEi.cold
.LCOLDE13:
	.text
.LHOTE13:
	.section	.text.unlikely
	.align 2
.LCOLDB14:
	.text
.LHOTB14:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PluralRules6selectEd
	.type	_ZNK6icu_6711PluralRules6selectEd, @function
_ZNK6icu_6711PluralRules6selectEd:
.LFB3814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm3
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	.LC2(%rip), %xmm2
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm7
	andpd	%xmm2, %xmm1
	movq	%rdx, %xmm4
	movapd	%xmm1, %xmm0
	movapd	%xmm1, %xmm5
	punpcklqdq	%xmm7, %xmm4
	andpd	%xmm2, %xmm0
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm4, -160(%rbp)
	movsd	.LC3(%rip), %xmm4
	ucomisd	%xmm0, %xmm4
	jbe	.L638
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm0, %xmm0
	movsd	.LC8(%rip), %xmm7
	movapd	%xmm2, %xmm6
	andnpd	%xmm1, %xmm6
	cvtsi2sdq	%rax, %xmm0
	movapd	%xmm0, %xmm5
	cmpnlesd	%xmm1, %xmm5
	andpd	%xmm7, %xmm5
	subsd	%xmm5, %xmm0
	movapd	%xmm0, %xmm5
	orpd	%xmm6, %xmm5
.L638:
	ucomisd	%xmm5, %xmm1
	jnp	.L749
.L639:
	movsd	.LC4(%rip), %xmm0
	movapd	%xmm2, %xmm7
	mulsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm5
	movapd	%xmm0, %xmm6
	andpd	%xmm2, %xmm5
	ucomisd	%xmm5, %xmm4
	ja	.L750
.L642:
	ucomisd	%xmm6, %xmm0
	jnp	.L751
.L643:
	movsd	.LC6(%rip), %xmm0
	movapd	%xmm2, %xmm7
	mulsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm5
	movapd	%xmm0, %xmm6
	andpd	%xmm2, %xmm5
	ucomisd	%xmm5, %xmm4
	ja	.L752
.L646:
	ucomisd	%xmm0, %xmm6
	jnp	.L753
.L724:
	movsd	.LC7(%rip), %xmm0
	movapd	%xmm2, %xmm7
	mulsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm5
	movapd	%xmm0, %xmm6
	andpd	%xmm2, %xmm5
	ucomisd	%xmm5, %xmm4
	ja	.L754
.L649:
	ucomisd	%xmm6, %xmm0
	jnp	.L755
.L725:
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rdi
	movl	$30, %edx
	leaq	.LC12(%rip), %rcx
	movw	%ax, -68(%rbp)
	movl	$1, %esi
	movl	$1, %eax
	movaps	%xmm0, -96(%rbp)
	movapd	%xmm1, %xmm0
	movsd	%xmm3, -208(%rbp)
	movsd	%xmm1, -200(%rbp)
	movq	$0, -80(%rbp)
	movl	$0, -72(%rbp)
	call	__sprintf_chk@PLT
	xorl	%esi, %esi
	leaq	-78(%rbp), %rdi
	movl	$10, %edx
	call	strtol@PLT
	movq	.LC3(%rip), %rcx
	cmpb	$48, -80(%rbp)
	movsd	-200(%rbp), %xmm1
	movsd	-208(%rbp), %xmm3
	movq	.LC2(%rip), %xmm2
	movq	%rcx, %xmm4
	jne	.L702
	cmpb	$48, -81(%rbp)
	jne	.L703
	cmpb	$48, -82(%rbp)
	jne	.L704
	cmpb	$48, -83(%rbp)
	jne	.L705
	cmpb	$48, -84(%rbp)
	jne	.L706
	cmpb	$48, -85(%rbp)
	jne	.L707
	cmpb	$48, -86(%rbp)
	jne	.L708
	cmpb	$48, -87(%rbp)
	jne	.L709
	cmpb	$48, -88(%rbp)
	jne	.L710
	cmpb	$48, -89(%rbp)
	jne	.L711
	cmpb	$48, -90(%rbp)
	jne	.L712
	cmpb	$48, -91(%rbp)
	jne	.L713
	cmpb	$48, -92(%rbp)
	jne	.L714
	cmpb	$48, -93(%rbp)
	jne	.L715
	cmpb	$48, -94(%rbp)
	jne	.L716
	xorl	%r14d, %r14d
	cmpb	$48, -95(%rbp)
	sete	%r14b
	negl	%r14d
	.p2align 4,,10
	.p2align 3
.L651:
	subl	%eax, %r14d
	je	.L641
.L645:
	movapd	%xmm3, %xmm0
	movapd	%xmm3, %xmm5
	andpd	%xmm2, %xmm0
	ucomisd	%xmm0, %xmm4
	jbe	.L653
	cvttsd2siq	%xmm3, %rax
	pxor	%xmm0, %xmm0
	movsd	.LC8(%rip), %xmm5
	andnpd	%xmm3, %xmm2
	cvtsi2sdq	%rax, %xmm0
	movapd	%xmm0, %xmm4
	cmpnlesd	%xmm3, %xmm4
	andpd	%xmm5, %xmm4
	movapd	%xmm0, %xmm5
	subsd	%xmm4, %xmm5
	orpd	%xmm2, %xmm5
.L653:
	ucomisd	%xmm5, %xmm3
	jp	.L726
	jne	.L726
.L641:
	xorl	%ebx, %ebx
.L652:
	pxor	%xmm7, %xmm7
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -144(%rbp)
	comisd	%xmm3, %xmm7
	seta	-103(%rbp)
	call	uprv_isNaN_67@PLT
	movsd	-144(%rbp), %xmm0
	movb	%al, -102(%rbp)
	call	uprv_isInfinite_67@PLT
	movb	%al, -101(%rbp)
	cmpw	$0, -102(%rbp)
	movl	%eax, %ecx
	je	.L655
	movq	$0, -112(%rbp)
	movb	$0, -104(%rbp)
	movl	$0, -136(%rbp)
	movq	$0, -128(%rbp)
.L656:
	movq	8(%r13), %r15
	movq	$0, -120(%rbp)
	testq	%r15, %r15
	je	.L756
.L661:
	orb	-102(%rbp), %cl
	je	.L663
.L691:
	leaq	_ZN6icu_67L20PLURAL_KEYWORD_OTHERE(%rip), %rax
	leaq	-168(%rbp), %rdx
	movl	$5, %ecx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
.L662:
	movdqa	-192(%rbp), %xmm3
	leaq	-152(%rbp), %rdi
	leaq	-160(%rbp), %r13
	movaps	%xmm3, -160(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713IFixedDecimalD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L757
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L750:
	.cfi_restore_state
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm5, %xmm5
	andnpd	%xmm0, %xmm7
	movsd	.LC8(%rip), %xmm8
	cvtsi2sdq	%rax, %xmm5
	movapd	%xmm5, %xmm6
	cmpnlesd	%xmm0, %xmm6
	andpd	%xmm8, %xmm6
	subsd	%xmm6, %xmm5
	movapd	%xmm5, %xmm6
	orpd	%xmm7, %xmm6
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L752:
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm5, %xmm5
	andnpd	%xmm0, %xmm7
	movsd	.LC8(%rip), %xmm8
	cvtsi2sdq	%rax, %xmm5
	movapd	%xmm5, %xmm6
	cmpnlesd	%xmm0, %xmm6
	andpd	%xmm8, %xmm6
	subsd	%xmm6, %xmm5
	movapd	%xmm5, %xmm6
	orpd	%xmm7, %xmm6
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L754:
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm5, %xmm5
	andnpd	%xmm0, %xmm7
	movsd	.LC8(%rip), %xmm8
	cvtsi2sdq	%rax, %xmm5
	movapd	%xmm5, %xmm6
	cmpnlesd	%xmm0, %xmm6
	andpd	%xmm8, %xmm6
	subsd	%xmm6, %xmm5
	movapd	%xmm5, %xmm6
	orpd	%xmm7, %xmm6
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L676:
	movapd	%xmm2, %xmm0
	movsd	%xmm2, -200(%rbp)
	call	uprv_floor_67@PLT
	movsd	-200(%rbp), %xmm2
	ucomisd	%xmm2, %xmm0
	jp	.L679
	je	.L680
.L679:
	movzbl	32(%rbx), %eax
	xorl	%edx, %edx
	testb	%al, %al
	je	.L698
	movl	$1, %eax
.L687:
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L688
	testb	%dl, %dl
	je	.L665
.L688:
	movl	%eax, %edx
	andl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L698:
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.L727
	testb	%dl, %dl
	je	.L689
.L727:
	testb	%al, %al
	jne	.L666
	.p2align 4,,10
	.p2align 3
.L664:
	movq	72(%r15), %r15
	testq	%r15, %r15
	je	.L691
.L663:
	movq	80(%r15), %r14
	testq	%r14, %r14
	je	.L664
	leaq	CSWTCH.347(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L689:
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.L666
	.p2align 4,,10
	.p2align 3
.L665:
	movl	36(%rbx), %eax
	testl	%eax, %eax
	je	.L667
	subl	$21, %eax
	cmpl	$4, %eax
	ja	.L668
	cmpl	$6, 0(%r13,%rax,4)
	ja	.L668
	movl	0(%r13,%rax,4), %eax
	leaq	.L670(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L670:
	.long	.L675-.L670
	.long	.L674-.L670
	.long	.L673-.L670
	.long	.L672-.L670
	.long	.L671-.L670
	.long	.L668-.L670
	.long	.L717-.L670
	.text
	.p2align 4,,10
	.p2align 3
.L717:
	pxor	%xmm2, %xmm2
	.p2align 4,,10
	.p2align 3
.L669:
	cmpb	$0, 33(%rbx)
	jne	.L676
.L680:
	cmpl	$1, 8(%rbx)
	je	.L758
.L678:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L759
	movl	8(%rsi), %edx
	testl	%edx, %edx
	jle	.L679
	leal	-1(%rdx), %eax
	leal	-3(%rdx), %r8d
	movl	%eax, %edi
	andl	$-2, %edi
	subl	%edi, %r8d
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L685:
	leal	1(%rax), %r9d
	pxor	%xmm0, %xmm0
	testl	%r9d, %r9d
	jle	.L684
	movq	24(%rsi), %r9
	pxor	%xmm0, %xmm0
	cvtsi2sdl	(%r9,%rdi), %xmm0
.L684:
	comisd	%xmm0, %xmm2
	jnb	.L760
.L695:
	subl	$2, %eax
	addq	$8, %rdi
	cmpl	%r8d, %eax
	jne	.L685
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L671:
	pxor	%xmm2, %xmm2
	cvtsi2sdl	-136(%rbp), %xmm2
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L673:
	pxor	%xmm2, %xmm2
	cvtsi2sdq	-128(%rbp), %xmm2
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L674:
	pxor	%xmm2, %xmm2
	cvtsi2sdq	-112(%rbp), %xmm2
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L672:
	pxor	%xmm2, %xmm2
	cvtsi2sdq	-120(%rbp), %xmm2
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L675:
	movsd	-144(%rbp), %xmm2
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L706:
	movl	$11, %r14d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L726:
	movapd	%xmm3, %xmm0
	movsd	%xmm1, -208(%rbp)
	movsd	%xmm3, -200(%rbp)
	call	uprv_isNaN_67@PLT
	movsd	-200(%rbp), %xmm3
	movsd	-208(%rbp), %xmm1
	testb	%al, %al
	jne	.L641
	movapd	%xmm3, %xmm0
	call	uprv_isPositiveInfinity_67@PLT
	movsd	-200(%rbp), %xmm3
	movsd	-208(%rbp), %xmm1
	testb	%al, %al
	jne	.L641
	movapd	%xmm3, %xmm0
	movl	%r14d, %edi
	call	_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi.part.0
	movsd	-208(%rbp), %xmm1
	movsd	-200(%rbp), %xmm3
	movq	%rax, %rbx
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L667:
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L665
	.p2align 4,,10
	.p2align 3
.L666:
	leaq	8(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L760:
	movl	%edx, %r9d
	pxor	%xmm0, %xmm0
	subl	%eax, %r9d
	testl	%eax, %eax
	jle	.L693
	movq	24(%rsi), %r10
	movslq	%r9d, %r9
	pxor	%xmm0, %xmm0
	cvtsi2sdl	(%r10,%r9,4), %xmm0
.L693:
	comisd	%xmm2, %xmm0
	jb	.L695
	xorl	%eax, %eax
	movl	$1, %edx
	movl	$1, %esi
.L683:
	cmpb	$0, 32(%rbx)
	jne	.L687
	andl	$1, %eax
	movl	%eax, %edx
	movl	%esi, %eax
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L759:
	movl	16(%rbx), %eax
	cmpl	$-1, %eax
	je	.L718
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	cvtsi2sdl	%eax, %xmm0
	movl	$0, %eax
	ucomisd	%xmm2, %xmm0
	setnp	%dl
	cmovne	%eax, %edx
	setp	%al
	cmovne	%edi, %eax
	movl	%edx, %esi
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L758:
	pxor	%xmm1, %xmm1
	movapd	%xmm2, %xmm0
	cvtsi2sdl	12(%rbx), %xmm1
	call	fmod@PLT
	movapd	%xmm0, %xmm2
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L718:
	xorl	%eax, %eax
	movl	$1, %esi
	movl	$1, %edx
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L655:
	movsd	-144(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movl	$0, %edx
	movl	%r14d, -136(%rbp)
	movq	%rbx, -128(%rbp)
	cvttsd2siq	%xmm1, %rax
	cvtsi2sdq	%rax, %xmm0
	movq	%rax, -112(%rbp)
	ucomisd	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	movb	%al, -104(%rbp)
	testq	%rbx, %rbx
	je	.L656
	movabsq	$-3689348814741910323, %rdi
	movq	%rbx, %rax
	movabsq	$1844674407370955160, %rsi
	imulq	%rdi, %rax
	addq	%rsi, %rax
	rorq	%rax
	cmpq	%rsi, %rax
	ja	.L659
	movabsq	$7378697629483820647, %r8
	.p2align 4,,10
	.p2align 3
.L660:
	movq	%rbx, %rax
	sarq	$63, %rbx
	imulq	%r8
	sarq	$2, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rax
	movq	%rdx, %rbx
	imulq	%rdi, %rax
	addq	%rsi, %rax
	rorq	%rax
	cmpq	%rsi, %rax
	jbe	.L660
.L659:
	movq	8(%r13), %r15
	movq	%rbx, -120(%rbp)
	testq	%r15, %r15
	jne	.L661
.L756:
	leaq	_ZN6icu_67L19PLURAL_DEFAULT_RULEE(%rip), %rax
	leaq	-168(%rbp), %rdx
	movl	$-1, %ecx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L702:
	movl	$15, %r14d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L703:
	movl	$14, %r14d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L704:
	movl	$13, %r14d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L749:
	movl	$0, %r14d
	je	.L641
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L705:
	movl	$12, %r14d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L751:
	movl	$1, %r14d
	je	.L645
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L753:
	movl	$2, %r14d
	je	.L645
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L755:
	movl	$3, %r14d
	je	.L645
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L707:
	movl	$10, %r14d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L714:
	movl	$3, %r14d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L708:
	movl	$9, %r14d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L709:
	movl	$8, %r14d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L710:
	movl	$7, %r14d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L711:
	movl	$6, %r14d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L712:
	movl	$5, %r14d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L713:
	movl	$4, %r14d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L715:
	movl	$2, %r14d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L716:
	movl	$1, %r14d
	jmp	.L651
.L757:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_6711PluralRules6selectEd.cold, @function
_ZNK6icu_6711PluralRules6selectEd.cold:
.LFSB3814:
.L668:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3814:
	.text
	.size	_ZNK6icu_6711PluralRules6selectEd, .-_ZNK6icu_6711PluralRules6selectEd
	.section	.text.unlikely
	.size	_ZNK6icu_6711PluralRules6selectEd.cold, .-_ZNK6icu_6711PluralRules6selectEd.cold
.LCOLDE14:
	.text
.LHOTE14:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRules21getUniqueKeywordValueERKNS_13UnicodeStringE
	.type	_ZN6icu_6711PluralRules21getUniqueKeywordValueERKNS_13UnicodeStringE, @function
_ZN6icu_6711PluralRules21getUniqueKeywordValueERKNS_13UnicodeStringE:
.LFB3818:
	.cfi_startproc
	endbr64
	movsd	.LC15(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3818:
	.size	_ZN6icu_6711PluralRules21getUniqueKeywordValueERKNS_13UnicodeStringE, .-_ZN6icu_6711PluralRules21getUniqueKeywordValueERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRules19getAllKeywordValuesERKNS_13UnicodeStringEPdiR10UErrorCode
	.type	_ZN6icu_6711PluralRules19getAllKeywordValuesERKNS_13UnicodeStringEPdiR10UErrorCode, @function
_ZN6icu_6711PluralRules19getAllKeywordValuesERKNS_13UnicodeStringEPdiR10UErrorCode:
.LFB3819:
	.cfi_startproc
	endbr64
	movl	$16, (%r8)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3819:
	.size	_ZN6icu_6711PluralRules19getAllKeywordValuesERKNS_13UnicodeStringEPdiR10UErrorCode, .-_ZN6icu_6711PluralRules19getAllKeywordValuesERKNS_13UnicodeStringEPdiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PluralRules15rulesForKeywordERKNS_13UnicodeStringE
	.type	_ZNK6icu_6711PluralRules15rulesForKeywordERKNS_13UnicodeStringE, @function
_ZNK6icu_6711PluralRules15rulesForKeywordERKNS_13UnicodeStringE:
.LFB3823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %r12
	movq	%rsi, %rbx
	testq	%r12, %r12
	jne	.L764
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L786:
	testw	%dx, %dx
	js	.L767
	sarl	$5, %edx
.L768:
	testw	%ax, %ax
	js	.L769
	sarl	$5, %eax
.L770:
	cmpl	%edx, %eax
	jne	.L771
	testb	%cl, %cl
	je	.L785
.L771:
	movq	72(%r12), %r12
	testq	%r12, %r12
	je	.L772
.L764:
	movswl	8(%rbx), %eax
	movswl	16(%r12), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	je	.L786
	testb	%cl, %cl
	je	.L771
.L763:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L769:
	.cfi_restore_state
	movl	12(%rbx), %eax
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L767:
	movl	20(%r12), %edx
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L772:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L785:
	.cfi_restore_state
	leaq	8(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	testb	%cl, %cl
	je	.L771
	jmp	.L763
	.cfi_endproc
.LFE3823:
	.size	_ZNK6icu_6711PluralRules15rulesForKeywordERKNS_13UnicodeStringE, .-_ZNK6icu_6711PluralRules15rulesForKeywordERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PluralRules9isKeywordERKNS_13UnicodeStringE
	.type	_ZNK6icu_6711PluralRules9isKeywordERKNS_13UnicodeStringE, @function
_ZNK6icu_6711PluralRules9isKeywordERKNS_13UnicodeStringE:
.LFB3824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movswl	8(%rsi), %edx
	movq	%rsi, %rbx
	testw	%dx, %dx
	js	.L788
	sarl	$5, %edx
.L789:
	movl	$5, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	_ZN6icu_67L20PLURAL_KEYWORD_OTHERE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L20PLURAL_KEYWORD_OTHERE(%rip), %rdx
	movl	$1, %r8d
	testb	%al, %al
	je	.L787
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.L791
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L814:
	testw	%dx, %dx
	js	.L794
	sarl	$5, %edx
.L795:
	testw	%ax, %ax
	js	.L796
	sarl	$5, %eax
.L797:
	cmpl	%edx, %eax
	jne	.L798
	testb	%r8b, %r8b
	je	.L813
.L798:
	movq	72(%r12), %r12
	testq	%r12, %r12
	je	.L799
.L791:
	movswl	8(%rbx), %eax
	movswl	16(%r12), %edx
	movl	%eax, %r8d
	andl	$1, %r8d
	testb	$1, %dl
	je	.L814
	testb	%r8b, %r8b
	je	.L798
.L787:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L796:
	.cfi_restore_state
	movl	12(%rbx), %eax
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L794:
	movl	20(%r12), %edx
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L788:
	movl	12(%rsi), %edx
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L799:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L813:
	.cfi_restore_state
	leaq	8(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r8b
	testb	%r8b, %r8b
	je	.L798
	jmp	.L787
	.cfi_endproc
.LFE3824:
	.size	_ZNK6icu_6711PluralRules9isKeywordERKNS_13UnicodeStringE, .-_ZNK6icu_6711PluralRules9isKeywordERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PluralRules15getKeywordOtherEv
	.type	_ZNK6icu_6711PluralRules15getKeywordOtherEv, @function
_ZNK6icu_6711PluralRules15getKeywordOtherEv:
.LFB3825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	_ZN6icu_67L20PLURAL_KEYWORD_OTHERE(%rip), %rax
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L818
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L818:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3825:
	.size	_ZNK6icu_6711PluralRules15getKeywordOtherEv, .-_ZNK6icu_6711PluralRules15getKeywordOtherEv
	.section	.rodata.str1.1
.LC16:
	.string	"locales_ordinals"
.LC17:
	.string	"rules"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRules19getRuleFromResourceERKNS_6LocaleE11UPluralTypeR10UErrorCode
	.type	_ZN6icu_6711PluralRules19getRuleFromResourceERKNS_6LocaleE11UPluralTypeR10UErrorCode, @function
_ZN6icu_6711PluralRules19getRuleFromResourceERKNS_6LocaleE11UPluralTypeR10UErrorCode:
.LFB3836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	$2, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	subq	$648, %rsp
	movl	(%r8), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -576(%rbp)
	movw	%r13w, -568(%rbp)
	testl	%r14d, %r14d
	jle	.L820
	leaq	-576(%rbp), %r13
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
.L821:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L868
	addq	$648, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L820:
	.cfi_restore_state
	movq	%r8, %r14
	movq	%rdx, %r13
	leaq	.LC10(%rip), %rsi
	movq	%r8, %rdx
	xorl	%edi, %edi
	movl	%ecx, %r15d
	call	ures_openDirect_67@PLT
	movl	(%r14), %r11d
	movq	%rax, -616(%rbp)
	testl	%r11d, %r11d
	jg	.L865
	testl	%r15d, %r15d
	je	.L844
	cmpl	$1, %r15d
	jne	.L869
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
.L824:
	movq	%r14, %rcx
	xorl	%edx, %edx
	call	ures_getByKey_67@PLT
	movl	(%r14), %r10d
	movq	%rax, -624(%rbp)
	testl	%r10d, %r10d
	jle	.L825
.L864:
	leaq	-576(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
.L826:
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L823
	call	ures_close_67@PLT
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L869:
	movl	$1, (%r14)
.L865:
	leaq	-576(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
.L823:
	movq	-616(%rbp), %rax
	testq	%rax, %rax
	je	.L821
	movq	%rax, %rdi
	call	ures_close_67@PLT
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L844:
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdi
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L825:
	movq	%r13, %rdi
	leaq	-320(%rbp), %r15
	movl	$0, -600(%rbp)
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	-624(%rbp), %rdi
	movq	%r14, %rcx
	movq	%rax, %rsi
	leaq	-600(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -632(%rbp)
	call	ures_getStringByKey_67@PLT
	testq	%rax, %rax
	je	.L870
.L827:
	movl	-600(%rbp), %esi
	movq	%rax, %rdi
	leal	1(%rsi), %edx
	movq	%r15, %rsi
	call	u_UCharsToChars_67@PLT
	movq	-616(%rbp), %rdi
	movq	%r14, %rcx
	xorl	%edx, %edx
	leaq	.LC17(%rip), %rsi
	call	ures_getByKey_67@PLT
	movl	(%r14), %r9d
	movq	%rax, -648(%rbp)
	testl	%r9d, %r9d
	jle	.L831
	leaq	-576(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
.L832:
	movq	-648(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L826
	call	ures_close_67@PLT
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L831:
	movq	%rax, %rdi
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	ures_getByKey_67@PLT
	movl	(%r14), %r8d
	movq	%rax, -656(%rbp)
	testl	%r8d, %r8d
	jle	.L833
	leaq	-576(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
.L834:
	movq	-656(%rbp), %rax
	testq	%rax, %rax
	je	.L832
	movq	%rax, %rdi
	call	ures_close_67@PLT
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L870:
	movl	$0, -584(%rbp)
	movq	%r13, %rdi
	leaq	-584(%rbp), %r13
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	%r15, %rdi
	movl	$157, %edx
	movq	%rax, %rsi
	call	__strcpy_chk@PLT
	movq	%r12, -640(%rbp)
	movq	%r15, %r12
	movq	-632(%rbp), %r15
	movq	%r14, -648(%rbp)
	movq	-624(%rbp), %r14
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L872:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$0, -600(%rbp)
	call	ures_getStringByKey_67@PLT
	testq	%rax, %rax
	jne	.L871
	movl	$0, -584(%rbp)
.L830:
	movq	%r13, %rcx
	movl	$157, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	uloc_getParent_67@PLT
	testl	%eax, %eax
	jg	.L872
	movq	-640(%rbp), %r12
	jmp	.L864
.L833:
	movq	%rax, %rdi
	leaq	-512(%rbp), %r15
	call	ures_getSize_67@PLT
	movl	$2, %edi
	movq	%rbx, -512(%rbp)
	movl	%eax, -660(%rbp)
	movw	%di, -504(%rbp)
	movq	$0, -592(%rbp)
	testl	%eax, %eax
	jle	.L835
	leaq	-592(%rbp), %rax
	leaq	-384(%rbp), %rsi
	movq	%r12, -688(%rbp)
	movq	%rax, -680(%rbp)
	leaq	-596(%rbp), %rax
	movq	%rsi, %r12
	leaq	-584(%rbp), %r13
	movq	%rax, -672(%rbp)
	leaq	-448(%rbp), %rax
	leaq	-512(%rbp), %r15
	movq	%r14, -640(%rbp)
	movq	%rax, %r14
	movl	$0, -632(%rbp)
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L836:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L837:
	movq	-592(%rbp), %rsi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movzwl	-376(%rbp), %eax
	testw	%ax, %ax
	js	.L838
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L839:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$58, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movw	%dx, -584(%rbp)
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	-440(%rbp), %eax
	testw	%ax, %ax
	js	.L840
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L867:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	$59, %eax
	movq	%r15, %rdi
	movw	%ax, -584(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addl	$1, -632(%rbp)
	movl	-632(%rbp), %eax
	cmpl	%eax, -660(%rbp)
	je	.L873
.L842:
	movl	$2, %ecx
	movq	-672(%rbp), %rsi
	movq	-680(%rbp), %rdx
	movq	%rbx, -448(%rbp)
	movw	%cx, -440(%rbp)
	movq	-656(%rbp), %rdi
	movq	-640(%rbp), %rcx
	movl	$0, -596(%rbp)
	call	ures_getNextString_67@PLT
	movq	-640(%rbp), %rsi
	movl	(%rsi), %esi
	testl	%esi, %esi
	jg	.L836
	movl	-596(%rbp), %ecx
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L840:
	movl	-436(%rbp), %ecx
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L838:
	movl	-372(%rbp), %ecx
	jmp	.L839
.L873:
	movq	-688(%rbp), %r12
.L835:
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-576(%rbp), %r13
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L834
.L871:
	movq	-648(%rbp), %r14
	movq	%r12, %r15
	movq	-640(%rbp), %r12
	movl	$0, (%r14)
	jmp	.L827
.L868:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3836:
	.size	_ZN6icu_6711PluralRules19getRuleFromResourceERKNS_6LocaleE11UPluralTypeR10UErrorCode, .-_ZN6icu_6711PluralRules19getRuleFromResourceERKNS_6LocaleE11UPluralTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713AndConstraintC2ERKS0_
	.type	_ZN6icu_6713AndConstraintC2ERKS0_, @function
_ZN6icu_6713AndConstraintC2ERKS0_:
.LFB3839:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713AndConstraintE(%rip), %rax
	xorl	%ecx, %ecx
	movl	$-1, 16(%rdi)
	movq	%rax, (%rdi)
	movabsq	$-4294967296, %rax
	movq	%rax, 8(%rdi)
	movl	48(%rsi), %eax
	movq	$0, 24(%rdi)
	movw	%cx, 32(%rdi)
	movl	$0, 36(%rdi)
	movq	$0, 40(%rdi)
	movl	%eax, 48(%rdi)
	testl	%eax, %eax
	jg	.L886
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	16(%rsi), %eax
	movq	%rdi, %rbx
	movq	8(%rsi), %rdx
	cmpq	$0, 24(%rsi)
	movl	%eax, 16(%rdi)
	movq	%rdx, 8(%rdi)
	je	.L877
	leaq	48(%rdi), %r14
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L878
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movl	48(%rbx), %edx
	testl	%edx, %edx
	jle	.L890
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r13, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679UVector32D0Ev@PLT
	.p2align 4,,10
	.p2align 3
.L890:
	.cfi_restore_state
	movq	%r13, 24(%rbx)
	movq	24(%r12), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector326assignERKS0_R10UErrorCode@PLT
.L877:
	movzwl	32(%r12), %eax
	cmpq	$0, 40(%r12)
	movw	%ax, 32(%rbx)
	movl	36(%r12), %eax
	movl	%eax, 36(%rbx)
	je	.L874
	movl	$56, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L882
	movq	40(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713AndConstraintC1ERKS0_
	movq	%r13, 40(%rbx)
.L874:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L886:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
.L878:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	48(%rbx), %eax
	testl	%eax, %eax
	jg	.L874
.L889:
	movl	$7, 48(%rbx)
	jmp	.L874
.L882:
	movq	$0, 40(%rbx)
	jmp	.L889
	.cfi_endproc
.LFE3839:
	.size	_ZN6icu_6713AndConstraintC2ERKS0_, .-_ZN6icu_6713AndConstraintC2ERKS0_
	.globl	_ZN6icu_6713AndConstraintC1ERKS0_
	.set	_ZN6icu_6713AndConstraintC1ERKS0_,_ZN6icu_6713AndConstraintC2ERKS0_
	.section	.text.unlikely
	.align 2
.LCOLDB18:
	.text
.LHOTB18:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713AndConstraint11isFulfilledERKNS_13IFixedDecimalE
	.type	_ZN6icu_6713AndConstraint11isFulfilledERKNS_13IFixedDecimalE, @function
_ZN6icu_6713AndConstraint11isFulfilledERKNS_13IFixedDecimalE:
.LFB3845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	36(%rdi), %eax
	testl	%eax, %eax
	je	.L909
	subl	$21, %eax
	cmpl	$4, %eax
	ja	.L893
	movq	%rsi, %rdi
	leaq	CSWTCH.347(%rip), %rdx
	movl	(%rdx,%rax,4), %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	cmpb	$0, 33(%rbx)
	movapd	%xmm0, %xmm2
	jne	.L894
	cmpl	$1, 8(%rbx)
	je	.L917
.L896:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L918
.L900:
	movl	8(%rsi), %eax
	testl	%eax, %eax
	jle	.L902
	leal	-1(%rax), %edx
	leal	-2(%rax), %ecx
	pxor	%xmm1, %xmm1
	andl	$-2, %edx
	leal	1(%rax), %r9d
	movq	%xmm1, %r10
	subl	%edx, %ecx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L904:
	movapd	%xmm1, %xmm0
	testl	%eax, %eax
	jle	.L903
	movq	24(%rsi), %rdi
	pxor	%xmm0, %xmm0
	cvtsi2sdl	(%rdi,%rdx), %xmm0
.L903:
	comisd	%xmm0, %xmm2
	jb	.L908
	movl	%r9d, %edi
	leal	-1(%rax), %r8d
	movq	%r10, %xmm0
	subl	%eax, %edi
	testl	%r8d, %r8d
	jle	.L906
	movq	24(%rsi), %r8
	movslq	%edi, %rdi
	pxor	%xmm0, %xmm0
	cvtsi2sdl	(%r8,%rdi,4), %xmm0
.L906:
	comisd	%xmm2, %xmm0
	jnb	.L912
.L908:
	subl	$2, %eax
	addq	$8, %rdx
	cmpl	%ecx, %eax
	jne	.L904
.L902:
	xorl	%eax, %eax
.L899:
	cmpb	$0, 32(%rbx)
	je	.L891
	xorl	$1, %eax
.L891:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L894:
	.cfi_restore_state
	movsd	%xmm0, -24(%rbp)
	call	uprv_floor_67@PLT
	movsd	-24(%rbp), %xmm2
	ucomisd	%xmm2, %xmm0
	jp	.L902
	jne	.L902
	cmpl	$1, 8(%rbx)
	jne	.L896
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L909:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L918:
	.cfi_restore_state
	movl	16(%rbx), %edx
	movl	$1, %eax
	cmpl	$-1, %edx
	je	.L899
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movl	$0, %edx
	ucomisd	%xmm2, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L917:
	pxor	%xmm1, %xmm1
	movapd	%xmm2, %xmm0
	cvtsi2sdl	12(%rbx), %xmm1
	call	fmod@PLT
	movq	24(%rbx), %rsi
	movapd	%xmm0, %xmm2
	testq	%rsi, %rsi
	jne	.L900
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L912:
	movl	$1, %eax
	jmp	.L899
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6713AndConstraint11isFulfilledERKNS_13IFixedDecimalE.cold, @function
_ZN6icu_6713AndConstraint11isFulfilledERKNS_13IFixedDecimalE.cold:
.LFSB3845:
.L893:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3845:
	.text
	.size	_ZN6icu_6713AndConstraint11isFulfilledERKNS_13IFixedDecimalE, .-_ZN6icu_6713AndConstraint11isFulfilledERKNS_13IFixedDecimalE
	.section	.text.unlikely
	.size	_ZN6icu_6713AndConstraint11isFulfilledERKNS_13IFixedDecimalE.cold, .-_ZN6icu_6713AndConstraint11isFulfilledERKNS_13IFixedDecimalE.cold
.LCOLDE18:
	.text
.LHOTE18:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713AndConstraint3addER10UErrorCode
	.type	_ZN6icu_6713AndConstraint3addER10UErrorCode, @function
_ZN6icu_6713AndConstraint3addER10UErrorCode:
.LFB3846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	48(%rdi), %eax
	testl	%eax, %eax
	jg	.L924
	movq	%rdi, %rbx
	movl	$56, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L922
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6713AndConstraintE(%rip), %rdx
	movabsq	$-4294967296, %rcx
	movq	$0, 48(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, (%rax)
	movq	%rcx, 8(%rax)
	movl	$-1, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 36(%rax)
	movq	$0, 44(%rax)
	movq	%rax, 40(%rbx)
.L919:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L924:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L922:
	.cfi_restore_state
	movq	$0, 40(%rbx)
	movl	$7, (%r12)
	jmp	.L919
	.cfi_endproc
.LFE3846:
	.size	_ZN6icu_6713AndConstraint3addER10UErrorCode, .-_ZN6icu_6713AndConstraint3addER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712OrConstraintC2ERKS0_
	.type	_ZN6icu_6712OrConstraintC2ERKS0_, @function
_ZN6icu_6712OrConstraintC2ERKS0_:
.LFB3851:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712OrConstraintE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	24(%rsi), %eax
	movq	$0, 16(%rdi)
	movl	%eax, 24(%rdi)
	testl	%eax, %eax
	jg	.L942
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpq	$0, 8(%rsi)
	je	.L928
	movl	$56, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L929
	movq	8(%r12), %r14
	leaq	16+_ZTVN6icu_6713AndConstraintE(%rip), %rax
	xorl	%ecx, %ecx
	movl	$-1, 16(%r13)
	movq	%rax, 0(%r13)
	movabsq	$-4294967296, %rax
	movq	%rax, 8(%r13)
	movl	48(%r14), %eax
	movq	$0, 24(%r13)
	movw	%cx, 32(%r13)
	movl	$0, 36(%r13)
	movq	$0, 40(%r13)
	movl	%eax, 48(%r13)
	testl	%eax, %eax
	jg	.L935
	movl	16(%r14), %eax
	movq	8(%r14), %rdx
	cmpq	$0, 24(%r14)
	movq	%rdx, 8(%r13)
	movl	%eax, 16(%r13)
	je	.L931
	movl	$32, %edi
	leaq	48(%r13), %r15
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L932
	movq	%r15, %rsi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movl	48(%r13), %edx
	movq	-56(%rbp), %rdi
	testl	%edx, %edx
	jle	.L946
	call	_ZN6icu_679UVector32D0Ev@PLT
	.p2align 4,,10
	.p2align 3
.L935:
	movq	%r13, 8(%rbx)
.L928:
	cmpq	$0, 16(%r12)
	je	.L925
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L938
	movq	16(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6712OrConstraintC1ERKS0_
	movl	24(%r13), %eax
	movq	%r13, 16(%rbx)
	testl	%eax, %eax
	jle	.L925
	movl	%eax, 24(%rbx)
.L925:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L942:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L946:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rdi, 24(%r13)
	movq	24(%r14), %rsi
	movq	%r15, %rdx
	call	_ZN6icu_679UVector326assignERKS0_R10UErrorCode@PLT
.L931:
	movzwl	32(%r14), %eax
	cmpq	$0, 40(%r14)
	movw	%ax, 32(%r13)
	movl	36(%r14), %eax
	movl	%eax, 36(%r13)
	je	.L935
	movl	$56, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L936
	movq	40(%r14), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713AndConstraintC1ERKS0_
	movq	%r15, 40(%r13)
	jmp	.L935
.L932:
	movl	48(%r13), %eax
	testl	%eax, %eax
	jg	.L935
.L945:
	movl	$7, 48(%r13)
	jmp	.L935
.L929:
	movq	$0, 8(%rbx)
	movl	$7, 24(%rbx)
	jmp	.L925
.L938:
	movq	$0, 16(%rbx)
	movl	$7, 24(%rbx)
	jmp	.L925
.L936:
	movq	$0, 40(%r13)
	jmp	.L945
	.cfi_endproc
.LFE3851:
	.size	_ZN6icu_6712OrConstraintC2ERKS0_, .-_ZN6icu_6712OrConstraintC2ERKS0_
	.globl	_ZN6icu_6712OrConstraintC1ERKS0_
	.set	_ZN6icu_6712OrConstraintC1ERKS0_,_ZN6icu_6712OrConstraintC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712OrConstraint3addER10UErrorCode
	.type	_ZN6icu_6712OrConstraint3addER10UErrorCode, @function
_ZN6icu_6712OrConstraint3addER10UErrorCode:
.LFB3857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jg	.L954
	.p2align 4,,10
	.p2align 3
.L948:
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L948
	movl	$56, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L950
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6713AndConstraintE(%rip), %rdx
	movabsq	$-4294967296, %rcx
	movq	$0, 48(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, (%rax)
	movq	%rcx, 8(%rax)
	movl	$-1, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 36(%rax)
	movq	$0, 44(%rax)
	movq	%rax, 8(%rbx)
.L947:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L954:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L950:
	.cfi_restore_state
	movq	$0, 8(%rbx)
	movl	$7, (%r12)
	jmp	.L947
	.cfi_endproc
.LFE3857:
	.size	_ZN6icu_6712OrConstraint3addER10UErrorCode, .-_ZN6icu_6712OrConstraint3addER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712OrConstraint11isFulfilledERKNS_13IFixedDecimalE
	.type	_ZN6icu_6712OrConstraint11isFulfilledERKNS_13IFixedDecimalE, @function
_ZN6icu_6712OrConstraint11isFulfilledERKNS_13IFixedDecimalE:
.LFB3858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	.p2align 4,,10
	.p2align 3
.L959:
	movq	8(%r13), %rbx
	testq	%rbx, %rbx
	je	.L961
	.p2align 4,,10
	.p2align 3
.L957:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713AndConstraint11isFulfilledERKNS_13IFixedDecimalE
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L962
	testb	%al, %al
	jne	.L957
.L962:
	movq	16(%r13), %r13
	testq	%r13, %r13
	je	.L955
	testb	%al, %al
	je	.L959
.L955:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L961:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3858:
	.size	_ZN6icu_6712OrConstraint11isFulfilledERKNS_13IFixedDecimalE, .-_ZN6icu_6712OrConstraint11isFulfilledERKNS_13IFixedDecimalE
	.align 2
	.p2align 4
	.globl	_ZN6icu_679RuleChainC2ERKS0_
	.type	_ZN6icu_679RuleChainC2ERKS0_, @function
_ZN6icu_679RuleChainC2ERKS0_:
.LFB3860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679RuleChainE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	addq	$8, %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	pxor	%xmm0, %xmm0
	leaq	88(%r12), %rsi
	leaq	88(%rbx), %rdi
	movups	%xmm0, 72(%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	152(%r12), %rsi
	leaq	152(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	216(%r12), %eax
	movw	%ax, 216(%rbx)
	movl	220(%r12), %eax
	movl	%eax, 220(%rbx)
	testl	%eax, %eax
	jg	.L968
	cmpq	$0, 80(%r12)
	je	.L975
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L972
	movq	80(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6712OrConstraintC1ERKS0_
	movl	24(%r13), %eax
	movq	%r13, 80(%rbx)
	testl	%eax, %eax
	jg	.L977
.L975:
	cmpq	$0, 72(%r12)
	je	.L968
	movl	$224, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L974
	movq	72(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679RuleChainC1ERKS0_
	movl	220(%r13), %eax
	movq	%r13, 72(%rbx)
	testl	%eax, %eax
	jle	.L968
.L977:
	movl	%eax, 220(%rbx)
.L968:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L972:
	.cfi_restore_state
	movq	$0, 80(%rbx)
	movl	$7, 220(%rbx)
	jmp	.L975
.L974:
	movq	$0, 72(%rbx)
	movl	$7, 220(%rbx)
	jmp	.L968
	.cfi_endproc
.LFE3860:
	.size	_ZN6icu_679RuleChainC2ERKS0_, .-_ZN6icu_679RuleChainC2ERKS0_
	.globl	_ZN6icu_679RuleChainC1ERKS0_
	.set	_ZN6icu_679RuleChainC1ERKS0_,_ZN6icu_679RuleChainC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRulesaSERKS0_
	.type	_ZN6icu_6711PluralRulesaSERKS0_, @function
_ZN6icu_6711PluralRulesaSERKS0_:
.LFB3804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L986
	movq	8(%rdi), %r13
	movq	%rsi, %rbx
	testq	%r13, %r13
	je	.L981
	movq	0(%r13), %rax
	leaq	_ZN6icu_679RuleChainD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L982
	call	_ZN6icu_679RuleChainD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L981:
	movq	$0, 8(%r12)
	movl	16(%rbx), %eax
	movl	%eax, 16(%r12)
	testl	%eax, %eax
	jg	.L986
	cmpq	$0, 8(%rbx)
	je	.L986
	movl	$224, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L984
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679RuleChainC1ERKS0_
	movl	220(%r13), %eax
	movq	%r13, 8(%r12)
	testl	%eax, %eax
	jle	.L986
	movl	%eax, 16(%r12)
.L986:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L982:
	.cfi_restore_state
	call	*%rax
	jmp	.L981
.L984:
	movq	$0, 8(%r12)
	movl	$7, 16(%r12)
	jmp	.L986
	.cfi_endproc
.LFE3804:
	.size	_ZN6icu_6711PluralRulesaSERKS0_, .-_ZN6icu_6711PluralRulesaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRulesC2ERKS0_
	.type	_ZN6icu_6711PluralRulesC2ERKS0_, @function
_ZN6icu_6711PluralRulesC2ERKS0_:
.LFB3793:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711PluralRulesE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$0, 16(%rdi)
	cmpq	%rsi, %rdi
	je	.L996
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rsi), %eax
	movl	%eax, 16(%rdi)
	testl	%eax, %eax
	jg	.L990
	cmpq	$0, 8(%rsi)
	je	.L990
	movl	$224, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L994
	movq	8(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679RuleChainC1ERKS0_
	movl	220(%r13), %eax
	movq	%r13, 8(%rbx)
	testl	%eax, %eax
	jle	.L990
	movl	%eax, 16(%rbx)
.L990:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L996:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
.L994:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	$0, 8(%rbx)
	movl	$7, 16(%rbx)
	jmp	.L990
	.cfi_endproc
.LFE3793:
	.size	_ZN6icu_6711PluralRulesC2ERKS0_, .-_ZN6icu_6711PluralRulesC2ERKS0_
	.globl	_ZN6icu_6711PluralRulesC1ERKS0_
	.set	_ZN6icu_6711PluralRulesC1ERKS0_,_ZN6icu_6711PluralRulesC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PluralRules5cloneEv
	.type	_ZNK6icu_6711PluralRules5cloneEv, @function
_ZNK6icu_6711PluralRules5cloneEv:
.LFB3803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L999
	leaq	16+_ZTVN6icu_6711PluralRulesE(%rip), %r13
	movq	$0, 8(%rax)
	movq	%r13, (%rax)
	movl	$0, 16(%rax)
	cmpq	%rbx, %rax
	je	.L999
	movl	16(%rbx), %eax
	movl	%eax, 16(%r12)
	testl	%eax, %eax
	jg	.L1001
	cmpq	$0, 8(%rbx)
	je	.L999
	movl	$224, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1002
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679RuleChainC1ERKS0_
	movl	220(%r14), %eax
	movq	%r14, 8(%r12)
	testl	%eax, %eax
	jle	.L1018
	movl	%eax, 16(%r12)
	movq	(%r12), %rax
	movq	8(%rax), %rax
.L1009:
	leaq	_ZN6icu_6711PluralRulesD0Ev(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L1001
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*%rax
.L999:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1001:
	.cfi_restore_state
	movq	%r13, (%r12)
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1007
	movq	0(%r13), %rax
	leaq	_ZN6icu_679RuleChainD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1008
	call	_ZN6icu_679RuleChainD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1007:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1018:
	.cfi_restore_state
	movl	16(%r12), %eax
	testl	%eax, %eax
	jle	.L999
	movq	(%r12), %rax
	movq	8(%rax), %rax
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1008:
	call	*%rax
	jmp	.L1007
.L1002:
	movq	$0, 8(%r12)
	movq	(%r12), %rax
	movl	$7, 16(%r12)
	movq	8(%rax), %rax
	jmp	.L1009
	.cfi_endproc
.LFE3803:
	.size	_ZNK6icu_6711PluralRules5cloneEv, .-_ZNK6icu_6711PluralRules5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679RuleChain6selectERKNS_13IFixedDecimalE
	.type	_ZNK6icu_679RuleChain6selectERKNS_13IFixedDecimalE, @function
_ZNK6icu_679RuleChain6selectERKNS_13IFixedDecimalE:
.LFB3866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	call	*24(%rax)
	testb	%al, %al
	jne	.L1020
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*32(%rax)
	testb	%al, %al
	jne	.L1020
	.p2align 4,,10
	.p2align 3
.L1028:
	movq	80(%r14), %r12
	testq	%r12, %r12
	je	.L1021
	.p2align 4,,10
	.p2align 3
.L1025:
	movq	8(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1022
	.p2align 4,,10
	.p2align 3
.L1023:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713AndConstraint11isFulfilledERKNS_13IFixedDecimalE
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1030
	testb	%al, %al
	jne	.L1023
.L1030:
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L1031
	testb	%al, %al
	je	.L1025
	.p2align 4,,10
	.p2align 3
.L1022:
	leaq	8(%r14), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1031:
	testb	%al, %al
	jne	.L1022
.L1021:
	movq	72(%r14), %r14
	testq	%r14, %r14
	jne	.L1028
.L1020:
	leaq	_ZN6icu_67L20PLURAL_KEYWORD_OTHERE(%rip), %rax
	leaq	-64(%rbp), %rdx
	movl	$5, %ecx
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
.L1019:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1047
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1047:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3866:
	.size	_ZNK6icu_679RuleChain6selectERKNS_13IFixedDecimalE, .-_ZNK6icu_679RuleChain6selectERKNS_13IFixedDecimalE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE
	.type	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE, @function
_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE:
.LFB3816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1053
	call	_ZNK6icu_679RuleChain6selectERKNS_13IFixedDecimalE
.L1048:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1054
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1053:
	.cfi_restore_state
	leaq	_ZN6icu_67L19PLURAL_DEFAULT_RULEE(%rip), %rax
	leaq	-32(%rbp), %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	jmp	.L1048
.L1054:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3816:
	.size	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE, .-_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC19:
	.string	" "
	.string	"i"
	.string	"s"
	.string	" "
	.string	""
	.string	""
	.align 2
.LC20:
	.string	"n"
	.string	"o"
	.string	"t"
	.string	" "
	.string	""
	.string	""
	.align 2
.LC21:
	.string	"m"
	.string	"o"
	.string	"d"
	.string	" "
	.string	""
	.string	""
	.align 2
.LC22:
	.string	" "
	.string	"i"
	.string	"s"
	.string	" "
	.string	"n"
	.string	"o"
	.string	"t"
	.string	" "
	.string	""
	.string	""
	.align 2
.LC23:
	.string	" "
	.string	"n"
	.string	"o"
	.string	"t"
	.string	" "
	.string	"i"
	.string	"n"
	.string	" "
	.string	""
	.string	""
	.align 2
.LC24:
	.string	" "
	.string	"n"
	.string	"o"
	.string	"t"
	.string	" "
	.string	"w"
	.string	"i"
	.string	"t"
	.string	"h"
	.string	"i"
	.string	"n"
	.string	" "
	.string	""
	.string	""
	.align 2
.LC25:
	.string	" "
	.string	"i"
	.string	"n"
	.string	" "
	.string	""
	.string	""
	.align 2
.LC26:
	.string	" "
	.string	"w"
	.string	"i"
	.string	"t"
	.string	"h"
	.string	"i"
	.string	"n"
	.string	" "
	.string	""
	.string	""
	.align 2
.LC27:
	.string	"."
	.string	"."
	.string	""
	.string	""
	.align 2
.LC28:
	.string	","
	.string	" "
	.string	""
	.string	""
	.align 2
.LC29:
	.string	" "
	.string	"a"
	.string	"n"
	.string	"d"
	.string	" "
	.string	""
	.string	""
	.align 2
.LC30:
	.string	" "
	.string	"o"
	.string	"r"
	.string	" "
	.string	""
	.string	""
	.align 2
.LC31:
	.string	";"
	.string	" "
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_679RuleChain9dumpRulesERNS_13UnicodeStringE
	.type	_ZN6icu_679RuleChain9dumpRulesERNS_13UnicodeStringE, @function
_ZN6icu_679RuleChain9dumpRulesERNS_13UnicodeStringE:
.LFB3868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 80(%rdi)
	je	.L1056
	movzwl	16(%rdi), %eax
	leaq	8(%rdi), %rsi
	testw	%ax, %ax
	js	.L1057
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1058:
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r15, %r14
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$58, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	leaq	-168(%rbp), %rax
	movw	%cx, -168(%rbp)
	movl	$1, %ecx
	movq	%rax, %rsi
	movq	%rax, %rbx
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$32, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movw	%si, -168(%rbp)
	movl	$1, %ecx
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-216(%rbp), %rax
	movq	80(%rax), %rax
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	testq	%rax, %rax
	je	.L1143
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	8(%rax), %r15
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	-184(%rbp), %rdx
	leaq	-128(%rbp), %r12
	movl	$-1, %ecx
	leaq	.LC29(%rip), %rax
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L1112
	sarl	$5, %ecx
.L1113:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1114:
	testq	%r15, %r15
	je	.L1059
	movl	8(%r15), %edx
	testl	%edx, %edx
	jne	.L1060
	cmpq	$0, 24(%r15)
	je	.L1160
.L1060:
	movl	36(%r15), %esi
	leaq	-128(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN6icu_67L11tokenStringENS_9tokenTypeE
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L1071
	sarl	$5, %ecx
.L1072:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$32, %eax
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	-184(%rbp), %rsi
	movl	$1, %ecx
	movw	%ax, -168(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	$1, 8(%r15)
	je	.L1161
.L1073:
	cmpq	$0, 24(%r15)
	movzbl	32(%r15), %eax
	jne	.L1078
	testb	%al, %al
	je	.L1079
	leaq	.LC22(%rip), %rax
.L1158:
	movq	-184(%rbp), %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L1084
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1085:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1151:
	movl	16(%r15), %edx
	leaq	-160(%rbp), %r13
	xorl	%r8d, %r8d
	movl	$10, %ecx
	movl	$16, %esi
	movq	%r13, %rdi
	call	uprv_itou_67@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L1086
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1087:
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1062:
	movq	40(%r15), %r15
	testq	%r15, %r15
	jne	.L1162
.L1059:
	movq	-208(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L1143
	movq	-184(%rbp), %rdx
	leaq	-128(%rbp), %r12
	movl	$-1, %ecx
	leaq	.LC30(%rip), %rax
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L1115
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1116:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-208(%rbp), %rax
	testq	%rax, %rax
	jne	.L1163
	.p2align 4,,10
	.p2align 3
.L1143:
	movq	%r14, %r15
.L1056:
	movq	-216(%rbp), %rax
	cmpq	$0, 72(%rax)
	je	.L1055
	leaq	-128(%rbp), %r12
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	.LC31(%rip), %rax
	leaq	-168(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L1119
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1120:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-216(%rbp), %rax
	movq	%r15, %rsi
	movq	72(%rax), %rdi
	call	_ZN6icu_679RuleChain9dumpRulesERNS_13UnicodeStringE
.L1055:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1164
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1057:
	.cfi_restore_state
	movl	20(%rdi), %ecx
	jmp	.L1058
.L1119:
	movl	-116(%rbp), %ecx
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1078:
	movzbl	33(%r15), %edx
	testb	%al, %al
	je	.L1088
	leaq	.LC23(%rip), %rax
	testb	%dl, %dl
	jne	.L1153
	leaq	.LC24(%rip), %rax
.L1153:
	movq	-184(%rbp), %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L1093
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1094:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	24(%r15), %rdx
	movl	8(%rdx), %eax
	testl	%eax, %eax
	jle	.L1062
	movq	$0, -200(%rbp)
	xorl	%ebx, %ebx
	leaq	-160(%rbp), %r13
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1166:
	movl	%eax, %ecx
	xorl	%r9d, %r9d
	subl	%ebx, %ecx
	testl	%ecx, %ecx
	jle	.L1103
	movq	24(%rdx), %rcx
	movq	-200(%rbp), %rsi
	movl	(%rcx,%rsi), %r9d
.L1103:
	leal	1(%rbx), %ecx
	subl	%ecx, %eax
	testl	%eax, %eax
	jle	.L1165
	movq	24(%rdx), %rax
	movl	(%rax,%rcx,4), %eax
	movl	%eax, -188(%rbp)
.L1102:
	movl	$10, %ecx
	xorl	%r8d, %r8d
	movl	%r9d, %edx
	movl	$16, %esi
	movq	%r13, %rdi
	call	uprv_itou_67@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L1104
	sarl	$5, %ecx
.L1105:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-184(%rbp), %rdx
	movl	$-1, %ecx
	movq	%r12, %rdi
	leaq	.LC27(%rip), %rax
	movl	$1, %esi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L1106
	sarl	$5, %ecx
.L1107:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$10, %ecx
	xorl	%r8d, %r8d
	movl	$16, %esi
	movq	%r13, %rdi
	movl	-188(%rbp), %edx
	call	uprv_itou_67@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L1108
	sarl	$5, %ecx
.L1109:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	addl	$2, %ebx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	24(%r15), %rax
	cmpl	8(%rax), %ebx
	jge	.L1062
	movq	-184(%rbp), %rdx
	leaq	.LC28(%rip), %rax
	movl	$-1, %ecx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L1110
	sarl	$5, %ecx
.L1145:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	24(%r15), %rdx
	movl	8(%rdx), %eax
	addq	$8, -200(%rbp)
	cmpl	%eax, %ebx
	jge	.L1062
.L1095:
	testl	%eax, %eax
	jg	.L1166
	movl	$0, -188(%rbp)
	xorl	%r9d, %r9d
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1110:
	movl	-116(%rbp), %ecx
	jmp	.L1145
	.p2align 4,,10
	.p2align 3
.L1108:
	movl	-116(%rbp), %ecx
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1106:
	movl	-116(%rbp), %ecx
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1104:
	movl	-116(%rbp), %ecx
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1165:
	movl	$0, -188(%rbp)
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1071:
	movl	-116(%rbp), %ecx
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1086:
	movl	-116(%rbp), %ecx
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1079:
	leaq	.LC19(%rip), %rax
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1112:
	movl	-116(%rbp), %ecx
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1084:
	movl	-116(%rbp), %ecx
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1088:
	leaq	.LC25(%rip), %rax
	testb	%dl, %dl
	jne	.L1153
	leaq	.LC26(%rip), %rax
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	-184(%rbp), %rdx
	leaq	.LC21(%rip), %rax
	movl	$-1, %ecx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L1074
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1075:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	12(%r15), %edx
	leaq	-160(%rbp), %r13
	xorl	%r8d, %r8d
	movl	$10, %ecx
	movl	$16, %esi
	movq	%r13, %rdi
	call	uprv_itou_67@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L1076
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1077:
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1093:
	movl	-116(%rbp), %ecx
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1160:
	cmpl	$-1, 16(%r15)
	je	.L1062
	leaq	-128(%rbp), %r12
	movl	36(%r15), %esi
	movq	%r12, %rdi
	call	_ZN6icu_67L11tokenStringENS_9tokenTypeE
	movswl	-120(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	cmovs	-116(%rbp), %ecx
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-184(%rbp), %rdx
	movl	$-1, %ecx
	movq	%r12, %rdi
	leaq	.LC19(%rip), %rax
	movl	$1, %esi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testw	%ax, %ax
	jns	.L1065
	movl	-116(%rbp), %ecx
.L1065:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 32(%r15)
	leaq	.LC20(%rip), %rax
	je	.L1151
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1076:
	movl	-116(%rbp), %ecx
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1074:
	movl	-116(%rbp), %ecx
	jmp	.L1075
.L1115:
	movl	-116(%rbp), %ecx
	jmp	.L1116
.L1164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3868:
	.size	_ZN6icu_679RuleChain9dumpRulesERNS_13UnicodeStringE, .-_ZN6icu_679RuleChain9dumpRulesERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PluralRules8getRulesEv
	.type	_ZNK6icu_6711PluralRules8getRulesEv, @function
_ZNK6icu_6711PluralRules8getRulesEv:
.LFB3837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movq	8(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L1167
	movq	%r12, %rsi
	call	_ZN6icu_679RuleChain9dumpRulesERNS_13UnicodeStringE
.L1167:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3837:
	.size	_ZNK6icu_6711PluralRules8getRulesEv, .-_ZNK6icu_6711PluralRules8getRulesEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679RuleChain11getKeywordsEiPNS_13UnicodeStringERi
	.type	_ZNK6icu_679RuleChain11getKeywordsEiPNS_13UnicodeStringERi, @function
_ZNK6icu_679RuleChain11getKeywordsEiPNS_13UnicodeStringERi:
.LFB3869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leal	-1(%rsi), %ebx
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1180:
	movslq	(%r14), %rdi
	cmpl	%ebx, %edi
	jge	.L1176
	leal	1(%rdi), %eax
	salq	$6, %rdi
	leaq	8(%r13), %rsi
	movl	%eax, (%r14)
	addq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	72(%r13), %r13
	testq	%r13, %r13
	je	.L1179
.L1175:
	movl	220(%r13), %eax
	testl	%eax, %eax
	jle	.L1180
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1176:
	.cfi_restore_state
	popq	%rbx
	movl	$15, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1179:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3869:
	.size	_ZNK6icu_679RuleChain11getKeywordsEiPNS_13UnicodeStringERi, .-_ZNK6icu_679RuleChain11getKeywordsEiPNS_13UnicodeStringERi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679RuleChain9isKeywordERKNS_13UnicodeStringE
	.type	_ZNK6icu_679RuleChain9isKeywordERKNS_13UnicodeStringE, @function
_ZNK6icu_679RuleChain9isKeywordERKNS_13UnicodeStringE:
.LFB3870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1200:
	testw	%dx, %dx
	js	.L1183
	sarl	$5, %edx
.L1184:
	testw	%ax, %ax
	js	.L1185
	sarl	$5, %eax
.L1186:
	cmpl	%edx, %eax
	jne	.L1187
	testb	%r8b, %r8b
	je	.L1198
.L1187:
	movq	72(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1199
.L1189:
	movswl	8(%r12), %eax
	movswl	16(%rbx), %edx
	movl	%eax, %r8d
	andl	$1, %r8d
	testb	$1, %dl
	je	.L1200
	testb	%r8b, %r8b
	je	.L1187
.L1181:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1185:
	.cfi_restore_state
	movl	12(%r12), %eax
	jmp	.L1186
	.p2align 4,,10
	.p2align 3
.L1183:
	movl	20(%rbx), %edx
	jmp	.L1184
	.p2align 4,,10
	.p2align 3
.L1199:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1198:
	.cfi_restore_state
	leaq	8(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r8b
	testb	%r8b, %r8b
	je	.L1187
	jmp	.L1181
	.cfi_endproc
.LFE3870:
	.size	_ZNK6icu_679RuleChain9isKeywordERKNS_13UnicodeStringE, .-_ZNK6icu_679RuleChain9isKeywordERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716PluralRuleParserC2Ev
	.type	_ZN6icu_6716PluralRuleParserC2Ev, @function
_ZN6icu_6716PluralRuleParserC2Ev:
.LFB3872:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6716PluralRuleParserE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$0, 16(%rdi)
	movq	%rax, (%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 24(%rdi)
	movl	$2, %eax
	movw	%ax, 32(%rdi)
	movq	$0, 88(%rdi)
	movq	$-1, 112(%rdi)
	movups	%xmm0, 96(%rdi)
	ret
	.cfi_endproc
.LFE3872:
	.size	_ZN6icu_6716PluralRuleParserC2Ev, .-_ZN6icu_6716PluralRuleParserC2Ev
	.globl	_ZN6icu_6716PluralRuleParserC1Ev
	.set	_ZN6icu_6716PluralRuleParserC1Ev,_ZN6icu_6716PluralRuleParserC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716PluralRuleParser14getNumberValueERKNS_13UnicodeStringE
	.type	_ZN6icu_6716PluralRuleParser14getNumberValueERKNS_13UnicodeStringE, @function
_ZN6icu_6716PluralRuleParser14getNumberValueERKNS_13UnicodeStringE:
.LFB3878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$152, %rsp
	.cfi_offset 12, -24
	movswl	8(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testw	%dx, %dx
	js	.L1203
	sarl	$5, %edx
.L1204:
	leaq	-160(%rbp), %r12
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movl	$128, %r8d
	movq	%r12, %rcx
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	cltq
	movb	$0, -160(%rbp,%rax)
	call	strtol@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1207
	addq	$152, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1203:
	.cfi_restore_state
	movl	12(%rdi), %edx
	jmp	.L1204
.L1207:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3878:
	.size	_ZN6icu_6716PluralRuleParser14getNumberValueERKNS_13UnicodeStringE, .-_ZN6icu_6716PluralRuleParser14getNumberValueERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716PluralRuleParser12getNextTokenER10UErrorCode
	.type	_ZN6icu_6716PluralRuleParser12getNextTokenER10UErrorCode, @function
_ZN6icu_6716PluralRuleParser12getNextTokenER10UErrorCode:
.LFB3880:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1208
	jmp	_ZN6icu_6716PluralRuleParser12getNextTokenER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1208:
	ret
	.cfi_endproc
.LFE3880:
	.size	_ZN6icu_6716PluralRuleParser12getNextTokenER10UErrorCode, .-_ZN6icu_6716PluralRuleParser12getNextTokenER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716PluralRuleParser8charTypeEDs
	.type	_ZN6icu_6716PluralRuleParser8charTypeEDs, @function
_ZN6icu_6716PluralRuleParser8charTypeEDs:
.LFB3881:
	.cfi_startproc
	endbr64
	leal	-48(%rdi), %edx
	movl	$1, %eax
	cmpw	$9, %dx
	jbe	.L1210
	leal	-97(%rdi), %edx
	movl	$10, %eax
	cmpw	$25, %dx
	jbe	.L1210
	cmpw	$64, %di
	ja	.L1212
	xorl	%eax, %eax
	cmpw	$31, %di
	jbe	.L1210
	subl	$32, %edi
	cmpw	$32, %di
	ja	.L1213
	leaq	.L1215(%rip), %rdx
	movzwl	%di, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1215:
	.long	.L1222-.L1215
	.long	.L1221-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1220-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1219-.L1215
	.long	.L1213-.L1215
	.long	.L1218-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1226-.L1215
	.long	.L1217-.L1215
	.long	.L1213-.L1215
	.long	.L1216-.L1215
	.long	.L1213-.L1215
	.long	.L1213-.L1215
	.long	.L1214-.L1215
	.text
.L1226:
	movl	$5, %eax
.L1210:
	ret
.L1222:
	movl	$4, %eax
	ret
.L1213:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1212:
	movl	$18, %eax
	cmpw	$126, %di
	je	.L1210
	xorl	%eax, %eax
	cmpw	$8230, %di
	sete	%al
	leal	(%rax,%rax,8), %eax
	ret
.L1214:
	movl	$6, %eax
	ret
.L1216:
	movl	$16, %eax
	ret
.L1217:
	movl	$3, %eax
	ret
.L1218:
	movl	$7, %eax
	ret
.L1219:
	movl	$2, %eax
	ret
.L1220:
	movl	$13, %eax
	ret
.L1221:
	movl	$17, %eax
	ret
	.cfi_endproc
.LFE3881:
	.size	_ZN6icu_6716PluralRuleParser8charTypeEDs, .-_ZN6icu_6716PluralRuleParser8charTypeEDs
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716PluralRuleParser10getKeyTypeERKNS_13UnicodeStringENS_9tokenTypeE
	.type	_ZN6icu_6716PluralRuleParser10getKeyTypeERKNS_13UnicodeStringENS_9tokenTypeE, @function
_ZN6icu_6716PluralRuleParser10getKeyTypeERKNS_13UnicodeStringENS_9tokenTypeE:
.LFB3882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$10, %esi
	je	.L1262
.L1230:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1263
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1262:
	.cfi_restore_state
	movswl	8(%rdi), %edx
	movq	%rdi, %r13
	testw	%dx, %dx
	js	.L1231
	sarl	$5, %edx
.L1232:
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	_ZN6icu_67L8PK_VAR_NE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L8PK_VAR_NE(%rip), %rdx
	testb	%al, %al
	je	.L1248
	movswl	8(%r13), %edx
	testw	%dx, %dx
	js	.L1233
	sarl	$5, %edx
.L1234:
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	_ZN6icu_67L8PK_VAR_IE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L8PK_VAR_IE(%rip), %rdx
	testb	%al, %al
	je	.L1249
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L1235
	movswl	%ax, %edx
	sarl	$5, %edx
.L1236:
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	_ZN6icu_67L8PK_VAR_FE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L8PK_VAR_FE(%rip), %rdx
	testb	%al, %al
	je	.L1250
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L1237
	movswl	%ax, %edx
	sarl	$5, %edx
.L1238:
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	_ZN6icu_67L8PK_VAR_TE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L8PK_VAR_TE(%rip), %rdx
	testb	%al, %al
	je	.L1251
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L1239
	movswl	%ax, %edx
	sarl	$5, %edx
.L1240:
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	_ZN6icu_67L8PK_VAR_VE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L8PK_VAR_VE(%rip), %rdx
	testb	%al, %al
	je	.L1252
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L1241
	movswl	%ax, %edx
	sarl	$5, %edx
.L1242:
	movl	$2, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	_ZN6icu_67L5PK_ISE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L5PK_ISE(%rip), %rdx
	testb	%al, %al
	je	.L1253
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L1243
	movswl	%ax, %edx
	sarl	$5, %edx
.L1244:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	_ZN6icu_67L6PK_ANDE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L6PK_ANDE(%rip), %rdx
	testb	%al, %al
	je	.L1254
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L1245
	sarl	$5, %eax
	movl	%eax, %edx
.L1246:
	movl	$2, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	_ZN6icu_67L5PK_INE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L5PK_INE(%rip), %rdx
	testb	%al, %al
	je	.L1255
	leaq	-48(%rbp), %r14
	leaq	_ZN6icu_67L9PK_WITHINE(%rip), %rax
	movl	$6, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rax, -48(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	je	.L1256
	leaq	_ZN6icu_67L6PK_NOTE(%rip), %rax
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	je	.L1257
	leaq	_ZN6icu_67L6PK_MODE(%rip), %rax
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	je	.L1258
	leaq	_ZN6icu_67L5PK_ORE(%rip), %rax
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	je	.L1259
	leaq	_ZN6icu_67L10PK_DECIMALE(%rip), %rax
	movl	$7, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	je	.L1260
	leaq	_ZN6icu_67L10PK_INTEGERE(%rip), %rax
	movl	$7, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK6icu_6713UnicodeString7compareENS_14ConstChar16PtrEi
	testb	%al, %al
	movl	$27, %eax
	cmove	%eax, %r12d
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1231:
	movl	12(%rdi), %edx
	jmp	.L1232
	.p2align 4,,10
	.p2align 3
.L1248:
	movl	$21, %r12d
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1249:
	movl	$22, %r12d
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1233:
	movl	12(%r13), %edx
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1235:
	movl	12(%r13), %edx
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1250:
	movl	$23, %r12d
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1251:
	movl	$25, %r12d
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1237:
	movl	12(%r13), %edx
	jmp	.L1238
.L1252:
	movl	$24, %r12d
	jmp	.L1230
.L1239:
	movl	12(%r13), %edx
	jmp	.L1240
.L1253:
	movl	$20, %r12d
	jmp	.L1230
.L1241:
	movl	12(%r13), %edx
	jmp	.L1242
.L1254:
	movl	$11, %r12d
	jmp	.L1230
.L1243:
	movl	12(%r13), %edx
	jmp	.L1244
.L1255:
	movl	$15, %r12d
	jmp	.L1230
.L1245:
	movl	12(%r13), %edx
	jmp	.L1246
.L1256:
	movl	$19, %r12d
	jmp	.L1230
.L1263:
	call	__stack_chk_fail@PLT
.L1257:
	movl	$14, %r12d
	jmp	.L1230
.L1258:
	movl	$13, %r12d
	jmp	.L1230
.L1259:
	movl	$12, %r12d
	jmp	.L1230
.L1260:
	movl	$26, %r12d
	jmp	.L1230
	.cfi_endproc
.LFE3882:
	.size	_ZN6icu_6716PluralRuleParser10getKeyTypeERKNS_13UnicodeStringENS_9tokenTypeE, .-_ZN6icu_6716PluralRuleParser10getKeyTypeERKNS_13UnicodeStringENS_9tokenTypeE
	.align 2
	.p2align 4
	.type	_ZN6icu_6716PluralRuleParser5parseERKNS_13UnicodeStringEPNS_11PluralRulesER10UErrorCode.part.0, @function
_ZN6icu_6716PluralRuleParser5parseERKNS_13UnicodeStringEPNS_11PluralRulesER10UErrorCode.part.0:
.LFB5391:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	.L1271(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	%rdx, -200(%rbp)
	movl	16(%rbx), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_679RuleChainE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, %xmm1
	movswl	8(%rsi), %eax
	movhps	.LC32(%rip), %xmm1
	movaps	%xmm1, -224(%rbp)
	testw	%ax, %ax
	js	.L1265
	.p2align 4,,10
	.p2align 3
.L1453:
	sarl	$5, %eax
.L1266:
	cmpl	%eax, %edx
	jge	.L1264
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L1264
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6716PluralRuleParser12getNextTokenER10UErrorCode.part.0
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L1264
	movl	92(%rbx), %eax
	testl	%eax, %eax
	je	.L1268
	cmpl	$3, %eax
	je	.L1268
	movl	88(%rbx), %esi
	leaq	24(%rbx), %rdi
	call	_ZN6icu_6716PluralRuleParser10getKeyTypeERKNS_13UnicodeStringENS_9tokenTypeE
	movl	%eax, 88(%rbx)
	movl	92(%rbx), %eax
.L1268:
	cmpl	$25, %eax
	ja	.L1269
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1271:
	.long	.L1279-.L1271
	.long	.L1280-.L1271
	.long	.L1273-.L1271
	.long	.L1279-.L1271
	.long	.L1269-.L1271
	.long	.L1278-.L1271
	.long	.L1277-.L1271
	.long	.L1269-.L1271
	.long	.L1273-.L1271
	.long	.L1269-.L1271
	.long	.L1276-.L1271
	.long	.L1275-.L1271
	.long	.L1275-.L1271
	.long	.L1273-.L1271
	.long	.L1274-.L1271
	.long	.L1273-.L1271
	.long	.L1273-.L1271
	.long	.L1273-.L1271
	.long	.L1269-.L1271
	.long	.L1273-.L1271
	.long	.L1272-.L1271
	.long	.L1270-.L1271
	.long	.L1270-.L1271
	.long	.L1270-.L1271
	.long	.L1270-.L1271
	.long	.L1270-.L1271
	.text
	.p2align 4,,10
	.p2align 3
.L1273:
	cmpl	$1, 88(%rbx)
	je	.L1451
	.p2align 4,,10
	.p2align 3
.L1269:
	movl	$65792, (%r12)
.L1264:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1452
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1270:
	.cfi_restore_state
	movl	88(%rbx), %eax
	leal	-13(%rax), %edx
	cmpl	$4, %edx
	jbe	.L1281
	leal	-19(%rax), %edx
	cmpl	$1, %edx
	ja	.L1269
	.p2align 4,,10
	.p2align 3
.L1281:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L1264
	cmpl	$27, %eax
	ja	.L1289
	leaq	.L1291(%rip), %rsi
	movl	%eax, %ecx
	movslq	(%rsi,%rcx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L1291:
	.long	.L1289-.L1291
	.long	.L1302-.L1291
	.long	.L1301-.L1291
	.long	.L1289-.L1291
	.long	.L1289-.L1291
	.long	.L1289-.L1291
	.long	.L1289-.L1291
	.long	.L1289-.L1291
	.long	.L1289-.L1291
	.long	.L1289-.L1291
	.long	.L1300-.L1291
	.long	.L1299-.L1291
	.long	.L1298-.L1291
	.long	.L1297-.L1291
	.long	.L1296-.L1291
	.long	.L1294-.L1291
	.long	.L1294-.L1291
	.long	.L1295-.L1291
	.long	.L1289-.L1291
	.long	.L1294-.L1291
	.long	.L1289-.L1291
	.long	.L1293-.L1291
	.long	.L1293-.L1291
	.long	.L1293-.L1291
	.long	.L1293-.L1291
	.long	.L1293-.L1291
	.long	.L1415-.L1291
	.long	.L1416-.L1291
	.text
	.p2align 4,,10
	.p2align 3
.L1279:
	movl	88(%rbx), %eax
	cmpl	$10, %eax
	je	.L1281
	cmpl	$28, %eax
	jne	.L1269
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1275:
	movl	88(%rbx), %eax
	leal	-21(%rax), %edx
	cmpl	$4, %edx
	jbe	.L1281
	jmp	.L1269
.L1416:
	leaq	24(%rbx), %r14
.L1290:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6716PluralRuleParser12getNextTokenER10UErrorCode.part.0
	movl	(%r12), %r9d
	movl	88(%rbx), %eax
	testl	%r9d, %r9d
	jg	.L1449
	cmpl	$28, %eax
	jbe	.L1362
	movq	104(%rbx), %rdi
.L1363:
	movswl	32(%rbx), %ecx
	addq	$152, %rdi
	testw	%cx, %cx
	js	.L1365
	sarl	$5, %ecx
.L1366:
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
.L1364:
	movl	(%r12), %r10d
	testl	%r10d, %r10d
	jle	.L1290
.L1450:
	movl	88(%rbx), %eax
.L1449:
	movl	%eax, 92(%rbx)
	jmp	.L1264
.L1415:
	leaq	24(%rbx), %r14
.L1292:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6716PluralRuleParser12getNextTokenER10UErrorCode.part.0
	movl	(%r12), %edi
	movl	88(%rbx), %eax
	testl	%edi, %edi
	jg	.L1449
	cmpl	$28, %eax
	jbe	.L1368
	movq	104(%rbx), %rdi
.L1369:
	movswl	32(%rbx), %ecx
	addq	$88, %rdi
	testw	%cx, %cx
	js	.L1371
	sarl	$5, %ecx
.L1372:
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
.L1370:
	movl	(%r12), %r8d
	testl	%r8d, %r8d
	jle	.L1292
	jmp	.L1450
	.p2align 4,,10
	.p2align 3
.L1276:
	movl	88(%rbx), %eax
	cmpl	$5, %eax
	jne	.L1269
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L1264
.L1289:
	movl	%eax, 92(%rbx)
.L1380:
	movq	8(%rbx), %rsi
	movl	16(%rbx), %edx
	movswl	8(%rsi), %eax
	testw	%ax, %ax
	jns	.L1453
.L1265:
	movl	12(%rsi), %eax
	jmp	.L1266
.L1295:
	movq	96(%rbx), %rax
	movb	$1, 32(%rax)
.L1294:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1310
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L1454
	movq	%r14, %rdi
	call	_ZN6icu_679UVector32D0Ev@PLT
	movl	88(%rbx), %eax
	movl	(%r12), %edx
	.p2align 4,,10
	.p2align 3
.L1306:
	movl	%eax, 92(%rbx)
	testl	%edx, %edx
	jle	.L1380
	jmp	.L1264
.L1296:
	movq	96(%rbx), %rdx
	movb	$1, 32(%rdx)
	movl	(%r12), %edx
	jmp	.L1306
.L1293:
	movq	96(%rbx), %rdx
	movl	%eax, 36(%rdx)
	jmp	.L1289
.L1297:
	movq	96(%rbx), %rdx
	movl	$1, 8(%rdx)
	jmp	.L1289
.L1298:
	movq	104(%rbx), %rax
	movq	80(%rax), %rax
	.p2align 4,,10
	.p2align 3
.L1307:
	movq	%rax, %r14
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1307
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1308
	movq	$0, 8(%rax)
	movl	$56, %edi
	movl	$0, 24(%rax)
	leaq	16+_ZTVN6icu_6712OrConstraintE(%rip), %rax
	movq	%rax, (%r15)
	movq	%r15, 16(%r14)
	movq	$0, 16(%r15)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1309
	leaq	16+_ZTVN6icu_6713AndConstraintE(%rip), %rdi
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rax)
	movq	%rdi, (%rax)
	movabsq	$-4294967296, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdi, 8(%rax)
	movl	$-1, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 36(%rax)
	movq	$0, 44(%rax)
	movq	%rax, 8(%r15)
.L1376:
	movq	%rax, 96(%rbx)
	movl	(%r12), %edx
	movl	88(%rbx), %eax
	jmp	.L1306
.L1299:
	movq	96(%rbx), %r14
	movl	48(%r14), %edx
	testl	%edx, %edx
	jg	.L1455
	movl	$56, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1305
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	leaq	16+_ZTVN6icu_6713AndConstraintE(%rip), %rax
	movq	%rax, (%rcx)
	movabsq	$-4294967296, %rax
	movq	%rax, 8(%rcx)
	movq	$0, 36(%rcx)
	movq	$0, 44(%rcx)
	movl	88(%rbx), %eax
	movl	$-1, 16(%rcx)
	movl	(%r12), %edx
	movq	$0, 24(%rcx)
	movq	%rcx, 40(%r14)
.L1304:
	movq	%rcx, 96(%rbx)
	jmp	.L1306
.L1300:
	movl	$224, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1448
	movdqa	-224(%rbp), %xmm2
	movl	$2, %r11d
	pxor	%xmm0, %xmm0
	movl	$2, %r15d
	movw	%r11w, 16(%rax)
	leaq	24(%rbx), %rsi
	leaq	8(%r14), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm0, 72(%rax)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 88(%r14)
	movq	%rax, 152(%r14)
	movl	$2, %eax
	movw	%ax, 160(%r14)
	xorl	%eax, %eax
	movw	%r15w, 96(%r14)
	movw	%ax, 216(%r14)
	movl	$0, 220(%r14)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-200(%rbp), %rax
	movq	8(%rax), %r15
	testq	%r15, %r15
	jne	.L1350
	movq	%r14, 8(%rax)
.L1351:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1448
	movq	$0, 8(%rax)
	movl	$56, %edi
	movq	$0, 16(%rax)
	movl	$0, 24(%rax)
	leaq	16+_ZTVN6icu_6712OrConstraintE(%rip), %rax
	movq	%rax, (%r15)
	movq	%r15, 80(%r14)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1359
	leaq	16+_ZTVN6icu_6713AndConstraintE(%rip), %rdi
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rax)
	movq	%rdi, (%rax)
	movabsq	$-4294967296, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdi, 8(%rax)
	movl	$-1, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 36(%rax)
	movq	$0, 44(%rax)
	movq	%rax, 8(%r15)
.L1375:
	movq	%rax, %xmm0
	movq	%r14, %xmm3
	movl	88(%rbx), %eax
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 96(%rbx)
	movl	(%r12), %edx
	jmp	.L1306
.L1301:
	movq	96(%rbx), %rax
	testq	%rax, %rax
	je	.L1339
	movq	24(%rax), %r14
	testq	%r14, %r14
	je	.L1339
	movslq	8(%r14), %rax
	movl	%eax, %esi
	movl	%eax, 112(%rbx)
	addl	$1, %esi
	js	.L1341
	cmpl	12(%r14), %esi
	jle	.L1389
.L1341:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1343
	movq	96(%rbx), %rax
	movq	24(%rax), %r15
.L1344:
	movslq	8(%r15), %rax
	movl	%eax, %esi
	movl	%eax, 116(%rbx)
	addl	$1, %esi
	js	.L1345
	cmpl	12(%r15), %esi
	jle	.L1346
.L1345:
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L1445
	movslq	8(%r15), %rax
.L1346:
	movq	24(%r15), %rdx
	movl	$-1, (%rdx,%rax,4)
	addl	$1, 8(%r15)
.L1445:
	movl	88(%rbx), %eax
.L1446:
	movl	(%r12), %edx
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1451:
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L1264
.L1302:
	movq	96(%rbx), %r14
	movzwl	32(%rbx), %eax
	leaq	24(%rbx), %r10
	movabsq	$-4294967295, %rdx
	cmpq	%rdx, 8(%r14)
	je	.L1456
	movq	24(%r14), %r15
	testq	%r15, %r15
	je	.L1457
	movl	112(%rbx), %r14d
	testl	%r14d, %r14d
	js	.L1327
	movl	8(%r15), %edx
	testl	%edx, %edx
	jle	.L1327
	subl	%r14d, %edx
	testl	%edx, %edx
	jle	.L1327
	movq	24(%r15), %rcx
	movslq	%r14d, %rdx
	cmpl	$-1, (%rcx,%rdx,4)
	jne	.L1327
	testw	%ax, %ax
	js	.L1328
	movswl	%ax, %edx
	sarl	$5, %edx
.L1329:
	leaq	-192(%rbp), %r11
	movq	%r10, %rdi
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movq	%r11, %rcx
	movl	$128, %r8d
	movq	%r10, -232(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movq	-208(%rbp), %r11
	xorl	%esi, %esi
	movl	$10, %edx
	cltq
	movq	%r11, %rdi
	movb	$0, -192(%rbp,%rax)
	call	strtol@PLT
	movl	%r14d, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movq	96(%rbx), %rax
	movl	116(%rbx), %r15d
	movq	-208(%rbp), %r11
	movq	-232(%rbp), %r10
	movq	24(%rax), %r14
	movzwl	32(%rbx), %eax
	testw	%ax, %ax
	js	.L1330
	movswl	%ax, %edx
	sarl	$5, %edx
.L1331:
	movq	%r11, %rcx
	movq	%r10, %rdi
	xorl	%r9d, %r9d
	movl	$128, %r8d
	xorl	%esi, %esi
	movq	%r11, -208(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movq	-208(%rbp), %r11
	movl	$10, %edx
	xorl	%esi, %esi
	cltq
	movq	%r11, %rdi
	movb	$0, -192(%rbp,%rax)
	call	strtol@PLT
	movl	%r15d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movl	88(%rbx), %eax
	movl	(%r12), %edx
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1280:
	movl	88(%rbx), %eax
	cmpl	$28, %eax
	ja	.L1269
	movl	$270260556, %edx
	btq	%rax, %rdx
	jc	.L1281
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1277:
	movl	88(%rbx), %eax
	leal	-26(%rax), %edx
	cmpl	$1, %edx
	ja	.L1269
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1278:
	movl	88(%rbx), %eax
	leal	-21(%rax), %edx
	cmpl	$4, %edx
	jbe	.L1281
	cmpl	$6, %eax
	je	.L1281
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1272:
	movl	88(%rbx), %eax
	cmpl	$1, %eax
	je	.L1281
	cmpl	$14, %eax
	jne	.L1269
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1274:
	movl	88(%rbx), %eax
	leal	-15(%rax), %edx
	andl	$-5, %edx
	je	.L1281
	cmpl	$1, %eax
	je	.L1281
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1368:
	movl	$268435528, %edx
	btq	%rax, %rdx
	jc	.L1289
	movq	104(%rbx), %rdi
	cmpl	$9, %eax
	jne	.L1369
	movb	$1, 216(%rdi)
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1362:
	movl	$268435528, %edx
	btq	%rax, %rdx
	jc	.L1289
	movq	104(%rbx), %rdi
	cmpl	$9, %eax
	jne	.L1363
	movb	$1, 217(%rdi)
	jmp	.L1364
	.p2align 4,,10
	.p2align 3
.L1365:
	movl	36(%rbx), %ecx
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1371:
	movl	36(%rbx), %ecx
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1350:
	movq	72(%r15), %rax
	testq	%rax, %rax
	je	.L1352
	.p2align 4,,10
	.p2align 3
.L1357:
	movswl	16(%rax), %edx
	leaq	8(%rax), %rdi
	testw	%dx, %dx
	js	.L1353
	sarl	$5, %edx
.L1354:
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	$5, %r9d
	leaq	_ZN6icu_67L20PLURAL_KEYWORD_OTHERE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L20PLURAL_KEYWORD_OTHERE(%rip), %r11
	testb	%al, %al
	jne	.L1355
	movq	72(%r15), %rax
.L1356:
	movq	%rax, 72(%r14)
	movq	%r14, 72(%r15)
	jmp	.L1351
	.p2align 4,,10
	.p2align 3
.L1455:
	movl	%edx, (%r12)
	xorl	%ecx, %ecx
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1339:
	movl	$65792, (%r12)
	movl	$2, 92(%rbx)
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1355:
	movq	72(%r15), %r15
	movq	72(%r15), %rax
	testq	%rax, %rax
	jne	.L1357
.L1352:
	xorl	%eax, %eax
	jmp	.L1356
	.p2align 4,,10
	.p2align 3
.L1327:
	movl	116(%rbx), %r14d
	testw	%ax, %ax
	js	.L1332
	movswl	%ax, %edx
	sarl	$5, %edx
.L1333:
	leaq	-192(%rbp), %r11
	movq	%r10, %rdi
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movq	%r11, %rcx
	movl	$128, %r8d
	movq	%r11, -208(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movq	-208(%rbp), %r11
	movl	$10, %edx
	xorl	%esi, %esi
	cltq
	movq	%r11, %rdi
	movb	$0, -192(%rbp,%rax)
	call	strtol@PLT
	movl	%r14d, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movq	96(%rbx), %rax
	movslq	112(%rbx), %rdx
	movq	24(%rax), %rsi
	testl	%edx, %edx
	js	.L1334
	movl	8(%rsi), %edi
	movslq	116(%rbx), %rcx
	movl	88(%rbx), %eax
	testl	%edi, %edi
	jle	.L1446
	movl	%edi, %r8d
	subl	%edx, %r8d
	testl	%r8d, %r8d
	jle	.L1336
	movq	24(%rsi), %r8
	xorl	%esi, %esi
	movl	(%r8,%rdx,4), %edx
	testl	%ecx, %ecx
	js	.L1337
	subl	%ecx, %edi
	testl	%edi, %edi
	jg	.L1381
	.p2align 4,,10
	.p2align 3
.L1337:
	cmpl	%edx, %esi
	jge	.L1446
	movl	$65792, (%r12)
	movl	%eax, 92(%rbx)
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1353:
	movl	20(%rax), %edx
	jmp	.L1354
	.p2align 4,,10
	.p2align 3
.L1454:
	movslq	8(%r14), %rax
	movq	96(%rbx), %rdx
	movl	%eax, %esi
	movq	%r14, 24(%rdx)
	addl	$1, %esi
	js	.L1313
	cmpl	12(%r14), %esi
	jle	.L1386
.L1313:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L1458
	movq	96(%rbx), %rdx
	movslq	8(%r14), %rax
	movq	24(%rdx), %r15
.L1314:
	movq	24(%r14), %rcx
	movl	$-1, (%rcx,%rax,4)
	addl	$1, 8(%r14)
.L1316:
	movslq	8(%r15), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L1317
	cmpl	12(%r15), %esi
	jle	.L1318
.L1317:
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L1319
	movq	96(%rbx), %rdx
.L1320:
	movabsq	$4294967296, %rax
	movq	%rax, 112(%rbx)
	movl	88(%rbx), %eax
	movl	$2147483647, 16(%rdx)
	cmpl	$19, %eax
	setne	33(%rdx)
	movl	(%r12), %edx
	jmp	.L1306
.L1310:
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L1450
.L1448:
	movl	$7, (%r12)
	jmp	.L1450
	.p2align 4,,10
	.p2align 3
.L1458:
	movq	96(%rbx), %rdx
	movq	24(%rdx), %r15
	jmp	.L1316
.L1457:
	testw	%ax, %ax
	js	.L1325
	movswl	%ax, %edx
	sarl	$5, %edx
.L1326:
	leaq	-192(%rbp), %r15
	movq	%r10, %rdi
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movl	$128, %r8d
	movq	%r15, %rcx
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	cltq
	movb	$0, -192(%rbp,%rax)
	call	strtol@PLT
	movl	(%r12), %edx
	movl	%eax, 16(%r14)
	movl	88(%rbx), %eax
	jmp	.L1306
.L1386:
	movq	%r14, %r15
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1334:
	movslq	116(%rbx), %rcx
	movl	88(%rbx), %eax
.L1336:
	testl	%ecx, %ecx
	js	.L1446
	movl	8(%rsi), %edx
	testl	%edx, %edx
	jle	.L1446
	subl	%ecx, %edx
	testl	%edx, %edx
	jle	.L1446
	movq	24(%rsi), %r8
	xorl	%edx, %edx
.L1381:
	movl	(%r8,%rcx,4), %esi
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1319:
	movslq	8(%r15), %rax
	movq	96(%rbx), %rdx
.L1318:
	movq	24(%r15), %rcx
	movl	$-1, (%rcx,%rax,4)
	addl	$1, 8(%r15)
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1332:
	movl	36(%rbx), %edx
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1456:
	testw	%ax, %ax
	js	.L1322
	movswl	%ax, %edx
	sarl	$5, %edx
.L1323:
	leaq	-192(%rbp), %r15
	movq	%r10, %rdi
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movl	$128, %r8d
	movq	%r15, %rcx
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	cltq
	movb	$0, -192(%rbp,%rax)
	call	strtol@PLT
	movl	(%r12), %edx
	movl	%eax, 12(%r14)
	movl	88(%rbx), %eax
	jmp	.L1306
.L1343:
	movq	96(%rbx), %rdx
	movslq	8(%r14), %rax
	movq	24(%rdx), %r15
.L1342:
	movq	24(%r14), %rdx
	movl	$-1, (%rdx,%rax,4)
	addl	$1, 8(%r14)
	jmp	.L1344
.L1389:
	movq	%r14, %r15
	jmp	.L1342
.L1322:
	movl	36(%rbx), %edx
	jmp	.L1323
.L1328:
	movl	36(%rbx), %edx
	jmp	.L1329
.L1325:
	movl	36(%rbx), %edx
	jmp	.L1326
.L1330:
	movl	36(%rbx), %edx
	jmp	.L1331
.L1452:
	call	__stack_chk_fail@PLT
.L1309:
	movq	$0, 8(%r15)
	movl	$7, (%r12)
	jmp	.L1376
.L1308:
	movq	$0, 16(%r14)
	jmp	.L1448
.L1305:
	movl	88(%rbx), %eax
	movq	$0, 40(%r14)
	movl	$7, (%r12)
	movq	$0, 96(%rbx)
	movl	%eax, 92(%rbx)
	jmp	.L1264
.L1359:
	movq	$0, 8(%r15)
	movl	$7, (%r12)
	jmp	.L1375
	.cfi_endproc
.LFE5391:
	.size	_ZN6icu_6716PluralRuleParser5parseERKNS_13UnicodeStringEPNS_11PluralRulesER10UErrorCode.part.0, .-_ZN6icu_6716PluralRuleParser5parseERKNS_13UnicodeStringEPNS_11PluralRulesER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716PluralRuleParser5parseERKNS_13UnicodeStringEPNS_11PluralRulesER10UErrorCode
	.type	_ZN6icu_6716PluralRuleParser5parseERKNS_13UnicodeStringEPNS_11PluralRulesER10UErrorCode, @function
_ZN6icu_6716PluralRuleParser5parseERKNS_13UnicodeStringEPNS_11PluralRulesER10UErrorCode:
.LFB3829:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1459
	jmp	_ZN6icu_6716PluralRuleParser5parseERKNS_13UnicodeStringEPNS_11PluralRulesER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1459:
	ret
	.cfi_endproc
.LFE3829:
	.size	_ZN6icu_6716PluralRuleParser5parseERKNS_13UnicodeStringEPNS_11PluralRulesER10UErrorCode, .-_ZN6icu_6716PluralRuleParser5parseERKNS_13UnicodeStringEPNS_11PluralRulesER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6711PluralRules11createRulesERKNS_13UnicodeStringER10UErrorCode.part.0, @function
_ZN6icu_6711PluralRules11createRulesERKNS_13UnicodeStringER10UErrorCode.part.0:
.LFB5392:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16+_ZTVN6icu_6716PluralRuleParserE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$2, %esi
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%r14, -176(%rbp)
	movl	$0, -160(%rbp)
	movq	%rax, -152(%rbp)
	movw	%si, -144(%rbp)
	movq	$0, -88(%rbp)
	movq	$-1, -64(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1462
	movl	$0, 16(%rax)
	movl	(%rbx), %ecx
	leaq	16+_ZTVN6icu_6711PluralRulesE(%rip), %r15
	movq	%r15, (%rax)
	movq	$0, 8(%rax)
	testl	%ecx, %ecx
	jle	.L1479
.L1463:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1465:
	leaq	-152(%rbp), %rdi
	movq	%r14, -176(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1480
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1462:
	.cfi_restore_state
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L1465
	movl	$7, (%rbx)
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1479:
	movq	%rax, %rdx
	leaq	-176(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r13, %rsi
	call	_ZN6icu_6716PluralRuleParser5parseERKNS_13UnicodeStringEPNS_11PluralRulesER10UErrorCode.part.0
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1465
	movq	(%r12), %rax
	leaq	_ZN6icu_6711PluralRulesD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1466
	movq	8(%r12), %r13
	movq	%r15, (%r12)
	testq	%r13, %r13
	je	.L1463
	movq	0(%r13), %rax
	leaq	_ZN6icu_679RuleChainD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1468
	call	_ZN6icu_679RuleChainD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1466:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*%rax
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1468:
	call	*%rax
	jmp	.L1463
.L1480:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5392:
	.size	_ZN6icu_6711PluralRules11createRulesERKNS_13UnicodeStringER10UErrorCode.part.0, .-_ZN6icu_6711PluralRules11createRulesERKNS_13UnicodeStringER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRules11createRulesERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6711PluralRules11createRulesERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6711PluralRules11createRulesERKNS_13UnicodeStringER10UErrorCode:
.LFB3806:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1482
	jmp	_ZN6icu_6711PluralRules11createRulesERKNS_13UnicodeStringER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1482:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3806:
	.size	_ZN6icu_6711PluralRules11createRulesERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6711PluralRules11createRulesERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRules18createDefaultRulesER10UErrorCode
	.type	_ZN6icu_6711PluralRules18createDefaultRulesER10UErrorCode, @function
_ZN6icu_6711PluralRules18createDefaultRulesER10UErrorCode:
.LFB3807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r14
	leaq	-120(%rbp), %rdx
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	_ZN6icu_67L19PLURAL_DEFAULT_RULEE(%rip), %rax
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L1484
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711PluralRules11createRulesERKNS_13UnicodeStringER10UErrorCode.part.0
	movq	%rax, %r13
.L1484:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1489
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1489:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3807:
	.size	_ZN6icu_6711PluralRules18createDefaultRulesER10UErrorCode, .-_ZN6icu_6711PluralRules18createDefaultRulesER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6711PluralRules17internalForLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode.part.0, @function
_ZN6icu_6711PluralRules17internalForLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode.part.0:
.LFB5393:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$24, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -244(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1491
	movl	$0, 16(%rax)
	movl	0(%r13), %edi
	leaq	16+_ZTVN6icu_6711PluralRulesE(%rip), %rbx
	movq	%rbx, (%rax)
	movl	-244(%rbp), %ecx
	testl	%edi, %edi
	movq	$0, 8(%rax)
	jle	.L1513
.L1492:
	movq	8(%r12), %r13
	movq	%rbx, (%r12)
	testq	%r13, %r13
	je	.L1503
	movq	0(%r13), %rax
	leaq	_ZN6icu_679RuleChainD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1504
	call	_ZN6icu_679RuleChainD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1503:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1490:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1514
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1513:
	.cfi_restore_state
	leaq	-240(%rbp), %r15
	movq	%rax, %rsi
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6711PluralRules19getRuleFromResourceERKNS_6LocaleE11UPluralTypeR10UErrorCode
	movswl	-232(%rbp), %eax
	testw	%ax, %ax
	js	.L1495
	sarl	$5, %eax
.L1496:
	movl	0(%r13), %edx
	testl	%eax, %eax
	jne	.L1497
	cmpl	$7, %edx
	je	.L1498
	leaq	-176(%rbp), %r14
	leaq	_ZN6icu_67L19PLURAL_DEFAULT_RULEE(%rip), %rsi
	movq	%r14, %rdi
	leaq	16+_ZTVN6icu_6716PluralRuleParserE(%rip), %rbx
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$2, %esi
	movl	$0, 0(%r13)
	movq	%rbx, -176(%rbp)
	movl	$0, -160(%rbp)
	movq	%rax, -152(%rbp)
	movw	%si, -144(%rbp)
	movq	$0, -88(%rbp)
	movq	$-1, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
.L1499:
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6716PluralRuleParser5parseERKNS_13UnicodeStringEPNS_11PluralRulesER10UErrorCode.part.0
	jmp	.L1500
.L1491:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1490
	movl	$7, 0(%r13)
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1497:
	leaq	16+_ZTVN6icu_6716PluralRuleParserE(%rip), %rbx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$2, %ecx
	movq	%rbx, -176(%rbp)
	movl	$0, -160(%rbp)
	movq	%rax, -152(%rbp)
	movw	%cx, -144(%rbp)
	movq	$0, -88(%rbp)
	movq	$-1, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testl	%edx, %edx
	jle	.L1515
.L1500:
	leaq	-152(%rbp), %rdi
	movq	%rbx, -176(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1495:
	movl	-228(%rbp), %eax
	jmp	.L1496
	.p2align 4,,10
	.p2align 3
.L1498:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r12), %rax
	leaq	_ZN6icu_6711PluralRulesD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1492
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*%rax
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1504:
	call	*%rax
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1515:
	leaq	-176(%rbp), %r14
	jmp	.L1499
.L1514:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5393:
	.size	_ZN6icu_6711PluralRules17internalForLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode.part.0, .-_ZN6icu_6711PluralRules17internalForLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRules17internalForLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode
	.type	_ZN6icu_6711PluralRules17internalForLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode, @function
_ZN6icu_6711PluralRules17internalForLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode:
.LFB3812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L1516
	movl	%esi, %r14d
	movq	%rdx, %r13
	cmpl	$1, %esi
	jg	.L1542
	movq	%rdi, %r15
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1519
	movl	$0, 16(%rax)
	movl	0(%r13), %edi
	leaq	16+_ZTVN6icu_6711PluralRulesE(%rip), %rbx
	movq	%rbx, (%rax)
	movq	$0, 8(%rax)
	testl	%edi, %edi
	jle	.L1543
.L1520:
	movq	8(%r12), %r13
	movq	%rbx, (%r12)
	testq	%r13, %r13
	je	.L1531
	movq	0(%r13), %rax
	leaq	_ZN6icu_679RuleChainD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1532
	call	_ZN6icu_679RuleChainD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1531:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1516:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1544
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1542:
	.cfi_restore_state
	movl	$1, (%rdx)
	jmp	.L1516
	.p2align 4,,10
	.p2align 3
.L1543:
	leaq	-240(%rbp), %r9
	movq	%rax, %rsi
	movq	%r13, %r8
	movl	%r14d, %ecx
	movq	%r9, %rdi
	movq	%r15, %rdx
	movq	%r9, -248(%rbp)
	call	_ZN6icu_6711PluralRules19getRuleFromResourceERKNS_6LocaleE11UPluralTypeR10UErrorCode
	movswl	-232(%rbp), %eax
	movq	-248(%rbp), %r9
	testw	%ax, %ax
	js	.L1523
	sarl	$5, %eax
.L1524:
	movl	0(%r13), %edx
	testl	%eax, %eax
	jne	.L1525
	cmpl	$7, %edx
	je	.L1526
	leaq	-176(%rbp), %r14
	leaq	_ZN6icu_67L19PLURAL_DEFAULT_RULEE(%rip), %rsi
	movq	%r9, -248(%rbp)
	movq	%r14, %rdi
	leaq	16+_ZTVN6icu_6716PluralRuleParserE(%rip), %rbx
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-248(%rbp), %r9
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$2, %esi
	movl	$0, 0(%r13)
	movq	-248(%rbp), %r9
	movq	%rbx, -176(%rbp)
	movl	$0, -160(%rbp)
	movq	%rax, -152(%rbp)
	movw	%si, -144(%rbp)
	movq	$0, -88(%rbp)
	movq	$-1, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
.L1527:
	movq	%r9, %rsi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r9, -248(%rbp)
	call	_ZN6icu_6716PluralRuleParser5parseERKNS_13UnicodeStringEPNS_11PluralRulesER10UErrorCode.part.0
	movq	-248(%rbp), %r9
.L1528:
	leaq	-152(%rbp), %rdi
	movq	%r9, -248(%rbp)
	movq	%rbx, -176(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-248(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1516
	.p2align 4,,10
	.p2align 3
.L1523:
	movl	-228(%rbp), %eax
	jmp	.L1524
.L1519:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1516
	movl	$7, 0(%r13)
	jmp	.L1516
	.p2align 4,,10
	.p2align 3
.L1525:
	leaq	16+_ZTVN6icu_6716PluralRuleParserE(%rip), %rbx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$2, %ecx
	movq	%rbx, -176(%rbp)
	movl	$0, -160(%rbp)
	movq	%rax, -152(%rbp)
	movw	%cx, -144(%rbp)
	movq	$0, -88(%rbp)
	movq	$-1, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testl	%edx, %edx
	jg	.L1528
	leaq	-176(%rbp), %r14
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1526:
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r12), %rax
	leaq	_ZN6icu_6711PluralRulesD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1520
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*%rax
	jmp	.L1516
	.p2align 4,,10
	.p2align 3
.L1532:
	call	*%rax
	jmp	.L1531
.L1544:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3812:
	.size	_ZN6icu_6711PluralRules17internalForLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode, .-_ZN6icu_6711PluralRules17internalForLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716PluralRuleParser11checkSyntaxER10UErrorCode
	.type	_ZN6icu_6716PluralRuleParser11checkSyntaxER10UErrorCode, @function
_ZN6icu_6716PluralRuleParser11checkSyntaxER10UErrorCode:
.LFB3879:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1592
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	92(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L1548
	cmpl	$3, %eax
	jne	.L1595
.L1548:
	cmpl	$25, %eax
	ja	.L1549
	leaq	.L1551(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1551:
	.long	.L1559-.L1551
	.long	.L1560-.L1551
	.long	.L1553-.L1551
	.long	.L1559-.L1551
	.long	.L1549-.L1551
	.long	.L1558-.L1551
	.long	.L1557-.L1551
	.long	.L1549-.L1551
	.long	.L1553-.L1551
	.long	.L1549-.L1551
	.long	.L1556-.L1551
	.long	.L1555-.L1551
	.long	.L1555-.L1551
	.long	.L1553-.L1551
	.long	.L1554-.L1551
	.long	.L1553-.L1551
	.long	.L1553-.L1551
	.long	.L1553-.L1551
	.long	.L1549-.L1551
	.long	.L1553-.L1551
	.long	.L1552-.L1551
	.long	.L1550-.L1551
	.long	.L1550-.L1551
	.long	.L1550-.L1551
	.long	.L1550-.L1551
	.long	.L1550-.L1551
	.text
	.p2align 4,,10
	.p2align 3
.L1559:
	movl	88(%rbx), %eax
	cmpl	$10, %eax
	je	.L1545
	cmpl	$28, %eax
	je	.L1545
	.p2align 4,,10
	.p2align 3
.L1549:
	movl	$65792, (%r12)
.L1545:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1592:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L1595:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	88(%rdi), %esi
	leaq	24(%rdi), %rdi
	call	_ZN6icu_6716PluralRuleParser10getKeyTypeERKNS_13UnicodeStringENS_9tokenTypeE
	movl	%eax, 88(%rbx)
	movl	92(%rbx), %eax
	jmp	.L1548
	.p2align 4,,10
	.p2align 3
.L1553:
	cmpl	$1, 88(%rbx)
	jne	.L1549
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1550:
	movl	88(%rbx), %eax
	leal	-13(%rax), %edx
	cmpl	$4, %edx
	jbe	.L1545
	subl	$19, %eax
	cmpl	$1, %eax
	jbe	.L1545
	jmp	.L1549
	.p2align 4,,10
	.p2align 3
.L1555:
	movl	88(%rbx), %eax
	subl	$21, %eax
	cmpl	$4, %eax
	jbe	.L1545
	jmp	.L1549
	.p2align 4,,10
	.p2align 3
.L1552:
	movl	88(%rbx), %eax
	cmpl	$1, %eax
	je	.L1545
	cmpl	$14, %eax
	jne	.L1549
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1554:
	movl	88(%rbx), %eax
	leal	-15(%rax), %edx
	andl	$-5, %edx
	je	.L1545
	cmpl	$1, %eax
	je	.L1545
	jmp	.L1549
	.p2align 4,,10
	.p2align 3
.L1556:
	cmpl	$5, 88(%rbx)
	jne	.L1549
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1557:
	movl	88(%rbx), %eax
	subl	$26, %eax
	cmpl	$1, %eax
	ja	.L1549
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1558:
	movl	88(%rbx), %eax
	leal	-21(%rax), %edx
	cmpl	$4, %edx
	jbe	.L1545
	cmpl	$6, %eax
	je	.L1545
	jmp	.L1549
	.p2align 4,,10
	.p2align 3
.L1560:
	movl	88(%rbx), %eax
	cmpl	$28, %eax
	ja	.L1549
	movl	$270260556, %edx
	btq	%rax, %rdx
	jc	.L1545
	jmp	.L1549
	.cfi_endproc
.LFE3879:
	.size	_ZN6icu_6716PluralRuleParser11checkSyntaxER10UErrorCode, .-_ZN6icu_6716PluralRuleParser11checkSyntaxER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724PluralKeywordEnumerationC2EPNS_9RuleChainER10UErrorCode
	.type	_ZN6icu_6724PluralKeywordEnumerationC2EPNS_9RuleChainER10UErrorCode, @function
_ZN6icu_6724PluralKeywordEnumerationC2EPNS_9RuleChainER10UErrorCode:
.LFB3884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	addq	$120, %r13
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	leaq	16+_ZTVN6icu_6724PluralKeywordEnumerationE(%rip), %rax
	movl	$0, -4(%r13)
	movq	%r12, %rsi
	movq	%rax, -120(%r13)
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L1616
.L1596:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1616:
	.cfi_restore_state
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	testq	%rbx, %rbx
	je	.L1599
	movl	$1, %r14d
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1619:
	sarl	$5, %edx
.L1603:
	movl	$5, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r10, %rdi
	leaq	_ZN6icu_67L20PLURAL_KEYWORD_OTHERE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L20PLURAL_KEYWORD_OTHERE(%rip), %rdx
	movq	72(%rbx), %rbx
	testb	%al, %al
	movl	$0, %eax
	cmove	%eax, %r14d
	testq	%rbx, %rbx
	je	.L1617
.L1606:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1600
	leaq	8(%rbx), %r10
	movq	%rax, %rdi
	movq	%r10, %rsi
	movq	%r10, -56(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%r12), %edx
	movq	-56(%rbp), %r10
	testl	%edx, %edx
	jg	.L1618
	movswl	16(%rbx), %edx
	testw	%dx, %dx
	jns	.L1619
	movl	20(%rbx), %edx
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1618:
	movq	(%r15), %rdx
	movq	%r15, %rdi
	movq	8(%rdx), %rdx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L1617:
	.cfi_restore_state
	testb	%r14b, %r14b
	je	.L1596
.L1599:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1600
	movq	%rax, %rdi
	leaq	_ZN6icu_67L20PLURAL_KEYWORD_OTHERE(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L1596
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L1600:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L1596
	.cfi_endproc
.LFE3884:
	.size	_ZN6icu_6724PluralKeywordEnumerationC2EPNS_9RuleChainER10UErrorCode, .-_ZN6icu_6724PluralKeywordEnumerationC2EPNS_9RuleChainER10UErrorCode
	.globl	_ZN6icu_6724PluralKeywordEnumerationC1EPNS_9RuleChainER10UErrorCode
	.set	_ZN6icu_6724PluralKeywordEnumerationC1EPNS_9RuleChainER10UErrorCode,_ZN6icu_6724PluralKeywordEnumerationC2EPNS_9RuleChainER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PluralRules11getKeywordsER10UErrorCode
	.type	_ZNK6icu_6711PluralRules11getKeywordsER10UErrorCode, @function
_ZNK6icu_6711PluralRules11getKeywordsER10UErrorCode:
.LFB3817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L1628
	movl	16(%rdi), %eax
	movq	%rdi, %r13
	movq	%rsi, %rbx
	testl	%eax, %eax
	jg	.L1629
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1624
	movq	8(%r13), %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6724PluralKeywordEnumerationC1EPNS_9RuleChainER10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L1620
	movq	%r12, %rdi
	call	_ZN6icu_6724PluralKeywordEnumerationD0Ev
.L1628:
	xorl	%r12d, %r12d
.L1620:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1629:
	.cfi_restore_state
	movl	%eax, (%rsi)
	xorl	%r12d, %r12d
	addq	$8, %rsp
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1624:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1628
	movl	$7, (%rbx)
	jmp	.L1628
	.cfi_endproc
.LFE3817:
	.size	_ZNK6icu_6711PluralRules11getKeywordsER10UErrorCode, .-_ZNK6icu_6711PluralRules11getKeywordsER10UErrorCode
	.section	.text.unlikely
	.align 2
.LCOLDB33:
	.text
.LHOTB33:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PluralRuleseqERKS0_
	.type	_ZNK6icu_6711PluralRuleseqERKS0_, @function
_ZNK6icu_6711PluralRuleseqERKS0_:
.LFB3826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	cmpq	%rsi, %rdi
	je	.L1654
	movl	16(%rdi), %r10d
	movq	%rdi, %r14
	testl	%r10d, %r10d
	jg	.L1632
	movl	$160, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1632
	movq	8(%r14), %rsi
	leaq	-60(%rbp), %r15
	movq	%rax, %rdi
	movq	%r15, %rdx
	call	_ZN6icu_6724PluralKeywordEnumerationC1EPNS_9RuleChainER10UErrorCode
	movl	-60(%rbp), %r9d
	testl	%r9d, %r9d
	jle	.L1670
	movq	%r12, %rdi
	call	_ZN6icu_6724PluralKeywordEnumerationD0Ev
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L1632
	movl	16(%rbx), %r8d
	xorl	%r12d, %r12d
	testl	%r8d, %r8d
	jg	.L1632
.L1635:
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1637
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	movq	%r15, %rdx
	call	_ZN6icu_6724PluralKeywordEnumerationC1EPNS_9RuleChainER10UErrorCode
	movl	-60(%rbp), %edi
	testl	%edi, %edi
	jg	.L1671
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*32(%rax)
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%eax, -68(%rbp)
	movq	0(%r13), %rax
	call	*32(%rax)
	cmpl	%eax, -68(%rbp)
	jne	.L1646
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*64(%rax)
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1672:
	movq	%rbx, %rdi
	call	_ZNK6icu_6711PluralRules9isKeywordERKNS_13UnicodeStringE
	testb	%al, %al
	je	.L1646
.L1643:
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*56(%rax)
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L1672
	movq	0(%r13), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*64(%rax)
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L1673:
	movq	%r14, %rdi
	call	_ZNK6icu_6711PluralRules9isKeywordERKNS_13UnicodeStringE
	testb	%al, %al
	je	.L1646
.L1647:
	movq	0(%r13), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*56(%rax)
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L1673
	movl	-60(%rbp), %ecx
	testl	%ecx, %ecx
	setle	%r14b
	jmp	.L1644
	.p2align 4,,10
	.p2align 3
.L1670:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1635
	movl	%eax, -60(%rbp)
	xorl	%r14d, %r14d
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L1671:
	movq	%r13, %rdi
	call	_ZN6icu_6724PluralKeywordEnumerationD0Ev
	movl	-60(%rbp), %esi
	testl	%esi, %esi
	jle	.L1664
	.p2align 4,,10
	.p2align 3
.L1640:
	xorl	%r14d, %r14d
	testq	%r12, %r12
	je	.L1630
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L1646:
	xorl	%r14d, %r14d
.L1644:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L1652:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L1630:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1674
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1632:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1654:
	movl	$1, %r14d
	jmp	.L1630
.L1637:
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jg	.L1640
	movl	$7, -60(%rbp)
	jmp	.L1640
.L1674:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_6711PluralRuleseqERKS0_.cold, @function
_ZNK6icu_6711PluralRuleseqERKS0_.cold:
.LFSB3826:
.L1664:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*32(%rax)
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE3826:
	.text
	.size	_ZNK6icu_6711PluralRuleseqERKS0_, .-_ZNK6icu_6711PluralRuleseqERKS0_
	.section	.text.unlikely
	.size	_ZNK6icu_6711PluralRuleseqERKS0_.cold, .-_ZNK6icu_6711PluralRuleseqERKS0_.cold
.LCOLDE33:
	.text
.LHOTE33:
	.section	.text.unlikely
.LCOLDB34:
	.text
.LHOTB34:
	.p2align 4
	.globl	_ZN6icu_6724tokenTypeToPluralOperandENS_9tokenTypeE
	.type	_ZN6icu_6724tokenTypeToPluralOperandENS_9tokenTypeE, @function
_ZN6icu_6724tokenTypeToPluralOperandENS_9tokenTypeE:
.LFB3893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$21, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$4, %edi
	ja	.L1676
	leaq	CSWTCH.347(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	(%rax,%rdi,4), %eax
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6724tokenTypeToPluralOperandENS_9tokenTypeE.cold, @function
_ZN6icu_6724tokenTypeToPluralOperandENS_9tokenTypeE.cold:
.LFSB3893:
.L1676:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3893:
	.text
	.size	_ZN6icu_6724tokenTypeToPluralOperandENS_9tokenTypeE, .-_ZN6icu_6724tokenTypeToPluralOperandENS_9tokenTypeE
	.section	.text.unlikely
	.size	_ZN6icu_6724tokenTypeToPluralOperandENS_9tokenTypeE.cold, .-_ZN6icu_6724tokenTypeToPluralOperandENS_9tokenTypeE.cold
.LCOLDE34:
	.text
.LHOTE34:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FixedDecimalC2Edil
	.type	_ZN6icu_6712FixedDecimalC2Edil, @function
_ZN6icu_6712FixedDecimalC2Edil:
.LFB3898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm2
	movq	%rcx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm2, %xmm1
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movups	%xmm1, (%rdi)
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	andpd	.LC2(%rip), %xmm0
	movsd	%xmm0, 16(%rdi)
	seta	57(%rdi)
	call	uprv_isNaN_67@PLT
	movsd	16(%rbx), %xmm0
	movb	%al, 58(%rbx)
	call	uprv_isInfinite_67@PLT
	movb	%al, 59(%rbx)
	cmpw	$0, 58(%rbx)
	je	.L1680
	movq	$0, 48(%rbx)
	movb	$0, 56(%rbx)
	movl	$0, 24(%rbx)
	movq	$0, 32(%rbx)
.L1681:
	movq	$0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1680:
	.cfi_restore_state
	movsd	16(%rbx), %xmm1
	pxor	%xmm0, %xmm0
	movl	$0, %edx
	movl	%r13d, 24(%rbx)
	movq	%r12, 32(%rbx)
	cvttsd2siq	%xmm1, %rax
	cvtsi2sdq	%rax, %xmm0
	movq	%rax, 48(%rbx)
	ucomisd	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	movb	%al, 56(%rbx)
	testq	%r12, %r12
	je	.L1681
	movabsq	$-3689348814741910323, %rsi
	movq	%r12, %rax
	movabsq	$1844674407370955160, %rcx
	imulq	%rsi, %rax
	addq	%rcx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	ja	.L1684
	movabsq	$7378697629483820647, %rdi
	.p2align 4,,10
	.p2align 3
.L1685:
	movq	%r12, %rax
	sarq	$63, %r12
	imulq	%rdi
	sarq	$2, %rdx
	subq	%r12, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r12
	imulq	%rsi, %rax
	addq	%rcx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	jbe	.L1685
.L1684:
	movq	%r12, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3898:
	.size	_ZN6icu_6712FixedDecimalC2Edil, .-_ZN6icu_6712FixedDecimalC2Edil
	.globl	_ZN6icu_6712FixedDecimalC1Edil
	.set	_ZN6icu_6712FixedDecimalC1Edil,_ZN6icu_6712FixedDecimalC2Edil
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FixedDecimalC2Edi
	.type	_ZN6icu_6712FixedDecimalC2Edi, @function
_ZN6icu_6712FixedDecimalC2Edi:
.LFB3901:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm6
	movq	%rcx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm6, %xmm1
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movups	%xmm1, (%rdi)
	testl	%esi, %esi
	je	.L1703
	movq	.LC2(%rip), %xmm2
	movsd	.LC3(%rip), %xmm5
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm3
	andpd	%xmm2, %xmm1
	movapd	%xmm2, %xmm4
	ucomisd	%xmm1, %xmm5
	ja	.L1704
	ucomisd	%xmm3, %xmm0
	jp	.L1700
.L1705:
	jne	.L1700
.L1692:
	xorl	%r12d, %r12d
.L1690:
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	andpd	%xmm2, %xmm0
	movsd	%xmm0, 16(%rbx)
	seta	57(%rbx)
	call	uprv_isNaN_67@PLT
	movsd	16(%rbx), %xmm0
	movb	%al, 58(%rbx)
	call	uprv_isInfinite_67@PLT
	movb	%al, 59(%rbx)
	cmpw	$0, 58(%rbx)
	je	.L1694
	movq	$0, 48(%rbx)
	movb	$0, 56(%rbx)
	movl	$0, 24(%rbx)
	movq	$0, 32(%rbx)
.L1695:
	movq	$0, 40(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1704:
	.cfi_restore_state
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC8(%rip), %xmm5
	andnpd	%xmm0, %xmm4
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm3
	cmpnlesd	%xmm0, %xmm3
	andpd	%xmm5, %xmm3
	subsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm3
	orpd	%xmm4, %xmm3
	ucomisd	%xmm3, %xmm0
	jnp	.L1705
.L1700:
	movaps	%xmm2, -64(%rbp)
	movsd	%xmm0, -40(%rbp)
	call	uprv_isNaN_67@PLT
	movsd	-40(%rbp), %xmm0
	movapd	-64(%rbp), %xmm2
	testb	%al, %al
	jne	.L1692
	call	uprv_isPositiveInfinity_67@PLT
	movsd	-40(%rbp), %xmm0
	movapd	-64(%rbp), %xmm2
	testb	%al, %al
	jne	.L1692
	movl	%r13d, %edi
	call	_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi.part.0
	movapd	-64(%rbp), %xmm2
	movsd	-40(%rbp), %xmm0
	movq	%rax, %r12
	jmp	.L1690
	.p2align 4,,10
	.p2align 3
.L1694:
	movsd	16(%rbx), %xmm1
	pxor	%xmm0, %xmm0
	movl	$0, %edx
	movl	%r13d, 24(%rbx)
	movq	%r12, 32(%rbx)
	cvttsd2siq	%xmm1, %rax
	cvtsi2sdq	%rax, %xmm0
	movq	%rax, 48(%rbx)
	ucomisd	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	movb	%al, 56(%rbx)
	testq	%r12, %r12
	je	.L1695
	movabsq	$-3689348814741910323, %rsi
	movq	%r12, %rax
	movabsq	$1844674407370955160, %rcx
	imulq	%rsi, %rax
	addq	%rcx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	ja	.L1698
	movabsq	$7378697629483820647, %rdi
	.p2align 4,,10
	.p2align 3
.L1699:
	movq	%r12, %rax
	sarq	$63, %r12
	imulq	%rdi
	sarq	$2, %rdx
	subq	%r12, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r12
	imulq	%rsi, %rax
	addq	%rcx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	jbe	.L1699
.L1698:
	movq	%r12, 40(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1703:
	.cfi_restore_state
	movq	.LC2(%rip), %xmm2
	jmp	.L1692
	.cfi_endproc
.LFE3901:
	.size	_ZN6icu_6712FixedDecimalC2Edi, .-_ZN6icu_6712FixedDecimalC2Edi
	.globl	_ZN6icu_6712FixedDecimalC1Edi
	.set	_ZN6icu_6712FixedDecimalC1Edi,_ZN6icu_6712FixedDecimalC2Edi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FixedDecimalC2Ed
	.type	_ZN6icu_6712FixedDecimalC2Ed, @function
_ZN6icu_6712FixedDecimalC2Ed:
.LFB3904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm3
	movapd	%xmm3, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	.LC2(%rip), %xmm2
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm4
	andpd	%xmm2, %xmm1
	movq	%rsi, %xmm0
	movapd	%xmm1, %xmm5
	punpcklqdq	%xmm4, %xmm0
	movsd	.LC3(%rip), %xmm4
	movups	%xmm0, (%rdi)
	movapd	%xmm1, %xmm0
	andpd	%xmm2, %xmm0
	ucomisd	%xmm0, %xmm4
	jbe	.L1707
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm0, %xmm0
	movsd	.LC8(%rip), %xmm7
	movapd	%xmm2, %xmm6
	andnpd	%xmm1, %xmm6
	cvtsi2sdq	%rax, %xmm0
	movapd	%xmm0, %xmm5
	cmpnlesd	%xmm1, %xmm5
	andpd	%xmm7, %xmm5
	subsd	%xmm5, %xmm0
	movapd	%xmm0, %xmm5
	orpd	%xmm6, %xmm5
.L1707:
	ucomisd	%xmm5, %xmm1
	jnp	.L1757
.L1708:
	movsd	.LC4(%rip), %xmm0
	movapd	%xmm2, %xmm7
	mulsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm5
	movapd	%xmm0, %xmm6
	andpd	%xmm2, %xmm5
	ucomisd	%xmm5, %xmm4
	ja	.L1758
.L1711:
	ucomisd	%xmm6, %xmm0
	jnp	.L1759
.L1712:
	movsd	.LC6(%rip), %xmm0
	movapd	%xmm2, %xmm7
	mulsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm5
	movapd	%xmm0, %xmm6
	andpd	%xmm2, %xmm5
	ucomisd	%xmm5, %xmm4
	ja	.L1760
.L1715:
	ucomisd	%xmm6, %xmm0
	jnp	.L1761
.L1751:
	movsd	.LC7(%rip), %xmm0
	movapd	%xmm2, %xmm7
	mulsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm5
	movapd	%xmm0, %xmm6
	andpd	%xmm2, %xmm5
	ucomisd	%xmm5, %xmm4
	ja	.L1762
.L1718:
	ucomisd	%xmm6, %xmm0
	jnp	.L1763
.L1719:
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdi
	movl	$30, %edx
	leaq	.LC12(%rip), %rcx
	movw	%ax, -52(%rbp)
	movl	$1, %esi
	movl	$1, %eax
	movaps	%xmm0, -80(%rbp)
	movapd	%xmm1, %xmm0
	movsd	%xmm3, -96(%rbp)
	movsd	%xmm1, -88(%rbp)
	movq	$0, -64(%rbp)
	movl	$0, -56(%rbp)
	call	__sprintf_chk@PLT
	xorl	%esi, %esi
	leaq	-62(%rbp), %rdi
	movl	$10, %edx
	call	strtol@PLT
	movq	.LC3(%rip), %rcx
	cmpb	$48, -64(%rbp)
	movsd	-88(%rbp), %xmm1
	movsd	-96(%rbp), %xmm3
	movq	.LC2(%rip), %xmm2
	movq	%rcx, %xmm4
	jne	.L1734
	cmpb	$48, -65(%rbp)
	jne	.L1735
	cmpb	$48, -66(%rbp)
	jne	.L1736
	cmpb	$48, -67(%rbp)
	jne	.L1737
	cmpb	$48, -68(%rbp)
	jne	.L1738
	cmpb	$48, -69(%rbp)
	jne	.L1739
	cmpb	$48, -70(%rbp)
	jne	.L1740
	cmpb	$48, -71(%rbp)
	jne	.L1741
	cmpb	$48, -72(%rbp)
	jne	.L1742
	cmpb	$48, -73(%rbp)
	jne	.L1743
	cmpb	$48, -74(%rbp)
	jne	.L1744
	cmpb	$48, -75(%rbp)
	jne	.L1745
	cmpb	$48, -76(%rbp)
	jne	.L1746
	cmpb	$48, -77(%rbp)
	jne	.L1747
	cmpb	$48, -78(%rbp)
	jne	.L1748
	xorl	%edx, %edx
	cmpb	$48, -79(%rbp)
	sete	%dl
	negl	%edx
	.p2align 4,,10
	.p2align 3
.L1721:
	subl	%eax, %edx
	movl	%edx, %r13d
	je	.L1710
.L1714:
	movapd	%xmm3, %xmm0
	movapd	%xmm3, %xmm5
	andpd	%xmm2, %xmm0
	ucomisd	%xmm0, %xmm4
	jbe	.L1724
	cvttsd2siq	%xmm3, %rax
	pxor	%xmm0, %xmm0
	movsd	.LC8(%rip), %xmm5
	andnpd	%xmm3, %xmm2
	cvtsi2sdq	%rax, %xmm0
	movapd	%xmm0, %xmm4
	cmpnlesd	%xmm3, %xmm4
	andpd	%xmm5, %xmm4
	movapd	%xmm0, %xmm5
	subsd	%xmm4, %xmm5
	orpd	%xmm2, %xmm5
.L1724:
	ucomisd	%xmm5, %xmm3
	jp	.L1753
	jne	.L1753
.L1710:
	xorl	%r12d, %r12d
.L1723:
	pxor	%xmm0, %xmm0
	movsd	%xmm1, 16(%rbx)
	comisd	%xmm3, %xmm0
	movapd	%xmm1, %xmm0
	seta	57(%rbx)
	call	uprv_isNaN_67@PLT
	movsd	16(%rbx), %xmm0
	movb	%al, 58(%rbx)
	call	uprv_isInfinite_67@PLT
	movb	%al, 59(%rbx)
	cmpw	$0, 58(%rbx)
	je	.L1726
	movq	$0, 48(%rbx)
	movb	$0, 56(%rbx)
	movl	$0, 24(%rbx)
	movq	$0, 32(%rbx)
.L1727:
	movq	$0, 40(%rbx)
.L1706:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1764
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1758:
	.cfi_restore_state
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm5, %xmm5
	andnpd	%xmm0, %xmm7
	movsd	.LC8(%rip), %xmm8
	cvtsi2sdq	%rax, %xmm5
	movapd	%xmm5, %xmm6
	cmpnlesd	%xmm0, %xmm6
	andpd	%xmm8, %xmm6
	subsd	%xmm6, %xmm5
	movapd	%xmm5, %xmm6
	orpd	%xmm7, %xmm6
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L1760:
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm5, %xmm5
	andnpd	%xmm0, %xmm7
	movsd	.LC8(%rip), %xmm8
	cvtsi2sdq	%rax, %xmm5
	movapd	%xmm5, %xmm6
	cmpnlesd	%xmm0, %xmm6
	andpd	%xmm8, %xmm6
	subsd	%xmm6, %xmm5
	movapd	%xmm5, %xmm6
	orpd	%xmm7, %xmm6
	jmp	.L1715
	.p2align 4,,10
	.p2align 3
.L1762:
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm5, %xmm5
	andnpd	%xmm0, %xmm7
	movsd	.LC8(%rip), %xmm8
	cvtsi2sdq	%rax, %xmm5
	movapd	%xmm5, %xmm6
	cmpnlesd	%xmm0, %xmm6
	andpd	%xmm8, %xmm6
	subsd	%xmm6, %xmm5
	movapd	%xmm5, %xmm6
	orpd	%xmm7, %xmm6
	jmp	.L1718
	.p2align 4,,10
	.p2align 3
.L1738:
	movl	$11, %edx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1726:
	movsd	16(%rbx), %xmm1
	pxor	%xmm0, %xmm0
	movl	$0, %edx
	movl	%r13d, 24(%rbx)
	movq	%r12, 32(%rbx)
	cvttsd2siq	%xmm1, %rax
	cvtsi2sdq	%rax, %xmm0
	movq	%rax, 48(%rbx)
	ucomisd	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	movb	%al, 56(%rbx)
	testq	%r12, %r12
	je	.L1727
	movabsq	$-3689348814741910323, %rsi
	movq	%r12, %rax
	movabsq	$1844674407370955160, %rcx
	imulq	%rsi, %rax
	addq	%rcx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	ja	.L1730
	movabsq	$7378697629483820647, %rdi
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	%r12, %rax
	sarq	$63, %r12
	imulq	%rdi
	sarq	$2, %rdx
	subq	%r12, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r12
	imulq	%rsi, %rax
	addq	%rcx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	jbe	.L1731
.L1730:
	movq	%r12, 40(%rbx)
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1753:
	movapd	%xmm3, %xmm0
	movsd	%xmm1, -96(%rbp)
	movsd	%xmm3, -88(%rbp)
	call	uprv_isNaN_67@PLT
	movsd	-88(%rbp), %xmm3
	movsd	-96(%rbp), %xmm1
	testb	%al, %al
	jne	.L1710
	movapd	%xmm3, %xmm0
	call	uprv_isPositiveInfinity_67@PLT
	movsd	-88(%rbp), %xmm3
	movsd	-96(%rbp), %xmm1
	testb	%al, %al
	jne	.L1710
	movapd	%xmm3, %xmm0
	movl	%r13d, %edi
	call	_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi.part.0
	movsd	-96(%rbp), %xmm1
	movsd	-88(%rbp), %xmm3
	movq	%rax, %r12
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1734:
	movl	$15, %edx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1735:
	movl	$14, %edx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1736:
	movl	$13, %edx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1757:
	movl	$0, %r13d
	je	.L1710
	jmp	.L1708
	.p2align 4,,10
	.p2align 3
.L1737:
	movl	$12, %edx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1759:
	movl	$1, %r13d
	je	.L1714
	jmp	.L1712
	.p2align 4,,10
	.p2align 3
.L1761:
	movl	$2, %r13d
	je	.L1714
	jmp	.L1751
	.p2align 4,,10
	.p2align 3
.L1763:
	movl	$3, %r13d
	je	.L1714
	jmp	.L1719
	.p2align 4,,10
	.p2align 3
.L1739:
	movl	$10, %edx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1746:
	movl	$3, %edx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1740:
	movl	$9, %edx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1741:
	movl	$8, %edx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1742:
	movl	$7, %edx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1743:
	movl	$6, %edx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1744:
	movl	$5, %edx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1745:
	movl	$4, %edx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1747:
	movl	$2, %edx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1748:
	movl	$1, %edx
	jmp	.L1721
.L1764:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3904:
	.size	_ZN6icu_6712FixedDecimalC2Ed, .-_ZN6icu_6712FixedDecimalC2Ed
	.globl	_ZN6icu_6712FixedDecimalC1Ed
	.set	_ZN6icu_6712FixedDecimalC1Ed,_ZN6icu_6712FixedDecimalC2Ed
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FixedDecimalC2Ev
	.type	_ZN6icu_6712FixedDecimalC2Ev, @function
_ZN6icu_6712FixedDecimalC2Ev:
.LFB3907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm2
	movq	%rsi, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm2, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movb	$0, 57(%rdi)
	movq	$0x000000000, 16(%rdi)
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	call	uprv_isNaN_67@PLT
	movsd	16(%rbx), %xmm0
	movb	%al, 58(%rbx)
	call	uprv_isInfinite_67@PLT
	movb	%al, 59(%rbx)
	cmpw	$0, 58(%rbx)
	jne	.L1767
	movsd	16(%rbx), %xmm0
	pxor	%xmm1, %xmm1
	movl	$0, %ecx
	cvttsd2siq	%xmm0, %rdx
	cvtsi2sdq	%rdx, %xmm1
	ucomisd	%xmm0, %xmm1
	setnp	%al
	cmovne	%ecx, %eax
.L1766:
	pxor	%xmm0, %xmm0
	movb	%al, 56(%rbx)
	movq	%rdx, 48(%rbx)
	movl	$0, 24(%rbx)
	movups	%xmm0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1767:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L1766
	.cfi_endproc
.LFE3907:
	.size	_ZN6icu_6712FixedDecimalC2Ev, .-_ZN6icu_6712FixedDecimalC2Ev
	.globl	_ZN6icu_6712FixedDecimalC1Ev
	.set	_ZN6icu_6712FixedDecimalC1Ev,_ZN6icu_6712FixedDecimalC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FixedDecimalC2ERKS0_
	.type	_ZN6icu_6712FixedDecimalC2ERKS0_, @function
_ZN6icu_6712FixedDecimalC2ERKS0_:
.LFB3913:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	movdqu	32(%rsi), %xmm2
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movl	24(%rsi), %eax
	movq	%rdx, %xmm0
	movups	%xmm2, 32(%rdi)
	movl	%eax, 24(%rdi)
	movq	48(%rsi), %rax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	movsd	16(%rsi), %xmm0
	movq	%rax, 48(%rdi)
	movl	56(%rsi), %eax
	movsd	%xmm0, 16(%rdi)
	movl	%eax, 56(%rdi)
	ret
	.cfi_endproc
.LFE3913:
	.size	_ZN6icu_6712FixedDecimalC2ERKS0_, .-_ZN6icu_6712FixedDecimalC2ERKS0_
	.globl	_ZN6icu_6712FixedDecimalC1ERKS0_
	.set	_ZN6icu_6712FixedDecimalC1ERKS0_,_ZN6icu_6712FixedDecimalC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FixedDecimal4initEd
	.type	_ZN6icu_6712FixedDecimal4initEd, @function
_ZN6icu_6712FixedDecimal4initEd:
.LFB3919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm3
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	.LC2(%rip), %xmm2
	movsd	.LC3(%rip), %xmm4
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	andpd	%xmm2, %xmm1
	movapd	%xmm1, %xmm0
	movapd	%xmm1, %xmm5
	andpd	%xmm2, %xmm0
	ucomisd	%xmm0, %xmm4
	jbe	.L1771
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm0, %xmm0
	movsd	.LC8(%rip), %xmm7
	movapd	%xmm2, %xmm6
	andnpd	%xmm1, %xmm6
	cvtsi2sdq	%rax, %xmm0
	movapd	%xmm0, %xmm5
	cmpnlesd	%xmm1, %xmm5
	andpd	%xmm7, %xmm5
	subsd	%xmm5, %xmm0
	movapd	%xmm0, %xmm5
	orpd	%xmm6, %xmm5
.L1771:
	ucomisd	%xmm5, %xmm1
	jnp	.L1821
.L1772:
	movsd	.LC4(%rip), %xmm0
	movapd	%xmm2, %xmm7
	mulsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm5
	movapd	%xmm0, %xmm6
	andpd	%xmm2, %xmm5
	ucomisd	%xmm5, %xmm4
	ja	.L1822
.L1775:
	ucomisd	%xmm6, %xmm0
	jnp	.L1823
.L1776:
	movsd	.LC6(%rip), %xmm0
	movapd	%xmm2, %xmm7
	mulsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm5
	movapd	%xmm0, %xmm6
	andpd	%xmm2, %xmm5
	ucomisd	%xmm5, %xmm4
	ja	.L1824
.L1779:
	ucomisd	%xmm6, %xmm0
	jnp	.L1825
.L1815:
	movsd	.LC7(%rip), %xmm0
	movapd	%xmm2, %xmm7
	mulsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm5
	movapd	%xmm0, %xmm6
	andpd	%xmm2, %xmm5
	ucomisd	%xmm5, %xmm4
	ja	.L1826
.L1782:
	ucomisd	%xmm6, %xmm0
	jnp	.L1827
.L1783:
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdi
	movl	$30, %edx
	movw	%ax, -52(%rbp)
	movl	$1, %esi
	movl	$1, %eax
	leaq	.LC12(%rip), %rcx
	movaps	%xmm0, -80(%rbp)
	movapd	%xmm1, %xmm0
	movsd	%xmm3, -96(%rbp)
	movsd	%xmm1, -88(%rbp)
	movq	$0, -64(%rbp)
	movl	$0, -56(%rbp)
	call	__sprintf_chk@PLT
	xorl	%esi, %esi
	leaq	-62(%rbp), %rdi
	movl	$10, %edx
	call	strtol@PLT
	movq	.LC3(%rip), %rsi
	cmpb	$48, -64(%rbp)
	movsd	-88(%rbp), %xmm1
	movsd	-96(%rbp), %xmm3
	movq	.LC2(%rip), %xmm2
	movq	%rsi, %xmm4
	jne	.L1798
	cmpb	$48, -65(%rbp)
	jne	.L1799
	cmpb	$48, -66(%rbp)
	jne	.L1800
	cmpb	$48, -67(%rbp)
	jne	.L1801
	cmpb	$48, -68(%rbp)
	jne	.L1802
	cmpb	$48, -69(%rbp)
	jne	.L1803
	cmpb	$48, -70(%rbp)
	jne	.L1804
	cmpb	$48, -71(%rbp)
	jne	.L1805
	cmpb	$48, -72(%rbp)
	jne	.L1806
	cmpb	$48, -73(%rbp)
	jne	.L1807
	cmpb	$48, -74(%rbp)
	jne	.L1808
	cmpb	$48, -75(%rbp)
	jne	.L1809
	cmpb	$48, -76(%rbp)
	jne	.L1810
	cmpb	$48, -77(%rbp)
	jne	.L1811
	cmpb	$48, -78(%rbp)
	jne	.L1812
	xorl	%edx, %edx
	cmpb	$48, -79(%rbp)
	sete	%dl
	negl	%edx
	.p2align 4,,10
	.p2align 3
.L1785:
	subl	%eax, %edx
	movl	%edx, %r13d
	je	.L1774
.L1778:
	movapd	%xmm3, %xmm0
	movapd	%xmm3, %xmm5
	andpd	%xmm2, %xmm0
	ucomisd	%xmm0, %xmm4
	jbe	.L1788
	cvttsd2siq	%xmm3, %rax
	pxor	%xmm0, %xmm0
	movsd	.LC8(%rip), %xmm5
	andnpd	%xmm3, %xmm2
	cvtsi2sdq	%rax, %xmm0
	movapd	%xmm0, %xmm4
	cmpnlesd	%xmm3, %xmm4
	andpd	%xmm5, %xmm4
	movapd	%xmm0, %xmm5
	subsd	%xmm4, %xmm5
	orpd	%xmm2, %xmm5
.L1788:
	ucomisd	%xmm5, %xmm3
	jp	.L1817
	jne	.L1817
.L1774:
	xorl	%r12d, %r12d
.L1787:
	pxor	%xmm0, %xmm0
	movsd	%xmm1, 16(%rbx)
	comisd	%xmm3, %xmm0
	movapd	%xmm1, %xmm0
	seta	57(%rbx)
	call	uprv_isNaN_67@PLT
	movsd	16(%rbx), %xmm0
	movb	%al, 58(%rbx)
	call	uprv_isInfinite_67@PLT
	movb	%al, 59(%rbx)
	cmpw	$0, 58(%rbx)
	je	.L1790
	movq	$0, 48(%rbx)
	movb	$0, 56(%rbx)
	movl	$0, 24(%rbx)
	movq	$0, 32(%rbx)
.L1791:
	movq	$0, 40(%rbx)
.L1770:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1828
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1822:
	.cfi_restore_state
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm5, %xmm5
	andnpd	%xmm0, %xmm7
	movsd	.LC8(%rip), %xmm8
	cvtsi2sdq	%rax, %xmm5
	movapd	%xmm5, %xmm6
	cmpnlesd	%xmm0, %xmm6
	andpd	%xmm8, %xmm6
	subsd	%xmm6, %xmm5
	movapd	%xmm5, %xmm6
	orpd	%xmm7, %xmm6
	jmp	.L1775
	.p2align 4,,10
	.p2align 3
.L1824:
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm5, %xmm5
	andnpd	%xmm0, %xmm7
	movsd	.LC8(%rip), %xmm8
	cvtsi2sdq	%rax, %xmm5
	movapd	%xmm5, %xmm6
	cmpnlesd	%xmm0, %xmm6
	andpd	%xmm8, %xmm6
	subsd	%xmm6, %xmm5
	movapd	%xmm5, %xmm6
	orpd	%xmm7, %xmm6
	jmp	.L1779
	.p2align 4,,10
	.p2align 3
.L1826:
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm5, %xmm5
	andnpd	%xmm0, %xmm7
	movsd	.LC8(%rip), %xmm8
	cvtsi2sdq	%rax, %xmm5
	movapd	%xmm5, %xmm6
	cmpnlesd	%xmm0, %xmm6
	andpd	%xmm8, %xmm6
	subsd	%xmm6, %xmm5
	movapd	%xmm5, %xmm6
	orpd	%xmm7, %xmm6
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1802:
	movl	$11, %edx
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1790:
	movsd	16(%rbx), %xmm1
	pxor	%xmm0, %xmm0
	movl	$0, %edx
	movl	%r13d, 24(%rbx)
	movq	%r12, 32(%rbx)
	cvttsd2siq	%xmm1, %rax
	cvtsi2sdq	%rax, %xmm0
	movq	%rax, 48(%rbx)
	ucomisd	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	movb	%al, 56(%rbx)
	testq	%r12, %r12
	je	.L1791
	movabsq	$-3689348814741910323, %rsi
	movq	%r12, %rax
	movabsq	$1844674407370955160, %rcx
	imulq	%rsi, %rax
	addq	%rcx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	ja	.L1794
	movabsq	$7378697629483820647, %rdi
	.p2align 4,,10
	.p2align 3
.L1795:
	movq	%r12, %rax
	sarq	$63, %r12
	imulq	%rdi
	sarq	$2, %rdx
	subq	%r12, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r12
	imulq	%rsi, %rax
	addq	%rcx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	jbe	.L1795
.L1794:
	movq	%r12, 40(%rbx)
	jmp	.L1770
	.p2align 4,,10
	.p2align 3
.L1817:
	movapd	%xmm3, %xmm0
	movsd	%xmm1, -96(%rbp)
	movsd	%xmm3, -88(%rbp)
	call	uprv_isNaN_67@PLT
	movsd	-88(%rbp), %xmm3
	movsd	-96(%rbp), %xmm1
	testb	%al, %al
	jne	.L1774
	movapd	%xmm3, %xmm0
	call	uprv_isPositiveInfinity_67@PLT
	movsd	-88(%rbp), %xmm3
	movsd	-96(%rbp), %xmm1
	testb	%al, %al
	jne	.L1774
	movapd	%xmm3, %xmm0
	movl	%r13d, %edi
	call	_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi.part.0
	movsd	-96(%rbp), %xmm1
	movsd	-88(%rbp), %xmm3
	movq	%rax, %r12
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L1798:
	movl	$15, %edx
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1799:
	movl	$14, %edx
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1800:
	movl	$13, %edx
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1821:
	movl	$0, %r13d
	je	.L1774
	jmp	.L1772
	.p2align 4,,10
	.p2align 3
.L1801:
	movl	$12, %edx
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1823:
	movl	$1, %r13d
	je	.L1778
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L1825:
	movl	$2, %r13d
	je	.L1778
	jmp	.L1815
	.p2align 4,,10
	.p2align 3
.L1827:
	movl	$3, %r13d
	je	.L1778
	jmp	.L1783
	.p2align 4,,10
	.p2align 3
.L1803:
	movl	$10, %edx
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1810:
	movl	$3, %edx
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1804:
	movl	$9, %edx
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1805:
	movl	$8, %edx
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1806:
	movl	$7, %edx
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1807:
	movl	$6, %edx
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1808:
	movl	$5, %edx
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1809:
	movl	$4, %edx
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1811:
	movl	$2, %edx
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1812:
	movl	$1, %edx
	jmp	.L1785
.L1828:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3919:
	.size	_ZN6icu_6712FixedDecimal4initEd, .-_ZN6icu_6712FixedDecimal4initEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FixedDecimal4initEdil
	.type	_ZN6icu_6712FixedDecimal4initEdil, @function
_ZN6icu_6712FixedDecimal4initEdil:
.LFB3920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	comisd	%xmm0, %xmm1
	andpd	.LC2(%rip), %xmm0
	movsd	%xmm0, 16(%rdi)
	seta	57(%rdi)
	call	uprv_isNaN_67@PLT
	movsd	16(%r12), %xmm0
	movb	%al, 58(%r12)
	call	uprv_isInfinite_67@PLT
	movb	%al, 59(%r12)
	cmpw	$0, 58(%r12)
	je	.L1830
	movq	$0, 48(%r12)
	movb	$0, 56(%r12)
	movl	$0, 24(%r12)
	movq	$0, 32(%r12)
.L1831:
	movq	$0, 40(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1830:
	.cfi_restore_state
	movsd	16(%r12), %xmm1
	pxor	%xmm0, %xmm0
	movl	$0, %edx
	movl	%r13d, 24(%r12)
	movq	%rbx, 32(%r12)
	cvttsd2siq	%xmm1, %rax
	cvtsi2sdq	%rax, %xmm0
	movq	%rax, 48(%r12)
	ucomisd	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	movb	%al, 56(%r12)
	testq	%rbx, %rbx
	je	.L1831
	movabsq	$-3689348814741910323, %rsi
	movq	%rbx, %rax
	movabsq	$1844674407370955160, %rcx
	imulq	%rsi, %rax
	addq	%rcx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	ja	.L1834
	movabsq	$7378697629483820647, %rdi
	.p2align 4,,10
	.p2align 3
.L1835:
	movq	%rbx, %rax
	sarq	$63, %rbx
	imulq	%rdi
	sarq	$2, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rax
	movq	%rdx, %rbx
	imulq	%rsi, %rax
	addq	%rcx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	jbe	.L1835
.L1834:
	movq	%rbx, 40(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3920:
	.size	_ZN6icu_6712FixedDecimal4initEdil, .-_ZN6icu_6712FixedDecimal4initEdil
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FixedDecimal9quickInitEd
	.type	_ZN6icu_6712FixedDecimal9quickInitEd, @function
_ZN6icu_6712FixedDecimal9quickInitEd:
.LFB3921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	.LC2(%rip), %xmm1
	movsd	.LC3(%rip), %xmm3
	andpd	%xmm1, %xmm0
	movapd	%xmm0, %xmm2
	movapd	%xmm0, %xmm4
	andpd	%xmm1, %xmm2
	ucomisd	%xmm2, %xmm3
	jbe	.L1839
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm2, %xmm2
	movsd	.LC8(%rip), %xmm6
	movapd	%xmm1, %xmm5
	andnpd	%xmm0, %xmm5
	cvtsi2sdq	%rax, %xmm2
	movapd	%xmm2, %xmm4
	cmpnlesd	%xmm0, %xmm4
	andpd	%xmm6, %xmm4
	subsd	%xmm4, %xmm2
	movapd	%xmm2, %xmm4
	orpd	%xmm5, %xmm4
.L1839:
	ucomisd	%xmm4, %xmm0
	jnp	.L1871
.L1864:
	movsd	.LC4(%rip), %xmm2
	movapd	%xmm1, %xmm6
	mulsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm4
	movapd	%xmm2, %xmm5
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm3
	ja	.L1872
.L1842:
	ucomisd	%xmm5, %xmm2
	jnp	.L1873
.L1865:
	movsd	.LC6(%rip), %xmm2
	movapd	%xmm1, %xmm6
	mulsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm4
	movapd	%xmm2, %xmm5
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm3
	ja	.L1874
.L1845:
	ucomisd	%xmm5, %xmm2
	jnp	.L1875
.L1866:
	movsd	.LC7(%rip), %xmm2
	movapd	%xmm1, %xmm6
	mulsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm4
	movapd	%xmm2, %xmm5
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm3
	ja	.L1876
	ucomisd	%xmm2, %xmm5
	jnp	.L1877
.L1867:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1872:
	.cfi_restore_state
	cvttsd2siq	%xmm2, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC8(%rip), %xmm7
	andnpd	%xmm2, %xmm6
	cvtsi2sdq	%rax, %xmm4
	movapd	%xmm4, %xmm5
	cmpnlesd	%xmm2, %xmm5
	andpd	%xmm7, %xmm5
	subsd	%xmm5, %xmm4
	movapd	%xmm4, %xmm5
	orpd	%xmm6, %xmm5
	jmp	.L1842
	.p2align 4,,10
	.p2align 3
.L1874:
	cvttsd2siq	%xmm2, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC8(%rip), %xmm7
	andnpd	%xmm2, %xmm6
	cvtsi2sdq	%rax, %xmm4
	movapd	%xmm4, %xmm5
	cmpnlesd	%xmm2, %xmm5
	andpd	%xmm7, %xmm5
	subsd	%xmm5, %xmm4
	movapd	%xmm4, %xmm5
	orpd	%xmm6, %xmm5
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1876:
	cvttsd2siq	%xmm2, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC8(%rip), %xmm7
	andnpd	%xmm2, %xmm6
	cvtsi2sdq	%rax, %xmm4
	movapd	%xmm4, %xmm5
	cmpnlesd	%xmm2, %xmm5
	andpd	%xmm7, %xmm5
	subsd	%xmm5, %xmm4
	movapd	%xmm4, %xmm5
	orpd	%xmm6, %xmm5
	ucomisd	%xmm2, %xmm5
	jp	.L1867
.L1877:
	movl	$3, %r13d
	jne	.L1867
.L1843:
	movapd	%xmm0, %xmm4
	movapd	%xmm0, %xmm2
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm3
	jbe	.L1851
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm2, %xmm2
	andnpd	%xmm0, %xmm1
	cvtsi2sdq	%rax, %xmm2
	orpd	%xmm1, %xmm2
.L1851:
	ucomisd	%xmm2, %xmm0
	jp	.L1852
	jne	.L1852
.L1840:
	xorl	%ebx, %ebx
.L1854:
	movb	$0, 57(%r12)
	movsd	%xmm0, 16(%r12)
	call	uprv_isNaN_67@PLT
	movsd	16(%r12), %xmm0
	movb	%al, 58(%r12)
	call	uprv_isInfinite_67@PLT
	movb	%al, 59(%r12)
	cmpw	$0, 58(%r12)
	je	.L1855
	movq	$0, 48(%r12)
	movb	$0, 56(%r12)
	movl	$0, 24(%r12)
	movq	$0, 32(%r12)
.L1856:
	movq	$0, 40(%r12)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1852:
	.cfi_restore_state
	movsd	%xmm0, -40(%rbp)
	call	uprv_isNaN_67@PLT
	movsd	-40(%rbp), %xmm0
	testb	%al, %al
	jne	.L1840
	call	uprv_isPositiveInfinity_67@PLT
	movsd	-40(%rbp), %xmm0
	testb	%al, %al
	jne	.L1840
	movl	%r13d, %edi
	call	_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi.part.0
	movsd	-40(%rbp), %xmm0
	movq	%rax, %rbx
	jmp	.L1854
	.p2align 4,,10
	.p2align 3
.L1855:
	movsd	16(%r12), %xmm1
	pxor	%xmm0, %xmm0
	movl	$0, %edx
	movl	%r13d, 24(%r12)
	movq	%rbx, 32(%r12)
	cvttsd2siq	%xmm1, %rax
	cvtsi2sdq	%rax, %xmm0
	movq	%rax, 48(%r12)
	ucomisd	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	movb	%al, 56(%r12)
	testq	%rbx, %rbx
	je	.L1856
	movabsq	$-3689348814741910323, %rsi
	movq	%rbx, %rax
	movabsq	$1844674407370955160, %rcx
	imulq	%rsi, %rax
	addq	%rcx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	ja	.L1858
	movabsq	$7378697629483820647, %rdi
	.p2align 4,,10
	.p2align 3
.L1859:
	movq	%rbx, %rax
	sarq	$63, %rbx
	imulq	%rdi
	sarq	$2, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rax
	movq	%rdx, %rbx
	imulq	%rsi, %rax
	addq	%rcx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	jbe	.L1859
.L1858:
	movq	%rbx, 40(%r12)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1871:
	.cfi_restore_state
	movl	$0, %r13d
	je	.L1840
	jmp	.L1864
	.p2align 4,,10
	.p2align 3
.L1873:
	movl	$1, %r13d
	je	.L1843
	jmp	.L1865
	.p2align 4,,10
	.p2align 3
.L1875:
	movl	$2, %r13d
	je	.L1843
	jmp	.L1866
	.cfi_endproc
.LFE3921:
	.size	_ZN6icu_6712FixedDecimal9quickInitEd, .-_ZN6icu_6712FixedDecimal9quickInitEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FixedDecimal8decimalsEd
	.type	_ZN6icu_6712FixedDecimal8decimalsEd, @function
_ZN6icu_6712FixedDecimal8decimalsEd:
.LFB3922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	.LC2(%rip), %xmm1
	movsd	.LC3(%rip), %xmm3
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	andpd	%xmm1, %xmm0
	movapd	%xmm0, %xmm2
	movapd	%xmm0, %xmm4
	andpd	%xmm1, %xmm2
	ucomisd	%xmm2, %xmm3
	jbe	.L1879
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm2, %xmm2
	movsd	.LC8(%rip), %xmm6
	movapd	%xmm1, %xmm5
	andnpd	%xmm0, %xmm5
	cvtsi2sdq	%rax, %xmm2
	movapd	%xmm2, %xmm4
	cmpnlesd	%xmm0, %xmm4
	andpd	%xmm6, %xmm4
	subsd	%xmm4, %xmm2
	movapd	%xmm2, %xmm4
	orpd	%xmm5, %xmm4
.L1879:
	ucomisd	%xmm4, %xmm0
	jnp	.L1917
.L1911:
	movsd	.LC4(%rip), %xmm2
	movapd	%xmm1, %xmm6
	mulsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm4
	movapd	%xmm2, %xmm5
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm3
	ja	.L1918
.L1882:
	ucomisd	%xmm5, %xmm2
	jnp	.L1919
.L1912:
	movsd	.LC6(%rip), %xmm2
	movapd	%xmm1, %xmm6
	mulsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm4
	movapd	%xmm2, %xmm5
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm3
	ja	.L1920
.L1884:
	ucomisd	%xmm5, %xmm2
	jnp	.L1921
.L1913:
	movsd	.LC7(%rip), %xmm2
	mulsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm4
	movapd	%xmm2, %xmm5
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm3
	ja	.L1922
.L1886:
	ucomisd	%xmm2, %xmm5
	jnp	.L1923
.L1887:
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	leaq	-48(%rbp), %rdi
	movl	$30, %edx
	movw	%ax, -20(%rbp)
	leaq	.LC12(%rip), %rcx
	movl	$1, %esi
	movl	$1, %eax
	movaps	%xmm1, -48(%rbp)
	movq	$0, -32(%rbp)
	movl	$0, -24(%rbp)
	call	__sprintf_chk@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	leaq	-30(%rbp), %rdi
	call	strtol@PLT
	cmpb	$48, -32(%rbp)
	movq	%rax, %rdx
	jne	.L1896
	cmpb	$48, -33(%rbp)
	jne	.L1897
	cmpb	$48, -34(%rbp)
	jne	.L1898
	cmpb	$48, -35(%rbp)
	jne	.L1899
	cmpb	$48, -36(%rbp)
	jne	.L1900
	cmpb	$48, -37(%rbp)
	jne	.L1901
	cmpb	$48, -38(%rbp)
	jne	.L1902
	cmpb	$48, -39(%rbp)
	jne	.L1903
	cmpb	$48, -40(%rbp)
	jne	.L1904
	cmpb	$48, -41(%rbp)
	jne	.L1905
	cmpb	$48, -42(%rbp)
	jne	.L1906
	cmpb	$48, -43(%rbp)
	jne	.L1907
	cmpb	$48, -44(%rbp)
	jne	.L1908
	cmpb	$48, -45(%rbp)
	jne	.L1909
	cmpb	$48, -46(%rbp)
	jne	.L1910
	xorl	%eax, %eax
	cmpb	$48, -47(%rbp)
	sete	%al
	negl	%eax
	.p2align 4,,10
	.p2align 3
.L1890:
	subl	%edx, %eax
.L1878:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1924
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1918:
	.cfi_restore_state
	cvttsd2siq	%xmm2, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC8(%rip), %xmm7
	andnpd	%xmm2, %xmm6
	cvtsi2sdq	%rax, %xmm4
	movapd	%xmm4, %xmm5
	cmpnlesd	%xmm2, %xmm5
	andpd	%xmm7, %xmm5
	subsd	%xmm5, %xmm4
	movapd	%xmm4, %xmm5
	orpd	%xmm6, %xmm5
	jmp	.L1882
	.p2align 4,,10
	.p2align 3
.L1920:
	cvttsd2siq	%xmm2, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC8(%rip), %xmm7
	andnpd	%xmm2, %xmm6
	cvtsi2sdq	%rax, %xmm4
	movapd	%xmm4, %xmm5
	cmpnlesd	%xmm2, %xmm5
	andpd	%xmm7, %xmm5
	subsd	%xmm5, %xmm4
	movapd	%xmm4, %xmm5
	orpd	%xmm6, %xmm5
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L1922:
	cvttsd2siq	%xmm2, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC8(%rip), %xmm5
	andnpd	%xmm2, %xmm1
	cvtsi2sdq	%rax, %xmm4
	movapd	%xmm4, %xmm3
	cmpnlesd	%xmm2, %xmm3
	andpd	%xmm5, %xmm3
	movapd	%xmm4, %xmm5
	subsd	%xmm3, %xmm5
	orpd	%xmm1, %xmm5
	jmp	.L1886
.L1900:
	movl	$11, %eax
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L1896:
	movl	$15, %eax
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L1897:
	movl	$14, %eax
	jmp	.L1890
.L1898:
	movl	$13, %eax
	jmp	.L1890
.L1917:
	movl	$0, %eax
	je	.L1878
	jmp	.L1911
.L1899:
	movl	$12, %eax
	jmp	.L1890
.L1919:
	movl	$1, %eax
	je	.L1878
	jmp	.L1912
.L1921:
	movl	$2, %eax
	je	.L1878
	jmp	.L1913
.L1923:
	movl	$3, %eax
	je	.L1878
	jmp	.L1887
.L1901:
	movl	$10, %eax
	jmp	.L1890
.L1908:
	movl	$3, %eax
	jmp	.L1890
.L1902:
	movl	$9, %eax
	jmp	.L1890
.L1903:
	movl	$8, %eax
	jmp	.L1890
.L1904:
	movl	$7, %eax
	jmp	.L1890
.L1905:
	movl	$6, %eax
	jmp	.L1890
.L1906:
	movl	$5, %eax
	jmp	.L1890
.L1907:
	movl	$4, %eax
	jmp	.L1890
.L1909:
	movl	$2, %eax
	jmp	.L1890
.L1910:
	movl	$1, %eax
	jmp	.L1890
.L1924:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3922:
	.size	_ZN6icu_6712FixedDecimal8decimalsEd, .-_ZN6icu_6712FixedDecimal8decimalsEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi
	.type	_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi, @function
_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi:
.LFB3923:
	.cfi_startproc
	endbr64
	testl	%edi, %edi
	je	.L1933
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm2
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	subq	$24, %rsp
	movsd	.LC2(%rip), %xmm3
	movsd	.LC3(%rip), %xmm4
	andpd	%xmm3, %xmm1
	ucomisd	%xmm1, %xmm4
	ja	.L1936
	ucomisd	%xmm0, %xmm2
	jp	.L1931
.L1937:
	jne	.L1931
.L1927:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1936:
	.cfi_restore_state
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC8(%rip), %xmm4
	andnpd	%xmm0, %xmm3
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm2
	cmpnlesd	%xmm0, %xmm2
	andpd	%xmm4, %xmm2
	subsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm2
	orpd	%xmm3, %xmm2
	ucomisd	%xmm0, %xmm2
	jnp	.L1937
.L1931:
	movsd	%xmm0, -24(%rbp)
	call	uprv_isNaN_67@PLT
	testb	%al, %al
	jne	.L1927
	movsd	-24(%rbp), %xmm0
	call	uprv_isPositiveInfinity_67@PLT
	testb	%al, %al
	jne	.L1927
	movsd	-24(%rbp), %xmm0
	addq	$24, %rsp
	movl	%r12d, %edi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi.part.0
	.p2align 4,,10
	.p2align 3
.L1933:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3923:
	.size	_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi, .-_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FixedDecimal26adjustForMinFractionDigitsEi
	.type	_ZN6icu_6712FixedDecimal26adjustForMinFractionDigitsEi, @function
_ZN6icu_6712FixedDecimal26adjustForMinFractionDigitsEi:
.LFB3924:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	subl	24(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L1938
	movabsq	$99999999999999999, %r8
	movq	32(%rdi), %rax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1942:
	cmpq	%r8, %rax
	jg	.L1941
	leaq	(%rax,%rax,4), %rax
	addl	$1, %edx
	addq	%rax, %rax
	movq	%rax, 32(%rdi)
	cmpl	%edx, %ecx
	jne	.L1942
.L1941:
	movl	%esi, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1938:
	ret
	.cfi_endproc
.LFE3924:
	.size	_ZN6icu_6712FixedDecimal26adjustForMinFractionDigitsEi, .-_ZN6icu_6712FixedDecimal26adjustForMinFractionDigitsEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712FixedDecimal15isNanOrInfinityEv
	.type	_ZNK6icu_6712FixedDecimal15isNanOrInfinityEv, @function
_ZNK6icu_6712FixedDecimal15isNanOrInfinityEv:
.LFB3929:
	.cfi_startproc
	endbr64
	cmpw	$0, 58(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE3929:
	.size	_ZNK6icu_6712FixedDecimal15isNanOrInfinityEv, .-_ZNK6icu_6712FixedDecimal15isNanOrInfinityEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712FixedDecimal28getVisibleFractionDigitCountEv
	.type	_ZNK6icu_6712FixedDecimal28getVisibleFractionDigitCountEv, @function
_ZNK6icu_6712FixedDecimal28getVisibleFractionDigitCountEv:
.LFB3930:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE3930:
	.size	_ZNK6icu_6712FixedDecimal28getVisibleFractionDigitCountEv, .-_ZNK6icu_6712FixedDecimal28getVisibleFractionDigitCountEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6733PluralAvailableLocalesEnumerationC2ER10UErrorCode
	.type	_ZN6icu_6733PluralAvailableLocalesEnumerationC2ER10UErrorCode, @function
_ZN6icu_6733PluralAvailableLocalesEnumerationC2ER10UErrorCode:
.LFB3932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	leaq	16+_ZTVN6icu_6733PluralAvailableLocalesEnumerationE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rbx)
	movl	(%r12), %eax
	movups	%xmm0, 120(%rbx)
	testl	%eax, %eax
	jle	.L1946
	movl	%eax, 116(%rbx)
.L1945:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1946:
	.cfi_restore_state
	movl	$0, 116(%rbx)
	leaq	116(%rbx), %r13
	leaq	.LC10(%rip), %rsi
	xorl	%edi, %edi
	movq	%r13, %rdx
	call	ures_openDirect_67@PLT
	movq	%r13, %rcx
	xorl	%edx, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ures_getByKey_67@PLT
	movq	%rax, 120(%rbx)
	testq	%r12, %r12
	je	.L1945
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ures_close_67@PLT
	.cfi_endproc
.LFE3932:
	.size	_ZN6icu_6733PluralAvailableLocalesEnumerationC2ER10UErrorCode, .-_ZN6icu_6733PluralAvailableLocalesEnumerationC2ER10UErrorCode
	.globl	_ZN6icu_6733PluralAvailableLocalesEnumerationC1ER10UErrorCode
	.set	_ZN6icu_6733PluralAvailableLocalesEnumerationC1ER10UErrorCode,_ZN6icu_6733PluralAvailableLocalesEnumerationC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE12createObjectEPKvR10UErrorCode
	.type	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE12createObjectEPKvR10UErrorCode, @function
_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE12createObjectEPKvR10UErrorCode:
.LFB3808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-272(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$240, %rsp
	movq	56(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L1954
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6711PluralRules17internalForLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode.part.0
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1985
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L1970
.L1965:
	movq	(%r12), %rax
	leaq	_ZN6icu_6711PluralRulesD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1966
	movq	8(%r12), %r13
	leaq	16+_ZTVN6icu_6711PluralRulesE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1967
	movq	0(%r13), %rax
	leaq	_ZN6icu_679RuleChainD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1968
	call	_ZN6icu_679RuleChainD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1967:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1984:
	xorl	%r13d, %r13d
.L1953:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1986
	addq	$240, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1985:
	.cfi_restore_state
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L1954
	movl	$7, (%rbx)
.L1954:
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1984
	xorl	%r12d, %r12d
.L1970:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1956
	movl	(%rbx), %esi
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6717SharedPluralRulesE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	%r12, 24(%r13)
	testl	%esi, %esi
	jle	.L1987
	testq	%r12, %r12
	je	.L1961
	movq	(%r12), %rax
	leaq	_ZN6icu_6711PluralRulesD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1962
	movq	8(%r12), %r14
	leaq	16+_ZTVN6icu_6711PluralRulesE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1963
	movq	(%r14), %rax
	leaq	_ZN6icu_679RuleChainD0Ev(%rip), %rdx
	movq	%r14, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1964
	call	_ZN6icu_679RuleChainD1Ev
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1963:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1961:
	movq	%r13, %rdi
	call	_ZN6icu_6712SharedObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1959:
	testq	%r12, %r12
	jne	.L1965
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L1987:
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	jmp	.L1953
.L1956:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1959
	movl	$7, (%rbx)
	jmp	.L1959
	.p2align 4,,10
	.p2align 3
.L1966:
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	*%rax
	jmp	.L1953
	.p2align 4,,10
	.p2align 3
.L1968:
	call	*%rax
	jmp	.L1967
	.p2align 4,,10
	.p2align 3
.L1962:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1961
	.p2align 4,,10
	.p2align 3
.L1964:
	call	*%rax
	jmp	.L1963
.L1986:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3808:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE12createObjectEPKvR10UErrorCode, .-_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE12createObjectEPKvR10UErrorCode
	.section	.text._ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED2Ev,"axG",@progbits,_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED2Ev
	.type	_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED2Ev, @function
_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED2Ev:
.LFB4784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_17SharedPluralRulesEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	.cfi_endproc
.LFE4784:
	.size	_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED2Ev, .-_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED2Ev
	.weak	_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED1Ev
	.set	_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED1Ev,_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode
	.type	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode, @function
_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode:
.LFB3809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L2008
	movq	%rdx, %rbx
	testl	%esi, %esi
	jne	.L2010
	movq	%rdi, %r12
	movq	%rdx, %rdi
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%rbx), %edi
	movq	%rax, %r15
	testl	%edi, %edi
	jle	.L2011
.L2008:
	xorl	%r12d, %r12d
.L1990:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2012
	addq	$280, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2011:
	.cfi_restore_state
	leaq	-288(%rbp), %r13
	movq	%r12, %rsi
	xorl	%r12d, %r12d
	movl	$0, -296(%rbp)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE(%rip), %rax
	movq	%r13, %rdi
	movb	$0, -292(%rbp)
	leaq	-304(%rbp), %r14
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L2013
.L1995:
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_17SharedPluralRulesEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	jmp	.L1990
	.p2align 4,,10
	.p2align 3
.L2010:
	movl	$16, (%rdx)
	jmp	.L2008
	.p2align 4,,10
	.p2align 3
.L2013:
	leaq	-312(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	-316(%rbp), %r8
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %eax
	movq	-312(%rbp), %r12
	testl	%eax, %eax
	jle	.L2014
	testq	%r12, %r12
	jne	.L2015
.L1999:
	movl	%eax, (%rbx)
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L2015:
	movq	%r12, %r15
	xorl	%r12d, %r12d
.L1998:
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movl	(%rbx), %ecx
	movl	-316(%rbp), %eax
	testl	%ecx, %ecx
	je	.L1999
	testl	%eax, %eax
	jle	.L1995
	jmp	.L1999
	.p2align 4,,10
	.p2align 3
.L2014:
	testq	%r12, %r12
	je	.L1997
	movq	%r12, %rdi
	movq	%r12, %r15
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	jmp	.L1998
.L1997:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jne	.L1995
	movl	%eax, (%rbx)
	jmp	.L1995
.L2012:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3809:
	.size	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode, .-_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode
	.section	.text._ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED0Ev,"axG",@progbits,_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED0Ev
	.type	_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED0Ev, @function
_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED0Ev:
.LFB4786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_17SharedPluralRulesEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4786:
	.size	_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED0Ev, .-_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode.part.0, @function
_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode.part.0:
.LFB5399:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.L2035
.L2018:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2036
	addq	$296, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2035:
	.cfi_restore_state
	leaq	-288(%rbp), %r13
	movq	%r12, %rsi
	movq	%rax, -328(%rbp)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE(%rip), %rax
	movq	%r13, %rdi
	movb	$0, -292(%rbp)
	leaq	-304(%rbp), %r15
	movl	$0, -296(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%rbx), %esi
	movq	-328(%rbp), %r9
	testl	%esi, %esi
	jle	.L2037
.L2020:
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_17SharedPluralRulesEEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	jmp	.L2018
	.p2align 4,,10
	.p2align 3
.L2037:
	leaq	-312(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r9, %rdi
	leaq	-316(%rbp), %r8
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %edx
	movq	-312(%rbp), %r14
	testl	%edx, %edx
	jle	.L2038
	testq	%r14, %r14
	je	.L2024
	movq	%r14, %r12
	xorl	%r14d, %r14d
.L2023:
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movl	(%rbx), %ecx
	movl	-316(%rbp), %edx
	testl	%ecx, %ecx
	je	.L2024
	testl	%edx, %edx
	jle	.L2020
	.p2align 4,,10
	.p2align 3
.L2024:
	movl	%edx, (%rbx)
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2038:
	testq	%r14, %r14
	je	.L2022
	movq	%r14, %rdi
	movq	%r14, %r12
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	jmp	.L2023
	.p2align 4,,10
	.p2align 3
.L2022:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L2020
	movl	%edx, (%rbx)
	jmp	.L2020
.L2036:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5399:
	.size	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode.part.0, .-_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode
	.type	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode, @function
_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode:
.LFB3811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	(%rdx), %eax
	testl	%esi, %esi
	je	.L2040
	testl	%eax, %eax
	jg	.L2046
	cmpl	$1, %esi
	jg	.L2047
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711PluralRules17internalForLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L2047:
	.cfi_restore_state
	movl	$1, (%rdx)
.L2046:
	xorl	%r13d, %r13d
.L2039:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2040:
	.cfi_restore_state
	testl	%eax, %eax
	jg	.L2046
	movq	%rdx, %rsi
	call	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode.part.0
	movq	%rax, %r14
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L2046
	movq	24(%r14), %rdi
	call	_ZNK6icu_6711PluralRules5cloneEv
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	testq	%r13, %r13
	jne	.L2039
	movl	$7, (%r12)
	jmp	.L2039
	.cfi_endproc
.LFE3811:
	.size	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode, .-_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode:
.LFB3810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L2051
	movq	%rsi, %rbx
	call	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode.part.0
	movq	%rax, %r13
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L2051
	movq	24(%r13), %rdi
	call	_ZNK6icu_6711PluralRules5cloneEv
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	testq	%r12, %r12
	je	.L2053
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2051:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2053:
	.cfi_restore_state
	movl	$7, (%rbx)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3810:
	.size	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711PluralRules6selectERKNS_6number15FormattedNumberER10UErrorCode
	.type	_ZNK6icu_6711PluralRules6selectERKNS_6number15FormattedNumberER10UErrorCode, @function
_ZNK6icu_6711PluralRules6selectERKNS_6number15FormattedNumberER10UErrorCode:
.LFB3815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_676number15FormattedNumber18getDecimalQuantityERNS0_4impl15DecimalQuantityER10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L2055
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L2056:
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2060
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2055:
	.cfi_restore_state
	movq	8(%r14), %rsi
	testq	%rsi, %rsi
	je	.L2061
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZNK6icu_679RuleChain6selectERKNS_13IFixedDecimalE
	jmp	.L2056
	.p2align 4,,10
	.p2align 3
.L2061:
	leaq	_ZN6icu_67L19PLURAL_DEFAULT_RULEE(%rip), %rax
	leaq	-136(%rbp), %rdx
	movl	$-1, %ecx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	jmp	.L2056
.L2060:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3815:
	.size	_ZNK6icu_6711PluralRules6selectERKNS_6number15FormattedNumberER10UErrorCode, .-_ZNK6icu_6711PluralRules6selectERKNS_6number15FormattedNumberER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712FixedDecimalC2ERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712FixedDecimalC2ERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712FixedDecimalC2ERKNS_13UnicodeStringER10UErrorCode:
.LFB3910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-192(%rbp), %r14
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$192, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	movl	$0, -56(%rbp)
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm6
	leaq	-99(%rbp), %rax
	movl	$40, -104(%rbp)
	movq	%rsi, %xmm0
	movq	%rax, -112(%rbp)
	movq	%r12, %rsi
	xorl	%eax, %eax
	punpcklqdq	%xmm6, %xmm0
	movw	%ax, -100(%rbp)
	movups	%xmm0, (%rdi)
	leaq	-112(%rbp), %rdi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movl	-56(%rbp), %edx
	movq	-112(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14setToDecNumberENS_11StringPieceER10UErrorCode@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L2092
	movswl	8(%r12), %ecx
	testw	%cx, %cx
	js	.L2067
	sarl	$5, %ecx
.L2068:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$46, %esi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv@PLT
	cmpl	$-1, %r13d
	je	.L2093
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	js	.L2072
	movswl	%dx, %eax
	sarl	$5, %eax
	subl	%r13d, %eax
	subl	$1, %eax
	movl	%eax, %r13d
	je	.L2094
.L2074:
	movq	.LC2(%rip), %xmm2
	movsd	.LC3(%rip), %xmm5
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm3
	andpd	%xmm2, %xmm1
	movapd	%xmm2, %xmm4
	ucomisd	%xmm1, %xmm5
	ja	.L2095
	ucomisd	%xmm3, %xmm0
	jp	.L2088
.L2097:
	jne	.L2088
.L2077:
	xorl	%r12d, %r12d
.L2075:
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	andpd	%xmm2, %xmm0
	movsd	%xmm0, 16(%rbx)
	seta	57(%rbx)
	call	uprv_isNaN_67@PLT
	movsd	16(%rbx), %xmm0
	movb	%al, 58(%rbx)
	call	uprv_isInfinite_67@PLT
	movb	%al, 59(%rbx)
	cmpw	$0, 58(%rbx)
	je	.L2079
	movq	$0, 48(%rbx)
	movb	$0, 56(%rbx)
	movl	$0, 24(%rbx)
	movq	$0, 32(%rbx)
.L2080:
	movq	$0, 40(%rbx)
.L2071:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	cmpb	$0, -100(%rbp)
	jne	.L2091
.L2062:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2096
	addq	$192, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2067:
	.cfi_restore_state
	movl	12(%r12), %ecx
	jmp	.L2068
	.p2align 4,,10
	.p2align 3
.L2092:
	movb	$0, 57(%rbx)
	pxor	%xmm0, %xmm0
	movq	$0x000000000, 16(%rbx)
	call	uprv_isNaN_67@PLT
	movsd	16(%rbx), %xmm0
	movb	%al, 58(%rbx)
	call	uprv_isInfinite_67@PLT
	movb	%al, 59(%rbx)
	cmpw	$0, 58(%rbx)
	jne	.L2086
	movsd	16(%rbx), %xmm0
	pxor	%xmm1, %xmm1
	movl	$0, %ecx
	cvttsd2siq	%xmm0, %rdx
	cvtsi2sdq	%rdx, %xmm1
	ucomisd	%xmm0, %xmm1
	setnp	%al
	cmovne	%ecx, %eax
.L2064:
	movb	%al, 56(%rbx)
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	%rdx, 48(%rbx)
	movl	$0, 24(%rbx)
	movups	%xmm0, 32(%rbx)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	cmpb	$0, -100(%rbp)
	je	.L2062
.L2091:
	movq	-112(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2072:
	movl	12(%r12), %eax
	subl	%r13d, %eax
	subl	$1, %eax
	movl	%eax, %r13d
	jne	.L2074
.L2094:
	movq	.LC2(%rip), %xmm2
	jmp	.L2077
	.p2align 4,,10
	.p2align 3
.L2095:
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC8(%rip), %xmm5
	andnpd	%xmm0, %xmm4
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm3
	cmpnlesd	%xmm0, %xmm3
	andpd	%xmm5, %xmm3
	subsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm3
	orpd	%xmm4, %xmm3
	ucomisd	%xmm3, %xmm0
	jnp	.L2097
.L2088:
	movaps	%xmm2, -224(%rbp)
	movsd	%xmm0, -200(%rbp)
	call	uprv_isNaN_67@PLT
	movsd	-200(%rbp), %xmm0
	movapd	-224(%rbp), %xmm2
	testb	%al, %al
	jne	.L2077
	call	uprv_isPositiveInfinity_67@PLT
	movsd	-200(%rbp), %xmm0
	movapd	-224(%rbp), %xmm2
	testb	%al, %al
	jne	.L2077
	movl	%r13d, %edi
	call	_ZN6icu_6712FixedDecimal19getFractionalDigitsEdi.part.0
	movapd	-224(%rbp), %xmm2
	movsd	-200(%rbp), %xmm0
	movq	%rax, %r12
	jmp	.L2075
	.p2align 4,,10
	.p2align 3
.L2086:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L2064
	.p2align 4,,10
	.p2align 3
.L2079:
	movsd	16(%rbx), %xmm1
	pxor	%xmm0, %xmm0
	movl	$0, %edx
	movl	%r13d, 24(%rbx)
	movq	%r12, 32(%rbx)
	cvttsd2siq	%xmm1, %rax
	cvtsi2sdq	%rax, %xmm0
	movq	%rax, 48(%rbx)
	ucomisd	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	movb	%al, 56(%rbx)
	testq	%r12, %r12
	je	.L2080
	movabsq	$-3689348814741910323, %rsi
	movq	%r12, %rax
	movabsq	$1844674407370955160, %rcx
	imulq	%rsi, %rax
	addq	%rcx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	ja	.L2082
	movabsq	$7378697629483820647, %rdi
	.p2align 4,,10
	.p2align 3
.L2083:
	movq	%r12, %rax
	sarq	$63, %r12
	imulq	%rdi
	sarq	$2, %rdx
	subq	%r12, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r12
	imulq	%rsi, %rax
	addq	%rcx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	jbe	.L2083
.L2082:
	movq	%r12, 40(%rbx)
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2093:
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	andpd	.LC2(%rip), %xmm0
	movsd	%xmm0, 16(%rbx)
	seta	57(%rbx)
	call	uprv_isNaN_67@PLT
	movsd	16(%rbx), %xmm0
	movb	%al, 58(%rbx)
	call	uprv_isInfinite_67@PLT
	movb	%al, 59(%rbx)
	cmpw	$0, 58(%rbx)
	jne	.L2087
	movsd	16(%rbx), %xmm0
	pxor	%xmm1, %xmm1
	movl	$0, %ecx
	cvttsd2siq	%xmm0, %rdx
	cvtsi2sdq	%rdx, %xmm1
	ucomisd	%xmm0, %xmm1
	setnp	%al
	cmovne	%ecx, %eax
.L2070:
	pxor	%xmm0, %xmm0
	movb	%al, 56(%rbx)
	movq	%rdx, 48(%rbx)
	movl	$0, 24(%rbx)
	movups	%xmm0, 32(%rbx)
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2087:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L2070
.L2096:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3910:
	.size	_ZN6icu_6712FixedDecimalC2ERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712FixedDecimalC2ERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6712FixedDecimalC1ERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6712FixedDecimalC1ERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6712FixedDecimalC2ERKNS_13UnicodeStringER10UErrorCode
	.p2align 4
	.type	_ZN6icu_67L20getSamplesFromStringERKNS_13UnicodeStringEPdiR10UErrorCode, @function
_ZN6icu_67L20getSamplesFromStringERKNS_13UnicodeStringEPdiR10UErrorCode:
.LFB3821:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -344(%rbp)
	movl	%edx, -336(%rbp)
	movq	%rcx, -352(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L2137
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L2135
	.p2align 4,,10
	.p2align 3
.L2153:
	sarl	$5, %ecx
	cmpl	%r14d, %ecx
	jle	.L2098
.L2154:
	xorl	%edx, %edx
	testl	%r14d, %r14d
	js	.L2102
	movl	%r14d, %edx
	subl	%r14d, %ecx
.L2102:
	movl	$44, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %ebx
	cmpl	$-1, %eax
	je	.L2152
.L2103:
	leaq	-192(%rbp), %r13
	movl	%ebx, %ecx
	movl	%r14d, %edx
	movq	%r15, %rsi
	subl	%r14d, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movswl	-184(%rbp), %ecx
	testw	%cx, %cx
	js	.L2105
	sarl	$5, %ecx
.L2106:
	xorl	%edx, %edx
	movl	$126, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jns	.L2107
	movq	-352(%rbp), %rdx
	leaq	-256(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712FixedDecimalC1ERKNS_13UnicodeStringER10UErrorCode
	movl	-232(%rbp), %ecx
	movsd	-240(%rbp), %xmm0
	testl	%ecx, %ecx
	je	.L2108
	movsd	.LC35(%rip), %xmm1
	movsd	.LC3(%rip), %xmm5
	movapd	%xmm0, %xmm2
	andpd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm5
	jbe	.L2109
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC8(%rip), %xmm3
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm2
	cmpnlesd	%xmm0, %xmm2
	andpd	%xmm3, %xmm2
	subsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm2
	movsd	.LC35(%rip), %xmm1
	andnpd	%xmm0, %xmm1
	orpd	%xmm1, %xmm2
.L2109:
	ucomisd	%xmm0, %xmm2
	jp	.L2108
	je	.L2110
.L2108:
	movq	-344(%rbp), %rdx
	movslq	%r12d, %rax
	addl	$1, %r12d
	movsd	%xmm0, (%rdx,%rax,8)
.L2110:
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	leaq	-248(%rbp), %rdi
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm4
	movq	%rsi, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -256(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713IFixedDecimalD2Ev@PLT
.L2112:
	movq	%r13, %rdi
	leal	1(%rbx), %r14d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	-336(%rbp), %r12d
	jge	.L2098
.L2135:
	movswl	8(%r15), %ecx
	testw	%cx, %cx
	jns	.L2153
	movl	12(%r15), %ecx
	cmpl	%r14d, %ecx
	jg	.L2154
	.p2align 4,,10
	.p2align 3
.L2098:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2155
	addq	$344, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2105:
	.cfi_restore_state
	movl	-180(%rbp), %ecx
	jmp	.L2106
	.p2align 4,,10
	.p2align 3
.L2107:
	leaq	-128(%rbp), %r10
	movl	%eax, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -368(%rbp)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	-368(%rbp), %r10
	leaq	-320(%rbp), %r9
	movq	-352(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r9, -376(%rbp)
	movq	%r10, %rsi
	call	_ZN6icu_6712FixedDecimalC1ERKNS_13UnicodeStringER10UErrorCode
	movq	-368(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-368(%rbp), %r10
	leal	1(%r14), %edx
	movq	%r13, %rsi
	movl	$2147483647, %ecx
	leaq	-256(%rbp), %r14
	subl	%edx, %ecx
	movq	%r10, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	-368(%rbp), %r10
	movq	-352(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r10, %rsi
	call	_ZN6icu_6712FixedDecimalC1ERKNS_13UnicodeStringER10UErrorCode
	movq	-368(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-352(%rbp), %rax
	movsd	-304(%rbp), %xmm2
	movsd	-240(%rbp), %xmm8
	movq	-376(%rbp), %r9
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L2113
	comisd	%xmm8, %xmm2
	ja	.L2156
	movq	.LC8(%rip), %rax
	movsd	.LC3(%rip), %xmm5
	movapd	%xmm2, %xmm0
	movsd	.LC2(%rip), %xmm7
	movsd	.LC4(%rip), %xmm6
	movq	%rax, %xmm4
	jmp	.L2114
	.p2align 4,,10
	.p2align 3
.L2142:
	mulsd	%xmm6, %xmm0
	mulsd	%xmm6, %xmm4
.L2114:
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm3
	andpd	%xmm7, %xmm1
	ucomisd	%xmm1, %xmm5
	jbe	.L2116
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC8(%rip), %xmm10
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm3
	cmpnlesd	%xmm0, %xmm3
	andpd	%xmm10, %xmm3
	subsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm3
	movapd	%xmm7, %xmm1
	andnpd	%xmm0, %xmm1
	orpd	%xmm1, %xmm3
.L2116:
	ucomisd	%xmm3, %xmm0
	jp	.L2142
	jne	.L2142
	movq	.LC8(%rip), %rax
	movsd	.LC4(%rip), %xmm7
	movapd	%xmm8, %xmm0
	movsd	.LC2(%rip), %xmm9
	movq	%rax, %xmm6
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L2143:
	mulsd	%xmm7, %xmm0
	mulsd	%xmm7, %xmm6
.L2117:
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm3
	andpd	%xmm9, %xmm1
	ucomisd	%xmm1, %xmm5
	jbe	.L2119
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC8(%rip), %xmm11
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm3
	cmpnlesd	%xmm0, %xmm3
	andpd	%xmm11, %xmm3
	subsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm3
	movapd	%xmm9, %xmm1
	andnpd	%xmm0, %xmm1
	orpd	%xmm1, %xmm3
.L2119:
	ucomisd	%xmm3, %xmm0
	jp	.L2143
	jne	.L2143
	maxsd	%xmm4, %xmm6
	mulsd	%xmm6, %xmm2
	movapd	%xmm6, %xmm4
	mulsd	%xmm6, %xmm8
	movapd	%xmm2, %xmm0
	comisd	%xmm2, %xmm8
	jb	.L2124
	movl	-296(%rbp), %eax
	testl	%eax, %eax
	jle	.L2126
	movsd	.LC2(%rip), %xmm6
	jmp	.L2131
	.p2align 4,,10
	.p2align 3
.L2157:
	addsd	.LC8(%rip), %xmm0
	comisd	%xmm0, %xmm8
	jb	.L2124
.L2131:
	movapd	%xmm0, %xmm1
	divsd	%xmm4, %xmm1
	movapd	%xmm1, %xmm2
	movapd	%xmm1, %xmm3
	andpd	%xmm6, %xmm2
	ucomisd	%xmm2, %xmm5
	jbe	.L2127
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm2, %xmm2
	movsd	.LC8(%rip), %xmm7
	cvtsi2sdq	%rax, %xmm2
	movapd	%xmm2, %xmm3
	cmpnlesd	%xmm1, %xmm3
	andpd	%xmm7, %xmm3
	subsd	%xmm3, %xmm2
	movapd	%xmm2, %xmm3
	movapd	%xmm6, %xmm2
	andnpd	%xmm1, %xmm2
	orpd	%xmm2, %xmm3
.L2127:
	ucomisd	%xmm3, %xmm1
	jp	.L2146
	je	.L2128
.L2146:
	movq	-344(%rbp), %rsi
	movslq	%r12d, %rax
	addl	$1, %r12d
	movsd	%xmm1, (%rsi,%rax,8)
.L2128:
	cmpl	%r12d, -336(%rbp)
	jg	.L2157
.L2124:
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	leaq	-248(%rbp), %rdi
	movq	%r9, -376(%rbp)
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm4
	movq	%rsi, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -256(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713IFixedDecimalD2Ev@PLT
	movdqa	-368(%rbp), %xmm0
	leaq	-312(%rbp), %rdi
	movaps	%xmm0, -320(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	-376(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713IFixedDecimalD2Ev@PLT
	jmp	.L2112
	.p2align 4,,10
	.p2align 3
.L2152:
	movswl	8(%r15), %ebx
	testw	%bx, %bx
	js	.L2104
	sarl	$5, %ebx
	jmp	.L2103
	.p2align 4,,10
	.p2align 3
.L2104:
	movl	12(%r15), %ebx
	jmp	.L2103
	.p2align 4,,10
	.p2align 3
.L2126:
	addl	$1, %r12d
	movslq	%r12d, %rax
	jmp	.L2134
	.p2align 4,,10
	.p2align 3
.L2158:
	addsd	.LC8(%rip), %xmm0
	addq	$1, %rax
	comisd	%xmm0, %xmm8
	jb	.L2124
.L2134:
	movapd	%xmm0, %xmm1
	movq	-344(%rbp), %rsi
	movl	%eax, %r12d
	divsd	%xmm4, %xmm1
	movsd	%xmm1, -8(%rsi,%rax,8)
	cmpl	%eax, -336(%rbp)
	jg	.L2158
	jmp	.L2124
.L2156:
	movl	$3, (%rax)
.L2113:
	leaq	80+_ZTVN6icu_6712FixedDecimalE(%rip), %rax
	leaq	-248(%rbp), %rdi
	movq	%r9, -344(%rbp)
	leaq	-64(%rax), %rbx
	movq	%rax, %xmm5
	movq	%rbx, %xmm0
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -256(%rbp)
	movaps	%xmm0, -336(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713IFixedDecimalD2Ev@PLT
	movdqa	-336(%rbp), %xmm0
	leaq	-312(%rbp), %rdi
	movaps	%xmm0, -320(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	-344(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713IFixedDecimalD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L2098
.L2137:
	xorl	%r12d, %r12d
	jmp	.L2098
.L2155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3821:
	.size	_ZN6icu_67L20getSamplesFromStringERKNS_13UnicodeStringEPdiR10UErrorCode, .-_ZN6icu_67L20getSamplesFromStringERKNS_13UnicodeStringEPdiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711PluralRules10getSamplesERKNS_13UnicodeStringEPdiR10UErrorCode
	.type	_ZN6icu_6711PluralRules10getSamplesERKNS_13UnicodeStringEPdiR10UErrorCode, @function
_ZN6icu_6711PluralRules10getSamplesERKNS_13UnicodeStringEPdiR10UErrorCode:
.LFB3822:
	.cfi_startproc
	endbr64
	testl	%ecx, %ecx
	je	.L2189
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L2188
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jg	.L2190
	movq	8(%rdi), %rbx
	movq	%rdx, %r13
	movl	%ecx, %r12d
	testq	%rbx, %rbx
	jne	.L2171
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2192:
	testw	%dx, %dx
	js	.L2165
	sarl	$5, %edx
.L2166:
	testw	%ax, %ax
	js	.L2167
	sarl	$5, %eax
.L2168:
	cmpl	%edx, %eax
	jne	.L2169
	testb	%cl, %cl
	je	.L2191
.L2169:
	movq	72(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2188
.L2171:
	movswl	8(%rsi), %eax
	movswl	16(%rbx), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	je	.L2192
.L2164:
	testb	%cl, %cl
	je	.L2169
	leaq	152(%rbx), %rdi
	movq	%r15, %rcx
	movl	%r12d, %edx
	movq	%r13, %rsi
	call	_ZN6icu_67L20getSamplesFromStringERKNS_13UnicodeStringEPdiR10UErrorCode
	testl	%eax, %eax
	jne	.L2159
	addq	$16, %rsp
	leaq	88(%rbx), %rdi
	movq	%r15, %rcx
	movl	%r12d, %edx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r13, %rsi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_67L20getSamplesFromStringERKNS_13UnicodeStringEPdiR10UErrorCode
	.p2align 4,,10
	.p2align 3
.L2190:
	.cfi_restore_state
	movl	%eax, (%r8)
.L2188:
	xorl	%eax, %eax
.L2159:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2167:
	.cfi_restore_state
	movl	12(%rsi), %eax
	jmp	.L2168
	.p2align 4,,10
	.p2align 3
.L2165:
	movl	20(%rbx), %edx
	jmp	.L2166
	.p2align 4,,10
	.p2align 3
.L2189:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2191:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 15, -24
	leaq	8(%rbx), %rdi
	movq	%rsi, -40(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-40(%rbp), %rsi
	testb	%al, %al
	setne	%cl
	jmp	.L2164
	.cfi_endproc
.LFE3822:
	.size	_ZN6icu_6711PluralRules10getSamplesERKNS_13UnicodeStringEPdiR10UErrorCode, .-_ZN6icu_6711PluralRules10getSamplesERKNS_13UnicodeStringEPdiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724PluralKeywordEnumerationD2Ev
	.type	_ZN6icu_6724PluralKeywordEnumerationD2Ev, @function
_ZN6icu_6724PluralKeywordEnumerationD2Ev:
.LFB3890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724PluralKeywordEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	120(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -120(%rdi)
	call	_ZN6icu_677UVectorD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717StringEnumerationD2Ev@PLT
	.cfi_endproc
.LFE3890:
	.size	_ZN6icu_6724PluralKeywordEnumerationD2Ev, .-_ZN6icu_6724PluralKeywordEnumerationD2Ev
	.globl	_ZN6icu_6724PluralKeywordEnumerationD1Ev
	.set	_ZN6icu_6724PluralKeywordEnumerationD1Ev,_ZN6icu_6724PluralKeywordEnumerationD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724PluralKeywordEnumerationD0Ev
	.type	_ZN6icu_6724PluralKeywordEnumerationD0Ev, @function
_ZN6icu_6724PluralKeywordEnumerationD0Ev:
.LFB3892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724PluralKeywordEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	120(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -120(%rdi)
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3892:
	.size	_ZN6icu_6724PluralKeywordEnumerationD0Ev, .-_ZN6icu_6724PluralKeywordEnumerationD0Ev
	.section	.rodata
	.align 16
	.type	CSWTCH.347, @object
	.size	CSWTCH.347, 20
CSWTCH.347:
	.long	0
	.long	1
	.long	2
	.long	4
	.long	3
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6711PluralRulesE
	.section	.rodata._ZTSN6icu_6711PluralRulesE,"aG",@progbits,_ZTSN6icu_6711PluralRulesE,comdat
	.align 16
	.type	_ZTSN6icu_6711PluralRulesE, @object
	.size	_ZTSN6icu_6711PluralRulesE, 23
_ZTSN6icu_6711PluralRulesE:
	.string	"N6icu_6711PluralRulesE"
	.weak	_ZTIN6icu_6711PluralRulesE
	.section	.data.rel.ro._ZTIN6icu_6711PluralRulesE,"awG",@progbits,_ZTIN6icu_6711PluralRulesE,comdat
	.align 8
	.type	_ZTIN6icu_6711PluralRulesE, @object
	.size	_ZTIN6icu_6711PluralRulesE, 24
_ZTIN6icu_6711PluralRulesE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6711PluralRulesE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6716PluralRuleParserE
	.section	.rodata._ZTSN6icu_6716PluralRuleParserE,"aG",@progbits,_ZTSN6icu_6716PluralRuleParserE,comdat
	.align 16
	.type	_ZTSN6icu_6716PluralRuleParserE, @object
	.size	_ZTSN6icu_6716PluralRuleParserE, 28
_ZTSN6icu_6716PluralRuleParserE:
	.string	"N6icu_6716PluralRuleParserE"
	.weak	_ZTIN6icu_6716PluralRuleParserE
	.section	.data.rel.ro._ZTIN6icu_6716PluralRuleParserE,"awG",@progbits,_ZTIN6icu_6716PluralRuleParserE,comdat
	.align 8
	.type	_ZTIN6icu_6716PluralRuleParserE, @object
	.size	_ZTIN6icu_6716PluralRuleParserE, 24
_ZTIN6icu_6716PluralRuleParserE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6716PluralRuleParserE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6712FixedDecimalE
	.section	.rodata._ZTSN6icu_6712FixedDecimalE,"aG",@progbits,_ZTSN6icu_6712FixedDecimalE,comdat
	.align 16
	.type	_ZTSN6icu_6712FixedDecimalE, @object
	.size	_ZTSN6icu_6712FixedDecimalE, 24
_ZTSN6icu_6712FixedDecimalE:
	.string	"N6icu_6712FixedDecimalE"
	.weak	_ZTIN6icu_6712FixedDecimalE
	.section	.data.rel.ro._ZTIN6icu_6712FixedDecimalE,"awG",@progbits,_ZTIN6icu_6712FixedDecimalE,comdat
	.align 8
	.type	_ZTIN6icu_6712FixedDecimalE, @object
	.size	_ZTIN6icu_6712FixedDecimalE, 56
_ZTIN6icu_6712FixedDecimalE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_6712FixedDecimalE
	.long	0
	.long	2
	.quad	_ZTIN6icu_6713IFixedDecimalE
	.quad	2
	.quad	_ZTIN6icu_677UObjectE
	.quad	2050
	.weak	_ZTSN6icu_6713AndConstraintE
	.section	.rodata._ZTSN6icu_6713AndConstraintE,"aG",@progbits,_ZTSN6icu_6713AndConstraintE,comdat
	.align 16
	.type	_ZTSN6icu_6713AndConstraintE, @object
	.size	_ZTSN6icu_6713AndConstraintE, 25
_ZTSN6icu_6713AndConstraintE:
	.string	"N6icu_6713AndConstraintE"
	.weak	_ZTIN6icu_6713AndConstraintE
	.section	.data.rel.ro._ZTIN6icu_6713AndConstraintE,"awG",@progbits,_ZTIN6icu_6713AndConstraintE,comdat
	.align 8
	.type	_ZTIN6icu_6713AndConstraintE, @object
	.size	_ZTIN6icu_6713AndConstraintE, 24
_ZTIN6icu_6713AndConstraintE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713AndConstraintE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6712OrConstraintE
	.section	.rodata._ZTSN6icu_6712OrConstraintE,"aG",@progbits,_ZTSN6icu_6712OrConstraintE,comdat
	.align 16
	.type	_ZTSN6icu_6712OrConstraintE, @object
	.size	_ZTSN6icu_6712OrConstraintE, 24
_ZTSN6icu_6712OrConstraintE:
	.string	"N6icu_6712OrConstraintE"
	.weak	_ZTIN6icu_6712OrConstraintE
	.section	.data.rel.ro._ZTIN6icu_6712OrConstraintE,"awG",@progbits,_ZTIN6icu_6712OrConstraintE,comdat
	.align 8
	.type	_ZTIN6icu_6712OrConstraintE, @object
	.size	_ZTIN6icu_6712OrConstraintE, 24
_ZTIN6icu_6712OrConstraintE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712OrConstraintE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_679RuleChainE
	.section	.rodata._ZTSN6icu_679RuleChainE,"aG",@progbits,_ZTSN6icu_679RuleChainE,comdat
	.align 16
	.type	_ZTSN6icu_679RuleChainE, @object
	.size	_ZTSN6icu_679RuleChainE, 20
_ZTSN6icu_679RuleChainE:
	.string	"N6icu_679RuleChainE"
	.weak	_ZTIN6icu_679RuleChainE
	.section	.data.rel.ro._ZTIN6icu_679RuleChainE,"awG",@progbits,_ZTIN6icu_679RuleChainE,comdat
	.align 8
	.type	_ZTIN6icu_679RuleChainE, @object
	.size	_ZTIN6icu_679RuleChainE, 24
_ZTIN6icu_679RuleChainE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_679RuleChainE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6724PluralKeywordEnumerationE
	.section	.rodata._ZTSN6icu_6724PluralKeywordEnumerationE,"aG",@progbits,_ZTSN6icu_6724PluralKeywordEnumerationE,comdat
	.align 32
	.type	_ZTSN6icu_6724PluralKeywordEnumerationE, @object
	.size	_ZTSN6icu_6724PluralKeywordEnumerationE, 36
_ZTSN6icu_6724PluralKeywordEnumerationE:
	.string	"N6icu_6724PluralKeywordEnumerationE"
	.weak	_ZTIN6icu_6724PluralKeywordEnumerationE
	.section	.data.rel.ro._ZTIN6icu_6724PluralKeywordEnumerationE,"awG",@progbits,_ZTIN6icu_6724PluralKeywordEnumerationE,comdat
	.align 8
	.type	_ZTIN6icu_6724PluralKeywordEnumerationE, @object
	.size	_ZTIN6icu_6724PluralKeywordEnumerationE, 24
_ZTIN6icu_6724PluralKeywordEnumerationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6724PluralKeywordEnumerationE
	.quad	_ZTIN6icu_6717StringEnumerationE
	.weak	_ZTSN6icu_6733PluralAvailableLocalesEnumerationE
	.section	.rodata._ZTSN6icu_6733PluralAvailableLocalesEnumerationE,"aG",@progbits,_ZTSN6icu_6733PluralAvailableLocalesEnumerationE,comdat
	.align 32
	.type	_ZTSN6icu_6733PluralAvailableLocalesEnumerationE, @object
	.size	_ZTSN6icu_6733PluralAvailableLocalesEnumerationE, 45
_ZTSN6icu_6733PluralAvailableLocalesEnumerationE:
	.string	"N6icu_6733PluralAvailableLocalesEnumerationE"
	.weak	_ZTIN6icu_6733PluralAvailableLocalesEnumerationE
	.section	.data.rel.ro._ZTIN6icu_6733PluralAvailableLocalesEnumerationE,"awG",@progbits,_ZTIN6icu_6733PluralAvailableLocalesEnumerationE,comdat
	.align 8
	.type	_ZTIN6icu_6733PluralAvailableLocalesEnumerationE, @object
	.size	_ZTIN6icu_6733PluralAvailableLocalesEnumerationE, 24
_ZTIN6icu_6733PluralAvailableLocalesEnumerationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6733PluralAvailableLocalesEnumerationE
	.quad	_ZTIN6icu_6717StringEnumerationE
	.weak	_ZTSN6icu_6717SharedPluralRulesE
	.section	.rodata._ZTSN6icu_6717SharedPluralRulesE,"aG",@progbits,_ZTSN6icu_6717SharedPluralRulesE,comdat
	.align 16
	.type	_ZTSN6icu_6717SharedPluralRulesE, @object
	.size	_ZTSN6icu_6717SharedPluralRulesE, 29
_ZTSN6icu_6717SharedPluralRulesE:
	.string	"N6icu_6717SharedPluralRulesE"
	.weak	_ZTIN6icu_6717SharedPluralRulesE
	.section	.data.rel.ro._ZTIN6icu_6717SharedPluralRulesE,"awG",@progbits,_ZTIN6icu_6717SharedPluralRulesE,comdat
	.align 8
	.type	_ZTIN6icu_6717SharedPluralRulesE, @object
	.size	_ZTIN6icu_6717SharedPluralRulesE, 24
_ZTIN6icu_6717SharedPluralRulesE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717SharedPluralRulesE
	.quad	_ZTIN6icu_6712SharedObjectE
	.weak	_ZTSN6icu_678CacheKeyINS_17SharedPluralRulesEEE
	.section	.rodata._ZTSN6icu_678CacheKeyINS_17SharedPluralRulesEEE,"aG",@progbits,_ZTSN6icu_678CacheKeyINS_17SharedPluralRulesEEE,comdat
	.align 32
	.type	_ZTSN6icu_678CacheKeyINS_17SharedPluralRulesEEE, @object
	.size	_ZTSN6icu_678CacheKeyINS_17SharedPluralRulesEEE, 44
_ZTSN6icu_678CacheKeyINS_17SharedPluralRulesEEE:
	.string	"N6icu_678CacheKeyINS_17SharedPluralRulesEEE"
	.weak	_ZTIN6icu_678CacheKeyINS_17SharedPluralRulesEEE
	.section	.data.rel.ro._ZTIN6icu_678CacheKeyINS_17SharedPluralRulesEEE,"awG",@progbits,_ZTIN6icu_678CacheKeyINS_17SharedPluralRulesEEE,comdat
	.align 8
	.type	_ZTIN6icu_678CacheKeyINS_17SharedPluralRulesEEE, @object
	.size	_ZTIN6icu_678CacheKeyINS_17SharedPluralRulesEEE, 24
_ZTIN6icu_678CacheKeyINS_17SharedPluralRulesEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678CacheKeyINS_17SharedPluralRulesEEE
	.quad	_ZTIN6icu_6712CacheKeyBaseE
	.weak	_ZTSN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE
	.section	.rodata._ZTSN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE,"aG",@progbits,_ZTSN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE,comdat
	.align 32
	.type	_ZTSN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE, @object
	.size	_ZTSN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE, 51
_ZTSN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE:
	.string	"N6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE"
	.weak	_ZTIN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE
	.section	.data.rel.ro._ZTIN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE,"awG",@progbits,_ZTIN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE,comdat
	.align 8
	.type	_ZTIN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE, @object
	.size	_ZTIN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE, 24
_ZTIN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE
	.quad	_ZTIN6icu_678CacheKeyINS_17SharedPluralRulesEEE
	.weak	_ZTVN6icu_6711PluralRulesE
	.section	.data.rel.ro.local._ZTVN6icu_6711PluralRulesE,"awG",@progbits,_ZTVN6icu_6711PluralRulesE,comdat
	.align 8
	.type	_ZTVN6icu_6711PluralRulesE, @object
	.size	_ZTVN6icu_6711PluralRulesE, 48
_ZTVN6icu_6711PluralRulesE:
	.quad	0
	.quad	_ZTIN6icu_6711PluralRulesE
	.quad	_ZN6icu_6711PluralRulesD1Ev
	.quad	_ZN6icu_6711PluralRulesD0Ev
	.quad	_ZNK6icu_6711PluralRules17getDynamicClassIDEv
	.quad	_ZNK6icu_6711PluralRuleseqERKS0_
	.weak	_ZTVN6icu_6717SharedPluralRulesE
	.section	.data.rel.ro._ZTVN6icu_6717SharedPluralRulesE,"awG",@progbits,_ZTVN6icu_6717SharedPluralRulesE,comdat
	.align 8
	.type	_ZTVN6icu_6717SharedPluralRulesE, @object
	.size	_ZTVN6icu_6717SharedPluralRulesE, 40
_ZTVN6icu_6717SharedPluralRulesE:
	.quad	0
	.quad	_ZTIN6icu_6717SharedPluralRulesE
	.quad	_ZN6icu_6717SharedPluralRulesD1Ev
	.quad	_ZN6icu_6717SharedPluralRulesD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.weak	_ZTVN6icu_678CacheKeyINS_17SharedPluralRulesEEE
	.section	.data.rel.ro._ZTVN6icu_678CacheKeyINS_17SharedPluralRulesEEE,"awG",@progbits,_ZTVN6icu_678CacheKeyINS_17SharedPluralRulesEEE,comdat
	.align 8
	.type	_ZTVN6icu_678CacheKeyINS_17SharedPluralRulesEEE, @object
	.size	_ZTVN6icu_678CacheKeyINS_17SharedPluralRulesEEE, 80
_ZTVN6icu_678CacheKeyINS_17SharedPluralRulesEEE:
	.quad	0
	.quad	_ZTIN6icu_678CacheKeyINS_17SharedPluralRulesEEE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE8hashCodeEv
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci
	.weak	_ZTVN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE
	.section	.data.rel.ro._ZTVN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE,"awG",@progbits,_ZTVN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE,comdat
	.align 8
	.type	_ZTVN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE, @object
	.size	_ZTVN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE, 80
_ZTVN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE:
	.quad	0
	.quad	_ZTIN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEE
	.quad	_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED1Ev
	.quad	_ZN6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEED0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE8hashCodeEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE5cloneEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEEeqERKNS_12CacheKeyBaseE
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE12createObjectEPKvR10UErrorCode
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_17SharedPluralRulesEE16writeDescriptionEPci
	.weak	_ZTVN6icu_6713AndConstraintE
	.section	.data.rel.ro.local._ZTVN6icu_6713AndConstraintE,"awG",@progbits,_ZTVN6icu_6713AndConstraintE,comdat
	.align 8
	.type	_ZTVN6icu_6713AndConstraintE, @object
	.size	_ZTVN6icu_6713AndConstraintE, 32
_ZTVN6icu_6713AndConstraintE:
	.quad	0
	.quad	_ZTIN6icu_6713AndConstraintE
	.quad	_ZN6icu_6713AndConstraintD1Ev
	.quad	_ZN6icu_6713AndConstraintD0Ev
	.weak	_ZTVN6icu_6712OrConstraintE
	.section	.data.rel.ro.local._ZTVN6icu_6712OrConstraintE,"awG",@progbits,_ZTVN6icu_6712OrConstraintE,comdat
	.align 8
	.type	_ZTVN6icu_6712OrConstraintE, @object
	.size	_ZTVN6icu_6712OrConstraintE, 32
_ZTVN6icu_6712OrConstraintE:
	.quad	0
	.quad	_ZTIN6icu_6712OrConstraintE
	.quad	_ZN6icu_6712OrConstraintD1Ev
	.quad	_ZN6icu_6712OrConstraintD0Ev
	.weak	_ZTVN6icu_679RuleChainE
	.section	.data.rel.ro.local._ZTVN6icu_679RuleChainE,"awG",@progbits,_ZTVN6icu_679RuleChainE,comdat
	.align 8
	.type	_ZTVN6icu_679RuleChainE, @object
	.size	_ZTVN6icu_679RuleChainE, 32
_ZTVN6icu_679RuleChainE:
	.quad	0
	.quad	_ZTIN6icu_679RuleChainE
	.quad	_ZN6icu_679RuleChainD1Ev
	.quad	_ZN6icu_679RuleChainD0Ev
	.weak	_ZTVN6icu_6716PluralRuleParserE
	.section	.data.rel.ro.local._ZTVN6icu_6716PluralRuleParserE,"awG",@progbits,_ZTVN6icu_6716PluralRuleParserE,comdat
	.align 8
	.type	_ZTVN6icu_6716PluralRuleParserE, @object
	.size	_ZTVN6icu_6716PluralRuleParserE, 32
_ZTVN6icu_6716PluralRuleParserE:
	.quad	0
	.quad	_ZTIN6icu_6716PluralRuleParserE
	.quad	_ZN6icu_6716PluralRuleParserD1Ev
	.quad	_ZN6icu_6716PluralRuleParserD0Ev
	.weak	_ZTVN6icu_6724PluralKeywordEnumerationE
	.section	.data.rel.ro._ZTVN6icu_6724PluralKeywordEnumerationE,"awG",@progbits,_ZTVN6icu_6724PluralKeywordEnumerationE,comdat
	.align 8
	.type	_ZTVN6icu_6724PluralKeywordEnumerationE, @object
	.size	_ZTVN6icu_6724PluralKeywordEnumerationE, 104
_ZTVN6icu_6724PluralKeywordEnumerationE:
	.quad	0
	.quad	_ZTIN6icu_6724PluralKeywordEnumerationE
	.quad	_ZN6icu_6724PluralKeywordEnumerationD1Ev
	.quad	_ZN6icu_6724PluralKeywordEnumerationD0Ev
	.quad	_ZNK6icu_6724PluralKeywordEnumeration17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringEnumeration5cloneEv
	.quad	_ZNK6icu_6724PluralKeywordEnumeration5countER10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration4nextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode
	.quad	_ZN6icu_6724PluralKeywordEnumeration5snextER10UErrorCode
	.quad	_ZN6icu_6724PluralKeywordEnumeration5resetER10UErrorCode
	.quad	_ZNK6icu_6717StringEnumerationeqERKS0_
	.quad	_ZNK6icu_6717StringEnumerationneERKS0_
	.weak	_ZTVN6icu_6712FixedDecimalE
	.section	.data.rel.ro._ZTVN6icu_6712FixedDecimalE,"awG",@progbits,_ZTVN6icu_6712FixedDecimalE,comdat
	.align 8
	.type	_ZTVN6icu_6712FixedDecimalE, @object
	.size	_ZTVN6icu_6712FixedDecimalE, 104
_ZTVN6icu_6712FixedDecimalE:
	.quad	0
	.quad	_ZTIN6icu_6712FixedDecimalE
	.quad	_ZN6icu_6712FixedDecimalD1Ev
	.quad	_ZN6icu_6712FixedDecimalD0Ev
	.quad	_ZNK6icu_6712FixedDecimal16getPluralOperandENS_13PluralOperandE
	.quad	_ZNK6icu_6712FixedDecimal5isNaNEv
	.quad	_ZNK6icu_6712FixedDecimal10isInfiniteEv
	.quad	_ZNK6icu_6712FixedDecimal15hasIntegerValueEv
	.quad	-8
	.quad	_ZTIN6icu_6712FixedDecimalE
	.quad	_ZThn8_N6icu_6712FixedDecimalD1Ev
	.quad	_ZThn8_N6icu_6712FixedDecimalD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.weak	_ZTVN6icu_6733PluralAvailableLocalesEnumerationE
	.section	.data.rel.ro._ZTVN6icu_6733PluralAvailableLocalesEnumerationE,"awG",@progbits,_ZTVN6icu_6733PluralAvailableLocalesEnumerationE,comdat
	.align 8
	.type	_ZTVN6icu_6733PluralAvailableLocalesEnumerationE, @object
	.size	_ZTVN6icu_6733PluralAvailableLocalesEnumerationE, 104
_ZTVN6icu_6733PluralAvailableLocalesEnumerationE:
	.quad	0
	.quad	_ZTIN6icu_6733PluralAvailableLocalesEnumerationE
	.quad	_ZN6icu_6733PluralAvailableLocalesEnumerationD1Ev
	.quad	_ZN6icu_6733PluralAvailableLocalesEnumerationD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringEnumeration5cloneEv
	.quad	_ZNK6icu_6733PluralAvailableLocalesEnumeration5countER10UErrorCode
	.quad	_ZN6icu_6733PluralAvailableLocalesEnumeration4nextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5snextER10UErrorCode
	.quad	_ZN6icu_6733PluralAvailableLocalesEnumeration5resetER10UErrorCode
	.quad	_ZNK6icu_6717StringEnumerationeqERKS0_
	.quad	_ZNK6icu_6717StringEnumerationneERKS0_
	.local	_ZZN6icu_6724PluralKeywordEnumeration16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6724PluralKeywordEnumeration16getStaticClassIDEvE7classID,1,1
	.local	_ZZN6icu_6711PluralRules16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6711PluralRules16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.align 16
	.type	_ZN6icu_67L10PK_INTEGERE, @object
	.size	_ZN6icu_67L10PK_INTEGERE, 16
_ZN6icu_67L10PK_INTEGERE:
	.value	105
	.value	110
	.value	116
	.value	101
	.value	103
	.value	101
	.value	114
	.value	0
	.align 16
	.type	_ZN6icu_67L10PK_DECIMALE, @object
	.size	_ZN6icu_67L10PK_DECIMALE, 16
_ZN6icu_67L10PK_DECIMALE:
	.value	100
	.value	101
	.value	99
	.value	105
	.value	109
	.value	97
	.value	108
	.value	0
	.align 8
	.type	_ZN6icu_67L9PK_WITHINE, @object
	.size	_ZN6icu_67L9PK_WITHINE, 14
_ZN6icu_67L9PK_WITHINE:
	.value	119
	.value	105
	.value	116
	.value	104
	.value	105
	.value	110
	.value	0
	.align 2
	.type	_ZN6icu_67L8PK_VAR_VE, @object
	.size	_ZN6icu_67L8PK_VAR_VE, 4
_ZN6icu_67L8PK_VAR_VE:
	.value	118
	.value	0
	.align 2
	.type	_ZN6icu_67L8PK_VAR_TE, @object
	.size	_ZN6icu_67L8PK_VAR_TE, 4
_ZN6icu_67L8PK_VAR_TE:
	.value	116
	.value	0
	.align 2
	.type	_ZN6icu_67L8PK_VAR_FE, @object
	.size	_ZN6icu_67L8PK_VAR_FE, 4
_ZN6icu_67L8PK_VAR_FE:
	.value	102
	.value	0
	.align 2
	.type	_ZN6icu_67L8PK_VAR_IE, @object
	.size	_ZN6icu_67L8PK_VAR_IE, 4
_ZN6icu_67L8PK_VAR_IE:
	.value	105
	.value	0
	.align 2
	.type	_ZN6icu_67L8PK_VAR_NE, @object
	.size	_ZN6icu_67L8PK_VAR_NE, 4
_ZN6icu_67L8PK_VAR_NE:
	.value	110
	.value	0
	.align 2
	.type	_ZN6icu_67L5PK_ORE, @object
	.size	_ZN6icu_67L5PK_ORE, 6
_ZN6icu_67L5PK_ORE:
	.value	111
	.value	114
	.value	0
	.align 8
	.type	_ZN6icu_67L6PK_ANDE, @object
	.size	_ZN6icu_67L6PK_ANDE, 8
_ZN6icu_67L6PK_ANDE:
	.value	97
	.value	110
	.value	100
	.value	0
	.align 8
	.type	_ZN6icu_67L6PK_MODE, @object
	.size	_ZN6icu_67L6PK_MODE, 8
_ZN6icu_67L6PK_MODE:
	.value	109
	.value	111
	.value	100
	.value	0
	.align 2
	.type	_ZN6icu_67L5PK_ISE, @object
	.size	_ZN6icu_67L5PK_ISE, 6
_ZN6icu_67L5PK_ISE:
	.value	105
	.value	115
	.value	0
	.align 8
	.type	_ZN6icu_67L6PK_NOTE, @object
	.size	_ZN6icu_67L6PK_NOTE, 8
_ZN6icu_67L6PK_NOTE:
	.value	110
	.value	111
	.value	116
	.value	0
	.align 2
	.type	_ZN6icu_67L5PK_INE, @object
	.size	_ZN6icu_67L5PK_INE, 6
_ZN6icu_67L5PK_INE:
	.value	105
	.value	110
	.value	0
	.align 16
	.type	_ZN6icu_67L19PLURAL_DEFAULT_RULEE, @object
	.size	_ZN6icu_67L19PLURAL_DEFAULT_RULEE, 18
_ZN6icu_67L19PLURAL_DEFAULT_RULEE:
	.value	111
	.value	116
	.value	104
	.value	101
	.value	114
	.value	58
	.value	32
	.value	110
	.value	0
	.align 8
	.type	_ZN6icu_67L20PLURAL_KEYWORD_OTHERE, @object
	.size	_ZN6icu_67L20PLURAL_KEYWORD_OTHERE, 12
_ZN6icu_67L20PLURAL_KEYWORD_OTHERE:
	.value	111
	.value	116
	.value	104
	.value	101
	.value	114
	.value	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	0
	.long	1127219200
	.align 8
.LC4:
	.long	0
	.long	1076101120
	.align 8
.LC5:
	.long	0
	.long	1071644672
	.align 8
.LC6:
	.long	0
	.long	1079574528
	.align 8
.LC7:
	.long	0
	.long	1083129856
	.align 8
.LC8:
	.long	0
	.long	1072693248
	.align 8
.LC9:
	.long	0
	.long	1138753536
	.align 8
.LC15:
	.long	211361407
	.long	-1084999129
	.section	.data.rel.ro,"aw"
	.align 8
.LC32:
	.quad	_ZTVN6icu_6713UnicodeStringE+16
	.section	.rodata.cst8
	.align 8
.LC35:
	.long	4294967295
	.long	2147483647
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
