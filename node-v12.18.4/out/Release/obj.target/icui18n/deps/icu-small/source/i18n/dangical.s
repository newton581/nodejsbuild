	.file	"dangical.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dangi"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DangiCalendar7getTypeEv
	.type	_ZNK6icu_6713DangiCalendar7getTypeEv, @function
_ZNK6icu_6713DangiCalendar7getTypeEv:
.LFB2990:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE2990:
	.size	_ZNK6icu_6713DangiCalendar7getTypeEv, .-_ZNK6icu_6713DangiCalendar7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DangiCalendar17getDynamicClassIDEv
	.type	_ZNK6icu_6713DangiCalendar17getDynamicClassIDEv, @function
_ZNK6icu_6713DangiCalendar17getDynamicClassIDEv:
.LFB2994:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713DangiCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2994:
	.size	_ZNK6icu_6713DangiCalendar17getDynamicClassIDEv, .-_ZNK6icu_6713DangiCalendar17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DangiCalendarD2Ev
	.type	_ZN6icu_6713DangiCalendarD2Ev, @function
_ZN6icu_6713DangiCalendarD2Ev:
.LFB2986:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713DangiCalendarE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6715ChineseCalendarD2Ev@PLT
	.cfi_endproc
.LFE2986:
	.size	_ZN6icu_6713DangiCalendarD2Ev, .-_ZN6icu_6713DangiCalendarD2Ev
	.globl	_ZN6icu_6713DangiCalendarD1Ev
	.set	_ZN6icu_6713DangiCalendarD1Ev,_ZN6icu_6713DangiCalendarD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DangiCalendarD0Ev
	.type	_ZN6icu_6713DangiCalendarD0Ev, @function
_ZN6icu_6713DangiCalendarD0Ev:
.LFB2988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713DangiCalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6715ChineseCalendarD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2988:
	.size	_ZN6icu_6713DangiCalendarD0Ev, .-_ZN6icu_6713DangiCalendarD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DangiCalendar5cloneEv
	.type	_ZNK6icu_6713DangiCalendar5cloneEv, @function
_ZNK6icu_6713DangiCalendar5cloneEv:
.LFB2989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$624, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L7
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6715ChineseCalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6713DangiCalendarE(%rip), %rax
	movq	%rax, (%r12)
.L7:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2989:
	.size	_ZNK6icu_6713DangiCalendar5cloneEv, .-_ZNK6icu_6713DangiCalendar5cloneEv
	.p2align 4
	.type	calendar_dangi_cleanup, @function
calendar_dangi_cleanup:
.LFB2978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZL27gDangiCalendarZoneAstroCalc(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L14
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, _ZL27gDangiCalendarZoneAstroCalc(%rip)
.L14:
	movl	$1, %eax
	movl	$0, _ZL22gDangiCalendarInitOnce(%rip)
	mfence
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2978:
	.size	calendar_dangi_cleanup, .-calendar_dangi_cleanup
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DangiCalendarC2ERKS0_
	.type	_ZN6icu_6713DangiCalendarC2ERKS0_, @function
_ZN6icu_6713DangiCalendarC2ERKS0_:
.LFB2983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6715ChineseCalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6713DangiCalendarE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2983:
	.size	_ZN6icu_6713DangiCalendarC2ERKS0_, .-_ZN6icu_6713DangiCalendarC2ERKS0_
	.globl	_ZN6icu_6713DangiCalendarC1ERKS0_
	.set	_ZN6icu_6713DangiCalendarC1ERKS0_,_ZN6icu_6713DangiCalendarC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DangiCalendar16getStaticClassIDEv
	.type	_ZN6icu_6713DangiCalendar16getStaticClassIDEv, @function
_ZN6icu_6713DangiCalendar16getStaticClassIDEv:
.LFB2993:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713DangiCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2993:
	.size	_ZN6icu_6713DangiCalendar16getStaticClassIDEv, .-_ZN6icu_6713DangiCalendar16getStaticClassIDEv
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC4:
	.string	"G"
	.string	"M"
	.string	"T"
	.string	"+"
	.string	"8"
	.string	""
	.string	""
	.align 2
.LC5:
	.string	"K"
	.string	"o"
	.string	"r"
	.string	"e"
	.string	"a"
	.string	"n"
	.string	" "
	.string	"1"
	.string	"8"
	.string	"9"
	.string	"7"
	.string	""
	.string	""
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC6:
	.string	"K"
	.string	"o"
	.string	"r"
	.string	"e"
	.string	"a"
	.string	"n"
	.string	" "
	.string	"1"
	.string	"8"
	.string	"9"
	.string	"8"
	.string	"-"
	.string	"1"
	.string	"9"
	.string	"1"
	.string	"1"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC7:
	.string	"K"
	.string	"o"
	.string	"r"
	.string	"e"
	.string	"a"
	.string	"n"
	.string	" "
	.string	"1"
	.string	"9"
	.string	"1"
	.string	"2"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC8:
	.string	"K"
	.string	"O"
	.string	"R"
	.string	"E"
	.string	"A"
	.string	"_"
	.string	"Z"
	.string	"O"
	.string	"N"
	.string	"E"
	.string	""
	.string	""
	.text
	.p2align 4
	.type	_ZN6icu_67L25initDangiCalZoneAstroCalcEv, @function
_ZN6icu_67L25initDangiCalZoneAstroCalcEv:
.LFB2991:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-160(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	movq	%r13, %rdx
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	.LC1(%rip), %rax
	movq	%rax, -152(%rbp)
	movq	.LC2(%rip), %rax
	movq	%rax, -144(%rbp)
	movq	.LC3(%rip), %rax
	movq	%rax, -136(%rbp)
	leaq	.LC4(%rip), %rax
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$80, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L23
	xorl	%ecx, %ecx
	movl	$28800000, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6719InitialTimeZoneRuleC1ERKNS_13UnicodeStringEii@PLT
.L23:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rax
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$352, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L24
	subq	$8, %rsp
	movl	$1, %r9d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	pushq	$1
	leaq	-152(%rbp), %r8
	movl	$25200000, %edx
	movq	%rax, %rdi
	call	_ZN6icu_6721TimeArrayTimeZoneRuleC1ERKNS_13UnicodeStringEiiPKdiNS_12DateTimeRule12TimeRuleTypeE@PLT
	popq	%r8
	popq	%r9
.L24:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rax
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$352, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L25
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	movl	$1, %r9d
	pushq	$1
	leaq	-144(%rbp), %r8
	xorl	%ecx, %ecx
	movl	$28800000, %edx
	call	_ZN6icu_6721TimeArrayTimeZoneRuleC1ERKNS_13UnicodeStringEiiPKdiNS_12DateTimeRule12TimeRuleTypeE@PLT
	popq	%rsi
	popq	%rdi
.L25:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	.LC7(%rip), %rax
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$352, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L26
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movl	$32400000, %edx
	movq	%r12, %rsi
	pushq	$1
	movl	$1, %r9d
	leaq	-136(%rbp), %r8
	movq	%rax, %rdi
	call	_ZN6icu_6721TimeArrayTimeZoneRuleC1ERKNS_13UnicodeStringEiiPKdiNS_12DateTimeRule12TimeRuleTypeE@PLT
	popq	%rdx
	popq	%rcx
.L26:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	.LC8(%rip), %rax
	movl	$0, -164(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$112, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L27
	movq	-184(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6717RuleBasedTimeZoneC1ERKNS_13UnicodeStringEPNS_19InitialTimeZoneRuleE@PLT
.L27:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-164(%rbp), %r12
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r12, %rdx
	call	_ZN6icu_6717RuleBasedTimeZone17addTransitionRuleEPNS_12TimeZoneRuleER10UErrorCode@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717RuleBasedTimeZone17addTransitionRuleEPNS_12TimeZoneRuleER10UErrorCode@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717RuleBasedTimeZone17addTransitionRuleEPNS_12TimeZoneRuleER10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717RuleBasedTimeZone8completeER10UErrorCode@PLT
	movl	-164(%rbp), %eax
	testl	%eax, %eax
	jle	.L51
	testq	%r13, %r13
	je	.L30
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L30:
	movq	$0, _ZL27gDangiCalendarZoneAstroCalc(%rip)
.L29:
	leaq	calendar_dangi_cleanup(%rip), %rsi
	movl	$12, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	%r13, _ZL27gDangiCalendarZoneAstroCalc(%rip)
	jmp	.L29
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2991:
	.size	_ZN6icu_67L25initDangiCalZoneAstroCalcEv, .-_ZN6icu_67L25initDangiCalZoneAstroCalcEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DangiCalendar24getDangiCalZoneAstroCalcEv
	.type	_ZNK6icu_6713DangiCalendar24getDangiCalZoneAstroCalcEv, @function
_ZNK6icu_6713DangiCalendar24getDangiCalZoneAstroCalcEv:
.LFB2992:
	.cfi_startproc
	endbr64
	movl	_ZL22gDangiCalendarInitOnce(%rip), %eax
	cmpl	$2, %eax
	je	.L61
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL22gDangiCalendarInitOnce(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L55
	call	_ZN6icu_67L25initDangiCalZoneAstroCalcEv
	leaq	_ZL22gDangiCalendarInitOnce(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L55:
	movq	_ZL27gDangiCalendarZoneAstroCalc(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore 6
	movq	_ZL27gDangiCalendarZoneAstroCalc(%rip), %rax
	ret
	.cfi_endproc
.LFE2992:
	.size	_ZNK6icu_6713DangiCalendar24getDangiCalZoneAstroCalcEv, .-_ZNK6icu_6713DangiCalendar24getDangiCalZoneAstroCalcEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DangiCalendarC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6713DangiCalendarC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6713DangiCalendarC2ERKNS_6LocaleER10UErrorCode:
.LFB2980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZL22gDangiCalendarInitOnce(%rip), %eax
	cmpl	$2, %eax
	je	.L66
	leaq	_ZL22gDangiCalendarInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L66
	call	_ZN6icu_67L25initDangiCalZoneAstroCalcEv
	leaq	_ZL22gDangiCalendarInitOnce(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L66:
	movq	_ZL27gDangiCalendarZoneAstroCalc(%rip), %rcx
	movq	%r13, %r8
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	$-2332, %edx
	call	_ZN6icu_6715ChineseCalendarC2ERKNS_6LocaleEiPKNS_8TimeZoneER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6713DangiCalendarE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2980:
	.size	_ZN6icu_6713DangiCalendarC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6713DangiCalendarC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6713DangiCalendarC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6713DangiCalendarC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6713DangiCalendarC2ERKNS_6LocaleER10UErrorCode
	.weak	_ZTSN6icu_6713DangiCalendarE
	.section	.rodata._ZTSN6icu_6713DangiCalendarE,"aG",@progbits,_ZTSN6icu_6713DangiCalendarE,comdat
	.align 16
	.type	_ZTSN6icu_6713DangiCalendarE, @object
	.size	_ZTSN6icu_6713DangiCalendarE, 25
_ZTSN6icu_6713DangiCalendarE:
	.string	"N6icu_6713DangiCalendarE"
	.weak	_ZTIN6icu_6713DangiCalendarE
	.section	.data.rel.ro._ZTIN6icu_6713DangiCalendarE,"awG",@progbits,_ZTIN6icu_6713DangiCalendarE,comdat
	.align 8
	.type	_ZTIN6icu_6713DangiCalendarE, @object
	.size	_ZTIN6icu_6713DangiCalendarE, 24
_ZTIN6icu_6713DangiCalendarE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713DangiCalendarE
	.quad	_ZTIN6icu_6715ChineseCalendarE
	.weak	_ZTVN6icu_6713DangiCalendarE
	.section	.data.rel.ro._ZTVN6icu_6713DangiCalendarE,"awG",@progbits,_ZTVN6icu_6713DangiCalendarE,comdat
	.align 8
	.type	_ZTVN6icu_6713DangiCalendarE, @object
	.size	_ZTVN6icu_6713DangiCalendarE, 488
_ZTVN6icu_6713DangiCalendarE:
	.quad	0
	.quad	_ZTIN6icu_6713DangiCalendarE
	.quad	_ZN6icu_6713DangiCalendarD1Ev
	.quad	_ZN6icu_6713DangiCalendarD0Ev
	.quad	_ZNK6icu_6713DangiCalendar17getDynamicClassIDEv
	.quad	_ZNK6icu_6713DangiCalendar5cloneEv
	.quad	_ZNK6icu_678CalendareqERKS0_
	.quad	_ZNK6icu_678Calendar14isEquivalentToERKS0_
	.quad	_ZN6icu_6715ChineseCalendar3addENS_8Calendar11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_6715ChineseCalendar3addE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_6715ChineseCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_6715ChineseCalendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6715ChineseCalendar14inDaylightTimeER10UErrorCode
	.quad	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6713DangiCalendar7getTypeEv
	.quad	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEv
	.quad	_ZN6icu_678Calendar11computeTimeER10UErrorCode
	.quad	_ZN6icu_678Calendar13computeFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode
	.quad	_ZNK6icu_6715ChineseCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.quad	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE
	.quad	_ZNK6icu_6715ChineseCalendar23handleComputeMonthStartEiia
	.quad	_ZNK6icu_6715ChineseCalendar20handleGetMonthLengthEii
	.quad	_ZNK6icu_678Calendar19handleGetYearLengthEi
	.quad	_ZN6icu_6715ChineseCalendar21handleGetExtendedYearEv
	.quad	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields
	.quad	_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii
	.quad	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6715ChineseCalendar23getFieldResolutionTableEv
	.quad	_ZN6icu_6715ChineseCalendar19handleComputeFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar21getDefaultMonthInYearEi
	.quad	_ZN6icu_678Calendar20getDefaultDayInMonthEii
	.quad	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6715ChineseCalendar18haveDefaultCenturyEv
	.quad	_ZNK6icu_6715ChineseCalendar19defaultCenturyStartEv
	.quad	_ZNK6icu_6715ChineseCalendar23defaultCenturyStartYearEv
	.quad	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode
	.quad	_ZN6icu_678Calendar14setRelatedYearEi
	.quad	_ZNK6icu_6715ChineseCalendar14winterSolsticeEi
	.quad	_ZNK6icu_6715ChineseCalendar11newMoonNearEda
	.quad	_ZNK6icu_6715ChineseCalendar20synodicMonthsBetweenEii
	.quad	_ZNK6icu_6715ChineseCalendar14majorSolarTermEi
	.quad	_ZNK6icu_6715ChineseCalendar19hasNoMajorSolarTermEi
	.quad	_ZNK6icu_6715ChineseCalendar18isLeapMonthBetweenEii
	.quad	_ZN6icu_6715ChineseCalendar20computeChineseFieldsEiiia
	.quad	_ZNK6icu_6715ChineseCalendar7newYearEi
	.quad	_ZN6icu_6715ChineseCalendar11offsetMonthEiii
	.local	_ZZN6icu_6713DangiCalendar16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6713DangiCalendar16getStaticClassIDEvE7classID,1,1
	.local	_ZL22gDangiCalendarInitOnce
	.comm	_ZL22gDangiCalendarInitOnce,8,8
	.local	_ZL27gDangiCalendarZoneAstroCalc
	.comm	_ZL27gDangiCalendarZoneAstroCalc,8,8
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	744488960
	.long	-1031749620
	.align 8
.LC2:
	.long	2734686208
	.long	-1031764658
	.align 8
.LC3:
	.long	1065353216
	.long	-1032151582
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
