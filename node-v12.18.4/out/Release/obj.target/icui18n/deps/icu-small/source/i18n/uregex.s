	.file	"uregex.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RegularExpressionC2Ev
	.type	_ZN6icu_6717RegularExpressionC2Ev, @function
_ZN6icu_6717RegularExpressionC2Ev:
.LFB3037:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1919252592, (%rdi)
	movq	$0, 24(%rdi)
	movl	$0, 32(%rdi)
	movl	$0, 56(%rdi)
	movb	$0, 60(%rdi)
	movups	%xmm0, 8(%rdi)
	movups	%xmm0, 40(%rdi)
	ret
	.cfi_endproc
.LFE3037:
	.size	_ZN6icu_6717RegularExpressionC2Ev, .-_ZN6icu_6717RegularExpressionC2Ev
	.globl	_ZN6icu_6717RegularExpressionC1Ev
	.set	_ZN6icu_6717RegularExpressionC1Ev,_ZN6icu_6717RegularExpressionC2Ev
	.p2align 4
	.globl	uregex_pattern_67
	.type	uregex_pattern_67, @function
uregex_pattern_67:
.LFB3047:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L3
	testq	%rdi, %rdi
	je	.L5
	cmpl	$1919252592, (%rdi)
	jne	.L5
	testq	%rsi, %rsi
	je	.L7
	movl	32(%rdi), %eax
	movl	%eax, (%rsi)
.L7:
	movq	24(%rdi), %rax
.L3:
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, (%rdx)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3047:
	.size	uregex_pattern_67, .-uregex_pattern_67
	.p2align 4
	.globl	uregex_appendTail_67
	.type	uregex_appendTail_67, @function
uregex_appendTail_67:
.LFB3103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$56, %rsp
	movl	(%rcx), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L13
	cmpl	$15, %edx
	je	.L72
.L13:
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L12
	xorl	%r10d, %r10d
	testq	%r13, %r13
	je	.L16
.L78:
	cmpl	$1919252592, 0(%r13)
	jne	.L16
	movq	48(%r13), %rax
	testq	%rax, %rax
	je	.L73
.L18:
	testq	%r12, %r12
	je	.L16
	testq	%r14, %r14
	je	.L16
	movq	(%r14), %rdi
	movslq	(%r12), %r15
	movq	%rdi, -72(%rbp)
	testq	%rdi, %rdi
	je	.L74
.L19:
	testl	%r15d, %r15d
	js	.L16
	movq	40(%r13), %rcx
	movzbl	130(%rcx), %esi
	testq	%rax, %rax
	je	.L20
	movq	152(%rcx), %rdx
	testb	%sil, %sil
	cmovne	144(%rcx), %rdx
	cmpq	$-1, %rdx
	je	.L45
	movq	32(%rcx), %rdi
	movq	56(%rdi), %rax
	cmpq	$0, 72(%rax)
	je	.L75
	leaq	-60(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movb	%r10b, -81(%rbp)
	movl	$0, -60(%rbp)
	call	utext_extract_67@PLT
	movzbl	-81(%rbp), %r10d
	movl	%eax, %edx
	movq	(%r14), %rax
	movq	%rax, -80(%rbp)
.L22:
	movl	56(%r13), %esi
	cmpl	%esi, %edx
	je	.L76
	cmpl	$-1, %esi
	movq	48(%r13), %rdi
	sete	%r9b
	testl	%esi, %esi
	jg	.L26
	movslq	%edx, %rdx
	movq	-72(%rbp), %rcx
	movq	%rdx, %rax
	negq	%rax
	leaq	(%rcx,%rax,2), %r11
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L31:
	movzwl	(%rdi,%rdx,2), %ecx
	movl	%edx, %r8d
	testw	%cx, %cx
	jne	.L47
	testb	%r9b, %r9b
	jne	.L27
.L47:
	cmpl	%r15d, %eax
	jge	.L29
	movw	%cx, (%r11,%rdx,2)
.L29:
	addq	$1, %rdx
	addl	$1, %eax
	cmpl	%edx, %esi
	jne	.L31
.L24:
	cmpl	%r15d, %eax
	jge	.L37
	movq	-72(%rbp), %rsi
	movslq	%eax, %rdx
	xorl	%ecx, %ecx
	movw	%cx, (%rsi,%rdx,2)
	movq	-80(%rbp), %rsi
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rdx, (%r14)
	subl	%eax, (%r12)
.L38:
	testb	%r10b, %r10b
	je	.L12
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L12
	movl	$15, (%rbx)
	.p2align 4,,10
	.p2align 3
.L12:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L77
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movl	(%r12), %esi
	testl	%esi, %esi
	jne	.L12
	movl	$0, (%rcx)
	movl	$1, %r10d
	testq	%r13, %r13
	jne	.L78
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$1, (%rbx)
	xorl	%eax, %eax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L73:
	cmpb	$0, 60(%r13)
	jne	.L18
	movl	$66306, (%rbx)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L20:
	testb	%sil, %sil
	je	.L35
	movq	144(%rcx), %rsi
.L36:
	movq	48(%rcx), %rdx
	movq	32(%rcx), %rdi
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movq	-72(%rbp), %rcx
	movb	%r10b, -81(%rbp)
	call	utext_extract_67@PLT
	movq	(%r14), %rsi
	movzbl	-81(%rbp), %r10d
	movq	%rsi, -80(%rbp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$15, %edx
	movl	$-124, %ecx
	movq	-80(%rbp), %rsi
	cmove	%ecx, %edx
	movl	%edx, (%rbx)
	testq	%rsi, %rsi
	je	.L38
	leaq	(%rsi,%r15,2), %rdx
	movq	%rdx, (%r14)
	movl	$0, (%r12)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L74:
	testl	%r15d, %r15d
	jle	.L19
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L45:
	movq	-72(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, -80(%rbp)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L75:
	movq	-72(%rbp), %rax
	movq	%rax, -80(%rbp)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L27:
	movl	%r8d, 56(%r13)
	jmp	.L24
.L35:
	movq	152(%rcx), %rsi
	cmpq	$-1, %rsi
	cmove	%rax, %rsi
	jmp	.L36
.L76:
	xorl	%eax, %eax
	jmp	.L24
.L26:
	movslq	%edx, %r11
	movq	-72(%rbp), %rcx
	movq	%r11, %rax
	negq	%rax
	leaq	(%rcx,%rax,2), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L34:
	movzwl	(%rdi,%r11,2), %edx
	movl	%r11d, %r8d
	testw	%dx, %dx
	jne	.L32
	testb	%r9b, %r9b
	jne	.L27
.L32:
	cmpl	%eax, %r15d
	jne	.L79
	subl	%r8d, %esi
	leal	(%r15,%rsi), %eax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L79:
	movw	%dx, (%rcx,%r11,2)
	addq	$1, %r11
	addl	$1, %eax
	cmpl	%r11d, %esi
	jne	.L34
	jmp	.L24
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3103:
	.size	uregex_appendTail_67, .-uregex_appendTail_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717RegularExpressionD2Ev
	.type	_ZN6icu_6717RegularExpressionD2Ev, @function
_ZN6icu_6717RegularExpressionD2Ev:
.LFB3040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L81
	call	_ZN6icu_6712RegexMatcherD0Ev@PLT
.L81:
	movq	16(%rbx), %rax
	movq	$0, 40(%rbx)
	testq	%rax, %rax
	je	.L83
	lock subl	$1, (%rax)
	je	.L97
.L83:
	cmpb	$0, 60(%rbx)
	je	.L80
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L80
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L85
	call	_ZN6icu_6712RegexPatternD0Ev@PLT
.L85:
	movq	24(%rbx), %rdi
	call	uprv_free_67@PLT
	movq	16(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L83
	.cfi_endproc
.LFE3040:
	.size	_ZN6icu_6717RegularExpressionD2Ev, .-_ZN6icu_6717RegularExpressionD2Ev
	.globl	_ZN6icu_6717RegularExpressionD1Ev
	.set	_ZN6icu_6717RegularExpressionD1Ev,_ZN6icu_6717RegularExpressionD2Ev
	.p2align 4
	.globl	uregex_close_67
	.type	uregex_close_67, @function
uregex_close_67:
.LFB3045:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L119
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$1919252592, (%rdi)
	jne	.L98
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L100
	call	_ZN6icu_6712RegexMatcherD0Ev@PLT
.L100:
	movq	$0, 40(%r12)
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L102
	lock subl	$1, (%rax)
	je	.L122
.L102:
	cmpb	$0, 60(%r12)
	je	.L105
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L105
	call	uprv_free_67@PLT
.L105:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L104
	call	_ZN6icu_6712RegexPatternD0Ev@PLT
.L104:
	movq	24(%r12), %rdi
	call	uprv_free_67@PLT
	movq	16(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE3045:
	.size	uregex_close_67, .-uregex_close_67
	.p2align 4
	.globl	uregex_open_67
	.type	uregex_open_67, @function
uregex_open_67:
.LFB3043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movl	%edx, -216(%rbp)
	movl	(%r8), %r9d
	movq	%rcx, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L123
	cmpl	$-1, %esi
	movl	%esi, %r15d
	movq	%r8, %rbx
	setl	%dl
	testl	%esi, %esi
	sete	%al
	orb	%al, %dl
	jne	.L147
	movq	%rdi, %r13
	testq	%rdi, %rdi
	je	.L147
	movl	%esi, %edx
	cmpl	$-1, %esi
	je	.L159
.L127:
	movl	$64, %edi
	movl	%edx, -232(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movl	-232(%rbp), %edx
	movq	%rax, %r12
	leal	1(%rdx), %r14d
	movslq	%r14d, %r14
	addq	%r14, %r14
	testq	%rax, %rax
	je	.L128
	movl	$1919252592, (%rax)
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, 24(%rax)
	movl	$0, 32(%rax)
	movl	$0, 56(%rax)
	movb	$0, 60(%rax)
	movups	%xmm0, 8(%rax)
	movups	%xmm0, 40(%rax)
	call	uprv_malloc_67@PLT
	movq	%r14, %rdi
	movq	%rax, -240(%rbp)
	call	uprv_malloc_67@PLT
	movq	-240(%rbp), %r11
	movq	%rax, %r8
	testq	%r11, %r11
	je	.L129
	testq	%rax, %rax
	movl	-232(%rbp), %edx
	je	.L129
	movq	%r11, 16(%r12)
	movq	%r13, %rsi
	movq	%rax, %rdi
	movl	$1, (%r11)
	mfence
	movq	%rax, 24(%r12)
	movl	%r15d, 32(%r12)
	movq	%rax, -232(%rbp)
	call	u_memcpy_67@PLT
	movq	-232(%rbp), %r8
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	leaq	-208(%rbp), %rdi
	movslq	%r15d, %rdx
	movw	%cx, -2(%r8,%r14)
	movl	$18, %ecx
	leaq	-208(%rbp), %r14
	movq	%r8, %rsi
	rep stosq
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movl	$878368812, -208(%rbp)
	movl	$144, -196(%rbp)
	call	utext_openUChars_67@PLT
	movq	-224(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L132
	movl	-216(%rbp), %esi
	movq	%rbx, %rcx
	movq	%r14, %rdi
	call	_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode@PLT
	movq	%r14, %rdi
	movq	%rax, 8(%r12)
	call	utext_close_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L160
.L134:
	movq	8(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZNK6icu_6712RegexPattern7matcherER10UErrorCode@PLT
	movq	%rax, 40(%r12)
	movq	%rax, %rdi
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L123
.L135:
	endbr64
	testq	%rdi, %rdi
	je	.L136
.L162:
	call	_ZN6icu_6712RegexMatcherD0Ev@PLT
.L136:
	movq	$0, 40(%r12)
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L138
	lock subl	$1, (%rax)
	jne	.L138
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L140
	call	_ZN6icu_6712RegexPatternD0Ev@PLT
.L140:
	movq	24(%r12), %rdi
	call	uprv_free_67@PLT
	movq	16(%r12), %rdi
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L138:
	cmpb	$0, 60(%r12)
	je	.L141
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L141
	call	uprv_free_67@PLT
.L141:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L123:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L161
	addq	$200, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L132:
	movl	-216(%rbp), %esi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6712RegexPattern7compileEP5UTextjR10UErrorCode@PLT
	movq	%r14, %rdi
	movq	%rax, 8(%r12)
	call	utext_close_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L134
.L160:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L162
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L159:
	call	u_strlen_67@PLT
	movl	%eax, %edx
	jmp	.L127
.L161:
	call	__stack_chk_fail@PLT
.L129:
	movl	$7, (%rbx)
	movq	%r12, %rdi
	movq	%r8, -224(%rbp)
	movq	%r11, -216(%rbp)
	call	_ZN6icu_6717RegularExpressionD1Ev
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-224(%rbp), %r8
	movq	-216(%rbp), %r11
.L143:
	movq	%r11, %rdi
	movq	%r8, -216(%rbp)
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	movq	-216(%rbp), %r8
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L123
.L128:
	movl	$4, %edi
	call	uprv_malloc_67@PLT
	movq	%r14, %rdi
	movq	%rax, -216(%rbp)
	call	uprv_malloc_67@PLT
	movl	$7, (%rbx)
	movq	-216(%rbp), %r11
	movq	%rax, %r8
	jmp	.L143
	.cfi_endproc
.LFE3043:
	.size	uregex_open_67, .-uregex_open_67
	.p2align 4
	.globl	uregex_openUText_67
	.type	uregex_openUText_67, @function
uregex_openUText_67:
.LFB3044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L163
	movq	%rdi, %r14
	movq	%rcx, %rbx
	testq	%rdi, %rdi
	je	.L166
	movl	%esi, %r15d
	movq	%rdx, %r13
	call	utext_nativeLength_67@PLT
	testq	%rax, %rax
	movq	%rax, -232(%rbp)
	je	.L166
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-232(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L167
	movl	$1919252592, (%rax)
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movb	$0, 60(%rax)
	leaq	-212(%rbp), %r9
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	$0, 24(%rax)
	movl	$0, 32(%rax)
	movl	$0, 56(%rax)
	movups	%xmm0, 8(%rax)
	movups	%xmm0, 40(%rax)
	movq	%rdx, -256(%rbp)
	movl	$0, -212(%rbp)
	call	utext_extract_67@PLT
	movl	$4, %edi
	movl	%eax, -232(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, -248(%rbp)
	movl	-232(%rbp), %eax
	leal	1(%rax), %r8d
	movslq	%r8d, %rdi
	movl	%r8d, -240(%rbp)
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	-248(%rbp), %r9
	movq	%rax, %r11
	testq	%r9, %r9
	je	.L168
	testq	%rax, %rax
	movl	-240(%rbp), %r8d
	movq	-256(%rbp), %rdx
	je	.L168
	movq	%r9, 16(%r12)
	movq	%r11, %rcx
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	-208(%rbp), %r14
	movl	$1, (%r9)
	movq	%rbx, %r9
	mfence
	movq	%rax, 24(%r12)
	movq	%r11, -240(%rbp)
	movl	-232(%rbp), %eax
	movl	%eax, 32(%r12)
	call	utext_extract_67@PLT
	movq	-240(%rbp), %r11
	xorl	%eax, %eax
	leaq	-208(%rbp), %rdi
	movl	$18, %ecx
	movslq	-232(%rbp), %rdx
	rep stosq
	movq	%r11, %rsi
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movl	$878368812, -208(%rbp)
	movl	$144, -196(%rbp)
	call	utext_openUChars_67@PLT
	testq	%r13, %r13
	je	.L171
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode@PLT
	movq	%rax, 8(%r12)
.L172:
	movq	%r14, %rdi
	call	utext_close_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L199
	movq	8(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZNK6icu_6712RegexPattern7matcherER10UErrorCode@PLT
	movq	%rax, 40(%r12)
	movq	%rax, %rdi
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L163
.L174:
	endbr64
	testq	%rdi, %rdi
	je	.L175
.L201:
	call	_ZN6icu_6712RegexMatcherD0Ev@PLT
.L175:
	movq	$0, 40(%r12)
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L177
	lock subl	$1, (%rax)
	jne	.L177
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L179
	call	_ZN6icu_6712RegexPatternD0Ev@PLT
.L179:
	movq	24(%r12), %rdi
	call	uprv_free_67@PLT
	movq	16(%r12), %rdi
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L177:
	cmpb	$0, 60(%r12)
	je	.L180
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L180
	call	uprv_free_67@PLT
.L180:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L163:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L199:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L201
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%rbx, %rdx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6712RegexPattern7compileEP5UTextjR10UErrorCode@PLT
	movq	%rax, 8(%r12)
	jmp	.L172
.L200:
	call	__stack_chk_fail@PLT
.L167:
	leaq	-212(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	$0, -212(%rbp)
	movq	%r14, %rdi
	call	utext_extract_67@PLT
	movl	$4, %edi
	movl	%eax, %r12d
	call	uprv_malloc_67@PLT
	leal	1(%r12), %edi
	movslq	%edi, %rdi
	movq	%rax, -232(%rbp)
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movl	$7, (%rbx)
	movq	-232(%rbp), %r9
	movq	%rax, %r11
.L182:
	movq	%r9, %rdi
	movq	%r11, -232(%rbp)
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	movq	-232(%rbp), %r11
	movq	%r11, %rdi
	call	uprv_free_67@PLT
	jmp	.L163
.L168:
	movl	$7, (%rbx)
	movq	%r12, %rdi
	movq	%r11, -240(%rbp)
	movq	%r9, -232(%rbp)
	call	_ZN6icu_6717RegularExpressionD1Ev
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-240(%rbp), %r11
	movq	-232(%rbp), %r9
	jmp	.L182
	.cfi_endproc
.LFE3044:
	.size	uregex_openUText_67, .-uregex_openUText_67
	.p2align 4
	.globl	uregex_clone_67
	.type	uregex_clone_67, @function
uregex_clone_67:
.LFB3046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L202
	movq	%rdi, %rbx
	movq	%rsi, %r13
	testq	%rdi, %rdi
	je	.L204
	cmpl	$1919252592, (%rdi)
	jne	.L204
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L229
	pxor	%xmm0, %xmm0
	movb	$0, 60(%rax)
	movq	%r13, %rsi
	movl	$1919252592, (%rax)
	movq	$0, 24(%rax)
	movl	$0, 32(%rax)
	movl	$0, 56(%rax)
	movups	%xmm0, 8(%rax)
	movups	%xmm0, 40(%rax)
	movq	8(%rbx), %rdi
	call	_ZNK6icu_6712RegexPattern7matcherER10UErrorCode@PLT
	movl	0(%r13), %edx
	movq	%rax, 40(%r12)
	testl	%edx, %edx
	jg	.L230
	movdqu	8(%rbx), %xmm1
	movups	%xmm1, 8(%r12)
	movq	24(%rbx), %rax
	movq	%rax, 24(%r12)
	movl	32(%rbx), %eax
	movl	%eax, 32(%r12)
	movq	16(%rbx), %rax
	lock addl	$1, (%rax)
.L202:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	movl	$1, 0(%r13)
	xorl	%r12d, %r12d
	addq	$8, %rsp
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L208
	movq	%rax, %rdi
	call	_ZN6icu_6712RegexMatcherD0Ev@PLT
.L208:
	movq	$0, 40(%r12)
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L210
	lock subl	$1, (%rax)
	jne	.L210
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L212
	call	_ZN6icu_6712RegexPatternD0Ev@PLT
.L212:
	movq	24(%r12), %rdi
	call	uprv_free_67@PLT
	movq	16(%r12), %rdi
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L210:
	cmpb	$0, 60(%r12)
	je	.L213
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L213
	call	uprv_free_67@PLT
.L213:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L202
.L229:
	movl	$7, 0(%r13)
	jmp	.L202
	.cfi_endproc
.LFE3046:
	.size	uregex_clone_67, .-uregex_clone_67
	.p2align 4
	.globl	uregex_patternUText_67
	.type	uregex_patternUText_67, @function
uregex_patternUText_67:
.LFB3048:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexPattern11patternTextER10UErrorCode@PLT
	.cfi_endproc
.LFE3048:
	.size	uregex_patternUText_67, .-uregex_patternUText_67
	.p2align 4
	.globl	uregex_flags_67
	.type	uregex_flags_67, @function
uregex_flags_67:
.LFB3049:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L232
	testq	%rdi, %rdi
	je	.L234
	cmpl	$1919252592, (%rdi)
	jne	.L234
	movq	8(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexPattern5flagsEv@PLT
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$1, (%rsi)
.L232:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3049:
	.size	uregex_flags_67, .-uregex_flags_67
	.p2align 4
	.globl	uregex_setText_67
	.type	uregex_setText_67, @function
uregex_setText_67:
.LFB3050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L239
	movq	%rdi, %rbx
	movq	%rcx, %r12
	testq	%rdi, %rdi
	je	.L241
	cmpl	$1919252592, (%rdi)
	jne	.L241
	testq	%rsi, %rsi
	je	.L241
	cmpl	$-1, %edx
	jl	.L241
	cmpb	$0, 60(%rdi)
	je	.L244
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L244
	movl	%edx, -204(%rbp)
	movq	%rsi, -200(%rbp)
	call	uprv_free_67@PLT
	movl	-204(%rbp), %edx
	movq	-200(%rbp), %rsi
.L244:
	movq	%rsi, 48(%rbx)
	leaq	-192(%rbp), %r13
	xorl	%eax, %eax
	leaq	-192(%rbp), %rdi
	movl	%edx, 56(%rbx)
	movl	$18, %ecx
	movslq	%edx, %rdx
	rep stosq
	movb	$0, 60(%rbx)
	movq	%r12, %rcx
	movq	%r13, %rdi
	movl	$878368812, -192(%rbp)
	movl	$144, -180(%rbp)
	call	utext_openUChars_67@PLT
	movq	40(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6712RegexMatcher5resetEP5UText@PLT
	movq	%r13, %rdi
	call	utext_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L239:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L254
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	movl	$1, (%r12)
	jmp	.L239
.L254:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3050:
	.size	uregex_setText_67, .-uregex_setText_67
	.p2align 4
	.globl	uregex_setUText_67
	.type	uregex_setUText_67, @function
uregex_setUText_67:
.LFB3051:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L267
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rdi, %rdi
	je	.L257
	cmpl	$1919252592, (%rdi)
	jne	.L257
	testq	%rsi, %rsi
	je	.L257
	cmpb	$0, 60(%rdi)
	je	.L259
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L259
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movq	-24(%rbp), %rsi
.L259:
	movq	$0, 48(%rbx)
	movq	40(%rbx), %rdi
	movl	$-1, 56(%rbx)
	movb	$1, 60(%rbx)
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexMatcher5resetEP5UText@PLT
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movl	$1, (%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE3051:
	.size	uregex_setUText_67, .-uregex_setUText_67
	.p2align 4
	.globl	uregex_getText_67
	.type	uregex_getText_67, @function
uregex_getText_67:
.LFB3052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L270
	movq	%rdi, %rbx
	movq	%rdx, %r12
	testq	%rdi, %rdi
	je	.L272
	cmpl	$1919252592, (%rdi)
	jne	.L272
	movq	48(%rdi), %r13
	movq	%rsi, %r14
	testq	%r13, %r13
	je	.L286
.L274:
	testq	%r14, %r14
	je	.L270
	movl	56(%rbx), %eax
	movl	%eax, (%r14)
	.p2align 4,,10
	.p2align 3
.L270:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L287
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	movl	$1, (%r12)
	xorl	%r13d, %r13d
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L286:
	movq	40(%rdi), %rdi
	call	_ZNK6icu_6712RegexMatcher9inputTextEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	utext_nativeLength_67@PLT
	cmpq	$0, 32(%r15)
	movq	%rax, %rdx
	jne	.L275
	cmpq	%rax, 16(%r15)
	je	.L288
.L275:
	leaq	-60(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	movl	$0, -60(%rbp)
	call	utext_extract_67@PLT
	leal	1(%rax), %edi
	movl	%eax, 56(%rbx)
	movslq	%edi, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %r9
	xorl	%esi, %esi
	movq	%rax, %r13
	movl	56(%rbx), %eax
	movq	%r15, %rdi
	movq	%r13, %rcx
	leal	1(%rax), %r8d
	call	utext_extract_67@PLT
	movq	%r13, 48(%rbx)
	movb	$1, 60(%rbx)
	jmp	.L274
.L288:
	movslq	28(%r15), %rcx
	cmpq	%rdx, %rcx
	jne	.L275
	movq	48(%r15), %r13
	movl	%ecx, 56(%rbx)
	movb	$0, 60(%rbx)
	movq	%r13, 48(%rbx)
	jmp	.L274
.L287:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3052:
	.size	uregex_getText_67, .-uregex_getText_67
	.p2align 4
	.globl	uregex_getUText_67
	.type	uregex_getUText_67, @function
uregex_getUText_67:
.LFB3053:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	movq	%rsi, %rax
	testl	%ecx, %ecx
	jg	.L290
	testq	%rdi, %rdi
	je	.L291
	cmpl	$1919252592, (%rdi)
	jne	.L291
	movq	40(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexMatcher8getInputEP5UTextR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L291:
	movl	$1, (%rdx)
.L290:
	ret
	.cfi_endproc
.LFE3053:
	.size	uregex_getUText_67, .-uregex_getUText_67
	.p2align 4
	.globl	uregex_refreshUText_67
	.type	uregex_refreshUText_67, @function
uregex_refreshUText_67:
.LFB3054:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L295
	testq	%rdi, %rdi
	je	.L297
	cmpl	$1919252592, (%rdi)
	jne	.L297
	movq	40(%rdi), %rdi
	jmp	_ZN6icu_6712RegexMatcher16refreshInputTextEP5UTextR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L297:
	movl	$1, (%rdx)
.L295:
	ret
	.cfi_endproc
.LFE3054:
	.size	uregex_refreshUText_67, .-uregex_refreshUText_67
	.p2align 4
	.globl	uregex_matches64_67
	.type	uregex_matches64_67, @function
uregex_matches64_67:
.LFB3056:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L301
	testq	%rdi, %rdi
	je	.L303
	cmpl	$1919252592, (%rdi)
	jne	.L303
	cmpq	$0, 48(%rdi)
	je	.L311
.L305:
	movq	40(%rdi), %rdi
	cmpq	$-1, %rsi
	je	.L312
	jmp	_ZN6icu_6712RegexMatcher7matchesElR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L303:
	movl	$1, (%rdx)
.L301:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	cmpb	$0, 60(%rdi)
	jne	.L305
	movl	$66306, (%rdx)
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%rdx, %rsi
	jmp	_ZN6icu_6712RegexMatcher7matchesER10UErrorCode@PLT
	.cfi_endproc
.LFE3056:
	.size	uregex_matches64_67, .-uregex_matches64_67
	.p2align 4
	.globl	uregex_matches_67
	.type	uregex_matches_67, @function
uregex_matches_67:
.LFB3055:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L313
	testq	%rdi, %rdi
	je	.L315
	cmpl	$1919252592, (%rdi)
	jne	.L315
	cmpq	$0, 48(%rdi)
	je	.L323
.L317:
	movq	40(%rdi), %rdi
	movslq	%esi, %r8
	cmpl	$-1, %esi
	je	.L324
	movq	%r8, %rsi
	jmp	_ZN6icu_6712RegexMatcher7matchesElR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L315:
	movl	$1, (%rdx)
.L313:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	cmpb	$0, 60(%rdi)
	jne	.L317
	movl	$66306, (%rdx)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%rdx, %rsi
	jmp	_ZN6icu_6712RegexMatcher7matchesER10UErrorCode@PLT
	.cfi_endproc
.LFE3055:
	.size	uregex_matches_67, .-uregex_matches_67
	.p2align 4
	.globl	uregex_lookingAt64_67
	.type	uregex_lookingAt64_67, @function
uregex_lookingAt64_67:
.LFB3058:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L325
	testq	%rdi, %rdi
	je	.L327
	cmpl	$1919252592, (%rdi)
	jne	.L327
	cmpq	$0, 48(%rdi)
	je	.L335
.L329:
	movq	40(%rdi), %rdi
	cmpq	$-1, %rsi
	je	.L336
	jmp	_ZN6icu_6712RegexMatcher9lookingAtElR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L327:
	movl	$1, (%rdx)
.L325:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	cmpb	$0, 60(%rdi)
	jne	.L329
	movl	$66306, (%rdx)
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L336:
	movq	%rdx, %rsi
	jmp	_ZN6icu_6712RegexMatcher9lookingAtER10UErrorCode@PLT
	.cfi_endproc
.LFE3058:
	.size	uregex_lookingAt64_67, .-uregex_lookingAt64_67
	.p2align 4
	.globl	uregex_lookingAt_67
	.type	uregex_lookingAt_67, @function
uregex_lookingAt_67:
.LFB3057:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L337
	testq	%rdi, %rdi
	je	.L339
	cmpl	$1919252592, (%rdi)
	jne	.L339
	cmpq	$0, 48(%rdi)
	je	.L347
.L341:
	movq	40(%rdi), %rdi
	movslq	%esi, %r8
	cmpl	$-1, %esi
	je	.L348
	movq	%r8, %rsi
	jmp	_ZN6icu_6712RegexMatcher9lookingAtElR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L339:
	movl	$1, (%rdx)
.L337:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	cmpb	$0, 60(%rdi)
	jne	.L341
	movl	$66306, (%rdx)
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L348:
	movq	%rdx, %rsi
	jmp	_ZN6icu_6712RegexMatcher9lookingAtER10UErrorCode@PLT
	.cfi_endproc
.LFE3057:
	.size	uregex_lookingAt_67, .-uregex_lookingAt_67
	.p2align 4
	.globl	uregex_findNext_67
	.type	uregex_findNext_67, @function
uregex_findNext_67:
.LFB3061:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L349
	testq	%rdi, %rdi
	je	.L351
	cmpl	$1919252592, (%rdi)
	jne	.L351
	cmpq	$0, 48(%rdi)
	je	.L358
.L353:
	movq	40(%rdi), %rdi
	jmp	_ZN6icu_6712RegexMatcher4findER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L351:
	movl	$1, (%rsi)
.L349:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	cmpb	$0, 60(%rdi)
	jne	.L353
	movl	$66306, (%rsi)
	jmp	.L349
	.cfi_endproc
.LFE3061:
	.size	uregex_findNext_67, .-uregex_findNext_67
	.p2align 4
	.globl	uregex_find64_67
	.type	uregex_find64_67, @function
uregex_find64_67:
.LFB3060:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L370
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rdi, %rdi
	je	.L361
	cmpl	$1919252592, (%rdi)
	jne	.L361
	cmpq	$0, 48(%rdi)
	je	.L373
.L363:
	movq	40(%rbx), %rdi
	cmpq	$-1, %rsi
	je	.L374
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexMatcher4findElR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movl	$1, (%rdx)
.L359:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	cmpb	$0, 60(%rdi)
	jne	.L363
	movl	$66306, (%rdx)
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L374:
	movq	%rdx, -24(%rbp)
	call	_ZN6icu_6712RegexMatcher19resetPreserveRegionEv@PLT
	movq	-24(%rbp), %rdx
	movq	40(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	%rdx, %rsi
	jmp	_ZN6icu_6712RegexMatcher4findER10UErrorCode@PLT
	.cfi_endproc
.LFE3060:
	.size	uregex_find64_67, .-uregex_find64_67
	.p2align 4
	.globl	uregex_find_67
	.type	uregex_find_67, @function
uregex_find_67:
.LFB3059:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L386
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rdi, %rdi
	je	.L377
	cmpl	$1919252592, (%rdi)
	jne	.L377
	cmpq	$0, 48(%rdi)
	je	.L389
.L379:
	movq	40(%rbx), %rdi
	movslq	%esi, %r8
	cmpl	$-1, %esi
	je	.L390
	addq	$24, %rsp
	movq	%r8, %rsi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexMatcher4findElR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	movl	$1, (%rdx)
.L375:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	cmpb	$0, 60(%rdi)
	jne	.L379
	movl	$66306, (%rdx)
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L390:
	movq	%rdx, -24(%rbp)
	call	_ZN6icu_6712RegexMatcher19resetPreserveRegionEv@PLT
	movq	-24(%rbp), %rdx
	movq	40(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	%rdx, %rsi
	jmp	_ZN6icu_6712RegexMatcher4findER10UErrorCode@PLT
	.cfi_endproc
.LFE3059:
	.size	uregex_find_67, .-uregex_find_67
	.p2align 4
	.globl	uregex_groupCount_67
	.type	uregex_groupCount_67, @function
uregex_groupCount_67:
.LFB3062:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L391
	testq	%rdi, %rdi
	je	.L393
	cmpl	$1919252592, (%rdi)
	jne	.L393
	movq	40(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexMatcher10groupCountEv@PLT
	.p2align 4,,10
	.p2align 3
.L393:
	movl	$1, (%rsi)
.L391:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3062:
	.size	uregex_groupCount_67, .-uregex_groupCount_67
	.p2align 4
	.globl	uregex_groupNumberFromName_67
	.type	uregex_groupNumberFromName_67, @function
uregex_groupNumberFromName_67:
.LFB3063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$104, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L398
	movq	%rcx, %r12
	testq	%rdi, %rdi
	je	.L400
	cmpl	$1919252592, (%rdi)
	jne	.L400
	movq	8(%rdi), %r14
	leaq	-112(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6712RegexPattern19groupNumberFromNameERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r13, %rdi
	movl	%eax, -116(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-116(%rbp), %eax
.L398:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L407
	addq	$104, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L398
.L407:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3063:
	.size	uregex_groupNumberFromName_67, .-uregex_groupNumberFromName_67
	.p2align 4
	.globl	uregex_groupNumberFromCName_67
	.type	uregex_groupNumberFromCName_67, @function
uregex_groupNumberFromCName_67:
.LFB3064:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L408
	testq	%rdi, %rdi
	je	.L410
	cmpl	$1919252592, (%rdi)
	jne	.L410
	movq	8(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexPattern19groupNumberFromNameEPKciR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L410:
	movl	$1, (%rcx)
.L408:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3064:
	.size	uregex_groupNumberFromCName_67, .-uregex_groupNumberFromCName_67
	.p2align 4
	.globl	uregex_start64_67
	.type	uregex_start64_67, @function
uregex_start64_67:
.LFB3068:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L415
	testq	%rdi, %rdi
	je	.L417
	cmpl	$1919252592, (%rdi)
	jne	.L417
	cmpq	$0, 48(%rdi)
	je	.L424
.L419:
	movq	40(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexMatcher7start64EiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L417:
	movl	$1, (%rdx)
.L415:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	cmpb	$0, 60(%rdi)
	jne	.L419
	movl	$66306, (%rdx)
	jmp	.L415
	.cfi_endproc
.LFE3068:
	.size	uregex_start64_67, .-uregex_start64_67
	.p2align 4
	.globl	uregex_start_67
	.type	uregex_start_67, @function
uregex_start_67:
.LFB3067:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L435
	testq	%rdi, %rdi
	je	.L427
	cmpl	$1919252592, (%rdi)
	jne	.L427
	cmpq	$0, 48(%rdi)
	je	.L437
.L429:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	40(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6712RegexMatcher7start64EiR10UErrorCode@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L427:
	.cfi_restore 6
	movl	$1, (%rdx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L435:
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	cmpb	$0, 60(%rdi)
	jne	.L429
	movl	$66306, (%rdx)
	ret
	.cfi_endproc
.LFE3067:
	.size	uregex_start_67, .-uregex_start_67
	.p2align 4
	.type	uregex_group_67.part.0, @function
uregex_group_67.part.0:
.LFB3971:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	40(%rdi), %rdi
	movq	%r8, %rdx
	testl	%ecx, %ecx
	je	.L439
	cmpq	$0, 48(%r12)
	je	.L440
.L439:
	call	_ZNK6icu_6712RegexMatcher5startEiR10UErrorCode@PLT
	movq	40(%r12), %rdi
	movq	%rbx, %rdx
	movl	%r13d, %esi
	movslq	%eax, %r14
	call	_ZNK6icu_6712RegexMatcher3endEiR10UErrorCode@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L441
.L446:
	xorl	%r13d, %r13d
.L438:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	subl	%r14d, %eax
	movl	%eax, %r13d
	cmpl	%eax, %r15d
	jg	.L451
	movl	$15, %eax
	movl	$-124, %edx
	cmove	%edx, %eax
	movl	%eax, (%rbx)
.L444:
	testl	%r15d, %r15d
	jle	.L438
	movq	48(%r12), %rax
	movq	-56(%rbp), %rdi
	movl	%r15d, %edx
	leaq	(%rax,%r14,2), %rsi
	call	u_memcpy_67@PLT
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L451:
	movq	-56(%rbp), %rcx
	cltq
	xorl	%edx, %edx
	movl	%r13d, %r15d
	movw	%dx, (%rcx,%rax,2)
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L440:
	call	_ZNK6icu_6712RegexMatcher7start64EiR10UErrorCode@PLT
	movq	40(%r12), %rdi
	movq	%rbx, %rdx
	movl	%r13d, %esi
	movq	%rax, -64(%rbp)
	call	_ZNK6icu_6712RegexMatcher5end64EiR10UErrorCode@PLT
	movq	%rax, %r14
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L446
	movq	40(%r12), %rdi
	call	_ZNK6icu_6712RegexMatcher9inputTextEv@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	%rbx, %r9
	addq	$24, %rsp
	movl	%r15d, %r8d
	movq	%r14, %rdx
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	utext_extract_67@PLT
	.cfi_endproc
.LFE3971:
	.size	uregex_group_67.part.0, .-uregex_group_67.part.0
	.p2align 4
	.globl	uregex_group_67
	.type	uregex_group_67, @function
uregex_group_67:
.LFB3065:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L452
	testq	%rdi, %rdi
	je	.L454
	cmpl	$1919252592, (%rdi)
	jne	.L454
	cmpq	$0, 48(%rdi)
	je	.L466
.L456:
	testl	%ecx, %ecx
	js	.L454
	jle	.L459
	testq	%rdx, %rdx
	jne	.L459
	.p2align 4,,10
	.p2align 3
.L454:
	movl	$1, (%r8)
.L452:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	cmpb	$0, 60(%rdi)
	jne	.L456
	movl	$66306, (%r8)
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L459:
	jmp	uregex_group_67.part.0
	.cfi_endproc
.LFE3065:
	.size	uregex_group_67, .-uregex_group_67
	.section	.text._ZN6icu_6710RegexCImpl17appendReplacementEPNS_17RegularExpressionEPKDsiPPDsPiP10UErrorCode,"axG",@progbits,_ZN6icu_6710RegexCImpl17appendReplacementEPNS_17RegularExpressionEPKDsiPPDsPiP10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6710RegexCImpl17appendReplacementEPNS_17RegularExpressionEPKDsiPPDsPiP10UErrorCode
	.type	_ZN6icu_6710RegexCImpl17appendReplacementEPNS_17RegularExpressionEPKDsiPPDsPiP10UErrorCode, @function
_ZN6icu_6710RegexCImpl17appendReplacementEPNS_17RegularExpressionEPKDsiPPDsPiP10UErrorCode:
.LFB3099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$184, %rsp
	movq	%rcx, -160(%rbp)
	movq	%r8, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	testq	%r8, %r8
	je	.L468
	cmpl	$15, %eax
	je	.L635
.L468:
	testl	%eax, %eax
	jg	.L467
	movb	$0, -169(%rbp)
	testq	%r11, %r11
	je	.L471
.L643:
	cmpl	$1919252592, (%r11)
	jne	.L471
	movq	48(%r11), %rdx
	testq	%rdx, %rdx
	je	.L636
.L473:
	testq	%r12, %r12
	je	.L471
	cmpl	$-1, %r14d
	jl	.L471
	movq	-152(%rbp), %rax
	testq	%rax, %rax
	je	.L471
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L471
	movq	(%rdi), %rdi
	movl	(%rax), %eax
	movq	%rdi, -192(%rbp)
	movl	%eax, -180(%rbp)
	testq	%rdi, %rdi
	je	.L637
.L474:
	movl	-180(%rbp), %r13d
	testl	%r13d, %r13d
	js	.L471
	movq	40(%r11), %rax
	cmpb	$0, 130(%rax)
	movq	%rax, -208(%rbp)
	je	.L475
	cmpl	$-1, %r14d
	je	.L638
.L476:
	movq	-208(%rbp), %rsi
	movq	152(%rsi), %r13
	movq	32(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L477
	movq	56(%rdi), %rax
	cmpq	$0, 72(%rax)
	je	.L639
	leaq	-132(%rbp), %r15
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %r9
	xorl	%esi, %esi
	movq	%r11, -168(%rbp)
	movl	$0, -132(%rbp)
	call	utext_extract_67@PLT
	movq	%r15, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$0, -132(%rbp)
	movl	%eax, %r13d
	movq	-208(%rbp), %rax
	movq	136(%rax), %rdx
	movq	152(%rax), %rsi
	movq	32(%rax), %rdi
	call	utext_extract_67@PLT
	movq	-168(%rbp), %r11
	leal	(%rax,%r13), %r9d
.L479:
	cmpl	%r13d, %r9d
	jle	.L548
	movl	-180(%rbp), %eax
	movq	48(%r11), %r8
	testl	%eax, %eax
	jle	.L481
	movl	%r9d, %ecx
	movq	-192(%rbp), %r10
	subl	%r13d, %ecx
	cmpl	%eax, %ecx
	cmovg	%eax, %ecx
	movslq	%r13d, %rax
	leaq	(%r8,%rax,2), %rdi
	leaq	16(%r8,%rax,2), %rax
	cmpq	%rax, %r10
	leaq	16(%r10), %rax
	setnb	%dl
	cmpq	%rax, %rdi
	setnb	%al
	orb	%al, %dl
	je	.L549
	cmpl	$7, %ecx
	jle	.L549
	testl	%ecx, %ecx
	movl	$1, %eax
	cmovg	%ecx, %eax
	xorl	%edx, %edx
	movl	%eax, %esi
	shrl	$3, %esi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L483:
	movdqu	(%rdi,%rdx), %xmm0
	movups	%xmm0, (%r10,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L483
	movl	%eax, %edx
	andl	$-8, %edx
	leal	0(%r13,%rdx), %esi
	cmpl	%eax, %edx
	je	.L481
	movslq	%esi, %rax
	movq	-192(%rbp), %r10
	movzwl	(%r8,%rax,2), %edi
	movl	%edx, %eax
	movw	%di, (%r10,%rax,2)
	leal	1(%rdx), %eax
	leal	1(%rsi), %edi
	cmpl	%ecx, %eax
	jge	.L481
	movslq	%edi, %rdi
	cltq
	movzwl	(%r8,%rdi,2), %edi
	movw	%di, (%r10,%rax,2)
	leal	2(%rdx), %eax
	leal	2(%rsi), %edi
	cmpl	%ecx, %eax
	jge	.L481
	movslq	%edi, %rdi
	cltq
	movzwl	(%r8,%rdi,2), %edi
	movw	%di, (%r10,%rax,2)
	leal	3(%rdx), %eax
	leal	3(%rsi), %edi
	cmpl	%ecx, %eax
	jge	.L481
	movslq	%edi, %rdi
	cltq
	movzwl	(%r8,%rdi,2), %edi
	movw	%di, (%r10,%rax,2)
	leal	4(%rdx), %eax
	leal	4(%rsi), %edi
	cmpl	%ecx, %eax
	jge	.L481
	movslq	%edi, %rdi
	cltq
	movzwl	(%r8,%rdi,2), %edi
	movw	%di, (%r10,%rax,2)
	leal	5(%rdx), %edi
	leal	5(%rsi), %eax
	cmpl	%ecx, %edi
	jge	.L481
	cltq
	movslq	%edi, %rdi
	addl	$6, %edx
	leaq	(%rax,%rax), %rsi
	movzwl	(%r8,%rax,2), %eax
	movw	%ax, (%r10,%rdi,2)
	cmpl	%ecx, %edx
	jge	.L481
	movzwl	2(%r8,%rsi), %eax
	movslq	%edx, %rdx
	movw	%ax, (%r10,%rdx,2)
	.p2align 4,,10
	.p2align 3
.L481:
	subl	%r13d, %r9d
	movl	%r9d, %r15d
.L480:
	movl	$0, -132(%rbp)
	xorl	%eax, %eax
	testl	%r14d, %r14d
	jle	.L488
	movl	%r15d, -168(%rbp)
	movq	%r11, -200(%rbp)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L641:
	cmpw	$92, %cx
	jne	.L640
.L621:
	cmpl	%r14d, %edx
	jge	.L494
	movzwl	2(%r12,%rdi), %r13d
	movl	%r13d, %eax
	andl	$-33, %eax
	cmpw	$85, %ax
	jne	.L495
	movq	uregex_ucstr_unescape_charAt_67@GOTPCREL(%rip), %rdi
	movl	%r14d, %edx
	leaq	-132(%rbp), %rsi
	movq	%r12, %rcx
	call	u_unescapeAt_67@PLT
	movl	-132(%rbp), %edx
	cmpl	$-1, %eax
	je	.L495
	movl	-168(%rbp), %edi
	movl	-180(%rbp), %esi
	cmpl	$65535, %eax
	jg	.L497
	cmpl	%esi, %edi
	jge	.L498
	movslq	%edi, %rdx
	movq	-192(%rbp), %rdi
	movw	%ax, (%rdi,%rdx,2)
.L498:
	addl	$1, -168(%rbp)
	movl	-132(%rbp), %eax
	.p2align 4,,10
	.p2align 3
.L492:
	cmpl	%eax, %r14d
	jle	.L626
.L487:
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L632
	movslq	%eax, %rsi
	leal	1(%rax), %edx
	movzwl	(%r12,%rsi,2), %ecx
	movl	%edx, -132(%rbp)
	leaq	(%rsi,%rsi), %rdi
	cmpw	$36, %cx
	jne	.L641
	cmpw	$92, %cx
	je	.L621
	cmpl	%r14d, %edx
	jl	.L502
	movl	$-1, %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	jne	.L508
.L503:
	movl	$66325, (%rbx)
	movl	-168(%rbp), %r15d
.L489:
	cmpl	-180(%rbp), %r15d
	jge	.L536
.L542:
	movq	-192(%rbp), %rdi
	movslq	%r15d, %rax
	xorl	%ecx, %ecx
	movw	%cx, (%rdi,%rax,2)
.L536:
	testl	%r15d, %r15d
	jle	.L540
	movq	-152(%rbp), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L540
	movq	-160(%rbp), %rsi
	movq	(%rsi), %rdx
	cmpl	%r15d, -180(%rbp)
	jle	.L541
	movslq	%r15d, %rcx
	subl	%r15d, %eax
	leaq	(%rdx,%rcx,2), %rdx
	movq	%rdx, (%rsi)
	movl	%eax, (%rdi)
	.p2align 4,,10
	.p2align 3
.L540:
	cmpb	$0, -169(%rbp)
	je	.L467
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L467
.L544:
	movl	$15, (%rbx)
	.p2align 4,,10
	.p2align 3
.L467:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L642
	addq	$184, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L635:
	.cfi_restore_state
	movl	(%r8), %eax
	testl	%eax, %eax
	jne	.L467
	movl	$0, (%r9)
	movb	$1, -169(%rbp)
	testq	%r11, %r11
	jne	.L643
	.p2align 4,,10
	.p2align 3
.L471:
	movl	$1, (%rbx)
	xorl	%r15d, %r15d
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L636:
	cmpb	$0, 60(%r11)
	jne	.L473
.L475:
	movl	$66306, (%rbx)
	xorl	%r15d, %r15d
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L637:
	testl	%eax, %eax
	jle	.L474
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L640:
	movslq	-168(%rbp), %rax
	movl	-180(%rbp), %edi
	cmpl	%edi, %eax
	jge	.L491
	movq	-192(%rbp), %rsi
	movw	%cx, (%rsi,%rax,2)
.L491:
	addl	$1, -168(%rbp)
	movl	%edx, %eax
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L502:
	movzwl	2(%r12,%rdi), %r13d
	movl	%r13d, %r8d
	movl	%r13d, %ecx
	andl	$63488, %r8d
	cmpl	$55296, %r8d
	je	.L644
.L506:
	movl	%r13d, %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	jne	.L508
	cmpl	$123, %r13d
	jne	.L503
	movslq	-132(%rbp), %rdx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r9d
	movq	%rax, -128(%rbp)
	movq	%rdx, %rcx
	leal	1(%rdx), %eax
	leaq	(%rdx,%rdx), %rsi
	movzwl	(%r12,%rdx,2), %edx
	movw	%r9w, -120(%rbp)
	andl	$-1024, %edx
	movl	%eax, -132(%rbp)
	cmpl	$55296, %edx
	jne	.L515
	cmpl	%r14d, %eax
	jne	.L645
.L515:
	movl	(%rbx), %r8d
	leaq	-128(%rbp), %r15
	testl	%r8d, %r8d
	jg	.L634
	.p2align 4,,10
	.p2align 3
.L516:
	cmpl	%eax, %r14d
	jle	.L524
	movslq	%eax, %rdx
	leal	1(%rax), %ecx
	movzwl	(%r12,%rdx,2), %r13d
	leaq	(%rdx,%rdx), %rsi
	movl	%ecx, -132(%rbp)
	movl	%r13d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L519
	cmpl	%r14d, %ecx
	jne	.L646
.L519:
	movl	%r13d, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpl	$25, %eax
	jbe	.L521
.L520:
	leal	-49(%r13), %eax
	cmpl	$8, %eax
	jbe	.L521
	cmpl	$125, %r13d
	je	.L647
.L524:
	movl	$66325, (%rbx)
.L634:
	xorl	%r13d, %r13d
.L517:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L504:
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L632
	movq	-192(%rbp), %rsi
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L527
	movslq	-168(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
.L527:
	movq	-200(%rbp), %rax
	cmpl	$1919252592, (%rax)
	jne	.L531
	cmpq	$0, 48(%rax)
	je	.L648
.L530:
	movl	-180(%rbp), %ecx
	subl	-168(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L554
	testq	%rdx, %rdx
	jne	.L554
	.p2align 4,,10
	.p2align 3
.L531:
	movl	$1, (%rbx)
	movl	-168(%rbp), %r15d
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L521:
	movq	%r15, %rdi
	movl	%r13d, %esi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L634
	cmpl	$125, %r13d
	je	.L634
	movl	-132(%rbp), %eax
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L495:
	movslq	-168(%rbp), %rax
	movl	-180(%rbp), %esi
	cmpl	%esi, %eax
	jge	.L501
	movq	-192(%rbp), %rdi
	movw	%r13w, (%rdi,%rax,2)
.L501:
	leal	1(%rdx), %eax
	addl	$1, -168(%rbp)
	movl	%eax, -132(%rbp)
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L507:
	testl	%edx, %edx
	jle	.L506
	movzwl	(%r12,%rsi,2), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L506
	sall	$10, %eax
	leal	-56613888(%r13,%rax), %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	je	.L503
	.p2align 4,,10
	.p2align 3
.L508:
	movq	-208(%rbp), %rax
	movq	8(%rax), %rax
	movq	136(%rax), %rax
	movl	8(%rax), %eax
	movl	%eax, -176(%rbp)
	movl	-132(%rbp), %eax
	cmpl	%eax, %r14d
	jle	.L551
	movq	%rbx, -216(%rbp)
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L510:
	movl	%ebx, %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	je	.L628
	movl	%ebx, %edi
	call	u_charDigitValue_67@PLT
	leal	0(%r13,%r13,4), %edx
	leal	(%rax,%rdx,2), %ecx
	cmpl	-176(%rbp), %ecx
	jg	.L512
	movslq	-132(%rbp), %rdx
	leal	1(%rdx), %eax
	movq	%rdx, %rsi
	leaq	(%rdx,%rdx), %rdi
	movzwl	(%r12,%rdx,2), %edx
	movl	%eax, -132(%rbp)
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L513
	cmpl	%r14d, %eax
	jne	.L649
.L513:
	addl	$1, %r15d
	cmpl	%eax, %r14d
	jle	.L552
	movl	%ecx, %r13d
.L505:
	movslq	%eax, %rdx
	movzwl	(%r12,%rdx,2), %ebx
	leaq	(%rdx,%rdx), %rsi
	movl	%ebx, %ecx
	movl	%ebx, %edx
	andl	$63488, %ecx
	cmpl	$55296, %ecx
	jne	.L510
	andb	$4, %dh
	jne	.L511
	addl	$1, %eax
	cmpl	%r14d, %eax
	je	.L510
	movzwl	2(%r12,%rsi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L510
	sall	$10, %ebx
	leal	-56613888(%rax,%rbx), %ebx
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L649:
	movzwl	2(%r12,%rdi), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L513
	leal	2(%rsi), %eax
	movl	%eax, -132(%rbp)
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L511:
	testl	%eax, %eax
	jle	.L510
	movzwl	-2(%r12,%rsi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L510
	sall	$10, %eax
	leal	-56613888(%rbx,%rax), %ebx
	jmp	.L510
.L554:
	testl	%ecx, %ecx
	movl	$0, %eax
	movq	%rbx, %r8
	movl	%r13d, %esi
	cmovs	%eax, %ecx
	movq	-200(%rbp), %rdi
	call	uregex_group_67.part.0
	addl	%eax, -168(%rbp)
	movl	(%rbx), %eax
	cmpl	$15, %eax
	jne	.L533
	movl	$0, (%rbx)
	movl	-132(%rbp), %eax
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L533:
	testl	%eax, %eax
	jg	.L632
	movl	-132(%rbp), %eax
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L646:
	movzwl	2(%r12,%rsi), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L520
	addl	$2, %eax
	sall	$10, %r13d
	movl	%eax, -132(%rbp)
	leal	-56613888(%rdx,%r13), %r13d
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L644:
	andb	$4, %ch
	jne	.L507
	addl	$2, %eax
	cmpl	%r14d, %eax
	je	.L506
	movzwl	4(%r12,%rdi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L506
	sall	$10, %r13d
	leal	-56613888(%rax,%r13), %r13d
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L647:
	movq	-200(%rbp), %rax
	movq	8(%rax), %rax
	movq	192(%rax), %rdi
	testq	%rdi, %rdi
	je	.L524
	movq	%r15, %rsi
	call	uhash_geti_67@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L524
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L628:
	movq	-216(%rbp), %rbx
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L512:
	movq	-216(%rbp), %rbx
	testl	%r15d, %r15d
	jne	.L504
	movl	$8, (%rbx)
	movl	-168(%rbp), %r15d
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L552:
	movq	-216(%rbp), %rbx
	movl	%ecx, %r13d
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L497:
	cmpl	%esi, %edi
	jge	.L499
	movl	%eax, %edx
	movslq	%edi, %rcx
	movq	-192(%rbp), %rdi
	sarl	$10, %edx
	subw	$10304, %dx
	movw	%dx, (%rdi,%rcx,2)
.L499:
	movl	-168(%rbp), %edi
	leal	1(%rdi), %edx
	cmpl	-180(%rbp), %edx
	jge	.L500
	movq	-192(%rbp), %rdi
	andw	$1023, %ax
	movslq	%edx, %rdx
	orw	$-9216, %ax
	movw	%ax, (%rdi,%rdx,2)
.L500:
	addl	$2, -168(%rbp)
	movl	-132(%rbp), %eax
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L626:
	movl	-168(%rbp), %r15d
.L488:
	cmpl	-180(%rbp), %r15d
	jl	.L542
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L536
.L543:
	movq	-152(%rbp), %rax
	cmpl	%r15d, (%rax)
	je	.L650
	movl	$15, (%rbx)
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L632:
	movl	-168(%rbp), %r15d
	jmp	.L489
.L639:
	movl	136(%rsi), %r9d
	jmp	.L479
.L477:
	movq	136(%rsi), %rdx
	movl	-180(%rbp), %r8d
	movq	%r13, %rsi
	leaq	-132(%rbp), %r9
	movq	-192(%rbp), %rcx
	movq	%r11, -168(%rbp)
	movl	$0, -132(%rbp)
	call	utext_extract_67@PLT
	movq	-168(%rbp), %r11
	movl	%eax, %r15d
	jmp	.L480
.L638:
	movq	%r12, %rdi
	movq	%r11, -168(%rbp)
	call	u_strlen_67@PLT
	movq	-168(%rbp), %r11
	movl	%eax, %r14d
	movq	48(%r11), %rdx
	jmp	.L476
.L650:
	movl	$-124, (%rbx)
	testl	%r15d, %r15d
	jle	.L651
	movq	-160(%rbp), %rax
	movq	(%rax), %rdx
.L541:
	movslq	-180(%rbp), %rax
	movq	-160(%rbp), %rdi
	leaq	(%rdx,%rax,2), %rax
	movq	%rax, (%rdi)
	movq	-152(%rbp), %rax
	movl	$0, (%rax)
	jmp	.L540
.L648:
	cmpb	$0, 60(%rax)
	jne	.L530
	movl	$66306, (%rbx)
	movl	-168(%rbp), %r15d
	jmp	.L489
.L645:
	movzwl	2(%r12,%rsi), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L515
	leal	2(%rcx), %eax
	movl	%eax, -132(%rbp)
	jmp	.L515
.L549:
	movq	-192(%rbp), %rsi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L482:
	movzwl	(%rdi,%rdx,2), %eax
	movw	%ax, (%rsi,%rdx,2)
	addq	$1, %rdx
	cmpl	%edx, %ecx
	jg	.L482
	jmp	.L481
.L494:
	movl	-168(%rbp), %r15d
	cmpl	-180(%rbp), %r15d
	jge	.L543
	jmp	.L542
.L548:
	xorl	%r15d, %r15d
	jmp	.L480
.L551:
	xorl	%r13d, %r13d
	jmp	.L504
.L651:
	cmpb	$0, -169(%rbp)
	je	.L467
	jmp	.L544
.L642:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3099:
	.size	_ZN6icu_6710RegexCImpl17appendReplacementEPNS_17RegularExpressionEPKDsiPPDsPiP10UErrorCode, .-_ZN6icu_6710RegexCImpl17appendReplacementEPNS_17RegularExpressionEPKDsiPPDsPiP10UErrorCode
	.text
	.p2align 4
	.globl	uregex_appendReplacement_67
	.type	uregex_appendReplacement_67, @function
uregex_appendReplacement_67:
.LFB3100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$184, %rsp
	movq	%rcx, -160(%rbp)
	movq	%r8, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	testq	%r8, %r8
	je	.L653
	cmpl	$15, %eax
	je	.L820
.L653:
	testl	%eax, %eax
	jg	.L652
	movb	$0, -169(%rbp)
	testq	%r11, %r11
	je	.L656
.L828:
	cmpl	$1919252592, (%r11)
	jne	.L656
	movq	48(%r11), %rdx
	testq	%rdx, %rdx
	je	.L821
.L658:
	testq	%r12, %r12
	je	.L656
	cmpl	$-1, %r14d
	jl	.L656
	movq	-152(%rbp), %rax
	testq	%rax, %rax
	je	.L656
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L656
	movq	(%rdi), %rdi
	movl	(%rax), %eax
	movq	%rdi, -192(%rbp)
	movl	%eax, -180(%rbp)
	testq	%rdi, %rdi
	je	.L822
.L659:
	movl	-180(%rbp), %r13d
	testl	%r13d, %r13d
	js	.L656
	movq	40(%r11), %rax
	cmpb	$0, 130(%rax)
	movq	%rax, -208(%rbp)
	je	.L660
	cmpl	$-1, %r14d
	je	.L823
.L661:
	movq	-208(%rbp), %rsi
	movq	152(%rsi), %r13
	movq	32(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L662
	movq	56(%rdi), %rax
	cmpq	$0, 72(%rax)
	je	.L824
	leaq	-132(%rbp), %r15
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %r9
	xorl	%esi, %esi
	movq	%r11, -168(%rbp)
	movl	$0, -132(%rbp)
	call	utext_extract_67@PLT
	movq	%r15, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$0, -132(%rbp)
	movl	%eax, %r13d
	movq	-208(%rbp), %rax
	movq	136(%rax), %rdx
	movq	152(%rax), %rsi
	movq	32(%rax), %rdi
	call	utext_extract_67@PLT
	movq	-168(%rbp), %r11
	leal	0(%r13,%rax), %r9d
.L664:
	cmpl	%r13d, %r9d
	jle	.L733
	movl	-180(%rbp), %eax
	movq	48(%r11), %r8
	testl	%eax, %eax
	jle	.L666
	movl	%r9d, %ecx
	movq	-192(%rbp), %r10
	subl	%r13d, %ecx
	cmpl	%eax, %ecx
	cmovg	%eax, %ecx
	movslq	%r13d, %rax
	leaq	(%r8,%rax,2), %rdi
	leaq	16(%r8,%rax,2), %rax
	cmpq	%rax, %r10
	leaq	16(%r10), %rax
	setnb	%dl
	cmpq	%rax, %rdi
	setnb	%al
	orb	%al, %dl
	je	.L734
	cmpl	$7, %ecx
	jle	.L734
	testl	%ecx, %ecx
	movl	$1, %eax
	cmovg	%ecx, %eax
	xorl	%edx, %edx
	movl	%eax, %esi
	shrl	$3, %esi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L668:
	movdqu	(%rdi,%rdx), %xmm0
	movups	%xmm0, (%r10,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L668
	movl	%eax, %edx
	andl	$-8, %edx
	leal	0(%r13,%rdx), %esi
	cmpl	%eax, %edx
	je	.L666
	movslq	%esi, %rax
	movq	-192(%rbp), %r10
	movzwl	(%r8,%rax,2), %edi
	movl	%edx, %eax
	movw	%di, (%r10,%rax,2)
	leal	1(%rdx), %eax
	leal	1(%rsi), %edi
	cmpl	%ecx, %eax
	jge	.L666
	movslq	%edi, %rdi
	cltq
	movzwl	(%r8,%rdi,2), %edi
	movw	%di, (%r10,%rax,2)
	leal	2(%rdx), %eax
	leal	2(%rsi), %edi
	cmpl	%ecx, %eax
	jge	.L666
	movslq	%edi, %rdi
	cltq
	movzwl	(%r8,%rdi,2), %edi
	movw	%di, (%r10,%rax,2)
	leal	3(%rdx), %eax
	leal	3(%rsi), %edi
	cmpl	%ecx, %eax
	jge	.L666
	movslq	%edi, %rdi
	cltq
	movzwl	(%r8,%rdi,2), %edi
	movw	%di, (%r10,%rax,2)
	leal	4(%rdx), %eax
	leal	4(%rsi), %edi
	cmpl	%ecx, %eax
	jge	.L666
	movslq	%edi, %rdi
	cltq
	movzwl	(%r8,%rdi,2), %edi
	movw	%di, (%r10,%rax,2)
	leal	5(%rdx), %edi
	leal	5(%rsi), %eax
	cmpl	%ecx, %edi
	jge	.L666
	cltq
	movslq	%edi, %rdi
	addl	$6, %edx
	leaq	(%rax,%rax), %rsi
	movzwl	(%r8,%rax,2), %eax
	movw	%ax, (%r10,%rdi,2)
	cmpl	%ecx, %edx
	jge	.L666
	movzwl	2(%r8,%rsi), %eax
	movslq	%edx, %rdx
	movw	%ax, (%r10,%rdx,2)
	.p2align 4,,10
	.p2align 3
.L666:
	subl	%r13d, %r9d
	movl	%r9d, %r15d
.L665:
	movl	$0, -132(%rbp)
	xorl	%eax, %eax
	testl	%r14d, %r14d
	jle	.L673
	movl	%r15d, -168(%rbp)
	movq	%r11, -200(%rbp)
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L826:
	cmpw	$92, %cx
	jne	.L825
.L806:
	cmpl	%edx, %r14d
	jle	.L679
	movzwl	2(%r12,%rdi), %r13d
	movl	%r13d, %eax
	andl	$-33, %eax
	cmpw	$85, %ax
	jne	.L680
	movq	uregex_ucstr_unescape_charAt_67@GOTPCREL(%rip), %rdi
	movl	%r14d, %edx
	leaq	-132(%rbp), %rsi
	movq	%r12, %rcx
	call	u_unescapeAt_67@PLT
	movl	-132(%rbp), %edx
	cmpl	$-1, %eax
	je	.L680
	movl	-168(%rbp), %edi
	movl	-180(%rbp), %esi
	cmpl	$65535, %eax
	jg	.L682
	cmpl	%esi, %edi
	jge	.L683
	movslq	%edi, %rdx
	movq	-192(%rbp), %rdi
	movw	%ax, (%rdi,%rdx,2)
.L683:
	addl	$1, -168(%rbp)
	movl	-132(%rbp), %eax
	.p2align 4,,10
	.p2align 3
.L677:
	cmpl	%eax, %r14d
	jle	.L811
.L672:
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L817
	movslq	%eax, %rsi
	leal	1(%rax), %edx
	movzwl	(%r12,%rsi,2), %ecx
	movl	%edx, -132(%rbp)
	leaq	(%rsi,%rsi), %rdi
	cmpw	$36, %cx
	jne	.L826
	cmpw	$92, %cx
	je	.L806
	cmpl	%edx, %r14d
	jg	.L687
	movl	$-1, %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	jne	.L693
.L688:
	movl	$66325, (%rbx)
	movl	-168(%rbp), %r15d
.L674:
	cmpl	-180(%rbp), %r15d
	jge	.L721
.L727:
	movq	-192(%rbp), %rdi
	movslq	%r15d, %rax
	xorl	%ecx, %ecx
	movw	%cx, (%rdi,%rax,2)
.L721:
	testl	%r15d, %r15d
	jle	.L725
	movq	-152(%rbp), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L725
	movq	-160(%rbp), %rsi
	movq	(%rsi), %rdx
	cmpl	-180(%rbp), %r15d
	jge	.L726
	movslq	%r15d, %rcx
	subl	%r15d, %eax
	leaq	(%rdx,%rcx,2), %rdx
	movq	%rdx, (%rsi)
	movl	%eax, (%rdi)
	.p2align 4,,10
	.p2align 3
.L725:
	cmpb	$0, -169(%rbp)
	je	.L652
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L652
.L729:
	movl	$15, (%rbx)
	.p2align 4,,10
	.p2align 3
.L652:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L827
	addq	$184, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L820:
	.cfi_restore_state
	movl	(%r8), %eax
	testl	%eax, %eax
	jne	.L652
	movl	$0, (%r9)
	movb	$1, -169(%rbp)
	testq	%r11, %r11
	jne	.L828
	.p2align 4,,10
	.p2align 3
.L656:
	movl	$1, (%rbx)
	xorl	%r15d, %r15d
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L821:
	cmpb	$0, 60(%r11)
	jne	.L658
.L660:
	movl	$66306, (%rbx)
	xorl	%r15d, %r15d
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L822:
	testl	%eax, %eax
	jle	.L659
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L825:
	movslq	-168(%rbp), %rax
	movl	-180(%rbp), %edi
	cmpl	%edi, %eax
	jge	.L676
	movq	-192(%rbp), %rsi
	movw	%cx, (%rsi,%rax,2)
.L676:
	addl	$1, -168(%rbp)
	movl	%edx, %eax
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L687:
	movzwl	2(%r12,%rdi), %r13d
	movl	%r13d, %r8d
	movl	%r13d, %ecx
	andl	$63488, %r8d
	cmpl	$55296, %r8d
	je	.L829
.L691:
	movl	%r13d, %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	jne	.L693
	cmpl	$123, %r13d
	jne	.L688
	movslq	-132(%rbp), %rdx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r9d
	movq	%rax, -128(%rbp)
	movq	%rdx, %rcx
	leal	1(%rdx), %eax
	leaq	(%rdx,%rdx), %rsi
	movzwl	(%r12,%rdx,2), %edx
	movw	%r9w, -120(%rbp)
	andl	$-1024, %edx
	movl	%eax, -132(%rbp)
	cmpl	$55296, %edx
	jne	.L700
	cmpl	%eax, %r14d
	jne	.L830
.L700:
	movl	(%rbx), %r8d
	leaq	-128(%rbp), %r15
	testl	%r8d, %r8d
	jg	.L819
	.p2align 4,,10
	.p2align 3
.L701:
	cmpl	%eax, %r14d
	jle	.L709
	movslq	%eax, %rdx
	leal	1(%rax), %ecx
	movzwl	(%r12,%rdx,2), %r13d
	leaq	(%rdx,%rdx), %rsi
	movl	%ecx, -132(%rbp)
	movl	%r13d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L704
	cmpl	%ecx, %r14d
	jne	.L831
.L704:
	movl	%r13d, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpl	$25, %eax
	jbe	.L706
.L705:
	leal	-49(%r13), %eax
	cmpl	$8, %eax
	jbe	.L706
	cmpl	$125, %r13d
	je	.L832
.L709:
	movl	$66325, (%rbx)
.L819:
	xorl	%r13d, %r13d
.L702:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L689:
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L817
	movq	-192(%rbp), %rsi
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L712
	movslq	-168(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
.L712:
	movq	-200(%rbp), %rax
	cmpl	$1919252592, (%rax)
	jne	.L716
	cmpq	$0, 48(%rax)
	je	.L833
.L715:
	movl	-180(%rbp), %ecx
	subl	-168(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L739
	testq	%rdx, %rdx
	jne	.L739
	.p2align 4,,10
	.p2align 3
.L716:
	movl	$1, (%rbx)
	movl	-168(%rbp), %r15d
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L706:
	movq	%r15, %rdi
	movl	%r13d, %esi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L819
	cmpl	$125, %r13d
	je	.L819
	movl	-132(%rbp), %eax
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L680:
	movslq	-168(%rbp), %rax
	movl	-180(%rbp), %esi
	cmpl	%esi, %eax
	jge	.L686
	movq	-192(%rbp), %rdi
	movw	%r13w, (%rdi,%rax,2)
.L686:
	leal	1(%rdx), %eax
	addl	$1, -168(%rbp)
	movl	%eax, -132(%rbp)
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L692:
	testl	%edx, %edx
	jle	.L691
	movzwl	(%r12,%rsi,2), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L691
	sall	$10, %eax
	leal	-56613888(%r13,%rax), %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	je	.L688
	.p2align 4,,10
	.p2align 3
.L693:
	movq	-208(%rbp), %rax
	movq	8(%rax), %rax
	movq	136(%rax), %rax
	movl	8(%rax), %eax
	movl	%eax, -176(%rbp)
	movl	-132(%rbp), %eax
	cmpl	%eax, %r14d
	jle	.L736
	movq	%rbx, -216(%rbp)
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L695:
	movl	%ebx, %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	je	.L813
	movl	%ebx, %edi
	call	u_charDigitValue_67@PLT
	leal	0(%r13,%r13,4), %edx
	leal	(%rax,%rdx,2), %ecx
	cmpl	%ecx, -176(%rbp)
	jl	.L697
	movslq	-132(%rbp), %rdx
	leal	1(%rdx), %eax
	movq	%rdx, %rsi
	leaq	(%rdx,%rdx), %rdi
	movzwl	(%r12,%rdx,2), %edx
	movl	%eax, -132(%rbp)
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L698
	cmpl	%eax, %r14d
	jne	.L834
.L698:
	addl	$1, %r15d
	cmpl	%eax, %r14d
	jle	.L737
	movl	%ecx, %r13d
.L690:
	movslq	%eax, %rdx
	movzwl	(%r12,%rdx,2), %ebx
	leaq	(%rdx,%rdx), %rsi
	movl	%ebx, %ecx
	movl	%ebx, %edx
	andl	$63488, %ecx
	cmpl	$55296, %ecx
	jne	.L695
	andb	$4, %dh
	jne	.L696
	addl	$1, %eax
	cmpl	%eax, %r14d
	je	.L695
	movzwl	2(%r12,%rsi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L695
	sall	$10, %ebx
	leal	-56613888(%rax,%rbx), %ebx
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L834:
	movzwl	2(%r12,%rdi), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L698
	leal	2(%rsi), %eax
	movl	%eax, -132(%rbp)
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L696:
	testl	%eax, %eax
	jle	.L695
	movzwl	-2(%r12,%rsi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L695
	sall	$10, %eax
	leal	-56613888(%rbx,%rax), %ebx
	jmp	.L695
.L739:
	testl	%ecx, %ecx
	movl	$0, %eax
	movq	%rbx, %r8
	movl	%r13d, %esi
	cmovs	%eax, %ecx
	movq	-200(%rbp), %rdi
	call	uregex_group_67.part.0
	addl	%eax, -168(%rbp)
	movl	(%rbx), %eax
	cmpl	$15, %eax
	jne	.L718
	movl	$0, (%rbx)
	movl	-132(%rbp), %eax
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L718:
	testl	%eax, %eax
	jg	.L817
	movl	-132(%rbp), %eax
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L831:
	movzwl	2(%r12,%rsi), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L705
	addl	$2, %eax
	sall	$10, %r13d
	movl	%eax, -132(%rbp)
	leal	-56613888(%rdx,%r13), %r13d
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L829:
	andb	$4, %ch
	jne	.L692
	addl	$2, %eax
	cmpl	%eax, %r14d
	je	.L691
	movzwl	4(%r12,%rdi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L691
	sall	$10, %r13d
	leal	-56613888(%rax,%r13), %r13d
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L832:
	movq	-200(%rbp), %rax
	movq	8(%rax), %rax
	movq	192(%rax), %rdi
	testq	%rdi, %rdi
	je	.L709
	movq	%r15, %rsi
	call	uhash_geti_67@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L709
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L813:
	movq	-216(%rbp), %rbx
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L697:
	movq	-216(%rbp), %rbx
	testl	%r15d, %r15d
	jne	.L689
	movl	$8, (%rbx)
	movl	-168(%rbp), %r15d
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L737:
	movq	-216(%rbp), %rbx
	movl	%ecx, %r13d
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L682:
	cmpl	%esi, %edi
	jge	.L684
	movl	%eax, %edx
	movslq	%edi, %rcx
	movq	-192(%rbp), %rdi
	sarl	$10, %edx
	subw	$10304, %dx
	movw	%dx, (%rdi,%rcx,2)
.L684:
	movl	-168(%rbp), %edi
	leal	1(%rdi), %edx
	cmpl	-180(%rbp), %edx
	jge	.L685
	movq	-192(%rbp), %rdi
	andw	$1023, %ax
	movslq	%edx, %rdx
	orw	$-9216, %ax
	movw	%ax, (%rdi,%rdx,2)
.L685:
	addl	$2, -168(%rbp)
	movl	-132(%rbp), %eax
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L811:
	movl	-168(%rbp), %r15d
.L673:
	cmpl	-180(%rbp), %r15d
	jl	.L727
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L721
.L728:
	movq	-152(%rbp), %rax
	cmpl	%r15d, (%rax)
	je	.L835
	movl	$15, (%rbx)
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L817:
	movl	-168(%rbp), %r15d
	jmp	.L674
.L824:
	movl	136(%rsi), %r9d
	jmp	.L664
.L662:
	movq	136(%rsi), %rdx
	movl	-180(%rbp), %r8d
	movq	%r13, %rsi
	leaq	-132(%rbp), %r9
	movq	-192(%rbp), %rcx
	movq	%r11, -168(%rbp)
	movl	$0, -132(%rbp)
	call	utext_extract_67@PLT
	movq	-168(%rbp), %r11
	movl	%eax, %r15d
	jmp	.L665
.L823:
	movq	%r12, %rdi
	movq	%r11, -168(%rbp)
	call	u_strlen_67@PLT
	movq	-168(%rbp), %r11
	movl	%eax, %r14d
	movq	48(%r11), %rdx
	jmp	.L661
.L835:
	movl	$-124, (%rbx)
	testl	%r15d, %r15d
	jle	.L836
	movq	-160(%rbp), %rax
	movq	(%rax), %rdx
.L726:
	movslq	-180(%rbp), %rax
	movq	-160(%rbp), %rdi
	leaq	(%rdx,%rax,2), %rax
	movq	%rax, (%rdi)
	movq	-152(%rbp), %rax
	movl	$0, (%rax)
	jmp	.L725
.L833:
	cmpb	$0, 60(%rax)
	jne	.L715
	movl	$66306, (%rbx)
	movl	-168(%rbp), %r15d
	jmp	.L674
.L830:
	movzwl	2(%r12,%rsi), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L700
	leal	2(%rcx), %eax
	movl	%eax, -132(%rbp)
	jmp	.L700
.L734:
	movq	-192(%rbp), %rsi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L667:
	movzwl	(%rdi,%rdx,2), %eax
	movw	%ax, (%rsi,%rdx,2)
	addq	$1, %rdx
	cmpl	%edx, %ecx
	jg	.L667
	jmp	.L666
.L679:
	movl	-168(%rbp), %r15d
	cmpl	-180(%rbp), %r15d
	jge	.L728
	jmp	.L727
.L733:
	xorl	%r15d, %r15d
	jmp	.L665
.L736:
	xorl	%r13d, %r13d
	jmp	.L689
.L836:
	cmpb	$0, -169(%rbp)
	je	.L652
	jmp	.L729
.L827:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3100:
	.size	uregex_appendReplacement_67, .-uregex_appendReplacement_67
	.p2align 4
	.globl	uregex_end64_67
	.type	uregex_end64_67, @function
uregex_end64_67:
.LFB3070:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L837
	testq	%rdi, %rdi
	je	.L839
	cmpl	$1919252592, (%rdi)
	jne	.L839
	cmpq	$0, 48(%rdi)
	je	.L846
.L841:
	movq	40(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexMatcher5end64EiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L839:
	movl	$1, (%rdx)
.L837:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L846:
	cmpb	$0, 60(%rdi)
	jne	.L841
	movl	$66306, (%rdx)
	jmp	.L837
	.cfi_endproc
.LFE3070:
	.size	uregex_end64_67, .-uregex_end64_67
	.p2align 4
	.globl	uregex_end_67
	.type	uregex_end_67, @function
uregex_end_67:
.LFB3069:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L857
	testq	%rdi, %rdi
	je	.L849
	cmpl	$1919252592, (%rdi)
	jne	.L849
	cmpq	$0, 48(%rdi)
	je	.L859
.L851:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	40(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6712RegexMatcher5end64EiR10UErrorCode@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L849:
	.cfi_restore 6
	movl	$1, (%rdx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L857:
	ret
	.p2align 4,,10
	.p2align 3
.L859:
	cmpb	$0, 60(%rdi)
	jne	.L851
	movl	$66306, (%rdx)
	ret
	.cfi_endproc
.LFE3069:
	.size	uregex_end_67, .-uregex_end_67
	.p2align 4
	.globl	uregex_groupUText_67
	.type	uregex_groupUText_67, @function
uregex_groupUText_67:
.LFB3066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L861
	testq	%rdi, %rdi
	je	.L862
	cmpl	$1919252592, (%rdi)
	jne	.L862
	cmpq	$0, 48(%rdi)
	je	.L872
.L864:
	movq	40(%rdi), %rdi
	call	_ZNK6icu_6712RegexMatcher5groupEiP5UTextRlR10UErrorCode@PLT
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L872:
	cmpb	$0, 60(%rdi)
	jne	.L864
	movl	$66306, (%r8)
	.p2align 4,,10
	.p2align 3
.L861:
	movl	$0, -12(%rbp)
	testq	%rdx, %rdx
	je	.L873
.L865:
	movq	%rdx, %rax
.L860:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L874
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L862:
	.cfi_restore_state
	movl	$1, (%r8)
	movl	$0, -12(%rbp)
	testq	%rdx, %rdx
	jne	.L865
.L873:
	leaq	-12(%rbp), %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	utext_openUChars_67@PLT
	movq	%rax, %rdx
	jmp	.L865
.L874:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3066:
	.size	uregex_groupUText_67, .-uregex_groupUText_67
	.p2align 4
	.globl	uregex_reset64_67
	.type	uregex_reset64_67, @function
uregex_reset64_67:
.LFB3072:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L875
	testq	%rdi, %rdi
	je	.L877
	cmpl	$1919252592, (%rdi)
	jne	.L877
	cmpq	$0, 48(%rdi)
	je	.L883
.L879:
	movq	40(%rdi), %rdi
	jmp	_ZN6icu_6712RegexMatcher5resetElR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L875:
	ret
	.p2align 4,,10
	.p2align 3
.L877:
	movl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L883:
	cmpb	$0, 60(%rdi)
	jne	.L879
	movl	$66306, (%rdx)
	ret
	.cfi_endproc
.LFE3072:
	.size	uregex_reset64_67, .-uregex_reset64_67
	.p2align 4
	.globl	uregex_reset_67
	.type	uregex_reset_67, @function
uregex_reset_67:
.LFB3071:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L884
	testq	%rdi, %rdi
	je	.L886
	cmpl	$1919252592, (%rdi)
	jne	.L886
	cmpq	$0, 48(%rdi)
	je	.L892
.L888:
	movq	40(%rdi), %rdi
	movslq	%esi, %rsi
	jmp	_ZN6icu_6712RegexMatcher5resetElR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L884:
	ret
	.p2align 4,,10
	.p2align 3
.L886:
	movl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L892:
	cmpb	$0, 60(%rdi)
	jne	.L888
	movl	$66306, (%rdx)
	ret
	.cfi_endproc
.LFE3071:
	.size	uregex_reset_67, .-uregex_reset_67
	.p2align 4
	.globl	uregex_replaceFirst_67
	.type	uregex_replaceFirst_67, @function
uregex_replaceFirst_67:
.LFB3096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	%r8d, -76(%rbp)
	movl	(%r9), %r8d
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L893
	movq	%rdi, %r13
	movq	%r9, %rbx
	testq	%rdi, %rdi
	je	.L959
	cmpl	$1919252592, (%rdi)
	jne	.L959
	movq	48(%rdi), %rax
	movq	%rsi, %r15
	movl	%edx, %r14d
	testq	%rax, %rax
	je	.L962
.L897:
	testq	%r15, %r15
	je	.L959
	cmpl	$-1, %r14d
	jl	.L959
	cmpq	$0, -72(%rbp)
	movl	-76(%rbp), %edx
	je	.L963
.L898:
	testl	%edx, %edx
	js	.L959
	testq	%rax, %rax
	je	.L964
.L899:
	movq	40(%r13), %rdi
	movq	%rbx, %rdx
	xorl	%esi, %esi
	call	_ZN6icu_6712RegexMatcher5resetElR10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L901
	cmpl	$1919252592, 0(%r13)
	jne	.L959
	cmpq	$0, 48(%r13)
	je	.L965
.L903:
	movq	40(%r13), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	_ZN6icu_6712RegexMatcher4findElR10UErrorCode@PLT
	testb	%al, %al
	je	.L935
	leaq	-72(%rbp), %rcx
	movq	%rbx, %r9
	leaq	-76(%rbp), %r8
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710RegexCImpl17appendReplacementEPNS_17RegularExpressionEPKDsiPPDsPiP10UErrorCode
	movl	%eax, %r12d
.L904:
	movl	(%rbx), %eax
	cmpl	$15, %eax
	jne	.L905
.L932:
	movl	-76(%rbp), %esi
	testl	%esi, %esi
	jne	.L893
	movl	$0, (%rbx)
	movl	$1, %r10d
.L906:
	cmpl	$1919252592, 0(%r13)
	jne	.L910
	movq	48(%r13), %rax
	testq	%rax, %rax
	je	.L966
.L908:
	movq	-72(%rbp), %r15
	movl	-76(%rbp), %r14d
	testq	%r15, %r15
	je	.L967
.L909:
	testl	%r14d, %r14d
	js	.L910
	movq	40(%r13), %rdx
	movzbl	130(%rdx), %ecx
	testq	%rax, %rax
	je	.L912
	testb	%cl, %cl
	movq	152(%rdx), %r11
	cmovne	144(%rdx), %r11
	xorl	%eax, %eax
	cmpq	$-1, %r11
	je	.L914
	movq	32(%rdx), %rdi
	movq	56(%rdi), %rax
	cmpq	$0, 72(%rax)
	je	.L968
	leaq	-60(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r11, %rdx
	xorl	%esi, %esi
	movb	%r10b, -77(%rbp)
	movl	$0, -60(%rbp)
	call	utext_extract_67@PLT
	movzbl	-77(%rbp), %r10d
.L914:
	movl	56(%r13), %esi
	cmpl	%eax, %esi
	je	.L969
	cmpl	$-1, %esi
	movq	48(%r13), %rdi
	cltq
	sete	%r8b
	testl	%esi, %esi
	jg	.L918
	movq	%rax, %rdx
	negq	%rdx
	leaq	(%r15,%rdx,2), %r11
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L922:
	movzwl	(%rdi,%rax,2), %ecx
	movl	%eax, %r9d
	testw	%cx, %cx
	jne	.L939
	testb	%r8b, %r8b
	jne	.L919
.L939:
	cmpl	%edx, %r14d
	jle	.L921
	movw	%cx, (%r11,%rax,2)
.L921:
	addq	$1, %rax
	addl	$1, %edx
	cmpl	%eax, %esi
	jne	.L922
.L916:
	cmpl	%r14d, %edx
	jge	.L928
	movslq	%edx, %rax
	xorl	%ecx, %ecx
	movw	%cx, (%r15,%rax,2)
.L929:
	addl	%edx, %r12d
	testb	%r10b, %r10b
	je	.L893
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L893
	movl	$15, (%rbx)
	.p2align 4,,10
	.p2align 3
.L893:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L970
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L963:
	.cfi_restore_state
	testl	%edx, %edx
	jle	.L898
	.p2align 4,,10
	.p2align 3
.L959:
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L962:
	cmpb	$0, 60(%rdi)
	jne	.L897
.L960:
	movl	$66306, (%rbx)
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L901:
	xorl	%r12d, %r12d
	cmpl	$15, %eax
	je	.L932
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L964:
	cmpb	$0, 60(%r13)
	jne	.L899
.L961:
	movl	$66306, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L893
.L967:
	testl	%r14d, %r14d
	jle	.L909
	.p2align 4,,10
	.p2align 3
.L910:
	movl	$1, (%rbx)
	jmp	.L893
.L965:
	cmpb	$0, 60(%r13)
	jne	.L903
	jmp	.L961
.L966:
	cmpb	$0, 60(%r13)
	jne	.L908
	jmp	.L960
.L905:
	testl	%eax, %eax
	jg	.L893
	xorl	%r10d, %r10d
	jmp	.L906
.L928:
	movl	$15, %eax
	movl	$-124, %ecx
	cmove	%ecx, %eax
	movl	%eax, (%rbx)
	jmp	.L929
.L912:
	testb	%cl, %cl
	je	.L926
	movq	144(%rdx), %rsi
.L927:
	movq	48(%rdx), %r11
	movq	32(%rdx), %rdi
	movq	%rbx, %r9
	movl	%r14d, %r8d
	movq	%r15, %rcx
	movb	%r10b, -77(%rbp)
	movq	%r11, %rdx
	call	utext_extract_67@PLT
	movzbl	-77(%rbp), %r10d
	movl	%eax, %edx
	jmp	.L916
.L935:
	xorl	%r12d, %r12d
	jmp	.L904
.L968:
	movl	%r11d, %eax
	jmp	.L914
.L919:
	movl	%r9d, 56(%r13)
	jmp	.L916
.L926:
	movq	152(%rdx), %rsi
	cmpq	$-1, %rsi
	jne	.L927
	xorl	%esi, %esi
	jmp	.L927
.L970:
	call	__stack_chk_fail@PLT
.L918:
	imulq	$-2, %rax, %rcx
	xorl	%edx, %edx
	addq	%r15, %rcx
.L925:
	movzwl	(%rdi,%rax,2), %r11d
	movl	%eax, %r9d
	testw	%r11w, %r11w
	jne	.L923
	testb	%r8b, %r8b
	jne	.L919
.L923:
	cmpl	%r14d, %edx
	jne	.L971
	subl	%r9d, %esi
	addl	%esi, %edx
	jmp	.L916
.L971:
	movw	%r11w, (%rcx,%rax,2)
	addq	$1, %rax
	addl	$1, %edx
	cmpl	%eax, %esi
	jne	.L925
	jmp	.L916
.L969:
	xorl	%edx, %edx
	jmp	.L916
	.cfi_endproc
.LFE3096:
	.size	uregex_replaceFirst_67, .-uregex_replaceFirst_67
	.p2align 4
	.globl	uregex_replaceAll_67
	.type	uregex_replaceAll_67, @function
uregex_replaceAll_67:
.LFB3094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -148(%rbp)
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L972
	movq	%rdi, %r14
	movq	%r9, %r13
	testq	%rdi, %rdi
	je	.L974
	cmpl	$1919252592, (%rdi)
	jne	.L974
	movq	48(%rdi), %rax
	movq	%rsi, %r12
	movq	%rcx, %rbx
	movl	%r8d, %r10d
	testq	%rax, %rax
	je	.L1229
.L976:
	testq	%r12, %r12
	je	.L974
	cmpl	$-1, -148(%rbp)
	jl	.L974
	testq	%rbx, %rbx
	jne	.L1103
	testl	%r10d, %r10d
	jg	.L974
.L1103:
	testl	%r10d, %r10d
	js	.L974
	testq	%rax, %rax
	je	.L1230
.L978:
	movq	40(%r14), %rdi
	xorl	%esi, %esi
	movq	%r13, %rdx
	movl	%r10d, -152(%rbp)
	call	_ZN6icu_6712RegexMatcher5resetElR10UErrorCode@PLT
	movl	0(%r13), %eax
	movl	-152(%rbp), %r10d
	testl	%eax, %eax
	movl	%eax, -136(%rbp)
	jg	.L980
	xorl	%r15d, %r15d
	movq	%rbx, -160(%rbp)
	movl	%r15d, -164(%rbp)
	.p2align 4,,10
	.p2align 3
.L1056:
	cmpl	$1919252592, (%r14)
	jne	.L1231
	cmpq	$0, 48(%r14)
	je	.L1232
.L983:
	movq	40(%r14), %rdi
	leaq	-136(%rbp), %rsi
	call	_ZN6icu_6712RegexMatcher4findER10UErrorCode@PLT
	movl	-152(%rbp), %ecx
	testl	%ecx, %ecx
	sete	-165(%rbp)
	movzbl	-165(%rbp), %ebx
	testb	%al, %al
	je	.L1214
	movl	0(%r13), %eax
	cmpl	$15, %eax
	jne	.L984
	testb	%bl, %bl
	jne	.L1233
.L984:
	testl	%eax, %eax
	jg	.L986
	movb	$0, -166(%rbp)
.L985:
	cmpl	$1919252592, (%r14)
	jne	.L989
	movq	48(%r14), %rax
	testq	%rax, %rax
	je	.L1234
.L988:
	movl	-152(%rbp), %edx
	testl	%edx, %edx
	jle	.L1104
	cmpq	$0, -160(%rbp)
	je	.L989
.L1104:
	movl	-152(%rbp), %r15d
	testl	%r15d, %r15d
	js	.L989
	movq	40(%r14), %rbx
	cmpb	$0, 130(%rbx)
	movq	%rbx, -200(%rbp)
	je	.L1226
	movl	-148(%rbp), %ebx
	movl	%ebx, %r15d
	cmpl	$-1, %ebx
	je	.L1235
.L993:
	movq	-200(%rbp), %rcx
	movq	152(%rcx), %rsi
	movq	32(%rcx), %rdi
	testq	%rax, %rax
	je	.L994
	movq	56(%rdi), %rax
	cmpq	$0, 72(%rax)
	je	.L1236
	leaq	-132(%rbp), %r9
	movq	%rsi, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r9, -176(%rbp)
	movl	$0, -132(%rbp)
	call	utext_extract_67@PLT
	movq	-176(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$0, -132(%rbp)
	movl	%eax, %ebx
	movq	-200(%rbp), %rax
	movq	136(%rax), %rdx
	movq	152(%rax), %rsi
	movq	32(%rax), %rdi
	call	utext_extract_67@PLT
	leal	(%rbx,%rax), %r10d
.L996:
	cmpl	%ebx, %r10d
	jle	.L1092
	movl	-152(%rbp), %esi
	movq	48(%r14), %rcx
	testl	%esi, %esi
	jle	.L998
	movl	%r10d, %eax
	movq	-160(%rbp), %r9
	movslq	%ebx, %rdx
	subl	%ebx, %eax
	cmpl	%esi, %eax
	cmovg	%esi, %eax
	leaq	(%rcx,%rdx,2), %rsi
	leaq	16(%rcx,%rdx,2), %rdx
	cmpq	%rdx, %r9
	leaq	16(%r9), %rdx
	setnb	%dil
	cmpq	%rdx, %rsi
	setnb	%dl
	orb	%dl, %dil
	je	.L1093
	cmpl	$7, %eax
	jle	.L1093
	testl	%eax, %eax
	movl	$1, %edi
	cmovg	%eax, %edi
	xorl	%edx, %edx
	movl	%edi, %r8d
	shrl	$3, %r8d
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L1000:
	movdqu	(%rsi,%rdx), %xmm0
	movups	%xmm0, (%r9,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r8
	jne	.L1000
	movl	%edi, %edx
	andl	$-8, %edx
	leal	(%rbx,%rdx), %esi
	cmpl	%edi, %edx
	je	.L998
	movslq	%esi, %rdi
	movq	-160(%rbp), %r11
	movl	%edx, %r8d
	addl	$1, %esi
	movzwl	(%rcx,%rdi,2), %r9d
	leaq	(%r8,%r8), %rdi
	movw	%r9w, (%r11,%r8,2)
	leal	1(%rdx), %r8d
	cmpl	%eax, %r8d
	jge	.L998
	movslq	%esi, %r8
	leaq	(%r8,%r8), %rsi
	movzwl	(%rcx,%r8,2), %r8d
	movw	%r8w, 2(%r11,%rdi)
	leal	2(%rdx), %r8d
	cmpl	%eax, %r8d
	jge	.L998
	movzwl	2(%rcx,%rsi), %r8d
	movw	%r8w, 4(%r11,%rdi)
	leal	3(%rdx), %r8d
	cmpl	%eax, %r8d
	jge	.L998
	movzwl	4(%rcx,%rsi), %r8d
	movw	%r8w, 6(%r11,%rdi)
	leal	4(%rdx), %r8d
	cmpl	%eax, %r8d
	jge	.L998
	movzwl	6(%rcx,%rsi), %r8d
	movw	%r8w, 8(%r11,%rdi)
	leal	5(%rdx), %r8d
	cmpl	%eax, %r8d
	jge	.L998
	movzwl	8(%rcx,%rsi), %r8d
	addl	$6, %edx
	movw	%r8w, 10(%r11,%rdi)
	cmpl	%eax, %edx
	jge	.L998
	movzwl	10(%rcx,%rsi), %eax
	movw	%ax, 12(%r11,%rdi)
	.p2align 4,,10
	.p2align 3
.L998:
	subl	%ebx, %r10d
	movl	%r10d, -176(%rbp)
.L997:
	movl	$0, -132(%rbp)
	xorl	%eax, %eax
	testl	%r15d, %r15d
	jle	.L1005
	movq	%r14, -192(%rbp)
	movq	%r13, %r14
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1238:
	cmpw	$92, %cx
	jne	.L1237
.L1209:
	cmpl	%edx, %r15d
	jle	.L1011
	movzwl	2(%r12,%rdi), %ebx
	movl	%ebx, %eax
	andl	$-33, %eax
	cmpw	$85, %ax
	jne	.L1012
	movq	uregex_ucstr_unescape_charAt_67@GOTPCREL(%rip), %rdi
	movl	%r15d, %edx
	leaq	-132(%rbp), %rsi
	movq	%r12, %rcx
	call	u_unescapeAt_67@PLT
	movl	-132(%rbp), %edx
	cmpl	$-1, %eax
	je	.L1012
	movl	-176(%rbp), %ebx
	cmpl	$65535, %eax
	jg	.L1014
	cmpl	%ebx, -152(%rbp)
	jle	.L1015
	movslq	%ebx, %rdx
	movq	-160(%rbp), %rbx
	movw	%ax, (%rbx,%rdx,2)
.L1015:
	addl	$1, -176(%rbp)
	movl	-132(%rbp), %eax
	.p2align 4,,10
	.p2align 3
.L1009:
	cmpl	%r15d, %eax
	jge	.L1216
.L1004:
	movl	(%r14), %ebx
	testl	%ebx, %ebx
	jg	.L1222
	movslq	%eax, %rsi
	leal	1(%rax), %edx
	movzwl	(%r12,%rsi,2), %ecx
	movl	%edx, -132(%rbp)
	leaq	(%rsi,%rsi), %rdi
	cmpw	$36, %cx
	jne	.L1238
	cmpw	$92, %cx
	je	.L1209
	cmpl	%edx, %r15d
	jg	.L1019
	movl	$-1, %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	jne	.L1025
.L1020:
	movq	%r14, %r13
	movq	-192(%rbp), %r14
	movl	$66325, 0(%r13)
.L1006:
	movl	-152(%rbp), %ebx
	cmpl	%ebx, -176(%rbp)
	jge	.L1052
.L1084:
	movslq	-176(%rbp), %rax
	movq	-160(%rbp), %rbx
	xorl	%edi, %edi
	movw	%di, (%rbx,%rax,2)
.L1052:
	movl	-176(%rbp), %ebx
	testl	%ebx, %ebx
	jle	.L1054
	movl	-152(%rbp), %edi
	movb	$1, -165(%rbp)
	testl	%edi, %edi
	je	.L1054
	cmpl	%ebx, %edi
	jle	.L1055
	movq	-160(%rbp), %rsi
	movslq	%ebx, %rax
	subl	%ebx, %edi
	movl	%edi, -152(%rbp)
	leaq	(%rsi,%rax,2), %rax
	sete	-165(%rbp)
	movq	%rax, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L1054:
	movl	-176(%rbp), %ebx
	addl	%ebx, -164(%rbp)
	cmpb	$0, -166(%rbp)
	je	.L986
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L986
	movl	$15, 0(%r13)
	.p2align 4,,10
	.p2align 3
.L986:
	movl	-136(%rbp), %edx
	testl	%edx, %edx
	jle	.L1056
.L1214:
	movl	-164(%rbp), %r15d
	movq	-160(%rbp), %rbx
	movl	-152(%rbp), %r10d
.L982:
	movl	0(%r13), %eax
	cmpl	$15, %eax
	jne	.L1057
	cmpb	$0, -165(%rbp)
	jne	.L1086
.L1057:
	xorl	%r11d, %r11d
	testl	%eax, %eax
	jg	.L1228
.L1058:
	cmpl	$1919252592, (%r14)
	jne	.L1062
	movq	48(%r14), %rax
	testq	%rax, %rax
	je	.L1239
.L1061:
	testl	%r10d, %r10d
	jle	.L1106
	testq	%rbx, %rbx
	je	.L1062
.L1106:
	testl	%r10d, %r10d
	js	.L1062
	movq	40(%r14), %rdx
	movzbl	130(%rdx), %ecx
	testq	%rax, %rax
	je	.L1065
	testb	%cl, %cl
	movq	152(%rdx), %r12
	cmovne	144(%rdx), %r12
	xorl	%eax, %eax
	cmpq	$-1, %r12
	je	.L1067
	movq	32(%rdx), %rdi
	movq	56(%rdi), %rax
	cmpq	$0, 72(%rax)
	je	.L1240
	leaq	-132(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	xorl	%esi, %esi
	movl	%r10d, -152(%rbp)
	movb	%r11b, -148(%rbp)
	movl	$0, -132(%rbp)
	call	utext_extract_67@PLT
	movl	-152(%rbp), %r10d
	movzbl	-148(%rbp), %r11d
.L1067:
	movl	56(%r14), %esi
	cmpl	%esi, %eax
	je	.L1101
	cmpl	$-1, %esi
	movq	48(%r14), %rdi
	cltq
	sete	%r8b
	testl	%esi, %esi
	jg	.L1070
	movq	%rax, %rdx
	negq	%rdx
	leaq	(%rbx,%rdx,2), %r12
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1074:
	movzwl	(%rdi,%rax,2), %ecx
	movl	%eax, %r9d
	testw	%cx, %cx
	jne	.L1107
	testb	%r8b, %r8b
	jne	.L1071
.L1107:
	cmpl	%edx, %r10d
	jle	.L1073
	movw	%cx, (%r12,%rax,2)
.L1073:
	addq	$1, %rax
	addl	$1, %edx
	cmpl	%eax, %esi
	jne	.L1074
.L1069:
	addl	%edx, %r15d
	cmpl	%r10d, %edx
	jge	.L1080
	movslq	%edx, %rdx
	xorl	%eax, %eax
	movw	%ax, (%rbx,%rdx,2)
	movl	-136(%rbp), %edx
	movl	%edx, %eax
	testb	%r11b, %r11b
	je	.L1059
	cmpl	$0, 0(%r13)
	jg	.L1059
.L1083:
	movl	$15, 0(%r13)
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L1059:
	testl	%eax, %eax
	jg	.L979
	.p2align 4,,10
	.p2align 3
.L972:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1241
	addq	$168, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L974:
	.cfi_restore_state
	movl	$1, 0(%r13)
	xorl	%r15d, %r15d
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L1229:
	cmpb	$0, 60(%rdi)
	jne	.L976
	movl	$66306, (%r9)
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L980:
	testl	%r10d, %r10d
	jne	.L1108
	cmpl	$15, %eax
	je	.L1102
.L1108:
	xorl	%r15d, %r15d
.L979:
	movl	%eax, 0(%r13)
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L1230:
	cmpb	$0, 60(%r14)
	jne	.L978
	xorl	%r15d, %r15d
	movl	$66306, %eax
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L989:
	movl	$1, 0(%r13)
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L1231:
	movl	-152(%rbp), %r10d
	movl	-164(%rbp), %r15d
	movl	$1, -136(%rbp)
	movq	-160(%rbp), %rbx
	testl	%r10d, %r10d
	sete	-165(%rbp)
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1233:
	movl	$0, 0(%r13)
	movb	$1, -166(%rbp)
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L1237:
	movslq	-176(%rbp), %rax
	cmpl	%eax, -152(%rbp)
	jle	.L1008
	movq	-160(%rbp), %rbx
	movw	%cx, (%rbx,%rax,2)
.L1008:
	addl	$1, -176(%rbp)
	movl	%edx, %eax
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1019:
	movzwl	2(%r12,%rdi), %ebx
	movl	%ebx, %r8d
	movl	%ebx, %ecx
	andl	$63488, %r8d
	cmpl	$55296, %r8d
	je	.L1242
.L1023:
	movl	%ebx, %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	jne	.L1025
	cmpl	$123, %ebx
	jne	.L1020
	movslq	-132(%rbp), %rdx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r11d
	movq	%rax, -128(%rbp)
	movq	%rdx, %rcx
	leal	1(%rdx), %eax
	leaq	(%rdx,%rdx), %rsi
	movzwl	(%r12,%rdx,2), %edx
	movw	%r11w, -120(%rbp)
	andl	$-1024, %edx
	movl	%eax, -132(%rbp)
	cmpl	$55296, %edx
	jne	.L1032
	cmpl	%eax, %r15d
	jne	.L1243
.L1032:
	movl	(%r14), %r10d
	leaq	-128(%rbp), %r8
	movq	%r8, %r13
	testl	%r10d, %r10d
	jg	.L1225
	.p2align 4,,10
	.p2align 3
.L1033:
	cmpl	%eax, %r15d
	jle	.L1244
	movslq	%eax, %rdx
	leal	1(%rax), %ecx
	movzwl	(%r12,%rdx,2), %ebx
	leaq	(%rdx,%rdx), %rsi
	movl	%ecx, -132(%rbp)
	movl	%ebx, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1036
	cmpl	%ecx, %r15d
	jne	.L1245
.L1036:
	movl	%ebx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpl	$25, %eax
	jbe	.L1038
.L1037:
	leal	-49(%rbx), %eax
	cmpl	$8, %eax
	jbe	.L1038
	movq	%r13, %r8
	cmpl	$125, %ebx
	je	.L1246
.L1041:
	movl	$66325, (%r14)
.L1225:
	xorl	%r13d, %r13d
.L1034:
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1021:
	movl	(%r14), %r8d
	testl	%r8d, %r8d
	jg	.L1222
	movq	-160(%rbp), %rbx
	xorl	%edx, %edx
	testq	%rbx, %rbx
	je	.L1044
	movslq	-176(%rbp), %rax
	leaq	(%rbx,%rax,2), %rdx
.L1044:
	movq	-192(%rbp), %rax
	cmpl	$1919252592, (%rax)
	jne	.L1047
	cmpq	$0, 48(%rax)
	je	.L1247
.L1046:
	movl	-152(%rbp), %ecx
	subl	-176(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L1105
	testq	%rdx, %rdx
	jne	.L1105
	.p2align 4,,10
	.p2align 3
.L1047:
	movq	%r14, %r13
	movq	-192(%rbp), %r14
	movl	$1, 0(%r13)
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1038:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	(%r14), %r9d
	testl	%r9d, %r9d
	jg	.L1221
	cmpl	$125, %ebx
	je	.L1221
	movl	-132(%rbp), %eax
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1232:
	cmpb	$0, 60(%r14)
	jne	.L983
	movl	-152(%rbp), %r10d
	movl	-164(%rbp), %r15d
	movl	$66306, -136(%rbp)
	movq	-160(%rbp), %rbx
	testl	%r10d, %r10d
	sete	-165(%rbp)
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1234:
	cmpb	$0, 60(%r14)
	jne	.L988
.L1226:
	movl	$66306, 0(%r13)
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L1012:
	movslq	-176(%rbp), %rax
	cmpl	%eax, -152(%rbp)
	jle	.L1018
	movq	-160(%rbp), %rdi
	movw	%bx, (%rdi,%rax,2)
.L1018:
	leal	1(%rdx), %eax
	addl	$1, -176(%rbp)
	movl	%eax, -132(%rbp)
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1062:
	movl	$1, 0(%r13)
	movl	-136(%rbp), %eax
	jmp	.L1059
.L1024:
	testl	%edx, %edx
	jle	.L1023
	movzwl	(%r12,%rsi,2), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1023
	sall	$10, %eax
	leal	-56613888(%rbx,%rax), %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	je	.L1020
	.p2align 4,,10
	.p2align 3
.L1025:
	movq	-200(%rbp), %rax
	movq	8(%rax), %rax
	movq	136(%rax), %rax
	movl	8(%rax), %eax
	movl	%eax, -184(%rbp)
	movl	-132(%rbp), %eax
	cmpl	%eax, %r15d
	jle	.L1095
	movq	%r14, -208(%rbp)
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1027:
	movl	%r14d, %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	je	.L1218
	movl	%r14d, %edi
	call	u_charDigitValue_67@PLT
	leal	0(%r13,%r13,4), %edx
	leal	(%rax,%rdx,2), %ecx
	cmpl	%ecx, -184(%rbp)
	jl	.L1029
	movslq	-132(%rbp), %rdx
	leal	1(%rdx), %eax
	movq	%rdx, %rsi
	leaq	(%rdx,%rdx), %rdi
	movzwl	(%r12,%rdx,2), %edx
	movl	%eax, -132(%rbp)
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1030
	cmpl	%eax, %r15d
	jne	.L1248
.L1030:
	addl	$1, %ebx
	cmpl	%eax, %r15d
	jle	.L1096
	movl	%ecx, %r13d
.L1022:
	movslq	%eax, %rdx
	movzwl	(%r12,%rdx,2), %r14d
	leaq	(%rdx,%rdx), %rsi
	movl	%r14d, %ecx
	movl	%r14d, %edx
	andl	$63488, %ecx
	cmpl	$55296, %ecx
	jne	.L1027
	andb	$4, %dh
	jne	.L1028
	addl	$1, %eax
	cmpl	%eax, %r15d
	je	.L1027
	movzwl	2(%r12,%rsi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L1027
	movl	%r14d, %edi
	sall	$10, %edi
	leal	-56613888(%rax,%rdi), %r14d
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1248:
	movzwl	2(%r12,%rdi), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L1030
	leal	2(%rsi), %eax
	movl	%eax, -132(%rbp)
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1028:
	testl	%eax, %eax
	jle	.L1027
	movzwl	-2(%r12,%rsi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1027
	sall	$10, %eax
	leal	-56613888(%r14,%rax), %r14d
	jmp	.L1027
.L1105:
	testl	%ecx, %ecx
	movl	$0, %eax
	movq	%r14, %r8
	movl	%r13d, %esi
	cmovs	%eax, %ecx
	movq	-192(%rbp), %rdi
	call	uregex_group_67.part.0
	addl	%eax, -176(%rbp)
	movl	(%r14), %eax
	cmpl	$15, %eax
	jne	.L1049
	movl	$0, (%r14)
	movl	-132(%rbp), %eax
	jmp	.L1009
.L1049:
	testl	%eax, %eax
	jg	.L1222
	movl	-132(%rbp), %eax
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1245:
	movzwl	2(%r12,%rsi), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L1037
	addl	$2, %eax
	sall	$10, %ebx
	movl	%eax, -132(%rbp)
	leal	-56613888(%rdx,%rbx), %ebx
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1242:
	andb	$4, %ch
	jne	.L1024
	addl	$2, %eax
	cmpl	%eax, %r15d
	je	.L1023
	movzwl	4(%r12,%rdi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L1023
	sall	$10, %ebx
	leal	-56613888(%rax,%rbx), %ebx
	jmp	.L1023
.L1102:
	xorl	%r10d, %r10d
	xorl	%r15d, %r15d
.L1086:
	movl	$0, 0(%r13)
	movl	$1, %r11d
	jmp	.L1058
.L1246:
	movq	-192(%rbp), %rax
	movq	8(%rax), %rax
	movq	192(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1041
	movq	%r13, %rsi
	movq	%r13, -184(%rbp)
	call	uhash_geti_67@PLT
	movq	-184(%rbp), %r8
	testl	%eax, %eax
	movl	%eax, %r13d
	je	.L1041
	jmp	.L1034
.L1080:
	je	.L1249
	movl	$15, 0(%r13)
.L1228:
	movl	-136(%rbp), %eax
	jmp	.L1059
.L1218:
	movq	-208(%rbp), %r14
	jmp	.L1021
.L1029:
	movq	-208(%rbp), %r14
	testl	%ebx, %ebx
	jne	.L1021
	movq	%r14, %r13
	movq	-192(%rbp), %r14
	movl	$8, 0(%r13)
	jmp	.L1006
.L1014:
	cmpl	%ebx, -152(%rbp)
	jle	.L1016
	movl	%eax, %edx
	movslq	%ebx, %rcx
	movq	-160(%rbp), %rbx
	sarl	$10, %edx
	subw	$10304, %dx
	movw	%dx, (%rbx,%rcx,2)
.L1016:
	movl	-176(%rbp), %ebx
	leal	1(%rbx), %edx
	cmpl	-152(%rbp), %edx
	jge	.L1017
	movq	-160(%rbp), %rbx
	andw	$1023, %ax
	movslq	%edx, %rdx
	orw	$-9216, %ax
	movw	%ax, (%rbx,%rdx,2)
.L1017:
	addl	$2, -176(%rbp)
	movl	-132(%rbp), %eax
	jmp	.L1009
.L1096:
	movq	-208(%rbp), %r14
	movl	%ecx, %r13d
	jmp	.L1021
.L1216:
	movq	%r14, %r13
	movq	-192(%rbp), %r14
.L1005:
	movl	-152(%rbp), %ebx
	cmpl	%ebx, -176(%rbp)
	jl	.L1084
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L1052
.L1085:
	movl	-152(%rbp), %ebx
	movl	$15, %eax
	cmpl	%ebx, -176(%rbp)
	movl	$-124, %edx
	cmove	%edx, %eax
	movl	%eax, 0(%r13)
	jmp	.L1052
.L1222:
	movq	%r14, %r13
	movq	-192(%rbp), %r14
	jmp	.L1006
.L1236:
	movl	136(%rcx), %r10d
	movl	%esi, %ebx
	jmp	.L996
.L1239:
	cmpb	$0, 60(%r14)
	jne	.L1061
	movl	$66306, 0(%r13)
	jmp	.L1228
.L994:
	movq	136(%rcx), %rdx
	movl	-152(%rbp), %r8d
	leaq	-132(%rbp), %r9
	movl	$0, -132(%rbp)
	movq	-160(%rbp), %rcx
	call	utext_extract_67@PLT
	movl	%eax, -176(%rbp)
	jmp	.L997
.L1244:
	movq	%r13, %r8
	jmp	.L1041
.L1235:
	movq	%r12, %rdi
	call	u_strlen_67@PLT
	movl	%eax, %r15d
	movq	48(%r14), %rax
	jmp	.L993
.L1055:
	movslq	-152(%rbp), %rax
	movq	-160(%rbp), %rbx
	movl	$0, -152(%rbp)
	leaq	(%rbx,%rax,2), %rax
	movq	%rax, -160(%rbp)
	jmp	.L1054
.L1247:
	cmpb	$0, 60(%rax)
	jne	.L1046
	movq	%r14, %r13
	movq	%rax, %r14
	movl	$66306, 0(%r13)
	jmp	.L1006
.L1243:
	movzwl	2(%r12,%rsi), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L1032
	leal	2(%rcx), %eax
	movl	%eax, -132(%rbp)
	jmp	.L1032
.L1093:
	movq	-160(%rbp), %rdi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L999:
	movzwl	(%rsi,%rdx,2), %ecx
	movw	%cx, (%rdi,%rdx,2)
	addq	$1, %rdx
	cmpl	%edx, %eax
	jg	.L999
	jmp	.L998
.L1011:
	movq	%r14, %r13
	movl	-176(%rbp), %ebx
	movq	-192(%rbp), %r14
	cmpl	%ebx, -152(%rbp)
	jg	.L1084
	jmp	.L1085
.L1065:
	testb	%cl, %cl
	je	.L1078
	movq	144(%rdx), %rsi
.L1079:
	movq	48(%rdx), %r12
	movq	32(%rdx), %rdi
	movl	%r10d, %r8d
	movq	%r13, %r9
	movq	%rbx, %rcx
	movb	%r11b, -152(%rbp)
	movq	%r12, %rdx
	movl	%r10d, -148(%rbp)
	call	utext_extract_67@PLT
	movl	-148(%rbp), %r10d
	movzbl	-152(%rbp), %r11d
	movl	%eax, %edx
	jmp	.L1069
.L1092:
	movl	$0, -176(%rbp)
	jmp	.L997
.L1095:
	xorl	%r13d, %r13d
	jmp	.L1021
.L1240:
	movl	%r12d, %eax
	jmp	.L1067
.L1249:
	movl	$-124, 0(%r13)
	testb	%r11b, %r11b
	je	.L1228
	movl	-136(%rbp), %edx
	jmp	.L1083
.L1071:
	movl	%r9d, 56(%r14)
	jmp	.L1069
.L1078:
	movq	152(%rdx), %rsi
	cmpq	$-1, %rsi
	jne	.L1079
	xorl	%esi, %esi
	jmp	.L1079
.L1241:
	call	__stack_chk_fail@PLT
.L1070:
	imulq	$-2, %rax, %rcx
	xorl	%edx, %edx
	addq	%rbx, %rcx
.L1077:
	movzwl	(%rdi,%rax,2), %r12d
	movl	%eax, %r9d
	testw	%r12w, %r12w
	jne	.L1075
	testb	%r8b, %r8b
	jne	.L1071
.L1075:
	cmpl	%edx, %r10d
	jne	.L1250
	subl	%r9d, %esi
	movl	%esi, %edx
	addl	%r10d, %edx
	jmp	.L1069
.L1250:
	movw	%r12w, (%rcx,%rax,2)
	addq	$1, %rax
	addl	$1, %edx
	cmpl	%eax, %esi
	jne	.L1077
	jmp	.L1069
.L1101:
	xorl	%edx, %edx
	jmp	.L1069
.L1221:
	movq	%r13, %r8
	jmp	.L1225
	.cfi_endproc
.LFE3094:
	.size	uregex_replaceAll_67, .-uregex_replaceAll_67
	.p2align 4
	.globl	uregex_setRegion64_67
	.type	uregex_setRegion64_67, @function
uregex_setRegion64_67:
.LFB3074:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1251
	testq	%rdi, %rdi
	je	.L1253
	cmpl	$1919252592, (%rdi)
	jne	.L1253
	cmpq	$0, 48(%rdi)
	je	.L1259
.L1255:
	movq	40(%rdi), %rdi
	jmp	_ZN6icu_6712RegexMatcher6regionEllR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L1251:
	ret
	.p2align 4,,10
	.p2align 3
.L1253:
	movl	$1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1259:
	cmpb	$0, 60(%rdi)
	jne	.L1255
	movl	$66306, (%rcx)
	ret
	.cfi_endproc
.LFE3074:
	.size	uregex_setRegion64_67, .-uregex_setRegion64_67
	.p2align 4
	.globl	uregex_setRegion_67
	.type	uregex_setRegion_67, @function
uregex_setRegion_67:
.LFB3073:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1260
	testq	%rdi, %rdi
	je	.L1262
	cmpl	$1919252592, (%rdi)
	jne	.L1262
	cmpq	$0, 48(%rdi)
	je	.L1268
.L1264:
	movq	40(%rdi), %rdi
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	jmp	_ZN6icu_6712RegexMatcher6regionEllR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L1260:
	ret
	.p2align 4,,10
	.p2align 3
.L1262:
	movl	$1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1268:
	cmpb	$0, 60(%rdi)
	jne	.L1264
	movl	$66306, (%rcx)
	ret
	.cfi_endproc
.LFE3073:
	.size	uregex_setRegion_67, .-uregex_setRegion_67
	.p2align 4
	.globl	uregex_setRegionAndStart_67
	.type	uregex_setRegionAndStart_67, @function
uregex_setRegionAndStart_67:
.LFB3075:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L1269
	testq	%rdi, %rdi
	je	.L1271
	cmpl	$1919252592, (%rdi)
	jne	.L1271
	cmpq	$0, 48(%rdi)
	je	.L1277
.L1273:
	movq	40(%rdi), %rdi
	jmp	_ZN6icu_6712RegexMatcher6regionElllR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L1269:
	ret
	.p2align 4,,10
	.p2align 3
.L1271:
	movl	$1, (%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L1277:
	cmpb	$0, 60(%rdi)
	jne	.L1273
	movl	$66306, (%r8)
	ret
	.cfi_endproc
.LFE3075:
	.size	uregex_setRegionAndStart_67, .-uregex_setRegionAndStart_67
	.p2align 4
	.globl	uregex_regionStart64_67
	.type	uregex_regionStart64_67, @function
uregex_regionStart64_67:
.LFB3077:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1288
	testq	%rdi, %rdi
	je	.L1280
	cmpl	$1919252592, (%rdi)
	jne	.L1280
	cmpq	$0, 48(%rdi)
	je	.L1290
.L1282:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	40(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6712RegexMatcher11regionStartEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L1280:
	.cfi_restore 6
	movl	$1, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1288:
	ret
	.p2align 4,,10
	.p2align 3
.L1290:
	cmpb	$0, 60(%rdi)
	jne	.L1282
	movl	$66306, (%rsi)
	ret
	.cfi_endproc
.LFE3077:
	.size	uregex_regionStart64_67, .-uregex_regionStart64_67
	.p2align 4
	.globl	uregex_regionStart_67
	.type	uregex_regionStart_67, @function
uregex_regionStart_67:
.LFB3076:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1291
	testq	%rdi, %rdi
	je	.L1293
	cmpl	$1919252592, (%rdi)
	jne	.L1293
	cmpq	$0, 48(%rdi)
	je	.L1300
.L1295:
	movq	40(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexMatcher11regionStartEv@PLT
	.p2align 4,,10
	.p2align 3
.L1293:
	movl	$1, (%rsi)
.L1291:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1300:
	cmpb	$0, 60(%rdi)
	jne	.L1295
	movl	$66306, (%rsi)
	jmp	.L1291
	.cfi_endproc
.LFE3076:
	.size	uregex_regionStart_67, .-uregex_regionStart_67
	.p2align 4
	.globl	uregex_regionEnd64_67
	.type	uregex_regionEnd64_67, @function
uregex_regionEnd64_67:
.LFB3079:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1311
	testq	%rdi, %rdi
	je	.L1303
	cmpl	$1919252592, (%rdi)
	jne	.L1303
	cmpq	$0, 48(%rdi)
	je	.L1313
.L1305:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	40(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6712RegexMatcher9regionEndEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L1303:
	.cfi_restore 6
	movl	$1, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1311:
	ret
	.p2align 4,,10
	.p2align 3
.L1313:
	cmpb	$0, 60(%rdi)
	jne	.L1305
	movl	$66306, (%rsi)
	ret
	.cfi_endproc
.LFE3079:
	.size	uregex_regionEnd64_67, .-uregex_regionEnd64_67
	.p2align 4
	.globl	uregex_regionEnd_67
	.type	uregex_regionEnd_67, @function
uregex_regionEnd_67:
.LFB3078:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1314
	testq	%rdi, %rdi
	je	.L1316
	cmpl	$1919252592, (%rdi)
	jne	.L1316
	cmpq	$0, 48(%rdi)
	je	.L1323
.L1318:
	movq	40(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexMatcher9regionEndEv@PLT
	.p2align 4,,10
	.p2align 3
.L1316:
	movl	$1, (%rsi)
.L1314:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1323:
	cmpb	$0, 60(%rdi)
	jne	.L1318
	movl	$66306, (%rsi)
	jmp	.L1314
	.cfi_endproc
.LFE3078:
	.size	uregex_regionEnd_67, .-uregex_regionEnd_67
	.p2align 4
	.globl	uregex_hasTransparentBounds_67
	.type	uregex_hasTransparentBounds_67, @function
uregex_hasTransparentBounds_67:
.LFB3080:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1324
	testq	%rdi, %rdi
	je	.L1326
	cmpl	$1919252592, (%rdi)
	jne	.L1326
	movq	40(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexMatcher20hasTransparentBoundsEv@PLT
	.p2align 4,,10
	.p2align 3
.L1326:
	movl	$1, (%rsi)
.L1324:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3080:
	.size	uregex_hasTransparentBounds_67, .-uregex_hasTransparentBounds_67
	.p2align 4
	.globl	uregex_useTransparentBounds_67
	.type	uregex_useTransparentBounds_67, @function
uregex_useTransparentBounds_67:
.LFB3081:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L1331
	testq	%rdi, %rdi
	je	.L1333
	cmpl	$1919252592, (%rdi)
	jne	.L1333
	movq	40(%rdi), %rdi
	movsbl	%sil, %esi
	jmp	_ZN6icu_6712RegexMatcher20useTransparentBoundsEa@PLT
	.p2align 4,,10
	.p2align 3
.L1333:
	movl	$1, (%rdx)
.L1331:
	ret
	.cfi_endproc
.LFE3081:
	.size	uregex_useTransparentBounds_67, .-uregex_useTransparentBounds_67
	.p2align 4
	.globl	uregex_hasAnchoringBounds_67
	.type	uregex_hasAnchoringBounds_67, @function
uregex_hasAnchoringBounds_67:
.LFB3082:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1337
	testq	%rdi, %rdi
	je	.L1339
	cmpl	$1919252592, (%rdi)
	jne	.L1339
	movq	40(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexMatcher18hasAnchoringBoundsEv@PLT
	.p2align 4,,10
	.p2align 3
.L1339:
	movl	$1, (%rsi)
.L1337:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3082:
	.size	uregex_hasAnchoringBounds_67, .-uregex_hasAnchoringBounds_67
	.p2align 4
	.globl	uregex_useAnchoringBounds_67
	.type	uregex_useAnchoringBounds_67, @function
uregex_useAnchoringBounds_67:
.LFB3083:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L1344
	testq	%rdi, %rdi
	je	.L1346
	cmpl	$1919252592, (%rdi)
	jne	.L1346
	movq	40(%rdi), %rdi
	movsbl	%sil, %esi
	jmp	_ZN6icu_6712RegexMatcher18useAnchoringBoundsEa@PLT
	.p2align 4,,10
	.p2align 3
.L1346:
	movl	$1, (%rdx)
.L1344:
	ret
	.cfi_endproc
.LFE3083:
	.size	uregex_useAnchoringBounds_67, .-uregex_useAnchoringBounds_67
	.p2align 4
	.globl	uregex_hitEnd_67
	.type	uregex_hitEnd_67, @function
uregex_hitEnd_67:
.LFB3084:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1350
	testq	%rdi, %rdi
	je	.L1352
	cmpl	$1919252592, (%rdi)
	jne	.L1352
	cmpq	$0, 48(%rdi)
	je	.L1359
.L1354:
	movq	40(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexMatcher6hitEndEv@PLT
	.p2align 4,,10
	.p2align 3
.L1352:
	movl	$1, (%rsi)
.L1350:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1359:
	cmpb	$0, 60(%rdi)
	jne	.L1354
	movl	$66306, (%rsi)
	jmp	.L1350
	.cfi_endproc
.LFE3084:
	.size	uregex_hitEnd_67, .-uregex_hitEnd_67
	.p2align 4
	.globl	uregex_requireEnd_67
	.type	uregex_requireEnd_67, @function
uregex_requireEnd_67:
.LFB3085:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1360
	testq	%rdi, %rdi
	je	.L1362
	cmpl	$1919252592, (%rdi)
	jne	.L1362
	cmpq	$0, 48(%rdi)
	je	.L1369
.L1364:
	movq	40(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexMatcher10requireEndEv@PLT
	.p2align 4,,10
	.p2align 3
.L1362:
	movl	$1, (%rsi)
.L1360:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1369:
	cmpb	$0, 60(%rdi)
	jne	.L1364
	movl	$66306, (%rsi)
	jmp	.L1360
	.cfi_endproc
.LFE3085:
	.size	uregex_requireEnd_67, .-uregex_requireEnd_67
	.p2align 4
	.globl	uregex_setTimeLimit_67
	.type	uregex_setTimeLimit_67, @function
uregex_setTimeLimit_67:
.LFB3086:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L1370
	testq	%rdi, %rdi
	je	.L1372
	cmpl	$1919252592, (%rdi)
	jne	.L1372
	movq	40(%rdi), %rdi
	jmp	_ZN6icu_6712RegexMatcher12setTimeLimitEiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L1372:
	movl	$1, (%rdx)
.L1370:
	ret
	.cfi_endproc
.LFE3086:
	.size	uregex_setTimeLimit_67, .-uregex_setTimeLimit_67
	.p2align 4
	.globl	uregex_getTimeLimit_67
	.type	uregex_getTimeLimit_67, @function
uregex_getTimeLimit_67:
.LFB3087:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1376
	testq	%rdi, %rdi
	je	.L1378
	cmpl	$1919252592, (%rdi)
	jne	.L1378
	movq	40(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexMatcher12getTimeLimitEv@PLT
	.p2align 4,,10
	.p2align 3
.L1378:
	movl	$1, (%rsi)
.L1376:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3087:
	.size	uregex_getTimeLimit_67, .-uregex_getTimeLimit_67
	.p2align 4
	.globl	uregex_setStackLimit_67
	.type	uregex_setStackLimit_67, @function
uregex_setStackLimit_67:
.LFB3088:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L1383
	testq	%rdi, %rdi
	je	.L1385
	cmpl	$1919252592, (%rdi)
	jne	.L1385
	movq	40(%rdi), %rdi
	jmp	_ZN6icu_6712RegexMatcher13setStackLimitEiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L1385:
	movl	$1, (%rdx)
.L1383:
	ret
	.cfi_endproc
.LFE3088:
	.size	uregex_setStackLimit_67, .-uregex_setStackLimit_67
	.p2align 4
	.globl	uregex_getStackLimit_67
	.type	uregex_getStackLimit_67, @function
uregex_getStackLimit_67:
.LFB3089:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1389
	testq	%rdi, %rdi
	je	.L1391
	cmpl	$1919252592, (%rdi)
	jne	.L1391
	movq	40(%rdi), %rdi
	jmp	_ZNK6icu_6712RegexMatcher13getStackLimitEv@PLT
	.p2align 4,,10
	.p2align 3
.L1391:
	movl	$1, (%rsi)
.L1389:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3089:
	.size	uregex_getStackLimit_67, .-uregex_getStackLimit_67
	.p2align 4
	.globl	uregex_setMatchCallback_67
	.type	uregex_setMatchCallback_67, @function
uregex_setMatchCallback_67:
.LFB3090:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1396
	testq	%rdi, %rdi
	je	.L1398
	cmpl	$1919252592, (%rdi)
	jne	.L1398
	movq	40(%rdi), %rdi
	jmp	_ZN6icu_6712RegexMatcher16setMatchCallbackEPFaPKviES2_R10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L1398:
	movl	$1, (%rcx)
.L1396:
	ret
	.cfi_endproc
.LFE3090:
	.size	uregex_setMatchCallback_67, .-uregex_setMatchCallback_67
	.p2align 4
	.globl	uregex_getMatchCallback_67
	.type	uregex_getMatchCallback_67, @function
uregex_getMatchCallback_67:
.LFB3091:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1402
	testq	%rdi, %rdi
	je	.L1404
	cmpl	$1919252592, (%rdi)
	jne	.L1404
	movq	40(%rdi), %rdi
	jmp	_ZN6icu_6712RegexMatcher16getMatchCallbackERPFaPKviERS2_R10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L1404:
	movl	$1, (%rcx)
.L1402:
	ret
	.cfi_endproc
.LFE3091:
	.size	uregex_getMatchCallback_67, .-uregex_getMatchCallback_67
	.p2align 4
	.globl	uregex_setFindProgressCallback_67
	.type	uregex_setFindProgressCallback_67, @function
uregex_setFindProgressCallback_67:
.LFB3092:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1408
	testq	%rdi, %rdi
	je	.L1410
	cmpl	$1919252592, (%rdi)
	jne	.L1410
	movq	40(%rdi), %rdi
	jmp	_ZN6icu_6712RegexMatcher23setFindProgressCallbackEPFaPKvlES2_R10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L1410:
	movl	$1, (%rcx)
.L1408:
	ret
	.cfi_endproc
.LFE3092:
	.size	uregex_setFindProgressCallback_67, .-uregex_setFindProgressCallback_67
	.p2align 4
	.globl	uregex_getFindProgressCallback_67
	.type	uregex_getFindProgressCallback_67, @function
uregex_getFindProgressCallback_67:
.LFB3093:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1414
	testq	%rdi, %rdi
	je	.L1416
	cmpl	$1919252592, (%rdi)
	jne	.L1416
	movq	40(%rdi), %rdi
	jmp	_ZN6icu_6712RegexMatcher23getFindProgressCallbackERPFaPKvlERS2_R10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L1416:
	movl	$1, (%rcx)
.L1414:
	ret
	.cfi_endproc
.LFE3093:
	.size	uregex_getFindProgressCallback_67, .-uregex_getFindProgressCallback_67
	.p2align 4
	.globl	uregex_replaceAllUText_67
	.type	uregex_replaceAllUText_67, @function
uregex_replaceAllUText_67:
.LFB3095:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1420
	testq	%rdi, %rdi
	je	.L1422
	cmpl	$1919252592, (%rdi)
	jne	.L1422
	cmpq	$0, 48(%rdi)
	je	.L1432
.L1424:
	testq	%rsi, %rsi
	je	.L1422
	movq	40(%rdi), %rdi
	jmp	_ZN6icu_6712RegexMatcher10replaceAllEP5UTextS2_R10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L1422:
	movl	$1, (%rcx)
.L1420:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1432:
	cmpb	$0, 60(%rdi)
	jne	.L1424
	movl	$66306, (%rcx)
	jmp	.L1420
	.cfi_endproc
.LFE3095:
	.size	uregex_replaceAllUText_67, .-uregex_replaceAllUText_67
	.p2align 4
	.globl	uregex_replaceFirstUText_67
	.type	uregex_replaceFirstUText_67, @function
uregex_replaceFirstUText_67:
.LFB3097:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1433
	testq	%rdi, %rdi
	je	.L1435
	cmpl	$1919252592, (%rdi)
	jne	.L1435
	cmpq	$0, 48(%rdi)
	je	.L1445
.L1437:
	testq	%rsi, %rsi
	je	.L1435
	movq	40(%rdi), %rdi
	jmp	_ZN6icu_6712RegexMatcher12replaceFirstEP5UTextS2_R10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L1435:
	movl	$1, (%rcx)
.L1433:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1445:
	cmpb	$0, 60(%rdi)
	jne	.L1437
	movl	$66306, (%rcx)
	jmp	.L1433
	.cfi_endproc
.LFE3097:
	.size	uregex_replaceFirstUText_67, .-uregex_replaceFirstUText_67
	.p2align 4
	.globl	uregex_appendReplacementUText_67
	.type	uregex_appendReplacementUText_67, @function
uregex_appendReplacementUText_67:
.LFB3101:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movq	40(%rdi), %rdi
	movq	%rdx, %rsi
	movq	%r8, %rdx
	jmp	_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode@PLT
	.cfi_endproc
.LFE3101:
	.size	uregex_appendReplacementUText_67, .-uregex_appendReplacementUText_67
	.p2align 4
	.globl	uregex_appendTailUText_67
	.type	uregex_appendTailUText_67, @function
uregex_appendTailUText_67:
.LFB3104:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode@PLT
	.cfi_endproc
.LFE3104:
	.size	uregex_appendTailUText_67, .-uregex_appendTailUText_67
	.p2align 4
	.globl	uregex_split_67
	.type	uregex_split_67, @function
uregex_split_67:
.LFB3106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r14
	movq	%r8, -88(%rbp)
	movl	%edx, -68(%rbp)
	movq	%rcx, -128(%rbp)
	movl	(%r14), %r8d
	movl	%r9d, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L1448
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L1450
	cmpl	$1919252592, (%rdi)
	jne	.L1450
	cmpq	$0, 48(%rdi)
	movq	%rsi, %r15
	je	.L1506
.L1452:
	testq	%r15, %r15
	jne	.L1488
	movl	-68(%rbp), %esi
	testl	%esi, %esi
	jg	.L1450
.L1488:
	movl	-68(%rbp), %eax
	shrl	$31, %eax
	cmpq	$0, -88(%rbp)
	sete	%dl
	orb	%al, %dl
	jne	.L1450
	movl	-104(%rbp), %r13d
	testl	%r13d, %r13d
	jle	.L1450
	movq	40(%rbx), %rdi
	call	_ZN6icu_6712RegexMatcher5resetEv@PLT
	movq	40(%rbx), %rdi
	xorl	%r10d, %r10d
	movq	48(%rdi), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1448
	movq	32(%rdi), %rax
	movl	%r10d, -80(%rbp)
	xorl	%r12d, %r12d
	movq	%rax, -152(%rbp)
	call	_ZNK6icu_6712RegexMatcher10groupCountEv@PLT
	subl	$1, %r13d
	movl	$0, -60(%rbp)
	movl	-80(%rbp), %r10d
	movl	%eax, -100(%rbp)
	movq	$0, -136(%rbp)
	movl	%r13d, -72(%rbp)
	je	.L1507
	movq	%r15, -96(%rbp)
	movq	%r14, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	40(%rbx), %rdi
	movl	%r10d, -80(%rbp)
	call	_ZN6icu_6712RegexMatcher4findEv@PLT
	movslq	-80(%rbp), %r13
	movq	-88(%rbp), %rdi
	movslq	%r12d, %rdx
	movl	-68(%rbp), %r8d
	movl	$0, %r11d
	movq	%r13, %r10
	salq	$3, %r13
	leaq	(%rdi,%r13), %rsi
	movq	-96(%rbp), %rdi
	subl	%r12d, %r8d
	movq	%rsi, -112(%rbp)
	cmovs	%r11d, %r8d
	leaq	(%rdi,%rdx,2), %rcx
	testb	%al, %al
	je	.L1459
	movq	40(%rbx), %rax
	movq	%rcx, (%rsi)
	leaq	-60(%rbp), %r15
	movq	-136(%rbp), %rsi
	movq	-152(%rbp), %rdi
	movq	%r15, %r9
	movq	136(%rax), %rdx
	call	utext_extract_67@PLT
	movl	-80(%rbp), %r10d
	movl	$0, %r11d
	leal	1(%r12,%rax), %r12d
	movl	-60(%rbp), %eax
	cmpl	$15, %eax
	je	.L1508
	movq	-120(%rbp), %rsi
	movl	%eax, (%rsi)
.L1461:
	movq	40(%rbx), %rax
	movl	-100(%rbp), %ecx
	movq	144(%rax), %rax
	movq	%rax, -136(%rbp)
	testl	%ecx, %ecx
	jle	.L1487
	cmpl	-72(%rbp), %r10d
	je	.L1487
	movq	-88(%rbp), %rax
	movl	$1, %esi
	movq	%r15, %r8
	movl	%r10d, %r14d
	movl	%esi, %r15d
	leaq	8(%rax,%r13), %r13
	movq	%rbx, %rax
	movl	%r12d, %ebx
	movq	%rax, %r12
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1465:
	movl	-68(%rbp), %ecx
	subl	%ebx, %ecx
	testq	%rdx, %rdx
	jne	.L1489
	testl	%ecx, %ecx
	jle	.L1489
	.p2align 4,,10
	.p2align 3
.L1466:
	movl	$1, -60(%rbp)
	movl	$1, %eax
.L1464:
	addl	$1, %ebx
.L1468:
	movq	-120(%rbp), %rdi
	movl	%eax, (%rdi)
.L1469:
	addl	$1, %r15d
	addq	$8, %r13
	cmpl	-72(%rbp), %r14d
	je	.L1504
	cmpl	%r15d, -100(%rbp)
	jl	.L1504
.L1470:
	movq	-96(%rbp), %rsi
	movslq	%ebx, %rax
	addl	$1, %r14d
	movq	%r13, -112(%rbp)
	cmpl	$1919252592, (%r12)
	movl	%r11d, -60(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	movq	%rdx, 0(%r13)
	jne	.L1466
	cmpq	$0, 48(%r12)
	jne	.L1465
	cmpb	$0, 60(%r12)
	jne	.L1465
	movl	$66306, -60(%rbp)
	movl	$66306, %eax
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1504:
	movq	%r12, %rax
	movl	%ebx, %r12d
	movq	%rax, %rbx
.L1462:
	movq	-136(%rbp), %rdi
	leal	1(%r14), %r10d
	cmpq	%rdi, -144(%rbp)
	je	.L1509
	cmpl	%r10d, -72(%rbp)
	jg	.L1455
	movq	-96(%rbp), %r15
	movq	-120(%rbp), %r14
	movq	%rdi, %rbx
.L1476:
	cmpq	%rbx, -144(%rbp)
	jle	.L1456
	movslq	-72(%rbp), %rax
	movq	-88(%rbp), %rsi
	movq	%rax, %rbx
	leaq	(%rsi,%rax,8), %rax
	cmpl	%r10d, %ebx
	je	.L1457
	movq	(%rax), %r12
	subq	(%rsi), %r12
	sarq	%r12
.L1457:
	movslq	%r12d, %rdx
	movl	-68(%rbp), %r8d
	movq	%r14, %r9
	movq	-136(%rbp), %rsi
	leaq	(%r15,%rdx,2), %rcx
	movq	-152(%rbp), %rdi
	movq	-144(%rbp), %rdx
	subl	%r12d, %r8d
	movq	%rcx, (%rax)
	movl	$0, %eax
	cmovs	%eax, %r8d
	call	utext_extract_67@PLT
	movl	-104(%rbp), %r10d
	leal	1(%r12,%rax), %r12d
	.p2align 4,,10
	.p2align 3
.L1458:
	cmpq	$0, -128(%rbp)
	je	.L1479
	movq	-128(%rbp), %rax
	movl	%r12d, (%rax)
.L1479:
	cmpl	%r12d, -68(%rbp)
	jge	.L1448
	movl	$15, (%r14)
	.p2align 4,,10
	.p2align 3
.L1448:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1510
	addq	$120, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1450:
	.cfi_restore_state
	movl	$1, (%r14)
	xorl	%r10d, %r10d
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L1506:
	cmpb	$0, 60(%rdi)
	jne	.L1452
	movl	$66306, (%r14)
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L1489:
	testl	%ecx, %ecx
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	%r8, -80(%rbp)
	cmovs	%r11d, %ecx
	call	uregex_group_67.part.0
	movq	-80(%rbp), %r8
	movl	$0, %r11d
	leal	1(%rbx,%rax), %ebx
	movl	-60(%rbp), %eax
	cmpl	$15, %eax
	jne	.L1468
	movl	%r11d, -60(%rbp)
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1508:
	movl	%r11d, -60(%rbp)
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1487:
	movl	%r10d, %r14d
	jmp	.L1462
.L1459:
	movq	-120(%rbp), %r14
	movq	%rcx, (%rsi)
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rsi
	movl	%r10d, -80(%rbp)
	movq	-152(%rbp), %rdi
	movq	%r14, %r9
	call	utext_extract_67@PLT
	movl	-80(%rbp), %r10d
	leal	1(%r12,%rax), %r12d
	addl	$1, %r10d
.L1475:
	cmpl	%r10d, -104(%rbp)
	jle	.L1458
	movl	-72(%rbp), %eax
	movq	-88(%rbp), %rbx
	xorl	%esi, %esi
	movl	%r10d, -80(%rbp)
	subl	%r10d, %eax
	leaq	8(,%rax,8), %rdx
	movslq	%r10d, %rax
	leaq	(%rbx,%rax,8), %rdi
	call	memset@PLT
	movl	-80(%rbp), %r10d
	jmp	.L1458
.L1509:
	movl	%r14d, %r9d
	movq	-96(%rbp), %r15
	movq	-120(%rbp), %r14
	cmpl	%r12d, -68(%rbp)
	jle	.L1473
	movslq	%r12d, %rax
	xorl	%edx, %edx
	leaq	(%r15,%rax,2), %rax
	movw	%dx, (%rax)
	cmpl	%r9d, -72(%rbp)
	jle	.L1482
	movq	-88(%rbp), %rbx
	movslq	%r10d, %r10
	leaq	(%rbx,%r10,8), %rbx
	leal	2(%r9), %r10d
	movq	%rbx, -112(%rbp)
.L1482:
	movq	-112(%rbp), %rbx
	movq	%rax, (%rbx)
.L1481:
	addl	$1, %r12d
	jmp	.L1475
.L1456:
	addl	$1, %r10d
	jmp	.L1475
.L1473:
	leal	2(%r9), %eax
	cmpl	%r9d, -72(%rbp)
	cmovg	%eax, %r10d
	jmp	.L1481
.L1507:
	movq	-136(%rbp), %rbx
	movl	%r13d, %r10d
	movl	%r13d, %r12d
	jmp	.L1476
.L1510:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3106:
	.size	uregex_split_67, .-uregex_split_67
	.p2align 4
	.globl	uregex_splitUText_67
	.type	uregex_splitUText_67, @function
uregex_splitUText_67:
.LFB3107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	40(%rdi), %r14
	movq	%rcx, %rbx
	movq	%r14, %rdi
	call	_ZNK6icu_6712RegexMatcher9inputTextEv@PLT
	movq	%rbx, %r8
	movl	%r13d, %ecx
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCode@PLT
	.cfi_endproc
.LFE3107:
	.size	uregex_splitUText_67, .-uregex_splitUText_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
