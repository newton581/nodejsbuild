	.file	"name2uni.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725NameUnicodeTransliterator17getDynamicClassIDEv
	.type	_ZNK6icu_6725NameUnicodeTransliterator17getDynamicClassIDEv, @function
_ZNK6icu_6725NameUnicodeTransliterator17getDynamicClassIDEv:
.LFB2362:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6725NameUnicodeTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2362:
	.size	_ZNK6icu_6725NameUnicodeTransliterator17getDynamicClassIDEv, .-_ZNK6icu_6725NameUnicodeTransliterator17getDynamicClassIDEv
	.p2align 4
	.type	_set_add, @function
_set_add:
.LFB2363:
	.cfi_startproc
	endbr64
	jmp	uset_add_67@PLT
	.cfi_endproc
.LFE2363:
	.size	_set_add, .-_set_add
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725NameUnicodeTransliterator5cloneEv
	.type	_ZNK6icu_6725NameUnicodeTransliterator5cloneEv, @function
_ZNK6icu_6725NameUnicodeTransliterator5cloneEv:
.LFB2374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$288, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6725NameUnicodeTransliteratorE(%rip), %rax
	leaq	88(%rbx), %rsi
	movq	%rax, (%r12)
	leaq	88(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKS0_@PLT
.L4:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2374:
	.size	_ZNK6icu_6725NameUnicodeTransliterator5cloneEv, .-_ZNK6icu_6725NameUnicodeTransliterator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725NameUnicodeTransliterator16getStaticClassIDEv
	.type	_ZN6icu_6725NameUnicodeTransliterator16getStaticClassIDEv, @function
_ZN6icu_6725NameUnicodeTransliterator16getStaticClassIDEv:
.LFB2361:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6725NameUnicodeTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2361:
	.size	_ZN6icu_6725NameUnicodeTransliterator16getStaticClassIDEv, .-_ZN6icu_6725NameUnicodeTransliterator16getStaticClassIDEv
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"N"
	.string	"a"
	.string	"m"
	.string	"e"
	.string	"-"
	.string	"A"
	.string	"n"
	.string	"y"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725NameUnicodeTransliteratorC2EPNS_13UnicodeFilterE
	.type	_ZN6icu_6725NameUnicodeTransliteratorC2EPNS_13UnicodeFilterE, @function
_ZN6icu_6725NameUnicodeTransliteratorC2EPNS_13UnicodeFilterE:
.LFB2365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-160(%rbp), %r14
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-112(%rbp), %r12
	movq	%r14, %rdx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rax
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6725NameUnicodeTransliteratorE(%rip), %rax
	addq	$88, %rbx
	movq	%rax, -88(%rbx)
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	_set_add(%rip), %rax
	movq	%rax, -152(%rbp)
	movq	%rbx, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	uprv_getCharNameCharacters_67@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L14
	subq	$-128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2365:
	.size	_ZN6icu_6725NameUnicodeTransliteratorC2EPNS_13UnicodeFilterE, .-_ZN6icu_6725NameUnicodeTransliteratorC2EPNS_13UnicodeFilterE
	.globl	_ZN6icu_6725NameUnicodeTransliteratorC1EPNS_13UnicodeFilterE
	.set	_ZN6icu_6725NameUnicodeTransliteratorC1EPNS_13UnicodeFilterE,_ZN6icu_6725NameUnicodeTransliteratorC2EPNS_13UnicodeFilterE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725NameUnicodeTransliteratorC2ERKS0_
	.type	_ZN6icu_6725NameUnicodeTransliteratorC2ERKS0_, @function
_ZN6icu_6725NameUnicodeTransliteratorC2ERKS0_:
.LFB2372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6725NameUnicodeTransliteratorE(%rip), %rax
	leaq	88(%r12), %rsi
	movq	%rax, (%rbx)
	leaq	88(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSetC1ERKS0_@PLT
	.cfi_endproc
.LFE2372:
	.size	_ZN6icu_6725NameUnicodeTransliteratorC2ERKS0_, .-_ZN6icu_6725NameUnicodeTransliteratorC2ERKS0_
	.globl	_ZN6icu_6725NameUnicodeTransliteratorC1ERKS0_
	.set	_ZN6icu_6725NameUnicodeTransliteratorC1ERKS0_,_ZN6icu_6725NameUnicodeTransliteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725NameUnicodeTransliteratorD2Ev
	.type	_ZN6icu_6725NameUnicodeTransliteratorD2Ev, @function
_ZN6icu_6725NameUnicodeTransliteratorD2Ev:
.LFB2368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725NameUnicodeTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	88(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -88(%rdi)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714TransliteratorD2Ev@PLT
	.cfi_endproc
.LFE2368:
	.size	_ZN6icu_6725NameUnicodeTransliteratorD2Ev, .-_ZN6icu_6725NameUnicodeTransliteratorD2Ev
	.globl	_ZN6icu_6725NameUnicodeTransliteratorD1Ev
	.set	_ZN6icu_6725NameUnicodeTransliteratorD1Ev,_ZN6icu_6725NameUnicodeTransliteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725NameUnicodeTransliteratorD0Ev
	.type	_ZN6icu_6725NameUnicodeTransliteratorD0Ev, @function
_ZN6icu_6725NameUnicodeTransliteratorD0Ev:
.LFB2370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725NameUnicodeTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	88(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -88(%rdi)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2370:
	.size	_ZN6icu_6725NameUnicodeTransliteratorD0Ev, .-_ZN6icu_6725NameUnicodeTransliteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725NameUnicodeTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.type	_ZNK6icu_6725NameUnicodeTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, @function
_ZNK6icu_6725NameUnicodeTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona:
.LFB2375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$312, %rsp
	movq	%rdi, -296(%rbp)
	movl	%ecx, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uprv_getMaxCharNameLength_67@PLT
	testl	%eax, %eax
	jne	.L22
.L96:
	movl	12(%r12), %eax
	movl	%eax, 8(%r12)
.L21:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	addl	$1, %eax
	movslq	%eax, %rdi
	movl	%eax, -324(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, -312(%rbp)
	testq	%rax, %rax
	je	.L96
	leaq	_ZN6icu_67L4OPENE(%rip), %rax
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rax, -264(%rbp)
	leaq	-256(%rbp), %rax
	leaq	-264(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rdx, -344(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	8(%r12), %r15d
	movl	12(%r12), %r14d
	movl	$2, %esi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edi
	movq	%rax, -192(%rbp)
	movw	%si, -184(%rbp)
	movq	%rax, -128(%rbp)
	movw	%di, -120(%rbp)
	cmpl	%r14d, %r15d
	jge	.L65
	leaq	-128(%rbp), %rax
	movq	%r12, -336(%rbp)
	xorl	%r13d, %r13d
	movq	%rax, -288(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -320(%rbp)
	movq	-296(%rbp), %rax
	movl	$-1, -276(%rbp)
	addq	$88, %rax
	movq	%rax, -296(%rbp)
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%rbx), %rax
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	*80(%rax)
	movl	%eax, %r12d
	cmpl	$1, %r13d
	jne	.L29
.L27:
	movl	%r12d, %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	je	.L35
	movzwl	-120(%rbp), %edx
	testw	%dx, %dx
	js	.L36
	movswl	%dx, %eax
	sarl	$5, %eax
.L37:
	movl	$1, %r13d
	testl	%eax, %eax
	jle	.L30
	leal	-1(%rax), %ecx
	je	.L38
	andl	$2, %edx
	leaq	-118(%rbp), %rax
	cmove	-104(%rbp), %rax
	movslq	%ecx, %rcx
	cmpw	$32, (%rax,%rcx,2)
	je	.L30
.L38:
	movl	$32, %ecx
	movq	-344(%rbp), %rsi
	movq	-288(%rbp), %rdi
	xorl	%edx, %edx
	movw	%cx, -264(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L40
	sarl	$5, %eax
.L41:
	xorl	%r13d, %r13d
	cmpl	%eax, -324(%rbp)
	setge	%r13b
	.p2align 4,,10
	.p2align 3
.L30:
	xorl	%eax, %eax
	cmpl	$65535, %r12d
	seta	%al
	addl	$1, %eax
.L31:
	addl	%eax, %r15d
	cmpl	%r14d, %r15d
	jl	.L26
	movq	-336(%rbp), %r12
	movl	%r14d, %eax
	subl	12(%r12), %eax
	addl	%eax, 4(%r12)
	cmpb	$0, -280(%rbp)
	movl	%r14d, 12(%r12)
	je	.L95
	movl	-276(%rbp), %eax
	testl	%eax, %eax
	js	.L95
.L25:
	movl	-276(%rbp), %eax
	movq	-312(%rbp), %rdi
	movl	%eax, 8(%r12)
	call	uprv_free_67@PLT
	movq	-288(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-320(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-304(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L101:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L43
	movswl	%ax, %r13d
	sarl	$5, %r13d
.L44:
	testl	%r13d, %r13d
	jle	.L45
	leal	-1(%r13), %ecx
	je	.L45
	testb	$2, %al
	leaq	-118(%rbp), %rdx
	cmove	-104(%rbp), %rdx
	movslq	%ecx, %rsi
	cmpw	$32, (%rdx,%rsi,2)
	cmove	%ecx, %r13d
.L45:
	testb	$17, %al
	jne	.L73
	leaq	-118(%rbp), %rdi
	testb	$2, %al
	cmove	-104(%rbp), %rdi
.L47:
	movl	%r13d, %esi
	call	uprv_isInvariantUString_67@PLT
	testb	%al, %al
	jne	.L98
.L49:
	cmpl	%r14d, %r15d
	jge	.L99
	movq	(%rbx), %rax
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	*80(%rax)
	movl	$-1, -276(%rbp)
	movl	%eax, %r12d
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%r13d, %r13d
	cmpl	$92, %r12d
	jne	.L30
	movq	-304(%rbp), %rdi
	movl	%r14d, %ecx
	movl	%r15d, %edx
	movq	%rbx, %rsi
	call	_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringERKNS_11ReplaceableEii@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	js	.L67
	cmpl	%r14d, %eax
	jge	.L67
	movzwl	-120(%rbp), %eax
	testb	$1, %al
	jne	.L100
	testw	%ax, %ax
	js	.L33
	movswl	%ax, %edx
	sarl	$5, %edx
.L34:
	movq	(%rbx), %rcx
	movq	80(%rcx), %rcx
	testl	%edx, %edx
	je	.L94
	andl	$31, %eax
	movw	%ax, -120(%rbp)
.L94:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	*%rcx
	movl	%r15d, -276(%rbp)
	movl	%r13d, %r15d
	movl	%eax, %r12d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L67:
	movl	%r15d, -276(%rbp)
	xorl	%r13d, %r13d
	movl	$1, %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L35:
	cmpl	$125, %r12d
	je	.L101
	movq	-296(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L102
	subl	$1, %r15d
	xorl	%r13d, %r13d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L36:
	movl	-116(%rbp), %eax
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L40:
	movl	-116(%rbp), %eax
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L43:
	movl	-116(%rbp), %r13d
	jmp	.L44
.L99:
	movq	-336(%rbp), %r12
	movl	%r14d, %eax
	subl	12(%r12), %eax
	addl	%eax, 4(%r12)
	movl	%r14d, 12(%r12)
	.p2align 4,,10
	.p2align 3
.L95:
	movl	%r15d, -276(%rbp)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L102:
	movq	-288(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L58
	sarl	$5, %eax
.L59:
	xorl	%r13d, %r13d
	cmpl	%eax, -324(%rbp)
	setg	%r13b
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L98:
	movq	-312(%rbp), %r12
	xorl	%r9d, %r9d
	movl	%r13d, %edx
	xorl	%esi, %esi
	movl	-324(%rbp), %r8d
	movq	-288(%rbp), %rdi
	movb	$0, (%r12)
	movq	%r12, %rcx
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movq	-344(%rbp), %rdx
	movq	%r12, %rsi
	movl	$2, %edi
	movl	$0, -264(%rbp)
	call	u_charFromName_67@PLT
	movl	-264(%rbp), %edx
	testl	%edx, %edx
	jg	.L49
	movzwl	-184(%rbp), %edx
	addl	$1, %r15d
	testb	$1, %dl
	je	.L50
	movq	-320(%rbp), %rdi
	movl	%eax, -328(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movl	-328(%rbp), %eax
.L51:
	movq	-320(%rbp), %r13
	movl	%eax, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movq	(%rbx), %rax
	movq	%r13, %rcx
	movl	%r15d, %edx
	movl	-276(%rbp), %r13d
	movq	%rbx, %rdi
	movl	%r13d, %esi
	call	*32(%rax)
	movswl	-184(%rbp), %eax
	movl	%r15d, %edx
	subl	%r13d, %edx
	testw	%ax, %ax
	js	.L54
	sarl	$5, %eax
.L55:
	subl	%eax, %edx
	subl	%edx, %r15d
	subl	%edx, %r14d
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L58:
	movl	-116(%rbp), %eax
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	-128(%rbp), %rax
	movl	%r15d, -276(%rbp)
	movq	%rax, -288(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -320(%rbp)
	jmp	.L25
.L100:
	movq	-288(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movq	(%rbx), %rax
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	*80(%rax)
	movl	%r15d, -276(%rbp)
	movl	%r13d, %r15d
	movl	%eax, %r12d
	jmp	.L27
.L73:
	xorl	%edi, %edi
	jmp	.L47
.L33:
	movl	-116(%rbp), %edx
	jmp	.L34
.L50:
	testw	%dx, %dx
	js	.L52
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L53:
	testl	%ecx, %ecx
	je	.L51
	andl	$31, %edx
	movw	%dx, -184(%rbp)
	jmp	.L51
.L54:
	movl	-180(%rbp), %eax
	jmp	.L55
.L52:
	movl	-180(%rbp), %ecx
	jmp	.L53
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2375:
	.size	_ZNK6icu_6725NameUnicodeTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, .-_ZNK6icu_6725NameUnicodeTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.weak	_ZTSN6icu_6725NameUnicodeTransliteratorE
	.section	.rodata._ZTSN6icu_6725NameUnicodeTransliteratorE,"aG",@progbits,_ZTSN6icu_6725NameUnicodeTransliteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6725NameUnicodeTransliteratorE, @object
	.size	_ZTSN6icu_6725NameUnicodeTransliteratorE, 37
_ZTSN6icu_6725NameUnicodeTransliteratorE:
	.string	"N6icu_6725NameUnicodeTransliteratorE"
	.weak	_ZTIN6icu_6725NameUnicodeTransliteratorE
	.section	.data.rel.ro._ZTIN6icu_6725NameUnicodeTransliteratorE,"awG",@progbits,_ZTIN6icu_6725NameUnicodeTransliteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6725NameUnicodeTransliteratorE, @object
	.size	_ZTIN6icu_6725NameUnicodeTransliteratorE, 24
_ZTIN6icu_6725NameUnicodeTransliteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725NameUnicodeTransliteratorE
	.quad	_ZTIN6icu_6714TransliteratorE
	.weak	_ZTVN6icu_6725NameUnicodeTransliteratorE
	.section	.data.rel.ro._ZTVN6icu_6725NameUnicodeTransliteratorE,"awG",@progbits,_ZTVN6icu_6725NameUnicodeTransliteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6725NameUnicodeTransliteratorE, @object
	.size	_ZTVN6icu_6725NameUnicodeTransliteratorE, 152
_ZTVN6icu_6725NameUnicodeTransliteratorE:
	.quad	0
	.quad	_ZTIN6icu_6725NameUnicodeTransliteratorE
	.quad	_ZN6icu_6725NameUnicodeTransliteratorD1Ev
	.quad	_ZN6icu_6725NameUnicodeTransliteratorD0Ev
	.quad	_ZNK6icu_6725NameUnicodeTransliterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6725NameUnicodeTransliterator5cloneEv
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.quad	_ZNK6icu_6725NameUnicodeTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.quad	_ZNK6icu_6714Transliterator5getIDEv
	.quad	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE
	.section	.rodata
	.align 8
	.type	_ZN6icu_67L4OPENE, @object
	.size	_ZN6icu_67L4OPENE, 12
_ZN6icu_67L4OPENE:
	.value	92
	.value	78
	.value	126
	.value	123
	.value	126
	.value	0
	.local	_ZZN6icu_6725NameUnicodeTransliterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6725NameUnicodeTransliterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
