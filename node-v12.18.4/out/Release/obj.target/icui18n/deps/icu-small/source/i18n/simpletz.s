	.file	"simpletz.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone17getDynamicClassIDEv
	.type	_ZNK6icu_6714SimpleTimeZone17getDynamicClassIDEv, @function
_ZNK6icu_6714SimpleTimeZone17getDynamicClassIDEv:
.LFB3277:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714SimpleTimeZone16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3277:
	.size	_ZNK6icu_6714SimpleTimeZone17getDynamicClassIDEv, .-_ZNK6icu_6714SimpleTimeZone17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone12getRawOffsetEv
	.type	_ZNK6icu_6714SimpleTimeZone12getRawOffsetEv, @function
_ZNK6icu_6714SimpleTimeZone12getRawOffsetEv:
.LFB3313:
	.cfi_startproc
	endbr64
	movl	100(%rdi), %eax
	ret
	.cfi_endproc
.LFE3313:
	.size	_ZNK6icu_6714SimpleTimeZone12getRawOffsetEv, .-_ZNK6icu_6714SimpleTimeZone12getRawOffsetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone12setRawOffsetEi
	.type	_ZN6icu_6714SimpleTimeZone12setRawOffsetEi, @function
_ZN6icu_6714SimpleTimeZone12setRawOffsetEi:
.LFB3314:
	.cfi_startproc
	endbr64
	movl	%esi, 100(%rdi)
	movb	$0, 120(%rdi)
	ret
	.cfi_endproc
.LFE3314:
	.size	_ZN6icu_6714SimpleTimeZone12setRawOffsetEi, .-_ZN6icu_6714SimpleTimeZone12setRawOffsetEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone13getDSTSavingsEv
	.type	_ZNK6icu_6714SimpleTimeZone13getDSTSavingsEv, @function
_ZNK6icu_6714SimpleTimeZone13getDSTSavingsEv:
.LFB3316:
	.cfi_startproc
	endbr64
	movl	116(%rdi), %eax
	ret
	.cfi_endproc
.LFE3316:
	.size	_ZNK6icu_6714SimpleTimeZone13getDSTSavingsEv, .-_ZNK6icu_6714SimpleTimeZone13getDSTSavingsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone15useDaylightTimeEv
	.type	_ZNK6icu_6714SimpleTimeZone15useDaylightTimeEv, @function
_ZNK6icu_6714SimpleTimeZone15useDaylightTimeEv:
.LFB3317:
	.cfi_startproc
	endbr64
	movzbl	104(%rdi), %eax
	ret
	.cfi_endproc
.LFE3317:
	.size	_ZNK6icu_6714SimpleTimeZone15useDaylightTimeEv, .-_ZNK6icu_6714SimpleTimeZone15useDaylightTimeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone20countTransitionRulesER10UErrorCode
	.type	_ZNK6icu_6714SimpleTimeZone20countTransitionRulesER10UErrorCode, @function
_ZNK6icu_6714SimpleTimeZone20countTransitionRulesER10UErrorCode:
.LFB3335:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpb	$0, 104(%rdi)
	setne	%al
	addl	%eax, %eax
	ret
	.cfi_endproc
.LFE3335:
	.size	_ZNK6icu_6714SimpleTimeZone20countTransitionRulesER10UErrorCode, .-_ZNK6icu_6714SimpleTimeZone20countTransitionRulesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone12hasSameRulesERKNS_8TimeZoneE
	.type	_ZNK6icu_6714SimpleTimeZone12hasSameRulesERKNS_8TimeZoneE, @function
_ZNK6icu_6714SimpleTimeZone12hasSameRulesERKNS_8TimeZoneE:
.LFB3319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L14
	movq	(%rdi), %rax
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L12
	xorl	%r12d, %r12d
	cmpb	$42, (%rdi)
	je	.L10
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L10
.L12:
	movl	100(%rbx), %eax
	xorl	%r12d, %r12d
	cmpl	%eax, 100(%r13)
	je	.L29
.L10:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movzbl	104(%r13), %eax
	cmpb	104(%rbx), %al
	jne	.L10
	movl	$1, %r12d
	testb	%al, %al
	je	.L10
	movl	116(%rbx), %eax
	xorl	%r12d, %r12d
	cmpl	%eax, 116(%r13)
	jne	.L10
	movl	108(%rbx), %eax
	cmpl	%eax, 108(%r13)
	jne	.L10
	movabsq	$-4278190081, %rdx
	movq	72(%r13), %rax
	xorq	72(%rbx), %rax
	testq	%rdx, %rax
	jne	.L10
	movl	80(%rbx), %eax
	cmpl	%eax, 80(%r13)
	jne	.L10
	movl	112(%rbx), %eax
	cmpl	%eax, 112(%r13)
	jne	.L10
	movq	88(%r13), %rax
	xorq	88(%rbx), %rax
	testq	%rdx, %rax
	jne	.L10
	movl	84(%rbx), %eax
	cmpl	%eax, 84(%r13)
	jne	.L10
	movl	96(%rbx), %eax
	cmpl	%eax, 96(%r13)
	sete	%r12b
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$8, %rsp
	movl	$1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3319:
	.size	_ZNK6icu_6714SimpleTimeZone12hasSameRulesERKNS_8TimeZoneE, .-_ZNK6icu_6714SimpleTimeZone12hasSameRulesERKNS_8TimeZoneE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone14inDaylightTimeEdR10UErrorCode
	.type	_ZNK6icu_6714SimpleTimeZone14inDaylightTimeEdR10UErrorCode, @function
_ZNK6icu_6714SimpleTimeZone14inDaylightTimeEdR10UErrorCode:
.LFB3318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L35
.L30:
	addq	$16, %rsp
	movl	%r15d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	%rdi, %r14
	movl	$656, %edi
	movq	%rsi, %r12
	movsd	%xmm0, -40(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L32
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6717GregorianCalendarC1ERKNS_8TimeZoneER10UErrorCode@PLT
	movsd	-40(%rbp), %xmm0
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	*96(%rax)
	movq	%r13, %rdi
	movl	%eax, %r15d
	movq	0(%r13), %rax
	call	*8(%rax)
	jmp	.L30
.L32:
	movl	$7, (%r12)
	jmp	.L30
	.cfi_endproc
.LFE3318:
	.size	_ZNK6icu_6714SimpleTimeZone14inDaylightTimeEdR10UErrorCode, .-_ZNK6icu_6714SimpleTimeZone14inDaylightTimeEdR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6714SimpleTimeZone15decodeStartRuleER10UErrorCode.part.0, @function
_ZN6icu_6714SimpleTimeZone15decodeStartRuleER10UErrorCode.part.0:
.LFB4377:
	.cfi_startproc
	movzbl	73(%rdi), %eax
	testb	%al, %al
	je	.L37
	cmpb	$0, 89(%rdi)
	je	.L38
	movl	116(%rdi), %edx
	movb	$1, 104(%rdi)
	testl	%edx, %edx
	jne	.L39
	movl	$3600000, 116(%rdi)
.L39:
	movsbq	72(%rdi), %rdx
	cmpb	$11, %dl
	ja	.L43
	cmpl	$86400000, 76(%rdi)
	ja	.L43
	cmpl	$2, 80(%rdi)
	jbe	.L55
.L43:
	movl	$1, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movb	$0, 104(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movzbl	74(%rdi), %ecx
	testb	%cl, %cl
	jne	.L45
	movl	$1, 108(%rdi)
.L46:
	testb	%al, %al
	jle	.L43
.L50:
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rcx
	cmpb	%al, (%rcx,%rdx)
	jl	.L43
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	jle	.L47
	movl	$2, 108(%rdi)
	cmpb	$7, %cl
	jg	.L43
	addl	$5, %eax
	cmpb	$10, %al
	ja	.L43
	ret
.L38:
	movb	$0, 104(%rdi)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L47:
	negl	%ecx
	movb	%cl, 74(%rdi)
	testb	%al, %al
	jle	.L49
	movl	$3, 108(%rdi)
	cmpb	$7, %cl
	jg	.L43
	jmp	.L50
.L49:
	negl	%eax
	movl	$4, 108(%rdi)
	movb	%al, 73(%rdi)
	cmpb	$7, %cl
	jg	.L43
	jmp	.L46
	.cfi_endproc
.LFE4377:
	.size	_ZN6icu_6714SimpleTimeZone15decodeStartRuleER10UErrorCode.part.0, .-_ZN6icu_6714SimpleTimeZone15decodeStartRuleER10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6714SimpleTimeZone13decodeEndRuleER10UErrorCode.part.0, @function
_ZN6icu_6714SimpleTimeZone13decodeEndRuleER10UErrorCode.part.0:
.LFB4378:
	.cfi_startproc
	cmpb	$0, 73(%rdi)
	movzbl	89(%rdi), %eax
	je	.L57
	testb	%al, %al
	je	.L58
	movl	116(%rdi), %edx
	movb	$1, 104(%rdi)
	testl	%edx, %edx
	jne	.L60
	movl	$3600000, 116(%rdi)
.L60:
	movsbq	88(%rdi), %rdx
	cmpb	$11, %dl
	ja	.L63
	cmpl	$86400000, 92(%rdi)
	ja	.L63
	cmpl	$2, 84(%rdi)
	jbe	.L78
.L63:
	movl	$1, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movb	$0, 104(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movb	$0, 104(%rdi)
	testb	%al, %al
	jne	.L60
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	movzbl	90(%rdi), %ecx
	testb	%cl, %cl
	jne	.L65
	movl	$1, 112(%rdi)
.L66:
	testb	%al, %al
	jle	.L63
.L70:
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rcx
	cmpb	%al, (%rcx,%rdx)
	jl	.L63
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	jle	.L67
	movl	$2, 112(%rdi)
	cmpb	$7, %cl
	jg	.L63
	addl	$5, %eax
	cmpb	$10, %al
	ja	.L63
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	negl	%ecx
	movb	%cl, 90(%rdi)
	testb	%al, %al
	jle	.L69
	movl	$3, 112(%rdi)
	cmpb	$7, %cl
	jg	.L63
	jmp	.L70
.L69:
	negl	%eax
	movl	$4, 112(%rdi)
	movb	%al, 89(%rdi)
	cmpb	$7, %cl
	jg	.L63
	jmp	.L66
	.cfi_endproc
.LFE4378:
	.size	_ZN6icu_6714SimpleTimeZone13decodeEndRuleER10UErrorCode.part.0, .-_ZN6icu_6714SimpleTimeZone13decodeEndRuleER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode
	.type	_ZNK6icu_6714SimpleTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode, @function
_ZNK6icu_6714SimpleTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode:
.LFB3311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -100(%rbp)
	movl	(%r9), %esi
	movl	%edx, -108(%rbp)
	movq	%r8, -96(%rbp)
	movsd	%xmm0, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jg	.L79
	movq	(%rdi), %rax
	leaq	_ZNK6icu_6714SimpleTimeZone12getRawOffsetEv(%rip), %rdx
	movq	%rdi, %r12
	movq	%rcx, %rbx
	movq	%r9, %r15
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L81
	movl	100(%rdi), %eax
.L82:
	movl	%eax, (%rbx)
	divsd	.LC0(%rip), %xmm0
	leaq	-64(%rbp), %r14
	leaq	-68(%rbp), %r13
	call	uprv_floor_67@PLT
	movsd	-88(%rbp), %xmm3
	movq	%r14, %rcx
	movq	%r13, %rdx
	movsd	.LC0(%rip), %xmm1
	leaq	-76(%rbp), %rax
	leaq	-60(%rbp), %r8
	leaq	-72(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	mulsd	%xmm0, %xmm1
	movq	%r8, -136(%rbp)
	movq	%rsi, -128(%rbp)
	subsd	%xmm1, %xmm3
	cvttsd2sil	%xmm3, %r9d
	movl	%r9d, -104(%rbp)
	call	_ZN6icu_675Grego11dayToFieldsEdRiS1_S1_S1_S1_@PLT
	movl	-76(%rbp), %r10d
	movq	(%r12), %rax
	movl	-72(%rbp), %ecx
	movl	-104(%rbp), %r9d
	testb	$3, %r10b
	movq	40(%rax), %rax
	movl	%ecx, %edx
	jne	.L83
	imull	$-1030792151, %r10d, %edx
	addl	$85899344, %edx
	movl	%edx, %edi
	rorl	$2, %edi
	cmpl	$42949672, %edi
	jbe	.L84
	leal	12(%rcx), %edx
.L83:
	movslq	%edx, %rdx
	subq	$8, %rsp
	movl	-68(%rbp), %r8d
	movl	$1, %esi
	leaq	_ZN6icu_675Grego12MONTH_LENGTHE(%rip), %rdi
	pushq	%r15
	movsbl	(%rdi,%rdx), %edx
	movq	%r12, %rdi
	pushq	%rdx
	movl	%r10d, %edx
	pushq	%r9
	movzbl	-64(%rbp), %r9d
	call	*%rax
	movq	-96(%rbp), %rdi
	movl	(%r15), %edx
	subl	(%rbx), %eax
	movl	%eax, (%rdi)
	addq	$32, %rsp
	testl	%edx, %edx
	jg	.L79
	testl	%eax, %eax
	jle	.L85
	movl	-100(%rbp), %ecx
	movl	%ecx, %eax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L90
	cmpl	$3, %eax
	je	.L79
	movl	%ecx, %esi
	andl	$12, %esi
	cmpl	$12, %esi
	je	.L79
.L90:
	movq	(%r12), %rax
	leaq	_ZNK6icu_6714SimpleTimeZone13getDSTSavingsEv(%rip), %rdx
	movq	104(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L91
	movl	116(%r12), %eax
.L92:
	pxor	%xmm0, %xmm0
	movsd	-88(%rbp), %xmm1
	cvtsi2sdl	%eax, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -88(%rbp)
	divsd	.LC0(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	-88(%rbp), %xmm1
	movq	%r14, %rcx
	movq	%r13, %rdx
	movsd	.LC0(%rip), %xmm2
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdi
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	cvttsd2sil	%xmm1, %eax
	movl	%eax, -88(%rbp)
	call	_ZN6icu_675Grego11dayToFieldsEdRiS1_S1_S1_S1_@PLT
	movl	-76(%rbp), %r14d
	movq	(%r12), %rdx
	movl	-72(%rbp), %ecx
	movl	-88(%rbp), %eax
	testb	$3, %r14b
	movq	40(%rdx), %r13
	movl	%ecx, %edx
	jne	.L93
	imull	$-1030792151, %r14d, %edx
	addl	$85899344, %edx
	movl	%edx, %esi
	rorl	$2, %esi
	cmpl	$42949672, %esi
	jbe	.L94
	leal	12(%rcx), %edx
.L93:
	movslq	%edx, %rdx
	subq	$8, %rsp
	movzbl	-64(%rbp), %r9d
	movl	-68(%rbp), %r8d
	leaq	_ZN6icu_675Grego12MONTH_LENGTHE(%rip), %rsi
	pushq	%r15
	movq	%r12, %rdi
	movsbl	(%rsi,%rdx), %edx
	movl	$1, %esi
	pushq	%rdx
	movl	%r14d, %edx
	pushq	%rax
	call	*%r13
	subl	(%rbx), %eax
	movq	-96(%rbp), %rbx
	movl	%eax, (%rbx)
	addq	$32, %rsp
	.p2align 4,,10
	.p2align 3
.L79:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movl	%edx, %edi
	leal	12(%rcx), %edx
	rorl	$4, %edi
	cmpl	$10737419, %edi
	cmovnb	%ecx, %edx
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L85:
	movl	-108(%rbp), %ecx
	movl	%ecx, %eax
	andl	$3, %eax
	cmpl	$3, %eax
	je	.L90
	cmpl	$1, %eax
	je	.L79
	andl	$12, %ecx
	cmpl	$4, %ecx
	jne	.L79
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L81:
	call	*%rax
	movsd	-88(%rbp), %xmm0
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L94:
	movl	%edx, %esi
	leal	12(%rcx), %edx
	rorl	$4, %esi
	cmpl	$10737419, %esi
	cmovnb	%ecx, %edx
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L92
.L119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3311:
	.size	_ZNK6icu_6714SimpleTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode, .-_ZNK6icu_6714SimpleTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone5cloneEv
	.type	_ZNK6icu_6714SimpleTimeZone5cloneEv, @function
_ZNK6icu_6714SimpleTimeZone5cloneEv:
.LFB3300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L120
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6713BasicTimeZoneC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714SimpleTimeZoneE(%rip), %rax
	movq	%rax, (%r12)
	cmpq	%rbx, %r12
	je	.L120
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678TimeZoneaSERKS0_@PLT
	movl	100(%rbx), %eax
	pxor	%xmm0, %xmm0
	movl	%eax, 100(%r12)
	movzbl	72(%rbx), %eax
	movb	%al, 72(%r12)
	movzbl	73(%rbx), %eax
	movb	%al, 73(%r12)
	movzbl	74(%rbx), %eax
	movb	%al, 74(%r12)
	movl	76(%rbx), %eax
	movl	%eax, 76(%r12)
	movl	80(%rbx), %eax
	movl	%eax, 80(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movzbl	88(%rbx), %eax
	movb	%al, 88(%r12)
	movzbl	89(%rbx), %eax
	movb	%al, 89(%r12)
	movzbl	90(%rbx), %eax
	movb	%al, 90(%r12)
	movl	92(%rbx), %eax
	movl	%eax, 92(%r12)
	movl	84(%rbx), %eax
	movl	%eax, 84(%r12)
	movl	112(%rbx), %eax
	movl	%eax, 112(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movl	116(%rbx), %eax
	movl	%eax, 116(%r12)
	movzbl	104(%rbx), %eax
	movb	%al, 104(%r12)
	movups	%xmm0, 128(%r12)
	movb	$0, 120(%r12)
	movups	%xmm0, 144(%r12)
.L120:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3300:
	.size	_ZNK6icu_6714SimpleTimeZone5cloneEv, .-_ZNK6icu_6714SimpleTimeZone5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZoneD2Ev
	.type	_ZN6icu_6714SimpleTimeZoneD2Ev, @function
_ZN6icu_6714SimpleTimeZoneD2Ev:
.LFB3292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714SimpleTimeZoneE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	128(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L126
	movq	(%rdi), %rax
	call	*8(%rax)
.L126:
	movq	136(%r12), %rdi
	testq	%rdi, %rdi
	je	.L127
	movq	(%rdi), %rax
	call	*8(%rax)
.L127:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L128
	movq	(%rdi), %rax
	call	*8(%rax)
.L128:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L129
	movq	(%rdi), %rax
	call	*8(%rax)
.L129:
	movb	$0, 120(%r12)
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, 128(%r12)
	movups	%xmm0, 144(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713BasicTimeZoneD2Ev@PLT
	.cfi_endproc
.LFE3292:
	.size	_ZN6icu_6714SimpleTimeZoneD2Ev, .-_ZN6icu_6714SimpleTimeZoneD2Ev
	.globl	_ZN6icu_6714SimpleTimeZoneD1Ev
	.set	_ZN6icu_6714SimpleTimeZoneD1Ev,_ZN6icu_6714SimpleTimeZoneD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZoneD0Ev
	.type	_ZN6icu_6714SimpleTimeZoneD0Ev, @function
_ZN6icu_6714SimpleTimeZoneD0Ev:
.LFB3294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714SimpleTimeZoneE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	128(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L144
	movq	(%rdi), %rax
	call	*8(%rax)
.L144:
	movq	136(%r12), %rdi
	testq	%rdi, %rdi
	je	.L145
	movq	(%rdi), %rax
	call	*8(%rax)
.L145:
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.L146
	movq	(%rdi), %rax
	call	*8(%rax)
.L146:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L147
	movq	(%rdi), %rax
	call	*8(%rax)
.L147:
	movb	$0, 120(%r12)
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, 128(%r12)
	movups	%xmm0, 144(%r12)
	call	_ZN6icu_6713BasicTimeZoneD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3294:
	.size	_ZN6icu_6714SimpleTimeZoneD0Ev, .-_ZN6icu_6714SimpleTimeZoneD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone16getStaticClassIDEv
	.type	_ZN6icu_6714SimpleTimeZone16getStaticClassIDEv, @function
_ZN6icu_6714SimpleTimeZone16getStaticClassIDEv:
.LFB3276:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714SimpleTimeZone16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3276:
	.size	_ZN6icu_6714SimpleTimeZone16getStaticClassIDEv, .-_ZN6icu_6714SimpleTimeZone16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringE
	.type	_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringE, @function
_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringE:
.LFB3279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6713BasicTimeZoneC2ERKNS_13UnicodeStringE@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movl	%r12d, 100(%rbx)
	leaq	16+_ZTVN6icu_6714SimpleTimeZoneE(%rip), %rax
	movb	$0, 74(%rbx)
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	movw	%ax, 72(%rbx)
	movabsq	$15461882265600001, %rax
	movl	$0, 76(%rbx)
	movq	$0, 80(%rbx)
	movw	%dx, 88(%rbx)
	movb	$0, 90(%rbx)
	movq	$0, 92(%rbx)
	movb	$0, 104(%rbx)
	movl	$1, 108(%rbx)
	movq	%rax, 112(%rbx)
	movb	$0, 120(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 144(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3279:
	.size	_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringE, .-_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringE
	.globl	_ZN6icu_6714SimpleTimeZoneC1EiRKNS_13UnicodeStringE
	.set	_ZN6icu_6714SimpleTimeZoneC1EiRKNS_13UnicodeStringE,_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiaaaiR10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiaaaiR10UErrorCode, @function
_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiaaaiR10UErrorCode:
.LFB3282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	32(%rbp), %r14d
	movl	24(%rbp), %r13d
	movl	%esi, -60(%rbp)
	movq	%rdx, %rsi
	movl	%ecx, -56(%rbp)
	movl	40(%rbp), %r15d
	movl	%r9d, -52(%rbp)
	movb	%r8b, -61(%rbp)
	movb	%r14b, -62(%rbp)
	call	_ZN6icu_6713BasicTimeZoneC2ERKNS_13UnicodeStringE@PLT
	movl	-56(%rbp), %ecx
	movl	-52(%rbp), %r9d
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6714SimpleTimeZoneE(%rip), %rax
	movb	%r12b, 73(%rbx)
	movq	%rax, (%rbx)
	movl	-60(%rbp), %eax
	movb	%cl, 72(%rbx)
	movl	%eax, 100(%rbx)
	movl	16(%rbp), %eax
	movb	%r9b, 74(%rbx)
	movl	%eax, 76(%rbx)
	movl	48(%rbp), %eax
	movb	%r13b, 88(%rbx)
	movl	%eax, 92(%rbx)
	movabsq	$15461882265600001, %rax
	movq	%rax, 112(%rbx)
	movq	56(%rbp), %rax
	movb	%r14b, 89(%rbx)
	movl	(%rax), %eax
	movb	%r15b, 90(%rbx)
	movq	$0, 80(%rbx)
	movl	$0, 96(%rbx)
	movl	$1, 108(%rbx)
	movb	$0, 120(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 144(%rbx)
	testl	%eax, %eax
	jg	.L164
	testb	%r14b, %r14b
	setne	%sil
	testb	%r12b, %r12b
	setne	%al
	andb	%sil, %al
	jne	.L166
	movb	$0, 104(%rbx)
	testb	%r12b, %r12b
	jne	.L187
.L167:
	movb	$0, 104(%rbx)
	testb	%r14b, %r14b
	jne	.L177
.L164:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movb	$1, 104(%rbx)
.L187:
	cmpb	$11, %cl
	ja	.L180
	cmpl	$86400000, 16(%rbp)
	ja	.L180
	testb	%r9b, %r9b
	jne	.L192
.L170:
	cmpb	$0, -61(%rbp)
	jle	.L180
.L175:
	movsbq	%cl, %rcx
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rax
	movzbl	-61(%rbp), %edx
	cmpb	%dl, (%rax,%rcx)
	jl	.L180
.L176:
	testb	%sil, %sil
	je	.L167
	movb	$1, 104(%rbx)
.L177:
	cmpb	$11, %r13b
	ja	.L180
	cmpl	$86400000, 48(%rbp)
	ja	.L180
	testb	%r15b, %r15b
	je	.L181
	jle	.L182
	movl	$2, 112(%rbx)
	cmpb	$7, %r15b
	jg	.L180
	leal	5(%r14), %r10d
	cmpb	$10, %r10b
	jbe	.L164
.L180:
	movq	56(%rbp), %rax
	movl	$1, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L185:
	.cfi_restore_state
	movl	%r14d, %eax
	movl	$4, 112(%rbx)
	negl	%eax
	movb	%al, -62(%rbp)
	movb	%al, 89(%rbx)
	cmpb	$7, %r11b
	jg	.L180
	.p2align 4,,10
	.p2align 3
.L181:
	cmpb	$0, -62(%rbp)
	jle	.L180
.L186:
	movsbq	%r13b, %rdx
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rax
	movzbl	-62(%rbp), %edi
	cmpb	(%rax,%rdx), %dil
	jle	.L164
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L192:
	jle	.L171
	movl	$2, 108(%rbx)
	cmpb	$7, %r9b
	jg	.L180
	addl	$5, %r12d
	movl	%eax, %esi
	cmpb	$10, %r12b
	jbe	.L176
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L171:
	negl	%r9d
	movb	%r9b, 74(%rbx)
	testb	%r12b, %r12b
	jle	.L174
	movl	$3, 108(%rbx)
	cmpb	$7, %r9b
	jg	.L180
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L182:
	movl	%r15d, %r11d
	negl	%r11d
	movb	%r11b, 90(%rbx)
	testb	%r14b, %r14b
	jle	.L185
	movl	$3, 112(%rbx)
	cmpb	$7, %r11b
	jg	.L180
	jmp	.L186
.L174:
	movl	%r12d, %eax
	movl	$4, 108(%rbx)
	negl	%eax
	movb	%al, -61(%rbp)
	movb	%al, 73(%rbx)
	cmpb	$7, %r9b
	jg	.L180
	jmp	.L170
	.cfi_endproc
.LFE3282:
	.size	_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiaaaiR10UErrorCode, .-_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiaaaiR10UErrorCode
	.globl	_ZN6icu_6714SimpleTimeZoneC1EiRKNS_13UnicodeStringEaaaiaaaiR10UErrorCode
	.set	_ZN6icu_6714SimpleTimeZoneC1EiRKNS_13UnicodeStringEaaaiaaaiR10UErrorCode,_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiaaaiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiaaaiiR10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiaaaiiR10UErrorCode, @function
_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiaaaiiR10UErrorCode:
.LFB3285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	24(%rbp), %r9d
	movl	32(%rbp), %r8d
	movl	%esi, -64(%rbp)
	movq	%rdx, %rsi
	movl	40(%rbp), %r15d
	movq	64(%rbp), %rbx
	movl	%ecx, -60(%rbp)
	movl	%r9d, -56(%rbp)
	movl	%r8d, -52(%rbp)
	call	_ZN6icu_6713BasicTimeZoneC2ERKNS_13UnicodeStringE@PLT
	movl	-60(%rbp), %ecx
	movl	-56(%rbp), %r9d
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6714SimpleTimeZoneE(%rip), %rax
	movl	-52(%rbp), %r8d
	movb	$0, 120(%r12)
	movq	%rax, (%r12)
	movl	-64(%rbp), %eax
	movb	%cl, 72(%r12)
	movl	(%rbx), %ecx
	movl	%eax, 100(%r12)
	movl	16(%rbp), %eax
	movb	%r14b, 73(%r12)
	movl	%eax, 76(%r12)
	movl	48(%rbp), %eax
	movb	%r13b, 74(%r12)
	movl	%eax, 92(%r12)
	movl	56(%rbp), %eax
	movb	%r9b, 88(%r12)
	movl	%eax, 116(%r12)
	movabsq	$4294967297, %rax
	movb	%r8b, 89(%r12)
	movb	%r15b, 90(%r12)
	movq	$0, 80(%r12)
	movl	$0, 96(%r12)
	movq	%rax, 108(%r12)
	movups	%xmm0, 128(%r12)
	movups	%xmm0, 144(%r12)
	testl	%ecx, %ecx
	jg	.L195
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714SimpleTimeZone15decodeStartRuleER10UErrorCode.part.0
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L195
	call	_ZN6icu_6714SimpleTimeZone13decodeEndRuleER10UErrorCode.part.0
.L195:
	movl	56(%rbp), %eax
	testl	%eax, %eax
	jne	.L193
	movl	$1, (%rbx)
.L193:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3285:
	.size	_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiaaaiiR10UErrorCode, .-_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiaaaiiR10UErrorCode
	.globl	_ZN6icu_6714SimpleTimeZoneC1EiRKNS_13UnicodeStringEaaaiaaaiiR10UErrorCode
	.set	_ZN6icu_6714SimpleTimeZoneC1EiRKNS_13UnicodeStringEaaaiaaaiiR10UErrorCode,_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiaaaiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiNS0_8TimeModeEaaaiS4_iR10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiNS0_8TimeModeEaaaiS4_iR10UErrorCode, @function
_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiNS0_8TimeModeEaaaiS4_iR10UErrorCode:
.LFB3288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	32(%rbp), %r9d
	movl	40(%rbp), %r8d
	movl	%esi, -64(%rbp)
	movq	%rdx, %rsi
	movl	48(%rbp), %r15d
	movq	80(%rbp), %rbx
	movl	%ecx, -60(%rbp)
	movl	%r9d, -56(%rbp)
	movl	%r8d, -52(%rbp)
	call	_ZN6icu_6713BasicTimeZoneC2ERKNS_13UnicodeStringE@PLT
	movl	-60(%rbp), %ecx
	movl	-56(%rbp), %r9d
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6714SimpleTimeZoneE(%rip), %rax
	movl	-52(%rbp), %r8d
	movb	$0, 120(%r12)
	movq	%rax, (%r12)
	movl	-64(%rbp), %eax
	movb	%cl, 72(%r12)
	movl	(%rbx), %ecx
	movl	%eax, 100(%r12)
	movl	16(%rbp), %eax
	movb	%r14b, 73(%r12)
	movl	%eax, 76(%r12)
	movl	24(%rbp), %eax
	movb	%r13b, 74(%r12)
	movl	%eax, 80(%r12)
	movl	56(%rbp), %eax
	movb	%r9b, 88(%r12)
	movl	%eax, 92(%r12)
	movl	64(%rbp), %eax
	movb	%r8b, 89(%r12)
	movl	%eax, 84(%r12)
	movl	72(%rbp), %eax
	movb	%r15b, 90(%r12)
	movl	%eax, 116(%r12)
	movabsq	$4294967297, %rax
	movl	$0, 96(%r12)
	movq	%rax, 108(%r12)
	movups	%xmm0, 128(%r12)
	movups	%xmm0, 144(%r12)
	testl	%ecx, %ecx
	jg	.L201
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714SimpleTimeZone15decodeStartRuleER10UErrorCode.part.0
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L201
	call	_ZN6icu_6714SimpleTimeZone13decodeEndRuleER10UErrorCode.part.0
.L201:
	movl	72(%rbp), %eax
	testl	%eax, %eax
	jne	.L199
	movl	$1, (%rbx)
.L199:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3288:
	.size	_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiNS0_8TimeModeEaaaiS4_iR10UErrorCode, .-_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiNS0_8TimeModeEaaaiS4_iR10UErrorCode
	.globl	_ZN6icu_6714SimpleTimeZoneC1EiRKNS_13UnicodeStringEaaaiNS0_8TimeModeEaaaiS4_iR10UErrorCode
	.set	_ZN6icu_6714SimpleTimeZoneC1EiRKNS_13UnicodeStringEaaaiNS0_8TimeModeEaaaiS4_iR10UErrorCode,_ZN6icu_6714SimpleTimeZoneC2EiRKNS_13UnicodeStringEaaaiNS0_8TimeModeEaaaiS4_iR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone9constructEiaaaiNS0_8TimeModeEaaaiS1_iR10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZone9constructEiaaaiNS0_8TimeModeEaaaiS1_iR10UErrorCode, @function
_ZN6icu_6714SimpleTimeZone9constructEiaaaiNS0_8TimeModeEaaaiS1_iR10UErrorCode:
.LFB3290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movb	%dl, 72(%rdi)
	movb	%cl, 73(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	movl	24(%rbp), %r11d
	movl	32(%rbp), %eax
	pushq	%r14
	pushq	%r13
	movq	72(%rbp), %r10
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	40(%rbp), %r13d
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	16(%rbp), %ebx
	movb	%r8b, 74(%rdi)
	movl	64(%rbp), %esi
	movl	%ebx, 80(%rdi)
	movl	48(%rbp), %ebx
	movb	%r11b, 88(%rdi)
	movl	%ebx, 92(%rdi)
	movl	56(%rbp), %ebx
	movb	%al, 89(%rdi)
	movl	%ebx, 84(%rdi)
	movabsq	$4294967297, %rbx
	movb	%r13b, 90(%rdi)
	movq	%rbx, 108(%rdi)
	movl	(%r10), %ebx
	movl	%r12d, 100(%rdi)
	movl	%r9d, 76(%rdi)
	movl	%esi, 116(%rdi)
	movl	$0, 96(%rdi)
	testl	%ebx, %ebx
	jg	.L225
	testb	%al, %al
	movl	%ecx, %r15d
	movl	%eax, %r14d
	setne	%r12b
	testb	%cl, %cl
	setne	%bl
	andb	%r12b, %bl
	jne	.L239
	movb	$0, 104(%rdi)
	testb	%cl, %cl
	jne	.L209
.L211:
	movb	$0, 104(%rdi)
	testb	%al, %al
	jne	.L222
.L225:
	testl	%esi, %esi
	jne	.L205
	movl	$1, (%r10)
.L205:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	movb	$1, 104(%rdi)
	testl	%esi, %esi
	jne	.L209
	movl	$3600000, 116(%rdi)
.L209:
	cmpb	$11, %dl
	ja	.L227
	cmpl	$86400000, %r9d
	ja	.L227
	cmpl	$2, 16(%rbp)
	ja	.L227
	testb	%r8b, %r8b
	jne	.L240
.L216:
	testb	%r15b, %r15b
	jle	.L227
.L220:
	movsbq	%dl, %rdx
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rcx
	cmpb	%r15b, (%rcx,%rdx)
	jl	.L227
.L221:
	testb	%r12b, %r12b
	je	.L211
	movl	116(%rdi), %edx
	movb	$1, 104(%rdi)
	testl	%edx, %edx
	jne	.L222
	movl	$3600000, 116(%rdi)
.L222:
	cmpb	$11, %r11b
	ja	.L227
	cmpl	$86400000, 48(%rbp)
	ja	.L227
	cmpl	$2, 56(%rbp)
	ja	.L227
	testb	%r13b, %r13b
	je	.L229
	jle	.L230
	movl	$2, 112(%rdi)
	cmpb	$7, %r13b
	jg	.L227
	addl	$5, %eax
	cmpb	$10, %al
	jbe	.L225
	.p2align 4,,10
	.p2align 3
.L227:
	movl	$1, (%r10)
	jmp	.L225
.L232:
	movl	%eax, %r14d
	movl	$4, 112(%rdi)
	negl	%r14d
	movb	%r14b, 89(%rdi)
	cmpb	$7, %r13b
	jg	.L227
	.p2align 4,,10
	.p2align 3
.L229:
	testb	%r14b, %r14b
	jle	.L227
.L233:
	movsbq	%r11b, %r11
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rax
	cmpb	%r14b, (%rax,%r11)
	jge	.L225
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L240:
	jle	.L217
	movl	$2, 108(%rdi)
	cmpb	$7, %r8b
	jg	.L227
	addl	$5, %ecx
	movl	%ebx, %r12d
	cmpb	$10, %cl
	jbe	.L221
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L230:
	negl	%r13d
	movb	%r13b, 90(%rdi)
	testb	%al, %al
	jle	.L232
	movl	$3, 112(%rdi)
	cmpb	$7, %r13b
	jg	.L227
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L217:
	negl	%r8d
	movb	%r8b, 74(%rdi)
	testb	%cl, %cl
	jle	.L219
	movl	$3, 108(%rdi)
	cmpb	$7, %r8b
	jg	.L227
	jmp	.L220
.L219:
	movl	%ecx, %r15d
	movl	$4, 108(%rdi)
	negl	%r15d
	movb	%r15b, 73(%rdi)
	cmpb	$7, %r8b
	jg	.L227
	jmp	.L216
	.cfi_endproc
.LFE3290:
	.size	_ZN6icu_6714SimpleTimeZone9constructEiaaaiNS0_8TimeModeEaaaiS1_iR10UErrorCode, .-_ZN6icu_6714SimpleTimeZone9constructEiaaaiNS0_8TimeModeEaaaiS1_iR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZoneC2ERKS0_
	.type	_ZN6icu_6714SimpleTimeZoneC2ERKS0_, @function
_ZN6icu_6714SimpleTimeZoneC2ERKS0_:
.LFB3296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6713BasicTimeZoneC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714SimpleTimeZoneE(%rip), %rax
	movq	%rax, (%rbx)
	cmpq	%r12, %rbx
	je	.L241
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_678TimeZoneaSERKS0_@PLT
	movzbl	74(%r12), %eax
	movl	76(%r12), %esi
	movb	$0, 120(%rbx)
	movzwl	72(%r12), %edx
	movzbl	90(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	%al, 74(%rbx)
	movl	108(%r12), %eax
	movw	%dx, 72(%rbx)
	movl	92(%r12), %edx
	movl	%esi, 76(%rbx)
	movq	80(%r12), %rsi
	movl	%eax, 108(%rbx)
	movq	112(%r12), %rax
	movq	%rsi, 80(%rbx)
	movzwl	88(%r12), %esi
	movq	%rax, 112(%rbx)
	movzbl	104(%r12), %eax
	movl	%edx, 92(%rbx)
	movq	96(%r12), %rdx
	movw	%si, 88(%rbx)
	movb	%cl, 90(%rbx)
	movq	%rdx, 96(%rbx)
	movb	%al, 104(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 144(%rbx)
.L241:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3296:
	.size	_ZN6icu_6714SimpleTimeZoneC2ERKS0_, .-_ZN6icu_6714SimpleTimeZoneC2ERKS0_
	.globl	_ZN6icu_6714SimpleTimeZoneC1ERKS0_
	.set	_ZN6icu_6714SimpleTimeZoneC1ERKS0_,_ZN6icu_6714SimpleTimeZoneC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZoneaSERKS0_
	.type	_ZN6icu_6714SimpleTimeZoneaSERKS0_, @function
_ZN6icu_6714SimpleTimeZoneaSERKS0_:
.LFB3298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L245
	movq	%rsi, %rbx
	call	_ZN6icu_678TimeZoneaSERKS0_@PLT
	movl	100(%rbx), %eax
	pxor	%xmm0, %xmm0
	movl	%eax, 100(%r12)
	movzbl	72(%rbx), %eax
	movb	%al, 72(%r12)
	movzbl	73(%rbx), %eax
	movb	%al, 73(%r12)
	movzbl	74(%rbx), %eax
	movb	%al, 74(%r12)
	movl	76(%rbx), %eax
	movl	%eax, 76(%r12)
	movl	80(%rbx), %eax
	movl	%eax, 80(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 108(%r12)
	movzbl	88(%rbx), %eax
	movb	%al, 88(%r12)
	movzbl	89(%rbx), %eax
	movb	%al, 89(%r12)
	movzbl	90(%rbx), %eax
	movb	%al, 90(%r12)
	movl	92(%rbx), %eax
	movl	%eax, 92(%r12)
	movl	84(%rbx), %eax
	movl	%eax, 84(%r12)
	movl	112(%rbx), %eax
	movl	%eax, 112(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	movl	116(%rbx), %eax
	movl	%eax, 116(%r12)
	movzbl	104(%rbx), %eax
	movb	%al, 104(%r12)
	movups	%xmm0, 128(%r12)
	movb	$0, 120(%r12)
	movups	%xmm0, 144(%r12)
.L245:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3298:
	.size	_ZN6icu_6714SimpleTimeZoneaSERKS0_, .-_ZN6icu_6714SimpleTimeZoneaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone12setStartYearEi
	.type	_ZN6icu_6714SimpleTimeZone12setStartYearEi, @function
_ZN6icu_6714SimpleTimeZone12setStartYearEi:
.LFB3301:
	.cfi_startproc
	endbr64
	movl	%esi, 96(%rdi)
	movb	$0, 120(%rdi)
	ret
	.cfi_endproc
.LFE3301:
	.size	_ZN6icu_6714SimpleTimeZone12setStartYearEi, .-_ZN6icu_6714SimpleTimeZone12setStartYearEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiiNS0_8TimeModeER10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiiNS0_8TimeModeER10UErrorCode, @function
_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiiNS0_8TimeModeER10UErrorCode:
.LFB3302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movb	%sil, 72(%rdi)
	movb	%dl, 73(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movb	%cl, 74(%rdi)
	movq	16(%rbp), %rax
	movl	%r8d, 76(%rdi)
	movl	%r9d, 80(%rdi)
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L250
	testb	%dl, %dl
	je	.L251
	cmpb	$0, 89(%rdi)
	movl	%edx, %r10d
	je	.L252
	movl	116(%rdi), %r11d
	movb	$1, 104(%rdi)
	testl	%r11d, %r11d
	jne	.L253
	movl	$3600000, 116(%rdi)
.L253:
	cmpb	$11, %sil
	ja	.L256
	cmpl	$86400000, %r8d
	ja	.L256
	cmpl	$2, %r9d
	ja	.L256
	testb	%cl, %cl
	jne	.L258
	movl	$1, 108(%rdi)
.L259:
	testb	%r10b, %r10b
	jle	.L256
.L263:
	movsbq	%sil, %rsi
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rdx
	cmpb	%r10b, (%rdx,%rsi)
	jl	.L256
.L250:
	movb	$0, 120(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movb	$0, 104(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movb	$0, 120(%rdi)
	ret
.L262:
	.cfi_restore_state
	movl	%edx, %r10d
	movl	$4, 108(%rdi)
	negl	%r10d
	movb	%r10b, 73(%rdi)
	cmpb	$7, %cl
	jle	.L259
.L256:
	movl	$1, (%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movb	$0, 120(%rdi)
	ret
.L252:
	.cfi_restore_state
	movb	$0, 104(%rdi)
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L258:
	jle	.L260
	movl	$2, 108(%rdi)
	cmpb	$7, %cl
	jg	.L256
	addl	$5, %edx
	cmpb	$10, %dl
	jbe	.L250
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L260:
	negl	%ecx
	movb	%cl, 74(%rdi)
	testb	%dl, %dl
	jle	.L262
	movl	$3, 108(%rdi)
	cmpb	$7, %cl
	jg	.L256
	jmp	.L263
	.cfi_endproc
.LFE3302:
	.size	_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiiNS0_8TimeModeER10UErrorCode, .-_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiiNS0_8TimeModeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiNS0_8TimeModeER10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiNS0_8TimeModeER10UErrorCode, @function
_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiNS0_8TimeModeER10UErrorCode:
.LFB3303:
	.cfi_startproc
	endbr64
	movb	%sil, 72(%rdi)
	movb	%dl, 73(%rdi)
	movb	$0, 74(%rdi)
	movl	(%r9), %r10d
	movl	%ecx, 76(%rdi)
	movl	%r8d, 80(%rdi)
	testl	%r10d, %r10d
	jg	.L270
	testb	%dl, %dl
	je	.L271
	cmpb	$0, 89(%rdi)
	je	.L272
	movl	116(%rdi), %eax
	movb	$1, 104(%rdi)
	testl	%eax, %eax
	jne	.L273
	movl	$3600000, 116(%rdi)
.L273:
	cmpb	$11, %sil
	ja	.L276
	cmpl	$86400000, %ecx
	ja	.L276
	cmpl	$2, %r8d
	ja	.L276
	movl	$1, 108(%rdi)
	testb	%dl, %dl
	jle	.L276
	movsbq	%sil, %rsi
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rax
	cmpb	(%rax,%rsi), %dl
	jg	.L276
.L270:
	movb	$0, 120(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	movb	$0, 104(%rdi)
	movb	$0, 120(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	movl	$1, (%r9)
	movb	$0, 120(%rdi)
	ret
.L272:
	movb	$0, 104(%rdi)
	jmp	.L273
	.cfi_endproc
.LFE3303:
	.size	_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiNS0_8TimeModeER10UErrorCode, .-_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiNS0_8TimeModeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiiNS0_8TimeModeEaR10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiiNS0_8TimeModeEaR10UErrorCode, @function
_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiiNS0_8TimeModeEaR10UErrorCode:
.LFB3304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r11d
	movl	%ecx, %r10d
	negl	%r11d
	negl	%r10d
	movb	%sil, 72(%rdi)
	movb	%r10b, 74(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%r8d, 76(%rdi)
	cmpb	$0, 16(%rbp)
	movq	24(%rbp), %rax
	movl	%r9d, 80(%rdi)
	cmove	%r11d, %edx
	movb	%dl, 73(%rdi)
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jg	.L283
	testb	%dl, %dl
	je	.L284
	cmpb	$0, 89(%rdi)
	movl	%edx, %r11d
	je	.L285
	cmpl	$0, 116(%rdi)
	movb	$1, 104(%rdi)
	jne	.L286
	movl	$3600000, 116(%rdi)
.L286:
	cmpb	$11, %sil
	ja	.L289
	cmpl	$86400000, %r8d
	ja	.L289
	cmpl	$2, %r9d
	ja	.L289
	testb	%r10b, %r10b
	jne	.L291
	movl	$1, 108(%rdi)
.L292:
	testb	%r11b, %r11b
	jle	.L289
.L296:
	movsbq	%sil, %rsi
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rdx
	cmpb	%r11b, (%rdx,%rsi)
	jl	.L289
.L283:
	movb	$0, 120(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movb	$0, 104(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movb	$0, 120(%rdi)
	ret
.L295:
	.cfi_restore_state
	movl	%edx, %r11d
	movl	$4, 108(%rdi)
	negl	%r11d
	movb	%r11b, 73(%rdi)
	cmpb	$7, %cl
	jle	.L292
.L289:
	movl	$1, (%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movb	$0, 120(%rdi)
	ret
.L285:
	.cfi_restore_state
	movb	$0, 104(%rdi)
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L291:
	jle	.L293
	movl	$2, 108(%rdi)
	cmpb	$7, %r10b
	jg	.L289
	addl	$5, %edx
	cmpb	$10, %dl
	jbe	.L283
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L293:
	movb	%cl, 74(%rdi)
	testb	%r11b, %r11b
	jle	.L295
	movl	$3, 108(%rdi)
	cmpb	$7, %cl
	jg	.L289
	jmp	.L296
	.cfi_endproc
.LFE3304:
	.size	_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiiNS0_8TimeModeEaR10UErrorCode, .-_ZN6icu_6714SimpleTimeZone12setStartRuleEiiiiNS0_8TimeModeEaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiiNS0_8TimeModeER10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiiNS0_8TimeModeER10UErrorCode, @function
_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiiNS0_8TimeModeER10UErrorCode:
.LFB3305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movb	%sil, 88(%rdi)
	movb	%dl, 89(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movb	%cl, 90(%rdi)
	movq	16(%rbp), %rax
	movl	%r8d, 92(%rdi)
	movl	%r9d, 84(%rdi)
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L303
	cmpb	$0, 73(%rdi)
	movl	%edx, %r10d
	je	.L304
	testb	%dl, %dl
	jne	.L328
.L304:
	movb	$0, 104(%rdi)
	testb	%dl, %dl
	jne	.L306
.L303:
	movb	$0, 120(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	movl	116(%rdi), %r11d
	movb	$1, 104(%rdi)
	testl	%r11d, %r11d
	jne	.L306
	movl	$3600000, 116(%rdi)
.L306:
	cmpb	$11, %sil
	ja	.L311
	cmpl	$86400000, %r8d
	ja	.L311
	cmpl	$2, %r9d
	ja	.L311
	testb	%cl, %cl
	jne	.L313
	movl	$1, 112(%rdi)
.L314:
	testb	%r10b, %r10b
	jle	.L311
.L318:
	movsbq	%sil, %rsi
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rdx
	cmpb	%r10b, (%rdx,%rsi)
	jge	.L303
.L311:
	movl	$1, (%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movb	$0, 120(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	jle	.L315
	movl	$2, 112(%rdi)
	cmpb	$7, %cl
	jg	.L311
	addl	$5, %edx
	cmpb	$10, %dl
	jbe	.L303
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L315:
	negl	%ecx
	movb	%cl, 90(%rdi)
	testb	%dl, %dl
	jle	.L317
	movl	$3, 112(%rdi)
	cmpb	$7, %cl
	jg	.L311
	jmp	.L318
.L317:
	movl	%edx, %r10d
	movl	$4, 112(%rdi)
	negl	%r10d
	movb	%r10b, 89(%rdi)
	cmpb	$7, %cl
	jg	.L311
	jmp	.L314
	.cfi_endproc
.LFE3305:
	.size	_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiiNS0_8TimeModeER10UErrorCode, .-_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiiNS0_8TimeModeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiNS0_8TimeModeER10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiNS0_8TimeModeER10UErrorCode, @function
_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiNS0_8TimeModeER10UErrorCode:
.LFB3306:
	.cfi_startproc
	endbr64
	movb	%sil, 88(%rdi)
	movb	%dl, 89(%rdi)
	movb	$0, 90(%rdi)
	movl	(%r9), %r10d
	movl	%ecx, 92(%rdi)
	movl	%r8d, 84(%rdi)
	testl	%r10d, %r10d
	jg	.L331
	cmpb	$0, 73(%rdi)
	je	.L332
	testb	%dl, %dl
	jne	.L347
.L332:
	movb	$0, 104(%rdi)
	testb	%dl, %dl
	jne	.L334
.L331:
	movb	$0, 120(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	movl	116(%rdi), %eax
	movb	$1, 104(%rdi)
	testl	%eax, %eax
	jne	.L334
	movl	$3600000, 116(%rdi)
.L334:
	cmpb	$11, %sil
	ja	.L339
	cmpl	$86400000, %ecx
	ja	.L339
	cmpl	$2, %r8d
	ja	.L339
	movl	$1, 112(%rdi)
	testb	%dl, %dl
	jle	.L339
	movsbq	%sil, %rsi
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rax
	cmpb	(%rax,%rsi), %dl
	jle	.L331
.L339:
	movl	$1, (%r9)
	movb	$0, 120(%rdi)
	ret
	.cfi_endproc
.LFE3306:
	.size	_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiNS0_8TimeModeER10UErrorCode, .-_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiNS0_8TimeModeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiiNS0_8TimeModeEaR10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiiNS0_8TimeModeEaR10UErrorCode, @function
_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiiNS0_8TimeModeEaR10UErrorCode:
.LFB3307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r11d
	movl	%ecx, %r10d
	negl	%r11d
	negl	%r10d
	movb	%sil, 88(%rdi)
	movb	%r10b, 90(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%r8d, 92(%rdi)
	cmpb	$0, 16(%rbp)
	movq	24(%rbp), %rax
	movl	%r9d, 84(%rdi)
	cmove	%r11d, %edx
	movb	%dl, 89(%rdi)
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jg	.L351
	cmpb	$0, 73(%rdi)
	movl	%edx, %r11d
	je	.L352
	testb	%dl, %dl
	jne	.L376
.L352:
	movb	$0, 104(%rdi)
	testb	%r11b, %r11b
	jne	.L354
.L351:
	movb	$0, 120(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	cmpl	$0, 116(%rdi)
	movb	$1, 104(%rdi)
	jne	.L354
	movl	$3600000, 116(%rdi)
.L354:
	cmpb	$11, %sil
	ja	.L359
	cmpl	$86400000, %r8d
	ja	.L359
	cmpl	$2, %r9d
	ja	.L359
	testb	%r10b, %r10b
	jne	.L361
	movl	$1, 112(%rdi)
.L362:
	testb	%r11b, %r11b
	jle	.L359
.L366:
	movsbq	%sil, %rsi
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rdx
	cmpb	%r11b, (%rdx,%rsi)
	jge	.L351
.L359:
	movl	$1, (%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movb	$0, 120(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	jle	.L363
	movl	$2, 112(%rdi)
	cmpb	$7, %r10b
	jg	.L359
	addl	$5, %edx
	cmpb	$10, %dl
	jbe	.L351
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L363:
	movb	%cl, 90(%rdi)
	testb	%r11b, %r11b
	jle	.L365
	movl	$3, 112(%rdi)
	cmpb	$7, %cl
	jg	.L359
	jmp	.L366
.L365:
	movl	%edx, %r11d
	movl	$4, 112(%rdi)
	negl	%r11d
	movb	%r11b, 89(%rdi)
	cmpb	$7, %cl
	jg	.L359
	jmp	.L362
	.cfi_endproc
.LFE3307:
	.size	_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiiNS0_8TimeModeEaR10UErrorCode, .-_ZN6icu_6714SimpleTimeZone10setEndRuleEiiiiNS0_8TimeModeEaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone13compareToRuleEaaaaaiiNS0_5EModeEaaai
	.type	_ZN6icu_6714SimpleTimeZone13compareToRuleEaaaaaiiNS0_5EModeEaaai, @function
_ZN6icu_6714SimpleTimeZone13compareToRuleEaaaaaiiNS0_5EModeEaaai:
.LFB3312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	addl	16(%rbp), %r9d
	movl	24(%rbp), %r11d
	pushq	%r12
	movl	48(%rbp), %r10d
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	40(%rbp), %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movl	32(%rbp), %ebx
	cmpl	$86399999, %r9d
	jle	.L378
	movl	%esi, %eax
	.p2align 4,,10
	.p2align 3
.L381:
	movsbw	%r8b, %dx
	movl	%r8d, %r13d
	addl	$1, %ecx
	subl	$86400000, %r9d
	imull	$-109, %edx, %edx
	sarb	$7, %r13b
	shrw	$8, %dx
	addl	%r8d, %edx
	sarb	$2, %dl
	subl	%r13d, %edx
	leal	0(,%rdx,8), %r13d
	subl	%edx, %r13d
	movl	%r13d, %edx
	subl	%edx, %r8d
	addl	$1, %r8d
	cmpb	%cl, %al
	jge	.L379
	addl	$1, %edi
	movl	$1, %ecx
.L379:
	cmpl	$86399999, %r9d
	jg	.L381
.L380:
	movl	$-1, %eax
	cmpb	%dil, %bl
	jg	.L377
	movl	$1, %eax
	jl	.L377
	cmpb	%r10b, %sil
	movsbl	%cl, %ecx
	cmovle	%esi, %r10d
	cmpl	$3, %r11d
	je	.L385
	ja	.L386
	movsbl	%r10b, %edx
	cmpl	$1, %r11d
	je	.L389
	cmpl	$2, %r11d
	jne	.L400
	movsbl	%r12b, %r12d
	movsbl	%r8b, %r8d
	testb	%r10b, %r10b
	jle	.L391
	subl	%ecx, %r8d
	subl	$1, %edx
	subl	%r8d, %r12d
	leal	6(%r12), %esi
	movslq	%esi, %rax
	movl	%esi, %edi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edi
	shrq	$32, %rax
	addl	%esi, %eax
	sarl	$2, %eax
	subl	%edi, %eax
	leal	0(,%rax,8), %edi
	subl	%eax, %edi
	leal	0(,%rdx,8), %eax
	subl	%edi, %esi
	subl	%edx, %eax
	leal	1(%rsi,%rax), %edx
	.p2align 4,,10
	.p2align 3
.L389:
	movl	$-1, %eax
	cmpl	%ecx, %edx
	jg	.L377
	movl	$1, %eax
	jl	.L377
	xorl	%eax, %eax
	cmpl	%r9d, 56(%rbp)
	movl	$-1, %edx
	setl	%al
	cmovg	%edx, %eax
.L377:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	subl	$1, %edi
	movl	%edx, %ecx
.L378:
	testl	%r9d, %r9d
	jns	.L380
	movsbl	%r8b, %r8d
	subl	$1, %ecx
	addl	$86400000, %r9d
	addl	$5, %r8d
	movslq	%r8d, %rax
	movl	%r8d, %r13d
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %r13d
	shrq	$32, %rax
	addl	%r8d, %eax
	sarl	$2, %eax
	subl	%r13d, %eax
	leal	0(,%rax,8), %r13d
	subl	%eax, %r13d
	subl	%r13d, %r8d
	addl	$1, %r8d
	testb	%cl, %cl
	jg	.L378
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L386:
	cmpl	$4, %r11d
	jne	.L400
	movsbl	%r10b, %r10d
	movsbl	%r12b, %r12d
	movsbl	%r8b, %r8d
	movl	%r10d, %eax
	subl	%r12d, %eax
	leal	49(%rax,%r8), %edx
	subl	%ecx, %edx
	movslq	%edx, %rax
	movl	%edx, %esi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %esi
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$2, %eax
	subl	%esi, %eax
	leal	0(,%rax,8), %esi
	subl	%eax, %esi
	subl	%esi, %edx
	subl	%edx, %r10d
	movl	%r10d, %edx
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L400:
	xorl	%edx, %edx
	jmp	.L389
.L385:
	movsbl	%r12b, %edx
	movsbl	%r10b, %r10d
	movsbl	%r8b, %r8d
	addl	$49, %edx
	subl	%r10d, %edx
	subl	%r8d, %edx
	addl	%ecx, %edx
	movslq	%edx, %rax
	movl	%edx, %esi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %esi
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$2, %eax
	subl	%esi, %eax
	leal	0(,%rax,8), %esi
	subl	%eax, %esi
	subl	%esi, %edx
	addl	%r10d, %edx
	jmp	.L389
.L391:
	leal	1(%rdx), %eax
	movsbl	%sil, %esi
	leal	(%rsi,%rax,8), %r10d
	subl	%ecx, %esi
	leal	7(%rsi,%r8), %esi
	subl	%eax, %r10d
	subl	%r12d, %esi
	movslq	%esi, %rax
	movl	%esi, %edx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edx
	shrq	$32, %rax
	addl	%esi, %eax
	sarl	$2, %eax
	subl	%edx, %eax
	leal	0(,%rax,8), %edx
	subl	%eax, %edx
	subl	%edx, %esi
	movl	%r10d, %edx
	subl	%esi, %edx
	jmp	.L389
	.cfi_endproc
.LFE3312:
	.size	_ZN6icu_6714SimpleTimeZone13compareToRuleEaaaaaiiNS0_5EModeEaaai, .-_ZN6icu_6714SimpleTimeZone13compareToRuleEaaaaaiiNS0_5EModeEaaai
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiR10UErrorCode
	.type	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiR10UErrorCode, @function
_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiR10UErrorCode:
.LFB3309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rbp), %rbx
	cmpl	$11, %ecx
	ja	.L412
	movq	%rdi, %r13
	movl	%r8d, %r14d
	movl	%edx, %r8d
	movl	%esi, %edi
	movq	0(%r13), %rax
	movl	%edx, %r10d
	movl	%ecx, %r12d
	andl	$3, %r8d
	movq	168(%rax), %rax
	testl	%ecx, %ecx
	je	.L421
	leal	-1(%rcx), %edx
	testl	%r8d, %r8d
	je	.L430
.L406:
	leaq	_ZN6icu_675Grego12MONTH_LENGTHE(%rip), %rsi
	movslq	%edx, %rdx
	movsbl	(%rsi,%rdx), %edx
	movl	%edx, %r11d
.L405:
	movl	%r12d, %ecx
	testl	%r8d, %r8d
	jne	.L408
	imull	$-1030792151, %r10d, %ecx
	addl	$85899344, %ecx
	movl	%ecx, %r8d
	rorl	$2, %r8d
	cmpl	$42949672, %r8d
	ja	.L410
	rorl	$4, %ecx
	cmpl	$10737418, %ecx
	ja	.L431
.L410:
	leal	12(%r12), %ecx
.L408:
	movslq	%ecx, %rcx
	leaq	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiiR10UErrorCode(%rip), %r15
	movzbl	%r9b, %r8d
	movsbl	(%rsi,%rcx), %esi
	movl	%esi, %ecx
	cmpq	%r15, %rax
	jne	.L411
	movl	(%rbx), %eax
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jg	.L402
	cmpb	$1, %dil
	ja	.L412
	testl	%r14d, %r14d
	jle	.L412
	subl	$1, %r9d
	cmpb	$6, %r9b
	ja	.L412
	cmpl	%r14d, %esi
	jl	.L412
	subl	$28, %ecx
	cmpb	$3, %cl
	ja	.L412
	cmpl	$86399999, 16(%rbp)
	ja	.L412
	subl	$28, %r11d
	cmpb	$3, %r11b
	ja	.L412
	cmpb	$0, 104(%r13)
	movl	100(%r13), %r15d
	je	.L402
	cmpl	96(%r13), %r10d
	jl	.L402
	andl	$1, %edi
	je	.L402
	movzbl	88(%r13), %eax
	movsbl	72(%r13), %edi
	movl	76(%r13), %r11d
	movsbl	73(%r13), %r10d
	cmpb	%al, %dil
	movb	%al, -49(%rbp)
	movsbl	74(%r13), %r9d
	movl	%edi, %ebx
	setg	-56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 80(%r13)
	movl	108(%r13), %ecx
	jne	.L413
	movl	%r15d, %eax
	negl	%eax
.L413:
	pushq	%r11
	pushq	%r10
	pushq	%r9
	movl	16(%rbp), %r9d
	pushq	%rdi
	movl	%r12d, %edi
	pushq	%rcx
	movl	%r14d, %ecx
	pushq	%rax
	movl	%r8d, -68(%rbp)
	movl	%edx, -64(%rbp)
	movl	%esi, -60(%rbp)
	call	_ZN6icu_6714SimpleTimeZone13compareToRuleEaaaaaiiNS0_5EModeEaaai
	addq	$48, %rsp
	notl	%eax
	movl	%eax, %r10d
	shrl	$31, %r10d
	movl	%r10d, %eax
	cmpb	%r10b, -56(%rbp)
	je	.L414
	movsbl	-49(%rbp), %eax
	movl	84(%r13), %ecx
	movl	92(%r13), %r9d
	movsbl	89(%r13), %edi
	testl	%ecx, %ecx
	movsbl	90(%r13), %r11d
	movl	-60(%rbp), %esi
	movl	%eax, -56(%rbp)
	movl	112(%r13), %eax
	movl	-64(%rbp), %edx
	movl	-68(%rbp), %r8d
	movl	%eax, -72(%rbp)
	je	.L432
	movl	%r15d, %eax
	negl	%eax
	cmpl	$2, %ecx
	movl	$0, %ecx
	cmovne	%ecx, %eax
.L416:
	pushq	%r9
	movl	16(%rbp), %r9d
	movl	%r14d, %ecx
	pushq	%rdi
	movl	-56(%rbp), %edi
	pushq	%r11
	pushq	%rdi
	movl	-72(%rbp), %edi
	movl	%r10d, -60(%rbp)
	pushq	%rdi
	movl	%r12d, %edi
	pushq	%rax
	call	_ZN6icu_6714SimpleTimeZone13compareToRuleEaaaaaiiNS0_5EModeEaaai
	addq	$48, %rsp
	movl	-60(%rbp), %r10d
	shrl	$31, %eax
	cmpb	-49(%rbp), %bl
	jg	.L417
	testb	%al, %al
	je	.L402
	testb	%r10b, %r10b
	je	.L402
.L418:
	addl	116(%r13), %r15d
.L402:
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	imull	$-1030792151, %r10d, %ecx
	addl	$85899344, %ecx
	movl	%ecx, %esi
	rorl	$2, %esi
	cmpl	$42949672, %esi
	jbe	.L407
	leal	11(%r12), %edx
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L421:
	movl	$31, %edx
	movl	$31, %r11d
	leaq	_ZN6icu_675Grego12MONTH_LENGTHE(%rip), %rsi
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L407:
	rorl	$4, %ecx
	leal	11(%r12), %esi
	cmpl	$10737419, %ecx
	cmovb	%esi, %edx
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L431:
	movl	%r12d, %ecx
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L411:
	pushq	%rbx
	movl	16(%rbp), %ebx
	movzbl	%dil, %r11d
	movl	%r8d, %r9d
	pushq	%rdx
	movl	%r14d, %r8d
	movl	%r12d, %ecx
	movl	%r10d, %edx
	pushq	%rsi
	movq	%r13, %rdi
	movl	%r11d, %esi
	pushq	%rbx
	call	*%rax
	movl	%eax, %r15d
	addq	$32, %rsp
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L432:
	movl	116(%r13), %eax
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L412:
	movl	$1, (%rbx)
	movl	$-1, %r15d
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L414:
	cmpb	-49(%rbp), %bl
	jle	.L402
.L420:
	testb	%al, %al
	je	.L402
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L417:
	orl	%r10d, %eax
	jmp	.L420
	.cfi_endproc
.LFE3309:
	.size	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiR10UErrorCode, .-_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiiR10UErrorCode
	.type	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiiR10UErrorCode, @function
_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiiR10UErrorCode:
.LFB3310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rbp), %rax
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L433
	cmpb	$1, %sil
	ja	.L435
	testl	%r8d, %r8d
	jle	.L435
	movl	%ecx, %r13d
	cmpl	$11, %ecx
	ja	.L435
	leal	-1(%r9), %ecx
	movl	%r9d, %r12d
	cmpb	$6, %cl
	ja	.L435
	cmpl	24(%rbp), %r8d
	jg	.L435
	movq	%rdi, %rbx
	movl	24(%rbp), %edi
	leal	-28(%rdi), %ecx
	cmpl	$3, %ecx
	ja	.L435
	cmpl	$86399999, 16(%rbp)
	ja	.L435
	movl	32(%rbp), %edi
	leal	-28(%rdi), %ecx
	cmpl	$3, %ecx
	ja	.L435
	cmpb	$0, 104(%rbx)
	movl	100(%rbx), %r15d
	je	.L433
	cmpl	96(%rbx), %edx
	jl	.L433
	andl	$1, %esi
	jne	.L452
.L433:
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_restore_state
	movzbl	88(%rbx), %eax
	movsbl	72(%rbx), %esi
	movl	76(%rbx), %r9d
	movsbl	73(%rbx), %r11d
	cmpb	%al, %sil
	movb	%al, -49(%rbp)
	movsbl	74(%rbx), %edi
	movl	%esi, %r14d
	setg	-56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 80(%rbx)
	movl	108(%rbx), %edx
	jne	.L437
	movl	%r15d, %eax
	negl	%eax
.L437:
	pushq	%r9
	movzbl	%r12b, %r12d
	movl	16(%rbp), %r9d
	movsbl	%r8b, %ecx
	pushq	%r11
	movl	%r12d, %r8d
	pushq	%rdi
	movl	%r13d, %edi
	pushq	%rsi
	movl	24(%rbp), %esi
	pushq	%rdx
	movl	32(%rbp), %edx
	pushq	%rax
	movl	%ecx, -60(%rbp)
	call	_ZN6icu_6714SimpleTimeZone13compareToRuleEaaaaaiiNS0_5EModeEaaai
	addq	$48, %rsp
	notl	%eax
	movl	%eax, %r11d
	shrl	$31, %r11d
	movl	%r11d, %eax
	cmpb	%r11b, -56(%rbp)
	je	.L438
	movl	84(%rbx), %edx
	movl	92(%rbx), %r8d
	movsbl	89(%rbx), %edi
	movsbl	90(%rbx), %esi
	testl	%edx, %edx
	movsbl	-49(%rbp), %r9d
	movl	112(%rbx), %r10d
	movl	-60(%rbp), %ecx
	je	.L453
	movl	%r15d, %eax
	negl	%eax
	cmpl	$2, %edx
	movl	$0, %edx
	cmovne	%edx, %eax
.L440:
	pushq	%r8
	movl	32(%rbp), %edx
	movl	%r12d, %r8d
	pushq	%rdi
	movl	%r13d, %edi
	pushq	%rsi
	movl	24(%rbp), %esi
	pushq	%r9
	movl	16(%rbp), %r9d
	pushq	%r10
	pushq	%rax
	movl	%r11d, -56(%rbp)
	call	_ZN6icu_6714SimpleTimeZone13compareToRuleEaaaaaiiNS0_5EModeEaaai
	addq	$48, %rsp
	movl	-56(%rbp), %r11d
	shrl	$31, %eax
	cmpb	-49(%rbp), %r14b
	jg	.L441
	testb	%al, %al
	je	.L433
	testb	%r11b, %r11b
	je	.L433
.L442:
	addl	116(%rbx), %r15d
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L453:
	movl	116(%rbx), %eax
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L435:
	movl	$1, (%rax)
	movl	$-1, %r15d
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L438:
	cmpb	-49(%rbp), %r14b
	jle	.L433
.L444:
	testb	%al, %al
	je	.L433
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L441:
	orl	%r11d, %eax
	jmp	.L444
	.cfi_endproc
.LFE3310:
	.size	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiiR10UErrorCode, .-_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiR10UErrorCode
	.type	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiR10UErrorCode, @function
_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiR10UErrorCode:
.LFB3308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$11, %ecx
	ja	.L489
	movl	%edx, %r11d
	movq	(%rdi), %rdx
	movl	%r8d, %r14d
	movq	%rdi, %r13
	movl	%r11d, %r10d
	movl	%ecx, %r12d
	movzbl	%sil, %r15d
	movzbl	%r9b, %r8d
	movq	40(%rdx), %rax
	andl	$3, %r10d
	je	.L490
.L457:
	leaq	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiR10UErrorCode(%rip), %rdi
	cmpq	%rdi, %rax
	jne	.L460
.L493:
	movq	168(%rdx), %rax
	testl	%r12d, %r12d
	je	.L479
	leal	-1(%r12), %edx
	testl	%r10d, %r10d
	je	.L491
.L462:
	leaq	_ZN6icu_675Grego12MONTH_LENGTHE(%rip), %rbx
	movslq	%edx, %rdx
	movsbl	(%rbx,%rdx), %edx
	movl	%edx, %edi
.L461:
	movl	%r12d, %ecx
	testl	%r10d, %r10d
	jne	.L464
	imull	$-1030792151, %r11d, %ecx
	addl	$85899344, %ecx
	movl	%ecx, %r10d
	rorl	$2, %r10d
	cmpl	$42949672, %r10d
	ja	.L466
	rorl	$4, %ecx
	cmpl	$10737418, %ecx
	jbe	.L466
	movl	%r12d, %ecx
	.p2align 4,,10
	.p2align 3
.L464:
	movslq	%ecx, %rcx
	movsbl	(%rbx,%rcx), %r10d
	leaq	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiiR10UErrorCode(%rip), %rbx
	movl	%r10d, %ecx
	cmpq	%rbx, %rax
	jne	.L467
	movq	24(%rbp), %rax
	xorl	%r15d, %r15d
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L454
	cmpb	$1, %sil
	ja	.L468
	testl	%r14d, %r14d
	jle	.L468
	subl	$1, %r9d
	cmpb	$6, %r9b
	ja	.L468
	cmpl	%r10d, %r14d
	jg	.L468
	subl	$28, %ecx
	cmpb	$3, %cl
	ja	.L468
	cmpl	$86399999, 16(%rbp)
	ja	.L468
	subl	$28, %edi
	cmpb	$3, %dil
	ja	.L468
	cmpb	$0, 104(%r13)
	movl	100(%r13), %r15d
	je	.L454
	cmpl	96(%r13), %r11d
	jl	.L454
	andl	$1, %esi
	jne	.L492
.L454:
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	imull	$-1030792151, %r11d, %ecx
	addl	$85899344, %ecx
	movl	%ecx, %edi
	rorl	$2, %edi
	cmpl	$42949672, %edi
	ja	.L459
	rorl	$4, %ecx
	cmpl	$10737418, %ecx
	jbe	.L459
	movl	%r12d, %ecx
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L459:
	leaq	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiR10UErrorCode(%rip), %rdi
	leal	12(%r12), %ecx
	cmpq	%rdi, %rax
	je	.L493
.L460:
	movslq	%ecx, %rcx
	leaq	_ZN6icu_675Grego12MONTH_LENGTHE(%rip), %rdx
	subq	$8, %rsp
	pushq	24(%rbp)
	movsbl	(%rdx,%rcx), %edx
	pushq	%rdx
.L488:
	movl	16(%rbp), %ebx
	movl	%r8d, %r9d
	movl	%r15d, %esi
	movl	%r14d, %r8d
	movl	%r12d, %ecx
	movl	%r11d, %edx
	movq	%r13, %rdi
	pushq	%rbx
	call	*%rax
	movl	%eax, %r15d
	addq	$32, %rsp
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L489:
	movq	24(%rbp), %rax
	xorl	%r15d, %r15d
	movl	$1, (%rax)
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L491:
	imull	$-1030792151, %r11d, %ecx
	addl	$85899344, %ecx
	movl	%ecx, %edi
	rorl	$2, %edi
	cmpl	$42949672, %edi
	jbe	.L463
	leal	11(%r12), %edx
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L479:
	movl	$31, %edx
	movl	$31, %edi
	leaq	_ZN6icu_675Grego12MONTH_LENGTHE(%rip), %rbx
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L466:
	leal	12(%r12), %ecx
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L467:
	pushq	24(%rbp)
	pushq	%rdx
	pushq	%r10
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L463:
	rorl	$4, %ecx
	leal	11(%r12), %edi
	cmpl	$10737419, %ecx
	cmovb	%edi, %edx
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L492:
	movzbl	88(%r13), %eax
	movsbl	72(%r13), %esi
	movl	76(%r13), %r11d
	movsbl	73(%r13), %r9d
	cmpb	%al, %sil
	movb	%al, -49(%rbp)
	movsbl	74(%r13), %edi
	movl	%esi, %ebx
	setg	-56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 80(%r13)
	movl	108(%r13), %ecx
	jne	.L470
	movl	%r15d, %eax
	negl	%eax
.L470:
	pushq	%r11
	pushq	%r9
	movl	16(%rbp), %r9d
	pushq	%rdi
	movl	%r12d, %edi
	pushq	%rsi
	movl	%r10d, %esi
	pushq	%rcx
	movl	%r14d, %ecx
	pushq	%rax
	movl	%r8d, -68(%rbp)
	movl	%edx, -64(%rbp)
	movl	%r10d, -60(%rbp)
	call	_ZN6icu_6714SimpleTimeZone13compareToRuleEaaaaaiiNS0_5EModeEaaai
	addq	$48, %rsp
	notl	%eax
	movl	%eax, %r11d
	shrl	$31, %r11d
	movl	%r11d, %ecx
	cmpb	%r11b, -56(%rbp)
	je	.L471
	movsbl	-49(%rbp), %eax
	movl	84(%r13), %ecx
	movl	92(%r13), %edi
	movsbl	89(%r13), %esi
	testl	%ecx, %ecx
	movsbl	90(%r13), %r9d
	movl	-60(%rbp), %r10d
	movl	%eax, -56(%rbp)
	movl	112(%r13), %eax
	movl	-64(%rbp), %edx
	movl	-68(%rbp), %r8d
	movl	%eax, -72(%rbp)
	jne	.L472
	movl	116(%r13), %eax
.L473:
	pushq	%rdi
	movl	%r14d, %ecx
	movl	%r12d, %edi
	pushq	%rsi
	movl	-56(%rbp), %esi
	pushq	%r9
	movl	16(%rbp), %r9d
	pushq	%rsi
	movl	-72(%rbp), %esi
	movl	%r11d, -60(%rbp)
	pushq	%rsi
	movl	%r10d, %esi
	pushq	%rax
	call	_ZN6icu_6714SimpleTimeZone13compareToRuleEaaaaaiiNS0_5EModeEaaai
	movl	-60(%rbp), %r11d
	addq	$48, %rsp
	shrl	$31, %eax
	movl	%r11d, %ecx
	orl	%eax, %ecx
	cmpb	-49(%rbp), %bl
	jg	.L477
	testb	%al, %al
	je	.L454
	testb	%r11b, %r11b
	je	.L454
.L475:
	addl	116(%r13), %r15d
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L471:
	cmpb	-49(%rbp), %bl
	jle	.L454
.L477:
	testb	%cl, %cl
	je	.L454
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L468:
	movq	24(%rbp), %rax
	movl	$-1, %r15d
	movl	$1, (%rax)
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L472:
	movl	%r15d, %eax
	negl	%eax
	cmpl	$2, %ecx
	movl	$0, %ecx
	cmovne	%ecx, %eax
	jmp	.L473
	.cfi_endproc
.LFE3308:
	.size	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiR10UErrorCode, .-_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone13setDSTSavingsEiR10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZone13setDSTSavingsEiR10UErrorCode, @function
_ZN6icu_6714SimpleTimeZone13setDSTSavingsEiR10UErrorCode:
.LFB3315:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L495
	movl	$1, (%rdx)
	movb	$0, 120(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	movl	%esi, 116(%rdi)
	movb	$0, 120(%rdi)
	ret
	.cfi_endproc
.LFE3315:
	.size	_ZN6icu_6714SimpleTimeZone13setDSTSavingsEiR10UErrorCode, .-_ZN6icu_6714SimpleTimeZone13setDSTSavingsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone11decodeRulesER10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZone11decodeRulesER10UErrorCode, @function
_ZN6icu_6714SimpleTimeZone11decodeRulesER10UErrorCode:
.LFB3320:
	.cfi_startproc
	endbr64
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	jg	.L497
	movzbl	73(%rdi), %edx
	movzbl	89(%rdi), %eax
	testb	%dl, %dl
	je	.L499
	testb	%al, %al
	je	.L500
	movl	116(%rdi), %ecx
	movb	$1, 104(%rdi)
	testl	%ecx, %ecx
	jne	.L501
	movl	$3600000, 116(%rdi)
.L501:
	movsbq	72(%rdi), %rcx
	cmpb	$11, %cl
	ja	.L516
	cmpl	$86400000, 76(%rdi)
	ja	.L516
	cmpl	$2, 80(%rdi)
	ja	.L516
	movzbl	74(%rdi), %r8d
	testb	%r8b, %r8b
	je	.L533
	jle	.L508
	movl	$2, 108(%rdi)
	cmpb	$7, %r8b
	jg	.L516
	addl	$5, %edx
	cmpb	$10, %dl
	ja	.L516
.L512:
	testb	%al, %al
	jne	.L525
	movb	$0, 104(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	movb	$0, 104(%rdi)
	testb	%al, %al
	jne	.L513
.L497:
	ret
.L522:
	negl	%eax
	movl	$4, 112(%rdi)
	movb	%al, 89(%rdi)
	cmpb	$7, %cl
	jle	.L519
.L516:
	movl	$1, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	movl	116(%rdi), %edx
	movb	$1, 104(%rdi)
	testl	%edx, %edx
	jne	.L513
	movl	$3600000, 116(%rdi)
.L513:
	movsbq	88(%rdi), %rdx
	cmpb	$11, %dl
	ja	.L516
	cmpl	$86400000, 92(%rdi)
	ja	.L516
	cmpl	$2, 84(%rdi)
	ja	.L516
	movzbl	90(%rdi), %ecx
	testb	%cl, %cl
	jne	.L518
	movl	$1, 112(%rdi)
.L519:
	testb	%al, %al
	jle	.L516
.L523:
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rcx
	cmpb	%al, (%rcx,%rdx)
	jl	.L516
	ret
.L500:
	movb	$0, 104(%rdi)
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L533:
	movl	$1, 108(%rdi)
.L507:
	testb	%dl, %dl
	jle	.L516
.L511:
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %r8
	cmpb	%dl, (%r8,%rcx)
	jge	.L512
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L518:
	jle	.L520
	movl	$2, 112(%rdi)
	cmpb	$7, %cl
	jg	.L516
	addl	$5, %eax
	cmpb	$10, %al
	ja	.L516
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	negl	%ecx
	movb	%cl, 90(%rdi)
	testb	%al, %al
	jle	.L522
	movl	$3, 112(%rdi)
	cmpb	$7, %cl
	jg	.L516
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L508:
	negl	%r8d
	movb	%r8b, 74(%rdi)
	testb	%dl, %dl
	jle	.L510
	movl	$3, 108(%rdi)
	cmpb	$7, %r8b
	jg	.L516
	jmp	.L511
.L510:
	negl	%edx
	movl	$4, 108(%rdi)
	movb	%dl, 73(%rdi)
	cmpb	$7, %r8b
	jg	.L516
	jmp	.L507
	.cfi_endproc
.LFE3320:
	.size	_ZN6icu_6714SimpleTimeZone11decodeRulesER10UErrorCode, .-_ZN6icu_6714SimpleTimeZone11decodeRulesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone15decodeStartRuleER10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZone15decodeStartRuleER10UErrorCode, @function
_ZN6icu_6714SimpleTimeZone15decodeStartRuleER10UErrorCode:
.LFB3321:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L534
	movzbl	73(%rdi), %eax
	testb	%al, %al
	je	.L537
	cmpb	$0, 89(%rdi)
	je	.L538
	movl	116(%rdi), %edx
	movb	$1, 104(%rdi)
	testl	%edx, %edx
	jne	.L539
	movl	$3600000, 116(%rdi)
.L539:
	movsbq	72(%rdi), %rdx
	cmpb	$11, %dl
	ja	.L542
	cmpl	$86400000, 76(%rdi)
	ja	.L542
	cmpl	$2, 80(%rdi)
	ja	.L542
	movzbl	74(%rdi), %ecx
	testb	%cl, %cl
	jne	.L544
	movl	$1, 108(%rdi)
.L545:
	testb	%al, %al
	jle	.L542
.L549:
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rcx
	cmpb	%al, (%rcx,%rdx)
	jl	.L542
.L534:
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	movb	$0, 104(%rdi)
	ret
.L548:
	negl	%eax
	movl	$4, 108(%rdi)
	movb	%al, 73(%rdi)
	cmpb	$7, %cl
	jle	.L545
.L542:
	movl	$1, (%rsi)
	ret
.L538:
	movb	$0, 104(%rdi)
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L544:
	jle	.L546
	movl	$2, 108(%rdi)
	cmpb	$7, %cl
	jg	.L542
	addl	$5, %eax
	cmpb	$10, %al
	ja	.L542
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	negl	%ecx
	movb	%cl, 74(%rdi)
	testb	%al, %al
	jle	.L548
	movl	$3, 108(%rdi)
	cmpb	$7, %cl
	jg	.L542
	jmp	.L549
	.cfi_endproc
.LFE3321:
	.size	_ZN6icu_6714SimpleTimeZone15decodeStartRuleER10UErrorCode, .-_ZN6icu_6714SimpleTimeZone15decodeStartRuleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone13decodeEndRuleER10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZone13decodeEndRuleER10UErrorCode, @function
_ZN6icu_6714SimpleTimeZone13decodeEndRuleER10UErrorCode:
.LFB3322:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L553
	cmpb	$0, 73(%rdi)
	movzbl	89(%rdi), %eax
	je	.L556
	testb	%al, %al
	je	.L557
	movl	116(%rdi), %edx
	movb	$1, 104(%rdi)
	testl	%edx, %edx
	jne	.L559
	movl	$3600000, 116(%rdi)
.L559:
	movsbq	88(%rdi), %rdx
	cmpb	$11, %dl
	ja	.L562
	cmpl	$86400000, 92(%rdi)
	ja	.L562
	cmpl	$2, 84(%rdi)
	ja	.L562
	movzbl	90(%rdi), %ecx
	testb	%cl, %cl
	jne	.L564
	movl	$1, 112(%rdi)
.L565:
	testb	%al, %al
	jle	.L562
.L569:
	leaq	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE(%rip), %rcx
	cmpb	%al, (%rcx,%rdx)
	jl	.L562
.L553:
	ret
	.p2align 4,,10
	.p2align 3
.L556:
	movb	$0, 104(%rdi)
	testb	%al, %al
	jne	.L559
	ret
.L568:
	negl	%eax
	movl	$4, 112(%rdi)
	movb	%al, 89(%rdi)
	cmpb	$7, %cl
	jle	.L565
.L562:
	movl	$1, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L557:
	movb	$0, 104(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	jle	.L566
	movl	$2, 112(%rdi)
	cmpb	$7, %cl
	jg	.L562
	addl	$5, %eax
	cmpb	$10, %al
	ja	.L562
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	negl	%ecx
	movb	%cl, 90(%rdi)
	testb	%al, %al
	jle	.L568
	movl	$3, 112(%rdi)
	cmpb	$7, %cl
	jg	.L562
	jmp	.L569
	.cfi_endproc
.LFE3322:
	.size	_ZN6icu_6714SimpleTimeZone13decodeEndRuleER10UErrorCode, .-_ZN6icu_6714SimpleTimeZone13decodeEndRuleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone20clearTransitionRulesEv
	.type	_ZN6icu_6714SimpleTimeZone20clearTransitionRulesEv, @function
_ZN6icu_6714SimpleTimeZone20clearTransitionRulesEv:
.LFB3325:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movb	$0, 120(%rdi)
	movups	%xmm0, 128(%rdi)
	movups	%xmm0, 144(%rdi)
	ret
	.cfi_endproc
.LFE3325:
	.size	_ZN6icu_6714SimpleTimeZone20clearTransitionRulesEv, .-_ZN6icu_6714SimpleTimeZone20clearTransitionRulesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone21deleteTransitionRulesEv
	.type	_ZN6icu_6714SimpleTimeZone21deleteTransitionRulesEv, @function
_ZN6icu_6714SimpleTimeZone21deleteTransitionRulesEv:
.LFB3326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	128(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L577
	movq	(%rdi), %rax
	call	*8(%rax)
.L577:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L578
	movq	(%rdi), %rax
	call	*8(%rax)
.L578:
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L579
	movq	(%rdi), %rax
	call	*8(%rax)
.L579:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L580
	movq	(%rdi), %rax
	call	*8(%rax)
.L580:
	pxor	%xmm0, %xmm0
	movb	$0, 120(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 144(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3326:
	.size	_ZN6icu_6714SimpleTimeZone21deleteTransitionRulesEv, .-_ZN6icu_6714SimpleTimeZone21deleteTransitionRulesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SimpleTimeZone19initTransitionRulesER10UErrorCode
	.type	_ZN6icu_6714SimpleTimeZone19initTransitionRulesER10UErrorCode, @function
_ZN6icu_6714SimpleTimeZone19initTransitionRulesER10UErrorCode:
.LFB3334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L594
	cmpb	$0, 120(%rdi)
	movq	%rdi, %r15
	je	.L711
.L594:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L712
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L711:
	.cfi_restore_state
	leaq	-256(%rbp), %r12
	movq	%rsi, %rbx
	call	_ZN6icu_6714SimpleTimeZone21deleteTransitionRulesEv
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	movq	%r12, %rdi
	leaq	8(%r15), %rsi
	movq	%rax, -256(%rbp)
	movw	%cx, -248(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	cmpb	$0, 104(%r15)
	je	.L596
	movl	80(%r15), %r14d
	cmpl	$1, %r14d
	je	.L597
	cmpl	$2, %r14d
	movl	$0, %eax
	cmovne	%eax, %r14d
.L597:
	movl	108(%r15), %eax
	cmpl	$3, %eax
	je	.L598
	ja	.L599
	cmpl	$1, %eax
	je	.L600
	cmpl	$2, %eax
	jne	.L602
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L604
	movsbl	74(%r15), %ecx
	movsbl	73(%r15), %edx
	movl	%r14d, %r9d
	movq	%rax, %rdi
	movsbl	72(%r15), %esi
	movl	76(%r15), %r8d
	call	_ZN6icu_6712DateTimeRuleC1EiiiiNS0_12TimeRuleTypeE@PLT
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L596:
	movq	(%r15), %rax
	leaq	_ZNK6icu_6714SimpleTimeZone12getRawOffsetEv(%rip), %rdx
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L636
	movl	100(%r15), %r14d
.L637:
	movl	$80, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L638
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6719InitialTimeZoneRuleC1ERKNS_13UnicodeStringEii@PLT
	movq	%r13, 128(%r15)
.L634:
	movb	$1, 120(%r15)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L599:
	cmpl	$4, %eax
	jne	.L602
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L604
	subq	$8, %rsp
	movsbl	74(%r15), %ecx
	movl	76(%r15), %r9d
	xorl	%r8d, %r8d
	movsbl	73(%r15), %edx
	movsbl	72(%r15), %esi
	pushq	%r14
	movq	%rax, %rdi
	call	_ZN6icu_6712DateTimeRuleC1EiiiaiNS0_12TimeRuleTypeE@PLT
	popq	%r11
	popq	%r14
.L605:
	leaq	-192(%rbp), %r14
	leaq	_ZN6icu_67L7DST_STRE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	leaq	-128(%rbp), %rax
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movq	(%r15), %rax
	leaq	_ZNK6icu_6714SimpleTimeZone12getRawOffsetEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L713
	movl	100(%r15), %edx
.L607:
	movq	104(%rax), %rax
	leaq	_ZNK6icu_6714SimpleTimeZone13getDSTSavingsEv(%rip), %rcx
	cmpq	%rcx, %rax
	jne	.L608
	movl	116(%r15), %ecx
.L609:
	movl	$96, %edi
	movl	%ecx, -296(%rbp)
	movl	%edx, -288(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L610
	movl	_ZN6icu_6718AnnualTimeZoneRule8MAX_YEARE(%rip), %esi
	subq	$8, %rsp
	movq	%rax, %rdi
	movq	%r13, %r8
	movl	-288(%rbp), %edx
	movl	-296(%rbp), %ecx
	movq	%rax, -288(%rbp)
	pushq	%rsi
	movq	-280(%rbp), %rsi
	movl	96(%r15), %r9d
	call	_ZN6icu_6718AnnualTimeZoneRuleC1ERKNS_13UnicodeStringEiiPNS_12DateTimeRuleEii@PLT
	movq	-288(%rbp), %rax
	popq	%r9
	popq	%r10
.L610:
	movq	%rax, 152(%r15)
	movq	-280(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	152(%r15), %r8
	testq	%r8, %r8
	je	.L710
	movq	(%r8), %rax
	leaq	_ZNK6icu_6714SimpleTimeZone12getRawOffsetEv(%rip), %rcx
	movq	56(%rax), %r9
	movq	(%r15), %rax
	movq	64(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L612
	movl	100(%r15), %esi
.L613:
	movq	%r8, %rdi
	leaq	-264(%rbp), %rcx
	xorl	%edx, %edx
	call	*%r9
	movl	84(%r15), %r8d
	cmpl	$1, %r8d
	je	.L614
	cmpl	$2, %r8d
	movl	$0, %eax
	cmovne	%eax, %r8d
.L614:
	movl	112(%r15), %eax
	cmpl	$3, %eax
	je	.L640
	ja	.L641
	cmpl	$1, %eax
	jne	.L714
	movl	$40, %edi
	movl	%r8d, -288(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L710
	movl	92(%r15), %ecx
	movsbl	89(%r15), %edx
	movq	%rax, %rdi
	movsbl	88(%r15), %esi
	movl	-288(%rbp), %r8d
	call	_ZN6icu_6712DateTimeRuleC1EiiiNS0_12TimeRuleTypeE@PLT
.L616:
	leaq	_ZN6icu_67L7STD_STRE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-280(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movq	(%r15), %rax
	leaq	_ZNK6icu_6714SimpleTimeZone12getRawOffsetEv(%rip), %rcx
	movq	64(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L617
	movl	100(%r15), %edx
.L618:
	movl	$96, %edi
	movl	%edx, -288(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L619
	movl	_ZN6icu_6718AnnualTimeZoneRule8MAX_YEARE(%rip), %ecx
	subq	$8, %rsp
	movq	%rax, %rdi
	movq	%r13, %r8
	movl	-288(%rbp), %edx
	movq	-280(%rbp), %rsi
	movq	%rax, -288(%rbp)
	pushq	%rcx
	movl	96(%r15), %r9d
	xorl	%ecx, %ecx
	call	_ZN6icu_6718AnnualTimeZoneRuleC1ERKNS_13UnicodeStringEiiPNS_12DateTimeRuleEii@PLT
	popq	%rax
	movq	-288(%rbp), %rax
	popq	%rdx
.L619:
	movq	%rax, 144(%r15)
	movq	-280(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	144(%r15), %r13
	testq	%r13, %r13
	je	.L710
	movq	0(%r13), %rax
	movq	152(%r15), %rdi
	movq	56(%rax), %rax
	movq	%rax, -288(%rbp)
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	leaq	_ZNK6icu_6714SimpleTimeZone12getRawOffsetEv(%rip), %rcx
	movl	%eax, %edx
	movq	(%r15), %rax
	movq	64(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L621
	movl	100(%r15), %esi
.L622:
	movq	-288(%rbp), %rax
	leaq	-272(%rbp), %rcx
	movq	%r13, %rdi
	call	*%rax
	movsd	-264(%rbp), %xmm0
	comisd	-272(%rbp), %xmm0
	jbe	.L707
	leaq	_ZN6icu_67L7DST_STRE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-280(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movq	(%r15), %rax
	leaq	_ZNK6icu_6714SimpleTimeZone12getRawOffsetEv(%rip), %rcx
	movq	64(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L625
	movl	100(%r15), %edx
.L626:
	movq	152(%r15), %rdi
	movl	%edx, -296(%rbp)
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movl	$80, %edi
	movl	%eax, -288(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L627
	movl	-288(%rbp), %ecx
	movl	-296(%rbp), %edx
	movq	%rax, %rdi
	movq	-280(%rbp), %rsi
	call	_ZN6icu_6719InitialTimeZoneRuleC1ERKNS_13UnicodeStringEii@PLT
.L627:
	movq	%r13, 128(%r15)
	movq	-280(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpq	$0, 128(%r15)
	je	.L710
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L633
	movq	144(%r15), %rdx
	movq	128(%r15), %rsi
	movq	%rax, %rdi
	movsd	-272(%rbp), %xmm0
	call	_ZN6icu_6718TimeZoneTransitionC1EdRKNS_12TimeZoneRuleES3_@PLT
.L633:
	movq	%r13, 136(%r15)
	testq	%r13, %r13
	jne	.L634
	.p2align 4,,10
	.p2align 3
.L710:
	movl	$7, (%rbx)
	movq	%r15, %rdi
	call	_ZN6icu_6714SimpleTimeZone21deleteTransitionRulesEv
.L635:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L714:
	cmpl	$2, %eax
	jne	.L616
	movl	$40, %edi
	movl	%r8d, -288(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L710
	movl	-288(%rbp), %r8d
	movsbl	90(%r15), %ecx
	movq	%rax, %rdi
	movsbl	89(%r15), %edx
	movsbl	88(%r15), %esi
	movl	%r8d, %r9d
	movl	92(%r15), %r8d
	call	_ZN6icu_6712DateTimeRuleC1EiiiiNS0_12TimeRuleTypeE@PLT
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L641:
	cmpl	$4, %eax
	jne	.L616
	movl	$40, %edi
	movl	%r8d, -288(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L710
	subq	$8, %rsp
	movsbl	90(%r15), %ecx
	movl	92(%r15), %r9d
	movq	%rax, %rdi
	movl	-288(%rbp), %r8d
	movsbl	88(%r15), %esi
	movsbl	89(%r15), %edx
	pushq	%r8
	xorl	%r8d, %r8d
	call	_ZN6icu_6712DateTimeRuleC1EiiiaiNS0_12TimeRuleTypeE@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L616
.L604:
	movl	$7, (%rbx)
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L636:
	movq	%r15, %rdi
	call	*%rax
	movl	%eax, %r14d
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L600:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L604
	movl	76(%r15), %ecx
	movsbl	73(%r15), %edx
	movl	%r14d, %r8d
	movq	%rax, %rdi
	movsbl	72(%r15), %esi
	call	_ZN6icu_6712DateTimeRuleC1EiiiNS0_12TimeRuleTypeE@PLT
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L602:
	movl	$27, (%rbx)
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L598:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L604
	subq	$8, %rsp
	movsbl	73(%r15), %edx
	movl	76(%r15), %r9d
	movq	%rax, %rdi
	movsbl	74(%r15), %ecx
	movsbl	72(%r15), %esi
	pushq	%r14
	movl	$1, %r8d
	call	_ZN6icu_6712DateTimeRuleC1EiiiaiNS0_12TimeRuleTypeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L707:
	leaq	_ZN6icu_67L7STD_STRE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-280(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movq	(%r15), %rax
	leaq	_ZNK6icu_6714SimpleTimeZone12getRawOffsetEv(%rip), %rcx
	movq	64(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L630
	movl	100(%r15), %edx
.L631:
	movl	$80, %edi
	movl	%edx, -288(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L632
	movl	-288(%rbp), %edx
	movq	-280(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN6icu_6719InitialTimeZoneRuleC1ERKNS_13UnicodeStringEii@PLT
.L632:
	movq	%r13, 128(%r15)
	movq	-280(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpq	$0, 128(%r15)
	je	.L710
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L633
	movq	152(%r15), %rdx
	movq	128(%r15), %rsi
	movq	%rax, %rdi
	movsd	-264(%rbp), %xmm0
	call	_ZN6icu_6718TimeZoneTransitionC1EdRKNS_12TimeZoneRuleES3_@PLT
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L608:
	movl	%edx, -288(%rbp)
	movq	%r15, %rdi
	call	*%rax
	movl	-288(%rbp), %edx
	movl	%eax, %ecx
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L713:
	movq	%r15, %rdi
	call	*%rdx
	movl	%eax, %edx
	movq	(%r15), %rax
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L612:
	movq	%r9, -296(%rbp)
	movq	%r15, %rdi
	movq	%r8, -288(%rbp)
	call	*%rax
	movq	-296(%rbp), %r9
	movq	-288(%rbp), %r8
	movl	%eax, %esi
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L640:
	movl	$40, %edi
	movl	%r8d, -288(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L710
	subq	$8, %rsp
	movsbl	90(%r15), %ecx
	movl	92(%r15), %r9d
	movq	%rax, %rdi
	movl	-288(%rbp), %r8d
	movsbl	89(%r15), %edx
	movsbl	88(%r15), %esi
	pushq	%r8
	movl	$1, %r8d
	call	_ZN6icu_6712DateTimeRuleC1EiiiaiNS0_12TimeRuleTypeE@PLT
	popq	%rdi
	popq	%r8
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L617:
	movq	%r15, %rdi
	call	*%rax
	movl	%eax, %edx
	jmp	.L618
.L621:
	movl	%edx, -296(%rbp)
	movq	%r15, %rdi
	call	*%rax
	movl	-296(%rbp), %edx
	movl	%eax, %esi
	jmp	.L622
.L625:
	movq	%r15, %rdi
	call	*%rax
	movl	%eax, %edx
	jmp	.L626
.L630:
	movq	%r15, %rdi
	call	*%rax
	movl	%eax, %edx
	jmp	.L631
.L712:
	call	__stack_chk_fail@PLT
.L638:
	movq	$0, 128(%r15)
	jmp	.L710
	.cfi_endproc
.LFE3334:
	.size	_ZN6icu_6714SimpleTimeZone19initTransitionRulesER10UErrorCode, .-_ZN6icu_6714SimpleTimeZone19initTransitionRulesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE
	.type	_ZNK6icu_6714SimpleTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE, @function
_ZNK6icu_6714SimpleTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE:
.LFB3324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movsd	%xmm0, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 104(%rdi)
	jne	.L716
.L741:
	xorl	%eax, %eax
.L715:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L743
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L716:
	.cfi_restore_state
	movq	%rdi, %rbx
	leaq	_ZZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCodeE5gLock(%rip), %rdi
	movl	%esi, %r12d
	movq	%rdx, %r13
	movl	$0, -76(%rbp)
	call	umtx_lock_67@PLT
	cmpb	$0, 120(%rbx)
	je	.L744
.L718:
	leaq	_ZZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCodeE5gLock(%rip), %rdi
	call	umtx_unlock_67@PLT
	movl	-76(%rbp), %eax
	testl	%eax, %eax
	jg	.L741
	movq	136(%rbx), %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-88(%rbp), %xmm2
	comisd	%xmm2, %xmm0
	ja	.L741
	testb	%r12b, %r12b
	jne	.L730
	ucomisd	%xmm2, %xmm0
	movl	$0, %edx
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L741
.L730:
	movq	144(%rbx), %r14
	movq	152(%rbx), %rdi
	movsbl	%r12b, %ecx
	movl	%ecx, -96(%rbp)
	movq	(%r14), %rax
	movq	80(%rax), %r15
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	152(%rbx), %rdi
	movl	%eax, %r12d
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movl	-96(%rbp), %ecx
	movl	%r12d, %edx
	movsd	-88(%rbp), %xmm0
	movl	%eax, %esi
	leaq	-72(%rbp), %r8
	movq	%r14, %rdi
	movl	%ecx, -100(%rbp)
	call	*%r15
	movq	152(%rbx), %r12
	movq	144(%rbx), %rdi
	movl	%eax, %r14d
	movq	(%r12), %rax
	movq	80(%rax), %r9
	movq	%r9, -96(%rbp)
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	144(%rbx), %rdi
	movl	%eax, %r15d
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movl	-100(%rbp), %ecx
	movsd	-88(%rbp), %xmm0
	movl	%r15d, %edx
	movq	-96(%rbp), %r9
	movl	%eax, %esi
	leaq	-64(%rbp), %r8
	movq	%r12, %rdi
	call	*%r9
	testb	%r14b, %r14b
	je	.L721
	movsd	-72(%rbp), %xmm0
	testb	%al, %al
	je	.L722
	movsd	-64(%rbp), %xmm1
	comisd	%xmm1, %xmm0
	ja	.L722
	comisd	%xmm0, %xmm1
	jbe	.L741
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L744:
	leaq	-76(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6714SimpleTimeZone19initTransitionRulesER10UErrorCode
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L721:
	movsd	-64(%rbp), %xmm1
	testb	%al, %al
	je	.L715
.L727:
	movapd	%xmm1, %xmm0
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransition7setTimeEd@PLT
	movq	144(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransition7setFromERKNS_12TimeZoneRuleE@PLT
	movq	152(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransition5setToERKNS_12TimeZoneRuleE@PLT
	movl	$1, %eax
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L722:
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransition7setTimeEd@PLT
	movq	152(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransition7setFromERKNS_12TimeZoneRuleE@PLT
	movq	144(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransition5setToERKNS_12TimeZoneRuleE@PLT
	movl	$1, %eax
	jmp	.L715
.L743:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3324:
	.size	_ZNK6icu_6714SimpleTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE, .-_ZNK6icu_6714SimpleTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCode
	.type	_ZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCode, @function
_ZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCode:
.LFB3327:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L751
	ret
	.p2align 4,,10
	.p2align 3
.L751:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	leaq	_ZZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCodeE5gLock(%rip), %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	umtx_lock_67@PLT
	cmpb	$0, 120(%r13)
	je	.L752
.L747:
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	_ZZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCodeE5gLock(%rip), %rdi
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.p2align 4,,10
	.p2align 3
.L752:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6714SimpleTimeZone19initTransitionRulesER10UErrorCode
	jmp	.L747
	.cfi_endproc
.LFE3327:
	.size	_ZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCode, .-_ZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE
	.type	_ZNK6icu_6714SimpleTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE, @function
_ZNK6icu_6714SimpleTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE:
.LFB3323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movsd	%xmm0, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 104(%rdi)
	jne	.L754
.L782:
	xorl	%eax, %eax
.L753:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L784
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L754:
	.cfi_restore_state
	movq	%rdi, %rbx
	leaq	_ZZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCodeE5gLock(%rip), %rdi
	movl	%esi, %r12d
	movq	%rdx, %r13
	movl	$0, -76(%rbp)
	call	umtx_lock_67@PLT
	cmpb	$0, 120(%rbx)
	je	.L785
.L756:
	leaq	_ZZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCodeE5gLock(%rip), %rdi
	call	umtx_unlock_67@PLT
	movl	-76(%rbp), %eax
	testl	%eax, %eax
	jg	.L782
	movq	136(%rbx), %rdi
	movsbl	%r12b, %r14d
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-88(%rbp), %xmm2
	comisd	%xmm2, %xmm0
	ja	.L758
	testb	%r12b, %r12b
	je	.L759
	ucomisd	%xmm0, %xmm2
	movl	$0, %edx
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L759
.L758:
	movq	136(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransitionaSERKS0_@PLT
.L759:
	movq	144(%rbx), %r15
	movq	152(%rbx), %rdi
	movq	(%r15), %rax
	movq	72(%rax), %r9
	movq	%r9, -96(%rbp)
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	152(%rbx), %rdi
	movl	%eax, %r12d
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movl	%r12d, %edx
	movsd	-88(%rbp), %xmm0
	movl	%r14d, %ecx
	movl	%eax, %esi
	leaq	-72(%rbp), %r8
	movq	%r15, %rdi
	movq	-96(%rbp), %r9
	call	*%r9
	movq	152(%rbx), %r15
	movq	144(%rbx), %rdi
	movl	%eax, %r12d
	movq	(%r15), %rax
	movq	72(%rax), %r9
	movq	%r9, -104(%rbp)
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	144(%rbx), %rdi
	movl	%eax, -96(%rbp)
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movl	-96(%rbp), %edx
	movsd	-88(%rbp), %xmm0
	movl	%r14d, %ecx
	movq	-104(%rbp), %r9
	movl	%eax, %esi
	leaq	-64(%rbp), %r8
	movq	%r15, %rdi
	call	*%r9
	testb	%r12b, %r12b
	je	.L760
	movsd	-72(%rbp), %xmm0
	testb	%al, %al
	je	.L761
	movsd	-64(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L761
	comisd	%xmm1, %xmm0
	jbe	.L782
.L766:
	movapd	%xmm1, %xmm0
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransition7setTimeEd@PLT
	movq	144(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransition7setFromERKNS_12TimeZoneRuleE@PLT
	movq	152(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransition5setToERKNS_12TimeZoneRuleE@PLT
	movl	$1, %eax
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L785:
	leaq	-76(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6714SimpleTimeZone19initTransitionRulesER10UErrorCode
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L760:
	movsd	-64(%rbp), %xmm1
	testb	%al, %al
	je	.L753
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L761:
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransition7setTimeEd@PLT
	movq	152(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransition7setFromERKNS_12TimeZoneRuleE@PLT
	movq	144(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransition5setToERKNS_12TimeZoneRuleE@PLT
	movl	$1, %eax
	jmp	.L753
.L784:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3323:
	.size	_ZNK6icu_6714SimpleTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE, .-_ZNK6icu_6714SimpleTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZone16getTimeZoneRulesERPKNS_19InitialTimeZoneRuleEPPKNS_12TimeZoneRuleERiR10UErrorCode
	.type	_ZNK6icu_6714SimpleTimeZone16getTimeZoneRulesERPKNS_19InitialTimeZoneRuleEPPKNS_12TimeZoneRuleERiR10UErrorCode, @function
_ZNK6icu_6714SimpleTimeZone16getTimeZoneRulesERPKNS_19InitialTimeZoneRuleEPPKNS_12TimeZoneRuleERiR10UErrorCode:
.LFB3336:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jle	.L802
	ret
	.p2align 4,,10
	.p2align 3
.L802:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	leaq	_ZZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCodeE5gLock(%rip), %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	call	umtx_lock_67@PLT
	cmpb	$0, 120(%r15)
	je	.L803
.L789:
	leaq	_ZZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCodeE5gLock(%rip), %rdi
	call	umtx_unlock_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L786
	movq	128(%r15), %rax
	movq	144(%r15), %rdx
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L791
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L793
	movq	%rdx, (%r14)
	cmpl	$1, %eax
	je	.L791
	movq	152(%r15), %rax
	movq	%rax, 8(%r14)
	movl	$2, %eax
.L791:
	movl	%eax, (%r12)
.L786:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L803:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6714SimpleTimeZone19initTransitionRulesER10UErrorCode
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L793:
	xorl	%eax, %eax
	jmp	.L791
	.cfi_endproc
.LFE3336:
	.size	_ZNK6icu_6714SimpleTimeZone16getTimeZoneRulesERPKNS_19InitialTimeZoneRuleEPPKNS_12TimeZoneRuleERiR10UErrorCode, .-_ZNK6icu_6714SimpleTimeZone16getTimeZoneRulesERPKNS_19InitialTimeZoneRuleEPPKNS_12TimeZoneRuleERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SimpleTimeZoneeqERKNS_8TimeZoneE
	.type	_ZNK6icu_6714SimpleTimeZoneeqERKNS_8TimeZoneE, @function
_ZNK6icu_6714SimpleTimeZoneeqERKNS_8TimeZoneE:
.LFB3299:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L816
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L806
	cmpb	$42, (%rdi)
	je	.L808
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L806
.L808:
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L806:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_678TimeZoneeqERKS0_@PLT
	testb	%al, %al
	je	.L808
	movq	(%r12), %rax
	leaq	_ZNK6icu_6714SimpleTimeZone12hasSameRulesERKNS_8TimeZoneE(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L809
	call	_ZNK6icu_6714SimpleTimeZone12hasSameRulesERKNS_8TimeZoneE
.L810:
	testb	%al, %al
	popq	%r12
	popq	%r13
	setne	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L816:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L809:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	call	*%rax
	jmp	.L810
	.cfi_endproc
.LFE3299:
	.size	_ZNK6icu_6714SimpleTimeZoneeqERKNS_8TimeZoneE, .-_ZNK6icu_6714SimpleTimeZoneeqERKNS_8TimeZoneE
	.section	.text._ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode,"axG",@progbits,_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode
	.type	_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode, @function
_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode:
.LFB2269:
	.cfi_startproc
	endbr64
	movsbl	%sil, %esi
	jmp	_ZNK6icu_678TimeZone9getOffsetEdaRiS1_R10UErrorCode@PLT
	.cfi_endproc
.LFE2269:
	.size	_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode, .-_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode
	.weak	_ZTSN6icu_6714SimpleTimeZoneE
	.section	.rodata._ZTSN6icu_6714SimpleTimeZoneE,"aG",@progbits,_ZTSN6icu_6714SimpleTimeZoneE,comdat
	.align 16
	.type	_ZTSN6icu_6714SimpleTimeZoneE, @object
	.size	_ZTSN6icu_6714SimpleTimeZoneE, 26
_ZTSN6icu_6714SimpleTimeZoneE:
	.string	"N6icu_6714SimpleTimeZoneE"
	.weak	_ZTIN6icu_6714SimpleTimeZoneE
	.section	.data.rel.ro._ZTIN6icu_6714SimpleTimeZoneE,"awG",@progbits,_ZTIN6icu_6714SimpleTimeZoneE,comdat
	.align 8
	.type	_ZTIN6icu_6714SimpleTimeZoneE, @object
	.size	_ZTIN6icu_6714SimpleTimeZoneE, 24
_ZTIN6icu_6714SimpleTimeZoneE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714SimpleTimeZoneE
	.quad	_ZTIN6icu_6713BasicTimeZoneE
	.weak	_ZTVN6icu_6714SimpleTimeZoneE
	.section	.data.rel.ro._ZTVN6icu_6714SimpleTimeZoneE,"awG",@progbits,_ZTVN6icu_6714SimpleTimeZoneE,comdat
	.align 8
	.type	_ZTVN6icu_6714SimpleTimeZoneE, @object
	.size	_ZTVN6icu_6714SimpleTimeZoneE, 192
_ZTVN6icu_6714SimpleTimeZoneE:
	.quad	0
	.quad	_ZTIN6icu_6714SimpleTimeZoneE
	.quad	_ZN6icu_6714SimpleTimeZoneD1Ev
	.quad	_ZN6icu_6714SimpleTimeZoneD0Ev
	.quad	_ZNK6icu_6714SimpleTimeZone17getDynamicClassIDEv
	.quad	_ZNK6icu_6714SimpleTimeZoneeqERKNS_8TimeZoneE
	.quad	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiR10UErrorCode
	.quad	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiR10UErrorCode
	.quad	_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode
	.quad	_ZN6icu_6714SimpleTimeZone12setRawOffsetEi
	.quad	_ZNK6icu_6714SimpleTimeZone12getRawOffsetEv
	.quad	_ZNK6icu_6714SimpleTimeZone15useDaylightTimeEv
	.quad	_ZNK6icu_6714SimpleTimeZone14inDaylightTimeEdR10UErrorCode
	.quad	_ZNK6icu_6714SimpleTimeZone12hasSameRulesERKNS_8TimeZoneE
	.quad	_ZNK6icu_6714SimpleTimeZone5cloneEv
	.quad	_ZNK6icu_6714SimpleTimeZone13getDSTSavingsEv
	.quad	_ZNK6icu_6714SimpleTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE
	.quad	_ZNK6icu_6714SimpleTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE
	.quad	_ZNK6icu_6713BasicTimeZone24hasEquivalentTransitionsERKS0_ddaR10UErrorCode
	.quad	_ZNK6icu_6714SimpleTimeZone20countTransitionRulesER10UErrorCode
	.quad	_ZNK6icu_6714SimpleTimeZone16getTimeZoneRulesERPKNS_19InitialTimeZoneRuleEPPKNS_12TimeZoneRuleERiR10UErrorCode
	.quad	_ZNK6icu_6713BasicTimeZone18getSimpleRulesNearEdRPNS_19InitialTimeZoneRuleERPNS_18AnnualTimeZoneRuleES6_R10UErrorCode
	.quad	_ZNK6icu_6714SimpleTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode
	.quad	_ZNK6icu_6714SimpleTimeZone9getOffsetEhiiihiiiR10UErrorCode
	.local	_ZZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCodeE5gLock
	.comm	_ZZNK6icu_6714SimpleTimeZone20checkTransitionRulesER10UErrorCodeE5gLock,56,32
	.section	.rodata
	.align 8
	.type	_ZN6icu_67L7STD_STRE, @object
	.size	_ZN6icu_67L7STD_STRE, 12
_ZN6icu_67L7STD_STRE:
	.value	40
	.value	83
	.value	84
	.value	68
	.value	41
	.value	0
	.align 8
	.type	_ZN6icu_67L7DST_STRE, @object
	.size	_ZN6icu_67L7DST_STRE, 12
_ZN6icu_67L7DST_STRE:
	.value	40
	.value	68
	.value	83
	.value	84
	.value	41
	.value	0
	.globl	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE
	.align 8
	.type	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE, @object
	.size	_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE, 12
_ZN6icu_6714SimpleTimeZone17STATICMONTHLENGTHE:
	.ascii	"\037\035\037\036\037\036\037\037\036\037\036\037"
	.local	_ZZN6icu_6714SimpleTimeZone16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6714SimpleTimeZone16getStaticClassIDEvE7classID,1,1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1100257648
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
