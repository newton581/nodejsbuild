	.file	"unesctrn.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722UnescapeTransliterator17getDynamicClassIDEv
	.type	_ZNK6icu_6722UnescapeTransliterator17getDynamicClassIDEv, @function
_ZNK6icu_6722UnescapeTransliterator17getDynamicClassIDEv:
.LFB2346:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6722UnescapeTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2346:
	.size	_ZNK6icu_6722UnescapeTransliterator17getDynamicClassIDEv, .-_ZNK6icu_6722UnescapeTransliterator17getDynamicClassIDEv
	.p2align 4
	.type	_ZN6icu_67L12_createXML10ERKNS_13UnicodeStringENS_14Transliterator5TokenE, @function
_ZN6icu_67L12_createXML10ERKNS_13UnicodeStringENS_14Transliterator5TokenE:
.LFB2352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	leaq	16+_ZTVN6icu_6722UnescapeTransliteratorE(%rip), %rax
	leaq	_ZN6icu_67L10SPEC_XML10E(%rip), %rdx
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L5:
	movl	%eax, %r12d
	addq	$1, %rax
	cmpw	$-1, (%rdx,%rax,2)
	jne	.L5
	addl	$2, %r12d
	movslq	%r12d, %r12
	addq	%r12, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L6
	movq	%r12, %rdx
	leaq	_ZN6icu_67L10SPEC_XML10E(%rip), %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
.L6:
	movq	%rcx, 88(%r13)
.L3:
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2352:
	.size	_ZN6icu_67L12_createXML10ERKNS_13UnicodeStringENS_14Transliterator5TokenE, .-_ZN6icu_67L12_createXML10ERKNS_13UnicodeStringENS_14Transliterator5TokenE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UnescapeTransliteratorD2Ev
	.type	_ZN6icu_6722UnescapeTransliteratorD2Ev, @function
_ZN6icu_6722UnescapeTransliteratorD2Ev:
.LFB2363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722UnescapeTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714TransliteratorD2Ev@PLT
	.cfi_endproc
.LFE2363:
	.size	_ZN6icu_6722UnescapeTransliteratorD2Ev, .-_ZN6icu_6722UnescapeTransliteratorD2Ev
	.globl	_ZN6icu_6722UnescapeTransliteratorD1Ev
	.set	_ZN6icu_6722UnescapeTransliteratorD1Ev,_ZN6icu_6722UnescapeTransliteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UnescapeTransliteratorD0Ev
	.type	_ZN6icu_6722UnescapeTransliteratorD0Ev, @function
_ZN6icu_6722UnescapeTransliteratorD0Ev:
.LFB2365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722UnescapeTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2365:
	.size	_ZN6icu_6722UnescapeTransliteratorD0Ev, .-_ZN6icu_6722UnescapeTransliteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722UnescapeTransliterator5cloneEv
	.type	_ZNK6icu_6722UnescapeTransliterator5cloneEv, @function
_ZNK6icu_6722UnescapeTransliterator5cloneEv:
.LFB2366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$96, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L19
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	movq	88(%r12), %r14
	leaq	16+_ZTVN6icu_6722UnescapeTransliteratorE(%rip), %rax
	movq	%rax, 0(%r13)
	cmpw	$-1, (%r14)
	je	.L24
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L22:
	movl	%eax, %r12d
	addq	$1, %rax
	cmpw	$-1, -2(%r14,%rax,2)
	jne	.L22
	addl	$1, %r12d
	movslq	%r12d, %r12
	addq	%r12, %r12
.L21:
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L23
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
.L23:
	movq	%rcx, 88(%r13)
.L19:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movl	$2, %r12d
	jmp	.L21
	.cfi_endproc
.LFE2366:
	.size	_ZNK6icu_6722UnescapeTransliterator5cloneEv, .-_ZNK6icu_6722UnescapeTransliterator5cloneEv
	.p2align 4
	.type	_ZN6icu_67L14_createUnicodeERKNS_13UnicodeStringENS_14Transliterator5TokenE, @function
_ZN6icu_67L14_createUnicodeERKNS_13UnicodeStringENS_14Transliterator5TokenE:
.LFB2348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L33
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	leaq	16+_ZTVN6icu_6722UnescapeTransliteratorE(%rip), %rax
	leaq	_ZN6icu_67L12SPEC_UnicodeE(%rip), %rdx
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L35:
	movl	%eax, %r12d
	addq	$1, %rax
	cmpw	$-1, (%rdx,%rax,2)
	jne	.L35
	addl	$2, %r12d
	movslq	%r12d, %r12
	addq	%r12, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L36
	movq	%r12, %rdx
	leaq	_ZN6icu_67L12SPEC_UnicodeE(%rip), %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
.L36:
	movq	%rcx, 88(%r13)
.L33:
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2348:
	.size	_ZN6icu_67L14_createUnicodeERKNS_13UnicodeStringENS_14Transliterator5TokenE, .-_ZN6icu_67L14_createUnicodeERKNS_13UnicodeStringENS_14Transliterator5TokenE
	.p2align 4
	.type	_ZN6icu_67L11_createPerlERKNS_13UnicodeStringENS_14Transliterator5TokenE, @function
_ZN6icu_67L11_createPerlERKNS_13UnicodeStringENS_14Transliterator5TokenE:
.LFB2353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L45
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	leaq	16+_ZTVN6icu_6722UnescapeTransliteratorE(%rip), %rax
	leaq	_ZN6icu_67L9SPEC_PerlE(%rip), %rdx
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L47:
	movl	%eax, %r12d
	addq	$1, %rax
	cmpw	$-1, (%rdx,%rax,2)
	jne	.L47
	addl	$2, %r12d
	movslq	%r12d, %r12
	addq	%r12, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L48
	movq	%r12, %rdx
	leaq	_ZN6icu_67L9SPEC_PerlE(%rip), %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
.L48:
	movq	%rcx, 88(%r13)
.L45:
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2353:
	.size	_ZN6icu_67L11_createPerlERKNS_13UnicodeStringENS_14Transliterator5TokenE, .-_ZN6icu_67L11_createPerlERKNS_13UnicodeStringENS_14Transliterator5TokenE
	.p2align 4
	.type	_ZN6icu_67L10_createAnyERKNS_13UnicodeStringENS_14Transliterator5TokenE, @function
_ZN6icu_67L10_createAnyERKNS_13UnicodeStringENS_14Transliterator5TokenE:
.LFB2354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L57
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	leaq	16+_ZTVN6icu_6722UnescapeTransliteratorE(%rip), %rax
	leaq	_ZN6icu_67L8SPEC_AnyE(%rip), %rdx
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L59:
	movl	%eax, %r12d
	addq	$1, %rax
	cmpw	$-1, (%rdx,%rax,2)
	jne	.L59
	addl	$2, %r12d
	movslq	%r12d, %r12
	addq	%r12, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L60
	movq	%r12, %rdx
	leaq	_ZN6icu_67L8SPEC_AnyE(%rip), %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
.L60:
	movq	%rcx, 88(%r13)
.L57:
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2354:
	.size	_ZN6icu_67L10_createAnyERKNS_13UnicodeStringENS_14Transliterator5TokenE, .-_ZN6icu_67L10_createAnyERKNS_13UnicodeStringENS_14Transliterator5TokenE
	.p2align 4
	.type	_ZN6icu_67L11_createJavaERKNS_13UnicodeStringENS_14Transliterator5TokenE, @function
_ZN6icu_67L11_createJavaERKNS_13UnicodeStringENS_14Transliterator5TokenE:
.LFB2349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L69
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	leaq	16+_ZTVN6icu_6722UnescapeTransliteratorE(%rip), %rax
	leaq	_ZN6icu_67L9SPEC_JavaE(%rip), %rdx
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L71:
	movl	%eax, %r12d
	addq	$1, %rax
	cmpw	$-1, (%rdx,%rax,2)
	jne	.L71
	addl	$2, %r12d
	movslq	%r12d, %r12
	addq	%r12, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L72
	movq	%r12, %rdx
	leaq	_ZN6icu_67L9SPEC_JavaE(%rip), %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
.L72:
	movq	%rcx, 88(%r13)
.L69:
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2349:
	.size	_ZN6icu_67L11_createJavaERKNS_13UnicodeStringENS_14Transliterator5TokenE, .-_ZN6icu_67L11_createJavaERKNS_13UnicodeStringENS_14Transliterator5TokenE
	.p2align 4
	.type	_ZN6icu_67L8_createCERKNS_13UnicodeStringENS_14Transliterator5TokenE, @function
_ZN6icu_67L8_createCERKNS_13UnicodeStringENS_14Transliterator5TokenE:
.LFB2350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L81
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	leaq	16+_ZTVN6icu_6722UnescapeTransliteratorE(%rip), %rax
	leaq	_ZN6icu_67L6SPEC_CE(%rip), %rdx
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L83:
	movl	%eax, %r12d
	addq	$1, %rax
	cmpw	$-1, (%rdx,%rax,2)
	jne	.L83
	addl	$2, %r12d
	movslq	%r12d, %r12
	addq	%r12, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L84
	movq	%r12, %rdx
	leaq	_ZN6icu_67L6SPEC_CE(%rip), %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
.L84:
	movq	%rcx, 88(%r13)
.L81:
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2350:
	.size	_ZN6icu_67L8_createCERKNS_13UnicodeStringENS_14Transliterator5TokenE, .-_ZN6icu_67L8_createCERKNS_13UnicodeStringENS_14Transliterator5TokenE
	.p2align 4
	.type	_ZN6icu_67L10_createXMLERKNS_13UnicodeStringENS_14Transliterator5TokenE, @function
_ZN6icu_67L10_createXMLERKNS_13UnicodeStringENS_14Transliterator5TokenE:
.LFB2351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L93
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	leaq	16+_ZTVN6icu_6722UnescapeTransliteratorE(%rip), %rax
	leaq	_ZN6icu_67L8SPEC_XMLE(%rip), %rdx
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L95:
	movl	%eax, %r12d
	addq	$1, %rax
	cmpw	$-1, (%rdx,%rax,2)
	jne	.L95
	addl	$2, %r12d
	movslq	%r12d, %r12
	addq	%r12, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L96
	movq	%r12, %rdx
	leaq	_ZN6icu_67L8SPEC_XMLE(%rip), %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
.L96:
	movq	%rcx, 88(%r13)
.L93:
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2351:
	.size	_ZN6icu_67L10_createXMLERKNS_13UnicodeStringENS_14Transliterator5TokenE, .-_ZN6icu_67L10_createXMLERKNS_13UnicodeStringENS_14Transliterator5TokenE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UnescapeTransliterator16getStaticClassIDEv
	.type	_ZN6icu_6722UnescapeTransliterator16getStaticClassIDEv, @function
_ZN6icu_6722UnescapeTransliterator16getStaticClassIDEv:
.LFB2345:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6722UnescapeTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2345:
	.size	_ZN6icu_6722UnescapeTransliterator16getStaticClassIDEv, .-_ZN6icu_6722UnescapeTransliterator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UnescapeTransliteratorC2ERKNS_13UnicodeStringEPKDs
	.type	_ZN6icu_6722UnescapeTransliteratorC2ERKNS_13UnicodeStringEPKDs, @function
_ZN6icu_6722UnescapeTransliteratorC2ERKNS_13UnicodeStringEPKDs:
.LFB2357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	xorl	%edx, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	leaq	16+_ZTVN6icu_6722UnescapeTransliteratorE(%rip), %rax
	cmpw	$-1, 0(%r13)
	movq	%rax, (%rbx)
	je	.L110
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L108:
	movl	%eax, %r12d
	addq	$1, %rax
	cmpw	$-1, -2(%r13,%rax,2)
	jne	.L108
	addl	$1, %r12d
	movslq	%r12d, %r12
	addq	%r12, %r12
.L107:
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L109
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
.L109:
	movq	%rcx, 88(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movl	$2, %r12d
	jmp	.L107
	.cfi_endproc
.LFE2357:
	.size	_ZN6icu_6722UnescapeTransliteratorC2ERKNS_13UnicodeStringEPKDs, .-_ZN6icu_6722UnescapeTransliteratorC2ERKNS_13UnicodeStringEPKDs
	.globl	_ZN6icu_6722UnescapeTransliteratorC1ERKNS_13UnicodeStringEPKDs
	.set	_ZN6icu_6722UnescapeTransliteratorC1ERKNS_13UnicodeStringEPKDs,_ZN6icu_6722UnescapeTransliteratorC2ERKNS_13UnicodeStringEPKDs
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UnescapeTransliteratorC2ERKS0_
	.type	_ZN6icu_6722UnescapeTransliteratorC2ERKS0_, @function
_ZN6icu_6722UnescapeTransliteratorC2ERKS0_:
.LFB2360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	movq	88(%r12), %r13
	leaq	16+_ZTVN6icu_6722UnescapeTransliteratorE(%rip), %rax
	movq	%rax, (%rbx)
	cmpw	$-1, 0(%r13)
	je	.L120
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L118:
	movl	%eax, %r12d
	addq	$1, %rax
	cmpw	$-1, -2(%r13,%rax,2)
	jne	.L118
	addl	$1, %r12d
	movslq	%r12d, %r12
	addq	%r12, %r12
.L117:
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L119
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
.L119:
	movq	%rcx, 88(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movl	$2, %r12d
	jmp	.L117
	.cfi_endproc
.LFE2360:
	.size	_ZN6icu_6722UnescapeTransliteratorC2ERKS0_, .-_ZN6icu_6722UnescapeTransliteratorC2ERKS0_
	.globl	_ZN6icu_6722UnescapeTransliteratorC1ERKS0_
	.set	_ZN6icu_6722UnescapeTransliteratorC1ERKS0_,_ZN6icu_6722UnescapeTransliteratorC2ERKS0_
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC0:
	.string	"H"
	.string	"e"
	.string	"x"
	.string	"-"
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"/"
	.string	"U"
	.string	"n"
	.string	"i"
	.string	"c"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	""
	.string	""
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC1:
	.string	"H"
	.string	"e"
	.string	"x"
	.string	"-"
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"/"
	.string	"J"
	.string	"a"
	.string	"v"
	.string	"a"
	.string	""
	.string	""
	.align 2
.LC2:
	.string	"H"
	.string	"e"
	.string	"x"
	.string	"-"
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"/"
	.string	"C"
	.string	""
	.string	""
	.align 2
.LC3:
	.string	"H"
	.string	"e"
	.string	"x"
	.string	"-"
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"/"
	.string	"X"
	.string	"M"
	.string	"L"
	.string	""
	.string	""
	.align 2
.LC4:
	.string	"H"
	.string	"e"
	.string	"x"
	.string	"-"
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"/"
	.string	"X"
	.string	"M"
	.string	"L"
	.string	"1"
	.string	"0"
	.string	""
	.string	""
	.align 2
.LC5:
	.string	"H"
	.string	"e"
	.string	"x"
	.string	"-"
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"/"
	.string	"P"
	.string	"e"
	.string	"r"
	.string	"l"
	.string	""
	.string	""
	.align 2
.LC6:
	.string	"H"
	.string	"e"
	.string	"x"
	.string	"-"
	.string	"A"
	.string	"n"
	.string	"y"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UnescapeTransliterator11registerIDsEv
	.type	_ZN6icu_6722UnescapeTransliterator11registerIDsEv, @function
_ZN6icu_6722UnescapeTransliterator11registerIDsEv:
.LFB2355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-120(%rbp), %r14
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-112(%rbp), %r12
	movq	%r14, %rdx
	movq	%r12, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rax
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdx
	leaq	_ZN6icu_67L14_createUnicodeERKNS_13UnicodeStringENS_14Transliterator5TokenE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC1(%rip), %rax
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdx
	leaq	_ZN6icu_67L11_createJavaERKNS_13UnicodeStringENS_14Transliterator5TokenE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC2(%rip), %rax
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdx
	leaq	_ZN6icu_67L8_createCERKNS_13UnicodeStringENS_14Transliterator5TokenE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC3(%rip), %rax
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdx
	leaq	_ZN6icu_67L10_createXMLERKNS_13UnicodeStringENS_14Transliterator5TokenE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC4(%rip), %rax
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdx
	leaq	_ZN6icu_67L12_createXML10ERKNS_13UnicodeStringENS_14Transliterator5TokenE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC5(%rip), %rax
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdx
	leaq	_ZN6icu_67L11_createPerlERKNS_13UnicodeStringENS_14Transliterator5TokenE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC6(%rip), %rax
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdx
	leaq	_ZN6icu_67L10_createAnyERKNS_13UnicodeStringENS_14Transliterator5TokenE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$104, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L129:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2355:
	.size	_ZN6icu_6722UnescapeTransliterator11registerIDsEv, .-_ZN6icu_6722UnescapeTransliterator11registerIDsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722UnescapeTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.type	_ZNK6icu_6722UnescapeTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, @function
_ZNK6icu_6722UnescapeTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona:
.LFB2367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movb	%cl, -161(%rbp)
	movl	12(%rdx), %eax
	movq	%rdx, -192(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movl	8(%rdx), %ebx
	movl	%eax, -132(%rbp)
	movl	%ebx, -144(%rbp)
	cmpl	%eax, %ebx
	jge	.L195
	movq	%rsi, %r15
	movq	%rdi, %r13
.L132:
	movq	88(%r13), %rax
	movzwl	(%rax), %esi
	cmpw	$-1, %si
	je	.L133
	movl	-144(%rbp), %ebx
	movq	%r15, %r14
	leal	1(%rbx), %edi
	addl	$2, %ebx
	movl	%edi, -160(%rbp)
	movl	%ebx, -168(%rbp)
	xorl	%ebx, %ebx
	movl	%ebx, %ecx
	.p2align 4,,10
	.p2align 3
.L160:
	leal	1(%rcx), %edi
	movzwl	%si, %r12d
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %ebx
	leaq	(%rdi,%rdi), %r15
	leal	5(%rcx), %edi
	movzwl	4(%rax,%r15), %edx
	movl	%edi, -140(%rbp)
	movl	%ebx, -136(%rbp)
	movzwl	2(%rax,%r15), %ebx
	movzwl	6(%rax,%r15), %eax
	movw	%dx, -156(%rbp)
	movw	%ax, -152(%rbp)
	testw	%si, %si
	je	.L196
	movq	(%r14), %rax
	movl	-144(%rbp), %esi
	movq	%r14, %rdi
	call	*72(%rax)
	movl	%eax, %r9d
	movq	88(%r13), %rax
	cmpw	%r9w, 8(%rax,%r15)
	jne	.L137
	cmpl	$1, %r12d
	je	.L165
	movl	-160(%rbp), %esi
	cmpl	%esi, -132(%rbp)
	jle	.L140
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*72(%rax)
	movl	%eax, %r9d
	movq	88(%r13), %rax
	cmpw	%r9w, 10(%rax,%r15)
	jne	.L137
	movl	-168(%rbp), %ecx
	cmpl	$2, %r12d
	je	.L134
	cmpl	%ecx, -132(%rbp)
	jle	.L140
	leaq	12(%r15), %rdx
	movw	%bx, -172(%rbp)
	movl	%ecx, %r15d
	movq	%rdx, %rbx
.L136:
	movq	(%r14), %rax
	movl	%r15d, %esi
	movq	%r14, %rdi
	addl	$1, %r15d
	call	*72(%rax)
	movl	%eax, %r8d
	movq	88(%r13), %rax
	cmpw	%r8w, (%rax,%rbx)
	jne	.L137
	movl	%r15d, %edx
	subl	-144(%rbp), %edx
	cmpl	%edx, %r12d
	jle	.L197
	addq	$2, %rbx
	cmpl	%r15d, -132(%rbp)
	jne	.L136
.L140:
	cmpb	$0, -161(%rbp)
	jne	.L154
.L137:
	movl	-140(%rbp), %ecx
	addl	-136(%rbp), %r12d
	addl	%r12d, %ecx
	movslq	%ecx, %rdx
	movzwl	(%rax,%rdx,2), %esi
	cmpw	$-1, %si
	jne	.L160
	movq	%r14, %r15
.L133:
	movl	-144(%rbp), %ebx
	movq	(%r15), %rax
	movq	%r15, %rdi
	movl	%ebx, %esi
	call	*80(%rax)
	cmpl	$65535, %eax
	seta	%al
	movzbl	%al, %eax
	leal	1(%rax,%rbx), %eax
	movl	%eax, -144(%rbp)
	cmpl	-132(%rbp), %eax
	jl	.L132
.L154:
	movq	-192(%rbp), %rbx
	movl	-132(%rbp), %eax
	subl	12(%rbx), %eax
.L131:
	addl	%eax, 4(%rbx)
	movq	%rbx, %rax
	movl	-132(%rbp), %ebx
	movl	%ebx, 12(%rax)
	movl	-144(%rbp), %ebx
	movl	%ebx, 8(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L198
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movl	-144(%rbp), %ecx
.L134:
	movzwl	-152(%rbp), %eax
	xorl	%r15d, %r15d
	xorl	%r9d, %r9d
	movsbl	%bl, %ebx
	movl	%r12d, -176(%rbp)
	movl	%ebx, %r12d
	movq	%r13, -184(%rbp)
	movl	%r15d, %r13d
	movl	%ecx, %r15d
	movq	%r14, %rcx
	movl	%eax, -172(%rbp)
	movl	%r9d, %r14d
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L200:
	addl	$1, %r15d
.L193:
	movl	%r13d, %edx
	addl	$1, %r14d
	imull	%r12d, %edx
	leal	(%rdx,%rax), %r13d
	cmpl	%r14d, -172(%rbp)
	je	.L189
.L148:
	cmpl	%r15d, -132(%rbp)
	jle	.L199
	movq	(%rcx), %rax
	movq	%rcx, %rdi
	movq	%rcx, -152(%rbp)
	movl	%r15d, %esi
	call	*80(%rax)
	movl	%r12d, %esi
	movl	%eax, %edi
	movl	%eax, %ebx
	call	u_digit_67@PLT
	movq	-152(%rbp), %rcx
	testl	%eax, %eax
	js	.L189
	cmpl	$65535, %ebx
	jbe	.L200
	addl	$2, %r15d
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L189:
	movzwl	-156(%rbp), %eax
	movl	%r14d, %r9d
	movl	-176(%rbp), %r12d
	movq	%rcx, %r14
	movl	%r15d, %ecx
	movl	%r13d, %r15d
	movq	-184(%rbp), %r13
	cmpl	%eax, %r9d
	jge	.L149
.L169:
	movq	88(%r13), %rax
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L165:
	movl	-160(%rbp), %ecx
	jmp	.L134
.L199:
	cmpb	$0, -161(%rbp)
	movl	%r14d, %r9d
	movl	-176(%rbp), %r12d
	movq	%rcx, %r14
	movl	%r15d, %ecx
	movl	%r13d, %r15d
	movq	-184(%rbp), %r13
	jne	.L154
	movzwl	-156(%rbp), %eax
	cmpl	%eax, %r9d
	jl	.L169
.L149:
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	je	.L167
	cmpl	%ecx, -132(%rbp)
	jle	.L151
	movl	-140(%rbp), %eax
	movl	%r15d, -172(%rbp)
	movq	%r14, %r15
	movl	%r12d, -156(%rbp)
	leal	(%r12,%rax), %edx
	movl	-136(%rbp), %eax
	movl	%ecx, %r12d
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx), %rbx
	addl	%ecx, %eax
	movq	%rbx, %r14
	movl	%eax, -152(%rbp)
	movl	%eax, %ebx
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L203:
	cmpl	%ebx, %r12d
	je	.L201
	addq	$2, %r14
	cmpl	%r12d, -132(%rbp)
	je	.L202
.L153:
	movq	(%r15), %rax
	movl	%r12d, %esi
	movq	%r15, %rdi
	addl	$1, %r12d
	call	*72(%rax)
	movl	%eax, %r8d
	movq	88(%r13), %rax
	cmpw	%r8w, (%rax,%r14)
	je	.L203
	movl	-156(%rbp), %r12d
	movq	%r15, %r14
	jmp	.L137
.L197:
	movzwl	-172(%rbp), %ebx
	movl	%r15d, %ecx
	jmp	.L134
.L202:
	movl	%r12d, %ecx
	movl	-156(%rbp), %r12d
	movq	%r15, %r14
.L151:
	cmpl	-144(%rbp), %ecx
	jle	.L169
	cmpb	$0, -161(%rbp)
	je	.L169
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L201:
	movl	-172(%rbp), %eax
	movl	%eax, %esi
.L150:
	leaq	-128(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	movq	(%r15), %rax
	movq	%r12, %rcx
	movq	%r15, %rdi
	movl	-152(%rbp), %r14d
	movl	-144(%rbp), %ebx
	movl	%r14d, %edx
	movl	%ebx, %esi
	subl	%ebx, %r14d
	call	*32(%rax)
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L157
	sarl	$5, %eax
.L158:
	movq	%r12, %rdi
	subl	%eax, %r14d
	subl	%r14d, -132(%rbp)
	movl	-132(%rbp), %ebx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	-144(%rbp), %ebx
	jle	.L154
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L157:
	movl	-116(%rbp), %eax
	jmp	.L158
.L167:
	movl	%r15d, %esi
	movl	%ecx, -152(%rbp)
	movq	%r14, %r15
	jmp	.L150
.L195:
	movq	-192(%rbp), %rbx
	xorl	%eax, %eax
	jmp	.L131
.L198:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2367:
	.size	_ZNK6icu_6722UnescapeTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, .-_ZNK6icu_6722UnescapeTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.weak	_ZTSN6icu_6722UnescapeTransliteratorE
	.section	.rodata._ZTSN6icu_6722UnescapeTransliteratorE,"aG",@progbits,_ZTSN6icu_6722UnescapeTransliteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6722UnescapeTransliteratorE, @object
	.size	_ZTSN6icu_6722UnescapeTransliteratorE, 34
_ZTSN6icu_6722UnescapeTransliteratorE:
	.string	"N6icu_6722UnescapeTransliteratorE"
	.weak	_ZTIN6icu_6722UnescapeTransliteratorE
	.section	.data.rel.ro._ZTIN6icu_6722UnescapeTransliteratorE,"awG",@progbits,_ZTIN6icu_6722UnescapeTransliteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6722UnescapeTransliteratorE, @object
	.size	_ZTIN6icu_6722UnescapeTransliteratorE, 24
_ZTIN6icu_6722UnescapeTransliteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722UnescapeTransliteratorE
	.quad	_ZTIN6icu_6714TransliteratorE
	.weak	_ZTVN6icu_6722UnescapeTransliteratorE
	.section	.data.rel.ro._ZTVN6icu_6722UnescapeTransliteratorE,"awG",@progbits,_ZTVN6icu_6722UnescapeTransliteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6722UnescapeTransliteratorE, @object
	.size	_ZTVN6icu_6722UnescapeTransliteratorE, 152
_ZTVN6icu_6722UnescapeTransliteratorE:
	.quad	0
	.quad	_ZTIN6icu_6722UnescapeTransliteratorE
	.quad	_ZN6icu_6722UnescapeTransliteratorD1Ev
	.quad	_ZN6icu_6722UnescapeTransliteratorD0Ev
	.quad	_ZNK6icu_6722UnescapeTransliterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6722UnescapeTransliterator5cloneEv
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.quad	_ZNK6icu_6722UnescapeTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.quad	_ZNK6icu_6714Transliterator5getIDEv
	.quad	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE
	.local	_ZZN6icu_6722UnescapeTransliterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6722UnescapeTransliterator16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.align 32
	.type	_ZN6icu_67L8SPEC_AnyE, @object
	.size	_ZN6icu_67L8SPEC_AnyE, 96
_ZN6icu_67L8SPEC_AnyE:
	.value	2
	.value	0
	.value	16
	.value	4
	.value	6
	.value	85
	.value	43
	.value	2
	.value	0
	.value	16
	.value	4
	.value	4
	.value	92
	.value	117
	.value	2
	.value	0
	.value	16
	.value	8
	.value	8
	.value	92
	.value	85
	.value	3
	.value	1
	.value	16
	.value	1
	.value	6
	.value	38
	.value	35
	.value	120
	.value	59
	.value	2
	.value	1
	.value	10
	.value	1
	.value	7
	.value	38
	.value	35
	.value	59
	.value	3
	.value	1
	.value	16
	.value	1
	.value	6
	.value	92
	.value	120
	.value	123
	.value	125
	.value	-1
	.align 16
	.type	_ZN6icu_67L9SPEC_PerlE, @object
	.size	_ZN6icu_67L9SPEC_PerlE, 20
_ZN6icu_67L9SPEC_PerlE:
	.value	3
	.value	1
	.value	16
	.value	1
	.value	6
	.value	92
	.value	120
	.value	123
	.value	125
	.value	-1
	.align 16
	.type	_ZN6icu_67L10SPEC_XML10E, @object
	.size	_ZN6icu_67L10SPEC_XML10E, 18
_ZN6icu_67L10SPEC_XML10E:
	.value	2
	.value	1
	.value	10
	.value	1
	.value	7
	.value	38
	.value	35
	.value	59
	.value	-1
	.align 16
	.type	_ZN6icu_67L8SPEC_XMLE, @object
	.size	_ZN6icu_67L8SPEC_XMLE, 20
_ZN6icu_67L8SPEC_XMLE:
	.value	3
	.value	1
	.value	16
	.value	1
	.value	6
	.value	38
	.value	35
	.value	120
	.value	59
	.value	-1
	.align 16
	.type	_ZN6icu_67L6SPEC_CE, @object
	.size	_ZN6icu_67L6SPEC_CE, 30
_ZN6icu_67L6SPEC_CE:
	.value	2
	.value	0
	.value	16
	.value	4
	.value	4
	.value	92
	.value	117
	.value	2
	.value	0
	.value	16
	.value	8
	.value	8
	.value	92
	.value	85
	.value	-1
	.align 16
	.type	_ZN6icu_67L9SPEC_JavaE, @object
	.size	_ZN6icu_67L9SPEC_JavaE, 16
_ZN6icu_67L9SPEC_JavaE:
	.value	2
	.value	0
	.value	16
	.value	4
	.value	4
	.value	92
	.value	117
	.value	-1
	.align 16
	.type	_ZN6icu_67L12SPEC_UnicodeE, @object
	.size	_ZN6icu_67L12SPEC_UnicodeE, 16
_ZN6icu_67L12SPEC_UnicodeE:
	.value	2
	.value	0
	.value	16
	.value	4
	.value	6
	.value	85
	.value	43
	.value	-1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
