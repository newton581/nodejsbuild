	.file	"collationkeys.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CollationKeys13LevelCallbackD2Ev
	.type	_ZN6icu_6713CollationKeys13LevelCallbackD2Ev, @function
_ZN6icu_6713CollationKeys13LevelCallbackD2Ev:
.LFB3316:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3316:
	.size	_ZN6icu_6713CollationKeys13LevelCallbackD2Ev, .-_ZN6icu_6713CollationKeys13LevelCallbackD2Ev
	.globl	_ZN6icu_6713CollationKeys13LevelCallbackD1Ev
	.set	_ZN6icu_6713CollationKeys13LevelCallbackD1Ev,_ZN6icu_6713CollationKeys13LevelCallbackD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CollationKeys13LevelCallback11needToWriteENS_9Collation5LevelE
	.type	_ZN6icu_6713CollationKeys13LevelCallback11needToWriteENS_9Collation5LevelE, @function
_ZN6icu_6713CollationKeys13LevelCallback11needToWriteENS_9Collation5LevelE:
.LFB3319:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3319:
	.size	_ZN6icu_6713CollationKeys13LevelCallback11needToWriteENS_9Collation5LevelE, .-_ZN6icu_6713CollationKeys13LevelCallback11needToWriteENS_9Collation5LevelE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CollationKeys13LevelCallbackD0Ev
	.type	_ZN6icu_6713CollationKeys13LevelCallbackD0Ev, @function
_ZN6icu_6713CollationKeys13LevelCallbackD0Ev:
.LFB3318:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3318:
	.size	_ZN6icu_6713CollationKeys13LevelCallbackD0Ev, .-_ZN6icu_6713CollationKeys13LevelCallbackD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715SortKeyByteSink6AppendEPKci
	.type	_ZN6icu_6715SortKeyByteSink6AppendEPKci, @function
_ZN6icu_6715SortKeyByteSink6AppendEPKci:
.LFB3295:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jle	.L5
	testq	%rsi, %rsi
	je	.L5
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jle	.L9
	movl	%eax, %ecx
	subl	%edx, %ecx
	jns	.L13
	movl	$0, 24(%rdi)
	movslq	%eax, %rcx
	subl	%eax, %edx
	addq	%rcx, %rsi
.L9:
	movslq	20(%rdi), %r8
	leal	(%r8,%rdx), %eax
	movq	%r8, %rcx
	addq	8(%rdi), %r8
	movl	%eax, 20(%rdi)
	cmpq	%rsi, %r8
	je	.L5
	movl	16(%rdi), %eax
	subl	%ecx, %eax
	cmpl	%eax, %edx
	jle	.L14
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L5:
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	%ecx, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movslq	%edx, %rdx
	movq	%r8, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3295:
	.size	_ZN6icu_6715SortKeyByteSink6AppendEPKci, .-_ZN6icu_6715SortKeyByteSink6AppendEPKci
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715SortKeyByteSink15GetAppendBufferEiiPciPi
	.type	_ZN6icu_6715SortKeyByteSink15GetAppendBufferEiiPciPi, @function
_ZN6icu_6715SortKeyByteSink15GetAppendBufferEiiPciPi:
.LFB3296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%r9, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L22
	movl	%r8d, %r12d
	cmpl	%r8d, %esi
	jg	.L22
	movl	24(%rdi), %eax
	movq	%rdi, %rbx
	movq	%rcx, %r14
	testl	%eax, %eax
	jg	.L21
	movl	20(%rdi), %r8d
	movl	%edx, %r10d
	movl	16(%rdi), %edx
	subl	%r8d, %edx
	cmpl	%edx, %esi
	jle	.L27
	movq	(%rdi), %rax
	movl	%r8d, %edx
	movl	%r10d, %esi
	call	*48(%rax)
	testb	%al, %al
	jne	.L28
.L21:
	popq	%rbx
	movl	%r12d, 0(%r13)
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	16(%rbx), %eax
	subl	20(%rbx), %eax
	movl	%eax, 0(%r13)
	movslq	20(%rbx), %rax
	addq	8(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	movl	$0, 0(%r13)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	movl	%edx, (%r9)
	popq	%r13
	movslq	20(%rdi), %rax
	popq	%r14
	addq	8(%rdi), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3296:
	.size	_ZN6icu_6715SortKeyByteSink15GetAppendBufferEiiPciPi, .-_ZN6icu_6715SortKeyByteSink15GetAppendBufferEiiPciPi
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj, @function
_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj:
.LFB3310:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movslq	56(%rdi), %r12
	movl	8(%rdi), %eax
	cmpl	%eax, %r12d
	jl	.L30
	cmpb	$0, 60(%rdi)
	je	.L29
	addl	%eax, %eax
	leal	2(%r12), %r13d
	cmpl	%eax, %r13d
	cmovl	%eax, %r13d
	movslq	%r13d, %rdi
	cmpl	$199, %r13d
	jle	.L38
.L32:
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L33
	movq	(%rbx), %r8
	testl	%r12d, %r12d
	jg	.L39
	cmpb	$0, 12(%rbx)
	jne	.L40
.L35:
	movq	%r15, (%rbx)
	movslq	56(%rbx), %r12
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
.L30:
	leal	1(%r12), %eax
	movl	%eax, 56(%rbx)
	movq	(%rbx), %rax
	movb	%r14b, (%rax,%r12)
.L29:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movl	$200, %edi
	movl	$200, %r13d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L39:
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -56(%rbp)
	cmpl	%r13d, %r12d
	cmovg	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	movq	-56(%rbp), %r8
	je	.L35
.L40:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L35
.L33:
	movb	$0, 60(%rbx)
	jmp	.L29
	.cfi_endproc
.LFE3310:
	.size	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj, .-_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel14appendWeight16Ej, @function
_ZN6icu_6712_GLOBAL__N_112SortKeyLevel14appendWeight16Ej:
.LFB3311:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	shrl	$8, %r15d
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movslq	56(%rdi), %r13
	movl	8(%rdi), %eax
	testb	%sil, %sil
	jne	.L58
	leal	1(%r13), %edx
	movl	$2, %r14d
	cmpl	%edx, %eax
	jge	.L59
.L53:
	cmpb	$0, 60(%rbx)
	je	.L41
	addl	%eax, %eax
	addl	%r13d, %r14d
	cmpl	%eax, %r14d
	cmovl	%eax, %r14d
	movslq	%r14d, %rdi
	cmpl	$199, %r14d
	jle	.L60
.L46:
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L47
	movq	(%rbx), %r8
	testl	%r13d, %r13d
	jg	.L61
	cmpb	$0, 12(%rbx)
	jne	.L62
.L49:
	movl	56(%rbx), %eax
	movq	%rcx, (%rbx)
	movl	%r14d, 8(%rbx)
	movslq	%eax, %rdx
	addl	$1, %eax
	movb	$1, 12(%rbx)
	movl	%eax, 56(%rbx)
	movb	%r15b, (%rcx,%rdx)
	testb	%r12b, %r12b
	je	.L41
	leal	1(%rax), %edx
	cltq
	movl	%edx, 56(%rbx)
	movq	(%rbx), %rdx
	movb	%r12b, (%rdx,%rax)
.L63:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	(%rbx), %rax
	movl	%edx, 56(%rbx)
	movb	%r15b, (%rax,%r13)
.L41:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	leal	2(%r13), %edx
	movl	$4, %r14d
	cmpl	%edx, %eax
	jl	.L53
	leal	1(%r13), %eax
	movl	%eax, 56(%rbx)
	movq	(%rbx), %rax
	movb	%r15b, (%rax,%r13)
	movl	56(%rbx), %eax
	leal	1(%rax), %edx
	cltq
	movl	%edx, 56(%rbx)
	movq	(%rbx), %rdx
	movb	%r12b, (%rdx,%rax)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$200, %edi
	movl	$200, %r14d
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L61:
	cmpl	%r13d, 8(%rbx)
	cmovle	8(%rbx), %r13d
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -56(%rbp)
	cmpl	%r14d, %r13d
	cmovg	%r14d, %r13d
	movslq	%r13d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	movq	-56(%rbp), %r8
	movq	%rax, %rcx
	je	.L49
.L62:
	movq	%r8, %rdi
	movq	%rcx, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-56(%rbp), %rcx
	jmp	.L49
.L47:
	movb	$0, 60(%rbx)
	jmp	.L41
	.cfi_endproc
.LFE3311:
	.size	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel14appendWeight16Ej, .-_ZN6icu_6712_GLOBAL__N_112SortKeyLevel14appendWeight16Ej
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3752:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3752:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3755:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L77
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L65
	cmpb	$0, 12(%rbx)
	jne	.L78
.L69:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L65:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L69
	.cfi_endproc
.LFE3755:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3758:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L81
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3758:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3761:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L84
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3761:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L90
.L86:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L91
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3763:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3764:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3764:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3765:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3765:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3766:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3766:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3767:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3767:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3768:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3768:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3769:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L107
	testl	%edx, %edx
	jle	.L107
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L110
.L99:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L99
	.cfi_endproc
.LFE3769:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L114
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L114
	testl	%r12d, %r12d
	jg	.L121
	cmpb	$0, 12(%rbx)
	jne	.L122
.L116:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L116
.L122:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L114:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3770:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L124
	movq	(%rdi), %r8
.L125:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L128
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L128
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L128:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3771:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3772:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L135
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3772:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3773:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3773:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3774:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3774:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3775:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3775:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3777:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3777:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3779:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3779:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715SortKeyByteSinkD2Ev
	.type	_ZN6icu_6715SortKeyByteSinkD2Ev, @function
_ZN6icu_6715SortKeyByteSinkD2Ev:
.LFB3292:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6715SortKeyByteSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678ByteSinkD2Ev@PLT
	.cfi_endproc
.LFE3292:
	.size	_ZN6icu_6715SortKeyByteSinkD2Ev, .-_ZN6icu_6715SortKeyByteSinkD2Ev
	.globl	_ZN6icu_6715SortKeyByteSinkD1Ev
	.set	_ZN6icu_6715SortKeyByteSinkD1Ev,_ZN6icu_6715SortKeyByteSinkD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715SortKeyByteSinkD0Ev
	.type	_ZN6icu_6715SortKeyByteSinkD0Ev, @function
_ZN6icu_6715SortKeyByteSinkD0Ev:
.LFB3294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715SortKeyByteSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3294:
	.size	_ZN6icu_6715SortKeyByteSinkD0Ev, .-_ZN6icu_6715SortKeyByteSinkD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CollationKeys26writeSortKeyUpToQuaternaryERNS_17CollationIteratorEPKaRKNS_17CollationSettingsERNS_15SortKeyByteSinkENS_9Collation5LevelERNS0_13LevelCallbackEaR10UErrorCode
	.type	_ZN6icu_6713CollationKeys26writeSortKeyUpToQuaternaryERNS_17CollationIteratorEPKaRKNS_17CollationSettingsERNS_15SortKeyByteSinkENS_9Collation5LevelERNS0_13LevelCallbackEaR10UErrorCode, @function
_ZN6icu_6713CollationKeys26writeSortKeyUpToQuaternaryERNS_17CollationIteratorEPKaRKNS_17CollationSettingsERNS_15SortKeyByteSinkENS_9Collation5LevelERNS0_13LevelCallbackEaR10UErrorCode:
.LFB3320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$456, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	16(%rbp), %eax
	movq	24(%rbp), %r14
	movq	%rsi, -432(%rbp)
	movq	%rdx, -368(%rbp)
	movq	%rcx, -360(%rbp)
	movl	(%r14), %r11d
	movq	%r9, -456(%rbp)
	movb	%al, -421(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r11d, %r11d
	jg	.L144
	movl	24(%rdx), %esi
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movl	%r8d, %ecx
	movl	%esi, %eax
	sarl	$12, %eax
	movslq	%eax, %rdx
	movl	%eax, -436(%rbp)
	leaq	_ZN6icu_67L10levelMasksE(%rip), %rax
	movl	(%rax,%rdx,4), %edx
	movl	%edx, %eax
	orl	$8, %eax
	testl	$1024, %esi
	cmovne	%eax, %edx
	movl	$-1, %eax
	sall	%cl, %eax
	andl	%edx, %eax
	je	.L144
	movl	$0, -352(%rbp)
	movl	%esi, %ecx
	andl	$12, %ecx
	movl	%ecx, -472(%rbp)
	jne	.L604
.L149:
	movl	%esi, %edx
	movl	$32768, %edi
	movl	$0, %ecx
	movb	$0, -116(%rbp)
	andl	$1536, %edx
	movb	$1, -68(%rbp)
	movq	%r12, %rbx
	cmpl	$512, %edx
	movl	$16191, %edx
	movl	$40, -312(%rbp)
	cmove	%edi, %ecx
	movl	%eax, %edi
	movb	$0, -308(%rbp)
	movl	$0, -264(%rbp)
	movl	%ecx, -464(%rbp)
	movl	$65343, %ecx
	cmove	%ecx, %edx
	andl	$32, %edi
	movb	$1, -260(%rbp)
	xorl	%r15d, %r15d
	movl	%edi, -340(%rbp)
	movl	%eax, %edi
	andl	$2, %edi
	movl	%edx, -440(%rbp)
	leaq	-307(%rbp), %rdx
	movl	%edi, -416(%rbp)
	movl	%eax, %edi
	andl	$4, %edi
	movq	%rdx, -320(%rbp)
	leaq	-243(%rbp), %rdx
	movl	%edi, -408(%rbp)
	movl	%esi, %edi
	andl	$256, %esi
	andl	$2048, %edi
	movq	%rdx, -256(%rbp)
	leaq	-179(%rbp), %rdx
	movl	%edi, -468(%rbp)
	movl	%eax, %edi
	andl	$16, %eax
	movq	%rdx, -192(%rbp)
	andl	$8, %edi
	leaq	-115(%rbp), %rdx
	movl	$40, -248(%rbp)
	movb	$0, -244(%rbp)
	movl	$0, -200(%rbp)
	movb	$1, -196(%rbp)
	movl	$40, -184(%rbp)
	movb	$0, -180(%rbp)
	movl	$0, -136(%rbp)
	movb	$1, -132(%rbp)
	movq	%rdx, -128(%rbp)
	movl	$40, -120(%rbp)
	movl	$0, -72(%rbp)
	movl	%edi, -344(%rbp)
	movl	%esi, -460(%rbp)
	movl	%eax, -404(%rbp)
	movl	$0, -484(%rbp)
	movl	$0, -480(%rbp)
	movl	$0, -420(%rbp)
	movl	$0, -444(%rbp)
	movl	$0, -448(%rbp)
	movl	$0, -412(%rbp)
	.p2align 4,,10
	.p2align 3
.L151:
	movslq	368(%rbx), %rax
	movl	24(%rbx), %edx
	cmpl	%eax, %edx
	je	.L605
	jle	.L154
	leal	1(%rax), %edx
	movl	%edx, 368(%rbx)
	movq	32(%rbx), %rdx
	movq	(%rdx,%rax,8), %r12
.L155:
	movq	%r12, %rax
	sarq	$32, %rax
	movl	%eax, %r8d
	cmpl	%eax, -352(%rbp)
	jbe	.L167
	cmpl	$33554432, %eax
	ja	.L606
.L167:
	cmpl	$1, %r8d
	jbe	.L207
	movl	-416(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L207
	movq	-368(%rbp), %rsi
	movl	%r8d, %r13d
	movq	-432(%rbp), %rdi
	shrl	$24, %r13d
	movq	32(%rsi), %rdx
	movl	%r13d, %eax
	movzbl	(%rdi,%rax), %ecx
	testq	%rdx, %rdx
	je	.L208
	movzbl	(%rdx,%rax), %r13d
	testb	%r13b, %r13b
	je	.L209
	sall	$24, %r13d
	andl	$16777215, %r8d
	orl	%r13d, %r8d
	shrl	$24, %r13d
.L208:
	testb	%cl, %cl
	je	.L210
	movl	-412(%rbp), %eax
	shrl	$24, %eax
	cmpl	%r13d, %eax
	je	.L211
.L210:
	movq	-360(%rbp), %rdi
	movl	-412(%rbp), %esi
	movl	24(%rdi), %eax
	testl	%esi, %esi
	je	.L212
	cmpl	%r8d, %esi
	jbe	.L213
	cmpl	$2, %r13d
	jbe	.L212
	testl	%eax, %eax
	jle	.L214
	subl	$1, %eax
	movl	%eax, 24(%rdi)
	.p2align 4,,10
	.p2align 3
.L212:
	testl	%eax, %eax
	jle	.L220
	movq	-360(%rbp), %rsi
	subl	$1, %eax
	movl	%eax, 24(%rsi)
.L221:
	testb	%cl, %cl
	movl	$0, %eax
	cmovne	%r8d, %eax
	movl	%eax, -412(%rbp)
.L211:
	movl	%r8d, %eax
	shrl	$16, %eax
	testl	$16711680, %r8d
	je	.L224
	movb	%al, -59(%rbp)
	movl	%r8d, %eax
	movl	$1, %edx
	rolw	$8, %ax
	movw	%ax, -58(%rbp)
	movq	-360(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rax
	testl	$65280, %r8d
	je	.L225
	xorl	%edx, %edx
	testb	%r8b, %r8b
	setne	%dl
	addl	$2, %edx
.L225:
	movl	%r8d, -376(%rbp)
	movq	-360(%rbp), %rdi
	leaq	-59(%rbp), %rsi
	call	*%rax
	movl	-376(%rbp), %r8d
.L224:
	cmpb	$0, -421(%rbp)
	jne	.L207
	movq	-360(%rbp), %rax
	movl	20(%rax), %esi
	cmpl	%esi, 16(%rax)
	jl	.L607
	.p2align 4,,10
	.p2align 3
.L207:
	testl	%r12d, %r12d
	je	.L151
	movl	%r12d, %eax
	movl	-408(%rbp), %edi
	shrl	$24, %eax
	movl	%eax, -376(%rbp)
	testl	%edi, %edi
	je	.L231
	movl	%r12d, %r13d
	shrl	$16, %r13d
	jne	.L608
.L231:
	movl	-344(%rbp), %eax
	testl	%eax, %eax
	je	.L256
	movl	-436(%rbp), %eax
	testl	%eax, %eax
	jne	.L257
	testl	%r8d, %r8d
	sete	%al
.L258:
	testb	%al, %al
	jne	.L256
	movl	%r12d, %r13d
	shrl	$8, %r13d
	movl	%r13d, %eax
	andl	$254, %eax
	movl	%eax, -384(%rbp)
	testb	$-64, %r13b
	jne	.L259
	testl	%eax, %eax
	je	.L259
	addl	$1, -448(%rbp)
	.p2align 4,,10
	.p2align 3
.L256:
	movl	-404(%rbp), %edx
	testl	%edx, %edx
	je	.L271
	movl	-440(%rbp), %r13d
	andl	%r12d, %r13d
	cmpl	$1280, %r13d
	je	.L609
	movl	-464(%rbp), %eax
	testl	%eax, %eax
	jne	.L273
	movl	-420(%rbp), %eax
	leaq	-192(%rbp), %rdi
	testl	%eax, %eax
	jne	.L610
.L274:
	leal	49152(%r13), %eax
.L591:
	cmpl	$1280, %r13d
	cmova	%eax, %r13d
	movl	%r13d, %esi
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel14appendWeight16Ej
	movl	$0, -420(%rbp)
.L271:
	movl	-340(%rbp), %r13d
	testl	%r13d, %r13d
	je	.L294
	movzwl	%r12w, %eax
	andl	$192, %r12d
	jne	.L295
	cmpl	$256, %eax
	ja	.L611
.L295:
	cmpl	$256, %eax
	je	.L612
	shrl	$6, %eax
	leaq	-128(%rbp), %rdi
	andl	$3, %eax
	leal	252(%rax), %r13d
	testl	%r15d, %r15d
	jne	.L613
.L298:
	movl	%r13d, %esi
	xorl	%r15d, %r15d
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
.L294:
	cmpl	$1, -376(%rbp)
	jne	.L151
	movl	(%r14), %ebx
	testl	%ebx, %ebx
	jg	.L229
	movl	-408(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L312
	movl	-344(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L398
	movl	-404(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L399
	movl	-340(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L359
	movl	$1, %ebx
	leaq	_ZN6icu_6713CollationKeys13LevelCallback11needToWriteENS_9Collation5LevelE(%rip), %r12
.L315:
	movq	-456(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%r12, %rax
	jne	.L352
.L355:
	movq	-360(%rbp), %rax
	andb	-68(%rbp), %bl
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L614
	movq	-360(%rbp), %rsi
	subl	$1, %eax
	movl	%eax, 24(%rsi)
	movq	%rsi, %rdi
.L356:
	movl	-72(%rbp), %eax
	movq	-128(%rbp), %rsi
	leal	-1(%rax), %edx
	movq	(%rdi), %rax
	call	*16(%rax)
.L351:
	testb	%bl, %bl
	je	.L316
.L359:
	movq	-360(%rbp), %rax
.L602:
	cmpq	$0, 8(%rax)
	je	.L316
.L229:
	cmpb	$0, -116(%rbp)
	jne	.L615
	cmpb	$0, -180(%rbp)
	jne	.L616
.L364:
	cmpb	$0, -244(%rbp)
	jne	.L617
.L365:
	cmpb	$0, -308(%rbp)
	jne	.L618
.L144:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L619
	addq	$456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L605:
	.cfi_restore_state
	movl	$0, 368(%rbx)
	xorl	%edx, %edx
.L153:
	movq	(%rbx), %rax
	addl	$1, %edx
	leaq	-324(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%edx, 24(%rbx)
	movq	%r14, %rdx
	call	*64(%rax)
	movzbl	%al, %edx
	cmpb	$-65, %al
	ja	.L157
	movabsq	$-281474976710656, %rsi
	movq	%rax, %r12
	sall	$16, %eax
	salq	$32, %r12
	movl	%eax, %ecx
	sall	$8, %edx
	andq	%rsi, %r12
	andl	$-16777216, %ecx
	movl	%edx, %eax
	orq	%rcx, %r12
.L587:
	orq	%rax, %r12
.L588:
	movslq	368(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, 368(%rbx)
	movq	32(%rbx), %rdx
	movq	%r12, (%rdx,%rax,8)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L606:
	testl	%r15d, %r15d
	jne	.L620
	movl	%eax, %r15d
	.p2align 4,,10
	.p2align 3
.L203:
	movl	-340(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L202
	movq	-368(%rbp), %rax
	movl	%r15d, %r12d
	shrl	$24, %r12d
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L179
	movzbl	(%rax,%r12), %r12d
	testb	%r12b, %r12b
	je	.L180
	sall	$24, %r12d
	andl	$16777215, %r15d
	orl	%r12d, %r15d
	shrl	$24, %r12d
.L179:
	cmpl	$26, %r12d
	ja	.L621
.L181:
	movl	%r15d, %r9d
	movl	%r15d, %ecx
	movl	%r15d, %r8d
	movslq	-72(%rbp), %rdx
	shrl	$16, %r9d
	shrl	$8, %ecx
	movl	-120(%rbp), %edi
	movl	%r12d, %r13d
	andl	$16711680, %r15d
	je	.L182
	testb	%cl, %cl
	je	.L383
	cmpb	$1, %r8b
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$8, %eax
	cmpb	$1, %r8b
	sbbl	%esi, %esi
	addl	$4, %esi
.L183:
	addl	%edx, %esi
	cmpl	%edi, %esi
	jg	.L370
	leal	1(%rdx), %eax
	movl	%eax, -72(%rbp)
	movq	-128(%rbp), %rax
	movb	%r13b, (%rax,%rdx)
	movl	-72(%rbp), %eax
	movq	-128(%rbp), %r15
.L368:
	leal	1(%rax), %edx
	cltq
	movl	%edx, -72(%rbp)
	movb	%r9b, (%r15,%rax)
	testb	%cl, %cl
	je	.L202
	movslq	-72(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -72(%rbp)
	movq	-128(%rbp), %rdx
	movb	%cl, (%rdx,%rax)
	testb	%r8b, %r8b
	je	.L202
	movslq	-72(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -72(%rbp)
	movq	-128(%rbp), %rdx
	movb	%r8b, (%rdx,%rax)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L623:
	leal	1(%rax), %edx
	movl	%edx, 368(%rbx)
	movq	32(%rbx), %rdx
	movq	(%rdx,%rax,8), %r12
.L189:
	movq	%r12, %rax
	sarq	$32, %rax
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L622
.L202:
	movslq	368(%rbx), %rax
	movl	24(%rbx), %edx
	cmpl	%edx, %eax
	jl	.L623
	cmpl	$39, %edx
	jle	.L190
	leaq	24(%rbx), %rdi
	movq	%r14, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L191
	movl	24(%rbx), %edx
.L190:
	movq	(%rbx), %rax
	addl	$1, %edx
	leaq	-324(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%edx, 24(%rbx)
	movq	%r14, %rdx
	call	*64(%rax)
	movl	%eax, %ecx
	movzbl	%al, %eax
	cmpb	$-65, %cl
	ja	.L192
	movabsq	$-281474976710656, %rdi
	movq	%rcx, %r12
	sall	$16, %ecx
	salq	$32, %r12
	andl	$-16777216, %ecx
	sall	$8, %eax
	andq	%rdi, %r12
	orq	%rcx, %r12
	orq	%rax, %r12
.L590:
	movslq	368(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, 368(%rbx)
	movq	32(%rbx), %rdx
	movq	%r12, (%rdx,%rax,8)
	movq	%r12, %rax
	sarq	$32, %rax
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L202
.L622:
	cmpl	%eax, -352(%rbp)
	jbe	.L406
	cmpl	$33554432, %eax
	ja	.L203
.L406:
	movl	%r15d, %r8d
	xorl	%r15d, %r15d
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L192:
	cmpl	$192, %eax
	je	.L624
	movq	16(%rbx), %rsi
.L200:
	cmpl	$193, %eax
	jne	.L201
	leal	-193(%rcx), %r12d
	salq	$32, %r12
	orq	$83887360, %r12
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L625:
	movslq	368(%rbx), %rax
	movabsq	$4311744768, %rdi
	leal	1(%rax), %edx
	movl	%edx, 368(%rbx)
	movq	32(%rbx), %rdx
	movq	%rdi, (%rdx,%rax,8)
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$1, %r8d
	xorl	%r15d, %r15d
	movabsq	$4311744768, %r12
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L201:
	movl	-324(%rbp), %edx
	movq	%r14, %r8
	movq	%rbx, %rdi
	call	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode@PLT
	movq	%rax, %r12
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L624:
	movl	-324(%rbp), %eax
	testl	%eax, %eax
	js	.L625
	movq	16(%rbx), %rdx
	movq	32(%rdx), %rsi
	movq	(%rsi), %rdi
	movq	16(%rdi), %rcx
	cmpl	$55295, %eax
	jg	.L195
	movl	%eax, %edx
	movq	(%rdi), %rdi
	andl	$31, %eax
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L196:
	movl	(%rcx,%rdx), %ecx
	movzbl	%cl, %eax
	cmpb	$-65, %cl
	ja	.L200
	movabsq	$-281474976710656, %rsi
	movq	%rcx, %rdx
	sall	$16, %ecx
	salq	$32, %rdx
	andl	$-16777216, %ecx
	sall	$8, %eax
	andq	%rsi, %rdx
	movl	%eax, %r12d
	orq	%rdx, %rcx
	orq	%rcx, %r12
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L182:
	leal	1(%rdx), %eax
	cmpl	%edi, %eax
	jle	.L371
	movl	$2, %eax
.L370:
	cmpb	$0, -68(%rbp)
	movl	%ecx, -392(%rbp)
	movl	%r9d, -384(%rbp)
	movb	%r8b, -376(%rbp)
	je	.L202
	leal	(%rdi,%rdi), %r12d
	movl	$200, %esi
	movl	%edx, -400(%rbp)
	cmpl	$200, %r12d
	cmovl	%esi, %r12d
	addl	%edx, %eax
	cmpl	%r12d, %eax
	cmovge	%eax, %r12d
	movslq	%r12d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L185
	movl	-400(%rbp), %edx
	movq	-128(%rbp), %r11
	movzbl	-376(%rbp), %r8d
	movl	-384(%rbp), %r9d
	testl	%edx, %edx
	movl	-392(%rbp), %ecx
	jle	.L186
	cmpl	%edx, -120(%rbp)
	cmovle	-120(%rbp), %edx
	movq	%r11, %rsi
	movq	%rax, %rdi
	movl	%ecx, -400(%rbp)
	cmpl	%edx, %r12d
	movl	%r9d, -392(%rbp)
	cmovle	%r12d, %edx
	movb	%r8b, -384(%rbp)
	movq	%r11, -376(%rbp)
	movslq	%edx, %rdx
	call	memcpy@PLT
	movl	-400(%rbp), %ecx
	movl	-392(%rbp), %r9d
	movzbl	-384(%rbp), %r8d
	movq	-376(%rbp), %r11
.L186:
	cmpb	$0, -116(%rbp)
	jne	.L626
.L187:
	movslq	-72(%rbp), %rdx
	movq	%r15, -128(%rbp)
	movl	%r12d, -120(%rbp)
	leal	1(%rdx), %eax
	movb	$1, -116(%rbp)
	movl	%eax, -72(%rbp)
	movb	%r13b, (%r15,%rdx)
	testb	%r9b, %r9b
	jne	.L368
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L621:
	leaq	-128(%rbp), %rdi
	movl	$27, %esi
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L195:
	cmpl	$65535, %eax
	jg	.L197
	cmpl	$56320, %eax
	movl	$320, %edx
	movq	(%rdi), %r8
	movl	$0, %edi
	cmovl	%edx, %edi
	movl	%eax, %edx
	sarl	$5, %edx
.L589:
	addl	%edi, %edx
	andl	$31, %eax
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L180:
	movq	-368(%rbp), %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_6717CollationSettings9reorderExEj@PLT
	movl	%eax, %r12d
	movl	%eax, %r15d
	shrl	$24, %r12d
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L383:
	movl	$4, %eax
	movl	$2, %esi
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L371:
	movl	%eax, -72(%rbp)
	movq	-128(%rbp), %rax
	movb	%r12b, (%rax,%rdx)
	jmp	.L202
.L154:
	cmpl	$39, %edx
	jle	.L153
	leaq	24(%rbx), %rdi
	movq	%r14, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L156
	movl	24(%rbx), %edx
	jmp	.L153
.L197:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L196
	cmpl	44(%rdi), %eax
	jl	.L199
	movslq	48(%rdi), %rdx
	salq	$2, %rdx
	jmp	.L196
.L257:
	cmpl	$65535, %r12d
	setbe	%al
	jmp	.L258
.L626:
	movq	%r11, %rdi
	movl	%ecx, -392(%rbp)
	movl	%r9d, -384(%rbp)
	movb	%r8b, -376(%rbp)
	call	uprv_free_67@PLT
	movl	-392(%rbp), %ecx
	movl	-384(%rbp), %r9d
	movzbl	-376(%rbp), %r8d
	jmp	.L187
.L157:
	cmpl	$192, %edx
	je	.L627
	movq	16(%rbx), %rsi
.L165:
	cmpl	$193, %edx
	jne	.L166
	leal	-193(%rax), %r12d
	salq	$32, %r12
	orq	$83887360, %r12
	jmp	.L588
.L259:
	movl	-460(%rbp), %eax
	movzbl	%r13b, %r13d
	testl	%eax, %eax
	jne	.L260
	movl	-448(%rbp), %eax
	leaq	-320(%rbp), %rdi
	testl	%eax, %eax
	je	.L261
	movl	-384(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L262
	movl	-264(%rbp), %r10d
	leaq	-320(%rbp), %rdi
	testl	%r10d, %r10d
	je	.L264
	movl	-448(%rbp), %eax
	leaq	-320(%rbp), %rdi
	subl	$1, %eax
	cmpl	$6, %eax
	jg	.L373
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L608:
	cmpl	$1280, %r13d
	je	.L628
	movl	-468(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L235
.L631:
	movl	-444(%rbp), %edx
	leaq	-256(%rbp), %rdi
	testl	%edx, %edx
	jne	.L629
.L236:
	movl	%r13d, %esi
	movl	%r8d, -384(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel14appendWeight16Ej
	movl	-384(%rbp), %r8d
	movl	$0, -444(%rbp)
	jmp	.L231
.L185:
	movb	$0, -68(%rbp)
	jmp	.L202
.L609:
	addl	$1, -420(%rbp)
	jmp	.L271
.L273:
	movl	-460(%rbp), %eax
	testl	%eax, %eax
	jne	.L280
	movl	-420(%rbp), %eax
	leaq	-192(%rbp), %rdi
	testl	%eax, %eax
	jne	.L630
.L281:
	leal	16384(%r13), %eax
	jmp	.L591
.L220:
	movq	-360(%rbp), %rax
	movslq	20(%rax), %rdx
	movq	%rax, %rdi
	cmpl	16(%rax), %edx
	jl	.L222
	movq	(%rax), %rax
	movb	%cl, -384(%rbp)
	movl	$1, %esi
	movl	%r8d, -376(%rbp)
	call	*48(%rax)
	movl	-376(%rbp), %r8d
	movzbl	-384(%rbp), %ecx
	testb	%al, %al
	je	.L223
	movq	-360(%rbp), %rax
	movslq	20(%rax), %rdx
.L222:
	movq	-360(%rbp), %rax
	movq	8(%rax), %rax
	movb	%r13b, (%rax,%rdx)
.L223:
	movq	-360(%rbp), %rax
	addl	$1, 20(%rax)
	jmp	.L221
.L637:
	movslq	368(%rbx), %rax
	movabsq	$4311744768, %rsi
	leal	1(%rax), %edx
	movl	%edx, 368(%rbx)
	movq	32(%rbx), %rdx
	movq	%rsi, (%rdx,%rax,8)
.L156:
	movl	-408(%rbp), %edx
	movl	$256, %r13d
	movl	$1, -376(%rbp)
	movl	$1, %r8d
	movl	$16777472, %r12d
	testl	%edx, %edx
	je	.L231
	movl	-468(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L631
.L235:
	movl	-444(%rbp), %eax
	testl	%eax, %eax
	jne	.L376
.L241:
	leal	-1(%r8), %eax
	movslq	-200(%rbp), %rdx
	cmpl	$33554431, %eax
	ja	.L245
.L375:
	subl	$1, %edx
	cmpl	%edx, -484(%rbp)
	jl	.L246
.L248:
	cmpl	$1, %r8d
	leaq	-256(%rbp), %rdi
	movl	%r8d, -384(%rbp)
	setne	%sil
	movzbl	%sil, %esi
	addl	$1, %esi
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	movl	-200(%rbp), %eax
	movl	$0, -480(%rbp)
	movl	-384(%rbp), %r8d
	movl	%eax, -484(%rbp)
	jmp	.L231
.L612:
	movl	-472(%rbp), %r12d
	testl	%r12d, %r12d
	je	.L297
	movl	$1, %r13d
	leaq	-128(%rbp), %rdi
	testl	%r15d, %r15d
	je	.L298
	subl	$1, %r15d
	cmpl	$112, %r15d
	jg	.L592
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L611:
	addl	$1, %r15d
	jmp	.L294
.L209:
	movq	%rsi, %rdi
	movl	%r8d, %esi
	movb	%cl, -376(%rbp)
	call	_ZNK6icu_6717CollationSettings9reorderExEj@PLT
	movzbl	-376(%rbp), %ecx
	movl	%eax, %r13d
	movl	%eax, %r8d
	shrl	$24, %r13d
	jmp	.L208
.L620:
	subl	$1, %r15d
	cmpl	$112, %r15d
	jle	.L169
	movl	%eax, -376(%rbp)
	movq	%rbx, -384(%rbp)
	movq	%r14, -392(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L633:
	movq	-128(%rbp), %rbx
.L171:
	leal	1(%r12), %eax
	movl	%eax, -72(%rbp)
	movb	$-116, (%rbx,%r12)
.L172:
	subl	$113, %r15d
	cmpl	$112, %r15d
	jle	.L632
.L176:
	movslq	-72(%rbp), %r12
	movl	-120(%rbp), %eax
	cmpl	%eax, %r12d
	jl	.L633
	cmpb	$0, -68(%rbp)
	je	.L172
	leal	2(%r12), %edx
	addl	%eax, %eax
	movl	$200, %r13d
	cmpl	%eax, %edx
	cmovge	%edx, %eax
	cmpl	$200, %eax
	cmovge	%eax, %r13d
	movslq	%r13d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L173
	movq	-128(%rbp), %r14
	testl	%r12d, %r12d
	jle	.L174
	cmpl	%r12d, -120(%rbp)
	cmovle	-120(%rbp), %r12d
	movq	%r14, %rsi
	movq	%rax, %rdi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
.L174:
	cmpb	$0, -116(%rbp)
	jne	.L634
.L175:
	movq	%rbx, -128(%rbp)
	movslq	-72(%rbp), %r12
	movl	%r13d, -120(%rbp)
	movb	$1, -116(%rbp)
	jmp	.L171
.L632:
	movl	-376(%rbp), %r8d
	movq	-384(%rbp), %rbx
	movq	-392(%rbp), %r14
.L169:
	leal	28(%r15), %esi
	leaq	-128(%rbp), %rdi
	movl	%r8d, -376(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	movl	-376(%rbp), %r8d
	movl	%r8d, %r15d
	jmp	.L203
.L634:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L175
.L260:
	movl	-448(%rbp), %esi
	leaq	-320(%rbp), %rdi
	testl	%esi, %esi
	jne	.L635
.L268:
	movl	$0, -448(%rbp)
	movl	-384(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L264
	movl	%r13d, %eax
	movl	$3, %r13d
	shrl	$6, %eax
	subl	%eax, %r13d
	sall	$4, %r13d
.L264:
	movl	%r13d, %esi
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	jmp	.L256
.L213:
	testl	%eax, %eax
	jle	.L217
	subl	$1, %eax
	movl	%eax, 24(%rdi)
	jmp	.L212
.L173:
	movb	$0, -68(%rbp)
	jmp	.L172
.L628:
	movl	-468(%rbp), %esi
	testl	%esi, %esi
	je	.L407
	cmpl	$33554432, %r8d
	je	.L233
.L407:
	addl	$1, -444(%rbp)
	jmp	.L231
.L280:
	cmpl	$256, %r13d
	jbe	.L287
	cmpl	$65535, %r12d
	jbe	.L288
	xorl	$49152, %r13d
	leal	-16384(%r13), %eax
	cmpl	$50432, %r13d
	cmovb	%eax, %r13d
.L287:
	movl	-420(%rbp), %eax
	leaq	-192(%rbp), %rdi
	testl	%eax, %eax
	jne	.L636
.L289:
	movl	%r13d, %esi
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel14appendWeight16Ej
	movl	$0, -420(%rbp)
	jmp	.L271
.L166:
	movl	-324(%rbp), %edx
	movq	%r14, %r8
	movl	%eax, %ecx
	movq	%rbx, %rdi
	call	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode@PLT
	movq	%rax, %r12
	jmp	.L155
.L627:
	movl	-324(%rbp), %eax
	testl	%eax, %eax
	js	.L637
	movq	16(%rbx), %rdx
	movq	32(%rdx), %rsi
	movq	(%rsi), %rdi
	movq	16(%rdi), %rcx
	cmpl	$55295, %eax
	jle	.L638
	cmpl	$65535, %eax
	jg	.L162
	cmpl	$56320, %eax
	movq	(%rdi), %r8
	movl	$320, %edx
	movl	$0, %edi
	cmovl	%edx, %edi
	movl	%eax, %edx
	andl	$31, %eax
	sarl	$5, %edx
	addl	%edi, %edx
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L161:
	movl	(%rcx,%rdx), %eax
	movzbl	%al, %edx
	cmpb	$-65, %al
	ja	.L165
	movabsq	$-281474976710656, %rdi
	movq	%rax, %rsi
	sall	$16, %eax
	salq	$32, %rsi
	andl	$-16777216, %eax
	sall	$8, %edx
	andq	%rdi, %rsi
	movl	%edx, %r12d
	orq	%rsi, %rax
	jmp	.L587
.L262:
	movl	-448(%rbp), %eax
	leaq	-320(%rbp), %rdi
	subl	$1, %eax
	cmpl	$6, %eax
	jle	.L266
.L373:
	leaq	-320(%rbp), %rdi
	movl	%r12d, -392(%rbp)
	movq	%rbx, -400(%rbp)
	movq	%rdi, %r12
	movl	%eax, %ebx
.L265:
	movl	$112, %esi
	movq	%r12, %rdi
	subl	$7, %ebx
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	cmpl	$6, %ebx
	jg	.L265
	movl	-384(%rbp), %r9d
	movl	%ebx, %eax
	movq	%r12, %rdi
	movq	-400(%rbp), %rbx
	movl	-392(%rbp), %r12d
	testl	%r9d, %r9d
	jne	.L266
.L372:
	addl	$1, %eax
.L267:
	sall	$4, %eax
	movq	%rdi, -392(%rbp)
	movl	%eax, %esi
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	movq	-392(%rbp), %rdi
.L261:
	movl	-384(%rbp), %r8d
	movl	$0, -448(%rbp)
	testl	%r8d, %r8d
	je	.L264
	shrl	$6, %r13d
	addl	$13, %r13d
	sall	$4, %r13d
	jmp	.L264
.L613:
	subl	$1, %r15d
	cmpl	$112, %r15d
	jle	.L302
.L592:
	movl	%r13d, -384(%rbp)
	movslq	-72(%rbp), %r12
	movq	%rbx, -392(%rbp)
	movq	%r14, -400(%rbp)
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L639:
	movq	-128(%rbp), %rbx
.L304:
	leal	1(%r12), %eax
	movl	%eax, -72(%rbp)
	movb	$-116, (%rbx,%r12)
.L305:
	subl	$113, %r15d
	cmpl	$112, %r15d
	jle	.L309
	movslq	-72(%rbp), %r12
.L310:
	movl	-120(%rbp), %eax
	cmpl	%r12d, %eax
	jg	.L639
	cmpb	$0, -68(%rbp)
	je	.L305
	addl	%eax, %eax
	movl	$200, %esi
	leal	2(%r12), %r8d
	cmpl	$200, %eax
	cmovl	%esi, %eax
	cmpl	%eax, %r8d
	cmovge	%r8d, %eax
	movslq	%eax, %rdi
	movq	%rdi, %r13
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L306
	movq	-128(%rbp), %r14
	testl	%r12d, %r12d
	jle	.L307
	cmpl	%r12d, -120(%rbp)
	cmovle	-120(%rbp), %r12d
	movq	%r14, %rsi
	movq	%rax, %rdi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
.L307:
	cmpb	$0, -116(%rbp)
	jne	.L640
.L308:
	movq	%rbx, -128(%rbp)
	movslq	-72(%rbp), %r12
	movl	%r13d, -120(%rbp)
	movb	$1, -116(%rbp)
	jmp	.L304
.L309:
	movl	-384(%rbp), %r13d
	movq	-392(%rbp), %rbx
	movq	-400(%rbp), %r14
	cmpl	$27, %r13d
	ja	.L302
.L299:
	leal	28(%r15), %esi
.L311:
	leaq	-128(%rbp), %rdi
	movq	%rdi, -384(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	movq	-384(%rbp), %rdi
	jmp	.L298
.L640:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L308
.L306:
	movb	$0, -68(%rbp)
	jmp	.L305
.L199:
	movl	%eax, %edx
	movq	(%rdi), %r8
	movl	%eax, %edi
	sarl	$11, %edx
	sarl	$5, %edi
	addl	$2080, %edx
	andl	$63, %edi
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	jmp	.L589
.L297:
	movslq	-72(%rbp), %r12
	testl	%r12d, %r12d
	je	.L301
	movl	$1, %r13d
	leaq	-128(%rbp), %rdi
	testl	%r15d, %r15d
	je	.L298
	subl	$1, %r15d
	cmpl	$112, %r15d
	jle	.L299
	movl	%r13d, -384(%rbp)
	movq	%rbx, -392(%rbp)
	movq	%r14, -400(%rbp)
	jmp	.L310
.L302:
	movl	$252, %esi
	subl	%r15d, %esi
	jmp	.L311
.L610:
	movl	-420(%rbp), %eax
	subl	$1, %eax
	cmpl	$96, %eax
	jle	.L275
	movl	%r15d, -384(%rbp)
	movq	%rdi, %r15
	movq	%rbx, -392(%rbp)
	movl	%eax, %ebx
	.p2align 4,,10
	.p2align 3
.L276:
	movl	$101, %esi
	movq	%r15, %rdi
	subl	$97, %ebx
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	cmpl	$96, %ebx
	jg	.L276
	movl	%ebx, %eax
	movq	%r15, %rdi
	movq	-392(%rbp), %rbx
	movl	-384(%rbp), %r15d
.L275:
	movl	$197, %esi
	leal	5(%rax), %edx
	movq	%rdi, -384(%rbp)
	subl	%eax, %esi
	cmpl	$1279, %r13d
	cmovbe	%edx, %esi
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	movq	-384(%rbp), %rdi
	jmp	.L274
.L217:
	movslq	20(%rdi), %rdx
	cmpl	16(%rdi), %edx
	jge	.L641
.L218:
	movq	-360(%rbp), %rax
	movq	8(%rax), %rax
	movb	$-1, (%rax,%rdx)
.L219:
	movq	-360(%rbp), %rax
	addl	$1, 20(%rax)
	movl	24(%rax), %eax
	jmp	.L212
.L604:
	movl	28(%rdi), %edi
	addl	$1, %edi
	movl	%edi, -352(%rbp)
	jmp	.L149
.L618:
	movq	-320(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L144
.L638:
	movl	%eax, %edx
	movq	(%rdi), %rdi
	sarl	$5, %edx
	movslq	%edx, %rdx
.L586:
	movzwl	(%rdi,%rdx,2), %edx
	andl	$31, %eax
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L161
.L641:
	movq	(%rdi), %rax
	movb	%cl, -384(%rbp)
	movl	$1, %esi
	movl	%r8d, -376(%rbp)
	call	*48(%rax)
	movl	-376(%rbp), %r8d
	movzbl	-384(%rbp), %ecx
	testb	%al, %al
	je	.L219
	movq	-360(%rbp), %rax
	movslq	20(%rax), %rdx
	jmp	.L218
.L214:
	movslq	20(%rdi), %rdx
	cmpl	16(%rdi), %edx
	jl	.L215
	movq	(%rdi), %rax
	movb	%cl, -384(%rbp)
	movl	$1, %esi
	movl	%r8d, -376(%rbp)
	call	*48(%rax)
	movl	-376(%rbp), %r8d
	movzbl	-384(%rbp), %ecx
	testb	%al, %al
	je	.L219
	movq	-360(%rbp), %rax
	movslq	20(%rax), %rdx
.L215:
	movq	-360(%rbp), %rax
	movq	8(%rax), %rax
	movb	$3, (%rax,%rdx)
	jmp	.L219
.L288:
	addl	$16384, %r13d
	jmp	.L287
.L607:
	movl	(%r14), %r8d
	testl	%r8d, %r8d
	jg	.L229
	jmp	.L602
.L233:
	movl	-444(%rbp), %eax
	testl	%eax, %eax
	jne	.L642
	movl	-200(%rbp), %edx
	movl	$33554432, %r8d
	jmp	.L375
.L316:
	movl	$7, (%r14)
	jmp	.L229
.L245:
	movzbl	-376(%rbp), %eax
	movb	%r13b, -400(%rbp)
	movb	%al, -392(%rbp)
	movl	-248(%rbp), %eax
	testb	%r13b, %r13b
	jne	.L643
	movl	$2, %esi
	cmpl	%eax, %edx
	jl	.L644
.L366:
	cmpb	$0, -196(%rbp)
	movl	%r13d, -480(%rbp)
	je	.L231
	addl	%eax, %eax
	movl	$200, %edi
	movl	%r8d, -488(%rbp)
	cmpl	$200, %eax
	movl	%edx, -496(%rbp)
	cmovl	%edi, %eax
	addl	%edx, %esi
	cmpl	%eax, %esi
	cmovge	%esi, %eax
	movslq	%eax, %rdi
	movl	%eax, -384(%rbp)
	call	uprv_malloc_67@PLT
	movl	-488(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L252
	movl	-496(%rbp), %edx
	movq	-256(%rbp), %r11
	testl	%edx, %edx
	jg	.L645
.L253:
	cmpb	$0, -244(%rbp)
	jne	.L646
.L254:
	movl	-384(%rbp), %eax
	cmpb	$0, -400(%rbp)
	movq	%r10, -256(%rbp)
	movb	$1, -244(%rbp)
	movslq	-200(%rbp), %rdx
	movl	%eax, -248(%rbp)
	jne	.L255
.L367:
	leal	1(%rdx), %eax
	movl	%r13d, -480(%rbp)
	movl	%eax, -200(%rbp)
	movzbl	-392(%rbp), %eax
	movb	%al, (%r10,%rdx)
	jmp	.L231
.L643:
	leal	2(%rdx), %esi
	cmpl	%eax, %esi
	jg	.L403
	movq	-256(%rbp), %r10
.L255:
	movb	%r13b, (%r10,%rdx)
	movl	-200(%rbp), %eax
	movq	-256(%rbp), %rdx
	movzbl	-392(%rbp), %esi
	movl	%r13d, -480(%rbp)
	addl	$1, %eax
	cltq
	movb	%sil, (%rdx,%rax)
	addl	$2, -200(%rbp)
	jmp	.L231
.L635:
	movl	-448(%rbp), %eax
	subl	$1, %eax
	cmpl	$12, %eax
	jle	.L269
	movl	%r12d, -392(%rbp)
	movq	%rdi, %r12
	movq	%rbx, -400(%rbp)
	movl	%eax, %ebx
.L270:
	movl	$48, %esi
	movq	%r12, %rdi
	subl	$13, %ebx
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	cmpl	$12, %ebx
	jg	.L270
	movl	%ebx, %eax
	movq	%r12, %rdi
	movq	-400(%rbp), %rbx
	movl	-392(%rbp), %r12d
.L269:
	addl	$3, %eax
	movq	%rdi, -392(%rbp)
	sall	$4, %eax
	movl	%eax, %esi
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	movq	-392(%rbp), %rdi
	jmp	.L268
.L636:
	movl	-420(%rbp), %eax
	subl	$1, %eax
	cmpl	$32, %eax
	jle	.L290
	movl	%r12d, -384(%rbp)
	movq	%rdi, %r12
	movq	%rbx, -392(%rbp)
	movl	%eax, %ebx
.L291:
	movl	$165, %esi
	movq	%r12, %rdi
	subl	$33, %ebx
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	cmpl	$32, %ebx
	jg	.L291
	movl	%ebx, %eax
	movq	%r12, %rdi
	movq	-392(%rbp), %rbx
	movl	-384(%rbp), %r12d
.L290:
	movl	$197, %esi
	leal	133(%rax), %edx
	movq	%rdi, -384(%rbp)
	subl	%eax, %esi
	cmpl	$34047, %r13d
	cmovbe	%edx, %esi
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	movq	-384(%rbp), %rdi
	jmp	.L289
.L642:
	movl	$33554432, %r8d
.L376:
	movl	-444(%rbp), %eax
	movl	%r8d, -400(%rbp)
	subl	$1, %eax
	movslq	%eax, %rdx
	movl	%eax, %esi
	movl	%eax, -392(%rbp)
	imulq	$1041204193, %rdx, %rdx
	sarl	$31, %esi
	sarq	$35, %rdx
	subl	%esi, %edx
	movl	%edx, %esi
	sall	$5, %esi
	addl	%esi, %edx
	movl	%eax, %esi
	subl	%edx, %esi
	movl	%esi, %edx
	leal	5(%rsi), %edi
	movl	$69, %esi
	subl	%edx, %esi
	movl	%edx, -384(%rbp)
	cmpl	$1279, -480(%rbp)
	cmovbe	%edi, %esi
	leaq	-256(%rbp), %rdi
	movq	%rdi, -480(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	movl	-392(%rbp), %eax
	movl	-384(%rbp), %edx
	movl	-400(%rbp), %r8d
	subl	%edx, %eax
	testl	%eax, %eax
	movl	%eax, -444(%rbp)
	jle	.L241
	movq	-480(%rbp), %rdi
	movl	%r8d, -384(%rbp)
	movl	%r12d, -392(%rbp)
	movq	%rbx, -400(%rbp)
	movq	%rdi, %r12
	movl	%eax, %ebx
.L244:
	movl	$37, %esi
	movq	%r12, %rdi
	subl	$33, %ebx
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	testl	%ebx, %ebx
	jg	.L244
	movl	%ebx, -444(%rbp)
	movl	-384(%rbp), %r8d
	movl	-392(%rbp), %r12d
	movq	-400(%rbp), %rbx
	jmp	.L241
.L630:
	movl	-420(%rbp), %eax
	subl	$1, %eax
	cmpl	$32, %eax
	jle	.L282
	movl	%r12d, -384(%rbp)
	movq	%rdi, %r12
	movq	%rbx, -392(%rbp)
	movl	%eax, %ebx
.L283:
	movl	$37, %esi
	movq	%r12, %rdi
	subl	$33, %ebx
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	cmpl	$32, %ebx
	jg	.L283
	movl	%ebx, %eax
	movq	%r12, %rdi
	movq	-392(%rbp), %rbx
	movl	-384(%rbp), %r12d
.L282:
	movl	$69, %esi
	leal	5(%rax), %edx
	movq	%rdi, -384(%rbp)
	subl	%eax, %esi
	cmpl	$1279, %r13d
	cmovbe	%edx, %esi
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	movq	-384(%rbp), %rdi
	jmp	.L281
.L629:
	movl	-444(%rbp), %eax
	subl	$1, %eax
	cmpl	$32, %eax
	jle	.L237
	movl	%r8d, -384(%rbp)
	movl	%r12d, -392(%rbp)
	movq	%rdi, %r12
	movq	%rbx, -400(%rbp)
	movl	%eax, %ebx
.L238:
	movl	$37, %esi
	movq	%r12, %rdi
	subl	$33, %ebx
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	cmpl	$32, %ebx
	jg	.L238
	movl	%ebx, %eax
	movq	%r12, %rdi
	movl	-384(%rbp), %r8d
	movq	-400(%rbp), %rbx
	movl	-392(%rbp), %r12d
.L237:
	movl	$69, %esi
	leal	5(%rax), %edx
	movl	%r8d, -392(%rbp)
	subl	%eax, %esi
	cmpl	$1279, %r13d
	movq	%rdi, -384(%rbp)
	cmovbe	%edx, %esi
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	movl	-392(%rbp), %r8d
	movq	-384(%rbp), %rdi
	jmp	.L236
.L615:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -180(%rbp)
	je	.L364
.L616:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -244(%rbp)
	je	.L365
.L617:
	movq	-256(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L365
.L301:
	leaq	-128(%rbp), %rdi
	movl	$1, %esi
	call	_ZN6icu_6712_GLOBAL__N_112SortKeyLevel10appendByteEj
	jmp	.L294
.L246:
	movq	-256(%rbp), %rax
	movslq	-484(%rbp), %rsi
	movslq	%edx, %rdx
	addq	%rax, %rsi
	addq	%rdx, %rax
.L249:
	movzbl	(%rsi), %edx
	movzbl	(%rax), %ecx
	addq	$1, %rsi
	subq	$1, %rax
	movb	%cl, -1(%rsi)
	movb	%dl, 1(%rax)
	cmpq	%rax, %rsi
	jb	.L249
	jmp	.L248
.L312:
	movq	-456(%rbp), %rax
	leaq	_ZN6icu_6713CollationKeys13LevelCallback11needToWriteENS_9Collation5LevelE(%rip), %r12
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%r12, %rax
	jne	.L318
.L321:
	movq	-360(%rbp), %rax
	movzbl	-196(%rbp), %ebx
	movl	24(%rax), %eax
	andl	$1, %ebx
	testl	%eax, %eax
	jle	.L647
	movq	-360(%rbp), %rsi
	subl	$1, %eax
	movl	%eax, 24(%rsi)
	movq	%rsi, %rdi
.L322:
	movl	-200(%rbp), %eax
	movq	-256(%rbp), %rsi
	leal	-1(%rax), %edx
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	-344(%rbp), %edi
	testl	%edi, %edi
	je	.L325
.L313:
	movq	-456(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%r12, %rax
	jne	.L326
.L329:
	movq	-360(%rbp), %rax
	andb	-260(%rbp), %bl
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L648
	movq	-360(%rbp), %rsi
	subl	$1, %eax
	movl	%eax, 24(%rsi)
.L330:
	movl	-264(%rbp), %eax
	leal	-1(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L325
	movq	-320(%rbp), %rax
	xorl	%r8d, %r8d
	movq	-360(%rbp), %r15
	movq	%r14, -352(%rbp)
	movq	%r12, -368(%rbp)
	movl	%ebx, %r14d
	movl	%r8d, %r12d
	movzbl	(%rax), %r13d
	movzbl	%r13b, %r9d
	movl	%ecx, %r13d
.L374:
	leal	1(%r12), %edx
	cmpl	%edx, %r13d
	jle	.L649
.L339:
	movslq	%edx, %rsi
	movzbl	(%rax,%rsi), %ebx
	testb	%r9b, %r9b
	je	.L401
	movl	24(%r15), %edx
	addl	$2, %r12d
	testl	%edx, %edx
	jle	.L335
	subl	$1, %edx
	movl	%edx, 24(%r15)
	cmpl	%r12d, %r13d
	jle	.L584
	movslq	%r12d, %rdx
	movzbl	(%rax,%rdx), %r9d
	leal	1(%r12), %edx
	cmpl	%edx, %r13d
	jg	.L339
.L649:
	movl	%r14d, %ebx
	movq	-368(%rbp), %r12
	movl	%r9d, %r13d
	movq	-352(%rbp), %r14
	testb	%r9b, %r9b
	je	.L325
	movq	-360(%rbp), %rsi
	movl	24(%rsi), %eax
	testl	%eax, %eax
	jle	.L340
	subl	$1, %eax
	movl	%eax, 24(%rsi)
.L325:
	movl	-404(%rbp), %esi
	testl	%esi, %esi
	je	.L343
.L314:
	movq	-456(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%r12, %rax
	jne	.L344
.L347:
	movq	-360(%rbp), %rax
	andb	-132(%rbp), %bl
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L650
	movq	-360(%rbp), %rsi
	subl	$1, %eax
	movl	%eax, 24(%rsi)
	movq	%rsi, %rdi
.L348:
	movl	-136(%rbp), %eax
	movq	-192(%rbp), %rsi
	leal	-1(%rax), %edx
	movq	(%rdi), %rax
	call	*16(%rax)
.L343:
	movl	-340(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L351
	jmp	.L315
.L162:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L161
	cmpl	44(%rdi), %eax
	jl	.L164
	movslq	48(%rdi), %rdx
	salq	$2, %rdx
	jmp	.L161
.L266:
	movl	$13, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	jmp	.L267
.L403:
	movl	$4, %esi
	jmp	.L366
.L644:
	movq	-256(%rbp), %r10
	jmp	.L367
.L398:
	movl	$1, %ebx
	leaq	_ZN6icu_6713CollationKeys13LevelCallback11needToWriteENS_9Collation5LevelE(%rip), %r12
	jmp	.L313
.L647:
	movq	-360(%rbp), %r15
	movslq	20(%r15), %rdx
	cmpl	16(%r15), %edx
	jl	.L323
	movq	(%r15), %rax
	movl	$1, %esi
	movq	%r15, %rdi
	call	*48(%rax)
	testb	%al, %al
	je	.L324
	movslq	20(%r15), %rdx
.L323:
	movq	-360(%rbp), %rax
	movq	8(%rax), %rax
	movb	$1, (%rax,%rdx)
.L324:
	movq	-360(%rbp), %rax
	addl	$1, 20(%rax)
	movq	%rax, %rdi
	jmp	.L322
.L646:
	movq	%r11, %rdi
	movq	%r10, -496(%rbp)
	movl	%r8d, -480(%rbp)
	call	uprv_free_67@PLT
	movq	-496(%rbp), %r10
	movl	-480(%rbp), %r8d
	jmp	.L254
.L645:
	movl	-384(%rbp), %eax
	cmpl	%edx, -248(%rbp)
	movq	%r11, %rsi
	movq	%r10, %rdi
	cmovle	-248(%rbp), %edx
	movl	%r8d, -496(%rbp)
	movq	%r11, -480(%rbp)
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	movl	-496(%rbp), %r8d
	movq	-480(%rbp), %r11
	movq	%rax, %r10
	jmp	.L253
.L335:
	movslq	20(%r15), %rdx
	cmpl	16(%r15), %edx
	jl	.L337
	movq	(%r15), %rax
	movb	%r9b, -376(%rbp)
	movl	$1, %esi
	movq	%r15, %rdi
	call	*48(%rax)
	testb	%al, %al
	je	.L338
	movslq	20(%r15), %rdx
	movzbl	-376(%rbp), %r9d
.L337:
	movzbl	%bl, %eax
	movq	8(%r15), %rsi
	sarl	$4, %eax
	orl	%r9d, %eax
	movb	%al, (%rsi,%rdx)
.L338:
	addl	$1, 20(%r15)
	cmpl	%r12d, %r13d
	jle	.L584
	movq	-320(%rbp), %rax
	movslq	%r12d, %rdx
	movzbl	(%rax,%rdx), %r9d
	jmp	.L374
.L648:
	movq	-360(%rbp), %r15
	movslq	20(%r15), %rdx
	cmpl	16(%r15), %edx
	jl	.L331
	movq	(%r15), %rax
	movl	$1, %esi
	movq	%r15, %rdi
	call	*48(%rax)
	testb	%al, %al
	je	.L332
	movslq	20(%r15), %rdx
.L331:
	movq	-360(%rbp), %rax
	movq	8(%rax), %rax
	movb	$1, (%rax,%rdx)
.L332:
	movq	-360(%rbp), %rax
	addl	$1, 20(%rax)
	jmp	.L330
.L164:
	movl	%eax, %edx
	movq	(%rdi), %rdi
	movl	%eax, %r8d
	sarl	$11, %edx
	sarl	$5, %r8d
	addl	$2080, %edx
	andl	$63, %r8d
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	addl	%r8d, %edx
	movslq	%edx, %rdx
	jmp	.L586
.L614:
	movq	-360(%rbp), %r15
	movslq	20(%r15), %rdx
	cmpl	16(%r15), %edx
	jl	.L357
	movq	(%r15), %rax
	movl	$1, %esi
	movq	%r15, %rdi
	call	*48(%rax)
	testb	%al, %al
	je	.L358
	movslq	20(%r15), %rdx
.L357:
	movq	-360(%rbp), %rax
	movq	8(%rax), %rax
	movb	$1, (%rax,%rdx)
.L358:
	movq	-360(%rbp), %rax
	addl	$1, 20(%rax)
	movq	%rax, %rdi
	jmp	.L356
.L352:
	movq	-456(%rbp), %rdi
	movl	$5, %esi
	call	*%rax
	testb	%al, %al
	je	.L229
	jmp	.L355
.L399:
	movl	$1, %ebx
	leaq	_ZN6icu_6713CollationKeys13LevelCallback11needToWriteENS_9Collation5LevelE(%rip), %r12
	jmp	.L314
.L318:
	movq	-456(%rbp), %rdi
	movl	$2, %esi
	call	*%rax
	testb	%al, %al
	je	.L229
	jmp	.L321
.L326:
	movq	-456(%rbp), %rdi
	movl	$3, %esi
	call	*%rax
	testb	%al, %al
	je	.L229
	jmp	.L329
.L340:
	movslq	20(%rsi), %rdx
	movq	%rsi, %r15
	cmpl	16(%rsi), %edx
	jl	.L341
	movq	(%rsi), %rax
	movq	%r15, %rdi
	movl	$1, %esi
	call	*48(%rax)
	testb	%al, %al
	je	.L342
	movslq	20(%r15), %rdx
.L341:
	movq	-360(%rbp), %rax
	movq	8(%rax), %rax
	movb	%r13b, (%rax,%rdx)
.L342:
	movq	-360(%rbp), %rax
	addl	$1, 20(%rax)
	jmp	.L325
.L252:
	movb	$0, -196(%rbp)
	jmp	.L231
.L584:
	movl	%r14d, %ebx
	movq	-368(%rbp), %r12
	movq	-352(%rbp), %r14
	jmp	.L325
.L650:
	movq	-360(%rbp), %r15
	movslq	20(%r15), %rdx
	cmpl	16(%r15), %edx
	jl	.L349
	movq	(%r15), %rax
	movl	$1, %esi
	movq	%r15, %rdi
	call	*48(%rax)
	testb	%al, %al
	je	.L350
	movslq	20(%r15), %rdx
.L349:
	movq	-360(%rbp), %rax
	movq	8(%rax), %rax
	movb	$1, (%rax,%rdx)
.L350:
	movq	-360(%rbp), %rax
	addl	$1, 20(%rax)
	movq	%rax, %rdi
	jmp	.L348
.L344:
	movq	-456(%rbp), %rdi
	movl	$4, %esi
	call	*%rax
	testb	%al, %al
	je	.L229
	jmp	.L347
.L401:
	movl	%edx, %r12d
	movzbl	%bl, %r9d
	jmp	.L374
.L619:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3320:
	.size	_ZN6icu_6713CollationKeys26writeSortKeyUpToQuaternaryERNS_17CollationIteratorEPKaRKNS_17CollationSettingsERNS_15SortKeyByteSinkENS_9Collation5LevelERNS0_13LevelCallbackEaR10UErrorCode, .-_ZN6icu_6713CollationKeys26writeSortKeyUpToQuaternaryERNS_17CollationIteratorEPKaRKNS_17CollationSettingsERNS_15SortKeyByteSinkENS_9Collation5LevelERNS0_13LevelCallbackEaR10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6715SortKeyByteSinkE
	.section	.rodata._ZTSN6icu_6715SortKeyByteSinkE,"aG",@progbits,_ZTSN6icu_6715SortKeyByteSinkE,comdat
	.align 16
	.type	_ZTSN6icu_6715SortKeyByteSinkE, @object
	.size	_ZTSN6icu_6715SortKeyByteSinkE, 27
_ZTSN6icu_6715SortKeyByteSinkE:
	.string	"N6icu_6715SortKeyByteSinkE"
	.weak	_ZTIN6icu_6715SortKeyByteSinkE
	.section	.data.rel.ro._ZTIN6icu_6715SortKeyByteSinkE,"awG",@progbits,_ZTIN6icu_6715SortKeyByteSinkE,comdat
	.align 8
	.type	_ZTIN6icu_6715SortKeyByteSinkE, @object
	.size	_ZTIN6icu_6715SortKeyByteSinkE, 24
_ZTIN6icu_6715SortKeyByteSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715SortKeyByteSinkE
	.quad	_ZTIN6icu_678ByteSinkE
	.weak	_ZTSN6icu_6713CollationKeys13LevelCallbackE
	.section	.rodata._ZTSN6icu_6713CollationKeys13LevelCallbackE,"aG",@progbits,_ZTSN6icu_6713CollationKeys13LevelCallbackE,comdat
	.align 32
	.type	_ZTSN6icu_6713CollationKeys13LevelCallbackE, @object
	.size	_ZTSN6icu_6713CollationKeys13LevelCallbackE, 40
_ZTSN6icu_6713CollationKeys13LevelCallbackE:
	.string	"N6icu_6713CollationKeys13LevelCallbackE"
	.weak	_ZTIN6icu_6713CollationKeys13LevelCallbackE
	.section	.data.rel.ro._ZTIN6icu_6713CollationKeys13LevelCallbackE,"awG",@progbits,_ZTIN6icu_6713CollationKeys13LevelCallbackE,comdat
	.align 8
	.type	_ZTIN6icu_6713CollationKeys13LevelCallbackE, @object
	.size	_ZTIN6icu_6713CollationKeys13LevelCallbackE, 24
_ZTIN6icu_6713CollationKeys13LevelCallbackE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713CollationKeys13LevelCallbackE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTVN6icu_6715SortKeyByteSinkE
	.section	.data.rel.ro._ZTVN6icu_6715SortKeyByteSinkE,"awG",@progbits,_ZTVN6icu_6715SortKeyByteSinkE,comdat
	.align 8
	.type	_ZTVN6icu_6715SortKeyByteSinkE, @object
	.size	_ZTVN6icu_6715SortKeyByteSinkE, 72
_ZTVN6icu_6715SortKeyByteSinkE:
	.quad	0
	.quad	_ZTIN6icu_6715SortKeyByteSinkE
	.quad	0
	.quad	0
	.quad	_ZN6icu_6715SortKeyByteSink6AppendEPKci
	.quad	_ZN6icu_6715SortKeyByteSink15GetAppendBufferEiiPciPi
	.quad	_ZN6icu_678ByteSink5FlushEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6713CollationKeys13LevelCallbackE
	.section	.data.rel.ro.local._ZTVN6icu_6713CollationKeys13LevelCallbackE,"awG",@progbits,_ZTVN6icu_6713CollationKeys13LevelCallbackE,comdat
	.align 8
	.type	_ZTVN6icu_6713CollationKeys13LevelCallbackE, @object
	.size	_ZTVN6icu_6713CollationKeys13LevelCallbackE, 40
_ZTVN6icu_6713CollationKeys13LevelCallbackE:
	.quad	0
	.quad	_ZTIN6icu_6713CollationKeys13LevelCallbackE
	.quad	_ZN6icu_6713CollationKeys13LevelCallbackD1Ev
	.quad	_ZN6icu_6713CollationKeys13LevelCallbackD0Ev
	.quad	_ZN6icu_6713CollationKeys13LevelCallback11needToWriteENS_9Collation5LevelE
	.section	.rodata
	.align 32
	.type	_ZN6icu_67L10levelMasksE, @object
	.size	_ZN6icu_67L10levelMasksE, 64
_ZN6icu_67L10levelMasksE:
	.long	2
	.long	6
	.long	22
	.long	54
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	54
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
