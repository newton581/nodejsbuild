	.file	"msgfmt.cpp"
	.text
	.p2align 4
	.type	equalFormatsForHash, @function
equalFormatsForHash:
.LFB2962:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE2962:
	.size	equalFormatsForHash, .-equalFormatsForHash
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat17getDynamicClassIDEv
	.type	_ZNK6icu_6713MessageFormat17getDynamicClassIDEv, @function
_ZNK6icu_6713MessageFormat17getDynamicClassIDEv:
.LFB2964:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713MessageFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2964:
	.size	_ZNK6icu_6713MessageFormat17getDynamicClassIDEv, .-_ZNK6icu_6713MessageFormat17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721FormatNameEnumeration17getDynamicClassIDEv
	.type	_ZNK6icu_6721FormatNameEnumeration17getDynamicClassIDEv, @function
_ZNK6icu_6721FormatNameEnumeration17getDynamicClassIDEv:
.LFB2966:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6721FormatNameEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2966:
	.size	_ZNK6icu_6721FormatNameEnumeration17getDynamicClassIDEv, .-_ZNK6icu_6721FormatNameEnumeration17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat9getLocaleEv
	.type	_ZNK6icu_6713MessageFormat9getLocaleEv, @function
_ZNK6icu_6713MessageFormat9getLocaleEv:
.LFB2998:
	.cfi_startproc
	endbr64
	leaq	328(%rdi), %rax
	ret
	.cfi_endproc
.LFE2998:
	.size	_ZNK6icu_6713MessageFormat9getLocaleEv, .-_ZNK6icu_6713MessageFormat9getLocaleEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat11DummyFormateqERKNS_6FormatE
	.type	_ZNK6icu_6713MessageFormat11DummyFormateqERKNS_6FormatE, @function
_ZNK6icu_6713MessageFormat11DummyFormateqERKNS_6FormatE:
.LFB3069:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3069:
	.size	_ZNK6icu_6713MessageFormat11DummyFormateqERKNS_6FormatE, .-_ZNK6icu_6713MessageFormat11DummyFormateqERKNS_6FormatE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode:
.LFB3071:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	movl	(%rcx), %edx
	testl	%edx, %edx
	jle	.L9
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$16, (%rcx)
	ret
	.cfi_endproc
.LFE3071:
	.size	_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB3072:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	movl	(%r8), %edx
	testl	%edx, %edx
	jle	.L12
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$16, (%r8)
	ret
	.cfi_endproc
.LFE3072:
	.size	_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.globl	_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.set	_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode,_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat11DummyFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.type	_ZNK6icu_6713MessageFormat11DummyFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, @function
_ZNK6icu_6713MessageFormat11DummyFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE:
.LFB3074:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3074:
	.size	_ZNK6icu_6713MessageFormat11DummyFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, .-_ZNK6icu_6713MessageFormat11DummyFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721FormatNameEnumeration5resetER10UErrorCode
	.type	_ZN6icu_6721FormatNameEnumeration5resetER10UErrorCode, @function
_ZN6icu_6721FormatNameEnumeration5resetER10UErrorCode:
.LFB3079:
	.cfi_startproc
	endbr64
	movl	$0, 116(%rdi)
	ret
	.cfi_endproc
.LFE3079:
	.size	_ZN6icu_6721FormatNameEnumeration5resetER10UErrorCode, .-_ZN6icu_6721FormatNameEnumeration5resetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721FormatNameEnumeration5countER10UErrorCode
	.type	_ZNK6icu_6721FormatNameEnumeration5countER10UErrorCode, @function
_ZNK6icu_6721FormatNameEnumeration5countER10UErrorCode:
.LFB3080:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L15
	movl	8(%rdx), %eax
.L15:
	ret
	.cfi_endproc
.LFE3080:
	.size	_ZNK6icu_6721FormatNameEnumeration5countER10UErrorCode, .-_ZNK6icu_6721FormatNameEnumeration5countER10UErrorCode
	.section	.text._ZN6icu_6713MessageFormat11DummyFormatD2Ev,"axG",@progbits,_ZN6icu_6713MessageFormat11DummyFormatD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6713MessageFormat11DummyFormatD2Ev
	.type	_ZN6icu_6713MessageFormat11DummyFormatD2Ev, @function
_ZN6icu_6713MessageFormat11DummyFormatD2Ev:
.LFB4330:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713MessageFormat11DummyFormatE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_676FormatD2Ev@PLT
	.cfi_endproc
.LFE4330:
	.size	_ZN6icu_6713MessageFormat11DummyFormatD2Ev, .-_ZN6icu_6713MessageFormat11DummyFormatD2Ev
	.weak	_ZN6icu_6713MessageFormat11DummyFormatD1Ev
	.set	_ZN6icu_6713MessageFormat11DummyFormatD1Ev,_ZN6icu_6713MessageFormat11DummyFormatD2Ev
	.section	.text._ZN6icu_6713MessageFormat11DummyFormatD0Ev,"axG",@progbits,_ZN6icu_6713MessageFormat11DummyFormatD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6713MessageFormat11DummyFormatD0Ev
	.type	_ZN6icu_6713MessageFormat11DummyFormatD0Ev, @function
_ZN6icu_6713MessageFormat11DummyFormatD0Ev:
.LFB4332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713MessageFormat11DummyFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_676FormatD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4332:
	.size	_ZN6icu_6713MessageFormat11DummyFormatD0Ev, .-_ZN6icu_6713MessageFormat11DummyFormatD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat11DummyFormat5cloneEv
	.type	_ZNK6icu_6713MessageFormat11DummyFormat5cloneEv, @function
_ZNK6icu_6713MessageFormat11DummyFormat5cloneEv:
.LFB3070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$328, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L22
	xorl	%eax, %eax
	movl	$41, %ecx
	movq	%r12, %rdi
	rep stosq
	movq	%r12, %rdi
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6713MessageFormat11DummyFormatE(%rip), %rax
	movq	%rax, (%r12)
.L22:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3070:
	.size	_ZNK6icu_6713MessageFormat11DummyFormat5cloneEv, .-_ZNK6icu_6713MessageFormat11DummyFormat5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat9setLocaleERKNS_6LocaleE
	.type	_ZN6icu_6713MessageFormat9setLocaleERKNS_6LocaleE, @function
_ZN6icu_6713MessageFormat9setLocaleERKNS_6LocaleE:
.LFB2997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	328(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	jne	.L28
	movq	720(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	(%rdi), %rax
	call	*8(%rax)
.L30:
	movq	728(%rbx), %rdi
	movq	$0, 720(%rbx)
	testq	%rdi, %rdi
	je	.L31
	movq	(%rdi), %rax
	call	*8(%rax)
.L31:
	movq	$0, 728(%rbx)
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	368(%rbx), %rsi
	movq	%rbx, %rdi
	movq	%rsi, %rdx
	call	_ZN6icu_676Format12setLocaleIDsEPKcS2_@PLT
	movq	768(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L32
	movq	(%rdi), %rax
	call	*8(%rax)
.L32:
	movq	800(%rbx), %rdi
	movq	$0, 768(%rbx)
	testq	%rdi, %rdi
	je	.L33
	movq	(%rdi), %rax
	call	*8(%rax)
.L33:
	movq	$0, 800(%rbx)
.L28:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2997:
	.size	_ZN6icu_6713MessageFormat9setLocaleERKNS_6LocaleE, .-_ZN6icu_6713MessageFormat9setLocaleERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721FormatNameEnumerationD2Ev
	.type	_ZN6icu_6721FormatNameEnumerationD2Ev, @function
_ZN6icu_6721FormatNameEnumerationD2Ev:
.LFB3082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721FormatNameEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L48
	movq	(%rdi), %rax
	call	*8(%rax)
.L48:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717StringEnumerationD2Ev@PLT
	.cfi_endproc
.LFE3082:
	.size	_ZN6icu_6721FormatNameEnumerationD2Ev, .-_ZN6icu_6721FormatNameEnumerationD2Ev
	.globl	_ZN6icu_6721FormatNameEnumerationD1Ev
	.set	_ZN6icu_6721FormatNameEnumerationD1Ev,_ZN6icu_6721FormatNameEnumerationD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat22PluralSelectorProviderD2Ev
	.type	_ZN6icu_6713MessageFormat22PluralSelectorProviderD2Ev, @function
_ZN6icu_6713MessageFormat22PluralSelectorProviderD2Ev:
.LFB3089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L54
	movq	(%rdi), %rax
	call	*8(%rax)
.L54:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712PluralFormat14PluralSelectorD2Ev@PLT
	.cfi_endproc
.LFE3089:
	.size	_ZN6icu_6713MessageFormat22PluralSelectorProviderD2Ev, .-_ZN6icu_6713MessageFormat22PluralSelectorProviderD2Ev
	.globl	_ZN6icu_6713MessageFormat22PluralSelectorProviderD1Ev
	.set	_ZN6icu_6713MessageFormat22PluralSelectorProviderD1Ev,_ZN6icu_6713MessageFormat22PluralSelectorProviderD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat9toPatternERNS_13UnicodeStringE
	.type	_ZNK6icu_6713MessageFormat9toPatternERNS_13UnicodeStringE, @function
_ZNK6icu_6713MessageFormat9toPatternERNS_13UnicodeStringE:
.LFB3003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	744(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L63
	call	uhash_count_67@PLT
	testl	%eax, %eax
	jne	.L61
.L63:
	movl	648(%rbx), %eax
	testl	%eax, %eax
	je	.L61
	movswl	576(%rbx), %ecx
	leaq	568(%rbx), %rsi
	testw	%cx, %cx
	js	.L65
	popq	%rbx
	movq	%r12, %rdi
	sarl	$5, %ecx
	popq	%r12
	xorl	%edx, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movl	580(%rbx), %ecx
	movq	%r12, %rdi
	popq	%rbx
	xorl	%edx, %edx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3003:
	.size	_ZNK6icu_6713MessageFormat9toPatternERNS_13UnicodeStringE, .-_ZNK6icu_6713MessageFormat9toPatternERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721FormatNameEnumeration5snextER10UErrorCode
	.type	_ZN6icu_6721FormatNameEnumeration5snextER10UErrorCode, @function
_ZN6icu_6721FormatNameEnumeration5snextER10UErrorCode:
.LFB3078:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L73
	movq	120(%rdi), %r8
	movl	116(%rdi), %esi
	cmpl	8(%r8), %esi
	jge	.L73
	leal	1(%rsi), %eax
	movl	%eax, 116(%rdi)
	movq	%r8, %rdi
	jmp	_ZNK6icu_677UVector9elementAtEi@PLT
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3078:
	.size	_ZN6icu_6721FormatNameEnumeration5snextER10UErrorCode, .-_ZN6icu_6721FormatNameEnumeration5snextER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat14getFormatNamesER10UErrorCode
	.type	_ZN6icu_6713MessageFormat14getFormatNamesER10UErrorCode, @function
_ZN6icu_6713MessageFormat14getFormatNamesER10UErrorCode:
.LFB3023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L77
	movq	%rdi, %r12
	movl	$40, %edi
	movq	%rsi, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L79
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
.L79:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L94
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	leaq	568(%r12), %rax
	movq	640(%r12), %rcx
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L84:
	movslq	%r15d, %rbx
	addq	$1, %rbx
	salq	$4, %rbx
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L96:
	addq	$16, %rbx
	cmpl	$1, %eax
	je	.L95
.L83:
	movl	(%rcx,%rbx), %eax
	movl	%r15d, %edx
	addl	$1, %r15d
	cmpl	$5, %eax
	jne	.L96
	movl	$64, %edi
	movl	%edx, -72(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movl	-72(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L86
	addl	$2, %edx
	movq	-80(%rbp), %rsi
	movq	%r9, %rdi
	movq	%r9, -72(%rbp)
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	640(%r12), %rdx
	movzwl	8(%rdx), %ecx
	movl	4(%rdx), %edx
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	-72(%rbp), %r9
.L86:
	movq	%r13, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	640(%r12), %rcx
	movl	12(%rcx,%rbx), %eax
	cmpl	%eax, %r15d
	cmovl	%eax, %r15d
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$128, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L77
	movq	%r15, %rdi
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	leaq	16+_ZTVN6icu_6721FormatNameEnumerationE(%rip), %rax
	movq	%r14, 120(%r15)
	movq	%rax, (%r15)
	movl	$0, 116(%r15)
.L77:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L94:
	.cfi_restore_state
	movl	$7, 0(%r13)
	xorl	%r15d, %r15d
	jmp	.L77
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3023:
	.size	_ZN6icu_6713MessageFormat14getFormatNamesER10UErrorCode, .-_ZN6icu_6713MessageFormat14getFormatNamesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721FormatNameEnumerationD0Ev
	.type	_ZN6icu_6721FormatNameEnumerationD0Ev, @function
_ZN6icu_6721FormatNameEnumerationD0Ev:
.LFB3084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721FormatNameEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L99
	movq	(%rdi), %rax
	call	*8(%rax)
.L99:
	movq	%r12, %rdi
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3084:
	.size	_ZN6icu_6721FormatNameEnumerationD0Ev, .-_ZN6icu_6721FormatNameEnumerationD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat22PluralSelectorProviderD0Ev
	.type	_ZN6icu_6713MessageFormat22PluralSelectorProviderD0Ev, @function
_ZN6icu_6713MessageFormat22PluralSelectorProviderD0Ev:
.LFB3091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L105
	movq	(%rdi), %rax
	call	*8(%rax)
.L105:
	movq	%r12, %rdi
	call	_ZN6icu_6712PluralFormat14PluralSelectorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3091:
	.size	_ZN6icu_6713MessageFormat22PluralSelectorProviderD0Ev, .-_ZN6icu_6713MessageFormat22PluralSelectorProviderD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat10getFormatsERi
	.type	_ZNK6icu_6713MessageFormat10getFormatsERi, @function
_ZNK6icu_6713MessageFormat10getFormatsERi:
.LFB3021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	640(%rdi), %rdx
	.p2align 4,,10
	.p2align 3
.L124:
	movslq	%esi, %rax
	addq	$1, %rax
	salq	$4, %rax
	addq	%rdx, %rax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L141:
	addq	$16, %rax
	cmpl	$1, %ecx
	je	.L140
.L113:
	movl	(%rax), %ecx
	addl	$1, %esi
	cmpl	$5, %ecx
	jne	.L141
	movl	12(%rax), %eax
	addl	$1, %r13d
	cmpl	%eax, %esi
	cmovl	%eax, %esi
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L140:
	movq	680(%rbx), %r9
	movl	$0, (%r12)
	testq	%r9, %r9
	je	.L142
	cmpl	%r13d, 688(%rbx)
	jl	.L143
.L116:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L122:
	leal	1(%rax), %esi
	cltq
	addq	$1, %rax
	movslq	%esi, %rsi
	salq	$4, %rax
	addq	%rdx, %rax
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L144:
	addq	$1, %rsi
	addq	$16, %rax
	cmpl	$1, %ecx
	je	.L110
.L119:
	movl	(%rax), %ecx
	movq	%rsi, %r13
	movl	%esi, %r15d
	movq	%rax, %r10
	salq	$4, %r13
	cmpl	$5, %ecx
	jne	.L144
	movslq	(%r12), %rax
	movq	736(%rbx), %rdi
	leal	1(%rax), %ecx
	leaq	(%r9,%rax,8), %r14
	movl	%ecx, (%r12)
	testq	%rdi, %rdi
	je	.L121
	call	uhash_iget_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L145
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713MessageFormat11DummyFormatE(%rip), %rdx
	leaq	_ZTIN6icu_676FormatE(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	__dynamic_cast@PLT
	movq	-56(%rbp), %rdi
	movq	640(%rbx), %rdx
	testq	%rax, %rax
	movl	$0, %eax
	movq	680(%rbx), %r9
	cmovne	%rax, %rdi
	leaq	(%rdx,%r13), %r10
.L121:
	cmpl	%r15d, 12(%r10)
	movl	%r15d, %eax
	cmovge	12(%r10), %eax
	movq	%rdi, (%r14)
	jmp	.L122
.L117:
	movl	$0, 688(%rbx)
	xorl	%r9d, %r9d
.L110:
	addq	$24, %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L145:
	.cfi_restore_state
	movq	640(%rbx), %rdx
	movq	680(%rbx), %r9
	leaq	(%rdx,%r13), %r10
	jmp	.L121
.L143:
	movslq	%r13d, %rsi
	movq	%r9, %rdi
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L117
	movq	%rax, 680(%rbx)
	movq	640(%rbx), %rdx
	movl	%r13d, 688(%rbx)
	jmp	.L116
.L142:
	movl	%r13d, 688(%rbx)
	movslq	%r13d, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L117
	movq	%rax, 680(%rbx)
	movq	640(%rbx), %rdx
	jmp	.L116
	.cfi_endproc
.LFE3021:
	.size	_ZNK6icu_6713MessageFormat10getFormatsERi, .-_ZNK6icu_6713MessageFormat10getFormatsERi
	.align 2
	.p2align 4
	.type	_ZNK6icu_6713MessageFormat5parseEiRKNS_13UnicodeStringERNS_13ParsePositionERiR10UErrorCode.part.0, @function
_ZNK6icu_6713MessageFormat5parseEiRKNS_13UnicodeStringERNS_13ParsePositionERiR10UErrorCode.part.0:
.LFB4372:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$456, %rsp
	movq	%rdi, -392(%rbp)
	movq	%rdx, -400(%rbp)
	movq	%rcx, -480(%rbp)
	movq	%r8, -432(%rbp)
	movq	%r9, -488(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	704(%rdi), %rax
	movl	$120, %edi
	testl	%eax, %eax
	je	.L147
	movabsq	$82351536043346212, %rdx
	movq	$-1, %rdi
	cmpq	%rdx, %rax
	jbe	.L263
.L147:
	call	_ZN6icu_677UMemorynaEm@PLT
	testq	%rax, %rax
	je	.L218
	movq	-392(%rbp), %rcx
	leaq	8(%rax), %rdi
	movq	%rdi, -424(%rbp)
	movslq	704(%rcx), %r12
	testl	%r12d, %r12d
	jne	.L149
	movq	$1, (%rax)
	call	_ZN6icu_6711FormattableC1Ev@PLT
.L148:
	movq	-392(%rbp), %rax
	movq	-480(%rbp), %rdi
	movslq	%ebx, %rdx
	leal	1(%rbx), %r15d
	salq	$4, %rdx
	leaq	-368(%rbp), %rbx
	leaq	568(%rax), %rcx
	movq	640(%rax), %rax
	movl	8(%rdi), %r13d
	movabsq	$-4294967296, %rdi
	movq	%rcx, -472(%rbp)
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rcx
	movzwl	8(%rax,%rdx), %esi
	addl	4(%rax,%rdx), %esi
	movq	%rbx, -440(%rbp)
	movq	%rcx, -368(%rbp)
	movq	%rdi, -360(%rbp)
	.p2align 4,,10
	.p2align 3
.L208:
	movslq	%r15d, %r10
	salq	$4, %r10
	leaq	(%rax,%r10), %rbx
	movl	4(%rbx), %r14d
	movl	(%rbx), %r12d
	subl	%esi, %r14d
	jne	.L264
.L152:
	addl	%r13d, %r14d
	cmpl	$1, %r12d
	je	.L265
	movq	-392(%rbp), %rax
	subl	$2, %r12d
	leal	1(%r15), %edx
	movq	640(%rax), %rax
	cmpl	$1, %r12d
	jbe	.L266
	movl	(%rbx), %edi
	cmpl	%r15d, 12(%rax,%r10)
	movl	%r15d, %r12d
	cmovge	12(%rax,%r10), %r12d
	leal	-5(%rdi), %edx
	cmpl	$1, %edx
	jbe	.L267
	movswq	26(%rax,%r10), %rdi
	movl	$2, %ecx
	movq	-392(%rbp), %rsi
	movw	%cx, -312(%rbp)
	movq	%rdi, %rdx
	movl	%edi, -408(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdi
	leaq	0(,%rdx,8), %rcx
	movq	%rdi, -320(%rbp)
	movq	736(%rsi), %rdi
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	salq	$4, %rdx
	addq	-424(%rbp), %rdx
	movq	%rdx, -416(%rbp)
	testq	%rdi, %rdi
	je	.L172
	movl	%r15d, %esi
	call	uhash_iget_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L214
	xorl	%ebx, %ebx
.L213:
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713MessageFormat11DummyFormatE(%rip), %rdx
	leaq	_ZTIN6icu_676FormatE(%rip), %rsi
	movq	%r13, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L268
.L168:
	testl	%ebx, %ebx
	je	.L214
	movq	-392(%rbp), %r13
	movq	736(%r13), %rdi
	testq	%rdi, %rdi
	je	.L174
	movl	%r15d, %esi
	call	uhash_iget_67@PLT
	testq	%rax, %rax
	je	.L174
	movq	640(%r13), %rax
.L172:
	movslq	%r12d, %rbx
	leal	1(%r12), %r15d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdi
	movl	$2, %r9d
	salq	$4, %rbx
	leaq	-256(%rbp), %r12
	movl	%r14d, -448(%rbp)
	leaq	16(%rbx), %r8
	movzwl	8(%rax,%rbx), %edx
	movq	%rbx, -456(%rbp)
	movq	%r12, %r14
	addl	4(%rax,%rbx), %edx
	movl	%r15d, -464(%rbp)
	movq	%r8, %r12
	movq	%rdi, -256(%rbp)
	movq	-472(%rbp), %rbx
	movw	%r9w, -248(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L269:
	movq	-392(%rbp), %rax
	movzwl	8(%r15), %edx
	addl	4(%r15), %edx
	movq	640(%rax), %rax
.L176:
	leaq	(%rax,%r12), %r15
	movq	%rbx, %rsi
	movq	%r14, %rdi
	addq	$16, %r12
	movl	(%r15), %r13d
	movl	4(%r15), %ecx
	andl	$-5, %r13d
	subl	%edx, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	cmpl	$1, %r13d
	jne	.L269
	movswl	-248(%rbp), %ecx
	movq	-400(%rbp), %rsi
	movq	%r14, %r12
	movq	-456(%rbp), %rbx
	movl	-464(%rbp), %r15d
	movl	%ecx, %eax
	sarl	$5, %ecx
	movl	-448(%rbp), %r14d
	movswl	8(%rsi), %r13d
	jne	.L270
	testw	%r13w, %r13w
	js	.L191
	sarl	$5, %r13d
.L192:
	leaq	-192(%rbp), %r10
	movq	-400(%rbp), %rsi
	movl	%r13d, %ecx
	movl	%r14d, %edx
	movq	%r10, %rdi
	subl	%r14d, %ecx
	movq	%r10, -464(%rbp)
	leaq	-128(%rbp), %r14
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	leaq	-370(%rbp), %r11
	movl	$2, %esi
	xorl	%edx, %edx
	movl	$123, %edi
	movw	%si, -120(%rbp)
	movq	%r11, %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%di, -370(%rbp)
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%r11, -456(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	-352(%rbp), %r9
	movl	-408(%rbp), %edx
	xorl	%r8d, %r8d
	movq	%r9, %rdi
	movl	$10, %ecx
	movl	$16, %esi
	movq	%r9, -448(%rbp)
	call	uprv_itou_67@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	-448(%rbp), %r9
	movq	%r9, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-448(%rbp), %r9
	movl	$125, %r8d
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	-456(%rbp), %r11
	movw	%r8w, -370(%rbp)
	movq	%r11, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	-120(%rbp), %esi
	movq	-464(%rbp), %r10
	testw	%si, %si
	js	.L193
	movswl	%si, %ecx
	sarl	$5, %ecx
.L194:
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L195
	movswl	%ax, %edx
	sarl	$5, %edx
.L196:
	testb	$1, %sil
	je	.L197
	notl	%eax
	andl	$1, %eax
	testb	%al, %al
	jne	.L271
.L201:
	movq	%r14, %rdi
	movq	%r10, -408(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-408(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L203:
	movq	-392(%rbp), %r14
	leaq	-320(%rbp), %rdi
	addq	640(%r14), %rbx
	movzwl	8(%rbx), %esi
	addl	4(%rbx), %esi
	movl	%esi, -408(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	640(%r14), %rax
	movl	-408(%rbp), %esi
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L149:
	movq	%r12, (%rax)
	subq	$1, %r12
	movq	%rdi, %r13
	js	.L148
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%r13, %rdi
	subq	$1, %r12
	addq	$112, %r13
	call	_ZN6icu_6711FormattableC1Ev@PLT
	cmpq	$-1, %r12
	jne	.L151
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L263:
	leaq	0(,%rax,8), %rdi
	subq	%rax, %rdi
	salq	$4, %rdi
	addq	$8, %rdi
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L264:
	movq	-400(%rbp), %rax
	movzwl	8(%rax), %edx
	testb	$1, %dl
	je	.L153
	movq	-392(%rbp), %rax
	movzwl	576(%rax), %eax
	movw	%ax, -408(%rbp)
	movzbl	-408(%rbp), %eax
	notl	%eax
	andl	$1, %eax
.L154:
	testb	%al, %al
	je	.L152
	movq	-480(%rbp), %rax
	movl	%r13d, 12(%rax)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L267:
	movswq	26(%rax,%r10), %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rsi
	movl	$2, %r10d
	movswl	10(%rbx), %ebx
	movq	%rsi, -320(%rbp)
	movq	-392(%rbp), %rsi
	leaq	0(,%rdi,8), %rcx
	movl	%edi, -408(%rbp)
	subq	%rdi, %rcx
	movq	736(%rsi), %rdi
	movw	%r10w, -312(%rbp)
	movq	%rcx, %rdx
	salq	$4, %rdx
	addq	-424(%rbp), %rdx
	movq	%rdx, -416(%rbp)
	testq	%rdi, %rdi
	je	.L167
	movl	%r15d, %esi
	call	uhash_iget_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L213
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L268:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movl	%r14d, -360(%rbp)
	movq	-440(%rbp), %rcx
	movq	-416(%rbp), %rdx
	movq	-400(%rbp), %rsi
	call	*56(%rax)
	movl	-360(%rbp), %r13d
	cmpl	%r13d, %r14d
	je	.L272
	movslq	%r12d, %rbx
	leal	1(%r12), %r15d
	salq	$4, %rbx
.L202:
	movq	-432(%rbp), %rsi
	movl	-408(%rbp), %eax
	cmpl	%eax, (%rsi)
	jg	.L203
	addl	$1, %eax
	movl	%eax, (%rsi)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L266:
	movzwl	8(%rbx), %esi
	movl	%edx, %r15d
	addl	4(%rbx), %esi
	movl	%r14d, %r13d
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L167:
	testl	%ebx, %ebx
	je	.L172
.L174:
	cmpl	$2, %ebx
	jne	.L273
	movq	-392(%rbp), %rax
	movq	-440(%rbp), %rcx
	leal	2(%r15), %esi
	movl	%r14d, -360(%rbp)
	movq	-400(%rbp), %rdx
	leaq	552(%rax), %rdi
	call	_ZN6icu_6712ChoiceFormat13parseArgumentERKNS_14MessagePatternEiRKNS_13UnicodeStringERNS_13ParsePositionE@PLT
	cmpl	-360(%rbp), %r14d
	je	.L274
	movq	-416(%rbp), %rdi
	movslq	%r12d, %rbx
	leal	1(%r12), %r15d
	salq	$4, %rbx
	call	_ZN6icu_6711Formattable9setDoubleEd@PLT
	movl	-360(%rbp), %r13d
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L153:
	testw	%dx, %dx
	js	.L155
	movswl	%dx, %eax
	sarl	$5, %eax
.L156:
	xorl	%r8d, %r8d
	testl	%r13d, %r13d
	js	.L157
	cmpl	%eax, %r13d
	movl	%eax, %r8d
	cmovle	%r13d, %r8d
.L157:
	xorl	%r9d, %r9d
	testl	%r14d, %r14d
	js	.L158
	subl	%r8d, %eax
	cmpl	%r14d, %eax
	cmovg	%r14d, %eax
	movl	%eax, %r9d
.L158:
	movq	-400(%rbp), %rax
	andl	$2, %edx
	leaq	10(%rax), %rcx
	jne	.L160
	movq	24(%rax), %rcx
.L160:
	movq	-472(%rbp), %rdi
	movl	%r14d, %edx
	movq	%r10, -408(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movq	-408(%rbp), %r10
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L197:
	testl	%ecx, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L199
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L199:
	andl	$2, %esi
	leaq	-118(%rbp), %rcx
	cmove	-104(%rbp), %rcx
	movq	%r10, -448(%rbp)
	movq	%r10, %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movq	-448(%rbp), %r10
	testb	%al, %al
	je	.L201
.L271:
	movq	-416(%rbp), %rdi
	movq	%r10, %rsi
	movq	%r10, -448(%rbp)
	call	_ZN6icu_6711Formattable9setStringERKNS_13UnicodeStringE@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-448(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L195:
	movl	-180(%rbp), %edx
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L193:
	movl	-116(%rbp), %ecx
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L270:
	xorl	%r8d, %r8d
	testl	%r14d, %r14d
	js	.L181
	testw	%r13w, %r13w
	js	.L182
	movswl	%r13w, %r8d
	sarl	$5, %r8d
.L183:
	cmpl	%r8d, %r14d
	cmovle	%r14d, %r8d
.L181:
	testw	%r13w, %r13w
	js	.L184
	movswl	%r13w, %r9d
	sarl	$5, %r9d
.L185:
	movl	%eax, %edx
	andl	$1, %edx
	testw	%ax, %ax
	js	.L275
	testb	%dl, %dl
	jne	.L260
	testl	%ecx, %ecx
	js	.L260
.L212:
	testb	$2, %al
	movq	-400(%rbp), %rdi
	leaq	-246(%rbp), %rsi
	cmove	-232(%rbp), %rsi
	subl	%r8d, %r9d
	xorl	%edx, %edx
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movl	%eax, %r13d
.L190:
	testl	%r13d, %r13d
	jns	.L192
	.p2align 4,,10
	.p2align 3
.L260:
	movq	-480(%rbp), %rax
	movq	%r12, %rdi
	movl	%r14d, 12(%rax)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L173:
	leaq	-320(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L161:
	movq	-440(%rbp), %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	-424(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L146
	movq	-8(%rcx), %rax
	movq	%rcx, %r12
	leaq	0(,%rax,8), %rbx
	subq	%rax, %rbx
	salq	$4, %rbx
	addq	%rcx, %rbx
	cmpq	%rbx, %rcx
	je	.L207
	.p2align 4,,10
	.p2align 3
.L206:
	movq	-112(%rbx), %rax
	subq	$112, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, %r12
	jne	.L206
.L207:
	movq	-424(%rbp), %rdi
	subq	$8, %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
	movq	$0, -424(%rbp)
.L146:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	movq	-424(%rbp), %rax
	addq	$456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movl	12(%rsi), %r13d
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L155:
	movl	12(%rax), %eax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L275:
	movl	-244(%rbp), %ecx
	testb	%dl, %dl
	jne	.L260
	testl	%ecx, %ecx
	js	.L260
	jne	.L212
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-400(%rbp), %rsi
	movl	12(%rsi), %r9d
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L182:
	movl	12(%rsi), %r8d
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L265:
	movq	-480(%rbp), %rax
	movq	-440(%rbp), %rdi
	movl	%r14d, 8(%rax)
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	jmp	.L146
.L272:
	movq	-480(%rbp), %rax
	movl	%r14d, 12(%rax)
	jmp	.L173
.L273:
	subl	$3, %ebx
	cmpl	$3, %ebx
	movq	-488(%rbp), %rbx
	sbbl	%eax, %eax
	andl	$11, %eax
	addl	$5, %eax
	movl	%eax, (%rbx)
	jmp	.L173
.L274:
	movq	-480(%rbp), %rax
	movl	%r14d, 12(%rax)
	jmp	.L173
.L276:
	call	__stack_chk_fail@PLT
.L218:
	movq	$0, -424(%rbp)
	jmp	.L148
.L214:
	movq	-392(%rbp), %rax
	movq	640(%rax), %rax
	jmp	.L172
	.cfi_endproc
.LFE4372:
	.size	_ZNK6icu_6713MessageFormat5parseEiRKNS_13UnicodeStringERNS_13ParsePositionERiR10UErrorCode.part.0, .-_ZNK6icu_6713MessageFormat5parseEiRKNS_13UnicodeStringERNS_13ParsePositionERiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionERi
	.type	_ZNK6icu_6713MessageFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionERi, @function
_ZNK6icu_6713MessageFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionERi:
.LFB3046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$0, (%rcx)
	cmpb	$0, 676(%rdi)
	movl	$0, -12(%rbp)
	jne	.L282
	movq	%rcx, %r8
	leaq	-12(%rbp), %r9
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	xorl	%esi, %esi
	call	_ZNK6icu_6713MessageFormat5parseEiRKNS_13UnicodeStringERNS_13ParsePositionERiR10UErrorCode.part.0
.L277:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L283
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movl	8(%rdx), %eax
	movl	%eax, 12(%rdx)
	xorl	%eax, %eax
	jmp	.L277
.L283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3046:
	.size	_ZNK6icu_6713MessageFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionERi, .-_ZNK6icu_6713MessageFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionERi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.type	_ZNK6icu_6713MessageFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, @function
_ZNK6icu_6713MessageFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE:
.LFB3048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	leaq	_ZNK6icu_6713MessageFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionERi(%rip), %rdx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -32(%rbp)
	movq	184(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L285
	cmpb	$0, 676(%rdi)
	movl	$0, -28(%rbp)
	jne	.L294
	movq	%rsi, %rdx
	leaq	-28(%rbp), %r9
	leaq	-32(%rbp), %r8
	xorl	%esi, %esi
	call	_ZNK6icu_6713MessageFormat5parseEiRKNS_13UnicodeStringERNS_13ParsePositionERiR10UErrorCode.part.0
	movq	%rax, %rsi
.L288:
	testq	%rsi, %rsi
	je	.L284
	movl	-32(%rbp), %edx
	movq	%r12, %rdi
	call	_ZN6icu_6711Formattable10adoptArrayEPS0_i@PLT
.L284:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L295
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	leaq	-32(%rbp), %rcx
	movq	%r10, %rdx
	call	*%rax
	movq	%rax, %rsi
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L294:
	movl	8(%rcx), %eax
	movl	%eax, 12(%rcx)
	jmp	.L284
.L295:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3048:
	.size	_ZNK6icu_6713MessageFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, .-_ZNK6icu_6713MessageFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat5parseERKNS_13UnicodeStringERiR10UErrorCode
	.type	_ZNK6icu_6713MessageFormat5parseERKNS_13UnicodeStringERiR10UErrorCode, @function
_ZNK6icu_6713MessageFormat5parseERKNS_13UnicodeStringERiR10UErrorCode:
.LFB3047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 676(%rdi)
	jne	.L311
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rdx, %r8
	leaq	_ZNK6icu_6713MessageFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionERi(%rip), %rdx
	movq	%rax, -64(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rax, -56(%rbp)
	movq	(%rdi), %rax
	movq	184(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L299
	movl	$0, (%r8)
	cmpb	$0, 676(%rdi)
	movl	$0, -68(%rbp)
	jne	.L312
	leaq	-64(%rbp), %r12
	movq	%rsi, %rdx
	leaq	-68(%rbp), %r9
	xorl	%esi, %esi
	movq	%r12, %rcx
	call	_ZNK6icu_6713MessageFormat5parseEiRKNS_13UnicodeStringERNS_13ParsePositionERiR10UErrorCode.part.0
	movq	%rax, %r13
.L302:
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	je	.L313
.L301:
	movq	%r12, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
.L296:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L314
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movl	$6, (%rbx)
	testq	%r13, %r13
	je	.L301
	movq	-8(%r13), %rax
	leaq	0(,%rax,8), %rbx
	subq	%rax, %rbx
	salq	$4, %rbx
	addq	%r13, %rbx
	cmpq	%rbx, %r13
	je	.L303
	.p2align 4,,10
	.p2align 3
.L304:
	movq	-112(%rbx), %rax
	subq	$112, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, %r13
	jne	.L304
.L303:
	leaq	-8(%r13), %rdi
	xorl	%r13d, %r13d
	call	_ZN6icu_677UMemorydaEPv@PLT
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L311:
	movl	$65804, (%rcx)
	xorl	%r13d, %r13d
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L299:
	leaq	-64(%rbp), %r12
	movq	%r8, %rcx
	movq	%r12, %rdx
	call	*%rax
	movq	%rax, %r13
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L312:
	movl	$0, -52(%rbp)
	xorl	%r13d, %r13d
	leaq	-64(%rbp), %r12
	movl	$6, (%rcx)
	jmp	.L301
.L314:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3047:
	.size	_ZNK6icu_6713MessageFormat5parseERKNS_13UnicodeStringERiR10UErrorCode, .-_ZNK6icu_6713MessageFormat5parseERKNS_13UnicodeStringERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat9getFormatERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6713MessageFormat9getFormatERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6713MessageFormat9getFormatERKNS_13UnicodeStringER10UErrorCode:
.LFB3019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L316
	cmpq	$0, 736(%rdi)
	movq	%rdi, %r13
	je	.L316
	movq	%rsi, %rdi
	movq	%rsi, %r14
	movq	%rdx, %rbx
	call	_ZN6icu_6714MessagePattern20validateArgumentNameERKNS_13UnicodeStringE@PLT
	movl	%eax, %r15d
	cmpl	$-1, %eax
	jl	.L317
	movq	640(%r13), %rcx
	xorl	%r12d, %r12d
	leaq	568(%r13), %rdi
	.p2align 4,,10
	.p2align 3
.L318:
	movslq	%r12d, %rbx
	addq	$1, %rbx
	salq	$4, %rbx
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L351:
	addq	$16, %rbx
	cmpl	$1, %eax
	je	.L316
.L321:
	movl	(%rcx,%rbx), %eax
	movl	%r12d, %edx
	addl	$1, %r12d
	cmpl	$5, %eax
	jne	.L351
	leal	2(%rdx), %eax
	cltq
	salq	$4, %rax
	addq	%rcx, %rax
	cmpl	$8, (%rax)
	je	.L352
	movswl	10(%rax), %eax
	cmpl	%r15d, %eax
	sete	%al
.L330:
	testb	%al, %al
	jne	.L353
	movq	640(%r13), %rcx
	movl	12(%rcx,%rbx), %eax
	cmpl	%eax, %r12d
	cmovl	%eax, %r12d
	jmp	.L318
.L353:
	movq	736(%r13), %rdi
	testq	%rdi, %rdi
	je	.L316
	movl	%r12d, %esi
	call	uhash_iget_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L316
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713MessageFormat11DummyFormatE(%rip), %rdx
	leaq	_ZTIN6icu_676FormatE(%rip), %rsi
	movq	%rax, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L315
	.p2align 4,,10
	.p2align 3
.L316:
	xorl	%r12d, %r12d
.L315:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movzwl	8(%r14), %edx
	movl	%edx, %esi
	andl	$1, %esi
	testw	%dx, %dx
	js	.L322
	testb	%sil, %sil
	jne	.L323
	movswl	%dx, %ecx
	xorl	%r8d, %r8d
	sarl	$5, %ecx
.L334:
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L327:
	andl	$2, %edx
	leaq	10(%r14), %rcx
	je	.L354
.L329:
	movzwl	8(%rax), %edx
	movl	4(%rax), %esi
	movq	%rdi, -56(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movq	-56(%rbp), %rdi
.L326:
	testb	%al, %al
	sete	%al
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L322:
	movl	12(%r14), %ecx
	testb	%sil, %sil
	je	.L325
.L323:
	movzbl	576(%r13), %eax
	notl	%eax
	andl	$1, %eax
	jmp	.L326
.L354:
	movq	24(%r14), %rcx
	jmp	.L329
.L325:
	testl	%ecx, %ecx
	movl	$0, %r8d
	cmovle	%ecx, %r8d
	jns	.L334
	xorl	%r9d, %r9d
	jmp	.L327
.L317:
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L315
	.cfi_endproc
.LFE3019:
	.size	_ZN6icu_6713MessageFormat9getFormatERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6713MessageFormat9getFormatERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat16getStaticClassIDEv
	.type	_ZN6icu_6713MessageFormat16getStaticClassIDEv, @function
_ZN6icu_6713MessageFormat16getStaticClassIDEv:
.LFB2963:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713MessageFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2963:
	.size	_ZN6icu_6713MessageFormat16getStaticClassIDEv, .-_ZN6icu_6713MessageFormat16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721FormatNameEnumeration16getStaticClassIDEv
	.type	_ZN6icu_6721FormatNameEnumeration16getStaticClassIDEv, @function
_ZN6icu_6721FormatNameEnumeration16getStaticClassIDEv:
.LFB2965:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6721FormatNameEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2965:
	.size	_ZN6icu_6721FormatNameEnumeration16getStaticClassIDEv, .-_ZN6icu_6721FormatNameEnumeration16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat16allocateArgTypesEiR10UErrorCode
	.type	_ZN6icu_6713MessageFormat16allocateArgTypesEiR10UErrorCode, @function
_ZN6icu_6713MessageFormat16allocateArgTypesEiR10UErrorCode:
.LFB2993:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L365
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	708(%rdi), %edx
	cmpl	%edx, %esi
	jle	.L357
	cmpl	$9, %esi
	jle	.L363
	addl	%edx, %edx
	cmpl	%esi, %edx
	cmovge	%edx, %esi
	movl	%esi, %r13d
	movslq	%esi, %rsi
	salq	$2, %rsi
.L359:
	movq	696(%rbx), %rdi
	call	uprv_realloc_67@PLT
	testq	%rax, %rax
	je	.L368
	movl	%r13d, 708(%rbx)
	movq	%rax, 696(%rbx)
	movl	$1, %eax
.L357:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$40, %esi
	movl	$10, %r13d
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L368:
	movl	$7, (%r12)
	jmp	.L357
	.cfi_endproc
.LFE2993:
	.size	_ZN6icu_6713MessageFormat16allocateArgTypesEiR10UErrorCode, .-_ZN6icu_6713MessageFormat16allocateArgTypesEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat12resetPatternEv
	.type	_ZN6icu_6713MessageFormat12resetPatternEv, @function
_ZN6icu_6713MessageFormat12resetPatternEv:
.LFB3001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	552(%rdi), %rdi
	subq	$8, %rsp
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	736(%rbx), %rdi
	call	uhash_close_67@PLT
	movq	744(%rbx), %rdi
	movq	$0, 736(%rbx)
	call	uhash_close_67@PLT
	movb	$0, 712(%rbx)
	movq	$0, 744(%rbx)
	movl	$0, 704(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3001:
	.size	_ZN6icu_6713MessageFormat12resetPatternEv, .-_ZN6icu_6713MessageFormat12resetPatternEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat20nextTopLevelArgStartEi
	.type	_ZNK6icu_6713MessageFormat20nextTopLevelArgStartEi, @function
_ZNK6icu_6713MessageFormat20nextTopLevelArgStartEi:
.LFB3004:
	.cfi_startproc
	endbr64
	movq	640(%rdi), %rcx
	movl	%esi, %eax
	testl	%esi, %esi
	je	.L372
	movslq	%esi, %rdx
	salq	$4, %rdx
	movl	12(%rcx,%rdx), %edx
	cmpl	%edx, %esi
	cmovl	%edx, %eax
.L372:
	movslq	%eax, %rdx
	salq	$4, %rdx
	addq	%rcx, %rdx
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L383:
	addq	$16, %rdx
	cmpl	$1, %ecx
	je	.L382
.L374:
	movl	16(%rdx), %ecx
	addl	$1, %eax
	cmpl	$5, %ecx
	jne	.L383
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3004:
	.size	_ZNK6icu_6713MessageFormat20nextTopLevelArgStartEi, .-_ZNK6icu_6713MessageFormat20nextTopLevelArgStartEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat17setArgStartFormatEiPNS_6FormatER10UErrorCode
	.type	_ZN6icu_6713MessageFormat17setArgStartFormatEiPNS_6FormatER10UErrorCode, @function
_ZN6icu_6713MessageFormat17setArgStartFormatEiPNS_6FormatER10UErrorCode:
.LFB3005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	(%rcx), %edx
	testl	%edx, %edx
	jg	.L401
	cmpq	$0, 736(%rdi)
	movq	%rdi, %rbx
	movl	%esi, %r14d
	movq	%rcx, %r13
	je	.L402
.L388:
	testq	%r12, %r12
	je	.L403
.L391:
	movq	736(%rbx), %rdi
	movq	%r13, %rcx
	popq	%rbx
	movq	%r12, %rdx
	movl	%r14d, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uhash_iput_67@PLT
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	leaq	equalFormatsForHash(%rip), %rdx
	call	uhash_open_67@PLT
	movq	%rax, 736(%rbx)
	movq	%rax, %rdi
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L389
	.p2align 4,,10
	.p2align 3
.L401:
	testq	%r12, %r12
	je	.L384
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	8(%rax), %rax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movl	$328, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L391
	xorl	%eax, %eax
	movl	$41, %ecx
	movq	%r12, %rdi
	rep stosq
	movq	%r12, %rdi
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6713MessageFormat11DummyFormatE(%rip), %rax
	movq	%rax, (%r12)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L384:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	call	uhash_setValueDeleter_67@PLT
	jmp	.L388
	.cfi_endproc
.LFE3005:
	.size	_ZN6icu_6713MessageFormat17setArgStartFormatEiPNS_6FormatER10UErrorCode, .-_ZN6icu_6713MessageFormat17setArgStartFormatEiPNS_6FormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat14argNameMatchesEiRKNS_13UnicodeStringEi
	.type	_ZN6icu_6713MessageFormat14argNameMatchesEiRKNS_13UnicodeStringEi, @function
_ZN6icu_6713MessageFormat14argNameMatchesEiRKNS_13UnicodeStringEi:
.LFB3009:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	salq	$4, %rsi
	addq	640(%rdi), %rsi
	cmpl	$8, (%rsi)
	je	.L422
	movswl	10(%rsi), %eax
	cmpl	%ecx, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	movzwl	8(%rdx), %eax
	movl	%eax, %ecx
	andl	$1, %ecx
	testw	%ax, %ax
	js	.L406
	testb	%cl, %cl
	jne	.L407
	movswl	%ax, %r9d
	xorl	%r8d, %r8d
	sarl	$5, %r9d
.L415:
	movl	%r9d, %ecx
	subl	%r8d, %ecx
	cmpl	%r9d, %ecx
	cmovle	%ecx, %r9d
.L411:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	10(%rdx), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	$2, %al
	jne	.L413
	movq	24(%rdx), %rcx
.L413:
	movzwl	8(%rsi), %edx
	movl	4(%rsi), %esi
	addq	$568, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore 6
	movzwl	576(%rdi), %eax
	notl	%eax
	andl	$1, %eax
	testb	%al, %al
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	movl	12(%rdx), %r9d
	testb	%cl, %cl
	jne	.L407
	testl	%r9d, %r9d
	movl	$0, %r8d
	cmovle	%r9d, %r8d
	jns	.L415
	xorl	%r9d, %r9d
	jmp	.L411
	.cfi_endproc
.LFE3009:
	.size	_ZN6icu_6713MessageFormat14argNameMatchesEiRKNS_13UnicodeStringEi, .-_ZN6icu_6713MessageFormat14argNameMatchesEiRKNS_13UnicodeStringEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat23setCustomArgStartFormatEiPNS_6FormatER10UErrorCode
	.type	_ZN6icu_6713MessageFormat23setCustomArgStartFormatEiPNS_6FormatER10UErrorCode, @function
_ZN6icu_6713MessageFormat23setCustomArgStartFormatEiPNS_6FormatER10UErrorCode:
.LFB3010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	(%rcx), %edx
	movq	%rdi, %rbx
	testl	%edx, %edx
	jg	.L441
	cmpq	$0, 736(%rdi)
	je	.L442
.L427:
	testq	%r12, %r12
	je	.L443
.L430:
	movq	736(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%r14d, %esi
	call	uhash_iput_67@PLT
.L426:
	movq	744(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L444
.L431:
	popq	%rbx
	movq	%r13, %rcx
	popq	%r12
	movl	%r14d, %esi
	popq	%r13
	movl	$1, %edx
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uhash_iputi_67@PLT
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	leaq	equalFormatsForHash(%rip), %rdx
	call	uhash_open_67@PLT
	movq	%rax, 736(%rbx)
	movq	%rax, %rdi
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L428
	.p2align 4,,10
	.p2align 3
.L441:
	testq	%r12, %r12
	je	.L426
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movq	744(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L431
.L444:
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	movq	%r13, %rcx
	xorl	%edx, %edx
	call	uhash_open_67@PLT
	movq	%rax, 744(%rbx)
	movq	%rax, %rdi
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L443:
	movl	$328, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L430
	xorl	%eax, %eax
	movl	$41, %ecx
	movq	%r12, %rdi
	rep stosq
	movq	%r12, %rdi
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6713MessageFormat11DummyFormatE(%rip), %rax
	movq	%rax, (%r12)
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L428:
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	call	uhash_setValueDeleter_67@PLT
	jmp	.L427
	.cfi_endproc
.LFE3010:
	.size	_ZN6icu_6713MessageFormat23setCustomArgStartFormatEiPNS_6FormatER10UErrorCode, .-_ZN6icu_6713MessageFormat23setCustomArgStartFormatEiPNS_6FormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat12adoptFormatsEPPNS_6FormatEi
	.type	_ZN6icu_6713MessageFormat12adoptFormatsEPPNS_6FormatEi, @function
_ZN6icu_6713MessageFormat12adoptFormatsEPPNS_6FormatEi:
.LFB3012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -92(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L445
	testl	%edx, %edx
	js	.L445
	movq	%rdi, %r14
	movq	736(%rdi), %rdi
	movq	%rsi, %r15
	testq	%rdi, %rdi
	je	.L447
	call	uhash_removeAll_67@PLT
.L447:
	movq	744(%r14), %rdi
	testq	%rdi, %rdi
	je	.L448
	call	uhash_removeAll_67@PLT
.L448:
	movl	-92(%rbp), %eax
	movl	$0, -60(%rbp)
	testl	%eax, %eax
	je	.L445
	subl	$1, %eax
	movq	640(%r14), %rcx
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	movl	%eax, -96(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-60(%rbp), %rax
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L451:
	movslq	%ebx, %rax
	movl	%r13d, %r12d
	movl	%r13d, %edi
	addq	$1, %rax
	salq	$4, %rax
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L471:
	addq	$16, %rax
	cmpl	$1, %edx
	je	.L452
.L454:
	movl	(%rcx,%rax), %edx
	addl	$1, %ebx
	cmpl	$5, %edx
	jne	.L471
	movq	(%r15,%r13,8), %rdx
	movq	-88(%rbp), %rcx
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%rax, -72(%rbp)
	call	_ZN6icu_6713MessageFormat23setCustomArgStartFormatEiPNS_6FormatER10UErrorCode
	cmpq	-80(%rbp), %r13
	movq	-72(%rbp), %rax
	leal	1(%r12), %edi
	je	.L445
	movl	-60(%rbp), %edx
	addq	$1, %r13
	testl	%edx, %edx
	jg	.L452
	movq	640(%r14), %rcx
	movl	12(%rcx,%rax), %eax
	cmpl	%eax, %ebx
	cmovl	%eax, %ebx
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L452:
	cmpl	%edi, -92(%rbp)
	jle	.L445
	movl	-96(%rbp), %ebx
	movslq	%edi, %rax
	leaq	(%r15,%rax,8), %r12
	subl	%edi, %ebx
	addq	%rax, %rbx
	leaq	8(%r15,%rbx,8), %rbx
.L456:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L455
	movq	(%rdi), %rax
	call	*8(%rax)
.L455:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L456
.L445:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L472
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L472:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3012:
	.size	_ZN6icu_6713MessageFormat12adoptFormatsEPPNS_6FormatEi, .-_ZN6icu_6713MessageFormat12adoptFormatsEPPNS_6FormatEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat10setFormatsEPPKNS_6FormatEi
	.type	_ZN6icu_6713MessageFormat10setFormatsEPPKNS_6FormatEi, @function
_ZN6icu_6713MessageFormat10setFormatsEPPKNS_6FormatEi:
.LFB3013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L473
	movl	%edx, %ebx
	testl	%edx, %edx
	js	.L473
	movq	%rdi, %r12
	movq	736(%rdi), %rdi
	movq	%rsi, %r14
	testq	%rdi, %rdi
	je	.L475
	call	uhash_removeAll_67@PLT
.L475:
	movq	744(%r12), %rdi
	testq	%rdi, %rdi
	je	.L476
	call	uhash_removeAll_67@PLT
.L476:
	movl	$0, -60(%rbp)
	testl	%ebx, %ebx
	je	.L473
	leal	-1(%rbx), %eax
	movq	640(%r12), %rcx
	xorl	%r15d, %r15d
	leaq	-60(%rbp), %r13
	leaq	(%r14,%rax,8), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L478:
	movslq	%r15d, %rbx
	addq	$1, %rbx
	salq	$4, %rbx
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L503:
	addq	$16, %rbx
	cmpl	$1, %eax
	je	.L473
.L481:
	movl	(%rcx,%rbx), %eax
	addl	$1, %r15d
	cmpl	$5, %eax
	jne	.L503
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.L483
	movq	(%rdx), %rax
	movq	%rdx, %rdi
	call	*32(%rax)
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L504
.L483:
	movq	%r13, %rcx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713MessageFormat23setCustomArgStartFormatEiPNS_6FormatER10UErrorCode
	cmpq	%r14, -72(%rbp)
	je	.L505
	movl	-60(%rbp), %edx
	addq	$8, %r14
	testl	%edx, %edx
	jg	.L479
	movq	640(%r12), %rcx
	movl	12(%rcx,%rbx), %eax
	cmpl	%eax, %r15d
	cmovl	%eax, %r15d
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L504:
	movl	$7, -60(%rbp)
	jmp	.L483
.L505:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L473
.L479:
	leaq	552(%r12), %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	736(%r12), %rdi
	call	uhash_close_67@PLT
	movq	744(%r12), %rdi
	movq	$0, 736(%r12)
	call	uhash_close_67@PLT
	movq	$0, 744(%r12)
	movl	$0, 704(%r12)
	movb	$0, 712(%r12)
	.p2align 4,,10
	.p2align 3
.L473:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L506
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L506:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3013:
	.size	_ZN6icu_6713MessageFormat10setFormatsEPPKNS_6FormatEi, .-_ZN6icu_6713MessageFormat10setFormatsEPPKNS_6FormatEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat9setFormatEiRKNS_6FormatE
	.type	_ZN6icu_6713MessageFormat9setFormatEiRKNS_6FormatE, @function
_ZN6icu_6713MessageFormat9setFormatEiRKNS_6FormatE:
.LFB3018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L507
	movq	640(%rdi), %r8
	movq	%rdi, %r13
	xorl	%r12d, %r12d
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L512:
	movslq	%r12d, %rax
	addq	$1, %rax
	salq	$4, %rax
	addq	%r8, %rax
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L524:
	addq	$16, %rax
	cmpl	$1, %ecx
	je	.L507
.L510:
	movl	(%rax), %ecx
	addl	$1, %r12d
	cmpl	$5, %ecx
	jne	.L524
	cmpl	%edi, %esi
	je	.L525
	movl	12(%rax), %eax
	addl	$1, %edi
	cmpl	%eax, %r12d
	cmovl	%eax, %r12d
	jmp	.L512
.L525:
	movq	(%rdx), %rax
	movq	%rdx, %rdi
	call	*32(%rax)
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L507
	leaq	-28(%rbp), %rcx
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713MessageFormat23setCustomArgStartFormatEiPNS_6FormatER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L507:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L526
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L526:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3018:
	.size	_ZN6icu_6713MessageFormat9setFormatEiRKNS_6FormatE, .-_ZN6icu_6713MessageFormat9setFormatEiRKNS_6FormatE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat9setFormatERKNS_13UnicodeStringERKNS_6FormatER10UErrorCode
	.type	_ZN6icu_6713MessageFormat9setFormatERKNS_13UnicodeStringERKNS_6FormatER10UErrorCode, @function
_ZN6icu_6713MessageFormat9setFormatERKNS_13UnicodeStringERKNS_6FormatER10UErrorCode:
.LFB3020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%rsi, -56(%rbp)
	testl	%r8d, %r8d
	jle	.L557
.L527:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L557:
	.cfi_restore_state
	movq	%rdi, %r14
	movq	%rsi, %rdi
	movq	%rdx, %r13
	movq	%rcx, %r15
	call	_ZN6icu_6714MessagePattern20validateArgumentNameERKNS_13UnicodeStringE@PLT
	movl	%eax, -60(%rbp)
	cmpl	$-1, %eax
	jl	.L529
	leaq	568(%r14), %rax
	movq	640(%r14), %rcx
	xorl	%r12d, %r12d
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L530:
	movslq	%r12d, %rbx
	addq	$1, %rbx
	salq	$4, %rbx
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L558:
	addq	$16, %rbx
	cmpl	$1, %eax
	je	.L527
.L533:
	movl	(%rcx,%rbx), %eax
	movl	%r12d, %edx
	addl	$1, %r12d
	cmpl	$5, %eax
	jne	.L558
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L527
	leal	2(%rdx), %eax
	cltq
	salq	$4, %rax
	addq	%rcx, %rax
	cmpl	$8, (%rax)
	je	.L559
	movswl	10(%rax), %eax
	cmpl	-60(%rbp), %eax
	sete	%al
.L543:
	testb	%al, %al
	je	.L544
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*32(%rax)
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L560
	movq	%r15, %rcx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713MessageFormat23setCustomArgStartFormatEiPNS_6FormatER10UErrorCode
.L544:
	movq	640(%r14), %rcx
	movl	12(%rcx,%rbx), %eax
	cmpl	%eax, %r12d
	cmovl	%eax, %r12d
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L559:
	movq	-56(%rbp), %rdi
	movzwl	8(%rdi), %edx
	movl	%edx, %esi
	andl	$1, %esi
	testw	%dx, %dx
	js	.L535
	testb	%sil, %sil
	jne	.L536
	movswl	%dx, %ecx
	xorl	%r8d, %r8d
	sarl	$5, %ecx
.L547:
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L540:
	movq	-56(%rbp), %rsi
	andl	$2, %edx
	leaq	10(%rsi), %rcx
	jne	.L542
	movq	24(%rsi), %rcx
.L542:
	movzwl	8(%rax), %edx
	movl	4(%rax), %esi
	movq	-72(%rbp), %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
.L539:
	testb	%al, %al
	sete	%al
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L536:
	movzbl	576(%r14), %eax
	notl	%eax
	andl	$1, %eax
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L535:
	movl	12(%rdi), %ecx
	testb	%sil, %sil
	jne	.L536
	testl	%ecx, %ecx
	movl	$0, %r8d
	cmovle	%ecx, %r8d
	jns	.L547
	xorl	%r9d, %r9d
	jmp	.L540
.L529:
	movl	$1, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L560:
	.cfi_restore_state
	movl	$7, (%r15)
	jmp	.L527
	.cfi_endproc
.LFE3020:
	.size	_ZN6icu_6713MessageFormat9setFormatERKNS_13UnicodeStringERKNS_6FormatER10UErrorCode, .-_ZN6icu_6713MessageFormat9setFormatERKNS_13UnicodeStringERKNS_6FormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat11adoptFormatEiPNS_6FormatE
	.type	_ZN6icu_6713MessageFormat11adoptFormatEiPNS_6FormatE, @function
_ZN6icu_6713MessageFormat11adoptFormatEiPNS_6FormatE:
.LFB3014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L562
	movq	640(%rdi), %r10
	movl	%esi, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L566:
	movslq	%esi, %rax
	addq	$1, %rax
	salq	$4, %rax
	addq	%r10, %rax
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L578:
	addq	$16, %rax
	cmpl	$1, %ecx
	je	.L562
.L564:
	movl	(%rax), %ecx
	addl	$1, %esi
	cmpl	$5, %ecx
	jne	.L578
	cmpl	%r8d, %r9d
	je	.L579
	movl	12(%rax), %eax
	addl	$1, %r8d
	cmpl	%eax, %esi
	cmovl	%eax, %esi
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L562:
	testq	%rdx, %rdx
	je	.L561
	movq	(%rdx), %rax
	movq	%rdx, %rdi
	call	*8(%rax)
.L561:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L580
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L579:
	.cfi_restore_state
	leaq	-12(%rbp), %rcx
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713MessageFormat23setCustomArgStartFormatEiPNS_6FormatER10UErrorCode
	jmp	.L561
.L580:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3014:
	.size	_ZN6icu_6713MessageFormat11adoptFormatEiPNS_6FormatE, .-_ZN6icu_6713MessageFormat11adoptFormatEiPNS_6FormatE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat11adoptFormatERKNS_13UnicodeStringEPNS_6FormatER10UErrorCode
	.type	_ZN6icu_6713MessageFormat11adoptFormatERKNS_13UnicodeStringEPNS_6FormatER10UErrorCode, @function
_ZN6icu_6713MessageFormat11adoptFormatERKNS_13UnicodeStringEPNS_6FormatER10UErrorCode:
.LFB3017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	movl	(%rcx), %edx
	movq	%rsi, -56(%rbp)
	testl	%edx, %edx
	jle	.L604
.L582:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L581
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L604:
	.cfi_restore_state
	movq	%rdi, %r14
	movq	%rsi, %rdi
	movq	%rcx, %r12
	call	_ZN6icu_6714MessagePattern20validateArgumentNameERKNS_13UnicodeStringE@PLT
	movl	%eax, -60(%rbp)
	cmpl	$-1, %eax
	jl	.L583
	movq	640(%r14), %rdx
	movq	-72(%rbp), %r15
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L584:
	movslq	%r13d, %rbx
	addq	$1, %rbx
	salq	$4, %rbx
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L605:
	addq	$16, %rbx
	cmpl	$1, %eax
	je	.L586
.L587:
	movl	(%rdx,%rbx), %eax
	movl	%r13d, %esi
	addl	$1, %r13d
	cmpl	$5, %eax
	jne	.L605
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L586
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rdx
	addl	$2, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713MessageFormat14argNameMatchesEiRKNS_13UnicodeStringEi
	testb	%al, %al
	je	.L589
	testq	%r15, %r15
	je	.L606
.L592:
	movq	%r15, %rdx
	movq	%r12, %rcx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713MessageFormat23setCustomArgStartFormatEiPNS_6FormatER10UErrorCode
	xorl	%r15d, %r15d
.L589:
	movq	640(%r14), %rdx
	movl	12(%rdx,%rbx), %eax
	cmpl	%eax, %r13d
	cmovl	%eax, %r13d
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L606:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L595
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L592
	movl	$7, (%r12)
.L581:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	.cfi_restore_state
	testq	%r15, %r15
	je	.L581
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L595:
	.cfi_restore_state
	movq	%rdi, %r15
	jmp	.L592
.L583:
	movl	$1, (%r12)
	jmp	.L582
	.cfi_endproc
.LFE3017:
	.size	_ZN6icu_6713MessageFormat11adoptFormatERKNS_13UnicodeStringEPNS_6FormatER10UErrorCode, .-_ZN6icu_6713MessageFormat11adoptFormatERKNS_13UnicodeStringEPNS_6FormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat18getCachedFormatterEi
	.type	_ZNK6icu_6713MessageFormat18getCachedFormatterEi, @function
_ZNK6icu_6713MessageFormat18getCachedFormatterEi:
.LFB3011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	736(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L610
	call	uhash_iget_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L610
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713MessageFormat11DummyFormatE(%rip), %rdx
	leaq	_ZTIN6icu_676FormatE(%rip), %rsi
	movq	%rax, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	jne	.L610
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	.cfi_restore_state
	xorl	%r12d, %r12d
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3011:
	.size	_ZNK6icu_6713MessageFormat18getCachedFormatterEi, .-_ZNK6icu_6713MessageFormat18getCachedFormatterEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat10getArgNameEi
	.type	_ZN6icu_6713MessageFormat10getArgNameEi, @function
_ZN6icu_6713MessageFormat10getArgNameEi:
.LFB3022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	addq	$568, %rsi
	salq	$4, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	addq	72(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rdx), %ecx
	movl	4(%rdx), %edx
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L618
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L618:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3022:
	.size	_ZN6icu_6713MessageFormat10getArgNameEi, .-_ZN6icu_6713MessageFormat10getArgNameEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat20getArgFromListByNameEPKNS_11FormattableEPKNS_13UnicodeStringEiRS4_
	.type	_ZNK6icu_6713MessageFormat20getArgFromListByNameEPKNS_11FormattableEPKNS_13UnicodeStringEiRS4_, @function
_ZNK6icu_6713MessageFormat20getArgFromListByNameEPKNS_11FormattableEPKNS_13UnicodeStringEiRS4_:
.LFB3028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	testl	%ecx, %ecx
	jle	.L620
	movq	%r8, %r15
	movq	%rdx, %r12
	leal	-1(%rcx), %ebx
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L637:
	notl	%eax
	andl	$1, %eax
	testb	%al, %al
	je	.L636
.L630:
	leaq	1(%r14), %rax
	addq	$64, %r12
	cmpq	%r14, %rbx
	je	.L620
	movq	%rax, %r14
.L632:
	movzwl	8(%r15), %esi
	testw	%si, %si
	js	.L621
	movswl	%si, %ecx
	sarl	$5, %ecx
.L622:
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L623
	movswl	%ax, %edx
	sarl	$5, %edx
.L624:
	testb	$1, %sil
	jne	.L637
	testl	%ecx, %ecx
	movl	%r13d, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L627
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L627:
	andl	$2, %esi
	leaq	10(%r15), %rcx
	jne	.L629
	movq	24(%r15), %rcx
.L629:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jne	.L630
.L636:
	leaq	0(,%r14,8), %rax
	subq	%r14, %rax
	salq	$4, %rax
	addq	-56(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	.cfi_restore_state
	movl	12(%r12), %edx
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L621:
	movl	12(%r15), %ecx
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L620:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3028:
	.size	_ZNK6icu_6713MessageFormat20getArgFromListByNameEPKNS_11FormattableEPKNS_13UnicodeStringEiRS4_, .-_ZNK6icu_6713MessageFormat20getArgFromListByNameEPKNS_11FormattableEPKNS_13UnicodeStringEiRS4_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat33getLiteralStringUntilNextArgumentEi
	.type	_ZNK6icu_6713MessageFormat33getLiteralStringUntilNextArgumentEi, @function
_ZNK6icu_6713MessageFormat33getLiteralStringUntilNextArgumentEi:
.LFB3038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movslq	%edx, %r15
	pushq	%r14
	salq	$4, %r15
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	568(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	640(%rsi), %rbx
	movq	%rsi, -56(%rbp)
	movzwl	8(%rbx,%r15), %edx
	addl	4(%rbx,%r15), %edx
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	addq	$16, %r15
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L642:
	movq	-56(%rbp), %rax
	movzwl	8(%rbx), %edx
	addl	4(%rbx), %edx
	movq	640(%rax), %rbx
.L640:
	addq	%r15, %rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	$16, %r15
	movl	(%rbx), %r14d
	movl	4(%rbx), %ecx
	andl	$-5, %r14d
	subl	%edx, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	cmpl	$1, %r14d
	jne	.L642
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3038:
	.size	_ZNK6icu_6713MessageFormat33getLiteralStringUntilNextArgumentEi, .-_ZNK6icu_6713MessageFormat33getLiteralStringUntilNextArgumentEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat14updateMetaDataERNS_17AppendableWrapperEiPNS_13FieldPositionEPKNS_11FormattableE
	.type	_ZNK6icu_6713MessageFormat14updateMetaDataERNS_17AppendableWrapperEiPNS_13FieldPositionEPKNS_11FormattableE, @function
_ZNK6icu_6713MessageFormat14updateMetaDataERNS_17AppendableWrapperEiPNS_13FieldPositionEPKNS_11FormattableE:
.LFB3039:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3039:
	.size	_ZNK6icu_6713MessageFormat14updateMetaDataERNS_17AppendableWrapperEiPNS_13FieldPositionEPKNS_11FormattableE, .-_ZNK6icu_6713MessageFormat14updateMetaDataERNS_17AppendableWrapperEiPNS_13FieldPositionEPKNS_11FormattableE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat19findOtherSubMessageEi
	.type	_ZNK6icu_6713MessageFormat19findOtherSubMessageEi, @function
_ZNK6icu_6713MessageFormat19findOtherSubMessageEi:
.LFB3040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movslq	%esi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r12, %rbx
	subq	$120, %rsp
	movl	648(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	salq	$4, %rax
	addq	640(%rdi), %rax
	movl	(%rax), %eax
	subl	$12, %eax
	cmpl	$1, %eax
	ja	.L645
	addl	$1, %ebx
	movslq	%ebx, %r12
.L645:
	leaq	_ZL12OTHER_STRING(%rip), %rax
	leaq	-136(%rbp), %rdx
	movl	$5, %ecx
	xorl	%esi, %esi
	movq	%rax, -136(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-118(%rbp), %rdi
	movq	640(%r14), %rax
	movq	%rdi, -152(%rbp)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L670:
	testb	%cl, %cl
	jne	.L649
	movswl	%dx, %r9d
	xorl	%r8d, %r8d
	sarl	$5, %r9d
.L659:
	movl	%r9d, %ecx
	subl	%r8d, %ecx
	cmpl	%r9d, %ecx
	cmovle	%ecx, %r9d
.L653:
	andl	$2, %edx
	movl	4(%rax), %esi
	movq	-152(%rbp), %rcx
	leaq	568(%r14), %rdi
	cmove	-104(%rbp), %rcx
	movzwl	8(%rax), %edx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
.L652:
	testb	%al, %al
	je	.L647
	movq	640(%r14), %rax
	leaq	16(%rax,%r12), %rcx
	movl	(%rcx), %esi
	leal	-12(%rsi), %edx
	cmpl	$1, %edx
	jbe	.L669
	cmpl	%r15d, 12(%rcx)
	movl	%r15d, %ebx
	cmovge	12(%rcx), %ebx
	addl	$1, %ebx
	cmpl	%r13d, %ebx
	jge	.L657
.L656:
	movslq	%ebx, %r12
.L658:
	salq	$4, %r12
	leal	1(%rbx), %r15d
	addq	%r12, %rax
	cmpl	$6, (%rax)
	je	.L657
	movzwl	-120(%rbp), %edx
	movl	%edx, %ecx
	andl	$1, %ecx
	testw	%dx, %dx
	jns	.L670
	movl	-116(%rbp), %r9d
	testb	%cl, %cl
	jne	.L649
	testl	%r9d, %r9d
	movl	$0, %r8d
	cmovle	%r9d, %r8d
	jns	.L659
	xorl	%r9d, %r9d
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L649:
	movzbl	576(%r14), %eax
	notl	%eax
	andl	$1, %eax
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L669:
	addl	$2, %ebx
	cmpl	%ebx, 44(%rax,%r12)
	cmovge	44(%rax,%r12), %ebx
	addl	$1, %ebx
	cmpl	%ebx, %r13d
	jg	.L656
	.p2align 4,,10
	.p2align 3
.L657:
	xorl	%r15d, %r15d
.L647:
	movq	-160(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L671
	addq	$120, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L671:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3040:
	.size	_ZNK6icu_6713MessageFormat19findOtherSubMessageEi, .-_ZNK6icu_6713MessageFormat19findOtherSubMessageEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat24findFirstPluralNumberArgEiRKNS_13UnicodeStringE
	.type	_ZNK6icu_6713MessageFormat24findFirstPluralNumberArgEiRKNS_13UnicodeStringE, @function
_ZNK6icu_6713MessageFormat24findFirstPluralNumberArgEiRKNS_13UnicodeStringE:
.LFB3041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leal	1(%rsi), %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%r12d, %rbx
	salq	$4, %rbx
	subq	$8, %rsp
	movq	640(%rdi), %rcx
	leaq	(%rcx,%rbx), %rdx
	movl	(%rdx), %eax
	cmpl	$1, %eax
	jne	.L673
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L675:
	addl	$1, %r12d
	movslq	%r12d, %rbx
	salq	$4, %rbx
	leaq	(%rcx,%rbx), %rdx
	movl	(%rdx), %eax
	cmpl	$1, %eax
	je	.L683
.L673:
	cmpl	$4, %eax
	je	.L685
	cmpl	$5, %eax
	jne	.L675
	movswl	8(%r13), %eax
	movswl	10(%rdx), %edx
	movl	%eax, %esi
	sarl	$5, %eax
	cmpl	$1, %edx
	ja	.L676
	testl	%eax, %eax
	jne	.L706
.L676:
	movl	12(%rcx,%rbx), %eax
	cmpl	%eax, %r12d
	cmovl	%eax, %r12d
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L706:
	movl	%esi, %edx
	andl	$1, %edx
	testw	%si, %si
	jns	.L705
	movl	12(%r13), %eax
.L705:
	testb	%dl, %dl
	jne	.L707
	testl	%eax, %eax
	movl	%r15d, %r8d
	movl	$0, %r9d
	cmovle	%eax, %r8d
	js	.L680
	movl	%eax, %r9d
	subl	%r8d, %r9d
	cmpl	%eax, %r9d
	cmovg	%eax, %r9d
.L680:
	andl	$2, %esi
	leaq	10(%r13), %r10
	jne	.L682
	movq	24(%r13), %r10
.L682:
	leal	1(%r12), %eax
	leaq	568(%r14), %rdi
	cltq
	salq	$4, %rax
	addq	%rax, %rcx
	movzwl	8(%rcx), %edx
	movl	4(%rcx), %esi
	movq	%r10, %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
.L679:
	testb	%al, %al
	je	.L672
	movq	640(%r14), %rcx
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L707:
	movzbl	576(%r14), %eax
	notl	%eax
	andl	$1, %eax
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L683:
	xorl	%r12d, %r12d
.L672:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L685:
	.cfi_restore_state
	movl	$-1, %r12d
	jmp	.L672
	.cfi_endproc
.LFE3041:
	.size	_ZNK6icu_6713MessageFormat24findFirstPluralNumberArgEiRKNS_13UnicodeStringE, .-_ZNK6icu_6713MessageFormat24findFirstPluralNumberArgEiRKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat11copyObjectsERKS0_R10UErrorCode
	.type	_ZN6icu_6713MessageFormat11copyObjectsERKS0_R10UErrorCode, @function
_ZN6icu_6713MessageFormat11copyObjectsERKS0_R10UErrorCode:
.LFB3042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movslq	704(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edx, 704(%rdi)
	testl	%edx, %edx
	jle	.L709
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L708
	movl	708(%rdi), %eax
	movq	696(%rdi), %rdi
	cmpl	%eax, %edx
	jle	.L711
	cmpl	$9, %edx
	jle	.L727
	addl	%eax, %eax
	cmpl	%edx, %eax
	cmovge	%eax, %edx
	movslq	%edx, %rsi
	movq	%rsi, %r14
	salq	$2, %rsi
.L712:
	call	uprv_realloc_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L722
	movq	%rax, 696(%r12)
	movslq	704(%r12), %rdx
	movl	%r14d, 708(%r12)
.L711:
	movq	696(%r13), %rsi
	salq	$2, %rdx
	call	memcpy@PLT
.L709:
	movq	736(%r12), %rdi
	testq	%rdi, %rdi
	je	.L715
	call	uhash_removeAll_67@PLT
.L715:
	movq	744(%r12), %rdi
	testq	%rdi, %rdi
	je	.L716
	call	uhash_removeAll_67@PLT
.L716:
	movq	736(%r13), %rdi
	testq	%rdi, %rdi
	je	.L717
	cmpq	$0, 736(%r12)
	je	.L747
.L718:
	call	uhash_count_67@PLT
	leaq	-60(%rbp), %rcx
	xorl	%r14d, %r14d
	movl	$-1, -60(%rbp)
	movl	%eax, -76(%rbp)
	movq	%rcx, -72(%rbp)
	testl	%eax, %eax
	jg	.L720
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L748:
	movq	736(%r13), %rdi
	movq	-72(%rbp), %rsi
	call	uhash_nextElement_67@PLT
	movq	8(%rax), %rdi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L722
	movl	16(%r15), %esi
	movq	736(%r12), %rdi
	movq	%rbx, %rcx
	addl	$1, %r14d
	call	uhash_iput_67@PLT
	cmpl	%r14d, -76(%rbp)
	je	.L717
.L720:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L748
.L717:
	movq	744(%r13), %rdi
	testq	%rdi, %rdi
	je	.L708
	cmpq	$0, 744(%r12)
	je	.L749
.L723:
	call	uhash_count_67@PLT
	xorl	%r14d, %r14d
	leaq	-60(%rbp), %r15
	movl	$-1, -60(%rbp)
	movl	%eax, -72(%rbp)
	testl	%eax, %eax
	jg	.L724
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L750:
	movq	744(%r13), %rdi
	movq	%r15, %rsi
	addl	$1, %r14d
	call	uhash_nextElement_67@PLT
	movq	744(%r12), %rdi
	movq	%rbx, %rcx
	movl	8(%rax), %edx
	movl	16(%rax), %esi
	call	uhash_iputi_67@PLT
	cmpl	%r14d, -72(%rbp)
	je	.L708
.L724:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L750
	.p2align 4,,10
	.p2align 3
.L708:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L751
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L727:
	.cfi_restore_state
	movl	$40, %esi
	movl	$10, %r14d
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L722:
	movl	$7, (%rbx)
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L747:
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	movq	%rbx, %rcx
	leaq	equalFormatsForHash(%rip), %rdx
	call	uhash_open_67@PLT
	movl	(%rbx), %ecx
	movq	%rax, 736(%r12)
	testl	%ecx, %ecx
	jg	.L708
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	call	uhash_setValueDeleter_67@PLT
	movq	736(%r13), %rdi
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L749:
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	call	uhash_open_67@PLT
	movq	%rax, 744(%r12)
	movq	744(%r13), %rdi
	jmp	.L723
.L751:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3042:
	.size	_ZN6icu_6713MessageFormat11copyObjectsERKS0_R10UErrorCode, .-_ZN6icu_6713MessageFormat11copyObjectsERKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormataSERKS0_
	.type	_ZN6icu_6713MessageFormataSERKS0_, @function
_ZN6icu_6713MessageFormataSERKS0_:
.LFB2994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L753
	movq	%rsi, %r13
	leaq	552(%r12), %r14
	call	_ZN6icu_676FormataSERKS0_@PLT
	movq	(%r12), %rax
	leaq	328(%r13), %rsi
	movq	%r12, %rdi
	call	*64(%rax)
	leaq	552(%r13), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePatternaSERKS0_@PLT
	leaq	-44(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movzbl	712(%r13), %eax
	movl	$0, -44(%rbp)
	movb	%al, 712(%r12)
	call	_ZN6icu_6713MessageFormat11copyObjectsERKS0_R10UErrorCode
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L757
.L753:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L758
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L757:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	736(%r12), %rdi
	call	uhash_close_67@PLT
	movq	744(%r12), %rdi
	movq	$0, 736(%r12)
	call	uhash_close_67@PLT
	movq	$0, 744(%r12)
	movl	$0, 704(%r12)
	movb	$0, 712(%r12)
	jmp	.L753
.L758:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2994:
	.size	_ZN6icu_6713MessageFormataSERKS0_, .-_ZN6icu_6713MessageFormataSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormatC2ERKS0_
	.type	_ZN6icu_6713MessageFormatC2ERKS0_, @function
_ZN6icu_6713MessageFormatC2ERKS0_:
.LFB2987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	552(%rbx), %r13
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676FormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6713MessageFormatE(%rip), %rax
	leaq	328(%r12), %rsi
	movq	%rax, (%rbx)
	leaq	328(%rbx), %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	552(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6714MessagePatternC1ERKS0_@PLT
	pxor	%xmm0, %xmm0
	leaq	-44(%rbp), %rdx
	movq	%r12, %rsi
	movzbl	712(%r12), %eax
	movq	%rbx, %rdi
	movq	$0, 680(%rbx)
	movl	$0, 688(%rbx)
	movb	%al, 712(%rbx)
	leaq	16+_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE(%rip), %rax
	movq	%rax, 752(%rbx)
	movq	%rax, 784(%rbx)
	movq	$0, 696(%rbx)
	movq	$0, 704(%rbx)
	movq	%rbx, 760(%rbx)
	movq	$0, 768(%rbx)
	movl	$0, 776(%rbx)
	movq	%rbx, 792(%rbx)
	movq	$0, 800(%rbx)
	movl	$1, 808(%rbx)
	movups	%xmm0, 720(%rbx)
	movups	%xmm0, 736(%rbx)
	movl	$0, -44(%rbp)
	call	_ZN6icu_6713MessageFormat11copyObjectsERKS0_R10UErrorCode
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L759
	movq	%r13, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	736(%rbx), %rdi
	call	uhash_close_67@PLT
	movq	744(%rbx), %rdi
	movq	$0, 736(%rbx)
	call	uhash_close_67@PLT
	movb	$0, 712(%rbx)
	movq	$0, 744(%rbx)
	movl	$0, 704(%rbx)
.L759:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L763
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L763:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2987:
	.size	_ZN6icu_6713MessageFormatC2ERKS0_, .-_ZN6icu_6713MessageFormatC2ERKS0_
	.globl	_ZN6icu_6713MessageFormatC1ERKS0_
	.set	_ZN6icu_6713MessageFormatC1ERKS0_,_ZN6icu_6713MessageFormatC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat5cloneEv
	.type	_ZNK6icu_6713MessageFormat5cloneEv, @function
_ZNK6icu_6713MessageFormat5cloneEv:
.LFB2996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$816, %edi
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L764
	movq	%rax, %rdi
	movq	%r13, %rsi
	leaq	552(%r12), %r14
	call	_ZN6icu_676FormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6713MessageFormatE(%rip), %rax
	leaq	328(%r13), %rsi
	movq	%rax, (%r12)
	leaq	328(%r12), %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	552(%r13), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePatternC1ERKS0_@PLT
	pxor	%xmm0, %xmm0
	leaq	-44(%rbp), %rdx
	movq	%r13, %rsi
	movzbl	712(%r13), %eax
	movq	%r12, %rdi
	movq	$0, 680(%r12)
	movl	$0, 688(%r12)
	movb	%al, 712(%r12)
	leaq	16+_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE(%rip), %rax
	movq	%rax, 752(%r12)
	movq	%rax, 784(%r12)
	movq	$0, 696(%r12)
	movq	$0, 704(%r12)
	movq	%r12, 760(%r12)
	movq	$0, 768(%r12)
	movl	$0, 776(%r12)
	movq	%r12, 792(%r12)
	movq	$0, 800(%r12)
	movl	$1, 808(%r12)
	movups	%xmm0, 720(%r12)
	movups	%xmm0, 736(%r12)
	movl	$0, -44(%rbp)
	call	_ZN6icu_6713MessageFormat11copyObjectsERKS0_R10UErrorCode
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L764
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	736(%r12), %rdi
	call	uhash_close_67@PLT
	movq	744(%r12), %rdi
	movq	$0, 736(%r12)
	call	uhash_close_67@PLT
	movq	$0, 744(%r12)
	movl	$0, 704(%r12)
	movb	$0, 712(%r12)
.L764:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L772
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L772:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2996:
	.size	_ZNK6icu_6713MessageFormat5cloneEv, .-_ZNK6icu_6713MessageFormat5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat5parseEiRKNS_13UnicodeStringERNS_13ParsePositionERiR10UErrorCode
	.type	_ZNK6icu_6713MessageFormat5parseEiRKNS_13UnicodeStringERNS_13ParsePositionERiR10UErrorCode, @function
_ZNK6icu_6713MessageFormat5parseEiRKNS_13UnicodeStringERNS_13ParsePositionERiR10UErrorCode:
.LFB3043:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	movl	$0, (%r8)
	testl	%eax, %eax
	jg	.L777
	cmpb	$0, 676(%rdi)
	jne	.L778
	jmp	_ZNK6icu_6713MessageFormat5parseEiRKNS_13UnicodeStringERNS_13ParsePositionERiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L778:
	movl	$65804, (%r9)
.L777:
	movl	8(%rcx), %eax
	movl	%eax, 12(%rcx)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3043:
	.size	_ZNK6icu_6713MessageFormat5parseEiRKNS_13UnicodeStringERNS_13ParsePositionERiR10UErrorCode, .-_ZNK6icu_6713MessageFormat5parseEiRKNS_13UnicodeStringERNS_13ParsePositionERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat19autoQuoteApostropheERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6713MessageFormat19autoQuoteApostropheERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6713MessageFormat19autoQuoteApostropheERKNS_13UnicodeStringER10UErrorCode:
.LFB3049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movw	%cx, 8(%rdi)
	movl	(%rdx), %edi
	testl	%edi, %edi
	jle	.L790
.L780:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L779:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L790:
	.cfi_restore_state
	movzwl	8(%rsi), %eax
	movq	%rdx, %rbx
	testw	%ax, %ax
	js	.L781
	movswl	%ax, %r14d
	sarl	$5, %r14d
.L782:
	testb	$17, %al
	jne	.L788
	leaq	10(%rsi), %r13
	testb	$2, %al
	je	.L791
.L783:
	leal	1(%r14,%r14), %r15d
	movq	%r12, %rdi
	movl	%r15d, %esi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L792
	movq	%rbx, %r8
	movl	%r15d, %ecx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	umsg_autoQuoteApostrophe_67@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	(%rbx), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L779
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L791:
	movq	24(%rsi), %r13
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L792:
	movl	$7, (%rbx)
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L781:
	movl	12(%rsi), %r14d
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L788:
	xorl	%r13d, %r13d
	jmp	.L783
	.cfi_endproc
.LFE3049:
	.size	_ZN6icu_6713MessageFormat19autoQuoteApostropheERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6713MessageFormat19autoQuoteApostropheERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat19createIntegerFormatERKNS_6LocaleER10UErrorCode
	.type	_ZNK6icu_6713MessageFormat19createIntegerFormatERKNS_6LocaleER10UErrorCode, @function
_ZNK6icu_6713MessageFormat19createIntegerFormatERKNS_6LocaleER10UErrorCode:
.LFB3063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L793
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	movq	%rax, %rdi
	call	__dynamic_cast@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L793
	movq	(%rax), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*232(%rax)
	movq	(%r12), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*528(%rax)
	movq	(%r12), %rax
	movl	$1, %esi
	movq	%r12, %rdi
	call	*184(%rax)
.L793:
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3063:
	.size	_ZNK6icu_6713MessageFormat19createIntegerFormatERKNS_6LocaleER10UErrorCode, .-_ZNK6icu_6713MessageFormat19createIntegerFormatERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat22getDefaultNumberFormatER10UErrorCode
	.type	_ZNK6icu_6713MessageFormat22getDefaultNumberFormatER10UErrorCode, @function
_ZNK6icu_6713MessageFormat22getDefaultNumberFormatER10UErrorCode:
.LFB3064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	720(%rdi), %r12
	testq	%r12, %r12
	je	.L811
.L802:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L811:
	.cfi_restore_state
	movq	%rdi, %rbx
	leaq	328(%rdi), %rdi
	movq	%rsi, %r13
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 720(%rbx)
	movq	%rax, %rdi
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L804
	testq	%rdi, %rdi
	je	.L805
	movq	(%rdi), %rax
	call	*8(%rax)
.L805:
	movq	$0, 720(%rbx)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L804:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L812
	movq	%rdi, %r12
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L812:
	movl	$7, 0(%r13)
	jmp	.L802
	.cfi_endproc
.LFE3064:
	.size	_ZNK6icu_6713MessageFormat22getDefaultNumberFormatER10UErrorCode, .-_ZNK6icu_6713MessageFormat22getDefaultNumberFormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat20getDefaultDateFormatER10UErrorCode
	.type	_ZNK6icu_6713MessageFormat20getDefaultDateFormatER10UErrorCode, @function
_ZNK6icu_6713MessageFormat20getDefaultDateFormatER10UErrorCode:
.LFB3065:
	.cfi_startproc
	endbr64
	movq	728(%rdi), %rax
	testq	%rax, %rax
	je	.L819
	ret
	.p2align 4,,10
	.p2align 3
.L819:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	328(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	$3, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$3, %edi
	call	_ZN6icu_6710DateFormat22createDateTimeInstanceENS0_6EStyleES1_RKNS_6LocaleE@PLT
	movq	%rax, 728(%rbx)
	testq	%rax, %rax
	je	.L820
.L813:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L820:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L813
	.cfi_endproc
.LFE3065:
	.size	_ZNK6icu_6713MessageFormat20getDefaultDateFormatER10UErrorCode, .-_ZNK6icu_6713MessageFormat20getDefaultDateFormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat18usesNamedArgumentsEv
	.type	_ZNK6icu_6713MessageFormat18usesNamedArgumentsEv, @function
_ZNK6icu_6713MessageFormat18usesNamedArgumentsEv:
.LFB3066:
	.cfi_startproc
	endbr64
	movzbl	676(%rdi), %eax
	ret
	.cfi_endproc
.LFE3066:
	.size	_ZNK6icu_6713MessageFormat18usesNamedArgumentsEv, .-_ZNK6icu_6713MessageFormat18usesNamedArgumentsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat15getArgTypeCountEv
	.type	_ZNK6icu_6713MessageFormat15getArgTypeCountEv, @function
_ZNK6icu_6713MessageFormat15getArgTypeCountEv:
.LFB3067:
	.cfi_startproc
	endbr64
	movl	704(%rdi), %eax
	ret
	.cfi_endproc
.LFE3067:
	.size	_ZNK6icu_6713MessageFormat15getArgTypeCountEv, .-_ZNK6icu_6713MessageFormat15getArgTypeCountEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat12equalFormatsEPKvS2_
	.type	_ZN6icu_6713MessageFormat12equalFormatsEPKvS2_, @function
_ZN6icu_6713MessageFormat12equalFormatsEPKvS2_:
.LFB3068:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE3068:
	.size	_ZN6icu_6713MessageFormat12equalFormatsEPKvS2_, .-_ZN6icu_6713MessageFormat12equalFormatsEPKvS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721FormatNameEnumerationC2EPNS_7UVectorER10UErrorCode
	.type	_ZN6icu_6721FormatNameEnumerationC2EPNS_7UVectorER10UErrorCode, @function
_ZN6icu_6721FormatNameEnumerationC2EPNS_7UVectorER10UErrorCode:
.LFB3076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	leaq	16+_ZTVN6icu_6721FormatNameEnumerationE(%rip), %rax
	movq	%r12, 120(%rbx)
	movq	%rax, (%rbx)
	movl	$0, 116(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3076:
	.size	_ZN6icu_6721FormatNameEnumerationC2EPNS_7UVectorER10UErrorCode, .-_ZN6icu_6721FormatNameEnumerationC2EPNS_7UVectorER10UErrorCode
	.globl	_ZN6icu_6721FormatNameEnumerationC1EPNS_7UVectorER10UErrorCode
	.set	_ZN6icu_6721FormatNameEnumerationC1EPNS_7UVectorER10UErrorCode,_ZN6icu_6721FormatNameEnumerationC2EPNS_7UVectorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat22PluralSelectorProviderC2ERKS0_11UPluralType
	.type	_ZN6icu_6713MessageFormat22PluralSelectorProviderC2ERKS0_11UPluralType, @function
_ZN6icu_6713MessageFormat22PluralSelectorProviderC2ERKS0_11UPluralType:
.LFB3086:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 16(%rdi)
	movl	%edx, 24(%rdi)
	ret
	.cfi_endproc
.LFE3086:
	.size	_ZN6icu_6713MessageFormat22PluralSelectorProviderC2ERKS0_11UPluralType, .-_ZN6icu_6713MessageFormat22PluralSelectorProviderC2ERKS0_11UPluralType
	.globl	_ZN6icu_6713MessageFormat22PluralSelectorProviderC1ERKS0_11UPluralType
	.set	_ZN6icu_6713MessageFormat22PluralSelectorProviderC1ERKS0_11UPluralType,_ZN6icu_6713MessageFormat22PluralSelectorProviderC2ERKS0_11UPluralType
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat22PluralSelectorProvider5resetEv
	.type	_ZN6icu_6713MessageFormat22PluralSelectorProvider5resetEv, @function
_ZN6icu_6713MessageFormat22PluralSelectorProvider5resetEv:
.LFB3093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L828
	movq	(%rdi), %rax
	call	*8(%rax)
.L828:
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3093:
	.size	_ZN6icu_6713MessageFormat22PluralSelectorProvider5resetEv, .-_ZN6icu_6713MessageFormat22PluralSelectorProvider5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormatD2Ev
	.type	_ZN6icu_6713MessageFormatD2Ev, @function
_ZN6icu_6713MessageFormatD2Ev:
.LFB2990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713MessageFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	736(%rdi), %rdi
	call	uhash_close_67@PLT
	movq	744(%r12), %rdi
	call	uhash_close_67@PLT
	movq	696(%r12), %rdi
	call	uprv_free_67@PLT
	movq	680(%r12), %rdi
	call	uprv_free_67@PLT
	movq	720(%r12), %rdi
	testq	%rdi, %rdi
	je	.L834
	movq	(%rdi), %rax
	call	*8(%rax)
.L834:
	movq	728(%r12), %rdi
	testq	%rdi, %rdi
	je	.L835
	movq	(%rdi), %rax
	call	*8(%rax)
.L835:
	movq	800(%r12), %rdi
	leaq	16+_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE(%rip), %rbx
	leaq	784(%r12), %r13
	movq	%rbx, 784(%r12)
	testq	%rdi, %rdi
	je	.L836
	movq	(%rdi), %rax
	call	*8(%rax)
.L836:
	movq	%r13, %rdi
	leaq	752(%r12), %r13
	call	_ZN6icu_6712PluralFormat14PluralSelectorD2Ev@PLT
	movq	768(%r12), %rdi
	movq	%rbx, 752(%r12)
	testq	%rdi, %rdi
	je	.L837
	movq	(%rdi), %rax
	call	*8(%rax)
.L837:
	movq	%r13, %rdi
	call	_ZN6icu_6712PluralFormat14PluralSelectorD2Ev@PLT
	leaq	552(%r12), %rdi
	call	_ZN6icu_6714MessagePatternD1Ev@PLT
	leaq	328(%r12), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676FormatD2Ev@PLT
	.cfi_endproc
.LFE2990:
	.size	_ZN6icu_6713MessageFormatD2Ev, .-_ZN6icu_6713MessageFormatD2Ev
	.globl	_ZN6icu_6713MessageFormatD1Ev
	.set	_ZN6icu_6713MessageFormatD1Ev,_ZN6icu_6713MessageFormatD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormatD0Ev
	.type	_ZN6icu_6713MessageFormatD0Ev, @function
_ZN6icu_6713MessageFormatD0Ev:
.LFB2992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713MessageFormatD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2992:
	.size	_ZN6icu_6713MessageFormatD0Ev, .-_ZN6icu_6713MessageFormatD0Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat11findKeywordERKNS_13UnicodeStringEPKPKDs
	.type	_ZN6icu_6713MessageFormat11findKeywordERKNS_13UnicodeStringEPKPKDs, @function
_ZN6icu_6713MessageFormat11findKeywordERKNS_13UnicodeStringEPKPKDs:
.LFB3062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movswl	8(%rdi), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r12d, %eax
	sarl	$5, %r12d
	je	.L853
	movq	%rsi, %rbx
	testw	%ax, %ax
	js	.L876
	movl	%r12d, -364(%rbp)
	testb	$17, %al
	jne	.L864
.L880:
	testb	$2, %al
	jne	.L877
	movq	24(%rdi), %rdi
.L856:
	leaq	-364(%rbp), %rsi
	leaq	-352(%rbp), %r13
	call	_ZN6icu_6712PatternProps14trimWhiteSpaceEPKDsRi@PLT
	movl	-364(%rbp), %ecx
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	-360(%rbp), %rdx
	movq	%rax, -360(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-288(%rbp), %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7toLowerERKNS_6LocaleE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L858
	xorl	%r14d, %r14d
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L878:
	sarl	$5, %edx
.L860:
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	je	.L861
	addq	$1, %r14
	movq	(%rbx,%r14,8), %rdi
	testq	%rdi, %rdi
	je	.L858
.L862:
	call	u_strlen_67@PLT
	movswl	-344(%rbp), %edx
	movq	(%rbx,%r14,8), %r15
	movl	%r14d, %r12d
	movl	%eax, %r9d
	testw	%dx, %dx
	jns	.L878
	movl	-340(%rbp), %edx
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L858:
	movl	$-1, %r12d
.L861:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L853:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L879
	addq	$328, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L877:
	.cfi_restore_state
	addq	$10, %rdi
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L876:
	movl	12(%rdi), %r12d
	movl	%r12d, -364(%rbp)
	testb	$17, %al
	je	.L880
.L864:
	xorl	%edi, %edi
	jmp	.L856
.L879:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3062:
	.size	_ZN6icu_6713MessageFormat11findKeywordERKNS_13UnicodeStringEPKPKDs, .-_ZN6icu_6713MessageFormat11findKeywordERKNS_13UnicodeStringEPKPKDs
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormateqERKNS_6FormatE
	.type	_ZNK6icu_6713MessageFormateqERKNS_6FormatE, @function
_ZNK6icu_6713MessageFormateqERKNS_6FormatE:
.LFB2995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L881
	movq	%rdi, %r13
	movq	%rsi, %rbx
	call	_ZNK6icu_676FormateqERKS0_@PLT
	testb	%al, %al
	jne	.L883
.L885:
	xorl	%eax, %eax
.L881:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L903
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L883:
	.cfi_restore_state
	leaq	552(%rbx), %rsi
	leaq	552(%r13), %rdi
	call	_ZNK6icu_6714MessagePatterneqERKS0_@PLT
	testb	%al, %al
	je	.L885
	leaq	328(%rbx), %rsi
	leaq	328(%r13), %rdi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	je	.L885
	movq	744(%r13), %rdi
	testq	%rdi, %rdi
	sete	%cl
	cmpq	$0, 744(%rbx)
	sete	%al
	cmpb	%al, %cl
	jne	.L885
	testq	%rdi, %rdi
	je	.L888
	call	uhash_count_67@PLT
	movq	744(%rbx), %rdi
	movl	%eax, %r15d
	movl	%eax, -68(%rbp)
	call	uhash_count_67@PLT
	cmpl	%eax, %r15d
	jne	.L885
	movl	$-1, -64(%rbp)
	movl	$-1, -60(%rbp)
	testl	%r15d, %r15d
	jle	.L888
	leaq	-64(%rbp), %rax
	xorl	%r12d, %r12d
	movq	%rax, -80(%rbp)
	leaq	-60(%rbp), %rax
	movq	%rax, -88(%rbp)
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L904:
	movq	736(%r13), %rdi
	call	uhash_iget_67@PLT
	movl	16(%r15), %esi
	movq	736(%rbx), %rdi
	movq	%rax, %r14
	call	uhash_iget_67@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	(%r14), %rax
	call	*24(%rax)
	testb	%al, %al
	je	.L885
	addl	$1, %r12d
	cmpl	%r12d, -68(%rbp)
	je	.L888
.L889:
	movq	744(%r13), %rdi
	movq	-80(%rbp), %rsi
	call	uhash_nextElement_67@PLT
	movq	-88(%rbp), %rsi
	movq	744(%rbx), %rdi
	movq	%rax, %r14
	call	uhash_nextElement_67@PLT
	movl	16(%r14), %esi
	movq	%rax, %r15
	cmpl	16(%rax), %esi
	je	.L904
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L888:
	movl	$1, %eax
	jmp	.L881
.L903:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2995:
	.size	_ZNK6icu_6713MessageFormateqERKNS_6FormatE, .-_ZNK6icu_6713MessageFormateqERKNS_6FormatE
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC1:
	.string	":"
	.string	":"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6713MessageFormat23createAppropriateFormatERNS_13UnicodeStringES2_RNS_11Formattable4TypeER11UParseErrorR10UErrorCode.part.0, @function
_ZN6icu_6713MessageFormat23createAppropriateFormatERNS_13UnicodeStringES2_RNS_11Formattable4TypeER11UParseErrorR10UErrorCode.part.0:
.LFB4378:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	_ZL8TYPE_IDS(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$1016, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713MessageFormat11findKeywordERKNS_13UnicodeStringEPKPKDs
	cmpl	$5, %eax
	ja	.L906
	leaq	.L908(%rip), %rcx
	movl	%eax, %edx
	movl	%eax, %r10d
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L908:
	.long	.L912-.L908
	.long	.L911-.L908
	.long	.L911-.L908
	.long	.L910-.L908
	.long	.L909-.L908
	.long	.L907-.L908
	.text
	.p2align 4,,10
	.p2align 3
.L911:
	movl	$0, (%rbx)
	xorl	%esi, %esi
	movq	%r14, %rdi
	addq	$328, %r12
	movl	%r10d, -1048(%rbp)
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceERKNS_13UnicodeStringEi@PLT
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r14, %rdi
	movl	$2, %r9d
	leaq	.LC1(%rip), %rcx
	movl	%eax, %esi
	movl	%eax, %ebx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movl	-1048(%rbp), %r10d
	testb	%al, %al
	jne	.L921
	leaq	-512(%rbp), %r15
	leal	2(%rbx), %edx
	movl	$2147483647, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r13, %rdx
	call	_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L905:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L963
	addq	$1016, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L910:
	.cfi_restore_state
	movl	$1, (%rbx)
	movl	$752, %edi
	leaq	328(%r12), %r15
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L925
	movq	%r13, %rcx
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6721RuleBasedNumberFormatC1ENS_15URBNFRuleSetTagERKNS_6LocaleER10UErrorCode@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L905
.L961:
	movswl	8(%r14), %eax
	testw	%ax, %ax
	js	.L930
	sarl	$5, %eax
.L931:
	testl	%eax, %eax
	jle	.L905
	leaq	-1028(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$0, -1028(%rbp)
	call	_ZN6icu_6721RuleBasedNumberFormat17setDefaultRuleSetERKNS_13UnicodeStringER10UErrorCode@PLT
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L907:
	movl	$1, (%rbx)
	movl	$752, %edi
	leaq	328(%r12), %r15
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L925
	movq	%rax, %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movl	$2, %esi
	call	_ZN6icu_6721RuleBasedNumberFormatC1ENS_15URBNFRuleSetTagERKNS_6LocaleER10UErrorCode@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L905
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L909:
	movl	$1, (%rbx)
	movl	$752, %edi
	leaq	328(%r12), %r15
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L925
	movq	%r15, %rdx
	movq	%r13, %rcx
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6721RuleBasedNumberFormatC1ENS_15URBNFRuleSetTagERKNS_6LocaleER10UErrorCode@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L905
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L912:
	movl	$1, (%rbx)
	leaq	_ZL16NUMBER_STYLE_IDS(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713MessageFormat11findKeywordERKNS_13UnicodeStringEPKPKDs
	leaq	328(%r12), %r10
	cmpl	$2, %eax
	je	.L913
	jg	.L914
	testl	%eax, %eax
	je	.L915
	cmpl	$1, %eax
	jne	.L917
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZN6icu_6712NumberFormat22createCurrencyInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r12
	jmp	.L905
.L906:
	movl	$3, (%rbx)
	xorl	%r12d, %r12d
	movl	$1, 0(%r13)
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L914:
	cmpl	$3, %eax
	jne	.L917
	movl	$2, (%rbx)
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%r10, %rsi
	call	_ZNK6icu_6713MessageFormat19createIntegerFormatERKNS_6LocaleER10UErrorCode
	movq	%rax, %r12
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L930:
	movl	12(%r14), %eax
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L921:
	movq	%r14, %rdi
	leaq	_ZL14DATE_STYLE_IDS(%rip), %rsi
	movl	%r10d, -1048(%rbp)
	call	_ZN6icu_6713MessageFormat11findKeywordERKNS_13UnicodeStringEPKPKDs
	movl	-1048(%rbp), %r10d
	movl	$2, %edi
	testl	%eax, %eax
	movl	%eax, %ebx
	js	.L922
	cltq
	leaq	_ZL11DATE_STYLES(%rip), %rdx
	movl	(%rdx,%rax,4), %edi
.L922:
	movq	%r12, %rsi
	cmpl	$1, %r10d
	je	.L964
	call	_ZN6icu_6710DateFormat18createTimeInstanceENS0_6EStyleERKNS_6LocaleE@PLT
	movq	%rax, %r12
.L924:
	testl	%ebx, %ebx
	jns	.L905
	testq	%r12, %r12
	je	.L905
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6716SimpleDateFormatE(%rip), %rdx
	leaq	_ZTIN6icu_676FormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L905
	movq	(%rax), %rax
	movq	%r14, %rsi
	call	*256(%rax)
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L913:
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZN6icu_6712NumberFormat21createPercentInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r12
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L917:
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r10, -1048(%rbp)
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceERKNS_13UnicodeStringEi@PLT
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r14, %rdi
	movl	$2, %r9d
	leaq	.LC1(%rip), %rcx
	movl	%eax, %esi
	movl	%eax, %ebx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movq	-1048(%rbp), %r10
	testb	%al, %al
	jne	.L920
	leaq	-1024(%rbp), %r15
	movl	$2147483647, %ecx
	leal	2(%rbx), %edx
	movq	%r14, %rsi
	leaq	-960(%rbp), %r12
	movq	%r15, %rdi
	leaq	-512(%rbp), %r14
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676number15NumberFormatter11forSkeletonERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	-1048(%rbp), %r10
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r10, %rdx
	call	_ZNO6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter8toFormatER10UErrorCode@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	leaq	-744(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-792(%rbp), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-824(%rbp), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	leaq	-920(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	-944(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L915:
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r12
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L964:
	call	_ZN6icu_6710DateFormat18createDateInstanceENS0_6EStyleERKNS_6LocaleE@PLT
	movq	%rax, %r12
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L920:
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L905
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_676FormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L905
	movq	(%rax), %rax
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	*560(%rax)
	jmp	.L905
.L963:
	call	__stack_chk_fail@PLT
.L925:
	movl	$7, 0(%r13)
	xorl	%r12d, %r12d
	jmp	.L905
	.cfi_endproc
.LFE4378:
	.size	_ZN6icu_6713MessageFormat23createAppropriateFormatERNS_13UnicodeStringES2_RNS_11Formattable4TypeER11UParseErrorR10UErrorCode.part.0, .-_ZN6icu_6713MessageFormat23createAppropriateFormatERNS_13UnicodeStringES2_RNS_11Formattable4TypeER11UParseErrorR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat23createAppropriateFormatERNS_13UnicodeStringES2_RNS_11Formattable4TypeER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6713MessageFormat23createAppropriateFormatERNS_13UnicodeStringES2_RNS_11Formattable4TypeER11UParseErrorR10UErrorCode, @function
_ZN6icu_6713MessageFormat23createAppropriateFormatERNS_13UnicodeStringES2_RNS_11Formattable4TypeER11UParseErrorR10UErrorCode:
.LFB3052:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L966
	jmp	_ZN6icu_6713MessageFormat23createAppropriateFormatERNS_13UnicodeStringES2_RNS_11Formattable4TypeER11UParseErrorR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L966:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3052:
	.size	_ZN6icu_6713MessageFormat23createAppropriateFormatERNS_13UnicodeStringES2_RNS_11Formattable4TypeER11UParseErrorR10UErrorCode, .-_ZN6icu_6713MessageFormat23createAppropriateFormatERNS_13UnicodeStringES2_RNS_11Formattable4TypeER11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode.part.0, @function
_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode.part.0:
.LFB4379:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$264, %rsp
	movq	736(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L968
	call	uhash_removeAll_67@PLT
.L968:
	movq	744(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L969
	call	uhash_removeAll_67@PLT
.L969:
	movl	648(%rbx), %eax
	movl	0(%r13), %edx
	movl	$0, 704(%rbx)
	leal	-2(%rax), %r15d
	cmpl	$2, %r15d
	jle	.L970
	testl	%edx, %edx
	jg	.L967
	leal	-5(%rax), %ecx
	movq	640(%rbx), %rsi
	movl	$32, %eax
	addq	$3, %rcx
	salq	$4, %rcx
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L973:
	addq	$16, %rax
	cmpq	%rax, %rcx
	je	.L1030
.L976:
	leaq	(%rsi,%rax), %rdx
	cmpl	$7, (%rdx)
	jne	.L973
	movswl	10(%rdx), %edx
	cmpl	704(%rbx), %edx
	jl	.L973
	addl	$1, %edx
	addq	$16, %rax
	movl	%edx, 704(%rbx)
	cmpq	%rax, %rcx
	jne	.L976
	.p2align 4,,10
	.p2align 3
.L1030:
	movl	704(%rbx), %ecx
	movl	708(%rbx), %eax
	cmpl	%eax, %ecx
	jle	.L1031
	cmpl	$9, %ecx
	jle	.L1006
	addl	%eax, %eax
	cmpl	%ecx, %eax
	cmovge	%eax, %ecx
	movslq	%ecx, %rsi
	movq	%rsi, %r12
	salq	$2, %rsi
.L979:
	movq	696(%rbx), %rdi
	call	uprv_realloc_67@PLT
	testq	%rax, %rax
	je	.L1032
	movl	704(%rbx), %ecx
	movq	%rax, 696(%rbx)
	movl	%r12d, 708(%rbx)
	testl	%ecx, %ecx
	jle	.L978
.L981:
	leal	-1(%rcx), %eax
	movq	696(%rbx), %rsi
	cmpl	$2, %eax
	jbe	.L1007
	movl	%ecx, %edx
	movdqa	.LC2(%rip), %xmm0
	movq	%rsi, %rax
	shrl	$2, %edx
	salq	$4, %rdx
	addq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L985:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L985
	movl	%ecx, %eax
	andl	$-4, %eax
	testb	$3, %cl
	je	.L978
.L983:
	movslq	%eax, %rdx
	movl	$6, (%rsi,%rdx,4)
	leal	1(%rax), %edx
	cmpl	%ecx, %edx
	jge	.L978
	movslq	%edx, %rdx
	addl	$2, %eax
	movl	$6, (%rsi,%rdx,4)
	cmpl	%ecx, %eax
	jge	.L978
	cltq
	movl	$6, (%rsi,%rax,4)
.L978:
	movb	$0, 712(%rbx)
	cmpl	$1, %r15d
	jle	.L967
.L982:
	leaq	568(%rbx), %rax
	movq	%r13, %r9
	movl	$1, %r12d
	movl	%r15d, %r13d
	movq	%rax, -288(%rbp)
	leaq	.L992(%rip), %r11
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1033:
	movl	%ecx, %r12d
.L988:
	cmpl	%r12d, %r13d
	jle	.L967
.L1001:
	movl	(%r9), %ecx
	testl	%ecx, %ecx
	jg	.L967
	movq	640(%rbx), %rax
	movslq	%r12d, %r15
	leal	1(%r12), %ecx
	salq	$4, %r15
	leaq	(%rax,%r15), %rdx
	cmpl	$5, (%rdx)
	jne	.L1033
	leaq	16(%rax,%r15), %rsi
	movzwl	10(%rdx), %edx
	movq	$-1, %r14
	cmpl	$7, (%rsi)
	jne	.L989
	movswq	10(%rsi), %r14
.L989:
	cmpw	$5, %dx
	ja	.L990
	movslq	(%r11,%rdx,4), %rdx
	addq	%r11, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L992:
	.long	.L1029-.L992
	.long	.L994-.L992
	.long	.L991-.L992
	.long	.L991-.L992
	.long	.L1029-.L992
	.long	.L991-.L992
	.text
.L1032:
	movl	$7, 0(%r13)
	.p2align 4,,10
	.p2align 3
.L967:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1034
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L991:
	.cfi_restore_state
	movl	$1, -260(%rbp)
	movl	%ecx, %r12d
.L996:
	cmpl	$-1, %r14d
	je	.L988
	movq	696(%rbx), %rax
	movl	-260(%rbp), %edx
	leaq	(%rax,%r14,4), %rax
	movl	(%rax), %ecx
	cmpl	$6, %ecx
	je	.L1000
	cmpl	%edx, %ecx
	je	.L1000
	movb	$1, 712(%rbx)
.L1000:
	movl	%edx, (%rax)
	jmp	.L988
.L990:
	movl	$5, (%r9)
.L1029:
	movl	$3, -260(%rbp)
	movl	%ecx, %r12d
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L994:
	leaq	32(%rax,%r15), %rax
	leal	3(%r12), %edi
	movq	-288(%rbp), %rsi
	movq	%r9, -304(%rbp)
	movl	4(%rax), %edx
	movzwl	8(%rax), %ecx
	movl	%edi, -296(%rbp)
	leaq	-256(%rbp), %rdi
	movq	%rdi, -280(%rbp)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movq	-304(%rbp), %r9
	movq	%rax, -192(%rbp)
	movq	640(%rbx), %rax
	movw	%dx, -184(%rbp)
	leaq	48(%rax,%r15), %rax
	leaq	-192(%rbp), %r15
	cmpl	$10, (%rax)
	je	.L1035
.L997:
	movl	(%r9), %eax
	xorl	%edx, %edx
	testl	%eax, %eax
	jg	.L998
	movq	-280(%rbp), %rsi
	movq	%r15, %rdx
	leaq	-128(%rbp), %r8
	movq	%rbx, %rdi
	leaq	-260(%rbp), %rcx
	movq	%r9, -304(%rbp)
	call	_ZN6icu_6713MessageFormat23createAppropriateFormatERNS_13UnicodeStringES2_RNS_11Formattable4TypeER11UParseErrorR10UErrorCode.part.0
	movq	-304(%rbp), %r9
	movq	%rax, %rdx
.L998:
	movq	%r9, %rcx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movq	%r9, -304(%rbp)
	call	_ZN6icu_6713MessageFormat17setArgStartFormatEiPNS_6FormatER10UErrorCode
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-280(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-296(%rbp), %r12d
	movq	-304(%rbp), %r9
	leaq	.L992(%rip), %r11
	addl	$1, %r12d
	jmp	.L996
.L970:
	testl	%edx, %edx
	jg	.L967
	movl	708(%rbx), %esi
	testl	%esi, %esi
	jns	.L978
.L1006:
	movl	$40, %esi
	movl	$10, %r12d
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1031:
	testl	%ecx, %ecx
	jg	.L981
	movb	$0, 712(%rbx)
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1035:
	leaq	-128(%rbp), %r8
	movzwl	8(%rax), %ecx
	movl	4(%rax), %edx
	movq	-288(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -296(%rbp)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	-296(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-296(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leal	4(%r12), %eax
	movq	-304(%rbp), %r9
	movl	%eax, -296(%rbp)
	jmp	.L997
.L1007:
	xorl	%eax, %eax
	jmp	.L983
.L1034:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4379:
	.size	_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode.part.0, .-_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode
	.type	_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode, @function
_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode:
.LFB3051:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1036
	jmp	_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1036:
	ret
	.cfi_endproc
.LFE3051:
	.size	_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode, .-_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringERKNS_6LocaleER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringERKNS_6LocaleER11UParseErrorR10UErrorCode, @function
_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringERKNS_6LocaleER11UParseErrorR10UErrorCode:
.LFB2984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6713MessageFormatE(%rip), %rax
	movq	%r15, %rsi
	leaq	328(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	552(%rbx), %r15
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	368(%rbx), %rsi
	movq	$0, 680(%rbx)
	movq	%rbx, %rdi
	movl	$0, 688(%rbx)
	movq	%rsi, %rdx
	movq	$0, 696(%rbx)
	movq	$0, 704(%rbx)
	movb	$0, 712(%rbx)
	movq	%rax, 752(%rbx)
	movq	%rbx, 760(%rbx)
	movq	$0, 768(%rbx)
	movl	$0, 776(%rbx)
	movq	%rax, 784(%rbx)
	movq	%rbx, 792(%rbx)
	movq	$0, 800(%rbx)
	movl	$1, 808(%rbx)
	movups	%xmm0, 720(%rbx)
	movups	%xmm0, 736(%rbx)
	call	_ZN6icu_676Format12setLocaleIDsEPKcS2_@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L1045
.L1038:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1045:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6714MessagePattern5parseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L1042
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode.part.0
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L1038
.L1042:
	movq	%r15, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	736(%rbx), %rdi
	call	uhash_close_67@PLT
	movq	744(%rbx), %rdi
	movq	$0, 736(%rbx)
	call	uhash_close_67@PLT
	movb	$0, 712(%rbx)
	movq	$0, 744(%rbx)
	movl	$0, 704(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2984:
	.size	_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringERKNS_6LocaleER11UParseErrorR10UErrorCode, .-_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringERKNS_6LocaleER11UParseErrorR10UErrorCode
	.globl	_ZN6icu_6713MessageFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER11UParseErrorR10UErrorCode
	.set	_ZN6icu_6713MessageFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER11UParseErrorR10UErrorCode,_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringERKNS_6LocaleER11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, @function
_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode:
.LFB3000:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jle	.L1056
	ret
	.p2align 4,,10
	.p2align 3
.L1056:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	552(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6714MessagePattern5parseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L1050
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode.part.0
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1050
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1050:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	736(%r12), %rdi
	call	uhash_close_67@PLT
	movq	744(%r12), %rdi
	movq	$0, 736(%r12)
	call	uhash_close_67@PLT
	movq	$0, 744(%r12)
	movl	$0, 704(%r12)
	movb	$0, 712(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3000:
	.size	_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, .-_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode:
.LFB2999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	leaq	_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode(%rip), %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1058
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L1066
.L1057:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1067
	addq	$88, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1066:
	.cfi_restore_state
	leaq	552(%rdi), %r14
	leaq	-112(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern5parseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L1062
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode.part.0
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L1057
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	736(%r12), %rdi
	call	uhash_close_67@PLT
	movq	744(%r12), %rdi
	movq	$0, 736(%r12)
	call	uhash_close_67@PLT
	movq	$0, 744(%r12)
	movl	$0, 704(%r12)
	movb	$0, 712(%r12)
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1058:
	leaq	-112(%rbp), %rdx
	movq	%r13, %rcx
	call	*%rax
	jmp	.L1057
.L1067:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2999:
	.size	_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringE29UMessagePatternApostropheModeP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringE29UMessagePatternApostropheModeP11UParseErrorR10UErrorCode, @function
_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringE29UMessagePatternApostropheModeP11UParseErrorR10UErrorCode:
.LFB3002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpl	560(%rdi), %edx
	je	.L1069
	movl	%edx, %ebx
	leaq	552(%rdi), %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movl	%ebx, 560(%r12)
.L1069:
	movq	(%r12), %rax
	leaq	_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode(%rip), %rdx
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1070
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L1077
.L1068:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1077:
	.cfi_restore_state
	leaq	552(%r12), %rbx
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6714MessagePattern5parseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L1074
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode.part.0
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L1068
.L1074:
	movq	%rbx, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	736(%r12), %rdi
	call	uhash_close_67@PLT
	movq	744(%r12), %rdi
	movq	$0, 736(%r12)
	call	uhash_close_67@PLT
	movq	$0, 744(%r12)
	movl	$0, 704(%r12)
	movb	$0, 712(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1070:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE3002:
	.size	_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringE29UMessagePatternApostropheModeP11UParseErrorR10UErrorCode, .-_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringE29UMessagePatternApostropheModeP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode:
.LFB2981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6713MessageFormatE(%rip), %rax
	movq	%r13, %rsi
	leaq	328(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	552(%rbx), %r13
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	368(%rbx), %rsi
	movq	$0, 680(%rbx)
	movq	%rbx, %rdi
	movl	$0, 688(%rbx)
	movq	%rsi, %rdx
	movq	$0, 696(%rbx)
	movq	$0, 704(%rbx)
	movb	$0, 712(%rbx)
	movq	%rax, 752(%rbx)
	movq	%rbx, 760(%rbx)
	movq	$0, 768(%rbx)
	movl	$0, 776(%rbx)
	movq	%rax, 784(%rbx)
	movq	%rbx, 792(%rbx)
	movq	$0, 800(%rbx)
	movl	$1, 808(%rbx)
	movups	%xmm0, 720(%rbx)
	movups	%xmm0, 736(%rbx)
	call	_ZN6icu_676Format12setLocaleIDsEPKcS2_@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L1086
.L1078:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1087
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1086:
	.cfi_restore_state
	leaq	-112(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6714MessagePattern5parseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L1082
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode.part.0
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L1078
	.p2align 4,,10
	.p2align 3
.L1082:
	movq	%r13, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	736(%rbx), %rdi
	call	uhash_close_67@PLT
	movq	744(%rbx), %rdi
	movq	$0, 736(%rbx)
	call	uhash_close_67@PLT
	movb	$0, 712(%rbx)
	movq	$0, 744(%rbx)
	movl	$0, 704(%rbx)
	jmp	.L1078
.L1087:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2981:
	.size	_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6713MessageFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6713MessageFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode,_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringER10UErrorCode:
.LFB2978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	552(%rbx), %r14
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6713MessageFormatE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	leaq	328(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	368(%rbx), %rsi
	movq	$0, 680(%rbx)
	movq	%rbx, %rdi
	movl	$0, 688(%rbx)
	movq	%rsi, %rdx
	movq	$0, 696(%rbx)
	movq	$0, 704(%rbx)
	movb	$0, 712(%rbx)
	movq	%rax, 752(%rbx)
	movq	%rbx, 760(%rbx)
	movq	$0, 768(%rbx)
	movl	$0, 776(%rbx)
	movq	%rax, 784(%rbx)
	movq	%rbx, 792(%rbx)
	movq	$0, 800(%rbx)
	movl	$1, 808(%rbx)
	movups	%xmm0, 720(%rbx)
	movups	%xmm0, 736(%rbx)
	call	_ZN6icu_676Format12setLocaleIDsEPKcS2_@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L1096
.L1088:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1097
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1096:
	.cfi_restore_state
	leaq	-112(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern5parseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L1092
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode.part.0
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L1088
	.p2align 4,,10
	.p2align 3
.L1092:
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	736(%rbx), %rdi
	call	uhash_close_67@PLT
	movq	744(%rbx), %rdi
	movq	$0, 736(%rbx)
	call	uhash_close_67@PLT
	movb	$0, 712(%rbx)
	movq	$0, 744(%rbx)
	movl	$0, 704(%rbx)
	jmp	.L1088
.L1097:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2978:
	.size	_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6713MessageFormatC1ERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6713MessageFormatC1ERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6713MessageFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6713MessageFormat23formatComplexSubMessageEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperER10UErrorCode.part.0, @function
_ZNK6icu_6713MessageFormat23formatComplexSubMessageEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperER10UErrorCode.part.0:
.LFB4377:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	568(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-1008(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	%esi, %rbx
	salq	$4, %rbx
	subq	$1032, %rsp
	movq	16(%rbp), %rax
	movq	%rdx, -1040(%rbp)
	movq	%rcx, -1056(%rbp)
	movq	640(%rdi), %rcx
	movq	%rax, -1048(%rbp)
	movq	24(%rbp), %rax
	movq	%r8, -1064(%rbp)
	movl	%r9d, -1024(%rbp)
	movl	$2, %r9d
	movq	%rax, -1032(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%r9w, -1000(%rbp)
	movzwl	8(%rcx,%rbx), %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	addl	4(%rcx,%rbx), %edx
	addq	$16, %rbx
	movq	%rax, -1008(%rbp)
	leaq	(%rcx,%rbx), %r9
	movl	(%r9), %r8d
	movl	4(%r9), %r10d
	cmpl	$1, %r8d
	je	.L1099
	movq	-1040(%rbp), %rax
	leal	1(%rsi), %r12d
	leaq	-1008(%rbp), %r14
	addq	$16, %rax
	movq	%rax, -1072(%rbp)
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1105:
	cmpl	$5, %r8d
	je	.L1142
.L1115:
	addl	$1, %r12d
	movslq	%r12d, %rbx
	salq	$4, %rbx
	leaq	(%rcx,%rbx), %r9
	movl	(%r9), %r8d
	movl	4(%r9), %r10d
	cmpl	$1, %r8d
	je	.L1099
.L1100:
	leal	-2(%r8), %eax
	andl	$-3, %eax
	jne	.L1105
	movl	%r10d, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%r8d, -1020(%rbp)
	subl	%edx, %ecx
	movq	%r9, -1016(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	-1020(%rbp), %r8d
	movq	-1016(%rbp), %r9
	cmpl	$4, %r8d
	je	.L1143
.L1106:
	movzwl	8(%r9), %edx
	movq	640(%r13), %rcx
	addl	4(%r9), %edx
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1142:
	movl	%r10d, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%r10d, -1020(%rbp)
	subl	%edx, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	640(%r13), %rcx
	movl	-1020(%rbp), %r10d
	movq	%r15, %rdi
	movl	12(%rcx,%rbx), %eax
	movl	%r10d, %esi
	cmpl	%eax, %r12d
	cmovl	%eax, %r12d
	movslq	%r12d, %rax
	salq	$4, %rax
	movzwl	8(%rcx,%rax), %edx
	addl	4(%rcx,%rax), %edx
	movq	%r14, %rcx
	movl	%edx, -1016(%rbp)
	call	_ZN6icu_6711MessageImpl24appendReducedApostrophesERKNS_13UnicodeStringEiiRS1_@PLT
	movq	640(%r13), %rcx
	movl	-1016(%rbp), %edx
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1099:
	movl	%r10d, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	subl	%edx, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	-1000(%rbp), %ecx
	testw	%cx, %cx
	js	.L1101
	sarl	$5, %ecx
.L1102:
	xorl	%edx, %edx
	movl	$123, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	js	.L1144
	movl	$2, %edi
	leaq	328(%r13), %rdx
	movq	-1032(%rbp), %rcx
	leaq	-944(%rbp), %r13
	leaq	-880(%rbp), %r12
	movw	%di, -936(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -944(%rbp)
	call	_ZN6icu_6713MessageFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	cmpl	$1, -320(%rbp)
	jne	.L1145
	movq	-1032(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jle	.L1146
.L1118:
	movq	%r12, %rdi
	call	_ZN6icu_6713MessageFormatD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1123:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1147
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1143:
	.cfi_restore_state
	movq	-1040(%rbp), %rax
	cmpb	$0, 216(%rax)
	je	.L1107
	leaq	152(%rax), %rsi
	movzwl	160(%rax), %eax
	testw	%ax, %ax
	js	.L1108
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1114:
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r9, -1016(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-1016(%rbp), %r9
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1145:
	leaq	-328(%rbp), %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	-1032(%rbp), %rax
	movl	$1, -320(%rbp)
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L1118
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1108:
	movq	-1040(%rbp), %rax
	movl	164(%rax), %ecx
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1146:
	leaq	-328(%rbp), %rbx
	movq	%rax, %r15
	movq	%rax, %rcx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6714MessagePattern5parseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L1122
	movq	-1032(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode.part.0
	movl	(%r15), %eax
	testl	%eax, %eax
	jle	.L1121
	.p2align 4,,10
	.p2align 3
.L1122:
	movq	%rbx, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	-144(%rbp), %rdi
	call	uhash_close_67@PLT
	movq	-136(%rbp), %rdi
	movq	$0, -144(%rbp)
	call	uhash_close_67@PLT
	movq	-1032(%rbp), %rax
	movq	$0, -136(%rbp)
	movl	$0, -176(%rbp)
	movl	(%rax), %edx
	movb	$0, -168(%rbp)
	testl	%edx, %edx
	jg	.L1118
.L1121:
	subq	$8, %rsp
	pushq	-1032(%rbp)
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	-1024(%rbp), %r9d
	pushq	$0
	movq	%r12, %rdi
	pushq	-1048(%rbp)
	movq	-1064(%rbp), %r8
	movq	-1056(%rbp), %rcx
	call	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0
	addq	$32, %rsp
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1101:
	movl	-996(%rbp), %ecx
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1144:
	movq	-1048(%rbp), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rcx
	movzwl	-1000(%rbp), %eax
	testw	%ax, %ax
	js	.L1124
	movswl	%ax, %edx
	sarl	$5, %edx
.L1125:
	testb	$17, %al
	jne	.L1131
	leaq	-998(%rbp), %rsi
	testb	$2, %al
	cmove	-984(%rbp), %rsi
.L1126:
	call	*%rcx
	movswl	-1000(%rbp), %eax
	testw	%ax, %ax
	js	.L1127
	sarl	$5, %eax
.L1128:
	movq	-1048(%rbp), %rdi
	addl	%eax, 8(%rdi)
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1107:
	movq	720(%r13), %rbx
	testq	%rbx, %rbx
	je	.L1148
.L1110:
	movq	-1072(%rbp), %rsi
	movq	-1032(%rbp), %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%r9, -1016(%rbp)
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
	movq	-1016(%rbp), %r9
	movzwl	8(%rax), %edx
	movq	%rax, %rsi
	testw	%dx, %dx
	js	.L1113
	movswl	%dx, %ecx
	sarl	$5, %ecx
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1113:
	movl	12(%rax), %ecx
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1127:
	movl	-996(%rbp), %eax
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1124:
	movl	-996(%rbp), %edx
	jmp	.L1125
.L1131:
	xorl	%esi, %esi
	jmp	.L1126
.L1148:
	movq	-1032(%rbp), %rsi
	leaq	328(%r13), %rdi
	movq	%r9, -1016(%rbp)
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	-1016(%rbp), %r9
	movq	%rax, 720(%r13)
	movq	%rax, %rdi
	movq	-1032(%rbp), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jle	.L1111
	testq	%rdi, %rdi
	je	.L1112
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-1016(%rbp), %r9
.L1112:
	movq	$0, 720(%r13)
	jmp	.L1110
.L1111:
	testq	%rdi, %rdi
	je	.L1149
	movq	%rdi, %rbx
	jmp	.L1110
.L1149:
	movl	$7, (%rax)
	jmp	.L1110
.L1147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4377:
	.size	_ZNK6icu_6713MessageFormat23formatComplexSubMessageEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperER10UErrorCode.part.0, .-_ZNK6icu_6713MessageFormat23formatComplexSubMessageEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperER10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0, @function
_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0:
.LFB4374:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1128, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -1136(%rbp)
	movq	24(%rbp), %rax
	movq	32(%rbp), %rdx
	movq	%rcx, -1152(%rbp)
	movq	%rdi, -1080(%rbp)
	movq	640(%rdi), %r13
	movq	%r8, -1120(%rbp)
	movl	(%rdx), %ebx
	movl	%r9d, -1112(%rbp)
	movq	16(%rbp), %rcx
	movq	%rax, -1144(%rbp)
	movq	%rdx, -1096(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	568(%rdi), %rax
	movq	%rax, -1104(%rbp)
	movslq	%esi, %rax
	salq	$4, %rax
	movzwl	8(%r13,%rax), %r14d
	addl	4(%r13,%rax), %r14d
	testl	%ebx, %ebx
	jg	.L1150
	leaq	-880(%rbp), %rbx
	movl	%r14d, %edx
	leal	1(%rsi), %r15d
	movq	%r13, %r8
	movq	%rbx, %r14
	movq	%rcx, %rbx
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1387:
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	$17, %al
	jne	.L1302
.L1388:
	testb	$2, %al
	leaq	-870(%rbp), %rsi
	cmove	-856(%rbp), %rsi
	call	*%rcx
	movswl	-872(%rbp), %eax
	testw	%ax, %ax
	js	.L1155
.L1389:
	sarl	$5, %eax
.L1156:
	addl	%eax, 8(%rbx)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	$1, %r13d
	je	.L1150
	movl	4(%r12), %r10d
	movzwl	8(%r12), %r9d
	cmpl	$4, %r13d
	je	.L1386
	cmpl	$5, %r13d
	je	.L1182
	movq	-1096(%rbp), %rax
	movl	(%rax), %eax
.L1161:
	movzwl	%r9w, %edx
	addl	$1, %r15d
	addl	%r10d, %edx
	testl	%eax, %eax
	jg	.L1150
.L1395:
	movq	-1080(%rbp), %rax
	movq	640(%rax), %r8
.L1300:
	movslq	%r15d, %r12
	movq	-1104(%rbp), %rsi
	movq	%r14, %rdi
	salq	$4, %r12
	movq	%r12, -1088(%rbp)
	addq	%r8, %r12
	movl	4(%r12), %ecx
	movl	(%r12), %r13d
	subl	%edx, %ecx
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rcx
	movzwl	-872(%rbp), %eax
	testw	%ax, %ax
	jns	.L1387
	movl	-868(%rbp), %edx
	testb	$17, %al
	je	.L1388
.L1302:
	xorl	%esi, %esi
	call	*%rcx
	movswl	-872(%rbp), %eax
	testw	%ax, %ax
	jns	.L1389
	.p2align 4,,10
	.p2align 3
.L1155:
	movl	-868(%rbp), %eax
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	-1080(%rbp), %rax
	movl	%r15d, %esi
	movq	-1088(%rbp), %rdi
	movl	$0, -1128(%rbp)
	movq	640(%rax), %rax
	cmpl	%r15d, 12(%rax,%rdi)
	cmovge	12(%rax,%rdi), %esi
	movl	(%r12), %edi
	movl	%esi, -1108(%rbp)
	leal	-5(%rdi), %edx
	cmpl	$1, %edx
	ja	.L1183
	movswl	10(%r12), %esi
	movl	%esi, -1128(%rbp)
.L1183:
	movq	-1088(%rbp), %rdi
	movq	-1104(%rbp), %rsi
	leaq	-1056(%rbp), %r13
	leaq	16(%rax,%rdi), %r12
	movq	%r13, %rdi
	movzwl	8(%r12), %ecx
	movl	4(%r12), %edx
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	cmpq	$0, -1120(%rbp)
	jne	.L1184
	movswq	10(%r12), %rdx
	movq	%rdx, %rax
	testl	%edx, %edx
	js	.L1185
	cmpl	-1112(%rbp), %edx
	jge	.L1185
	leaq	0(,%rdx,8), %rdx
	leal	2(%r15), %esi
	subq	%rax, %rdx
	movl	%esi, -1088(%rbp)
	movq	%rdx, %rax
	salq	$4, %rax
	addq	-1152(%rbp), %rax
	movq	%rax, %r12
	je	.L1390
.L1187:
	movq	-1136(%rbp), %rax
	testq	%rax, %rax
	je	.L1208
	cmpl	%r15d, 136(%rax)
	je	.L1391
.L1208:
	movq	-1080(%rbp), %rax
	movq	736(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1230
	movl	%r15d, %esi
	call	uhash_iget_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1231
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713MessageFormat11DummyFormatE(%rip), %rdx
	leaq	_ZTIN6icu_676FormatE(%rip), %rsi
	movq	%rax, -1160(%rbp)
	call	__dynamic_cast@PLT
	movq	%rax, -1168(%rbp)
	testq	%rax, %rax
	je	.L1392
.L1231:
	movl	-1128(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L1258
	movq	-1080(%rbp), %rax
	movq	736(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1257
	movl	%r15d, %esi
	call	uhash_iget_67@PLT
	testq	%rax, %rax
	je	.L1257
.L1258:
	movq	%r12, %rdi
	call	_ZNK6icu_6711Formattable9isNumericEv@PLT
	testb	%al, %al
	je	.L1393
	movq	-1080(%rbp), %rax
	movq	720(%rax), %r15
	testq	%r15, %r15
	je	.L1394
.L1261:
	movl	$2, %edi
	movq	-1096(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movw	%di, -872(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -880(%rbp)
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
	movq	-1096(%rbp), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L1272
.L1382:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rcx
	movzwl	-872(%rbp), %eax
	testw	%ax, %ax
	js	.L1273
	movswl	%ax, %edx
	sarl	$5, %edx
.L1274:
	testb	$17, %al
	jne	.L1324
	leaq	-870(%rbp), %rsi
	testb	$2, %al
	cmove	-856(%rbp), %rsi
.L1275:
	call	*%rcx
	movswl	-872(%rbp), %eax
	testw	%ax, %ax
	jns	.L1366
.L1276:
	movl	-868(%rbp), %eax
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1360:
	movq	-1088(%rbp), %rbx
.L1185:
	movl	$123, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movzwl	-1048(%rbp), %eax
	testw	%ax, %ax
	js	.L1199
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1200:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$125, %esi
	movl	$1, %ecx
	xorl	%edx, %edx
	movw	%si, -1058(%rbp)
	movq	%rax, %rdi
	leaq	-1058(%rbp), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r12
	movq	(%rdi), %rax
	movq	40(%rax), %rcx
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L1201
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	$17, %al
	jne	.L1313
.L1401:
	leaq	10(%r12), %rsi
	testb	$2, %al
	jne	.L1203
	movq	24(%r12), %rsi
.L1203:
	call	*%rcx
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L1205
.L1366:
	sarl	$5, %eax
.L1277:
	addl	%eax, 8(%rbx)
.L1272:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1207:
	movslq	-1108(%rbp), %rax
	movq	-1080(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	salq	$4, %rax
	addq	640(%rsi), %rax
	movzwl	8(%rax), %edx
	addl	4(%rax), %edx
	addl	$1, %r15d
	movl	%edx, -1088(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-1096(%rbp), %rax
	movq	$0, -1144(%rbp)
	movl	-1088(%rbp), %edx
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L1395
	.p2align 4,,10
	.p2align 3
.L1150:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1396
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1386:
	.cfi_restore_state
	movq	-1136(%rbp), %rax
	cmpb	$0, 216(%rax)
	leaq	16(%rax), %r12
	je	.L1159
	movswl	160(%rax), %edx
	movl	%edx, %ecx
	sarl	$5, %edx
	je	.L1160
	movq	-1096(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1161
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	testw	%cx, %cx
	jns	.L1162
	movq	-1136(%rbp), %rsi
	movl	164(%rsi), %edx
.L1162:
	testb	$17, %cl
	jne	.L1304
	andl	$2, %ecx
	movq	-1136(%rbp), %rsi
	je	.L1164
	addq	$162, %rsi
.L1163:
	movl	%r9d, -1108(%rbp)
	movl	%r10d, -1088(%rbp)
	call	*%rax
	movq	-1136(%rbp), %rax
	movl	-1088(%rbp), %r10d
	movl	-1108(%rbp), %r9d
	movswl	160(%rax), %eax
	testw	%ax, %ax
	js	.L1165
	sarl	$5, %eax
.L1166:
	addl	%eax, 8(%rbx)
	movq	-1096(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1184:
	movl	-1112(%rbp), %eax
	testl	%eax, %eax
	jle	.L1185
	movq	-1120(%rbp), %rdi
	xorl	%r12d, %r12d
	subl	$1, %eax
	movl	%r15d, -1160(%rbp)
	movq	%rbx, -1088(%rbp)
	movq	%r12, %r15
	movq	%rax, %r12
	movq	%rdi, %rbx
	jmp	.L1198
	.p2align 4,,10
	.p2align 3
.L1398:
	notl	%eax
	andl	$1, %eax
	testb	%al, %al
	je	.L1397
.L1196:
	leaq	1(%r15), %rax
	addq	$64, %rbx
	cmpq	%r12, %r15
	je	.L1360
	movq	%rax, %r15
.L1198:
	movzwl	-1048(%rbp), %esi
	testw	%si, %si
	js	.L1188
	movswl	%si, %ecx
	sarl	$5, %ecx
.L1189:
	movzwl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L1190
	movswl	%ax, %edx
	sarl	$5, %edx
.L1191:
	testb	$1, %sil
	jne	.L1398
	testl	%ecx, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L1194
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L1194:
	andl	$2, %esi
	leaq	-1046(%rbp), %rcx
	movq	%rbx, %rdi
	cmove	-1032(%rbp), %rcx
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jne	.L1196
.L1397:
	movq	%r15, %r12
	movq	-1088(%rbp), %rbx
	movl	-1160(%rbp), %r15d
	leaq	0(,%r12,8), %rax
	subq	%r12, %rax
	salq	$4, %rax
	addq	-1152(%rbp), %rax
	movq	%rax, %r12
	je	.L1185
	leal	2(%r15), %eax
	movl	%eax, -1088(%rbp)
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1190:
	movl	12(%rbx), %edx
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1188:
	movl	-1044(%rbp), %ecx
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	-1080(%rbp), %rax
	movq	720(%rax), %r13
	testq	%r13, %r13
	je	.L1399
.L1173:
	movl	$2, %edi
	movq	-1096(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movw	%di, -872(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r13, %rdi
	movl	%r9d, -1108(%rbp)
	movl	%r10d, -1088(%rbp)
	movq	%rax, -880(%rbp)
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
	movq	-1096(%rbp), %rax
	movl	-1088(%rbp), %r10d
	movl	-1108(%rbp), %r9d
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jle	.L1380
.L1176:
	movq	%r14, %rdi
	movl	%r9d, -1108(%rbp)
	movl	%r10d, -1088(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-1096(%rbp), %rax
	movl	-1088(%rbp), %r10d
	movl	-1108(%rbp), %r9d
	movl	(%rax), %eax
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1392:
	movq	-1160(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6712ChoiceFormatE(%rip), %rdx
	leaq	_ZTIN6icu_676FormatE(%rip), %rsi
	movq	%rdi, -1088(%rbp)
	call	__dynamic_cast@PLT
	movq	-1088(%rbp), %rdi
	testq	%rax, %rax
	je	.L1400
.L1232:
	movl	$2, %r15d
	movq	-1096(%rbp), %rcx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rsi
	movw	%r15w, -984(%rbp)
	leaq	-992(%rbp), %r15
	movq	%r15, %rdx
	movq	%rax, -992(%rbp)
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
	movzwl	-984(%rbp), %eax
	testw	%ax, %ax
	js	.L1239
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1240:
	xorl	%edx, %edx
	movl	$123, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	jns	.L1247
	movzwl	-984(%rbp), %eax
	testw	%ax, %ax
	js	.L1244
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L1245:
	xorl	%edx, %edx
	movl	$39, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	js	.L1246
	movq	-1080(%rbp), %rax
	cmpl	$1, 560(%rax)
	je	.L1246
.L1247:
	movq	-1096(%rbp), %r12
	movq	-1080(%rbp), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	leaq	328(%rax), %rdx
	movq	%r12, %rcx
	call	_ZN6icu_6713MessageFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	movl	(%r12), %r11d
	testl	%r11d, %r11d
	jg	.L1242
	subq	$8, %rsp
	pushq	-1096(%rbp)
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	-1112(%rbp), %r9d
	pushq	-1144(%rbp)
	movq	%r14, %rdi
	movq	-1120(%rbp), %r8
	pushq	%rbx
	movq	-1152(%rbp), %rcx
	call	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0
	addq	$32, %rsp
.L1242:
	movq	%r14, %rdi
	call	_ZN6icu_6713MessageFormatD1Ev
.L1248:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1390:
	movq	(%rbx), %rdi
	movl	$4, %edx
	leaq	_ZL11NULL_STRING(%rip), %rsi
	movq	(%rdi), %rax
	call	*40(%rax)
	addl	$4, 8(%rbx)
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1199:
	movl	-1044(%rbp), %ecx
	jmp	.L1200
	.p2align 4,,10
	.p2align 3
.L1201:
	movl	12(%r12), %edx
	testb	$17, %al
	je	.L1401
.L1313:
	xorl	%esi, %esi
	call	*%rcx
	movswl	8(%r12), %eax
	testw	%ax, %ax
	jns	.L1366
	.p2align 4,,10
	.p2align 3
.L1205:
	movl	12(%r12), %eax
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1391:
	pxor	%xmm0, %xmm0
	ucomisd	128(%rax), %xmm0
	movq	144(%rax), %rdi
	jp	.L1271
	jne	.L1271
	movswl	160(%rax), %edx
	movl	%edx, %eax
	sarl	$5, %edx
	je	.L1211
	movq	-1096(%rbp), %rsi
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L1207
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	movq	40(%rcx), %rcx
	testw	%ax, %ax
	jns	.L1213
	movq	-1136(%rbp), %rsi
	movl	164(%rsi), %edx
.L1213:
	testb	$17, %al
	jne	.L1314
	testb	$2, %al
	movq	-1136(%rbp), %rax
	leaq	162(%rax), %rsi
	jne	.L1214
	movq	176(%rax), %rsi
.L1214:
	call	*%rcx
	movq	-1136(%rbp), %rax
	movswl	160(%rax), %eax
	testw	%ax, %ax
	js	.L1216
.L1364:
	sarl	$5, %eax
.L1283:
	addl	%eax, 8(%rbx)
	jmp	.L1207
.L1393:
	movq	%r12, %rdi
	call	_ZNK6icu_6711Formattable7getTypeEv@PLT
	testl	%eax, %eax
	jne	.L1270
	movq	-1080(%rbp), %rax
	movq	728(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L1271
	movl	$3, %edi
	leaq	328(%rax), %rdx
	movl	$3, %esi
	movq	%rax, %r15
	call	_ZN6icu_6710DateFormat22createDateTimeInstanceENS0_6EStyleES1_RKNS_6LocaleE@PLT
	movq	%rax, 728(%r15)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L1271
	movq	-1096(%rbp), %rax
	movl	$7, (%rax)
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	-1096(%rbp), %r15
	movl	$2, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdx
	movw	%cx, -872(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r15, %rcx
	movq	%rax, -880(%rbp)
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
	movl	(%r15), %esi
	testl	%esi, %esi
	jg	.L1272
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1244:
	movl	-980(%rbp), %ecx
	jmp	.L1245
	.p2align 4,,10
	.p2align 3
.L1246:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rcx
	movzwl	-984(%rbp), %eax
	testw	%ax, %ax
	js	.L1249
	movswl	%ax, %edx
	sarl	$5, %edx
.L1250:
	testb	$17, %al
	jne	.L1251
	leaq	-982(%rbp), %r12
	testb	$2, %al
	cmove	-968(%rbp), %r12
	movq	%r12, -1168(%rbp)
.L1251:
	movq	-1168(%rbp), %rsi
	call	*%rcx
	movswl	-984(%rbp), %eax
	testw	%ax, %ax
	js	.L1252
	sarl	$5, %eax
.L1253:
	addl	%eax, 8(%rbx)
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1400:
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6712PluralFormatE(%rip), %rdx
	leaq	_ZTIN6icu_676FormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	-1088(%rbp), %rdi
	testq	%rax, %rax
	jne	.L1232
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6712SelectFormatE(%rip), %rdx
	leaq	_ZTIN6icu_676FormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	-1088(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.L1232
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	-1096(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rax, -880(%rbp)
	movl	$2, %eax
	movw	%ax, -872(%rbp)
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
	movq	-1096(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1272
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rcx
	movzwl	-872(%rbp), %eax
	testw	%ax, %ax
	js	.L1234
	movswl	%ax, %edx
	sarl	$5, %edx
.L1235:
	testb	$17, %al
	jne	.L1236
	leaq	-870(%rbp), %rsi
	testb	$2, %al
	cmove	-856(%rbp), %rsi
	movq	%rsi, %r15
.L1236:
	movq	%r15, %rsi
	call	*%rcx
	movswl	-872(%rbp), %eax
	testw	%ax, %ax
	jns	.L1366
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1239:
	movl	-980(%rbp), %ecx
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	-1096(%rbp), %r13
	movq	144(%rax), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	movl	%r10d, -1088(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r10d
	movq	%r13, %rcx
	movl	%r9d, -1108(%rbp)
	movw	%r10w, -872(%rbp)
	movq	%rax, -880(%rbp)
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
	movl	0(%r13), %r11d
	movl	-1088(%rbp), %r10d
	movl	-1108(%rbp), %r9d
	testl	%r11d, %r11d
	jg	.L1176
.L1380:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rcx
	movzwl	-872(%rbp), %eax
	testw	%ax, %ax
	js	.L1177
	movswl	%ax, %edx
	sarl	$5, %edx
.L1178:
	testb	$17, %al
	jne	.L1308
	leaq	-870(%rbp), %rsi
	testb	$2, %al
	cmove	-856(%rbp), %rsi
.L1179:
	movl	%r9d, -1108(%rbp)
	movl	%r10d, -1088(%rbp)
	call	*%rcx
	movswl	-872(%rbp), %eax
	movl	-1088(%rbp), %r10d
	movl	-1108(%rbp), %r9d
	testw	%ax, %ax
	js	.L1180
	sarl	$5, %eax
.L1181:
	addl	%eax, 8(%rbx)
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1230:
	movl	-1128(%rbp), %eax
	testl	%eax, %eax
	je	.L1258
.L1257:
	cmpl	$2, -1128(%rbp)
	je	.L1402
	movl	-1128(%rbp), %eax
	subl	$3, %eax
	andl	$-3, %eax
	je	.L1403
	cmpl	$4, -1128(%rbp)
	jne	.L1296
	movq	-1096(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK6icu_6711Formattable9getStringER10UErrorCode@PLT
	movq	-1080(%rbp), %r12
	movl	-1088(%rbp), %esi
	movq	%r15, %rcx
	movq	%rax, %rdx
	leaq	552(%r12), %rdi
	call	_ZN6icu_6712SelectFormat14findSubMessageERKNS_14MessagePatternEiRKNS_13UnicodeStringER10UErrorCode@PLT
	movl	(%r15), %esi
	testl	%esi, %esi
	jg	.L1207
	cmpl	$1, 560(%r12)
	jne	.L1404
	pushq	-1096(%rbp)
	movq	-1152(%rbp), %rcx
	xorl	%edx, %edx
	movl	%eax, %esi
	movl	-1112(%rbp), %r9d
	pushq	%rbx
	movq	-1120(%rbp), %r8
	movq	-1080(%rbp), %rdi
	call	_ZNK6icu_6713MessageFormat23formatComplexSubMessageEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperER10UErrorCode.part.0
	popq	%rdx
	popq	%rcx
	jmp	.L1207
.L1249:
	movl	-980(%rbp), %edx
	jmp	.L1250
.L1211:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	-1096(%rbp), %r15
	movq	%r14, %rdx
	movq	%rax, -880(%rbp)
	movl	$2, %eax
	movw	%ax, -872(%rbp)
	movq	-1136(%rbp), %rax
	movq	%r15, %rcx
	leaq	16(%rax), %rsi
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L1272
	jmp	.L1382
.L1399:
	movq	-1096(%rbp), %rsi
	leaq	328(%rax), %rdi
	movl	%r9d, -1108(%rbp)
	movl	%r10d, -1088(%rbp)
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	-1088(%rbp), %r10d
	movq	%rax, %rdi
	movq	-1080(%rbp), %rax
	movq	%rdi, 720(%rax)
	movq	-1096(%rbp), %rax
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	movl	-1108(%rbp), %r9d
	jle	.L1174
	testq	%rdi, %rdi
	je	.L1175
	movq	(%rdi), %rax
	call	*8(%rax)
	movl	-1108(%rbp), %r9d
	movl	-1088(%rbp), %r10d
.L1175:
	movq	-1080(%rbp), %rax
	movq	$0, 720(%rax)
	jmp	.L1173
.L1177:
	movl	-868(%rbp), %edx
	jmp	.L1178
.L1180:
	movl	-868(%rbp), %eax
	jmp	.L1181
.L1252:
	movl	-980(%rbp), %eax
	jmp	.L1253
.L1174:
	testq	%rdi, %rdi
	je	.L1405
	movq	%rdi, %r13
	jmp	.L1173
.L1165:
	movq	-1136(%rbp), %rax
	movl	164(%rax), %eax
	jmp	.L1166
.L1164:
	movq	176(%rsi), %rsi
	jmp	.L1163
.L1308:
	xorl	%esi, %esi
	jmp	.L1179
.L1273:
	movl	-868(%rbp), %edx
	jmp	.L1274
.L1404:
	subq	$8, %rsp
	pushq	-1096(%rbp)
	xorl	%edx, %edx
	movl	%eax, %esi
	movl	-1112(%rbp), %r9d
	pushq	$0
	movq	-1120(%rbp), %r8
	movq	-1152(%rbp), %rcx
	pushq	%rbx
	movq	-1080(%rbp), %rdi
	call	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0
	addq	$32, %rsp
	jmp	.L1207
.L1403:
	movq	%r12, %rdi
	call	_ZNK6icu_6711Formattable9isNumericEv@PLT
	testb	%al, %al
	je	.L1362
	movq	-1080(%rbp), %rdi
	cmpl	$3, -1128(%rbp)
	leaq	784(%rdi), %rax
	leaq	752(%rdi), %r15
	cmovne	%rax, %r15
	leaq	552(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -1144(%rbp)
	movq	%r15, -1128(%rbp)
	movl	-1088(%rbp), %r15d
	movl	%r15d, %esi
	call	_ZNK6icu_6714MessagePattern15getPluralOffsetEi@PLT
	movl	%r15d, -880(%rbp)
	leaq	-864(%rbp), %r15
	movq	%r15, %rdi
	movq	%r13, -872(%rbp)
	movsd	%xmm0, -1160(%rbp)
	call	_ZN6icu_6711FormattableC1Ev@PLT
	pxor	%xmm2, %xmm2
	movsd	-1160(%rbp), %xmm1
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r10d
	movq	%rax, -728(%rbp)
	ucomisd	%xmm2, %xmm1
	movw	%r10w, -720(%rbp)
	movl	$-1, -744(%rbp)
	movq	$0, -736(%rbp)
	movb	$0, -664(%rbp)
	movsd	%xmm1, -752(%rbp)
	jp	.L1291
	jne	.L1291
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
.L1293:
	movq	-1096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	movq	-1096(%rbp), %r12
	movq	%r14, %rcx
	movl	-1088(%rbp), %esi
	movq	-1128(%rbp), %rdx
	movq	-1144(%rbp), %rdi
	movq	%r12, %r8
	call	_ZN6icu_6712PluralFormat14findSubMessageERKNS_14MessagePatternEiRKNS0_14PluralSelectorEPvdR10UErrorCode@PLT
	movl	(%r12), %r9d
	movl	%eax, %esi
	testl	%r9d, %r9d
	jg	.L1294
	movq	-1080(%rbp), %rax
	cmpl	$1, 560(%rax)
	jne	.L1406
	pushq	-1096(%rbp)
	movq	%rax, %rdi
	movq	%r14, %rdx
	movq	-1120(%rbp), %r8
	movl	-1112(%rbp), %r9d
	pushq	%rbx
	movq	-1152(%rbp), %rcx
	call	_ZNK6icu_6713MessageFormat23formatComplexSubMessageEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperER10UErrorCode.part.0
	popq	%rdi
	popq	%r8
.L1294:
	leaq	-728(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	jmp	.L1207
.L1270:
	movq	-1096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6711Formattable9getStringER10UErrorCode@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r12
	movq	(%rdi), %rax
	movq	40(%rax), %rcx
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L1278
	movswl	%ax, %edx
	sarl	$5, %edx
.L1279:
	testb	$17, %al
	jne	.L1326
	leaq	10(%r12), %rsi
	testb	$2, %al
	jne	.L1280
	movq	24(%r12), %rsi
.L1280:
	call	*%rcx
	movswl	8(%r12), %eax
	testw	%ax, %ax
	jns	.L1364
	movl	12(%r12), %eax
	jmp	.L1283
.L1402:
	movq	%r12, %rdi
	call	_ZNK6icu_6711Formattable9isNumericEv@PLT
	testb	%al, %al
	je	.L1362
	movq	-1096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	movq	-1080(%rbp), %r15
	movl	-1088(%rbp), %esi
	leaq	552(%r15), %rdi
	call	_ZN6icu_6712ChoiceFormat14findSubMessageERKNS_14MessagePatternEid@PLT
	movl	%eax, %esi
	movq	-1096(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L1207
	cmpl	$1, 560(%r15)
	jne	.L1407
	pushq	-1096(%rbp)
	movl	-1112(%rbp), %r9d
	xorl	%edx, %edx
	movq	-1120(%rbp), %r8
	pushq	%rbx
	movq	-1152(%rbp), %rcx
	movq	-1080(%rbp), %rdi
	call	_ZNK6icu_6713MessageFormat23formatComplexSubMessageEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperER10UErrorCode.part.0
	popq	%r11
	popq	%r12
	jmp	.L1207
.L1304:
	xorl	%esi, %esi
	jmp	.L1163
.L1324:
	xorl	%esi, %esi
	jmp	.L1275
.L1405:
	movl	$7, (%rax)
	jmp	.L1173
.L1394:
	movq	-1096(%rbp), %rsi
	leaq	328(%rax), %rdi
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %rdi
	movq	-1080(%rbp), %rax
	movq	%rdi, 720(%rax)
	movq	-1096(%rbp), %rax
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jle	.L1262
	testq	%rdi, %rdi
	je	.L1263
	movq	(%rdi), %rax
	call	*8(%rax)
.L1263:
	movq	-1080(%rbp), %rax
	movq	$0, 720(%rax)
	jmp	.L1261
.L1291:
	movq	-1096(%rbp), %rsi
	movq	%r12, %rdi
	movsd	%xmm1, -1160(%rbp)
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	movsd	-1160(%rbp), %xmm1
	leaq	-992(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -1160(%rbp)
	subsd	%xmm1, %xmm0
	call	_ZN6icu_6711FormattableC1Ed@PLT
	movq	-1160(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	movq	-1160(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	jmp	.L1293
.L1216:
	movq	-1136(%rbp), %rax
	movl	164(%rax), %eax
	jmp	.L1283
.L1278:
	movl	12(%r12), %edx
	jmp	.L1279
.L1262:
	testq	%rdi, %rdi
	je	.L1408
	movq	%rdi, %r15
	jmp	.L1261
.L1406:
	subq	$8, %rsp
	pushq	-1096(%rbp)
	movl	-1112(%rbp), %r9d
	movq	%r14, %rdx
	movq	-1120(%rbp), %r8
	pushq	$0
	movq	%rax, %rdi
	pushq	%rbx
	movq	-1152(%rbp), %rcx
	call	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0
	addq	$32, %rsp
	jmp	.L1294
.L1314:
	xorl	%esi, %esi
	jmp	.L1214
.L1326:
	xorl	%esi, %esi
	jmp	.L1280
.L1407:
	subq	$8, %rsp
	movl	-1112(%rbp), %r9d
	xorl	%edx, %edx
	movq	-1120(%rbp), %r8
	movq	-1152(%rbp), %rcx
	movq	-1080(%rbp), %rdi
	pushq	%rax
	pushq	$0
	pushq	%rbx
	call	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0
	addq	$32, %rsp
	jmp	.L1207
.L1362:
	movq	-1096(%rbp), %rax
	movq	%r13, %rdi
	movl	$1, (%rax)
.L1285:
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1150
.L1296:
	movq	-1096(%rbp), %rax
	movq	%r13, %rdi
	movl	$5, (%rax)
	jmp	.L1285
.L1234:
	movl	-868(%rbp), %edx
	jmp	.L1235
.L1408:
	movl	$7, (%rax)
	jmp	.L1261
.L1396:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4374:
	.size	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0, .-_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode:
.LFB3033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	32(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1409
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1409:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3033:
	.size	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6713MessageFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6713MessageFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB3026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	(%r8), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L1419
.L1414:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1420
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1419:
	.cfi_restore_state
	movq	%rdi, %r14
	movq	%rsi, %rdi
	movq	%rsi, %r15
	movq	%rcx, %r13
	movq	%r8, %rbx
	call	_ZNK6icu_6711Formattable7getTypeEv@PLT
	cmpl	$4, %eax
	jne	.L1421
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1414
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	subq	$8, %rsp
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rax
	movq	8(%r15), %rcx
	leaq	-96(%rbp), %r10
	pushq	%rbx
	movl	16(%r15), %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	pushq	%r13
	pushq	%rax
	movq	%r10, -80(%rbp)
	movq	%r10, -104(%rbp)
	movq	%r12, -88(%rbp)
	movl	$0, -72(%rbp)
	call	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0
	movq	-104(%rbp), %r10
	addq	$32, %rsp
	movq	%r10, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1421:
	movl	$1, (%rbx)
	jmp	.L1414
.L1420:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3026:
	.size	_ZNK6icu_6713MessageFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6713MessageFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat23formatComplexSubMessageEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperER10UErrorCode
	.type	_ZNK6icu_6713MessageFormat23formatComplexSubMessageEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperER10UErrorCode, @function
_ZNK6icu_6713MessageFormat23formatComplexSubMessageEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperER10UErrorCode:
.LFB3037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	24(%rbp), %rax
	movq	16(%rbp), %r10
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jg	.L1422
	cmpl	$1, 560(%rdi)
	jne	.L1426
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6713MessageFormat23formatComplexSubMessageEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1426:
	.cfi_restore_state
	subq	$8, %rsp
	pushq	%rax
	pushq	$0
	pushq	%r10
	call	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0
	addq	$32, %rsp
.L1422:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3037:
	.size	_ZNK6icu_6713MessageFormat23formatComplexSubMessageEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperER10UErrorCode, .-_ZNK6icu_6713MessageFormat23formatComplexSubMessageEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat6formatEPKNS_13UnicodeStringEPKNS_11FormattableEiRS1_R10UErrorCode
	.type	_ZNK6icu_6713MessageFormat6formatEPKNS_13UnicodeStringEPKNS_11FormattableEiRS1_R10UErrorCode, @function
_ZNK6icu_6713MessageFormat6formatEPKNS_13UnicodeStringEPKNS_11FormattableEiRS1_R10UErrorCode:
.LFB3027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%r8, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L1428
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	subq	$8, %rsp
	movq	%r8, -56(%rbp)
	leaq	-64(%rbp), %r13
	movq	%rax, -64(%rbp)
	leaq	-48(%rbp), %rax
	movq	%rsi, %r8
	xorl	%esi, %esi
	pushq	%r9
	movl	%ecx, %r9d
	movq	%rdx, %rcx
	xorl	%edx, %edx
	pushq	$0
	pushq	%rax
	movq	%r13, -48(%rbp)
	movl	$0, -40(%rbp)
	call	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0
	addq	$32, %rsp
	movq	%r13, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
.L1428:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1431
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1431:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3027:
	.size	_ZNK6icu_6713MessageFormat6formatEPKNS_13UnicodeStringEPKNS_11FormattableEiRS1_R10UErrorCode, .-_ZNK6icu_6713MessageFormat6formatEPKNS_13UnicodeStringEPKNS_11FormattableEiRS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6713MessageFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6713MessageFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB3024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L1433
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	subq	$8, %rsp
	movq	%rcx, -56(%rbp)
	leaq	-64(%rbp), %r13
	movq	%rax, -64(%rbp)
	leaq	-48(%rbp), %rax
	movq	%rsi, %rcx
	xorl	%esi, %esi
	pushq	%r9
	movl	%edx, %r9d
	xorl	%edx, %edx
	pushq	%r8
	xorl	%r8d, %r8d
	pushq	%rax
	movq	%r13, -48(%rbp)
	movl	$0, -40(%rbp)
	call	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0
	addq	$32, %rsp
	movq	%r13, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
.L1433:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1436
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1436:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3024:
	.size	_ZNK6icu_6713MessageFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6713MessageFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat6formatEPKNS_11FormattableEPKNS_13UnicodeStringEiRS4_PNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6713MessageFormat6formatEPKNS_11FormattableEPKNS_13UnicodeStringEiRS4_PNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6713MessageFormat6formatEPKNS_11FormattableEPKNS_13UnicodeStringEiRS4_PNS_13FieldPositionER10UErrorCode:
.LFB3029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%r8, %r12
	subq	$48, %rsp
	movq	16(%rbp), %rax
	movq	%fs:40, %r10
	movq	%r10, -24(%rbp)
	xorl	%r10d, %r10d
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L1438
	subq	$8, %rsp
	leaq	-64(%rbp), %r13
	movq	%rdx, %r8
	xorl	%edx, %edx
	pushq	%rax
	leaq	-48(%rbp), %rax
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %r11
	pushq	%r9
	movl	%ecx, %r9d
	movq	%rsi, %rcx
	xorl	%esi, %esi
	pushq	%rax
	movq	%r11, -64(%rbp)
	movq	%r12, -56(%rbp)
	movq	%r13, -48(%rbp)
	movl	$0, -40(%rbp)
	call	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0
	addq	$32, %rsp
	movq	%r13, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
.L1438:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1441
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1441:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3029:
	.size	_ZNK6icu_6713MessageFormat6formatEPKNS_11FormattableEPKNS_13UnicodeStringEiRS4_PNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6713MessageFormat6formatEPKNS_11FormattableEPKNS_13UnicodeStringEiRS4_PNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713MessageFormat6formatERKNS_13UnicodeStringEPKNS_11FormattableEiRS1_R10UErrorCode
	.type	_ZN6icu_6713MessageFormat6formatERKNS_13UnicodeStringEPKNS_11FormattableEiRS1_R10UErrorCode, @function
_ZN6icu_6713MessageFormat6formatERKNS_13UnicodeStringEPKNS_11FormattableEiRS1_R10UErrorCode:
.LFB3025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-328(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-880(%rbp), %r12
	pushq	%rbx
	movq	%r12, %xmm2
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$984, %rsp
	movl	%edx, -1020(%rbp)
	movq	%rsi, -1016(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE(%rip), %rax
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -1008(%rbp)
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6713MessageFormatE(%rip), %rax
	movq	%rax, -880(%rbp)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	leaq	-552(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	movq	-512(%rbp), %rsi
	pxor	%xmm1, %xmm1
	movq	%r12, %rdi
	movdqa	-1008(%rbp), %xmm0
	movq	$0, -200(%rbp)
	movq	%rsi, %rdx
	movl	$0, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -176(%rbp)
	movb	$0, -168(%rbp)
	movq	$0, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -80(%rbp)
	movl	$1, -72(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN6icu_676Format12setLocaleIDsEPKcS2_@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L1451
.L1445:
	movq	%r12, %rdi
	call	_ZN6icu_6713MessageFormatD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1452
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1451:
	.cfi_restore_state
	movq	%rbx, %rcx
	leaq	-960(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern5parseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1447
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713MessageFormat20cacheExplicitFormatsER10UErrorCode.part.0
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1447
.L1446:
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	subq	$8, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	pushq	%rbx
	movl	-1020(%rbp), %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	pushq	$0
	movq	-1016(%rbp), %rcx
	leaq	-992(%rbp), %r15
	movq	%rax, -992(%rbp)
	leaq	-976(%rbp), %rax
	pushq	%rax
	movq	%r13, -984(%rbp)
	movq	%r15, -976(%rbp)
	movl	$0, -968(%rbp)
	call	_ZNK6icu_6713MessageFormat6formatEiPKvPKNS_11FormattableEPKNS_13UnicodeStringEiRNS_17AppendableWrapperEPNS_13FieldPositionER10UErrorCode.part.0
	addq	$32, %rsp
	movq	%r15, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1447:
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern5clearEv@PLT
	movq	-144(%rbp), %rdi
	call	uhash_close_67@PLT
	movq	-136(%rbp), %rdi
	movq	$0, -144(%rbp)
	call	uhash_close_67@PLT
	movl	(%rbx), %edx
	movb	$0, -168(%rbp)
	movq	$0, -136(%rbp)
	movl	$0, -176(%rbp)
	testl	%edx, %edx
	jle	.L1446
	jmp	.L1445
.L1452:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3025:
	.size	_ZN6icu_6713MessageFormat6formatERKNS_13UnicodeStringEPKNS_11FormattableEiRS1_R10UErrorCode, .-_ZN6icu_6713MessageFormat6formatERKNS_13UnicodeStringEPKNS_11FormattableEiRS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713MessageFormat22PluralSelectorProvider6selectEPvdR10UErrorCode
	.type	_ZNK6icu_6713MessageFormat22PluralSelectorProvider6selectEPvdR10UErrorCode, @function
_ZNK6icu_6713MessageFormat22PluralSelectorProvider6selectEPvdR10UErrorCode:
.LFB3092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %edi
	movsd	%xmm0, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L1487
	cmpq	$0, 16(%rsi)
	movq	8(%rsi), %rdi
	movq	%rsi, %r12
	movq	%rdx, %rbx
	movq	%rcx, %r13
	je	.L1488
.L1456:
	movl	(%rbx), %esi
	call	_ZNK6icu_6713MessageFormat19findOtherSubMessageEi
	movq	8(%rbx), %rdx
	movq	8(%r12), %rdi
	movl	%eax, %esi
	call	_ZNK6icu_6713MessageFormat24findFirstPluralNumberArgEiRKNS_13UnicodeStringE
	movl	%eax, 136(%rbx)
	testl	%eax, %eax
	jle	.L1460
	movq	8(%r12), %rdx
	movq	736(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1460
	movl	%eax, %esi
	call	uhash_iget_67@PLT
	movq	%rax, 144(%rbx)
	jmp	.L1459
	.p2align 4,,10
	.p2align 3
.L1460:
	movq	144(%rbx), %rax
.L1459:
	testq	%rax, %rax
	je	.L1489
.L1461:
	leaq	16(%rbx), %r15
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	ucomisd	-152(%rbp), %xmm0
	jp	.L1472
	je	.L1485
.L1472:
	movl	$5, 0(%r13)
.L1487:
	leaq	_ZL12OTHER_STRING(%rip), %rax
	leaq	-128(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	$5, %ecx
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
.L1453:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1490
	addq	$120, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1485:
	.cfi_restore_state
	movq	144(%rbx), %rdi
	leaq	152(%rbx), %rdx
	movq	%r13, %rcx
	movq	%r15, %rsi
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1467
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_676FormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1467
	leaq	-128(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	-152(%rbp), %r8
	movq	%r8, %rdx
	call	_ZNK6icu_6713DecimalFormat23formatToDecimalQuantityERKNS_11FormattableERNS_6number4impl15DecimalQuantityER10UErrorCode@PLT
	movl	0(%r13), %eax
	movq	-152(%rbp), %r8
	testl	%eax, %eax
	jle	.L1468
	leaq	_ZL12OTHER_STRING(%rip), %rax
	leaq	-136(%rbp), %rdx
	movl	$5, %ecx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-152(%rbp), %r8
.L1469:
	movq	%r8, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	jmp	.L1453
	.p2align 4,,10
	.p2align 3
.L1488:
	movl	24(%rsi), %esi
	addq	$328, %rdi
	movq	%rcx, %rdx
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movl	0(%r13), %esi
	movq	%rax, 16(%r12)
	testl	%esi, %esi
	jg	.L1487
	movq	8(%r12), %rdi
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1467:
	movq	16(%r12), %rsi
	movsd	-152(%rbp), %xmm0
	movq	%r14, %rdi
	call	_ZNK6icu_6711PluralRules6selectEd@PLT
	jmp	.L1453
	.p2align 4,,10
	.p2align 3
.L1489:
	movq	8(%r12), %rdx
	movq	720(%rdx), %r15
	testq	%r15, %r15
	je	.L1491
.L1462:
	movq	%r15, 144(%rbx)
	movb	$1, 216(%rbx)
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1468:
	movq	16(%r12), %rsi
	movq	%r8, %rdx
	movq	%r14, %rdi
	movq	%r8, -152(%rbp)
	call	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE@PLT
	movq	-152(%rbp), %r8
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1491:
	leaq	328(%rdx), %rdi
	movq	%r13, %rsi
	movq	%rdx, -160(%rbp)
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	-160(%rbp), %rdx
	movl	0(%r13), %ecx
	movq	%rax, %rdi
	movq	%rax, 720(%rdx)
	testl	%ecx, %ecx
	jle	.L1463
	testq	%rax, %rax
	je	.L1464
	movq	(%rax), %rax
	call	*8(%rax)
	movq	-160(%rbp), %rdx
.L1464:
	movq	$0, 720(%rdx)
	jmp	.L1462
.L1463:
	testq	%rax, %rax
	je	.L1492
	movq	%rax, %r15
	jmp	.L1462
.L1492:
	movl	$7, 0(%r13)
	jmp	.L1462
.L1490:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3092:
	.size	_ZNK6icu_6713MessageFormat22PluralSelectorProvider6selectEPvdR10UErrorCode, .-_ZNK6icu_6713MessageFormat22PluralSelectorProvider6selectEPvdR10UErrorCode
	.weak	_ZTSN6icu_6713MessageFormat22PluralSelectorProviderE
	.section	.rodata._ZTSN6icu_6713MessageFormat22PluralSelectorProviderE,"aG",@progbits,_ZTSN6icu_6713MessageFormat22PluralSelectorProviderE,comdat
	.align 32
	.type	_ZTSN6icu_6713MessageFormat22PluralSelectorProviderE, @object
	.size	_ZTSN6icu_6713MessageFormat22PluralSelectorProviderE, 49
_ZTSN6icu_6713MessageFormat22PluralSelectorProviderE:
	.string	"N6icu_6713MessageFormat22PluralSelectorProviderE"
	.weak	_ZTIN6icu_6713MessageFormat22PluralSelectorProviderE
	.section	.data.rel.ro._ZTIN6icu_6713MessageFormat22PluralSelectorProviderE,"awG",@progbits,_ZTIN6icu_6713MessageFormat22PluralSelectorProviderE,comdat
	.align 8
	.type	_ZTIN6icu_6713MessageFormat22PluralSelectorProviderE, @object
	.size	_ZTIN6icu_6713MessageFormat22PluralSelectorProviderE, 24
_ZTIN6icu_6713MessageFormat22PluralSelectorProviderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713MessageFormat22PluralSelectorProviderE
	.quad	_ZTIN6icu_6712PluralFormat14PluralSelectorE
	.weak	_ZTSN6icu_6713MessageFormat11DummyFormatE
	.section	.rodata._ZTSN6icu_6713MessageFormat11DummyFormatE,"aG",@progbits,_ZTSN6icu_6713MessageFormat11DummyFormatE,comdat
	.align 32
	.type	_ZTSN6icu_6713MessageFormat11DummyFormatE, @object
	.size	_ZTSN6icu_6713MessageFormat11DummyFormatE, 38
_ZTSN6icu_6713MessageFormat11DummyFormatE:
	.string	"N6icu_6713MessageFormat11DummyFormatE"
	.weak	_ZTIN6icu_6713MessageFormat11DummyFormatE
	.section	.data.rel.ro._ZTIN6icu_6713MessageFormat11DummyFormatE,"awG",@progbits,_ZTIN6icu_6713MessageFormat11DummyFormatE,comdat
	.align 8
	.type	_ZTIN6icu_6713MessageFormat11DummyFormatE, @object
	.size	_ZTIN6icu_6713MessageFormat11DummyFormatE, 24
_ZTIN6icu_6713MessageFormat11DummyFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713MessageFormat11DummyFormatE
	.quad	_ZTIN6icu_676FormatE
	.weak	_ZTSN6icu_6713MessageFormatE
	.section	.rodata._ZTSN6icu_6713MessageFormatE,"aG",@progbits,_ZTSN6icu_6713MessageFormatE,comdat
	.align 16
	.type	_ZTSN6icu_6713MessageFormatE, @object
	.size	_ZTSN6icu_6713MessageFormatE, 25
_ZTSN6icu_6713MessageFormatE:
	.string	"N6icu_6713MessageFormatE"
	.weak	_ZTIN6icu_6713MessageFormatE
	.section	.data.rel.ro._ZTIN6icu_6713MessageFormatE,"awG",@progbits,_ZTIN6icu_6713MessageFormatE,comdat
	.align 8
	.type	_ZTIN6icu_6713MessageFormatE, @object
	.size	_ZTIN6icu_6713MessageFormatE, 24
_ZTIN6icu_6713MessageFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713MessageFormatE
	.quad	_ZTIN6icu_676FormatE
	.weak	_ZTSN6icu_6721FormatNameEnumerationE
	.section	.rodata._ZTSN6icu_6721FormatNameEnumerationE,"aG",@progbits,_ZTSN6icu_6721FormatNameEnumerationE,comdat
	.align 32
	.type	_ZTSN6icu_6721FormatNameEnumerationE, @object
	.size	_ZTSN6icu_6721FormatNameEnumerationE, 33
_ZTSN6icu_6721FormatNameEnumerationE:
	.string	"N6icu_6721FormatNameEnumerationE"
	.weak	_ZTIN6icu_6721FormatNameEnumerationE
	.section	.data.rel.ro._ZTIN6icu_6721FormatNameEnumerationE,"awG",@progbits,_ZTIN6icu_6721FormatNameEnumerationE,comdat
	.align 8
	.type	_ZTIN6icu_6721FormatNameEnumerationE, @object
	.size	_ZTIN6icu_6721FormatNameEnumerationE, 24
_ZTIN6icu_6721FormatNameEnumerationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6721FormatNameEnumerationE
	.quad	_ZTIN6icu_6717StringEnumerationE
	.weak	_ZTVN6icu_6713MessageFormatE
	.section	.data.rel.ro._ZTVN6icu_6713MessageFormatE,"awG",@progbits,_ZTVN6icu_6713MessageFormatE,comdat
	.align 8
	.type	_ZTVN6icu_6713MessageFormatE, @object
	.size	_ZTVN6icu_6713MessageFormatE, 216
_ZTVN6icu_6713MessageFormatE:
	.quad	0
	.quad	_ZTIN6icu_6713MessageFormatE
	.quad	_ZN6icu_6713MessageFormatD1Ev
	.quad	_ZN6icu_6713MessageFormatD0Ev
	.quad	_ZNK6icu_6713MessageFormat17getDynamicClassIDEv
	.quad	_ZNK6icu_6713MessageFormateqERKNS_6FormatE
	.quad	_ZNK6icu_6713MessageFormat5cloneEv
	.quad	_ZNK6icu_6713MessageFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6713MessageFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.quad	_ZN6icu_6713MessageFormat9setLocaleERKNS_6LocaleE
	.quad	_ZNK6icu_6713MessageFormat9getLocaleEv
	.quad	_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.quad	_ZN6icu_6713MessageFormat12applyPatternERKNS_13UnicodeStringE29UMessagePatternApostropheModeP11UParseErrorR10UErrorCode
	.quad	_ZNK6icu_6713MessageFormat9toPatternERNS_13UnicodeStringE
	.quad	_ZN6icu_6713MessageFormat12adoptFormatsEPPNS_6FormatEi
	.quad	_ZN6icu_6713MessageFormat10setFormatsEPPKNS_6FormatEi
	.quad	_ZN6icu_6713MessageFormat11adoptFormatEiPNS_6FormatE
	.quad	_ZN6icu_6713MessageFormat9setFormatEiRKNS_6FormatE
	.quad	_ZN6icu_6713MessageFormat14getFormatNamesER10UErrorCode
	.quad	_ZN6icu_6713MessageFormat9getFormatERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6713MessageFormat9setFormatERKNS_13UnicodeStringERKNS_6FormatER10UErrorCode
	.quad	_ZN6icu_6713MessageFormat11adoptFormatERKNS_13UnicodeStringEPNS_6FormatER10UErrorCode
	.quad	_ZNK6icu_6713MessageFormat10getFormatsERi
	.quad	_ZNK6icu_6713MessageFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionERi
	.quad	_ZNK6icu_6713MessageFormat5parseERKNS_13UnicodeStringERiR10UErrorCode
	.weak	_ZTVN6icu_6713MessageFormat11DummyFormatE
	.section	.data.rel.ro._ZTVN6icu_6713MessageFormat11DummyFormatE,"awG",@progbits,_ZTVN6icu_6713MessageFormat11DummyFormatE,comdat
	.align 8
	.type	_ZTVN6icu_6713MessageFormat11DummyFormatE, @object
	.size	_ZTVN6icu_6713MessageFormat11DummyFormatE, 88
_ZTVN6icu_6713MessageFormat11DummyFormatE:
	.quad	0
	.quad	_ZTIN6icu_6713MessageFormat11DummyFormatE
	.quad	_ZN6icu_6713MessageFormat11DummyFormatD1Ev
	.quad	_ZN6icu_6713MessageFormat11DummyFormatD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6713MessageFormat11DummyFormateqERKNS_6FormatE
	.quad	_ZNK6icu_6713MessageFormat11DummyFormat5cloneEv
	.quad	_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6713MessageFormat11DummyFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.quad	_ZNK6icu_6713MessageFormat11DummyFormat6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode
	.weak	_ZTVN6icu_6721FormatNameEnumerationE
	.section	.data.rel.ro._ZTVN6icu_6721FormatNameEnumerationE,"awG",@progbits,_ZTVN6icu_6721FormatNameEnumerationE,comdat
	.align 8
	.type	_ZTVN6icu_6721FormatNameEnumerationE, @object
	.size	_ZTVN6icu_6721FormatNameEnumerationE, 104
_ZTVN6icu_6721FormatNameEnumerationE:
	.quad	0
	.quad	_ZTIN6icu_6721FormatNameEnumerationE
	.quad	_ZN6icu_6721FormatNameEnumerationD1Ev
	.quad	_ZN6icu_6721FormatNameEnumerationD0Ev
	.quad	_ZNK6icu_6721FormatNameEnumeration17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringEnumeration5cloneEv
	.quad	_ZNK6icu_6721FormatNameEnumeration5countER10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration4nextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode
	.quad	_ZN6icu_6721FormatNameEnumeration5snextER10UErrorCode
	.quad	_ZN6icu_6721FormatNameEnumeration5resetER10UErrorCode
	.quad	_ZNK6icu_6717StringEnumerationeqERKS0_
	.quad	_ZNK6icu_6717StringEnumerationneERKS0_
	.weak	_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE
	.section	.data.rel.ro.local._ZTVN6icu_6713MessageFormat22PluralSelectorProviderE,"awG",@progbits,_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE,comdat
	.align 8
	.type	_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE, @object
	.size	_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE, 40
_ZTVN6icu_6713MessageFormat22PluralSelectorProviderE:
	.quad	0
	.quad	_ZTIN6icu_6713MessageFormat22PluralSelectorProviderE
	.quad	_ZN6icu_6713MessageFormat22PluralSelectorProviderD1Ev
	.quad	_ZN6icu_6713MessageFormat22PluralSelectorProviderD0Ev
	.quad	_ZNK6icu_6713MessageFormat22PluralSelectorProvider6selectEPvdR10UErrorCode
	.local	_ZZN6icu_6721FormatNameEnumeration16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6721FormatNameEnumeration16getStaticClassIDEvE7classID,1,1
	.local	_ZZN6icu_6713MessageFormat16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6713MessageFormat16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.align 8
	.type	_ZL12OTHER_STRING, @object
	.size	_ZL12OTHER_STRING, 12
_ZL12OTHER_STRING:
	.value	111
	.value	116
	.value	104
	.value	101
	.value	114
	.value	0
	.align 8
	.type	_ZL11NULL_STRING, @object
	.size	_ZL11NULL_STRING, 10
_ZL11NULL_STRING:
	.value	110
	.value	117
	.value	108
	.value	108
	.value	0
	.align 16
	.type	_ZL11DATE_STYLES, @object
	.size	_ZL11DATE_STYLES, 20
_ZL11DATE_STYLES:
	.long	2
	.long	3
	.long	2
	.long	1
	.long	0
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL14DATE_STYLE_IDS, @object
	.size	_ZL14DATE_STYLE_IDS, 48
_ZL14DATE_STYLE_IDS:
	.quad	_ZL8ID_EMPTY
	.quad	_ZL8ID_SHORT
	.quad	_ZL9ID_MEDIUM
	.quad	_ZL7ID_LONG
	.quad	_ZL7ID_FULL
	.quad	0
	.section	.rodata
	.align 8
	.type	_ZL7ID_FULL, @object
	.size	_ZL7ID_FULL, 10
_ZL7ID_FULL:
	.value	102
	.value	117
	.value	108
	.value	108
	.value	0
	.align 8
	.type	_ZL7ID_LONG, @object
	.size	_ZL7ID_LONG, 10
_ZL7ID_LONG:
	.value	108
	.value	111
	.value	110
	.value	103
	.value	0
	.align 8
	.type	_ZL9ID_MEDIUM, @object
	.size	_ZL9ID_MEDIUM, 14
_ZL9ID_MEDIUM:
	.value	109
	.value	101
	.value	100
	.value	105
	.value	117
	.value	109
	.value	0
	.align 8
	.type	_ZL8ID_SHORT, @object
	.size	_ZL8ID_SHORT, 12
_ZL8ID_SHORT:
	.value	115
	.value	104
	.value	111
	.value	114
	.value	116
	.value	0
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL16NUMBER_STYLE_IDS, @object
	.size	_ZL16NUMBER_STYLE_IDS, 40
_ZL16NUMBER_STYLE_IDS:
	.quad	_ZL8ID_EMPTY
	.quad	_ZL11ID_CURRENCY
	.quad	_ZL10ID_PERCENT
	.quad	_ZL10ID_INTEGER
	.quad	0
	.section	.rodata
	.align 16
	.type	_ZL10ID_INTEGER, @object
	.size	_ZL10ID_INTEGER, 16
_ZL10ID_INTEGER:
	.value	105
	.value	110
	.value	116
	.value	101
	.value	103
	.value	101
	.value	114
	.value	0
	.align 16
	.type	_ZL10ID_PERCENT, @object
	.size	_ZL10ID_PERCENT, 16
_ZL10ID_PERCENT:
	.value	112
	.value	101
	.value	114
	.value	99
	.value	101
	.value	110
	.value	116
	.value	0
	.align 16
	.type	_ZL11ID_CURRENCY, @object
	.size	_ZL11ID_CURRENCY, 18
_ZL11ID_CURRENCY:
	.value	99
	.value	117
	.value	114
	.value	114
	.value	101
	.value	110
	.value	99
	.value	121
	.value	0
	.align 2
	.type	_ZL8ID_EMPTY, @object
	.size	_ZL8ID_EMPTY, 2
_ZL8ID_EMPTY:
	.zero	2
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL8TYPE_IDS, @object
	.size	_ZL8TYPE_IDS, 56
_ZL8TYPE_IDS:
	.quad	_ZL9ID_NUMBER
	.quad	_ZL7ID_DATE
	.quad	_ZL7ID_TIME
	.quad	_ZL11ID_SPELLOUT
	.quad	_ZL10ID_ORDINAL
	.quad	_ZL11ID_DURATION
	.quad	0
	.section	.rodata
	.align 16
	.type	_ZL11ID_DURATION, @object
	.size	_ZL11ID_DURATION, 18
_ZL11ID_DURATION:
	.value	100
	.value	117
	.value	114
	.value	97
	.value	116
	.value	105
	.value	111
	.value	110
	.value	0
	.align 16
	.type	_ZL10ID_ORDINAL, @object
	.size	_ZL10ID_ORDINAL, 16
_ZL10ID_ORDINAL:
	.value	111
	.value	114
	.value	100
	.value	105
	.value	110
	.value	97
	.value	108
	.value	0
	.align 16
	.type	_ZL11ID_SPELLOUT, @object
	.size	_ZL11ID_SPELLOUT, 18
_ZL11ID_SPELLOUT:
	.value	115
	.value	112
	.value	101
	.value	108
	.value	108
	.value	111
	.value	117
	.value	116
	.value	0
	.align 8
	.type	_ZL7ID_TIME, @object
	.size	_ZL7ID_TIME, 10
_ZL7ID_TIME:
	.value	116
	.value	105
	.value	109
	.value	101
	.value	0
	.align 8
	.type	_ZL7ID_DATE, @object
	.size	_ZL7ID_DATE, 10
_ZL7ID_DATE:
	.value	100
	.value	97
	.value	116
	.value	101
	.value	0
	.align 8
	.type	_ZL9ID_NUMBER, @object
	.size	_ZL9ID_NUMBER, 14
_ZL9ID_NUMBER:
	.value	110
	.value	117
	.value	109
	.value	98
	.value	101
	.value	114
	.value	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	6
	.long	6
	.long	6
	.long	6
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
