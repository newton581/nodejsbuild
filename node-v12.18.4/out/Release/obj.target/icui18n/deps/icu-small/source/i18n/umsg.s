	.file	"umsg.cpp"
	.text
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.type	umsg_vparse_67.part.0, @function
umsg_vparse_67.part.0:
.LFB3270:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$200, %rsp
	movq	%r9, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %edx
	je	.L56
.L2:
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, %r15
	movq	%rax, -224(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	-200(%rbp), %rcx
	movq	%r12, %rdi
	call	*192(%rax)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movl	(%r14), %esi
	movq	%rcx, -128(%rbp)
	movl	$2, %ecx
	movq	%rax, -208(%rbp)
	movw	%cx, -120(%rbp)
	testl	%esi, %esi
	jle	.L3
	movq	%rax, %r13
	xorl	%r15d, %r15d
	leaq	.L6(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r13, %rdi
	call	_ZNK6icu_6711Formattable7getTypeEv@PLT
	cmpl	$6, %eax
	ja	.L4
	movl	%eax, %eax
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L6:
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L9-.L6
	.long	.L8-.L6
	.long	.L5-.L6
	.long	.L7-.L6
	.long	.L5-.L6
	.text
	.p2align 4,,10
	.p2align 3
.L10:
	movl	(%rbx), %eax
	cmpl	$47, %eax
	ja	.L15
	movl	%eax, %edx
	addl	$8, %eax
	addq	16(%rbx), %rdx
	movl	%eax, (%rbx)
.L16:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L14
	movsd	8(%r13), %xmm0
	movsd	%xmm0, (%rax)
.L4:
	addl	$1, %r15d
	addq	$112, %r13
	cmpl	%r15d, (%r14)
	jg	.L25
.L3:
	movq	-208(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L26
	movq	-8(%rsi), %rax
	leaq	0(,%rax,8), %rbx
	subq	%rax, %rbx
	salq	$4, %rbx
	addq	%rsi, %rbx
	cmpq	%rbx, %rsi
	je	.L27
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L28:
	movq	-112(%rbx), %rax
	subq	$112, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, %r12
	jne	.L28
.L27:
	movq	-208(%rbp), %rdi
	subq	$8, %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L26:
	leaq	-128(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-224(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	(%rbx), %eax
	cmpl	$47, %eax
	ja	.L21
	movl	%eax, %edx
	addl	$8, %eax
	addq	16(%rbx), %rdx
	movl	%eax, (%rbx)
.L22:
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	movq	%rcx, -216(%rbp)
	je	.L14
	movq	8(%r13), %rsi
	leaq	-128(%rbp), %rdi
	movq	%rdi, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	-120(%rbp), %eax
	movq	-232(%rbp), %rdi
	movq	-216(%rbp), %rcx
	testw	%ax, %ax
	js	.L23
	sarl	$5, %eax
.L24:
	xorl	%r8d, %r8d
	movl	%eax, %edx
	xorl	%esi, %esi
	movq	%rcx, -232(%rbp)
	movl	%eax, -216(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-232(%rbp), %rcx
	movslq	-216(%rbp), %rax
	xorl	%edx, %edx
	movw	%dx, (%rcx,%rax,2)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L9:
	movl	(%rbx), %eax
	cmpl	$47, %eax
	ja	.L17
	movl	%eax, %edx
	addl	$8, %eax
	addq	16(%rbx), %rdx
	movl	%eax, (%rbx)
.L18:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L14
	movq	8(%r13), %rdx
	movl	%edx, (%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L7:
	movl	(%rbx), %eax
	cmpl	$47, %eax
	ja	.L19
	movl	%eax, %edx
	addl	$8, %eax
	addq	16(%rbx), %rdx
	movl	%eax, (%rbx)
.L20:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L14
	movq	8(%r13), %rdx
	movq	%rdx, (%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L14:
	movq	-200(%rbp), %rax
	movl	$1, (%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L15:
	movq	8(%rbx), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%rbx)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L21:
	movq	8(%rbx), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%rbx)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L17:
	movq	8(%rbx), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%rbx)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L19:
	movq	8(%rbx), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%rbx)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L23:
	movl	-116(%rbp), %eax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%rsi, %rdi
	movq	%rsi, -208(%rbp)
	call	u_strlen_67@PLT
	movq	-208(%rbp), %rsi
	movl	%eax, %edx
	jmp	.L2
.L57:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	umsg_vparse_67.part.0.cold, @function
umsg_vparse_67.part.0.cold:
.LFSB3270:
.L5:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3270:
	.text
	.size	umsg_vparse_67.part.0, .-umsg_vparse_67.part.0
	.section	.text.unlikely
	.size	umsg_vparse_67.part.0.cold, .-umsg_vparse_67.part.0.cold
.LCOLDE0:
	.text
.LHOTE0:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720MessageFormatAdapter14getArgTypeListERKNS_13MessageFormatERi
	.type	_ZN6icu_6720MessageFormatAdapter14getArgTypeListERKNS_13MessageFormatERi, @function
_ZN6icu_6720MessageFormatAdapter14getArgTypeListERKNS_13MessageFormatERi:
.LFB2527:
	.cfi_startproc
	endbr64
	movl	704(%rdi), %edx
	movq	696(%rdi), %rax
	movl	%edx, (%rsi)
	ret
	.cfi_endproc
.LFE2527:
	.size	_ZN6icu_6720MessageFormatAdapter14getArgTypeListERKNS_13MessageFormatERi, .-_ZN6icu_6720MessageFormatAdapter14getArgTypeListERKNS_13MessageFormatERi
	.p2align 4
	.globl	umsg_close_67
	.type	umsg_close_67, @function
umsg_close_67:
.LFB2537:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L59
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L59:
	ret
	.cfi_endproc
.LFE2537:
	.size	umsg_close_67, .-umsg_close_67
	.p2align 4
	.globl	umsg_clone_67
	.type	umsg_clone_67, @function
umsg_clone_67:
.LFB2538:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L61
	testq	%rdi, %rdi
	je	.L70
	movq	(%rdi), %rax
	call	*32(%rax)
	testq	%rax, %rax
	je	.L71
.L61:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$1, (%rsi)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$7, (%rbx)
	jmp	.L61
	.cfi_endproc
.LFE2538:
	.size	umsg_clone_67, .-umsg_clone_67
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	""
	.text
	.p2align 4
	.globl	umsg_getLocale_67
	.type	umsg_getLocale_67, @function
umsg_getLocale_67:
.LFB2540:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L76
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*72(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	40(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore 6
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
.LFE2540:
	.size	umsg_getLocale_67, .-umsg_getLocale_67
	.p2align 4
	.globl	umsg_applyPattern_67
	.type	umsg_applyPattern_67, @function
umsg_applyPattern_67:
.LFB2541:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L81
	movl	(%r8), %eax
	movq	%r8, %r12
	testl	%eax, %eax
	jg	.L81
	movq	%rdi, %r14
	testq	%rdi, %rdi
	je	.L83
	testq	%rsi, %rsi
	movq	%rcx, %r13
	sete	%cl
	testl	%edx, %edx
	setne	%al
	testb	%al, %cl
	jne	.L83
	cmpl	$-1, %edx
	jl	.L83
	testq	%r13, %r13
	leaq	-128(%rbp), %rax
	leaq	-192(%rbp), %r15
	cmove	%rax, %r13
	movq	(%rdi), %rax
	movq	%r15, %rdi
	movq	88(%rax), %rbx
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movq	%r14, %rdi
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	*%rbx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movl	$1, (%r12)
	jmp	.L81
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2541:
	.size	umsg_applyPattern_67, .-umsg_applyPattern_67
	.p2align 4
	.globl	umsg_toPattern_67
	.type	umsg_toPattern_67, @function
umsg_toPattern_67:
.LFB2542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L105
	movq	%rcx, %r13
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jg	.L105
	movq	%rdi, %r14
	testq	%rdi, %rdi
	je	.L99
	movl	%edx, %r12d
	testl	%edx, %edx
	js	.L99
	movq	%rsi, %rbx
	jle	.L100
	testq	%rsi, %rsi
	je	.L99
.L100:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	testq	%rbx, %rbx
	jne	.L106
	leaq	-128(%rbp), %r15
	testl	%r12d, %r12d
	jne	.L106
.L101:
	movq	(%r14), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*104(%rax)
	movl	%r12d, %edx
	leaq	-136(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	%rbx, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L97:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	leaq	-128(%rbp), %r15
	movl	%r12d, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$1, 0(%r13)
	movl	$-1, %r12d
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$-1, %r12d
	jmp	.L97
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2542:
	.size	umsg_toPattern_67, .-umsg_toPattern_67
	.section	.text.unlikely
.LCOLDB2:
	.text
.LHOTB2:
	.p2align 4
	.globl	umsg_vformat_67
	.type	umsg_vformat_67, @function
umsg_vformat_67:
.LFB2544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$200, %rsp
	movq	%rdi, -200(%rbp)
	movq	%rsi, -224(%rbp)
	movl	%edx, -184(%rbp)
	movq	%r8, -192(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testq	%r8, %r8
	je	.L162
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jg	.L162
	testq	%rdi, %rdi
	je	.L120
	testl	%edx, %edx
	js	.L120
	jle	.L121
	testq	%rsi, %rsi
	je	.L120
.L121:
	movq	-200(%rbp), %rax
	movq	696(%rax), %r12
	movl	704(%rax), %eax
	movl	%eax, -180(%rbp)
	testl	%eax, %eax
	jne	.L187
	movl	$120, %edi
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L155
	movl	$1, %edx
.L124:
	movq	-208(%rbp), %rax
	movq	%rdx, (%rax)
	leaq	8(%rax), %r14
	leaq	-1(%rdx), %rax
.L159:
	movq	%r14, %r13
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r13, %rdi
	subq	$1, %r15
	addq	$112, %r13
	call	_ZN6icu_6711FormattableC1Ev@PLT
	cmpq	$-1, %r15
	jne	.L127
	movl	-180(%rbp), %eax
	testl	%eax, %eax
	jle	.L188
.L156:
	movq	%r14, %rcx
	xorl	%eax, %eax
	movq	%r14, -216(%rbp)
	leaq	.L130(%rip), %r15
	movq	%r12, %r14
	movq	%rax, %r13
	movq	%rbx, %r12
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L150:
	cmpl	$6, (%r14,%r13,4)
	ja	.L128
	movl	(%r14,%r13,4), %eax
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L130:
	.long	.L136-.L130
	.long	.L135-.L130
	.long	.L134-.L130
	.long	.L133-.L130
	.long	.L129-.L130
	.long	.L131-.L130
	.long	.L129-.L130
	.text
	.p2align 4,,10
	.p2align 3
.L129:
	movl	(%r12), %eax
	cmpl	$47, %eax
	ja	.L149
	addl	$8, %eax
	movl	%eax, (%r12)
	.p2align 4,,10
	.p2align 3
.L139:
	addq	$1, %r13
	addq	$112, %rbx
	cmpl	%r13d, -180(%rbp)
	jg	.L150
	movq	-216(%rbp), %r14
	movl	$2, %esi
	leaq	-128(%rbp), %r12
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	leaq	-160(%rbp), %rbx
	movq	%r12, %rcx
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	-192(%rbp), %r9
	movl	-180(%rbp), %edx
	movw	%si, -120(%rbp)
	movq	%rbx, %r8
	movq	-200(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, -160(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -152(%rbp)
	movl	$0, -144(%rbp)
	call	_ZNK6icu_6713MessageFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode@PLT
	testq	%r14, %r14
	je	.L151
.L157:
	movq	-8(%r14), %rax
	leaq	0(,%rax,8), %r13
	subq	%rax, %r13
	salq	$4, %r13
	addq	%r14, %r13
	cmpq	%r14, %r13
	je	.L153
	.p2align 4,,10
	.p2align 3
.L152:
	movq	-112(%r13), %rax
	subq	$112, %r13
	movq	%r13, %rdi
	call	*(%rax)
	cmpq	%r14, %r13
	jne	.L152
.L153:
	movq	-208(%rbp), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L151:
	movq	-192(%rbp), %rax
	movl	$-1, %r13d
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L154
	movq	%rax, %rcx
	movl	-184(%rbp), %edx
	movq	-224(%rbp), %rax
	movq	%r12, %rdi
	leaq	-168(%rbp), %rsi
	movq	%rax, -168(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r13d
.L154:
	movq	%rbx, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L118:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L189
	addq	$200, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	movl	(%r12), %eax
	cmpl	$47, %eax
	ja	.L144
	movl	%eax, %esi
	addl	$8, %eax
	addq	16(%r12), %rsi
	movl	%eax, (%r12)
.L145:
	movq	(%rsi), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711Formattable8setInt64El@PLT
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L133:
	movl	(%r12), %eax
	cmpl	$47, %eax
	ja	.L146
	movl	%eax, %esi
	addl	$8, %eax
	addq	16(%r12), %rsi
	movl	%eax, (%r12)
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L148
.L190:
	leaq	-128(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-232(%rbp), %r8
	movq	%rbx, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6711Formattable9setStringERKNS_13UnicodeStringE@PLT
	movq	-232(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L135:
	movl	4(%r12), %eax
	cmpl	$175, %eax
	ja	.L140
	movl	%eax, %esi
	addl	$16, %eax
	addq	16(%r12), %rsi
	movl	%eax, 4(%r12)
.L141:
	movsd	(%rsi), %xmm0
	movq	%rbx, %rdi
	call	_ZN6icu_6711Formattable9setDoubleEd@PLT
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L136:
	movl	4(%r12), %eax
	cmpl	$175, %eax
	ja	.L137
	movl	%eax, %esi
	addl	$16, %eax
	addq	16(%r12), %rsi
	movl	%eax, 4(%r12)
.L138:
	movsd	(%rsi), %xmm0
	movq	%rbx, %rdi
	call	_ZN6icu_6711Formattable7setDateEd@PLT
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L134:
	movl	(%r12), %eax
	cmpl	$47, %eax
	ja	.L142
	movl	%eax, %esi
	addl	$8, %eax
	addq	16(%r12), %rsi
	movl	%eax, (%r12)
.L143:
	movl	(%rsi), %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6711Formattable7setLongEi@PLT
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L187:
	movslq	%eax, %rdx
	movabsq	$82351536043346212, %rax
	cmpq	%rax, %rdx
	ja	.L123
	leaq	0(,%rdx,8), %rdi
	movq	%rdx, -216(%rbp)
	subq	%rdx, %rdi
	salq	$4, %rdi
	addq	$8, %rdi
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	-216(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, -208(%rbp)
	jne	.L124
.L125:
	cmpl	$0, -180(%rbp)
	jle	.L155
	movq	$-8, -208(%rbp)
	xorl	%r14d, %r14d
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L149:
	addq	$8, 8(%r12)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L140:
	movq	8(%r12), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r12)
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L142:
	movq	8(%r12), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r12)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L146:
	movq	8(%r12), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r12)
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.L190
.L148:
	movq	-192(%rbp), %rax
	movl	$1, (%rax)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L144:
	movq	8(%r12), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r12)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L137:
	movq	8(%r12), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r12)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L123:
	movq	$-1, %rdi
	movq	%rdx, -216(%rbp)
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L125
	movq	-216(%rbp), %rdx
	leaq	8(%rax), %r14
	movq	%rdx, (%rax)
	movq	%rdx, %rax
	subq	$1, %rax
	jns	.L159
	movl	-180(%rbp), %eax
	testl	%eax, %eax
	jg	.L156
	.p2align 4,,10
	.p2align 3
.L188:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	leaq	-128(%rbp), %r12
	movq	%r14, %rsi
	movq	%rax, -128(%rbp)
	leaq	-160(%rbp), %rbx
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	-192(%rbp), %r9
	movw	%dx, -120(%rbp)
	movq	-200(%rbp), %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-180(%rbp), %edx
	movq	%rax, -160(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -152(%rbp)
	movl	$0, -144(%rbp)
	call	_ZNK6icu_6713MessageFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode@PLT
	jmp	.L157
.L120:
	movq	-192(%rbp), %rax
	movl	$-1, %r13d
	movl	$1, (%rax)
	jmp	.L118
.L162:
	movl	$-1, %r13d
	jmp	.L118
.L155:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-160(%rbp), %rbx
	xorl	%esi, %esi
	movq	-192(%rbp), %r9
	movq	%rax, -128(%rbp)
	leaq	-128(%rbp), %r12
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movl	-180(%rbp), %edx
	movq	-200(%rbp), %rdi
	movq	%rax, -160(%rbp)
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	$4294967295, %eax
	movw	$2, -120(%rbp)
	movq	%rax, -152(%rbp)
	movl	$0, -144(%rbp)
	call	_ZNK6icu_6713MessageFormat6formatEPKNS_11FormattableEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode@PLT
	jmp	.L151
.L189:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	umsg_vformat_67.cold, @function
umsg_vformat_67.cold:
.LFSB2544:
.L128:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE2544:
	.text
	.size	umsg_vformat_67, .-umsg_vformat_67
	.section	.text.unlikely
	.size	umsg_vformat_67.cold, .-umsg_vformat_67.cold
.LCOLDE2:
	.text
.LHOTE2:
	.p2align 4
	.globl	umsg_format_67
	.type	umsg_format_67, @function
umsg_format_67:
.LFB2543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L192
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L192:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	-208(%rbp), %r9
	leaq	16(%rbp), %rax
	movq	%rcx, %r8
	movq	%rax, -200(%rbp)
	movq	%r9, %rcx
	leaq	-176(%rbp), %rax
	movl	$32, -208(%rbp)
	movl	$48, -204(%rbp)
	movq	%rax, -192(%rbp)
	call	umsg_vformat_67
	movq	-184(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L195
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L195:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2543:
	.size	umsg_format_67, .-umsg_format_67
	.p2align 4
	.globl	umsg_parse_67
	.type	umsg_parse_67, @function
umsg_parse_67:
.LFB2545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L197
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L197:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movl	$40, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movl	$48, -204(%rbp)
	movq	%rax, -192(%rbp)
	testq	%r8, %r8
	je	.L196
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L196
	testq	%rdi, %rdi
	je	.L199
	testq	%rsi, %rsi
	je	.L199
	cmpl	$-1, %edx
	jl	.L199
	testq	%rcx, %rcx
	je	.L199
	movq	%r8, %r9
	leaq	-208(%rbp), %r8
	call	umsg_vparse_67.part.0
.L196:
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L207
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movl	$1, (%r8)
	jmp	.L196
.L207:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2545:
	.size	umsg_parse_67, .-umsg_parse_67
	.p2align 4
	.globl	umsg_vparse_67
	.type	umsg_vparse_67, @function
umsg_vparse_67:
.LFB2546:
	.cfi_startproc
	endbr64
	testq	%r9, %r9
	je	.L208
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L208
	testq	%rdi, %rdi
	je	.L210
	testq	%rsi, %rsi
	je	.L210
	cmpl	$-1, %edx
	jl	.L210
	testq	%rcx, %rcx
	je	.L210
	jmp	umsg_vparse_67.part.0
	.p2align 4,,10
	.p2align 3
.L208:
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	movl	$1, (%r9)
	ret
	.cfi_endproc
.LFE2546:
	.size	umsg_vparse_67, .-umsg_vparse_67
	.p2align 4
	.globl	umsg_autoQuoteApostrophe_67
	.type	umsg_autoQuoteApostrophe_67, @function
umsg_autoQuoteApostrophe_67:
.LFB2547:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	je	.L290
	movl	%esi, %eax
	movl	%ecx, %esi
	movq	%r8, %rcx
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jg	.L290
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	testq	%rdi, %rdi
	je	.L218
	cmpl	$-1, %eax
	jl	.L218
	movq	%rdx, %r12
	testq	%rdx, %rdx
	jne	.L219
	testl	%esi, %esi
	jg	.L218
.L219:
	cmpl	$-1, %eax
	je	.L298
	testl	%eax, %eax
	jle	.L250
.L302:
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L245:
	movslq	%edx, %r11
	movzwl	(%rbx,%r11,2), %r13d
	cmpl	$2, %r10d
	je	.L222
	cmpl	$3, %r10d
	je	.L223
	cmpl	$1, %r10d
	jne	.L225
.L224:
	leal	-123(%r13), %r10d
	leal	1(%r9), %r8d
	addl	$1, %edx
	testw	$-3, %r10w
	je	.L233
	cmpw	$39, %r13w
	je	.L299
	cmpl	%r9d, %esi
	jle	.L235
	movslq	%r9d, %r10
	movl	$39, %r14d
	addl	$2, %r9d
	movw	%r14w, (%r12,%r10,2)
	leaq	(%r10,%r10), %r11
	cmpl	%r8d, %esi
	jle	.L295
	movw	%r13w, 2(%r12,%r11)
	cmpl	%edx, %eax
	jg	.L294
	.p2align 4,,10
	.p2align 3
.L267:
	movl	%r9d, %r8d
.L246:
	addq	$16, %rsp
	movq	%r12, %rdi
	movl	%r8d, %edx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	u_terminateUChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	cmpl	%r11d, %esi
	jle	.L295
	movl	$39, %r8d
	movw	%r8w, (%r12,%r11,2)
	cmpl	%r10d, %eax
	jle	.L267
.L294:
	movslq	%edx, %r11
	movzwl	(%rbx,%r11,2), %r13d
.L225:
	leal	1(%rdx), %r10d
	movslq	%r9d, %r8
	subl	%r9d, %edx
	subq	%r11, %r8
	movslq	%r10d, %r10
	leaq	(%r12,%r8,2), %r14
	movslq	%r10d, %r11
	leal	1(%r9), %r8d
	cmpw	$39, %r13w
	je	.L226
.L300:
	cmpw	$123, %r13w
	je	.L227
	cmpl	%r9d, %esi
	jle	.L253
	leal	(%rdx,%r8), %r9d
	movw	%r13w, -2(%r14,%r10,2)
	cmpl	%r9d, %eax
	jle	.L246
	movzwl	(%rbx,%r10,2), %r13d
	movl	%r8d, %r9d
	addq	$1, %r10
	leal	1(%r9), %r8d
	movslq	%r10d, %r11
	cmpw	$39, %r13w
	jne	.L300
	.p2align 4,,10
	.p2align 3
.L226:
	cmpl	%r9d, %esi
	jle	.L251
	movslq	%r9d, %r9
	movl	$39, %r14d
	movw	%r14w, (%r12,%r9,2)
	cmpl	%r11d, %eax
	jle	.L269
	movzwl	(%rbx,%r11,2), %r13d
	movl	%r10d, %edx
	movl	%r8d, %r9d
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L233:
	cmpl	%r9d, %esi
	jle	.L268
	movslq	%r9d, %r9
	movw	%r13w, (%r12,%r9,2)
	cmpl	%edx, %eax
	jle	.L269
	movslq	%edx, %r9
	movzwl	(%rbx,%r9,2), %r13d
	movl	%r8d, %r9d
	.p2align 4,,10
	.p2align 3
.L222:
	addl	$1, %edx
	movslq	%r9d, %r8
	movslq	%edx, %r10
.L239:
	movslq	%r8d, %r11
	leal	1(%r8), %r9d
	movl	%r10d, %edx
	cmpw	$39, %r13w
	je	.L238
	cmpl	%r8d, %esi
	jle	.L297
	movw	%r13w, (%r12,%r8,2)
	addq	$1, %r8
	cmpl	%r10d, %eax
	jle	.L247
	movzwl	(%rbx,%r10,2), %r13d
	addq	$1, %r10
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L227:
	addl	$1, %edi
	cmpl	%r9d, %esi
	jle	.L254
	movslq	%r9d, %r9
	movl	$123, %r13d
	movw	%r13w, (%r12,%r9,2)
	cmpl	%r11d, %eax
	jle	.L246
	movzwl	(%rbx,%r11,2), %r13d
	movl	%r10d, %edx
	movl	%r8d, %r9d
	.p2align 4,,10
	.p2align 3
.L223:
	addl	$1, %edx
	movslq	%r9d, %r8
	movslq	%edx, %r10
.L232:
	movslq	%r8d, %r11
	leal	1(%r8), %r9d
	movl	%r10d, %edx
	cmpw	$123, %r13w
	je	.L240
	cmpw	$125, %r13w
	je	.L241
	cmpl	%r8d, %esi
	jle	.L296
	movw	%r13w, (%r12,%r8,2)
	cmpl	%r10d, %eax
	jle	.L267
.L293:
	movzwl	(%rbx,%r10,2), %r13d
	addq	$1, %r8
	addq	$1, %r10
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L254:
	movl	%r10d, %edx
	movl	%r8d, %r9d
.L296:
	movl	$2, %r11d
	movl	$3, %r10d
.L229:
	cmpl	%edx, %eax
	jg	.L245
	movl	%r9d, %r8d
	cmpl	$1, %r11d
	ja	.L246
.L247:
	leal	1(%r9), %r8d
	cmpl	%r9d, %esi
	jle	.L246
	movslq	%r9d, %r9
	movl	$39, %eax
	movw	%ax, (%r12,%r9,2)
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L268:
	movl	%r8d, %r9d
.L297:
	movl	$1, %r11d
	movl	$2, %r10d
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L299:
	cmpl	%r9d, %esi
	jg	.L301
	movl	%r8d, %r9d
.L295:
	movl	$-1, %r11d
	xorl	%r10d, %r10d
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L241:
	subl	$1, %edi
	jne	.L244
	cmpl	%r11d, %esi
	jle	.L260
	movl	$125, %r14d
	leal	1(%r11), %r9d
	movw	%r14w, (%r12,%r11,2)
	cmpl	%r10d, %eax
	jg	.L294
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L240:
	addl	$1, %edi
	cmpl	%r8d, %esi
	jle	.L296
	movl	$123, %r11d
	movw	%r11w, (%r12,%r8,2)
	cmpl	%r10d, %eax
	jg	.L293
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L244:
	cmpl	%r8d, %esi
	jle	.L296
	movl	$125, %r13d
	movw	%r13w, (%r12,%r8,2)
	cmpl	%r10d, %eax
	jg	.L293
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L301:
	movslq	%r9d, %r9
	movl	$39, %r10d
	movw	%r10w, (%r12,%r9,2)
	cmpl	%edx, %eax
	jle	.L246
	movslq	%edx, %r11
	movl	%r8d, %r9d
	movzwl	(%rbx,%r11,2), %r13d
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L251:
	movl	%r10d, %edx
	movl	%r8d, %r9d
	xorl	%r11d, %r11d
	movl	$1, %r10d
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L253:
	movl	%r10d, %edx
	movl	%r8d, %r9d
	movl	$-1, %r11d
	xorl	%r10d, %r10d
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L235:
	addl	$2, %r9d
	cmpl	%r8d, %esi
	jle	.L295
	movslq	%r8d, %r8
	movw	%r13w, (%r12,%r8,2)
	cmpl	%edx, %eax
	jg	.L294
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L260:
	xorl	%r10d, %r10d
	movl	$-1, %r11d
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L298:
	movq	%rbx, %rdi
	movq	%rcx, -48(%rbp)
	movl	%esi, -36(%rbp)
	call	u_strlen_67@PLT
	movq	-48(%rbp), %rcx
	movl	-36(%rbp), %esi
	testl	%eax, %eax
	jg	.L302
.L250:
	xorl	%r8d, %r8d
	jmp	.L246
.L269:
	movl	%r8d, %r9d
	jmp	.L247
.L218:
	movl	$1, (%rcx)
	addq	$16, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L290:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE2547:
	.size	umsg_autoQuoteApostrophe_67, .-umsg_autoQuoteApostrophe_67
	.p2align 4
	.type	umsg_open_67.part.0, @function
umsg_open_67.part.0:
.LFB3271:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	leaq	-368(%rbp), %rax
	movl	%esi, %ecx
	cmove	%rax, %r12
	cmpl	$-1, %esi
	je	.L313
.L305:
	xorl	%esi, %esi
	leaq	-432(%rbp), %r15
	cmpl	$-1, %r13d
	movq	%r9, -456(%rbp)
	leaq	-440(%rbp), %rdx
	sete	%sil
	movq	%r15, %rdi
	movq	%r14, -440(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-456(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-288(%rbp), %r14
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	$816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L306
	movq	%rax, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN6icu_6713MessageFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER11UParseErrorR10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L314
.L307:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L315
	addq	$424, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	cmpb	$0, 712(%r13)
	je	.L307
	movl	$65804, (%rbx)
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%rdx, -456(%rbp)
	call	u_strlen_67@PLT
	movq	-456(%rbp), %r9
	movl	%eax, %ecx
	jmp	.L305
.L306:
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	$7, (%rbx)
	jmp	.L307
.L315:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3271:
	.size	umsg_open_67.part.0, .-umsg_open_67.part.0
	.p2align 4
	.globl	umsg_open_67
	.type	umsg_open_67, @function
umsg_open_67:
.LFB2536:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	je	.L316
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L316
	testq	%rdi, %rdi
	je	.L322
	cmpl	$-1, %esi
	jl	.L322
	jmp	umsg_open_67.part.0
	.p2align 4,,10
	.p2align 3
.L322:
	movl	$1, (%r8)
.L316:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2536:
	.size	umsg_open_67, .-umsg_open_67
	.p2align 4
	.globl	u_parseMessage_67
	.type	u_parseMessage_67, @function
u_parseMessage_67:
.LFB2532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsi, %rdi
	movl	%edx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%r9, %rbx
	subq	$248, %rsp
	testb	%al, %al
	je	.L325
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L325:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movl	$48, -240(%rbp)
	movq	%rax, -232(%rbp)
	leaq	-208(%rbp), %rax
	movl	$48, -236(%rbp)
	movq	%rax, -224(%rbp)
	testq	%rbx, %rbx
	je	.L324
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L324
	testq	%rdi, %rdi
	je	.L336
	cmpl	$-1, %esi
	jl	.L336
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movq	%r10, %rdx
	call	umsg_open_67.part.0
	movl	$0, -244(%rbp)
	movq	%rax, %rdi
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L331
	cmpl	$-1, %r13d
	setl	%dl
	testq	%r12, %r12
	sete	%al
	orb	%al, %dl
	jne	.L337
	testq	%rdi, %rdi
	je	.L337
	leaq	-244(%rbp), %rcx
	movq	%rbx, %r9
	movl	%r13d, %edx
	movq	%r12, %rsi
	leaq	-240(%rbp), %r8
	movq	%rdi, -264(%rbp)
	call	umsg_vparse_67.part.0
	movq	-264(%rbp), %rdi
.L334:
	movq	(%rdi), %rax
	call	*8(%rax)
.L324:
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L342
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movl	$1, (%rbx)
.L331:
	testq	%rdi, %rdi
	jne	.L334
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L336:
	movl	$1, (%rbx)
	jmp	.L324
.L342:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2532:
	.size	u_parseMessage_67, .-u_parseMessage_67
	.p2align 4
	.globl	u_parseMessageWithError_67
	.type	u_parseMessageWithError_67, @function
u_parseMessageWithError_67:
.LFB2534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsi, %rdi
	movl	%edx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -40
	movq	16(%rbp), %rbx
	testb	%al, %al
	je	.L344
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L344:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	24(%rbp), %rax
	movl	$48, -240(%rbp)
	movq	%rax, -232(%rbp)
	leaq	-208(%rbp), %rax
	movl	$48, -236(%rbp)
	movq	%rax, -224(%rbp)
	testq	%rbx, %rbx
	je	.L343
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L343
	testq	%rdi, %rdi
	je	.L355
	cmpl	$-1, %esi
	jl	.L355
	movq	%rbx, %r8
	movq	%r9, %rcx
	movq	%r10, %rdx
	call	umsg_open_67.part.0
	movl	$0, -244(%rbp)
	movq	%rax, %rdi
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L350
	cmpl	$-1, %r13d
	setl	%dl
	testq	%r12, %r12
	sete	%al
	orb	%al, %dl
	jne	.L356
	testq	%rdi, %rdi
	je	.L356
	leaq	-244(%rbp), %rcx
	movq	%rbx, %r9
	movl	%r13d, %edx
	movq	%r12, %rsi
	leaq	-240(%rbp), %r8
	movq	%rdi, -264(%rbp)
	call	umsg_vparse_67.part.0
	movq	-264(%rbp), %rdi
.L353:
	movq	(%rdi), %rax
	call	*8(%rax)
.L343:
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L361
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	movl	$1, (%rbx)
.L350:
	testq	%rdi, %rdi
	jne	.L353
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L355:
	movl	$1, (%rbx)
	jmp	.L343
.L361:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2534:
	.size	u_parseMessageWithError_67, .-u_parseMessageWithError_67
	.p2align 4
	.globl	u_formatMessage_67
	.type	u_formatMessage_67, @function
u_formatMessage_67:
.LFB2528:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsi, %rdi
	movl	%edx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%r9, %rbx
	subq	$208, %rsp
	testb	%al, %al
	je	.L363
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L363:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-240(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, -232(%rbp)
	leaq	-208(%rbp), %rax
	movl	$48, -240(%rbp)
	movl	$48, -236(%rbp)
	movq	%rax, -224(%rbp)
	testq	%rbx, %rbx
	je	.L375
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L376
	testq	%rdi, %rdi
	je	.L370
	cmpl	$-1, %esi
	jl	.L370
	movq	%r10, %rdx
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	call	umsg_open_67.part.0
	movq	%r13, %rsi
	movq	%rbx, %r8
	movl	%r14d, %edx
	leaq	-240(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	umsg_vformat_67
	movl	%eax, %r13d
	testq	%r12, %r12
	je	.L362
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L362:
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L377
	addq	$208, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	movl	$1, (%rbx)
.L376:
	leaq	-240(%rbp), %rcx
	movq	%rbx, %r8
.L375:
	movq	%r13, %rsi
	movl	%r14d, %edx
	xorl	%edi, %edi
	call	umsg_vformat_67
	movl	%eax, %r13d
	jmp	.L362
.L377:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2528:
	.size	u_formatMessage_67, .-u_formatMessage_67
	.p2align 4
	.globl	u_formatMessageWithError_67
	.type	u_formatMessageWithError_67, @function
u_formatMessageWithError_67:
.LFB2530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsi, %rdi
	movl	%edx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rbp), %rbx
	testb	%al, %al
	je	.L379
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L379:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	24(%rbp), %rax
	leaq	-240(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, -232(%rbp)
	leaq	-208(%rbp), %rax
	movl	$48, -240(%rbp)
	movl	$48, -236(%rbp)
	movq	%rax, -224(%rbp)
	testq	%rbx, %rbx
	je	.L391
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L392
	testq	%rdi, %rdi
	je	.L386
	cmpl	$-1, %esi
	jl	.L386
	movq	%r9, %rcx
	movq	%r10, %rdx
	movq	%rbx, %r8
	call	umsg_open_67.part.0
	movq	%r13, %rsi
	movq	%rbx, %r8
	movl	%r14d, %edx
	leaq	-240(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	umsg_vformat_67
	movl	%eax, %r13d
	testq	%r12, %r12
	je	.L378
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L378:
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L393
	addq	$208, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	movl	$1, (%rbx)
.L392:
	leaq	-240(%rbp), %rcx
	movq	%rbx, %r8
.L391:
	movq	%r13, %rsi
	movl	%r14d, %edx
	xorl	%edi, %edi
	call	umsg_vformat_67
	movl	%eax, %r13d
	jmp	.L378
.L393:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2530:
	.size	u_formatMessageWithError_67, .-u_formatMessageWithError_67
	.p2align 4
	.globl	u_vformatMessage_67
	.type	u_vformatMessage_67, @function
u_vformatMessage_67:
.LFB2529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	xorl	%r8d, %r8d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L405
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L406
	movq	%rdi, %r10
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L400
	movl	%edx, %esi
	cmpl	$-1, %edx
	jl	.L400
	movq	%r10, %rdx
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	call	umsg_open_67.part.0
	movq	%r13, %rsi
	movq	%rbx, %r8
	movq	%r15, %rcx
	movl	%r14d, %edx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	umsg_vformat_67
	movl	%eax, %r13d
	testq	%r12, %r12
	je	.L394
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L394:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movl	$1, (%rbx)
.L406:
	movq	%rbx, %r8
.L405:
	addq	$8, %rsp
	movq	%r15, %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	popq	%rbx
	xorl	%edi, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	umsg_vformat_67
	.cfi_endproc
.LFE2529:
	.size	u_vformatMessage_67, .-u_vformatMessage_67
	.p2align 4
	.globl	u_vformatMessageWithError_67
	.type	u_vformatMessageWithError_67, @function
u_vformatMessageWithError_67:
.LFB2531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	xorl	%r8d, %r8d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rbx
	movq	16(%rbp), %r15
	testq	%rbx, %rbx
	je	.L418
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L419
	movq	%rdi, %r10
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L413
	movl	%edx, %esi
	cmpl	$-1, %edx
	jl	.L413
	movq	%r9, %rcx
	movq	%r10, %rdx
	movq	%rbx, %r8
	call	umsg_open_67.part.0
	movq	%r13, %rsi
	movq	%rbx, %r8
	movq	%r15, %rcx
	movl	%r14d, %edx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	umsg_vformat_67
	movl	%eax, %r13d
	testq	%r12, %r12
	je	.L407
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L407:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	movl	$1, (%rbx)
.L419:
	movq	%rbx, %r8
.L418:
	addq	$8, %rsp
	movq	%r15, %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	popq	%rbx
	xorl	%edi, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	umsg_vformat_67
	.cfi_endproc
.LFE2531:
	.size	u_vformatMessageWithError_67, .-u_vformatMessageWithError_67
	.p2align 4
	.globl	u_vparseMessage_67
	.type	u_vparseMessage_67, @function
u_vparseMessage_67:
.LFB2533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L420
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L420
	movq	%rdi, %r10
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L431
	movl	%edx, %esi
	cmpl	$-1, %edx
	jl	.L431
	movq	%rcx, %r12
	movl	%r8d, %r13d
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movq	%r10, %rdx
	movq	%r9, %r14
	call	umsg_open_67.part.0
	movl	$0, -44(%rbp)
	movq	%rax, %rdi
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L426
	cmpl	$-1, %r13d
	setl	%dl
	testq	%r12, %r12
	sete	%al
	orb	%al, %dl
	jne	.L432
	testq	%rdi, %rdi
	je	.L432
	leaq	-44(%rbp), %rcx
	movq	%rbx, %r9
	movq	%r14, %r8
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%rdi, -56(%rbp)
	call	umsg_vparse_67.part.0
	movq	-56(%rbp), %rdi
.L429:
	movq	(%rdi), %rax
	call	*8(%rax)
.L420:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L437
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movl	$1, (%rbx)
.L426:
	testq	%rdi, %rdi
	jne	.L429
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L431:
	movl	$1, (%rbx)
	jmp	.L420
.L437:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2533:
	.size	u_vparseMessage_67, .-u_vparseMessage_67
	.p2align 4
	.globl	u_vparseMessageWithError_67
	.type	u_vparseMessageWithError_67, @function
u_vparseMessageWithError_67:
.LFB2535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	24(%rbp), %rbx
	movq	16(%rbp), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L438
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L438
	movq	%rdi, %r10
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L449
	movl	%edx, %esi
	cmpl	$-1, %edx
	jl	.L449
	movl	%r8d, %r13d
	movq	%r10, %rdx
	movq	%rbx, %r8
	movq	%r9, %r14
	call	umsg_open_67.part.0
	movl	$0, -44(%rbp)
	movq	%rax, %rdi
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L444
	cmpl	$-1, %r13d
	setl	%dl
	testq	%r12, %r12
	sete	%al
	orb	%al, %dl
	jne	.L450
	testq	%rdi, %rdi
	je	.L450
	leaq	-44(%rbp), %rcx
	movq	%rbx, %r9
	movq	%r14, %r8
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%rdi, -56(%rbp)
	call	umsg_vparse_67.part.0
	movq	-56(%rbp), %rdi
.L447:
	movq	(%rdi), %rax
	call	*8(%rax)
.L438:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L455
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	.cfi_restore_state
	movl	$1, (%rbx)
.L444:
	testq	%rdi, %rdi
	jne	.L447
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L449:
	movl	$1, (%rbx)
	jmp	.L438
.L455:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2535:
	.size	u_vparseMessageWithError_67, .-u_vparseMessageWithError_67
	.p2align 4
	.globl	umsg_setLocale_67
	.type	umsg_setLocale_67, @function
umsg_setLocale_67:
.LFB2539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L456
	movq	(%rdi), %rax
	leaq	-272(%rbp), %r13
	movq	%rdi, %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	64(%rax), %rbx
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	*%rbx
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
.L456:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L463
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L463:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2539:
	.size	umsg_setLocale_67, .-umsg_setLocale_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
