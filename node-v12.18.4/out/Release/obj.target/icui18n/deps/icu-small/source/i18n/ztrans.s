	.file	"ztrans.cpp"
	.text
	.p2align 4
	.globl	ztrans_open_67
	.type	ztrans_open_67, @function
ztrans_open_67:
.LFB2075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$32, %edi
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -40
	movsd	%xmm0, -40(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1
	movsd	-40(%rbp), %xmm0
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6718TimeZoneTransitionC1EdRKNS_12TimeZoneRuleES3_@PLT
.L1:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2075:
	.size	ztrans_open_67, .-ztrans_open_67
	.p2align 4
	.globl	ztrans_openEmpty_67
	.type	ztrans_openEmpty_67, @function
ztrans_openEmpty_67:
.LFB2076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$32, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L8
	movq	%rax, %rdi
	call	_ZN6icu_6718TimeZoneTransitionC1Ev@PLT
.L8:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2076:
	.size	ztrans_openEmpty_67, .-ztrans_openEmpty_67
	.p2align 4
	.globl	ztrans_close_67
	.type	ztrans_close_67, @function
ztrans_close_67:
.LFB2077:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L14
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L14:
	ret
	.cfi_endproc
.LFE2077:
	.size	ztrans_close_67, .-ztrans_close_67
	.p2align 4
	.globl	ztrans_clone_67
	.type	ztrans_clone_67, @function
ztrans_clone_67:
.LFB2078:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6718TimeZoneTransition5cloneEv@PLT
	.cfi_endproc
.LFE2078:
	.size	ztrans_clone_67, .-ztrans_clone_67
	.p2align 4
	.globl	ztrans_equals_67
	.type	ztrans_equals_67, @function
ztrans_equals_67:
.LFB2079:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6718TimeZoneTransitioneqERKS0_@PLT
	.cfi_endproc
.LFE2079:
	.size	ztrans_equals_67, .-ztrans_equals_67
	.p2align 4
	.globl	ztrans_getTime_67
	.type	ztrans_getTime_67, @function
ztrans_getTime_67:
.LFB2080:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	.cfi_endproc
.LFE2080:
	.size	ztrans_getTime_67, .-ztrans_getTime_67
	.p2align 4
	.globl	ztrans_setTime_67
	.type	ztrans_setTime_67, @function
ztrans_setTime_67:
.LFB2081:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6718TimeZoneTransition7setTimeEd@PLT
	.cfi_endproc
.LFE2081:
	.size	ztrans_setTime_67, .-ztrans_setTime_67
	.p2align 4
	.globl	ztrans_getFrom_67
	.type	ztrans_getFrom_67, @function
ztrans_getFrom_67:
.LFB2082:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	.cfi_endproc
.LFE2082:
	.size	ztrans_getFrom_67, .-ztrans_getFrom_67
	.p2align 4
	.globl	ztrans_setFrom_67
	.type	ztrans_setFrom_67, @function
ztrans_setFrom_67:
.LFB2083:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6718TimeZoneTransition7setFromERKNS_12TimeZoneRuleE@PLT
	.cfi_endproc
.LFE2083:
	.size	ztrans_setFrom_67, .-ztrans_setFrom_67
	.p2align 4
	.globl	ztrans_adoptFrom_67
	.type	ztrans_adoptFrom_67, @function
ztrans_adoptFrom_67:
.LFB2084:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6718TimeZoneTransition9adoptFromEPNS_12TimeZoneRuleE@PLT
	.cfi_endproc
.LFE2084:
	.size	ztrans_adoptFrom_67, .-ztrans_adoptFrom_67
	.p2align 4
	.globl	ztrans_getTo_67
	.type	ztrans_getTo_67, @function
ztrans_getTo_67:
.LFB2085:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	.cfi_endproc
.LFE2085:
	.size	ztrans_getTo_67, .-ztrans_getTo_67
	.p2align 4
	.globl	ztrans_setTo_67
	.type	ztrans_setTo_67, @function
ztrans_setTo_67:
.LFB2086:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6718TimeZoneTransition5setToERKNS_12TimeZoneRuleE@PLT
	.cfi_endproc
.LFE2086:
	.size	ztrans_setTo_67, .-ztrans_setTo_67
	.p2align 4
	.globl	ztrans_adoptTo_67
	.type	ztrans_adoptTo_67, @function
ztrans_adoptTo_67:
.LFB2087:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6718TimeZoneTransition7adoptToEPNS_12TimeZoneRuleE@PLT
	.cfi_endproc
.LFE2087:
	.size	ztrans_adoptTo_67, .-ztrans_adoptTo_67
	.p2align 4
	.globl	ztrans_getStaticClassID_67
	.type	ztrans_getStaticClassID_67, @function
ztrans_getStaticClassID_67:
.LFB2088:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6718TimeZoneTransition16getStaticClassIDEv@PLT
	.cfi_endproc
.LFE2088:
	.size	ztrans_getStaticClassID_67, .-ztrans_getStaticClassID_67
	.p2align 4
	.globl	ztrans_getDynamicClassID_67
	.type	ztrans_getDynamicClassID_67, @function
ztrans_getDynamicClassID_67:
.LFB2089:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6718TimeZoneTransition17getDynamicClassIDEv@PLT
	.cfi_endproc
.LFE2089:
	.size	ztrans_getDynamicClassID_67, .-ztrans_getDynamicClassID_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
