	.file	"anytrans.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717AnyTransliterator17getDynamicClassIDEv
	.type	_ZNK6icu_6717AnyTransliterator17getDynamicClassIDEv, @function
_ZNK6icu_6717AnyTransliterator17getDynamicClassIDEv:
.LFB3099:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6717AnyTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3099:
	.size	_ZNK6icu_6717AnyTransliterator17getDynamicClassIDEv, .-_ZNK6icu_6717AnyTransliterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717AnyTransliteratorD2Ev
	.type	_ZN6icu_6717AnyTransliteratorD2Ev, @function
_ZN6icu_6717AnyTransliteratorD2Ev:
.LFB3104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717AnyTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	call	uhash_close_67@PLT
	leaq	96(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714TransliteratorD2Ev@PLT
	.cfi_endproc
.LFE3104:
	.size	_ZN6icu_6717AnyTransliteratorD2Ev, .-_ZN6icu_6717AnyTransliteratorD2Ev
	.globl	_ZN6icu_6717AnyTransliteratorD1Ev
	.set	_ZN6icu_6717AnyTransliteratorD1Ev,_ZN6icu_6717AnyTransliteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717AnyTransliteratorD0Ev
	.type	_ZN6icu_6717AnyTransliteratorD0Ev, @function
_ZN6icu_6717AnyTransliteratorD0Ev:
.LFB3106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717AnyTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	call	uhash_close_67@PLT
	leaq	96(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3106:
	.size	_ZN6icu_6717AnyTransliteratorD0Ev, .-_ZN6icu_6717AnyTransliteratorD0Ev
	.p2align 4
	.type	_deleteTransliterator, @function
_deleteTransliterator:
.LFB3092:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L7
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6717AnyTransliteratorD0Ev(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L9
	leaq	16+_ZTVN6icu_6717AnyTransliteratorE(%rip), %rax
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	call	uhash_close_67@PLT
	leaq	96(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE3092:
	.size	_deleteTransliterator, .-_deleteTransliterator
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717AnyTransliterator5cloneEv
	.type	_ZNK6icu_6717AnyTransliterator5cloneEv, @function
_ZNK6icu_6717AnyTransliterator5cloneEv:
.LFB3110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$168, %edi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L13
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6717AnyTransliteratorE(%rip), %rax
	leaq	96(%rbx), %rsi
	movq	%rax, (%r12)
	leaq	96(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	xorl	%edx, %edx
	leaq	-28(%rbp), %r8
	movl	$7, %ecx
	movl	160(%rbx), %eax
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	movl	$0, -28(%rbp)
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	movl	%eax, 160(%r12)
	call	uhash_openSize_67@PLT
	movl	-28(%rbp), %edx
	movq	%rax, 88(%r12)
	testl	%edx, %edx
	jg	.L13
	leaq	_deleteTransliterator(%rip), %rsi
	movq	%rax, %rdi
	call	uhash_setValueDeleter_67@PLT
.L13:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3110:
	.size	_ZNK6icu_6717AnyTransliterator5cloneEv, .-_ZNK6icu_6717AnyTransliterator5cloneEv
	.align 2
	.p2align 4
	.type	_ZNK6icu_6717AnyTransliterator17getTransliteratorE11UScriptCode.part.0, @function
_ZNK6icu_6717AnyTransliterator17getTransliteratorE11UScriptCode.part.0:
.LFB4038:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	%esi, %edi
	leaq	96(%rbx), %r15
	subq	$184, %rsp
	movl	%esi, -220(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -196(%rbp)
	call	uscript_getShortName_67@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$45, %ecx
	movq	%r12, %rdi
	xorl	%edx, %edx
	movw	%cx, -198(%rbp)
	leaq	-198(%rbp), %rsi
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	104(%rbx), %ecx
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L23
	sarl	$5, %ecx
.L24:
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	leaq	-196(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -216(%rbp)
	call	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR10UErrorCode@PLT
	movl	-196(%rbp), %edx
	movq	%rax, %r14
	testl	%edx, %edx
	jle	.L53
	testq	%rax, %rax
	je	.L26
	movq	(%rax), %rax
	leaq	_ZN6icu_6717AnyTransliteratorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L28
	leaq	16+_ZTVN6icu_6717AnyTransliteratorE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	call	uhash_close_67@PLT
	leaq	96(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L26:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	$-1, %ecx
	movq	%r12, %rdi
	xorl	%edx, %edx
	leaq	_ZL11LATIN_PIVOT(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	104(%rbx), %ecx
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L29
	sarl	$5, %ecx
.L30:
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	leaq	_ZL11LATIN_PIVOT(%rip), %rax
	movq	-216(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator14createInstanceERKNS_13UnicodeStringE15UTransDirectionR10UErrorCode@PLT
	movq	%rax, %r14
	movl	-196(%rbp), %eax
	testl	%eax, %eax
	jle	.L54
	testq	%r14, %r14
	je	.L32
	movq	(%r14), %rax
	leaq	_ZN6icu_6717AnyTransliteratorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L33
	leaq	16+_ZTVN6icu_6717AnyTransliteratorE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	call	uhash_close_67@PLT
	leaq	96(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L32:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movl	108(%rbx), %ecx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L53:
	testq	%rax, %rax
	je	.L26
.L27:
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	88(%rbx), %rdi
	movl	-220(%rbp), %esi
	call	uhash_iget_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L56
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	movq	(%r14), %rax
	leaq	_ZN6icu_6717AnyTransliteratorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L34
	leaq	16+_ZTVN6icu_6717AnyTransliteratorE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	call	uhash_close_67@PLT
	leaq	96(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	movq	%r14, %rdi
	movq	%r15, %r14
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L23:
	movl	108(%rbx), %ecx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L54:
	testq	%r14, %r14
	jne	.L27
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	*%rax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r14, %rdi
	movq	%r15, %r14
	call	*%rax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L56:
	movq	88(%rbx), %rdi
	movq	-216(%rbp), %rcx
	movq	%r14, %rdx
	movl	-220(%rbp), %esi
	call	uhash_iput_67@PLT
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	jmp	.L32
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4038:
	.size	_ZNK6icu_6717AnyTransliterator17getTransliteratorE11UScriptCode.part.0, .-_ZNK6icu_6717AnyTransliterator17getTransliteratorE11UScriptCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717ScriptRunIteratorC2ERKNS_11ReplaceableEii
	.type	_ZN6icu_6717ScriptRunIteratorC2ERKNS_11ReplaceableEii, @function
_ZN6icu_6717ScriptRunIteratorC2ERKNS_11ReplaceableEii:
.LFB3094:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	%edx, 8(%rdi)
	movl	%ecx, 12(%rdi)
	movl	%edx, 24(%rdi)
	ret
	.cfi_endproc
.LFE3094:
	.size	_ZN6icu_6717ScriptRunIteratorC2ERKNS_11ReplaceableEii, .-_ZN6icu_6717ScriptRunIteratorC2ERKNS_11ReplaceableEii
	.globl	_ZN6icu_6717ScriptRunIteratorC1ERKNS_11ReplaceableEii
	.set	_ZN6icu_6717ScriptRunIteratorC1ERKNS_11ReplaceableEii,_ZN6icu_6717ScriptRunIteratorC2ERKNS_11ReplaceableEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717ScriptRunIterator4nextEv
	.type	_ZN6icu_6717ScriptRunIterator4nextEv, @function
_ZN6icu_6717ScriptRunIterator4nextEv:
.LFB3096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	24(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	12(%rdi), %eax
	movl	$0, -28(%rbp)
	movl	$-1, 16(%rdi)
	movl	%esi, 20(%rdi)
	cmpl	%eax, %esi
	je	.L58
	movq	%rdi, %rbx
	cmpl	8(%rdi), %esi
	jle	.L60
	leaq	-28(%rbp), %r12
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L61:
	movl	20(%rbx), %eax
	leal	-1(%rax), %esi
	movl	%esi, 20(%rbx)
	cmpl	%esi, 8(%rbx)
	jge	.L74
.L63:
	movq	(%rbx), %rdi
	subl	$1, %esi
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	%r12, %rsi
	movl	%eax, %edi
	call	uscript_getScript_67@PLT
	cmpl	$1, %eax
	jbe	.L61
.L74:
	movl	24(%rbx), %esi
	movl	12(%rbx), %eax
.L60:
	leaq	-28(%rbp), %r12
	cmpl	%eax, %esi
	jl	.L62
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L65:
	cmpl	%eax, %edx
	jne	.L66
.L64:
	movl	24(%rbx), %eax
	leal	1(%rax), %esi
	movl	%esi, 24(%rbx)
	cmpl	%esi, 12(%rbx)
	jle	.L66
.L62:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	%r12, %rsi
	movl	%eax, %edi
	call	uscript_getScript_67@PLT
	cmpl	$1, %eax
	jbe	.L64
	movl	16(%rbx), %edx
	cmpl	$-1, %edx
	jne	.L65
	movl	%eax, 16(%rbx)
	movl	24(%rbx), %eax
	leal	1(%rax), %esi
	movl	%esi, 24(%rbx)
	cmpl	%esi, 12(%rbx)
	jg	.L62
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$1, %r8d
.L58:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L75:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3096:
	.size	_ZN6icu_6717ScriptRunIterator4nextEv, .-_ZN6icu_6717ScriptRunIterator4nextEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717AnyTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.type	_ZNK6icu_6717AnyTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, @function
_ZNK6icu_6717AnyTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona:
.LFB3111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movl	8(%rdx), %r13d
	movl	12(%rdx), %r12d
	movq	%rdi, -112(%rbp)
	movq	%rsi, -120(%rbp)
	movq	(%rdx), %rdx
	movb	%cl, -121(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rbx), %eax
	movq	%rsi, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%eax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r15, %rdi
	call	_ZN6icu_6717ScriptRunIterator4nextEv
	testb	%al, %al
	je	.L78
.L94:
	movl	-72(%rbp), %eax
	cmpl	%r13d, %eax
	jle	.L79
	movq	-112(%rbp), %r14
	movl	-80(%rbp), %esi
	cmpl	%esi, 160(%r14)
	je	.L80
	cmpl	$-1, %esi
	movl	%esi, -100(%rbp)
	je	.L80
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	88(%r14), %rdi
	movl	-100(%rbp), %esi
	call	uhash_iget_67@PLT
	xorl	%edi, %edi
	movq	%rax, %r14
	call	umtx_unlock_67@PLT
	testq	%r14, %r14
	movl	-100(%rbp), %esi
	je	.L92
.L81:
	xorl	%ecx, %ecx
	cmpb	$0, -121(%rbp)
	je	.L83
	xorl	%ecx, %ecx
	cmpl	%r12d, -72(%rbp)
	setge	%cl
.L83:
	movl	-76(%rbp), %esi
	movl	%r13d, %edi
	movl	%ecx, -104(%rbp)
	call	uprv_max_67@PLT
	movl	-72(%rbp), %esi
	movl	%r12d, %edi
	movl	%eax, 8(%rbx)
	call	uprv_min_67@PLT
	movq	(%r14), %r9
	movl	-104(%rbp), %ecx
	movq	%rbx, %rdx
	movl	%eax, 12(%rbx)
	movq	-120(%rbp), %rsi
	movq	%r14, %rdi
	movl	%eax, -100(%rbp)
	call	*88(%r9)
	movl	-100(%rbp), %eax
	movl	12(%rbx), %edx
	subl	%eax, %edx
	movl	%edx, %eax
	leal	(%r12,%rdx), %ecx
	movl	-72(%rbp), %edx
	addl	%eax, -84(%rbp)
	leal	(%rax,%rdx), %esi
	movl	%esi, -72(%rbp)
	cmpl	%edx, %r12d
	jle	.L93
.L82:
	movq	%r15, %rdi
	movl	%ecx, %r12d
	call	_ZN6icu_6717ScriptRunIterator4nextEv
	testb	%al, %al
	jne	.L94
.L78:
	movl	%r12d, 12(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
	call	_ZNK6icu_6717AnyTransliterator17getTransliteratorE11UScriptCode.part.0
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L81
	movl	-72(%rbp), %eax
	.p2align 4,,10
	.p2align 3
.L80:
	movl	%eax, 8(%rbx)
	movl	%r12d, %ecx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L93:
	movl	%ecx, %r12d
	jmp	.L78
.L95:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3111:
	.size	_ZNK6icu_6717AnyTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, .-_ZNK6icu_6717AnyTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717ScriptRunIterator11adjustLimitEi
	.type	_ZN6icu_6717ScriptRunIterator11adjustLimitEi, @function
_ZN6icu_6717ScriptRunIterator11adjustLimitEi:
.LFB3097:
	.cfi_startproc
	endbr64
	addl	%esi, 24(%rdi)
	addl	%esi, 12(%rdi)
	ret
	.cfi_endproc
.LFE3097:
	.size	_ZN6icu_6717ScriptRunIterator11adjustLimitEi, .-_ZN6icu_6717ScriptRunIterator11adjustLimitEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717AnyTransliterator16getStaticClassIDEv
	.type	_ZN6icu_6717AnyTransliterator16getStaticClassIDEv, @function
_ZN6icu_6717AnyTransliterator16getStaticClassIDEv:
.LFB3098:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6717AnyTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3098:
	.size	_ZN6icu_6717AnyTransliterator16getStaticClassIDEv, .-_ZN6icu_6717AnyTransliterator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717AnyTransliteratorC2ERKNS_13UnicodeStringES3_S3_11UScriptCodeR10UErrorCode
	.type	_ZN6icu_6717AnyTransliteratorC2ERKNS_13UnicodeStringES3_S3_11UScriptCodeR10UErrorCode, @function
_ZN6icu_6717AnyTransliteratorC2ERKNS_13UnicodeStringES3_S3_11UScriptCodeR10UErrorCode:
.LFB3101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	leaq	16+_ZTVN6icu_6717AnyTransliteratorE(%rip), %rax
	movq	%r12, %r8
	movl	$2, %edx
	movq	%rax, (%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	movl	$7, %ecx
	movw	%dx, 104(%rbx)
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	xorl	%edx, %edx
	movq	%rax, 96(%rbx)
	movl	%r15d, 160(%rbx)
	call	uhash_openSize_67@PLT
	movl	(%r12), %ecx
	movq	%rax, 88(%rbx)
	testl	%ecx, %ecx
	jle	.L108
.L98:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	leaq	_deleteTransliterator(%rip), %rsi
	movq	%rax, %rdi
	leaq	96(%rbx), %r12
	call	uhash_setValueDeleter_67@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L101
	sarl	$5, %eax
.L102:
	testl	%eax, %eax
	jle	.L98
	movl	$47, %eax
	movl	$1, %ecx
	movq	%r12, %rdi
	xorl	%edx, %edx
	leaq	-58(%rbp), %rsi
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	8(%r13), %ecx
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L104
	sarl	$5, %ecx
.L105:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L101:
	movl	12(%r13), %eax
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L104:
	movl	12(%r13), %ecx
	jmp	.L105
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3101:
	.size	_ZN6icu_6717AnyTransliteratorC2ERKNS_13UnicodeStringES3_S3_11UScriptCodeR10UErrorCode, .-_ZN6icu_6717AnyTransliteratorC2ERKNS_13UnicodeStringES3_S3_11UScriptCodeR10UErrorCode
	.globl	_ZN6icu_6717AnyTransliteratorC1ERKNS_13UnicodeStringES3_S3_11UScriptCodeR10UErrorCode
	.set	_ZN6icu_6717AnyTransliteratorC1ERKNS_13UnicodeStringES3_S3_11UScriptCodeR10UErrorCode,_ZN6icu_6717AnyTransliteratorC2ERKNS_13UnicodeStringES3_S3_11UScriptCodeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717AnyTransliteratorC2ERKS0_
	.type	_ZN6icu_6717AnyTransliteratorC2ERKS0_, @function
_ZN6icu_6717AnyTransliteratorC2ERKS0_:
.LFB3108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6717AnyTransliteratorE(%rip), %rax
	leaq	96(%r12), %rsi
	movq	%rax, (%rbx)
	leaq	96(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	xorl	%edx, %edx
	leaq	-28(%rbp), %r8
	movl	$7, %ecx
	movl	160(%r12), %eax
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	movl	$0, -28(%rbp)
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	movl	%eax, 160(%rbx)
	call	uhash_openSize_67@PLT
	movl	-28(%rbp), %edx
	movq	%rax, 88(%rbx)
	testl	%edx, %edx
	jg	.L110
	leaq	_deleteTransliterator(%rip), %rsi
	movq	%rax, %rdi
	call	uhash_setValueDeleter_67@PLT
.L110:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L115:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3108:
	.size	_ZN6icu_6717AnyTransliteratorC2ERKS0_, .-_ZN6icu_6717AnyTransliteratorC2ERKS0_
	.globl	_ZN6icu_6717AnyTransliteratorC1ERKS0_
	.set	_ZN6icu_6717AnyTransliteratorC1ERKS0_,_ZN6icu_6717AnyTransliteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717AnyTransliterator17getTransliteratorE11UScriptCode
	.type	_ZNK6icu_6717AnyTransliterator17getTransliteratorE11UScriptCode, @function
_ZNK6icu_6717AnyTransliterator17getTransliteratorE11UScriptCode:
.LFB3112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	cmpl	%esi, 160(%rdi)
	je	.L118
	movl	%esi, %r12d
	cmpl	$-1, %esi
	je	.L118
	movq	%rdi, %r13
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	88(%r13), %rdi
	movl	%r12d, %esi
	call	uhash_iget_67@PLT
	xorl	%edi, %edi
	movq	%rax, %r14
	call	umtx_unlock_67@PLT
	testq	%r14, %r14
	je	.L120
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r14d, %r14d
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	addq	$8, %rsp
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6717AnyTransliterator17getTransliteratorE11UScriptCode.part.0
	.cfi_endproc
.LFE3112:
	.size	_ZNK6icu_6717AnyTransliterator17getTransliteratorE11UScriptCode, .-_ZNK6icu_6717AnyTransliterator17getTransliteratorE11UScriptCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717AnyTransliterator11registerIDsEv
	.type	_ZN6icu_6717AnyTransliterator11registerIDsEv, @function
_ZN6icu_6717AnyTransliterator11registerIDsEv:
.LFB3114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-600(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$648, %rsp
	.cfi_offset 3, -56
	movq	uhash_compareCaselessUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uhash_hashCaselessUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-624(%rbp), %rax
	movl	$0, -624(%rbp)
	movq	%rax, %r8
	movq	%rax, -664(%rbp)
	movq	$0, -608(%rbp)
	call	uhash_init_67@PLT
	movl	-624(%rbp), %ebx
	testl	%ebx, %ebx
	jle	.L158
.L122:
	call	_ZN6icu_6714Transliterator22_countAvailableSourcesEv@PLT
	movl	%eax, -680(%rbp)
	testl	%eax, %eax
	jle	.L123
	leaq	-512(%rbp), %rax
	movl	$0, -672(%rbp)
	movq	%rax, -648(%rbp)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L143:
	movq	-648(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addl	$1, -672(%rbp)
	movl	-672(%rbp), %eax
	cmpl	%eax, -680(%rbp)
	je	.L123
.L127:
	movq	-648(%rbp), %rsi
	movl	-672(%rbp), %edi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r11d
	movq	%rax, -512(%rbp)
	movw	%r11w, -504(%rbp)
	call	_ZN6icu_6714Transliterator19_getAvailableSourceEiRNS_13UnicodeStringE@PLT
	movswl	-504(%rbp), %edx
	testw	%dx, %dx
	js	.L124
	sarl	$5, %edx
.L125:
	subq	$8, %rsp
	movq	-648(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	pushq	$0
	movl	$3, %r9d
	leaq	_ZL3ANY(%rip), %rcx
	leaq	_ZL3ANY(%rip), %rbx
	call	_ZNK6icu_6713UnicodeString13doCaseCompareEiiPKDsiij@PLT
	popq	%r9
	popq	%r10
	testb	%al, %al
	je	.L143
	movq	-648(%rbp), %rdi
	call	_ZN6icu_6714Transliterator22_countAvailableTargetsERKNS_13UnicodeStringE@PLT
	movl	%eax, -676(%rbp)
	testl	%eax, %eax
	jle	.L143
	movl	$0, -636(%rbp)
	leaq	-448(%rbp), %r15
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L160:
	movswl	%ax, %r12d
	sarl	$5, %r12d
	testb	$17, %al
	jne	.L146
.L161:
	testb	$2, %al
	leaq	-438(%rbp), %rdi
	movl	%r12d, %esi
	cmove	-424(%rbp), %rdi
	call	uprv_isInvariantUString_67@PLT
	testb	%al, %al
	jne	.L159
.L135:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addl	$1, -636(%rbp)
	movl	-636(%rbp), %eax
	cmpl	%eax, -676(%rbp)
	je	.L143
.L142:
	movq	-648(%rbp), %rsi
	movl	-636(%rbp), %edi
	movl	$2, %r8d
	movq	%r15, %rdx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r8w, -440(%rbp)
	movq	%rax, -448(%rbp)
	call	_ZN6icu_6714Transliterator19_getAvailableTargetEiRKNS_13UnicodeStringERS1_@PLT
	movq	-608(%rbp), %rdi
	movq	%r15, %rsi
	call	uhash_geti_67@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L135
	movl	$0, -624(%rbp)
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L130
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L130:
	movq	-664(%rbp), %rcx
	movq	-608(%rbp), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	call	uhash_puti_67@PLT
	movzwl	-440(%rbp), %eax
	movl	$0, -616(%rbp)
	testw	%ax, %ax
	jns	.L160
	movl	-436(%rbp), %r12d
	testb	$17, %al
	je	.L161
.L146:
	xorl	%edi, %edi
	movl	%r12d, %esi
	call	uprv_isInvariantUString_67@PLT
	testb	%al, %al
	je	.L135
	.p2align 4,,10
	.p2align 3
.L159:
	leaq	-192(%rbp), %r13
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	movl	%r12d, %edx
	movq	%r13, %rcx
	movq	%r15, %rdi
	movl	$128, %r8d
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	leaq	-616(%rbp), %rax
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	-620(%rbp), %rsi
	movq	%rax, %rcx
	movb	$0, -65(%rbp)
	movq	%rax, -656(%rbp)
	call	uscript_getCode_67@PLT
	cmpl	$1, %eax
	jne	.L135
	movl	-616(%rbp), %edi
	testl	%edi, %edi
	jg	.L135
	movl	-620(%rbp), %eax
	movl	%eax, -668(%rbp)
	cmpl	$-1, %eax
	je	.L135
	movq	-648(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6714Transliterator23_countAvailableVariantsERKNS_13UnicodeStringES3_@PLT
	movl	%eax, -640(%rbp)
	testl	%eax, %eax
	jle	.L135
	leaq	-320(%rbp), %rax
	leaq	-384(%rbp), %r12
	movq	%rax, -632(%rbp)
	leaq	-256(%rbp), %r13
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L162:
	movq	(%r14), %rax
	leaq	_ZN6icu_6717AnyTransliteratorD0Ev(%rip), %rcx
	cmpq	%rcx, 8(%rax)
	jne	.L139
	leaq	16+_ZTVN6icu_6717AnyTransliteratorE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	call	uhash_close_67@PLT
	leaq	96(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L140:
	movq	-632(%rbp), %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	%ebx, -640(%rbp)
	je	.L135
.L141:
	movq	-648(%rbp), %rsi
	movl	$2, %ecx
	movq	%r15, %rdx
	movl	%ebx, %edi
	movw	%cx, -376(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rcx
	movq	%rax, %r14
	movq	%rax, -384(%rbp)
	call	_ZN6icu_6714Transliterator20_getAvailableVariantEiRKNS_13UnicodeStringES3_RS1_@PLT
	movq	-656(%rbp), %rdx
	movl	$2, %esi
	movq	%r13, %rdi
	leaq	_ZL3ANY(%rip), %rax
	movw	%si, -312(%rbp)
	movl	$3, %ecx
	movl	$1, %esi
	movq	%rax, -616(%rbp)
	movq	%r14, -320(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	-632(%rbp), %rcx
	call	_ZN6icu_6722TransliteratorIDParser7STVtoIDERKNS_13UnicodeStringES3_S3_RS1_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$0, -624(%rbp)
	movl	$168, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L137
	movq	-664(%rbp), %r9
	movq	%r15, %rdx
	movq	%r12, %rcx
	movq	%rax, %rdi
	movl	-668(%rbp), %r8d
	movq	-632(%rbp), %rsi
	call	_ZN6icu_6717AnyTransliteratorC1ERKNS_13UnicodeStringES3_S3_11UScriptCodeR10UErrorCode
	movl	-624(%rbp), %edx
	testl	%edx, %edx
	jg	.L162
.L138:
	movq	%r14, %rdi
	call	_ZN6icu_6714Transliterator17_registerInstanceEPS0_@PLT
	movq	-656(%rbp), %rdx
	movl	$4, %ecx
	movq	%r13, %rdi
	leaq	_ZL7NULL_ID(%rip), %rax
	movl	$1, %esi
	movq	%rax, -616(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6714Transliterator23_registerSpecialInverseERKNS_13UnicodeStringES3_a@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%r14, %rdi
	call	_ZN6icu_6717AnyTransliteratorD0Ev
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L124:
	movl	-500(%rbp), %edx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L123:
	movq	-608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L121
	call	uhash_close_67@PLT
.L121:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L158:
	.cfi_restore_state
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	movq	%r12, -608(%rbp)
	call	uhash_setKeyDeleter_67@PLT
	jmp	.L122
.L137:
	movl	-624(%rbp), %eax
	testl	%eax, %eax
	jg	.L140
	jmp	.L138
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3114:
	.size	_ZN6icu_6717AnyTransliterator11registerIDsEv, .-_ZN6icu_6717AnyTransliterator11registerIDsEv
	.weak	_ZTSN6icu_6717AnyTransliteratorE
	.section	.rodata._ZTSN6icu_6717AnyTransliteratorE,"aG",@progbits,_ZTSN6icu_6717AnyTransliteratorE,comdat
	.align 16
	.type	_ZTSN6icu_6717AnyTransliteratorE, @object
	.size	_ZTSN6icu_6717AnyTransliteratorE, 29
_ZTSN6icu_6717AnyTransliteratorE:
	.string	"N6icu_6717AnyTransliteratorE"
	.weak	_ZTIN6icu_6717AnyTransliteratorE
	.section	.data.rel.ro._ZTIN6icu_6717AnyTransliteratorE,"awG",@progbits,_ZTIN6icu_6717AnyTransliteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6717AnyTransliteratorE, @object
	.size	_ZTIN6icu_6717AnyTransliteratorE, 24
_ZTIN6icu_6717AnyTransliteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717AnyTransliteratorE
	.quad	_ZTIN6icu_6714TransliteratorE
	.weak	_ZTVN6icu_6717AnyTransliteratorE
	.section	.data.rel.ro._ZTVN6icu_6717AnyTransliteratorE,"awG",@progbits,_ZTVN6icu_6717AnyTransliteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6717AnyTransliteratorE, @object
	.size	_ZTVN6icu_6717AnyTransliteratorE, 152
_ZTVN6icu_6717AnyTransliteratorE:
	.quad	0
	.quad	_ZTIN6icu_6717AnyTransliteratorE
	.quad	_ZN6icu_6717AnyTransliteratorD1Ev
	.quad	_ZN6icu_6717AnyTransliteratorD0Ev
	.quad	_ZNK6icu_6717AnyTransliterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6717AnyTransliterator5cloneEv
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.quad	_ZNK6icu_6717AnyTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.quad	_ZNK6icu_6714Transliterator5getIDEv
	.quad	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE
	.local	_ZZN6icu_6717AnyTransliterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6717AnyTransliterator16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.align 16
	.type	_ZL11LATIN_PIVOT, @object
	.size	_ZL11LATIN_PIVOT, 24
_ZL11LATIN_PIVOT:
	.value	45
	.value	76
	.value	97
	.value	116
	.value	110
	.value	59
	.value	76
	.value	97
	.value	116
	.value	110
	.value	45
	.value	0
	.align 8
	.type	_ZL7NULL_ID, @object
	.size	_ZL7NULL_ID, 10
_ZL7NULL_ID:
	.value	78
	.value	117
	.value	108
	.value	108
	.value	0
	.align 8
	.type	_ZL3ANY, @object
	.size	_ZL3ANY, 8
_ZL3ANY:
	.value	65
	.value	110
	.value	121
	.value	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
