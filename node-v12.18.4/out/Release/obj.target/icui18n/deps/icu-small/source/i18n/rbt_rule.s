	.file	"rbt_rule.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719TransliterationRule16getContextLengthEv
	.type	_ZNK6icu_6719TransliterationRule16getContextLengthEv, @function
_ZNK6icu_6719TransliterationRule16getContextLengthEv:
.LFB2433:
	.cfi_startproc
	endbr64
	movzbl	124(%rdi), %eax
	andl	$1, %eax
	addl	116(%rdi), %eax
	ret
	.cfi_endproc
.LFE2433:
	.size	_ZNK6icu_6719TransliterationRule16getContextLengthEv, .-_ZNK6icu_6719TransliterationRule16getContextLengthEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719TransliterationRuleD2Ev
	.type	_ZN6icu_6719TransliterationRuleD2Ev, @function
_ZN6icu_6719TransliterationRuleD2Ev:
.LFB2430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6719TransliterationRuleE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	104(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4
	movq	(%rdi), %rax
	call	*8(%rax)
.L4:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5
	movq	(%rdi), %rax
	call	*8(%rax)
.L5:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L6
	movq	(%rdi), %rax
	call	*8(%rax)
.L6:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L7
	movq	(%rdi), %rax
	call	*8(%rax)
.L7:
	addq	$8, %rsp
	leaq	40(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE2430:
	.size	_ZN6icu_6719TransliterationRuleD2Ev, .-_ZN6icu_6719TransliterationRuleD2Ev
	.globl	_ZN6icu_6719TransliterationRuleD1Ev
	.set	_ZN6icu_6719TransliterationRuleD1Ev,_ZN6icu_6719TransliterationRuleD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719TransliterationRuleD0Ev
	.type	_ZN6icu_6719TransliterationRuleD0Ev, @function
_ZN6icu_6719TransliterationRuleD0Ev:
.LFB2432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6719TransliterationRuleD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2432:
	.size	_ZN6icu_6719TransliterationRuleD0Ev, .-_ZN6icu_6719TransliterationRuleD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719TransliterationRule5masksERKS0_
	.type	_ZNK6icu_6719TransliterationRule5masksERKS0_, @function
_ZNK6icu_6719TransliterationRule5masksERKS0_:
.LFB2436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movzwl	48(%rdi), %ecx
	testw	%cx, %cx
	js	.L24
	movswl	%cx, %edx
	sarl	$5, %edx
.L25:
	movl	116(%r12), %r15d
	movzwl	48(%rbx), %eax
	movl	%edx, %r10d
	movl	116(%rbx), %r14d
	subl	%r15d, %r10d
	testw	%ax, %ax
	js	.L26
	movswl	%ax, %r13d
	sarl	$5, %r13d
.L27:
	subl	%r14d, %r13d
	testb	$1, %cl
	je	.L28
	notl	%eax
	andl	$1, %eax
.L29:
	movsbl	%al, %eax
	cmpl	%r14d, %r15d
	jne	.L33
	cmpl	%r13d, %r10d
	jne	.L33
	movl	120(%r12), %ecx
	movl	120(%rbx), %edx
	testl	%eax, %eax
	jne	.L34
	cmpl	%edx, %ecx
	jle	.L59
.L34:
	xorl	%r8d, %r8d
	cmpl	%edx, %ecx
	jle	.L38
.L23:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	xorl	%r8d, %r8d
	cmpl	%r14d, %r15d
	jg	.L23
	cmpl	%r13d, %r10d
	jge	.L60
.L38:
	testl	%eax, %eax
	sete	%r8b
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L28:
	testl	%edx, %edx
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%edx, %r8d
	js	.L30
	movl	%edx, %r9d
	subl	%r8d, %r9d
	cmpl	%edx, %r9d
	cmovg	%edx, %r9d
.L30:
	andl	$2, %ecx
	leaq	50(%r12), %rcx
	jne	.L32
	movq	64(%r12), %rcx
.L32:
	movl	%r14d, %esi
	leaq	40(%rbx), %rdi
	movl	%r10d, -52(%rbp)
	subl	%r15d, %esi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movl	-52(%rbp), %r10d
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L26:
	movl	52(%rbx), %r13d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L24:
	movl	52(%rdi), %edx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L60:
	jne	.L23
	movl	120(%r12), %ecx
	movl	120(%rbx), %edx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L59:
	movzbl	124(%r12), %edx
	movzbl	124(%rbx), %eax
	movl	$1, %r8d
	cmpb	%al, %dl
	je	.L23
	andl	$3, %edx
	je	.L23
	andl	$3, %eax
	cmpb	$3, %al
	sete	%r8b
	jmp	.L23
	.cfi_endproc
.LFE2436:
	.size	_ZNK6icu_6719TransliterationRule5masksERKS0_, .-_ZNK6icu_6719TransliterationRule5masksERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719TransliterationRule6toRuleERNS_13UnicodeStringEa
	.type	_ZNK6icu_6719TransliterationRule6toRuleERNS_13UnicodeStringEa, @function
_ZNK6icu_6719TransliterationRule6toRuleERNS_13UnicodeStringEa:
.LFB2440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movsbl	%dl, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$2, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%si, -248(%rbp)
	movq	8(%rdi), %rsi
	movq	%rax, -256(%rbp)
	movq	%rax, -192(%rbp)
	movzbl	124(%rdi), %eax
	movw	%r8w, -184(%rbp)
	andl	$1, %eax
	testq	%rsi, %rsi
	je	.L95
	testb	%al, %al
	jne	.L73
	leaq	-192(%rbp), %r14
	addq	$8, %rsi
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	%r14, %rcx
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEPKNS_14UnicodeMatcherEaS2_@PLT
	leaq	-264(%rbp), %rax
	movq	%rax, -280(%rbp)
.L74:
	movl	$123, %esi
	movq	%r14, %r8
	movl	%r13d, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_@PLT
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L96
	addq	$8, %rsi
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEPKNS_14UnicodeMatcherEaS2_@PLT
.L69:
	movq	%r14, %r8
	movl	%r13d, %ecx
	movl	$1, %edx
	movl	$125, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_@PLT
.L70:
	movq	24(%rbx), %rsi
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	testq	%rsi, %rsi
	leaq	8(%rsi), %rax
	cmovne	%rax, %rsi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEPKNS_14UnicodeMatcherEaS2_@PLT
	testb	$2, 124(%rbx)
	jne	.L97
.L68:
	leaq	-128(%rbp), %r15
	movl	$3, %ecx
	movl	$1, %esi
	movq	-280(%rbp), %rdx
	leaq	_ZL10FORWARD_OP(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r14, %r8
	movl	%r13d, %ecx
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringERKS1_aaS2_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	32(%rbx), %rdi
	leaq	-256(%rbp), %r15
	movq	(%rdi), %rax
	call	*40(%rax)
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	%r14, %r8
	movl	%r13d, %ecx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringERKS1_aaS2_@PLT
	movq	%r14, %r8
	movl	%r13d, %ecx
	movl	$1, %edx
	movl	$59, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movl	$94, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%ax, -264(%rbp)
	leaq	-264(%rbp), %rax
	movl	$1, %r15d
	movq	%rax, %rsi
	movq	%rax, -280(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L99
.L75:
	addq	$8, %rsi
.L63:
	leaq	-192(%rbp), %r14
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	%r14, %rcx
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEPKNS_14UnicodeMatcherEaS2_@PLT
	testb	%r15b, %r15b
	jne	.L74
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L100
	addq	$8, %rsi
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEPKNS_14UnicodeMatcherEaS2_@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$36, %edx
	movq	-280(%rbp), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%dx, -264(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L95:
	cmpq	$0, 24(%rdi)
	setne	%cl
	movl	%ecx, %r15d
	leaq	-264(%rbp), %rcx
	movq	%rcx, -280(%rbp)
	testb	%al, %al
	je	.L63
	movq	%rcx, %rsi
	movl	$94, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%cx, -264(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L75
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%r14, %rcx
	movl	%r13d, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEPKNS_14UnicodeMatcherEaS2_@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L99:
	leaq	-192(%rbp), %r14
	movl	%r13d, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r14, %rcx
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEPKNS_14UnicodeMatcherEaS2_@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%r14, %rcx
	movl	%r13d, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEPKNS_14UnicodeMatcherEaS2_@PLT
	jmp	.L69
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2440:
	.size	_ZNK6icu_6719TransliterationRule6toRuleERNS_13UnicodeStringEa, .-_ZNK6icu_6719TransliterationRule6toRuleERNS_13UnicodeStringEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719TransliterationRuleC2ERKNS_13UnicodeStringEiiS3_iiPPNS_14UnicodeFunctorEiaaPKNS_23TransliterationRuleDataER10UErrorCode
	.type	_ZN6icu_6719TransliterationRuleC2ERKNS_13UnicodeStringEiiS3_iiPPNS_14UnicodeFunctorEiaaPKNS_23TransliterationRuleDataER10UErrorCode, @function
_ZN6icu_6719TransliterationRuleC2ERKNS_13UnicodeStringEiiS3_iiPPNS_14UnicodeFunctorEiaaPKNS_23TransliterationRuleDataER10UErrorCode:
.LFB2424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6719TransliterationRuleE(%rip), %rax
	movl	$2, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	40(%rbp), %r10d
	movl	48(%rbp), %r14d
	movq	%rax, (%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r11w, 48(%rdi)
	movq	%rax, 40(%rdi)
	movq	56(%rbp), %rax
	movq	$0, 104(%rdi)
	movq	%rax, 128(%rdi)
	movq	64(%rbp), %rax
	movl	(%rax), %ebx
	testl	%ebx, %ebx
	jg	.L101
	movswl	8(%rsi), %eax
	movq	%rdi, %rbx
	movq	%r8, %r13
	movl	%r9d, %r12d
	testl	%edx, %edx
	js	.L139
	testw	%ax, %ax
	js	.L107
	movswl	%ax, %edi
	sarl	$5, %edi
.L108:
	cmpl	%edi, %edx
	jg	.L114
	movl	%edx, 116(%rbx)
	testl	%ecx, %ecx
	js	.L110
	cmpl	%ecx, %edx
	jle	.L104
.L114:
	movq	64(%rbp), %rax
	movl	$1, (%rax)
.L101:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movl	$0, 116(%rdi)
	xorl	%edx, %edx
	testl	%ecx, %ecx
	js	.L110
.L104:
	testw	%ax, %ax
	js	.L115
	sarl	$5, %eax
.L116:
	cmpl	%eax, %ecx
	jg	.L114
	subl	%edx, %ecx
	movswl	8(%r13), %r9d
	movl	%ecx, 120(%rbx)
	testl	%r12d, %r12d
	js	.L140
.L118:
	testw	%r9w, %r9w
	js	.L121
	sarl	$5, %r9d
.L122:
	cmpl	%r9d, %r12d
	jg	.L114
.L120:
	movq	24(%rbp), %rax
	leaq	40(%rbx), %r15
	movl	%r10d, -56(%rbp)
	movq	%r15, %rdi
	movq	%rax, 104(%rbx)
	movl	32(%rbp), %eax
	movl	%eax, 112(%rbx)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	-56(%rbp), %r10d
	testb	%r10b, %r10b
	jne	.L123
	movb	$0, 124(%rbx)
	movl	$2, %eax
.L124:
	testb	%r14b, %r14b
	je	.L125
	movb	%al, 124(%rbx)
.L125:
	movl	116(%rbx), %eax
	movq	$0, 8(%rbx)
	testl	%eax, %eax
	jg	.L141
.L126:
	movl	120(%rbx), %eax
	movq	$0, 16(%rbx)
	testl	%eax, %eax
	jg	.L142
	movswl	48(%rbx), %ecx
	testw	%cx, %cx
	js	.L130
.L143:
	sarl	$5, %ecx
.L131:
	movl	%ecx, %edi
	movl	%ecx, -56(%rbp)
	subl	%eax, %edi
	movq	$0, 24(%rbx)
	movl	%edi, %eax
	subl	116(%rbx), %eax
	testl	%eax, %eax
	jle	.L132
	movl	$112, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L133
	movl	-56(%rbp), %ecx
	movq	128(%rbx), %r9
	movq	%rax, %rdi
	xorl	%r8d, %r8d
	movl	120(%rbx), %edx
	movq	%r15, %rsi
	addl	116(%rbx), %edx
	movq	%rax, -56(%rbp)
	call	_ZN6icu_6713StringMatcherC1ERKNS_13UnicodeStringEiiiRKNS_23TransliterationRuleDataE@PLT
	movq	-56(%rbp), %rax
	movq	%rax, 24(%rbx)
.L132:
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L134
	movl	16(%rbp), %edx
	movq	128(%rbx), %rcx
	movq	%r13, %rsi
	movq	%rax, %rdi
	addl	%r12d, %edx
	call	_ZN6icu_6714StringReplacerC1ERKNS_13UnicodeStringEiPKNS_23TransliterationRuleDataE@PLT
	movq	%r15, 32(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movl	12(%rsi), %edi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L110:
	movswl	%ax, %edx
	sarl	$5, %edx
	testw	%ax, %ax
	jns	.L112
	movl	12(%rsi), %edx
.L112:
	subl	116(%rbx), %edx
	movswl	8(%r13), %r9d
	movl	%edx, 120(%rbx)
	testl	%r12d, %r12d
	jns	.L118
.L140:
	testw	%r9w, %r9w
	js	.L119
	movswl	%r9w, %r12d
	sarl	$5, %r12d
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L123:
	movb	$1, 124(%rbx)
	movl	$3, %eax
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$112, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L129
	movl	116(%rbx), %edx
	movl	120(%rbx), %ecx
	movq	%rax, %rdi
	xorl	%r8d, %r8d
	movq	128(%rbx), %r9
	movq	%r15, %rsi
	movq	%rax, -56(%rbp)
	addl	%edx, %ecx
	call	_ZN6icu_6713StringMatcherC1ERKNS_13UnicodeStringEiiiRKNS_23TransliterationRuleDataE@PLT
	movq	-56(%rbp), %rax
	movswl	48(%rbx), %ecx
	movq	%rax, 16(%rbx)
	movl	120(%rbx), %eax
	testw	%cx, %cx
	jns	.L143
.L130:
	movl	52(%rbx), %ecx
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L141:
	movl	$112, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L127
	movl	116(%rbx), %ecx
	movq	128(%rbx), %r9
	movq	%rax, %rdi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_6713StringMatcherC1ERKNS_13UnicodeStringEiiiRKNS_23TransliterationRuleDataE@PLT
	movq	-56(%rbp), %rax
	movq	%rax, 8(%rbx)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L121:
	movl	12(%r13), %r9d
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L115:
	movl	12(%rsi), %eax
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L119:
	movl	12(%r13), %r12d
	jmp	.L120
.L133:
	movq	64(%rbp), %rax
	movq	$0, 24(%rbx)
	movl	$7, (%rax)
	jmp	.L101
.L134:
	movq	64(%rbp), %rax
	movq	$0, 32(%rbx)
	movl	$7, (%rax)
	jmp	.L101
.L127:
	movq	64(%rbp), %rax
	movq	$0, 8(%rbx)
	movl	$7, (%rax)
	jmp	.L101
.L129:
	movq	64(%rbp), %rax
	movq	$0, 16(%rbx)
	movl	$7, (%rax)
	jmp	.L101
	.cfi_endproc
.LFE2424:
	.size	_ZN6icu_6719TransliterationRuleC2ERKNS_13UnicodeStringEiiS3_iiPPNS_14UnicodeFunctorEiaaPKNS_23TransliterationRuleDataER10UErrorCode, .-_ZN6icu_6719TransliterationRuleC2ERKNS_13UnicodeStringEiiS3_iiPPNS_14UnicodeFunctorEiaaPKNS_23TransliterationRuleDataER10UErrorCode
	.globl	_ZN6icu_6719TransliterationRuleC1ERKNS_13UnicodeStringEiiS3_iiPPNS_14UnicodeFunctorEiaaPKNS_23TransliterationRuleDataER10UErrorCode
	.set	_ZN6icu_6719TransliterationRuleC1ERKNS_13UnicodeStringEiiS3_iiPPNS_14UnicodeFunctorEiaaPKNS_23TransliterationRuleDataER10UErrorCode,_ZN6icu_6719TransliterationRuleC2ERKNS_13UnicodeStringEiiS3_iiPPNS_14UnicodeFunctorEiaaPKNS_23TransliterationRuleDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719TransliterationRuleC2ERS0_
	.type	_ZN6icu_6719TransliterationRuleC2ERS0_, @function
_ZN6icu_6719TransliterationRuleC2ERS0_:
.LFB2427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6719TransliterationRuleE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	addq	$40, %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$40, %rdi
	movq	%rax, -40(%rdi)
	movq	$0, -32(%rdi)
	movq	$0, -24(%rdi)
	movq	$0, -16(%rdi)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzbl	124(%r12), %eax
	movq	116(%r12), %rdx
	movq	$0, 104(%rbx)
	movslq	112(%r12), %rdi
	movl	$0, 112(%rbx)
	movb	%al, 124(%rbx)
	movq	128(%r12), %rax
	movq	%rdx, 116(%rbx)
	movq	%rax, 128(%rbx)
	testl	%edi, %edi
	jle	.L145
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movslq	112(%r12), %rdx
	movq	104(%r12), %rsi
	movq	%rax, 104(%rbx)
	movq	%rax, %rdi
	salq	$3, %rdx
	call	memcpy@PLT
.L145:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L146
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 8(%rbx)
.L146:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L147
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 16(%rbx)
.L147:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L148
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 24(%rbx)
.L148:
	movq	32(%r12), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2427:
	.size	_ZN6icu_6719TransliterationRuleC2ERS0_, .-_ZN6icu_6719TransliterationRuleC2ERS0_
	.globl	_ZN6icu_6719TransliterationRuleC1ERS0_
	.set	_ZN6icu_6719TransliterationRuleC1ERS0_,_ZN6icu_6719TransliterationRuleC2ERS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719TransliterationRule13getIndexValueEv
	.type	_ZNK6icu_6719TransliterationRule13getIndexValueEv, @function
_ZNK6icu_6719TransliterationRule13getIndexValueEv:
.LFB2434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movswl	48(%rdi), %eax
	movq	%rdi, %rbx
	movl	116(%rdi), %esi
	testw	%ax, %ax
	js	.L160
	sarl	$5, %eax
.L161:
	cmpl	%eax, %esi
	je	.L164
	leaq	40(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	128(%rbx), %rdi
	movl	%eax, %esi
	movl	%eax, %r12d
	call	_ZNK6icu_6723TransliterationRuleData13lookupMatcherEi@PLT
	testq	%rax, %rax
	jne	.L164
	movzbl	%r12b, %eax
.L159:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movl	52(%rdi), %eax
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L164:
	movl	$-1, %eax
	jmp	.L159
	.cfi_endproc
.LFE2434:
	.size	_ZNK6icu_6719TransliterationRule13getIndexValueEv, .-_ZNK6icu_6719TransliterationRule13getIndexValueEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719TransliterationRule17matchesIndexValueEh
	.type	_ZNK6icu_6719TransliterationRule17matchesIndexValueEh, @function
_ZNK6icu_6719TransliterationRule17matchesIndexValueEh:
.LFB2435:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L169
.L167:
	leaq	8(%rax), %rdi
	movq	8(%rax), %rax
	movzbl	%sil, %esi
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L169:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	jne	.L167
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2435:
	.size	_ZNK6icu_6719TransliterationRule17matchesIndexValueEh, .-_ZNK6icu_6719TransliterationRule17matchesIndexValueEh
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719TransliterationRule15matchAndReplaceERNS_11ReplaceableER14UTransPositiona
	.type	_ZNK6icu_6719TransliterationRule15matchAndReplaceERNS_11ReplaceableER14UTransPositiona, @function
_ZNK6icu_6719TransliterationRule15matchAndReplaceERNS_11ReplaceableER14UTransPositiona:
.LFB2439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	%ecx, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	104(%rdi), %rax
	testq	%rax, %rax
	je	.L171
	movl	112(%rdi), %edx
	testl	%edx, %edx
	jle	.L171
	xorl	%r14d, %r14d
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L207:
	movq	104(%rbx), %rax
.L172:
	movq	(%rax,%r14,8), %rdi
	addq	$1, %r14
	call	_ZN6icu_6713StringMatcher10resetMatchEv@PLT
	cmpl	%r14d, 112(%rbx)
	jg	.L207
.L171:
	movl	0(%r13), %r15d
	leal	-1(%r15), %r14d
	testl	%r15d, %r15d
	jle	.L173
	movq	(%r12), %rax
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*80(%rax)
	leal	-2(%r15), %edx
	cmpl	$65535, %eax
	cmova	%edx, %r14d
.L173:
	movl	8(%r13), %ecx
	testl	%ecx, %ecx
	movl	%ecx, -72(%rbp)
	leal	-1(%rcx), %r15d
	jle	.L175
	movq	(%r12), %rax
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	*80(%rax)
	movl	-72(%rbp), %ecx
	subl	$2, %ecx
	cmpl	$65535, %eax
	cmova	%ecx, %r15d
.L175:
	movq	8(%rbx), %rdi
	movl	%r15d, -64(%rbp)
	testq	%rdi, %rdi
	je	.L177
	movq	(%rdi), %rax
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r12, %rsi
	call	*56(%rax)
	cmpl	$2, %eax
	je	.L178
.L184:
	xorl	%eax, %eax
.L170:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L208
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movl	-64(%rbp), %r15d
.L177:
	testl	%r15d, %r15d
	js	.L180
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*64(%rax)
	cmpl	%r15d, %eax
	jg	.L209
.L180:
	leal	1(%r15), %eax
	movl	%eax, %r15d
.L182:
	testb	$1, 124(%rbx)
	je	.L183
	cmpl	%r14d, -64(%rbp)
	jne	.L184
.L183:
	movl	8(%r13), %r14d
	movq	16(%rbx), %rdi
	movl	%r14d, -64(%rbp)
	testq	%rdi, %rdi
	je	.L185
	movq	(%rdi), %rax
	movl	12(%r13), %ecx
	leaq	-64(%rbp), %rdx
	movq	%r12, %rsi
	movsbl	-68(%rbp), %r8d
	call	*56(%rax)
	cmpl	$2, %eax
	jne	.L170
	movl	-64(%rbp), %r14d
.L185:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L190
	cmpb	$0, -68(%rbp)
	jne	.L210
.L189:
	movq	(%rdi), %rax
	movl	4(%r13), %ecx
	leaq	-64(%rbp), %rdx
	movq	%r12, %rsi
	movsbl	-68(%rbp), %r8d
	call	*56(%rax)
	cmpl	$2, %eax
	jne	.L170
.L190:
	testb	$2, 124(%rbx)
	je	.L188
	movl	-64(%rbp), %eax
	cmpl	%eax, 4(%r13)
	jne	.L184
	cmpb	$0, -68(%rbp)
	jne	.L191
.L188:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movl	%r14d, %ecx
	movl	8(%r13), %edx
	leaq	-60(%rbp), %r8
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	%r12, %rsi
	call	*16(%rax)
	movl	12(%r13), %esi
	subl	8(%r13), %r14d
	subl	%r14d, %eax
	addl	%eax, 4(%r13)
	movl	-64(%rbp), %edi
	addl	%eax, %esi
	movl	-60(%rbp), %r12d
	movl	%esi, 12(%r13)
	addl	%eax, %edi
	movl	%edi, -64(%rbp)
	call	uprv_min_67@PLT
	movl	%r12d, %esi
	movl	%eax, %edi
	call	uprv_min_67@PLT
	movl	%r15d, %edi
	movl	%eax, %esi
	call	uprv_max_67@PLT
	movl	%eax, 8(%r13)
	movl	$2, %eax
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L209:
	movq	(%r12), %rax
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	*80(%rax)
	cmpl	$65535, %eax
	seta	%al
	movzbl	%al, %eax
	leal	1(%r15,%rax), %eax
	movl	%eax, %r15d
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L210:
	cmpl	%r14d, 12(%r13)
	jne	.L189
.L191:
	movl	$1, %eax
	jmp	.L170
.L208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2439:
	.size	_ZNK6icu_6719TransliterationRule15matchAndReplaceERNS_11ReplaceableER14UTransPositiona, .-_ZNK6icu_6719TransliterationRule15matchAndReplaceERNS_11ReplaceableER14UTransPositiona
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719TransliterationRule7setDataEPKNS_23TransliterationRuleDataE
	.type	_ZN6icu_6719TransliterationRule7setDataEPKNS_23TransliterationRuleDataE, @function
_ZN6icu_6719TransliterationRule7setDataEPKNS_23TransliterationRuleDataE:
.LFB2441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, 128(%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L212
	movq	(%rdi), %rax
	call	*48(%rax)
.L212:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L213
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*48(%rax)
.L213:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L214
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*48(%rax)
.L214:
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE2441:
	.size	_ZN6icu_6719TransliterationRule7setDataEPKNS_23TransliterationRuleDataE, .-_ZN6icu_6719TransliterationRule7setDataEPKNS_23TransliterationRuleDataE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719TransliterationRule14addSourceSetToERNS_10UnicodeSetE
	.type	_ZNK6icu_6719TransliterationRule14addSourceSetToERNS_10UnicodeSetE, @function
_ZNK6icu_6719TransliterationRule14addSourceSetToERNS_10UnicodeSetE:
.LFB2442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	116(%rdi), %ebx
	movl	120(%rdi), %eax
	addl	%ebx, %eax
	movl	%eax, -52(%rbp)
	cmpl	%eax, %ebx
	jge	.L225
	movq	%rdi, %r12
	movq	%rsi, %r13
	leaq	40(%rdi), %r15
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L229:
	movq	(%rax), %rax
	movq	%r13, %rsi
	call	*40(%rax)
	cmpl	%ebx, -52(%rbp)
	jle	.L225
.L227:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	128(%r12), %rdi
	movl	%eax, %r14d
	xorl	%eax, %eax
	cmpl	$65535, %r14d
	movl	%r14d, %esi
	seta	%al
	leal	1(%rax,%rbx), %ebx
	call	_ZNK6icu_6723TransliterationRuleData13lookupMatcherEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L229
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	cmpl	%ebx, -52(%rbp)
	jg	.L227
.L225:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2442:
	.size	_ZNK6icu_6719TransliterationRule14addSourceSetToERNS_10UnicodeSetE, .-_ZNK6icu_6719TransliterationRule14addSourceSetToERNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719TransliterationRule14addTargetSetToERNS_10UnicodeSetE
	.type	_ZNK6icu_6719TransliterationRule14addTargetSetToERNS_10UnicodeSetE, @function
_ZNK6icu_6719TransliterationRule14addTargetSetToERNS_10UnicodeSetE:
.LFB2443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE2443:
	.size	_ZNK6icu_6719TransliterationRule14addTargetSetToERNS_10UnicodeSetE, .-_ZNK6icu_6719TransliterationRule14addTargetSetToERNS_10UnicodeSetE
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6719TransliterationRuleE
	.section	.rodata._ZTSN6icu_6719TransliterationRuleE,"aG",@progbits,_ZTSN6icu_6719TransliterationRuleE,comdat
	.align 16
	.type	_ZTSN6icu_6719TransliterationRuleE, @object
	.size	_ZTSN6icu_6719TransliterationRuleE, 31
_ZTSN6icu_6719TransliterationRuleE:
	.string	"N6icu_6719TransliterationRuleE"
	.weak	_ZTIN6icu_6719TransliterationRuleE
	.section	.data.rel.ro._ZTIN6icu_6719TransliterationRuleE,"awG",@progbits,_ZTIN6icu_6719TransliterationRuleE,comdat
	.align 8
	.type	_ZTIN6icu_6719TransliterationRuleE, @object
	.size	_ZTIN6icu_6719TransliterationRuleE, 24
_ZTIN6icu_6719TransliterationRuleE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719TransliterationRuleE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTVN6icu_6719TransliterationRuleE
	.section	.data.rel.ro.local._ZTVN6icu_6719TransliterationRuleE,"awG",@progbits,_ZTVN6icu_6719TransliterationRuleE,comdat
	.align 8
	.type	_ZTVN6icu_6719TransliterationRuleE, @object
	.size	_ZTVN6icu_6719TransliterationRuleE, 56
_ZTVN6icu_6719TransliterationRuleE:
	.quad	0
	.quad	_ZTIN6icu_6719TransliterationRuleE
	.quad	_ZN6icu_6719TransliterationRuleD1Ev
	.quad	_ZN6icu_6719TransliterationRuleD0Ev
	.quad	_ZNK6icu_6719TransliterationRule16getContextLengthEv
	.quad	_ZNK6icu_6719TransliterationRule5masksERKS0_
	.quad	_ZNK6icu_6719TransliterationRule6toRuleERNS_13UnicodeStringEa
	.section	.rodata
	.align 8
	.type	_ZL10FORWARD_OP, @object
	.size	_ZL10FORWARD_OP, 8
_ZL10FORWARD_OP:
	.value	32
	.value	62
	.value	32
	.value	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
