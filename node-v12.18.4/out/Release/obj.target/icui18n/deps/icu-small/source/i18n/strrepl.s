	.file	"strrepl.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714StringReplacer17getDynamicClassIDEv
	.type	_ZNK6icu_6714StringReplacer17getDynamicClassIDEv, @function
_ZNK6icu_6714StringReplacer17getDynamicClassIDEv:
.LFB2427:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714StringReplacer16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2427:
	.size	_ZNK6icu_6714StringReplacer17getDynamicClassIDEv, .-_ZNK6icu_6714StringReplacer17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714StringReplacer10toReplacerEv
	.type	_ZNK6icu_6714StringReplacer10toReplacerEv, @function
_ZNK6icu_6714StringReplacer10toReplacerEv:
.LFB2457:
	.cfi_startproc
	endbr64
	leaq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE2457:
	.size	_ZNK6icu_6714StringReplacer10toReplacerEv, .-_ZNK6icu_6714StringReplacer10toReplacerEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714StringReplacerD2Ev
	.type	_ZN6icu_6714StringReplacerD2Ev, @function
_ZN6icu_6714StringReplacerD2Ev:
.LFB2453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	112+_ZTVN6icu_6714StringReplacerE(%rip), %rax
	leaq	-96(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movups	%xmm0, -16(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	.cfi_endproc
.LFE2453:
	.size	_ZN6icu_6714StringReplacerD2Ev, .-_ZN6icu_6714StringReplacerD2Ev
	.globl	_ZN6icu_6714StringReplacerD1Ev
	.set	_ZN6icu_6714StringReplacerD1Ev,_ZN6icu_6714StringReplacerD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714StringReplacerD0Ev
	.type	_ZN6icu_6714StringReplacerD0Ev, @function
_ZN6icu_6714StringReplacerD0Ev:
.LFB2455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	112+_ZTVN6icu_6714StringReplacerE(%rip), %rax
	leaq	-96(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movups	%xmm0, -16(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2455:
	.size	_ZN6icu_6714StringReplacerD0Ev, .-_ZN6icu_6714StringReplacerD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714StringReplacer5cloneEv
	.type	_ZNK6icu_6714StringReplacer5cloneEv, @function
_ZNK6icu_6714StringReplacer5cloneEv:
.LFB2456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L8
	leaq	112+_ZTVN6icu_6714StringReplacerE(%rip), %rax
	leaq	16(%rbx), %rsi
	leaq	-96(%rax), %rcx
	movq	%rax, %xmm1
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 16(%r12)
	movl	$2, %eax
	movq	%rcx, %xmm0
	leaq	16(%r12), %rdi
	movw	%ax, 24(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	80(%rbx), %eax
	movq	88(%rbx), %rdx
	movl	%eax, 80(%r12)
	movzwl	84(%rbx), %eax
	movq	%rdx, 88(%r12)
	movw	%ax, 84(%r12)
.L8:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2456:
	.size	_ZNK6icu_6714StringReplacer5cloneEv, .-_ZNK6icu_6714StringReplacer5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714StringReplacer7setDataEPKNS_23TransliterationRuleDataE
	.type	_ZN6icu_6714StringReplacer7setDataEPKNS_23TransliterationRuleDataE, @function
_ZN6icu_6714StringReplacer7setDataEPKNS_23TransliterationRuleDataE:
.LFB2461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, 88(%rdi)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L26:
	sarl	$5, %eax
	cmpl	%eax, %r14d
	jge	.L14
.L27:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	88(%rbx), %rdi
	movl	%eax, %esi
	movl	%eax, %r13d
	call	_ZNK6icu_6723TransliterationRuleData6lookupEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L18
	movq	(%rax), %rax
	movq	88(%rbx), %rsi
	call	*48(%rax)
.L18:
	xorl	%eax, %eax
	cmpl	$65535, %r13d
	seta	%al
	leal	1(%r14,%rax), %r14d
.L20:
	movswl	24(%rbx), %eax
	testw	%ax, %ax
	jns	.L26
	movl	28(%rbx), %eax
	cmpl	%eax, %r14d
	jl	.L27
.L14:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2461:
	.size	_ZN6icu_6714StringReplacer7setDataEPKNS_23TransliterationRuleDataE, .-_ZN6icu_6714StringReplacer7setDataEPKNS_23TransliterationRuleDataE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714StringReplacer17toReplacerPatternERNS_13UnicodeStringEa
	.type	_ZNK6icu_6714StringReplacer17toReplacerPatternERNS_13UnicodeStringEa, @function
_ZNK6icu_6714StringReplacer17toReplacerPatternERNS_13UnicodeStringEa:
.LFB2459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rsi), %eax
	testb	$1, %al
	je	.L29
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L30:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %esi
	movzbl	84(%r12), %ecx
	leaq	-192(%rbp), %rbx
	movq	%rax, -192(%rbp)
	movl	80(%r12), %eax
	movsbl	%r13b, %r13d
	movw	%si, -184(%rbp)
	movl	%eax, -228(%rbp)
	movq	%rbx, -224(%rbp)
	testl	%eax, %eax
	jns	.L33
	testb	%cl, %cl
	jne	.L75
.L33:
	xorl	%ebx, %ebx
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L78:
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	%cl, %cl
	setne	%cl
	cmpl	%ebx, %edx
	jle	.L37
.L79:
	cmpl	%ebx, -228(%rbp)
	jne	.L38
	testb	%cl, %cl
	jne	.L76
.L38:
	testw	%ax, %ax
	js	.L39
	movswl	%ax, %edx
	sarl	$5, %edx
.L40:
	movl	$65535, %r15d
	cmpl	%ebx, %edx
	jbe	.L41
	testb	$2, %al
	je	.L42
	leaq	26(%r12), %rax
.L43:
	movzwl	(%rax,%rbx,2), %r15d
.L41:
	movq	88(%r12), %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_6723TransliterationRuleData14lookupReplacerEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L77
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	leaq	_ZThn8_NK6icu_6714StringReplacer17toReplacerPatternERNS_13UnicodeStringEa(%rip), %rsi
	movq	%rax, -128(%rbp)
	movq	(%rdi), %rax
	leaq	-128(%rbp), %r15
	movw	%cx, -120(%rbp)
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L46
	subq	$8, %rdi
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	.LTHUNK3
.L47:
	leaq	-194(%rbp), %r11
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %r9d
	movq	%r11, %rcx
	movl	$32, %eax
	movq	%r15, %rdi
	movq	%r11, -216(%rbp)
	movw	%ax, -194(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movq	-216(%rbp), %r11
	movl	$32, %edx
	movq	%r15, %rdi
	movw	%dx, -194(%rbp)
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r11, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%r14, %rdi
	movl	%r13d, %ecx
	movl	$1, %edx
	movq	-224(%rbp), %r8
	movq	%r15, %rsi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringERKS1_aaS2_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L45:
	movzbl	84(%r12), %ecx
	addq	$1, %rbx
.L48:
	movzwl	24(%r12), %eax
	testw	%ax, %ax
	jns	.L78
	movl	28(%r12), %edx
	testb	%cl, %cl
	setne	%cl
	cmpl	%ebx, %edx
	jg	.L79
.L37:
	movl	-228(%rbp), %eax
	cmpl	%edx, %eax
	jle	.L49
	testb	%cl, %cl
	jne	.L80
.L49:
	movl	%r13d, %ecx
	movl	$1, %edx
	movl	$-1, %esi
	movq	%r14, %rdi
	movq	-224(%rbp), %rbx
	movq	%rbx, %r8
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$200, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movl	28(%r12), %edx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L42:
	movq	40(%r12), %rax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L77:
	movq	-224(%rbp), %r8
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L46:
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	*%rax
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L76:
	movl	%r13d, %ecx
	movl	$1, %edx
	movl	$124, %esi
	movq	%r14, %rdi
	movq	-224(%rbp), %r8
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_@PLT
	movzwl	24(%r12), %eax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L29:
	testw	%ax, %ax
	js	.L31
	movswl	%ax, %edx
	sarl	$5, %edx
.L32:
	testl	%edx, %edx
	je	.L30
	andl	$31, %eax
	movw	%ax, 8(%r14)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L80:
	movl	%eax, %ebx
	movq	-224(%rbp), %r12
	subl	%edx, %ebx
	subl	$1, %ebx
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r12, %r8
	movl	%r13d, %ecx
	movl	$1, %edx
	movl	$64, %esi
	movq	%r14, %rdi
	subl	$1, %ebx
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_@PLT
	cmpl	$-1, %ebx
	jne	.L50
	movl	%r13d, %ecx
	movl	$1, %edx
	movl	$124, %esi
	movq	%r14, %rdi
	movq	-224(%rbp), %r8
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%rbx, %r15
	movl	%eax, %ebx
	leal	1(%rax), %eax
	movl	%eax, -216(%rbp)
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r15, %r8
	movl	%r13d, %ecx
	movl	$1, %edx
	movl	$64, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_@PLT
	addl	$1, %ebx
	jne	.L34
	movl	-228(%rbp), %ecx
	movl	-216(%rbp), %eax
	negl	%eax
	testl	%ecx, %ecx
	cmovns	%ebx, %eax
	leal	2(%rcx,%rax), %eax
	movzbl	84(%r12), %ecx
	movl	%eax, -228(%rbp)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L31:
	movl	12(%rsi), %edx
	jmp	.L32
.L81:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2459:
	.size	_ZNK6icu_6714StringReplacer17toReplacerPatternERNS_13UnicodeStringEa, .-_ZNK6icu_6714StringReplacer17toReplacerPatternERNS_13UnicodeStringEa
	.set	.LTHUNK3,_ZNK6icu_6714StringReplacer17toReplacerPatternERNS_13UnicodeStringEa
	.p2align 4
	.globl	_ZThn8_NK6icu_6714StringReplacer17toReplacerPatternERNS_13UnicodeStringEa
	.type	_ZThn8_NK6icu_6714StringReplacer17toReplacerPatternERNS_13UnicodeStringEa, @function
_ZThn8_NK6icu_6714StringReplacer17toReplacerPatternERNS_13UnicodeStringEa:
.LFB3181:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK3
	.cfi_endproc
.LFE3181:
	.size	_ZThn8_NK6icu_6714StringReplacer17toReplacerPatternERNS_13UnicodeStringEa, .-_ZThn8_NK6icu_6714StringReplacer17toReplacerPatternERNS_13UnicodeStringEa
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714StringReplacer19addReplacementSetToERNS_10UnicodeSetE
	.type	_ZNK6icu_6714StringReplacer19addReplacementSetToERNS_10UnicodeSetE, @function
_ZNK6icu_6714StringReplacer19addReplacementSetToERNS_10UnicodeSetE:
.LFB2460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZThn8_NK6icu_6714StringReplacer19addReplacementSetToERNS_10UnicodeSetE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L94:
	sarl	$5, %eax
	cmpl	%eax, %r15d
	jge	.L82
.L95:
	leaq	16(%r12), %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	88(%r12), %rdi
	movl	%eax, %esi
	movl	%eax, %ebx
	call	_ZNK6icu_6723TransliterationRuleData14lookupReplacerEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L93
	movq	(%rax), %rax
	movq	32(%rax), %rax
	cmpq	%r14, %rax
	jne	.L88
	subq	$8, %rdi
	movq	%r13, %rsi
	call	.LTHUNK4
.L87:
	xorl	%eax, %eax
	cmpl	$65535, %ebx
	seta	%al
	leal	1(%r15,%rax), %r15d
.L90:
	movswl	24(%r12), %eax
	testw	%ax, %ax
	jns	.L94
	movl	28(%r12), %eax
	cmpl	%eax, %r15d
	jl	.L95
.L82:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r13, %rsi
	call	*%rax
	jmp	.L87
	.cfi_endproc
.LFE2460:
	.size	_ZNK6icu_6714StringReplacer19addReplacementSetToERNS_10UnicodeSetE, .-_ZNK6icu_6714StringReplacer19addReplacementSetToERNS_10UnicodeSetE
	.set	.LTHUNK4,_ZNK6icu_6714StringReplacer19addReplacementSetToERNS_10UnicodeSetE
	.p2align 4
	.globl	_ZThn8_NK6icu_6714StringReplacer19addReplacementSetToERNS_10UnicodeSetE
	.type	_ZThn8_NK6icu_6714StringReplacer19addReplacementSetToERNS_10UnicodeSetE, @function
_ZThn8_NK6icu_6714StringReplacer19addReplacementSetToERNS_10UnicodeSetE:
.LFB3182:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK4
	.cfi_endproc
.LFE3182:
	.size	_ZThn8_NK6icu_6714StringReplacer19addReplacementSetToERNS_10UnicodeSetE, .-_ZThn8_NK6icu_6714StringReplacer19addReplacementSetToERNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714StringReplacer7replaceERNS_11ReplaceableEiiRi
	.type	_ZN6icu_6714StringReplacer7replaceERNS_11ReplaceableEiiRi, @function
_ZN6icu_6714StringReplacer7replaceERNS_11ReplaceableEiiRi:
.LFB2458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movl	%edx, -220(%rbp)
	movl	%ecx, -236(%rbp)
	movq	%r8, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 85(%rdi)
	jne	.L97
	movq	(%r15), %rax
	movl	%ecx, %edi
	movl	%edx, %esi
	leaq	16(%rbx), %rcx
	movl	%edi, %edx
	movq	%r15, %rdi
	call	*32(%rax)
	movswl	24(%rbx), %r12d
	testw	%r12w, %r12w
	js	.L98
	sarl	$5, %r12d
.L99:
	movl	80(%rbx), %eax
	cmpb	$0, 84(%rbx)
	movl	%eax, -224(%rbp)
	je	.L96
.L161:
	movl	80(%rbx), %r13d
	testl	%r13d, %r13d
	js	.L156
	movswl	24(%rbx), %eax
	testw	%ax, %ax
	js	.L133
	sarl	$5, %eax
.L134:
	movl	-224(%rbp), %ebx
	movl	-220(%rbp), %edx
	addl	%edx, %ebx
	cmpl	%eax, %r13d
	jle	.L132
	addl	%r12d, %edx
	subl	%eax, %r13d
	movl	%edx, %ebx
	.p2align 4,,10
	.p2align 3
.L140:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*64(%rax)
	cmpl	%ebx, %eax
	jle	.L136
	movq	(%r15), %rax
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	*80(%rax)
	cmpl	$65535, %eax
	ja	.L137
	addl	$1, %ebx
	subl	$1, %r13d
	jne	.L140
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L97:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movb	$0, 85(%rdi)
	movl	$2, %esi
	movq	%r15, %rdi
	movq	%rax, -192(%rbp)
	movq	(%r15), %rax
	movw	%si, -184(%rbp)
	call	*64(%rax)
	movl	-220(%rbp), %r14d
	movl	%eax, -232(%rbp)
	testl	%r14d, %r14d
	jle	.L101
	movq	(%r15), %rax
	leal	-1(%r14), %r12d
	movq	%r15, %rdi
	movl	$1, %r13d
	movl	%r12d, %esi
	call	*80(%rax)
	cmpl	$65535, %eax
	ja	.L157
.L102:
	movl	-232(%rbp), %r14d
	movq	(%r15), %rax
	movl	%r12d, %esi
	movq	%r15, %rdi
	movl	-220(%rbp), %edx
	movl	%r14d, %ecx
	addl	%r14d, %r13d
	call	*40(%rax)
	leaq	-128(%rbp), %rax
	movl	%r13d, -228(%rbp)
	movq	%rax, -248(%rbp)
.L103:
	movl	-228(%rbp), %r14d
	xorl	%r13d, %r13d
	leaq	-192(%rbp), %rax
	movl	$0, -224(%rbp)
	movq	%rax, -200(%rbp)
	movl	%r14d, %eax
	movl	%r13d, %r14d
	movl	%eax, %r13d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L159:
	sarl	$5, %eax
	cmpl	%eax, %r14d
	jge	.L106
.L160:
	cmpl	%r14d, 80(%rbx)
	jne	.L107
	movl	%r13d, %eax
	subl	-228(%rbp), %eax
	movl	%eax, -224(%rbp)
.L107:
	leaq	16(%rbx), %rdi
	movl	%r14d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	88(%rbx), %rdi
	movl	%eax, %esi
	movl	%eax, %r12d
	call	_ZNK6icu_6723TransliterationRuleData14lookupReplacerEi@PLT
	testq	%rax, %rax
	je	.L158
	movswl	-184(%rbp), %ecx
	movb	$1, 85(%rbx)
	testw	%cx, %cx
	js	.L110
	sarl	$5, %ecx
.L111:
	testl	%ecx, %ecx
	jle	.L112
	movq	(%r15), %r8
	movq	%rax, -216(%rbp)
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	-200(%rbp), %rcx
	movq	%r15, %rdi
	call	*32(%r8)
	movzwl	-184(%rbp), %ecx
	movq	-216(%rbp), %rax
	testw	%cx, %cx
	js	.L113
	movswl	%cx, %esi
	sarl	$5, %esi
	addl	%esi, %r13d
	testb	$1, %cl
	je	.L115
.L114:
	movq	-200(%rbp), %rdi
	movq	%rax, -216(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movq	-216(%rbp), %rax
.L112:
	movq	(%rax), %rcx
	leaq	_ZThn8_N6icu_6714StringReplacer7replaceERNS_11ReplaceableEiiRi(%rip), %rdx
	movq	16(%rcx), %r10
	cmpq	%rdx, %r10
	jne	.L116
	movq	-208(%rbp), %r8
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movq	%r15, %rsi
	leaq	-8(%rax), %rdi
	call	.LTHUNK2
.L117:
	addl	%eax, %r13d
.L109:
	xorl	%eax, %eax
	cmpl	$65535, %r12d
	seta	%al
	leal	1(%r14,%rax), %r14d
.L119:
	movswl	24(%rbx), %eax
	testw	%ax, %ax
	jns	.L159
	movl	28(%rbx), %eax
	cmpl	%eax, %r14d
	jl	.L160
.L106:
	movl	%r13d, %eax
	movl	%r14d, %r13d
	movl	%eax, %r14d
	movswl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L120
	sarl	$5, %eax
.L121:
	movq	(%r15), %r8
	testl	%eax, %eax
	jle	.L122
	movq	-200(%rbp), %rcx
	movl	%r14d, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*32(%r8)
	movswl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L123
	sarl	$5, %eax
.L124:
	movq	(%r15), %r8
	addl	%eax, %r14d
.L122:
	movl	-228(%rbp), %esi
	movl	%r14d, %r12d
	movl	%r14d, %edx
	movq	%r15, %rdi
	movl	-224(%rbp), %eax
	movl	-220(%rbp), %ecx
	subl	%esi, %r12d
	cmpl	%r13d, 80(%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r13
	cmove	%r12d, %eax
	movl	%eax, -224(%rbp)
	call	*40(%r8)
	movq	(%r15), %rax
	movl	$2, %edx
	movq	%r15, %rdi
	movl	-232(%rbp), %esi
	movq	32(%rax), %rax
	movw	%dx, -120(%rbp)
	leal	(%r14,%r12), %edx
	movq	-248(%rbp), %r14
	addl	%r12d, %esi
	movq	%r13, -128(%rbp)
	movq	%r14, %rcx
	call	*%rax
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r15), %rax
	movl	$2, %ecx
	movq	%r15, %rdi
	movl	-236(%rbp), %edx
	movl	-220(%rbp), %esi
	movq	32(%rax), %rax
	movw	%cx, -120(%rbp)
	movq	%r14, %rcx
	addl	%r12d, %edx
	addl	%r12d, %esi
	movq	%r13, -128(%rbp)
	call	*%rax
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-200(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 84(%rbx)
	jne	.L161
.L96:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L162
	addq	$216, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movl	28(%rbx), %r12d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L110:
	movl	-180(%rbp), %ecx
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L113:
	movl	-180(%rbp), %esi
	addl	%esi, %r13d
	testb	$1, %cl
	jne	.L114
.L115:
	testl	%esi, %esi
	je	.L112
	andl	$31, %ecx
	movw	%cx, -184(%rbp)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L158:
	movq	-200(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L116:
	movq	-208(%rbp), %r8
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	*%r10
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L120:
	movl	-180(%rbp), %eax
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L156:
	movl	-220(%rbp), %r14d
	testl	%r14d, %r14d
	jle	.L128
	.p2align 4,,10
	.p2align 3
.L131:
	movq	(%r15), %rax
	leal	-1(%r14), %ebx
	movq	%r15, %rdi
	movl	%ebx, %esi
	call	*80(%rax)
	cmpl	$65535, %eax
	jbe	.L129
	subl	$2, %r14d
	addl	$1, %r13d
	testl	%r14d, %r14d
	jle	.L154
	testl	%r13d, %r13d
	js	.L131
.L154:
	movl	%r14d, -220(%rbp)
.L128:
	movl	-220(%rbp), %ebx
	addl	%r13d, %ebx
.L132:
	movq	-208(%rbp), %rax
	movl	%ebx, (%rax)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L129:
	addl	$1, %r13d
	jns	.L144
	testl	%ebx, %ebx
	jle	.L144
	movl	%ebx, %r14d
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L157:
	leal	-2(%r14), %r12d
	movl	$2, %r13d
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	-128(%rbp), %r14
	movl	$65535, %esi
	movq	%r14, %rdi
	movq	%r14, -248(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r14, %rcx
	movl	-232(%rbp), %r12d
	movl	%r12d, %edx
	movl	%r12d, %esi
	call	*32(%rax)
	movl	%r12d, %eax
	movq	%r14, %rdi
	addl	$1, %eax
	movl	%eax, -228(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L123:
	movl	-180(%rbp), %eax
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L133:
	movl	28(%rbx), %eax
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L137:
	addl	$2, %ebx
	subl	$1, %r13d
	jne	.L140
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L144:
	movl	%ebx, -220(%rbp)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L136:
	addl	%r13d, %ebx
	jmp	.L132
.L162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2458:
	.size	_ZN6icu_6714StringReplacer7replaceERNS_11ReplaceableEiiRi, .-_ZN6icu_6714StringReplacer7replaceERNS_11ReplaceableEiiRi
	.set	.LTHUNK2,_ZN6icu_6714StringReplacer7replaceERNS_11ReplaceableEiiRi
	.p2align 4
	.globl	_ZThn8_N6icu_6714StringReplacer7replaceERNS_11ReplaceableEiiRi
	.type	_ZThn8_N6icu_6714StringReplacer7replaceERNS_11ReplaceableEiiRi, @function
_ZThn8_N6icu_6714StringReplacer7replaceERNS_11ReplaceableEiiRi:
.LFB3183:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK2
	.cfi_endproc
.LFE3183:
	.size	_ZThn8_N6icu_6714StringReplacer7replaceERNS_11ReplaceableEiiRi, .-_ZThn8_N6icu_6714StringReplacer7replaceERNS_11ReplaceableEiiRi
	.p2align 4
	.globl	_ZThn8_N6icu_6714StringReplacerD1Ev
	.type	_ZThn8_N6icu_6714StringReplacerD1Ev, @function
_ZThn8_N6icu_6714StringReplacerD1Ev:
.LFB3177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	112+_ZTVN6icu_6714StringReplacerE(%rip), %rax
	leaq	-96(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	addq	$8, %rdi
	subq	$8, %rsp
	movups	%xmm0, -16(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	.cfi_endproc
.LFE3177:
	.size	_ZThn8_N6icu_6714StringReplacerD1Ev, .-_ZThn8_N6icu_6714StringReplacerD1Ev
	.p2align 4
	.globl	_ZThn8_N6icu_6714StringReplacerD0Ev
	.type	_ZThn8_N6icu_6714StringReplacerD0Ev, @function
_ZThn8_N6icu_6714StringReplacerD0Ev:
.LFB3178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	112+_ZTVN6icu_6714StringReplacerE(%rip), %rax
	leaq	-96(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	addq	$8, %rdi
	subq	$8, %rsp
	movups	%xmm0, -16(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3178:
	.size	_ZThn8_N6icu_6714StringReplacerD0Ev, .-_ZThn8_N6icu_6714StringReplacerD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715UnicodeReplacerD2Ev
	.type	_ZN6icu_6715UnicodeReplacerD2Ev, @function
_ZN6icu_6715UnicodeReplacerD2Ev:
.LFB2423:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2423:
	.size	_ZN6icu_6715UnicodeReplacerD2Ev, .-_ZN6icu_6715UnicodeReplacerD2Ev
	.globl	_ZN6icu_6715UnicodeReplacerD1Ev
	.set	_ZN6icu_6715UnicodeReplacerD1Ev,_ZN6icu_6715UnicodeReplacerD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715UnicodeReplacerD0Ev
	.type	_ZN6icu_6715UnicodeReplacerD0Ev, @function
_ZN6icu_6715UnicodeReplacerD0Ev:
.LFB2425:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE2425:
	.size	_ZN6icu_6715UnicodeReplacerD0Ev, .-_ZN6icu_6715UnicodeReplacerD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714StringReplacer16getStaticClassIDEv
	.type	_ZN6icu_6714StringReplacer16getStaticClassIDEv, @function
_ZN6icu_6714StringReplacer16getStaticClassIDEv:
.LFB2426:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714StringReplacer16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2426:
	.size	_ZN6icu_6714StringReplacer16getStaticClassIDEv, .-_ZN6icu_6714StringReplacer16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714StringReplacerC2ERKNS_13UnicodeStringEiPKNS_23TransliterationRuleDataE
	.type	_ZN6icu_6714StringReplacerC2ERKNS_13UnicodeStringEiPKNS_23TransliterationRuleDataE, @function
_ZN6icu_6714StringReplacerC2ERKNS_13UnicodeStringEiPKNS_23TransliterationRuleDataE:
.LFB2435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	112+_ZTVN6icu_6714StringReplacerE(%rip), %rax
	movq	%rax, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	leaq	-96(%rax), %rdx
	pushq	%r12
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdx, %xmm0
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movups	%xmm0, -16(%rdi)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	$257, %edx
	movl	%r13d, 80(%rbx)
	movq	%r12, 88(%rbx)
	movw	%dx, 84(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2435:
	.size	_ZN6icu_6714StringReplacerC2ERKNS_13UnicodeStringEiPKNS_23TransliterationRuleDataE, .-_ZN6icu_6714StringReplacerC2ERKNS_13UnicodeStringEiPKNS_23TransliterationRuleDataE
	.globl	_ZN6icu_6714StringReplacerC1ERKNS_13UnicodeStringEiPKNS_23TransliterationRuleDataE
	.set	_ZN6icu_6714StringReplacerC1ERKNS_13UnicodeStringEiPKNS_23TransliterationRuleDataE,_ZN6icu_6714StringReplacerC2ERKNS_13UnicodeStringEiPKNS_23TransliterationRuleDataE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714StringReplacerC2ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataE
	.type	_ZN6icu_6714StringReplacerC2ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataE, @function
_ZN6icu_6714StringReplacerC2ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataE:
.LFB2438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	112+_ZTVN6icu_6714StringReplacerE(%rip), %rax
	leaq	-96(%rax), %rcx
	movq	%rax, %xmm1
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movups	%xmm0, -16(%rdi)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	$256, %edx
	movq	%r12, 88(%rbx)
	movl	$0, 80(%rbx)
	movw	%dx, 84(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2438:
	.size	_ZN6icu_6714StringReplacerC2ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataE, .-_ZN6icu_6714StringReplacerC2ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataE
	.globl	_ZN6icu_6714StringReplacerC1ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataE
	.set	_ZN6icu_6714StringReplacerC1ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataE,_ZN6icu_6714StringReplacerC2ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714StringReplacerC2ERKS0_
	.type	_ZN6icu_6714StringReplacerC2ERKS0_, @function
_ZN6icu_6714StringReplacerC2ERKS0_:
.LFB2450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	112+_ZTVN6icu_6714StringReplacerE(%rip), %rax
	leaq	-96(%rax), %rcx
	movq	%rax, %xmm1
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	16(%rsi), %rsi
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movups	%xmm0, -16(%rdi)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	80(%r12), %eax
	movq	88(%r12), %rdx
	movl	%eax, 80(%rbx)
	movzwl	84(%r12), %eax
	movq	%rdx, 88(%rbx)
	movw	%ax, 84(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2450:
	.size	_ZN6icu_6714StringReplacerC2ERKS0_, .-_ZN6icu_6714StringReplacerC2ERKS0_
	.globl	_ZN6icu_6714StringReplacerC1ERKS0_
	.set	_ZN6icu_6714StringReplacerC1ERKS0_,_ZN6icu_6714StringReplacerC2ERKS0_
	.weak	_ZTSN6icu_6715UnicodeReplacerE
	.section	.rodata._ZTSN6icu_6715UnicodeReplacerE,"aG",@progbits,_ZTSN6icu_6715UnicodeReplacerE,comdat
	.align 16
	.type	_ZTSN6icu_6715UnicodeReplacerE, @object
	.size	_ZTSN6icu_6715UnicodeReplacerE, 27
_ZTSN6icu_6715UnicodeReplacerE:
	.string	"N6icu_6715UnicodeReplacerE"
	.weak	_ZTIN6icu_6715UnicodeReplacerE
	.section	.data.rel.ro._ZTIN6icu_6715UnicodeReplacerE,"awG",@progbits,_ZTIN6icu_6715UnicodeReplacerE,comdat
	.align 8
	.type	_ZTIN6icu_6715UnicodeReplacerE, @object
	.size	_ZTIN6icu_6715UnicodeReplacerE, 16
_ZTIN6icu_6715UnicodeReplacerE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_6715UnicodeReplacerE
	.weak	_ZTSN6icu_6714StringReplacerE
	.section	.rodata._ZTSN6icu_6714StringReplacerE,"aG",@progbits,_ZTSN6icu_6714StringReplacerE,comdat
	.align 16
	.type	_ZTSN6icu_6714StringReplacerE, @object
	.size	_ZTSN6icu_6714StringReplacerE, 26
_ZTSN6icu_6714StringReplacerE:
	.string	"N6icu_6714StringReplacerE"
	.weak	_ZTIN6icu_6714StringReplacerE
	.section	.data.rel.ro._ZTIN6icu_6714StringReplacerE,"awG",@progbits,_ZTIN6icu_6714StringReplacerE,comdat
	.align 8
	.type	_ZTIN6icu_6714StringReplacerE, @object
	.size	_ZTIN6icu_6714StringReplacerE, 56
_ZTIN6icu_6714StringReplacerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_6714StringReplacerE
	.long	0
	.long	2
	.quad	_ZTIN6icu_6714UnicodeFunctorE
	.quad	2
	.quad	_ZTIN6icu_6715UnicodeReplacerE
	.quad	2050
	.weak	_ZTVN6icu_6715UnicodeReplacerE
	.section	.data.rel.ro._ZTVN6icu_6715UnicodeReplacerE,"awG",@progbits,_ZTVN6icu_6715UnicodeReplacerE,comdat
	.align 8
	.type	_ZTVN6icu_6715UnicodeReplacerE, @object
	.size	_ZTVN6icu_6715UnicodeReplacerE, 56
_ZTVN6icu_6715UnicodeReplacerE:
	.quad	0
	.quad	_ZTIN6icu_6715UnicodeReplacerE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6714StringReplacerE
	.section	.data.rel.ro._ZTVN6icu_6714StringReplacerE,"awG",@progbits,_ZTVN6icu_6714StringReplacerE,comdat
	.align 8
	.type	_ZTVN6icu_6714StringReplacerE, @object
	.size	_ZTVN6icu_6714StringReplacerE, 152
_ZTVN6icu_6714StringReplacerE:
	.quad	0
	.quad	_ZTIN6icu_6714StringReplacerE
	.quad	_ZN6icu_6714StringReplacerD1Ev
	.quad	_ZN6icu_6714StringReplacerD0Ev
	.quad	_ZNK6icu_6714StringReplacer17getDynamicClassIDEv
	.quad	_ZNK6icu_6714StringReplacer5cloneEv
	.quad	_ZNK6icu_6714UnicodeFunctor9toMatcherEv
	.quad	_ZNK6icu_6714StringReplacer10toReplacerEv
	.quad	_ZN6icu_6714StringReplacer7setDataEPKNS_23TransliterationRuleDataE
	.quad	_ZN6icu_6714StringReplacer7replaceERNS_11ReplaceableEiiRi
	.quad	_ZNK6icu_6714StringReplacer17toReplacerPatternERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6714StringReplacer19addReplacementSetToERNS_10UnicodeSetE
	.quad	-8
	.quad	_ZTIN6icu_6714StringReplacerE
	.quad	_ZThn8_N6icu_6714StringReplacerD1Ev
	.quad	_ZThn8_N6icu_6714StringReplacerD0Ev
	.quad	_ZThn8_N6icu_6714StringReplacer7replaceERNS_11ReplaceableEiiRi
	.quad	_ZThn8_NK6icu_6714StringReplacer17toReplacerPatternERNS_13UnicodeStringEa
	.quad	_ZThn8_NK6icu_6714StringReplacer19addReplacementSetToERNS_10UnicodeSetE
	.local	_ZZN6icu_6714StringReplacer16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6714StringReplacer16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
