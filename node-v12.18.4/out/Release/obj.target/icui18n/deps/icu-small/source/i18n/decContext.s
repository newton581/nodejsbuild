	.file	"decContext.cpp"
	.text
	.p2align 4
	.globl	uprv_decContextClearStatus_67
	.type	uprv_decContextClearStatus_67, @function
uprv_decContextClearStatus_67:
.LFB85:
	.cfi_startproc
	endbr64
	notl	%esi
	movq	%rdi, %rax
	andl	%esi, 20(%rdi)
	ret
	.cfi_endproc
.LFE85:
	.size	uprv_decContextClearStatus_67, .-uprv_decContextClearStatus_67
	.p2align 4
	.globl	uprv_decContextDefault_67
	.type	uprv_decContextDefault_67, @function
uprv_decContextDefault_67:
.LFB86:
	.cfi_startproc
	endbr64
	movabsq	$4294967291705032713, %rdx
	movq	$8927, 16(%rdi)
	movq	%rdi, %rax
	movabsq	$11884901889, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 8(%rdi)
	movb	$0, 24(%rdi)
	cmpl	$64, %esi
	je	.L4
	jg	.L5
	testl	%esi, %esi
	je	.L6
	cmpl	$32, %esi
	jne	.L8
	movabsq	$412316860423, %rdx
	movl	$0, 16(%rdi)
	movabsq	$17179869089, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 8(%rdi)
	movb	$1, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$128, %esi
	jne	.L8
	movabsq	$26388279066658, %rdx
	movl	$0, 16(%rdi)
	movabsq	$17179863041, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 8(%rdi)
	movb	$1, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$128, 20(%rax)
.L6:
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movabsq	$1649267441680, %rdx
	movl	$0, 16(%rdi)
	movabsq	$17179868801, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 8(%rdi)
	movb	$1, 24(%rdi)
	ret
	.cfi_endproc
.LFE86:
	.size	uprv_decContextDefault_67, .-uprv_decContextDefault_67
	.p2align 4
	.globl	uprv_decContextGetRounding_67
	.type	uprv_decContextGetRounding_67, @function
uprv_decContextGetRounding_67:
.LFB87:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	ret
	.cfi_endproc
.LFE87:
	.size	uprv_decContextGetRounding_67, .-uprv_decContextGetRounding_67
	.p2align 4
	.globl	uprv_decContextGetStatus_67
	.type	uprv_decContextGetStatus_67, @function
uprv_decContextGetStatus_67:
.LFB88:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	ret
	.cfi_endproc
.LFE88:
	.size	uprv_decContextGetStatus_67, .-uprv_decContextGetStatus_67
	.p2align 4
	.globl	uprv_decContextRestoreStatus_67
	.type	uprv_decContextRestoreStatus_67, @function
uprv_decContextRestoreStatus_67:
.LFB89:
	.cfi_startproc
	endbr64
	movl	%edx, %r8d
	movl	20(%rdi), %edx
	movq	%rdi, %rax
	xorl	%edx, %esi
	andl	%r8d, %esi
	xorl	%edx, %esi
	movl	%esi, 20(%rdi)
	ret
	.cfi_endproc
.LFE89:
	.size	uprv_decContextRestoreStatus_67, .-uprv_decContextRestoreStatus_67
	.p2align 4
	.globl	uprv_decContextSaveStatus_67
	.type	uprv_decContextSaveStatus_67, @function
uprv_decContextSaveStatus_67:
.LFB90:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	andl	%esi, %eax
	ret
	.cfi_endproc
.LFE90:
	.size	uprv_decContextSaveStatus_67, .-uprv_decContextSaveStatus_67
	.p2align 4
	.globl	uprv_decContextSetRounding_67
	.type	uprv_decContextSetRounding_67, @function
uprv_decContextSetRounding_67:
.LFB91:
	.cfi_startproc
	endbr64
	movl	%esi, 12(%rdi)
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE91:
	.size	uprv_decContextSetRounding_67, .-uprv_decContextSetRounding_67
	.p2align 4
	.globl	uprv_decContextSetStatus_67
	.type	uprv_decContextSetStatus_67, @function
uprv_decContextSetStatus_67:
.LFB92:
	.cfi_startproc
	endbr64
	orl	%esi, 20(%rdi)
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE92:
	.size	uprv_decContextSetStatus_67, .-uprv_decContextSetStatus_67
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Conversion syntax"
.LC1:
	.string	"Division by zero"
.LC2:
	.string	"Division impossible"
.LC3:
	.string	"Division undefined"
.LC4:
	.string	"Inexact"
.LC5:
	.string	"Insufficient storage"
.LC6:
	.string	"Invalid context"
.LC7:
	.string	"Invalid operation"
.LC8:
	.string	"Overflow"
.LC9:
	.string	"Clamped"
.LC10:
	.string	"Rounded"
.LC11:
	.string	"Subnormal"
.LC12:
	.string	"Underflow"
.LC13:
	.string	"No status"
	.text
	.p2align 4
	.globl	uprv_decContextSetStatusFromString_67
	.type	uprv_decContextSetStatusFromString_67, @function
uprv_decContextSetStatusFromString_67:
.LFB93:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$18, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	.LC0(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L20
	orl	$1, 20(%rbx)
	movq	%rbx, %rax
.L19:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movl	$17, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L22
	movq	%rbx, %rax
	orl	$2, 20(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	$20, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L36
	movl	$19, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L24
	orl	$8, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%rbx, %rax
	orl	$4, 20(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movl	$8, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L37
	movl	$21, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L26
	orl	$16, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L37:
	orl	$32, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$16, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L27
	orl	$64, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L19
.L27:
	movl	$18, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L28
	orl	$128, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L19
.L28:
	movl	$9, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L29
	orl	$512, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L19
.L29:
	movl	$8, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L30
	orl	$1024, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L19
.L30:
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L31
	orl	$2048, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L19
.L31:
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L32
	orl	$4096, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L19
.L32:
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L33
	orl	$8192, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L19
.L33:
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	movl	$0, %eax
	cmove	%rbx, %rax
	jmp	.L19
	.cfi_endproc
.LFE93:
	.size	uprv_decContextSetStatusFromString_67, .-uprv_decContextSetStatusFromString_67
	.p2align 4
	.globl	uprv_decContextSetStatusFromStringQuiet_67
	.type	uprv_decContextSetStatusFromStringQuiet_67, @function
uprv_decContextSetStatusFromStringQuiet_67:
.LFB103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$18, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	.LC0(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L39
	orl	$1, 20(%rbx)
	movq	%rbx, %rax
.L38:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movl	$17, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L41
	movq	%rbx, %rax
	orl	$2, 20(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	$20, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L55
	movl	$19, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L43
	orl	$8, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%rbx, %rax
	orl	$4, 20(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	$8, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L56
	movl	$21, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L45
	orl	$16, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L56:
	orl	$32, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$16, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L46
	orl	$64, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L38
.L46:
	movl	$18, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L47
	orl	$128, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L38
.L47:
	movl	$9, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L48
	orl	$512, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L38
.L48:
	movl	$8, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L49
	orl	$1024, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L38
.L49:
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L50
	orl	$2048, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L38
.L50:
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L51
	orl	$4096, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L38
.L51:
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L52
	orl	$8192, 20(%rbx)
	movq	%rbx, %rax
	jmp	.L38
.L52:
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	movl	$0, %eax
	cmove	%rbx, %rax
	jmp	.L38
	.cfi_endproc
.LFE103:
	.size	uprv_decContextSetStatusFromStringQuiet_67, .-uprv_decContextSetStatusFromStringQuiet_67
	.p2align 4
	.globl	uprv_decContextSetStatusQuiet_67
	.type	uprv_decContextSetStatusQuiet_67, @function
uprv_decContextSetStatusQuiet_67:
.LFB101:
	.cfi_startproc
	endbr64
	orl	%esi, 20(%rdi)
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE101:
	.size	uprv_decContextSetStatusQuiet_67, .-uprv_decContextSetStatusQuiet_67
	.section	.rodata.str1.1
.LC14:
	.string	"Multiple status"
	.text
	.p2align 4
	.globl	uprv_decContextStatusToString_67
	.type	uprv_decContextStatusToString_67, @function
uprv_decContextStatusToString_67:
.LFB96:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %edx
	leaq	.LC7(%rip), %rax
	cmpl	$128, %edx
	je	.L58
	leaq	.LC1(%rip), %rax
	cmpl	$2, %edx
	je	.L58
	leaq	.LC8(%rip), %rax
	cmpl	$512, %edx
	je	.L58
	leaq	.LC12(%rip), %rax
	cmpl	$8192, %edx
	je	.L58
	leaq	.LC4(%rip), %rax
	cmpl	$32, %edx
	je	.L58
	leaq	.LC2(%rip), %rax
	cmpl	$4, %edx
	je	.L58
	leaq	.LC3(%rip), %rax
	cmpl	$8, %edx
	je	.L58
	leaq	.LC10(%rip), %rax
	cmpl	$2048, %edx
	je	.L58
	leaq	.LC9(%rip), %rax
	cmpl	$1024, %edx
	je	.L58
	leaq	.LC11(%rip), %rax
	cmpl	$4096, %edx
	je	.L58
	leaq	.LC0(%rip), %rax
	cmpl	$1, %edx
	je	.L58
	leaq	.LC5(%rip), %rax
	cmpl	$16, %edx
	je	.L58
	leaq	.LC6(%rip), %rax
	cmpl	$64, %edx
	je	.L58
	testl	%edx, %edx
	leaq	.LC14(%rip), %rax
	leaq	.LC13(%rip), %rdx
	cmove	%rdx, %rax
.L58:
	ret
	.cfi_endproc
.LFE96:
	.size	uprv_decContextStatusToString_67, .-uprv_decContextStatusToString_67
	.p2align 4
	.globl	uprv_decContextTestSavedStatus_67
	.type	uprv_decContextTestSavedStatus_67, @function
uprv_decContextTestSavedStatus_67:
.LFB97:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %edi
	setne	%al
	ret
	.cfi_endproc
.LFE97:
	.size	uprv_decContextTestSavedStatus_67, .-uprv_decContextTestSavedStatus_67
	.p2align 4
	.globl	uprv_decContextTestStatus_67
	.type	uprv_decContextTestStatus_67, @function
uprv_decContextTestStatus_67:
.LFB98:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, 20(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE98:
	.size	uprv_decContextTestStatus_67, .-uprv_decContextTestStatus_67
	.p2align 4
	.globl	uprv_decContextZeroStatus_67
	.type	uprv_decContextZeroStatus_67, @function
uprv_decContextZeroStatus_67:
.LFB99:
	.cfi_startproc
	endbr64
	movl	$0, 20(%rdi)
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE99:
	.size	uprv_decContextZeroStatus_67, .-uprv_decContextZeroStatus_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
