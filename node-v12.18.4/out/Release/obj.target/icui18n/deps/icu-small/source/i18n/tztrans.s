	.file	"tztrans.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718TimeZoneTransition17getDynamicClassIDEv
	.type	_ZNK6icu_6718TimeZoneTransition17getDynamicClassIDEv, @function
_ZNK6icu_6718TimeZoneTransition17getDynamicClassIDEv:
.LFB1301:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6718TimeZoneTransition16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE1301:
	.size	_ZNK6icu_6718TimeZoneTransition17getDynamicClassIDEv, .-_ZNK6icu_6718TimeZoneTransition17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TimeZoneTransitionD2Ev
	.type	_ZN6icu_6718TimeZoneTransitionD2Ev, @function
_ZN6icu_6718TimeZoneTransitionD2Ev:
.LFB1312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718TimeZoneTransitionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4
	movq	(%rdi), %rax
	call	*8(%rax)
.L4:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5
	movq	(%rdi), %rax
	call	*8(%rax)
.L5:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE1312:
	.size	_ZN6icu_6718TimeZoneTransitionD2Ev, .-_ZN6icu_6718TimeZoneTransitionD2Ev
	.globl	_ZN6icu_6718TimeZoneTransitionD1Ev
	.set	_ZN6icu_6718TimeZoneTransitionD1Ev,_ZN6icu_6718TimeZoneTransitionD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TimeZoneTransitionD0Ev
	.type	_ZN6icu_6718TimeZoneTransitionD0Ev, @function
_ZN6icu_6718TimeZoneTransitionD0Ev:
.LFB1314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718TimeZoneTransitionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L14
	movq	(%rdi), %rax
	call	*8(%rax)
.L14:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L15
	movq	(%rdi), %rax
	call	*8(%rax)
.L15:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE1314:
	.size	_ZN6icu_6718TimeZoneTransitionD0Ev, .-_ZN6icu_6718TimeZoneTransitionD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TimeZoneTransition16getStaticClassIDEv
	.type	_ZN6icu_6718TimeZoneTransition16getStaticClassIDEv, @function
_ZN6icu_6718TimeZoneTransition16getStaticClassIDEv:
.LFB1300:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6718TimeZoneTransition16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE1300:
	.size	_ZN6icu_6718TimeZoneTransition16getStaticClassIDEv, .-_ZN6icu_6718TimeZoneTransition16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TimeZoneTransitionC2EdRKNS_12TimeZoneRuleES3_
	.type	_ZN6icu_6718TimeZoneTransitionC2EdRKNS_12TimeZoneRuleES3_, @function
_ZN6icu_6718TimeZoneTransitionC2EdRKNS_12TimeZoneRuleES3_:
.LFB1303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718TimeZoneTransitionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rax, (%rbx)
	movq	(%rsi), %rax
	movsd	%xmm0, 8(%rbx)
	call	*24(%rax)
	movq	%r12, %rdi
	movq	%rax, 16(%rbx)
	movq	(%r12), %rax
	call	*24(%rax)
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1303:
	.size	_ZN6icu_6718TimeZoneTransitionC2EdRKNS_12TimeZoneRuleES3_, .-_ZN6icu_6718TimeZoneTransitionC2EdRKNS_12TimeZoneRuleES3_
	.globl	_ZN6icu_6718TimeZoneTransitionC1EdRKNS_12TimeZoneRuleES3_
	.set	_ZN6icu_6718TimeZoneTransitionC1EdRKNS_12TimeZoneRuleES3_,_ZN6icu_6718TimeZoneTransitionC2EdRKNS_12TimeZoneRuleES3_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TimeZoneTransitionC2Ev
	.type	_ZN6icu_6718TimeZoneTransitionC2Ev, @function
_ZN6icu_6718TimeZoneTransitionC2Ev:
.LFB1306:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6718TimeZoneTransitionE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0x000000000, 8(%rdi)
	movq	%rax, (%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE1306:
	.size	_ZN6icu_6718TimeZoneTransitionC2Ev, .-_ZN6icu_6718TimeZoneTransitionC2Ev
	.globl	_ZN6icu_6718TimeZoneTransitionC1Ev
	.set	_ZN6icu_6718TimeZoneTransitionC1Ev,_ZN6icu_6718TimeZoneTransitionC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TimeZoneTransitionC2ERKS0_
	.type	_ZN6icu_6718TimeZoneTransitionC2ERKS0_, @function
_ZN6icu_6718TimeZoneTransitionC2ERKS0_:
.LFB1309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsd	8(%rsi), %xmm0
	leaq	16+_ZTVN6icu_6718TimeZoneTransitionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movsd	%xmm0, 8(%rdi)
	pxor	%xmm0, %xmm0
	movq	%rax, (%rdi)
	movups	%xmm0, 16(%rdi)
	movq	16(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L28
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 16(%rbx)
.L28:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L27
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 24(%rbx)
.L27:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1309:
	.size	_ZN6icu_6718TimeZoneTransitionC2ERKS0_, .-_ZN6icu_6718TimeZoneTransitionC2ERKS0_
	.globl	_ZN6icu_6718TimeZoneTransitionC1ERKS0_
	.set	_ZN6icu_6718TimeZoneTransitionC1ERKS0_,_ZN6icu_6718TimeZoneTransitionC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718TimeZoneTransition5cloneEv
	.type	_ZNK6icu_6718TimeZoneTransition5cloneEv, @function
_ZNK6icu_6718TimeZoneTransition5cloneEv:
.LFB1315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L37
	movsd	8(%rbx), %xmm0
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN6icu_6718TimeZoneTransitionE(%rip), %rax
	movq	%rax, (%r12)
	movsd	%xmm0, 8(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%r12)
	testq	%rdi, %rdi
	je	.L40
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 16(%r12)
.L40:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L37
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 24(%r12)
.L37:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1315:
	.size	_ZNK6icu_6718TimeZoneTransition5cloneEv, .-_ZNK6icu_6718TimeZoneTransition5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TimeZoneTransitionaSERKS0_
	.type	_ZN6icu_6718TimeZoneTransitionaSERKS0_, @function
_ZN6icu_6718TimeZoneTransitionaSERKS0_:
.LFB1316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L50
	movsd	8(%rsi), %xmm0
	movq	16(%rsi), %r13
	movq	%rsi, %rbx
	movsd	%xmm0, 8(%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L51
	movq	(%rdi), %rax
	call	*8(%rax)
.L51:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	24(%r12), %rdi
	movq	24(%rbx), %r13
	movq	%rax, 16(%r12)
	testq	%rdi, %rdi
	je	.L52
	movq	(%rdi), %rax
	call	*8(%rax)
.L52:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%rax, 24(%r12)
.L50:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1316:
	.size	_ZN6icu_6718TimeZoneTransitionaSERKS0_, .-_ZN6icu_6718TimeZoneTransitionaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718TimeZoneTransitioneqERKS0_
	.type	_ZNK6icu_6718TimeZoneTransitioneqERKS0_, @function
_ZNK6icu_6718TimeZoneTransitioneqERKS0_:
.LFB1317:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L69
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L62
	cmpb	$42, (%rdi)
	je	.L64
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L64
.L62:
	movsd	8(%rbx), %xmm0
	ucomisd	8(%r12), %xmm0
	jp	.L64
	jne	.L64
	movq	16(%rbx), %rdi
	movq	16(%r12), %rsi
	testq	%rdi, %rdi
	je	.L81
	testq	%rsi, %rsi
	je	.L64
	movq	(%rdi), %rax
	call	*32(%rax)
	testb	%al, %al
	je	.L64
.L68:
	movq	24(%rbx), %rdi
	movq	24(%r12), %rsi
	testq	%rdi, %rdi
	je	.L82
	testq	%rsi, %rsi
	je	.L64
	movq	(%rdi), %rax
	call	*32(%rax)
	testb	%al, %al
	setne	%al
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L81:
	testq	%rsi, %rsi
	je	.L68
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%eax, %eax
.L60:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	testq	%rsi, %rsi
	sete	%al
	jmp	.L60
	.cfi_endproc
.LFE1317:
	.size	_ZNK6icu_6718TimeZoneTransitioneqERKS0_, .-_ZNK6icu_6718TimeZoneTransitioneqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718TimeZoneTransitionneERKS0_
	.type	_ZNK6icu_6718TimeZoneTransitionneERKS0_, @function
_ZNK6icu_6718TimeZoneTransitionneERKS0_:
.LFB1318:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L92
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L85
	cmpb	$42, (%rdi)
	je	.L87
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L87
.L85:
	movsd	8(%rbx), %xmm0
	ucomisd	8(%r12), %xmm0
	jp	.L87
	jne	.L87
	movq	16(%rbx), %rdi
	movq	16(%r12), %rsi
	testq	%rdi, %rdi
	je	.L104
	testq	%rsi, %rsi
	je	.L87
	movq	(%rdi), %rax
	call	*32(%rax)
	testb	%al, %al
	je	.L87
.L91:
	movq	24(%rbx), %rdi
	movq	24(%r12), %rsi
	testq	%rdi, %rdi
	je	.L105
	testq	%rsi, %rsi
	je	.L87
	movq	(%rdi), %rax
	call	*32(%rax)
	testb	%al, %al
	sete	%al
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L104:
	testq	%rsi, %rsi
	je	.L91
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$1, %eax
.L83:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	testq	%rsi, %rsi
	setne	%al
	jmp	.L83
	.cfi_endproc
.LFE1318:
	.size	_ZNK6icu_6718TimeZoneTransitionneERKS0_, .-_ZNK6icu_6718TimeZoneTransitionneERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TimeZoneTransition7setTimeEd
	.type	_ZN6icu_6718TimeZoneTransition7setTimeEd, @function
_ZN6icu_6718TimeZoneTransition7setTimeEd:
.LFB1319:
	.cfi_startproc
	endbr64
	movsd	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE1319:
	.size	_ZN6icu_6718TimeZoneTransition7setTimeEd, .-_ZN6icu_6718TimeZoneTransition7setTimeEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TimeZoneTransition7setFromERKNS_12TimeZoneRuleE
	.type	_ZN6icu_6718TimeZoneTransition7setFromERKNS_12TimeZoneRuleE, @function
_ZN6icu_6718TimeZoneTransition7setFromERKNS_12TimeZoneRuleE:
.LFB1320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L108
	movq	(%rdi), %rax
	call	*8(%rax)
.L108:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	%rax, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1320:
	.size	_ZN6icu_6718TimeZoneTransition7setFromERKNS_12TimeZoneRuleE, .-_ZN6icu_6718TimeZoneTransition7setFromERKNS_12TimeZoneRuleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TimeZoneTransition9adoptFromEPNS_12TimeZoneRuleE
	.type	_ZN6icu_6718TimeZoneTransition9adoptFromEPNS_12TimeZoneRuleE, @function
_ZN6icu_6718TimeZoneTransition9adoptFromEPNS_12TimeZoneRuleE:
.LFB1321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L114
	movq	(%rdi), %rax
	call	*8(%rax)
.L114:
	movq	%r12, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1321:
	.size	_ZN6icu_6718TimeZoneTransition9adoptFromEPNS_12TimeZoneRuleE, .-_ZN6icu_6718TimeZoneTransition9adoptFromEPNS_12TimeZoneRuleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TimeZoneTransition5setToERKNS_12TimeZoneRuleE
	.type	_ZN6icu_6718TimeZoneTransition5setToERKNS_12TimeZoneRuleE, @function
_ZN6icu_6718TimeZoneTransition5setToERKNS_12TimeZoneRuleE:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L120
	movq	(%rdi), %rax
	call	*8(%rax)
.L120:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1322:
	.size	_ZN6icu_6718TimeZoneTransition5setToERKNS_12TimeZoneRuleE, .-_ZN6icu_6718TimeZoneTransition5setToERKNS_12TimeZoneRuleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718TimeZoneTransition7adoptToEPNS_12TimeZoneRuleE
	.type	_ZN6icu_6718TimeZoneTransition7adoptToEPNS_12TimeZoneRuleE, @function
_ZN6icu_6718TimeZoneTransition7adoptToEPNS_12TimeZoneRuleE:
.LFB1323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L126
	movq	(%rdi), %rax
	call	*8(%rax)
.L126:
	movq	%r12, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1323:
	.size	_ZN6icu_6718TimeZoneTransition7adoptToEPNS_12TimeZoneRuleE, .-_ZN6icu_6718TimeZoneTransition7adoptToEPNS_12TimeZoneRuleE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718TimeZoneTransition7getTimeEv
	.type	_ZNK6icu_6718TimeZoneTransition7getTimeEv, @function
_ZNK6icu_6718TimeZoneTransition7getTimeEv:
.LFB1324:
	.cfi_startproc
	endbr64
	movsd	8(%rdi), %xmm0
	ret
	.cfi_endproc
.LFE1324:
	.size	_ZNK6icu_6718TimeZoneTransition7getTimeEv, .-_ZNK6icu_6718TimeZoneTransition7getTimeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718TimeZoneTransition5getToEv
	.type	_ZNK6icu_6718TimeZoneTransition5getToEv, @function
_ZNK6icu_6718TimeZoneTransition5getToEv:
.LFB1325:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE1325:
	.size	_ZNK6icu_6718TimeZoneTransition5getToEv, .-_ZNK6icu_6718TimeZoneTransition5getToEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718TimeZoneTransition7getFromEv
	.type	_ZNK6icu_6718TimeZoneTransition7getFromEv, @function
_ZNK6icu_6718TimeZoneTransition7getFromEv:
.LFB1326:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1326:
	.size	_ZNK6icu_6718TimeZoneTransition7getFromEv, .-_ZNK6icu_6718TimeZoneTransition7getFromEv
	.weak	_ZTSN6icu_6718TimeZoneTransitionE
	.section	.rodata._ZTSN6icu_6718TimeZoneTransitionE,"aG",@progbits,_ZTSN6icu_6718TimeZoneTransitionE,comdat
	.align 16
	.type	_ZTSN6icu_6718TimeZoneTransitionE, @object
	.size	_ZTSN6icu_6718TimeZoneTransitionE, 30
_ZTSN6icu_6718TimeZoneTransitionE:
	.string	"N6icu_6718TimeZoneTransitionE"
	.weak	_ZTIN6icu_6718TimeZoneTransitionE
	.section	.data.rel.ro._ZTIN6icu_6718TimeZoneTransitionE,"awG",@progbits,_ZTIN6icu_6718TimeZoneTransitionE,comdat
	.align 8
	.type	_ZTIN6icu_6718TimeZoneTransitionE, @object
	.size	_ZTIN6icu_6718TimeZoneTransitionE, 24
_ZTIN6icu_6718TimeZoneTransitionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718TimeZoneTransitionE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6718TimeZoneTransitionE
	.section	.data.rel.ro.local._ZTVN6icu_6718TimeZoneTransitionE,"awG",@progbits,_ZTVN6icu_6718TimeZoneTransitionE,comdat
	.align 8
	.type	_ZTVN6icu_6718TimeZoneTransitionE, @object
	.size	_ZTVN6icu_6718TimeZoneTransitionE, 40
_ZTVN6icu_6718TimeZoneTransitionE:
	.quad	0
	.quad	_ZTIN6icu_6718TimeZoneTransitionE
	.quad	_ZN6icu_6718TimeZoneTransitionD1Ev
	.quad	_ZN6icu_6718TimeZoneTransitionD0Ev
	.quad	_ZNK6icu_6718TimeZoneTransition17getDynamicClassIDEv
	.local	_ZZN6icu_6718TimeZoneTransition16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6718TimeZoneTransition16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
