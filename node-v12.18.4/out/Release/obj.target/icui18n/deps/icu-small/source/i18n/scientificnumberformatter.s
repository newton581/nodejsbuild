	.file	"scientificnumberformatter.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725ScientificNumberFormatter16SuperscriptStyle5cloneEv
	.type	_ZNK6icu_6725ScientificNumberFormatter16SuperscriptStyle5cloneEv, @function
_ZNK6icu_6725ScientificNumberFormatter16SuperscriptStyle5cloneEv:
.LFB2437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatter16SuperscriptStyleE(%rip), %rdx
	movq	%rdx, (%rax)
.L1:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2437:
	.size	_ZNK6icu_6725ScientificNumberFormatter16SuperscriptStyle5cloneEv, .-_ZNK6icu_6725ScientificNumberFormatter16SuperscriptStyle5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725ScientificNumberFormatter11MarkupStyle5cloneEv
	.type	_ZNK6icu_6725ScientificNumberFormatter11MarkupStyle5cloneEv, @function
_ZNK6icu_6725ScientificNumberFormatter11MarkupStyle5cloneEv:
.LFB2445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$136, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L8
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatter11MarkupStyleE(%rip), %rax
	leaq	8(%rbx), %rsi
	movq	%rax, (%r12)
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	72(%rbx), %rsi
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L8:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2445:
	.size	_ZNK6icu_6725ScientificNumberFormatter11MarkupStyle5cloneEv, .-_ZNK6icu_6725ScientificNumberFormatter11MarkupStyle5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ScientificNumberFormatterD2Ev
	.type	_ZN6icu_6725ScientificNumberFormatterD2Ev, @function
_ZN6icu_6725ScientificNumberFormatterD2Ev:
.LFB2457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L15
	movq	(%rdi), %rax
	call	*8(%rax)
.L15:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L16
	movq	(%rdi), %rax
	call	*8(%rax)
.L16:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2457:
	.size	_ZN6icu_6725ScientificNumberFormatterD2Ev, .-_ZN6icu_6725ScientificNumberFormatterD2Ev
	.globl	_ZN6icu_6725ScientificNumberFormatterD1Ev
	.set	_ZN6icu_6725ScientificNumberFormatterD1Ev,_ZN6icu_6725ScientificNumberFormatterD2Ev
	.section	.text._ZN6icu_6725ScientificNumberFormatter11MarkupStyleD2Ev,"axG",@progbits,_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD2Ev
	.type	_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD2Ev, @function
_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD2Ev:
.LFB3202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatter11MarkupStyleE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -72(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatter5StyleE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3202:
	.size	_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD2Ev, .-_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD2Ev
	.weak	_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD1Ev
	.set	_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD1Ev,_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD2Ev
	.section	.text._ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD2Ev,"axG",@progbits,_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD2Ev
	.type	_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD2Ev, @function
_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD2Ev:
.LFB3206:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatter5StyleE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3206:
	.size	_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD2Ev, .-_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD2Ev
	.weak	_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD1Ev
	.set	_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD1Ev,_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD2Ev
	.section	.text._ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD0Ev,"axG",@progbits,_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD0Ev
	.type	_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD0Ev, @function
_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD0Ev:
.LFB3208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatter5StyleE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3208:
	.size	_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD0Ev, .-_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725ScientificNumberFormatter11MarkupStyle6formatERKNS_13UnicodeStringERNS_21FieldPositionIteratorES4_RS2_R10UErrorCode
	.type	_ZNK6icu_6725ScientificNumberFormatter11MarkupStyle6formatERKNS_13UnicodeStringERNS_21FieldPositionIteratorES4_RS2_R10UErrorCode, @function
_ZNK6icu_6725ScientificNumberFormatter11MarkupStyle6formatERKNS_13UnicodeStringERNS_21FieldPositionIteratorES4_RS2_R10UErrorCode:
.LFB2449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L30
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	%rsi, %r15
	movq	%rdx, %rbx
	xorl	%r12d, %r12d
	movq	%rax, -80(%rbp)
	movl	$4294967295, %eax
	leaq	-80(%rbp), %r14
	movq	%rax, -72(%rbp)
	leaq	8(%rdi), %rax
	movl	$0, -64(%rbp)
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6721FieldPositionIterator4nextERNS_13FieldPositionE@PLT
	testb	%al, %al
	je	.L32
.L47:
	movl	-72(%rbp), %eax
	cmpl	$3, %eax
	je	.L33
	cmpl	$5, %eax
	jne	.L35
	movl	-64(%rbp), %ecx
	movl	%r12d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	subl	%r12d, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-88(%rbp), %rax
	movl	-64(%rbp), %r12d
	movswl	80(%rax), %ecx
	leaq	72(%rax), %rsi
	testw	%cx, %cx
	js	.L41
	sarl	$5, %ecx
.L42:
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6721FieldPositionIterator4nextERNS_13FieldPositionE@PLT
	testb	%al, %al
	jne	.L47
.L32:
	movzwl	8(%r15), %eax
	testw	%ax, %ax
	js	.L43
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L44:
	movq	%r13, %rdi
	subl	%r12d, %ecx
	movl	%r12d, %edx
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
.L30:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L48
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movl	-68(%rbp), %ecx
	movl	%r12d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	subl	%r12d, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-96(%rbp), %rax
	movl	-64(%rbp), %r12d
	movswl	8(%rax), %ecx
	testw	%cx, %cx
	js	.L36
	sarl	$5, %ecx
	movq	%rax, %rsi
.L37:
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-88(%rbp), %rax
	movswl	16(%rax), %ecx
	testw	%cx, %cx
	js	.L38
	sarl	$5, %ecx
.L39:
	movq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L41:
	movl	84(%rax), %ecx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L38:
	movl	20(%rax), %ecx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L36:
	movl	12(%rax), %ecx
	movq	%rax, %rsi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L43:
	movl	12(%r15), %ecx
	jmp	.L44
.L48:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2449:
	.size	_ZNK6icu_6725ScientificNumberFormatter11MarkupStyle6formatERKNS_13UnicodeStringERNS_21FieldPositionIteratorES4_RS2_R10UErrorCode, .-_ZNK6icu_6725ScientificNumberFormatter11MarkupStyle6formatERKNS_13UnicodeStringERNS_21FieldPositionIteratorES4_RS2_R10UErrorCode
	.section	.text._ZN6icu_6725ScientificNumberFormatter11MarkupStyleD0Ev,"axG",@progbits,_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD0Ev
	.type	_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD0Ev, @function
_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD0Ev:
.LFB3204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatter11MarkupStyleE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -72(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatter5StyleE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3204:
	.size	_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD0Ev, .-_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ScientificNumberFormatterD0Ev
	.type	_ZN6icu_6725ScientificNumberFormatterD0Ev, @function
_ZN6icu_6725ScientificNumberFormatterD0Ev:
.LFB2459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L52
	movq	(%rdi), %rax
	call	*8(%rax)
.L52:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L53
	movq	(%rdi), %rax
	call	*8(%rax)
.L53:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2459:
	.size	_ZN6icu_6725ScientificNumberFormatterD0Ev, .-_ZN6icu_6725ScientificNumberFormatterD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ScientificNumberFormatter25createSuperscriptInstanceEPNS_13DecimalFormatER10UErrorCode
	.type	_ZN6icu_6725ScientificNumberFormatter25createSuperscriptInstanceEPNS_13DecimalFormatER10UErrorCode, @function
_ZN6icu_6725ScientificNumberFormatter25createSuperscriptInstanceEPNS_13DecimalFormatER10UErrorCode:
.LFB2425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$8, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L62
	movl	(%rbx), %esi
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatter16SuperscriptStyleE(%rip), %rax
	movq	%rax, (%r15)
	testl	%esi, %esi
	jg	.L100
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L101
.L81:
	movq	.LC0(%rip), %xmm0
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r15, %xmm2
	movl	(%rbx), %ecx
	movq	%rax, %xmm1
	movl	$2, %edx
	leaq	8(%r12), %r14
	punpcklqdq	%xmm1, %xmm0
	movw	%dx, 16(%r12)
	movups	%xmm0, (%r12)
	movq	%r13, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 72(%r12)
	testl	%ecx, %ecx
	jg	.L65
	testq	%r15, %r15
	je	.L68
	testq	%r13, %r13
	je	.L68
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*312(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L68
	movswl	1744(%rax), %ecx
	leaq	1736(%rax), %rsi
	testw	%cx, %cx
	js	.L69
	sarl	$5, %ecx
.L70:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	1168(%r13), %ecx
	leaq	1160(%r13), %rsi
	testw	%cx, %cx
	js	.L71
	sarl	$5, %ecx
.L72:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	272(%r13), %ecx
	leaq	264(%r13), %rsi
	testw	%cx, %cx
	js	.L73
	sarl	$5, %ecx
.L74:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L61
	.p2align 4,,10
	.p2align 3
.L65:
	movq	(%r12), %rax
	leaq	_ZN6icu_6725ScientificNumberFormatterD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L76
	movq	72(%r12), %rdi
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatterE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L77
	movq	(%rdi), %rax
	call	*8(%rax)
.L77:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L78
	movq	(%rdi), %rax
	call	*8(%rax)
.L78:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L100:
	leaq	_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD0Ev(%rip), %rax
.L63:
	movq	%r15, %rdi
	call	*%rax
.L79:
	testq	%r13, %r13
	je	.L85
	movq	0(%r13), %rax
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	*8(%rax)
.L61:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	$1, (%rbx)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L73:
	movl	276(%r13), %ecx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L71:
	movl	1172(%r13), %ecx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L69:
	movl	1748(%rax), %ecx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*%rax
	jmp	.L61
.L62:
	cmpl	$0, (%rbx)
	jg	.L79
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L81
	movl	$7, (%rbx)
	jmp	.L79
.L85:
	xorl	%r12d, %r12d
	jmp	.L61
.L101:
	movq	(%r15), %rax
	movl	$7, (%rbx)
	movq	8(%rax), %rax
	jmp	.L63
	.cfi_endproc
.LFE2425:
	.size	_ZN6icu_6725ScientificNumberFormatter25createSuperscriptInstanceEPNS_13DecimalFormatER10UErrorCode, .-_ZN6icu_6725ScientificNumberFormatter25createSuperscriptInstanceEPNS_13DecimalFormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ScientificNumberFormatter14createInstanceEPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode
	.type	_ZN6icu_6725ScientificNumberFormatter14createInstanceEPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode, @function
_ZN6icu_6725ScientificNumberFormatter14createInstanceEPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode:
.LFB2432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %esi
	testl	%esi, %esi
	jg	.L103
	movl	$88, %edi
	movq	%rdx, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L104
	leaq	8(%rax), %r15
	movq	%r14, %xmm2
	movl	(%rbx), %ecx
	movl	$2, %edx
	movq	.LC0(%rip), %xmm0
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, 16(%r12)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	movq	%r13, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 72(%r12)
	testl	%ecx, %ecx
	jg	.L105
	testq	%r13, %r13
	je	.L108
	testq	%r14, %r14
	je	.L108
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*312(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L108
	movswl	1744(%rax), %ecx
	leaq	1736(%rax), %rsi
	testw	%cx, %cx
	js	.L109
	sarl	$5, %ecx
.L110:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	1168(%r13), %ecx
	leaq	1160(%r13), %rsi
	testw	%cx, %cx
	js	.L111
	sarl	$5, %ecx
.L112:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	272(%r13), %ecx
	leaq	264(%r13), %rsi
	testw	%cx, %cx
	js	.L113
	sarl	$5, %ecx
.L114:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L102
	.p2align 4,,10
	.p2align 3
.L105:
	movq	(%r12), %rax
	leaq	_ZN6icu_6725ScientificNumberFormatterD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L116
	movq	72(%r12), %rdi
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatterE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L117
	movq	(%rdi), %rax
	call	*8(%rax)
.L117:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L118
	movq	(%rdi), %rax
	call	*8(%rax)
.L118:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L102
.L104:
	movl	$7, (%rbx)
	.p2align 4,,10
	.p2align 3
.L103:
	testq	%r14, %r14
	je	.L119
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L119:
	testq	%r13, %r13
	je	.L120
	movq	0(%r13), %rax
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	*8(%rax)
.L102:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movl	$1, (%rbx)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L113:
	movl	276(%r13), %ecx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L111:
	movl	1172(%r13), %ecx
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L109:
	movl	1748(%rax), %ecx
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*%rax
	jmp	.L102
.L120:
	xorl	%r12d, %r12d
	jmp	.L102
	.cfi_endproc
.LFE2432:
	.size	_ZN6icu_6725ScientificNumberFormatter14createInstanceEPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode, .-_ZN6icu_6725ScientificNumberFormatter14createInstanceEPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ScientificNumberFormatter25createSuperscriptInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6725ScientificNumberFormatter25createSuperscriptInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6725ScientificNumberFormatter25createSuperscriptInstanceERKNS_6LocaleER10UErrorCode:
.LFB2429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movl	$8, %edi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L135
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatter16SuperscriptStyleE(%rip), %rax
	movq	%rax, (%r12)
.L135:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712NumberFormat24createScientificInstanceERKNS_6LocaleER10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6725ScientificNumberFormatter14createInstanceEPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode
	.cfi_endproc
.LFE2429:
	.size	_ZN6icu_6725ScientificNumberFormatter25createSuperscriptInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6725ScientificNumberFormatter25createSuperscriptInstanceERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ScientificNumberFormatter20createMarkupInstanceEPNS_13DecimalFormatERKNS_13UnicodeStringES5_R10UErrorCode
	.type	_ZN6icu_6725ScientificNumberFormatter20createMarkupInstanceEPNS_13DecimalFormatERKNS_13UnicodeStringES5_R10UErrorCode, @function
_ZN6icu_6725ScientificNumberFormatter20createMarkupInstanceEPNS_13DecimalFormatERKNS_13UnicodeStringES5_R10UErrorCode:
.LFB2430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$136, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L141
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatter11MarkupStyleE(%rip), %rax
	leaq	8(%r12), %rdi
	movq	%rbx, %rsi
	movq	%rax, (%r12)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	72(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L141:
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6725ScientificNumberFormatter14createInstanceEPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode
	.cfi_endproc
.LFE2430:
	.size	_ZN6icu_6725ScientificNumberFormatter20createMarkupInstanceEPNS_13DecimalFormatERKNS_13UnicodeStringES5_R10UErrorCode, .-_ZN6icu_6725ScientificNumberFormatter20createMarkupInstanceEPNS_13DecimalFormatERKNS_13UnicodeStringES5_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ScientificNumberFormatter20createMarkupInstanceERKNS_6LocaleERKNS_13UnicodeStringES6_R10UErrorCode
	.type	_ZN6icu_6725ScientificNumberFormatter20createMarkupInstanceERKNS_6LocaleERKNS_13UnicodeStringES6_R10UErrorCode, @function
_ZN6icu_6725ScientificNumberFormatter20createMarkupInstanceERKNS_6LocaleERKNS_13UnicodeStringES6_R10UErrorCode:
.LFB2431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$136, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L147
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatter11MarkupStyleE(%rip), %rax
	leaq	8(%r12), %rdi
	movq	%rbx, %rsi
	movq	%rax, (%r12)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	72(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L147:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712NumberFormat24createScientificInstanceERKNS_6LocaleER10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%rbx
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6725ScientificNumberFormatter14createInstanceEPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode
	.cfi_endproc
.LFE2431:
	.size	_ZN6icu_6725ScientificNumberFormatter20createMarkupInstanceERKNS_6LocaleERKNS_13UnicodeStringES6_R10UErrorCode, .-_ZN6icu_6725ScientificNumberFormatter20createMarkupInstanceERKNS_6LocaleERKNS_13UnicodeStringES6_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ScientificNumberFormatterC2EPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode
	.type	_ZN6icu_6725ScientificNumberFormatterC2EPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode, @function
_ZN6icu_6725ScientificNumberFormatterC2EPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode:
.LFB2451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, %xmm1
	movl	$2, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, -40(%rbp)
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatterE(%rip), %rdx
	movq	%rdx, %xmm0
	movl	(%rcx), %edx
	movw	%ax, 16(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	movq	%rsi, %xmm0
	movhps	-40(%rbp), %xmm0
	movups	%xmm0, 72(%rdi)
	testl	%edx, %edx
	jg	.L152
	movq	%rcx, %r12
	testq	%rsi, %rsi
	je	.L156
	cmpq	$0, -40(%rbp)
	je	.L156
	movq	(%rsi), %rax
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	*312(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L156
	movswl	1744(%rax), %ecx
	leaq	1736(%rax), %rsi
	testw	%cx, %cx
	js	.L157
	sarl	$5, %ecx
.L158:
	leaq	8(%rbx), %r12
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	1168(%r13), %ecx
	leaq	1160(%r13), %rsi
	testw	%cx, %cx
	js	.L159
	sarl	$5, %ecx
.L160:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	272(%r13), %ecx
	leaq	264(%r13), %rsi
	testw	%cx, %cx
	js	.L161
	sarl	$5, %ecx
.L162:
	addq	$24, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movl	$1, (%r12)
.L152:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movl	1748(%rax), %ecx
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L161:
	movl	276(%r13), %ecx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L159:
	movl	1172(%r13), %ecx
	jmp	.L160
	.cfi_endproc
.LFE2451:
	.size	_ZN6icu_6725ScientificNumberFormatterC2EPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode, .-_ZN6icu_6725ScientificNumberFormatterC2EPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode
	.globl	_ZN6icu_6725ScientificNumberFormatterC1EPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode
	.set	_ZN6icu_6725ScientificNumberFormatterC1EPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode,_ZN6icu_6725ScientificNumberFormatterC2EPNS_13DecimalFormatEPNS0_5StyleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ScientificNumberFormatterC2ERKS0_
	.type	_ZN6icu_6725ScientificNumberFormatterC2ERKS0_, @function
_ZN6icu_6725ScientificNumberFormatterC2ERKS0_:
.LFB2454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725ScientificNumberFormatterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	8(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	72(%r12), %rdi
	pxor	%xmm0, %xmm0
	movups	%xmm0, 72(%rbx)
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	80(%r12), %rdi
	movq	%rax, 72(%rbx)
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 80(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2454:
	.size	_ZN6icu_6725ScientificNumberFormatterC2ERKS0_, .-_ZN6icu_6725ScientificNumberFormatterC2ERKS0_
	.globl	_ZN6icu_6725ScientificNumberFormatterC1ERKS0_
	.set	_ZN6icu_6725ScientificNumberFormatterC1ERKS0_,_ZN6icu_6725ScientificNumberFormatterC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ScientificNumberFormatter14getPreExponentERKNS_20DecimalFormatSymbolsERNS_13UnicodeStringE
	.type	_ZN6icu_6725ScientificNumberFormatter14getPreExponentERKNS_20DecimalFormatSymbolsERNS_13UnicodeStringE, @function
_ZN6icu_6725ScientificNumberFormatter14getPreExponentERKNS_20DecimalFormatSymbolsERNS_13UnicodeStringE:
.LFB2461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	1736(%rdi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movswl	1744(%rdi), %ecx
	movq	%rdi, %rbx
	testw	%cx, %cx
	js	.L170
	sarl	$5, %ecx
.L171:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	1168(%rbx), %ecx
	leaq	1160(%rbx), %rsi
	testw	%cx, %cx
	js	.L172
	sarl	$5, %ecx
.L173:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	272(%rbx), %ecx
	leaq	264(%rbx), %rsi
	testw	%cx, %cx
	js	.L174
	popq	%rbx
	movq	%r12, %rdi
	sarl	$5, %ecx
	popq	%r12
	xorl	%edx, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movl	1748(%rdi), %ecx
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L174:
	movl	276(%rbx), %ecx
	movq	%r12, %rdi
	popq	%rbx
	xorl	%edx, %edx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movl	1172(%rbx), %ecx
	jmp	.L173
	.cfi_endproc
.LFE2461:
	.size	_ZN6icu_6725ScientificNumberFormatter14getPreExponentERKNS_20DecimalFormatSymbolsERNS_13UnicodeStringE, .-_ZN6icu_6725ScientificNumberFormatter14getPreExponentERKNS_20DecimalFormatSymbolsERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725ScientificNumberFormatter16SuperscriptStyle6formatERKNS_13UnicodeStringERNS_21FieldPositionIteratorES4_RS2_R10UErrorCode
	.type	_ZNK6icu_6725ScientificNumberFormatter16SuperscriptStyle6formatERKNS_13UnicodeStringERNS_21FieldPositionIteratorES4_RS2_R10UErrorCode, @function
_ZNK6icu_6725ScientificNumberFormatter16SuperscriptStyle6formatERKNS_13UnicodeStringERNS_21FieldPositionIteratorES4_RS2_R10UErrorCode:
.LFB2444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -112(%rbp)
	movl	(%r9), %edi
	movq	%rcx, -120(%rbp)
	movq	%r9, -136(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testl	%edi, %edi
	jg	.L178
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movl	$0, -64(%rbp)
	movq	%rsi, %r14
	xorl	%r12d, %r12d
	movq	%rax, -80(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -72(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L183:
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %rdi
	call	_ZN6icu_6721FieldPositionIterator4nextERNS_13FieldPositionE@PLT
	testb	%al, %al
	je	.L180
	movl	-72(%rbp), %eax
	cmpl	$4, %eax
	je	.L181
	cmpl	$5, %eax
	je	.L182
	cmpl	$3, %eax
	jne	.L183
	movl	-68(%rbp), %ecx
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	subl	%r12d, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-120(%rbp), %rax
	movl	-64(%rbp), %r12d
	movswl	8(%rax), %ecx
	testw	%cx, %cx
	js	.L184
	sarl	$5, %ecx
	movq	%rax, %rsi
.L185:
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L182:
	movl	-68(%rbp), %ecx
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	subl	%r12d, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-136(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L189
	movl	-64(%rbp), %r12d
	movl	-68(%rbp), %r15d
	cmpl	%r15d, %r12d
	jle	.L183
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r14, %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	movl	%eax, %ebx
	call	u_charDigitValue_67@PLT
	testl	%eax, %eax
	js	.L188
	leaq	_ZN6icu_67L18kSuperscriptDigitsE(%rip), %rdx
	cltq
	leaq	-82(%rbp), %rsi
	movq	%r13, %rdi
	movzwl	(%rdx,%rax,2), %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movw	%ax, -82(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	$65535, %ebx
	ja	.L191
	addl	$1, %r15d
	cmpl	%r15d, %r12d
	jg	.L194
.L192:
	movl	-64(%rbp), %r12d
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L191:
	addl	$2, %r15d
	cmpl	%r15d, %r12d
	jg	.L194
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L181:
	movl	-68(%rbp), %r15d
	movl	-64(%rbp), %eax
	movq	%r14, %rdi
	movl	%r15d, %esi
	movl	%eax, -124(%rbp)
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	$11, %edi
	movl	%eax, %ebx
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L187
	movl	%r15d, %ecx
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	subl	%r12d, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$8315, %ecx
	movw	%cx, -82(%rbp)
.L203:
	leaq	-82(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-124(%rbp), %r12d
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$12, %edi
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L188
	movl	%r15d, %ecx
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	subl	%r12d, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$8314, %edx
	movw	%dx, -82(%rbp)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L184:
	movl	12(%rax), %ecx
	movq	%rax, %rsi
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L188:
	movq	-136(%rbp), %rax
	movl	$10, (%rax)
.L189:
	movq	-104(%rbp), %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
.L178:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movzwl	8(%r14), %eax
	testw	%ax, %ax
	js	.L195
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L196:
	subl	%r12d, %ecx
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L195:
	movl	12(%r14), %ecx
	jmp	.L196
.L204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2444:
	.size	_ZNK6icu_6725ScientificNumberFormatter16SuperscriptStyle6formatERKNS_13UnicodeStringERNS_21FieldPositionIteratorES4_RS2_R10UErrorCode, .-_ZNK6icu_6725ScientificNumberFormatter16SuperscriptStyle6formatERKNS_13UnicodeStringERNS_21FieldPositionIteratorES4_RS2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725ScientificNumberFormatter6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6725ScientificNumberFormatter6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6725ScientificNumberFormatter6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode:
.LFB2460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L206
	leaq	-160(%rbp), %r15
	movq	%rdi, %r14
	movq	%rcx, %rbx
	movq	%rsi, %r13
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	call	_ZN6icu_6721FieldPositionIteratorC1Ev@PLT
	movq	72(%r14), %rdi
	leaq	-128(%rbp), %r10
	movq	%rbx, %r8
	movq	%r10, %rdx
	movq	%r10, -168(%rbp)
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	80(%r14), %rdi
	movq	%r12, %r8
	leaq	8(%r14), %rcx
	movq	-168(%rbp), %r10
	movq	%rbx, %r9
	movq	%r15, %rdx
	movq	(%rdi), %rax
	movq	%r10, %rsi
	call	*32(%rax)
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6721FieldPositionIteratorD1Ev@PLT
	movq	-168(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L206:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L209
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L209:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2460:
	.size	_ZNK6icu_6725ScientificNumberFormatter6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6725ScientificNumberFormatter6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode
	.weak	_ZTSN6icu_6725ScientificNumberFormatter5StyleE
	.section	.rodata._ZTSN6icu_6725ScientificNumberFormatter5StyleE,"aG",@progbits,_ZTSN6icu_6725ScientificNumberFormatter5StyleE,comdat
	.align 32
	.type	_ZTSN6icu_6725ScientificNumberFormatter5StyleE, @object
	.size	_ZTSN6icu_6725ScientificNumberFormatter5StyleE, 43
_ZTSN6icu_6725ScientificNumberFormatter5StyleE:
	.string	"N6icu_6725ScientificNumberFormatter5StyleE"
	.weak	_ZTIN6icu_6725ScientificNumberFormatter5StyleE
	.section	.data.rel.ro._ZTIN6icu_6725ScientificNumberFormatter5StyleE,"awG",@progbits,_ZTIN6icu_6725ScientificNumberFormatter5StyleE,comdat
	.align 8
	.type	_ZTIN6icu_6725ScientificNumberFormatter5StyleE, @object
	.size	_ZTIN6icu_6725ScientificNumberFormatter5StyleE, 24
_ZTIN6icu_6725ScientificNumberFormatter5StyleE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725ScientificNumberFormatter5StyleE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6725ScientificNumberFormatter16SuperscriptStyleE
	.section	.rodata._ZTSN6icu_6725ScientificNumberFormatter16SuperscriptStyleE,"aG",@progbits,_ZTSN6icu_6725ScientificNumberFormatter16SuperscriptStyleE,comdat
	.align 32
	.type	_ZTSN6icu_6725ScientificNumberFormatter16SuperscriptStyleE, @object
	.size	_ZTSN6icu_6725ScientificNumberFormatter16SuperscriptStyleE, 55
_ZTSN6icu_6725ScientificNumberFormatter16SuperscriptStyleE:
	.string	"N6icu_6725ScientificNumberFormatter16SuperscriptStyleE"
	.weak	_ZTIN6icu_6725ScientificNumberFormatter16SuperscriptStyleE
	.section	.data.rel.ro._ZTIN6icu_6725ScientificNumberFormatter16SuperscriptStyleE,"awG",@progbits,_ZTIN6icu_6725ScientificNumberFormatter16SuperscriptStyleE,comdat
	.align 8
	.type	_ZTIN6icu_6725ScientificNumberFormatter16SuperscriptStyleE, @object
	.size	_ZTIN6icu_6725ScientificNumberFormatter16SuperscriptStyleE, 24
_ZTIN6icu_6725ScientificNumberFormatter16SuperscriptStyleE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725ScientificNumberFormatter16SuperscriptStyleE
	.quad	_ZTIN6icu_6725ScientificNumberFormatter5StyleE
	.weak	_ZTSN6icu_6725ScientificNumberFormatter11MarkupStyleE
	.section	.rodata._ZTSN6icu_6725ScientificNumberFormatter11MarkupStyleE,"aG",@progbits,_ZTSN6icu_6725ScientificNumberFormatter11MarkupStyleE,comdat
	.align 32
	.type	_ZTSN6icu_6725ScientificNumberFormatter11MarkupStyleE, @object
	.size	_ZTSN6icu_6725ScientificNumberFormatter11MarkupStyleE, 50
_ZTSN6icu_6725ScientificNumberFormatter11MarkupStyleE:
	.string	"N6icu_6725ScientificNumberFormatter11MarkupStyleE"
	.weak	_ZTIN6icu_6725ScientificNumberFormatter11MarkupStyleE
	.section	.data.rel.ro._ZTIN6icu_6725ScientificNumberFormatter11MarkupStyleE,"awG",@progbits,_ZTIN6icu_6725ScientificNumberFormatter11MarkupStyleE,comdat
	.align 8
	.type	_ZTIN6icu_6725ScientificNumberFormatter11MarkupStyleE, @object
	.size	_ZTIN6icu_6725ScientificNumberFormatter11MarkupStyleE, 24
_ZTIN6icu_6725ScientificNumberFormatter11MarkupStyleE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725ScientificNumberFormatter11MarkupStyleE
	.quad	_ZTIN6icu_6725ScientificNumberFormatter5StyleE
	.weak	_ZTSN6icu_6725ScientificNumberFormatterE
	.section	.rodata._ZTSN6icu_6725ScientificNumberFormatterE,"aG",@progbits,_ZTSN6icu_6725ScientificNumberFormatterE,comdat
	.align 32
	.type	_ZTSN6icu_6725ScientificNumberFormatterE, @object
	.size	_ZTSN6icu_6725ScientificNumberFormatterE, 37
_ZTSN6icu_6725ScientificNumberFormatterE:
	.string	"N6icu_6725ScientificNumberFormatterE"
	.weak	_ZTIN6icu_6725ScientificNumberFormatterE
	.section	.data.rel.ro._ZTIN6icu_6725ScientificNumberFormatterE,"awG",@progbits,_ZTIN6icu_6725ScientificNumberFormatterE,comdat
	.align 8
	.type	_ZTIN6icu_6725ScientificNumberFormatterE, @object
	.size	_ZTIN6icu_6725ScientificNumberFormatterE, 24
_ZTIN6icu_6725ScientificNumberFormatterE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725ScientificNumberFormatterE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6725ScientificNumberFormatter5StyleE
	.section	.data.rel.ro._ZTVN6icu_6725ScientificNumberFormatter5StyleE,"awG",@progbits,_ZTVN6icu_6725ScientificNumberFormatter5StyleE,comdat
	.align 8
	.type	_ZTVN6icu_6725ScientificNumberFormatter5StyleE, @object
	.size	_ZTVN6icu_6725ScientificNumberFormatter5StyleE, 56
_ZTVN6icu_6725ScientificNumberFormatter5StyleE:
	.quad	0
	.quad	_ZTIN6icu_6725ScientificNumberFormatter5StyleE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6725ScientificNumberFormatter16SuperscriptStyleE
	.section	.data.rel.ro._ZTVN6icu_6725ScientificNumberFormatter16SuperscriptStyleE,"awG",@progbits,_ZTVN6icu_6725ScientificNumberFormatter16SuperscriptStyleE,comdat
	.align 8
	.type	_ZTVN6icu_6725ScientificNumberFormatter16SuperscriptStyleE, @object
	.size	_ZTVN6icu_6725ScientificNumberFormatter16SuperscriptStyleE, 56
_ZTVN6icu_6725ScientificNumberFormatter16SuperscriptStyleE:
	.quad	0
	.quad	_ZTIN6icu_6725ScientificNumberFormatter16SuperscriptStyleE
	.quad	_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD1Ev
	.quad	_ZN6icu_6725ScientificNumberFormatter16SuperscriptStyleD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6725ScientificNumberFormatter16SuperscriptStyle5cloneEv
	.quad	_ZNK6icu_6725ScientificNumberFormatter16SuperscriptStyle6formatERKNS_13UnicodeStringERNS_21FieldPositionIteratorES4_RS2_R10UErrorCode
	.weak	_ZTVN6icu_6725ScientificNumberFormatter11MarkupStyleE
	.section	.data.rel.ro._ZTVN6icu_6725ScientificNumberFormatter11MarkupStyleE,"awG",@progbits,_ZTVN6icu_6725ScientificNumberFormatter11MarkupStyleE,comdat
	.align 8
	.type	_ZTVN6icu_6725ScientificNumberFormatter11MarkupStyleE, @object
	.size	_ZTVN6icu_6725ScientificNumberFormatter11MarkupStyleE, 56
_ZTVN6icu_6725ScientificNumberFormatter11MarkupStyleE:
	.quad	0
	.quad	_ZTIN6icu_6725ScientificNumberFormatter11MarkupStyleE
	.quad	_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD1Ev
	.quad	_ZN6icu_6725ScientificNumberFormatter11MarkupStyleD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6725ScientificNumberFormatter11MarkupStyle5cloneEv
	.quad	_ZNK6icu_6725ScientificNumberFormatter11MarkupStyle6formatERKNS_13UnicodeStringERNS_21FieldPositionIteratorES4_RS2_R10UErrorCode
	.weak	_ZTVN6icu_6725ScientificNumberFormatterE
	.section	.data.rel.ro._ZTVN6icu_6725ScientificNumberFormatterE,"awG",@progbits,_ZTVN6icu_6725ScientificNumberFormatterE,comdat
	.align 8
	.type	_ZTVN6icu_6725ScientificNumberFormatterE, @object
	.size	_ZTVN6icu_6725ScientificNumberFormatterE, 40
_ZTVN6icu_6725ScientificNumberFormatterE:
	.quad	0
	.quad	_ZTIN6icu_6725ScientificNumberFormatterE
	.quad	_ZN6icu_6725ScientificNumberFormatterD1Ev
	.quad	_ZN6icu_6725ScientificNumberFormatterD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.section	.rodata
	.align 16
	.type	_ZN6icu_67L18kSuperscriptDigitsE, @object
	.size	_ZN6icu_67L18kSuperscriptDigitsE, 20
_ZN6icu_67L18kSuperscriptDigitsE:
	.value	8304
	.value	185
	.value	178
	.value	179
	.value	8308
	.value	8309
	.value	8310
	.value	8311
	.value	8312
	.value	8313
	.section	.data.rel.ro.local,"aw"
	.align 8
.LC0:
	.quad	_ZTVN6icu_6725ScientificNumberFormatterE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
