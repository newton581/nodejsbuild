	.file	"utf16collationiterator.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722UTF16CollationIterator9getOffsetEv
	.type	_ZNK6icu_6722UTF16CollationIterator9getOffsetEv, @function
_ZNK6icu_6722UTF16CollationIterator9getOffsetEv:
.LFB3270:
	.cfi_startproc
	endbr64
	movq	400(%rdi), %rax
	subq	392(%rdi), %rax
	sarq	%rax
	ret
	.cfi_endproc
.LFE3270:
	.size	_ZNK6icu_6722UTF16CollationIterator9getOffsetEv, .-_ZNK6icu_6722UTF16CollationIterator9getOffsetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UTF16CollationIterator14handleNextCE32ERiR10UErrorCode
	.type	_ZN6icu_6722UTF16CollationIterator14handleNextCE32ERiR10UErrorCode, @function
_ZN6icu_6722UTF16CollationIterator14handleNextCE32ERiR10UErrorCode:
.LFB3271:
	.cfi_startproc
	endbr64
	movq	400(%rdi), %rax
	cmpq	408(%rdi), %rax
	je	.L6
	movq	8(%rdi), %rcx
	leaq	2(%rax), %rdx
	movzwl	(%rax), %eax
	movq	%rdx, 400(%rdi)
	movl	%eax, (%rsi)
	movl	%eax, %edx
	movq	(%rcx), %rsi
	sarl	$5, %eax
	cltq
	andl	$31, %edx
	movzwl	(%rsi,%rax,2), %eax
	leal	(%rdx,%rax,4), %eax
	movq	16(%rcx), %rdx
	cltq
	movl	(%rdx,%rax,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$-1, (%rsi)
	movl	$192, %eax
	ret
	.cfi_endproc
.LFE3271:
	.size	_ZN6icu_6722UTF16CollationIterator14handleNextCE32ERiR10UErrorCode, .-_ZN6icu_6722UTF16CollationIterator14handleNextCE32ERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UTF16CollationIterator23handleGetTrailSurrogateEv
	.type	_ZN6icu_6722UTF16CollationIterator23handleGetTrailSurrogateEv, @function
_ZN6icu_6722UTF16CollationIterator23handleGetTrailSurrogateEv:
.LFB3272:
	.cfi_startproc
	endbr64
	movq	400(%rdi), %rax
	cmpq	408(%rdi), %rax
	je	.L9
	movzwl	(%rax), %r8d
	movl	%r8d, %edx
	andl	$64512, %edx
	cmpl	$56320, %edx
	jne	.L7
	addq	$2, %rax
	movq	%rax, 400(%rdi)
.L7:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3272:
	.size	_ZN6icu_6722UTF16CollationIterator23handleGetTrailSurrogateEv, .-_ZN6icu_6722UTF16CollationIterator23handleGetTrailSurrogateEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UTF16CollationIterator13nextCodePointER10UErrorCode
	.type	_ZN6icu_6722UTF16CollationIterator13nextCodePointER10UErrorCode, @function
_ZN6icu_6722UTF16CollationIterator13nextCodePointER10UErrorCode:
.LFB3274:
	.cfi_startproc
	endbr64
	movq	400(%rdi), %rax
	movq	408(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L13
	movzwl	(%rax), %r8d
	movl	%r8d, %esi
	testl	%r8d, %r8d
	jne	.L12
	testq	%rdx, %rdx
	je	.L26
.L12:
	movl	%esi, %r9d
	leaq	2(%rax), %rcx
	andl	$-1024, %r9d
	movq	%rcx, 400(%rdi)
	cmpl	$55296, %r9d
	jne	.L10
	cmpq	%rcx, %rdx
	je	.L10
	movzwl	2(%rax), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L27
.L10:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	sall	$10, %esi
	addq	$4, %rax
	leal	-56613888(%rdx,%rsi), %r8d
	movq	%rax, 400(%rdi)
	movl	%r8d, %eax
	ret
.L26:
	movq	%rax, 408(%rdi)
	movl	$-1, %r8d
	jmp	.L10
.L13:
	movl	$-1, %r8d
	jmp	.L10
	.cfi_endproc
.LFE3274:
	.size	_ZN6icu_6722UTF16CollationIterator13nextCodePointER10UErrorCode, .-_ZN6icu_6722UTF16CollationIterator13nextCodePointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UTF16CollationIterator17previousCodePointER10UErrorCode
	.type	_ZN6icu_6722UTF16CollationIterator17previousCodePointER10UErrorCode, @function
_ZN6icu_6722UTF16CollationIterator17previousCodePointER10UErrorCode:
.LFB3275:
	.cfi_startproc
	endbr64
	movq	400(%rdi), %rax
	movq	392(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L30
	movzwl	-2(%rax), %r8d
	leaq	-2(%rax), %rcx
	movq	%rcx, 400(%rdi)
	movl	%r8d, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L28
	cmpq	%rcx, %rdx
	je	.L28
	movzwl	-4(%rax), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	je	.L37
.L28:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	sall	$10, %edx
	subq	$4, %rax
	leal	-56613888(%r8,%rdx), %r8d
	movq	%rax, 400(%rdi)
	movl	%r8d, %eax
	ret
.L30:
	movl	$-1, %r8d
	jmp	.L28
	.cfi_endproc
.LFE3275:
	.size	_ZN6icu_6722UTF16CollationIterator17previousCodePointER10UErrorCode, .-_ZN6icu_6722UTF16CollationIterator17previousCodePointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UTF16CollationIterator20forwardNumCodePointsEiR10UErrorCode
	.type	_ZN6icu_6722UTF16CollationIterator20forwardNumCodePointsEiR10UErrorCode, @function
_ZN6icu_6722UTF16CollationIterator20forwardNumCodePointsEiR10UErrorCode:
.LFB3276:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jle	.L38
	movq	408(%rdi), %r8
	movq	400(%rdi), %rdx
	testq	%r8, %r8
	sete	%r9b
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%rcx, %rdx
	testl	%esi, %esi
	je	.L38
.L40:
	cmpq	%rdx, %r8
	je	.L38
	movzwl	(%rdx), %eax
	testw	%ax, %ax
	jne	.L41
	testb	%r9b, %r9b
	jne	.L61
.L41:
	leaq	2(%rdx), %rcx
	andl	$64512, %eax
	subl	$1, %esi
	movq	%rcx, 400(%rdi)
	cmpl	$55296, %eax
	jne	.L45
	cmpq	%rcx, %r8
	je	.L45
	movzwl	2(%rdx), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L45
	addq	$4, %rdx
	movq	%rdx, 400(%rdi)
	testl	%esi, %esi
	jne	.L40
.L38:
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%rdx, 408(%rdi)
	ret
	.cfi_endproc
.LFE3276:
	.size	_ZN6icu_6722UTF16CollationIterator20forwardNumCodePointsEiR10UErrorCode, .-_ZN6icu_6722UTF16CollationIterator20forwardNumCodePointsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UTF16CollationIterator21backwardNumCodePointsEiR10UErrorCode
	.type	_ZN6icu_6722UTF16CollationIterator21backwardNumCodePointsEiR10UErrorCode, @function
_ZN6icu_6722UTF16CollationIterator21backwardNumCodePointsEiR10UErrorCode:
.LFB3277:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jle	.L62
	movq	392(%rdi), %r8
	movq	400(%rdi), %rax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%rcx, %rax
	testl	%esi, %esi
	je	.L62
.L64:
	cmpq	%rax, %r8
	je	.L62
	movzwl	-2(%rax), %edx
	leaq	-2(%rax), %rcx
	subl	$1, %esi
	movq	%rcx, 400(%rdi)
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L67
	cmpq	%rcx, %r8
	je	.L67
	movzwl	-4(%rax), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L67
	subq	$4, %rax
	movq	%rax, 400(%rdi)
	testl	%esi, %esi
	jne	.L64
.L62:
	ret
	.cfi_endproc
.LFE3277:
	.size	_ZN6icu_6722UTF16CollationIterator21backwardNumCodePointsEiR10UErrorCode, .-_ZN6icu_6722UTF16CollationIterator21backwardNumCodePointsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UTF16CollationIteratorD2Ev
	.type	_ZN6icu_6722UTF16CollationIteratorD2Ev, @function
_ZN6icu_6722UTF16CollationIteratorD2Ev:
.LFB3265:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6722UTF16CollationIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CollationIteratorD2Ev@PLT
	.cfi_endproc
.LFE3265:
	.size	_ZN6icu_6722UTF16CollationIteratorD2Ev, .-_ZN6icu_6722UTF16CollationIteratorD2Ev
	.globl	_ZN6icu_6722UTF16CollationIteratorD1Ev
	.set	_ZN6icu_6722UTF16CollationIteratorD1Ev,_ZN6icu_6722UTF16CollationIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UTF16CollationIteratorD0Ev
	.type	_ZN6icu_6722UTF16CollationIteratorD0Ev, @function
_ZN6icu_6722UTF16CollationIteratorD0Ev:
.LFB3267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722UTF16CollationIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CollationIteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3267:
	.size	_ZN6icu_6722UTF16CollationIteratorD0Ev, .-_ZN6icu_6722UTF16CollationIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UTF16CollationIterator13resetToOffsetEi
	.type	_ZN6icu_6722UTF16CollationIterator13resetToOffsetEi, @function
_ZN6icu_6722UTF16CollationIterator13resetToOffsetEi:
.LFB3269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movslq	%esi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6717CollationIterator5resetEv@PLT
	movq	392(%rbx), %rax
	leaq	(%rax,%r12,2), %rax
	movq	%rax, 400(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3269:
	.size	_ZN6icu_6722UTF16CollationIterator13resetToOffsetEi, .-_ZN6icu_6722UTF16CollationIterator13resetToOffsetEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIterator13resetToOffsetEi
	.type	_ZN6icu_6725FCDUTF16CollationIterator13resetToOffsetEi, @function
_ZN6icu_6725FCDUTF16CollationIterator13resetToOffsetEi:
.LFB3286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movslq	%esi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6717CollationIterator5resetEv@PLT
	movq	416(%rbx), %rax
	movb	$1, 520(%rbx)
	leaq	(%rax,%r12,2), %rax
	movq	%rax, 424(%rbx)
	movq	%rax, %xmm0
	movq	440(%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 408(%rbx)
	movups	%xmm0, 392(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3286:
	.size	_ZN6icu_6725FCDUTF16CollationIterator13resetToOffsetEi, .-_ZN6icu_6725FCDUTF16CollationIterator13resetToOffsetEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UTF16CollationIterator18foundNULTerminatorEv
	.type	_ZN6icu_6722UTF16CollationIterator18foundNULTerminatorEv, @function
_ZN6icu_6722UTF16CollationIterator18foundNULTerminatorEv:
.LFB3273:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 408(%rdi)
	je	.L87
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	movq	400(%rdi), %rax
	subq	$2, %rax
	movq	%rax, %xmm0
	movl	$1, %eax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 400(%rdi)
	ret
	.cfi_endproc
.LFE3273:
	.size	_ZN6icu_6722UTF16CollationIterator18foundNULTerminatorEv, .-_ZN6icu_6722UTF16CollationIterator18foundNULTerminatorEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725FCDUTF16CollationIterator9getOffsetEv
	.type	_ZNK6icu_6725FCDUTF16CollationIterator9getOffsetEv, @function
_ZNK6icu_6725FCDUTF16CollationIterator9getOffsetEv:
.LFB3287:
	.cfi_startproc
	endbr64
	cmpb	$0, 520(%rdi)
	movq	400(%rdi), %rax
	movq	416(%rdi), %rcx
	jne	.L93
	movq	392(%rdi), %rsi
	movq	424(%rdi), %rdx
	cmpq	%rdx, %rsi
	je	.L93
	cmpq	%rax, %rsi
	je	.L94
	movq	432(%rdi), %rax
.L93:
	subq	%rcx, %rax
	sarq	%rax
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%rdx, %rax
	subq	%rcx, %rax
	sarq	%rax
	ret
	.cfi_endproc
.LFE3287:
	.size	_ZNK6icu_6725FCDUTF16CollationIterator9getOffsetEv, .-_ZNK6icu_6725FCDUTF16CollationIterator9getOffsetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIterator18foundNULTerminatorEv
	.type	_ZN6icu_6725FCDUTF16CollationIterator18foundNULTerminatorEv, @function
_ZN6icu_6725FCDUTF16CollationIterator18foundNULTerminatorEv:
.LFB3289:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 408(%rdi)
	je	.L98
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	movq	400(%rdi), %rax
	subq	$2, %rax
	movq	%rax, %xmm0
	movq	%rax, 440(%rdi)
	movl	$1, %eax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 400(%rdi)
	ret
	.cfi_endproc
.LFE3289:
	.size	_ZN6icu_6725FCDUTF16CollationIterator18foundNULTerminatorEv, .-_ZN6icu_6725FCDUTF16CollationIterator18foundNULTerminatorEv
	.align 2
	.p2align 4
	.type	_ZN6icu_6725FCDUTF16CollationIterator11nextSegmentER10UErrorCode.part.0, @function
_ZN6icu_6725FCDUTF16CollationIterator11nextSegmentER10UErrorCode.part.0:
.LFB4327:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	400(%rdi), %rbx
	movq	440(%rdi), %rdx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L150:
	movq	56(%rdi), %rcx
	movzwl	%ax, %esi
	movzbl	%ah, %eax
	movzbl	(%rcx,%rax), %eax
	testb	%al, %al
	je	.L100
	movl	%esi, %ecx
	shrb	$5, %cl
	btl	%ecx, %eax
	jnc	.L100
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L101
	cmpq	%rdx, %r14
	jne	.L148
.L101:
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	movzbl	%ah, %edx
	testw	$-256, %ax
	je	.L149
	cmpb	%r15b, %dl
	jb	.L110
	leal	32382(%rax), %edx
	testw	$-3, %dx
	je	.L110
.L103:
	movq	440(%r12), %rdx
	movl	%eax, %r15d
	cmpq	%r14, %rdx
	je	.L106
	testb	%al, %al
	je	.L106
	movq	%r14, %rbx
.L107:
	movq	448(%r12), %rdi
	movzwl	(%rbx), %eax
	leaq	2(%rbx), %r14
	cmpw	8(%rdi), %ax
	jnb	.L150
.L100:
	cmpq	%rbx, 400(%r12)
	je	.L106
.L116:
	movq	%rbx, 432(%r12)
	movq	%rbx, 408(%r12)
.L104:
	movb	$0, 520(%r12)
	movl	$1, %eax
.L99:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	cmpq	%rbx, %rdx
	je	.L109
	movzwl	2(%r14), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L109
	sall	$10, %esi
	leaq	4(%r14), %rbx
	leal	-56613888(%rax,%rsi), %esi
	.p2align 4,,10
	.p2align 3
.L109:
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	cmpw	$255, %ax
	jbe	.L151
	movq	%rbx, %r14
.L110:
	movq	440(%r12), %rdx
	movq	448(%r12), %rdi
	cmpq	%r14, %rdx
	je	.L108
	movzwl	(%r14), %eax
	cmpw	8(%rdi), %ax
	jb	.L108
	movq	56(%rdi), %rcx
	movzwl	%ax, %esi
	movzbl	%ah, %eax
	movzbl	(%rcx,%rax), %eax
	testb	%al, %al
	je	.L108
	movl	%esi, %ecx
	shrb	$5, %cl
	btl	%ecx, %eax
	jnc	.L108
	movl	%esi, %eax
	leaq	2(%r14), %rbx
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L152
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L148:
	movzwl	2(%rbx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L101
	sall	$10, %esi
	leaq	4(%rbx), %r14
	leal	-56613888(%rax,%rsi), %esi
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%r14, 432(%r12)
	movq	%r14, 408(%r12)
	jmp	.L104
.L151:
	movq	448(%r12), %rdi
	.p2align 4,,10
	.p2align 3
.L108:
	movq	400(%r12), %rbx
	movq	%r14, %r8
	movq	%r14, %rdx
	leaq	456(%r12), %rcx
	movq	%r13, %r9
	subq	%rbx, %r8
	movq	%rbx, %rsi
	sarq	%r8
	call	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_RNS_13UnicodeStringEiR10UErrorCode@PLT
	movl	0(%r13), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L99
	movq	%rbx, %xmm0
	movq	%r14, %xmm1
	movswl	464(%r12), %eax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 424(%r12)
	testb	$17, %al
	jne	.L119
	leaq	466(%r12), %rdx
	testb	$2, %al
	jne	.L112
	movq	480(%r12), %rdx
.L112:
	movq	%rdx, 392(%r12)
	testw	%ax, %ax
	js	.L114
	sarl	$5, %eax
.L115:
	cltq
	movq	%rdx, 400(%r12)
	leaq	(%rdx,%rax,2), %rax
	movq	%rax, 408(%r12)
	jmp	.L104
.L114:
	movl	468(%r12), %eax
	jmp	.L115
.L119:
	xorl	%edx, %edx
	jmp	.L112
.L149:
	cmpq	%rbx, 400(%r12)
	je	.L103
	jmp	.L116
	.cfi_endproc
.LFE4327:
	.size	_ZN6icu_6725FCDUTF16CollationIterator11nextSegmentER10UErrorCode.part.0, .-_ZN6icu_6725FCDUTF16CollationIterator11nextSegmentER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIterator14handleNextCE32ERiR10UErrorCode
	.type	_ZN6icu_6725FCDUTF16CollationIterator14handleNextCE32ERiR10UErrorCode, @function
_ZN6icu_6725FCDUTF16CollationIterator14handleNextCE32ERiR10UErrorCode:
.LFB3288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	cmpb	$0, 520(%rdi)
	movq	400(%rdi), %rax
	jg	.L187
	je	.L188
	movq	%rax, 424(%rdi)
	movq	%rax, 392(%rdi)
	cmpq	%rax, 432(%rdi)
	je	.L169
	movb	$0, 520(%rdi)
	cmpq	%rax, 408(%rdi)
	jne	.L168
.L169:
	movq	440(%rdi), %rcx
	movb	$1, 520(%rdi)
	movq	%rcx, 408(%rdi)
.L170:
	cmpq	%rcx, %rax
	je	.L162
	movzwl	(%rax), %r10d
	leaq	2(%rax), %r11
	movq	%r11, 400(%rdi)
	movl	%r10d, %r8d
	movl	%r10d, (%rbx)
	sarl	$5, %r8d
	cmpl	$191, %r10d
	jg	.L189
	movl	%r10d, %r9d
	movslq	%r8d, %r8
	andl	$31, %r9d
.L159:
	movq	8(%rdi), %rdx
	movq	(%rdx), %rax
	movq	16(%rdx), %rdx
	movzwl	(%rax,%r8,2), %eax
	leal	(%r9,%rax,4), %eax
	cltq
	movl	(%rdx,%rax,4), %eax
.L153:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	cmpq	%rax, 408(%rdi)
	jne	.L168
	movq	424(%rdi), %rsi
	cmpq	%rsi, 392(%rdi)
	je	.L169
	movq	432(%rdi), %rax
	movq	%rax, %xmm0
	movq	%rax, 424(%rdi)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 392(%rdi)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	_ZN6icu_6712CollationFCD9tcccIndexE(%rip), %r9
	movslq	%r8d, %r8
	movzbl	(%r9,%r8), %r12d
	movl	%r10d, %r9d
	andl	$31, %r9d
	testl	%r12d, %r12d
	je	.L159
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %r13
	movl	0(%r13,%r12,4), %r12d
	btl	%r10d, %r12d
	jnc	.L159
	andl	$2096897, %r10d
	cmpl	$3841, %r10d
	je	.L160
	cmpq	%rcx, %r11
	je	.L159
	movzwl	2(%rax), %ecx
	movl	%ecx, %esi
	cmpl	$767, %ecx
	jle	.L159
	sarl	$5, %ecx
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %r10
	movslq	%ecx, %rcx
	movzbl	(%r10,%rcx), %ecx
	testl	%ecx, %ecx
	je	.L159
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %r10
	movl	(%r10,%rcx,4), %ecx
	btl	%esi, %ecx
	jnc	.L159
.L160:
	movq	%rax, 400(%rdi)
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L162
	movq	%rdx, %rsi
	movq	%rdi, -40(%rbp)
	call	_ZN6icu_6725FCDUTF16CollationIterator11nextSegmentER10UErrorCode.part.0
	testb	%al, %al
	je	.L162
	movq	-40(%rbp), %rdi
	movq	400(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L168:
	movzwl	(%rax), %r8d
	leaq	2(%rax), %rdx
	movq	%rdx, 400(%rdi)
	movl	%r8d, %r9d
	movl	%r8d, (%rbx)
	sarl	$5, %r8d
	andl	$31, %r9d
	movslq	%r8d, %r8
	jmp	.L159
.L162:
	movl	$-1, (%rbx)
	movl	$192, %eax
	jmp	.L153
.L187:
	movq	408(%rdi), %rcx
	jmp	.L170
	.cfi_endproc
.LFE3288:
	.size	_ZN6icu_6725FCDUTF16CollationIterator14handleNextCE32ERiR10UErrorCode, .-_ZN6icu_6725FCDUTF16CollationIterator14handleNextCE32ERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIterator13nextCodePointER10UErrorCode
	.type	_ZN6icu_6725FCDUTF16CollationIterator13nextCodePointER10UErrorCode, @function
_ZN6icu_6725FCDUTF16CollationIterator13nextCodePointER10UErrorCode:
.LFB3290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	cmpb	$0, 520(%rdi)
	movq	400(%rdi), %rax
	jg	.L233
	je	.L234
	movq	%rax, 424(%rdi)
	movq	%rax, 392(%rdi)
	cmpq	%rax, 432(%rdi)
	je	.L206
	movb	$0, 520(%rdi)
	cmpq	%rax, 408(%rdi)
	jne	.L205
.L206:
	movq	440(%rdi), %rdx
	movb	$1, 520(%rdi)
	movq	%rdx, 408(%rdi)
.L208:
	cmpq	%rdx, %rax
	je	.L232
	movzwl	(%rax), %r8d
	leaq	2(%rax), %rcx
	movq	%rcx, 400(%rdi)
	movl	%r8d, %r9d
	movl	%r8d, %r11d
	cmpl	$191, %r8d
	jg	.L235
	testl	%r8d, %r8d
	jne	.L190
	testq	%rdx, %rdx
	je	.L236
.L190:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	cmpq	%rax, 408(%rdi)
	jne	.L205
	movq	424(%rdi), %rbx
	cmpq	%rbx, 392(%rdi)
	je	.L206
	movq	432(%rdi), %rax
	movq	%rax, %xmm0
	movq	%rax, 424(%rdi)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 392(%rdi)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L235:
	movl	%r8d, %r10d
	leaq	_ZN6icu_6712CollationFCD9tcccIndexE(%rip), %rbx
	sarl	$5, %r10d
	movslq	%r10d, %r10
	movzbl	(%rbx,%r10), %r10d
	testl	%r10d, %r10d
	jne	.L237
.L195:
	movl	%r8d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L190
	cmpq	%rcx, 408(%rdi)
	je	.L190
	movzwl	(%rcx), %edx
.L207:
	movl	%edx, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L190
	addq	$2, %rcx
	sall	$10, %r8d
	movq	%rcx, 400(%rdi)
	leal	-56613888(%r8,%rdx), %r8d
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	leaq	2(%rax), %rcx
	movzwl	(%rax), %r8d
	movq	%rcx, 400(%rdi)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L237:
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %rbx
	movl	(%rbx,%r10,4), %r10d
	btl	%r8d, %r10d
	jnc	.L195
	andl	$2096897, %r9d
	cmpl	$3841, %r9d
	je	.L196
	cmpq	%rdx, %rcx
	je	.L190
	movzwl	2(%rax), %edx
	cmpl	$767, %edx
	jle	.L199
	movl	%edx, %r10d
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %rbx
	sarl	$5, %r10d
	movslq	%r10d, %r10
	movzbl	(%rbx,%r10), %r10d
	testl	%r10d, %r10d
	je	.L199
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %rbx
	movl	(%rbx,%r10,4), %r10d
	btl	%edx, %r10d
	jnc	.L199
.L196:
	movq	%rax, 400(%rdi)
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L232
	movq	%rdi, -24(%rbp)
	call	_ZN6icu_6725FCDUTF16CollationIterator11nextSegmentER10UErrorCode.part.0
	testb	%al, %al
	je	.L232
	movq	-24(%rbp), %rdi
	movq	400(%rdi), %rax
	leaq	2(%rax), %rcx
	movzwl	(%rax), %r8d
	movq	%rcx, 400(%rdi)
	jmp	.L195
.L233:
	movq	408(%rdi), %rdx
	jmp	.L208
.L199:
	andl	$-1024, %r11d
	cmpl	$55296, %r11d
	je	.L207
	jmp	.L190
.L236:
	movq	%rax, %xmm0
	movq	%rax, 440(%rdi)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 400(%rdi)
.L232:
	movl	$-1, %r8d
	jmp	.L190
	.cfi_endproc
.LFE3290:
	.size	_ZN6icu_6725FCDUTF16CollationIterator13nextCodePointER10UErrorCode, .-_ZN6icu_6725FCDUTF16CollationIterator13nextCodePointER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6725FCDUTF16CollationIterator15previousSegmentER10UErrorCode.part.0, @function
_ZN6icu_6725FCDUTF16CollationIterator15previousSegmentER10UErrorCode.part.0:
.LFB4328:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	400(%rdi), %r14
	movq	416(%rdi), %rdx
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L283:
	movzwl	%ax, %esi
	andl	$64512, %eax
	cmpl	$56320, %eax
	je	.L240
	movq	56(%rdi), %rdx
	movl	%esi, %eax
	movzbl	%ah, %eax
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L239
	movl	%esi, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L239
.L241:
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	testb	%al, %al
	je	.L282
	testb	%r15b, %r15b
	je	.L262
	cmpb	%r15b, %al
	ja	.L252
.L262:
	leal	32382(%rax), %edx
	testw	$-3, %dx
	je	.L252
.L243:
	movq	416(%r12), %rdx
	movzbl	%ah, %eax
	movl	%eax, %r15d
	cmpq	%r13, %rdx
	je	.L247
	testl	%eax, %eax
	je	.L247
	movq	%r13, %r14
.L248:
	movq	448(%r12), %rdi
	movzwl	-2(%r14), %eax
	leaq	-2(%r14), %r13
	cmpw	8(%rdi), %ax
	jnb	.L283
.L239:
	cmpq	%r14, 400(%r12)
	je	.L247
.L258:
	movq	%r14, 424(%r12)
	movq	%r14, 392(%r12)
.L244:
	movb	$0, 520(%r12)
	movl	$1, %eax
.L238:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	cmpq	%r14, %rdx
	jnb	.L251
	movzwl	-4(%r13), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L251
	sall	$10, %eax
	leaq	-4(%r13), %r14
	leal	-56613888(%rsi,%rax), %esi
	.p2align 4,,10
	.p2align 3
.L251:
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	testw	%ax, %ax
	je	.L284
	movq	%r14, %r13
.L252:
	movq	448(%r12), %rdi
	cmpw	$255, %ax
	jbe	.L249
	movq	416(%r12), %rdx
	cmpq	%r13, %rdx
	je	.L249
	movzwl	-2(%r13), %eax
	cmpw	8(%rdi), %ax
	jb	.L249
	movzwl	%ax, %esi
	andl	$64512, %eax
	leaq	-2(%r13), %r14
	cmpl	$56320, %eax
	je	.L250
	movq	56(%rdi), %rdx
	movl	%esi, %eax
	movzbl	%ah, %eax
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L249
	movl	%esi, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jc	.L251
	.p2align 4,,10
	.p2align 3
.L249:
	movq	400(%r12), %r14
	leaq	456(%r12), %rcx
	movq	%rbx, %r9
	movq	%r13, %rsi
	movq	%r14, %r8
	movq	%r14, %rdx
	subq	%r13, %r8
	sarq	%r8
	call	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_RNS_13UnicodeStringEiR10UErrorCode@PLT
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L238
	movq	%r13, %xmm0
	movq	%r14, %xmm1
	movswl	464(%r12), %eax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 424(%r12)
	testb	$17, %al
	jne	.L261
	leaq	466(%r12), %rdx
	testb	$2, %al
	jne	.L254
	movq	480(%r12), %rdx
.L254:
	movq	%rdx, 392(%r12)
	testw	%ax, %ax
	js	.L256
	sarl	$5, %eax
.L257:
	cltq
	leaq	(%rdx,%rax,2), %rax
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 400(%r12)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L240:
	cmpq	%r13, %rdx
	jnb	.L241
	movzwl	-4(%r14), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L241
	sall	$10, %eax
	leaq	-4(%r14), %r13
	leal	-56613888(%rsi,%rax), %esi
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%r13, 424(%r12)
	movq	%r13, 392(%r12)
	jmp	.L244
.L256:
	movl	468(%r12), %eax
	jmp	.L257
.L261:
	xorl	%edx, %edx
	jmp	.L254
.L284:
	movq	448(%r12), %rdi
	jmp	.L249
.L282:
	cmpq	%r14, 400(%r12)
	je	.L243
	jmp	.L258
	.cfi_endproc
.LFE4328:
	.size	_ZN6icu_6725FCDUTF16CollationIterator15previousSegmentER10UErrorCode.part.0, .-_ZN6icu_6725FCDUTF16CollationIterator15previousSegmentER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIterator17previousCodePointER10UErrorCode
	.type	_ZN6icu_6725FCDUTF16CollationIterator17previousCodePointER10UErrorCode, @function
_ZN6icu_6725FCDUTF16CollationIterator17previousCodePointER10UErrorCode:
.LFB3291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	cmpb	$0, 520(%rdi)
	movq	400(%rdi), %rax
	js	.L324
	je	.L297
	movq	%rax, 432(%rdi)
	movq	%rax, 408(%rdi)
	cmpq	%rax, 424(%rdi)
	je	.L325
	movb	$0, 520(%rdi)
.L297:
	cmpq	%rax, 392(%rdi)
	jne	.L326
	movq	424(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L298
	movq	%rdx, %xmm0
	movq	%rdx, 432(%rdi)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 400(%rdi)
.L298:
	movq	416(%rdi), %rcx
	movb	$-1, 520(%rdi)
	movq	%rcx, 392(%rdi)
.L300:
	cmpq	%rcx, %rdx
	je	.L292
	movzwl	-2(%rdx), %r9d
	leaq	-2(%rdx), %r8
	movq	%r8, 400(%rdi)
	movl	%r9d, %r10d
	movl	%r9d, %eax
	cmpl	$767, %r9d
	jle	.L289
	movl	%r9d, %r11d
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %rbx
	sarl	$5, %r11d
	movslq	%r11d, %r11
	movzbl	(%rbx,%r11), %r11d
	testl	%r11d, %r11d
	je	.L289
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %rbx
	movl	(%rbx,%r11,4), %r11d
	btl	%r9d, %r11d
	jnc	.L289
	andl	$2096897, %r10d
	cmpl	$3841, %r10d
	je	.L290
	cmpq	%rcx, %r8
	je	.L285
	movzwl	-4(%rdx), %ecx
	movl	%ecx, %r10d
	cmpl	$191, %ecx
	jle	.L291
	sarl	$5, %ecx
	leaq	_ZN6icu_6712CollationFCD9tcccIndexE(%rip), %r11
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %ecx
	testl	%ecx, %ecx
	je	.L291
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %r11
	movl	(%r11,%rcx,4), %ecx
	btl	%r10d, %ecx
	jnc	.L291
.L290:
	movl	(%rsi), %eax
	movq	%rdx, 400(%rdi)
	testl	%eax, %eax
	jg	.L292
	movq	%rdi, -24(%rbp)
	call	_ZN6icu_6725FCDUTF16CollationIterator15previousSegmentER10UErrorCode.part.0
	testb	%al, %al
	je	.L292
	movq	-24(%rbp), %rdi
	movq	400(%rdi), %rax
	leaq	-2(%rax), %r8
	movzwl	-2(%rax), %eax
	movq	%r8, 400(%rdi)
	.p2align 4,,10
	.p2align 3
.L289:
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L285
.L328:
	cmpq	%r8, 392(%rdi)
	je	.L285
.L299:
	movzwl	-2(%r8), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	je	.L327
.L285:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	movq	416(%rdi), %rcx
	movb	$-1, 520(%rdi)
	movq	%rax, %rdx
	movq	%rcx, 392(%rdi)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L326:
	leaq	-2(%rax), %r8
	movzwl	-2(%rax), %eax
	movq	%r8, 400(%rdi)
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L285
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L327:
	subq	$2, %r8
	sall	$10, %edx
	movq	%r8, 400(%rdi)
	addq	$24, %rsp
	leal	-56613888(%rax,%rdx), %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L324:
	.cfi_restore_state
	movq	392(%rdi), %rcx
	movq	%rax, %rdx
	jmp	.L300
.L291:
	andl	$-1024, %r9d
	cmpl	$56320, %r9d
	je	.L299
	jmp	.L285
.L292:
	movl	$-1, %eax
	jmp	.L285
	.cfi_endproc
.LFE3291:
	.size	_ZN6icu_6725FCDUTF16CollationIterator17previousCodePointER10UErrorCode, .-_ZN6icu_6725FCDUTF16CollationIterator17previousCodePointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIterator21backwardNumCodePointsEiR10UErrorCode
	.type	_ZN6icu_6725FCDUTF16CollationIterator21backwardNumCodePointsEiR10UErrorCode, @function
_ZN6icu_6725FCDUTF16CollationIterator21backwardNumCodePointsEiR10UErrorCode:
.LFB3293:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jle	.L369
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZN6icu_6712CollationFCD9tcccIndexE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	400(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L342:
	cmpb	$0, 520(%rbx)
	js	.L373
	je	.L340
	movq	%rax, 432(%rbx)
	movq	%rax, 408(%rbx)
	cmpq	424(%rbx), %rax
	je	.L341
	movb	$0, 520(%rbx)
.L340:
	movq	392(%rbx), %rcx
	cmpq	%rcx, %rax
	jne	.L374
	movq	424(%rbx), %rax
	cmpq	%rax, %rcx
	je	.L341
	movq	%rax, %xmm0
	movq	%rax, 432(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 400(%rbx)
.L341:
	movq	416(%rbx), %rcx
	movb	$-1, 520(%rbx)
	movq	%rcx, 392(%rbx)
.L372:
	movq	%rax, %r10
	cmpq	%rax, %rcx
	je	.L329
	movzwl	-2(%r10), %edi
	subq	$2, %rax
	movq	%rax, 400(%rbx)
	movl	%edi, %esi
	cmpl	$767, %edi
	jle	.L332
	movl	%edi, %r11d
	sarl	$5, %r11d
	movslq	%r11d, %r11
	movzbl	0(%r13,%r11), %r11d
	testl	%r11d, %r11d
	je	.L332
	movl	(%r14,%r11,4), %r11d
	btl	%edi, %r11d
	jnc	.L332
	andl	$2096897, %esi
	cmpl	$3841, %esi
	je	.L333
	cmpq	%rcx, %rax
	je	.L334
	movzwl	-4(%r10), %ecx
	movl	%ecx, %esi
	cmpl	$191, %ecx
	jle	.L335
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r15,%rcx), %ecx
	testl	%ecx, %ecx
	je	.L335
	movl	(%r8,%rcx,4), %ecx
	btl	%esi, %ecx
	jnc	.L335
	.p2align 4,,10
	.p2align 3
.L333:
	movl	(%rdx), %eax
	movq	%r10, 400(%rbx)
	testl	%eax, %eax
	jg	.L329
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN6icu_6725FCDUTF16CollationIterator15previousSegmentER10UErrorCode.part.0
	testb	%al, %al
	je	.L329
	movq	400(%rbx), %rcx
	movq	-56(%rbp), %rdx
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %r8
	leaq	-2(%rcx), %rax
	movzwl	-2(%rcx), %esi
	movq	%rax, 400(%rbx)
	.p2align 4,,10
	.p2align 3
.L332:
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L334
	cmpq	%rax, 392(%rbx)
	je	.L334
.L343:
	movzwl	-2(%rax), %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L334
	subq	$2, %rax
	movq	%rax, 400(%rbx)
	.p2align 4,,10
	.p2align 3
.L334:
	subl	$1, %r12d
	jne	.L342
.L329:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	leaq	-2(%rax), %rcx
	movzwl	-2(%rax), %esi
	movq	%rcx, 400(%rbx)
	movq	%rcx, %rax
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L335:
	andl	$-1024, %edi
	cmpl	$56320, %edi
	je	.L343
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L373:
	movq	392(%rbx), %rcx
	jmp	.L372
.L369:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE3293:
	.size	_ZN6icu_6725FCDUTF16CollationIterator21backwardNumCodePointsEiR10UErrorCode, .-_ZN6icu_6725FCDUTF16CollationIterator21backwardNumCodePointsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIterator20forwardNumCodePointsEiR10UErrorCode
	.type	_ZN6icu_6725FCDUTF16CollationIterator20forwardNumCodePointsEiR10UErrorCode, @function
_ZN6icu_6725FCDUTF16CollationIterator20forwardNumCodePointsEiR10UErrorCode:
.LFB3292:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jle	.L421
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZN6icu_6712CollationFCD8tcccBitsE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN6icu_6712CollationFCD9tcccIndexE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	400(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L393:
	cmpb	$0, 520(%rbx)
	jg	.L425
	je	.L426
	movq	%rax, 424(%rbx)
	movq	%rax, 392(%rbx)
	cmpq	%rax, 432(%rbx)
	je	.L392
	movb	$0, 520(%rbx)
	cmpq	%rax, 408(%rbx)
	jne	.L391
.L392:
	movq	440(%rbx), %rcx
	movb	$1, 520(%rbx)
	movq	%rcx, 408(%rbx)
.L424:
	movq	%rax, %r10
	cmpq	%rax, %rcx
	je	.L375
	movzwl	(%r10), %edi
	addq	$2, %rax
	movq	%rax, 400(%rbx)
	movl	%edi, %esi
	cmpl	$191, %edi
	jle	.L380
	movl	%edi, %r11d
	sarl	$5, %r11d
	movslq	%r11d, %r11
	movzbl	0(%r13,%r11), %r11d
	testl	%r11d, %r11d
	je	.L381
	movl	(%r14,%r11,4), %r11d
	btl	%edi, %r11d
	jnc	.L381
	andl	$2096897, %esi
	cmpl	$3841, %esi
	je	.L382
	cmpq	%rcx, %rax
	je	.L385
	movzwl	2(%r10), %ecx
	cmpl	$767, %ecx
	jle	.L386
	movl	%ecx, %r9d
	sarl	$5, %r9d
	movslq	%r9d, %r9
	movzbl	(%r15,%r9), %r9d
	testl	%r9d, %r9d
	je	.L386
	movl	(%r8,%r9,4), %r9d
	btl	%ecx, %r9d
	jnc	.L386
	.p2align 4,,10
	.p2align 3
.L382:
	movl	(%rdx), %eax
	movq	%r10, 400(%rbx)
	testl	%eax, %eax
	jg	.L375
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN6icu_6725FCDUTF16CollationIterator11nextSegmentER10UErrorCode.part.0
	testb	%al, %al
	je	.L375
	movq	400(%rbx), %rcx
	movq	-56(%rbp), %rdx
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %r8
	leaq	2(%rcx), %rax
	movzwl	(%rcx), %esi
	movq	%rax, 400(%rbx)
	.p2align 4,,10
	.p2align 3
.L381:
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L385
	cmpq	%rax, 408(%rbx)
	je	.L385
	movzwl	(%rax), %ecx
.L394:
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L385
	addq	$2, %rax
	movq	%rax, 400(%rbx)
	.p2align 4,,10
	.p2align 3
.L385:
	subl	$1, %r12d
	jne	.L393
.L375:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	cmpq	%rax, 408(%rbx)
	jne	.L391
	movq	424(%rbx), %rdi
	cmpq	%rdi, 392(%rbx)
	je	.L392
	movq	432(%rbx), %rax
	movq	%rax, %xmm0
	movq	%rax, 424(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 392(%rbx)
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L391:
	leaq	2(%rax), %rcx
	movzwl	(%rax), %esi
	movq	%rcx, 400(%rbx)
	movq	%rcx, %rax
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L380:
	testl	%edi, %edi
	jne	.L385
	testq	%rcx, %rcx
	jne	.L385
	movq	%r10, %xmm0
	movq	%r10, 440(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 400(%rbx)
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L386:
	andl	$-1024, %edi
	cmpl	$55296, %edi
	je	.L394
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L425:
	movq	408(%rbx), %rcx
	jmp	.L424
.L421:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE3292:
	.size	_ZN6icu_6725FCDUTF16CollationIterator20forwardNumCodePointsEiR10UErrorCode, .-_ZN6icu_6725FCDUTF16CollationIterator20forwardNumCodePointsEiR10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3535:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3535:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3538:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L440
	ret
	.p2align 4,,10
	.p2align 3
.L440:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L428
	cmpb	$0, 12(%rbx)
	jne	.L441
.L432:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L428:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L432
	.cfi_endproc
.LFE3538:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3541:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L444
	ret
	.p2align 4,,10
	.p2align 3
.L444:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3541:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3544:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L447
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3544:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L453
.L449:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L454
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L454:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3546:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3547:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3547:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3548:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3548:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3549:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3549:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3550:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3550:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3551:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3551:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3552:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L470
	testl	%edx, %edx
	jle	.L470
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L473
.L462:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L462
	.cfi_endproc
.LFE3552:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L477
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L477
	testl	%r12d, %r12d
	jg	.L484
	cmpb	$0, 12(%rbx)
	jne	.L485
.L479:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L479
.L485:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L477:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3553:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L487
	movq	(%rdi), %r8
.L488:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L491
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L491
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L491:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3554:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3555:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L498
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3555:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3556:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3556:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3557:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3557:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3558:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3558:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3560:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3560:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3562:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3562:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UTF16CollationIteratorC2ERKS0_PKDs
	.type	_ZN6icu_6722UTF16CollationIteratorC2ERKS0_PKDs, @function
_ZN6icu_6722UTF16CollationIteratorC2ERKS0_PKDs:
.LFB3262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6717CollationIteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6722UTF16CollationIteratorE(%rip), %rax
	movq	392(%r13), %rdx
	movq	%r12, 392(%rbx)
	movq	%rax, (%rbx)
	movq	400(%r13), %rax
	subq	%rdx, %rax
	addq	%r12, %rax
	movq	%rax, 400(%rbx)
	movq	408(%r13), %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	addq	%rcx, %r12
	testq	%rax, %rax
	cmovne	%r12, %rax
	movq	%rax, 408(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3262:
	.size	_ZN6icu_6722UTF16CollationIteratorC2ERKS0_PKDs, .-_ZN6icu_6722UTF16CollationIteratorC2ERKS0_PKDs
	.globl	_ZN6icu_6722UTF16CollationIteratorC1ERKS0_PKDs
	.set	_ZN6icu_6722UTF16CollationIteratorC1ERKS0_PKDs,_ZN6icu_6722UTF16CollationIteratorC2ERKS0_PKDs
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIteratorC2ERKS0_PKDs
	.type	_ZN6icu_6725FCDUTF16CollationIteratorC2ERKS0_PKDs, @function
_ZN6icu_6725FCDUTF16CollationIteratorC2ERKS0_PKDs:
.LFB3279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6717CollationIteratorC2ERKS0_@PLT
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6725FCDUTF16CollationIteratorE(%rip), %rax
	movq	$0, 408(%rbx)
	movups	%xmm0, 392(%rbx)
	movq	416(%r12), %rdx
	leaq	456(%r12), %rsi
	movq	%rax, (%rbx)
	movq	424(%r12), %rax
	movq	%r13, 416(%rbx)
	subq	%rdx, %rax
	addq	%r13, %rax
	movq	%rax, 424(%rbx)
	movq	432(%r12), %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	addq	%r13, %rcx
	testq	%rax, %rax
	cmovne	%rcx, %rax
	movq	%rax, 432(%rbx)
	movq	440(%r12), %rax
	movq	%rax, %rdi
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	leaq	456(%rbx), %rdi
	addq	%r13, %rdx
	testq	%rax, %rax
	cmovne	%rdx, %rax
	movq	%rax, 440(%rbx)
	movq	448(%r12), %rax
	movq	%rax, 448(%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzbl	520(%r12), %eax
	movb	%al, 520(%rbx)
	testb	%al, %al
	je	.L512
	movq	392(%r12), %rax
.L513:
	movq	416(%r12), %rdx
	subq	%rdx, %rax
	addq	%r13, %rax
	movq	%rax, 392(%rbx)
	movq	400(%r12), %rax
	subq	%rdx, %rax
	addq	%r13, %rax
	movq	%rax, 400(%rbx)
	movq	408(%r12), %rax
	movq	%rax, %rsi
	subq	%rdx, %rsi
	addq	%rsi, %r13
	testq	%rax, %rax
	cmovne	%r13, %rax
	movq	%rax, 408(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	.cfi_restore_state
	movq	392(%r12), %rsi
	movq	424(%r12), %rax
	cmpq	%rax, %rsi
	je	.L513
	movswl	464(%rbx), %eax
	testb	$17, %al
	jne	.L521
	leaq	466(%rbx), %rcx
	testb	$2, %al
	je	.L529
.L517:
	movq	400(%r12), %rdx
	movq	%rcx, 392(%rbx)
	subq	%rsi, %rdx
	addq	%rcx, %rdx
	movq	%rdx, 400(%rbx)
	testw	%ax, %ax
	js	.L519
	sarl	$5, %eax
.L520:
	cltq
	leaq	(%rcx,%rax,2), %rax
	movq	%rax, 408(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L529:
	.cfi_restore_state
	movq	480(%rbx), %rcx
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L519:
	movl	468(%rbx), %eax
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L521:
	xorl	%ecx, %ecx
	jmp	.L517
	.cfi_endproc
.LFE3279:
	.size	_ZN6icu_6725FCDUTF16CollationIteratorC2ERKS0_PKDs, .-_ZN6icu_6725FCDUTF16CollationIteratorC2ERKS0_PKDs
	.globl	_ZN6icu_6725FCDUTF16CollationIteratorC1ERKS0_PKDs
	.set	_ZN6icu_6725FCDUTF16CollationIteratorC1ERKS0_PKDs,_ZN6icu_6725FCDUTF16CollationIteratorC2ERKS0_PKDs
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIterator15switchToForwardEv
	.type	_ZN6icu_6725FCDUTF16CollationIterator15switchToForwardEv, @function
_ZN6icu_6725FCDUTF16CollationIterator15switchToForwardEv:
.LFB3294:
	.cfi_startproc
	endbr64
	cmpb	$0, 520(%rdi)
	js	.L535
	movq	424(%rdi), %rax
	cmpq	%rax, 392(%rdi)
	je	.L534
	movq	432(%rdi), %rax
	movq	%rax, %xmm0
	movq	%rax, 424(%rdi)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 392(%rdi)
.L534:
	movq	440(%rdi), %rax
	movb	$1, 520(%rdi)
	movq	%rax, 408(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	movq	400(%rdi), %rax
	movq	%rax, 424(%rdi)
	movq	%rax, 392(%rdi)
	cmpq	432(%rdi), %rax
	je	.L534
	movb	$0, 520(%rdi)
	ret
	.cfi_endproc
.LFE3294:
	.size	_ZN6icu_6725FCDUTF16CollationIterator15switchToForwardEv, .-_ZN6icu_6725FCDUTF16CollationIterator15switchToForwardEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIterator11nextSegmentER10UErrorCode
	.type	_ZN6icu_6725FCDUTF16CollationIterator11nextSegmentER10UErrorCode, @function
_ZN6icu_6725FCDUTF16CollationIterator11nextSegmentER10UErrorCode:
.LFB3295:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L537
	jmp	_ZN6icu_6725FCDUTF16CollationIterator11nextSegmentER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L537:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3295:
	.size	_ZN6icu_6725FCDUTF16CollationIterator11nextSegmentER10UErrorCode, .-_ZN6icu_6725FCDUTF16CollationIterator11nextSegmentER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIterator16switchToBackwardEv
	.type	_ZN6icu_6725FCDUTF16CollationIterator16switchToBackwardEv, @function
_ZN6icu_6725FCDUTF16CollationIterator16switchToBackwardEv:
.LFB3296:
	.cfi_startproc
	endbr64
	cmpb	$0, 520(%rdi)
	movq	424(%rdi), %rax
	jle	.L539
	movq	400(%rdi), %rdx
	movq	%rdx, 432(%rdi)
	movq	%rdx, 408(%rdi)
	cmpq	%rax, %rdx
	je	.L542
	movb	$0, 520(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	cmpq	%rax, 392(%rdi)
	je	.L542
	movq	%rax, %xmm0
	movq	%rax, 432(%rdi)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 400(%rdi)
.L542:
	movq	416(%rdi), %rax
	movb	$-1, 520(%rdi)
	movq	%rax, 392(%rdi)
	ret
	.cfi_endproc
.LFE3296:
	.size	_ZN6icu_6725FCDUTF16CollationIterator16switchToBackwardEv, .-_ZN6icu_6725FCDUTF16CollationIterator16switchToBackwardEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIterator15previousSegmentER10UErrorCode
	.type	_ZN6icu_6725FCDUTF16CollationIterator15previousSegmentER10UErrorCode, @function
_ZN6icu_6725FCDUTF16CollationIterator15previousSegmentER10UErrorCode:
.LFB3297:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L544
	jmp	_ZN6icu_6725FCDUTF16CollationIterator15previousSegmentER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L544:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3297:
	.size	_ZN6icu_6725FCDUTF16CollationIterator15previousSegmentER10UErrorCode, .-_ZN6icu_6725FCDUTF16CollationIterator15previousSegmentER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIterator9normalizeEPKDsS2_R10UErrorCode
	.type	_ZN6icu_6725FCDUTF16CollationIterator9normalizeEPKDsS2_R10UErrorCode, @function
_ZN6icu_6725FCDUTF16CollationIterator9normalizeEPKDsS2_R10UErrorCode:
.LFB3298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	subq	%rsi, %r8
	sarq	%r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	leaq	456(%rdi), %rcx
	pushq	%r13
	movq	%r14, %r9
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	448(%rdi), %rdi
	call	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_RNS_13UnicodeStringEiR10UErrorCode@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L553
	movq	%r12, %xmm0
	movq	%r13, %xmm1
	movswl	464(%rbx), %eax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 424(%rbx)
	testb	$17, %al
	jne	.L552
	leaq	466(%rbx), %rdx
	testb	$2, %al
	jne	.L547
	movq	480(%rbx), %rdx
.L547:
	movq	%rdx, 392(%rbx)
	testw	%ax, %ax
	js	.L549
	sarl	$5, %eax
.L550:
	cltq
	leaq	(%rdx,%rax,2), %rax
	movq	%rax, 408(%rbx)
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L553:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L549:
	.cfi_restore_state
	movl	468(%rbx), %eax
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L552:
	xorl	%edx, %edx
	jmp	.L547
	.cfi_endproc
.LFE3298:
	.size	_ZN6icu_6725FCDUTF16CollationIterator9normalizeEPKDsS2_R10UErrorCode, .-_ZN6icu_6725FCDUTF16CollationIterator9normalizeEPKDsS2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722UTF16CollationIteratoreqERKNS_17CollationIteratorE
	.type	_ZNK6icu_6722UTF16CollationIteratoreqERKNS_17CollationIteratorE, @function
_ZNK6icu_6722UTF16CollationIteratoreqERKNS_17CollationIteratorE:
.LFB3268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK6icu_6717CollationIteratoreqERKS0_@PLT
	testb	%al, %al
	je	.L555
	movq	400(%r12), %rdx
	movq	400(%rbx), %rax
	subq	392(%r12), %rdx
	subq	392(%rbx), %rax
	cmpq	%rax, %rdx
	sete	%al
.L555:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3268:
	.size	_ZNK6icu_6722UTF16CollationIteratoreqERKNS_17CollationIteratorE, .-_ZNK6icu_6722UTF16CollationIteratoreqERKNS_17CollationIteratorE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725FCDUTF16CollationIteratoreqERKNS_17CollationIteratorE
	.type	_ZNK6icu_6725FCDUTF16CollationIteratoreqERKNS_17CollationIteratorE, @function
_ZNK6icu_6725FCDUTF16CollationIteratoreqERKNS_17CollationIteratorE:
.LFB3285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK6icu_6717CollationIteratoreqERKS0_@PLT
	testb	%al, %al
	je	.L561
	movzbl	520(%r12), %eax
	cmpb	520(%rbx), %al
	je	.L572
	xorl	%eax, %eax
.L561:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	testb	%al, %al
	jne	.L573
	movq	392(%r12), %r8
	movq	424(%r12), %rdx
	movq	392(%rbx), %r9
	movq	424(%rbx), %rcx
	cmpq	%rdx, %r8
	sete	%dil
	cmpq	%rcx, %r9
	sete	%sil
	cmpb	%sil, %dil
	jne	.L561
	movq	416(%r12), %rdi
	movq	416(%rbx), %rsi
	cmpq	%rdx, %r8
	je	.L564
	subq	%rdi, %rdx
	subq	%rsi, %rcx
	cmpq	%rcx, %rdx
	jne	.L561
	movq	400(%r12), %rdx
	movq	400(%rbx), %rax
	subq	%r8, %rdx
	subq	%r9, %rax
	cmpq	%rax, %rdx
	sete	%al
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L573:
	movq	416(%r12), %rdi
	movq	416(%rbx), %rsi
.L564:
	movq	400(%r12), %rdx
	movq	400(%rbx), %rax
	popq	%rbx
	popq	%r12
	subq	%rsi, %rax
	subq	%rdi, %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpq	%rax, %rdx
	sete	%al
	ret
	.cfi_endproc
.LFE3285:
	.size	_ZNK6icu_6725FCDUTF16CollationIteratoreqERKNS_17CollationIteratorE, .-_ZNK6icu_6725FCDUTF16CollationIteratoreqERKNS_17CollationIteratorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIteratorD2Ev
	.type	_ZN6icu_6725FCDUTF16CollationIteratorD2Ev, @function
_ZN6icu_6725FCDUTF16CollationIteratorD2Ev:
.LFB3282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725FCDUTF16CollationIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	456(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -456(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6722UTF16CollationIteratorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717CollationIteratorD2Ev@PLT
	.cfi_endproc
.LFE3282:
	.size	_ZN6icu_6725FCDUTF16CollationIteratorD2Ev, .-_ZN6icu_6725FCDUTF16CollationIteratorD2Ev
	.globl	_ZN6icu_6725FCDUTF16CollationIteratorD1Ev
	.set	_ZN6icu_6725FCDUTF16CollationIteratorD1Ev,_ZN6icu_6725FCDUTF16CollationIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FCDUTF16CollationIteratorD0Ev
	.type	_ZN6icu_6725FCDUTF16CollationIteratorD0Ev, @function
_ZN6icu_6725FCDUTF16CollationIteratorD0Ev:
.LFB3284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725FCDUTF16CollationIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	456(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -456(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6722UTF16CollationIteratorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6717CollationIteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3284:
	.size	_ZN6icu_6725FCDUTF16CollationIteratorD0Ev, .-_ZN6icu_6725FCDUTF16CollationIteratorD0Ev
	.weak	_ZTSN6icu_6722UTF16CollationIteratorE
	.section	.rodata._ZTSN6icu_6722UTF16CollationIteratorE,"aG",@progbits,_ZTSN6icu_6722UTF16CollationIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6722UTF16CollationIteratorE, @object
	.size	_ZTSN6icu_6722UTF16CollationIteratorE, 34
_ZTSN6icu_6722UTF16CollationIteratorE:
	.string	"N6icu_6722UTF16CollationIteratorE"
	.weak	_ZTIN6icu_6722UTF16CollationIteratorE
	.section	.data.rel.ro._ZTIN6icu_6722UTF16CollationIteratorE,"awG",@progbits,_ZTIN6icu_6722UTF16CollationIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6722UTF16CollationIteratorE, @object
	.size	_ZTIN6icu_6722UTF16CollationIteratorE, 24
_ZTIN6icu_6722UTF16CollationIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722UTF16CollationIteratorE
	.quad	_ZTIN6icu_6717CollationIteratorE
	.weak	_ZTSN6icu_6725FCDUTF16CollationIteratorE
	.section	.rodata._ZTSN6icu_6725FCDUTF16CollationIteratorE,"aG",@progbits,_ZTSN6icu_6725FCDUTF16CollationIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6725FCDUTF16CollationIteratorE, @object
	.size	_ZTSN6icu_6725FCDUTF16CollationIteratorE, 37
_ZTSN6icu_6725FCDUTF16CollationIteratorE:
	.string	"N6icu_6725FCDUTF16CollationIteratorE"
	.weak	_ZTIN6icu_6725FCDUTF16CollationIteratorE
	.section	.data.rel.ro._ZTIN6icu_6725FCDUTF16CollationIteratorE,"awG",@progbits,_ZTIN6icu_6725FCDUTF16CollationIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6725FCDUTF16CollationIteratorE, @object
	.size	_ZTIN6icu_6725FCDUTF16CollationIteratorE, 24
_ZTIN6icu_6725FCDUTF16CollationIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725FCDUTF16CollationIteratorE
	.quad	_ZTIN6icu_6722UTF16CollationIteratorE
	.weak	_ZTVN6icu_6722UTF16CollationIteratorE
	.section	.data.rel.ro._ZTVN6icu_6722UTF16CollationIteratorE,"awG",@progbits,_ZTVN6icu_6722UTF16CollationIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6722UTF16CollationIteratorE, @object
	.size	_ZTVN6icu_6722UTF16CollationIteratorE, 144
_ZTVN6icu_6722UTF16CollationIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6722UTF16CollationIteratorE
	.quad	_ZN6icu_6722UTF16CollationIteratorD1Ev
	.quad	_ZN6icu_6722UTF16CollationIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6722UTF16CollationIteratoreqERKNS_17CollationIteratorE
	.quad	_ZN6icu_6722UTF16CollationIterator13resetToOffsetEi
	.quad	_ZNK6icu_6722UTF16CollationIterator9getOffsetEv
	.quad	_ZN6icu_6722UTF16CollationIterator13nextCodePointER10UErrorCode
	.quad	_ZN6icu_6722UTF16CollationIterator17previousCodePointER10UErrorCode
	.quad	_ZN6icu_6722UTF16CollationIterator14handleNextCE32ERiR10UErrorCode
	.quad	_ZN6icu_6722UTF16CollationIterator23handleGetTrailSurrogateEv
	.quad	_ZN6icu_6722UTF16CollationIterator18foundNULTerminatorEv
	.quad	_ZNK6icu_6717CollationIterator25forbidSurrogateCodePointsEv
	.quad	_ZN6icu_6722UTF16CollationIterator20forwardNumCodePointsEiR10UErrorCode
	.quad	_ZN6icu_6722UTF16CollationIterator21backwardNumCodePointsEiR10UErrorCode
	.quad	_ZNK6icu_6717CollationIterator11getDataCE32Ei
	.quad	_ZN6icu_6717CollationIterator22getCE32FromBuilderDataEjR10UErrorCode
	.weak	_ZTVN6icu_6725FCDUTF16CollationIteratorE
	.section	.data.rel.ro._ZTVN6icu_6725FCDUTF16CollationIteratorE,"awG",@progbits,_ZTVN6icu_6725FCDUTF16CollationIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6725FCDUTF16CollationIteratorE, @object
	.size	_ZTVN6icu_6725FCDUTF16CollationIteratorE, 144
_ZTVN6icu_6725FCDUTF16CollationIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6725FCDUTF16CollationIteratorE
	.quad	_ZN6icu_6725FCDUTF16CollationIteratorD1Ev
	.quad	_ZN6icu_6725FCDUTF16CollationIteratorD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6725FCDUTF16CollationIteratoreqERKNS_17CollationIteratorE
	.quad	_ZN6icu_6725FCDUTF16CollationIterator13resetToOffsetEi
	.quad	_ZNK6icu_6725FCDUTF16CollationIterator9getOffsetEv
	.quad	_ZN6icu_6725FCDUTF16CollationIterator13nextCodePointER10UErrorCode
	.quad	_ZN6icu_6725FCDUTF16CollationIterator17previousCodePointER10UErrorCode
	.quad	_ZN6icu_6725FCDUTF16CollationIterator14handleNextCE32ERiR10UErrorCode
	.quad	_ZN6icu_6722UTF16CollationIterator23handleGetTrailSurrogateEv
	.quad	_ZN6icu_6725FCDUTF16CollationIterator18foundNULTerminatorEv
	.quad	_ZNK6icu_6717CollationIterator25forbidSurrogateCodePointsEv
	.quad	_ZN6icu_6725FCDUTF16CollationIterator20forwardNumCodePointsEiR10UErrorCode
	.quad	_ZN6icu_6725FCDUTF16CollationIterator21backwardNumCodePointsEiR10UErrorCode
	.quad	_ZNK6icu_6717CollationIterator11getDataCE32Ei
	.quad	_ZN6icu_6717CollationIterator22getCE32FromBuilderDataEjR10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
