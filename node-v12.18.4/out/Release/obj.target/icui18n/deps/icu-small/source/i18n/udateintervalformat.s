	.file	"udateintervalformat.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6726UFormattedDateIntervalImplC2Ev
	.type	_ZN6icu_6726UFormattedDateIntervalImplC2Ev, @function
_ZN6icu_6726UFormattedDateIntervalImplC2Ev:
.LFB2773:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6721FormattedDateIntervalE(%rip), %rax
	movl	$1430672896, (%rdi)
	movq	%rax, 24(%rdi)
	leaq	24(%rdi), %rax
	movl	$1178880342, 16(%rdi)
	movq	$0, 32(%rdi)
	movl	$27, 40(%rdi)
	movq	%rax, 8(%rdi)
	ret
	.cfi_endproc
.LFE2773:
	.size	_ZN6icu_6726UFormattedDateIntervalImplC2Ev, .-_ZN6icu_6726UFormattedDateIntervalImplC2Ev
	.globl	_ZN6icu_6726UFormattedDateIntervalImplC1Ev
	.set	_ZN6icu_6726UFormattedDateIntervalImplC1Ev,_ZN6icu_6726UFormattedDateIntervalImplC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6726UFormattedDateIntervalImplD2Ev
	.type	_ZN6icu_6726UFormattedDateIntervalImplD2Ev, @function
_ZN6icu_6726UFormattedDateIntervalImplD2Ev:
.LFB2776:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN6icu_6721FormattedDateIntervalD1Ev@PLT
	.cfi_endproc
.LFE2776:
	.size	_ZN6icu_6726UFormattedDateIntervalImplD2Ev, .-_ZN6icu_6726UFormattedDateIntervalImplD2Ev
	.globl	_ZN6icu_6726UFormattedDateIntervalImplD1Ev
	.set	_ZN6icu_6726UFormattedDateIntervalImplD1Ev,_ZN6icu_6726UFormattedDateIntervalImplD2Ev
	.p2align 4
	.globl	udtitvfmt_openResult_67
	.type	udtitvfmt_openResult_67, @function
udtitvfmt_openResult_67:
.LFB2778:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L6
	leaq	16+_ZTVN6icu_6721FormattedDateIntervalE(%rip), %rcx
	leaq	24(%rax), %rdx
	movl	$1430672896, (%rax)
	movl	$1178880342, 16(%rax)
	movq	%rcx, 24(%rax)
	movq	$0, 32(%rax)
	movl	$27, 40(%rax)
	movq	%rdx, 8(%rax)
.L4:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L6:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L4
	.cfi_endproc
.LFE2778:
	.size	udtitvfmt_openResult_67, .-udtitvfmt_openResult_67
	.p2align 4
	.globl	udtitvfmt_resultAsValue_67
	.type	udtitvfmt_resultAsValue_67, @function
udtitvfmt_resultAsValue_67:
.LFB2779:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movq	%rdi, %rax
	testl	%edx, %edx
	jg	.L13
	testq	%rdi, %rdi
	je	.L17
	cmpl	$1178880342, 16(%rdi)
	jne	.L18
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$3, (%rsi)
.L13:
	xorl	%eax, %eax
.L19:
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	jmp	.L19
	.cfi_endproc
.LFE2779:
	.size	udtitvfmt_resultAsValue_67, .-udtitvfmt_resultAsValue_67
	.p2align 4
	.globl	udtitvfmt_closeResult_67
	.type	udtitvfmt_closeResult_67, @function
udtitvfmt_closeResult_67:
.LFB2780:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L26
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$1178880342, 16(%rdi)
	jne	.L20
	leaq	24(%rdi), %rdi
	call	_ZN6icu_6721FormattedDateIntervalD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE2780:
	.size	udtitvfmt_closeResult_67, .-udtitvfmt_closeResult_67
	.p2align 4
	.globl	udtitvfmt_close_67
	.type	udtitvfmt_close_67, @function
udtitvfmt_close_67:
.LFB2784:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L29
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L29:
	ret
	.cfi_endproc
.LFE2784:
	.size	udtitvfmt_close_67, .-udtitvfmt_close_67
	.p2align 4
	.globl	udtitvfmt_formatCalendarToResult_67
	.type	udtitvfmt_formatCalendarToResult_67, @function
udtitvfmt_formatCalendarToResult_67:
.LFB2787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L31
	movq	%rcx, %rbx
	testq	%rcx, %rcx
	je	.L37
	cmpl	$1178880342, 16(%rcx)
	jne	.L38
	leaq	-48(%rbp), %r12
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6718DateIntervalFormat13formatToValueERNS_8CalendarES2_R10UErrorCode@PLT
	leaq	24(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6721FormattedDateIntervalaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6721FormattedDateIntervalD1Ev@PLT
.L31:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L39
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movl	$3, (%r8)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$1, (%r8)
	jmp	.L31
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2787:
	.size	udtitvfmt_formatCalendarToResult_67, .-udtitvfmt_formatCalendarToResult_67
	.p2align 4
	.globl	udtitvfmt_open_67
	.type	udtitvfmt_open_67, @function
udtitvfmt_open_67:
.LFB2781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L40
	movq	%rdi, %r10
	movq	%rcx, %r13
	movl	%r8d, %r11d
	movq	%r9, %rbx
	testq	%rsi, %rsi
	je	.L59
	cmpl	$-1, %edx
	jl	.L43
.L44:
	testq	%r13, %r13
	je	.L60
	cmpl	$-1, %r11d
	jl	.L43
.L46:
	leaq	-360(%rbp), %r9
	movq	%rsi, -360(%rbp)
	xorl	%esi, %esi
	cmpl	$-1, %edx
	leaq	-352(%rbp), %r14
	movl	%edx, %ecx
	sete	%sil
	movq	%r9, %rdx
	movq	%r14, %rdi
	movl	%r11d, -388(%rbp)
	movq	%r10, -376(%rbp)
	movq	%r9, -384(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-376(%rbp), %r10
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-288(%rbp), %r15
	movq	%r10, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6718DateIntervalFormat14createInstanceERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L47
	testq	%r13, %r13
	je	.L48
	movl	-388(%rbp), %r11d
	movq	-384(%rbp), %r9
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r13, -360(%rbp)
	cmpl	$-1, %r11d
	movl	%r11d, %ecx
	movq	%r9, %rdx
	sete	%sil
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r15, %rdi
	call	_ZN6icu_678TimeZone14createTimeZoneERKNS_13UnicodeStringE@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r13, %r13
	je	.L61
	movq	(%r12), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*72(%rax)
.L48:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L62
	addq	$360, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L44
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$7, (%rbx)
	.p2align 4,,10
	.p2align 3
.L47:
	testq	%r12, %r12
	je	.L48
	movq	(%r12), %rax
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*8(%rax)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L60:
	testl	%r11d, %r11d
	je	.L46
	jmp	.L43
.L62:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2781:
	.size	udtitvfmt_open_67, .-udtitvfmt_open_67
	.p2align 4
	.globl	udtitvfmt_format_67
	.type	udtitvfmt_format_67, @function
udtitvfmt_format_67:
.LFB2785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L76
	movq	%rdi, %r10
	movq	%rsi, %rbx
	movl	%edx, %r13d
	movq	%rcx, %r9
	movq	%r8, %r12
	testq	%rsi, %rsi
	je	.L79
	testl	%edx, %edx
	js	.L66
	movl	$2, %edx
	leaq	-128(%rbp), %r14
	movq	%rcx, -240(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdi, -216(%rbp)
	movl	%r13d, %ecx
	movq	%r14, %rdi
	movw	%dx, -120(%rbp)
	xorl	%edx, %edx
	movsd	%xmm1, -232(%rbp)
	movsd	%xmm0, -224(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movq	-216(%rbp), %r10
	movsd	-224(%rbp), %xmm0
	movsd	-232(%rbp), %xmm1
	movq	-240(%rbp), %r9
.L74:
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movl	$0, -176(%rbp)
	movq	%rax, -192(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -184(%rbp)
	testq	%r9, %r9
	je	.L69
	movl	(%r9), %eax
	leaq	-160(%rbp), %r15
	movq	%r9, -224(%rbp)
	movq	%r15, %rdi
	movq	%r10, -232(%rbp)
	movl	%eax, -184(%rbp)
	call	_ZN6icu_6712DateIntervalC1Edd@PLT
	movq	%r15, %rsi
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	-232(%rbp), %r10
	leaq	-192(%rbp), %rax
	movq	%rax, %rcx
	movq	%rax, -216(%rbp)
	movq	%r10, %rdi
	call	_ZNK6icu_6718DateIntervalFormat6formatEPKNS_12DateIntervalERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode@PLT
	movl	(%r12), %esi
	movq	-224(%rbp), %r9
	testl	%esi, %esi
	jg	.L72
	movq	-180(%rbp), %rax
	movq	%rax, 4(%r9)
.L73:
	movq	%r12, %rcx
	leaq	-200(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%rbx, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
.L71:
	movq	%r15, %rdi
	call	_ZN6icu_6712DateIntervalD1Ev@PLT
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L63:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$200, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	$1, (%r12)
	xorl	%r12d, %r12d
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L79:
	testl	%edx, %edx
	jne	.L66
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r14
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	-160(%rbp), %r15
	movq	%r10, -224(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_6712DateIntervalC1Edd@PLT
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	-224(%rbp), %r10
	leaq	-192(%rbp), %rax
	movq	%rax, %rcx
	movq	%rax, -216(%rbp)
	movq	%r10, %rdi
	call	_ZNK6icu_6718DateIntervalFormat6formatEPKNS_12DateIntervalERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L73
.L72:
	movl	$-1, %r12d
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L76:
	movl	$-1, %r12d
	jmp	.L63
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2785:
	.size	udtitvfmt_format_67, .-udtitvfmt_format_67
	.p2align 4
	.globl	udtitvfmt_formatToResult_67
	.type	udtitvfmt_formatToResult_67, @function
udtitvfmt_formatToResult_67:
.LFB2786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L81
	movq	%rsi, %rbx
	movq	%rdx, %r12
	testq	%rsi, %rsi
	je	.L89
	cmpl	$1178880342, 16(%rsi)
	jne	.L90
	leaq	-112(%rbp), %r14
	leaq	-80(%rbp), %r15
	movq	%rdi, %r13
	movq	%r14, %rdi
	call	_ZN6icu_6712DateIntervalC1Edd@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6718DateIntervalFormat13formatToValueERKNS_12DateIntervalER10UErrorCode@PLT
	leaq	24(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6721FormattedDateIntervalaSEOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6721FormattedDateIntervalD1Ev@PLT
.L85:
	movq	%r14, %rdi
	call	_ZN6icu_6712DateIntervalD1Ev@PLT
.L81:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L91
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movl	$3, (%rdx)
	leaq	-112(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6712DateIntervalC1Edd@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L89:
	movl	$1, (%rdx)
	leaq	-112(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6712DateIntervalC1Edd@PLT
	jmp	.L85
.L91:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2786:
	.size	udtitvfmt_formatToResult_67, .-udtitvfmt_formatToResult_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
