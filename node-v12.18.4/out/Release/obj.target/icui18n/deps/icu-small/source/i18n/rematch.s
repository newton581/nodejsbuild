	.file	"rematch.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher5end64ER10UErrorCode
	.type	_ZNK6icu_6712RegexMatcher5end64ER10UErrorCode, @function
_ZNK6icu_6712RegexMatcher5end64ER10UErrorCode:
.LFB3251:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L5
	cmpb	$0, 130(%rdi)
	je	.L7
	movq	8(%rdi), %rax
	movq	136(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	js	.L8
	movq	144(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	$-1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$66306, (%rsi)
	movq	$-1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$8, (%rsi)
	movq	$-1, %rax
	ret
	.cfi_endproc
.LFE3251:
	.size	_ZNK6icu_6712RegexMatcher5end64ER10UErrorCode, .-_ZNK6icu_6712RegexMatcher5end64ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher10groupCountEv
	.type	_ZNK6icu_6712RegexMatcher10groupCountEv, @function
_ZNK6icu_6712RegexMatcher10groupCountEv:
.LFB3264:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	136(%rax), %rax
	movl	8(%rax), %eax
	ret
	.cfi_endproc
.LFE3264:
	.size	_ZNK6icu_6712RegexMatcher10groupCountEv, .-_ZNK6icu_6712RegexMatcher10groupCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher18hasAnchoringBoundsEv
	.type	_ZNK6icu_6712RegexMatcher18hasAnchoringBoundsEv, @function
_ZNK6icu_6712RegexMatcher18hasAnchoringBoundsEv:
.LFB3265:
	.cfi_startproc
	endbr64
	movzbl	129(%rdi), %eax
	ret
	.cfi_endproc
.LFE3265:
	.size	_ZNK6icu_6712RegexMatcher18hasAnchoringBoundsEv, .-_ZNK6icu_6712RegexMatcher18hasAnchoringBoundsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher20hasTransparentBoundsEv
	.type	_ZNK6icu_6712RegexMatcher20hasTransparentBoundsEv, @function
_ZNK6icu_6712RegexMatcher20hasTransparentBoundsEv:
.LFB3266:
	.cfi_startproc
	endbr64
	movzbl	128(%rdi), %eax
	ret
	.cfi_endproc
.LFE3266:
	.size	_ZNK6icu_6712RegexMatcher20hasTransparentBoundsEv, .-_ZNK6icu_6712RegexMatcher20hasTransparentBoundsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher6hitEndEv
	.type	_ZNK6icu_6712RegexMatcher6hitEndEv, @function
_ZNK6icu_6712RegexMatcher6hitEndEv:
.LFB3267:
	.cfi_startproc
	endbr64
	movzbl	168(%rdi), %eax
	ret
	.cfi_endproc
.LFE3267:
	.size	_ZNK6icu_6712RegexMatcher6hitEndEv, .-_ZNK6icu_6712RegexMatcher6hitEndEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher9inputTextEv
	.type	_ZNK6icu_6712RegexMatcher9inputTextEv, @function
_ZNK6icu_6712RegexMatcher9inputTextEv:
.LFB3269:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE3269:
	.size	_ZNK6icu_6712RegexMatcher9inputTextEv, .-_ZNK6icu_6712RegexMatcher9inputTextEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher7patternEv
	.type	_ZNK6icu_6712RegexMatcher7patternEv, @function
_ZNK6icu_6712RegexMatcher7patternEv:
.LFB3276:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE3276:
	.size	_ZNK6icu_6712RegexMatcher7patternEv, .-_ZNK6icu_6712RegexMatcher7patternEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher9regionEndEv
	.type	_ZNK6icu_6712RegexMatcher9regionEndEv, @function
_ZNK6icu_6712RegexMatcher9regionEndEv:
.LFB3279:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE3279:
	.size	_ZNK6icu_6712RegexMatcher9regionEndEv, .-_ZNK6icu_6712RegexMatcher9regionEndEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher11regionEnd64Ev
	.type	_ZNK6icu_6712RegexMatcher11regionEnd64Ev, @function
_ZNK6icu_6712RegexMatcher11regionEnd64Ev:
.LFB3280:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE3280:
	.size	_ZNK6icu_6712RegexMatcher11regionEnd64Ev, .-_ZNK6icu_6712RegexMatcher11regionEnd64Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher11regionStartEv
	.type	_ZNK6icu_6712RegexMatcher11regionStartEv, @function
_ZNK6icu_6712RegexMatcher11regionStartEv:
.LFB3281:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE3281:
	.size	_ZNK6icu_6712RegexMatcher11regionStartEv, .-_ZNK6icu_6712RegexMatcher11regionStartEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher13regionStart64Ev
	.type	_ZNK6icu_6712RegexMatcher13regionStart64Ev, @function
_ZNK6icu_6712RegexMatcher13regionStart64Ev:
.LFB3282:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE3282:
	.size	_ZNK6icu_6712RegexMatcher13regionStart64Ev, .-_ZNK6icu_6712RegexMatcher13regionStart64Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher10requireEndEv
	.type	_ZNK6icu_6712RegexMatcher10requireEndEv, @function
_ZNK6icu_6712RegexMatcher10requireEndEv:
.LFB3287:
	.cfi_startproc
	endbr64
	movzbl	169(%rdi), %eax
	ret
	.cfi_endproc
.LFE3287:
	.size	_ZNK6icu_6712RegexMatcher10requireEndEv, .-_ZNK6icu_6712RegexMatcher10requireEndEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher5resetEv
	.type	_ZN6icu_6712RegexMatcher5resetEv, @function
_ZN6icu_6712RegexMatcher5resetEv:
.LFB3288:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdx
	pxor	%xmm0, %xmm0
	movabsq	$42949672960000, %rcx
	movq	$0, 64(%rdi)
	movups	%xmm0, 136(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movq	%rdi, %rax
	movq	%rdx, 72(%rdi)
	movq	%rdx, 120(%rdi)
	movq	%rdx, 88(%rdi)
	movq	%rdx, 104(%rdi)
	xorl	%edx, %edx
	movq	$0, 112(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 96(%rdi)
	movb	$0, 130(%rdi)
	movw	%dx, 168(%rdi)
	movq	%rcx, 268(%rdi)
	movups	%xmm0, 152(%rdi)
	ret
	.cfi_endproc
.LFE3288:
	.size	_ZN6icu_6712RegexMatcher5resetEv, .-_ZN6icu_6712RegexMatcher5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher18useAnchoringBoundsEa
	.type	_ZN6icu_6712RegexMatcher18useAnchoringBoundsEa, @function
_ZN6icu_6712RegexMatcher18useAnchoringBoundsEa:
.LFB3302:
	.cfi_startproc
	endbr64
	movb	%sil, 129(%rdi)
	movq	%rdi, %rax
	testb	%sil, %sil
	je	.L22
	movq	72(%rdi), %rdx
	movq	64(%rdi), %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 80(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movq	48(%rdi), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 80(%rax)
	ret
	.cfi_endproc
.LFE3302:
	.size	_ZN6icu_6712RegexMatcher18useAnchoringBoundsEa, .-_ZN6icu_6712RegexMatcher18useAnchoringBoundsEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher20useTransparentBoundsEa
	.type	_ZN6icu_6712RegexMatcher20useTransparentBoundsEa, @function
_ZN6icu_6712RegexMatcher20useTransparentBoundsEa:
.LFB3303:
	.cfi_startproc
	endbr64
	movb	%sil, 128(%rdi)
	movq	%rdi, %rax
	testb	%sil, %sil
	jne	.L26
	movq	72(%rdi), %rdx
	movq	64(%rdi), %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 96(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	48(%rdi), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 96(%rax)
	ret
	.cfi_endproc
.LFE3303:
	.size	_ZN6icu_6712RegexMatcher20useTransparentBoundsEa, .-_ZN6icu_6712RegexMatcher20useTransparentBoundsEa
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher12getTimeLimitEv
	.type	_ZNK6icu_6712RegexMatcher12getTimeLimitEv, @function
_ZNK6icu_6712RegexMatcher12getTimeLimitEv:
.LFB3305:
	.cfi_startproc
	endbr64
	movl	264(%rdi), %eax
	ret
	.cfi_endproc
.LFE3305:
	.size	_ZNK6icu_6712RegexMatcher12getTimeLimitEv, .-_ZNK6icu_6712RegexMatcher12getTimeLimitEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher13getStackLimitEv
	.type	_ZNK6icu_6712RegexMatcher13getStackLimitEv, @function
_ZNK6icu_6712RegexMatcher13getStackLimitEv:
.LFB3307:
	.cfi_startproc
	endbr64
	movl	276(%rdi), %eax
	ret
	.cfi_endproc
.LFE3307:
	.size	_ZNK6icu_6712RegexMatcher13getStackLimitEv, .-_ZNK6icu_6712RegexMatcher13getStackLimitEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher16setMatchCallbackEPFaPKviES2_R10UErrorCode
	.type	_ZN6icu_6712RegexMatcher16setMatchCallbackEPFaPKviES2_R10UErrorCode, @function
_ZN6icu_6712RegexMatcher16setMatchCallbackEPFaPKviES2_R10UErrorCode:
.LFB3308:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L31
	movq	%rsi, 280(%rdi)
	movq	%rdx, 288(%rdi)
.L31:
	ret
	.cfi_endproc
.LFE3308:
	.size	_ZN6icu_6712RegexMatcher16setMatchCallbackEPFaPKviES2_R10UErrorCode, .-_ZN6icu_6712RegexMatcher16setMatchCallbackEPFaPKviES2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher16getMatchCallbackERPFaPKviERS2_R10UErrorCode
	.type	_ZN6icu_6712RegexMatcher16getMatchCallbackERPFaPKviERS2_R10UErrorCode, @function
_ZN6icu_6712RegexMatcher16getMatchCallbackERPFaPKviERS2_R10UErrorCode:
.LFB3309:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L33
	movq	280(%rdi), %rax
	movq	%rax, (%rsi)
	movq	288(%rdi), %rax
	movq	%rax, (%rdx)
.L33:
	ret
	.cfi_endproc
.LFE3309:
	.size	_ZN6icu_6712RegexMatcher16getMatchCallbackERPFaPKviERS2_R10UErrorCode, .-_ZN6icu_6712RegexMatcher16getMatchCallbackERPFaPKviERS2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher23setFindProgressCallbackEPFaPKvlES2_R10UErrorCode
	.type	_ZN6icu_6712RegexMatcher23setFindProgressCallbackEPFaPKvlES2_R10UErrorCode, @function
_ZN6icu_6712RegexMatcher23setFindProgressCallbackEPFaPKvlES2_R10UErrorCode:
.LFB3310:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L35
	movq	%rsi, 296(%rdi)
	movq	%rdx, 304(%rdi)
.L35:
	ret
	.cfi_endproc
.LFE3310:
	.size	_ZN6icu_6712RegexMatcher23setFindProgressCallbackEPFaPKvlES2_R10UErrorCode, .-_ZN6icu_6712RegexMatcher23setFindProgressCallbackEPFaPKvlES2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher23getFindProgressCallbackERPFaPKvlERS2_R10UErrorCode
	.type	_ZN6icu_6712RegexMatcher23getFindProgressCallbackERPFaPKvlERS2_R10UErrorCode, @function
_ZN6icu_6712RegexMatcher23getFindProgressCallbackERPFaPKvlERS2_R10UErrorCode:
.LFB3311:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L37
	movq	296(%rdi), %rax
	movq	%rax, (%rsi)
	movq	304(%rdi), %rax
	movq	%rax, (%rdx)
.L37:
	ret
	.cfi_endproc
.LFE3311:
	.size	_ZN6icu_6712RegexMatcher23getFindProgressCallbackERPFaPKvlERS2_R10UErrorCode, .-_ZN6icu_6712RegexMatcher23getFindProgressCallbackERPFaPKvlERS2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher17getDynamicClassIDEv
	.type	_ZNK6icu_6712RegexMatcher17getDynamicClassIDEv, @function
_ZNK6icu_6712RegexMatcher17getDynamicClassIDEv:
.LFB3322:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712RegexMatcher16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3322:
	.size	_ZNK6icu_6712RegexMatcher17getDynamicClassIDEv, .-_ZNK6icu_6712RegexMatcher17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher5resetERKNS_13UnicodeStringE
	.type	_ZN6icu_6712RegexMatcher5resetERKNS_13UnicodeStringE, @function
_ZN6icu_6712RegexMatcher5resetERKNS_13UnicodeStringE:
.LFB3290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	316(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	32(%rdi), %rdi
	movq	%r13, %rdx
	call	utext_openConstUnicodeString_67@PLT
	movq	8(%r12), %rdx
	movq	%rax, 32(%r12)
	cmpb	$0, 184(%rdx)
	jne	.L57
.L41:
	movl	316(%r12), %edx
	testl	%edx, %edx
	jle	.L58
.L43:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	32(%r12), %rdi
	call	utext_nativeLength_67@PLT
	pxor	%xmm0, %xmm0
	movq	24(%r12), %rdi
	movq	$0, 64(%r12)
	movq	%rax, 48(%r12)
	movq	%rax, 72(%r12)
	movq	%rax, 120(%r12)
	movq	%rax, 88(%r12)
	movq	%rax, 104(%r12)
	xorl	%eax, %eax
	movups	%xmm0, 136(%r12)
	movdqa	.LC0(%rip), %xmm0
	movw	%ax, 168(%r12)
	movabsq	$42949672960000, %rax
	movq	$0, 112(%r12)
	movq	$0, 80(%r12)
	movq	$0, 96(%r12)
	movb	$0, 130(%r12)
	movq	%rax, 268(%r12)
	movups	%xmm0, 152(%r12)
	testq	%rdi, %rdi
	je	.L44
	movq	(%rdi), %rax
	call	*8(%rax)
.L44:
	movq	320(%r12), %rdi
	movq	$0, 24(%r12)
	movb	$1, 312(%r12)
	testq	%rdi, %rdi
	je	.L45
	movq	(%rdi), %rax
	movq	32(%r12), %rsi
	movq	%r13, %rdx
	call	*64(%rax)
.L45:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L43
	movq	(%rdi), %rax
	movq	32(%r12), %rsi
	movq	%r13, %rdx
	call	*64(%rax)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	40(%r12), %rdi
	movq	%r13, %r8
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	utext_clone_67@PLT
	movq	%rax, 40(%r12)
	jmp	.L41
	.cfi_endproc
.LFE3290:
	.size	_ZN6icu_6712RegexMatcher5resetERKNS_13UnicodeStringE, .-_ZN6icu_6712RegexMatcher5resetERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher5resetEP5UText
	.type	_ZN6icu_6712RegexMatcher5resetEP5UText, @function
_ZN6icu_6712RegexMatcher5resetEP5UText:
.LFB3291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	cmpq	%rsi, %rdi
	je	.L61
	leaq	316(%r12), %r14
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%rsi, %r13
	movq	%r14, %r8
	call	utext_clone_67@PLT
	movq	8(%r12), %rdx
	movq	%rax, 32(%r12)
	cmpb	$0, 184(%rdx)
	jne	.L77
	movl	316(%r12), %edx
	testl	%edx, %edx
	jle	.L78
.L63:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movq	32(%r12), %rdi
	call	utext_nativeLength_67@PLT
	movq	24(%r12), %rdi
	movq	%rax, 48(%r12)
	testq	%rdi, %rdi
	je	.L64
	movq	(%rdi), %rax
	call	*8(%rax)
.L64:
	movq	320(%r12), %rdi
	movq	$0, 24(%r12)
	testq	%rdi, %rdi
	je	.L65
	movq	(%rdi), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	*64(%rax)
.L65:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L61
	movq	(%rdi), %rax
	movq	32(%r12), %rsi
	movq	%r14, %rdx
	call	*64(%rax)
.L61:
	movq	48(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 64(%r12)
	movups	%xmm0, 136(%r12)
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, 72(%r12)
	movq	%rax, 120(%r12)
	movq	%rax, 88(%r12)
	movq	%rax, 104(%r12)
	xorl	%eax, %eax
	movw	%ax, 168(%r12)
	movabsq	$42949672960000, %rax
	movq	%rax, 268(%r12)
	movq	%r12, %rax
	movq	$0, 112(%r12)
	movq	$0, 80(%r12)
	movq	$0, 96(%r12)
	movb	$0, 130(%r12)
	movb	$0, 312(%r12)
	movups	%xmm0, 152(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	40(%r12), %rdi
	xorl	%edx, %edx
	movq	%r14, %r8
	movl	$1, %ecx
	movq	%rax, %rsi
	call	utext_clone_67@PLT
	movl	316(%r12), %edx
	movq	%rax, 40(%r12)
	testl	%edx, %edx
	jg	.L63
	jmp	.L78
	.cfi_endproc
.LFE3291:
	.size	_ZN6icu_6712RegexMatcher5resetEP5UText, .-_ZN6icu_6712RegexMatcher5resetEP5UText
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher5inputEv
	.type	_ZNK6icu_6712RegexMatcher5inputEv, @function
_ZNK6icu_6712RegexMatcher5inputEv:
.LFB3268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L89
.L79:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	movl	$0, -44(%rbp)
	movq	48(%rbx), %rdx
	movq	56(%rdi), %rcx
	cmpq	$0, 72(%rcx)
	je	.L91
	leaq	-44(%rbp), %r14
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r14, %r9
	call	utext_extract_67@PLT
	movl	$0, -44(%rbp)
	movl	%eax, %r13d
.L82:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L83
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1Eiii@PLT
.L83:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	48(%rbx), %rdx
	movq	32(%rbx), %rdi
	movq	%r14, %r9
	movq	%rax, %rcx
	movl	%r13d, %r8d
	xorl	%esi, %esi
	call	utext_extract_67@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movq	%r12, 24(%rbx)
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L91:
	movl	%edx, %r13d
	leaq	-44(%rbp), %r14
	jmp	.L82
.L90:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3268:
	.size	_ZNK6icu_6712RegexMatcher5inputEv, .-_ZNK6icu_6712RegexMatcher5inputEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher12setTimeLimitEiR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher12setTimeLimitEiR10UErrorCode, @function
_ZN6icu_6712RegexMatcher12setTimeLimitEiR10UErrorCode:
.LFB3304:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L92
	movl	316(%rdi), %eax
	testl	%eax, %eax
	jg	.L96
	testl	%esi, %esi
	js	.L97
	movl	%esi, 264(%rdi)
.L92:
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	movl	%eax, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$1, (%rdx)
	ret
	.cfi_endproc
.LFE3304:
	.size	_ZN6icu_6712RegexMatcher12setTimeLimitEiR10UErrorCode, .-_ZN6icu_6712RegexMatcher12setTimeLimitEiR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode.part.0, @function
_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode.part.0:
.LFB4469:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	48(%rdi), %rdx
	movq	160(%rdi), %rsi
	cmpq	%rsi, %rdx
	jle	.L98
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	cmpq	$0, 32(%rdi)
	je	.L105
.L100:
	movq	56(%rdi), %rax
	cmpq	$0, 72(%rax)
	je	.L106
	movq	%r15, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	utext_extract_67@PLT
	movl	$0, (%r15)
	movl	%eax, %r13d
.L102:
	movslq	%r13d, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L107
	movq	48(%rbx), %rdx
	movq	32(%rbx), %rdi
	movq	%r15, %r9
	movl	%r13d, %r8d
	movq	160(%rbx), %rsi
	movq	%rax, %rcx
	call	utext_extract_67@PLT
	movq	%r14, %rdi
	call	utext_nativeLength_67@PLT
	movq	%r14, %rdi
	movq	%r15, %r9
	movl	%r13d, %r8d
	movq	%r12, %rcx
	movq	%rax, %rsi
	movq	%rax, %rdx
	call	utext_replace_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
.L107:
	.cfi_restore_state
	movl	$7, 316(%rbx)
.L98:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	cmpq	16(%rdi), %rdx
	jne	.L100
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jne	.L100
	movq	%r14, %rdi
	call	utext_nativeLength_67@PLT
	movq	32(%rbx), %rdx
	movq	48(%rbx), %r8
	movq	%r15, %r9
	movq	%rax, %rsi
	movq	160(%rbx), %rax
	movq	%r14, %rdi
	movq	48(%rdx), %rdx
	addq	$8, %rsp
	popq	%rbx
	subl	%eax, %r8d
	popq	%r12
	leaq	(%rdx,%rax,2), %rcx
	popq	%r13
	movq	%rsi, %rdx
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	utext_replace_67@PLT
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movl	%edx, %r13d
	subl	%esi, %r13d
	jmp	.L102
	.cfi_endproc
.LFE4469:
	.size	_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode.part.0, .-_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher10appendTailERNS_13UnicodeStringE
	.type	_ZN6icu_6712RegexMatcher10appendTailERNS_13UnicodeStringE, @function
_ZN6icu_6712RegexMatcher10appendTailERNS_13UnicodeStringE:
.LFB3248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$18, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-196(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-192(%rbp), %rdi
	movq	%r15, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$176, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -196(%rbp)
	rep stosq
	movq	%r14, %rdi
	movl	$878368812, -192(%rbp)
	movl	$144, -180(%rbp)
	call	utext_openUnicodeString_67@PLT
	movl	-196(%rbp), %eax
	testl	%eax, %eax
	jle	.L114
.L109:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$176, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movl	316(%r13), %eax
	testl	%eax, %eax
	jg	.L116
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode.part.0
.L111:
	movq	%r14, %rdi
	call	utext_close_67@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L116:
	movl	%eax, -196(%rbp)
	jmp	.L111
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3248:
	.size	_ZN6icu_6712RegexMatcher10appendTailERNS_13UnicodeStringE, .-_ZN6icu_6712RegexMatcher10appendTailERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher8getInputEP5UTextR10UErrorCode
	.type	_ZNK6icu_6712RegexMatcher8getInputEP5UTextR10UErrorCode, @function
_ZNK6icu_6712RegexMatcher8getInputEP5UTextR10UErrorCode:
.LFB3270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L131
	movl	316(%rdi), %eax
	movq	%rdi, %r14
	movq	%rdx, %r12
	testl	%eax, %eax
	jg	.L132
	movq	32(%rdi), %rdi
	testq	%rsi, %rsi
	je	.L121
	cmpq	$0, 32(%rdi)
	movq	48(%r14), %rdx
	jne	.L122
	cmpq	16(%rdi), %rdx
	je	.L133
.L122:
	movq	56(%rdi), %rax
	movl	%edx, %r13d
	cmpq	$0, 72(%rax)
	je	.L124
	leaq	-60(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	$0, -60(%rbp)
	call	utext_extract_67@PLT
	movl	%eax, %r13d
.L124:
	movslq	%r13d, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L131
	movl	$0, (%r12)
	movq	%r12, %r9
	movl	%r13d, %r8d
	xorl	%esi, %esi
	movq	48(%r14), %rdx
	movq	32(%r14), %rdi
	movq	%rax, %rcx
	call	utext_extract_67@PLT
	movl	$0, (%r12)
	movq	%rbx, %rdi
	call	utext_nativeLength_67@PLT
	movq	%r12, %r9
	movl	%r13d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	utext_replace_67@PLT
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	movq	%rbx, %rax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L132:
	movl	%eax, (%rdx)
	movq	%rsi, %rax
.L117:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L134
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jne	.L122
	movq	48(%rdi), %r13
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	call	utext_nativeLength_67@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %r9
	xorl	%esi, %esi
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movl	%edx, %r8d
	movq	%rax, %rdx
	call	utext_replace_67@PLT
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%rbx, %rax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%rdx, %r8
	movq	%rdi, %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	utext_clone_67@PLT
	jmp	.L117
.L134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3270:
	.size	_ZNK6icu_6712RegexMatcher8getInputEP5UTextR10UErrorCode, .-_ZNK6icu_6712RegexMatcher8getInputEP5UTextR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher16refreshInputTextEP5UTextR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher16refreshInputTextEP5UTextR10UErrorCode, @function
_ZN6icu_6712RegexMatcher16refreshInputTextEP5UTextR10UErrorCode:
.LFB3293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L137
	movq	%rsi, %r13
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	je	.L139
	movq	32(%rdi), %rdi
	call	utext_nativeLength_67@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	utext_nativeLength_67@PLT
	cmpq	%rax, %r14
	je	.L145
.L139:
	movl	$1, (%rbx)
.L137:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	32(%r12), %rdi
	call	utext_getNativeIndex_67@PLT
	movq	32(%r12), %rdi
	xorl	%edx, %edx
	movq	%rbx, %r8
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%rax, %r14
	call	utext_clone_67@PLT
	movl	(%rbx), %edx
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	testl	%edx, %edx
	jg	.L137
	movq	%r14, %rsi
	call	utext_setNativeIndex_67@PLT
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L137
	call	utext_getNativeIndex_67@PLT
	movq	40(%r12), %rdi
	movq	%rbx, %r8
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%rax, %r14
	call	utext_clone_67@PLT
	movq	%rax, 40(%r12)
	movq	%rax, %rdi
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L137
	movq	%r14, %rsi
	call	utext_setNativeIndex_67@PLT
	jmp	.L137
	.cfi_endproc
.LFE3293:
	.size	_ZN6icu_6712RegexMatcher16refreshInputTextEP5UTextR10UErrorCode, .-_ZN6icu_6712RegexMatcher16refreshInputTextEP5UTextR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode.part.0, @function
_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode.part.0:
.LFB4485:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -184(%rbp)
	movq	%rsi, %rdi
	movq	%rsi, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	utext_nativeLength_67@PLT
	movq	136(%r14), %rdx
	movq	160(%r14), %rsi
	movq	%rax, %rbx
	cmpq	%rsi, %rdx
	jle	.L147
	movq	32(%r14), %rdi
	cmpq	$0, 32(%rdi)
	je	.L262
.L148:
	movq	56(%rdi), %rax
	cmpq	$0, 72(%rax)
	je	.L263
	leaq	-144(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$0, -144(%rbp)
	call	utext_extract_67@PLT
	movl	%eax, %r13d
.L150:
	leal	1(%r13), %r8d
	movslq	%r8d, %rdi
	movl	%r8d, -172(%rbp)
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movl	-172(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L264
	movq	-184(%rbp), %rax
	movq	%r12, %r9
	movq	%r14, %rcx
	movq	136(%rax), %rdx
	movq	160(%rax), %rsi
	movq	32(%rax), %rdi
	call	utext_extract_67@PLT
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%r12, %r9
	movq	-168(%rbp), %rdi
	movl	%r13d, %r8d
	movq	%r14, %rcx
	call	utext_replace_67@PLT
	movq	%r14, %rdi
	cltq
	addq	%rax, %rbx
	call	uprv_free_67@PLT
.L147:
	movq	-184(%rbp), %rsi
	movq	144(%rsi), %rax
	movq	%rax, 160(%rsi)
	movq	32(%r15), %rax
	testq	%rax, %rax
	jg	.L153
	movslq	28(%r15), %rdx
	negq	%rax
	cmpq	%rdx, %rax
	jl	.L265
.L153:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	utext_setNativeIndex_67@PLT
	movl	40(%r15), %edx
.L154:
	cmpl	%edx, 44(%r15)
	jle	.L155
	movq	48(%r15), %rcx
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %eax
	cmpw	$-10241, %ax
	jbe	.L266
.L155:
	movq	%r15, %rdi
	call	utext_next32_67@PLT
	cmpl	$-1, %eax
	setne	%cl
.L158:
	movl	(%r12), %r10d
	testl	%r10d, %r10d
	setle	%dl
	andb	%cl, %dl
	movb	%dl, -172(%rbp)
	jne	.L156
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L159:
	cmpl	$36, %eax
	je	.L178
	cmpl	$65535, %eax
	ja	.L179
	movw	%ax, -144(%rbp)
	leaq	-144(%rbp), %rcx
	movq	%r12, %r9
	movl	$1, %r8d
.L261:
	movq	-168(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	call	utext_replace_67@PLT
	movl	40(%r15), %edx
	cltq
	addq	%rax, %rbx
.L173:
	cmpl	%edx, 44(%r15)
	jle	.L214
.L269:
	movq	48(%r15), %rcx
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %eax
	cmpw	$-10241, %ax
	ja	.L214
	movl	(%r12), %r9d
	addl	$1, %edx
	movl	%edx, 40(%r15)
	movzbl	-172(%rbp), %edx
	testl	%r9d, %r9d
	jg	.L146
.L270:
	testb	%dl, %dl
	je	.L146
.L156:
	cmpl	$92, %eax
	jne	.L159
	movl	40(%r15), %edx
	cmpl	44(%r15), %edx
	jge	.L160
	movq	48(%r15), %rcx
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %r13d
	cmpw	$-10241, %r13w
	ja	.L160
	movl	%r13d, %eax
	andl	$-33, %eax
	cmpw	$85, %ax
	je	.L161
.L162:
	addl	$1, %edx
	movl	%edx, 40(%r15)
	cmpl	$65535, %r13d
	jbe	.L267
.L176:
	movl	%r13d, %eax
	movl	(%r12), %edi
	andw	$1023, %r13w
	sarl	$10, %eax
	orw	$-9216, %r13w
	subw	$10304, %ax
	movw	%r13w, -126(%rbp)
	movw	%ax, -128(%rbp)
	testl	%edi, %edi
	jle	.L268
.L202:
	movl	40(%r15), %edx
	cmpl	%edx, 44(%r15)
	jg	.L269
.L214:
	movq	%r15, %rdi
	call	utext_next32_67@PLT
	movl	(%r12), %r9d
	cmpl	$-1, %eax
	setne	%dl
	testl	%r9d, %r9d
	jle	.L270
.L146:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L271
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movl	%eax, %edx
	andw	$1023, %ax
	leaq	-128(%rbp), %r14
	movq	%r12, %r9
	sarl	$10, %edx
	orw	$-9216, %ax
	movl	$2, %r8d
	movq	%r14, %rcx
	subw	$10304, %dx
	movw	%ax, -126(%rbp)
	movw	%dx, -128(%rbp)
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%r15, %rdi
	call	utext_current32_67@PLT
	movl	%eax, %r13d
	cmpl	$-1, %eax
	je	.L146
	andl	$-33, %eax
	cmpl	$85, %eax
	je	.L161
	movl	40(%r15), %edx
	cmpl	44(%r15), %edx
	jge	.L174
	movq	48(%r15), %rcx
	movslq	%edx, %rax
	cmpw	$-10241, (%rcx,%rax,2)
	jbe	.L162
.L174:
	movq	%r15, %rdi
	call	utext_next32_67@PLT
	cmpl	$65535, %r13d
	ja	.L176
.L267:
	leaq	-144(%rbp), %rcx
	movq	%r12, %r9
	movw	%r13w, -144(%rbp)
	movl	$1, %r8d
.L260:
	movq	-168(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	call	utext_replace_67@PLT
	cltq
	addq	%rax, %rbx
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%r15, %rdi
	call	utext_current32_67@PLT
	movl	%eax, %edi
	cmpl	$123, %eax
	je	.L272
	call	u_isdigit_67@PLT
	testb	%al, %al
	jne	.L273
	movl	$66325, (%r12)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L161:
	movq	uregex_utext_unescape_charAt_67@GOTPCREL(%rip), %rdi
	leaq	-144(%rbp), %rcx
	leaq	-148(%rbp), %rsi
	movl	$2147483647, %edx
	movl	$0, -148(%rbp)
	movl	$0, -132(%rbp)
	movl	$-1, -136(%rbp)
	movq	%r15, -144(%rbp)
	call	u_unescapeAt_67@PLT
	cmpl	$-1, %eax
	je	.L202
	cmpl	$65535, %eax
	jbe	.L274
	movl	%eax, %edx
	movl	(%r12), %r8d
	andw	$1023, %ax
	sarl	$10, %edx
	orw	$-9216, %ax
	subw	$10304, %dx
	movw	%ax, -126(%rbp)
	movw	%dx, -128(%rbp)
	testl	%r8d, %r8d
	jle	.L275
.L168:
	movl	-136(%rbp), %eax
	movl	-148(%rbp), %esi
	cmpl	%esi, %eax
	je	.L276
	leal	-1(%rsi), %edx
	cmpl	%edx, %eax
	je	.L202
	subl	%eax, %esi
	movq	%r15, %rdi
	subl	$1, %esi
	call	utext_moveIndex32_67@PLT
	movl	40(%r15), %edx
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L266:
	addl	$1, %edx
	movl	$1, %ecx
	movl	%edx, 40(%r15)
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L265:
	movq	48(%r15), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L153
	movl	%eax, 40(%r15)
	movl	%eax, %edx
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L262:
	movq	48(%r14), %rax
	cmpq	16(%rdi), %rax
	jne	.L148
	movslq	28(%rdi), %rcx
	cmpq	%rcx, %rax
	jne	.L148
	movq	48(%rdi), %rax
	movl	%edx, %r8d
	movq	%r12, %r9
	movq	%rbx, %rdx
	movq	-168(%rbp), %rdi
	subl	%esi, %r8d
	leaq	(%rax,%rsi,2), %rcx
	movq	%rbx, %rsi
	call	utext_replace_67@PLT
	cltq
	addq	%rax, %rbx
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L268:
	leaq	-128(%rbp), %r14
	movq	%r12, %r9
	movl	$2, %r8d
	movq	%r14, %rcx
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L273:
	movq	-184(%rbp), %rax
	movl	44(%r15), %edx
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movq	%rbx, -192(%rbp)
	movq	8(%rax), %rax
	movq	%r12, -200(%rbp)
	movq	%r15, %r12
	movq	136(%rax), %rax
	movl	8(%rax), %esi
	movl	40(%r15), %eax
	movl	%esi, %ebx
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L278:
	addl	$1, %eax
	movl	%eax, 40(%r12)
.L198:
	addl	$1, %r14d
	movl	%r15d, %r13d
.L199:
	cmpl	%edx, %eax
	jge	.L193
	movq	48(%r12), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %edi
	movzwl	%di, %r15d
	cmpw	$-10241, %di
	jbe	.L194
.L193:
	movq	%r12, %rdi
	call	utext_current32_67@PLT
	movl	%eax, %r15d
	cmpl	$-1, %eax
	je	.L256
.L194:
	movl	%r15d, %edi
	call	u_isdigit_67@PLT
	testb	%al, %al
	je	.L256
	movl	%r15d, %edi
	call	u_charDigitValue_67@PLT
	leal	0(%r13,%r13,4), %edx
	leal	(%rax,%rdx,2), %r15d
	cmpl	%r15d, %ebx
	jl	.L277
	movl	40(%r12), %eax
	movl	44(%r12), %edx
	cmpl	%edx, %eax
	jge	.L197
	movq	48(%r12), %r8
	movslq	%eax, %rdi
	cmpw	$-10241, (%r8,%rdi,2)
	jbe	.L278
.L197:
	movq	%r12, %rdi
	call	utext_next32_67@PLT
	movl	40(%r12), %eax
	movl	44(%r12), %edx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L276:
	movl	40(%r15), %edx
	testl	%edx, %edx
	jle	.L171
	movq	48(%r15), %rcx
	movslq	%edx, %rax
	cmpw	$-10241, -2(%rcx,%rax,2)
	ja	.L171
	subl	$1, %edx
	movl	%edx, 40(%r15)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L274:
	movw	%ax, -150(%rbp)
	leaq	-150(%rbp), %rcx
	movq	%r12, %r9
	movl	$1, %r8d
.L258:
	movq	-168(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	call	utext_replace_67@PLT
	cltq
	addq	%rax, %rbx
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L272:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	leaq	-128(%rbp), %r14
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	movw	%cx, -120(%rbp)
	call	utext_next32_67@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L188
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%r15, %rdi
	call	utext_next32_67@PLT
	movl	%eax, %r13d
	cmpl	$-1, %eax
	je	.L189
	andl	$-33, %eax
	subl	$65, %eax
	cmpl	$25, %eax
	jbe	.L185
	leal	-49(%r13), %eax
	cmpl	$8, %eax
	jbe	.L185
	cmpl	$125, %r13d
	je	.L279
.L189:
	movl	$66325, (%r12)
	xorl	%r13d, %r13d
.L184:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L182:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L202
	movq	-184(%rbp), %rax
	movl	316(%rax), %eax
	testl	%eax, %eax
	jle	.L201
	movl	%eax, (%r12)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L185:
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L188
	cmpl	$125, %r13d
	jne	.L181
.L188:
	xorl	%r13d, %r13d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L201:
	movq	-168(%rbp), %rdi
	call	utext_nativeLength_67@PLT
	movq	%rax, %r14
	movq	-184(%rbp), %rax
	cmpb	$0, 130(%rax)
	je	.L280
	testl	%r13d, %r13d
	js	.L204
	movq	8(%rax), %rax
	movq	136(%rax), %rdx
	movl	8(%rdx), %eax
	cmpl	%eax, %r13d
	jg	.L204
	testl	%r13d, %r13d
	je	.L281
	testl	%eax, %eax
	jle	.L218
	subl	$1, %r13d
	subl	%r13d, %eax
	testl	%eax, %eax
	jle	.L218
	movq	24(%rdx), %rax
	movslq	%r13d, %r13
	movslq	(%rax,%r13,4), %rax
	leal	1(%rax), %edx
.L208:
	movq	-184(%rbp), %rsi
	movslq	%edx, %rdx
	movq	184(%rsi), %rcx
	movq	16(%rcx,%rax,8), %rsi
	movq	16(%rcx,%rdx,8), %rdx
.L207:
	testq	%rsi, %rsi
	js	.L282
	movq	-184(%rbp), %rax
	movq	32(%rax), %rdi
	cmpq	$0, 32(%rdi)
	jne	.L210
	movq	48(%rax), %rax
	cmpq	16(%rdi), %rax
	je	.L283
.L210:
	movq	56(%rdi), %rax
	cmpq	$0, 72(%rax)
	je	.L284
	leaq	-144(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rdx, -200(%rbp)
	movq	%rsi, -192(%rbp)
	movl	$0, -144(%rbp)
	call	utext_extract_67@PLT
	movq	-200(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movl	%eax, %r13d
.L212:
	leal	1(%r13), %r8d
	movq	%rdx, -208(%rbp)
	movslq	%r8d, %rdi
	movq	%rsi, -200(%rbp)
	addq	%rdi, %rdi
	movl	%r8d, -192(%rbp)
	call	uprv_malloc_67@PLT
	movl	-192(%rbp), %r8d
	movq	-200(%rbp), %rsi
	testq	%rax, %rax
	movq	-208(%rbp), %rdx
	movq	%rax, %r10
	je	.L285
	movq	-184(%rbp), %rax
	movq	%r10, %rcx
	movq	%r12, %r9
	movq	%r10, -192(%rbp)
	movq	32(%rax), %rdi
	call	utext_extract_67@PLT
	movl	%r13d, %r8d
	movq	%r12, %r9
	movq	%r14, %rdx
	movq	-192(%rbp), %r10
	movq	-168(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r10, %rcx
	call	utext_replace_67@PLT
	movq	-192(%rbp), %r10
	movslq	%eax, %r13
	movq	%r10, %rdi
	addq	%r13, %rbx
	call	uprv_free_67@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%r12, %r15
	movq	-192(%rbp), %rbx
	movq	-200(%rbp), %r12
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L263:
	movl	%edx, %r13d
	subl	%esi, %r13d
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L275:
	leaq	-128(%rbp), %r14
	movq	%r12, %r9
	movl	$2, %r8d
	movq	%r14, %rcx
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%r15, %rdi
	call	utext_previous32_67@PLT
	movl	40(%r15), %edx
	jmp	.L173
.L280:
	movl	$66306, (%r12)
	movq	%r12, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
.L259:
	movq	-168(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r14, %rsi
	call	utext_replace_67@PLT
	cltq
	addq	%rax, %rbx
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L279:
	movq	-184(%rbp), %rax
	movq	8(%rax), %rax
	movq	192(%rax), %rdi
	testq	%rdi, %rdi
	je	.L189
	movq	%r14, %rsi
	call	uhash_geti_67@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L189
	jmp	.L184
.L277:
	movq	%r12, %r15
	movq	-192(%rbp), %rbx
	movq	-200(%rbp), %r12
	testl	%r14d, %r14d
	jne	.L182
	movl	$8, (%r12)
	jmp	.L202
.L204:
	movl	$8, (%r12)
	movq	%r12, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L259
.L281:
	movq	-184(%rbp), %rax
	movq	136(%rax), %rsi
	movq	144(%rax), %rdx
	jmp	.L207
.L218:
	movl	$1, %edx
	xorl	%eax, %eax
	jmp	.L208
.L284:
	movl	%edx, %r13d
	subl	%esi, %r13d
	jmp	.L212
.L282:
	movq	%r12, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L259
.L283:
	movslq	28(%rdi), %rcx
	cmpq	%rcx, %rax
	jne	.L210
	movq	48(%rdi), %rax
	movl	%edx, %r8d
	movq	%r12, %r9
	subl	%esi, %r8d
	leaq	(%rax,%rsi,2), %rcx
	jmp	.L259
.L285:
	movl	$7, (%r12)
	jmp	.L202
.L264:
	movl	$7, (%r12)
	jmp	.L146
.L271:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4485:
	.size	_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode.part.0, .-_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode
	.type	_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode, @function
_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode:
.LFB3247:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movl	(%rcx), %edi
	testl	%edi, %edi
	jg	.L291
	movl	316(%rax), %edi
	testl	%edi, %edi
	jg	.L293
	cmpb	$0, 130(%rax)
	jne	.L289
	movl	$66306, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	movl	%edi, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rax, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rax, -8(%rbp)
	call	_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode.part.0
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3247:
	.size	_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode, .-_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher5resetElR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher5resetElR10UErrorCode, @function
_ZN6icu_6712RegexMatcher5resetElR10UErrorCode:
.LFB3292:
	.cfi_startproc
	endbr64
	movl	(%rdx), %r8d
	movq	%rdi, %rax
	testl	%r8d, %r8d
	jg	.L295
	pxor	%xmm0, %xmm0
	movq	48(%rdi), %rcx
	movq	$0, 64(%rdi)
	movups	%xmm0, 136(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movq	%rcx, 72(%rdi)
	movq	$0, 112(%rdi)
	movq	%rcx, 120(%rdi)
	movq	$0, 80(%rdi)
	movq	%rcx, 88(%rdi)
	movq	$0, 96(%rdi)
	movq	%rcx, 104(%rdi)
	movb	$0, 130(%rdi)
	movups	%xmm0, 152(%rdi)
	xorl	%edi, %edi
	movw	%di, 168(%rax)
	movabsq	$42949672960000, %rdi
	movq	%rdi, 268(%rax)
	testq	%rsi, %rsi
	js	.L298
	cmpq	%rcx, %rsi
	jg	.L298
	movq	%rsi, 144(%rax)
.L295:
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	movl	$8, (%rdx)
	ret
	.cfi_endproc
.LFE3292:
	.size	_ZN6icu_6712RegexMatcher5resetElR10UErrorCode, .-_ZN6icu_6712RegexMatcher5resetElR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher13setStackLimitEiR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher13setStackLimitEiR10UErrorCode, @function
_ZN6icu_6712RegexMatcher13setStackLimitEiR10UErrorCode:
.LFB3306:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L306
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	316(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	jg	.L309
	movl	%esi, %r12d
	testl	%esi, %esi
	js	.L310
	movq	48(%rdi), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 64(%rdi)
	movups	%xmm0, 136(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, 72(%rdi)
	movq	%rax, 120(%rdi)
	movq	%rax, 88(%rdi)
	movq	%rax, 104(%rdi)
	movl	$0, %eax
	movw	%ax, 168(%rdi)
	movabsq	$42949672960000, %rax
	movq	$0, 112(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 96(%rdi)
	movb	$0, 130(%rdi)
	movq	%rax, 268(%rdi)
	movups	%xmm0, 152(%rdi)
	movq	176(%rdi), %rdi
	je	.L311
	movq	8(%rbx), %rax
	movslq	%esi, %rsi
	shrq	$2, %rsi
	cmpl	%esi, 128(%rax)
	cmovge	128(%rax), %esi
	call	_ZN6icu_679UVector6414setMaxCapacityEi@PLT
.L304:
	movl	%r12d, 276(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	movl	%eax, (%rdx)
.L299:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	xorl	%esi, %esi
	call	_ZN6icu_679UVector6414setMaxCapacityEi@PLT
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L310:
	movl	$1, (%rdx)
	jmp	.L299
	.cfi_endproc
.LFE3306:
	.size	_ZN6icu_6712RegexMatcher13setStackLimitEiR10UErrorCode, .-_ZN6icu_6712RegexMatcher13setStackLimitEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher6regionElllR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher6regionElllR10UErrorCode, @function
_ZN6icu_6712RegexMatcher6regionElllR10UErrorCode:
.LFB3277:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	movq	%rdi, %rax
	testl	%r9d, %r9d
	jg	.L314
	cmpq	%rdx, %rsi
	jg	.L324
	testq	%rsi, %rsi
	js	.L324
.L315:
	cmpq	%rdx, %rsi
	movq	%rdx, %r9
	movq	48(%rax), %rdi
	cmovge	%rsi, %r9
	cmpq	%r9, %rdi
	jge	.L317
	movl	$1, (%r8)
.L317:
	cmpq	$-1, %rcx
	je	.L329
	xorl	%edi, %edi
	movq	%rsi, %xmm0
	movdqa	.LC0(%rip), %xmm1
	movq	%rdx, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movw	%di, 168(%rax)
	movabsq	$42949672960000, %rdi
	movq	$0, 136(%rax)
	movb	$0, 130(%rax)
	movq	%rdi, 268(%rax)
	movups	%xmm1, 152(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 112(%rax)
	cmpq	%rcx, %rsi
	jg	.L325
	cmpq	%rcx, %rdx
	jge	.L320
.L325:
	movl	$8, (%r8)
.L320:
	movq	%rcx, 144(%rax)
.L319:
	cmpb	$0, 128(%rax)
	jne	.L322
	movups	%xmm0, 96(%rax)
.L322:
	cmpb	$0, 129(%rax)
	je	.L314
	movups	%xmm0, 80(%rax)
.L314:
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	movl	$1, (%r8)
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L329:
	pxor	%xmm0, %xmm0
	movq	%rdx, %xmm3
	movq	%rdi, 88(%rax)
	xorl	%r8d, %r8d
	movups	%xmm0, 136(%rax)
	movdqa	.LC0(%rip), %xmm0
	movq	%rdi, 104(%rax)
	movabsq	$42949672960000, %rdi
	movups	%xmm0, 152(%rax)
	movq	%rsi, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movq	$0, 80(%rax)
	movq	$0, 96(%rax)
	movb	$0, 130(%rax)
	movw	%r8w, 168(%rax)
	movq	%rdi, 268(%rax)
	movq	%rsi, 112(%rax)
	movq	%rdx, 120(%rax)
	movups	%xmm0, 64(%rax)
	jmp	.L319
	.cfi_endproc
.LFE3277:
	.size	_ZN6icu_6712RegexMatcher6regionElllR10UErrorCode, .-_ZN6icu_6712RegexMatcher6regionElllR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher6regionEllR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher6regionEllR10UErrorCode, @function
_ZN6icu_6712RegexMatcher6regionEllR10UErrorCode:
.LFB3278:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movl	(%rcx), %edi
	testl	%edi, %edi
	jg	.L332
	cmpq	%rdx, %rsi
	jg	.L338
	testq	%rsi, %rsi
	js	.L338
.L333:
	cmpq	%rdx, %rsi
	movq	%rdx, %r8
	movq	48(%rax), %rdi
	cmovge	%rsi, %r8
	cmpq	%r8, %rdi
	jge	.L335
	movl	$1, (%rcx)
.L335:
	pxor	%xmm1, %xmm1
	movq	%rsi, %xmm0
	movq	%rdx, %xmm2
	xorl	%ecx, %ecx
	movq	%rdi, 88(%rax)
	punpcklqdq	%xmm2, %xmm0
	cmpb	$0, 128(%rax)
	movups	%xmm1, 136(%rax)
	movdqa	.LC0(%rip), %xmm1
	movq	%rdi, 104(%rax)
	movabsq	$42949672960000, %rdi
	movq	$0, 80(%rax)
	movq	$0, 96(%rax)
	movb	$0, 130(%rax)
	movw	%cx, 168(%rax)
	movq	%rdi, 268(%rax)
	movq	%rsi, 112(%rax)
	movq	%rdx, 120(%rax)
	movups	%xmm1, 152(%rax)
	movups	%xmm0, 64(%rax)
	jne	.L336
	movups	%xmm0, 96(%rax)
.L336:
	cmpb	$0, 129(%rax)
	je	.L332
	movups	%xmm0, 80(%rax)
.L332:
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	movl	$1, (%rcx)
	jmp	.L333
	.cfi_endproc
.LFE3278:
	.size	_ZN6icu_6712RegexMatcher6regionEllR10UErrorCode, .-_ZN6icu_6712RegexMatcher6regionEllR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher5end64EiR10UErrorCode
	.type	_ZNK6icu_6712RegexMatcher5end64EiR10UErrorCode, @function
_ZNK6icu_6712RegexMatcher5end64EiR10UErrorCode:
.LFB3252:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L346
	cmpb	$0, 130(%rdi)
	je	.L349
	testl	%esi, %esi
	js	.L342
	movq	8(%rdi), %rax
	movq	136(%rax), %rcx
	movl	8(%rcx), %eax
	cmpl	%eax, %esi
	jg	.L342
	testl	%esi, %esi
	je	.L350
	movl	$1, %edx
	testl	%eax, %eax
	jle	.L345
	subl	$1, %esi
	subl	%esi, %eax
	testl	%eax, %eax
	jle	.L345
	movq	24(%rcx), %rax
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %edx
	addl	$1, %edx
.L345:
	movq	184(%rdi), %rax
	movslq	%edx, %rdx
	movq	16(%rax,%rdx,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	movq	144(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	movl	$8, (%rdx)
	movq	$-1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	movq	$-1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	movl	$66306, (%rdx)
	movq	$-1, %rax
	ret
	.cfi_endproc
.LFE3252:
	.size	_ZNK6icu_6712RegexMatcher5end64EiR10UErrorCode, .-_ZNK6icu_6712RegexMatcher5end64EiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher7start64EiR10UErrorCode
	.type	_ZNK6icu_6712RegexMatcher7start64EiR10UErrorCode, @function
_ZNK6icu_6712RegexMatcher7start64EiR10UErrorCode:
.LFB3300:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L359
	movl	316(%rdi), %eax
	testl	%eax, %eax
	jg	.L362
	cmpb	$0, 130(%rdi)
	je	.L363
	testl	%esi, %esi
	js	.L355
	movq	8(%rdi), %rax
	movq	136(%rax), %rcx
	movl	8(%rcx), %eax
	cmpl	%eax, %esi
	jg	.L355
	testl	%esi, %esi
	je	.L364
	xorl	%edx, %edx
	testl	%eax, %eax
	jle	.L358
	subl	$1, %esi
	subl	%esi, %eax
	testl	%eax, %eax
	jle	.L358
	movq	24(%rcx), %rax
	movslq	%esi, %rsi
	movslq	(%rax,%rsi,4), %rdx
.L358:
	movq	184(%rdi), %rax
	movq	16(%rax,%rdx,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	movq	136(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	movl	$8, (%rdx)
	movq	$-1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	movq	$-1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	movl	%eax, (%rdx)
	movq	$-1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	movl	$66306, (%rdx)
	movq	$-1, %rax
	ret
	.cfi_endproc
.LFE3300:
	.size	_ZNK6icu_6712RegexMatcher7start64EiR10UErrorCode, .-_ZNK6icu_6712RegexMatcher7start64EiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher5groupEiP5UTextRlR10UErrorCode
	.type	_ZNK6icu_6712RegexMatcher5groupEiP5UTextRlR10UErrorCode, @function
_ZNK6icu_6712RegexMatcher5groupEiP5UTextRlR10UErrorCode:
.LFB3261:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	movl	(%r8), %edx
	movq	$0, (%rcx)
	testl	%edx, %edx
	jg	.L385
	movl	316(%rdi), %edx
	testl	%edx, %edx
	jle	.L367
	movl	%edx, (%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	cmpb	$0, 130(%rdi)
	je	.L387
	testl	%esi, %esi
	js	.L369
	movq	8(%rdi), %rdx
	movq	136(%rdx), %r9
	movl	8(%r9), %edx
	cmpl	%edx, %esi
	jg	.L369
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	testl	%esi, %esi
	jne	.L388
	movq	136(%rdi), %r12
	movq	144(%rdi), %rdx
	movq	32(%rdi), %rsi
	testq	%r12, %r12
	js	.L389
.L373:
	subq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rdx, (%rcx)
	xorl	%edx, %edx
	movl	$1, %ecx
	call	utext_clone_67@PLT
	testq	%rax, %rax
	je	.L365
	movq	%r12, %rdx
	subq	32(%rax), %rdx
	js	.L375
	movslq	28(%rax), %rcx
	cmpq	%rcx, %rdx
	jge	.L375
	movq	48(%rax), %rcx
	cmpw	$-9217, (%rcx,%rdx,2)
	jbe	.L390
.L375:
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -24(%rbp)
	call	utext_setNativeIndex_67@PLT
	movq	-24(%rbp), %rax
.L365:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	movl	$66306, (%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	movl	$8, (%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	subl	$1, %esi
	testl	%edx, %edx
	jle	.L379
	subl	%esi, %edx
	testl	%edx, %edx
	jle	.L379
	movq	24(%r9), %rdx
	movslq	%esi, %rsi
	movslq	(%rdx,%rsi,4), %rdx
	leal	1(%rdx), %esi
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L379:
	movl	$1, %esi
	xorl	%edx, %edx
.L372:
	movq	184(%rdi), %r9
	movq	16(%r9,%rdx,8), %r12
	movslq	%esi, %rdx
	movq	32(%rdi), %rsi
	movq	16(%r9,%rdx,8), %rdx
	testq	%r12, %r12
	jns	.L373
.L389:
	addq	$24, %rsp
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	utext_clone_67@PLT
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	movl	%edx, 40(%rax)
	jmp	.L365
	.cfi_endproc
.LFE3261:
	.size	_ZNK6icu_6712RegexMatcher5groupEiP5UTextRlR10UErrorCode, .-_ZNK6icu_6712RegexMatcher5groupEiP5UTextRlR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher3endER10UErrorCode
	.type	_ZNK6icu_6712RegexMatcher3endER10UErrorCode, @function
_ZNK6icu_6712RegexMatcher3endER10UErrorCode:
.LFB3250:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L395
	cmpb	$0, 130(%rdi)
	je	.L396
	movq	8(%rdi), %rax
	movq	136(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	js	.L397
	movl	144(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L395:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	movl	$66306, (%rsi)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L397:
	movl	$8, (%rsi)
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3250:
	.size	_ZNK6icu_6712RegexMatcher3endER10UErrorCode, .-_ZNK6icu_6712RegexMatcher3endER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher5groupEP5UTextRlR10UErrorCode
	.type	_ZNK6icu_6712RegexMatcher5groupEP5UTextRlR10UErrorCode, @function
_ZNK6icu_6712RegexMatcher5groupEP5UTextRlR10UErrorCode:
.LFB3260:
	.cfi_startproc
	endbr64
	movq	%rdi, %r9
	movq	%rsi, %rdi
	movl	(%rcx), %esi
	movq	$0, (%rdx)
	testl	%esi, %esi
	jg	.L399
	movq	%rcx, %r8
	movl	316(%r9), %ecx
	testl	%ecx, %ecx
	jle	.L400
	movl	%ecx, (%r8)
.L399:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	cmpb	$0, 130(%r9)
	je	.L414
	movq	8(%r9), %rcx
	movq	136(%rcx), %rcx
	movl	8(%rcx), %eax
	testl	%eax, %eax
	jns	.L402
	movl	$8, (%r8)
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L414:
	movl	$66306, (%r8)
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	136(%r9), %r12
	movq	32(%r9), %rsi
	testq	%r12, %r12
	js	.L415
	movq	144(%r9), %rcx
	subq	%r12, %rcx
	movq	%rcx, (%rdx)
	xorl	%edx, %edx
	movl	$1, %ecx
	call	utext_clone_67@PLT
	testq	%rax, %rax
	je	.L398
	movq	%r12, %rdx
	subq	32(%rax), %rdx
	js	.L404
	movslq	28(%rax), %rcx
	cmpq	%rcx, %rdx
	jge	.L404
	movq	48(%rax), %rcx
	cmpw	$-9217, (%rcx,%rdx,2)
	jbe	.L416
.L404:
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -24(%rbp)
	call	utext_setNativeIndex_67@PLT
	movq	-24(%rbp), %rax
.L398:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	addq	$24, %rsp
	movl	$1, %ecx
	xorl	%edx, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	utext_clone_67@PLT
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	movl	%edx, 40(%rax)
	jmp	.L398
	.cfi_endproc
.LFE3260:
	.size	_ZNK6icu_6712RegexMatcher5groupEP5UTextRlR10UErrorCode, .-_ZNK6icu_6712RegexMatcher5groupEP5UTextRlR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher5startER10UErrorCode
	.type	_ZNK6icu_6712RegexMatcher5startER10UErrorCode, @function
_ZNK6icu_6712RegexMatcher5startER10UErrorCode:
.LFB3298:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L422
	movl	316(%rdi), %eax
	testl	%eax, %eax
	jg	.L423
	cmpb	$0, 130(%rdi)
	je	.L424
	movq	8(%rdi), %rax
	movq	136(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	js	.L425
	movl	136(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L423:
	movl	%eax, (%rsi)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	movl	$66306, (%rsi)
	movl	$-1, %eax
	ret
.L425:
	movl	$8, (%rsi)
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3298:
	.size	_ZNK6icu_6712RegexMatcher5startER10UErrorCode, .-_ZNK6icu_6712RegexMatcher5startER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher7start64ER10UErrorCode
	.type	_ZNK6icu_6712RegexMatcher7start64ER10UErrorCode, @function
_ZNK6icu_6712RegexMatcher7start64ER10UErrorCode:
.LFB3299:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L431
	movl	316(%rdi), %eax
	testl	%eax, %eax
	jg	.L432
	cmpb	$0, 130(%rdi)
	je	.L433
	movq	8(%rdi), %rax
	movq	136(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	js	.L434
	movq	136(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	movq	$-1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	movl	%eax, (%rsi)
	movq	$-1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	movl	$66306, (%rsi)
	movq	$-1, %rax
	ret
.L434:
	movl	$8, (%rsi)
	movq	$-1, %rax
	ret
	.cfi_endproc
.LFE3299:
	.size	_ZNK6icu_6712RegexMatcher7start64ER10UErrorCode, .-_ZNK6icu_6712RegexMatcher7start64ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher3endEiR10UErrorCode
	.type	_ZNK6icu_6712RegexMatcher3endEiR10UErrorCode, @function
_ZNK6icu_6712RegexMatcher3endEiR10UErrorCode:
.LFB3253:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L442
	cmpb	$0, 130(%rdi)
	je	.L445
	testl	%esi, %esi
	js	.L438
	movq	8(%rdi), %rax
	movq	136(%rax), %rcx
	movl	8(%rcx), %eax
	cmpl	%eax, %esi
	jg	.L438
	testl	%esi, %esi
	je	.L446
	movl	$1, %edx
	testl	%eax, %eax
	jle	.L441
	subl	$1, %esi
	subl	%esi, %eax
	testl	%eax, %eax
	jle	.L441
	movq	24(%rcx), %rax
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %edx
	addl	$1, %edx
.L441:
	movq	184(%rdi), %rax
	movslq	%edx, %rdx
	movl	16(%rax,%rdx,8), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L446:
	movl	144(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	movl	$8, (%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	movl	$66306, (%rdx)
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3253:
	.size	_ZNK6icu_6712RegexMatcher3endEiR10UErrorCode, .-_ZNK6icu_6712RegexMatcher3endEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher5groupER10UErrorCode
	.type	_ZNK6icu_6712RegexMatcher5groupER10UErrorCode, @function
_ZNK6icu_6712RegexMatcher5groupER10UErrorCode:
.LFB3259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movw	%cx, 8(%rdi)
	movl	(%rdx), %edi
	testl	%edi, %edi
	jg	.L447
	movl	316(%rsi), %eax
	movq	%rsi, %rbx
	movq	%rdx, %r12
	testl	%eax, %eax
	jg	.L455
	cmpb	$0, 130(%rsi)
	je	.L456
	movq	8(%rsi), %rax
	movq	136(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	js	.L457
	movq	136(%rsi), %r13
	movq	144(%rsi), %r15
	cmpq	$-1, %r13
	je	.L447
	cmpq	%r13, %r15
	jne	.L458
.L447:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	movq	32(%rsi), %rdi
	movq	%rdx, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	utext_extract_67@PLT
	cmpl	$15, (%r12)
	jne	.L447
	movl	$0, (%r12)
	movl	%eax, %esi
	movq	%r14, %rdi
	movl	%eax, -52(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movl	-52(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L459
	movq	32(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r12, %r9
	movq	%r15, %rdx
	call	utext_extract_67@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L455:
	movl	%eax, (%rdx)
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$7, (%r12)
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L456:
	movl	$66306, (%rdx)
	jmp	.L447
.L457:
	movl	$8, (%rdx)
	jmp	.L447
	.cfi_endproc
.LFE3259:
	.size	_ZNK6icu_6712RegexMatcher5groupER10UErrorCode, .-_ZNK6icu_6712RegexMatcher5groupER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher17appendReplacementERNS_13UnicodeStringERKS1_R10UErrorCode
	.type	_ZN6icu_6712RegexMatcher17appendReplacementERNS_13UnicodeStringERKS1_R10UErrorCode, @function
_ZN6icu_6712RegexMatcher17appendReplacementERNS_13UnicodeStringERKS1_R10UErrorCode:
.LFB3246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-352(%rbp), %r15
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-352(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movl	$18, %ecx
	pushq	%rbx
	movq	%r12, %rdx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	rep stosq
	movq	%r15, %rdi
	movl	$878368812, -352(%rbp)
	movl	$144, -340(%rbp)
	call	utext_openConstUnicodeString_67@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L468
.L461:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L469
	addq	$312, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_restore_state
	xorl	%ebx, %ebx
	leaq	-208(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rax
	movl	$18, %ecx
	leaq	-208(%rbp), %rbx
	rep stosq
	movq	%rbx, %rdi
	movl	$878368812, -208(%rbp)
	movl	$144, -196(%rbp)
	call	utext_openUnicodeString_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L470
.L462:
	movq	%r15, %rdi
	call	utext_close_67@PLT
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L470:
	movl	316(%r13), %eax
	testl	%eax, %eax
	jg	.L471
	cmpb	$0, 130(%r13)
	jne	.L465
	movl	$66306, (%r12)
.L464:
	movq	%rbx, %rdi
	call	utext_close_67@PLT
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L471:
	movl	%eax, (%r12)
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L465:
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode.part.0
	jmp	.L464
.L469:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3246:
	.size	_ZN6icu_6712RegexMatcher17appendReplacementERNS_13UnicodeStringERKS1_R10UErrorCode, .-_ZN6icu_6712RegexMatcher17appendReplacementERNS_13UnicodeStringERKS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher5startEiR10UErrorCode
	.type	_ZNK6icu_6712RegexMatcher5startEiR10UErrorCode, @function
_ZNK6icu_6712RegexMatcher5startEiR10UErrorCode:
.LFB3301:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L480
	movl	316(%rdi), %eax
	testl	%eax, %eax
	jg	.L483
	cmpb	$0, 130(%rdi)
	je	.L484
	testl	%esi, %esi
	js	.L476
	movq	8(%rdi), %rax
	movq	136(%rax), %rcx
	movl	8(%rcx), %eax
	cmpl	%eax, %esi
	jg	.L476
	testl	%esi, %esi
	je	.L485
	xorl	%edx, %edx
	testl	%eax, %eax
	jle	.L479
	subl	$1, %esi
	subl	%esi, %eax
	testl	%eax, %eax
	jle	.L479
	movq	24(%rcx), %rax
	movslq	%esi, %rsi
	movslq	(%rax,%rsi,4), %rdx
.L479:
	movq	184(%rdi), %rax
	movl	16(%rax,%rdx,8), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L485:
	movl	136(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	movl	$8, (%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L480:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	movl	%eax, (%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	movl	$66306, (%rdx)
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3301:
	.size	_ZNK6icu_6712RegexMatcher5startEiR10UErrorCode, .-_ZNK6icu_6712RegexMatcher5startEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher5groupEiR10UErrorCode
	.type	_ZNK6icu_6712RegexMatcher5groupEiR10UErrorCode, @function
_ZNK6icu_6712RegexMatcher5groupEiR10UErrorCode:
.LFB3262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movl	(%rcx), %edi
	testl	%edi, %edi
	jg	.L486
	movl	316(%rsi), %eax
	movq	%rsi, %rbx
	movq	%rcx, %r13
	testl	%eax, %eax
	jg	.L505
	cmpb	$0, 130(%rsi)
	je	.L506
	testl	%edx, %edx
	js	.L490
	movq	8(%rsi), %rax
	movq	136(%rax), %rsi
	movl	8(%rsi), %eax
	cmpl	%eax, %edx
	jg	.L490
	testl	%edx, %edx
	jne	.L492
	movq	136(%rbx), %r14
	movq	144(%rbx), %r15
.L493:
	cmpq	$-1, %r14
	je	.L486
	cmpq	%r14, %r15
	jne	.L498
.L486:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	xorl	%ecx, %ecx
	testl	%eax, %eax
	jle	.L494
	leal	-1(%rdx), %edi
	movl	%eax, %r8d
	subl	%edi, %r8d
	testl	%r8d, %r8d
	jle	.L494
	movq	24(%rsi), %rcx
	movslq	%edi, %rdi
	movslq	(%rcx,%rdi,4), %rcx
.L494:
	movq	184(%rbx), %rdi
	movq	16(%rdi,%rcx,8), %r14
	movl	$1, %ecx
	testl	%eax, %eax
	jle	.L495
	subl	$1, %edx
	subl	%edx, %eax
	testl	%eax, %eax
	jle	.L495
	movq	24(%rsi), %rax
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ecx
	addl	$1, %ecx
.L495:
	movslq	%ecx, %rcx
	movq	16(%rdi,%rcx,8), %r15
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L498:
	movq	32(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %r9
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	utext_extract_67@PLT
	cmpl	$15, 0(%r13)
	jne	.L486
	movl	$0, 0(%r13)
	movl	%eax, %esi
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movl	-52(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L507
	movq	32(%rbx), %rdi
	movq	%r14, %rsi
	movq	%r13, %r9
	movq	%r15, %rdx
	call	utext_extract_67@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L490:
	movl	$8, 0(%r13)
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L505:
	movl	%eax, (%rcx)
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L507:
	movl	$7, 0(%r13)
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L506:
	movl	$66306, (%rcx)
	jmp	.L486
	.cfi_endproc
.LFE3262:
	.size	_ZNK6icu_6712RegexMatcher5groupEiR10UErrorCode, .-_ZNK6icu_6712RegexMatcher5groupEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode, @function
_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode:
.LFB3249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L509
	movl	316(%rdi), %eax
	movq	%rdi, %rbx
	movq	%rdx, %r12
	testl	%eax, %eax
	jg	.L516
	movq	48(%rdi), %rdx
	movq	160(%rdi), %rsi
	cmpq	%rsi, %rdx
	jle	.L509
	movq	32(%rdi), %rdi
	cmpq	$0, 32(%rdi)
	jne	.L511
	cmpq	16(%rdi), %rdx
	je	.L517
.L511:
	movq	56(%rdi), %rax
	cmpq	$0, 72(%rax)
	je	.L518
	movq	%r12, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	utext_extract_67@PLT
	movl	$0, (%r12)
	movl	%eax, %r15d
.L513:
	movslq	%r15d, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L519
	movq	48(%rbx), %rdx
	movq	32(%rbx), %rdi
	movq	%r12, %r9
	movl	%r15d, %r8d
	movq	160(%rbx), %rsi
	movq	%rax, %rcx
	call	utext_extract_67@PLT
	movq	%r13, %rdi
	call	utext_nativeLength_67@PLT
	movq	%r13, %rdi
	movq	%r12, %r9
	movl	%r15d, %r8d
	movq	%rax, %rsi
	movq	%r14, %rcx
	movq	%rax, %rdx
	call	utext_replace_67@PLT
	movq	%r14, %rdi
	call	uprv_free_67@PLT
.L509:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore_state
	movl	%eax, (%rdx)
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L518:
	movl	%edx, %r15d
	subl	%esi, %r15d
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L517:
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jne	.L511
	movq	%r13, %rdi
	call	utext_nativeLength_67@PLT
	movq	32(%rbx), %rdx
	movq	48(%rbx), %r8
	movq	%r12, %r9
	movq	%rax, %rsi
	movq	160(%rbx), %rax
	movq	%r13, %rdi
	movq	48(%rdx), %rdx
	subl	%eax, %r8d
	leaq	(%rdx,%rax,2), %rcx
	movq	%rsi, %rdx
	call	utext_replace_67@PLT
	jmp	.L509
.L519:
	movl	$7, 316(%rbx)
	jmp	.L509
	.cfi_endproc
.LFE3249:
	.size	_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode, .-_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3697:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3697:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3700:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L533
	ret
	.p2align 4,,10
	.p2align 3
.L533:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L521
	cmpb	$0, 12(%rbx)
	jne	.L534
.L525:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L521:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L525
	.cfi_endproc
.LFE3700:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3703:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L537
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3703:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3706:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L540
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L540:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3706:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L546
.L542:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L547
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L547:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3708:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3709:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3709:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3710:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3710:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3711:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3711:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3712:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3712:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3713:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3713:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3714:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L563
	testl	%edx, %edx
	jle	.L563
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L566
.L555:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L563:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L555
	.cfi_endproc
.LFE3714:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L570
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L570
	testl	%r12d, %r12d
	jg	.L577
	cmpb	$0, 12(%rbx)
	jne	.L578
.L572:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L572
.L578:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L570:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3715:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L580
	movq	(%rdi), %r8
.L581:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L584
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L584
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L584:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3716:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3717:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L591
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3717:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3718:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3718:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3719:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3719:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3720:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3720:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3722:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3722:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3724:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3724:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher4initER10UErrorCode
	.type	_ZN6icu_6712RegexMatcher4initER10UErrorCode, @function
_ZN6icu_6712RegexMatcher4initER10UErrorCode:
.LFB3244:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$256, %eax
	xorl	%edx, %edx
	movb	$0, 130(%rdi)
	movdqa	.LC0(%rip), %xmm1
	movw	%ax, 128(%rdi)
	movw	%dx, 168(%rdi)
	movb	$0, 313(%rdi)
	movups	%xmm0, 176(%rdi)
	movl	(%rsi), %eax
	movups	%xmm1, 152(%rdi)
	movdqa	.LC1(%rip), %xmm1
	movl	%eax, 316(%rdi)
	leaq	200(%rdi), %rax
	movl	$0, 56(%rdi)
	movq	$0, 280(%rdi)
	movq	$0, 288(%rdi)
	movq	$0, 296(%rdi)
	movq	$0, 304(%rdi)
	movq	%rax, 192(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movb	$0, 312(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 136(%rdi)
	movups	%xmm1, 264(%rdi)
	movups	%xmm0, 320(%rdi)
	movups	%xmm0, 8(%rdi)
	movups	%xmm0, 24(%rdi)
	ret
	.cfi_endproc
.LFE3244:
	.size	_ZN6icu_6712RegexMatcher4initER10UErrorCode, .-_ZN6icu_6712RegexMatcher4initER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher5init2EP5UTextR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher5init2EP5UTextR10UErrorCode, @function
_ZN6icu_6712RegexMatcher5init2EP5UTextR10UErrorCode:
.LFB3245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rdx), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	jg	.L612
	movq	8(%rdi), %rax
	movq	%rsi, %r13
	movq	%rdx, %r12
	movslq	132(%rax), %rdi
	cmpl	$8, %edi
	jg	.L626
.L601:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L602
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_679UVector64C1ER10UErrorCode@PLT
	movq	32(%rbx), %rdi
	movq	%r14, 176(%rbx)
	cmpq	%rdi, %r13
	je	.L604
	leaq	316(%rbx), %r14
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r14, %r8
	call	utext_clone_67@PLT
	movq	8(%rbx), %rdx
	movq	%rax, 32(%rbx)
	cmpb	$0, 184(%rdx)
	jne	.L627
.L605:
	movl	316(%rbx), %edx
	testl	%edx, %edx
	jle	.L628
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L612
.L613:
	movl	%edx, (%r12)
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L612:
	movl	%eax, 316(%rbx)
.L598:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L626:
	.cfi_restore_state
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 192(%rbx)
	testq	%rax, %rax
	jne	.L601
.L624:
	movl	$7, 316(%rbx)
	movl	$7, (%r12)
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L628:
	movq	32(%rbx), %rdi
	call	utext_nativeLength_67@PLT
	movq	24(%rbx), %rdi
	movq	%rax, 48(%rbx)
	testq	%rdi, %rdi
	je	.L607
	movq	(%rdi), %rax
	call	*8(%rax)
.L607:
	movq	320(%rbx), %rdi
	movq	$0, 24(%rbx)
	testq	%rdi, %rdi
	je	.L608
	movq	(%rdi), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	*64(%rax)
.L608:
	movq	328(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L604
	movq	(%rdi), %rax
	movq	32(%rbx), %rsi
	movq	%r14, %rdx
	call	*64(%rax)
.L604:
	movq	48(%rbx), %rax
	pxor	%xmm0, %xmm0
	movb	$0, 130(%rbx)
	movb	$0, 312(%rbx)
	movq	%rax, 72(%rbx)
	movq	%rax, 120(%rbx)
	movq	%rax, 88(%rbx)
	movq	%rax, 104(%rbx)
	xorl	%eax, %eax
	movw	%ax, 168(%rbx)
	movabsq	$42949672960000, %rax
	movq	%rax, 268(%rbx)
	movl	(%r12), %eax
	movups	%xmm0, 136(%rbx)
	movdqa	.LC0(%rip), %xmm0
	movq	$0, 64(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 96(%rbx)
	movups	%xmm0, 152(%rbx)
	testl	%eax, %eax
	jg	.L612
	movl	316(%rbx), %edx
	testl	%edx, %edx
	jg	.L613
	movq	8(%rbx), %rax
	movq	176(%rbx), %rdi
	movl	$2000000, %esi
	cmpl	$2000000, 128(%rax)
	cmovge	128(%rax), %esi
	call	_ZN6icu_679UVector6414setMaxCapacityEi@PLT
	movl	(%r12), %eax
	movl	$8000000, 276(%rbx)
	testl	%eax, %eax
	jg	.L612
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L627:
	.cfi_restore_state
	movq	40(%rbx), %rdi
	movq	%r14, %r8
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	utext_clone_67@PLT
	movq	%rax, 40(%rbx)
	jmp	.L605
.L602:
	movq	$0, 176(%rbx)
	jmp	.L624
	.cfi_endproc
.LFE3245:
	.size	_ZN6icu_6712RegexMatcher5init2EP5UTextR10UErrorCode, .-_ZN6icu_6712RegexMatcher5init2EP5UTextR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcherC2EPKNS_12RegexPatternE
	.type	_ZN6icu_6712RegexMatcherC2EPKNS_12RegexPatternE, @function
_ZN6icu_6712RegexMatcherC2EPKNS_12RegexPatternE:
.LFB3226:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6712RegexMatcherE(%rip), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rax, (%rdi)
	movdqa	.LC0(%rip), %xmm1
	movl	$256, %eax
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 136(%rdi)
	movups	%xmm0, 176(%rdi)
	movups	%xmm0, 320(%rdi)
	movdqa	.LC1(%rip), %xmm0
	movw	%ax, 128(%rdi)
	leaq	200(%rdi), %rax
	movl	$0, 316(%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movl	$0, 56(%rdi)
	movb	$0, 130(%rdi)
	movw	%dx, 168(%rdi)
	movq	%rax, 192(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 280(%rdi)
	movq	$0, 288(%rdi)
	movq	$0, 296(%rdi)
	movq	$0, 304(%rdi)
	movw	%cx, 312(%rdi)
	movups	%xmm1, 152(%rdi)
	movups	%xmm0, 264(%rdi)
	testq	%rsi, %rsi
	je	.L633
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	movq	%rsi, 8(%rdi)
	leaq	316(%rdi), %rdx
	movq	3832(%rax), %rsi
	jmp	_ZN6icu_6712RegexMatcher5init2EP5UTextR10UErrorCode
	.p2align 4,,10
	.p2align 3
.L633:
	movl	$1, 316(%rdi)
	ret
	.cfi_endproc
.LFE3226:
	.size	_ZN6icu_6712RegexMatcherC2EPKNS_12RegexPatternE, .-_ZN6icu_6712RegexMatcherC2EPKNS_12RegexPatternE
	.globl	_ZN6icu_6712RegexMatcherC1EPKNS_12RegexPatternE
	.set	_ZN6icu_6712RegexMatcherC1EPKNS_12RegexPatternE,_ZN6icu_6712RegexMatcherC2EPKNS_12RegexPatternE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcherC2ERKNS_13UnicodeStringES3_jR10UErrorCode
	.type	_ZN6icu_6712RegexMatcherC2ERKNS_13UnicodeStringES3_jR10UErrorCode, @function
_ZN6icu_6712RegexMatcherC2ERKNS_13UnicodeStringES3_jR10UErrorCode:
.LFB3229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	xorl	%edx, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$224, %rsp
	movdqa	.LC0(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6712RegexMatcherE(%rip), %rax
	movups	%xmm0, 64(%rdi)
	movq	%rax, (%rdi)
	movl	$256, %eax
	movw	%ax, 128(%rdi)
	movl	(%r8), %eax
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 136(%rdi)
	movups	%xmm0, 176(%rdi)
	movups	%xmm0, 320(%rdi)
	movdqa	.LC1(%rip), %xmm0
	movw	%dx, 168(%rdi)
	leaq	200(%rdi), %rdx
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movl	$0, 56(%rdi)
	movb	$0, 130(%rdi)
	movl	%eax, 316(%rdi)
	movq	%rdx, 192(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 48(%rdi)
	movups	%xmm1, 152(%rdi)
	movups	%xmm0, 264(%rdi)
	movq	$0, 280(%rdi)
	movq	$0, 288(%rdi)
	movq	$0, 296(%rdi)
	movq	$0, 304(%rdi)
	xorl	%edi, %edi
	movw	%di, 312(%rbx)
	testl	%eax, %eax
	jg	.L634
	movq	%rsi, %rdi
	leaq	-112(%rbp), %rdx
	movl	%ecx, %esi
	movq	%r8, %rcx
	movq	%r8, %r12
	leaq	-256(%rbp), %r14
	call	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode@PLT
	movl	$18, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %xmm0
	leaq	-256(%rbp), %rdi
	xorl	%eax, %eax
	rep stosq
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, %rdi
	movl	$878368812, -256(%rbp)
	movups	%xmm0, 8(%rbx)
	movl	$144, -244(%rbp)
	call	utext_openConstUnicodeString_67@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher5init2EP5UTextR10UErrorCode
	movq	%r14, %rdi
	call	utext_close_67@PLT
	movb	$1, 312(%rbx)
.L634:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L638
	addq	$224, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L638:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3229:
	.size	_ZN6icu_6712RegexMatcherC2ERKNS_13UnicodeStringES3_jR10UErrorCode, .-_ZN6icu_6712RegexMatcherC2ERKNS_13UnicodeStringES3_jR10UErrorCode
	.globl	_ZN6icu_6712RegexMatcherC1ERKNS_13UnicodeStringES3_jR10UErrorCode
	.set	_ZN6icu_6712RegexMatcherC1ERKNS_13UnicodeStringES3_jR10UErrorCode,_ZN6icu_6712RegexMatcherC2ERKNS_13UnicodeStringES3_jR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcherC2EP5UTextS2_jR10UErrorCode
	.type	_ZN6icu_6712RegexMatcherC2EP5UTextS2_jR10UErrorCode, @function
_ZN6icu_6712RegexMatcherC2EP5UTextS2_jR10UErrorCode:
.LFB3232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	200(%r12), %rdx
	subq	$88, %rsp
	movdqa	.LC0(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6712RegexMatcherE(%rip), %rax
	movups	%xmm0, 64(%rdi)
	movq	%rax, (%rdi)
	movl	(%r8), %eax
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movl	$0, 56(%rdi)
	movl	$256, %edi
	movups	%xmm0, 136(%r12)
	movups	%xmm0, 176(%r12)
	movups	%xmm0, 320(%r12)
	movdqa	.LC1(%rip), %xmm0
	movw	%di, 128(%r12)
	movb	$0, 130(%r12)
	movw	%r9w, 168(%r12)
	movl	%eax, 316(%r12)
	movq	%rdx, 192(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 24(%r12)
	movq	$0, 48(%r12)
	movups	%xmm1, 152(%r12)
	movups	%xmm0, 264(%r12)
	movq	$0, 280(%r12)
	movq	$0, 288(%r12)
	movq	$0, 296(%r12)
	movq	$0, 304(%r12)
	movw	%r10w, 312(%r12)
	testl	%eax, %eax
	jle	.L644
.L639:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L645
	addq	$88, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L644:
	.cfi_restore_state
	movq	%rsi, %rdi
	movq	%r8, %r13
	leaq	-112(%rbp), %rdx
	movl	%ecx, %esi
	movq	%r8, %rcx
	call	_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode@PLT
	movl	0(%r13), %edx
	movq	%rax, 16(%r12)
	testl	%edx, %edx
	jg	.L639
	movq	%rax, 8(%r12)
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexMatcher5init2EP5UTextR10UErrorCode
	jmp	.L639
.L645:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3232:
	.size	_ZN6icu_6712RegexMatcherC2EP5UTextS2_jR10UErrorCode, .-_ZN6icu_6712RegexMatcherC2EP5UTextS2_jR10UErrorCode
	.globl	_ZN6icu_6712RegexMatcherC1EP5UTextS2_jR10UErrorCode
	.set	_ZN6icu_6712RegexMatcherC1EP5UTextS2_jR10UErrorCode,_ZN6icu_6712RegexMatcherC2EP5UTextS2_jR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcherC2ERKNS_13UnicodeStringEjR10UErrorCode
	.type	_ZN6icu_6712RegexMatcherC2ERKNS_13UnicodeStringEjR10UErrorCode, @function
_ZN6icu_6712RegexMatcherC2ERKNS_13UnicodeStringEjR10UErrorCode:
.LFB3235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movl	$256, %r8d
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	%edx, %esi
	leaq	200(%r12), %rdx
	subq	$80, %rsp
	movdqa	.LC0(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6712RegexMatcherE(%rip), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, (%r12)
	movl	(%rcx), %eax
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movups	%xmm0, 112(%r12)
	movups	%xmm0, 136(%r12)
	movups	%xmm0, 176(%r12)
	movups	%xmm0, 320(%r12)
	movdqa	.LC1(%rip), %xmm0
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movl	$0, 56(%r12)
	movw	%r8w, 128(%r12)
	movb	$0, 130(%r12)
	movw	%r9w, 168(%r12)
	movl	%eax, 316(%r12)
	movq	%rdx, 192(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 24(%r12)
	movq	$0, 48(%r12)
	movq	$0, 280(%r12)
	movups	%xmm1, 152(%r12)
	movups	%xmm0, 264(%r12)
	movq	$0, 288(%r12)
	movq	$0, 296(%r12)
	movq	$0, 304(%r12)
	movw	%r10w, 312(%r12)
	testl	%eax, %eax
	jle	.L651
.L646:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L652
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	movq	%rcx, %r13
	leaq	-96(%rbp), %rdx
	call	_ZN6icu_6712RegexPattern7compileERKNS_13UnicodeStringEjR11UParseErrorR10UErrorCode@PLT
	movl	0(%r13), %edx
	movq	%rax, 16(%r12)
	testl	%edx, %edx
	jg	.L646
	movq	%rax, 8(%r12)
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	3832(%rax), %rsi
	call	_ZN6icu_6712RegexMatcher5init2EP5UTextR10UErrorCode
	jmp	.L646
.L652:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3235:
	.size	_ZN6icu_6712RegexMatcherC2ERKNS_13UnicodeStringEjR10UErrorCode, .-_ZN6icu_6712RegexMatcherC2ERKNS_13UnicodeStringEjR10UErrorCode
	.globl	_ZN6icu_6712RegexMatcherC1ERKNS_13UnicodeStringEjR10UErrorCode
	.set	_ZN6icu_6712RegexMatcherC1ERKNS_13UnicodeStringEjR10UErrorCode,_ZN6icu_6712RegexMatcherC2ERKNS_13UnicodeStringEjR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcherC2EP5UTextjR10UErrorCode
	.type	_ZN6icu_6712RegexMatcherC2EP5UTextjR10UErrorCode, @function
_ZN6icu_6712RegexMatcherC2EP5UTextjR10UErrorCode:
.LFB3238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movl	$256, %r8d
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	%edx, %esi
	leaq	200(%r12), %rdx
	subq	$80, %rsp
	movdqa	.LC0(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6712RegexMatcherE(%rip), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, (%r12)
	movl	(%rcx), %eax
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movups	%xmm0, 112(%r12)
	movups	%xmm0, 136(%r12)
	movups	%xmm0, 176(%r12)
	movups	%xmm0, 320(%r12)
	movdqa	.LC1(%rip), %xmm0
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movl	$0, 56(%r12)
	movw	%r8w, 128(%r12)
	movb	$0, 130(%r12)
	movw	%r9w, 168(%r12)
	movl	%eax, 316(%r12)
	movq	%rdx, 192(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 24(%r12)
	movq	$0, 48(%r12)
	movq	$0, 280(%r12)
	movups	%xmm1, 152(%r12)
	movups	%xmm0, 264(%r12)
	movq	$0, 288(%r12)
	movq	$0, 296(%r12)
	movq	$0, 304(%r12)
	movw	%r10w, 312(%r12)
	testl	%eax, %eax
	jle	.L658
.L653:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L659
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L658:
	.cfi_restore_state
	movq	%rcx, %r13
	leaq	-96(%rbp), %rdx
	call	_ZN6icu_6712RegexPattern7compileEP5UTextjR11UParseErrorR10UErrorCode@PLT
	movl	0(%r13), %edx
	movq	%rax, 16(%r12)
	testl	%edx, %edx
	jg	.L653
	movq	%rax, 8(%r12)
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	3832(%rax), %rsi
	call	_ZN6icu_6712RegexMatcher5init2EP5UTextR10UErrorCode
	jmp	.L653
.L659:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3238:
	.size	_ZN6icu_6712RegexMatcherC2EP5UTextjR10UErrorCode, .-_ZN6icu_6712RegexMatcherC2EP5UTextjR10UErrorCode
	.globl	_ZN6icu_6712RegexMatcherC1EP5UTextjR10UErrorCode
	.set	_ZN6icu_6712RegexMatcherC1EP5UTextjR10UErrorCode,_ZN6icu_6712RegexMatcherC2EP5UTextjR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712RegexMatcher11appendGroupEiP5UTextR10UErrorCode
	.type	_ZNK6icu_6712RegexMatcher11appendGroupEiP5UTextR10UErrorCode, @function
_ZNK6icu_6712RegexMatcher11appendGroupEiP5UTextR10UErrorCode:
.LFB3263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L660
	movq	%rdx, %r13
	movl	316(%rdi), %edx
	movq	%rdi, %r12
	movq	%rcx, %rbx
	testl	%edx, %edx
	jle	.L662
	movl	%edx, (%rcx)
.L660:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L681
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L662:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	%esi, %r14d
	call	utext_nativeLength_67@PLT
	cmpb	$0, 130(%r12)
	movq	%rax, %r15
	je	.L682
	testl	%r14d, %r14d
	js	.L664
	movq	8(%r12), %rax
	movq	136(%rax), %rdx
	movl	8(%rdx), %eax
	cmpl	%eax, %r14d
	jg	.L664
	testl	%r14d, %r14d
	je	.L683
	testl	%eax, %eax
	jle	.L677
	leal	-1(%r14), %esi
	subl	%esi, %eax
	testl	%eax, %eax
	jle	.L677
	movq	24(%rdx), %rax
	movslq	%esi, %rsi
	movslq	(%rax,%rsi,4), %rax
	leal	1(%rax), %edx
.L668:
	movq	184(%r12), %rcx
	movq	16(%rcx,%rax,8), %rsi
	movslq	%edx, %rax
	movq	16(%rcx,%rax,8), %rdx
.L667:
	testq	%rsi, %rsi
	js	.L679
	movq	32(%r12), %rdi
	cmpq	$0, 32(%rdi)
	jne	.L670
	movq	48(%r12), %rax
	cmpq	16(%rdi), %rax
	je	.L684
.L670:
	movq	56(%rdi), %rax
	cmpq	$0, 72(%rax)
	je	.L685
	leaq	-60(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movl	$0, -60(%rbp)
	call	utext_extract_67@PLT
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movl	%eax, -72(%rbp)
.L672:
	leal	1(%rax), %r8d
	movq	%rdx, -96(%rbp)
	movslq	%r8d, %rdi
	movq	%rsi, -88(%rbp)
	addq	%rdi, %rdi
	movl	%r8d, -80(%rbp)
	call	uprv_malloc_67@PLT
	movl	-80(%rbp), %r8d
	movq	-88(%rbp), %rsi
	testq	%rax, %rax
	movq	-96(%rbp), %rdx
	movq	%rax, %r14
	je	.L686
	movq	32(%r12), %rdi
	movq	%rbx, %r9
	movq	%rax, %rcx
	call	utext_extract_67@PLT
	movl	-72(%rbp), %r8d
	movq	%rbx, %r9
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	utext_replace_67@PLT
	movq	%r14, %rdi
	cltq
	movq	%rax, -72(%rbp)
	call	uprv_free_67@PLT
	movq	-72(%rbp), %rax
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L682:
	movl	$66306, (%rbx)
.L679:
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
.L680:
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	utext_replace_67@PLT
	cltq
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L664:
	movl	$8, (%rbx)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L683:
	movq	136(%r12), %rsi
	movq	144(%r12), %rdx
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L685:
	movl	%edx, %eax
	subl	%esi, %eax
	movl	%eax, -72(%rbp)
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L677:
	movl	$1, %edx
	xorl	%eax, %eax
	jmp	.L668
.L684:
	movslq	28(%rdi), %rcx
	cmpq	%rcx, %rax
	jne	.L670
	movq	48(%rdi), %rax
	movl	%edx, %r8d
	movq	%rbx, %r9
	subl	%esi, %r8d
	leaq	(%rax,%rsi,2), %rcx
	jmp	.L680
.L681:
	call	__stack_chk_fail@PLT
.L686:
	movl	$7, (%rbx)
	xorl	%eax, %eax
	jmp	.L660
	.cfi_endproc
.LFE3263:
	.size	_ZNK6icu_6712RegexMatcher11appendGroupEiP5UTextR10UErrorCode, .-_ZNK6icu_6712RegexMatcher11appendGroupEiP5UTextR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher19resetPreserveRegionEv
	.type	_ZN6icu_6712RegexMatcher19resetPreserveRegionEv, @function
_ZN6icu_6712RegexMatcher19resetPreserveRegionEv:
.LFB3289:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movb	$0, 130(%rdi)
	movups	%xmm0, 136(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movw	%ax, 168(%rdi)
	movabsq	$42949672960000, %rax
	movq	%rax, 268(%rdi)
	movups	%xmm0, 152(%rdi)
	ret
	.cfi_endproc
.LFE3289:
	.size	_ZN6icu_6712RegexMatcher19resetPreserveRegionEv, .-_ZN6icu_6712RegexMatcher19resetPreserveRegionEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher8setTraceEa
	.type	_ZN6icu_6712RegexMatcher8setTraceEa, @function
_ZN6icu_6712RegexMatcher8setTraceEa:
.LFB3294:
	.cfi_startproc
	endbr64
	movb	%sil, 313(%rdi)
	ret
	.cfi_endproc
.LFE3294:
	.size	_ZN6icu_6712RegexMatcher8setTraceEa, .-_ZN6icu_6712RegexMatcher8setTraceEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher10resetStackEv
	.type	_ZN6icu_6712RegexMatcher10resetStackEv, @function
_ZN6icu_6712RegexMatcher10resetStackEv:
.LFB3312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	176(%rdi), %rdi
	call	_ZN6icu_679UVector6417removeAllElementsEv@PLT
	movq	8(%rbx), %rax
	movq	176(%rbx), %r12
	movl	128(%rax), %r13d
	movslq	8(%r12), %rax
	movl	%r13d, %esi
	addl	%eax, %esi
	js	.L690
	cmpl	12(%r12), %esi
	jle	.L691
.L690:
	leaq	316(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L694
	movslq	8(%r12), %rax
	leal	0(%r13,%rax), %esi
.L691:
	movq	24(%r12), %rdx
	movl	%esi, 8(%r12)
	leaq	(%rdx,%rax,8), %r13
.L692:
	movl	316(%rbx), %eax
	testl	%eax, %eax
	jg	.L695
	movq	8(%rbx), %rax
	movl	128(%rax), %eax
	cmpl	$2, %eax
	jle	.L689
	subl	$3, %eax
	leaq	16(%r13), %rdi
	movl	$255, %esi
	leaq	8(,%rax,8), %rdx
	call	memset@PLT
.L689:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L695:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L694:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L692
	.cfi_endproc
.LFE3312:
	.size	_ZN6icu_6712RegexMatcher10resetStackEv, .-_ZN6icu_6712RegexMatcher10resetStackEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher15isUWordBoundaryElR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher15isUWordBoundaryElR10UErrorCode, @function
_ZN6icu_6712RegexMatcher15isUWordBoundaryElR10UErrorCode:
.LFB3315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 320(%rdi)
	je	.L703
.L698:
	cmpq	%r13, 104(%rbx)
	jg	.L700
	movb	$1, 168(%rbx)
	movl	$1, %eax
.L697:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L700:
	.cfi_restore_state
	movq	320(%rbx), %rdi
	movl	%r13d, %esi
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L703:
	.cfi_restore_state
	movq	%rdx, %r12
	call	_ZN6icu_676Locale10getEnglishEv@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 320(%rbx)
	movq	%rax, %rdi
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L701
	movq	(%rdi), %rax
	movq	32(%rbx), %rsi
	movq	%r12, %rdx
	call	*64(%rax)
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L701:
	xorl	%eax, %eax
	jmp	.L697
	.cfi_endproc
.LFE3315:
	.size	_ZN6icu_6712RegexMatcher15isUWordBoundaryElR10UErrorCode, .-_ZN6icu_6712RegexMatcher15isUWordBoundaryElR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher19followingGCBoundaryElR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher19followingGCBoundaryElR10UErrorCode, @function
_ZN6icu_6712RegexMatcher19followingGCBoundaryElR10UErrorCode:
.LFB3316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	328(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L709
.L705:
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*120(%rax)
	movslq	%eax, %r8
	cmpl	$-1, %eax
	je	.L706
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L706:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %r8
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L709:
	.cfi_restore_state
	movq	%rdx, %r13
	call	_ZN6icu_676Locale10getEnglishEv@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713BreakIterator23createCharacterInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, 328(%rbx)
	movq	%rax, %rdi
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L706
	movq	(%rdi), %rax
	movq	32(%rbx), %rsi
	movq	%r13, %rdx
	call	*64(%rax)
	movq	328(%rbx), %rdi
	jmp	.L705
	.cfi_endproc
.LFE3316:
	.size	_ZN6icu_6712RegexMatcher19followingGCBoundaryElR10UErrorCode, .-_ZN6icu_6712RegexMatcher19followingGCBoundaryElR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher13IncrementTimeER10UErrorCode
	.type	_ZN6icu_6712RegexMatcher13IncrementTimeER10UErrorCode, @function
_ZN6icu_6712RegexMatcher13IncrementTimeER10UErrorCode:
.LFB3317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	268(%rdi), %eax
	movq	%rdi, %rbx
	movl	$10000, 272(%rdi)
	leal	1(%rax), %esi
	movq	280(%rdi), %rax
	movl	%esi, 268(%rdi)
	testq	%rax, %rax
	je	.L711
	movq	288(%rdi), %rdi
	call	*%rax
	testb	%al, %al
	je	.L717
.L711:
	movl	264(%rbx), %eax
	testl	%eax, %eax
	jle	.L710
	cmpl	268(%rbx), %eax
	jg	.L710
	movl	$66322, (%r12)
.L710:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	.cfi_restore_state
	popq	%rbx
	movl	$66323, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3317:
	.size	_ZN6icu_6712RegexMatcher13IncrementTimeER10UErrorCode, .-_ZN6icu_6712RegexMatcher13IncrementTimeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher16getStaticClassIDEv
	.type	_ZN6icu_6712RegexMatcher16getStaticClassIDEv, @function
_ZN6icu_6712RegexMatcher16getStaticClassIDEv:
.LFB3321:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712RegexMatcher16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3321:
	.size	_ZN6icu_6712RegexMatcher16getStaticClassIDEv, .-_ZN6icu_6712RegexMatcher16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcherD2Ev
	.type	_ZN6icu_6712RegexMatcherD2Ev, @function
_ZN6icu_6712RegexMatcherD2Ev:
.LFB3241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712RegexMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	176(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L720
	movq	(%rdi), %rax
	call	*8(%rax)
.L720:
	movq	192(%r12), %rdi
	leaq	200(%r12), %rax
	cmpq	%rax, %rdi
	je	.L721
	call	uprv_free_67@PLT
	movq	$0, 192(%r12)
.L721:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L722
	call	_ZN6icu_6712RegexPatternD0Ev@PLT
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
.L722:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L723
	movq	(%rdi), %rax
	call	*8(%rax)
.L723:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L724
	call	utext_close_67@PLT
.L724:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L725
	call	utext_close_67@PLT
.L725:
	movq	320(%r12), %rdi
	testq	%rdi, %rdi
	je	.L726
	movq	(%rdi), %rax
	call	*8(%rax)
.L726:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L727
	movq	(%rdi), %rax
	call	*8(%rax)
.L727:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3241:
	.size	_ZN6icu_6712RegexMatcherD2Ev, .-_ZN6icu_6712RegexMatcherD2Ev
	.globl	_ZN6icu_6712RegexMatcherD1Ev
	.set	_ZN6icu_6712RegexMatcherD1Ev,_ZN6icu_6712RegexMatcherD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcherD0Ev
	.type	_ZN6icu_6712RegexMatcherD0Ev, @function
_ZN6icu_6712RegexMatcherD0Ev:
.LFB3243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6712RegexMatcherD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3243:
	.size	_ZN6icu_6712RegexMatcherD0Ev, .-_ZN6icu_6712RegexMatcherD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher14isWordBoundaryEl
	.type	_ZN6icu_6712RegexMatcher14isWordBoundaryEl, @function
_ZN6icu_6712RegexMatcher14isWordBoundaryEl:
.LFB3313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	cmpq	%rsi, 104(%rbx)
	jg	.L753
	movb	$1, 168(%rbx)
	xorl	%r12d, %r12d
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L776:
	addq	32(%rdi), %rax
	cmpq	%rax, 96(%rbx)
	jge	.L752
.L777:
	movq	32(%rbx), %rdi
	movl	40(%rdi), %eax
	testl	%eax, %eax
	jle	.L765
	movq	48(%rdi), %rdx
	movslq	%eax, %rcx
	cmpw	$-10241, -2(%rdx,%rcx,2)
	ja	.L765
	subl	$1, %eax
	movl	%eax, 40(%rdi)
	cltq
	movzwl	(%rdx,%rax,2), %r13d
.L766:
	movl	$11, %esi
	movl	%r13d, %edi
	call	u_hasBinaryProperty_67@PLT
	testb	%al, %al
	je	.L767
.L775:
	movq	32(%rbx), %rdi
.L768:
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jle	.L776
	movq	56(%rdi), %rax
	call	*64(%rax)
	cmpq	%rax, 96(%rbx)
	jl	.L777
.L752:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L765:
	.cfi_restore_state
	call	utext_previous32_67@PLT
	movl	%eax, %r13d
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L767:
	movl	%r13d, %edi
	call	u_charType_67@PLT
	cmpb	$16, %al
	je	.L775
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	movl	%r13d, %esi
	leaq	208(%rax), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	xorl	%eax, %r12d
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L753:
	movq	%rsi, %rdx
	subq	32(%rdi), %rdx
	js	.L755
	movslq	28(%rdi), %rax
	cmpq	%rdx, %rax
	jg	.L778
.L755:
	call	utext_setNativeIndex_67@PLT
	movq	32(%rbx), %rdi
	movslq	40(%rdi), %rax
.L756:
	cmpl	%eax, 44(%rdi)
	jle	.L757
	movq	48(%rdi), %rdx
	movzwl	(%rdx,%rax,2), %r12d
	cmpw	$-10241, %r12w
	ja	.L757
.L758:
	movl	$11, %esi
	movl	%r12d, %edi
	call	u_hasBinaryProperty_67@PLT
	testb	%al, %al
	jne	.L761
	movl	%r12d, %edi
	call	u_charType_67@PLT
	cmpb	$16, %al
	je	.L761
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	movl	%r12d, %esi
	leaq	208(%rax), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	%eax, %r12d
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L757:
	call	utext_current32_67@PLT
	movl	%eax, %r12d
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L761:
	addq	$8, %rsp
	xorl	%r12d, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L778:
	.cfi_restore_state
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L755
	movl	%edx, 40(%rdi)
	movslq	%edx, %rax
	jmp	.L756
	.cfi_endproc
.LFE3313:
	.size	_ZN6icu_6712RegexMatcher14isWordBoundaryEl, .-_ZN6icu_6712RegexMatcher14isWordBoundaryEl
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher19isChunkWordBoundaryEi
	.type	_ZN6icu_6712RegexMatcher19isChunkWordBoundaryEi, @function
_ZN6icu_6712RegexMatcher19isChunkWordBoundaryEi:
.LFB3314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	%esi, %rbx
	movq	%rbx, %rdx
	subq	$24, %rsp
	movq	32(%rdi), %rax
	movq	48(%rax), %r14
	movq	104(%rdi), %rax
	cmpq	%rax, %rbx
	jl	.L780
	movb	$1, 168(%rdi)
	xorl	%r15d, %r15d
.L781:
	movq	96(%r13), %rcx
	cmpq	%rcx, %rbx
	jg	.L788
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L789:
	movl	$11, %esi
	movl	%r12d, %edi
	call	u_hasBinaryProperty_67@PLT
	testb	%al, %al
	je	.L798
.L790:
	movq	96(%r13), %rcx
	movslq	%ebx, %rax
	cmpq	%rcx, %rax
	jle	.L779
	movl	%ebx, %edx
.L788:
	leal	-1(%rdx), %ebx
	movslq	%ebx, %rax
	movzwl	(%r14,%rax,2), %r12d
	leaq	(%rax,%rax), %rdi
	movl	%r12d, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L789
	cmpq	%rcx, %rax
	jle	.L789
	movzwl	-2(%r14,%rdi), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L789
	sall	$10, %eax
	leal	-2(%rdx), %ebx
	leal	-56613888(%r12,%rax), %r12d
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L798:
	movl	%r12d, %edi
	call	u_charType_67@PLT
	cmpb	$16, %al
	je	.L790
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	movl	%r12d, %esi
	leaq	208(%rax), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	xorl	%eax, %r15d
.L779:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L780:
	.cfi_restore_state
	movzwl	(%r14,%rbx,2), %r12d
	leaq	(%rbx,%rbx), %rdi
	movl	%r12d, %esi
	movl	%r12d, %ecx
	andl	$63488, %esi
	cmpl	$55296, %esi
	je	.L799
.L782:
	movl	$11, %esi
	movl	%r12d, %edi
	movl	%edx, -52(%rbp)
	call	u_hasBinaryProperty_67@PLT
	movl	-52(%rbp), %edx
	testb	%al, %al
	jne	.L786
	movl	%r12d, %edi
	movl	%edx, -52(%rbp)
	call	u_charType_67@PLT
	cmpb	$16, %al
	je	.L786
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	movl	%r12d, %esi
	leaq	208(%rax), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	-52(%rbp), %edx
	movl	%eax, %r15d
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L786:
	xorl	%r15d, %r15d
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L799:
	andb	$4, %ch
	jne	.L783
	leal	1(%rbx), %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rax
	je	.L782
	movzwl	2(%r14,%rdi), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L782
	sall	$10, %r12d
	leal	-56613888(%rax,%r12), %r12d
	jmp	.L782
.L783:
	cmpq	96(%r13), %rbx
	jle	.L782
	movzwl	-2(%r14,%rdi), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L782
	sall	$10, %eax
	leal	-56613888(%r12,%rax), %r12d
	jmp	.L782
	.cfi_endproc
.LFE3314:
	.size	_ZN6icu_6712RegexMatcher19isChunkWordBoundaryEi, .-_ZN6icu_6712RegexMatcher19isChunkWordBoundaryEi
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB2:
	.text
.LHOTB2:
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0, @function
_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0:
.LFB4497:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%rcx, -128(%rbp)
	movb	%dl, -161(%rbp)
	movq	%rsi, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	32(%rax), %rdx
	movq	24(%rdx), %rcx
	movzwl	48(%rax), %edx
	movq	%rcx, -120(%rbp)
	testb	$17, %dl
	jne	.L1467
	andl	$2, %edx
	jne	.L1752
	movq	64(%rax), %rcx
	movq	%rcx, -152(%rbp)
.L801:
	movq	104(%rax), %rcx
	movl	128(%rax), %eax
	movq	176(%rbx), %rdi
	movl	%eax, 56(%rbx)
	movq	%rcx, -160(%rbp)
	call	_ZN6icu_679UVector6417removeAllElementsEv@PLT
	movq	8(%rbx), %rax
	movq	176(%rbx), %r13
	movl	128(%rax), %r12d
	movslq	8(%r13), %rax
	movl	%r12d, %esi
	addl	%eax, %esi
	js	.L803
	cmpl	12(%r13), %esi
	jle	.L804
.L803:
	leaq	316(%rbx), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L1468
	movslq	8(%r13), %rax
	leal	(%r12,%rax), %esi
.L804:
	movq	24(%r13), %rdx
	movl	%esi, 8(%r13)
	leaq	(%rdx,%rax,8), %r15
.L805:
	movl	316(%rbx), %eax
	testl	%eax, %eax
	jg	.L806
	movq	8(%rbx), %r12
	movl	128(%r12), %eax
	cmpl	$2, %eax
	jle	.L807
	subl	$3, %eax
	leaq	16(%r15), %rdi
	movl	$255, %esi
	leaq	8(,%rax,8), %rdx
	call	memset@PLT
.L807:
	movq	-144(%rbp), %rax
	movq	$0, 8(%r15)
	movq	%rax, (%r15)
	movl	132(%r12), %eax
	testl	%eax, %eax
	jle	.L1753
	subl	$1, %eax
	movq	192(%rbx), %rdi
	xorl	%esi, %esi
	leaq	8(,%rax,8), %rdx
	call	memset@PLT
	movq	8(%r15), %rdx
.L1464:
	movq	%rbx, %r12
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	-120(%rbp), %rax
	leaq	1(%rdx), %rbx
	leaq	0(,%rdx,8), %rdi
	movq	(%rax,%rdx,8), %rax
	movq	%rbx, 8(%r15)
	movl	%eax, %r13d
	movl	%eax, %esi
	movl	%eax, %r14d
	andl	$16777215, %r13d
	shrl	$24, %esi
	cmpl	$989855743, %eax
	ja	.L808
	leaq	.L810(%rip), %rcx
	movslq	(%rcx,%rsi,4), %rsi
	addq	%rcx, %rsi
	notrack jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L810:
	.long	.L808-.L810
	.long	.L865-.L810
	.long	.L864-.L810
	.long	.L863-.L810
	.long	.L862-.L810
	.long	.L808-.L810
	.long	.L861-.L810
	.long	.L860-.L810
	.long	.L859-.L810
	.long	.L858-.L810
	.long	.L857-.L810
	.long	.L856-.L810
	.long	.L855-.L810
	.long	.L854-.L810
	.long	.L853-.L810
	.long	.L852-.L810
	.long	.L851-.L810
	.long	.L850-.L810
	.long	.L849-.L810
	.long	.L848-.L810
	.long	.L847-.L810
	.long	.L846-.L810
	.long	.L845-.L810
	.long	.L844-.L810
	.long	.L843-.L810
	.long	.L842-.L810
	.long	.L841-.L810
	.long	.L840-.L810
	.long	.L839-.L810
	.long	.L838-.L810
	.long	.L837-.L810
	.long	.L808-.L810
	.long	.L836-.L810
	.long	.L835-.L810
	.long	.L834-.L810
	.long	.L833-.L810
	.long	.L832-.L810
	.long	.L831-.L810
	.long	.L830-.L810
	.long	.L829-.L810
	.long	.L828-.L810
	.long	.L827-.L810
	.long	.L826-.L810
	.long	.L825-.L810
	.long	.L824-.L810
	.long	.L823-.L810
	.long	.L822-.L810
	.long	.L821-.L810
	.long	.L820-.L810
	.long	.L819-.L810
	.long	.L818-.L810
	.long	.L817-.L810
	.long	.L816-.L810
	.long	.L815-.L810
	.long	.L814-.L810
	.long	.L813-.L810
	.long	.L812-.L810
	.long	.L811-.L810
	.long	.L809-.L810
	.text
	.p2align 4,,10
	.p2align 3
.L1752:
	leaq	50(%rax), %rcx
	movq	%rcx, -152(%rbp)
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L838:
	movq	-120(%rbp), %rbx
	movslq	%r13d, %rax
	leaq	0(,%rax,8), %rdx
	movq	(%rbx,%rax,8), %rax
	movq	24(%rbx,%rdx), %rdi
	movq	16(%rbx,%rdx), %rsi
	andl	$16777215, %eax
	movl	%edi, %r9d
	leaq	(%r15,%rax,8), %rdx
	movq	16(%rdx), %rax
	addq	$1, %rax
	movq	%rax, 16(%rdx)
	cmpq	%r9, %rax
	jnb	.L1754
	movslq	%esi, %rsi
	cmpq	%rsi, %rax
	jl	.L1457
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	cmpl	$-1, %edi
	je	.L1458
.L1212:
	testl	%eax, %eax
	jle	.L1755
	.p2align 4,,10
	.p2align 3
.L853:
	movq	%r12, %rbx
.L1748:
	movb	$0, 130(%rbx)
.L1453:
	movq	%r15, 184(%rbx)
.L800:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1756
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L852:
	.cfi_restore_state
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L1757
.L1117:
	movslq	%r13d, %r13
	movq	%r13, 8(%r15)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L809:
	movq	(%r15), %rsi
	cmpq	120(%r12), %rsi
	jl	.L1022
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movb	$1, 168(%r12)
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1023
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1023:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L814:
	movq	(%r15), %rsi
	cmpq	88(%r12), %rsi
	jge	.L1758
	movq	32(%r12), %rdi
	movq	%rsi, %rax
	subq	32(%rdi), %rax
	js	.L930
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jl	.L1759
.L930:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %edx
.L931:
	cmpl	%edx, 44(%rdi)
	jle	.L932
	movq	48(%rdi), %rsi
	movslq	%edx, %rax
	movzwl	(%rsi,%rax,2), %eax
	cmpw	$-10241, %ax
	jbe	.L1760
.L932:
	call	utext_next32_67@PLT
.L933:
	cmpl	$10, %eax
	je	.L934
.L940:
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L935
	xorl	%esi, %esi
	xorl	%edx, %edx
.L935:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rdi)
	leaq	(%r8,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L815:
	cmpq	$0, 320(%r12)
	movq	(%r15), %rbx
	je	.L1761
.L980:
	cmpq	104(%r12), %rbx
	jge	.L1762
	movq	320(%r12), %rdi
	movl	%ebx, %esi
	movq	(%rdi), %rax
	call	*136(%rax)
.L983:
	testl	%r13d, %r13d
	setne	%dl
	cmpb	%al, %dl
	je	.L984
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L816:
	movq	(%r15), %rcx
	testb	$1, %al
	je	.L1396
	movb	$1, 168(%r12)
	movq	120(%r12), %r13
.L1397:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	cmpq	%rcx, %r13
	je	.L1763
	movq	-120(%rbp), %rdi
	movq	(%rdi,%rbx,8), %rdi
	movq	%rdi, %rdx
	movq	%rdi, -136(%rbp)
	andl	$16777215, %edx
	movq	%rcx, 16(%r15,%rdx,8)
	movq	%r13, (%r15)
	testl	%eax, %eax
	jle	.L1764
.L1411:
	addq	$1, %rbx
	movq	%rbx, 8(%r15)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L817:
	movslq	%r13d, %r13
	movq	(%r15), %rsi
	movq	16(%r15,%r13,8), %rbx
	cmpq	%rsi, %rbx
	je	.L1765
	movq	32(%r12), %rdi
	movq	%rsi, %rdx
	subq	32(%rdi), %rdx
	js	.L1427
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jge	.L1427
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	jbe	.L1766
.L1427:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L1428:
	testl	%eax, %eax
	jle	.L1429
	movq	48(%rdi), %rdx
	movslq	%eax, %rsi
	cmpw	$-10241, -2(%rdx,%rsi,2)
	ja	.L1429
	subl	$1, %eax
	movslq	%eax, %rsi
	movl	%eax, 40(%rdi)
	movzwl	(%rdx,%rsi,2), %r13d
.L1430:
	cmpl	%eax, 28(%rdi)
	jl	.L1431
	movslq	%eax, %rdx
	addq	32(%rdi), %rdx
.L1432:
	movq	%rdx, (%r15)
	testl	%eax, %eax
	jle	.L1433
	movq	48(%rdi), %rdx
	movslq	%eax, %rsi
	cmpw	$-10241, -2(%rdx,%rsi,2)
	ja	.L1433
	subl	$1, %eax
	movl	%eax, 40(%rdi)
	cltq
	movzwl	(%rdx,%rax,2), %eax
.L1434:
	movq	8(%r15), %r14
	cmpl	$10, %r13d
	je	.L1767
.L1435:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L853
	movq	176(%r12), %r13
	movl	56(%r12), %ebx
	movslq	8(%r13), %rdx
	movl	%ebx, %esi
	addl	%edx, %esi
	js	.L1438
	cmpl	12(%r13), %esi
	jle	.L1439
.L1438:
	movq	-128(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L1768
	movq	-128(%rbp), %rax
	movslq	8(%r13), %rdx
	movl	(%rax), %eax
	leal	(%rbx,%rdx), %esi
.L1439:
	movq	24(%r13), %rdi
	movl	%esi, 8(%r13)
	leaq	(%rdi,%rdx,8), %rbx
.L1441:
	testl	%eax, %eax
	jg	.L1217
	movslq	56(%r12), %rdx
	movq	%rbx, %r13
	leaq	16(%rbx), %rsi
	movl	$16, %ecx
	salq	$3, %rdx
	subq	%rdx, %r13
	leaq	-8(%rdx), %rdi
	shrq	$3, %rdi
	cmpq	%rsi, %r13
	setnb	%sil
	subq	%rdx, %rcx
	testq	%rcx, %rcx
	setle	%dl
	orb	%dl, %sil
	je	.L1532
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L1532
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1443:
	movdqu	0(%r13,%rdx), %xmm6
	movups	%xmm6, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L1443
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	leaq	0(%r13,%rsi), %rdx
	addq	%rbx, %rsi
	cmpq	%rdi, %r8
	je	.L1445
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L1445:
	movl	272(%r12), %ecx
	leal	-1(%rcx), %edx
	testl	%edx, %edx
	jle	.L1446
	movl	%edx, 272(%r12)
.L1447:
	subq	$1, %r14
	movq	%rbx, %r15
	movq	%r14, 8(%r13)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L812:
	movq	(%r15), %rsi
	cmpq	120(%r12), %rsi
	jl	.L999
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movb	$1, 168(%r12)
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1000
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1000:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L813:
	movq	(%r15), %rsi
	cmpq	88(%r12), %rsi
	jge	.L1769
	movq	32(%r12), %rdi
	movq	%rsi, %rdx
	subq	32(%rdi), %rdx
	js	.L954
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jl	.L1770
.L954:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
.L955:
	cmpl	%eax, 44(%rdi)
	jle	.L956
	movq	48(%rdi), %rdx
	movzwl	(%rdx,%rax,2), %eax
	cmpw	$-10241, %ax
	ja	.L956
.L957:
	cmpl	$10, %eax
	je	.L1771
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	js	.L1772
.L959:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rdi)
	leaq	(%r8,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L811:
	movq	(%r15), %rsi
	cmpq	120(%r12), %rsi
	jl	.L1009
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movb	$1, 168(%r12)
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1010
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1010:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L818:
	movq	8(%r12), %rax
	movq	-160(%rbp), %rdi
	movslq	%r13d, %rbx
	movl	%r13d, %esi
	salq	$5, %rbx
	addq	112(%rax), %rbx
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	(%r15), %r14
	movq	32(%r12), %rdi
	movq	%rax, %r13
	movq	%r14, %rax
	subq	32(%rdi), %rax
	js	.L1368
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jge	.L1368
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	jbe	.L1773
.L1368:
	movq	%r14, %rsi
	call	utext_setNativeIndex_67@PLT
.L1369:
	cmpq	120(%r12), %r14
	jge	.L1370
	.p2align 4,,10
	.p2align 3
.L1747:
	movq	32(%r12), %rdi
.L1371:
	movl	40(%rdi), %eax
	cmpl	44(%rdi), %eax
	jge	.L1373
	movq	48(%rdi), %rcx
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %esi
	cmpw	$-10241, %si
	ja	.L1373
	addl	$1, %eax
	movl	%eax, 40(%rdi)
	cmpl	$255, %esi
	jg	.L1375
.L1861:
	movl	%esi, %eax
	andl	$7, %esi
	sarl	$3, %eax
	cltq
	movsbl	(%rbx,%rax), %eax
	btl	%esi, %eax
	jnc	.L1372
	movq	32(%r12), %rdi
	movslq	40(%rdi), %r14
	cmpl	28(%rdi), %r14d
	jg	.L1774
.L1376:
	addq	32(%rdi), %r14
	cmpq	120(%r12), %r14
	jl	.L1371
.L1370:
	movb	$1, 168(%r12)
.L1372:
	movq	-128(%rbp), %rax
	movq	(%r15), %rdx
	movq	8(%r15), %rbx
	movl	(%rax), %eax
	cmpq	%r14, %rdx
	je	.L1775
	movq	-120(%rbp), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	%rcx, %rsi
	movq	%rcx, -136(%rbp)
	andl	$16777215, %esi
	movq	%rdx, 16(%r15,%rsi,8)
	movq	%r14, (%r15)
	testl	%eax, %eax
	jle	.L1776
.L1381:
	leaq	1(%rbx), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L819:
	movq	(%r15), %rsi
	cmpq	120(%r12), %rsi
	jl	.L1053
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movb	$1, 168(%r12)
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1054
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1054:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L820:
	movq	176(%r12), %r9
	movq	120(%r12), %rax
	cmpq	%rax, (%r15)
	je	.L1365
	movslq	56(%r12), %rax
	movl	8(%r9), %edx
	movq	24(%r9), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L1366
	xorl	%esi, %esi
	xorl	%edx, %edx
.L1366:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%r9)
	leaq	(%rdi,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L821:
	movq	32(%r12), %r9
	movq	-120(%rbp), %rbx
	leaq	2(%rdx), %rax
	leaq	3(%rdx), %rsi
	movq	%rax, 8(%r15)
	movq	56(%r9), %r10
	movq	8(%rbx,%rdi), %rax
	movq	%rsi, 8(%r15)
	movq	16(%rbx,%rdi), %rsi
	cmpq	$0, 72(%r10)
	movl	%esi, %r14d
	je	.L1333
	leal	(%rsi,%rsi,2), %r14d
.L1333:
	addq	$4, %rdx
	movslq	%r13d, %r13
	movq	-120(%rbp), %rbx
	movq	%rdx, 8(%r15)
	movq	192(%r12), %rdx
	leaq	32(,%r13,8), %rcx
	movq	24(%rbx,%rdi), %rbx
	leaq	(%rdx,%rcx), %r13
	movq	0(%r13), %rsi
	testq	%rsi, %rsi
	js	.L1777
	jne	.L1340
	movq	-128(%rbp), %rax
	movq	$-1, 0(%r13)
	movl	(%rax), %eax
.L1341:
	movq	-16(%rdx,%rcx), %rsi
	andl	$16777215, %ebx
	movq	%rsi, 112(%r12)
	movq	-8(%rdx,%rcx), %rdx
	movq	%rdx, 120(%r12)
	movq	%rbx, 8(%r15)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L822:
	movq	120(%r12), %rax
	cmpq	%rax, (%r15)
	je	.L1331
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L1332
	xorl	%esi, %esi
	xorl	%edx, %edx
.L1332:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rdi)
	leaq	(%r8,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L824:
	movq	176(%r12), %rsi
	movq	192(%r12), %rax
	movslq	%r13d, %r13
	leaq	0(,%r13,8), %rdx
	movslq	8(%rsi), %rsi
	movq	%rsi, (%rax,%r13,8)
	movq	(%r15), %rsi
	movq	%rsi, 8(%rax,%rdx)
	movq	112(%r12), %rsi
	movq	%rsi, 16(%rax,%rdx)
	movq	120(%r12), %rsi
	movq	%rsi, 24(%rax,%rdx)
	movq	64(%r12), %rsi
	movq	%rsi, 112(%r12)
	movq	(%r15), %rsi
	movq	%rsi, 120(%r12)
	movq	$-1, 32(%rax,%rdx)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	.p2align 4,,10
	.p2align 3
.L867:
	testl	%eax, %eax
	jg	.L853
	movq	8(%r15), %rdx
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L825:
	movq	(%r15), %rsi
	cmpq	80(%r12), %rsi
	je	.L1778
	movq	32(%r12), %rdi
	movq	%rsi, %rdx
	subq	32(%rdi), %rdx
	js	.L963
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jge	.L963
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	jbe	.L1779
.L963:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L964:
	testl	%eax, %eax
	jle	.L965
	movq	48(%rdi), %rdx
	movslq	%eax, %rsi
	cmpw	$-10241, -2(%rdx,%rsi,2)
	ja	.L965
	subl	$1, %eax
	movl	%eax, 40(%rdi)
	cltq
	movzwl	(%rdx,%rax,2), %eax
.L966:
	movq	88(%r12), %rbx
	cmpq	%rbx, (%r15)
	jge	.L967
	testl	$-8368, %eax
	jne	.L967
	leal	-10(%rax), %edx
	cmpl	$3, %edx
	jbe	.L1534
	cmpl	$133, %eax
	je	.L1534
	subl	$8232, %eax
	cmpl	$1, %eax
	ja	.L967
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L823:
	leaq	2(%rdx), %rax
	addq	$3, %rdx
	movq	%rax, 8(%r15)
	movq	-120(%rbp), %rax
	movq	8(%rax,%rdi), %r9
	movq	%rdx, 8(%r15)
	movq	16(%rax,%rdi), %rax
	movq	32(%r12), %rdi
	movq	56(%rdi), %rdx
	movl	%eax, %r14d
	cmpq	$0, 72(%rdx)
	je	.L1299
	leal	(%rax,%rax,2), %r14d
.L1299:
	movslq	%r13d, %r13
	leaq	32(,%r13,8), %rbx
	movq	192(%r12), %r13
	addq	%rbx, %r13
	movq	0(%r13), %rsi
	testq	%rsi, %rsi
	js	.L1780
	jne	.L1306
	movq	$-1, 0(%r13)
.L1307:
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L1315
	xorl	%esi, %esi
	xorl	%edx, %edx
.L1315:
	movl	%edx, 8(%rdi)
	subq	%rax, %rsi
	movq	192(%r12), %rax
	leaq	(%r8,%rsi,8), %r15
	movq	-16(%rax,%rbx), %rdx
	movq	%rdx, 112(%r12)
	movq	-8(%rax,%rbx), %rax
	movq	%rax, 120(%r12)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L826:
	movq	(%r15), %rsi
	cmpq	88(%r12), %rsi
	jge	.L1781
	movq	32(%r12), %rdi
	movq	%rsi, %rdx
	subq	32(%rdi), %rdx
	js	.L942
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jl	.L1782
.L942:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
.L943:
	cmpl	%eax, 44(%rdi)
	jle	.L944
	movq	48(%rdi), %rdx
	movzwl	(%rdx,%rax,2), %eax
	cmpw	$-10241, %ax
	ja	.L944
.L945:
	testl	$-8368, %eax
	jne	.L946
	leal	-10(%rax), %edx
	cmpl	$3, %edx
	jbe	.L947
	cmpl	$133, %eax
	je	.L947
	leal	-8232(%rax), %edx
	cmpl	$1, %edx
	ja	.L946
.L947:
	cmpl	$10, %eax
	je	.L948
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L830:
	movq	192(%r12), %rax
	movq	176(%r12), %rdi
	movslq	%r13d, %r13
	leaq	0(,%r13,8), %rbx
	movq	(%rax,%r13,8), %rsi
	cmpl	%esi, 8(%rdi)
	jg	.L1783
.L1264:
	movq	8(%rax,%rbx), %rdx
	movq	%rdx, (%r15)
	movq	16(%rax,%rbx), %rdx
	movq	%rdx, 112(%r12)
	movq	24(%rax,%rbx), %rax
	movq	%rax, 120(%r12)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L831:
	movq	176(%r12), %rsi
	movq	192(%r12), %rax
	movslq	%r13d, %r13
	leaq	0(,%r13,8), %rdx
	movslq	8(%rsi), %rsi
	movq	%rsi, (%rax,%r13,8)
	movq	(%r15), %rsi
	movq	%rsi, 8(%rax,%rdx)
	movq	112(%r12), %rsi
	movq	%rsi, 16(%rax,%rdx)
	movq	120(%r12), %rsi
	movq	%rsi, 24(%rax,%rdx)
	movq	-128(%rbp), %rax
	movdqu	96(%r12), %xmm6
	movl	(%rax), %eax
	movups	%xmm6, 112(%r12)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L828:
	movq	-152(%rbp), %rax
	movslq	%r13d, %r13
	movq	(%r15), %rsi
	addq	$2, %rdx
	leaq	(%rax,%r13,2), %rax
	movq	%rax, -136(%rbp)
	movq	-120(%rbp), %rax
	movq	8(%rax,%rdi), %rbx
	movq	32(%r12), %rdi
	movq	%rdx, 8(%r15)
	movl	%ebx, %eax
	andl	$16777215, %eax
	movl	%eax, %ebx
	movq	%rsi, %rax
	subq	32(%rdi), %rax
	js	.L1282
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jge	.L1282
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	jbe	.L1784
.L1282:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
.L1283:
	leaq	-80(%rbp), %r14
	movq	%rdi, %rsi
	xorl	%r13d, %r13d
	movq	%r14, %rdi
	call	_ZN6icu_6724CaseFoldingUTextIteratorC1ER5UText@PLT
	movq	%r15, -176(%rbp)
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1787:
	addq	32(%rdi), %rax
.L1290:
	cmpq	%rax, 120(%r12)
	jle	.L1785
.L1291:
	movq	-136(%rbp), %rdi
	movslq	%r13d, %rax
	leal	1(%r13), %ecx
	leaq	(%rax,%rax), %rsi
	movzwl	(%rdi,%rax,2), %r15d
	movl	%r15d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L1521
	cmpl	%ecx, %ebx
	jne	.L1286
.L1521:
	movl	%ecx, %r13d
.L1288:
	movq	%r14, %rdi
	call	_ZN6icu_6724CaseFoldingUTextIterator4nextEv@PLT
	cmpl	%r15d, %eax
	jne	.L1786
.L1294:
	cmpl	%r13d, %ebx
	jle	.L1284
	movq	%r14, %rdi
	call	_ZN6icu_6724CaseFoldingUTextIterator11inExpansionEv@PLT
	testb	%al, %al
	jne	.L1291
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jle	.L1787
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L1290
	.p2align 4,,10
	.p2align 3
.L829:
	movq	(%r15), %rsi
	cmpq	120(%r12), %rsi
	jl	.L1788
	movb	$1, 168(%r12)
.L1278:
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L1281
	xorl	%esi, %esi
	xorl	%edx, %edx
.L1281:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rdi)
	leaq	(%r8,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L827:
	movslq	%r13d, %r13
	leaq	(%r15,%r13,8), %rax
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	jns	.L1248
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1249
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1249:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L832:
	movq	-120(%rbp), %rax
	movslq	%ebx, %rbx
	addq	$2, %rdx
	movq	%rdx, 8(%r15)
	movq	(%rax,%rbx,8), %rax
	movq	(%r15), %rbx
	andl	$16777215, %eax
	cmpq	%rbx, 16(%r15,%rax,8)
	jl	.L1789
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1263
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1263:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L833:
	movq	(%r15), %rax
	movslq	%r13d, %r13
	movq	%rax, 16(%r15,%r13,8)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L850:
	cmpb	$0, 130(%r12)
	movq	(%r15), %rax
	je	.L996
	cmpq	%rax, 144(%r12)
	je	.L1790
.L997:
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L998
	xorl	%esi, %esi
	xorl	%edx, %edx
.L998:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rdi)
	leaq	(%r8,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L851:
	movq	(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexMatcher14isWordBoundaryEl
	testl	%r13d, %r13d
	movl	%eax, %r9d
	setne	%al
	cmpb	%al, %r9b
	je	.L978
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L854:
	movq	-128(%rbp), %rax
	movslq	%r13d, %r13
	movq	%r13, 8(%r15)
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L855:
	movq	(%r15), %rsi
	cmpq	120(%r12), %rsi
	jl	.L1079
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movb	$1, 168(%r12)
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1080
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1080:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L856:
	movq	(%r15), %rsi
	cmpq	120(%r12), %rsi
	jl	.L1066
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movb	$1, 168(%r12)
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1067
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1067:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L857:
	movq	(%r15), %rsi
	cmpq	120(%r12), %rsi
	jl	.L1041
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movb	$1, 168(%r12)
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1042
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1042:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L858:
	leal	2(%r13), %eax
	movslq	%r13d, %r13
	cltq
	movq	16(%r15,%rax,8), %xmm0
	movq	-128(%rbp), %rax
	movhps	(%r15), %xmm0
	movl	(%rax), %eax
	movups	%xmm0, 16(%r15,%r13,8)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L859:
	movq	(%r15), %rax
	addl	$2, %r13d
	movslq	%r13d, %r13
	movq	%rax, 16(%r15,%r13,8)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L860:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L861:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L853
	movq	176(%r12), %r14
	movl	56(%r12), %ebx
	movslq	8(%r14), %rdx
	movl	%ebx, %esi
	addl	%edx, %esi
	js	.L891
	cmpl	12(%r14), %esi
	jle	.L892
.L891:
	movq	-128(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L1791
	movq	-128(%rbp), %rax
	movslq	8(%r14), %rdx
	movl	(%rax), %eax
	leal	(%rbx,%rdx), %esi
.L892:
	movq	24(%r14), %rdi
	movl	%esi, 8(%r14)
	leaq	(%rdi,%rdx,8), %rbx
.L894:
	testl	%eax, %eax
	jg	.L1217
	movslq	56(%r12), %rdx
	movq	%rbx, %r14
	leaq	16(%rbx), %rsi
	movl	$16, %ecx
	salq	$3, %rdx
	subq	%rdx, %r14
	leaq	-8(%rdx), %rdi
	shrq	$3, %rdi
	cmpq	%rsi, %r14
	setnb	%sil
	subq	%rdx, %rcx
	testq	%rcx, %rcx
	setle	%dl
	orb	%dl, %sil
	je	.L1472
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L1472
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L898:
	movdqu	(%r14,%rdx), %xmm4
	movups	%xmm4, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L898
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	leaq	(%r14,%rsi), %rdx
	addq	%rbx, %rsi
	cmpq	%rdi, %r8
	je	.L900
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L900:
	movl	272(%r12), %ecx
	leal	-1(%rcx), %edx
	testl	%edx, %edx
	jle	.L901
	movl	%edx, 272(%r12)
.L902:
	movslq	%r13d, %r13
	movq	%rbx, %r15
	movq	%r13, 8(%r14)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L862:
	movq	-120(%rbp), %rax
	movq	(%r15), %rsi
	addq	$2, %rdx
	movslq	%r13d, %r13
	movq	8(%rax,%rdi), %rbx
	movq	-152(%rbp), %rax
	movq	%rdx, 8(%r15)
	movq	%rsi, %r9
	movq	32(%r12), %rdi
	andl	$16777215, %ebx
	leaq	(%rax,%r13,2), %r13
	subq	32(%rdi), %r9
	js	.L877
	movslq	28(%rdi), %rax
	movq	%rax, %rdx
	cmpq	%rax, %r9
	jge	.L877
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%r9,2)
	jbe	.L1792
.L877:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	movl	28(%rdi), %edx
.L878:
	xorl	%r14d, %r14d
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L1795:
	addq	32(%rdi), %rax
.L881:
	cmpq	%rax, 120(%r12)
	jle	.L1793
	movq	32(%r12), %rdi
	movl	40(%rdi), %edx
	cmpl	44(%rdi), %edx
	jge	.L884
	movq	48(%rdi), %rcx
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %eax
	cmpw	$-10241, %ax
	ja	.L884
	addl	$1, %edx
	movl	%edx, 40(%rdi)
.L885:
	movslq	%r14d, %rdx
	leal	1(%r14), %ecx
	leaq	(%rdx,%rdx), %rdi
	movzwl	0(%r13,%rdx,2), %edx
	movl	%edx, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L886
	cmpl	%ecx, %ebx
	jne	.L1794
.L886:
	cmpl	%eax, %edx
	jne	.L883
	movq	32(%r12), %rdi
	movl	%ecx, %r14d
	movslq	40(%rdi), %rax
	movl	28(%rdi), %edx
.L887:
	cmpl	%r14d, %ebx
	jle	.L879
	cmpl	%edx, %eax
	jle	.L1795
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L863:
	movq	(%r15), %rsi
	cmpq	120(%r12), %rsi
	jl	.L1796
	movb	$1, 168(%r12)
.L873:
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L876
	xorl	%esi, %esi
	xorl	%edx, %edx
.L876:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rdi)
	leaq	(%r8,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L864:
	cmpb	$0, -161(%rbp)
	movq	(%r15), %rax
	je	.L906
	cmpq	%rax, 120(%r12)
	je	.L906
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	js	.L1797
.L907:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rdi)
	leaq	(%r8,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L865:
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	js	.L1798
.L866:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rdi)
	leaq	(%r8,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L834:
	movslq	%r13d, %r13
	leaq	(%r15,%r13,8), %rax
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	jns	.L1236
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1237
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1237:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L842:
	movslq	%ebx, %rbx
	movq	-120(%rbp), %rdi
	movslq	%r13d, %rax
	addq	$4, %rdx
	movq	$0, 16(%r15,%rax,8)
	addq	$1, %rbx
	leaq	0(,%rbx,8), %rax
	movq	%rdx, 8(%r15)
	movl	(%rdi,%rbx,8), %edx
	movq	8(%rdi,%rax), %r14
	movl	%r14d, %ecx
	testl	%edx, %edx
	jne	.L1147
	movq	-128(%rbp), %rbx
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L1799
.L1147:
	cmpl	$-1, %r14d
	je	.L1800
	testl	%r14d, %r14d
	je	.L1162
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L843:
	movq	(%r15), %rsi
	cmpq	88(%r12), %rsi
	jge	.L1801
	movq	32(%r12), %rdi
	movq	%rsi, %rdx
	subq	32(%rdi), %rdx
	js	.L909
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jl	.L1802
.L909:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L910:
	cmpl	%eax, 44(%rdi)
	jle	.L911
	movq	48(%rdi), %rsi
	movslq	%eax, %rdx
	movzwl	(%rsi,%rdx,2), %ebx
	cmpw	$-10241, %bx
	jbe	.L1803
.L911:
	call	utext_next32_67@PLT
	movq	32(%r12), %rdi
	movl	%eax, %ebx
	movl	40(%rdi), %eax
.L912:
	cmpl	%eax, 28(%rdi)
	jl	.L913
	cltq
	addq	32(%rdi), %rax
.L914:
	cmpq	%rax, 88(%r12)
	jg	.L915
	testl	$-8368, %ebx
	jne	.L916
	leal	-10(%rbx), %eax
	cmpl	$3, %eax
	jbe	.L917
	cmpl	$133, %ebx
	je	.L917
	leal	-8232(%rbx), %eax
	cmpl	$1, %eax
	ja	.L916
.L917:
	cmpl	$10, %ebx
	je	.L918
.L919:
	movq	-128(%rbp), %rax
	movl	$257, %r14d
	movw	%r14w, 168(%r12)
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L839:
	movq	-120(%rbp), %rcx
	movslq	%r13d, %rax
	leaq	0(,%rax,8), %rdx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rcx,%rdx), %rdi
	movq	16(%rcx,%rdx), %rsi
	andl	$16777215, %eax
	movl	%edi, %r9d
	leaq	(%r15,%rax,8), %rdx
	movq	16(%rdx), %rax
	addq	$1, %rax
	movq	%rax, 16(%rdx)
	cmpq	%r9, %rax
	jnb	.L1804
	movslq	%esi, %rsi
	cmpq	%rsi, %rax
	jl	.L1166
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	cmpl	$-1, %edi
	je	.L1456
.L1167:
	testl	%eax, %eax
	jle	.L1805
.L1168:
	leal	4(%r13), %edx
	movslq	%edx, %rdx
	movq	%rdx, 8(%r15)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L840:
	movq	(%r15), %rsi
	cmpq	120(%r12), %rsi
	jl	.L1107
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movb	$1, 168(%r12)
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1108
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1108:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L841:
	movq	-120(%rbp), %rcx
	movslq	%r13d, %r13
	movslq	%ebx, %rbx
	leaq	0(,%rbx,8), %rax
	leaq	(%r15,%r13,8), %rdi
	leaq	4(%rdx), %r13
	movq	$0, 16(%rdi)
	movq	%r13, 8(%r15)
	movq	8(%rcx,%rax), %rsi
	movq	16(%rcx,%rax), %rdx
	movq	-128(%rbp), %rax
	movq	(%rcx,%rbx,8), %rbx
	movl	(%rax), %eax
	cmpl	$-1, %edx
	je	.L1806
	testl	%esi, %esi
	jne	.L867
	testl	%edx, %edx
	jne	.L1190
.L1188:
	andl	$16777215, %ebx
	movq	%rbx, %rdx
	addq	$1, %rdx
	movq	%rdx, 8(%r15)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L836:
	movq	176(%r12), %rdx
	movq	192(%r12), %rax
	movslq	%r13d, %r13
	movslq	8(%rdx), %rdx
	movq	%rdx, (%rax,%r13,8)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L837:
	movq	(%r15), %rsi
	cmpq	80(%r12), %rsi
	jle	.L1807
	movq	32(%r12), %rdi
	movq	%rsi, %rdx
	subq	32(%rdi), %rdx
	js	.L972
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jl	.L1808
.L972:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L973:
	testl	%eax, %eax
	jle	.L974
	movq	48(%rdi), %rdx
	movslq	%eax, %rsi
	cmpw	$-10241, -2(%rdx,%rsi,2)
	jbe	.L1809
.L974:
	call	utext_previous32_67@PLT
.L975:
	cmpl	$10, %eax
	je	.L1810
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	js	.L1811
.L977:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rdi)
	leaq	(%r8,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L835:
	movq	192(%r12), %rax
	movslq	%r13d, %r13
	movslq	56(%r12), %r10
	movq	176(%r12), %rdi
	movq	(%rax,%r13,8), %rsi
	movq	%r10, %rdx
	movq	24(%rdi), %r9
	movslq	%esi, %rax
	subq	%r10, %rax
	salq	$3, %rax
	leaq	(%r9,%rax), %rbx
	cmpq	%rbx, %r15
	jne	.L1227
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L846:
	movq	(%r15), %rsi
	cmpq	120(%r12), %rsi
	jl	.L1090
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movb	$1, 168(%r12)
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1091
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1091:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L847:
	movq	88(%r12), %rax
	cmpq	%rax, (%r15)
	jge	.L1039
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1040
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1040:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L848:
	movq	(%r15), %rbx
	cmpq	120(%r12), %rbx
	jl	.L1034
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movb	$1, 168(%r12)
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1035
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1035:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L849:
	movq	-120(%rbp), %rax
	movslq	%r13d, %r13
	movq	(%r15), %rdx
	movq	-8(%rax,%r13,8), %rax
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %r14
	movq	-128(%rbp), %rax
	andl	$16777215, %r14d
	movl	(%rax), %eax
	cmpq	%rdx, 16(%r15,%r14,8)
	jge	.L867
	testl	%eax, %eax
	jle	.L1812
.L1132:
	movq	%r13, 8(%r15)
	movq	%rdx, 16(%r15,%r14,8)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L844:
	movq	80(%r12), %rax
	cmpq	%rax, (%r15)
	je	.L1813
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L961
	xorl	%esi, %esi
	xorl	%edx, %edx
.L961:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rdi)
	leaq	(%r8,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L845:
	movq	(%r15), %rsi
	cmpq	120(%r12), %rsi
	jl	.L986
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movb	$1, 168(%r12)
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L987
	xorl	%edi, %edi
	xorl	%edx, %edx
.L987:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L806:
	movq	-128(%rbp), %rbx
	movl	%eax, (%rbx)
	jmp	.L800
.L906:
	movq	144(%r12), %rdx
	movq	%rax, %xmm2
	movq	%r12, %rbx
	movq	-144(%rbp), %xmm0
	movb	$1, 130(%r12)
	punpcklqdq	%xmm2, %xmm0
	movq	%rdx, 152(%r12)
	movups	%xmm0, 136(%r12)
	jmp	.L1453
.L986:
	movq	32(%r12), %r9
	movq	%rsi, %rdx
	subq	32(%r9), %rdx
	js	.L988
	movslq	28(%r9), %rax
	cmpq	%rax, %rdx
	jl	.L1814
.L988:
	movq	%r9, %rdi
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %r9
	movl	40(%r9), %eax
.L989:
	cmpl	%eax, 44(%r9)
	jle	.L990
	movq	48(%r9), %rsi
	movslq	%eax, %rdx
	movzwl	(%rsi,%rdx,2), %edi
	cmpw	$-10241, %di
	jbe	.L1815
.L990:
	movq	%r9, %rdi
	call	utext_next32_67@PLT
	movl	%eax, %edi
.L991:
	call	u_charType_67@PLT
	cmpb	$9, %al
	sete	%dl
	testl	%r13d, %r13d
	setne	%al
	cmpb	%al, %dl
	je	.L992
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jle	.L1816
	movq	56(%rdi), %rax
	call	*64(%rax)
.L994:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1789:
	movq	-128(%rbp), %rax
	movslq	%r13d, %r13
	movq	%r13, 8(%r15)
	movl	(%rax), %eax
	jmp	.L867
.L1079:
	movq	32(%r12), %rdi
	movq	%rsi, %rax
	subq	32(%rdi), %rax
	js	.L1081
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jl	.L1817
.L1081:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %edx
.L1082:
	cmpl	%edx, 44(%rdi)
	jle	.L1083
	movq	48(%rdi), %rsi
	movslq	%edx, %rax
	movzwl	(%rsi,%rax,2), %eax
	cmpw	$-10241, %ax
	jbe	.L1818
.L1083:
	call	utext_next32_67@PLT
.L1084:
	movl	%eax, %edi
	andl	$-8368, %edi
	jne	.L1085
	leal	-10(%rax), %edx
	cmpl	$3, %edx
	jbe	.L1086
	cmpl	$133, %eax
	je	.L1086
	subl	$8232, %eax
	cmpl	$1, %eax
	ja	.L1085
.L1086:
	movq	176(%r12), %r8
	movslq	56(%r12), %rax
	xorl	%esi, %esi
	movl	8(%r8), %edx
	movq	24(%r8), %r9
	subl	%eax, %edx
	js	.L1087
	movslq	%edx, %rsi
	movl	%edx, %edi
.L1087:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edi, 8(%r8)
	leaq	(%r9,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1041:
	movq	32(%r12), %rdi
	andl	$8388608, %r14d
	movl	%eax, %ebx
	movq	%rsi, %rdx
	setne	%r13b
	andl	$8388607, %ebx
	subq	32(%rdi), %rdx
	js	.L1043
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jl	.L1819
.L1043:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L1044:
	cmpl	%eax, 44(%rdi)
	jle	.L1045
	movq	48(%rdi), %rsi
	movslq	%eax, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	cmpw	$-10241, %dx
	jbe	.L1820
.L1045:
	call	utext_next32_67@PLT
	movl	%eax, %esi
.L1046:
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rdi
	cmpl	$255, %esi
	jg	.L1047
	movl	%esi, %edx
	movslq	%ebx, %rax
	sarl	$3, %edx
	salq	$5, %rax
	movslq	%edx, %rdx
	addq	%rdi, %rax
	movsbl	2608(%rdx,%rax), %eax
	movl	%esi, %edx
	andl	$7, %edx
	btl	%edx, %eax
	jc	.L1744
.L1048:
	testb	%r13b, %r13b
	je	.L1049
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jle	.L1821
	movq	56(%rdi), %rax
	call	*64(%rax)
.L1051:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1796:
	movq	32(%r12), %rdi
	movq	%rsi, %rax
	subq	32(%rdi), %rax
	js	.L869
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jl	.L1822
.L869:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %edx
.L870:
	cmpl	%edx, 44(%rdi)
	jle	.L871
	movq	48(%rdi), %rsi
	movslq	%edx, %rax
	movzwl	(%rsi,%rax,2), %eax
	cmpw	$-10241, %ax
	jbe	.L1823
.L871:
	call	utext_next32_67@PLT
.L872:
	cmpl	%eax, %r13d
	jne	.L873
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L874
	addq	32(%rdi), %rax
.L875:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1066:
	movq	32(%r12), %rdi
	movq	%rsi, %rdx
	subq	32(%rdi), %rdx
	js	.L1068
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jl	.L1824
.L1068:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L1069:
	cmpl	%eax, 44(%rdi)
	jle	.L1070
	movq	48(%rdi), %rsi
	movslq	%eax, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	cmpw	$-10241, %dx
	jbe	.L1825
.L1070:
	call	utext_next32_67@PLT
	movl	%eax, %r14d
.L1071:
	cmpl	$255, %r14d
	jg	.L1072
	movq	8(%r12), %rax
	movl	%r14d, %edx
	movslq	%r13d, %r13
	sarl	$3, %edx
	salq	$5, %r13
	movslq	%edx, %rdx
	addq	112(%rax), %r13
	movsbl	0(%r13,%rdx), %eax
	movl	%r14d, %edx
	andl	$7, %edx
	btl	%edx, %eax
	jnc	.L1073
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L1074
	addq	32(%rdi), %rax
.L1075:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1788:
	movq	32(%r12), %r9
	movq	%rsi, %rdx
	subq	32(%r9), %rdx
	js	.L1274
	movslq	28(%r9), %rax
	cmpq	%rax, %rdx
	jl	.L1826
.L1274:
	movq	%r9, %rdi
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %r9
	movl	40(%r9), %eax
.L1275:
	cmpl	%eax, 44(%r9)
	jle	.L1276
	movq	48(%r9), %rsi
	movslq	%eax, %rdx
	movzwl	(%rsi,%rdx,2), %edi
	cmpw	$-10241, %di
	jbe	.L1827
.L1276:
	movq	%r9, %rdi
	call	utext_next32_67@PLT
	movl	%eax, %edi
.L1277:
	xorl	%esi, %esi
	call	u_foldCase_67@PLT
	cmpl	%eax, %r13d
	jne	.L1278
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L1279
	addq	32(%rdi), %rax
.L1280:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1053:
	movq	32(%r12), %rdi
	movq	%rsi, %rdx
	subq	32(%rdi), %rdx
	js	.L1055
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jl	.L1828
.L1055:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L1056:
	cmpl	%eax, 44(%rdi)
	jle	.L1057
	movq	48(%rdi), %rsi
	movslq	%eax, %rdx
	movzwl	(%rsi,%rdx,2), %esi
	cmpw	$-10241, %si
	jbe	.L1829
.L1057:
	call	utext_next32_67@PLT
	movl	%eax, %esi
.L1058:
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	cmpl	$255, %esi
	jg	.L1059
	movl	%esi, %edx
	movslq	%r13d, %r13
	andl	$7, %esi
	sarl	$3, %edx
	salq	$5, %r13
	addq	%r13, %rax
	movslq	%edx, %rdx
	movsbl	2608(%rdx,%rax), %eax
	btl	%esi, %eax
	jc	.L1060
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L1061
	addq	32(%rdi), %rax
.L1062:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1758:
	movq	-128(%rbp), %rax
	movl	$257, %ebx
	movw	%bx, 168(%r12)
	movl	(%rax), %eax
	jmp	.L867
.L1022:
	movq	32(%r12), %rdi
	movq	%rsi, %rax
	subq	32(%rdi), %rax
	js	.L1024
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jl	.L1830
.L1024:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %edx
.L1025:
	cmpl	%edx, 44(%rdi)
	jle	.L1026
	movq	48(%rdi), %rsi
	movslq	%edx, %rax
	movzwl	(%rsi,%rax,2), %eax
	cmpw	$-10241, %ax
	jbe	.L1831
.L1026:
	call	utext_next32_67@PLT
.L1027:
	xorl	%edx, %edx
	testl	$-8368, %eax
	jne	.L1028
	leal	-10(%rax), %edx
	cmpl	$3, %edx
	jbe	.L1492
	cmpl	$133, %eax
	je	.L1492
	subl	$8232, %eax
	cmpl	$1, %eax
	setbe	%dl
.L1028:
	testl	%r13d, %r13d
	setne	%al
	cmpb	%dl, %al
	je	.L1030
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jle	.L1832
	movq	56(%rdi), %rax
	call	*64(%rax)
.L1032:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1009:
	movq	32(%r12), %rdi
	movq	%rsi, %rax
	subq	32(%rdi), %rax
	js	.L1011
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jl	.L1833
.L1011:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %edx
.L1012:
	cmpl	%edx, 44(%rdi)
	jle	.L1013
	movq	48(%rdi), %rsi
	movslq	%edx, %rax
	movzwl	(%rsi,%rax,2), %eax
	cmpw	$-10241, %ax
	jbe	.L1834
.L1013:
	call	utext_next32_67@PLT
.L1014:
	testl	$-8368, %eax
	jne	.L1015
	leal	-10(%rax), %edx
	cmpl	$3, %edx
	jbe	.L1016
	cmpl	$133, %eax
	je	.L1016
	leal	-8232(%rax), %edx
	cmpl	$1, %edx
	ja	.L1015
.L1016:
	movq	32(%r12), %rdi
	cmpl	$13, %eax
	je	.L1835
.L1017:
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L1019
	addq	32(%rdi), %rax
.L1020:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L999:
	movq	32(%r12), %rdi
	movq	%rsi, %rdx
	subq	32(%rdi), %rdx
	js	.L1001
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jl	.L1836
.L1001:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L1002:
	cmpl	%eax, 44(%rdi)
	jle	.L1003
	movq	48(%rdi), %rsi
	movslq	%eax, %rdx
	movzwl	(%rsi,%rdx,2), %ebx
	cmpw	$-10241, %bx
	jbe	.L1837
.L1003:
	call	utext_next32_67@PLT
	movl	%eax, %ebx
.L1004:
	movl	%ebx, %edi
	call	u_charType_67@PLT
	cmpb	$12, %al
	sete	%al
	cmpl	$9, %ebx
	sete	%dl
	orl	%edx, %eax
	testl	%r13d, %r13d
	setne	%dl
	cmpb	%dl, %al
	je	.L1005
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jle	.L1838
	movq	56(%rdi), %rax
	call	*64(%rax)
.L1007:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1039:
	movq	-128(%rbp), %rax
	movl	$257, %ecx
	movw	%cx, 168(%r12)
	movl	(%rax), %eax
	jmp	.L867
.L1781:
	movq	-128(%rbp), %rax
	movl	$257, %r10d
	movw	%r10w, 168(%r12)
	movl	(%rax), %eax
	jmp	.L867
.L1769:
	movq	-128(%rbp), %rax
	movl	$257, %r9d
	movw	%r9w, 168(%r12)
	movl	(%rax), %eax
	jmp	.L867
.L1801:
	movl	$257, %eax
	movw	%ax, 168(%r12)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1107:
	movq	32(%r12), %rdi
	movq	%rsi, %rax
	subq	32(%rdi), %rax
	js	.L1109
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jl	.L1839
.L1109:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %edx
.L1110:
	cmpl	%edx, 44(%rdi)
	jle	.L1111
	movq	48(%rdi), %rsi
	movslq	%edx, %rax
	movzwl	(%rsi,%rax,2), %eax
	cmpw	$-10241, %ax
	jbe	.L1840
.L1111:
	call	utext_next32_67@PLT
.L1112:
	cmpl	$10, %eax
	je	.L1841
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jle	.L1842
	movq	56(%rdi), %rax
	call	*64(%rax)
.L1116:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1807:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1754:
	cmpl	$-1, %edi
	je	.L1206
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1804:
	cmpl	$-1, %edi
	je	.L1165
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1090:
	movq	32(%r12), %rdi
	movq	%rsi, %rdx
	subq	32(%rdi), %rdx
	js	.L1092
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jl	.L1843
.L1092:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L1093:
	cmpl	%eax, 44(%rdi)
	jle	.L1094
	movq	48(%rdi), %rsi
	movslq	%eax, %rdx
	movzwl	(%rsi,%rdx,2), %ebx
	cmpw	$-10241, %bx
	jbe	.L1844
.L1094:
	call	utext_next32_67@PLT
	movq	32(%r12), %rdi
	movl	%eax, %ebx
	movl	40(%rdi), %eax
.L1095:
	cmpl	%eax, 28(%rdi)
	jl	.L1096
	cltq
	addq	32(%rdi), %rax
.L1097:
	movq	%rax, (%r15)
	cmpl	$13, %ebx
	je	.L1098
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L996:
	cmpq	%rax, 112(%r12)
	jne	.L997
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1227:
	testl	%r10d, %r10d
	jle	.L1232
	leaq	16(%r9,%rax), %rax
	leal	-1(%r10), %r10d
	cmpq	%rax, %r15
	leaq	16(%r15), %rax
	setnb	%r9b
	cmpq	%rax, %rbx
	setnb	%al
	orb	%al, %r9b
	je	.L1229
	cmpl	$3, %r10d
	jbe	.L1229
	movl	%edx, %r9d
	xorl	%eax, %eax
	shrl	%r9d
	salq	$4, %r9
	.p2align 4,,10
	.p2align 3
.L1231:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %r9
	jne	.L1231
	movl	%edx, %eax
	andl	$-2, %eax
	andl	$1, %edx
	je	.L1232
	movq	(%r15,%rax,8), %rdx
	movq	%rdx, (%rbx,%rax,8)
.L1232:
	call	_ZN6icu_679UVector647setSizeEi@PLT
	movq	-128(%rbp), %rax
	movq	%rbx, %r15
	movl	(%rax), %eax
	jmp	.L867
.L1762:
	movb	$1, 168(%r12)
	movl	$1, %eax
	jmp	.L983
.L1780:
	movq	(%r15), %rsi
	movslq	%r9d, %rax
	subq	%rax, %rsi
	movq	%rsi, 0(%r13)
	testq	%rsi, %rsi
	jle	.L1301
	movq	%rsi, %r9
	subq	32(%rdi), %r9
	js	.L1302
	movslq	28(%rdi), %rax
	movq	%rax, %rdx
	cmpq	%rax, %r9
	jge	.L1302
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%r9,2)
	ja	.L1302
	movl	%r9d, 40(%rdi)
	movslq	%r9d, %rax
.L1303:
	cmpl	%edx, %eax
	jg	.L1312
.L1749:
	addq	32(%rdi), %rax
	movq	%rax, %rsi
.L1313:
	movq	%rsi, 0(%r13)
.L1301:
	testq	%rsi, %rsi
	js	.L1307
	movq	(%r15), %rax
	movslq	%r14d, %r14
	subq	%r14, %rax
	cmpq	%rsi, %rax
	jg	.L1307
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L1845
.L1316:
	movq	%rsi, (%r15)
	jmp	.L867
.L1777:
	movq	(%r15), %rsi
	cltq
	subq	%rax, %rsi
	movq	%rsi, 0(%r13)
	testq	%rsi, %rsi
	jle	.L1335
	movq	%rsi, %rdi
	subq	32(%r9), %rdi
	js	.L1336
	movslq	28(%r9), %rax
	movq	%rax, %rdx
	cmpq	%rax, %rdi
	jge	.L1336
	movq	48(%r9), %rax
	cmpw	$-9217, (%rax,%rdi,2)
	ja	.L1336
	movl	%edi, 40(%r9)
	movslq	%edi, %rax
.L1337:
	cmpl	%edx, %eax
	jg	.L1346
.L1750:
	addq	32(%r9), %rax
	movq	%rax, %rsi
.L1347:
	movq	%rsi, 0(%r13)
.L1335:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	testq	%rsi, %rsi
	js	.L1746
	movq	(%r15), %rdx
	movslq	%r14d, %r14
	subq	%r14, %rdx
	cmpq	%rsi, %rdx
	jg	.L1746
	testl	%eax, %eax
	jle	.L1846
.L1350:
	movq	%rsi, (%r15)
	jmp	.L867
.L1798:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L866
.L1757:
	movq	176(%r12), %rcx
	movl	56(%r12), %r14d
	movslq	8(%rcx), %rdx
	movl	%r14d, %esi
	addl	%edx, %esi
	js	.L1118
	cmpl	12(%rcx), %esi
	jle	.L1119
.L1118:
	movq	-128(%rbp), %rdx
	movq	%rcx, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-136(%rbp), %rcx
	testb	%al, %al
	je	.L1847
	movq	-128(%rbp), %rax
	movslq	8(%rcx), %rdx
	movl	(%rax), %eax
	leal	(%r14,%rdx), %esi
.L1119:
	movq	24(%rcx), %rdi
	movl	%esi, 8(%rcx)
	leaq	(%rdi,%rdx,8), %r14
.L1121:
	testl	%eax, %eax
	jg	.L1848
	movslq	56(%r12), %rdx
	movq	%r14, %r15
	leaq	16(%r14), %rsi
	movl	$16, %ecx
	salq	$3, %rdx
	subq	%rdx, %r15
	leaq	-8(%rdx), %rdi
	shrq	$3, %rdi
	cmpq	%rsi, %r15
	setnb	%sil
	subq	%rdx, %rcx
	testq	%rcx, %rcx
	setle	%dl
	orb	%dl, %sil
	je	.L1508
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L1508
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1124:
	movdqu	(%r15,%rdx), %xmm5
	movups	%xmm5, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1124
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	leaq	(%r15,%rsi), %rdx
	addq	%r14, %rsi
	cmpq	%rdi, %r8
	je	.L1126
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L1126:
	movl	272(%r12), %ecx
	leal	-1(%rcx), %edx
	testl	%edx, %edx
	jle	.L1127
	movl	%edx, 272(%r12)
.L1128:
	movq	%rbx, 8(%r15)
	movq	%r14, %r15
	jmp	.L1117
.L1797:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L907
.L1236:
	movq	40(%r12), %rdi
	movq	24(%rax), %r13
	movq	%rsi, %rax
	subq	32(%rdi), %rax
	js	.L1238
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jl	.L1849
.L1238:
	call	utext_setNativeIndex_67@PLT
.L1239:
	movq	(%r15), %rsi
	movq	32(%r12), %rdi
	movq	%rsi, %rax
	subq	32(%rdi), %rax
	js	.L1240
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jl	.L1850
.L1240:
	call	utext_setNativeIndex_67@PLT
	jmp	.L1245
	.p2align 4,,10
	.p2align 3
.L1852:
	call	utext_getNativeIndex_67@PLT
	cmpq	120(%r12), %rax
	jge	.L1851
	movq	40(%r12), %rdi
	call	utext_next32_67@PLT
	movq	32(%r12), %rdi
	movl	%eax, %ebx
	call	utext_next32_67@PLT
	cmpl	%eax, %ebx
	jne	.L1244
.L1245:
	movq	40(%r12), %rdi
	call	utext_getNativeIndex_67@PLT
	movq	32(%r12), %rdi
	cmpq	%rax, %r13
	jg	.L1852
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L1853
	addq	32(%rdi), %rax
.L1246:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1765:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1331:
	movq	192(%r12), %rdx
	movslq	%r13d, %rax
	addq	$2, %rax
	movq	(%rdx,%rax,8), %rsi
	movq	%rsi, 112(%r12)
	movq	8(%rdx,%rax,8), %rax
	movq	%rax, 120(%r12)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1778:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1800:
	movq	(%r15), %rdx
	leal	1(%r13), %eax
	cltq
	movq	%rdx, 16(%r15,%rax,8)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1806:
	movq	(%r15), %rdx
	movq	%rdx, 24(%rdi)
	testl	%esi, %esi
	jne	.L867
.L1190:
	testl	%eax, %eax
	jg	.L1188
	movq	176(%r12), %rcx
	movl	56(%r12), %r14d
	movslq	8(%rcx), %rdx
	movl	%r14d, %esi
	addl	%edx, %esi
	js	.L1191
	cmpl	12(%rcx), %esi
	jle	.L1192
.L1191:
	movq	-128(%rbp), %rdx
	movq	%rcx, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-136(%rbp), %rcx
	testb	%al, %al
	je	.L1854
	movq	-128(%rbp), %rax
	movslq	8(%rcx), %rdx
	movl	(%rax), %eax
	leal	(%r14,%rdx), %esi
.L1192:
	movq	24(%rcx), %rdi
	movl	%esi, 8(%rcx)
	leaq	(%rdi,%rdx,8), %r14
.L1194:
	testl	%eax, %eax
	jg	.L1855
	movslq	56(%r12), %rdx
	movq	%r14, %r15
	leaq	16(%r14), %rsi
	movl	$16, %ecx
	salq	$3, %rdx
	subq	%rdx, %r15
	leaq	-8(%rdx), %rdi
	shrq	$3, %rdi
	cmpq	%rsi, %r15
	setnb	%sil
	subq	%rdx, %rcx
	testq	%rcx, %rcx
	setle	%dl
	orb	%dl, %sil
	je	.L1513
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L1513
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
.L1197:
	movdqu	(%r15,%rdx), %xmm2
	movups	%xmm2, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L1197
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	leaq	(%r15,%rsi), %rdx
	addq	%r14, %rsi
	cmpq	%rdi, %r8
	je	.L1199
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L1199:
	movl	272(%r12), %ecx
	leal	-1(%rcx), %edx
	testl	%edx, %edx
	jle	.L1200
	movl	%edx, 272(%r12)
.L1201:
	movq	%r13, 8(%r15)
	movq	%r14, %r15
	jmp	.L1188
.L978:
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	js	.L1856
.L979:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1813:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1034:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1857
.L1036:
	movq	(%rdi), %rax
	movl	%ebx, %esi
	call	*120(%rax)
	movl	%eax, %r9d
	cltq
	cmpl	$-1, %r9d
	cmove	%rbx, %rax
.L1037:
	movq	120(%r12), %rdx
	movq	%rax, (%r15)
	cmpq	%rax, %rdx
	jg	.L1858
	movb	$1, 168(%r12)
	movq	-128(%rbp), %rax
	movq	%rdx, (%r15)
	movl	(%rax), %eax
	jmp	.L867
.L1783:
	movslq	56(%r12), %r10
	movslq	%esi, %rax
	movq	24(%rdi), %r9
	subq	%r10, %rax
	movq	%r10, %rdx
	salq	$3, %rax
	leaq	(%r9,%rax), %r13
	testl	%r10d, %r10d
	jle	.L1269
	leaq	16(%r9,%rax), %rax
	leal	-1(%r10), %r10d
	cmpq	%rax, %r15
	leaq	16(%r15), %rax
	setnb	%r9b
	cmpq	%rax, %r13
	setnb	%al
	orb	%al, %r9b
	je	.L1266
	cmpl	$3, %r10d
	jbe	.L1266
	movl	%edx, %r9d
	xorl	%eax, %eax
	shrl	%r9d
	salq	$4, %r9
	.p2align 4,,10
	.p2align 3
.L1268:
	movdqu	(%r15,%rax), %xmm3
	movups	%xmm3, 0(%r13,%rax)
	addq	$16, %rax
	cmpq	%rax, %r9
	jne	.L1268
	movl	%edx, %eax
	andl	$-2, %eax
	andl	$1, %edx
	je	.L1269
	movq	(%r15,%rax,8), %rdx
	movq	%rdx, 0(%r13,%rax,8)
.L1269:
	call	_ZN6icu_679UVector647setSizeEi@PLT
	movq	%r13, %r15
	movq	192(%r12), %rax
	jmp	.L1264
.L1396:
	movq	32(%r12), %rdi
	movq	%rcx, %rax
	subq	32(%rdi), %rax
	js	.L1398
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jge	.L1398
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L1398
	movl	%eax, 40(%rdi)
	jmp	.L1399
.L944:
	call	utext_current32_67@PLT
	jmp	.L945
.L956:
	call	utext_current32_67@PLT
	jmp	.L957
.L1073:
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	js	.L1859
.L1078:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1060:
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	js	.L1860
.L1065:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1047:
	movslq	%ebx, %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	8(%rdi,%rax,8), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L1048
.L1744:
	testl	%r14d, %r14d
	sete	%r13b
	jmp	.L1048
.L918:
	movq	80(%r12), %rax
	cmpq	%rax, (%r15)
	jle	.L919
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
	testl	%eax, %eax
	jle	.L920
	movq	48(%rdi), %rsi
	movslq	%eax, %rdx
	cmpw	$-10241, -2(%rsi,%rdx,2)
	ja	.L920
	subl	$1, %eax
	movl	%eax, 40(%rdi)
.L921:
	testl	%eax, %eax
	jle	.L922
	movq	48(%rdi), %rdx
	movslq	%eax, %rsi
	cmpw	$-10241, -2(%rdx,%rsi,2)
	ja	.L922
	subl	$1, %eax
	movl	%eax, 40(%rdi)
	cltq
	movzwl	(%rdx,%rax,2), %eax
.L923:
	cmpl	$13, %eax
	jne	.L919
.L916:
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L928
	xorl	%edi, %edi
	xorl	%edx, %edx
.L928:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1398:
	movq	%rcx, %rsi
	movq	%rcx, -136(%rbp)
	call	utext_setNativeIndex_67@PLT
	movq	-136(%rbp), %rcx
.L1399:
	movq	%rcx, %r13
	cmpq	%rcx, 120(%r12)
	jle	.L1400
	movq	32(%r12), %rdi
	andl	$2, %r14d
	.p2align 4,,10
	.p2align 3
.L1401:
	movl	40(%rdi), %edx
	cmpl	44(%rdi), %edx
	jge	.L1402
	movq	48(%rdi), %rcx
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %eax
	cmpw	$-10241, %ax
	ja	.L1402
	addl	$1, %edx
	movl	%edx, 40(%rdi)
.L1403:
	movl	%eax, %edx
	andl	$127, %edx
	cmpl	$41, %edx
	jg	.L1404
	cmpl	$10, %eax
	je	.L1405
	movl	%eax, %edx
	andl	$-8368, %edx
	orl	%r14d, %edx
	jne	.L1404
	leal	-10(%rax), %edx
	cmpl	$3, %edx
	jbe	.L1405
	cmpl	$133, %eax
	je	.L1405
	subl	$8232, %eax
	cmpl	$1, %eax
	jbe	.L1405
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L1408
	addq	32(%rdi), %rax
	movq	%rax, %r13
	cmpq	120(%r12), %rax
	jl	.L1401
.L1400:
	movb	$1, 168(%r12)
	movq	8(%r15), %rbx
	movq	(%r15), %rcx
	jmp	.L1397
	.p2align 4,,10
	.p2align 3
.L1405:
	movq	8(%r15), %rbx
	movq	(%r15), %rcx
	jmp	.L1397
	.p2align 4,,10
	.p2align 3
.L1373:
	call	utext_next32_67@PLT
	movl	%eax, %esi
	cmpl	$255, %esi
	jle	.L1861
.L1375:
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L1372
	movq	32(%r12), %rdi
	movslq	40(%rdi), %r14
	cmpl	28(%rdi), %r14d
	jle	.L1376
.L1774:
	movq	56(%rdi), %rax
	call	*64(%rax)
	movq	%rax, %r14
	cmpq	%rax, 120(%r12)
	jg	.L1747
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L884:
	call	utext_next32_67@PLT
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L1402:
	call	utext_next32_67@PLT
	jmp	.L1403
	.p2align 4,,10
	.p2align 3
.L1286:
	movzwl	2(%rdi,%rsi), %eax
	movl	%eax, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L1521
	movl	%r15d, %edx
	addl	$2, %r13d
	sall	$10, %edx
	leal	-56613888(%rax,%rdx), %r15d
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1408:
	movq	56(%rdi), %rax
	call	*64(%rax)
	movq	%rax, %r13
	cmpq	%rax, 120(%r12)
	jle	.L1400
	movq	32(%r12), %rdi
	jmp	.L1401
	.p2align 4,,10
	.p2align 3
.L1794:
	movzwl	2(%r13,%rdi), %esi
	movl	%esi, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L886
	sall	$10, %edx
	leal	2(%r14), %ecx
	leal	-56613888(%rsi,%rdx), %edx
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L1467:
	movq	$0, -152(%rbp)
	jmp	.L801
.L1793:
	movb	$1, 168(%r12)
.L883:
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L889
	xorl	%esi, %esi
	xorl	%edx, %edx
.L889:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rdi)
	leaq	(%r8,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L967:
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L970
	xorl	%esi, %esi
	xorl	%edx, %edx
.L970:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rdi)
	leaq	(%r8,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1468:
	xorl	%r15d, %r15d
	jmp	.L805
.L1433:
	call	utext_previous32_67@PLT
	jmp	.L1434
.L1429:
	call	utext_previous32_67@PLT
	movq	32(%r12), %rdi
	movl	%eax, %r13d
	movl	40(%rdi), %eax
	jmp	.L1430
.L965:
	call	utext_previous32_67@PLT
	jmp	.L966
.L1284:
	movq	%r14, %rdi
	movq	-176(%rbp), %r15
	call	_ZN6icu_6724CaseFoldingUTextIterator11inExpansionEv@PLT
	testb	%al, %al
	jne	.L1292
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jle	.L1862
	movq	56(%rdi), %rax
	call	*64(%rax)
.L1296:
	movq	%rax, (%r15)
	jmp	.L1297
.L1785:
	movb	$1, 168(%r12)
	movq	%r14, %rdi
	call	_ZN6icu_6724CaseFoldingUTextIterator11inExpansionEv@PLT
.L1292:
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1298
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1298:
	movl	%edx, 8(%rsi)
	subq	%rax, %rdi
	leaq	(%r8,%rdi,8), %r15
.L1297:
	movq	%r14, %rdi
	call	_ZN6icu_6724CaseFoldingUTextIteratorD1Ev@PLT
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1746:
	movq	192(%r12), %rdx
	jmp	.L1341
.L1786:
	movq	%r14, %rdi
	call	_ZN6icu_6724CaseFoldingUTextIterator11inExpansionEv@PLT
	jmp	.L1292
.L1753:
	xorl	%edx, %edx
	jmp	.L1464
.L946:
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	js	.L1863
.L952:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L981:
	testl	%r13d, %r13d
	jne	.L853
.L984:
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	js	.L1864
.L985:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1763:
	addq	$1, %rbx
	movq	%rbx, 8(%r15)
	jmp	.L867
.L1775:
	leaq	1(%rbx), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L867
.L1457:
	leal	4(%r13), %eax
	cltq
	movq	%rax, 8(%r15)
	movl	272(%r12), %eax
	subl	$1, %eax
	movl	%eax, 272(%r12)
	testl	%eax, %eax
	jle	.L1208
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1166:
	movl	272(%r12), %eax
	subl	$1, %eax
	movl	%eax, 272(%r12)
	testl	%eax, %eax
	jle	.L1183
.L1745:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L1168
.L1015:
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	js	.L1865
.L1021:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L879:
	cmpl	%edx, %eax
	jle	.L1454
	movq	56(%rdi), %rax
	call	*64(%rax)
.L888:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1085:
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L1088
	addq	32(%rdi), %rax
.L1089:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1217:
	movq	-128(%rbp), %rax
	movq	%r12, %rbx
	movl	$66321, (%rax)
	jmp	.L1748
.L1162:
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	xorl	%esi, %esi
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	js	.L1163
	movslq	%edx, %rsi
	movl	%edx, %ecx
.L1163:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%ecx, 8(%rdi)
	leaq	(%r8,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1431:
	movq	56(%rdi), %rax
	call	*64(%rax)
	movq	32(%r12), %rdi
	movq	%rax, %rdx
	movl	40(%rdi), %eax
	jmp	.L1432
.L1306:
	movq	%rsi, %rdx
	subq	32(%rdi), %rdx
	js	.L1308
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jl	.L1866
.L1308:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L1309:
	testl	%eax, %eax
	jle	.L1310
	movq	48(%rdi), %rsi
	movslq	%eax, %rdx
	cmpw	$-10241, -2(%rsi,%rdx,2)
	jbe	.L1867
.L1310:
	call	utext_previous32_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L1311:
	cmpl	%eax, 28(%rdi)
	jl	.L1312
	cltq
	jmp	.L1749
.L1340:
	movq	%rsi, %rdx
	subq	32(%r9), %rdx
	js	.L1342
	movslq	28(%r9), %rax
	cmpq	%rax, %rdx
	jl	.L1868
.L1342:
	movq	%r9, %rdi
	movq	%rcx, -136(%rbp)
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %r9
	movq	-136(%rbp), %rcx
	movl	40(%r9), %eax
.L1343:
	testl	%eax, %eax
	jle	.L1344
	movq	48(%r9), %rsi
	movslq	%eax, %rdx
	cmpw	$-10241, -2(%rsi,%rdx,2)
	jbe	.L1869
.L1344:
	movq	%r9, %rdi
	movq	%rcx, -136(%rbp)
	call	utext_previous32_67@PLT
	movq	32(%r12), %r9
	movq	-136(%rbp), %rcx
	movl	40(%r9), %eax
.L1345:
	cmpl	%eax, 28(%r9)
	jl	.L1346
	cltq
	jmp	.L1750
.L1764:
	movq	176(%r12), %r14
	movl	56(%r12), %r13d
	movslq	8(%r14), %rdx
	movl	%r13d, %esi
	addl	%edx, %esi
	js	.L1412
	cmpl	12(%r14), %esi
	jle	.L1413
.L1412:
	movq	-128(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L1870
	movq	-128(%rbp), %rax
	movslq	8(%r14), %rdx
	movl	(%rax), %eax
	leal	0(%r13,%rdx), %esi
.L1413:
	movq	24(%r14), %rdi
	movl	%esi, 8(%r14)
	leaq	(%rdi,%rdx,8), %r13
.L1415:
	testl	%eax, %eax
	jg	.L1871
	movslq	56(%r12), %rdx
	movq	%r13, %r14
	leaq	16(%r13), %rsi
	movl	$16, %ecx
	salq	$3, %rdx
	subq	%rdx, %r14
	leaq	-8(%rdx), %rdi
	shrq	$3, %rdi
	cmpq	%rsi, %r14
	setnb	%sil
	subq	%rdx, %rcx
	testq	%rcx, %rcx
	setle	%dl
	orb	%dl, %sil
	je	.L1531
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L1531
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1418:
	movdqu	(%r14,%rdx), %xmm7
	movups	%xmm7, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L1418
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	leaq	(%r14,%rsi), %rdx
	addq	%r13, %rsi
	cmpq	%rdi, %r8
	je	.L1420
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L1420:
	movl	272(%r12), %ecx
	leal	-1(%rcx), %edx
	testl	%edx, %edx
	jle	.L1421
	movl	%edx, 272(%r12)
.L1422:
	movq	%rbx, 8(%r14)
	movq	%r13, %r15
	movq	8(%r13), %rbx
	jmp	.L1411
.L1776:
	movq	176(%r12), %r14
	movl	56(%r12), %r13d
	movslq	8(%r14), %rdx
	movl	%r13d, %esi
	addl	%edx, %esi
	js	.L1382
	cmpl	12(%r14), %esi
	jle	.L1383
.L1382:
	movq	-128(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L1872
	movq	-128(%rbp), %rax
	movslq	8(%r14), %rdx
	movl	(%rax), %eax
	leal	0(%r13,%rdx), %esi
.L1383:
	movq	24(%r14), %rdi
	movl	%esi, 8(%r14)
	leaq	(%rdi,%rdx,8), %r13
.L1385:
	testl	%eax, %eax
	jg	.L1873
	movslq	56(%r12), %rdx
	movq	%r13, %r14
	leaq	16(%r13), %rsi
	movl	$16, %ecx
	salq	$3, %rdx
	subq	%rdx, %r14
	leaq	-8(%rdx), %rdi
	shrq	$3, %rdi
	cmpq	%rsi, %r14
	setnb	%sil
	subq	%rdx, %rcx
	testq	%rcx, %rcx
	setle	%dl
	orb	%dl, %sil
	je	.L1529
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L1529
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1388:
	movdqu	(%r14,%rdx), %xmm7
	movups	%xmm7, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L1388
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	leaq	(%r14,%rsi), %rdx
	addq	%r13, %rsi
	cmpq	%rdi, %r8
	je	.L1390
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L1390:
	movl	272(%r12), %ecx
	leal	-1(%rcx), %edx
	testl	%edx, %edx
	jle	.L1391
	movl	%edx, 272(%r12)
.L1392:
	movq	%rbx, 8(%r14)
	movq	%r13, %r15
	movq	8(%r13), %rbx
	jmp	.L1381
.L915:
	movq	32(%r12), %rdi
	movl	40(%rdi), %edx
	cmpl	44(%rdi), %edx
	jge	.L924
	movq	48(%rdi), %rsi
	movslq	%edx, %rax
	movzwl	(%rsi,%rax,2), %eax
	cmpw	$-10241, %ax
	ja	.L924
	addl	$1, %edx
	movl	%edx, 40(%rdi)
.L925:
	cmpl	$13, %ebx
	jne	.L916
	cmpl	$10, %eax
	jne	.L916
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L926
	addq	32(%rdi), %rax
.L927:
	cmpq	%rax, 88(%r12)
	jg	.L916
	movq	-128(%rbp), %rax
	movl	$257, %r13d
	movw	%r13w, 168(%r12)
	movl	(%rax), %eax
	jmp	.L867
.L913:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L914
.L1049:
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	js	.L1874
.L1052:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1096:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L1097
.L1851:
	movb	$1, 168(%r12)
.L1244:
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	js	.L1875
.L1247:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1767:
	cmpq	(%r15), %rbx
	jge	.L1435
	cmpl	$13, %eax
	jne	.L1435
	movq	-120(%rbp), %rax
	cmpb	$52, -13(%rax,%r14,8)
	jne	.L1435
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L1436
	addq	32(%rdi), %rax
.L1437:
	movq	%rax, (%r15)
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	40(%r12), %rdi
	movq	24(%rax), %r13
	leaq	-112(%rbp), %rbx
	leaq	-80(%rbp), %r14
	call	utext_setNativeIndex_67@PLT
	movq	(%r15), %rsi
	movq	32(%r12), %rdi
	call	utext_setNativeIndex_67@PLT
	movq	40(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6724CaseFoldingUTextIteratorC1ER5UText@PLT
	movq	32(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6724CaseFoldingUTextIteratorC1ER5UText@PLT
	movq	%r15, -136(%rbp)
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L1256:
	movq	%rbx, %rdi
	call	_ZN6icu_6724CaseFoldingUTextIterator11inExpansionEv@PLT
	testb	%al, %al
	jne	.L1254
	movq	40(%r12), %rdi
	call	utext_getNativeIndex_67@PLT
	cmpq	%rax, %r15
	jle	.L1876
.L1254:
	movq	%r14, %rdi
	call	_ZN6icu_6724CaseFoldingUTextIterator11inExpansionEv@PLT
	testb	%al, %al
	jne	.L1252
	movq	32(%r12), %rdi
	call	utext_getNativeIndex_67@PLT
	cmpq	120(%r12), %rax
	jge	.L1877
.L1252:
	movq	%rbx, %rdi
	call	_ZN6icu_6724CaseFoldingUTextIterator4nextEv@PLT
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	_ZN6icu_6724CaseFoldingUTextIterator4nextEv@PLT
	cmpl	%eax, %r13d
	je	.L1256
.L1257:
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L1261
	xorl	%esi, %esi
	xorl	%edx, %edx
.L1261:
	movl	%edx, 8(%rdi)
	subq	%rax, %rsi
	leaq	(%r8,%rsi,8), %r15
.L1260:
	movq	%r14, %rdi
	call	_ZN6icu_6724CaseFoldingUTextIteratorD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6724CaseFoldingUTextIteratorD1Ev@PLT
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1799:
	movq	-8(%rdi,%rax), %rbx
	movq	176(%r12), %rdi
	movl	56(%r12), %r9d
	movslq	8(%rdi), %rax
	movl	%r9d, %r10d
	addl	%eax, %r10d
	js	.L1148
	cmpl	12(%rdi), %r10d
	jle	.L1149
.L1148:
	movq	-128(%rbp), %rdx
	movl	%r10d, %esi
	movl	%r9d, -168(%rbp)
	movl	%ecx, -176(%rbp)
	movq	%rdi, -136(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-136(%rbp), %rdi
	movl	-176(%rbp), %ecx
	testb	%al, %al
	movl	-168(%rbp), %r9d
	je	.L1878
	movq	-128(%rbp), %rdx
	movslq	8(%rdi), %rax
	movl	(%rdx), %esi
	leal	(%r9,%rax), %r10d
.L1149:
	movq	24(%rdi), %rdx
	movl	%r10d, 8(%rdi)
	leaq	(%rdx,%rax,8), %rdx
.L1151:
	testl	%esi, %esi
	jg	.L1879
	movslq	56(%r12), %rax
	movq	%rdx, %r15
	leaq	16(%rdx), %rsi
	movl	$16, %r11d
	salq	$3, %rax
	subq	%rax, %r15
	leaq	-8(%rax), %rdi
	shrq	$3, %rdi
	cmpq	%rsi, %r15
	setnb	%sil
	subq	%rax, %r11
	testq	%r11, %r11
	setle	%al
	orb	%al, %sil
	je	.L1510
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L1510
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
.L1154:
	movdqu	(%r15,%rax), %xmm5
	movups	%xmm5, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L1154
	movq	%rdi, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rsi
	leaq	(%r15,%rsi), %rax
	addq	%rdx, %rsi
	cmpq	%rdi, %r9
	je	.L1156
	movq	(%rax), %rax
	movq	%rax, (%rsi)
.L1156:
	movl	272(%r12), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L1157
	movl	%eax, 272(%r12)
.L1158:
	andl	$16777215, %ebx
	leaq	1(%rbx), %rax
	movq	%rax, 8(%r15)
	movq	%rdx, %r15
	jmp	.L1147
.L1365:
	movq	192(%r12), %rdx
	movslq	%r13d, %rax
	movq	%r9, %rdi
	addq	$2, %rax
	leaq	0(,%rax,8), %rsi
	movq	(%rdx,%rax,8), %rax
	movq	%rax, 112(%r12)
	movq	8(%rdx,%rsi), %rax
	movq	%rax, 120(%r12)
	movq	-16(%rdx,%rsi), %rsi
	call	_ZN6icu_679UVector647setSizeEi@PLT
	movq	176(%r12), %rdi
	movslq	56(%r12), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L1367
	xorl	%esi, %esi
	xorl	%edx, %edx
.L1367:
	subq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rdi)
	leaq	(%r8,%rsi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1809:
	subl	$1, %eax
	movl	%eax, 40(%rdi)
	cltq
	movzwl	(%rdx,%rax,2), %eax
	jmp	.L975
.L1830:
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L1024
	movl	%eax, 40(%rdi)
	movl	%eax, %edx
	jmp	.L1025
.L1828:
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L1055
	movl	%edx, 40(%rdi)
	movl	%edx, %eax
	jmp	.L1056
.L1808:
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L972
	movl	%edx, 40(%rdi)
	movl	%edx, %eax
	jmp	.L973
.L1826:
	movq	48(%r9), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L1274
	movl	%edx, 40(%r9)
	movl	%edx, %eax
	jmp	.L1275
.L1824:
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L1068
	movl	%edx, 40(%rdi)
	movl	%edx, %eax
	jmp	.L1069
.L1822:
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L869
	movl	%eax, 40(%rdi)
	movl	%eax, %edx
	jmp	.L870
.L1819:
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L1043
	movl	%edx, 40(%rdi)
	movl	%edx, %eax
	jmp	.L1044
.L1817:
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L1081
	movl	%eax, 40(%rdi)
	movl	%eax, %edx
	jmp	.L1082
.L1814:
	movq	48(%r9), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L988
	movl	%edx, 40(%r9)
	movl	%edx, %eax
	jmp	.L989
.L1773:
	movl	%eax, 40(%rdi)
	jmp	.L1369
.L1792:
	movl	%r9d, 40(%rdi)
	movslq	%r9d, %rax
	jmp	.L878
.L1784:
	movl	%eax, 40(%rdi)
	jmp	.L1283
.L1770:
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L954
	movl	%edx, 40(%rdi)
	movslq	%edx, %rax
	jmp	.L955
.L1836:
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L1001
	movl	%edx, 40(%rdi)
	movl	%edx, %eax
	jmp	.L1002
.L1802:
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L909
	movl	%edx, 40(%rdi)
	movl	%edx, %eax
	jmp	.L910
.L1833:
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L1011
	movl	%eax, 40(%rdi)
	movl	%eax, %edx
	jmp	.L1012
.L1759:
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L930
	movl	%eax, 40(%rdi)
	movl	%eax, %edx
	jmp	.L931
.L1782:
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L942
	movl	%edx, 40(%rdi)
	movslq	%edx, %rax
	jmp	.L943
.L1843:
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L1092
	movl	%edx, 40(%rdi)
	movl	%edx, %eax
	jmp	.L1093
.L1839:
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L1109
	movl	%eax, 40(%rdi)
	movl	%eax, %edx
	jmp	.L1110
.L1761:
	call	_ZN6icu_676Locale10getEnglishEv@PLT
	movq	-128(%rbp), %r14
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	(%r14), %r8d
	movq	%rax, 320(%r12)
	movq	%rax, %rdi
	testl	%r8d, %r8d
	jg	.L981
	movq	(%rax), %rax
	movq	32(%r12), %rsi
	movq	%r14, %rdx
	call	*64(%rax)
	jmp	.L980
.L1818:
	addl	$1, %edx
	movl	%edx, 40(%rdi)
	jmp	.L1084
.L1815:
	addl	$1, %eax
	movl	%eax, 40(%r9)
	jmp	.L991
.L1831:
	addl	$1, %edx
	movl	%edx, 40(%rdi)
	jmp	.L1027
.L1825:
	addl	$1, %eax
	movzwl	%dx, %r14d
	movl	%eax, 40(%rdi)
	jmp	.L1071
.L1823:
	addl	$1, %edx
	movl	%edx, 40(%rdi)
	jmp	.L872
.L1820:
	addl	$1, %eax
	movzwl	%dx, %esi
	movl	%eax, 40(%rdi)
	jmp	.L1046
.L1827:
	addl	$1, %eax
	movl	%eax, 40(%r9)
	jmp	.L1277
.L1760:
	addl	$1, %edx
	movl	%edx, 40(%rdi)
	jmp	.L933
.L1803:
	addl	$1, %eax
	movl	%eax, 40(%rdi)
	jmp	.L912
.L1454:
	addq	32(%rdi), %rax
	jmp	.L888
.L1832:
	addq	32(%rdi), %rax
	jmp	.L1032
.L1088:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L1089
.L1816:
	addq	32(%rdi), %rax
	jmp	.L994
.L1838:
	addq	32(%rdi), %rax
	jmp	.L1007
.L1840:
	addl	$1, %edx
	movl	%edx, 40(%rdi)
	jmp	.L1112
.L1829:
	addl	$1, %eax
	movl	%eax, 40(%rdi)
	jmp	.L1058
.L1834:
	addl	$1, %edx
	movl	%edx, 40(%rdi)
	jmp	.L1014
.L1837:
	addl	$1, %eax
	movl	%eax, 40(%rdi)
	jmp	.L1004
.L1844:
	addl	$1, %eax
	movl	%eax, 40(%rdi)
	jmp	.L1095
.L1858:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1842:
	addq	32(%rdi), %rax
	jmp	.L1116
.L1865:
	xorl	%edi, %edi
	xorl	%edx, %edx
	jmp	.L1021
.L1864:
	xorl	%edi, %edi
	xorl	%edx, %edx
	jmp	.L985
.L1863:
	xorl	%edi, %edi
	xorl	%edx, %edx
	jmp	.L952
.L1856:
	xorl	%edi, %edi
	xorl	%edx, %edx
	jmp	.L979
.L1772:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L959
.L1811:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L977
.L1849:
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L1238
	movl	%eax, 40(%rdi)
	jmp	.L1239
.L1850:
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L1240
	movl	%eax, 40(%rdi)
	jmp	.L1245
.L1766:
	movl	%edx, 40(%rdi)
	movl	%edx, %eax
	jmp	.L1428
.L1779:
	movl	%edx, 40(%rdi)
	movl	%edx, %eax
	jmp	.L964
.L1821:
	addq	32(%rdi), %rax
	jmp	.L1051
.L1862:
	addq	32(%rdi), %rax
	jmp	.L1296
.L1805:
	movq	176(%r12), %rcx
	movl	56(%r12), %r14d
	movslq	8(%rcx), %rdx
	movl	%r14d, %esi
	addl	%edx, %esi
	js	.L1169
	cmpl	12(%rcx), %esi
	jle	.L1170
.L1169:
	movq	-128(%rbp), %rdx
	movq	%rcx, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-136(%rbp), %rcx
	testb	%al, %al
	je	.L1880
	movq	-128(%rbp), %rax
	movslq	8(%rcx), %rdx
	movl	(%rax), %eax
	leal	(%r14,%rdx), %esi
.L1170:
	movq	24(%rcx), %rdi
	movl	%esi, 8(%rcx)
	leaq	(%rdi,%rdx,8), %r14
.L1172:
	testl	%eax, %eax
	jg	.L1881
	movslq	56(%r12), %rdx
	movq	%r14, %r15
	leaq	16(%r14), %rsi
	movl	$16, %ecx
	salq	$3, %rdx
	subq	%rdx, %r15
	leaq	-8(%rdx), %rdi
	shrq	$3, %rdi
	cmpq	%rsi, %r15
	setnb	%sil
	subq	%rdx, %rcx
	testq	%rcx, %rcx
	setle	%dl
	orb	%dl, %sil
	je	.L1512
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L1512
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
.L1175:
	movdqu	(%r15,%rdx), %xmm3
	movups	%xmm3, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L1175
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	leaq	(%r15,%rsi), %rdx
	addq	%r14, %rsi
	cmpq	%rdi, %r8
	je	.L1177
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L1177:
	movl	272(%r12), %ecx
	leal	-1(%rcx), %edx
	testl	%edx, %edx
	jle	.L1178
	movl	%edx, 272(%r12)
.L1179:
	movq	%rbx, 8(%r15)
	movq	%r14, %r15
	jmp	.L1168
.L1846:
	movq	176(%r12), %rcx
	movl	56(%r12), %r14d
	movq	8(%r15), %rbx
	movslq	8(%rcx), %rdx
	movl	%r14d, %esi
	addl	%edx, %esi
	js	.L1351
	cmpl	12(%rcx), %esi
	jle	.L1352
.L1351:
	movq	-128(%rbp), %rdx
	movq	%rcx, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-136(%rbp), %rcx
	testb	%al, %al
	je	.L1882
	movq	-128(%rbp), %rax
	movslq	8(%rcx), %rdx
	movl	(%rax), %eax
	leal	(%r14,%rdx), %esi
.L1352:
	movq	24(%rcx), %rdi
	movl	%esi, 8(%rcx)
	leaq	(%rdi,%rdx,8), %r14
.L1354:
	testl	%eax, %eax
	jg	.L1883
	movslq	56(%r12), %rdx
	movq	%r14, %r15
	leaq	16(%r14), %rsi
	movl	$16, %ecx
	salq	$3, %rdx
	subq	%rdx, %r15
	leaq	-8(%rdx), %rdi
	shrq	$3, %rdi
	cmpq	%rsi, %r15
	setnb	%sil
	subq	%rdx, %rcx
	testq	%rcx, %rcx
	setle	%dl
	orb	%dl, %sil
	je	.L1526
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L1526
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
.L1357:
	movdqu	(%r15,%rdx), %xmm7
	movups	%xmm7, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1357
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	leaq	(%r15,%rsi), %rdx
	addq	%r14, %rsi
	cmpq	%rdi, %r8
	je	.L1359
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L1359:
	movl	272(%r12), %ecx
	leal	-1(%rcx), %edx
	testl	%edx, %edx
	jle	.L1360
	movl	%edx, 272(%r12)
.L1361:
	subq	$4, %rbx
	movq	%rbx, 8(%r15)
	movq	%r14, %r15
	movq	0(%r13), %rsi
	jmp	.L1350
.L1845:
	movq	176(%r12), %rcx
	movl	56(%r12), %r14d
	movq	8(%r15), %rbx
	movslq	8(%rcx), %rdx
	movl	%r14d, %esi
	addl	%edx, %esi
	js	.L1317
	cmpl	12(%rcx), %esi
	jle	.L1318
.L1317:
	movq	-128(%rbp), %rdx
	movq	%rcx, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-136(%rbp), %rcx
	testb	%al, %al
	je	.L1884
	movq	-128(%rbp), %rax
	movslq	8(%rcx), %rdx
	movl	(%rax), %eax
	leal	(%r14,%rdx), %esi
.L1318:
	movq	24(%rcx), %rdi
	movl	%esi, 8(%rcx)
	leaq	(%rdi,%rdx,8), %r14
.L1320:
	testl	%eax, %eax
	jg	.L1885
	movslq	56(%r12), %rdx
	movq	%r14, %r15
	leaq	16(%r14), %rsi
	movl	$16, %ecx
	salq	$3, %rdx
	subq	%rdx, %r15
	leaq	-8(%rdx), %rdi
	shrq	$3, %rdi
	cmpq	%rsi, %r15
	setnb	%sil
	subq	%rdx, %rcx
	testq	%rcx, %rcx
	setle	%dl
	orb	%dl, %sil
	je	.L1524
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L1524
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
.L1323:
	movdqu	(%r15,%rdx), %xmm3
	movups	%xmm3, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L1323
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	leaq	(%r15,%rsi), %rdx
	addq	%r14, %rsi
	cmpq	%rdi, %r8
	je	.L1325
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L1325:
	movl	272(%r12), %ecx
	leal	-1(%rcx), %edx
	testl	%edx, %edx
	jle	.L1326
	movl	%edx, 272(%r12)
.L1327:
	subq	$3, %rbx
	movq	%rbx, 8(%r15)
	movq	%r14, %r15
	movq	0(%r13), %rsi
	jmp	.L1316
.L1755:
	movq	176(%r12), %r14
	movl	56(%r12), %ebx
	movslq	8(%r14), %rdx
	movl	%ebx, %esi
	addl	%edx, %esi
	js	.L1213
	cmpl	12(%r14), %esi
	jle	.L1214
.L1213:
	movq	-128(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L1886
	movq	-128(%rbp), %rax
	movslq	8(%r14), %rdx
	movl	(%rax), %eax
	leal	(%rbx,%rdx), %esi
.L1214:
	movq	24(%r14), %rdi
	movl	%esi, 8(%r14)
	leaq	(%rdi,%rdx,8), %rbx
.L1216:
	testl	%eax, %eax
	jg	.L1217
	movslq	56(%r12), %rdx
	movq	%rbx, %r14
	leaq	16(%rbx), %rsi
	movl	$16, %ecx
	salq	$3, %rdx
	subq	%rdx, %r14
	leaq	-8(%rdx), %rdi
	shrq	$3, %rdi
	cmpq	%rsi, %r14
	setnb	%sil
	subq	%rdx, %rcx
	testq	%rcx, %rcx
	setle	%dl
	orb	%dl, %sil
	je	.L1514
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L1514
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
.L1219:
	movdqu	(%r14,%rdx), %xmm2
	movups	%xmm2, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L1219
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	leaq	(%r14,%rsi), %rdx
	addq	%rbx, %rsi
	cmpq	%rdi, %r8
	je	.L1221
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L1221:
	movl	272(%r12), %ecx
	leal	-1(%rcx), %edx
	testl	%edx, %edx
	jle	.L1222
	movl	%edx, 272(%r12)
.L1223:
	leal	4(%r13), %edx
	movq	%rbx, %r15
	movslq	%edx, %rdx
	movq	%rdx, 8(%r14)
	jmp	.L867
.L1866:
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L1308
	movl	%edx, 40(%rdi)
	movl	%edx, %eax
	jmp	.L1309
.L1868:
	movq	48(%r9), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L1342
	movl	%edx, 40(%r9)
	movl	%edx, %eax
	jmp	.L1343
.L1867:
	subl	$1, %eax
	movl	%eax, 40(%rdi)
	jmp	.L1311
.L1869:
	subl	$1, %eax
	movl	%eax, 40(%r9)
	jmp	.L1345
.L1874:
	xorl	%edi, %edi
	xorl	%edx, %edx
	jmp	.L1052
.L1860:
	xorl	%edi, %edi
	xorl	%edx, %edx
	jmp	.L1065
.L1859:
	xorl	%edi, %edi
	xorl	%edx, %edx
	jmp	.L1078
.L1875:
	xorl	%edi, %edi
	xorl	%edx, %edx
	jmp	.L1247
.L924:
	call	utext_next32_67@PLT
	jmp	.L925
.L1492:
	movl	$1, %edx
	jmp	.L1028
.L1336:
	movq	%r9, %rdi
	movq	%rcx, -136(%rbp)
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %r9
	movq	-136(%rbp), %rcx
	movslq	40(%r9), %rax
	movl	28(%r9), %edx
	jmp	.L1337
.L1302:
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	movl	28(%rdi), %edx
	jmp	.L1303
.L1534:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1266:
	movl	%r10d, %edx
	xorl	%eax, %eax
.L1272:
	movq	(%r15,%rax,8), %rcx
	movq	%rcx, 0(%r13,%rax,8)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rcx, %rdx
	jne	.L1272
	jmp	.L1269
.L1472:
	movq	%rbx, %rdi
	movq	%r14, %rsi
.L897:
	movsq
	cmpq	%rsi, %rbx
	jne	.L897
	jmp	.L900
.L1508:
	movq	%r14, %rdi
	movq	%r15, %rsi
.L1123:
	movsq
	cmpq	%rsi, %r14
	jne	.L1123
	jmp	.L1126
.L1312:
	movq	56(%rdi), %rax
	call	*64(%rax)
	movq	%rax, %rsi
	jmp	.L1313
.L1346:
	movq	56(%r9), %rax
	movq	%rcx, -136(%rbp)
	movq	%r9, %rdi
	call	*64(%rax)
	movq	-136(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L1347
.L1812:
	movq	176(%r12), %rdi
	movl	56(%r12), %ecx
	movslq	8(%rdi), %rdx
	movl	%ecx, %esi
	addl	%edx, %esi
	js	.L1133
	cmpl	12(%rdi), %esi
	jle	.L1134
.L1133:
	movq	-128(%rbp), %rdx
	movl	%ecx, -176(%rbp)
	movq	%rdi, -136(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-136(%rbp), %rdi
	movl	-176(%rbp), %ecx
	testb	%al, %al
	je	.L1887
	movq	-128(%rbp), %rax
	movslq	8(%rdi), %rdx
	movl	(%rax), %eax
	leal	(%rcx,%rdx), %esi
.L1134:
	movq	24(%rdi), %r9
	movl	%esi, 8(%rdi)
	leaq	(%r9,%rdx,8), %rcx
.L1136:
	testl	%eax, %eax
	jg	.L1888
	movslq	56(%r12), %rdx
	movq	%rcx, %r15
	leaq	16(%rcx), %rsi
	movl	$16, %r11d
	salq	$3, %rdx
	subq	%rdx, %r15
	leaq	-8(%rdx), %rdi
	shrq	$3, %rdi
	cmpq	%rsi, %r15
	setnb	%sil
	subq	%rdx, %r11
	testq	%r11, %r11
	setle	%dl
	orb	%dl, %sil
	je	.L1509
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rdi
	je	.L1509
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
.L1139:
	movdqu	(%r15,%rdx), %xmm4
	movups	%xmm4, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L1139
	movq	%rdi, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rsi
	leaq	(%r15,%rsi), %rdx
	addq	%rcx, %rsi
	cmpq	%rdi, %r9
	je	.L1141
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L1141:
	movl	272(%r12), %edi
	leal	-1(%rdi), %edx
	testl	%edx, %edx
	jle	.L1142
	movl	%edx, 272(%r12)
.L1143:
	movq	%rbx, 8(%r15)
	movq	(%rcx), %rdx
	movq	%rcx, %r15
	jmp	.L1132
.L1790:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1841:
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1114
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1114:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1059:
	movslq	%r13d, %r13
	leaq	0(%r13,%r13,4), %rdx
	leaq	(%rdx,%rdx,4), %rdx
	leaq	8(%rax,%rdx,8), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L1060
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L1063
	addq	32(%rdi), %rax
.L1064:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1165:
	movslq	%esi, %rsi
	cmpq	%rsi, %rax
	jl	.L1166
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
.L1456:
	movq	(%r15), %rsi
	cmpq	24(%rdx), %rsi
	je	.L867
	movq	%rsi, 24(%rdx)
	jmp	.L1167
.L1771:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L934:
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L937
	addq	32(%rdi), %rax
.L938:
	cmpq	%rax, 88(%r12)
	jne	.L940
	movq	-128(%rbp), %rax
	movl	$257, %r11d
	movw	%r11w, 168(%r12)
	movl	(%rax), %eax
	jmp	.L867
.L992:
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L995
	xorl	%edi, %edi
	xorl	%edx, %edx
.L995:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1848:
	movq	-128(%rbp), %rax
	movl	$66321, (%rax)
	movl	$66321, %eax
	jmp	.L1117
.L1810:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1072:
	movq	-160(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L1073
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L1076
	addq	32(%rdi), %rax
.L1077:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1005:
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1008
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1008:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1030:
	movq	176(%r12), %rsi
	movslq	56(%r12), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %r8
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L1033
	xorl	%edi, %edi
	xorl	%edx, %edx
.L1033:
	subq	%rax, %rdi
	movq	-128(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%r8,%rdi,8), %r15
	movl	(%rax), %eax
	jmp	.L867
.L1206:
	movslq	%esi, %rsi
	cmpq	%rsi, %rax
	jl	.L1457
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
.L1458:
	movq	(%r15), %rsi
	cmpq	24(%rdx), %rsi
	je	.L867
	movq	%rsi, 24(%rdx)
	jmp	.L1212
.L1098:
	cmpq	120(%r12), %rax
	jl	.L1099
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1873:
	movq	-128(%rbp), %rax
	movq	8(%r15), %rbx
	movl	$66321, (%rax)
	movl	$66321, %eax
	jmp	.L1381
.L1871:
	movq	-128(%rbp), %rax
	movq	8(%r15), %rbx
	movl	$66321, (%rax)
	movl	$66321, %eax
	jmp	.L1411
.L1279:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L1280
.L1074:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L1075
.L874:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L875
.L1183:
	movl	268(%r12), %eax
	movl	$10000, 272(%r12)
	leal	1(%rax), %esi
	movq	280(%r12), %rax
	movl	%esi, 268(%r12)
	testq	%rax, %rax
	je	.L1184
	movq	288(%r12), %rdi
	call	*%rax
	testb	%al, %al
	je	.L1889
.L1184:
	movl	264(%r12), %eax
	testl	%eax, %eax
	jle	.L1745
	cmpl	268(%r12), %eax
	movq	-128(%rbp), %rax
	jle	.L1186
	movl	(%rax), %eax
	jmp	.L1168
.L1208:
	movl	268(%r12), %eax
	movl	$10000, 272(%r12)
	leal	1(%rax), %esi
	movq	280(%r12), %rax
	movl	%esi, 268(%r12)
	testq	%rax, %rax
	je	.L1209
	movq	288(%r12), %rdi
	call	*%rax
	testb	%al, %al
	je	.L1890
.L1209:
	movl	264(%r12), %eax
	testl	%eax, %eax
	jle	.L1891
	cmpl	268(%r12), %eax
	jle	.L1211
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1888:
	movq	-128(%rbp), %rax
	movq	(%r15), %rdx
	movl	$66321, (%rax)
	movl	$66321, %eax
	jmp	.L1132
.L1061:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L1062
.L1229:
	movl	%r10d, %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1235:
	movq	(%r15,%rax,8), %rcx
	movq	%rcx, (%rbx,%rax,8)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rcx, %rdx
	jne	.L1235
	jmp	.L1232
.L1019:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L1020
.L1879:
	movq	-128(%rbp), %rax
	movl	$66321, (%rax)
	jmp	.L1147
.L937:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L938
.L1099:
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
	cmpl	44(%rdi), %eax
	jge	.L1100
	movq	48(%rdi), %rsi
	movslq	%eax, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	cmpw	$-10241, %dx
	ja	.L1100
	cmpw	$10, %dx
	je	.L1101
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L926:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L927
.L901:
	movl	268(%r12), %eax
	movl	$10000, 272(%r12)
	leal	1(%rax), %esi
	movq	280(%r12), %rax
	movl	%esi, 268(%r12)
	testq	%rax, %rax
	je	.L903
	movq	288(%r12), %rdi
	call	*%rax
	testb	%al, %al
	je	.L1892
.L903:
	movl	264(%r12), %eax
	testl	%eax, %eax
	jle	.L905
	cmpl	268(%r12), %eax
	jg	.L905
	movq	-128(%rbp), %rax
	movl	$66322, (%rax)
	movl	$66322, %eax
	jmp	.L902
.L1876:
	movq	%r14, %rdi
	movq	-136(%rbp), %r15
	call	_ZN6icu_6724CaseFoldingUTextIterator11inExpansionEv@PLT
	testb	%al, %al
	jne	.L1257
	movq	32(%r12), %rdi
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L1258
	addq	32(%rdi), %rax
.L1259:
	movq	%rax, (%r15)
	jmp	.L1260
.L1127:
	movl	268(%r12), %eax
	movl	$10000, 272(%r12)
	leal	1(%rax), %esi
	movq	280(%r12), %rax
	movl	%esi, 268(%r12)
	testq	%rax, %rax
	je	.L1129
	movq	288(%r12), %rdi
	call	*%rax
	testb	%al, %al
	je	.L1893
.L1129:
	movl	264(%r12), %eax
	testl	%eax, %eax
	jle	.L1131
	cmpl	268(%r12), %eax
	jg	.L1131
	movq	-128(%rbp), %rax
	movl	$66322, (%rax)
	movl	$66322, %eax
	jmp	.L1128
.L948:
	movq	80(%r12), %rax
	cmpq	%rax, (%r15)
	jg	.L949
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1857:
	call	_ZN6icu_676Locale10getEnglishEv@PLT
	movq	-128(%rbp), %r14
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6713BreakIterator23createCharacterInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	(%r14), %esi
	movq	%r14, %rdx
	movq	%rax, 328(%r12)
	movq	%rax, %rdi
	movq	%rbx, %rax
	testl	%esi, %esi
	jg	.L1037
	movq	(%rdi), %rax
	movq	32(%r12), %rsi
	call	*64(%rax)
	movq	328(%r12), %rdi
	jmp	.L1036
.L1853:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L1246
.L1100:
	call	utext_current32_67@PLT
	cmpl	$10, %eax
	je	.L1102
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1881:
	movq	-128(%rbp), %rax
	movl	$66321, (%rax)
	movl	$66321, %eax
	jmp	.L1168
.L1877:
	movb	$1, 168(%r12)
	jmp	.L1257
.L1885:
	movq	-128(%rbp), %rax
	movq	0(%r13), %rsi
	movl	$66321, (%rax)
	movl	$66321, %eax
	jmp	.L1316
.L1847:
	movq	-128(%rbp), %rax
	xorl	%r14d, %r14d
	movl	(%rax), %eax
	jmp	.L1121
.L1883:
	movq	-128(%rbp), %rax
	movq	0(%r13), %rsi
	movl	$66321, (%rax)
	movl	$66321, %eax
	jmp	.L1350
.L1791:
	movq	-128(%rbp), %rax
	xorl	%ebx, %ebx
	movl	(%rax), %eax
	jmp	.L894
.L1855:
	movq	-128(%rbp), %rax
	movl	$66321, (%rax)
	movl	$66321, %eax
	jmp	.L1188
.L1258:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L1259
.L1076:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L1077
.L1063:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L1064
.L1421:
	movl	268(%r12), %eax
	movl	$10000, 272(%r12)
	leal	1(%rax), %esi
	movq	280(%r12), %rax
	movl	%esi, 268(%r12)
	testq	%rax, %rax
	je	.L1423
	movq	288(%r12), %rdi
	call	*%rax
	testb	%al, %al
	je	.L1894
.L1423:
	movl	264(%r12), %eax
	testl	%eax, %eax
	jle	.L1425
	cmpl	268(%r12), %eax
	jg	.L1425
	movq	-128(%rbp), %rax
	movl	$66322, (%rax)
	movl	$66322, %eax
	jmp	.L1422
.L1391:
	movl	268(%r12), %eax
	movl	$10000, 272(%r12)
	leal	1(%rax), %esi
	movq	280(%r12), %rax
	movl	%esi, 268(%r12)
	testq	%rax, %rax
	je	.L1393
	movq	288(%r12), %rdi
	call	*%rax
	testb	%al, %al
	je	.L1895
.L1393:
	movl	264(%r12), %eax
	testl	%eax, %eax
	jle	.L1395
	cmpl	268(%r12), %eax
	jg	.L1395
	movq	-128(%rbp), %rax
	movl	$66322, (%rax)
	movl	$66322, %eax
	jmp	.L1392
.L1446:
	movl	268(%r12), %eax
	movl	$10000, 272(%r12)
	leal	1(%rax), %esi
	movq	280(%r12), %rax
	movl	%esi, 268(%r12)
	testq	%rax, %rax
	je	.L1448
	movq	288(%r12), %rdi
	call	*%rax
	testb	%al, %al
	je	.L1896
.L1448:
	movl	264(%r12), %eax
	testl	%eax, %eax
	jle	.L1450
	cmpl	268(%r12), %eax
	jg	.L1450
	movq	-128(%rbp), %rax
	movl	$66322, (%rax)
	movl	$66322, %eax
	jmp	.L1447
.L1870:
	movq	-128(%rbp), %rax
	xorl	%r13d, %r13d
	movl	(%rax), %eax
	jmp	.L1415
.L905:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L902
.L1835:
	call	utext_current32_67@PLT
	movq	32(%r12), %rdi
	cmpl	$10, %eax
	jne	.L1017
	call	utext_next32_67@PLT
	movq	32(%r12), %rdi
	jmp	.L1017
.L1768:
	movq	-128(%rbp), %rax
	xorl	%ebx, %ebx
	movl	(%rax), %eax
	jmp	.L1441
.L1872:
	movq	-128(%rbp), %rax
	xorl	%r13d, %r13d
	movl	(%rax), %eax
	jmp	.L1385
.L1131:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L1128
.L1142:
	movl	268(%r12), %eax
	movl	$10000, 272(%r12)
	leal	1(%rax), %esi
	movq	280(%r12), %rax
	movl	%esi, 268(%r12)
	testq	%rax, %rax
	je	.L1144
	movq	%rcx, -136(%rbp)
	movq	288(%r12), %rdi
	call	*%rax
	movq	-136(%rbp), %rcx
	testb	%al, %al
	je	.L1897
.L1144:
	movl	264(%r12), %eax
	testl	%eax, %eax
	jle	.L1146
	cmpl	268(%r12), %eax
	jg	.L1146
	movq	-128(%rbp), %rax
	movl	$66322, (%rax)
	movl	$66322, %eax
	jmp	.L1143
.L949:
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
	testl	%eax, %eax
	jle	.L950
	movq	48(%rdi), %rdx
	movslq	%eax, %rsi
	cmpw	$-10241, -2(%rdx,%rsi,2)
	ja	.L950
	subl	$1, %eax
	movl	%eax, 40(%rdi)
	cltq
	movzwl	(%rdx,%rax,2), %eax
.L951:
	cmpl	$13, %eax
	je	.L946
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1891:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1887:
	movq	-128(%rbp), %rax
	xorl	%ecx, %ecx
	movl	(%rax), %eax
	jmp	.L1136
.L1157:
	movl	268(%r12), %eax
	movl	$10000, 272(%r12)
	leal	1(%rax), %esi
	movq	280(%r12), %rax
	movl	%esi, 268(%r12)
	testq	%rax, %rax
	je	.L1159
	movq	%rdx, -176(%rbp)
	movq	288(%r12), %rdi
	movl	%ecx, -136(%rbp)
	call	*%rax
	movl	-136(%rbp), %ecx
	movq	-176(%rbp), %rdx
	testb	%al, %al
	je	.L1898
.L1159:
	movl	264(%r12), %eax
	testl	%eax, %eax
	jle	.L1158
	cmpl	268(%r12), %eax
	jg	.L1158
	movq	-128(%rbp), %rax
	movl	$66322, (%rax)
	jmp	.L1158
.L1532:
	movq	%rbx, %rdi
	movq	%r13, %rsi
.L1442:
	movsq
	cmpq	%rsi, %rbx
	jne	.L1442
	jmp	.L1445
.L1878:
	movq	-128(%rbp), %rax
	xorl	%edx, %edx
	movl	(%rax), %esi
	jmp	.L1151
.L1529:
	movq	%r13, %rdi
	movq	%r14, %rsi
.L1387:
	movsq
	cmpq	%rsi, %r13
	jne	.L1387
	jmp	.L1390
.L1531:
	movq	%r13, %rdi
	movq	%r14, %rsi
.L1417:
	movsq
	cmpq	%rsi, %r13
	jne	.L1417
	jmp	.L1420
.L1510:
	movq	%rdx, %rdi
	movq	%r15, %rsi
.L1153:
	movsq
	cmpq	%rsi, %rdx
	jne	.L1153
	jmp	.L1156
.L1889:
	movq	-128(%rbp), %rax
	movl	$66323, (%rax)
	movl	$66323, %eax
	jmp	.L1168
.L1890:
	movq	-128(%rbp), %rax
	movq	%r12, %rbx
	movl	$66323, (%rax)
	jmp	.L1748
.L1395:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L1392
.L1425:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L1422
.L1450:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L1447
.L1509:
	movq	%rcx, %rdi
	movq	%r15, %rsi
.L1138:
	movsq
	cmpq	%rsi, %rcx
	jne	.L1138
	jmp	.L1141
.L1178:
	movl	268(%r12), %eax
	movl	$10000, 272(%r12)
	leal	1(%rax), %esi
	movq	280(%r12), %rax
	movl	%esi, 268(%r12)
	testq	%rax, %rax
	je	.L1180
	movq	288(%r12), %rdi
	call	*%rax
	testb	%al, %al
	je	.L1899
.L1180:
	movl	264(%r12), %eax
	testl	%eax, %eax
	jle	.L1182
	cmpl	268(%r12), %eax
	jg	.L1182
	movq	-128(%rbp), %rax
	movl	$66322, (%rax)
	movl	$66322, %eax
	jmp	.L1179
.L1326:
	movl	268(%r12), %eax
	movl	$10000, 272(%r12)
	leal	1(%rax), %esi
	movq	280(%r12), %rax
	movl	%esi, 268(%r12)
	testq	%rax, %rax
	je	.L1328
	movq	288(%r12), %rdi
	call	*%rax
	testb	%al, %al
	je	.L1900
.L1328:
	movl	264(%r12), %eax
	testl	%eax, %eax
	jle	.L1330
	cmpl	268(%r12), %eax
	jg	.L1330
	movq	-128(%rbp), %rax
	movl	$66322, (%rax)
	movl	$66322, %eax
	jmp	.L1327
.L1146:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L1143
.L1880:
	movq	-128(%rbp), %rax
	xorl	%r14d, %r14d
	movl	(%rax), %eax
	jmp	.L1172
.L1102:
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
	cmpl	44(%rdi), %eax
	jge	.L1103
	movq	48(%rdi), %rsi
	movslq	%eax, %rdx
	cmpw	$-10241, (%rsi,%rdx,2)
	jbe	.L1101
.L1103:
	call	utext_next32_67@PLT
	movq	32(%r12), %rdi
.L1104:
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L1105
	addq	32(%rdi), %rax
.L1106:
	movq	%rax, (%r15)
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L867
.L1200:
	movl	268(%r12), %eax
	movl	$10000, 272(%r12)
	leal	1(%rax), %esi
	movq	280(%r12), %rax
	movl	%esi, 268(%r12)
	testq	%rax, %rax
	je	.L1202
	movq	288(%r12), %rdi
	call	*%rax
	testb	%al, %al
	je	.L1901
.L1202:
	movl	264(%r12), %eax
	testl	%eax, %eax
	jle	.L1204
	cmpl	268(%r12), %eax
	jg	.L1204
	movq	-128(%rbp), %rax
	movl	$66322, (%rax)
	movl	$66322, %eax
	jmp	.L1201
.L1222:
	movl	268(%r12), %eax
	movl	$10000, 272(%r12)
	leal	1(%rax), %esi
	movq	280(%r12), %rax
	movl	%esi, 268(%r12)
	testq	%rax, %rax
	je	.L1224
	movq	288(%r12), %rdi
	call	*%rax
	testb	%al, %al
	je	.L1902
.L1224:
	movl	264(%r12), %eax
	testl	%eax, %eax
	jle	.L1226
	cmpl	268(%r12), %eax
	jg	.L1226
	movq	-128(%rbp), %rax
	movl	$66322, (%rax)
	movl	$66322, %eax
	jmp	.L1223
.L1886:
	movq	-128(%rbp), %rax
	xorl	%ebx, %ebx
	movl	(%rax), %eax
	jmp	.L1216
.L1884:
	movq	-128(%rbp), %rax
	xorl	%r14d, %r14d
	movl	(%rax), %eax
	jmp	.L1320
.L1882:
	movq	-128(%rbp), %rax
	xorl	%r14d, %r14d
	movl	(%rax), %eax
	jmp	.L1354
.L1211:
	movq	-128(%rbp), %rax
	movq	%r12, %rbx
	movl	$66322, (%rax)
	jmp	.L1748
.L950:
	call	utext_previous32_67@PLT
	jmp	.L951
.L1186:
	movl	$66322, (%rax)
	movl	$66322, %eax
	jmp	.L1168
.L1360:
	movl	268(%r12), %eax
	movl	$10000, 272(%r12)
	leal	1(%rax), %esi
	movq	280(%r12), %rax
	movl	%esi, 268(%r12)
	testq	%rax, %rax
	je	.L1362
	movq	288(%r12), %rdi
	call	*%rax
	testb	%al, %al
	je	.L1903
.L1362:
	movl	264(%r12), %eax
	testl	%eax, %eax
	jle	.L1364
	cmpl	268(%r12), %eax
	jg	.L1364
	movq	-128(%rbp), %rax
	movl	$66322, (%rax)
	movl	$66322, %eax
	jmp	.L1361
.L1105:
	movq	56(%rdi), %rax
	call	*64(%rax)
	jmp	.L1106
.L1854:
	movq	-128(%rbp), %rax
	xorl	%r14d, %r14d
	movl	(%rax), %eax
	jmp	.L1194
.L1330:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L1327
.L1892:
	movq	-128(%rbp), %rax
	movl	$66323, (%rax)
	movl	$66323, %eax
	jmp	.L902
.L1893:
	movq	-128(%rbp), %rax
	movl	$66323, (%rax)
	movl	$66323, %eax
	jmp	.L1128
.L1101:
	addl	$1, %eax
	movl	%eax, 40(%rdi)
	jmp	.L1104
.L1512:
	movq	%r14, %rdi
	movq	%r15, %rsi
.L1174:
	movsq
	cmpq	%rsi, %r14
	jne	.L1174
	jmp	.L1177
.L1894:
	movq	-128(%rbp), %rax
	movl	$66323, (%rax)
	movl	$66323, %eax
	jmp	.L1422
.L1895:
	movq	-128(%rbp), %rax
	movl	$66323, (%rax)
	movl	$66323, %eax
	jmp	.L1392
.L1896:
	movq	-128(%rbp), %rax
	movl	$66323, (%rax)
	movl	$66323, %eax
	jmp	.L1447
.L1524:
	movq	%r14, %rdi
	movq	%r15, %rsi
.L1322:
	movsq
	cmpq	%rsi, %r14
	jne	.L1322
	jmp	.L1325
.L922:
	call	utext_previous32_67@PLT
	jmp	.L923
.L920:
	call	utext_previous32_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
	jmp	.L921
.L1514:
	movq	%rbx, %rdi
	movq	%r14, %rsi
.L1218:
	movsq
	cmpq	%rsi, %rbx
	jne	.L1218
	jmp	.L1221
.L1182:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L1179
.L1513:
	movq	%r14, %rdi
	movq	%r15, %rsi
.L1196:
	movsq
	cmpq	%rsi, %r14
	jne	.L1196
	jmp	.L1199
.L1226:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L1223
.L1526:
	movq	%r14, %rdi
	movq	%r15, %rsi
.L1356:
	movsq
	cmpq	%rsi, %r14
	jne	.L1356
	jmp	.L1359
.L1364:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L1361
.L1899:
	movq	-128(%rbp), %rax
	movl	$66323, (%rax)
	movl	$66323, %eax
	jmp	.L1179
.L1900:
	movq	-128(%rbp), %rax
	movl	$66323, (%rax)
	movl	$66323, %eax
	jmp	.L1327
.L1898:
	movq	-128(%rbp), %rax
	movl	$66323, (%rax)
	jmp	.L1158
.L1436:
	movq	56(%rdi), %rax
	call	*64(%rax)
	movq	8(%r15), %r14
	jmp	.L1437
.L1204:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L1201
.L1903:
	movq	-128(%rbp), %rax
	movl	$66323, (%rax)
	movl	$66323, %eax
	jmp	.L1361
.L1897:
	movq	-128(%rbp), %rax
	movl	$66323, (%rax)
	movl	$66323, %eax
	jmp	.L1143
.L1901:
	movq	-128(%rbp), %rax
	movl	$66323, (%rax)
	movl	$66323, %eax
	jmp	.L1201
.L1902:
	movq	-128(%rbp), %rax
	movl	$66323, (%rax)
	movl	$66323, %eax
	jmp	.L1223
.L1756:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0.cold, @function
_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0.cold:
.LFSB4497:
.L808:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE4497:
	.text
	.size	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0, .-_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0.cold, .-_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0.cold
.LCOLDE2:
	.text
.LHOTE2:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode, @function
_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode:
.LFB3319:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1904
	movsbl	%dl, %edx
	jmp	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1904:
	ret
	.cfi_endproc
.LFE3319:
	.size	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode, .-_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode
	.section	.text.unlikely
	.align 2
.LCOLDB3:
	.text
.LHOTB3:
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0, @function
_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0:
.LFB4498:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$200, %rsp
	movb	%dl, -201(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	32(%rax), %rdx
	movq	24(%rdx), %rdi
	movzwl	48(%rax), %edx
	movq	%rdi, %r14
	testb	$17, %dl
	jne	.L2406
	andl	$2, %edx
	jne	.L2800
	movq	64(%rax), %rdi
	movq	%rdi, -176(%rbp)
.L1907:
	movq	104(%rax), %rdi
	movq	32(%r15), %rdx
	movl	128(%rax), %eax
	movq	%rdi, -200(%rbp)
	movq	48(%rdx), %rdi
	movl	%eax, 56(%r15)
	movq	%rdi, -160(%rbp)
	movq	176(%r15), %rdi
	call	_ZN6icu_679UVector6417removeAllElementsEv@PLT
	movq	8(%r15), %rax
	movq	176(%r15), %r12
	movl	128(%rax), %r13d
	movslq	8(%r12), %rax
	movl	%r13d, %esi
	addl	%eax, %esi
	js	.L1909
	cmpl	12(%r12), %esi
	jle	.L1910
.L1909:
	leaq	316(%r15), %rdx
	movq	%r12, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L2407
	movslq	8(%r12), %rax
	leal	0(%r13,%rax), %esi
.L1910:
	movq	24(%r12), %rdx
	movl	%esi, 8(%r12)
	leaq	(%rdx,%rax,8), %r11
.L1911:
	movl	316(%r15), %eax
	testl	%eax, %eax
	jg	.L1912
	movq	8(%r15), %r12
	movl	128(%r12), %eax
	cmpl	$2, %eax
	jle	.L1913
	subl	$3, %eax
	leaq	16(%r11), %rdi
	movl	$255, %esi
	movq	%r11, -168(%rbp)
	leaq	8(,%rax,8), %rdx
	call	memset@PLT
	movq	-168(%rbp), %r11
.L1913:
	movslq	%ebx, %rax
	movq	$0, 8(%r11)
	movq	%rax, -216(%rbp)
	movq	%rax, (%r11)
	movl	132(%r12), %eax
	testl	%eax, %eax
	jle	.L2801
	subl	$1, %eax
	movq	192(%r15), %rdi
	xorl	%esi, %esi
	movq	%r11, -168(%rbp)
	leaq	8(,%rax,8), %rdx
	call	memset@PLT
	movq	-168(%rbp), %r11
	movq	8(%r11), %rbx
.L2400:
	movq	%r11, %r12
	leaq	.L1916(%rip), %r10
	movq	%r15, %r11
	.p2align 4,,10
	.p2align 3
.L2393:
	movq	(%r14,%rbx,8), %rdx
	leaq	1(%rbx), %r13
	leaq	0(,%rbx,8), %rax
	movq	%r13, 8(%r12)
	movl	%edx, %r15d
	movl	%edx, %esi
	movl	%edx, %ecx
	andl	$16777215, %r15d
	shrl	$24, %esi
	cmpl	$989855743, %edx
	ja	.L1914
	movslq	(%r10,%rsi,4), %rsi
	addq	%r10, %rsi
	notrack jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L1916:
	.long	.L1914-.L1916
	.long	.L1971-.L1916
	.long	.L1970-.L1916
	.long	.L1969-.L1916
	.long	.L1968-.L1916
	.long	.L1914-.L1916
	.long	.L1967-.L1916
	.long	.L1966-.L1916
	.long	.L1965-.L1916
	.long	.L1964-.L1916
	.long	.L1963-.L1916
	.long	.L1962-.L1916
	.long	.L1961-.L1916
	.long	.L1960-.L1916
	.long	.L1959-.L1916
	.long	.L1958-.L1916
	.long	.L1957-.L1916
	.long	.L1956-.L1916
	.long	.L1955-.L1916
	.long	.L1954-.L1916
	.long	.L1953-.L1916
	.long	.L1952-.L1916
	.long	.L1951-.L1916
	.long	.L1950-.L1916
	.long	.L1949-.L1916
	.long	.L1948-.L1916
	.long	.L1947-.L1916
	.long	.L1946-.L1916
	.long	.L1945-.L1916
	.long	.L1944-.L1916
	.long	.L1943-.L1916
	.long	.L1914-.L1916
	.long	.L1942-.L1916
	.long	.L1941-.L1916
	.long	.L1940-.L1916
	.long	.L1939-.L1916
	.long	.L1938-.L1916
	.long	.L1937-.L1916
	.long	.L1936-.L1916
	.long	.L1935-.L1916
	.long	.L1934-.L1916
	.long	.L1933-.L1916
	.long	.L1932-.L1916
	.long	.L1931-.L1916
	.long	.L1930-.L1916
	.long	.L1929-.L1916
	.long	.L1928-.L1916
	.long	.L1927-.L1916
	.long	.L1926-.L1916
	.long	.L1925-.L1916
	.long	.L1924-.L1916
	.long	.L1923-.L1916
	.long	.L1922-.L1916
	.long	.L1921-.L1916
	.long	.L1920-.L1916
	.long	.L1919-.L1916
	.long	.L1918-.L1916
	.long	.L1917-.L1916
	.long	.L1915-.L1916
	.text
	.p2align 4,,10
	.p2align 3
.L2800:
	leaq	50(%rax), %rdi
	movq	%rdi, -176(%rbp)
	jmp	.L1907
	.p2align 4,,10
	.p2align 3
.L1944:
	movslq	%r15d, %rax
	leaq	0(,%rax,8), %rdx
	movq	(%r14,%rax,8), %rax
	movq	24(%r14,%rdx), %rsi
	movq	16(%r14,%rdx), %rcx
	andl	$16777215, %eax
	leaq	(%r12,%rax,8), %rdx
	movl	%esi, %edi
	movq	16(%rdx), %rax
	addq	$1, %rax
	movq	%rax, 16(%rdx)
	cmpq	%rdi, %rax
	jnb	.L2802
	movslq	%ecx, %rcx
	cmpq	%rcx, %rax
	jl	.L2396
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	cmpl	$-1, %esi
	je	.L2397
.L2204:
	testl	%ecx, %ecx
	jle	.L2803
	.p2align 4,,10
	.p2align 3
.L1959:
	movq	%r11, %r15
	movq	%r12, %r11
.L2799:
	movb	$0, 130(%r15)
.L2394:
	movq	%r11, 184(%r15)
.L1906:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2804
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1958:
	.cfi_restore_state
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L2805
.L2109:
	movslq	%r15d, %r15
	movq	%r15, 8(%r12)
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1915:
	movq	(%r12), %rax
	movq	120(%r11), %rcx
	cmpq	%rcx, %rax
	jl	.L2062
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movb	$1, 168(%r11)
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2063
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2063:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1920:
	movq	88(%r11), %rax
	movq	(%r12), %rdx
	subq	$1, %rax
	cmpq	%rax, %rdx
	jge	.L2806
.L2012:
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2014
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2014:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1921:
	cmpq	$0, 320(%r11)
	movq	(%r12), %rbx
	je	.L2807
.L2036:
	cmpq	104(%r11), %rbx
	jl	.L2038
	movb	$1, 168(%r11)
	movl	$1, %eax
.L2039:
	testl	%r15d, %r15d
	setne	%dl
	cmpb	%al, %dl
	je	.L2040
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1922:
	movq	(%r12), %rdi
	andl	$1, %edx
	movq	120(%r11), %r15
	movq	%rdi, -168(%rbp)
	je	.L2348
	movb	$1, 168(%r11)
	movslq	%r15d, %rdx
.L2349:
	movq	-152(%rbp), %rdi
	movl	(%rdi), %ecx
	cmpq	%rdx, -168(%rbp)
	je	.L2808
	movq	8(%r14,%rax), %rax
	movq	-168(%rbp), %rdi
	andl	$16777215, %eax
	movq	%rdi, 16(%r12,%rax,8)
	movq	%rdx, (%r12)
	testl	%ecx, %ecx
	jle	.L2809
.L2360:
	addq	$1, %r13
	movq	%r13, 8(%r12)
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1923:
	movslq	%r15d, %r15
	movq	-152(%rbp), %rcx
	movq	(%r12), %rsi
	movslq	16(%r12,%r15,8), %rdi
	movl	(%rcx), %ecx
	cmpq	%rsi, %rdi
	je	.L1973
	movq	-160(%rbp), %r8
	leaq	-1(%rsi), %rdx
	movq	%rdx, (%r12)
	leaq	(%rdx,%rdx), %r13
	movzwl	(%r8,%rdx,2), %r8d
	movl	%r8d, %r9d
	andl	$64512, %r9d
	cmpl	$56320, %r9d
	jne	.L2375
	testq	%rdx, %rdx
	jg	.L2810
.L2375:
	cmpw	$10, %r8w
	je	.L2811
.L2376:
	testl	%ecx, %ecx
	jg	.L1959
	movq	176(%r11), %r15
	movl	56(%r11), %r13d
	movslq	8(%r15), %rax
	movl	%r13d, %esi
	addl	%eax, %esi
	js	.L2379
	cmpl	12(%r15), %esi
	jle	.L2380
.L2379:
	movq	-152(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r11, -168(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2812
	movq	-152(%rbp), %rdi
	movslq	8(%r15), %rax
	movl	(%rdi), %ecx
	leal	0(%r13,%rax), %esi
.L2380:
	movq	24(%r15), %rdx
	movl	%esi, 8(%r15)
	leaq	(%rdx,%rax,8), %r13
.L2382:
	testl	%ecx, %ecx
	jg	.L2209
	movslq	56(%r11), %rax
	movq	%r13, %r12
	leaq	16(%r13), %rdx
	salq	$3, %rax
	subq	%rax, %r12
	leaq	-8(%rax), %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %r12
	movl	$16, %edx
	setnb	%dil
	subq	%rax, %rdx
	testq	%rdx, %rdx
	setle	%al
	orb	%al, %dil
	je	.L2480
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L2480
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L2384:
	movdqu	(%r12,%rax), %xmm6
	movups	%xmm6, 0(%r13,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L2384
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rsi, %rdi
	je	.L2386
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L2386:
	movl	272(%r11), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L2387
	movl	%eax, 272(%r11)
.L2388:
	movq	%rbx, 8(%r12)
	movq	%r13, %r12
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1918:
	movq	(%r12), %rax
	movq	120(%r11), %rdx
	cmpq	%rdx, %rax
	jl	.L2050
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movb	$1, 168(%r11)
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2051
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2051:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1917:
	movq	(%r12), %rdx
	movq	120(%r11), %r9
	cmpq	%r9, %rdx
	jl	.L2055
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movb	$1, 168(%r11)
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2056
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2056:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1919:
	movq	(%r12), %rax
	cmpq	88(%r11), %rax
	jge	.L2813
	movq	-160(%rbp), %rdi
	cmpw	$10, (%rdi,%rax,2)
	je	.L2814
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	js	.L2815
.L2023:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1924:
	movq	8(%r11), %rdx
	movslq	%r15d, %rax
	movq	-200(%rbp), %rdi
	movl	%r15d, %esi
	salq	$5, %rax
	movq	%r11, -184(%rbp)
	addq	112(%rdx), %rax
	movq	%rax, -168(%rbp)
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	-184(%rbp), %r11
	movq	(%r12), %rsi
	leaq	.L1916(%rip), %r10
	movq	%rax, -192(%rbp)
	movq	120(%r11), %rcx
	movslq	%esi, %rbx
	movl	%esi, %r15d
	cmpq	%rcx, %rbx
	jge	.L2319
	movq	%r12, -232(%rbp)
	movq	%rbx, %r13
	movq	-160(%rbp), %r12
	movq	%r14, -224(%rbp)
	jmp	.L2320
	.p2align 4,,10
	.p2align 3
.L2474:
	movl	%r15d, %edi
	movl	%ebx, %r15d
	movl	%edi, %ebx
	cmpl	$255, %esi
	jg	.L2323
.L2818:
	movl	%esi, %edi
	movq	-168(%rbp), %rdx
	andl	$7, %esi
	sarl	$3, %edi
	movslq	%edi, %rdi
	movsbl	(%rdx,%rdi), %edi
	btl	%esi, %edi
	jnc	.L2816
	movslq	%r15d, %rbx
	movq	%rbx, %r13
	cmpq	%rcx, %rbx
	jge	.L2817
.L2320:
	addq	%r13, %r13
	leal	1(%r15), %ebx
	leaq	(%r12,%r13), %r14
	movzwl	(%r14), %esi
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L2474
	movslq	%ebx, %rdi
	cmpq	%rcx, %rdi
	je	.L2474
	leaq	2(%r13), %rdi
	leaq	(%r12,%rdi), %r8
	movzwl	(%r8), %r9d
	movl	%r9d, %r11d
	andl	$-1024, %r11d
	cmpl	$56320, %r11d
	jne	.L2474
	sall	$10, %esi
	addl	$2, %r15d
	movq	%r8, %r14
	movq	%rdi, %r13
	leal	-56613888(%r9,%rsi), %esi
	movl	$56320, %eax
	cmpl	$255, %esi
	jle	.L2818
	.p2align 4,,10
	.p2align 3
.L2323:
	movq	-192(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2328
	movq	-184(%rbp), %rax
	movslq	%r15d, %rbx
	movq	%rbx, %r13
	movq	120(%rax), %rcx
	cmpq	%rcx, %rbx
	jl	.L2320
.L2817:
	movq	-232(%rbp), %r12
	movq	-224(%rbp), %r14
	movq	-184(%rbp), %r11
	movq	(%r12), %rsi
.L2319:
	movb	$1, 168(%r11)
.L2321:
	movq	-152(%rbp), %rax
	movq	8(%r12), %r13
	movl	(%rax), %ecx
	cmpq	%rsi, %rbx
	je	.L2819
	movq	(%r14,%r13,8), %rax
	andl	$16777215, %eax
	movq	%rsi, 16(%r12,%rax,8)
	movq	%rbx, (%r12)
	testl	%ecx, %ecx
	jle	.L2820
.L2333:
	addq	$1, %r13
	movq	%r13, 8(%r12)
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1925:
	movq	(%r12), %rax
	movq	120(%r11), %rdx
	cmpq	%rdx, %rax
	jl	.L2083
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movb	$1, 168(%r11)
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2084
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2084:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1926:
	movq	176(%r11), %rdi
	movq	120(%r11), %rax
	cmpq	%rax, (%r12)
	je	.L2316
	movslq	56(%r11), %rax
	movl	8(%rdi), %edx
	movq	24(%rdi), %rsi
	subl	%eax, %edx
	movslq	%edx, %rcx
	jns	.L2317
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L2317:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rdi)
	leaq	(%rsi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1927:
	leaq	2(%rbx), %rdx
	movq	192(%r11), %rsi
	movslq	%r15d, %r15
	movq	%rdx, 8(%r12)
	leaq	3(%rbx), %rdx
	movq	8(%r14,%rax), %r8
	addq	$4, %rbx
	movq	%rdx, 8(%r12)
	leaq	32(,%r15,8), %rdx
	movq	16(%r14,%rax), %r13
	movq	%rbx, 8(%r12)
	leaq	(%rsi,%rdx), %rbx
	movq	24(%r14,%rax), %rdi
	movq	(%rbx), %r9
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	testq	%r9, %r9
	js	.L2821
	jne	.L2296
	movq	$-1, (%rbx)
.L2297:
	movq	-16(%rsi,%rdx), %rax
	andl	$16777215, %edi
	movq	%rax, 112(%r11)
	movq	-8(%rsi,%rdx), %rax
	movq	%rax, 120(%r11)
	movq	%rdi, 8(%r12)
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1928:
	movq	120(%r11), %rax
	cmpq	%rax, (%r12)
	je	.L2291
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	jns	.L2292
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L2292:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1929:
	movq	192(%r11), %rsi
	movslq	%r15d, %r15
	leaq	2(%rbx), %rdx
	addq	$3, %rbx
	leaq	32(,%r15,8), %rcx
	movq	%rdx, 8(%r12)
	movq	8(%r14,%rax), %rdi
	leaq	(%rsi,%rcx), %r13
	movq	%rbx, 8(%r12)
	movq	16(%r14,%rax), %rdx
	movq	0(%r13), %r8
	testq	%r8, %r8
	js	.L2822
	jne	.L2270
	movq	$-1, 0(%r13)
.L2271:
	movq	176(%r11), %r8
	movslq	56(%r11), %rax
	movl	8(%r8), %edx
	movq	24(%r8), %r9
	subl	%eax, %edx
	movslq	%edx, %rdi
	jns	.L2275
	xorl	%edi, %edi
	xorl	%edx, %edx
.L2275:
	movl	%edx, 8(%r8)
	subq	%rax, %rdi
	movq	-16(%rsi,%rcx), %rax
	leaq	(%r9,%rdi,8), %r12
	movq	%rax, 112(%r11)
	movq	-8(%rsi,%rcx), %rax
	movq	%rax, 120(%r11)
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1930:
	movq	176(%r11), %rcx
	movq	192(%r11), %rax
	movslq	%r15d, %r15
	leaq	0(,%r15,8), %rdx
	movslq	8(%rcx), %rcx
	movq	%rcx, (%rax,%r15,8)
	movq	(%r12), %rcx
	movq	%rcx, 8(%rax,%rdx)
	movq	112(%r11), %rcx
	movq	%rcx, 16(%rax,%rdx)
	movq	120(%r11), %rcx
	movq	%rcx, 24(%rax,%rdx)
	movq	64(%r11), %rcx
	movq	%rcx, 112(%r11)
	movq	(%r12), %rcx
	movq	%rcx, 120(%r11)
	movq	$-1, 32(%rax,%rdx)
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	.p2align 4,,10
	.p2align 3
.L1973:
	testl	%ecx, %ecx
	jg	.L1959
	movq	8(%r12), %rbx
	jmp	.L2393
	.p2align 4,,10
	.p2align 3
.L1931:
	movq	(%r12), %rax
	cmpq	80(%r11), %rax
	je	.L2823
	cmpq	88(%r11), %rax
	jge	.L2027
	movq	-160(%rbp), %rdi
	movzwl	-2(%rdi,%rax,2), %eax
	testl	$57168, %eax
	jne	.L2027
	leal	-10(%rax), %edx
	cmpl	$3, %edx
	jbe	.L2482
	cmpl	$133, %eax
	je	.L2482
	subl	$8232, %eax
	cmpl	$1, %eax
	ja	.L2027
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1932:
	movq	(%r12), %rax
	cmpq	88(%r11), %rax
	jge	.L2824
	movq	-160(%rbp), %rdi
	leaq	(%rax,%rax), %rcx
	movzwl	(%rdi,%rax,2), %edx
	testl	$57168, %edx
	jne	.L2016
	leal	-10(%rdx), %esi
	cmpl	$3, %esi
	jbe	.L2017
	cmpl	$133, %edx
	je	.L2017
	leal	-8232(%rdx), %esi
	cmpl	$1, %esi
	ja	.L2016
.L2017:
	cmpl	$10, %edx
	je	.L2018
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1933:
	movslq	%r15d, %r15
	leaq	(%r12,%r15,8), %rax
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	jns	.L2236
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2237
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2237:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1934:
	addq	$2, %rbx
	movq	8(%r14,%rax), %rax
	movslq	%r15d, %r15
	movq	120(%r11), %rcx
	movq	-176(%rbp), %rdi
	movq	%rbx, 8(%r12)
	leaq	-96(%rbp), %r13
	movq	(%r12), %rdx
	movq	-160(%rbp), %rsi
	movq	%r11, -192(%rbp)
	movl	%eax, %ebx
	leaq	(%rdi,%r15,2), %rdi
	xorl	%r15d, %r15d
	andl	$16777215, %ebx
	movq	%rdi, -168(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_6724CaseFoldingUCharIteratorC1EPKDsll@PLT
	movq	-192(%rbp), %r11
	movq	%r14, -184(%rbp)
	movl	%r15d, %r14d
	movq	%r12, -224(%rbp)
	movq	-168(%rbp), %r12
	movq	%r11, -168(%rbp)
	jmp	.L2262
	.p2align 4,,10
	.p2align 3
.L2464:
	movq	%r13, %rdi
	movl	%esi, %r14d
	call	_ZN6icu_6724CaseFoldingUCharIterator4nextEv@PLT
	cmpl	%r15d, %eax
	jne	.L2825
.L2262:
	cmpl	%r14d, %ebx
	jle	.L2260
	movslq	%r14d, %rax
	leal	1(%r14), %esi
	movzwl	(%r12,%rax,2), %r15d
	leaq	(%rax,%rax), %rdi
	movl	%r15d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L2464
	cmpl	%esi, %ebx
	je	.L2464
	movzwl	2(%r12,%rdi), %eax
	movl	%eax, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L2464
	movl	%r15d, %ecx
	movq	%r13, %rdi
	addl	$2, %r14d
	sall	$10, %ecx
	leal	-56613888(%rax,%rcx), %r15d
	call	_ZN6icu_6724CaseFoldingUCharIterator4nextEv@PLT
	cmpl	%r15d, %eax
	je	.L2262
.L2825:
	movq	-184(%rbp), %r14
	movq	-168(%rbp), %r11
	cmpl	$-1, %eax
	je	.L2826
	movq	%r13, %rdi
	movq	%r11, -168(%rbp)
	call	_ZN6icu_6724CaseFoldingUCharIterator11inExpansionEv@PLT
	movq	-168(%rbp), %r11
.L2264:
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	jns	.L2266
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L2266:
	movl	%edx, 8(%rsi)
	subq	%rax, %rcx
	leaq	(%rdi,%rcx,8), %r12
.L2265:
	movq	%r13, %rdi
	movq	%r11, -168(%rbp)
	call	_ZN6icu_6724CaseFoldingUCharIteratorD1Ev@PLT
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1935:
	movq	(%r12), %rax
	movq	120(%r11), %rdx
	cmpq	%rdx, %rax
	jl	.L2827
	movb	$1, 168(%r11)
.L2258:
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	jns	.L2259
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L2259:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1936:
	movq	192(%r11), %rax
	movq	176(%r11), %rdi
	movslq	%r15d, %r15
	leaq	0(,%r15,8), %rbx
	movq	(%rax,%r15,8), %rsi
	cmpl	%esi, 8(%rdi)
	jg	.L2828
.L2247:
	movq	8(%rax,%rbx), %rdx
	movq	%rdx, (%r12)
	movq	16(%rax,%rbx), %rdx
	movq	%rdx, 112(%r11)
	movq	24(%rax,%rbx), %rax
	movq	%rax, 120(%r11)
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1937:
	movq	176(%r11), %rcx
	movq	192(%r11), %rax
	movslq	%r15d, %r15
	leaq	0(,%r15,8), %rdx
	movslq	8(%rcx), %rcx
	movq	%rcx, (%rax,%r15,8)
	movq	(%r12), %rcx
	movq	%rcx, 8(%rax,%rdx)
	movq	112(%r11), %rcx
	movq	%rcx, 16(%rax,%rdx)
	movq	120(%r11), %rcx
	movq	%rcx, 24(%rax,%rdx)
	movq	-152(%rbp), %rax
	movdqu	96(%r11), %xmm6
	movl	(%rax), %ecx
	movups	%xmm6, 112(%r11)
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1938:
	addq	$2, %rbx
	movslq	%r13d, %r13
	movq	%rbx, 8(%r12)
	movq	(%r14,%r13,8), %rax
	andl	$16777215, %eax
	movslq	16(%r12,%rax,8), %rax
	cmpq	(%r12), %rax
	jl	.L2829
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2246
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2246:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1939:
	movq	(%r12), %rax
	movslq	%r15d, %r15
	movq	%rax, 16(%r12,%r15,8)
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1956:
	cmpb	$0, 130(%r11)
	movq	(%r12), %rax
	je	.L2047
	cmpq	%rax, 144(%r11)
	je	.L2830
.L2048:
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	jns	.L2049
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L2049:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1957:
	movl	(%r12), %esi
	movq	%r11, %rdi
	movq	%r11, -168(%rbp)
	call	_ZN6icu_6712RegexMatcher19isChunkWordBoundaryEi
	testl	%r15d, %r15d
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	movl	%eax, %r8d
	setne	%al
	cmpb	%al, %r8b
	je	.L2034
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1960:
	movq	-152(%rbp), %rax
	movslq	%r15d, %r15
	movq	%r15, 8(%r12)
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1961:
	movq	(%r12), %rax
	movq	120(%r11), %rcx
	cmpq	%rcx, %rax
	jl	.L2095
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movb	$1, 168(%r11)
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2096
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2096:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1962:
	movq	(%r12), %rax
	movq	120(%r11), %rdx
	cmpq	%rdx, %rax
	jl	.L2089
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movb	$1, 168(%r11)
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2090
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2090:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1963:
	movq	(%r12), %rax
	movq	120(%r11), %rcx
	cmpq	%rcx, %rax
	jl	.L2076
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movb	$1, 168(%r11)
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2077
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2077:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1964:
	leal	2(%r15), %eax
	movslq	%r15d, %r15
	cltq
	movq	16(%r12,%rax,8), %xmm0
	movq	-152(%rbp), %rax
	movhps	(%r12), %xmm0
	movl	(%rax), %ecx
	movups	%xmm0, 16(%r12,%r15,8)
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1965:
	movq	(%r12), %rax
	addl	$2, %r15d
	movslq	%r15d, %r15
	movq	%rax, 16(%r12,%r15,8)
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1966:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1967:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L1959
	movq	176(%r11), %r13
	movl	56(%r11), %ebx
	movslq	8(%r13), %rax
	movl	%ebx, %esi
	addl	%eax, %esi
	js	.L1984
	cmpl	12(%r13), %esi
	jle	.L1985
.L1984:
	movq	-152(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r11, -168(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2831
	movq	-152(%rbp), %rdi
	movslq	8(%r13), %rax
	movl	(%rdi), %ecx
	leal	(%rbx,%rax), %esi
.L1985:
	movq	24(%r13), %rdx
	movl	%esi, 8(%r13)
	leaq	(%rdx,%rax,8), %rbx
.L1987:
	testl	%ecx, %ecx
	jg	.L2209
	movslq	56(%r11), %rax
	movl	$16, %edx
	movq	%rbx, %r12
	salq	$3, %rax
	subq	%rax, %rdx
	leaq	-8(%rax), %rsi
	subq	%rax, %r12
	shrq	$3, %rsi
	leaq	16(%rbx), %rax
	testq	%rdx, %rdx
	setle	%dl
	cmpq	%rax, %r12
	setnb	%al
	orb	%al, %dl
	je	.L2411
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L2411
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1991:
	movdqu	(%r12,%rax), %xmm4
	movups	%xmm4, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1991
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%rsi, %rdi
	je	.L1993
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1993:
	movl	272(%r11), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L1994
	movl	%eax, 272(%r11)
.L1995:
	movslq	%r15d, %r15
	movq	%r15, 8(%r12)
	movq	%rbx, %r12
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1968:
	movq	120(%r11), %rdx
	movq	(%r12), %r8
	addq	$2, %rbx
	movslq	%r15d, %r15
	movq	-160(%rbp), %rcx
	movq	8(%r14,%rax), %rdi
	movq	%rbx, 8(%r12)
	leaq	(%rcx,%r8,2), %rax
	leaq	(%rcx,%rdx,2), %rsi
	movq	-176(%rbp), %rcx
	andl	$16777215, %edi
	leaq	(%rcx,%r15,2), %rdx
	leaq	(%rax,%rdi,2), %rcx
	jmp	.L1981
	.p2align 4,,10
	.p2align 3
.L2833:
	cmpq	%rax, %rsi
	jbe	.L2832
	addq	$2, %rax
	addq	$2, %rdx
	movzwl	-2(%rdx), %ebx
	cmpw	%bx, -2(%rax)
	jne	.L1980
.L1981:
	cmpq	%rax, %rcx
	ja	.L2833
	movq	-152(%rbp), %rax
	addq	%r8, %rdi
	movq	%rdi, (%r12)
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1969:
	movq	(%r12), %rax
	movq	120(%r11), %rdx
	cmpq	%rdx, %rax
	jl	.L2834
	movb	$1, 168(%r11)
.L1976:
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	jns	.L1977
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L1977:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1970:
	cmpb	$0, -201(%rbp)
	movq	(%r12), %rax
	je	.L1999
	cmpq	%rax, 120(%r11)
	je	.L1999
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	js	.L2835
.L2000:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1971:
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	js	.L2836
.L1972:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1940:
	movslq	%r15d, %r15
	leaq	(%r12,%r15,8), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	jns	.L2228
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2229
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2229:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1948:
	movslq	%r13d, %r13
	movslq	%r15d, %rax
	addq	$4, %rbx
	movq	$0, 16(%r12,%rax,8)
	addq	$1, %r13
	leaq	0(,%r13,8), %rdx
	movq	%rbx, 8(%r12)
	movl	(%r14,%r13,8), %eax
	movq	8(%r14,%rdx), %rbx
	movl	%ebx, %r8d
	testl	%eax, %eax
	jne	.L2139
	movq	-152(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L2837
.L2139:
	cmpl	$-1, %ebx
	je	.L2838
	testl	%ebx, %ebx
	je	.L2154
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1949:
	movq	88(%r11), %rdx
	movq	(%r12), %rax
	leaq	-2(%rdx), %rcx
	cmpq	%rcx, %rax
	jge	.L2001
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2002
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2002:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1950:
	movq	80(%r11), %rax
	cmpq	%rax, (%r12)
	je	.L2839
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	jns	.L2025
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L2025:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1951:
	movq	(%r12), %rax
	movq	120(%r11), %rdx
	cmpq	%rdx, %rax
	jl	.L2042
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movb	$1, 168(%r11)
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2043
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2043:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1952:
	movq	(%r12), %rax
	movq	120(%r11), %rdx
	cmpq	%rdx, %rax
	jl	.L2101
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movb	$1, 168(%r11)
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2102
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2102:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1953:
	movq	88(%r11), %rax
	cmpq	%rax, (%r12)
	jge	.L2074
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2075
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2075:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1954:
	movq	(%r12), %rbx
	cmpq	120(%r11), %rbx
	jl	.L2069
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movb	$1, 168(%r11)
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2070
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2070:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1955:
	movslq	%r15d, %r15
	movq	-152(%rbp), %rdi
	movq	(%r12), %rax
	movq	-8(%r14,%r15,8), %rbx
	movl	(%rdi), %ecx
	andl	$16777215, %ebx
	movslq	16(%r12,%rbx,8), %rdx
	cmpq	%rax, %rdx
	jge	.L1973
	testl	%ecx, %ecx
	jle	.L2840
.L2124:
	movq	%r15, 8(%r12)
	movq	%rax, 16(%r12,%rbx,8)
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1945:
	movslq	%r15d, %rax
	leaq	0(,%rax,8), %rdx
	movq	(%r14,%rax,8), %rax
	movq	24(%r14,%rdx), %rsi
	movq	16(%r14,%rdx), %rcx
	andl	$16777215, %eax
	leaq	(%r12,%rax,8), %rdx
	movl	%esi, %edi
	movq	16(%rdx), %rax
	addq	$1, %rax
	movq	%rax, 16(%rdx)
	cmpq	%rdi, %rax
	jnb	.L2841
	movslq	%ecx, %rcx
	cmpq	%rcx, %rax
	jl	.L2158
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	cmpl	$-1, %esi
	je	.L2395
.L2159:
	testl	%ecx, %ecx
	jle	.L2842
.L2160:
	leal	4(%r15), %eax
	cltq
	movq	%rax, 8(%r12)
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1946:
	movq	(%r12), %rax
	movq	120(%r11), %rdx
	cmpq	%rdx, %rax
	jl	.L2104
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movb	$1, 168(%r11)
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2105
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2105:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1947:
	movslq	%r13d, %r13
	movslq	%r15d, %r15
	addq	$4, %rbx
	movq	-152(%rbp), %rdi
	leaq	0(,%r13,8), %rax
	leaq	(%r12,%r15,8), %rdx
	movq	$0, 16(%rdx)
	movl	(%rdi), %ecx
	movq	%rbx, 8(%r12)
	movq	8(%r14,%rax), %rsi
	movq	16(%r14,%rax), %rax
	movq	(%r14,%r13,8), %r13
	cmpl	$-1, %eax
	je	.L2843
	testl	%esi, %esi
	jne	.L1973
	testl	%eax, %eax
	jne	.L2182
.L2180:
	andl	$16777215, %r13d
	leaq	1(%r13), %rax
	movq	%rax, 8(%r12)
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1942:
	movq	176(%r11), %rdx
	movq	192(%r11), %rax
	movslq	%r15d, %r15
	movslq	8(%rdx), %rdx
	movq	%rdx, (%rax,%r15,8)
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1943:
	movq	(%r12), %rax
	cmpq	80(%r11), %rax
	jle	.L2844
	movq	-160(%rbp), %rdi
	cmpw	$10, -2(%rdi,%rax,2)
	je	.L2845
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	js	.L2846
.L2033:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1941:
	movq	192(%r11), %rax
	movslq	%r15d, %r15
	movslq	56(%r11), %r8
	movq	176(%r11), %rdi
	movq	(%rax,%r15,8), %rsi
	movq	%r8, %rdx
	movq	24(%rdi), %rcx
	movslq	%esi, %rax
	subq	%r8, %rax
	salq	$3, %rax
	leaq	(%rcx,%rax), %rbx
	cmpq	%rbx, %r12
	jne	.L2219
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1912:
	movq	-152(%rbp), %rdi
	movl	%eax, (%rdi)
	jmp	.L1906
.L1999:
	movq	%r11, %r15
	movq	%rax, %xmm2
	movq	%r12, %r11
	movq	-216(%rbp), %xmm0
	movq	144(%r15), %rdx
	movb	$1, 130(%r15)
	punpcklqdq	%xmm2, %xmm0
	movq	%rdx, 152(%r15)
	movups	%xmm0, 136(%r15)
	jmp	.L2394
.L2806:
	je	.L2847
	movq	-152(%rbp), %rax
	movl	$257, %r9d
	movw	%r9w, 168(%r11)
	movl	(%rax), %ecx
	jmp	.L1973
.L2844:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2104:
	movq	-160(%rbp), %rdi
	leaq	1(%rax), %rsi
	leaq	(%rax,%rax), %r8
	movq	%rsi, (%r12)
	movzwl	(%rdi,%rax,2), %ecx
	movl	%ecx, %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	jne	.L2106
	cmpq	%rsi, %rdx
	jne	.L2848
.L2106:
	cmpl	$10, %ecx
	je	.L2107
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2841:
	cmpl	$-1, %esi
	je	.L2157
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2001:
	cmpq	%rdx, %rax
	jl	.L2003
	movl	$257, %eax
	movw	%ax, 168(%r11)
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2834:
	movq	-160(%rbp), %rdi
	leaq	1(%rax), %rsi
	leaq	(%rax,%rax), %r8
	movq	%rsi, (%r12)
	movzwl	(%rdi,%rax,2), %ecx
	movl	%ecx, %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	jne	.L1975
	cmpq	%rsi, %rdx
	jne	.L2849
.L1975:
	cmpl	%ecx, %r15d
	jne	.L1976
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2042:
	movq	-160(%rbp), %rdi
	leaq	1(%rax), %rcx
	leaq	(%rax,%rax), %r8
	movq	%rcx, (%r12)
	movzwl	(%rdi,%rax,2), %edi
	movl	%edi, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L2044
	cmpq	%rcx, %rdx
	jne	.L2850
.L2044:
	movq	%r11, -168(%rbp)
	call	u_charType_67@PLT
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	cmpb	$9, %al
	sete	%dl
	testl	%r15d, %r15d
	setne	%al
	cmpb	%al, %dl
	je	.L2045
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2083:
	movq	-160(%rbp), %rdi
	leaq	1(%rax), %rcx
	leaq	(%rax,%rax), %r8
	movq	%rcx, (%r12)
	movzwl	(%rdi,%rax,2), %esi
	movl	%esi, %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	jne	.L2085
	cmpq	%rcx, %rdx
	jne	.L2851
.L2085:
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	cmpl	$255, %esi
	jg	.L2086
	movl	%esi, %edx
	movslq	%r15d, %r15
	andl	$7, %esi
	sarl	$3, %edx
	salq	$5, %r15
	addq	%r15, %rax
	movslq	%edx, %rdx
	movsbl	2608(%rdx,%rax), %eax
	btl	%esi, %eax
	jc	.L2087
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2802:
	cmpl	$-1, %esi
	je	.L2198
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2074:
	movq	-152(%rbp), %rax
	movl	$257, %edx
	movw	%dx, 168(%r11)
	movl	(%rax), %ecx
	jmp	.L1973
.L2101:
	movq	-160(%rbp), %rdi
	leaq	1(%rax), %rsi
	leaq	(%rax,%rax), %r8
	movq	%rsi, (%r12)
	movzwl	(%rdi,%rax,2), %edi
	movl	%edi, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L2103
	cmpq	%rsi, %rdx
	jne	.L2852
.L2103:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	cmpl	$13, %edi
	jne	.L1973
	cmpq	%rsi, %rdx
	jle	.L1973
	movq	-160(%rbp), %rax
	cmpw	$10, (%rax,%rsi,2)
	jne	.L1973
	addq	$1, %rsi
	movq	%rsi, (%r12)
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L2076:
	movq	-160(%rbp), %rsi
	movl	%edx, %r13d
	leaq	1(%rax), %rdi
	leaq	(%rax,%rax), %r9
	andl	$8388608, %r13d
	movq	%rdi, (%r12)
	movzwl	(%rsi,%rax,2), %esi
	setne	%bl
	andl	$8388607, %edx
	movl	%esi, %r8d
	andl	$-1024, %r8d
	cmpl	$55296, %r8d
	jne	.L2078
	cmpq	%rdi, %rcx
	jne	.L2853
.L2078:
	movq	_ZN6icu_6715RegexStaticSets11gStaticSetsE(%rip), %rax
	cmpl	$255, %esi
	jg	.L2079
	movl	%esi, %ecx
	movslq	%edx, %rdx
	andl	$7, %esi
	sarl	$3, %ecx
	salq	$5, %rdx
	addq	%rdx, %rax
	movslq	%ecx, %rcx
	movsbl	2608(%rcx,%rax), %eax
	btl	%esi, %eax
	jc	.L2797
.L2080:
	testb	%bl, %bl
	je	.L2081
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2062:
	movq	-160(%rbp), %rdi
	leaq	1(%rax), %rsi
	leaq	(%rax,%rax), %r8
	movq	%rsi, (%r12)
	movzwl	(%rdi,%rax,2), %edx
	movl	%edx, %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	jne	.L2064
	cmpq	%rsi, %rcx
	jne	.L2854
.L2064:
	xorl	%eax, %eax
	testl	$-8368, %edx
	jne	.L2065
	leal	-10(%rdx), %eax
	cmpl	$3, %eax
	jbe	.L2433
	cmpl	$133, %edx
	je	.L2433
	subl	$8232, %edx
	cmpl	$1, %edx
	setbe	%al
.L2065:
	testl	%r15d, %r15d
	setne	%dl
	cmpb	%al, %dl
	je	.L2067
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2824:
	movq	-152(%rbp), %rax
	movl	$257, %r8d
	movw	%r8w, 168(%r11)
	movl	(%rax), %ecx
	jmp	.L1973
.L2050:
	movq	-160(%rbp), %rbx
	leaq	1(%rax), %rcx
	leaq	(%rax,%rax), %rdi
	movq	%rcx, (%r12)
	movzwl	(%rbx,%rax,2), %ebx
	movl	%ebx, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L2052
	cmpq	%rcx, %rdx
	jne	.L2855
.L2052:
	movl	%ebx, %edi
	movq	%r11, -168(%rbp)
	call	u_charType_67@PLT
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	cmpb	$12, %al
	sete	%al
	cmpl	$9, %ebx
	sete	%dl
	orl	%edx, %eax
	testl	%r15d, %r15d
	setne	%dl
	cmpb	%dl, %al
	je	.L2053
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2829:
	movq	-152(%rbp), %rax
	movslq	%r15d, %r15
	movq	%r15, 8(%r12)
	movl	(%rax), %ecx
	jmp	.L1973
.L2055:
	movq	-160(%rbp), %rcx
	leaq	1(%rdx), %rax
	leaq	(%rdx,%rdx), %rdi
	movq	%rax, (%r12)
	movzwl	(%rcx,%rdx,2), %esi
	movl	%esi, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L2057
	cmpq	%rax, %r9
	jne	.L2856
.L2057:
	testl	$-8368, %esi
	jne	.L2058
	leal	-10(%rsi), %edx
	cmpl	$3, %edx
	jbe	.L2059
	cmpl	$133, %esi
	je	.L2059
	leal	-8232(%rsi), %edx
	cmpl	$1, %edx
	ja	.L2058
.L2059:
	movq	-152(%rbp), %rdi
	movl	(%rdi), %ecx
	cmpl	$13, %esi
	jne	.L1973
	cmpq	%rax, %r9
	jle	.L1973
	movq	-160(%rbp), %rdi
	leaq	1(%rax), %rdx
	leaq	(%rax,%rax), %r8
	movq	%rdx, (%r12)
	movzwl	(%rdi,%rax,2), %esi
	movl	%esi, %edi
	andl	$64512, %edi
	cmpq	%rdx, %r9
	je	.L2060
	cmpl	$55296, %edi
	je	.L2857
.L2060:
	cmpw	$10, %si
	je	.L1973
	movq	%rax, (%r12)
	testq	%rax, %rax
	jle	.L1973
	cmpl	$56320, %edi
	jne	.L1973
	movq	-160(%rbp), %rax
	movzwl	-2(%rax,%r8), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L1973
	subq	$2, %rdx
	movq	%rdx, (%r12)
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L2813:
	movq	-152(%rbp), %rax
	movl	$257, %edi
	movw	%di, 168(%r11)
	movl	(%rax), %ecx
	jmp	.L1973
.L2089:
	movq	-160(%rbp), %rbx
	leaq	1(%rax), %rcx
	leaq	(%rax,%rax), %rdi
	movq	%rcx, (%r12)
	movzwl	(%rbx,%rax,2), %r13d
	movl	%r13d, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L2091
	cmpq	%rcx, %rdx
	jne	.L2858
.L2091:
	cmpl	$255, %r13d
	jg	.L2092
	movq	8(%r11), %rax
	movl	%r13d, %edx
	movslq	%r15d, %r15
	andl	$7, %r13d
	sarl	$3, %edx
	salq	$5, %r15
	movslq	%edx, %rdx
	addq	112(%rax), %r15
	movsbl	(%r15,%rdx), %eax
	btl	%r13d, %eax
	jnc	.L2093
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2095:
	movq	-160(%rbp), %rdi
	leaq	1(%rax), %rsi
	leaq	(%rax,%rax), %r8
	movq	%rsi, (%r12)
	movzwl	(%rdi,%rax,2), %edx
	movl	%edx, %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	jne	.L2097
	cmpq	%rsi, %rcx
	jne	.L2859
.L2097:
	movl	%edx, %esi
	andl	$-8368, %esi
	jne	.L2860
	leal	-10(%rdx), %eax
	cmpl	$3, %eax
	jbe	.L2099
	cmpl	$133, %edx
	je	.L2099
	subl	$8232, %edx
	cmpl	$1, %edx
	jbe	.L2099
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2827:
	movq	-160(%rbp), %rdi
	leaq	1(%rax), %rcx
	leaq	(%rax,%rax), %r8
	movq	%rcx, (%r12)
	movzwl	(%rdi,%rax,2), %edi
	movl	%edi, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L2257
	cmpq	%rcx, %rdx
	jne	.L2861
.L2257:
	xorl	%esi, %esi
	movq	%r11, -168(%rbp)
	call	u_foldCase_67@PLT
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	cmpl	%eax, %r15d
	jne	.L2258
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2047:
	cmpq	%rax, 112(%r11)
	jne	.L2048
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2219:
	testl	%r8d, %r8d
	jle	.L2224
	leaq	16(%rcx,%rax), %rax
	leal	-1(%r8), %r8d
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%cl
	cmpq	%rax, %rbx
	setnb	%al
	orb	%al, %cl
	je	.L2221
	cmpl	$3, %r8d
	jbe	.L2221
	movl	%edx, %ecx
	xorl	%eax, %eax
	shrl	%ecx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2223:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L2223
	movl	%edx, %eax
	andl	$-2, %eax
	andl	$1, %edx
	je	.L2224
	movq	(%r12,%rax,8), %rdx
	movq	%rdx, (%rbx,%rax,8)
.L2224:
	movq	%r11, -168(%rbp)
	movq	%rbx, %r12
	call	_ZN6icu_679UVector647setSizeEi@PLT
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	movl	(%rax), %ecx
	jmp	.L1973
.L2038:
	movq	320(%r11), %rdi
	movq	%r11, -168(%rbp)
	movl	%ebx, %esi
	movq	(%rdi), %rax
	call	*136(%rax)
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	jmp	.L2039
.L2836:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L1972
.L2822:
	movq	(%r12), %rax
	movslq	%edi, %rdi
	subq	%rdi, %rax
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	jle	.L2268
	cmpq	48(%r11), %rax
	jge	.L2272
	movq	-160(%rbp), %rdi
	leaq	(%rax,%rax), %r8
	movzwl	(%rdi,%rax,2), %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L2272
	movq	-160(%rbp), %rdi
	movzwl	-2(%rdi,%r8), %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	jne	.L2272
	subq	$1, %rax
	movq	%rax, 0(%r13)
	.p2align 4,,10
	.p2align 3
.L2272:
	movq	(%r12), %rdi
	movslq	%edx, %rdx
	subq	%rdx, %rdi
	cmpq	%rax, %rdi
	jg	.L2271
	movq	-152(%rbp), %rdi
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L2862
.L2276:
	movq	%rax, (%r12)
	jmp	.L1973
.L2821:
	movq	(%r12), %rax
	movslq	%r8d, %r8
	subq	%r8, %rax
	movq	%rax, (%rbx)
	testq	%rax, %rax
	jle	.L2294
	cmpq	48(%r11), %rax
	jge	.L2298
	movq	-160(%rbp), %r8
	leaq	(%rax,%rax), %r9
	movzwl	(%r8,%rax,2), %r8d
	andl	$-1024, %r8d
	cmpl	$56320, %r8d
	jne	.L2298
	movq	-160(%rbp), %r8
	movzwl	-2(%r8,%r9), %r8d
	andl	$-1024, %r8d
	cmpl	$55296, %r8d
	jne	.L2298
	subq	$1, %rax
	movq	%rax, (%rbx)
	.p2align 4,,10
	.p2align 3
.L2298:
	movq	(%r12), %r8
	movslq	%r13d, %r13
	subq	%r13, %r8
	cmpq	%rax, %r8
	jg	.L2297
	testl	%ecx, %ecx
	jle	.L2863
.L2301:
	movq	%rax, (%r12)
	jmp	.L1973
.L2805:
	movq	176(%r11), %rdi
	movl	56(%r11), %ebx
	movslq	8(%rdi), %rax
	movl	%ebx, %esi
	addl	%eax, %esi
	js	.L2110
	cmpl	12(%rdi), %esi
	jle	.L2111
.L2110:
	movq	-152(%rbp), %rdx
	movq	%r11, -184(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-168(%rbp), %rdi
	movq	-184(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2864
	movq	-152(%rbp), %rcx
	movslq	8(%rdi), %rax
	movl	(%rcx), %ecx
	leal	(%rbx,%rax), %esi
.L2111:
	movq	24(%rdi), %rdx
	movl	%esi, 8(%rdi)
	leaq	(%rdx,%rax,8), %rbx
.L2113:
	testl	%ecx, %ecx
	jg	.L2865
	movslq	56(%r11), %rax
	movq	%rbx, %r12
	leaq	16(%rbx), %rdx
	movl	$16, %edi
	salq	$3, %rax
	subq	%rax, %r12
	leaq	-8(%rax), %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %r12
	setnb	%dl
	subq	%rax, %rdi
	testq	%rdi, %rdi
	setle	%al
	orb	%al, %dl
	je	.L2449
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L2449
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L2116:
	movdqu	(%r12,%rax), %xmm5
	movups	%xmm5, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L2116
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%rsi, %rdi
	je	.L2118
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L2118:
	movl	272(%r11), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L2119
	movl	%eax, 272(%r11)
.L2120:
	movq	%r13, 8(%r12)
	movq	%rbx, %r12
	jmp	.L2109
.L2835:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L2000
.L2228:
	movq	24(%rax), %rdi
	movq	(%r12), %rax
	cmpq	%rdi, %rcx
	jge	.L2457
	movq	%rdi, %rdx
	movq	-160(%rbp), %r9
	movq	120(%r11), %rsi
	subq	%rcx, %rdx
	subq	%rax, %rcx
	addq	%rax, %rdx
	leaq	(%r9,%rcx,2), %rcx
	movq	%rdx, %r8
	jmp	.L2234
	.p2align 4,,10
	.p2align 3
.L2231:
	movzwl	(%r9,%rax,2), %ebx
	cmpw	%bx, (%rcx,%rax,2)
	jne	.L2232
	addq	$1, %rax
	cmpq	%rdx, %rax
	je	.L2866
.L2234:
	cmpq	%rax, %rsi
	jg	.L2231
	movb	$1, 168(%r11)
.L2232:
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2235
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2235:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2838:
	movq	(%r12), %rdx
	leal	1(%r15), %eax
	cltq
	movq	%rdx, 16(%r12,%rax,8)
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2839:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2843:
	movq	(%r12), %rax
	movq	%rax, 24(%rdx)
	testl	%esi, %esi
	jne	.L1973
.L2182:
	testl	%ecx, %ecx
	jg	.L2180
	movq	176(%r11), %r15
	movl	56(%r11), %r8d
	movslq	8(%r15), %rax
	movl	%r8d, %esi
	addl	%eax, %esi
	js	.L2183
	cmpl	12(%r15), %esi
	jle	.L2184
.L2183:
	movq	-152(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r11, -184(%rbp)
	movl	%r8d, -168(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movl	-168(%rbp), %r8d
	movq	-184(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2867
	movq	-152(%rbp), %rdi
	movslq	8(%r15), %rax
	movl	(%rdi), %ecx
	leal	(%r8,%rax), %esi
.L2184:
	movq	24(%r15), %rdx
	movl	%esi, 8(%r15)
	leaq	(%rdx,%rax,8), %r8
.L2186:
	testl	%ecx, %ecx
	jg	.L2868
	movslq	56(%r11), %rax
	movq	%r8, %r12
	leaq	16(%r8), %rdx
	movl	$16, %edi
	salq	$3, %rax
	subq	%rax, %r12
	leaq	-8(%rax), %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %r12
	setnb	%dl
	subq	%rax, %rdi
	testq	%rdi, %rdi
	setle	%al
	orb	%al, %dl
	je	.L2454
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L2454
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
.L2189:
	movdqu	(%r12,%rax), %xmm2
	movups	%xmm2, (%r8,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2189
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%r8, %rdx
	cmpq	%rsi, %rdi
	je	.L2191
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L2191:
	movl	272(%r11), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L2192
	movl	%eax, 272(%r11)
.L2193:
	movq	%rbx, 8(%r12)
	movq	%r8, %r12
	jmp	.L2180
.L2291:
	movq	192(%r11), %rdx
	movslq	%r15d, %rax
	addq	$2, %rax
	movq	(%rdx,%rax,8), %rcx
	movq	%rcx, 112(%r11)
	movq	8(%rdx,%rax,8), %rax
	movq	%rax, 120(%r11)
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2034:
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	js	.L2869
.L2035:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2823:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2069:
	movq	328(%r11), %rdi
	testq	%rdi, %rdi
	je	.L2870
.L2071:
	movq	(%rdi), %rax
	movq	%r11, -168(%rbp)
	movl	%ebx, %esi
	call	*120(%rax)
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	movl	%eax, %r8d
	cltq
	cmpl	$-1, %r8d
	cmove	%rbx, %rax
.L2072:
	movq	120(%r11), %rdx
	movq	%rax, (%r12)
	cmpq	%rax, %rdx
	jg	.L2871
	movq	-152(%rbp), %rax
	movb	$1, 168(%r11)
	movq	%rdx, (%r12)
	movl	(%rax), %ecx
	jmp	.L1973
.L2828:
	movslq	56(%r11), %r8
	movslq	%esi, %rax
	movq	24(%rdi), %rcx
	subq	%r8, %rax
	movq	%r8, %rdx
	salq	$3, %rax
	leaq	(%rcx,%rax), %r13
	testl	%r8d, %r8d
	jle	.L2252
	leaq	16(%rcx,%rax), %rax
	leal	-1(%r8), %r8d
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%cl
	cmpq	%rax, %r13
	setnb	%al
	orb	%al, %cl
	je	.L2249
	cmpl	$3, %r8d
	jbe	.L2249
	movl	%edx, %ecx
	xorl	%eax, %eax
	shrl	%ecx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2251:
	movdqu	(%r12,%rax), %xmm3
	movups	%xmm3, 0(%r13,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L2251
	movl	%edx, %eax
	andl	$-2, %eax
	andl	$1, %edx
	je	.L2252
	movq	(%r12,%rax,8), %rdx
	movq	%rdx, 0(%r13,%rax,8)
.L2252:
	movq	%r11, -168(%rbp)
	movq	%r13, %r12
	call	_ZN6icu_679UVector647setSizeEi@PLT
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	movq	192(%r11), %rax
	jmp	.L2247
.L2348:
	movslq	%edi, %rdx
	movslq	%edi, %rsi
	cmpq	%r15, %rdx
	jge	.L2350
	movl	%ecx, %edi
	movq	%r12, -192(%rbp)
	movq	-160(%rbp), %rcx
	movq	%rax, -224(%rbp)
	andl	$2, %edi
	movq	%r11, -232(%rbp)
	movl	%edi, %r11d
	.p2align 4,,10
	.p2align 3
.L2351:
	movslq	%esi, %rax
	leal	1(%rsi), %edx
	leaq	(%rax,%rax), %r8
	movzwl	(%rcx,%rax,2), %eax
	movl	%eax, %edi
	movl	%eax, %r9d
	andl	$64512, %edi
	cmpl	$55296, %edi
	je	.L2872
.L2352:
	movl	%eax, %r9d
	andl	$127, %r9d
	cmpl	$41, %r9d
	jle	.L2403
.L2478:
	movslq	%edx, %rsi
	movslq	%edx, %rdx
	cmpq	%r15, %rdx
	jl	.L2351
	movq	-192(%rbp), %r12
	movq	-224(%rbp), %rax
	movq	-232(%rbp), %r11
	jmp	.L2350
	.p2align 4,,10
	.p2align 3
.L2079:
	movslq	%edx, %rdx
	movq	%r11, -168(%rbp)
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rdx
	leaq	8(%rax,%rdx,8), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2080
.L2797:
	testl	%r13d, %r13d
	sete	%bl
	jmp	.L2080
.L2087:
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	js	.L2873
.L2088:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2093:
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	js	.L2874
.L2094:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2353:
	andl	$127, %r9d
	cmpw	$41, %r9w
	ja	.L2875
	.p2align 4,,10
	.p2align 3
.L2403:
	cmpl	$10, %eax
	je	.L2355
	movl	%eax, %r9d
	andl	$-8368, %r9d
	orl	%r11d, %r9d
	jne	.L2478
	leal	-10(%rax), %r9d
	cmpl	$3, %r9d
	jbe	.L2355
	cmpl	$133, %eax
	je	.L2355
	subl	$8232, %eax
	cmpl	$1, %eax
	ja	.L2478
.L2355:
	movq	-192(%rbp), %r12
	movq	-224(%rbp), %rax
	movq	-232(%rbp), %r11
	testl	%esi, %esi
	jle	.L2485
	cmpl	$56320, %edi
	je	.L2356
.L2485:
	movslq	%esi, %rdx
	jmp	.L2349
	.p2align 4,,10
	.p2align 3
.L2872:
	movslq	%edx, %r12
	cmpq	%r15, %r12
	je	.L2353
	leaq	2(%r8), %r9
	movq	%r9, -184(%rbp)
	movzwl	2(%rcx,%r8), %r9d
	movl	%r9d, %r12d
	andl	$-1024, %r12d
	cmpl	$56320, %r12d
	jne	.L2352
	leal	2(%rsi), %r12d
	sall	$10, %eax
	movslq	%edx, %rsi
	movl	$56320, %edi
	movq	-184(%rbp), %r8
	leal	-56613888(%r9,%rax), %eax
	movl	%r12d, %edx
	jmp	.L2352
	.p2align 4,,10
	.p2align 3
.L2406:
	movq	$0, -176(%rbp)
	jmp	.L1907
.L2832:
	movb	$1, 168(%r11)
.L1980:
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	jns	.L1982
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L1982:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2027:
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	jns	.L2030
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L2030:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2407:
	xorl	%r11d, %r11d
	jmp	.L1911
.L2260:
	movq	%r13, %rdi
	movq	-184(%rbp), %r14
	movq	-224(%rbp), %r12
	call	_ZN6icu_6724CaseFoldingUCharIterator11inExpansionEv@PLT
	movq	-168(%rbp), %r11
	testb	%al, %al
	jne	.L2264
	movq	%r13, %rdi
	call	_ZN6icu_6724CaseFoldingUCharIterator8getIndexEv@PLT
	movq	-168(%rbp), %r11
	movq	%rax, (%r12)
	jmp	.L2265
.L2801:
	xorl	%ebx, %ebx
	jmp	.L2400
.L2016:
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	js	.L2876
.L2020:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2819:
	addq	$1, %r13
	movq	%r13, 8(%r12)
	jmp	.L1973
.L2808:
	addq	$2, %rbx
	movq	%rbx, 8(%r12)
	jmp	.L1973
.L2037:
	testl	%r15d, %r15d
	jne	.L1959
.L2040:
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	js	.L2877
.L2041:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2058:
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	js	.L2878
.L2061:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2209:
	movq	-152(%rbp), %rax
	movq	%r11, %r15
	movq	%r12, %r11
	movl	$66321, (%rax)
	jmp	.L2799
.L2158:
	movl	272(%r11), %eax
	subl	$1, %eax
	movl	%eax, 272(%r11)
	testl	%eax, %eax
	jle	.L2175
.L2798:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L2160
.L2396:
	leal	4(%r15), %eax
	cltq
	movq	%rax, 8(%r12)
	movl	272(%r11), %eax
	subl	$1, %eax
	movl	%eax, 272(%r11)
	testl	%eax, %eax
	jle	.L2200
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2816:
	movq	-232(%rbp), %r12
	movq	-224(%rbp), %r14
	movq	-184(%rbp), %r11
	movq	(%r12), %rsi
	cmpl	$56320, %eax
	jne	.L2484
	testl	%ebx, %ebx
	jle	.L2484
	movq	-160(%rbp), %rax
	movslq	%ebx, %rdx
	leal	-2(%r15), %ebx
	movslq	%ebx, %rbx
	movzwl	-2(%rax,%r13), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	cmovne	%rdx, %rbx
	jmp	.L2321
.L2484:
	movslq	%ebx, %rbx
	jmp	.L2321
.L2154:
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	xorl	%ecx, %ecx
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	js	.L2155
	movslq	%edx, %rcx
	movl	%edx, %r8d
.L2155:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%r8d, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2296:
	movq	-160(%rbp), %r8
	leaq	-1(%r9), %rax
	movq	%rax, (%rbx)
	leaq	(%rax,%rax), %r15
	movzwl	(%r8,%rax,2), %r8d
	andl	$-1024, %r8d
	cmpl	$56320, %r8d
	jne	.L2298
	testq	%rax, %rax
	je	.L2298
	movq	-160(%rbp), %r8
	movzwl	-2(%r8,%r15), %r8d
	andl	$-1024, %r8d
	cmpl	$55296, %r8d
	jne	.L2298
	leaq	-2(%r9), %rax
	movq	%rax, (%rbx)
	jmp	.L2298
	.p2align 4,,10
	.p2align 3
.L2270:
	movq	-160(%rbp), %rdi
	leaq	-1(%r8), %rax
	movq	%rax, 0(%r13)
	leaq	(%rax,%rax), %r9
	movzwl	(%rdi,%rax,2), %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L2272
	testq	%rax, %rax
	je	.L2272
	movq	-160(%rbp), %rdi
	movzwl	-2(%rdi,%r9), %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	jne	.L2272
	leaq	-2(%r8), %rax
	movq	%rax, 0(%r13)
	jmp	.L2272
	.p2align 4,,10
	.p2align 3
.L2809:
	movq	176(%r11), %r15
	movl	56(%r11), %ebx
	movslq	8(%r15), %rax
	movl	%ebx, %esi
	addl	%eax, %esi
	js	.L2361
	cmpl	12(%r15), %esi
	jle	.L2362
.L2361:
	movq	-152(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r11, -168(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2879
	movq	-152(%rbp), %rdi
	movslq	8(%r15), %rax
	movl	(%rdi), %ecx
	leal	(%rbx,%rax), %esi
.L2362:
	movq	24(%r15), %rdx
	movl	%esi, 8(%r15)
	leaq	(%rdx,%rax,8), %rbx
.L2364:
	testl	%ecx, %ecx
	jg	.L2880
	movslq	56(%r11), %rax
	movq	%rbx, %r12
	leaq	16(%rbx), %rdx
	salq	$3, %rax
	subq	%rax, %r12
	leaq	-8(%rax), %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %r12
	movl	$16, %edx
	setnb	%dil
	subq	%rax, %rdx
	testq	%rdx, %rdx
	setle	%al
	orb	%al, %dil
	je	.L2479
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L2479
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L2367:
	movdqu	(%r12,%rax), %xmm7
	movups	%xmm7, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2367
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%rsi, %rdi
	je	.L2369
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L2369:
	movl	272(%r11), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L2370
	movl	%eax, 272(%r11)
.L2371:
	movq	%r13, 8(%r12)
	movq	%rbx, %r12
	movq	8(%rbx), %r13
	jmp	.L2360
.L2820:
	movq	176(%r11), %r15
	movl	56(%r11), %ebx
	movslq	8(%r15), %rax
	movl	%ebx, %esi
	addl	%eax, %esi
	js	.L2334
	cmpl	12(%r15), %esi
	jle	.L2335
.L2334:
	movq	-152(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r11, -168(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2881
	movq	-152(%rbp), %rdi
	movslq	8(%r15), %rax
	movl	(%rdi), %ecx
	leal	(%rbx,%rax), %esi
.L2335:
	movq	24(%r15), %rdx
	movl	%esi, 8(%r15)
	leaq	(%rdx,%rax,8), %rbx
.L2337:
	testl	%ecx, %ecx
	jg	.L2882
	movslq	56(%r11), %rax
	movq	%rbx, %r12
	leaq	16(%rbx), %rdx
	salq	$3, %rax
	subq	%rax, %r12
	leaq	-8(%rax), %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %r12
	movl	$16, %edx
	setnb	%dil
	subq	%rax, %rdx
	testq	%rdx, %rdx
	setle	%al
	orb	%al, %dil
	je	.L2475
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L2475
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L2340:
	movdqu	(%r12,%rax), %xmm7
	movups	%xmm7, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2340
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%rsi, %rdi
	je	.L2342
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L2342:
	movl	272(%r11), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L2343
	movl	%eax, 272(%r11)
.L2344:
	movq	%r13, 8(%r12)
	movq	%rbx, %r12
	movq	8(%rbx), %r13
	jmp	.L2333
.L2081:
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	js	.L2883
.L2082:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2003:
	subq	$1, %rdx
	cmpq	%rdx, %rax
	je	.L2884
	cmpq	%rcx, %rax
	je	.L2885
.L2007:
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	jns	.L2011
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L2011:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2811:
	cmpq	%rdx, %rdi
	jge	.L2376
	movq	-160(%rbp), %rdi
	cmpw	$13, -2(%rdi,%r13)
	jne	.L2376
	cmpb	$52, -5(%r14,%rax)
	jne	.L2376
	leaq	-2(%rsi), %rax
	movq	%rdi, %rdx
	movzwl	(%rdx,%rax,2), %edx
	movq	%rax, (%r12)
	leaq	(%rax,%rax), %rdi
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L2376
	testq	%rax, %rax
	jle	.L2376
	movq	-160(%rbp), %rax
	movzwl	-2(%rax,%rdi), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L2376
	subq	$3, %rsi
	movq	%rsi, (%r12)
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2236:
	movq	-160(%rbp), %rbx
	movq	24(%rax), %rcx
	leaq	-96(%rbp), %r13
	leaq	-144(%rbp), %r15
	movq	%r15, %rdi
	movq	%r11, -168(%rbp)
	movq	%rbx, %rsi
	call	_ZN6icu_6724CaseFoldingUCharIteratorC1EPKDsll@PLT
	movq	(%r12), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	-168(%rbp), %r11
	movq	120(%r11), %rcx
	call	_ZN6icu_6724CaseFoldingUCharIteratorC1EPKDsll@PLT
	movq	-168(%rbp), %r11
	movq	%r12, -184(%rbp)
	movq	%r11, %rbx
	jmp	.L2242
	.p2align 4,,10
	.p2align 3
.L2888:
	movq	%r13, %rdi
	call	_ZN6icu_6724CaseFoldingUCharIterator4nextEv@PLT
	cmpl	$-1, %eax
	je	.L2886
	cmpl	%eax, %r12d
	jne	.L2887
.L2242:
	movq	%r15, %rdi
	call	_ZN6icu_6724CaseFoldingUCharIterator4nextEv@PLT
	movl	%eax, %r12d
	cmpl	$-1, %eax
	jne	.L2888
	movq	%r13, %rdi
	movq	%rbx, -168(%rbp)
	movq	-184(%rbp), %r12
	call	_ZN6icu_6724CaseFoldingUCharIterator11inExpansionEv@PLT
	movq	-168(%rbp), %r11
	testb	%al, %al
	jne	.L2240
	movq	%r13, %rdi
	movq	%r11, -168(%rbp)
	call	_ZN6icu_6724CaseFoldingUCharIterator8getIndexEv@PLT
	movq	-168(%rbp), %r11
	movq	%rax, (%r12)
.L2243:
	movq	%r13, %rdi
	movq	%r11, -168(%rbp)
	call	_ZN6icu_6724CaseFoldingUCharIteratorD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6724CaseFoldingUCharIteratorD1Ev@PLT
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	movl	(%rax), %ecx
	jmp	.L1973
.L2316:
	movq	192(%r11), %rdx
	movslq	%r15d, %rax
	movq	%r11, -168(%rbp)
	addq	$2, %rax
	leaq	0(,%rax,8), %rcx
	movq	(%rdx,%rax,8), %rax
	movq	%rax, 112(%r11)
	movq	8(%rdx,%rcx), %rax
	movq	%rax, 120(%r11)
	movq	-16(%rdx,%rcx), %rsi
	call	_ZN6icu_679UVector647setSizeEi@PLT
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	jns	.L2318
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L2318:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2837:
	movq	176(%r11), %rcx
	movl	56(%r11), %r13d
	movq	-8(%r14,%rdx), %rdi
	movslq	8(%rcx), %rdx
	movl	%r13d, %esi
	movq	%rdi, -168(%rbp)
	addl	%edx, %esi
	js	.L2140
	cmpl	12(%rcx), %esi
	jle	.L2141
.L2140:
	movq	-152(%rbp), %rdx
	movq	%rcx, %rdi
	movq	%r11, -224(%rbp)
	movl	%r8d, -192(%rbp)
	movq	%rcx, -184(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-184(%rbp), %rcx
	movl	-192(%rbp), %r8d
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	movq	-224(%rbp), %r11
	je	.L2889
	movq	-152(%rbp), %rax
	movslq	8(%rcx), %rdx
	movl	(%rax), %eax
	leal	0(%r13,%rdx), %esi
.L2141:
	movq	24(%rcx), %rdi
	movl	%esi, 8(%rcx)
	leaq	(%rdi,%rdx,8), %r13
.L2143:
	testl	%eax, %eax
	jg	.L2890
	movslq	56(%r11), %rax
	movq	%r13, %r12
	leaq	16(%r13), %rdx
	salq	$3, %rax
	subq	%rax, %r12
	leaq	-8(%rax), %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %r12
	movl	$16, %edx
	setnb	%cl
	subq	%rax, %rdx
	testq	%rdx, %rdx
	setle	%al
	orb	%al, %cl
	je	.L2451
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L2451
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
.L2146:
	movdqu	(%r12,%rax), %xmm5
	movups	%xmm5, 0(%r13,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2146
	movq	%rsi, %rcx
	andq	$-2, %rcx
	leaq	0(,%rcx,8), %rax
	leaq	(%r12,%rax), %rdx
	addq	%r13, %rax
	cmpq	%rsi, %rcx
	je	.L2148
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
.L2148:
	movl	272(%r11), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L2149
	movl	%eax, 272(%r11)
.L2150:
	movq	-168(%rbp), %rax
	andl	$16777215, %eax
	addq	$1, %rax
	movq	%rax, 8(%r12)
	movq	%r13, %r12
	jmp	.L2139
.L2807:
	movq	%r11, -168(%rbp)
	call	_ZN6icu_676Locale10getEnglishEv@PLT
	movq	-152(%rbp), %r13
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	0(%r13), %esi
	movq	%r13, %rdx
	movq	-168(%rbp), %r11
	movq	%rax, %rdi
	leaq	.L1916(%rip), %r10
	testl	%esi, %esi
	movq	%rax, 320(%r11)
	jg	.L2037
	movq	(%rax), %rax
	movq	32(%r11), %rsi
	call	*64(%rax)
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	jmp	.L2036
.L2871:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2875:
	movq	-192(%rbp), %r12
	movq	-224(%rbp), %rax
	movq	%r15, %rdx
	movq	-232(%rbp), %r11
.L2350:
	movb	$1, 168(%r11)
	jmp	.L2349
.L2869:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L2035
.L2878:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L2061
.L2877:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L2041
.L2876:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L2020
.L2815:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L2023
.L2846:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L2033
.L2862:
	movq	176(%r11), %r15
	movl	56(%r11), %r8d
	movq	8(%r12), %rbx
	movslq	8(%r15), %rax
	movl	%r8d, %esi
	addl	%eax, %esi
	js	.L2277
	cmpl	12(%r15), %esi
	jle	.L2278
.L2277:
	movq	-152(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r11, -184(%rbp)
	movl	%r8d, -168(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movl	-168(%rbp), %r8d
	movq	-184(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2891
	movq	-152(%rbp), %rdi
	movslq	8(%r15), %rax
	movl	(%rdi), %ecx
	leal	(%r8,%rax), %esi
.L2278:
	movq	24(%r15), %rdx
	movl	%esi, 8(%r15)
	leaq	(%rdx,%rax,8), %r8
.L2280:
	testl	%ecx, %ecx
	jg	.L2892
	movslq	56(%r11), %rax
	movq	%r8, %r12
	leaq	16(%r8), %rdx
	movl	$16, %edi
	salq	$3, %rax
	subq	%rax, %r12
	leaq	-8(%rax), %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %r12
	setnb	%dl
	subq	%rax, %rdi
	testq	%rdi, %rdi
	setle	%al
	orb	%al, %dl
	je	.L2467
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L2467
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
.L2283:
	movdqu	(%r12,%rax), %xmm3
	movups	%xmm3, (%r8,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2283
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%r8, %rdx
	cmpq	%rsi, %rdi
	je	.L2285
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L2285:
	movl	272(%r11), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L2286
	movl	%eax, 272(%r11)
.L2287:
	subq	$3, %rbx
	movq	%rbx, 8(%r12)
	movq	%r8, %r12
	movq	0(%r13), %rax
	jmp	.L2276
.L2863:
	movq	176(%r11), %r15
	movl	56(%r11), %r8d
	movq	8(%r12), %r13
	movslq	8(%r15), %rax
	movl	%r8d, %esi
	addl	%eax, %esi
	js	.L2302
	cmpl	12(%r15), %esi
	jle	.L2303
.L2302:
	movq	-152(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r11, -184(%rbp)
	movl	%r8d, -168(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movl	-168(%rbp), %r8d
	movq	-184(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2893
	movq	-152(%rbp), %rdi
	movslq	8(%r15), %rax
	movl	(%rdi), %ecx
	leal	(%r8,%rax), %esi
.L2303:
	movq	24(%r15), %rdx
	movl	%esi, 8(%r15)
	leaq	(%rdx,%rax,8), %r8
.L2305:
	testl	%ecx, %ecx
	jg	.L2894
	movslq	56(%r11), %rax
	movq	%r8, %r12
	leaq	16(%r8), %rdx
	movl	$16, %edi
	salq	$3, %rax
	subq	%rax, %r12
	leaq	-8(%rax), %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %r12
	setnb	%dl
	subq	%rax, %rdi
	testq	%rdi, %rdi
	setle	%al
	orb	%al, %dl
	je	.L2469
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L2469
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
.L2308:
	movdqu	(%r12,%rax), %xmm7
	movups	%xmm7, (%r8,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2308
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%r8, %rdx
	cmpq	%rsi, %rdi
	je	.L2310
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L2310:
	movl	272(%r11), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L2311
	movl	%eax, 272(%r11)
.L2312:
	subq	$4, %r13
	movq	%r13, 8(%r12)
	movq	%r8, %r12
	movq	(%rbx), %rax
	jmp	.L2301
.L2803:
	movq	176(%r11), %r13
	movl	56(%r11), %ebx
	movslq	8(%r13), %rax
	movl	%ebx, %esi
	addl	%eax, %esi
	js	.L2205
	cmpl	12(%r13), %esi
	jle	.L2206
.L2205:
	movq	-152(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r11, -168(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2895
	movq	-152(%rbp), %rdi
	movslq	8(%r13), %rax
	movl	(%rdi), %ecx
	leal	(%rbx,%rax), %esi
.L2206:
	movq	24(%r13), %rdx
	movl	%esi, 8(%r13)
	leaq	(%rdx,%rax,8), %rbx
.L2208:
	testl	%ecx, %ecx
	jg	.L2209
	movslq	56(%r11), %rax
	movq	%rbx, %r12
	leaq	16(%rbx), %rdx
	salq	$3, %rax
	subq	%rax, %r12
	leaq	-8(%rax), %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %r12
	movl	$16, %edx
	setnb	%dil
	subq	%rax, %rdx
	testq	%rdx, %rdx
	setle	%al
	orb	%al, %dil
	je	.L2455
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L2455
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
.L2211:
	movdqu	(%r12,%rax), %xmm2
	movups	%xmm2, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2211
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%rsi, %rdi
	je	.L2213
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L2213:
	movl	272(%r11), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L2214
	movl	%eax, 272(%r11)
.L2215:
	leal	4(%r15), %eax
	cltq
	movq	%rax, 8(%r12)
	movq	%rbx, %r12
	jmp	.L1973
.L2842:
	movq	176(%r11), %rdi
	movl	56(%r11), %ebx
	movslq	8(%rdi), %rax
	movl	%ebx, %esi
	addl	%eax, %esi
	js	.L2161
	cmpl	12(%rdi), %esi
	jle	.L2162
.L2161:
	movq	-152(%rbp), %rdx
	movq	%r11, -184(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-168(%rbp), %rdi
	movq	-184(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2896
	movq	-152(%rbp), %rcx
	movslq	8(%rdi), %rax
	movl	(%rcx), %ecx
	leal	(%rbx,%rax), %esi
.L2162:
	movq	24(%rdi), %rdx
	movl	%esi, 8(%rdi)
	leaq	(%rdx,%rax,8), %rbx
.L2164:
	testl	%ecx, %ecx
	jg	.L2897
	movslq	56(%r11), %rax
	movq	%rbx, %r12
	leaq	16(%rbx), %rdx
	movl	$16, %edi
	salq	$3, %rax
	subq	%rax, %r12
	leaq	-8(%rax), %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %r12
	setnb	%dl
	subq	%rax, %rdi
	testq	%rdi, %rdi
	setle	%al
	orb	%al, %dl
	je	.L2453
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L2453
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
.L2167:
	movdqu	(%r12,%rax), %xmm3
	movups	%xmm3, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2167
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%rsi, %rdi
	je	.L2169
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L2169:
	movl	272(%r11), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L2170
	movl	%eax, 272(%r11)
.L2171:
	movq	%r13, 8(%r12)
	movq	%rbx, %r12
	jmp	.L2160
.L2861:
	movq	-160(%rbp), %rcx
	movzwl	2(%rcx,%r8), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L2257
	addq	$2, %rax
	sall	$10, %edi
	movq	%rax, (%r12)
	leal	-56613888(%rdx,%rdi), %edi
	jmp	.L2257
.L2856:
	movq	-160(%rbp), %rcx
	movzwl	2(%rcx,%rdi), %ecx
	movl	%ecx, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L2057
	leaq	2(%rdx), %rax
	sall	$10, %esi
	movq	%rax, (%r12)
	leal	-56613888(%rcx,%rsi), %esi
	jmp	.L2057
.L2855:
	movq	-160(%rbp), %rcx
	movzwl	2(%rcx,%rdi), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L2052
	addq	$2, %rax
	sall	$10, %ebx
	movq	%rax, (%r12)
	leal	-56613888(%rdx,%rbx), %ebx
	jmp	.L2052
.L2854:
	movq	-160(%rbp), %rdi
	movzwl	2(%rdi,%r8), %ecx
	movl	%ecx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L2064
	addq	$2, %rax
	sall	$10, %edx
	movq	%rax, (%r12)
	leal	-56613888(%rcx,%rdx), %edx
	jmp	.L2064
.L2853:
	movq	-160(%rbp), %rdi
	movzwl	2(%rdi,%r9), %ecx
	movl	%ecx, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L2078
	addq	$2, %rax
	sall	$10, %esi
	movq	%rax, (%r12)
	leal	-56613888(%rcx,%rsi), %esi
	jmp	.L2078
.L2852:
	movq	-160(%rbp), %rcx
	movzwl	2(%rcx,%r8), %ecx
	movl	%ecx, %r8d
	andl	$-1024, %r8d
	cmpl	$56320, %r8d
	jne	.L2103
	leaq	2(%rax), %rsi
	sall	$10, %edi
	movq	%rsi, (%r12)
	leal	-56613888(%rcx,%rdi), %edi
	jmp	.L2103
.L2859:
	movq	-160(%rbp), %rdi
	movzwl	2(%rdi,%r8), %ecx
	movl	%ecx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L2097
	addq	$2, %rax
	sall	$10, %edx
	movq	%rax, (%r12)
	leal	-56613888(%rcx,%rdx), %edx
	jmp	.L2097
.L2858:
	movzwl	2(%rbx,%rdi), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L2091
	addq	$2, %rax
	sall	$10, %r13d
	movq	%rax, (%r12)
	leal	-56613888(%rdx,%r13), %r13d
	jmp	.L2091
.L2848:
	movq	-160(%rbp), %rdi
	movzwl	2(%rdi,%r8), %edx
	movl	%edx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L2106
	addq	$2, %rax
	sall	$10, %ecx
	movq	%rax, (%r12)
	leal	-56613888(%rdx,%rcx), %ecx
	jmp	.L2106
.L2851:
	movq	-160(%rbp), %rdi
	movzwl	2(%rdi,%r8), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L2085
	addq	$2, %rax
	sall	$10, %esi
	movq	%rax, (%r12)
	leal	-56613888(%rdx,%rsi), %esi
	jmp	.L2085
.L2850:
	movq	-160(%rbp), %rcx
	movzwl	2(%rcx,%r8), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L2044
	addq	$2, %rax
	sall	$10, %edi
	movq	%rax, (%r12)
	leal	-56613888(%rdx,%rdi), %edi
	jmp	.L2044
.L2849:
	movq	-160(%rbp), %rdi
	movzwl	2(%rdi,%r8), %edx
	movl	%edx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L1975
	addq	$2, %rax
	sall	$10, %ecx
	movq	%rax, (%r12)
	leal	-56613888(%rdx,%rcx), %ecx
	jmp	.L1975
.L2874:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L2094
.L2883:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L2082
.L2873:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L2088
.L2099:
	movq	176(%r11), %rdi
	movslq	56(%r11), %rax
	xorl	%ecx, %ecx
	movl	8(%rdi), %edx
	movq	24(%rdi), %r8
	subl	%eax, %edx
	js	.L2100
	movslq	%edx, %rcx
	movl	%edx, %esi
.L2100:
	subq	%rax, %rcx
	movq	-152(%rbp), %rax
	movl	%esi, 8(%rdi)
	leaq	(%r8,%rcx,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2433:
	movl	$1, %eax
	jmp	.L2065
.L2884:
	movq	-160(%rbp), %rdi
	leaq	(%rax,%rax), %rsi
	movzwl	(%rdi,%rax,2), %ecx
	movl	%ecx, %edi
	movl	%ecx, %edx
	andl	$63488, %edi
	cmpl	$55296, %edi
	je	.L2898
.L2005:
	testl	$57168, %ecx
	jne	.L2007
	leal	-10(%rcx), %edx
	cmpl	$3, %edx
	jbe	.L2009
	cmpl	$133, %ecx
	je	.L2009
	leal	-8232(%rcx), %edx
	cmpl	$1, %edx
	ja	.L2007
.L2009:
	cmpl	$10, %ecx
	je	.L2899
.L2010:
	movq	-152(%rbp), %rax
	movl	$257, %r15d
	movw	%r15w, 168(%r11)
	movl	(%rax), %ecx
	jmp	.L1973
.L2482:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2249:
	movl	%r8d, %edx
	xorl	%eax, %eax
.L2255:
	movq	(%r12,%rax,8), %rcx
	movq	%rcx, 0(%r13,%rax,8)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rcx, %rdx
	jne	.L2255
	jmp	.L2252
.L2449:
	movq	%rbx, %rdi
	movq	%r12, %rsi
.L2115:
	movsq
	cmpq	%rsi, %rbx
	jne	.L2115
	jmp	.L2118
.L2411:
	movq	%rbx, %rdi
	movq	%r12, %rsi
.L1990:
	movsq
	cmpq	%rsi, %rbx
	jne	.L1990
	jmp	.L1993
.L2840:
	movq	176(%r11), %rdi
	movl	56(%r11), %r8d
	movslq	8(%rdi), %rax
	movl	%r8d, %esi
	addl	%eax, %esi
	js	.L2125
	cmpl	12(%rdi), %esi
	jle	.L2126
.L2125:
	movq	-152(%rbp), %rdx
	movq	%r11, -192(%rbp)
	movl	%r8d, -184(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-168(%rbp), %rdi
	movl	-184(%rbp), %r8d
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	movq	-192(%rbp), %r11
	je	.L2900
	movq	-152(%rbp), %rcx
	movslq	8(%rdi), %rax
	movl	(%rcx), %ecx
	leal	(%r8,%rax), %esi
.L2126:
	movq	24(%rdi), %rdx
	movl	%esi, 8(%rdi)
	leaq	(%rdx,%rax,8), %r8
.L2128:
	testl	%ecx, %ecx
	jg	.L2901
	movslq	56(%r11), %rax
	movq	%r8, %r12
	leaq	16(%r8), %rdx
	salq	$3, %rax
	subq	%rax, %r12
	leaq	-8(%rax), %rdi
	shrq	$3, %rdi
	cmpq	%rdx, %r12
	movl	$16, %edx
	setnb	%sil
	subq	%rax, %rdx
	testq	%rdx, %rdx
	setle	%al
	orb	%al, %sil
	je	.L2450
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L2450
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rdx
	shrq	%rdx
	salq	$4, %rdx
.L2131:
	movdqu	(%r12,%rax), %xmm4
	movups	%xmm4, (%r8,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2131
	movq	%rdi, %rsi
	andq	$-2, %rsi
	leaq	0(,%rsi,8), %rax
	leaq	(%r12,%rax), %rdx
	addq	%r8, %rax
	cmpq	%rdi, %rsi
	je	.L2133
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
.L2133:
	movl	272(%r11), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L2134
	movl	%eax, 272(%r11)
.L2135:
	movq	%r13, 8(%r12)
	movq	(%r8), %rax
	movq	%r8, %r12
	jmp	.L2124
.L2866:
	movq	-160(%rbp), %rcx
	movzwl	-2(%rcx,%rdi,2), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L2902
.L2230:
	movq	-152(%rbp), %rax
	movq	%r8, (%r12)
	movl	(%rax), %ecx
	jmp	.L1973
.L2092:
	movq	-200(%rbp), %rdi
	movl	%r15d, %esi
	movq	%r11, -168(%rbp)
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2093
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2086:
	movslq	%r15d, %r15
	movq	%r11, -168(%rbp)
	leaq	(%r15,%r15,4), %rdx
	leaq	(%rdx,%rdx,4), %rdx
	leaq	8(%rax,%rdx,8), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	jne	.L2087
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2860:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2826:
	movb	$1, 168(%r11)
	movq	%r13, %rdi
	call	_ZN6icu_6724CaseFoldingUCharIterator11inExpansionEv@PLT
	movq	-168(%rbp), %r11
	jmp	.L2264
.L2045:
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2046
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2046:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2814:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2053:
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2054
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2054:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2107:
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2108
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2108:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2157:
	movslq	%ecx, %rcx
	cmpq	%rcx, %rax
	jl	.L2158
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
.L2395:
	movq	(%r12), %rax
	cmpq	24(%rdx), %rax
	je	.L1973
	movq	%rax, 24(%rdx)
	jmp	.L2159
.L2847:
	movq	-160(%rbp), %rax
	cmpw	$10, (%rax,%rdx,2)
	jne	.L2012
	movq	-152(%rbp), %rax
	movl	$257, %ebx
	movw	%bx, 168(%r11)
	movl	(%rax), %ecx
	jmp	.L1973
.L2067:
	movq	176(%r11), %rcx
	movslq	56(%r11), %rax
	movl	8(%rcx), %edx
	movq	24(%rcx), %rdi
	subl	%eax, %edx
	movslq	%edx, %rsi
	jns	.L2068
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2068:
	subq	%rax, %rsi
	movq	-152(%rbp), %rax
	movl	%edx, 8(%rcx)
	leaq	(%rdi,%rsi,8), %r12
	movl	(%rax), %ecx
	jmp	.L1973
.L2845:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2830:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2294:
	jne	.L2297
	jmp	.L2298
.L2268:
	jne	.L2271
	jmp	.L2272
.L2198:
	movslq	%ecx, %rcx
	cmpq	%rcx, %rax
	jl	.L2396
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
.L2397:
	movq	(%r12), %rax
	cmpq	24(%rdx), %rax
	je	.L1973
	movq	%rax, 24(%rdx)
	jmp	.L2204
.L2865:
	movq	-152(%rbp), %rax
	movl	$66321, %ecx
	movl	$66321, (%rax)
	jmp	.L2109
.L2328:
	movq	%r14, %rdx
	movq	-232(%rbp), %r12
	movq	-224(%rbp), %r14
	movzwl	(%rdx), %eax
	movq	-184(%rbp), %r11
	movq	(%r12), %rsi
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L2484
	testl	%ebx, %ebx
	jle	.L2484
	movq	-160(%rbp), %rax
	movslq	%ebx, %rbx
	movzwl	-2(%rax,%r13), %edx
	leal	-2(%r15), %eax
	cltq
	andl	$-1024, %edx
	cmpl	$55296, %edx
	cmove	%rax, %rbx
	jmp	.L2321
.L2886:
	movb	$1, 168(%rbx)
	movq	%rbx, %r11
.L2240:
	movq	176(%r11), %rsi
	movslq	56(%r11), %rax
	movl	8(%rsi), %edx
	movq	24(%rsi), %rdi
	subl	%eax, %edx
	movslq	%edx, %rcx
	jns	.L2244
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L2244:
	subq	%rax, %rcx
	movl	%edx, 8(%rsi)
	leaq	(%rdi,%rcx,8), %r12
	jmp	.L2243
.L2810:
	movq	-160(%rbp), %r9
	movzwl	-2(%r9,%r13), %r9d
	andl	$-1024, %r9d
	cmpl	$55296, %r9d
	jne	.L2375
	subq	$2, %rsi
	movq	%rsi, (%r12)
	jmp	.L2376
.L2882:
	movq	-152(%rbp), %rax
	movq	8(%r12), %r13
	movl	$66321, %ecx
	movl	$66321, (%rax)
	jmp	.L2333
.L2880:
	movq	-152(%rbp), %rax
	movq	8(%r12), %r13
	movl	$66321, %ecx
	movl	$66321, (%rax)
	jmp	.L2360
.L2221:
	movl	%r8d, %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2227:
	movq	(%r12,%rax,8), %rcx
	movq	%rcx, (%rbx,%rax,8)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rcx, %rdx
	jne	.L2227
	jmp	.L2224
.L2200:
	movl	268(%r11), %eax
	movl	$10000, 272(%r11)
	leal	1(%rax), %esi
	movq	280(%r11), %rax
	movl	%esi, 268(%r11)
	testq	%rax, %rax
	je	.L2201
	movq	%r11, -168(%rbp)
	movq	288(%r11), %rdi
	call	*%rax
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2903
.L2201:
	movl	264(%r11), %eax
	testl	%eax, %eax
	jle	.L2904
	cmpl	268(%r11), %eax
	jle	.L2203
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2890:
	movq	-152(%rbp), %rax
	movl	$66321, (%rax)
	jmp	.L2139
.L2175:
	movl	268(%r11), %eax
	movl	$10000, 272(%r11)
	leal	1(%rax), %esi
	movq	280(%r11), %rax
	movl	%esi, 268(%r11)
	testq	%rax, %rax
	je	.L2176
	movq	%r11, -168(%rbp)
	movq	288(%r11), %rdi
	call	*%rax
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2905
.L2176:
	movl	264(%r11), %eax
	testl	%eax, %eax
	jle	.L2798
	cmpl	268(%r11), %eax
	movq	-152(%rbp), %rax
	jle	.L2178
	movl	(%rax), %ecx
	jmp	.L2160
.L2901:
	movq	-152(%rbp), %rax
	movl	$66321, %ecx
	movl	$66321, (%rax)
	movq	(%r12), %rax
	jmp	.L2124
.L2887:
	movq	%rbx, %r11
	jmp	.L2240
.L2902:
	cmpq	%rax, %rsi
	jle	.L2230
	movzwl	(%rcx,%rax,2), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L2230
	jmp	.L2232
.L1994:
	movl	268(%r11), %eax
	movl	$10000, 272(%r11)
	leal	1(%rax), %esi
	movq	280(%r11), %rax
	movl	%esi, 268(%r11)
	testq	%rax, %rax
	je	.L1996
	movq	%r11, -168(%rbp)
	movq	288(%r11), %rdi
	call	*%rax
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2906
.L1996:
	movl	264(%r11), %eax
	testl	%eax, %eax
	jle	.L1998
	cmpl	268(%r11), %eax
	jg	.L1998
	movq	-152(%rbp), %rax
	movl	$66322, %ecx
	movl	$66322, (%rax)
	jmp	.L1995
.L2870:
	movq	%r11, -168(%rbp)
	call	_ZN6icu_676Locale10getEnglishEv@PLT
	movq	-152(%rbp), %r15
	movq	%rax, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713BreakIterator23createCharacterInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	(%r15), %ecx
	movq	%r15, %rdx
	movq	-168(%rbp), %r11
	movq	%rax, %rdi
	leaq	.L1916(%rip), %r10
	testl	%ecx, %ecx
	movq	%rax, 328(%r11)
	movq	%rbx, %rax
	jg	.L2072
	movq	(%rdi), %rax
	movq	32(%r11), %rsi
	call	*64(%rax)
	movq	-168(%rbp), %r11
	movq	328(%r11), %rdi
	jmp	.L2071
.L2885:
	movq	-160(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	cmpw	$13, (%rdi,%rax,2)
	jne	.L2007
	cmpw	$10, 2(%rdi,%rdx)
	jne	.L2007
	movq	-152(%rbp), %rax
	movl	$257, %r13d
	movw	%r13w, 168(%r11)
	movl	(%rax), %ecx
	jmp	.L1973
.L2119:
	movl	268(%r11), %eax
	movl	$10000, 272(%r11)
	leal	1(%rax), %esi
	movq	280(%r11), %rax
	movl	%esi, 268(%r11)
	testq	%rax, %rax
	je	.L2121
	movq	%r11, -168(%rbp)
	movq	288(%r11), %rdi
	call	*%rax
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2907
.L2121:
	movl	264(%r11), %eax
	testl	%eax, %eax
	jle	.L2123
	cmpl	268(%r11), %eax
	jg	.L2123
	movq	-152(%rbp), %rax
	movl	$66322, %ecx
	movl	$66322, (%rax)
	jmp	.L2120
.L2018:
	cmpq	80(%r11), %rax
	jg	.L2019
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2831:
	movq	-152(%rbp), %rax
	xorl	%ebx, %ebx
	movl	(%rax), %ecx
	jmp	.L1987
.L2864:
	movq	-152(%rbp), %rax
	xorl	%ebx, %ebx
	movl	(%rax), %ecx
	jmp	.L2113
.L2894:
	movq	-152(%rbp), %rax
	movl	$66321, %ecx
	movl	$66321, (%rax)
	movq	(%rbx), %rax
	jmp	.L2301
.L2892:
	movq	-152(%rbp), %rax
	movl	$66321, %ecx
	movl	$66321, (%rax)
	movq	0(%r13), %rax
	jmp	.L2276
.L2897:
	movq	-152(%rbp), %rax
	movl	$66321, %ecx
	movl	$66321, (%rax)
	jmp	.L2160
.L2868:
	movq	-152(%rbp), %rax
	movl	$66321, %ecx
	movl	$66321, (%rax)
	jmp	.L2180
.L2387:
	movl	268(%r11), %eax
	movl	$10000, 272(%r11)
	leal	1(%rax), %esi
	movq	280(%r11), %rax
	movl	%esi, 268(%r11)
	testq	%rax, %rax
	je	.L2389
	movq	%r11, -168(%rbp)
	movq	288(%r11), %rdi
	call	*%rax
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2908
.L2389:
	movl	264(%r11), %eax
	testl	%eax, %eax
	jle	.L2391
	cmpl	268(%r11), %eax
	jg	.L2391
	movq	-152(%rbp), %rax
	movl	$66322, %ecx
	movl	$66322, (%rax)
	jmp	.L2388
.L2343:
	movl	268(%r11), %eax
	movl	$10000, 272(%r11)
	leal	1(%rax), %esi
	movq	280(%r11), %rax
	movl	%esi, 268(%r11)
	testq	%rax, %rax
	je	.L2345
	movq	%r11, -168(%rbp)
	movq	288(%r11), %rdi
	call	*%rax
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2909
.L2345:
	movl	264(%r11), %eax
	testl	%eax, %eax
	jle	.L2347
	cmpl	268(%r11), %eax
	jg	.L2347
	movq	-152(%rbp), %rax
	movl	$66322, %ecx
	movl	$66322, (%rax)
	jmp	.L2344
.L2370:
	movl	268(%r11), %eax
	movl	$10000, 272(%r11)
	leal	1(%rax), %esi
	movq	280(%r11), %rax
	movl	%esi, 268(%r11)
	testq	%rax, %rax
	je	.L2372
	movq	%r11, -168(%rbp)
	movq	288(%r11), %rdi
	call	*%rax
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2910
.L2372:
	movl	264(%r11), %eax
	testl	%eax, %eax
	jle	.L2374
	cmpl	268(%r11), %eax
	jg	.L2374
	movq	-152(%rbp), %rax
	movl	$66322, %ecx
	movl	$66322, (%rax)
	jmp	.L2371
.L2812:
	movq	-152(%rbp), %rax
	xorl	%r13d, %r13d
	movl	(%rax), %ecx
	jmp	.L2382
.L1998:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1995
.L2879:
	movq	-152(%rbp), %rax
	xorl	%ebx, %ebx
	movl	(%rax), %ecx
	jmp	.L2364
.L2123:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L2120
.L2881:
	movq	-152(%rbp), %rax
	xorl	%ebx, %ebx
	movl	(%rax), %ecx
	jmp	.L2337
.L2149:
	movl	268(%r11), %eax
	movl	$10000, 272(%r11)
	leal	1(%rax), %esi
	movq	280(%r11), %rax
	movl	%esi, 268(%r11)
	testq	%rax, %rax
	je	.L2151
	movl	%r8d, -192(%rbp)
	movq	288(%r11), %rdi
	movq	%r11, -184(%rbp)
	call	*%rax
	movq	-184(%rbp), %r11
	movl	-192(%rbp), %r8d
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2911
.L2151:
	movl	264(%r11), %eax
	testl	%eax, %eax
	jle	.L2150
	cmpl	268(%r11), %eax
	jg	.L2150
	movq	-152(%rbp), %rax
	movl	$66322, (%rax)
	jmp	.L2150
.L2898:
	andb	$4, %dh
	je	.L2005
	cmpq	80(%r11), %rax
	jle	.L2005
	movq	-160(%rbp), %rdi
	movzwl	-2(%rdi,%rsi), %edx
	movl	%edx, %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	jne	.L2005
	movl	%edx, %eax
	sall	$10, %eax
	addl	%ecx, %eax
	leal	-56613888(%rax), %edx
	andl	$-8368, %edx
	jne	.L2007
	subl	$56622120, %eax
	cmpl	$1, %eax
	ja	.L2007
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2900:
	movq	-152(%rbp), %rax
	xorl	%r8d, %r8d
	movl	(%rax), %ecx
	jmp	.L2128
.L2019:
	movq	-160(%rbp), %rax
	cmpw	$13, -2(%rax,%rcx)
	je	.L2016
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2889:
	movq	-152(%rbp), %rax
	xorl	%r13d, %r13d
	movl	(%rax), %eax
	jmp	.L2143
.L2134:
	movl	268(%r11), %eax
	movl	$10000, 272(%r11)
	leal	1(%rax), %esi
	movq	280(%r11), %rax
	movl	%esi, 268(%r11)
	testq	%rax, %rax
	je	.L2136
	movq	%r8, -184(%rbp)
	movq	288(%r11), %rdi
	movq	%r11, -168(%rbp)
	call	*%rax
	movq	-168(%rbp), %r11
	movq	-184(%rbp), %r8
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2912
.L2136:
	movl	264(%r11), %eax
	testl	%eax, %eax
	jle	.L2138
	cmpl	268(%r11), %eax
	jg	.L2138
	movq	-152(%rbp), %rax
	movl	$66322, %ecx
	movl	$66322, (%rax)
	jmp	.L2135
.L2475:
	movq	%rbx, %rdi
	movq	%r12, %rsi
.L2339:
	movsq
	cmpq	%rbx, %rsi
	jne	.L2339
	jmp	.L2342
.L2904:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L1973
.L2480:
	movq	%r13, %rdi
	movq	%r12, %rsi
.L2383:
	movsq
	cmpq	%r13, %rsi
	jne	.L2383
	jmp	.L2386
.L2479:
	movq	%rbx, %rdi
	movq	%r12, %rsi
.L2366:
	movsq
	cmpq	%rsi, %rbx
	jne	.L2366
	jmp	.L2369
.L2905:
	movq	-152(%rbp), %rax
	movl	$66323, %ecx
	movl	$66323, (%rax)
	jmp	.L2160
.L2374:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L2371
.L2450:
	movq	%r8, %rdi
	movq	%r12, %rsi
.L2130:
	movsq
	cmpq	%rsi, %r8
	jne	.L2130
	jmp	.L2133
.L2903:
	movq	-152(%rbp), %rax
	movq	%r11, %r15
	movq	%r12, %r11
	movl	$66323, (%rax)
	jmp	.L2799
.L2451:
	movq	%r13, %rdi
	movq	%r12, %rsi
.L2145:
	movsq
	cmpq	%rsi, %r13
	jne	.L2145
	jmp	.L2148
.L2391:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L2388
.L2347:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L2344
.L2356:
	movq	-160(%rbp), %rdi
	subl	$2, %edx
	movslq	%edx, %rdx
	movzwl	-2(%rdi,%r8), %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	cmovne	%rsi, %rdx
	jmp	.L2349
.L2170:
	movl	268(%r11), %eax
	movl	$10000, 272(%r11)
	leal	1(%rax), %esi
	movq	280(%r11), %rax
	movl	%esi, 268(%r11)
	testq	%rax, %rax
	je	.L2172
	movq	%r11, -168(%rbp)
	movq	288(%r11), %rdi
	call	*%rax
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2913
.L2172:
	movl	264(%r11), %eax
	testl	%eax, %eax
	jle	.L2174
	cmpl	268(%r11), %eax
	jg	.L2174
	movq	-152(%rbp), %rax
	movl	$66322, %ecx
	movl	$66322, (%rax)
	jmp	.L2171
.L2893:
	movq	-152(%rbp), %rax
	xorl	%r8d, %r8d
	movl	(%rax), %ecx
	jmp	.L2305
.L2896:
	movq	-152(%rbp), %rax
	xorl	%ebx, %ebx
	movl	(%rax), %ecx
	jmp	.L2164
.L2192:
	movl	268(%r11), %eax
	movl	$10000, 272(%r11)
	leal	1(%rax), %esi
	movq	280(%r11), %rax
	movl	%esi, 268(%r11)
	testq	%rax, %rax
	je	.L2194
	movq	%r8, -184(%rbp)
	movq	288(%r11), %rdi
	movq	%r11, -168(%rbp)
	call	*%rax
	movq	-168(%rbp), %r11
	movq	-184(%rbp), %r8
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2914
.L2194:
	movl	264(%r11), %eax
	testl	%eax, %eax
	jle	.L2196
	cmpl	268(%r11), %eax
	jg	.L2196
	movq	-152(%rbp), %rax
	movl	$66322, %ecx
	movl	$66322, (%rax)
	jmp	.L2193
.L2895:
	movq	-152(%rbp), %rax
	xorl	%ebx, %ebx
	movl	(%rax), %ecx
	jmp	.L2208
.L2203:
	movq	-152(%rbp), %rax
	movq	%r11, %r15
	movq	%r12, %r11
	movl	$66322, (%rax)
	jmp	.L2799
.L2214:
	movl	268(%r11), %eax
	movl	$10000, 272(%r11)
	leal	1(%rax), %esi
	movq	280(%r11), %rax
	movl	%esi, 268(%r11)
	testq	%rax, %rax
	je	.L2216
	movq	%r11, -168(%rbp)
	movq	288(%r11), %rdi
	call	*%rax
	movq	-168(%rbp), %r11
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2915
.L2216:
	movl	264(%r11), %eax
	testl	%eax, %eax
	jle	.L2218
	cmpl	268(%r11), %eax
	jg	.L2218
	movq	-152(%rbp), %rax
	movl	$66322, %ecx
	movl	$66322, (%rax)
	jmp	.L2215
.L2138:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L2135
.L2286:
	movl	268(%r11), %eax
	movl	$10000, 272(%r11)
	leal	1(%rax), %esi
	movq	280(%r11), %rax
	movl	%esi, 268(%r11)
	testq	%rax, %rax
	je	.L2288
	movq	%r8, -184(%rbp)
	movq	288(%r11), %rdi
	movq	%r11, -168(%rbp)
	call	*%rax
	movq	-168(%rbp), %r11
	movq	-184(%rbp), %r8
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2916
.L2288:
	movl	264(%r11), %eax
	testl	%eax, %eax
	jle	.L2290
	cmpl	268(%r11), %eax
	jg	.L2290
	movq	-152(%rbp), %rax
	movl	$66322, %ecx
	movl	$66322, (%rax)
	jmp	.L2287
.L2891:
	movq	-152(%rbp), %rax
	xorl	%r8d, %r8d
	movl	(%rax), %ecx
	jmp	.L2280
.L2311:
	movl	268(%r11), %eax
	movl	$10000, 272(%r11)
	leal	1(%rax), %esi
	movq	280(%r11), %rax
	movl	%esi, 268(%r11)
	testq	%rax, %rax
	je	.L2313
	movq	%r8, -184(%rbp)
	movq	288(%r11), %rdi
	movq	%r11, -168(%rbp)
	call	*%rax
	movq	-168(%rbp), %r11
	movq	-184(%rbp), %r8
	leaq	.L1916(%rip), %r10
	testb	%al, %al
	je	.L2917
.L2313:
	movl	264(%r11), %eax
	testl	%eax, %eax
	jle	.L2315
	cmpl	268(%r11), %eax
	jg	.L2315
	movq	-152(%rbp), %rax
	movl	$66322, %ecx
	movl	$66322, (%rax)
	jmp	.L2312
.L2178:
	movl	$66322, (%rax)
	movl	$66322, %ecx
	jmp	.L2160
.L2907:
	movq	-152(%rbp), %rax
	movl	$66323, %ecx
	movl	$66323, (%rax)
	jmp	.L2120
.L2906:
	movq	-152(%rbp), %rax
	movl	$66323, %ecx
	movl	$66323, (%rax)
	jmp	.L1995
.L2453:
	movq	%rbx, %rdi
	movq	%r12, %rsi
.L2166:
	movsq
	cmpq	%rsi, %rbx
	jne	.L2166
	jmp	.L2169
.L2454:
	movq	%r8, %rdi
	movq	%r12, %rsi
.L2188:
	movsq
	cmpq	%rsi, %r8
	jne	.L2188
	jmp	.L2191
.L2469:
	movq	%r8, %rdi
	movq	%r12, %rsi
.L2307:
	movsq
	cmpq	%rsi, %r8
	jne	.L2307
	jmp	.L2310
.L2467:
	movq	%r8, %rdi
	movq	%r12, %rsi
.L2282:
	movsq
	cmpq	%rsi, %r8
	jne	.L2282
	jmp	.L2285
.L2174:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L2171
.L2457:
	movq	%rax, %r8
	jmp	.L2230
.L2867:
	movq	-152(%rbp), %rax
	xorl	%r8d, %r8d
	movl	(%rax), %ecx
	jmp	.L2186
.L2908:
	movq	-152(%rbp), %rax
	movl	$66323, %ecx
	movl	$66323, (%rax)
	jmp	.L2388
.L2909:
	movq	-152(%rbp), %rax
	movl	$66323, %ecx
	movl	$66323, (%rax)
	jmp	.L2344
.L2910:
	movq	-152(%rbp), %rax
	movl	$66323, %ecx
	movl	$66323, (%rax)
	jmp	.L2371
.L2455:
	movq	%rbx, %rdi
	movq	%r12, %rsi
.L2210:
	movsq
	cmpq	%rsi, %rbx
	jne	.L2210
	jmp	.L2213
.L2899:
	cmpq	80(%r11), %rax
	jle	.L2010
	movq	-160(%rbp), %rax
	cmpw	$13, -2(%rax,%rsi)
	jne	.L2010
	jmp	.L2007
.L2218:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L2215
.L2290:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L2287
.L2315:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L2312
.L2912:
	movq	-152(%rbp), %rax
	movl	$66323, %ecx
	movl	$66323, (%rax)
	jmp	.L2135
.L2915:
	movq	-152(%rbp), %rax
	movl	$66323, %ecx
	movl	$66323, (%rax)
	jmp	.L2215
.L2911:
	movq	-152(%rbp), %rax
	movl	$66323, (%rax)
	jmp	.L2150
.L2916:
	movq	-152(%rbp), %rax
	movl	$66323, %ecx
	movl	$66323, (%rax)
	jmp	.L2287
.L2917:
	movq	-152(%rbp), %rax
	movl	$66323, %ecx
	movl	$66323, (%rax)
	jmp	.L2312
.L2857:
	movq	-160(%rbp), %rdi
	leaq	2(%r8), %rbx
	movzwl	2(%rdi,%r8), %r13d
	movl	%r13d, %edi
	andl	$64512, %edi
	cmpl	$56320, %edi
	je	.L2918
	movl	$55296, %edi
	jmp	.L2060
.L2196:
	movq	-152(%rbp), %rax
	movl	(%rax), %ecx
	jmp	.L2193
.L2913:
	movq	-152(%rbp), %rax
	movl	$66323, %ecx
	movl	$66323, (%rax)
	jmp	.L2171
.L2914:
	movq	-152(%rbp), %rax
	movl	$66323, %ecx
	movl	$66323, (%rax)
	jmp	.L2193
.L2804:
	call	__stack_chk_fail@PLT
.L2918:
	leaq	2(%rax), %r9
	sall	$10, %esi
	movq	%rdx, %rax
	movq	%rbx, %r8
	movq	%r9, (%r12)
	leal	9216(%r13,%rsi), %esi
	movq	%r9, %rdx
	jmp	.L2060
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0.cold, @function
_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0.cold:
.LFSB4498:
.L1914:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE4498:
	.text
	.size	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0, .-_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0.cold, .-_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0.cold
.LCOLDE3:
	.text
.LHOTE3:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode, @function
_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode:
.LFB3320:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L2919
	movsbl	%dl, %edx
	jmp	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L2919:
	ret
	.cfi_endproc
.LFE3320:
	.size	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode, .-_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher9lookingAtER10UErrorCode
	.type	_ZN6icu_6712RegexMatcher9lookingAtER10UErrorCode, @function
_ZN6icu_6712RegexMatcher9lookingAtER10UErrorCode:
.LFB3272:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L2936
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	316(%rdi), %edx
	testl	%edx, %edx
	jg	.L2939
	cmpb	$0, 312(%rdi)
	movq	32(%rdi), %r13
	jne	.L2940
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movb	$0, 130(%rdi)
	movq	112(%rdi), %rsi
	movw	%ax, 168(%rdi)
	movabsq	$42949672960000, %rax
	movups	%xmm0, 136(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, 268(%rdi)
	movups	%xmm0, 152(%rdi)
.L2926:
	cmpq	$0, 32(%r13)
	movl	(%r12), %eax
	jne	.L2931
	movq	48(%rbx), %rdx
	cmpq	16(%r13), %rdx
	jne	.L2931
	movslq	28(%r13), %rcx
	cmpq	%rcx, %rdx
	je	.L2941
	.p2align 4,,10
	.p2align 3
.L2931:
	testl	%eax, %eax
	jg	.L2932
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0
.L2932:
	movzbl	130(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2939:
	.cfi_restore_state
	movl	%edx, (%rsi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2936:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L2940:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r13, %rdi
	call	utext_nativeLength_67@PLT
	movq	%rax, %r8
	movslq	28(%r13), %rax
	cmpq	%rax, %r8
	je	.L2942
	movq	72(%r13), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	js	.L2927
	movswl	%cx, %edx
	sarl	$5, %edx
.L2928:
	testb	$17, %cl
	jne	.L2934
	andl	$2, %ecx
	je	.L2930
	addq	$10, %rax
.L2929:
	movq	%rax, 48(%r13)
	movslq	%edx, %rax
	movq	32(%rbx), %rdi
	movl	%edx, 44(%r13)
	movl	%edx, 28(%r13)
	movq	%rax, 16(%r13)
	call	utext_nativeLength_67@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	32(%rbx), %r13
	movups	%xmm0, 136(%rbx)
	movdqa	.LC0(%rip), %xmm0
	xorl	%esi, %esi
	movq	%rax, 48(%rbx)
	movq	%rax, 72(%rbx)
	movq	%rax, 120(%rbx)
	movq	%rax, 88(%rbx)
	movq	%rax, 104(%rbx)
	movabsq	$42949672960000, %rax
	movq	$0, 64(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 96(%rbx)
	movb	$0, 130(%rbx)
	movw	%dx, 168(%rbx)
	movq	%rax, 268(%rbx)
	movups	%xmm0, 152(%rbx)
	jmp	.L2926
	.p2align 4,,10
	.p2align 3
.L2927:
	movl	12(%rax), %edx
	jmp	.L2928
	.p2align 4,,10
	.p2align 3
.L2941:
	testl	%eax, %eax
	jg	.L2932
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0
	jmp	.L2932
	.p2align 4,,10
	.p2align 3
.L2942:
	movq	112(%rbx), %rsi
	movq	32(%rbx), %r13
	jmp	.L2926
	.p2align 4,,10
	.p2align 3
.L2930:
	movq	24(%rax), %rax
	jmp	.L2929
	.p2align 4,,10
	.p2align 3
.L2934:
	xorl	%eax, %eax
	jmp	.L2929
	.cfi_endproc
.LFE3272:
	.size	_ZN6icu_6712RegexMatcher9lookingAtER10UErrorCode, .-_ZN6icu_6712RegexMatcher9lookingAtER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher9lookingAtElR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher9lookingAtElR10UErrorCode, @function
_ZN6icu_6712RegexMatcher9lookingAtElR10UErrorCode:
.LFB3273:
	.cfi_startproc
	endbr64
	movl	(%rdx), %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L2962
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	316(%rdi), %edx
	testl	%edx, %edx
	jg	.L2965
	pxor	%xmm0, %xmm0
	movq	48(%rdi), %rax
	xorl	%ecx, %ecx
	movq	$0, 64(%rdi)
	movups	%xmm0, 136(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movw	%cx, 168(%rdi)
	movabsq	$42949672960000, %rcx
	movq	%rax, 72(%rdi)
	movq	$0, 112(%rdi)
	movq	%rax, 120(%rdi)
	movq	$0, 80(%rdi)
	movq	%rax, 88(%rdi)
	movq	$0, 96(%rdi)
	movq	%rax, 104(%rdi)
	movb	$0, 130(%rdi)
	movq	%rcx, 268(%rdi)
	movups	%xmm0, 152(%rdi)
	testq	%rsi, %rsi
	js	.L2954
	cmpb	$0, 312(%rdi)
	jne	.L2966
.L2958:
	cmpq	%rsi, %rax
	jl	.L2954
	movq	32(%rbx), %rax
	movl	(%r12), %edx
	cmpq	$0, 32(%rax)
	jne	.L2956
	movq	48(%rbx), %rcx
	cmpq	16(%rax), %rcx
	je	.L2967
.L2956:
	testl	%edx, %edx
	jg	.L2957
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0
.L2957:
	movzbl	130(%rbx), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2965:
	.cfi_restore_state
	movl	%edx, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2962:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L2948:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	cmpq	112(%rbx), %rsi
	jge	.L2968
.L2954:
	movl	$8, (%r12)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2966:
	.cfi_restore_state
	movq	32(%rdi), %r13
	movq	%rsi, -40(%rbp)
	movq	%r13, %rdi
	call	utext_nativeLength_67@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r8
	movslq	28(%r13), %rax
	cmpq	%rax, %r8
	je	.L2948
	movq	72(%r13), %rdx
	movzwl	8(%rdx), %ecx
	testw	%cx, %cx
	js	.L2949
	movswl	%cx, %eax
	sarl	$5, %eax
.L2950:
	testb	$17, %cl
	jne	.L2960
	andl	$2, %ecx
	je	.L2952
	addq	$10, %rdx
.L2951:
	movq	%rdx, 48(%r13)
	movslq	%eax, %rdx
	movq	32(%rbx), %rdi
	movq	%rdx, 16(%r13)
	movl	%eax, 44(%r13)
	movl	%eax, 28(%r13)
	movq	%rsi, -40(%rbp)
	call	utext_nativeLength_67@PLT
	pxor	%xmm1, %xmm1
	xorl	%edx, %edx
	movq	-40(%rbp), %rsi
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, 48(%rbx)
	movabsq	$42949672960000, %rcx
	movq	$0, 64(%rbx)
	movq	%rax, 72(%rbx)
	movq	$0, 112(%rbx)
	movq	%rax, 120(%rbx)
	movq	$0, 80(%rbx)
	movq	%rax, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	%rax, 104(%rbx)
	movb	$0, 130(%rbx)
	movw	%dx, 168(%rbx)
	movq	%rcx, 268(%rbx)
	movups	%xmm1, 136(%rbx)
	movups	%xmm0, 152(%rbx)
	jmp	.L2958
	.p2align 4,,10
	.p2align 3
.L2949:
	movl	12(%rdx), %eax
	jmp	.L2950
	.p2align 4,,10
	.p2align 3
.L2967:
	movslq	28(%rax), %rax
	cmpq	%rax, %rcx
	jne	.L2956
	testl	%edx, %edx
	jg	.L2957
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0
	jmp	.L2957
	.p2align 4,,10
	.p2align 3
.L2952:
	movq	24(%rdx), %rdx
	jmp	.L2951
.L2960:
	xorl	%edx, %edx
	jmp	.L2951
.L2968:
	movq	120(%rbx), %rax
	jmp	.L2958
	.cfi_endproc
.LFE3273:
	.size	_ZN6icu_6712RegexMatcher9lookingAtElR10UErrorCode, .-_ZN6icu_6712RegexMatcher9lookingAtElR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher7matchesER10UErrorCode
	.type	_ZN6icu_6712RegexMatcher7matchesER10UErrorCode, @function
_ZN6icu_6712RegexMatcher7matchesER10UErrorCode:
.LFB3274:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L2984
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	316(%rdi), %edx
	testl	%edx, %edx
	jg	.L2987
	cmpb	$0, 312(%rdi)
	movq	32(%rdi), %r13
	jne	.L2988
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movb	$0, 130(%rdi)
	movq	112(%rdi), %rsi
	movw	%ax, 168(%rdi)
	movabsq	$42949672960000, %rax
	movups	%xmm0, 136(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, 268(%rdi)
	movups	%xmm0, 152(%rdi)
.L2974:
	cmpq	$0, 32(%r13)
	movl	(%r12), %eax
	jne	.L2979
	movq	48(%rbx), %rdx
	cmpq	16(%r13), %rdx
	jne	.L2979
	movslq	28(%r13), %rcx
	cmpq	%rcx, %rdx
	je	.L2989
	.p2align 4,,10
	.p2align 3
.L2979:
	testl	%eax, %eax
	jg	.L2980
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0
.L2980:
	movzbl	130(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2987:
	.cfi_restore_state
	movl	%edx, (%rsi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2984:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L2988:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r13, %rdi
	call	utext_nativeLength_67@PLT
	movq	%rax, %r8
	movslq	28(%r13), %rax
	cmpq	%rax, %r8
	je	.L2990
	movq	72(%r13), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	js	.L2975
	movswl	%cx, %edx
	sarl	$5, %edx
.L2976:
	testb	$17, %cl
	jne	.L2982
	andl	$2, %ecx
	je	.L2978
	addq	$10, %rax
.L2977:
	movq	%rax, 48(%r13)
	movslq	%edx, %rax
	movq	32(%rbx), %rdi
	movl	%edx, 44(%r13)
	movl	%edx, 28(%r13)
	movq	%rax, 16(%r13)
	call	utext_nativeLength_67@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	32(%rbx), %r13
	movups	%xmm0, 136(%rbx)
	movdqa	.LC0(%rip), %xmm0
	xorl	%esi, %esi
	movq	%rax, 48(%rbx)
	movq	%rax, 72(%rbx)
	movq	%rax, 120(%rbx)
	movq	%rax, 88(%rbx)
	movq	%rax, 104(%rbx)
	movabsq	$42949672960000, %rax
	movq	$0, 64(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 96(%rbx)
	movb	$0, 130(%rbx)
	movw	%dx, 168(%rbx)
	movq	%rax, 268(%rbx)
	movups	%xmm0, 152(%rbx)
	jmp	.L2974
	.p2align 4,,10
	.p2align 3
.L2975:
	movl	12(%rax), %edx
	jmp	.L2976
	.p2align 4,,10
	.p2align 3
.L2989:
	testl	%eax, %eax
	jg	.L2980
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0
	jmp	.L2980
	.p2align 4,,10
	.p2align 3
.L2990:
	movq	112(%rbx), %rsi
	movq	32(%rbx), %r13
	jmp	.L2974
	.p2align 4,,10
	.p2align 3
.L2978:
	movq	24(%rax), %rax
	jmp	.L2977
	.p2align 4,,10
	.p2align 3
.L2982:
	xorl	%eax, %eax
	jmp	.L2977
	.cfi_endproc
.LFE3274:
	.size	_ZN6icu_6712RegexMatcher7matchesER10UErrorCode, .-_ZN6icu_6712RegexMatcher7matchesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher7matchesElR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher7matchesElR10UErrorCode, @function
_ZN6icu_6712RegexMatcher7matchesElR10UErrorCode:
.LFB3275:
	.cfi_startproc
	endbr64
	movl	(%rdx), %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L3010
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	316(%rdi), %edx
	testl	%edx, %edx
	jg	.L3013
	pxor	%xmm0, %xmm0
	movq	48(%rdi), %rax
	xorl	%ecx, %ecx
	movq	$0, 64(%rdi)
	movups	%xmm0, 136(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movw	%cx, 168(%rdi)
	movabsq	$42949672960000, %rcx
	movq	%rax, 72(%rdi)
	movq	$0, 112(%rdi)
	movq	%rax, 120(%rdi)
	movq	$0, 80(%rdi)
	movq	%rax, 88(%rdi)
	movq	$0, 96(%rdi)
	movq	%rax, 104(%rdi)
	movb	$0, 130(%rdi)
	movq	%rcx, 268(%rdi)
	movups	%xmm0, 152(%rdi)
	testq	%rsi, %rsi
	js	.L3002
	cmpb	$0, 312(%rdi)
	jne	.L3014
.L3006:
	cmpq	%rsi, %rax
	jl	.L3002
	movq	32(%rbx), %rax
	movl	(%r12), %edx
	cmpq	$0, 32(%rax)
	jne	.L3004
	movq	48(%rbx), %rcx
	cmpq	16(%rax), %rcx
	je	.L3015
.L3004:
	testl	%edx, %edx
	jg	.L3005
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0
.L3005:
	movzbl	130(%rbx), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3013:
	.cfi_restore_state
	movl	%edx, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3010:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L2996:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	cmpq	112(%rbx), %rsi
	jge	.L3016
.L3002:
	movl	$8, (%r12)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3014:
	.cfi_restore_state
	movq	32(%rdi), %r13
	movq	%rsi, -40(%rbp)
	movq	%r13, %rdi
	call	utext_nativeLength_67@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r8
	movslq	28(%r13), %rax
	cmpq	%rax, %r8
	je	.L2996
	movq	72(%r13), %rdx
	movzwl	8(%rdx), %ecx
	testw	%cx, %cx
	js	.L2997
	movswl	%cx, %eax
	sarl	$5, %eax
.L2998:
	testb	$17, %cl
	jne	.L3008
	andl	$2, %ecx
	je	.L3000
	addq	$10, %rdx
.L2999:
	movq	%rdx, 48(%r13)
	movslq	%eax, %rdx
	movq	32(%rbx), %rdi
	movq	%rdx, 16(%r13)
	movl	%eax, 44(%r13)
	movl	%eax, 28(%r13)
	movq	%rsi, -40(%rbp)
	call	utext_nativeLength_67@PLT
	pxor	%xmm1, %xmm1
	xorl	%edx, %edx
	movq	-40(%rbp), %rsi
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, 48(%rbx)
	movabsq	$42949672960000, %rcx
	movq	$0, 64(%rbx)
	movq	%rax, 72(%rbx)
	movq	$0, 112(%rbx)
	movq	%rax, 120(%rbx)
	movq	$0, 80(%rbx)
	movq	%rax, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	%rax, 104(%rbx)
	movb	$0, 130(%rbx)
	movw	%dx, 168(%rbx)
	movq	%rcx, 268(%rbx)
	movups	%xmm1, 136(%rbx)
	movups	%xmm0, 152(%rbx)
	jmp	.L3006
	.p2align 4,,10
	.p2align 3
.L2997:
	movl	12(%rdx), %eax
	jmp	.L2998
	.p2align 4,,10
	.p2align 3
.L3015:
	movslq	28(%rax), %rax
	cmpq	%rax, %rcx
	jne	.L3004
	testl	%edx, %edx
	jg	.L3005
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0
	jmp	.L3005
	.p2align 4,,10
	.p2align 3
.L3000:
	movq	24(%rdx), %rdx
	jmp	.L2999
.L3008:
	xorl	%edx, %edx
	jmp	.L2999
.L3016:
	movq	120(%rbx), %rax
	jmp	.L3006
	.cfi_endproc
.LFE3275:
	.size	_ZN6icu_6712RegexMatcher7matchesElR10UErrorCode, .-_ZN6icu_6712RegexMatcher7matchesElR10UErrorCode
	.section	.text.unlikely
	.align 2
.LCOLDB4:
	.text
.LHOTB4:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher14findUsingChunkER10UErrorCode
	.type	_ZN6icu_6712RegexMatcher14findUsingChunkER10UErrorCode, @function
_ZN6icu_6712RegexMatcher14findUsingChunkER10UErrorCode:
.LFB3258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	144(%rdi), %rax
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L3018
	movl	112(%rdi), %r12d
.L3018:
	movq	32(%rbx), %rdx
	cmpb	$0, 130(%rbx)
	movq	48(%rdx), %rcx
	movq	%rcx, -56(%rbp)
	je	.L3019
	movq	%rax, 152(%rbx)
	movq	120(%rbx), %r13
	cmpq	136(%rbx), %rax
	je	.L3110
.L3020:
	movq	8(%rbx), %rax
	subl	124(%rax), %r13d
	cmpl	%r13d, %r12d
	jg	.L3024
.L3111:
	cmpl	$5, 144(%rax)
	ja	.L3025
	movl	144(%rax), %edx
	leaq	.L3027(%rip), %rsi
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L3027:
	.long	.L3031-.L3027
	.long	.L3026-.L3027
	.long	.L3030-.L3027
	.long	.L3029-.L3027
	.long	.L3028-.L3027
	.long	.L3026-.L3027
	.text
	.p2align 4,,10
	.p2align 3
.L3019:
	cmpq	$0, 152(%rbx)
	jns	.L3023
	movq	8(%rbx), %rax
	movq	120(%rbx), %r13
	subl	124(%rax), %r13d
	cmpl	%r13d, %r12d
	jle	.L3111
	jmp	.L3024
	.p2align 4,,10
	.p2align 3
.L3113:
	movq	176(%rax), %rdx
	movl	%esi, %eax
	andl	$7, %esi
	sarl	$3, %eax
	cltq
	movsbl	(%rdx,%rax), %eax
	btl	%esi, %eax
	jnc	.L3044
.L3041:
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L3033
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0
	movl	(%r14), %r11d
	testl	%r11d, %r11d
	jg	.L3033
	cmpb	$0, 130(%rbx)
	jne	.L3045
.L3044:
	cmpl	%r13d, %r15d
	jg	.L3024
	movq	296(%rbx), %rax
	testq	%rax, %rax
	je	.L3046
	movq	304(%rbx), %rdi
	movslq	%r15d, %rsi
	call	*%rax
	testb	%al, %al
	je	.L3037
.L3046:
	movq	8(%rbx), %rax
	movl	%r15d, %r12d
.L3030:
	movq	-56(%rbp), %rcx
	movslq	%r12d, %rdx
	leal	1(%r12), %r15d
	leaq	(%rdx,%rdx), %rdi
	movzwl	(%rcx,%rdx,2), %esi
	movl	%esi, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L3112
.L3039:
	cmpl	$255, %esi
	jle	.L3113
	movq	160(%rax), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L3041
	jmp	.L3044
	.p2align 4,,10
	.p2align 3
.L3024:
	movb	$0, 130(%rbx)
	xorl	%eax, %eax
	movb	$1, 168(%rbx)
.L3017:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3023:
	.cfi_restore_state
	movb	$1, 168(%rbx)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3110:
	.cfi_restore_state
	movslq	%r12d, %rax
	cmpq	%r13, %rax
	jge	.L3024
	leaq	(%rax,%rax), %rsi
	movzwl	(%rcx,%rax,2), %eax
	leal	1(%r12), %edx
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L3114
.L3067:
	movl	%edx, %r12d
	jmp	.L3020
	.p2align 4,,10
	.p2align 3
.L3026:
	movl	168(%rax), %eax
	movl	%eax, -64(%rbp)
.L3047:
	movq	-56(%rbp), %rcx
	movslq	%r12d, %rax
	leal	1(%r12), %r15d
	leaq	(%rax,%rax), %rsi
	movzwl	(%rcx,%rax,2), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L3115
.L3048:
	cmpl	-64(%rbp), %eax
	je	.L3049
.L3051:
	cmpl	%r13d, %r15d
	jg	.L3024
	movq	296(%rbx), %rax
	testq	%rax, %rax
	je	.L3052
	movq	304(%rbx), %rdi
	movslq	%r15d, %rsi
	call	*%rax
	testb	%al, %al
	je	.L3037
.L3052:
	movl	%r15d, %r12d
	jmp	.L3047
	.p2align 4,,10
	.p2align 3
.L3031:
	movl	(%r14), %esi
	testl	%esi, %esi
	jg	.L3033
.L3117:
	movl	%r12d, %esi
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0
	movl	(%r14), %esi
	testl	%esi, %esi
	jg	.L3033
	cmpb	$0, 130(%rbx)
	jne	.L3045
	cmpl	%r13d, %r12d
	jge	.L3023
	movq	-56(%rbp), %rcx
	movslq	%r12d, %rax
	leal	1(%r12), %r15d
	leaq	(%rax,%rax), %rdx
	movslq	%r15d, %r8
	movzwl	(%rcx,%rax,2), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L3116
.L3035:
	movq	296(%rbx), %rax
	testq	%rax, %rax
	je	.L3036
	movq	304(%rbx), %rdi
	movq	%r8, %rsi
	call	*%rax
	testb	%al, %al
	je	.L3037
	movl	(%r14), %esi
.L3036:
	movl	%r15d, %r12d
	testl	%esi, %esi
	jle	.L3117
.L3033:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3029:
	.cfi_restore_state
	movslq	%r12d, %rax
	cmpq	112(%rbx), %rax
	jg	.L3118
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L3033
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L3033
	movzbl	130(%rbx), %eax
	jmp	.L3017
	.p2align 4,,10
	.p2align 3
.L3028:
	movslq	%r12d, %r15
	cmpq	80(%rbx), %r15
	je	.L3119
.L3053:
	testb	$1, 24(%rax)
	je	.L3055
.L3054:
	movq	-56(%rbp), %rax
	leaq	(%r15,%r15), %r8
	cmpw	$10, -2(%rax,%r15,2)
	je	.L3056
.L3058:
	cmpl	%r13d, %r12d
	jge	.L3024
	movq	-56(%rbp), %rax
	leal	1(%r12), %r15d
	movslq	%r15d, %rsi
	movzwl	(%rax,%r8), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L3120
.L3059:
	movq	296(%rbx), %rax
	testq	%rax, %rax
	je	.L3060
	movq	304(%rbx), %rdi
	call	*%rax
	testb	%al, %al
	je	.L3037
.L3060:
	movslq	%r15d, %r15
	movq	%r15, %r12
	jmp	.L3054
	.p2align 4,,10
	.p2align 3
.L3061:
	cmpl	%r13d, %r12d
	jge	.L3024
	movq	-56(%rbp), %rcx
	movslq	%r12d, %rax
	leal	1(%r12), %r15d
	leaq	(%rax,%rax), %rdx
	movslq	%r15d, %rsi
	movzwl	(%rcx,%rax,2), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L3121
.L3064:
	movq	296(%rbx), %rax
	testq	%rax, %rax
	je	.L3065
	movq	304(%rbx), %rdi
	call	*%rax
	testb	%al, %al
	je	.L3037
.L3065:
	movslq	%r15d, %r15
	movq	%r15, %r12
.L3055:
	movq	-56(%rbp), %rax
	leaq	(%r15,%r15), %rsi
	movzwl	-2(%rax,%r15,2), %eax
	testl	$57168, %eax
	jne	.L3061
	leal	-10(%rax), %edx
	cmpl	$3, %edx
	jbe	.L3062
	cmpl	$133, %eax
	je	.L3062
	leal	-8232(%rax), %edx
	cmpl	$1, %edx
	ja	.L3061
.L3062:
	cmpl	$13, %eax
	je	.L3122
.L3063:
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L3033
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L3033
	cmpb	$0, 130(%rbx)
	je	.L3061
	.p2align 4,,10
	.p2align 3
.L3045:
	movl	$1, %eax
	jmp	.L3017
	.p2align 4,,10
	.p2align 3
.L3037:
	movl	$66323, (%r14)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3049:
	.cfi_restore_state
	movl	(%r14), %r10d
	testl	%r10d, %r10d
	jg	.L3033
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0
	movl	(%r14), %r9d
	testl	%r9d, %r9d
	jg	.L3033
	cmpb	$0, 130(%rbx)
	je	.L3051
	jmp	.L3045
	.p2align 4,,10
	.p2align 3
.L3115:
	movslq	%r15d, %rdx
	cmpq	120(%rbx), %rdx
	je	.L3048
	movzwl	2(%rcx,%rsi), %edx
	movl	%edx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L3048
	sall	$10, %eax
	leal	2(%r12), %r15d
	leal	-56613888(%rdx,%rax), %eax
	jmp	.L3048
	.p2align 4,,10
	.p2align 3
.L3112:
	movslq	%r15d, %rdx
	cmpq	120(%rbx), %rdx
	je	.L3039
	movzwl	2(%rcx,%rdi), %edx
	movl	%edx, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L3039
	sall	$10, %esi
	leal	2(%r12), %r15d
	leal	-56613888(%rdx,%rsi), %esi
	jmp	.L3039
	.p2align 4,,10
	.p2align 3
.L3116:
	cmpq	%r8, 120(%rbx)
	je	.L3035
	movzwl	2(%rcx,%rdx), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L3035
	leal	2(%r12), %r15d
	movslq	%r15d, %r8
	jmp	.L3035
	.p2align 4,,10
	.p2align 3
.L3056:
	movl	(%r14), %esi
	movq	%r8, -64(%rbp)
	testl	%esi, %esi
	jg	.L3033
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L3033
	cmpb	$0, 130(%rbx)
	movq	-64(%rbp), %r8
	je	.L3058
	jmp	.L3045
	.p2align 4,,10
	.p2align 3
.L3121:
	cmpq	%rsi, 120(%rbx)
	je	.L3064
	movzwl	2(%rcx,%rdx), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L3064
	leal	2(%r12), %r15d
	movslq	%r15d, %rsi
	jmp	.L3064
	.p2align 4,,10
	.p2align 3
.L3120:
	cmpq	%rsi, 120(%rbx)
	je	.L3059
	movq	-56(%rbp), %rax
	movzwl	2(%rax,%r8), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L3059
	leal	2(%r12), %r15d
	movslq	%r15d, %rsi
	jmp	.L3059
	.p2align 4,,10
	.p2align 3
.L3114:
	movslq	%edx, %rax
	cmpq	48(%rbx), %rax
	je	.L3067
	movzwl	2(%rcx,%rsi), %eax
	addl	$2, %r12d
	andl	$-1024, %eax
	cmpl	$56320, %eax
	cmovne	%edx, %r12d
	jmp	.L3020
	.p2align 4,,10
	.p2align 3
.L3119:
	movl	(%r14), %r8d
	testl	%r8d, %r8d
	jg	.L3033
	movq	%rbx, %rdi
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	%r12d, %esi
	call	_ZN6icu_6712RegexMatcher12MatchChunkAtEiaR10UErrorCode.part.0
	movl	(%r14), %edi
	testl	%edi, %edi
	jg	.L3033
	cmpb	$0, 130(%rbx)
	jne	.L3045
	movq	-56(%rbp), %rcx
	leal	1(%r12), %esi
	leaq	(%r15,%r15), %rdi
	movq	8(%rbx), %rax
	movzwl	(%rcx,%r15,2), %edx
	movslq	%esi, %r15
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L3123
	movq	%r15, %r12
	jmp	.L3053
	.p2align 4,,10
	.p2align 3
.L3118:
	movb	$0, 130(%rbx)
	xorl	%eax, %eax
	jmp	.L3017
	.p2align 4,,10
	.p2align 3
.L3122:
	cmpq	120(%rbx), %r15
	jge	.L3063
	movq	-56(%rbp), %rax
	cmpw	$10, (%rax,%rsi)
	sete	%al
	movzbl	%al, %eax
	addl	%eax, %r12d
	jmp	.L3063
.L3123:
	cmpq	120(%rbx), %r15
	je	.L3071
	movzwl	2(%rcx,%rdi), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L3124
.L3071:
	movl	%esi, %r12d
	jmp	.L3053
.L3124:
	addl	$2, %r12d
	movslq	%r12d, %r15
	jmp	.L3053
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexMatcher14findUsingChunkER10UErrorCode.cold, @function
_ZN6icu_6712RegexMatcher14findUsingChunkER10UErrorCode.cold:
.LFSB3258:
.L3025:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3258:
	.text
	.size	_ZN6icu_6712RegexMatcher14findUsingChunkER10UErrorCode, .-_ZN6icu_6712RegexMatcher14findUsingChunkER10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexMatcher14findUsingChunkER10UErrorCode.cold, .-_ZN6icu_6712RegexMatcher14findUsingChunkER10UErrorCode.cold
.LCOLDE4:
	.text
.LHOTE4:
	.section	.text.unlikely
	.align 2
.LCOLDB5:
	.text
.LHOTB5:
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0, @function
_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0:
.LFB4503:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	32(%rdi), %rdi
	movq	32(%rdi), %rdx
	testq	%rdx, %rdx
	jne	.L3126
	movq	48(%r12), %rax
	cmpq	16(%rdi), %rax
	je	.L3259
.L3126:
	movq	144(%r12), %rax
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L3127
	movq	112(%r12), %r13
.L3127:
	cmpb	$0, 130(%r12)
	jne	.L3260
	cmpq	$0, 152(%r12)
	jns	.L3138
	movq	56(%rdi), %rcx
	movq	120(%r12), %rbx
.L3137:
	movq	8(%r12), %rax
	cmpq	$0, 72(%rcx)
	movslq	124(%rax), %rdx
	je	.L3261
.L3139:
	testl	%edx, %edx
	setg	%dl
	movzbl	%dl, %edx
	subq	%rdx, %rbx
.L3141:
	cmpl	$5, 144(%rax)
	ja	.L3142
	movl	144(%rax), %edx
	leaq	.L3144(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L3144:
	.long	.L3148-.L3144
	.long	.L3143-.L3144
	.long	.L3147-.L3144
	.long	.L3146-.L3144
	.long	.L3145-.L3144
	.long	.L3143-.L3144
	.text
	.p2align 4,,10
	.p2align 3
.L3260:
	movq	%rax, 152(%r12)
	movq	120(%r12), %rbx
	cmpq	136(%r12), %rax
	je	.L3262
	movq	56(%rdi), %rcx
	movq	8(%r12), %rax
	cmpq	$0, 72(%rcx)
	movslq	124(%rax), %rdx
	jne	.L3139
.L3261:
	subq	%rdx, %rbx
	cmpq	%r13, %rbx
	jge	.L3141
.L3140:
	movb	$0, 130(%r12)
	xorl	%eax, %eax
	movb	$1, 168(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3148:
	.cfi_restore_state
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L3149
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L3149
	cmpb	$0, 130(%r12)
	jne	.L3172
	cmpq	%rbx, %r13
	jge	.L3138
	movq	32(%r12), %rdi
	movq	%r13, %rax
	subq	32(%rdi), %rax
	js	.L3151
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jl	.L3263
.L3151:
	movq	%r13, %rsi
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %r13d
.L3152:
	cmpl	%r13d, 44(%rdi)
	jle	.L3153
	movq	48(%rdi), %rdx
	movslq	%r13d, %rax
	cmpw	$-10241, (%rdx,%rax,2)
	jbe	.L3264
.L3153:
	call	utext_next32_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %r13d
.L3154:
	cmpl	%r13d, 28(%rdi)
	jge	.L3265
	movq	56(%rdi), %rax
	call	*64(%rax)
	movq	%rax, %r13
.L3156:
	movq	296(%r12), %rax
	testq	%rax, %rax
	je	.L3148
	movq	304(%r12), %rdi
	movq	%r13, %rsi
	call	*%rax
	testb	%al, %al
	jne	.L3148
.L3175:
	movl	$66323, (%r14)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3146:
	.cfi_restore_state
	cmpq	112(%r12), %r13
	jg	.L3266
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L3267
	.p2align 4,,10
	.p2align 3
.L3149:
	xorl	%eax, %eax
.L3125:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3138:
	.cfi_restore_state
	movb	$1, 168(%r12)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3143:
	.cfi_restore_state
	movl	168(%rax), %eax
	movq	%r13, %rdx
	movl	%eax, -56(%rbp)
	subq	32(%rdi), %rdx
	js	.L3176
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jl	.L3268
.L3176:
	movq	%r13, %rsi
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L3178:
	cmpl	%eax, 44(%rdi)
	jle	.L3179
.L3270:
	movq	48(%rdi), %rcx
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	cmpw	$-10241, %dx
	ja	.L3179
	addl	$1, %eax
	movl	%eax, 40(%rdi)
	cmpl	%eax, 28(%rdi)
	jl	.L3181
.L3271:
	cltq
	addq	32(%rdi), %rax
	movq	%rax, %r15
	cmpl	%edx, -56(%rbp)
	je	.L3269
.L3183:
	cmpq	%r15, %rbx
	jl	.L3140
	movq	296(%r12), %rax
	testq	%rax, %rax
	je	.L3185
	movq	304(%r12), %rdi
	movq	%r15, %rsi
	call	*%rax
	testb	%al, %al
	je	.L3175
.L3185:
	movq	32(%r12), %rdi
	movq	%r15, %r13
	movl	40(%rdi), %eax
	cmpl	%eax, 44(%rdi)
	jg	.L3270
.L3179:
	call	utext_next32_67@PLT
	movq	32(%r12), %rdi
	movl	%eax, %edx
	movl	40(%rdi), %eax
	cmpl	%eax, 28(%rdi)
	jge	.L3271
.L3181:
	movq	56(%rdi), %rax
	movl	%edx, -60(%rbp)
	call	*64(%rax)
	movl	-60(%rbp), %edx
	movq	%rax, %r15
	cmpl	%edx, -56(%rbp)
	jne	.L3183
.L3269:
	movl	(%r14), %r10d
	testl	%r10d, %r10d
	jg	.L3149
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0
	movl	(%r14), %r9d
	testl	%r9d, %r9d
	jg	.L3149
	cmpb	$0, 130(%r12)
	jne	.L3172
	movq	32(%r12), %rdi
	movq	%r15, %rax
	subq	32(%rdi), %rax
	js	.L3184
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jge	.L3184
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L3184
	movl	%eax, 40(%rdi)
	jmp	.L3183
	.p2align 4,,10
	.p2align 3
.L3265:
	movslq	%r13d, %r13
	addq	32(%rdi), %r13
	jmp	.L3156
	.p2align 4,,10
	.p2align 3
.L3259:
	movslq	28(%rdi), %rcx
	cmpq	%rcx, %rax
	jne	.L3126
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexMatcher14findUsingChunkER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L3263:
	.cfi_restore_state
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L3151
	movl	%eax, 40(%rdi)
	movl	%eax, %r13d
	jmp	.L3152
	.p2align 4,,10
	.p2align 3
.L3264:
	addl	$1, %r13d
	movl	%r13d, 40(%rdi)
	jmp	.L3154
	.p2align 4,,10
	.p2align 3
.L3145:
	cmpq	80(%r12), %r13
	je	.L3272
	movq	%r13, %rdx
	subq	32(%rdi), %rdx
	js	.L3193
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jl	.L3273
.L3193:
	movq	%r13, %rsi
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L3194:
	testl	%eax, %eax
	jle	.L3195
	movq	48(%rdi), %rdx
	movslq	%eax, %rcx
	cmpw	$-10241, -2(%rdx,%rcx,2)
	jbe	.L3274
.L3195:
	call	utext_previous32_67@PLT
	movq	32(%r12), %rdi
	movl	%eax, %r15d
.L3196:
	movq	%r13, %rax
	subq	32(%rdi), %rax
	js	.L3197
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jge	.L3197
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L3197
	movl	%eax, 40(%rdi)
	jmp	.L3192
	.p2align 4,,10
	.p2align 3
.L3147:
	movq	%r13, %rax
	subq	32(%rdi), %rax
	js	.L3159
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jl	.L3275
.L3159:
	movq	%r13, %rsi
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %edx
.L3161:
	cmpl	%edx, 44(%rdi)
	jle	.L3162
.L3277:
	movq	48(%rdi), %rcx
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %r15d
	cmpw	$-10241, %r15w
	ja	.L3162
	addl	$1, %edx
	movl	%edx, 40(%rdi)
	cmpl	%edx, 28(%rdi)
	jl	.L3164
.L3278:
	movslq	%edx, %rdx
	addq	32(%rdi), %rdx
	movq	%rdx, %r8
.L3165:
	testl	%r15d, %r15d
	js	.L3167
	movq	8(%r12), %rax
	cmpl	$255, %r15d
	jle	.L3276
	movq	160(%rax), %rdi
	movl	%r15d, %esi
	movq	%r8, -56(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L3167
.L3171:
	movl	(%r14), %r15d
	testl	%r15d, %r15d
	jg	.L3149
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0
	movl	(%r14), %r11d
	testl	%r11d, %r11d
	jg	.L3149
	cmpb	$0, 130(%r12)
	jne	.L3172
	movq	32(%r12), %rdi
	movq	%r13, %rax
	movq	-56(%rbp), %r8
	subq	32(%rdi), %rax
	js	.L3173
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jge	.L3173
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L3173
	movl	%eax, 40(%rdi)
	.p2align 4,,10
	.p2align 3
.L3167:
	cmpq	%r8, %rbx
	jl	.L3140
	movq	296(%r12), %rax
	testq	%rax, %rax
	je	.L3174
	movq	%r8, -56(%rbp)
	movq	%r8, %rsi
	movq	304(%r12), %rdi
	call	*%rax
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L3175
.L3174:
	movq	32(%r12), %rdi
	movq	%r8, %r13
	movl	40(%rdi), %edx
	cmpl	%edx, 44(%rdi)
	jg	.L3277
.L3162:
	call	utext_next32_67@PLT
	movq	32(%r12), %rdi
	movl	%eax, %r15d
	movl	40(%rdi), %edx
	cmpl	%edx, 28(%rdi)
	jge	.L3278
.L3164:
	movq	56(%rdi), %rax
	call	*64(%rax)
	movq	%rax, %r8
	jmp	.L3165
	.p2align 4,,10
	.p2align 3
.L3173:
	movq	%r13, %rsi
	movq	%r8, -56(%rbp)
	call	utext_setNativeIndex_67@PLT
	movq	-56(%rbp), %r8
	jmp	.L3167
	.p2align 4,,10
	.p2align 3
.L3197:
	movq	%r13, %rsi
	call	utext_setNativeIndex_67@PLT
.L3192:
	movq	8(%r12), %rax
	testb	$1, 24(%rax)
	je	.L3199
.L3198:
	cmpl	$10, %r15d
	je	.L3279
.L3200:
	cmpq	%r13, %rbx
	jle	.L3140
	movq	32(%r12), %rdi
	movl	40(%rdi), %edx
	cmpl	44(%rdi), %edx
	jge	.L3202
	movq	48(%rdi), %rcx
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %r15d
	cmpw	$-10241, %r15w
	ja	.L3202
	addl	$1, %edx
	movl	%edx, 40(%rdi)
.L3203:
	cmpl	%edx, 28(%rdi)
	jl	.L3204
	movslq	%edx, %rdx
	addq	32(%rdi), %rdx
	movq	%rdx, %r13
.L3205:
	movq	296(%r12), %rax
	testq	%rax, %rax
	je	.L3198
	movq	304(%r12), %rdi
	movq	%r13, %rsi
	call	*%rax
	testb	%al, %al
	jne	.L3198
	jmp	.L3175
	.p2align 4,,10
	.p2align 3
.L3208:
	cmpl	$13, %r15d
	jne	.L3209
	cmpq	%r13, 120(%r12)
	jle	.L3209
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
	cmpl	44(%rdi), %eax
	jge	.L3210
	movq	48(%rdi), %rcx
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	cmpw	$-10241, %dx
	ja	.L3210
	cmpw	$10, %dx
	jne	.L3209
.L3211:
	addl	$1, %eax
	movl	%eax, 40(%rdi)
.L3213:
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jg	.L3214
	addq	32(%rdi), %rax
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L3209:
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L3149
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L3149
	cmpb	$0, 130(%r12)
	jne	.L3172
	movq	32(%r12), %rdi
	movq	%r13, %rax
	subq	32(%rdi), %rax
	js	.L3215
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jge	.L3215
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L3215
	movl	%eax, 40(%rdi)
	.p2align 4,,10
	.p2align 3
.L3207:
	cmpq	%r13, %rbx
	jle	.L3140
	movq	32(%r12), %rdi
	movl	40(%rdi), %edx
	cmpl	44(%rdi), %edx
	jge	.L3216
	movq	48(%rdi), %rcx
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %r15d
	cmpw	$-10241, %r15w
	ja	.L3216
	addl	$1, %edx
	movl	%edx, 40(%rdi)
.L3217:
	cmpl	%edx, 28(%rdi)
	jl	.L3218
	movslq	%edx, %rdx
	addq	32(%rdi), %rdx
	movq	%rdx, %r13
.L3219:
	movq	296(%r12), %rax
	testq	%rax, %rax
	je	.L3199
	movq	304(%r12), %rdi
	movq	%r13, %rsi
	call	*%rax
	testb	%al, %al
	je	.L3175
.L3199:
	testl	$-8368, %r15d
	jne	.L3207
	leal	-10(%r15), %eax
	cmpl	$3, %eax
	jbe	.L3208
	cmpl	$133, %r15d
	je	.L3208
	leal	-8232(%r15), %eax
	cmpl	$1, %eax
	ja	.L3207
	jmp	.L3208
	.p2align 4,,10
	.p2align 3
.L3262:
	cmpq	%rbx, %r13
	jge	.L3140
	movq	%r13, %rax
	subq	%rdx, %rax
	movq	%rax, %rdx
	js	.L3132
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jl	.L3280
.L3132:
	movq	%r13, %rsi
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L3133:
	cmpl	%eax, 44(%rdi)
	jle	.L3134
	movq	48(%rdi), %rcx
	movslq	%eax, %rdx
	cmpw	$-10241, (%rcx,%rdx,2)
	jbe	.L3281
.L3134:
	call	utext_next32_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
.L3135:
	movq	56(%rdi), %rcx
	cmpl	%eax, 28(%rdi)
	jl	.L3136
	cltq
	addq	32(%rdi), %rax
	movq	120(%r12), %rbx
	movq	%rax, %r13
	jmp	.L3137
	.p2align 4,,10
	.p2align 3
.L3184:
	movq	%r15, %rsi
	call	utext_setNativeIndex_67@PLT
	jmp	.L3183
	.p2align 4,,10
	.p2align 3
.L3276:
	movq	176(%rax), %rdx
	movl	%r15d, %eax
	andl	$7, %r15d
	sarl	$3, %eax
	cltq
	movsbl	(%rdx,%rax), %eax
	btl	%r15d, %eax
	jnc	.L3167
	jmp	.L3171
	.p2align 4,,10
	.p2align 3
.L3216:
	call	utext_next32_67@PLT
	movq	32(%r12), %rdi
	movl	%eax, %r15d
	movl	40(%rdi), %edx
	jmp	.L3217
	.p2align 4,,10
	.p2align 3
.L3202:
	call	utext_next32_67@PLT
	movq	32(%r12), %rdi
	movl	%eax, %r15d
	movl	40(%rdi), %edx
	jmp	.L3203
	.p2align 4,,10
	.p2align 3
.L3218:
	movq	56(%rdi), %rax
	call	*64(%rax)
	movq	%rax, %r13
	jmp	.L3219
	.p2align 4,,10
	.p2align 3
.L3204:
	movq	56(%rdi), %rax
	call	*64(%rax)
	movq	%rax, %r13
	jmp	.L3205
	.p2align 4,,10
	.p2align 3
.L3279:
	movl	(%r14), %esi
	testl	%esi, %esi
	jg	.L3149
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L3149
	cmpb	$0, 130(%r12)
	jne	.L3172
	movq	32(%r12), %rdi
	movq	%r13, %rax
	subq	32(%rdi), %rax
	js	.L3201
	movslq	28(%rdi), %rdx
	cmpq	%rdx, %rax
	jge	.L3201
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L3201
	movl	%eax, 40(%rdi)
	jmp	.L3200
	.p2align 4,,10
	.p2align 3
.L3136:
	call	*64(%rcx)
	movq	32(%r12), %rdi
	movq	120(%r12), %rbx
	movq	%rax, %r13
	movq	56(%rdi), %rcx
	jmp	.L3137
	.p2align 4,,10
	.p2align 3
.L3267:
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L3149
	movzbl	130(%r12), %eax
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3268:
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L3176
	movl	%edx, 40(%rdi)
	movl	%edx, %eax
	jmp	.L3178
	.p2align 4,,10
	.p2align 3
.L3275:
	movq	48(%rdi), %rdx
	cmpw	$-9217, (%rdx,%rax,2)
	ja	.L3159
	movl	%eax, 40(%rdi)
	movl	%eax, %edx
	jmp	.L3161
	.p2align 4,,10
	.p2align 3
.L3280:
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L3132
	movl	%edx, 40(%rdi)
	movl	%edx, %eax
	jmp	.L3133
	.p2align 4,,10
	.p2align 3
.L3281:
	addl	$1, %eax
	movl	%eax, 40(%rdi)
	jmp	.L3135
	.p2align 4,,10
	.p2align 3
.L3273:
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L3193
	movl	%edx, 40(%rdi)
	movl	%edx, %eax
	jmp	.L3194
	.p2align 4,,10
	.p2align 3
.L3274:
	subl	$1, %eax
	movl	%eax, 40(%rdi)
	cltq
	movzwl	(%rdx,%rax,2), %r15d
	jmp	.L3196
	.p2align 4,,10
	.p2align 3
.L3201:
	movq	%r13, %rsi
	call	utext_setNativeIndex_67@PLT
	jmp	.L3200
	.p2align 4,,10
	.p2align 3
.L3215:
	movq	%r13, %rsi
	call	utext_setNativeIndex_67@PLT
	jmp	.L3207
	.p2align 4,,10
	.p2align 3
.L3266:
	movb	$0, 130(%r12)
	xorl	%eax, %eax
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3272:
	movl	(%r14), %r8d
	testl	%r8d, %r8d
	jg	.L3149
	movq	%r12, %rdi
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6712RegexMatcher7MatchAtElaR10UErrorCode.part.0
	movl	(%r14), %edi
	testl	%edi, %edi
	jg	.L3149
	cmpb	$0, 130(%r12)
	jne	.L3172
	movq	32(%r12), %rdi
	movq	%r13, %rdx
	subq	32(%rdi), %rdx
	js	.L3187
	movslq	28(%rdi), %rax
	cmpq	%rax, %rdx
	jge	.L3187
	movq	48(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L3187
	movl	%edx, 40(%rdi)
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L3188:
	cmpl	%eax, 44(%rdi)
	jle	.L3189
	movq	48(%rdi), %rcx
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %r15d
	cmpw	$-10241, %r15w
	ja	.L3189
	addl	$1, %eax
	movl	%eax, 40(%rdi)
.L3190:
	cmpl	%eax, 28(%rdi)
	jl	.L3191
	cltq
	addq	32(%rdi), %rax
	movq	%rax, %r13
	jmp	.L3192
	.p2align 4,,10
	.p2align 3
.L3172:
	movl	$1, %eax
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3187:
	movq	%r13, %rsi
	call	utext_setNativeIndex_67@PLT
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
	jmp	.L3188
	.p2align 4,,10
	.p2align 3
.L3189:
	call	utext_next32_67@PLT
	movq	32(%r12), %rdi
	movl	%eax, %r15d
	movl	40(%rdi), %eax
	jmp	.L3190
.L3191:
	movq	56(%rdi), %rax
	call	*64(%rax)
	movq	%rax, %r13
	jmp	.L3192
.L3210:
	call	utext_current32_67@PLT
	cmpl	$10, %eax
	jne	.L3209
	movq	32(%r12), %rdi
	movl	40(%rdi), %eax
	cmpl	44(%rdi), %eax
	jge	.L3212
	movq	48(%rdi), %rcx
	movslq	%eax, %rdx
	cmpw	$-10241, (%rcx,%rdx,2)
	jbe	.L3211
.L3212:
	call	utext_next32_67@PLT
	movq	32(%r12), %rdi
	jmp	.L3213
.L3214:
	movq	56(%rdi), %rax
	call	*64(%rax)
	movq	%rax, %r13
	jmp	.L3209
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0.cold, @function
_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0.cold:
.LFSB4503:
.L3142:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE4503:
	.text
	.size	_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0, .-_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0
	.section	.text.unlikely
	.size	_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0.cold, .-_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0.cold
.LCOLDE5:
	.text
.LHOTE5:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher4findER10UErrorCode
	.type	_ZN6icu_6712RegexMatcher4findER10UErrorCode, @function
_ZN6icu_6712RegexMatcher4findER10UErrorCode:
.LFB3256:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L3282
	movl	316(%rdi), %eax
	testl	%eax, %eax
	jg	.L3286
	jmp	_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L3286:
	movl	%eax, (%rsi)
.L3282:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3256:
	.size	_ZN6icu_6712RegexMatcher4findER10UErrorCode, .-_ZN6icu_6712RegexMatcher4findER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCode.part.0, @function
_ZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCode.part.0:
.LFB4507:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -360(%rbp)
	movq	%rsi, -344(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	subl	$1, %ecx
	movl	%ecx, -428(%rbp)
	movq	136(%rax), %rax
	movl	8(%rax), %eax
	movl	%eax, -336(%rbp)
	je	.L3356
	leaq	-304(%rbp), %rax
	xorl	%ebx, %ebx
	movq	$0, -376(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-308(%rbp), %rax
	movq	%rax, -440(%rbp)
	leaq	-146(%rbp), %rax
	movq	%rax, -416(%rbp)
.L3288:
	movl	316(%r15), %r8d
	testl	%r8d, %r8d
	jle	.L3357
.L3295:
	movq	-344(%rbp), %rsi
	leal	1(%rbx), %eax
	movq	120(%r15), %rdx
	movl	%eax, -328(%rbp)
	cmpq	$0, 32(%rsi)
	jne	.L3329
	movq	48(%r15), %rax
	cmpq	16(%rsi), %rax
	je	.L3358
.L3329:
	movq	-376(%rbp), %rsi
	movq	-344(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	-308(%rbp), %r9
	movl	$0, -308(%rbp)
	call	utext_extract_67@PLT
	leal	1(%rax), %r8d
	movl	%eax, %r12d
	movslq	%r8d, %rdi
	movl	%r8d, -336(%rbp)
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movl	-336(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L3359
	movq	120(%r15), %rdx
	movq	%r14, %r9
	movq	%rax, %rcx
	movslq	%ebx, %rbx
	movq	-376(%rbp), %rsi
	movq	-344(%rbp), %rdi
	call	utext_extract_67@PLT
	movq	-352(%rbp), %rax
	leaq	(%rax,%rbx,8), %rbx
	movq	(%rbx), %r8
	testq	%r8, %r8
	je	.L3332
	movq	%r8, %rdi
	call	utext_nativeLength_67@PLT
	movq	(%rbx), %rdi
	movq	%r14, %r9
	movl	%r12d, %r8d
	movq	%rax, %rdx
	movq	%r13, %rcx
	xorl	%esi, %esi
	call	utext_replace_67@PLT
.L3333:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
.L3287:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3360
	movl	-328(%rbp), %eax
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3357:
	.cfi_restore_state
	movq	-424(%rbp), %rsi
	movq	%r15, %rdi
	movl	$0, -304(%rbp)
	call	_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0
	testb	%al, %al
	je	.L3295
	movq	-344(%rbp), %rdi
	movq	136(%r15), %rdx
	cmpq	$0, 32(%rdi)
	jne	.L3296
	movq	48(%r15), %rax
	cmpq	16(%rdi), %rax
	je	.L3361
.L3296:
	movq	-440(%rbp), %r9
	movq	-376(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	-344(%rbp), %rdi
	movl	$0, -308(%rbp)
	call	utext_extract_67@PLT
	leal	1(%rax), %r8d
	movl	%eax, %r12d
	movslq	%r8d, %rdi
	movl	%r8d, -328(%rbp)
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movl	-328(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L3362
	movq	-376(%rbp), %rsi
	movq	136(%r15), %rdx
	movq	%r14, %r9
	movq	%rax, %rcx
	movq	-344(%rbp), %rdi
	call	utext_extract_67@PLT
	movq	-352(%rbp), %rsi
	movslq	%ebx, %rax
	leaq	(%rsi,%rax,8), %r10
	movq	(%r10), %r8
	movq	%r10, -328(%rbp)
	testq	%r8, %r8
	je	.L3300
	movq	%r8, %rdi
	call	utext_nativeLength_67@PLT
	movq	%r14, %r9
	movl	%r12d, %r8d
	movq	%r13, %rcx
	movq	-328(%rbp), %r10
	movq	%rax, %rdx
	xorl	%esi, %esi
	movq	(%r10), %rdi
	call	utext_replace_67@PLT
.L3301:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
.L3298:
	movq	144(%r15), %rax
	movl	-336(%rbp), %edi
	movq	%rax, -376(%rbp)
	testl	%edi, %edi
	jle	.L3339
	movl	-360(%rbp), %esi
	leal	-2(%rsi), %eax
	cmpl	%ebx, %eax
	jle	.L3339
	leal	-3(%rsi), %eax
	leal	1(%rbx), %r12d
	movl	%eax, %esi
	movslq	%r12d, %r12
	subl	%ebx, %esi
	xorl	%ebx, %ebx
	movq	%rsi, -368(%rbp)
	jmp	.L3303
	.p2align 4,,10
	.p2align 3
.L3366:
	testq	%r13, %r13
	je	.L3311
	movq	%r13, %rdi
	call	utext_nativeLength_67@PLT
	movq	%r14, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	utext_replace_67@PLT
.L3312:
	movq	-328(%rbp), %rax
	movq	%r13, (%rax)
	leal	2(%rbx), %eax
	cmpl	%eax, -336(%rbp)
	jl	.L3302
	leaq	1(%rbx), %rax
	addq	$1, %r12
	cmpq	-368(%rbp), %rbx
	je	.L3302
	movq	%rax, %rbx
.L3303:
	movl	(%r14), %esi
	movl	%r12d, -356(%rbp)
	testl	%esi, %esi
	jg	.L3309
	cmpb	$0, 130(%r15)
	je	.L3363
	movq	8(%r15), %rax
	leal	1(%rbx), %ecx
	movq	136(%rax), %rdx
	movl	8(%rdx), %eax
	cmpl	%ecx, %eax
	jl	.L3364
	subl	%ebx, %eax
	testl	%eax, %eax
	jle	.L3340
	movq	24(%rdx), %rax
	movslq	(%rax,%rbx,4), %rax
	leal	1(%rax), %edx
.L3307:
	movl	316(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L3365
	movq	184(%r15), %rcx
	movslq	%edx, %rdx
	movq	16(%rcx,%rax,8), %rsi
	movq	-352(%rbp), %rax
	movq	16(%rcx,%rdx,8), %rdx
	leaq	(%rax,%r12,8), %rdi
	movq	(%rax,%r12,8), %r13
	movq	%rdi, -328(%rbp)
	cmpq	%rdx, %rsi
	je	.L3366
	movq	32(%r15), %r10
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %r9
	movq	%rdx, -408(%rbp)
	movq	%r10, %rdi
	movq	%rsi, -400(%rbp)
	movq	%r10, -392(%rbp)
	call	utext_extract_67@PLT
	movq	-392(%rbp), %r10
	movq	-400(%rbp), %rsi
	movl	%eax, -384(%rbp)
	movl	(%r14), %eax
	movq	-408(%rbp), %rdx
	cmpl	$15, %eax
	je	.L3344
	testl	%eax, %eax
	jg	.L3312
.L3344:
	movq	-416(%rbp), %rcx
	movl	-384(%rbp), %eax
	movl	$0, (%r14)
	movl	$40, -152(%rbp)
	movq	%rcx, -160(%rbp)
	leal	1(%rax), %r8d
	movb	$0, -148(%rbp)
	cmpl	$39, %eax
	jg	.L3367
.L3314:
	movq	%r14, %r9
	movq	%r10, %rdi
	movl	%r8d, -392(%rbp)
	call	utext_extract_67@PLT
	testq	%r13, %r13
	movl	-392(%rbp), %r8d
	jne	.L3368
	movl	(%r14), %ecx
	movzbl	-148(%rbp), %edx
	testl	%ecx, %ecx
	jle	.L3319
	movq	-160(%rbp), %rdi
.L3318:
	testb	%dl, %dl
	je	.L3312
	call	uprv_free_67@PLT
	jmp	.L3312
	.p2align 4,,10
	.p2align 3
.L3358:
	movslq	28(%rsi), %rcx
	cmpq	%rcx, %rax
	jne	.L3329
	movq	-352(%rbp), %rax
	movslq	%ebx, %rbx
	movq	-376(%rbp), %r15
	leaq	(%rax,%rbx,8), %rbx
	movq	48(%rsi), %rax
	movq	(%rbx), %r8
	leaq	(%rax,%r15,2), %r12
	testq	%r8, %r8
	je	.L3330
	movq	%r8, %rdi
	movq	%rdx, -336(%rbp)
	call	utext_nativeLength_67@PLT
	movq	(%rbx), %rdi
	movq	%r14, %r9
	movq	%r12, %rcx
	movq	-336(%rbp), %rdx
	xorl	%esi, %esi
	movl	%edx, %r8d
	movq	%rax, %rdx
	subl	%r15d, %r8d
	call	utext_replace_67@PLT
	jmp	.L3287
	.p2align 4,,10
	.p2align 3
.L3340:
	movl	$1, %edx
	xorl	%eax, %eax
	jmp	.L3307
	.p2align 4,,10
	.p2align 3
.L3311:
	movq	%r14, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	utext_openUChars_67@PLT
	movq	%rax, %r13
	jmp	.L3312
	.p2align 4,,10
	.p2align 3
.L3339:
	movl	%ebx, -356(%rbp)
	.p2align 4,,10
	.p2align 3
.L3302:
	movl	-356(%rbp), %eax
	movq	120(%r15), %r12
	leal	1(%rax), %ebx
	cmpq	%r12, -376(%rbp)
	je	.L3369
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L3343
	cmpl	%ebx, -428(%rbp)
	jg	.L3288
.L3334:
	movl	-360(%rbp), %eax
	movl	%eax, -328(%rbp)
	cmpq	-376(%rbp), %r12
	jle	.L3287
	movq	-344(%rbp), %rbx
	cmpq	$0, 32(%rbx)
	jne	.L3290
	movq	48(%r15), %rax
	cmpq	16(%rbx), %rax
	je	.L3370
.L3290:
	movq	-376(%rbp), %rsi
	movq	%r12, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	-344(%rbp), %rdi
	leaq	-308(%rbp), %r9
	movl	$0, -308(%rbp)
	call	utext_extract_67@PLT
	leal	1(%rax), %r13d
	movl	%eax, %ebx
	movslq	%r13d, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3371
	movq	-344(%rbp), %rdi
	movl	%r13d, %r8d
	movq	%r14, %r9
	movq	%rax, %rcx
	movq	120(%r15), %rdx
	movq	-376(%rbp), %rsi
	call	utext_extract_67@PLT
	movslq	-428(%rbp), %rax
	movq	-352(%rbp), %rdi
	leaq	(%rdi,%rax,8), %r13
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L3293
	movq	%rax, %rdi
	call	utext_nativeLength_67@PLT
	movq	0(%r13), %rdi
	movq	%r14, %r9
	movl	%ebx, %r8d
	movq	%rax, %rdx
	movq	%r12, %rcx
	xorl	%esi, %esi
	call	utext_replace_67@PLT
.L3294:
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	movl	-360(%rbp), %eax
	movl	%eax, -328(%rbp)
	jmp	.L3287
	.p2align 4,,10
	.p2align 3
.L3368:
	movq	-160(%rbp), %rcx
	movq	%r13, %rdi
	movq	%rcx, -392(%rbp)
	call	utext_nativeLength_67@PLT
	movq	%r13, %rdi
	movq	%r14, %r9
	xorl	%esi, %esi
	movl	-384(%rbp), %r8d
	movq	-392(%rbp), %rcx
	movq	%rax, %rdx
	call	utext_replace_67@PLT
	movq	-160(%rbp), %rdi
	movzbl	-148(%rbp), %edx
	jmp	.L3318
	.p2align 4,,10
	.p2align 3
.L3367:
	movslq	%r8d, %rdi
	movq	%r10, -448(%rbp)
	addq	%rdi, %rdi
	movq	%rdx, -408(%rbp)
	movq	%rsi, -400(%rbp)
	movl	%r8d, -392(%rbp)
	call	uprv_malloc_67@PLT
	movl	-392(%rbp), %r8d
	movq	-400(%rbp), %rsi
	testq	%rax, %rax
	movq	-408(%rbp), %rdx
	movq	-448(%rbp), %r10
	movq	%rax, %rcx
	je	.L3315
	cmpb	$0, -148(%rbp)
	jne	.L3372
.L3316:
	movq	%rcx, -160(%rbp)
	movl	%r8d, -152(%rbp)
	movb	$1, -148(%rbp)
	jmp	.L3314
.L3364:
	movl	$8, (%r14)
.L3309:
	movq	-352(%rbp), %rax
	leaq	(%rax,%r12,8), %rdi
	movq	(%rax,%r12,8), %r13
	movq	%rdi, -328(%rbp)
	jmp	.L3312
	.p2align 4,,10
	.p2align 3
.L3332:
	movq	%r8, %rax
	movslq	%r12d, %rdx
	leaq	-304(%rbp), %rdi
	movq	%r13, %rsi
	leaq	-304(%rbp), %r12
	movl	$18, %ecx
	rep stosq
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	$878368812, -304(%rbp)
	movl	$144, -292(%rbp)
	call	utext_openUChars_67@PLT
	xorl	%edi, %edi
	movq	%r14, %r8
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r12, %rsi
	call	utext_clone_67@PLT
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	call	utext_close_67@PLT
	jmp	.L3333
	.p2align 4,,10
	.p2align 3
.L3319:
	testb	%dl, %dl
	jne	.L3373
	testl	%r8d, %r8d
	jle	.L3321
	cmpl	%r8d, -152(%rbp)
	movl	%r8d, %edx
	cmovle	-152(%rbp), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -392(%rbp)
	call	uprv_malloc_67@PLT
	movq	-392(%rbp), %rdx
	testq	%rax, %rax
	je	.L3374
	movq	-160(%rbp), %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movb	$0, -148(%rbp)
	movl	$40, -152(%rbp)
	movq	%rax, %r8
	movq	-416(%rbp), %rax
	movq	%rax, -160(%rbp)
.L3322:
	movslq	-384(%rbp), %rdx
	movq	%r8, %rsi
	xorl	%edi, %edi
	movq	%r14, %rcx
	movq	%r8, -384(%rbp)
	call	utext_openUChars_67@PLT
	movl	(%r14), %edx
	movq	-384(%rbp), %r8
	testl	%edx, %edx
	jg	.L3375
	orl	$32, 8(%rax)
	movq	-160(%rbp), %rdi
	movq	%rax, %r13
	movzbl	-148(%rbp), %edx
	jmp	.L3318
	.p2align 4,,10
	.p2align 3
.L3300:
	movq	-424(%rbp), %rdi
	movslq	%r12d, %rdx
	movq	%r8, %rax
	movq	%r13, %rsi
	movq	-424(%rbp), %r12
	movl	$18, %ecx
	rep stosq
	movq	%r14, %rcx
	movl	$878368812, -304(%rbp)
	movl	$144, -292(%rbp)
	movq	%r12, %rdi
	call	utext_openUChars_67@PLT
	xorl	%edi, %edi
	movq	%r14, %r8
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r12, %rsi
	call	utext_clone_67@PLT
	movq	-328(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, (%r10)
	call	utext_close_67@PLT
	jmp	.L3301
	.p2align 4,,10
	.p2align 3
.L3361:
	movslq	28(%rdi), %rcx
	movq	%rdi, %rsi
	cmpq	%rcx, %rax
	jne	.L3296
	movq	-352(%rbp), %rdi
	movslq	%ebx, %rax
	leaq	(%rdi,%rax,8), %r12
	movq	48(%rsi), %rax
	movq	-376(%rbp), %rdi
	movq	(%r12), %r8
	leaq	(%rax,%rdi,2), %r13
	testq	%r8, %r8
	je	.L3297
	movq	%r8, %rdi
	movq	%rdx, -328(%rbp)
	call	utext_nativeLength_67@PLT
	movq	(%r12), %rdi
	movq	%r14, %r9
	movq	%r13, %rcx
	movq	-328(%rbp), %rdx
	xorl	%esi, %esi
	movl	%edx, %r8d
	movq	%rax, %rdx
	subl	-376(%rbp), %r8d
	call	utext_replace_67@PLT
	jmp	.L3298
	.p2align 4,,10
	.p2align 3
.L3373:
	movq	-160(%rbp), %r8
	movq	-416(%rbp), %rax
	movl	$40, -152(%rbp)
	movb	$0, -148(%rbp)
	movq	%rax, -160(%rbp)
	testq	%r8, %r8
	jne	.L3322
.L3321:
	movl	$7, (%r14)
	jmp	.L3312
	.p2align 4,,10
	.p2align 3
.L3372:
	movq	-160(%rbp), %rdi
	movl	%r8d, -432(%rbp)
	movq	%rdx, -400(%rbp)
	movq	%rsi, -392(%rbp)
	movq	%rax, -408(%rbp)
	call	uprv_free_67@PLT
	movl	-432(%rbp), %r8d
	movq	-448(%rbp), %r10
	movq	-408(%rbp), %rcx
	movq	-400(%rbp), %rdx
	movq	-392(%rbp), %rsi
	jmp	.L3316
.L3315:
	movl	$7, (%r14)
	movq	-160(%rbp), %rcx
	jmp	.L3314
	.p2align 4,,10
	.p2align 3
.L3365:
	movl	%ecx, (%r14)
	jmp	.L3309
.L3363:
	movl	$66306, (%r14)
	jmp	.L3309
.L3375:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movq	-160(%rbp), %rdi
	movzbl	-148(%rbp), %edx
	jmp	.L3318
.L3330:
	leaq	-304(%rbp), %r13
	movq	%r8, %rax
	movl	$18, %ecx
	movq	%r12, %rsi
	leaq	-304(%rbp), %rdi
	subq	-376(%rbp), %rdx
	rep stosq
	movq	%r14, %rcx
	movq	%r13, %rdi
	movl	$878368812, -304(%rbp)
	movl	$144, -292(%rbp)
	call	utext_openUChars_67@PLT
	xorl	%edi, %edi
	movq	%r14, %r8
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r13, %rsi
	call	utext_clone_67@PLT
	movq	%r13, %rdi
	movq	%rax, (%rbx)
	call	utext_close_67@PLT
	jmp	.L3287
.L3356:
	movq	$0, -376(%rbp)
	movq	120(%rdi), %r12
	jmp	.L3334
.L3297:
	movq	-424(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r8, %rax
	movl	$18, %ecx
	movq	-424(%rbp), %r13
	subq	-376(%rbp), %rdx
	rep stosq
	movq	%r14, %rcx
	movl	$878368812, -304(%rbp)
	movl	$144, -292(%rbp)
	movq	%r13, %rdi
	call	utext_openUChars_67@PLT
	xorl	%edi, %edi
	movq	%r14, %r8
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r13, %rsi
	call	utext_clone_67@PLT
	movq	%r13, %rdi
	movq	%rax, (%r12)
	call	utext_close_67@PLT
	jmp	.L3298
.L3343:
	movl	%ebx, -328(%rbp)
	jmp	.L3287
.L3369:
	movl	%ebx, -328(%rbp)
	cmpl	%ebx, -360(%rbp)
	jle	.L3287
	movq	-352(%rbp), %rax
	movslq	%ebx, %r13
	leaq	(%rax,%r13,8), %rbx
	movl	-356(%rbp), %eax
	movq	(%rbx), %rdi
	addl	$2, %eax
	movl	%eax, -328(%rbp)
	testq	%rdi, %rdi
	je	.L3376
	call	utext_nativeLength_67@PLT
	movq	(%rbx), %rdi
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movq	%rax, %rdx
	leaq	_ZZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCodeE11emptyString(%rip), %rcx
	xorl	%esi, %esi
	call	utext_replace_67@PLT
	jmp	.L3287
.L3293:
	leaq	-304(%rbp), %r15
	leaq	-304(%rbp), %rdi
	movslq	%ebx, %rdx
	movq	%r12, %rsi
	movl	$18, %ecx
	rep stosq
	movq	%r14, %rcx
	movq	%r15, %rdi
	movl	$878368812, -304(%rbp)
	movl	$144, -292(%rbp)
	call	utext_openUChars_67@PLT
	xorl	%edi, %edi
	movq	%r14, %r8
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r15, %rsi
	call	utext_clone_67@PLT
	movq	%r15, %rdi
	movq	%rax, 0(%r13)
	call	utext_close_67@PLT
	jmp	.L3294
.L3370:
	movslq	28(%rbx), %rdx
	cmpq	%rdx, %rax
	jne	.L3290
	movslq	-428(%rbp), %rax
	movq	-352(%rbp), %rbx
	movq	-376(%rbp), %r13
	leaq	(%rbx,%rax,8), %rbx
	movq	-344(%rbp), %rax
	movq	(%rbx), %rdi
	movq	48(%rax), %rax
	leaq	(%rax,%r13,2), %r15
	testq	%rdi, %rdi
	je	.L3291
	call	utext_nativeLength_67@PLT
	movl	%r12d, %r8d
	movq	(%rbx), %rdi
	movq	%r14, %r9
	movq	%rax, %rdx
	subl	%r13d, %r8d
	movq	%r15, %rcx
	xorl	%esi, %esi
	call	utext_replace_67@PLT
	jmp	.L3287
.L3374:
	movl	$7, (%r14)
	movq	-160(%rbp), %rdi
	movzbl	-148(%rbp), %edx
	jmp	.L3318
.L3376:
	movq	%r14, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	utext_openUChars_67@PLT
	movq	%rax, (%rbx)
	jmp	.L3287
.L3360:
	call	__stack_chk_fail@PLT
.L3359:
	movl	$7, (%r14)
	jmp	.L3287
.L3291:
	xorl	%eax, %eax
	leaq	-304(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	leaq	-304(%rbp), %r12
	movl	$36, %ecx
	subq	-376(%rbp), %rdx
	rep stosl
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	$878368812, -304(%rbp)
	movl	$144, -292(%rbp)
	call	utext_openUChars_67@PLT
	xorl	%edi, %edi
	movq	%r14, %r8
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r12, %rsi
	call	utext_clone_67@PLT
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	call	utext_close_67@PLT
	jmp	.L3287
.L3371:
	movl	-360(%rbp), %eax
	movl	$7, (%r14)
	movl	%eax, -328(%rbp)
	jmp	.L3287
.L3362:
	leal	1(%rbx), %eax
	movl	$7, (%r14)
	movl	%eax, -328(%rbp)
	jmp	.L3287
	.cfi_endproc
.LFE4507:
	.size	_ZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCode.part.0, .-_ZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCode, @function
_ZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCode:
.LFB3297:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L3403
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	testl	%ecx, %ecx
	jle	.L3406
	movq	%rdi, %r12
	movq	32(%rdi), %rdi
	movq	%rsi, %r15
	movq	%rdx, %r14
	cmpq	%rdi, %rsi
	je	.L3382
	leaq	316(%r12), %r8
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r8, -56(%rbp)
	call	utext_clone_67@PLT
	movq	8(%r12), %rdx
	movq	-56(%rbp), %r8
	movq	%rax, 32(%r12)
	cmpb	$0, 184(%rdx)
	jne	.L3407
.L3383:
	movl	316(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L3384
	movq	120(%r12), %rax
	jmp	.L3385
	.p2align 4,,10
	.p2align 3
.L3406:
	movl	$1, (%r8)
.L3377:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3384:
	.cfi_restore_state
	movq	32(%r12), %rdi
	movq	%r8, -56(%rbp)
	call	utext_nativeLength_67@PLT
	movq	24(%r12), %rdi
	movq	-56(%rbp), %r8
	movq	%rax, 48(%r12)
	testq	%rdi, %rdi
	je	.L3386
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-56(%rbp), %r8
.L3386:
	movq	320(%r12), %rdi
	movq	$0, 24(%r12)
	testq	%rdi, %rdi
	je	.L3387
	movq	(%rdi), %rax
	movq	%r8, -56(%rbp)
	movq	%r8, %rdx
	movq	%r15, %rsi
	call	*64(%rax)
	movq	-56(%rbp), %r8
.L3387:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3382
	movq	(%rdi), %rax
	movq	32(%r12), %rsi
	movq	%r8, %rdx
	call	*64(%rax)
.L3382:
	movq	48(%r12), %rax
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movabsq	$42949672960000, %rcx
	movups	%xmm0, 136(%r12)
	movdqa	.LC0(%rip), %xmm0
	movq	$0, 64(%r12)
	movq	%rax, 72(%r12)
	movq	$0, 112(%r12)
	movq	%rax, 120(%r12)
	movq	$0, 80(%r12)
	movq	%rax, 88(%r12)
	movq	$0, 96(%r12)
	movq	%rax, 104(%r12)
	movb	$0, 130(%r12)
	movw	%dx, 168(%r12)
	movq	%rcx, 268(%r12)
	movb	$0, 312(%r12)
	movups	%xmm0, 152(%r12)
.L3385:
	testq	%rax, %rax
	je	.L3377
	addq	$24, %rsp
	movq	%rbx, %r8
	movl	%r13d, %ecx
	movq	%r14, %rdx
	popq	%rbx
	.cfi_restore 3
	movq	%r15, %rsi
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L3403:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3407:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	40(%r12), %rdi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	utext_clone_67@PLT
	movq	-56(%rbp), %r8
	movq	%rax, 40(%r12)
	jmp	.L3383
	.cfi_endproc
.LFE3297:
	.size	_ZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCode, .-_ZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode, @function
_ZN6icu_6712RegexMatcher5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode:
.LFB3296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movq	%r8, %rdx
	subq	$216, %rsp
	movq	%rdi, -240(%rbp)
	leaq	-208(%rbp), %rdi
	movl	%ecx, -220(%rbp)
	movl	$18, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	rep stosq
	movq	%r15, %rdi
	movl	$878368812, -208(%rbp)
	movl	$144, -196(%rbp)
	call	utext_openConstUnicodeString_67@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L3429
.L3408:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3430
	addq	$216, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3429:
	.cfi_restore_state
	movslq	-220(%rbp), %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -232(%rbp)
	testq	%rax, %rax
	je	.L3431
	movl	-220(%rbp), %eax
	testl	%eax, %eax
	jle	.L3411
	subl	$1, %eax
	movq	%rbx, %r13
	movq	-232(%rbp), %r14
	movq	%rax, -248(%rbp)
	salq	$6, %rax
	leaq	64(%r13,%rax), %rax
	movq	%r14, %rbx
	movq	%rax, -216(%rbp)
	.p2align 4,,10
	.p2align 3
.L3413:
	movq	%r13, %rsi
	movq	%r12, %rdx
	xorl	%edi, %edi
	addq	$64, %r13
	call	utext_openUnicodeString_67@PLT
	addq	$8, %r14
	movq	%rax, -8(%r14)
	cmpq	-216(%rbp), %r13
	jne	.L3413
	movl	(%r12), %eax
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jg	.L3417
	movq	-240(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712RegexMatcher5resetEP5UText
	cmpq	$0, 120(%r14)
	je	.L3417
	movl	-220(%rbp), %ecx
	movq	%r12, %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	-232(%rbp), %rdx
	call	_ZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCode.part.0
	movl	%eax, %r13d
.L3417:
	movq	-232(%rbp), %rax
	movq	-248(%rbp), %rcx
	leaq	8(%rax,%rcx,8), %r12
	.p2align 4,,10
	.p2align 3
.L3416:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	utext_close_67@PLT
	cmpq	%r12, %rbx
	jne	.L3416
.L3414:
	movq	-232(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	%r15, %rdi
	call	utext_close_67@PLT
	jmp	.L3408
	.p2align 4,,10
	.p2align 3
.L3411:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L3414
	movl	$1, (%r12)
	jmp	.L3414
.L3430:
	call	__stack_chk_fail@PLT
.L3431:
	movl	$7, (%r12)
	jmp	.L3408
	.cfi_endproc
.LFE3296:
	.size	_ZN6icu_6712RegexMatcher5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode, .-_ZN6icu_6712RegexMatcher5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher4findElR10UErrorCode
	.type	_ZN6icu_6712RegexMatcher4findElR10UErrorCode, @function
_ZN6icu_6712RegexMatcher4findElR10UErrorCode:
.LFB3257:
	.cfi_startproc
	endbr64
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L3432
	movl	316(%rdi), %eax
	testl	%eax, %eax
	jg	.L3439
	pxor	%xmm0, %xmm0
	movq	48(%rdi), %rax
	xorl	%ecx, %ecx
	movq	$0, 64(%rdi)
	movups	%xmm0, 136(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movw	%cx, 168(%rdi)
	movabsq	$42949672960000, %rcx
	movq	%rax, 72(%rdi)
	movq	$0, 112(%rdi)
	movq	%rax, 120(%rdi)
	movq	$0, 80(%rdi)
	movq	%rax, 88(%rdi)
	movq	$0, 96(%rdi)
	movq	%rax, 104(%rdi)
	movb	$0, 130(%rdi)
	movq	%rcx, 268(%rdi)
	movups	%xmm0, 152(%rdi)
	testq	%rsi, %rsi
	js	.L3437
	cmpq	%rax, %rsi
	jg	.L3437
	movl	(%rdx), %eax
	movq	%rsi, 144(%rdi)
	testl	%eax, %eax
	jg	.L3432
	movq	%rdx, %rsi
	jmp	_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L3439:
	movl	%eax, (%rdx)
.L3432:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3437:
	movl	$8, (%rdx)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3257:
	.size	_ZN6icu_6712RegexMatcher4findElR10UErrorCode, .-_ZN6icu_6712RegexMatcher4findElR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher4findEv
	.type	_ZN6icu_6712RegexMatcher4findEv, @function
_ZN6icu_6712RegexMatcher4findEv:
.LFB3255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	316(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L3440
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0
.L3440:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L3445
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3445:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3255:
	.size	_ZN6icu_6712RegexMatcher4findEv, .-_ZN6icu_6712RegexMatcher4findEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher12replaceFirstERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712RegexMatcher12replaceFirstERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712RegexMatcher12replaceFirstERKNS_13UnicodeStringER10UErrorCode:
.LFB3285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	movl	$18, %ecx
	leaq	-352(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-352(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	movq	%r14, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-208(%rbp), %rbx
	subq	$328, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	rep stosq
	leaq	-208(%rbp), %rdi
	movl	$18, %ecx
	movl	$878368812, -352(%rbp)
	rep stosq
	movl	$2, %edi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$144, -340(%rbp)
	movw	%di, 8(%r13)
	movq	%r15, %rdi
	movq	%rax, 0(%r13)
	movl	$878368812, -208(%rbp)
	movl	$144, -196(%rbp)
	call	utext_openConstUnicodeString_67@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	utext_openUnicodeString_67@PLT
	movl	(%r14), %r8d
	testl	%r8d, %r8d
	jg	.L3449
	movl	316(%r12), %eax
	testl	%eax, %eax
	jle	.L3448
.L3456:
	movl	%eax, (%r14)
.L3449:
	movq	%rbx, %rdi
	call	utext_close_67@PLT
	movq	%r15, %rdi
	call	utext_close_67@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3457
	addq	$328, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3448:
	.cfi_restore_state
	movq	48(%r12), %rax
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movups	%xmm0, 136(%r12)
	leaq	-356(%rbp), %rsi
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, 72(%r12)
	movq	%rax, 120(%r12)
	movq	%rax, 88(%r12)
	movq	%rax, 104(%r12)
	movabsq	$42949672960000, %rax
	movq	$0, 64(%r12)
	movq	$0, 112(%r12)
	movq	$0, 80(%r12)
	movq	$0, 96(%r12)
	movb	$0, 130(%r12)
	movw	%cx, 168(%r12)
	movq	%rax, 268(%r12)
	movups	%xmm0, 152(%r12)
	movl	$0, -356(%rbp)
	call	_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0
	testb	%al, %al
	je	.L3458
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L3449
	movl	316(%r12), %eax
	testl	%eax, %eax
	jg	.L3456
	cmpb	$0, 130(%r12)
	jne	.L3452
	movl	$66306, (%r14)
	jmp	.L3449
	.p2align 4,,10
	.p2align 3
.L3458:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6712RegexMatcher8getInputEP5UTextR10UErrorCode
	jmp	.L3449
	.p2align 4,,10
	.p2align 3
.L3452:
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode.part.0
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L3449
	movl	316(%r12), %eax
	testl	%eax, %eax
	jg	.L3456
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode.part.0
	jmp	.L3449
.L3457:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3285:
	.size	_ZN6icu_6712RegexMatcher12replaceFirstERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712RegexMatcher12replaceFirstERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher10replaceAllERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712RegexMatcher10replaceAllERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712RegexMatcher10replaceAllERKNS_13UnicodeStringER10UErrorCode:
.LFB3283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movl	$18, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-352(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$344, %rsp
	movl	0(%r13), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	rep stosq
	movl	$18, %ecx
	movq	%r15, %rdi
	movl	$878368812, -352(%rbp)
	rep stosq
	movl	$2, %edi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$144, -340(%rbp)
	movl	$878368812, -208(%rbp)
	movl	$144, -196(%rbp)
	movq	%rax, (%r12)
	movw	%di, 8(%r12)
	testl	%r8d, %r8d
	jle	.L3476
.L3459:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3477
	addq	$344, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3476:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, %r14
	movq	%rdx, %rsi
	movq	%r13, %rdx
	call	utext_openConstUnicodeString_67@PLT
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	utext_openUnicodeString_67@PLT
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L3463
	movl	316(%r14), %eax
	testl	%eax, %eax
	jg	.L3465
	movq	48(%r14), %rax
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	$0, 64(%r14)
	movups	%xmm0, 136(%r14)
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, 72(%r14)
	movq	%rax, 120(%r14)
	movq	%rax, 88(%r14)
	movq	%rax, 104(%r14)
	movabsq	$42949672960000, %rax
	movq	%rax, 268(%r14)
	leaq	-356(%rbp), %rax
	movq	$0, 112(%r14)
	movq	$0, 80(%r14)
	movq	$0, 96(%r14)
	movb	$0, 130(%r14)
	movw	%cx, 168(%r14)
	movq	%rax, -376(%rbp)
	movups	%xmm0, 152(%r14)
.L3464:
	movq	-376(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -356(%rbp)
	call	_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0
	testb	%al, %al
	je	.L3466
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L3463
	movl	316(%r14), %eax
	testl	%eax, %eax
	jg	.L3465
	cmpb	$0, 130(%r14)
	jne	.L3469
	movl	$66306, 0(%r13)
	.p2align 4,,10
	.p2align 3
.L3463:
	movq	%r15, %rdi
	call	utext_close_67@PLT
	movq	%rbx, %rdi
	call	utext_close_67@PLT
	jmp	.L3459
.L3469:
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode.part.0
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L3463
	movl	316(%r14), %eax
	testl	%eax, %eax
	jle	.L3464
.L3465:
	movl	%eax, 0(%r13)
	jmp	.L3463
.L3466:
	cmpl	$0, 0(%r13)
	jg	.L3463
	movl	316(%r14), %eax
	testl	%eax, %eax
	jg	.L3465
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode.part.0
	jmp	.L3463
.L3477:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3283:
	.size	_ZN6icu_6712RegexMatcher10replaceAllERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712RegexMatcher10replaceAllERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher10replaceAllEP5UTextS2_R10UErrorCode
	.type	_ZN6icu_6712RegexMatcher10replaceAllEP5UTextS2_R10UErrorCode, @function
_ZN6icu_6712RegexMatcher10replaceAllEP5UTextS2_R10UErrorCode:
.LFB3284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r11d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r11d, %r11d
	jg	.L3479
	movl	316(%rdi), %eax
	movq	%rdi, %r12
	movq	%rcx, %r13
	testl	%eax, %eax
	jg	.L3489
	movq	%rsi, %rbx
	testq	%rdx, %rdx
	je	.L3493
.L3481:
	movq	48(%r12), %rdx
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movabsq	$42949672960000, %rsi
	movups	%xmm0, 136(%r12)
	leaq	-272(%rbp), %r15
	movdqa	.LC0(%rip), %xmm0
	movq	$0, 64(%r12)
	movq	%rdx, 72(%r12)
	movq	$0, 112(%r12)
	movq	%rdx, 120(%r12)
	movq	$0, 80(%r12)
	movq	%rdx, 88(%r12)
	movq	$0, 96(%r12)
	movq	%rdx, 104(%r12)
	movb	$0, 130(%r12)
	movw	%r8w, 168(%r12)
	movq	%rsi, 268(%r12)
	movups	%xmm0, 152(%r12)
.L3487:
	testl	%eax, %eax
	jle	.L3494
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L3489
	.p2align 4,,10
	.p2align 3
.L3479:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3495
	addq	$248, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3489:
	.cfi_restore_state
	movl	%eax, 0(%r13)
	jmp	.L3479
	.p2align 4,,10
	.p2align 3
.L3494:
	movl	$0, -272(%rbp)
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0
	testb	%al, %al
	je	.L3483
	movl	0(%r13), %edi
	testl	%edi, %edi
	jg	.L3479
	movl	316(%r12), %eax
	testl	%eax, %eax
	jg	.L3489
	cmpb	$0, 130(%r12)
	jne	.L3486
	movl	$66306, 0(%r13)
	jmp	.L3479
	.p2align 4,,10
	.p2align 3
.L3493:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r9d
	leaq	-272(%rbp), %r15
	movl	$18, %ecx
	movq	%rax, -128(%rbp)
	leaq	-272(%rbp), %rdi
	movq	%rdx, %rax
	movq	%r13, %rdx
	rep stosq
	movw	%r9w, -120(%rbp)
	leaq	-128(%rbp), %r9
	movq	%r15, %rdi
	movq	%r9, %rsi
	movq	%r9, -280(%rbp)
	movl	$878368812, -272(%rbp)
	movl	$144, -260(%rbp)
	call	utext_openUnicodeString_67@PLT
	movq	%r13, %r8
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r15, %rsi
	xorl	%edi, %edi
	call	utext_clone_67@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	utext_close_67@PLT
	movq	-280(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	0(%r13), %r10d
	testl	%r10d, %r10d
	jg	.L3479
	movl	316(%r12), %eax
	jmp	.L3481
	.p2align 4,,10
	.p2align 3
.L3486:
	movq	%r14, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode.part.0
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L3479
	movl	316(%r12), %eax
	jmp	.L3487
.L3483:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L3479
	movl	316(%r12), %eax
	testl	%eax, %eax
	jg	.L3489
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode.part.0
	jmp	.L3479
.L3495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3284:
	.size	_ZN6icu_6712RegexMatcher10replaceAllEP5UTextS2_R10UErrorCode, .-_ZN6icu_6712RegexMatcher10replaceAllEP5UTextS2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712RegexMatcher12replaceFirstEP5UTextS2_R10UErrorCode
	.type	_ZN6icu_6712RegexMatcher12replaceFirstEP5UTextS2_R10UErrorCode, @function
_ZN6icu_6712RegexMatcher12replaceFirstEP5UTextS2_R10UErrorCode:
.LFB3286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L3497
	movl	316(%rdi), %eax
	movq	%rdi, %r12
	movq	%rcx, %r13
	testl	%eax, %eax
	jle	.L3498
.L3507:
	movl	%eax, 0(%r13)
.L3497:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3508
	addq	$248, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3498:
	.cfi_restore_state
	movq	48(%rdi), %rax
	pxor	%xmm0, %xmm0
	movq	%rsi, %r15
	xorl	%esi, %esi
	movw	%si, 168(%rdi)
	leaq	-272(%rbp), %rbx
	movq	%rax, 72(%rdi)
	movq	%rbx, %rsi
	movq	%rax, 120(%rdi)
	movq	%rax, 88(%rdi)
	movq	%rax, 104(%rdi)
	movabsq	$42949672960000, %rax
	movups	%xmm0, 136(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movq	$0, 64(%rdi)
	movq	$0, 112(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 96(%rdi)
	movb	$0, 130(%rdi)
	movq	%rax, 268(%rdi)
	movups	%xmm0, 152(%rdi)
	movl	$0, -272(%rbp)
	call	_ZN6icu_6712RegexMatcher4findER10UErrorCode.part.0
	testb	%al, %al
	je	.L3509
	testq	%r14, %r14
	je	.L3510
.L3500:
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L3497
	movl	316(%r12), %eax
	testl	%eax, %eax
	jg	.L3507
	cmpb	$0, 130(%r12)
	jne	.L3503
	movl	$66306, 0(%r13)
	jmp	.L3497
	.p2align 4,,10
	.p2align 3
.L3509:
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZNK6icu_6712RegexMatcher8getInputEP5UTextR10UErrorCode
	movq	%rax, %r14
	jmp	.L3497
	.p2align 4,,10
	.p2align 3
.L3503:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode.part.0
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L3497
	movl	316(%r12), %eax
	testl	%eax, %eax
	jg	.L3507
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode.part.0
	jmp	.L3497
	.p2align 4,,10
	.p2align 3
.L3510:
	movl	$2, %ecx
	leaq	-128(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r13, %rdx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, -120(%rbp)
	movl	$18, %ecx
	movq	%r9, %rsi
	movq	%rax, -128(%rbp)
	movq	%r14, %rax
	rep stosq
	movq	%rbx, %rdi
	movq	%r9, -280(%rbp)
	movl	$878368812, -272(%rbp)
	movl	$144, -260(%rbp)
	call	utext_openUnicodeString_67@PLT
	movq	%r13, %r8
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	call	utext_clone_67@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	utext_close_67@PLT
	movq	-280(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L3500
.L3508:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3286:
	.size	_ZN6icu_6712RegexMatcher12replaceFirstEP5UTextS2_R10UErrorCode, .-_ZN6icu_6712RegexMatcher12replaceFirstEP5UTextS2_R10UErrorCode
	.weak	_ZTSN6icu_6712RegexMatcherE
	.section	.rodata._ZTSN6icu_6712RegexMatcherE,"aG",@progbits,_ZTSN6icu_6712RegexMatcherE,comdat
	.align 16
	.type	_ZTSN6icu_6712RegexMatcherE, @object
	.size	_ZTSN6icu_6712RegexMatcherE, 24
_ZTSN6icu_6712RegexMatcherE:
	.string	"N6icu_6712RegexMatcherE"
	.weak	_ZTIN6icu_6712RegexMatcherE
	.section	.data.rel.ro._ZTIN6icu_6712RegexMatcherE,"awG",@progbits,_ZTIN6icu_6712RegexMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_6712RegexMatcherE, @object
	.size	_ZTIN6icu_6712RegexMatcherE, 24
_ZTIN6icu_6712RegexMatcherE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712RegexMatcherE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6712RegexMatcherE
	.section	.data.rel.ro.local._ZTVN6icu_6712RegexMatcherE,"awG",@progbits,_ZTVN6icu_6712RegexMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_6712RegexMatcherE, @object
	.size	_ZTVN6icu_6712RegexMatcherE, 512
_ZTVN6icu_6712RegexMatcherE:
	.quad	0
	.quad	_ZTIN6icu_6712RegexMatcherE
	.quad	_ZN6icu_6712RegexMatcherD1Ev
	.quad	_ZN6icu_6712RegexMatcherD0Ev
	.quad	_ZNK6icu_6712RegexMatcher17getDynamicClassIDEv
	.quad	_ZN6icu_6712RegexMatcher7matchesER10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher7matchesElR10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher9lookingAtER10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher9lookingAtElR10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher4findEv
	.quad	_ZN6icu_6712RegexMatcher4findER10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher4findElR10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher5groupER10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher5groupEiR10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher10groupCountEv
	.quad	_ZNK6icu_6712RegexMatcher5groupEP5UTextRlR10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher5groupEiP5UTextRlR10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher5startER10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher7start64ER10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher5startEiR10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher7start64EiR10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher3endER10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher5end64ER10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher3endEiR10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher5end64EiR10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher5resetEv
	.quad	_ZN6icu_6712RegexMatcher5resetElR10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher5resetERKNS_13UnicodeStringE
	.quad	_ZN6icu_6712RegexMatcher5resetEP5UText
	.quad	_ZN6icu_6712RegexMatcher16refreshInputTextEP5UTextR10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher5inputEv
	.quad	_ZNK6icu_6712RegexMatcher9inputTextEv
	.quad	_ZNK6icu_6712RegexMatcher8getInputEP5UTextR10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher6regionEllR10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher6regionElllR10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher11regionStartEv
	.quad	_ZNK6icu_6712RegexMatcher13regionStart64Ev
	.quad	_ZNK6icu_6712RegexMatcher9regionEndEv
	.quad	_ZNK6icu_6712RegexMatcher11regionEnd64Ev
	.quad	_ZNK6icu_6712RegexMatcher20hasTransparentBoundsEv
	.quad	_ZN6icu_6712RegexMatcher20useTransparentBoundsEa
	.quad	_ZNK6icu_6712RegexMatcher18hasAnchoringBoundsEv
	.quad	_ZN6icu_6712RegexMatcher18useAnchoringBoundsEa
	.quad	_ZNK6icu_6712RegexMatcher6hitEndEv
	.quad	_ZNK6icu_6712RegexMatcher10requireEndEv
	.quad	_ZNK6icu_6712RegexMatcher7patternEv
	.quad	_ZN6icu_6712RegexMatcher10replaceAllERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher10replaceAllEP5UTextS2_R10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher12replaceFirstERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher12replaceFirstEP5UTextS2_R10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher17appendReplacementERNS_13UnicodeStringERKS1_R10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher17appendReplacementEP5UTextS2_R10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher10appendTailERNS_13UnicodeStringE
	.quad	_ZN6icu_6712RegexMatcher10appendTailEP5UTextR10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher5splitERKNS_13UnicodeStringEPS1_iR10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher12setTimeLimitEiR10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher12getTimeLimitEv
	.quad	_ZN6icu_6712RegexMatcher13setStackLimitEiR10UErrorCode
	.quad	_ZNK6icu_6712RegexMatcher13getStackLimitEv
	.quad	_ZN6icu_6712RegexMatcher16setMatchCallbackEPFaPKviES2_R10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher16getMatchCallbackERPFaPKviERS2_R10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher23setFindProgressCallbackEPFaPKvlES2_R10UErrorCode
	.quad	_ZN6icu_6712RegexMatcher23getFindProgressCallbackERPFaPKvlERS2_R10UErrorCode
	.local	_ZZN6icu_6712RegexMatcher16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6712RegexMatcher16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.align 2
	.type	_ZZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCodeE11emptyString, @object
	.size	_ZZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCodeE11emptyString, 2
_ZZN6icu_6712RegexMatcher5splitEP5UTextPS2_iR10UErrorCodeE11emptyString:
	.zero	2
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	-1
	.quad	0
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	8000000
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
