	.file	"quantityformatter.cpp"
	.text
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3210:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3210:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3213:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L15
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3
	cmpb	$0, 12(%rbx)
	jne	.L16
.L7:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L7
	.cfi_endproc
.LFE3213:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3216:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L19
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3216:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3219:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L22
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3219:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L28
.L24:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L29
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3221:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3222:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3222:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3223:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3223:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3224:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3224:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3225:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3225:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3226:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3226:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3227:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L45
	testl	%edx, %edx
	jle	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L48
.L37:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L37
	.cfi_endproc
.LFE3227:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L52
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L52
	testl	%r12d, %r12d
	jg	.L59
	cmpb	$0, 12(%rbx)
	jne	.L60
.L54:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L54
.L60:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3228:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L62
	movq	(%rdi), %r8
.L63:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L66
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L66
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3229:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3230:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L73
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3230:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3231:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3231:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3232:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3232:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3233:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3233:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3235:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3235:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3237:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3237:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717QuantityFormatterC2Ev
	.type	_ZN6icu_6717QuantityFormatterC2Ev, @function
_ZN6icu_6717QuantityFormatterC2Ev:
.LFB2865:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE2865:
	.size	_ZN6icu_6717QuantityFormatterC2Ev, .-_ZN6icu_6717QuantityFormatterC2Ev
	.globl	_ZN6icu_6717QuantityFormatterC1Ev
	.set	_ZN6icu_6717QuantityFormatterC1Ev,_ZN6icu_6717QuantityFormatterC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717QuantityFormatterC2ERKS0_
	.type	_ZN6icu_6717QuantityFormatterC2ERKS0_, @function
_ZN6icu_6717QuantityFormatterC2ERKS0_:
.LFB2868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L83
	movq	(%r14,%rbx), %rax
	leaq	8(%r12), %rdi
	leaq	8(%rax), %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L83:
	movq	%r12, 0(%r13,%rbx)
	addq	$8, %rbx
	cmpq	$48, %rbx
	je	.L90
.L84:
	cmpq	$0, (%r14,%rbx)
	jne	.L81
	movq	$0, 0(%r13,%rbx)
	addq	$8, %rbx
	cmpq	$48, %rbx
	jne	.L84
.L90:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2868:
	.size	_ZN6icu_6717QuantityFormatterC2ERKS0_, .-_ZN6icu_6717QuantityFormatterC2ERKS0_
	.globl	_ZN6icu_6717QuantityFormatterC1ERKS0_
	.set	_ZN6icu_6717QuantityFormatterC1ERKS0_,_ZN6icu_6717QuantityFormatterC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717QuantityFormatteraSERKS0_
	.type	_ZN6icu_6717QuantityFormatteraSERKS0_, @function
_ZN6icu_6717QuantityFormatteraSERKS0_:
.LFB2870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	%rsi, %rdi
	je	.L92
	movq	%rsi, %r14
	xorl	%ebx, %ebx
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L96
	movq	(%r14,%rbx), %rax
	leaq	8(%r12), %rdi
	leaq	8(%rax), %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L96:
	movq	%r12, 0(%r13,%rbx)
	addq	$8, %rbx
	cmpq	$48, %rbx
	je	.L92
.L97:
	movq	0(%r13,%rbx), %r12
	testq	%r12, %r12
	je	.L93
	movq	%r12, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L93:
	cmpq	$0, (%r14,%rbx)
	jne	.L94
	movq	$0, 0(%r13,%rbx)
	addq	$8, %rbx
	cmpq	$48, %rbx
	jne	.L97
.L92:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2870:
	.size	_ZN6icu_6717QuantityFormatteraSERKS0_, .-_ZN6icu_6717QuantityFormatteraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717QuantityFormatterD2Ev
	.type	_ZN6icu_6717QuantityFormatterD2Ev, @function
_ZN6icu_6717QuantityFormatterD2Ev:
.LFB2872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	48(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L108:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L107
	movq	%r12, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L107:
	addq	$8, %rbx
	cmpq	%r13, %rbx
	jne	.L108
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2872:
	.size	_ZN6icu_6717QuantityFormatterD2Ev, .-_ZN6icu_6717QuantityFormatterD2Ev
	.globl	_ZN6icu_6717QuantityFormatterD1Ev
	.set	_ZN6icu_6717QuantityFormatterD1Ev,_ZN6icu_6717QuantityFormatterD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717QuantityFormatter5resetEv
	.type	_ZN6icu_6717QuantityFormatter5resetEv, @function
_ZN6icu_6717QuantityFormatter5resetEv:
.LFB2874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	48(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L118:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L115
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	$0, -8(%rbx)
	cmpq	%r13, %rbx
	jne	.L118
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%r13, %rbx
	jne	.L118
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2874:
	.size	_ZN6icu_6717QuantityFormatter5resetEv, .-_ZN6icu_6717QuantityFormatter5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717QuantityFormatter11addIfAbsentEPKcRKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6717QuantityFormatter11addIfAbsentEPKcRKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6717QuantityFormatter11addIfAbsentEPKcRKNS_13UnicodeStringER10UErrorCode:
.LFB2875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movq	%rcx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6714StandardPlural15indexFromStringEPKcR10UErrorCode@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L120
	cltq
	movl	$1, %r14d
	leaq	(%r12,%rax,8), %r15
	cmpq	$0, (%r15)
	je	.L127
.L120:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L122
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%edx, %edx
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	%rax, 8(%r12)
	movl	$2, %eax
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%ax, 16(%r12)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L128
	movq	%r12, (%r15)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L120
.L122:
	movl	$7, (%rbx)
	xorl	%r14d, %r14d
	jmp	.L120
	.cfi_endproc
.LFE2875:
	.size	_ZN6icu_6717QuantityFormatter11addIfAbsentEPKcRKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6717QuantityFormatter11addIfAbsentEPKcRKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717QuantityFormatter7isValidEv
	.type	_ZNK6icu_6717QuantityFormatter7isValidEv, @function
_ZNK6icu_6717QuantityFormatter7isValidEv:
.LFB2876:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE2876:
	.size	_ZNK6icu_6717QuantityFormatter7isValidEv, .-_ZNK6icu_6717QuantityFormatter7isValidEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717QuantityFormatter12getByVariantEPKc
	.type	_ZNK6icu_6717QuantityFormatter12getByVariantEPKc, @function
_ZNK6icu_6717QuantityFormatter12getByVariantEPKc:
.LFB2877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringEPKc@PLT
	movl	$5, %edx
	testl	%eax, %eax
	cmovs	%edx, %eax
	cltq
	movq	(%rbx,%rax,8), %rax
	testq	%rax, %rax
	je	.L134
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	40(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2877:
	.size	_ZNK6icu_6717QuantityFormatter12getByVariantEPKc, .-_ZNK6icu_6717QuantityFormatter12getByVariantEPKc
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717QuantityFormatter6formatERKNS_15SimpleFormatterERKNS_13UnicodeStringERS4_RNS_13FieldPositionER10UErrorCode
	.type	_ZN6icu_6717QuantityFormatter6formatERKNS_15SimpleFormatterERKNS_13UnicodeStringERS4_RNS_13FieldPositionER10UErrorCode, @function
_ZN6icu_6717QuantityFormatter6formatERKNS_15SimpleFormatterERKNS_13UnicodeStringERS4_RNS_13FieldPositionER10UErrorCode:
.LFB2881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L143
.L137:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L144
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	subq	$8, %rsp
	movq	%rsi, -32(%rbp)
	movq	%rcx, %rbx
	leaq	-32(%rbp), %rsi
	pushq	%r8
	movq	%rdx, %rcx
	movl	$1, %r9d
	leaq	-36(%rbp), %r8
	movl	$1, %edx
	call	_ZNK6icu_6715SimpleFormatter15formatAndAppendEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	movl	12(%rbx), %eax
	popq	%rcx
	popq	%rsi
	testl	%eax, %eax
	je	.L145
.L138:
	movl	-36(%rbp), %edx
	testl	%edx, %edx
	js	.L140
	addl	%edx, %eax
	addl	%edx, 16(%rbx)
	movl	%eax, 12(%rbx)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L145:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	je	.L137
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L140:
	movq	$0, 12(%rbx)
	jmp	.L137
.L144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2881:
	.size	_ZN6icu_6717QuantityFormatter6formatERKNS_15SimpleFormatterERKNS_13UnicodeStringERS4_RNS_13FieldPositionER10UErrorCode, .-_ZN6icu_6717QuantityFormatter6formatERKNS_15SimpleFormatterERKNS_13UnicodeStringERS4_RNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717QuantityFormatter12selectPluralERKNS_11FormattableERKNS_12NumberFormatERKNS_11PluralRulesERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZN6icu_6717QuantityFormatter12selectPluralERKNS_11FormattableERKNS_12NumberFormatERKNS_11PluralRulesERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZN6icu_6717QuantityFormatter12selectPluralERKNS_11FormattableERKNS_12NumberFormatERKNS_11PluralRulesERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB2879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	$5, %r12d
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -280(%rbp)
	movl	(%r9), %ecx
	movq	%r8, -288(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L146
	movq	%rsi, %r14
	movq	%rdx, %r15
	movl	$2, %edx
	xorl	%ecx, %ecx
	movw	%dx, -184(%rbp)
	movq	%rdi, %r13
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -192(%rbp)
	movq	%r9, %rbx
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	movq	%rax, -296(%rbp)
	je	.L148
	leaq	-272(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	-296(%rbp), %r9
	movq	%r9, %rdi
	call	_ZNK6icu_6713DecimalFormat23formatToDecimalQuantityERKNS_11FormattableERNS_6number4impl15DecimalQuantityER10UErrorCode@PLT
	movl	(%rbx), %eax
	movq	-296(%rbp), %r9
	testl	%eax, %eax
	jg	.L162
	leaq	-128(%rbp), %r12
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -296(%rbp)
	movq	%r12, %rdi
	leaq	-192(%rbp), %r15
	call	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-296(%rbp), %r9
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	-288(%rbp), %rcx
	movq	-280(%rbp), %rdx
	movq	(%r9), %rax
	movq	%r9, %rdi
	call	*40(%rax)
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
.L151:
	movq	%r15, %rdi
	movl	$5, %r12d
	call	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringERKNS_13UnicodeStringE@PLT
	testl	%eax, %eax
	cmovns	%eax, %r12d
.L150:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L146:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	addq	$264, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movq	%r14, %rdi
	leaq	-192(%rbp), %r15
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%r13, %rdi
	call	_ZNK6icu_6711Formattable7getTypeEv@PLT
	cmpl	$1, %eax
	je	.L164
	movq	%r13, %rdi
	call	_ZNK6icu_6711Formattable7getTypeEv@PLT
	cmpl	$2, %eax
	je	.L165
	movq	%r13, %rdi
	call	_ZNK6icu_6711Formattable7getTypeEv@PLT
	cmpl	$5, %eax
	je	.L166
	movl	$1, (%rbx)
	leaq	-192(%rbp), %r15
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L164:
	movsd	8(%r13), %xmm0
	leaq	-128(%rbp), %r12
.L161:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6711PluralRules6selectEd@PLT
.L160:
	leaq	-192(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r14), %rax
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	-288(%rbp), %rcx
	movq	-280(%rbp), %rdx
	movq	%r14, %rdi
	call	*40(%rax)
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L165:
	leaq	-128(%rbp), %r12
	movl	8(%r13), %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6711PluralRules6selectEi@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L166:
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %r12
	cvtsi2sdq	8(%r13), %xmm0
	jmp	.L161
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2879:
	.size	_ZN6icu_6717QuantityFormatter12selectPluralERKNS_11FormattableERKNS_12NumberFormatERKNS_11PluralRulesERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZN6icu_6717QuantityFormatter12selectPluralERKNS_11FormattableERKNS_12NumberFormatERKNS_11PluralRulesERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717QuantityFormatter6formatERKNS_11FormattableERKNS_12NumberFormatERKNS_11PluralRulesERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6717QuantityFormatter6formatERKNS_11FormattableERKNS_12NumberFormatERKNS_11PluralRulesERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6717QuantityFormatter6formatERKNS_11FormattableERKNS_12NumberFormatERKNS_11PluralRulesERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB2878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	movl	$2, %r8d
	movq	%rcx, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	movq	%r14, %rcx
	subq	$104, %rsp
	movq	16(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%r8w, -120(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rbx, %r8
	movq	%r15, %r9
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6717QuantityFormatter12selectPluralERKNS_11FormattableERKNS_12NumberFormatERKNS_11PluralRulesERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	movl	(%r15), %r9d
	testl	%r9d, %r9d
	jg	.L169
	cltq
	movq	0(%r13,%rax,8), %rdi
	testq	%rdi, %rdi
	je	.L176
.L170:
	subq	$8, %rsp
	leaq	-136(%rbp), %rsi
	movq	%r12, %rcx
	movl	$1, %r9d
	pushq	%r15
	leaq	-140(%rbp), %r8
	movl	$1, %edx
	movq	%r14, -136(%rbp)
	call	_ZNK6icu_6715SimpleFormatter15formatAndAppendEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode@PLT
	movl	12(%rbx), %eax
	popq	%rcx
	popq	%rsi
	testl	%eax, %eax
	je	.L177
	movl	-140(%rbp), %edx
	testl	%edx, %edx
	js	.L173
.L179:
	addl	%edx, %eax
	addl	%edx, 16(%rbx)
	movl	%eax, 12(%rbx)
.L169:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L178
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	40(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L170
	movl	$27, (%r15)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L177:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	je	.L169
	movl	-140(%rbp), %edx
	testl	%edx, %edx
	jns	.L179
.L173:
	movq	$0, 12(%rbx)
	jmp	.L169
.L178:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2878:
	.size	_ZNK6icu_6717QuantityFormatter6formatERKNS_11FormattableERKNS_12NumberFormatERKNS_11PluralRulesERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6717QuantityFormatter6formatERKNS_11FormattableERKNS_12NumberFormatERKNS_11PluralRulesERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717QuantityFormatter15formatAndSelectEdRKNS_12NumberFormatERKNS_11PluralRulesERNS_22FormattedStringBuilderERNS_14StandardPlural4FormER10UErrorCode
	.type	_ZN6icu_6717QuantityFormatter15formatAndSelectEdRKNS_12NumberFormatERKNS_11PluralRulesERNS_22FormattedStringBuilderERNS_14StandardPlural4FormER10UErrorCode, @function
_ZN6icu_6717QuantityFormatter15formatAndSelectEdRKNS_12NumberFormatERKNS_11PluralRulesERNS_22FormattedStringBuilderERNS_14StandardPlural4FormER10UErrorCode:
.LFB2880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	movl	$2, %r8d
	subq	$408, %rsp
	movq	%rsi, -440(%rbp)
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	movq	%rcx, -432(%rbp)
	xorl	%ecx, %ecx
	movsd	%xmm0, -424(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -416(%rbp)
	movw	%r8w, -408(%rbp)
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L181
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	leaq	-288(%rbp), %r13
	leaq	-136(%rbp), %r15
	movq	%rax, %r14
	movq	%r13, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movsd	-424(%rbp), %xmm0
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZNK6icu_6713DecimalFormat17toNumberFormatterER10UErrorCode@PLT
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.L190
.L182:
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
.L184:
	leaq	-416(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L180:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L191
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L182
	movq	%r12, %rdi
	leaq	-280(%rbp), %rsi
	leaq	-352(%rbp), %rbx
	call	_ZN6icu_6722FormattedStringBuilderaSERKS0_@PLT
	movq	-440(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	leaq	-416(%rbp), %r12
	call	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
.L183:
	movq	%r12, %rdi
	call	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringERKNS_13UnicodeStringE@PLT
	movl	$5, %edx
	movq	-432(%rbp), %rbx
	movq	%r12, %rdi
	testl	%eax, %eax
	cmovs	%edx, %eax
	movl	%eax, (%rbx)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L181:
	movsd	-424(%rbp), %xmm0
	movq	%r15, -352(%rbp)
	movl	$2, %edx
	leaq	-288(%rbp), %r15
	movq	%r15, %rdi
	movw	%dx, -344(%rbp)
	call	_ZN6icu_6711FormattableC1Ed@PLT
	movq	%rbx, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	-352(%rbp), %r10
	movq	%r10, %rdx
	movq	%r10, -448(%rbp)
	call	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movl	(%rbx), %ecx
	movq	-448(%rbp), %r10
	testl	%ecx, %ecx
	jle	.L192
.L185:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L192:
	movzbl	_ZN6icu_67L20kGeneralNumericFieldE(%rip), %ecx
	movq	%r10, %rdx
	movq	%rbx, %r8
	movq	%r12, %rdi
	movl	132(%r12), %esi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movl	(%rbx), %eax
	movq	-448(%rbp), %r10
	testl	%eax, %eax
	jg	.L185
	movsd	-424(%rbp), %xmm0
	movq	-440(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-416(%rbp), %r12
	call	_ZNK6icu_6711PluralRules6selectEd@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-448(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L183
.L191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2880:
	.size	_ZN6icu_6717QuantityFormatter15formatAndSelectEdRKNS_12NumberFormatERKNS_11PluralRulesERNS_22FormattedStringBuilderERNS_14StandardPlural4FormER10UErrorCode, .-_ZN6icu_6717QuantityFormatter15formatAndSelectEdRKNS_12NumberFormatERKNS_11PluralRulesERNS_22FormattedStringBuilderERNS_14StandardPlural4FormER10UErrorCode
	.section	.rodata
	.type	_ZN6icu_67L20kGeneralNumericFieldE, @object
	.size	_ZN6icu_67L20kGeneralNumericFieldE, 1
_ZN6icu_67L20kGeneralNumericFieldE:
	.byte	1
	.type	_ZN6icu_67L15kUndefinedFieldE, @object
	.size	_ZN6icu_67L15kUndefinedFieldE, 1
_ZN6icu_67L15kUndefinedFieldE:
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
