	.file	"nfrule.cpp"
	.text
	.section	.text._ZNK6icu_6721RuleBasedNumberFormat9isLenientEv,"axG",@progbits,_ZNK6icu_6721RuleBasedNumberFormat9isLenientEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721RuleBasedNumberFormat9isLenientEv
	.type	_ZNK6icu_6721RuleBasedNumberFormat9isLenientEv, @function
_ZNK6icu_6721RuleBasedNumberFormat9isLenientEv:
.LFB2436:
	.cfi_startproc
	endbr64
	movzbl	652(%rdi), %eax
	ret
	.cfi_endproc
.LFE2436:
	.size	_ZNK6icu_6721RuleBasedNumberFormat9isLenientEv, .-_ZNK6icu_6721RuleBasedNumberFormat9isLenientEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NFRuleD2Ev
	.type	_ZN6icu_676NFRuleD2Ev, @function
_ZN6icu_676NFRuleD2Ev:
.LFB2695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rax
	movq	88(%rdi), %rdi
	cmpq	%rdi, %rax
	je	.L4
	testq	%rdi, %rdi
	je	.L8
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	80(%rbx), %rdi
.L5:
	movq	$0, 88(%rbx)
.L4:
	testq	%rdi, %rdi
	je	.L6
	movq	(%rdi), %rax
	call	*8(%rax)
.L6:
	movq	104(%rbx), %rdi
	movq	$0, 80(%rbx)
	testq	%rdi, %rdi
	je	.L7
	movq	(%rdi), %rax
	call	*8(%rax)
.L7:
	movq	$0, 104(%rbx)
	addq	$8, %rsp
	leaq	16(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	%rax, %rdi
	jmp	.L5
	.cfi_endproc
.LFE2695:
	.size	_ZN6icu_676NFRuleD2Ev, .-_ZN6icu_676NFRuleD2Ev
	.globl	_ZN6icu_676NFRuleD1Ev
	.set	_ZN6icu_676NFRuleD1Ev,_ZN6icu_676NFRuleD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NFRule19extractSubstitutionEPKNS_9NFRuleSetEPKS0_R10UErrorCode
	.type	_ZN6icu_676NFRule19extractSubstitutionEPKNS_9NFRuleSetEPKS0_R10UErrorCode, @function
_ZN6icu_676NFRule19extractSubstitutionEPKNS_9NFRuleSetEPKS0_R10UErrorCode:
.LFB2700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	$-1, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	8+_ZN6icu_67L13RULE_PREFIXESE(%rip), %rbx
	subq	$120, %rsp
	movq	%rsi, -136(%rbp)
	movl	$60, %esi
	movq	%rdx, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L64:
	sarl	$5, %ecx
.L18:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	cmpl	$-1, %eax
	je	.L19
	cmpl	$-1, %r12d
	je	.L47
	cmpl	%r12d, %eax
	jl	.L47
.L19:
	movq	(%rbx), %r14
	addq	$8, %rbx
	testq	%r14, %r14
	je	.L21
.L65:
	movzwl	(%r14), %esi
.L22:
	movswl	24(%r13), %ecx
	testw	%cx, %cx
	jns	.L64
	movl	28(%r13), %ecx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L47:
	movq	(%rbx), %r14
	addq	$8, %rbx
	movl	%eax, %r12d
	testq	%r14, %r14
	jne	.L65
.L21:
	cmpl	$-1, %r12d
	je	.L16
	movswl	24(%r13), %r9d
	testw	%r9w, %r9w
	js	.L25
	sarl	$5, %r9d
	xorl	%r8d, %r8d
.L26:
	xorl	%edx, %edx
	movl	$3, %ecx
	leaq	_ZN6icu_67L22gGreaterGreaterGreaterE(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	cmpl	%eax, %r12d
	je	.L66
	movzwl	24(%r13), %eax
	testw	%ax, %ax
	js	.L29
	movswl	%ax, %ecx
	leal	1(%r12), %edx
	sarl	$5, %ecx
	cmpl	%r12d, %ecx
	jbe	.L31
.L68:
	leaq	26(%r13), %rsi
	testb	$2, %al
	jne	.L33
	movq	40(%r13), %rsi
.L33:
	movslq	%r12d, %rax
	movzwl	(%rsi,%rax,2), %esi
	cmpw	$60, %si
	sete	%al
	xorl	%r8d, %r8d
	movl	%eax, %ebx
	testl	%edx, %edx
	js	.L34
.L44:
	cmpl	%ecx, %edx
	cmovg	%ecx, %edx
	movl	%edx, %r8d
	subl	%edx, %ecx
.L34:
	movl	%r8d, %edx
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	cmpl	$-1, %eax
	je	.L16
	testb	%bl, %bl
	je	.L28
	movzwl	24(%r13), %ecx
	testw	%cx, %cx
	js	.L35
	movswl	%cx, %edx
	sarl	$5, %edx
.L36:
	leal	-1(%rdx), %esi
	leal	1(%rax), %r10d
	cmpl	%eax, %esi
	jle	.L37
	cmpl	%r10d, %edx
	jbe	.L37
	andl	$2, %ecx
	leaq	26(%r13), %rdx
	jne	.L39
	movq	40(%r13), %rdx
.L39:
	movslq	%r10d, %rax
	cmpw	$60, (%rdx,%rax,2)
	jne	.L37
	movl	%r10d, %eax
	.p2align 4,,10
	.p2align 3
.L28:
	cmpl	$-1, %eax
	je	.L16
	leal	1(%rax), %r10d
.L37:
	leaq	-128(%rbp), %rbx
	subl	%r12d, %r10d
	movl	$2, %ecx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rbx, %rdi
	movl	%r10d, -156(%rbp)
	movq	%rax, -128(%rbp)
	movw	%cx, -120(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-120(%rbp), %edx
	movl	-156(%rbp), %r10d
	testw	%dx, %dx
	js	.L41
	sarl	$5, %edx
.L42:
	movl	%r10d, %r9d
	movl	%r12d, %r8d
	movq	%r15, %rcx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	%r10d, -156(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	subq	$8, %rsp
	movq	96(%r13), %r8
	movq	%rbx, %r9
	pushq	-152(%rbp)
	movq	-136(%rbp), %rcx
	movq	%r13, %rsi
	movl	%r12d, %edi
	movq	-144(%rbp), %rdx
	call	_ZN6icu_6714NFSubstitution16makeSubstitutionEiPKNS_6NFRuleES3_PKNS_9NFRuleSetEPKNS_21RuleBasedNumberFormatERKNS_13UnicodeStringER10UErrorCode@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	-156(%rbp), %r10d
	movl	%r12d, %esi
	movq	%r15, %rdi
	movq	%rax, %r14
	movl	%r10d, %edx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	popq	%rax
	popq	%rdx
.L16:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movl	28(%r13), %r9d
	movl	$0, %r8d
	testl	%r9d, %r9d
	cmovle	%r9d, %r8d
	subl	%r8d, %r9d
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L66:
	leal	2(%r12), %eax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L41:
	movl	-116(%rbp), %edx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L29:
	movl	28(%r13), %ecx
	leal	1(%r12), %edx
	cmpl	%r12d, %ecx
	ja	.L68
.L31:
	testl	%edx, %edx
	js	.L43
	xorl	%ebx, %ebx
	movl	$65535, %esi
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L35:
	movl	28(%r13), %edx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%edx, %edx
	movl	$65535, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	jmp	.L28
.L67:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2700:
	.size	_ZN6icu_676NFRule19extractSubstitutionEPKNS_9NFRuleSetEPKS0_R10UErrorCode, .-_ZN6icu_676NFRule19extractSubstitutionEPKNS_9NFRuleSetEPKS0_R10UErrorCode
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"c"
	.string	"a"
	.string	"r"
	.string	"d"
	.string	"i"
	.string	"n"
	.string	"a"
	.string	"l"
	.string	""
	.string	""
	.align 2
.LC1:
	.string	"o"
	.string	"r"
	.string	"d"
	.string	"i"
	.string	"n"
	.string	"a"
	.string	"l"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_676NFRule20extractSubstitutionsEPKNS_9NFRuleSetERKNS_13UnicodeStringEPKS0_R10UErrorCode.part.0, @function
_ZN6icu_676NFRule20extractSubstitutionsEPKNS_9NFRuleSetERKNS_13UnicodeStringEPKS0_R10UErrorCode.part.0:
.LFB3779:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$200, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676NFRule19extractSubstitutionEPKNS_9NFRuleSetEPKS0_R10UErrorCode
	movq	%rax, 80(%rbx)
	testq	%rax, %rax
	je	.L111
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676NFRule19extractSubstitutionEPKNS_9NFRuleSetEPKS0_R10UErrorCode
	movswl	24(%rbx), %r9d
	movq	%rax, 88(%rbx)
	testw	%r9w, %r9w
	js	.L72
.L114:
	sarl	$5, %r9d
	xorl	%r8d, %r8d
.L73:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	_ZN6icu_67L22gDollarOpenParenthesisE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jns	.L112
.L69:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movswl	24(%rbx), %r9d
	movq	$0, 88(%rbx)
	testw	%r9w, %r9w
	jns	.L114
.L72:
	movl	28(%rbx), %r9d
	movl	$0, %r8d
	testl	%r9d, %r9d
	cmovle	%r9d, %r8d
	subl	%r8d, %r9d
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L112:
	movswl	24(%rbx), %r9d
	testw	%r9w, %r9w
	js	.L76
	sarl	$5, %r9d
	cmpl	%r9d, %eax
	movl	%r9d, %r8d
	cmovle	%eax, %r8d
.L77:
	subl	%r8d, %r9d
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	_ZN6icu_67L24gClosedParenthesisDollarE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	js	.L69
	movswl	24(%rbx), %ecx
	testw	%cx, %cx
	js	.L79
	sarl	$5, %ecx
	cmpl	%ecx, %r15d
	movl	%ecx, %edx
	cmovle	%r15d, %edx
.L80:
	subl	%edx, %ecx
	movl	$44, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, -212(%rbp)
	testl	%eax, %eax
	js	.L115
	movl	%eax, %ecx
	leaq	-192(%rbp), %rdi
	leal	2(%r15), %edx
	movq	%r13, %rsi
	subl	%r15d, %ecx
	movq	%rdi, -224(%rbp)
	leaq	-128(%rbp), %r15
	subl	$2, %ecx
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	leaq	.LC0(%rip), %rax
	movl	$-1, %ecx
	movq	%r15, %rdi
	movq	%rax, -200(%rbp)
	leaq	-200(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdx
	movq	%rax, -240(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L83
	testb	$1, %al
	je	.L116
.L84:
	movzbl	-184(%rbp), %eax
	notl	%eax
	andl	$1, %eax
	movb	%al, -232(%rbp)
.L87:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -232(%rbp)
	je	.L103
	movq	-240(%rbp), %rdx
	leaq	.LC1(%rip), %rax
	movl	$-1, %ecx
	movq	%r15, %rdi
	movl	$1, %esi
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L91
	testb	$1, %al
	jne	.L92
	movswl	%ax, %edx
	xorl	%r8d, %r8d
	sarl	$5, %edx
.L98:
	movl	%edx, %r9d
	subl	%r8d, %r9d
	cmpl	%edx, %r9d
	cmovg	%edx, %r9d
.L96:
	testb	$2, %al
	movq	-224(%rbp), %rdi
	leaq	-118(%rbp), %rcx
	cmove	-104(%rbp), %rcx
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movb	%al, -232(%rbp)
.L95:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -232(%rbp)
	movl	$1, %r8d
	jne	.L117
.L90:
	movl	-212(%rbp), %eax
	movq	96(%rbx), %r9
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%r8d, -240(%rbp)
	subl	%eax, %r14d
	leal	1(%rax), %edx
	movq	%r9, -232(%rbp)
	leal	-1(%r14), %ecx
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movl	-240(%rbp), %r8d
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	-232(%rbp), %r9
	movl	%r8d, %esi
	movq	%r9, %rdi
	call	_ZNK6icu_6721RuleBasedNumberFormat18createPluralFormatE11UPluralTypeRKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%rax, 104(%rbx)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-224(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L76:
	movl	28(%rbx), %r9d
	cmpl	%r9d, %eax
	movl	%r9d, %r8d
	cmovle	%eax, %r8d
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L115:
	movl	$9, (%r12)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L79:
	movl	28(%rbx), %ecx
	cmpl	%ecx, %r15d
	movl	%ecx, %edx
	cmovle	%r15d, %edx
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L116:
	movswl	%ax, %edx
	xorl	%r8d, %r8d
	sarl	$5, %edx
.L99:
	movl	%edx, %r9d
	subl	%r8d, %r9d
	cmpl	%edx, %r9d
	cmovg	%edx, %r9d
.L88:
	testb	$2, %al
	movq	-224(%rbp), %rdi
	leaq	-118(%rbp), %rcx
	cmove	-104(%rbp), %rcx
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movb	%al, -232(%rbp)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L103:
	xorl	%r8d, %r8d
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L83:
	movl	-116(%rbp), %edx
	testb	$1, %al
	jne	.L84
	testl	%edx, %edx
	movl	$0, %r8d
	cmovle	%edx, %r8d
	jns	.L99
	xorl	%r9d, %r9d
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L92:
	movzbl	-184(%rbp), %eax
	notl	%eax
	andl	$1, %eax
	movb	%al, -232(%rbp)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L91:
	movl	-116(%rbp), %edx
	testb	$1, %al
	jne	.L92
	testl	%edx, %edx
	movl	$0, %r8d
	cmovle	%edx, %r8d
	jns	.L98
	xorl	%r9d, %r9d
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$1, (%r12)
	movq	-224(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L69
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3779:
	.size	_ZN6icu_676NFRule20extractSubstitutionsEPKNS_9NFRuleSetERKNS_13UnicodeStringEPKS0_R10UErrorCode.part.0, .-_ZN6icu_676NFRule20extractSubstitutionsEPKNS_9NFRuleSetERKNS_13UnicodeStringEPKS0_R10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NFRule20extractSubstitutionsEPKNS_9NFRuleSetERKNS_13UnicodeStringEPKS0_R10UErrorCode
	.type	_ZN6icu_676NFRule20extractSubstitutionsEPKNS_9NFRuleSetERKNS_13UnicodeStringEPKS0_R10UErrorCode, @function
_ZN6icu_676NFRule20extractSubstitutionsEPKNS_9NFRuleSetERKNS_13UnicodeStringEPKS0_R10UErrorCode:
.LFB2699:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L120
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	jmp	_ZN6icu_676NFRule20extractSubstitutionsEPKNS_9NFRuleSetERKNS_13UnicodeStringEPKS0_R10UErrorCode.part.0
	.cfi_endproc
.LFE2699:
	.size	_ZN6icu_676NFRule20extractSubstitutionsEPKNS_9NFRuleSetERKNS_13UnicodeStringEPKS0_R10UErrorCode, .-_ZN6icu_676NFRule20extractSubstitutionsEPKNS_9NFRuleSetERKNS_13UnicodeStringEPKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NFRule12setBaseValueElR10UErrorCode
	.type	_ZN6icu_676NFRule12setBaseValueElR10UErrorCode, @function
_ZN6icu_676NFRule12setBaseValueElR10UErrorCode:
.LFB2701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%rsi, (%rdi)
	movl	$10, 8(%rdi)
	testq	%rsi, %rsi
	jg	.L133
	xorl	%eax, %eax
	movw	%ax, 12(%rdi)
.L121:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movq	%rdx, %r12
	cvtsi2sdq	%rsi, %xmm0
	call	uprv_log_67@PLT
	movsd	%xmm0, -40(%rbp)
	pxor	%xmm0, %xmm0
	cvtsi2sdl	8(%rbx), %xmm0
	call	uprv_log_67@PLT
	movsd	-40(%rbp), %xmm1
	movl	8(%rbx), %edi
	divsd	%xmm0, %xmm1
	cvttsd2sil	%xmm1, %r14d
	leal	1(%r14), %r13d
	movzwl	%r13w, %esi
	call	_ZN6icu_6710util64_powEjt@PLT
	movl	%r14d, %edx
	cmpq	(%rbx), %rax
	movq	80(%rbx), %rdi
	cmovle	%r13d, %edx
	movw	%dx, 12(%rbx)
	testq	%rdi, %rdi
	je	.L124
	movq	(%rdi), %rax
	movl	8(%rbx), %esi
	movswl	%dx, %edx
	movq	%r12, %rcx
	call	*32(%rax)
.L124:
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L121
	movq	(%rdi), %rax
	movswl	12(%rbx), %edx
	movq	%r12, %rcx
	movl	8(%rbx), %esi
	movq	32(%rax), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE2701:
	.size	_ZN6icu_676NFRule12setBaseValueElR10UErrorCode, .-_ZN6icu_676NFRule12setBaseValueElR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NFRule19parseRuleDescriptorERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676NFRule19parseRuleDescriptorERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676NFRule19parseRuleDescriptorERNS_13UnicodeStringER10UErrorCode:
.LFB2698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -160(%rbp)
	movswl	8(%rsi), %ecx
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testw	%cx, %cx
	js	.L135
	sarl	$5, %ecx
.L136:
	xorl	%edx, %edx
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %r13d
	cmpl	$-1, %eax
	jne	.L269
.L137:
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L203
	movswl	%ax, %edx
	sarl	$5, %edx
.L204:
	testl	%edx, %edx
	jle	.L134
	testb	$2, %al
	jne	.L270
	movq	24(%r12), %rax
	cmpw	$39, (%rax)
	je	.L271
.L134:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L272
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	cmpw	$39, 10(%r12)
	leaq	10(%r12), %rax
	jne	.L134
.L271:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L269:
	leaq	-128(%rbp), %r15
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r10d
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	movw	%r10w, -120(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L138
	movswl	%ax, %edx
	sarl	$5, %edx
.L139:
	movl	%r13d, %r9d
	addl	$1, %r13d
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movslq	%r13d, %rbx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	addq	%rbx, %rbx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L273:
	movswl	%dx, %eax
	sarl	$5, %eax
	cmpl	%eax, %r13d
	jge	.L142
.L274:
	movl	$65535, %edi
	cmpl	%r13d, %eax
	jbe	.L143
	andl	$2, %edx
	leaq	10(%r12), %rax
	jne	.L145
	movq	24(%r12), %rax
.L145:
	movzwl	(%rax,%rbx), %edi
.L143:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	addq	$2, %rbx
	testb	%al, %al
	je	.L142
	addl	$1, %r13d
.L146:
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	jns	.L273
	movl	12(%r12), %eax
	cmpl	%eax, %r13d
	jl	.L274
.L142:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L147
	movswl	%ax, %r13d
	sarl	$5, %r13d
	testl	%r13d, %r13d
	je	.L213
.L277:
	leal	-1(%r13), %edx
	testb	$2, %al
	jne	.L150
	movq	-104(%rbp), %rcx
	movzwl	(%rcx), %ebx
	cmpl	%edx, %r13d
	jbe	.L210
.L151:
	movslq	%edx, %rsi
	movzwl	(%rcx,%rsi,2), %r14d
	leal	-48(%rbx), %ecx
	cmpw	$9, %cx
	ja	.L149
	cmpw	$120, %r14w
	je	.L149
.L152:
	testl	%r13d, %r13d
	jle	.L153
	movl	%edx, %esi
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	movq	%rsi, -136(%rbp)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L275:
	movswl	%ax, %ecx
	sarl	$5, %ecx
	cmpl	%r9d, %ecx
	jbe	.L156
.L276:
	leaq	-118(%rbp), %rcx
	testb	$2, %al
	movq	%rcx, %rax
	cmove	-104(%rbp), %rax
	movzwl	(%rax,%rdx,2), %r14d
	leal	-48(%r14), %eax
	cmpw	$9, %ax
	ja	.L158
	movzwl	%r14w, %eax
	leaq	(%rbx,%rbx,4), %rsi
	subl	$48, %eax
	cltq
	leaq	(%rax,%rsi,2), %rbx
.L159:
	leal	1(%r9), %r8d
	leaq	1(%rdx), %rdi
	cmpq	%rdx, -136(%rbp)
	je	.L160
	movzwl	-120(%rbp), %eax
	movq	%rdi, %rdx
.L162:
	movl	%edx, %r9d
	movl	%edx, %r8d
	testw	%ax, %ax
	jns	.L275
	movl	-116(%rbp), %ecx
	cmpl	%r9d, %ecx
	ja	.L276
.L156:
	movl	$65535, %edi
	movq	%rdx, -152(%rbp)
	movl	%r9d, -144(%rbp)
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	movl	-144(%rbp), %r9d
	movq	-152(%rbp), %rdx
	testb	%al, %al
	je	.L171
	movl	$-1, %r14d
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L135:
	movl	12(%rsi), %ecx
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L203:
	movl	12(%r12), %edx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L138:
	movl	-116(%rbp), %edx
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L147:
	movl	-116(%rbp), %r13d
	testl	%r13d, %r13d
	jne	.L277
.L213:
	movl	$-1, %ebx
.L265:
	movl	$-1, %r14d
.L149:
	movl	%r13d, %edx
	movl	$2, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	leaq	_ZN6icu_67L7gMinusXE(%rip), %rcx
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L7gMinusXE(%rip), %rdx
	testb	%al, %al
	jne	.L182
	movq	-160(%rbp), %rax
	movq	$-1, (%rax)
.L176:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L150:
	movzwl	-118(%rbp), %ebx
	leaq	-118(%rbp), %rcx
	cmpl	%edx, %r13d
	ja	.L151
.L210:
	leal	-48(%rbx), %ecx
	cmpw	$9, %cx
	jbe	.L152
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L158:
	movq	%rdx, -152(%rbp)
	movl	%r9d, -144(%rbp)
	cmpw	$47, %r14w
	je	.L160
	cmpw	$62, %r14w
	je	.L160
	movzwl	%r14w, %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	movl	-144(%rbp), %r9d
	movq	-152(%rbp), %rdx
	testb	%al, %al
	jne	.L159
	movl	%r14d, %eax
	andl	$-3, %eax
	cmpw	$44, %ax
	je	.L159
	.p2align 4,,10
	.p2align 3
.L171:
	movq	-168(%rbp), %rax
	movq	%r15, %rdi
	movl	$9, (%rax)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L160:
	movq	-168(%rbp), %rdx
	movq	-160(%rbp), %rdi
	movq	%rbx, %rsi
	movl	%r8d, -136(%rbp)
	call	_ZN6icu_676NFRule12setBaseValueElR10UErrorCode
	cmpw	$47, %r14w
	movslq	-136(%rbp), %r8
	je	.L278
.L163:
	cmpw	$62, %r14w
	jne	.L176
	movswl	-120(%rbp), %ecx
	leaq	-118(%rbp), %rdx
	movl	-116(%rbp), %esi
	movl	%ecx, %eax
	sarl	$5, %ecx
	testb	$2, %al
	cmove	-104(%rbp), %rdx
	testw	%ax, %ax
	js	.L279
	cmpl	%r8d, %ecx
	jle	.L176
.L280:
	jbe	.L171
	cmpw	$62, (%rdx,%r8,2)
	jne	.L171
	movq	-160(%rbp), %rsi
	addq	$1, %r8
	movzwl	12(%rsi), %eax
	testw	%ax, %ax
	jle	.L171
	subl	$1, %eax
	movw	%ax, 12(%rsi)
	cmpl	%r8d, %ecx
	jg	.L280
	jmp	.L176
.L279:
	cmpl	%r8d, %esi
	jle	.L176
.L281:
	jbe	.L171
	cmpw	$62, (%rdx,%r8,2)
	jne	.L171
	movq	-160(%rbp), %rbx
	addq	$1, %r8
	movzwl	12(%rbx), %eax
	testw	%ax, %ax
	jle	.L171
	subl	$1, %eax
	movw	%ax, 12(%rbx)
	cmpl	%r8d, %esi
	jg	.L281
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L182:
	cmpl	$3, %r13d
	jne	.L176
	cmpw	$120, %r14w
	movswl	-120(%rbp), %eax
	sete	%dl
	cmpw	$48, %bx
	jne	.L183
	testb	%dl, %dl
	je	.L183
	movq	-160(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	$-3, (%rsi)
	movl	$10, 8(%rsi)
	movw	%r9w, 12(%rsi)
	testw	%ax, %ax
	js	.L194
.L268:
	movswl	%ax, %edx
	sarl	$5, %edx
.L195:
	movl	$-1, %ecx
	cmpl	$1, %edx
	jbe	.L196
	leaq	-118(%rbp), %rdx
	testb	$2, %al
	movq	%rdx, %rax
	cmove	-104(%rbp), %rax
	movzwl	2(%rax), %ecx
.L196:
	movq	-160(%rbp), %rax
	movw	%cx, 14(%rax)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L278:
	leal	1(%r8), %ebx
	cmpl	%r13d, %ebx
	jge	.L164
	movslq	%ebx, %rsi
	xorl	%r8d, %r8d
	addq	%rsi, %rsi
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L284:
	movswl	%ax, %edx
	sarl	$5, %edx
	cmpl	%edx, %ebx
	jnb	.L282
.L208:
	leaq	-118(%rbp), %rdx
	testb	$2, %al
	movq	%rdx, %rax
	cmove	-104(%rbp), %rax
	movzwl	(%rax,%rsi), %r14d
	leal	-48(%r14), %eax
	cmpw	$9, %ax
	jbe	.L283
	movq	%rsi, -136(%rbp)
	cmpw	$62, %r14w
	je	.L216
	movzwl	%r14w, %edi
	movq	%r8, -144(%rbp)
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	movq	-144(%rbp), %r8
	movq	-136(%rbp), %rsi
	testb	%al, %al
	jne	.L169
	movl	%r14d, %eax
	andl	$-3, %eax
	cmpw	$44, %ax
	jne	.L171
.L169:
	addl	$1, %ebx
	addq	$2, %rsi
	cmpl	%r13d, %ebx
	je	.L170
.L172:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	jns	.L284
	movl	-116(%rbp), %edx
	cmpl	%edx, %ebx
	jb	.L208
.L282:
	movl	$65535, %edi
	movq	%rsi, -144(%rbp)
	movq	%r8, -136(%rbp)
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	movq	-136(%rbp), %r8
	movq	-144(%rbp), %rsi
	testb	%al, %al
	je	.L171
	movl	$-1, %r14d
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L283:
	movzwl	%r14w, %eax
	leaq	(%r8,%r8,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %r8
	jmp	.L169
.L216:
	movl	%ebx, %r13d
.L170:
	movq	-160(%rbp), %rax
	movl	%r8d, 8(%rax)
	testl	%r8d, %r8d
	je	.L285
	movq	(%rax), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jle	.L174
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	call	uprv_log_67@PLT
	movq	-160(%rbp), %rax
	movsd	%xmm0, -136(%rbp)
	pxor	%xmm0, %xmm0
	cvtsi2sdl	8(%rax), %xmm0
	call	uprv_log_67@PLT
	movsd	-136(%rbp), %xmm1
	movq	-160(%rbp), %rax
	divsd	%xmm0, %xmm1
	movl	8(%rax), %edi
	cvttsd2sil	%xmm1, %edx
	leal	1(%rdx), %ebx
	movl	%edx, -136(%rbp)
	movzwl	%bx, %esi
	call	_ZN6icu_6710util64_powEjt@PLT
	movq	-160(%rbp), %rsi
	movl	-136(%rbp), %edx
	cmpq	(%rsi), %rax
	movl	%edx, %eax
	cmovle	%ebx, %eax
.L174:
	movq	-160(%rbp), %rsi
	movslq	%r13d, %r8
	movw	%ax, 12(%rsi)
	jmp	.L163
.L285:
	movq	-168(%rbp), %rax
	movl	$9, (%rax)
	xorl	%eax, %eax
	jmp	.L174
.L183:
	cmpw	$120, %bx
	sete	%cl
	testb	%dl, %dl
	je	.L188
	testb	%cl, %cl
	je	.L188
	movq	-160(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	$-2, (%rsi)
	movl	$10, 8(%rsi)
	movw	%r8w, 12(%rsi)
	testw	%ax, %ax
	js	.L189
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L190:
	movl	$-1, %edx
	cmpl	$1, %ecx
	jbe	.L191
	leaq	-118(%rbp), %rdx
	testb	$2, %al
	movq	%rdx, %rax
	cmove	-104(%rbp), %rax
	movzwl	2(%rax), %edx
.L191:
	movq	-160(%rbp), %rax
	movw	%dx, 14(%rax)
	jmp	.L176
.L188:
	cmpw	$48, %r14w
	jne	.L193
	testb	%cl, %cl
	je	.L193
	movq	-160(%rbp), %rsi
	xorl	%edi, %edi
	movq	$-4, (%rsi)
	movl	$10, 8(%rsi)
	movw	%di, 12(%rsi)
	testw	%ax, %ax
	jns	.L268
.L194:
	movl	-116(%rbp), %edx
	jmp	.L195
.L153:
	movq	-168(%rbp), %rdx
	movq	-160(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN6icu_676NFRule12setBaseValueElR10UErrorCode
	jmp	.L176
.L189:
	movl	-116(%rbp), %ecx
	jmp	.L190
.L193:
	testw	%ax, %ax
	js	.L198
	sarl	$5, %eax
	movl	%eax, %edx
.L199:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r15, %rdi
	leaq	_ZN6icu_67L4gNaNE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L4gNaNE(%rip), %rdx
	testb	%al, %al
	jne	.L200
	movq	-160(%rbp), %rax
	xorl	%ecx, %ecx
	movq	$-6, (%rax)
	movl	$10, 8(%rax)
	movw	%cx, 12(%rax)
	jmp	.L176
.L164:
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rsi
	xorl	%edx, %edx
	movl	$0, 8(%rax)
	movl	$9, (%rsi)
	movw	%dx, 12(%rax)
	jmp	.L176
.L200:
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L201
	sarl	$5, %eax
	movl	%eax, %edx
.L202:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r15, %rdi
	leaq	_ZN6icu_67L4gInfE(%rip), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_67L4gInfE(%rip), %rdx
	testb	%al, %al
	jne	.L176
	movq	-160(%rbp), %rax
	movq	$-5, (%rax)
	movl	$10, 8(%rax)
	movw	$0, 12(%rax)
	jmp	.L176
.L198:
	movl	-116(%rbp), %edx
	jmp	.L199
.L272:
	call	__stack_chk_fail@PLT
.L201:
	movl	-116(%rbp), %edx
	jmp	.L202
	.cfi_endproc
.LFE2698:
	.size	_ZN6icu_676NFRule19parseRuleDescriptorERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676NFRule19parseRuleDescriptorERNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NFRuleC2EPKNS_21RuleBasedNumberFormatERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676NFRuleC2EPKNS_21RuleBasedNumberFormatERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676NFRuleC2EPKNS_21RuleBasedNumberFormatERKNS_13UnicodeStringER10UErrorCode:
.LFB2692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	16(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	movq	$0, (%rdi)
	movq	$10, 8(%rdi)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movswl	24(%r12), %eax
	pxor	%xmm0, %xmm0
	movq	%rbx, 96(%r12)
	movq	$0, 104(%r12)
	shrl	$5, %eax
	movups	%xmm0, 80(%r12)
	jne	.L289
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	popq	%rbx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676NFRule19parseRuleDescriptorERNS_13UnicodeStringER10UErrorCode
	.cfi_endproc
.LFE2692:
	.size	_ZN6icu_676NFRuleC2EPKNS_21RuleBasedNumberFormatERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676NFRuleC2EPKNS_21RuleBasedNumberFormatERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_676NFRuleC1EPKNS_21RuleBasedNumberFormatERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_676NFRuleC1EPKNS_21RuleBasedNumberFormatERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_676NFRuleC2EPKNS_21RuleBasedNumberFormatERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NFRule9makeRulesERNS_13UnicodeStringEPNS_9NFRuleSetEPKS0_PKNS_21RuleBasedNumberFormatERNS_10NFRuleListER10UErrorCode
	.type	_ZN6icu_676NFRule9makeRulesERNS_13UnicodeStringEPNS_9NFRuleSetEPKS0_PKNS_21RuleBasedNumberFormatERNS_10NFRuleListER10UErrorCode, @function
_ZN6icu_676NFRule9makeRulesERNS_13UnicodeStringEPNS_9NFRuleSetEPKS0_PKNS_21RuleBasedNumberFormatERNS_10NFRuleListER10UErrorCode:
.LFB2697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$112, %edi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$216, %rsp
	movq	%rsi, -208(%rbp)
	movq	%rdx, -216(%rbp)
	movq	%rcx, -200(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L291
	movq	$0, (%rax)
	leaq	16(%rax), %r13
	movq	%r14, %rsi
	movq	%rax, %r15
	movq	$10, 8(%rax)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-200(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 104(%r15)
	movups	%xmm0, 80(%r15)
	movq	%rax, 96(%r15)
	movswl	24(%r15), %eax
	shrl	$5, %eax
	jne	.L359
.L292:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	8(%r14), %ecx
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	jns	.L293
	movl	12(%r14), %ecx
.L293:
	xorl	%edx, %edx
	movl	$91, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %r9d
	testl	%eax, %eax
	js	.L299
	movswl	8(%r14), %ecx
	testw	%cx, %cx
	js	.L297
	sarl	$5, %ecx
.L298:
	xorl	%edx, %edx
	movl	$93, %esi
	movq	%r14, %rdi
	movl	%r9d, -224(%rbp)
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	-224(%rbp), %r9d
	movl	%eax, %r10d
	cmpl	%eax, %r9d
	jg	.L299
	movq	(%r15), %rdx
	testq	%rdx, %rdx
	jle	.L360
	movzwl	12(%r15), %esi
	movl	8(%r15), %edi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movl	%eax, -244(%rbp)
	movl	$2, %eax
	movl	%r9d, -240(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%rcx, -224(%rbp)
	movw	%ax, -184(%rbp)
	call	_ZN6icu_6710util64_powEjt@PLT
	movq	-232(%rbp), %rdx
	movq	-224(%rbp), %rcx
	movq	%rax, %r8
	movl	-240(%rbp), %r9d
	movl	-244(%rbp), %r10d
	movq	%rdx, %rax
	xorl	%edx, %edx
	divq	%r8
	testq	%rdx, %rdx
	jne	.L361
.L304:
	movl	$2, %esi
	movl	$112, %edi
	movq	%rcx, -128(%rbp)
	movl	%r10d, -244(%rbp)
	movl	%r9d, -240(%rbp)
	movw	%si, -120(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L305
	movq	$0, (%rax)
	leaq	16(%rax), %r8
	leaq	-128(%rbp), %r13
	movq	$10, 8(%rax)
	movq	%r8, %rdi
	movq	%r13, %rsi
	movq	%rax, -232(%rbp)
	movq	%r8, -224(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-232(%rbp), %r11
	pxor	%xmm0, %xmm0
	movq	-200(%rbp), %rax
	movq	-224(%rbp), %r8
	movl	-240(%rbp), %r9d
	movswl	24(%r11), %edx
	movq	%rax, 96(%r11)
	movq	$0, 104(%r11)
	movl	-244(%rbp), %r10d
	shrl	$5, %edx
	movups	%xmm0, 80(%r11)
	jne	.L362
.L306:
	movq	%r13, %rdi
	movl	%r10d, -232(%rbp)
	movl	%r9d, -224(%rbp)
	movq	%r11, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r15), %rax
	movq	-200(%rbp), %r11
	movl	-224(%rbp), %r9d
	movl	-232(%rbp), %r10d
	testq	%rax, %rax
	js	.L363
	movq	%rax, (%r11)
	movq	-208(%rbp), %rax
	cmpb	$0, 160(%rax)
	jne	.L308
	addq	$1, (%r15)
.L308:
	movl	8(%r15), %eax
	leaq	-192(%rbp), %r13
	movl	%r9d, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%r10d, -200(%rbp)
	movl	%eax, 8(%r11)
	movzwl	12(%r15), %eax
	movq	%r11, -232(%rbp)
	movw	%ax, 12(%r11)
	movl	%r9d, -224(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	-200(%rbp), %r10d
	movl	-224(%rbp), %r9d
	movq	-232(%rbp), %r11
	leal	1(%r10), %eax
	movl	%eax, -200(%rbp)
	movswl	8(%r14), %eax
	testw	%ax, %ax
	js	.L311
	sarl	$5, %eax
.L312:
	cmpl	%eax, -200(%rbp)
	jl	.L364
.L313:
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L303
	movq	-216(%rbp), %rcx
	movq	%r11, %rdi
	movq	%r12, %r8
	movq	%r13, %rdx
	movq	-208(%rbp), %rsi
	movl	%r10d, -240(%rbp)
	movl	%r9d, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN6icu_676NFRule20extractSubstitutionsEPKNS_9NFRuleSetERKNS_13UnicodeStringEPKS0_R10UErrorCode.part.0
	movl	-240(%rbp), %r10d
	movl	-232(%rbp), %r9d
	movq	-224(%rbp), %r11
.L303:
	movq	%r13, %rdi
	movl	%r10d, -240(%rbp)
	movl	%r9d, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	-184(%rbp), %eax
	movq	-224(%rbp), %r11
	movl	-232(%rbp), %r9d
	movl	-240(%rbp), %r10d
	testw	%ax, %ax
	js	.L314
	movswl	%ax, %edx
	sarl	$5, %edx
.L315:
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r14, %rcx
	movq	%r13, %rdi
	movq	%r11, -240(%rbp)
	movl	%r10d, -224(%rbp)
	movl	%r9d, -232(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movl	-224(%rbp), %r10d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	-232(%rbp), %r9d
	movl	%r10d, %ecx
	subl	%r9d, %ecx
	leal	1(%r9), %edx
	subl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	8(%r14), %eax
	movl	-224(%rbp), %r10d
	movq	-240(%rbp), %r11
	testw	%ax, %ax
	js	.L316
	sarl	$5, %eax
.L317:
	cmpl	-200(%rbp), %eax
	jg	.L365
.L318:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L319
	movq	-216(%rbp), %rcx
	movq	%r12, %r8
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	-208(%rbp), %rsi
	movq	%r11, -200(%rbp)
	call	_ZN6icu_676NFRule20extractSubstitutionsEPKNS_9NFRuleSetERKNS_13UnicodeStringEPKS0_R10UErrorCode.part.0
	movq	-200(%rbp), %r11
.L319:
	testq	%r11, %r11
	je	.L320
	cmpq	$0, (%r11)
	js	.L321
	movl	12(%rbx), %eax
	movq	(%rbx), %rdi
	cmpl	%eax, 8(%rbx)
	je	.L366
.L322:
	testq	%rdi, %rdi
	je	.L323
	movl	8(%rbx), %eax
	leal	1(%rax), %edx
	movl	%edx, 8(%rbx)
	movq	%r11, (%rdi,%rax,8)
.L320:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L360:
	movl	%edx, %eax
	andl	$-3, %eax
	cmpl	$-3, %eax
	je	.L299
	leal	6(%rdx), %eax
	cmpl	$1, %eax
	ja	.L367
.L299:
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L295
	movq	-216(%rbp), %rcx
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	-208(%rbp), %rsi
	call	_ZN6icu_676NFRule20extractSubstitutionsEPKNS_9NFRuleSetERKNS_13UnicodeStringEPKS0_R10UErrorCode.part.0
.L295:
	cmpq	$0, (%r15)
	js	.L324
	movl	12(%rbx), %eax
	movq	(%rbx), %rdi
	cmpl	%eax, 8(%rbx)
	je	.L368
	testq	%rdi, %rdi
	je	.L326
.L370:
	movl	8(%rbx), %eax
	leal	1(%rax), %edx
	movl	%edx, 8(%rbx)
	movq	%r15, (%rdi,%rax,8)
.L290:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L369
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	movl	12(%r14), %ecx
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L324:
	movq	-208(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_679NFRuleSet19setNonNumericalRuleEPNS_6NFRuleE@PLT
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L359:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676NFRule19parseRuleDescriptorERNS_13UnicodeStringER10UErrorCode
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L326:
	movq	$0, 8(%rbx)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L368:
	addl	$10, %eax
	movl	%eax, 12(%rbx)
	leaq	0(,%rax,8), %rsi
	call	uprv_realloc_67@PLT
	movq	%rax, %rdi
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	jne	.L370
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L363:
	cmpl	$-2, %eax
	je	.L371
	cmpl	$-4, %eax
	jne	.L308
	movq	%rax, (%r11)
	movq	$-2, (%r15)
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L362:
	movq	%r11, %rdi
	movq	%r12, %rdx
	movq	%r8, %rsi
	movl	%r10d, -232(%rbp)
	movl	%r9d, -224(%rbp)
	movq	%r11, -200(%rbp)
	call	_ZN6icu_676NFRule19parseRuleDescriptorERNS_13UnicodeStringER10UErrorCode
	movq	-200(%rbp), %r11
	movl	-224(%rbp), %r9d
	movl	-232(%rbp), %r10d
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L361:
	movq	(%r15), %rdx
	testq	%rdx, %rdx
	jle	.L302
.L358:
	leal	1(%r10), %eax
	xorl	%r11d, %r11d
	leaq	-192(%rbp), %r13
	movl	%eax, -200(%rbp)
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L316:
	movl	12(%r14), %eax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L314:
	movl	-180(%rbp), %edx
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L365:
	subl	%r10d, %eax
	movl	-200(%rbp), %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	leal	-1(%rax), %ecx
	movq	%r11, -224(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-224(%rbp), %r11
	jmp	.L318
.L367:
	movw	$2, -184(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movq	%rcx, -192(%rbp)
.L302:
	andl	$-3, %edx
	cmpl	$-4, %edx
	je	.L304
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L321:
	movq	-208(%rbp), %rdi
	movq	%r11, %rsi
	call	_ZN6icu_679NFRuleSet19setNonNumericalRuleEPNS_6NFRuleE@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L311:
	movl	12(%r14), %eax
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L364:
	subl	%r10d, %eax
	movl	-200(%rbp), %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	leal	-1(%rax), %ecx
	movl	%r9d, -240(%rbp)
	movq	%r11, -232(%rbp)
	movl	%r10d, -224(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	-240(%rbp), %r9d
	movq	-232(%rbp), %r11
	movl	-224(%rbp), %r10d
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L323:
	movq	$0, 8(%rbx)
	jmp	.L320
.L371:
	movq	$-3, (%r11)
	jmp	.L308
.L366:
	addl	$10, %eax
	movq	%r11, -200(%rbp)
	movl	%eax, 12(%rbx)
	leaq	0(,%rax,8), %rsi
	call	uprv_realloc_67@PLT
	movq	-200(%rbp), %r11
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	jmp	.L322
.L291:
	movl	$7, (%r12)
	jmp	.L290
.L369:
	call	__stack_chk_fail@PLT
.L305:
	leaq	-128(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$7, (%r12)
	leaq	-192(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L290
	.cfi_endproc
.LFE2697:
	.size	_ZN6icu_676NFRule9makeRulesERNS_13UnicodeStringEPNS_9NFRuleSetEPKS0_PKNS_21RuleBasedNumberFormatERNS_10NFRuleListER10UErrorCode, .-_ZN6icu_676NFRule9makeRulesERNS_13UnicodeStringEPNS_9NFRuleSetEPKS0_PKNS_21RuleBasedNumberFormatERNS_10NFRuleListER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRule16expectedExponentEv
	.type	_ZNK6icu_676NFRule16expectedExponentEv, @function
_ZNK6icu_676NFRule16expectedExponentEv:
.LFB2702:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L377
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	jle	.L372
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	call	uprv_log_67@PLT
	movsd	%xmm0, -40(%rbp)
	pxor	%xmm0, %xmm0
	cvtsi2sdl	8(%rbx), %xmm0
	call	uprv_log_67@PLT
	movsd	-40(%rbp), %xmm1
	movl	8(%rbx), %edi
	divsd	%xmm0, %xmm1
	cvttsd2sil	%xmm1, %r13d
	leal	1(%r13), %r12d
	movzwl	%r12w, %esi
	call	_ZN6icu_6710util64_powEjt@PLT
	cmpq	%rax, (%rbx)
	movl	%r13d, %eax
	cmovge	%r12d, %eax
.L372:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE2702:
	.size	_ZNK6icu_676NFRule16expectedExponentEv, .-_ZNK6icu_676NFRule16expectedExponentEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRule20indexOfAnyRulePrefixEv
	.type	_ZNK6icu_676NFRule20indexOfAnyRulePrefixEv, @function
_ZNK6icu_676NFRule20indexOfAnyRulePrefixEv:
.LFB2703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$60, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	8+_ZN6icu_67L13RULE_PREFIXESE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	$-1, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L395:
	sarl	$5, %ecx
.L382:
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	cmpl	$-1, %eax
	je	.L383
	cmpl	$-1, %r12d
	je	.L387
	cmpl	%r12d, %eax
	jl	.L387
.L383:
	movq	(%r14), %rax
	addq	$8, %r14
	testq	%rax, %rax
	je	.L380
.L396:
	movzwl	(%rax), %esi
.L386:
	movswl	24(%rbx), %ecx
	testw	%cx, %cx
	jns	.L395
	movl	28(%rbx), %ecx
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L387:
	addq	$8, %r14
	movl	%eax, %r12d
	movq	-8(%r14), %rax
	testq	%rax, %rax
	jne	.L396
.L380:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2703:
	.size	_ZNK6icu_676NFRule20indexOfAnyRulePrefixEv, .-_ZNK6icu_676NFRule20indexOfAnyRulePrefixEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRuleeqERKS0_
	.type	_ZNK6icu_676NFRuleeqERKS0_, @function
_ZNK6icu_676NFRuleeqERKS0_:
.LFB2705:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	cmpq	%rax, (%rdi)
	je	.L398
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	movabsq	$281474976710655, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rdi), %rax
	movq	%rdi, %rbx
	xorq	8(%rsi), %rax
	testq	%rdx, %rax
	je	.L435
.L400:
	xorl	%eax, %eax
.L397:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L435:
	.cfi_restore_state
	movswl	24(%rsi), %ecx
	movzwl	24(%rdi), %eax
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L401
	testw	%ax, %ax
	js	.L402
	movswl	%ax, %edx
	sarl	$5, %edx
.L403:
	testw	%cx, %cx
	js	.L404
	sarl	$5, %ecx
.L405:
	testb	%sil, %sil
	jne	.L400
	cmpl	%edx, %ecx
	jne	.L400
	leaq	16(%r12), %rsi
	leaq	16(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L401:
	testb	%sil, %sil
	je	.L400
	movq	80(%rbx), %rdi
	movq	80(%r12), %rsi
	testq	%rdi, %rdi
	je	.L406
	testq	%rsi, %rsi
	je	.L400
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	je	.L400
.L409:
	movq	88(%rbx), %rdi
	movq	88(%r12), %rsi
	testq	%rdi, %rdi
	je	.L436
	testq	%rsi, %rsi
	je	.L400
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	setne	%al
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L402:
	movl	28(%rdi), %edx
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L406:
	testq	%rsi, %rsi
	jne	.L400
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L404:
	movl	28(%r12), %ecx
	jmp	.L405
.L436:
	testq	%rsi, %rsi
	sete	%al
	jmp	.L397
	.cfi_endproc
.LFE2705:
	.size	_ZNK6icu_676NFRuleeqERKS0_, .-_ZNK6icu_676NFRuleeqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRule15_appendRuleTextERNS_13UnicodeStringE
	.type	_ZNK6icu_676NFRule15_appendRuleTextERNS_13UnicodeStringE, @function
_ZNK6icu_676NFRule15_appendRuleTextERNS_13UnicodeStringE:
.LFB2707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$632, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	jle	.L498
.L438:
	leaq	-576(%rbp), %r15
	movl	$10, %ecx
	xorl	%r8d, %r8d
	movl	$512, %edx
	movq	%r15, %rsi
	leaq	-640(%rbp), %r14
	call	_ZN6icu_6710util64_touElPDsjja@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movswl	-632(%rbp), %ecx
	testw	%cx, %cx
	js	.L450
	sarl	$5, %ecx
.L451:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	$10, 8(%rbx)
	jne	.L452
	leaq	-642(%rbp), %r13
.L457:
	movq	(%rbx), %rax
	testq	%rax, %rax
	jle	.L453
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	call	uprv_log_67@PLT
	movsd	%xmm0, -664(%rbp)
	pxor	%xmm0, %xmm0
	cvtsi2sdl	8(%rbx), %xmm0
	call	uprv_log_67@PLT
	movsd	-664(%rbp), %xmm1
	movl	8(%rbx), %edi
	divsd	%xmm0, %xmm1
	cvttsd2sil	%xmm1, %edx
	leal	1(%rdx), %ecx
	movl	%edx, -668(%rbp)
	movzwl	%cx, %esi
	movl	%ecx, -664(%rbp)
	call	_ZN6icu_6710util64_powEjt@PLT
	movl	-668(%rbp), %edx
	movl	-664(%rbp), %ecx
	cmpq	(%rbx), %rax
	movswl	%dx, %edx
	movswl	%cx, %ecx
	cmovle	%ecx, %edx
.L458:
	movswl	12(%rbx), %eax
	xorl	%r8d, %r8d
	subl	%eax, %edx
	movl	%edx, -668(%rbp)
	testl	%edx, %edx
	jle	.L446
	.p2align 4,,10
	.p2align 3
.L460:
	movl	$62, %r10d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%r8d, -664(%rbp)
	movw	%r10w, -642(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-664(%rbp), %r8d
	addl	$1, %r8d
	cmpl	%r8d, -668(%rbp)
	jne	.L460
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L498:
	leal	6(%rdi), %eax
	cmpl	$-6, %edi
	jb	.L438
	leaq	.L440(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L440:
	.long	.L445-.L440
	.long	.L444-.L440
	.long	.L443-.L440
	.long	.L442-.L440
	.long	.L441-.L440
	.long	.L439-.L440
	.text
	.p2align 4,,10
	.p2align 3
.L441:
	movl	$120, %eax
	movw	%ax, -642(%rbp)
.L496:
	leaq	-642(%rbp), %r13
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	14(%rbx), %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movl	$46, %eax
	testw	%dx, %dx
	cmove	%eax, %edx
	movw	%dx, -642(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%rax, %rdi
	movl	$120, %eax
	movw	%ax, -642(%rbp)
.L497:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	-576(%rbp), %r15
	leaq	-640(%rbp), %r14
.L446:
	movl	$58, %r8d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movw	%r8w, -642(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$32, %r9d
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%r9w, -642(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	24(%rbx), %eax
	testw	%ax, %ax
	js	.L462
	movswl	%ax, %edx
	sarl	$5, %edx
.L463:
	testl	%edx, %edx
	je	.L464
	testb	$2, %al
	je	.L465
	cmpw	$32, 26(%rbx)
	leaq	26(%rbx), %rax
	je	.L499
.L464:
	movl	$2, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	leaq	16(%rbx), %rsi
	movq	%r14, %rdi
	movw	%dx, -632(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -640(%rbp)
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movq	88(%rbx), %rdi
	movq	%rcx, -576(%rbp)
	movl	$2, %ecx
	movw	%cx, -568(%rbp)
	testq	%rdi, %rdi
	je	.L468
	movq	(%rdi), %rax
	movq	%r15, %rsi
	call	*40(%rax)
	movq	88(%rbx), %rax
	movswl	-568(%rbp), %r9d
	movl	8(%rax), %esi
	testw	%r9w, %r9w
	js	.L469
	sarl	$5, %r9d
.L470:
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
.L468:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L471
	movq	(%rdi), %rax
	movq	%r15, %rsi
	call	*40(%rax)
	movq	80(%rbx), %rax
	movswl	-568(%rbp), %r9d
	movl	8(%rax), %esi
	testw	%r9w, %r9w
	js	.L472
	sarl	$5, %r9d
.L473:
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
.L471:
	movswl	-632(%rbp), %ecx
	testw	%cx, %cx
	js	.L474
	sarl	$5, %ecx
.L475:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movl	$59, %eax
	movq	%r12, %rdi
	movw	%ax, -642(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L500
	addq	$632, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	movl	$3, %ecx
	xorl	%edx, %edx
	leaq	_ZN6icu_67L4gNaNE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	_ZN6icu_67L4gNaNE(%rip), %rax
	leaq	-642(%rbp), %r13
	leaq	-576(%rbp), %r15
	leaq	-640(%rbp), %r14
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L439:
	movl	$2, %ecx
	xorl	%edx, %edx
	leaq	_ZN6icu_67L7gMinusXE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	_ZN6icu_67L7gMinusXE(%rip), %rax
	leaq	-642(%rbp), %r13
	leaq	-576(%rbp), %r15
	leaq	-640(%rbp), %r14
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L443:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$120, %r14d
	leaq	-642(%rbp), %r13
	movw	%r14w, -642(%rbp)
	movl	$48, %r15d
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	14(%rbx), %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movl	$46, %eax
	testw	%dx, %dx
	cmove	%eax, %edx
	movw	%dx, -642(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movw	%r15w, -642(%rbp)
	movq	%rax, %rdi
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L442:
	movl	$48, %eax
	movw	%ax, -642(%rbp)
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L444:
	movl	$3, %ecx
	xorl	%edx, %edx
	leaq	_ZN6icu_67L4gInfE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	_ZN6icu_67L4gInfE(%rip), %rax
	leaq	-642(%rbp), %r13
	leaq	-576(%rbp), %r15
	leaq	-640(%rbp), %r14
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L450:
	movl	-628(%rbp), %ecx
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L474:
	movl	-628(%rbp), %ecx
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L462:
	movl	28(%rbx), %edx
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L465:
	movq	40(%rbx), %rax
	cmpw	$32, (%rax)
	jne	.L464
.L499:
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.L467
	movl	8(%rax), %edi
	testl	%edi, %edi
	je	.L464
.L467:
	movl	$39, %esi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%si, -642(%rbp)
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L469:
	movl	-564(%rbp), %r9d
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L472:
	movl	-564(%rbp), %r9d
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L452:
	movl	$47, %r13d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%r13w, -642(%rbp)
	leaq	-642(%rbp), %r13
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movslq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	$10, %ecx
	movl	$512, %edx
	call	_ZN6icu_6710util64_touElPDsjja@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movzwl	-632(%rbp), %eax
	testw	%ax, %ax
	js	.L455
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L456:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	8(%rbx), %r11d
	testl	%r11d, %r11d
	jne	.L457
.L453:
	xorl	%edx, %edx
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L455:
	movl	-628(%rbp), %ecx
	jmp	.L456
.L500:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2707:
	.size	_ZNK6icu_676NFRule15_appendRuleTextERNS_13UnicodeStringE, .-_ZNK6icu_676NFRule15_appendRuleTextERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRule10getDivisorEv
	.type	_ZNK6icu_676NFRule10getDivisorEv, @function
_ZNK6icu_676NFRule10getDivisorEv:
.LFB2708:
	.cfi_startproc
	endbr64
	movzwl	12(%rdi), %esi
	movl	8(%rdi), %edi
	jmp	_ZN6icu_6710util64_powEjt@PLT
	.cfi_endproc
.LFE2708:
	.size	_ZNK6icu_676NFRule10getDivisorEv, .-_ZNK6icu_676NFRule10getDivisorEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRule8doFormatElRNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZNK6icu_676NFRule8doFormatElRNS_13UnicodeStringEiiR10UErrorCode, @function
_ZNK6icu_676NFRule8doFormatElRNS_13UnicodeStringEiiR10UErrorCode:
.LFB2709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -136(%rbp)
	movswl	24(%rdi), %ebx
	leaq	16(%rdi), %rsi
	movl	%r8d, -148(%rbp)
	movq	%r9, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -160(%rbp)
	movq	104(%rdi), %rax
	testw	%bx, %bx
	js	.L503
	sarl	$5, %ebx
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L504
.L505:
	subl	%r8d, %ebx
	movl	$-1, %ecx
	leaq	16(%r15), %rdi
	xorl	%edx, %edx
	movl	%ebx, %r9d
	leaq	_ZN6icu_67L22gDollarOpenParenthesisE(%rip), %rsi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movswl	24(%r15), %r9d
	xorl	%r8d, %r8d
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L509
	testw	%r9w, %r9w
	js	.L510
	movswl	%r9w, %r8d
	sarl	$5, %r8d
.L511:
	cmpl	%ebx, %r8d
	cmovg	%ebx, %r8d
.L509:
	testw	%r9w, %r9w
	js	.L512
	sarl	$5, %r9d
.L513:
	xorl	%edx, %edx
	subl	%r8d, %r9d
	leaq	16(%r15), %rdi
	movl	$-1, %ecx
	leaq	_ZN6icu_67L24gClosedParenthesisDollarE(%rip), %rsi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L514
	sarl	$5, %edx
	movl	%edx, -160(%rbp)
.L515:
	movswl	24(%r15), %ecx
	testw	%cx, %cx
	js	.L516
	sarl	$5, %ecx
	leaq	-128(%rbp), %r14
	subl	$1, %ecx
	cmpl	%eax, %ecx
	jg	.L547
.L518:
	movq	104(%r15), %r9
	movzwl	12(%r15), %esi
	movl	8(%r15), %edi
	movq	%r9, -168(%rbp)
	call	_ZN6icu_6710util64_powEjt@PLT
	xorl	%edx, %edx
	movq	-168(%rbp), %r9
	movq	%r14, %rdi
	movq	%rax, %r8
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rcx
	movq	%r9, %rsi
	divq	%r8
	movl	%eax, %edx
	call	_ZNK6icu_6712PluralFormat6formatEiR10UErrorCode@PLT
	movswl	-120(%rbp), %r9d
	testw	%r9w, %r9w
	js	.L521
	sarl	$5, %r9d
.L522:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r14, %rcx
	movl	%r13d, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testl	%ebx, %ebx
	jg	.L548
	movswl	24(%r15), %r14d
	testw	%r14w, %r14w
	js	.L526
.L550:
	movswl	8(%r12), %eax
	sarl	$5, %r14d
	testw	%ax, %ax
	js	.L528
.L551:
	sarl	$5, %eax
.L529:
	subl	-160(%rbp), %eax
	subl	%eax, %r14d
.L507:
	movq	88(%r15), %rdi
	testq	%rdi, %rdi
	je	.L530
	movq	(%rdi), %rax
	movl	%r13d, %ecx
	movq	48(%rax), %rax
	cmpl	8(%rdi), %ebx
	jge	.L531
	subl	%r14d, %ecx
.L531:
	movq	-144(%rbp), %r9
	movl	-148(%rbp), %r8d
	movq	%r12, %rdx
	movq	-136(%rbp), %rsi
	call	*%rax
.L530:
	movq	80(%r15), %rdi
	testq	%rdi, %rdi
	je	.L502
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpl	8(%rdi), %ebx
	jge	.L533
	subl	%r14d, %r13d
.L533:
	movq	-144(%rbp), %r9
	movl	-148(%rbp), %r8d
	movl	%r13d, %ecx
	movq	%r12, %rdx
	movq	-136(%rbp), %rsi
	call	*%rax
.L502:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L549
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	.cfi_restore_state
	movl	28(%r15), %r9d
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L504:
	movl	%ebx, %r9d
	xorl	%r8d, %r8d
	leaq	16(%r15), %rcx
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L503:
	movl	28(%rdi), %ebx
	testq	%rax, %rax
	je	.L504
	testl	%ebx, %ebx
	movl	$0, %r8d
	cmovle	%ebx, %r8d
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L548:
	movl	%ebx, %ecx
	xorl	%edx, %edx
	leaq	16(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movswl	-120(%rbp), %r9d
	testw	%r9w, %r9w
	js	.L524
	sarl	$5, %r9d
.L525:
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movswl	24(%r15), %r14d
	testw	%r14w, %r14w
	jns	.L550
.L526:
	movswl	8(%r12), %eax
	movl	28(%r15), %r14d
	testw	%ax, %ax
	jns	.L551
.L528:
	movl	12(%r12), %eax
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L521:
	movl	-116(%rbp), %r9d
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L516:
	movl	28(%r15), %ecx
	leaq	-128(%rbp), %r14
	subl	$1, %ecx
	cmpl	%eax, %ecx
	jle	.L518
.L547:
	leal	2(%rax), %edx
	movl	$2147483647, %ecx
	leaq	16(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movswl	-120(%rbp), %r9d
	testw	%r9w, %r9w
	js	.L519
	sarl	$5, %r9d
.L520:
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	%r13d, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L514:
	movl	12(%r12), %esi
	movl	%esi, -160(%rbp)
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L524:
	movl	-116(%rbp), %r9d
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L519:
	movl	-116(%rbp), %r9d
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L510:
	movl	28(%r15), %r8d
	jmp	.L511
.L549:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2709:
	.size	_ZNK6icu_676NFRule8doFormatElRNS_13UnicodeStringEiiR10UErrorCode, .-_ZNK6icu_676NFRule8doFormatElRNS_13UnicodeStringEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRule8doFormatEdRNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZNK6icu_676NFRule8doFormatEdRNS_13UnicodeStringEiiR10UErrorCode, @function
_ZNK6icu_676NFRule8doFormatEdRNS_13UnicodeStringEiiR10UErrorCode:
.LFB2710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	16(%rdi), %rsi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -148(%rbp)
	movswl	24(%rdi), %ebx
	movq	%r8, -144(%rbp)
	movsd	%xmm0, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -160(%rbp)
	movq	104(%rdi), %rax
	testw	%bx, %bx
	js	.L553
	sarl	$5, %ebx
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L554
.L555:
	subl	%r8d, %ebx
	movl	$-1, %ecx
	leaq	16(%r14), %rdi
	xorl	%edx, %edx
	movl	%ebx, %r9d
	leaq	_ZN6icu_67L22gDollarOpenParenthesisE(%rip), %rsi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movswl	24(%r14), %r9d
	xorl	%r8d, %r8d
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L559
	testw	%r9w, %r9w
	js	.L560
	movswl	%r9w, %r8d
	sarl	$5, %r8d
.L561:
	cmpl	%ebx, %r8d
	cmovg	%ebx, %r8d
.L559:
	testw	%r9w, %r9w
	js	.L562
	sarl	$5, %r9d
.L563:
	xorl	%edx, %edx
	subl	%r8d, %r9d
	leaq	16(%r14), %rdi
	movl	$-1, %ecx
	leaq	_ZN6icu_67L24gClosedParenthesisDollarE(%rip), %rsi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L564
	sarl	$5, %edx
	movl	%edx, -160(%rbp)
.L565:
	movswl	24(%r14), %ecx
	testw	%cx, %cx
	js	.L566
	sarl	$5, %ecx
	leaq	-128(%rbp), %r15
	subl	$1, %ecx
	cmpl	%eax, %ecx
	jg	.L607
.L568:
	movsd	-136(%rbp), %xmm1
	comisd	.LC2(%rip), %xmm1
	movzwl	12(%r14), %esi
	movl	8(%r14), %edi
	jb	.L571
	movsd	.LC3(%rip), %xmm0
	comisd	%xmm1, %xmm0
	ja	.L608
.L571:
	call	_ZN6icu_6710util64_powEjt@PLT
	testq	%rax, %rax
	js	.L577
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L578:
	movsd	-136(%rbp), %xmm2
	divsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
.L576:
	cvttsd2sil	%xmm0, %edx
	movq	104(%r14), %rsi
	movq	-144(%rbp), %rcx
	movq	%r15, %rdi
	call	_ZNK6icu_6712PluralFormat6formatEiR10UErrorCode@PLT
	movswl	-120(%rbp), %r9d
	testw	%r9w, %r9w
	js	.L579
	sarl	$5, %r9d
.L580:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r15, %rcx
	movl	%r13d, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testl	%ebx, %ebx
	jg	.L609
	movzwl	24(%r14), %eax
	testw	%ax, %ax
	js	.L584
.L611:
	movswl	%ax, %r15d
	movswl	8(%r12), %eax
	sarl	$5, %r15d
	testw	%ax, %ax
	js	.L586
.L612:
	sarl	$5, %eax
.L587:
	subl	-160(%rbp), %eax
	subl	%eax, %r15d
.L557:
	movq	88(%r14), %rdi
	testq	%rdi, %rdi
	je	.L588
	movq	(%rdi), %rax
	movl	%r13d, %edx
	movq	56(%rax), %rax
	cmpl	8(%rdi), %ebx
	jge	.L589
	subl	%r15d, %edx
.L589:
	movq	-144(%rbp), %r8
	movl	-148(%rbp), %ecx
	movq	%r12, %rsi
	movsd	-136(%rbp), %xmm0
	call	*%rax
.L588:
	movq	80(%r14), %rdi
	testq	%rdi, %rdi
	je	.L552
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	cmpl	8(%rdi), %ebx
	jge	.L591
	subl	%r15d, %r13d
.L591:
	movq	-144(%rbp), %r8
	movl	-148(%rbp), %ecx
	movl	%r13d, %edx
	movq	%r12, %rsi
	movsd	-136(%rbp), %xmm0
	call	*%rax
.L552:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L610
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore_state
	movl	28(%r14), %r9d
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L554:
	movl	%ebx, %r9d
	xorl	%r8d, %r8d
	leaq	16(%r14), %rcx
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L553:
	movl	28(%rdi), %ebx
	testq	%rax, %rax
	je	.L554
	testl	%ebx, %ebx
	movl	$0, %r8d
	cmovle	%ebx, %r8d
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L609:
	movl	%ebx, %ecx
	xorl	%edx, %edx
	leaq	16(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movswl	-120(%rbp), %r9d
	testw	%r9w, %r9w
	js	.L582
	sarl	$5, %r9d
.L583:
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzwl	24(%r14), %eax
	testw	%ax, %ax
	jns	.L611
.L584:
	movswl	8(%r12), %eax
	movl	28(%r14), %r15d
	testw	%ax, %ax
	jns	.L612
.L586:
	movl	12(%r12), %eax
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L579:
	movl	-116(%rbp), %r9d
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L566:
	movl	28(%r14), %ecx
	leaq	-128(%rbp), %r15
	subl	$1, %ecx
	cmpl	%eax, %ecx
	jle	.L568
.L607:
	leal	2(%rax), %edx
	movl	$2147483647, %ecx
	leaq	16(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movswl	-120(%rbp), %r9d
	testw	%r9w, %r9w
	js	.L569
	sarl	$5, %r9d
.L570:
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	xorl	%edx, %edx
	movl	%r13d, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L564:
	movl	12(%r12), %esi
	movl	%esi, -160(%rbp)
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L577:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L582:
	movl	-116(%rbp), %r9d
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L569:
	movl	-116(%rbp), %r9d
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L560:
	movl	28(%r14), %r8d
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L608:
	call	_ZN6icu_6710util64_powEjt@PLT
	testq	%rax, %rax
	js	.L574
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L575:
	mulsd	-136(%rbp), %xmm0
	call	uprv_round_67@PLT
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L574:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L575
.L610:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2710:
	.size	_ZNK6icu_676NFRule8doFormatEdRNS_13UnicodeStringEiiR10UErrorCode, .-_ZNK6icu_676NFRule8doFormatEdRNS_13UnicodeStringEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRule14shouldRollBackEl
	.type	_ZNK6icu_676NFRule14shouldRollBackEl, @function
_ZNK6icu_676NFRule14shouldRollBackEl:
.LFB2711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	80(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L617
	movq	(%rdi), %rax
	call	*112(%rax)
	testb	%al, %al
	jne	.L621
.L617:
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L616
	movq	(%rdi), %rax
	call	*112(%rax)
	testb	%al, %al
	jne	.L621
.L616:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	movzwl	12(%rbx), %esi
	movl	8(%rbx), %edi
	call	_ZN6icu_6710util64_powEjt@PLT
	movq	%rax, %rcx
	movq	%r12, %rax
	cqto
	idivq	%rcx
	testq	%rdx, %rdx
	jne	.L616
	movq	(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	cqto
	idivq	%rcx
	testq	%rdx, %rdx
	setne	%al
	ret
	.cfi_endproc
.LFE2711:
	.size	_ZNK6icu_676NFRule14shouldRollBackEl, .-_ZNK6icu_676NFRule14shouldRollBackEl
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NFRule23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode
	.type	_ZN6icu_676NFRule23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode, @function
_ZN6icu_676NFRule23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode:
.LFB2721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L627
	call	_ZN6icu_6714NFSubstitution23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
.L627:
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L626
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714NFSubstitution23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L626:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2721:
	.size	_ZN6icu_676NFRule23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode, .-_ZN6icu_676NFRule23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_676NFRule8findTextERKNS_13UnicodeStringES3_iPi.part.0, @function
_ZNK6icu_676NFRule8findTextERKNS_13UnicodeStringES3_iPi.part.0:
.LFB3784:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-336(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-176(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$328, %rsp
	movq	%rcx, -344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	104(%r15), %rdi
	movq	%r14, %r8
	movq	%r13, %rcx
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	%r15, %rdx
	movq	%r12, %rsi
	movl	%ebx, -324(%rbp)
	movq	%rax, -336(%rbp)
	movl	$0, -328(%rbp)
	movl	$0, -320(%rbp)
	call	_ZNK6icu_6712PluralFormat9parseTypeERKNS_13UnicodeStringEPKNS_6NFRuleERNS_11FormattableERNS_13FieldPositionE@PLT
	movl	-324(%rbp), %ebx
	testl	%ebx, %ebx
	js	.L634
	movswl	24(%r15), %r9d
	leaq	16(%r15), %r11
	testw	%r9w, %r9w
	js	.L635
	sarl	$5, %r9d
	xorl	%r8d, %r8d
.L636:
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$-1, %ecx
	movq	%r11, -352(%rbp)
	leaq	_ZN6icu_67L22gDollarOpenParenthesisE(%rip), %rsi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	xorl	%r8d, %r8d
	movswl	24(%r15), %r9d
	movq	-352(%rbp), %r11
	testl	%eax, %eax
	movl	%eax, %r10d
	js	.L638
	testw	%r9w, %r9w
	js	.L639
	movswl	%r9w, %r8d
	sarl	$5, %r8d
.L640:
	cmpl	%r10d, %r8d
	cmovg	%r10d, %r8d
.L638:
	testw	%r9w, %r9w
	js	.L641
	sarl	$5, %r9d
.L642:
	subl	%r8d, %r9d
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$-1, %ecx
	leaq	_ZN6icu_67L24gClosedParenthesisDollarE(%rip), %rsi
	movl	%r10d, -364(%rbp)
	leaq	-304(%rbp), %r15
	movq	%r11, -360(%rbp)
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movl	-320(%rbp), %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	-364(%rbp), %r10d
	movq	-360(%rbp), %r11
	movl	%eax, -352(%rbp)
	movl	%esi, -368(%rbp)
	movl	%r10d, %ecx
	movq	%r11, %rsi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movl	-352(%rbp), %edx
	movl	$2147483647, %ecx
	movq	-360(%rbp), %r11
	leaq	-240(%rbp), %r10
	addl	$2, %edx
	movq	%r10, %rdi
	movq	%r11, %rsi
	movq	%r10, -352(%rbp)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movzwl	-296(%rbp), %eax
	movq	-352(%rbp), %r10
	testw	%ax, %ax
	js	.L643
	testb	$1, %al
	jne	.L644
	movswl	%ax, %edx
	movl	%ebx, %esi
	xorl	%r8d, %r8d
	sarl	$5, %edx
	subl	%edx, %esi
.L665:
	movl	%edx, %r9d
	subl	%r8d, %r9d
	cmpl	%edx, %r9d
	cmovg	%edx, %r9d
.L648:
	testb	$2, %al
	leaq	-294(%rbp), %rcx
	cmove	-280(%rbp), %rcx
	movq	%r10, -352(%rbp)
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movq	-352(%rbp), %r10
	testb	%al, %al
	je	.L676
.L650:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L634:
	movq	-344(%rbp), %rax
	movl	$-1, %r12d
	movl	$0, (%rax)
.L663:
	movq	%r14, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L677
	addq	$328, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L635:
	.cfi_restore_state
	movl	28(%r15), %r9d
	movl	$0, %r8d
	testl	%r9d, %r9d
	cmovle	%r9d, %r8d
	subl	%r8d, %r9d
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L643:
	movl	-292(%rbp), %edx
	testb	$1, %al
	je	.L646
.L644:
	movzbl	8(%r12), %eax
	notl	%eax
	andl	$1, %eax
	testb	%al, %al
	jne	.L650
.L676:
	movzwl	-232(%rbp), %eax
	movl	%eax, %ecx
	andl	$1, %ecx
	testw	%ax, %ax
	js	.L651
	testb	%cl, %cl
	je	.L678
.L652:
	movzwl	8(%r12), %eax
	notl	%eax
	andl	$1, %eax
.L655:
	testb	%al, %al
	jne	.L650
	movswl	-296(%rbp), %eax
	movl	%eax, %edx
	sarl	$5, %eax
	testw	%dx, %dx
	movl	-368(%rbp), %edx
	cmovs	-292(%rbp), %eax
	subl	%ebx, %edx
	leal	(%rdx,%rax), %ecx
	movswl	-232(%rbp), %edx
	testw	%dx, %dx
	js	.L679
	sarl	$5, %edx
.L662:
	movq	-344(%rbp), %rsi
	addl	%ecx, %edx
	movq	%r10, %rdi
	subl	%eax, %ebx
	movl	%ebx, %r12d
	movl	%edx, (%rsi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L641:
	movl	28(%r15), %r9d
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L678:
	movswl	%ax, %edx
	xorl	%r8d, %r8d
	sarl	$5, %edx
.L664:
	movl	%edx, %r9d
	subl	%r8d, %r9d
	cmpl	%edx, %r9d
	cmovg	%edx, %r9d
.L656:
	testb	$2, %al
	movl	-368(%rbp), %esi
	movq	%r12, %rdi
	movq	%r10, -352(%rbp)
	leaq	-230(%rbp), %rcx
	cmove	-216(%rbp), %rcx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movq	-352(%rbp), %r10
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L679:
	movl	-228(%rbp), %edx
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L646:
	movl	%ebx, %esi
	movl	$0, %r8d
	subl	%edx, %esi
	testl	%edx, %edx
	cmovle	%edx, %r8d
	jns	.L665
	xorl	%r9d, %r9d
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L651:
	movl	-228(%rbp), %edx
	testb	%cl, %cl
	jne	.L652
	testl	%edx, %edx
	movl	$0, %r8d
	cmovle	%edx, %r8d
	jns	.L664
	xorl	%r9d, %r9d
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L639:
	movl	28(%r15), %r8d
	jmp	.L640
.L677:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3784:
	.size	_ZNK6icu_676NFRule8findTextERKNS_13UnicodeStringES3_iPi.part.0, .-_ZNK6icu_676NFRule8findTextERKNS_13UnicodeStringES3_iPi.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRule12prefixLengthERKNS_13UnicodeStringES3_R10UErrorCode
	.type	_ZNK6icu_676NFRule12prefixLengthERKNS_13UnicodeStringES3_R10UErrorCode, @function
_ZNK6icu_676NFRule12prefixLengthERKNS_13UnicodeStringES3_R10UErrorCode:
.LFB2715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movswl	8(%rdx), %eax
	testw	%ax, %ax
	js	.L681
	sarl	$5, %eax
.L682:
	testl	%eax, %eax
	je	.L746
	movq	96(%r13), %rdi
	leaq	_ZNK6icu_6721RuleBasedNumberFormat9isLenientEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	200(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L685
	movzbl	652(%rdi), %eax
.L686:
	testb	%al, %al
	je	.L687
	movq	96(%r13), %rdi
	call	_ZNK6icu_6721RuleBasedNumberFormat11getCollatorEv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L748
	movq	(%rax), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*312(%rax)
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	movq	0(%r13), %rax
	call	*312(%rax)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L720
	testq	%r14, %r14
	je	.L720
	movl	$0, -60(%rbp)
	leaq	-60(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L744:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6724CollationElementIterator4nextER10UErrorCode@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	%eax, %r13d
	call	_ZN6icu_6724CollationElementIterator4nextER10UErrorCode@PLT
	movl	%eax, %r12d
	cmpl	$-1, %eax
	jne	.L747
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L695:
	cmpl	$-1, %r13d
	je	.L749
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6724CollationElementIterator4nextER10UErrorCode@PLT
	movl	%eax, %r13d
.L747:
	movl	%r13d, %eax
	shrl	$16, %eax
	je	.L695
	movl	%eax, -68(%rbp)
.L696:
	shrl	$16, %r12d
	je	.L700
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L702:
	cmpl	$-1, %eax
	je	.L694
.L700:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6724CollationElementIterator4nextER10UErrorCode@PLT
	movl	%eax, %r12d
	shrl	$16, %r12d
	je	.L702
	cmpl	$-1, %eax
	je	.L694
.L701:
	cmpl	%r12d, -68(%rbp)
	jne	.L703
	cmpl	$-1, %r13d
	jne	.L744
.L703:
	xorl	%r12d, %r12d
.L693:
	movq	%r15, %rdi
	call	_ZN6icu_6724CollationElementIteratorD0Ev@PLT
	jmp	.L692
.L748:
	movl	$7, (%rbx)
	.p2align 4,,10
	.p2align 3
.L746:
	xorl	%r12d, %r12d
.L680:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L750
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L687:
	.cfi_restore_state
	movzwl	8(%r12), %eax
	movl	%eax, %ecx
	andl	$1, %ecx
	testw	%ax, %ax
	js	.L705
	testb	%cl, %cl
	jne	.L706
	movswl	%ax, %edx
	xorl	%r8d, %r8d
	sarl	$5, %edx
.L715:
	movl	%edx, %r9d
	subl	%r8d, %r9d
	cmpl	%edx, %r9d
	cmovg	%edx, %r9d
.L710:
	leaq	10(%r12), %rcx
	testb	$2, %al
	jne	.L712
	movq	24(%r12), %rcx
.L712:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
.L709:
	testb	%al, %al
	jne	.L746
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L714
	movswl	%ax, %r12d
	sarl	$5, %r12d
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L681:
	movl	12(%rdx), %eax
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L705:
	movl	12(%r12), %edx
	testb	%cl, %cl
	jne	.L706
	testl	%edx, %edx
	movl	$0, %r8d
	cmovle	%edx, %r8d
	jns	.L715
	xorl	%r9d, %r9d
	jmp	.L710
.L720:
	movl	$7, (%rbx)
	testq	%r15, %r15
	jne	.L703
	xorl	%r12d, %r12d
.L692:
	testq	%r14, %r14
	je	.L680
	movq	%r14, %rdi
	call	_ZN6icu_6724CollationElementIteratorD0Ev@PLT
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L749:
	movl	$0, -68(%rbp)
	movl	$-1, %r13d
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L706:
	movzwl	8(%r14), %eax
	notl	%eax
	andl	$1, %eax
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L685:
	call	*%rax
	jmp	.L686
.L714:
	movl	12(%r12), %r12d
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L694:
	movq	%r14, %rdi
	call	_ZNK6icu_6724CollationElementIterator9getOffsetEv@PLT
	xorl	%edx, %edx
	cmpl	$-1, %r13d
	setne	%dl
	subl	%edx, %eax
	movl	%eax, %r12d
	jmp	.L693
.L750:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2715:
	.size	_ZNK6icu_676NFRule12prefixLengthERKNS_13UnicodeStringES3_R10UErrorCode, .-_ZNK6icu_676NFRule12prefixLengthERKNS_13UnicodeStringES3_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRule15findTextLenientERKNS_13UnicodeStringES3_iPi
	.type	_ZNK6icu_676NFRule15findTextLenientERKNS_13UnicodeStringES3_iPi, @function
_ZNK6icu_676NFRule15findTextLenientERKNS_13UnicodeStringES3_iPi:
.LFB2719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	leaq	-132(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	movl	$2, %ecx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%rdx, -160(%rbp)
	movq	%r8, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, -120(%rbp)
	movq	%rax, -128(%rbp)
	movl	$0, -132(%rbp)
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L763:
	sarl	$5, %r9d
	cmpl	%r9d, %r12d
	jge	.L754
.L764:
	subl	%r12d, %r9d
	movq	%r15, %rdi
	movl	%r9d, -148(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-120(%rbp), %edx
	movl	-148(%rbp), %r9d
	testw	%dx, %dx
	js	.L755
	sarl	$5, %edx
.L756:
	movl	%r12d, %r8d
	movq	%rbx, %rcx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	-160(%rbp), %rdx
	call	_ZNK6icu_676NFRule12prefixLengthERKNS_13UnicodeStringES3_R10UErrorCode
	movl	-132(%rbp), %edx
	testl	%edx, %edx
	jg	.L754
	testl	%eax, %eax
	jne	.L762
	addl	$1, %r12d
.L759:
	movswl	8(%rbx), %r9d
	testw	%r9w, %r9w
	jns	.L763
	movl	12(%rbx), %r9d
	cmpl	%r9d, %r12d
	jl	.L764
.L754:
	movq	-168(%rbp), %rax
	movl	$-1, %r12d
	movl	$0, (%rax)
.L758:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L765
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L755:
	.cfi_restore_state
	movl	-116(%rbp), %edx
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L762:
	movq	-168(%rbp), %rcx
	movl	%eax, (%rcx)
	jmp	.L758
.L765:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2719:
	.size	_ZNK6icu_676NFRule15findTextLenientERKNS_13UnicodeStringES3_iPi, .-_ZNK6icu_676NFRule15findTextLenientERKNS_13UnicodeStringES3_iPi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRule8findTextERKNS_13UnicodeStringES3_iPi
	.type	_ZNK6icu_676NFRule8findTextERKNS_13UnicodeStringES3_iPi, @function
_ZNK6icu_676NFRule8findTextERKNS_13UnicodeStringES3_iPi:
.LFB2718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	movl	%ecx, %edx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	cmpq	$0, 104(%rdi)
	je	.L767
	addq	$24, %rsp
	movq	%r8, %rcx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676NFRule8findTextERKNS_13UnicodeStringES3_iPi.part.0
	.p2align 4,,10
	.p2align 3
.L767:
	.cfi_restore_state
	movq	96(%rdi), %rdi
	leaq	_ZNK6icu_6721RuleBasedNumberFormat9isLenientEv(%rip), %rcx
	movq	(%rdi), %rax
	movq	200(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L768
	movzbl	652(%rdi), %eax
.L769:
	testb	%al, %al
	jne	.L770
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L771
	sarl	$5, %eax
.L772:
	movl	%eax, (%r8)
	xorl	%r8d, %r8d
	movswl	8(%r14), %r9d
	testl	%edx, %edx
	js	.L774
	testw	%r9w, %r9w
	js	.L775
	movswl	%r9w, %r8d
	sarl	$5, %r8d
.L776:
	cmpl	%r8d, %edx
	cmovle	%edx, %r8d
.L774:
	testw	%r9w, %r9w
	js	.L777
	sarl	$5, %r9d
.L778:
	movzwl	8(%r13), %eax
	movl	%eax, %edx
	andl	$1, %edx
	testw	%ax, %ax
	js	.L779
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testb	%dl, %dl
	jne	.L766
.L780:
	testl	%ecx, %ecx
	je	.L766
	leaq	10(%r13), %rsi
	testb	$2, %al
	jne	.L783
	movq	24(%r13), %rsi
.L783:
	addq	$24, %rsp
	movq	%r14, %rdi
	subl	%r8d, %r9d
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	.p2align 4,,10
	.p2align 3
.L771:
	.cfi_restore_state
	movl	12(%r13), %eax
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L770:
	addq	$24, %rsp
	movl	%edx, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676NFRule15findTextLenientERKNS_13UnicodeStringES3_iPi
	.p2align 4,,10
	.p2align 3
.L779:
	.cfi_restore_state
	movl	12(%r13), %ecx
	testb	%dl, %dl
	jne	.L766
	testl	%ecx, %ecx
	jns	.L780
.L766:
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L777:
	.cfi_restore_state
	movl	12(%r14), %r9d
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L768:
	movq	%r8, -48(%rbp)
	movl	%edx, -36(%rbp)
	call	*%rax
	movq	-48(%rbp), %r8
	movl	-36(%rbp), %edx
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L775:
	movl	12(%r14), %r8d
	jmp	.L776
	.cfi_endproc
.LFE2718:
	.size	_ZNK6icu_676NFRule8findTextERKNS_13UnicodeStringES3_iPi, .-_ZNK6icu_676NFRule8findTextERKNS_13UnicodeStringES3_iPi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRule11stripPrefixERNS_13UnicodeStringERKS1_RNS_13ParsePositionE
	.type	_ZNK6icu_676NFRule11stripPrefixERNS_13UnicodeStringERKS1_RNS_13ParsePositionE, @function
_ZNK6icu_676NFRule11stripPrefixERNS_13UnicodeStringERKS1_RNS_13ParsePositionE:
.LFB2713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rcx, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movswl	8(%rdx), %eax
	testw	%ax, %ax
	js	.L793
	sarl	$5, %eax
	testl	%eax, %eax
	jne	.L808
.L792:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L809
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L793:
	.cfi_restore_state
	movl	12(%rdx), %eax
	testl	%eax, %eax
	je	.L792
.L808:
	leaq	-28(%rbp), %rcx
	movq	%r12, %rsi
	movl	$0, -28(%rbp)
	call	_ZNK6icu_676NFRule12prefixLengthERKNS_13UnicodeStringES3_R10UErrorCode
	movl	-28(%rbp), %edx
	testl	%edx, %edx
	jg	.L792
	testl	%eax, %eax
	je	.L792
	addl	%eax, 8(%rbx)
	cmpl	$2147483647, %eax
	jne	.L798
	movzwl	8(%r12), %edx
	movl	$2, %eax
	movl	%edx, %ecx
	andl	$31, %ecx
	andl	$1, %edx
	cmove	%ecx, %eax
	movw	%ax, 8(%r12)
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L798:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%eax, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L792
.L809:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2713:
	.size	_ZNK6icu_676NFRule11stripPrefixERNS_13UnicodeStringERKS1_RNS_13ParsePositionE, .-_ZNK6icu_676NFRule11stripPrefixERNS_13UnicodeStringERKS1_RNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRule12allIgnorableERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_676NFRule12allIgnorableERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_676NFRule12allIgnorableERKNS_13UnicodeStringER10UErrorCode:
.LFB2720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movswl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L811
	sarl	$5, %eax
.L812:
	movl	$1, %r13d
	testl	%eax, %eax
	je	.L810
	movq	96(%r14), %rdi
	leaq	_ZNK6icu_6721RuleBasedNumberFormat9isLenientEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	200(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L814
	movzbl	652(%rdi), %r13d
.L815:
	testb	%r13b, %r13b
	jne	.L841
.L810:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L842
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L841:
	.cfi_restore_state
	movq	96(%r14), %rdi
	call	_ZNK6icu_6721RuleBasedNumberFormat11getCollatorEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L818
	movq	(%rax), %rax
	movq	%r12, %rsi
	call	*312(%rax)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L818
	movl	$0, -44(%rbp)
	leaq	-44(%rbp), %rbx
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L822:
	shrl	$16, %eax
	jne	.L826
.L840:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6724CollationElementIterator4nextER10UErrorCode@PLT
	cmpl	$-1, %eax
	jne	.L822
	movl	$1, %r13d
.L820:
	movq	%r12, %rdi
	call	_ZN6icu_6724CollationElementIteratorD0Ev@PLT
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L811:
	movl	12(%rsi), %eax
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L818:
	movl	$7, (%rbx)
	xorl	%r13d, %r13d
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L814:
	call	*%rax
	movl	%eax, %r13d
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L826:
	xorl	%r13d, %r13d
	jmp	.L820
.L842:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2720:
	.size	_ZNK6icu_676NFRule12allIgnorableERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_676NFRule12allIgnorableERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRule16matchToDelimiterERKNS_13UnicodeStringEidS3_RNS_13ParsePositionEPKNS_14NFSubstitutionEjd
	.type	_ZNK6icu_676NFRule16matchToDelimiterERKNS_13UnicodeStringEidS3_RNS_13ParsePositionEPKNS_14NFSubstitutionEjd, @function
_ZNK6icu_676NFRule16matchToDelimiterERKNS_13UnicodeStringEidS3_RNS_13ParsePositionEPKNS_14NFSubstitutionEjd:
.LFB2714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rcx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	leaq	-264(%rbp), %rdx
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -288(%rbp)
	movq	%r8, -296(%rbp)
	movsd	%xmm0, -280(%rbp)
	movsd	%xmm1, -312(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -264(%rbp)
	call	_ZNK6icu_676NFRule12allIgnorableERKNS_13UnicodeStringER10UErrorCode
	testb	%al, %al
	jne	.L844
	movl	-264(%rbp), %edx
	pxor	%xmm0, %xmm0
	testl	%edx, %edx
	jle	.L878
.L843:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L879
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L844:
	.cfi_restore_state
	movsd	-280(%rbp), %xmm0
	testq	%r15, %r15
	je	.L843
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rax, -256(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rax, -248(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	(%r15), %rax
	movq	96(%r13), %rdi
	leaq	_ZNK6icu_6721RuleBasedNumberFormat9isLenientEv(%rip), %rdx
	movq	80(%rax), %rbx
	movq	(%rdi), %rax
	movq	200(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L859
	movsbl	652(%rdi), %ecx
.L860:
	leaq	-256(%rbp), %rax
	movl	16(%rbp), %r8d
	movq	-304(%rbp), %r9
	movq	%r14, %rsi
	movq	%rax, -320(%rbp)
	movsd	-312(%rbp), %xmm1
	movq	%rax, %rdx
	movq	%r15, %rdi
	movsd	-280(%rbp), %xmm0
	call	*%rbx
	testb	%al, %al
	je	.L861
	movl	-248(%rbp), %eax
	testl	%eax, %eax
	je	.L861
	movq	-296(%rbp), %rbx
	movsd	-168(%rbp), %xmm0
	movl	%eax, 8(%rbx)
.L863:
	movq	-304(%rbp), %rdi
	movsd	%xmm0, -280(%rbp)
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	-320(%rbp), %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movsd	-280(%rbp), %xmm0
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L878:
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rax, -256(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rax, -248(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movl	%r12d, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-260(%rbp), %rax
	movq	-288(%rbp), %rdx
	movq	%rax, %r8
	movq	%rax, -328(%rbp)
	call	_ZNK6icu_676NFRule8findTextERKNS_13UnicodeStringES3_iPi
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L869
	leaq	-256(%rbp), %rax
	leaq	-240(%rbp), %r12
	movq	%rax, -320(%rbp)
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L881:
	sarl	$5, %eax
.L850:
	testl	%eax, %eax
	jle	.L851
	movq	(%r15), %rax
	movq	96(%r13), %rdi
	leaq	_ZNK6icu_6721RuleBasedNumberFormat9isLenientEv(%rip), %rsi
	movq	80(%rax), %r10
	movq	(%rdi), %rax
	movq	200(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L852
	movsbl	652(%rdi), %ecx
.L853:
	movq	-304(%rbp), %r9
	movl	16(%rbp), %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	movsd	-312(%rbp), %xmm1
	movsd	-280(%rbp), %xmm0
	movq	-320(%rbp), %rdx
	call	*%r10
	testb	%al, %al
	je	.L854
	cmpl	-248(%rbp), %ebx
	je	.L880
.L854:
	movl	-244(%rbp), %eax
	testl	%eax, %eax
	jg	.L877
	movl	-248(%rbp), %eax
.L877:
	movq	-296(%rbp), %rsi
	movl	%eax, 12(%rsi)
.L851:
	addl	-260(%rbp), %ebx
	movq	-328(%rbp), %r8
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	-288(%rbp), %rdx
	movl	%ebx, %ecx
	movl	$0, -248(%rbp)
	call	_ZNK6icu_676NFRule8findTextERKNS_13UnicodeStringES3_iPi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testl	%ebx, %ebx
	js	.L846
.L857:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movl	$2, %eax
	movw	%ax, -232(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-232(%rbp), %edx
	testw	%dx, %dx
	js	.L847
	sarl	$5, %edx
.L848:
	movl	%ebx, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movswl	-232(%rbp), %eax
	testw	%ax, %ax
	jns	.L881
	movl	-228(%rbp), %eax
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L861:
	movl	-244(%rbp), %eax
	movq	-296(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movl	%eax, 12(%rbx)
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L847:
	movl	-228(%rbp), %edx
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L852:
	movq	%r10, -336(%rbp)
	call	*%rax
	movq	-336(%rbp), %r10
	movsbl	%al, %ecx
	jmp	.L853
.L869:
	leaq	-256(%rbp), %rax
	movq	%rax, -320(%rbp)
	.p2align 4,,10
	.p2align 3
.L846:
	movq	-296(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$0, 8(%rax)
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L859:
	call	*%rax
	movsbl	%al, %ecx
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L880:
	movq	-296(%rbp), %rax
	addl	-260(%rbp), %ebx
	movq	%r12, %rdi
	movsd	-168(%rbp), %xmm0
	movl	%ebx, 8(%rax)
	movsd	%xmm0, -280(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movsd	-280(%rbp), %xmm0
	jmp	.L863
.L879:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2714:
	.size	_ZNK6icu_676NFRule16matchToDelimiterERKNS_13UnicodeStringEidS3_RNS_13ParsePositionEPKNS_14NFSubstitutionEjd, .-_ZNK6icu_676NFRule16matchToDelimiterERKNS_13UnicodeStringEidS3_RNS_13ParsePositionEPKNS_14NFSubstitutionEjd
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NFRule7doParseERKNS_13UnicodeStringERNS_13ParsePositionEadjRNS_11FormattableE
	.type	_ZNK6icu_676NFRule7doParseERKNS_13UnicodeStringERNS_13ParsePositionEadjRNS_11FormattableE, @function
_ZNK6icu_676NFRule7doParseERKNS_13UnicodeStringERNS_13ParsePositionEadjRNS_11FormattableE:
.LFB2712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$424, %rsp
	movq	%rdx, -416(%rbp)
	movl	%ecx, -460(%rbp)
	movl	%r8d, -380(%rbp)
	movq	%r9, -456(%rbp)
	movsd	%xmm0, -376(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rax, -352(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rax, -344(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.L883
	movl	8(%rax), %eax
	movl	%eax, -356(%rbp)
	movq	88(%rbx), %rax
	testq	%rax, %rax
	je	.L960
.L884:
	movl	8(%rax), %eax
	movl	%eax, -360(%rbp)
.L889:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r14d
	movq	%rax, -256(%rbp)
	leaq	16(%rbx), %rax
	movq	%rax, -368(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, %rdi
	movw	%r14w, -248(%rbp)
	movq	%rax, -440(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-248(%rbp), %edx
	testw	%dx, %dx
	js	.L891
	sarl	$5, %edx
.L892:
	movl	-356(%rbp), %r9d
	xorl	%r8d, %r8d
	leaq	16(%rbx), %rcx
	xorl	%esi, %esi
	movq	-440(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movswl	-248(%rbp), %eax
	testw	%ax, %ax
	js	.L893
	sarl	$5, %eax
	testl	%eax, %eax
	jne	.L895
.L958:
	movzwl	-312(%rbp), %eax
	movl	-344(%rbp), %esi
.L896:
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L902
	sarl	$5, %edx
.L903:
	testw	%ax, %ax
	js	.L904
	cwtl
	sarl	$5, %eax
.L905:
	testl	%esi, %esi
	jne	.L906
	movl	-356(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L961
.L906:
	movq	(%rbx), %rcx
	cmpq	$-5, %rcx
	je	.L962
	cmpq	$-6, %rcx
	je	.L963
	subl	%eax, %edx
	testq	%rcx, %rcx
	movl	$0, %eax
	movl	$2, %r9d
	cmovs	%rax, %rcx
	pxor	%xmm2, %xmm2
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	%edx, -432(%rbp)
	movq	%rax, -192(%rbp)
	movl	-360(%rbp), %eax
	pxor	%xmm3, %xmm3
	xorl	%r12d, %r12d
	subl	-356(%rbp), %eax
	cvtsi2sdq	%rcx, %xmm2
	movw	%r9w, -184(%rbp)
	leaq	-192(%rbp), %r13
	movl	%eax, -384(%rbp)
	leaq	-352(%rbp), %rax
	movl	$0, -428(%rbp)
	movq	%rax, -408(%rbp)
	movsd	%xmm2, -424(%rbp)
	movsd	%xmm3, -448(%rbp)
	.p2align 4,,10
	.p2align 3
.L910:
	movl	$0, -344(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-184(%rbp), %edx
	testw	%dx, %dx
	js	.L911
	sarl	$5, %edx
.L912:
	movl	-356(%rbp), %r14d
	movl	-384(%rbp), %r9d
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	-368(%rbp), %rcx
	movl	%r14d, %r8d
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	subq	$8, %rsp
	movq	80(%rbx), %r9
	movl	%r12d, %edx
	movl	-380(%rbp), %eax
	movq	%rbx, %rdi
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	-408(%rbp), %r8
	movsd	-376(%rbp), %xmm1
	pushq	%rax
	movsd	-424(%rbp), %xmm0
	call	_ZNK6icu_676NFRule16matchToDelimiterERKNS_13UnicodeStringEidS3_RNS_13ParsePositionEPKNS_14NFSubstitutionEjd
	movl	-344(%rbp), %r12d
	popq	%rdi
	movsd	%xmm0, -392(%rbp)
	popq	%r8
	testl	%r12d, %r12d
	jne	.L913
	cmpq	$0, 80(%rbx)
	je	.L913
	movl	-340(%rbp), %eax
	movq	-416(%rbp), %rsi
	addl	%r14d, %eax
	cmpl	12(%rsi), %eax
	jle	.L929
	movl	%eax, 12(%rsi)
.L929:
	movq	-416(%rbp), %rcx
	movl	-428(%rbp), %eax
	movl	%eax, 8(%rcx)
	testl	%eax, %eax
	je	.L934
	cmpb	$0, -460(%rbp)
	movl	$0, 12(%rcx)
	je	.L934
	cmpq	$0, 80(%rbx)
	je	.L964
.L934:
	movq	-456(%rbp), %rdi
	movsd	-448(%rbp), %xmm0
	call	_ZN6icu_6711Formattable9setDoubleEd@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L907:
	movq	-440(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-408(%rbp), %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L965
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L913:
	.cfi_restore_state
	movswl	-312(%rbp), %r9d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %esi
	movq	%rax, -128(%rbp)
	movw	%si, -120(%rbp)
	testw	%r9w, %r9w
	js	.L916
	sarl	$5, %r9d
.L917:
	leaq	-128(%rbp), %r14
	subl	%r12d, %r9d
	movq	%r14, %rdi
	movl	%r9d, -400(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-120(%rbp), %edx
	movl	-400(%rbp), %r9d
	testw	%dx, %dx
	js	.L918
	sarl	$5, %edx
.L919:
	movl	%r12d, %r8d
	movq	%r15, %rcx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movswl	24(%rbx), %r9d
	movq	%rax, -336(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rax, -328(%rbp)
	testw	%r9w, %r9w
	js	.L920
	sarl	$5, %r9d
.L921:
	subl	-360(%rbp), %r9d
	movq	%r13, %rdi
	movl	%r9d, -400(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-184(%rbp), %edx
	movl	-400(%rbp), %r9d
	testw	%dx, %dx
	js	.L922
	sarl	$5, %edx
.L923:
	movl	-360(%rbp), %r8d
	movq	-368(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movl	-380(%rbp), %eax
	xorl	%edx, %edx
	movq	%r13, %rcx
	subq	$8, %rsp
	movq	88(%rbx), %r9
	movq	%r14, %rsi
	movq	%rbx, %rdi
	pushq	%rax
	movsd	-376(%rbp), %xmm1
	leaq	-336(%rbp), %r8
	movsd	-392(%rbp), %xmm0
	movq	%r8, -400(%rbp)
	call	_ZNK6icu_676NFRule16matchToDelimiterERKNS_13UnicodeStringEidS3_RNS_13ParsePositionEPKNS_14NFSubstitutionEjd
	movl	-328(%rbp), %edx
	popq	%rax
	movq	-400(%rbp), %r8
	movl	-344(%rbp), %eax
	testl	%edx, %edx
	popq	%rcx
	jne	.L925
	cmpq	$0, 88(%rbx)
	je	.L925
	movq	-416(%rbp), %rsi
	movl	-356(%rbp), %edx
	addl	-324(%rbp), %edx
	addl	%edx, %eax
	cmpl	12(%rsi), %eax
	jle	.L927
	movl	%eax, 12(%rsi)
	.p2align 4,,10
	.p2align 3
.L927:
	movq	%r8, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-356(%rbp), %esi
	cmpl	%esi, -360(%rbp)
	je	.L929
	movl	-344(%rbp), %edx
	testl	%edx, %edx
	jle	.L929
	movswl	-312(%rbp), %eax
	testw	%ax, %ax
	js	.L930
	sarl	$5, %eax
	cmpl	%eax, %edx
	jge	.L929
.L959:
	cmpl	%edx, %r12d
	jne	.L910
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L925:
	addl	-432(%rbp), %eax
	addl	%edx, %eax
	cmpl	-428(%rbp), %eax
	jle	.L927
	movl	%eax, -428(%rbp)
	movsd	%xmm0, -448(%rbp)
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L911:
	movl	-180(%rbp), %edx
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L930:
	cmpl	-308(%rbp), %edx
	jl	.L959
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L916:
	movl	-308(%rbp), %r9d
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L922:
	movl	-180(%rbp), %edx
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L920:
	movl	28(%rbx), %r9d
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L918:
	movl	-116(%rbp), %edx
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L960:
	movswl	24(%rbx), %edx
	movl	%edx, %eax
	sarl	$5, %edx
	movl	%edx, -360(%rbp)
	testw	%ax, %ax
	jns	.L889
	movl	28(%rbx), %eax
	movl	%eax, -360(%rbp)
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L891:
	movl	-244(%rbp), %edx
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L904:
	movl	-308(%rbp), %eax
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L902:
	movl	12(%r12), %edx
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L893:
	movl	-244(%rbp), %eax
	testl	%eax, %eax
	je	.L958
.L895:
	movq	-440(%rbp), %rdx
	leaq	-336(%rbp), %rcx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movl	$0, -336(%rbp)
	call	_ZNK6icu_676NFRule12prefixLengthERKNS_13UnicodeStringES3_R10UErrorCode
	movl	-336(%rbp), %r13d
	testl	%r13d, %r13d
	jg	.L958
	movl	-344(%rbp), %esi
	testl	%eax, %eax
	jne	.L898
	movzwl	-312(%rbp), %eax
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L883:
	movswl	24(%rbx), %edx
	movq	88(%rbx), %rax
	testw	%dx, %dx
	js	.L886
	sarl	$5, %edx
	movl	%edx, -356(%rbp)
	testq	%rax, %rax
	jne	.L884
	movl	%edx, -360(%rbp)
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L886:
	movl	28(%rbx), %esi
	movl	%esi, -356(%rbp)
	movl	%esi, -360(%rbp)
	testq	%rax, %rax
	jne	.L884
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L964:
	movsd	.LC3(%rip), %xmm0
	divsd	-448(%rbp), %xmm0
	movsd	%xmm0, -448(%rbp)
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L961:
	movl	-340(%rbp), %eax
	movq	-416(%rbp), %rbx
	movq	-456(%rbp), %rdi
	movl	%eax, 12(%rbx)
	call	_ZN6icu_6711Formattable7setLongEi@PLT
	leaq	-352(%rbp), %rax
	movq	%rax, -408(%rbp)
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L962:
	movq	-416(%rbp), %rax
	movl	%esi, 8(%rax)
	call	uprv_getInfinity_67@PLT
	movq	-456(%rbp), %rdi
	call	_ZN6icu_6711Formattable9setDoubleEd@PLT
	leaq	-352(%rbp), %rax
	movq	%rax, -408(%rbp)
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L963:
	movq	-416(%rbp), %rax
	movl	%esi, 8(%rax)
	call	uprv_getNaN_67@PLT
	movq	-456(%rbp), %rdi
	call	_ZN6icu_6711Formattable9setDoubleEd@PLT
	leaq	-352(%rbp), %rax
	movq	%rax, -408(%rbp)
	jmp	.L907
.L898:
	addl	%eax, %esi
	movl	%esi, -344(%rbp)
	cmpl	$2147483647, %eax
	jne	.L900
	movzwl	-312(%rbp), %eax
	testb	$1, %al
	je	.L901
	movl	$2, %r11d
	movl	$2, %eax
	movw	%r11w, -312(%rbp)
	jmp	.L896
.L900:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%eax, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L958
.L901:
	andl	$31, %eax
	movw	%ax, -312(%rbp)
	jmp	.L896
.L965:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2712:
	.size	_ZNK6icu_676NFRule7doParseERKNS_13UnicodeStringERNS_13ParsePositionEadjRNS_11FormattableE, .-_ZNK6icu_676NFRule7doParseERKNS_13UnicodeStringERNS_13ParsePositionEadjRNS_11FormattableE
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZN6icu_67L13RULE_PREFIXESE, @object
	.size	_ZN6icu_67L13RULE_PREFIXESE, 96
_ZN6icu_67L13RULE_PREFIXESE:
	.quad	_ZN6icu_67L9gLessLessE
	.quad	_ZN6icu_67L12gLessPercentE
	.quad	_ZN6icu_67L9gLessHashE
	.quad	_ZN6icu_67L9gLessZeroE
	.quad	_ZN6icu_67L15gGreaterGreaterE
	.quad	_ZN6icu_67L15gGreaterPercentE
	.quad	_ZN6icu_67L12gGreaterHashE
	.quad	_ZN6icu_67L12gGreaterZeroE
	.quad	_ZN6icu_67L13gEqualPercentE
	.quad	_ZN6icu_67L10gEqualHashE
	.quad	_ZN6icu_67L10gEqualZeroE
	.quad	0
	.section	.rodata
	.align 8
	.type	_ZN6icu_67L22gGreaterGreaterGreaterE, @object
	.size	_ZN6icu_67L22gGreaterGreaterGreaterE, 8
_ZN6icu_67L22gGreaterGreaterGreaterE:
	.value	62
	.value	62
	.value	62
	.value	0
	.align 2
	.type	_ZN6icu_67L10gEqualZeroE, @object
	.size	_ZN6icu_67L10gEqualZeroE, 6
_ZN6icu_67L10gEqualZeroE:
	.value	61
	.value	48
	.value	0
	.align 2
	.type	_ZN6icu_67L10gEqualHashE, @object
	.size	_ZN6icu_67L10gEqualHashE, 6
_ZN6icu_67L10gEqualHashE:
	.value	61
	.value	35
	.value	0
	.align 2
	.type	_ZN6icu_67L13gEqualPercentE, @object
	.size	_ZN6icu_67L13gEqualPercentE, 6
_ZN6icu_67L13gEqualPercentE:
	.value	61
	.value	37
	.value	0
	.align 2
	.type	_ZN6icu_67L12gGreaterZeroE, @object
	.size	_ZN6icu_67L12gGreaterZeroE, 6
_ZN6icu_67L12gGreaterZeroE:
	.value	62
	.value	48
	.value	0
	.align 2
	.type	_ZN6icu_67L12gGreaterHashE, @object
	.size	_ZN6icu_67L12gGreaterHashE, 6
_ZN6icu_67L12gGreaterHashE:
	.value	62
	.value	35
	.value	0
	.align 2
	.type	_ZN6icu_67L15gGreaterPercentE, @object
	.size	_ZN6icu_67L15gGreaterPercentE, 6
_ZN6icu_67L15gGreaterPercentE:
	.value	62
	.value	37
	.value	0
	.align 2
	.type	_ZN6icu_67L15gGreaterGreaterE, @object
	.size	_ZN6icu_67L15gGreaterGreaterE, 6
_ZN6icu_67L15gGreaterGreaterE:
	.value	62
	.value	62
	.value	0
	.align 2
	.type	_ZN6icu_67L9gLessZeroE, @object
	.size	_ZN6icu_67L9gLessZeroE, 6
_ZN6icu_67L9gLessZeroE:
	.value	60
	.value	48
	.value	0
	.align 2
	.type	_ZN6icu_67L9gLessHashE, @object
	.size	_ZN6icu_67L9gLessHashE, 6
_ZN6icu_67L9gLessHashE:
	.value	60
	.value	35
	.value	0
	.align 2
	.type	_ZN6icu_67L12gLessPercentE, @object
	.size	_ZN6icu_67L12gLessPercentE, 6
_ZN6icu_67L12gLessPercentE:
	.value	60
	.value	37
	.value	0
	.align 2
	.type	_ZN6icu_67L9gLessLessE, @object
	.size	_ZN6icu_67L9gLessLessE, 6
_ZN6icu_67L9gLessLessE:
	.value	60
	.value	60
	.value	0
	.align 2
	.type	_ZN6icu_67L24gClosedParenthesisDollarE, @object
	.size	_ZN6icu_67L24gClosedParenthesisDollarE, 6
_ZN6icu_67L24gClosedParenthesisDollarE:
	.value	41
	.value	36
	.value	0
	.align 2
	.type	_ZN6icu_67L22gDollarOpenParenthesisE, @object
	.size	_ZN6icu_67L22gDollarOpenParenthesisE, 6
_ZN6icu_67L22gDollarOpenParenthesisE:
	.value	36
	.value	40
	.value	0
	.align 8
	.type	_ZN6icu_67L4gNaNE, @object
	.size	_ZN6icu_67L4gNaNE, 8
_ZN6icu_67L4gNaNE:
	.value	78
	.value	97
	.value	78
	.value	0
	.align 8
	.type	_ZN6icu_67L4gInfE, @object
	.size	_ZN6icu_67L4gInfE, 8
_ZN6icu_67L4gInfE:
	.value	73
	.value	110
	.value	102
	.value	0
	.align 2
	.type	_ZN6icu_67L7gMinusXE, @object
	.size	_ZN6icu_67L7gMinusXE, 6
_ZN6icu_67L7gMinusXE:
	.value	45
	.value	120
	.value	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	0
	.align 8
.LC3:
	.long	0
	.long	1072693248
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
