	.file	"collationcompare.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationCompare21compareUpToQuaternaryERNS_17CollationIteratorES2_RKNS_17CollationSettingsER10UErrorCode
	.type	_ZN6icu_6716CollationCompare21compareUpToQuaternaryERNS_17CollationIteratorES2_RKNS_17CollationSettingsER10UErrorCode, @function
_ZN6icu_6716CollationCompare21compareUpToQuaternaryERNS_17CollationIteratorES2_RKNS_17CollationSettingsER10UErrorCode:
.LFB3255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -80(%rbp)
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L82
	movq	%rdi, %r15
	movl	24(%rdx), %edi
	movl	$0, -68(%rbp)
	movq	%rsi, %r13
	movq	%rcx, %rbx
	movl	%edi, -72(%rbp)
	andl	$12, %edi
	jne	.L269
.L4:
	xorl	%r14d, %r14d
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L271:
	leal	1(%rax), %edx
	movl	%edx, 368(%r15)
	movq	32(%r15), %rdx
	movq	(%rdx,%rax,8), %rcx
.L6:
	movq	%rcx, %rax
	sarq	$32, %rax
	movl	%eax, %r12d
	cmpl	%eax, -68(%rbp)
	jbe	.L19
	cmpl	$33554432, %eax
	ja	.L270
.L19:
	testl	%eax, %eax
	jne	.L72
.L38:
	movslq	368(%r15), %rax
	movl	24(%r15), %edx
	cmpl	%edx, %eax
	jl	.L271
	cmpl	$39, %edx
	jle	.L7
	leaq	24(%r15), %rdi
	movq	%rbx, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L8
	movl	24(%r15), %edx
.L7:
	movq	(%r15), %rax
	addl	$1, %edx
	leaq	-60(%rbp), %rsi
	movq	%r15, %rdi
	movl	%edx, 24(%r15)
	movq	%rbx, %rdx
	call	*64(%rax)
	movl	%eax, %ecx
	movzbl	%al, %edx
	cmpb	$-65, %al
	ja	.L9
	movabsq	$-281474976710656, %rdi
	movq	%rcx, %rax
	sall	$16, %ecx
	salq	$32, %rax
	andl	$-16777216, %ecx
	sall	$8, %edx
	andq	%rdi, %rax
	orq	%rax, %rcx
	movl	%edx, %eax
.L255:
	orq	%rax, %rcx
.L256:
	movslq	368(%r15), %rax
	leal	1(%rax), %edx
	movl	%edx, 368(%r15)
	movq	32(%r15), %rdx
	movq	%rcx, (%rdx,%rax,8)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L273:
	leal	1(%rax), %edx
	movl	%edx, 368(%r13)
	movq	32(%r13), %rdx
	movq	(%rdx,%rax,8), %rcx
.L40:
	movq	%rcx, %rax
	sarq	$32, %rax
	movl	%eax, %r8d
	cmpl	%eax, -68(%rbp)
	jbe	.L53
	cmpl	$33554432, %eax
	ja	.L272
.L53:
	testl	%eax, %eax
	jne	.L70
.L72:
	movslq	368(%r13), %rax
	movl	24(%r13), %edx
	cmpl	%edx, %eax
	jl	.L273
	cmpl	$39, %edx
	jle	.L41
	leaq	24(%r13), %rdi
	movq	%rbx, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L42
	movl	24(%r13), %edx
.L41:
	movq	0(%r13), %rax
	addl	$1, %edx
	leaq	-60(%rbp), %rsi
	movq	%r13, %rdi
	movl	%edx, 24(%r13)
	movq	%rbx, %rdx
	call	*64(%rax)
	movl	%eax, %ecx
	movzbl	%al, %edx
	cmpb	$-65, %al
	ja	.L43
	movabsq	$-281474976710656, %rdi
	movq	%rcx, %rax
	sall	$16, %ecx
	salq	$32, %rax
	andl	$-16777216, %ecx
	sall	$8, %edx
	andq	%rdi, %rax
	orq	%rax, %rcx
	movl	%edx, %eax
.L262:
	orq	%rax, %rcx
.L263:
	movslq	368(%r13), %rax
	leal	1(%rax), %edx
	movl	%edx, 368(%r13)
	movq	32(%r13), %rdx
	movq	%rcx, (%rdx,%rax,8)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L270:
	leaq	-60(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L37:
	movslq	368(%r15), %rax
	movq	32(%r15), %rsi
	movabsq	$-4294967296, %rdi
	andq	%rdi, %rcx
	leal	-1(%rax), %edx
	movslq	%edx, %rdx
	movq	%rcx, (%rsi,%rdx,8)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L274:
	leal	1(%rax), %edx
	movq	(%rsi,%rax,8), %rcx
	movl	%edx, 368(%r15)
.L21:
	movq	%rcx, %rax
	sarq	$32, %rax
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L34
.L276:
	movslq	368(%r15), %rax
	movq	32(%r15), %rsi
	leal	-1(%rax), %edx
	movslq	%edx, %rdx
	movq	$0, (%rsi,%rdx,8)
.L35:
	movl	24(%r15), %edx
	cmpl	%eax, %edx
	jg	.L274
	cmpl	$39, %edx
	jle	.L22
	leaq	24(%r15), %rdi
	movq	%rbx, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L23
	movl	24(%r15), %edx
.L22:
	movq	(%r15), %rax
	addl	$1, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%edx, 24(%r15)
	movq	%rbx, %rdx
	call	*64(%rax)
	movzbl	%al, %edx
	cmpb	$-65, %al
	jbe	.L258
	cmpl	$192, %edx
	je	.L275
	movq	16(%r15), %rsi
.L32:
	cmpl	$193, %edx
	jne	.L33
	leal	-193(%rax), %ecx
	salq	$32, %rcx
	orq	$83887360, %rcx
.L259:
	movslq	368(%r15), %rax
	leal	1(%rax), %edx
	movl	%edx, 368(%r15)
	movq	32(%r15), %rdx
	movq	%rcx, (%rdx,%rax,8)
	movq	%rcx, %rax
	sarq	$32, %rax
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L276
.L34:
	cmpl	%eax, -68(%rbp)
	jbe	.L147
	cmpl	$33554432, %eax
	ja	.L37
.L147:
	movl	$1, %r14d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L275:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	js	.L277
	movq	16(%r15), %rdx
	movq	32(%rdx), %rsi
	movq	(%rsi), %rdi
	movq	16(%rdi), %rcx
	cmpl	$55295, %eax
	jg	.L27
	movl	%eax, %edx
	movq	(%rdi), %rdi
	sarl	$5, %edx
	movslq	%edx, %rdx
.L257:
	movzwl	(%rdi,%rdx,2), %edx
	andl	$31, %eax
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L28:
	movl	(%rcx,%rdx), %eax
	movzbl	%al, %edx
	cmpb	$-65, %al
	ja	.L32
	.p2align 4,,10
	.p2align 3
.L258:
	movabsq	$-281474976710656, %rdi
	movq	%rax, %rcx
	sall	$16, %eax
	salq	$32, %rcx
	andl	$-16777216, %eax
	sall	$8, %edx
	andq	%rdi, %rcx
	orq	%rax, %rcx
	movl	%edx, %eax
	orq	%rax, %rcx
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L277:
	movslq	368(%r15), %rax
	movabsq	$4311744768, %rdi
	leal	1(%rax), %edx
	movl	%edx, 368(%r15)
	movq	32(%r15), %rdx
	movq	%rdi, (%rdx,%rax,8)
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$1, %r12d
	movl	$1, %r14d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L33:
	movl	-60(%rbp), %edx
	movl	%eax, %ecx
	movq	%rbx, %r8
	movq	%r15, %rdi
	call	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode@PLT
	movq	%rax, %rcx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L27:
	cmpl	$65535, %eax
	jg	.L29
	cmpl	$56320, %eax
	movq	(%rdi), %r8
	movl	$320, %edx
	movl	$0, %edi
	cmovl	%edx, %edi
	movl	%eax, %edx
	andl	$31, %eax
	sarl	$5, %edx
	addl	%edi, %edx
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L272:
	leaq	-60(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L71:
	movslq	368(%r13), %rax
	movq	32(%r13), %rsi
	movabsq	$-4294967296, %rdi
	andq	%rdi, %rcx
	leal	-1(%rax), %edx
	movslq	%edx, %rdx
	movq	%rcx, (%rsi,%rdx,8)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L278:
	leal	1(%rax), %edx
	movq	(%rsi,%rax,8), %rcx
	movl	%edx, 368(%r13)
.L55:
	movq	%rcx, %rax
	sarq	$32, %rax
	movl	%eax, %r8d
	testl	%eax, %eax
	jne	.L68
.L280:
	movslq	368(%r13), %rax
	movq	32(%r13), %rsi
	leal	-1(%rax), %edx
	movslq	%edx, %rdx
	movq	$0, (%rsi,%rdx,8)
.L69:
	movl	24(%r13), %edx
	cmpl	%eax, %edx
	jg	.L278
	cmpl	$39, %edx
	jle	.L56
	leaq	24(%r13), %rdi
	movq	%rbx, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L57
	movl	24(%r13), %edx
.L56:
	movq	0(%r13), %rax
	addl	$1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%edx, 24(%r13)
	movq	%rbx, %rdx
	call	*64(%rax)
	movzbl	%al, %edx
	cmpb	$-65, %al
	jbe	.L265
	cmpl	$192, %edx
	je	.L279
	movq	16(%r13), %rsi
.L66:
	cmpl	$193, %edx
	jne	.L67
	leal	-193(%rax), %ecx
	salq	$32, %rcx
	orq	$83887360, %rcx
.L266:
	movslq	368(%r13), %rax
	movq	32(%r13), %rsi
	leal	1(%rax), %edx
	movl	%edx, 368(%r13)
	movq	%rcx, (%rsi,%rax,8)
	movq	%rcx, %rax
	sarq	$32, %rax
	movl	%eax, %r8d
	testl	%eax, %eax
	je	.L280
.L68:
	cmpl	%eax, -68(%rbp)
	jbe	.L152
	cmpl	$33554432, %eax
	ja	.L71
.L152:
	movl	$1, %r14d
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L279:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	js	.L281
	movq	16(%r13), %rdx
	movq	32(%rdx), %rsi
	movq	(%rsi), %rdi
	movq	16(%rdi), %rcx
	cmpl	$55295, %eax
	jg	.L61
	movl	%eax, %edx
	movq	(%rdi), %rdi
	sarl	$5, %edx
	movslq	%edx, %rdx
.L264:
	movzwl	(%rdi,%rdx,2), %edx
	andl	$31, %eax
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L62:
	movl	(%rcx,%rdx), %eax
	movzbl	%al, %edx
	cmpb	$-65, %al
	ja	.L66
	.p2align 4,,10
	.p2align 3
.L265:
	movabsq	$-281474976710656, %rdi
	movq	%rax, %rcx
	sall	$16, %eax
	salq	$32, %rcx
	andl	$-16777216, %eax
	andq	%rdi, %rcx
	orq	%rax, %rcx
	movl	%edx, %eax
	sall	$8, %eax
	orq	%rax, %rcx
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L281:
	movslq	368(%r13), %rax
	movabsq	$4311744768, %rdi
	leal	1(%rax), %edx
	movl	%edx, 368(%r13)
	movq	32(%r13), %rdx
	movq	%rdi, (%rdx,%rax,8)
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$1, %r8d
	movl	$1, %r14d
.L70:
	cmpl	%r8d, %r12d
	jne	.L282
.L73:
	cmpl	$1, %r12d
	jne	.L38
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L82
	movl	-72(%rbp), %eax
	movl	%eax, %ebx
	sarl	$12, %ebx
	movl	%ebx, -68(%rbp)
	testl	%ebx, %ebx
	jle	.L83
	movq	32(%r15), %r12
	andl	$2048, %eax
	movq	32(%r13), %rcx
	movl	%eax, %edx
	movq	%r12, %r8
	je	.L85
	movb	%r14b, -81(%rbp)
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	movq	%r15, -96(%rbp)
	movq	%r13, -104(%rbp)
.L84:
	movslq	%r9d, %rax
	movslq	4(%r12,%rax,8), %rdx
	leal	1(%r9), %eax
	cltq
	subl	$1, %edx
	cmpl	$33554431, %edx
	jbe	.L283
.L93:
	movslq	4(%r12,%rax,8), %rdx
	movl	%eax, %r14d
	addq	$1, %rax
	subl	$1, %edx
	cmpl	$33554431, %edx
	ja	.L93
.L94:
	movslq	%r10d, %rax
	movl	%r10d, %r13d
	movslq	4(%rcx,%rax,8), %rax
	movl	%eax, %r15d
	leal	-1(%rax), %edx
	leal	1(%r10), %eax
	cltq
	cmpl	$33554431, %edx
	jbe	.L92
.L95:
	movslq	4(%rcx,%rax,8), %rdx
	movl	%eax, %r13d
	addq	$1, %rax
	movl	%edx, %r15d
	subl	$1, %edx
	cmpl	$33554431, %edx
	ja	.L95
.L92:
	movl	%r13d, %edx
	movl	%r14d, %ebx
.L104:
	leal	-1(%rbx), %eax
	cltq
	cmpl	%ebx, %r9d
	jge	.L284
.L101:
	movzwl	2(%r12,%rax,8), %esi
	movl	%eax, %ebx
	testl	%esi, %esi
	setne	%r8b
	cmpl	%eax, %r9d
	setge	%dil
	subq	$1, %rax
	orb	%dil, %r8b
	je	.L101
	cmpl	%edx, %r10d
	jge	.L285
.L98:
	leal	-1(%rdx), %eax
	cltq
.L102:
	movzwl	2(%rcx,%rax,8), %edi
	movl	%eax, %edx
	testl	%edi, %edi
	setne	%r11b
	cmpl	%eax, %r10d
	setge	%r8b
	subq	$1, %rax
	orb	%r8b, %r11b
	je	.L102
.L99:
	cmpl	%edi, %esi
	jne	.L286
	testl	%esi, %esi
	jne	.L104
.L97:
	cmpl	$1, %r15d
	je	.L252
	leal	1(%r14), %r9d
	leal	1(%r13), %r10d
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L67:
	movl	-60(%rbp), %edx
	movl	%eax, %ecx
	movq	%rbx, %r8
	movq	%r13, %rdi
	call	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode@PLT
	movq	%rax, %rcx
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L61:
	cmpl	$65535, %eax
	jg	.L63
	cmpl	$56320, %eax
	movq	(%rdi), %r8
	movl	$320, %edx
	movl	$0, %edi
	cmovl	%edx, %edi
	movl	%eax, %edx
	andl	$31, %eax
	sarl	$5, %edx
	addl	%edi, %edx
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L62
.L63:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L62
	cmpl	44(%rdi), %eax
	jl	.L65
	movslq	48(%rdi), %rdx
	salq	$2, %rdx
	jmp	.L62
.L29:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L28
	cmpl	44(%rdi), %eax
	jl	.L31
	movslq	48(%rdi), %rdx
	salq	$2, %rdx
	jmp	.L28
.L9:
	cmpl	$192, %edx
	je	.L287
	movq	16(%r15), %rsi
.L17:
	cmpl	$193, %edx
	jne	.L18
	subl	$193, %ecx
	salq	$32, %rcx
	orq	$83887360, %rcx
	jmp	.L256
.L43:
	cmpl	$192, %edx
	je	.L288
	movq	16(%r13), %rsi
.L51:
	cmpl	$193, %edx
	jne	.L52
	subl	$193, %ecx
	salq	$32, %rcx
	orq	$83887360, %rcx
	jmp	.L263
.L289:
	movslq	368(%r15), %rax
	movabsq	$4311744768, %rdi
	leal	1(%rax), %edx
	movl	%edx, 368(%r15)
	movq	32(%r15), %rdx
	movq	%rdi, (%rdx,%rax,8)
.L8:
	movl	$1, %r12d
	jmp	.L72
.L290:
	movslq	368(%r13), %rax
	movabsq	$4311744768, %rdi
	leal	1(%rax), %edx
	movl	%edx, 368(%r13)
	movq	32(%r13), %rdx
	movq	%rdi, (%rdx,%rax,8)
.L42:
	movl	$1, %r8d
	cmpl	%r8d, %r12d
	je	.L73
.L282:
	movq	-80(%rbp), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L74
	movl	%r12d, %edx
	shrl	$24, %edx
	movzbl	(%rax,%rdx), %edx
	testb	%dl, %dl
	jne	.L157
	cmpl	$1, %r12d
	jbe	.L157
	movq	-80(%rbp), %rbx
	movl	%r12d, %esi
	movl	%r8d, -68(%rbp)
	movq	%rbx, %rdi
	call	_ZNK6icu_6717CollationSettings9reorderExEj@PLT
	movl	-68(%rbp), %r8d
	movl	%eax, %r12d
	movq	32(%rbx), %rax
.L77:
	movl	%r8d, %edx
	shrl	$24, %edx
	movzbl	(%rax,%rdx), %eax
	testb	%al, %al
	jne	.L158
	cmpl	$1, %r8d
	jbe	.L158
	movq	-80(%rbp), %rdi
	movl	%r8d, %esi
	call	_ZNK6icu_6717CollationSettings9reorderExEj@PLT
	movl	%eax, %r8d
.L74:
	cmpl	%r8d, %r12d
.L267:
	jnb	.L80
.L89:
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L52:
	movl	-60(%rbp), %edx
	movq	%rbx, %r8
	movq	%r13, %rdi
	call	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode@PLT
	movq	%rax, %rcx
	jmp	.L40
.L18:
	movl	-60(%rbp), %edx
	movq	%rbx, %r8
	movq	%r15, %rdi
	call	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode@PLT
	movq	%rax, %rcx
	jmp	.L6
.L287:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	js	.L289
	movq	16(%r15), %rdx
	movq	32(%rdx), %rsi
	movq	(%rsi), %rdi
	movq	16(%rdi), %rcx
	cmpl	$55295, %eax
	jg	.L12
	movl	%eax, %edx
	movq	(%rdi), %rdi
	sarl	$5, %edx
	movslq	%edx, %rdx
.L254:
	movzwl	(%rdi,%rdx,2), %edx
	andl	$31, %eax
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L13:
	movl	(%rcx,%rdx), %ecx
	movzbl	%cl, %edx
	cmpb	$-65, %cl
	ja	.L17
	movabsq	$-281474976710656, %rax
	movq	%rcx, %rsi
	salq	$32, %rsi
	andq	%rax, %rsi
	movl	%ecx, %eax
	movl	%edx, %ecx
	sall	$16, %eax
	sall	$8, %ecx
	andl	$-16777216, %eax
	orq	%rsi, %rax
	jmp	.L255
.L65:
	movl	%eax, %edx
	movq	(%rdi), %rdi
	movl	%eax, %r8d
	sarl	$11, %edx
	sarl	$5, %r8d
	addl	$2080, %edx
	andl	$63, %r8d
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	addl	%r8d, %edx
	movslq	%edx, %rdx
	jmp	.L264
.L288:
	movl	-60(%rbp), %edi
	testl	%edi, %edi
	js	.L290
	movq	16(%r13), %rax
	movq	32(%rax), %rsi
	movq	(%rsi), %rcx
	movq	16(%rcx), %rdx
	cmpl	$55295, %edi
	jg	.L46
	movl	%edi, %eax
	movq	(%rcx), %rcx
	sarl	$5, %eax
	cltq
.L260:
	movzwl	(%rcx,%rax,2), %ecx
.L261:
	movl	%edi, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
.L47:
	movl	(%rdx,%rax), %ecx
	movzbl	%cl, %edx
	cmpb	$-65, %cl
	ja	.L51
	movabsq	$-281474976710656, %rax
	movq	%rcx, %rsi
	salq	$32, %rsi
	andq	%rax, %rsi
	movl	%ecx, %eax
	movl	%edx, %ecx
	sall	$16, %eax
	sall	$8, %ecx
	andl	$-16777216, %eax
	orq	%rsi, %rax
	jmp	.L262
.L31:
	movl	%eax, %edx
	movq	(%rdi), %rdi
	movl	%eax, %r8d
	sarl	$11, %edx
	sarl	$5, %r8d
	addl	$2080, %edx
	andl	$63, %r8d
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	addl	%r8d, %edx
	movslq	%edx, %rdx
	jmp	.L257
.L12:
	cmpl	$65535, %eax
	jg	.L14
	cmpl	$56320, %eax
	movq	(%rdi), %r8
	movl	$320, %edx
	movl	$0, %edi
	cmovl	%edx, %edi
	movl	%eax, %edx
	andl	$31, %eax
	sarl	$5, %edx
	addl	%edi, %edx
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L13
.L46:
	cmpl	$65535, %edi
	jg	.L48
	cmpl	$56320, %edi
	movq	(%rcx), %r8
	movl	$320, %eax
	movl	$0, %ecx
	cmovl	%eax, %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%ecx, %eax
	cltq
	movzwl	(%r8,%rax,2), %ecx
	jmp	.L261
.L295:
	cmpl	$2, -68(%rbp)
	je	.L82
	testb	%r14b, %r14b
	jne	.L156
	andl	$192, %esi
	jne	.L156
.L82:
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L291
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L48:
	.cfi_restore_state
	movl	$512, %eax
	cmpl	$1114111, %edi
	ja	.L47
	cmpl	44(%rcx), %edi
	jl	.L50
	movslq	48(%rcx), %rax
	salq	$2, %rax
	jmp	.L47
.L14:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L13
	cmpl	44(%rdi), %eax
	jl	.L16
	movslq	48(%rdi), %rdx
	salq	$2, %rdx
	jmp	.L13
.L269:
	movl	28(%rdx), %eax
	addl	$1, %eax
	movl	%eax, -68(%rbp)
	jmp	.L4
.L299:
	movq	-80(%rbp), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L135
	movl	%r13d, %edx
	shrl	$24, %edx
	movzbl	(%rax,%rdx), %edx
	testb	%dl, %dl
	jne	.L159
	cmpl	$1, %r13d
	jbe	.L159
	movq	-80(%rbp), %rbx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6717CollationSettings9reorderExEj@PLT
	movl	%eax, %r13d
	movq	32(%rbx), %rax
.L138:
	movl	%r12d, %edx
	shrl	$24, %edx
	movzbl	(%rax,%rdx), %eax
	testb	%al, %al
	jne	.L160
	cmpl	$1, %r12d
	jbe	.L160
	movq	-80(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZNK6icu_6717CollationSettings9reorderExEj@PLT
	movl	%eax, %r12d
.L135:
	cmpl	%r12d, %r13d
	jb	.L89
.L80:
	movl	$1, %eax
	jmp	.L1
.L292:
	cmpl	%esi, %edi
	jne	.L267
	cmpl	$256, %edi
	je	.L83
.L86:
	addq	$8, %r8
.L85:
	movl	(%r8), %edi
	shrl	$16, %edi
	je	.L86
	leal	1(%rdx), %eax
	cltq
.L87:
	movzwl	-6(%rcx,%rax,8), %esi
	movl	%eax, %edx
	addq	$1, %rax
	testl	%esi, %esi
	je	.L87
	jmp	.L292
.L252:
	movzbl	-81(%rbp), %r14d
	movq	-96(%rbp), %r15
	movq	-104(%rbp), %r13
.L83:
	testl	$1024, -72(%rbp)
	je	.L115
	movq	32(%r15), %r9
	movq	32(%r13), %rcx
	xorl	%esi, %esi
	xorl	%eax, %eax
.L116:
	movl	-68(%rbp), %edx
	addl	$1, %eax
	testl	%edx, %edx
	jne	.L107
	movslq	%eax, %rdi
.L215:
	movq	-8(%r9,%rdi,8), %rdx
	movl	%edi, %eax
	addq	$1, %rdi
	movq	%rdx, %r8
	movl	%edx, %r10d
	sarq	$32, %r8
	testl	%r8d, %r8d
	je	.L215
	testl	%edx, %edx
	je	.L215
	movl	%edx, %r12d
	leal	1(%rsi), %edx
	andl	$49152, %r12d
	movslq	%edx, %rdx
.L216:
	movq	-8(%rcx,%rdx,8), %rdi
	movl	%edx, %esi
	addq	$1, %rdx
	movq	%rdi, %r11
	sarq	$32, %r11
	testl	%r11d, %r11d
	je	.L216
	testl	%edi, %edi
	je	.L216
	movl	%edi, %edx
	andl	$49152, %edx
.L110:
	cmpl	%edx, %r12d
	jne	.L293
	shrl	$16, %r10d
	cmpl	$256, %r10d
	jne	.L116
.L115:
	cmpl	$1, -68(%rbp)
	jle	.L82
	movl	-72(%rbp), %eax
	movq	32(%r15), %r9
	movl	$65343, %r8d
	movq	32(%r13), %r10
	andl	$1536, %eax
	movq	%r9, %rbx
	cmpl	$512, %eax
	movl	$16191, %eax
	cmovne	%eax, %r8d
	xorl	%esi, %esi
	xorl	%edi, %edi
.L118:
	movq	(%rbx), %rax
	movl	%eax, %ecx
	movl	%eax, %r12d
	orl	%eax, %esi
	andl	%r8d, %ecx
	je	.L119
	leal	1(%rdi), %edx
	movslq	%edx, %rdx
.L120:
	movq	-8(%r10,%rdx,8), %rax
	movl	%edx, %edi
	addq	$1, %rdx
	movl	%eax, %r11d
	orl	%eax, %esi
	andl	%r8d, %eax
	je	.L120
	cmpl	%eax, %ecx
	jne	.L294
	cmpl	$256, %ecx
	je	.L295
.L119:
	addq	$8, %rbx
	jmp	.L118
.L50:
	movl	%edi, %eax
	movq	(%rcx), %rcx
	movl	%edi, %r8d
	sarl	$11, %eax
	sarl	$5, %r8d
	addl	$2080, %eax
	andl	$63, %r8d
	cltq
	movzwl	(%rcx,%rax,2), %eax
	addl	%r8d, %eax
	cltq
	jmp	.L260
.L16:
	movl	%eax, %edx
	movq	(%rdi), %rdi
	movl	%eax, %r8d
	sarl	$11, %edx
	sarl	$5, %r8d
	addl	$2080, %edx
	andl	$63, %r8d
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	addl	%r8d, %edx
	movslq	%edx, %rdx
	jmp	.L254
.L158:
	sall	$24, %eax
	andl	$16777215, %r8d
	orl	%eax, %r8d
	jmp	.L74
.L157:
	movl	%r12d, %r11d
	sall	$24, %edx
	andl	$16777215, %r11d
	orl	%r11d, %edx
	movl	%edx, %r12d
	jmp	.L77
.L285:
	xorl	%edi, %edi
	jmp	.L99
.L284:
	cmpl	%edx, %r10d
	jge	.L97
	xorl	%esi, %esi
	jmp	.L98
.L107:
	movslq	%eax, %rdx
.L111:
	movq	-8(%r9,%rdx,8), %rdi
	movl	%edx, %eax
	addq	$1, %rdx
	movl	%edi, %r10d
	cmpl	$65535, %edi
	jbe	.L111
	movl	%edi, %r12d
	leal	1(%rsi), %edx
	andl	$49152, %r12d
	movslq	%edx, %rdi
.L112:
	movq	-8(%rcx,%rdi,8), %r8
	movl	%edi, %esi
	addq	$1, %rdi
	movl	%r8d, %edx
	cmpl	$65535, %r8d
	jbe	.L112
	andl	$49152, %edx
	jmp	.L110
.L293:
	testl	$256, -72(%rbp)
	jne	.L114
	cmpl	%edx, %r12d
	jb	.L89
	jmp	.L80
.L294:
	movl	-72(%rbp), %edx
	andl	$1792, %edx
	cmpl	$768, %edx
	je	.L296
.L122:
	cmpl	%eax, %ecx
	jb	.L89
	jmp	.L80
.L114:
	cmpl	%edx, %r12d
	jnb	.L89
	jmp	.L80
.L156:
	xorl	%edx, %edx
	jmp	.L127
.L297:
	sarq	$32, %rax
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L129
.L130:
	addq	$8, %r9
.L127:
	movq	(%r9), %rax
	movl	%eax, %r13d
	orl	$-193, %r13d
	cmpw	$256, %ax
	jbe	.L297
.L129:
	addl	$1, %edx
	movslq	%edx, %rcx
.L133:
	movq	-8(%r10,%rcx,8), %rax
	movl	%ecx, %edx
	movzwl	%ax, %r12d
	cmpw	$256, %ax
	jbe	.L298
	orl	$-193, %r12d
.L132:
	cmpl	%r12d, %r13d
	jne	.L299
	cmpl	$1, %r13d
	jne	.L130
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L298:
	sarq	$32, %rax
	addq	$1, %rcx
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L133
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L296:
	cmpl	$256, %ecx
	jbe	.L123
	movl	%ecx, %edx
	addl	$16384, %ecx
	xorb	$-64, %dh
	cmpl	$65536, %r12d
	cmovnb	%edx, %ecx
.L123:
	cmpl	$256, %eax
	jbe	.L122
	movl	%eax, %edx
	addl	$16384, %eax
	xorb	$-64, %dh
	cmpl	$65536, %r11d
	cmovnb	%edx, %eax
	jmp	.L122
.L160:
	sall	$24, %eax
	andl	$16777215, %r12d
	orl	%eax, %r12d
	jmp	.L135
.L159:
	sall	$24, %edx
	andl	$16777215, %r13d
	orl	%edx, %r13d
	jmp	.L138
.L283:
	movl	%r9d, %r14d
	jmp	.L94
.L291:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L286:
	jl	.L89
	jmp	.L80
	.cfi_endproc
.LFE3255:
	.size	_ZN6icu_6716CollationCompare21compareUpToQuaternaryERNS_17CollationIteratorES2_RKNS_17CollationSettingsER10UErrorCode, .-_ZN6icu_6716CollationCompare21compareUpToQuaternaryERNS_17CollationIteratorES2_RKNS_17CollationSettingsER10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
