	.file	"erarules.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678EraRulesD2Ev
	.type	_ZN6icu_678EraRulesD2Ev, @function
_ZN6icu_678EraRulesD2Ev:
.LFB2386:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2386:
	.size	_ZN6icu_678EraRulesD2Ev, .-_ZN6icu_678EraRulesD2Ev
	.globl	_ZN6icu_678EraRulesD1Ev
	.set	_ZN6icu_678EraRulesD1Ev,_ZN6icu_678EraRulesD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678EraRules12getStartDateEiRA3_iR10UErrorCode
	.type	_ZNK6icu_678EraRules12getStartDateEiRA3_iR10UErrorCode, @function
_ZNK6icu_678EraRules12getStartDateEiRA3_iR10UErrorCode:
.LFB2389:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L3
	testl	%esi, %esi
	js	.L5
	cmpl	%esi, 8(%rdi)
	jle	.L5
	movq	(%rdi), %rax
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %eax
	cmpl	$-2147483391, %eax
	je	.L8
	movl	%eax, %ecx
	sarl	$16, %ecx
	movl	%ecx, (%rdx)
	movzbl	%ah, %ecx
	andl	$255, %eax
	movl	%ecx, 4(%rdx)
	movl	%eax, 8(%rdx)
.L3:
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movabsq	$8589934591, %rax
	movl	$1, 8(%rdx)
	movq	%rax, (%rdx)
	ret
	.cfi_endproc
.LFE2389:
	.size	_ZNK6icu_678EraRules12getStartDateEiRA3_iR10UErrorCode, .-_ZNK6icu_678EraRules12getStartDateEiRA3_iR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678EraRules12getStartYearEiR10UErrorCode
	.type	_ZNK6icu_678EraRules12getStartYearEiR10UErrorCode, @function
_ZNK6icu_678EraRules12getStartYearEiR10UErrorCode:
.LFB2390:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	movl	$2147483647, %eax
	testl	%ecx, %ecx
	jg	.L9
	testl	%esi, %esi
	js	.L11
	cmpl	%esi, 8(%rdi)
	jle	.L11
	movq	(%rdi), %rax
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %edx
	movl	%edx, %eax
	sarl	$16, %eax
	cmpl	$-2147483391, %edx
	movl	$-1, %edx
	cmove	%edx, %eax
.L9:
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$1, (%rdx)
	movl	$2147483647, %eax
	ret
	.cfi_endproc
.LFE2390:
	.size	_ZNK6icu_678EraRules12getStartYearEiR10UErrorCode, .-_ZNK6icu_678EraRules12getStartYearEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678EraRules11getEraIndexEiiiR10UErrorCode
	.type	_ZNK6icu_678EraRules11getEraIndexEiiiR10UErrorCode, @function
_ZNK6icu_678EraRules11getEraIndexEiiiR10UErrorCode:
.LFB2391:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L28
	leal	-1(%rdx), %eax
	cmpl	$11, %eax
	ja	.L18
	leal	-1(%rcx), %eax
	cmpl	$30, %eax
	ja	.L18
	movl	%esi, %r10d
	movslq	12(%rdi), %rax
	movl	8(%rdi), %r9d
	sall	$8, %edx
	sall	$16, %r10d
	movq	(%rdi), %rdi
	orl	%r10d, %ecx
	movq	%rax, %r8
	orl	%edx, %ecx
	movl	(%rdi,%rax,4), %eax
	movl	%ecx, %r10d
	cmpl	$-32768, %esi
	jl	.L34
	cmpl	$32767, %esi
	jg	.L22
	cmpl	%ecx, %eax
	movl	$0, %eax
	cmovg	%eax, %r8d
.L22:
	leal	-1(%r9), %ecx
	cmpl	%ecx, %r8d
	jge	.L32
	.p2align 4,,10
	.p2align 3
.L37:
	cmpl	$-32768, %esi
	jl	.L31
	cmpl	$32767, %esi
	jg	.L24
	movl	%r8d, %eax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L36:
	cmpl	%ecx, %eax
	jge	.L35
.L26:
	leal	(%r9,%rax), %edx
	movl	%eax, %r8d
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rdx
	cmpl	%r10d, (%rdi,%rdx,4)
	jle	.L36
.L25:
	movl	%eax, %r9d
	leal	-1(%r9), %ecx
	cmpl	%ecx, %r8d
	jl	.L37
.L32:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	leal	(%r9,%r8), %eax
	movl	%eax, %r8d
	shrl	$31, %r8d
	addl	%eax, %r8d
	sarl	%r8d
	cmpl	%r8d, %ecx
	jg	.L24
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L34:
	cmpl	$-2147483391, %eax
	movl	$0, %eax
	cmovne	%eax, %r8d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L31:
	movl	%r8d, %eax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L27:
	cmpl	%eax, %ecx
	jle	.L38
.L23:
	leal	(%r9,%rax), %edx
	movl	%eax, %r8d
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rdx
	cmpl	$-2147483391, (%rdi,%rdx,4)
	je	.L27
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L38:
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	ret
.L18:
	movl	$1, (%r8)
	movl	$-1, %eax
	ret
.L28:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2391:
	.size	_ZNK6icu_678EraRules11getEraIndexEiiiR10UErrorCode, .-_ZNK6icu_678EraRules11getEraIndexEiiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678EraRules14initCurrentEraEv
	.type	_ZN6icu_678EraRules14initCurrentEraEv, @function
_ZN6icu_678EraRules14initCurrentEraEv:
.LFB2392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	call	ucal_getNow_67@PLT
	movsd	%xmm0, -72(%rbp)
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movsd	-72(%rbp), %xmm0
	testq	%rax, %rax
	je	.L40
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	-52(%rbp), %rcx
	leaq	-56(%rbp), %rdx
	leaq	-60(%rbp), %r8
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*48(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movl	-52(%rbp), %eax
	pxor	%xmm1, %xmm1
	addl	-56(%rbp), %eax
	cvtsi2sdl	%eax, %xmm1
	movsd	-72(%rbp), %xmm0
	addsd	%xmm1, %xmm0
.L40:
	leaq	-36(%rbp), %rcx
	leaq	-40(%rbp), %rdx
	leaq	-44(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	leaq	-28(%rbp), %r9
	leaq	-32(%rbp), %r8
	call	_ZN6icu_675Grego12timeToFieldsEdRiS1_S1_S1_S1_S1_@PLT
	movl	-44(%rbp), %eax
	movl	-48(%rbp), %ecx
	addl	$1, %eax
	sall	$16, %ecx
	orl	-40(%rbp), %ecx
	sall	$8, %eax
	orl	%eax, %ecx
	movl	8(%rbx), %eax
	leal	-1(%rax), %edx
	testl	%edx, %edx
	jle	.L41
	movq	(%rbx), %rsi
	movslq	%edx, %rax
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L49:
	leal	-1(%rax), %edx
	subq	$1, %rax
	testl	%eax, %eax
	jle	.L41
.L42:
	movl	%eax, %edx
	cmpl	%ecx, (%rsi,%rax,4)
	jg	.L49
.L41:
	movl	%edx, 12(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L50:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2392:
	.size	_ZN6icu_678EraRules14initCurrentEraEv, .-_ZN6icu_678EraRules14initCurrentEraEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678EraRulesC2ERNS_11LocalMemoryIiEEi
	.type	_ZN6icu_678EraRulesC2ERNS_11LocalMemoryIiEEi, @function
_ZN6icu_678EraRulesC2ERNS_11LocalMemoryIiEEi:
.LFB2383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	$0, (%rdi)
	movl	%edx, 8(%rdi)
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678EraRules14initCurrentEraEv
	.cfi_endproc
.LFE2383:
	.size	_ZN6icu_678EraRulesC2ERNS_11LocalMemoryIiEEi, .-_ZN6icu_678EraRulesC2ERNS_11LocalMemoryIiEEi
	.globl	_ZN6icu_678EraRulesC1ERNS_11LocalMemoryIiEEi
	.set	_ZN6icu_678EraRulesC1ERNS_11LocalMemoryIiEEi,_ZN6icu_678EraRulesC2ERNS_11LocalMemoryIiEEi
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"supplementalData"
.LC1:
	.string	"calendarData"
.LC2:
	.string	"eras"
.LC3:
	.string	"start"
.LC4:
	.string	"named"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678EraRules14createInstanceEPKcaR10UErrorCode
	.type	_ZN6icu_678EraRules14createInstanceEPKcaR10UErrorCode, @function
_ZN6icu_678EraRules14createInstanceEPKcaR10UErrorCode:
.LFB2388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -84(%rbp)
	movl	(%rdx), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L53
	movq	%rdi, %r12
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	movq	%rdx, %r15
	call	ures_openDirect_67@PLT
	movq	%r15, %rcx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r14
	movq	%rax, %rdx
	movq	%rax, %rdi
	call	ures_getByKey_67@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	ures_getByKey_67@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	ures_getByKey_67@PLT
	movl	(%r15), %r9d
	testl	%r9d, %r9d
	jle	.L119
	testq	%r14, %r14
	je	.L53
.L125:
	movq	%r14, %rdi
	call	ures_close_67@PLT
.L53:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L120
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movq	%r14, %rdi
	call	ures_getSize_67@PLT
	movslq	%eax, %r12
	movl	%eax, -116(%rbp)
	salq	$2, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L78
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	-64(%rbp), %rax
	movl	$2147483647, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rbx, -96(%rbp)
.L75:
	movq	%r14, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L58
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	ures_getNextResource_67@PLT
	movl	(%r15), %r8d
	movq	%rax, %r13
	testl	%r8d, %r8d
	jg	.L109
	movq	%rax, %rdi
	call	ures_getKey_67@PLT
	movq	-112(%rbp), %rsi
	movl	$10, %edx
	movq	%rax, %r12
	movq	%rax, %rdi
	call	strtol@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	strlen@PLT
	movq	%rax, %r8
	movq	-64(%rbp), %rax
	subq	%r12, %rax
	cmpq	%r8, %rax
	jne	.L115
	testl	%ebx, %ebx
	js	.L115
	cmpl	%ebx, -116(%rbp)
	jle	.L115
	movq	-96(%rbp), %rcx
	movslq	%ebx, %rax
	leaq	(%rcx,%rax,4), %rax
	movl	(%rax), %edi
	movq	%rax, -128(%rbp)
	testl	%edi, %edi
	jne	.L112
	leaq	-68(%rbp), %rax
	movb	$1, -85(%rbp)
	movq	%rax, -104(%rbp)
.L63:
	movq	%r13, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L64
.L123:
	xorl	%esi, %esi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	ures_getNextResource_67@PLT
	movl	(%r15), %esi
	movq	%rax, %r12
	testl	%esi, %esi
	jg	.L114
	movq	%rax, %rdi
	call	ures_getKey_67@PLT
	movl	$6, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L121
	movq	%rax, %rsi
	movl	$6, %ecx
	leaq	.LC4(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L122
.L68:
	testq	%r12, %r12
	je	.L63
	movq	%r12, %rdi
	call	ures_close_67@PLT
	movq	%r13, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	jne	.L123
.L64:
	movq	-128(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L71
	testl	%ebx, %ebx
	jne	.L115
	movl	$-2147483391, (%rax)
.L71:
	cmpb	$0, -85(%rbp)
	je	.L72
	cmpl	%ebx, -120(%rbp)
	jle	.L115
.L73:
	testq	%r13, %r13
	je	.L75
	movq	%r13, %rdi
	call	ures_close_67@PLT
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L121:
	movq	-104(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	ures_getIntVector_67@PLT
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L114
	cmpl	$3, -68(%rbp)
	jne	.L67
	movl	(%rax), %edx
	leal	32768(%rdx), %ecx
	cmpl	$65535, %ecx
	ja	.L67
	movl	4(%rax), %ecx
	leal	-1(%rcx), %esi
	cmpl	$11, %esi
	ja	.L67
	movl	8(%rax), %eax
	leal	-1(%rax), %esi
	cmpl	$30, %esi
	jbe	.L124
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$3, (%r15)
	movq	-96(%rbp), %rbx
.L69:
	testq	%r12, %r12
	je	.L61
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L61:
	testq	%r13, %r13
	je	.L118
	movq	%r13, %rdi
	call	ures_close_67@PLT
.L118:
	xorl	%r13d, %r13d
.L57:
	movq	%rbx, %rdi
	call	uprv_free_67@PLT
	testq	%r14, %r14
	jne	.L125
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L122:
	movq	-104(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	ures_getString_67@PLT
	movl	$5, %edx
	leaq	_ZN6icu_67L9VAL_FALSEE(%rip), %rsi
	movq	%rax, %rdi
	call	u_strncmp_67@PLT
	movl	$0, %ecx
	testl	%eax, %eax
	movzbl	-85(%rbp), %eax
	cmove	%ecx, %eax
	movb	%al, -85(%rbp)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L124:
	sall	$16, %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	orl	%eax, %edx
	movq	-128(%rbp), %rax
	movl	%edx, (%rax)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L114:
	movq	-96(%rbp), %rbx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L115:
	movq	-96(%rbp), %rbx
.L62:
	movl	$3, (%r15)
	jmp	.L61
.L72:
	movl	-120(%rbp), %eax
	cmpl	%ebx, %eax
	cmovle	%eax, %ebx
	movl	%ebx, -120(%rbp)
	jmp	.L73
.L58:
	movl	-120(%rbp), %r12d
	movq	-96(%rbp), %rbx
	movl	$16, %edi
	cmpl	$2147483647, %r12d
	je	.L77
	cmpb	$0, -84(%rbp)
	jne	.L77
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L78
	movq	$0, (%rax)
	movl	%r12d, 8(%rax)
.L117:
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	movq	%rbx, 0(%r13)
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	_ZN6icu_678EraRules14initCurrentEraEv
	jmp	.L57
.L109:
	movq	-96(%rbp), %rbx
	jmp	.L61
.L112:
	movq	%rcx, %rbx
	jmp	.L62
.L77:
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L78
	movq	$0, (%rax)
	movl	-116(%rbp), %eax
	movl	%eax, 8(%r13)
	jmp	.L117
.L78:
	movl	$7, (%r15)
	jmp	.L118
.L120:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2388:
	.size	_ZN6icu_678EraRules14createInstanceEPKcaR10UErrorCode, .-_ZN6icu_678EraRules14createInstanceEPKcaR10UErrorCode
	.section	.rodata
	.align 8
	.type	_ZN6icu_67L9VAL_FALSEE, @object
	.size	_ZN6icu_67L9VAL_FALSEE, 10
_ZN6icu_67L9VAL_FALSEE:
	.value	102
	.value	97
	.value	108
	.value	115
	.value	101
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
