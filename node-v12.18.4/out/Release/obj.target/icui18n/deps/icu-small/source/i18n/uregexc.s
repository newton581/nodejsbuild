	.file	"uregexc.cpp"
	.text
	.p2align 4
	.globl	uregex_openC_67
	.type	uregex_openC_67, @function
uregex_openC_67:
.LFB2244:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1
	movq	%rcx, %rbx
	testq	%rdi, %rdi
	je	.L13
	leaq	-128(%rbp), %r15
	movl	%esi, %r13d
	movq	%rdi, %rsi
	movq	%rdx, %r14
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKc@PLT
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L4
	movswl	%ax, %esi
	sarl	$5, %esi
	testb	$17, %al
	jne	.L9
.L15:
	leaq	-118(%rbp), %rdi
	testb	$2, %al
	cmove	-104(%rbp), %rdi
.L6:
	movq	%rbx, %r8
	movq	%r14, %rcx
	movl	%r13d, %edx
	call	uregex_open_67@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L14
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movl	-116(%rbp), %esi
	testb	$17, %al
	je	.L15
.L9:
	xorl	%edi, %edi
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$1, (%rcx)
	jmp	.L1
.L14:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2244:
	.size	uregex_openC_67, .-uregex_openC_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
