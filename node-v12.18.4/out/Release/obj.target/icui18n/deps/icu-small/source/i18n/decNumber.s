	.file	"decNumber.cpp"
	.text
	.p2align 4
	.type	_ZL13decUnitAddSubPKhiS0_iiPhi, @function
_ZL13decUnitAddSubPKhiS0_iiPhi:
.LFB2126:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rax
	movslq	%ecx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	(%r9,%rax), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	(%r9,%rcx), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%rax, -56(%rbp)
	movl	16(%rbp), %r10d
	testl	%r8d, %r8d
	je	.L37
	movslq	%r8d, %rcx
	addq	%rcx, %r12
	leaq	(%r9,%rcx), %r9
	cmpq	%r15, %rdi
	jne	.L3
	cmpl	%r8d, %esi
	jl	.L3
.L10:
	leaq	(%r14,%rcx), %rsi
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%r15, %r9
	movq	%r14, %rsi
.L2:
	cmpq	%r13, %r12
	jbe	.L11
	movq	%r12, %rax
	movq	%r13, %r12
	movq	%rax, %r13
.L11:
	cmpq	%r12, %r9
	jnb	.L12
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$-10, %r8d
	subq	%r9, %rdi
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L13:
	testl	%r11d, %r11d
	jns	.L66
	addl	$100, %r11d
	movl	%r11d, %edx
	shrl	%edx
	imull	$26215, %edx, %edx
	shrl	$17, %edx
	movl	%edx, %eax
	subl	$10, %edx
	imull	%r8d, %eax
	addl	%eax, %r11d
	movb	%r11b, (%r9,%rcx)
.L14:
	addq	$1, %rcx
	cmpq	%rdi, %rcx
	je	.L67
.L17:
	movzbl	(%rsi,%rcx), %r11d
	addl	%r11d, %edx
	movzbl	(%rbx,%rcx), %r11d
	imull	%r10d, %r11d
	addl	%edx, %r11d
	cmpl	$9, %r11d
	ja	.L13
	movb	%r11b, (%r9,%rcx)
	addq	$1, %rcx
	xorl	%edx, %edx
	cmpq	%rdi, %rcx
	jne	.L17
.L67:
	addq	%rcx, %rsi
	addq	%rcx, %rbx
	cmpq	%r13, %r12
	jnb	.L20
.L18:
	addq	-56(%rbp), %r14
	movq	%r12, %r8
	movl	$-10, %edi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L71:
	movzbl	(%rsi), %ecx
	addq	$1, %rsi
	addl	%edx, %ecx
	cmpl	$9, %ecx
	jbe	.L68
.L23:
	testl	%ecx, %ecx
	jns	.L69
	addl	$100, %ecx
	movl	%ecx, %edx
	shrl	%edx
	imull	$26215, %edx, %edx
	shrl	$17, %edx
	movl	%edx, %eax
	subl	$10, %edx
	imull	%edi, %eax
	addl	%eax, %ecx
	movb	%cl, (%r8)
.L24:
	addq	$1, %r8
	cmpq	%r13, %r8
	jnb	.L70
.L26:
	cmpq	%r14, %rsi
	jb	.L71
	movzbl	(%rbx), %ecx
	addq	$1, %rbx
	imull	%r10d, %ecx
	addl	%edx, %ecx
	cmpl	$9, %ecx
	ja	.L23
.L68:
	movb	%cl, (%r8)
	addq	$1, %r8
	xorl	%edx, %edx
	cmpq	%r13, %r8
	jb	.L26
.L70:
	leaq	1(%r12), %rcx
	movq	%r13, %rax
	subq	%r12, %rax
	cmpq	%rcx, %r13
	movl	$1, %ecx
	cmovb	%rcx, %rax
	addq	%rax, %r12
.L20:
	testl	%edx, %edx
	je	.L64
	jg	.L72
	cmpq	%r13, %r15
	jnb	.L41
	movq	%r15, %rcx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L31:
	movzbl	(%rcx), %esi
	addl	$9, %eax
	subl	%esi, %eax
	cmpl	$10, %eax
	je	.L32
	movb	%al, (%rcx)
	addq	$1, %rcx
	xorl	%eax, %eax
	cmpq	%rcx, %r13
	jne	.L31
.L30:
	movl	%eax, %ecx
	subl	%edx, %ecx
	cmpl	$1, %ecx
	je	.L35
	notl	%edx
	addq	$1, %r13
	addl	%eax, %edx
	movb	%dl, -1(%r13)
.L35:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	subl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	%r11d, %edx
	movl	%r8d, %eax
	shrl	%edx
	imull	$26215, %edx, %edx
	shrl	$17, %edx
	imull	%edx, %eax
	addl	%eax, %r11d
	movb	%r11b, (%r9,%rcx)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L69:
	movl	%ecx, %edx
	movl	%edi, %eax
	shrl	%edx
	imull	$26215, %edx, %edx
	shrl	$17, %edx
	imull	%edx, %eax
	addl	%eax, %ecx
	movb	%cl, (%r8)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L72:
	movb	%dl, (%r12)
	addq	$1, %r12
.L64:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	subl	%r15d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	cmpq	%r9, %r15
	jnb	.L37
	movq	-56(%rbp), %rax
	leaq	(%r14,%rax), %rdx
	cmpq	%rdx, %r14
	jnb	.L38
	movq	%r9, %rax
	leaq	1(%r14), %rsi
	subq	%r15, %rax
	addq	%r14, %rax
	cmpq	%rax, %rdx
	cmovb	%rdx, %rax
	movq	%r14, %rdx
	notq	%rdx
	addq	%rax, %rdx
	cmpq	$14, %rdx
	seta	%dil
	cmpq	%rax, %rsi
	setbe	%dl
	testb	%dl, %dil
	je	.L39
	leaq	15(%r14), %rdx
	subq	%r15, %rdx
	cmpq	$30, %rdx
	jbe	.L39
	movq	%rax, %rdx
	movl	$1, %r11d
	movq	%r15, %xmm1
	movdqa	.LC0(%rip), %xmm3
	subq	%r14, %rdx
	cmpq	%rax, %rsi
	movdqa	.LC1(%rip), %xmm2
	cmovbe	%rdx, %r11
	leaq	1(%r15), %rdx
	movq	%rdx, %xmm5
	xorl	%edx, %edx
	movq	%r11, %rsi
	punpcklqdq	%xmm5, %xmm1
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L6:
	movdqu	(%r14,%rdx), %xmm4
	movdqa	%xmm1, %xmm0
	paddq	%xmm3, %xmm1
	paddq	%xmm2, %xmm0
	movups	%xmm4, (%r15,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L6
	movq	%r11, %r8
	movhlps	%xmm0, %xmm6
	andq	$-16, %r8
	movq	%xmm6, %rdi
	leaq	(%r14,%r8), %rsi
	leaq	(%r15,%r8), %rdx
	cmpq	%r11, %r8
	je	.L9
	movzbl	(%rsi), %edi
	leaq	1(%rsi), %r8
	movb	%dil, (%rdx)
	leaq	1(%rdx), %rdi
	cmpq	%r8, %rax
	jbe	.L9
	movzbl	1(%rsi), %edi
	leaq	2(%rsi), %r8
	movb	%dil, 1(%rdx)
	leaq	2(%rdx), %rdi
	cmpq	%r8, %rax
	jbe	.L9
	movzbl	2(%rsi), %edi
	leaq	3(%rsi), %r8
	movb	%dil, 2(%rdx)
	leaq	3(%rdx), %rdi
	cmpq	%r8, %rax
	jbe	.L9
	movzbl	3(%rsi), %edi
	leaq	4(%rsi), %r8
	movb	%dil, 3(%rdx)
	leaq	4(%rdx), %rdi
	cmpq	%r8, %rax
	jbe	.L9
	movzbl	4(%rsi), %edi
	leaq	5(%rsi), %r8
	movb	%dil, 4(%rdx)
	leaq	5(%rdx), %rdi
	cmpq	%r8, %rax
	jbe	.L9
	movzbl	5(%rsi), %edi
	leaq	6(%rsi), %r8
	movb	%dil, 5(%rdx)
	leaq	6(%rdx), %rdi
	cmpq	%r8, %rax
	jbe	.L9
	movzbl	6(%rsi), %edi
	leaq	7(%rsi), %r8
	movb	%dil, 6(%rdx)
	leaq	7(%rdx), %rdi
	cmpq	%r8, %rax
	jbe	.L9
	movzbl	7(%rsi), %edi
	leaq	8(%rsi), %r8
	movb	%dil, 7(%rdx)
	leaq	8(%rdx), %rdi
	cmpq	%r8, %rax
	jbe	.L9
	movzbl	8(%rsi), %edi
	leaq	9(%rsi), %r8
	movb	%dil, 8(%rdx)
	leaq	9(%rdx), %rdi
	cmpq	%r8, %rax
	jbe	.L9
	movzbl	9(%rsi), %edi
	leaq	10(%rsi), %r8
	movb	%dil, 9(%rdx)
	leaq	10(%rdx), %rdi
	cmpq	%r8, %rax
	jbe	.L9
	movzbl	10(%rsi), %edi
	leaq	11(%rsi), %r8
	movb	%dil, 10(%rdx)
	leaq	11(%rdx), %rdi
	cmpq	%r8, %rax
	jbe	.L9
	movzbl	11(%rsi), %edi
	leaq	12(%rsi), %r8
	movb	%dil, 11(%rdx)
	leaq	12(%rdx), %rdi
	cmpq	%r8, %rax
	jbe	.L9
	movzbl	12(%rsi), %edi
	leaq	13(%rsi), %r8
	movb	%dil, 12(%rdx)
	leaq	13(%rdx), %rdi
	cmpq	%r8, %rax
	jbe	.L9
	movzbl	13(%rsi), %edi
	leaq	14(%rsi), %r8
	movb	%dil, 13(%rdx)
	leaq	14(%rdx), %rdi
	cmpq	%r8, %rax
	jbe	.L9
	movzbl	14(%rsi), %eax
	leaq	15(%rdx), %rdi
	movb	%al, 14(%rdx)
	.p2align 4,,10
	.p2align 3
.L9:
	cmpq	%r9, %rdi
	jb	.L4
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L32:
	movb	$0, (%rcx)
	addq	$1, %rcx
	movl	$1, %eax
	cmpq	%rcx, %r13
	jne	.L31
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%r15, %rdi
.L4:
	movq	%r9, %rdx
	leaq	1(%rdi), %rax
	movl	%r10d, 16(%rbp)
	subq	%rdi, %rdx
	cmpq	%rax, %r9
	movl	$1, %eax
	movq	%rcx, -72(%rbp)
	cmovb	%rax, %rdx
	xorl	%esi, %esi
	movq	%r9, -64(%rbp)
	call	memset@PLT
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rcx
	movl	16(%rbp), %r10d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%r9, %r12
	cmpq	%r13, %r9
	jnb	.L64
	xorl	%edx, %edx
	jmp	.L18
.L39:
	movq	%r15, %rdi
	movq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L5:
	movzbl	(%rdx), %esi
	addq	$1, %rdx
	addq	$1, %rdi
	movb	%sil, -1(%rdi)
	cmpq	%rax, %rdx
	jb	.L5
	jmp	.L9
.L41:
	movq	%r15, %r13
	movl	$1, %eax
	jmp	.L30
	.cfi_endproc
.LFE2126:
	.size	_ZL13decUnitAddSubPKhiS0_iiPhi, .-_ZL13decUnitAddSubPKhiS0_iiPhi
	.p2align 4
	.type	_ZL14decSetOverflowP9decNumberP10decContextPj, @function
_ZL14decSetOverflowP9decNumberP10decContextPj:
.LFB2135:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	8(%rdi), %edx
	movl	%edx, %r14d
	andl	$-128, %r14d
	cmpb	$0, 9(%rdi)
	jne	.L74
	cmpl	$1, (%rdi)
	je	.L91
.L74:
	xorl	%eax, %eax
	movq	$1, (%rbx)
	movw	%ax, 8(%rbx)
	movl	12(%r12), %eax
	cmpl	$6, %eax
	je	.L77
	ja	.L78
	testl	%eax, %eax
	je	.L79
	cmpl	$5, %eax
	jne	.L81
.L80:
	movslq	(%r12), %rax
	leaq	9(%rbx), %rcx
	movl	%eax, (%rbx)
	cmpl	$1, %eax
	jle	.L82
	leal	-2(%rax), %r15d
	movq	%rcx, %rdi
	movl	$9, %esi
	addq	$1, %r15
	movq	%r15, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	movl	$1, %eax
	addq	%r15, %rcx
.L82:
	leaq	_ZL9DECPOWERS(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	subl	$1, %eax
	movb	%al, (%rcx)
	movb	$0, 8(%rbx)
	movl	4(%r12), %eax
	subl	(%r12), %eax
	movb	%r14b, 8(%rbx)
	addl	$1, %eax
	movl	%eax, 4(%rbx)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L78:
	cmpl	$7, %eax
	je	.L80
.L81:
	orl	$64, %r14d
	movb	%r14b, 8(%rbx)
.L83:
	orl	$2592, 0(%r13)
.L73:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	testb	%dl, %dl
	jns	.L81
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L77:
	testb	%dl, %dl
	jns	.L80
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L91:
	testb	$112, %dl
	jne	.L74
	cmpb	$0, 24(%rsi)
	movl	4(%rsi), %eax
	je	.L75
	subl	(%rsi), %eax
	addl	$1, %eax
.L75:
	cmpl	%eax, 4(%rbx)
	jle	.L73
	movl	%eax, 4(%rbx)
	orl	$1024, 0(%r13)
	jmp	.L73
	.cfi_endproc
.LFE2135:
	.size	_ZL14decSetOverflowP9decNumberP10decContextPj, .-_ZL14decSetOverflowP9decNumberP10decContextPj
	.p2align 4
	.type	_ZL9decGetIntPK9decNumber, @function
_ZL9decGetIntPK9decNumber:
.LFB2139:
	.cfi_startproc
	movl	(%rdi), %edx
	movl	4(%rdi), %eax
	movzbl	8(%rdi), %r9d
	movzbl	9(%rdi), %ecx
	leal	(%rdx,%rax), %esi
	cmpl	$1, %edx
	je	.L126
.L93:
	leaq	9(%rdi), %rdx
	testl	%eax, %eax
	js	.L127
	je	.L96
	movzbl	%r9b, %ecx
	xorl	%edi, %edi
	cmpl	$10, %esi
	jle	.L128
.L123:
	andl	$1, %edi
	leal	-2147483646(%rdi), %r8d
.L122:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	notl	%eax
	leaq	10(%rdi,%rax), %rax
	.p2align 4,,10
	.p2align 3
.L97:
	cmpb	$0, (%rdx)
	jne	.L105
	addq	$1, %rdx
	movzbl	(%rdx), %ecx
	cmpq	%rax, %rdx
	jne	.L97
.L96:
	movzbl	%cl, %edi
	addq	$1, %rdx
	movl	$1, %eax
	cmpl	$10, %esi
	jg	.L123
.L128:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	cmpl	%esi, %eax
	jge	.L108
	movzbl	(%rdx), %r8d
	movslq	%eax, %r11
	leaq	_ZL9DECPOWERS(%rip), %r10
	movl	%r8d, %ecx
	imull	(%r10,%r11,4), %r8d
	leal	1(%rax), %r11d
	addl	%edi, %r8d
	cmpl	%r11d, %esi
	jle	.L101
	movzbl	1(%rdx), %ebx
	movslq	%r11d, %r11
	movl	%ebx, %ecx
	imull	(%r10,%r11,4), %ebx
	leal	2(%rax), %r11d
	addl	%ebx, %r8d
	cmpl	%esi, %r11d
	jge	.L101
	movzbl	2(%rdx), %ebx
	movslq	%r11d, %r11
	movl	%ebx, %ecx
	imull	(%r10,%r11,4), %ebx
	leal	3(%rax), %r11d
	addl	%ebx, %r8d
	cmpl	%esi, %r11d
	jge	.L101
	movzbl	3(%rdx), %ebx
	movslq	%r11d, %r11
	movl	%ebx, %ecx
	imull	(%r10,%r11,4), %ebx
	leal	4(%rax), %r11d
	addl	%ebx, %r8d
	cmpl	%r11d, %esi
	jle	.L101
	movzbl	4(%rdx), %ebx
	movslq	%r11d, %r11
	movl	%ebx, %ecx
	imull	(%r10,%r11,4), %ebx
	leal	5(%rax), %r11d
	addl	%ebx, %r8d
	cmpl	%r11d, %esi
	jle	.L101
	movzbl	5(%rdx), %ebx
	movslq	%r11d, %r11
	movl	%ebx, %ecx
	imull	(%r10,%r11,4), %ebx
	leal	6(%rax), %r11d
	addl	%ebx, %r8d
	cmpl	%r11d, %esi
	jle	.L101
	movzbl	6(%rdx), %ebx
	movslq	%r11d, %r11
	movl	%ebx, %ecx
	imull	(%r10,%r11,4), %ebx
	leal	7(%rax), %r11d
	addl	%ebx, %r8d
	cmpl	%r11d, %esi
	jle	.L101
	movzbl	7(%rdx), %ecx
	movslq	%r11d, %rbx
	addl	$8, %eax
	movzbl	%cl, %r11d
	imull	(%r10,%rbx,4), %r11d
	movl	%r11d, %r10d
	addl	%r8d, %r10d
	movl	%r10d, %r8d
	cmpl	%eax, %esi
	jle	.L101
	movzbl	8(%rdx), %r8d
	movl	%r8d, %ecx
	imull	$1000000000, %r8d, %r8d
	addl	%r10d, %r8d
.L101:
	movl	%esi, %eax
.L100:
	cmpl	$10, %esi
	je	.L129
.L102:
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	negl	%eax
	testb	%r9b, %r9b
	cmovs	%eax, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore 3
	.cfi_restore 6
	movl	$-2147483648, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	movl	%r9d, %edx
	xorl	%r8d, %r8d
	andl	$112, %edx
	orb	%cl, %dl
	jne	.L93
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	subl	$1, %eax
	leaq	_ZL9DECPOWERS(%rip), %rsi
	movslq	%eax, %rbx
	movl	%r8d, %eax
	cltd
	idivl	(%rsi,%rbx,4)
	cmpl	%ecx, %eax
	je	.L130
.L99:
	andl	$1, %edi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leal	-2147483646(%rdi), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	testb	%r9b, %r9b
	jns	.L120
	cmpl	$1999999997, %r8d
	jg	.L99
	testb	%r9b, %r9b
	js	.L102
.L120:
	cmpl	$999999999, %r8d
	jg	.L99
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L108:
	movl	%edi, %r8d
	jmp	.L100
	.cfi_endproc
.LFE2139:
	.size	_ZL9decGetIntPK9decNumber, .-_ZL9decGetIntPK9decNumber
	.p2align 4
	.type	_ZL8decDecapP9decNumberi, @function
_ZL8decDecapP9decNumberi:
.LFB2140:
	.cfi_startproc
	movl	(%rdi), %eax
	movq	%rdi, %r8
	cmpl	%esi, %eax
	jle	.L143
	subl	%esi, %eax
	leaq	9(%rdi), %rdi
	cmpl	$49, %eax
	jle	.L144
	cltq
	leaq	-1(%rax), %rsi
.L136:
	leal	1(%rsi), %edx
	movq	%rdx, %rax
	leaq	-1(%rdi,%rdx), %rdx
	cmpq	%rdx, %rdi
	ja	.L137
	leaq	8(%r8), %rcx
.L138:
	cmpb	$0, (%rdx)
	jne	.L137
	cmpl	$1, %eax
	je	.L137
	subq	$1, %rdx
	subl	$1, %eax
	cmpq	%rcx, %rdx
	jne	.L138
.L137:
	movl	%eax, (%r8)
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	movslq	%eax, %rdx
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rdx), %esi
	movq	%rsi, %rdx
	subq	$1, %rsi
	subl	$1, %edx
	subl	%edx, %eax
	cmpl	$1, %eax
	je	.L136
	leaq	(%rdi,%rsi), %r9
	movslq	%eax, %rcx
	leaq	_ZL9DECPOWERS(%rip), %r10
	movzbl	(%r9), %edx
	movl	%edx, %eax
	xorl	%edx, %edx
	divl	(%r10,%rcx,4)
	movb	%dl, (%r9)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L143:
	movb	$0, 9(%rdi)
	movq	%r8, %rax
	movl	$1, (%rdi)
	ret
	.cfi_endproc
.LFE2140:
	.size	_ZL8decDecapP9decNumberi, .-_ZL8decDecapP9decNumberi
	.p2align 4
	.type	_ZL11decToStringPK9decNumberPch, @function
_ZL11decToStringPK9decNumberPch:
.LFB2116:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsi, %rcx
	leaq	9(%r11), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movslq	(%r11), %rax
	movslq	4(%rdi), %rdi
	cmpl	$49, %eax
	jg	.L146
	leaq	_ZL8d2utable(%rip), %rsi
	movzbl	(%rsi,%rax), %r8d
	subq	$1, %r8
.L147:
	movzbl	8(%r11), %r10d
	addq	%r9, %r8
	testb	%r10b, %r10b
	jns	.L148
	movb	$45, (%rcx)
	movzbl	8(%r11), %r10d
	addq	$1, %rcx
.L148:
	testb	$112, %r10b
	je	.L149
	testb	$64, %r10b
	jne	.L274
	andl	$16, %r10d
	jne	.L275
.L152:
	movl	$5136718, (%rcx)
	testl	%edi, %edi
	jne	.L145
	cmpb	$0, 9(%r11)
	movl	(%r11), %eax
	jne	.L153
	cmpl	$1, %eax
	je	.L145
.L153:
	addq	$3, %rcx
	cmpl	$49, %eax
	jle	.L226
.L272:
	cmpq	%r8, %r9
	ja	.L212
.L157:
	movzbl	(%r8), %edx
	.p2align 4,,10
	.p2align 3
.L171:
	movabsq	$3472328296227680304, %r11
	leaq	1(%rdi), %rbx
	cmpl	$8, %ebx
	jnb	.L159
	testb	$4, %bl
	jne	.L276
	testl	%ebx, %ebx
	je	.L160
	movb	$48, (%rcx)
	testb	$2, %bl
	jne	.L277
.L160:
	movq	%rcx, %rsi
	leaq	_ZL9DECPOWERS(%rip), %r11
	.p2align 4,,10
	.p2align 3
.L170:
	movl	(%r11,%rdi,4), %r10d
	leal	(%r10,%r10), %eax
	cmpl	%edx, %eax
	jnb	.L165
	leal	0(,%r10,8), %eax
	movl	$52, %r12d
	cmpl	%edx, %eax
	ja	.L166
	movb	$56, (%rsi)
	subl	%eax, %edx
	movl	$60, %r12d
.L166:
	movl	%eax, %r10d
	shrl	%r10d
	cmpl	%edx, %r10d
	ja	.L167
	movb	%r12b, (%rsi)
	subl	%r10d, %edx
.L167:
	shrl	$2, %eax
.L165:
	cmpl	%edx, %eax
	ja	.L168
	addb	$2, (%rsi)
	subl	%eax, %edx
.L168:
	shrl	%eax
	cmpl	%edx, %eax
	ja	.L169
	addb	$1, (%rsi)
	subl	%eax, %edx
.L169:
	subq	$1, %rdi
	addq	$1, %rsi
	testl	%edi, %edi
	jns	.L170
	addq	%rbx, %rcx
	movq	%r8, %rax
.L158:
	subq	$1, %r8
	cmpq	%r8, %r9
	ja	.L212
	movzbl	-1(%rax), %edx
	xorl	%edi, %edi
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L146:
	movslq	%eax, %r8
	subq	$1, %r8
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L149:
	movl	(%r11), %r13d
	cmpl	$49, %r13d
	jg	.L278
	leaq	_ZL8d2utable(%rip), %rsi
	movslq	%r13d, %rax
	movzbl	(%rsi,%rax), %eax
	movl	%r13d, %esi
	subl	%eax, %esi
	testl	%edi, %edi
	je	.L279
	movzbl	(%r8), %eax
	leal	0(%r13,%rdi), %r12d
	testl	%edi, %edi
	jg	.L241
.L281:
	xorl	%ebx, %ebx
	cmpl	$-5, %r12d
	jl	.L241
.L172:
	testl	%r12d, %r12d
	jg	.L174
	movl	$11824, %edx
	addq	$2, %rcx
	movw	%dx, -2(%rcx)
	testl	%r12d, %r12d
	je	.L204
	notl	%r12d
	leaq	1(%r12), %r10
	movl	%r10d, %r11d
	testl	%r10d, %r10d
	je	.L203
	xorl	%edx, %edx
.L202:
	movl	%edx, %edi
	addl	$1, %edx
	movb	$48, (%rcx,%rdi)
	cmpl	%r11d, %edx
	jb	.L202
.L203:
	addq	%r10, %rcx
.L204:
	leaq	_ZL9DECPOWERS(%rip), %r11
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L280:
	movslq	%esi, %rdx
	subl	$1, %esi
	movl	(%r11,%rdx,4), %edi
	leal	(%rdi,%rdi), %edx
.L206:
	movb	$48, (%rcx)
	cmpl	%edx, %eax
	jbe	.L207
	leal	0(,%rdi,8), %edx
	movl	$52, %r10d
	cmpl	%eax, %edx
	ja	.L208
	movb	$56, (%rcx)
	subl	%edx, %eax
	movl	$60, %r10d
.L208:
	movl	%edx, %edi
	shrl	%edi
	cmpl	%eax, %edi
	ja	.L209
	movb	%r10b, (%rcx)
	subl	%edi, %eax
.L209:
	shrl	$2, %edx
.L207:
	cmpl	%eax, %edx
	ja	.L210
	addb	$2, (%rcx)
	subl	%edx, %eax
.L210:
	shrl	%edx
	cmpl	%eax, %edx
	ja	.L211
	addb	$1, (%rcx)
	subl	%edx, %eax
.L211:
	addq	$1, %rcx
.L201:
	testl	%esi, %esi
	jns	.L280
	cmpq	%r8, %r9
	je	.L193
	movzbl	-1(%r8), %eax
	movl	$-1, %esi
	subq	$1, %r8
	movl	$2, %edx
	movl	$1, %edi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L278:
	xorl	%esi, %esi
	testl	%edi, %edi
	je	.L272
	movzbl	(%r8), %eax
	leal	0(%r13,%rdi), %r12d
	testl	%edi, %edi
	jle	.L281
.L241:
	leal	-1(%r12), %ebx
	cmpl	$1, %r12d
	je	.L233
	andl	$1, %edx
	je	.L233
	testl	%ebx, %ebx
	js	.L282
	movslq	%ebx, %r12
	movl	%ebx, %edx
	imulq	$1431655766, %r12, %r12
	sarl	$31, %edx
	shrq	$32, %r12
	subl	%edx, %r12d
	leal	(%r12,%r12,2), %edx
	movl	%ebx, %r12d
	subl	%edx, %r12d
	subl	%r12d, %ebx
.L176:
	cmpb	$0, 9(%r11)
	jne	.L177
	cmpl	$1, %r13d
	je	.L283
.L177:
	addl	$1, %r12d
.L174:
	movl	%r12d, %r10d
	leaq	_ZL9DECPOWERS(%rip), %r13
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L285:
	movslq	%esi, %rdx
	subl	$1, %esi
	movl	0(%r13,%rdx,4), %edi
	leal	(%rdi,%rdi), %edx
.L181:
	movb	$48, (%rcx)
	cmpl	%edx, %eax
	jbe	.L183
	leal	0(,%rdi,8), %edx
	movl	$52, %r14d
	cmpl	%eax, %edx
	ja	.L184
	movb	$56, (%rcx)
	subl	%edx, %eax
	movl	$60, %r14d
.L184:
	movl	%edx, %edi
	shrl	%edi
	cmpl	%eax, %edi
	ja	.L185
	movb	%r14b, (%rcx)
	subl	%edi, %eax
.L185:
	shrl	$2, %edx
.L183:
	cmpl	%eax, %edx
	ja	.L186
	addb	$2, (%rcx)
	subl	%edx, %eax
.L186:
	shrl	%edx
	cmpl	%eax, %edx
	ja	.L187
	addb	$1, (%rcx)
	subl	%edx, %eax
.L187:
	addq	$1, %rcx
	subl	$1, %r10d
	je	.L284
.L189:
	testl	%esi, %esi
	jns	.L285
	cmpq	%r8, %r9
	je	.L182
	movzbl	-1(%r8), %eax
	movl	$-1, %esi
	subq	$1, %r8
	movl	$2, %edx
	movl	$1, %edi
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L212:
	movb	$0, (%rcx)
.L145:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	cmpl	(%r11), %r12d
	jge	.L193
.L225:
	movb	$46, (%rcx)
	leaq	_ZL9DECPOWERS(%rip), %r11
	addq	$1, %rcx
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L286:
	movslq	%esi, %rdx
	subl	$1, %esi
	movl	(%r11,%rdx,4), %edi
	leal	(%rdi,%rdi), %edx
.L192:
	movb	$48, (%rcx)
	cmpl	%edx, %eax
	jbe	.L194
	leal	0(,%rdi,8), %edx
	movl	$52, %r10d
	cmpl	%eax, %edx
	ja	.L195
	movb	$56, (%rcx)
	subl	%edx, %eax
	movl	$60, %r10d
.L195:
	movl	%edx, %edi
	shrl	%edi
	cmpl	%eax, %edi
	ja	.L196
	movb	%r10b, (%rcx)
	subl	%edi, %eax
.L196:
	shrl	$2, %edx
.L194:
	cmpl	%eax, %edx
	ja	.L197
	addb	$2, (%rcx)
	subl	%edx, %eax
.L197:
	shrl	%edx
	cmpl	%eax, %edx
	ja	.L198
	addb	$1, (%rcx)
	subl	%edx, %eax
.L198:
	addq	$1, %rcx
.L199:
	testl	%esi, %esi
	jns	.L286
	cmpq	%r8, %r9
	je	.L193
	movzbl	-1(%r8), %eax
	movl	$-1, %esi
	subq	$1, %r8
	movl	$2, %edx
	movl	$1, %edi
	jmp	.L192
.L182:
	cmpl	%r12d, (%r11)
	jg	.L225
	leal	-1(%r10), %r12d
	movq	%rcx, %rdi
	movl	$48, %esi
	movslq	%r12d, %r12
	addq	$1, %r12
	movq	%r12, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L193:
	testl	%ebx, %ebx
	je	.L212
	movb	$69, (%rcx)
	leaq	2(%rcx), %rax
	js	.L213
	movb	$43, 1(%rcx)
.L214:
	leaq	36+_ZL9DECPOWERS(%rip), %rsi
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	leaq	-36(%rsi), %r10
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L287:
	subl	%eax, %ebx
	addl	$2, %edx
	shrl	%eax
	movb	%dl, (%rcx)
	cmpl	%ebx, %eax
	ja	.L220
.L219:
	addl	$1, %edx
	subl	%eax, %ebx
	movb	%dl, (%rcx)
.L220:
	addq	$1, %rcx
	movl	$1, %r8d
.L222:
	leaq	-4(%rsi), %rax
	cmpq	%rsi, %r10
	je	.L212
	movq	%rax, %rsi
.L224:
	movb	$48, (%rcx)
	movl	(%rsi), %edi
	movl	$48, %edx
	leal	(%rdi,%rdi), %eax
	cmpl	%eax, %ebx
	jbe	.L215
	leal	0(,%rdi,8), %eax
	movl	$52, %edi
	cmpl	%eax, %ebx
	jb	.L216
	movb	$56, (%rcx)
	subl	%eax, %ebx
	movl	$60, %edi
	movl	$56, %edx
.L216:
	movl	%eax, %r9d
	shrl	%r9d
	cmpl	%ebx, %r9d
	ja	.L217
	movb	%dil, (%rcx)
	subl	%r9d, %ebx
	movl	%edi, %edx
.L217:
	shrl	$2, %eax
.L215:
	cmpl	%eax, %ebx
	jnb	.L287
	shrl	%eax
	cmpl	%ebx, %eax
	jbe	.L219
	cmpb	$48, %dl
	jne	.L220
	movl	%r8d, %eax
	xorl	%r8d, %r8d
	xorl	$1, %eax
	testb	%al, %al
	jne	.L222
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L275:
	movb	$115, (%rcx)
	addq	$1, %rcx
	jmp	.L152
.L226:
	movslq	%eax, %rdx
	leaq	_ZL8d2utable(%rip), %rsi
	movzbl	(%rsi,%rdx), %edx
	subl	%edx, %eax
	movslq	%eax, %rdi
.L227:
	cmpq	%r8, %r9
	ja	.L212
	movq	%r8, %rax
	testl	%edi, %edi
	js	.L158
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L274:
	movl	$121, %edi
	movl	$6712905, (%rcx)
	movw	%di, 7(%rcx)
	movl	$1953066601, 3(%rcx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L213:
	.cfi_restore_state
	movb	$45, 1(%rcx)
	negl	%ebx
	jmp	.L214
.L233:
	movl	$1, %r12d
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L159:
	movl	%ebx, %eax
	leaq	8(%rcx), %r10
	movq	%r11, (%rcx)
	andq	$-8, %r10
	movq	%r11, -8(%rcx,%rax)
	movq	%rcx, %rax
	subq	%r10, %rax
	addl	%ebx, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L160
	andl	$-8, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
.L163:
	movl	%eax, %r12d
	addl	$8, %eax
	movq	%r11, (%r10,%r12)
	cmpl	%esi, %eax
	jb	.L163
	jmp	.L160
.L276:
	movl	%ebx, %eax
	movl	$808464432, (%rcx)
	movl	$808464432, -4(%rcx,%rax)
	jmp	.L160
.L282:
	movl	$1, %edx
	subl	%r12d, %edx
	movslq	%edx, %r12
	movl	%edx, %edi
	imulq	$1431655766, %r12, %r12
	sarl	$31, %edi
	shrq	$32, %r12
	subl	%edi, %r12d
	leal	(%r12,%r12,2), %edi
	subl	%edi, %edx
	movl	%edx, %r12d
	je	.L176
	movl	$3, %edx
	subl	%r12d, %edx
	movl	%edx, %r12d
	subl	%edx, %ebx
	jmp	.L176
.L277:
	movl	%ebx, %eax
	movl	$12336, %esi
	movw	%si, -2(%rcx,%rax)
	jmp	.L160
.L283:
	andl	$112, %r10d
	jne	.L177
	testl	%r12d, %r12d
	je	.L233
	addl	$3, %ebx
	subl	$2, %r12d
	jmp	.L172
.L279:
	movslq	%esi, %rdi
	jmp	.L227
	.cfi_endproc
.LFE2116:
	.size	_ZL11decToStringPK9decNumberPch, .-_ZL11decToStringPK9decNumberPch
	.p2align 4
	.type	_ZL14decShiftToMostPhii.part.0, @function
_ZL14decShiftToMostPhii.part.0:
.LFB2605:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leal	(%rdx,%rsi), %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpl	$1, %r12d
	jle	.L319
	cmpl	$49, %esi
	movslq	%esi, %rsi
	jle	.L320
	subq	$1, %rsi
	addq	%rdi, %rsi
	cmpl	$49, %edx
	jg	.L293
.L321:
	movslq	%edx, %rax
	leaq	_ZL8d2utable(%rip), %r8
	movzbl	(%r8,%rax), %eax
	movq	%rax, %rcx
	addq	%rsi, %rax
	subl	$1, %ecx
	subl	%ecx, %edx
	movl	$1, %ecx
	subl	%edx, %ecx
	je	.L294
	cmpl	$49, %r12d
	jg	.L298
	movslq	%r12d, %r9
	movzbl	(%r8,%r9), %r11d
	subq	$1, %r11
.L299:
	addq	%rdi, %r11
	cmpq	%rsi, %rdi
	ja	.L308
	movslq	%ecx, %r8
	leaq	_ZL7multies(%rip), %r9
	movslq	%edx, %rdx
	movl	(%r9,%r8,4), %ebx
	leaq	_ZL9DECPOWERS(%rip), %r9
	movl	(%r9,%r8,4), %r13d
	movl	(%r9,%rdx,4), %r14d
	cmpq	%rax, %r11
	jnb	.L309
	movq	%rsi, %rdx
	movq	%rax, %r10
	movq	%rsi, %r9
	notq	%rdx
	addq	%rdi, %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %r11
	cmovbe	%rdx, %r11
	.p2align 4,,10
	.p2align 3
.L304:
	movzbl	(%r9), %edx
	subq	$1, %r10
	subq	$1, %r9
	movl	%edx, %r8d
	shrl	%cl, %r8d
	imull	%ebx, %r8d
	shrl	$17, %r8d
	imull	%r13d, %r8d
	subl	%r8d, %edx
	imull	%r14d, %edx
	cmpq	%r11, %r10
	ja	.L304
	cmpq	%r9, %rdi
	ja	.L305
	.p2align 4,,10
	.p2align 3
.L306:
	movzbl	(%r9), %r11d
	subq	$1, %r9
	subq	$1, %r10
	movl	%r11d, %r8d
	shrl	%cl, %r8d
	imull	%ebx, %r8d
	shrl	$17, %r8d
	addl	%r8d, %edx
	imull	%r13d, %r8d
	movb	%dl, 1(%r10)
	movl	%r11d, %edx
	subl	%r8d, %edx
	imull	%r14d, %edx
	cmpq	%r9, %rdi
	jbe	.L306
.L305:
	leaq	-1(%rdi), %rcx
	subq	%rsi, %rcx
	addq	%rcx, %rax
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L320:
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rsi), %esi
	subq	$1, %rsi
	addq	%rdi, %rsi
	cmpl	$49, %edx
	jle	.L321
.L293:
	movslq	%edx, %rax
	addq	%rsi, %rax
.L294:
	cmpq	%rsi, %rdi
	ja	.L308
	leaq	-1(%rdi), %r8
	xorl	%edx, %edx
	subq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L297:
	movzbl	(%rsi,%rdx), %ecx
	movb	%cl, (%rax,%rdx)
	subq	$1, %rdx
	cmpq	%rdx, %r8
	jne	.L297
	addq	%r8, %rax
	xorl	%edx, %edx
.L296:
	cmpq	%rax, %rdi
	ja	.L288
	subq	$1, %rdi
	.p2align 4,,10
	.p2align 3
.L307:
	movb	%dl, (%rax)
	subq	$1, %rax
	xorl	%edx, %edx
	cmpq	%rdi, %rax
	jne	.L307
.L288:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	movslq	%edx, %rdx
	leaq	_ZL9DECPOWERS(%rip), %rcx
	movzbl	(%rdi), %eax
	mulb	(%rcx,%rdx,4)
	movb	%al, (%rdi)
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	movq	%rax, %r10
	movq	%rsi, %r9
	xorl	%edx, %edx
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L298:
	movslq	%r12d, %r11
	subq	$1, %r11
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L308:
	xorl	%edx, %edx
	jmp	.L296
	.cfi_endproc
.LFE2605:
	.size	_ZL14decShiftToMostPhii.part.0, .-_ZL14decShiftToMostPhii.part.0
	.p2align 4
	.type	_ZL15decShiftToLeastPhii.part.0, @function
_ZL15decShiftToLeastPhii.part.0:
.LFB2606:
	.cfi_startproc
	cmpl	$49, %edx
	jg	.L323
	movslq	%edx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %r10d
	movl	%edx, %ecx
	movl	%r10d, %r8d
	subl	$1, %r10d
	subl	%r10d, %ecx
	cmpl	$1, %ecx
	je	.L355
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movslq	%r10d, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	cmpl	$49, %r10d
	jg	.L334
	movzbl	(%rax,%r12), %r12d
.L334:
	addq	%rdi, %r12
	movslq	%ecx, %r14
	leaq	_ZL7multies(%rip), %rax
	subl	%edx, %esi
	movzbl	(%r12), %r13d
	movl	(%rax,%r14,4), %ebx
	movl	$1, %r11d
	subl	%ecx, %r11d
	shrl	%cl, %r13d
	subl	%r11d, %esi
	imull	%ebx, %r13d
	movl	%esi, %r8d
	shrl	$17, %r13d
	movb	%r13b, (%rdi)
	testl	%esi, %esi
	jle	.L337
	movzbl	1(%r12), %r15d
	leaq	1(%r12), %rax
	movq	%rax, -56(%rbp)
	leaq	_ZL9DECPOWERS(%rip), %rax
	movl	%r15d, %esi
	movl	(%rax,%r14,4), %r12d
	movslq	%r11d, %r14
	shrl	%cl, %esi
	movl	(%rax,%r14,4), %eax
	imull	%ebx, %esi
	movl	%eax, %r14d
	shrl	$17, %esi
	movl	%esi, %r9d
	imull	%r12d, %r9d
	subl	%r9d, %r15d
	imull	%r15d, %eax
	addl	%eax, %r13d
	movl	%r8d, %eax
	subl	%ecx, %eax
	movb	%r13b, (%rdi)
	testl	%eax, %eax
	jle	.L337
	subl	%edx, %r10d
	movq	-56(%rbp), %r9
	subl	$1, %r8d
	movq	%rdi, %rdx
	subl	%r11d, %r10d
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L340:
	movzbl	1(%r9), %eax
	addl	%r10d, %r8d
	addq	$1, %r9
	movl	%eax, %esi
	shrl	%cl, %esi
	imull	%ebx, %esi
	shrl	$17, %esi
	movl	%esi, %r15d
	imull	%r12d, %r15d
	subl	%r15d, %eax
	imull	%r14d, %eax
	addl	%eax, %r13d
	leal	(%r11,%r8), %eax
	movb	%r13b, (%rdx)
	testl	%eax, %eax
	jle	.L354
.L339:
	addq	$1, %rdx
	movl	%esi, %r13d
	movb	%sil, (%rdx)
	testl	%r8d, %r8d
	jg	.L340
.L354:
	subq	%rdi, %rdx
	leal	1(%rdx), %eax
.L322:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movzbl	%r8b, %edx
.L325:
	movslq	%esi, %rax
	leaq	(%rdi,%rdx), %r9
	leaq	(%rdi,%rax), %r10
	cmpq	%r10, %r9
	jnb	.L341
	leaq	-1(%r10), %rcx
	movq	%rax, %r8
	subq	%r9, %rcx
	subq	%rdx, %r8
	cmpq	$14, %rcx
	jbe	.L327
	leaq	16(%rdi), %rcx
	cmpq	%rcx, %r9
	jb	.L327
	movq	%r8, %r11
	xorl	%ecx, %ecx
	andq	$-16, %r11
	.p2align 4,,10
	.p2align 3
.L328:
	movdqu	(%r9,%rcx), %xmm0
	movups	%xmm0, (%rdi,%rcx)
	addq	$16, %rcx
	cmpq	%r11, %rcx
	jne	.L328
	movq	%r8, %rsi
	andq	$-16, %rsi
	leaq	(%r9,%rsi), %rcx
	addq	%rsi, %rdi
	cmpq	%rsi, %r8
	je	.L330
	movzbl	(%rcx), %esi
	movb	%sil, (%rdi)
	leaq	1(%rcx), %rsi
	cmpq	%rsi, %r10
	jbe	.L330
	movzbl	1(%rcx), %esi
	movb	%sil, 1(%rdi)
	leaq	2(%rcx), %rsi
	cmpq	%rsi, %r10
	jbe	.L330
	movzbl	2(%rcx), %esi
	movb	%sil, 2(%rdi)
	leaq	3(%rcx), %rsi
	cmpq	%rsi, %r10
	jbe	.L330
	movzbl	3(%rcx), %esi
	movb	%sil, 3(%rdi)
	leaq	4(%rcx), %rsi
	cmpq	%rsi, %r10
	jbe	.L330
	movzbl	4(%rcx), %esi
	movb	%sil, 4(%rdi)
	leaq	5(%rcx), %rsi
	cmpq	%rsi, %r10
	jbe	.L330
	movzbl	5(%rcx), %esi
	movb	%sil, 5(%rdi)
	leaq	6(%rcx), %rsi
	cmpq	%rsi, %r10
	jbe	.L330
	movzbl	6(%rcx), %esi
	movb	%sil, 6(%rdi)
	leaq	7(%rcx), %rsi
	cmpq	%rsi, %r10
	jbe	.L330
	movzbl	7(%rcx), %esi
	movb	%sil, 7(%rdi)
	leaq	8(%rcx), %rsi
	cmpq	%rsi, %r10
	jbe	.L330
	movzbl	8(%rcx), %esi
	movb	%sil, 8(%rdi)
	leaq	9(%rcx), %rsi
	cmpq	%rsi, %r10
	jbe	.L330
	movzbl	9(%rcx), %esi
	movb	%sil, 9(%rdi)
	leaq	10(%rcx), %rsi
	cmpq	%rsi, %r10
	jbe	.L330
	movzbl	10(%rcx), %esi
	movb	%sil, 10(%rdi)
	leaq	11(%rcx), %rsi
	cmpq	%rsi, %r10
	jbe	.L330
	movzbl	11(%rcx), %esi
	movb	%sil, 11(%rdi)
	leaq	12(%rcx), %rsi
	cmpq	%rsi, %r10
	jbe	.L330
	movzbl	12(%rcx), %esi
	movb	%sil, 12(%rdi)
	leaq	13(%rcx), %rsi
	cmpq	%rsi, %r10
	jbe	.L330
	movzbl	13(%rcx), %esi
	movb	%sil, 13(%rdi)
	leaq	14(%rcx), %rsi
	cmpq	%rsi, %r10
	jbe	.L330
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%rdi)
.L330:
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	movslq	%edx, %rdx
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1, %eax
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L331:
	movzbl	(%r9,%rcx), %esi
	movb	%sil, (%rdi,%rcx)
	addq	$1, %rcx
	cmpq	%r8, %rcx
	jne	.L331
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L341:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2606:
	.size	_ZL15decShiftToLeastPhii.part.0, .-_ZL15decShiftToLeastPhii.part.0
	.p2align 4
	.type	_ZL7decTrimP9decNumberP10decContexthhPi.part.0, @function
_ZL7decTrimP9decNumberP10decContexthhPi.part.0:
.LFB2607:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdi), %ecx
	movl	4(%rdi), %r15d
	cmpl	$1, %ecx
	jle	.L356
	movq	%r8, %r13
	leaq	9(%rdi), %r8
	movq	%rdi, %r12
	movl	%r15d, %r11d
	leal	-1(%rcx), %ebx
	movq	%r8, %r10
	xorl	%edi, %edi
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L359:
	addl	$1, %edi
	addq	$1, %r10
	cmpl	%ebx, %edi
	je	.L360
.L361:
	movzbl	(%r10), %r9d
	movl	%r9d, %eax
	shrl	%eax
	imull	$26215, %eax, %eax
	shrl	$17, %eax
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	cmpl	%eax, %r9d
	jne	.L358
	testl	%r11d, %r11d
	jg	.L359
	testb	%dl, %dl
	jne	.L359
	testl	%r11d, %r11d
	je	.L358
	addl	$1, %edi
	addl	$1, %r11d
	addq	$1, %r10
	cmpl	%ebx, %edi
	jne	.L361
	.p2align 4,,10
	.p2align 3
.L360:
	cmpb	$0, 24(%rsi)
	je	.L362
.L391:
	testb	%r14b, %r14b
	je	.L388
.L362:
	movl	%ecx, %esi
	cmpl	$49, %ecx
	jle	.L389
.L363:
	cmpl	%esi, %ebx
	je	.L390
	movl	%ebx, %edx
	movq	%r8, %rdi
	call	_ZL15decShiftToLeastPhii.part.0
	movl	4(%r12), %r15d
	movl	(%r12), %ecx
.L364:
	addl	%ebx, %r15d
	subl	%ebx, %ecx
	movl	%r15d, 4(%r12)
	movl	%ecx, (%r12)
	movl	%ebx, 0(%r13)
.L356:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	testl	%edi, %edi
	je	.L356
	cmpb	$0, 24(%rsi)
	movl	%edi, %ebx
	jne	.L391
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L389:
	movslq	%ecx, %rax
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %esi
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L388:
	movl	4(%rsi), %eax
	subl	(%rsi), %eax
	addl	$1, %eax
	subl	%r15d, %eax
	testl	%eax, %eax
	jle	.L356
	cmpl	%eax, %ebx
	movl	%ecx, %esi
	cmovg	%eax, %ebx
	cmpl	$49, %ecx
	jg	.L363
	jmp	.L389
.L390:
	movb	$0, 9(%r12)
	jmp	.L364
	.cfi_endproc
.LFE2607:
	.size	_ZL7decTrimP9decNumberP10decContexthhPi.part.0, .-_ZL7decTrimP9decNumberP10decContexthhPi.part.0
	.p2align 4
	.type	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0, @function
_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0:
.LFB2608:
	.cfi_startproc
	movl	%ecx, %r10d
	subl	(%rsi), %r10d
	testl	%r10d, %r10d
	jle	.L546
	addl	%r10d, 4(%rdi)
	orl	$2048, (%r9)
	movl	(%r8), %eax
	cmpl	$1, %eax
	jle	.L405
	movl	$1, (%r8)
	cmpl	%r10d, %ecx
	jl	.L406
.L407:
	cmpl	$1, %r10d
	jle	.L431
	leal	-2(%r10), %eax
	leaq	1(%rdx,%rax), %r11
	.p2align 4,,10
	.p2align 3
.L413:
	cmpb	$0, (%rdx)
	je	.L412
	movl	$1, (%r8)
.L412:
	addq	$1, %rdx
	cmpq	%r11, %rdx
	jne	.L413
	movl	$1, %r10d
.L411:
	movl	%r10d, %ecx
	movzbl	(%r11), %eax
	subl	$1, %ecx
	jne	.L414
	cmpb	$4, %al
	ja	.L547
	testb	%al, %al
	je	.L417
	movl	$3, (%r8)
.L417:
	movl	(%rsi), %esi
	testl	%esi, %esi
	jle	.L548
	leaq	17(%r11), %rax
	leaq	9(%rdi), %r10
	movl	%esi, (%rdi)
	cmpq	%r10, %rax
	leaq	1(%r11), %rcx
	leaq	25(%rdi), %rax
	setbe	%dl
	cmpq	%rax, %rcx
	setnb	%al
	orb	%al, %dl
	leal	-1(%rsi), %edx
	je	.L420
	cmpl	$14, %edx
	jbe	.L420
	movl	%esi, %edx
	xorl	%eax, %eax
	shrl	$4, %edx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L421:
	movdqu	1(%r11,%rax), %xmm1
	movups	%xmm1, 9(%rdi,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L421
	movl	%esi, %edx
	movl	%esi, %edi
	andl	$-16, %edx
	andl	$15, %edi
	movl	%edx, %eax
	addq	%rax, %rcx
	addq	%r10, %rax
	cmpl	%edx, %esi
	je	.L543
	movzbl	(%rcx), %edx
	movb	%dl, (%rax)
	cmpl	$1, %edi
	je	.L543
	movzbl	1(%rcx), %edx
	movb	%dl, 1(%rax)
	cmpl	$2, %edi
	je	.L543
	movzbl	2(%rcx), %edx
	movb	%dl, 2(%rax)
	cmpl	$3, %edi
	je	.L543
	movzbl	3(%rcx), %edx
	movb	%dl, 3(%rax)
	cmpl	$4, %edi
	je	.L543
	movzbl	4(%rcx), %edx
	movb	%dl, 4(%rax)
	cmpl	$5, %edi
	je	.L543
	movzbl	5(%rcx), %edx
	movb	%dl, 5(%rax)
	cmpl	$6, %edi
	je	.L543
	movzbl	6(%rcx), %edx
	movb	%dl, 6(%rax)
	cmpl	$7, %edi
	je	.L543
	movzbl	7(%rcx), %edx
	movb	%dl, 7(%rax)
	cmpl	$8, %edi
	je	.L543
	movzbl	8(%rcx), %edx
	movb	%dl, 8(%rax)
	cmpl	$9, %edi
	je	.L543
	movzbl	9(%rcx), %edx
	movb	%dl, 9(%rax)
	cmpl	$10, %edi
	je	.L543
	movzbl	10(%rcx), %edx
	movb	%dl, 10(%rax)
	cmpl	$11, %edi
	je	.L543
	movzbl	11(%rcx), %edx
	movb	%dl, 11(%rax)
	cmpl	$12, %edi
	je	.L543
	movzbl	12(%rcx), %edx
	movb	%dl, 12(%rax)
	cmpl	$13, %edi
	je	.L543
	movzbl	13(%rcx), %edx
	movb	%dl, 13(%rax)
	cmpl	$14, %edi
	je	.L543
	movzbl	14(%rcx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L543:
	movl	(%r8), %eax
	testl	%eax, %eax
	je	.L541
	orl	$32, (%r9)
.L541:
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	cmpl	%r10d, %ecx
	jge	.L407
	cmpl	$1, %eax
	je	.L406
	testl	%ecx, %ecx
	jle	.L408
	subl	$1, %ecx
	leaq	1(%rdx,%rcx), %rcx
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L409:
	addq	$1, %rdx
	cmpq	%rcx, %rdx
	je	.L408
.L410:
	cmpb	$0, (%rdx)
	je	.L409
	movl	$1, (%r8)
.L406:
	orl	$32, (%r9)
	movb	$0, 9(%rdi)
	movl	$1, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	leaq	9(%rdi), %r10
	cmpq	%rdx, %r10
	je	.L394
	testl	%ecx, %ecx
	jle	.L399
	leaq	16(%rdx), %rax
	cmpq	%rax, %r10
	leaq	25(%rdi), %rax
	setnb	%sil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %sil
	leal	-1(%rcx), %esi
	je	.L396
	cmpl	$14, %esi
	jbe	.L396
	movl	%ecx, %esi
	xorl	%eax, %eax
	shrl	$4, %esi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L398:
	movdqu	(%rdx,%rax), %xmm0
	movups	%xmm0, 9(%rdi,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L398
	movl	%ecx, %esi
	movl	%ecx, %r11d
	andl	$-16, %esi
	andl	$15, %r11d
	movl	%esi, %eax
	addq	%rax, %rdx
	addq	%r10, %rax
	cmpl	%ecx, %esi
	je	.L399
	movzbl	(%rdx), %esi
	movb	%sil, (%rax)
	cmpl	$1, %r11d
	je	.L399
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rax)
	cmpl	$2, %r11d
	je	.L399
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rax)
	cmpl	$3, %r11d
	je	.L399
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rax)
	cmpl	$4, %r11d
	je	.L399
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rax)
	cmpl	$5, %r11d
	je	.L399
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rax)
	cmpl	$6, %r11d
	je	.L399
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rax)
	cmpl	$7, %r11d
	je	.L399
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rax)
	cmpl	$8, %r11d
	je	.L399
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rax)
	cmpl	$9, %r11d
	je	.L399
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rax)
	cmpl	$10, %r11d
	je	.L399
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rax)
	cmpl	$11, %r11d
	je	.L399
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rax)
	cmpl	$12, %r11d
	je	.L399
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rax)
	cmpl	$13, %r11d
	je	.L399
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rax)
	cmpl	$14, %r11d
	je	.L399
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L399:
	movl	%ecx, (%rdi)
.L394:
	movl	(%r8), %ecx
	testl	%ecx, %ecx
	je	.L541
	orl	$2080, (%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%al, %edx
	movl	%edx, %eax
	shrl	%cl, %eax
	movl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	_ZL7multies(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	_ZL9DECPOWERS(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	imull	0(%r13,%rcx,4), %eax
	movl	(%r12,%rcx,4), %ebx
	shrl	$17, %eax
	imull	%eax, %ebx
	movl	%ebx, %ecx
	movl	$1, %ebx
	cmpl	%ecx, %edx
	je	.L549
.L425:
	imull	$6554, %eax, %ecx
	shrl	$16, %ecx
	leal	0(,%rcx,8), %edx
	leal	(%rdx,%rcx,2), %edx
	subl	%edx, %eax
	leaq	_ZL6resmap(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	addl	%ebx, %eax
	movl	%eax, (%r8)
	movl	(%rsi), %ebx
	testl	%ebx, %ebx
	jle	.L426
	movl	%ebx, (%rdi)
	subl	$1, %ebx
	movl	%ecx, %edx
	movb	%cl, 9(%rdi)
	leal	(%r10,%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L419
	movl	%r10d, %eax
	leal	-1(%rcx), %r15d
	movq	%r8, -56(%rbp)
	xorl	%esi, %esi
	movl	0(%r13,%rax,4), %r14d
	movl	(%r12,%rax,4), %r13d
	movl	$1, %eax
	movl	%r10d, %ecx
	subl	%r10d, %eax
	movzbl	(%r12,%rax,4), %r12d
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L550:
	movb	%r8b, 10(%rdi,%rsi)
	movl	%r8d, %edx
	leaq	1(%rsi), %rax
	cmpq	%r15, %rsi
	je	.L539
	movq	%rax, %rsi
.L428:
	movzbl	1(%r11,%rsi), %eax
	movl	%eax, %r8d
	shrl	%cl, %r8d
	imull	%r14d, %r8d
	shrl	$17, %r8d
	movl	%r8d, %r10d
	imull	%r13d, %r10d
	subl	%r10d, %eax
	imull	%r12d, %eax
	addl	%eax, %edx
	movl	%ebx, %eax
	subl	%esi, %eax
	movb	%dl, 9(%rdi,%rsi)
	testl	%eax, %eax
	jg	.L550
.L539:
	movq	-56(%rbp), %r8
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L547:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	cmpb	$5, %al
	je	.L416
	movl	$7, (%r8)
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movb	$0, 9(%rdi)
	movl	$1, (%rdi)
.L419:
	movl	(%r8), %edx
	testl	%edx, %edx
	je	.L392
	orl	$32, (%r9)
.L392:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L549:
	.cfi_restore_state
	movl	(%r8), %ebx
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	testl	%eax, %eax
	jne	.L406
	movb	$0, 9(%rdi)
	movl	$1, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L548:
	movb	$0, 9(%rdi)
	movl	$1, (%rdi)
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L431:
	movq	%rdx, %r11
	jmp	.L411
.L416:
	addl	$5, (%r8)
	jmp	.L417
.L396:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L402:
	movzbl	(%rdx,%rax), %r10d
	movb	%r10b, 9(%rdi,%rax)
	movq	%rax, %r10
	addq	$1, %rax
	cmpq	%rsi, %r10
	jne	.L402
	movl	%ecx, (%rdi)
	jmp	.L394
.L420:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L424:
	movzbl	1(%r11,%rax), %ecx
	movb	%cl, 9(%rdi,%rax)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L424
	jmp	.L543
	.cfi_endproc
.LFE2608:
	.size	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0, .-_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	.p2align 4
	.type	_ZL14decUnitComparePKhiS0_ii, @function
_ZL14decUnitComparePKhiS0_ii:
.LFB2125:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jne	.L552
	movl	$1, %eax
	cmpl	%esi, %ecx
	jl	.L551
	jg	.L574
	movslq	%esi, %rsi
	subq	$1, %rsi
	leaq	(%rdx,%rsi), %rdx
	addq	%rdi, %rsi
	jnc	.L554
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L588:
	jb	.L574
	subq	$1, %rsi
	subq	$1, %rdx
	cmpq	%rsi, %r14
	ja	.L575
.L554:
	movzbl	(%rdx), %eax
	cmpb	%al, (%rsi)
	jbe	.L588
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L551:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L589
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L552:
	.cfi_restore_state
	movl	%r8d, %edi
	cmpl	$49, %r8d
	jg	.L555
	movslq	%r8d, %rax
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %edi
.L555:
	addl	%ecx, %edi
	movl	$1, %eax
	cmpl	%edi, %esi
	jg	.L551
	leal	1(%rsi), %eax
	cmpl	%eax, %edi
	jg	.L574
	addl	$2, %edi
	movslq	%edi, %rdi
	cmpq	$73, %rdi
	ja	.L590
	subq	$8, %rsp
	leaq	-128(%rbp), %rbx
	movq	%r13, %rdx
	movq	%r14, %rdi
	pushq	$-1
	movq	%rbx, %r9
	call	_ZL13decUnitAddSubPKhiS0_iiPhi
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	js	.L574
	cltq
	leaq	-1(%rbx,%rax), %rdx
	movzbl	-128(%rbp), %eax
	cmpq	%rbx, %rdx
	jbe	.L568
	xorl	%edi, %edi
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L591:
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	cmpq	%rdx, %rbx
	jnb	.L560
.L561:
	testb	%al, %al
	je	.L591
.L560:
	testb	%al, %al
	setne	%al
	movzbl	%al, %eax
	testq	%rdi, %rdi
	je	.L551
.L562:
	movl	%eax, -132(%rbp)
	call	uprv_free_67@PLT
	movl	-132(%rbp), %eax
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L590:
	movl	%r8d, -140(%rbp)
	movl	%ecx, -136(%rbp)
	movl	%esi, -132(%rbp)
	call	uprv_malloc_67@PLT
	movl	-132(%rbp), %esi
	movl	-136(%rbp), %ecx
	testq	%rax, %rax
	movl	-140(%rbp), %r8d
	movq	%rax, %rbx
	je	.L592
	subq	$8, %rsp
	movq	%r14, %rdi
	movq	%rax, %r9
	movq	%r13, %rdx
	pushq	$-1
	call	_ZL13decUnitAddSubPKhiS0_iiPhi
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	js	.L593
	cltq
	leaq	-1(%rbx,%rax), %rdx
	movzbl	(%rbx), %eax
	cmpq	%rdx, %rbx
	jnb	.L566
	movq	%rbx, %rdi
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L574:
	movl	$-1, %eax
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L575:
	xorl	%eax, %eax
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L593:
	movq	%rbx, %rdi
	movl	$-1, %eax
	jmp	.L562
.L568:
	testb	%al, %al
	setne	%al
	movzbl	%al, %eax
	jmp	.L551
.L566:
	testb	%al, %al
	movq	%rbx, %rdi
	setne	%al
	movzbl	%al, %eax
	jmp	.L562
.L589:
	call	__stack_chk_fail@PLT
.L592:
	movl	$-2147483648, %eax
	jmp	.L551
	.cfi_endproc
.LFE2125:
	.size	_ZL14decUnitComparePKhiS0_ii, .-_ZL14decUnitComparePKhiS0_ii
	.p2align 4
	.type	_ZL10decComparePK9decNumberS1_h, @function
_ZL10decComparePK9decNumberS1_h:
.LFB2124:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	cmpb	$0, 9(%rdi)
	movzbl	9(%rsi), %r8d
	jne	.L598
	cmpl	$1, (%rdi)
	je	.L640
.L598:
	testb	%dl, %dl
	je	.L641
.L596:
	movzbl	8(%rsi), %edx
	testb	%r8b, %r8b
	je	.L614
.L638:
	movzbl	8(%rdi), %ecx
	movl	$-1, %ebx
	movl	$1, %eax
.L604:
	movl	%edx, %r8d
	orl	%ecx, %r8d
	andl	$64, %r8d
	jne	.L642
.L609:
	movl	4(%rdi), %r8d
	movl	4(%rsi), %edx
	cmpl	%edx, %r8d
	jg	.L610
	movl	%r8d, %ecx
	movl	%eax, %ebx
	movq	%rsi, %rax
	movl	%edx, %r8d
	movq	%rdi, %rsi
	movl	%ecx, %edx
	movq	%rax, %rdi
.L610:
	movslq	(%rdi), %rcx
	subl	%edx, %r8d
	cmpl	$49, %ecx
	jg	.L611
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L611:
	movslq	(%rsi), %r9
	leaq	9(%rdi), %rdx
	cmpl	$49, %r9d
	jg	.L612
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%r9), %r9d
.L612:
	leaq	9(%rsi), %rdi
	movl	%r9d, %esi
	call	_ZL14decUnitComparePKhiS0_ii
	cmpl	$-2147483648, %eax
	je	.L594
	imull	%ebx, %eax
.L594:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	.cfi_restore_state
	movzbl	8(%rdi), %ecx
.L602:
	movsbl	%cl, %eax
	movl	%eax, %ebx
	sarl	$31, %eax
	sarl	$31, %ebx
	orl	$1, %eax
	andl	$2, %ebx
	subl	$1, %ebx
.L601:
	movzbl	8(%rsi), %edx
	testb	%r8b, %r8b
	jne	.L605
	cmpl	$1, (%rsi)
	je	.L643
.L605:
	movl	$1, %r8d
	testb	%dl, %dl
	js	.L644
.L608:
	cmpl	%r8d, %eax
	jl	.L639
	testl	%eax, %eax
	je	.L594
	movl	%edx, %r8d
	movl	$1, %eax
	orl	%ecx, %r8d
	andl	$64, %r8d
	je	.L609
.L642:
	andl	$64, %edx
	je	.L594
	andl	$64, %ecx
	movl	$0, %eax
	cmove	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	movl	(%rsi), %eax
	cmpl	$1, %eax
	jne	.L638
	testb	$112, %dl
	je	.L594
	movzbl	8(%rdi), %ecx
	movl	$-1, %ebx
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L640:
	movzbl	8(%rdi), %ecx
	testb	$112, %cl
	jne	.L599
	testb	%dl, %dl
	je	.L645
	testb	%r8b, %r8b
	jne	.L639
	cmpl	$1, (%rsi)
	jne	.L639
	xorl	%eax, %eax
	testb	$112, 8(%rsi)
	setne	%al
	negl	%eax
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L643:
	testb	$112, %dl
	jne	.L605
	cmpl	$1, %eax
	je	.L594
	xorl	%r8d, %r8d
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L644:
	cmpl	$-1, %eax
	je	.L604
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L599:
	.cfi_restore_state
	testb	%dl, %dl
	jne	.L596
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L645:
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	jmp	.L601
.L639:
	movl	$-1, %eax
	jmp	.L594
	.cfi_endproc
.LFE2124:
	.size	_ZL10decComparePK9decNumberS1_h, .-_ZL10decComparePK9decNumberS1_h
	.p2align 4
	.type	_ZL13decUnitAddSubPKhiS0_iiPhi.constprop.0, @function
_ZL13decUnitAddSubPKhiS0_iiPhi.constprop.0:
.LFB2615:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%ecx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movslq	%esi, %r12
	leaq	(%r8,%rcx), %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	(%r8,%r12), %rbx
	cmpq	%rsi, %rbx
	jnb	.L647
	movq	%rsi, %rax
	movq	%rbx, %rsi
	movq	%rax, %rbx
.L647:
	cmpq	%rsi, %r8
	jnb	.L648
	movq	%rsi, %r13
	xorl	%r11d, %r11d
	xorl	%ecx, %ecx
	movl	$-10, %r14d
	subq	%r8, %r13
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L649:
	testl	%r10d, %r10d
	jns	.L684
	addl	$100, %r10d
	movl	%r10d, %ecx
	shrl	%ecx
	imull	$26215, %ecx, %ecx
	shrl	$17, %ecx
	movl	%ecx, %eax
	subl	$10, %ecx
	imull	%r14d, %eax
	addl	%eax, %r10d
	movb	%r10b, (%r8,%r11)
.L650:
	addq	$1, %r11
	cmpq	%r13, %r11
	je	.L685
.L653:
	movzbl	(%rdi,%r11), %r10d
	addl	%r10d, %ecx
	movzbl	(%rdx,%r11), %r10d
	imull	%r9d, %r10d
	addl	%ecx, %r10d
	cmpl	$9, %r10d
	ja	.L649
	movb	%r10b, (%r8,%r11)
	addq	$1, %r11
	xorl	%ecx, %ecx
	cmpq	%r13, %r11
	jne	.L653
.L685:
	leaq	(%rdi,%r11), %r13
	addq	%r11, %rdx
	cmpq	%rbx, %rsi
	jnb	.L656
.L654:
	addq	%r12, %rdi
	movq	%rsi, %r11
	movl	$-10, %r12d
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L689:
	movzbl	0(%r13), %r10d
	addq	$1, %r13
	addl	%ecx, %r10d
	cmpl	$9, %r10d
	jbe	.L686
.L659:
	testl	%r10d, %r10d
	jns	.L687
	addl	$100, %r10d
	movl	%r10d, %ecx
	shrl	%ecx
	imull	$26215, %ecx, %ecx
	shrl	$17, %ecx
	movl	%ecx, %eax
	subl	$10, %ecx
	imull	%r12d, %eax
	addl	%eax, %r10d
	movb	%r10b, (%r11)
.L660:
	addq	$1, %r11
	cmpq	%r11, %rbx
	jbe	.L688
.L662:
	cmpq	%rdi, %r13
	jb	.L689
	movzbl	(%rdx), %r10d
	addq	$1, %rdx
	imull	%r9d, %r10d
	addl	%ecx, %r10d
	cmpl	$9, %r10d
	ja	.L659
.L686:
	movb	%r10b, (%r11)
	addq	$1, %r11
	xorl	%ecx, %ecx
	cmpq	%r11, %rbx
	ja	.L662
.L688:
	leaq	1(%rsi), %rdx
	movq	%rbx, %rax
	subq	%rsi, %rax
	cmpq	%rdx, %rbx
	movl	$1, %edx
	cmovb	%rdx, %rax
	addq	%rax, %rsi
.L656:
	testl	%ecx, %ecx
	je	.L683
	jg	.L690
	cmpq	%rbx, %r8
	jnb	.L673
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L681:
	movl	$1, %eax
.L670:
	movzbl	(%rdx), %esi
	addl	$9, %eax
	subl	%esi, %eax
	cmpl	$10, %eax
	je	.L667
	movb	%al, (%rdx)
	addq	$1, %rdx
	xorl	%eax, %eax
	cmpq	%rbx, %rdx
	jne	.L670
.L666:
	movl	%eax, %edx
	subl	%ecx, %edx
	cmpl	$1, %edx
	je	.L671
	notl	%ecx
	addq	$1, %rbx
	addl	%eax, %ecx
	movb	%cl, -1(%rbx)
.L671:
	movl	%r8d, %eax
	subl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L684:
	.cfi_restore_state
	movl	%r10d, %ecx
	movl	%r14d, %eax
	shrl	%ecx
	imull	$26215, %ecx, %ecx
	shrl	$17, %ecx
	imull	%ecx, %eax
	addl	%eax, %r10d
	movb	%r10b, (%r8,%r11)
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L687:
	movl	%r10d, %ecx
	movl	%r12d, %eax
	shrl	%ecx
	imull	$26215, %ecx, %ecx
	shrl	$17, %ecx
	imull	%ecx, %eax
	addl	%eax, %r10d
	movb	%r10b, (%r11)
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L690:
	movb	%cl, (%rsi)
	addq	$1, %rsi
.L683:
	movl	%esi, %eax
	subl	%r8d, %eax
.L646:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L667:
	.cfi_restore_state
	movb	$0, (%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rbx
	jne	.L681
.L682:
	movl	$1, %eax
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L648:
	xorl	%eax, %eax
	cmpq	%rbx, %r8
	jnb	.L646
	movq	%rdi, %r13
	movq	%r8, %rsi
	xorl	%ecx, %ecx
	jmp	.L654
.L673:
	movq	%r8, %rbx
	jmp	.L682
	.cfi_endproc
.LFE2615:
	.size	_ZL13decUnitAddSubPKhiS0_iiPhi.constprop.0, .-_ZL13decUnitAddSubPKhiS0_iiPhi.constprop.0
	.p2align 4
	.type	_ZL13decApplyRoundP9decNumberP10decContextiPj.part.0, @function
_ZL13decApplyRoundP9decNumberP10decContextiPj.part.0:
.LFB2614:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$7, 12(%rsi)
	ja	.L692
	movl	12(%rsi), %eax
	leaq	.L694(%rip), %rcx
	movq	%rdi, %r13
	movq	%rsi, %r14
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L694:
	.long	.L701-.L694
	.long	.L709-.L694
	.long	.L699-.L694
	.long	.L698-.L694
	.long	.L697-.L694
	.long	.L696-.L694
	.long	.L695-.L694
	.long	.L693-.L694
	.text
	.p2align 4,,10
	.p2align 3
.L695:
	cmpb	$0, 8(%rdi)
	js	.L709
.L696:
	testl	%edx, %edx
	jns	.L691
	movzbl	9(%r13), %esi
.L702:
	movslq	0(%r13), %r10
	leaq	9(%r13), %rdi
	movq	%rdi, %rbx
	movl	%r10d, %r12d
	cmpl	$1, %r10d
	ja	.L718
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L745:
	subl	$1, %r12d
	addq	$1, %rbx
	movzbl	(%rbx), %esi
	cmpl	$1, %r12d
	je	.L724
.L718:
	cmpb	$0, (%rbx)
	je	.L745
	movl	$-1, %r9d
.L713:
	cmpl	$49, %r10d
	jg	.L725
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%r10), %r10d
.L725:
	addq	$24, %rsp
	movq	%rdi, %r8
	movl	$1, %ecx
	movl	%r10d, %esi
	popq	%rbx
	leaq	_ZL7uarrone(%rip), %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZL13decUnitAddSubPKhiS0_iiPhi.constprop.0
	.p2align 4,,10
	.p2align 3
.L701:
	.cfi_restore_state
	cmpb	$0, 8(%rdi)
	js	.L696
.L709:
	testl	%edx, %edx
	jle	.L691
.L708:
	movzbl	9(%r13), %esi
.L704:
	movslq	0(%r13), %r10
	leaq	9(%r13), %rdi
	movq	%rdi, %rax
	movl	%r10d, %edx
	cmpl	$1, %r10d
	ja	.L712
	.p2align 4,,10
	.p2align 3
.L717:
	leaq	_ZL9DECPOWERS(%rip), %r8
	movl	%edx, %ecx
	movl	$1, %r9d
	movl	(%r8,%rcx,4), %ecx
	subl	$1, %ecx
	cmpl	%ecx, %esi
	jne	.L713
	subl	$1, %edx
	movl	(%r8,%rdx,4), %edx
	movb	%dl, (%rax)
	leaq	-1(%rax), %rdx
	cmpq	%rdx, %rdi
	ja	.L716
	movq	%rax, %rcx
	subq	%rax, %rdx
	xorl	%esi, %esi
	subq	%r13, %rcx
	leaq	10(%rdx,%r13), %rdi
	leaq	-9(%rcx), %r8
	movq	%r8, %rdx
	call	memset@PLT
.L716:
	movl	4(%r13), %eax
	addl	$1, %eax
	movl	%eax, 4(%r13)
	movl	4(%r14), %edi
	addl	0(%r13), %eax
	leal	1(%rdi), %edx
	cmpl	%edx, %eax
	jle	.L691
	addq	$24, %rsp
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZL14decSetOverflowP9decNumberP10decContextPj
	.p2align 4,,10
	.p2align 3
.L746:
	.cfi_restore_state
	subl	$1, %edx
	addq	$1, %rax
	movzbl	(%rax), %esi
	cmpl	$1, %edx
	je	.L717
.L712:
	cmpb	$9, (%rax)
	je	.L746
	movl	$1, %r9d
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L724:
	leaq	_ZL9DECPOWERS(%rip), %rax
	leal	-1(%r12), %edx
	movl	$-1, %r9d
	movl	(%rax,%rdx,4), %ecx
	cmpl	%ecx, %esi
	jne	.L713
	movl	%r12d, %edx
	movl	(%rax,%rdx,4), %eax
	subl	$1, %eax
	movb	%al, (%rbx)
	leaq	-1(%rbx), %rax
	cmpq	%rax, %rdi
	ja	.L721
	movq	%rbx, %rdx
	subq	%rbx, %rax
	movl	$9, %esi
	movl	%ecx, -52(%rbp)
	subq	%r13, %rdx
	leaq	10(%rax,%r13), %rdi
	subq	$9, %rdx
	call	memset@PLT
	movl	-52(%rbp), %ecx
.L721:
	movl	4(%r13), %eax
	subl	$1, %eax
	movl	%eax, 4(%r13)
	movl	8(%r14), %edx
	subl	(%r14), %edx
	cmpl	%edx, %eax
	jne	.L691
	cmpl	$1, %r12d
	je	.L747
.L722:
	subl	$1, %ecx
	movb	%cl, (%rbx)
	subl	$1, 0(%r13)
.L723:
	addl	$1, 4(%r13)
	orl	$14368, (%r15)
.L691:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L699:
	.cfi_restore_state
	cmpl	$4, %edx
	jg	.L708
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L698:
	cmpl	$5, %edx
	jg	.L708
	jne	.L691
	movzbl	9(%rdi), %esi
	testb	$1, %sil
	jne	.L704
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L697:
	cmpl	$5, %edx
	jg	.L708
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L693:
	movzbl	9(%rdi), %ecx
	leal	(%rcx,%rcx,4), %eax
	movl	%ecx, %edi
	movl	%ecx, %esi
	leal	(%rcx,%rax,8), %eax
	leal	(%rax,%rax,4), %eax
	shrw	$10, %ax
	leal	(%rax,%rax,4), %eax
	subl	%eax, %edi
	movzbl	%dil, %eax
	testl	%edx, %edx
	jns	.L732
	cmpl	$1, %eax
	jne	.L702
.L732:
	testl	%edx, %edx
	jle	.L691
	testl	%eax, %eax
	je	.L704
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L692:
	orl	$64, (%rcx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L747:
	.cfi_restore_state
	cmpl	$1, 0(%r13)
	jne	.L722
	movb	$0, (%rbx)
	jmp	.L723
	.cfi_endproc
.LFE2614:
	.size	_ZL13decApplyRoundP9decNumberP10decContextiPj.part.0, .-_ZL13decApplyRoundP9decNumberP10decContextiPj.part.0
	.p2align 4
	.type	_ZL15decSetSubnormalP9decNumberP10decContextPiPj, @function
_ZL15decSetSubnormalP9decNumberP10decContextPiPj:
.LFB2137:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movl	8(%rsi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	subl	$1, %eax
	subl	%eax, %r13d
	cmpb	$0, 9(%rdi)
	jne	.L749
	cmpl	$1, (%rdi)
	je	.L767
.L749:
	movl	(%r12), %edx
	movl	%edx, %eax
	orb	$16, %ah
	movl	%eax, (%r12)
	movl	%r13d, %eax
	subl	4(%rbx), %eax
	testl	%eax, %eax
	jle	.L768
	movq	16(%rsi), %rdx
	movl	(%rbx), %ecx
	leaq	9(%rbx), %r15
	movq	%r12, %r9
	movdqu	(%rsi), %xmm0
	movq	%r14, %r8
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movl	24(%rsi), %edx
	leaq	-96(%rbp), %rsi
	movaps	%xmm0, -96(%rbp)
	movl	%edx, -72(%rbp)
	movl	%ecx, %edx
	subl	%eax, %edx
	subl	%eax, -88(%rbp)
	movl	%edx, -96(%rbp)
	movq	%r15, %rdx
	movq	%rsi, -104(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	(%r14), %edx
	testl	%edx, %edx
	je	.L753
	movq	-104(%rbp), %rsi
	movq	%r12, %rcx
	movq	%rbx, %rdi
	call	_ZL13decApplyRoundP9decNumberP10decContextiPj.part.0
.L753:
	movl	(%r12), %eax
	testb	$32, %al
	je	.L754
	orb	$32, %ah
	movl	%eax, (%r12)
.L754:
	cmpl	%r13d, 4(%rbx)
	jg	.L769
	cmpb	$0, 9(%rbx)
	je	.L770
	.p2align 4,,10
	.p2align 3
.L748:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L771
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L770:
	.cfi_restore_state
	cmpl	$1, (%rbx)
	jne	.L748
	testb	$112, 8(%rbx)
	jne	.L748
	orl	$1024, (%r12)
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L768:
	testb	$32, %dl
	je	.L748
	orb	$48, %dh
	movl	%edx, (%r12)
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L769:
	movl	(%rbx), %esi
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZL14decShiftToMostPhii.part.0
	subl	$1, 4(%rbx)
	cmpb	$0, 9(%rbx)
	movl	%eax, (%rbx)
	jne	.L748
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L767:
	testb	$112, 8(%rdi)
	jne	.L749
	cmpl	%r13d, 4(%rdi)
	jge	.L748
	movl	%r13d, 4(%rdi)
	orl	$1024, (%rcx)
	jmp	.L748
.L771:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2137:
	.size	_ZL15decSetSubnormalP9decNumberP10decContextPiPj, .-_ZL15decSetSubnormalP9decNumberP10decContextPiPj
	.p2align 4
	.type	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0, @function
_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0:
.LFB2611:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzbl	8(%rsi), %eax
	testb	$16, %al
	je	.L773
	orl	$1073741952, (%r8)
.L774:
	movl	(%rcx), %eax
	cmpl	%eax, (%rsi)
	jle	.L813
.L776:
	movzbl	8(%rsi), %eax
	leaq	9(%r11), %rdx
	leaq	9(%rsi), %r8
	movq	%rdx, %r9
	movq	%r8, %r10
	movb	%al, 8(%r11)
	movslq	(%rcx), %rax
	cmpl	$49, %eax
	jle	.L814
	movslq	%eax, %rdi
	addq	%rdx, %rdi
	cmpq	%rdi, %rdx
	jnb	.L789
.L788:
	leaq	25(%rsi), %rbx
	leaq	10(%r11), %rax
	cmpq	%rbx, %rdx
	leaq	25(%r11), %rbx
	setnb	%r12b
	cmpq	%r8, %rbx
	setbe	%bl
	orb	%bl, %r12b
	je	.L805
	movq	%rdi, %rbx
	subq	%r11, %rbx
	subq	$10, %rbx
	cmpq	$14, %rbx
	seta	%r12b
	cmpq	%rax, %rdi
	setnb	%bl
	testb	%bl, %r12b
	je	.L805
	movq	%rdi, %r9
	subq	%r11, %r9
	cmpq	%rax, %rdi
	movl	$1, %eax
	leaq	-9(%r9), %r10
	cmovb	%rax, %r10
	movl	$9, %eax
	movq	%r10, %r9
	andq	$-16, %r9
	addq	$9, %r9
	.p2align 4,,10
	.p2align 3
.L791:
	movdqu	(%rsi,%rax), %xmm0
	movups	%xmm0, (%r11,%rax)
	addq	$16, %rax
	cmpq	%r9, %rax
	jne	.L791
	movq	%r10, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	addq	%rsi, %r8
	cmpq	%rsi, %r10
	je	.L793
	movzbl	(%r8), %edx
	movb	%dl, (%rax)
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L793
	movzbl	1(%r8), %edx
	movb	%dl, 1(%rax)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L793
	movzbl	2(%r8), %edx
	movb	%dl, 2(%rax)
	leaq	3(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L793
	movzbl	3(%r8), %edx
	movb	%dl, 3(%rax)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L793
	movzbl	4(%r8), %edx
	movb	%dl, 4(%rax)
	leaq	5(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L793
	movzbl	5(%r8), %edx
	movb	%dl, 5(%rax)
	leaq	6(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L793
	movzbl	6(%r8), %edx
	movb	%dl, 6(%rax)
	leaq	7(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L793
	movzbl	7(%r8), %edx
	movb	%dl, 7(%rax)
	leaq	8(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L793
	movzbl	8(%r8), %edx
	movb	%dl, 8(%rax)
	leaq	9(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L793
	movzbl	9(%r8), %edx
	movb	%dl, 9(%rax)
	leaq	10(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L793
	movzbl	10(%r8), %edx
	movb	%dl, 10(%rax)
	leaq	11(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L793
	movzbl	11(%r8), %edx
	movb	%dl, 11(%rax)
	leaq	12(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L793
	movzbl	12(%r8), %edx
	movb	%dl, 12(%rax)
	leaq	13(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L793
	movzbl	13(%r8), %edx
	movb	%dl, 13(%rax)
	leaq	14(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L793
	movzbl	14(%r8), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L793:
	movslq	(%rcx), %rax
	cmpl	$49, %eax
	jg	.L789
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
.L789:
	movl	%eax, (%r11)
	movl	(%rcx), %edx
	cmpl	%edx, %eax
	jg	.L794
.L812:
	movzbl	8(%r11), %eax
.L778:
	andl	$-17, %eax
	popq	%rbx
	popq	%r12
	movl	$0, 4(%r11)
	orl	$32, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movb	%al, 8(%r11)
	movq	%r11, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L773:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L774
	testb	$16, 8(%rdx)
	je	.L775
	movq	%rdx, %rsi
	orl	$1073741952, (%r8)
	movl	(%rcx), %eax
	cmpl	%eax, (%rsi)
	jg	.L776
	.p2align 4,,10
	.p2align 3
.L813:
	cmpq	%r11, %rsi
	je	.L812
	movzbl	8(%rsi), %eax
	movb	%al, 8(%r11)
	movl	4(%rsi), %edx
	movl	%edx, 4(%r11)
	movl	(%rsi), %edx
	movl	%edx, (%r11)
	movzbl	9(%rsi), %edx
	movb	%dl, 9(%r11)
	movl	(%rsi), %edx
	cmpl	$1, %edx
	jle	.L778
	leaq	10(%r11), %r8
	leaq	9(%rsi), %rdi
	movslq	%edx, %rcx
	cmpl	$49, %edx
	jg	.L780
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rcx), %ecx
.L780:
	addq	%rdi, %rcx
	leaq	10(%rsi), %rdx
	cmpq	%rdx, %rcx
	jbe	.L778
	movq	%rcx, %rax
	subq	%rsi, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L781
	leaq	25(%rsi), %rax
	subq	%r8, %rax
	cmpq	$30, %rax
	jbe	.L781
	movq	%rcx, %rax
	subq	%rsi, %rax
	leaq	-10(%rax), %r9
	movl	$10, %eax
	movq	%r9, %rdi
	andq	$-16, %rdi
	addq	$10, %rdi
	.p2align 4,,10
	.p2align 3
.L782:
	movdqu	(%rsi,%rax), %xmm1
	movups	%xmm1, (%r11,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L782
	movq	%r9, %rsi
	andq	$-16, %rsi
	addq	%rsi, %rdx
	leaq	(%r8,%rsi), %rax
	cmpq	%rsi, %r9
	je	.L812
	movzbl	(%rdx), %esi
	movb	%sil, (%rax)
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L812
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rax)
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L812
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rax)
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L812
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rax)
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L812
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rax)
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L812
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rax)
	leaq	6(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L812
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rax)
	leaq	7(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L812
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rax)
	leaq	8(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L812
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rax)
	leaq	9(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L812
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rax)
	leaq	10(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L812
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rax)
	leaq	11(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L812
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rax)
	leaq	12(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L812
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rax)
	leaq	13(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L812
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rax)
	leaq	14(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L812
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L805:
	movzbl	(%r10), %eax
	addq	$1, %r9
	addq	$1, %r10
	movb	%al, -1(%r9)
	cmpq	%r9, %rdi
	ja	.L805
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L814:
	leaq	_ZL8d2utable(%rip), %rdi
	movzbl	(%rdi,%rax), %edi
	movq	%rdi, %rax
	addq	%rdx, %rdi
	cmpq	%rdi, %rdx
	jb	.L788
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L794:
	subl	%edx, %eax
	movq	%r11, %rdi
	movl	%eax, %esi
	call	_ZL8decDecapP9decNumberi
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L775:
	testb	$32, %al
	cmove	%rdx, %rsi
	jmp	.L774
.L781:
	movq	%rcx, %rax
	movl	$10, %edx
	subq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L785:
	movzbl	(%rsi,%rdx), %ecx
	movb	%cl, (%r11,%rdx)
	addq	$1, %rdx
	cmpq	%rax, %rdx
	jne	.L785
	jmp	.L812
	.cfi_endproc
.LFE2611:
	.size	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0, .-_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	.p2align 4
	.type	_ZL11decFinalizeP9decNumberP10decContextPiPj, @function
_ZL11decFinalizeP9decNumberP10decContextPiPj:
.LFB2134:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	8(%rsi), %r8d
	movslq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	4(%rdi), %edx
	movl	%r8d, %eax
	subl	%esi, %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	jg	.L816
	jl	.L864
	movl	$256, %eax
	cmpb	$0, 9(%rdi)
	movl	$1, -68(%rbp)
	movw	%ax, -60(%rbp)
	movzbl	8(%rdi), %eax
	movl	%r8d, -64(%rbp)
	jne	.L819
	cmpl	$1, %esi
	jne	.L819
	testb	$112, %al
	je	.L816
	testb	$64, %al
	jne	.L816
	leaq	9(%rdi), %rdi
	cmpl	%edx, %r8d
	jl	.L837
	subl	%edx, %r8d
.L838:
	movq	%rdi, %rax
	movl	$1, %ecx
	leaq	-59(%rbp), %rdi
	movl	$1, %ebx
	leaq	_ZL8d2utable(%rip), %rdx
.L834:
	movzbl	(%rdx,%rsi), %r9d
	movl	%ecx, %esi
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L819:
	testb	$64, %al
	je	.L865
	.p2align 4,,10
	.p2align 3
.L816:
	movl	(%r15), %edx
.L826:
	testl	%edx, %edx
	jne	.L866
.L828:
	movl	4(%r13), %ecx
	movl	4(%r12), %edi
	movl	%ecx, %edx
	subl	0(%r13), %edx
	addl	$1, %edx
	cmpl	%edx, %edi
	jle	.L815
	movl	(%r12), %esi
	subl	%esi, %ecx
	addl	$1, %ecx
	cmpl	%ecx, %edi
	jg	.L867
	cmpb	$0, 24(%r13)
	je	.L815
	subl	%edx, %edi
	cmpb	$0, 9(%r12)
	movl	%edi, %r13d
	jne	.L830
	cmpl	$1, %esi
	je	.L868
.L830:
	testl	%r13d, %r13d
	je	.L832
	movl	%r13d, %edx
	leaq	9(%r12), %rdi
	call	_ZL14decShiftToMostPhii.part.0
	movl	4(%r12), %edx
	movl	%eax, %esi
	subl	%r13d, %edx
.L832:
	movl	%esi, (%r12)
.L831:
	movl	%edx, 4(%r12)
	orl	$1024, (%r14)
.L815:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L869
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_restore_state
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL13decApplyRoundP9decNumberP10decContextiPj.part.0
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L865:
	leaq	9(%r12), %rdi
	cmpl	%edx, %r8d
	jge	.L870
	subl	%r8d, %edx
	movl	%edx, %r8d
	cmpl	$49, %esi
	jle	.L835
	leaq	-59(%rbp), %rax
	movl	$1, %r9d
	movl	$-1, %ebx
.L836:
	movl	%esi, %ecx
	movq	%rdi, %rdx
	movl	%r9d, %esi
	movq	%rax, %rdi
.L833:
	call	_ZL14decUnitComparePKhiS0_ii
	cmpl	$-2147483648, %eax
	je	.L825
	imull	%ebx, %eax
	movl	(%r15), %edx
	testl	%eax, %eax
	jne	.L826
	testl	%edx, %edx
	jns	.L826
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL13decApplyRoundP9decNumberP10decContextiPj.part.0
	.p2align 4,,10
	.p2align 3
.L864:
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL15decSetSubnormalP9decNumberP10decContextPiPj
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L870:
	subl	%edx, %r8d
	cmpl	$49, %esi
	jle	.L838
	movl	$1, %ebx
	movl	$1, %ecx
	leaq	-59(%rbp), %rdx
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L867:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL14decSetOverflowP9decNumberP10decContextPj
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L825:
	orl	$16, (%r14)
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L868:
	testb	$112, 8(%r12)
	je	.L831
	jmp	.L830
.L869:
	call	__stack_chk_fail@PLT
.L837:
	subl	%r8d, %edx
	movl	%edx, %r8d
.L835:
	leaq	_ZL8d2utable(%rip), %rdx
	leaq	-59(%rbp), %rax
	orl	$-1, %ebx
	movzbl	(%rdx,%rsi), %ecx
	movl	$1, %esi
	jmp	.L834
	.cfi_endproc
.LFE2134:
	.size	_ZL11decFinalizeP9decNumberP10decContextPiPj, .-_ZL11decFinalizeP9decNumberP10decContextPiPj
	.p2align 4
	.type	_ZL12decCompareOpP9decNumberPKS_S2_P10decContexthPj, @function
_ZL12decCompareOpP9decNumberPKS_S2_P10decContexthPj:
.LFB2123:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$40, %rsp
	movzbl	8(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	8(%r14), %eax
	cmpb	$4, %r8b
	je	.L977
	movl	%eax, %ecx
	orl	%edx, %ecx
	testb	$48, %cl
	je	.L877
	cmpb	$1, %r8b
	je	.L878
	cmpb	$6, %r8b
	je	.L978
	andl	$16, %ecx
	jne	.L878
	testb	$48, %dl
	jne	.L979
.L897:
	movl	$0, -60(%rbp)
	testb	$32, %dl
	jne	.L920
.L922:
	movl	%edx, %ecx
.L921:
	movb	%cl, 8(%r12)
	movl	4(%r13), %eax
	leaq	-60(%rbp), %r14
	movq	%r10, %rsi
	leaq	9(%r13), %rdx
	movq	%r12, %rdi
	movq	%r15, %r9
	movq	%r14, %r8
	movl	%eax, 4(%r12)
	movl	0(%r13), %ecx
	movq	%r10, -72(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	-72(%rbp), %r10
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L877:
	leal	-7(%r8), %eax
	cmpb	$1, %al
	jbe	.L980
.L898:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r10, -72(%rbp)
	call	_ZL10decComparePK9decNumberS1_h
	cmpl	$-2147483648, %eax
	je	.L900
	cmpb	$1, %bl
	je	.L939
	cmpb	$6, %bl
	movq	-72(%rbp), %r10
	jne	.L901
.L939:
	testl	%eax, %eax
	sete	%dl
	cmpb	$4, %bl
	sete	%cl
	andl	%ecx, %edx
.L903:
	testb	%dl, %dl
	je	.L905
	movl	4(%r14), %eax
	cmpl	%eax, 4(%r13)
	je	.L981
	movzbl	8(%r13), %eax
	jl	.L907
	testb	%al, %al
	js	.L908
.L970:
	movl	$256, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	.p2align 4,,10
	.p2align 3
.L876:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L982
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L977:
	.cfi_restore_state
	testb	%dl, %dl
	js	.L983
	testb	%al, %al
	js	.L970
.L874:
	movl	%eax, %ecx
	orl	%edx, %ecx
	andl	$48, %ecx
	je	.L898
	testb	$48, %dl
	je	.L883
	testb	$48, %al
	je	.L881
	testb	$16, %dl
	je	.L882
	testb	$32, %al
	jne	.L883
.L882:
	testb	$32, %dl
	je	.L884
	testb	$16, %al
	je	.L884
.L885:
	testb	%dl, %dl
	js	.L937
	movq	$1, (%r12)
	movb	$0, 8(%r12)
	movb	$1, 9(%r12)
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L980:
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r10, -72(%rbp)
	call	_ZL10decComparePK9decNumberS1_h
	movq	-72(%rbp), %r10
	cmpl	$-2147483648, %eax
	je	.L900
.L899:
	cmpb	$3, %bl
	movl	$0, -60(%rbp)
	sete	%sil
	cmpb	$8, %bl
	sete	%dl
	orl	%edx, %esi
	testl	%eax, %eax
	jne	.L912
	movzbl	8(%r13), %ecx
	movzbl	8(%r14), %eax
	movl	%ecx, %r8d
	movl	%eax, %edi
	movl	%ecx, %edx
	andl	$-128, %r8d
	andl	$-128, %edi
	cmpb	%dil, %r8b
	je	.L913
	testb	%cl, %cl
	js	.L966
.L965:
	testb	%sil, %sil
	je	.L921
.L920:
	movl	%eax, %ecx
	movq	%r14, %r13
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L978:
	orl	$1073741952, (%r9)
.L878:
	movq	%r15, %r8
	movq	%r10, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L900:
	orl	$16, (%r15)
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L983:
	testb	%al, %al
	js	.L874
.L908:
	movq	$1, (%r12)
	movb	$1, 9(%r12)
.L875:
	movb	$-128, 8(%r12)
	jmp	.L876
.L905:
	movq	$1, (%r12)
	movb	$0, 8(%r12)
	testl	%eax, %eax
	jne	.L911
	movb	$0, 9(%r12)
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L981:
	xorl	%edx, %edx
	movq	$1, (%r12)
	movw	%dx, 8(%r12)
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L907:
	testb	%al, %al
	jns	.L908
	jmp	.L970
.L901:
	cmpb	$4, %bl
	jne	.L904
.L926:
	testl	%eax, %eax
	sete	%dl
	jmp	.L903
.L904:
	cmpb	$5, %bl
	jne	.L899
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L883:
	testb	%dl, %dl
	js	.L938
	movq	$1, (%r12)
	movb	$1, 9(%r12)
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L979:
	testb	$48, %al
	je	.L897
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L913:
	movl	4(%r13), %r8d
	movl	4(%r14), %edi
	testb	%cl, %cl
	jns	.L916
	testb	%al, %al
	jns	.L916
	cmpl	%edi, %r8d
	jl	.L965
	.p2align 4,,10
	.p2align 3
.L966:
	testb	%sil, %sil
	jne	.L922
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L912:
	testb	%sil, %sil
	je	.L924
	negl	%eax
.L924:
	testl	%eax, %eax
	jle	.L984
	movzbl	8(%r13), %edx
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L911:
	movb	$1, 9(%r12)
	js	.L875
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L984:
	movzbl	8(%r14), %eax
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L881:
	testb	%dl, %dl
	jns	.L970
.L937:
	movl	$-1, %eax
	jmp	.L926
.L889:
	testb	%dl, %dl
	jns	.L908
	.p2align 4,,10
	.p2align 3
.L938:
	movl	$1, %eax
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L916:
	cmpl	%edi, %r8d
	jle	.L966
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L884:
	movslq	(%r14), %rcx
	cmpl	$49, %ecx
	jg	.L886
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L886:
	movslq	0(%r13), %rax
	cmpl	$49, %eax
	jg	.L887
	leaq	_ZL8d2utable(%rip), %rsi
	movzbl	(%rsi,%rax), %eax
.L887:
	cmpl	%eax, %ecx
	jl	.L885
	jg	.L889
	leaq	9(%r13), %rcx
	leaq	-1(%rax), %rsi
	leaq	8(%r14,%rax), %rdi
	addq	%rsi, %rcx
	jc	.L932
	leaq	8(%r13), %rsi
	xorl	%eax, %eax
	subq	%rcx, %rsi
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L985:
	jb	.L931
	subq	$1, %rax
	cmpq	%rsi, %rax
	je	.L932
.L891:
	movzbl	(%rdi,%rax), %ebx
	cmpb	%bl, (%rcx,%rax)
	jbe	.L985
	movl	$-1, %ecx
	movl	$1, %eax
.L890:
	testb	%dl, %dl
	cmovs	%ecx, %eax
	jmp	.L926
.L932:
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.L890
.L931:
	movl	$1, %ecx
	movl	$-1, %eax
	jmp	.L890
.L982:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2123:
	.size	_ZL12decCompareOpP9decNumberPKS_S2_P10decContexthPj, .-_ZL12decCompareOpP9decNumberPKS_S2_P10decContexthPj
	.p2align 4
	.type	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj, @function
_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj:
.LFB2119:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -512(%rbp)
	movzbl	8(%rsi), %edx
	movq	%rsi, -488(%rbp)
	movq	%r8, -504(%rbp)
	movl	%edx, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	8(%r13), %eax
	movl	$0, -468(%rbp)
	xorl	%eax, %ecx
	andl	$-128, %ecx
	movb	%cl, -489(%rbp)
	movl	%edx, %ecx
	orl	%eax, %ecx
	testb	$112, %cl
	je	.L987
	andl	$48, %ecx
	jne	.L1099
	testb	$64, %dl
	jne	.L990
	cmpb	$0, 9(%rsi)
	jne	.L990
	cmpl	$1, (%rsi)
	je	.L1100
	.p2align 4,,10
	.p2align 3
.L990:
	testb	$64, %al
	jne	.L992
	cmpb	$0, 9(%r13)
	jne	.L992
	cmpl	$1, 0(%r13)
	jne	.L992
	testb	$112, %al
	je	.L991
	.p2align 4,,10
	.p2align 3
.L992:
	movzbl	-489(%rbp), %eax
	movq	$1, (%r12)
	movb	$0, 9(%r12)
	orl	$64, %eax
	movb	%al, 8(%r12)
.L989:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1101
	addq	$584, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L987:
	.cfi_restore_state
	movl	(%rsi), %edx
	movslq	0(%r13), %rcx
	cmpl	%ecx, %edx
	jge	.L1102
	cmpl	$2, %edx
	jle	.L994
.L1108:
	addl	$8, %ecx
	leal	8(%rdx), %ebx
	movslq	%ecx, %rax
	imulq	$954437177, %rbx, %rbx
	sarl	$31, %ecx
	imulq	$954437177, %rax, %rax
	shrq	$33, %rbx
	sarq	$33, %rax
	subl	%ecx, %eax
	leal	(%rax,%rbx), %r15d
	sall	$2, %eax
	cmpl	$40, %eax
	jg	.L1103
	leaq	-464(%rbp), %rax
	xorl	%r14d, %r14d
	movq	$0, -536(%rbp)
	movq	%rax, -552(%rbp)
.L995:
	leaq	-416(%rbp), %rdi
	leal	0(,%rbx,4), %eax
	movq	$0, -528(%rbp)
	movq	%rdi, -520(%rbp)
	cmpl	$10, %ebx
	ja	.L1104
.L996:
	movl	%r15d, %edx
	leal	14(%r15), %eax
	addl	$7, %edx
	cmovns	%edx, %eax
	movl	%eax, %ebx
	andl	$-8, %eax
	leal	(%rax,%r15,8), %eax
	sarl	$3, %ebx
	cmpl	$160, %eax
	jg	.L1105
	testb	%r14b, %r14b
	je	.L1064
	movq	-504(%rbp), %rax
	orl	$16, (%rax)
	.p2align 4,,10
	.p2align 3
.L1047:
	movq	-528(%rbp), %rax
	testq	%rax, %rax
	je	.L1048
	movq	%rax, %rdi
	call	uprv_free_67@PLT
.L1048:
	movq	-536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L989
	call	uprv_free_67@PLT
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L994:
	leaq	_ZL8d2utable(%rip), %r14
	movslq	%edx, %rsi
	movzbl	(%r14,%rsi), %eax
	cmpl	$49, %ecx
	jle	.L1106
	addl	%ecx, %eax
	cmpl	$145, %eax
	jg	.L1053
	movq	-488(%rbp), %rax
	movb	$0, -208(%rbp)
	leaq	-208(%rbp), %r10
	movq	$0, -544(%rbp)
	leaq	9(%rax), %rbx
.L1052:
	movzbl	(%r14,%rsi), %r11d
.L1038:
	addq	%rbx, %r11
	cmpq	%rbx, %r11
	jbe	.L1039
	leaq	9(%r13), %r15
	movq	%r12, -528(%rbp)
	xorl	%r14d, %r14d
	movl	$1, %eax
	movq	%r13, -536(%rbp)
	movq	%r10, %r12
	movl	%ecx, %r13d
	movq	%r15, -520(%rbp)
	movq	%r11, %r15
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1040:
	movslq	%eax, %rsi
	addq	$1, %rbx
	addl	$1, %eax
	addq	$1, %r14
	movb	$0, (%r12,%rsi)
	cmpq	%rbx, %r15
	jbe	.L1107
.L1042:
	movzbl	(%rbx), %r9d
	testb	%r9b, %r9b
	je	.L1040
	leaq	(%r12,%r14), %rdi
	subl	%r14d, %eax
	movl	%r13d, %ecx
	addq	$1, %rbx
	movq	-520(%rbp), %rdx
	movl	%eax, %esi
	movq	%rdi, %r8
	call	_ZL13decUnitAddSubPKhiS0_iiPhi.constprop.0
	addl	%r14d, %eax
	addq	$1, %r14
	cmpq	%rbx, %r15
	ja	.L1042
.L1107:
	movq	-536(%rbp), %r13
	movq	%r12, %r10
	movslq	%eax, %rdx
	movq	-528(%rbp), %r12
	movq	$0, -536(%rbp)
	subq	$1, %rdx
	movq	$0, -528(%rbp)
.L1031:
	movzbl	-489(%rbp), %ebx
	movb	%bl, 8(%r12)
	addq	%r10, %rdx
	jc	.L1043
.L1044:
	cmpb	$0, (%rdx)
	jne	.L1043
	cmpl	$1, %eax
	je	.L1043
	subq	$1, %rdx
	subl	$1, %eax
	cmpq	%rdx, %r10
	jbe	.L1044
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	-488(%rbp), %rbx
	movl	%eax, (%r12)
	movl	4(%r13), %ecx
	movl	4(%rbx), %edx
	leal	(%rcx,%rdx), %esi
	shrl	$31, %ecx
	shrl	$31, %edx
	testb	%dl, %cl
	je	.L1045
	testl	%esi, %esi
	movl	$-1999999998, %edx
	cmovg	%edx, %esi
.L1045:
	movq	-512(%rbp), %rbx
	movl	%eax, %ecx
	movq	%r10, %rdx
	movq	%r12, %rdi
	movl	%esi, 4(%r12)
	movq	-504(%rbp), %r9
	leaq	-468(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r13, %r8
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r9, %rcx
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
.L1000:
	movq	-544(%rbp), %rax
	testq	%rax, %rax
	je	.L1047
	movq	%rax, %rdi
	call	uprv_free_67@PLT
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1102:
	movl	%edx, %eax
	movl	%ecx, %edx
	movslq	%eax, %rcx
	movq	%r13, %rax
	movq	%rsi, %r13
	movq	%rax, -488(%rbp)
	cmpl	$2, %edx
	jle	.L994
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1106:
	movzbl	(%r14,%rcx), %edi
	addl	%edi, %eax
	cmpl	$145, %eax
	jg	.L1053
	movb	$0, -208(%rbp)
	leaq	-208(%rbp), %r10
	movq	$0, -544(%rbp)
.L1035:
	movzbl	(%r14,%rcx), %ecx
.L1036:
	movq	-488(%rbp), %rax
	movq	%rsi, %r11
	leaq	9(%rax), %rbx
	cmpl	$49, %edx
	jg	.L1038
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	-512(%rbp), %rcx
	movq	%r13, %rdx
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L1103:
	movslq	%eax, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	movq	%rax, -552(%rbp)
	movq	%rax, -536(%rbp)
	sete	%r14b
	jmp	.L995
.L1100:
	andl	$112, %edx
	jne	.L990
.L991:
	movq	-504(%rbp), %rax
	orl	$128, (%rax)
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L1064:
	movq	$0, -544(%rbp)
	leaq	-368(%rbp), %r10
.L998:
	movslq	%ebx, %rax
	movl	0(%r13), %r8d
	leaq	9(%r13), %rcx
	movq	%rax, -560(%rbp)
	leaq	(%r10,%rax,8), %rax
	movq	%rax, -600(%rbp)
	movq	-552(%rbp), %rax
	leaq	-4(%rax), %rbx
	testl	%r8d, %r8d
	jle	.L1002
	movl	$0, (%rax)
	movq	%rax, %rbx
	xorl	%edi, %edi
	movl	$1, %eax
	xorl	%esi, %esi
	addl	%ecx, %r8d
	leaq	_ZL9DECPOWERS(%rip), %r9
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1109:
	movslq	%esi, %rax
	movl	(%r9,%rax,4), %eax
.L1051:
	movzbl	(%rcx), %edx
	addl	$1, %esi
	addq	$1, %rcx
	imull	%edx, %eax
	addl	%eax, %edi
	movl	%r8d, %eax
	movl	%edi, (%rbx)
	subl	%ecx, %eax
	cmpl	$8, %esi
	jg	.L1003
	testl	%eax, %eax
	jg	.L1109
.L1003:
	leaq	4(%rbx), %rdx
	testl	%eax, %eax
	jle	.L1002
	movl	$0, 4(%rbx)
	movl	$1, %eax
	xorl	%edi, %edi
	xorl	%esi, %esi
	movq	%rdx, %rbx
	jmp	.L1051
.L1002:
	movq	-488(%rbp), %rax
	movl	(%rax), %r8d
	leaq	9(%rax), %rcx
	movq	-520(%rbp), %rax
	leaq	-4(%rax), %r14
	testl	%r8d, %r8d
	jle	.L1007
	movl	$0, (%rax)
	movq	%rax, %r14
	xorl	%edi, %edi
	movl	$1, %eax
	xorl	%esi, %esi
	addl	%ecx, %r8d
	leaq	_ZL9DECPOWERS(%rip), %r9
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1110:
	movslq	%esi, %rax
	movl	(%r9,%rax,4), %eax
.L1050:
	movzbl	(%rcx), %edx
	addl	$1, %esi
	addq	$1, %rcx
	imull	%edx, %eax
	addl	%eax, %edi
	movl	%r8d, %eax
	movl	%edi, (%r14)
	subl	%ecx, %eax
	cmpl	$8, %esi
	jg	.L1008
	testl	%eax, %eax
	jg	.L1110
.L1008:
	leaq	4(%r14), %rdx
	testl	%eax, %eax
	jle	.L1007
	movl	$0, 4(%r14)
	movl	$1, %eax
	movq	%rdx, %r14
	xorl	%edi, %edi
	xorl	%esi, %esi
	jmp	.L1050
.L1007:
	movq	-600(%rbp), %rdi
	movslq	%r15d, %r15
	leaq	0(,%r15,8), %rax
	movq	%rdi, %r15
	movq	%rax, -624(%rbp)
	addq	%rax, %r15
	cmpq	%r15, %rdi
	jnb	.L1011
	leaq	-1(%rax), %rcx
	xorl	%esi, %esi
	movq	%rax, %rdx
	movq	%r10, -576(%rbp)
	movq	%rcx, -568(%rbp)
	call	memset@PLT
	cmpq	%r14, -520(%rbp)
	movq	-576(%rbp), %r10
	ja	.L1012
.L1013:
	movq	-552(%rbp), %r9
	movq	%rbx, %rax
	movq	%r13, -616(%rbp)
	movl	$18, %esi
	movq	%r12, -608(%rbp)
	movq	-600(%rbp), %r12
	movabsq	$19342813113834067, %r11
	subq	%r9, %rax
	movq	%rax, -552(%rbp)
	shrq	$2, %rax
	addq	$1, %rax
	movq	%rax, %r8
	movq	%rax, -584(%rbp)
	andq	$-4, %rax
	leaq	(%r9,%rax,4), %rdi
	movq	%rax, -576(%rbp)
	salq	$3, %rax
	shrq	$2, %r8
	movq	%rax, -568(%rbp)
	salq	$4, %r8
	movq	%rdi, -592(%rbp)
	movq	-520(%rbp), %rdi
	addq	%r9, %r8
	.p2align 4,,10
	.p2align 3
.L1029:
	movq	%rdi, %rdx
	subq	-520(%rbp), %rdx
	sarq	$2, %rdx
	leaq	(%r12,%rdx,8), %rcx
	cmpq	%rbx, %r9
	ja	.L1022
	cmpq	$15, -552(%rbp)
	movl	(%rdi), %r13d
	jbe	.L1060
	movd	%r13d, %xmm6
	addq	-560(%rbp), %rdx
	pshufd	$0, %xmm6, %xmm2
	leaq	(%r10,%rdx,8), %rax
	movq	%r9, %rdx
	movdqa	%xmm2, %xmm3
	punpckhdq	%xmm2, %xmm3
	punpckldq	%xmm2, %xmm2
	.p2align 4,,10
	.p2align 3
.L1021:
	movdqu	(%rdx), %xmm0
	movdqu	(%rax), %xmm5
	addq	$16, %rdx
	addq	$32, %rax
	movdqu	-16(%rax), %xmm4
	movdqa	%xmm0, %xmm1
	punpckhdq	%xmm0, %xmm1
	punpckldq	%xmm0, %xmm0
	pmuludq	%xmm3, %xmm1
	pmuludq	%xmm2, %xmm0
	paddq	%xmm4, %xmm1
	paddq	%xmm5, %xmm0
	movups	%xmm0, -32(%rax)
	movups	%xmm1, -16(%rax)
	cmpq	%r8, %rdx
	jne	.L1021
	movq	-584(%rbp), %rdx
	addq	-568(%rbp), %rcx
	cmpq	%rdx, -576(%rbp)
	je	.L1022
	movq	-592(%rbp), %rax
.L1019:
	movl	(%rax), %edx
	imulq	%r13, %rdx
	addq	%rdx, (%rcx)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rbx
	jb	.L1022
	movl	4(%rax), %edx
	imulq	%r13, %rdx
	addq	%rdx, 8(%rcx)
	leaq	8(%rax), %rdx
	cmpq	%rdx, %rbx
	jb	.L1022
	movl	8(%rax), %edx
	imulq	%r13, %rdx
	addq	%rdx, 16(%rcx)
	leaq	12(%rax), %rdx
	cmpq	%rdx, %rbx
	jb	.L1022
	movl	12(%rax), %eax
	imulq	%rax, %r13
	addq	%r13, 24(%rcx)
.L1022:
	subl	$1, %esi
	testl	%esi, %esi
	jle	.L1018
	cmpq	%r14, %rdi
	je	.L1018
.L1016:
	addq	$4, %rdi
	cmpq	%r14, %rdi
	jbe	.L1029
	movq	-608(%rbp), %r12
	movq	-616(%rbp), %r13
	cmpq	%r15, -600(%rbp)
	jnb	.L1065
	movq	-624(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -568(%rbp)
.L1012:
	movq	-600(%rbp), %r8
	movq	%r10, %rcx
	movl	$3435973837, %r11d
	movl	$3518437209, %r9d
	.p2align 4,,10
	.p2align 3
.L1030:
	movq	(%r8), %rax
	movl	%eax, %edx
	movl	%eax, %ebx
	shrl	$5, %eax
	movq	%rdx, %rdi
	imulq	$175921861, %rax, %rax
	imulq	%r11, %rdi
	shrq	$39, %rax
	shrq	$35, %rdi
	leal	(%rdi,%rdi,4), %esi
	addl	%esi, %esi
	subl	%esi, %ebx
	imulq	$1374389535, %rdx, %rsi
	movb	%bl, (%rcx)
	shrq	$37, %rsi
	leal	(%rsi,%rsi,4), %ebx
	addl	%ebx, %ebx
	subl	%ebx, %edi
	movb	%dil, 1(%rcx)
	imulq	$274877907, %rdx, %rdi
	shrq	$38, %rdi
	leal	(%rdi,%rdi,4), %ebx
	addl	%ebx, %ebx
	subl	%ebx, %esi
	movb	%sil, 2(%rcx)
	movq	%rdx, %rsi
	imulq	%r9, %rsi
	shrq	$45, %rsi
	leal	(%rsi,%rsi,4), %ebx
	addl	%ebx, %ebx
	subl	%ebx, %edi
	movb	%dil, 3(%rcx)
	leal	(%rax,%rax,4), %edi
	addl	%edi, %edi
	subl	%edi, %esi
	imulq	$1125899907, %rdx, %rdi
	movb	%sil, 4(%rcx)
	shrq	$50, %rdi
	leal	(%rdi,%rdi,4), %esi
	movl	%edi, %ebx
	addl	%esi, %esi
	subl	%esi, %eax
	imulq	$1801439851, %rdx, %rsi
	movb	%al, 5(%rcx)
	shrq	$54, %rsi
	leal	(%rsi,%rsi,4), %eax
	addl	%eax, %eax
	subl	%eax, %ebx
	imulq	$1441151881, %rdx, %rax
	movb	%bl, 6(%rcx)
	movl	%esi, %ebx
	shrq	$57, %rax
	leal	(%rax,%rax,4), %edx
	movb	%al, 8(%rcx)
	addl	%edx, %edx
	subl	%edx, %ebx
	addq	$8, %r8
	addq	$9, %rcx
	movb	%bl, -2(%rcx)
	cmpq	%r15, %r8
	jb	.L1030
	movq	-568(%rbp), %rax
	shrq	$3, %rax
	leaq	9(%rax,%rax,8), %rdx
	movq	-600(%rbp), %rax
	addq	$1, %rax
	cmpq	%rax, %r15
	movl	$9, %eax
	cmovb	%rax, %rdx
	movl	%edx, %eax
	movslq	%edx, %rdx
	subq	$1, %rdx
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	%r12, %rcx
	cmpq	%r15, %r12
	jnb	.L1027
	.p2align 4,,10
	.p2align 3
.L1024:
	movq	(%rcx), %rsi
	cmpq	$999999999, %rsi
	jbe	.L1025
	movq	%rsi, %rdx
	shrq	$9, %rdx
	movq	%rdx, %rax
	mulq	%r11
	shrq	$11, %rdx
	movq	%rdx, %r13
	movl	%edx, %eax
	movabsq	$999999999999999999, %rdx
	cmpq	%rdx, %rsi
	jbe	.L1026
	movabsq	$81129638414607, %rax
	movq	%rsi, %rdx
	shrq	$18, %rdx
	mulq	%rdx
	movabsq	$1000000000000000000, %rax
	shrq	$24, %rdx
	addq	%rdx, 16(%rcx)
	imulq	%rdx, %rax
	imull	$1000000000, %edx, %edx
	subq	%rax, %rsi
	movl	%r13d, %eax
	subl	%edx, %eax
.L1026:
	addq	%rax, 8(%rcx)
	imulq	$1000000000, %rax, %rax
	subq	%rax, %rsi
	movq	%rsi, (%rcx)
.L1025:
	addq	$8, %rcx
	cmpq	%r15, %rcx
	jb	.L1024
.L1027:
	movl	$18, %esi
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1053:
	movslq	%eax, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L1034
	movq	-488(%rbp), %rax
	movslq	0(%r13), %rcx
	movb	$0, (%r10)
	movq	%r10, -544(%rbp)
	movl	(%rax), %edx
	movslq	%edx, %rsi
	cmpl	$49, %ecx
	jle	.L1035
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1105:
	movslq	%eax, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -544(%rbp)
	testq	%rax, %rax
	je	.L1066
	testb	%r14b, %r14b
	jne	.L1066
	movq	%rax, %r10
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1066:
	movq	-504(%rbp), %rax
	orl	$16, (%rax)
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1104:
	movl	%eax, %edi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	movq	%rax, -520(%rbp)
	sete	%al
	movq	%rbx, -528(%rbp)
	orl	%eax, %r14d
	jmp	.L996
.L1060:
	movq	%r9, %rax
	jmp	.L1019
.L1039:
	movzbl	-489(%rbp), %eax
	movq	%r10, %rdx
	movq	$0, -528(%rbp)
	movq	$0, -536(%rbp)
	movb	%al, 8(%r12)
	movl	$1, %eax
	jmp	.L1044
.L1011:
	cmpq	%r14, -520(%rbp)
	jbe	.L1013
.L1065:
	movq	$-1, %rdx
	xorl	%eax, %eax
	jmp	.L1031
.L1101:
	call	__stack_chk_fail@PLT
.L1034:
	movq	-504(%rbp), %rax
	orl	$16, (%rax)
	jmp	.L989
	.cfi_endproc
.LFE2119:
	.size	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj, .-_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	.p2align 4
	.type	_ZL13decQuantizeOpP9decNumberPKS_S2_P10decContexthPj.constprop.0, @function
_ZL13decQuantizeOpP9decNumberPKS_S2_P10decContexthPj.constprop.0:
.LFB2616:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movl	(%rcx), %r15d
	movzbl	8(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rdx), %eax
	movl	$0, -100(%rbp)
	movl	%ecx, %edx
	orl	%eax, %edx
	testb	$112, %dl
	je	.L1112
	andl	$48, %edx
	jne	.L1188
	xorl	%ecx, %eax
	testb	$64, %al
	je	.L1115
.L1187:
	orl	$128, (%rbx)
.L1114:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1189
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1112:
	.cfi_restore_state
	testb	%r8b, %r8b
	je	.L1126
	movl	4(%rdi), %eax
.L1127:
	cmpl	$-2147483648, %eax
	je	.L1187
	movl	8(%r14), %esi
	leal	-1(%r15), %edx
	subl	%edx, %esi
	cmpl	%eax, %esi
	jg	.L1187
	leal	2147483646(%rax), %edx
	cmpl	$1, %edx
	jbe	.L1187
	cmpl	4(%r14), %eax
	jg	.L1187
	movl	(%r12), %edx
	movzbl	9(%r12), %esi
	cmpl	$1, %edx
	je	.L1190
.L1129:
	movl	4(%r12), %esi
	movl	%eax, %r8d
	subl	%esi, %r8d
	subl	%r8d, %edx
	cmpl	%edx, %r15d
	jl	.L1187
	testl	%r8d, %r8d
	jle	.L1140
	movq	16(%r14), %rsi
	movdqu	(%r14), %xmm2
	movq	%rbx, %r9
	leaq	-100(%rbp), %r8
	movq	%r13, %rdi
	movl	%eax, -116(%rbp)
	movq	%rsi, -80(%rbp)
	movl	24(%r14), %esi
	movaps	%xmm2, -96(%rbp)
	movb	%cl, 8(%r13)
	movl	%edx, -96(%rbp)
	movl	4(%r12), %edx
	movl	%esi, -72(%rbp)
	leaq	-96(%rbp), %rsi
	movl	%edx, 4(%r13)
	movl	(%r12), %ecx
	leaq	9(%r12), %rdx
	movq	%rsi, -128(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	-100(%rbp), %edx
	movl	-116(%rbp), %eax
	testl	%edx, %edx
	je	.L1141
	movq	-128(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%r13, %rdi
	call	_ZL13decApplyRoundP9decNumberP10decContextiPj.part.0
	movl	-116(%rbp), %eax
.L1141:
	movl	4(%r13), %r8d
	movl	0(%r13), %r9d
	movl	$0, -100(%rbp)
	cmpl	%r8d, %eax
	jge	.L1139
	cmpl	%r9d, %r15d
	jne	.L1143
	movl	(%rbx), %eax
	andl	$-2081, %eax
	orb	$-128, %al
	movl	%eax, (%rbx)
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1126:
	movb	%cl, -116(%rbp)
	call	_ZL9decGetIntPK9decNumber
	movzbl	-116(%rbp), %ecx
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	%rdi, %rdx
	movq	%r9, %r8
	movq	%r14, %rcx
	movq	%r13, %rdi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1115:
	cmpq	%r13, %rsi
	je	.L1114
	movb	%cl, 8(%r13)
	movl	4(%rsi), %eax
	movl	%eax, 4(%r13)
	movl	(%rsi), %eax
	movl	%eax, 0(%r13)
	movzbl	9(%rsi), %eax
	movb	%al, 9(%r13)
	movl	(%rsi), %eax
	cmpl	$1, %eax
	jle	.L1114
	leaq	10(%r13), %rdi
	leaq	9(%rsi), %rcx
	movslq	%eax, %rdx
	cmpl	$49, %eax
	jg	.L1119
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdx), %edx
.L1119:
	addq	%rdx, %rcx
	leaq	10(%r12), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1114
	movq	%rcx, %rax
	subq	%r12, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L1120
	leaq	25(%r12), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L1120
	movq	%rcx, %rax
	subq	%r12, %rax
	leaq	-10(%rax), %r8
	movl	$10, %eax
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	$10, %rsi
	.p2align 4,,10
	.p2align 3
.L1122:
	movdqu	(%r12,%rax), %xmm0
	movups	%xmm0, 0(%r13,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L1122
	movq	%r8, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%rdi,%rsi), %rdx
	cmpq	%r8, %rsi
	je	.L1114
	movzbl	(%rax), %esi
	movb	%sil, (%rdx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1114
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1114
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1114
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1114
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1114
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1114
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1114
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1114
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1114
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1114
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1114
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1114
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1114
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1114
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1190:
	movl	%ecx, %edi
	andl	$112, %edi
	orb	%sil, %dil
	jne	.L1129
	cmpq	%r13, %r12
	je	.L1130
	movb	%cl, 8(%r13)
	movl	4(%r12), %edx
	movl	%edx, 4(%r13)
	movl	(%r12), %r9d
	movl	%r9d, 0(%r13)
	movzbl	9(%r12), %edx
	movb	%dl, 9(%r13)
	movl	(%r12), %edx
	cmpl	$1, %edx
	jle	.L1131
	leaq	10(%r13), %rdi
	leaq	9(%r12), %rsi
	movslq	%edx, %rcx
	cmpl	$49, %edx
	jg	.L1133
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rcx), %ecx
.L1133:
	addq	%rcx, %rsi
	leaq	10(%r12), %rcx
	cmpq	%rcx, %rsi
	jbe	.L1131
	movq	%rsi, %rdx
	subq	%r12, %rdx
	subq	$11, %rdx
	cmpq	$14, %rdx
	jbe	.L1134
	leaq	25(%r12), %rdx
	subq	%rdi, %rdx
	cmpq	$30, %rdx
	jbe	.L1134
	movq	%rsi, %rdx
	subq	%r12, %rdx
	leaq	-10(%rdx), %r9
	movl	$10, %edx
	movq	%r9, %r8
	andq	$-16, %r8
	addq	$10, %r8
.L1135:
	movdqu	(%r12,%rdx), %xmm3
	movups	%xmm3, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L1135
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%rdi,%r8), %rcx
	cmpq	%r8, %r9
	je	.L1137
	movzbl	(%rdx), %edi
	movb	%dil, (%rcx)
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L1137
	movzbl	1(%rdx), %edi
	movb	%dil, 1(%rcx)
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L1137
	movzbl	2(%rdx), %edi
	movb	%dil, 2(%rcx)
	leaq	3(%rdx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L1137
	movzbl	3(%rdx), %edi
	movb	%dil, 3(%rcx)
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L1137
	movzbl	4(%rdx), %edi
	movb	%dil, 4(%rcx)
	leaq	5(%rdx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L1137
	movzbl	5(%rdx), %edi
	movb	%dil, 5(%rcx)
	leaq	6(%rdx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L1137
	movzbl	6(%rdx), %edi
	movb	%dil, 6(%rcx)
	leaq	7(%rdx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L1137
	movzbl	7(%rdx), %edi
	movb	%dil, 7(%rcx)
	leaq	8(%rdx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L1137
	movzbl	8(%rdx), %edi
	movb	%dil, 8(%rcx)
	leaq	9(%rdx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L1137
	movzbl	9(%rdx), %edi
	movb	%dil, 9(%rcx)
	leaq	10(%rdx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L1137
	movzbl	10(%rdx), %edi
	movb	%dil, 10(%rcx)
	leaq	11(%rdx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L1137
	movzbl	11(%rdx), %edi
	movb	%dil, 11(%rcx)
	leaq	12(%rdx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L1137
	movzbl	12(%rdx), %edi
	movb	%dil, 12(%rcx)
	leaq	13(%rdx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L1137
	movzbl	13(%rdx), %edi
	movb	%dil, 13(%rcx)
	leaq	14(%rdx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L1137
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rcx)
.L1137:
	movl	0(%r13), %r9d
.L1131:
	movl	%eax, 4(%r13)
	movl	%eax, %r8d
	.p2align 4,,10
	.p2align 3
.L1139:
	movl	4(%r14), %eax
	subl	%r9d, %eax
	addl	$1, %eax
	cmpl	%r8d, %eax
	jl	.L1187
	leaq	-100(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	andl	$-8193, (%rbx)
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1140:
	cmpq	%r13, %r12
	je	.L1144
	movb	%cl, 8(%r13)
	movl	4(%r12), %edx
	movl	%edx, 4(%r13)
	movl	(%r12), %r9d
	movl	%r9d, 0(%r13)
	movzbl	9(%r12), %edx
	movb	%dl, 9(%r13)
	movl	(%r12), %ecx
	cmpl	$1, %ecx
	jle	.L1145
	leaq	10(%r13), %r10
	leaq	9(%r12), %rdi
	movslq	%ecx, %rdx
	cmpl	$49, %ecx
	jg	.L1147
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
.L1147:
	addq	%rdx, %rdi
	leaq	10(%r12), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1145
	movq	%rdi, %rdx
	subq	%r12, %rdx
	subq	$11, %rdx
	cmpq	$14, %rdx
	jbe	.L1148
	leaq	25(%r12), %rdx
	subq	%r10, %rdx
	cmpq	$30, %rdx
	jbe	.L1148
	movq	%rdi, %rdx
	subq	%r12, %rdx
	leaq	-10(%rdx), %r11
	movl	$10, %edx
	movq	%r11, %r9
	andq	$-16, %r9
	addq	$10, %r9
	.p2align 4,,10
	.p2align 3
.L1149:
	movdqu	(%r12,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%r9, %rdx
	jne	.L1149
	movq	%r11, %rdx
	andq	$-16, %rdx
	addq	%rdx, %rcx
	addq	%rdx, %r10
	cmpq	%rdx, %r11
	je	.L1144
	movzbl	(%rcx), %edx
	movb	%dl, (%r10)
	leaq	1(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1144
	movzbl	1(%rcx), %edx
	movb	%dl, 1(%r10)
	leaq	2(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1144
	movzbl	2(%rcx), %edx
	movb	%dl, 2(%r10)
	leaq	3(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1144
	movzbl	3(%rcx), %edx
	movb	%dl, 3(%r10)
	leaq	4(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1144
	movzbl	4(%rcx), %edx
	movb	%dl, 4(%r10)
	leaq	5(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1144
	movzbl	5(%rcx), %edx
	movb	%dl, 5(%r10)
	leaq	6(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1144
	movzbl	6(%rcx), %edx
	movb	%dl, 6(%r10)
	leaq	7(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1144
	movzbl	7(%rcx), %edx
	movb	%dl, 7(%r10)
	leaq	8(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1144
	movzbl	8(%rcx), %edx
	movb	%dl, 8(%r10)
	leaq	9(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1144
	movzbl	9(%rcx), %edx
	movb	%dl, 9(%r10)
	leaq	10(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1144
	movzbl	10(%rcx), %edx
	movb	%dl, 10(%r10)
	leaq	11(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1144
	movzbl	11(%rcx), %edx
	movb	%dl, 11(%r10)
	leaq	12(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1144
	movzbl	12(%rcx), %edx
	movb	%dl, 12(%r10)
	leaq	13(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1144
	movzbl	13(%rcx), %edx
	movb	%dl, 13(%r10)
	leaq	14(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1144
	movzbl	14(%rcx), %edx
	movb	%dl, 14(%r10)
.L1144:
	movl	0(%r13), %r9d
.L1145:
	testl	%r8d, %r8d
	jne	.L1153
	movl	4(%r13), %r8d
	jmp	.L1139
.L1120:
	subq	%r12, %rcx
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L1125:
	movzbl	(%r12,%rax), %edx
	movb	%dl, 0(%r13,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L1125
	jmp	.L1114
.L1153:
	subl	%eax, %esi
	movl	%esi, %edx
	je	.L1154
	movl	%r9d, %esi
	leaq	9(%r13), %rdi
	movl	%r8d, -116(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	movl	-116(%rbp), %r8d
	movl	%eax, %r9d
.L1154:
	addl	4(%r13), %r8d
	movl	%r9d, 0(%r13)
	movl	%r8d, 4(%r13)
	jmp	.L1139
.L1143:
	movl	%r9d, %esi
	leaq	9(%r13), %rdi
	movl	$1, %edx
	call	_ZL14decShiftToMostPhii.part.0
	movl	%eax, 0(%r13)
	movl	%eax, %r9d
	movl	4(%r13), %eax
	leal	-1(%rax), %r8d
	movl	%r8d, 4(%r13)
	jmp	.L1139
.L1148:
	subq	%r12, %rdi
	movl	$10, %ecx
	movq	%rdi, %rdx
.L1152:
	movzbl	(%r12,%rcx), %edi
	movb	%dil, 0(%r13,%rcx)
	addq	$1, %rcx
	cmpq	%rdx, %rcx
	jne	.L1152
	jmp	.L1144
.L1130:
	movl	$1, %r9d
	jmp	.L1131
.L1189:
	call	__stack_chk_fail@PLT
.L1134:
	subq	%r12, %rsi
	movl	$10, %edx
.L1138:
	movzbl	(%r12,%rdx), %ecx
	movb	%cl, 0(%r13,%rdx)
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	jne	.L1138
	jmp	.L1137
	.cfi_endproc
.LFE2616:
	.size	_ZL13decQuantizeOpP9decNumberPKS_S2_P10decContexthPj.constprop.0, .-_ZL13decQuantizeOpP9decNumberPKS_S2_P10decContexthPj.constprop.0
	.p2align 4
	.globl	uprv_decNumberFromInt32_67
	.type	uprv_decNumberFromInt32_67, @function
uprv_decNumberFromInt32_67:
.LFB2054:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	testl	%esi, %esi
	js	.L1192
	movl	$0, %r8d
	movq	$1, (%rax)
	movl	%esi, %edi
	movw	%r8w, 8(%rax)
	jne	.L1193
.L1194:
	ret
	.p2align 4,,10
	.p2align 3
.L1192:
	cmpl	$-2147483648, %esi
	je	.L1205
	xorl	%edx, %edx
	movq	$1, (%rax)
	movl	%esi, %edi
	movw	%dx, 8(%rax)
	negl	%edi
.L1193:
	leaq	9(%rax), %r9
	movl	$3435973837, %r10d
	movq	%r9, %rcx
	.p2align 4,,10
	.p2align 3
.L1196:
	movl	%edi, %edx
	movl	%edi, %r11d
	addq	$1, %rcx
	imulq	%r10, %rdx
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %r8d
	addl	%r8d, %r8d
	subl	%r8d, %r11d
	movl	%edi, %r8d
	movl	%edx, %edi
	movb	%r11b, -1(%rcx)
	cmpl	$9, %r8d
	ja	.L1196
	subq	%r9, %rcx
	movl	%ecx, %edi
	movslq	%ecx, %rcx
	leaq	-1(%r9,%rcx), %rdx
	cmpq	%rdx, %r9
	ja	.L1197
	leaq	8(%rax), %rcx
.L1198:
	cmpb	$0, (%rdx)
	jne	.L1197
	cmpl	$1, %edi
	je	.L1197
	subq	$1, %rdx
	subl	$1, %edi
	cmpq	%rcx, %rdx
	jne	.L1198
	.p2align 4,,10
	.p2align 3
.L1197:
	movl	%edi, (%rax)
	testl	%esi, %esi
	jns	.L1194
	movb	$-128, 8(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L1205:
	xorl	%ecx, %ecx
	movq	$1, (%rdi)
	movw	%cx, 8(%rdi)
	movl	$-2147483648, %edi
	jmp	.L1193
	.cfi_endproc
.LFE2054:
	.size	uprv_decNumberFromInt32_67, .-uprv_decNumberFromInt32_67
	.p2align 4
	.globl	uprv_decNumberFromUInt32_67
	.type	uprv_decNumberFromUInt32_67, @function
uprv_decNumberFromUInt32_67:
.LFB2055:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	movq	$1, (%rdi)
	movq	%rdi, %rax
	movw	%dx, 8(%rdi)
	testl	%esi, %esi
	je	.L1207
	leaq	9(%rdi), %r9
	movl	$3435973837, %edi
	movq	%r9, %r8
	.p2align 4,,10
	.p2align 3
.L1208:
	movl	%esi, %edx
	movl	%esi, %r10d
	addq	$1, %r8
	imulq	%rdi, %rdx
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r10d
	movl	%esi, %ecx
	movl	%edx, %esi
	movb	%r10b, -1(%r8)
	cmpl	$9, %ecx
	ja	.L1208
	subq	%r9, %r8
	movl	%r8d, %ecx
	movslq	%r8d, %r8
	leaq	-1(%r9,%r8), %rdx
	cmpq	%rdx, %r9
	ja	.L1209
	leaq	8(%rax), %rsi
.L1210:
	cmpb	$0, (%rdx)
	jne	.L1209
	cmpl	$1, %ecx
	je	.L1209
	subq	$1, %rdx
	subl	$1, %ecx
	cmpq	%rsi, %rdx
	jne	.L1210
	.p2align 4,,10
	.p2align 3
.L1209:
	movl	%ecx, (%rax)
.L1207:
	ret
	.cfi_endproc
.LFE2055:
	.size	uprv_decNumberFromUInt32_67, .-uprv_decNumberFromUInt32_67
	.p2align 4
	.globl	uprv_decNumberToInt32_67
	.type	uprv_decNumberToInt32_67, @function
uprv_decNumberToInt32_67:
.LFB2056:
	.cfi_startproc
	endbr64
	movzbl	8(%rdi), %edx
	movq	%rsi, %r8
	testb	$112, %dl
	jne	.L1220
	movl	(%rdi), %eax
	cmpl	$10, %eax
	jle	.L1260
.L1220:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$128, %esi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uprv_decContextSetStatus_67@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1260:
	.cfi_restore 6
	movl	4(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L1220
	movzbl	9(%rdi), %r9d
	cmpl	$1, %eax
	jle	.L1227
	movzbl	10(%rdi), %ecx
	cmpl	$2, %eax
	je	.L1229
	movzbl	11(%rdi), %esi
	leal	(%rsi,%rsi,4), %esi
	leal	(%rcx,%rsi,2), %ecx
	cmpl	$3, %eax
	je	.L1230
	movzbl	12(%rdi), %esi
	imull	$100, %esi, %esi
	addl	%esi, %ecx
	cmpl	$4, %eax
	je	.L1230
	movzbl	13(%rdi), %esi
	imull	$1000, %esi, %esi
	addl	%esi, %ecx
	cmpl	$5, %eax
	je	.L1230
	movzbl	14(%rdi), %esi
	imull	$10000, %esi, %esi
	addl	%esi, %ecx
	cmpl	$6, %eax
	je	.L1230
	movzbl	15(%rdi), %esi
	imull	$100000, %esi, %esi
	addl	%esi, %ecx
	cmpl	$7, %eax
	je	.L1230
	movzbl	16(%rdi), %esi
	imull	$1000000, %esi, %esi
	addl	%esi, %ecx
	cmpl	$8, %eax
	je	.L1224
	movzbl	17(%rdi), %esi
	imull	$10000000, %esi, %esi
	addl	%esi, %ecx
	cmpl	$9, %eax
	je	.L1224
	movzbl	18(%rdi), %eax
	imull	$100000000, %eax, %eax
	addl	%eax, %ecx
.L1224:
	cmpl	$214748364, %ecx
	ja	.L1220
	.p2align 4,,10
	.p2align 3
.L1230:
	cmpl	$214748364, %ecx
	jne	.L1229
	cmpl	$7, %r9d
	jbe	.L1229
	testb	%dl, %dl
	jns	.L1220
	cmpl	$8, %r9d
	jne	.L1220
	movl	$-2147483648, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1229:
	leal	0(,%rcx,8), %eax
	leal	(%rax,%rcx,2), %eax
	addl	%eax, %r9d
.L1227:
	movl	%r9d, %eax
	negl	%eax
	testb	%dl, %dl
	cmovns	%r9d, %eax
	ret
	.cfi_endproc
.LFE2056:
	.size	uprv_decNumberToInt32_67, .-uprv_decNumberToInt32_67
	.p2align 4
	.globl	uprv_decNumberToUInt32_67
	.type	uprv_decNumberToUInt32_67, @function
uprv_decNumberToUInt32_67:
.LFB2057:
	.cfi_startproc
	endbr64
	movzbl	8(%rdi), %edx
	movq	%rsi, %r8
	testb	$112, %dl
	jne	.L1262
	movl	(%rdi), %ecx
	cmpl	$10, %ecx
	jle	.L1302
.L1262:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$128, %esi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uprv_decContextSetStatus_67@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1302:
	.cfi_restore 6
	movl	4(%rdi), %eax
	testl	%eax, %eax
	jne	.L1262
	movzbl	9(%rdi), %eax
	testb	%dl, %dl
	js	.L1303
	cmpl	$1, %ecx
	jle	.L1300
	movzbl	10(%rdi), %esi
	cmpl	$2, %ecx
	je	.L1265
	movzbl	11(%rdi), %edx
	leal	(%rdx,%rdx,4), %edx
	leal	(%rsi,%rdx,2), %esi
	cmpl	$3, %ecx
	je	.L1271
	movzbl	12(%rdi), %edx
	imull	$100, %edx, %edx
	addl	%edx, %esi
	cmpl	$4, %ecx
	je	.L1271
	movzbl	13(%rdi), %edx
	imull	$1000, %edx, %edx
	addl	%edx, %esi
	cmpl	$5, %ecx
	je	.L1271
	movzbl	14(%rdi), %edx
	imull	$10000, %edx, %edx
	addl	%edx, %esi
	cmpl	$6, %ecx
	je	.L1271
	movzbl	15(%rdi), %edx
	imull	$100000, %edx, %edx
	addl	%edx, %esi
	cmpl	$7, %ecx
	je	.L1271
	movzbl	16(%rdi), %edx
	imull	$1000000, %edx, %edx
	addl	%edx, %esi
	cmpl	$8, %ecx
	je	.L1271
	movzbl	17(%rdi), %edx
	imull	$10000000, %edx, %edx
	addl	%edx, %esi
	cmpl	$9, %ecx
	je	.L1267
	movzbl	18(%rdi), %edx
	imull	$100000000, %edx, %edx
	addl	%edx, %esi
.L1267:
	cmpl	$429496729, %esi
	ja	.L1262
	.p2align 4,,10
	.p2align 3
.L1271:
	cmpl	$429496729, %esi
	jne	.L1265
	cmpl	$5, %eax
	ja	.L1262
.L1265:
	leal	0(,%rsi,8), %edx
	leal	(%rdx,%rsi,2), %edx
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1300:
	ret
	.p2align 4,,10
	.p2align 3
.L1303:
	cmpl	$1, %ecx
	jne	.L1262
	testb	%al, %al
	jne	.L1262
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2057:
	.size	uprv_decNumberToUInt32_67, .-uprv_decNumberToUInt32_67
	.p2align 4
	.globl	uprv_decNumberToString_67
	.type	uprv_decNumberToString_67, @function
uprv_decNumberToString_67:
.LFB2058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	_ZL11decToStringPK9decNumberPch
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2058:
	.size	uprv_decNumberToString_67, .-uprv_decNumberToString_67
	.p2align 4
	.globl	uprv_decNumberToEngString_67
	.type	uprv_decNumberToEngString_67, @function
uprv_decNumberToEngString_67:
.LFB2059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	_ZL11decToStringPK9decNumberPch
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2059:
	.size	uprv_decNumberToEngString_67, .-uprv_decNumberToEngString_67
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"infinity"
.LC3:
	.string	"INFINITY"
.LC4:
	.string	"inf"
.LC5:
	.string	"INF"
	.text
	.p2align 4
	.globl	uprv_decNumberFromString_67
	.type	uprv_decNumberFromString_67, @function
uprv_decNumberFromString_67:
.LFB2060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movq	%rsi, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -116(%rbp)
.L1314:
	movzbl	(%rdx), %eax
	leal	-48(%rax), %edi
	cmpb	$9, %dil
	jbe	.L1431
	cmpb	$46, %al
	jne	.L1311
	testq	%r9, %r9
	je	.L1432
.L1311:
	cmpq	%rsi, %rdx
	jne	.L1312
	cmpb	$45, %al
	je	.L1433
	cmpb	$43, %al
	je	.L1430
.L1312:
	testq	%rbx, %rbx
	je	.L1434
	testb	%al, %al
	jne	.L1435
	xorl	%r8d, %r8d
.L1328:
	cmpb	$48, (%r14)
	je	.L1436
.L1342:
	testq	%r9, %r9
	je	.L1345
	cmpq	%rbx, %r9
	movq	%rbx, %rax
	movl	%r8d, %edi
	setb	%dl
	subq	%r9, %rax
	subl	%eax, %edi
	testb	%dl, %dl
	cmovne	%edi, %r8d
.L1345:
	cmpl	%r15d, 0(%r13)
	jge	.L1365
	movslq	%r15d, %rdi
	cmpl	$49, %r15d
	jg	.L1348
	movslq	%r15d, %rax
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %edi
.L1348:
	xorl	%r10d, %r10d
	leaq	-112(%rbp), %r11
	cmpl	$45, %edi
	jg	.L1437
.L1347:
	leaq	-1(%r14), %rax
	movq	%r11, %rsi
	cmpq	%rbx, %r14
	ja	.L1354
	.p2align 4,,10
	.p2align 3
.L1355:
	movzbl	(%rbx), %edx
	cmpb	$46, %dl
	je	.L1353
	subl	$48, %edx
	addq	$1, %rsi
	movb	%dl, -1(%rsi)
.L1353:
	subq	$1, %rbx
	cmpq	%rax, %rbx
	jne	.L1355
.L1354:
	movb	%cl, 8(%r12)
	movl	%r8d, 4(%r12)
	movl	%r15d, (%r12)
	movl	0(%r13), %edx
	cmpl	%r15d, %edx
	jl	.L1438
	movl	8(%r13), %eax
	subl	%r15d, %eax
	cmpl	%r8d, %eax
	jge	.L1357
	movl	4(%r13), %eax
	subl	$1, %r8d
	subl	%edx, %eax
	cmpl	%eax, %r8d
	jle	.L1356
.L1357:
	leaq	-116(%rbp), %rcx
	leaq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, -136(%rbp)
	movl	$0, -120(%rbp)
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movq	-136(%rbp), %r10
.L1356:
	testq	%r10, %r10
	je	.L1358
	movq	%r10, %rdi
	call	uprv_free_67@PLT
.L1358:
	movl	-116(%rbp), %esi
	testl	%esi, %esi
	je	.L1363
	movl	%esi, %eax
	andl	$221, %eax
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1431:
	addl	$1, %r15d
	movq	%rdx, %rbx
.L1310:
	addq	$1, %rdx
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1435:
	andl	$-33, %eax
	movl	$1, -116(%rbp)
	cmpb	$69, %al
	jne	.L1316
	movzbl	1(%rdx), %edi
	cmpb	$45, %dil
	je	.L1439
	cmpb	$43, %dil
	je	.L1331
	leaq	1(%rdx), %rsi
	xorl	%r10d, %r10d
.L1330:
	testb	%dil, %dil
	je	.L1316
	cmpb	$48, %dil
	je	.L1337
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1426:
	addq	$1, %rsi
	cmpb	$48, %al
	jne	.L1440
.L1337:
	movsbl	1(%rsi), %eax
	testb	%al, %al
	jne	.L1426
	movl	$48, %eax
.L1364:
	movq	%rsi, %rdx
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L1338:
	leal	-48(%rax,%r8,2), %eax
	addq	$1, %rdx
	leal	(%rax,%r8,8), %r8d
	movsbl	(%rdx), %eax
	leal	-48(%rax), %r11d
	cmpb	$9, %r11b
	jbe	.L1338
.L1335:
	testb	%al, %al
	jne	.L1316
	addq	$10, %rsi
	cmpq	%rdx, %rsi
	ja	.L1339
	jb	.L1375
	cmpb	$49, %dil
	jle	.L1339
.L1375:
	movl	$1999999998, %r8d
.L1339:
	movl	%r8d, %eax
	movl	$0, -116(%rbp)
	negl	%eax
	testb	%r10b, %r10b
	cmovne	%eax, %r8d
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1434:
	movl	$1, -116(%rbp)
	testq	%r9, %r9
	jne	.L1316
	testb	%al, %al
	jne	.L1441
.L1316:
	movl	$1, %esi
.L1360:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
.L1359:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
.L1363:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1442
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1433:
	.cfi_restore_state
	addq	$1, %r14
	movl	$-128, %ecx
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1432:
	movq	%rdx, %r9
	cmpq	%rdx, %r14
	jne	.L1310
	.p2align 4,,10
	.p2align 3
.L1430:
	addq	$1, %r14
	jmp	.L1310
.L1322:
	movl	%ecx, %eax
	orl	$32, %eax
	movb	%al, 8(%r12)
	movzbl	(%rdx), %eax
	andl	$-33, %eax
	cmpb	$83, %al
	jne	.L1362
	orl	$16, %ecx
	addq	$1, %rdx
	movb	%cl, 8(%r12)
	movzbl	(%rdx), %eax
	andl	$-33, %eax
.L1362:
	cmpb	$78, %al
	jne	.L1316
	movzbl	1(%rdx), %eax
	andl	$-33, %eax
	cmpb	$65, %al
	jne	.L1316
	movzbl	2(%rdx), %eax
	andl	$-33, %eax
	cmpb	$78, %al
	jne	.L1316
	movzbl	3(%rdx), %eax
	leaq	3(%rdx), %r14
	cmpb	$48, %al
	jne	.L1324
.L1325:
	movzbl	1(%r14), %eax
	addq	$1, %r14
	cmpb	$48, %al
	je	.L1325
.L1324:
	testb	%al, %al
	je	.L1363
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1316
	movl	%r15d, %esi
	movq	%r14, %rax
	subl	%r14d, %esi
.L1326:
	movq	%rax, %rbx
	leaq	1(%rax), %rax
	movzbl	1(%rbx), %edx
	leal	(%rsi,%rax), %r15d
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	jbe	.L1326
	testb	%dl, %dl
	jne	.L1316
	movl	0(%r13), %eax
	cmpl	%r15d, %eax
	jle	.L1443
.L1327:
	movl	$0, -116(%rbp)
	movzbl	8(%r12), %ecx
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L1365:
	leaq	9(%r12), %r11
	xorl	%r10d, %r10d
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1436:
	cmpq	%rbx, %r14
	jnb	.L1342
	.p2align 4,,10
	.p2align 3
.L1344:
	movzbl	(%r14), %eax
	cmpb	$46, %al
	je	.L1343
	cmpb	$48, %al
	jne	.L1342
	subl	$1, %r15d
.L1343:
	addq	$1, %r14
	cmpq	%r14, %rbx
	ja	.L1344
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1438:
	leaq	-120(%rbp), %r14
	leaq	-116(%rbp), %r9
	movq	%r11, %rdx
	movl	%r15d, %ecx
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, -136(%rbp)
	movl	$0, -120(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, %rcx
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movq	-136(%rbp), %r10
	jmp	.L1356
	.p2align 4,,10
	.p2align 3
.L1439:
	movzbl	2(%rdx), %edi
	leaq	2(%rdx), %rsi
	movl	$1, %r10d
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1437:
	movb	%cl, -137(%rbp)
	movl	%r8d, -136(%rbp)
	call	uprv_malloc_67@PLT
	movl	-136(%rbp), %r8d
	movzbl	-137(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r11
	je	.L1444
	movq	%rax, %r10
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1441:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	$1, (%r12)
	leaq	.LC2(%rip), %rdi
	movw	%si, 8(%r12)
	leaq	.LC3(%rip), %r8
	.p2align 4,,10
	.p2align 3
.L1320:
	movzbl	(%rdx,%rax), %esi
	cmpb	(%rdi,%rax), %sil
	je	.L1317
	cmpb	(%r8,%rax), %sil
	jne	.L1368
.L1317:
	addq	$1, %rax
	testb	%sil, %sil
	jne	.L1320
.L1319:
	orl	$64, %ecx
	movb	%cl, 8(%r12)
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1440:
	leal	-48(%rax), %edx
	movl	%eax, %edi
	cmpb	$9, %dl
	jbe	.L1364
	movq	%rsi, %rdx
	xorl	%r8d, %r8d
	jmp	.L1335
	.p2align 4,,10
	.p2align 3
.L1331:
	movzbl	2(%rdx), %edi
	leaq	2(%rdx), %rsi
	xorl	%r10d, %r10d
	jmp	.L1330
.L1368:
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rdi
	leaq	.LC5(%rip), %r8
	.p2align 4,,10
	.p2align 3
.L1318:
	movzbl	(%rdx,%rax), %esi
	cmpb	(%rdi,%rax), %sil
	je	.L1321
	cmpb	(%r8,%rax), %sil
	jne	.L1322
.L1321:
	addq	$1, %rax
	testb	%sil, %sil
	jne	.L1318
	jmp	.L1319
.L1332:
	leal	-48(%rdi), %eax
	cmpb	$9, %al
	ja	.L1316
	movsbl	%dil, %eax
	jmp	.L1364
.L1442:
	call	__stack_chk_fail@PLT
.L1443:
	cmpb	$0, 24(%r13)
	jne	.L1316
	cmpl	%r15d, %eax
	jl	.L1316
	jmp	.L1327
.L1444:
	movl	-116(%rbp), %eax
	movl	%eax, %esi
	andl	$221, %eax
	orl	$16, %esi
	orl	$16, %eax
	movl	%esi, -116(%rbp)
.L1349:
	testl	%eax, %eax
	je	.L1359
	testl	$1073741824, %esi
	je	.L1360
	andl	$-1073741825, %esi
	jmp	.L1359
	.cfi_endproc
.LFE2060:
	.size	uprv_decNumberFromString_67, .-uprv_decNumberFromString_67
	.p2align 4
	.globl	uprv_decNumberAnd_67
	.type	uprv_decNumberAnd_67, @function
uprv_decNumberAnd_67:
.LFB2063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	4(%rsi), %ecx
	testl	%ecx, %ecx
	jne	.L1446
	movzbl	8(%rsi), %eax
	testb	$112, %al
	jne	.L1446
	testb	%al, %al
	js	.L1446
	movl	4(%rdx), %r8d
	testl	%r8d, %r8d
	jne	.L1446
	movzbl	8(%rdx), %eax
	testb	$112, %al
	jne	.L1446
	testb	%al, %al
	js	.L1446
	movslq	(%rsi), %rax
	leaq	9(%rsi), %r9
	leaq	9(%rdx), %r10
	leaq	9(%r12), %r13
	cmpl	$49, %eax
	jg	.L1449
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rax), %r11d
	subq	$1, %r11
.L1450:
	movslq	(%rdx), %rax
	addq	%r9, %r11
	cmpl	$49, %eax
	jg	.L1451
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %ebx
	subq	$1, %rbx
.L1452:
	movslq	(%rdi), %rax
	addq	%r10, %rbx
	leaq	-1(%r13,%rax), %rcx
	cmpl	$49, %eax
	jg	.L1454
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	leaq	-1(%r13,%rax), %rcx
.L1454:
	cmpq	%rcx, %r13
	ja	.L1467
	addq	$1, %rcx
	movq	%r13, %r8
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1477:
	cmpq	%rbx, %r10
	jbe	.L1457
	movb	$0, (%r8)
	.p2align 4,,10
	.p2align 3
.L1458:
	addq	$1, %r8
	addq	$1, %r9
	addq	$1, %r10
	cmpq	%r8, %rcx
	je	.L1476
.L1461:
	cmpq	%r9, %r11
	jb	.L1477
	movzbl	(%r9), %esi
	cmpq	%rbx, %r10
	ja	.L1459
	movzbl	(%r10), %edx
	movl	%esi, %eax
	movb	$0, (%r8)
	orb	%dl, %al
	je	.L1458
	movzbl	%dl, %r14d
	leal	(%r14,%r14,4), %eax
	leal	(%r14,%rax,8), %eax
	movl	%edx, %r14d
	andl	%esi, %edx
	leal	(%rax,%rax,4), %eax
	shrw	$11, %ax
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	subl	%eax, %r14d
	andl	$1, %edx
	movl	%r14d, %eax
	je	.L1465
	movb	$1, (%r8)
.L1465:
	movzbl	%sil, %r14d
	leal	(%r14,%r14,4), %edx
	leal	(%r14,%rdx,8), %edx
	leal	(%rdx,%rdx,4), %edx
	shrw	$11, %dx
	leal	(%rdx,%rdx,4), %edx
	addl	%edx, %edx
	subl	%edx, %esi
	orl	%esi, %eax
	cmpb	$1, %al
	jbe	.L1458
	.p2align 4,,10
	.p2align 3
.L1446:
	movl	$32, %eax
	movq	$1, (%r12)
	movl	$128, %esi
	movw	%ax, 8(%r12)
	call	uprv_decContextSetStatus_67@PLT
.L1474:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1459:
	.cfi_restore_state
	movb	$0, (%r8)
	xorl	%eax, %eax
	testb	%sil, %sil
	jne	.L1465
	jmp	.L1458
	.p2align 4,,10
	.p2align 3
.L1457:
	movzbl	(%r10), %edx
	movb	$0, (%r8)
	testb	%dl, %dl
	je	.L1458
	movzbl	%dl, %esi
	leal	(%rsi,%rsi,4), %eax
	leal	(%rsi,%rax,8), %eax
	xorl	%esi, %esi
	leal	(%rax,%rax,4), %eax
	shrw	$11, %ax
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1451:
	movslq	%eax, %rbx
	subq	$1, %rbx
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1449:
	movslq	%eax, %r11
	subq	$1, %r11
	jmp	.L1450
	.p2align 4,,10
	.p2align 3
.L1476:
	subq	%r13, %rcx
	movl	%ecx, %r8d
	movslq	%ecx, %rcx
	leaq	-1(%rcx), %rax
.L1455:
	addq	%rax, %r13
	jc	.L1462
	leaq	8(%r12), %rax
.L1463:
	cmpb	$0, 0(%r13)
	jne	.L1462
	cmpl	$1, %r8d
	je	.L1462
	subq	$1, %r13
	subl	$1, %r8d
	cmpq	%r13, %rax
	jne	.L1463
	.p2align 4,,10
	.p2align 3
.L1462:
	movl	%r8d, (%r12)
	movl	$0, 4(%r12)
	movb	$0, 8(%r12)
	jmp	.L1474
.L1467:
	movq	$-1, %rax
	jmp	.L1455
	.cfi_endproc
.LFE2063:
	.size	uprv_decNumberAnd_67, .-uprv_decNumberAnd_67
	.p2align 4
	.globl	uprv_decNumberCompare_67
	.type	uprv_decNumberCompare_67, @function
uprv_decNumberCompare_67:
.LFB2064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movzbl	8(%rax), %edx
	orb	8(%rsi), %dl
	movq	%fs:40, %rcx
	movq	%rcx, -24(%rbp)
	xorl	%ecx, %ecx
	andl	$48, %edx
	movl	$0, -28(%rbp)
	je	.L1479
	movq	%rsi, %rdx
	leaq	-28(%rbp), %r8
	movq	%r13, %rcx
	movq	%rax, %rsi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
.L1480:
	movl	-28(%rbp), %esi
	testl	%esi, %esi
	jne	.L1482
.L1485:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1496
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1479:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	_ZL10decComparePK9decNumberS1_h
	cmpl	$-2147483648, %eax
	je	.L1497
	movq	$1, (%r12)
	movb	$0, 8(%r12)
	testl	%eax, %eax
	jne	.L1483
	movl	-28(%rbp), %esi
	movb	$0, 9(%r12)
	testl	%esi, %esi
	je	.L1485
.L1482:
	testb	$-35, %sil
	je	.L1486
.L1498:
	testl	$1073741824, %esi
	je	.L1487
	andl	$-1073741825, %esi
.L1486:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1483:
	movb	$1, 9(%r12)
	jns	.L1480
	movb	$-128, 8(%r12)
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1497:
	movl	-28(%rbp), %esi
	orl	$16, %esi
	movl	%esi, -28(%rbp)
	testb	$-35, %sil
	je	.L1486
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1487:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L1486
.L1496:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2064:
	.size	uprv_decNumberCompare_67, .-uprv_decNumberCompare_67
	.p2align 4
	.globl	uprv_decNumberCompareSignal_67
	.type	uprv_decNumberCompareSignal_67, @function
uprv_decNumberCompareSignal_67:
.LFB2065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movzbl	8(%rax), %edx
	orb	8(%rsi), %dl
	movq	%fs:40, %rcx
	movq	%rcx, -24(%rbp)
	xorl	%ecx, %ecx
	andl	$48, %edx
	movl	$0, -28(%rbp)
	je	.L1500
	movq	%rsi, %rdx
	leaq	-28(%rbp), %r8
	movq	%r13, %rcx
	movq	%rax, %rsi
	movl	$1073741952, -28(%rbp)
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
.L1501:
	movl	-28(%rbp), %esi
	testl	%esi, %esi
	jne	.L1503
.L1505:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1519
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1500:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	_ZL10decComparePK9decNumberS1_h
	cmpl	$-2147483648, %eax
	je	.L1520
	movq	$1, (%r12)
	movb	$0, 8(%r12)
	testl	%eax, %eax
	jne	.L1508
	movl	-28(%rbp), %esi
	movb	$0, 9(%r12)
	testl	%esi, %esi
	je	.L1505
.L1503:
	testb	$-35, %sil
	je	.L1506
.L1521:
	testl	$1073741824, %esi
	je	.L1507
	andl	$-1073741825, %esi
.L1506:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L1505
	.p2align 4,,10
	.p2align 3
.L1508:
	movb	$1, 9(%r12)
	jns	.L1501
	movb	$-128, 8(%r12)
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1520:
	movl	-28(%rbp), %esi
	orl	$16, %esi
	movl	%esi, -28(%rbp)
	testb	$-35, %sil
	je	.L1506
	jmp	.L1521
	.p2align 4,,10
	.p2align 3
.L1507:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L1506
.L1519:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2065:
	.size	uprv_decNumberCompareSignal_67, .-uprv_decNumberCompareSignal_67
	.p2align 4
	.globl	uprv_decNumberCompareTotal_67
	.type	uprv_decNumberCompareTotal_67, @function
uprv_decNumberCompareTotal_67:
.LFB2066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	8(%rsi), %eax
	movq	%rsi, %rbx
	movzbl	8(%rdx), %edx
	testb	%al, %al
	js	.L1580
	testb	%dl, %dl
	js	.L1557
	movl	%eax, %ecx
	orl	%edx, %ecx
	andl	$48, %ecx
	je	.L1527
.L1583:
	testb	$48, %al
	je	.L1549
	testb	$48, %dl
	je	.L1574
	testb	$16, %al
	je	.L1530
	testb	$32, %dl
	je	.L1530
	.p2align 4,,10
	.p2align 3
.L1549:
	testb	%al, %al
	js	.L1557
.L1571:
	movq	$1, (%r12)
	movb	$1, 9(%r12)
.L1547:
	popq	%rbx
	movq	%r12, %rax
	movb	$-128, 8(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1527:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZL10decComparePK9decNumberS1_h
	cmpl	$-2147483648, %eax
	jne	.L1581
	movl	$32, %edx
	movq	$1, (%r12)
	movl	$16, %esi
	movq	%r14, %rdi
	movw	%dx, 8(%r12)
	call	uprv_decContextSetStatus_67@PLT
.L1569:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1581:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L1548
.L1538:
	movl	4(%r13), %eax
	cmpl	%eax, 4(%rbx)
	je	.L1582
	movzbl	8(%rbx), %eax
	jl	.L1549
.L1574:
	testb	%al, %al
	js	.L1571
.L1557:
	movl	$256, %eax
	popq	%rbx
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1580:
	.cfi_restore_state
	testb	%dl, %dl
	jns	.L1571
	movl	%eax, %ecx
	orl	%edx, %ecx
	andl	$48, %ecx
	je	.L1527
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1530:
	testb	$32, %al
	jne	.L1584
.L1532:
	movslq	0(%r13), %rcx
	cmpl	$49, %ecx
	jg	.L1534
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rcx), %ecx
.L1534:
	movslq	(%rbx), %rdx
	cmpl	$49, %edx
	jg	.L1535
	leaq	_ZL8d2utable(%rip), %rsi
	movzbl	(%rsi,%rdx), %edx
.L1535:
	cmpl	%ecx, %edx
	jg	.L1574
	jl	.L1549
	leaq	9(%rbx), %rcx
	leaq	-1(%rdx), %rsi
	leaq	8(%r13,%rdx), %rdi
	addq	%rsi, %rcx
	jc	.L1538
	leaq	8(%rbx), %rsi
	xorl	%edx, %edx
	subq	%rcx, %rsi
	jmp	.L1541
	.p2align 4,,10
	.p2align 3
.L1585:
	jb	.L1540
	subq	$1, %rdx
	cmpq	%rdx, %rsi
	je	.L1538
.L1541:
	movzbl	(%rdi,%rdx), %r8d
	cmpb	%r8b, (%rcx,%rdx)
	jbe	.L1585
	testb	%al, %al
	jns	.L1557
	movl	$-1, %eax
.L1548:
	movq	$1, (%r12)
	movw	$256, 8(%r12)
	testl	%eax, %eax
	jns	.L1569
	jmp	.L1547
	.p2align 4,,10
	.p2align 3
.L1584:
	andl	$16, %edx
	jne	.L1574
	jmp	.L1532
	.p2align 4,,10
	.p2align 3
.L1582:
	xorl	%ecx, %ecx
	movq	$1, (%r12)
	movw	%cx, 8(%r12)
	jmp	.L1569
.L1540:
	testb	%al, %al
	jns	.L1571
	movl	$1, %eax
	jmp	.L1548
	.cfi_endproc
.LFE2066:
	.size	uprv_decNumberCompareTotal_67, .-uprv_decNumberCompareTotal_67
	.p2align 4
	.globl	uprv_decNumberCompareTotalMag_67
	.type	uprv_decNumberCompareTotalMag_67, @function
uprv_decNumberCompareTotalMag_67:
.LFB2067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movq	%rcx, -168(%rbp)
	movzbl	8(%rsi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%r13b, %r13b
	js	.L1696
.L1587:
	movzbl	8(%rbx), %ecx
	xorl	%r15d, %r15d
	testb	%cl, %cl
	jns	.L1604
	movl	(%rbx), %eax
	cmpl	$49, %eax
	jle	.L1697
	leal	11(%rax), %edi
.L1597:
	movq	%r9, -176(%rbp)
	call	uprv_malloc_67@PLT
	movq	-176(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L1599
	movzbl	8(%r12), %r13d
	movzbl	8(%rbx), %ecx
	cmpq	%rax, %rbx
	je	.L1601
	movl	4(%rbx), %edx
	movslq	(%rbx), %rax
	movl	%edx, 4(%r15)
	movzwl	8(%rbx), %edx
	movl	%eax, (%r15)
	movw	%dx, 8(%r15)
	cmpl	$1, %eax
	jle	.L1653
	leaq	10(%r15), %rdi
	leaq	9(%rbx), %r11
	movq	%r15, %r10
	cmpl	$49, %eax
	jle	.L1698
.L1603:
	addq	%r11, %rax
	leaq	10(%rbx), %rsi
	cmpq	%rsi, %rax
	jbe	.L1654
	subq	%rbx, %rax
	movq	%r10, -192(%rbp)
	leaq	-10(%rax), %rdx
	movq	%r9, -184(%rbp)
	movb	%cl, -176(%rbp)
	call	memcpy@PLT
	movq	-192(%rbp), %r10
	movzbl	-176(%rbp), %ecx
	movq	-184(%rbp), %r9
	movq	%r10, %rbx
.L1601:
	andl	$127, %ecx
	movb	%cl, 8(%rbx)
	testb	%r13b, %r13b
	jns	.L1604
.L1617:
	movq	$1, (%r14)
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1604:
	movl	%r13d, %eax
	orl	%ecx, %eax
	testb	$48, %al
	je	.L1699
	testb	$48, %r13b
	je	.L1617
	testb	$48, %cl
	jne	.L1700
.L1616:
	movq	$1, (%r14)
.L1690:
	movl	$256, %eax
	movw	%ax, 8(%r14)
.L1695:
	testq	%r9, %r9
	je	.L1693
.L1623:
	movq	%r9, %rdi
	call	uprv_free_67@PLT
	testq	%r15, %r15
	je	.L1627
.L1639:
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	jmp	.L1627
	.p2align 4,,10
	.p2align 3
.L1697:
	leaq	_ZL8d2utable(%rip), %rdx
	movslq	%eax, %rsi
	movzbl	(%rdx,%rsi), %edi
	addl	$11, %edi
	cmpl	$48, %edi
	ja	.L1597
	movl	4(%rbx), %edi
	movl	%eax, -112(%rbp)
	movl	%edi, -108(%rbp)
	movzwl	8(%rbx), %edi
	movw	%di, -104(%rbp)
	cmpl	$1, %eax
	jg	.L1643
	leaq	-112(%rbp), %rbx
	xorl	%r15d, %r15d
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1699:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r9, -176(%rbp)
	call	_ZL10decComparePK9decNumberS1_h
	movq	-176(%rbp), %r9
	cmpl	$-2147483648, %eax
	jne	.L1701
	testq	%r9, %r9
	je	.L1694
	movq	%r9, %rdi
	call	uprv_free_67@PLT
.L1694:
	testq	%r15, %r15
	je	.L1632
	movq	%r15, %rdi
	call	uprv_free_67@PLT
.L1632:
	movl	$32, %ecx
	movq	$1, (%r14)
	movl	$16, %esi
	movq	-168(%rbp), %rdi
	movw	%cx, 8(%r14)
	call	uprv_decContextSetStatus_67@PLT
.L1627:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1702
	addq	$152, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1701:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L1703
.L1615:
	movl	4(%rbx), %eax
	cmpl	%eax, 4(%r12)
	je	.L1704
	movzbl	8(%r12), %eax
	movq	$1, (%r14)
	jge	.L1705
	testb	%al, %al
	js	.L1690
.L1689:
	movb	$1, 9(%r14)
.L1634:
	movb	$-128, 8(%r14)
	testq	%r9, %r9
	jne	.L1623
	.p2align 4,,10
	.p2align 3
.L1693:
	testq	%r15, %r15
	jne	.L1639
	jmp	.L1627
	.p2align 4,,10
	.p2align 3
.L1696:
	movl	(%rsi), %eax
	cmpl	$49, %eax
	jg	.L1588
	leaq	_ZL8d2utable(%rip), %rdx
	movslq	%eax, %r8
	movzbl	(%rdx,%r8), %edi
	addl	$11, %edi
	cmpl	$48, %edi
	ja	.L1589
	movl	4(%rsi), %ecx
	movl	%eax, -160(%rbp)
	movl	%ecx, -156(%rbp)
	movzwl	8(%rsi), %ecx
	movw	%cx, -152(%rbp)
	cmpl	$1, %eax
	jg	.L1645
	leaq	-160(%rbp), %r12
.L1593:
	andl	$127, %r13d
	movb	%r13b, 8(%r12)
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1588:
	leal	11(%rax), %edi
.L1589:
	call	uprv_malloc_67@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L1632
	movzbl	8(%r12), %r13d
	cmpq	%rax, %r12
	je	.L1593
	movl	4(%r12), %edx
	movslq	(%r12), %rax
	movl	%edx, 4(%r9)
	movzwl	8(%r12), %edx
	movl	%eax, (%r9)
	movw	%dx, 8(%r9)
	cmpl	$1, %eax
	jle	.L1651
	leaq	10(%r9), %rdi
	leaq	9(%r12), %rsi
	movq	%r9, %r15
	cmpl	$49, %eax
	jle	.L1706
.L1595:
	addq	%rsi, %rax
	leaq	10(%r12), %rsi
	cmpq	%rsi, %rax
	jbe	.L1652
	subq	%r12, %rax
	andl	$127, %r13d
	movq	%r15, %r12
	movq	%r9, -176(%rbp)
	leaq	-10(%rax), %rdx
	call	memcpy@PLT
	movb	%r13b, 8(%r12)
	movq	-176(%rbp), %r9
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1643:
	leaq	9(%rbx), %r11
	leaq	-112(%rbp), %r10
	xorl	%r15d, %r15d
	leaq	-102(%rbp), %rdi
.L1642:
	movzbl	(%rdx,%rsi), %eax
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1645:
	leaq	9(%r12), %rsi
	leaq	-160(%rbp), %r15
	leaq	-150(%rbp), %rdi
.L1644:
	movzbl	(%rdx,%r8), %eax
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1705:
	testb	%al, %al
	js	.L1689
	movl	$256, %esi
	movw	%si, 8(%r14)
	testq	%r9, %r9
	jne	.L1623
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1700:
	testb	$16, %r13b
	je	.L1608
	testb	$32, %cl
	jne	.L1617
.L1608:
	andl	$32, %r13d
	jne	.L1707
.L1610:
	movslq	(%rbx), %rdx
	cmpl	$49, %edx
	jg	.L1612
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdx), %edx
.L1612:
	movslq	(%r12), %rax
	cmpl	$49, %eax
	jg	.L1613
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rax), %eax
.L1613:
	cmpl	%eax, %edx
	jl	.L1616
	jg	.L1617
	leaq	9(%r12), %rdx
	leaq	-1(%rax), %rcx
	leaq	8(%rbx,%rax), %rsi
	addq	%rcx, %rdx
	jc	.L1615
	leaq	8(%r12), %rcx
	xorl	%eax, %eax
	subq	%rdx, %rcx
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L1708:
	jb	.L1617
	subq	$1, %rax
	cmpq	%rcx, %rax
	je	.L1615
.L1618:
	movzbl	(%rsi,%rax), %edi
	cmpb	%dil, (%rdx,%rax)
	jbe	.L1708
	jmp	.L1616
	.p2align 4,,10
	.p2align 3
.L1707:
	andl	$16, %ecx
	jne	.L1616
	jmp	.L1610
	.p2align 4,,10
	.p2align 3
.L1704:
	xorl	%edx, %edx
	movq	$1, (%r14)
	movw	%dx, 8(%r14)
	jmp	.L1695
	.p2align 4,,10
	.p2align 3
.L1653:
	movq	%r15, %rbx
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1651:
	movq	%r9, %r12
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1654:
	movq	%r10, %rbx
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1652:
	movq	%r15, %r12
	jmp	.L1593
.L1702:
	call	__stack_chk_fail@PLT
.L1599:
	testq	%r9, %r9
	je	.L1632
	movq	%r9, %rdi
	call	uprv_free_67@PLT
	jmp	.L1632
.L1703:
	movq	$1, (%r14)
	movw	$256, 8(%r14)
	jns	.L1695
	jmp	.L1634
.L1698:
	leaq	_ZL8d2utable(%rip), %rdx
	movslq	%eax, %rsi
	jmp	.L1642
.L1706:
	leaq	_ZL8d2utable(%rip), %rdx
	movslq	%eax, %r8
	jmp	.L1644
	.cfi_endproc
.LFE2067:
	.size	uprv_decNumberCompareTotalMag_67, .-uprv_decNumberCompareTotalMag_67
	.p2align 4
	.globl	uprv_decNumberInvert_67
	.type	uprv_decNumberInvert_67, @function
uprv_decNumberInvert_67:
.LFB2072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rdx, %rdi
	subq	$8, %rsp
	movl	4(%rsi), %r9d
	testl	%r9d, %r9d
	jne	.L1710
	movzbl	8(%rsi), %eax
	testb	$112, %al
	jne	.L1710
	testb	%al, %al
	js	.L1710
	movslq	(%rsi), %rax
	leaq	9(%rsi), %rcx
	leaq	9(%r12), %r8
	cmpl	$49, %eax
	jle	.L1732
	movslq	%eax, %rdx
	movslq	(%rdi), %rax
	subq	$1, %rdx
	addq	%rcx, %rdx
	cmpl	$49, %eax
	jle	.L1733
.L1715:
	leaq	-1(%r8,%rax), %rax
.L1716:
	cmpq	%rax, %r8
	ja	.L1724
	leaq	1(%rax), %r9
	movq	%r8, %rsi
.L1720:
	cmpq	%rcx, %rdx
	jb	.L1718
	movzbl	(%rcx), %r11d
	leal	(%r11,%r11,4), %eax
	movl	%r11d, %r10d
	leal	(%r11,%rax,8), %eax
	leal	(%rax,%rax,4), %eax
	shrw	$11, %ax
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	subl	%eax, %r11d
	andl	$1, %r10d
	sete	(%rsi)
	cmpb	$1, %r11b
	ja	.L1710
.L1723:
	addq	$1, %rsi
	addq	$1, %rcx
	cmpq	%r9, %rsi
	jne	.L1720
	subq	%r8, %rsi
	movl	%esi, %r9d
	movslq	%esi, %rsi
	leaq	-1(%rsi), %rax
.L1717:
	addq	%rax, %r8
	jc	.L1721
	leaq	8(%r12), %rax
.L1722:
	cmpb	$0, (%r8)
	jne	.L1721
	cmpl	$1, %r9d
	je	.L1721
	subq	$1, %r8
	subl	$1, %r9d
	cmpq	%rax, %r8
	jne	.L1722
.L1721:
	movl	%r9d, (%r12)
	movl	$0, 4(%r12)
	movb	$0, 8(%r12)
	jmp	.L1730
	.p2align 4,,10
	.p2align 3
.L1710:
	movl	$32, %eax
	movq	$1, (%r12)
	movl	$128, %esi
	movw	%ax, 8(%r12)
	call	uprv_decContextSetStatus_67@PLT
.L1730:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1732:
	.cfi_restore_state
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %edx
	movslq	(%rdi), %rax
	subq	$1, %rdx
	addq	%rcx, %rdx
	cmpl	$49, %eax
	jg	.L1715
.L1733:
	leaq	_ZL8d2utable(%rip), %rsi
	movzbl	(%rsi,%rax), %eax
	leaq	-1(%r8,%rax), %rax
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1718:
	movb	$1, (%rsi)
	jmp	.L1723
.L1724:
	movq	$-1, %rax
	jmp	.L1717
	.cfi_endproc
.LFE2072:
	.size	uprv_decNumberInvert_67, .-uprv_decNumberInvert_67
	.p2align 4
	.globl	uprv_decNumberLogB_67
	.type	uprv_decNumberLogB_67, @function
uprv_decNumberLogB_67:
.LFB2074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rsi), %eax
	movl	$0, -28(%rbp)
	testb	$48, %al
	jne	.L1779
	testb	$64, %al
	jne	.L1780
	movl	(%rsi), %edx
	movzbl	9(%rsi), %ecx
	cmpl	$1, %edx
	je	.L1781
.L1748:
	movl	4(%rsi), %eax
	addl	%edx, %eax
	movl	%eax, %edi
	subl	$1, %edi
	js	.L1750
	movl	$0, %esi
	movq	$1, (%r12)
	movl	%edi, %ecx
	movw	%si, 8(%r12)
	jne	.L1752
	.p2align 4,,10
	.p2align 3
.L1758:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1782
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1780:
	.cfi_restore_state
	cmpq	%rdi, %rsi
	je	.L1740
	movb	%al, 8(%rdi)
	movl	4(%rsi), %edx
	movl	%edx, 4(%rdi)
	movl	(%rsi), %edx
	movl	%edx, (%rdi)
	movzbl	9(%rsi), %edx
	movb	%dl, 9(%rdi)
	movl	(%rsi), %ecx
	cmpl	$1, %ecx
	jle	.L1740
	leaq	10(%rdi), %r8
	movslq	%ecx, %rdx
	leaq	9(%rsi), %rdi
	cmpl	$49, %ecx
	jg	.L1742
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
.L1742:
	addq	%rdx, %rdi
	leaq	10(%rsi), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1740
	movq	%rdi, %rax
	subq	%rsi, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L1743
	leaq	25(%rsi), %rax
	subq	%r8, %rax
	cmpq	$30, %rax
	jbe	.L1743
	movq	%rdi, %rax
	subq	%rsi, %rax
	leaq	-10(%rax), %r9
	movl	$10, %eax
	movq	%r9, %rcx
	andq	$-16, %rcx
	addq	$10, %rcx
	.p2align 4,,10
	.p2align 3
.L1744:
	movdqu	(%rsi,%rax), %xmm0
	movups	%xmm0, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L1744
	movq	%r9, %rcx
	andq	$-16, %rcx
	leaq	(%rdx,%rcx), %rax
	leaq	(%r8,%rcx), %rdx
	cmpq	%r9, %rcx
	je	.L1746
	movzbl	(%rax), %ecx
	movb	%cl, (%rdx)
	leaq	1(%rax), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1746
	movzbl	1(%rax), %ecx
	movb	%cl, 1(%rdx)
	leaq	2(%rax), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1746
	movzbl	2(%rax), %ecx
	movb	%cl, 2(%rdx)
	leaq	3(%rax), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1746
	movzbl	3(%rax), %ecx
	movb	%cl, 3(%rdx)
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1746
	movzbl	4(%rax), %ecx
	movb	%cl, 4(%rdx)
	leaq	5(%rax), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1746
	movzbl	5(%rax), %ecx
	movb	%cl, 5(%rdx)
	leaq	6(%rax), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1746
	movzbl	6(%rax), %ecx
	movb	%cl, 6(%rdx)
	leaq	7(%rax), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1746
	movzbl	7(%rax), %ecx
	movb	%cl, 7(%rdx)
	leaq	8(%rax), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1746
	movzbl	8(%rax), %ecx
	movb	%cl, 8(%rdx)
	leaq	9(%rax), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1746
	movzbl	9(%rax), %ecx
	movb	%cl, 9(%rdx)
	leaq	10(%rax), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1746
	movzbl	10(%rax), %ecx
	movb	%cl, 10(%rdx)
	leaq	11(%rax), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1746
	movzbl	11(%rax), %ecx
	movb	%cl, 11(%rdx)
	leaq	12(%rax), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1746
	movzbl	12(%rax), %ecx
	movb	%cl, 12(%rdx)
	leaq	13(%rax), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1746
	movzbl	13(%rax), %ecx
	movb	%cl, 13(%rdx)
	leaq	14(%rax), %rcx
	cmpq	%rcx, %rdi
	jbe	.L1746
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
	.p2align 4,,10
	.p2align 3
.L1746:
	movzbl	8(%r12), %eax
.L1740:
	andl	$127, %eax
	movb	%al, 8(%r12)
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1750:
	cmpl	$-2147483648, %edi
	je	.L1783
	xorl	%edx, %edx
	movq	$1, (%r12)
	movl	$1, %ecx
	movw	%dx, 8(%r12)
	subl	%eax, %ecx
.L1752:
	leaq	9(%r12), %r8
	movl	$3435973837, %r9d
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L1754:
	movl	%ecx, %eax
	movl	%ecx, %r10d
	addq	$1, %rdx
	imulq	%r9, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %esi
	addl	%esi, %esi
	subl	%esi, %r10d
	movl	%ecx, %esi
	movl	%eax, %ecx
	movb	%r10b, -1(%rdx)
	cmpl	$9, %esi
	ja	.L1754
	subq	%r8, %rdx
	movl	%edx, %ecx
	movslq	%edx, %rdx
	leaq	-1(%r8,%rdx), %rax
	cmpq	%rax, %r8
	ja	.L1755
	leaq	8(%r12), %rdx
.L1756:
	cmpb	$0, (%rax)
	jne	.L1755
	cmpl	$1, %ecx
	je	.L1755
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rdx, %rax
	jne	.L1756
	.p2align 4,,10
	.p2align 3
.L1755:
	movl	%ecx, (%r12)
	testl	%edi, %edi
	jns	.L1758
	movb	$-128, 8(%r12)
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1779:
	movq	%rdx, %rcx
	leaq	-28(%rbp), %r8
	xorl	%edx, %edx
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-28(%rbp), %esi
	testl	%esi, %esi
	je	.L1758
	testb	$-35, %sil
	je	.L1749
	testl	$1073741824, %esi
	je	.L1757
	andl	$-1073741825, %esi
.L1749:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1781:
	andl	$112, %eax
	orb	%cl, %al
	jne	.L1748
	movq	$1, (%rdi)
	movl	$192, %edi
	movl	$2, %esi
	movw	%di, 8(%r12)
	movl	$2, -28(%rbp)
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L1757:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L1783:
	xorl	%ecx, %ecx
	movq	$1, (%r12)
	movw	%cx, 8(%r12)
	movl	$-2147483648, %ecx
	jmp	.L1752
	.p2align 4,,10
	.p2align 3
.L1743:
	movq	%rdi, %rax
	movl	$10, %edx
	subq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L1747:
	movzbl	(%rsi,%rdx), %ecx
	movb	%cl, (%r12,%rdx)
	addq	$1, %rdx
	cmpq	%rax, %rdx
	jne	.L1747
	jmp	.L1746
.L1782:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2074:
	.size	uprv_decNumberLogB_67, .-uprv_decNumberLogB_67
	.p2align 4
	.globl	uprv_decNumberMax_67
	.type	uprv_decNumberMax_67, @function
uprv_decNumberMax_67:
.LFB2076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$16, %rsp
	movzbl	8(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rsi), %eax
	movl	$0, -48(%rbp)
	movl	%eax, %edx
	orl	%ecx, %edx
	testb	$48, %dl
	je	.L1785
	andl	$16, %edx
	je	.L1826
.L1786:
	movq	%r12, %rsi
	leaq	-48(%rbp), %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-48(%rbp), %esi
	testl	%esi, %esi
	jne	.L1789
.L1795:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1827
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1826:
	.cfi_restore_state
	testb	$48, %al
	jne	.L1828
.L1787:
	testb	$32, %al
	jne	.L1829
	movl	$0, -44(%rbp)
.L1792:
	movb	%al, 8(%r13)
	movl	4(%r12), %eax
	leaq	-44(%rbp), %r14
	movq	%r15, %rsi
	leaq	-48(%rbp), %r9
	leaq	9(%r12), %rdx
	movq	%r14, %r8
	movq	%r13, %rdi
	movl	%eax, 4(%r13)
	movl	(%r12), %ecx
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%r9, %rcx
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-48(%rbp), %esi
	testl	%esi, %esi
	je	.L1795
	.p2align 4,,10
	.p2align 3
.L1789:
	testb	$-35, %sil
	je	.L1796
	testl	$1073741824, %esi
	je	.L1797
	andl	$-1073741825, %esi
.L1796:
	movq	%r15, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1785:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZL10decComparePK9decNumberS1_h
	cmpl	$-2147483648, %eax
	je	.L1830
	movl	$0, -44(%rbp)
	testl	%eax, %eax
	jne	.L1831
	movzbl	8(%r12), %eax
	movzbl	8(%r14), %ecx
	movl	%eax, %esi
	movl	%ecx, %edx
	andl	$-128, %esi
	andl	$-128, %edx
	cmpb	%dl, %sil
	je	.L1790
	testb	%al, %al
	jns	.L1792
.L1791:
	movl	%ecx, %eax
	movq	%r14, %r12
	jmp	.L1792
	.p2align 4,,10
	.p2align 3
.L1828:
	testb	$48, %cl
	je	.L1787
	jmp	.L1786
	.p2align 4,,10
	.p2align 3
.L1830:
	movl	-48(%rbp), %esi
	orl	$16, %esi
	movl	%esi, -48(%rbp)
	jmp	.L1789
	.p2align 4,,10
	.p2align 3
.L1797:
	movl	$32, %eax
	movq	$1, 0(%r13)
	movw	%ax, 8(%r13)
	jmp	.L1796
	.p2align 4,,10
	.p2align 3
.L1829:
	movl	$0, -44(%rbp)
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L1790:
	movl	4(%r12), %edx
	movl	4(%r14), %esi
	testb	%al, %al
	jns	.L1793
	testb	%cl, %cl
	jns	.L1793
	cmpl	%edx, %esi
	jg	.L1792
	jmp	.L1791
.L1831:
	jle	.L1794
	movzbl	8(%r12), %eax
	jmp	.L1792
	.p2align 4,,10
	.p2align 3
.L1793:
	cmpl	%edx, %esi
	jl	.L1792
	jmp	.L1791
.L1794:
	movzbl	8(%r14), %ecx
	jmp	.L1791
.L1827:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2076:
	.size	uprv_decNumberMax_67, .-uprv_decNumberMax_67
	.p2align 4
	.globl	uprv_decNumberMaxMag_67
	.type	uprv_decNumberMaxMag_67, @function
uprv_decNumberMaxMag_67:
.LFB2077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$16, %rsp
	movzbl	8(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rsi), %eax
	movl	$0, -48(%rbp)
	movl	%eax, %edx
	orl	%ecx, %edx
	testb	$48, %dl
	je	.L1833
	andl	$16, %edx
	je	.L1875
.L1834:
	movq	%r12, %rsi
	leaq	-48(%rbp), %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-48(%rbp), %esi
	testl	%esi, %esi
	jne	.L1837
.L1843:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1876
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1875:
	.cfi_restore_state
	testb	$48, %al
	jne	.L1877
.L1835:
	testb	$32, %al
	jne	.L1878
	movl	$0, -44(%rbp)
.L1840:
	movb	%al, 8(%r13)
	movl	4(%r12), %eax
	leaq	-44(%rbp), %r14
	movq	%r15, %rsi
	leaq	-48(%rbp), %r9
	leaq	9(%r12), %rdx
	movq	%r14, %r8
	movq	%r13, %rdi
	movl	%eax, 4(%r13)
	movl	(%r12), %ecx
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%r9, %rcx
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-48(%rbp), %esi
	testl	%esi, %esi
	je	.L1843
	.p2align 4,,10
	.p2align 3
.L1837:
	testb	$-35, %sil
	je	.L1844
	testl	$1073741824, %esi
	je	.L1845
	andl	$-1073741825, %esi
.L1844:
	movq	%r15, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L1843
	.p2align 4,,10
	.p2align 3
.L1833:
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZL10decComparePK9decNumberS1_h
	cmpl	$-2147483648, %eax
	je	.L1879
	movl	$0, -44(%rbp)
	testl	%eax, %eax
	jne	.L1880
	movzbl	8(%r12), %eax
	movzbl	8(%r14), %ecx
	movl	%eax, %esi
	movl	%ecx, %edx
	andl	$-128, %esi
	andl	$-128, %edx
	cmpb	%dl, %sil
	je	.L1838
	testb	%al, %al
	jns	.L1840
	.p2align 4,,10
	.p2align 3
.L1839:
	movl	%ecx, %eax
	movq	%r14, %r12
	jmp	.L1840
	.p2align 4,,10
	.p2align 3
.L1877:
	testb	$48, %cl
	je	.L1835
	jmp	.L1834
	.p2align 4,,10
	.p2align 3
.L1879:
	movl	-48(%rbp), %esi
	orl	$16, %esi
	movl	%esi, -48(%rbp)
	jmp	.L1837
	.p2align 4,,10
	.p2align 3
.L1845:
	movl	$32, %eax
	movq	$1, 0(%r13)
	movw	%ax, 8(%r13)
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1878:
	movl	$0, -44(%rbp)
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1838:
	movl	4(%r12), %esi
	movl	4(%r14), %edx
	testb	%al, %al
	jns	.L1841
	testb	%cl, %cl
	jns	.L1841
	cmpl	%edx, %esi
	jl	.L1840
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1841:
	cmpl	%edx, %esi
	jg	.L1840
	jmp	.L1839
.L1876:
	call	__stack_chk_fail@PLT
.L1880:
	jle	.L1842
	movzbl	8(%r12), %eax
	jmp	.L1840
	.p2align 4,,10
	.p2align 3
.L1842:
	movzbl	8(%r14), %ecx
	jmp	.L1839
	.cfi_endproc
.LFE2077:
	.size	uprv_decNumberMaxMag_67, .-uprv_decNumberMaxMag_67
	.p2align 4
	.globl	uprv_decNumberMin_67
	.type	uprv_decNumberMin_67, @function
uprv_decNumberMin_67:
.LFB2078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$16, %rsp
	movzbl	8(%rdx), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rsi), %eax
	movl	$0, -48(%rbp)
	movl	%eax, %ecx
	orl	%edx, %ecx
	testb	$48, %cl
	je	.L1882
	andl	$16, %ecx
	je	.L1923
.L1883:
	movq	%r12, %rsi
	leaq	-48(%rbp), %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-48(%rbp), %esi
	testl	%esi, %esi
	jne	.L1886
.L1892:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1924
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1923:
	.cfi_restore_state
	testb	$48, %al
	jne	.L1925
.L1884:
	testb	$32, %al
	jne	.L1926
	movl	$0, -44(%rbp)
.L1888:
	movl	%eax, %edx
.L1891:
	movb	%dl, 8(%r13)
	movl	4(%r12), %eax
	leaq	-44(%rbp), %r14
	movq	%r15, %rsi
	leaq	-48(%rbp), %r9
	leaq	9(%r12), %rdx
	movq	%r14, %r8
	movq	%r13, %rdi
	movl	%eax, 4(%r13)
	movl	(%r12), %ecx
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%r9, %rcx
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-48(%rbp), %esi
	testl	%esi, %esi
	je	.L1892
	.p2align 4,,10
	.p2align 3
.L1886:
	testb	$-35, %sil
	je	.L1893
	testl	$1073741824, %esi
	je	.L1894
	andl	$-1073741825, %esi
.L1893:
	movq	%r15, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L1892
	.p2align 4,,10
	.p2align 3
.L1882:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZL10decComparePK9decNumberS1_h
	cmpl	$-2147483648, %eax
	je	.L1927
	movl	$0, -44(%rbp)
	testl	%eax, %eax
	jne	.L1928
	movzbl	8(%r12), %eax
	movzbl	8(%r14), %edx
	movl	%eax, %esi
	movl	%edx, %ecx
	andl	$-128, %esi
	andl	$-128, %ecx
	cmpb	%cl, %sil
	je	.L1887
	testb	%al, %al
	js	.L1888
.L1895:
	movq	%r14, %r12
	jmp	.L1891
	.p2align 4,,10
	.p2align 3
.L1925:
	testb	$48, %dl
	je	.L1884
	jmp	.L1883
	.p2align 4,,10
	.p2align 3
.L1927:
	movl	-48(%rbp), %esi
	orl	$16, %esi
	movl	%esi, -48(%rbp)
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1894:
	movl	$32, %eax
	movq	$1, 0(%r13)
	movw	%ax, 8(%r13)
	jmp	.L1893
	.p2align 4,,10
	.p2align 3
.L1926:
	movl	$0, -44(%rbp)
	jmp	.L1895
	.p2align 4,,10
	.p2align 3
.L1887:
	movl	4(%r12), %ecx
	movl	4(%r14), %esi
	testb	%al, %al
	jns	.L1890
	testb	%dl, %dl
	jns	.L1890
	cmpl	%ecx, %esi
	jg	.L1895
	jmp	.L1888
	.p2align 4,,10
	.p2align 3
.L1890:
	cmpl	%ecx, %esi
	jl	.L1895
	jmp	.L1888
.L1924:
	call	__stack_chk_fail@PLT
.L1928:
	jns	.L1929
	movzbl	8(%r12), %eax
	jmp	.L1888
.L1929:
	movzbl	8(%r14), %edx
	jmp	.L1895
	.cfi_endproc
.LFE2078:
	.size	uprv_decNumberMin_67, .-uprv_decNumberMin_67
	.p2align 4
	.globl	uprv_decNumberMinMag_67
	.type	uprv_decNumberMinMag_67, @function
uprv_decNumberMinMag_67:
.LFB2079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	subq	$16, %rsp
	movzbl	8(%rsi), %edx
	movzbl	8(%r12), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -48(%rbp)
	movl	%edx, %eax
	orl	%ecx, %eax
	testb	$48, %al
	je	.L1931
	testb	$16, %al
	je	.L1969
.L1932:
	movq	%r14, %rsi
	leaq	-48(%rbp), %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-48(%rbp), %esi
	testl	%esi, %esi
	jne	.L1935
.L1940:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1970
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1969:
	.cfi_restore_state
	testb	$48, %dl
	jne	.L1971
.L1933:
	andl	$32, %edx
	jne	.L1972
	movl	$0, -44(%rbp)
.L1937:
	movq	%r14, %r12
.L1938:
	movzbl	8(%r12), %eax
	leaq	-44(%rbp), %r14
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	-48(%rbp), %r9
	leaq	9(%r12), %rdx
	movq	%r14, %r8
	movb	%al, 8(%r13)
	movl	4(%r12), %eax
	movl	%eax, 4(%r13)
	movl	(%r12), %ecx
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%r9, %rcx
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-48(%rbp), %esi
	testl	%esi, %esi
	je	.L1940
	.p2align 4,,10
	.p2align 3
.L1935:
	testb	$-35, %sil
	je	.L1941
	testl	$1073741824, %esi
	je	.L1942
	andl	$-1073741825, %esi
.L1941:
	movq	%r15, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L1931:
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZL10decComparePK9decNumberS1_h
	cmpl	$-2147483648, %eax
	je	.L1973
	movl	$0, -44(%rbp)
	testl	%eax, %eax
	jne	.L1974
	movzbl	8(%r14), %eax
	movzbl	8(%r12), %edx
	movl	%eax, %esi
	movl	%edx, %ecx
	andl	$-128, %esi
	andl	$-128, %ecx
	cmpb	%cl, %sil
	je	.L1936
	testb	%al, %al
	jns	.L1938
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L1971:
	andl	$48, %ecx
	je	.L1933
	jmp	.L1932
	.p2align 4,,10
	.p2align 3
.L1973:
	movl	-48(%rbp), %esi
	orl	$16, %esi
	movl	%esi, -48(%rbp)
	jmp	.L1935
	.p2align 4,,10
	.p2align 3
.L1942:
	movl	$32, %eax
	movq	$1, 0(%r13)
	movw	%ax, 8(%r13)
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1972:
	movl	$0, -44(%rbp)
	jmp	.L1938
	.p2align 4,,10
	.p2align 3
.L1936:
	movl	4(%r14), %ecx
	movl	4(%r12), %esi
	testb	%al, %al
	jns	.L1939
	testb	%dl, %dl
	jns	.L1939
	cmpl	%ecx, %esi
	jg	.L1938
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L1939:
	cmpl	%ecx, %esi
	jl	.L1938
	jmp	.L1937
.L1970:
	call	__stack_chk_fail@PLT
.L1974:
	js	.L1937
	jmp	.L1938
	.cfi_endproc
.LFE2079:
	.size	uprv_decNumberMinMag_67, .-uprv_decNumberMinMag_67
	.p2align 4
	.globl	uprv_decNumberOr_67
	.type	uprv_decNumberOr_67, @function
uprv_decNumberOr_67:
.LFB2084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	4(%rsi), %ecx
	testl	%ecx, %ecx
	jne	.L1976
	movzbl	8(%rsi), %eax
	testb	$112, %al
	jne	.L1976
	testb	%al, %al
	js	.L1976
	movl	4(%rdx), %r14d
	testl	%r14d, %r14d
	jne	.L1976
	movzbl	8(%rdx), %eax
	testb	$112, %al
	jne	.L1976
	testb	%al, %al
	js	.L1976
	movslq	(%rsi), %rax
	leaq	9(%rsi), %r9
	leaq	9(%rdx), %rcx
	leaq	9(%r12), %r13
	cmpl	$49, %eax
	jg	.L1979
	leaq	_ZL8d2utable(%rip), %rsi
	movzbl	(%rsi,%rax), %r10d
	subq	$1, %r10
.L1980:
	movslq	(%rdx), %rax
	addq	%r9, %r10
	cmpl	$49, %eax
	jg	.L1981
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %r11d
	subq	$1, %r11
.L1982:
	movslq	(%rdi), %rax
	addq	%rcx, %r11
	leaq	-1(%r13,%rax), %rbx
	cmpl	$49, %eax
	jg	.L1984
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	leaq	-1(%r13,%rax), %rbx
.L1984:
	cmpq	%rbx, %r13
	ja	.L1998
	addq	$1, %rbx
	movq	%r13, %r8
	jmp	.L1988
	.p2align 4,,10
	.p2align 3
.L2009:
	movzbl	(%r9), %edx
	cmpq	%r11, %rcx
	jbe	.L1997
	movl	%edx, %eax
	xorl	%esi, %esi
.L1987:
	movb	$0, (%r8)
	testb	%al, %al
	jne	.L2007
	.p2align 4,,10
	.p2align 3
.L1995:
	addq	$1, %r8
	addq	$1, %r9
	addq	$1, %rcx
	cmpq	%rbx, %r8
	je	.L2008
.L1988:
	cmpq	%r9, %r10
	jnb	.L2009
	xorl	%edx, %edx
	cmpq	%r11, %rcx
	jbe	.L1997
	movb	$0, (%r8)
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L2007:
	testb	$1, %al
	je	.L1992
	movb	$1, (%r8)
.L1992:
	movzbl	%dl, %r14d
	leal	(%r14,%r14,4), %eax
	leal	(%r14,%rax,8), %eax
	movzbl	%sil, %r14d
	leal	(%rax,%rax,4), %eax
	shrw	$11, %ax
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	subl	%eax, %edx
	leal	(%r14,%r14,4), %eax
	leal	(%r14,%rax,8), %eax
	leal	(%rax,%rax,4), %eax
	shrw	$11, %ax
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	subl	%eax, %esi
	orl	%esi, %edx
	cmpb	$1, %dl
	jbe	.L1995
	.p2align 4,,10
	.p2align 3
.L1976:
	movl	$32, %eax
	movq	$1, (%r12)
	movl	$128, %esi
	movw	%ax, 8(%r12)
	call	uprv_decContextSetStatus_67@PLT
.L2005:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1997:
	.cfi_restore_state
	movzbl	(%rcx), %esi
	movl	%edx, %eax
	orl	%esi, %eax
	jmp	.L1987
	.p2align 4,,10
	.p2align 3
.L1981:
	movslq	%eax, %r11
	subq	$1, %r11
	jmp	.L1982
	.p2align 4,,10
	.p2align 3
.L1979:
	movslq	%eax, %r10
	subq	$1, %r10
	jmp	.L1980
	.p2align 4,,10
	.p2align 3
.L2008:
	subq	%r13, %r8
	movl	%r8d, %r14d
	movslq	%r8d, %r8
	leaq	-1(%r8), %rax
.L1985:
	addq	%rax, %r13
	jc	.L1989
	leaq	8(%r12), %rax
.L1990:
	cmpb	$0, 0(%r13)
	jne	.L1989
	cmpl	$1, %r14d
	je	.L1989
	subq	$1, %r13
	subl	$1, %r14d
	cmpq	%rax, %r13
	jne	.L1990
	.p2align 4,,10
	.p2align 3
.L1989:
	movl	%r14d, (%r12)
	movl	$0, 4(%r12)
	movb	$0, 8(%r12)
	jmp	.L2005
.L1998:
	movq	$-1, %rax
	jmp	.L1985
	.cfi_endproc
.LFE2084:
	.size	uprv_decNumberOr_67, .-uprv_decNumberOr_67
	.p2align 4
	.globl	uprv_decNumberMultiply_67
	.type	uprv_decNumberMultiply_67, @function
uprv_decNumberMultiply_67:
.LFB2086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	leaq	-28(%rbp), %r8
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	movl	-28(%rbp), %esi
	testl	%esi, %esi
	jne	.L2022
.L2011:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2023
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2022:
	.cfi_restore_state
	testb	$-35, %sil
	je	.L2012
	testl	$1073741824, %esi
	je	.L2013
	andl	$-1073741825, %esi
.L2012:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L2011
	.p2align 4,,10
	.p2align 3
.L2013:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L2012
.L2023:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2086:
	.size	uprv_decNumberMultiply_67, .-uprv_decNumberMultiply_67
	.p2align 4
	.globl	uprv_decNumberQuantize_67
	.type	uprv_decNumberQuantize_67, @function
uprv_decNumberQuantize_67:
.LFB2088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movzbl	8(%rsi), %edi
	movl	(%rcx), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rdx), %eax
	movl	$0, -104(%rbp)
	movl	%edi, %ecx
	movl	$0, -100(%rbp)
	orl	%eax, %ecx
	testb	$112, %cl
	je	.L2025
	andl	$48, %ecx
	jne	.L2112
	xorl	%edi, %eax
	testb	$64, %al
	je	.L2028
.L2041:
	movl	$128, -104(%rbp)
	movl	$128, %esi
.L2029:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
.L2070:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
.L2031:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2113
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2025:
	.cfi_restore_state
	movl	4(%rdx), %r14d
	cmpl	$-2147483648, %r14d
	je	.L2041
	movl	8(%r13), %edx
	leal	-1(%rbx), %eax
	subl	%eax, %edx
	cmpl	%r14d, %edx
	jg	.L2041
	leal	2147483646(%r14), %eax
	cmpl	$1, %eax
	jbe	.L2041
	cmpl	4(%r13), %r14d
	jg	.L2041
	movl	(%rsi), %eax
	movzbl	9(%rsi), %edx
	cmpl	$1, %eax
	je	.L2114
.L2042:
	movl	4(%rsi), %edx
	movl	%r14d, %r15d
	subl	%edx, %r15d
	subl	%r15d, %eax
	cmpl	%eax, %ebx
	jl	.L2041
	testl	%r15d, %r15d
	jle	.L2053
	movq	16(%r13), %rdx
	movdqu	0(%r13), %xmm2
	leaq	-96(%rbp), %r15
	leaq	-104(%rbp), %r9
	leaq	-100(%rbp), %r8
	movq	%rdx, -80(%rbp)
	movl	24(%r13), %edx
	movaps	%xmm2, -96(%rbp)
	movb	%dil, 8(%r12)
	movq	%r12, %rdi
	movl	%eax, -96(%rbp)
	movl	4(%rsi), %eax
	movl	%edx, -72(%rbp)
	leaq	9(%rsi), %rdx
	movl	%eax, 4(%r12)
	movl	(%rsi), %ecx
	movq	%r15, %rsi
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	-100(%rbp), %edx
	testl	%edx, %edx
	je	.L2054
	movq	%r9, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZL13decApplyRoundP9decNumberP10decContextiPj.part.0
.L2054:
	movl	4(%r12), %r15d
	movl	(%r12), %r8d
	movl	$0, -100(%rbp)
	cmpl	%r15d, %r14d
	jge	.L2052
	cmpl	%r8d, %ebx
	jne	.L2056
	movl	-104(%rbp), %esi
	andl	$-2081, %esi
	orb	$-128, %sil
	movl	%esi, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L2071:
	testb	$-35, %sil
	je	.L2070
	testl	$1073741824, %esi
	je	.L2029
	andl	$-1073741825, %esi
	jmp	.L2070
	.p2align 4,,10
	.p2align 3
.L2112:
	leaq	-104(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-104(%rbp), %esi
.L2027:
	testl	%esi, %esi
	je	.L2031
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2028:
	cmpq	%rsi, %r12
	je	.L2031
	movb	%dil, 8(%r12)
	movl	4(%rsi), %eax
	movl	%eax, 4(%r12)
	movl	(%rsi), %eax
	movl	%eax, (%r12)
	movzbl	9(%rsi), %eax
	movb	%al, 9(%r12)
	movl	(%rsi), %eax
	cmpl	$1, %eax
	jle	.L2031
	leaq	10(%r12), %r8
	leaq	9(%rsi), %rcx
	movslq	%eax, %rdx
	cmpl	$49, %eax
	jg	.L2034
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdx), %edx
.L2034:
	addq	%rdx, %rcx
	leaq	10(%rsi), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2031
	movq	%rcx, %rax
	subq	%rsi, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2035
	leaq	25(%r12), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L2035
	movq	%rcx, %rax
	subq	%rsi, %rax
	leaq	-10(%rax), %r9
	movl	$10, %eax
	movq	%r9, %rdi
	andq	$-16, %rdi
	addq	$10, %rdi
	.p2align 4,,10
	.p2align 3
.L2037:
	movdqu	(%rsi,%rax), %xmm0
	movups	%xmm0, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L2037
	movq	%r9, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%r8,%rsi), %rdx
	cmpq	%r9, %rsi
	je	.L2031
	movzbl	(%rax), %esi
	movb	%sil, (%rdx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2031
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2031
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2031
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2031
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2031
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2031
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2031
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2031
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2031
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2031
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2031
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2031
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2031
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2031
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2114:
	movl	%edi, %ecx
	andl	$112, %ecx
	orb	%dl, %cl
	jne	.L2042
	cmpq	%rsi, %r12
	je	.L2043
	movb	%dil, 8(%r12)
	movl	4(%rsi), %eax
	movl	%eax, 4(%r12)
	movl	(%rsi), %r8d
	movl	%r8d, (%r12)
	movzbl	9(%rsi), %eax
	movb	%al, 9(%r12)
	movl	(%rsi), %eax
	cmpl	$1, %eax
	jle	.L2044
	leaq	10(%r12), %rdi
	leaq	9(%rsi), %rcx
	movslq	%eax, %rdx
	cmpl	$49, %eax
	jg	.L2046
	movq	%rdx, %rax
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %edx
.L2046:
	addq	%rdx, %rcx
	leaq	10(%rsi), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2044
	movq	%rcx, %rax
	subq	%rsi, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2047
	leaq	25(%r12), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L2047
	movq	%rcx, %rax
	subq	%rsi, %rax
	leaq	-10(%rax), %r9
	movl	$10, %eax
	movq	%r9, %r8
	andq	$-16, %r8
	addq	$10, %r8
.L2048:
	movdqu	(%rsi,%rax), %xmm3
	movups	%xmm3, (%r12,%rax)
	addq	$16, %rax
	cmpq	%r8, %rax
	jne	.L2048
	movq	%r9, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%rdi,%rsi), %rdx
	cmpq	%rsi, %r9
	je	.L2050
	movzbl	(%rax), %esi
	movb	%sil, (%rdx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2050
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2050
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2050
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2050
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2050
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2050
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2050
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2050
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2050
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2050
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2050
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2050
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2050
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2050
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
.L2050:
	movl	(%r12), %r8d
.L2044:
	movl	%r14d, 4(%r12)
	movl	%r14d, %r15d
	.p2align 4,,10
	.p2align 3
.L2052:
	movl	4(%r13), %eax
	subl	%r8d, %eax
	addl	$1, %eax
	cmpl	%r15d, %eax
	jge	.L2069
	movl	-104(%rbp), %esi
	orb	$-128, %sil
	movl	%esi, -104(%rbp)
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2053:
	cmpq	%rsi, %r12
	je	.L2058
	movb	%dil, 8(%r12)
	movl	4(%rsi), %eax
	movl	%eax, 4(%r12)
	movl	(%rsi), %r8d
	movl	%r8d, (%r12)
	movzbl	9(%rsi), %eax
	movb	%al, 9(%r12)
	movl	(%rsi), %ecx
	cmpl	$1, %ecx
	jle	.L2059
	leaq	10(%r12), %r9
	leaq	9(%rsi), %rdi
	movslq	%ecx, %rax
	cmpl	$49, %ecx
	jg	.L2061
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rax), %eax
.L2061:
	addq	%rax, %rdi
	leaq	10(%rsi), %rcx
	cmpq	%rcx, %rdi
	jbe	.L2059
	movq	%rdi, %rax
	subq	%rsi, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2062
	leaq	25(%r12), %rax
	subq	%rcx, %rax
	cmpq	$30, %rax
	jbe	.L2062
	movq	%rdi, %rax
	subq	%rsi, %rax
	leaq	-10(%rax), %r10
	movl	$10, %eax
	movq	%r10, %r8
	andq	$-16, %r8
	addq	$10, %r8
	.p2align 4,,10
	.p2align 3
.L2063:
	movdqu	(%rsi,%rax), %xmm1
	movups	%xmm1, (%r12,%rax)
	addq	$16, %rax
	cmpq	%r8, %rax
	jne	.L2063
	movq	%r10, %rax
	andq	$-16, %rax
	addq	%rax, %rcx
	addq	%rax, %r9
	cmpq	%rax, %r10
	je	.L2058
	movzbl	(%rcx), %eax
	movb	%al, (%r9)
	leaq	1(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2058
	movzbl	1(%rcx), %eax
	movb	%al, 1(%r9)
	leaq	2(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2058
	movzbl	2(%rcx), %eax
	movb	%al, 2(%r9)
	leaq	3(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2058
	movzbl	3(%rcx), %eax
	movb	%al, 3(%r9)
	leaq	4(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2058
	movzbl	4(%rcx), %eax
	movb	%al, 4(%r9)
	leaq	5(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2058
	movzbl	5(%rcx), %eax
	movb	%al, 5(%r9)
	leaq	6(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2058
	movzbl	6(%rcx), %eax
	movb	%al, 6(%r9)
	leaq	7(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2058
	movzbl	7(%rcx), %eax
	movb	%al, 7(%r9)
	leaq	8(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2058
	movzbl	8(%rcx), %eax
	movb	%al, 8(%r9)
	leaq	9(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2058
	movzbl	9(%rcx), %eax
	movb	%al, 9(%r9)
	leaq	10(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2058
	movzbl	10(%rcx), %eax
	movb	%al, 10(%r9)
	leaq	11(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2058
	movzbl	11(%rcx), %eax
	movb	%al, 11(%r9)
	leaq	12(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2058
	movzbl	12(%rcx), %eax
	movb	%al, 12(%r9)
	leaq	13(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2058
	movzbl	13(%rcx), %eax
	movb	%al, 13(%r9)
	leaq	14(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2058
	movzbl	14(%rcx), %eax
	movb	%al, 14(%r9)
.L2058:
	movl	(%r12), %r8d
.L2059:
	testl	%r15d, %r15d
	jne	.L2067
	movl	4(%r12), %r15d
	jmp	.L2052
.L2035:
	subq	%rsi, %rcx
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L2040:
	movzbl	(%rsi,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L2040
	jmp	.L2031
.L2069:
	movq	%r13, %rsi
	leaq	-104(%rbp), %rcx
	leaq	-100(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-104(%rbp), %esi
	andl	$-8193, %esi
	movl	%esi, -104(%rbp)
	jmp	.L2027
.L2067:
	subl	%r14d, %edx
	je	.L2068
	movl	%r8d, %esi
	leaq	9(%r12), %rdi
	call	_ZL14decShiftToMostPhii.part.0
	movl	%eax, %r8d
.L2068:
	addl	4(%r12), %r15d
	movl	%r8d, (%r12)
	movl	%r15d, 4(%r12)
	jmp	.L2052
.L2056:
	movl	%r8d, %esi
	leaq	9(%r12), %rdi
	movl	$1, %edx
	call	_ZL14decShiftToMostPhii.part.0
	movl	%eax, (%r12)
	movl	%eax, %r8d
	movl	4(%r12), %eax
	leal	-1(%rax), %r15d
	movl	%r15d, 4(%r12)
	jmp	.L2052
.L2062:
	movq	%rdi, %rax
	movl	$10, %ecx
	subq	%rsi, %rax
.L2066:
	movzbl	(%rsi,%rcx), %edi
	movb	%dil, (%r12,%rcx)
	addq	$1, %rcx
	cmpq	%rax, %rcx
	jne	.L2066
	jmp	.L2058
.L2043:
	movl	$1, %r8d
	jmp	.L2044
.L2113:
	call	__stack_chk_fail@PLT
.L2047:
	subq	%rsi, %rcx
	movl	$10, %eax
.L2051:
	movzbl	(%rsi,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L2051
	jmp	.L2050
	.cfi_endproc
.LFE2088:
	.size	uprv_decNumberQuantize_67, .-uprv_decNumberQuantize_67
	.p2align 4
	.globl	uprv_decNumberNormalize_67
	.type	uprv_decNumberNormalize_67, @function
uprv_decNumberNormalize_67:
.LFB2089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rsi), %eax
	movl	$0, -64(%rbp)
	movl	$0, -60(%rbp)
	testb	$48, %al
	je	.L2116
	movq	%rdx, %rcx
	leaq	-64(%rbp), %r8
	xorl	%edx, %edx
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
.L2117:
	movl	-64(%rbp), %esi
	testl	%esi, %esi
	jne	.L2155
.L2133:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2156
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2116:
	.cfi_restore_state
	movb	%al, 8(%rdi)
	movl	4(%rsi), %eax
	leaq	-64(%rbp), %r14
	leaq	-60(%rbp), %r15
	leaq	9(%rsi), %rdx
	movq	%r14, %r9
	movq	%r15, %r8
	movl	%eax, 4(%rdi)
	movl	(%rsi), %ecx
	movq	%r13, %rsi
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	8(%r13), %edx
	movl	%edx, %eax
	subl	(%r12), %eax
	addl	$1, %eax
	cmpl	4(%r12), %eax
	jge	.L2157
	movl	-60(%rbp), %edx
.L2123:
	testl	%edx, %edx
	jne	.L2158
.L2125:
	movl	4(%r13), %ecx
	movl	4(%r12), %edi
	movl	%ecx, %edx
	subl	0(%r13), %edx
	addl	$1, %edx
	cmpl	%edx, %edi
	jle	.L2154
	movl	(%r12), %esi
	subl	%esi, %ecx
	addl	$1, %ecx
	cmpl	%ecx, %edi
	jg	.L2159
	cmpb	$0, 24(%r13)
	je	.L2154
	subl	%edx, %edi
	cmpb	$0, 9(%r12)
	movl	%edi, %r14d
	jne	.L2129
	cmpl	$1, %esi
	je	.L2160
.L2129:
	testl	%r14d, %r14d
	je	.L2131
	movl	%r14d, %edx
	leaq	9(%r12), %rdi
	call	_ZL14decShiftToMostPhii.part.0
	movl	4(%r12), %edx
	movl	%eax, %esi
	subl	%r14d, %edx
.L2131:
	movzbl	8(%r12), %eax
	orl	$1024, -64(%rbp)
	movl	%esi, (%r12)
	movl	%edx, 4(%r12)
	andl	$112, %eax
	jmp	.L2124
	.p2align 4,,10
	.p2align 3
.L2157:
	jg	.L2153
	movl	%edx, -48(%rbp)
	movl	$256, %ecx
	leaq	-52(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	movl	$1, -52(%rbp)
	movw	%cx, -44(%rbp)
	call	_ZL10decComparePK9decNumberS1_h
	cmpl	$-2147483648, %eax
	je	.L2161
	movl	-60(%rbp), %edx
	testl	%eax, %eax
	jne	.L2123
	testl	%edx, %edx
	jns	.L2123
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL13decApplyRoundP9decNumberP10decContextiPj.part.0
	.p2align 4,,10
	.p2align 3
.L2153:
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL15decSetSubnormalP9decNumberP10decContextPiPj
.L2154:
	movzbl	8(%r12), %eax
	andl	$112, %eax
.L2124:
	movl	$0, -56(%rbp)
	testb	%al, %al
	jne	.L2117
.L2130:
	movzbl	9(%r12), %eax
	testb	$1, %al
	jne	.L2117
	testb	%al, %al
	jne	.L2132
	cmpl	$1, (%r12)
	je	.L2162
.L2132:
	movq	%r13, %rsi
	leaq	-56(%rbp), %r8
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZL7decTrimP9decNumberP10decContexthhPi.part.0
	movl	-64(%rbp), %esi
	testl	%esi, %esi
	je	.L2133
	.p2align 4,,10
	.p2align 3
.L2155:
	testb	$-35, %sil
	je	.L2134
	testl	$1073741824, %esi
	je	.L2135
	andl	$-1073741825, %esi
.L2134:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L2133
	.p2align 4,,10
	.p2align 3
.L2158:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL13decApplyRoundP9decNumberP10decContextiPj.part.0
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2135:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L2134
	.p2align 4,,10
	.p2align 3
.L2159:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL14decSetOverflowP9decNumberP10decContextPj
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2162:
	movl	$0, 4(%r12)
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L2161:
	orl	$16, -64(%rbp)
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2160:
	testb	$112, 8(%r12)
	jne	.L2129
	orl	$1024, -64(%rbp)
	movl	%edx, 4(%r12)
	movl	$0, -56(%rbp)
	jmp	.L2130
.L2156:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2089:
	.size	uprv_decNumberNormalize_67, .-uprv_decNumberNormalize_67
	.p2align 4
	.globl	uprv_decNumberReduce_67
	.type	uprv_decNumberReduce_67, @function
uprv_decNumberReduce_67:
.LFB2090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rsi), %eax
	movl	$0, -52(%rbp)
	movl	$0, -48(%rbp)
	testb	$48, %al
	je	.L2164
	movq	%rdx, %rcx
	leaq	-52(%rbp), %r8
	xorl	%edx, %edx
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
.L2165:
	movl	-52(%rbp), %esi
	testl	%esi, %esi
	jne	.L2178
.L2167:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2179
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2164:
	.cfi_restore_state
	movb	%al, 8(%rdi)
	movl	4(%rsi), %eax
	leaq	-48(%rbp), %r14
	leaq	-52(%rbp), %r9
	leaq	9(%rsi), %rdx
	movq	%r14, %r8
	movl	%eax, 4(%rdi)
	movl	(%rsi), %ecx
	movq	%r13, %rsi
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, %rcx
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	$0, -44(%rbp)
	testb	$112, 8(%r12)
	jne	.L2165
	movzbl	9(%r12), %eax
	testb	$1, %al
	jne	.L2165
	testb	%al, %al
	jne	.L2166
	cmpl	$1, (%r12)
	je	.L2180
.L2166:
	movq	%r13, %rsi
	leaq	-44(%rbp), %r8
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZL7decTrimP9decNumberP10decContexthhPi.part.0
	movl	-52(%rbp), %esi
	testl	%esi, %esi
	je	.L2167
	.p2align 4,,10
	.p2align 3
.L2178:
	testb	$-35, %sil
	je	.L2168
	testl	$1073741824, %esi
	je	.L2169
	andl	$-1073741825, %esi
.L2168:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2169:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L2168
	.p2align 4,,10
	.p2align 3
.L2180:
	movl	$0, 4(%r12)
	jmp	.L2165
.L2179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2090:
	.size	uprv_decNumberReduce_67, .-uprv_decNumberReduce_67
	.p2align 4
	.globl	uprv_decNumberRescale_67
	.type	uprv_decNumberRescale_67, @function
uprv_decNumberRescale_67:
.LFB2091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r15d
	movzbl	8(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rdx), %eax
	movl	$0, -104(%rbp)
	movl	%ecx, %edx
	movl	$0, -100(%rbp)
	orl	%eax, %edx
	testb	$112, %dl
	je	.L2182
	andl	$48, %edx
	jne	.L2269
	xorl	%ecx, %eax
	testb	$64, %al
	je	.L2185
.L2198:
	movl	$128, -104(%rbp)
	movl	$128, %esi
.L2186:
	movl	$32, %eax
	movq	$1, 0(%r13)
	movw	%ax, 8(%r13)
.L2227:
	movq	%r14, %rdi
	call	uprv_decContextSetStatus_67@PLT
.L2188:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2270
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2182:
	.cfi_restore_state
	movb	%cl, -120(%rbp)
	call	_ZL9decGetIntPK9decNumber
	movl	%eax, %ebx
	cmpl	$-2147483648, %eax
	je	.L2198
	movl	8(%r14), %edi
	leal	-1(%r15), %eax
	subl	%eax, %edi
	cmpl	%ebx, %edi
	jg	.L2198
	leal	2147483646(%rbx), %eax
	cmpl	$1, %eax
	jbe	.L2198
	cmpl	4(%r14), %ebx
	jg	.L2198
	movl	(%r12), %eax
	movzbl	9(%r12), %edx
	movzbl	-120(%rbp), %ecx
	cmpl	$1, %eax
	je	.L2271
.L2199:
	movl	4(%r12), %edx
	movl	%ebx, %r8d
	subl	%edx, %r8d
	subl	%r8d, %eax
	cmpl	%eax, %r15d
	jl	.L2198
	testl	%r8d, %r8d
	jle	.L2210
	movq	16(%r14), %rdx
	movdqu	(%r14), %xmm2
	leaq	-96(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-104(%rbp), %r9
	leaq	-100(%rbp), %r8
	movq	%rsi, -120(%rbp)
	movq	%rdx, -80(%rbp)
	movl	24(%r14), %edx
	movaps	%xmm2, -96(%rbp)
	movb	%cl, 8(%r13)
	movl	%eax, -96(%rbp)
	movl	4(%r12), %eax
	movl	%edx, -72(%rbp)
	leaq	9(%r12), %rdx
	movl	%eax, 4(%r13)
	movl	(%r12), %ecx
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	-100(%rbp), %edx
	testl	%edx, %edx
	je	.L2211
	movq	-120(%rbp), %rsi
	movq	%r9, %rcx
	movq	%r13, %rdi
	call	_ZL13decApplyRoundP9decNumberP10decContextiPj.part.0
.L2211:
	movl	4(%r13), %r8d
	movl	0(%r13), %esi
	movl	$0, -100(%rbp)
	cmpl	%r8d, %ebx
	jge	.L2209
	cmpl	%esi, %r15d
	jne	.L2213
	movl	-104(%rbp), %esi
	andl	$-2081, %esi
	orb	$-128, %sil
	movl	%esi, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L2228:
	testb	$-35, %sil
	je	.L2227
	testl	$1073741824, %esi
	je	.L2186
	andl	$-1073741825, %esi
	jmp	.L2227
	.p2align 4,,10
	.p2align 3
.L2269:
	movq	%rdi, %rdx
	leaq	-104(%rbp), %r8
	movq	%r14, %rcx
	movq	%r13, %rdi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-104(%rbp), %esi
.L2184:
	testl	%esi, %esi
	je	.L2188
	jmp	.L2228
	.p2align 4,,10
	.p2align 3
.L2185:
	cmpq	%rsi, %r13
	je	.L2188
	movb	%cl, 8(%r13)
	movl	4(%rsi), %eax
	movl	%eax, 4(%r13)
	movl	(%rsi), %eax
	movl	%eax, 0(%r13)
	movzbl	9(%rsi), %eax
	movb	%al, 9(%r13)
	movl	(%rsi), %eax
	cmpl	$1, %eax
	jle	.L2188
	leaq	10(%r13), %rdi
	leaq	9(%rsi), %rcx
	movslq	%eax, %rdx
	cmpl	$49, %eax
	jg	.L2191
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdx), %edx
.L2191:
	addq	%rdx, %rcx
	leaq	10(%r12), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2188
	movq	%rcx, %rax
	subq	%r12, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2192
	leaq	25(%r13), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L2192
	movq	%rcx, %rax
	subq	%r12, %rax
	leaq	-10(%rax), %r8
	movl	$10, %eax
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	$10, %rsi
	.p2align 4,,10
	.p2align 3
.L2194:
	movdqu	(%r12,%rax), %xmm0
	movups	%xmm0, 0(%r13,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L2194
	movq	%r8, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%rdi,%rsi), %rdx
	cmpq	%r8, %rsi
	je	.L2188
	movzbl	(%rax), %esi
	movb	%sil, (%rdx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2188
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2188
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2188
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2188
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2188
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2188
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2188
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2188
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2188
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2188
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2188
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2188
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2188
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2188
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2271:
	movl	%ecx, %esi
	andl	$112, %esi
	orb	%dl, %sil
	jne	.L2199
	cmpq	%r12, %r13
	je	.L2200
	movb	%cl, 8(%r13)
	movl	4(%r12), %eax
	movl	%eax, 4(%r13)
	movl	(%r12), %esi
	movl	%esi, 0(%r13)
	movzbl	9(%r12), %eax
	movb	%al, 9(%r13)
	movl	(%r12), %eax
	cmpl	$1, %eax
	jle	.L2201
	leaq	10(%r13), %rdi
	leaq	9(%r12), %rcx
	movslq	%eax, %rdx
	cmpl	$49, %eax
	jg	.L2203
	movq	%rdx, %rax
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %edx
.L2203:
	addq	%rdx, %rcx
	leaq	10(%r12), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2201
	movq	%rcx, %rax
	subq	%r12, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2204
	leaq	25(%r13), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L2204
	movq	%rcx, %rax
	subq	%r12, %rax
	leaq	-10(%rax), %r8
	movl	$10, %eax
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	$10, %rsi
.L2205:
	movdqu	(%r12,%rax), %xmm3
	movups	%xmm3, 0(%r13,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L2205
	movq	%r8, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%rdi,%rsi), %rdx
	cmpq	%rsi, %r8
	je	.L2207
	movzbl	(%rax), %esi
	movb	%sil, (%rdx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2207
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2207
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2207
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2207
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2207
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2207
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2207
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2207
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2207
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2207
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2207
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2207
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2207
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2207
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
.L2207:
	movl	0(%r13), %esi
.L2201:
	movl	%ebx, 4(%r13)
	movl	%ebx, %r8d
	.p2align 4,,10
	.p2align 3
.L2209:
	movl	4(%r14), %eax
	subl	%esi, %eax
	addl	$1, %eax
	cmpl	%r8d, %eax
	jge	.L2226
	movl	-104(%rbp), %esi
	orb	$-128, %sil
	movl	%esi, -104(%rbp)
	jmp	.L2228
	.p2align 4,,10
	.p2align 3
.L2210:
	cmpq	%r12, %r13
	je	.L2215
	movb	%cl, 8(%r13)
	movl	4(%r12), %eax
	movl	%eax, 4(%r13)
	movl	(%r12), %esi
	movl	%esi, 0(%r13)
	movzbl	9(%r12), %eax
	movb	%al, 9(%r13)
	movl	(%r12), %ecx
	cmpl	$1, %ecx
	jle	.L2216
	leaq	10(%r13), %r9
	leaq	9(%r12), %rdi
	movslq	%ecx, %rax
	cmpl	$49, %ecx
	jg	.L2218
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rax), %eax
.L2218:
	addq	%rax, %rdi
	leaq	10(%r12), %rcx
	cmpq	%rcx, %rdi
	jbe	.L2216
	movq	%rdi, %rax
	subq	%r12, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2219
	leaq	25(%r13), %rax
	subq	%rcx, %rax
	cmpq	$30, %rax
	jbe	.L2219
	movq	%rdi, %rax
	subq	%r12, %rax
	leaq	-10(%rax), %r10
	movl	$10, %eax
	movq	%r10, %rsi
	andq	$-16, %rsi
	addq	$10, %rsi
	.p2align 4,,10
	.p2align 3
.L2220:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, 0(%r13,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L2220
	movq	%r10, %rax
	andq	$-16, %rax
	addq	%rax, %rcx
	addq	%rax, %r9
	cmpq	%rax, %r10
	je	.L2215
	movzbl	(%rcx), %eax
	movb	%al, (%r9)
	leaq	1(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2215
	movzbl	1(%rcx), %eax
	movb	%al, 1(%r9)
	leaq	2(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2215
	movzbl	2(%rcx), %eax
	movb	%al, 2(%r9)
	leaq	3(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2215
	movzbl	3(%rcx), %eax
	movb	%al, 3(%r9)
	leaq	4(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2215
	movzbl	4(%rcx), %eax
	movb	%al, 4(%r9)
	leaq	5(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2215
	movzbl	5(%rcx), %eax
	movb	%al, 5(%r9)
	leaq	6(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2215
	movzbl	6(%rcx), %eax
	movb	%al, 6(%r9)
	leaq	7(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2215
	movzbl	7(%rcx), %eax
	movb	%al, 7(%r9)
	leaq	8(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2215
	movzbl	8(%rcx), %eax
	movb	%al, 8(%r9)
	leaq	9(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2215
	movzbl	9(%rcx), %eax
	movb	%al, 9(%r9)
	leaq	10(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2215
	movzbl	10(%rcx), %eax
	movb	%al, 10(%r9)
	leaq	11(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2215
	movzbl	11(%rcx), %eax
	movb	%al, 11(%r9)
	leaq	12(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2215
	movzbl	12(%rcx), %eax
	movb	%al, 12(%r9)
	leaq	13(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2215
	movzbl	13(%rcx), %eax
	movb	%al, 13(%r9)
	leaq	14(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L2215
	movzbl	14(%rcx), %eax
	movb	%al, 14(%r9)
.L2215:
	movl	0(%r13), %esi
.L2216:
	testl	%r8d, %r8d
	jne	.L2224
	movl	4(%r13), %r8d
	jmp	.L2209
.L2192:
	subq	%r12, %rcx
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L2197:
	movzbl	(%r12,%rax), %edx
	movb	%dl, 0(%r13,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L2197
	jmp	.L2188
.L2226:
	movq	%r14, %rsi
	leaq	-104(%rbp), %rcx
	leaq	-100(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-104(%rbp), %esi
	andl	$-8193, %esi
	movl	%esi, -104(%rbp)
	jmp	.L2184
.L2224:
	subl	%ebx, %edx
	je	.L2225
	leaq	9(%r13), %rdi
	movl	%r8d, -120(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	movl	-120(%rbp), %r8d
	movl	%eax, %esi
.L2225:
	addl	4(%r13), %r8d
	movl	%esi, 0(%r13)
	movl	%r8d, 4(%r13)
	jmp	.L2209
.L2213:
	leaq	9(%r13), %rdi
	movl	$1, %edx
	call	_ZL14decShiftToMostPhii.part.0
	movl	%eax, 0(%r13)
	movl	%eax, %esi
	movl	4(%r13), %eax
	leal	-1(%rax), %r8d
	movl	%r8d, 4(%r13)
	jmp	.L2209
.L2219:
	movq	%rdi, %rax
	movl	$10, %ecx
	subq	%r12, %rax
.L2223:
	movzbl	(%r12,%rcx), %esi
	movb	%sil, 0(%r13,%rcx)
	addq	$1, %rcx
	cmpq	%rax, %rcx
	jne	.L2223
	jmp	.L2215
.L2200:
	movl	$1, %esi
	jmp	.L2201
.L2270:
	call	__stack_chk_fail@PLT
.L2204:
	subq	%r12, %rcx
	movl	$10, %eax
.L2208:
	movzbl	(%r12,%rax), %edx
	movb	%dl, 0(%r13,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L2208
	jmp	.L2207
	.cfi_endproc
.LFE2091:
	.size	uprv_decNumberRescale_67, .-uprv_decNumberRescale_67
	.p2align 4
	.globl	uprv_decNumberRotate_67
	.type	uprv_decNumberRotate_67, @function
uprv_decNumberRotate_67:
.LFB2094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movzbl	8(%rsi), %r13d
	testb	$48, %r13b
	jne	.L2273
	movzbl	8(%rdx), %eax
	testb	$48, %al
	je	.L2386
	movl	$1073741952, %r11d
	testb	$16, %r13b
	je	.L2331
.L2276:
	movl	(%r14), %eax
	cmpl	%eax, (%rbx)
	jle	.L2387
.L2277:
	movb	%r13b, 8(%r12)
	movslq	(%r14), %rsi
	leaq	9(%r12), %rdx
	leaq	9(%rbx), %r8
	movq	%rdx, %rax
	movq	%r8, %rdi
	cmpl	$49, %esi
	jg	.L2287
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rsi), %ecx
	movq	%rcx, %rsi
	addq	%rdx, %rcx
	cmpq	%rdx, %rcx
	jbe	.L2290
.L2289:
	leaq	25(%rbx), %r9
	leaq	10(%r12), %rsi
	cmpq	%rdx, %r9
	leaq	25(%r12), %r9
	setbe	%r10b
	cmpq	%r9, %r8
	setnb	%r9b
	orb	%r9b, %r10b
	je	.L2370
	movq	%rcx, %r9
	subq	%r12, %r9
	subq	$10, %r9
	cmpq	$14, %r9
	seta	%r10b
	cmpq	%rcx, %rsi
	setbe	%r9b
	testb	%r9b, %r10b
	je	.L2370
	movq	%rcx, %rax
	subq	%r12, %rax
	cmpq	%rcx, %rsi
	leaq	-9(%rax), %rdi
	movl	$1, %eax
	cmova	%rax, %rdi
	movl	$9, %eax
	movq	%rdi, %rsi
	andq	$-16, %rsi
	addq	$9, %rsi
	.p2align 4,,10
	.p2align 3
.L2292:
	movdqu	(%rbx,%rax), %xmm0
	movups	%xmm0, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L2292
	movq	%rdi, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%r8,%rsi), %rdx
	cmpq	%rsi, %rdi
	je	.L2294
	movzbl	(%rdx), %esi
	movb	%sil, (%rax)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2294
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rax)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2294
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rax)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2294
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rax)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2294
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rax)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2294
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rax)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2294
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rax)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2294
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rax)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2294
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rax)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2294
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rax)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2294
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rax)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2294
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rax)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2294
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rax)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2294
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rax)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2294
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L2294:
	movslq	(%r14), %rsi
	cmpl	$49, %esi
	jle	.L2388
.L2290:
	movl	%esi, (%r12)
	movl	(%r14), %eax
	cmpl	%eax, %esi
	jg	.L2295
.L2385:
	movzbl	8(%r12), %r13d
.L2279:
	movl	$0, 4(%r12)
	andl	$-17, %r13d
	orl	$32, %r13d
	movb	%r13b, 8(%r12)
	testl	%r11d, %r11d
	je	.L2355
	testb	$-35, %r11b
	jne	.L2389
.L2330:
	movl	%r11d, %esi
	movq	%r14, %rdi
	call	uprv_decContextSetStatus_67@PLT
.L2355:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2386:
	.cfi_restore_state
	testb	$64, %al
	jne	.L2339
	movl	4(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L2339
	movq	%rdx, %rdi
	call	_ZL9decGetIntPK9decNumber
	movl	%eax, %r15d
	leal	2147483646(%rax), %eax
	cmpl	$1, %eax
	jbe	.L2339
	cmpl	$-2147483648, %r15d
	jne	.L2390
	.p2align 4,,10
	.p2align 3
.L2339:
	movl	$128, %r11d
.L2298:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L2330
	.p2align 4,,10
	.p2align 3
.L2370:
	movzbl	(%rdi), %edx
	addq	$1, %rax
	addq	$1, %rdi
	movb	%dl, -1(%rax)
	cmpq	%rcx, %rax
	jb	.L2370
	jmp	.L2294
	.p2align 4,,10
	.p2align 3
.L2273:
	movl	$1073741952, %r11d
	testb	$16, %r13b
	jne	.L2276
	xorl	%r11d, %r11d
	testq	%rdx, %rdx
	je	.L2276
	movzbl	8(%rdx), %eax
.L2331:
	testb	$16, %al
	je	.L2391
	movq	%rdx, %rbx
	movl	%eax, %r13d
	movl	$1073741952, %r11d
	movl	(%r14), %eax
	cmpl	%eax, (%rbx)
	jg	.L2277
	.p2align 4,,10
	.p2align 3
.L2387:
	cmpq	%rbx, %r12
	je	.L2385
	movb	%r13b, 8(%r12)
	movl	4(%rbx), %eax
	movl	%eax, 4(%r12)
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	movzbl	9(%rbx), %eax
	movb	%al, 9(%r12)
	movl	(%rbx), %eax
	cmpl	$1, %eax
	jle	.L2279
	leaq	10(%r12), %rdi
	leaq	9(%rbx), %rcx
	movslq	%eax, %rdx
	cmpl	$49, %eax
	jg	.L2281
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdx), %edx
.L2281:
	addq	%rdx, %rcx
	leaq	10(%rbx), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2279
	movq	%rcx, %rax
	subq	%rbx, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2282
	leaq	25(%r12), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L2282
	movq	%rcx, %rax
	subq	%rbx, %rax
	leaq	-10(%rax), %r8
	movl	$10, %eax
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	$10, %rsi
	.p2align 4,,10
	.p2align 3
.L2283:
	movdqu	(%rbx,%rax), %xmm1
	movups	%xmm1, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L2283
	movq	%r8, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%rdi,%rsi), %rdx
	cmpq	%r8, %rsi
	je	.L2385
	movzbl	(%rax), %esi
	movb	%sil, (%rdx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2385
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2385
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2385
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2385
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2385
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2385
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2385
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2385
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2385
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2385
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2385
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2385
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2385
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2385
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
	jmp	.L2385
	.p2align 4,,10
	.p2align 3
.L2287:
	movslq	%esi, %rcx
	addq	%rdx, %rcx
	cmpq	%rdx, %rcx
	ja	.L2289
	jmp	.L2290
	.p2align 4,,10
	.p2align 3
.L2388:
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rsi), %esi
	movl	%esi, (%r12)
	movl	(%r14), %eax
	cmpl	%eax, %esi
	jle	.L2385
.L2295:
	subl	%eax, %esi
	movq	%r12, %rdi
	call	_ZL8decDecapP9decNumberi
	jmp	.L2385
	.p2align 4,,10
	.p2align 3
.L2391:
	movl	%r13d, %ecx
	xorl	%r11d, %r11d
	andl	$32, %ecx
	cmove	%eax, %r13d
	cmove	%rdx, %rbx
	jmp	.L2276
.L2389:
	testl	$1073741824, %r11d
	je	.L2298
	andl	$-1073741825, %r11d
	jmp	.L2330
	.p2align 4,,10
	.p2align 3
.L2390:
	movl	%r15d, %edx
	movl	$128, %r11d
	sarl	$31, %edx
	movl	%edx, %eax
	xorl	%r15d, %eax
	subl	%edx, %eax
	cmpl	(%r14), %eax
	jg	.L2298
	cmpq	%r12, %rbx
	je	.L2302
	movb	%r13b, 8(%r12)
	movl	4(%rbx), %eax
	movl	%eax, 4(%r12)
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	movzbl	9(%rbx), %eax
	movb	%al, 9(%r12)
	movl	(%rbx), %edx
	cmpl	$1, %edx
	jle	.L2302
	leaq	10(%r12), %rdi
	leaq	9(%rbx), %rcx
	movslq	%edx, %rax
	cmpl	$49, %edx
	jg	.L2304
	leaq	_ZL8d2utable(%rip), %rsi
	movzbl	(%rsi,%rax), %eax
.L2304:
	addq	%rax, %rcx
	leaq	10(%rbx), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2302
	movq	%rcx, %rax
	subq	%rbx, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2305
	leaq	25(%rbx), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L2305
	movq	%rcx, %rax
	subq	%rbx, %rax
	leaq	-10(%rax), %r8
	movl	$10, %eax
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	$10, %rsi
	.p2align 4,,10
	.p2align 3
.L2307:
	movdqu	(%rbx,%rax), %xmm2
	movups	%xmm2, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L2307
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	%rsi, %rdx
	leaq	(%rdi,%rsi), %rax
	cmpq	%rsi, %r8
	je	.L2302
	movzbl	(%rdx), %esi
	movb	%sil, (%rax)
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2302
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rax)
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2302
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rax)
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2302
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rax)
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2302
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rax)
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2302
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rax)
	leaq	6(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2302
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rax)
	leaq	7(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2302
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rax)
	leaq	8(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2302
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rax)
	leaq	9(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2302
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rax)
	leaq	10(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2302
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rax)
	leaq	11(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2302
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rax)
	leaq	12(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2302
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rax)
	leaq	13(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2302
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rax)
	leaq	14(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2302
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
.L2302:
	testl	%r15d, %r15d
	jns	.L2301
	addl	(%r14), %r15d
.L2301:
	testl	%r15d, %r15d
	je	.L2355
	movl	(%r14), %eax
	cmpl	%r15d, %eax
	je	.L2355
	testb	$64, 8(%r12)
	jne	.L2355
	movslq	(%r12), %rdx
	leaq	9(%r12), %r13
	cmpl	$49, %edx
	jg	.L2312
	leaq	_ZL8d2utable(%rip), %rsi
	movzbl	(%rsi,%rdx), %edx
	subq	$1, %rdx
.L2313:
	leaq	1(%r13,%rdx), %rdi
	cmpl	$49, %eax
	jg	.L2314
	leaq	_ZL8d2utable(%rip), %rsi
	movslq	%eax, %rdx
	movzbl	(%rsi,%rdx), %ecx
	subq	$1, %rcx
	leaq	0(%r13,%rcx), %rbx
	cmpq	%rdi, %rbx
	jb	.L2392
.L2316:
	movl	$1, %eax
	movq	%rcx, -56(%rbp)
	movq	%rax, %rdx
	subq	%rdi, %rdx
	addq	%rbx, %rdx
	cmpq	%rdi, %rbx
	cmovb	%rax, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movl	(%r14), %eax
	movq	-56(%rbp), %rcx
	cmpl	$49, %eax
	movl	%eax, (%r12)
	jg	.L2318
	movl	%eax, %r8d
	subl	%r15d, %r8d
	movl	%r8d, %r15d
	jne	.L2393
.L2319:
	leal	1(%rcx), %eax
	movq	%rax, %rdx
	leaq	-1(%r13,%rax), %rax
	cmpq	%rax, %r13
	ja	.L2328
	movl	%ecx, %ecx
	movq	%rax, %rbx
	leaq	8(%r12), %rsi
	subq	%rcx, %rbx
	movq	%rbx, %rcx
.L2329:
	cmpb	$0, (%rax)
	jne	.L2328
	cmpq	%rcx, %rax
	je	.L2342
	subq	$1, %rax
	subl	$1, %edx
	cmpq	%rsi, %rax
	jne	.L2329
.L2328:
	movl	%edx, (%r12)
	jmp	.L2355
	.p2align 4,,10
	.p2align 3
.L2282:
	subq	%rbx, %rcx
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L2286:
	movzbl	(%rbx,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L2286
	jmp	.L2385
.L2342:
	movl	$1, %edx
	jmp	.L2328
.L2305:
	movq	%rcx, %rax
	movl	$10, %edx
	subq	%rbx, %rax
.L2310:
	movzbl	(%rbx,%rdx), %ecx
	movb	%cl, (%r12,%rdx)
	addq	$1, %rdx
	cmpq	%rax, %rdx
	jne	.L2310
	jmp	.L2302
.L2314:
	movslq	%eax, %rcx
	subq	$1, %rcx
	leaq	0(%r13,%rcx), %rbx
	cmpq	%rbx, %rdi
	jbe	.L2316
	movl	%eax, (%r12)
	subl	%r15d, %eax
	movl	%eax, %r15d
.L2320:
	leaq	0(%r13,%r15), %rax
	movq	%rbx, %rdx
	cmpq	%rbx, %rax
	jnb	.L2326
.L2323:
	movzbl	(%rax), %esi
	movzbl	(%rdx), %edi
	addq	$1, %rax
	subq	$1, %rdx
	movb	%dil, -1(%rax)
	movb	%sil, 1(%rdx)
	cmpq	%rdx, %rax
	jb	.L2323
.L2326:
	leaq	-1(%r13,%r15), %rax
	cmpq	%rax, %r13
	jnb	.L2324
	movq	%r13, %rdx
.L2325:
	movzbl	(%rdx), %esi
	movzbl	(%rax), %edi
	addq	$1, %rdx
	subq	$1, %rax
	movb	%dil, -1(%rdx)
	movb	%sil, 1(%rax)
	cmpq	%rax, %rdx
	jb	.L2325
.L2324:
	cmpq	%rbx, %r13
	jnb	.L2319
	movq	%r13, %rax
.L2327:
	movzbl	(%rax), %edx
	movzbl	(%rbx), %esi
	addq	$1, %rax
	subq	$1, %rbx
	movb	%sil, -1(%rax)
	movb	%dl, 1(%rbx)
	cmpq	%rbx, %rax
	jb	.L2327
	jmp	.L2319
.L2312:
	subq	$1, %rdx
	jmp	.L2313
.L2318:
	subl	%r15d, %eax
	movl	%eax, %r15d
	je	.L2319
	jmp	.L2320
.L2393:
	leaq	_ZL8d2utable(%rip), %rsi
	movslq	%eax, %rdx
.L2332:
	movzbl	(%rsi,%rdx), %edx
	movl	$1, %r9d
	subl	$1, %edx
	subl	%edx, %eax
	movl	%eax, %r10d
	subl	%r10d, %r9d
	je	.L2320
	movzbl	9(%r12), %eax
	leaq	_ZL9DECPOWERS(%rip), %r14
	movl	%r9d, %edi
	xorl	%edx, %edx
	divl	(%r14,%rdi,4)
	movl	%edx, -56(%rbp)
	cmpl	%r8d, %r9d
	je	.L2394
	movl	%r9d, %edx
	movl	%r8d, %esi
	movq	%r13, %rdi
	movq	%rcx, -72(%rbp)
	movl	%r10d, -60(%rbp)
	call	_ZL15decShiftToLeastPhii.part.0
	movq	-72(%rbp), %rcx
	movl	-60(%rbp), %r10d
.L2322:
	movzbl	-56(%rbp), %eax
	imull	(%r14,%r10,4), %eax
	addb	%al, (%rbx)
	jmp	.L2320
.L2392:
	movl	%eax, %r8d
	movl	%eax, (%r12)
	subl	%r15d, %r8d
	movl	%r8d, %r15d
	jmp	.L2332
.L2394:
	movb	$0, 9(%r12)
	jmp	.L2322
	.cfi_endproc
.LFE2094:
	.size	uprv_decNumberRotate_67, .-uprv_decNumberRotate_67
	.p2align 4
	.globl	uprv_decNumberSameQuantum_67
	.type	uprv_decNumberSameQuantum_67, @function
uprv_decNumberSameQuantum_67:
.LFB2095:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movzbl	8(%rsi), %edi
	movzbl	8(%rdx), %ecx
	movl	%edi, %r8d
	orl	%ecx, %r8d
	andl	$112, %r8d
	je	.L2396
	testb	$48, %dil
	je	.L2397
	movl	$1, %edx
	testb	$48, %cl
	jne	.L2398
.L2397:
	shrb	$6, %cl
	movq	$1, (%rax)
	andl	$1, %ecx
	andl	$64, %edi
	movb	$0, 8(%rax)
	movl	%edi, %edx
	cmovne	%ecx, %edx
	movb	%dl, 9(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L2396:
	movl	4(%rdx), %ecx
	cmpl	%ecx, 4(%rsi)
	sete	%dl
.L2398:
	movq	$1, (%rax)
	movb	$0, 8(%rax)
	movb	%dl, 9(%rax)
	ret
	.cfi_endproc
.LFE2095:
	.size	uprv_decNumberSameQuantum_67, .-uprv_decNumberSameQuantum_67
	.p2align 4
	.globl	uprv_decNumberScaleB_67
	.type	uprv_decNumberScaleB_67, @function
uprv_decNumberScaleB_67:
.LFB2096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movzbl	8(%rsi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -48(%rbp)
	testb	$48, %r13b
	jne	.L2406
	movzbl	8(%rdx), %eax
	testb	$48, %al
	je	.L2487
	testb	$16, %r13b
	je	.L2449
.L2448:
	movl	$1073741952, -48(%rbp)
	movl	$1073741952, %r8d
.L2410:
	movl	(%r14), %eax
	cmpl	%eax, (%rbx)
	jle	.L2488
.L2412:
	movb	%r13b, 8(%r12)
	movslq	(%r14), %rsi
	leaq	9(%r12), %rdx
	leaq	9(%rbx), %r9
	movq	%rdx, %rax
	movq	%r9, %rdi
	cmpl	$49, %esi
	jg	.L2422
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rsi), %ecx
	movq	%rcx, %rsi
	addq	%rdx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L2425
.L2424:
	leaq	25(%r12), %r10
	leaq	10(%r12), %rsi
	cmpq	%r9, %r10
	leaq	25(%rbx), %r10
	setbe	%r11b
	cmpq	%r10, %rdx
	setnb	%r10b
	orb	%r10b, %r11b
	je	.L2472
	movq	%rcx, %r10
	subq	%r12, %r10
	subq	$10, %r10
	cmpq	$14, %r10
	seta	%r11b
	cmpq	%rsi, %rcx
	setnb	%r10b
	testb	%r10b, %r11b
	je	.L2472
	movq	%rcx, %rax
	subq	%r12, %rax
	cmpq	%rsi, %rcx
	leaq	-9(%rax), %rdi
	movl	$1, %eax
	cmovb	%rax, %rdi
	movl	$9, %eax
	movq	%rdi, %rsi
	andq	$-16, %rsi
	addq	$9, %rsi
	.p2align 4,,10
	.p2align 3
.L2427:
	movdqu	(%rbx,%rax), %xmm0
	movups	%xmm0, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L2427
	movq	%rdi, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%r9,%rsi), %rdx
	cmpq	%rsi, %rdi
	je	.L2429
	movzbl	(%rdx), %esi
	movb	%sil, (%rax)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2429
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rax)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2429
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rax)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2429
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rax)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2429
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rax)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2429
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rax)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2429
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rax)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2429
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rax)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2429
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rax)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2429
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rax)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2429
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rax)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2429
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rax)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2429
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rax)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2429
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rax)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2429
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L2429:
	movslq	(%r14), %rsi
	cmpl	$49, %esi
	jle	.L2489
.L2425:
	movl	%esi, (%r12)
	movl	(%r14), %eax
	cmpl	%eax, %esi
	jg	.L2430
.L2486:
	movzbl	8(%r12), %r13d
.L2414:
	movl	$0, 4(%r12)
	andl	$-17, %r13d
	orl	$32, %r13d
	movb	%r13b, 8(%r12)
.L2431:
	testl	%r8d, %r8d
	je	.L2445
	testb	$-35, %r8b
	jne	.L2490
.L2447:
	movl	%r8d, %esi
	movq	%r14, %rdi
	call	uprv_decContextSetStatus_67@PLT
.L2445:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2491
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2487:
	.cfi_restore_state
	testb	$64, %al
	jne	.L2432
	movl	4(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L2432
	movq	%rdx, %rdi
	call	_ZL9decGetIntPK9decNumber
	leal	2147483646(%rax), %edx
	cmpl	$1, %edx
	jbe	.L2432
	cmpl	$-2147483648, %eax
	jne	.L2492
	.p2align 4,,10
	.p2align 3
.L2432:
	movl	$128, -48(%rbp)
	movl	$128, %r8d
.L2434:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L2447
	.p2align 4,,10
	.p2align 3
.L2472:
	movzbl	(%rdi), %edx
	addq	$1, %rax
	addq	$1, %rdi
	movb	%dl, -1(%rax)
	cmpq	%rax, %rcx
	ja	.L2472
	jmp	.L2429
	.p2align 4,,10
	.p2align 3
.L2406:
	testb	$16, %r13b
	jne	.L2448
	xorl	%r8d, %r8d
	testq	%rdx, %rdx
	je	.L2410
	movzbl	8(%rdx), %eax
.L2449:
	testb	$16, %al
	je	.L2411
	movq	%rdx, %rbx
	movl	%eax, %r13d
	movl	(%r14), %eax
	movl	$1073741952, -48(%rbp)
	movl	$1073741952, %r8d
	cmpl	%eax, (%rbx)
	jg	.L2412
	.p2align 4,,10
	.p2align 3
.L2488:
	cmpq	%rbx, %r12
	je	.L2486
	movb	%r13b, 8(%r12)
	movl	4(%rbx), %eax
	movl	-48(%rbp), %r8d
	movl	%eax, 4(%r12)
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	movzbl	9(%rbx), %eax
	movb	%al, 9(%r12)
	movl	(%rbx), %eax
	cmpl	$1, %eax
	jle	.L2414
	leaq	10(%r12), %rdi
	leaq	9(%rbx), %rcx
	movslq	%eax, %rdx
	cmpl	$49, %eax
	jg	.L2416
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdx), %edx
.L2416:
	addq	%rdx, %rcx
	leaq	10(%rbx), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2414
	movq	%rcx, %rax
	subq	%rbx, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2417
	leaq	25(%r12), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L2417
	movq	%rcx, %rax
	subq	%rbx, %rax
	leaq	-10(%rax), %r9
	movl	$10, %eax
	movq	%r9, %rsi
	andq	$-16, %rsi
	addq	$10, %rsi
	.p2align 4,,10
	.p2align 3
.L2418:
	movdqu	(%rbx,%rax), %xmm1
	movups	%xmm1, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L2418
	movq	%r9, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%rdi,%rsi), %rdx
	cmpq	%r9, %rsi
	je	.L2486
	movzbl	(%rax), %esi
	movb	%sil, (%rdx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2486
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2486
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2486
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2486
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2486
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2486
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2486
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2486
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2486
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2486
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2486
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2486
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2486
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2486
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
	jmp	.L2486
	.p2align 4,,10
	.p2align 3
.L2422:
	movslq	%esi, %rcx
	addq	%rdx, %rcx
	cmpq	%rcx, %rdx
	jb	.L2424
	jmp	.L2425
	.p2align 4,,10
	.p2align 3
.L2489:
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rsi), %esi
	movl	%esi, (%r12)
	movl	(%r14), %eax
	cmpl	%eax, %esi
	jle	.L2486
.L2430:
	subl	%eax, %esi
	movq	%r12, %rdi
	call	_ZL8decDecapP9decNumberi
	movzbl	8(%r12), %r13d
	movl	-48(%rbp), %r8d
	jmp	.L2414
	.p2align 4,,10
	.p2align 3
.L2411:
	movl	%r13d, %ecx
	xorl	%r8d, %r8d
	andl	$32, %ecx
	cmove	%eax, %r13d
	cmove	%rdx, %rbx
	jmp	.L2410
.L2490:
	testl	$1073741824, %r8d
	je	.L2434
	andl	$-1073741825, %r8d
	jmp	.L2447
	.p2align 4,,10
	.p2align 3
.L2492:
	cltd
	movl	%edx, %ecx
	xorl	%eax, %ecx
	subl	%edx, %ecx
	movl	4(%r14), %edx
	addl	(%r14), %edx
	addl	%edx, %edx
	cmpl	%edx, %ecx
	jg	.L2432
	cmpq	%r12, %rbx
	je	.L2436
	movb	%r13b, 8(%r12)
	movl	4(%rbx), %edx
	movl	%edx, 4(%r12)
	movl	(%rbx), %edx
	movl	%edx, (%r12)
	movzbl	9(%rbx), %edx
	movb	%dl, 9(%r12)
	movl	(%rbx), %ecx
	cmpl	$1, %ecx
	jle	.L2436
	leaq	10(%r12), %r8
	leaq	9(%rbx), %rsi
	movslq	%ecx, %rdx
	cmpl	$49, %ecx
	jg	.L2438
	movq	%rdx, %rcx
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rcx), %edx
.L2438:
	addq	%rdx, %rsi
	leaq	10(%rbx), %rcx
	cmpq	%rcx, %rsi
	jbe	.L2436
	movq	%rsi, %rdx
	subq	%rbx, %rdx
	subq	$11, %rdx
	cmpq	$14, %rdx
	jbe	.L2439
	leaq	25(%rbx), %rdx
	subq	%r8, %rdx
	cmpq	$30, %rdx
	jbe	.L2439
	movq	%rsi, %rdx
	subq	%rbx, %rdx
	leaq	-10(%rdx), %r9
	movl	$10, %edx
	movq	%r9, %rdi
	andq	$-16, %rdi
	addq	$10, %rdi
	.p2align 4,,10
	.p2align 3
.L2440:
	movdqu	(%rbx,%rdx), %xmm2
	movups	%xmm2, (%r12,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L2440
	movq	%r9, %rdi
	andq	$-16, %rdi
	addq	%rdi, %rcx
	leaq	(%r8,%rdi), %rdx
	cmpq	%rdi, %r9
	je	.L2442
	movzbl	(%rcx), %edi
	movb	%dil, (%rdx)
	leaq	1(%rcx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L2442
	movzbl	1(%rcx), %edi
	movb	%dil, 1(%rdx)
	leaq	2(%rcx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L2442
	movzbl	2(%rcx), %edi
	movb	%dil, 2(%rdx)
	leaq	3(%rcx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L2442
	movzbl	3(%rcx), %edi
	movb	%dil, 3(%rdx)
	leaq	4(%rcx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L2442
	movzbl	4(%rcx), %edi
	movb	%dil, 4(%rdx)
	leaq	5(%rcx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L2442
	movzbl	5(%rcx), %edi
	movb	%dil, 5(%rdx)
	leaq	6(%rcx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L2442
	movzbl	6(%rcx), %edi
	movb	%dil, 6(%rdx)
	leaq	7(%rcx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L2442
	movzbl	7(%rcx), %edi
	movb	%dil, 7(%rdx)
	leaq	8(%rcx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L2442
	movzbl	8(%rcx), %edi
	movb	%dil, 8(%rdx)
	leaq	9(%rcx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L2442
	movzbl	9(%rcx), %edi
	movb	%dil, 9(%rdx)
	leaq	10(%rcx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L2442
	movzbl	10(%rcx), %edi
	movb	%dil, 10(%rdx)
	leaq	11(%rcx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L2442
	movzbl	11(%rcx), %edi
	movb	%dil, 11(%rdx)
	leaq	12(%rcx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L2442
	movzbl	12(%rcx), %edi
	movb	%dil, 12(%rdx)
	leaq	13(%rcx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L2442
	movzbl	13(%rcx), %edi
	movb	%dil, 13(%rdx)
	leaq	14(%rcx), %rdi
	cmpq	%rdi, %rsi
	jbe	.L2442
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%rdx)
.L2442:
	movzbl	8(%r12), %r13d
.L2436:
	andl	$64, %r13d
	jne	.L2445
	addl	%eax, 4(%r12)
	leaq	-48(%rbp), %rcx
	leaq	-44(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-48(%rbp), %r8d
	jmp	.L2431
	.p2align 4,,10
	.p2align 3
.L2417:
	subq	%rbx, %rcx
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L2421:
	movzbl	(%rbx,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L2421
	jmp	.L2486
.L2439:
	movq	%rsi, %rdx
	movl	$10, %ecx
	subq	%rbx, %rdx
.L2443:
	movzbl	(%rbx,%rcx), %esi
	movb	%sil, (%r12,%rcx)
	addq	$1, %rcx
	cmpq	%rdx, %rcx
	jne	.L2443
	jmp	.L2442
.L2491:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2096:
	.size	uprv_decNumberScaleB_67, .-uprv_decNumberScaleB_67
	.p2align 4
	.globl	uprv_decNumberShift_67
	.type	uprv_decNumberShift_67, @function
uprv_decNumberShift_67:
.LFB2097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movzbl	8(%rsi), %r13d
	testb	$48, %r13b
	jne	.L2494
	movzbl	8(%rdx), %eax
	testb	$48, %al
	je	.L2580
	movl	$1073741952, %r11d
	testb	$16, %r13b
	je	.L2540
.L2497:
	movl	(%r14), %eax
	cmpl	%eax, (%rbx)
	jle	.L2581
.L2498:
	movb	%r13b, 8(%r12)
	movslq	(%r14), %rsi
	leaq	9(%r12), %rdx
	leaq	9(%rbx), %r8
	movq	%rdx, %rax
	movq	%r8, %rdi
	cmpl	$49, %esi
	jg	.L2508
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rsi), %ecx
	movq	%rcx, %rsi
	addq	%rdx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L2511
.L2510:
	leaq	25(%r12), %r9
	leaq	10(%r12), %rsi
	cmpq	%r9, %r8
	leaq	25(%rbx), %r9
	setnb	%r10b
	cmpq	%r9, %rdx
	setnb	%r9b
	orb	%r9b, %r10b
	je	.L2567
	movq	%rcx, %r9
	subq	%r12, %r9
	subq	$10, %r9
	cmpq	$14, %r9
	seta	%r10b
	cmpq	%rsi, %rcx
	setnb	%r9b
	testb	%r9b, %r10b
	je	.L2567
	movq	%rcx, %rax
	subq	%r12, %rax
	cmpq	%rsi, %rcx
	leaq	-9(%rax), %rdi
	movl	$1, %eax
	cmovb	%rax, %rdi
	movl	$9, %eax
	movq	%rdi, %rsi
	andq	$-16, %rsi
	addq	$9, %rsi
	.p2align 4,,10
	.p2align 3
.L2513:
	movdqu	(%rbx,%rax), %xmm0
	movups	%xmm0, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L2513
	movq	%rdi, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%r8,%rsi), %rdx
	cmpq	%rsi, %rdi
	je	.L2515
	movzbl	(%rdx), %esi
	movb	%sil, (%rax)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2515
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rax)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2515
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rax)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2515
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rax)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2515
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rax)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2515
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rax)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2515
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rax)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2515
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rax)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2515
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rax)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2515
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rax)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2515
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rax)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2515
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rax)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2515
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rax)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2515
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rax)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2515
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L2515:
	movslq	(%r14), %rsi
	cmpl	$49, %esi
	jle	.L2582
.L2511:
	movl	%esi, (%r12)
	movl	(%r14), %eax
	cmpl	%eax, %esi
	jg	.L2516
.L2579:
	movzbl	8(%r12), %r13d
.L2500:
	movl	$0, 4(%r12)
	andl	$-17, %r13d
	orl	$32, %r13d
	movb	%r13b, 8(%r12)
	testl	%r11d, %r11d
	je	.L2558
	testb	$-35, %r11b
	jne	.L2583
.L2539:
	movl	%r11d, %esi
	movq	%r14, %rdi
	call	uprv_decContextSetStatus_67@PLT
.L2558:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2580:
	.cfi_restore_state
	testb	$64, %al
	jne	.L2547
	movl	4(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L2547
	movq	%rdx, %rdi
	call	_ZL9decGetIntPK9decNumber
	movl	%eax, %r15d
	leal	2147483646(%rax), %eax
	cmpl	$1, %eax
	jbe	.L2547
	cmpl	$-2147483648, %r15d
	jne	.L2584
	.p2align 4,,10
	.p2align 3
.L2547:
	movl	$128, %r11d
.L2519:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L2539
	.p2align 4,,10
	.p2align 3
.L2567:
	movzbl	(%rdi), %edx
	addq	$1, %rax
	addq	$1, %rdi
	movb	%dl, -1(%rax)
	cmpq	%rax, %rcx
	ja	.L2567
	jmp	.L2515
	.p2align 4,,10
	.p2align 3
.L2494:
	movl	$1073741952, %r11d
	testb	$16, %r13b
	jne	.L2497
	xorl	%r11d, %r11d
	testq	%rdx, %rdx
	je	.L2497
	movzbl	8(%rdx), %eax
.L2540:
	testb	$16, %al
	je	.L2585
	movq	%rdx, %rbx
	movl	%eax, %r13d
	movl	$1073741952, %r11d
	movl	(%r14), %eax
	cmpl	%eax, (%rbx)
	jg	.L2498
	.p2align 4,,10
	.p2align 3
.L2581:
	cmpq	%rbx, %r12
	je	.L2579
	movb	%r13b, 8(%r12)
	movl	4(%rbx), %eax
	movl	%eax, 4(%r12)
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	movzbl	9(%rbx), %eax
	movb	%al, 9(%r12)
	movl	(%rbx), %eax
	cmpl	$1, %eax
	jle	.L2500
	leaq	10(%r12), %rdi
	leaq	9(%rbx), %rcx
	movslq	%eax, %rdx
	cmpl	$49, %eax
	jg	.L2502
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdx), %edx
.L2502:
	addq	%rdx, %rcx
	leaq	10(%rbx), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2500
	movq	%rcx, %rax
	subq	%rbx, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2503
	leaq	25(%r12), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L2503
	movq	%rcx, %rax
	subq	%rbx, %rax
	leaq	-10(%rax), %r8
	movl	$10, %eax
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	$10, %rsi
	.p2align 4,,10
	.p2align 3
.L2504:
	movdqu	(%rbx,%rax), %xmm1
	movups	%xmm1, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L2504
	movq	%r8, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%rdi,%rsi), %rdx
	cmpq	%rsi, %r8
	je	.L2579
	movzbl	(%rax), %esi
	movb	%sil, (%rdx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2579
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2579
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2579
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2579
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2579
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2579
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2579
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2579
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2579
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2579
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2579
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2579
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2579
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2579
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
	jmp	.L2579
	.p2align 4,,10
	.p2align 3
.L2508:
	movslq	%esi, %rcx
	addq	%rdx, %rcx
	cmpq	%rcx, %rdx
	jb	.L2510
	jmp	.L2511
	.p2align 4,,10
	.p2align 3
.L2582:
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rsi), %esi
	movl	%esi, (%r12)
	movl	(%r14), %eax
	cmpl	%eax, %esi
	jle	.L2579
.L2516:
	subl	%eax, %esi
	movq	%r12, %rdi
	call	_ZL8decDecapP9decNumberi
	jmp	.L2579
	.p2align 4,,10
	.p2align 3
.L2585:
	movl	%r13d, %ecx
	xorl	%r11d, %r11d
	andl	$32, %ecx
	cmove	%eax, %r13d
	cmove	%rdx, %rbx
	jmp	.L2497
.L2583:
	testl	$1073741824, %r11d
	je	.L2519
	andl	$-1073741825, %r11d
	jmp	.L2539
	.p2align 4,,10
	.p2align 3
.L2584:
	movl	%r15d, %edx
	movl	$128, %r11d
	sarl	$31, %edx
	movl	%edx, %eax
	xorl	%r15d, %eax
	subl	%edx, %eax
	cmpl	(%r14), %eax
	jg	.L2519
	cmpq	%r12, %rbx
	je	.L2523
	movb	%r13b, 8(%r12)
	movl	4(%rbx), %eax
	movl	%eax, 4(%r12)
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	movzbl	9(%rbx), %eax
	movb	%al, 9(%r12)
	movl	(%rbx), %edx
	cmpl	$1, %edx
	jle	.L2523
	leaq	10(%r12), %rdi
	leaq	9(%rbx), %rcx
	movslq	%edx, %rax
	cmpl	$49, %edx
	jg	.L2525
	movq	%rax, %rdx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdx), %eax
.L2525:
	addq	%rax, %rcx
	leaq	10(%rbx), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2523
	movq	%rcx, %rax
	subq	%rbx, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2526
	leaq	25(%rbx), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L2526
	movq	%rcx, %rax
	subq	%rbx, %rax
	leaq	-10(%rax), %r8
	movl	$10, %eax
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	$10, %rsi
	.p2align 4,,10
	.p2align 3
.L2528:
	movdqu	(%rbx,%rax), %xmm2
	movups	%xmm2, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L2528
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	%rsi, %rdx
	leaq	(%rdi,%rsi), %rax
	cmpq	%rsi, %r8
	je	.L2523
	movzbl	(%rdx), %esi
	movb	%sil, (%rax)
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2523
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rax)
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2523
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rax)
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2523
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rax)
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2523
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rax)
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2523
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rax)
	leaq	6(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2523
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rax)
	leaq	7(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2523
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rax)
	leaq	8(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2523
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rax)
	leaq	9(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2523
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rax)
	leaq	10(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2523
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rax)
	leaq	11(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2523
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rax)
	leaq	12(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2523
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rax)
	leaq	13(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2523
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rax)
	leaq	14(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2523
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
.L2523:
	testl	%r15d, %r15d
	je	.L2558
	testb	$64, 8(%r12)
	jne	.L2558
	testl	%r15d, %r15d
	jle	.L2532
	movl	(%r14), %edx
	cmpl	%r15d, %edx
	je	.L2536
	movl	(%r12), %esi
	leal	(%r15,%rsi), %eax
	cmpl	%eax, %edx
	jl	.L2586
.L2534:
	cmpl	$1, %esi
	jg	.L2535
	cmpb	$0, 9(%r12)
	je	.L2558
.L2535:
	leaq	9(%r12), %rdi
	movl	%r15d, %edx
	call	_ZL14decShiftToMostPhii.part.0
	movl	%eax, (%r12)
	jmp	.L2558
	.p2align 4,,10
	.p2align 3
.L2503:
	subq	%rbx, %rcx
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L2507:
	movzbl	(%rbx,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L2507
	jmp	.L2579
.L2536:
	movb	$0, 9(%r12)
	movl	$1, (%r12)
	jmp	.L2558
.L2532:
	movl	%r15d, %edx
	movslq	(%r12), %rsi
	negl	%edx
	cmpl	%esi, %edx
	jge	.L2536
	leaq	9(%r12), %rdi
	cmpl	$49, %esi
	jg	.L2537
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rsi), %esi
	cmpl	%esi, %edx
	je	.L2587
.L2537:
	call	_ZL15decShiftToLeastPhii.part.0
.L2538:
	addl	%r15d, (%r12)
	jmp	.L2558
.L2526:
	movq	%rcx, %rax
	movl	$10, %edx
	subq	%rbx, %rax
.L2531:
	movzbl	(%rbx,%rdx), %ecx
	movb	%cl, (%r12,%rdx)
	addq	$1, %rdx
	cmpq	%rax, %rdx
	jne	.L2531
	jmp	.L2523
.L2586:
	subl	%edx, %eax
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZL8decDecapP9decNumberi
	movl	(%r12), %esi
	jmp	.L2534
.L2587:
	movb	$0, 9(%r12)
	jmp	.L2538
	.cfi_endproc
.LFE2097:
	.size	uprv_decNumberShift_67, .-uprv_decNumberShift_67
	.p2align 4
	.globl	uprv_decNumberToIntegralExact_67
	.type	uprv_decNumberToIntegralExact_67, @function
uprv_decNumberToIntegralExact_67:
.LFB2100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rsi), %eax
	movl	$0, -88(%rbp)
	testb	$112, %al
	je	.L2589
	testb	$64, %al
	jne	.L2658
	movq	%rdx, %rcx
	leaq	-88(%rbp), %r8
	xorl	%edx, %edx
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-88(%rbp), %esi
.L2600:
	testl	%esi, %esi
	je	.L2616
	testb	$-35, %sil
	jne	.L2659
.L2614:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
.L2616:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2660
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2658:
	.cfi_restore_state
	cmpq	%rdi, %rsi
	je	.L2616
	movb	%al, 8(%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 4(%rdi)
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	movzbl	9(%rsi), %eax
	movb	%al, 9(%rdi)
	movl	(%rsi), %eax
	cmpl	$1, %eax
	jle	.L2616
	leaq	10(%rdi), %r8
	leaq	9(%rsi), %rcx
	movslq	%eax, %rdx
	cmpl	$49, %eax
	jg	.L2593
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdx), %edx
.L2593:
	addq	%rdx, %rcx
	leaq	10(%rsi), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2616
	movq	%rcx, %rax
	subq	%rsi, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2594
	leaq	25(%rsi), %rax
	subq	%r8, %rax
	cmpq	$30, %rax
	jbe	.L2594
	movq	%rcx, %rax
	subq	%rsi, %rax
	leaq	-10(%rax), %r9
	movl	$10, %eax
	movq	%r9, %rdi
	andq	$-16, %rdi
	addq	$10, %rdi
	.p2align 4,,10
	.p2align 3
.L2596:
	movdqu	(%rsi,%rax), %xmm1
	movups	%xmm1, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L2596
	movq	%r9, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%r8,%rsi), %rdx
	cmpq	%r9, %rsi
	jne	.L2657
	jmp	.L2616
	.p2align 4,,10
	.p2align 3
.L2589:
	movl	4(%rsi), %edi
	testl	%edi, %edi
	js	.L2601
	cmpq	%r12, %rsi
	je	.L2616
	movb	%al, 8(%r12)
	movl	4(%rsi), %eax
	movl	%eax, 4(%r12)
	movl	(%rsi), %eax
	movl	%eax, (%r12)
	movzbl	9(%rsi), %eax
	movb	%al, 9(%r12)
	movl	(%rsi), %eax
	cmpl	$1, %eax
	jle	.L2616
	leaq	10(%r12), %r8
	leaq	9(%rsi), %rcx
	movslq	%eax, %rdx
	cmpl	$49, %eax
	jg	.L2604
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdx), %edx
.L2604:
	addq	%rdx, %rcx
	leaq	10(%rsi), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2616
	movq	%rcx, %rax
	subq	%rsi, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2605
	leaq	25(%rsi), %rax
	subq	%r8, %rax
	cmpq	$30, %rax
	jbe	.L2605
	movq	%rcx, %rax
	subq	%rsi, %rax
	leaq	-10(%rax), %r9
	movl	$10, %eax
	movq	%r9, %rdi
	andq	$-16, %rdi
	addq	$10, %rdi
	.p2align 4,,10
	.p2align 3
.L2607:
	movdqu	(%rsi,%rax), %xmm0
	movups	%xmm0, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L2607
	movq	%r9, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%r8,%rsi), %rdx
	cmpq	%rsi, %r9
	je	.L2616
.L2657:
	movzbl	(%rax), %esi
	movb	%sil, (%rdx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2616
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2616
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2616
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2616
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2616
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2616
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2616
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2616
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2616
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2616
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2616
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2616
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2616
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2616
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
	jmp	.L2616
	.p2align 4,,10
	.p2align 3
.L2601:
	movq	16(%rdx), %rax
	movdqu	(%rdx), %xmm2
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %r14
	movw	%cx, -44(%rbp)
	leaq	-84(%rbp), %r9
	movl	$1, %r8d
	movq	%r14, %rcx
	movq	%rax, -64(%rbp)
	movl	24(%rdx), %eax
	movq	%r12, %rdi
	leaq	-52(%rbp), %rdx
	movaps	%xmm2, -80(%rbp)
	movl	%eax, -56(%rbp)
	movl	(%rsi), %eax
	movl	$0, -64(%rbp)
	movl	%eax, -80(%rbp)
	movq	$1, -52(%rbp)
	movl	$0, -84(%rbp)
	call	_ZL13decQuantizeOpP9decNumberPKS_S2_P10decContexthPj.constprop.0
	movl	-84(%rbp), %esi
	testl	%esi, %esi
	jne	.L2661
.L2611:
	movl	-60(%rbp), %esi
	orl	-88(%rbp), %esi
	movl	%esi, -88(%rbp)
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2659:
	testl	$1073741824, %esi
	je	.L2615
	andl	$-1073741825, %esi
	jmp	.L2614
	.p2align 4,,10
	.p2align 3
.L2661:
	testb	$-35, %sil
	je	.L2612
	testl	$1073741824, %esi
	je	.L2613
	andl	$-1073741825, %esi
.L2612:
	movq	%r14, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L2611
	.p2align 4,,10
	.p2align 3
.L2615:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L2614
	.p2align 4,,10
	.p2align 3
.L2605:
	subq	%rsi, %rcx
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L2610:
	movzbl	(%rsi,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L2610
	jmp	.L2616
	.p2align 4,,10
	.p2align 3
.L2594:
	subq	%rsi, %rcx
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L2599:
	movzbl	(%rsi,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L2599
	jmp	.L2616
	.p2align 4,,10
	.p2align 3
.L2613:
	movl	$32, %edx
	movq	$1, (%r12)
	movw	%dx, 8(%r12)
	jmp	.L2612
.L2660:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2100:
	.size	uprv_decNumberToIntegralExact_67, .-uprv_decNumberToIntegralExact_67
	.p2align 4
	.globl	uprv_decNumberToIntegralValue_67
	.type	uprv_decNumberToIntegralValue_67, @function
uprv_decNumberToIntegralValue_67:
.LFB2101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$104, %rsp
	movdqu	(%rdx), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdx), %rax
	movl	24(%rdx), %edx
	movl	$0, -120(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	movzbl	8(%rsi), %eax
	movl	%edx, -88(%rbp)
	movl	$0, -96(%rbp)
	testb	$112, %al
	je	.L2663
	testb	$64, %al
	jne	.L2734
	leaq	-112(%rbp), %rcx
	leaq	-120(%rbp), %r8
	xorl	%edx, %edx
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-120(%rbp), %esi
.L2674:
	testl	%esi, %esi
	je	.L2690
	testb	$-35, %sil
	jne	.L2735
.L2688:
	leaq	-112(%rbp), %rdi
	call	uprv_decContextSetStatus_67@PLT
.L2690:
	movl	-92(%rbp), %eax
	andl	$128, %eax
	orl	%eax, 20(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2736
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2734:
	.cfi_restore_state
	cmpq	%rsi, %rdi
	je	.L2690
	movb	%al, 8(%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 4(%rdi)
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	movzbl	9(%rsi), %eax
	movb	%al, 9(%rdi)
	movl	(%rsi), %eax
	cmpl	$1, %eax
	jle	.L2690
	leaq	10(%rdi), %r8
	leaq	9(%rsi), %rcx
	movslq	%eax, %rdx
	cmpl	$49, %eax
	jg	.L2667
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdx), %edx
.L2667:
	addq	%rdx, %rcx
	leaq	10(%rsi), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2690
	movq	%rcx, %rax
	subq	%rsi, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2668
	leaq	25(%r12), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L2668
	movq	%rcx, %rax
	subq	%rsi, %rax
	leaq	-10(%rax), %r9
	movl	$10, %eax
	movq	%r9, %rdi
	andq	$-16, %rdi
	addq	$10, %rdi
	.p2align 4,,10
	.p2align 3
.L2670:
	movdqu	(%rsi,%rax), %xmm2
	movups	%xmm2, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L2670
.L2733:
	movq	%r9, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%r8,%rsi), %rdx
	cmpq	%rsi, %r9
	je	.L2690
	movzbl	(%rax), %esi
	movb	%sil, (%rdx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2690
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2690
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2690
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2690
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2690
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2690
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2690
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2690
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2690
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2690
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2690
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2690
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2690
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2690
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
	jmp	.L2690
	.p2align 4,,10
	.p2align 3
.L2663:
	movl	4(%rsi), %edi
	testl	%edi, %edi
	js	.L2675
	cmpq	%rsi, %r12
	je	.L2690
	movb	%al, 8(%r12)
	movl	4(%rsi), %eax
	movl	%eax, 4(%r12)
	movl	(%rsi), %eax
	movl	%eax, (%r12)
	movzbl	9(%rsi), %eax
	movb	%al, 9(%r12)
	movl	(%rsi), %eax
	cmpl	$1, %eax
	jle	.L2690
	leaq	10(%r12), %r8
	leaq	9(%rsi), %rcx
	movslq	%eax, %rdx
	cmpl	$49, %eax
	jg	.L2678
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdx), %edx
.L2678:
	addq	%rdx, %rcx
	leaq	10(%rsi), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2690
	movq	%rcx, %rax
	subq	%rsi, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L2679
	leaq	25(%r12), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L2679
	movq	%rcx, %rax
	subq	%rsi, %rax
	leaq	-10(%rax), %r9
	movl	$10, %eax
	movq	%r9, %rdi
	andq	$-16, %rdi
	addq	$10, %rdi
	.p2align 4,,10
	.p2align 3
.L2681:
	movdqu	(%rsi,%rax), %xmm1
	movups	%xmm1, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L2681
	jmp	.L2733
	.p2align 4,,10
	.p2align 3
.L2675:
	movq	-96(%rbp), %rax
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %r13
	movl	%edx, -56(%rbp)
	movw	%cx, -44(%rbp)
	leaq	-52(%rbp), %rdx
	leaq	-116(%rbp), %r9
	movq	%r13, %rcx
	movq	%rax, -64(%rbp)
	movl	(%rsi), %eax
	movl	$1, %r8d
	movq	%r12, %rdi
	movaps	%xmm0, -80(%rbp)
	movl	$0, -64(%rbp)
	movl	%eax, -80(%rbp)
	movq	$1, -52(%rbp)
	movl	$0, -116(%rbp)
	call	_ZL13decQuantizeOpP9decNumberPKS_S2_P10decContexthPj.constprop.0
	movl	-116(%rbp), %esi
	testl	%esi, %esi
	jne	.L2737
.L2685:
	movl	-60(%rbp), %esi
	orl	-120(%rbp), %esi
	movl	%esi, -120(%rbp)
	jmp	.L2674
	.p2align 4,,10
	.p2align 3
.L2735:
	testl	$1073741824, %esi
	je	.L2689
	andl	$-1073741825, %esi
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2737:
	testb	$-35, %sil
	je	.L2686
	testl	$1073741824, %esi
	je	.L2687
	andl	$-1073741825, %esi
.L2686:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L2685
	.p2align 4,,10
	.p2align 3
.L2689:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2679:
	subq	%rsi, %rcx
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L2684:
	movzbl	(%rsi,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L2684
	jmp	.L2690
	.p2align 4,,10
	.p2align 3
.L2668:
	subq	%rsi, %rcx
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L2673:
	movzbl	(%rsi,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L2673
	jmp	.L2690
	.p2align 4,,10
	.p2align 3
.L2687:
	movl	$32, %edx
	movq	$1, (%r12)
	movw	%dx, 8(%r12)
	jmp	.L2686
.L2736:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2101:
	.size	uprv_decNumberToIntegralValue_67, .-uprv_decNumberToIntegralValue_67
	.p2align 4
	.globl	uprv_decNumberXor_67
	.type	uprv_decNumberXor_67, @function
uprv_decNumberXor_67:
.LFB2102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	4(%rsi), %ecx
	testl	%ecx, %ecx
	jne	.L2739
	movzbl	8(%rsi), %eax
	testb	$112, %al
	jne	.L2739
	testb	%al, %al
	js	.L2739
	movl	4(%rdx), %r14d
	testl	%r14d, %r14d
	jne	.L2739
	movzbl	8(%rdx), %eax
	testb	$112, %al
	jne	.L2739
	testb	%al, %al
	js	.L2739
	movslq	(%rsi), %rax
	leaq	9(%rsi), %r9
	leaq	9(%rdx), %rcx
	leaq	9(%r12), %r13
	cmpl	$49, %eax
	jg	.L2742
	leaq	_ZL8d2utable(%rip), %rsi
	movzbl	(%rsi,%rax), %r10d
	subq	$1, %r10
.L2743:
	movslq	(%rdx), %rax
	addq	%r9, %r10
	cmpl	$49, %eax
	jg	.L2744
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %r11d
	subq	$1, %r11
.L2745:
	movslq	(%rdi), %rax
	addq	%rcx, %r11
	leaq	-1(%r13,%rax), %rbx
	cmpl	$49, %eax
	jg	.L2747
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	leaq	-1(%r13,%rax), %rbx
.L2747:
	cmpq	%rbx, %r13
	ja	.L2761
	addq	$1, %rbx
	movq	%r13, %r8
	jmp	.L2751
	.p2align 4,,10
	.p2align 3
.L2772:
	movzbl	(%r9), %edx
	cmpq	%r11, %rcx
	jbe	.L2760
	movl	%edx, %eax
	xorl	%esi, %esi
.L2750:
	movb	$0, (%r8)
	testb	%al, %al
	jne	.L2770
	.p2align 4,,10
	.p2align 3
.L2758:
	addq	$1, %r8
	addq	$1, %r9
	addq	$1, %rcx
	cmpq	%rbx, %r8
	je	.L2771
.L2751:
	cmpq	%r9, %r10
	jnb	.L2772
	xorl	%edx, %edx
	cmpq	%r11, %rcx
	jbe	.L2760
	movb	$0, (%r8)
	jmp	.L2758
	.p2align 4,,10
	.p2align 3
.L2770:
	movl	%edx, %eax
	xorl	%esi, %eax
	testb	$1, %al
	je	.L2755
	movb	$1, (%r8)
.L2755:
	movzbl	%dl, %r14d
	leal	(%r14,%r14,4), %eax
	leal	(%r14,%rax,8), %eax
	movzbl	%sil, %r14d
	leal	(%rax,%rax,4), %eax
	shrw	$11, %ax
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	subl	%eax, %edx
	leal	(%r14,%r14,4), %eax
	leal	(%r14,%rax,8), %eax
	leal	(%rax,%rax,4), %eax
	shrw	$11, %ax
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	subl	%eax, %esi
	orl	%esi, %edx
	cmpb	$1, %dl
	jbe	.L2758
	.p2align 4,,10
	.p2align 3
.L2739:
	movl	$32, %eax
	movq	$1, (%r12)
	movl	$128, %esi
	movw	%ax, 8(%r12)
	call	uprv_decContextSetStatus_67@PLT
.L2768:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2760:
	.cfi_restore_state
	movzbl	(%rcx), %esi
	movl	%edx, %eax
	orl	%esi, %eax
	jmp	.L2750
	.p2align 4,,10
	.p2align 3
.L2744:
	movslq	%eax, %r11
	subq	$1, %r11
	jmp	.L2745
	.p2align 4,,10
	.p2align 3
.L2742:
	movslq	%eax, %r10
	subq	$1, %r10
	jmp	.L2743
	.p2align 4,,10
	.p2align 3
.L2771:
	subq	%r13, %r8
	movl	%r8d, %r14d
	movslq	%r8d, %r8
	leaq	-1(%r8), %rax
.L2748:
	addq	%rax, %r13
	jc	.L2752
	leaq	8(%r12), %rax
.L2753:
	cmpb	$0, 0(%r13)
	jne	.L2752
	cmpl	$1, %r14d
	je	.L2752
	subq	$1, %r13
	subl	$1, %r14d
	cmpq	%rax, %r13
	jne	.L2753
	.p2align 4,,10
	.p2align 3
.L2752:
	movl	%r14d, (%r12)
	movl	$0, 4(%r12)
	movb	$0, 8(%r12)
	jmp	.L2768
.L2761:
	movq	$-1, %rax
	jmp	.L2748
	.cfi_endproc
.LFE2102:
	.size	uprv_decNumberXor_67, .-uprv_decNumberXor_67
	.p2align 4
	.globl	_Z19uprv_decNumberClassPK9decNumberP10decContext
	.type	_Z19uprv_decNumberClassPK9decNumberP10decContext, @function
_Z19uprv_decNumberClassPK9decNumberP10decContext:
.LFB2103:
	.cfi_startproc
	endbr64
	movzbl	8(%rdi), %edx
	testb	$112, %dl
	jne	.L2788
	cmpb	$0, 9(%rdi)
	movl	(%rdi), %eax
	jne	.L2776
	cmpl	$1, %eax
	je	.L2789
	addl	4(%rdi), %eax
	cmpl	8(%rsi), %eax
	jg	.L2780
.L2779:
	movsbl	%dl, %eax
	sarl	$31, %eax
	andl	$-3, %eax
	addl	$7, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2788:
	movl	$1, %eax
	testb	$32, %dl
	jne	.L2773
	xorl	%eax, %eax
	testb	$16, %dl
	jne	.L2773
	movsbl	%dl, %eax
	sarl	$31, %eax
	andl	$-7, %eax
	addl	$9, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2773:
	ret
	.p2align 4,,10
	.p2align 3
.L2776:
	addl	4(%rdi), %eax
	cmpl	8(%rsi), %eax
	jle	.L2779
.L2780:
	movsbl	%dl, %eax
	sarl	$31, %eax
	andl	$-5, %eax
	addl	$8, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2789:
	movsbl	%dl, %eax
	notl	%eax
	shrl	$31, %eax
	addl	$5, %eax
	ret
	.cfi_endproc
.LFE2103:
	.size	_Z19uprv_decNumberClassPK9decNumberP10decContext, .-_Z19uprv_decNumberClassPK9decNumberP10decContext
	.section	.rodata.str1.1
.LC6:
	.string	"-Infinity"
.LC7:
	.string	"Invalid"
.LC8:
	.string	"+Normal"
.LC9:
	.string	"-Normal"
.LC10:
	.string	"+Zero"
.LC11:
	.string	"-Zero"
.LC12:
	.string	"+Subnormal"
.LC13:
	.string	"-Subnormal"
.LC14:
	.string	"+Infinity"
.LC15:
	.string	"NaN"
.LC16:
	.string	"sNaN"
	.text
	.p2align 4
	.globl	uprv_decNumberClassToString_67
	.type	uprv_decNumberClassToString_67, @function
uprv_decNumberClassToString_67:
.LFB2104:
	.cfi_startproc
	endbr64
	leaq	.LC8(%rip), %rax
	cmpl	$8, %edi
	je	.L2790
	leaq	.LC9(%rip), %rax
	cmpl	$3, %edi
	je	.L2790
	leaq	.LC10(%rip), %rax
	cmpl	$6, %edi
	je	.L2790
	leaq	.LC11(%rip), %rax
	cmpl	$5, %edi
	je	.L2790
	leaq	.LC12(%rip), %rax
	cmpl	$7, %edi
	je	.L2790
	leaq	.LC13(%rip), %rax
	cmpl	$4, %edi
	je	.L2790
	leaq	.LC14(%rip), %rax
	cmpl	$9, %edi
	je	.L2790
	leaq	.LC6(%rip), %rax
	cmpl	$2, %edi
	je	.L2790
	leaq	.LC15(%rip), %rax
	cmpl	$1, %edi
	je	.L2790
	testl	%edi, %edi
	leaq	.LC7(%rip), %rax
	leaq	.LC16(%rip), %rdx
	cmove	%rdx, %rax
.L2790:
	ret
	.cfi_endproc
.LFE2104:
	.size	uprv_decNumberClassToString_67, .-uprv_decNumberClassToString_67
	.p2align 4
	.globl	uprv_decNumberCopy_67
	.type	uprv_decNumberCopy_67, @function
uprv_decNumberCopy_67:
.LFB2105:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	cmpq	%rsi, %rdi
	je	.L2812
	movzbl	8(%rsi), %edx
	movb	%dl, 8(%rdi)
	movl	4(%rsi), %edx
	movl	%edx, 4(%rdi)
	movl	(%rsi), %edx
	movl	%edx, (%rdi)
	movzbl	9(%rsi), %edx
	movb	%dl, 9(%rdi)
	movl	(%rsi), %edx
	cmpl	$1, %edx
	jle	.L2812
	leaq	10(%rdi), %r9
	movslq	%edx, %rcx
	leaq	9(%rsi), %rdi
	cmpl	$49, %edx
	jg	.L2814
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rcx), %ecx
.L2814:
	addq	%rcx, %rdi
	leaq	10(%rsi), %rcx
	cmpq	%rcx, %rdi
	jbe	.L2812
	movq	%rdi, %rdx
	subq	%rsi, %rdx
	subq	$11, %rdx
	cmpq	$14, %rdx
	jbe	.L2815
	leaq	25(%rax), %rdx
	subq	%rcx, %rdx
	cmpq	$30, %rdx
	jbe	.L2815
	movq	%rdi, %rdx
	subq	%rsi, %rdx
	leaq	-10(%rdx), %r10
	movl	$10, %edx
	movq	%r10, %r8
	andq	$-16, %r8
	addq	$10, %r8
	.p2align 4,,10
	.p2align 3
.L2816:
	movdqu	(%rsi,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L2816
	movq	%r10, %rsi
	andq	$-16, %rsi
	leaq	(%rcx,%rsi), %rdx
	leaq	(%r9,%rsi), %rcx
	cmpq	%rsi, %r10
	je	.L2812
	movzbl	(%rdx), %esi
	movb	%sil, (%rcx)
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L2812
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rcx)
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L2812
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rcx)
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L2812
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rcx)
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L2812
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rcx)
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L2812
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rcx)
	leaq	6(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L2812
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rcx)
	leaq	7(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L2812
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rcx)
	leaq	8(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L2812
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rcx)
	leaq	9(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L2812
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rcx)
	leaq	10(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L2812
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rcx)
	leaq	11(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L2812
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rcx)
	leaq	12(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L2812
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rcx)
	leaq	13(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L2812
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rcx)
	leaq	14(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L2812
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L2815:
	subq	%rsi, %rdi
	movl	$10, %edx
	.p2align 4,,10
	.p2align 3
.L2819:
	movzbl	(%rsi,%rdx), %ecx
	movb	%cl, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L2819
.L2812:
	ret
	.cfi_endproc
.LFE2105:
	.size	uprv_decNumberCopy_67, .-uprv_decNumberCopy_67
	.p2align 4
	.type	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj, @function
_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj:
.LFB2117:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movzbl	8(%rdx), %edi
	movzbl	8(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edi, %r9d
	movl	%ecx, %eax
	xorl	%r8d, %r9d
	xorl	%r9d, %eax
	movb	%al, -184(%rbp)
	movl	%ecx, %eax
	orl	%edi, %eax
	movl	%eax, %r11d
	andl	$112, %r11d
	movb	%r11b, -200(%rbp)
	je	.L2828
	testb	$48, %al
	jne	.L2956
	testb	$64, %cl
	jne	.L2957
	movl	%r9d, %ecx
.L2951:
	andl	$-128, %ecx
	movq	$1, (%r12)
	orl	$64, %ecx
	movb	$0, 9(%r12)
	movb	%cl, 8(%r12)
.L2830:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2958
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2828:
	.cfi_restore_state
	movzbl	9(%rsi), %eax
	movl	%r8d, %ebx
	movb	%al, -208(%rbp)
	testb	%al, %al
	je	.L2959
.L2834:
	movl	0(%r13), %eax
	movl	4(%rdx), %r14d
	movl	%eax, -192(%rbp)
	movzbl	9(%rdx), %eax
	movl	%eax, %r10d
	movb	%al, -216(%rbp)
	movslq	(%rdx), %rax
	testb	%r10b, %r10b
	jne	.L2843
	cmpl	$1, %eax
	je	.L2960
.L2843:
	movl	4(%rsi), %r10d
	movl	%r14d, %r11d
	movl	%r10d, %r8d
	movl	%r10d, -204(%rbp)
	movslq	(%rsi), %r10
	subl	%r8d, %r11d
	movl	%r11d, %r8d
	jne	.L2847
	movl	$1, %r9d
	cmpl	$1, %eax
	jle	.L2885
.L2848:
	cmpb	$0, -184(%rbp)
	js	.L2853
.L2864:
	cmpl	%eax, %r10d
	leaq	9(%r12), %rbx
	cmovge	%r10, %rax
	cmpl	%eax, -192(%rbp)
	jle	.L2865
	cmpq	%rdx, %r12
	jne	.L2893
	testl	%r8d, %r8d
	jle	.L2893
.L2865:
	leal	1(%rax), %edi
	cmpl	$49, %eax
	jg	.L2868
	leaq	_ZL8d2utable(%rip), %rdi
	movzbl	(%rdi,%rax), %edi
	addl	$1, %edi
.L2868:
	xorl	%r14d, %r14d
	leaq	-160(%rbp), %r10
	cmpl	$92, %edi
	jg	.L2961
.L2866:
	andl	$-128, %ecx
	movb	%cl, 8(%r12)
	movl	4(%rsi), %eax
	movl	%eax, 4(%r12)
	movslq	(%rdx), %rcx
	cmpl	$49, %ecx
	jg	.L2869
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L2869:
	movslq	(%rsi), %r11
	addq	$9, %rdx
	cmpl	$49, %r11d
	jg	.L2870
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%r11), %r11d
.L2870:
	subq	$8, %rsp
	leaq	9(%rsi), %rdi
	movl	%r11d, %esi
	movq	%r10, -200(%rbp)
	pushq	%r9
	movq	%r10, %r9
	call	_ZL13decUnitAddSubPKhiS0_iiPhi
	movq	-200(%rbp), %r10
	movl	%eax, %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	js	.L2871
	movl	%ecx, (%r12)
.L2872:
	movl	$0, -164(%rbp)
	leaq	-164(%rbp), %r8
	cmpq	%rbx, %r10
	je	.L2873
	cmpl	%ecx, -192(%rbp)
	jge	.L2874
	cmpl	$49, %ecx
	jg	.L2875
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L2875:
	movslq	%ecx, %rax
	leaq	-1(%r10,%rax), %rax
	cmpq	%rax, %r10
	ja	.L2876
.L2877:
	cmpb	$0, (%rax)
	jne	.L2876
	cmpl	$1, %ecx
	je	.L2876
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rax, %r10
	jbe	.L2877
	.p2align 4,,10
	.p2align 3
.L2876:
	movl	%ecx, (%r12)
.L2874:
	leaq	-164(%rbp), %r8
	movq	%r15, %r9
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, -192(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	(%r12), %ecx
	movq	-192(%rbp), %r8
.L2873:
	cmpl	$49, %ecx
	jg	.L2878
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L2878:
	movslq	%ecx, %rax
	leaq	-1(%rbx,%rax), %rax
	cmpq	%rbx, %rax
	jb	.L2879
	leaq	8(%r12), %rdx
.L2880:
	cmpb	$0, (%rax)
	jne	.L2879
	cmpl	$1, %ecx
	je	.L2879
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rdx, %rax
	jne	.L2880
	.p2align 4,,10
	.p2align 3
.L2879:
	movl	%ecx, (%r12)
	movq	%r8, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	cmpb	$0, 9(%r12)
	jne	.L2881
	cmpl	$1, (%r12)
	je	.L2962
.L2881:
	testq	%r14, %r14
	je	.L2830
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L2830
	.p2align 4,,10
	.p2align 3
.L2959:
	cmpl	$1, (%rsi)
	jne	.L2834
	testb	$112, %cl
	jne	.L2834
	movl	4(%rsi), %r14d
	movb	%dil, 8(%r12)
	leaq	9(%rdx), %r10
	movq	%r15, %r9
	movl	4(%rdx), %eax
	leaq	-164(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, -192(%rbp)
	movl	%eax, 4(%r12)
	movl	(%rdx), %ecx
	movq	%r10, %rdx
	movl	$0, -164(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movzbl	8(%r12), %eax
	movl	4(%r12), %edx
	movq	-192(%rbp), %r8
	xorl	%ebx, %eax
	movl	%r14d, %ebx
	subl	%edx, %ebx
	cmpb	$0, 9(%r12)
	movb	%al, 8(%r12)
	jne	.L2835
	cmpl	$1, (%r12)
	je	.L2963
.L2835:
	testl	%ebx, %ebx
	jns	.L2838
.L2955:
	movl	(%r12), %esi
	movl	0(%r13), %ecx
	movl	%esi, %eax
	subl	%ebx, %eax
	cmpl	%ecx, %eax
	jle	.L2845
	movl	%esi, %ebx
	orl	$2048, (%r15)
	movl	(%r12), %eax
	subl	%ecx, %ebx
	jne	.L2964
.L2842:
	addl	%ebx, 4(%r12)
	movl	%eax, (%r12)
	jmp	.L2838
	.p2align 4,,10
	.p2align 3
.L2960:
	testb	$112, %dil
	je	.L2965
	movl	4(%rsi), %r11d
	movl	%r14d, %r8d
	movslq	(%rsi), %r10
	movl	%r11d, -204(%rbp)
	subl	%r11d, %r8d
	je	.L2885
	.p2align 4,,10
	.p2align 3
.L2847:
	testl	%r8d, %r8d
	js	.L2966
.L2858:
	movl	-192(%rbp), %r14d
	leal	(%r8,%rax), %r11d
	leal	1(%r14,%r10), %r9d
	cmpl	%r9d, %r11d
	jg	.L2967
	movl	$1, %r9d
	cmpl	$48, %r8d
	jg	.L2863
	leal	1(%r8), %eax
	leaq	_ZL8d2utable(%rip), %rdi
	cltq
	movzbl	(%rdi,%rax), %eax
	leaq	_ZL9DECPOWERS(%rip), %rdi
	subl	$1, %eax
	subl	%eax, %r8d
	movslq	%r8d, %r8
	movl	(%rdi,%r8,4), %r9d
	movl	%eax, %r8d
.L2863:
	cmpb	$0, -184(%rbp)
	movslq	%r11d, %rax
	jns	.L2864
.L2853:
	negl	%r9d
	jmp	.L2864
	.p2align 4,,10
	.p2align 3
.L2957:
	andl	$64, %edi
	je	.L2951
	cmpb	$0, -184(%rbp)
	jns	.L2951
	orl	$128, (%r15)
	jmp	.L2830
	.p2align 4,,10
	.p2align 3
.L2956:
	movq	%r15, %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	jmp	.L2830
	.p2align 4,,10
	.p2align 3
.L2967:
	subl	%eax, %r14d
	movsbl	-184(%rbp), %eax
	movq	%r15, %r9
	movq	%r13, %rsi
	movb	%dil, 8(%r12)
	leaq	9(%rdx), %r10
	leaq	-164(%rbp), %r8
	movq	%r12, %rdi
	sarl	$31, %eax
	movq	%r8, -184(%rbp)
	orl	$1, %eax
	movl	%eax, -164(%rbp)
	movl	4(%rdx), %eax
	movl	%eax, 4(%r12)
	movl	(%rdx), %ecx
	movq	%r10, %rdx
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	testl	%r14d, %r14d
	movq	-184(%rbp), %r8
	jle	.L2862
	movl	(%r12), %esi
	leaq	9(%r12), %rdi
	movl	%r14d, %edx
	call	_ZL14decShiftToMostPhii.part.0
	subl	%r14d, 4(%r12)
	movq	-184(%rbp), %r8
	movl	%eax, (%r12)
.L2862:
	cmpb	$0, -200(%rbp)
	jne	.L2838
	xorb	%bl, 8(%r12)
.L2838:
	movq	%r15, %rcx
	movq	%r8, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	jmp	.L2830
	.p2align 4,,10
	.p2align 3
.L2966:
	movl	-204(%rbp), %r8d
	movl	%ecx, %edi
	movl	%eax, %ecx
	movl	%r10d, %eax
	movslq	%ecx, %r10
	movl	%r9d, %ecx
	movq	%rsi, %r9
	movb	$1, -200(%rbp)
	movq	%rdx, %rsi
	subl	%r14d, %r8d
	movq	%r9, %rdx
	jmp	.L2858
	.p2align 4,,10
	.p2align 3
.L2885:
	cmpl	%r14d, 8(%r13)
	jg	.L2890
	movl	4(%r13), %edi
	movl	-192(%rbp), %ebx
	subl	%ebx, %edi
	addl	$1, %edi
	cmpl	%r14d, %edi
	jl	.L2890
	cmpl	%eax, %ebx
	jl	.L2890
	cmpl	%r10d, %ebx
	jl	.L2890
	cmpb	$0, -184(%rbp)
	movzbl	-208(%rbp), %edi
	movzbl	-216(%rbp), %r8d
	js	.L2849
	addl	%r8d, %edi
	cmpl	$9, %edi
	jg	.L2850
	testl	%r10d, %r10d
	jle	.L2968
.L2851:
	cmpq	%r12, %rsi
	je	.L2852
	movq	%r12, %rdi
	call	uprv_decNumberCopy_67
.L2852:
	movzbl	-216(%rbp), %eax
	addb	-208(%rbp), %al
	movb	%al, 9(%r12)
	jmp	.L2830
	.p2align 4,,10
	.p2align 3
.L2890:
	movl	$1, %r9d
	xorl	%r8d, %r8d
	jmp	.L2848
	.p2align 4,,10
	.p2align 3
.L2871:
	negl	%ecx
	addb	$-128, 8(%r12)
	movl	%ecx, (%r12)
	jmp	.L2872
	.p2align 4,,10
	.p2align 3
.L2961:
	movslq	%edi, %rdi
	movq	%rdx, -224(%rbp)
	movq	%rsi, -216(%rbp)
	movl	%r9d, -208(%rbp)
	movl	%r8d, -204(%rbp)
	movb	%cl, -200(%rbp)
	call	uprv_malloc_67@PLT
	movzbl	-200(%rbp), %ecx
	movl	-204(%rbp), %r8d
	testq	%rax, %rax
	movl	-208(%rbp), %r9d
	movq	%rax, %r10
	movq	-216(%rbp), %rsi
	movq	-224(%rbp), %rdx
	je	.L2969
	movq	%rax, %r14
	jmp	.L2866
	.p2align 4,,10
	.p2align 3
.L2893:
	movq	%rbx, %r10
	xorl	%r14d, %r14d
	jmp	.L2866
	.p2align 4,,10
	.p2align 3
.L2962:
	movzbl	8(%r12), %eax
	testb	$112, %al
	jne	.L2881
	cmpb	$0, -184(%rbp)
	jns	.L2881
	testb	$32, (%r15)
	jne	.L2881
	cmpl	$6, 12(%r13)
	je	.L2970
	andl	$127, %eax
	movb	%al, 8(%r12)
	jmp	.L2881
	.p2align 4,,10
	.p2align 3
.L2965:
	movb	%cl, 8(%r12)
	movl	4(%rsi), %eax
	leaq	9(%rsi), %rdx
	movq	%r15, %r9
	leaq	-164(%rbp), %r8
	movq	%r12, %rdi
	movl	%r14d, %ebx
	movl	$0, -164(%rbp)
	movl	%eax, 4(%r12)
	movl	(%rsi), %ecx
	movq	%r13, %rsi
	movq	%r8, -184(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	4(%r12), %edx
	movq	-184(%rbp), %r8
	subl	%edx, %ebx
	jns	.L2838
	jmp	.L2955
	.p2align 4,,10
	.p2align 3
.L2845:
	subl	%r14d, %edx
.L2883:
	leaq	9(%r12), %rdi
	movq	%r8, -184(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	movq	-184(%rbp), %r8
	jmp	.L2842
.L2963:
	testb	$112, %al
	jne	.L2835
	testl	%ebx, %ebx
	jns	.L2836
	movl	%r14d, 4(%r12)
.L2836:
	cmpb	$0, -184(%rbp)
	jns	.L2838
	cmpl	$6, 12(%r13)
	je	.L2839
	movb	$0, 8(%r12)
	jmp	.L2838
.L2964:
	movl	%ecx, %edx
	subl	%esi, %edx
	movl	%eax, %esi
	jmp	.L2883
.L2849:
	subl	%r8d, %edi
	testl	%edi, %edi
	jle	.L2891
	cmpq	%r12, %rsi
	je	.L2854
	movq	%r12, %rdi
	call	uprv_decNumberCopy_67
.L2854:
	movzbl	-208(%rbp), %eax
	subb	-216(%rbp), %al
	movb	%al, 9(%r12)
	movslq	(%r12), %rax
	cmpl	$49, %eax
	jg	.L2855
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
.L2855:
	movslq	%eax, %rdx
	leaq	9(%r12), %rcx
	leaq	8(%r12,%rdx), %rdx
	cmpq	%rdx, %rcx
	ja	.L2856
	leaq	8(%r12), %rcx
.L2857:
	cmpb	$0, (%rdx)
	jne	.L2856
	cmpl	$1, %eax
	je	.L2856
	subq	$1, %rdx
	subl	$1, %eax
	cmpq	%rcx, %rdx
	jne	.L2857
.L2856:
	movl	%eax, (%r12)
	jmp	.L2830
.L2968:
	movslq	%r10d, %r8
	leaq	_ZL9DECPOWERS(%rip), %r9
	cmpl	%edi, (%r9,%r8,4)
	jg	.L2851
.L2850:
	cmpl	%r10d, %eax
	leaq	9(%r12), %rbx
	cmovl	%r10, %rax
	cmpl	%eax, -192(%rbp)
	jle	.L2896
	movq	%rbx, %r10
	xorl	%r8d, %r8d
	movl	$1, %r9d
	xorl	%r14d, %r14d
	jmp	.L2866
.L2970:
	orl	$-128, %eax
	movb	%al, 8(%r12)
	jmp	.L2881
.L2891:
	movl	$1, %r9d
	xorl	%r8d, %r8d
	jmp	.L2853
.L2896:
	xorl	%r8d, %r8d
	movl	$1, %r9d
	jmp	.L2865
.L2958:
	call	__stack_chk_fail@PLT
.L2839:
	movb	$-128, 8(%r12)
	jmp	.L2838
.L2969:
	orl	$16, (%r15)
	jmp	.L2830
	.cfi_endproc
.LFE2117:
	.size	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj, .-_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj
	.p2align 4
	.globl	uprv_decNumberFMA_67
	.type	uprv_decNumberFMA_67, @function
uprv_decNumberFMA_67:
.LFB2071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	subq	$176, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -180(%rbp)
	testb	$112, 8(%rsi)
	je	.L3047
	testb	$112, 8(%rdx)
	je	.L3048
.L2979:
	testb	$112, 8(%r13)
	je	.L3039
.L3046:
	movl	(%r14), %eax
.L2986:
	movq	16(%r12), %rdx
	movdqu	(%r12), %xmm0
	movabsq	$-4294967290705032705, %rcx
	addl	(%rsi), %eax
	movq	%rdx, -160(%rbp)
	movl	24(%r12), %edx
	leal	11(%rax), %edi
	movaps	%xmm0, -176(%rbp)
	movl	%edx, -152(%rbp)
	movl	%eax, -176(%rbp)
	movq	%rcx, -172(%rbp)
	cmpl	$49, %eax
	jg	.L2993
	cltq
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %edi
	addl	$11, %edi
.L2993:
	cmpl	$84, %edi
	jbe	.L2994
	movq	%rsi, -200(%rbp)
	call	uprv_malloc_67@PLT
	movq	-200(%rbp), %rsi
	testq	%rax, %rax
	je	.L3049
	leaq	-180(%rbp), %r9
	leaq	-176(%rbp), %rcx
	movq	%r14, %rdx
	movq	%rax, %rdi
	movq	%r9, %r8
	movq	%r9, -208(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	movl	-180(%rbp), %eax
	movq	-200(%rbp), %r10
	movq	-208(%rbp), %r9
	testb	$-128, %al
	jne	.L3050
	movq	%r10, %rsi
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%r10, -200(%rbp)
	call	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj
	movq	-200(%rbp), %r10
.L2998:
	movq	%r10, %rdi
	call	uprv_free_67@PLT
.L2997:
	movl	-180(%rbp), %esi
	testl	%esi, %esi
	je	.L2999
	movl	%esi, %eax
	andl	$221, %eax
	jmp	.L2996
	.p2align 4,,10
	.p2align 3
.L3039:
	cmpl	$999999, (%r12)
	jle	.L3008
.L2987:
	movl	$64, -180(%rbp)
	movl	$64, %esi
.L2989:
	movl	%esi, %eax
	andl	$221, %eax
.L3002:
	testl	%eax, %eax
	jne	.L3003
.L3000:
	movq	%r12, %rdi
	call	uprv_decContextSetStatus_67@PLT
.L2999:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3051
	addq	$176, %rsp
	movq	%r15, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3047:
	.cfi_restore_state
	cmpl	$999999, (%r8)
	jle	.L3052
.L2980:
	movl	$64, -180(%rbp)
	movl	$64, %esi
.L3003:
	movl	$32, %ecx
	movq	$1, (%r15)
	movw	%cx, 8(%r15)
	jmp	.L3000
	.p2align 4,,10
	.p2align 3
.L2994:
	leaq	-180(%rbp), %r9
	leaq	-128(%rbp), %r10
	movq	%r14, %rdx
	movq	%r9, %r8
	movq	%r10, %rdi
	leaq	-176(%rbp), %rcx
	movq	%r9, -208(%rbp)
	movq	%r10, -200(%rbp)
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	movl	-180(%rbp), %esi
	movq	-200(%rbp), %r10
	movq	-208(%rbp), %r9
	testb	$-128, %sil
	je	.L3053
	testl	$1073741824, %esi
	je	.L3012
	movq	%r10, %rsi
	xorl	%r10d, %r10d
.L3011:
	xorl	%edi, %edi
	leaq	-140(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movw	%di, -132(%rbp)
	movq	%r15, %rdi
	movq	%r10, -200(%rbp)
	movq	$1, -140(%rbp)
	call	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj
	movq	-200(%rbp), %r10
	testq	%r10, %r10
	jne	.L2998
	jmp	.L2997
	.p2align 4,,10
	.p2align 3
.L3048:
	cmpl	$999999, (%r8)
	jg	.L2980
.L3009:
	cmpl	$999999, 4(%r12)
	jg	.L2980
	cmpl	$-999999, 8(%r12)
	jl	.L2980
	movl	(%r14), %eax
	cmpl	$999999, %eax
	jg	.L2983
	movl	4(%r14), %edx
	addl	%eax, %edx
	addl	$1999996, %edx
	cmpl	$2999996, %edx
	jbe	.L2984
	cmpb	$0, 9(%r14)
	jne	.L2983
	cmpl	$1, %eax
	jne	.L2983
.L2984:
	testb	$112, 8(%r13)
	jne	.L2986
	.p2align 4,,10
	.p2align 3
.L3008:
	cmpl	$999999, 4(%r12)
	jg	.L2987
	cmpl	$-999999, 8(%r12)
	jl	.L2987
	movl	0(%r13), %eax
	cmpl	$999999, %eax
	jg	.L2990
	movl	4(%r13), %edx
	addl	%eax, %edx
	addl	$1999996, %edx
	cmpl	$2999996, %edx
	jbe	.L3046
	cmpb	$0, 9(%r13)
	jne	.L2990
	cmpl	$1, %eax
	je	.L3046
	.p2align 4,,10
	.p2align 3
.L2990:
	movl	$128, -180(%rbp)
	movl	$128, %esi
	jmp	.L2989
	.p2align 4,,10
	.p2align 3
.L3052:
	cmpl	$999999, 4(%r8)
	jg	.L2980
	cmpl	$-999999, 8(%r8)
	jl	.L2980
	movl	(%rsi), %eax
	cmpl	$999999, %eax
	jg	.L2983
	movl	4(%rsi), %edx
	addl	%eax, %edx
	addl	$1999996, %edx
	cmpl	$2999996, %edx
	jbe	.L2977
	cmpb	$0, 9(%rsi)
	jne	.L2983
	cmpl	$1, %eax
	jne	.L2983
.L2977:
	testb	$112, 8(%r14)
	jne	.L2979
	jmp	.L3009
	.p2align 4,,10
	.p2align 3
.L3050:
	testl	$1073741824, %eax
	jne	.L3054
	movl	$32, %edx
	movq	$1, (%r15)
	movw	%dx, 8(%r15)
	jmp	.L2998
	.p2align 4,,10
	.p2align 3
.L2983:
	movl	$128, -180(%rbp)
	movl	$128, %esi
	jmp	.L3003
	.p2align 4,,10
	.p2align 3
.L3053:
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r10, %rsi
	movq	%r15, %rdi
	call	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj
	jmp	.L2997
	.p2align 4,,10
	.p2align 3
.L3012:
	movl	$32, %eax
	movq	$1, (%r15)
	movw	%ax, 8(%r15)
	movl	%esi, %eax
	andl	$221, %eax
	jmp	.L3002
.L3051:
	call	__stack_chk_fail@PLT
.L3049:
	movl	-180(%rbp), %eax
	movl	%eax, %esi
	andl	$221, %eax
	orl	$16, %esi
	orl	$16, %eax
	movl	%esi, -180(%rbp)
.L2996:
	testl	%eax, %eax
	je	.L3000
	testl	$1073741824, %esi
	je	.L3003
	andl	$-1073741825, %esi
	jmp	.L3000
.L3054:
	movq	%r10, %rsi
	jmp	.L3011
	.cfi_endproc
.LFE2071:
	.size	uprv_decNumberFMA_67, .-uprv_decNumberFMA_67
	.p2align 4
	.globl	uprv_decNumberNextPlus_67
	.type	uprv_decNumberNextPlus_67, @function
uprv_decNumberNextPlus_67:
.LFB2082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -40
	movdqu	(%rdx), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdx), %rax
	movl	$0, -84(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -64(%rbp)
	movl	24(%rdx), %eax
	movl	%eax, -56(%rbp)
	movzbl	8(%rsi), %eax
	andl	$-64, %eax
	cmpb	$-64, %al
	je	.L3069
	movl	$256, %edx
	leaq	-80(%rbp), %rcx
	leaq	-84(%rbp), %r9
	xorl	%r8d, %r8d
	movabsq	$-4294967295999999999, %rax
	movw	%dx, -44(%rbp)
	leaq	-52(%rbp), %rdx
	movq	%rax, -52(%rbp)
	movl	$0, -68(%rbp)
	call	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj
	movl	-84(%rbp), %eax
	movl	%eax, %esi
	andl	$1073741952, %esi
	movl	%esi, -84(%rbp)
	jne	.L3070
.L3058:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3071
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3070:
	.cfi_restore_state
	movl	%eax, %edx
	andl	$128, %edx
	je	.L3059
	testl	$1073741824, %eax
	jne	.L3061
	movl	$32, %eax
	movq	$1, 0(%r13)
	movw	%ax, 8(%r13)
.L3059:
	movq	%r12, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L3058
	.p2align 4,,10
	.p2align 3
.L3069:
	movslq	(%rdx), %rax
	leaq	9(%rdi), %rcx
	movl	%eax, (%rdi)
	cmpl	$1, %eax
	jle	.L3057
	leal	-2(%rax), %ebx
	movq	%rcx, %rdi
	movl	$9, %esi
	addq	$1, %rbx
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	movl	$1, %eax
	addq	%rbx, %rcx
.L3057:
	leaq	_ZL9DECPOWERS(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	subl	$1, %eax
	movb	%al, (%rcx)
	movb	$0, 8(%r13)
	movl	4(%r12), %eax
	subl	(%r12), %eax
	movb	$-128, 8(%r13)
	addl	$1, %eax
	movl	%eax, 4(%r13)
	jmp	.L3058
	.p2align 4,,10
	.p2align 3
.L3061:
	movl	%edx, %esi
	jmp	.L3059
.L3071:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2082:
	.size	uprv_decNumberNextPlus_67, .-uprv_decNumberNextPlus_67
	.p2align 4
	.globl	uprv_decNumberNextToward_67
	.type	uprv_decNumberNextToward_67, @function
uprv_decNumberNextToward_67:
.LFB2083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$64, %rsp
	movdqu	(%rcx), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rcx), %rax
	movl	$0, -84(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -64(%rbp)
	movl	24(%rcx), %eax
	movl	%eax, -56(%rbp)
	movzbl	8(%rsi), %eax
	testb	$48, %al
	jne	.L3073
	movzbl	8(%rdx), %edx
	testb	$48, %dl
	je	.L3163
	testb	$16, %al
	jne	.L3124
.L3125:
	testb	$16, %dl
	je	.L3078
	movq	%rbx, %r13
	movl	$1073741952, -84(%rbp)
	movl	(%r14), %ebx
	movl	%edx, %eax
	movl	$1073741952, %r8d
	cmpl	%ebx, 0(%r13)
	jg	.L3079
	.p2align 4,,10
	.p2align 3
.L3168:
	cmpq	%r13, %r12
	je	.L3162
	movb	%al, 8(%r12)
	movl	4(%r13), %edx
	movl	-84(%rbp), %r8d
	movl	%edx, 4(%r12)
	movl	0(%r13), %edx
	movl	%edx, (%r12)
	movzbl	9(%r13), %edx
	movb	%dl, 9(%r12)
	movl	0(%r13), %edx
	cmpl	$1, %edx
	jle	.L3081
	leaq	10(%r12), %rdi
	leaq	9(%r13), %rsi
	movslq	%edx, %rcx
	cmpl	$49, %edx
	jg	.L3083
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rcx), %ecx
.L3083:
	addq	%rsi, %rcx
	leaq	10(%r13), %rdx
	cmpq	%rdx, %rcx
	jbe	.L3081
	movq	%rcx, %rax
	subq	%r13, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L3084
	leaq	25(%r12), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L3084
	movq	%rcx, %rax
	subq	%r13, %rax
	leaq	-10(%rax), %r9
	movl	$10, %eax
	movq	%r9, %rsi
	andq	$-16, %rsi
	addq	$10, %rsi
	.p2align 4,,10
	.p2align 3
.L3085:
	movdqu	0(%r13,%rax), %xmm2
	movups	%xmm2, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L3085
	movq	%r9, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%rdi,%rsi), %rdx
	cmpq	%rsi, %r9
	je	.L3162
	movzbl	(%rax), %esi
	movb	%sil, (%rdx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3162
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3162
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3162
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3162
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3162
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3162
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3162
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3162
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3162
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3162
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3162
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3162
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3162
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3162
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
	jmp	.L3162
	.p2align 4,,10
	.p2align 3
.L3163:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZL10decComparePK9decNumberS1_h
	cmpl	$-2147483648, %eax
	je	.L3164
	testl	%eax, %eax
	je	.L3165
	movzbl	8(%r13), %edx
	andl	$-64, %edx
	testl	%eax, %eax
	js	.L3166
	cmpb	$64, %dl
	je	.L3167
	movl	$6, -68(%rbp)
	movl	$128, %r8d
.L3115:
	movl	$256, %edx
	leaq	-80(%rbp), %rcx
	leaq	-84(%rbp), %r9
	movq	%r13, %rsi
	movw	%dx, -44(%rbp)
	movq	%r12, %rdi
	leaq	-52(%rbp), %rdx
	movabsq	$-4294967295999999999, %rax
	movq	%rax, -52(%rbp)
	call	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj
	testb	$112, 8(%r12)
	jne	.L3120
	cmpb	$0, 9(%r12)
	movl	(%r12), %eax
	jne	.L3119
	cmpl	$1, %eax
	je	.L3120
.L3119:
	addl	4(%r12), %eax
	cmpl	8(%r14), %eax
	jg	.L3114
.L3120:
	movl	-84(%rbp), %r8d
	jmp	.L3098
	.p2align 4,,10
	.p2align 3
.L3073:
	testb	$16, %al
	je	.L3076
.L3124:
	movl	$1073741952, -84(%rbp)
	movl	$1073741952, %r8d
.L3077:
	movl	(%r14), %ebx
	cmpl	%ebx, 0(%r13)
	jle	.L3168
.L3079:
	movb	%al, 8(%r12)
	movslq	(%r14), %rsi
	leaq	9(%r12), %rdx
	leaq	9(%r13), %r9
	movq	%rdx, %rax
	movq	%r9, %rdi
	cmpl	$49, %esi
	jg	.L3089
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rsi), %ecx
	movq	%rcx, %rsi
	addq	%rdx, %rcx
	cmpq	%rdx, %rcx
	jbe	.L3092
.L3091:
	leaq	25(%r12), %rsi
	leaq	10(%r12), %r10
	cmpq	%r9, %rsi
	leaq	25(%r13), %rsi
	setbe	%r11b
	cmpq	%rsi, %rdx
	setnb	%sil
	orb	%sil, %r11b
	je	.L3148
	movq	%rcx, %rsi
	subq	%r12, %rsi
	subq	$10, %rsi
	cmpq	$14, %rsi
	seta	%r11b
	cmpq	%rcx, %r10
	setbe	%sil
	testb	%sil, %r11b
	je	.L3148
	movq	%rcx, %rax
	subq	%r12, %rax
	cmpq	%rcx, %r10
	leaq	-9(%rax), %rsi
	movl	$1, %eax
	cmova	%rax, %rsi
	movl	$9, %eax
	movq	%rsi, %rdi
	andq	$-16, %rdi
	addq	$9, %rdi
	.p2align 4,,10
	.p2align 3
.L3094:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L3094
	movq	%rsi, %rdi
	andq	$-16, %rdi
	leaq	(%rdx,%rdi), %rax
	leaq	(%r9,%rdi), %rdx
	cmpq	%rdi, %rsi
	je	.L3096
	movzbl	(%rdx), %esi
	movb	%sil, (%rax)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3096
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rax)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3096
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rax)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3096
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rax)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3096
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rax)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3096
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rax)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3096
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rax)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3096
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rax)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3096
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rax)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3096
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rax)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3096
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rax)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3096
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rax)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3096
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rax)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3096
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rax)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3096
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L3096:
	movslq	(%r14), %rsi
	cmpl	$49, %esi
	jg	.L3092
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rsi), %esi
.L3092:
	movl	%esi, (%r12)
	movl	(%r14), %eax
	cmpl	%eax, %esi
	jg	.L3097
.L3162:
	movzbl	8(%r12), %eax
.L3081:
	movl	$0, 4(%r12)
	andl	$-17, %eax
	orl	$32, %eax
	movb	%al, 8(%r12)
.L3098:
	testl	%r8d, %r8d
	jne	.L3100
.L3114:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3169
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3148:
	.cfi_restore_state
	movzbl	(%rdi), %edx
	addq	$1, %rax
	addq	$1, %rdi
	movb	%dl, -1(%rax)
	cmpq	%rax, %rcx
	ja	.L3148
	jmp	.L3096
	.p2align 4,,10
	.p2align 3
.L3164:
	movl	-84(%rbp), %r8d
	orl	$16, %r8d
	movl	%r8d, -84(%rbp)
.L3100:
	testb	$-35, %r8b
	je	.L3122
	testl	$1073741824, %r8d
	je	.L3123
	andl	$-1073741825, %r8d
.L3122:
	movl	%r8d, %esi
	movq	%r14, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L3114
	.p2align 4,,10
	.p2align 3
.L3089:
	movslq	%esi, %rcx
	addq	%rdx, %rcx
	cmpq	%rcx, %rdx
	jb	.L3091
	jmp	.L3092
	.p2align 4,,10
	.p2align 3
.L3076:
	xorl	%r8d, %r8d
	testq	%rdx, %rdx
	je	.L3077
	movzbl	8(%rdx), %edx
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3078:
	movl	%eax, %ecx
	xorl	%r8d, %r8d
	andl	$32, %ecx
	cmove	%edx, %eax
	cmove	%rbx, %r13
	jmp	.L3077
	.p2align 4,,10
	.p2align 3
.L3097:
	subl	%eax, %esi
	movq	%r12, %rdi
	call	_ZL8decDecapP9decNumberi
	movzbl	8(%r12), %eax
	movl	-84(%rbp), %r8d
	jmp	.L3081
	.p2align 4,,10
	.p2align 3
.L3123:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L3122
	.p2align 4,,10
	.p2align 3
.L3167:
	movslq	(%r14), %rax
	leaq	9(%r12), %rcx
	movl	%eax, (%r12)
	cmpl	$1, %eax
	jle	.L3117
	leal	-2(%rax), %ebx
	movq	%rcx, %rdi
	movl	$9, %esi
	addq	$1, %rbx
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	movl	$1, %eax
	addq	%rbx, %rcx
.L3117:
	leaq	_ZL9DECPOWERS(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	subl	$1, %eax
	movb	%al, (%rcx)
	movb	$0, 8(%r12)
	movl	4(%r14), %eax
	subl	(%r14), %eax
	addl	$1, %eax
	movl	%eax, 4(%r12)
	jmp	.L3114
	.p2align 4,,10
	.p2align 3
.L3165:
	movzbl	8(%rbx), %eax
	andl	$-128, %eax
	movl	%eax, %esi
	cmpq	%r12, %r13
	je	.L3102
	movzbl	8(%r13), %eax
	movb	%al, 8(%r12)
	movl	4(%r13), %edx
	movl	%edx, 4(%r12)
	movl	0(%r13), %edx
	movl	%edx, (%r12)
	movzbl	9(%r13), %edx
	movb	%dl, 9(%r12)
	movl	0(%r13), %ecx
	cmpl	$1, %ecx
	jle	.L3103
	leaq	10(%r12), %r8
	leaq	9(%r13), %rdi
	movslq	%ecx, %rdx
	cmpl	$49, %ecx
	jg	.L3105
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
.L3105:
	addq	%rdi, %rdx
	leaq	10(%r13), %rcx
	cmpq	%rcx, %rdx
	jbe	.L3103
	movq	%rdx, %rax
	subq	%r13, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L3106
	leaq	25(%r13), %rax
	subq	%r8, %rax
	cmpq	$30, %rax
	jbe	.L3106
	movq	%rdx, %rax
	subq	%r13, %rax
	leaq	-10(%rax), %r9
	movl	$10, %eax
	movq	%r9, %rdi
	andq	$-16, %rdi
	addq	$10, %rdi
	.p2align 4,,10
	.p2align 3
.L3107:
	movdqu	0(%r13,%rax), %xmm3
	movups	%xmm3, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L3107
	movq	%r9, %rdi
	andq	$-16, %rdi
	addq	%rdi, %rcx
	leaq	(%r8,%rdi), %rax
	cmpq	%rdi, %r9
	je	.L3102
	movzbl	(%rcx), %edi
	movb	%dil, (%rax)
	leaq	1(%rcx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L3102
	movzbl	1(%rcx), %edi
	movb	%dil, 1(%rax)
	leaq	2(%rcx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L3102
	movzbl	2(%rcx), %edi
	movb	%dil, 2(%rax)
	leaq	3(%rcx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L3102
	movzbl	3(%rcx), %edi
	movb	%dil, 3(%rax)
	leaq	4(%rcx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L3102
	movzbl	4(%rcx), %edi
	movb	%dil, 4(%rax)
	leaq	5(%rcx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L3102
	movzbl	5(%rcx), %edi
	movb	%dil, 5(%rax)
	leaq	6(%rcx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L3102
	movzbl	6(%rcx), %edi
	movb	%dil, 6(%rax)
	leaq	7(%rcx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L3102
	movzbl	7(%rcx), %edi
	movb	%dil, 7(%rax)
	leaq	8(%rcx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L3102
	movzbl	8(%rcx), %edi
	movb	%dil, 8(%rax)
	leaq	9(%rcx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L3102
	movzbl	9(%rcx), %edi
	movb	%dil, 9(%rax)
	leaq	10(%rcx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L3102
	movzbl	10(%rcx), %edi
	movb	%dil, 10(%rax)
	leaq	11(%rcx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L3102
	movzbl	11(%rcx), %edi
	movb	%dil, 11(%rax)
	leaq	12(%rcx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L3102
	movzbl	12(%rcx), %edi
	movb	%dil, 12(%rax)
	leaq	13(%rcx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L3102
	movzbl	13(%rcx), %edi
	movb	%dil, 13(%rax)
	leaq	14(%rcx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L3102
	movzbl	14(%rcx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L3102:
	movzbl	8(%r12), %eax
.L3103:
	andl	$127, %eax
	movl	-84(%rbp), %r8d
	orl	%esi, %eax
	movb	%al, 8(%r12)
	jmp	.L3098
	.p2align 4,,10
	.p2align 3
.L3166:
	cmpb	$-64, %dl
	je	.L3170
	movl	$0, -68(%rbp)
	xorl	%r8d, %r8d
	jmp	.L3115
	.p2align 4,,10
	.p2align 3
.L3084:
	subq	%r13, %rcx
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L3088:
	movzbl	0(%r13,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L3088
	jmp	.L3162
.L3170:
	movslq	(%r14), %rax
	leaq	9(%r12), %rcx
	movl	%eax, (%r12)
	cmpl	$1, %eax
	jle	.L3113
	leal	-2(%rax), %ebx
	movq	%rcx, %rdi
	movl	$9, %esi
	addq	$1, %rbx
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	movl	$1, %eax
	addq	%rbx, %rcx
.L3113:
	leaq	_ZL9DECPOWERS(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	subl	$1, %eax
	movb	%al, (%rcx)
	movb	$0, 8(%r12)
	movl	4(%r14), %eax
	subl	(%r14), %eax
	movb	$-128, 8(%r12)
	addl	$1, %eax
	movl	%eax, 4(%r12)
	jmp	.L3114
.L3106:
	subq	%r13, %rdx
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L3110:
	movzbl	0(%r13,%rax), %ecx
	movb	%cl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L3110
	jmp	.L3102
.L3169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2083:
	.size	uprv_decNumberNextToward_67, .-uprv_decNumberNextToward_67
	.p2align 4
	.type	_ZL11decDivideOpP9decNumberPKS_S2_P10decContexthPj, @function
_ZL11decDivideOpP9decNumberPKS_S2_P10decContexthPj:
.LFB2118:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -312(%rbp)
	movzbl	8(%rdx), %edx
	movq	%rcx, -328(%rbp)
	movq	%rdi, -288(%rbp)
	movq	%rsi, -296(%rbp)
	movl	%r8d, -316(%rbp)
	movq	%r9, -304(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rsi), %eax
	movl	%eax, %ecx
	xorl	%edx, %ecx
	andl	$-128, %ecx
	movb	%cl, -318(%rbp)
	movl	%eax, %ecx
	orl	%edx, %ecx
	testb	$112, %cl
	je	.L3172
	andl	$48, %ecx
	jne	.L3366
	movzbl	-316(%rbp), %ecx
	andl	$80, %ecx
	testb	$64, %al
	jne	.L3367
	movl	$0, -196(%rbp)
	testb	%cl, %cl
	jne	.L3368
	movzbl	-318(%rbp), %eax
	cmpb	$0, -316(%rbp)
	movb	$0, 9(%rdi)
	leaq	-196(%rbp), %r12
	movq	$1, (%rdi)
	movb	%al, 8(%rdi)
	jns	.L3178
	movq	-328(%rbp), %rcx
	movl	8(%rcx), %eax
	subl	(%rcx), %eax
	addl	$1, %eax
	movl	%eax, 4(%rdi)
	movq	-304(%rbp), %rax
	orl	$1024, (%rax)
.L3178:
	movq	-304(%rbp), %rcx
	movq	-328(%rbp), %rsi
	movq	%r12, %rdx
	movq	-288(%rbp), %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	jmp	.L3174
.L3172:
	movq	-312(%rbp), %rbx
	movzbl	9(%rsi), %ecx
	cmpb	$0, 9(%rbx)
	je	.L3369
.L3179:
	movq	-296(%rbp), %rbx
	movl	(%rbx), %r9d
	cmpl	$1, %r9d
	je	.L3370
.L3182:
	movq	-296(%rbp), %rcx
	movl	4(%rcx), %edi
	movq	-312(%rbp), %rcx
	movl	4(%rcx), %esi
	movslq	(%rcx), %r15
	leal	(%rdi,%r9), %ecx
	leal	(%rsi,%r15), %edx
	subl	%edx, %ecx
	movl	%ecx, -272(%rbp)
	jns	.L3186
	movl	-316(%rbp), %ebx
	cmpb	$-128, %bl
	je	.L3186
	andl	$32, %ebx
	jne	.L3371
	cmpl	%esi, %edi
	jg	.L3186
	testb	$64, -316(%rbp)
	jne	.L3290
	cmpl	$-1, -272(%rbp)
	je	.L3186
.L3290:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %rcx
	leaq	-196(%rbp), %r12
	movl	$0, -196(%rbp)
	movq	-328(%rbp), %r15
	movq	-304(%rbp), %r9
	movq	%r12, %r8
	movb	%al, 8(%rbx)
	movl	4(%rcx), %eax
	leaq	9(%rcx), %rdx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movl	%eax, 4(%rbx)
	movl	(%rcx), %ecx
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r9, %rcx
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	jmp	.L3174
.L3369:
	cmpl	$1, (%rbx)
	jne	.L3179
	andl	$112, %edx
	jne	.L3179
	testb	%cl, %cl
	jne	.L3180
	movq	-296(%rbp), %rcx
	cmpl	$1, (%rcx)
	je	.L3372
.L3180:
	movq	-288(%rbp), %rax
	xorl	%ebx, %ebx
	movq	$1, (%rax)
	movw	%bx, 8(%rax)
	testb	$80, -316(%rbp)
	je	.L3181
	movq	-304(%rbp), %rax
	orl	$128, (%rax)
	jmp	.L3174
	.p2align 4,,10
	.p2align 3
.L3186:
	movq	-328(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -280(%rbp)
	leal	1(%rax), %r14d
	cmpl	$48, %eax
	jg	.L3189
	movslq	%r14d, %r14
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%r14), %ecx
	movq	%rcx, -232(%rbp)
	cmpb	$47, %cl
	ja	.L3190
	movq	$0, -344(%rbp)
	leaq	-192(%rbp), %r12
.L3191:
	movl	-280(%rbp), %eax
	leal	-1(%r15,%rax), %eax
	cmpl	%r9d, %eax
	cmovl	%r9d, %eax
	leal	2(%rax), %r13d
	cmpl	$49, %eax
	jg	.L3194
	cltq
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %r10d
	leal	2(%r10), %r13d
.L3194:
	movl	-316(%rbp), %ecx
	leal	1(%r13), %eax
	movb	%cl, -319(%rbp)
	testb	%cl, %cl
	js	.L3195
	leal	2(%r13), %edx
	movl	%eax, %r13d
	movl	%edx, %eax
.L3195:
	leaq	-144(%rbp), %rbx
	movq	$0, -352(%rbp)
	movq	%rbx, -216(%rbp)
	cmpl	$73, %eax
	jg	.L3373
.L3196:
	movq	-216(%rbp), %rcx
	movslq	%r13d, %rax
	leaq	-1(%rcx,%rax), %rbx
	movq	-296(%rbp), %rax
	movslq	%r9d, %rcx
	leaq	-1(%rcx), %r8
	addq	$9, %rax
	cmpl	$49, %r9d
	jg	.L3200
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rcx), %r8d
	subq	$1, %r8
.L3200:
	movq	%r8, %r14
	addq	%rax, %r14
	jc	.L3374
	subq	%r14, %rax
	movq	%r14, %rdx
	subq	-296(%rbp), %rdx
	movq	%rcx, -240(%rbp)
	leaq	(%rbx,%rax), %rdi
	subq	$8, %rdx
	leaq	(%r14,%rax), %rsi
	movl	%r9d, -220(%rbp)
	call	memcpy@PLT
	movq	-296(%rbp), %rcx
	movq	%rbx, %rax
	movl	-220(%rbp), %r9d
	subq	%r14, %rax
	leaq	8(%rax,%rcx), %rax
	movq	-240(%rbp), %rcx
.L3205:
	cmpq	%rax, -216(%rbp)
	ja	.L3206
	movq	-216(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rcx, -240(%rbp)
	movl	%r9d, -220(%rbp)
	subq	%rdi, %rax
	leaq	1(%rax), %rdx
	call	memset@PLT
	movl	-220(%rbp), %r9d
	movq	-240(%rbp), %rcx
.L3206:
	movq	-312(%rbp), %rax
	leaq	9(%rax), %r14
	cmpl	$49, %r15d
	jg	.L3375
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%r15), %r15d
	leaq	-1(%r14,%r15), %rax
	movzbl	(%rax), %esi
	movq	%rax, -248(%rbp)
	leal	(%rsi,%rsi,4), %eax
	movl	%esi, -240(%rbp)
	movl	%esi, %edi
	addl	%eax, %eax
	movl	%eax, -224(%rbp)
	cmpl	$1, %r15d
	jg	.L3376
.L3207:
	movzbl	(%rbx), %r8d
	movl	-272(%rbp), %r10d
	leaq	4+_ZL9DECPOWERS(%rip), %rax
	movl	%r8d, %edx
	cmpb	$9, %r8b
	jbe	.L3211
	.p2align 4,,10
	.p2align 3
.L3208:
	addq	$4, %rax
	subl	$1, %r10d
	cmpl	%r8d, (%rax)
	jbe	.L3208
	movl	%r10d, -272(%rbp)
.L3211:
	cmpl	$9, %esi
	jbe	.L3209
	movl	-272(%rbp), %r8d
	leaq	4+_ZL9DECPOWERS(%rip), %rax
	.p2align 4,,10
	.p2align 3
.L3210:
	addq	$4, %rax
	addl	$1, %r8d
	cmpl	%esi, (%rax)
	jbe	.L3210
	movl	%r8d, -272(%rbp)
.L3209:
	cmpb	$0, -316(%rbp)
	js	.L3212
	cmpl	$49, %r9d
	jg	.L3213
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %r9d
.L3213:
	movl	%r13d, %eax
	subl	%r9d, %eax
	movl	%eax, -220(%rbp)
	movl	-272(%rbp), %eax
	testl	%eax, %eax
	js	.L3377
	movq	-216(%rbp), %rdi
	movl	$1, %edx
	movl	%r13d, %esi
	call	_ZL15decShiftToLeastPhii.part.0
	movl	-220(%rbp), %eax
	movb	$0, (%rbx)
	xorl	%edx, %edx
	addl	$1, -272(%rbp)
	subl	$1, %eax
	movl	%r13d, -220(%rbp)
	movl	%eax, -332(%rbp)
	movl	$0, -336(%rbp)
.L3217:
	movq	-232(%rbp), %rax
	movl	$0, -268(%rbp)
	movl	$0, -260(%rbp)
	leaq	-1(%r12,%rax), %rax
	movq	%rax, -256(%rbp)
	movzbl	-316(%rbp), %eax
	andl	$80, %eax
	movb	%al, -320(%rbp)
	movl	-220(%rbp), %eax
	movl	%eax, -276(%rbp)
	subl	%r15d, %eax
	movl	%eax, -232(%rbp)
	movzbl	-319(%rbp), %eax
	notl	%eax
	shrb	$7, %al
	movb	%al, -317(%rbp)
	movq	%r14, %rax
	movl	%r13d, %r14d
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L3240:
	movl	-272(%rbp), %eax
	subl	-276(%rbp), %eax
	xorl	%r12d, %r12d
	addl	-220(%rbp), %eax
	movslq	-232(%rbp), %r8
	movl	%eax, -264(%rbp)
	movq	%r13, %rax
	addq	-216(%rbp), %r8
	movl	%r14d, %r13d
	movq	%rax, %r14
	jmp	.L3218
	.p2align 4,,10
	.p2align 3
.L3380:
	movl	%eax, %edx
	negl	%eax
	movl	%eax, %r9d
.L3230:
	addl	%edx, %r12d
	movl	%r13d, %esi
	movl	%r15d, %ecx
	subl	-232(%rbp), %esi
	movq	%r14, %rdx
	movq	%r8, %rdi
	call	_ZL13decUnitAddSubPKhiS0_iiPhi.constprop.0
.L3220:
	movzbl	(%rbx), %edx
.L3218:
	cmpq	%rbx, -216(%rbp)
	jnb	.L3219
	testb	%dl, %dl
	je	.L3378
.L3219:
	cmpl	-220(%rbp), %r13d
	jl	.L3360
	je	.L3379
	movzbl	-1(%rbx), %eax
	leal	(%rdx,%rdx,4), %edx
	leal	(%rax,%rdx,2), %eax
	cltd
	idivl	-240(%rbp)
.L3229:
	testl	%eax, %eax
	jne	.L3380
	movl	$-1, %r9d
	movl	$1, %edx
	jmp	.L3230
.L3367:
	andl	$64, %edx
	orb	%cl, %dl
	jne	.L3381
	movzbl	-318(%rbp), %eax
	movq	$1, (%rdi)
	movb	$0, 9(%rdi)
	orl	$64, %eax
	movb	%al, 8(%rdi)
	jmp	.L3174
.L3366:
	movq	-328(%rbp), %rcx
	movq	-312(%rbp), %rdx
	movq	%r9, %r8
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
.L3174:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3382
	movq	-288(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3189:
	.cfi_restore_state
	movslq	%r14d, %rax
	movq	%rax, -232(%rbp)
.L3190:
	movq	-232(%rbp), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3192
	movq	-312(%rbp), %rax
	movq	%r12, -344(%rbp)
	movslq	(%rax), %r15
	movq	-296(%rbp), %rax
	movl	(%rax), %r9d
	jmp	.L3191
.L3371:
	movq	-288(%rbp), %rax
	movzbl	-318(%rbp), %ecx
	movq	$1, (%rax)
	movb	$0, 9(%rax)
	movb	%cl, 8(%rax)
	jmp	.L3174
	.p2align 4,,10
	.p2align 3
.L3378:
	subl	$1, %r13d
	subq	$1, %rbx
	jmp	.L3220
	.p2align 4,,10
	.p2align 3
.L3379:
	movq	-216(%rbp), %rdi
	movq	-248(%rbp), %rax
	movq	%rbx, %rcx
	subq	%rbx, %rdi
	addq	%rax, %rdi
	jmp	.L3226
	.p2align 4,,10
	.p2align 3
.L3383:
	cmpq	%rdi, %rax
	je	.L3225
	subq	$1, %rcx
	subq	$1, %rax
.L3226:
	xorl	%esi, %esi
	cmpq	%r14, %rax
	jb	.L3223
	movzbl	(%rax), %esi
.L3223:
	cmpb	%sil, (%rcx)
	je	.L3383
	jb	.L3360
	movzbl	-1(%rbx), %eax
	leal	(%rdx,%rdx,4), %edx
	leal	(%rax,%rdx,2), %eax
	cltd
	idivl	-224(%rbp)
	jmp	.L3229
	.p2align 4,,10
	.p2align 3
.L3360:
	movl	-260(%rbp), %r10d
	movq	%r14, %rax
	movl	%r13d, %r14d
	movq	%rax, %r13
	testl	%r10d, %r10d
	jne	.L3227
	testb	%r12b, %r12b
	jne	.L3227
	movl	$0, -260(%rbp)
	movq	-256(%rbp), %r12
.L3231:
	movq	-216(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L3236
	cmpl	$1, %r14d
	je	.L3228
.L3236:
	movl	-264(%rbp), %r8d
	subl	$1, -232(%rbp)
	testl	%r8d, %r8d
	jne	.L3292
	cmpb	$0, -317(%rbp)
	jne	.L3286
.L3292:
	subl	$1, -220(%rbp)
	movzbl	(%rbx), %edx
	movq	%r12, -256(%rbp)
	jmp	.L3240
.L3281:
	movl	$1, %r14d
	.p2align 4,,10
	.p2align 3
.L3227:
	movq	-256(%rbp), %rax
	movl	-260(%rbp), %r9d
	addl	$1, -268(%rbp)
	movb	%r12b, (%rax)
	testl	%r9d, %r9d
	je	.L3384
.L3233:
	movq	-256(%rbp), %rax
	addl	$1, -260(%rbp)
	movl	-280(%rbp), %ecx
	leaq	-1(%rax), %r12
	cmpl	%ecx, -268(%rbp)
	jle	.L3231
	movq	%r13, %rax
	movl	%r14d, %r13d
	movq	%rax, %r14
.L3235:
	movq	-256(%rbp), %r12
.L3242:
	cmpb	$0, -316(%rbp)
	movl	$0, -196(%rbp)
	js	.L3385
	movl	-268(%rbp), %eax
	addl	-264(%rbp), %eax
	cmpl	-280(%rbp), %eax
	jg	.L3268
	cmpb	$0, -320(%rbp)
	je	.L3244
	movq	-216(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-312(%rbp), %rcx
	cmpb	$0, (%rdi)
	movzbl	8(%rax), %ebx
	movl	4(%rcx), %ecx
	movl	4(%rax), %eax
	jne	.L3248
	cmpl	$1, %r13d
	je	.L3386
.L3248:
	movzbl	(%r12), %edi
	movl	-264(%rbp), %edx
	movb	%dil, -220(%rbp)
	movl	-332(%rbp), %edi
	addl	%edi, %edx
	subl	%eax, %edx
	addl	%ecx, %edx
	cmpl	%edx, %edi
	cmovle	%edi, %edx
	testl	%edx, %edx
	je	.L3249
	cmpl	%r13d, %edx
	je	.L3387
	movq	-216(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZL15decShiftToLeastPhii.part.0
	movl	%eax, %r13d
	movq	-312(%rbp), %rax
	movl	4(%rax), %ecx
	movq	-296(%rbp), %rax
	movl	4(%rax), %eax
.L3249:
	movq	-216(%rbp), %rdi
	movslq	%r13d, %rdx
	leaq	-1(%rdi,%rdx), %rdx
	cmpq	%rdx, %rdi
	ja	.L3252
.L3254:
	cmpb	$0, (%rdx)
	jne	.L3252
	cmpl	$1, %r13d
	je	.L3287
	subl	$1, %r13d
	subq	$1, %rdx
	cmpq	%rdx, -216(%rbp)
	jbe	.L3254
.L3252:
	cmpl	$49, %r13d
	jle	.L3388
	movl	%r13d, %r15d
	jmp	.L3255
.L3287:
	movl	$1, %r15d
.L3255:
	cmpl	%eax, %ecx
	cmovle	%ecx, %eax
	movl	%eax, -264(%rbp)
	testb	$16, -316(%rbp)
	jne	.L3256
.L3294:
	andl	$-128, %ebx
	movl	%r13d, -268(%rbp)
	movq	-216(%rbp), %r12
	movb	%bl, -318(%rbp)
.L3244:
	movq	-288(%rbp), %rax
	movl	-264(%rbp), %ebx
	movq	%r12, %rdx
	leaq	-196(%rbp), %r13
	movq	-328(%rbp), %r15
	movq	-304(%rbp), %r9
	movq	%r13, %r8
	movl	%ebx, 4(%rax)
	movzbl	-318(%rbp), %ebx
	movq	%rax, %rdi
	movl	-268(%rbp), %ecx
	movq	%r15, %rsi
	movb	%bl, 8(%rax)
	movq	%rax, %rbx
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r9, %rcx
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
.L3247:
	movq	-352(%rbp), %rax
	testq	%rax, %rax
	je	.L3198
	movq	%rax, %rdi
	call	uprv_free_67@PLT
.L3198:
	movq	-344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3174
	call	uprv_free_67@PLT
	jmp	.L3174
	.p2align 4,,10
	.p2align 3
.L3225:
	movq	-216(%rbp), %rax
	movq	%r14, %r13
	movb	$0, (%rax)
	addb	$1, %r12b
	jne	.L3281
	movl	-260(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L3281
	movl	$0, -260(%rbp)
	movq	-256(%rbp), %r12
.L3228:
	cmpb	$0, -320(%rbp)
	jne	.L3285
	cmpb	$0, -319(%rbp)
	jns	.L3291
	movl	-264(%rbp), %ecx
	cmpl	%ecx, -336(%rbp)
	jge	.L3285
.L3291:
	movl	$1, %r14d
	jmp	.L3236
.L3384:
	movzbl	%r12b, %edx
	cmpb	$9, %r12b
	jbe	.L3233
	movl	-268(%rbp), %ecx
	leaq	4+_ZL9DECPOWERS(%rip), %rax
	.p2align 4,,10
	.p2align 3
.L3234:
	addq	$4, %rax
	addl	$1, %ecx
	cmpl	%edx, (%rax)
	jbe	.L3234
	movl	%ecx, -268(%rbp)
	jmp	.L3233
.L3286:
	movl	$0, -264(%rbp)
	movq	%r13, %rax
	movl	%r14d, %r13d
	movq	%rax, %r14
.L3237:
	movl	-260(%rbp), %edi
	testl	%edi, %edi
	jne	.L3389
	movb	$0, (%r12)
	movl	$1, -268(%rbp)
	jmp	.L3242
.L3381:
	orl	$128, (%r9)
	jmp	.L3174
.L3375:
	movslq	%r15d, %rax
	leaq	-1(%r14,%rax), %rax
	movzbl	(%rax), %esi
	movq	%rax, -248(%rbp)
	movzbl	-1(%rax), %eax
	leal	1(%rsi), %edx
	movl	%esi, %edi
	movl	%edx, -240(%rbp)
	leal	(%rsi,%rsi,4), %edx
	leal	(%rax,%rdx,2), %eax
	movl	%eax, -224(%rbp)
.L3275:
	addl	$1, -224(%rbp)
	jmp	.L3207
.L3385:
	movq	-216(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L3293
	cmpl	$1, %r13d
	jle	.L3244
.L3293:
	movl	$1, -196(%rbp)
	jmp	.L3244
.L3212:
	movq	-296(%rbp), %rax
	movq	-312(%rbp), %rcx
	movl	4(%rax), %eax
	subl	4(%rcx), %eax
	movl	%eax, -336(%rbp)
	cmpb	%dil, %dl
	jnb	.L3279
	leal	-1(%r13), %eax
	subl	$1, -272(%rbp)
	movl	%eax, -220(%rbp)
	movl	$0, -332(%rbp)
	jmp	.L3217
.L3370:
	movl	%eax, %edx
	andl	$112, %edx
	orb	%cl, %dl
	jne	.L3182
	cmpb	$0, -316(%rbp)
	js	.L3390
	testb	$32, -316(%rbp)
	je	.L3184
	movq	-288(%rbp), %rax
	movzbl	-318(%rbp), %ebx
	movq	$1, (%rax)
	movb	$0, 9(%rax)
	movb	%bl, 8(%rax)
	jmp	.L3174
.L3373:
	movslq	%eax, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -216(%rbp)
	testq	%rax, %rax
	je	.L3197
	movq	-296(%rbp), %rcx
	movq	-312(%rbp), %rbx
	movq	%rax, -352(%rbp)
	movl	(%rcx), %r9d
	movslq	(%rbx), %r15
	jmp	.L3196
.L3393:
	movl	%ecx, %eax
	cmpl	$1, %ecx
	jle	.L3266
.L3269:
	cmpb	$9, (%r12)
	jne	.L3265
	subl	$1, %eax
	addq	$1, %r12
	cmpl	$1, %eax
	jne	.L3269
	movl	%eax, -280(%rbp)
.L3266:
	movslq	-280(%rbp), %rcx
	leaq	_ZL9DECPOWERS(%rip), %rax
	movzbl	(%r12), %edx
	movl	(%rax,%rcx,4), %eax
	subl	$1, %eax
	cmpl	%eax, %edx
	jne	.L3265
.L3268:
	movq	-304(%rbp), %rax
	orl	$4, (%rax)
	jmp	.L3247
.L3377:
	negl	%eax
	movl	%eax, %edx
	cmpl	%eax, %r13d
	jne	.L3215
	movq	-216(%rbp), %rax
	movb	$0, (%rax)
.L3216:
	movl	-272(%rbp), %eax
	movq	%rbx, %rdi
	xorl	%esi, %esi
	notl	%eax
	cltq
	leaq	1(%rax), %rdx
	subq	%rax, %rdi
	call	memset@PLT
	movl	-220(%rbp), %ecx
	movzbl	(%rbx), %edx
	movl	%r13d, -220(%rbp)
	movl	$0, -336(%rbp)
	addl	-272(%rbp), %ecx
	movl	%ecx, -332(%rbp)
	movl	$0, -272(%rbp)
	jmp	.L3217
.L3389:
	leaq	1(%r12), %rax
	movq	%rax, -256(%rbp)
	jmp	.L3235
.L3376:
	leal	1(%rsi), %eax
	movl	%eax, -240(%rbp)
	movq	-248(%rbp), %rax
	movzbl	-1(%rax), %eax
	addl	%eax, -224(%rbp)
	cmpl	$2, %r15d
	je	.L3207
	jmp	.L3275
	.p2align 4,,10
	.p2align 3
.L3285:
	movq	%r13, %r14
	movl	$1, %r13d
	jmp	.L3237
.L3368:
	movb	%al, 8(%rdi)
	movl	4(%rsi), %eax
	leaq	-196(%rbp), %r12
	leaq	9(%rsi), %rdx
	movq	%r12, %r8
	movl	%eax, 4(%rdi)
	movl	(%rsi), %ecx
	movq	-328(%rbp), %rsi
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	jmp	.L3178
.L3279:
	movl	%r13d, -220(%rbp)
	movl	$0, -332(%rbp)
	jmp	.L3217
.L3215:
	movq	-216(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZL15decShiftToLeastPhii.part.0
	jmp	.L3216
.L3374:
	movq	%rbx, %rax
	jmp	.L3205
.L3256:
	movq	-216(%rbp), %rdi
	movl	%r15d, %ecx
	movl	%r15d, %esi
	movl	$1, %r9d
	movq	%rdi, %r8
	movq	%rdi, %rdx
	call	_ZL13decUnitAddSubPKhiS0_iiPhi.constprop.0
	movl	%eax, %esi
	movq	-312(%rbp), %rax
	movl	4(%rax), %ecx
	movl	%ecx, %r8d
	movl	%ecx, -232(%rbp)
	movslq	(%rax), %rcx
	subl	-264(%rbp), %r8d
	cmpl	$49, %ecx
	jg	.L3257
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L3257:
	movq	-216(%rbp), %rdi
	movq	%r14, %rdx
	movl	%esi, -232(%rbp)
	call	_ZL14decUnitComparePKhiS0_ii
	movl	-232(%rbp), %esi
	cmpl	$-2147483648, %eax
	movl	%eax, %ecx
	je	.L3391
	movq	-216(%rbp), %rdi
	movslq	%esi, %rax
	addq	%rdi, %rax
	movq	%rdi, %rdx
	cmpq	%rax, %rdi
	jnb	.L3263
.L3259:
	movzbl	(%rdx), %esi
	movl	%esi, %edi
	shrb	%dil
	andl	$1, %esi
	movb	%dil, (%rdx)
	je	.L3262
	addb	$5, -1(%rdx)
.L3262:
	addq	$1, %rdx
	cmpq	%rax, %rdx
	jne	.L3259
.L3263:
	testl	%ecx, %ecx
	jle	.L3392
.L3260:
	movl	-280(%rbp), %ecx
	cmpl	%ecx, -268(%rbp)
	je	.L3393
.L3265:
	movq	-312(%rbp), %rax
	movl	4(%rax), %ecx
	movl	%ecx, %r8d
	movl	%ecx, -220(%rbp)
	movslq	(%rax), %rcx
	subl	-264(%rbp), %r8d
	cmpl	$49, %ecx
	jg	.L3270
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L3270:
	subq	$8, %rsp
	movq	-216(%rbp), %r9
	movl	%r15d, %esi
	movq	%r14, %rdx
	pushq	$-1
	movq	%r9, %rdi
	movq	%r9, %r15
	call	_ZL13decUnitAddSubPKhiS0_iiPhi
	popq	%rcx
	popq	%rsi
	negl	%eax
	movl	%eax, -268(%rbp)
	movl	%eax, %edx
	cltq
	leaq	-1(%r15,%rax), %rax
	cmpq	%rax, %r15
	ja	.L3271
.L3272:
	cmpb	$0, (%rax)
	jne	.L3364
	cmpl	$1, %edx
	je	.L3364
	subl	$1, %edx
	subq	$1, %rax
	cmpq	%rax, -216(%rbp)
	jbe	.L3272
.L3364:
	movl	%edx, -268(%rbp)
.L3271:
	notl	%ebx
	movq	-216(%rbp), %r12
	andl	$-128, %ebx
	movb	%bl, -318(%rbp)
	jmp	.L3244
.L3181:
	movq	%rax, %rbx
	movq	-304(%rbp), %rax
	orl	$2, (%rax)
	movzbl	-318(%rbp), %eax
	orl	$64, %eax
	movb	%al, 8(%rbx)
	jmp	.L3174
.L3184:
	movq	%rbx, %rsi
	movq	-288(%rbp), %rbx
	movq	-312(%rbp), %rax
	movq	%rbx, %rdi
	movl	4(%rax), %r11d
	call	uprv_decNumberCopy_67
	cmpl	%r11d, 4(%rbx)
	jle	.L3174
	movl	%r11d, 4(%rbx)
	jmp	.L3174
.L3390:
	movl	4(%rbx), %r11d
	movq	%rbx, %rsi
	movq	-288(%rbp), %rbx
	movl	$0, -196(%rbp)
	movq	-312(%rbp), %rax
	movq	%rbx, %rdi
	subl	4(%rax), %r11d
	call	uprv_decNumberCopy_67
	movzbl	-318(%rbp), %ecx
	movq	-328(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%r11d, 4(%rbx)
	leaq	-196(%rbp), %rdx
	movb	%cl, 8(%rbx)
	movq	-304(%rbp), %rcx
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	jmp	.L3174
.L3197:
	movq	-304(%rbp), %rax
	orl	$16, (%rax)
	jmp	.L3198
	.p2align 4,,10
	.p2align 3
.L3392:
	jne	.L3294
	testb	$1, -220(%rbp)
	jne	.L3260
	jmp	.L3294
.L3391:
	movq	-304(%rbp), %rax
	orl	$16, (%rax)
	jmp	.L3247
.L3192:
	movq	-304(%rbp), %rax
	orl	$16, (%rax)
	jmp	.L3174
.L3388:
	movslq	%r13d, %rdx
	leaq	_ZL8d2utable(%rip), %rsi
	movzbl	(%rsi,%rdx), %r15d
	jmp	.L3255
	.p2align 4,,10
	.p2align 3
.L3372:
	testb	$112, %al
	jne	.L3180
	movq	-304(%rbp), %rax
	xorl	%r12d, %r12d
	movq	$1, (%rdi)
	movw	%r12w, 8(%rdi)
	orl	$8, (%rax)
	jmp	.L3174
.L3386:
	cmpl	%ecx, %eax
	movq	-288(%rbp), %rdi
	movq	-328(%rbp), %rsi
	leaq	-196(%rbp), %rdx
	cmovg	%ecx, %eax
	andl	$-128, %ebx
	movq	-304(%rbp), %rcx
	movl	$1, (%rdi)
	movb	$0, 9(%rdi)
	movl	%eax, 4(%rdi)
	movb	%bl, 8(%rdi)
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	jmp	.L3247
.L3387:
	movq	-216(%rbp), %rdi
	movl	$1, %r13d
	movb	$0, (%rdi)
	movq	%rdi, %rdx
	jmp	.L3254
.L3382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2118:
	.size	_ZL11decDivideOpP9decNumberPKS_S2_P10decContexthPj, .-_ZL11decDivideOpP9decNumberPKS_S2_P10decContexthPj
	.p2align 4
	.globl	uprv_decNumberDivide_67
	.type	uprv_decNumberDivide_67, @function
uprv_decNumberDivide_67:
.LFB2068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$128, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	leaq	-28(%rbp), %r9
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	call	_ZL11decDivideOpP9decNumberPKS_S2_P10decContexthPj
	movl	-28(%rbp), %esi
	testl	%esi, %esi
	jne	.L3406
.L3395:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3407
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3406:
	.cfi_restore_state
	testb	$-35, %sil
	je	.L3396
	testl	$1073741824, %esi
	je	.L3397
	andl	$-1073741825, %esi
.L3396:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L3395
	.p2align 4,,10
	.p2align 3
.L3397:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L3396
.L3407:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2068:
	.size	uprv_decNumberDivide_67, .-uprv_decNumberDivide_67
	.p2align 4
	.globl	uprv_decNumberDivideInteger_67
	.type	uprv_decNumberDivideInteger_67, @function
uprv_decNumberDivideInteger_67:
.LFB2069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$32, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	leaq	-28(%rbp), %r9
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	call	_ZL11decDivideOpP9decNumberPKS_S2_P10decContexthPj
	movl	-28(%rbp), %esi
	testl	%esi, %esi
	jne	.L3420
.L3409:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3421
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3420:
	.cfi_restore_state
	testb	$-35, %sil
	je	.L3410
	testl	$1073741824, %esi
	je	.L3411
	andl	$-1073741825, %esi
.L3410:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L3409
	.p2align 4,,10
	.p2align 3
.L3411:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L3410
.L3421:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2069:
	.size	uprv_decNumberDivideInteger_67, .-uprv_decNumberDivideInteger_67
	.p2align 4
	.globl	uprv_decNumberRemainder_67
	.type	uprv_decNumberRemainder_67, @function
uprv_decNumberRemainder_67:
.LFB2092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$64, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	leaq	-28(%rbp), %r9
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	call	_ZL11decDivideOpP9decNumberPKS_S2_P10decContexthPj
	movl	-28(%rbp), %esi
	testl	%esi, %esi
	jne	.L3434
.L3423:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3435
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3434:
	.cfi_restore_state
	testb	$-35, %sil
	je	.L3424
	testl	$1073741824, %esi
	je	.L3425
	andl	$-1073741825, %esi
.L3424:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L3423
	.p2align 4,,10
	.p2align 3
.L3425:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L3424
.L3435:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2092:
	.size	uprv_decNumberRemainder_67, .-uprv_decNumberRemainder_67
	.p2align 4
	.globl	uprv_decNumberRemainderNear_67
	.type	uprv_decNumberRemainderNear_67, @function
uprv_decNumberRemainderNear_67:
.LFB2093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	leaq	-28(%rbp), %r9
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	call	_ZL11decDivideOpP9decNumberPKS_S2_P10decContexthPj
	movl	-28(%rbp), %esi
	testl	%esi, %esi
	jne	.L3448
.L3437:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3449
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3448:
	.cfi_restore_state
	testb	$-35, %sil
	je	.L3438
	testl	$1073741824, %esi
	je	.L3439
	andl	$-1073741825, %esi
.L3438:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L3437
	.p2align 4,,10
	.p2align 3
.L3439:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L3438
.L3449:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2093:
	.size	uprv_decNumberRemainderNear_67, .-uprv_decNumberRemainderNear_67
	.p2align 4
	.type	_ZL8decExpOpP9decNumberPKS_P10decContextPj, @function
_ZL8decExpOpP9decNumberPKS_P10decContextPj:
.LFB2120:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$680, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -616(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rsi), %eax
	movl	$0, -600(%rbp)
	testb	$112, %al
	je	.L3451
	testb	$64, %al
	je	.L3452
	testb	%al, %al
	jns	.L3453
	xorl	%r14d, %r14d
	movq	$1, (%rdi)
	movw	%r14w, 8(%rdi)
	jmp	.L3454
	.p2align 4,,10
	.p2align 3
.L3451:
	cmpb	$0, 9(%rsi)
	jne	.L3464
	cmpl	$1, (%rsi)
	je	.L3559
.L3464:
	movl	$1024, %ebx
	movq	%r12, %rsi
	movl	$1, -480(%rbp)
	movw	%bx, -472(%rbp)
	movq	-616(%rbp), %rbx
	movl	(%rbx), %edx
	leaq	-480(%rbp), %rbx
	movq	%rbx, %rdi
	movl	%edx, %ecx
	negl	%edx
	testb	%al, %al
	notl	%ecx
	cmovs	%ecx, %edx
	movl	%edx, -476(%rbp)
	movl	$1, %edx
	call	_ZL10decComparePK9decNumberS1_h
	cmpl	$-2147483648, %eax
	je	.L3557
	testl	%eax, %eax
	js	.L3468
	movq	-616(%rbp), %rax
	movl	$256, %r11d
	movl	(%rax), %ebx
	movq	$1, 0(%r13)
	movl	$1, %eax
	movw	%r11w, 8(%r13)
	movl	%ebx, %edx
	subl	$1, %edx
	je	.L3469
	leaq	9(%r13), %rdi
	movl	$1, %esi
	call	_ZL14decShiftToMostPhii.part.0
.L3469:
	movl	%eax, 0(%r13)
	movl	$1, %eax
	subl	%ebx, %eax
	movl	%eax, 4(%r13)
	orl	$2080, (%r14)
	.p2align 4,,10
	.p2align 3
.L3454:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3560
	addq	$680, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3452:
	.cfi_restore_state
	movq	%rdx, %rcx
	movq	%r14, %r8
	xorl	%edx, %edx
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	jmp	.L3454
	.p2align 4,,10
	.p2align 3
.L3453:
	cmpq	%rdi, %rsi
	je	.L3454
	movb	%al, 8(%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 4(%rdi)
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	movzbl	9(%rsi), %eax
	movb	%al, 9(%rdi)
	movl	(%rsi), %eax
	cmpl	$1, %eax
	jle	.L3454
	leaq	10(%rdi), %rdi
	leaq	9(%rsi), %rcx
	movslq	%eax, %rdx
	cmpl	$49, %eax
	jle	.L3561
.L3458:
	addq	%rdx, %rcx
	leaq	10(%r12), %rdx
	cmpq	%rdx, %rcx
	jbe	.L3454
	movq	%rcx, %rax
	subq	%r12, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L3459
	leaq	25(%r12), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L3459
	movq	%rcx, %rax
	subq	%r12, %rax
	leaq	-10(%rax), %r8
	movl	$10, %eax
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	$10, %rsi
	.p2align 4,,10
	.p2align 3
.L3461:
	movdqu	(%r12,%rax), %xmm0
	movups	%xmm0, 0(%r13,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L3461
	movq	%r8, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%rdi,%rsi), %rdx
	cmpq	%r8, %rsi
	je	.L3454
	movzbl	(%rax), %esi
	movb	%sil, (%rdx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3454
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3454
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3454
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3454
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3454
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3454
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3454
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3454
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3454
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3454
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3454
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3454
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3454
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3454
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
	jmp	.L3454
	.p2align 4,,10
	.p2align 3
.L3559:
	movl	$256, %r12d
	movq	$1, (%rdi)
	movw	%r12w, 8(%rdi)
	jmp	.L3454
	.p2align 4,,10
	.p2align 3
.L3557:
	orl	$16, (%r14)
	jmp	.L3454
	.p2align 4,,10
	.p2align 3
.L3468:
	leaq	-592(%rbp), %rax
	movl	$64, %esi
	movq	%rax, %rdi
	movq	%rax, -624(%rbp)
	call	uprv_decContextDefault_67@PLT
	movq	-616(%rbp), %rax
	movl	4(%r12), %esi
	movb	$0, -568(%rbp)
	movq	4(%rax), %rax
	movq	%rax, -588(%rbp)
	movslq	(%r12), %rax
	leal	(%rsi,%rax), %ecx
	cmpl	$8, %ecx
	jg	.L3562
	movl	$8, %edx
	xorl	%edi, %edi
	subl	%ecx, %edx
	cmpl	$8, %eax
	setg	%dil
	cmpl	%edi, %edx
	cmovg	%edi, %edx
	addl	%edx, %ecx
	movl	%ecx, -684(%rbp)
	js	.L3512
	movl	%eax, %r15d
	negl	%r15d
	subl	%edx, %r15d
	cmpl	%r15d, %esi
	je	.L3513
	cmpl	$49, %eax
	jg	.L3473
	movslq	%eax, %rdx
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
	addl	$11, %edx
	cmpl	$84, %edx
	ja	.L3474
	movq	$0, -672(%rbp)
	leaq	-432(%rbp), %rcx
.L3475:
	movzwl	8(%r12), %edx
	movl	%esi, 4(%rcx)
	movl	%eax, (%rcx)
	movw	%dx, 8(%rcx)
	cmpl	$1, %eax
	jg	.L3563
.L3516:
	movq	%rcx, %r12
.L3478:
	movl	%r15d, 4(%r12)
	movl	(%r12), %eax
.L3472:
	movq	-616(%rbp), %rcx
	cmpl	%eax, (%rcx)
	cmovge	(%rcx), %eax
	addl	-684(%rbp), %eax
	movl	%eax, -688(%rbp)
	addl	$2, %eax
	leal	(%rax,%rax), %esi
	movl	%eax, -640(%rbp)
	movl	%esi, -648(%rbp)
	leal	11(%rsi), %eax
	cmpl	$49, %esi
	jg	.L3482
	movslq	%esi, %rax
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	addl	$11, %eax
.L3482:
	movq	$0, -632(%rbp)
	leaq	-240(%rbp), %r10
	cmpl	$180, %eax
	ja	.L3564
.L3483:
	movl	-688(%rbp), %eax
	cmpl	$47, -640(%rbp)
	leal	15(%rax), %edi
	jg	.L3485
	addl	$4, %eax
	leaq	_ZL8d2utable(%rip), %rdx
	cltq
	movzbl	(%rdx,%rax), %edi
	addl	$11, %edi
.L3485:
	cmpl	$96, %edi
	jbe	.L3519
	movq	%r10, -656(%rbp)
	call	uprv_malloc_67@PLT
	movq	-656(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -680(%rbp)
	je	.L3565
	movq	%r12, %r15
	cmpq	%rax, %r12
	je	.L3489
	movq	%rax, %r15
.L3486:
	movq	(%r12), %rax
	movl	(%r12), %edx
	movq	%rax, (%r15)
	movzwl	8(%r12), %eax
	movw	%ax, 8(%r15)
	cmpl	$1, %edx
	jle	.L3489
	leaq	10(%r15), %rdi
	leaq	9(%r12), %rax
	movslq	%edx, %rcx
	cmpl	$49, %edx
	jg	.L3491
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rcx), %ecx
.L3491:
	addq	%rcx, %rax
	leaq	10(%r12), %rsi
	cmpq	%rsi, %rax
	jbe	.L3489
	subq	%r12, %rax
	movq	%r10, -656(%rbp)
	leaq	-10(%rax), %rdx
	call	memcpy@PLT
	movq	-656(%rbp), %r10
.L3489:
	movl	$256, %edi
	movq	$1, (%r10)
	leaq	-560(%rbp), %rax
	movl	$512, %r8d
	movw	%di, 8(%r10)
	movl	$256, %r9d
	movl	$64, %esi
	movq	%rax, %rdi
	movq	%r10, -712(%rbp)
	movq	$1, -480(%rbp)
	movw	%r8w, -472(%rbp)
	movq	$1, -492(%rbp)
	movw	%r9w, -484(%rbp)
	movq	%rax, -696(%rbp)
	call	uprv_decContextDefault_67@PLT
	movq	-544(%rbp), %rax
	movdqa	-560(%rbp), %xmm1
	leaq	-528(%rbp), %rcx
	movq	-712(%rbp), %r10
	movq	%r13, -704(%rbp)
	movq	%rax, -512(%rbp)
	movl	-536(%rbp), %eax
	movq	%r12, -656(%rbp)
	movq	-696(%rbp), %r12
	movl	%eax, -504(%rbp)
	movl	-648(%rbp), %eax
	movl	$-999999999, -552(%rbp)
	movl	%eax, -592(%rbp)
	movl	-640(%rbp), %eax
	movq	%r14, -648(%rbp)
	movq	%r10, %r14
	movl	%eax, -560(%rbp)
	leaq	-600(%rbp), %rax
	movq	%rcx, -664(%rbp)
	movq	%rax, %r13
	movaps	%xmm1, -528(%rbp)
	jmp	.L3494
	.p2align 4,,10
	.p2align 3
.L3522:
	movq	-664(%rbp), %rcx
	movq	%r13, %r9
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	leaq	-492(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj
.L3494:
	movq	-648(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	-624(%rbp), %rcx
	movq	%r14, %rdi
	call	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	-656(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	movq	%r13, %r9
	movl	$128, %r8d
	call	_ZL11decDivideOpP9decNumberPKS_S2_P10decContexthPj
	movl	(%r14), %ecx
	movl	4(%r14), %edx
	movl	-640(%rbp), %edi
	movl	4(%r15), %eax
	addl	(%r15), %eax
	leal	(%rcx,%rdx), %esi
	addl	%edi, %eax
	cmpl	%esi, %eax
	jge	.L3522
	cmpl	%edi, %ecx
	jl	.L3522
	movl	-684(%rbp), %esi
	movq	%r14, %r10
	movq	-704(%rbp), %r13
	movq	-648(%rbp), %r14
	testl	%esi, %esi
	jne	.L3566
	movzbl	9(%r10), %esi
	movzbl	8(%r10), %edi
.L3498:
	movl	$1, -596(%rbp)
	cmpl	$1, %ecx
	je	.L3499
.L3506:
	movq	-616(%rbp), %rbx
	movq	-624(%rbp), %rsi
	leaq	-596(%rbp), %r12
	movq	%r14, %r9
	movq	%r12, %r8
	movl	(%rbx), %eax
	movb	%dil, 8(%r13)
	movq	%r13, %rdi
	movl	%edx, 4(%r13)
	leaq	9(%r10), %rdx
	movl	%eax, -592(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	%r13, %rdi
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3507
	call	uprv_free_67@PLT
.L3507:
	movq	-632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3508
	call	uprv_free_67@PLT
.L3508:
	movq	-680(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3454
	call	uprv_free_67@PLT
	jmp	.L3454
	.p2align 4,,10
	.p2align 3
.L3562:
	movl	$512, %r10d
	cmpb	$0, 8(%r12)
	movq	$1, -240(%rbp)
	movw	%r10w, -232(%rbp)
	jns	.L3511
	movl	$-2, -236(%rbp)
.L3511:
	movq	$0, -632(%rbp)
	movl	$100000000, %r12d
	movl	$11, %eax
	leaq	-240(%rbp), %rbx
	movq	$0, -680(%rbp)
	leaq	-336(%rbp), %r10
	movq	$0, -672(%rbp)
.L3471:
	movl	%eax, -592(%rbp)
	movl	$256, %eax
	movl	$1, %r15d
	xorl	%edx, %edx
	movw	%ax, 8(%r10)
	movl	(%r14), %eax
	movq	%rbx, -640(%rbp)
	movq	-624(%rbp), %rbx
	movq	%r13, -648(%rbp)
	movq	%r14, %r13
	movq	%r10, %r14
	movq	$1, (%r10)
	jmp	.L3505
	.p2align 4,,10
	.p2align 3
.L3500:
	cmpl	$31, %r15d
	je	.L3556
	testl	%edx, %edx
	jne	.L3504
.L3502:
	addl	$1, %r15d
.L3505:
	testb	$34, %ah
	je	.L3496
	movzbl	9(%r14), %esi
	testb	$2, %ah
	jne	.L3567
	testb	%sil, %sil
	jne	.L3496
	cmpl	$1, (%r14)
	je	.L3568
	.p2align 4,,10
	.p2align 3
.L3496:
	addl	%r12d, %r12d
	jns	.L3500
	movq	-640(%rbp), %rdx
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	cmpl	$31, %r15d
	je	.L3556
.L3504:
	movq	%r14, %rdx
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	movl	0(%r13), %eax
	movl	$1, %edx
	jmp	.L3502
	.p2align 4,,10
	.p2align 3
.L3561:
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdx), %edx
	jmp	.L3458
	.p2align 4,,10
	.p2align 3
.L3459:
	subq	%r12, %rcx
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L3463:
	movzbl	(%r12,%rax), %edx
	movb	%dl, 0(%r13,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L3463
	jmp	.L3454
	.p2align 4,,10
	.p2align 3
.L3568:
	movzbl	8(%r14), %edi
	movl	%edi, %esi
	andl	$112, %esi
	jne	.L3496
	movl	$1, -596(%rbp)
	movq	%r14, %r10
	movq	%r13, %r14
	movq	-648(%rbp), %r13
	movl	4(%r10), %edx
	.p2align 4,,10
	.p2align 3
.L3499:
	movl	%edi, %eax
	movl	$1, %ecx
	andl	$112, %eax
	orb	%sil, %al
	jne	.L3506
	movl	$0, -596(%rbp)
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3556:
	movq	%r14, %r10
	movq	%r13, %r14
	movq	-648(%rbp), %r13
	movzbl	9(%r10), %esi
	movl	(%r10), %ecx
	movzbl	8(%r10), %edi
	movl	4(%r10), %edx
	jmp	.L3498
	.p2align 4,,10
	.p2align 3
.L3519:
	movq	$0, -680(%rbp)
	leaq	-336(%rbp), %r15
	jmp	.L3486
	.p2align 4,,10
	.p2align 3
.L3512:
	movl	$0, -684(%rbp)
	movq	$0, -672(%rbp)
	jmp	.L3472
	.p2align 4,,10
	.p2align 3
.L3567:
	movq	%r14, %r10
	movq	%r13, %r14
	movq	-648(%rbp), %r13
	movl	(%r10), %ecx
	movzbl	8(%r10), %edi
	movl	4(%r10), %edx
	jmp	.L3498
	.p2align 4,,10
	.p2align 3
.L3474:
	movl	%edx, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, -672(%rbp)
	testq	%rax, %rax
	je	.L3557
	movq	%rax, %rcx
	cmpq	%rax, %r12
	je	.L3478
	movl	4(%r12), %esi
	movslq	(%r12), %rax
	jmp	.L3475
	.p2align 4,,10
	.p2align 3
.L3473:
	leal	11(%rax), %edx
	cmpl	$84, %edx
	ja	.L3474
	movzbl	8(%r12), %edx
	movl	%esi, -428(%rbp)
	leaq	-432(%rbp), %rcx
	leaq	-422(%rbp), %rdi
	movl	%eax, -432(%rbp)
	movb	%dl, -424(%rbp)
	movzbl	9(%r12), %edx
	movq	$0, -672(%rbp)
	movb	%dl, -423(%rbp)
	leaq	9(%r12), %rdx
.L3480:
	addq	%rdx, %rax
	leaq	10(%r12), %rsi
	cmpq	%rsi, %rax
	jbe	.L3516
	subq	%r12, %rax
	movq	%rcx, -632(%rbp)
	leaq	-10(%rax), %rdx
	call	memcpy@PLT
	movq	-632(%rbp), %rcx
	movq	%rcx, %r12
	jmp	.L3478
	.p2align 4,,10
	.p2align 3
.L3564:
	movl	%eax, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, -632(%rbp)
	testq	%rax, %rax
	je	.L3569
	movq	%rax, %r10
	jmp	.L3483
.L3566:
	movl	-688(%rbp), %eax
	movslq	-684(%rbp), %rcx
	leaq	_ZL9DECPOWERS(%rip), %rdx
	movq	%r10, %rbx
	movq	%r15, %r10
	movl	(%rdx,%rcx,4), %r12d
	addl	$4, %eax
	jmp	.L3471
.L3563:
	leaq	10(%rcx), %rdi
	leaq	9(%r12), %rdx
	cmpl	$49, %eax
	jg	.L3480
	leaq	_ZL8d2utable(%rip), %rsi
	movzbl	(%rsi,%rax), %eax
	jmp	.L3480
.L3560:
	call	__stack_chk_fail@PLT
.L3565:
	movq	-672(%rbp), %rax
	orl	$16, (%r14)
	testq	%rax, %rax
	je	.L3488
	movq	%rax, %rdi
	call	uprv_free_67@PLT
	movq	-632(%rbp), %rax
	testq	%rax, %rax
	je	.L3454
.L3558:
	movq	%rax, %rdi
	call	uprv_free_67@PLT
	jmp	.L3454
.L3513:
	movq	$0, -672(%rbp)
	jmp	.L3472
.L3569:
	movq	-672(%rbp), %rax
	orl	$16, (%r14)
	testq	%rax, %rax
	jne	.L3558
	jmp	.L3454
.L3488:
	movq	-632(%rbp), %rax
	testq	%rax, %rax
	jne	.L3558
	jmp	.L3454
	.cfi_endproc
.LFE2120:
	.size	_ZL8decExpOpP9decNumberPKS_P10decContextPj, .-_ZL8decExpOpP9decNumberPKS_P10decContextPj
	.p2align 4
	.globl	uprv_decNumberExp_67
	.type	uprv_decNumberExp_67, @function
uprv_decNumberExp_67:
.LFB2070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$999999, (%rdx)
	movl	$0, -28(%rbp)
	jg	.L3571
	cmpl	$999999, 4(%rdx)
	jle	.L3592
.L3571:
	movl	$64, -28(%rbp)
	movl	$64, %esi
.L3577:
	movl	$32, %eax
	movq	$1, 0(%r13)
	movw	%ax, 8(%r13)
.L3576:
	movq	%r12, %rdi
	call	uprv_decContextSetStatus_67@PLT
.L3579:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3593
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3592:
	.cfi_restore_state
	cmpl	$-999999, 8(%rdx)
	jl	.L3571
	movl	(%rsi), %eax
	cmpl	$999999, %eax
	jg	.L3574
	movl	4(%rsi), %edx
	addl	%eax, %edx
	addl	$1999996, %edx
	cmpl	$2999996, %edx
	jbe	.L3575
	cmpb	$0, 9(%rsi)
	jne	.L3574
	cmpl	$1, %eax
	jne	.L3574
	testb	$112, 8(%rsi)
	je	.L3575
	.p2align 4,,10
	.p2align 3
.L3574:
	movl	$128, -28(%rbp)
	movl	$128, %esi
	jmp	.L3577
	.p2align 4,,10
	.p2align 3
.L3575:
	leaq	-28(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZL8decExpOpP9decNumberPKS_P10decContextPj
	movl	-28(%rbp), %esi
	testl	%esi, %esi
	je	.L3579
	testb	$-35, %sil
	je	.L3576
	testl	$1073741824, %esi
	je	.L3577
	andl	$-1073741825, %esi
	jmp	.L3576
.L3593:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2070:
	.size	uprv_decNumberExp_67, .-uprv_decNumberExp_67
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"2.302585092994045684017991454684364207601"
	.align 8
.LC18:
	.string	"0.6931471805599453094172321214581765680755"
	.text
	.p2align 4
	.type	_ZL7decLnOpP9decNumberPKS_P10decContextPj, @function
_ZL7decLnOpP9decNumberPKS_P10decContextPj:
.LFB2121:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$552, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -536(%rbp)
	movq	%rsi, -456(%rbp)
	movq	%rdx, -496(%rbp)
	movq	%rcx, -544(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rsi), %eax
	movl	$0, -428(%rbp)
	testb	$112, %al
	je	.L3595
	testb	$64, %al
	je	.L3596
	testb	%al, %al
	jns	.L3597
.L3610:
	movq	-544(%rbp), %rax
	orl	$128, (%rax)
.L3598:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3981
	movq	-536(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3595:
	.cfi_restore_state
	movzbl	9(%rsi), %ecx
	testb	%cl, %cl
	je	.L3982
.L3609:
	testb	%al, %al
	js	.L3610
	movq	-456(%rbp), %rdi
	movq	-496(%rbp), %rax
	movl	4(%rdi), %r12d
	movl	(%rax), %eax
	movl	(%rdi), %edx
	testl	%r12d, %r12d
	jne	.L3611
	cmpl	$40, %eax
	jg	.L3611
	movl	8(%rdi), %edi
	movl	%edi, %esi
	movl	%edi, -448(%rbp)
	andl	$16776960, %esi
	cmpl	$65536, %esi
	je	.L3983
.L3612:
	cmpb	$2, %cl
	je	.L3984
	.p2align 4,,10
	.p2align 3
.L3611:
	cmpl	$7, %eax
	movl	$7, %ecx
	cmovl	%ecx, %eax
	cmpl	%edx, %eax
	cmovl	%edx, %eax
	leal	2(%rax), %ebx
	movl	%ebx, -472(%rbp)
	cmpl	$49, %ebx
	jg	.L3613
	cmpl	$16, %ebx
	movl	$16, %eax
	leaq	_ZL8d2utable(%rip), %rcx
	cmovge	%ebx, %eax
	cltq
	movzbl	(%rcx,%rax), %eax
	addl	$11, %eax
	cmpl	$60, %eax
	ja	.L3614
	movq	$0, -576(%rbp)
	leaq	-320(%rbp), %r14
.L3615:
	addl	-472(%rbp), %edx
	leal	11(%rdx), %edi
	cmpl	$49, %edx
	jg	.L3618
	cmpl	$16, %edx
	movl	$16, %eax
	cmovl	%eax, %edx
	leaq	_ZL8d2utable(%rip), %rax
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edi
	addl	$11, %edi
.L3618:
	cmpl	$96, %edi
	jbe	.L3778
	call	uprv_malloc_67@PLT
	movq	%rax, -568(%rbp)
	testq	%rax, %rax
	je	.L3985
	movq	%rax, %r12
.L3619:
	leaq	-416(%rbp), %rax
	movl	$64, %esi
	leaq	9(%r14), %rbx
	movq	%rax, %rdi
	movq	%rax, -504(%rbp)
	call	uprv_decContextDefault_67@PLT
	movq	-456(%rbp), %rax
	movq	%rbx, -520(%rbp)
	movl	(%rax), %edi
	addl	4(%rax), %edi
	js	.L3620
	movl	$0, %ebx
	movq	$1, (%r14)
	movl	%edi, %edx
	movw	%bx, 8(%r14)
	jne	.L3621
.L3622:
	movabsq	$144959621680334080, %rax
	leaq	9(%r12), %rbx
	movq	$1, (%r12)
	movq	%rax, 8(%r12)
	movl	$7, %eax
	movl	%eax, %edx
	leaq	8(%r12,%rax), %rax
	movq	%rbx, -512(%rbp)
	cmpq	%rax, %rbx
	ja	.L3628
	leaq	8(%r12), %rcx
.L3629:
	cmpb	$0, (%rax)
	jne	.L3628
	cmpl	$1, %edx
	je	.L3628
	subq	$1, %rax
	subl	$1, %edx
	cmpq	%rcx, %rax
	jne	.L3629
	.p2align 4,,10
	.p2align 3
.L3628:
	movq	-504(%rbp), %rbx
	movl	%edx, (%r12)
	movq	%r14, %rsi
	movq	%r12, %rdx
	movl	$-6, 4(%r12)
	leaq	-428(%rbp), %r15
	movq	%r14, %rdi
	movq	%r15, %r8
	movq	%rbx, %rcx
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	movq	-456(%rbp), %rdi
	movq	%r15, %r9
	movq	%rbx, %rsi
	leaq	-424(%rbp), %r8
	movl	$0, -424(%rbp)
	movzbl	8(%rdi), %eax
	movl	(%rdi), %ecx
	leaq	9(%rdi), %rdx
	movq	%r8, -584(%rbp)
	movl	$2, -416(%rbp)
	movb	%al, 8(%r12)
	movl	4(%rdi), %eax
	movq	%r12, %rdi
	movl	$5, -404(%rbp)
	movl	%eax, 4(%r12)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movzbl	9(%r12), %edi
	movl	(%r12), %eax
	movl	$0, 4(%r12)
	movzbl	8(%r12), %r8d
	testb	%dil, %dil
	jne	.L3630
	cmpl	$1, %eax
	je	.L3986
.L3630:
	movzbl	%dil, %ecx
	cmpl	$10, %eax
	jle	.L3987
.L3633:
	movl	%edi, %eax
	movl	$-3, %edi
	andl	$1, %eax
	subl	$2147483646, %eax
	leal	0(,%rax,8), %edx
	leal	-10(%rdx,%rax,2), %eax
	leaq	_ZL4LNnn(%rip), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%eax, %edx
	andl	$3, %edx
	subl	%edx, %edi
	movzwl	%ax, %edx
	sarl	$2, %edx
.L3642:
	xorl	%eax, %eax
	movl	$1, (%r12)
	movw	%ax, 8(%r12)
	testl	%edx, %edx
	je	.L3632
	movq	-512(%rbp), %rcx
	movl	$3435973837, %esi
	.p2align 4,,10
	.p2align 3
.L3643:
	movl	%edx, %eax
	movl	%edx, %ebx
	addq	$1, %rcx
	imulq	%rsi, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %r8d
	addl	%r8d, %r8d
	subl	%r8d, %ebx
	movl	%edx, %r8d
	movl	%eax, %edx
	movb	%bl, -1(%rcx)
	cmpl	$9, %r8d
	ja	.L3643
	movq	-512(%rbp), %rbx
	subq	%rbx, %rcx
	movl	%ecx, %edx
	movslq	%ecx, %rcx
	leaq	-1(%rbx,%rcx), %rax
	cmpq	%rax, %rbx
	ja	.L3644
	leaq	8(%r12), %rcx
.L3645:
	cmpb	$0, (%rax)
	jne	.L3644
	cmpl	$1, %edx
	je	.L3644
	subq	$1, %rax
	subl	$1, %edx
	cmpq	%rcx, %rax
	jne	.L3645
	.p2align 4,,10
	.p2align 3
.L3644:
	movl	%edx, (%r12)
.L3632:
	movl	%edi, 4(%r12)
	movq	%r15, %r9
	movq	%r12, %rdx
	movq	%r14, %rsi
	movb	$-128, 8(%r12)
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r15, %r13
	movq	-504(%rbp), %rcx
	movq	%r12, %r15
	movl	$16, -416(%rbp)
	movl	$3, -404(%rbp)
	call	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj
	movl	$256, %edx
	movzbl	8(%r14), %esi
	movq	-496(%rbp), %rax
	movb	$0, -392(%rbp)
	movq	4(%rax), %rax
	movq	$1, -344(%rbp)
	movw	%dx, -336(%rbp)
	movq	%rax, -412(%rbp)
	movq	-400(%rbp), %rax
	movdqa	-416(%rbp), %xmm1
	movl	$9, -468(%rbp)
	movq	%rax, -368(%rbp)
	movl	-392(%rbp), %eax
	movaps	%xmm1, -384(%rbp)
	movl	%eax, -360(%rbp)
	movabsq	$-8589926000065410, %rax
	movq	%rax, -380(%rbp)
	movq	-456(%rbp), %rax
	movl	$9, -416(%rbp)
	movl	(%rax), %eax
	movl	%eax, -448(%rbp)
	addl	$9, %eax
	movl	%eax, -384(%rbp)
	leaq	-384(%rbp), %rax
	movq	%rax, -464(%rbp)
	jmp	.L3766
	.p2align 4,,10
	.p2align 3
.L3992:
	testb	$48, %al
	jne	.L3988
	testb	$64, %dl
	je	.L3649
	andl	$64, %esi
	je	.L3977
	cmpb	$0, -476(%rbp)
	js	.L3989
	.p2align 4,,10
	.p2align 3
.L3977:
	andl	$-128, %edx
	movslq	(%r14), %r10
	movzbl	8(%r14), %esi
	movb	$0, 9(%r15)
	orl	$64, %edx
	movq	$1, (%r15)
	movb	%dl, 8(%r15)
.L3652:
	movzbl	8(%r15), %r9d
	movl	$1, %ecx
	andl	$112, %r9d
	je	.L3704
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L3703:
	movl	4(%r15), %eax
	movq	-496(%rbp), %rdi
	movl	4(%r14), %edx
	addl	%ecx, %eax
	addl	(%rdi), %eax
	addl	%r10d, %edx
	cmpl	%edx, %eax
	jl	.L3704
	movzbl	8(%r15), %eax
.L3705:
	movl	%eax, %edi
	movl	%eax, %edx
	xorl	%esi, %edi
	orl	%esi, %edx
	movb	%dil, -476(%rbp)
	testb	$112, %dl
	je	.L3710
	andl	$48, %edx
	jne	.L3990
	testb	$64, %sil
	je	.L3713
	testb	$64, %al
	je	.L3714
	cmpb	$0, -476(%rbp)
	js	.L3991
.L3714:
	andl	$-128, %esi
.L3715:
	orl	$64, %esi
	movq	$1, (%r14)
	movb	$0, 9(%r14)
	movb	%sil, 8(%r14)
.L3712:
	movl	-468(%rbp), %eax
	movl	-472(%rbp), %ebx
	cmpl	%ebx, %eax
	je	.L3765
	addl	%eax, %eax
	cmpl	%eax, %ebx
	cmovle	%ebx, %eax
	movq	-456(%rbp), %rbx
	movl	%eax, -468(%rbp)
	movl	%eax, -416(%rbp)
	addl	(%rbx), %eax
	movl	%eax, -384(%rbp)
.L3766:
	leaq	-420(%rbp), %rax
	movq	%rax, -448(%rbp)
.L3765:
	addl	$-128, %esi
	movq	-464(%rbp), %rbx
	movq	%r13, %rcx
	movq	%r15, %rdi
	movb	%sil, 8(%r14)
	movq	%r14, %rsi
	movq	%rbx, %rdx
	call	_ZL8decExpOpP9decNumberPKS_P10decContextPj
	addb	$-128, 8(%r14)
	movq	%r15, %rsi
	movq	%r13, %r8
	movq	-456(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r15, %rdi
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	movzbl	8(%r15), %edx
	movzbl	-336(%rbp), %esi
	movl	%edx, %eax
	xorl	%esi, %eax
	addl	$-128, %eax
	movb	%al, -476(%rbp)
	movl	%edx, %eax
	orl	%esi, %eax
	movl	%eax, %r12d
	andl	$112, %r12d
	jne	.L3992
	movslq	(%r15), %rcx
	movzbl	9(%r15), %r9d
	movslq	-344(%rbp), %r10
	movl	-340(%rbp), %eax
	cmpl	$1, %ecx
	je	.L3993
.L3653:
	movl	-384(%rbp), %ebx
	movzbl	-335(%rbp), %r11d
	movl	%ebx, -488(%rbp)
	testb	%r11b, %r11b
	jne	.L3662
	cmpl	$1, %r10d
	je	.L3994
.L3662:
	movl	4(%r15), %edi
	movl	%eax, %r8d
	subl	%edi, %r8d
	jne	.L3667
	cmpl	$1, %r10d
	jle	.L3774
	movq	%r15, %r11
	movl	$1, %r9d
	leaq	-344(%rbp), %rbx
.L3668:
	movl	%r9d, %eax
	negl	%eax
	cmpb	$0, -476(%rbp)
	cmovs	%eax, %r9d
.L3683:
	cmpl	%ecx, %r10d
	cmovge	%r10, %rcx
	cmpl	%ecx, -488(%rbp)
	jle	.L3684
	cmpq	%rbx, %r15
	jne	.L3795
	testl	%r8d, %r8d
	jle	.L3795
.L3684:
	leal	1(%rcx), %edi
	cmpl	$49, %ecx
	jg	.L3687
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %edi
	addl	$1, %edi
.L3687:
	xorl	%r12d, %r12d
	leaq	-160(%rbp), %r10
	cmpl	$92, %edi
	jg	.L3995
.L3685:
	andl	$-128, %edx
	movb	%dl, 8(%r15)
	movl	4(%r11), %eax
	movl	%eax, 4(%r15)
	movslq	(%rbx), %rcx
	cmpl	$49, %ecx
	jg	.L3688
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L3688:
	movslq	(%r11), %rsi
	leaq	9(%rbx), %rdx
	cmpl	$49, %esi
	jg	.L3689
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rsi), %esi
.L3689:
	subq	$8, %rsp
	leaq	9(%r11), %rdi
	movq	%r10, -528(%rbp)
	pushq	%r9
	movq	%r10, %r9
	call	_ZL13decUnitAddSubPKhiS0_iiPhi
	popq	%r8
	movq	-528(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %ecx
	popq	%r9
	js	.L3690
	movl	%eax, (%r15)
.L3691:
	movl	$0, -420(%rbp)
	cmpq	%r10, -512(%rbp)
	je	.L3692
	cmpl	%ecx, -488(%rbp)
	jge	.L3693
	cmpl	$49, %ecx
	jg	.L3694
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L3694:
	movslq	%ecx, %rax
	leaq	-1(%r10,%rax), %rax
	cmpq	%rax, %r10
	ja	.L3695
.L3696:
	cmpb	$0, (%rax)
	jne	.L3695
	cmpl	$1, %ecx
	je	.L3695
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rax, %r10
	jbe	.L3696
	.p2align 4,,10
	.p2align 3
.L3695:
	movl	%ecx, (%r15)
.L3693:
	movq	-448(%rbp), %r8
	movq	%r13, %r9
	movq	%r10, %rdx
	movq	%r15, %rdi
	movq	-464(%rbp), %rsi
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	(%r15), %ecx
.L3692:
	cmpl	$49, %ecx
	jg	.L3697
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L3697:
	movq	-512(%rbp), %rbx
	movslq	%ecx, %rax
	leaq	-1(%rbx,%rax), %rax
	cmpq	%rax, %rbx
	ja	.L3698
	leaq	8(%r15), %rdx
.L3699:
	cmpb	$0, (%rax)
	jne	.L3698
	cmpl	$1, %ecx
	je	.L3698
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rax, %rdx
	jne	.L3699
	.p2align 4,,10
	.p2align 3
.L3698:
	movl	%ecx, (%r15)
	movq	-448(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	-464(%rbp), %rsi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movzbl	9(%r15), %r9d
	testb	%r9b, %r9b
	jne	.L3700
	cmpl	$1, (%r15)
	je	.L3996
.L3700:
	testq	%r12, %r12
	je	.L3978
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	movzbl	9(%r15), %r9d
.L3978:
	movl	(%r15), %ecx
.L3648:
	movslq	(%r14), %r10
	movzbl	8(%r14), %esi
	testb	%r9b, %r9b
	jne	.L3703
	cmpl	$1, %ecx
	jne	.L3703
	jmp	.L3652
	.p2align 4,,10
	.p2align 3
.L3982:
	cmpl	$1, (%rsi)
	jne	.L3609
	movl	$192, %r13d
	movq	$1, (%rdi)
	movw	%r13w, 8(%rdi)
	jmp	.L3598
	.p2align 4,,10
	.p2align 3
.L3596:
	movq	%rcx, %r8
	movq	-496(%rbp), %rcx
	xorl	%edx, %edx
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	jmp	.L3598
	.p2align 4,,10
	.p2align 3
.L3597:
	cmpq	%rdi, %rsi
	je	.L3598
	movb	%al, 8(%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 4(%rdi)
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	movzbl	9(%rsi), %eax
	movb	%al, 9(%rdi)
	movl	(%rsi), %edx
	cmpl	$1, %edx
	jle	.L3598
	addq	$10, %rdi
	leaq	9(%rsi), %rcx
	movslq	%edx, %rax
	cmpl	$49, %edx
	jle	.L3997
.L3602:
	movq	-456(%rbp), %r10
	addq	%rax, %rcx
	leaq	10(%r10), %rdx
	cmpq	%rdx, %rcx
	jbe	.L3598
	movq	%rcx, %rax
	subq	%r10, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L3603
	leaq	25(%r10), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L3603
	movq	%rcx, %rax
	movq	-536(%rbp), %r9
	subq	%r10, %rax
	leaq	-10(%rax), %r8
	movl	$10, %eax
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	$10, %rsi
	.p2align 4,,10
	.p2align 3
.L3605:
	movdqu	(%r10,%rax), %xmm0
	movups	%xmm0, (%r9,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L3605
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	%rsi, %rdx
	leaq	(%rdi,%rsi), %rax
	cmpq	%r8, %rsi
	je	.L3598
	movzbl	(%rdx), %esi
	movb	%sil, (%rax)
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3598
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rax)
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3598
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rax)
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3598
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rax)
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3598
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rax)
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3598
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rax)
	leaq	6(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3598
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rax)
	leaq	7(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3598
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rax)
	leaq	8(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3598
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rax)
	leaq	9(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3598
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rax)
	leaq	10(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3598
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rax)
	leaq	11(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3598
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rax)
	leaq	12(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3598
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rax)
	leaq	13(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3598
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rax)
	leaq	14(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L3598
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
	jmp	.L3598
	.p2align 4,,10
	.p2align 3
.L3613:
	addl	$13, %eax
.L3614:
	movl	%eax, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, -576(%rbp)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3616
	movq	-456(%rbp), %rax
	movl	(%rax), %edx
	jmp	.L3615
	.p2align 4,,10
	.p2align 3
.L3704:
	movzbl	9(%r14), %eax
	cmpl	%r10d, -472(%rbp)
	je	.L3706
	cmpl	$1, %r10d
	je	.L3998
.L3707:
	movzbl	8(%r15), %eax
	cmpl	$1, %ecx
	jne	.L3705
	movl	%eax, %edx
	andl	$112, %edx
	orb	%r9b, %dl
	jne	.L3705
	movl	4(%r14), %edx
	subl	-472(%rbp), %edx
	xorl	%r9d, %r9d
	movl	%edx, 4(%r15)
	jmp	.L3705
	.p2align 4,,10
	.p2align 3
.L3710:
	movzbl	9(%r14), %edi
	movl	4(%r15), %ebx
	cmpl	$1, %r10d
	je	.L3999
.L3716:
	movl	-416(%rbp), %r11d
	testb	%r9b, %r9b
	jne	.L3725
	cmpl	$1, %ecx
	je	.L4000
.L3725:
	movl	4(%r14), %edx
	movl	%ebx, %r8d
	subl	%edx, %r8d
	jne	.L3730
	cmpl	$1, %ecx
	jle	.L3773
	movq	%r15, %rbx
	movq	%r14, %r12
	movl	$1, %r9d
.L3731:
	movl	%r9d, %eax
	negl	%eax
	cmpb	$0, -476(%rbp)
	cmovs	%eax, %r9d
.L3745:
	cmpl	%r10d, %ecx
	cmovl	%r10d, %ecx
	cmpl	%ecx, %r11d
	jle	.L3746
	cmpq	%rbx, %r14
	jne	.L3806
	testl	%r8d, %r8d
	jle	.L3806
.L3746:
	leal	1(%rcx), %edi
	cmpl	$49, %ecx
	jg	.L3749
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %edi
	addl	$1, %edi
.L3749:
	movq	$0, -488(%rbp)
	leaq	-160(%rbp), %r10
	cmpl	$92, %edi
	jg	.L4001
.L3747:
	andl	$-128, %esi
	movb	%sil, 8(%r14)
	movl	4(%r12), %eax
	movl	%eax, 4(%r14)
	movslq	(%rbx), %rcx
	cmpl	$49, %ecx
	jg	.L3750
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L3750:
	movslq	(%r12), %rsi
	leaq	9(%rbx), %rdx
	cmpl	$49, %esi
	jg	.L3751
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rsi), %esi
.L3751:
	subq	$8, %rsp
	leaq	9(%r12), %rdi
	movl	%r11d, -480(%rbp)
	pushq	%r9
	movq	%r10, %r9
	movq	%r10, -528(%rbp)
	call	_ZL13decUnitAddSubPKhiS0_iiPhi
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	movq	-528(%rbp), %r10
	movl	-480(%rbp), %r11d
	movl	%eax, %ecx
	js	.L3752
	movl	%eax, (%r14)
.L3753:
	movl	$0, -420(%rbp)
	cmpq	-520(%rbp), %r10
	je	.L3754
	cmpl	%ecx, %r11d
	jge	.L3755
	cmpl	$49, %ecx
	jg	.L3756
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L3756:
	movslq	%ecx, %rax
	leaq	-1(%r10,%rax), %rax
	cmpq	%rax, %r10
	ja	.L3757
.L3758:
	cmpb	$0, (%rax)
	jne	.L3757
	cmpl	$1, %ecx
	je	.L3757
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rax, %r10
	jbe	.L3758
	.p2align 4,,10
	.p2align 3
.L3757:
	movl	%ecx, (%r14)
.L3755:
	movq	-448(%rbp), %r8
	movq	%r13, %r9
	movq	%r10, %rdx
	movq	%r14, %rdi
	movq	-504(%rbp), %rsi
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	(%r14), %ecx
.L3754:
	cmpl	$49, %ecx
	jg	.L3759
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L3759:
	movq	-520(%rbp), %rdi
	movslq	%ecx, %rax
	leaq	-1(%rdi,%rax), %rax
	cmpq	%rdi, %rax
	jb	.L3760
	leaq	8(%r14), %rdx
.L3761:
	cmpb	$0, (%rax)
	jne	.L3760
	cmpl	$1, %ecx
	je	.L3760
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rax, %rdx
	jne	.L3761
	.p2align 4,,10
	.p2align 3
.L3760:
	movl	%ecx, (%r14)
	movq	-448(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-504(%rbp), %rsi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	cmpb	$0, 9(%r14)
	jne	.L3762
	cmpl	$1, (%r14)
	je	.L4002
.L3762:
	cmpq	$0, -488(%rbp)
	je	.L3979
	movq	-488(%rbp), %rdi
	call	uprv_free_67@PLT
.L3979:
	movzbl	8(%r14), %esi
	jmp	.L3712
	.p2align 4,,10
	.p2align 3
.L3994:
	testb	$112, %sil
	je	.L4003
	movl	4(%r15), %edi
	movl	%eax, %r8d
	subl	%edi, %r8d
	je	.L3774
	.p2align 4,,10
	.p2align 3
.L3667:
	movq	%r15, %r11
	leaq	-344(%rbp), %rbx
	testl	%r8d, %r8d
	jns	.L3676
	subl	%eax, %edi
	leal	-128(%rsi), %eax
	movl	%edx, %esi
	movl	%r10d, %edx
	movl	%edi, %r8d
	movl	%ecx, %r10d
	movq	%r15, %rbx
	movslq	%edx, %rcx
	movl	$1, %r12d
	movl	%eax, %edx
	leaq	-344(%rbp), %r11
.L3676:
	movl	-488(%rbp), %r9d
	leal	(%r8,%r10), %eax
	leal	1(%r9,%rcx), %edi
	cmpl	%edi, %eax
	jle	.L3677
	movb	%sil, 8(%r15)
	subl	%r10d, %r9d
	leaq	9(%rbx), %rdx
	movq	%r15, %rdi
	movsbl	-476(%rbp), %eax
	movq	-448(%rbp), %r8
	movl	%r9d, -488(%rbp)
	movq	%r13, %r9
	movq	-464(%rbp), %rsi
	sarl	$31, %eax
	orl	$1, %eax
	movl	%eax, -420(%rbp)
	movl	4(%rbx), %eax
	movl	%eax, 4(%r15)
	movl	(%rbx), %ecx
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	-488(%rbp), %r10d
	testl	%r10d, %r10d
	jle	.L3680
	movl	(%r15), %esi
	movq	-512(%rbp), %rdi
	movl	%r10d, %edx
	movl	%r10d, -476(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	movl	-476(%rbp), %r10d
	subl	%r10d, 4(%r15)
	movl	%eax, (%r15)
.L3680:
	testb	%r12b, %r12b
	jne	.L3681
	addb	$-128, 8(%r15)
.L3681:
	movq	-448(%rbp), %rdx
	movq	-464(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r15, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movzbl	9(%r15), %r9d
	movl	(%r15), %ecx
	jmp	.L3648
	.p2align 4,,10
	.p2align 3
.L4000:
	testb	$112, %al
	je	.L4004
	movl	4(%r14), %edx
	movl	%ebx, %r8d
	subl	%edx, %r8d
	je	.L3773
	.p2align 4,,10
	.p2align 3
.L3730:
	testl	%r8d, %r8d
	js	.L4005
	movq	-512(%rbp), %rdx
	movq	%r15, %rbx
	movq	%r14, %r12
.L3739:
	leal	(%r8,%rcx), %edi
	leal	1(%r10,%r11), %r9d
	cmpl	%r9d, %edi
	jle	.L3740
	subl	%ecx, %r11d
	movsbl	-476(%rbp), %ecx
	movq	%r13, %r9
	movq	%r14, %rdi
	movb	%al, 8(%r14)
	movl	4(%rbx), %eax
	movl	%r11d, %r12d
	sarl	$31, %ecx
	movq	-448(%rbp), %r8
	movq	-504(%rbp), %rsi
	orl	$1, %ecx
	movl	%eax, 4(%r14)
	movl	%ecx, -420(%rbp)
	movl	(%rbx), %ecx
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	testl	%r12d, %r12d
	jle	.L3743
	movl	(%r14), %esi
	movq	-520(%rbp), %rdi
	movl	%r12d, %edx
	call	_ZL14decShiftToMostPhii.part.0
	subl	%r12d, 4(%r14)
	movl	%eax, (%r14)
.L3743:
	movq	-504(%rbp), %rsi
	movq	-448(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r14, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movzbl	8(%r14), %esi
	jmp	.L3712
	.p2align 4,,10
	.p2align 3
.L3649:
	notl	%esi
	movl	%esi, %edx
	jmp	.L3977
	.p2align 4,,10
	.p2align 3
.L3988:
	movq	-464(%rbp), %rcx
	movq	%r13, %r8
	movq	%r15, %rsi
	movq	%r15, %rdi
	leaq	-344(%rbp), %rdx
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movzbl	9(%r15), %r9d
	movl	(%r15), %ecx
	jmp	.L3648
	.p2align 4,,10
	.p2align 3
.L3713:
	andl	$-128, %eax
	movl	%eax, %esi
	jmp	.L3715
	.p2align 4,,10
	.p2align 3
.L3990:
	movq	-504(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r13, %r8
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movzbl	8(%r14), %esi
	jmp	.L3712
	.p2align 4,,10
	.p2align 3
.L3690:
	negl	%ecx
	addb	$-128, 8(%r15)
	movl	%ecx, (%r15)
	jmp	.L3691
	.p2align 4,,10
	.p2align 3
.L3752:
	negl	%ecx
	addb	$-128, 8(%r14)
	movl	%ecx, (%r14)
	jmp	.L3753
	.p2align 4,,10
	.p2align 3
.L3677:
	movl	$1, %r9d
	cmpl	$48, %r8d
	jg	.L3682
	leal	1(%r8), %esi
	leaq	_ZL8d2utable(%rip), %rdi
	movslq	%esi, %rsi
	movzbl	(%rdi,%rsi), %esi
	leaq	_ZL9DECPOWERS(%rip), %rdi
	subl	$1, %esi
	subl	%esi, %r8d
	movslq	%r8d, %r8
	movl	(%rdi,%r8,4), %r9d
	movl	%esi, %r8d
.L3682:
	movslq	%eax, %r10
	jmp	.L3668
	.p2align 4,,10
	.p2align 3
.L3740:
	movl	$1, %r9d
	cmpl	$48, %r8d
	jg	.L3744
	leal	1(%r8), %eax
	leaq	_ZL8d2utable(%rip), %rdx
	cltq
	movzbl	(%rdx,%rax), %eax
	leaq	_ZL9DECPOWERS(%rip), %rdx
	subl	$1, %eax
	subl	%eax, %r8d
	movslq	%r8d, %r8
	movl	(%rdx,%r8,4), %r9d
	movl	%eax, %r8d
.L3744:
	movl	%edi, %ecx
	jmp	.L3731
	.p2align 4,,10
	.p2align 3
.L3774:
	cmpl	%eax, -376(%rbp)
	jg	.L3791
	movl	-380(%rbp), %esi
	movl	-488(%rbp), %ebx
	subl	%ebx, %esi
	addl	$1, %esi
	cmpl	%eax, %esi
	jl	.L3791
	cmpl	%r10d, %ebx
	jge	.L4006
	.p2align 4,,10
	.p2align 3
.L3791:
	movq	%r15, %r11
	movl	$1, %r9d
	xorl	%r8d, %r8d
	leaq	-344(%rbp), %rbx
	jmp	.L3668
	.p2align 4,,10
	.p2align 3
.L3773:
	cmpl	%ebx, -408(%rbp)
	jg	.L3802
	movl	-412(%rbp), %eax
	subl	%r11d, %eax
	addl	$1, %eax
	cmpl	%ebx, %eax
	jl	.L3802
	cmpl	%r11d, %ecx
	jle	.L4007
.L3802:
	movq	%r15, %rbx
	movq	%r14, %r12
	movl	$1, %r9d
	xorl	%r8d, %r8d
	jmp	.L3731
	.p2align 4,,10
	.p2align 3
.L4005:
	movl	%ecx, %edi
	subl	%ebx, %edx
	movl	%r10d, %ecx
	movq	%r14, %rbx
	movl	%edi, %r10d
	movl	%esi, %edi
	movl	%edx, %r8d
	movl	%eax, %esi
	movq	-520(%rbp), %rdx
	movl	%edi, %eax
	movq	%r15, %r12
	jmp	.L3739
	.p2align 4,,10
	.p2align 3
.L3998:
	movl	%esi, %edx
	andl	$112, %edx
	orb	%al, %dl
	jne	.L3707
	movq	-504(%rbp), %rcx
	movq	%r13, %r9
	movl	$1, %r8d
	movq	-456(%rbp), %rsi
	leaq	-344(%rbp), %rdx
	leaq	-332(%rbp), %rdi
	call	_ZL12decCompareOpP9decNumberPKS_S2_P10decContexthPj
	cmpb	$0, -323(%rbp)
	jne	.L3708
	movl	$0, 4(%r14)
	movzbl	9(%r14), %ecx
	xorl	%edx, %edx
	movl	(%r14), %r10d
	movzbl	8(%r14), %esi
.L3709:
	movl	$1, -424(%rbp)
	cmpl	$1, %r10d
	jne	.L3767
	movl	%esi, %eax
	andl	$112, %eax
	orb	%cl, %al
	jne	.L3767
	movl	$0, -424(%rbp)
	jmp	.L3767
	.p2align 4,,10
	.p2align 3
.L3993:
	movl	%edx, %edi
	andl	$112, %edi
	orb	%r9b, %dil
	jne	.L3653
	movl	4(%r15), %ebx
	movb	%sil, 8(%r15)
	movq	%r13, %r9
	movl	%r10d, %ecx
	movl	%eax, 4(%r15)
	movq	-448(%rbp), %r8
	movq	%r15, %rdi
	leaq	-335(%rbp), %rdx
	movq	-464(%rbp), %rsi
	movl	%ebx, %r12d
	movl	$0, -420(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movzbl	8(%r15), %eax
	movl	4(%r15), %edx
	addl	$-128, %eax
	subl	%edx, %r12d
	cmpb	$0, 9(%r15)
	movb	%al, 8(%r15)
	jne	.L3654
	cmpl	$1, (%r15)
	je	.L4008
.L3654:
	testl	%r12d, %r12d
	jns	.L3681
	movl	(%r15), %esi
	movl	-384(%rbp), %eax
	movl	%esi, %ecx
	subl	%r12d, %ecx
	cmpl	%eax, %ecx
	jle	.L3660
	orl	$2048, -428(%rbp)
	movl	%esi, %r12d
	subl	%eax, %r12d
	je	.L3666
	subl	%esi, %eax
	movl	%eax, %edx
	jmp	.L3771
	.p2align 4,,10
	.p2align 3
.L3995:
	movslq	%edi, %rdi
	movb	%dl, -553(%rbp)
	movq	%r11, -552(%rbp)
	movl	%r9d, -480(%rbp)
	movl	%r8d, -528(%rbp)
	call	uprv_malloc_67@PLT
	movl	-528(%rbp), %r8d
	movl	-480(%rbp), %r9d
	testq	%rax, %rax
	movq	-552(%rbp), %r11
	movzbl	-553(%rbp), %edx
	movq	%rax, %r10
	je	.L4009
	movq	%rax, %r12
	jmp	.L3685
	.p2align 4,,10
	.p2align 3
.L3999:
	movl	%esi, %edx
	andl	$112, %edx
	orb	%dil, %dl
	jne	.L3716
	movl	4(%r14), %r12d
	movb	%al, 8(%r14)
	movq	%r13, %r9
	movq	%r14, %rdi
	movl	%ebx, 4(%r14)
	movq	-512(%rbp), %rdx
	movq	-448(%rbp), %r8
	movq	-504(%rbp), %rsi
	movl	%r12d, %ebx
	movl	$0, -420(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	4(%r14), %edx
	subl	%edx, %ebx
	cmpb	$0, 9(%r14)
	jne	.L3717
	cmpl	$1, (%r14)
	je	.L4010
.L3717:
	testl	%ebx, %ebx
	jns	.L3743
	movl	(%r14), %esi
	movl	-416(%rbp), %eax
	movl	%esi, %ecx
	subl	%ebx, %ecx
	cmpl	%eax, %ecx
	jle	.L3723
	orl	$2048, -428(%rbp)
	movl	%esi, %ebx
	subl	%eax, %ebx
	je	.L3729
	subl	%esi, %eax
	movl	%eax, %edx
	jmp	.L3769
	.p2align 4,,10
	.p2align 3
.L3795:
	movq	-512(%rbp), %r10
	xorl	%r12d, %r12d
	jmp	.L3685
	.p2align 4,,10
	.p2align 3
.L4001:
	movslq	%edi, %rdi
	movb	%sil, -552(%rbp)
	movl	%r11d, -480(%rbp)
	movl	%r8d, -528(%rbp)
	movl	%r9d, -488(%rbp)
	call	uprv_malloc_67@PLT
	movl	-488(%rbp), %r9d
	movl	-528(%rbp), %r8d
	testq	%rax, %rax
	movl	-480(%rbp), %r11d
	movzbl	-552(%rbp), %esi
	movq	%rax, %r10
	je	.L4011
	movq	%rax, -488(%rbp)
	jmp	.L3747
	.p2align 4,,10
	.p2align 3
.L3806:
	movq	$0, -488(%rbp)
	movq	-520(%rbp), %r10
	jmp	.L3747
	.p2align 4,,10
	.p2align 3
.L3996:
	movzbl	8(%r15), %eax
	testb	$112, %al
	jne	.L3700
	cmpb	$0, -476(%rbp)
	jns	.L3700
	testb	$32, -428(%rbp)
	jne	.L3700
	cmpl	$6, -372(%rbp)
	je	.L4012
	andl	$127, %eax
	movb	%al, 8(%r15)
	jmp	.L3700
	.p2align 4,,10
	.p2align 3
.L4002:
	movzbl	8(%r14), %eax
	testb	$112, %al
	jne	.L3762
	cmpb	$0, -476(%rbp)
	jns	.L3762
	testb	$32, -428(%rbp)
	jne	.L3762
	cmpl	$6, -404(%rbp)
	je	.L4013
	andl	$127, %eax
	movb	%al, 8(%r14)
	jmp	.L3762
	.p2align 4,,10
	.p2align 3
.L3989:
	orl	$128, -428(%rbp)
	movzbl	9(%r15), %r9d
	movl	(%r15), %ecx
	jmp	.L3648
	.p2align 4,,10
	.p2align 3
.L3991:
	orl	$128, -428(%rbp)
	jmp	.L3712
	.p2align 4,,10
	.p2align 3
.L3706:
	movl	$1, -424(%rbp)
	movl	4(%r14), %edx
.L3767:
	movq	-536(%rbp), %rbx
	movq	-496(%rbp), %r14
	movl	%r10d, %ecx
	movq	-584(%rbp), %r15
	movq	-544(%rbp), %r9
	movl	(%r14), %eax
	movb	%sil, 8(%rbx)
	movq	%rbx, %rdi
	movl	%edx, 4(%rbx)
	movq	-504(%rbp), %rsi
	movq	%r15, %r8
	movq	-520(%rbp), %rdx
	movl	%eax, -416(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, %rcx
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movq	-576(%rbp), %rax
	testq	%rax, %rax
	je	.L3768
	movq	%rax, %rdi
	call	uprv_free_67@PLT
.L3768:
	movq	-568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3598
.L3980:
	call	uprv_free_67@PLT
	jmp	.L3598
	.p2align 4,,10
	.p2align 3
.L4003:
	movq	-512(%rbp), %rdx
	movq	-448(%rbp), %r8
	movq	%r13, %r9
	movq	%r15, %rdi
	movq	-464(%rbp), %rsi
	movl	%eax, -476(%rbp)
	movl	$0, -420(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	-476(%rbp), %eax
	movl	4(%r15), %edx
	movl	%eax, %ebx
	subl	%edx, %ebx
	jns	.L3681
	movl	(%r15), %esi
	movl	-384(%rbp), %ecx
	movl	%esi, %edi
	subl	%ebx, %edi
	cmpl	%ecx, %edi
	jg	.L4014
	subl	%eax, %edx
.L3772:
	movq	-512(%rbp), %rdi
	call	_ZL14decShiftToMostPhii.part.0
	addl	4(%r15), %ebx
	movl	%eax, %esi
	movl	%ebx, %edx
.L3666:
	movl	%esi, (%r15)
	movl	%edx, 4(%r15)
	jmp	.L3681
	.p2align 4,,10
	.p2align 3
.L4004:
	movq	-520(%rbp), %rdx
	movq	%r13, %r9
	movl	%r10d, %ecx
	movq	%r14, %rdi
	movq	-448(%rbp), %r8
	movq	-504(%rbp), %rsi
	movl	%ebx, %r12d
	movl	$0, -420(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	4(%r14), %edx
	subl	%edx, %r12d
	jns	.L3743
	movl	(%r14), %esi
	movl	-416(%rbp), %eax
	movl	%esi, %ecx
	subl	%r12d, %ecx
	cmpl	%eax, %ecx
	jg	.L4015
	subl	%ebx, %edx
.L3770:
	movq	-520(%rbp), %rdi
	call	_ZL14decShiftToMostPhii.part.0
	movl	4(%r14), %edx
	movl	%eax, %esi
	addl	%r12d, %edx
.L3729:
	movl	%esi, (%r14)
	movl	%edx, 4(%r14)
	jmp	.L3743
	.p2align 4,,10
	.p2align 3
.L3620:
	cmpl	$-2147483648, %edi
	je	.L4016
	xorl	%r10d, %r10d
	movq	$1, (%r14)
	movl	%edi, %edx
	movw	%r10w, 8(%r14)
	negl	%edx
.L3621:
	leaq	9(%r14), %rcx
	movl	$3435973837, %esi
	.p2align 4,,10
	.p2align 3
.L3624:
	movl	%edx, %eax
	movl	%edx, %ebx
	addq	$1, %rcx
	imulq	%rsi, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %r8d
	addl	%r8d, %r8d
	subl	%r8d, %ebx
	movl	%edx, %r8d
	movl	%eax, %edx
	movb	%bl, -1(%rcx)
	cmpl	$9, %r8d
	ja	.L3624
	movq	-520(%rbp), %rbx
	subq	%rbx, %rcx
	movl	%ecx, %edx
	movslq	%ecx, %rcx
	leaq	-1(%rbx,%rcx), %rax
	cmpq	%rbx, %rax
	jb	.L3625
	leaq	8(%r14), %rcx
.L3626:
	cmpb	$0, (%rax)
	jne	.L3625
	cmpl	$1, %edx
	je	.L3625
	subq	$1, %rax
	subl	$1, %edx
	cmpq	%rcx, %rax
	jne	.L3626
	.p2align 4,,10
	.p2align 3
.L3625:
	movl	%edx, (%r14)
	testl	%edi, %edi
	jns	.L3622
	movb	$-128, 8(%r14)
	jmp	.L3622
	.p2align 4,,10
	.p2align 3
.L3778:
	movq	$0, -568(%rbp)
	leaq	-256(%rbp), %r12
	jmp	.L3619
.L3987:
	cmpl	$1, %eax
	jle	.L3637
	movzbl	10(%r12), %esi
	leal	(%rsi,%rsi,4), %edx
	leal	(%rcx,%rdx,2), %ecx
	cmpl	$2, %eax
	je	.L3637
	movzbl	11(%r12), %esi
	imull	$100, %esi, %esi
	addl	%esi, %ecx
	cmpl	$3, %eax
	je	.L3637
	movzbl	12(%r12), %esi
	imull	$1000, %esi, %esi
	addl	%esi, %ecx
	cmpl	$4, %eax
	je	.L3637
	movzbl	13(%r12), %esi
	imull	$10000, %esi, %esi
	addl	%esi, %ecx
	cmpl	$5, %eax
	je	.L3637
	movzbl	14(%r12), %esi
	imull	$100000, %esi, %esi
	addl	%esi, %ecx
	cmpl	$6, %eax
	je	.L3637
	movzbl	15(%r12), %esi
	imull	$1000000, %esi, %esi
	addl	%esi, %ecx
	cmpl	$7, %eax
	je	.L3637
	movzbl	16(%r12), %esi
	imull	$10000000, %esi, %esi
	addl	%esi, %ecx
	cmpl	$8, %eax
	je	.L3637
	movzbl	17(%r12), %esi
	imull	$100000000, %esi, %edx
	addl	%ecx, %edx
	movl	%edx, %ecx
	cmpl	$9, %eax
	je	.L3637
	movzbl	18(%r12), %esi
	movl	$1000000000, %r9d
	imull	$1000000000, %esi, %ecx
	addl	%edx, %ecx
	movl	%ecx, %eax
	cltd
	idivl	%r9d
	cmpl	%esi, %eax
	jne	.L3633
	testb	%r8b, %r8b
	jns	.L3956
	cmpl	$1999999997, %ecx
	jg	.L3633
	testb	%r8b, %r8b
	jns	.L3956
.L3775:
	negl	%ecx
	.p2align 4,,10
	.p2align 3
.L3640:
	cmpl	$9, %ecx
	jle	.L3776
	subl	$10, %ecx
	leaq	_ZL4LNnn(%rip), %rax
	movl	$-3, %edi
	movslq	%ecx, %rcx
	movzwl	(%rax,%rcx,2), %edx
	movl	%edx, %eax
	sarl	$2, %edx
	andl	$3, %eax
	subl	%eax, %edi
	jmp	.L3642
.L3956:
	cmpl	$999999999, %ecx
	jg	.L3633
.L3637:
	testb	%r8b, %r8b
	jns	.L3640
	negl	%ecx
	jmp	.L3640
	.p2align 4,,10
	.p2align 3
.L4006:
	cmpl	%ebx, %ecx
	jg	.L3791
	cmpb	$0, -476(%rbp)
	movzbl	%r9b, %eax
	movzbl	%r11b, %esi
	js	.L3669
	addl	%esi, %eax
	cmpl	$9, %eax
	jg	.L3670
	testl	%ecx, %ecx
	jle	.L4017
.L3671:
	addl	%r11d, %r9d
	movb	%r9b, 9(%r15)
	jmp	.L3648
	.p2align 4,,10
	.p2align 3
.L4007:
	cmpl	%r10d, %r11d
	jl	.L3802
	cmpb	$0, -476(%rbp)
	movzbl	%dil, %eax
	movzbl	%r9b, %edx
	js	.L3732
	addl	%edx, %eax
	cmpl	$9, %eax
	jg	.L3733
	testl	%r10d, %r10d
	jle	.L4018
.L3734:
	addl	%edi, %r9d
	movb	%r9b, 9(%r14)
	jmp	.L3712
.L4014:
	orl	$2048, -428(%rbp)
	movl	%esi, %ebx
	subl	%ecx, %ebx
	je	.L3666
	movl	%ecx, %edx
	subl	%esi, %edx
	jmp	.L3772
.L3660:
	subl	%ebx, %edx
.L3771:
	movq	-512(%rbp), %rdi
	call	_ZL14decShiftToMostPhii.part.0
	movl	4(%r15), %edx
	movl	%eax, %esi
	addl	%r12d, %edx
	jmp	.L3666
.L4015:
	orl	$2048, -428(%rbp)
	movl	%esi, %r12d
	subl	%eax, %r12d
	je	.L3729
	subl	%esi, %eax
	movl	%eax, %edx
	jmp	.L3770
.L3723:
	subl	%r12d, %edx
.L3769:
	movq	-520(%rbp), %rdi
	call	_ZL14decShiftToMostPhii.part.0
	addl	4(%r14), %ebx
	movl	%eax, %esi
	movl	%ebx, %edx
	jmp	.L3729
.L3997:
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	jmp	.L3602
.L3983:
	cmpl	$2, %edx
	jne	.L3612
	movq	-496(%rbp), %rdi
	leaq	-416(%rbp), %rdx
	leaq	.LC17(%rip), %rsi
	movq	16(%rdi), %rax
	movdqu	(%rdi), %xmm2
	movq	%rax, -400(%rbp)
	movl	24(%rdi), %eax
	movq	-536(%rbp), %rdi
	movaps	%xmm2, -416(%rbp)
	movl	%eax, -392(%rbp)
	movl	$3, -404(%rbp)
	movaps	%xmm2, -448(%rbp)
	call	uprv_decNumberFromString_67
	movq	-544(%rbp), %rax
	orl	$2080, (%rax)
	jmp	.L3598
.L3984:
	cmpl	$1, %edx
	jne	.L3611
	movq	-496(%rbp), %rdi
	leaq	-416(%rbp), %rdx
	leaq	.LC18(%rip), %rsi
	movq	16(%rdi), %rax
	movdqu	(%rdi), %xmm4
	movq	%rax, -400(%rbp)
	movl	24(%rdi), %eax
	movq	-536(%rbp), %rdi
	movaps	%xmm4, -416(%rbp)
	movl	%eax, -392(%rbp)
	movl	$3, -404(%rbp)
	movaps	%xmm4, -448(%rbp)
	call	uprv_decNumberFromString_67
	movq	-544(%rbp), %rax
	orl	$2080, (%rax)
	jmp	.L3598
.L3603:
	movq	-456(%rbp), %rdi
	movq	%rcx, %rax
	movl	$10, %edx
	movq	-536(%rbp), %rsi
	subq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L3608:
	movzbl	(%rdi,%rdx), %ecx
	movb	%cl, (%rsi,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L3608
	jmp	.L3598
.L3986:
	testb	$112, %r8b
	je	.L4019
	xorl	%ecx, %ecx
	testb	%r8b, %r8b
	js	.L3775
.L3776:
	leal	0(,%rcx,8), %eax
	leaq	_ZL4LNnn(%rip), %rdx
	movl	$-3, %edi
	leal	-10(%rax,%rcx,2), %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%eax, %edx
	sarl	$2, %eax
	andl	$3, %edx
	subl	%edx, %edi
	movl	%eax, %edx
	jmp	.L3642
	.p2align 4,,10
	.p2align 3
.L4008:
	testb	$112, %al
	jne	.L3654
	testl	%r12d, %r12d
	jns	.L3655
	movl	%ebx, 4(%r15)
.L3655:
	cmpb	$0, -476(%rbp)
	jns	.L3681
	cmpl	$6, -372(%rbp)
	je	.L3658
	movb	$0, 8(%r15)
	jmp	.L3681
.L4016:
	xorl	%r11d, %r11d
	movq	$1, (%r14)
	movl	$-2147483648, %edx
	movw	%r11w, 8(%r14)
	jmp	.L3621
.L4010:
	testb	$112, 8(%r14)
	jne	.L3717
	testl	%ebx, %ebx
	jns	.L3718
	movl	%r12d, 4(%r14)
.L3718:
	cmpb	$0, -476(%rbp)
	jns	.L3743
	cmpl	$6, -404(%rbp)
	je	.L3721
	movb	$0, 8(%r14)
	jmp	.L3743
.L3708:
	movq	-544(%rbp), %rax
	movzbl	9(%r14), %ecx
	movl	(%r14), %r10d
	movl	4(%r14), %edx
	orl	$2080, (%rax)
	movzbl	8(%r14), %esi
	jmp	.L3709
.L3732:
	subl	%edx, %eax
	testl	%eax, %eax
	jle	.L3803
	subl	%r9d, %edi
	movb	%dil, 9(%r14)
	cmpl	$49, %r10d
	jg	.L3736
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%r10), %r10d
.L3736:
	movq	-520(%rbp), %rdi
	movslq	%r10d, %rax
	leaq	-1(%rdi,%rax), %rax
	cmpq	%rdi, %rax
	jb	.L3737
	leaq	8(%r14), %rdx
.L3738:
	cmpb	$0, (%rax)
	jne	.L3737
	cmpl	$1, %r10d
	je	.L3737
	subq	$1, %rax
	subl	$1, %r10d
	cmpq	%rax, %rdx
	jne	.L3738
.L3737:
	movl	%r10d, (%r14)
	jmp	.L3712
.L3669:
	subl	%esi, %eax
	testl	%eax, %eax
	jle	.L3792
	subl	%r11d, %r9d
	movb	%r9b, 9(%r15)
	cmpl	$49, %ecx
	jg	.L3673
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L3673:
	movq	-512(%rbp), %rdi
	movslq	%ecx, %rax
	leaq	-1(%rdi,%rax), %rax
	cmpq	%rax, %rdi
	ja	.L3674
	leaq	8(%r15), %rdx
.L3675:
	cmpb	$0, (%rax)
	jne	.L3674
	cmpl	$1, %ecx
	je	.L3674
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rax, %rdx
	jne	.L3675
.L3674:
	movl	%ecx, (%r15)
	jmp	.L3648
.L4018:
	movslq	%r10d, %rdx
	leaq	_ZL9DECPOWERS(%rip), %rbx
	cmpl	(%rbx,%rdx,4), %eax
	jl	.L3734
.L3733:
	cmpl	%r10d, %ecx
	movq	%r14, %r12
	movq	%r15, %rbx
	cmovl	%r10d, %ecx
	cmpl	%ecx, %r11d
	jle	.L3810
	movq	-520(%rbp), %r10
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	$0, -488(%rbp)
	jmp	.L3747
.L4019:
	movl	$1, (%r12)
	movl	$-3, %edi
	movb	$0, 9(%r12)
	jmp	.L3632
.L4012:
	orl	$-128, %eax
	movb	%al, 8(%r15)
	jmp	.L3700
.L4017:
	movslq	%ecx, %rsi
	leaq	_ZL9DECPOWERS(%rip), %rdi
	cmpl	(%rdi,%rsi,4), %eax
	jl	.L3671
.L3670:
	cmpl	%r10d, %ecx
	movq	%r15, %r11
	cmovl	%r10, %rcx
	cmpl	%ecx, -488(%rbp)
	jle	.L3811
	movq	-512(%rbp), %r10
	leaq	-344(%rbp), %rbx
	xorl	%r8d, %r8d
	xorl	%r12d, %r12d
	movl	$1, %r9d
	jmp	.L3685
.L4013:
	orl	$-128, %eax
	movb	%al, 8(%r14)
	jmp	.L3762
.L3810:
	xorl	%r8d, %r8d
	movl	$1, %r9d
	jmp	.L3746
.L3792:
	movq	%r15, %r11
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	leaq	-344(%rbp), %rbx
	jmp	.L3683
.L3811:
	leaq	-344(%rbp), %rbx
	xorl	%r8d, %r8d
	movl	$1, %r9d
	jmp	.L3684
.L3803:
	movq	%r14, %r12
	movq	%r15, %rbx
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	jmp	.L3745
.L3658:
	movb	$-128, 8(%r15)
	jmp	.L3681
.L3721:
	movb	$-128, 8(%r14)
	jmp	.L3743
.L3981:
	call	__stack_chk_fail@PLT
.L4011:
	orl	$16, -428(%rbp)
	movzbl	8(%r14), %esi
	jmp	.L3712
.L4009:
	orl	$16, -428(%rbp)
	movzbl	9(%r15), %r9d
	movl	(%r15), %ecx
	jmp	.L3648
.L3616:
	movq	-544(%rbp), %rax
	orl	$16, (%rax)
	jmp	.L3598
.L3985:
	movq	-544(%rbp), %rax
	movq	-576(%rbp), %rdi
	orl	$16, (%rax)
	testq	%rdi, %rdi
	jne	.L3980
	jmp	.L3598
	.cfi_endproc
.LFE2121:
	.size	_ZL7decLnOpP9decNumberPKS_P10decContextPj, .-_ZL7decLnOpP9decNumberPKS_P10decContextPj
	.p2align 4
	.globl	uprv_decNumberLn_67
	.type	uprv_decNumberLn_67, @function
uprv_decNumberLn_67:
.LFB2073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$999999, (%rdx)
	movl	$0, -28(%rbp)
	jg	.L4021
	cmpl	$999999, 4(%rdx)
	jle	.L4042
.L4021:
	movl	$64, -28(%rbp)
	movl	$64, %esi
.L4027:
	movl	$32, %eax
	movq	$1, 0(%r13)
	movw	%ax, 8(%r13)
.L4026:
	movq	%r12, %rdi
	call	uprv_decContextSetStatus_67@PLT
.L4029:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4043
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4042:
	.cfi_restore_state
	cmpl	$-999999, 8(%rdx)
	jl	.L4021
	movl	(%rsi), %eax
	cmpl	$999999, %eax
	jg	.L4024
	movl	4(%rsi), %edx
	addl	%eax, %edx
	addl	$1999996, %edx
	cmpl	$2999996, %edx
	jbe	.L4025
	cmpb	$0, 9(%rsi)
	jne	.L4024
	cmpl	$1, %eax
	jne	.L4024
	testb	$112, 8(%rsi)
	je	.L4025
	.p2align 4,,10
	.p2align 3
.L4024:
	movl	$128, -28(%rbp)
	movl	$128, %esi
	jmp	.L4027
	.p2align 4,,10
	.p2align 3
.L4025:
	leaq	-28(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZL7decLnOpP9decNumberPKS_P10decContextPj
	movl	-28(%rbp), %esi
	testl	%esi, %esi
	je	.L4029
	testb	$-35, %sil
	je	.L4026
	testl	$1073741824, %esi
	je	.L4027
	andl	$-1073741825, %esi
	jmp	.L4026
.L4043:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2073:
	.size	uprv_decNumberLn_67, .-uprv_decNumberLn_67
	.p2align 4
	.globl	uprv_decNumberLog10_67
	.type	uprv_decNumberLog10_67, @function
uprv_decNumberLog10_67:
.LFB2075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$999999, (%rdx)
	movl	$0, -272(%rbp)
	movl	$0, -268(%rbp)
	jg	.L4045
	cmpl	$999999, 4(%rdx)
	jle	.L4139
.L4045:
	movl	$64, -272(%rbp)
	movl	$64, %r13d
.L4083:
	movl	$32, %esi
	movq	$1, (%r14)
	movw	%si, 8(%r14)
.L4082:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	uprv_decContextSetStatus_67@PLT
.L4081:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4140
	addq	$264, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4139:
	.cfi_restore_state
	cmpl	$-999999, 8(%rdx)
	jl	.L4045
	movl	(%rsi), %eax
	movq	%rsi, %r13
	cmpl	$999999, %eax
	jg	.L4048
	movl	4(%rsi), %edx
	addl	%eax, %edx
	addl	$1999996, %edx
	cmpl	$2999996, %edx
	jbe	.L4049
	cmpb	$0, 9(%rsi)
	jne	.L4048
	cmpl	$1, %eax
	jne	.L4048
	testb	$112, 8(%rsi)
	jne	.L4048
.L4049:
	leaq	-256(%rbp), %r15
	movl	$64, %esi
	movq	%r15, %rdi
	call	uprv_decContextDefault_67@PLT
	movzbl	8(%r13), %eax
	movl	%eax, %r10d
	andl	$240, %r10d
	jne	.L4052
	cmpb	$0, 9(%r13)
	movl	0(%r13), %ecx
	jne	.L4050
	cmpl	$1, %ecx
	je	.L4051
.L4050:
	movb	%al, -216(%rbp)
	movl	4(%r13), %eax
	leaq	-264(%rbp), %rbx
	leaq	9(%r13), %rdx
	leaq	-224(%rbp), %rdi
	leaq	-260(%rbp), %r9
	movq	%rbx, %r8
	movq	%r15, %rsi
	movb	%r10b, -280(%rbp)
	movl	$0, -264(%rbp)
	movl	$0, -260(%rbp)
	movl	$1, -256(%rbp)
	movl	%eax, -220(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	testb	$32, -260(%rbp)
	jne	.L4052
	cmpb	$1, -215(%rbp)
	movzbl	-280(%rbp), %r10d
	je	.L4141
	.p2align 4,,10
	.p2align 3
.L4052:
	movl	0(%r13), %ecx
.L4051:
	addl	$6, %ecx
	cmpl	%ecx, (%r12)
	cmovge	(%r12), %ecx
	leal	3(%rcx), %edx
	addl	$14, %ecx
	cmpl	$49, %edx
	jg	.L4063
	movslq	%edx, %rax
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rax), %ecx
	addl	$11, %ecx
.L4063:
	cmpl	$60, %ecx
	jbe	.L4094
	movl	%ecx, %edi
	movl	%edx, -280(%rbp)
	call	uprv_malloc_67@PLT
	movl	-280(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L4142
	movq	%rax, %rbx
.L4064:
	leaq	-272(%rbp), %r9
	movl	%edx, -256(%rbp)
	movq	%r13, %rsi
	movq	%r15, %rdx
	movabsq	$-4294963000032705, %rax
	movq	%r9, %rcx
	movq	%rbx, %rdi
	movq	%r10, -288(%rbp)
	movq	%r9, -280(%rbp)
	movq	%rax, -252(%rbp)
	movb	$0, -232(%rbp)
	call	_ZL7decLnOpP9decNumberPKS_P10decContextPj
	movl	-272(%rbp), %r13d
	movq	-280(%rbp), %r9
	movq	-288(%rbp), %r10
	testb	$-35, %r13b
	je	.L4066
	testl	$1073741824, %r13d
	jne	.L4066
	testq	%r10, %r10
	je	.L4067
.L4135:
	movq	%r10, %rdi
	call	uprv_free_67@PLT
	movl	-272(%rbp), %r13d
.L4061:
	testl	%r13d, %r13d
	je	.L4081
	movl	%r13d, %eax
	andl	$221, %eax
	jmp	.L4084
.L4142:
	movl	-272(%rbp), %eax
	movl	%eax, %r13d
	andl	$221, %eax
	orl	$16, %r13d
	orl	$16, %eax
	movl	%r13d, -272(%rbp)
.L4084:
	testl	%eax, %eax
	je	.L4082
	.p2align 4,,10
	.p2align 3
.L4067:
	testl	$1073741824, %r13d
	je	.L4083
	andl	$-1073741825, %r13d
	jmp	.L4082
	.p2align 4,,10
	.p2align 3
.L4066:
	movzbl	8(%rbx), %eax
	testb	$112, %al
	jne	.L4068
	cmpb	$0, 9(%rbx)
	jne	.L4069
	cmpl	$1, (%rbx)
	je	.L4143
.L4069:
	movl	(%r12), %edi
	leal	3(%rdi), %edx
	addl	$14, %edi
	cmpl	$49, %edx
	jg	.L4077
	movslq	%edx, %rax
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rax), %edi
	addl	$11, %edi
.L4077:
	cmpl	$60, %edi
	jbe	.L4078
	movq	%r9, -296(%rbp)
	movl	%edx, -288(%rbp)
	movq	%r10, -280(%rbp)
	call	uprv_malloc_67@PLT
	movq	-280(%rbp), %r10
	movl	-288(%rbp), %edx
	testq	%rax, %rax
	movq	-296(%rbp), %r9
	movq	%rax, %r13
	je	.L4144
	xorl	%ecx, %ecx
	movl	%edx, -256(%rbp)
	movq	%rax, %rdi
	movq	%r15, %rdx
	movw	%cx, -216(%rbp)
	leaq	-224(%rbp), %rsi
	leaq	-268(%rbp), %rcx
	movq	%r10, -280(%rbp)
	movq	%r9, -288(%rbp)
	movq	$2, -224(%rbp)
	movb	$1, -214(%rbp)
	call	_ZL7decLnOpP9decNumberPKS_P10decContextPj
	movl	(%r12), %eax
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	-288(%rbp), %r9
	movl	$128, %r8d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%eax, -256(%rbp)
	call	_ZL11decDivideOpP9decNumberPKS_S2_P10decContexthPj
	movq	-280(%rbp), %r10
	testq	%r10, %r10
	jne	.L4145
.L4089:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
.L4136:
	movl	-272(%rbp), %r13d
	jmp	.L4061
	.p2align 4,,10
	.p2align 3
.L4094:
	leaq	-192(%rbp), %rbx
	xorl	%r10d, %r10d
	jmp	.L4064
	.p2align 4,,10
	.p2align 3
.L4048:
	movl	$128, -272(%rbp)
	movl	$128, %r13d
	jmp	.L4083
	.p2align 4,,10
	.p2align 3
.L4068:
	cmpq	%r14, %rbx
	je	.L4075
	movq	(%rbx), %rax
	movl	(%rbx), %edx
	movq	%rax, (%r14)
	movzwl	8(%rbx), %eax
	movw	%ax, 8(%r14)
	cmpl	$1, %edx
	jle	.L4075
	leaq	10(%r14), %rdi
	leaq	9(%rbx), %rax
	movslq	%edx, %rcx
	cmpl	$49, %edx
	jg	.L4074
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rcx), %ecx
.L4074:
	addq	%rcx, %rax
	leaq	10(%rbx), %rsi
	cmpq	%rsi, %rax
	jbe	.L4075
	subq	%rbx, %rax
	movq	%r10, -280(%rbp)
	leaq	-10(%rax), %rdx
	call	memcpy@PLT
	movq	-280(%rbp), %r10
.L4075:
	testq	%r10, %r10
	jne	.L4135
	jmp	.L4061
	.p2align 4,,10
	.p2align 3
.L4078:
	leaq	-128(%rbp), %r13
	movl	%edx, -256(%rbp)
	xorl	%eax, %eax
	leaq	-268(%rbp), %rcx
	leaq	-224(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%r10, -280(%rbp)
	movq	%r9, -288(%rbp)
	movq	$2, -224(%rbp)
	movw	%ax, -216(%rbp)
	movb	$1, -214(%rbp)
	call	_ZL7decLnOpP9decNumberPKS_P10decContextPj
	movl	(%r12), %eax
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	-288(%rbp), %r9
	movl	$128, %r8d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%eax, -256(%rbp)
	call	_ZL11decDivideOpP9decNumberPKS_S2_P10decContexthPj
	movq	-280(%rbp), %r10
	testq	%r10, %r10
	jne	.L4135
	jmp	.L4136
	.p2align 4,,10
	.p2align 3
.L4143:
	cmpq	%r14, %rbx
	je	.L4075
	movb	%al, 8(%r14)
	movl	4(%rbx), %eax
	movl	$1, (%r14)
	movl	%eax, 4(%r14)
	movb	$0, 9(%r14)
	jmp	.L4075
	.p2align 4,,10
	.p2align 3
.L4141:
	movl	-220(%rbp), %edi
	testl	%edi, %edi
	js	.L4053
	movl	$0, %r11d
	movl	%edi, %edx
	movl	$1, %ecx
	movq	$1, -224(%rbp)
	movw	%r11w, -216(%rbp)
	leaq	-215(%rbp), %r11
	jne	.L4055
.L4054:
	movb	%r10b, 8(%r14)
	movq	%r11, %rdx
	movq	%rbx, %r8
	movq	%r12, %rsi
	movl	%edi, 4(%r14)
	movq	%r14, %rdi
	leaq	-272(%rbp), %r9
	movl	$0, -264(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r9, %rcx
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-272(%rbp), %r13d
	jmp	.L4061
.L4053:
	cmpl	$-2147483648, %edi
	je	.L4146
	movq	$1, -224(%rbp)
	xorl	%r8d, %r8d
	movl	%edi, %edx
	movw	%r8w, -216(%rbp)
	negl	%edx
.L4055:
	leaq	-215(%rbp), %r11
	movl	$3435973837, %r8d
	movq	%r11, %rsi
	.p2align 4,,10
	.p2align 3
.L4057:
	movl	%edx, %eax
	movl	%edx, %r15d
	addq	$1, %rsi
	imulq	%r8, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r15d
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%r15b, -1(%rsi)
	cmpl	$9, %ecx
	ja	.L4057
	subq	%r11, %rsi
	movl	%esi, %ecx
	movslq	%esi, %rsi
	leaq	-1(%r11,%rsi), %rax
	cmpq	%r11, %rax
	jb	.L4058
.L4059:
	cmpb	$0, (%rax)
	jne	.L4058
	cmpl	$1, %ecx
	je	.L4058
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%r11, %rax
	jnb	.L4059
.L4058:
	movl	%ecx, -224(%rbp)
	movl	-220(%rbp), %eax
	testl	%edi, %edi
	jns	.L4147
	movb	$-128, -216(%rbp)
	movl	%eax, %edi
	movl	$-128, %r10d
	jmp	.L4054
.L4147:
	movzbl	-216(%rbp), %r10d
	movl	%eax, %edi
	jmp	.L4054
.L4146:
	xorl	%r9d, %r9d
	movl	$-2147483648, %edx
	movq	$1, -224(%rbp)
	movw	%r9w, -216(%rbp)
	jmp	.L4055
.L4140:
	call	__stack_chk_fail@PLT
.L4145:
	movq	%r10, %rdi
	call	uprv_free_67@PLT
	jmp	.L4089
.L4144:
	movl	-272(%rbp), %eax
	movl	%eax, %r13d
	orl	$16, %r13d
	movl	%r13d, -272(%rbp)
	testq	%r10, %r10
	jne	.L4135
	andl	$221, %eax
	orl	$16, %eax
	jmp	.L4084
	.cfi_endproc
.LFE2075:
	.size	uprv_decNumberLog10_67, .-uprv_decNumberLog10_67
	.p2align 4
	.globl	uprv_decNumberPower_67
	.type	uprv_decNumberPower_67, @function
uprv_decNumberPower_67:
.LFB2087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movzbl	8(%rsi), %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	movzbl	8(%rdx), %ecx
	movl	$0, -248(%rbp)
	movl	$0, -244(%rbp)
	movl	%eax, -280(%rbp)
	movl	%r15d, %eax
	orl	%ecx, %eax
	testb	$112, %al
	je	.L4152
	testb	$48, %al
	je	.L4150
	leaq	-244(%rbp), %r8
	movq	%r14, %rcx
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
.L4151:
	movl	-244(%rbp), %esi
	testl	%esi, %esi
	jne	.L4285
.L4213:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4286
	addq	$280, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4152:
	.cfi_restore_state
	movq	%r9, %rdi
	movb	%cl, -272(%rbp)
	movq	%r9, -264(%rbp)
	call	_ZL9decGetIntPK9decNumber
	movl	%r15d, %r8d
	movq	-264(%rbp), %r9
	movzbl	-272(%rbp), %ecx
	andl	$64, %r8d
	cmpl	$-2147483648, %eax
	movl	%eax, %ebx
	je	.L4287
	testb	%r15b, %r15b
	jns	.L4232
	testb	$1, %al
	jne	.L4222
.L4232:
	movb	$0, -272(%rbp)
	movl	$64, %edi
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L4288
.L4171:
	movq	$1, (%r12)
	movb	$0, 8(%r12)
	testl	%ebx, %ebx
	jne	.L4174
	movb	$1, 9(%r12)
	jmp	.L4213
	.p2align 4,,10
	.p2align 3
.L4287:
	testb	%r8b, %r8b
	jne	.L4289
	movzbl	9(%r13), %esi
	testb	%sil, %sil
	jne	.L4230
	cmpl	$1, 0(%r13)
	je	.L4231
	movb	$0, -272(%rbp)
	movl	%esi, %r8d
.L4217:
	testb	%r15b, %r15b
	jns	.L4290
.L4181:
	movl	$128, -244(%rbp)
	movl	$128, %esi
.L4211:
	movl	$32, %ecx
	movq	$1, (%r12)
	movw	%cx, 8(%r12)
.L4210:
	movq	%r14, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L4213
	.p2align 4,,10
	.p2align 3
.L4150:
	testb	$64, %cl
	je	.L4152
	testb	%r15b, %r15b
	jns	.L4153
	cmpb	$0, 9(%rsi)
	jne	.L4181
	cmpl	$1, (%rsi)
	jne	.L4181
	andl	$112, %r15d
	jne	.L4181
	.p2align 4,,10
	.p2align 3
.L4153:
	movl	$256, %r11d
	xorl	%edx, %edx
	leaq	-204(%rbp), %rsi
	movq	%r13, %rdi
	movb	%cl, -264(%rbp)
	movq	$1, -204(%rbp)
	movw	%r11w, -196(%rbp)
	call	_ZL10decComparePK9decNumberS1_h
	movzbl	-264(%rbp), %ecx
	cmpl	$-2147483648, %eax
	je	.L4156
	movq	$1, -192(%rbp)
	movb	$0, -184(%rbp)
	testl	%eax, %eax
	je	.L4157
	movb	$1, -183(%rbp)
	js	.L4291
.L4158:
	xorl	%edx, %edx
	movq	$1, (%r12)
	movw	%dx, 8(%r12)
.L4160:
	cmpb	$0, -183(%rbp)
	jne	.L4162
	movl	(%r14), %ebx
	movl	$1, %eax
	movb	$1, 9(%r12)
	movl	%ebx, %edx
	subl	$1, %edx
	je	.L4163
	leaq	9(%r12), %rdi
	movl	$1, %esi
	call	_ZL14decShiftToMostPhii.part.0
.L4163:
	movl	%eax, (%r12)
	movl	$1, %eax
	subl	%ebx, %eax
	movl	%eax, 4(%r12)
	movl	-244(%rbp), %eax
	movl	%eax, %esi
	andl	$221, %eax
	orl	$2080, %esi
	movl	%esi, -244(%rbp)
	jmp	.L4212
	.p2align 4,,10
	.p2align 3
.L4222:
	movb	$-128, -272(%rbp)
	movl	$-64, %edi
	movl	$-128, %eax
	testb	%r8b, %r8b
	jne	.L4171
.L4288:
	leal	2147483646(%rbx), %esi
	movl	$1, %r8d
	cmpl	$1, %esi
	movzbl	9(%r13), %esi
	seta	-264(%rbp)
	testb	%sil, %sil
	jne	.L4173
	cmpl	$1, 0(%r13)
	je	.L4218
.L4173:
	cmpb	$0, -264(%rbp)
	je	.L4217
	testl	%ebx, %ebx
	jne	.L4186
	movl	$256, %edi
	movq	$1, (%r12)
	movw	%di, 8(%r12)
	jmp	.L4213
	.p2align 4,,10
	.p2align 3
.L4174:
	movb	$0, 9(%r12)
.L4216:
	testb	%cl, %cl
	cmovns	%edi, %eax
	movb	%al, 8(%r12)
	jmp	.L4213
	.p2align 4,,10
	.p2align 3
.L4285:
	movl	%esi, %eax
	andl	$221, %eax
.L4212:
	testl	%eax, %eax
	je	.L4210
	testl	$1073741824, %esi
	je	.L4211
	andl	$-1073741825, %esi
	jmp	.L4210
	.p2align 4,,10
	.p2align 3
.L4289:
	xorl	%eax, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	cmpb	$0, 8(%r13)
	js	.L4181
	movl	$64, %edi
	xorl	%eax, %eax
	jmp	.L4216
	.p2align 4,,10
	.p2align 3
.L4186:
	movq	16(%r14), %rax
	movl	-280(%rbp), %edx
	movdqu	(%r14), %xmm0
	movq	%rax, -224(%rbp)
	movl	24(%r14), %eax
	movaps	%xmm0, -240(%rbp)
	movl	%eax, -216(%rbp)
	movl	4(%r9), %eax
	movl	$3, -228(%rbp)
	addl	(%r9), %eax
	leal	2(%rdx,%rax), %eax
	movl	%eax, -240(%rbp)
	cmpl	$999999999, %eax
	jg	.L4181
	movl	%ebx, %edx
	sarl	$31, %edx
	xorl	%edx, %ebx
	subl	%edx, %ebx
.L4185:
	cmpl	$49, %eax
	jle	.L4292
	leal	11(%rax), %r11d
.L4188:
	movl	%r11d, %edi
	movq	%r9, -304(%rbp)
	movb	%r8b, -288(%rbp)
	movl	%r11d, -280(%rbp)
	call	uprv_malloc_67@PLT
	movl	-280(%rbp), %r11d
	movzbl	-288(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, -296(%rbp)
	movq	-304(%rbp), %r9
	je	.L4293
	movq	%rax, %r10
.L4189:
	cmpb	$0, -264(%rbp)
	je	.L4294
	movl	$256, %esi
	xorl	%r15d, %r15d
	leaq	-244(%rbp), %rax
	cmpb	$0, 8(%r9)
	movq	$1, (%r10)
	movw	%si, 8(%r10)
	movq	%rax, -288(%rbp)
	js	.L4295
.L4194:
	movl	$1, %r9d
	leaq	-240(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r15, -304(%rbp)
	movq	%r12, -312(%rbp)
	movl	-244(%rbp), %eax
	movq	%rdi, %r15
	movq	%r13, -280(%rbp)
	movq	-288(%rbp), %r12
	movq	%r10, %r13
	movq	%r14, -320(%rbp)
	movl	%ebx, %r14d
	movl	%r9d, %ebx
	jmp	.L4204
	.p2align 4,,10
	.p2align 3
.L4200:
	cmpl	$31, %ebx
	je	.L4283
	testb	%dl, %dl
	jne	.L4203
.L4202:
	addl	$1, %ebx
.L4204:
	testb	$34, %ah
	je	.L4197
	testb	$2, %ah
	jne	.L4296
	cmpb	$0, 9(%r13)
	jne	.L4197
	cmpl	$1, 0(%r13)
	je	.L4297
	.p2align 4,,10
	.p2align 3
.L4197:
	addl	%r14d, %r14d
	jns	.L4200
	movq	-280(%rbp), %rdx
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	cmpl	$31, %ebx
	je	.L4298
.L4203:
	movq	%r13, %rdx
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	movl	-244(%rbp), %eax
	movzbl	-264(%rbp), %edx
	jmp	.L4202
	.p2align 4,,10
	.p2align 3
.L4230:
	movb	$0, -272(%rbp)
	jmp	.L4217
	.p2align 4,,10
	.p2align 3
.L4290:
	cmpl	$999999, -280(%rbp)
	jg	.L4179
	cmpl	$999999, 4(%r14)
	jg	.L4179
	cmpl	$-999999, 8(%r14)
	jl	.L4179
	movl	0(%r13), %eax
	cmpl	$999999, %eax
	jg	.L4181
	movl	4(%r13), %edi
	addl	%eax, %edi
	addl	$1999996, %edi
	cmpl	$2999996, %edi
	jbe	.L4182
	cmpl	$1, %eax
	jne	.L4181
	movl	%r15d, %edx
	andl	$112, %edx
	orb	%sil, %dl
	jne	.L4181
.L4182:
	movl	(%r9), %eax
	cmpl	$999999, %eax
	jg	.L4181
	movl	4(%r9), %edx
	addl	%eax, %edx
	addl	$1999996, %edx
	cmpl	$2999996, %edx
	jbe	.L4183
	subl	$1, %eax
	jne	.L4181
	andl	$112, %ecx
	orb	9(%r9), %cl
	jne	.L4181
.L4183:
	leaq	-240(%rbp), %rax
	movl	$64, %esi
	movq	%r9, -288(%rbp)
	movq	%rax, %rdi
	movb	%r8b, -280(%rbp)
	call	uprv_decContextDefault_67@PLT
	movl	0(%r13), %ecx
	movl	(%r14), %edx
	movabsq	$-4294963000032705, %rax
	movq	%rax, -236(%rbp)
	movzbl	-280(%rbp), %r8d
	leal	10(%rcx), %esi
	leal	10(%rdx), %eax
	cmpl	%edx, %ecx
	movb	$0, -216(%rbp)
	cmovge	%esi, %eax
	movb	$0, -264(%rbp)
	movq	-288(%rbp), %r9
	movl	%eax, -240(%rbp)
	jmp	.L4185
	.p2align 4,,10
	.p2align 3
.L4179:
	movl	$64, -244(%rbp)
	movl	$64, %esi
	jmp	.L4211
	.p2align 4,,10
	.p2align 3
.L4231:
	movb	$0, -264(%rbp)
	xorl	%eax, %eax
	movl	$64, %edi
	movb	$0, -272(%rbp)
.L4218:
	testb	$112, %r15b
	jne	.L4224
	testl	%ebx, %ebx
	je	.L4181
	testb	%cl, %cl
	movq	$1, (%r12)
	cmovs	%edi, %eax
	movb	$0, 9(%r12)
	movb	%al, 8(%r12)
	jmp	.L4213
	.p2align 4,,10
	.p2align 3
.L4297:
	movzbl	8(%r13), %ecx
	testb	$112, %cl
	jne	.L4197
	movq	-304(%rbp), %r15
	movq	-312(%rbp), %r12
	movq	%r13, %r10
	movq	-320(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L4199:
	andl	$127, %ecx
	orb	-272(%rbp), %cl
	movq	%r10, %rdi
	movq	%r14, %rsi
	movb	%cl, 8(%r10)
	movq	-288(%rbp), %rcx
	leaq	-248(%rbp), %rdx
	movq	%r10, -264(%rbp)
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movq	-264(%rbp), %r10
	cmpq	%r12, %r10
	je	.L4206
	movq	(%r10), %rax
	movl	(%r10), %edx
	movq	%rax, (%r12)
	movzwl	8(%r10), %eax
	movw	%ax, 8(%r12)
	cmpl	$1, %edx
	jle	.L4206
	leaq	10(%r12), %rdi
	leaq	9(%r10), %rax
	movslq	%edx, %rcx
	cmpl	$49, %edx
	jg	.L4208
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rcx), %ecx
.L4208:
	addq	%rcx, %rax
	leaq	10(%r10), %rsi
	cmpq	%rsi, %rax
	jbe	.L4206
	subq	%r10, %rax
	leaq	-10(%rax), %rdx
	call	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L4206:
	movq	-296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4209
	call	uprv_free_67@PLT
.L4209:
	testq	%r15, %r15
	je	.L4151
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	jmp	.L4151
	.p2align 4,,10
	.p2align 3
.L4292:
	cltq
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	leal	11(%rax), %r11d
	cmpl	$60, %r11d
	ja	.L4188
	movq	$0, -296(%rbp)
	leaq	-192(%rbp), %r10
	jmp	.L4189
	.p2align 4,,10
	.p2align 3
.L4224:
	xorl	%esi, %esi
	jmp	.L4173
	.p2align 4,,10
	.p2align 3
.L4157:
	movb	$0, -183(%rbp)
	jmp	.L4158
	.p2align 4,,10
	.p2align 3
.L4294:
	leaq	-240(%rbp), %rax
	leaq	-244(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%rax, %rdx
	movq	%r9, -272(%rbp)
	movq	%rax, %rbx
	movq	%r10, -264(%rbp)
	movb	%r8b, -280(%rbp)
	movq	%rcx, -288(%rbp)
	call	_ZL7decLnOpP9decNumberPKS_P10decContextPj
	movq	-264(%rbp), %r10
	movq	-272(%rbp), %r9
	cmpb	$0, 9(%r10)
	leaq	9(%r10), %r13
	jne	.L4191
	movl	(%r10), %edx
	movzbl	-280(%rbp), %r8d
	cmpl	$1, %edx
	je	.L4299
.L4191:
	movq	-288(%rbp), %r15
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r10, %rdi
	movq	%rbx, %rcx
	movq	%r10, -264(%rbp)
	movq	%r15, %r8
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	movq	-264(%rbp), %r10
	movq	%r15, %rcx
	movq	%rbx, %rdx
	xorl	%r15d, %r15d
	movq	%r10, %rsi
	movq	%r10, %rdi
	call	_ZL8decExpOpP9decNumberPKS_P10decContextPj
	movq	-264(%rbp), %r10
	movzbl	8(%r10), %eax
.L4192:
	movb	%al, 8(%r12)
	movl	4(%r10), %eax
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	-288(%rbp), %r9
	movl	(%r10), %ecx
	leaq	-248(%rbp), %rbx
	movq	%r12, %rdi
	movl	%eax, 4(%r12)
	movq	%rbx, %r8
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r9, %rcx
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	jmp	.L4206
	.p2align 4,,10
	.p2align 3
.L4156:
	movl	$32, %r8d
	movl	$16, %esi
	movq	%r14, %rdi
	movb	%cl, -264(%rbp)
	movq	$1, -192(%rbp)
	movw	%r8w, -184(%rbp)
	call	uprv_decContextSetStatus_67@PLT
	xorl	%r9d, %r9d
	cmpb	$0, -184(%rbp)
	movq	$1, (%r12)
	movw	%r9w, 8(%r12)
	movzbl	-264(%rbp), %ecx
	jns	.L4160
.L4159:
	testb	%cl, %cl
	jns	.L4151
.L4161:
	movb	$64, 8(%r12)
	jmp	.L4151
	.p2align 4,,10
	.p2align 3
.L4283:
	movq	-304(%rbp), %r15
	movq	-312(%rbp), %r12
	movq	%r13, %r10
	movq	-320(%rbp), %r14
.L4201:
	testb	$34, %ah
	jne	.L4205
	movzbl	8(%r10), %eax
	leaq	9(%r10), %r13
	jmp	.L4192
	.p2align 4,,10
	.p2align 3
.L4162:
	testb	%cl, %cl
	jns	.L4161
	jmp	.L4151
.L4296:
	movq	-304(%rbp), %r15
	movq	-312(%rbp), %r12
	movq	%r13, %r10
	movq	-320(%rbp), %r14
	movzbl	8(%r13), %ecx
	jmp	.L4199
.L4295:
	movl	%r11d, -312(%rbp)
	leaq	-204(%rbp), %r11
	movq	%r10, %rsi
	movq	%r11, %rdi
	movq	%r11, -280(%rbp)
	movq	%r10, -304(%rbp)
	call	uprv_decNumberCopy_67
	movq	-304(%rbp), %r10
	movq	%r13, %rdx
	leaq	-240(%rbp), %rax
	movq	-288(%rbp), %r9
	movq	%r11, %rsi
	movl	$128, %r8d
	movq	%rax, %rcx
	movq	%r10, %rdi
	call	_ZL11decDivideOpP9decNumberPKS_S2_P10decContexthPj
	movl	-312(%rbp), %r11d
	movq	-304(%rbp), %r10
	cmpl	$60, %r11d
	jbe	.L4228
	movl	%r11d, %edi
	call	uprv_malloc_67@PLT
	movq	-304(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L4300
	movq	%rax, %r13
.L4195:
	movq	%r10, %rsi
	movq	%r13, %rdi
	movq	%r10, -304(%rbp)
	call	uprv_decNumberCopy_67
	movq	-304(%rbp), %r10
	movq	-280(%rbp), %rsi
	movq	%r10, %rdi
	movq	%r10, -280(%rbp)
	call	uprv_decNumberCopy_67
	movq	-280(%rbp), %r10
	jmp	.L4194
.L4291:
	xorl	%r10d, %r10d
	movb	$-128, -184(%rbp)
	movq	$1, (%r12)
	movw	%r10w, 8(%r12)
	jmp	.L4159
.L4298:
	movq	-304(%rbp), %r15
	movq	-312(%rbp), %r12
	movq	%r13, %r10
	movq	-320(%rbp), %r14
	movl	-244(%rbp), %eax
	jmp	.L4201
.L4228:
	leaq	-128(%rbp), %r13
	jmp	.L4195
.L4299:
	movzbl	8(%r10), %eax
	testb	$112, %al
	jne	.L4191
	movb	$1, 9(%r10)
	xorl	%r15d, %r15d
	testb	%r8b, %r8b
	jne	.L4192
	movl	(%r14), %ebx
	movl	%ebx, %r8d
	subl	$1, %r8d
	je	.L4193
	movl	%r8d, %edx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZL14decShiftToMostPhii.part.0
	movq	-264(%rbp), %r10
	movl	%eax, %edx
	movzbl	8(%r10), %eax
.L4193:
	movl	%edx, (%r10)
	movl	$1, %edx
	xorl	%r15d, %r15d
	orl	$2080, -244(%rbp)
	subl	%ebx, %edx
	movl	%edx, 4(%r10)
	jmp	.L4192
.L4286:
	call	__stack_chk_fail@PLT
.L4205:
	movzbl	8(%r10), %ecx
	jmp	.L4199
.L4300:
	movl	-244(%rbp), %eax
	movq	-296(%rbp), %rbx
	movl	%eax, %esi
	orl	$16, %esi
	movl	%esi, -244(%rbp)
	testq	%rbx, %rbx
	je	.L4196
	movq	%rbx, %rdi
	call	uprv_free_67@PLT
	jmp	.L4151
.L4293:
	movl	-244(%rbp), %eax
	movl	%eax, %esi
	andl	$221, %eax
	orl	$16, %esi
	orl	$16, %eax
	movl	%esi, -244(%rbp)
	jmp	.L4212
.L4196:
	andl	$221, %eax
	orl	$16, %eax
	jmp	.L4212
	.cfi_endproc
.LFE2087:
	.size	uprv_decNumberPower_67, .-uprv_decNumberPower_67
	.p2align 4
	.globl	uprv_decNumberMinus_67
	.type	uprv_decNumberMinus_67, @function
uprv_decNumberMinus_67:
.LFB2080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	4(%rsi), %r13d
	movzbl	8(%rsi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -76(%rbp)
	movl	$1, -68(%rbp)
	movw	%dx, -60(%rbp)
	movl	%r13d, -64(%rbp)
	testb	$112, %bl
	jne	.L4329
	movb	%bl, 8(%rdi)
	movl	4(%rsi), %eax
	leaq	-76(%rbp), %r15
	leaq	-72(%rbp), %r8
	leaq	9(%rsi), %rdx
	movq	%r15, %r9
	movq	%r8, -88(%rbp)
	movl	%eax, 4(%rdi)
	movl	(%rsi), %ecx
	movq	%r14, %rsi
	movl	$0, -72(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movzbl	8(%r12), %eax
	movl	%r13d, %ecx
	movq	-88(%rbp), %r8
	movl	4(%r12), %edx
	addl	$-128, %eax
	subl	%edx, %ecx
	cmpb	$0, 9(%r12)
	movb	%al, 8(%r12)
	jne	.L4306
	cmpl	$1, (%r12)
	jne	.L4306
	testb	$112, %al
	jne	.L4306
	testl	%ecx, %ecx
	jns	.L4307
	movl	%r13d, 4(%r12)
.L4307:
	addb	$-128, %bl
	jns	.L4309
	cmpl	$6, 12(%r14)
	je	.L4310
	movb	$0, 8(%r12)
	jmp	.L4309
	.p2align 4,,10
	.p2align 3
.L4329:
	testb	$48, %bl
	jne	.L4330
	notl	%ebx
	movq	$1, (%rdi)
	andl	$-128, %ebx
	movb	$0, 9(%rdi)
	orl	$64, %ebx
	movb	%bl, 8(%rdi)
.L4305:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4331
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4330:
	.cfi_restore_state
	leaq	-68(%rbp), %r9
	movq	%rsi, %rdx
	leaq	-76(%rbp), %r8
	movq	%r14, %rcx
	movq	%r9, %rsi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-76(%rbp), %esi
.L4304:
	testl	%esi, %esi
	je	.L4305
	testb	$-35, %sil
	je	.L4314
	testl	$1073741824, %esi
	je	.L4315
	andl	$-1073741825, %esi
.L4314:
	movq	%r14, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L4305
	.p2align 4,,10
	.p2align 3
.L4315:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L4314
	.p2align 4,,10
	.p2align 3
.L4306:
	testl	%ecx, %ecx
	js	.L4332
.L4309:
	movq	%r14, %rsi
	movq	%r15, %rcx
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-76(%rbp), %esi
	jmp	.L4304
	.p2align 4,,10
	.p2align 3
.L4332:
	movl	(%r12), %esi
	movl	(%r14), %eax
	movl	%esi, %edi
	subl	%ecx, %edi
	cmpl	%eax, %edi
	jle	.L4312
	movl	%esi, %ecx
	orl	$2048, -76(%rbp)
	subl	%eax, %ecx
	jne	.L4333
.L4313:
	movl	%esi, (%r12)
	movl	%edx, 4(%r12)
	jmp	.L4309
.L4312:
	subl	%r13d, %edx
.L4316:
	leaq	9(%r12), %rdi
	movq	%r8, -96(%rbp)
	movl	%ecx, -88(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	movl	-88(%rbp), %ecx
	addl	4(%r12), %ecx
	movq	-96(%rbp), %r8
	movl	%eax, %esi
	movl	%ecx, %edx
	jmp	.L4313
.L4333:
	subl	%esi, %eax
	movl	%eax, %edx
	jmp	.L4316
.L4331:
	call	__stack_chk_fail@PLT
.L4310:
	movb	$-128, 8(%r12)
	jmp	.L4309
	.cfi_endproc
.LFE2080:
	.size	uprv_decNumberMinus_67, .-uprv_decNumberMinus_67
	.p2align 4
	.globl	uprv_decNumberPlus_67
	.type	uprv_decNumberPlus_67, @function
uprv_decNumberPlus_67:
.LFB2085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	4(%rsi), %r13d
	movzbl	8(%rsi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -76(%rbp)
	movl	$1, -68(%rbp)
	movw	%dx, -60(%rbp)
	movl	%r13d, -64(%rbp)
	testb	$112, %bl
	jne	.L4362
	movb	%bl, 8(%rdi)
	movl	4(%rsi), %eax
	leaq	-76(%rbp), %r15
	leaq	-72(%rbp), %r8
	leaq	9(%rsi), %rdx
	movq	%r15, %r9
	movq	%r8, -88(%rbp)
	movl	%eax, 4(%rdi)
	movl	(%rsi), %ecx
	movq	%r14, %rsi
	movl	$0, -72(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	4(%r12), %edx
	movl	%r13d, %ecx
	movq	-88(%rbp), %r8
	subl	%edx, %ecx
	cmpb	$0, 9(%r12)
	jne	.L4339
	cmpl	$1, (%r12)
	jne	.L4339
	testb	$112, 8(%r12)
	jne	.L4339
	testl	%ecx, %ecx
	jns	.L4340
	movl	%r13d, 4(%r12)
.L4340:
	testb	%bl, %bl
	jns	.L4342
	cmpl	$6, 12(%r14)
	je	.L4343
	movb	$0, 8(%r12)
	jmp	.L4342
	.p2align 4,,10
	.p2align 3
.L4362:
	testb	$48, %bl
	jne	.L4363
	andl	$-128, %ebx
	movq	$1, (%rdi)
	orl	$64, %ebx
	movb	$0, 9(%rdi)
	movb	%bl, 8(%rdi)
.L4338:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4364
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4363:
	.cfi_restore_state
	leaq	-68(%rbp), %r9
	movq	%rsi, %rdx
	leaq	-76(%rbp), %r8
	movq	%r14, %rcx
	movq	%r9, %rsi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-76(%rbp), %esi
.L4337:
	testl	%esi, %esi
	je	.L4338
	testb	$-35, %sil
	je	.L4347
	testl	$1073741824, %esi
	je	.L4348
	andl	$-1073741825, %esi
.L4347:
	movq	%r14, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L4338
	.p2align 4,,10
	.p2align 3
.L4348:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L4347
	.p2align 4,,10
	.p2align 3
.L4339:
	testl	%ecx, %ecx
	js	.L4365
.L4342:
	movq	%r14, %rsi
	movq	%r15, %rcx
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-76(%rbp), %esi
	jmp	.L4337
	.p2align 4,,10
	.p2align 3
.L4365:
	movl	(%r12), %esi
	movl	(%r14), %eax
	movl	%esi, %edi
	subl	%ecx, %edi
	cmpl	%eax, %edi
	jle	.L4345
	movl	%esi, %ecx
	orl	$2048, -76(%rbp)
	subl	%eax, %ecx
	jne	.L4366
.L4346:
	movl	%esi, (%r12)
	movl	%edx, 4(%r12)
	jmp	.L4342
.L4345:
	subl	%r13d, %edx
.L4349:
	leaq	9(%r12), %rdi
	movq	%r8, -96(%rbp)
	movl	%ecx, -88(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	movl	-88(%rbp), %ecx
	addl	4(%r12), %ecx
	movq	-96(%rbp), %r8
	movl	%eax, %esi
	movl	%ecx, %edx
	jmp	.L4346
.L4366:
	subl	%esi, %eax
	movl	%eax, %edx
	jmp	.L4349
.L4364:
	call	__stack_chk_fail@PLT
.L4343:
	movb	$-128, 8(%r12)
	jmp	.L4342
	.cfi_endproc
.LFE2085:
	.size	uprv_decNumberPlus_67, .-uprv_decNumberPlus_67
	.p2align 4
	.globl	uprv_decNumberAbs_67
	.type	uprv_decNumberAbs_67, @function
uprv_decNumberAbs_67:
.LFB2061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	4(%rsi), %r14d
	movzbl	8(%rsi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -76(%rbp)
	movl	$1, -68(%rbp)
	movw	%cx, -60(%rbp)
	movl	%r14d, -64(%rbp)
	testb	$112, %bl
	jne	.L4393
	movb	%bl, 8(%rdi)
	movl	4(%rsi), %eax
	leaq	-76(%rbp), %r15
	leaq	-72(%rbp), %r8
	leaq	9(%rsi), %rdx
	movq	%r15, %r9
	movq	%r8, -88(%rbp)
	andl	$-128, %ebx
	movl	%eax, 4(%rdi)
	movl	(%rsi), %ecx
	movq	%r13, %rsi
	movl	$0, -72(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	4(%r12), %edx
	movl	%r14d, %ecx
	movq	-88(%rbp), %r8
	xorb	8(%r12), %bl
	subl	%edx, %ecx
	cmpb	$0, 9(%r12)
	movb	%bl, 8(%r12)
	jne	.L4372
	cmpl	$1, (%r12)
	jne	.L4372
	andl	$112, %ebx
	jne	.L4372
	testl	%ecx, %ecx
	jns	.L4374
	movl	%r14d, 4(%r12)
	jmp	.L4374
	.p2align 4,,10
	.p2align 3
.L4393:
	andl	$48, %ebx
	jne	.L4394
	movl	$64, %edx
	movq	$1, (%rdi)
	movw	%dx, 8(%rdi)
.L4371:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4395
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4394:
	.cfi_restore_state
	leaq	-68(%rbp), %r9
	movq	%rdx, %rcx
	leaq	-76(%rbp), %r8
	movq	%rsi, %rdx
	movq	%r9, %rsi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-76(%rbp), %esi
.L4370:
	testl	%esi, %esi
	je	.L4371
	testb	$-35, %sil
	je	.L4378
	testl	$1073741824, %esi
	je	.L4379
	andl	$-1073741825, %esi
.L4378:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L4371
	.p2align 4,,10
	.p2align 3
.L4379:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L4378
	.p2align 4,,10
	.p2align 3
.L4372:
	testl	%ecx, %ecx
	js	.L4396
.L4374:
	movq	%r13, %rsi
	movq	%r15, %rcx
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-76(%rbp), %esi
	jmp	.L4370
	.p2align 4,,10
	.p2align 3
.L4396:
	movl	(%r12), %esi
	movl	0(%r13), %eax
	movl	%esi, %edi
	subl	%ecx, %edi
	cmpl	%eax, %edi
	jle	.L4376
	movl	%esi, %ecx
	orl	$2048, -76(%rbp)
	subl	%eax, %ecx
	jne	.L4397
.L4377:
	movl	%esi, (%r12)
	movl	%edx, 4(%r12)
	jmp	.L4374
.L4376:
	subl	%r14d, %edx
.L4380:
	leaq	9(%r12), %rdi
	movq	%r8, -96(%rbp)
	movl	%ecx, -88(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	movl	-88(%rbp), %ecx
	addl	4(%r12), %ecx
	movq	-96(%rbp), %r8
	movl	%eax, %esi
	movl	%ecx, %edx
	jmp	.L4377
.L4397:
	subl	%esi, %eax
	movl	%eax, %edx
	jmp	.L4380
.L4395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2061:
	.size	uprv_decNumberAbs_67, .-uprv_decNumberAbs_67
	.p2align 4
	.globl	uprv_decNumberSquareRoot_67
	.type	uprv_decNumberSquareRoot_67, @function
uprv_decNumberSquareRoot_67:
.LFB2098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -520(%rbp)
	movq	%rsi, -544(%rbp)
	movq	%rdx, -560(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rsi), %eax
	movl	$0, -472(%rbp)
	movl	$0, -468(%rbp)
	movl	$0, -464(%rbp)
	testb	$112, %al
	je	.L4399
	testb	$64, %al
	je	.L4400
	testb	%al, %al
	jns	.L4401
.L4425:
	movl	$128, -468(%rbp)
	movl	$128, %esi
.L4402:
	movq	-520(%rbp), %rax
	movl	$32, %edx
	movq	$1, (%rax)
	movw	%dx, 8(%rax)
.L4529:
	movq	-560(%rbp), %rdi
	call	uprv_decContextSetStatus_67@PLT
.L4532:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4645
	movq	-520(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4399:
	.cfi_restore_state
	movq	%rsi, %rbx
	movl	4(%rsi), %esi
	movl	%esi, -488(%rbp)
	sarl	%esi
	cmpb	$0, 9(%rbx)
	movl	%esi, -616(%rbp)
	je	.L4646
.L4414:
	testb	%al, %al
	js	.L4425
	movq	-544(%rbp), %rax
	movl	$7, %ecx
	movslq	(%rax), %rdx
	movq	-560(%rbp), %rax
	movl	(%rax), %eax
	leal	11(%rdx), %edi
	addl	$1, %eax
	cmpl	$7, %eax
	cmovl	%ecx, %eax
	cmpl	%edx, %eax
	cmovl	%edx, %eax
	movl	%eax, -528(%rbp)
	addl	$2, %eax
	movl	%eax, %r15d
	cmpl	$49, %edx
	jg	.L4427
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdx), %edi
	addl	$11, %edi
.L4427:
	leaq	-336(%rbp), %rax
	movq	$0, -608(%rbp)
	movq	%rax, -488(%rbp)
	cmpl	$48, %edi
	jg	.L4647
.L4428:
	movl	-528(%rbp), %eax
	leal	13(%rax), %edi
	movslq	%r15d, %rax
	cmpl	$49, %eax
	jg	.L4431
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %edi
	addl	$11, %edi
.L4431:
	cmpl	$60, %edi
	jg	.L4648
	leaq	-224(%rbp), %r12
	leaq	-288(%rbp), %r14
	movq	$0, -584(%rbp)
	movq	$0, -592(%rbp)
.L4432:
	movq	-488(%rbp), %rsi
	movq	-544(%rbp), %rcx
	cmpq	%rcx, %rsi
	je	.L4435
	movl	(%rcx), %r13d
	movzwl	8(%rcx), %eax
	movl	4(%rcx), %ebx
	movl	%r13d, (%rsi)
	movw	%ax, 8(%rsi)
	cmpl	$1, %r13d
	jle	.L4436
	leaq	10(%rsi), %rdi
	leaq	9(%rcx), %rax
	movslq	%r13d, %rdx
	cmpl	$49, %r13d
	jg	.L4438
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
.L4438:
	movq	-544(%rbp), %rcx
	addq	%rdx, %rax
	leaq	10(%rcx), %rsi
	cmpq	%rsi, %rax
	jbe	.L4436
	subq	%rcx, %rax
	leaq	-10(%rax), %rdx
	call	memcpy@PLT
.L4436:
	movq	-488(%rbp), %rdx
	movl	%r13d, %eax
	addl	%r13d, %ebx
	movl	$64, %esi
	negl	%eax
	leaq	-448(%rbp), %r13
	movl	%ebx, -612(%rbp)
	movl	%eax, 4(%rdx)
	movq	%r13, %rdi
	call	uprv_decContextDefault_67@PLT
	andl	$1, %ebx
	movabsq	$-4294967290705032705, %rax
	movb	$0, -360(%rbp)
	movq	%rax, -444(%rbp)
	movl	-528(%rbp), %eax
	movl	$3, -368(%rbp)
	movl	%eax, -448(%rbp)
	movb	$0, 8(%r14)
	movl	$3, (%r14)
	jne	.L4439
	movl	$517, %ebx
	movl	$2049, %eax
	movl	$-3, -364(%rbp)
	movl	$-3, 4(%r14)
	movb	$9, -359(%rbp)
	movw	%bx, -358(%rbp)
	movb	$9, 9(%r14)
	movw	%ax, 10(%r14)
.L4440:
	movq	-488(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r14, %rdi
	leaq	-464(%rbp), %rbx
	movq	%rbx, %r8
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	leaq	-368(%rbp), %rax
	movq	%r14, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%rax, -552(%rbp)
	call	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj
	xorl	%r8d, %r8d
	movl	$1280, %r9d
	movabsq	$-4294967295, %rax
	movq	%rax, -368(%rbp)
	leaq	9(%r12), %rdx
	leaq	9(%r14), %rax
	leaq	-416(%rbp), %rcx
	movq	%rax, -600(%rbp)
	movl	$3, %eax
	movq	$1, -380(%rbp)
	movw	%r8w, -372(%rbp)
	movw	%r9w, -360(%rbp)
	movq	%rcx, -536(%rbp)
	movq	%rdx, -568(%rbp)
	jmp	.L4495
	.p2align 4,,10
	.p2align 3
.L4652:
	testb	$48, %al
	jne	.L4649
	testb	$64, %dl
	je	.L4444
	andl	$64, %esi
	je	.L4632
	cmpb	$0, -512(%rbp)
	js	.L4650
	.p2align 4,,10
	.p2align 3
.L4632:
	andl	$-128, %edx
	movq	$1, (%r12)
	orl	$64, %edx
	movb	$0, 9(%r12)
	movb	%dl, 8(%r12)
.L4443:
	movq	-552(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	movl	-448(%rbp), %eax
	cmpl	%r15d, %eax
	jge	.L4651
.L4495:
	leal	-2(%rax,%rax), %eax
	movq	%r14, %rdx
	movq	%rbx, %r9
	movl	$128, %r8d
	cmpl	%r15d, %eax
	movq	-488(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	cmovg	%r15d, %eax
	movl	%eax, -448(%rbp)
	call	_ZL11decDivideOpP9decNumberPKS_S2_P10decContexthPj
	movzbl	8(%r12), %edx
	movzbl	8(%r14), %esi
	movl	%edx, %eax
	xorl	%esi, %eax
	movb	%al, -512(%rbp)
	movl	%edx, %eax
	orl	%esi, %eax
	testb	$112, %al
	jne	.L4652
	movslq	(%r12), %rcx
	movzbl	9(%r12), %r11d
	movslq	(%r14), %r10
	movl	4(%r14), %eax
	cmpl	$1, %ecx
	je	.L4653
.L4447:
	movl	-448(%rbp), %edi
	movzbl	9(%r14), %r9d
	movl	%edi, -524(%rbp)
	testb	%r9b, %r9b
	jne	.L4456
	cmpl	$1, %r10d
	je	.L4654
.L4456:
	movl	4(%r12), %edi
	movl	%eax, %r8d
	subl	%edi, %r8d
	jne	.L4461
	cmpl	$1, %r10d
	jle	.L4533
	movq	%r14, -496(%rbp)
	movq	%r12, %r11
	movl	$1, %r9d
.L4462:
	movl	%r9d, %eax
	negl	%eax
	cmpb	$0, -512(%rbp)
	cmovs	%eax, %r9d
.L4476:
	cmpl	%r10d, %ecx
	cmovl	%r10, %rcx
	cmpl	%ecx, -524(%rbp)
	jle	.L4477
	cmpq	-496(%rbp), %r12
	jne	.L4546
	testl	%r8d, %r8d
	jle	.L4546
.L4477:
	leal	1(%rcx), %edi
	cmpl	$49, %ecx
	jg	.L4480
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %edi
	addl	$1, %edi
.L4480:
	movq	$0, -576(%rbp)
	leaq	-160(%rbp), %r10
	cmpl	$92, %edi
	jg	.L4655
.L4478:
	andl	$-128, %edx
	movb	%dl, 8(%r12)
	movl	4(%r11), %eax
	movl	%eax, 4(%r12)
	movq	-496(%rbp), %rax
	movslq	(%rax), %rcx
	cmpl	$49, %ecx
	jg	.L4481
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L4481:
	movq	-496(%rbp), %rdx
	movslq	(%r11), %rsi
	addq	$9, %rdx
	cmpl	$49, %esi
	jg	.L4482
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rsi), %esi
.L4482:
	subq	$8, %rsp
	leaq	9(%r11), %rdi
	movq	%r10, -496(%rbp)
	pushq	%r9
	movq	%r10, %r9
	call	_ZL13decUnitAddSubPKhiS0_iiPhi
	popq	%rsi
	movq	-496(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %ecx
	popq	%rdi
	js	.L4483
	movl	%eax, (%r12)
.L4484:
	movl	$0, -416(%rbp)
	cmpq	-568(%rbp), %r10
	je	.L4485
	cmpl	%ecx, -524(%rbp)
	jge	.L4486
	cmpl	$49, %ecx
	jg	.L4487
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L4487:
	movslq	%ecx, %rax
	leaq	-1(%r10,%rax), %rax
	cmpq	%rax, %r10
	ja	.L4488
.L4489:
	cmpb	$0, (%rax)
	jne	.L4488
	cmpl	$1, %ecx
	je	.L4488
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rax, %r10
	jbe	.L4489
	.p2align 4,,10
	.p2align 3
.L4488:
	movl	%ecx, (%r12)
.L4486:
	movq	-536(%rbp), %r8
	movq	%rbx, %r9
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	(%r12), %ecx
.L4485:
	cmpl	$49, %ecx
	jg	.L4490
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L4490:
	movq	-568(%rbp), %rdi
	movslq	%ecx, %rax
	leaq	-1(%rdi,%rax), %rax
	cmpq	%rdi, %rax
	jb	.L4491
	leaq	8(%r12), %rdx
.L4492:
	cmpb	$0, (%rax)
	jne	.L4491
	cmpl	$1, %ecx
	je	.L4491
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rdx, %rax
	jne	.L4492
	.p2align 4,,10
	.p2align 3
.L4491:
	movl	%ecx, (%r12)
	movq	-536(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	cmpb	$0, 9(%r12)
	jne	.L4493
	cmpl	$1, (%r12)
	je	.L4656
.L4493:
	movq	-576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4443
	call	uprv_free_67@PLT
	jmp	.L4443
	.p2align 4,,10
	.p2align 3
.L4646:
	cmpl	$1, (%rbx)
	jne	.L4414
	cmpq	%rdi, %rbx
	je	.L4416
	movq	-544(%rbp), %rdx
	movb	%al, 8(%rdi)
	movl	4(%rdx), %eax
	movl	%eax, 4(%rdi)
	movl	(%rdx), %eax
	movl	%eax, (%rdi)
	movzbl	9(%rdx), %eax
	movb	%al, 9(%rdi)
	movq	%rdx, %rax
	movl	(%rdx), %edx
	cmpl	$1, %edx
	jle	.L4416
	leaq	9(%rax), %rcx
	leaq	10(%rdi), %rdi
	movslq	%edx, %rax
	cmpl	$49, %edx
	jg	.L4418
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
.L4418:
	movq	-544(%rbp), %r10
	addq	%rax, %rcx
	leaq	10(%r10), %rdx
	cmpq	%rdx, %rcx
	jbe	.L4416
	movq	%rcx, %rax
	subq	%r10, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L4419
	leaq	25(%r10), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L4419
	movq	%rcx, %rax
	movq	-520(%rbp), %r9
	subq	%r10, %rax
	leaq	-10(%rax), %r8
	movl	$10, %eax
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	$10, %rsi
	.p2align 4,,10
	.p2align 3
.L4421:
	movdqu	(%r10,%rax), %xmm3
	movups	%xmm3, (%r9,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L4421
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	%rsi, %rdx
	leaq	(%rdi,%rsi), %rax
	cmpq	%r8, %rsi
	je	.L4416
	movzbl	(%rdx), %esi
	movb	%sil, (%rax)
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4416
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rax)
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4416
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rax)
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4416
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rax)
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4416
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rax)
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4416
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rax)
	leaq	6(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4416
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rax)
	leaq	7(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4416
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rax)
	leaq	8(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4416
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rax)
	leaq	9(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4416
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rax)
	leaq	10(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4416
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rax)
	leaq	11(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4416
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rax)
	leaq	12(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4416
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rax)
	leaq	13(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4416
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rax)
	leaq	14(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4416
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L4416:
	movq	-520(%rbp), %rdi
	movl	-616(%rbp), %eax
	leaq	-468(%rbp), %rcx
	leaq	-472(%rbp), %rdx
	movq	-560(%rbp), %rsi
	movl	%eax, 4(%rdi)
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-468(%rbp), %esi
	jmp	.L4413
	.p2align 4,,10
	.p2align 3
.L4400:
	movq	%rdx, %rcx
	leaq	-468(%rbp), %r8
	xorl	%edx, %edx
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-468(%rbp), %esi
.L4413:
	testl	%esi, %esi
	je	.L4532
	movl	%esi, %eax
	andl	$221, %eax
.L4429:
	testl	%eax, %eax
	je	.L4529
	testl	$1073741824, %esi
	je	.L4402
	andl	$-1073741825, %esi
	jmp	.L4529
	.p2align 4,,10
	.p2align 3
.L4401:
	movq	%rsi, %rbx
	movq	-520(%rbp), %rsi
	cmpq	%rsi, %rbx
	je	.L4532
	movb	%al, 8(%rsi)
	movl	4(%rbx), %eax
	movl	%eax, 4(%rsi)
	movl	(%rbx), %eax
	movl	%eax, (%rsi)
	movzbl	9(%rbx), %eax
	movb	%al, 9(%rsi)
	movl	(%rbx), %edx
	cmpl	$1, %edx
	jle	.L4532
	leaq	10(%rsi), %rdi
	leaq	9(%rbx), %rcx
	movslq	%edx, %rax
	cmpl	$49, %edx
	jle	.L4657
.L4405:
	movq	-544(%rbp), %r10
	addq	%rax, %rcx
	leaq	10(%r10), %rdx
	cmpq	%rdx, %rcx
	jbe	.L4532
	movq	%rcx, %rax
	subq	%r10, %rax
	subq	$11, %rax
	cmpq	$14, %rax
	jbe	.L4407
	leaq	25(%r10), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L4407
	movq	%rcx, %rax
	movq	-520(%rbp), %r9
	subq	%r10, %rax
	leaq	-10(%rax), %r8
	movl	$10, %eax
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	$10, %rsi
	.p2align 4,,10
	.p2align 3
.L4409:
	movdqu	(%r10,%rax), %xmm0
	movups	%xmm0, (%r9,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L4409
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	%rsi, %rdx
	leaq	(%rdi,%rsi), %rax
	cmpq	%rsi, %r8
	je	.L4637
	movzbl	(%rdx), %esi
	movb	%sil, (%rax)
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4637
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rax)
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4637
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rax)
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4637
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rax)
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4637
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rax)
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4637
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rax)
	leaq	6(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4637
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rax)
	leaq	7(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4637
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rax)
	leaq	8(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4637
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rax)
	leaq	9(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4637
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rax)
	leaq	10(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4637
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rax)
	leaq	11(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4637
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rax)
	leaq	12(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4637
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rax)
	leaq	13(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4637
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rax)
	leaq	14(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L4637
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
	jmp	.L4637
	.p2align 4,,10
	.p2align 3
.L4648:
	movslq	%edi, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%r12, %rdi
	movq	%rax, -592(%rbp)
	movq	%rax, %rbx
	call	uprv_malloc_67@PLT
	movq	%rax, -584(%rbp)
	testq	%rbx, %rbx
	je	.L4550
	testq	%rax, %rax
	je	.L4550
	movq	-592(%rbp), %r14
	movq	%rax, %r12
	jmp	.L4432
	.p2align 4,,10
	.p2align 3
.L4647:
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -608(%rbp)
	testq	%rax, %rax
	je	.L4658
	movq	%rax, -488(%rbp)
	jmp	.L4428
	.p2align 4,,10
	.p2align 3
.L4654:
	testb	$112, %sil
	je	.L4659
	movl	4(%r12), %edi
	movl	%eax, %r8d
	subl	%edi, %r8d
	je	.L4533
	.p2align 4,,10
	.p2align 3
.L4461:
	movq	%r14, -496(%rbp)
	movq	%r12, %r11
	testl	%r8d, %r8d
	jns	.L4470
	subl	%eax, %edi
	movq	%r12, -496(%rbp)
	movl	%r10d, %eax
	movl	%ecx, %r10d
	movslq	%eax, %rcx
	movl	%edx, %eax
	movl	%edi, %r8d
	movl	%esi, %edx
	movq	%r14, %r11
	movl	%eax, %esi
.L4470:
	movl	-524(%rbp), %r9d
	leal	(%r8,%r10), %eax
	leal	1(%r9,%rcx), %edi
	cmpl	%edi, %eax
	jle	.L4471
	movsbl	-512(%rbp), %eax
	movq	-496(%rbp), %rdi
	movb	%sil, 8(%r12)
	subl	%r10d, %r9d
	movq	-536(%rbp), %r8
	movl	%r9d, -524(%rbp)
	movq	%r13, %rsi
	movq	%rbx, %r9
	sarl	$31, %eax
	leaq	9(%rdi), %rdx
	orl	$1, %eax
	movl	%eax, -416(%rbp)
	movl	4(%rdi), %eax
	movl	%eax, 4(%r12)
	movl	(%rdi), %ecx
	movq	%r12, %rdi
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	-524(%rbp), %r10d
	testl	%r10d, %r10d
	jle	.L4474
	movl	(%r12), %esi
	movq	-568(%rbp), %rdi
	movl	%r10d, %edx
	movl	%r10d, -512(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	movl	-512(%rbp), %r10d
	subl	%r10d, 4(%r12)
	movl	%eax, (%r12)
.L4474:
	movq	-536(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	jmp	.L4443
	.p2align 4,,10
	.p2align 3
.L4444:
	movl	%esi, %edx
	jmp	.L4632
	.p2align 4,,10
	.p2align 3
.L4649:
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	jmp	.L4443
	.p2align 4,,10
	.p2align 3
.L4483:
	negl	%ecx
	addb	$-128, 8(%r12)
	movl	%ecx, (%r12)
	jmp	.L4484
	.p2align 4,,10
	.p2align 3
.L4651:
	movl	-612(%rbp), %edi
	movq	-560(%rbp), %rdx
	leaq	-460(%rbp), %r9
	leaq	-472(%rbp), %r8
	movl	(%r14), %ecx
	movq	-536(%rbp), %rsi
	movl	$0, -460(%rbp)
	movq	16(%rdx), %rax
	movdqu	(%rdx), %xmm1
	movl	$0, -472(%rbp)
	movq	%rax, -400(%rbp)
	movl	24(%rdx), %eax
	movq	-600(%rbp), %rdx
	movaps	%xmm1, -512(%rbp)
	movl	%eax, -392(%rbp)
	movl	%edi, %eax
	shrl	$31, %eax
	movaps	%xmm1, -416(%rbp)
	addl	%edi, %eax
	movq	%r14, %rdi
	movq	%r8, -512(%rbp)
	movl	$3, -404(%rbp)
	sarl	%eax
	addl	%eax, 4(%r14)
	movl	%eax, %r15d
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movq	-512(%rbp), %r8
	movq	-536(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r9, %rcx
	movq	%r8, %rdx
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-460(%rbp), %eax
	testb	$2, %ah
	jne	.L4644
	subl	%r15d, 4(%r14)
	movl	(%r14), %ecx
	andl	$-2081, %eax
	movq	%rbx, %r9
	orl	%eax, -468(%rbp)
	movq	-552(%rbp), %rdx
	movl	%r15d, %eax
	movq	%r14, %rsi
	notl	%ecx
	negl	%eax
	movl	$128, %r8d
	movq	%r12, %rdi
	movl	%ecx, -364(%rbp)
	movq	%r13, %rcx
	subl	$1, -448(%rbp)
	movl	%eax, -512(%rbp)
	call	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	$1, -436(%rbp)
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	movq	%rbx, %r9
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	-488(%rbp), %rsi
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZL12decCompareOpP9decNumberPKS_S2_P10decContexthPj
	cmpb	$0, 8(%r12)
	js	.L4660
	movq	-552(%rbp), %rdx
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	$5, -436(%rbp)
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	movq	%rbx, %r9
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	-488(%rbp), %rdx
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZL12decCompareOpP9decNumberPKS_S2_P10decContexthPj
	cmpb	$0, 8(%r12)
	js	.L4661
.L4500:
	movl	4(%r14), %eax
	movzbl	8(%r14), %ebx
	addl	%r15d, %eax
	movl	%eax, 4(%r14)
	cmpq	%r12, %r14
	je	.L4502
	movl	%eax, 4(%r12)
	movl	(%r14), %edx
	movzwl	8(%r14), %eax
	movl	%edx, (%r12)
	movw	%ax, 8(%r12)
	cmpl	$1, %edx
	jle	.L4502
	leaq	10(%r12), %rdi
	movslq	%edx, %rax
	cmpl	$49, %edx
	jle	.L4662
.L4504:
	addq	-600(%rbp), %rax
	leaq	10(%r14), %rsi
	cmpq	%rsi, %rax
	jbe	.L4502
	subq	%r14, %rax
	leaq	-10(%rax), %rdx
	call	memcpy@PLT
.L4502:
	movl	$0, -456(%rbp)
	andl	$112, %ebx
	jne	.L4505
	movzbl	9(%r12), %eax
	testb	$1, %al
	jne	.L4505
	testb	%al, %al
	jne	.L4506
	cmpl	$1, (%r12)
	jne	.L4506
	movl	$0, 4(%r12)
	.p2align 4,,10
	.p2align 3
.L4507:
	leaq	-452(%rbp), %rbx
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r12, %rsi
	movl	$0, -452(%rbp)
	movq	%rbx, %r8
	movq	%r12, %rdi
	call	_ZL13decMultiplyOpP9decNumberPKS_S2_P10decContextPj
	testb	$2, -451(%rbp)
	jne	.L4511
	movq	-544(%rbp), %rdx
	movq	%rbx, %r9
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	-552(%rbp), %rdi
	movl	$1, %r8d
	call	_ZL12decCompareOpP9decNumberPKS_S2_P10decContexthPj
	cmpl	$1, -368(%rbp)
	jne	.L4511
	movzbl	-360(%rbp), %eax
	andl	$112, %eax
	orb	-359(%rbp), %al
	jne	.L4511
	movl	4(%r14), %edx
	movl	-616(%rbp), %ebx
	subl	%edx, %ebx
	js	.L4663
	movq	-560(%rbp), %rcx
	movl	4(%rcx), %eax
	subl	(%rcx), %eax
	addl	$1, %eax
	cmpl	%eax, -616(%rbp)
	jle	.L4514
	cmpb	$0, 24(%rcx)
	je	.L4514
	orl	$1024, -468(%rbp)
	subl	%edx, %eax
	movl	%eax, %ebx
.L4514:
	movl	-456(%rbp), %eax
	cmpl	%ebx, %eax
	jge	.L4515
	orl	$1024, -468(%rbp)
	movl	%eax, %ebx
.L4515:
	testl	%ebx, %ebx
	jle	.L4635
	movslq	(%r14), %rsi
	cmpl	$49, %esi
	jg	.L4517
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rsi), %esi
.L4517:
	cmpl	%esi, %ebx
	je	.L4664
	movq	-600(%rbp), %rdi
	movl	%ebx, %edx
	call	_ZL15decShiftToLeastPhii.part.0
.L4519:
	addl	%ebx, 4(%r14)
	subl	%ebx, (%r14)
.L4635:
	movl	-468(%rbp), %eax
	jmp	.L4508
	.p2align 4,,10
	.p2align 3
.L4506:
	movq	-560(%rbp), %rsi
	leaq	-456(%rbp), %r8
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZL7decTrimP9decNumberP10decContexthhPi.part.0
.L4505:
	movl	(%r12), %eax
	leal	-1(%rax,%rax), %eax
	cmpl	-528(%rbp), %eax
	jle	.L4507
.L4511:
	movl	-468(%rbp), %eax
	orl	$2080, %eax
	movl	%eax, -468(%rbp)
.L4508:
	testb	$32, %ah
	je	.L4521
	movq	-544(%rbp), %rbx
	movl	(%rbx), %ecx
	addl	4(%rbx), %ecx
	movq	-560(%rbp), %rbx
	movl	8(%rbx), %ebx
	leal	(%rbx,%rbx), %edx
	movl	%ebx, -488(%rbp)
	cmpl	%edx, %ecx
	jle	.L4522
	andb	$-49, %ah
	movl	%eax, -468(%rbp)
.L4522:
	testb	$32, %al
	je	.L4665
	.p2align 4,,10
	.p2align 3
.L4521:
	movq	-520(%rbp), %rbx
	cmpq	%rbx, %r14
	je	.L4434
	movq	(%r14), %rax
	movl	(%r14), %edx
	movq	%rax, (%rbx)
	movzwl	8(%r14), %eax
	movw	%ax, 8(%rbx)
	cmpl	$1, %edx
	jle	.L4434
	leaq	10(%rbx), %rdi
	movslq	%edx, %rax
	cmpl	$49, %edx
	jg	.L4525
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
.L4525:
	addq	-600(%rbp), %rax
	leaq	10(%r14), %rsi
	cmpq	%rsi, %rax
	jbe	.L4434
	subq	%r14, %rax
	leaq	-10(%rax), %rdx
	call	memcpy@PLT
.L4434:
	movq	-608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4526
	call	uprv_free_67@PLT
.L4526:
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4527
	call	uprv_free_67@PLT
.L4527:
	cmpq	$0, -584(%rbp)
	je	.L4637
	movq	-584(%rbp), %rdi
	call	uprv_free_67@PLT
.L4637:
	movl	-468(%rbp), %esi
	jmp	.L4413
	.p2align 4,,10
	.p2align 3
.L4471:
	movl	$1, %r9d
	cmpl	$48, %r8d
	jg	.L4475
	leal	1(%r8), %esi
	leaq	_ZL8d2utable(%rip), %rdi
	movslq	%esi, %rsi
	movzbl	(%rdi,%rsi), %esi
	leaq	_ZL9DECPOWERS(%rip), %rdi
	subl	$1, %esi
	subl	%esi, %r8d
	movslq	%r8d, %r8
	movl	(%rdi,%r8,4), %r9d
	movl	%esi, %r8d
.L4475:
	movslq	%eax, %r10
	jmp	.L4462
	.p2align 4,,10
	.p2align 3
.L4533:
	cmpl	%eax, -440(%rbp)
	jg	.L4542
	movl	-444(%rbp), %esi
	movl	-524(%rbp), %edi
	subl	%edi, %esi
	addl	$1, %esi
	cmpl	%eax, %esi
	jl	.L4542
	cmpl	%r10d, %edi
	jge	.L4666
.L4542:
	movq	%r14, -496(%rbp)
	movq	%r12, %r11
	movl	$1, %r9d
	xorl	%r8d, %r8d
	jmp	.L4462
	.p2align 4,,10
	.p2align 3
.L4653:
	movl	%edx, %edi
	andl	$112, %edi
	orb	%r11b, %dil
	jne	.L4447
	movl	4(%r12), %r11d
	movb	%sil, 8(%r12)
	movl	%r10d, %ecx
	movq	%rbx, %r9
	movl	%eax, 4(%r12)
	movq	-600(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-536(%rbp), %r8
	movl	%r11d, -496(%rbp)
	movl	$0, -416(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	-496(%rbp), %r11d
	movl	4(%r12), %edx
	movl	%r11d, %ecx
	subl	%edx, %ecx
	cmpb	$0, 9(%r12)
	jne	.L4448
	cmpl	$1, (%r12)
	jne	.L4448
	testb	$112, 8(%r12)
	jne	.L4448
	testl	%ecx, %ecx
	jns	.L4449
	movl	%r11d, 4(%r12)
.L4449:
	cmpb	$0, -512(%rbp)
	jns	.L4474
	cmpl	$6, -436(%rbp)
	je	.L4452
	movb	$0, 8(%r12)
	jmp	.L4474
	.p2align 4,,10
	.p2align 3
.L4448:
	testl	%ecx, %ecx
	jns	.L4474
	movl	(%r12), %esi
	movl	-448(%rbp), %eax
	movl	%esi, %edi
	subl	%ecx, %edi
	cmpl	%eax, %edi
	jle	.L4454
	orl	$2048, -464(%rbp)
	movl	%esi, %ecx
	subl	%eax, %ecx
	je	.L4460
	subl	%esi, %eax
	movl	%eax, %edx
.L4530:
	movq	-568(%rbp), %rdi
	movl	%ecx, -512(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	movl	-512(%rbp), %ecx
	movl	4(%r12), %edx
	movl	%eax, %esi
	addl	%ecx, %edx
	jmp	.L4460
	.p2align 4,,10
	.p2align 3
.L4655:
	movslq	%edi, %rdi
	movl	%r8d, -632(%rbp)
	movb	%dl, -625(%rbp)
	movq	%r11, -624(%rbp)
	movl	%r9d, -576(%rbp)
	call	uprv_malloc_67@PLT
	movl	-576(%rbp), %r9d
	movq	-624(%rbp), %r11
	testq	%rax, %rax
	movzbl	-625(%rbp), %edx
	movl	-632(%rbp), %r8d
	movq	%rax, %r10
	je	.L4667
	movq	%rax, -576(%rbp)
	jmp	.L4478
	.p2align 4,,10
	.p2align 3
.L4546:
	movq	$0, -576(%rbp)
	movq	-568(%rbp), %r10
	jmp	.L4478
	.p2align 4,,10
	.p2align 3
.L4665:
	andb	$-33, %ah
	.p2align 4,,10
	.p2align 3
.L4644:
	movl	%eax, -468(%rbp)
	jmp	.L4521
	.p2align 4,,10
	.p2align 3
.L4439:
	movq	-488(%rbp), %rax
	movl	$2049, %r10d
	movl	$517, %r11d
	addl	$1, -612(%rbp)
	movl	$-4, -364(%rbp)
	subl	$1, 4(%rax)
	movl	$-2, 4(%r14)
	movb	$9, -359(%rbp)
	movw	%r10w, -358(%rbp)
	movb	$9, 9(%r14)
	movw	%r11w, 10(%r14)
	jmp	.L4440
	.p2align 4,,10
	.p2align 3
.L4656:
	movzbl	8(%r12), %eax
	testb	$112, %al
	jne	.L4493
	cmpb	$0, -512(%rbp)
	jns	.L4493
	testb	$32, -464(%rbp)
	jne	.L4493
	cmpl	$6, -436(%rbp)
	je	.L4668
	andl	$127, %eax
	movb	%al, 8(%r12)
	jmp	.L4493
	.p2align 4,,10
	.p2align 3
.L4650:
	orl	$128, -464(%rbp)
	jmp	.L4443
	.p2align 4,,10
	.p2align 3
.L4659:
	movq	-536(%rbp), %r8
	movq	%rbx, %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-568(%rbp), %rdx
	movl	%eax, -512(%rbp)
	movl	$0, -416(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	-512(%rbp), %eax
	movl	4(%r12), %edx
	movl	%eax, %r8d
	subl	%edx, %r8d
	jns	.L4474
	movl	(%r12), %esi
	movl	-448(%rbp), %ecx
	movl	%esi, %edi
	subl	%r8d, %edi
	cmpl	%ecx, %edi
	jle	.L4459
	orl	$2048, -464(%rbp)
	movl	%esi, %r8d
	subl	%ecx, %r8d
	jne	.L4669
	.p2align 4,,10
	.p2align 3
.L4460:
	movl	%esi, (%r12)
	movl	%edx, 4(%r12)
	jmp	.L4474
	.p2align 4,,10
	.p2align 3
.L4660:
	addl	$1, -364(%rbp)
	movq	%rbx, %r9
	movl	$128, %r8d
	movb	$1, -359(%rbp)
.L4633:
	movq	-552(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movq	-536(%rbp), %rcx
	leaq	-380(%rbp), %rsi
	movq	%r14, %rdi
	movl	-512(%rbp), %eax
	addl	%eax, -408(%rbp)
	addl	%eax, -412(%rbp)
	call	_ZL8decAddOpP9decNumberPKS_S2_P10decContexthPj
	jmp	.L4500
	.p2align 4,,10
	.p2align 3
.L4657:
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	jmp	.L4405
	.p2align 4,,10
	.p2align 3
.L4662:
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	jmp	.L4504
	.p2align 4,,10
	.p2align 3
.L4666:
	cmpl	%ecx, %edi
	jl	.L4542
	cmpb	$0, -512(%rbp)
	movzbl	%r11b, %eax
	movzbl	%r9b, %esi
	js	.L4463
	addl	%esi, %eax
	cmpl	$9, %eax
	jg	.L4464
	testl	%ecx, %ecx
	jle	.L4670
.L4465:
	addl	%r11d, %r9d
	movb	%r9b, 9(%r12)
	jmp	.L4443
.L4407:
	movq	-544(%rbp), %rdi
	movq	%rcx, %rax
	movl	$10, %edx
	movq	-520(%rbp), %rsi
	subq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L4412:
	movzbl	(%rdi,%rdx), %ecx
	movb	%cl, (%rsi,%rdx)
	addq	$1, %rdx
	cmpq	%rax, %rdx
	jne	.L4412
	jmp	.L4637
	.p2align 4,,10
	.p2align 3
.L4435:
	movl	4(%rsi), %ebx
	movl	(%rsi), %r13d
	jmp	.L4436
.L4661:
	addl	$1, -364(%rbp)
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	movb	$1, -359(%rbp)
	jmp	.L4633
.L4459:
	subl	%eax, %edx
.L4531:
	movq	-568(%rbp), %rdi
	movl	%r8d, -512(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	movl	-512(%rbp), %r8d
	movl	4(%r12), %edx
	movl	%eax, %esi
	addl	%r8d, %edx
	jmp	.L4460
.L4454:
	subl	%r11d, %edx
	jmp	.L4530
.L4463:
	subl	%esi, %eax
	testl	%eax, %eax
	jle	.L4543
	subl	%r9d, %r11d
	movb	%r11b, 9(%r12)
	cmpl	$49, %ecx
	jg	.L4467
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L4467:
	movq	-568(%rbp), %rdx
	movslq	%ecx, %rax
	leaq	-1(%rdx,%rax), %rax
	cmpq	%rax, %rdx
	ja	.L4468
	leaq	8(%r12), %rdx
.L4469:
	cmpb	$0, (%rax)
	jne	.L4468
	cmpl	$1, %ecx
	je	.L4468
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rdx, %rax
	jne	.L4469
.L4468:
	movl	%ecx, (%r12)
	jmp	.L4443
.L4670:
	movslq	%ecx, %rsi
	leaq	_ZL9DECPOWERS(%rip), %rdi
	cmpl	(%rdi,%rsi,4), %eax
	jl	.L4465
.L4464:
	cmpl	%r10d, %ecx
	movq	%r14, -496(%rbp)
	movq	%r12, %r11
	cmovl	%r10, %rcx
	cmpl	%ecx, -524(%rbp)
	jle	.L4549
	movq	-568(%rbp), %r10
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	$0, -576(%rbp)
	jmp	.L4478
.L4419:
	movq	-544(%rbp), %rdi
	movq	%rcx, %rax
	movl	$10, %edx
	movq	-520(%rbp), %rsi
	subq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L4424:
	movzbl	(%rdi,%rdx), %ecx
	movb	%cl, (%rsi,%rdx)
	addq	$1, %rdx
	cmpq	%rax, %rdx
	jne	.L4424
	jmp	.L4416
.L4668:
	orl	$-128, %eax
	movb	%al, 8(%r12)
	jmp	.L4493
.L4669:
	movl	%ecx, %edx
	subl	%esi, %edx
	jmp	.L4531
.L4543:
	movq	%r14, -496(%rbp)
	movq	%r12, %r11
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	jmp	.L4476
.L4549:
	xorl	%r8d, %r8d
	movl	$1, %r9d
	jmp	.L4477
.L4452:
	movb	$-128, 8(%r12)
	jmp	.L4474
.L4663:
	movl	-468(%rbp), %eax
	orb	$8, %ah
	movl	%eax, -468(%rbp)
	jmp	.L4508
.L4645:
	call	__stack_chk_fail@PLT
.L4667:
	orl	$16, -464(%rbp)
	jmp	.L4443
.L4664:
	movb	$0, 9(%r14)
	jmp	.L4519
.L4658:
	movl	-468(%rbp), %eax
	movl	%eax, %esi
	andl	$221, %eax
	orl	$16, %esi
	orl	$16, %eax
	movl	%esi, -468(%rbp)
	jmp	.L4429
.L4550:
	orl	$16, -468(%rbp)
	jmp	.L4434
	.cfi_endproc
.LFE2098:
	.size	uprv_decNumberSquareRoot_67, .-uprv_decNumberSquareRoot_67
	.p2align 4
	.globl	uprv_decNumberAdd_67
	.type	uprv_decNumberAdd_67, @function
uprv_decNumberAdd_67:
.LFB2062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movzbl	8(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rdx), %eax
	movl	$0, -168(%rbp)
	movl	%ecx, %ebx
	movl	%ecx, %edi
	xorl	%eax, %ebx
	orl	%eax, %edi
	movb	%bl, -184(%rbp)
	testb	$112, %dil
	je	.L4672
	andl	$48, %edi
	jne	.L4813
	testb	$64, %cl
	jne	.L4814
	andl	$-128, %eax
	movl	%eax, %ecx
.L4678:
	orl	$64, %ecx
	movq	$1, (%r12)
	movb	$0, 9(%r12)
	movb	%cl, 8(%r12)
.L4679:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4815
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4672:
	.cfi_restore_state
	movzbl	9(%rsi), %r14d
	testb	%r14b, %r14b
	je	.L4816
.L4680:
	movzbl	9(%rdx), %r15d
	movl	0(%r13), %r11d
	movslq	(%rdx), %rdi
	movl	4(%rdx), %ebx
	testb	%r15b, %r15b
	jne	.L4689
	cmpl	$1, %edi
	je	.L4817
.L4689:
	movl	4(%rsi), %r9d
	movl	%ebx, %r8d
	movslq	(%rsi), %r10
	subl	%r9d, %r8d
	jne	.L4694
	movl	$1, %r9d
	cmpl	$1, %edi
	jle	.L4735
.L4695:
	cmpb	$0, -184(%rbp)
	js	.L4700
.L4711:
	cmpl	%r10d, %edi
	leaq	9(%r12), %rbx
	cmovl	%r10, %rdi
	cmpl	%edi, %r11d
	jle	.L4712
	cmpq	%rdx, %r12
	jne	.L4743
	testl	%r8d, %r8d
	jle	.L4743
.L4712:
	leal	1(%rdi), %eax
	cmpl	$49, %edi
	jg	.L4715
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdi), %eax
	addl	$1, %eax
.L4715:
	xorl	%r14d, %r14d
	leaq	-160(%rbp), %r15
	cmpl	$92, %eax
	jg	.L4818
.L4713:
	andl	$-128, %ecx
	movb	%cl, 8(%r12)
	movl	4(%rsi), %eax
	movl	%eax, 4(%r12)
	movslq	(%rdx), %rcx
	cmpl	$49, %ecx
	jg	.L4717
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L4717:
	movslq	(%rsi), %r10
	addq	$9, %rdx
	cmpl	$49, %r10d
	jg	.L4718
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%r10), %r10d
.L4718:
	subq	$8, %rsp
	leaq	9(%rsi), %rdi
	movl	%r10d, %esi
	movl	%r11d, -192(%rbp)
	pushq	%r9
	movq	%r15, %r9
	call	_ZL13decUnitAddSubPKhiS0_iiPhi
	popq	%rdx
	movl	-192(%rbp), %r11d
	testl	%eax, %eax
	movl	%eax, %ecx
	popq	%rsi
	js	.L4719
	movl	%eax, (%r12)
.L4720:
	leaq	-168(%rbp), %r9
	leaq	-164(%rbp), %r8
	movl	$0, -164(%rbp)
	cmpq	%rbx, %r15
	je	.L4721
	cmpl	%ecx, %r11d
	jge	.L4722
	cmpl	$49, %ecx
	jg	.L4723
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L4723:
	movslq	%ecx, %rax
	leaq	-1(%r15,%rax), %rax
	cmpq	%rax, %r15
	ja	.L4724
.L4725:
	cmpb	$0, (%rax)
	jne	.L4724
	cmpl	$1, %ecx
	je	.L4724
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rax, %r15
	jbe	.L4725
	.p2align 4,,10
	.p2align 3
.L4724:
	movl	%ecx, (%r12)
.L4722:
	leaq	-168(%rbp), %r9
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-164(%rbp), %r8
	movq	%r9, -200(%rbp)
	movq	%r8, -192(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	(%r12), %ecx
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %r8
.L4721:
	cmpl	$49, %ecx
	jg	.L4726
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L4726:
	movslq	%ecx, %rax
	leaq	-1(%rbx,%rax), %rax
	cmpq	%rbx, %rax
	jb	.L4727
	leaq	8(%r12), %rdx
.L4728:
	cmpb	$0, (%rax)
	jne	.L4727
	cmpl	$1, %ecx
	je	.L4727
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rdx, %rax
	jne	.L4728
	.p2align 4,,10
	.p2align 3
.L4727:
	movl	%ecx, (%r12)
	movq	%r8, %rdx
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	cmpb	$0, 9(%r12)
	jne	.L4729
	cmpl	$1, (%r12)
	je	.L4819
.L4729:
	testq	%r14, %r14
	je	.L4808
	movq	%r14, %rdi
	call	uprv_free_67@PLT
.L4808:
	movl	-168(%rbp), %esi
	jmp	.L4674
	.p2align 4,,10
	.p2align 3
.L4816:
	cmpl	$1, (%rsi)
	jne	.L4680
	testb	$112, %cl
	jne	.L4680
	movl	4(%rsi), %ebx
	movb	%al, 8(%r12)
	leaq	9(%rdx), %r10
	leaq	-168(%rbp), %r9
	movl	4(%rdx), %eax
	leaq	-164(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -200(%rbp)
	movl	%ebx, %r14d
	movl	%eax, 4(%r12)
	movl	(%rdx), %ecx
	movq	%r10, %rdx
	movq	%r8, -192(%rbp)
	movl	$0, -164(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	4(%r12), %edx
	movq	-192(%rbp), %r8
	movq	-200(%rbp), %r9
	subl	%edx, %r14d
	cmpb	$0, 9(%r12)
	jne	.L4681
	cmpl	$1, (%r12)
	je	.L4820
.L4681:
	testl	%r14d, %r14d
	jns	.L4709
.L4812:
	movl	(%r12), %esi
	movl	0(%r13), %eax
	movl	%esi, %ecx
	subl	%r14d, %ecx
	cmpl	%eax, %ecx
	jle	.L4692
	orl	$2048, -168(%rbp)
	movl	%esi, %r14d
	subl	%eax, %r14d
	jne	.L4821
.L4693:
	movl	%esi, (%r12)
	movl	%edx, 4(%r12)
	.p2align 4,,10
	.p2align 3
.L4709:
	movq	%r13, %rsi
	movq	%r9, %rcx
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-168(%rbp), %esi
	jmp	.L4674
	.p2align 4,,10
	.p2align 3
.L4817:
	testb	$112, %al
	je	.L4822
	movl	4(%rsi), %r9d
	movl	%ebx, %r8d
	movslq	(%rsi), %r10
	subl	%r9d, %r8d
	je	.L4735
	.p2align 4,,10
	.p2align 3
.L4694:
	testl	%r8d, %r8d
	js	.L4823
.L4705:
	leal	(%r8,%rdi), %ebx
	leal	1(%r11,%r10), %r9d
	cmpl	%r9d, %ebx
	jg	.L4824
	movl	$1, %r9d
	cmpl	$48, %r8d
	jg	.L4710
	leal	1(%r8), %eax
	leaq	_ZL8d2utable(%rip), %rdi
	cltq
	movzbl	(%rdi,%rax), %eax
	leaq	_ZL9DECPOWERS(%rip), %rdi
	subl	$1, %eax
	subl	%eax, %r8d
	movslq	%r8d, %r8
	movl	(%rdi,%r8,4), %r9d
	movl	%eax, %r8d
.L4710:
	cmpb	$0, -184(%rbp)
	movslq	%ebx, %rdi
	jns	.L4711
.L4700:
	negl	%r9d
	jmp	.L4711
	.p2align 4,,10
	.p2align 3
.L4814:
	testb	$64, %al
	je	.L4676
	cmpb	$0, -184(%rbp)
	js	.L4825
.L4676:
	andl	$-128, %ecx
	jmp	.L4678
	.p2align 4,,10
	.p2align 3
.L4813:
	leaq	-168(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-168(%rbp), %esi
.L4674:
	testl	%esi, %esi
	je	.L4679
.L4716:
	testb	$-35, %sil
	jne	.L4826
.L4732:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L4679
	.p2align 4,,10
	.p2align 3
.L4824:
	movb	%al, 8(%r12)
	movl	4(%rdx), %eax
	movl	%r11d, %ebx
	leaq	9(%rdx), %r10
	movsbl	-184(%rbp), %ecx
	leaq	-168(%rbp), %r9
	subl	%edi, %ebx
	movq	%r13, %rsi
	movl	%eax, 4(%r12)
	leaq	-164(%rbp), %r8
	movq	%r12, %rdi
	sarl	$31, %ecx
	movq	%r9, -192(%rbp)
	orl	$1, %ecx
	movq	%r8, -184(%rbp)
	movl	%ecx, -164(%rbp)
	movl	(%rdx), %ecx
	movq	%r10, %rdx
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	testl	%ebx, %ebx
	movq	-184(%rbp), %r8
	movq	-192(%rbp), %r9
	jle	.L4709
	movl	(%r12), %esi
	leaq	9(%r12), %rdi
	movl	%ebx, %edx
	movq	%r8, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	subl	%ebx, 4(%r12)
	movq	-192(%rbp), %r8
	movl	%eax, (%r12)
	movq	-184(%rbp), %r9
	jmp	.L4709
	.p2align 4,,10
	.p2align 3
.L4823:
	subl	%ebx, %r9d
	movl	%r9d, %r8d
	movl	%edi, %r9d
	movl	%r10d, %edi
	movslq	%r9d, %r10
	movl	%ecx, %r9d
	movl	%eax, %ecx
	movl	%r9d, %eax
	movq	%rsi, %r9
	movq	%rdx, %rsi
	movq	%r9, %rdx
	jmp	.L4705
	.p2align 4,,10
	.p2align 3
.L4735:
	cmpl	%ebx, 8(%r13)
	jg	.L4740
	movl	4(%r13), %eax
	subl	%r11d, %eax
	addl	$1, %eax
	cmpl	%ebx, %eax
	jl	.L4740
	cmpl	%edi, %r11d
	jl	.L4740
	cmpl	%r10d, %r11d
	jl	.L4740
	cmpb	$0, -184(%rbp)
	movzbl	%r14b, %r8d
	movzbl	%r15b, %eax
	js	.L4696
	addl	%r8d, %eax
	cmpl	$9, %eax
	jg	.L4697
	testl	%r10d, %r10d
	jle	.L4827
.L4698:
	cmpq	%rsi, %r12
	je	.L4699
	movq	%r12, %rdi
	call	uprv_decNumberCopy_67
.L4699:
	addl	%r15d, %r14d
	movl	-168(%rbp), %esi
	movb	%r14b, 9(%r12)
	jmp	.L4674
	.p2align 4,,10
	.p2align 3
.L4740:
	movl	$1, %r9d
	xorl	%r8d, %r8d
	jmp	.L4695
	.p2align 4,,10
	.p2align 3
.L4719:
	negl	%ecx
	addb	$-128, 8(%r12)
	movl	%ecx, (%r12)
	jmp	.L4720
	.p2align 4,,10
	.p2align 3
.L4826:
	testl	$1073741824, %esi
	je	.L4677
	andl	$-1073741825, %esi
	jmp	.L4732
	.p2align 4,,10
	.p2align 3
.L4825:
	movl	$128, -168(%rbp)
	movl	$128, %esi
.L4677:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L4732
	.p2align 4,,10
	.p2align 3
.L4818:
	movslq	%eax, %rdi
	movq	%rdx, -224(%rbp)
	movq	%rsi, -216(%rbp)
	movl	%r9d, -208(%rbp)
	movb	%cl, -201(%rbp)
	movl	%r8d, -200(%rbp)
	movl	%r11d, -192(%rbp)
	call	uprv_malloc_67@PLT
	movl	-192(%rbp), %r11d
	movl	-200(%rbp), %r8d
	testq	%rax, %rax
	movzbl	-201(%rbp), %ecx
	movl	-208(%rbp), %r9d
	movq	%rax, %r15
	movq	-216(%rbp), %rsi
	movq	-224(%rbp), %rdx
	je	.L4828
	movq	%rax, %r14
	jmp	.L4713
	.p2align 4,,10
	.p2align 3
.L4743:
	movq	%rbx, %r15
	xorl	%r14d, %r14d
	jmp	.L4713
	.p2align 4,,10
	.p2align 3
.L4819:
	movzbl	8(%r12), %eax
	testb	$112, %al
	jne	.L4729
	cmpb	$0, -184(%rbp)
	jns	.L4729
	testb	$32, -168(%rbp)
	jne	.L4729
	cmpl	$6, 12(%r13)
	je	.L4829
	andl	$127, %eax
	movb	%al, 8(%r12)
	jmp	.L4729
	.p2align 4,,10
	.p2align 3
.L4822:
	movb	%cl, 8(%r12)
	movl	4(%rsi), %eax
	leaq	9(%rsi), %rdx
	movq	%r12, %rdi
	leaq	-168(%rbp), %r9
	leaq	-164(%rbp), %r8
	movl	%ebx, %r14d
	movl	$0, -164(%rbp)
	movl	%eax, 4(%r12)
	movl	(%rsi), %ecx
	movq	%r13, %rsi
	movq	%r9, -192(%rbp)
	movq	%r8, -184(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	4(%r12), %edx
	movq	-184(%rbp), %r8
	movq	-192(%rbp), %r9
	subl	%edx, %r14d
	jns	.L4709
	jmp	.L4812
	.p2align 4,,10
	.p2align 3
.L4692:
	subl	%ebx, %edx
.L4734:
	leaq	9(%r12), %rdi
	movq	%r8, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	movl	4(%r12), %edx
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %r9
	movl	%eax, %esi
	addl	%r14d, %edx
	jmp	.L4693
.L4820:
	testb	$112, 8(%r12)
	jne	.L4681
	testl	%r14d, %r14d
	jns	.L4682
	movl	%ebx, 4(%r12)
.L4682:
	cmpb	$0, -184(%rbp)
	jns	.L4709
	cmpl	$6, 12(%r13)
	je	.L4685
	movb	$0, 8(%r12)
	jmp	.L4709
.L4821:
	subl	%esi, %eax
	movl	%eax, %edx
	jmp	.L4734
.L4696:
	subl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L4741
	cmpq	%rsi, %r12
	je	.L4701
	movq	%r12, %rdi
	call	uprv_decNumberCopy_67
.L4701:
	movslq	(%r12), %rax
	subl	%r15d, %r14d
	movb	%r14b, 9(%r12)
	cmpl	$49, %eax
	jg	.L4702
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
.L4702:
	movslq	%eax, %rdx
	leaq	9(%r12), %rcx
	leaq	8(%r12,%rdx), %rdx
	cmpq	%rdx, %rcx
	ja	.L4703
	leaq	8(%r12), %rcx
.L4704:
	cmpb	$0, (%rdx)
	jne	.L4703
	cmpl	$1, %eax
	je	.L4703
	subq	$1, %rdx
	subl	$1, %eax
	cmpq	%rcx, %rdx
	jne	.L4704
.L4703:
	movl	%eax, (%r12)
	movl	-168(%rbp), %esi
	jmp	.L4674
.L4827:
	movslq	%r10d, %r8
	leaq	_ZL9DECPOWERS(%rip), %r9
	cmpl	(%r9,%r8,4), %eax
	jl	.L4698
.L4697:
	cmpl	%r10d, %edi
	leaq	9(%r12), %rbx
	cmovl	%r10, %rdi
	cmpl	%edi, %r11d
	jle	.L4746
	movq	%rbx, %r15
	xorl	%r8d, %r8d
	movl	$1, %r9d
	xorl	%r14d, %r14d
	jmp	.L4713
.L4829:
	orl	$-128, %eax
	movb	%al, 8(%r12)
	jmp	.L4729
.L4741:
	xorl	%r8d, %r8d
	movl	$1, %r9d
	jmp	.L4700
.L4746:
	xorl	%r8d, %r8d
	movl	$1, %r9d
	jmp	.L4712
.L4815:
	call	__stack_chk_fail@PLT
.L4685:
	movb	$-128, 8(%r12)
	jmp	.L4709
.L4828:
	movl	-168(%rbp), %esi
	orl	$16, %esi
	movl	%esi, -168(%rbp)
	jmp	.L4716
	.cfi_endproc
.LFE2062:
	.size	uprv_decNumberAdd_67, .-uprv_decNumberAdd_67
	.p2align 4
	.globl	uprv_decNumberSubtract_67
	.type	uprv_decNumberSubtract_67, @function
uprv_decNumberSubtract_67:
.LFB2099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movzbl	8(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rdx), %eax
	movl	$0, -168(%rbp)
	movl	%ecx, %edi
	xorl	%eax, %edi
	leal	-128(%rdi), %ebx
	movl	%ecx, %edi
	orl	%eax, %edi
	movb	%bl, -184(%rbp)
	movl	%edi, %r14d
	andl	$112, %r14d
	je	.L4831
	andl	$48, %edi
	jne	.L4974
	testb	$64, %cl
	jne	.L4975
	notl	%eax
	movl	%eax, %ecx
.L4968:
	andl	$-128, %ecx
	movq	$1, (%r12)
	orl	$64, %ecx
	movb	$0, 9(%r12)
	movb	%cl, 8(%r12)
.L4838:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4976
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4831:
	.cfi_restore_state
	movzbl	9(%rsi), %r15d
	testb	%r15b, %r15b
	je	.L4977
.L4839:
	movl	0(%r13), %ebx
	movzbl	9(%rdx), %r11d
	movslq	(%rdx), %rdi
	movl	%ebx, -192(%rbp)
	movl	4(%rdx), %ebx
	testb	%r11b, %r11b
	jne	.L4848
	cmpl	$1, %edi
	je	.L4978
.L4848:
	movl	4(%rsi), %r9d
	movl	%ebx, %r8d
	movslq	(%rsi), %r10
	subl	%r9d, %r8d
	jne	.L4853
	movl	$1, %r9d
	cmpl	$1, %edi
	jle	.L4895
.L4854:
	cmpb	$0, -184(%rbp)
	js	.L4859
.L4871:
	cmpl	%r10d, %edi
	leaq	9(%r12), %rbx
	cmovl	%r10, %rdi
	cmpl	%edi, -192(%rbp)
	jle	.L4872
	cmpq	%rdx, %r12
	jne	.L4903
	testl	%r8d, %r8d
	jle	.L4903
.L4872:
	leal	1(%rdi), %eax
	cmpl	$49, %edi
	jg	.L4875
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rdi), %eax
	addl	$1, %eax
.L4875:
	xorl	%r14d, %r14d
	leaq	-160(%rbp), %r15
	cmpl	$92, %eax
	jg	.L4979
.L4873:
	andl	$-128, %ecx
	movb	%cl, 8(%r12)
	movl	4(%rsi), %eax
	movl	%eax, 4(%r12)
	movslq	(%rdx), %rcx
	cmpl	$49, %ecx
	jg	.L4877
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L4877:
	movslq	(%rsi), %r10
	addq	$9, %rdx
	cmpl	$49, %r10d
	jg	.L4878
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%r10), %r10d
.L4878:
	subq	$8, %rsp
	leaq	9(%rsi), %rdi
	movl	%r10d, %esi
	pushq	%r9
	movq	%r15, %r9
	call	_ZL13decUnitAddSubPKhiS0_iiPhi
	popq	%rdx
	popq	%rsi
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L4879
	movl	%eax, (%r12)
.L4880:
	leaq	-168(%rbp), %r9
	leaq	-164(%rbp), %r8
	movl	$0, -164(%rbp)
	cmpq	%rbx, %r15
	je	.L4881
	cmpl	%ecx, -192(%rbp)
	jge	.L4882
	cmpl	$49, %ecx
	jg	.L4883
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L4883:
	movslq	%ecx, %rax
	leaq	-1(%r15,%rax), %rax
	cmpq	%rax, %r15
	ja	.L4884
.L4885:
	cmpb	$0, (%rax)
	jne	.L4884
	cmpl	$1, %ecx
	je	.L4884
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rax, %r15
	jbe	.L4885
	.p2align 4,,10
	.p2align 3
.L4884:
	movl	%ecx, (%r12)
.L4882:
	leaq	-168(%rbp), %r9
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-164(%rbp), %r8
	movq	%r9, -200(%rbp)
	movq	%r8, -192(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	(%r12), %ecx
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %r8
.L4881:
	cmpl	$49, %ecx
	jg	.L4886
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L4886:
	movslq	%ecx, %rax
	leaq	-1(%rbx,%rax), %rax
	cmpq	%rbx, %rax
	jb	.L4887
	leaq	8(%r12), %rdx
.L4888:
	cmpb	$0, (%rax)
	jne	.L4887
	cmpl	$1, %ecx
	je	.L4887
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rdx, %rax
	jne	.L4888
	.p2align 4,,10
	.p2align 3
.L4887:
	movl	%ecx, (%r12)
	movq	%r8, %rdx
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	cmpb	$0, 9(%r12)
	jne	.L4889
	cmpl	$1, (%r12)
	je	.L4980
.L4889:
	testq	%r14, %r14
	je	.L4969
	movq	%r14, %rdi
	call	uprv_free_67@PLT
.L4969:
	movl	-168(%rbp), %esi
	jmp	.L4833
	.p2align 4,,10
	.p2align 3
.L4977:
	cmpl	$1, (%rsi)
	jne	.L4839
	testb	$112, %cl
	jne	.L4839
	movl	4(%rsi), %ebx
	movb	%al, 8(%r12)
	leaq	9(%rdx), %r10
	leaq	-168(%rbp), %r9
	movl	4(%rdx), %eax
	leaq	-164(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -200(%rbp)
	movl	%ebx, %r14d
	movl	%eax, 4(%r12)
	movl	(%rdx), %ecx
	movq	%r10, %rdx
	movq	%r8, -192(%rbp)
	movl	$0, -164(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movzbl	8(%r12), %eax
	movl	4(%r12), %edx
	movq	-192(%rbp), %r8
	movq	-200(%rbp), %r9
	addl	$-128, %eax
	subl	%edx, %r14d
	cmpb	$0, 9(%r12)
	movb	%al, 8(%r12)
	jne	.L4840
	cmpl	$1, (%r12)
	je	.L4981
.L4840:
	testl	%r14d, %r14d
	jns	.L4869
.L4973:
	movl	(%r12), %esi
	movl	0(%r13), %eax
	movl	%esi, %ecx
	subl	%r14d, %ecx
	cmpl	%eax, %ecx
	jle	.L4851
	orl	$2048, -168(%rbp)
	movl	%esi, %r14d
	subl	%eax, %r14d
	jne	.L4982
.L4852:
	movl	%esi, (%r12)
	movl	%edx, 4(%r12)
	jmp	.L4869
	.p2align 4,,10
	.p2align 3
.L4978:
	testb	$112, %al
	je	.L4983
	movl	4(%rsi), %r9d
	movl	%ebx, %r8d
	movslq	(%rsi), %r10
	subl	%r9d, %r8d
	je	.L4895
	.p2align 4,,10
	.p2align 3
.L4853:
	testl	%r8d, %r8d
	js	.L4984
.L4864:
	movl	-192(%rbp), %r11d
	leal	(%r8,%rdi), %ebx
	leal	1(%r11,%r10), %r9d
	cmpl	%r9d, %ebx
	jg	.L4985
	movl	$1, %r9d
	cmpl	$48, %r8d
	jg	.L4870
	leal	1(%r8), %eax
	leaq	_ZL8d2utable(%rip), %rdi
	cltq
	movzbl	(%rdi,%rax), %eax
	leaq	_ZL9DECPOWERS(%rip), %rdi
	subl	$1, %eax
	subl	%eax, %r8d
	movslq	%r8d, %r8
	movl	(%rdi,%r8,4), %r9d
	movl	%eax, %r8d
.L4870:
	cmpb	$0, -184(%rbp)
	movslq	%ebx, %rdi
	jns	.L4871
.L4859:
	negl	%r9d
	jmp	.L4871
	.p2align 4,,10
	.p2align 3
.L4975:
	testb	$64, %al
	je	.L4968
	cmpb	$0, -184(%rbp)
	jns	.L4968
	movl	$128, -168(%rbp)
	movl	$128, %esi
.L4836:
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
	jmp	.L4892
	.p2align 4,,10
	.p2align 3
.L4974:
	leaq	-168(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-168(%rbp), %esi
.L4833:
	testl	%esi, %esi
	je	.L4838
.L4876:
	testb	$-35, %sil
	jne	.L4986
.L4892:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L4838
	.p2align 4,,10
	.p2align 3
.L4985:
	movb	%al, 8(%r12)
	movl	4(%rdx), %eax
	movl	%r11d, %ebx
	leaq	9(%rdx), %r10
	movsbl	-184(%rbp), %ecx
	leaq	-168(%rbp), %r9
	subl	%edi, %ebx
	movq	%r13, %rsi
	movl	%eax, 4(%r12)
	leaq	-164(%rbp), %r8
	movq	%r12, %rdi
	sarl	$31, %ecx
	movq	%r9, -192(%rbp)
	orl	$1, %ecx
	movq	%r8, -184(%rbp)
	movl	%ecx, -164(%rbp)
	movl	(%rdx), %ecx
	movq	%r10, %rdx
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	testl	%ebx, %ebx
	movq	-184(%rbp), %r8
	movq	-192(%rbp), %r9
	jle	.L4868
	movl	(%r12), %esi
	leaq	9(%r12), %rdi
	movl	%ebx, %edx
	movq	%r8, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	subl	%ebx, 4(%r12)
	movq	-192(%rbp), %r8
	movl	%eax, (%r12)
	movq	-184(%rbp), %r9
.L4868:
	testb	%r14b, %r14b
	jne	.L4869
	addb	$-128, 8(%r12)
.L4869:
	movq	%r13, %rsi
	movq	%r9, %rcx
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-168(%rbp), %esi
	jmp	.L4833
	.p2align 4,,10
	.p2align 3
.L4984:
	subl	%ebx, %r9d
	movl	$1, %r14d
	movl	%r9d, %r8d
	leal	-128(%rax), %r9d
	movl	%ecx, %eax
	movl	%edi, %ecx
	movl	%r10d, %edi
	movslq	%ecx, %r10
	movl	%r9d, %ecx
	movq	%rsi, %r9
	movq	%rdx, %rsi
	movq	%r9, %rdx
	jmp	.L4864
	.p2align 4,,10
	.p2align 3
.L4895:
	cmpl	%ebx, 8(%r13)
	jg	.L4900
	movl	4(%r13), %eax
	movl	-192(%rbp), %r14d
	subl	%r14d, %eax
	addl	$1, %eax
	cmpl	%ebx, %eax
	jl	.L4900
	cmpl	%edi, %r14d
	jl	.L4900
	cmpl	%r10d, %r14d
	jl	.L4900
	cmpb	$0, -184(%rbp)
	movzbl	%r15b, %eax
	movzbl	%r11b, %r8d
	js	.L4855
	addl	%r8d, %eax
	cmpl	$9, %eax
	jg	.L4856
	testl	%r10d, %r10d
	jle	.L4987
.L4857:
	cmpq	%rsi, %r12
	je	.L4858
	movq	%r12, %rdi
	call	uprv_decNumberCopy_67
.L4858:
	addl	%r11d, %r15d
	movl	-168(%rbp), %esi
	movb	%r15b, 9(%r12)
	jmp	.L4833
	.p2align 4,,10
	.p2align 3
.L4900:
	movl	$1, %r9d
	xorl	%r8d, %r8d
	jmp	.L4854
	.p2align 4,,10
	.p2align 3
.L4879:
	negl	%ecx
	addb	$-128, 8(%r12)
	movl	%ecx, (%r12)
	jmp	.L4880
	.p2align 4,,10
	.p2align 3
.L4986:
	testl	$1073741824, %esi
	je	.L4836
	andl	$-1073741825, %esi
	jmp	.L4892
	.p2align 4,,10
	.p2align 3
.L4979:
	movslq	%eax, %rdi
	movq	%rdx, -224(%rbp)
	movq	%rsi, -216(%rbp)
	movl	%r9d, -208(%rbp)
	movl	%r8d, -204(%rbp)
	movb	%cl, -200(%rbp)
	call	uprv_malloc_67@PLT
	movzbl	-200(%rbp), %ecx
	movl	-204(%rbp), %r8d
	testq	%rax, %rax
	movl	-208(%rbp), %r9d
	movq	%rax, %r15
	movq	-216(%rbp), %rsi
	movq	-224(%rbp), %rdx
	je	.L4988
	movq	%rax, %r14
	jmp	.L4873
	.p2align 4,,10
	.p2align 3
.L4903:
	movq	%rbx, %r15
	xorl	%r14d, %r14d
	jmp	.L4873
	.p2align 4,,10
	.p2align 3
.L4980:
	movzbl	8(%r12), %eax
	testb	$112, %al
	jne	.L4889
	cmpb	$0, -184(%rbp)
	jns	.L4889
	testb	$32, -168(%rbp)
	jne	.L4889
	cmpl	$6, 12(%r13)
	je	.L4989
	andl	$127, %eax
	movb	%al, 8(%r12)
	jmp	.L4889
	.p2align 4,,10
	.p2align 3
.L4983:
	movb	%cl, 8(%r12)
	movl	4(%rsi), %eax
	leaq	9(%rsi), %rdx
	movq	%r12, %rdi
	leaq	-168(%rbp), %r9
	leaq	-164(%rbp), %r8
	movl	%ebx, %r14d
	movl	$0, -164(%rbp)
	movl	%eax, 4(%r12)
	movl	(%rsi), %ecx
	movq	%r13, %rsi
	movq	%r9, -192(%rbp)
	movq	%r8, -184(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	4(%r12), %edx
	movq	-184(%rbp), %r8
	movq	-192(%rbp), %r9
	subl	%edx, %r14d
	jns	.L4869
	jmp	.L4973
	.p2align 4,,10
	.p2align 3
.L4851:
	subl	%ebx, %edx
.L4894:
	leaq	9(%r12), %rdi
	movq	%r8, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	movl	4(%r12), %edx
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %r9
	movl	%eax, %esi
	addl	%r14d, %edx
	jmp	.L4852
.L4981:
	testb	$112, %al
	jne	.L4840
	testl	%r14d, %r14d
	jns	.L4841
	movl	%ebx, 4(%r12)
.L4841:
	cmpb	$0, -184(%rbp)
	jns	.L4869
	cmpl	$6, 12(%r13)
	je	.L4844
	movb	$0, 8(%r12)
	jmp	.L4869
.L4982:
	subl	%esi, %eax
	movl	%eax, %edx
	jmp	.L4894
.L4855:
	subl	%r8d, %eax
	testl	%eax, %eax
	jle	.L4901
	cmpq	%rsi, %r12
	je	.L4860
	movq	%r12, %rdi
	call	uprv_decNumberCopy_67
.L4860:
	movslq	(%r12), %rax
	subl	%r11d, %r15d
	movb	%r15b, 9(%r12)
	cmpl	$49, %eax
	jg	.L4861
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
.L4861:
	movslq	%eax, %rdx
	leaq	9(%r12), %rcx
	leaq	8(%r12,%rdx), %rdx
	cmpq	%rdx, %rcx
	ja	.L4862
	leaq	8(%r12), %rcx
.L4863:
	cmpb	$0, (%rdx)
	jne	.L4862
	cmpl	$1, %eax
	je	.L4862
	subq	$1, %rdx
	subl	$1, %eax
	cmpq	%rcx, %rdx
	jne	.L4863
.L4862:
	movl	%eax, (%r12)
	movl	-168(%rbp), %esi
	jmp	.L4833
.L4987:
	movslq	%r10d, %r8
	leaq	_ZL9DECPOWERS(%rip), %r9
	cmpl	(%r9,%r8,4), %eax
	jl	.L4857
.L4856:
	cmpl	%r10d, %edi
	leaq	9(%r12), %rbx
	cmovl	%r10, %rdi
	cmpl	%edi, -192(%rbp)
	jle	.L4906
	movq	%rbx, %r15
	xorl	%r8d, %r8d
	movl	$1, %r9d
	xorl	%r14d, %r14d
	jmp	.L4873
.L4989:
	orl	$-128, %eax
	movb	%al, 8(%r12)
	jmp	.L4889
.L4901:
	xorl	%r8d, %r8d
	movl	$1, %r9d
	jmp	.L4859
.L4906:
	xorl	%r8d, %r8d
	movl	$1, %r9d
	jmp	.L4872
.L4976:
	call	__stack_chk_fail@PLT
.L4844:
	movb	$-128, 8(%r12)
	jmp	.L4869
.L4988:
	movl	-168(%rbp), %esi
	orl	$16, %esi
	movl	%esi, -168(%rbp)
	jmp	.L4876
	.cfi_endproc
.LFE2099:
	.size	uprv_decNumberSubtract_67, .-uprv_decNumberSubtract_67
	.p2align 4
	.globl	uprv_decNumberNextMinus_67
	.type	uprv_decNumberNextMinus_67, @function
uprv_decNumberNextMinus_67:
.LFB2081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movzbl	8(%rsi), %ecx
	movdqu	(%rdx), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdx), %rax
	movl	$0, -216(%rbp)
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -192(%rbp)
	movl	24(%rdx), %eax
	movl	%eax, -184(%rbp)
	movl	%ecx, %eax
	andl	$-64, %eax
	cmpb	$64, %al
	je	.L5108
	movabsq	$-4294967295999999999, %rax
	movl	$256, %edi
	movl	%ecx, %ebx
	movl	$6, -196(%rbp)
	movq	%rax, -172(%rbp)
	movw	%di, -164(%rbp)
	andl	$112, %ebx
	je	.L4994
	testb	$48, %cl
	jne	.L5109
	movl	%ecx, %eax
	movl	$-64, %edx
	movq	$1, (%r12)
	andl	$-128, %eax
	movb	$0, 9(%r12)
	orl	$64, %eax
	andl	$64, %ecx
	cmove	%edx, %eax
	movb	%al, 8(%r12)
.L4993:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5110
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4994:
	.cfi_restore_state
	movzbl	9(%rsi), %r11d
	leal	-128(%rcx), %eax
	movl	4(%rsi), %r14d
	movb	%al, -232(%rbp)
	movslq	(%rsi), %rax
	testb	%r11b, %r11b
	jne	.L4998
	cmpl	$1, %eax
	jne	.L4998
	movb	$0, 8(%r12)
	leaq	-208(%rbp), %r10
	leaq	-216(%rbp), %r9
	movl	$1, %ecx
	leaq	-212(%rbp), %r8
	movq	%r10, %rsi
	movq	%r12, %rdi
	movl	%r14d, %ebx
	movl	$-1000000000, 4(%r12)
	leaq	-163(%rbp), %rdx
	movq	%r9, -256(%rbp)
	movq	%r8, -248(%rbp)
	movq	%r10, -240(%rbp)
	movl	$0, -212(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movzbl	8(%r12), %eax
	movl	4(%r12), %edx
	movq	-240(%rbp), %r10
	movq	-248(%rbp), %r8
	addl	$-128, %eax
	subl	%edx, %ebx
	cmpb	$0, 9(%r12)
	movq	-256(%rbp), %r9
	movb	%al, 8(%r12)
	jne	.L4999
	cmpl	$1, (%r12)
	jne	.L4999
	testb	$112, %al
	jne	.L4999
	testl	%ebx, %ebx
	jns	.L5000
	movl	%r14d, 4(%r12)
.L5000:
	cmpb	$0, -232(%rbp)
	jns	.L5024
	cmpl	$6, -196(%rbp)
	je	.L5003
	movb	$0, 8(%r12)
	.p2align 4,,10
	.p2align 3
.L5024:
	movq	%r10, %rsi
	movq	%r9, %rcx
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	movl	-216(%rbp), %eax
	movl	%eax, %esi
	andl	$1073741952, %esi
.L4996:
	movl	%esi, -216(%rbp)
	testl	%esi, %esi
	je	.L4993
	movl	%eax, %edx
	andl	$128, %edx
	je	.L5046
	testl	$1073741824, %eax
	jne	.L5060
	movl	$32, %eax
	movq	$1, (%r12)
	movw	%ax, 8(%r12)
.L5046:
	movq	%r13, %rdi
	call	uprv_decContextSetStatus_67@PLT
	jmp	.L4993
	.p2align 4,,10
	.p2align 3
.L4998:
	movl	$-1000000000, %r8d
	movl	-208(%rbp), %r10d
	subl	%r14d, %r8d
	je	.L5111
	js	.L5112
	xorl	%r9d, %r9d
	movl	$1, %r11d
	leaq	-172(%rbp), %rdx
.L5019:
	leal	(%r8,%r11), %edi
	leal	1(%r10,%rax), %r14d
	cmpl	%r14d, %edi
	jle	.L5020
	movsbl	-232(%rbp), %eax
	movb	%r9b, 8(%r12)
	movl	%r10d, %r14d
	movq	%r12, %rdi
	leaq	-208(%rbp), %r10
	subl	%r11d, %r14d
	leaq	9(%rdx), %r11
	sarl	$31, %eax
	leaq	-216(%rbp), %r9
	movq	%r10, %rsi
	movq	%r10, -232(%rbp)
	orl	$1, %eax
	leaq	-212(%rbp), %r8
	movq	%r9, -248(%rbp)
	movl	%eax, -212(%rbp)
	movl	4(%rdx), %eax
	movq	%r8, -240(%rbp)
	movl	%eax, 4(%r12)
	movl	(%rdx), %ecx
	movq	%r11, %rdx
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	testl	%r14d, %r14d
	movq	-232(%rbp), %r10
	movq	-240(%rbp), %r8
	movq	-248(%rbp), %r9
	jg	.L5113
.L5023:
	testb	%bl, %bl
	jne	.L5024
	addb	$-128, 8(%r12)
	jmp	.L5024
	.p2align 4,,10
	.p2align 3
.L5108:
	movslq	(%rdx), %rax
	leaq	9(%rdi), %rcx
	movl	%eax, (%rdi)
	cmpl	$1, %eax
	jle	.L4992
	leal	-2(%rax), %ebx
	movq	%rcx, %rdi
	movl	$9, %esi
	addq	$1, %rbx
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	movl	$1, %eax
	addq	%rbx, %rcx
.L4992:
	leaq	_ZL9DECPOWERS(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	subl	$1, %eax
	movb	%al, (%rcx)
	movb	$0, 8(%r12)
	movl	4(%r13), %eax
	subl	0(%r13), %eax
	addl	$1, %eax
	movl	%eax, 4(%r12)
	jmp	.L4993
	.p2align 4,,10
	.p2align 3
.L5111:
	cmpl	$-999999999, -200(%rbp)
	jl	.L5114
.L5053:
	movl	$1, %edi
	movl	$1, %r9d
	leaq	-172(%rbp), %rdx
.L5008:
	cmpb	$0, -232(%rbp)
	js	.L5014
.L5026:
	cmpl	%edi, %eax
	leaq	9(%r12), %r14
	cmovl	%edi, %eax
	cmpl	%eax, %r10d
	jle	.L5027
	cmpq	%rdx, %r12
	jne	.L5057
	testl	%r8d, %r8d
	jle	.L5057
.L5027:
	cmpl	$49, %eax
	jg	.L5029
.L5048:
	cltq
	leaq	_ZL8d2utable(%rip), %rdi
	movq	%r14, %rbx
	movzbl	(%rdi,%rax), %eax
	addl	$1, %eax
.L5030:
	xorl	%r15d, %r15d
	leaq	-160(%rbp), %r14
	cmpl	$92, %eax
	jg	.L5115
.L5028:
	andl	$-128, %ecx
	movb	%cl, 8(%r12)
	movl	4(%rsi), %eax
	movl	%eax, 4(%r12)
	movslq	(%rdx), %rcx
	cmpl	$49, %ecx
	jg	.L5031
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L5031:
	movslq	(%rsi), %r11
	addq	$9, %rdx
	cmpl	$49, %r11d
	jg	.L5032
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%r11), %r11d
.L5032:
	subq	$8, %rsp
	leaq	9(%rsi), %rdi
	movl	%r11d, %esi
	movl	%r10d, -240(%rbp)
	pushq	%r9
	movq	%r14, %r9
	call	_ZL13decUnitAddSubPKhiS0_iiPhi
	popq	%rdx
	movl	-240(%rbp), %r10d
	testl	%eax, %eax
	movl	%eax, %ecx
	popq	%rsi
	js	.L5033
	movl	%eax, (%r12)
.L5034:
	movl	$0, -212(%rbp)
	cmpq	%rbx, %r14
	je	.L5116
	cmpl	%ecx, %r10d
	jge	.L5036
	cmpl	$49, %ecx
	jle	.L5117
.L5037:
	movslq	%ecx, %rax
	leaq	-1(%r14,%rax), %rax
	cmpq	%rax, %r14
	ja	.L5038
.L5039:
	cmpb	$0, (%rax)
	jne	.L5038
	cmpl	$1, %ecx
	je	.L5038
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rax, %r14
	jbe	.L5039
	.p2align 4,,10
	.p2align 3
.L5038:
	movl	%ecx, (%r12)
.L5036:
	leaq	-208(%rbp), %r10
	leaq	-216(%rbp), %r9
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-212(%rbp), %r8
	movq	%r10, %rsi
	movq	%r9, -256(%rbp)
	movq	%r8, -248(%rbp)
	movq	%r10, -240(%rbp)
	call	_ZL11decSetCoeffP9decNumberP10decContextPKhiPiPj.isra.0
	movl	(%r12), %ecx
	movq	-256(%rbp), %r9
	movq	-248(%rbp), %r8
	movq	-240(%rbp), %r10
.L5035:
	cmpl	$49, %ecx
	jg	.L5040
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
.L5040:
	movslq	%ecx, %rax
	leaq	-1(%rbx,%rax), %rax
	cmpq	%rbx, %rax
	jb	.L5041
	leaq	8(%r12), %rdx
.L5042:
	cmpb	$0, (%rax)
	jne	.L5041
	cmpl	$1, %ecx
	je	.L5041
	subq	$1, %rax
	subl	$1, %ecx
	cmpq	%rdx, %rax
	jne	.L5042
	.p2align 4,,10
	.p2align 3
.L5041:
	movl	%ecx, (%r12)
	movq	%r8, %rdx
	movq	%r9, %rcx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZL11decFinalizeP9decNumberP10decContextPiPj
	cmpb	$0, 9(%r12)
	jne	.L5043
	cmpl	$1, (%r12)
	je	.L5118
.L5043:
	testq	%r15, %r15
	je	.L5107
	movq	%r15, %rdi
	call	uprv_free_67@PLT
.L5107:
	movl	-216(%rbp), %eax
	movl	%eax, %esi
	andl	$1073741952, %esi
	jmp	.L4996
	.p2align 4,,10
	.p2align 3
.L5109:
	leaq	-208(%rbp), %rcx
	leaq	-172(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-216(%rbp), %r8
	call	_ZL7decNaNsP9decNumberPKS_S2_P10decContextPj.isra.0
	movl	-216(%rbp), %eax
	movl	%eax, %esi
	andl	$1073741952, %esi
	jmp	.L4996
	.p2align 4,,10
	.p2align 3
.L5033:
	negl	%ecx
	addb	$-128, 8(%r12)
	movl	%ecx, (%r12)
	jmp	.L5034
.L5054:
	movl	$1, %edi
	leaq	-172(%rbp), %rdx
	movl	$1, %r9d
	.p2align 4,,10
	.p2align 3
.L5014:
	negl	%r9d
	jmp	.L5026
	.p2align 4,,10
	.p2align 3
.L5029:
	addl	$1, %eax
	movq	%r14, %rbx
	jmp	.L5030
	.p2align 4,,10
	.p2align 3
.L5020:
	movl	$1, %r9d
	cmpl	$48, %r8d
	jg	.L5008
	leal	1(%r8), %r9d
	leaq	_ZL8d2utable(%rip), %r11
	movslq	%r9d, %r9
	movzbl	(%r11,%r9), %r11d
	leaq	_ZL9DECPOWERS(%rip), %r9
	subl	$1, %r11d
	subl	%r11d, %r8d
	movslq	%r8d, %r8
	movl	(%r9,%r8,4), %r9d
	movl	%r11d, %r8d
	jmp	.L5008
	.p2align 4,,10
	.p2align 3
.L5116:
	leaq	-216(%rbp), %r9
	leaq	-208(%rbp), %r10
	leaq	-212(%rbp), %r8
	jmp	.L5035
	.p2align 4,,10
	.p2align 3
.L5112:
	movl	%ecx, %r9d
	movl	%eax, %r11d
	movq	%rsi, %rdx
	movl	$1, %eax
	leal	1000000000(%r14), %r8d
	movl	$1, %ebx
	movl	$-128, %ecx
	leaq	-172(%rbp), %rsi
	jmp	.L5019
	.p2align 4,,10
	.p2align 3
.L5115:
	movslq	%eax, %rdi
	movq	%rsi, -272(%rbp)
	movb	%cl, -261(%rbp)
	movl	%r8d, -260(%rbp)
	movl	%r9d, -256(%rbp)
	movq	%rdx, -248(%rbp)
	movl	%r10d, -240(%rbp)
	call	uprv_malloc_67@PLT
	movl	-240(%rbp), %r10d
	movq	-248(%rbp), %rdx
	testq	%rax, %rax
	movl	-256(%rbp), %r9d
	movl	-260(%rbp), %r8d
	movq	%rax, %r14
	movzbl	-261(%rbp), %ecx
	movq	-272(%rbp), %rsi
	je	.L5119
	movq	%rax, %r15
	jmp	.L5028
	.p2align 4,,10
	.p2align 3
.L5057:
	movq	%r14, %rbx
	xorl	%r15d, %r15d
	jmp	.L5028
	.p2align 4,,10
	.p2align 3
.L5060:
	movl	%edx, %esi
	jmp	.L5046
	.p2align 4,,10
	.p2align 3
.L5117:
	movslq	%ecx, %rcx
	leaq	_ZL8d2utable(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
	jmp	.L5037
.L5114:
	movl	-204(%rbp), %edx
	subl	%r10d, %edx
	cmpl	$-1000000001, %edx
	jl	.L5053
	testl	%r10d, %r10d
	jle	.L5053
	cmpl	%eax, %r10d
	jl	.L5053
	cmpb	$0, -232(%rbp)
	movzbl	%r11b, %edx
	js	.L5009
	addl	$1, %edx
	cmpl	$9, %edx
	jg	.L5010
	testl	%eax, %eax
	jle	.L5120
.L5011:
	cmpq	%r12, %rsi
	je	.L5013
	movq	%r12, %rdi
	call	uprv_decNumberCopy_67
.L5013:
	movl	-216(%rbp), %eax
	addl	$1, %r11d
	movb	%r11b, 9(%r12)
	movl	%eax, %esi
	andl	$1073741952, %esi
	jmp	.L4996
	.p2align 4,,10
	.p2align 3
.L5113:
	movl	(%r12), %esi
	leaq	9(%r12), %rdi
	movl	%r14d, %edx
	movq	%r10, -248(%rbp)
	movq	%r9, -232(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	subl	%r14d, 4(%r12)
	movq	-248(%rbp), %r10
	movl	%eax, (%r12)
	movq	-240(%rbp), %r8
	movq	-232(%rbp), %r9
	jmp	.L5023
.L4999:
	testl	%ebx, %ebx
	jns	.L5024
	movl	(%r12), %esi
	movl	-208(%rbp), %eax
	movl	%esi, %ecx
	subl	%ebx, %ecx
	cmpl	%eax, %ecx
	jle	.L5005
	orl	$2048, -216(%rbp)
	movl	%esi, %ebx
	subl	%eax, %ebx
	jne	.L5121
.L5006:
	movl	%esi, (%r12)
	movl	%edx, 4(%r12)
	jmp	.L5024
	.p2align 4,,10
	.p2align 3
.L5118:
	movzbl	8(%r12), %eax
	testb	$112, %al
	jne	.L5043
	cmpb	$0, -232(%rbp)
	jns	.L5043
	testb	$32, -216(%rbp)
	jne	.L5043
	cmpl	$6, -196(%rbp)
	je	.L5122
	andl	$127, %eax
	movb	%al, 8(%r12)
	jmp	.L5043
.L5005:
	subl	%r14d, %edx
.L5047:
	leaq	9(%r12), %rdi
	movq	%r10, -248(%rbp)
	movq	%r8, -240(%rbp)
	movq	%r9, -232(%rbp)
	call	_ZL14decShiftToMostPhii.part.0
	addl	4(%r12), %ebx
	movq	-248(%rbp), %r10
	movq	-240(%rbp), %r8
	movq	-232(%rbp), %r9
	movl	%eax, %esi
	movl	%ebx, %edx
	jmp	.L5006
.L5009:
	cmpl	$1, %edx
	jle	.L5054
	cmpq	%r12, %rsi
	je	.L5015
	movq	%r12, %rdi
	call	uprv_decNumberCopy_67
.L5015:
	movslq	(%r12), %rax
	subl	$1, %r11d
	movb	%r11b, 9(%r12)
	cmpl	$49, %eax
	jg	.L5016
	leaq	_ZL8d2utable(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
.L5016:
	movslq	%eax, %rdx
	leaq	9(%r12), %rcx
	leaq	8(%r12,%rdx), %rdx
	cmpq	%rdx, %rcx
	ja	.L5017
	leaq	8(%r12), %rcx
.L5018:
	cmpb	$0, (%rdx)
	jne	.L5017
	cmpl	$1, %eax
	je	.L5017
	subq	$1, %rdx
	subl	$1, %eax
	cmpq	%rcx, %rdx
	jne	.L5018
.L5017:
	movl	%eax, (%r12)
	movl	-216(%rbp), %eax
	movl	%eax, %esi
	andl	$1073741952, %esi
	jmp	.L4996
.L5010:
	testl	%eax, %eax
	movl	$1, %edx
	leaq	9(%r12), %r14
	cmovle	%edx, %eax
	cmpl	%eax, %r10d
	jg	.L5106
	leaq	-172(%rbp), %rdx
	movl	$1, %r9d
	jmp	.L5027
.L5122:
	orl	$-128, %eax
	movb	%al, 8(%r12)
	jmp	.L5043
.L5121:
	subl	%esi, %eax
	movl	%eax, %edx
	jmp	.L5047
.L5106:
	movq	%r14, %rbx
	leaq	-172(%rbp), %rdx
	movl	$1, %r9d
	xorl	%r15d, %r15d
	jmp	.L5028
.L5110:
	call	__stack_chk_fail@PLT
.L5120:
	leaq	_ZL9DECPOWERS(%rip), %rdi
	cmpl	(%rdi,%rax,4), %edx
	jl	.L5011
	leaq	9(%r12), %r14
	cmpl	$1, %r10d
	jg	.L5106
	leaq	-172(%rbp), %rdx
	movl	$1, %r9d
	movl	$1, %eax
	jmp	.L5048
.L5003:
	movb	$-128, 8(%r12)
	jmp	.L5024
.L5119:
	movl	-216(%rbp), %esi
	movl	%esi, %eax
	andl	$1073741952, %esi
	orl	$16, %eax
	jmp	.L4996
	.cfi_endproc
.LFE2081:
	.size	uprv_decNumberNextMinus_67, .-uprv_decNumberNextMinus_67
	.p2align 4
	.globl	uprv_decNumberCopyAbs_67
	.type	uprv_decNumberCopyAbs_67, @function
uprv_decNumberCopyAbs_67:
.LFB2106:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	cmpq	%rsi, %rdi
	je	.L5124
	movzbl	8(%rsi), %edx
	movb	%dl, 8(%rdi)
	movl	4(%rsi), %ecx
	movl	%ecx, 4(%rdi)
	movl	(%rsi), %ecx
	movl	%ecx, (%rdi)
	movzbl	9(%rsi), %ecx
	movb	%cl, 9(%rdi)
	movl	(%rsi), %ecx
	cmpl	$1, %ecx
	jle	.L5125
	leaq	10(%rdi), %r8
	leaq	9(%rsi), %r9
	movslq	%ecx, %rdi
	cmpl	$49, %ecx
	jg	.L5127
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rdi), %edi
.L5127:
	addq	%r9, %rdi
	leaq	10(%rsi), %rcx
	cmpq	%rcx, %rdi
	jbe	.L5125
	movq	%rdi, %rdx
	subq	%rsi, %rdx
	subq	$11, %rdx
	cmpq	$14, %rdx
	jbe	.L5128
	leaq	25(%rax), %rdx
	subq	%rcx, %rdx
	cmpq	$30, %rdx
	jbe	.L5128
	movq	%rdi, %rdx
	subq	%rsi, %rdx
	leaq	-10(%rdx), %r10
	movl	$10, %edx
	movq	%r10, %r9
	andq	$-16, %r9
	addq	$10, %r9
	.p2align 4,,10
	.p2align 3
.L5129:
	movdqu	(%rsi,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%r9, %rdx
	jne	.L5129
	movq	%r10, %rsi
	andq	$-16, %rsi
	leaq	(%rcx,%rsi), %rdx
	leaq	(%r8,%rsi), %rcx
	cmpq	%rsi, %r10
	je	.L5124
	movzbl	(%rdx), %esi
	movb	%sil, (%rcx)
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5124
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rcx)
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5124
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rcx)
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5124
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rcx)
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5124
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rcx)
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5124
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rcx)
	leaq	6(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5124
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rcx)
	leaq	7(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5124
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rcx)
	leaq	8(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5124
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rcx)
	leaq	9(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5124
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rcx)
	leaq	10(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5124
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rcx)
	leaq	11(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5124
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rcx)
	leaq	12(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5124
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rcx)
	leaq	13(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5124
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rcx)
	leaq	14(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5124
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L5124:
	movzbl	8(%rax), %edx
.L5125:
	andl	$127, %edx
	movb	%dl, 8(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L5128:
	subq	%rsi, %rdi
	movl	$10, %edx
	.p2align 4,,10
	.p2align 3
.L5132:
	movzbl	(%rsi,%rdx), %ecx
	movb	%cl, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L5132
	jmp	.L5124
	.cfi_endproc
.LFE2106:
	.size	uprv_decNumberCopyAbs_67, .-uprv_decNumberCopyAbs_67
	.p2align 4
	.globl	uprv_decNumberCopyNegate_67
	.type	uprv_decNumberCopyNegate_67, @function
uprv_decNumberCopyNegate_67:
.LFB2107:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	cmpq	%rsi, %rdi
	je	.L5141
	movzbl	8(%rsi), %edx
	movb	%dl, 8(%rdi)
	movl	4(%rsi), %ecx
	movl	%ecx, 4(%rdi)
	movl	(%rsi), %ecx
	movl	%ecx, (%rdi)
	movzbl	9(%rsi), %ecx
	movb	%cl, 9(%rdi)
	movl	(%rsi), %ecx
	cmpl	$1, %ecx
	jle	.L5142
	leaq	10(%rdi), %r8
	leaq	9(%rsi), %r9
	movslq	%ecx, %rdi
	cmpl	$49, %ecx
	jg	.L5144
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rdi), %edi
.L5144:
	addq	%r9, %rdi
	leaq	10(%rsi), %rcx
	cmpq	%rcx, %rdi
	jbe	.L5142
	movq	%rdi, %rdx
	subq	%rsi, %rdx
	subq	$11, %rdx
	cmpq	$14, %rdx
	jbe	.L5145
	leaq	25(%rax), %rdx
	subq	%rcx, %rdx
	cmpq	$30, %rdx
	jbe	.L5145
	movq	%rdi, %rdx
	subq	%rsi, %rdx
	leaq	-10(%rdx), %r10
	movl	$10, %edx
	movq	%r10, %r9
	andq	$-16, %r9
	addq	$10, %r9
	.p2align 4,,10
	.p2align 3
.L5146:
	movdqu	(%rsi,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%r9, %rdx
	jne	.L5146
	movq	%r10, %rsi
	andq	$-16, %rsi
	leaq	(%rcx,%rsi), %rdx
	leaq	(%r8,%rsi), %rcx
	cmpq	%rsi, %r10
	je	.L5141
	movzbl	(%rdx), %esi
	movb	%sil, (%rcx)
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5141
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rcx)
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5141
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rcx)
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5141
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rcx)
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5141
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rcx)
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5141
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rcx)
	leaq	6(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5141
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rcx)
	leaq	7(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5141
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rcx)
	leaq	8(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5141
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rcx)
	leaq	9(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5141
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rcx)
	leaq	10(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5141
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rcx)
	leaq	11(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5141
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rcx)
	leaq	12(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5141
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rcx)
	leaq	13(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5141
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rcx)
	leaq	14(%rdx), %rsi
	cmpq	%rsi, %rdi
	jbe	.L5141
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L5141:
	movzbl	8(%rax), %edx
.L5142:
	addl	$-128, %edx
	movb	%dl, 8(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L5145:
	subq	%rsi, %rdi
	movl	$10, %edx
	.p2align 4,,10
	.p2align 3
.L5149:
	movzbl	(%rsi,%rdx), %ecx
	movb	%cl, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L5149
	jmp	.L5141
	.cfi_endproc
.LFE2107:
	.size	uprv_decNumberCopyNegate_67, .-uprv_decNumberCopyNegate_67
	.p2align 4
	.globl	uprv_decNumberCopySign_67
	.type	uprv_decNumberCopySign_67, @function
uprv_decNumberCopySign_67:
.LFB2108:
	.cfi_startproc
	endbr64
	movzbl	8(%rdx), %edx
	movq	%rdi, %rax
	andl	$-128, %edx
	movl	%edx, %r9d
	cmpq	%rsi, %rdi
	je	.L5158
	movzbl	8(%rsi), %edx
	movb	%dl, 8(%rdi)
	movl	4(%rsi), %ecx
	movl	%ecx, 4(%rdi)
	movl	(%rsi), %ecx
	movl	%ecx, (%rdi)
	movzbl	9(%rsi), %ecx
	movb	%cl, 9(%rdi)
	movl	(%rsi), %ecx
	cmpl	$1, %ecx
	jle	.L5159
	leaq	10(%rdi), %r10
	leaq	9(%rsi), %r8
	movslq	%ecx, %rdi
	cmpl	$49, %ecx
	jg	.L5161
	leaq	_ZL8d2utable(%rip), %rcx
	movzbl	(%rcx,%rdi), %edi
.L5161:
	addq	%rdi, %r8
	leaq	10(%rsi), %rdi
	cmpq	%rdi, %r8
	jbe	.L5159
	movq	%r8, %rdx
	subq	%rsi, %rdx
	subq	$11, %rdx
	cmpq	$14, %rdx
	jbe	.L5162
	leaq	25(%rax), %rdx
	subq	%rdi, %rdx
	cmpq	$30, %rdx
	jbe	.L5162
	movq	%r8, %rdx
	movl	$10, %ecx
	subq	%rsi, %rdx
	leaq	-10(%rdx), %r11
	movq	%r11, %rdx
	andq	$-16, %rdx
	addq	$10, %rdx
	.p2align 4,,10
	.p2align 3
.L5163:
	movdqu	(%rsi,%rcx), %xmm0
	movups	%xmm0, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rdx, %rcx
	jne	.L5163
	movq	%r11, %rsi
	andq	$-16, %rsi
	leaq	(%rdi,%rsi), %rcx
	leaq	(%r10,%rsi), %rdx
	cmpq	%rsi, %r11
	je	.L5158
	movzbl	(%rcx), %esi
	movb	%sil, (%rdx)
	leaq	1(%rcx), %rsi
	cmpq	%rsi, %r8
	jbe	.L5158
	movzbl	1(%rcx), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%rcx), %rsi
	cmpq	%rsi, %r8
	jbe	.L5158
	movzbl	2(%rcx), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%rcx), %rsi
	cmpq	%rsi, %r8
	jbe	.L5158
	movzbl	3(%rcx), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%rcx), %rsi
	cmpq	%rsi, %r8
	jbe	.L5158
	movzbl	4(%rcx), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%rcx), %rsi
	cmpq	%rsi, %r8
	jbe	.L5158
	movzbl	5(%rcx), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%rcx), %rsi
	cmpq	%rsi, %r8
	jbe	.L5158
	movzbl	6(%rcx), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%rcx), %rsi
	cmpq	%rsi, %r8
	jbe	.L5158
	movzbl	7(%rcx), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%rcx), %rsi
	cmpq	%rsi, %r8
	jbe	.L5158
	movzbl	8(%rcx), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%rcx), %rsi
	cmpq	%rsi, %r8
	jbe	.L5158
	movzbl	9(%rcx), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%rcx), %rsi
	cmpq	%rsi, %r8
	jbe	.L5158
	movzbl	10(%rcx), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%rcx), %rsi
	cmpq	%rsi, %r8
	jbe	.L5158
	movzbl	11(%rcx), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%rcx), %rsi
	cmpq	%rsi, %r8
	jbe	.L5158
	movzbl	12(%rcx), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%rcx), %rsi
	cmpq	%rsi, %r8
	jbe	.L5158
	movzbl	13(%rcx), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%rcx), %rsi
	cmpq	%rsi, %r8
	jbe	.L5158
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%rdx)
	.p2align 4,,10
	.p2align 3
.L5158:
	movzbl	8(%rax), %edx
.L5159:
	andl	$127, %edx
	orl	%r9d, %edx
	movb	%dl, 8(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L5162:
	subq	%rsi, %r8
	movl	$10, %edx
	.p2align 4,,10
	.p2align 3
.L5166:
	movzbl	(%rsi,%rdx), %ecx
	movb	%cl, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L5166
	jmp	.L5158
	.cfi_endproc
.LFE2108:
	.size	uprv_decNumberCopySign_67, .-uprv_decNumberCopySign_67
	.p2align 4
	.globl	uprv_decNumberGetBCD_67
	.type	uprv_decNumberGetBCD_67, @function
uprv_decNumberGetBCD_67:
.LFB2109:
	.cfi_startproc
	endbr64
	movslq	(%rdi), %rdx
	movq	%rsi, %rax
	addq	$9, %rdi
	leaq	-1(%rsi,%rdx), %rdx
	cmpq	%rdx, %rsi
	ja	.L5175
	leaq	-1(%rsi), %r8
	.p2align 4,,10
	.p2align 3
.L5176:
	movzbl	(%rdi), %ecx
	subq	$1, %rdx
	addq	$1, %rdi
	movb	%cl, 1(%rdx)
	cmpq	%r8, %rdx
	jne	.L5176
.L5175:
	ret
	.cfi_endproc
.LFE2109:
	.size	uprv_decNumberGetBCD_67, .-uprv_decNumberGetBCD_67
	.p2align 4
	.globl	uprv_decNumberSetBCD_67
	.type	uprv_decNumberSetBCD_67, @function
uprv_decNumberSetBCD_67:
.LFB2110:
	.cfi_startproc
	endbr64
	movslq	(%rdi), %r8
	movq	%rdi, %rax
	leaq	9(%rdi), %rcx
	cmpl	$49, %r8d
	jg	.L5179
	leaq	_ZL8d2utable(%rip), %rdi
	movzbl	(%rdi,%r8), %r8d
	subq	$1, %r8
.L5180:
	movl	%edx, %edi
	addq	%r8, %rcx
	leaq	(%rsi,%rdi), %r8
	cmpq	%r8, %rsi
	jnb	.L5181
	movq	%rcx, %r9
	subq	%rdi, %r9
	.p2align 4,,10
	.p2align 3
.L5182:
	movzbl	(%rsi), %r8d
	subq	$1, %rcx
	addq	$1, %rsi
	movb	%r8b, 1(%rcx)
	cmpq	%rcx, %r9
	jne	.L5182
.L5181:
	movl	%edx, (%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L5179:
	subq	$1, %r8
	jmp	.L5180
	.cfi_endproc
.LFE2110:
	.size	uprv_decNumberSetBCD_67, .-uprv_decNumberSetBCD_67
	.p2align 4
	.globl	uprv_decNumberIsNormal_67
	.type	uprv_decNumberIsNormal_67, @function
uprv_decNumberIsNormal_67:
.LFB2111:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	testb	$112, 8(%rdi)
	jne	.L5184
	cmpb	$0, 9(%rdi)
	movl	(%rdi), %eax
	je	.L5190
.L5186:
	xorl	%r8d, %r8d
	addl	4(%rdi), %eax
	cmpl	8(%rsi), %eax
	setg	%r8b
.L5184:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5190:
	cmpl	$1, %eax
	jne	.L5186
	jmp	.L5184
	.cfi_endproc
.LFE2111:
	.size	uprv_decNumberIsNormal_67, .-uprv_decNumberIsNormal_67
	.p2align 4
	.globl	uprv_decNumberIsSubnormal_67
	.type	uprv_decNumberIsSubnormal_67, @function
uprv_decNumberIsSubnormal_67:
.LFB2112:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	testb	$112, 8(%rdi)
	jne	.L5191
	cmpb	$0, 9(%rdi)
	movl	(%rdi), %eax
	je	.L5197
.L5193:
	xorl	%r8d, %r8d
	addl	4(%rdi), %eax
	cmpl	8(%rsi), %eax
	setle	%r8b
.L5191:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5197:
	cmpl	$1, %eax
	jne	.L5193
	jmp	.L5191
	.cfi_endproc
.LFE2112:
	.size	uprv_decNumberIsSubnormal_67, .-uprv_decNumberIsSubnormal_67
	.p2align 4
	.globl	uprv_decNumberTrim_67
	.type	uprv_decNumberTrim_67, @function
uprv_decNumberTrim_67:
.LFB2113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uprv_decContextDefault_67@PLT
	movl	$0, -68(%rbp)
	testb	$112, 8(%r12)
	jne	.L5199
	movzbl	9(%r12), %eax
	testb	$1, %al
	jne	.L5199
	testb	%al, %al
	jne	.L5200
	cmpl	$1, (%r12)
	je	.L5203
.L5200:
	leaq	-68(%rbp), %r8
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL7decTrimP9decNumberP10decContexthhPi.part.0
.L5199:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5204
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5203:
	.cfi_restore_state
	movl	$0, 4(%r12)
	jmp	.L5199
.L5204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2113:
	.size	uprv_decNumberTrim_67, .-uprv_decNumberTrim_67
	.section	.rodata.str1.1
.LC19:
	.string	"decNumber 3.61"
	.text
	.p2align 4
	.globl	uprv_decNumberVersion_67
	.type	uprv_decNumberVersion_67, @function
uprv_decNumberVersion_67:
.LFB2114:
	.cfi_startproc
	endbr64
	leaq	.LC19(%rip), %rax
	ret
	.cfi_endproc
.LFE2114:
	.size	uprv_decNumberVersion_67, .-uprv_decNumberVersion_67
	.p2align 4
	.globl	uprv_decNumberZero_67
	.type	uprv_decNumberZero_67, @function
uprv_decNumberZero_67:
.LFB2115:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	movq	$1, (%rdi)
	movq	%rdi, %rax
	movw	%dx, 8(%rdi)
	ret
	.cfi_endproc
.LFE2115:
	.size	uprv_decNumberZero_67, .-uprv_decNumberZero_67
	.section	.rodata
	.align 8
	.type	_ZL6resmap, @object
	.size	_ZL6resmap, 10
_ZL6resmap:
	.string	""
	.ascii	"\003\003\003\003\005\007\007\007\007"
	.align 32
	.type	_ZL4LNnn, @object
	.size	_ZL4LNnn, 180
_ZL4LNnn:
	.value	9016
	.value	8652
	.value	8316
	.value	8008
	.value	7724
	.value	7456
	.value	7208
	.value	6972
	.value	6748
	.value	6540
	.value	6340
	.value	6148
	.value	5968
	.value	5792
	.value	5628
	.value	5464
	.value	5312
	.value	5164
	.value	5020
	.value	4884
	.value	4748
	.value	4620
	.value	4496
	.value	4376
	.value	4256
	.value	4144
	.value	4032
	.value	-26303
	.value	-27355
	.value	-28379
	.value	-29379
	.value	-30355
	.value	-31307
	.value	-32239
	.value	32389
	.value	31501
	.value	30629
	.value	29777
	.value	28945
	.value	28129
	.value	27329
	.value	26545
	.value	25777
	.value	25021
	.value	24281
	.value	23553
	.value	22837
	.value	22137
	.value	21445
	.value	20769
	.value	20101
	.value	19445
	.value	18801
	.value	18165
	.value	17541
	.value	16925
	.value	16321
	.value	15721
	.value	15133
	.value	14553
	.value	13985
	.value	13421
	.value	12865
	.value	12317
	.value	11777
	.value	11241
	.value	10717
	.value	10197
	.value	9685
	.value	9177
	.value	8677
	.value	8185
	.value	7697
	.value	7213
	.value	6737
	.value	6269
	.value	5801
	.value	5341
	.value	4889
	.value	4437
	.value	-25606
	.value	-30002
	.value	31186
	.value	26886
	.value	22630
	.value	18418
	.value	14254
	.value	10130
	.value	6046
	.value	20055
	.align 16
	.type	_ZL7multies, @object
	.size	_ZL7multies, 20
_ZL7multies:
	.long	131073
	.long	26215
	.long	5243
	.long	1049
	.long	210
	.align 32
	.type	_ZL9DECPOWERS, @object
	.size	_ZL9DECPOWERS, 40
_ZL9DECPOWERS:
	.long	1
	.long	10
	.long	100
	.long	1000
	.long	10000
	.long	100000
	.long	1000000
	.long	10000000
	.long	100000000
	.long	1000000000
	.type	_ZL7uarrone, @object
	.size	_ZL7uarrone, 1
_ZL7uarrone:
	.ascii	"\001"
	.align 32
	.type	_ZL8d2utable, @object
	.size	_ZL8d2utable, 50
_ZL8d2utable:
	.string	""
	.ascii	"\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022"
	.ascii	"\023\024\025\026\027\030\031\032\033\034\035\036\037 !\"#$%&"
	.ascii	"'()*+,-./01"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	16
	.quad	16
	.align 16
.LC1:
	.quad	15
	.quad	15
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
