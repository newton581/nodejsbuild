	.file	"measunit.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit17getDynamicClassIDEv
	.type	_ZNK6icu_6711MeasureUnit17getDynamicClassIDEv, @function
_ZNK6icu_6711MeasureUnit17getDynamicClassIDEv:
.LFB2362:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6711MeasureUnit16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2362:
	.size	_ZNK6icu_6711MeasureUnit17getDynamicClassIDEv, .-_ZNK6icu_6711MeasureUnit17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUniteqERKNS_7UObjectE
	.type	_ZNK6icu_6711MeasureUniteqERKNS_7UObjectE, @function
_ZNK6icu_6711MeasureUniteqERKNS_7UObjectE:
.LFB2734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L12
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L5
	xorl	%r13d, %r13d
	cmpb	$42, (%rdi)
	je	.L3
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L3
.L5:
	movq	8(%r12), %rsi
	testq	%rsi, %rsi
	je	.L6
	movq	96(%rsi), %rsi
.L7:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L9
.L16:
	movq	96(%rdi), %rdi
.L10:
	call	strcmp@PLT
	testl	%eax, %eax
	sete	%r13b
.L3:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movsbq	18(%r12), %rdx
	testb	%dl, %dl
	js	.L7
	movswl	16(%r12), %eax
	testw	%ax, %ax
	js	.L7
	leaq	_ZN6icu_67L8gOffsetsE(%rip), %rcx
	movq	8(%rbx), %rdi
	addl	(%rcx,%rdx,4), %eax
	leaq	_ZN6icu_67L9gSubTypesE(%rip), %rdx
	cltq
	movq	(%rdx,%rax,8), %rsi
	testq	%rdi, %rdi
	jne	.L16
.L9:
	movsbq	18(%rbx), %rdx
	testb	%dl, %dl
	js	.L10
	movswl	16(%rbx), %eax
	testw	%ax, %ax
	js	.L10
	leaq	_ZN6icu_67L8gOffsetsE(%rip), %rcx
	addl	(%rcx,%rdx,4), %eax
	leaq	_ZN6icu_67L9gSubTypesE(%rip), %rdx
	cltq
	movq	(%rdx,%rax,8), %rdi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L12:
	addq	$8, %rsp
	movl	$1, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2734:
	.size	_ZNK6icu_6711MeasureUniteqERKNS_7UObjectE, .-_ZNK6icu_6711MeasureUniteqERKNS_7UObjectE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnitD0Ev
	.type	_ZN6icu_6711MeasureUnitD0Ev, @function
_ZN6icu_6711MeasureUnitD0Ev:
.LFB2730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L18
	cmpb	$0, 108(%r13)
	jne	.L30
	movl	8(%r13), %eax
	movq	16(%r13), %r8
	testl	%eax, %eax
	jle	.L20
.L32:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L24:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	8(%r13), %eax
	addq	$1, %rbx
	movq	16(%r13), %r8
	cmpl	%ebx, %eax
	jg	.L24
.L20:
	cmpb	$0, 28(%r13)
	jne	.L31
.L25:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L18:
	movq	$0, 8(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L24
	cmpb	$0, 28(%r13)
	je	.L25
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L30:
	movq	96(%r13), %rdi
	call	uprv_free_67@PLT
	movl	8(%r13), %eax
	movq	16(%r13), %r8
	testl	%eax, %eax
	jg	.L32
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L25
	.cfi_endproc
.LFE2730:
	.size	_ZN6icu_6711MeasureUnitD0Ev, .-_ZN6icu_6711MeasureUnitD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnitD2Ev
	.type	_ZN6icu_6711MeasureUnitD2Ev, @function
_ZN6icu_6711MeasureUnitD2Ev:
.LFB2728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L34
	cmpb	$0, 108(%r13)
	jne	.L46
	movl	8(%r13), %eax
	movq	16(%r13), %r8
	testl	%eax, %eax
	jle	.L36
.L48:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L40:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L37
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	8(%r13), %eax
	addq	$1, %rbx
	movq	16(%r13), %r8
	cmpl	%ebx, %eax
	jg	.L40
.L36:
	cmpb	$0, 28(%r13)
	jne	.L47
.L41:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L34:
	movq	$0, 8(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L40
	cmpb	$0, 28(%r13)
	je	.L41
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L46:
	movq	96(%r13), %rdi
	call	uprv_free_67@PLT
	movl	8(%r13), %eax
	movq	16(%r13), %r8
	testl	%eax, %eax
	jg	.L48
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L41
	.cfi_endproc
.LFE2728:
	.size	_ZN6icu_6711MeasureUnitD2Ev, .-_ZN6icu_6711MeasureUnitD2Ev
	.globl	_ZN6icu_6711MeasureUnitD1Ev
	.set	_ZN6icu_6711MeasureUnitD1Ev,_ZN6icu_6711MeasureUnitD2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3008:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3008:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3011:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L62
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L50
	cmpb	$0, 12(%rbx)
	jne	.L63
.L54:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L50:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L54
	.cfi_endproc
.LFE3011:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3014:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L66
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3014:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3017:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L69
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3017:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L75
.L71:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L76
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3019:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3020:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3020:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3021:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3021:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3022:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3022:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3023:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3023:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3024:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3024:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3025:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L92
	testl	%edx, %edx
	jle	.L92
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L95
.L84:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L84
	.cfi_endproc
.LFE3025:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L99
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L99
	testl	%r12d, %r12d
	jg	.L106
	cmpb	$0, 12(%rbx)
	jne	.L107
.L101:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L101
.L107:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L99:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3026:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L109
	movq	(%rdi), %r8
.L110:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L113
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L113
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L113:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3027:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3028:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L120
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3028:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3029:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3029:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3030:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3030:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3031:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3031:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3033:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3033:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3035:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3035:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16getStaticClassIDEv
	.type	_ZN6icu_6711MeasureUnit16getStaticClassIDEv, @function
_ZN6icu_6711MeasureUnit16getStaticClassIDEv:
.LFB2361:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6711MeasureUnit16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2361:
	.size	_ZN6icu_6711MeasureUnit16getStaticClassIDEv, .-_ZN6icu_6711MeasureUnit16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createGForceER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createGForceER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createGForceER10UErrorCode:
.LFB2363:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L132
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L129
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$0, 18(%rax)
.L127:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L129:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L127
	.cfi_endproc
.LFE2363:
	.size	_ZN6icu_6711MeasureUnit12createGForceER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createGForceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getGForceEv
	.type	_ZN6icu_6711MeasureUnit9getGForceEv, @function
_ZN6icu_6711MeasureUnit9getGForceEv:
.LFB2364:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$0, 18(%rdi)
	ret
	.cfi_endproc
.LFE2364:
	.size	_ZN6icu_6711MeasureUnit9getGForceEv, .-_ZN6icu_6711MeasureUnit9getGForceEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit27createMeterPerSecondSquaredER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit27createMeterPerSecondSquaredER10UErrorCode, @function
_ZN6icu_6711MeasureUnit27createMeterPerSecondSquaredER10UErrorCode:
.LFB2365:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L141
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L138
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$0, 18(%rax)
.L136:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L138:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L136
	.cfi_endproc
.LFE2365:
	.size	_ZN6icu_6711MeasureUnit27createMeterPerSecondSquaredER10UErrorCode, .-_ZN6icu_6711MeasureUnit27createMeterPerSecondSquaredER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit24getMeterPerSecondSquaredEv
	.type	_ZN6icu_6711MeasureUnit24getMeterPerSecondSquaredEv, @function
_ZN6icu_6711MeasureUnit24getMeterPerSecondSquaredEv:
.LFB2366:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$0, 18(%rdi)
	ret
	.cfi_endproc
.LFE2366:
	.size	_ZN6icu_6711MeasureUnit24getMeterPerSecondSquaredEv, .-_ZN6icu_6711MeasureUnit24getMeterPerSecondSquaredEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createArcMinuteER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createArcMinuteER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createArcMinuteER10UErrorCode:
.LFB2367:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L150
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L147
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$1, 18(%rax)
.L145:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L147:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L145
	.cfi_endproc
.LFE2367:
	.size	_ZN6icu_6711MeasureUnit15createArcMinuteER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createArcMinuteER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getArcMinuteEv
	.type	_ZN6icu_6711MeasureUnit12getArcMinuteEv, @function
_ZN6icu_6711MeasureUnit12getArcMinuteEv:
.LFB2368:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$1, 18(%rdi)
	ret
	.cfi_endproc
.LFE2368:
	.size	_ZN6icu_6711MeasureUnit12getArcMinuteEv, .-_ZN6icu_6711MeasureUnit12getArcMinuteEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createArcSecondER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createArcSecondER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createArcSecondER10UErrorCode:
.LFB2369:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L159
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L156
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$1, 18(%rax)
.L154:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L156:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L154
	.cfi_endproc
.LFE2369:
	.size	_ZN6icu_6711MeasureUnit15createArcSecondER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createArcSecondER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getArcSecondEv
	.type	_ZN6icu_6711MeasureUnit12getArcSecondEv, @function
_ZN6icu_6711MeasureUnit12getArcSecondEv:
.LFB2370:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$1, 18(%rdi)
	ret
	.cfi_endproc
.LFE2370:
	.size	_ZN6icu_6711MeasureUnit12getArcSecondEv, .-_ZN6icu_6711MeasureUnit12getArcSecondEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createDegreeER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createDegreeER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createDegreeER10UErrorCode:
.LFB2371:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L168
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L165
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$1, 18(%rax)
.L163:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L165:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L163
	.cfi_endproc
.LFE2371:
	.size	_ZN6icu_6711MeasureUnit12createDegreeER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createDegreeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getDegreeEv
	.type	_ZN6icu_6711MeasureUnit9getDegreeEv, @function
_ZN6icu_6711MeasureUnit9getDegreeEv:
.LFB2372:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$1, 18(%rdi)
	ret
	.cfi_endproc
.LFE2372:
	.size	_ZN6icu_6711MeasureUnit9getDegreeEv, .-_ZN6icu_6711MeasureUnit9getDegreeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createRadianER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createRadianER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createRadianER10UErrorCode:
.LFB2373:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L177
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L174
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$1, 18(%rax)
.L172:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L174:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L172
	.cfi_endproc
.LFE2373:
	.size	_ZN6icu_6711MeasureUnit12createRadianER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createRadianER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getRadianEv
	.type	_ZN6icu_6711MeasureUnit9getRadianEv, @function
_ZN6icu_6711MeasureUnit9getRadianEv:
.LFB2374:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$1, 18(%rdi)
	ret
	.cfi_endproc
.LFE2374:
	.size	_ZN6icu_6711MeasureUnit9getRadianEv, .-_ZN6icu_6711MeasureUnit9getRadianEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit21createRevolutionAngleER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit21createRevolutionAngleER10UErrorCode, @function
_ZN6icu_6711MeasureUnit21createRevolutionAngleER10UErrorCode:
.LFB2375:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L186
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L183
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$4, %edx
	movw	%dx, 16(%rax)
	movb	$1, 18(%rax)
.L181:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L183:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L181
	.cfi_endproc
.LFE2375:
	.size	_ZN6icu_6711MeasureUnit21createRevolutionAngleER10UErrorCode, .-_ZN6icu_6711MeasureUnit21createRevolutionAngleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit18getRevolutionAngleEv
	.type	_ZN6icu_6711MeasureUnit18getRevolutionAngleEv, @function
_ZN6icu_6711MeasureUnit18getRevolutionAngleEv:
.LFB2376:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$4, %edx
	movw	%dx, 16(%rdi)
	movb	$1, 18(%rdi)
	ret
	.cfi_endproc
.LFE2376:
	.size	_ZN6icu_6711MeasureUnit18getRevolutionAngleEv, .-_ZN6icu_6711MeasureUnit18getRevolutionAngleEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createAcreER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createAcreER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createAcreER10UErrorCode:
.LFB2377:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L195
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L192
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$2, 18(%rax)
.L190:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L192:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L190
	.cfi_endproc
.LFE2377:
	.size	_ZN6icu_6711MeasureUnit10createAcreER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createAcreER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getAcreEv
	.type	_ZN6icu_6711MeasureUnit7getAcreEv, @function
_ZN6icu_6711MeasureUnit7getAcreEv:
.LFB2378:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$2, 18(%rdi)
	ret
	.cfi_endproc
.LFE2378:
	.size	_ZN6icu_6711MeasureUnit7getAcreEv, .-_ZN6icu_6711MeasureUnit7getAcreEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11createDunamER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit11createDunamER10UErrorCode, @function
_ZN6icu_6711MeasureUnit11createDunamER10UErrorCode:
.LFB2379:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L204
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L201
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$2, 18(%rax)
.L199:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L201:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L199
	.cfi_endproc
.LFE2379:
	.size	_ZN6icu_6711MeasureUnit11createDunamER10UErrorCode, .-_ZN6icu_6711MeasureUnit11createDunamER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8getDunamEv
	.type	_ZN6icu_6711MeasureUnit8getDunamEv, @function
_ZN6icu_6711MeasureUnit8getDunamEv:
.LFB2380:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$2, 18(%rdi)
	ret
	.cfi_endproc
.LFE2380:
	.size	_ZN6icu_6711MeasureUnit8getDunamEv, .-_ZN6icu_6711MeasureUnit8getDunamEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13createHectareER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit13createHectareER10UErrorCode, @function
_ZN6icu_6711MeasureUnit13createHectareER10UErrorCode:
.LFB2381:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L213
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L210
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$2, 18(%rax)
.L208:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L210:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L208
	.cfi_endproc
.LFE2381:
	.size	_ZN6icu_6711MeasureUnit13createHectareER10UErrorCode, .-_ZN6icu_6711MeasureUnit13createHectareER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10getHectareEv
	.type	_ZN6icu_6711MeasureUnit10getHectareEv, @function
_ZN6icu_6711MeasureUnit10getHectareEv:
.LFB2382:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$2, 18(%rdi)
	ret
	.cfi_endproc
.LFE2382:
	.size	_ZN6icu_6711MeasureUnit10getHectareEv, .-_ZN6icu_6711MeasureUnit10getHectareEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit22createSquareCentimeterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit22createSquareCentimeterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit22createSquareCentimeterER10UErrorCode:
.LFB2383:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L222
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L219
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$2, 18(%rax)
.L217:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L219:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L217
	.cfi_endproc
.LFE2383:
	.size	_ZN6icu_6711MeasureUnit22createSquareCentimeterER10UErrorCode, .-_ZN6icu_6711MeasureUnit22createSquareCentimeterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit19getSquareCentimeterEv
	.type	_ZN6icu_6711MeasureUnit19getSquareCentimeterEv, @function
_ZN6icu_6711MeasureUnit19getSquareCentimeterEv:
.LFB2384:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$2, 18(%rdi)
	ret
	.cfi_endproc
.LFE2384:
	.size	_ZN6icu_6711MeasureUnit19getSquareCentimeterEv, .-_ZN6icu_6711MeasureUnit19getSquareCentimeterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createSquareFootER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createSquareFootER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createSquareFootER10UErrorCode:
.LFB2385:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L231
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L228
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$4, %edx
	movw	%dx, 16(%rax)
	movb	$2, 18(%rax)
.L226:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L228:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L226
	.cfi_endproc
.LFE2385:
	.size	_ZN6icu_6711MeasureUnit16createSquareFootER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createSquareFootER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getSquareFootEv
	.type	_ZN6icu_6711MeasureUnit13getSquareFootEv, @function
_ZN6icu_6711MeasureUnit13getSquareFootEv:
.LFB2386:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$4, %edx
	movw	%dx, 16(%rdi)
	movb	$2, 18(%rdi)
	ret
	.cfi_endproc
.LFE2386:
	.size	_ZN6icu_6711MeasureUnit13getSquareFootEv, .-_ZN6icu_6711MeasureUnit13getSquareFootEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createSquareInchER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createSquareInchER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createSquareInchER10UErrorCode:
.LFB2387:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L240
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L237
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$5, %edx
	movw	%dx, 16(%rax)
	movb	$2, 18(%rax)
.L235:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L237:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L235
	.cfi_endproc
.LFE2387:
	.size	_ZN6icu_6711MeasureUnit16createSquareInchER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createSquareInchER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getSquareInchEv
	.type	_ZN6icu_6711MeasureUnit13getSquareInchEv, @function
_ZN6icu_6711MeasureUnit13getSquareInchEv:
.LFB2388:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$5, %edx
	movw	%dx, 16(%rdi)
	movb	$2, 18(%rdi)
	ret
	.cfi_endproc
.LFE2388:
	.size	_ZN6icu_6711MeasureUnit13getSquareInchEv, .-_ZN6icu_6711MeasureUnit13getSquareInchEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit21createSquareKilometerER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit21createSquareKilometerER10UErrorCode, @function
_ZN6icu_6711MeasureUnit21createSquareKilometerER10UErrorCode:
.LFB2389:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L249
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L246
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$6, %edx
	movw	%dx, 16(%rax)
	movb	$2, 18(%rax)
.L244:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L246:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L244
	.cfi_endproc
.LFE2389:
	.size	_ZN6icu_6711MeasureUnit21createSquareKilometerER10UErrorCode, .-_ZN6icu_6711MeasureUnit21createSquareKilometerER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit18getSquareKilometerEv
	.type	_ZN6icu_6711MeasureUnit18getSquareKilometerEv, @function
_ZN6icu_6711MeasureUnit18getSquareKilometerEv:
.LFB2390:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$6, %edx
	movw	%dx, 16(%rdi)
	movb	$2, 18(%rdi)
	ret
	.cfi_endproc
.LFE2390:
	.size	_ZN6icu_6711MeasureUnit18getSquareKilometerEv, .-_ZN6icu_6711MeasureUnit18getSquareKilometerEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17createSquareMeterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit17createSquareMeterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit17createSquareMeterER10UErrorCode:
.LFB2391:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L258
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L255
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$7, %edx
	movw	%dx, 16(%rax)
	movb	$2, 18(%rax)
.L253:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L255:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L253
	.cfi_endproc
.LFE2391:
	.size	_ZN6icu_6711MeasureUnit17createSquareMeterER10UErrorCode, .-_ZN6icu_6711MeasureUnit17createSquareMeterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14getSquareMeterEv
	.type	_ZN6icu_6711MeasureUnit14getSquareMeterEv, @function
_ZN6icu_6711MeasureUnit14getSquareMeterEv:
.LFB2392:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$7, %edx
	movw	%dx, 16(%rdi)
	movb	$2, 18(%rdi)
	ret
	.cfi_endproc
.LFE2392:
	.size	_ZN6icu_6711MeasureUnit14getSquareMeterEv, .-_ZN6icu_6711MeasureUnit14getSquareMeterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createSquareMileER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createSquareMileER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createSquareMileER10UErrorCode:
.LFB2393:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L267
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L264
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$8, %edx
	movw	%dx, 16(%rax)
	movb	$2, 18(%rax)
.L262:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L264:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L262
	.cfi_endproc
.LFE2393:
	.size	_ZN6icu_6711MeasureUnit16createSquareMileER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createSquareMileER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getSquareMileEv
	.type	_ZN6icu_6711MeasureUnit13getSquareMileEv, @function
_ZN6icu_6711MeasureUnit13getSquareMileEv:
.LFB2394:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$8, %edx
	movw	%dx, 16(%rdi)
	movb	$2, 18(%rdi)
	ret
	.cfi_endproc
.LFE2394:
	.size	_ZN6icu_6711MeasureUnit13getSquareMileEv, .-_ZN6icu_6711MeasureUnit13getSquareMileEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createSquareYardER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createSquareYardER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createSquareYardER10UErrorCode:
.LFB2395:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L276
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L273
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$9, %edx
	movw	%dx, 16(%rax)
	movb	$2, 18(%rax)
.L271:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L273:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L271
	.cfi_endproc
.LFE2395:
	.size	_ZN6icu_6711MeasureUnit16createSquareYardER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createSquareYardER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getSquareYardEv
	.type	_ZN6icu_6711MeasureUnit13getSquareYardEv, @function
_ZN6icu_6711MeasureUnit13getSquareYardEv:
.LFB2396:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$9, %edx
	movw	%dx, 16(%rdi)
	movb	$2, 18(%rdi)
	ret
	.cfi_endproc
.LFE2396:
	.size	_ZN6icu_6711MeasureUnit13getSquareYardEv, .-_ZN6icu_6711MeasureUnit13getSquareYardEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11createKaratER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit11createKaratER10UErrorCode, @function
_ZN6icu_6711MeasureUnit11createKaratER10UErrorCode:
.LFB2397:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L285
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L282
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$3, 18(%rax)
.L280:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L282:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L280
	.cfi_endproc
.LFE2397:
	.size	_ZN6icu_6711MeasureUnit11createKaratER10UErrorCode, .-_ZN6icu_6711MeasureUnit11createKaratER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8getKaratEv
	.type	_ZN6icu_6711MeasureUnit8getKaratEv, @function
_ZN6icu_6711MeasureUnit8getKaratEv:
.LFB2398:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$3, 18(%rdi)
	ret
	.cfi_endproc
.LFE2398:
	.size	_ZN6icu_6711MeasureUnit8getKaratEv, .-_ZN6icu_6711MeasureUnit8getKaratEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit27createMilligramPerDeciliterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit27createMilligramPerDeciliterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit27createMilligramPerDeciliterER10UErrorCode:
.LFB2399:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L294
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L291
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$3, 18(%rax)
.L289:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L291:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L289
	.cfi_endproc
.LFE2399:
	.size	_ZN6icu_6711MeasureUnit27createMilligramPerDeciliterER10UErrorCode, .-_ZN6icu_6711MeasureUnit27createMilligramPerDeciliterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit24getMilligramPerDeciliterEv
	.type	_ZN6icu_6711MeasureUnit24getMilligramPerDeciliterEv, @function
_ZN6icu_6711MeasureUnit24getMilligramPerDeciliterEv:
.LFB2400:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$3, 18(%rdi)
	ret
	.cfi_endproc
.LFE2400:
	.size	_ZN6icu_6711MeasureUnit24getMilligramPerDeciliterEv, .-_ZN6icu_6711MeasureUnit24getMilligramPerDeciliterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit23createMillimolePerLiterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit23createMillimolePerLiterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit23createMillimolePerLiterER10UErrorCode:
.LFB2401:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L303
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L300
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$3, 18(%rax)
.L298:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L300:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L298
	.cfi_endproc
.LFE2401:
	.size	_ZN6icu_6711MeasureUnit23createMillimolePerLiterER10UErrorCode, .-_ZN6icu_6711MeasureUnit23createMillimolePerLiterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit20getMillimolePerLiterEv
	.type	_ZN6icu_6711MeasureUnit20getMillimolePerLiterEv, @function
_ZN6icu_6711MeasureUnit20getMillimolePerLiterEv:
.LFB2402:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$3, 18(%rdi)
	ret
	.cfi_endproc
.LFE2402:
	.size	_ZN6icu_6711MeasureUnit20getMillimolePerLiterEv, .-_ZN6icu_6711MeasureUnit20getMillimolePerLiterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createMoleER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createMoleER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createMoleER10UErrorCode:
.LFB2403:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L312
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L309
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$3, 18(%rax)
.L307:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L309:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L307
	.cfi_endproc
.LFE2403:
	.size	_ZN6icu_6711MeasureUnit10createMoleER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createMoleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getMoleEv
	.type	_ZN6icu_6711MeasureUnit7getMoleEv, @function
_ZN6icu_6711MeasureUnit7getMoleEv:
.LFB2404:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$3, 18(%rdi)
	ret
	.cfi_endproc
.LFE2404:
	.size	_ZN6icu_6711MeasureUnit7getMoleEv, .-_ZN6icu_6711MeasureUnit7getMoleEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit20createPartPerMillionER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit20createPartPerMillionER10UErrorCode, @function
_ZN6icu_6711MeasureUnit20createPartPerMillionER10UErrorCode:
.LFB2405:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L321
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L318
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$6, %edx
	movw	%dx, 16(%rax)
	movb	$3, 18(%rax)
.L316:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L318:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L316
	.cfi_endproc
.LFE2405:
	.size	_ZN6icu_6711MeasureUnit20createPartPerMillionER10UErrorCode, .-_ZN6icu_6711MeasureUnit20createPartPerMillionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17getPartPerMillionEv
	.type	_ZN6icu_6711MeasureUnit17getPartPerMillionEv, @function
_ZN6icu_6711MeasureUnit17getPartPerMillionEv:
.LFB2406:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$6, %edx
	movw	%dx, 16(%rdi)
	movb	$3, 18(%rdi)
	ret
	.cfi_endproc
.LFE2406:
	.size	_ZN6icu_6711MeasureUnit17getPartPerMillionEv, .-_ZN6icu_6711MeasureUnit17getPartPerMillionEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13createPercentER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit13createPercentER10UErrorCode, @function
_ZN6icu_6711MeasureUnit13createPercentER10UErrorCode:
.LFB2407:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L330
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L327
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$4, %edx
	movw	%dx, 16(%rax)
	movb	$3, 18(%rax)
.L325:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L327:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L325
	.cfi_endproc
.LFE2407:
	.size	_ZN6icu_6711MeasureUnit13createPercentER10UErrorCode, .-_ZN6icu_6711MeasureUnit13createPercentER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10getPercentEv
	.type	_ZN6icu_6711MeasureUnit10getPercentEv, @function
_ZN6icu_6711MeasureUnit10getPercentEv:
.LFB2408:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$4, %edx
	movw	%dx, 16(%rdi)
	movb	$3, 18(%rdi)
	ret
	.cfi_endproc
.LFE2408:
	.size	_ZN6icu_6711MeasureUnit10getPercentEv, .-_ZN6icu_6711MeasureUnit10getPercentEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14createPermilleER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit14createPermilleER10UErrorCode, @function
_ZN6icu_6711MeasureUnit14createPermilleER10UErrorCode:
.LFB2409:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L339
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L336
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$5, %edx
	movw	%dx, 16(%rax)
	movb	$3, 18(%rax)
.L334:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L336:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L334
	.cfi_endproc
.LFE2409:
	.size	_ZN6icu_6711MeasureUnit14createPermilleER10UErrorCode, .-_ZN6icu_6711MeasureUnit14createPermilleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11getPermilleEv
	.type	_ZN6icu_6711MeasureUnit11getPermilleEv, @function
_ZN6icu_6711MeasureUnit11getPermilleEv:
.LFB2410:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$5, %edx
	movw	%dx, 16(%rdi)
	movb	$3, 18(%rdi)
	ret
	.cfi_endproc
.LFE2410:
	.size	_ZN6icu_6711MeasureUnit11getPermilleEv, .-_ZN6icu_6711MeasureUnit11getPermilleEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createPermyriadER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createPermyriadER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createPermyriadER10UErrorCode:
.LFB2411:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L348
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L345
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$7, %edx
	movw	%dx, 16(%rax)
	movb	$3, 18(%rax)
.L343:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L345:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L343
	.cfi_endproc
.LFE2411:
	.size	_ZN6icu_6711MeasureUnit15createPermyriadER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createPermyriadER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getPermyriadEv
	.type	_ZN6icu_6711MeasureUnit12getPermyriadEv, @function
_ZN6icu_6711MeasureUnit12getPermyriadEv:
.LFB2412:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$7, %edx
	movw	%dx, 16(%rdi)
	movb	$3, 18(%rdi)
	ret
	.cfi_endproc
.LFE2412:
	.size	_ZN6icu_6711MeasureUnit12getPermyriadEv, .-_ZN6icu_6711MeasureUnit12getPermyriadEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit27createLiterPer100KilometersER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit27createLiterPer100KilometersER10UErrorCode, @function
_ZN6icu_6711MeasureUnit27createLiterPer100KilometersER10UErrorCode:
.LFB2413:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L357
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L354
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$4, 18(%rax)
.L352:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L354:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L352
	.cfi_endproc
.LFE2413:
	.size	_ZN6icu_6711MeasureUnit27createLiterPer100KilometersER10UErrorCode, .-_ZN6icu_6711MeasureUnit27createLiterPer100KilometersER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit24getLiterPer100KilometersEv
	.type	_ZN6icu_6711MeasureUnit24getLiterPer100KilometersEv, @function
_ZN6icu_6711MeasureUnit24getLiterPer100KilometersEv:
.LFB2414:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$4, 18(%rdi)
	ret
	.cfi_endproc
.LFE2414:
	.size	_ZN6icu_6711MeasureUnit24getLiterPer100KilometersEv, .-_ZN6icu_6711MeasureUnit24getLiterPer100KilometersEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit23createLiterPerKilometerER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit23createLiterPerKilometerER10UErrorCode, @function
_ZN6icu_6711MeasureUnit23createLiterPerKilometerER10UErrorCode:
.LFB2415:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L366
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L363
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$4, 18(%rax)
.L361:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L363:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L361
	.cfi_endproc
.LFE2415:
	.size	_ZN6icu_6711MeasureUnit23createLiterPerKilometerER10UErrorCode, .-_ZN6icu_6711MeasureUnit23createLiterPerKilometerER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit20getLiterPerKilometerEv
	.type	_ZN6icu_6711MeasureUnit20getLiterPerKilometerEv, @function
_ZN6icu_6711MeasureUnit20getLiterPerKilometerEv:
.LFB2416:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$4, 18(%rdi)
	ret
	.cfi_endproc
.LFE2416:
	.size	_ZN6icu_6711MeasureUnit20getLiterPerKilometerEv, .-_ZN6icu_6711MeasureUnit20getLiterPerKilometerEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit19createMilePerGallonER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit19createMilePerGallonER10UErrorCode, @function
_ZN6icu_6711MeasureUnit19createMilePerGallonER10UErrorCode:
.LFB2417:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L375
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L372
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$4, 18(%rax)
.L370:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L372:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L370
	.cfi_endproc
.LFE2417:
	.size	_ZN6icu_6711MeasureUnit19createMilePerGallonER10UErrorCode, .-_ZN6icu_6711MeasureUnit19createMilePerGallonER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16getMilePerGallonEv
	.type	_ZN6icu_6711MeasureUnit16getMilePerGallonEv, @function
_ZN6icu_6711MeasureUnit16getMilePerGallonEv:
.LFB2418:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$4, 18(%rdi)
	ret
	.cfi_endproc
.LFE2418:
	.size	_ZN6icu_6711MeasureUnit16getMilePerGallonEv, .-_ZN6icu_6711MeasureUnit16getMilePerGallonEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit27createMilePerGallonImperialER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit27createMilePerGallonImperialER10UErrorCode, @function
_ZN6icu_6711MeasureUnit27createMilePerGallonImperialER10UErrorCode:
.LFB2419:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L384
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L381
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$4, 18(%rax)
.L379:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L381:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L379
	.cfi_endproc
.LFE2419:
	.size	_ZN6icu_6711MeasureUnit27createMilePerGallonImperialER10UErrorCode, .-_ZN6icu_6711MeasureUnit27createMilePerGallonImperialER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit24getMilePerGallonImperialEv
	.type	_ZN6icu_6711MeasureUnit24getMilePerGallonImperialEv, @function
_ZN6icu_6711MeasureUnit24getMilePerGallonImperialEv:
.LFB2420:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$4, 18(%rdi)
	ret
	.cfi_endproc
.LFE2420:
	.size	_ZN6icu_6711MeasureUnit24getMilePerGallonImperialEv, .-_ZN6icu_6711MeasureUnit24getMilePerGallonImperialEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9createBitER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit9createBitER10UErrorCode, @function
_ZN6icu_6711MeasureUnit9createBitER10UErrorCode:
.LFB2421:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L393
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L390
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$6, 18(%rax)
.L388:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L390:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L388
	.cfi_endproc
.LFE2421:
	.size	_ZN6icu_6711MeasureUnit9createBitER10UErrorCode, .-_ZN6icu_6711MeasureUnit9createBitER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit6getBitEv
	.type	_ZN6icu_6711MeasureUnit6getBitEv, @function
_ZN6icu_6711MeasureUnit6getBitEv:
.LFB2422:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$6, 18(%rdi)
	ret
	.cfi_endproc
.LFE2422:
	.size	_ZN6icu_6711MeasureUnit6getBitEv, .-_ZN6icu_6711MeasureUnit6getBitEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createByteER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createByteER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createByteER10UErrorCode:
.LFB2423:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L402
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L399
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$6, 18(%rax)
.L397:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L399:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L397
	.cfi_endproc
.LFE2423:
	.size	_ZN6icu_6711MeasureUnit10createByteER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createByteER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getByteEv
	.type	_ZN6icu_6711MeasureUnit7getByteEv, @function
_ZN6icu_6711MeasureUnit7getByteEv:
.LFB2424:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$6, 18(%rdi)
	ret
	.cfi_endproc
.LFE2424:
	.size	_ZN6icu_6711MeasureUnit7getByteEv, .-_ZN6icu_6711MeasureUnit7getByteEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13createGigabitER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit13createGigabitER10UErrorCode, @function
_ZN6icu_6711MeasureUnit13createGigabitER10UErrorCode:
.LFB2425:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L411
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L408
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$6, 18(%rax)
.L406:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L408:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L406
	.cfi_endproc
.LFE2425:
	.size	_ZN6icu_6711MeasureUnit13createGigabitER10UErrorCode, .-_ZN6icu_6711MeasureUnit13createGigabitER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10getGigabitEv
	.type	_ZN6icu_6711MeasureUnit10getGigabitEv, @function
_ZN6icu_6711MeasureUnit10getGigabitEv:
.LFB2426:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$6, 18(%rdi)
	ret
	.cfi_endproc
.LFE2426:
	.size	_ZN6icu_6711MeasureUnit10getGigabitEv, .-_ZN6icu_6711MeasureUnit10getGigabitEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14createGigabyteER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit14createGigabyteER10UErrorCode, @function
_ZN6icu_6711MeasureUnit14createGigabyteER10UErrorCode:
.LFB2427:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L420
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L417
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$6, 18(%rax)
.L415:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L417:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L415
	.cfi_endproc
.LFE2427:
	.size	_ZN6icu_6711MeasureUnit14createGigabyteER10UErrorCode, .-_ZN6icu_6711MeasureUnit14createGigabyteER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11getGigabyteEv
	.type	_ZN6icu_6711MeasureUnit11getGigabyteEv, @function
_ZN6icu_6711MeasureUnit11getGigabyteEv:
.LFB2428:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$6, 18(%rdi)
	ret
	.cfi_endproc
.LFE2428:
	.size	_ZN6icu_6711MeasureUnit11getGigabyteEv, .-_ZN6icu_6711MeasureUnit11getGigabyteEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13createKilobitER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit13createKilobitER10UErrorCode, @function
_ZN6icu_6711MeasureUnit13createKilobitER10UErrorCode:
.LFB2429:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L429
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L426
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$4, %edx
	movw	%dx, 16(%rax)
	movb	$6, 18(%rax)
.L424:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L426:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L424
	.cfi_endproc
.LFE2429:
	.size	_ZN6icu_6711MeasureUnit13createKilobitER10UErrorCode, .-_ZN6icu_6711MeasureUnit13createKilobitER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10getKilobitEv
	.type	_ZN6icu_6711MeasureUnit10getKilobitEv, @function
_ZN6icu_6711MeasureUnit10getKilobitEv:
.LFB2430:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$4, %edx
	movw	%dx, 16(%rdi)
	movb	$6, 18(%rdi)
	ret
	.cfi_endproc
.LFE2430:
	.size	_ZN6icu_6711MeasureUnit10getKilobitEv, .-_ZN6icu_6711MeasureUnit10getKilobitEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14createKilobyteER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit14createKilobyteER10UErrorCode, @function
_ZN6icu_6711MeasureUnit14createKilobyteER10UErrorCode:
.LFB2431:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L438
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L435
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$5, %edx
	movw	%dx, 16(%rax)
	movb	$6, 18(%rax)
.L433:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L435:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L433
	.cfi_endproc
.LFE2431:
	.size	_ZN6icu_6711MeasureUnit14createKilobyteER10UErrorCode, .-_ZN6icu_6711MeasureUnit14createKilobyteER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11getKilobyteEv
	.type	_ZN6icu_6711MeasureUnit11getKilobyteEv, @function
_ZN6icu_6711MeasureUnit11getKilobyteEv:
.LFB2432:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$5, %edx
	movw	%dx, 16(%rdi)
	movb	$6, 18(%rdi)
	ret
	.cfi_endproc
.LFE2432:
	.size	_ZN6icu_6711MeasureUnit11getKilobyteEv, .-_ZN6icu_6711MeasureUnit11getKilobyteEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13createMegabitER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit13createMegabitER10UErrorCode, @function
_ZN6icu_6711MeasureUnit13createMegabitER10UErrorCode:
.LFB2433:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L447
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L444
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$6, %edx
	movw	%dx, 16(%rax)
	movb	$6, 18(%rax)
.L442:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L444:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L442
	.cfi_endproc
.LFE2433:
	.size	_ZN6icu_6711MeasureUnit13createMegabitER10UErrorCode, .-_ZN6icu_6711MeasureUnit13createMegabitER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10getMegabitEv
	.type	_ZN6icu_6711MeasureUnit10getMegabitEv, @function
_ZN6icu_6711MeasureUnit10getMegabitEv:
.LFB2434:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$6, %edx
	movw	%dx, 16(%rdi)
	movb	$6, 18(%rdi)
	ret
	.cfi_endproc
.LFE2434:
	.size	_ZN6icu_6711MeasureUnit10getMegabitEv, .-_ZN6icu_6711MeasureUnit10getMegabitEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14createMegabyteER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit14createMegabyteER10UErrorCode, @function
_ZN6icu_6711MeasureUnit14createMegabyteER10UErrorCode:
.LFB2435:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L456
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L453
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$7, %edx
	movw	%dx, 16(%rax)
	movb	$6, 18(%rax)
.L451:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L453:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L451
	.cfi_endproc
.LFE2435:
	.size	_ZN6icu_6711MeasureUnit14createMegabyteER10UErrorCode, .-_ZN6icu_6711MeasureUnit14createMegabyteER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11getMegabyteEv
	.type	_ZN6icu_6711MeasureUnit11getMegabyteEv, @function
_ZN6icu_6711MeasureUnit11getMegabyteEv:
.LFB2436:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$7, %edx
	movw	%dx, 16(%rdi)
	movb	$6, 18(%rdi)
	ret
	.cfi_endproc
.LFE2436:
	.size	_ZN6icu_6711MeasureUnit11getMegabyteEv, .-_ZN6icu_6711MeasureUnit11getMegabyteEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14createPetabyteER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit14createPetabyteER10UErrorCode, @function
_ZN6icu_6711MeasureUnit14createPetabyteER10UErrorCode:
.LFB2437:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L465
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L462
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$8, %edx
	movw	%dx, 16(%rax)
	movb	$6, 18(%rax)
.L460:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L465:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L462:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L460
	.cfi_endproc
.LFE2437:
	.size	_ZN6icu_6711MeasureUnit14createPetabyteER10UErrorCode, .-_ZN6icu_6711MeasureUnit14createPetabyteER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11getPetabyteEv
	.type	_ZN6icu_6711MeasureUnit11getPetabyteEv, @function
_ZN6icu_6711MeasureUnit11getPetabyteEv:
.LFB2438:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$8, %edx
	movw	%dx, 16(%rdi)
	movb	$6, 18(%rdi)
	ret
	.cfi_endproc
.LFE2438:
	.size	_ZN6icu_6711MeasureUnit11getPetabyteEv, .-_ZN6icu_6711MeasureUnit11getPetabyteEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13createTerabitER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit13createTerabitER10UErrorCode, @function
_ZN6icu_6711MeasureUnit13createTerabitER10UErrorCode:
.LFB2439:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L474
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L471
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$9, %edx
	movw	%dx, 16(%rax)
	movb	$6, 18(%rax)
.L469:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L471:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L469
	.cfi_endproc
.LFE2439:
	.size	_ZN6icu_6711MeasureUnit13createTerabitER10UErrorCode, .-_ZN6icu_6711MeasureUnit13createTerabitER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10getTerabitEv
	.type	_ZN6icu_6711MeasureUnit10getTerabitEv, @function
_ZN6icu_6711MeasureUnit10getTerabitEv:
.LFB2440:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$9, %edx
	movw	%dx, 16(%rdi)
	movb	$6, 18(%rdi)
	ret
	.cfi_endproc
.LFE2440:
	.size	_ZN6icu_6711MeasureUnit10getTerabitEv, .-_ZN6icu_6711MeasureUnit10getTerabitEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14createTerabyteER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit14createTerabyteER10UErrorCode, @function
_ZN6icu_6711MeasureUnit14createTerabyteER10UErrorCode:
.LFB2441:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L483
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L480
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$10, %edx
	movw	%dx, 16(%rax)
	movb	$6, 18(%rax)
.L478:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L480:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L478
	.cfi_endproc
.LFE2441:
	.size	_ZN6icu_6711MeasureUnit14createTerabyteER10UErrorCode, .-_ZN6icu_6711MeasureUnit14createTerabyteER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11getTerabyteEv
	.type	_ZN6icu_6711MeasureUnit11getTerabyteEv, @function
_ZN6icu_6711MeasureUnit11getTerabyteEv:
.LFB2442:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$10, %edx
	movw	%dx, 16(%rdi)
	movb	$6, 18(%rdi)
	ret
	.cfi_endproc
.LFE2442:
	.size	_ZN6icu_6711MeasureUnit11getTerabyteEv, .-_ZN6icu_6711MeasureUnit11getTerabyteEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13createCenturyER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit13createCenturyER10UErrorCode, @function
_ZN6icu_6711MeasureUnit13createCenturyER10UErrorCode:
.LFB2443:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L492
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L489
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L487:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L489:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L487
	.cfi_endproc
.LFE2443:
	.size	_ZN6icu_6711MeasureUnit13createCenturyER10UErrorCode, .-_ZN6icu_6711MeasureUnit13createCenturyER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10getCenturyEv
	.type	_ZN6icu_6711MeasureUnit10getCenturyEv, @function
_ZN6icu_6711MeasureUnit10getCenturyEv:
.LFB2444:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2444:
	.size	_ZN6icu_6711MeasureUnit10getCenturyEv, .-_ZN6icu_6711MeasureUnit10getCenturyEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9createDayER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit9createDayER10UErrorCode, @function
_ZN6icu_6711MeasureUnit9createDayER10UErrorCode:
.LFB2445:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L501
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L498
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L496:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L498:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L496
	.cfi_endproc
.LFE2445:
	.size	_ZN6icu_6711MeasureUnit9createDayER10UErrorCode, .-_ZN6icu_6711MeasureUnit9createDayER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit6getDayEv
	.type	_ZN6icu_6711MeasureUnit6getDayEv, @function
_ZN6icu_6711MeasureUnit6getDayEv:
.LFB2446:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2446:
	.size	_ZN6icu_6711MeasureUnit6getDayEv, .-_ZN6icu_6711MeasureUnit6getDayEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createDayPersonER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createDayPersonER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createDayPersonER10UErrorCode:
.LFB2447:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L510
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L507
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L505:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L507:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L505
	.cfi_endproc
.LFE2447:
	.size	_ZN6icu_6711MeasureUnit15createDayPersonER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createDayPersonER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getDayPersonEv
	.type	_ZN6icu_6711MeasureUnit12getDayPersonEv, @function
_ZN6icu_6711MeasureUnit12getDayPersonEv:
.LFB2448:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2448:
	.size	_ZN6icu_6711MeasureUnit12getDayPersonEv, .-_ZN6icu_6711MeasureUnit12getDayPersonEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createDecadeER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createDecadeER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createDecadeER10UErrorCode:
.LFB2449:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L519
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L516
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L514:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L519:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L516:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L514
	.cfi_endproc
.LFE2449:
	.size	_ZN6icu_6711MeasureUnit12createDecadeER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createDecadeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getDecadeEv
	.type	_ZN6icu_6711MeasureUnit9getDecadeEv, @function
_ZN6icu_6711MeasureUnit9getDecadeEv:
.LFB2450:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2450:
	.size	_ZN6icu_6711MeasureUnit9getDecadeEv, .-_ZN6icu_6711MeasureUnit9getDecadeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createHourER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createHourER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createHourER10UErrorCode:
.LFB2451:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L528
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L525
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$4, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L523:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L525:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L523
	.cfi_endproc
.LFE2451:
	.size	_ZN6icu_6711MeasureUnit10createHourER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createHourER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getHourEv
	.type	_ZN6icu_6711MeasureUnit7getHourEv, @function
_ZN6icu_6711MeasureUnit7getHourEv:
.LFB2452:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$4, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2452:
	.size	_ZN6icu_6711MeasureUnit7getHourEv, .-_ZN6icu_6711MeasureUnit7getHourEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17createMicrosecondER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit17createMicrosecondER10UErrorCode, @function
_ZN6icu_6711MeasureUnit17createMicrosecondER10UErrorCode:
.LFB2453:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L537
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L534
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$5, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L532:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L534:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L532
	.cfi_endproc
.LFE2453:
	.size	_ZN6icu_6711MeasureUnit17createMicrosecondER10UErrorCode, .-_ZN6icu_6711MeasureUnit17createMicrosecondER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14getMicrosecondEv
	.type	_ZN6icu_6711MeasureUnit14getMicrosecondEv, @function
_ZN6icu_6711MeasureUnit14getMicrosecondEv:
.LFB2454:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$5, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2454:
	.size	_ZN6icu_6711MeasureUnit14getMicrosecondEv, .-_ZN6icu_6711MeasureUnit14getMicrosecondEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17createMillisecondER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit17createMillisecondER10UErrorCode, @function
_ZN6icu_6711MeasureUnit17createMillisecondER10UErrorCode:
.LFB2455:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L546
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L543
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$6, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L541:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L543:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L541
	.cfi_endproc
.LFE2455:
	.size	_ZN6icu_6711MeasureUnit17createMillisecondER10UErrorCode, .-_ZN6icu_6711MeasureUnit17createMillisecondER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14getMillisecondEv
	.type	_ZN6icu_6711MeasureUnit14getMillisecondEv, @function
_ZN6icu_6711MeasureUnit14getMillisecondEv:
.LFB2456:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$6, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2456:
	.size	_ZN6icu_6711MeasureUnit14getMillisecondEv, .-_ZN6icu_6711MeasureUnit14getMillisecondEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createMinuteER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createMinuteER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createMinuteER10UErrorCode:
.LFB2457:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L555
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L552
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$7, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L550:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L555:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L552:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L550
	.cfi_endproc
.LFE2457:
	.size	_ZN6icu_6711MeasureUnit12createMinuteER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createMinuteER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getMinuteEv
	.type	_ZN6icu_6711MeasureUnit9getMinuteEv, @function
_ZN6icu_6711MeasureUnit9getMinuteEv:
.LFB2458:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$7, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2458:
	.size	_ZN6icu_6711MeasureUnit9getMinuteEv, .-_ZN6icu_6711MeasureUnit9getMinuteEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11createMonthER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit11createMonthER10UErrorCode, @function
_ZN6icu_6711MeasureUnit11createMonthER10UErrorCode:
.LFB2459:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L564
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L561
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$8, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L559:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L561:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L559
	.cfi_endproc
.LFE2459:
	.size	_ZN6icu_6711MeasureUnit11createMonthER10UErrorCode, .-_ZN6icu_6711MeasureUnit11createMonthER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8getMonthEv
	.type	_ZN6icu_6711MeasureUnit8getMonthEv, @function
_ZN6icu_6711MeasureUnit8getMonthEv:
.LFB2460:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$8, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2460:
	.size	_ZN6icu_6711MeasureUnit8getMonthEv, .-_ZN6icu_6711MeasureUnit8getMonthEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17createMonthPersonER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit17createMonthPersonER10UErrorCode, @function
_ZN6icu_6711MeasureUnit17createMonthPersonER10UErrorCode:
.LFB2461:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L573
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L570
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$9, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L568:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L570:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L568
	.cfi_endproc
.LFE2461:
	.size	_ZN6icu_6711MeasureUnit17createMonthPersonER10UErrorCode, .-_ZN6icu_6711MeasureUnit17createMonthPersonER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14getMonthPersonEv
	.type	_ZN6icu_6711MeasureUnit14getMonthPersonEv, @function
_ZN6icu_6711MeasureUnit14getMonthPersonEv:
.LFB2462:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$9, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2462:
	.size	_ZN6icu_6711MeasureUnit14getMonthPersonEv, .-_ZN6icu_6711MeasureUnit14getMonthPersonEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createNanosecondER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createNanosecondER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createNanosecondER10UErrorCode:
.LFB2463:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L582
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L579
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$10, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L577:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L582:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L579:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L577
	.cfi_endproc
.LFE2463:
	.size	_ZN6icu_6711MeasureUnit16createNanosecondER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createNanosecondER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getNanosecondEv
	.type	_ZN6icu_6711MeasureUnit13getNanosecondEv, @function
_ZN6icu_6711MeasureUnit13getNanosecondEv:
.LFB2464:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$10, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2464:
	.size	_ZN6icu_6711MeasureUnit13getNanosecondEv, .-_ZN6icu_6711MeasureUnit13getNanosecondEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createSecondER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createSecondER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createSecondER10UErrorCode:
.LFB2465:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L591
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L588
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$11, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L586:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L588:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L586
	.cfi_endproc
.LFE2465:
	.size	_ZN6icu_6711MeasureUnit12createSecondER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createSecondER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getSecondEv
	.type	_ZN6icu_6711MeasureUnit9getSecondEv, @function
_ZN6icu_6711MeasureUnit9getSecondEv:
.LFB2466:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$11, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2466:
	.size	_ZN6icu_6711MeasureUnit9getSecondEv, .-_ZN6icu_6711MeasureUnit9getSecondEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createWeekER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createWeekER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createWeekER10UErrorCode:
.LFB2467:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L600
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L597
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$12, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L595:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L600:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L597:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L595
	.cfi_endproc
.LFE2467:
	.size	_ZN6icu_6711MeasureUnit10createWeekER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createWeekER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getWeekEv
	.type	_ZN6icu_6711MeasureUnit7getWeekEv, @function
_ZN6icu_6711MeasureUnit7getWeekEv:
.LFB2468:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$12, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2468:
	.size	_ZN6icu_6711MeasureUnit7getWeekEv, .-_ZN6icu_6711MeasureUnit7getWeekEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createWeekPersonER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createWeekPersonER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createWeekPersonER10UErrorCode:
.LFB2469:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L609
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L606
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$13, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L604:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L609:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L606:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L604
	.cfi_endproc
.LFE2469:
	.size	_ZN6icu_6711MeasureUnit16createWeekPersonER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createWeekPersonER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getWeekPersonEv
	.type	_ZN6icu_6711MeasureUnit13getWeekPersonEv, @function
_ZN6icu_6711MeasureUnit13getWeekPersonEv:
.LFB2470:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$13, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2470:
	.size	_ZN6icu_6711MeasureUnit13getWeekPersonEv, .-_ZN6icu_6711MeasureUnit13getWeekPersonEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createYearER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createYearER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createYearER10UErrorCode:
.LFB2471:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L618
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L615
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$14, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L613:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L618:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L615:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L613
	.cfi_endproc
.LFE2471:
	.size	_ZN6icu_6711MeasureUnit10createYearER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createYearER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getYearEv
	.type	_ZN6icu_6711MeasureUnit7getYearEv, @function
_ZN6icu_6711MeasureUnit7getYearEv:
.LFB2472:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$14, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2472:
	.size	_ZN6icu_6711MeasureUnit7getYearEv, .-_ZN6icu_6711MeasureUnit7getYearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createYearPersonER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createYearPersonER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createYearPersonER10UErrorCode:
.LFB2473:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L627
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L624
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$15, %edx
	movw	%dx, 16(%rax)
	movb	$7, 18(%rax)
.L622:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L627:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L624:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L622
	.cfi_endproc
.LFE2473:
	.size	_ZN6icu_6711MeasureUnit16createYearPersonER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createYearPersonER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getYearPersonEv
	.type	_ZN6icu_6711MeasureUnit13getYearPersonEv, @function
_ZN6icu_6711MeasureUnit13getYearPersonEv:
.LFB2474:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$15, %edx
	movw	%dx, 16(%rdi)
	movb	$7, 18(%rdi)
	ret
	.cfi_endproc
.LFE2474:
	.size	_ZN6icu_6711MeasureUnit13getYearPersonEv, .-_ZN6icu_6711MeasureUnit13getYearPersonEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createAmpereER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createAmpereER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createAmpereER10UErrorCode:
.LFB2475:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L636
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L633
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$8, 18(%rax)
.L631:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L636:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L633:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L631
	.cfi_endproc
.LFE2475:
	.size	_ZN6icu_6711MeasureUnit12createAmpereER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createAmpereER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getAmpereEv
	.type	_ZN6icu_6711MeasureUnit9getAmpereEv, @function
_ZN6icu_6711MeasureUnit9getAmpereEv:
.LFB2476:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$8, 18(%rdi)
	ret
	.cfi_endproc
.LFE2476:
	.size	_ZN6icu_6711MeasureUnit9getAmpereEv, .-_ZN6icu_6711MeasureUnit9getAmpereEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17createMilliampereER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit17createMilliampereER10UErrorCode, @function
_ZN6icu_6711MeasureUnit17createMilliampereER10UErrorCode:
.LFB2477:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L645
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L642
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$8, 18(%rax)
.L640:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L642:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L640
	.cfi_endproc
.LFE2477:
	.size	_ZN6icu_6711MeasureUnit17createMilliampereER10UErrorCode, .-_ZN6icu_6711MeasureUnit17createMilliampereER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14getMilliampereEv
	.type	_ZN6icu_6711MeasureUnit14getMilliampereEv, @function
_ZN6icu_6711MeasureUnit14getMilliampereEv:
.LFB2478:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$8, 18(%rdi)
	ret
	.cfi_endproc
.LFE2478:
	.size	_ZN6icu_6711MeasureUnit14getMilliampereEv, .-_ZN6icu_6711MeasureUnit14getMilliampereEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9createOhmER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit9createOhmER10UErrorCode, @function
_ZN6icu_6711MeasureUnit9createOhmER10UErrorCode:
.LFB2479:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L654
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L651
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$8, 18(%rax)
.L649:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L654:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L651:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L649
	.cfi_endproc
.LFE2479:
	.size	_ZN6icu_6711MeasureUnit9createOhmER10UErrorCode, .-_ZN6icu_6711MeasureUnit9createOhmER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit6getOhmEv
	.type	_ZN6icu_6711MeasureUnit6getOhmEv, @function
_ZN6icu_6711MeasureUnit6getOhmEv:
.LFB2480:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$8, 18(%rdi)
	ret
	.cfi_endproc
.LFE2480:
	.size	_ZN6icu_6711MeasureUnit6getOhmEv, .-_ZN6icu_6711MeasureUnit6getOhmEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createVoltER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createVoltER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createVoltER10UErrorCode:
.LFB2481:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L663
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L660
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$8, 18(%rax)
.L658:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L663:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L660:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L658
	.cfi_endproc
.LFE2481:
	.size	_ZN6icu_6711MeasureUnit10createVoltER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createVoltER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getVoltEv
	.type	_ZN6icu_6711MeasureUnit7getVoltEv, @function
_ZN6icu_6711MeasureUnit7getVoltEv:
.LFB2482:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$8, 18(%rdi)
	ret
	.cfi_endproc
.LFE2482:
	.size	_ZN6icu_6711MeasureUnit7getVoltEv, .-_ZN6icu_6711MeasureUnit7getVoltEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit24createBritishThermalUnitER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit24createBritishThermalUnitER10UErrorCode, @function
_ZN6icu_6711MeasureUnit24createBritishThermalUnitER10UErrorCode:
.LFB2483:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L672
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L669
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$9, 18(%rax)
.L667:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L672:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L669:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L667
	.cfi_endproc
.LFE2483:
	.size	_ZN6icu_6711MeasureUnit24createBritishThermalUnitER10UErrorCode, .-_ZN6icu_6711MeasureUnit24createBritishThermalUnitER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit21getBritishThermalUnitEv
	.type	_ZN6icu_6711MeasureUnit21getBritishThermalUnitEv, @function
_ZN6icu_6711MeasureUnit21getBritishThermalUnitEv:
.LFB2484:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$9, 18(%rdi)
	ret
	.cfi_endproc
.LFE2484:
	.size	_ZN6icu_6711MeasureUnit21getBritishThermalUnitEv, .-_ZN6icu_6711MeasureUnit21getBritishThermalUnitEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13createCalorieER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit13createCalorieER10UErrorCode, @function
_ZN6icu_6711MeasureUnit13createCalorieER10UErrorCode:
.LFB2485:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L681
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L678
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$9, 18(%rax)
.L676:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L681:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L678:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L676
	.cfi_endproc
.LFE2485:
	.size	_ZN6icu_6711MeasureUnit13createCalorieER10UErrorCode, .-_ZN6icu_6711MeasureUnit13createCalorieER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10getCalorieEv
	.type	_ZN6icu_6711MeasureUnit10getCalorieEv, @function
_ZN6icu_6711MeasureUnit10getCalorieEv:
.LFB2486:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$9, 18(%rdi)
	ret
	.cfi_endproc
.LFE2486:
	.size	_ZN6icu_6711MeasureUnit10getCalorieEv, .-_ZN6icu_6711MeasureUnit10getCalorieEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit18createElectronvoltER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit18createElectronvoltER10UErrorCode, @function
_ZN6icu_6711MeasureUnit18createElectronvoltER10UErrorCode:
.LFB2487:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L690
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L687
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$9, 18(%rax)
.L685:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L690:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L687:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L685
	.cfi_endproc
.LFE2487:
	.size	_ZN6icu_6711MeasureUnit18createElectronvoltER10UErrorCode, .-_ZN6icu_6711MeasureUnit18createElectronvoltER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15getElectronvoltEv
	.type	_ZN6icu_6711MeasureUnit15getElectronvoltEv, @function
_ZN6icu_6711MeasureUnit15getElectronvoltEv:
.LFB2488:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$9, 18(%rdi)
	ret
	.cfi_endproc
.LFE2488:
	.size	_ZN6icu_6711MeasureUnit15getElectronvoltEv, .-_ZN6icu_6711MeasureUnit15getElectronvoltEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17createFoodcalorieER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit17createFoodcalorieER10UErrorCode, @function
_ZN6icu_6711MeasureUnit17createFoodcalorieER10UErrorCode:
.LFB2489:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L699
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L696
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$9, 18(%rax)
.L694:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L699:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L696:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L694
	.cfi_endproc
.LFE2489:
	.size	_ZN6icu_6711MeasureUnit17createFoodcalorieER10UErrorCode, .-_ZN6icu_6711MeasureUnit17createFoodcalorieER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14getFoodcalorieEv
	.type	_ZN6icu_6711MeasureUnit14getFoodcalorieEv, @function
_ZN6icu_6711MeasureUnit14getFoodcalorieEv:
.LFB2490:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$9, 18(%rdi)
	ret
	.cfi_endproc
.LFE2490:
	.size	_ZN6icu_6711MeasureUnit14getFoodcalorieEv, .-_ZN6icu_6711MeasureUnit14getFoodcalorieEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11createJouleER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit11createJouleER10UErrorCode, @function
_ZN6icu_6711MeasureUnit11createJouleER10UErrorCode:
.LFB2491:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L708
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L705
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$4, %edx
	movw	%dx, 16(%rax)
	movb	$9, 18(%rax)
.L703:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L708:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L705:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L703
	.cfi_endproc
.LFE2491:
	.size	_ZN6icu_6711MeasureUnit11createJouleER10UErrorCode, .-_ZN6icu_6711MeasureUnit11createJouleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8getJouleEv
	.type	_ZN6icu_6711MeasureUnit8getJouleEv, @function
_ZN6icu_6711MeasureUnit8getJouleEv:
.LFB2492:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$4, %edx
	movw	%dx, 16(%rdi)
	movb	$9, 18(%rdi)
	ret
	.cfi_endproc
.LFE2492:
	.size	_ZN6icu_6711MeasureUnit8getJouleEv, .-_ZN6icu_6711MeasureUnit8getJouleEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17createKilocalorieER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit17createKilocalorieER10UErrorCode, @function
_ZN6icu_6711MeasureUnit17createKilocalorieER10UErrorCode:
.LFB2493:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L717
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L714
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$5, %edx
	movw	%dx, 16(%rax)
	movb	$9, 18(%rax)
.L712:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L714:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L712
	.cfi_endproc
.LFE2493:
	.size	_ZN6icu_6711MeasureUnit17createKilocalorieER10UErrorCode, .-_ZN6icu_6711MeasureUnit17createKilocalorieER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14getKilocalorieEv
	.type	_ZN6icu_6711MeasureUnit14getKilocalorieEv, @function
_ZN6icu_6711MeasureUnit14getKilocalorieEv:
.LFB2494:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$5, %edx
	movw	%dx, 16(%rdi)
	movb	$9, 18(%rdi)
	ret
	.cfi_endproc
.LFE2494:
	.size	_ZN6icu_6711MeasureUnit14getKilocalorieEv, .-_ZN6icu_6711MeasureUnit14getKilocalorieEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createKilojouleER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createKilojouleER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createKilojouleER10UErrorCode:
.LFB2495:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L726
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L723
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$6, %edx
	movw	%dx, 16(%rax)
	movb	$9, 18(%rax)
.L721:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L726:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L723:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L721
	.cfi_endproc
.LFE2495:
	.size	_ZN6icu_6711MeasureUnit15createKilojouleER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createKilojouleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getKilojouleEv
	.type	_ZN6icu_6711MeasureUnit12getKilojouleEv, @function
_ZN6icu_6711MeasureUnit12getKilojouleEv:
.LFB2496:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$6, %edx
	movw	%dx, 16(%rdi)
	movb	$9, 18(%rdi)
	ret
	.cfi_endproc
.LFE2496:
	.size	_ZN6icu_6711MeasureUnit12getKilojouleEv, .-_ZN6icu_6711MeasureUnit12getKilojouleEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit18createKilowattHourER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit18createKilowattHourER10UErrorCode, @function
_ZN6icu_6711MeasureUnit18createKilowattHourER10UErrorCode:
.LFB2497:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L735
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L732
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$7, %edx
	movw	%dx, 16(%rax)
	movb	$9, 18(%rax)
.L730:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L735:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L732:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L730
	.cfi_endproc
.LFE2497:
	.size	_ZN6icu_6711MeasureUnit18createKilowattHourER10UErrorCode, .-_ZN6icu_6711MeasureUnit18createKilowattHourER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15getKilowattHourEv
	.type	_ZN6icu_6711MeasureUnit15getKilowattHourEv, @function
_ZN6icu_6711MeasureUnit15getKilowattHourEv:
.LFB2498:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$7, %edx
	movw	%dx, 16(%rdi)
	movb	$9, 18(%rdi)
	ret
	.cfi_endproc
.LFE2498:
	.size	_ZN6icu_6711MeasureUnit15getKilowattHourEv, .-_ZN6icu_6711MeasureUnit15getKilowattHourEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13createThermUsER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit13createThermUsER10UErrorCode, @function
_ZN6icu_6711MeasureUnit13createThermUsER10UErrorCode:
.LFB2499:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L744
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L741
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$8, %edx
	movw	%dx, 16(%rax)
	movb	$9, 18(%rax)
.L739:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L744:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L741:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L739
	.cfi_endproc
.LFE2499:
	.size	_ZN6icu_6711MeasureUnit13createThermUsER10UErrorCode, .-_ZN6icu_6711MeasureUnit13createThermUsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10getThermUsEv
	.type	_ZN6icu_6711MeasureUnit10getThermUsEv, @function
_ZN6icu_6711MeasureUnit10getThermUsEv:
.LFB2500:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$8, %edx
	movw	%dx, 16(%rdi)
	movb	$9, 18(%rdi)
	ret
	.cfi_endproc
.LFE2500:
	.size	_ZN6icu_6711MeasureUnit10getThermUsEv, .-_ZN6icu_6711MeasureUnit10getThermUsEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createNewtonER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createNewtonER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createNewtonER10UErrorCode:
.LFB2501:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L753
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L750
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$10, 18(%rax)
.L748:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L753:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L750:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L748
	.cfi_endproc
.LFE2501:
	.size	_ZN6icu_6711MeasureUnit12createNewtonER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createNewtonER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getNewtonEv
	.type	_ZN6icu_6711MeasureUnit9getNewtonEv, @function
_ZN6icu_6711MeasureUnit9getNewtonEv:
.LFB2502:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$10, 18(%rdi)
	ret
	.cfi_endproc
.LFE2502:
	.size	_ZN6icu_6711MeasureUnit9getNewtonEv, .-_ZN6icu_6711MeasureUnit9getNewtonEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createPoundForceER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createPoundForceER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createPoundForceER10UErrorCode:
.LFB2503:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L762
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L759
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$10, 18(%rax)
.L757:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L762:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L759:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L757
	.cfi_endproc
.LFE2503:
	.size	_ZN6icu_6711MeasureUnit16createPoundForceER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createPoundForceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getPoundForceEv
	.type	_ZN6icu_6711MeasureUnit13getPoundForceEv, @function
_ZN6icu_6711MeasureUnit13getPoundForceEv:
.LFB2504:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$10, 18(%rdi)
	ret
	.cfi_endproc
.LFE2504:
	.size	_ZN6icu_6711MeasureUnit13getPoundForceEv, .-_ZN6icu_6711MeasureUnit13getPoundForceEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createGigahertzER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createGigahertzER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createGigahertzER10UErrorCode:
.LFB2505:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L771
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L768
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$11, 18(%rax)
.L766:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L771:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L768:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L766
	.cfi_endproc
.LFE2505:
	.size	_ZN6icu_6711MeasureUnit15createGigahertzER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createGigahertzER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getGigahertzEv
	.type	_ZN6icu_6711MeasureUnit12getGigahertzEv, @function
_ZN6icu_6711MeasureUnit12getGigahertzEv:
.LFB2506:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$11, 18(%rdi)
	ret
	.cfi_endproc
.LFE2506:
	.size	_ZN6icu_6711MeasureUnit12getGigahertzEv, .-_ZN6icu_6711MeasureUnit12getGigahertzEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11createHertzER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit11createHertzER10UErrorCode, @function
_ZN6icu_6711MeasureUnit11createHertzER10UErrorCode:
.LFB2507:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L780
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L777
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$11, 18(%rax)
.L775:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L780:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L777:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L775
	.cfi_endproc
.LFE2507:
	.size	_ZN6icu_6711MeasureUnit11createHertzER10UErrorCode, .-_ZN6icu_6711MeasureUnit11createHertzER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8getHertzEv
	.type	_ZN6icu_6711MeasureUnit8getHertzEv, @function
_ZN6icu_6711MeasureUnit8getHertzEv:
.LFB2508:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$11, 18(%rdi)
	ret
	.cfi_endproc
.LFE2508:
	.size	_ZN6icu_6711MeasureUnit8getHertzEv, .-_ZN6icu_6711MeasureUnit8getHertzEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createKilohertzER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createKilohertzER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createKilohertzER10UErrorCode:
.LFB2509:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L789
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L786
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$11, 18(%rax)
.L784:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L789:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L786:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L784
	.cfi_endproc
.LFE2509:
	.size	_ZN6icu_6711MeasureUnit15createKilohertzER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createKilohertzER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getKilohertzEv
	.type	_ZN6icu_6711MeasureUnit12getKilohertzEv, @function
_ZN6icu_6711MeasureUnit12getKilohertzEv:
.LFB2510:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$11, 18(%rdi)
	ret
	.cfi_endproc
.LFE2510:
	.size	_ZN6icu_6711MeasureUnit12getKilohertzEv, .-_ZN6icu_6711MeasureUnit12getKilohertzEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createMegahertzER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createMegahertzER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createMegahertzER10UErrorCode:
.LFB2511:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L798
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L795
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$11, 18(%rax)
.L793:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L798:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L795:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L793
	.cfi_endproc
.LFE2511:
	.size	_ZN6icu_6711MeasureUnit15createMegahertzER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createMegahertzER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getMegahertzEv
	.type	_ZN6icu_6711MeasureUnit12getMegahertzEv, @function
_ZN6icu_6711MeasureUnit12getMegahertzEv:
.LFB2512:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$11, 18(%rdi)
	ret
	.cfi_endproc
.LFE2512:
	.size	_ZN6icu_6711MeasureUnit12getMegahertzEv, .-_ZN6icu_6711MeasureUnit12getMegahertzEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit22createDotPerCentimeterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit22createDotPerCentimeterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit22createDotPerCentimeterER10UErrorCode:
.LFB2513:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L807
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L804
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$12, 18(%rax)
.L802:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L807:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L804:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L802
	.cfi_endproc
.LFE2513:
	.size	_ZN6icu_6711MeasureUnit22createDotPerCentimeterER10UErrorCode, .-_ZN6icu_6711MeasureUnit22createDotPerCentimeterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit19getDotPerCentimeterEv
	.type	_ZN6icu_6711MeasureUnit19getDotPerCentimeterEv, @function
_ZN6icu_6711MeasureUnit19getDotPerCentimeterEv:
.LFB2514:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$12, 18(%rdi)
	ret
	.cfi_endproc
.LFE2514:
	.size	_ZN6icu_6711MeasureUnit19getDotPerCentimeterEv, .-_ZN6icu_6711MeasureUnit19getDotPerCentimeterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createDotPerInchER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createDotPerInchER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createDotPerInchER10UErrorCode:
.LFB2515:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L816
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L813
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$12, 18(%rax)
.L811:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L816:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L813:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L811
	.cfi_endproc
.LFE2515:
	.size	_ZN6icu_6711MeasureUnit16createDotPerInchER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createDotPerInchER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getDotPerInchEv
	.type	_ZN6icu_6711MeasureUnit13getDotPerInchEv, @function
_ZN6icu_6711MeasureUnit13getDotPerInchEv:
.LFB2516:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$12, 18(%rdi)
	ret
	.cfi_endproc
.LFE2516:
	.size	_ZN6icu_6711MeasureUnit13getDotPerInchEv, .-_ZN6icu_6711MeasureUnit13getDotPerInchEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8createEmER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit8createEmER10UErrorCode, @function
_ZN6icu_6711MeasureUnit8createEmER10UErrorCode:
.LFB2517:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L825
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L822
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$12, 18(%rax)
.L820:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L825:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L822:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L820
	.cfi_endproc
.LFE2517:
	.size	_ZN6icu_6711MeasureUnit8createEmER10UErrorCode, .-_ZN6icu_6711MeasureUnit8createEmER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit5getEmEv
	.type	_ZN6icu_6711MeasureUnit5getEmEv, @function
_ZN6icu_6711MeasureUnit5getEmEv:
.LFB2518:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$12, 18(%rdi)
	ret
	.cfi_endproc
.LFE2518:
	.size	_ZN6icu_6711MeasureUnit5getEmEv, .-_ZN6icu_6711MeasureUnit5getEmEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createMegapixelER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createMegapixelER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createMegapixelER10UErrorCode:
.LFB2519:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L834
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L831
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$12, 18(%rax)
.L829:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L834:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L831:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L829
	.cfi_endproc
.LFE2519:
	.size	_ZN6icu_6711MeasureUnit15createMegapixelER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createMegapixelER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getMegapixelEv
	.type	_ZN6icu_6711MeasureUnit12getMegapixelEv, @function
_ZN6icu_6711MeasureUnit12getMegapixelEv:
.LFB2520:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$12, 18(%rdi)
	ret
	.cfi_endproc
.LFE2520:
	.size	_ZN6icu_6711MeasureUnit12getMegapixelEv, .-_ZN6icu_6711MeasureUnit12getMegapixelEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11createPixelER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit11createPixelER10UErrorCode, @function
_ZN6icu_6711MeasureUnit11createPixelER10UErrorCode:
.LFB2521:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L843
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L840
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$4, %edx
	movw	%dx, 16(%rax)
	movb	$12, 18(%rax)
.L838:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L843:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L840:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L838
	.cfi_endproc
.LFE2521:
	.size	_ZN6icu_6711MeasureUnit11createPixelER10UErrorCode, .-_ZN6icu_6711MeasureUnit11createPixelER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8getPixelEv
	.type	_ZN6icu_6711MeasureUnit8getPixelEv, @function
_ZN6icu_6711MeasureUnit8getPixelEv:
.LFB2522:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$4, %edx
	movw	%dx, 16(%rdi)
	movb	$12, 18(%rdi)
	ret
	.cfi_endproc
.LFE2522:
	.size	_ZN6icu_6711MeasureUnit8getPixelEv, .-_ZN6icu_6711MeasureUnit8getPixelEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit24createPixelPerCentimeterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit24createPixelPerCentimeterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit24createPixelPerCentimeterER10UErrorCode:
.LFB2523:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L852
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L849
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$5, %edx
	movw	%dx, 16(%rax)
	movb	$12, 18(%rax)
.L847:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L852:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L849:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L847
	.cfi_endproc
.LFE2523:
	.size	_ZN6icu_6711MeasureUnit24createPixelPerCentimeterER10UErrorCode, .-_ZN6icu_6711MeasureUnit24createPixelPerCentimeterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit21getPixelPerCentimeterEv
	.type	_ZN6icu_6711MeasureUnit21getPixelPerCentimeterEv, @function
_ZN6icu_6711MeasureUnit21getPixelPerCentimeterEv:
.LFB2524:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$5, %edx
	movw	%dx, 16(%rdi)
	movb	$12, 18(%rdi)
	ret
	.cfi_endproc
.LFE2524:
	.size	_ZN6icu_6711MeasureUnit21getPixelPerCentimeterEv, .-_ZN6icu_6711MeasureUnit21getPixelPerCentimeterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit18createPixelPerInchER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit18createPixelPerInchER10UErrorCode, @function
_ZN6icu_6711MeasureUnit18createPixelPerInchER10UErrorCode:
.LFB2525:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L861
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L858
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$6, %edx
	movw	%dx, 16(%rax)
	movb	$12, 18(%rax)
.L856:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L861:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L858:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L856
	.cfi_endproc
.LFE2525:
	.size	_ZN6icu_6711MeasureUnit18createPixelPerInchER10UErrorCode, .-_ZN6icu_6711MeasureUnit18createPixelPerInchER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15getPixelPerInchEv
	.type	_ZN6icu_6711MeasureUnit15getPixelPerInchEv, @function
_ZN6icu_6711MeasureUnit15getPixelPerInchEv:
.LFB2526:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$6, %edx
	movw	%dx, 16(%rdi)
	movb	$12, 18(%rdi)
	ret
	.cfi_endproc
.LFE2526:
	.size	_ZN6icu_6711MeasureUnit15getPixelPerInchEv, .-_ZN6icu_6711MeasureUnit15getPixelPerInchEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit22createAstronomicalUnitER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit22createAstronomicalUnitER10UErrorCode, @function
_ZN6icu_6711MeasureUnit22createAstronomicalUnitER10UErrorCode:
.LFB2527:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L870
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L867
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L865:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L870:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L867:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L865
	.cfi_endproc
.LFE2527:
	.size	_ZN6icu_6711MeasureUnit22createAstronomicalUnitER10UErrorCode, .-_ZN6icu_6711MeasureUnit22createAstronomicalUnitER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit19getAstronomicalUnitEv
	.type	_ZN6icu_6711MeasureUnit19getAstronomicalUnitEv, @function
_ZN6icu_6711MeasureUnit19getAstronomicalUnitEv:
.LFB2528:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2528:
	.size	_ZN6icu_6711MeasureUnit19getAstronomicalUnitEv, .-_ZN6icu_6711MeasureUnit19getAstronomicalUnitEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createCentimeterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createCentimeterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createCentimeterER10UErrorCode:
.LFB2529:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L879
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L876
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L874:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L879:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L876:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L874
	.cfi_endproc
.LFE2529:
	.size	_ZN6icu_6711MeasureUnit16createCentimeterER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createCentimeterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getCentimeterEv
	.type	_ZN6icu_6711MeasureUnit13getCentimeterEv, @function
_ZN6icu_6711MeasureUnit13getCentimeterEv:
.LFB2530:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2530:
	.size	_ZN6icu_6711MeasureUnit13getCentimeterEv, .-_ZN6icu_6711MeasureUnit13getCentimeterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createDecimeterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createDecimeterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createDecimeterER10UErrorCode:
.LFB2531:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L888
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L885
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L883:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L888:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L885:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L883
	.cfi_endproc
.LFE2531:
	.size	_ZN6icu_6711MeasureUnit15createDecimeterER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createDecimeterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getDecimeterEv
	.type	_ZN6icu_6711MeasureUnit12getDecimeterEv, @function
_ZN6icu_6711MeasureUnit12getDecimeterEv:
.LFB2532:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2532:
	.size	_ZN6icu_6711MeasureUnit12getDecimeterEv, .-_ZN6icu_6711MeasureUnit12getDecimeterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createFathomER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createFathomER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createFathomER10UErrorCode:
.LFB2533:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L897
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L894
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L892:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L897:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L894:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L892
	.cfi_endproc
.LFE2533:
	.size	_ZN6icu_6711MeasureUnit12createFathomER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createFathomER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getFathomEv
	.type	_ZN6icu_6711MeasureUnit9getFathomEv, @function
_ZN6icu_6711MeasureUnit9getFathomEv:
.LFB2534:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2534:
	.size	_ZN6icu_6711MeasureUnit9getFathomEv, .-_ZN6icu_6711MeasureUnit9getFathomEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createFootER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createFootER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createFootER10UErrorCode:
.LFB2535:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L906
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L903
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$4, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L901:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L906:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L903:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L901
	.cfi_endproc
.LFE2535:
	.size	_ZN6icu_6711MeasureUnit10createFootER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createFootER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getFootEv
	.type	_ZN6icu_6711MeasureUnit7getFootEv, @function
_ZN6icu_6711MeasureUnit7getFootEv:
.LFB2536:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$4, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2536:
	.size	_ZN6icu_6711MeasureUnit7getFootEv, .-_ZN6icu_6711MeasureUnit7getFootEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13createFurlongER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit13createFurlongER10UErrorCode, @function
_ZN6icu_6711MeasureUnit13createFurlongER10UErrorCode:
.LFB2537:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L915
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L912
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$5, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L910:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L915:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L912:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L910
	.cfi_endproc
.LFE2537:
	.size	_ZN6icu_6711MeasureUnit13createFurlongER10UErrorCode, .-_ZN6icu_6711MeasureUnit13createFurlongER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10getFurlongEv
	.type	_ZN6icu_6711MeasureUnit10getFurlongEv, @function
_ZN6icu_6711MeasureUnit10getFurlongEv:
.LFB2538:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$5, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2538:
	.size	_ZN6icu_6711MeasureUnit10getFurlongEv, .-_ZN6icu_6711MeasureUnit10getFurlongEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createInchER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createInchER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createInchER10UErrorCode:
.LFB2539:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L924
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L921
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$6, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L919:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L924:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L921:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L919
	.cfi_endproc
.LFE2539:
	.size	_ZN6icu_6711MeasureUnit10createInchER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createInchER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getInchEv
	.type	_ZN6icu_6711MeasureUnit7getInchEv, @function
_ZN6icu_6711MeasureUnit7getInchEv:
.LFB2540:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$6, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2540:
	.size	_ZN6icu_6711MeasureUnit7getInchEv, .-_ZN6icu_6711MeasureUnit7getInchEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createKilometerER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createKilometerER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createKilometerER10UErrorCode:
.LFB2541:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L933
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L930
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$7, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L928:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L933:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L930:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L928
	.cfi_endproc
.LFE2541:
	.size	_ZN6icu_6711MeasureUnit15createKilometerER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createKilometerER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getKilometerEv
	.type	_ZN6icu_6711MeasureUnit12getKilometerEv, @function
_ZN6icu_6711MeasureUnit12getKilometerEv:
.LFB2542:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$7, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2542:
	.size	_ZN6icu_6711MeasureUnit12getKilometerEv, .-_ZN6icu_6711MeasureUnit12getKilometerEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createLightYearER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createLightYearER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createLightYearER10UErrorCode:
.LFB2543:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L942
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L939
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$8, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L937:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L942:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L939:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L937
	.cfi_endproc
.LFE2543:
	.size	_ZN6icu_6711MeasureUnit15createLightYearER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createLightYearER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getLightYearEv
	.type	_ZN6icu_6711MeasureUnit12getLightYearEv, @function
_ZN6icu_6711MeasureUnit12getLightYearEv:
.LFB2544:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$8, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2544:
	.size	_ZN6icu_6711MeasureUnit12getLightYearEv, .-_ZN6icu_6711MeasureUnit12getLightYearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11createMeterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit11createMeterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit11createMeterER10UErrorCode:
.LFB2545:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L951
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L948
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$9, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L946:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L951:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L948:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L946
	.cfi_endproc
.LFE2545:
	.size	_ZN6icu_6711MeasureUnit11createMeterER10UErrorCode, .-_ZN6icu_6711MeasureUnit11createMeterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8getMeterEv
	.type	_ZN6icu_6711MeasureUnit8getMeterEv, @function
_ZN6icu_6711MeasureUnit8getMeterEv:
.LFB2546:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$9, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2546:
	.size	_ZN6icu_6711MeasureUnit8getMeterEv, .-_ZN6icu_6711MeasureUnit8getMeterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createMicrometerER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createMicrometerER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createMicrometerER10UErrorCode:
.LFB2547:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L960
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L957
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$10, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L955:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L960:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L957:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L955
	.cfi_endproc
.LFE2547:
	.size	_ZN6icu_6711MeasureUnit16createMicrometerER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createMicrometerER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getMicrometerEv
	.type	_ZN6icu_6711MeasureUnit13getMicrometerEv, @function
_ZN6icu_6711MeasureUnit13getMicrometerEv:
.LFB2548:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$10, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2548:
	.size	_ZN6icu_6711MeasureUnit13getMicrometerEv, .-_ZN6icu_6711MeasureUnit13getMicrometerEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createMileER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createMileER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createMileER10UErrorCode:
.LFB2549:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L969
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L966
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$11, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L964:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L969:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L966:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L964
	.cfi_endproc
.LFE2549:
	.size	_ZN6icu_6711MeasureUnit10createMileER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createMileER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getMileEv
	.type	_ZN6icu_6711MeasureUnit7getMileEv, @function
_ZN6icu_6711MeasureUnit7getMileEv:
.LFB2550:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$11, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2550:
	.size	_ZN6icu_6711MeasureUnit7getMileEv, .-_ZN6icu_6711MeasureUnit7getMileEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit22createMileScandinavianER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit22createMileScandinavianER10UErrorCode, @function
_ZN6icu_6711MeasureUnit22createMileScandinavianER10UErrorCode:
.LFB2551:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L978
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L975
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$12, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L973:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L978:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L975:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L973
	.cfi_endproc
.LFE2551:
	.size	_ZN6icu_6711MeasureUnit22createMileScandinavianER10UErrorCode, .-_ZN6icu_6711MeasureUnit22createMileScandinavianER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit19getMileScandinavianEv
	.type	_ZN6icu_6711MeasureUnit19getMileScandinavianEv, @function
_ZN6icu_6711MeasureUnit19getMileScandinavianEv:
.LFB2552:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$12, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2552:
	.size	_ZN6icu_6711MeasureUnit19getMileScandinavianEv, .-_ZN6icu_6711MeasureUnit19getMileScandinavianEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createMillimeterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createMillimeterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createMillimeterER10UErrorCode:
.LFB2553:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L987
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L984
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$13, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L982:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L987:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L984:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L982
	.cfi_endproc
.LFE2553:
	.size	_ZN6icu_6711MeasureUnit16createMillimeterER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createMillimeterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getMillimeterEv
	.type	_ZN6icu_6711MeasureUnit13getMillimeterEv, @function
_ZN6icu_6711MeasureUnit13getMillimeterEv:
.LFB2554:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$13, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2554:
	.size	_ZN6icu_6711MeasureUnit13getMillimeterEv, .-_ZN6icu_6711MeasureUnit13getMillimeterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createNanometerER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createNanometerER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createNanometerER10UErrorCode:
.LFB2555:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L996
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L993
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$14, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L991:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L996:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L993:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L991
	.cfi_endproc
.LFE2555:
	.size	_ZN6icu_6711MeasureUnit15createNanometerER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createNanometerER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getNanometerEv
	.type	_ZN6icu_6711MeasureUnit12getNanometerEv, @function
_ZN6icu_6711MeasureUnit12getNanometerEv:
.LFB2556:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$14, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2556:
	.size	_ZN6icu_6711MeasureUnit12getNanometerEv, .-_ZN6icu_6711MeasureUnit12getNanometerEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit18createNauticalMileER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit18createNauticalMileER10UErrorCode, @function
_ZN6icu_6711MeasureUnit18createNauticalMileER10UErrorCode:
.LFB2557:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1005
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1002
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$15, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L1000:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1005:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1002:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1000
	.cfi_endproc
.LFE2557:
	.size	_ZN6icu_6711MeasureUnit18createNauticalMileER10UErrorCode, .-_ZN6icu_6711MeasureUnit18createNauticalMileER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15getNauticalMileEv
	.type	_ZN6icu_6711MeasureUnit15getNauticalMileEv, @function
_ZN6icu_6711MeasureUnit15getNauticalMileEv:
.LFB2558:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$15, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2558:
	.size	_ZN6icu_6711MeasureUnit15getNauticalMileEv, .-_ZN6icu_6711MeasureUnit15getNauticalMileEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createParsecER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createParsecER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createParsecER10UErrorCode:
.LFB2559:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1014
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1011
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$16, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L1009:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1014:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1011:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1009
	.cfi_endproc
.LFE2559:
	.size	_ZN6icu_6711MeasureUnit12createParsecER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createParsecER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getParsecEv
	.type	_ZN6icu_6711MeasureUnit9getParsecEv, @function
_ZN6icu_6711MeasureUnit9getParsecEv:
.LFB2560:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$16, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2560:
	.size	_ZN6icu_6711MeasureUnit9getParsecEv, .-_ZN6icu_6711MeasureUnit9getParsecEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createPicometerER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createPicometerER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createPicometerER10UErrorCode:
.LFB2561:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1023
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1020
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$17, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L1018:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1023:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1020:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1018
	.cfi_endproc
.LFE2561:
	.size	_ZN6icu_6711MeasureUnit15createPicometerER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createPicometerER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getPicometerEv
	.type	_ZN6icu_6711MeasureUnit12getPicometerEv, @function
_ZN6icu_6711MeasureUnit12getPicometerEv:
.LFB2562:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$17, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2562:
	.size	_ZN6icu_6711MeasureUnit12getPicometerEv, .-_ZN6icu_6711MeasureUnit12getPicometerEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11createPointER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit11createPointER10UErrorCode, @function
_ZN6icu_6711MeasureUnit11createPointER10UErrorCode:
.LFB2563:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1032
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1029
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$18, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L1027:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1032:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1029:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1027
	.cfi_endproc
.LFE2563:
	.size	_ZN6icu_6711MeasureUnit11createPointER10UErrorCode, .-_ZN6icu_6711MeasureUnit11createPointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8getPointEv
	.type	_ZN6icu_6711MeasureUnit8getPointEv, @function
_ZN6icu_6711MeasureUnit8getPointEv:
.LFB2564:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$18, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2564:
	.size	_ZN6icu_6711MeasureUnit8getPointEv, .-_ZN6icu_6711MeasureUnit8getPointEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17createSolarRadiusER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit17createSolarRadiusER10UErrorCode, @function
_ZN6icu_6711MeasureUnit17createSolarRadiusER10UErrorCode:
.LFB2565:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1041
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1038
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$19, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L1036:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1041:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1038:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1036
	.cfi_endproc
.LFE2565:
	.size	_ZN6icu_6711MeasureUnit17createSolarRadiusER10UErrorCode, .-_ZN6icu_6711MeasureUnit17createSolarRadiusER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14getSolarRadiusEv
	.type	_ZN6icu_6711MeasureUnit14getSolarRadiusEv, @function
_ZN6icu_6711MeasureUnit14getSolarRadiusEv:
.LFB2566:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$19, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2566:
	.size	_ZN6icu_6711MeasureUnit14getSolarRadiusEv, .-_ZN6icu_6711MeasureUnit14getSolarRadiusEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createYardER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createYardER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createYardER10UErrorCode:
.LFB2567:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1050
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1047
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$20, %edx
	movw	%dx, 16(%rax)
	movb	$13, 18(%rax)
.L1045:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1050:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1047:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1045
	.cfi_endproc
.LFE2567:
	.size	_ZN6icu_6711MeasureUnit10createYardER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createYardER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getYardEv
	.type	_ZN6icu_6711MeasureUnit7getYardEv, @function
_ZN6icu_6711MeasureUnit7getYardEv:
.LFB2568:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$20, %edx
	movw	%dx, 16(%rdi)
	movb	$13, 18(%rdi)
	ret
	.cfi_endproc
.LFE2568:
	.size	_ZN6icu_6711MeasureUnit7getYardEv, .-_ZN6icu_6711MeasureUnit7getYardEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9createLuxER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit9createLuxER10UErrorCode, @function
_ZN6icu_6711MeasureUnit9createLuxER10UErrorCode:
.LFB2569:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1059
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1056
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$14, 18(%rax)
.L1054:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1059:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1056:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1054
	.cfi_endproc
.LFE2569:
	.size	_ZN6icu_6711MeasureUnit9createLuxER10UErrorCode, .-_ZN6icu_6711MeasureUnit9createLuxER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit6getLuxEv
	.type	_ZN6icu_6711MeasureUnit6getLuxEv, @function
_ZN6icu_6711MeasureUnit6getLuxEv:
.LFB2570:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$14, 18(%rdi)
	ret
	.cfi_endproc
.LFE2570:
	.size	_ZN6icu_6711MeasureUnit6getLuxEv, .-_ZN6icu_6711MeasureUnit6getLuxEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit21createSolarLuminosityER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit21createSolarLuminosityER10UErrorCode, @function
_ZN6icu_6711MeasureUnit21createSolarLuminosityER10UErrorCode:
.LFB2571:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1068
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1065
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$14, 18(%rax)
.L1063:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1068:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1065:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1063
	.cfi_endproc
.LFE2571:
	.size	_ZN6icu_6711MeasureUnit21createSolarLuminosityER10UErrorCode, .-_ZN6icu_6711MeasureUnit21createSolarLuminosityER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit18getSolarLuminosityEv
	.type	_ZN6icu_6711MeasureUnit18getSolarLuminosityEv, @function
_ZN6icu_6711MeasureUnit18getSolarLuminosityEv:
.LFB2572:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$14, 18(%rdi)
	ret
	.cfi_endproc
.LFE2572:
	.size	_ZN6icu_6711MeasureUnit18getSolarLuminosityEv, .-_ZN6icu_6711MeasureUnit18getSolarLuminosityEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11createCaratER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit11createCaratER10UErrorCode, @function
_ZN6icu_6711MeasureUnit11createCaratER10UErrorCode:
.LFB2573:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1077
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1074
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$15, 18(%rax)
.L1072:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1077:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1074:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1072
	.cfi_endproc
.LFE2573:
	.size	_ZN6icu_6711MeasureUnit11createCaratER10UErrorCode, .-_ZN6icu_6711MeasureUnit11createCaratER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8getCaratEv
	.type	_ZN6icu_6711MeasureUnit8getCaratEv, @function
_ZN6icu_6711MeasureUnit8getCaratEv:
.LFB2574:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$15, 18(%rdi)
	ret
	.cfi_endproc
.LFE2574:
	.size	_ZN6icu_6711MeasureUnit8getCaratEv, .-_ZN6icu_6711MeasureUnit8getCaratEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createDaltonER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createDaltonER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createDaltonER10UErrorCode:
.LFB2575:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1086
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1083
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$15, 18(%rax)
.L1081:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1086:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1083:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1081
	.cfi_endproc
.LFE2575:
	.size	_ZN6icu_6711MeasureUnit12createDaltonER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createDaltonER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getDaltonEv
	.type	_ZN6icu_6711MeasureUnit9getDaltonEv, @function
_ZN6icu_6711MeasureUnit9getDaltonEv:
.LFB2576:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$15, 18(%rdi)
	ret
	.cfi_endproc
.LFE2576:
	.size	_ZN6icu_6711MeasureUnit9getDaltonEv, .-_ZN6icu_6711MeasureUnit9getDaltonEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createEarthMassER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createEarthMassER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createEarthMassER10UErrorCode:
.LFB2577:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1095
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1092
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$15, 18(%rax)
.L1090:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1095:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1092:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1090
	.cfi_endproc
.LFE2577:
	.size	_ZN6icu_6711MeasureUnit15createEarthMassER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createEarthMassER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getEarthMassEv
	.type	_ZN6icu_6711MeasureUnit12getEarthMassEv, @function
_ZN6icu_6711MeasureUnit12getEarthMassEv:
.LFB2578:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$15, 18(%rdi)
	ret
	.cfi_endproc
.LFE2578:
	.size	_ZN6icu_6711MeasureUnit12getEarthMassEv, .-_ZN6icu_6711MeasureUnit12getEarthMassEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createGramER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createGramER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createGramER10UErrorCode:
.LFB2579:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1104
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1101
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$15, 18(%rax)
.L1099:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1104:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1101:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1099
	.cfi_endproc
.LFE2579:
	.size	_ZN6icu_6711MeasureUnit10createGramER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createGramER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getGramEv
	.type	_ZN6icu_6711MeasureUnit7getGramEv, @function
_ZN6icu_6711MeasureUnit7getGramEv:
.LFB2580:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$15, 18(%rdi)
	ret
	.cfi_endproc
.LFE2580:
	.size	_ZN6icu_6711MeasureUnit7getGramEv, .-_ZN6icu_6711MeasureUnit7getGramEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14createKilogramER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit14createKilogramER10UErrorCode, @function
_ZN6icu_6711MeasureUnit14createKilogramER10UErrorCode:
.LFB2581:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1113
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1110
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$4, %edx
	movw	%dx, 16(%rax)
	movb	$15, 18(%rax)
.L1108:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1113:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1110:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1108
	.cfi_endproc
.LFE2581:
	.size	_ZN6icu_6711MeasureUnit14createKilogramER10UErrorCode, .-_ZN6icu_6711MeasureUnit14createKilogramER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11getKilogramEv
	.type	_ZN6icu_6711MeasureUnit11getKilogramEv, @function
_ZN6icu_6711MeasureUnit11getKilogramEv:
.LFB2582:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$4, %edx
	movw	%dx, 16(%rdi)
	movb	$15, 18(%rdi)
	ret
	.cfi_endproc
.LFE2582:
	.size	_ZN6icu_6711MeasureUnit11getKilogramEv, .-_ZN6icu_6711MeasureUnit11getKilogramEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createMetricTonER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createMetricTonER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createMetricTonER10UErrorCode:
.LFB2583:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1122
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1119
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$5, %edx
	movw	%dx, 16(%rax)
	movb	$15, 18(%rax)
.L1117:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1122:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1119:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1117
	.cfi_endproc
.LFE2583:
	.size	_ZN6icu_6711MeasureUnit15createMetricTonER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createMetricTonER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getMetricTonEv
	.type	_ZN6icu_6711MeasureUnit12getMetricTonEv, @function
_ZN6icu_6711MeasureUnit12getMetricTonEv:
.LFB2584:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$5, %edx
	movw	%dx, 16(%rdi)
	movb	$15, 18(%rdi)
	ret
	.cfi_endproc
.LFE2584:
	.size	_ZN6icu_6711MeasureUnit12getMetricTonEv, .-_ZN6icu_6711MeasureUnit12getMetricTonEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createMicrogramER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createMicrogramER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createMicrogramER10UErrorCode:
.LFB2585:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1131
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1128
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$6, %edx
	movw	%dx, 16(%rax)
	movb	$15, 18(%rax)
.L1126:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1131:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1128:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1126
	.cfi_endproc
.LFE2585:
	.size	_ZN6icu_6711MeasureUnit15createMicrogramER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createMicrogramER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getMicrogramEv
	.type	_ZN6icu_6711MeasureUnit12getMicrogramEv, @function
_ZN6icu_6711MeasureUnit12getMicrogramEv:
.LFB2586:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$6, %edx
	movw	%dx, 16(%rdi)
	movb	$15, 18(%rdi)
	ret
	.cfi_endproc
.LFE2586:
	.size	_ZN6icu_6711MeasureUnit12getMicrogramEv, .-_ZN6icu_6711MeasureUnit12getMicrogramEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createMilligramER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createMilligramER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createMilligramER10UErrorCode:
.LFB2587:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1140
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1137
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$7, %edx
	movw	%dx, 16(%rax)
	movb	$15, 18(%rax)
.L1135:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1140:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1137:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1135
	.cfi_endproc
.LFE2587:
	.size	_ZN6icu_6711MeasureUnit15createMilligramER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createMilligramER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getMilligramEv
	.type	_ZN6icu_6711MeasureUnit12getMilligramEv, @function
_ZN6icu_6711MeasureUnit12getMilligramEv:
.LFB2588:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$7, %edx
	movw	%dx, 16(%rdi)
	movb	$15, 18(%rdi)
	ret
	.cfi_endproc
.LFE2588:
	.size	_ZN6icu_6711MeasureUnit12getMilligramEv, .-_ZN6icu_6711MeasureUnit12getMilligramEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11createOunceER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit11createOunceER10UErrorCode, @function
_ZN6icu_6711MeasureUnit11createOunceER10UErrorCode:
.LFB2589:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1149
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1146
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$8, %edx
	movw	%dx, 16(%rax)
	movb	$15, 18(%rax)
.L1144:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1149:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1146:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1144
	.cfi_endproc
.LFE2589:
	.size	_ZN6icu_6711MeasureUnit11createOunceER10UErrorCode, .-_ZN6icu_6711MeasureUnit11createOunceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8getOunceEv
	.type	_ZN6icu_6711MeasureUnit8getOunceEv, @function
_ZN6icu_6711MeasureUnit8getOunceEv:
.LFB2590:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$8, %edx
	movw	%dx, 16(%rdi)
	movb	$15, 18(%rdi)
	ret
	.cfi_endproc
.LFE2590:
	.size	_ZN6icu_6711MeasureUnit8getOunceEv, .-_ZN6icu_6711MeasureUnit8getOunceEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createOunceTroyER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createOunceTroyER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createOunceTroyER10UErrorCode:
.LFB2591:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1158
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1155
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$9, %edx
	movw	%dx, 16(%rax)
	movb	$15, 18(%rax)
.L1153:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1158:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1155:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1153
	.cfi_endproc
.LFE2591:
	.size	_ZN6icu_6711MeasureUnit15createOunceTroyER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createOunceTroyER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getOunceTroyEv
	.type	_ZN6icu_6711MeasureUnit12getOunceTroyEv, @function
_ZN6icu_6711MeasureUnit12getOunceTroyEv:
.LFB2592:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$9, %edx
	movw	%dx, 16(%rdi)
	movb	$15, 18(%rdi)
	ret
	.cfi_endproc
.LFE2592:
	.size	_ZN6icu_6711MeasureUnit12getOunceTroyEv, .-_ZN6icu_6711MeasureUnit12getOunceTroyEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11createPoundER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit11createPoundER10UErrorCode, @function
_ZN6icu_6711MeasureUnit11createPoundER10UErrorCode:
.LFB2593:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1167
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1164
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$10, %edx
	movw	%dx, 16(%rax)
	movb	$15, 18(%rax)
.L1162:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1167:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1164:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1162
	.cfi_endproc
.LFE2593:
	.size	_ZN6icu_6711MeasureUnit11createPoundER10UErrorCode, .-_ZN6icu_6711MeasureUnit11createPoundER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8getPoundEv
	.type	_ZN6icu_6711MeasureUnit8getPoundEv, @function
_ZN6icu_6711MeasureUnit8getPoundEv:
.LFB2594:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$10, %edx
	movw	%dx, 16(%rdi)
	movb	$15, 18(%rdi)
	ret
	.cfi_endproc
.LFE2594:
	.size	_ZN6icu_6711MeasureUnit8getPoundEv, .-_ZN6icu_6711MeasureUnit8getPoundEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createSolarMassER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createSolarMassER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createSolarMassER10UErrorCode:
.LFB2595:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1176
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1173
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$11, %edx
	movw	%dx, 16(%rax)
	movb	$15, 18(%rax)
.L1171:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1176:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1173:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1171
	.cfi_endproc
.LFE2595:
	.size	_ZN6icu_6711MeasureUnit15createSolarMassER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createSolarMassER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getSolarMassEv
	.type	_ZN6icu_6711MeasureUnit12getSolarMassEv, @function
_ZN6icu_6711MeasureUnit12getSolarMassEv:
.LFB2596:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$11, %edx
	movw	%dx, 16(%rdi)
	movb	$15, 18(%rdi)
	ret
	.cfi_endproc
.LFE2596:
	.size	_ZN6icu_6711MeasureUnit12getSolarMassEv, .-_ZN6icu_6711MeasureUnit12getSolarMassEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11createStoneER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit11createStoneER10UErrorCode, @function
_ZN6icu_6711MeasureUnit11createStoneER10UErrorCode:
.LFB2597:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1185
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1182
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$12, %edx
	movw	%dx, 16(%rax)
	movb	$15, 18(%rax)
.L1180:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1185:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1182:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1180
	.cfi_endproc
.LFE2597:
	.size	_ZN6icu_6711MeasureUnit11createStoneER10UErrorCode, .-_ZN6icu_6711MeasureUnit11createStoneER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8getStoneEv
	.type	_ZN6icu_6711MeasureUnit8getStoneEv, @function
_ZN6icu_6711MeasureUnit8getStoneEv:
.LFB2598:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$12, %edx
	movw	%dx, 16(%rdi)
	movb	$15, 18(%rdi)
	ret
	.cfi_endproc
.LFE2598:
	.size	_ZN6icu_6711MeasureUnit8getStoneEv, .-_ZN6icu_6711MeasureUnit8getStoneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9createTonER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit9createTonER10UErrorCode, @function
_ZN6icu_6711MeasureUnit9createTonER10UErrorCode:
.LFB2599:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1194
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1191
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$13, %edx
	movw	%dx, 16(%rax)
	movb	$15, 18(%rax)
.L1189:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1194:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1191:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1189
	.cfi_endproc
.LFE2599:
	.size	_ZN6icu_6711MeasureUnit9createTonER10UErrorCode, .-_ZN6icu_6711MeasureUnit9createTonER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit6getTonEv
	.type	_ZN6icu_6711MeasureUnit6getTonEv, @function
_ZN6icu_6711MeasureUnit6getTonEv:
.LFB2600:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$13, %edx
	movw	%dx, 16(%rdi)
	movb	$15, 18(%rdi)
	ret
	.cfi_endproc
.LFE2600:
	.size	_ZN6icu_6711MeasureUnit6getTonEv, .-_ZN6icu_6711MeasureUnit6getTonEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14createGigawattER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit14createGigawattER10UErrorCode, @function
_ZN6icu_6711MeasureUnit14createGigawattER10UErrorCode:
.LFB2601:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1203
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1200
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$17, 18(%rax)
.L1198:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1203:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1200:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1198
	.cfi_endproc
.LFE2601:
	.size	_ZN6icu_6711MeasureUnit14createGigawattER10UErrorCode, .-_ZN6icu_6711MeasureUnit14createGigawattER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11getGigawattEv
	.type	_ZN6icu_6711MeasureUnit11getGigawattEv, @function
_ZN6icu_6711MeasureUnit11getGigawattEv:
.LFB2602:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$17, 18(%rdi)
	ret
	.cfi_endproc
.LFE2602:
	.size	_ZN6icu_6711MeasureUnit11getGigawattEv, .-_ZN6icu_6711MeasureUnit11getGigawattEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createHorsepowerER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createHorsepowerER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createHorsepowerER10UErrorCode:
.LFB2603:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1212
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1209
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$17, 18(%rax)
.L1207:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1212:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1209:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1207
	.cfi_endproc
.LFE2603:
	.size	_ZN6icu_6711MeasureUnit16createHorsepowerER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createHorsepowerER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getHorsepowerEv
	.type	_ZN6icu_6711MeasureUnit13getHorsepowerEv, @function
_ZN6icu_6711MeasureUnit13getHorsepowerEv:
.LFB2604:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$17, 18(%rdi)
	ret
	.cfi_endproc
.LFE2604:
	.size	_ZN6icu_6711MeasureUnit13getHorsepowerEv, .-_ZN6icu_6711MeasureUnit13getHorsepowerEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14createKilowattER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit14createKilowattER10UErrorCode, @function
_ZN6icu_6711MeasureUnit14createKilowattER10UErrorCode:
.LFB2605:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1221
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1218
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$17, 18(%rax)
.L1216:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1221:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1218:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1216
	.cfi_endproc
.LFE2605:
	.size	_ZN6icu_6711MeasureUnit14createKilowattER10UErrorCode, .-_ZN6icu_6711MeasureUnit14createKilowattER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11getKilowattEv
	.type	_ZN6icu_6711MeasureUnit11getKilowattEv, @function
_ZN6icu_6711MeasureUnit11getKilowattEv:
.LFB2606:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$17, 18(%rdi)
	ret
	.cfi_endproc
.LFE2606:
	.size	_ZN6icu_6711MeasureUnit11getKilowattEv, .-_ZN6icu_6711MeasureUnit11getKilowattEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14createMegawattER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit14createMegawattER10UErrorCode, @function
_ZN6icu_6711MeasureUnit14createMegawattER10UErrorCode:
.LFB2607:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1230
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1227
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$17, 18(%rax)
.L1225:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1230:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1227:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1225
	.cfi_endproc
.LFE2607:
	.size	_ZN6icu_6711MeasureUnit14createMegawattER10UErrorCode, .-_ZN6icu_6711MeasureUnit14createMegawattER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11getMegawattEv
	.type	_ZN6icu_6711MeasureUnit11getMegawattEv, @function
_ZN6icu_6711MeasureUnit11getMegawattEv:
.LFB2608:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$17, 18(%rdi)
	ret
	.cfi_endproc
.LFE2608:
	.size	_ZN6icu_6711MeasureUnit11getMegawattEv, .-_ZN6icu_6711MeasureUnit11getMegawattEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createMilliwattER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createMilliwattER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createMilliwattER10UErrorCode:
.LFB2609:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1239
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1236
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$4, %edx
	movw	%dx, 16(%rax)
	movb	$17, 18(%rax)
.L1234:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1239:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1236:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1234
	.cfi_endproc
.LFE2609:
	.size	_ZN6icu_6711MeasureUnit15createMilliwattER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createMilliwattER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getMilliwattEv
	.type	_ZN6icu_6711MeasureUnit12getMilliwattEv, @function
_ZN6icu_6711MeasureUnit12getMilliwattEv:
.LFB2610:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$4, %edx
	movw	%dx, 16(%rdi)
	movb	$17, 18(%rdi)
	ret
	.cfi_endproc
.LFE2610:
	.size	_ZN6icu_6711MeasureUnit12getMilliwattEv, .-_ZN6icu_6711MeasureUnit12getMilliwattEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createWattER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createWattER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createWattER10UErrorCode:
.LFB2611:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1248
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1245
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$5, %edx
	movw	%dx, 16(%rax)
	movb	$17, 18(%rax)
.L1243:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1248:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1245:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1243
	.cfi_endproc
.LFE2611:
	.size	_ZN6icu_6711MeasureUnit10createWattER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createWattER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getWattEv
	.type	_ZN6icu_6711MeasureUnit7getWattEv, @function
_ZN6icu_6711MeasureUnit7getWattEv:
.LFB2612:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$5, %edx
	movw	%dx, 16(%rdi)
	movb	$17, 18(%rdi)
	ret
	.cfi_endproc
.LFE2612:
	.size	_ZN6icu_6711MeasureUnit7getWattEv, .-_ZN6icu_6711MeasureUnit7getWattEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createAtmosphereER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createAtmosphereER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createAtmosphereER10UErrorCode:
.LFB2613:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1257
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1254
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$18, 18(%rax)
.L1252:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1257:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1254:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1252
	.cfi_endproc
.LFE2613:
	.size	_ZN6icu_6711MeasureUnit16createAtmosphereER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createAtmosphereER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getAtmosphereEv
	.type	_ZN6icu_6711MeasureUnit13getAtmosphereEv, @function
_ZN6icu_6711MeasureUnit13getAtmosphereEv:
.LFB2614:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$18, 18(%rdi)
	ret
	.cfi_endproc
.LFE2614:
	.size	_ZN6icu_6711MeasureUnit13getAtmosphereEv, .-_ZN6icu_6711MeasureUnit13getAtmosphereEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9createBarER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit9createBarER10UErrorCode, @function
_ZN6icu_6711MeasureUnit9createBarER10UErrorCode:
.LFB2615:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1266
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1263
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$18, 18(%rax)
.L1261:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1266:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1263:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1261
	.cfi_endproc
.LFE2615:
	.size	_ZN6icu_6711MeasureUnit9createBarER10UErrorCode, .-_ZN6icu_6711MeasureUnit9createBarER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit6getBarEv
	.type	_ZN6icu_6711MeasureUnit6getBarEv, @function
_ZN6icu_6711MeasureUnit6getBarEv:
.LFB2616:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$18, 18(%rdi)
	ret
	.cfi_endproc
.LFE2616:
	.size	_ZN6icu_6711MeasureUnit6getBarEv, .-_ZN6icu_6711MeasureUnit6getBarEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17createHectopascalER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit17createHectopascalER10UErrorCode, @function
_ZN6icu_6711MeasureUnit17createHectopascalER10UErrorCode:
.LFB2617:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1275
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1272
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$18, 18(%rax)
.L1270:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1275:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1272:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1270
	.cfi_endproc
.LFE2617:
	.size	_ZN6icu_6711MeasureUnit17createHectopascalER10UErrorCode, .-_ZN6icu_6711MeasureUnit17createHectopascalER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14getHectopascalEv
	.type	_ZN6icu_6711MeasureUnit14getHectopascalEv, @function
_ZN6icu_6711MeasureUnit14getHectopascalEv:
.LFB2618:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$18, 18(%rdi)
	ret
	.cfi_endproc
.LFE2618:
	.size	_ZN6icu_6711MeasureUnit14getHectopascalEv, .-_ZN6icu_6711MeasureUnit14getHectopascalEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createInchHgER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createInchHgER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createInchHgER10UErrorCode:
.LFB2619:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1284
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1281
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$18, 18(%rax)
.L1279:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1284:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1281:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1279
	.cfi_endproc
.LFE2619:
	.size	_ZN6icu_6711MeasureUnit12createInchHgER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createInchHgER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getInchHgEv
	.type	_ZN6icu_6711MeasureUnit9getInchHgEv, @function
_ZN6icu_6711MeasureUnit9getInchHgEv:
.LFB2620:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$18, 18(%rdi)
	ret
	.cfi_endproc
.LFE2620:
	.size	_ZN6icu_6711MeasureUnit9getInchHgEv, .-_ZN6icu_6711MeasureUnit9getInchHgEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createKilopascalER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createKilopascalER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createKilopascalER10UErrorCode:
.LFB2621:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1293
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1290
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$4, %edx
	movw	%dx, 16(%rax)
	movb	$18, 18(%rax)
.L1288:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1293:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1290:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1288
	.cfi_endproc
.LFE2621:
	.size	_ZN6icu_6711MeasureUnit16createKilopascalER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createKilopascalER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getKilopascalEv
	.type	_ZN6icu_6711MeasureUnit13getKilopascalEv, @function
_ZN6icu_6711MeasureUnit13getKilopascalEv:
.LFB2622:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$4, %edx
	movw	%dx, 16(%rdi)
	movb	$18, 18(%rdi)
	ret
	.cfi_endproc
.LFE2622:
	.size	_ZN6icu_6711MeasureUnit13getKilopascalEv, .-_ZN6icu_6711MeasureUnit13getKilopascalEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createMegapascalER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createMegapascalER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createMegapascalER10UErrorCode:
.LFB2623:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1302
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1299
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$5, %edx
	movw	%dx, 16(%rax)
	movb	$18, 18(%rax)
.L1297:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1302:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1299:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1297
	.cfi_endproc
.LFE2623:
	.size	_ZN6icu_6711MeasureUnit16createMegapascalER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createMegapascalER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getMegapascalEv
	.type	_ZN6icu_6711MeasureUnit13getMegapascalEv, @function
_ZN6icu_6711MeasureUnit13getMegapascalEv:
.LFB2624:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$5, %edx
	movw	%dx, 16(%rdi)
	movb	$18, 18(%rdi)
	ret
	.cfi_endproc
.LFE2624:
	.size	_ZN6icu_6711MeasureUnit13getMegapascalEv, .-_ZN6icu_6711MeasureUnit13getMegapascalEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14createMillibarER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit14createMillibarER10UErrorCode, @function
_ZN6icu_6711MeasureUnit14createMillibarER10UErrorCode:
.LFB2625:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1311
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1308
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$6, %edx
	movw	%dx, 16(%rax)
	movb	$18, 18(%rax)
.L1306:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1311:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1308:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1306
	.cfi_endproc
.LFE2625:
	.size	_ZN6icu_6711MeasureUnit14createMillibarER10UErrorCode, .-_ZN6icu_6711MeasureUnit14createMillibarER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11getMillibarEv
	.type	_ZN6icu_6711MeasureUnit11getMillibarEv, @function
_ZN6icu_6711MeasureUnit11getMillibarEv:
.LFB2626:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$6, %edx
	movw	%dx, 16(%rdi)
	movb	$18, 18(%rdi)
	ret
	.cfi_endproc
.LFE2626:
	.size	_ZN6icu_6711MeasureUnit11getMillibarEv, .-_ZN6icu_6711MeasureUnit11getMillibarEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit25createMillimeterOfMercuryER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit25createMillimeterOfMercuryER10UErrorCode, @function
_ZN6icu_6711MeasureUnit25createMillimeterOfMercuryER10UErrorCode:
.LFB2627:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1320
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1317
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$7, %edx
	movw	%dx, 16(%rax)
	movb	$18, 18(%rax)
.L1315:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1320:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1317:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1315
	.cfi_endproc
.LFE2627:
	.size	_ZN6icu_6711MeasureUnit25createMillimeterOfMercuryER10UErrorCode, .-_ZN6icu_6711MeasureUnit25createMillimeterOfMercuryER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit22getMillimeterOfMercuryEv
	.type	_ZN6icu_6711MeasureUnit22getMillimeterOfMercuryEv, @function
_ZN6icu_6711MeasureUnit22getMillimeterOfMercuryEv:
.LFB2628:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$7, %edx
	movw	%dx, 16(%rdi)
	movb	$18, 18(%rdi)
	ret
	.cfi_endproc
.LFE2628:
	.size	_ZN6icu_6711MeasureUnit22getMillimeterOfMercuryEv, .-_ZN6icu_6711MeasureUnit22getMillimeterOfMercuryEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createPascalER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createPascalER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createPascalER10UErrorCode:
.LFB2629:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1329
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1326
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$8, %edx
	movw	%dx, 16(%rax)
	movb	$18, 18(%rax)
.L1324:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1329:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1326:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1324
	.cfi_endproc
.LFE2629:
	.size	_ZN6icu_6711MeasureUnit12createPascalER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createPascalER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getPascalEv
	.type	_ZN6icu_6711MeasureUnit9getPascalEv, @function
_ZN6icu_6711MeasureUnit9getPascalEv:
.LFB2630:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$8, %edx
	movw	%dx, 16(%rdi)
	movb	$18, 18(%rdi)
	ret
	.cfi_endproc
.LFE2630:
	.size	_ZN6icu_6711MeasureUnit9getPascalEv, .-_ZN6icu_6711MeasureUnit9getPascalEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit24createPoundPerSquareInchER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit24createPoundPerSquareInchER10UErrorCode, @function
_ZN6icu_6711MeasureUnit24createPoundPerSquareInchER10UErrorCode:
.LFB2631:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1338
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1335
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$9, %edx
	movw	%dx, 16(%rax)
	movb	$18, 18(%rax)
.L1333:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1338:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1335:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1333
	.cfi_endproc
.LFE2631:
	.size	_ZN6icu_6711MeasureUnit24createPoundPerSquareInchER10UErrorCode, .-_ZN6icu_6711MeasureUnit24createPoundPerSquareInchER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit21getPoundPerSquareInchEv
	.type	_ZN6icu_6711MeasureUnit21getPoundPerSquareInchEv, @function
_ZN6icu_6711MeasureUnit21getPoundPerSquareInchEv:
.LFB2632:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$9, %edx
	movw	%dx, 16(%rdi)
	movb	$18, 18(%rdi)
	ret
	.cfi_endproc
.LFE2632:
	.size	_ZN6icu_6711MeasureUnit21getPoundPerSquareInchEv, .-_ZN6icu_6711MeasureUnit21getPoundPerSquareInchEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit22createKilometerPerHourER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit22createKilometerPerHourER10UErrorCode, @function
_ZN6icu_6711MeasureUnit22createKilometerPerHourER10UErrorCode:
.LFB2633:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1347
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1344
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$19, 18(%rax)
.L1342:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1347:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1344:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1342
	.cfi_endproc
.LFE2633:
	.size	_ZN6icu_6711MeasureUnit22createKilometerPerHourER10UErrorCode, .-_ZN6icu_6711MeasureUnit22createKilometerPerHourER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit19getKilometerPerHourEv
	.type	_ZN6icu_6711MeasureUnit19getKilometerPerHourEv, @function
_ZN6icu_6711MeasureUnit19getKilometerPerHourEv:
.LFB2634:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$19, 18(%rdi)
	ret
	.cfi_endproc
.LFE2634:
	.size	_ZN6icu_6711MeasureUnit19getKilometerPerHourEv, .-_ZN6icu_6711MeasureUnit19getKilometerPerHourEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createKnotER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createKnotER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createKnotER10UErrorCode:
.LFB2635:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1356
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1353
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$19, 18(%rax)
.L1351:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1356:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1353:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1351
	.cfi_endproc
.LFE2635:
	.size	_ZN6icu_6711MeasureUnit10createKnotER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createKnotER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getKnotEv
	.type	_ZN6icu_6711MeasureUnit7getKnotEv, @function
_ZN6icu_6711MeasureUnit7getKnotEv:
.LFB2636:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$19, 18(%rdi)
	ret
	.cfi_endproc
.LFE2636:
	.size	_ZN6icu_6711MeasureUnit7getKnotEv, .-_ZN6icu_6711MeasureUnit7getKnotEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit20createMeterPerSecondER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit20createMeterPerSecondER10UErrorCode, @function
_ZN6icu_6711MeasureUnit20createMeterPerSecondER10UErrorCode:
.LFB2637:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1365
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1362
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$19, 18(%rax)
.L1360:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1365:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1362:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1360
	.cfi_endproc
.LFE2637:
	.size	_ZN6icu_6711MeasureUnit20createMeterPerSecondER10UErrorCode, .-_ZN6icu_6711MeasureUnit20createMeterPerSecondER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17getMeterPerSecondEv
	.type	_ZN6icu_6711MeasureUnit17getMeterPerSecondEv, @function
_ZN6icu_6711MeasureUnit17getMeterPerSecondEv:
.LFB2638:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$19, 18(%rdi)
	ret
	.cfi_endproc
.LFE2638:
	.size	_ZN6icu_6711MeasureUnit17getMeterPerSecondEv, .-_ZN6icu_6711MeasureUnit17getMeterPerSecondEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17createMilePerHourER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit17createMilePerHourER10UErrorCode, @function
_ZN6icu_6711MeasureUnit17createMilePerHourER10UErrorCode:
.LFB2639:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1374
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1371
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$19, 18(%rax)
.L1369:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1374:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1371:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1369
	.cfi_endproc
.LFE2639:
	.size	_ZN6icu_6711MeasureUnit17createMilePerHourER10UErrorCode, .-_ZN6icu_6711MeasureUnit17createMilePerHourER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14getMilePerHourEv
	.type	_ZN6icu_6711MeasureUnit14getMilePerHourEv, @function
_ZN6icu_6711MeasureUnit14getMilePerHourEv:
.LFB2640:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$19, 18(%rdi)
	ret
	.cfi_endproc
.LFE2640:
	.size	_ZN6icu_6711MeasureUnit14getMilePerHourEv, .-_ZN6icu_6711MeasureUnit14getMilePerHourEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13createCelsiusER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit13createCelsiusER10UErrorCode, @function
_ZN6icu_6711MeasureUnit13createCelsiusER10UErrorCode:
.LFB2641:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1383
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1380
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$20, 18(%rax)
.L1378:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1383:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1380:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1378
	.cfi_endproc
.LFE2641:
	.size	_ZN6icu_6711MeasureUnit13createCelsiusER10UErrorCode, .-_ZN6icu_6711MeasureUnit13createCelsiusER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10getCelsiusEv
	.type	_ZN6icu_6711MeasureUnit10getCelsiusEv, @function
_ZN6icu_6711MeasureUnit10getCelsiusEv:
.LFB2642:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$20, 18(%rdi)
	ret
	.cfi_endproc
.LFE2642:
	.size	_ZN6icu_6711MeasureUnit10getCelsiusEv, .-_ZN6icu_6711MeasureUnit10getCelsiusEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createFahrenheitER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createFahrenheitER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createFahrenheitER10UErrorCode:
.LFB2643:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1392
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1389
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$20, 18(%rax)
.L1387:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1392:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1389:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1387
	.cfi_endproc
.LFE2643:
	.size	_ZN6icu_6711MeasureUnit16createFahrenheitER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createFahrenheitER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getFahrenheitEv
	.type	_ZN6icu_6711MeasureUnit13getFahrenheitEv, @function
_ZN6icu_6711MeasureUnit13getFahrenheitEv:
.LFB2644:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$20, 18(%rdi)
	ret
	.cfi_endproc
.LFE2644:
	.size	_ZN6icu_6711MeasureUnit13getFahrenheitEv, .-_ZN6icu_6711MeasureUnit13getFahrenheitEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit24createGenericTemperatureER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit24createGenericTemperatureER10UErrorCode, @function
_ZN6icu_6711MeasureUnit24createGenericTemperatureER10UErrorCode:
.LFB2645:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1401
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1398
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$20, 18(%rax)
.L1396:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1401:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1398:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1396
	.cfi_endproc
.LFE2645:
	.size	_ZN6icu_6711MeasureUnit24createGenericTemperatureER10UErrorCode, .-_ZN6icu_6711MeasureUnit24createGenericTemperatureER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit21getGenericTemperatureEv
	.type	_ZN6icu_6711MeasureUnit21getGenericTemperatureEv, @function
_ZN6icu_6711MeasureUnit21getGenericTemperatureEv:
.LFB2646:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$20, 18(%rdi)
	ret
	.cfi_endproc
.LFE2646:
	.size	_ZN6icu_6711MeasureUnit21getGenericTemperatureEv, .-_ZN6icu_6711MeasureUnit21getGenericTemperatureEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createKelvinER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createKelvinER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createKelvinER10UErrorCode:
.LFB2647:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1410
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1407
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$20, 18(%rax)
.L1405:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1410:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1407:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1405
	.cfi_endproc
.LFE2647:
	.size	_ZN6icu_6711MeasureUnit12createKelvinER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createKelvinER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getKelvinEv
	.type	_ZN6icu_6711MeasureUnit9getKelvinEv, @function
_ZN6icu_6711MeasureUnit9getKelvinEv:
.LFB2648:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$20, 18(%rdi)
	ret
	.cfi_endproc
.LFE2648:
	.size	_ZN6icu_6711MeasureUnit9getKelvinEv, .-_ZN6icu_6711MeasureUnit9getKelvinEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17createNewtonMeterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit17createNewtonMeterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit17createNewtonMeterER10UErrorCode:
.LFB2649:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1419
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1416
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$21, 18(%rax)
.L1414:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1419:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1416:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1414
	.cfi_endproc
.LFE2649:
	.size	_ZN6icu_6711MeasureUnit17createNewtonMeterER10UErrorCode, .-_ZN6icu_6711MeasureUnit17createNewtonMeterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14getNewtonMeterEv
	.type	_ZN6icu_6711MeasureUnit14getNewtonMeterEv, @function
_ZN6icu_6711MeasureUnit14getNewtonMeterEv:
.LFB2650:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$21, 18(%rdi)
	ret
	.cfi_endproc
.LFE2650:
	.size	_ZN6icu_6711MeasureUnit14getNewtonMeterEv, .-_ZN6icu_6711MeasureUnit14getNewtonMeterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createPoundFootER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createPoundFootER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createPoundFootER10UErrorCode:
.LFB2651:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1428
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1425
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$21, 18(%rax)
.L1423:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1428:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1425:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1423
	.cfi_endproc
.LFE2651:
	.size	_ZN6icu_6711MeasureUnit15createPoundFootER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createPoundFootER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getPoundFootEv
	.type	_ZN6icu_6711MeasureUnit12getPoundFootEv, @function
_ZN6icu_6711MeasureUnit12getPoundFootEv:
.LFB2652:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$21, 18(%rdi)
	ret
	.cfi_endproc
.LFE2652:
	.size	_ZN6icu_6711MeasureUnit12getPoundFootEv, .-_ZN6icu_6711MeasureUnit12getPoundFootEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14createAcreFootER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit14createAcreFootER10UErrorCode, @function
_ZN6icu_6711MeasureUnit14createAcreFootER10UErrorCode:
.LFB2653:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1437
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1434
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1432:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1437:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1434:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1432
	.cfi_endproc
.LFE2653:
	.size	_ZN6icu_6711MeasureUnit14createAcreFootER10UErrorCode, .-_ZN6icu_6711MeasureUnit14createAcreFootER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11getAcreFootEv
	.type	_ZN6icu_6711MeasureUnit11getAcreFootEv, @function
_ZN6icu_6711MeasureUnit11getAcreFootEv:
.LFB2654:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2654:
	.size	_ZN6icu_6711MeasureUnit11getAcreFootEv, .-_ZN6icu_6711MeasureUnit11getAcreFootEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createBarrelER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createBarrelER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createBarrelER10UErrorCode:
.LFB2655:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1446
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1443
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$1, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1441:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1446:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1443:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1441
	.cfi_endproc
.LFE2655:
	.size	_ZN6icu_6711MeasureUnit12createBarrelER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createBarrelER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getBarrelEv
	.type	_ZN6icu_6711MeasureUnit9getBarrelEv, @function
_ZN6icu_6711MeasureUnit9getBarrelEv:
.LFB2656:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$1, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2656:
	.size	_ZN6icu_6711MeasureUnit9getBarrelEv, .-_ZN6icu_6711MeasureUnit9getBarrelEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createBushelER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createBushelER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createBushelER10UErrorCode:
.LFB2657:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1455
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1452
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1450:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1455:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1452:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1450
	.cfi_endproc
.LFE2657:
	.size	_ZN6icu_6711MeasureUnit12createBushelER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createBushelER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getBushelEv
	.type	_ZN6icu_6711MeasureUnit9getBushelEv, @function
_ZN6icu_6711MeasureUnit9getBushelEv:
.LFB2658:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$2, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2658:
	.size	_ZN6icu_6711MeasureUnit9getBushelEv, .-_ZN6icu_6711MeasureUnit9getBushelEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createCentiliterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createCentiliterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createCentiliterER10UErrorCode:
.LFB2659:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1464
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1461
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$3, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1459:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1464:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1461:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1459
	.cfi_endproc
.LFE2659:
	.size	_ZN6icu_6711MeasureUnit16createCentiliterER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createCentiliterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getCentiliterEv
	.type	_ZN6icu_6711MeasureUnit13getCentiliterEv, @function
_ZN6icu_6711MeasureUnit13getCentiliterEv:
.LFB2660:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$3, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2660:
	.size	_ZN6icu_6711MeasureUnit13getCentiliterEv, .-_ZN6icu_6711MeasureUnit13getCentiliterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit21createCubicCentimeterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit21createCubicCentimeterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit21createCubicCentimeterER10UErrorCode:
.LFB2661:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1473
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1470
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$4, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1468:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1473:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1470:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1468
	.cfi_endproc
.LFE2661:
	.size	_ZN6icu_6711MeasureUnit21createCubicCentimeterER10UErrorCode, .-_ZN6icu_6711MeasureUnit21createCubicCentimeterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit18getCubicCentimeterEv
	.type	_ZN6icu_6711MeasureUnit18getCubicCentimeterEv, @function
_ZN6icu_6711MeasureUnit18getCubicCentimeterEv:
.LFB2662:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$4, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2662:
	.size	_ZN6icu_6711MeasureUnit18getCubicCentimeterEv, .-_ZN6icu_6711MeasureUnit18getCubicCentimeterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createCubicFootER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createCubicFootER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createCubicFootER10UErrorCode:
.LFB2663:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1482
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1479
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$5, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1477:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1482:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1479:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1477
	.cfi_endproc
.LFE2663:
	.size	_ZN6icu_6711MeasureUnit15createCubicFootER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createCubicFootER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getCubicFootEv
	.type	_ZN6icu_6711MeasureUnit12getCubicFootEv, @function
_ZN6icu_6711MeasureUnit12getCubicFootEv:
.LFB2664:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$5, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2664:
	.size	_ZN6icu_6711MeasureUnit12getCubicFootEv, .-_ZN6icu_6711MeasureUnit12getCubicFootEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createCubicInchER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createCubicInchER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createCubicInchER10UErrorCode:
.LFB2665:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1491
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1488
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$6, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1486:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1491:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1488:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1486
	.cfi_endproc
.LFE2665:
	.size	_ZN6icu_6711MeasureUnit15createCubicInchER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createCubicInchER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getCubicInchEv
	.type	_ZN6icu_6711MeasureUnit12getCubicInchEv, @function
_ZN6icu_6711MeasureUnit12getCubicInchEv:
.LFB2666:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$6, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2666:
	.size	_ZN6icu_6711MeasureUnit12getCubicInchEv, .-_ZN6icu_6711MeasureUnit12getCubicInchEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit20createCubicKilometerER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit20createCubicKilometerER10UErrorCode, @function
_ZN6icu_6711MeasureUnit20createCubicKilometerER10UErrorCode:
.LFB2667:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1500
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1497
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$7, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1495:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1500:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1497:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1495
	.cfi_endproc
.LFE2667:
	.size	_ZN6icu_6711MeasureUnit20createCubicKilometerER10UErrorCode, .-_ZN6icu_6711MeasureUnit20createCubicKilometerER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17getCubicKilometerEv
	.type	_ZN6icu_6711MeasureUnit17getCubicKilometerEv, @function
_ZN6icu_6711MeasureUnit17getCubicKilometerEv:
.LFB2668:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$7, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2668:
	.size	_ZN6icu_6711MeasureUnit17getCubicKilometerEv, .-_ZN6icu_6711MeasureUnit17getCubicKilometerEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createCubicMeterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createCubicMeterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createCubicMeterER10UErrorCode:
.LFB2669:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1509
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1506
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$8, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1504:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1509:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1506:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1504
	.cfi_endproc
.LFE2669:
	.size	_ZN6icu_6711MeasureUnit16createCubicMeterER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createCubicMeterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getCubicMeterEv
	.type	_ZN6icu_6711MeasureUnit13getCubicMeterEv, @function
_ZN6icu_6711MeasureUnit13getCubicMeterEv:
.LFB2670:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$8, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2670:
	.size	_ZN6icu_6711MeasureUnit13getCubicMeterEv, .-_ZN6icu_6711MeasureUnit13getCubicMeterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createCubicMileER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createCubicMileER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createCubicMileER10UErrorCode:
.LFB2671:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1518
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1515
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$9, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1513:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1518:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1515:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1513
	.cfi_endproc
.LFE2671:
	.size	_ZN6icu_6711MeasureUnit15createCubicMileER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createCubicMileER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getCubicMileEv
	.type	_ZN6icu_6711MeasureUnit12getCubicMileEv, @function
_ZN6icu_6711MeasureUnit12getCubicMileEv:
.LFB2672:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$9, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2672:
	.size	_ZN6icu_6711MeasureUnit12getCubicMileEv, .-_ZN6icu_6711MeasureUnit12getCubicMileEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createCubicYardER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createCubicYardER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createCubicYardER10UErrorCode:
.LFB2673:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1527
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1524
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$10, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1522:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1527:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1524:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1522
	.cfi_endproc
.LFE2673:
	.size	_ZN6icu_6711MeasureUnit15createCubicYardER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createCubicYardER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getCubicYardEv
	.type	_ZN6icu_6711MeasureUnit12getCubicYardEv, @function
_ZN6icu_6711MeasureUnit12getCubicYardEv:
.LFB2674:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$10, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2674:
	.size	_ZN6icu_6711MeasureUnit12getCubicYardEv, .-_ZN6icu_6711MeasureUnit12getCubicYardEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9createCupER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit9createCupER10UErrorCode, @function
_ZN6icu_6711MeasureUnit9createCupER10UErrorCode:
.LFB2675:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1536
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1533
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$11, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1531:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1536:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1533:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1531
	.cfi_endproc
.LFE2675:
	.size	_ZN6icu_6711MeasureUnit9createCupER10UErrorCode, .-_ZN6icu_6711MeasureUnit9createCupER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit6getCupEv
	.type	_ZN6icu_6711MeasureUnit6getCupEv, @function
_ZN6icu_6711MeasureUnit6getCupEv:
.LFB2676:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$11, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2676:
	.size	_ZN6icu_6711MeasureUnit6getCupEv, .-_ZN6icu_6711MeasureUnit6getCupEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createCupMetricER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createCupMetricER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createCupMetricER10UErrorCode:
.LFB2677:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1545
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1542
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$12, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1540:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1545:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1542:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1540
	.cfi_endproc
.LFE2677:
	.size	_ZN6icu_6711MeasureUnit15createCupMetricER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createCupMetricER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getCupMetricEv
	.type	_ZN6icu_6711MeasureUnit12getCupMetricEv, @function
_ZN6icu_6711MeasureUnit12getCupMetricEv:
.LFB2678:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$12, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2678:
	.size	_ZN6icu_6711MeasureUnit12getCupMetricEv, .-_ZN6icu_6711MeasureUnit12getCupMetricEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createDeciliterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createDeciliterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createDeciliterER10UErrorCode:
.LFB2679:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1554
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1551
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$13, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1549:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1554:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1551:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1549
	.cfi_endproc
.LFE2679:
	.size	_ZN6icu_6711MeasureUnit15createDeciliterER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createDeciliterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getDeciliterEv
	.type	_ZN6icu_6711MeasureUnit12getDeciliterEv, @function
_ZN6icu_6711MeasureUnit12getDeciliterEv:
.LFB2680:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$13, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2680:
	.size	_ZN6icu_6711MeasureUnit12getDeciliterEv, .-_ZN6icu_6711MeasureUnit12getDeciliterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createFluidOunceER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createFluidOunceER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createFluidOunceER10UErrorCode:
.LFB2681:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1563
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1560
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$14, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1558:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1563:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1560:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1558
	.cfi_endproc
.LFE2681:
	.size	_ZN6icu_6711MeasureUnit16createFluidOunceER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createFluidOunceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getFluidOunceEv
	.type	_ZN6icu_6711MeasureUnit13getFluidOunceEv, @function
_ZN6icu_6711MeasureUnit13getFluidOunceEv:
.LFB2682:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$14, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2682:
	.size	_ZN6icu_6711MeasureUnit13getFluidOunceEv, .-_ZN6icu_6711MeasureUnit13getFluidOunceEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit24createFluidOunceImperialER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit24createFluidOunceImperialER10UErrorCode, @function
_ZN6icu_6711MeasureUnit24createFluidOunceImperialER10UErrorCode:
.LFB2683:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1572
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1569
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$15, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1567:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1572:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1569:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1567
	.cfi_endproc
.LFE2683:
	.size	_ZN6icu_6711MeasureUnit24createFluidOunceImperialER10UErrorCode, .-_ZN6icu_6711MeasureUnit24createFluidOunceImperialER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit21getFluidOunceImperialEv
	.type	_ZN6icu_6711MeasureUnit21getFluidOunceImperialEv, @function
_ZN6icu_6711MeasureUnit21getFluidOunceImperialEv:
.LFB2684:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$15, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2684:
	.size	_ZN6icu_6711MeasureUnit21getFluidOunceImperialEv, .-_ZN6icu_6711MeasureUnit21getFluidOunceImperialEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12createGallonER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12createGallonER10UErrorCode, @function
_ZN6icu_6711MeasureUnit12createGallonER10UErrorCode:
.LFB2685:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1581
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1578
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$16, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1576:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1581:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1578:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1576
	.cfi_endproc
.LFE2685:
	.size	_ZN6icu_6711MeasureUnit12createGallonER10UErrorCode, .-_ZN6icu_6711MeasureUnit12createGallonER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit9getGallonEv
	.type	_ZN6icu_6711MeasureUnit9getGallonEv, @function
_ZN6icu_6711MeasureUnit9getGallonEv:
.LFB2686:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$16, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2686:
	.size	_ZN6icu_6711MeasureUnit9getGallonEv, .-_ZN6icu_6711MeasureUnit9getGallonEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit20createGallonImperialER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit20createGallonImperialER10UErrorCode, @function
_ZN6icu_6711MeasureUnit20createGallonImperialER10UErrorCode:
.LFB2687:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1590
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1587
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$17, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1585:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1590:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1587:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1585
	.cfi_endproc
.LFE2687:
	.size	_ZN6icu_6711MeasureUnit20createGallonImperialER10UErrorCode, .-_ZN6icu_6711MeasureUnit20createGallonImperialER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17getGallonImperialEv
	.type	_ZN6icu_6711MeasureUnit17getGallonImperialEv, @function
_ZN6icu_6711MeasureUnit17getGallonImperialEv:
.LFB2688:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$17, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2688:
	.size	_ZN6icu_6711MeasureUnit17getGallonImperialEv, .-_ZN6icu_6711MeasureUnit17getGallonImperialEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createHectoliterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createHectoliterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createHectoliterER10UErrorCode:
.LFB2689:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1599
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1596
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$18, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1594:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1599:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1596:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1594
	.cfi_endproc
.LFE2689:
	.size	_ZN6icu_6711MeasureUnit16createHectoliterER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createHectoliterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getHectoliterEv
	.type	_ZN6icu_6711MeasureUnit13getHectoliterEv, @function
_ZN6icu_6711MeasureUnit13getHectoliterEv:
.LFB2690:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$18, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2690:
	.size	_ZN6icu_6711MeasureUnit13getHectoliterEv, .-_ZN6icu_6711MeasureUnit13getHectoliterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11createLiterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit11createLiterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit11createLiterER10UErrorCode:
.LFB2691:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1608
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1605
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$19, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1603:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1608:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1605:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1603
	.cfi_endproc
.LFE2691:
	.size	_ZN6icu_6711MeasureUnit11createLiterER10UErrorCode, .-_ZN6icu_6711MeasureUnit11createLiterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8getLiterEv
	.type	_ZN6icu_6711MeasureUnit8getLiterEv, @function
_ZN6icu_6711MeasureUnit8getLiterEv:
.LFB2692:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$19, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2692:
	.size	_ZN6icu_6711MeasureUnit8getLiterEv, .-_ZN6icu_6711MeasureUnit8getLiterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit15createMegaliterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit15createMegaliterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit15createMegaliterER10UErrorCode:
.LFB2693:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1617
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1614
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$20, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1612:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1617:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1614:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1612
	.cfi_endproc
.LFE2693:
	.size	_ZN6icu_6711MeasureUnit15createMegaliterER10UErrorCode, .-_ZN6icu_6711MeasureUnit15createMegaliterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getMegaliterEv
	.type	_ZN6icu_6711MeasureUnit12getMegaliterEv, @function
_ZN6icu_6711MeasureUnit12getMegaliterEv:
.LFB2694:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$20, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2694:
	.size	_ZN6icu_6711MeasureUnit12getMegaliterEv, .-_ZN6icu_6711MeasureUnit12getMegaliterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createMilliliterER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createMilliliterER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createMilliliterER10UErrorCode:
.LFB2695:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1626
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1623
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$21, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1621:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1626:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1623:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1621
	.cfi_endproc
.LFE2695:
	.size	_ZN6icu_6711MeasureUnit16createMilliliterER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createMilliliterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getMilliliterEv
	.type	_ZN6icu_6711MeasureUnit13getMilliliterEv, @function
_ZN6icu_6711MeasureUnit13getMilliliterEv:
.LFB2696:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$21, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2696:
	.size	_ZN6icu_6711MeasureUnit13getMilliliterEv, .-_ZN6icu_6711MeasureUnit13getMilliliterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10createPintER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit10createPintER10UErrorCode, @function
_ZN6icu_6711MeasureUnit10createPintER10UErrorCode:
.LFB2697:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1635
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1632
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$22, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1630:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1635:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1632:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1630
	.cfi_endproc
.LFE2697:
	.size	_ZN6icu_6711MeasureUnit10createPintER10UErrorCode, .-_ZN6icu_6711MeasureUnit10createPintER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit7getPintEv
	.type	_ZN6icu_6711MeasureUnit7getPintEv, @function
_ZN6icu_6711MeasureUnit7getPintEv:
.LFB2698:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$22, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2698:
	.size	_ZN6icu_6711MeasureUnit7getPintEv, .-_ZN6icu_6711MeasureUnit7getPintEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createPintMetricER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createPintMetricER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createPintMetricER10UErrorCode:
.LFB2699:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1644
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1641
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$23, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1639:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1644:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1641:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1639
	.cfi_endproc
.LFE2699:
	.size	_ZN6icu_6711MeasureUnit16createPintMetricER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createPintMetricER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getPintMetricEv
	.type	_ZN6icu_6711MeasureUnit13getPintMetricEv, @function
_ZN6icu_6711MeasureUnit13getPintMetricEv:
.LFB2700:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$23, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2700:
	.size	_ZN6icu_6711MeasureUnit13getPintMetricEv, .-_ZN6icu_6711MeasureUnit13getPintMetricEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11createQuartER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit11createQuartER10UErrorCode, @function
_ZN6icu_6711MeasureUnit11createQuartER10UErrorCode:
.LFB2701:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1653
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1650
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$24, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1648:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1653:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1650:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1648
	.cfi_endproc
.LFE2701:
	.size	_ZN6icu_6711MeasureUnit11createQuartER10UErrorCode, .-_ZN6icu_6711MeasureUnit11createQuartER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8getQuartEv
	.type	_ZN6icu_6711MeasureUnit8getQuartEv, @function
_ZN6icu_6711MeasureUnit8getQuartEv:
.LFB2702:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$24, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2702:
	.size	_ZN6icu_6711MeasureUnit8getQuartEv, .-_ZN6icu_6711MeasureUnit8getQuartEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit16createTablespoonER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit16createTablespoonER10UErrorCode, @function
_ZN6icu_6711MeasureUnit16createTablespoonER10UErrorCode:
.LFB2703:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1662
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1659
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$25, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1657:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1662:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1659:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1657
	.cfi_endproc
.LFE2703:
	.size	_ZN6icu_6711MeasureUnit16createTablespoonER10UErrorCode, .-_ZN6icu_6711MeasureUnit16createTablespoonER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getTablespoonEv
	.type	_ZN6icu_6711MeasureUnit13getTablespoonEv, @function
_ZN6icu_6711MeasureUnit13getTablespoonEv:
.LFB2704:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$25, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2704:
	.size	_ZN6icu_6711MeasureUnit13getTablespoonEv, .-_ZN6icu_6711MeasureUnit13getTablespoonEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit14createTeaspoonER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit14createTeaspoonER10UErrorCode, @function
_ZN6icu_6711MeasureUnit14createTeaspoonER10UErrorCode:
.LFB2705:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1671
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1668
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movl	$26, %edx
	movw	%dx, 16(%rax)
	movb	$22, 18(%rax)
.L1666:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1671:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L1668:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1666
	.cfi_endproc
.LFE2705:
	.size	_ZN6icu_6711MeasureUnit14createTeaspoonER10UErrorCode, .-_ZN6icu_6711MeasureUnit14createTeaspoonER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit11getTeaspoonEv
	.type	_ZN6icu_6711MeasureUnit11getTeaspoonEv, @function
_ZN6icu_6711MeasureUnit11getTeaspoonEv:
.LFB2706:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdx
	movq	$0, 8(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$26, %edx
	movw	%dx, 16(%rdi)
	movb	$22, 18(%rdi)
	ret
	.cfi_endproc
.LFE2706:
	.size	_ZN6icu_6711MeasureUnit11getTeaspoonEv, .-_ZN6icu_6711MeasureUnit11getTeaspoonEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnitC2Ev
	.type	_ZN6icu_6711MeasureUnitC2Ev, @function
_ZN6icu_6711MeasureUnitC2Ev:
.LFB2709:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movw	%ax, 16(%rdi)
	movb	$16, 18(%rdi)
	ret
	.cfi_endproc
.LFE2709:
	.size	_ZN6icu_6711MeasureUnitC2Ev, .-_ZN6icu_6711MeasureUnitC2Ev
	.globl	_ZN6icu_6711MeasureUnitC1Ev
	.set	_ZN6icu_6711MeasureUnitC1Ev,_ZN6icu_6711MeasureUnitC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnitC2Eii
	.type	_ZN6icu_6711MeasureUnitC2Eii, @function
_ZN6icu_6711MeasureUnitC2Eii:
.LFB2712:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movw	%dx, 16(%rdi)
	movb	%sil, 18(%rdi)
	ret
	.cfi_endproc
.LFE2712:
	.size	_ZN6icu_6711MeasureUnitC2Eii, .-_ZN6icu_6711MeasureUnitC2Eii
	.globl	_ZN6icu_6711MeasureUnitC1Eii
	.set	_ZN6icu_6711MeasureUnitC1Eii,_ZN6icu_6711MeasureUnitC2Eii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnitC2EOS0_
	.type	_ZN6icu_6711MeasureUnitC2EOS0_, @function
_ZN6icu_6711MeasureUnitC2EOS0_:
.LFB2718:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rsi), %rax
	movq	$0, 8(%rsi)
	movq	%rax, 8(%rdi)
	movzwl	16(%rsi), %eax
	movw	%ax, 16(%rdi)
	movzbl	18(%rsi), %eax
	movb	%al, 18(%rdi)
	ret
	.cfi_endproc
.LFE2718:
	.size	_ZN6icu_6711MeasureUnitC2EOS0_, .-_ZN6icu_6711MeasureUnitC2EOS0_
	.globl	_ZN6icu_6711MeasureUnitC1EOS0_
	.set	_ZN6icu_6711MeasureUnitC1EOS0_,_ZN6icu_6711MeasureUnitC2EOS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnitaSERKS0_
	.type	_ZN6icu_6711MeasureUnitaSERKS0_, @function
_ZN6icu_6711MeasureUnitaSERKS0_:
.LFB2724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L1679
	movq	8(%rdi), %r14
	movq	%rsi, %rbx
	testq	%r14, %r14
	je	.L1680
	cmpb	$0, 108(%r14)
	jne	.L1727
.L1681:
	movl	8(%r14), %eax
	movq	16(%r14), %r8
	testl	%eax, %eax
	jle	.L1682
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	(%r8,%r13,8), %rdi
	testq	%rdi, %rdi
	je	.L1683
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	8(%r14), %eax
	addq	$1, %r13
	movq	16(%r14), %r8
	cmpl	%r13d, %eax
	jg	.L1686
.L1682:
	cmpb	$0, 28(%r14)
	jne	.L1728
.L1687:
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1680:
	cmpq	$0, 8(%rbx)
	je	.L1688
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	movl	$160, %edi
	movl	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1689
	movq	8(%rbx), %r14
	leaq	32(%rax), %rax
	xorl	%ecx, %ecx
	movl	$8, %edx
	movl	$0, -32(%rax)
	movl	$0, -24(%rax)
	movq	%rax, 16(%r15)
	leaq	109(%r15), %rax
	movw	%cx, 108(%r15)
	xorl	%ecx, %ecx
	movb	$0, 28(%r15)
	movq	%rax, 96(%r15)
	movl	(%r14), %eax
	movl	$0, 152(%r15)
	movl	$40, 104(%r15)
	movl	8(%r14), %esi
	movl	$8, 24(%r15)
	movl	%eax, (%r15)
	movq	$0, -104(%rbp)
	testl	%esi, %esi
	jle	.L1691
	.p2align 4,,10
	.p2align 3
.L1690:
	movq	16(%r14), %rax
	movq	-104(%rbp), %rsi
	movq	(%rax,%rsi,8), %r13
	cmpl	%ecx, %edx
	jne	.L1692
	cmpl	$8, %edx
	je	.L1693
	leal	(%rdx,%rdx), %r9d
	testl	%r9d, %r9d
	jg	.L1729
.L1711:
	movl	$7, -88(%rbp)
.L1691:
	movl	152(%r14), %edx
	movq	96(%r14), %rsi
	leaq	-88(%rbp), %rcx
	leaq	96(%r15), %rdi
	leaq	-96(%rbp), %r13
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-88(%rbp), %edx
	movq	%r15, 8(%r12)
	testl	%edx, %edx
	jle	.L1730
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %r14
	cmpb	$0, 108(%r15)
	movb	$16, -62(%rbp)
	movq	%r14, -80(%rbp)
	movq	$0, -72(%rbp)
	movw	%ax, -64(%rbp)
	jne	.L1731
.L1709:
	movl	8(%r15), %eax
	movq	16(%r15), %r9
	testl	%eax, %eax
	jle	.L1701
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1705:
	movq	(%r9,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L1702
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	8(%r15), %eax
	addq	$1, %rbx
	movq	16(%r15), %r9
	cmpl	%ebx, %eax
	jg	.L1705
.L1701:
	cmpb	$0, 28(%r15)
	jne	.L1732
.L1706:
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1710:
	movq	-72(%rbp), %rax
	leaq	-80(%rbp), %rdi
	movq	%r14, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, 8(%r12)
	movzbl	-62(%rbp), %eax
	movb	%al, 18(%r12)
	movzwl	-64(%rbp), %eax
	movw	%ax, 16(%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
.L1679:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1733
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1688:
	.cfi_restore_state
	movq	$0, 8(%r12)
.L1707:
	movzbl	18(%rbx), %eax
	movb	%al, 18(%r12)
	movzwl	16(%rbx), %eax
	movw	%ax, 16(%r12)
	jmp	.L1679
	.p2align 4,,10
	.p2align 3
.L1683:
	addq	$1, %r13
	cmpl	%r13d, %eax
	jg	.L1686
	cmpb	$0, 28(%r14)
	je	.L1687
.L1728:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1693:
	movl	$256, %edi
	movl	%edx, -112(%rbp)
	call	uprv_malloc_67@PLT
	movl	-112(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L1711
	movl	$32, %r9d
.L1712:
	cmpl	%edx, 24(%r15)
	cmovle	24(%r15), %edx
	movq	%rcx, %rdi
	movl	%r9d, -112(%rbp)
	movq	16(%r15), %rsi
	cmpl	%r9d, %edx
	cmovg	%r9d, %edx
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, 28(%r15)
	movl	-112(%rbp), %r9d
	movq	%rax, %rcx
	jne	.L1734
.L1696:
	movq	%rcx, 16(%r15)
	movl	%r9d, 24(%r15)
	movb	$1, 28(%r15)
.L1692:
	movl	$12, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1697
	movq	0(%r13), %rdx
	movq	16(%r15), %rsi
	addq	$1, -104(%rbp)
	movq	%rdx, (%rax)
	movl	8(%r13), %edx
	movl	%edx, 8(%rax)
	movslq	8(%r15), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, 8(%r15)
	movq	%rax, (%rsi,%rdx,8)
	movq	-104(%rbp), %rax
	cmpl	%eax, 8(%r14)
	jle	.L1691
	movl	24(%r15), %edx
	jmp	.L1690
	.p2align 4,,10
	.p2align 3
.L1729:
	movslq	%r9d, %rdi
	movl	%edx, -116(%rbp)
	salq	$3, %rdi
	movl	%r9d, -112(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1711
	movl	-116(%rbp), %edx
	movl	-112(%rbp), %r9d
	testl	%edx, %edx
	jg	.L1712
	cmpb	$0, 28(%r15)
	je	.L1696
.L1734:
	movq	16(%r15), %rdi
	movl	%r9d, -116(%rbp)
	movq	%rcx, -112(%rbp)
	call	uprv_free_67@PLT
	movl	-116(%rbp), %r9d
	movq	-112(%rbp), %rcx
	jmp	.L1696
	.p2align 4,,10
	.p2align 3
.L1702:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L1705
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L1730:
	movq	%r13, %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1727:
	movq	96(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L1681
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	96(%r15), %rdi
	call	uprv_free_67@PLT
	jmp	.L1709
	.p2align 4,,10
	.p2align 3
.L1732:
	movq	%r9, %rdi
	call	uprv_free_67@PLT
	jmp	.L1706
.L1697:
	movslq	8(%r15), %rax
	leal	1(%rax), %edx
	movl	%edx, 8(%r15)
	movq	16(%r15), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L1711
.L1733:
	call	__stack_chk_fail@PLT
.L1689:
	movb	$16, -62(%rbp)
	leaq	-96(%rbp), %r13
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %r14
	movq	$0, -72(%rbp)
	movw	$0, -64(%rbp)
	jmp	.L1710
	.cfi_endproc
.LFE2724:
	.size	_ZN6icu_6711MeasureUnitaSERKS0_, .-_ZN6icu_6711MeasureUnitaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnitC2ERKS0_
	.type	_ZN6icu_6711MeasureUnitC2ERKS0_, @function
_ZN6icu_6711MeasureUnitC2ERKS0_:
.LFB2715:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6711MeasureUnitaSERKS0_
	.cfi_endproc
.LFE2715:
	.size	_ZN6icu_6711MeasureUnitC2ERKS0_, .-_ZN6icu_6711MeasureUnitC2ERKS0_
	.globl	_ZN6icu_6711MeasureUnitC1ERKS0_
	.set	_ZN6icu_6711MeasureUnitC1ERKS0_,_ZN6icu_6711MeasureUnitC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit5cloneEv
	.type	_ZNK6icu_6711MeasureUnit5cloneEv, @function
_ZNK6icu_6711MeasureUnit5cloneEv:
.LFB2726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1736
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, 8(%r12)
	movq	%rax, (%r12)
	call	_ZN6icu_6711MeasureUnitaSERKS0_
.L1736:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2726:
	.size	_ZNK6icu_6711MeasureUnit5cloneEv, .-_ZNK6icu_6711MeasureUnit5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnitaSEOS0_
	.type	_ZN6icu_6711MeasureUnitaSEOS0_, @function
_ZN6icu_6711MeasureUnitaSEOS0_:
.LFB2725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	%rsi, %rdi
	je	.L1743
	movq	8(%rdi), %r14
	movq	%rsi, %r13
	testq	%r14, %r14
	je	.L1744
	cmpb	$0, 108(%r14)
	jne	.L1756
.L1745:
	movl	8(%r14), %eax
	movq	16(%r14), %r8
	testl	%eax, %eax
	jle	.L1746
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1750:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L1747
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	8(%r14), %eax
	addq	$1, %rbx
	movq	16(%r14), %r8
	cmpl	%ebx, %eax
	jg	.L1750
.L1746:
	cmpb	$0, 28(%r14)
	jne	.L1757
.L1751:
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1744:
	movq	8(%r13), %rax
	movq	%rax, 8(%r12)
	movzbl	18(%r13), %eax
	movq	$0, 8(%r13)
	movb	%al, 18(%r12)
	movzwl	16(%r13), %eax
	movw	%ax, 16(%r12)
.L1743:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1747:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L1750
	cmpb	$0, 28(%r14)
	je	.L1751
.L1757:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L1751
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	96(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L1745
	.cfi_endproc
.LFE2725:
	.size	_ZN6icu_6711MeasureUnitaSEOS0_, .-_ZN6icu_6711MeasureUnitaSEOS0_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit7getTypeEv
	.type	_ZNK6icu_6711MeasureUnit7getTypeEv, @function
_ZNK6icu_6711MeasureUnit7getTypeEv:
.LFB2731:
	.cfi_startproc
	endbr64
	movsbq	18(%rdi), %rax
	leaq	.LC0(%rip), %r8
	cmpb	$-1, %al
	je	.L1758
	leaq	_ZN6icu_67L6gTypesE(%rip), %rdx
	movq	(%rdx,%rax,8), %r8
.L1758:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE2731:
	.size	_ZNK6icu_6711MeasureUnit7getTypeEv, .-_ZNK6icu_6711MeasureUnit7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit10getSubtypeEv
	.type	_ZNK6icu_6711MeasureUnit10getSubtypeEv, @function
_ZNK6icu_6711MeasureUnit10getSubtypeEv:
.LFB2732:
	.cfi_startproc
	endbr64
	movsbq	18(%rdi), %rdx
	leaq	.LC0(%rip), %rax
	cmpb	$-1, %dl
	je	.L1762
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L1764
	movq	96(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1764:
	testb	%dl, %dl
	js	.L1762
	movswl	16(%rdi), %ecx
	testw	%cx, %cx
	js	.L1762
	leaq	_ZN6icu_67L8gOffsetsE(%rip), %rax
	addl	(%rax,%rdx,4), %ecx
	leaq	_ZN6icu_67L9gSubTypesE(%rip), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
.L1762:
	ret
	.cfi_endproc
.LFE2732:
	.size	_ZNK6icu_6711MeasureUnit10getSubtypeEv, .-_ZNK6icu_6711MeasureUnit10getSubtypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit13getIdentifierEv
	.type	_ZNK6icu_6711MeasureUnit13getIdentifierEv, @function
_ZNK6icu_6711MeasureUnit13getIdentifierEv:
.LFB2733:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L1769
	movq	96(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1769:
	movsbq	18(%rdi), %rcx
	testb	%cl, %cl
	js	.L1768
	movswl	16(%rdi), %edx
	testw	%dx, %dx
	js	.L1768
	leaq	_ZN6icu_67L8gOffsetsE(%rip), %rax
	addl	(%rax,%rcx,4), %edx
	leaq	_ZN6icu_67L9gSubTypesE(%rip), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
.L1768:
	ret
	.cfi_endproc
.LFE2733:
	.size	_ZNK6icu_6711MeasureUnit13getIdentifierEv, .-_ZNK6icu_6711MeasureUnit13getIdentifierEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit8getIndexEv
	.type	_ZNK6icu_6711MeasureUnit8getIndexEv, @function
_ZNK6icu_6711MeasureUnit8getIndexEv:
.LFB2735:
	.cfi_startproc
	endbr64
	movsbq	18(%rdi), %rcx
	leaq	_ZN6icu_67L8gIndexesE(%rip), %rdx
	movswl	16(%rdi), %eax
	addl	(%rdx,%rcx,4), %eax
	ret
	.cfi_endproc
.LFE2735:
	.size	_ZNK6icu_6711MeasureUnit8getIndexEv, .-_ZNK6icu_6711MeasureUnit8getIndexEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getAvailableEPS0_iR10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12getAvailableEPS0_iR10UErrorCode, @function
_ZN6icu_6711MeasureUnit12getAvailableEPS0_iR10UErrorCode:
.LFB2736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	movq	%rdi, -72(%rbp)
	testl	%ecx, %ecx
	jg	.L1773
	movq	$0, -64(%rbp)
	movl	$0, -56(%rbp)
	cmpl	$473, %esi
	jle	.L1796
	.p2align 4,,10
	.p2align 3
.L1775:
	movq	-64(%rbp), %rax
	leaq	4+_ZN6icu_67L8gOffsetsE(%rip), %rsi
	leaq	-4(%rsi), %rdx
	movl	(%rsi,%rax,4), %r12d
	subl	(%rdx,%rax,4), %r12d
	testl	%r12d, %r12d
	jle	.L1786
	movzbl	-64(%rbp), %eax
	movq	-72(%rbp), %rsi
	xorl	%ebx, %ebx
	movb	%al, -49(%rbp)
	movslq	-56(%rbp), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	8(%rsi,%rax,8), %r15
	.p2align 4,,10
	.p2align 3
.L1785:
	movzbl	-49(%rbp), %eax
	movq	(%r15), %r14
	movw	%bx, 8(%r15)
	movb	%al, 10(%r15)
	testq	%r14, %r14
	je	.L1777
	cmpb	$0, 108(%r14)
	jne	.L1797
.L1778:
	movl	8(%r14), %ecx
	movq	16(%r14), %r9
	testl	%ecx, %ecx
	jle	.L1779
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1783:
	movq	(%r9,%r13,8), %rdi
	testq	%rdi, %rdi
	je	.L1780
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	8(%r14), %ecx
	addq	$1, %r13
	movq	16(%r14), %r9
	cmpl	%r13d, %ecx
	jg	.L1783
.L1779:
	cmpb	$0, 28(%r14)
	jne	.L1798
.L1784:
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1777:
	addl	$1, %ebx
	movq	$0, (%r15)
	addq	$24, %r15
	cmpl	%ebx, %r12d
	jne	.L1785
	addl	%r12d, -56(%rbp)
.L1786:
	addq	$1, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	$23, %rax
	jne	.L1775
	movl	$474, %eax
.L1773:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1780:
	.cfi_restore_state
	addq	$1, %r13
	cmpl	%r13d, %ecx
	jg	.L1783
	cmpb	$0, 28(%r14)
	je	.L1784
.L1798:
	movq	%r9, %rdi
	call	uprv_free_67@PLT
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1797:
	movq	96(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L1778
.L1796:
	movl	$15, (%rdx)
	addq	$40, %rsp
	movl	$474, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2736:
	.size	_ZN6icu_6711MeasureUnit12getAvailableEPS0_iR10UErrorCode, .-_ZN6icu_6711MeasureUnit12getAvailableEPS0_iR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12getAvailableEPKcPS0_iR10UErrorCode
	.type	_ZN6icu_6711MeasureUnit12getAvailableEPKcPS0_iR10UErrorCode, @function
_ZN6icu_6711MeasureUnit12getAvailableEPKcPS0_iR10UErrorCode:
.LFB2737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -136(%rbp)
	movl	%edx, -124(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L1825
.L1799:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1826
	addq	$104, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1825:
	.cfi_restore_state
	leaq	-96(%rbp), %r8
	movq	%rdi, %rsi
	leaq	_ZN6icu_67L6gTypesE(%rip), %r14
	movq	%r8, %rdi
	leaq	-80(%rbp), %rbx
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-88(%rbp), %rax
	movl	$23, %r8d
	xorl	%r9d, %r9d
	movq	%rax, -120(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -112(%rbp)
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1803:
	je	.L1805
	movl	%r13d, %r8d
	cmpl	%r9d, %r8d
	jle	.L1802
.L1801:
	leal	(%r9,%r8), %ecx
	movq	%rbx, %rdi
	movl	%r8d, -104(%rbp)
	sarl	%ecx
	movl	%r9d, -100(%rbp)
	movslq	%ecx, %r13
	movq	(%r14,%r13,8), %rsi
	movq	%r13, %r15
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711StringPiece7compareES0_@PLT
	movl	-100(%rbp), %r9d
	movl	-104(%rbp), %r8d
	testl	%eax, %eax
	movl	%eax, %r12d
	jns	.L1803
	leal	1(%r13), %r9d
	cmpl	%r9d, %r8d
	jg	.L1801
.L1802:
	xorl	%r14d, %r14d
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1805:
	leal	1(%r13), %edx
	leaq	_ZN6icu_67L8gOffsetsE(%rip), %rsi
	movslq	%edx, %rdx
	movl	(%rsi,%rdx,4), %r14d
	subl	(%rsi,%r13,4), %r14d
	cmpl	-124(%rbp), %r14d
	jg	.L1827
	testl	%r14d, %r14d
	jle	.L1799
	movb	%r15b, -100(%rbp)
	movq	-136(%rbp), %r15
	addq	$8, %r15
	.p2align 4,,10
	.p2align 3
.L1814:
	movzbl	-100(%rbp), %eax
	movq	(%r15), %r13
	movw	%r12w, 8(%r15)
	movb	%al, 10(%r15)
	testq	%r13, %r13
	je	.L1806
	cmpb	$0, 108(%r13)
	jne	.L1828
.L1807:
	movl	8(%r13), %edx
	movq	16(%r13), %r8
	testl	%edx, %edx
	jle	.L1808
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1812:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L1809
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	8(%r13), %edx
	addq	$1, %rbx
	movq	16(%r13), %r8
	cmpl	%ebx, %edx
	jg	.L1812
.L1808:
	cmpb	$0, 28(%r13)
	jne	.L1829
.L1813:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1806:
	addl	$1, %r12d
	movq	$0, (%r15)
	addq	$24, %r15
	cmpl	%r12d, %r14d
	jne	.L1814
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1809:
	addq	$1, %rbx
	cmpl	%ebx, %edx
	jg	.L1812
	cmpb	$0, 28(%r13)
	je	.L1813
.L1829:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1828:
	movq	96(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1827:
	movq	-144(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L1799
.L1826:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2737:
	.size	_ZN6icu_6711MeasureUnit12getAvailableEPKcPS0_iR10UErrorCode, .-_ZN6icu_6711MeasureUnit12getAvailableEPKcPS0_iR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit17getAvailableTypesER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit17getAvailableTypesER10UErrorCode, @function
_ZN6icu_6711MeasureUnit17getAvailableTypesER10UErrorCode:
.LFB2738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movl	$23, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	_ZN6icu_67L6gTypesE(%rip), %rdi
	subq	$8, %rsp
	call	uenum_openCharStringsEnumeration_67@PLT
	movq	%rax, %r13
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1835
	movl	$128, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1833
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6718UStringEnumerationC1EP12UEnumeration@PLT
.L1830:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1835:
	.cfi_restore_state
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	uenum_close_67@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1833:
	.cfi_restore_state
	movl	$7, (%rbx)
	movq	%r13, %rdi
	call	uenum_close_67@PLT
	jmp	.L1830
	.cfi_endproc
.LFE2738:
	.size	_ZN6icu_6711MeasureUnit17getAvailableTypesER10UErrorCode, .-_ZN6icu_6711MeasureUnit17getAvailableTypesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13getIndexCountEv
	.type	_ZN6icu_6711MeasureUnit13getIndexCountEv, @function
_ZN6icu_6711MeasureUnit13getIndexCountEv:
.LFB2739:
	.cfi_startproc
	endbr64
	movl	$175, %eax
	ret
	.cfi_endproc
.LFE2739:
	.size	_ZN6icu_6711MeasureUnit13getIndexCountEv, .-_ZN6icu_6711MeasureUnit13getIndexCountEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit33internalGetIndexForTypeAndSubtypeEPKcS2_
	.type	_ZN6icu_6711MeasureUnit33internalGetIndexForTypeAndSubtypeEPKcS2_, @function
_ZN6icu_6711MeasureUnit33internalGetIndexForTypeAndSubtypeEPKcS2_:
.LFB2740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$88, %rsp
	movq	%rsi, -120(%rbp)
	movq	%rdi, %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %r15
	xorl	%r8d, %r8d
	movl	$23, %r9d
	leaq	_ZN6icu_67L6gTypesE(%rip), %r10
	movq	%rax, -104(%rbp)
	jmp	.L1838
	.p2align 4,,10
	.p2align 3
.L1840:
	je	.L1842
	movl	%r13d, %r9d
	cmpl	%r8d, %r9d
	jle	.L1839
.L1838:
	leal	(%r8,%r9), %ecx
	movq	%rbx, %rdi
	movl	%r9d, -112(%rbp)
	sarl	%ecx
	movl	%r8d, -108(%rbp)
	movslq	%ecx, %r13
	movq	(%r10,%r13,8), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-104(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711StringPiece7compareES0_@PLT
	movl	-108(%rbp), %r8d
	movl	-112(%rbp), %r9d
	leaq	_ZN6icu_67L6gTypesE(%rip), %r10
	testl	%eax, %eax
	jns	.L1840
	leal	1(%r13), %r8d
	cmpl	%r8d, %r9d
	jg	.L1838
	.p2align 4,,10
	.p2align 3
.L1839:
	movl	$-1, %r15d
.L1837:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1851
	addq	$88, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1842:
	.cfi_restore_state
	movq	-120(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	leal	1(%r13), %ecx
	leaq	_ZN6icu_67L8gOffsetsE(%rip), %rax
	movq	-88(%rbp), %rdx
	movslq	%ecx, %rcx
	movq	-96(%rbp), %r12
	movl	(%rax,%rcx,4), %r9d
	movl	(%rax,%r13,4), %eax
	movq	%rdx, -120(%rbp)
	movl	%eax, -124(%rbp)
	cmpl	%eax, %r9d
	jle	.L1839
	movl	%eax, %r14d
	jmp	.L1847
	.p2align 4,,10
	.p2align 3
.L1844:
	je	.L1852
	movl	%ecx, %r9d
.L1845:
	cmpl	%r9d, %r14d
	jge	.L1839
.L1847:
	leal	(%r9,%r14), %r11d
	leaq	_ZN6icu_67L9gSubTypesE(%rip), %rdi
	movl	%r9d, -112(%rbp)
	movl	%r11d, %ecx
	movl	%r11d, -108(%rbp)
	shrl	$31, %ecx
	addl	%r11d, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	movl	%ecx, -104(%rbp)
	movl	%ecx, %r15d
	movq	(%rdi,%rax,8), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-120(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711StringPiece7compareES0_@PLT
	movl	-104(%rbp), %ecx
	movl	-108(%rbp), %r11d
	testl	%eax, %eax
	movl	-112(%rbp), %r9d
	jns	.L1844
	leal	1(%rcx), %r14d
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1852:
	cmpl	$-1, %r11d
	jl	.L1837
	leaq	_ZN6icu_67L8gIndexesE(%rip), %rax
	addl	(%rax,%r13,4), %r15d
	subl	-124(%rbp), %r15d
	jmp	.L1837
.L1851:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2740:
	.size	_ZN6icu_6711MeasureUnit33internalGetIndexForTypeAndSubtypeEPKcS2_, .-_ZN6icu_6711MeasureUnit33internalGetIndexForTypeAndSubtypeEPKcS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13findBySubTypeENS_11StringPieceEPS0_
	.type	_ZN6icu_6711MeasureUnit13findBySubTypeENS_11StringPieceEPS0_, @function
_ZN6icu_6711MeasureUnit13findBySubTypeENS_11StringPieceEPS0_:
.LFB2741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$88, %rsp
	movq	%rdi, -96(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1854:
	leaq	_ZN6icu_67L8gIndexesE(%rip), %rax
	leaq	4(%rax), %rdx
	movl	(%rdx,%r12,4), %ecx
	cmpl	%ecx, (%rax,%r12,4)
	je	.L1869
	leaq	4+_ZN6icu_67L8gOffsetsE(%rip), %rax
	movl	(%rax,%r12,4), %r8d
	subq	$4, %rax
	movl	(%rax,%r12,4), %eax
	movl	%eax, -108(%rbp)
	cmpl	%eax, %r8d
	jle	.L1869
	movq	%rbx, %r14
	movq	%r12, -88(%rbp)
	movl	%eax, %r13d
	movl	%r8d, %ebx
	jmp	.L1858
	.p2align 4,,10
	.p2align 3
.L1859:
	je	.L1878
	movl	%r15d, %ebx
	cmpl	%r13d, %ebx
	jle	.L1876
.L1858:
	leal	(%rbx,%r13), %r12d
	leaq	_ZN6icu_67L9gSubTypesE(%rip), %rcx
	movq	%r14, %rdi
	movl	%r12d, %r15d
	shrl	$31, %r15d
	addl	%r12d, %r15d
	sarl	%r15d
	movslq	%r15d, %rax
	movq	(%rcx,%rax,8), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6711StringPiece7compareES0_@PLT
	testl	%eax, %eax
	jns	.L1859
	leal	1(%r15), %r13d
	cmpl	%r13d, %ebx
	jg	.L1858
.L1876:
	movq	-88(%rbp), %r12
	movq	%r14, %rbx
.L1869:
	addq	$1, %r12
	cmpq	$23, %r12
	jne	.L1854
	xorl	%eax, %eax
.L1853:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1879
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1878:
	.cfi_restore_state
	movq	%r14, %rbx
	movl	%r12d, %r14d
	movq	-88(%rbp), %r12
	cmpl	$-1, %r14d
	jl	.L1869
	movq	-120(%rbp), %rax
	movl	%r15d, %ecx
	subl	-108(%rbp), %ecx
	movb	%r12b, 18(%rax)
	movq	8(%rax), %r12
	movw	%cx, 16(%rax)
	testq	%r12, %r12
	je	.L1861
	cmpb	$0, 108(%r12)
	jne	.L1880
.L1862:
	movl	8(%r12), %eax
	movq	16(%r12), %r8
	testl	%eax, %eax
	jle	.L1863
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1867:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L1864
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	8(%r12), %eax
	addq	$1, %rbx
	movq	16(%r12), %r8
	cmpl	%ebx, %eax
	jg	.L1867
.L1863:
	cmpb	$0, 28(%r12)
	jne	.L1881
.L1868:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1861:
	movq	-120(%rbp), %rax
	movq	$0, 8(%rax)
	movl	$1, %eax
	jmp	.L1853
	.p2align 4,,10
	.p2align 3
.L1864:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L1867
	cmpb	$0, 28(%r12)
	je	.L1868
.L1881:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L1868
.L1880:
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L1862
.L1879:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2741:
	.size	_ZN6icu_6711MeasureUnit13findBySubTypeENS_11StringPieceEPS0_, .-_ZN6icu_6711MeasureUnit13findBySubTypeENS_11StringPieceEPS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnitC2EONS_15MeasureUnitImplE
	.type	_ZN6icu_6711MeasureUnitC2EONS_15MeasureUnitImplE, @function
_ZN6icu_6711MeasureUnitC2EONS_15MeasureUnitImplE:
.LFB2722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	movq	%r13, %rdx
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	$-1, %eax
	movq	$0, 8(%rdi)
	movw	%ax, 16(%rdi)
	movb	$-1, 18(%rdi)
	movq	96(%rsi), %rdi
	movl	152(%rsi), %esi
	call	_ZN6icu_6711MeasureUnit13findBySubTypeENS_11StringPieceEPS0_
	testb	%al, %al
	je	.L1891
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1891:
	.cfi_restore_state
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1884
	movl	(%rbx), %eax
	movq	16(%rbx), %rsi
	movl	%eax, (%r12)
	movl	8(%rbx), %eax
	movq	%rsi, 16(%r12)
	movl	%eax, 8(%r12)
	movzbl	28(%rbx), %edx
	movslq	24(%rbx), %rax
	movb	%dl, 28(%r12)
	leaq	32(%rbx), %rdx
	movl	%eax, 24(%r12)
	cmpq	%rdx, %rsi
	je	.L1892
	movq	%rdx, 16(%rbx)
	movl	$8, 24(%rbx)
	movb	$0, 28(%rbx)
.L1886:
	movl	$0, 8(%rbx)
	leaq	96(%rbx), %rsi
	leaq	96(%r12), %rdi
	call	_ZN6icu_6710CharStringC1EOS0_@PLT
.L1884:
	movq	%r12, 8(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1892:
	.cfi_restore_state
	leaq	32(%r12), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdi, 16(%r12)
	call	memcpy@PLT
	jmp	.L1886
	.cfi_endproc
.LFE2722:
	.size	_ZN6icu_6711MeasureUnitC2EONS_15MeasureUnitImplE, .-_ZN6icu_6711MeasureUnitC2EONS_15MeasureUnitImplE
	.globl	_ZN6icu_6711MeasureUnitC1EONS_15MeasureUnitImplE
	.set	_ZN6icu_6711MeasureUnitC1EONS_15MeasureUnitImplE,_ZN6icu_6711MeasureUnitC2EONS_15MeasureUnitImplE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit18resolveUnitPerUnitERKS0_S2_Pb
	.type	_ZN6icu_6711MeasureUnit18resolveUnitPerUnitERKS0_S2_Pb, @function
_ZN6icu_6711MeasureUnit18resolveUnitPerUnitERKS0_S2_Pb:
.LFB2742:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movsbq	18(%rsi), %rdi
	testb	%dil, %dil
	js	.L1904
	movswl	16(%rsi), %r10d
	testw	%r10w, %r10w
	js	.L1904
	leaq	_ZN6icu_67L8gOffsetsE(%rip), %rsi
	addl	(%rsi,%rdi,4), %r10d
.L1894:
	movsbq	18(%rdx), %rsi
	testb	%sil, %sil
	js	.L1899
	movswl	16(%rdx), %r11d
	testw	%r11w, %r11w
	js	.L1899
	leaq	_ZN6icu_67L8gOffsetsE(%rip), %rdx
	addl	(%rdx,%rsi,4), %r11d
	cmpl	$-1, %r10d
	je	.L1899
	movl	$10, %r8d
	xorl	%r9d, %r9d
	leaq	_ZN6icu_67L23unitPerUnitToSingleUnitE(%rip), %rdi
	cmpl	$-1, %r11d
	jne	.L1896
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L1911:
	jl	.L1910
	cmpl	%r11d, 4(%rdx)
	jg	.L1907
	jge	.L1902
.L1910:
	leal	1(%rsi), %r9d
	cmpl	%r8d, %r9d
	jge	.L1899
.L1896:
	leal	(%r9,%r8), %esi
	sarl	%esi
	movslq	%esi, %rdx
	salq	$4, %rdx
	addq	%rdi, %rdx
	cmpl	%r10d, (%rdx)
	jle	.L1911
.L1907:
	movl	%esi, %r8d
	cmpl	%r8d, %r9d
	jl	.L1896
.L1899:
	movb	$0, (%rcx)
	xorl	%edx, %edx
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movw	%dx, 16(%rax)
	movb	$16, 18(%rax)
	ret
.L1902:
	movb	$1, (%rcx)
	movl	12(%rdx), %ecx
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rdi
	movl	8(%rdx), %edx
	movq	%rdi, (%rax)
	movq	$0, 8(%rax)
	movw	%cx, 16(%rax)
	movb	%dl, 18(%rax)
	ret
.L1904:
	movl	$-1, %r10d
	jmp	.L1894
	.cfi_endproc
.LFE2742:
	.size	_ZN6icu_6711MeasureUnit18resolveUnitPerUnitERKS0_S2_Pb, .-_ZN6icu_6711MeasureUnit18resolveUnitPerUnitERKS0_S2_Pb
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit6createEiiR10UErrorCode
	.type	_ZN6icu_6711MeasureUnit6createEiiR10UErrorCode, @function
_ZN6icu_6711MeasureUnit6createEiiR10UErrorCode:
.LFB2743:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1917
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edi, %r12d
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1914
	leaq	16+_ZTVN6icu_6711MeasureUnitE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movw	%r13w, 16(%rax)
	movb	%r12b, 18(%rax)
.L1912:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1917:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
.L1914:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$7, (%rbx)
	jmp	.L1912
	.cfi_endproc
.LFE2743:
	.size	_ZN6icu_6711MeasureUnit6createEiiR10UErrorCode, .-_ZN6icu_6711MeasureUnit6createEiiR10UErrorCode
	.section	.rodata.str1.1
.LC1:
	.string	"duration"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit8initTimeEPKc
	.type	_ZN6icu_6711MeasureUnit8initTimeEPKc, @function
_ZN6icu_6711MeasureUnit8initTimeEPKc:
.LFB2744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	.LC1(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %r14
	xorl	%r8d, %r8d
	movl	$23, %r10d
	leaq	_ZN6icu_67L6gTypesE(%rip), %r11
	movq	%rax, -104(%rbp)
	jmp	.L1921
	.p2align 4,,10
	.p2align 3
.L1923:
	je	.L1922
	movl	%ecx, %r10d
	cmpl	%r10d, %r8d
	jge	.L1930
.L1921:
	leal	(%r8,%r10), %ecx
	movq	%r12, %rdi
	movl	%r10d, -116(%rbp)
	sarl	%ecx
	movl	%r8d, -112(%rbp)
	movslq	%ecx, %rax
	movl	%ecx, -108(%rbp)
	movq	(%r11,%rax,8), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-104(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711StringPiece7compareES0_@PLT
	movl	-108(%rbp), %ecx
	movl	-112(%rbp), %r8d
	leaq	_ZN6icu_67L6gTypesE(%rip), %r11
	testl	%eax, %eax
	movl	-116(%rbp), %r10d
	jns	.L1923
	leal	1(%rcx), %r8d
	cmpl	%r10d, %r8d
	jl	.L1921
.L1930:
	movl	$-1, %ecx
.L1922:
	movb	%cl, 18(%rbx)
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	_ZN6icu_67L8gOffsetsE(%rip), %r15
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movsbq	18(%rbx), %rax
	movq	-96(%rbp), %r13
	leal	1(%rax), %edx
	movl	(%r15,%rax,4), %r14d
	movq	-88(%rbp), %rax
	movslq	%edx, %rdx
	movl	(%r15,%rdx,4), %r9d
	movq	%rax, -104(%rbp)
	cmpl	%r14d, %r9d
	jg	.L1926
	jmp	.L1932
	.p2align 4,,10
	.p2align 3
.L1927:
	je	.L1936
	movl	%ecx, %r9d
	cmpl	%r9d, %r14d
	jge	.L1937
.L1926:
	leal	(%r9,%r14), %eax
	leaq	_ZN6icu_67L9gSubTypesE(%rip), %rdi
	movl	%r9d, -112(%rbp)
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	movl	%ecx, -108(%rbp)
	movq	(%rdi,%rax,8), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-104(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711StringPiece7compareES0_@PLT
	movl	-108(%rbp), %ecx
	movl	-112(%rbp), %r9d
	testl	%eax, %eax
	jns	.L1927
	leal	1(%rcx), %r14d
	cmpl	%r9d, %r14d
	jl	.L1926
.L1937:
	movsbq	18(%rbx), %rax
	movl	$-1, %ecx
	movl	(%r15,%rax,4), %r14d
.L1925:
	subl	%r14d, %ecx
	movw	%cx, 16(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1938
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1936:
	.cfi_restore_state
	movsbq	18(%rbx), %rax
	movl	(%r15,%rax,4), %r14d
	jmp	.L1925
	.p2align 4,,10
	.p2align 3
.L1932:
	movl	$-1, %ecx
	jmp	.L1925
.L1938:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2744:
	.size	_ZN6icu_6711MeasureUnit8initTimeEPKc, .-_ZN6icu_6711MeasureUnit8initTimeEPKc
	.section	.rodata.str1.1
.LC2:
	.string	"currency"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit12initCurrencyENS_11StringPieceE
	.type	_ZN6icu_6711MeasureUnit12initCurrencyENS_11StringPieceE, @function
_ZN6icu_6711MeasureUnit12initCurrencyENS_11StringPieceE:
.LFB2745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	.LC2(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-88(%rbp), %rax
	movq	%r12, -128(%rbp)
	movl	$23, %r8d
	movq	%r13, -136(%rbp)
	leaq	_ZN6icu_67L6gTypesE(%rip), %r10
	movl	%r8d, %r12d
	movq	%rax, -120(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -104(%rbp)
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L1942:
	je	.L1962
	movl	%ebx, %r12d
	cmpl	%r12d, %r15d
	jge	.L1941
.L1940:
	leal	(%r15,%r12), %ebx
	movq	%r14, %rdi
	sarl	%ebx
	movslq	%ebx, %r13
	movq	(%r10,%r13,8), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-120(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711StringPiece7compareES0_@PLT
	leaq	_ZN6icu_67L6gTypesE(%rip), %r10
	testl	%eax, %eax
	jns	.L1942
	leal	1(%rbx), %r15d
	cmpl	%r12d, %r15d
	jl	.L1940
.L1941:
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	movb	$-1, 18(%r12)
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L1962:
	leal	1(%rbx), %eax
	movq	%r13, %r9
	leaq	_ZN6icu_67L8gOffsetsE(%rip), %r15
	movq	-128(%rbp), %r12
	cltq
	movl	(%r15,%r9,4), %r9d
	movq	-136(%rbp), %r13
	movl	(%r15,%rax,4), %r8d
	movb	%bl, 18(%r12)
	cmpl	%r9d, %r8d
	jle	.L1955
	movq	%r12, -104(%rbp)
	movq	%r14, %r12
	movl	%r9d, %r14d
	movq	%r15, -128(%rbp)
	movl	%r8d, %r15d
	jmp	.L1945
	.p2align 4,,10
	.p2align 3
.L1946:
	je	.L1963
	movl	%ebx, %r15d
	cmpl	%r14d, %r15d
	jle	.L1960
.L1945:
	leal	(%r15,%r14), %eax
	leaq	_ZN6icu_67L9gSubTypesE(%rip), %rcx
	movq	%r12, %rdi
	movl	%eax, %ebx
	shrl	$31, %ebx
	addl	%eax, %ebx
	sarl	%ebx
	movslq	%ebx, %rax
	movl	%ebx, -120(%rbp)
	movq	(%rcx,%rax,8), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-112(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711StringPiece7compareES0_@PLT
	testl	%eax, %eax
	jns	.L1946
	leal	1(%rbx), %r14d
	cmpl	%r14d, %r15d
	jg	.L1945
.L1960:
	movq	%r12, %r14
	movq	-104(%rbp), %r12
.L1955:
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1949
	movl	$0, (%rax)
	leaq	32(%rax), %rax
	movl	-112(%rbp), %edx
	movq	%r14, %rcx
	movl	$0, -24(%rax)
	leaq	96(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, 16(%rbx)
	leaq	109(%rbx), %rax
	movq	%rax, 96(%rbx)
	xorl	%eax, %eax
	movl	$8, 24(%rbx)
	movb	$0, 28(%rbx)
	movl	$0, 152(%rbx)
	movl	$40, 104(%rbx)
	movw	%ax, 108(%rbx)
	movl	$0, -80(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	$-1, %edx
	movq	%rbx, 8(%r12)
	movw	%dx, 16(%r12)
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L1963:
	movl	-120(%rbp), %r10d
	movq	%r12, %r14
	movq	-128(%rbp), %r15
	movq	-104(%rbp), %r12
	cmpl	$-1, %r10d
	je	.L1955
.L1948:
	movsbq	18(%r12), %rax
	subw	(%r15,%rax,4), %r10w
	movw	%r10w, 16(%r12)
.L1939:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1964
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1949:
	.cfi_restore_state
	movq	$0, 8(%r12)
	movq	-144(%rbp), %rdi
	leaq	_ZN6icu_67L17kDefaultCurrency8E(%rip), %rsi
	leaq	_ZN6icu_67L8gOffsetsE(%rip), %r15
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movsbq	18(%r12), %rax
	movq	-96(%rbp), %r13
	movq	%r12, -104(%rbp)
	movq	-88(%rbp), %rbx
	movq	%r15, -112(%rbp)
	leaq	_ZN6icu_67L9gSubTypesE(%rip), %r9
	leal	1(%rax), %edx
	movl	(%r15,%rax,4), %r8d
	movq	%r13, %rax
	movq	%r14, %r13
	movslq	%edx, %rdx
	movq	%rbx, -120(%rbp)
	movq	%rax, %r14
	movl	(%r15,%rdx,4), %ecx
	movl	%r8d, %r15d
	movl	%ecx, %r12d
	jmp	.L1954
.L1952:
	je	.L1965
.L1953:
	movl	%ebx, %r12d
.L1954:
	cmpl	%r12d, %r15d
	jge	.L1959
	leal	(%r15,%r12), %eax
	movl	$2, %ecx
	movq	%r13, %rdi
	cltd
	idivl	%ecx
	cltq
	movq	(%r9,%rax,8), %rsi
	movq	%rax, %rbx
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-120(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711StringPiece7compareES0_@PLT
	leaq	_ZN6icu_67L9gSubTypesE(%rip), %r9
	testl	%eax, %eax
	jns	.L1952
	leal	1(%rbx), %r15d
	movl	%r12d, %ebx
	jmp	.L1953
.L1964:
	call	__stack_chk_fail@PLT
.L1959:
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r15
	orl	$-1, %r10d
	jmp	.L1948
.L1965:
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r15
	movl	%ebx, %r10d
	jmp	.L1948
	.cfi_endproc
.LFE2745:
	.size	_ZN6icu_6711MeasureUnit12initCurrencyENS_11StringPieceE, .-_ZN6icu_6711MeasureUnit12initCurrencyENS_11StringPieceE
	.section	.rodata.str1.1
.LC3:
	.string	"none"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit10initNoUnitEPKc
	.type	_ZN6icu_6711MeasureUnit10initNoUnitEPKc, @function
_ZN6icu_6711MeasureUnit10initNoUnitEPKc:
.LFB2746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	.LC3(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %r14
	xorl	%r8d, %r8d
	movl	$23, %r10d
	leaq	_ZN6icu_67L6gTypesE(%rip), %r11
	movq	%rax, -104(%rbp)
	jmp	.L1967
	.p2align 4,,10
	.p2align 3
.L1969:
	je	.L1968
	movl	%ecx, %r10d
	cmpl	%r10d, %r8d
	jge	.L1976
.L1967:
	leal	(%r8,%r10), %ecx
	movq	%r12, %rdi
	movl	%r10d, -116(%rbp)
	sarl	%ecx
	movl	%r8d, -112(%rbp)
	movslq	%ecx, %rax
	movl	%ecx, -108(%rbp)
	movq	(%r11,%rax,8), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-104(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711StringPiece7compareES0_@PLT
	movl	-108(%rbp), %ecx
	movl	-112(%rbp), %r8d
	leaq	_ZN6icu_67L6gTypesE(%rip), %r11
	testl	%eax, %eax
	movl	-116(%rbp), %r10d
	jns	.L1969
	leal	1(%rcx), %r8d
	cmpl	%r10d, %r8d
	jl	.L1967
.L1976:
	movl	$-1, %ecx
.L1968:
	movb	%cl, 18(%rbx)
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	_ZN6icu_67L8gOffsetsE(%rip), %r15
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movsbq	18(%rbx), %rax
	movq	-96(%rbp), %r13
	leal	1(%rax), %edx
	movl	(%r15,%rax,4), %r14d
	movq	-88(%rbp), %rax
	movslq	%edx, %rdx
	movl	(%r15,%rdx,4), %r9d
	movq	%rax, -104(%rbp)
	cmpl	%r14d, %r9d
	jg	.L1972
	jmp	.L1978
	.p2align 4,,10
	.p2align 3
.L1973:
	je	.L1982
	movl	%ecx, %r9d
	cmpl	%r9d, %r14d
	jge	.L1983
.L1972:
	leal	(%r9,%r14), %eax
	leaq	_ZN6icu_67L9gSubTypesE(%rip), %rdi
	movl	%r9d, -112(%rbp)
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	movl	%ecx, -108(%rbp)
	movq	(%rdi,%rax,8), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-104(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711StringPiece7compareES0_@PLT
	movl	-108(%rbp), %ecx
	movl	-112(%rbp), %r9d
	testl	%eax, %eax
	jns	.L1973
	leal	1(%rcx), %r14d
	cmpl	%r9d, %r14d
	jl	.L1972
.L1983:
	movsbq	18(%rbx), %rax
	movl	$-1, %ecx
	movl	(%r15,%rax,4), %r14d
.L1971:
	subl	%r14d, %ecx
	movw	%cx, 16(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1984
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1982:
	.cfi_restore_state
	movsbq	18(%rbx), %rax
	movl	(%r15,%rax,4), %r14d
	jmp	.L1971
	.p2align 4,,10
	.p2align 3
.L1978:
	movl	$-1, %ecx
	jmp	.L1971
.L1984:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2746:
	.size	_ZN6icu_6711MeasureUnit10initNoUnitEPKc, .-_ZN6icu_6711MeasureUnit10initNoUnitEPKc
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit5setToEii
	.type	_ZN6icu_6711MeasureUnit5setToEii, @function
_ZN6icu_6711MeasureUnit5setToEii:
.LFB2747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	movb	%sil, 18(%rdi)
	movw	%dx, 16(%rdi)
	testq	%r13, %r13
	je	.L1986
	cmpb	$0, 108(%r13)
	jne	.L1998
	movl	8(%r13), %eax
	movq	16(%r13), %r8
	testl	%eax, %eax
	jle	.L1988
.L2000:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1992:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L1989
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	8(%r13), %eax
	addq	$1, %rbx
	movq	16(%r13), %r8
	cmpl	%ebx, %eax
	jg	.L1992
.L1988:
	cmpb	$0, 28(%r13)
	jne	.L1999
.L1993:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1986:
	movq	$0, 8(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1989:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L1992
	cmpb	$0, 28(%r13)
	je	.L1993
	jmp	.L1999
	.p2align 4,,10
	.p2align 3
.L1998:
	movq	96(%r13), %rdi
	call	uprv_free_67@PLT
	movl	8(%r13), %eax
	movq	16(%r13), %r8
	testl	%eax, %eax
	jg	.L2000
	jmp	.L1988
	.p2align 4,,10
	.p2align 3
.L1999:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L1993
	.cfi_endproc
.LFE2747:
	.size	_ZN6icu_6711MeasureUnit5setToEii, .-_ZN6icu_6711MeasureUnit5setToEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit9getOffsetEv
	.type	_ZNK6icu_6711MeasureUnit9getOffsetEv, @function
_ZNK6icu_6711MeasureUnit9getOffsetEv:
.LFB2748:
	.cfi_startproc
	endbr64
	movsbq	18(%rdi), %rdx
	testb	%dl, %dl
	js	.L2004
	movswl	16(%rdi), %eax
	testw	%ax, %ax
	js	.L2004
	leaq	_ZN6icu_67L8gOffsetsE(%rip), %rcx
	addl	(%rcx,%rdx,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2004:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2748:
	.size	_ZNK6icu_6711MeasureUnit9getOffsetEv, .-_ZNK6icu_6711MeasureUnit9getOffsetEv
	.weak	_ZTSN6icu_6711MeasureUnitE
	.section	.rodata._ZTSN6icu_6711MeasureUnitE,"aG",@progbits,_ZTSN6icu_6711MeasureUnitE,comdat
	.align 16
	.type	_ZTSN6icu_6711MeasureUnitE, @object
	.size	_ZTSN6icu_6711MeasureUnitE, 23
_ZTSN6icu_6711MeasureUnitE:
	.string	"N6icu_6711MeasureUnitE"
	.weak	_ZTIN6icu_6711MeasureUnitE
	.section	.data.rel.ro._ZTIN6icu_6711MeasureUnitE,"awG",@progbits,_ZTIN6icu_6711MeasureUnitE,comdat
	.align 8
	.type	_ZTIN6icu_6711MeasureUnitE, @object
	.size	_ZTIN6icu_6711MeasureUnitE, 24
_ZTIN6icu_6711MeasureUnitE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6711MeasureUnitE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6711MeasureUnitE
	.section	.data.rel.ro.local._ZTVN6icu_6711MeasureUnitE,"awG",@progbits,_ZTVN6icu_6711MeasureUnitE,comdat
	.align 8
	.type	_ZTVN6icu_6711MeasureUnitE, @object
	.size	_ZTVN6icu_6711MeasureUnitE, 56
_ZTVN6icu_6711MeasureUnitE:
	.quad	0
	.quad	_ZTIN6icu_6711MeasureUnitE
	.quad	_ZN6icu_6711MeasureUnitD1Ev
	.quad	_ZN6icu_6711MeasureUnitD0Ev
	.quad	_ZNK6icu_6711MeasureUnit17getDynamicClassIDEv
	.quad	_ZNK6icu_6711MeasureUnit5cloneEv
	.quad	_ZNK6icu_6711MeasureUniteqERKNS_7UObjectE
	.data
	.align 32
	.type	_ZN6icu_67L23unitPerUnitToSingleUnitE, @object
	.size	_ZN6icu_67L23unitPerUnitToSingleUnitE, 160
_ZN6icu_67L23unitPerUnitToSingleUnitE:
	.long	378
	.long	382
	.long	12
	.long	5
	.long	378
	.long	387
	.long	12
	.long	6
	.long	388
	.long	343
	.long	19
	.zero	4
	.long	390
	.long	350
	.long	19
	.long	2
	.long	392
	.long	343
	.long	19
	.long	3
	.long	392
	.long	463
	.long	4
	.long	2
	.long	392
	.long	464
	.long	4
	.long	3
	.long	411
	.long	460
	.long	3
	.long	1
	.long	414
	.long	12
	.long	18
	.long	9
	.long	466
	.long	388
	.long	4
	.long	1
	.section	.rodata.str1.1
.LC4:
	.string	"g-force"
.LC5:
	.string	"meter-per-square-second"
.LC6:
	.string	"arc-minute"
.LC7:
	.string	"arc-second"
.LC8:
	.string	"degree"
.LC9:
	.string	"radian"
.LC10:
	.string	"revolution"
.LC11:
	.string	"acre"
.LC12:
	.string	"dunam"
.LC13:
	.string	"hectare"
.LC14:
	.string	"square-centimeter"
.LC15:
	.string	"square-foot"
.LC16:
	.string	"square-inch"
.LC17:
	.string	"square-kilometer"
.LC18:
	.string	"square-meter"
.LC19:
	.string	"square-mile"
.LC20:
	.string	"square-yard"
.LC21:
	.string	"karat"
.LC22:
	.string	"milligram-per-deciliter"
.LC23:
	.string	"millimole-per-liter"
.LC24:
	.string	"mole"
.LC25:
	.string	"percent"
.LC26:
	.string	"permille"
.LC27:
	.string	"permillion"
.LC28:
	.string	"permyriad"
.LC29:
	.string	"liter-per-100-kilometer"
.LC30:
	.string	"liter-per-kilometer"
.LC31:
	.string	"mile-per-gallon"
.LC32:
	.string	"mile-per-gallon-imperial"
.LC33:
	.string	"ADP"
.LC34:
	.string	"AED"
.LC35:
	.string	"AFA"
.LC36:
	.string	"AFN"
.LC37:
	.string	"ALK"
.LC38:
	.string	"ALL"
.LC39:
	.string	"AMD"
.LC40:
	.string	"ANG"
.LC41:
	.string	"AOA"
.LC42:
	.string	"AOK"
.LC43:
	.string	"AON"
.LC44:
	.string	"AOR"
.LC45:
	.string	"ARA"
.LC46:
	.string	"ARP"
.LC47:
	.string	"ARS"
.LC48:
	.string	"ARY"
.LC49:
	.string	"ATS"
.LC50:
	.string	"AUD"
.LC51:
	.string	"AWG"
.LC52:
	.string	"AYM"
.LC53:
	.string	"AZM"
.LC54:
	.string	"AZN"
.LC55:
	.string	"BAD"
.LC56:
	.string	"BAM"
.LC57:
	.string	"BBD"
.LC58:
	.string	"BDT"
.LC59:
	.string	"BEC"
.LC60:
	.string	"BEF"
.LC61:
	.string	"BEL"
.LC62:
	.string	"BGJ"
.LC63:
	.string	"BGK"
.LC64:
	.string	"BGL"
.LC65:
	.string	"BGN"
.LC66:
	.string	"BHD"
.LC67:
	.string	"BIF"
.LC68:
	.string	"BMD"
.LC69:
	.string	"BND"
.LC70:
	.string	"BOB"
.LC71:
	.string	"BOP"
.LC72:
	.string	"BOV"
.LC73:
	.string	"BRB"
.LC74:
	.string	"BRC"
.LC75:
	.string	"BRE"
.LC76:
	.string	"BRL"
.LC77:
	.string	"BRN"
.LC78:
	.string	"BRR"
.LC79:
	.string	"BSD"
.LC80:
	.string	"BTN"
.LC81:
	.string	"BUK"
.LC82:
	.string	"BWP"
.LC83:
	.string	"BYB"
.LC84:
	.string	"BYN"
.LC85:
	.string	"BYR"
.LC86:
	.string	"BZD"
.LC87:
	.string	"CAD"
.LC88:
	.string	"CDF"
.LC89:
	.string	"CHC"
.LC90:
	.string	"CHE"
.LC91:
	.string	"CHF"
.LC92:
	.string	"CHW"
.LC93:
	.string	"CLF"
.LC94:
	.string	"CLP"
.LC95:
	.string	"CNY"
.LC96:
	.string	"COP"
.LC97:
	.string	"COU"
.LC98:
	.string	"CRC"
.LC99:
	.string	"CSD"
.LC100:
	.string	"CSJ"
.LC101:
	.string	"CSK"
.LC102:
	.string	"CUC"
.LC103:
	.string	"CUP"
.LC104:
	.string	"CVE"
.LC105:
	.string	"CYP"
.LC106:
	.string	"CZK"
.LC107:
	.string	"DDM"
.LC108:
	.string	"DEM"
.LC109:
	.string	"DJF"
.LC110:
	.string	"DKK"
.LC111:
	.string	"DOP"
.LC112:
	.string	"DZD"
.LC113:
	.string	"ECS"
.LC114:
	.string	"ECV"
.LC115:
	.string	"EEK"
.LC116:
	.string	"EGP"
.LC117:
	.string	"ERN"
.LC118:
	.string	"ESA"
.LC119:
	.string	"ESB"
.LC120:
	.string	"ESP"
.LC121:
	.string	"ETB"
.LC122:
	.string	"EUR"
.LC123:
	.string	"FIM"
.LC124:
	.string	"FJD"
.LC125:
	.string	"FKP"
.LC126:
	.string	"FRF"
.LC127:
	.string	"GBP"
.LC128:
	.string	"GEK"
.LC129:
	.string	"GEL"
.LC130:
	.string	"GHC"
.LC131:
	.string	"GHP"
.LC132:
	.string	"GHS"
.LC133:
	.string	"GIP"
.LC134:
	.string	"GMD"
.LC135:
	.string	"GNE"
.LC136:
	.string	"GNF"
.LC137:
	.string	"GNS"
.LC138:
	.string	"GQE"
.LC139:
	.string	"GRD"
.LC140:
	.string	"GTQ"
.LC141:
	.string	"GWE"
.LC142:
	.string	"GWP"
.LC143:
	.string	"GYD"
.LC144:
	.string	"HKD"
.LC145:
	.string	"HNL"
.LC146:
	.string	"HRD"
.LC147:
	.string	"HRK"
.LC148:
	.string	"HTG"
.LC149:
	.string	"HUF"
.LC150:
	.string	"IDR"
.LC151:
	.string	"IEP"
.LC152:
	.string	"ILP"
.LC153:
	.string	"ILR"
.LC154:
	.string	"ILS"
.LC155:
	.string	"INR"
.LC156:
	.string	"IQD"
.LC157:
	.string	"IRR"
.LC158:
	.string	"ISJ"
.LC159:
	.string	"ISK"
.LC160:
	.string	"ITL"
.LC161:
	.string	"JMD"
.LC162:
	.string	"JOD"
.LC163:
	.string	"JPY"
.LC164:
	.string	"KES"
.LC165:
	.string	"KGS"
.LC166:
	.string	"KHR"
.LC167:
	.string	"KMF"
.LC168:
	.string	"KPW"
.LC169:
	.string	"KRW"
.LC170:
	.string	"KWD"
.LC171:
	.string	"KYD"
.LC172:
	.string	"KZT"
.LC173:
	.string	"LAJ"
.LC174:
	.string	"LAK"
.LC175:
	.string	"LBP"
.LC176:
	.string	"LKR"
.LC177:
	.string	"LRD"
.LC178:
	.string	"LSL"
.LC179:
	.string	"LSM"
.LC180:
	.string	"LTL"
.LC181:
	.string	"LTT"
.LC182:
	.string	"LUC"
.LC183:
	.string	"LUF"
.LC184:
	.string	"LUL"
.LC185:
	.string	"LVL"
.LC186:
	.string	"LVR"
.LC187:
	.string	"LYD"
.LC188:
	.string	"MAD"
.LC189:
	.string	"MDL"
.LC190:
	.string	"MGA"
.LC191:
	.string	"MGF"
.LC192:
	.string	"MKD"
.LC193:
	.string	"MLF"
.LC194:
	.string	"MMK"
.LC195:
	.string	"MNT"
.LC196:
	.string	"MOP"
.LC197:
	.string	"MRO"
.LC198:
	.string	"MRU"
.LC199:
	.string	"MTL"
.LC200:
	.string	"MTP"
.LC201:
	.string	"MUR"
.LC202:
	.string	"MVQ"
.LC203:
	.string	"MVR"
.LC204:
	.string	"MWK"
.LC205:
	.string	"MXN"
.LC206:
	.string	"MXP"
.LC207:
	.string	"MXV"
.LC208:
	.string	"MYR"
.LC209:
	.string	"MZE"
.LC210:
	.string	"MZM"
.LC211:
	.string	"MZN"
.LC212:
	.string	"NAD"
.LC213:
	.string	"NGN"
.LC214:
	.string	"NIC"
.LC215:
	.string	"NIO"
.LC216:
	.string	"NLG"
.LC217:
	.string	"NOK"
.LC218:
	.string	"NPR"
.LC219:
	.string	"NZD"
.LC220:
	.string	"OMR"
.LC221:
	.string	"PAB"
.LC222:
	.string	"PEH"
.LC223:
	.string	"PEI"
.LC224:
	.string	"PEN"
.LC225:
	.string	"PES"
.LC226:
	.string	"PGK"
.LC227:
	.string	"PHP"
.LC228:
	.string	"PKR"
.LC229:
	.string	"PLN"
.LC230:
	.string	"PLZ"
.LC231:
	.string	"PTE"
.LC232:
	.string	"PYG"
.LC233:
	.string	"QAR"
.LC234:
	.string	"RHD"
.LC235:
	.string	"ROK"
.LC236:
	.string	"ROL"
.LC237:
	.string	"RON"
.LC238:
	.string	"RSD"
.LC239:
	.string	"RUB"
.LC240:
	.string	"RUR"
.LC241:
	.string	"RWF"
.LC242:
	.string	"SAR"
.LC243:
	.string	"SBD"
.LC244:
	.string	"SCR"
.LC245:
	.string	"SDD"
.LC246:
	.string	"SDG"
.LC247:
	.string	"SDP"
.LC248:
	.string	"SEK"
.LC249:
	.string	"SGD"
.LC250:
	.string	"SHP"
.LC251:
	.string	"SIT"
.LC252:
	.string	"SKK"
.LC253:
	.string	"SLL"
.LC254:
	.string	"SOS"
.LC255:
	.string	"SRD"
.LC256:
	.string	"SRG"
.LC257:
	.string	"SSP"
.LC258:
	.string	"STD"
.LC259:
	.string	"STN"
.LC260:
	.string	"SUR"
.LC261:
	.string	"SVC"
.LC262:
	.string	"SYP"
.LC263:
	.string	"SZL"
.LC264:
	.string	"THB"
.LC265:
	.string	"TJR"
.LC266:
	.string	"TJS"
.LC267:
	.string	"TMM"
.LC268:
	.string	"TMT"
.LC269:
	.string	"TND"
.LC270:
	.string	"TOP"
.LC271:
	.string	"TPE"
.LC272:
	.string	"TRL"
.LC273:
	.string	"TRY"
.LC274:
	.string	"TTD"
.LC275:
	.string	"TWD"
.LC276:
	.string	"TZS"
.LC277:
	.string	"UAH"
.LC278:
	.string	"UAK"
.LC279:
	.string	"UGS"
.LC280:
	.string	"UGW"
.LC281:
	.string	"UGX"
.LC282:
	.string	"USD"
.LC283:
	.string	"USN"
.LC284:
	.string	"USS"
.LC285:
	.string	"UYI"
.LC286:
	.string	"UYN"
.LC287:
	.string	"UYP"
.LC288:
	.string	"UYU"
.LC289:
	.string	"UYW"
.LC290:
	.string	"UZS"
.LC291:
	.string	"VEB"
.LC292:
	.string	"VEF"
.LC293:
	.string	"VES"
.LC294:
	.string	"VNC"
.LC295:
	.string	"VND"
.LC296:
	.string	"VUV"
.LC297:
	.string	"WST"
.LC298:
	.string	"XAF"
.LC299:
	.string	"XAG"
.LC300:
	.string	"XAU"
.LC301:
	.string	"XBA"
.LC302:
	.string	"XBB"
.LC303:
	.string	"XBC"
.LC304:
	.string	"XBD"
.LC305:
	.string	"XCD"
.LC306:
	.string	"XDR"
.LC307:
	.string	"XEU"
.LC308:
	.string	"XOF"
.LC309:
	.string	"XPD"
.LC310:
	.string	"XPF"
.LC311:
	.string	"XPT"
.LC312:
	.string	"XSU"
.LC313:
	.string	"XTS"
.LC314:
	.string	"XUA"
.LC315:
	.string	"XXX"
.LC316:
	.string	"YDD"
.LC317:
	.string	"YER"
.LC318:
	.string	"YUD"
.LC319:
	.string	"YUM"
.LC320:
	.string	"YUN"
.LC321:
	.string	"ZAL"
.LC322:
	.string	"ZAR"
.LC323:
	.string	"ZMK"
.LC324:
	.string	"ZMW"
.LC325:
	.string	"ZRN"
.LC326:
	.string	"ZRZ"
.LC327:
	.string	"ZWC"
.LC328:
	.string	"ZWD"
.LC329:
	.string	"ZWL"
.LC330:
	.string	"ZWN"
.LC331:
	.string	"ZWR"
.LC332:
	.string	"bit"
.LC333:
	.string	"byte"
.LC334:
	.string	"gigabit"
.LC335:
	.string	"gigabyte"
.LC336:
	.string	"kilobit"
.LC337:
	.string	"kilobyte"
.LC338:
	.string	"megabit"
.LC339:
	.string	"megabyte"
.LC340:
	.string	"petabyte"
.LC341:
	.string	"terabit"
.LC342:
	.string	"terabyte"
.LC343:
	.string	"century"
.LC344:
	.string	"day"
.LC345:
	.string	"day-person"
.LC346:
	.string	"decade"
.LC347:
	.string	"hour"
.LC348:
	.string	"microsecond"
.LC349:
	.string	"millisecond"
.LC350:
	.string	"minute"
.LC351:
	.string	"month"
.LC352:
	.string	"month-person"
.LC353:
	.string	"nanosecond"
.LC354:
	.string	"second"
.LC355:
	.string	"week"
.LC356:
	.string	"week-person"
.LC357:
	.string	"year"
.LC358:
	.string	"year-person"
.LC359:
	.string	"ampere"
.LC360:
	.string	"milliampere"
.LC361:
	.string	"ohm"
.LC362:
	.string	"volt"
.LC363:
	.string	"british-thermal-unit"
.LC364:
	.string	"calorie"
.LC365:
	.string	"electronvolt"
.LC366:
	.string	"foodcalorie"
.LC367:
	.string	"joule"
.LC368:
	.string	"kilocalorie"
.LC369:
	.string	"kilojoule"
.LC370:
	.string	"kilowatt-hour"
.LC371:
	.string	"therm-us"
.LC372:
	.string	"newton"
.LC373:
	.string	"pound-force"
.LC374:
	.string	"gigahertz"
.LC375:
	.string	"hertz"
.LC376:
	.string	"kilohertz"
.LC377:
	.string	"megahertz"
.LC378:
	.string	"dot-per-centimeter"
.LC379:
	.string	"dot-per-inch"
.LC380:
	.string	"em"
.LC381:
	.string	"megapixel"
.LC382:
	.string	"pixel"
.LC383:
	.string	"pixel-per-centimeter"
.LC384:
	.string	"pixel-per-inch"
.LC385:
	.string	"astronomical-unit"
.LC386:
	.string	"centimeter"
.LC387:
	.string	"decimeter"
.LC388:
	.string	"fathom"
.LC389:
	.string	"foot"
.LC390:
	.string	"furlong"
.LC391:
	.string	"inch"
.LC392:
	.string	"kilometer"
.LC393:
	.string	"light-year"
.LC394:
	.string	"meter"
.LC395:
	.string	"micrometer"
.LC396:
	.string	"mile"
.LC397:
	.string	"mile-scandinavian"
.LC398:
	.string	"millimeter"
.LC399:
	.string	"nanometer"
.LC400:
	.string	"nautical-mile"
.LC401:
	.string	"parsec"
.LC402:
	.string	"picometer"
.LC403:
	.string	"point"
.LC404:
	.string	"solar-radius"
.LC405:
	.string	"yard"
.LC406:
	.string	"lux"
.LC407:
	.string	"solar-luminosity"
.LC408:
	.string	"carat"
.LC409:
	.string	"dalton"
.LC410:
	.string	"earth-mass"
.LC411:
	.string	"gram"
.LC412:
	.string	"kilogram"
.LC413:
	.string	"metric-ton"
.LC414:
	.string	"microgram"
.LC415:
	.string	"milligram"
.LC416:
	.string	"ounce"
.LC417:
	.string	"ounce-troy"
.LC418:
	.string	"pound"
.LC419:
	.string	"solar-mass"
.LC420:
	.string	"stone"
.LC421:
	.string	"ton"
.LC422:
	.string	"gigawatt"
.LC423:
	.string	"horsepower"
.LC424:
	.string	"kilowatt"
.LC425:
	.string	"megawatt"
.LC426:
	.string	"milliwatt"
.LC427:
	.string	"watt"
.LC428:
	.string	"atmosphere"
.LC429:
	.string	"bar"
.LC430:
	.string	"hectopascal"
.LC431:
	.string	"inch-ofhg"
.LC432:
	.string	"kilopascal"
.LC433:
	.string	"megapascal"
.LC434:
	.string	"millibar"
.LC435:
	.string	"millimeter-ofhg"
.LC436:
	.string	"pascal"
.LC437:
	.string	"pound-force-per-square-inch"
.LC438:
	.string	"kilometer-per-hour"
.LC439:
	.string	"knot"
.LC440:
	.string	"meter-per-second"
.LC441:
	.string	"mile-per-hour"
.LC442:
	.string	"celsius"
.LC443:
	.string	"fahrenheit"
.LC444:
	.string	"generic"
.LC445:
	.string	"kelvin"
.LC446:
	.string	"newton-meter"
.LC447:
	.string	"pound-force-foot"
.LC448:
	.string	"acre-foot"
.LC449:
	.string	"barrel"
.LC450:
	.string	"bushel"
.LC451:
	.string	"centiliter"
.LC452:
	.string	"cubic-centimeter"
.LC453:
	.string	"cubic-foot"
.LC454:
	.string	"cubic-inch"
.LC455:
	.string	"cubic-kilometer"
.LC456:
	.string	"cubic-meter"
.LC457:
	.string	"cubic-mile"
.LC458:
	.string	"cubic-yard"
.LC459:
	.string	"cup"
.LC460:
	.string	"cup-metric"
.LC461:
	.string	"deciliter"
.LC462:
	.string	"fluid-ounce"
.LC463:
	.string	"fluid-ounce-imperial"
.LC464:
	.string	"gallon"
.LC465:
	.string	"gallon-imperial"
.LC466:
	.string	"hectoliter"
.LC467:
	.string	"liter"
.LC468:
	.string	"megaliter"
.LC469:
	.string	"milliliter"
.LC470:
	.string	"pint"
.LC471:
	.string	"pint-metric"
.LC472:
	.string	"quart"
.LC473:
	.string	"tablespoon"
.LC474:
	.string	"teaspoon"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZN6icu_67L9gSubTypesE, @object
	.size	_ZN6icu_67L9gSubTypesE, 3792
_ZN6icu_67L9gSubTypesE:
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.quad	.LC81
	.quad	.LC82
	.quad	.LC83
	.quad	.LC84
	.quad	.LC85
	.quad	.LC86
	.quad	.LC87
	.quad	.LC88
	.quad	.LC89
	.quad	.LC90
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.quad	.LC94
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.quad	.LC113
	.quad	.LC114
	.quad	.LC115
	.quad	.LC116
	.quad	.LC117
	.quad	.LC118
	.quad	.LC119
	.quad	.LC120
	.quad	.LC121
	.quad	.LC122
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.quad	.LC129
	.quad	.LC130
	.quad	.LC131
	.quad	.LC132
	.quad	.LC133
	.quad	.LC134
	.quad	.LC135
	.quad	.LC136
	.quad	.LC137
	.quad	.LC138
	.quad	.LC139
	.quad	.LC140
	.quad	.LC141
	.quad	.LC142
	.quad	.LC143
	.quad	.LC144
	.quad	.LC145
	.quad	.LC146
	.quad	.LC147
	.quad	.LC148
	.quad	.LC149
	.quad	.LC150
	.quad	.LC151
	.quad	.LC152
	.quad	.LC153
	.quad	.LC154
	.quad	.LC155
	.quad	.LC156
	.quad	.LC157
	.quad	.LC158
	.quad	.LC159
	.quad	.LC160
	.quad	.LC161
	.quad	.LC162
	.quad	.LC163
	.quad	.LC164
	.quad	.LC165
	.quad	.LC166
	.quad	.LC167
	.quad	.LC168
	.quad	.LC169
	.quad	.LC170
	.quad	.LC171
	.quad	.LC172
	.quad	.LC173
	.quad	.LC174
	.quad	.LC175
	.quad	.LC176
	.quad	.LC177
	.quad	.LC178
	.quad	.LC179
	.quad	.LC180
	.quad	.LC181
	.quad	.LC182
	.quad	.LC183
	.quad	.LC184
	.quad	.LC185
	.quad	.LC186
	.quad	.LC187
	.quad	.LC188
	.quad	.LC189
	.quad	.LC190
	.quad	.LC191
	.quad	.LC192
	.quad	.LC193
	.quad	.LC194
	.quad	.LC195
	.quad	.LC196
	.quad	.LC197
	.quad	.LC198
	.quad	.LC199
	.quad	.LC200
	.quad	.LC201
	.quad	.LC202
	.quad	.LC203
	.quad	.LC204
	.quad	.LC205
	.quad	.LC206
	.quad	.LC207
	.quad	.LC208
	.quad	.LC209
	.quad	.LC210
	.quad	.LC211
	.quad	.LC212
	.quad	.LC213
	.quad	.LC214
	.quad	.LC215
	.quad	.LC216
	.quad	.LC217
	.quad	.LC218
	.quad	.LC219
	.quad	.LC220
	.quad	.LC221
	.quad	.LC222
	.quad	.LC223
	.quad	.LC224
	.quad	.LC225
	.quad	.LC226
	.quad	.LC227
	.quad	.LC228
	.quad	.LC229
	.quad	.LC230
	.quad	.LC231
	.quad	.LC232
	.quad	.LC233
	.quad	.LC234
	.quad	.LC235
	.quad	.LC236
	.quad	.LC237
	.quad	.LC238
	.quad	.LC239
	.quad	.LC240
	.quad	.LC241
	.quad	.LC242
	.quad	.LC243
	.quad	.LC244
	.quad	.LC245
	.quad	.LC246
	.quad	.LC247
	.quad	.LC248
	.quad	.LC249
	.quad	.LC250
	.quad	.LC251
	.quad	.LC252
	.quad	.LC253
	.quad	.LC254
	.quad	.LC255
	.quad	.LC256
	.quad	.LC257
	.quad	.LC258
	.quad	.LC259
	.quad	.LC260
	.quad	.LC261
	.quad	.LC262
	.quad	.LC263
	.quad	.LC264
	.quad	.LC265
	.quad	.LC266
	.quad	.LC267
	.quad	.LC268
	.quad	.LC269
	.quad	.LC270
	.quad	.LC271
	.quad	.LC272
	.quad	.LC273
	.quad	.LC274
	.quad	.LC275
	.quad	.LC276
	.quad	.LC277
	.quad	.LC278
	.quad	.LC279
	.quad	.LC280
	.quad	.LC281
	.quad	.LC282
	.quad	.LC283
	.quad	.LC284
	.quad	.LC285
	.quad	.LC286
	.quad	.LC287
	.quad	.LC288
	.quad	.LC289
	.quad	.LC290
	.quad	.LC291
	.quad	.LC292
	.quad	.LC293
	.quad	.LC294
	.quad	.LC295
	.quad	.LC296
	.quad	.LC297
	.quad	.LC298
	.quad	.LC299
	.quad	.LC300
	.quad	.LC301
	.quad	.LC302
	.quad	.LC303
	.quad	.LC304
	.quad	.LC305
	.quad	.LC306
	.quad	.LC307
	.quad	.LC308
	.quad	.LC309
	.quad	.LC310
	.quad	.LC311
	.quad	.LC312
	.quad	.LC313
	.quad	.LC314
	.quad	.LC315
	.quad	.LC316
	.quad	.LC317
	.quad	.LC318
	.quad	.LC319
	.quad	.LC320
	.quad	.LC321
	.quad	.LC322
	.quad	.LC323
	.quad	.LC324
	.quad	.LC325
	.quad	.LC326
	.quad	.LC327
	.quad	.LC328
	.quad	.LC329
	.quad	.LC330
	.quad	.LC331
	.quad	.LC332
	.quad	.LC333
	.quad	.LC334
	.quad	.LC335
	.quad	.LC336
	.quad	.LC337
	.quad	.LC338
	.quad	.LC339
	.quad	.LC340
	.quad	.LC341
	.quad	.LC342
	.quad	.LC343
	.quad	.LC344
	.quad	.LC345
	.quad	.LC346
	.quad	.LC347
	.quad	.LC348
	.quad	.LC349
	.quad	.LC350
	.quad	.LC351
	.quad	.LC352
	.quad	.LC353
	.quad	.LC354
	.quad	.LC355
	.quad	.LC356
	.quad	.LC357
	.quad	.LC358
	.quad	.LC359
	.quad	.LC360
	.quad	.LC361
	.quad	.LC362
	.quad	.LC363
	.quad	.LC364
	.quad	.LC365
	.quad	.LC366
	.quad	.LC367
	.quad	.LC368
	.quad	.LC369
	.quad	.LC370
	.quad	.LC371
	.quad	.LC372
	.quad	.LC373
	.quad	.LC374
	.quad	.LC375
	.quad	.LC376
	.quad	.LC377
	.quad	.LC378
	.quad	.LC379
	.quad	.LC380
	.quad	.LC381
	.quad	.LC382
	.quad	.LC383
	.quad	.LC384
	.quad	.LC385
	.quad	.LC386
	.quad	.LC387
	.quad	.LC388
	.quad	.LC389
	.quad	.LC390
	.quad	.LC391
	.quad	.LC392
	.quad	.LC393
	.quad	.LC394
	.quad	.LC395
	.quad	.LC396
	.quad	.LC397
	.quad	.LC398
	.quad	.LC399
	.quad	.LC400
	.quad	.LC401
	.quad	.LC402
	.quad	.LC403
	.quad	.LC404
	.quad	.LC405
	.quad	.LC406
	.quad	.LC407
	.quad	.LC408
	.quad	.LC409
	.quad	.LC410
	.quad	.LC411
	.quad	.LC412
	.quad	.LC413
	.quad	.LC414
	.quad	.LC415
	.quad	.LC416
	.quad	.LC417
	.quad	.LC418
	.quad	.LC419
	.quad	.LC420
	.quad	.LC421
	.quad	.LC0
	.quad	.LC25
	.quad	.LC26
	.quad	.LC422
	.quad	.LC423
	.quad	.LC424
	.quad	.LC425
	.quad	.LC426
	.quad	.LC427
	.quad	.LC428
	.quad	.LC429
	.quad	.LC430
	.quad	.LC431
	.quad	.LC432
	.quad	.LC433
	.quad	.LC434
	.quad	.LC435
	.quad	.LC436
	.quad	.LC437
	.quad	.LC438
	.quad	.LC439
	.quad	.LC440
	.quad	.LC441
	.quad	.LC442
	.quad	.LC443
	.quad	.LC444
	.quad	.LC445
	.quad	.LC446
	.quad	.LC447
	.quad	.LC448
	.quad	.LC449
	.quad	.LC450
	.quad	.LC451
	.quad	.LC452
	.quad	.LC453
	.quad	.LC454
	.quad	.LC455
	.quad	.LC456
	.quad	.LC457
	.quad	.LC458
	.quad	.LC459
	.quad	.LC460
	.quad	.LC461
	.quad	.LC462
	.quad	.LC463
	.quad	.LC464
	.quad	.LC465
	.quad	.LC466
	.quad	.LC467
	.quad	.LC468
	.quad	.LC469
	.quad	.LC470
	.quad	.LC471
	.quad	.LC472
	.quad	.LC473
	.quad	.LC474
	.section	.rodata.str1.1
.LC475:
	.string	"acceleration"
.LC476:
	.string	"angle"
.LC477:
	.string	"area"
.LC478:
	.string	"concentr"
.LC479:
	.string	"consumption"
.LC480:
	.string	"digital"
.LC481:
	.string	"electric"
.LC482:
	.string	"energy"
.LC483:
	.string	"force"
.LC484:
	.string	"frequency"
.LC485:
	.string	"graphics"
.LC486:
	.string	"length"
.LC487:
	.string	"light"
.LC488:
	.string	"mass"
.LC489:
	.string	"power"
.LC490:
	.string	"pressure"
.LC491:
	.string	"speed"
.LC492:
	.string	"temperature"
.LC493:
	.string	"torque"
.LC494:
	.string	"volume"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZN6icu_67L6gTypesE, @object
	.size	_ZN6icu_67L6gTypesE, 184
_ZN6icu_67L6gTypesE:
	.quad	.LC475
	.quad	.LC476
	.quad	.LC477
	.quad	.LC478
	.quad	.LC479
	.quad	.LC2
	.quad	.LC480
	.quad	.LC1
	.quad	.LC481
	.quad	.LC482
	.quad	.LC483
	.quad	.LC484
	.quad	.LC485
	.quad	.LC486
	.quad	.LC487
	.quad	.LC488
	.quad	.LC3
	.quad	.LC489
	.quad	.LC490
	.quad	.LC491
	.quad	.LC492
	.quad	.LC493
	.quad	.LC494
	.section	.rodata
	.align 32
	.type	_ZN6icu_67L8gIndexesE, @object
	.size	_ZN6icu_67L8gIndexesE, 96
_ZN6icu_67L8gIndexesE:
	.long	0
	.long	2
	.long	7
	.long	17
	.long	25
	.long	29
	.long	29
	.long	40
	.long	56
	.long	60
	.long	69
	.long	71
	.long	75
	.long	82
	.long	103
	.long	105
	.long	119
	.long	122
	.long	128
	.long	138
	.long	142
	.long	146
	.long	148
	.long	175
	.align 32
	.type	_ZN6icu_67L8gOffsetsE, @object
	.size	_ZN6icu_67L8gOffsetsE, 96
_ZN6icu_67L8gOffsetsE:
	.long	0
	.long	2
	.long	7
	.long	17
	.long	25
	.long	29
	.long	328
	.long	339
	.long	355
	.long	359
	.long	368
	.long	370
	.long	374
	.long	381
	.long	402
	.long	404
	.long	418
	.long	421
	.long	427
	.long	437
	.long	441
	.long	445
	.long	447
	.long	474
	.local	_ZZN6icu_6711MeasureUnit16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6711MeasureUnit16getStaticClassIDEvE7classID,1,1
	.type	_ZN6icu_67L17kDefaultCurrency8E, @object
	.size	_ZN6icu_67L17kDefaultCurrency8E, 4
_ZN6icu_67L17kDefaultCurrency8E:
	.string	"XXX"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
