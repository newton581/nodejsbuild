	.file	"numparse_affixes.cpp"
	.text
	.section	.text._ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv,"axG",@progbits,_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.type	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv, @function
_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv:
.LFB2799:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2799:
	.size	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv, .-_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.section	.text._ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE,"axG",@progbits,_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.type	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE, @function
_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE:
.LFB2800:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2800:
	.size	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE, .-_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.section	.text._ZN6icu_678numparse4impl12AffixMatcherD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl12AffixMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl12AffixMatcherD2Ev
	.type	_ZN6icu_678numparse4impl12AffixMatcherD2Ev, @function
_ZN6icu_678numparse4impl12AffixMatcherD2Ev:
.LFB4665:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678numparse4impl12AffixMatcherE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	.cfi_endproc
.LFE4665:
	.size	_ZN6icu_678numparse4impl12AffixMatcherD2Ev, .-_ZN6icu_678numparse4impl12AffixMatcherD2Ev
	.weak	_ZN6icu_678numparse4impl12AffixMatcherD1Ev
	.set	_ZN6icu_678numparse4impl12AffixMatcherD1Ev,_ZN6icu_678numparse4impl12AffixMatcherD2Ev
	.section	.text._ZN6icu_678numparse4impl16CodePointMatcherD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl16CodePointMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl16CodePointMatcherD2Ev
	.type	_ZN6icu_678numparse4impl16CodePointMatcherD2Ev, @function
_ZN6icu_678numparse4impl16CodePointMatcherD2Ev:
.LFB4000:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678numparse4impl16CodePointMatcherE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	.cfi_endproc
.LFE4000:
	.size	_ZN6icu_678numparse4impl16CodePointMatcherD2Ev, .-_ZN6icu_678numparse4impl16CodePointMatcherD2Ev
	.weak	_ZN6icu_678numparse4impl16CodePointMatcherD1Ev
	.set	_ZN6icu_678numparse4impl16CodePointMatcherD1Ev,_ZN6icu_678numparse4impl16CodePointMatcherD2Ev
	.section	.text._ZN6icu_678numparse4impl12AffixMatcherD0Ev,"axG",@progbits,_ZN6icu_678numparse4impl12AffixMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl12AffixMatcherD0Ev
	.type	_ZN6icu_678numparse4impl12AffixMatcherD0Ev, @function
_ZN6icu_678numparse4impl12AffixMatcherD0Ev:
.LFB4667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl12AffixMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4667:
	.size	_ZN6icu_678numparse4impl12AffixMatcherD0Ev, .-_ZN6icu_678numparse4impl12AffixMatcherD0Ev
	.section	.text._ZN6icu_678numparse4impl16CodePointMatcherD0Ev,"axG",@progbits,_ZN6icu_678numparse4impl16CodePointMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl16CodePointMatcherD0Ev
	.type	_ZN6icu_678numparse4impl16CodePointMatcherD0Ev, @function
_ZN6icu_678numparse4impl16CodePointMatcherD0Ev:
.LFB4002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl16CodePointMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4002:
	.size	_ZN6icu_678numparse4impl16CodePointMatcherD0Ev, .-_ZN6icu_678numparse4impl16CodePointMatcherD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl16CodePointMatcher9smokeTestERKNS_13StringSegmentE
	.type	_ZNK6icu_678numparse4impl16CodePointMatcher9smokeTestERKNS_13StringSegmentE, @function
_ZNK6icu_678numparse4impl16CodePointMatcher9smokeTestERKNS_13StringSegmentE:
.LFB3437:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movl	8(%r8), %esi
	jmp	_ZNK6icu_6713StringSegment10startsWithEi@PLT
	.cfi_endproc
.LFE3437:
	.size	_ZNK6icu_678numparse4impl16CodePointMatcher9smokeTestERKNS_13StringSegmentE, .-_ZNK6icu_678numparse4impl16CodePointMatcher9smokeTestERKNS_13StringSegmentE
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"<"
	.string	"C"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	"P"
	.string	"o"
	.string	"i"
	.string	"n"
	.string	"t"
	.string	">"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl16CodePointMatcher8toStringEv
	.type	_ZNK6icu_678numparse4impl16CodePointMatcher8toStringEv, @function
_ZNK6icu_678numparse4impl16CodePointMatcher8toStringEv:
.LFB3438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3438:
	.size	_ZNK6icu_678numparse4impl16CodePointMatcher8toStringEv, .-_ZNK6icu_678numparse4impl16CodePointMatcher8toStringEv
	.section	.text._ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD2Ev
	.type	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD2Ev, @function
_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD2Ev:
.LFB4669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN6icu_678numparse4impl26AffixPatternMatcherBuilderE(%rip), %rax
	leaq	-48(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 28(%rdi)
	movups	%xmm0, (%rdi)
	jne	.L16
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl13TokenConsumerD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl13TokenConsumerD2Ev@PLT
	.cfi_endproc
.LFE4669:
	.size	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD2Ev, .-_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD2Ev
	.weak	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD1Ev
	.set	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD1Ev,_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD2Ev
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_1L7matchedEPKN6icu_678numparse4impl19AffixPatternMatcherERKNS0_13UnicodeStringE, @function
_ZN12_GLOBAL__N_1L7matchedEPKN6icu_678numparse4impl19AffixPatternMatcherERKNS0_13UnicodeStringE:
.LFB3339:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$104, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L32
	movq	56(%rdi), %rax
	leaq	-112(%rbp), %r14
	leaq	-120(%rbp), %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-104(%rbp), %edx
	testb	$1, %dl
	je	.L20
	movzwl	8(%r13), %r12d
	andl	$1, %r12d
.L21:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L17:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	testw	%dx, %dx
	js	.L22
	sarl	$5, %edx
.L23:
	movzwl	8(%r13), %r12d
	testw	%r12w, %r12w
	js	.L24
	movswl	%r12w, %eax
	sarl	$5, %eax
.L25:
	cmpl	%edx, %eax
	notl	%r12d
	sete	%al
	andb	%al, %r12b
	je	.L21
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r12b
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L32:
	movzwl	8(%rsi), %r12d
	andl	$1, %r12d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L22:
	movl	-100(%rbp), %edx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L24:
	movl	12(%r13), %eax
	jmp	.L25
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3339:
	.size	_ZN12_GLOBAL__N_1L7matchedEPKN6icu_678numparse4impl19AffixPatternMatcherERKNS0_13UnicodeStringE, .-_ZN12_GLOBAL__N_1L7matchedEPKN6icu_678numparse4impl19AffixPatternMatcherERKNS0_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl12AffixMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.type	_ZNK6icu_678numparse4impl12AffixMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, @function
_ZNK6icu_678numparse4impl12AffixMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode:
.LFB3469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_678numparse4impl12ParsedNumber10seenNumberEv@PLT
	testb	%al, %al
	jne	.L35
	testb	$1, 88(%rbx)
	je	.L38
	cmpq	$0, 8(%r12)
	je	.L38
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	movq	8(%r12), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	%eax, %r15d
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	cmpl	%r15d, %eax
	je	.L34
	movq	8(%r12), %rax
	leaq	-128(%rbp), %r12
	movl	$-1, %ecx
	leaq	-136(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	56(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	80(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L35:
	testb	$1, 152(%rbx)
	je	.L38
	cmpq	$0, 16(%r12)
	je	.L38
	movq	8(%r12), %rdi
	leaq	80(%rbx), %rsi
	call	_ZN12_GLOBAL__N_1L7matchedEPKN6icu_678numparse4impl19AffixPatternMatcherERKNS0_13UnicodeStringE
	testb	%al, %al
	jne	.L47
.L38:
	xorl	%r14d, %r14d
.L34:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L48
	addq	$104, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	movq	16(%r12), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	%eax, %r15d
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	cmpl	%r15d, %eax
	je	.L34
	movq	16(%r12), %rax
	leaq	-128(%rbp), %r12
	movl	$-1, %ecx
	leaq	-136(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	56(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	144(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L34
.L48:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3469:
	.size	_ZNK6icu_678numparse4impl12AffixMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, .-_ZNK6icu_678numparse4impl12AffixMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.section	.rodata.str2.2
	.align 2
.LC1:
	.string	":"
	.string	"n"
	.string	"e"
	.string	"g"
	.string	"a"
	.string	"t"
	.string	"i"
	.string	"v"
	.string	"e"
	.string	" "
	.string	""
	.string	""
	.align 2
.LC2:
	.string	" "
	.string	""
	.string	""
	.align 2
.LC3:
	.string	">"
	.string	""
	.string	""
	.align 2
.LC4:
	.string	"n"
	.string	"u"
	.string	"l"
	.string	"l"
	.string	""
	.string	""
	.align 2
.LC5:
	.string	"#"
	.string	""
	.string	""
	.align 2
.LC6:
	.string	"<"
	.string	"A"
	.string	"f"
	.string	"f"
	.string	"i"
	.string	"x"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl12AffixMatcher8toStringEv
	.type	_ZNK6icu_678numparse4impl12AffixMatcher8toStringEv, @function
_ZNK6icu_678numparse4impl12AffixMatcher8toStringEv:
.LFB3473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$728, %rsp
	movq	%rdi, -728(%rbp)
	movl	24(%rsi), %r12d
	movq	%r13, %rdi
	leaq	.LC3(%rip), %rsi
	andl	$1, %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L50
	movq	56(%rax), %rax
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	-256(%rbp), %r14
	leaq	-712(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -712(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
.L51:
	leaq	-384(%rbp), %r15
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L52
	movq	56(%rax), %rax
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	-512(%rbp), %rbx
	leaq	-712(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, -712(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
.L53:
	testl	%r12d, %r12d
	leaq	.LC2(%rip), %rax
	leaq	.LC1(%rip), %rsi
	cmove	%rax, %rsi
	leaq	-640(%rbp), %r8
	leaq	-320(%rbp), %r12
	movq	%r8, %rdi
	movq	%r8, -744(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	leaq	-704(%rbp), %r9
	leaq	.LC6(%rip), %rsi
	movq	%r9, %rdi
	movq	%r9, -736(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-744(%rbp), %r8
	movq	-736(%rbp), %r9
	leaq	-576(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -736(%rbp)
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r8, -768(%rbp)
	movq	%r9, -760(%rbp)
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movq	-736(%rbp), %r10
	leaq	-448(%rbp), %r11
	movq	%rbx, %rdx
	movq	%r11, %rdi
	movq	%r11, -736(%rbp)
	movq	%r10, %rsi
	movq	%r10, -752(%rbp)
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movq	-736(%rbp), %r11
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -744(%rbp)
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	leaq	-192(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -736(%rbp)
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movq	-736(%rbp), %rcx
	movq	-728(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rcx, %rsi
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movq	-736(%rbp), %rcx
	movq	%rcx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-744(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-752(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-760(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-768(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	movq	-728(%rbp), %rax
	addq	$728, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	leaq	-256(%rbp), %r14
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	-512(%rbp), %rbx
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	jmp	.L53
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3473:
	.size	_ZNK6icu_678numparse4impl12AffixMatcher8toStringEv, .-_ZNK6icu_678numparse4impl12AffixMatcher8toStringEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl12AffixMatcher9smokeTestERKNS_13StringSegmentE
	.type	_ZNK6icu_678numparse4impl12AffixMatcher9smokeTestERKNS_13StringSegmentE, @function
_ZNK6icu_678numparse4impl12AffixMatcher9smokeTestERKNS_13StringSegmentE:
.LFB3470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L63
	movq	(%rdi), %rax
	call	*32(%rax)
	testb	%al, %al
	je	.L63
.L59:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L59
	movq	(%rdi), %rax
	popq	%rbx
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	32(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE3470:
	.size	_ZNK6icu_678numparse4impl12AffixMatcher9smokeTestERKNS_13StringSegmentE, .-_ZNK6icu_678numparse4impl12AffixMatcher9smokeTestERKNS_13StringSegmentE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl16CodePointMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.type	_ZNK6icu_678numparse4impl16CodePointMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, @function
_ZNK6icu_678numparse4impl16CodePointMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode:
.LFB3436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	8(%rdi), %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment10startsWithEi@PLT
	testb	%al, %al
	jne	.L71
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment23adjustOffsetByCodePointEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_678numparse4impl12ParsedNumber16setCharsConsumedERKNS_13StringSegmentE@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3436:
	.size	_ZNK6icu_678numparse4impl16CodePointMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, .-_ZNK6icu_678numparse4impl16CodePointMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder10addMatcherERNS1_18NumberParseMatcherE
	.type	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder10addMatcherERNS1_18NumberParseMatcherE, @function
_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder10addMatcherERNS1_18NumberParseMatcherE:
.LFB3356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movslq	56(%rdi), %r12
	cmpl	24(%rdi), %r12d
	jl	.L81
	leal	(%r12,%r12), %r15d
	testl	%r15d, %r15d
	jle	.L81
	movslq	%r15d, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L82
	movq	16(%rbx), %r8
	testl	%r12d, %r12d
	jg	.L83
	cmpb	$0, 28(%rbx)
	jne	.L84
.L78:
	movq	%r14, 16(%rbx)
	movslq	56(%rbx), %r12
	movl	%r15d, 24(%rbx)
	movb	$1, 28(%rbx)
	jmp	.L74
.L82:
	movslq	56(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L81:
	movq	16(%rbx), %r14
.L74:
	leal	1(%r12), %eax
	movl	%eax, 56(%rbx)
	movq	%r13, (%r14,%r12,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	cmpl	%r12d, 24(%rbx)
	cmovle	24(%rbx), %r12d
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -56(%rbp)
	cmpl	%r12d, %r15d
	movl	%r12d, %edx
	cmovle	%r15d, %edx
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, 28(%rbx)
	movq	-56(%rbp), %r8
	je	.L78
.L84:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L78
	.cfi_endproc
.LFE3356:
	.size	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder10addMatcherERNS1_18NumberParseMatcherE, .-_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder10addMatcherERNS1_18NumberParseMatcherE
	.set	.LTHUNK0,_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder10addMatcherERNS1_18NumberParseMatcherE
	.section	.text._ZN6icu_678numparse4impl19AffixPatternMatcherD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl19AffixPatternMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl19AffixPatternMatcherD2Ev
	.type	_ZN6icu_678numparse4impl19AffixPatternMatcherD2Ev, @function
_ZN6icu_678numparse4impl19AffixPatternMatcherD2Ev:
.LFB4677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl19AffixPatternMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 68(%rdi)
	movq	%rax, (%rdi)
	jne	.L89
.L86:
	leaq	16+_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE(%rip), %rax
	cmpb	$0, 20(%r12)
	movq	%rax, (%r12)
	jne	.L90
.L87:
	leaq	16+_ZTVN6icu_678numparse4impl18CompositionMatcherE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	56(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L90:
	movq	8(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L87
	.cfi_endproc
.LFE4677:
	.size	_ZN6icu_678numparse4impl19AffixPatternMatcherD2Ev, .-_ZN6icu_678numparse4impl19AffixPatternMatcherD2Ev
	.weak	_ZN6icu_678numparse4impl19AffixPatternMatcherD1Ev
	.set	_ZN6icu_678numparse4impl19AffixPatternMatcherD1Ev,_ZN6icu_678numparse4impl19AffixPatternMatcherD2Ev
	.section	.text._ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD0Ev,"axG",@progbits,_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilderD0Ev
	.type	_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilderD0Ev, @function
_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilderD0Ev:
.LFB4724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN6icu_678numparse4impl26AffixPatternMatcherBuilderE(%rip), %rax
	leaq	-48(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	subq	$8, %rsp
	cmpb	$0, 20(%rdi)
	movups	%xmm0, -8(%rdi)
	jne	.L94
.L92:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl13TokenConsumerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L92
	.cfi_endproc
.LFE4724:
	.size	_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilderD0Ev, .-_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilderD0Ev
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD0Ev
	.type	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD0Ev, @function
_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD0Ev:
.LFB4671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN6icu_678numparse4impl26AffixPatternMatcherBuilderE(%rip), %rax
	leaq	-48(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 28(%rdi)
	movups	%xmm0, (%rdi)
	jne	.L98
.L96:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl13TokenConsumerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L96
	.cfi_endproc
.LFE4671:
	.size	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD0Ev, .-_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD0Ev
	.section	.text._ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilderD1Ev
	.type	_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilderD1Ev, @function
_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilderD1Ev:
.LFB4725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN6icu_678numparse4impl26AffixPatternMatcherBuilderE(%rip), %rax
	leaq	-48(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 20(%rdi)
	movups	%xmm0, -8(%rdi)
	jne	.L102
	addq	$8, %rsp
	leaq	-8(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl13TokenConsumerD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	leaq	-8(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl13TokenConsumerD2Ev@PLT
	.cfi_endproc
.LFE4725:
	.size	_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilderD1Ev, .-_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilderD1Ev
	.section	.text._ZN6icu_678numparse4impl19AffixPatternMatcherD0Ev,"axG",@progbits,_ZN6icu_678numparse4impl19AffixPatternMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl19AffixPatternMatcherD0Ev
	.type	_ZN6icu_678numparse4impl19AffixPatternMatcherD0Ev, @function
_ZN6icu_678numparse4impl19AffixPatternMatcherD0Ev:
.LFB4679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl19AffixPatternMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 68(%rdi)
	movq	%rax, (%rdi)
	jne	.L107
.L104:
	leaq	16+_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE(%rip), %rax
	cmpb	$0, 20(%r12)
	movq	%rax, (%r12)
	jne	.L108
.L105:
	leaq	16+_ZTVN6icu_678numparse4impl18CompositionMatcherE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movq	56(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L108:
	movq	8(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L105
	.cfi_endproc
.LFE4679:
	.size	_ZN6icu_678numparse4impl19AffixPatternMatcherD0Ev, .-_ZN6icu_678numparse4impl19AffixPatternMatcherD0Ev
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_1L6equalsEPKN6icu_678numparse4impl19AffixPatternMatcherES5_, @function
_ZN12_GLOBAL__N_1L6equalsEPKN6icu_678numparse4impl19AffixPatternMatcherES5_:
.LFB3341:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	$1, %r12d
	pushq	%rbx
	subq	$160, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rax
	orq	%rsi, %rax
	je	.L109
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L120
	testq	%rsi, %rsi
	je	.L120
	movq	56(%rsi), %rax
	leaq	-184(%rbp), %r12
	leaq	-112(%rbp), %r13
	movl	$-1, %ecx
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	56(%rbx), %rax
	leaq	-176(%rbp), %r14
	movl	$-1, %ecx
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-168(%rbp), %edx
	testb	$1, %dl
	jne	.L126
	testw	%dx, %dx
	js	.L113
	sarl	$5, %edx
.L114:
	movzwl	-104(%rbp), %r12d
	testw	%r12w, %r12w
	js	.L115
	movswl	%r12w, %eax
	sarl	$5, %eax
.L116:
	cmpl	%edx, %eax
	notl	%r12d
	sete	%al
	andb	%al, %r12b
	jne	.L127
.L112:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L109:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L128
	addq	$160, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L126:
	movzbl	-104(%rbp), %r12d
	andl	$1, %r12d
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L115:
	movl	-100(%rbp), %eax
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L113:
	movl	-164(%rbp), %edx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r12b
	jmp	.L112
.L128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3341:
	.size	_ZN12_GLOBAL__N_1L6equalsEPKN6icu_678numparse4impl19AffixPatternMatcherES5_, .-_ZN12_GLOBAL__N_1L6equalsEPKN6icu_678numparse4impl19AffixPatternMatcherES5_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl12AffixMatcher11postProcessERNS1_12ParsedNumberE
	.type	_ZNK6icu_678numparse4impl12AffixMatcher11postProcessERNS1_12ParsedNumberE, @function
_ZNK6icu_678numparse4impl12AffixMatcher11postProcessERNS1_12ParsedNumberE:
.LFB3471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	80(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L170
	movq	56(%rax), %rax
	leaq	-128(%rbp), %r14
	movl	$-1, %ecx
	leaq	-136(%rbp), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-120(%rbp), %edx
	testb	$1, %dl
	je	.L133
	movzbl	88(%r12), %r15d
	andl	$1, %r15d
.L134:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testb	%r15b, %r15b
	je	.L129
.L132:
	leaq	144(%r12), %r14
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN12_GLOBAL__N_1L7matchedEPKN6icu_678numparse4impl19AffixPatternMatcherERKNS0_13UnicodeStringE
	testb	%al, %al
	je	.L129
	testb	$1, 88(%r12)
	jne	.L171
.L143:
	testb	$1, 152(%r12)
	jne	.L172
.L144:
	movq	8(%rbx), %rdi
	movl	24(%rbx), %eax
	orl	%eax, 76(%r12)
	testq	%rdi, %rdi
	je	.L145
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*40(%rax)
.L145:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L129
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L129:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L173
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	testw	%dx, %dx
	js	.L135
	sarl	$5, %edx
.L136:
	movzwl	88(%r12), %eax
	testw	%ax, %ax
	js	.L137
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L138:
	testb	$1, %al
	jne	.L147
	cmpl	%edx, %ecx
	je	.L139
.L147:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L170:
	testb	$1, 88(%rsi)
	je	.L129
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L135:
	movl	-116(%rbp), %edx
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L137:
	movl	92(%r12), %ecx
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	-128(%rbp), %r15
	movl	$2, %edx
	movq	%r13, %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r15, %rsi
	movw	%dx, -120(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L172:
	leaq	-128(%rbp), %r13
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -128(%rbp)
	movq	%r13, %rsi
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r15b
	jmp	.L134
.L173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3471:
	.size	_ZNK6icu_678numparse4impl12AffixMatcher11postProcessERNS1_12ParsedNumberE, .-_ZNK6icu_678numparse4impl12AffixMatcher11postProcessERNS1_12ParsedNumberE
	.p2align 4
	.globl	_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilder10addMatcherERNS1_18NumberParseMatcherE
	.type	_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilder10addMatcherERNS1_18NumberParseMatcherE, @function
_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilder10addMatcherERNS1_18NumberParseMatcherE:
.LFB4726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movslq	48(%rdi), %r12
	cmpl	16(%rdi), %r12d
	jl	.L183
	leal	(%r12,%r12), %r15d
	testl	%r15d, %r15d
	jle	.L183
	movslq	%r15d, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L184
	movq	8(%rbx), %r8
	testl	%r12d, %r12d
	jg	.L185
	cmpb	$0, 20(%rbx)
	jne	.L186
.L180:
	movq	%r14, 8(%rbx)
	movslq	48(%rbx), %r12
	movl	%r15d, 16(%rbx)
	movb	$1, 20(%rbx)
	jmp	.L176
.L184:
	movslq	48(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L183:
	movq	8(%rbx), %r14
.L176:
	leal	1(%r12), %eax
	movl	%eax, 48(%rbx)
	movq	%r13, (%r14,%r12,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	cmpl	%r12d, 16(%rbx)
	cmovle	16(%rbx), %r12d
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -56(%rbp)
	cmpl	%r12d, %r15d
	movl	%r12d, %edx
	cmovle	%r15d, %edx
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, 20(%rbx)
	movq	-56(%rbp), %r8
	je	.L180
.L186:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L180
	.cfi_endproc
.LFE4726:
	.size	_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilder10addMatcherERNS1_18NumberParseMatcherE, .-_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilder10addMatcherERNS1_18NumberParseMatcherE
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3940:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3940:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3943:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L200
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L188
	cmpb	$0, 12(%rbx)
	jne	.L201
.L192:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L188:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L192
	.cfi_endproc
.LFE3943:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3946:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L204
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3946:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3949:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L207
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3949:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L213
.L209:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L214
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L214:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3951:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3952:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3952:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3953:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3953:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3954:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3954:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3955:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3955:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3956:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3956:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3957:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L230
	testl	%edx, %edx
	jle	.L230
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L233
.L222:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L222
	.cfi_endproc
.LFE3957:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L237
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L237
	testl	%r12d, %r12d
	jg	.L244
	cmpb	$0, 12(%rbx)
	jne	.L245
.L239:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L239
.L245:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L237:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3958:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L247
	movq	(%rdi), %r8
.L248:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L251
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L251
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L251:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3959:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3960:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L258
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3960:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3961:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3961:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3962:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3962:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3963:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3963:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3965:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3965:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3967:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3967:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderC2ERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEPNS1_17IgnorablesMatcherE
	.type	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderC2ERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEPNS1_17IgnorablesMatcherE, @function
_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderC2ERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEPNS1_17IgnorablesMatcherE:
.LFB3353:
	.cfi_startproc
	endbr64
	leaq	64+_ZTVN6icu_678numparse4impl26AffixPatternMatcherBuilderE(%rip), %rax
	movq	%rdx, %xmm2
	movb	$0, 28(%rdi)
	leaq	-48(%rax), %r8
	movq	%rax, %xmm1
	leaq	32(%rdi), %rax
	movl	$3, 24(%rdi)
	movq	%r8, %xmm0
	movq	%rax, 16(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 56(%rdi)
	movups	%xmm0, (%rdi)
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 80(%rdi)
	movups	%xmm0, 64(%rdi)
	ret
	.cfi_endproc
.LFE3353:
	.size	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderC2ERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEPNS1_17IgnorablesMatcherE, .-_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderC2ERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEPNS1_17IgnorablesMatcherE
	.globl	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderC1ERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEPNS1_17IgnorablesMatcherE
	.set	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderC1ERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEPNS1_17IgnorablesMatcherE,_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderC2ERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEPNS1_17IgnorablesMatcherE
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder5buildEv
	.type	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder5buildEv, @function
_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder5buildEv:
.LFB3357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	48(%rsi), %rbx
	leaq	70(%r12), %r13
	movl	40(%rsi), %edx
	call	_ZN6icu_678numparse4impl18ArraySeriesMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEi@PLT
	movzwl	8(%rbx), %ecx
	leaq	16+_ZTVN6icu_678numparse4impl19AffixPatternMatcherE(%rip), %rax
	movq	%rax, (%r12)
	testw	%cx, %cx
	js	.L266
	movswl	%cx, %edx
	sarl	$5, %edx
.L282:
	leal	1(%rdx), %r14d
	movq	%r13, 56(%r12)
	movl	$4, 64(%r12)
	movb	$0, 68(%r12)
	cmpl	$4, %r14d
	jg	.L283
.L274:
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	testb	$17, %cl
	jne	.L279
.L287:
	andl	$2, %ecx
	leaq	10(%rbx), %rsi
	je	.L284
.L275:
	movq	%r13, %rdi
	call	memcpy@PLT
	movswl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L277
	sarl	$5, %eax
.L278:
	movq	56(%r12), %rdx
	cltq
	xorl	%ecx, %ecx
	movw	%cx, (%rdx,%rax,2)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movq	24(%rbx), %rsi
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L266:
	movl	12(%rbx), %edx
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L277:
	movl	12(%rbx), %eax
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L283:
	movslq	%r14d, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L285
	cmpb	$0, 68(%r12)
	jne	.L286
.L272:
	movq	%r13, 56(%r12)
	movl	%r14d, 64(%r12)
	movb	$1, 68(%r12)
.L271:
	movzwl	8(%rbx), %ecx
	testw	%cx, %cx
	js	.L273
	movswl	%cx, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	testb	$17, %cl
	je	.L287
	.p2align 4,,10
	.p2align 3
.L279:
	xorl	%esi, %esi
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L273:
	movl	12(%rbx), %edx
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L286:
	movq	56(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L272
.L285:
	movq	56(%r12), %r13
	jmp	.L271
	.cfi_endproc
.LFE3357:
	.size	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder5buildEv, .-_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder5buildEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouseC2EPKNS1_26AffixTokenMatcherSetupDataE
	.type	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouseC2EPKNS1_26AffixTokenMatcherSetupDataE, @function
_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouseC2EPKNS1_26AffixTokenMatcherSetupDataE:
.LFB3417:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movl	$2, %ecx
	movq	%rsi, (%rdi)
	movq	%rax, %xmm0
	movq	%rax, 16(%rdi)
	movl	$2, %eax
	movl	$2, %esi
	movw	%ax, 24(%rdi)
	leaq	16+_ZTVN6icu_678numparse4impl16MinusSignMatcherE(%rip), %rax
	movl	$2, %r8d
	movl	$2, %r9d
	movq	%rax, 8(%rdi)
	leaq	16+_ZTVN6icu_678numparse4impl15PlusSignMatcherE(%rip), %rax
	movl	$2, %r10d
	movl	$2, %r11d
	movq	%rax, %xmm1
	leaq	16+_ZTVN6icu_678numparse4impl14PercentMatcherE(%rip), %rax
	movw	%dx, 112(%rdi)
	movl	$2, %edx
	punpcklqdq	%xmm0, %xmm1
	movw	%cx, 200(%rdi)
	xorl	%ecx, %ecx
	movups	%xmm1, 96(%rdi)
	movq	%rax, %xmm1
	leaq	16+_ZTVN6icu_678numparse4impl15PermilleMatcherE(%rip), %rax
	punpcklqdq	%xmm0, %xmm1
	movw	%si, 280(%rdi)
	movups	%xmm1, 184(%rdi)
	movq	%rax, %xmm1
	leaq	16+_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE(%rip), %rax
	movq	%rax, 344(%rdi)
	movl	$2, %eax
	punpcklqdq	%xmm0, %xmm1
	movw	%ax, 632(%rdi)
	movl	$2, %eax
	movw	%ax, 696(%rdi)
	movl	$2, %eax
	movw	%ax, 760(%rdi)
	movl	$2, %eax
	movw	%ax, 824(%rdi)
	movl	$2, %eax
	movw	%ax, 888(%rdi)
	leaq	1021(%rdi), %rax
	movq	%rax, 1008(%rdi)
	leaq	1096(%rdi), %rax
	movw	%r8w, 368(%rdi)
	movw	%r9w, 432(%rdi)
	movw	%r10w, 504(%rdi)
	movw	%r11w, 568(%rdi)
	movw	%dx, 952(%rdi)
	movups	%xmm1, 264(%rdi)
	movq	%xmm0, 360(%rdi)
	movq	%xmm0, 424(%rdi)
	movq	%xmm0, 496(%rdi)
	movq	%xmm0, 560(%rdi)
	movq	%xmm0, 624(%rdi)
	movq	%xmm0, 688(%rdi)
	movq	%xmm0, 752(%rdi)
	movq	%xmm0, 816(%rdi)
	movq	%xmm0, 880(%rdi)
	movq	%xmm0, 944(%rdi)
	movl	$0, 1064(%rdi)
	movl	$40, 1016(%rdi)
	movw	%cx, 1020(%rdi)
	movl	$0, 1072(%rdi)
	movq	%rax, 1080(%rdi)
	movl	$8, 1088(%rdi)
	movb	$0, 1092(%rdi)
	ret
	.cfi_endproc
.LFE3417:
	.size	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouseC2EPKNS1_26AffixTokenMatcherSetupDataE, .-_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouseC2EPKNS1_26AffixTokenMatcherSetupDataE
	.globl	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouseC1EPKNS1_26AffixTokenMatcherSetupDataE
	.set	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouseC1EPKNS1_26AffixTokenMatcherSetupDataE,_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouseC2EPKNS1_26AffixTokenMatcherSetupDataE
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse9minusSignEv
	.type	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse9minusSignEv, @function
_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse9minusSignEv:
.LFB3419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-120(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	call	_ZN6icu_678numparse4impl16MinusSignMatcherC1ERKNS_20DecimalFormatSymbolsEb@PLT
	movq	%r13, %rsi
	leaq	16(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-56(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, 80(%rbx)
	movzbl	-48(%rbp), %eax
	movb	%al, 88(%rbx)
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L292
	addq	$96, %rsp
	leaq	8(%rbx), %r14
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L292:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3419:
	.size	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse9minusSignEv, .-_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse9minusSignEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8plusSignEv
	.type	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8plusSignEv, @function
_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8plusSignEv:
.LFB3423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-120(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	call	_ZN6icu_678numparse4impl15PlusSignMatcherC1ERKNS_20DecimalFormatSymbolsEb@PLT
	movq	%r13, %rsi
	leaq	104(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-56(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, 168(%rbx)
	movzbl	-48(%rbp), %eax
	movb	%al, 176(%rbx)
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L296
	addq	$96, %rsp
	leaq	96(%rbx), %r14
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L296:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3423:
	.size	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8plusSignEv, .-_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8plusSignEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse7percentEv
	.type	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse7percentEv, @function
_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse7percentEv:
.LFB3425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-120(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	call	_ZN6icu_678numparse4impl14PercentMatcherC1ERKNS_20DecimalFormatSymbolsE@PLT
	movq	%r13, %rsi
	leaq	192(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-56(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, 256(%rbx)
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L300
	addq	$96, %rsp
	leaq	184(%rbx), %r14
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L300:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3425:
	.size	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse7percentEv, .-_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse7percentEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8permilleEv
	.type	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8permilleEv, @function
_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8permilleEv:
.LFB3427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-120(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	call	_ZN6icu_678numparse4impl15PermilleMatcherC1ERKNS_20DecimalFormatSymbolsE@PLT
	movq	%r13, %rsi
	leaq	272(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-56(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, 336(%rbx)
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L304
	addq	$96, %rsp
	leaq	264(%rbx), %r14
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L304:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3427:
	.size	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8permilleEv, .-_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8permilleEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8currencyER10UErrorCode
	.type	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8currencyER10UErrorCode, @function
_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8currencyER10UErrorCode:
.LFB3429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-768(%rbp), %r14
	leaq	-704(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-784(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-248(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$776, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	movl	32(%rax), %ecx
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherC1ERKNS_6number4impl15CurrencySymbolsERKNS_20DecimalFormatSymbolsEiR10UErrorCode@PLT
	leaq	344(%rbx), %rax
	leaq	360(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, -808(%rbp)
	movq	-776(%rbp), %rax
	movq	%rax, 352(%rbx)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	424(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movzbl	-640(%rbp), %eax
	leaq	496(%rbx), %rcx
	leaq	-632(%rbp), %rdx
	movb	%al, 488(%rbx)
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	movq	%rdx, -800(%rbp)
	movq	%rcx, -792(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-800(%rbp), %rdx
	movq	-792(%rbp), %rcx
	addq	$64, %rdx
	addq	$64, %rcx
	cmpq	%r12, %rdx
	jne	.L306
	leaq	880(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-184(%rbp), %r8
	leaq	944(%rbx), %rdi
	movq	%r8, %rsi
	movq	%r8, -792(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-120(%rbp), %rsi
	leaq	1008(%rbx), %rdi
	call	_ZN6icu_6710CharStringaSEOS0_@PLT
	leaq	16+_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE(%rip), %rax
	cmpb	$0, -108(%rbp)
	movq	-792(%rbp), %r8
	movq	%rax, -784(%rbp)
	jne	.L313
.L307:
	movq	%r8, %rdi
	leaq	-696(%rbp), %rbx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	leaq	-312(%rbp), %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L308:
	movq	(%r12), %rax
	movq	%r12, %rdi
	subq	$64, %r12
	call	*(%rax)
	cmpq	%rbx, %r12
	jne	.L308
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L314
	movq	-808(%rbp), %rax
	addq	$776, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movq	-120(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-792(%rbp), %r8
	jmp	.L307
.L314:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3429:
	.size	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8currencyER10UErrorCode, .-_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8currencyER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse10ignorablesEv
	.type	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse10ignorablesEv, @function
_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse10ignorablesEv:
.LFB3431:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	ret
	.cfi_endproc
.LFE3431:
	.size	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse10ignorablesEv, .-_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse10ignorablesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse20nextCodePointMatcherEiR10UErrorCode
	.type	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse20nextCodePointMatcherEiR10UErrorCode, @function
_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse20nextCodePointMatcherEiR10UErrorCode:
.LFB3432:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L332
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	1088(%rdi), %r14d
	cmpl	1072(%rdi), %r14d
	jne	.L318
	cmpl	$8, %r14d
	je	.L319
	leal	(%r14,%r14), %ecx
	testl	%ecx, %ecx
	jg	.L335
.L324:
	movl	$7, (%r12)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movslq	%ecx, %rdi
	movl	%ecx, -56(%rbp)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L324
	testl	%r14d, %r14d
	movq	1080(%rbx), %r8
	movslq	-56(%rbp), %rcx
	jg	.L325
.L321:
	cmpb	$0, 1092(%rbx)
	jne	.L336
.L322:
	movq	%r15, 1080(%rbx)
	movl	%ecx, 1088(%rbx)
	movb	$1, 1092(%rbx)
.L318:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L323
	movslq	1072(%rbx), %rdx
	leaq	16+_ZTVN6icu_678numparse4impl16CodePointMatcherE(%rip), %rsi
	movl	%r13d, 8(%rax)
	movq	%rsi, (%rax)
	leal	1(%rdx), %ecx
	movl	%ecx, 1072(%rbx)
	movq	1080(%rbx), %rcx
	movq	%rax, (%rcx,%rdx,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movl	$32, %ecx
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L324
.L325:
	cmpl	%r14d, 1088(%rbx)
	movq	1080(%rbx), %r8
	movq	%r15, %rdi
	movl	%ecx, -60(%rbp)
	cmovle	1088(%rbx), %r14d
	movq	%r8, %rsi
	movq	%r8, -56(%rbp)
	cmpl	%ecx, %r14d
	movslq	%r14d, %rdx
	cmovg	%rcx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %r8
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L336:
	movq	%r8, %rdi
	movl	%ecx, -56(%rbp)
	call	uprv_free_67@PLT
	movl	-56(%rbp), %ecx
	jmp	.L322
.L323:
	movslq	1072(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, 1072(%rbx)
	movq	1080(%rbx), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L324
	.cfi_endproc
.LFE3432:
	.size	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse20nextCodePointMatcherEiR10UErrorCode, .-_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse20nextCodePointMatcherEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl16CodePointMatcherC2Ei
	.type	_ZN6icu_678numparse4impl16CodePointMatcherC2Ei, @function
_ZN6icu_678numparse4impl16CodePointMatcherC2Ei:
.LFB3434:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678numparse4impl16CodePointMatcherE(%rip), %rax
	movl	%esi, 8(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE3434:
	.size	_ZN6icu_678numparse4impl16CodePointMatcherC2Ei, .-_ZN6icu_678numparse4impl16CodePointMatcherC2Ei
	.globl	_ZN6icu_678numparse4impl16CodePointMatcherC1Ei
	.set	_ZN6icu_678numparse4impl16CodePointMatcherC1Ei,_ZN6icu_678numparse4impl16CodePointMatcherC2Ei
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl19AffixPatternMatcher16fromAffixPatternERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEiPbR10UErrorCode
	.type	_ZN6icu_678numparse4impl19AffixPatternMatcher16fromAffixPatternERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEiPbR10UErrorCode, @function
_ZN6icu_678numparse4impl19AffixPatternMatcher16fromAffixPatternERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEiPbR10UErrorCode:
.LFB3439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	70(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movswl	8(%rsi), %eax
	shrl	$5, %eax
	je	.L362
	movb	$1, (%r8)
	movq	%rsi, %rdi
	xorl	%eax, %eax
	andb	$2, %ch
	je	.L363
.L341:
	leaq	64+_ZTVN6icu_678numparse4impl26AffixPatternMatcherBuilderE(%rip), %rcx
	movq	%rdx, %xmm3
	movq	%rdi, %xmm0
	movq	%r9, %rdx
	leaq	-48(%rcx), %rsi
	movq	%rcx, %xmm2
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, -64(%rbp)
	movq	%rsi, %xmm1
	leaq	-144(%rbp), %r13
	leaq	-112(%rbp), %rcx
	movaps	%xmm0, -80(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movq	%r13, %rsi
	movq	%rcx, -128(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movl	$3, -120(%rbp)
	movb	$0, -116(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN6icu_676number4impl10AffixUtils19iterateWithConsumerERKNS_13UnicodeStringERNS1_13TokenConsumerER10UErrorCode@PLT
	movl	-88(%rbp), %edx
	movq	-80(%rbp), %rbx
	leaq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678numparse4impl18ArraySeriesMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEi@PLT
	leaq	16+_ZTVN6icu_678numparse4impl19AffixPatternMatcherE(%rip), %rax
	movq	%rax, (%r12)
	movzwl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L342
	movswl	%ax, %edx
	sarl	$5, %edx
.L361:
	leal	1(%rdx), %r15d
	movq	%r14, 56(%r12)
	movl	$4, 64(%r12)
	movb	$0, 68(%r12)
	cmpl	$4, %r15d
	jg	.L364
.L350:
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	testb	$17, %al
	jne	.L358
.L369:
	leaq	10(%rbx), %rsi
	testb	$2, %al
	jne	.L351
	movq	24(%rbx), %rsi
.L351:
	movq	%r14, %rdi
	call	memcpy@PLT
	movswl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L353
	sarl	$5, %eax
.L354:
	movq	56(%r12), %rdx
	xorl	%ecx, %ecx
	movdqa	-160(%rbp), %xmm4
	cltq
	movw	%cx, (%rdx,%rax,2)
	cmpb	$0, -116(%rbp)
	movaps	%xmm4, -144(%rbp)
	jne	.L365
.L355:
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl13TokenConsumerD2Ev@PLT
.L338:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L366
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	movq	(%rdx), %rax
	movq	16(%rax), %rax
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L342:
	movl	12(%rbx), %edx
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L362:
	movb	$0, (%r8)
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	call	_ZN6icu_678numparse4impl18ArraySeriesMatcherC2Ev@PLT
	leaq	16+_ZTVN6icu_678numparse4impl19AffixPatternMatcherE(%rip), %rax
	xorl	%esi, %esi
	movq	%r14, 56(%r12)
	movq	%rax, (%r12)
	movl	$4, 64(%r12)
	movb	$0, 68(%r12)
	movw	%si, 70(%r12)
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L353:
	movl	12(%rbx), %eax
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L364:
	movslq	%r15d, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L367
	cmpb	$0, 68(%r12)
	jne	.L368
.L348:
	movq	%r14, 56(%r12)
	movl	%r15d, 64(%r12)
	movb	$1, 68(%r12)
.L347:
	movzwl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L349
	movswl	%ax, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	testb	$17, %al
	je	.L369
	.p2align 4,,10
	.p2align 3
.L358:
	xorl	%esi, %esi
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L365:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L349:
	movl	12(%rbx), %edx
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L368:
	movq	56(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L348
.L366:
	call	__stack_chk_fail@PLT
.L367:
	movq	56(%r12), %r14
	jmp	.L347
	.cfi_endproc
.LFE3439:
	.size	_ZN6icu_678numparse4impl19AffixPatternMatcher16fromAffixPatternERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEiPbR10UErrorCode, .-_ZN6icu_678numparse4impl19AffixPatternMatcher16fromAffixPatternERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEiPbR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl19AffixPatternMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEiRKNS_13UnicodeStringE
	.type	_ZN6icu_678numparse4impl19AffixPatternMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEiRKNS_13UnicodeStringE, @function
_ZN6icu_678numparse4impl19AffixPatternMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEiRKNS_13UnicodeStringE:
.LFB3444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	70(%rbx), %r13
	call	_ZN6icu_678numparse4impl18ArraySeriesMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEi@PLT
	movzwl	8(%r12), %ecx
	leaq	16+_ZTVN6icu_678numparse4impl19AffixPatternMatcherE(%rip), %rax
	movq	%rax, (%rbx)
	testw	%cx, %cx
	js	.L371
	movswl	%cx, %edx
	sarl	$5, %edx
.L387:
	leal	1(%rdx), %r14d
	movq	%r13, 56(%rbx)
	movl	$4, 64(%rbx)
	movb	$0, 68(%rbx)
	cmpl	$4, %r14d
	jg	.L388
.L379:
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	testb	$17, %cl
	jne	.L384
.L392:
	andl	$2, %ecx
	leaq	10(%r12), %rsi
	je	.L389
.L380:
	movq	%r13, %rdi
	call	memcpy@PLT
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L382
	sarl	$5, %eax
.L383:
	movq	56(%rbx), %rdx
	cltq
	xorl	%ecx, %ecx
	movw	%cx, (%rdx,%rax,2)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	movq	24(%r12), %rsi
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L371:
	movl	12(%r12), %edx
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L382:
	movl	12(%r12), %eax
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L388:
	movslq	%r14d, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L390
	cmpb	$0, 68(%rbx)
	jne	.L391
.L377:
	movq	%r13, 56(%rbx)
	movl	%r14d, 64(%rbx)
	movb	$1, 68(%rbx)
.L376:
	movzwl	8(%r12), %ecx
	testw	%cx, %cx
	js	.L378
	movswl	%cx, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	testb	$17, %cl
	je	.L392
	.p2align 4,,10
	.p2align 3
.L384:
	xorl	%esi, %esi
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L378:
	movl	12(%r12), %edx
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L391:
	movq	56(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L377
.L390:
	movq	56(%rbx), %r13
	jmp	.L376
	.cfi_endproc
.LFE3444:
	.size	_ZN6icu_678numparse4impl19AffixPatternMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEiRKNS_13UnicodeStringE, .-_ZN6icu_678numparse4impl19AffixPatternMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEiRKNS_13UnicodeStringE
	.globl	_ZN6icu_678numparse4impl19AffixPatternMatcherC1ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEiRKNS_13UnicodeStringE
	.set	_ZN6icu_678numparse4impl19AffixPatternMatcherC1ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEiRKNS_13UnicodeStringE,_ZN6icu_678numparse4impl19AffixPatternMatcherC2ERNS_15MaybeStackArrayIPKNS1_18NumberParseMatcherELi3EEEiRKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl19AffixPatternMatcher10getPatternEv
	.type	_ZNK6icu_678numparse4impl19AffixPatternMatcher10getPatternEv, @function
_ZNK6icu_678numparse4impl19AffixPatternMatcher10getPatternEv:
.LFB3446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	56(%rsi), %rax
	movl	$1, %esi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L396
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L396:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3446:
	.size	_ZNK6icu_678numparse4impl19AffixPatternMatcher10getPatternEv, .-_ZNK6icu_678numparse4impl19AffixPatternMatcher10getPatternEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl19AffixPatternMatchereqERKS2_
	.type	_ZNK6icu_678numparse4impl19AffixPatternMatchereqERKS2_, @function
_ZNK6icu_678numparse4impl19AffixPatternMatchereqERKS2_:
.LFB3447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	movq	%r15, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	56(%rsi), %rax
	movl	$1, %esi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	56(%r12), %rax
	leaq	-176(%rbp), %r14
	movl	$-1, %ecx
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-168(%rbp), %edx
	testb	$1, %dl
	je	.L398
	movzbl	-104(%rbp), %r12d
	andl	$1, %r12d
.L399:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L410
	addq	$160, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	testw	%dx, %dx
	js	.L400
	movzwl	-104(%rbp), %r12d
	sarl	$5, %edx
	testw	%r12w, %r12w
	js	.L402
.L411:
	movswl	%r12w, %eax
	sarl	$5, %eax
.L403:
	cmpl	%edx, %eax
	notl	%r12d
	sete	%al
	andb	%al, %r12b
	je	.L399
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r12b
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L400:
	movzwl	-104(%rbp), %r12d
	movl	-164(%rbp), %edx
	testw	%r12w, %r12w
	jns	.L411
.L402:
	movl	-100(%rbp), %eax
	jmp	.L403
.L410:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3447:
	.size	_ZNK6icu_678numparse4impl19AffixPatternMatchereqERKS2_, .-_ZNK6icu_678numparse4impl19AffixPatternMatchereqERKS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl21AffixMatcherWarehouseC2EPNS1_26AffixTokenMatcherWarehouseE
	.type	_ZN6icu_678numparse4impl21AffixMatcherWarehouseC2EPNS1_26AffixTokenMatcherWarehouseE, @function
_ZN6icu_678numparse4impl21AffixMatcherWarehouseC2EPNS1_26AffixTokenMatcherWarehouseE:
.LFB3452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl12AffixMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	768(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	16+_ZTVN6icu_678numparse4impl19AffixPatternMatcherE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	288(%rdi), %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	%rax, 32(%rdi)
	movq	%rax, 64(%rdi)
	movq	%rax, 96(%rdi)
	movq	%rax, 128(%rdi)
	movq	%rax, 160(%rdi)
	movq	%rax, 192(%rdi)
	movq	%rax, 224(%rdi)
	movq	%rax, 256(%rdi)
	.p2align 4,,10
	.p2align 3
.L413:
	movq	%rbx, %rdi
	call	_ZN6icu_678numparse4impl18ArraySeriesMatcherC2Ev@PLT
	leaq	70(%rbx), %rax
	movq	%r13, (%rbx)
	addq	$80, %rbx
	movq	%rax, -24(%rbx)
	xorl	%eax, %eax
	movl	$4, -16(%rbx)
	movb	$0, -12(%rbx)
	movw	%ax, -10(%rbx)
	cmpq	%r14, %rbx
	jne	.L413
	movq	%r15, 768(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3452:
	.size	_ZN6icu_678numparse4impl21AffixMatcherWarehouseC2EPNS1_26AffixTokenMatcherWarehouseE, .-_ZN6icu_678numparse4impl21AffixMatcherWarehouseC2EPNS1_26AffixTokenMatcherWarehouseE
	.globl	_ZN6icu_678numparse4impl21AffixMatcherWarehouseC1EPNS1_26AffixTokenMatcherWarehouseE
	.set	_ZN6icu_678numparse4impl21AffixMatcherWarehouseC1EPNS1_26AffixTokenMatcherWarehouseE,_ZN6icu_678numparse4impl21AffixMatcherWarehouseC2EPNS1_26AffixTokenMatcherWarehouseE
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl21AffixMatcherWarehouse13isInterestingERKNS_6number4impl20AffixPatternProviderERKNS1_17IgnorablesMatcherEiR10UErrorCode
	.type	_ZN6icu_678numparse4impl21AffixMatcherWarehouse13isInterestingERKNS_6number4impl20AffixPatternProviderERKNS1_17IgnorablesMatcherEiR10UErrorCode, @function
_ZN6icu_678numparse4impl21AffixMatcherWarehouse13isInterestingERKNS_6number4impl20AffixPatternProviderERKNS1_17IgnorablesMatcherEiR10UErrorCode:
.LFB3454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-384(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$376, %rsp
	movq	%rcx, -400(%rbp)
	movq	%rsi, -408(%rbp)
	movq	%rdi, %rsi
	movl	%edx, -392(%rbp)
	movl	$256, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	call	*32(%rax)
	movq	(%rbx), %rax
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	*32(%rax)
	movl	$2, %edx
	movl	$2, %ecx
	movq	%rbx, %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -248(%rbp)
	movq	%rax, -256(%rbp)
	movq	%rax, -192(%rbp)
	movq	(%rbx), %rax
	movw	%cx, -184(%rbp)
	call	*56(%rax)
	testb	%al, %al
	jne	.L436
	leaq	-256(%rbp), %r15
	leaq	-192(%rbp), %r8
	testl	$256, -392(%rbp)
	je	.L418
.L420:
	movl	$1, %r12d
.L419:
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L438
	addq	$376, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	movq	(%rbx), %rax
	leaq	-128(%rbp), %r12
	movl	$768, %edx
	movq	%rbx, %rsi
	leaq	-256(%rbp), %r15
	movq	%r12, %rdi
	call	*32(%rax)
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%rbx), %rax
	movl	$512, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*32(%rax)
	leaq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -416(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-416(%rbp), %r8
	testl	$256, -392(%rbp)
	jne	.L420
.L418:
	movq	-408(%rbp), %rdi
	movq	%r8, -392(%rbp)
	call	_ZNK6icu_678numparse4impl13SymbolMatcher6getSetEv@PLT
	movq	-400(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676number4impl10AffixUtils32containsOnlySymbolsAndIgnorablesERKNS_13UnicodeStringERKNS_10UnicodeSetER10UErrorCode@PLT
	movq	-392(%rbp), %r8
	testb	%al, %al
	je	.L420
	movq	-408(%rbp), %rdi
	call	_ZNK6icu_678numparse4impl13SymbolMatcher6getSetEv@PLT
	movq	-400(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676number4impl10AffixUtils32containsOnlySymbolsAndIgnorablesERKNS_13UnicodeStringERKNS_10UnicodeSetER10UErrorCode@PLT
	movq	-392(%rbp), %r8
	testb	%al, %al
	je	.L420
	movq	-408(%rbp), %rdi
	call	_ZNK6icu_678numparse4impl13SymbolMatcher6getSetEv@PLT
	movq	-400(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676number4impl10AffixUtils32containsOnlySymbolsAndIgnorablesERKNS_13UnicodeStringERKNS_10UnicodeSetER10UErrorCode@PLT
	movq	-392(%rbp), %r8
	testb	%al, %al
	je	.L420
	movq	-408(%rbp), %rdi
	call	_ZNK6icu_678numparse4impl13SymbolMatcher6getSetEv@PLT
	movq	-392(%rbp), %r8
	movq	-400(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_676number4impl10AffixUtils32containsOnlySymbolsAndIgnorablesERKNS_13UnicodeStringERKNS_10UnicodeSetER10UErrorCode@PLT
	movq	-392(%rbp), %r8
	testb	%al, %al
	je	.L420
	movq	-400(%rbp), %rdx
	movl	$-2, %esi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	movq	-392(%rbp), %r8
	testb	%al, %al
	jne	.L420
	movq	-400(%rbp), %rdx
	movl	$-1, %esi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	movq	-392(%rbp), %r8
	testb	%al, %al
	jne	.L420
	movq	-400(%rbp), %rdx
	movq	%r8, %rdi
	movl	$-2, %esi
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	movq	-392(%rbp), %r8
	testb	%al, %al
	jne	.L420
	movq	-400(%rbp), %rdx
	movq	%r8, %rdi
	orl	$-1, %esi
	call	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	movq	-392(%rbp), %r8
	movl	%eax, %r12d
	jmp	.L419
.L438:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3454:
	.size	_ZN6icu_678numparse4impl21AffixMatcherWarehouse13isInterestingERKNS_6number4impl20AffixPatternProviderERKNS1_17IgnorablesMatcherEiR10UErrorCode, .-_ZN6icu_678numparse4impl21AffixMatcherWarehouse13isInterestingERKNS_6number4impl20AffixPatternProviderERKNS1_17IgnorablesMatcherEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl21AffixMatcherWarehouse19createAffixMatchersERKNS_6number4impl20AffixPatternProviderERNS1_24MutableMatcherCollectionERKNS1_17IgnorablesMatcherEiR10UErrorCode
	.type	_ZN6icu_678numparse4impl21AffixMatcherWarehouse19createAffixMatchersERKNS_6number4impl20AffixPatternProviderERNS1_24MutableMatcherCollectionERKNS1_17IgnorablesMatcherEiR10UErrorCode, @function
_ZN6icu_678numparse4impl21AffixMatcherWarehouse19createAffixMatchersERKNS_6number4impl20AffixPatternProviderERNS1_24MutableMatcherCollectionERKNS1_17IgnorablesMatcherEiR10UErrorCode:
.LFB3455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$312, %rsp
	movq	%rdi, -296(%rbp)
	movq	%rsi, %rdi
	movq	%rsi, -272(%rbp)
	movq	%rcx, %rsi
	movq	%r9, %rcx
	movq	%rdx, -352(%rbp)
	movl	%r8d, %edx
	movl	%r8d, -280(%rbp)
	movq	%r9, -312(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_678numparse4impl21AffixMatcherWarehouse13isInterestingERKNS_6number4impl20AffixPatternProviderERKNS1_17IgnorablesMatcherEiR10UErrorCode
	movb	%al, -273(%rbp)
	testb	%al, %al
	je	.L439
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %esi
	movq	$0, -344(%rbp)
	xorl	%r15d, %r15d
	movq	%rax, -208(%rbp)
	movl	%ebx, %eax
	andl	$1024, %ebx
	shrb	$7, %al
	movw	%si, -200(%rbp)
	movb	%al, -333(%rbp)
	leaq	-208(%rbp), %rax
	movl	%ebx, -332(%rbp)
	movq	$0, -320(%rbp)
	movl	$0, -288(%rbp)
	movl	$0, -284(%rbp)
	movq	%rax, -304(%rbp)
.L473:
	movb	%r15b, -274(%rbp)
	testl	%r15d, %r15d
	je	.L550
	cmpb	$1, %r15b
	jne	.L444
	movl	-332(%rbp), %edx
	testl	%edx, %edx
	je	.L472
.L444:
	xorl	%r8d, %r8d
	movl	$5, %ecx
	movl	%r15d, %edx
	movl	$1, %esi
	movq	-304(%rbp), %r14
	movq	-272(%rbp), %rdi
	movb	$0, -242(%rbp)
	leaq	-144(%rbp), %r12
	movq	%r14, %r9
	call	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	movq	-296(%rbp), %rbx
	movq	%r14, %rsi
	movq	-312(%rbp), %r9
	movl	-280(%rbp), %ecx
	leaq	-242(%rbp), %r8
	movq	%r12, %rdi
	movq	768(%rbx), %rdx
	call	_ZN6icu_678numparse4impl19AffixPatternMatcher16fromAffixPatternERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEiPbR10UErrorCode
	movslq	-288(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$4, %rax
	leaq	288(%rbx,%rax), %rbx
	movq	%rax, -328(%rbp)
	cmpb	$0, 20(%rbx)
	jne	.L551
.L445:
	movzbl	-124(%rbp), %eax
	movslq	-128(%rbp), %rdx
	leaq	-120(%rbp), %r13
	movb	%al, 20(%rbx)
	movq	-136(%rbp), %rax
	movl	%edx, 16(%rbx)
	cmpq	%r13, %rax
	je	.L552
	movq	%rax, 8(%rbx)
	movq	%r13, -136(%rbp)
	movl	$3, -128(%rbp)
	movb	$0, -124(%rbp)
.L447:
	movl	-96(%rbp), %eax
	cmpb	$0, 68(%rbx)
	movl	%eax, 48(%rbx)
	jne	.L553
.L448:
	movslq	-80(%rbp), %rdx
	movzbl	-76(%rbp), %ecx
	leaq	-74(%rbp), %r14
	movq	-88(%rbp), %rax
	movl	%edx, 64(%rbx)
	movb	%cl, 68(%rbx)
	cmpq	%r14, %rax
	je	.L554
	movq	%rax, 56(%rbx)
.L451:
	leaq	16+_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE(%rip), %rax
	cmpb	$0, -124(%rbp)
	movq	%rax, -144(%rbp)
	jne	.L555
.L452:
	leaq	16+_ZTVN6icu_678numparse4impl18CompositionMatcherE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	cmpb	$0, -242(%rbp)
	movq	$0, -264(%rbp)
	je	.L453
	addl	$1, -288(%rbp)
	movq	-296(%rbp), %rax
	movq	-328(%rbp), %rsi
	movq	%rbx, -264(%rbp)
	leaq	368(%rax,%rsi), %rbx
.L453:
	movq	-304(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	%r15d, %edx
	movq	-272(%rbp), %rdi
	movl	$5, %ecx
	movb	$0, -241(%rbp)
	call	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE@PLT
	movq	-296(%rbp), %rax
	movq	%r12, %rdi
	movq	-312(%rbp), %r9
	movl	-280(%rbp), %ecx
	movq	-304(%rbp), %rsi
	leaq	-241(%rbp), %r8
	movq	768(%rax), %rdx
	call	_ZN6icu_678numparse4impl19AffixPatternMatcher16fromAffixPatternERKNS_13UnicodeStringERNS1_26AffixTokenMatcherWarehouseEiPbR10UErrorCode
	cmpb	$0, 20(%rbx)
	jne	.L556
.L454:
	movzbl	-124(%rbp), %eax
	movslq	-128(%rbp), %rdx
	movb	%al, 20(%rbx)
	movq	-136(%rbp), %rax
	movl	%edx, 16(%rbx)
	cmpq	%r13, %rax
	je	.L557
	movq	%rax, 8(%rbx)
	movq	%r13, -136(%rbp)
	movl	$3, -128(%rbp)
	movb	$0, -124(%rbp)
.L456:
	movl	-96(%rbp), %eax
	cmpb	$0, 68(%rbx)
	movl	%eax, 48(%rbx)
	jne	.L558
.L457:
	movslq	-80(%rbp), %rdx
	movzbl	-76(%rbp), %r13d
	movq	-88(%rbp), %rax
	movl	%edx, 64(%rbx)
	movb	%r13b, 68(%rbx)
	cmpq	%r14, %rax
	je	.L559
	movq	%rax, 56(%rbx)
.L460:
	leaq	16+_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE(%rip), %rax
	cmpb	$0, -124(%rbp)
	movq	%rax, -144(%rbp)
	jne	.L560
.L461:
	leaq	16+_ZTVN6icu_678numparse4impl18CompositionMatcherE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	cmpb	$0, -241(%rbp)
	je	.L462
	addl	$1, -288(%rbp)
	testl	%r15d, %r15d
	jne	.L517
	leaq	16+_ZTVN6icu_678numparse4impl12AffixMatcherE(%rip), %rax
	movq	-264(%rbp), %r13
	movq	-296(%rbp), %rsi
	movq	%rbx, %xmm3
	movq	%rax, -240(%rbp)
	movslq	-284(%rbp), %rax
	leaq	-240(%rbp), %r14
	movq	%r13, %xmm0
	movq	%r14, %rdi
	movq	%r13, -232(%rbp)
	leal	1(%rax), %r12d
	salq	$5, %rax
	punpcklqdq	%xmm3, %xmm0
	movq	%rbx, -224(%rbp)
	movl	$0, 24(%rsi,%rax)
	movups	%xmm0, 8(%rsi,%rax)
	movl	$0, -216(%rbp)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	testq	%r13, %r13
	je	.L529
	cmpb	$0, -333(%rbp)
	je	.L529
	leaq	16+_ZTVN6icu_678numparse4impl12AffixMatcherE(%rip), %rax
	movq	-264(%rbp), %rsi
	movq	%r14, %rdi
	movl	-284(%rbp), %r13d
	movq	%rax, -240(%rbp)
	movslq	%r12d, %rax
	salq	$5, %rax
	movq	%rsi, %r12
	addq	-296(%rbp), %rax
	movq	%rsi, -232(%rbp)
	movq	%rsi, 8(%rax)
	addl	$2, %r13d
	movq	$0, 16(%rax)
	movl	$0, 24(%rax)
	movq	$0, -224(%rbp)
	movl	$0, -216(%rbp)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	movq	%rbx, -344(%rbp)
	movq	%r12, -320(%rbp)
	xorl	%r12d, %r12d
.L515:
	leaq	16+_ZTVN6icu_678numparse4impl12AffixMatcherE(%rip), %rax
	movq	%r14, %rdi
	movq	$0, -232(%rbp)
	movq	%rax, -240(%rbp)
	leal	1(%r13), %eax
	movslq	%r13d, %r13
	movl	%eax, -284(%rbp)
	movq	-296(%rbp), %rax
	salq	$5, %r13
	movq	%rbx, -224(%rbp)
	addq	%r13, %rax
	movl	%r12d, -216(%rbp)
	movq	$0, 8(%rax)
	movq	%rbx, 16(%rax)
	movl	%r12d, 24(%rax)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
.L472:
	addl	$1, %r15d
	cmpl	$3, %r15d
	jne	.L473
.L566:
	movl	-284(%rbp), %eax
	movq	-296(%rbp), %rsi
	subl	$2, %eax
	movq	%rsi, -312(%rbp)
	salq	$5, %rax
	leaq	40(%rsi,%rax), %rax
	movq	%rax, -264(%rbp)
	.p2align 4,,10
	.p2align 3
.L506:
	cmpl	$1, -284(%rbp)
	jle	.L474
	movq	-296(%rbp), %rax
	movb	$0, -272(%rbp)
	leaq	8(%rax), %r15
	movq	%r15, %r14
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L562:
	movq	56(%rdx), %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	-240(%rbp), %r12
	leaq	-144(%rbp), %r15
	movq	%rdx, -240(%rbp)
	movq	%r15, %rdi
	movq	%r12, %rdx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-136(%rbp), %ebx
	testw	%bx, %bx
	js	.L476
	sarl	$5, %ebx
.L477:
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	32(%r14), %rcx
	testq	%rcx, %rcx
	je	.L478
.L513:
	movq	56(%rcx), %rdx
	movl	$1, %esi
	movl	$-1, %ecx
	movq	%r15, %rdi
	movq	%rdx, -240(%rbp)
	movq	%r12, %rdx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-136(%rbp), %edx
	testw	%dx, %dx
	js	.L479
	sarl	$5, %edx
	movl	%edx, %r13d
.L480:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L478:
	cmpl	%ebx, %r13d
	je	.L481
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.L482
	movq	56(%rdx), %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rdx, -240(%rbp)
	movq	%r12, %rdx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-136(%rbp), %ebx
	testw	%bx, %bx
	js	.L483
	sarl	$5, %ebx
.L484:
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	32(%r14), %rcx
	testq	%rcx, %rcx
	je	.L485
.L510:
	movq	56(%rcx), %rdx
	movl	$1, %esi
	movl	$-1, %ecx
	movq	%r15, %rdi
	movq	%rdx, -240(%rbp)
	movq	%r12, %rdx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-136(%rbp), %edx
	testw	%dx, %dx
	js	.L486
	sarl	$5, %edx
	movl	%edx, %r13d
.L487:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L485:
	cmpl	%ebx, %r13d
	jge	.L549
.L488:
	addq	$32, %r14
	cmpq	%r14, -264(%rbp)
	je	.L561
.L503:
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L562
	movq	32(%r14), %rcx
	testq	%rcx, %rcx
	je	.L481
	xorl	%ebx, %ebx
	leaq	-144(%rbp), %r15
	leaq	-240(%rbp), %r12
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L462:
	xorl	%ebx, %ebx
	testl	%r15d, %r15d
	je	.L563
.L517:
	movq	-320(%rbp), %rsi
	movq	-264(%rbp), %rdi
	call	_ZN12_GLOBAL__N_1L6equalsEPKN6icu_678numparse4impl19AffixPatternMatcherES5_
	testb	%al, %al
	jne	.L464
.L468:
	leaq	16+_ZTVN6icu_678numparse4impl12AffixMatcherE(%rip), %rax
	movq	-296(%rbp), %rdi
	xorl	%r12d, %r12d
	movq	%rbx, %xmm2
	movq	%rax, -240(%rbp)
	movslq	-284(%rbp), %rax
	leaq	-240(%rbp), %r14
	cmpb	$2, -274(%rbp)
	movq	-264(%rbp), %rsi
	movq	%rbx, -224(%rbp)
	sete	%r12b
	leal	1(%rax), %r13d
	salq	$5, %rax
	movl	%r12d, 24(%rdi,%rax)
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%rsi, -232(%rbp)
	movups	%xmm0, 8(%rdi,%rax)
	movq	%r14, %rdi
	movl	%r12d, -216(%rbp)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	cmpq	$0, -264(%rbp)
	setne	%al
	testb	%al, -333(%rbp)
	je	.L528
	testq	%rbx, %rbx
	je	.L528
	movq	-320(%rbp), %rsi
	movq	-264(%rbp), %rdi
	call	_ZN12_GLOBAL__N_1L6equalsEPKN6icu_678numparse4impl19AffixPatternMatcherES5_
	testb	%al, %al
	je	.L471
	movl	%r13d, -284(%rbp)
.L514:
	movq	-344(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN12_GLOBAL__N_1L6equalsEPKN6icu_678numparse4impl19AffixPatternMatcherES5_
	testb	%al, %al
	jne	.L472
	movl	-284(%rbp), %r13d
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L481:
	movq	8(%r14), %rdx
	testq	%rdx, %rdx
	je	.L490
	movq	56(%rdx), %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	-240(%rbp), %r12
	leaq	-144(%rbp), %r15
	movq	%rdx, -240(%rbp)
	movq	%r15, %rdi
	movq	%r12, %rdx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-136(%rbp), %ebx
	testw	%bx, %bx
	js	.L491
	sarl	$5, %ebx
.L492:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	40(%r14), %rcx
	xorl	%edx, %edx
	testq	%rcx, %rcx
	je	.L493
.L512:
	movq	56(%rcx), %rdx
	movl	$1, %esi
	movl	$-1, %ecx
	movq	%r15, %rdi
	movq	%rdx, -240(%rbp)
	movq	%r12, %rdx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-136(%rbp), %edx
	testw	%dx, %dx
	js	.L494
	sarl	$5, %edx
.L495:
	movq	%r15, %rdi
	movl	%edx, -280(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-280(%rbp), %edx
.L493:
	cmpl	%ebx, %edx
	je	.L488
	movq	8(%r14), %rdx
	testq	%rdx, %rdx
	je	.L496
	movq	56(%rdx), %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rdx, -240(%rbp)
	movq	%r12, %rdx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-136(%rbp), %ebx
	testw	%bx, %bx
	js	.L497
	sarl	$5, %ebx
.L498:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	40(%r14), %rsi
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L499
.L511:
	movq	56(%rsi), %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rdx, -240(%rbp)
	movq	%r12, %rdx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-136(%rbp), %edx
	testw	%dx, %dx
	js	.L500
	sarl	$5, %edx
.L501:
	movq	%r15, %rdi
	movl	%edx, -280(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-280(%rbp), %edx
.L499:
	cmpl	%ebx, %edx
	jl	.L488
.L549:
	movq	32(%r14), %rcx
	movq	(%r14), %rdx
	movq	8(%r14), %r8
	movq	40(%r14), %rsi
.L489:
	movl	16(%r14), %edi
	movq	%rcx, (%r14)
	movq	%rdx, %xmm0
	addq	$32, %r14
	movl	16(%r14), %ecx
	movq	%r8, %xmm1
	movq	%rsi, -24(%r14)
	leaq	16+_ZTVN6icu_678numparse4impl12AffixMatcherE(%rip), %rax
	movl	%edi, 16(%r14)
	punpcklqdq	%xmm1, %xmm0
	movl	%ecx, -16(%r14)
	movl	%edi, -216(%rbp)
	movq	%r12, %rdi
	movups	%xmm0, (%r14)
	movq	%rax, -240(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%r8, -224(%rbp)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	movzbl	-273(%rbp), %eax
	movb	%al, -272(%rbp)
	cmpq	%r14, -264(%rbp)
	jne	.L503
.L561:
	cmpb	$0, -272(%rbp)
	jne	.L506
.L507:
	movq	-352(%rbp), %r15
	movq	-312(%rbp), %r14
	xorl	%r13d, %r13d
	leaq	_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilder10addMatcherERNS1_18NumberParseMatcherE(%rip), %rbx
	leaq	-8(%r15), %r12
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L564:
	movq	%r12, %rdi
	call	.LTHUNK0
.L509:
	addl	$1, %r13d
	addq	$32, %r14
	cmpl	%r13d, -284(%rbp)
	jle	.L504
.L505:
	movq	(%r15), %rax
	movq	%r14, %rsi
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	je	.L564
	movq	%r15, %rdi
	call	*%rax
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L482:
	movq	32(%r14), %rcx
	xorl	%ebx, %ebx
	testq	%rcx, %rcx
	jne	.L510
	movq	8(%r14), %r8
	movq	40(%r14), %rsi
	xorl	%edx, %edx
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L479:
	movl	-132(%rbp), %r13d
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L476:
	movl	-132(%rbp), %ebx
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L490:
	movq	40(%r14), %rcx
	testq	%rcx, %rcx
	je	.L488
	xorl	%ebx, %ebx
	leaq	-144(%rbp), %r15
	leaq	-240(%rbp), %r12
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L483:
	movl	-132(%rbp), %ebx
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L486:
	movl	-132(%rbp), %r13d
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L496:
	movq	40(%r14), %rsi
	xorl	%ebx, %ebx
	movq	%rsi, %r8
	testq	%rsi, %rsi
	jne	.L511
	movq	32(%r14), %rcx
	movq	(%r14), %rdx
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L491:
	movl	-132(%rbp), %ebx
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L494:
	movl	-132(%rbp), %edx
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L500:
	movl	-132(%rbp), %edx
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L497:
	movl	-132(%rbp), %ebx
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L474:
	movl	-284(%rbp), %eax
	testl	%eax, %eax
	jne	.L507
	.p2align 4,,10
	.p2align 3
.L504:
	movq	-304(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L439:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L565
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L550:
	.cfi_restore_state
	movl	-332(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L444
	jmp	.L472
.L560:
	movq	-136(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L461
.L551:
	movq	8(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L445
.L558:
	movq	56(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L457
.L556:
	movq	8(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L454
.L555:
	movq	-136(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L452
.L553:
	movq	56(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L448
.L563:
	leaq	16+_ZTVN6icu_678numparse4impl12AffixMatcherE(%rip), %rax
	movq	-264(%rbp), %rbx
	leaq	-240(%rbp), %r14
	addl	$1, %r15d
	movq	%rax, -240(%rbp)
	movq	%r14, %rdi
	movslq	-284(%rbp), %rax
	movq	%rbx, -232(%rbp)
	movq	$0, -224(%rbp)
	leal	1(%rax), %r12d
	salq	$5, %rax
	addq	-296(%rbp), %rax
	movq	%rbx, 8(%rax)
	movq	$0, 16(%rax)
	movl	$0, 24(%rax)
	movl	$0, -216(%rbp)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	movq	%rbx, -320(%rbp)
	movl	%r12d, -284(%rbp)
	movq	$0, -344(%rbp)
	cmpl	$3, %r15d
	jne	.L473
	jmp	.L566
.L464:
	movq	-344(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN12_GLOBAL__N_1L6equalsEPKN6icu_678numparse4impl19AffixPatternMatcherES5_
	testb	%al, %al
	je	.L468
	jmp	.L472
.L528:
	addl	$1, %r15d
	movl	%r13d, -284(%rbp)
	cmpl	$3, %r15d
	jne	.L473
	jmp	.L566
.L559:
	leaq	70(%rbx), %rdi
	addq	%rdx, %rdx
	movq	%r14, %rsi
	movq	%rdi, 56(%rbx)
	call	memcpy@PLT
	leaq	16+_ZTVN6icu_678numparse4impl19AffixPatternMatcherE(%rip), %rax
	movq	%rax, -144(%rbp)
	testb	%r13b, %r13b
	je	.L460
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L460
.L557:
	leaq	24(%rbx), %rdi
	salq	$3, %rdx
	movq	%r13, %rsi
	movq	%rdi, 8(%rbx)
	call	memcpy@PLT
	jmp	.L456
.L554:
	leaq	70(%rbx), %rdi
	addq	%rdx, %rdx
	movq	%r14, %rsi
	movb	%cl, -264(%rbp)
	movq	%rdi, 56(%rbx)
	call	memcpy@PLT
	movzbl	-264(%rbp), %ecx
	leaq	16+_ZTVN6icu_678numparse4impl19AffixPatternMatcherE(%rip), %rax
	movq	%rax, -144(%rbp)
	testb	%cl, %cl
	je	.L451
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L451
.L552:
	leaq	24(%rbx), %rdi
	salq	$3, %rdx
	movq	%r13, %rsi
	movq	%rdi, 8(%rbx)
	call	memcpy@PLT
	jmp	.L447
.L471:
	leaq	16+_ZTVN6icu_678numparse4impl12AffixMatcherE(%rip), %rax
	movq	-264(%rbp), %rsi
	movq	%r14, %rdi
	addl	$2, -284(%rbp)
	movq	%rax, -240(%rbp)
	movslq	%r13d, %rax
	salq	$5, %rax
	addq	-296(%rbp), %rax
	movq	%rsi, -232(%rbp)
	movq	%rsi, 8(%rax)
	movq	$0, 16(%rax)
	movl	%r12d, 24(%rax)
	movq	$0, -224(%rbp)
	movl	%r12d, -216(%rbp)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	jmp	.L514
.L529:
	movq	-264(%rbp), %rax
	addl	$1, %r15d
	movq	%rbx, -344(%rbp)
	movl	%r12d, -284(%rbp)
	movq	%rax, -320(%rbp)
	cmpl	$3, %r15d
	jne	.L473
	jmp	.L566
.L565:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3455:
	.size	_ZN6icu_678numparse4impl21AffixMatcherWarehouse19createAffixMatchersERKNS_6number4impl20AffixPatternProviderERNS1_24MutableMatcherCollectionERKNS1_17IgnorablesMatcherEiR10UErrorCode, .-_ZN6icu_678numparse4impl21AffixMatcherWarehouse19createAffixMatchersERKNS_6number4impl20AffixPatternProviderERNS1_24MutableMatcherCollectionERKNS1_17IgnorablesMatcherEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl12AffixMatcherC2EPNS1_19AffixPatternMatcherES4_i
	.type	_ZN6icu_678numparse4impl12AffixMatcherC2EPNS1_19AffixPatternMatcherES4_i, @function
_ZN6icu_678numparse4impl12AffixMatcherC2EPNS1_19AffixPatternMatcherES4_i:
.LFB3467:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678numparse4impl12AffixMatcherE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	movl	%ecx, 24(%rdi)
	ret
	.cfi_endproc
.LFE3467:
	.size	_ZN6icu_678numparse4impl12AffixMatcherC2EPNS1_19AffixPatternMatcherES4_i, .-_ZN6icu_678numparse4impl12AffixMatcherC2EPNS1_19AffixPatternMatcherES4_i
	.globl	_ZN6icu_678numparse4impl12AffixMatcherC1EPNS1_19AffixPatternMatcherES4_i
	.set	_ZN6icu_678numparse4impl12AffixMatcherC1EPNS1_19AffixPatternMatcherES4_i,_ZN6icu_678numparse4impl12AffixMatcherC2EPNS1_19AffixPatternMatcherES4_i
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl12AffixMatcher9compareToERKS2_
	.type	_ZNK6icu_678numparse4impl12AffixMatcher9compareToERKS2_, @function
_ZNK6icu_678numparse4impl12AffixMatcher9compareToERKS2_:
.LFB3472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L569
	movq	56(%rax), %rax
	leaq	-136(%rbp), %r15
	leaq	-128(%rbp), %r12
	movl	$-1, %ecx
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-120(%rbp), %r14d
	testw	%r14w, %r14w
	js	.L570
	sarl	$5, %r14d
.L571:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	8(%r13), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L572
.L602:
	movq	56(%rdx), %rax
	movl	$-1, %ecx
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L573
	sarl	$5, %eax
.L574:
	movq	%r12, %rdi
	movl	%eax, -148(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-148(%rbp), %eax
.L572:
	cmpl	%r14d, %eax
	je	.L575
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L576
	movq	56(%rax), %rax
	movl	$-1, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-120(%rbp), %r14d
	testw	%r14w, %r14w
	js	.L577
	sarl	$5, %r14d
.L578:
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	8(%r13), %rax
	testq	%rax, %rax
	je	.L579
.L599:
	movq	56(%rax), %rax
	movl	$-1, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-120(%rbp), %ebx
	testw	%bx, %bx
	js	.L580
	sarl	$5, %ebx
.L581:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L579:
	movl	$-1, %eax
	cmpl	%r14d, %ebx
	jge	.L582
.L568:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L619
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L570:
	.cfi_restore_state
	movl	-116(%rbp), %r14d
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L569:
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L575
	xorl	%r14d, %r14d
	leaq	-136(%rbp), %r15
	leaq	-128(%rbp), %r12
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L591:
	movq	16(%r13), %rax
	xorl	%r14d, %r14d
	testq	%rax, %rax
	jne	.L599
	.p2align 4,,10
	.p2align 3
.L582:
	movl	$1, %eax
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L575:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L584
	movq	56(%rax), %rax
	leaq	-136(%rbp), %r15
	leaq	-128(%rbp), %r12
	movl	$-1, %ecx
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-120(%rbp), %r14d
	testw	%r14w, %r14w
	js	.L585
	sarl	$5, %r14d
.L586:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	16(%r13), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L587
.L600:
	movq	56(%rdx), %rax
	movl	$-1, %ecx
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L588
	sarl	$5, %eax
.L589:
	movq	%r12, %rdi
	movl	%eax, -148(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-148(%rbp), %eax
.L587:
	cmpl	%r14d, %eax
	je	.L601
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L591
	movq	56(%rax), %rax
	movl	$-1, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-120(%rbp), %r14d
	testw	%r14w, %r14w
	js	.L592
	sarl	$5, %r14d
.L593:
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	16(%r13), %rax
	testq	%rax, %rax
	jne	.L599
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L576:
	movq	8(%r13), %rax
	xorl	%r14d, %r14d
	testq	%rax, %rax
	jne	.L599
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L573:
	movl	-116(%rbp), %eax
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L580:
	movl	-116(%rbp), %ebx
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L584:
	movq	16(%r13), %rdx
	testq	%rdx, %rdx
	je	.L601
	xorl	%r14d, %r14d
	leaq	-136(%rbp), %r15
	leaq	-128(%rbp), %r12
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L577:
	movl	-116(%rbp), %r14d
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L588:
	movl	-116(%rbp), %eax
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L585:
	movl	-116(%rbp), %r14d
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L601:
	xorl	%eax, %eax
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L592:
	movl	-116(%rbp), %r14d
	jmp	.L593
.L619:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3472:
	.size	_ZNK6icu_678numparse4impl12AffixMatcher9compareToERKS2_, .-_ZNK6icu_678numparse4impl12AffixMatcher9compareToERKS2_
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB7:
	.text
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder12consumeTokenENS_6number4impl16AffixPatternTypeEiR10UErrorCode
	.type	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder12consumeTokenENS_6number4impl16AffixPatternTypeEiR10UErrorCode, @function
_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder12consumeTokenENS_6number4impl16AffixPatternTypeEiR10UErrorCode:
.LFB3355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	80(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L621
	movl	56(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L622
	movl	60(%rbx), %edx
	movq	%rdi, %rsi
	testl	%edx, %edx
	jns	.L657
.L623:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
.L624:
	testl	%r12d, %r12d
	jne	.L646
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L637
.L634:
	call	_ZNK6icu_678numparse4impl13SymbolMatcher6getSetEv@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L648
.L637:
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L620
	movq	72(%rbx), %r12
	movl	1088(%r12), %r15d
	cmpl	1072(%r12), %r15d
	jne	.L638
	cmpl	$8, %r15d
	je	.L639
	leal	(%r15,%r15), %r8d
	testl	%r8d, %r8d
	jg	.L658
.L644:
	movl	$7, (%r14)
	.p2align 4,,10
	.p2align 3
.L620:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L659
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	.cfi_restore_state
	testl	%esi, %esi
	je	.L634
.L646:
	leal	9(%r12), %edx
	cmpl	$-9, %r12d
	jb	.L626
	leaq	.L628(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L628:
	.long	.L632-.L628
	.long	.L632-.L628
	.long	.L632-.L628
	.long	.L632-.L628
	.long	.L632-.L628
	.long	.L631-.L628
	.long	.L630-.L628
	.long	.L629-.L628
	.long	.L627-.L628
	.text
	.p2align 4,,10
	.p2align 3
.L621:
	testl	%esi, %esi
	jne	.L646
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L632:
	movq	(%rbx), %rax
	movq	72(%rbx), %rdi
	movq	%r14, %rsi
	movq	24(%rax), %r13
	call	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouse8currencyER10UErrorCode
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	*%r13
	.p2align 4,,10
	.p2align 3
.L633:
	movl	%r12d, 60(%rbx)
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L648:
	movl	%r13d, %r12d
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L639:
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movl	$32, %r8d
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L644
.L645:
	cmpl	%r15d, 1088(%r12)
	movq	1080(%r12), %rsi
	movq	%rcx, %rdi
	movl	%r8d, -152(%rbp)
	cmovle	1088(%r12), %r15d
	cmpl	%r8d, %r15d
	movslq	%r15d, %rdx
	cmovg	%r8, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	movl	-152(%rbp), %r8d
	movq	%rax, %rcx
.L641:
	cmpb	$0, 1092(%r12)
	jne	.L660
.L642:
	movq	%rcx, 1080(%r12)
	movl	%r8d, 1088(%r12)
	movb	$1, 1092(%r12)
.L638:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L643
	leaq	16+_ZTVN6icu_678numparse4impl16CodePointMatcherE(%rip), %rax
	movl	%r13d, 8(%rsi)
	movq	%rbx, %rdi
	movq	%rax, (%rsi)
	movslq	1072(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 1072(%r12)
	movq	1080(%r12), %rdx
	movl	%r13d, %r12d
	movq	%rsi, (%rdx,%rax,8)
	movq	(%rbx), %rax
	call	*24(%rax)
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L629:
	movq	72(%rbx), %r13
	movq	(%rbx), %rax
	leaq	-144(%rbp), %r14
	movl	$1, %edx
	movq	%r14, %rdi
	leaq	-136(%rbp), %r15
	movq	24(%rax), %rcx
	movq	0(%r13), %rax
	movq	8(%rax), %rsi
	movq	%rcx, -160(%rbp)
	call	_ZN6icu_678numparse4impl15PlusSignMatcherC1ERKNS_20DecimalFormatSymbolsEb@PLT
	leaq	96(%r13), %r8
	leaq	104(%r13), %rdi
	movq	%r15, %rsi
	movq	%r8, -152(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-72(%rbp), %rax
	movq	%rax, 168(%r13)
	movzbl	-64(%rbp), %eax
	movb	%al, 176(%r13)
.L656:
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r8, %rsi
	call	*%rcx
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L627:
	movq	72(%rbx), %r13
	movq	(%rbx), %rax
	leaq	-144(%rbp), %r14
	movl	$1, %edx
	movq	%r14, %rdi
	leaq	-136(%rbp), %r15
	movq	24(%rax), %rcx
	movq	0(%r13), %rax
	movq	8(%rax), %rsi
	movq	%rcx, -160(%rbp)
	call	_ZN6icu_678numparse4impl16MinusSignMatcherC1ERKNS_20DecimalFormatSymbolsEb@PLT
	leaq	8(%r13), %r8
	leaq	16(%r13), %rdi
	movq	%r15, %rsi
	movq	%r8, -152(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-72(%rbp), %rax
	movq	%rax, 80(%r13)
	movzbl	-64(%rbp), %eax
	movb	%al, 88(%r13)
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L630:
	movq	72(%rbx), %r13
	movq	(%rbx), %rax
	leaq	-144(%rbp), %r14
	leaq	-136(%rbp), %r15
	movq	%r14, %rdi
	movq	24(%rax), %rdx
	movq	0(%r13), %rax
	movq	8(%rax), %rsi
	movq	%rdx, -160(%rbp)
	call	_ZN6icu_678numparse4impl14PercentMatcherC1ERKNS_20DecimalFormatSymbolsE@PLT
	leaq	184(%r13), %r8
	leaq	192(%r13), %rdi
	movq	%r15, %rsi
	movq	%r8, -152(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-72(%rbp), %rax
	movq	%rax, 256(%r13)
.L655:
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r8, %rsi
	call	*%rdx
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L631:
	movq	72(%rbx), %r13
	movq	(%rbx), %rax
	leaq	-144(%rbp), %r14
	leaq	-136(%rbp), %r15
	movq	%r14, %rdi
	movq	24(%rax), %rdx
	movq	0(%r13), %rax
	movq	8(%rax), %rsi
	movq	%rdx, -160(%rbp)
	call	_ZN6icu_678numparse4impl15PermilleMatcherC1ERKNS_20DecimalFormatSymbolsE@PLT
	leaq	264(%r13), %r8
	leaq	272(%r13), %rdi
	movq	%r15, %rsi
	movq	%r8, -152(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-72(%rbp), %rax
	movq	%rax, 336(%r13)
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L657:
	call	_ZNK6icu_678numparse4impl13SymbolMatcher6getSetEv@PLT
	movl	60(%rbx), %esi
	movq	%rax, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L624
	movq	80(%rbx), %rsi
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L658:
	movslq	%r8d, %rdi
	movl	%r8d, -152(%rbp)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L644
	testl	%r15d, %r15d
	movslq	-152(%rbp), %r8
	jle	.L641
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L660:
	movq	1080(%r12), %rdi
	movq	%rcx, -160(%rbp)
	movl	%r8d, -152(%rbp)
	call	uprv_free_67@PLT
	movq	-160(%rbp), %rcx
	movl	-152(%rbp), %r8d
	jmp	.L642
.L659:
	call	__stack_chk_fail@PLT
.L643:
	movslq	1072(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 1072(%r12)
	movq	1080(%r12), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L644
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder12consumeTokenENS_6number4impl16AffixPatternTypeEiR10UErrorCode.cold, @function
_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder12consumeTokenENS_6number4impl16AffixPatternTypeEiR10UErrorCode.cold:
.LFSB3355:
.L626:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3355:
	.text
	.size	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder12consumeTokenENS_6number4impl16AffixPatternTypeEiR10UErrorCode, .-_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder12consumeTokenENS_6number4impl16AffixPatternTypeEiR10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder12consumeTokenENS_6number4impl16AffixPatternTypeEiR10UErrorCode.cold, .-_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder12consumeTokenENS_6number4impl16AffixPatternTypeEiR10UErrorCode.cold
.LCOLDE7:
	.text
.LHOTE7:
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_678numparse4impl24MutableMatcherCollectionE
	.section	.rodata._ZTSN6icu_678numparse4impl24MutableMatcherCollectionE,"aG",@progbits,_ZTSN6icu_678numparse4impl24MutableMatcherCollectionE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl24MutableMatcherCollectionE, @object
	.size	_ZTSN6icu_678numparse4impl24MutableMatcherCollectionE, 50
_ZTSN6icu_678numparse4impl24MutableMatcherCollectionE:
	.string	"N6icu_678numparse4impl24MutableMatcherCollectionE"
	.weak	_ZTIN6icu_678numparse4impl24MutableMatcherCollectionE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl24MutableMatcherCollectionE,"awG",@progbits,_ZTIN6icu_678numparse4impl24MutableMatcherCollectionE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl24MutableMatcherCollectionE, @object
	.size	_ZTIN6icu_678numparse4impl24MutableMatcherCollectionE, 16
_ZTIN6icu_678numparse4impl24MutableMatcherCollectionE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl24MutableMatcherCollectionE
	.weak	_ZTSN6icu_678numparse4impl18CompositionMatcherE
	.section	.rodata._ZTSN6icu_678numparse4impl18CompositionMatcherE,"aG",@progbits,_ZTSN6icu_678numparse4impl18CompositionMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl18CompositionMatcherE, @object
	.size	_ZTSN6icu_678numparse4impl18CompositionMatcherE, 44
_ZTSN6icu_678numparse4impl18CompositionMatcherE:
	.string	"N6icu_678numparse4impl18CompositionMatcherE"
	.weak	_ZTIN6icu_678numparse4impl18CompositionMatcherE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl18CompositionMatcherE,"awG",@progbits,_ZTIN6icu_678numparse4impl18CompositionMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl18CompositionMatcherE, @object
	.size	_ZTIN6icu_678numparse4impl18CompositionMatcherE, 24
_ZTIN6icu_678numparse4impl18CompositionMatcherE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl18CompositionMatcherE
	.quad	_ZTIN6icu_678numparse4impl18NumberParseMatcherE
	.weak	_ZTSN6icu_678numparse4impl16CodePointMatcherE
	.section	.rodata._ZTSN6icu_678numparse4impl16CodePointMatcherE,"aG",@progbits,_ZTSN6icu_678numparse4impl16CodePointMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl16CodePointMatcherE, @object
	.size	_ZTSN6icu_678numparse4impl16CodePointMatcherE, 42
_ZTSN6icu_678numparse4impl16CodePointMatcherE:
	.string	"N6icu_678numparse4impl16CodePointMatcherE"
	.weak	_ZTIN6icu_678numparse4impl16CodePointMatcherE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl16CodePointMatcherE,"awG",@progbits,_ZTIN6icu_678numparse4impl16CodePointMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl16CodePointMatcherE, @object
	.size	_ZTIN6icu_678numparse4impl16CodePointMatcherE, 56
_ZTIN6icu_678numparse4impl16CodePointMatcherE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl16CodePointMatcherE
	.long	0
	.long	2
	.quad	_ZTIN6icu_678numparse4impl18NumberParseMatcherE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTSN6icu_678numparse4impl26AffixPatternMatcherBuilderE
	.section	.rodata._ZTSN6icu_678numparse4impl26AffixPatternMatcherBuilderE,"aG",@progbits,_ZTSN6icu_678numparse4impl26AffixPatternMatcherBuilderE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl26AffixPatternMatcherBuilderE, @object
	.size	_ZTSN6icu_678numparse4impl26AffixPatternMatcherBuilderE, 52
_ZTSN6icu_678numparse4impl26AffixPatternMatcherBuilderE:
	.string	"N6icu_678numparse4impl26AffixPatternMatcherBuilderE"
	.weak	_ZTIN6icu_678numparse4impl26AffixPatternMatcherBuilderE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl26AffixPatternMatcherBuilderE,"awG",@progbits,_ZTIN6icu_678numparse4impl26AffixPatternMatcherBuilderE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl26AffixPatternMatcherBuilderE, @object
	.size	_ZTIN6icu_678numparse4impl26AffixPatternMatcherBuilderE, 56
_ZTIN6icu_678numparse4impl26AffixPatternMatcherBuilderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl26AffixPatternMatcherBuilderE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl13TokenConsumerE
	.quad	2
	.quad	_ZTIN6icu_678numparse4impl24MutableMatcherCollectionE
	.quad	2050
	.weak	_ZTSN6icu_678numparse4impl19AffixPatternMatcherE
	.section	.rodata._ZTSN6icu_678numparse4impl19AffixPatternMatcherE,"aG",@progbits,_ZTSN6icu_678numparse4impl19AffixPatternMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl19AffixPatternMatcherE, @object
	.size	_ZTSN6icu_678numparse4impl19AffixPatternMatcherE, 45
_ZTSN6icu_678numparse4impl19AffixPatternMatcherE:
	.string	"N6icu_678numparse4impl19AffixPatternMatcherE"
	.weak	_ZTIN6icu_678numparse4impl19AffixPatternMatcherE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl19AffixPatternMatcherE,"awG",@progbits,_ZTIN6icu_678numparse4impl19AffixPatternMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl19AffixPatternMatcherE, @object
	.size	_ZTIN6icu_678numparse4impl19AffixPatternMatcherE, 24
_ZTIN6icu_678numparse4impl19AffixPatternMatcherE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl19AffixPatternMatcherE
	.quad	_ZTIN6icu_678numparse4impl18ArraySeriesMatcherE
	.weak	_ZTSN6icu_678numparse4impl12AffixMatcherE
	.section	.rodata._ZTSN6icu_678numparse4impl12AffixMatcherE,"aG",@progbits,_ZTSN6icu_678numparse4impl12AffixMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl12AffixMatcherE, @object
	.size	_ZTSN6icu_678numparse4impl12AffixMatcherE, 38
_ZTSN6icu_678numparse4impl12AffixMatcherE:
	.string	"N6icu_678numparse4impl12AffixMatcherE"
	.weak	_ZTIN6icu_678numparse4impl12AffixMatcherE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl12AffixMatcherE,"awG",@progbits,_ZTIN6icu_678numparse4impl12AffixMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl12AffixMatcherE, @object
	.size	_ZTIN6icu_678numparse4impl12AffixMatcherE, 56
_ZTIN6icu_678numparse4impl12AffixMatcherE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl12AffixMatcherE
	.long	0
	.long	2
	.quad	_ZTIN6icu_678numparse4impl18NumberParseMatcherE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTVN6icu_678numparse4impl18CompositionMatcherE
	.section	.data.rel.ro._ZTVN6icu_678numparse4impl18CompositionMatcherE,"awG",@progbits,_ZTVN6icu_678numparse4impl18CompositionMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl18CompositionMatcherE, @object
	.size	_ZTVN6icu_678numparse4impl18CompositionMatcherE, 88
_ZTVN6icu_678numparse4impl18CompositionMatcherE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl18CompositionMatcherE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_678numparse4impl19AffixPatternMatcherE
	.section	.data.rel.ro._ZTVN6icu_678numparse4impl19AffixPatternMatcherE,"awG",@progbits,_ZTVN6icu_678numparse4impl19AffixPatternMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl19AffixPatternMatcherE, @object
	.size	_ZTVN6icu_678numparse4impl19AffixPatternMatcherE, 96
_ZTVN6icu_678numparse4impl19AffixPatternMatcherE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl19AffixPatternMatcherE
	.quad	_ZN6icu_678numparse4impl19AffixPatternMatcherD1Ev
	.quad	_ZN6icu_678numparse4impl19AffixPatternMatcherD0Ev
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl13SeriesMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl13SeriesMatcher9smokeTestERKNS_13StringSegmentE
	.quad	_ZNK6icu_678numparse4impl13SeriesMatcher11postProcessERNS1_12ParsedNumberE
	.quad	_ZNK6icu_678numparse4impl18ArraySeriesMatcher8toStringEv
	.quad	_ZNK6icu_678numparse4impl18ArraySeriesMatcher5beginEv
	.quad	_ZNK6icu_678numparse4impl18ArraySeriesMatcher3endEv
	.quad	_ZNK6icu_678numparse4impl18ArraySeriesMatcher6lengthEv
	.weak	_ZTVN6icu_678numparse4impl26AffixPatternMatcherBuilderE
	.section	.data.rel.ro.local._ZTVN6icu_678numparse4impl26AffixPatternMatcherBuilderE,"awG",@progbits,_ZTVN6icu_678numparse4impl26AffixPatternMatcherBuilderE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl26AffixPatternMatcherBuilderE, @object
	.size	_ZTVN6icu_678numparse4impl26AffixPatternMatcherBuilderE, 88
_ZTVN6icu_678numparse4impl26AffixPatternMatcherBuilderE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl26AffixPatternMatcherBuilderE
	.quad	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD1Ev
	.quad	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilderD0Ev
	.quad	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder12consumeTokenENS_6number4impl16AffixPatternTypeEiR10UErrorCode
	.quad	_ZN6icu_678numparse4impl26AffixPatternMatcherBuilder10addMatcherERNS1_18NumberParseMatcherE
	.quad	-8
	.quad	_ZTIN6icu_678numparse4impl26AffixPatternMatcherBuilderE
	.quad	_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilderD1Ev
	.quad	_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilderD0Ev
	.quad	_ZThn8_N6icu_678numparse4impl26AffixPatternMatcherBuilder10addMatcherERNS1_18NumberParseMatcherE
	.weak	_ZTVN6icu_678numparse4impl16CodePointMatcherE
	.section	.data.rel.ro.local._ZTVN6icu_678numparse4impl16CodePointMatcherE,"awG",@progbits,_ZTVN6icu_678numparse4impl16CodePointMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl16CodePointMatcherE, @object
	.size	_ZTVN6icu_678numparse4impl16CodePointMatcherE, 72
_ZTVN6icu_678numparse4impl16CodePointMatcherE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl16CodePointMatcherE
	.quad	_ZN6icu_678numparse4impl16CodePointMatcherD1Ev
	.quad	_ZN6icu_678numparse4impl16CodePointMatcherD0Ev
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl16CodePointMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl16CodePointMatcher9smokeTestERKNS_13StringSegmentE
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.quad	_ZNK6icu_678numparse4impl16CodePointMatcher8toStringEv
	.weak	_ZTVN6icu_678numparse4impl12AffixMatcherE
	.section	.data.rel.ro.local._ZTVN6icu_678numparse4impl12AffixMatcherE,"awG",@progbits,_ZTVN6icu_678numparse4impl12AffixMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl12AffixMatcherE, @object
	.size	_ZTVN6icu_678numparse4impl12AffixMatcherE, 72
_ZTVN6icu_678numparse4impl12AffixMatcherE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl12AffixMatcherE
	.quad	_ZN6icu_678numparse4impl12AffixMatcherD1Ev
	.quad	_ZN6icu_678numparse4impl12AffixMatcherD0Ev
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl12AffixMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl12AffixMatcher9smokeTestERKNS_13StringSegmentE
	.quad	_ZNK6icu_678numparse4impl12AffixMatcher11postProcessERNS1_12ParsedNumberE
	.quad	_ZNK6icu_678numparse4impl12AffixMatcher8toStringEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
