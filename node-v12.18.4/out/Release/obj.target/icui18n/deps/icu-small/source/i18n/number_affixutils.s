	.file	"number_affixutils.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl13TokenConsumerD2Ev
	.type	_ZN6icu_676number4impl13TokenConsumerD2Ev, @function
_ZN6icu_676number4impl13TokenConsumerD2Ev:
.LFB2711:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2711:
	.size	_ZN6icu_676number4impl13TokenConsumerD2Ev, .-_ZN6icu_676number4impl13TokenConsumerD2Ev
	.globl	_ZN6icu_676number4impl13TokenConsumerD1Ev
	.set	_ZN6icu_676number4impl13TokenConsumerD1Ev,_ZN6icu_676number4impl13TokenConsumerD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl13TokenConsumerD0Ev
	.type	_ZN6icu_676number4impl13TokenConsumerD0Ev, @function
_ZN6icu_676number4impl13TokenConsumerD0Ev:
.LFB2713:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE2713:
	.size	_ZN6icu_676number4impl13TokenConsumerD0Ev, .-_ZN6icu_676number4impl13TokenConsumerD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SymbolProviderD2Ev
	.type	_ZN6icu_676number4impl14SymbolProviderD2Ev, @function
_ZN6icu_676number4impl14SymbolProviderD2Ev:
.LFB2715:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2715:
	.size	_ZN6icu_676number4impl14SymbolProviderD2Ev, .-_ZN6icu_676number4impl14SymbolProviderD2Ev
	.globl	_ZN6icu_676number4impl14SymbolProviderD1Ev
	.set	_ZN6icu_676number4impl14SymbolProviderD1Ev,_ZN6icu_676number4impl14SymbolProviderD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14SymbolProviderD0Ev
	.type	_ZN6icu_676number4impl14SymbolProviderD0Ev, @function
_ZN6icu_676number4impl14SymbolProviderD0Ev:
.LFB2717:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE2717:
	.size	_ZN6icu_676number4impl14SymbolProviderD0Ev, .-_ZN6icu_676number4impl14SymbolProviderD0Ev
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB0:
	.text
.LHOTB0:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl10AffixUtils14estimateLengthERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl10AffixUtils14estimateLengthERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl10AffixUtils14estimateLengthERKNS_13UnicodeStringER10UErrorCode:
.LFB2718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L12:
	cmpl	$39, %eax
	je	.L20
.L27:
	addl	$1, %r12d
.L16:
	cmpl	$65535, %eax
	seta	%al
	movzbl	%al, %eax
	addl	$1, %eax
.L17:
	addl	%eax, %r14d
.L18:
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L7
	sarl	$5, %eax
	cmpl	%eax, %r14d
	jge	.L9
.L29:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$2, %r13d
	je	.L10
	cmpl	$3, %r13d
	je	.L11
	testl	%r13d, %r13d
	je	.L12
	cmpl	$1, %r13d
	jne	.L28
	addl	$1, %r12d
	movl	$2, %r13d
	cmpl	$39, %eax
	jne	.L16
	xorl	%r13d, %r13d
.L15:
	movl	$1, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L7:
	movl	12(%r15), %eax
	cmpl	%eax, %r14d
	jl	.L29
.L9:
	subl	$1, %r13d
	cmpl	$1, %r13d
	ja	.L6
	movl	$1, (%rbx)
.L6:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	addl	$1, %r12d
	cmpl	$39, %eax
	jne	.L16
	movl	$2, %r13d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$39, %eax
	jne	.L27
	movl	$3, %r13d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$1, %r13d
	jmp	.L15
.L28:
	jmp	.L25
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl10AffixUtils14estimateLengthERKNS_13UnicodeStringER10UErrorCode.cold, @function
_ZN6icu_676number4impl10AffixUtils14estimateLengthERKNS_13UnicodeStringER10UErrorCode.cold:
.LFSB2718:
.L25:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE2718:
	.text
	.size	_ZN6icu_676number4impl10AffixUtils14estimateLengthERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl10AffixUtils14estimateLengthERKNS_13UnicodeStringER10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl10AffixUtils14estimateLengthERKNS_13UnicodeStringER10UErrorCode.cold, .-_ZN6icu_676number4impl10AffixUtils14estimateLengthERKNS_13UnicodeStringER10UErrorCode.cold
.LCOLDE0:
	.text
.LHOTE0:
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC1:
	.string	"'"
	.string	"'"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl10AffixUtils6escapeERKNS_13UnicodeStringE
	.type	_ZN6icu_676number4impl10AffixUtils6escapeERKNS_13UnicodeStringE, @function
_ZN6icu_676number4impl10AffixUtils6escapeERKNS_13UnicodeStringE:
.LFB2719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$2, %esi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%si, 8(%rdi)
	movq	%rax, (%rdi)
	leaq	-58(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L38:
	cmpl	$8240, %eax
	je	.L36
.L37:
	cmpl	$2, %r14d
	je	.L61
	.p2align 4,,10
	.p2align 3
.L42:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
.L41:
	xorl	%eax, %eax
	cmpl	$65535, %ebx
	seta	%al
	addl	$1, %eax
.L39:
	addl	%eax, %r15d
.L43:
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L31
	sarl	$5, %eax
	cmpl	%eax, %r15d
	jge	.L33
.L63:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %ebx
	cmpl	$39, %eax
	je	.L34
	jle	.L62
	cmpl	$164, %eax
	je	.L36
	jg	.L38
	leal	-43(%rax), %eax
	andl	$-3, %eax
	jne	.L37
.L36:
	testl	%r14d, %r14d
	jne	.L42
	movq	-72(%rbp), %rsi
	movl	$39, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%cx, -58(%rbp)
	movl	$1, %ecx
	movl	$2, %r14d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L62:
	cmpl	$37, %eax
	je	.L36
	cmpl	$2, %r14d
	jne	.L42
.L61:
	movq	-72(%rbp), %rsi
	movl	$39, %edx
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	movw	%dx, -58(%rbp)
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L31:
	movl	12(%r12), %eax
	cmpl	%eax, %r15d
	jl	.L63
.L33:
	cmpl	$2, %r14d
	je	.L64
.L30:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC1(%rip), %rax
	movl	$1, %eax
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$39, %eax
	leaq	-58(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L30
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2719:
	.size	_ZN6icu_676number4impl10AffixUtils6escapeERKNS_13UnicodeStringE, .-_ZN6icu_676number4impl10AffixUtils6escapeERKNS_13UnicodeStringE
	.section	.text.unlikely
	.align 2
.LCOLDB2:
	.text
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl10AffixUtils15getFieldForTypeENS1_16AffixPatternTypeE
	.type	_ZN6icu_676number4impl10AffixUtils15getFieldForTypeENS1_16AffixPatternTypeE, @function
_ZN6icu_676number4impl10AffixUtils15getFieldForTypeENS1_16AffixPatternTypeE:
.LFB2720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$-15, %edi
	jb	.L67
	leaq	.L69(%rip), %rdx
	leal	15(%rdi), %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L69:
	.long	.L72-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L72-.L69
	.long	.L72-.L69
	.long	.L72-.L69
	.long	.L72-.L69
	.long	.L72-.L69
	.long	.L71-.L69
	.long	.L70-.L69
	.long	.L68-.L69
	.long	.L68-.L69
	.text
	.p2align 4,,10
	.p2align 3
.L72:
	movl	$39, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	$42, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movl	$41, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movl	$40, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl10AffixUtils15getFieldForTypeENS1_16AffixPatternTypeE.cold, @function
_ZN6icu_676number4impl10AffixUtils15getFieldForTypeENS1_16AffixPatternTypeE.cold:
.LFSB2720:
.L67:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE2720:
	.text
	.size	_ZN6icu_676number4impl10AffixUtils15getFieldForTypeENS1_16AffixPatternTypeE, .-_ZN6icu_676number4impl10AffixUtils15getFieldForTypeENS1_16AffixPatternTypeE
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl10AffixUtils15getFieldForTypeENS1_16AffixPatternTypeE.cold, .-_ZN6icu_676number4impl10AffixUtils15getFieldForTypeENS1_16AffixPatternTypeE.cold
.LCOLDE2:
	.text
.LHOTE2:
	.section	.text.unlikely
	.align 2
.LCOLDB3:
	.text
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode:
.LFB2728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	movswl	8(%rdx), %ecx
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	js	.L77
.L107:
	cmpl	%r12d, %ecx
	jle	.L108
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$65535, %eax
	ja	.L109
	cmpl	$9, %r14d
	ja	.L110
	leaq	.L111(%rip), %rsi
	movl	%r14d, %ecx
	movslq	(%rsi,%rcx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L111:
	.long	.L82-.L111
	.long	.L105-.L111
	.long	.L149-.L111
	.long	.L102-.L111
	.long	.L79-.L111
	.long	.L99-.L111
	.long	.L96-.L111
	.long	.L93-.L111
	.long	.L90-.L111
	.long	.L150-.L111
	.text
	.p2align 4,,10
	.p2align 3
.L141:
	movl	%r14d, %r12d
	movl	$4, %r14d
	.p2align 4,,10
	.p2align 3
.L77:
	movl	12(%r13), %ecx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L109:
	cmpl	$9, %r14d
	ja	.L110
	leaq	.L140(%rip), %rsi
	movl	%r14d, %ecx
	movslq	(%rsi,%rcx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L140:
	.long	.L83-.L140
	.long	.L151-.L140
	.long	.L112-.L140
	.long	.L124-.L140
	.long	.L78-.L140
	.long	.L98-.L140
	.long	.L95-.L140
	.long	.L92-.L140
	.long	.L89-.L140
	.long	.L152-.L140
	.text
	.p2align 4,,10
	.p2align 3
.L149:
	movl	$1, %r14d
.L112:
	addl	%r14d, %r12d
	cmpl	$39, %eax
	jne	.L159
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L146
	sarl	$5, %eax
	cmpl	%eax, %r12d
	jge	.L101
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$65535, %eax
	ja	.L124
.L102:
	cmpl	$39, %eax
	je	.L160
.L124:
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L144
	sarl	$5, %eax
	cmpl	%r12d, %eax
	jle	.L101
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$65535, %eax
	ja	.L83
.L82:
	leal	1(%r12), %r14d
	cmpl	$45, %eax
	je	.L113
	jg	.L114
	cmpl	$39, %eax
	jne	.L139
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L147
	sarl	$5, %eax
	cmpl	%eax, %r14d
	jge	.L104
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$65535, %eax
	ja	.L161
	movl	%r14d, %r12d
	movl	$1, %r14d
	.p2align 4,,10
	.p2align 3
.L105:
	leal	(%r14,%r12), %esi
	cmpl	$39, %eax
	je	.L162
.L122:
	salq	$32, %rax
	movl	%esi, %r8d
	movl	$2, %edx
	orq	%rax, %r8
.L153:
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movl	$2, %r14d
	leal	(%r14,%r12), %esi
	cmpl	$39, %eax
	jne	.L122
.L162:
	movabsq	$167503724544, %r8
	movl	%esi, %eax
	popq	%rbx
	xorl	%edx, %edx
	orq	%rax, %r8
	popq	%r12
	popq	%r13
	movq	%r8, %rax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	cmpl	$164, %eax
	jne	.L78
	movswl	8(%r13), %eax
	addl	$1, %r12d
	movl	$5, %r14d
	testw	%ax, %ax
	js	.L77
	sarl	$5, %eax
	cmpl	%eax, %r12d
	jge	.L98
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$65535, %eax
	ja	.L98
.L99:
	cmpl	$164, %eax
	jne	.L98
	movswl	8(%r13), %eax
	addl	$1, %r12d
	movl	$6, %r14d
	testw	%ax, %ax
	js	.L77
	sarl	$5, %eax
	cmpl	%eax, %r12d
	jge	.L95
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$65535, %eax
	ja	.L95
.L96:
	cmpl	$164, %eax
	jne	.L95
	movswl	8(%r13), %eax
	addl	$1, %r12d
	movl	$7, %r14d
	testw	%ax, %ax
	js	.L77
	sarl	$5, %eax
	cmpl	%eax, %r12d
	jge	.L92
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$65535, %eax
	ja	.L92
.L93:
	cmpl	$164, %eax
	jne	.L92
	movswl	8(%r13), %eax
	addl	$1, %r12d
	movl	$8, %r14d
	testw	%ax, %ax
	js	.L77
	sarl	$5, %eax
	cmpl	%eax, %r12d
	jge	.L89
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$65535, %eax
	ja	.L89
.L90:
	cmpl	$164, %eax
	jne	.L89
	movswl	8(%r13), %eax
	addl	$1, %r12d
	testw	%ax, %ax
	js	.L156
.L134:
	sarl	$5, %eax
	cmpl	%eax, %r12d
	jge	.L137
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%edx, %edx
	cmpl	$65535, %eax
	seta	%dl
	addl	$1, %edx
.L88:
	cmpl	$164, %eax
	jne	.L137
	movswl	8(%r13), %eax
	addl	%edx, %r12d
	testw	%ax, %ax
	jns	.L134
.L156:
	movl	12(%r13), %ecx
	movl	$9, %r14d
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L83:
	leal	2(%r12), %r14d
	cmpl	$45, %eax
	jg	.L114
.L139:
	movabsq	$-8589934592, %rdx
	movl	%r14d, %r8d
	cmpl	$43, %eax
	je	.L153
	movabsq	$-12884901888, %rdx
	cmpl	$37, %eax
	je	.L153
.L118:
	salq	$32, %rax
	movl	%r14d, %r8d
	xorl	%edx, %edx
	orq	%rax, %r8
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L114:
	cmpl	$164, %eax
	je	.L119
	cmpl	$8240, %eax
	jne	.L118
	movabsq	$-17179869184, %rdx
	movl	%r14d, %r8d
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L113:
	movabsq	$-4294967296, %rdx
	movl	%r14d, %r8d
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L144:
	movl	12(%r13), %ecx
	xorl	%r14d, %r14d
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L119:
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L141
	sarl	$5, %eax
	cmpl	%r14d, %eax
	jle	.L142
	movl	%r14d, %esi
	movq	%r13, %rdi
	movl	%r14d, %r12d
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$65535, %eax
	jbe	.L79
.L78:
	movabsq	$-21474836480, %rdx
	movl	%r12d, %r8d
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L146:
	movl	12(%r13), %ecx
	movl	$3, %r14d
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L108:
	cmpl	$9, %r14d
	ja	.L110
	leaq	.L138(%rip), %rdx
	movl	%r14d, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L138:
	.long	.L101-.L138
	.long	.L104-.L138
	.long	.L104-.L138
	.long	.L101-.L138
	.long	.L78-.L138
	.long	.L98-.L138
	.long	.L95-.L138
	.long	.L92-.L138
	.long	.L89-.L138
	.long	.L137-.L138
	.text
	.p2align 4,,10
	.p2align 3
.L98:
	movabsq	$-25769803776, %rdx
	movl	%r12d, %r8d
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L95:
	movabsq	$-30064771072, %rdx
	movl	%r12d, %r8d
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L92:
	movabsq	$-34359738368, %rdx
	movl	%r12d, %r8d
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L89:
	movabsq	$-38654705664, %rdx
	movl	%r12d, %r8d
	jmp	.L153
.L152:
	movl	$2, %edx
	jmp	.L88
.L150:
	movl	$1, %edx
	jmp	.L88
.L104:
	movl	$1, (%rbx)
	movl	$4294967295, %r8d
	xorl	%edx, %edx
	jmp	.L153
.L160:
	leal	1(%r12), %eax
	movl	$2, %edx
	movabsq	$167503724544, %r8
	orq	%rax, %r8
	jmp	.L153
.L101:
	movl	$4294967295, %r8d
	xorl	%edx, %edx
	jmp	.L153
.L137:
	movabsq	$-64424509440, %rdx
	movl	%r12d, %r8d
	jmp	.L153
.L159:
	salq	$32, %rax
	movl	%r12d, %r8d
	movl	$2, %edx
	orq	%rax, %r8
	jmp	.L153
.L142:
	movl	%r14d, %r12d
	jmp	.L78
.L161:
	leal	3(%r12), %esi
	jmp	.L122
.L147:
	movl	%r14d, %r12d
	movl	$1, %r14d
	jmp	.L77
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode.cold, @function
_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode.cold:
.LFSB2728:
.L110:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	call	abort@PLT
	.cfi_endproc
.LFE2728:
	.text
	.size	_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode.cold, .-_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode.cold
.LCOLDE3:
	.text
.LHOTE3:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl10AffixUtils8unescapeERKNS_13UnicodeStringERNS_22FormattedStringBuilderEiRKNS1_14SymbolProviderENS6_5FieldER10UErrorCode
	.type	_ZN6icu_676number4impl10AffixUtils8unescapeERKNS_13UnicodeStringERNS_22FormattedStringBuilderEiRKNS1_14SymbolProviderENS6_5FieldER10UErrorCode, @function
_ZN6icu_676number4impl10AffixUtils8unescapeERKNS_13UnicodeStringERNS_22FormattedStringBuilderEiRKNS1_14SymbolProviderENS6_5FieldER10UErrorCode:
.LFB2721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -144(%rbp)
	movl	%edx, -136(%rbp)
	movq	%rcx, -176(%rbp)
	movb	%r8b, -154(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -132(%rbp)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L189:
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L167
	sarl	$5, %eax
.L168:
	testl	%eax, %eax
	setg	%al
.L169:
	testb	%al, %al
	je	.L165
.L173:
	movl	%r13d, %edi
	movl	%r10d, %esi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movabsq	$-4294967296, %rax
	andq	%rax, %r12
	andq	%rax, %rbx
	orq	%r12, %rdi
	orq	%rbx, %rsi
	call	_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode
	movq	%rax, %r12
	movl	%eax, %r13d
	movl	(%r14), %eax
	movq	%rdx, %rbx
	testl	%eax, %eax
	jg	.L165
	sarq	$32, %rdx
	movl	-136(%rbp), %r9d
	addl	-132(%rbp), %r9d
	cmpl	$-15, %edx
	je	.L187
	testl	%edx, %edx
	js	.L188
	movl	%ebx, -152(%rbp)
	movq	%r12, %rdx
	movzbl	-154(%rbp), %ecx
	movq	%r14, %r8
	sarq	$32, %rdx
.L186:
	movq	-144(%rbp), %rdi
	movl	%r9d, %esi
	call	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode@PLT
	movl	-152(%rbp), %r10d
	addl	%eax, -132(%rbp)
	testl	%r13d, %r13d
	js	.L165
.L164:
	testl	%r13d, %r13d
	je	.L189
	cmpl	$2, %r10d
	je	.L190
	testl	%r10d, %r10d
	jne	.L173
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L176
	sarl	$5, %eax
.L177:
	cmpl	%r13d, %eax
	setg	%al
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L188:
	movl	%edx, %edi
	movl	%r9d, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movl	%ebx, -164(%rbp)
	call	_ZN6icu_676number4impl10AffixUtils15getFieldForTypeENS1_16AffixPatternTypeE
	movq	-176(%rbp), %rsi
	leaq	-128(%rbp), %r11
	movq	-152(%rbp), %rdx
	movb	%al, -153(%rbp)
	movq	%r11, %rdi
	movq	(%rsi), %rax
	movq	%r11, -152(%rbp)
	call	*16(%rax)
	movq	-152(%rbp), %r11
	movq	%r14, %r8
	movl	-160(%rbp), %r9d
	movzbl	-153(%rbp), %ecx
	movq	-144(%rbp), %rdi
	movq	%r11, %rdx
	movl	%r9d, %esi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	-152(%rbp), %r11
	addl	%eax, -132(%rbp)
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-164(%rbp), %r10d
	testl	%r13d, %r13d
	jns	.L164
.L165:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L191
	movl	-132(%rbp), %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movl	%ebx, -152(%rbp)
	movq	%r14, %r8
	movl	$39, %ecx
	movl	$65533, %edx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L167:
	movl	12(%r15), %eax
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L190:
	movzwl	8(%r15), %edx
	testw	%dx, %dx
	js	.L171
	movswl	%dx, %eax
	sarl	$5, %eax
.L172:
	leal	-1(%rax), %ecx
	cmpl	%r13d, %ecx
	jne	.L173
	cmpl	%r13d, %eax
	jbe	.L173
	andl	$2, %edx
	leaq	10(%r15), %rdx
	jne	.L175
	movq	24(%r15), %rdx
.L175:
	movslq	%r13d, %rax
	cmpw	$39, (%rdx,%rax,2)
	jne	.L173
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L176:
	movl	12(%r15), %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L171:
	movl	12(%r15), %eax
	jmp	.L172
.L191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2721:
	.size	_ZN6icu_676number4impl10AffixUtils8unescapeERKNS_13UnicodeStringERNS_22FormattedStringBuilderEiRKNS1_14SymbolProviderENS6_5FieldER10UErrorCode, .-_ZN6icu_676number4impl10AffixUtils8unescapeERKNS_13UnicodeStringERNS_22FormattedStringBuilderEiRKNS1_14SymbolProviderENS6_5FieldER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl10AffixUtils23unescapedCodePointCountERKNS_13UnicodeStringERKNS1_14SymbolProviderER10UErrorCode
	.type	_ZN6icu_676number4impl10AffixUtils23unescapedCodePointCountERKNS_13UnicodeStringERKNS1_14SymbolProviderER10UErrorCode, @function
_ZN6icu_676number4impl10AffixUtils23unescapedCodePointCountERKNS_13UnicodeStringERKNS1_14SymbolProviderER10UErrorCode:
.LFB2722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$-4294967296, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L221:
	movswl	8(%rbx), %edx
	testw	%dx, %dx
	js	.L196
	sarl	$5, %edx
.L197:
	testl	%edx, %edx
	setg	%dl
.L198:
	testb	%dl, %dl
	je	.L194
.L202:
	movl	%r8d, %edi
	movl	%r9d, %esi
	andq	%r13, %rax
	andq	%r13, %rcx
	orq	%rcx, %rsi
	orq	%rax, %rdi
	movq	%r12, %rcx
	movq	%rbx, %rdx
	call	_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode
	movq	%rdx, %rcx
	movl	%edx, %r9d
	movl	(%r12), %edx
	movl	%eax, %r8d
	testl	%edx, %edx
	jg	.L194
	movq	%rcx, %rdx
	sarq	$32, %rdx
	cmpl	$-15, %edx
	je	.L219
	testl	%edx, %edx
	js	.L220
	movq	%rax, %rdx
	sarq	$32, %rdx
	cmpl	$65535, %edx
	seta	%dl
	movzbl	%dl, %edx
	leal	1(%rdx,%r14), %r14d
.L208:
	testl	%r8d, %r8d
	js	.L194
.L193:
	testl	%r8d, %r8d
	je	.L221
	cmpl	$2, %r9d
	je	.L222
	testl	%r9d, %r9d
	jne	.L202
	movswl	8(%rbx), %edx
	testw	%dx, %dx
	js	.L205
	sarl	$5, %edx
.L206:
	cmpl	%r8d, %edx
	setg	%dl
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L219:
	addl	$1, %r14d
	testl	%r8d, %r8d
	jns	.L193
.L194:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	addq	$136, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movq	(%r15), %r10
	leaq	-128(%rbp), %rdi
	movl	%ecx, -140(%rbp)
	movq	%r15, %rsi
	movq	%rcx, -168(%rbp)
	movq	%rax, -160(%rbp)
	movl	%r8d, -152(%rbp)
	movq	%rdi, -136(%rbp)
	call	*16(%r10)
	movswl	-120(%rbp), %edx
	movq	-136(%rbp), %rdi
	movl	-140(%rbp), %r9d
	movl	-152(%rbp), %r8d
	testw	%dx, %dx
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	js	.L210
	sarl	$5, %edx
.L211:
	movq	%rcx, -160(%rbp)
	addl	%edx, %r14d
	movq	%rax, -152(%rbp)
	movl	%r8d, -140(%rbp)
	movl	%r9d, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-136(%rbp), %r9d
	movl	-140(%rbp), %r8d
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %rcx
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L196:
	movl	12(%rbx), %edx
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L222:
	movzwl	8(%rbx), %esi
	testw	%si, %si
	js	.L200
	movswl	%si, %edx
	sarl	$5, %edx
.L201:
	leal	-1(%rdx), %edi
	cmpl	%r8d, %edi
	jne	.L202
	cmpl	%r8d, %edx
	jbe	.L202
	andl	$2, %esi
	leaq	10(%rbx), %rsi
	jne	.L204
	movq	24(%rbx), %rsi
.L204:
	movslq	%r8d, %rdx
	cmpw	$39, (%rsi,%rdx,2)
	jne	.L202
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L210:
	movl	-116(%rbp), %edx
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L205:
	movl	12(%rbx), %edx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L200:
	movl	12(%rbx), %edx
	jmp	.L201
.L223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2722:
	.size	_ZN6icu_676number4impl10AffixUtils23unescapedCodePointCountERKNS_13UnicodeStringERKNS1_14SymbolProviderER10UErrorCode, .-_ZN6icu_676number4impl10AffixUtils23unescapedCodePointCountERKNS_13UnicodeStringERKNS1_14SymbolProviderER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode
	.type	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode, @function
_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode:
.LFB2723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movswl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L225
	movswl	%dx, %edi
	sarl	$5, %edi
.L226:
	movabsq	$-4294967296, %rbx
	xorl	%esi, %esi
	testl	%edi, %edi
	je	.L230
	testw	%dx, %dx
	js	.L232
.L250:
	sarl	$5, %edx
.L233:
	testl	%edx, %edx
	setg	%r12b
	xorl	%edi, %edi
	testb	%r12b, %r12b
	je	.L230
.L243:
	andq	%rbx, %rax
	andq	%rbx, %rcx
	movl	%edi, %edi
	movq	%r14, %rdx
	orq	%rax, %rdi
	orq	%rcx, %rsi
	movq	%r15, %rcx
	call	_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode
	movq	%rdx, %rcx
	movl	(%r15), %edx
	movl	%eax, %edi
	testl	%edx, %edx
	jg	.L230
	movq	%rcx, %rdx
	sarq	$32, %rdx
	cmpl	%edx, %r13d
	je	.L228
	testl	%edi, %edi
	js	.L230
	movl	%ecx, %esi
	je	.L248
	cmpl	$2, %ecx
	je	.L249
	testl	%esi, %esi
	jne	.L243
	movswl	8(%r14), %edx
	testw	%dx, %dx
	js	.L241
	sarl	$5, %edx
.L242:
	cmpl	%edi, %edx
	setg	%r12b
	testb	%r12b, %r12b
	jne	.L243
	.p2align 4,,10
	.p2align 3
.L230:
	xorl	%r12d, %r12d
.L228:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	movswl	8(%r14), %edx
	testw	%dx, %dx
	jns	.L250
.L232:
	movl	12(%r14), %edx
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L249:
	movzwl	8(%r14), %r8d
	testw	%r8w, %r8w
	js	.L236
	movswl	%r8w, %edx
	sarl	$5, %edx
.L237:
	leal	-1(%rdx), %r9d
	cmpl	%edi, %r9d
	jne	.L243
	cmpl	%edi, %edx
	jbe	.L243
	andl	$2, %r8d
	leaq	10(%r14), %r8
	jne	.L240
	movq	24(%r14), %r8
.L240:
	movslq	%edi, %rdx
	cmpw	$39, (%r8,%rdx,2)
	jne	.L243
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L241:
	movl	12(%r14), %edx
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L236:
	movl	12(%r14), %edx
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L225:
	movl	12(%rdi), %edi
	jmp	.L226
	.cfi_endproc
.LFE2723:
	.size	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode, .-_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl10AffixUtils18hasCurrencySymbolsERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl10AffixUtils18hasCurrencySymbolsERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl10AffixUtils18hasCurrencySymbolsERKNS_13UnicodeStringER10UErrorCode:
.LFB2724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L252
	sarl	$5, %eax
.L253:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jne	.L270
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L277:
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L257
	sarl	$5, %eax
.L258:
	testl	%eax, %eax
	setg	%al
.L259:
	testb	%al, %al
	je	.L266
.L263:
	movl	%r13d, %edi
	movl	%r14d, %esi
	movq	%r15, %rdx
	movq	%rcx, -56(%rbp)
	movabsq	$-4294967296, %rax
	andq	%rax, %r12
	andq	%rax, %rbx
	orq	%r12, %rdi
	orq	%rbx, %rsi
	call	_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode
	movq	-56(%rbp), %rcx
	movq	%rax, %r12
	movl	%eax, %r13d
	movq	%rdx, %rbx
	movl	%edx, %r14d
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L266
	movq	%rdx, %rdi
	sarq	$32, %rdi
	testl	%edi, %edi
	js	.L276
.L269:
	testl	%r13d, %r13d
	js	.L266
.L270:
	testl	%r13d, %r13d
	je	.L277
	cmpl	$2, %r14d
	je	.L278
	testl	%r14d, %r14d
	jne	.L263
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L267
	sarl	$5, %eax
.L268:
	cmpl	%r13d, %eax
	setg	%al
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L278:
	movzwl	8(%r15), %edx
	testw	%dx, %dx
	js	.L261
	movswl	%dx, %eax
	sarl	$5, %eax
.L262:
	leal	-1(%rax), %esi
	cmpl	%r13d, %esi
	jne	.L263
	cmpl	%r13d, %eax
	jbe	.L263
	andl	$2, %edx
	leaq	10(%r15), %rdx
	jne	.L265
	movq	24(%r15), %rdx
.L265:
	movslq	%r13d, %rax
	cmpw	$39, (%rdx,%rax,2)
	jne	.L263
	.p2align 4,,10
	.p2align 3
.L266:
	xorl	%eax, %eax
.L255:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	call	_ZN6icu_676number4impl10AffixUtils15getFieldForTypeENS1_16AffixPatternTypeE
	movq	-56(%rbp), %rcx
	cmpb	$39, %al
	jne	.L269
	movl	$1, %eax
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L257:
	movl	12(%r15), %eax
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L267:
	movl	12(%r15), %eax
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L261:
	movl	12(%r15), %eax
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L252:
	movl	12(%rdi), %eax
	jmp	.L253
	.cfi_endproc
.LFE2724:
	.size	_ZN6icu_676number4impl10AffixUtils18hasCurrencySymbolsERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl10AffixUtils18hasCurrencySymbolsERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl10AffixUtils11replaceTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeEDsR10UErrorCode
	.type	_ZN6icu_676number4impl10AffixUtils11replaceTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeEDsR10UErrorCode, @function
_ZN6icu_676number4impl10AffixUtils11replaceTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeEDsR10UErrorCode:
.LFB2725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -80(%rbp)
	movl	%edx, -68(%rbp)
	movq	%r8, -88(%rbp)
	movw	%cx, -70(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movswl	8(%r15), %eax
	movq	-88(%rbp), %r11
	testw	%ax, %ax
	js	.L280
	sarl	$5, %eax
.L281:
	testl	%eax, %eax
	je	.L282
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	%r14d, %r10d
	movq	%r11, %r14
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L307:
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L284
	sarl	$5, %eax
.L285:
	testl	%eax, %eax
	setg	%al
.L286:
	testb	%al, %al
	je	.L282
.L290:
	movl	%r10d, %esi
	movl	%r13d, %edi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movabsq	$-4294967296, %rax
	andq	%rax, %r12
	andq	%rax, %rbx
	orq	%r12, %rdi
	orq	%rbx, %rsi
	call	_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode
	movq	%rax, %r12
	movl	%eax, %r13d
	movl	(%r14), %eax
	movq	%rdx, %rbx
	movl	%edx, %r10d
	testl	%eax, %eax
	jg	.L282
	movq	%rdx, %rax
	sarq	$32, %rax
	cmpl	%eax, -68(%rbp)
	je	.L306
.L295:
	testl	%r13d, %r13d
	js	.L282
	je	.L307
	cmpl	$2, %r10d
	je	.L308
	testl	%r10d, %r10d
	jne	.L290
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L293
	sarl	$5, %eax
.L294:
	cmpl	%r13d, %eax
	setg	%al
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L308:
	movzwl	8(%r15), %edx
	testw	%dx, %dx
	js	.L288
	movswl	%dx, %eax
	sarl	$5, %eax
.L289:
	leal	-1(%rax), %ecx
	cmpl	%r13d, %ecx
	jne	.L290
	cmpl	%r13d, %eax
	jbe	.L290
	andl	$2, %edx
	leaq	10(%r15), %rdx
	jne	.L292
	movq	24(%r15), %rdx
.L292:
	movslq	%r13d, %rax
	cmpw	$39, (%rdx,%rax,2)
	jne	.L290
	.p2align 4,,10
	.p2align 3
.L282:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L309
	movq	-80(%rbp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movl	12(%r15), %eax
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L293:
	movl	12(%r15), %eax
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L280:
	movl	12(%r15), %eax
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L288:
	movl	12(%r15), %eax
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L306:
	movzwl	-70(%rbp), %eax
	movq	-80(%rbp), %rdi
	movl	%edx, -88(%rbp)
	leaq	-58(%rbp), %rcx
	leal	-1(%r12), %esi
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movl	-88(%rbp), %r10d
	jmp	.L295
.L309:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2725:
	.size	_ZN6icu_676number4impl10AffixUtils11replaceTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeEDsR10UErrorCode, .-_ZN6icu_676number4impl10AffixUtils11replaceTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeEDsR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl10AffixUtils19iterateWithConsumerERKNS_13UnicodeStringERNS1_13TokenConsumerER10UErrorCode
	.type	_ZN6icu_676number4impl10AffixUtils19iterateWithConsumerERKNS_13UnicodeStringERNS1_13TokenConsumerER10UErrorCode, @function
_ZN6icu_676number4impl10AffixUtils19iterateWithConsumerERKNS_13UnicodeStringERNS1_13TokenConsumerER10UErrorCode:
.LFB2727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L311
	movswl	%ax, %edx
	sarl	$5, %edx
.L312:
	xorl	%r9d, %r9d
	testl	%edx, %edx
	je	.L313
	testw	%ax, %ax
	js	.L316
.L336:
	sarl	$5, %eax
.L317:
	testl	%eax, %eax
	setg	%al
	xorl	%r12d, %r12d
	testb	%al, %al
	je	.L313
.L327:
	movabsq	$-4294967296, %rax
	movl	%r12d, %edi
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%rax, %rsi
	andq	%rax, %rbx
	andq	%r8, %rsi
	orq	%rbx, %rdi
	orq	%r9, %rsi
	call	_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode
	movl	%edx, -52(%rbp)
	movq	%rdx, %r8
	movl	(%r15), %edx
	movq	%rax, %rbx
	movl	%eax, %r12d
	testl	%edx, %edx
	jg	.L313
	movq	(%r14), %rax
	movq	%r8, %rsi
	movq	%rbx, %rdx
	movq	%r8, -64(%rbp)
	sarq	$32, %rdx
	sarq	$32, %rsi
	movq	%r15, %rcx
	movq	%r14, %rdi
	call	*16(%rax)
	movl	(%r15), %eax
	movq	-64(%rbp), %r8
	movl	-52(%rbp), %r9d
	testl	%eax, %eax
	jg	.L313
	testl	%r12d, %r12d
	js	.L313
	je	.L334
	cmpl	$2, %r9d
	je	.L335
	testl	%r9d, %r9d
	jne	.L327
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L325
	sarl	$5, %eax
.L326:
	cmpl	%r12d, %eax
	setg	%al
	testb	%al, %al
	jne	.L327
.L313:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movswl	8(%r13), %eax
	testw	%ax, %ax
	jns	.L336
.L316:
	movl	12(%r13), %eax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L335:
	movzwl	8(%r13), %edx
	testw	%dx, %dx
	js	.L320
	movswl	%dx, %eax
	sarl	$5, %eax
.L321:
	leal	-1(%rax), %esi
	cmpl	%r12d, %esi
	jne	.L327
	cmpl	%r12d, %eax
	jbe	.L327
	andl	$2, %edx
	leaq	10(%r13), %rdx
	jne	.L324
	movq	24(%r13), %rdx
.L324:
	movslq	%r12d, %rax
	cmpw	$39, (%rdx,%rax,2)
	jne	.L327
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L325:
	movl	12(%r13), %eax
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L320:
	movl	12(%r13), %eax
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L311:
	movl	12(%rdi), %edx
	jmp	.L312
	.cfi_endproc
.LFE2727:
	.size	_ZN6icu_676number4impl10AffixUtils19iterateWithConsumerERKNS_13UnicodeStringERNS1_13TokenConsumerER10UErrorCode, .-_ZN6icu_676number4impl10AffixUtils19iterateWithConsumerERKNS_13UnicodeStringERNS1_13TokenConsumerER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl10AffixUtils7hasNextERKNS1_8AffixTagERKNS_13UnicodeStringE
	.type	_ZN6icu_676number4impl10AffixUtils7hasNextERKNS1_8AffixTagERKNS_13UnicodeStringE, @function
_ZN6icu_676number4impl10AffixUtils7hasNextERKNS1_8AffixTagERKNS_13UnicodeStringE:
.LFB2729:
	.cfi_startproc
	endbr64
	movslq	(%rdi), %rdx
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L337
	jne	.L339
	movswl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L340
	sarl	$5, %eax
	testl	%eax, %eax
	setg	%al
	ret
.L352:
	movl	$1, %eax
.L337:
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	movl	8(%rdi), %eax
	cmpl	$2, %eax
	je	.L353
	testl	%eax, %eax
	jne	.L352
	movswl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L347
	sarl	$5, %eax
.L348:
	cmpl	%eax, %edx
	setl	%al
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	movl	12(%rsi), %eax
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L340:
	movl	12(%rsi), %eax
	testl	%eax, %eax
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	movzwl	8(%rsi), %edi
	testw	%di, %di
	js	.L343
	movswl	%di, %ecx
	sarl	$5, %ecx
.L344:
	leal	-1(%rcx), %r8d
	movl	$1, %eax
	cmpl	%r8d, %edx
	jne	.L337
	cmpl	%edx, %ecx
	jbe	.L337
	andl	$2, %edi
	je	.L345
	addq	$10, %rsi
.L346:
	cmpw	$39, (%rsi,%rdx,2)
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	movl	12(%rsi), %ecx
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L345:
	movq	24(%rsi), %rsi
	jmp	.L346
	.cfi_endproc
.LFE2729:
	.size	_ZN6icu_676number4impl10AffixUtils7hasNextERKNS1_8AffixTagERKNS_13UnicodeStringE, .-_ZN6icu_676number4impl10AffixUtils7hasNextERKNS1_8AffixTagERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl10AffixUtils32containsOnlySymbolsAndIgnorablesERKNS_13UnicodeStringERKNS_10UnicodeSetER10UErrorCode
	.type	_ZN6icu_676number4impl10AffixUtils32containsOnlySymbolsAndIgnorablesERKNS_13UnicodeStringERKNS_10UnicodeSetER10UErrorCode, @function
_ZN6icu_676number4impl10AffixUtils32containsOnlySymbolsAndIgnorablesERKNS_13UnicodeStringERKNS_10UnicodeSetER10UErrorCode:
.LFB2726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movswl	8(%rdi), %eax
	movq	%rsi, -56(%rbp)
	testw	%ax, %ax
	js	.L355
	sarl	$5, %eax
.L356:
	testl	%eax, %eax
	je	.L357
	xorl	%edx, %edx
	xorl	%r13d, %r13d
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L387:
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L359
	sarl	$5, %eax
.L360:
	testl	%eax, %eax
	setg	%al
.L361:
	testb	%al, %al
	je	.L357
.L365:
	movabsq	$-4294967296, %rax
	movl	%r13d, %edi
	movl	%edx, %esi
	movq	%r14, %rcx
	andq	%rax, %r12
	andq	%rax, %rbx
	movq	%r15, %rdx
	orq	%r12, %rdi
	orq	%rbx, %rsi
	call	_ZN6icu_676number4impl10AffixUtils9nextTokenENS1_8AffixTagERKNS_13UnicodeStringER10UErrorCode
	movq	%rax, %r12
	movl	%eax, %r13d
	movl	(%r14), %eax
	movq	%rdx, %rbx
	testl	%eax, %eax
	jg	.L373
	movq	%rdx, %rax
	sarq	$32, %rax
	testl	%eax, %eax
	je	.L386
.L372:
	testl	%r13d, %r13d
	js	.L357
	je	.L387
	cmpl	$2, %edx
	je	.L388
	testl	%edx, %edx
	jne	.L365
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L368
	sarl	$5, %eax
.L369:
	cmpl	%r13d, %eax
	setg	%al
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L388:
	movzwl	8(%r15), %esi
	testw	%si, %si
	js	.L363
	movswl	%si, %eax
	sarl	$5, %eax
.L364:
	leal	-1(%rax), %edi
	cmpl	%r13d, %edi
	jne	.L365
	cmpl	%r13d, %eax
	jbe	.L365
	andl	$2, %esi
	leaq	10(%r15), %rsi
	jne	.L367
	movq	24(%r15), %rsi
.L367:
	movslq	%r13d, %rax
	cmpw	$39, (%rsi,%rax,2)
	jne	.L365
.L357:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	movl	12(%r15), %eax
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L368:
	movl	12(%r15), %eax
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L363:
	movl	12(%r15), %eax
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L355:
	movl	12(%rdi), %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L386:
	movq	-56(%rbp), %rdi
	movq	%r12, %rsi
	movl	%edx, -60(%rbp)
	sarq	$32, %rsi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	-60(%rbp), %edx
	testb	%al, %al
	jne	.L372
.L373:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2726:
	.size	_ZN6icu_676number4impl10AffixUtils32containsOnlySymbolsAndIgnorablesERKNS_13UnicodeStringERKNS_10UnicodeSetER10UErrorCode, .-_ZN6icu_676number4impl10AffixUtils32containsOnlySymbolsAndIgnorablesERKNS_13UnicodeStringERKNS_10UnicodeSetER10UErrorCode
	.weak	_ZTSN6icu_676number4impl13TokenConsumerE
	.section	.rodata._ZTSN6icu_676number4impl13TokenConsumerE,"aG",@progbits,_ZTSN6icu_676number4impl13TokenConsumerE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl13TokenConsumerE, @object
	.size	_ZTSN6icu_676number4impl13TokenConsumerE, 37
_ZTSN6icu_676number4impl13TokenConsumerE:
	.string	"N6icu_676number4impl13TokenConsumerE"
	.weak	_ZTIN6icu_676number4impl13TokenConsumerE
	.section	.data.rel.ro._ZTIN6icu_676number4impl13TokenConsumerE,"awG",@progbits,_ZTIN6icu_676number4impl13TokenConsumerE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl13TokenConsumerE, @object
	.size	_ZTIN6icu_676number4impl13TokenConsumerE, 16
_ZTIN6icu_676number4impl13TokenConsumerE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl13TokenConsumerE
	.weak	_ZTSN6icu_676number4impl14SymbolProviderE
	.section	.rodata._ZTSN6icu_676number4impl14SymbolProviderE,"aG",@progbits,_ZTSN6icu_676number4impl14SymbolProviderE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl14SymbolProviderE, @object
	.size	_ZTSN6icu_676number4impl14SymbolProviderE, 38
_ZTSN6icu_676number4impl14SymbolProviderE:
	.string	"N6icu_676number4impl14SymbolProviderE"
	.weak	_ZTIN6icu_676number4impl14SymbolProviderE
	.section	.data.rel.ro._ZTIN6icu_676number4impl14SymbolProviderE,"awG",@progbits,_ZTIN6icu_676number4impl14SymbolProviderE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl14SymbolProviderE, @object
	.size	_ZTIN6icu_676number4impl14SymbolProviderE, 16
_ZTIN6icu_676number4impl14SymbolProviderE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl14SymbolProviderE
	.weak	_ZTVN6icu_676number4impl13TokenConsumerE
	.section	.data.rel.ro._ZTVN6icu_676number4impl13TokenConsumerE,"awG",@progbits,_ZTVN6icu_676number4impl13TokenConsumerE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl13TokenConsumerE, @object
	.size	_ZTVN6icu_676number4impl13TokenConsumerE, 40
_ZTVN6icu_676number4impl13TokenConsumerE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl13TokenConsumerE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_676number4impl14SymbolProviderE
	.section	.data.rel.ro._ZTVN6icu_676number4impl14SymbolProviderE,"awG",@progbits,_ZTVN6icu_676number4impl14SymbolProviderE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl14SymbolProviderE, @object
	.size	_ZTVN6icu_676number4impl14SymbolProviderE, 40
_ZTVN6icu_676number4impl14SymbolProviderE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl14SymbolProviderE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
