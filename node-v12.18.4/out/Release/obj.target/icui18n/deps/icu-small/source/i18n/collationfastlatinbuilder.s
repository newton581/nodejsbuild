	.file	"collationfastlatinbuilder.cpp"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode.constprop.0, @function
_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode.constprop.0:
.LFB4356:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	7192(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	7200(%rdi), %rax
	movq	%rdi, %rbx
	movl	%eax, %r8d
	addl	$1, %r8d
	js	.L2
	cmpl	7204(%rdi), %r8d
	jle	.L3
.L2:
	movq	%r12, %rdx
	movl	%r8d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L18
	movslq	7200(%rbx), %rax
	leal	1(%rax), %r8d
.L3:
	movq	7216(%rbx), %rdx
	movslq	%r13d, %rsi
	movq	%rsi, (%rdx,%rax,8)
	movl	%r8d, 7200(%rbx)
.L5:
	movl	%r8d, %esi
	addl	$1, %esi
	js	.L6
	cmpl	7204(%rbx), %esi
	jle	.L7
.L6:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L19
	movl	7200(%rbx), %r8d
	leal	1(%r8), %esi
.L7:
	movq	7216(%rbx), %rax
	movslq	%r8d, %r8
	movabsq	$4311744768, %rcx
	movq	%rcx, (%rax,%r8,8)
	movl	%esi, 7200(%rbx)
.L9:
	movl	%esi, %r8d
	addl	$1, %r8d
	js	.L10
	cmpl	7204(%rbx), %r8d
	jle	.L11
.L10:
	movq	%r12, %rdx
	movl	%r8d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L1
	movl	7200(%rbx), %esi
	leal	1(%rsi), %r8d
.L11:
	movq	7216(%rbx), %rax
	movslq	%esi, %rsi
	movq	$0, (%rax,%rsi,8)
	movl	%r8d, 7200(%rbx)
.L1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	7200(%rbx), %esi
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L18:
	movl	7200(%rbx), %r8d
	jmp	.L5
	.cfi_endproc
.LFE4356:
	.size	_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode.constprop.0, .-_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode.constprop.0
	.align 2
	.p2align 4
	.type	_ZNK6icu_6725CollationFastLatinBuilder12encodeTwoCEsEll.part.0, @function
_ZNK6icu_6725CollationFastLatinBuilder12encodeTwoCEsEll.part.0:
.LFB4353:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	7232(%rdi), %r8d
	movq	%rsi, %rbx
	movq	7248(%rdi), %r9
	andb	$63, %bh
	testl	%r8d, %r8d
	je	.L38
	movl	%r8d, %r11d
	xorl	%ecx, %ecx
.L22:
	leal	(%r11,%rcx), %r10d
	movl	%r10d, %eax
	shrl	$31, %eax
	addl	%r10d, %eax
	sarl	%eax
	movslq	%eax, %r10
	cmpq	%rbx, (%r9,%r10,8)
	ja	.L57
	jb	.L26
	addq	%r10, %r10
.L21:
	movq	7256(%rdi), %rdi
	movzwl	(%rdi,%r10), %eax
	cmpl	$1, %eax
	je	.L20
	cmpl	$4095, %eax
	jbe	.L29
	shrl	$11, %esi
	andl	$24, %esi
	addl	$8, %esi
	orl	%esi, %eax
.L29:
	testq	%rdx, %rdx
	jne	.L58
.L20:
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	cmpl	%eax, %ecx
	je	.L27
	movl	%eax, %ecx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L57:
	cmpl	%eax, %ecx
	je	.L55
	movl	%eax, %r11d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rdx, %r11
	andq	$-49153, %r11
	testl	%r8d, %r8d
	je	.L39
	xorl	%r10d, %r10d
.L31:
	leal	(%r8,%r10), %esi
	movl	%esi, %ecx
	shrl	$31, %ecx
	addl	%esi, %ecx
	sarl	%ecx
	movslq	%ecx, %rsi
	cmpq	%r11, (%r9,%rsi,8)
	ja	.L32
	jb	.L33
.L56:
	addq	%rsi, %rsi
.L30:
	movzwl	(%rdi,%rsi), %ecx
	movl	%ecx, %esi
	cmpw	$1, %cx
	je	.L41
	andl	$49152, %edx
	cmpl	$4095, %eax
	jbe	.L36
	movl	%eax, %edi
	andl	$992, %edi
	cmpl	$160, %edi
	jne	.L36
	movl	%ecx, %r8d
	andl	$992, %r8d
	andl	$7, %esi
	sete	%dil
	testl	%edx, %edx
	sete	%sil
	testb	%sil, %dil
	je	.L36
	cmpl	$383, %r8d
	jbe	.L36
	andl	$-993, %eax
	orl	%r8d, %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L36:
	leal	-993(%rcx), %esi
	cmpl	$3102, %esi
	jbe	.L37
	shrl	$11, %edx
	addl	$8, %edx
	orl	%edx, %ecx
.L37:
	sall	$16, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	orl	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	cmpl	%ecx, %r10d
	je	.L34
	movl	%ecx, %r10d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	cmpl	%ecx, %r10d
	je	.L59
	movl	%ecx, %r8d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$1, %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L27:
	addl	$1, %ecx
.L55:
	notl	%ecx
	movslq	%ecx, %rax
	leaq	(%rax,%rax), %r10
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L34:
	leal	1(%r10), %esi
	notl	%esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L38:
	movq	$-2, %r10
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L59:
	notl	%r10d
	movslq	%r10d, %rsi
	jmp	.L56
.L39:
	movq	$-2, %rsi
	jmp	.L30
	.cfi_endproc
.LFE4353:
	.size	_ZNK6icu_6725CollationFastLatinBuilder12encodeTwoCEsEll.part.0, .-_ZNK6icu_6725CollationFastLatinBuilder12encodeTwoCEsEll.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6725CollationFastLatinBuilder13encodeCharCEsER10UErrorCode.part.0, @function
_ZN6icu_6725CollationFastLatinBuilder13encodeCharCEsER10UErrorCode.part.0:
.LFB4354:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	7312(%rdi), %eax
	testw	%ax, %ax
	js	.L61
	movswl	%ax, %r13d
	sarl	$5, %r13d
.L62:
	leaq	7304(%r15), %rbx
	movl	$448, %r14d
	leaq	-58(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L63:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movw	%dx, -58(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	subl	$1, %r14d
	jne	.L63
	movswl	7312(%r15), %eax
	testw	%ax, %ax
	js	.L64
	sarl	$5, %eax
	movl	%eax, -68(%rbp)
.L65:
	leaq	7200(%r15), %rax
	movq	%r12, -80(%rbp)
	leaq	32(%r15), %r14
	movq	%rbx, %r12
	movq	%rax, %rbx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L68
	movabsq	$4311744768, %rax
	cmpq	%rax, %rsi
	je	.L75
	movq	(%r14), %rdx
	movq	%r15, %rdi
	call	_ZNK6icu_6725CollationFastLatinBuilder12encodeTwoCEsEll.part.0
	movl	%eax, %r10d
	movl	%eax, %edx
	cmpl	$65535, %eax
	jbe	.L68
	movswl	7312(%r15), %r11d
	testw	%r11w, %r11w
	js	.L70
	sarl	$5, %r11d
.L71:
	subl	-68(%rbp), %r11d
	movl	$1, %edx
	cmpl	$1023, %r11d
	jle	.L81
	.p2align 4,,10
	.p2align 3
.L68:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9setCharAtEiDs@PLT
.L67:
	addq	$16, %r14
	addl	$1, %r13d
	cmpq	%r14, %rbx
	je	.L82
.L72:
	movq	-8(%r14), %rsi
	movq	%rsi, %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	jne	.L66
	movabsq	$4311744768, %rax
	cmpq	%rax, %rsi
	jne	.L67
.L75:
	movl	$1, %edx
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L70:
	movl	7316(%r15), %r11d
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L81:
	movl	%r10d, %eax
	movq	-80(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	shrl	$16, %eax
	movq	%r12, %rdi
	movl	%r11d, -84(%rbp)
	movl	%r10d, -72(%rbp)
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-72(%rbp), %r10d
	movq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %ecx
	movw	%r10w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-84(%rbp), %r11d
	movzwl	%r11w, %edx
	orb	$8, %dh
	jmp	.L68
.L82:
	movq	-96(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	setle	%al
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L83
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L61:
	.cfi_restore_state
	movl	7316(%rdi), %r13d
	jmp	.L62
.L64:
	movl	7316(%r15), %eax
	movl	%eax, -68(%rbp)
	jmp	.L65
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4354:
	.size	_ZN6icu_6725CollationFastLatinBuilder13encodeCharCEsER10UErrorCode.part.0, .-_ZN6icu_6725CollationFastLatinBuilder13encodeCharCEsER10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6725CollationFastLatinBuilder18encodeContractionsER10UErrorCode.part.0, @function
_ZN6icu_6725CollationFastLatinBuilder18encodeContractionsER10UErrorCode.part.0:
.LFB4355:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movswl	7312(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	7368(%rdi), %eax
	addl	$448, %eax
	movl	%eax, -80(%rbp)
	testw	%r13w, %r13w
	js	.L85
	movswl	%r13w, %eax
	sarl	$5, %eax
	movl	%eax, -92(%rbp)
.L86:
	xorl	%ebx, %ebx
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L87:
	addq	$1, %rbx
	cmpq	$448, %rbx
	je	.L125
.L102:
	movq	%rbx, %rax
	movl	%ebx, -68(%rbp)
	salq	$4, %rax
	movq	24(%r15,%rax), %rax
	movq	%rax, %rdx
	sarq	$32, %rdx
	cmpl	$1, %edx
	jne	.L87
	movabsq	$4311744768, %rcx
	cmpq	%rcx, %rax
	je	.L87
	testw	%r13w, %r13w
	js	.L88
	sarl	$5, %r13d
.L89:
	subl	-80(%rbp), %r13d
	leaq	7304(%r15), %r14
	movl	%r13d, -76(%rbp)
	cmpl	$1023, %r13d
	jg	.L126
	movl	%eax, %r13d
	andl	$2147483647, %eax
	movq	%rbx, -88(%rbp)
	movl	$1, %edx
	leaq	16(,%rax,8), %r12
	andl	$2147483647, %r13d
	movq	%r12, %rbx
	.p2align 4,,10
	.p2align 3
.L101:
	movl	7200(%r15), %eax
	cmpl	%eax, %r13d
	jge	.L91
	movq	7216(%r15), %rcx
	movq	-16(%rcx,%rbx), %r12
	cmpl	$511, %r12d
	jne	.L94
	testb	%dl, %dl
	jne	.L94
	movzwl	-76(%rbp), %r13d
	movq	-88(%rbp), %rbx
	movq	%r14, %rdi
	movl	-68(%rbp), %esi
	addl	7368(%r15), %esi
	orw	$1024, %r13w
	addq	$1, %rbx
	movzwl	%r13w, %edx
	call	_ZN6icu_6713UnicodeString9setCharAtEiDs@PLT
	movswl	7312(%r15), %r13d
	cmpq	$448, %rbx
	jne	.L102
.L125:
	testw	%r13w, %r13w
	js	.L103
	movswl	%r13w, %eax
	sarl	$5, %eax
	cmpl	%eax, -92(%rbp)
	jl	.L127
.L105:
	andl	$1, %r13d
	movl	$1, %eax
	je	.L84
	movq	-104(%rbp), %rax
	movl	$7, (%rax)
	xorl	%eax, %eax
.L84:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L128
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	leal	1(%r13), %edx
	xorl	%r12d, %r12d
.L107:
	cmpl	%edx, %eax
	jle	.L111
	movq	7216(%r15), %rcx
	movslq	%edx, %rdx
	leal	2(%r13), %edi
	movq	(%rcx,%rdx,8), %rsi
	xorl	%edx, %edx
	cmpl	%edi, %eax
	jle	.L96
	movq	(%rcx,%rbx), %rdx
.L96:
	testq	%rsi, %rsi
	je	.L111
	movabsq	$4311744768, %rax
	cmpq	%rax, %rsi
	je	.L97
	movq	%r15, %rdi
	call	_ZNK6icu_6725CollationFastLatinBuilder12encodeTwoCEsEll.part.0
	cmpl	$1, %eax
	je	.L97
	movl	%eax, %r10d
	cmpl	$65535, %eax
	jbe	.L95
	orw	$1536, %r12w
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movw	%r12w, -58(%rbp)
	leaq	-58(%rbp), %r12
	movq	%r12, %rsi
	movl	%eax, -72(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-72(%rbp), %r8d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%r8d, %eax
	shrl	$16, %eax
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-72(%rbp), %r8d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	movw	%r8w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L99:
	addl	$3, %r13d
	addq	$24, %rbx
	xorl	%edx, %edx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L97:
	orw	$512, %r12w
	leaq	-58(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$1, %ecx
	movw	%r12w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L111:
	xorl	%r10d, %r10d
.L95:
	orw	$1024, %r12w
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$1, %ecx
	movw	%r12w, -58(%rbp)
	leaq	-58(%rbp), %r12
	movq	%r12, %rsi
	movl	%r10d, -72(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-72(%rbp), %r10d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movw	%r10w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L94:
	leal	1(%r13), %edx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L88:
	movl	7316(%r15), %r13d
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L126:
	movl	7368(%r15), %esi
	movl	$1, %edx
	movq	%r14, %rdi
	addl	%ebx, %esi
	call	_ZN6icu_6713UnicodeString9setCharAtEiDs@PLT
	movswl	7312(%r15), %r13d
	jmp	.L87
.L103:
	movl	7316(%r15), %eax
	cmpl	%eax, -92(%rbp)
	jge	.L105
.L127:
	movl	$511, %eax
	leaq	-58(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	leaq	7304(%r15), %rdi
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	7312(%r15), %r13d
	jmp	.L105
.L85:
	movl	7316(%rdi), %eax
	movl	%eax, -92(%rbp)
	jmp	.L86
.L128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4355:
	.size	_ZN6icu_6725CollationFastLatinBuilder18encodeContractionsER10UErrorCode.part.0, .-_ZN6icu_6725CollationFastLatinBuilder18encodeContractionsER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CollationFastLatinBuilderC2ER10UErrorCode
	.type	_ZN6icu_6725CollationFastLatinBuilderC2ER10UErrorCode, @function
_ZN6icu_6725CollationFastLatinBuilderC2ER10UErrorCode:
.LFB3273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725CollationFastLatinBuilderE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	7192(%rdi), %rdi
	movq	%rax, -7192(%rdi)
	movups	%xmm0, -7184(%rdi)
	call	_ZN6icu_679UVector64C1ER10UErrorCode@PLT
	leaq	7224(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_679UVector64C1ER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 7256(%rbx)
	movq	%rax, 7304(%rbx)
	movl	$2, %eax
	movb	$0, 7296(%rbx)
	movw	%ax, 7312(%rbx)
	movl	$0, 7368(%rbx)
	movups	%xmm0, 7280(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3273:
	.size	_ZN6icu_6725CollationFastLatinBuilderC2ER10UErrorCode, .-_ZN6icu_6725CollationFastLatinBuilderC2ER10UErrorCode
	.globl	_ZN6icu_6725CollationFastLatinBuilderC1ER10UErrorCode
	.set	_ZN6icu_6725CollationFastLatinBuilderC1ER10UErrorCode,_ZN6icu_6725CollationFastLatinBuilderC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CollationFastLatinBuilder10loadGroupsERKNS_13CollationDataER10UErrorCode
	.type	_ZN6icu_6725CollationFastLatinBuilder10loadGroupsERKNS_13CollationDataER10UErrorCode, @function
_ZN6icu_6725CollationFastLatinBuilder10loadGroupsERKNS_13CollationDataER10UErrorCode:
.LFB3280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L142
.L132:
	xorl	%eax, %eax
.L131:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L143
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	leaq	7304(%rdi), %r14
	movq	%rdi, %r12
	leaq	-58(%rbp), %r15
	xorl	%edx, %edx
	movl	$5, 7368(%rdi)
	movl	$517, %edi
	movq	%rsi, %r13
	movl	$1, %ecx
	movw	%di, -58(%rbp)
	movq	%r15, %rsi
	movq	%r14, %rdi
	xorl	%ebx, %ebx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L133:
	leal	4096(%rbx), %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713CollationData22getLastPrimaryForGroupEi@PLT
	movl	%eax, 7264(%r12,%rbx,4)
	testl	%eax, %eax
	je	.L132
	xorl	%esi, %esi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movw	%si, -58(%rbp)
	addq	$1, %rbx
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpq	$4, %rbx
	jne	.L133
	movl	$4100, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713CollationData23getFirstPrimaryForGroupEi@PLT
	movl	$25, %esi
	movq	%r13, %rdi
	movl	%eax, 7280(%r12)
	call	_ZNK6icu_6713CollationData23getFirstPrimaryForGroupEi@PLT
	movl	$25, %esi
	movq	%r13, %rdi
	movl	%eax, 7284(%r12)
	call	_ZNK6icu_6713CollationData22getLastPrimaryForGroupEi@PLT
	movl	7280(%r12), %ecx
	movl	%eax, 7288(%r12)
	testl	%ecx, %ecx
	je	.L132
	movl	7284(%r12), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L131
	jmp	.L132
.L143:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3280:
	.size	_ZN6icu_6725CollationFastLatinBuilder10loadGroupsERKNS_13CollationDataER10UErrorCode, .-_ZN6icu_6725CollationFastLatinBuilder10loadGroupsERKNS_13CollationDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725CollationFastLatinBuilder11inSameGroupEjj
	.type	_ZNK6icu_6725CollationFastLatinBuilder11inSameGroupEjj, @function
_ZNK6icu_6725CollationFastLatinBuilder11inSameGroupEjj:
.LFB3281:
	.cfi_startproc
	endbr64
	movl	7292(%rdi), %eax
	cmpl	%edx, %eax
	setbe	%r8b
	cmpl	%esi, %eax
	jbe	.L144
	xorl	%r8d, %r8d
	cmpl	%edx, %eax
	jbe	.L144
	movl	7276(%rdi), %eax
	cmpl	%eax, %esi
	ja	.L159
	cmpl	%eax, %edx
	ja	.L144
	movl	7264(%rdi), %ecx
	cmpl	%ecx, %esi
	jbe	.L158
	cmpl	%ecx, %edx
	jbe	.L144
	movl	7268(%rdi), %ecx
	cmpl	%ecx, %esi
	jbe	.L158
	cmpl	%ecx, %edx
	jbe	.L144
	movl	7272(%rdi), %ecx
	cmpl	%esi, %ecx
	jnb	.L158
	cmpl	%edx, %ecx
	jnb	.L160
.L150:
	cmpl	%eax, %edx
	setbe	%r8b
.L144:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	cmpl	%eax, %edx
	seta	%r8b
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	movl	%ecx, %eax
	jmp	.L150
.L160:
	xorl	%r8d, %r8d
	jmp	.L144
	.cfi_endproc
.LFE3281:
	.size	_ZNK6icu_6725CollationFastLatinBuilder11inSameGroupEjj, .-_ZNK6icu_6725CollationFastLatinBuilder11inSameGroupEjj
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CollationFastLatinBuilder8resetCEsEv
	.type	_ZN6icu_6725CollationFastLatinBuilder8resetCEsEv, @function
_ZN6icu_6725CollationFastLatinBuilder8resetCEsEv:
.LFB3282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$7192, %rdi
	subq	$8, %rsp
	call	_ZN6icu_679UVector6417removeAllElementsEv@PLT
	leaq	7224(%rbx), %rdi
	call	_ZN6icu_679UVector6417removeAllElementsEv@PLT
	movl	7368(%rbx), %edx
	movb	$0, 7296(%rbx)
	movzwl	7312(%rbx), %eax
	testl	%edx, %edx
	jne	.L162
	testb	$1, %al
	jne	.L174
.L162:
	testw	%ax, %ax
	js	.L163
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L164:
	cmpl	%edx, %ecx
	jbe	.L161
	cmpl	$1023, %edx
	jg	.L166
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, 7312(%rbx)
.L161:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movl	7316(%rbx), %ecx
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L166:
	orl	$-32, %eax
	movl	%edx, 7316(%rbx)
	movw	%ax, 7312(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	7304(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	.cfi_endproc
.LFE3282:
	.size	_ZN6icu_6725CollationFastLatinBuilder8resetCEsEv, .-_ZN6icu_6725CollationFastLatinBuilder8resetCEsEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode
	.type	_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode, @function
_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode:
.LFB3286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movslq	%esi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$7192, %rdi
	subq	$24, %rsp
	movslq	7200(%rbx), %rax
	movl	%eax, %r9d
	addl	$1, %r9d
	js	.L176
	cmpl	7204(%rbx), %r9d
	jle	.L177
.L176:
	movq	%r14, %rdx
	movl	%r9d, %esi
	movq	%rdi, -56(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-56(%rbp), %rdi
	testb	%al, %al
	je	.L217
	movslq	7200(%rbx), %rax
	leal	1(%rax), %r9d
.L177:
	movq	7216(%rbx), %rdx
	movq	%r15, (%rdx,%rax,8)
	movl	%r9d, 7200(%rbx)
.L179:
	movl	%r9d, %esi
	addl	$1, %esi
	js	.L180
	cmpl	7204(%rbx), %esi
	jle	.L181
.L180:
	movq	%r14, %rdx
	movq	%rdi, -56(%rbp)
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	movq	-56(%rbp), %rdi
	testb	%al, %al
	je	.L218
	movl	7200(%rbx), %r9d
	leal	1(%r9), %esi
.L181:
	movq	7216(%rbx), %rax
	movslq	%r9d, %r9
	movq	%r13, (%rax,%r9,8)
	movl	%esi, 7200(%rbx)
.L183:
	movl	%esi, %r8d
	addl	$1, %r8d
	js	.L184
	cmpl	7204(%rbx), %r8d
	jle	.L185
.L184:
	movq	%r14, %rdx
	movl	%r8d, %esi
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L219
.L186:
	movl	(%r14), %edx
	testl	%edx, %edx
	setg	%al
	testq	%r13, %r13
	sete	%dl
	orb	%al, %dl
	je	.L220
.L187:
	testq	%r12, %r12
	je	.L175
	testb	%al, %al
	jne	.L175
	movq	%r12, %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	je	.L175
	movl	7232(%rbx), %r8d
	movq	7248(%rbx), %rsi
	andq	$-49153, %r12
	testl	%r8d, %r8d
	je	.L196
	xorl	%ecx, %ecx
.L197:
	leal	(%r8,%rcx), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rdx
	movl	%eax, %edi
	cmpq	%r12, (%rsi,%rdx,8)
	ja	.L198
	jb	.L221
.L199:
	testl	%edi, %edi
	js	.L222
.L175:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movl	7200(%rbx), %esi
	leal	1(%rsi), %r8d
.L185:
	movq	7216(%rbx), %rax
	movslq	%esi, %rsi
	movq	%r12, (%rax,%rsi,8)
	movl	%r8d, 7200(%rbx)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L218:
	movl	7200(%rbx), %esi
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L217:
	movl	7200(%rbx), %r9d
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L220:
	movq	%r13, %rdx
	xorl	%eax, %eax
	sarq	$32, %rdx
	cmpl	$1, %edx
	je	.L187
	movl	7232(%rbx), %r8d
	movq	7248(%rbx), %rdi
	andq	$-49153, %r13
	testl	%r8d, %r8d
	je	.L188
	xorl	%ecx, %ecx
.L189:
	leal	(%r8,%rcx), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rdx
	movl	%eax, %esi
	cmpq	%r13, (%rdi,%rdx,8)
	ja	.L223
	jb	.L224
.L193:
	xorl	%eax, %eax
	testl	%esi, %esi
	jns	.L187
	notl	%esi
	movl	%esi, %r8d
.L188:
	leaq	7224(%rbx), %rdi
	movq	%r14, %rcx
	movl	%r8d, %edx
	movq	%r13, %rsi
	call	_ZN6icu_679UVector6415insertElementAtEliR10UErrorCode@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	setg	%al
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L221:
	cmpl	%eax, %ecx
	je	.L200
	movl	%eax, %ecx
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L224:
	cmpl	%eax, %ecx
	je	.L194
	movl	%eax, %ecx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L198:
	cmpl	%eax, %ecx
	je	.L216
	movl	%eax, %r8d
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L223:
	cmpl	%eax, %ecx
	je	.L215
	movl	%eax, %r8d
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L222:
	notl	%edi
	movl	%edi, %r8d
.L196:
	addq	$24, %rsp
	movq	%r14, %rcx
	movq	%r12, %rsi
	movl	%r8d, %edx
	leaq	7224(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679UVector6415insertElementAtEliR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	addl	$1, %esi
.L215:
	notl	%esi
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L200:
	addl	$1, %edi
.L216:
	notl	%edi
	jmp	.L199
	.cfi_endproc
.LFE3286:
	.size	_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode, .-_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6725CollationFastLatinBuilder25getCEsFromContractionCE32ERKNS_13CollationDataEjR10UErrorCode.part.0, @function
_ZN6icu_6725CollationFastLatinBuilder25getCEsFromContractionCE32ERKNS_13CollationDataEjR10UErrorCode.part.0:
.LFB4349:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrl	$13, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$184, %rsp
	movq	%rsi, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	leaq	(%rax,%rdx,2), %r13
	movl	7200(%rdi), %eax
	movl	(%rcx), %edi
	movl	%eax, -200(%rbp)
	testl	%edi, %edi
	jle	.L262
.L226:
	movq	%rbx, %rdx
	movl	$511, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode.constprop.0
.L227:
	leaq	-176(%rbp), %r15
	addq	$4, %r13
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	-184(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r13, -184(%rbp)
	call	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode@PLT
	leaq	-126(%rbp), %rax
	movb	$0, -193(%rbp)
	movl	$-1, %r13d
	movq	%rax, -208(%rbp)
	.p2align 4,,10
	.p2align 3
.L228:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	je	.L229
.L265:
	movswl	-128(%rbp), %eax
	testw	%ax, %ax
	js	.L230
	movswl	%ax, %edx
	sarl	$5, %edx
.L231:
	testl	%edx, %edx
	je	.L228
	movq	-208(%rbp), %rdx
	testb	$2, %al
	cmove	-112(%rbp), %rdx
	movzwl	(%rdx), %r14d
	movl	%r14d, %edx
	cmpw	$383, %r14w
	jbe	.L234
	subw	$8192, %dx
	cmpw	$63, %dx
	ja	.L228
	subl	$7808, %r14d
.L234:
	cmpl	%r14d, %r13d
	je	.L263
	cmpb	$0, -193(%rbp)
	jne	.L264
.L236:
	testw	%ax, %ax
	js	.L237
	sarl	$5, %eax
	cmpl	$1, %eax
	je	.L239
.L240:
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movl	%r14d, %r13d
	call	_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode.constprop.0
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movb	$0, -193(%rbp)
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	jne	.L265
.L229:
	cmpb	$0, -193(%rbp)
	jne	.L266
.L241:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L242
	movq	$0, 16(%r12)
	movslq	-200(%rbp), %rax
	movabsq	$6442450944, %rdx
	orq	%rdx, %rax
	movq	%rax, 8(%r12)
	movl	$1, %eax
.L242:
	movq	%r15, %rdi
	movb	%al, -193(%rbp)
	call	_ZN6icu_6710UCharsTrie8IteratorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movzbl	-193(%rbp), %eax
	jne	.L267
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movl	-124(%rbp), %edx
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L237:
	movl	-124(%rbp), %eax
	cmpl	$1, %eax
	jne	.L240
.L239:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L240
	movl	-68(%rbp), %ecx
	movq	-216(%rbp), %rsi
	movq	%rbx, %r8
	movq	%r12, %rdi
	movl	$-1, %edx
	movl	%r14d, %r13d
	call	_ZN6icu_6725CollationFastLatinBuilder14getCEsFromCE32ERKNS_13CollationDataEijR10UErrorCode.part.0
	movb	$1, -193(%rbp)
	testb	%al, %al
	jne	.L228
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L264:
	movq	16(%r12), %rcx
	movq	8(%r12), %rdx
	movq	%rbx, %r8
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode
	movswl	-128(%rbp), %eax
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L263:
	cmpb	$0, -193(%rbp)
	je	.L228
	movq	%rbx, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode.constprop.0
	movb	$0, -193(%rbp)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L262:
	movzwl	0(%r13), %ecx
	movzwl	2(%r13), %eax
	movq	%rbx, %r8
	movq	%r12, %rdi
	movl	$-1, %edx
	sall	$16, %ecx
	orl	%eax, %ecx
	call	_ZN6icu_6725CollationFastLatinBuilder14getCEsFromCE32ERKNS_13CollationDataEijR10UErrorCode.part.0
	testb	%al, %al
	je	.L226
	movq	16(%r12), %rcx
	movq	8(%r12), %rdx
	movq	%rbx, %r8
	movq	%r12, %rdi
	movl	$511, %esi
	call	_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L266:
	movq	16(%r12), %rcx
	movq	8(%r12), %rdx
	movq	%rbx, %r8
	movl	%r13d, %esi
	movq	%r12, %rdi
	movb	%al, -193(%rbp)
	call	_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode
	movzbl	-193(%rbp), %eax
	jmp	.L241
.L267:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4349:
	.size	_ZN6icu_6725CollationFastLatinBuilder25getCEsFromContractionCE32ERKNS_13CollationDataEjR10UErrorCode.part.0, .-_ZN6icu_6725CollationFastLatinBuilder25getCEsFromContractionCE32ERKNS_13CollationDataEjR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CollationFastLatinBuilder25getCEsFromContractionCE32ERKNS_13CollationDataEjR10UErrorCode
	.type	_ZN6icu_6725CollationFastLatinBuilder25getCEsFromContractionCE32ERKNS_13CollationDataEjR10UErrorCode, @function
_ZN6icu_6725CollationFastLatinBuilder25getCEsFromContractionCE32ERKNS_13CollationDataEjR10UErrorCode:
.LFB3285:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L270
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	jmp	_ZN6icu_6725CollationFastLatinBuilder25getCEsFromContractionCE32ERKNS_13CollationDataEjR10UErrorCode.part.0
	.cfi_endproc
.LFE3285:
	.size	_ZN6icu_6725CollationFastLatinBuilder25getCEsFromContractionCE32ERKNS_13CollationDataEjR10UErrorCode, .-_ZN6icu_6725CollationFastLatinBuilder25getCEsFromContractionCE32ERKNS_13CollationDataEjR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6725CollationFastLatinBuilder14getCEsFromCE32ERKNS_13CollationDataEijR10UErrorCode.part.0, @function
_ZN6icu_6725CollationFastLatinBuilder14getCEsFromCE32ERKNS_13CollationDataEijR10UErrorCode.part.0:
.LFB4350:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	%ecx, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r15, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	call	_ZNK6icu_6713CollationData12getFinalCE32Ej@PLT
	movq	$0, 16(%r14)
	movzbl	%al, %esi
	cmpb	$-65, %al
	ja	.L324
	movabsq	$-281474976710656, %rdx
	movq	%rax, %rcx
	sall	$16, %eax
	salq	$32, %rcx
	andl	$-16777216, %eax
	sall	$8, %esi
	andq	%rdx, %rcx
	orq	%rax, %rcx
	orq	%rsi, %rcx
.L281:
	movq	%rcx, 8(%r14)
	xorl	%eax, %eax
.L282:
	testq	%rax, %rax
	sete	%al
	testq	%rcx, %rcx
	je	.L271
.L288:
	movq	%rcx, %rax
	sarq	$32, %rax
	testl	%eax, %eax
	je	.L302
	cmpl	7288(%r14), %eax
	ja	.L302
	movl	7292(%r14), %esi
	movl	%ecx, %edx
	cmpl	%esi, %eax
	jb	.L325
.L292:
	andl	$16191, %edx
	cmpl	$1279, %edx
	jbe	.L302
	movq	16(%r14), %rdx
	testq	%rdx, %rdx
	je	.L293
	movq	%rdx, %rdi
	sarq	$32, %rdi
	testl	%edi, %edi
	je	.L326
	cmpl	%esi, %eax
	jnb	.L327
	cmpl	%edi, %esi
	jbe	.L302
	movl	7276(%r14), %r8d
	cmpl	%r8d, %eax
	ja	.L328
	cmpl	%r8d, %edi
	ja	.L302
	movl	7264(%r14), %r9d
	cmpl	%r9d, %eax
	jbe	.L306
	cmpl	%r9d, %edi
	jbe	.L302
	movl	7268(%r14), %r9d
	cmpl	%r9d, %eax
	jbe	.L306
	cmpl	%r9d, %edi
	jbe	.L302
	movl	7272(%r14), %r9d
	cmpl	%r9d, %eax
	jbe	.L306
	cmpl	%r9d, %edi
	jbe	.L302
.L300:
	cmpl	%r8d, %edi
	seta	%al
	.p2align 4,,10
	.p2align 3
.L295:
	testb	%al, %al
	jne	.L302
	movl	%edx, %eax
	shrl	$16, %eax
	je	.L302
	testl	%edi, %edi
	je	.L301
	cmpl	%edi, %esi
	ja	.L329
.L301:
	movl	%edx, %eax
	andl	$16191, %eax
	cmpl	$1279, %eax
	jbe	.L302
.L293:
	orq	%rdx, %rcx
	andl	$192, %ecx
	sete	%al
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L325:
	movl	%ecx, %edi
	andl	$-16384, %edi
	cmpl	$83886080, %edi
	je	.L292
.L302:
	xorl	%eax, %eax
.L271:
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	movl	%eax, %edx
	andl	$15, %edx
	leal	-1(%rdx), %ecx
	cmpl	$1, %ecx
	ja	.L330
	movl	%eax, %ecx
	subl	%esi, %ecx
	movq	%rcx, %rax
	salq	$32, %rax
	orq	$83887360, %rax
	cmpl	$1, %edx
	cmove	%rax, %rcx
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L330:
	subl	$4, %edx
	cmpl	$10, %edx
	ja	.L302
	leaq	.L276(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L276:
	.long	.L280-.L276
	.long	.L279-.L276
	.long	.L278-.L276
	.long	.L302-.L276
	.long	.L302-.L276
	.long	.L277-.L276
	.long	.L302-.L276
	.long	.L302-.L276
	.long	.L302-.L276
	.long	.L302-.L276
	.long	.L275-.L276
	.text
.L275:
	movq	16(%r15), %rdx
	shrl	$13, %eax
	movl	%r13d, %edi
	movq	(%rdx,%rax,8), %rsi
	call	_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil@PLT
	movq	%rax, %rcx
	salq	$32, %rcx
	orq	$83887360, %rcx
	movq	%rcx, 8(%r14)
	jmp	.L288
.L280:
	movabsq	$-72057594037927936, %rdx
	movq	%rax, %rcx
	salq	$32, %rcx
	andq	%rdx, %rcx
	movl	%eax, %edx
	sall	$16, %eax
	shrl	$8, %edx
	andl	$-16777216, %eax
	andl	$65280, %edx
	orl	$1280, %eax
	orq	%rdx, %rcx
	movq	%rax, %xmm1
	orq	$83886080, %rcx
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r14)
	jmp	.L288
.L279:
	movl	%eax, %esi
	shrl	$8, %esi
	andl	$31, %esi
	cmpl	$2, %esi
	jg	.L302
	movq	8(%r15), %rdx
	shrl	$13, %eax
	leaq	(%rdx,%rax,4), %rdi
	movl	(%rdi), %eax
	movzbl	%al, %edx
	cmpb	$-65, %al
	jbe	.L331
	movl	%eax, %ecx
	andl	$15, %eax
	subl	%edx, %ecx
	cmpl	$1, %eax
	je	.L332
.L286:
	movq	%rcx, 8(%r14)
	xorl	%eax, %eax
	cmpl	$2, %esi
	jne	.L282
.L287:
	movl	4(%rdi), %edx
	movzbl	%dl, %esi
	cmpb	$-65, %dl
	jbe	.L333
	movl	%edx, %eax
	andl	$15, %edx
	subl	%esi, %eax
	movq	%rax, %rsi
	salq	$32, %rsi
	orq	$83887360, %rsi
	cmpl	$1, %edx
	cmove	%rsi, %rax
.L323:
	movq	%rax, 16(%r14)
	jmp	.L282
.L278:
	movl	%eax, %esi
	shrl	$8, %esi
	andl	$31, %esi
	cmpl	$2, %esi
	jg	.L302
	movq	16(%r15), %rdx
	shrl	$13, %eax
	leaq	(%rdx,%rax,8), %rdx
	xorl	%eax, %eax
	movq	(%rdx), %rcx
	movq	%rcx, 8(%r14)
	cmpl	$2, %esi
	jne	.L282
	movq	8(%rdx), %rax
	jmp	.L323
.L277:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L302
	movq	%r12, %rcx
	movq	%r15, %rsi
	popq	%r12
	movq	%r14, %rdi
	popq	%r13
	movl	%eax, %edx
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6725CollationFastLatinBuilder25getCEsFromContractionCE32ERKNS_13CollationDataEjR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	cmpl	%esi, %eax
	setb	%al
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L327:
	cmpl	%edi, %esi
	seta	%al
	jmp	.L295
.L331:
	movabsq	$-281474976710656, %r8
	movq	%rax, %rcx
	sall	$16, %eax
	salq	$32, %rcx
	andl	$-16777216, %eax
	sall	$8, %edx
	andq	%r8, %rcx
	orq	%rcx, %rax
	orq	%rdx, %rax
	movq	%rax, %rcx
	jmp	.L286
.L329:
	movl	%edx, %eax
	andl	$-16384, %eax
	cmpl	$83886080, %eax
	jne	.L302
	jmp	.L301
.L332:
	salq	$32, %rcx
	orq	$83887360, %rcx
	movq	%rcx, 8(%r14)
	cmpl	$2, %esi
	jne	.L288
	jmp	.L287
.L328:
	cmpl	%r8d, %edi
	setbe	%al
	jmp	.L295
.L333:
	movabsq	$-281474976710656, %rdi
	movq	%rdx, %rax
	sall	$16, %edx
	salq	$32, %rax
	andl	$-16777216, %edx
	sall	$8, %esi
	andq	%rdi, %rax
	orq	%rdx, %rax
	movl	%esi, %edx
	orq	%rdx, %rax
	jmp	.L323
.L306:
	movl	%r9d, %r8d
	jmp	.L300
	.cfi_endproc
.LFE4350:
	.size	_ZN6icu_6725CollationFastLatinBuilder14getCEsFromCE32ERKNS_13CollationDataEijR10UErrorCode.part.0, .-_ZN6icu_6725CollationFastLatinBuilder14getCEsFromCE32ERKNS_13CollationDataEijR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CollationFastLatinBuilder14getCEsFromCE32ERKNS_13CollationDataEijR10UErrorCode
	.type	_ZN6icu_6725CollationFastLatinBuilder14getCEsFromCE32ERKNS_13CollationDataEijR10UErrorCode, @function
_ZN6icu_6725CollationFastLatinBuilder14getCEsFromCE32ERKNS_13CollationDataEijR10UErrorCode:
.LFB3284:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jle	.L335
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	%ecx, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	call	_ZNK6icu_6713CollationData12getFinalCE32Ej@PLT
	movq	$0, 16(%r12)
	movzbl	%al, %edi
	cmpb	$-65, %al
	jbe	.L337
	movl	%eax, %esi
	andl	$15, %esi
	leal	-1(%rsi), %edx
	cmpl	$1, %edx
	ja	.L389
	movl	%eax, %ecx
	subl	%edi, %ecx
	movq	%rcx, %rax
	salq	$32, %rax
	orq	$83887360, %rax
	cmpl	$1, %esi
	cmove	%rax, %rcx
.L345:
	movq	%rcx, 8(%r12)
	xorl	%eax, %eax
.L346:
	testq	%rax, %rax
	sete	%al
	testq	%rcx, %rcx
	je	.L334
.L352:
	movq	%rcx, %rax
	sarq	$32, %rax
	testl	%eax, %eax
	je	.L365
	cmpl	7288(%r12), %eax
	ja	.L365
	movl	7292(%r12), %esi
	movl	%ecx, %edx
	cmpl	%esi, %eax
	jnb	.L355
	movl	%ecx, %edi
	andl	$-16384, %edi
	cmpl	$83886080, %edi
	je	.L355
.L365:
	xorl	%eax, %eax
.L334:
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movabsq	$-281474976710656, %rdx
	movq	%rax, %rcx
	sall	$16, %eax
	salq	$32, %rcx
	andl	$-16777216, %eax
	sall	$8, %edi
	andq	%rdx, %rcx
	orq	%rax, %rcx
	orq	%rdi, %rcx
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L355:
	andl	$16191, %edx
	cmpl	$1279, %edx
	jbe	.L365
	movq	16(%r12), %rdx
	testq	%rdx, %rdx
	je	.L356
	movq	%rdx, %rdi
	sarq	$32, %rdi
	testl	%edi, %edi
	jne	.L357
	cmpl	%esi, %eax
	setb	%al
.L358:
	testb	%al, %al
	jne	.L365
	movl	%edx, %eax
	shrl	$16, %eax
	je	.L365
	testl	%edi, %edi
	je	.L364
	cmpl	%edi, %esi
	ja	.L390
.L364:
	movl	%edx, %eax
	andl	$16191, %eax
	cmpl	$1279, %eax
	jbe	.L365
.L356:
	orq	%rdx, %rcx
	andl	$192, %ecx
	sete	%al
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L389:
	subl	$4, %esi
	cmpl	$10, %esi
	ja	.L365
	leaq	.L340(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rcx
	addq	%rdx, %rcx
	notrack jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L340:
	.long	.L344-.L340
	.long	.L343-.L340
	.long	.L342-.L340
	.long	.L365-.L340
	.long	.L365-.L340
	.long	.L341-.L340
	.long	.L365-.L340
	.long	.L365-.L340
	.long	.L365-.L340
	.long	.L365-.L340
	.long	.L339-.L340
	.text
.L339:
	movq	16(%r14), %rdx
	shrl	$13, %eax
	movl	%r15d, %edi
	movq	(%rdx,%rax,8), %rsi
	call	_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil@PLT
	movq	%rax, %rcx
	salq	$32, %rcx
	orq	$83887360, %rcx
	movq	%rcx, 8(%r12)
	jmp	.L352
.L341:
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L365
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6725CollationFastLatinBuilder25getCEsFromContractionCE32ERKNS_13CollationDataEjR10UErrorCode.part.0
.L342:
	.cfi_restore_state
	movl	%eax, %esi
	shrl	$8, %esi
	andl	$31, %esi
	cmpl	$2, %esi
	jg	.L365
	movq	16(%r14), %rdx
	shrl	$13, %eax
	leaq	(%rdx,%rax,8), %rdx
	xorl	%eax, %eax
	movq	(%rdx), %rcx
	movq	%rcx, 8(%r12)
	cmpl	$2, %esi
	jne	.L346
	movq	8(%rdx), %rax
.L388:
	movq	%rax, 16(%r12)
	jmp	.L346
.L343:
	movl	%eax, %esi
	shrl	$8, %esi
	andl	$31, %esi
	cmpl	$2, %esi
	jg	.L365
	movq	8(%r14), %rdx
	shrl	$13, %eax
	leaq	(%rdx,%rax,4), %rdi
	movl	(%rdi), %eax
	movzbl	%al, %edx
	cmpb	$-65, %al
	jbe	.L391
	movl	%eax, %ecx
	andl	$15, %eax
	subl	%edx, %ecx
	cmpl	$1, %eax
	je	.L392
.L350:
	movq	%rcx, 8(%r12)
	xorl	%eax, %eax
	cmpl	$2, %esi
	jne	.L346
.L351:
	movl	4(%rdi), %edx
	movzbl	%dl, %esi
	cmpb	$-65, %dl
	jbe	.L393
	movl	%edx, %eax
	andl	$15, %edx
	subl	%esi, %eax
	movq	%rax, %rsi
	salq	$32, %rsi
	orq	$83887360, %rsi
	cmpl	$1, %edx
	cmove	%rsi, %rax
	jmp	.L388
.L344:
	movabsq	$-72057594037927936, %rdx
	movq	%rax, %rcx
	salq	$32, %rcx
	andq	%rdx, %rcx
	movl	%eax, %edx
	sall	$16, %eax
	shrl	$8, %edx
	andl	$-16777216, %eax
	andl	$65280, %edx
	orl	$1280, %eax
	orq	%rdx, %rcx
	movq	%rax, %xmm1
	orq	$83886080, %rcx
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L357:
	cmpl	%esi, %eax
	jnb	.L394
	cmpl	%edi, %esi
	jbe	.L365
	movl	7276(%r12), %r8d
	cmpl	%r8d, %eax
	ja	.L395
	cmpl	%r8d, %edi
	ja	.L365
	movl	7264(%r12), %r9d
	cmpl	%r9d, %eax
	jbe	.L369
	cmpl	%r9d, %edi
	jbe	.L365
	movl	7268(%r12), %r9d
	cmpl	%r9d, %eax
	jbe	.L369
	cmpl	%r9d, %edi
	jbe	.L365
	movl	7272(%r12), %r9d
	cmpl	%r9d, %eax
	jbe	.L369
	cmpl	%r9d, %edi
	jbe	.L365
.L363:
	cmpl	%r8d, %edi
	seta	%al
	jmp	.L358
.L394:
	cmpl	%edi, %esi
	seta	%al
	jmp	.L358
.L391:
	movabsq	$-281474976710656, %r8
	movq	%rax, %rcx
	sall	$16, %eax
	salq	$32, %rcx
	andl	$-16777216, %eax
	sall	$8, %edx
	andq	%r8, %rcx
	orq	%rcx, %rax
	orq	%rdx, %rax
	movq	%rax, %rcx
	jmp	.L350
.L390:
	movl	%edx, %eax
	andl	$-16384, %eax
	cmpl	$83886080, %eax
	jne	.L365
	jmp	.L364
.L392:
	salq	$32, %rcx
	orq	$83887360, %rcx
	movq	%rcx, 8(%r12)
	cmpl	$2, %esi
	jne	.L352
	jmp	.L351
.L395:
	cmpl	%r8d, %edi
	setbe	%al
	jmp	.L358
.L393:
	movabsq	$-281474976710656, %rdi
	movq	%rdx, %rax
	sall	$16, %edx
	salq	$32, %rax
	andl	$-16777216, %edx
	andq	%rdi, %rax
	orq	%rdx, %rax
	movl	%esi, %edx
	sall	$8, %edx
	orq	%rdx, %rax
	jmp	.L388
.L369:
	movl	%r9d, %r8d
	jmp	.L363
	.cfi_endproc
.LFE3284:
	.size	_ZN6icu_6725CollationFastLatinBuilder14getCEsFromCE32ERKNS_13CollationDataEijR10UErrorCode, .-_ZN6icu_6725CollationFastLatinBuilder14getCEsFromCE32ERKNS_13CollationDataEijR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6725CollationFastLatinBuilder6getCEsERKNS_13CollationDataER10UErrorCode.part.0, @function
_ZN6icu_6725CollationFastLatinBuilder6getCEsERKNS_13CollationDataER10UErrorCode.part.0:
.LFB4351:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	24(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movdqa	.LC0(%rip), %xmm0
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L422:
	addq	$16, %r13
.L425:
	cmpw	$384, %bx
	je	.L397
	cmpw	$8256, %bx
	je	.L398
	movq	(%r15), %rax
	movzwl	%bx, %edx
	movl	%ebx, %r9d
	leal	1(%rbx), %edi
	movl	%edx, %ecx
	andl	$31, %r9d
	movq	16(%rax), %rsi
	sarl	$5, %ecx
	cmpw	$-10241, %bx
	jbe	.L459
	leal	320(%rcx), %r10d
	cmpl	$56320, %edx
	movq	(%rax), %rax
	cmovl	%r10d, %ecx
	movslq	%ecx, %r10
	movzwl	(%rax,%r10,2), %eax
	leal	(%r9,%rax,4), %eax
	cltq
	movl	(%rsi,%rax,4), %ecx
	cmpl	$192, %ecx
	jne	.L460
	movq	32(%r15), %rsi
	movl	%ebx, %r14d
	movl	%edi, %ebx
	movq	(%rsi), %rax
	movq	16(%rax), %rcx
	movq	(%rax), %rax
	movzwl	(%rax,%r10,2), %eax
	leal	(%r9,%rax,4), %eax
	cltq
	salq	$2, %rax
.L429:
	movl	(%rcx,%rax), %ecx
.L401:
	movl	(%r8), %edi
	testl	%edi, %edi
	jle	.L432
.L405:
	movups	%xmm0, 8(%r12)
	movups	%xmm0, 0(%r13)
	testw	%r14w, %r14w
	jne	.L422
	movabsq	$4311744768, %rdx
	xorl	%ecx, %ecx
.L421:
	movl	$511, %esi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6725CollationFastLatinBuilder19addContractionEntryEillR10UErrorCode
	movdqa	.LC1(%rip), %xmm1
	movq	-56(%rbp), %r8
	movdqa	.LC0(%rip), %xmm0
	movups	%xmm1, 24(%r12)
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L463:
	movl	%ebx, %r14d
	movq	%r15, %rsi
	movl	%edi, %ebx
	.p2align 4,,10
	.p2align 3
.L432:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6725CollationFastLatinBuilder14getCEsFromCE32ERKNS_13CollationDataEijR10UErrorCode.part.0
	movq	-56(%rbp), %r8
	movdqa	.LC0(%rip), %xmm0
	testb	%al, %al
	je	.L405
	movl	(%r8), %ecx
	movq	8(%r12), %rdx
	movdqu	8(%r12), %xmm2
	movq	16(%r12), %rsi
	testl	%ecx, %ecx
	setg	%al
	testq	%rdx, %rdx
	movups	%xmm2, 0(%r13)
	sete	%cl
	orb	%al, %cl
	je	.L461
.L406:
	testq	%rsi, %rsi
	je	.L414
	testb	%al, %al
	jne	.L414
	movq	%rsi, %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	je	.L414
	movl	7232(%r12), %r9d
	movq	7248(%r12), %rdi
	andq	$-49153, %rsi
	testl	%r9d, %r9d
	je	.L415
	xorl	%ecx, %ecx
.L416:
	leal	(%r9,%rcx), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rdx
	movl	%eax, %r10d
	cmpq	%rsi, (%rdi,%rdx,8)
	ja	.L417
	jb	.L462
.L418:
	testl	%r10d, %r10d
	jns	.L414
	notl	%r10d
	movl	%r10d, %r9d
.L415:
	movq	%r8, %rcx
	leaq	7224(%r12), %rdi
	movl	%r9d, %edx
	movq	%r8, -56(%rbp)
	call	_ZN6icu_679UVector6415insertElementAtEliR10UErrorCode@PLT
	movq	-56(%rbp), %r8
	movdqa	.LC0(%rip), %xmm0
	.p2align 4,,10
	.p2align 3
.L414:
	testw	%r14w, %r14w
	jne	.L422
	movq	8(%r12), %rdx
	movq	%rdx, %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	jne	.L437
	movabsq	$4311744768, %rax
	cmpq	%rax, %rdx
	jne	.L422
.L437:
	movq	16(%r12), %rcx
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L397:
	movq	(%r15), %rax
	movl	$8193, %ebx
	xorl	%r9d, %r9d
	movl	$8192, %edx
	movl	$512, %r10d
	movl	$8192, %r14d
	movq	16(%rax), %rsi
.L430:
	movq	(%rax), %rax
	movzwl	(%rax,%r10), %eax
	leal	(%r9,%rax,4), %eax
	cltq
	movl	(%rsi,%rax,4), %ecx
	movq	%r15, %rsi
	cmpl	$192, %ecx
	jne	.L401
	movq	32(%r15), %rsi
	movq	(%rsi), %rax
	movq	16(%rax), %rcx
	movq	(%rax), %rax
	movzwl	(%rax,%r10), %eax
	leal	(%r9,%rax,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L459:
	movslq	%ecx, %rcx
	movl	%ebx, %r14d
	movl	%edi, %ebx
	leaq	(%rcx,%rcx), %r10
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L460:
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L463
	movdqa	.LC0(%rip), %xmm3
	movl	%edi, %ebx
	movups	%xmm3, 8(%r12)
	movups	%xmm3, 0(%r13)
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L398:
	movslq	7200(%r12), %rax
	movq	%r8, %r14
	movl	%eax, %esi
	addl	$1, %esi
	js	.L426
	cmpl	7204(%r12), %esi
	jle	.L427
.L426:
	leaq	7192(%r12), %rdi
	movq	%r14, %rdx
	call	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L396
	movslq	7200(%r12), %rax
	leal	1(%rax), %esi
.L427:
	movq	7216(%r12), %rdx
	movq	$511, (%rdx,%rax,8)
	movl	%esi, 7200(%r12)
.L396:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movq	%rdx, %rcx
	xorl	%eax, %eax
	sarq	$32, %rcx
	cmpl	$1, %ecx
	je	.L406
	movl	7232(%r12), %r10d
	andb	$63, %dh
	movq	7248(%r12), %rcx
	movq	%rdx, %r9
	testl	%r10d, %r10d
	je	.L407
	xorl	%edx, %edx
.L408:
	leal	(%r10,%rdx), %edi
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	movslq	%eax, %r11
	movl	%eax, %edi
	cmpq	%r9, (%rcx,%r11,8)
	ja	.L464
	jb	.L465
.L412:
	xorl	%eax, %eax
	testl	%edi, %edi
	jns	.L406
	notl	%edi
	movl	%edi, %r10d
.L407:
	movq	%r8, %rcx
	movl	%r10d, %edx
	movq	%r9, %rsi
	movq	%r8, -56(%rbp)
	leaq	7224(%r12), %rdi
	call	_ZN6icu_679UVector6415insertElementAtEliR10UErrorCode@PLT
	movq	-56(%rbp), %r8
	movq	16(%r12), %rsi
	movdqa	.LC0(%rip), %xmm0
	movl	(%r8), %edx
	testl	%edx, %edx
	setg	%al
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L465:
	cmpl	%eax, %edx
	je	.L413
	movl	%eax, %edx
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L462:
	cmpl	%eax, %ecx
	je	.L419
	movl	%eax, %ecx
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L464:
	cmpl	%eax, %edx
	je	.L457
	movl	%eax, %r10d
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L417:
	cmpl	%eax, %ecx
	je	.L458
	movl	%eax, %r9d
	jmp	.L416
.L413:
	addl	$1, %edi
.L457:
	notl	%edi
	jmp	.L412
.L419:
	addl	$1, %r10d
.L458:
	notl	%r10d
	jmp	.L418
	.cfi_endproc
.LFE4351:
	.size	_ZN6icu_6725CollationFastLatinBuilder6getCEsERKNS_13CollationDataER10UErrorCode.part.0, .-_ZN6icu_6725CollationFastLatinBuilder6getCEsERKNS_13CollationDataER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CollationFastLatinBuilder6getCEsERKNS_13CollationDataER10UErrorCode
	.type	_ZN6icu_6725CollationFastLatinBuilder6getCEsERKNS_13CollationDataER10UErrorCode, @function
_ZN6icu_6725CollationFastLatinBuilder6getCEsERKNS_13CollationDataER10UErrorCode:
.LFB3283:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L466
	jmp	_ZN6icu_6725CollationFastLatinBuilder6getCEsERKNS_13CollationDataER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L466:
	ret
	.cfi_endproc
.LFE3283:
	.size	_ZN6icu_6725CollationFastLatinBuilder6getCEsERKNS_13CollationDataER10UErrorCode, .-_ZN6icu_6725CollationFastLatinBuilder6getCEsERKNS_13CollationDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CollationFastLatinBuilder11addUniqueCEElR10UErrorCode
	.type	_ZN6icu_6725CollationFastLatinBuilder11addUniqueCEElR10UErrorCode, @function
_ZN6icu_6725CollationFastLatinBuilder11addUniqueCEElR10UErrorCode:
.LFB3287:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	movq	%rdx, %rcx
	testl	%eax, %eax
	jg	.L468
	testq	%rsi, %rsi
	je	.L468
	movq	%rsi, %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	je	.L468
	movl	7232(%rdi), %r9d
	movq	7248(%rdi), %r11
	andq	$-49153, %rsi
	testl	%r9d, %r9d
	je	.L470
	xorl	%r8d, %r8d
.L471:
	leal	(%r9,%r8), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rdx
	movl	%eax, %r10d
	cmpq	%rsi, (%r11,%rdx,8)
	ja	.L481
	jb	.L482
.L475:
	testl	%r10d, %r10d
	js	.L483
.L468:
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	cmpl	%eax, %r8d
	je	.L476
	movl	%eax, %r8d
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L481:
	cmpl	%eax, %r8d
	je	.L480
	movl	%eax, %r9d
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L483:
	notl	%r10d
	movl	%r10d, %r9d
.L470:
	addq	$7224, %rdi
	movl	%r9d, %edx
	jmp	_ZN6icu_679UVector6415insertElementAtEliR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L476:
	addl	$1, %r10d
.L480:
	notl	%r10d
	jmp	.L475
	.cfi_endproc
.LFE3287:
	.size	_ZN6icu_6725CollationFastLatinBuilder11addUniqueCEElR10UErrorCode, .-_ZN6icu_6725CollationFastLatinBuilder11addUniqueCEElR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725CollationFastLatinBuilder9getMiniCEEl
	.type	_ZNK6icu_6725CollationFastLatinBuilder9getMiniCEEl, @function
_ZNK6icu_6725CollationFastLatinBuilder9getMiniCEEl:
.LFB3288:
	.cfi_startproc
	endbr64
	movl	7232(%rdi), %r8d
	movq	7248(%rdi), %r9
	andq	$-49153, %rsi
	testl	%r8d, %r8d
	je	.L492
	xorl	%edx, %edx
.L486:
	leal	(%r8,%rdx), %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	movslq	%eax, %rcx
	cmpq	%rsi, (%r9,%rcx,8)
	ja	.L494
	jb	.L490
	addq	%rcx, %rcx
.L485:
	movq	7256(%rdi), %rax
	movzwl	(%rax,%rcx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	cmpl	%eax, %edx
	je	.L491
	movl	%eax, %edx
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L494:
	cmpl	%eax, %edx
	je	.L493
	movl	%eax, %r8d
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L491:
	addl	$1, %edx
.L493:
	notl	%edx
	movslq	%edx, %rax
	leaq	(%rax,%rax), %rcx
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L492:
	movq	$-2, %rcx
	jmp	.L485
	.cfi_endproc
.LFE3288:
	.size	_ZNK6icu_6725CollationFastLatinBuilder9getMiniCEEl, .-_ZNK6icu_6725CollationFastLatinBuilder9getMiniCEEl
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CollationFastLatinBuilder15encodeUniqueCEsER10UErrorCode
	.type	_ZN6icu_6725CollationFastLatinBuilder15encodeUniqueCEsER10UErrorCode, @function
_ZN6icu_6725CollationFastLatinBuilder15encodeUniqueCEsER10UErrorCode:
.LFB3289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	(%rsi), %r9d
	movq	%rsi, -112(%rbp)
	testl	%r9d, %r9d
	jle	.L555
.L495:
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L555:
	.cfi_restore_state
	movq	%rdi, %r15
	movq	7256(%rdi), %rdi
	call	uprv_free_67@PLT
	movl	7232(%r15), %eax
	leal	(%rax,%rax), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 7256(%r15)
	testq	%rax, %rax
	je	.L556
	movl	7232(%r15), %esi
	movl	7264(%r15), %eax
	testl	%esi, %esi
	jle	.L525
	movl	$0, -92(%rbp)
	xorl	%r14d, %r14d
	movq	%r15, %r13
	xorl	%r12d, %r12d
	movl	$0, -76(%rbp)
	xorl	%r8d, %r8d
	movl	%r14d, %r15d
	movl	$0, -56(%rbp)
	movl	$0, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L498:
	movq	7248(%r13), %rcx
	leaq	(%r12,%r12), %rdi
	movq	%rdi, -88(%rbp)
	movq	(%rcx,%r12,8), %rbx
	movq	%rbx, -64(%rbp)
	sarq	$32, %rbx
	movq	%rbx, -72(%rbp)
	movl	%ebx, -52(%rbp)
	cmpl	%ebx, -80(%rbp)
	jne	.L557
	movq	-64(%rbp), %rdx
	movl	%edx, %r11d
	movl	%edx, %ecx
	shrl	$16, %r11d
	cmpl	-56(%rbp), %r11d
	je	.L507
	testl	%r15d, %r15d
	jne	.L508
	movl	-76(%rbp), %ecx
	leaq	(%r12,%r12), %rdi
	addq	7256(%r13), %rdi
	testl	%ecx, %ecx
	je	.L509
	cmpl	$991, %ecx
	ja	.L523
	movl	%edx, %ebx
	addl	$32, %ecx
	andl	$16191, %ebx
	movl	%ecx, -76(%rbp)
	cmpl	$1280, %ebx
	jbe	.L558
	movl	$1, -92(%rbp)
	movl	$1, %ecx
	xorl	%r10d, %r10d
	movl	$-3072, %r9d
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L557:
	cmpl	%eax, %ebx
	jbe	.L500
	movl	%r8d, %ecx
	movq	%r12, -104(%rbp)
	leaq	7304(%r13), %r14
	addl	$1, %ecx
	movslq	%ecx, %rbx
	movq	%rbx, %r12
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L560:
	addq	$1, %r12
	movl	7260(%r13,%r12,4), %eax
	cmpl	%eax, -52(%rbp)
	jbe	.L559
.L501:
	movl	%r15d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%r12d, %ebx
	call	_ZN6icu_6713UnicodeString9setCharAtEiDs@PLT
	cmpl	$3, %r12d
	jle	.L560
	movl	%r12d, %r8d
	movl	7232(%r13), %esi
	movq	-104(%rbp), %r12
	movl	$-1, %eax
.L500:
	movl	-72(%rbp), %ecx
	cmpl	7292(%r13), %ecx
	jnb	.L502
	testl	%r15d, %r15d
	je	.L533
	cmpl	$4087, %r15d
	ja	.L552
	addl	$8, %r15d
.L503:
	movq	-64(%rbp), %rdx
	movl	%edx, %edi
	movl	%edx, %ecx
	shrl	$16, %edi
	movl	%edi, -56(%rbp)
	cmpl	$1280, %edi
	je	.L561
	cmpl	$1279, -56(%rbp)
	jbe	.L514
	movl	-72(%rbp), %edx
	movq	-88(%rbp), %rdi
	addq	7256(%r13), %rdi
	movl	%edx, -80(%rbp)
.L518:
	andl	$16191, %ecx
	leal	-3072(%r15), %r9d
	movl	%r15d, %r10d
	cmpl	$1280, %ecx
	ja	.L541
	movl	$192, -76(%rbp)
	xorl	%ecx, %ecx
	movl	$0, -92(%rbp)
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L507:
	movl	-64(%rbp), %ebx
	leaq	(%r12,%r12), %rdi
	addq	7256(%r13), %rdi
	andl	$16191, %ebx
	cmpl	$1280, %ebx
	jbe	.L562
	cmpl	$6, -92(%rbp)
	jbe	.L563
.L523:
	movl	$1, %edx
	movw	%dx, (%rdi)
.L505:
	addq	$1, %r12
	cmpl	%r12d, %esi
	jg	.L498
.L525:
	movq	-112(%rbp), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	setle	%r12b
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L502:
	cmpl	$4095, %r15d
	jbe	.L534
	cmpl	$63487, %r15d
	ja	.L506
	addl	$1024, %r15d
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L559:
	movq	-104(%rbp), %r12
	movl	7232(%r13), %esi
	movl	%ebx, %r8d
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L509:
	movl	-64(%rbp), %ebx
	andl	$16191, %ebx
	cmpl	$1280, %ebx
	ja	.L540
	movl	$384, -76(%rbp)
	movl	$384, %ecx
.L520:
	movl	%r11d, -56(%rbp)
	movl	$0, -92(%rbp)
	.p2align 4,,10
	.p2align 3
.L528:
	movw	%cx, (%rdi)
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L561:
	movl	-64(%rbp), %ebx
	movl	-72(%rbp), %ecx
	leal	-3072(%r15), %r9d
	movl	%r15d, %r10d
	movq	-88(%rbp), %rdi
	addq	7256(%r13), %rdi
	andl	$16191, %ebx
	movl	%ecx, -80(%rbp)
	cmpl	$1280, %ebx
	ja	.L543
	movl	$160, -76(%rbp)
	xorl	%ecx, %ecx
	movl	$0, -92(%rbp)
	.p2align 4,,10
	.p2align 3
.L516:
	orl	%r10d, %ecx
	cmpl	$1016, %r9d
	jbe	.L528
	orw	-76(%rbp), %cx
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L533:
	movl	$3072, %r15d
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L534:
	movl	$4096, %r15d
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L508:
	cmpl	$1279, %r11d
	ja	.L513
	movl	-76(%rbp), %edx
	cmpl	$160, %edx
	je	.L536
	leaq	(%r12,%r12), %rdi
	addq	7256(%r13), %rdi
	cmpl	$127, %edx
	ja	.L523
.L554:
	movl	-64(%rbp), %ebx
	addl	$32, %edx
	leal	-3072(%r15), %r9d
	movl	%r15d, %r10d
	movl	%edx, -76(%rbp)
	andl	$16191, %ebx
	cmpl	$1280, %ebx
	jbe	.L564
	movl	$1, -92(%rbp)
	movl	$1, %ecx
	jmp	.L511
.L513:
	leaq	(%r12,%r12), %rdi
	addq	7256(%r13), %rdi
	cmpl	$1280, %r11d
	jne	.L565
	movl	-64(%rbp), %ebx
	leal	-3072(%r15), %r9d
	movl	%r15d, %r10d
	andl	$16191, %ebx
	cmpl	$1280, %ebx
	ja	.L550
	movl	$1280, -56(%rbp)
	xorl	%ecx, %ecx
	movl	$160, -76(%rbp)
	movl	$0, -92(%rbp)
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L540:
	movl	$1, -92(%rbp)
	movl	%ecx, %r15d
	xorl	%r10d, %r10d
	movl	$1, %ecx
	movl	$384, -76(%rbp)
	movl	$-3072, %r9d
.L511:
	movl	%r11d, -56(%rbp)
	jmp	.L516
.L506:
	movb	$1, 7296(%r13)
.L552:
	movq	7256(%r13), %rcx
	movq	-88(%rbp), %rdx
	movl	$1, %edi
	movw	%di, (%rcx,%rdx)
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L558:
	movzwl	-76(%rbp), %ecx
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L564:
	movl	%r11d, -56(%rbp)
	xorl	%ecx, %ecx
	movl	$0, -92(%rbp)
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L565:
	movl	-76(%rbp), %edx
	cmpl	$191, %edx
	jbe	.L538
	cmpl	$351, %edx
	jbe	.L554
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L562:
	movzwl	-92(%rbp), %ecx
	leal	-3072(%r15), %r9d
	movl	%r15d, %r10d
	jmp	.L516
.L536:
	movl	%r11d, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L514:
	andl	$16191, %ecx
	movq	-88(%rbp), %rdi
	addq	7256(%r13), %rdi
	movl	%r15d, %r10d
	cmpl	$1280, %ecx
	movl	-52(%rbp), %ecx
	leal	-3072(%r15), %r9d
	movl	%ecx, -80(%rbp)
	ja	.L521
	movl	$0, -76(%rbp)
	xorl	%ecx, %ecx
	movl	$0, -92(%rbp)
	jmp	.L516
.L543:
	movl	$1280, %r11d
.L550:
	movl	$1, -92(%rbp)
	movl	$1, %ecx
	movl	$160, -76(%rbp)
	jmp	.L511
.L521:
	movl	$1, -92(%rbp)
	movl	-56(%rbp), %r11d
	movl	$1, %ecx
	movl	$0, -76(%rbp)
	jmp	.L511
.L541:
	movl	$1, -92(%rbp)
	movl	-56(%rbp), %r11d
	movl	$1, %ecx
	movl	$192, -76(%rbp)
	jmp	.L511
.L538:
	movl	%r11d, -56(%rbp)
	jmp	.L518
.L556:
	movq	-112(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L495
.L563:
	addl	$1, -92(%rbp)
	leal	-3072(%r15), %r9d
	movl	-92(%rbp), %ecx
	movl	%r15d, %r10d
	movl	-56(%rbp), %r11d
	jmp	.L511
	.cfi_endproc
.LFE3289:
	.size	_ZN6icu_6725CollationFastLatinBuilder15encodeUniqueCEsER10UErrorCode, .-_ZN6icu_6725CollationFastLatinBuilder15encodeUniqueCEsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CollationFastLatinBuilder7forDataERKNS_13CollationDataER10UErrorCode
	.type	_ZN6icu_6725CollationFastLatinBuilder7forDataERKNS_13CollationDataER10UErrorCode, @function
_ZN6icu_6725CollationFastLatinBuilder7forDataERKNS_13CollationDataER10UErrorCode:
.LFB3279:
	.cfi_startproc
	endbr64
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L606
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movswl	7312(%rdi), %eax
	shrl	$5, %eax
	je	.L569
	movl	$27, (%rdx)
.L605:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_restore_state
	movq	%rsi, %r14
	call	_ZN6icu_6725CollationFastLatinBuilder10loadGroupsERKNS_13CollationDataER10UErrorCode
	testb	%al, %al
	je	.L605
	movl	7280(%r13), %eax
	movl	(%r12), %esi
	movl	%eax, 7292(%r13)
	testl	%esi, %esi
	jg	.L571
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6725CollationFastLatinBuilder6getCEsERKNS_13CollationDataER10UErrorCode.part.0
.L571:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6725CollationFastLatinBuilder15encodeUniqueCEsER10UErrorCode
	testb	%al, %al
	je	.L605
	cmpb	$0, 7296(%r13)
	leaq	7192(%r13), %rbx
	leaq	7224(%r13), %r15
	jne	.L572
.L581:
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L573
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6725CollationFastLatinBuilder13encodeCharCEsER10UErrorCode.part.0
	testb	%al, %al
	je	.L573
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L573
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6725CollationFastLatinBuilder18encodeContractionsER10UErrorCode.part.0
	testb	%al, %al
	setne	%al
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	7284(%r13), %eax
	movq	%rbx, %rdi
	movl	%eax, 7292(%r13)
	call	_ZN6icu_679UVector6417removeAllElementsEv@PLT
	movq	%r15, %rdi
	call	_ZN6icu_679UVector6417removeAllElementsEv@PLT
	movl	7368(%r13), %edx
	movb	$0, 7296(%r13)
	movzwl	7312(%r13), %eax
	testl	%edx, %edx
	jne	.L575
	testb	$1, %al
	jne	.L607
.L575:
	testw	%ax, %ax
	js	.L577
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L578:
	cmpl	%edx, %ecx
	jbe	.L576
	cmpl	$1023, %edx
	jg	.L579
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, 7312(%r13)
.L576:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L580
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6725CollationFastLatinBuilder6getCEsERKNS_13CollationDataER10UErrorCode.part.0
.L580:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6725CollationFastLatinBuilder15encodeUniqueCEsER10UErrorCode
	testb	%al, %al
	je	.L605
	cmpb	$0, 7296(%r13)
	je	.L581
.L573:
	xorl	%eax, %eax
.L582:
	movq	%rbx, %rdi
	movb	%al, -49(%rbp)
	call	_ZN6icu_679UVector6417removeAllElementsEv@PLT
	movq	%r15, %rdi
	call	_ZN6icu_679UVector6417removeAllElementsEv@PLT
	movzbl	-49(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_restore_state
	movl	7316(%r13), %ecx
	jmp	.L578
.L579:
	orl	$-32, %eax
	movl	%edx, 7316(%r13)
	movw	%ax, 7312(%r13)
	jmp	.L576
.L607:
	leaq	7304(%r13), %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L576
	.cfi_endproc
.LFE3279:
	.size	_ZN6icu_6725CollationFastLatinBuilder7forDataERKNS_13CollationDataER10UErrorCode, .-_ZN6icu_6725CollationFastLatinBuilder7forDataERKNS_13CollationDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CollationFastLatinBuilder13encodeCharCEsER10UErrorCode
	.type	_ZN6icu_6725CollationFastLatinBuilder13encodeCharCEsER10UErrorCode, @function
_ZN6icu_6725CollationFastLatinBuilder13encodeCharCEsER10UErrorCode:
.LFB3290:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L609
	jmp	_ZN6icu_6725CollationFastLatinBuilder13encodeCharCEsER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L609:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3290:
	.size	_ZN6icu_6725CollationFastLatinBuilder13encodeCharCEsER10UErrorCode, .-_ZN6icu_6725CollationFastLatinBuilder13encodeCharCEsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CollationFastLatinBuilder18encodeContractionsER10UErrorCode
	.type	_ZN6icu_6725CollationFastLatinBuilder18encodeContractionsER10UErrorCode, @function
_ZN6icu_6725CollationFastLatinBuilder18encodeContractionsER10UErrorCode:
.LFB3291:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L611
	jmp	_ZN6icu_6725CollationFastLatinBuilder18encodeContractionsER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L611:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3291:
	.size	_ZN6icu_6725CollationFastLatinBuilder18encodeContractionsER10UErrorCode, .-_ZN6icu_6725CollationFastLatinBuilder18encodeContractionsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725CollationFastLatinBuilder12encodeTwoCEsEll
	.type	_ZNK6icu_6725CollationFastLatinBuilder12encodeTwoCEsEll, @function
_ZNK6icu_6725CollationFastLatinBuilder12encodeTwoCEsEll:
.LFB3292:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L650
	movabsq	$4311744768, %rcx
	movl	$1, %eax
	cmpq	%rcx, %rsi
	je	.L650
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	7232(%rdi), %r8d
	movq	%rsi, %rbx
	movq	7248(%rdi), %r9
	andb	$63, %bh
	testl	%r8d, %r8d
	je	.L632
	movl	%r8d, %r11d
	xorl	%r10d, %r10d
.L615:
	leal	(%r11,%r10), %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	movslq	%eax, %rcx
	cmpq	%rbx, (%r9,%rcx,8)
	ja	.L655
	jb	.L619
.L653:
	addq	%rcx, %rcx
.L614:
	movq	7256(%rdi), %rdi
	movzwl	(%rdi,%rcx), %eax
	cmpl	$1, %eax
	je	.L612
	cmpl	$4095, %eax
	jbe	.L621
	shrl	$11, %esi
	andl	$24, %esi
	addl	$8, %esi
	orl	%esi, %eax
.L621:
	testq	%rdx, %rdx
	jne	.L656
.L612:
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L650:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L619:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	cmpl	%eax, %r10d
	je	.L620
	movl	%eax, %r10d
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L655:
	cmpl	%eax, %r10d
	je	.L617
	movl	%eax, %r11d
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L656:
	movq	%rdx, %r11
	andq	$-49153, %r11
	testl	%r8d, %r8d
	je	.L633
	xorl	%r10d, %r10d
.L623:
	leal	(%r8,%r10), %esi
	movl	%esi, %ecx
	shrl	$31, %ecx
	addl	%esi, %ecx
	sarl	%ecx
	movslq	%ecx, %rsi
	cmpq	%r11, (%r9,%rsi,8)
	ja	.L624
	jb	.L625
.L654:
	addq	%rsi, %rsi
.L622:
	movzwl	(%rdi,%rsi), %ecx
	movl	%ecx, %esi
	cmpw	$1, %cx
	je	.L635
	andl	$49152, %edx
	cmpl	$4095, %eax
	jbe	.L628
	movl	%eax, %edi
	andl	$992, %edi
	cmpl	$160, %edi
	je	.L657
.L628:
	leal	-993(%rcx), %esi
	cmpl	$3102, %esi
	jbe	.L629
	shrl	$11, %edx
	addl	$8, %edx
	orl	%edx, %ecx
.L629:
	sall	$16, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	orl	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L625:
	.cfi_restore_state
	cmpl	%ecx, %r10d
	je	.L626
	movl	%ecx, %r10d
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L624:
	cmpl	%ecx, %r10d
	je	.L658
	movl	%ecx, %r8d
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L657:
	movl	%ecx, %r8d
	andl	$992, %r8d
	andl	$7, %esi
	sete	%dil
	testl	%edx, %edx
	sete	%sil
	testb	%sil, %dil
	je	.L628
	cmpl	$383, %r8d
	jbe	.L628
	andl	$-993, %eax
	orl	%r8d, %eax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L635:
	movl	$1, %eax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L620:
	leal	1(%r10), %ecx
	notl	%ecx
	movslq	%ecx, %rcx
	addq	%rcx, %rcx
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L617:
	notl	%r10d
	movslq	%r10d, %rcx
	jmp	.L653
.L626:
	leal	1(%r10), %esi
	notl	%esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L622
.L632:
	movq	$-2, %rcx
	jmp	.L614
.L658:
	notl	%r10d
	movslq	%r10d, %rsi
	jmp	.L654
.L633:
	movq	$-2, %rsi
	jmp	.L622
	.cfi_endproc
.LFE3292:
	.size	_ZNK6icu_6725CollationFastLatinBuilder12encodeTwoCEsEll, .-_ZNK6icu_6725CollationFastLatinBuilder12encodeTwoCEsEll
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CollationFastLatinBuilderD2Ev
	.type	_ZN6icu_6725CollationFastLatinBuilderD2Ev, @function
_ZN6icu_6725CollationFastLatinBuilderD2Ev:
.LFB3276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725CollationFastLatinBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	7256(%rdi), %rdi
	call	uprv_free_67@PLT
	leaq	7304(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	7224(%r12), %rdi
	call	_ZN6icu_679UVector64D1Ev@PLT
	leaq	7192(%r12), %rdi
	call	_ZN6icu_679UVector64D1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3276:
	.size	_ZN6icu_6725CollationFastLatinBuilderD2Ev, .-_ZN6icu_6725CollationFastLatinBuilderD2Ev
	.globl	_ZN6icu_6725CollationFastLatinBuilderD1Ev
	.set	_ZN6icu_6725CollationFastLatinBuilderD1Ev,_ZN6icu_6725CollationFastLatinBuilderD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CollationFastLatinBuilderD0Ev
	.type	_ZN6icu_6725CollationFastLatinBuilderD0Ev, @function
_ZN6icu_6725CollationFastLatinBuilderD0Ev:
.LFB3278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725CollationFastLatinBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	7256(%rdi), %rdi
	call	uprv_free_67@PLT
	leaq	7304(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	7224(%r12), %rdi
	call	_ZN6icu_679UVector64D1Ev@PLT
	leaq	7192(%r12), %rdi
	call	_ZN6icu_679UVector64D1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3278:
	.size	_ZN6icu_6725CollationFastLatinBuilderD0Ev, .-_ZN6icu_6725CollationFastLatinBuilderD0Ev
	.weak	_ZTSN6icu_6725CollationFastLatinBuilderE
	.section	.rodata._ZTSN6icu_6725CollationFastLatinBuilderE,"aG",@progbits,_ZTSN6icu_6725CollationFastLatinBuilderE,comdat
	.align 32
	.type	_ZTSN6icu_6725CollationFastLatinBuilderE, @object
	.size	_ZTSN6icu_6725CollationFastLatinBuilderE, 37
_ZTSN6icu_6725CollationFastLatinBuilderE:
	.string	"N6icu_6725CollationFastLatinBuilderE"
	.weak	_ZTIN6icu_6725CollationFastLatinBuilderE
	.section	.data.rel.ro._ZTIN6icu_6725CollationFastLatinBuilderE,"awG",@progbits,_ZTIN6icu_6725CollationFastLatinBuilderE,comdat
	.align 8
	.type	_ZTIN6icu_6725CollationFastLatinBuilderE, @object
	.size	_ZTIN6icu_6725CollationFastLatinBuilderE, 24
_ZTIN6icu_6725CollationFastLatinBuilderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725CollationFastLatinBuilderE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6725CollationFastLatinBuilderE
	.section	.data.rel.ro._ZTVN6icu_6725CollationFastLatinBuilderE,"awG",@progbits,_ZTVN6icu_6725CollationFastLatinBuilderE,comdat
	.align 8
	.type	_ZTVN6icu_6725CollationFastLatinBuilderE, @object
	.size	_ZTVN6icu_6725CollationFastLatinBuilderE, 40
_ZTVN6icu_6725CollationFastLatinBuilderE:
	.quad	0
	.quad	_ZTIN6icu_6725CollationFastLatinBuilderE
	.quad	_ZN6icu_6725CollationFastLatinBuilderD1Ev
	.quad	_ZN6icu_6725CollationFastLatinBuilderD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	4311744768
	.quad	0
	.align 16
.LC1:
	.quad	6442450944
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
