	.file	"toupptrn.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723UppercaseTransliterator17getDynamicClassIDEv
	.type	_ZNK6icu_6723UppercaseTransliterator17getDynamicClassIDEv, @function
_ZNK6icu_6723UppercaseTransliterator17getDynamicClassIDEv:
.LFB3045:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6723UppercaseTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3045:
	.size	_ZNK6icu_6723UppercaseTransliterator17getDynamicClassIDEv, .-_ZNK6icu_6723UppercaseTransliterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723UppercaseTransliteratorD2Ev
	.type	_ZN6icu_6723UppercaseTransliteratorD2Ev, @function
_ZN6icu_6723UppercaseTransliteratorD2Ev:
.LFB3050:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6723UppercaseTransliteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6721CaseMapTransliteratorD2Ev@PLT
	.cfi_endproc
.LFE3050:
	.size	_ZN6icu_6723UppercaseTransliteratorD2Ev, .-_ZN6icu_6723UppercaseTransliteratorD2Ev
	.globl	_ZN6icu_6723UppercaseTransliteratorD1Ev
	.set	_ZN6icu_6723UppercaseTransliteratorD1Ev,_ZN6icu_6723UppercaseTransliteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723UppercaseTransliteratorD0Ev
	.type	_ZN6icu_6723UppercaseTransliteratorD0Ev, @function
_ZN6icu_6723UppercaseTransliteratorD0Ev:
.LFB3052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6723UppercaseTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6721CaseMapTransliteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3052:
	.size	_ZN6icu_6723UppercaseTransliteratorD0Ev, .-_ZN6icu_6723UppercaseTransliteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723UppercaseTransliterator5cloneEv
	.type	_ZNK6icu_6723UppercaseTransliterator5cloneEv, @function
_ZNK6icu_6723UppercaseTransliterator5cloneEv:
.LFB3056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L6
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6721CaseMapTransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6723UppercaseTransliteratorE(%rip), %rax
	movq	%rax, (%r12)
.L6:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3056:
	.size	_ZNK6icu_6723UppercaseTransliterator5cloneEv, .-_ZNK6icu_6723UppercaseTransliterator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723UppercaseTransliterator16getStaticClassIDEv
	.type	_ZN6icu_6723UppercaseTransliterator16getStaticClassIDEv, @function
_ZN6icu_6723UppercaseTransliterator16getStaticClassIDEv:
.LFB3044:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6723UppercaseTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3044:
	.size	_ZN6icu_6723UppercaseTransliterator16getStaticClassIDEv, .-_ZN6icu_6723UppercaseTransliterator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723UppercaseTransliteratorC2ERKS0_
	.type	_ZN6icu_6723UppercaseTransliteratorC2ERKS0_, @function
_ZN6icu_6723UppercaseTransliteratorC2ERKS0_:
.LFB3054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6721CaseMapTransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6723UppercaseTransliteratorE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3054:
	.size	_ZN6icu_6723UppercaseTransliteratorC2ERKS0_, .-_ZN6icu_6723UppercaseTransliteratorC2ERKS0_
	.globl	_ZN6icu_6723UppercaseTransliteratorC1ERKS0_
	.set	_ZN6icu_6723UppercaseTransliteratorC1ERKS0_,_ZN6icu_6723UppercaseTransliteratorC2ERKS0_
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"-"
	.string	"U"
	.string	"p"
	.string	"p"
	.string	"e"
	.string	"r"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723UppercaseTransliteratorC2Ev
	.type	_ZN6icu_6723UppercaseTransliteratorC2Ev, @function
_ZN6icu_6723UppercaseTransliteratorC2Ev:
.LFB3047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$9, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-96(%rbp), %r12
	movq	%rdi, %rbx
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rax
	movq	%rax, -104(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	ucase_toFullUpper_67@GOTPCREL(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6721CaseMapTransliteratorC2ERKNS_13UnicodeStringEPFiiPFiPvaES4_PPKDsiE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6723UppercaseTransliteratorE(%rip), %rax
	movq	%rax, (%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3047:
	.size	_ZN6icu_6723UppercaseTransliteratorC2Ev, .-_ZN6icu_6723UppercaseTransliteratorC2Ev
	.globl	_ZN6icu_6723UppercaseTransliteratorC1Ev
	.set	_ZN6icu_6723UppercaseTransliteratorC1Ev,_ZN6icu_6723UppercaseTransliteratorC2Ev
	.weak	_ZTSN6icu_6723UppercaseTransliteratorE
	.section	.rodata._ZTSN6icu_6723UppercaseTransliteratorE,"aG",@progbits,_ZTSN6icu_6723UppercaseTransliteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6723UppercaseTransliteratorE, @object
	.size	_ZTSN6icu_6723UppercaseTransliteratorE, 35
_ZTSN6icu_6723UppercaseTransliteratorE:
	.string	"N6icu_6723UppercaseTransliteratorE"
	.weak	_ZTIN6icu_6723UppercaseTransliteratorE
	.section	.data.rel.ro._ZTIN6icu_6723UppercaseTransliteratorE,"awG",@progbits,_ZTIN6icu_6723UppercaseTransliteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6723UppercaseTransliteratorE, @object
	.size	_ZTIN6icu_6723UppercaseTransliteratorE, 24
_ZTIN6icu_6723UppercaseTransliteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6723UppercaseTransliteratorE
	.quad	_ZTIN6icu_6721CaseMapTransliteratorE
	.weak	_ZTVN6icu_6723UppercaseTransliteratorE
	.section	.data.rel.ro._ZTVN6icu_6723UppercaseTransliteratorE,"awG",@progbits,_ZTVN6icu_6723UppercaseTransliteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6723UppercaseTransliteratorE, @object
	.size	_ZTVN6icu_6723UppercaseTransliteratorE, 152
_ZTVN6icu_6723UppercaseTransliteratorE:
	.quad	0
	.quad	_ZTIN6icu_6723UppercaseTransliteratorE
	.quad	_ZN6icu_6723UppercaseTransliteratorD1Ev
	.quad	_ZN6icu_6723UppercaseTransliteratorD0Ev
	.quad	_ZNK6icu_6723UppercaseTransliterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6723UppercaseTransliterator5cloneEv
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.quad	_ZNK6icu_6721CaseMapTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.quad	_ZNK6icu_6714Transliterator5getIDEv
	.quad	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE
	.local	_ZZN6icu_6723UppercaseTransliterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6723UppercaseTransliterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
