	.file	"uspoof.cpp"
	.text
	.p2align 4
	.globl	uspoof_clone_67
	.type	uspoof_clone_67, @function
uspoof_clone_67:
.LFB3101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L1
	movl	$48, %edi
	movq	%rax, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	_ZN6icu_679SpoofImplC1ERKS0_R10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L4
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_679SpoofImplD0Ev@PLT
.L4:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679SpoofImpl15asUSpoofCheckerEv@PLT
.L3:
	.cfi_restore_state
	movl	$7, (%rbx)
.L1:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3101:
	.size	uspoof_clone_67, .-uspoof_clone_67
	.p2align 4
	.globl	uspoof_close_67
	.type	uspoof_close_67, @function
uspoof_close_67:
.LFB3102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_679SpoofImpl12validateThisEP13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L10
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*8(%rax)
.L10:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L17:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3102:
	.size	uspoof_close_67, .-uspoof_close_67
	.p2align 4
	.globl	uspoof_setChecks_67
	.type	uspoof_setChecks_67, @function
uspoof_setChecks_67:
.LFB3103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	movq	%rdx, %rsi
	call	_ZN6icu_679SpoofImpl12validateThisEP13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L18
	testl	$-1073807360, %ebx
	jne	.L25
	movl	%ebx, 12(%rax)
.L18:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	popq	%rbx
	movl	$1, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3103:
	.size	uspoof_setChecks_67, .-uspoof_setChecks_67
	.p2align 4
	.globl	uspoof_getChecks_67
	.type	uspoof_getChecks_67, @function
uspoof_getChecks_67:
.LFB3104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L28
	movl	12(%rax), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3104:
	.size	uspoof_getChecks_67, .-uspoof_getChecks_67
	.p2align 4
	.globl	uspoof_setRestrictionLevel_67
	.type	uspoof_setRestrictionLevel_67, @function
uspoof_setRestrictionLevel_67:
.LFB3105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	leaq	-28(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	call	_ZN6icu_679SpoofImpl12validateThisEP13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L30
	orl	$16, 12(%rax)
	movl	%ebx, 40(%rax)
.L30:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L37:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3105:
	.size	uspoof_setRestrictionLevel_67, .-uspoof_setRestrictionLevel_67
	.p2align 4
	.globl	uspoof_getRestrictionLevel_67
	.type	uspoof_getRestrictionLevel_67, @function
uspoof_getRestrictionLevel_67:
.LFB3106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L41
	movl	40(%rax), %eax
.L38:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L43
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	$1610612736, %eax
	jmp	.L38
.L43:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3106:
	.size	uspoof_getRestrictionLevel_67, .-uspoof_getRestrictionLevel_67
	.p2align 4
	.globl	uspoof_setAllowedLocales_67
	.type	uspoof_setAllowedLocales_67, @function
uspoof_setAllowedLocales_67:
.LFB3107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	call	_ZN6icu_679SpoofImpl12validateThisEP13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L44
	movq	%r12, %rdx
	movq	%r13, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679SpoofImpl17setAllowedLocalesEPKcR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3107:
	.size	uspoof_setAllowedLocales_67, .-uspoof_setAllowedLocales_67
	.p2align 4
	.globl	uspoof_getAllowedLocales_67
	.type	uspoof_getAllowedLocales_67, @function
uspoof_getAllowedLocales_67:
.LFB3108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	_ZN6icu_679SpoofImpl12validateThisEP13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L47
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679SpoofImpl17getAllowedLocalesER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3108:
	.size	uspoof_getAllowedLocales_67, .-uspoof_getAllowedLocales_67
	.p2align 4
	.globl	uspoof_getAllowedChars_67
	.type	uspoof_getAllowedChars_67, @function
uspoof_getAllowedChars_67:
.LFB3109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L50
	movq	24(%rax), %rax
.L50:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3109:
	.size	uspoof_getAllowedChars_67, .-uspoof_getAllowedChars_67
	.p2align 4
	.globl	uspoof_getAllowedUnicodeSet_67
	.type	uspoof_getAllowedUnicodeSet_67, @function
uspoof_getAllowedUnicodeSet_67:
.LFB3110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L56
	movq	24(%rax), %rax
.L56:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3110:
	.size	uspoof_getAllowedUnicodeSet_67, .-uspoof_getAllowedUnicodeSet_67
	.p2align 4
	.globl	_Z34uspoof_getSkeletonUnicodeString_67PK13USpoofCheckerjRKN6icu_6713UnicodeStringERS3_P10UErrorCode
	.type	_Z34uspoof_getSkeletonUnicodeString_67PK13USpoofCheckerjRKN6icu_6713UnicodeStringERS3_P10UErrorCode, @function
_Z34uspoof_getSkeletonUnicodeString_67PK13USpoofCheckerjRKN6icu_6713UnicodeStringERS3_P10UErrorCode:
.LFB3124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	movl	(%r15), %esi
	testl	%esi, %esi
	jg	.L63
	movq	_ZL14gNfdNormalizer(%rip), %rdi
	movq	%rax, %r12
	movq	%r15, %rcx
	movq	%r14, %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	movl	$2, %edx
	leaq	-192(%rbp), %r13
	movq	(%rdi), %rax
	movq	%rbx, -192(%rbp)
	movw	%dx, -184(%rbp)
	movq	%r13, %rdx
	call	*24(%rax)
	movq	%rbx, -128(%rbp)
	movswl	-184(%rbp), %ebx
	movl	$2, %ecx
	movw	%cx, -120(%rbp)
	testw	%bx, %bx
	js	.L64
	sarl	$5, %ebx
	movl	%ebx, -196(%rbp)
.L65:
	movl	-196(%rbp), %eax
	leaq	-128(%rbp), %r14
	testl	%eax, %eax
	jle	.L66
	xorl	%ebx, %ebx
	leaq	-128(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L69:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %esi
	cmpl	$65535, %eax
	jbe	.L67
	movq	16(%r12), %rdi
	movq	%r14, %rdx
	addl	$2, %ebx
	call	_ZNK6icu_679SpoofData16confusableLookupEiRNS_13UnicodeStringE@PLT
	cmpl	%ebx, -196(%rbp)
	jg	.L69
.L66:
	movq	_ZL14gNfdNormalizer(%rip), %rdi
	movq	-208(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L63:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L74
	movq	-208(%rbp), %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movl	-180(%rbp), %eax
	movl	%eax, -196(%rbp)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L67:
	movq	16(%r12), %rdi
	movq	%r14, %rdx
	addl	$1, %ebx
	call	_ZNK6icu_679SpoofData16confusableLookupEiRNS_13UnicodeStringE@PLT
	cmpl	-196(%rbp), %ebx
	jl	.L69
	jmp	.L66
.L74:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3124:
	.size	_Z34uspoof_getSkeletonUnicodeString_67PK13USpoofCheckerjRKN6icu_6713UnicodeStringERS3_P10UErrorCode, .-_Z34uspoof_getSkeletonUnicodeString_67PK13USpoofCheckerjRKN6icu_6713UnicodeStringERS3_P10UErrorCode
	.p2align 4
	.globl	uspoof_areConfusableUnicodeString_67
	.type	uspoof_areConfusableUnicodeString_67, @function
uspoof_areConfusableUnicodeString_67:
.LFB3119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rcx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	movl	(%r12), %esi
	movq	%rax, %r9
	xorl	%eax, %eax
	testl	%esi, %esi
	jg	.L75
	movl	12(%r9), %eax
	andl	$7, %eax
	jne	.L77
	movl	$27, (%r12)
.L75:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L107
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	leaq	-192(%rbp), %r15
	xorl	%esi, %esi
	movq	%r12, %r8
	movq	%r14, %rdx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r10
	movl	$2, %eax
	movq	%r15, %rcx
	movq	%rbx, %rdi
	movq	%r9, -272(%rbp)
	movq	%r10, -192(%rbp)
	movw	%ax, -184(%rbp)
	call	_Z34uspoof_getSkeletonUnicodeString_67PK13USpoofCheckerjRKN6icu_6713UnicodeStringERS3_P10UErrorCode
	movl	$2, %edx
	xorl	%esi, %esi
	movq	%r12, %r8
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r10
	movw	%dx, -120(%rbp)
	movq	%rbx, %rdi
	movq	%r13, %rdx
	movq	%r10, -128(%rbp)
	leaq	-128(%rbp), %r10
	movq	%r10, %rcx
	movq	%r10, -264(%rbp)
	call	_Z34uspoof_getSkeletonUnicodeString_67PK13USpoofCheckerjRKN6icu_6713UnicodeStringERS3_P10UErrorCode
	movl	(%r12), %ecx
	movq	-264(%rbp), %r10
	movq	-272(%rbp), %r9
	testl	%ecx, %ecx
	jle	.L78
.L85:
	xorl	%eax, %eax
.L79:
	movq	%r10, %rdi
	movl	%eax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-264(%rbp), %eax
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L78:
	movswl	-120(%rbp), %ecx
	movzwl	-184(%rbp), %eax
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L80
	testw	%ax, %ax
	js	.L81
	movswl	%ax, %edx
	sarl	$5, %edx
.L82:
	testw	%cx, %cx
	js	.L83
	sarl	$5, %ecx
.L84:
	cmpl	%edx, %ecx
	jne	.L85
	testb	%sil, %sil
	jne	.L85
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	%r9, -272(%rbp)
	movq	%r10, -264(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-272(%rbp), %r9
	movq	-264(%rbp), %r10
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%r9, -264(%rbp)
	testb	%sil, %sil
	je	.L85
	leaq	-256(%rbp), %rbx
	movq	%r10, -272(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_679ScriptSetC1Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	-264(%rbp), %r9
	leaq	-224(%rbp), %r14
	movq	%r9, %rdi
	call	_ZNK6icu_679SpoofImpl20getResolvedScriptSetERKNS_13UnicodeStringERNS_9ScriptSetER10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_679ScriptSetC1Ev@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	-264(%rbp), %r9
	movq	%r9, %rdi
	call	_ZNK6icu_679SpoofImpl20getResolvedScriptSetERKNS_13UnicodeStringERNS_9ScriptSetER10UErrorCode@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_679ScriptSet10intersectsERKS0_@PLT
	movq	-264(%rbp), %r9
	movl	$1, %ecx
	movq	-272(%rbp), %r10
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L108
.L86:
	movl	12(%r9), %edx
	movq	%r14, %rdi
	movq	%r10, -272(%rbp)
	testb	$1, %dl
	cmovne	%ecx, %eax
	movl	%eax, %ecx
	andl	$-3, %ecx
	testb	$2, %dl
	cmove	%ecx, %eax
	movl	%eax, %ecx
	andl	$-5, %ecx
	andl	$4, %edx
	cmove	%ecx, %eax
	movl	%eax, -264(%rbp)
	call	_ZN6icu_679ScriptSetD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_679ScriptSetD1Ev@PLT
	movq	-272(%rbp), %r10
	movl	-264(%rbp), %eax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L81:
	movl	-180(%rbp), %edx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%rbx, %rdi
	call	_ZNK6icu_679ScriptSet7isEmptyEv@PLT
	movq	-264(%rbp), %r9
	movq	-272(%rbp), %r10
	testb	%al, %al
	je	.L109
.L87:
	movl	$2, %eax
	movl	$2, %ecx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L83:
	movl	-116(%rbp), %ecx
	jmp	.L84
.L109:
	movq	%r14, %rdi
	call	_ZNK6icu_679ScriptSet7isEmptyEv@PLT
	movq	-264(%rbp), %r9
	movq	-272(%rbp), %r10
	testb	%al, %al
	jne	.L87
	movl	$6, %eax
	movl	$6, %ecx
	jmp	.L86
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3119:
	.size	uspoof_areConfusableUnicodeString_67, .-uspoof_areConfusableUnicodeString_67
	.p2align 4
	.globl	uspoof_areConfusable_67
	.type	uspoof_areConfusable_67, @function
uspoof_areConfusable_67:
.LFB3117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%r9, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$184, %rsp
	movq	%rcx, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	movl	(%r12), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L110
	cmpl	$-1, %ebx
	jl	.L116
	cmpl	$-1, %r13d
	jl	.L116
	movq	%r14, -200(%rbp)
	xorl	%esi, %esi
	leaq	-192(%rbp), %r14
	cmpl	$-1, %ebx
	leaq	-200(%rbp), %rdx
	sete	%sil
	movl	%ebx, %ecx
	movq	%r14, %rdi
	movq	%rdx, -224(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%esi, %esi
	movq	-216(%rbp), %rax
	cmpl	$-1, %r13d
	movl	%r13d, %ecx
	movq	-224(%rbp), %rdx
	leaq	-128(%rbp), %rbx
	sete	%sil
	movq	%rbx, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	uspoof_areConfusableUnicodeString_67
	movq	%rbx, %rdi
	movl	%eax, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-216(%rbp), %eax
.L110:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L118
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L110
.L118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3117:
	.size	uspoof_areConfusable_67, .-uspoof_areConfusable_67
	.p2align 4
	.globl	uspoof_areConfusableUTF8_67
	.type	uspoof_areConfusableUTF8_67, @function
uspoof_areConfusableUTF8_67:
.LFB3118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%r9, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$168, %rsp
	movq	%rcx, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	movl	0(%r13), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L119
	cmpl	$-1, %r15d
	jl	.L127
	cmpl	$-1, %ebx
	jl	.L127
	cmpl	$-1, %r15d
	jne	.L123
	movq	%r14, %rdi
	call	strlen@PLT
	movl	%eax, %r15d
.L123:
	movl	%r15d, %edx
	leaq	-192(%rbp), %r15
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	cmpl	$-1, %ebx
	jne	.L124
	movq	-200(%rbp), %rdi
	call	strlen@PLT
	movl	%eax, %ebx
.L124:
	leaq	-128(%rbp), %r14
	movq	-200(%rbp), %rsi
	movl	%ebx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	uspoof_areConfusableUnicodeString_67
	movq	%r14, %rdi
	movl	%eax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-200(%rbp), %eax
.L119:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L129
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movl	$1, 0(%r13)
	xorl	%eax, %eax
	jmp	.L119
.L129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3118:
	.size	uspoof_areConfusableUTF8_67, .-uspoof_areConfusableUTF8_67
	.p2align 4
	.globl	uspoof_getSkeleton_67
	.type	uspoof_getSkeleton_67, @function
uspoof_getSkeleton_67:
.LFB3123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$200, %rsp
	movl	%esi, -220(%rbp)
	movq	16(%rbp), %r14
	movq	%r8, -216(%rbp)
	movq	%r14, %rsi
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	movl	(%r14), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L130
	cmpl	$-1, %ebx
	jl	.L132
	testl	%r13d, %r13d
	js	.L132
	jne	.L133
	cmpq	$0, -216(%rbp)
	je	.L133
.L132:
	movl	$1, (%r14)
	xorl	%eax, %eax
.L130:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L145
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	leaq	-200(%rbp), %r9
	movq	%r15, -200(%rbp)
	xorl	%esi, %esi
	cmpl	$-1, %ebx
	leaq	-192(%rbp), %r15
	sete	%sil
	movl	%ebx, %ecx
	movq	%r9, %rdx
	movq	%r15, %rdi
	movq	%r9, -240(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r14, %r8
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-128(%rbp), %r10
	movl	-220(%rbp), %esi
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movq	%r10, %rcx
	movq	%r10, -232(%rbp)
	movw	%ax, -120(%rbp)
	call	_Z34uspoof_getSkeletonUnicodeString_67PK13USpoofCheckerjRKN6icu_6713UnicodeStringERS3_P10UErrorCode
	movq	-232(%rbp), %r10
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	-240(%rbp), %r9
	movq	-216(%rbp), %rax
	movq	%r10, %rdi
	movq	%r10, -216(%rbp)
	movq	%r9, %rsi
	movq	%rax, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movswl	-120(%rbp), %eax
	movq	-216(%rbp), %r10
	testw	%ax, %ax
	js	.L134
	sarl	$5, %eax
.L135:
	movq	%r10, %rdi
	movl	%eax, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-216(%rbp), %eax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L134:
	movl	-116(%rbp), %eax
	jmp	.L135
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3123:
	.size	uspoof_getSkeleton_67, .-uspoof_getSkeleton_67
	.p2align 4
	.globl	uspoof_getSkeletonUTF8_67
	.type	uspoof_getSkeletonUTF8_67, @function
uspoof_getSkeletonUTF8_67:
.LFB3125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$184, %rsp
	movl	%esi, -220(%rbp)
	movq	16(%rbp), %r15
	movq	%r8, -216(%rbp)
	movq	%r15, %rsi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	movl	(%r15), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L146
	cmpl	$-1, %ebx
	jl	.L148
	testl	%r12d, %r12d
	js	.L148
	jne	.L149
	cmpq	$0, -216(%rbp)
	je	.L149
.L148:
	movl	$1, (%r15)
	xorl	%eax, %eax
.L146:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L167
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	cmpl	$-1, %ebx
	je	.L168
.L150:
	movl	%ebx, %edx
	leaq	-192(%rbp), %rbx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	leaq	-128(%rbp), %r14
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movq	%rbx, %rdx
	movq	%r15, %r8
	movq	%r14, %rcx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	-220(%rbp), %esi
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	call	_Z34uspoof_getSkeletonUnicodeString_67PK13USpoofCheckerjRKN6icu_6713UnicodeStringERS3_P10UErrorCode
	movl	(%r15), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L151
	movl	$0, -196(%rbp)
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L152
	movswl	%ax, %r8d
	sarl	$5, %r8d
.L153:
	testb	$17, %al
	jne	.L158
	leaq	-118(%rbp), %rcx
	testb	$2, %al
	cmove	-104(%rbp), %rcx
.L154:
	movq	-216(%rbp), %rdi
	leaq	-196(%rbp), %rdx
	movq	%r15, %r9
	movl	%r12d, %esi
	call	u_strToUTF8_67@PLT
	movl	-196(%rbp), %eax
.L151:
	movq	%r14, %rdi
	movl	%eax, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-216(%rbp), %eax
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r14, %rdi
	call	strlen@PLT
	movl	%eax, %ebx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L152:
	movl	-116(%rbp), %r8d
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L158:
	xorl	%ecx, %ecx
	jmp	.L154
.L167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3125:
	.size	uspoof_getSkeletonUTF8_67, .-uspoof_getSkeletonUTF8_67
	.p2align 4
	.globl	uspoof_serialize_67
	.type	uspoof_serialize_67, @function
uspoof_serialize_67:
.LFB3126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movq	%rcx, %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	subq	$8, %rsp
	call	_ZN6icu_679SpoofImpl12validateThisEP13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L170
	movq	16(%rax), %rdi
	addq	$8, %rsp
	movq	%r12, %rcx
	movl	%r14d, %edx
	popq	%r12
	movq	%r13, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_679SpoofData9serializeEPviR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3126:
	.size	uspoof_serialize_67, .-uspoof_serialize_67
	.p2align 4
	.globl	uspoof_openCheckResult_67
	.type	uspoof_openCheckResult_67, @function
uspoof_openCheckResult_67:
.LFB3131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$224, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L173
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN6icu_6711CheckResultC1Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711CheckResult19asUSpoofCheckResultEv@PLT
.L173:
	.cfi_restore_state
	movl	$7, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3131:
	.size	uspoof_openCheckResult_67, .-uspoof_openCheckResult_67
	.p2align 4
	.globl	uspoof_closeCheckResult_67
	.type	uspoof_closeCheckResult_67, @function
uspoof_closeCheckResult_67:
.LFB3132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6711CheckResult12validateThisEP17USpoofCheckResultR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L176
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*8(%rax)
.L176:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L183
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L183:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3132:
	.size	uspoof_closeCheckResult_67, .-uspoof_closeCheckResult_67
	.p2align 4
	.globl	uspoof_getCheckResultChecks_67
	.type	uspoof_getCheckResultChecks_67, @function
uspoof_getCheckResultChecks_67:
.LFB3133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6711CheckResult12validateThisEPK17USpoofCheckResultR10UErrorCode@PLT
	movl	(%rbx), %edx
	xorl	%r8d, %r8d
	testl	%edx, %edx
	jg	.L184
	movl	12(%rax), %r8d
.L184:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3133:
	.size	uspoof_getCheckResultChecks_67, .-uspoof_getCheckResultChecks_67
	.p2align 4
	.globl	uspoof_getCheckResultRestrictionLevel_67
	.type	uspoof_getCheckResultRestrictionLevel_67, @function
uspoof_getCheckResultRestrictionLevel_67:
.LFB3134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6711CheckResult12validateThisEPK17USpoofCheckResultR10UErrorCode@PLT
	movl	(%rbx), %edx
	movl	$1610612736, %r8d
	testl	%edx, %edx
	jg	.L189
	movl	216(%rax), %r8d
.L189:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3134:
	.size	uspoof_getCheckResultRestrictionLevel_67, .-uspoof_getCheckResultRestrictionLevel_67
	.p2align 4
	.globl	uspoof_getCheckResultNumerics_67
	.type	uspoof_getCheckResultNumerics_67, @function
uspoof_getCheckResultNumerics_67:
.LFB3135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6711CheckResult12validateThisEPK17USpoofCheckResultR10UErrorCode@PLT
	movl	(%rbx), %edx
	addq	$16, %rax
	testl	%edx, %edx
	movl	$0, %edx
	cmovg	%rdx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3135:
	.size	uspoof_getCheckResultNumerics_67, .-uspoof_getCheckResultNumerics_67
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC0:
	.string	"["
	.string	"'"
	.string	"\\"
	.string	"-"
	.string	"."
	.string	"\\"
	.string	":"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"0"
	.string	"B"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"7"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"8"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"F"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"F"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"F"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"F"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"0"
	.string	"B"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"0"
	.string	"0"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"0"
	.string	"0"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"0"
	.string	"1"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"0"
	.string	"1"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"0"
	.string	"2"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"0"
	.string	"A"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"0"
	.string	"F"
	.string	"B"
	.string	"]"
	.string	""
	.string	""
	.align 8
.LC1:
	.string	"["
	.string	"0"
	.string	"-"
	.string	"9"
	.string	"A"
	.string	"-"
	.string	"Z"
	.string	"_"
	.string	"a"
	.string	"-"
	.string	"z"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"0"
	.string	"C"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"0"
	.string	"D"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"0"
	.string	"D"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"0"
	.string	"F"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"0"
	.string	"F"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"3"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"3"
	.string	"4"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"3"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"4"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"4"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"4"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"7"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"8"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"A"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"A"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"A"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"B"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"C"
	.string	"D"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"D"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"D"
	.string	"E"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"E"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"E"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"F"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"F"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"F"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"1"
	.string	"F"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"2"
	.string	"1"
	.string	"B"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"2"
	.string	"1"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"2"
	.string	"1"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"2"
	.string	"2"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"2"
	.string	"3"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"2"
	.string	"5"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"2"
	.string	"B"
	.string	"B"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"2"
	.string	"B"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"2"
	.string	"E"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"0"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"0"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"0"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"0"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"0"
	.string	"F"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"1"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"1"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"1"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"1"
	.string	"B"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"2"
	.string	"3"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"2"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"2"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"2"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"3"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"3"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"3"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"3"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"3"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"4"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"4"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"7"
	.string	"B"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"7"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"8"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"8"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"8"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"8"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"8"
	.string	"E"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"A"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"A"
	.string	"3"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"C"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"3"
	.string	"F"
	.string	"C"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"4"
	.string	"5"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"4"
	.string	"8"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"4"
	.string	"F"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"1"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"2"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"2"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"2"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"3"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"5"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"5"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"6"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"8"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"B"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"D"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"E"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"E"
	.string	"F"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"5"
	.string	"F"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"2"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"3"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"4"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"5"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"6"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"6"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"7"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"7"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"7"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"7"
	.string	"9"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"8"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"8"
	.string	"F"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"A"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"A"
	.string	"2"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"D"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"D"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"E"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"E"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"E"
	.string	"E"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"F"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"6"
	.string	"F"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"7"
	.string	"5"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"7"
	.string	"B"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"8"
	.string	"A"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"8"
	.string	"A"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"8"
	.string	"B"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"8"
	.string	"B"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"8"
	.string	"C"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"0"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"4"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"4"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"5"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"5"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"5"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"6"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"6"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"6"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"6"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"7"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"7"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"7"
	.string	"9"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"7"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"8"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"8"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"8"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"8"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"8"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"9"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"9"
	.string	"3"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"A"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"A"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"B"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"B"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"B"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"B"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"B"
	.string	"C"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"C"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"C"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"C"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"C"
	.string	"B"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"C"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"D"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"E"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"E"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"E"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"F"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"9"
	.string	"F"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"0"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"0"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"0"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"0"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"0"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"1"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"1"
	.string	"3"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"2"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"2"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"3"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"3"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"3"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"3"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"3"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"3"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"3"
	.string	"E"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"4"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"4"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"4"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"4"
	.string	"B"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"4"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"5"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"6"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"7"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"8"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"8"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"8"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"8"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"8"
	.string	"F"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"9"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"9"
	.string	"3"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"A"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"A"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"B"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"B"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"B"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"B"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"B"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"B"
	.string	"C"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"C"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"C"
	.string	"7"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"C"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"C"
	.string	"B"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"C"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"D"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"E"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"E"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"E"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"E"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"F"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"A"
	.string	"F"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"0"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"0"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"0"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"0"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"0"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"1"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"1"
	.string	"3"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"2"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"2"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"3"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"3"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"3"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"3"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"3"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"3"
	.string	"C"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"4"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"4"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"4"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"4"
	.string	"B"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"4"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"5"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"5"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"5"
	.string	"F"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"6"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"6"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"6"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"7"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"8"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"8"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"8"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"8"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"8"
	.string	"E"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"9"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"9"
	.string	"2"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"9"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"9"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"9"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"9"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"9"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"9"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"A"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"A"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"A"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"A"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"A"
	.string	"E"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"B"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"B"
	.string	"E"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"C"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"C"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"C"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"C"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"C"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"D"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"D"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"E"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"B"
	.string	"E"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"0"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"0"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"0"
	.string	"E"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"1"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"1"
	.string	"2"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"2"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"2"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"3"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"3"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"3"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"3"
	.string	"D"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"4"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"4"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"4"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"4"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"4"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"5"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"5"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"6"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"6"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"6"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"6"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"8"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"8"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"8"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"8"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"8"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"8"
	.string	"E"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"9"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"9"
	.string	"2"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"A"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"A"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"B"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"B"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"B"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"B"
	.string	"C"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"C"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"C"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"C"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"C"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"C"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"D"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"D"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"E"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"E"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"E"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"E"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"F"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"C"
	.string	"F"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"0"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"0"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"0"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"0"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"0"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"0"
	.string	"E"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"1"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"1"
	.string	"2"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"3"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"3"
	.string	"D"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"4"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"4"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"4"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"4"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"4"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"5"
	.string	"4"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"5"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"6"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"6"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"6"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"6"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"7"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"7"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"8"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"8"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"8"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"8"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"9"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"9"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"9"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"A"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"A"
	.string	"7"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"B"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"B"
	.string	"3"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"B"
	.string	"B"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"B"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"C"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"C"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"C"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"C"
	.string	"F"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"D"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"D"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"D"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"D"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"D"
	.string	"F"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"0"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"3"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"3"
	.string	"4"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"3"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"4"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"4"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"5"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"5"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"8"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"8"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"8"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"8"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"8"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"8"
	.string	"C"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"A"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"A"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"A"
	.string	"7"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"B"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"B"
	.string	"4"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"B"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"C"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"C"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"C"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"C"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"C"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"D"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"D"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"D"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"E"
	.string	"D"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"0"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"2"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"2"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"3"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"3"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"3"
	.string	"E"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"4"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"4"
	.string	"4"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"4"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"4"
	.string	"9"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"4"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"4"
	.string	"E"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"5"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"5"
	.string	"3"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"5"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"5"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"5"
	.string	"B"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"5"
	.string	"D"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"6"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"6"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"6"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"7"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"7"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"7"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"7"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"8"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"8"
	.string	"2"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"8"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"8"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"9"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"9"
	.string	"4"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"9"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"9"
	.string	"9"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"9"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"9"
	.string	"E"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"A"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"A"
	.string	"3"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"A"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"A"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"A"
	.string	"B"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"A"
	.string	"D"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"B"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"B"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"B"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"0"
	.string	"F"
	.string	"C"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"0"
	.string	"4"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"0"
	.string	"5"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"0"
	.string	"9"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"0"
	.string	"C"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"0"
	.string	"C"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"0"
	.string	"D"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"0"
	.string	"F"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"0"
	.string	"F"
	.string	"7"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"0"
	.string	"F"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"0"
	.string	"F"
	.string	"D"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"0"
	.string	"F"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"0"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"4"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"4"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"4"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"5"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"5"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"5"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"5"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"5"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"6"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"8"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"8"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"8"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"9"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"B"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"B"
	.string	"2"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"B"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"B"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"B"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"C"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"C"
	.string	"2"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"C"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"C"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"D"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"2"
	.string	"D"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"3"
	.string	"1"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"3"
	.string	"1"
	.string	"2"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"3"
	.string	"1"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"3"
	.string	"1"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"3"
	.string	"5"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"3"
	.string	"5"
	.string	"D"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"3"
	.string	"5"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"3"
	.string	"8"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"3"
	.string	"8"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"7"
	.string	"8"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"7"
	.string	"A"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"7"
	.string	"A"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"7"
	.string	"A"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"7"
	.string	"A"
	.string	"9"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"7"
	.string	"B"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"7"
	.string	"B"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"7"
	.string	"C"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"7"
	.string	"D"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"7"
	.string	"D"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"7"
	.string	"D"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"7"
	.string	"E"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"7"
	.string	"E"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"C"
	.string	"9"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"C"
	.string	"B"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"C"
	.string	"B"
	.string	"D"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"C"
	.string	"B"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"E"
	.string	"0"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"E"
	.string	"9"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"E"
	.string	"9"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"E"
	.string	"A"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"E"
	.string	"F"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"0"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"1"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"1"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"1"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"2"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"4"
	.string	"5"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"4"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"4"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"5"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"5"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"5"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"5"
	.string	"B"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"5"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"5"
	.string	"F"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"7"
	.string	"0"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"7"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"7"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"7"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"7"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"7"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"7"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"8"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"B"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"B"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"B"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"B"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"C"
	.string	"2"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"C"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"C"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"C"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"C"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"C"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"D"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"D"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"D"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"D"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"E"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"E"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"E"
	.string	"4"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"E"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"E"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"F"
	.string	"2"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"F"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"F"
	.string	"6"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"F"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"F"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"1"
	.string	"F"
	.string	"F"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"2"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"2"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"8"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"9"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"A"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"A"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"A"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"A"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"B"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"B"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"B"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"B"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"C"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"C"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"C"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"C"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"D"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"D"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"D"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"2"
	.string	"D"
	.string	"D"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"0"
	.string	"0"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"0"
	.string	"0"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"0"
	.string	"4"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"0"
	.string	"9"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"0"
	.string	"9"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"0"
	.string	"9"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"0"
	.string	"9"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"0"
	.string	"9"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"0"
	.string	"A"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"0"
	.string	"F"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"0"
	.string	"F"
	.string	"C"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"0"
	.string	"F"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"1"
	.string	"0"
	.string	"5"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"1"
	.string	"2"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"1"
	.string	"2"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"1"
	.string	"A"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"1"
	.string	"B"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"3"
	.string	"4"
	.string	"0"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"4"
	.string	"D"
	.string	"B"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"4"
	.string	"E"
	.string	"0"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"9"
	.string	"F"
	.string	"F"
	.string	"C"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"6"
	.string	"7"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"7"
	.string	"1"
	.string	"7"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"7"
	.string	"1"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"7"
	.string	"8"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"7"
	.string	"8"
	.string	"D"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"7"
	.string	"9"
	.string	"2"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"7"
	.string	"9"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"7"
	.string	"A"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"7"
	.string	"A"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"7"
	.string	"B"
	.string	"8"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"7"
	.string	"B"
	.string	"9"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"7"
	.string	"C"
	.string	"2"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"7"
	.string	"C"
	.string	"A"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"9"
	.string	"E"
	.string	"7"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"9"
	.string	"F"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"A"
	.string	"6"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"A"
	.string	"7"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"A"
	.string	"7"
	.string	"A"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"A"
	.string	"7"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"B"
	.string	"0"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"B"
	.string	"0"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"B"
	.string	"0"
	.string	"9"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"B"
	.string	"0"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"B"
	.string	"1"
	.string	"1"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"B"
	.string	"1"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"B"
	.string	"2"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"B"
	.string	"2"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"B"
	.string	"2"
	.string	"8"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"B"
	.string	"2"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"B"
	.string	"6"
	.string	"6"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"B"
	.string	"6"
	.string	"7"
	.string	"\\"
	.string	"u"
	.string	"A"
	.string	"C"
	.string	"0"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"D"
	.string	"7"
	.string	"A"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"F"
	.string	"A"
	.string	"0"
	.string	"E"
	.string	"\\"
	.string	"u"
	.string	"F"
	.string	"A"
	.string	"0"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"F"
	.string	"A"
	.string	"1"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"F"
	.string	"A"
	.string	"1"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"F"
	.string	"A"
	.string	"1"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"F"
	.string	"A"
	.string	"1"
	.string	"F"
	.string	"\\"
	.string	"u"
	.string	"F"
	.string	"A"
	.string	"2"
	.string	"1"
	.string	"\\"
	.string	"u"
	.string	"F"
	.string	"A"
	.string	"2"
	.string	"3"
	.string	"\\"
	.string	"u"
	.string	"F"
	.string	"A"
	.string	"2"
	.string	"4"
	.string	"\\"
	.string	"u"
	.string	"F"
	.string	"A"
	.string	"2"
	.string	"7"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"F"
	.string	"A"
	.string	"2"
	.string	"9"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"1"
	.string	"1"
	.string	"3"
	.string	"0"
	.string	"1"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"1"
	.string	"1"
	.string	"3"
	.string	"0"
	.string	"3"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"1"
	.string	"1"
	.string	"3"
	.string	"3"
	.string	"B"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"1"
	.string	"1"
	.string	"3"
	.string	"3"
	.string	"C"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"1"
	.string	"6"
	.string	"F"
	.string	"F"
	.string	"0"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"1"
	.string	"6"
	.string	"F"
	.string	"F"
	.string	"1"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"1"
	.string	"B"
	.string	"1"
	.string	"5"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"1"
	.string	"B"
	.string	"1"
	.string	"5"
	.string	"2"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"1"
	.string	"B"
	.string	"1"
	.string	"6"
	.string	"4"
	.string	"-"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"1"
	.string	"B"
	.string	"1"
	.string	"6"
	.string	"7"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"2"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"2"
	.string	"A"
	.string	"6"
	.string	"D"
	.string	"D"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"2"
	.string	"A"
	.string	"7"
	.string	"0"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"2"
	.string	"B"
	.string	"7"
	.string	"3"
	.string	"4"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"2"
	.string	"B"
	.string	"7"
	.string	"4"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"2"
	.string	"B"
	.string	"8"
	.string	"1"
	.string	"D"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"2"
	.string	"B"
	.string	"8"
	.string	"2"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"2"
	.string	"C"
	.string	"E"
	.string	"A"
	.string	"1"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"2"
	.string	"C"
	.string	"E"
	.string	"B"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"2"
	.string	"E"
	.string	"B"
	.string	"E"
	.string	"0"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"3"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"U"
	.string	"0"
	.string	"0"
	.string	"0"
	.string	"3"
	.string	"1"
	.string	"3"
	.string	"4"
	.string	"A"
	.string	"]"
	.string	""
	.string	""
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_117initializeStaticsER10UErrorCode, @function
_ZN12_GLOBAL__N_117initializeStaticsER10UErrorCode:
.LFB3097:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$88, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L199
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
.L199:
	movq	%r12, %rdi
	movq	%rbx, _ZL13gInclusionSet(%rip)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	_ZL13gInclusionSet(%rip), %rdi
	testq	%rdi, %rdi
	je	.L215
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L202
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
.L202:
	movq	%r12, %rdi
	movq	%rbx, _ZL15gRecommendedSet(%rip)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	_ZL15gRecommendedSet(%rip), %rdi
	testq	%rdi, %rdi
	je	.L216
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6711Normalizer214getNFDInstanceER10UErrorCode@PLT
	leaq	_ZN12_GLOBAL__N_114uspoof_cleanupEv(%rip), %rsi
	movl	$3, %edi
	movq	%rax, _ZL14gNfdNormalizer(%rip)
	call	ucln_i18n_registerCleanup_67@PLT
.L198:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L217
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movq	_ZL13gInclusionSet(%rip), %rdi
	movl	$7, 0(%r13)
	testq	%rdi, %rdi
	je	.L198
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L215:
	movl	$7, 0(%r13)
	jmp	.L198
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3097:
	.size	_ZN12_GLOBAL__N_117initializeStaticsER10UErrorCode, .-_ZN12_GLOBAL__N_117initializeStaticsER10UErrorCode
	.p2align 4
	.globl	uspoof_internalInitStatics_67
	.type	uspoof_internalInitStatics_67, @function
uspoof_internalInitStatics_67:
.LFB3098:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L233
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZL21gSpoofInitStaticsOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L234
.L221:
	movl	4+_ZL21gSpoofInitStaticsOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L218
	movl	%eax, (%rbx)
.L218:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	leaq	_ZL21gSpoofInitStaticsOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L221
	movq	%rbx, %rdi
	call	_ZN12_GLOBAL__N_117initializeStaticsER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL21gSpoofInitStaticsOnce(%rip), %rdi
	movl	%eax, 4+_ZL21gSpoofInitStaticsOnce(%rip)
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	.cfi_endproc
.LFE3098:
	.size	uspoof_internalInitStatics_67, .-uspoof_internalInitStatics_67
	.p2align 4
	.globl	uspoof_getInclusionSet_67
	.type	uspoof_getInclusionSet_67, @function
uspoof_getInclusionSet_67:
.LFB3127:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L250
	movq	_ZL13gInclusionSet(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZL21gSpoofInitStaticsOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L251
.L238:
	movl	4+_ZL21gSpoofInitStaticsOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L237
	movl	%eax, (%rbx)
.L237:
	movq	_ZL13gInclusionSet(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	leaq	_ZL21gSpoofInitStaticsOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L238
	movq	%rbx, %rdi
	call	_ZN12_GLOBAL__N_117initializeStaticsER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL21gSpoofInitStaticsOnce(%rip), %rdi
	movl	%eax, 4+_ZL21gSpoofInitStaticsOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L237
	.cfi_endproc
.LFE3127:
	.size	uspoof_getInclusionSet_67, .-uspoof_getInclusionSet_67
	.p2align 4
	.globl	uspoof_getRecommendedSet_67
	.type	uspoof_getRecommendedSet_67, @function
uspoof_getRecommendedSet_67:
.LFB3128:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L267
	movq	_ZL15gRecommendedSet(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZL21gSpoofInitStaticsOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L268
.L255:
	movl	4+_ZL21gSpoofInitStaticsOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L254
	movl	%eax, (%rbx)
.L254:
	movq	_ZL15gRecommendedSet(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	leaq	_ZL21gSpoofInitStaticsOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L255
	movq	%rbx, %rdi
	call	_ZN12_GLOBAL__N_117initializeStaticsER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL21gSpoofInitStaticsOnce(%rip), %rdi
	movl	%eax, 4+_ZL21gSpoofInitStaticsOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L254
	.cfi_endproc
.LFE3128:
	.size	uspoof_getRecommendedSet_67, .-uspoof_getRecommendedSet_67
	.p2align 4
	.globl	uspoof_getInclusionUnicodeSet_67
	.type	uspoof_getInclusionUnicodeSet_67, @function
uspoof_getInclusionUnicodeSet_67:
.LFB3129:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L284
	movq	_ZL13gInclusionSet(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZL21gSpoofInitStaticsOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L285
.L272:
	movl	4+_ZL21gSpoofInitStaticsOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L271
	movl	%eax, (%rbx)
.L271:
	movq	_ZL13gInclusionSet(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	leaq	_ZL21gSpoofInitStaticsOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L272
	movq	%rbx, %rdi
	call	_ZN12_GLOBAL__N_117initializeStaticsER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL21gSpoofInitStaticsOnce(%rip), %rdi
	movl	%eax, 4+_ZL21gSpoofInitStaticsOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L271
	.cfi_endproc
.LFE3129:
	.size	uspoof_getInclusionUnicodeSet_67, .-uspoof_getInclusionUnicodeSet_67
	.p2align 4
	.globl	uspoof_getRecommendedUnicodeSet_67
	.type	uspoof_getRecommendedUnicodeSet_67, @function
uspoof_getRecommendedUnicodeSet_67:
.LFB3130:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L301
	movq	_ZL15gRecommendedSet(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZL21gSpoofInitStaticsOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L302
.L289:
	movl	4+_ZL21gSpoofInitStaticsOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L288
	movl	%eax, (%rbx)
.L288:
	movq	_ZL15gRecommendedSet(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	leaq	_ZL21gSpoofInitStaticsOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L289
	movq	%rbx, %rdi
	call	_ZN12_GLOBAL__N_117initializeStaticsER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL21gSpoofInitStaticsOnce(%rip), %rdi
	movl	%eax, 4+_ZL21gSpoofInitStaticsOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L288
	.cfi_endproc
.LFE3130:
	.size	uspoof_getRecommendedUnicodeSet_67, .-uspoof_getRecommendedUnicodeSet_67
	.p2align 4
	.globl	uspoof_open_67
	.type	uspoof_open_67, @function
uspoof_open_67:
.LFB3099:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L320
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	_ZL21gSpoofInitStaticsOnce(%rip), %eax
	movq	%rdi, %rbx
	cmpl	$2, %eax
	jne	.L321
.L305:
	movl	4+_ZL21gSpoofInitStaticsOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L306
	movl	%eax, (%rbx)
.L303:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	leaq	_ZL21gSpoofInitStaticsOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L305
	movq	%rbx, %rdi
	call	_ZN12_GLOBAL__N_117initializeStaticsER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL21gSpoofInitStaticsOnce(%rip), %rdi
	movl	%eax, 4+_ZL21gSpoofInitStaticsOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L306:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L303
	movl	$48, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L307
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_679SpoofImplC1ER10UErrorCode@PLT
	movl	(%rbx), %eax
	movq	%r12, %rdi
	testl	%eax, %eax
	jg	.L322
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679SpoofImpl15asUSpoofCheckerEv@PLT
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	call	_ZN6icu_679SpoofImplD0Ev@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L307:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L303
	.cfi_endproc
.LFE3099:
	.size	uspoof_open_67, .-uspoof_open_67
	.p2align 4
	.globl	uspoof_openFromSerialized_67
	.type	uspoof_openFromSerialized_67, @function
uspoof_openFromSerialized_67:
.LFB3100:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L345
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L349
	movl	_ZL21gSpoofInitStaticsOnce(%rip), %eax
	movl	%esi, %r15d
	movq	%rdx, %r13
	cmpl	$2, %eax
	jne	.L350
.L327:
	movl	4+_ZL21gSpoofInitStaticsOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L328
	movl	%eax, (%rbx)
.L323:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	leaq	_ZL21gSpoofInitStaticsOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L327
	movq	%rbx, %rdi
	call	_ZN12_GLOBAL__N_117initializeStaticsER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL21gSpoofInitStaticsOnce(%rip), %rdi
	movl	%eax, 4+_ZL21gSpoofInitStaticsOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L328:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L323
	movl	$56, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L330
	movl	%r15d, %edx
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679SpoofDataC1EPKviR10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L348
	movl	$48, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L332
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_679SpoofImplC1EPNS_9SpoofDataER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L351
	testq	%r13, %r13
	je	.L334
	movq	%r12, %rdi
	call	_ZNK6icu_679SpoofData4sizeEv@PLT
	movl	%eax, 0(%r13)
.L334:
	addq	$8, %rsp
	movq	%r14, %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679SpoofImpl15asUSpoofCheckerEv@PLT
	.p2align 4,,10
	.p2align 3
.L345:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1, (%rcx)
	jmp	.L323
.L332:
	movl	$7, (%rbx)
	.p2align 4,,10
	.p2align 3
.L348:
	movq	%r12, %rdi
	call	_ZN6icu_679SpoofDataD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L351:
	movq	%r14, %rdi
	call	_ZN6icu_679SpoofImplD0Ev@PLT
	jmp	.L323
.L330:
	movl	$7, (%rbx)
	jmp	.L323
	.cfi_endproc
.LFE3100:
	.size	uspoof_openFromSerialized_67, .-uspoof_openFromSerialized_67
	.p2align 4
	.type	_ZN12_GLOBAL__N_114uspoof_cleanupEv, @function
_ZN12_GLOBAL__N_114uspoof_cleanupEv:
.LFB3096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZL13gInclusionSet(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L353
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L353:
	movq	_ZL15gRecommendedSet(%rip), %rdi
	movq	$0, _ZL13gInclusionSet(%rip)
	testq	%rdi, %rdi
	je	.L354
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L354:
	movq	$0, _ZL15gRecommendedSet(%rip)
	movl	$1, %eax
	movq	$0, _ZL14gNfdNormalizer(%rip)
	movl	$0, _ZL21gSpoofInitStaticsOnce(%rip)
	mfence
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3096:
	.size	_ZN12_GLOBAL__N_114uspoof_cleanupEv, .-_ZN12_GLOBAL__N_114uspoof_cleanupEv
	.p2align 4
	.globl	uspoof_setAllowedUnicodeSet_67
	.type	uspoof_setAllowedUnicodeSet_67, @function
uspoof_setAllowedUnicodeSet_67:
.LFB3112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN6icu_679SpoofImpl12validateThisEP13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L362
	testb	$1, 32(%r13)
	je	.L364
	movl	$1, (%rbx)
.L362:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK6icu_6710UnicodeSet5cloneEv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L365
	testb	$1, 32(%rax)
	je	.L366
.L365:
	movl	$7, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L367
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L367:
	orl	$64, 12(%r12)
	movq	%r13, 24(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3112:
	.size	uspoof_setAllowedUnicodeSet_67, .-uspoof_setAllowedUnicodeSet_67
	.p2align 4
	.globl	uspoof_setAllowedChars_67
	.type	uspoof_setAllowedChars_67, @function
uspoof_setAllowedChars_67:
.LFB3111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN6icu_679SpoofImpl12validateThisEP13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L378
	testb	$1, 32(%r13)
	je	.L380
	movl	$1, (%rbx)
.L378:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK6icu_6710UnicodeSet5cloneEv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L381
	testb	$1, 32(%rax)
	je	.L382
.L381:
	movl	$7, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	.cfi_restore_state
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L383
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L383:
	orl	$64, 12(%r12)
	movq	%r13, 24(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3111:
	.size	uspoof_setAllowedChars_67, .-uspoof_setAllowedChars_67
	.p2align 4
	.type	_ZN12_GLOBAL__N_19checkImplEPKN6icu_679SpoofImplERKNS0_13UnicodeStringEPNS0_11CheckResultEP10UErrorCode, @function
_ZN12_GLOBAL__N_19checkImplEPKN6icu_679SpoofImplERKNS0_13UnicodeStringEPNS0_11CheckResultEP10UErrorCode:
.LFB3121:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -352(%rbp)
	movq	%rcx, -328(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711CheckResult5clearEv@PLT
	movl	12(%r12), %esi
	movl	%esi, %eax
	andl	$16, %eax
	movl	%eax, -340(%rbp)
	jne	.L449
.L395:
	testb	$-128, %sil
	jne	.L450
.L397:
	testl	$256, %esi
	jne	.L451
.L399:
	testb	$64, %sil
	je	.L401
	movswl	8(%r14), %ebx
	testw	%bx, %bx
	js	.L402
	sarl	$5, %ebx
.L403:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L408:
	cmpl	%ebx, %r13d
	jge	.L448
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %esi
	cmpl	$65535, %eax
	jbe	.L405
	movq	24(%r12), %rdi
	addl	$2, %r13d
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L408
.L406:
	orl	$64, -340(%rbp)
.L448:
	movl	12(%r12), %esi
.L401:
	testb	$32, %sil
	je	.L409
	movq	_ZL14gNfdNormalizer(%rip), %rdi
	movl	$2, %ecx
	movq	%r14, %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -320(%rbp)
	leaq	-320(%rbp), %r15
	movq	(%rdi), %rax
	movw	%cx, -312(%rbp)
	movq	%r15, %rdx
	movq	-328(%rbp), %rcx
	call	*24(%rax)
	movswl	-312(%rbp), %eax
	testw	%ax, %ax
	js	.L410
	sarl	$5, %eax
	movl	%eax, -336(%rbp)
.L411:
	leaq	-256(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	-336(%rbp), %edx
	testl	%edx, %edx
	jle	.L412
	movb	$0, -328(%rbp)
	xorl	%ebx, %ebx
	movl	$0, -332(%rbp)
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L453:
	cmpb	$0, -328(%rbp)
	movl	$0, -332(%rbp)
	jne	.L452
.L416:
	cmpl	-336(%rbp), %ebx
	jge	.L412
.L413:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r14d
	xorl	%eax, %eax
	cmpl	$65535, %r14d
	movl	%r14d, %edi
	seta	%al
	leal	1(%rax,%rbx), %ebx
	call	u_charType_67@PLT
	cmpb	$6, %al
	jne	.L453
	movl	-332(%rbp), %eax
	testl	%eax, %eax
	je	.L423
	cmpb	$0, -328(%rbp)
	je	.L454
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L455
.L418:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movb	$1, -328(%rbp)
	cmpl	-336(%rbp), %ebx
	jl	.L413
.L412:
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	12(%r12), %esi
.L409:
	movq	-352(%rbp), %rax
	movl	-340(%rbp), %ecx
	movl	%ecx, 12(%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6711CheckResult17toCombinedBitmaskEi@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L456
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	movl	-308(%rbp), %eax
	movl	%eax, -336(%rbp)
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L402:
	movl	12(%r14), %ebx
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L405:
	movq	24(%r12), %rdi
	addl	$1, %r13d
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L408
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L423:
	movl	%r14d, -332(%rbp)
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L452:
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	movb	$0, -328(%rbp)
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L454:
	movl	-332(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L418
.L455:
	orl	$32, -340(%rbp)
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L451:
	movq	-328(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_679SpoofImpl17findHiddenOverlayERKNS_13UnicodeStringER10UErrorCode@PLT
	cmpl	$-1, %eax
	je	.L447
	orl	$256, -340(%rbp)
.L447:
	movl	12(%r12), %esi
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L450:
	leaq	-256(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-328(%rbp), %rcx
	call	_ZNK6icu_679SpoofImpl11getNumericsERKNS_13UnicodeStringERNS_10UnicodeSetER10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet4sizeEv@PLT
	movl	-340(%rbp), %ecx
	movq	%r13, %rsi
	movl	%eax, %r8d
	movl	%ecx, %eax
	orb	$-128, %al
	cmpl	$1, %r8d
	cmovle	%ecx, %eax
	movl	%eax, -340(%rbp)
	movq	-352(%rbp), %rax
	leaq	16(%rax), %rdi
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movl	12(%r12), %esi
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L449:
	movq	-328(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_679SpoofImpl19getRestrictionLevelERKNS_13UnicodeStringER10UErrorCode@PLT
	xorl	%edx, %edx
	cmpl	%eax, 40(%r12)
	movq	-352(%rbp), %rcx
	setl	%dl
	movl	12(%r12), %esi
	sall	$4, %edx
	movl	%eax, 216(%rcx)
	movl	%edx, -340(%rbp)
	jmp	.L395
.L456:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3121:
	.size	_ZN12_GLOBAL__N_19checkImplEPKN6icu_679SpoofImplERKNS0_13UnicodeStringEPNS0_11CheckResultEP10UErrorCode, .-_ZN12_GLOBAL__N_19checkImplEPKN6icu_679SpoofImplERKNS0_13UnicodeStringEPNS0_11CheckResultEP10UErrorCode
	.p2align 4
	.globl	uspoof_check2UnicodeString_67
	.type	uspoof_check2UnicodeString_67, @function
uspoof_check2UnicodeString_67:
.LFB3122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rcx, %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L461
	movq	%rax, %r13
	testq	%r15, %r15
	je	.L460
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711CheckResult12validateThisEP17USpoofCheckResultR10UErrorCode@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L461
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12_GLOBAL__N_19checkImplEPKN6icu_679SpoofImplERKNS0_13UnicodeStringEPNS0_11CheckResultEP10UErrorCode
.L457:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L467
	addq	$256, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L460:
	leaq	-272(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_6711CheckResultC1Ev@PLT
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12_GLOBAL__N_19checkImplEPKN6icu_679SpoofImplERKNS0_13UnicodeStringEPNS0_11CheckResultEP10UErrorCode
	movq	%r15, %rdi
	movl	%eax, -276(%rbp)
	call	_ZN6icu_6711CheckResultD1Ev@PLT
	movl	-276(%rbp), %eax
	jmp	.L457
.L467:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3122:
	.size	uspoof_check2UnicodeString_67, .-uspoof_check2UnicodeString_67
	.p2align 4
	.globl	uspoof_checkUnicodeString_67
	.type	uspoof_checkUnicodeString_67, @function
uspoof_checkUnicodeString_67:
.LFB3120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	subq	$240, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L469
	movl	$0, (%rdx)
.L469:
	movq	%r13, %rsi
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L472
	leaq	-272(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_6711CheckResultC1Ev@PLT
	movq	%r12, %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN12_GLOBAL__N_19checkImplEPKN6icu_679SpoofImplERKNS0_13UnicodeStringEPNS0_11CheckResultEP10UErrorCode
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6711CheckResultD1Ev@PLT
.L468:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L477
	addq	$240, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L468
.L477:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3120:
	.size	uspoof_checkUnicodeString_67, .-uspoof_checkUnicodeString_67
	.p2align 4
	.globl	uspoof_checkUTF8_67
	.type	uspoof_checkUTF8_67, @function
uspoof_checkUTF8_67:
.LFB3115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L479
	movl	$0, (%rcx)
.L479:
	movl	(%r12), %eax
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jg	.L478
	testl	%edx, %edx
	js	.L490
.L481:
	leaq	-352(%rbp), %rbx
	movl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L485
	leaq	-288(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6711CheckResultC1Ev@PLT
	movq	%r13, %rdi
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	_ZN12_GLOBAL__N_19checkImplEPKN6icu_679SpoofImplERKNS0_13UnicodeStringEPNS0_11CheckResultEP10UErrorCode
	movq	%r14, %rdi
	movl	%eax, %r15d
	call	_ZN6icu_6711CheckResultD1Ev@PLT
.L482:
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L478:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L491
	addq	$328, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	movq	%rsi, %rdi
	movq	%rsi, -360(%rbp)
	call	strlen@PLT
	movq	-360(%rbp), %rsi
	movl	%eax, %edx
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L485:
	xorl	%r15d, %r15d
	jmp	.L482
.L491:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3115:
	.size	uspoof_checkUTF8_67, .-uspoof_checkUTF8_67
	.p2align 4
	.globl	uspoof_check_67
	.type	uspoof_check_67, @function
uspoof_check_67:
.LFB3113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$328, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L493
	movl	$0, (%rcx)
.L493:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L498
	cmpl	$-1, %r13d
	jl	.L504
	sete	%sil
	leaq	-352(%rbp), %r15
	movl	%r13d, %ecx
	movq	%rbx, -360(%rbp)
	leaq	-360(%rbp), %rdx
	movzbl	%sil, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L499
	leaq	-288(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6711CheckResultC1Ev@PLT
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN12_GLOBAL__N_19checkImplEPKN6icu_679SpoofImplERKNS0_13UnicodeStringEPNS0_11CheckResultEP10UErrorCode
	movq	%r14, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6711CheckResultD1Ev@PLT
.L496:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L492:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L505
	addq	$328, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	.cfi_restore_state
	movl	$1, (%r12)
	xorl	%r12d, %r12d
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L498:
	xorl	%r12d, %r12d
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L499:
	xorl	%r12d, %r12d
	jmp	.L496
.L505:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3113:
	.size	uspoof_check_67, .-uspoof_check_67
	.p2align 4
	.globl	uspoof_check2UTF8_67
	.type	uspoof_check2UTF8_67, @function
uspoof_check2UTF8_67:
.LFB3116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$328, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L506
	movq	%rdi, %r13
	movq	%rcx, %r14
	movq	%r8, %r12
	testl	%edx, %edx
	js	.L520
.L508:
	leaq	-352(%rbp), %r15
	movl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L512
	testq	%r14, %r14
	je	.L511
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711CheckResult12validateThisEP17USpoofCheckResultR10UErrorCode@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L512
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN12_GLOBAL__N_19checkImplEPKN6icu_679SpoofImplERKNS0_13UnicodeStringEPNS0_11CheckResultEP10UErrorCode
	movl	%eax, %ebx
.L510:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L506:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L521
	addq	$328, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	.cfi_restore_state
	movq	%rsi, %rdi
	movq	%rsi, -360(%rbp)
	call	strlen@PLT
	movq	-360(%rbp), %rsi
	movl	%eax, %edx
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L512:
	xorl	%ebx, %ebx
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L511:
	leaq	-288(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6711CheckResultC1Ev@PLT
	movq	%r13, %rdi
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN12_GLOBAL__N_19checkImplEPKN6icu_679SpoofImplERKNS0_13UnicodeStringEPNS0_11CheckResultEP10UErrorCode
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	_ZN6icu_6711CheckResultD1Ev@PLT
	jmp	.L510
.L521:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3116:
	.size	uspoof_check2UTF8_67, .-uspoof_check2UTF8_67
	.p2align 4
	.globl	uspoof_check2_67
	.type	uspoof_check2_67, @function
uspoof_check2_67:
.LFB3114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%r8, %rsi
	subq	$344, %rsp
	movq	%rcx, -376(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L530
	cmpl	$-1, %r13d
	jl	.L535
	sete	%sil
	leaq	-352(%rbp), %r15
	movl	%r13d, %ecx
	movq	%rbx, -360(%rbp)
	leaq	-360(%rbp), %rdx
	movzbl	%sil, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_679SpoofImpl12validateThisEPK13USpoofCheckerR10UErrorCode@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L528
	movq	-376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L527
	movq	%r12, %rsi
	call	_ZN6icu_6711CheckResult12validateThisEP17USpoofCheckResultR10UErrorCode@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L528
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN12_GLOBAL__N_19checkImplEPKN6icu_679SpoofImplERKNS0_13UnicodeStringEPNS0_11CheckResultEP10UErrorCode
	movl	%eax, %r12d
.L526:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L535:
	movl	$1, (%r12)
	xorl	%r12d, %r12d
.L522:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L536
	addq	$344, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L530:
	xorl	%r12d, %r12d
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L527:
	leaq	-288(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6711CheckResultC1Ev@PLT
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN12_GLOBAL__N_19checkImplEPKN6icu_679SpoofImplERKNS0_13UnicodeStringEPNS0_11CheckResultEP10UErrorCode
	movq	%r14, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6711CheckResultD1Ev@PLT
	jmp	.L526
.L536:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3114:
	.size	uspoof_check2_67, .-uspoof_check2_67
	.local	_ZL21gSpoofInitStaticsOnce
	.comm	_ZL21gSpoofInitStaticsOnce,8,8
	.local	_ZL14gNfdNormalizer
	.comm	_ZL14gNfdNormalizer,8,8
	.local	_ZL15gRecommendedSet
	.comm	_ZL15gRecommendedSet,8,8
	.local	_ZL13gInclusionSet
	.comm	_ZL13gInclusionSet,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
