	.file	"nounit.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NoUnit17getDynamicClassIDEv
	.type	_ZNK6icu_676NoUnit17getDynamicClassIDEv, @function
_ZNK6icu_676NoUnit17getDynamicClassIDEv:
.LFB2198:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_676NoUnit16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2198:
	.size	_ZNK6icu_676NoUnit17getDynamicClassIDEv, .-_ZNK6icu_676NoUnit17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676NoUnit5cloneEv
	.type	_ZNK6icu_676NoUnit5cloneEv, @function
_ZNK6icu_676NoUnit5cloneEv:
.LFB2208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711MeasureUnitC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_676NoUnitE(%rip), %rax
	movq	%rax, (%r12)
.L3:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2208:
	.size	_ZNK6icu_676NoUnit5cloneEv, .-_ZNK6icu_676NoUnit5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NoUnitD2Ev
	.type	_ZN6icu_676NoUnitD2Ev, @function
_ZN6icu_676NoUnitD2Ev:
.LFB2210:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676NoUnitE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6711MeasureUnitD2Ev@PLT
	.cfi_endproc
.LFE2210:
	.size	_ZN6icu_676NoUnitD2Ev, .-_ZN6icu_676NoUnitD2Ev
	.globl	_ZN6icu_676NoUnitD1Ev
	.set	_ZN6icu_676NoUnitD1Ev,_ZN6icu_676NoUnitD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NoUnitD0Ev
	.type	_ZN6icu_676NoUnitD0Ev, @function
_ZN6icu_676NoUnitD0Ev:
.LFB2212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676NoUnitE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6711MeasureUnitD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2212:
	.size	_ZN6icu_676NoUnitD0Ev, .-_ZN6icu_676NoUnitD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NoUnit16getStaticClassIDEv
	.type	_ZN6icu_676NoUnit16getStaticClassIDEv, @function
_ZN6icu_676NoUnit16getStaticClassIDEv:
.LFB2197:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_676NoUnit16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2197:
	.size	_ZN6icu_676NoUnit16getStaticClassIDEv, .-_ZN6icu_676NoUnit16getStaticClassIDEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NoUnit4baseEv
	.type	_ZN6icu_676NoUnit4baseEv, @function
_ZN6icu_676NoUnit4baseEv:
.LFB2199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6711MeasureUnitC2Ev@PLT
	leaq	16+_ZTVN6icu_676NoUnitE(%rip), %rax
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%rax, (%r12)
	call	_ZN6icu_6711MeasureUnit10initNoUnitEPKc@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2199:
	.size	_ZN6icu_676NoUnit4baseEv, .-_ZN6icu_676NoUnit4baseEv
	.section	.rodata.str1.1
.LC1:
	.string	"percent"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NoUnit7percentEv
	.type	_ZN6icu_676NoUnit7percentEv, @function
_ZN6icu_676NoUnit7percentEv:
.LFB2200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6711MeasureUnitC2Ev@PLT
	leaq	16+_ZTVN6icu_676NoUnitE(%rip), %rax
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, (%r12)
	call	_ZN6icu_6711MeasureUnit10initNoUnitEPKc@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2200:
	.size	_ZN6icu_676NoUnit7percentEv, .-_ZN6icu_676NoUnit7percentEv
	.section	.rodata.str1.1
.LC2:
	.string	"permille"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NoUnit8permilleEv
	.type	_ZN6icu_676NoUnit8permilleEv, @function
_ZN6icu_676NoUnit8permilleEv:
.LFB2201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6711MeasureUnitC2Ev@PLT
	leaq	16+_ZTVN6icu_676NoUnitE(%rip), %rax
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, (%r12)
	call	_ZN6icu_6711MeasureUnit10initNoUnitEPKc@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2201:
	.size	_ZN6icu_676NoUnit8permilleEv, .-_ZN6icu_676NoUnit8permilleEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NoUnitC2EPKc
	.type	_ZN6icu_676NoUnitC2EPKc, @function
_ZN6icu_676NoUnitC2EPKc:
.LFB2203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_6711MeasureUnitC2Ev@PLT
	leaq	16+_ZTVN6icu_676NoUnitE(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, (%r12)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711MeasureUnit10initNoUnitEPKc@PLT
	.cfi_endproc
.LFE2203:
	.size	_ZN6icu_676NoUnitC2EPKc, .-_ZN6icu_676NoUnitC2EPKc
	.globl	_ZN6icu_676NoUnitC1EPKc
	.set	_ZN6icu_676NoUnitC1EPKc,_ZN6icu_676NoUnitC2EPKc
	.align 2
	.p2align 4
	.globl	_ZN6icu_676NoUnitC2ERKS0_
	.type	_ZN6icu_676NoUnitC2ERKS0_, @function
_ZN6icu_676NoUnitC2ERKS0_:
.LFB2206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6711MeasureUnitC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_676NoUnitE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2206:
	.size	_ZN6icu_676NoUnitC2ERKS0_, .-_ZN6icu_676NoUnitC2ERKS0_
	.globl	_ZN6icu_676NoUnitC1ERKS0_
	.set	_ZN6icu_676NoUnitC1ERKS0_,_ZN6icu_676NoUnitC2ERKS0_
	.weak	_ZTSN6icu_676NoUnitE
	.section	.rodata._ZTSN6icu_676NoUnitE,"aG",@progbits,_ZTSN6icu_676NoUnitE,comdat
	.align 16
	.type	_ZTSN6icu_676NoUnitE, @object
	.size	_ZTSN6icu_676NoUnitE, 17
_ZTSN6icu_676NoUnitE:
	.string	"N6icu_676NoUnitE"
	.weak	_ZTIN6icu_676NoUnitE
	.section	.data.rel.ro._ZTIN6icu_676NoUnitE,"awG",@progbits,_ZTIN6icu_676NoUnitE,comdat
	.align 8
	.type	_ZTIN6icu_676NoUnitE, @object
	.size	_ZTIN6icu_676NoUnitE, 24
_ZTIN6icu_676NoUnitE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676NoUnitE
	.quad	_ZTIN6icu_6711MeasureUnitE
	.weak	_ZTVN6icu_676NoUnitE
	.section	.data.rel.ro._ZTVN6icu_676NoUnitE,"awG",@progbits,_ZTVN6icu_676NoUnitE,comdat
	.align 8
	.type	_ZTVN6icu_676NoUnitE, @object
	.size	_ZTVN6icu_676NoUnitE, 56
_ZTVN6icu_676NoUnitE:
	.quad	0
	.quad	_ZTIN6icu_676NoUnitE
	.quad	_ZN6icu_676NoUnitD1Ev
	.quad	_ZN6icu_676NoUnitD0Ev
	.quad	_ZNK6icu_676NoUnit17getDynamicClassIDEv
	.quad	_ZNK6icu_676NoUnit5cloneEv
	.quad	_ZNK6icu_6711MeasureUniteqERKNS_7UObjectE
	.local	_ZZN6icu_676NoUnit16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_676NoUnit16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
