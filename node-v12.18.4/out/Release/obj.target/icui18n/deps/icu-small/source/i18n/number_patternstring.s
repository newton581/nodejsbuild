	.file	"number_patternstring.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl17ParsedPatternInfo6lengthEi
	.type	_ZNK6icu_676number4impl17ParsedPatternInfo6lengthEi, @function
_ZNK6icu_676number4impl17ParsedPatternInfo6lengthEi:
.LFB3629:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	movl	%esi, %edx
	andl	$512, %ecx
	setne	%al
	andl	$1024, %edx
	je	.L4
	testb	%al, %al
	je	.L2
	addq	$400, %rdi
.L3:
	movl	4(%rdi), %eax
	subl	(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testl	%edx, %edx
	je	.L4
	addq	$232, %rdi
	movl	4(%rdi), %eax
	subl	(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	andl	$256, %esi
	je	.L6
	testb	%al, %al
	je	.L5
	addq	$384, %rdi
	movl	4(%rdi), %eax
	subl	(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	testl	%esi, %esi
	je	.L6
	addq	$216, %rdi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	392(%rdi), %rax
	addq	$224, %rdi
	testl	%ecx, %ecx
	cmovne	%rax, %rdi
	movl	4(%rdi), %eax
	subl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE3629:
	.size	_ZNK6icu_676number4impl17ParsedPatternInfo6lengthEi, .-_ZNK6icu_676number4impl17ParsedPatternInfo6lengthEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl17ParsedPatternInfo19positiveHasPlusSignEv
	.type	_ZNK6icu_676number4impl17ParsedPatternInfo19positiveHasPlusSignEv, @function
_ZNK6icu_676number4impl17ParsedPatternInfo19positiveHasPlusSignEv:
.LFB3633:
	.cfi_startproc
	endbr64
	movzbl	212(%rdi), %eax
	ret
	.cfi_endproc
.LFE3633:
	.size	_ZNK6icu_676number4impl17ParsedPatternInfo19positiveHasPlusSignEv, .-_ZNK6icu_676number4impl17ParsedPatternInfo19positiveHasPlusSignEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl17ParsedPatternInfo21hasNegativeSubpatternEv
	.type	_ZNK6icu_676number4impl17ParsedPatternInfo21hasNegativeSubpatternEv, @function
_ZNK6icu_676number4impl17ParsedPatternInfo21hasNegativeSubpatternEv:
.LFB3634:
	.cfi_startproc
	endbr64
	movzbl	432(%rdi), %eax
	ret
	.cfi_endproc
.LFE3634:
	.size	_ZNK6icu_676number4impl17ParsedPatternInfo21hasNegativeSubpatternEv, .-_ZNK6icu_676number4impl17ParsedPatternInfo21hasNegativeSubpatternEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl17ParsedPatternInfo20negativeHasMinusSignEv
	.type	_ZNK6icu_676number4impl17ParsedPatternInfo20negativeHasMinusSignEv, @function
_ZNK6icu_676number4impl17ParsedPatternInfo20negativeHasMinusSignEv:
.LFB3635:
	.cfi_startproc
	endbr64
	movzbl	379(%rdi), %eax
	ret
	.cfi_endproc
.LFE3635:
	.size	_ZNK6icu_676number4impl17ParsedPatternInfo20negativeHasMinusSignEv, .-_ZNK6icu_676number4impl17ParsedPatternInfo20negativeHasMinusSignEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl17ParsedPatternInfo7hasBodyEv
	.type	_ZNK6icu_676number4impl17ParsedPatternInfo7hasBodyEv, @function
_ZNK6icu_676number4impl17ParsedPatternInfo7hasBodyEv:
.LFB3638:
	.cfi_startproc
	endbr64
	movl	96(%rdi), %eax
	testl	%eax, %eax
	setg	%al
	ret
	.cfi_endproc
.LFE3638:
	.size	_ZNK6icu_676number4impl17ParsedPatternInfo7hasBodyEv, .-_ZNK6icu_676number4impl17ParsedPatternInfo7hasBodyEv
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB0:
	.text
.LHOTB0:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl17ParsedPatternInfo6charAtEii
	.type	_ZNK6icu_676number4impl17ParsedPatternInfo6charAtEii, @function
_ZNK6icu_676number4impl17ParsedPatternInfo6charAtEii:
.LFB3628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movl	%esi, %ecx
	andl	$512, %r8d
	setne	%al
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	andl	$1024, %ecx
	je	.L28
	testb	%al, %al
	je	.L26
	leaq	400(%rdi), %rcx
.L27:
	testl	%edx, %edx
	js	.L32
	movl	(%rcx), %eax
	movl	4(%rcx), %ecx
	subl	%eax, %ecx
	cmpl	%edx, %ecx
	jle	.L32
	addl	%edx, %eax
	movzwl	16(%rdi), %edx
	testw	%dx, %dx
	js	.L34
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L35:
	movl	$-1, %r8d
	cmpl	%eax, %ecx
	jbe	.L25
	andl	$2, %edx
	je	.L37
	addq	$18, %rdi
.L38:
	cltq
	movzwl	(%rdi,%rax,2), %r8d
.L25:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	testl	%ecx, %ecx
	je	.L28
	leaq	232(%rdi), %rcx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	andl	$256, %esi
	je	.L30
	leaq	384(%rdi), %rcx
	testb	%al, %al
	jne	.L27
	leaq	216(%rdi), %rcx
	testl	%esi, %esi
	jne	.L27
.L30:
	leaq	392(%rdi), %rax
	leaq	224(%rdi), %rcx
	testl	%r8d, %r8d
	cmovne	%rax, %rcx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L34:
	movl	20(%rdi), %ecx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L37:
	movq	32(%rdi), %rdi
	jmp	.L38
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_676number4impl17ParsedPatternInfo6charAtEii.cold, @function
_ZNK6icu_676number4impl17ParsedPatternInfo6charAtEii.cold:
.LFSB3628:
.L32:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3628:
	.text
	.size	_ZNK6icu_676number4impl17ParsedPatternInfo6charAtEii, .-_ZNK6icu_676number4impl17ParsedPatternInfo6charAtEii
	.section	.text.unlikely
	.size	_ZNK6icu_676number4impl17ParsedPatternInfo6charAtEii.cold, .-_ZNK6icu_676number4impl17ParsedPatternInfo6charAtEii.cold
.LCOLDE0:
	.text
.LHOTE0:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl17ParsedPatternInfo9getStringEi
	.type	_ZNK6icu_676number4impl17ParsedPatternInfo9getStringEi, @function
_ZNK6icu_676number4impl17ParsedPatternInfo9getStringEi:
.LFB3631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	%edx, %edi
	subq	$8, %rsp
	andl	$512, %edi
	setne	%al
	andl	$1024, %ecx
	je	.L56
	testb	%al, %al
	je	.L54
	leaq	400(%rsi), %rax
.L55:
	movl	(%rax), %edx
	movl	4(%rax), %ecx
	cmpl	%ecx, %edx
	jne	.L60
.L75:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	testl	%ecx, %ecx
	je	.L56
	movl	232(%rsi), %edx
	movl	236(%rsi), %ecx
	leaq	232(%rsi), %rax
	cmpl	%ecx, %edx
	je	.L75
.L60:
	movq	%r12, %rdi
	subl	%edx, %ecx
	addq	$8, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	andl	$256, %edx
	je	.L58
	testb	%al, %al
	je	.L57
	leaq	384(%rsi), %rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	216(%rsi), %rax
	testl	%edx, %edx
	jne	.L55
.L58:
	leaq	392(%rsi), %rdx
	leaq	224(%rsi), %rax
	testl	%edi, %edi
	cmovne	%rdx, %rax
	jmp	.L55
	.cfi_endproc
.LFE3631:
	.size	_ZNK6icu_676number4impl17ParsedPatternInfo9getStringEi, .-_ZNK6icu_676number4impl17ParsedPatternInfo9getStringEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl17ParsedPatternInfo18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode
	.type	_ZNK6icu_676number4impl17ParsedPatternInfo18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode, @function
_ZNK6icu_676number4impl17ParsedPatternInfo18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode:
.LFB3637:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	jmp	_ZN6icu_676number4impl10AffixUtils12containsTypeERKNS_13UnicodeStringENS1_16AffixPatternTypeER10UErrorCode@PLT
	.cfi_endproc
.LFE3637:
	.size	_ZNK6icu_676number4impl17ParsedPatternInfo18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode, .-_ZNK6icu_676number4impl17ParsedPatternInfo18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl17ParsedPatternInfo15hasCurrencySignEv
	.type	_ZNK6icu_676number4impl17ParsedPatternInfo15hasCurrencySignEv, @function
_ZNK6icu_676number4impl17ParsedPatternInfo15hasCurrencySignEv:
.LFB3636:
	.cfi_startproc
	endbr64
	movzbl	210(%rdi), %eax
	testb	%al, %al
	jne	.L77
	movzbl	432(%rdi), %eax
	testb	%al, %al
	je	.L77
	movzbl	378(%rdi), %eax
.L77:
	ret
	.cfi_endproc
.LFE3636:
	.size	_ZNK6icu_676number4impl17ParsedPatternInfo15hasCurrencySignEv, .-_ZNK6icu_676number4impl17ParsedPatternInfo15hasCurrencySignEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB4133:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4133:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB4136:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L95
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L83
	cmpb	$0, 12(%rbx)
	jne	.L96
.L87:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L83:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L87
	.cfi_endproc
.LFE4136:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB4139:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L99
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4139:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB4142:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L102
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE4142:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB4144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L108
.L104:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L109
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L109:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4144:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB4145:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4145:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB4146:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4146:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB4147:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4147:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4148:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4148:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4149:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4149:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB4150:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L125
	testl	%edx, %edx
	jle	.L125
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L128
.L117:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L117
	.cfi_endproc
.LFE4150:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB4151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L132
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L132
	testl	%r12d, %r12d
	jg	.L139
	cmpb	$0, 12(%rbx)
	jne	.L140
.L134:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L134
.L140:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L132:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4151:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB4152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L142
	movq	(%rdi), %r8
.L143:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L146
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L146
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L146:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4152:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB4153:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L153
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4153:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB4154:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4154:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB4155:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4155:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB4156:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4156:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB4158:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4158:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB4160:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4160:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl17ParsedPatternInfo22getLengthFromEndpointsERKNS1_9EndpointsE
	.type	_ZN6icu_676number4impl17ParsedPatternInfo22getLengthFromEndpointsERKNS1_9EndpointsE, @function
_ZN6icu_676number4impl17ParsedPatternInfo22getLengthFromEndpointsERKNS1_9EndpointsE:
.LFB3630:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	subl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE3630:
	.size	_ZN6icu_676number4impl17ParsedPatternInfo22getLengthFromEndpointsERKNS1_9EndpointsE, .-_ZN6icu_676number4impl17ParsedPatternInfo22getLengthFromEndpointsERKNS1_9EndpointsE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl17ParsedPatternInfo12getEndpointsEi
	.type	_ZNK6icu_676number4impl17ParsedPatternInfo12getEndpointsEi, @function
_ZNK6icu_676number4impl17ParsedPatternInfo12getEndpointsEi:
.LFB3632:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	movl	%esi, %edx
	andl	$512, %ecx
	setne	%al
	andl	$1024, %edx
	je	.L163
	testb	%al, %al
	je	.L161
	leaq	400(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	testl	%edx, %edx
	je	.L163
	leaq	232(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	andl	$256, %esi
	je	.L165
	testb	%al, %al
	je	.L164
	leaq	384(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	testl	%esi, %esi
	je	.L165
	leaq	216(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	leaq	392(%rdi), %rax
	addq	$224, %rdi
	testl	%ecx, %ecx
	cmove	%rdi, %rax
	ret
	.cfi_endproc
.LFE3632:
	.size	_ZNK6icu_676number4impl17ParsedPatternInfo12getEndpointsEi, .-_ZNK6icu_676number4impl17ParsedPatternInfo12getEndpointsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl17ParsedPatternInfo11ParserState4peekEv
	.type	_ZN6icu_676number4impl17ParsedPatternInfo11ParserState4peekEv, @function
_ZN6icu_676number4impl17ParsedPatternInfo11ParserState4peekEv:
.LFB3639:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %esi
	movq	(%rdi), %rdi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L180
	sarl	$5, %eax
.L181:
	cmpl	%eax, %esi
	je	.L182
	jmp	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	.p2align 4,,10
	.p2align 3
.L180:
	movl	12(%rdi), %eax
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L182:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3639:
	.size	_ZN6icu_676number4impl17ParsedPatternInfo11ParserState4peekEv, .-_ZN6icu_676number4impl17ParsedPatternInfo11ParserState4peekEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl17ParsedPatternInfo11ParserState4nextEv
	.type	_ZN6icu_676number4impl17ParsedPatternInfo11ParserState4nextEv, @function
_ZN6icu_676number4impl17ParsedPatternInfo11ParserState4nextEv:
.LFB3640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	8(%rdi), %esi
	movq	(%rdi), %rdi
	movswl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L184
	sarl	$5, %edx
.L185:
	cmpl	%edx, %esi
	je	.L187
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%ecx, %ecx
	movl	8(%rbx), %edx
	cmpl	$65535, %eax
	seta	%cl
	addl	$1, %ecx
.L186:
	addl	%ecx, %edx
	movl	%edx, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movl	12(%rdi), %edx
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$-1, %eax
	movl	$2, %ecx
	jmp	.L186
	.cfi_endproc
.LFE3640:
	.size	_ZN6icu_676number4impl17ParsedPatternInfo11ParserState4nextEv, .-_ZN6icu_676number4impl17ParsedPatternInfo11ParserState4nextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl17ParsedPatternInfo14consumeLiteralER10UErrorCode
	.type	_ZN6icu_676number4impl17ParsedPatternInfo14consumeLiteralER10UErrorCode, @function
_ZN6icu_676number4impl17ParsedPatternInfo14consumeLiteralER10UErrorCode:
.LFB3645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	416(%rdi), %esi
	movq	408(%rdi), %rdi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L191
	sarl	$5, %eax
.L192:
	cmpl	%eax, %esi
	je	.L195
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$-1, %eax
	je	.L195
	movq	408(%rbx), %rdi
	movl	416(%rbx), %esi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L196
	sarl	$5, %eax
	cmpl	%eax, %esi
	je	.L225
.L198:
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$39, %eax
	je	.L242
	movq	408(%rbx), %rdi
	movl	416(%rbx), %esi
	movswl	8(%rdi), %eax
.L244:
	testw	%ax, %ax
	js	.L217
	sarl	$5, %eax
.L218:
	cmpl	%eax, %esi
	je	.L225
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%edx, %edx
	movl	%eax, %r8d
	movl	416(%rbx), %eax
	cmpl	$65535, %r8d
	seta	%dl
	addl	$1, %edx
.L197:
	addl	%edx, %eax
	movl	%eax, 416(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movswl	%ax, %esi
	movl	%esi, %edx
	sarl	$5, %edx
.L210:
	cmpl	%edx, %r8d
	je	.L221
	movl	%r8d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	416(%rbx), %edx
	movl	$1, %esi
	movq	408(%rbx), %rdi
	cmpl	$65535, %eax
	movswl	8(%rdi), %eax
	jbe	.L202
.L221:
	movl	$2, %esi
.L202:
	addl	%edx, %esi
	movl	%esi, 416(%rbx)
	testw	%ax, %ax
	js	.L204
	sarl	$5, %eax
	cmpl	%esi, %eax
	je	.L195
.L205:
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	408(%rbx), %rdi
	movl	416(%rbx), %esi
	cmpl	$39, %eax
	movswl	8(%rdi), %eax
	je	.L244
	testw	%ax, %ax
	js	.L207
	sarl	$5, %eax
.L208:
	cmpl	%eax, %esi
	je	.L195
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$-1, %eax
	je	.L195
.L242:
	movq	408(%rbx), %rdi
	movl	416(%rbx), %r8d
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	jns	.L245
	movl	12(%rdi), %edx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L196:
	movl	12(%rdi), %eax
	cmpl	%eax, %esi
	jne	.L198
.L225:
	movl	$2, %edx
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L204:
	cmpl	%esi, 12(%rdi)
	jne	.L205
.L195:
	popq	%rbx
	movl	$65799, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movl	12(%rdi), %eax
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L191:
	movl	12(%rdi), %eax
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L217:
	movl	12(%rdi), %eax
	jmp	.L218
	.cfi_endproc
.LFE3645:
	.size	_ZN6icu_676number4impl17ParsedPatternInfo14consumeLiteralER10UErrorCode, .-_ZN6icu_676number4impl17ParsedPatternInfo14consumeLiteralER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl17ParsedPatternInfo14consumePaddingE24UNumberFormatPadPositionR10UErrorCode
	.type	_ZN6icu_676number4impl17ParsedPatternInfo14consumePaddingE24UNumberFormatPadPositionR10UErrorCode, @function
_ZN6icu_676number4impl17ParsedPatternInfo14consumePaddingE24UNumberFormatPadPositionR10UErrorCode:
.LFB3643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	416(%rdi), %esi
	movq	408(%rdi), %rdi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L247
	sarl	$5, %eax
.L248:
	cmpl	%eax, %esi
	je	.L246
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$42, %eax
	jne	.L246
	movq	424(%rbx), %rax
	cmpb	$0, 48(%rax)
	je	.L252
	movl	$65798, (%r12)
.L246:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movq	408(%rbx), %rdi
	movl	416(%rbx), %esi
	movl	%r13d, 52(%rax)
	movb	$1, 48(%rax)
	movswl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L253
	sarl	$5, %edx
.L254:
	cmpl	%edx, %esi
	je	.L257
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	416(%rbx), %edx
	movl	$1, %ecx
	cmpl	$65535, %eax
	movq	424(%rbx), %rax
	jbe	.L255
.L257:
	movl	$2, %ecx
.L255:
	addl	%ecx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%edx, 416(%rbx)
	movl	%edx, 160(%rax)
	call	_ZN6icu_676number4impl17ParsedPatternInfo14consumeLiteralER10UErrorCode
	movq	424(%rbx), %rax
	movl	416(%rbx), %edx
	movl	%edx, 164(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movl	12(%rdi), %eax
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L253:
	movl	12(%rdi), %edx
	jmp	.L254
	.cfi_endproc
.LFE3643:
	.size	_ZN6icu_676number4impl17ParsedPatternInfo14consumePaddingE24UNumberFormatPadPositionR10UErrorCode, .-_ZN6icu_676number4impl17ParsedPatternInfo14consumePaddingE24UNumberFormatPadPositionR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl17ParsedPatternInfo12consumeAffixERNS1_9EndpointsER10UErrorCode
	.type	_ZN6icu_676number4impl17ParsedPatternInfo12consumeAffixERNS1_9EndpointsER10UErrorCode, @function
_ZN6icu_676number4impl17ParsedPatternInfo12consumeAffixERNS1_9EndpointsER10UErrorCode:
.LFB3644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	.L266(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	416(%rdi), %esi
	movq	%rdx, %rbx
	movq	408(%r14), %rdi
	movl	%esi, 0(%r13)
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L260
	.p2align 4,,10
	.p2align 3
.L275:
	sarl	$5, %eax
.L261:
	cmpl	%eax, %esi
	je	.L262
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$64, %eax
	jg	.L263
	cmpl	$-1, %eax
	jl	.L264
	addl	$1, %eax
	cmpl	$65, %eax
	ja	.L264
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L266:
	.long	.L265-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L265-.L266
	.long	.L264-.L266
	.long	.L269-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L265-.L266
	.long	.L268-.L266
	.long	.L265-.L266
	.long	.L267-.L266
	.long	.L265-.L266
	.long	.L264-.L266
	.long	.L265-.L266
	.long	.L265-.L266
	.long	.L265-.L266
	.long	.L265-.L266
	.long	.L265-.L266
	.long	.L265-.L266
	.long	.L265-.L266
	.long	.L265-.L266
	.long	.L265-.L266
	.long	.L265-.L266
	.long	.L264-.L266
	.long	.L265-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L265-.L266
	.text
	.p2align 4,,10
	.p2align 3
.L263:
	cmpl	$164, %eax
	je	.L270
	cmpl	$8240, %eax
	jne	.L264
	movq	424(%r14), %rax
	movb	$1, 137(%rax)
.L264:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl17ParsedPatternInfo14consumeLiteralER10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L259
	movq	408(%r14), %rdi
	movl	416(%r14), %esi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	jns	.L275
.L260:
	movl	12(%rdi), %eax
	jmp	.L261
.L267:
	movq	424(%r14), %rax
	movb	$1, 139(%rax)
	jmp	.L264
.L269:
	movq	424(%r14), %rax
	movb	$1, 136(%rax)
	jmp	.L264
.L268:
	movq	424(%r14), %rax
	movb	$1, 140(%rax)
	jmp	.L264
.L265:
	movl	416(%r14), %eax
.L262:
	movl	%eax, 4(%r13)
.L259:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	movq	424(%r14), %rax
	movb	$1, 138(%rax)
	jmp	.L264
	.cfi_endproc
.LFE3644:
	.size	_ZN6icu_676number4impl17ParsedPatternInfo12consumeAffixERNS1_9EndpointsER10UErrorCode, .-_ZN6icu_676number4impl17ParsedPatternInfo12consumeAffixERNS1_9EndpointsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl17ParsedPatternInfo20consumeIntegerFormatER10UErrorCode
	.type	_ZN6icu_676number4impl17ParsedPatternInfo20consumeIntegerFormatER10UErrorCode, @function
_ZN6icu_676number4impl17ParsedPatternInfo20consumeIntegerFormatER10UErrorCode:
.LFB3647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	424(%rdi), %r13
	movl	416(%rdi), %esi
	movq	408(%rdi), %rdi
	leaq	56(%r13), %r12
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L277
.L322:
	sarl	$5, %eax
.L278:
	cmpl	%esi, %eax
	je	.L279
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$57, %eax
	jg	.L280
	cmpl	$47, %eax
	jg	.L281
	cmpl	$35, %eax
	je	.L282
	cmpl	$44, %eax
	jne	.L279
	addl	$1, 44(%r13)
	salq	$16, 0(%r13)
.L285:
	movq	408(%r14), %rdi
	movl	416(%r14), %r8d
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L301
	movswl	%ax, %esi
	movl	%esi, %edx
	sarl	$5, %edx
.L302:
	cmpl	%edx, %r8d
	je	.L309
	movl	%r8d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	408(%r14), %rdi
	movl	$1, %esi
	movl	416(%r14), %edx
	cmpl	$65535, %eax
	movswl	8(%rdi), %eax
	jbe	.L303
.L309:
	movl	$2, %esi
.L303:
	addl	%edx, %esi
	movl	%esi, 416(%r14)
	testw	%ax, %ax
	jns	.L322
.L277:
	movl	12(%rdi), %eax
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L280:
	cmpl	$64, %eax
	jne	.L279
	movl	16(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L290
	movl	12(%r13), %edx
	testl	%edx, %edx
	jg	.L290
	addl	$1, 44(%r13)
	addq	$1, 0(%r13)
	addl	$1, 20(%r13)
.L321:
	addl	$1, 24(%r13)
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L301:
	movl	12(%rdi), %edx
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L279:
	movq	0(%r13), %rax
	movq	%rax, %rdx
	shrq	$16, %rdx
	testw	%ax, %ax
	jne	.L300
	cmpw	$-1, %dx
	je	.L300
.L290:
	movl	$65792, (%rbx)
.L276:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	shrq	$32, %rax
	cmpw	$-1, %ax
	je	.L276
	testw	%dx, %dx
	jne	.L276
	movl	$65799, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movl	16(%r13), %edi
	testl	%edi, %edi
	jg	.L290
	movl	20(%r13), %esi
	addl	$1, 44(%r13)
	addq	$1, 0(%r13)
	testl	%esi, %esi
	jle	.L288
	addl	$1, 12(%r13)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L281:
	movl	20(%r13), %eax
	testl	%eax, %eax
	jg	.L290
	addl	$1, 44(%r13)
	movq	%r12, %rdi
	addq	$1, 0(%r13)
	addl	$1, 16(%r13)
	addl	$1, 24(%r13)
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	testb	%al, %al
	je	.L297
	movq	408(%r14), %rdi
	movl	416(%r14), %esi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L294
	sarl	$5, %eax
	cmpl	%eax, %esi
	je	.L308
.L296:
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$48, %eax
	je	.L285
.L297:
	movq	408(%r14), %rdi
	movl	416(%r14), %esi
	movswl	8(%rdi), %eax
	movl	%eax, %edx
	sarl	$5, %eax
	testw	%dx, %dx
	js	.L323
.L298:
	cmpl	%eax, %esi
	je	.L308
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	leal	-48(%rax), %esi
	movsbl	%sil, %esi
.L295:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity11appendDigitEaib@PLT
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L323:
	movl	12(%rdi), %eax
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L288:
	addl	$1, 8(%r13)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L294:
	cmpl	12(%rdi), %esi
	jne	.L296
.L308:
	movl	$-49, %esi
	jmp	.L295
	.cfi_endproc
.LFE3647:
	.size	_ZN6icu_676number4impl17ParsedPatternInfo20consumeIntegerFormatER10UErrorCode, .-_ZN6icu_676number4impl17ParsedPatternInfo20consumeIntegerFormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl17ParsedPatternInfo21consumeFractionFormatER10UErrorCode
	.type	_ZN6icu_676number4impl17ParsedPatternInfo21consumeFractionFormatER10UErrorCode, @function
_ZN6icu_676number4impl17ParsedPatternInfo21consumeFractionFormatER10UErrorCode:
.LFB3648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	424(%rdi), %r15
	movl	416(%rdi), %esi
	movq	408(%rdi), %rdi
	leaq	56(%r15), %r13
	movswl	8(%rdi), %eax
.L344:
	testw	%ax, %ax
	js	.L325
	sarl	$5, %eax
.L326:
	cmpl	%esi, %eax
	je	.L324
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$35, %eax
	je	.L328
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L324
	movl	32(%r15), %eax
	testl	%eax, %eax
	jg	.L349
	movq	408(%r14), %rdi
	addl	$1, 44(%r15)
	addl	$1, 28(%r15)
	movl	416(%r14), %esi
	movswl	8(%rdi), %eax
	addl	$1, 36(%r15)
	testw	%ax, %ax
	js	.L332
	sarl	$5, %eax
	cmpl	%eax, %esi
	je	.L345
.L334:
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$48, %eax
	jne	.L336
	addl	$1, %r12d
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L325:
	movl	12(%rdi), %eax
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L349:
	movl	$65792, (%rbx)
.L324:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	addl	$1, 44(%r15)
	addl	$1, %r12d
	addl	$1, 32(%r15)
	addl	$1, 36(%r15)
.L331:
	movq	408(%r14), %rdi
	movl	416(%r14), %r8d
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L340
	movswl	%ax, %esi
	movl	%esi, %edx
	sarl	$5, %edx
.L341:
	cmpl	%edx, %r8d
	je	.L346
	movl	%r8d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	408(%r14), %rdi
	movl	$1, %esi
	movl	416(%r14), %edx
	cmpl	$65535, %eax
	movswl	8(%rdi), %eax
	jbe	.L342
.L346:
	movl	$2, %esi
.L342:
	addl	%edx, %esi
	movl	%esi, 416(%r14)
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L340:
	movl	12(%rdi), %edx
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L336:
	movq	408(%r14), %rdi
	movl	416(%r14), %esi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L337
	sarl	$5, %eax
.L338:
	cmpl	%eax, %esi
	je	.L345
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	leal	-48(%rax), %esi
	movsbl	%sil, %esi
.L339:
	movl	%r12d, %edx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_676number4impl15DecimalQuantity11appendDigitEaib@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L332:
	cmpl	12(%rdi), %esi
	jne	.L334
.L345:
	movl	$-49, %esi
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L337:
	movl	12(%rdi), %eax
	jmp	.L338
	.cfi_endproc
.LFE3648:
	.size	_ZN6icu_676number4impl17ParsedPatternInfo21consumeFractionFormatER10UErrorCode, .-_ZN6icu_676number4impl17ParsedPatternInfo21consumeFractionFormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl17ParsedPatternInfo13consumeFormatER10UErrorCode
	.type	_ZN6icu_676number4impl17ParsedPatternInfo13consumeFormatER10UErrorCode, @function
_ZN6icu_676number4impl17ParsedPatternInfo13consumeFormatER10UErrorCode:
.LFB3646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_676number4impl17ParsedPatternInfo20consumeIntegerFormatER10UErrorCode
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L362
.L350:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movq	408(%r12), %rdi
	movl	416(%r12), %esi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L353
	sarl	$5, %eax
.L354:
	cmpl	%eax, %esi
	je	.L350
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$46, %eax
	jne	.L350
	movq	408(%r12), %rdi
	movl	416(%r12), %esi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L356
	sarl	$5, %eax
.L357:
	cmpl	%eax, %esi
	je	.L359
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%edx, %edx
	movl	%eax, %r8d
	movl	416(%r12), %eax
	cmpl	$65535, %r8d
	seta	%dl
	addl	$1, %edx
.L358:
	addl	%edx, %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, 416(%r12)
	movq	424(%r12), %rax
	addl	$1, 44(%rax)
	movb	$1, 40(%rax)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl17ParsedPatternInfo21consumeFractionFormatER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	movl	12(%rdi), %eax
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L356:
	movl	12(%rdi), %eax
	jmp	.L357
.L359:
	movl	$2, %edx
	jmp	.L358
	.cfi_endproc
.LFE3646:
	.size	_ZN6icu_676number4impl17ParsedPatternInfo13consumeFormatER10UErrorCode, .-_ZN6icu_676number4impl17ParsedPatternInfo13consumeFormatER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl17ParsedPatternInfo15consumeExponentER10UErrorCode
	.type	_ZN6icu_676number4impl17ParsedPatternInfo15consumeExponentER10UErrorCode, @function
_ZN6icu_676number4impl17ParsedPatternInfo15consumeExponentER10UErrorCode:
.LFB3649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	424(%rdi), %r12
	movl	416(%rdi), %esi
	movq	408(%rdi), %rdi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L364
	sarl	$5, %eax
.L365:
	cmpl	%eax, %esi
	je	.L363
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$69, %eax
	jne	.L363
	movq	(%r12), %rax
	movl	$4294901760, %edx
	andl	$4294901760, %eax
	cmpq	%rdx, %rax
	je	.L367
	movl	$65795, 0(%r13)
.L363:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	movl	12(%rdi), %eax
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L367:
	movq	408(%rbx), %rdi
	movl	416(%rbx), %r8d
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L369
	movswl	%ax, %esi
	movl	%esi, %edx
	sarl	$5, %edx
	cmpl	%edx, %r8d
	je	.L394
.L371:
	movl	%r8d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	408(%rbx), %rdi
	movl	$1, %esi
	movl	416(%rbx), %r8d
	cmpl	$65535, %eax
	movswl	8(%rdi), %eax
	jbe	.L374
	movl	$2, %esi
.L374:
	addl	%esi, %r8d
	movl	%r8d, 416(%rbx)
	addl	$1, 44(%r12)
	testw	%ax, %ax
	js	.L375
	movswl	%ax, %esi
.L389:
	sarl	$5, %esi
.L376:
	cmpl	%r8d, %esi
	je	.L388
	movl	%r8d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$43, %eax
	je	.L378
	movq	408(%rbx), %rdi
	movl	416(%rbx), %esi
	movswl	8(%rdi), %eax
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L395:
	sarl	$5, %eax
.L383:
	cmpl	%eax, %esi
	je	.L363
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$48, %eax
	jne	.L363
	movq	408(%rbx), %rdi
	movl	416(%rbx), %r8d
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L384
	movswl	%ax, %esi
	movl	%esi, %edx
	sarl	$5, %edx
.L385:
	cmpl	%edx, %r8d
	je	.L392
	movl	%r8d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	416(%rbx), %edx
	movl	$1, %esi
	movq	408(%rbx), %rdi
	cmpl	$65535, %eax
	movswl	8(%rdi), %eax
	jbe	.L386
.L392:
	movl	$2, %esi
.L386:
	addl	%edx, %esi
	movl	%esi, 416(%rbx)
	addl	$1, 132(%r12)
	addl	$1, 44(%r12)
.L388:
	testw	%ax, %ax
	jns	.L395
	movl	12(%rdi), %eax
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L384:
	movl	12(%rdi), %edx
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L369:
	cmpl	12(%rdi), %r8d
	jne	.L371
	addl	$2, %r8d
	movl	%r8d, 416(%rbx)
	addl	$1, 44(%r12)
	.p2align 4,,10
	.p2align 3
.L375:
	movl	12(%rdi), %esi
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L378:
	movq	408(%rbx), %rdi
	movl	416(%rbx), %r8d
	movzwl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L379
	movswl	%ax, %esi
	movl	%esi, %eax
	sarl	$5, %eax
.L380:
	cmpl	%eax, %r8d
	je	.L390
	movl	%r8d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r8d
	movl	416(%rbx), %eax
	cmpl	$65535, %r8d
	jbe	.L391
	movq	408(%rbx), %rdi
	movl	$2, %esi
.L381:
	addl	%eax, %esi
	movl	%esi, 416(%rbx)
	addl	$1, 44(%r12)
	movswl	8(%rdi), %eax
	movb	$1, 128(%r12)
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L391:
	movq	408(%rbx), %rdi
	movl	$1, %esi
	jmp	.L381
.L379:
	movl	12(%rdi), %eax
	jmp	.L380
.L394:
	addl	$2, %r8d
	movl	%r8d, 416(%rbx)
	addl	$1, 44(%r12)
	jmp	.L389
.L390:
	movl	$2, %esi
	jmp	.L381
	.cfi_endproc
.LFE3649:
	.size	_ZN6icu_676number4impl17ParsedPatternInfo15consumeExponentER10UErrorCode, .-_ZN6icu_676number4impl17ParsedPatternInfo15consumeExponentER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl17ParsedPatternInfo17consumeSubpatternER10UErrorCode
	.type	_ZN6icu_676number4impl17ParsedPatternInfo17consumeSubpatternER10UErrorCode, @function
_ZN6icu_676number4impl17ParsedPatternInfo17consumeSubpatternER10UErrorCode:
.LFB3642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	416(%rdi), %esi
	movq	408(%rdi), %rdi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L397
	sarl	$5, %eax
.L398:
	cmpl	%eax, %esi
	je	.L400
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$42, %eax
	jne	.L400
	movq	424(%r12), %rax
	cmpb	$0, 48(%rax)
	je	.L402
	movl	$65798, 0(%r13)
.L396:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	movq	408(%r12), %rdi
	movl	$0, 52(%rax)
	movb	$1, 48(%rax)
	movl	416(%r12), %esi
	movswl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L404
	sarl	$5, %edx
.L405:
	cmpl	%edx, %esi
	je	.L419
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	416(%r12), %edx
	movl	$1, %ecx
	cmpl	$65535, %eax
	movq	424(%r12), %rax
	jbe	.L406
.L419:
	movl	$2, %ecx
.L406:
	addl	%ecx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%edx, 416(%r12)
	movl	%edx, 160(%rax)
	call	_ZN6icu_676number4impl17ParsedPatternInfo14consumeLiteralER10UErrorCode
	movq	424(%r12), %rax
	movl	416(%r12), %edx
	movl	%edx, 164(%rax)
.L400:
	movl	0(%r13), %r10d
	testl	%r10d, %r10d
	jg	.L396
	movq	424(%r12), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	144(%rax), %rsi
	call	_ZN6icu_676number4impl17ParsedPatternInfo12consumeAffixERNS1_9EndpointsER10UErrorCode
	movl	0(%r13), %r9d
	testl	%r9d, %r9d
	jg	.L396
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl17ParsedPatternInfo14consumePaddingE24UNumberFormatPadPositionR10UErrorCode
	movl	0(%r13), %r8d
	testl	%r8d, %r8d
	jg	.L396
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_676number4impl17ParsedPatternInfo20consumeIntegerFormatER10UErrorCode
	movl	0(%r13), %edi
	testl	%edi, %edi
	jg	.L396
	movq	408(%r12), %rdi
	movl	416(%r12), %esi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L410
	sarl	$5, %eax
.L411:
	cmpl	%eax, %esi
	je	.L418
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$46, %eax
	je	.L424
.L414:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L396
.L418:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl17ParsedPatternInfo15consumeExponentER10UErrorCode
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L396
	movq	%r13, %rdx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl17ParsedPatternInfo14consumePaddingE24UNumberFormatPadPositionR10UErrorCode
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L396
	movq	424(%r12), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	152(%rax), %rsi
	call	_ZN6icu_676number4impl17ParsedPatternInfo12consumeAffixERNS1_9EndpointsER10UErrorCode
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L396
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$3, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl17ParsedPatternInfo14consumePaddingE24UNumberFormatPadPositionR10UErrorCode
	.p2align 4,,10
	.p2align 3
.L397:
	.cfi_restore_state
	movl	12(%rdi), %eax
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L404:
	movl	12(%rdi), %edx
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L410:
	movl	12(%rdi), %eax
	jmp	.L411
.L424:
	movq	408(%r12), %rdi
	movl	416(%r12), %esi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L415
	sarl	$5, %eax
.L416:
	cmpl	%eax, %esi
	je	.L420
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%edx, %edx
	movl	%eax, %r8d
	movl	416(%r12), %eax
	cmpl	$65535, %r8d
	seta	%dl
	addl	$1, %edx
.L417:
	addl	%edx, %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, 416(%r12)
	movq	424(%r12), %rax
	addl	$1, 44(%rax)
	movb	$1, 40(%rax)
	call	_ZN6icu_676number4impl17ParsedPatternInfo21consumeFractionFormatER10UErrorCode
	jmp	.L414
.L415:
	movl	12(%rdi), %eax
	jmp	.L416
.L420:
	movl	$2, %edx
	jmp	.L417
	.cfi_endproc
.LFE3642:
	.size	_ZN6icu_676number4impl17ParsedPatternInfo17consumeSubpatternER10UErrorCode, .-_ZN6icu_676number4impl17ParsedPatternInfo17consumeSubpatternER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl17ParsedPatternInfo14consumePatternERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl17ParsedPatternInfo14consumePatternERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl17ParsedPatternInfo14consumePatternERKNS_13UnicodeStringER10UErrorCode:
.LFB3641:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L454
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	72(%rbx), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, 424(%rbx)
	call	_ZN6icu_676number4impl17ParsedPatternInfo17consumeSubpatternER10UErrorCode
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L455
.L425:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	movq	408(%rbx), %rdi
	movl	416(%rbx), %r8d
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L429
	sarl	$5, %eax
	cmpl	%eax, %r8d
	je	.L425
.L431:
	movl	%r8d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$59, %eax
	je	.L433
.L440:
	movq	408(%rbx), %rdi
	movl	416(%rbx), %esi
	movswl	8(%rdi), %eax
	movl	%eax, %edx
	sarl	$5, %eax
	testw	%dx, %dx
	js	.L432
.L441:
	cmpl	%esi, %eax
	je	.L425
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$-1, %eax
	je	.L425
	movl	$65555, (%r12)
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L429:
	movl	12(%rdi), %esi
	cmpl	%esi, %r8d
	jne	.L431
	.p2align 4,,10
	.p2align 3
.L432:
	movl	12(%rdi), %eax
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L433:
	movq	408(%rbx), %r8
	movl	416(%rbx), %r9d
	movswl	8(%r8), %esi
	testw	%si, %si
	js	.L435
	sarl	$5, %esi
.L436:
	cmpl	%esi, %r9d
	je	.L442
	movl	%r9d, %esi
	movq	%r8, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	416(%rbx), %esi
	cmpl	$65535, %eax
	jbe	.L443
	movq	408(%rbx), %r8
	movl	$2, %r9d
.L437:
	movswl	8(%r8), %eax
	addl	%esi, %r9d
	movq	%r8, %rdi
	movl	%r9d, 416(%rbx)
	testw	%ax, %ax
	js	.L438
	sarl	$5, %eax
	cmpl	%eax, %r9d
	je	.L425
.L439:
	movl	%r9d, %esi
	movq	%r8, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$-1, %eax
	je	.L440
	leaq	240(%rbx), %rax
	movb	$1, 432(%rbx)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, 424(%rbx)
	call	_ZN6icu_676number4impl17ParsedPatternInfo17consumeSubpatternER10UErrorCode
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L440
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L435:
	movl	12(%r8), %esi
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L443:
	movq	408(%rbx), %r8
	movl	$1, %r9d
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L438:
	movl	12(%r8), %esi
	cmpl	%esi, %r9d
	jne	.L439
	jmp	.L432
.L442:
	movl	$2, %r9d
	jmp	.L437
	.cfi_endproc
.LFE3641:
	.size	_ZN6icu_676number4impl17ParsedPatternInfo14consumePatternERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl17ParsedPatternInfo14consumePatternERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl13PatternParser18parseToPatternInfoERKNS_13UnicodeStringERNS1_17ParsedPatternInfoER10UErrorCode
	.type	_ZN6icu_676number4impl13PatternParser18parseToPatternInfoERKNS_13UnicodeStringERNS1_17ParsedPatternInfoER10UErrorCode, @function
_ZN6icu_676number4impl13PatternParser18parseToPatternInfoERKNS_13UnicodeStringERNS1_17ParsedPatternInfoER10UErrorCode:
.LFB3621:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L485
	ret
	.p2align 4,,10
	.p2align 3
.L485:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rsi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	72(%rbx), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, 424(%rbx)
	call	_ZN6icu_676number4impl17ParsedPatternInfo17consumeSubpatternER10UErrorCode
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L486
.L456:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	.cfi_restore_state
	movq	408(%rbx), %rdi
	movl	416(%rbx), %r8d
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L460
	sarl	$5, %eax
	cmpl	%eax, %r8d
	je	.L456
.L462:
	movl	%r8d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$59, %eax
	je	.L464
.L471:
	movq	408(%rbx), %rdi
	movl	416(%rbx), %esi
	movswl	8(%rdi), %eax
	movl	%eax, %edx
	sarl	$5, %eax
	testw	%dx, %dx
	js	.L463
.L472:
	cmpl	%esi, %eax
	je	.L456
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$-1, %eax
	je	.L456
	movl	$65555, (%r12)
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L460:
	movl	12(%rdi), %esi
	cmpl	%esi, %r8d
	jne	.L462
	.p2align 4,,10
	.p2align 3
.L463:
	movl	12(%rdi), %eax
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L464:
	movq	408(%rbx), %r8
	movl	416(%rbx), %r9d
	movswl	8(%r8), %esi
	testw	%si, %si
	js	.L466
	sarl	$5, %esi
.L467:
	cmpl	%esi, %r9d
	je	.L473
	movl	%r9d, %esi
	movq	%r8, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	416(%rbx), %esi
	cmpl	$65535, %eax
	jbe	.L474
	movq	408(%rbx), %r8
	movl	$2, %r9d
.L468:
	movswl	8(%r8), %eax
	addl	%esi, %r9d
	movq	%r8, %rdi
	movl	%r9d, 416(%rbx)
	testw	%ax, %ax
	js	.L469
	sarl	$5, %eax
	cmpl	%eax, %r9d
	je	.L456
.L470:
	movl	%r9d, %esi
	movq	%r8, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$-1, %eax
	je	.L471
	leaq	240(%rbx), %rax
	movb	$1, 432(%rbx)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, 424(%rbx)
	call	_ZN6icu_676number4impl17ParsedPatternInfo17consumeSubpatternER10UErrorCode
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L471
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L466:
	movl	12(%r8), %esi
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L474:
	movq	408(%rbx), %r8
	movl	$1, %r9d
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L469:
	movl	12(%r8), %esi
	cmpl	%esi, %r9d
	jne	.L470
	jmp	.L463
.L473:
	movl	$2, %r9d
	jmp	.L468
	.cfi_endproc
.LFE3621:
	.size	_ZN6icu_676number4impl13PatternParser18parseToPatternInfoERKNS_13UnicodeStringERNS1_17ParsedPatternInfoER10UErrorCode, .-_ZN6icu_676number4impl13PatternParser18parseToPatternInfoERKNS_13UnicodeStringERNS1_17ParsedPatternInfoER10UErrorCode
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC2:
	.string	"'"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl13PatternParser23patternInfoToPropertiesERNS1_23DecimalFormatPropertiesERNS1_17ParsedPatternInfoENS1_14IgnoreRoundingER10UErrorCode
	.type	_ZN6icu_676number4impl13PatternParser23patternInfoToPropertiesERNS1_23DecimalFormatPropertiesERNS1_17ParsedPatternInfoENS1_14IgnoreRoundingER10UErrorCode, @function
_ZN6icu_676number4impl13PatternParser23patternInfoToPropertiesERNS1_23DecimalFormatPropertiesERNS1_17ParsedPatternInfoENS1_14IgnoreRoundingER10UErrorCode:
.LFB3651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$296, %rsp
	movq	%rcx, -328(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L488
	movl	$1, %r13d
	cmpl	$1, %edx
	je	.L544
.L488:
	movq	72(%rbx), %rdx
	movq	%rdx, %rcx
	movq	%rdx, %rsi
	shrq	$16, %rcx
	shrq	$32, %rsi
	movswl	%cx, %eax
	cmpw	$-1, %cx
	je	.L537
	movswl	%dx, %edx
	movl	$1, %ecx
.L489:
	cmpw	$-1, %si
	movl	96(%rbx), %r12d
	movl	100(%rbx), %r14d
	movl	%edx, 72(%r15)
	movl	$-1, %edx
	movb	%cl, 76(%r15)
	cmove	%edx, %eax
	movl	%eax, 752(%r15)
	testl	%r12d, %r12d
	jne	.L491
	movl	108(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L545
.L491:
	movl	88(%rbx), %r12d
	testl	%r12d, %r12d
	je	.L546
.L492:
	movl	92(%rbx), %eax
	testl	%eax, %eax
	jle	.L493
.L550:
	movl	%eax, 112(%r15)
	addl	84(%rbx), %eax
	movl	$-1, 100(%r15)
	movl	$-1, 84(%r15)
	movq	$0x000000000, 736(%r15)
	movl	%eax, 92(%r15)
.L494:
	movzbl	112(%rbx), %eax
	testb	%al, %al
	je	.L498
	movl	108(%rbx), %r9d
	testl	%r9d, %r9d
	sete	%al
.L498:
	movb	%al, 65(%r15)
	movl	204(%rbx), %eax
	testl	%eax, %eax
	jle	.L499
	movzbl	200(%rbx), %edx
	movl	92(%rbx), %r8d
	movl	%eax, 96(%r15)
	movb	%dl, 66(%r15)
	testl	%r8d, %r8d
	je	.L547
	movl	$1, 108(%r15)
	movl	$-1, 88(%r15)
.L501:
	movq	(%rbx), %rax
	leaq	_ZNK6icu_676number4impl17ParsedPatternInfo9getStringEi(%rip), %rsi
	movq	32(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L502
	movl	216(%rbx), %edx
	movl	220(%rbx), %ecx
	cmpl	%ecx, %edx
	jne	.L503
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rsi
	movl	$2, %edi
	leaq	-320(%rbp), %r13
	movq	%rsi, -320(%rbp)
	movw	%di, -312(%rbp)
.L504:
	leaq	_ZNK6icu_676number4impl17ParsedPatternInfo9getStringEi(%rip), %rsi
	cmpq	%rsi, %rax
	jne	.L505
.L552:
	movl	224(%rbx), %edx
	movl	228(%rbx), %ecx
	cmpl	%ecx, %edx
	jne	.L506
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %esi
	leaq	-256(%rbp), %r14
	movq	%rax, -256(%rbp)
	movw	%si, -248(%rbp)
.L507:
	cmpb	$0, 120(%rbx)
	leaq	392(%r15), %r11
	je	.L508
	movq	-328(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r11, -336(%rbp)
	movl	116(%rbx), %r12d
	call	_ZN6icu_676number4impl10AffixUtils14estimateLengthERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	-328(%rbp), %rsi
	movq	%r14, %rdi
	addl	%eax, %r12d
	call	_ZN6icu_676number4impl10AffixUtils14estimateLengthERKNS_13UnicodeStringER10UErrorCode@PLT
	leaq	_ZNK6icu_676number4impl17ParsedPatternInfo9getStringEi(%rip), %rsi
	movq	-336(%rbp), %r11
	addl	%r12d, %eax
	movl	%eax, 68(%r15)
	movq	(%rbx), %rax
	movq	32(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L509
	movl	232(%rbx), %edx
	movl	236(%rbx), %ecx
	cmpl	%ecx, %edx
	jne	.L510
	movl	$2, %ecx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-192(%rbp), %r10
	movw	%cx, -184(%rbp)
	movq	%rax, -192(%rbp)
	movl	$2, %eax
.L511:
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L514:
	cmpl	$1, %ecx
	je	.L519
	cmpl	$2, %ecx
	jne	.L517
	leaq	-182(%rbp), %rdx
	testb	$2, %al
	movq	%rdx, %rax
	cmove	-168(%rbp), %rax
	cmpw	$39, (%rax)
	je	.L548
.L519:
	movq	%r10, %rsi
	movq	%r11, %rdi
	movq	%r10, -328(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-328(%rbp), %r10
.L516:
	movl	124(%rbx), %eax
	movb	$0, 384(%r15)
	movq	%r10, %rdi
	movl	%eax, 388(%r15)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L522:
	leaq	544(%r15), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	672(%r15), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	320(%r15), %rax
	cmpb	$0, 432(%rbx)
	leaq	192(%r15), %r9
	movq	%rax, -328(%rbp)
	je	.L523
	movq	(%rbx), %rax
	leaq	_ZNK6icu_676number4impl17ParsedPatternInfo9getStringEi(%rip), %rdi
	movq	32(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L524
	movl	384(%rbx), %edx
	movl	388(%rbx), %ecx
	cmpl	%ecx, %edx
	jne	.L525
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	leaq	-128(%rbp), %r12
	movq	%rax, -128(%rbp)
	movw	%dx, -120(%rbp)
.L526:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%rbx), %rax
	leaq	_ZNK6icu_676number4impl17ParsedPatternInfo9getStringEi(%rip), %rdi
	movq	32(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L527
	movl	392(%rbx), %edx
	movl	396(%rbx), %ecx
	cmpl	%ecx, %edx
	jne	.L528
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
.L529:
	leaq	320(%r15), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L530:
	cmpb	$0, 208(%rbx)
	je	.L531
	movl	$2, 80(%r15)
.L532:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L549
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	movl	92(%rbx), %eax
	xorl	%r12d, %r12d
	testl	%r14d, %r14d
	sete	%r12b
	testl	%eax, %eax
	jg	.L550
.L493:
	leaq	128(%rbx), %rdi
	movq	%rdi, -336(%rbp)
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	testb	%al, %al
	je	.L551
	testb	%r13b, %r13b
	jne	.L496
	movl	108(%rbx), %eax
	movl	%r14d, 100(%r15)
	movq	$0x000000000, 736(%r15)
	movl	%eax, 84(%r15)
.L497:
	movl	$-1, 112(%r15)
	movl	$-1, 92(%r15)
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L547:
	movl	88(%rbx), %eax
	movl	%eax, 108(%r15)
	movl	96(%rbx), %eax
	movl	%eax, 88(%r15)
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L545:
	movl	%r14d, %esi
	movl	$1, %edi
	call	uprv_max_67@PLT
	movl	%eax, %r14d
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L508:
	movl	$-1, 68(%r15)
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movb	$1, 384(%r15)
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L531:
	cmpb	$0, 209(%rbx)
	je	.L533
	movl	$3, 80(%r15)
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L499:
	movb	$0, 66(%r15)
	movl	$-1, 96(%r15)
	movl	%r12d, 108(%r15)
	movl	$-1, 88(%r15)
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L506:
	leaq	-256(%rbp), %r14
	subl	%edx, %ecx
	leaq	8(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L503:
	leaq	-320(%rbp), %r13
	leaq	8(%rbx), %rsi
	subl	%edx, %ecx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	movq	(%rbx), %rax
	leaq	_ZNK6icu_676number4impl17ParsedPatternInfo9getStringEi(%rip), %rsi
	movq	32(%rax), %rax
	cmpq	%rsi, %rax
	je	.L552
.L505:
	leaq	-256(%rbp), %r14
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	*%rax
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L537:
	xorl	%ecx, %ecx
	movl	$-1, %edx
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L523:
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	leaq	320(%r15), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L528:
	subl	%edx, %ecx
	leaq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L525:
	leaq	-128(%rbp), %r12
	subl	%edx, %ecx
	leaq	8(%rbx), %rsi
	movq	%r9, -328(%rbp)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	movq	-328(%rbp), %r9
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L533:
	movl	$0, 80(%r15)
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L502:
	leaq	-320(%rbp), %r13
	movl	$256, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	*%rax
	movq	(%rbx), %rax
	movq	32(%rax), %rax
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L544:
	movzbl	210(%rsi), %r13d
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L551:
	testb	%r13b, %r13b
	movq	-336(%rbp), %rdi
	je	.L553
.L496:
	movl	$-1, 100(%r15)
	movl	$-1, 84(%r15)
	movq	$0x000000000, 736(%r15)
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L548:
	movq	%r11, %rdi
	movq	%r10, -336(%rbp)
	movq	%r11, -328(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	400(%r15), %eax
	movq	-328(%rbp), %r11
	movq	-336(%rbp), %r10
	testw	%ax, %ax
	js	.L520
	movswl	%ax, %edx
	sarl	$5, %edx
.L521:
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r11, %rdi
	leaq	.LC2(%rip), %rcx
	movq	%r10, -328(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movq	-328(%rbp), %r10
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L510:
	leaq	-192(%rbp), %r10
	subl	%edx, %ecx
	leaq	8(%rbx), %rsi
	movq	%r11, -336(%rbp)
	movq	%r10, %rdi
	movq	%r10, -328(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	movq	-328(%rbp), %r10
	movq	-336(%rbp), %r11
.L512:
	movzwl	-184(%rbp), %eax
	movl	-180(%rbp), %ecx
	testw	%ax, %ax
	js	.L514
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L517:
	leaq	-128(%rbp), %r12
	movq	%r10, %rsi
	subl	$2, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%r10, -328(%rbp)
	movq	%r11, -336(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	movq	-336(%rbp), %r11
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-328(%rbp), %r10
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L527:
	movl	$512, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L524:
	leaq	-128(%rbp), %r12
	movq	%r9, -328(%rbp)
	movl	$768, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*%rax
	movq	-328(%rbp), %r9
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L509:
	leaq	-192(%rbp), %r10
	movq	%r11, -336(%rbp)
	movl	$1024, %edx
	movq	%rbx, %rsi
	movq	%r10, -328(%rbp)
	movq	%r10, %rdi
	call	*%rax
	movq	-336(%rbp), %r11
	movq	-328(%rbp), %r10
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L520:
	movl	404(%r15), %edx
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L553:
	movl	108(%rbx), %eax
	movl	%r14d, 100(%r15)
	movl	%eax, 84(%r15)
	call	_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv@PLT
	movsd	%xmm0, 736(%r15)
	jmp	.L497
.L549:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3651:
	.size	_ZN6icu_676number4impl13PatternParser23patternInfoToPropertiesERNS1_23DecimalFormatPropertiesERNS1_17ParsedPatternInfoENS1_14IgnoreRoundingER10UErrorCode, .-_ZN6icu_676number4impl13PatternParser23patternInfoToPropertiesERNS1_23DecimalFormatPropertiesERNS1_17ParsedPatternInfoENS1_14IgnoreRoundingER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl18PatternStringUtils23ignoreRoundingIncrementEdi
	.type	_ZN6icu_676number4impl18PatternStringUtils23ignoreRoundingIncrementEdi, @function
_ZN6icu_676number4impl18PatternStringUtils23ignoreRoundingIncrementEdi:
.LFB3652:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%edi, %edi
	js	.L554
	addsd	%xmm0, %xmm0
	movsd	.LC3(%rip), %xmm1
	movsd	.LC4(%rip), %xmm2
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L558:
	comisd	%xmm0, %xmm1
	jb	.L556
	mulsd	%xmm2, %xmm0
	addl	$1, %eax
	cmpl	%eax, %edi
	jge	.L558
.L556:
	cmpl	%eax, %edi
	setl	%al
.L554:
	ret
	.cfi_endproc
.LFE3652:
	.size	_ZN6icu_676number4impl18PatternStringUtils23ignoreRoundingIncrementEdi, .-_ZN6icu_676number4impl18PatternStringUtils23ignoreRoundingIncrementEdi
	.section	.rodata.str2.2
	.align 2
.LC5:
	.string	"'"
	.string	"'"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl18PatternStringUtils19escapePaddingStringENS_13UnicodeStringERS3_iR10UErrorCode
	.type	_ZN6icu_676number4impl18PatternStringUtils19escapePaddingStringENS_13UnicodeStringERS3_iR10UErrorCode, @function
_ZN6icu_676number4impl18PatternStringUtils19escapePaddingStringENS_13UnicodeStringERS3_iR10UErrorCode:
.LFB3654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	%edx, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L563
	movswl	%ax, %edx
	sarl	$5, %edx
	testl	%edx, %edx
	je	.L596
.L565:
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L568
	sarl	$5, %edx
	movl	%edx, -72(%rbp)
.L569:
	testw	%ax, %ax
	js	.L570
	sarl	$5, %eax
	cmpl	$1, %eax
	je	.L571
.L572:
	movl	$39, %edx
	movl	-68(%rbp), %esi
	leaq	-58(%rbp), %r14
	xorl	%r8d, %r8d
	movw	%dx, -58(%rbp)
	movl	$1, %r9d
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	movl	$1, %r13d
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L598:
	movl	-68(%rbp), %ecx
	movswl	%ax, %edx
	sarl	$5, %edx
	leal	0(%r13,%rcx), %esi
	cmpl	%ebx, %edx
	jle	.L578
.L599:
	jbe	.L589
	testb	$2, %al
	je	.L580
	leaq	10(%r15), %rax
	movzwl	(%rax,%rbx,2), %eax
	cmpw	$39, %ax
	je	.L597
.L579:
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%ax, -58(%rbp)
	addl	$1, %r13d
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
.L582:
	addq	$1, %rbx
.L583:
	movzwl	8(%r15), %eax
	testw	%ax, %ax
	jns	.L598
	movl	-68(%rbp), %ecx
	movl	12(%r15), %edx
	leal	0(%r13,%rcx), %esi
	cmpl	%ebx, %edx
	jg	.L599
.L578:
	movl	$39, %eax
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
.L573:
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L584
.L601:
	sarl	$5, %eax
.L585:
	subl	-72(%rbp), %eax
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L600
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore_state
	movq	24(%r15), %rax
	movzwl	(%rax,%rbx,2), %eax
	cmpw	$39, %ax
	jne	.L579
.L597:
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rcx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	leaq	.LC5(%rip), %rax
	addl	$2, %r13d
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L589:
	movl	$-1, %eax
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L570:
	cmpl	$1, 12(%r15)
	jne	.L572
.L571:
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	leaq	.LC2(%rip), %rcx
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	.LC2(%rip), %rdx
	testb	%al, %al
	je	.L586
	movzwl	8(%r15), %eax
	testw	%ax, %ax
	js	.L574
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L575:
	movl	-68(%rbp), %esi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movswl	8(%r12), %eax
	testw	%ax, %ax
	jns	.L601
	.p2align 4,,10
	.p2align 3
.L584:
	movl	12(%r12), %eax
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L568:
	movl	12(%r12), %edi
	movl	%edi, -72(%rbp)
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L563:
	movl	12(%rdi), %edx
	testl	%edx, %edx
	jne	.L565
.L596:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	8(%r15), %eax
	testw	%ax, %ax
	js	.L566
	movswl	%ax, %edx
	sarl	$5, %edx
.L567:
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r15, %rdi
	leaq	_ZN6icu_676number4implL22kFallbackPaddingStringE(%rip), %rcx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movswl	8(%r15), %eax
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L586:
	movl	-68(%rbp), %esi
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rcx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	leaq	.LC5(%rip), %rax
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L566:
	movl	12(%r15), %edx
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L574:
	movl	12(%r15), %r9d
	jmp	.L575
.L600:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3654:
	.size	_ZN6icu_676number4impl18PatternStringUtils19escapePaddingStringENS_13UnicodeStringERS3_iR10UErrorCode, .-_ZN6icu_676number4impl18PatternStringUtils19escapePaddingStringENS_13UnicodeStringERS3_iR10UErrorCode
	.section	.rodata.str2.2
	.align 2
.LC6:
	.string	"%"
	.string	""
	.string	""
	.align 2
.LC7:
	.string	"0 "
	.string	""
	.align 2
.LC8:
	.string	"."
	.string	""
	.string	""
	.align 2
.LC9:
	.string	","
	.string	""
	.string	""
	.align 2
.LC10:
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC11:
	.string	"+"
	.string	""
	.string	""
	.align 2
.LC12:
	.string	";"
	.string	""
	.string	""
	.align 2
.LC13:
	.string	"@"
	.string	""
	.string	""
	.align 2
.LC14:
	.string	"E"
	.string	""
	.string	""
	.align 2
.LC15:
	.string	"*"
	.string	""
	.string	""
	.align 2
.LC16:
	.string	"#"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl18PatternStringUtils16convertLocalizedERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsEbR10UErrorCode
	.type	_ZN6icu_676number4impl18PatternStringUtils16convertLocalizedERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsEbR10UErrorCode, @function
_ZN6icu_676number4impl18PatternStringUtils16convertLocalizedERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsEbR10UErrorCode:
.LFB3655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2752(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$3080, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -2904(%rbp)
	leaq	-64(%rbp), %rdx
	movq	%rdi, -2912(%rbp)
	movq	%r8, -3120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -2936(%rbp)
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L603:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rsi
	movl	$2, %edi
	movl	$2, %r8d
	subq	$-128, %rax
	movq	%rsi, -128(%rax)
	movw	%di, -120(%rax)
	movq	%rsi, -64(%rax)
	movw	%r8w, -56(%rax)
	cmpq	%rax, %rdx
	jne	.L603
	testb	%cl, %cl
	jne	.L663
	movq	$448, -3112(%rbp)
	movq	%r15, %r13
	movl	$1, %r12d
	leaq	-2688(%rbp), %r9
	movq	$256, -3104(%rbp)
	movq	$128, -3096(%rbp)
	movq	$960, -3088(%rbp)
	movq	$704, -3080(%rbp)
	movq	$896, -3072(%rbp)
	movq	$1344, -3064(%rbp)
	movq	$1280, -3056(%rbp)
	movq	$384, -3048(%rbp)
	movq	$640, -3040(%rbp)
	movq	$832, -3032(%rbp)
	movq	$1088, -3024(%rbp)
	movq	$1024, -3016(%rbp)
	movq	$576, -3008(%rbp)
	movq	$512, -3000(%rbp)
	movq	$768, -2992(%rbp)
	movq	$1216, -2984(%rbp)
	movq	$1152, -2968(%rbp)
	movq	$192, -2960(%rbp)
	movq	$320, -2928(%rbp)
	movq	$0, -2944(%rbp)
	movl	$1, -2952(%rbp)
	movl	$0, -2972(%rbp)
.L604:
	leaq	-2816(%rbp), %rbx
	leaq	.LC6(%rip), %rsi
	movq	%r9, -2920(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-2920(%rbp), %r9
	movq	%rbx, %rsi
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-2904(%rbp), %rax
	movq	%r13, %rdi
	leaq	200(%rax), %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%rbx, %rdi
	leaq	.LC7(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-2960(%rbp), %rdi
	movq	%rbx, %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-2904(%rbp), %r13
	movq	-3096(%rbp), %rdi
	leaq	776(%r13), %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%rbx, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-2928(%rbp), %rdi
	movq	%rbx, %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3104(%rbp), %rdi
	leaq	8(%r13), %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%rbx, %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-3112(%rbp), %rdi
	movq	%rbx, %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3048(%rbp), %rdi
	leaq	72(%r13), %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%rbx, %rdi
	leaq	.LC10(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-3008(%rbp), %rdi
	movq	%rbx, %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3000(%rbp), %rdi
	leaq	392(%r13), %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%rbx, %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-3080(%rbp), %rdi
	movq	%rbx, %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3040(%rbp), %rdi
	leaq	456(%r13), %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%rbx, %rdi
	leaq	.LC12(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-3032(%rbp), %rdi
	movq	%rbx, %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-2992(%rbp), %rdi
	leaq	136(%r13), %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%rbx, %rdi
	leaq	.LC13(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-3088(%rbp), %rdi
	movq	%rbx, %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3072(%rbp), %rdi
	leaq	1032(%r13), %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%rbx, %rdi
	leaq	.LC14(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-3024(%rbp), %rdi
	movq	%rbx, %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3016(%rbp), %rdi
	leaq	712(%r13), %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%rbx, %rdi
	leaq	.LC15(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-2984(%rbp), %rdi
	movq	%rbx, %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-2968(%rbp), %rdi
	leaq	840(%r13), %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%rbx, %rdi
	leaq	.LC16(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-3064(%rbp), %rdi
	movq	%rbx, %rsi
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3056(%rbp), %rdi
	leaq	328(%r13), %rsi
	movq	%r13, -2904(%rbp)
	addq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	22(%r12), %r8
	movq	%r12, %rcx
	movslq	-2952(%rbp), %rax
	salq	$6, %r8
	negq	%rcx
	movq	%r14, -2952(%rbp)
	movq	%rcx, %rsi
	movq	%rax, %rcx
	movq	-2904(%rbp), %rax
	leaq	(%r15,%r8), %r13
	salq	$6, %rsi
	salq	$6, %rcx
	movq	%rbx, -2960(%rbp)
	movq	%r13, %rbx
	movq	%rsi, -2928(%rbp)
	movq	-2944(%rbp), %rsi
	movq	%rcx, -2920(%rbp)
	subq	%r12, %rsi
	xorl	%r12d, %r12d
	movq	%r15, -2968(%rbp)
	movq	%rsi, %rcx
	leaq	1096(%rax), %rsi
	addq	$264, %rax
	movq	%r12, %r14
	salq	$6, %rcx
	movq	%rsi, -2904(%rbp)
	movq	%rax, %r15
	movq	%rcx, %r13
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L697:
	sarl	$5, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString7replaceEiii@PLT
	testq	%r14, %r14
	je	.L696
.L607:
	movq	%r14, %rsi
	leaq	0(%r13,%rbx), %rdi
	salq	$6, %rsi
	addq	-2904(%rbp), %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	cmpl	$9, %r12d
	je	.L609
.L608:
	addq	$1, %r14
	subq	$-128, %rbx
.L610:
	movswl	8(%rbx), %edx
	movl	%r14d, %r12d
	leal	48(%r14), %ecx
	testw	%dx, %dx
	jns	.L697
	movq	-2928(%rbp), %rax
	movq	-2920(%rbp), %rdi
	xorl	%esi, %esi
	addq	%rbx, %rax
	movl	12(%rax,%rdi), %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString7replaceEiii@PLT
	testq	%r14, %r14
	jne	.L607
.L696:
	leaq	0(%r13,%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L609:
	movq	-2944(%rbp), %rsi
	movq	-2968(%rbp), %r15
	leaq	-2880(%rbp), %r12
	movq	-2952(%rbp), %r14
	movq	-2960(%rbp), %rbx
	movq	%rsi, %rax
	salq	$6, %rax
	leaq	(%r15,%rax), %r13
	addq	-2936(%rbp), %rax
	movq	%rax, -2904(%rbp)
	movq	%rsi, %rax
	negq	%rax
	salq	$6, %rax
	movq	%rax, -2928(%rbp)
	movslq	-2972(%rbp), %rax
	salq	$6, %rax
	movq	%rax, -2920(%rbp)
	.p2align 4,,10
	.p2align 3
.L618:
	movl	$8217, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movl	$39, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movswl	-2808(%rbp), %eax
	testw	%ax, %ax
	js	.L611
	sarl	$5, %eax
.L612:
	movswl	-2872(%rbp), %r9d
	testw	%r9w, %r9w
	js	.L613
	sarl	$5, %r9d
.L614:
	movswl	8(%r13), %edx
	testw	%dx, %dx
	js	.L615
	subq	$8, %rsp
	sarl	$5, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	pushq	%rax
	xorl	%esi, %esi
	movq	%r13, %rdi
	subq	$-128, %r13
	pushq	$0
	pushq	%rbx
	call	_ZN6icu_6713UnicodeString14findAndReplaceEiiRKS0_iiS2_ii@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpq	-2904(%rbp), %r13
	jne	.L618
.L616:
	movq	-2912(%rbp), %rax
	movl	$2, %edx
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rsi
	movq	%r15, -2944(%rbp)
	movw	%dx, 8(%rax)
	movzwl	8(%r14), %edx
	movq	%rsi, (%rax)
	leaq	2760(%r15), %rax
	movq	%rax, -2952(%rbp)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L699:
	movswl	%dx, %eax
	sarl	$5, %eax
	cmpl	%eax, %r12d
	jge	.L621
.L700:
	cmpl	%r12d, %eax
	jbe	.L664
	leaq	10(%r14), %rax
	testb	$2, %dl
	jne	.L624
	movq	24(%r14), %rax
.L624:
	movslq	%r12d, %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, -2904(%rbp)
	cmpw	$39, %ax
	je	.L698
.L622:
	leal	-3(%r13), %eax
	movl	%eax, -2920(%rbp)
	testl	%r13d, %r13d
	je	.L667
	cmpl	$1, %eax
	jbe	.L667
	movzwl	-2904(%rbp), %eax
	movq	-2912(%rbp), %rdi
	xorl	%edx, %edx
	leaq	-2882(%rbp), %rsi
	movl	$1, %ecx
	movl	$2, %r13d
	movw	%ax, -2882(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	8(%r14), %edx
.L626:
	addl	$1, %r12d
.L658:
	testw	%dx, %dx
	jns	.L699
	movl	12(%r14), %eax
	cmpl	%eax, %r12d
	jl	.L700
.L621:
	leal	-3(%r13), %eax
	movq	-2944(%rbp), %r15
	cmpl	$1, %eax
	jbe	.L701
	testl	%r13d, %r13d
	jne	.L702
.L660:
	leaq	-128(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L661:
	movq	(%rbx), %rax
	movq	%rbx, %r12
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%r15, %r12
	jne	.L661
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L703
	movq	-2912(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L615:
	.cfi_restore_state
	movq	-2928(%rbp), %rdi
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rcx
	leaq	(%rdi,%r13), %rdx
	movq	-2920(%rbp), %rdi
	movl	12(%rdx,%rdi), %edx
	pushq	%rax
	movq	%r13, %rdi
	subq	$-128, %r13
	pushq	$0
	pushq	%rbx
	call	_ZN6icu_6713UnicodeString14findAndReplaceEiiRKS0_iiS2_ii@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpq	%r13, -2904(%rbp)
	jne	.L618
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L613:
	movl	-2868(%rbp), %r9d
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L611:
	movl	-2804(%rbp), %eax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L667:
	movq	-2944(%rbp), %r15
	movl	%r13d, -2928(%rbp)
	movq	%r15, %r13
	movq	-2936(%rbp), %r15
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L706:
	movzbl	8(%r13), %eax
	andl	$1, %eax
	testb	%al, %al
	jne	.L704
.L639:
	movq	%rbx, %rdi
	subq	$-128, %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpq	%r15, %r13
	je	.L705
.L645:
	movswl	8(%r13), %ecx
	testw	%cx, %cx
	js	.L631
	sarl	$5, %ecx
.L632:
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movswl	-2808(%rbp), %edx
	testb	$1, %dl
	jne	.L706
	testw	%dx, %dx
	js	.L635
	sarl	$5, %edx
.L636:
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L637
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L638:
	testb	$1, %al
	jne	.L639
	cmpl	%edx, %ecx
	jne	.L639
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	testb	%al, %al
	je	.L639
	.p2align 4,,10
	.p2align 3
.L704:
	movq	%r13, %r15
	movl	-2928(%rbp), %r13d
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L640
	sarl	$5, %eax
	cmpl	$1, -2920(%rbp)
	leal	-1(%r12,%rax), %r12d
	jbe	.L707
.L642:
	movzwl	72(%r15), %eax
	leaq	64(%r15), %rsi
	testw	%ax, %ax
	js	.L643
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L644:
	movq	-2912(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzwl	8(%r14), %edx
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L631:
	movl	12(%r13), %ecx
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L637:
	movl	12(%r13), %ecx
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L635:
	movl	-2804(%rbp), %edx
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L698:
	testl	%r13d, %r13d
	je	.L708
	cmpl	$1, %r13d
	je	.L709
	cmpl	$2, %r13d
	je	.L665
	cmpl	$3, %r13d
	je	.L710
	cmpl	$4, %r13d
	je	.L666
	movq	-2912(%rbp), %r15
	movl	$39, %r8d
	leaq	-2882(%rbp), %rsi
	xorl	%edx, %edx
	movl	$1, %ecx
	movw	%r8w, -2882(%rbp)
	movl	$4, %r13d
	movq	%r15, %rdi
	movq	%rsi, -2904(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$39, %r9d
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	-2904(%rbp), %rsi
	movl	$1, %ecx
	movw	%r9w, -2882(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	8(%r14), %edx
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L664:
	movl	$-1, %edi
	movw	%di, -2904(%rbp)
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L705:
	leaq	-2680(%rbp), %r15
	movq	%r15, %r13
	movq	-2952(%rbp), %r15
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L713:
	movzbl	0(%r13), %eax
	andl	$1, %eax
	testb	%al, %al
	jne	.L711
.L654:
	movq	%rbx, %rdi
	subq	$-128, %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpq	%r13, %r15
	je	.L712
.L656:
	movswl	0(%r13), %ecx
	testw	%cx, %cx
	js	.L646
	sarl	$5, %ecx
.L647:
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movswl	-2808(%rbp), %edx
	testb	$1, %dl
	jne	.L713
	testw	%dx, %dx
	js	.L650
	sarl	$5, %edx
.L651:
	movzwl	0(%r13), %eax
	testw	%ax, %ax
	js	.L652
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L653:
	testb	$1, %al
	jne	.L654
	cmpl	%edx, %ecx
	jne	.L654
	leaq	-8(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	testb	%al, %al
	je	.L654
	.p2align 4,,10
	.p2align 3
.L711:
	movl	-2928(%rbp), %r13d
	leaq	-2882(%rbp), %rsi
	testl	%r13d, %r13d
	je	.L714
.L655:
	movzwl	-2904(%rbp), %eax
	movq	-2912(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movw	%ax, -2882(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzwl	8(%r14), %edx
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L646:
	movl	4(%r13), %ecx
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L652:
	movl	4(%r13), %ecx
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L650:
	movl	-2804(%rbp), %edx
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L712:
	cmpl	$1, -2920(%rbp)
	movl	-2928(%rbp), %r13d
	leaq	-2882(%rbp), %rsi
	jbe	.L715
.L657:
	movzwl	-2904(%rbp), %eax
	movq	-2912(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movw	%ax, -2882(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	8(%r14), %edx
	jmp	.L626
.L666:
	movl	$5, %r13d
	jmp	.L626
.L643:
	movl	76(%r15), %ecx
	jmp	.L644
.L640:
	movl	12(%r15), %eax
	cmpl	$1, -2920(%rbp)
	leal	-1(%r12,%rax), %r12d
	ja	.L642
.L707:
	movl	$39, %esi
	movl	$1, %ecx
	xorl	%edx, %edx
	xorl	%r13d, %r13d
	movq	-2912(%rbp), %rdi
	movw	%si, -2882(%rbp)
	leaq	-2882(%rbp), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L642
.L663:
	movq	$384, -3112(%rbp)
	xorl	%r12d, %r12d
	leaq	-2688(%rbp), %r13
	movq	%r15, %r9
	movq	$320, -3104(%rbp)
	movq	$192, -3096(%rbp)
	movq	$896, -3088(%rbp)
	movq	$640, -3080(%rbp)
	movq	$960, -3072(%rbp)
	movq	$1280, -3064(%rbp)
	movq	$1344, -3056(%rbp)
	movq	$448, -3048(%rbp)
	movq	$704, -3040(%rbp)
	movq	$768, -3032(%rbp)
	movq	$1024, -3024(%rbp)
	movq	$1088, -3016(%rbp)
	movq	$512, -3008(%rbp)
	movq	$576, -3000(%rbp)
	movq	$832, -2992(%rbp)
	movq	$1152, -2984(%rbp)
	movq	$1216, -2968(%rbp)
	movq	$128, -2960(%rbp)
	movq	$256, -2928(%rbp)
	movq	$1, -2944(%rbp)
	movl	$0, -2952(%rbp)
	movl	$1, -2972(%rbp)
	jmp	.L604
.L702:
	movq	-3120(%rbp), %rax
	movl	$65799, (%rax)
	jmp	.L660
.L708:
	movq	-2912(%rbp), %rdi
	xorl	%edx, %edx
	movl	$39, %r15d
	leaq	-2882(%rbp), %rsi
	movl	$1, %ecx
	movw	%r15w, -2882(%rbp)
	movl	$1, %r13d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	8(%r14), %edx
	jmp	.L626
.L665:
	movl	$3, %r13d
	jmp	.L626
.L701:
	movq	-2912(%rbp), %rdi
	movl	$39, %eax
	leaq	-2882(%rbp), %rsi
	xorl	%edx, %edx
	movl	$1, %ecx
	movw	%ax, -2882(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L660
.L714:
	movl	$39, %ecx
	movq	-2912(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rsi, -2920(%rbp)
	movw	%cx, -2882(%rbp)
	movl	$1, %ecx
	movl	$4, %r13d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-2920(%rbp), %rsi
	jmp	.L655
.L709:
	movq	-2912(%rbp), %rdi
	movl	$39, %r13d
	xorl	%edx, %edx
	leaq	-2882(%rbp), %rsi
	movl	$1, %ecx
	movw	%r13w, -2882(%rbp)
	xorl	%r13d, %r13d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	8(%r14), %edx
	jmp	.L626
.L715:
	movl	$39, %edx
	movl	$1, %ecx
	xorl	%r13d, %r13d
	movq	-2912(%rbp), %rdi
	movw	%dx, -2882(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -2920(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-2920(%rbp), %rsi
	jmp	.L657
.L710:
	movq	-2912(%rbp), %r15
	movl	$39, %r10d
	leaq	-2882(%rbp), %rsi
	xorl	%edx, %edx
	movl	$1, %ecx
	movw	%r10w, -2882(%rbp)
	movl	$1, %r13d
	movq	%r15, %rdi
	movq	%rsi, -2904(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$39, %r11d
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	-2904(%rbp), %rsi
	movl	$1, %ecx
	movw	%r11w, -2882(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	8(%r14), %edx
	jmp	.L626
.L703:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3655:
	.size	_ZN6icu_676number4impl18PatternStringUtils16convertLocalizedERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsEbR10UErrorCode, .-_ZN6icu_676number4impl18PatternStringUtils16convertLocalizedERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsEbR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE
	.type	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE, @function
_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE:
.LFB3656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movb	%r8b, -74(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%esi, -68(%rbp)
	movq	(%rdi), %rax
	cmpl	$1, %edx
	je	.L766
	call	*56(%rax)
	movl	-68(%rbp), %esi
	testb	%al, %al
	jne	.L767
	movb	$0, -73(%rbp)
.L765:
	movl	$256, %r15d
	xorl	%edx, %edx
.L719:
	movq	(%rbx), %rcx
	movq	24(%rcx), %rcx
	testb	%sil, %sil
	je	.L721
.L770:
	cmpl	$6, %r14d
	je	.L722
.L737:
	orl	%r14d, %r15d
.L722:
	cmpb	$1, %sil
	jne	.L723
	testb	%al, %al
	jne	.L723
	movl	%r15d, %esi
	movq	%rbx, %rdi
	cmpl	$2, %r13d
	je	.L724
	call	*%rcx
	movzbl	-73(%rbp), %edx
	addl	%edx, %eax
	movb	%dl, -68(%rbp)
	movl	%eax, -72(%rbp)
.L736:
	movzwl	8(%r12), %edx
	leaq	-58(%rbp), %r14
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	xorl	%r13d, %r13d
	movw	%ax, 8(%r12)
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jg	.L726
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L727:
	cmpb	$0, -73(%rbp)
	je	.L768
.L735:
	movl	$43, %eax
.L732:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movw	%ax, -58(%rbp)
	addl	$1, %r13d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	-72(%rbp), %r13d
	je	.L716
.L726:
	testl	%r13d, %r13d
	jne	.L746
	cmpb	$0, -68(%rbp)
	jne	.L727
.L746:
	movq	(%rbx), %rax
	cmpb	$0, -68(%rbp)
	movq	16(%rax), %rax
	je	.L729
	leal	-1(%r13), %edx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	*%rax
.L730:
	cmpw	$45, %ax
	jne	.L731
	cmpb	$0, -73(%rbp)
	jne	.L735
.L731:
	cmpw	$37, %ax
	jne	.L732
	cmpb	$0, -74(%rbp)
	movl	$8240, %esi
	cmovne	%esi, %eax
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L767:
	cmpl	$2, %r13d
	jne	.L769
	movq	(%rbx), %rcx
	movb	$0, -73(%rbp)
	movl	$768, %r15d
	movl	$512, %edx
	movq	24(%rcx), %rcx
	testb	%sil, %sil
	jne	.L770
	.p2align 4,,10
	.p2align 3
.L721:
	movl	%edx, %r15d
	cmpl	$6, %r14d
	jne	.L737
.L723:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	*%rcx
	movb	$0, -68(%rbp)
	movl	%eax, -72(%rbp)
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L729:
	movl	%r13d, %edx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L716:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L771
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L766:
	.cfi_restore_state
	call	*48(%rax)
	movq	%rbx, %rdi
	xorl	$1, %eax
	movb	%al, -73(%rbp)
	movq	(%rbx), %rax
	call	*56(%rax)
	movl	-68(%rbp), %esi
	testb	%al, %al
	je	.L765
	movq	(%rbx), %rax
	movl	%esi, -68(%rbp)
	movq	%rbx, %rdi
	call	*64(%rax)
	movzbl	-73(%rbp), %edi
	movl	-68(%rbp), %esi
	andl	%edi, %eax
	cmpb	$1, %al
	sbbl	%r15d, %r15d
	andl	$-512, %r15d
	addl	$768, %r15d
	cmpb	$1, %al
	sbbl	%edx, %edx
	notl	%edx
	andl	$512, %edx
	testb	%al, %al
	cmovne	%eax, %edi
	movb	%dil, -73(%rbp)
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L769:
	movq	(%rbx), %rax
	movl	%esi, -68(%rbp)
	movq	%rbx, %rdi
	movl	$256, %r15d
	call	*64(%rax)
	movb	$0, -73(%rbp)
	movl	-68(%rbp), %esi
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L724:
	call	*%rcx
	movb	$1, -68(%rbp)
	addl	$1, %eax
	movl	%eax, -72(%rbp)
	jmp	.L736
.L771:
	call	__stack_chk_fail@PLT
.L768:
	movl	$45, %eax
	jmp	.L732
	.cfi_endproc
.LFE3656:
	.size	_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE, .-_ZN6icu_676number4impl18PatternStringUtils26patternInfoToStringBuilderERKNS1_20AffixPatternProviderEbNS1_15PatternSignTypeENS_14StandardPlural4FormEbRNS_13UnicodeStringE
	.section	.text.unlikely
	.align 2
.LCOLDB17:
	.text
.LHOTB17:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE
	.type	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE, @function
_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE:
.LFB3657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$6, %edi
	ja	.L773
	leaq	.L775(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L775:
	.long	.L777-.L775
	.long	.L776-.L775
	.long	.L779-.L775
	.long	.L777-.L775
	.long	.L776-.L775
	.long	.L774-.L775
	.long	.L774-.L775
	.text
	.p2align 4,,10
	.p2align 3
.L776:
	cmpl	$1, %esi
	jbe	.L781
	subl	$2, %esi
	cmpl	$1, %esi
	ja	.L773
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L777:
	.cfi_restore_state
	cmpl	$1, %esi
	jbe	.L781
	subl	$2, %esi
	cmpl	$1, %esi
	ja	.L773
.L779:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L774:
	.cfi_restore_state
	cmpl	$3, %esi
	ja	.L773
	movl	%esi, %esi
	leaq	CSWTCH.200(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	(%rax,%rsi,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L781:
	.cfi_restore_state
	movl	$2, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE.cold, @function
_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE.cold:
.LFSB3657:
.L773:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3657:
	.text
	.size	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE, .-_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE.cold, .-_ZN6icu_676number4impl18PatternStringUtils18resolveSignDisplayE18UNumberSignDisplayNS1_6SignumE.cold
.LCOLDE17:
	.text
.LHOTE17:
	.section	.text._ZN6icu_676number4impl17ParsedPatternInfoD2Ev,"axG",@progbits,_ZN6icu_676number4impl17ParsedPatternInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl17ParsedPatternInfoD2Ev
	.type	_ZN6icu_676number4impl17ParsedPatternInfoD2Ev, @function
_ZN6icu_676number4impl17ParsedPatternInfoD2Ev:
.LFB4820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	296(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -296(%rdi)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r12), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	.cfi_endproc
.LFE4820:
	.size	_ZN6icu_676number4impl17ParsedPatternInfoD2Ev, .-_ZN6icu_676number4impl17ParsedPatternInfoD2Ev
	.weak	_ZN6icu_676number4impl17ParsedPatternInfoD1Ev
	.set	_ZN6icu_676number4impl17ParsedPatternInfoD1Ev,_ZN6icu_676number4impl17ParsedPatternInfoD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl18PatternStringUtils25propertiesToPatternStringERKNS1_23DecimalFormatPropertiesER10UErrorCode
	.type	_ZN6icu_676number4impl18PatternStringUtils25propertiesToPatternStringERKNS1_23DecimalFormatPropertiesER10UErrorCode, @function
_ZN6icu_676number4impl18PatternStringUtils25propertiesToPatternStringERKNS1_23DecimalFormatPropertiesER10UErrorCode:
.LFB3653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r13
	pushq	%r12
	movq	%r13, %xmm3
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2408, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -2392(%rbp)
	movq	.LC18(%rip), %xmm0
	movq	.LC19(%rip), %xmm1
	punpcklqdq	%xmm3, %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%r8w, 8(%rdi)
	movhps	.LC18(%rip), %xmm1
	movq	%r13, (%rdi)
	movl	72(%rsi), %edi
	movl	$100, %esi
	movaps	%xmm0, -2368(%rbp)
	movaps	%xmm1, -2352(%rbp)
	call	uprv_min_67@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	call	uprv_max_67@PLT
	movl	752(%r14), %edi
	movl	$100, %esi
	movl	%eax, -2332(%rbp)
	call	uprv_min_67@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	call	uprv_max_67@PLT
	movl	68(%r14), %edi
	movl	$100, %esi
	movl	%eax, -2336(%rbp)
	movzbl	76(%r14), %eax
	movb	%al, -2381(%rbp)
	call	uprv_min_67@PLT
	leaq	392(%r14), %rsi
	movl	%eax, -2400(%rbp)
	movzbl	384(%r14), %eax
	movb	%al, -2383(%rbp)
	movl	388(%r14), %eax
	movl	%eax, -2444(%rbp)
	leaq	-2240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2408(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	108(%r14), %edi
	movl	$100, %esi
	call	uprv_min_67@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	call	uprv_max_67@PLT
	movl	88(%r14), %edi
	movl	$100, %esi
	movl	%eax, -2420(%rbp)
	call	uprv_min_67@PLT
	movl	100(%r14), %edi
	movl	$100, %esi
	movl	%eax, -2424(%rbp)
	call	uprv_min_67@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	call	uprv_max_67@PLT
	movl	84(%r14), %edi
	movl	$100, %esi
	movl	%eax, -2328(%rbp)
	call	uprv_min_67@PLT
	movl	112(%r14), %edi
	movl	$100, %esi
	movl	%eax, -2380(%rbp)
	call	uprv_min_67@PLT
	movl	92(%r14), %edi
	movl	$100, %esi
	movl	%eax, %r15d
	call	uprv_min_67@PLT
	movl	96(%r14), %edi
	movl	$100, %esi
	movl	%eax, %ebx
	movzbl	65(%r14), %eax
	movb	%al, -2382(%rbp)
	call	uprv_min_67@PLT
	movl	$2, %r9d
	movl	$2, %edx
	movdqa	-2368(%rbp), %xmm0
	movl	%eax, -2396(%rbp)
	movzbl	66(%r14), %eax
	movl	$2, %r10d
	movl	$2, %r11d
	movl	$2, %ecx
	movl	$2, %esi
	movdqa	-2352(%rbp), %xmm1
	movl	$2, %edi
	movb	%al, -2384(%rbp)
	movl	$2, %eax
	movl	$2, %r8d
	movw	%ax, -1776(%rbp)
	movl	$2, %eax
	movw	%ax, -1688(%rbp)
	movl	$2, %eax
	movw	%ax, -1624(%rbp)
	movl	$2, %eax
	movw	%ax, -1560(%rbp)
	movl	$2, %eax
	movw	%ax, -1496(%rbp)
	movl	$2, %eax
	movw	%r9w, -1968(%rbp)
	movl	$2, %r9d
	movw	%r10w, -1904(%rbp)
	movl	$2, %r10d
	movw	%r11w, -1840(%rbp)
	movl	$2, %r11d
	movw	%dx, -1416(%rbp)
	movl	$2, %edx
	movw	%cx, -1352(%rbp)
	movl	$2, %ecx
	movw	%si, -1288(%rbp)
	movl	$2, %esi
	movw	%di, -1224(%rbp)
	movl	$2, %edi
	movq	%r13, -1912(%rbp)
	movq	%r13, -1848(%rbp)
	movq	%r13, -1784(%rbp)
	movb	$1, -1719(%rbp)
	movq	%r13, -1696(%rbp)
	movq	%r13, -1632(%rbp)
	movq	%r13, -1568(%rbp)
	movq	%r13, -1504(%rbp)
	movb	$1, -1439(%rbp)
	movq	%r13, -1360(%rbp)
	movq	%r13, -1296(%rbp)
	movq	%r13, -1232(%rbp)
	movaps	%xmm0, -1984(%rbp)
	movaps	%xmm1, -1712(%rbp)
	movups	%xmm0, -1432(%rbp)
	movb	$1, -1167(%rbp)
	movw	%ax, -872(%rbp)
	movl	$2, %eax
	movw	%ax, -808(%rbp)
	movl	$2, %eax
	movw	%ax, -744(%rbp)
	movl	$2, %eax
	movw	%r8w, -1144(%rbp)
	movl	$2, %r8d
	movw	%r9w, -1080(%rbp)
	movl	$2, %r9d
	movw	%ax, -680(%rbp)
	movl	$2, %eax
	movw	%r10w, -1016(%rbp)
	movl	$2, %r10d
	movw	%ax, -600(%rbp)
	leaq	-1984(%rbp), %rax
	movw	%si, -408(%rbp)
	movq	%r13, -1088(%rbp)
	movq	%r13, -1024(%rbp)
	movq	%r13, -960(%rbp)
	movw	%r11w, -952(%rbp)
	movb	$1, -895(%rbp)
	movq	%r13, -816(%rbp)
	movq	%r13, -752(%rbp)
	movq	%r13, -688(%rbp)
	movb	$1, -623(%rbp)
	movq	%r13, -544(%rbp)
	movw	%dx, -536(%rbp)
	movq	%r13, -480(%rbp)
	movw	%cx, -472(%rbp)
	movq	%r13, -416(%rbp)
	movb	$1, -351(%rbp)
	movw	%di, -328(%rbp)
	movq	%r13, -272(%rbp)
	movw	%r8w, -264(%rbp)
	movq	%r13, -208(%rbp)
	movups	%xmm0, -1160(%rbp)
	movups	%xmm0, -888(%rbp)
	movups	%xmm0, -616(%rbp)
	movups	%xmm0, -344(%rbp)
	movw	%r9w, -200(%rbp)
	movq	48(%r14), %rsi
	movq	%r13, -144(%rbp)
	movw	%r10w, -136(%rbp)
	movb	$1, -79(%rbp)
	movb	$1, -72(%rbp)
	testq	%rsi, %rsi
	je	.L929
	leaq	-1712(%rbp), %rcx
	movq	%r14, %rdx
	movq	%rax, -2368(%rbp)
	movq	%rcx, %rdi
	movq	-2392(%rbp), %rcx
	movq	%rdi, -2376(%rbp)
	call	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProvider5setToERKNS_18CurrencyPluralInfoERKNS1_23DecimalFormatPropertiesER10UErrorCode@PLT
	movq	-2368(%rbp), %rsi
	cmpb	$0, -72(%rbp)
	cmove	-2376(%rbp), %rsi
.L787:
	leaq	-2048(%rbp), %rax
	movl	$256, %edx
	movq	%rax, -2352(%rbp)
	movq	%rax, %rdi
	movq	(%rsi), %rax
	call	*32(%rax)
	movswl	-2040(%rbp), %ecx
	testw	%cx, %cx
	js	.L788
	sarl	$5, %ecx
.L789:
	movq	-2352(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-2352(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L790
	sarl	$5, %eax
	movl	%eax, -2432(%rbp)
.L791:
	cmpb	$0, -2381(%rbp)
	je	.L878
	movl	-2332(%rbp), %eax
	movl	-2336(%rbp), %edx
	leal	1(%rax,%rdx), %edi
	movl	%edi, -2416(%rbp)
	cmpl	%edx, %eax
	je	.L930
.L792:
	movl	$2, %edi
	movl	$-1, %esi
	movsd	736(%r14), %xmm4
	movq	%r13, -2176(%rbp)
	movw	%di, -2168(%rbp)
	movl	$100, %edi
	movsd	%xmm4, -2440(%rbp)
	call	uprv_min_67@PLT
	cmpl	%ebx, %eax
	je	.L794
	leaq	-2176(%rbp), %r13
	leaq	-2320(%rbp), %r14
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L931:
	movswl	%si, %eax
	sarl	$5, %eax
	cmpl	%eax, %r15d
	jle	.L797
.L796:
	movl	$64, %esi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%si, -2320(%rbp)
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L798:
	movswl	-2168(%rbp), %esi
	testw	%si, %si
	jns	.L931
	cmpl	-2164(%rbp), %r15d
	jg	.L796
.L797:
	leaq	-2320(%rbp), %r14
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L932:
	movswl	%si, %eax
	sarl	$5, %eax
	cmpl	%eax, %ebx
	jle	.L879
.L800:
	movl	$35, %ecx
	movq	%r14, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%cx, -2320(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	-2168(%rbp), %esi
.L802:
	testw	%si, %si
	jns	.L932
	cmpl	-2164(%rbp), %ebx
	jg	.L800
.L879:
	movl	$0, -2428(%rbp)
	xorl	%ebx, %ebx
.L801:
	movl	-2420(%rbp), %r15d
	leaq	-2320(%rbp), %r14
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L934:
	movl	%esi, %eax
	sarl	$5, %eax
	addl	%ebx, %eax
	cmpl	%eax, %r15d
	jle	.L933
.L815:
	movl	$48, %edx
	xorl	%esi, %esi
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movw	%dx, -2320(%rbp)
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movswl	-2168(%rbp), %esi
.L818:
	testw	%si, %si
	jns	.L934
	movl	-2164(%rbp), %esi
	leal	(%rbx,%rsi), %eax
	cmpl	%r15d, %eax
	jl	.L815
	movl	-2428(%rbp), %ecx
	cmpl	%ecx, -2328(%rbp)
	jg	.L875
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L933:
	movl	-2428(%rbp), %ecx
	cmpl	%ecx, -2328(%rbp)
	jle	.L874
.L875:
	movl	%ebx, %r14d
	movl	%ebx, -2420(%rbp)
	leaq	-2320(%rbp), %r15
	negl	%r14d
	movl	%r14d, %ebx
	movl	-2328(%rbp), %r14d
	.p2align 4,,10
	.p2align 3
.L819:
	movl	$48, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	addl	$1, %ebx
	movw	%ax, -2320(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	%ebx, %r14d
	jg	.L819
	movl	-2420(%rbp), %ebx
	movl	-2328(%rbp), %ecx
	movl	$1, %edx
	movswl	-2168(%rbp), %esi
	subl	%ebx, %edx
	leal	-1(%rcx,%rbx), %eax
	negl	%eax
	cmpl	%edx, %ecx
	movl	$0, %edx
	cmovl	%edx, %eax
	leal	-1(%rbx,%rax), %ebx
	testw	%si, %si
	js	.L935
.L874:
	sarl	$5, %esi
.L821:
	movl	-2416(%rbp), %edi
	addl	%ebx, %esi
	call	uprv_max_67@PLT
	movl	%eax, %esi
	leal	-1(%rax), %r14d
	movl	-2424(%rbp), %eax
	cmpl	$100, %eax
	je	.L823
	movl	%eax, %edi
	call	uprv_max_67@PLT
	leal	-1(%rax), %r14d
.L823:
	movl	-2380(%rbp), %ecx
	movl	%ebx, %eax
	cmpl	$100, %ecx
	je	.L824
	movl	%ecx, %edi
	movl	%ebx, %esi
	negl	%edi
	call	uprv_min_67@PLT
.L824:
	cmpl	%eax, %r14d
	jl	.L841
	movl	%eax, %edx
	movzbl	-2382(%rbp), %edi
	subl	$1, %eax
	movq	%r13, -2416(%rbp)
	shrl	$31, %edx
	movl	%r14d, %r13d
	movl	%eax, -2328(%rbp)
	movzbl	-2381(%rbp), %r14d
	orl	%edx, %edi
	leaq	-2320(%rbp), %r15
	movb	%dil, -2380(%rbp)
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L938:
	movswl	%cx, %edx
	sarl	$5, %edx
	leal	(%rdx,%rbx), %eax
	subl	%r13d, %eax
	subl	$1, %eax
	js	.L889
.L939:
	cmpl	%edx, %eax
	jge	.L889
	movl	$-1, %esi
	cmpl	%eax, %edx
	jbe	.L833
	leaq	-2166(%rbp), %rdx
	andl	$2, %ecx
	cltq
	cmove	-2152(%rbp), %rdx
	movzwl	(%rdx,%rax,2), %esi
.L833:
	movw	%si, -2320(%rbp)
.L928:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	testl	%r13d, %r13d
	je	.L936
	testb	%r14b, %r14b
	je	.L838
	testl	%r13d, %r13d
	jle	.L839
	cmpl	%r13d, -2332(%rbp)
	jne	.L839
	movl	$44, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movw	%ax, -2320(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L838:
	subl	$1, %r13d
	cmpl	-2328(%rbp), %r13d
	je	.L937
.L842:
	movzwl	-2168(%rbp), %ecx
	testw	%cx, %cx
	jns	.L938
	movl	-2164(%rbp), %edx
	leal	(%rdx,%rbx), %eax
	subl	%r13d, %eax
	subl	$1, %eax
	jns	.L939
.L889:
	movl	$35, %eax
	movw	%ax, -2320(%rbp)
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L936:
	cmpb	$0, -2380(%rbp)
	jne	.L940
.L836:
	testb	%r14b, %r14b
	je	.L838
.L839:
	movl	-2332(%rbp), %ecx
	cmpl	%r13d, %ecx
	jge	.L838
	movl	-2336(%rbp), %edx
	testl	%edx, %edx
	jle	.L838
	movl	%r13d, %eax
	subl	%ecx, %eax
	movl	%edx, %ecx
	cltd
	idivl	%ecx
	testl	%edx, %edx
	jne	.L838
	movl	$44, %eax
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movw	%ax, -2320(%rbp)
	subl	$1, %r13d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	-2328(%rbp), %r13d
	jne	.L842
	.p2align 4,,10
	.p2align 3
.L937:
	movq	-2416(%rbp), %r13
.L841:
	movl	$-1, %esi
	movl	$100, %edi
	call	uprv_min_67@PLT
	cmpl	-2396(%rbp), %eax
	jne	.L941
.L827:
	movswl	8(%r12), %ebx
	movl	%ebx, %eax
	sarl	$5, %ebx
	testw	%ax, %ax
	jns	.L847
	movl	12(%r12), %ebx
.L847:
	cmpb	$0, -72(%rbp)
	movq	-2368(%rbp), %rsi
	cmove	-2376(%rbp), %rsi
	movq	-2352(%rbp), %rdi
	xorl	%edx, %edx
	movq	(%rsi), %rax
	call	*32(%rax)
	movswl	-2040(%rbp), %ecx
	testw	%cx, %cx
	js	.L849
	sarl	$5, %ecx
.L850:
	movq	-2352(%rbp), %r15
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-2400(%rbp), %r15d
	testl	%r15d, %r15d
	jle	.L851
	cmpb	$0, -2383(%rbp)
	jne	.L851
	leaq	-2320(%rbp), %r14
	movq	%r13, -2328(%rbp)
	movq	%r12, %r13
	movq	%r14, %r12
	movl	-2432(%rbp), %r14d
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L942:
	sarl	$5, %eax
	movl	%r15d, %ecx
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L926
.L853:
	movl	$35, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	xorl	%edx, %edx
	movw	%r9w, -2320(%rbp)
	movl	%r14d, %esi
	movl	$1, %r9d
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	addl	$1, %ebx
.L855:
	movswl	8(%r13), %eax
	testw	%ax, %ax
	jns	.L942
	movl	%r15d, %eax
	subl	12(%r13), %eax
	testl	%eax, %eax
	jg	.L853
.L926:
	movl	-2444(%rbp), %eax
	movq	%r13, %r12
	movq	-2328(%rbp), %r13
	cmpl	$2, %eax
	je	.L856
	ja	.L857
	testl	%eax, %eax
	je	.L943
	movq	-2352(%rbp), %r14
	movq	-2408(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	-2432(%rbp), %r15d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-2392(%rbp), %rcx
	movq	%r14, -2352(%rbp)
	movl	%r15d, %edx
	call	_ZN6icu_676number4impl18PatternStringUtils19escapePaddingStringENS_13UnicodeStringERS3_iR10UErrorCode
	movq	-2352(%rbp), %rdi
	movl	%eax, %r14d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addl	$1, %r14d
	movl	$42, %edi
	xorl	%edx, %edx
	movw	%di, -2320(%rbp)
	movl	%r15d, %esi
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-2320(%rbp), %rcx
	movl	$1, %r9d
	addl	%r14d, %r15d
	addl	%r14d, %ebx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movl	%r15d, -2432(%rbp)
	.p2align 4,,10
	.p2align 3
.L861:
	movq	-2392(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L864
	.p2align 4,,10
	.p2align 3
.L851:
	cmpb	$0, -72(%rbp)
	movq	-2368(%rbp), %rdi
	cmove	-2376(%rbp), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	testb	%al, %al
	jne	.L944
.L864:
	movq	%r13, %rdi
	leaq	-1976(%rbp), %rbx
	leaq	-344(%rbp), %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE(%rip), %rax
	movq	%rax, -1712(%rbp)
	.p2align 4,,10
	.p2align 3
.L873:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	subq	$272, %r13
	call	*(%rax)
	cmpq	%rbx, %r13
	jne	.L873
	movq	-2376(%rbp), %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE(%rip), %rax
	leaq	-1784(%rbp), %rdi
	movq	%rax, -1984(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-1848(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-1912(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-2368(%rbp), %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	-2408(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L945
	addq	$2408, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L940:
	.cfi_restore_state
	movl	$46, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movw	%ax, -2320(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L878:
	movl	$1, -2416(%rbp)
	movl	$0, -2336(%rbp)
	movl	$0, -2332(%rbp)
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L857:
	cmpl	$3, -2444(%rbp)
	jne	.L861
	movl	$42, %ecx
	leaq	-2320(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%cx, -2320(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L862
	movswl	%ax, %r14d
	sarl	$5, %r14d
.L863:
	movq	-2352(%rbp), %r15
	movq	-2408(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r15, %rdi
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	-2392(%rbp), %rcx
	call	_ZN6icu_676number4impl18PatternStringUtils19escapePaddingStringENS_13UnicodeStringERS3_iR10UErrorCode
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L941:
	xorl	%edx, %edx
	movl	$69, %ebx
	movl	$1, %ecx
	movq	%r12, %rdi
	leaq	-2320(%rbp), %r15
	movw	%bx, -2320(%rbp)
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpb	$0, -2384(%rbp)
	jne	.L946
.L845:
	movl	-2396(%rbp), %r14d
	testl	%r14d, %r14d
	jle	.L827
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L846:
	movl	$48, %r10d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	addl	$1, %ebx
	movw	%r10w, -2320(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	%ebx, %r14d
	jne	.L846
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L790:
	movl	12(%r12), %eax
	movl	%eax, -2432(%rbp)
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L788:
	movl	-2036(%rbp), %ecx
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L849:
	movl	-2036(%rbp), %ecx
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L935:
	movl	-2164(%rbp), %esi
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L794:
	movsd	-2440(%rbp), %xmm5
	ucomisd	.LC1(%rip), %xmm5
	jp	.L803
	jne	.L803
.L807:
	movswl	-2168(%rbp), %esi
	xorl	%ebx, %ebx
	leaq	-2176(%rbp), %r13
	movl	$0, -2428(%rbp)
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L929:
	movq	-2392(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -2368(%rbp)
	call	_ZN6icu_676number4impl30PropertiesAffixPatternProvider5setToERKNS1_23DecimalFormatPropertiesER10UErrorCode@PLT
	leaq	-1712(%rbp), %rax
	movb	$1, -72(%rbp)
	movq	-2368(%rbp), %rsi
	movq	%rax, -2376(%rbp)
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L944:
	xorl	%edx, %edx
	movl	$59, %eax
	movl	$1, %ecx
	movq	%r12, %rdi
	leaq	-2320(%rbp), %rsi
	movw	%ax, -2320(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-2368(%rbp), %rsi
	cmpb	$0, -72(%rbp)
	movl	$768, %edx
	cmove	-2376(%rbp), %rsi
	movq	-2352(%rbp), %rdi
	movq	(%rsi), %rax
	call	*32(%rax)
	movzwl	-2040(%rbp), %eax
	testw	%ax, %ax
	js	.L868
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L869:
	movq	-2352(%rbp), %r15
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-2112(%rbp), %r14
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	-2432(%rbp), %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	subl	%edx, %ebx
	movl	%ebx, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-2368(%rbp), %rsi
	cmpb	$0, -72(%rbp)
	movq	%r15, %rdi
	cmove	-2376(%rbp), %rsi
	movl	$512, %edx
	movq	(%rsi), %rax
	call	*32(%rax)
	movzwl	-2040(%rbp), %eax
	testw	%ax, %ax
	js	.L871
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L872:
	movq	-2352(%rbp), %rbx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L930:
	movl	$0, -2332(%rbp)
	addl	$1, %eax
	movl	%eax, -2416(%rbp)
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L803:
	movl	-2380(%rbp), %edx
	testl	%edx, %edx
	js	.L804
	movsd	-2440(%rbp), %xmm6
	movsd	.LC3(%rip), %xmm1
	xorl	%eax, %eax
	movsd	.LC4(%rip), %xmm2
	movapd	%xmm6, %xmm0
	addsd	%xmm6, %xmm0
	.p2align 4,,10
	.p2align 3
.L808:
	comisd	%xmm0, %xmm1
	jb	.L919
	mulsd	%xmm2, %xmm0
	addl	$1, %eax
	cmpl	%eax, %edx
	jge	.L808
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L943:
	movq	-2352(%rbp), %r15
	movq	-2408(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	-2392(%rbp), %rcx
	call	_ZN6icu_676number4impl18PatternStringUtils19escapePaddingStringENS_13UnicodeStringERS3_iR10UErrorCode
	movq	%r15, %rdi
	movl	%eax, %r14d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$42, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movw	%r8w, -2320(%rbp)
	addl	$1, %r14d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-2320(%rbp), %rcx
	movl	$1, %r9d
	addl	%r14d, %ebx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	addl	%r14d, -2432(%rbp)
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L868:
	movl	-2036(%rbp), %ecx
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L946:
	movl	$43, %r11d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movw	%r11w, -2320(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L871:
	movl	-2036(%rbp), %ecx
	jmp	.L872
.L919:
	cmpl	%eax, -2380(%rbp)
	jl	.L807
.L804:
	movsd	-2440(%rbp), %xmm0
	xorl	%edi, %edi
	leaq	-2320(%rbp), %r15
	call	_ZN6icu_676number4impl13roundingutils20doubleFractionLengthEdPa@PLT
	movq	%r15, %rdi
	movswl	%ax, %r14d
	movl	%r14d, -2428(%rbp)
	movl	%r14d, %ebx
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movsd	-2440(%rbp), %xmm0
	movq	%r15, %rdi
	negl	%ebx
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi@PLT
	movl	$4, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	-2392(%rbp), %rcx
	call	_ZN6icu_676number4impl15DecimalQuantity16roundToMagnitudeEi25UNumberFormatRoundingModeR10UErrorCode@PLT
	movq	-2352(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNK6icu_676number4impl15DecimalQuantity13toPlainStringEv@PLT
	movzwl	-2040(%rbp), %eax
	testw	%ax, %ax
	js	.L809
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L810:
	testl	%ecx, %ecx
	je	.L811
	leaq	-2038(%rbp), %rdx
	testb	$2, %al
	movq	%rdx, %rax
	cmove	-2024(%rbp), %rax
	cmpw	$45, (%rax)
	je	.L947
.L811:
	leaq	-2176(%rbp), %r13
	movq	-2352(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
.L813:
	movq	-2352(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movswl	-2168(%rbp), %esi
	jmp	.L801
.L856:
	movq	-2352(%rbp), %r15
	movq	-2408(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	-2392(%rbp), %rcx
	call	_ZN6icu_676number4impl18PatternStringUtils19escapePaddingStringENS_13UnicodeStringERS3_iR10UErrorCode
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$42, %esi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movw	%si, -2320(%rbp)
	leaq	-2320(%rbp), %rcx
	movl	%ebx, %esi
	movq	%r12, %rdi
	movl	$1, %r9d
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L861
.L862:
	movl	12(%r12), %r14d
	jmp	.L863
.L809:
	movl	-2036(%rbp), %ecx
	jmp	.L810
.L947:
	leaq	-2176(%rbp), %r13
	movq	-2352(%rbp), %rsi
	subl	$1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L813
.L945:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3653:
	.size	_ZN6icu_676number4impl18PatternStringUtils25propertiesToPatternStringERKNS1_23DecimalFormatPropertiesER10UErrorCode, .-_ZN6icu_676number4impl18PatternStringUtils25propertiesToPatternStringERKNS1_23DecimalFormatPropertiesER10UErrorCode
	.section	.text._ZN6icu_676number4impl17ParsedPatternInfoD0Ev,"axG",@progbits,_ZN6icu_676number4impl17ParsedPatternInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev
	.type	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev, @function
_ZN6icu_676number4impl17ParsedPatternInfoD0Ev:
.LFB4822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	296(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -296(%rdi)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r12), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4822:
	.size	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev, .-_ZN6icu_676number4impl17ParsedPatternInfoD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl13PatternParser29parseToExistingPropertiesImplERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode
	.type	_ZN6icu_676number4impl13PatternParser29parseToExistingPropertiesImplERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode, @function
_ZN6icu_676number4impl13PatternParser29parseToExistingPropertiesImplERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode:
.LFB3650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -504(%rbp)
	movl	%edx, -508(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L951
	sarl	$5, %eax
	testl	%eax, %eax
	je	.L982
.L953:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-368(%rbp), %r13
	movq	.LC20(%rip), %xmm0
	movl	$2, %esi
	movq	%rax, %xmm1
	movq	%r13, %rdi
	movabsq	$281474976645120, %r14
	movq	%r8, -520(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movw	%si, -480(%rbp)
	leaq	-488(%rbp), %rbx
	leaq	-496(%rbp), %r15
	movaps	%xmm0, -496(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r14, -424(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movb	$0, -384(%rbp)
	movl	$0, -380(%rbp)
	movb	$0, -376(%rbp)
	movl	$0, -372(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	%r14, -256(%rbp)
	pxor	%xmm0, %xmm0
	leaq	-200(%rbp), %r14
	movq	%r14, %rdi
	movups	%xmm0, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movups	%xmm0, -232(%rbp)
	movb	$0, -296(%rbp)
	movq	$0, -292(%rbp)
	movb	$0, -284(%rbp)
	movq	$0, -264(%rbp)
	movb	$0, -216(%rbp)
	movl	$0, -212(%rbp)
	movb	$0, -208(%rbp)
	movl	$0, -204(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movl	(%r12), %edi
	pxor	%xmm0, %xmm0
	movb	$0, -128(%rbp)
	movq	$0, -124(%rbp)
	movq	-520(%rbp), %r8
	testl	%edi, %edi
	movb	$0, -116(%rbp)
	movq	$0, -96(%rbp)
	movq	%rbx, -88(%rbp)
	movl	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	movaps	%xmm0, -112(%rbp)
	jle	.L983
.L969:
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
.L950:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L984
	addq	$488, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L983:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	-424(%rbp), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN6icu_676number4impl17ParsedPatternInfo17consumeSubpatternER10UErrorCode
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L969
	movq	-88(%rbp), %rdi
	movl	-80(%rbp), %r8d
	movzwl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L956
	movswl	%ax, %edx
	movl	%edx, %esi
	sarl	$5, %esi
	cmpl	%esi, %r8d
	je	.L957
.L958:
	movl	%r8d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$59, %eax
	je	.L960
.L966:
	movq	-88(%rbp), %rdi
	movl	-80(%rbp), %esi
	movswl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L959
.L957:
	sarl	$5, %edx
.L967:
	cmpl	%esi, %edx
	je	.L968
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$-1, %eax
	je	.L968
	movl	$65555, (%r12)
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L951:
	movl	12(%rdi), %eax
	testl	%eax, %eax
	jne	.L953
.L982:
	movq	-504(%rbp), %rdi
	call	_ZN6icu_676number4impl23DecimalFormatProperties5clearEv@PLT
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L968:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L969
	movl	-508(%rbp), %edx
	movq	-504(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r15, %rsi
	call	_ZN6icu_676number4impl13PatternParser23patternInfoToPropertiesERNS1_23DecimalFormatPropertiesERNS1_17ParsedPatternInfoENS1_14IgnoreRoundingER10UErrorCode
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L956:
	movl	12(%rdi), %esi
	cmpl	%esi, %r8d
	jne	.L958
	.p2align 4,,10
	.p2align 3
.L959:
	movl	12(%rdi), %edx
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L960:
	movq	-88(%rbp), %r8
	movl	-80(%rbp), %r10d
	movswl	8(%r8), %esi
	testw	%si, %si
	js	.L961
	sarl	$5, %esi
.L962:
	cmpl	%esi, %r10d
	je	.L972
	movl	%r10d, %esi
	movq	%r8, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	-80(%rbp), %esi
	cmpl	$65535, %eax
	jbe	.L973
	movq	-88(%rbp), %r8
	movl	$2, %r10d
.L963:
	movzwl	8(%r8), %eax
	addl	%esi, %r10d
	movq	%r8, %rdi
	movl	%r10d, -80(%rbp)
	testw	%ax, %ax
	js	.L964
	movswl	%ax, %edx
	movl	%edx, %esi
	sarl	$5, %esi
	cmpl	%esi, %r10d
	je	.L957
.L965:
	movl	%r10d, %esi
	movq	%r8, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$-1, %eax
	je	.L966
	leaq	-256(%rbp), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	movb	$1, -64(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN6icu_676number4impl17ParsedPatternInfo17consumeSubpatternER10UErrorCode
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L966
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L973:
	movq	-88(%rbp), %r8
	movl	$1, %r10d
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L961:
	movl	12(%r8), %esi
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L964:
	movl	12(%r8), %esi
	cmpl	%esi, %r10d
	jne	.L965
	jmp	.L959
.L972:
	movl	$2, %r10d
	jmp	.L963
.L984:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3650:
	.size	_ZN6icu_676number4impl13PatternParser29parseToExistingPropertiesImplERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode, .-_ZN6icu_676number4impl13PatternParser29parseToExistingPropertiesImplERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl13PatternParser17parseToPropertiesERKNS_13UnicodeStringENS1_14IgnoreRoundingER10UErrorCode
	.type	_ZN6icu_676number4impl13PatternParser17parseToPropertiesERKNS_13UnicodeStringENS1_14IgnoreRoundingER10UErrorCode, @function
_ZN6icu_676number4impl13PatternParser17parseToPropertiesERKNS_13UnicodeStringENS1_14IgnoreRoundingER10UErrorCode:
.LFB3622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN6icu_676number4impl23DecimalFormatPropertiesC1Ev@PLT
	movq	%r15, %rcx
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl13PatternParser29parseToExistingPropertiesImplERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3622:
	.size	_ZN6icu_676number4impl13PatternParser17parseToPropertiesERKNS_13UnicodeStringENS1_14IgnoreRoundingER10UErrorCode, .-_ZN6icu_676number4impl13PatternParser17parseToPropertiesERKNS_13UnicodeStringENS1_14IgnoreRoundingER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl13PatternParser17parseToPropertiesERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl13PatternParser17parseToPropertiesERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl13PatternParser17parseToPropertiesERKNS_13UnicodeStringER10UErrorCode:
.LFB3626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_676number4impl23DecimalFormatPropertiesC1Ev@PLT
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%edx, %edx
	call	_ZN6icu_676number4impl13PatternParser29parseToExistingPropertiesImplERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3626:
	.size	_ZN6icu_676number4impl13PatternParser17parseToPropertiesERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl13PatternParser17parseToPropertiesERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode
	.type	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode, @function
_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode:
.LFB3627:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_676number4impl13PatternParser29parseToExistingPropertiesImplERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode
	.cfi_endproc
.LFE3627:
	.size	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode, .-_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode
	.section	.rodata
	.align 16
	.type	CSWTCH.200, @object
	.size	CSWTCH.200, 16
CSWTCH.200:
	.long	2
	.long	0
	.long	0
	.long	1
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_676number4impl17ParsedPatternInfoE
	.section	.rodata._ZTSN6icu_676number4impl17ParsedPatternInfoE,"aG",@progbits,_ZTSN6icu_676number4impl17ParsedPatternInfoE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl17ParsedPatternInfoE, @object
	.size	_ZTSN6icu_676number4impl17ParsedPatternInfoE, 41
_ZTSN6icu_676number4impl17ParsedPatternInfoE:
	.string	"N6icu_676number4impl17ParsedPatternInfoE"
	.weak	_ZTIN6icu_676number4impl17ParsedPatternInfoE
	.section	.data.rel.ro._ZTIN6icu_676number4impl17ParsedPatternInfoE,"awG",@progbits,_ZTIN6icu_676number4impl17ParsedPatternInfoE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl17ParsedPatternInfoE, @object
	.size	_ZTIN6icu_676number4impl17ParsedPatternInfoE, 56
_ZTIN6icu_676number4impl17ParsedPatternInfoE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl17ParsedPatternInfoE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl20AffixPatternProviderE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTVN6icu_676number4impl17ParsedPatternInfoE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl17ParsedPatternInfoE,"awG",@progbits,_ZTVN6icu_676number4impl17ParsedPatternInfoE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl17ParsedPatternInfoE, @object
	.size	_ZTVN6icu_676number4impl17ParsedPatternInfoE, 104
_ZTVN6icu_676number4impl17ParsedPatternInfoE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl17ParsedPatternInfoE
	.quad	_ZN6icu_676number4impl17ParsedPatternInfoD1Ev
	.quad	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev
	.quad	_ZNK6icu_676number4impl17ParsedPatternInfo6charAtEii
	.quad	_ZNK6icu_676number4impl17ParsedPatternInfo6lengthEi
	.quad	_ZNK6icu_676number4impl17ParsedPatternInfo9getStringEi
	.quad	_ZNK6icu_676number4impl17ParsedPatternInfo15hasCurrencySignEv
	.quad	_ZNK6icu_676number4impl17ParsedPatternInfo19positiveHasPlusSignEv
	.quad	_ZNK6icu_676number4impl17ParsedPatternInfo21hasNegativeSubpatternEv
	.quad	_ZNK6icu_676number4impl17ParsedPatternInfo20negativeHasMinusSignEv
	.quad	_ZNK6icu_676number4impl17ParsedPatternInfo18containsSymbolTypeENS1_16AffixPatternTypeER10UErrorCode
	.quad	_ZNK6icu_676number4impl17ParsedPatternInfo7hasBodyEv
	.section	.rodata
	.align 2
	.type	_ZN6icu_676number4implL22kFallbackPaddingStringE, @object
	.size	_ZN6icu_676number4implL22kFallbackPaddingStringE, 4
_ZN6icu_676number4implL22kFallbackPaddingStringE:
	.string	" "
	.string	""
	.string	""
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	0
	.align 8
.LC3:
	.long	0
	.long	1072693248
	.align 8
.LC4:
	.long	0
	.long	1076101120
	.section	.data.rel.ro,"aw"
	.align 8
.LC18:
	.quad	_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE+16
	.align 8
.LC19:
	.quad	_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE+16
	.section	.data.rel.ro.local,"aw"
	.align 8
.LC20:
	.quad	_ZTVN6icu_676number4impl17ParsedPatternInfoE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
