	.file	"double-conversion-fast-dtoa.cpp"
	.text
	.p2align 4
	.type	_ZN6icu_6717double_conversionL16RoundWeedCountedENS0_6VectorIcEEimmmPi.isra.0, @function
_ZN6icu_6717double_conversionL16RoundWeedCountedENS0_6VectorIcEEimmmPi.isra.0:
.LFB192:
	.cfi_startproc
	xorl	%eax, %eax
	cmpq	%rcx, %r8
	jnb	.L1
	movq	%rcx, %r10
	subq	%r8, %r10
	cmpq	%r10, %r8
	jnb	.L1
	subq	%rdx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L3
	movq	%rcx, %r10
	leaq	(%r8,%r8), %rax
	subq	%rdx, %r10
	cmpq	%rax, %r10
	jnb	.L13
.L3:
	xorl	%eax, %eax
	cmpq	%rdx, %r8
	jnb	.L1
	addq	%r8, %rcx
	subq	%r8, %rdx
	cmpq	%rdx, %rcx
	ja	.L1
	leal	-1(%rsi), %edx
	movslq	%edx, %rax
	addq	%rdi, %rax
	addb	$1, (%rax)
	testl	%edx, %edx
	jle	.L7
	movslq	%esi, %rdx
	leal	-2(%rsi), %ecx
	leaq	-2(%rdi,%rdx), %rdx
	subq	%rcx, %rdx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L15:
	addb	$1, -1(%rax)
	subq	$1, %rax
	movb	$48, 1(%rax)
	cmpq	%rdx, %rax
	je	.L7
.L8:
	cmpb	$58, (%rax)
	je	.L15
.L7:
	cmpb	$58, (%rdi)
	je	.L16
.L13:
	movl	$1, %eax
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movb	$49, (%rdi)
	addl	$1, (%r9)
	jmp	.L13
	.cfi_endproc
.LFE192:
	.size	_ZN6icu_6717double_conversionL16RoundWeedCountedENS0_6VectorIcEEimmmPi.isra.0, .-_ZN6icu_6717double_conversionL16RoundWeedCountedENS0_6VectorIcEEimmmPi.isra.0
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.globl	_ZN6icu_6717double_conversion8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_
	.type	_ZN6icu_6717double_conversion8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_, @function
_ZN6icu_6717double_conversion8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_:
.LFB169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%r9, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %edi
	ja	.L113
	movabsq	$4503599627370495, %r9
	movq	%xmm0, %rsi
	movabsq	$9218868437227405312, %r10
	andq	%rsi, %r9
	andq	%rsi, %r10
	je	.L76
	movabsq	$4503599627370496, %r14
	movq	%xmm0, %r8
	shrq	$52, %r8
	addq	%r9, %r14
	andl	$2047, %r8d
	movq	%r14, %r11
	salq	$11, %r14
	subl	$1075, %r8d
	movl	%r8d, %r15d
	subl	$11, %r8d
	testl	%edi, %edi
	jne	.L23
.L116:
	leal	-1(%r15), %ecx
	leaq	(%r11,%r11), %rdx
	movabsq	$-18014398509481984, %rdi
	leaq	1(%rdx), %rbx
	movl	%ecx, %eax
	.p2align 4,,10
	.p2align 3
.L24:
	salq	$10, %rbx
	subl	$10, %eax
	testq	%rdi, %rbx
	je	.L24
	testq	%rbx, %rbx
	js	.L25
	.p2align 4,,10
	.p2align 3
.L26:
	subl	$1, %eax
	addq	%rbx, %rbx
	jns	.L26
.L25:
	testq	%r9, %r9
	jne	.L28
	testq	%r10, %r10
	je	.L28
	shrq	$52, %rsi
	andl	$2047, %esi
	cmpl	$1, %esi
	je	.L28
	leal	-2(%r15), %ecx
	leaq	-1(,%r11,4), %rdx
.L29:
	subl	%eax, %ecx
	salq	%cl, %rdx
	movq	%rdx, %r15
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L113:
	cmpl	$2, %edi
	jne	.L114
	movq	%xmm0, %rax
	movl	%esi, %ebx
	movabsq	$4503599627370495, %rcx
	movabsq	$9218868437227405312, %rdx
	andq	%rax, %rcx
	testq	%rdx, %rax
	je	.L79
	movabsq	$4503599627370496, %rdx
	shrq	$52, %rax
	andl	$2047, %eax
	addq	%rdx, %rcx
	subl	$1075, %eax
.L59:
	leal	-11(%rax), %r14d
	salq	$11, %rcx
	movl	$-96, %esi
	movl	$-124, %edi
	movq	%rcx, %r15
	subl	%r14d, %esi
	subl	%r14d, %edi
	movl	$0, -72(%rbp)
	leaq	-88(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	movq	$0, -80(%rbp)
	call	_ZN6icu_6717double_conversion16PowersOfTenCache36GetCachedPowerForBinaryExponentRangeEiiPNS0_5DiyFpEPi@PLT
	movq	-80(%rbp), %r10
	movq	%r15, %r8
	movl	%r15d, %ecx
	shrq	$32, %r8
	movq	%rcx, %r9
	movl	-72(%rbp), %eax
	leaq	_ZN6icu_6717double_conversionL17kSmallPowersOfTenE(%rip), %rdx
	movq	%r10, %r11
	movq	%r8, %rdi
	movl	%r10d, %r10d
	imulq	%r10, %rdi
	shrq	$32, %r11
	leal	64(%r14,%rax), %esi
	imulq	%r10, %rcx
	imulq	%r11, %r9
	movl	%edi, %r10d
	imulq	%r11, %r8
	shrq	$32, %rdi
	movl	$1, %r11d
	shrq	$32, %rcx
	addq	%rcx, %r10
	movl	$2147483648, %ecx
	movl	%r9d, %eax
	shrq	$32, %r9
	addq	%rcx, %r10
	movl	%esi, %ecx
	addl	$65, %esi
	addq	%rdi, %r9
	imull	$1233, %esi, %esi
	addq	%rax, %r10
	addq	%r9, %r8
	negl	%ecx
	shrq	$32, %r10
	salq	%cl, %r11
	addq	%r8, %r10
	leaq	-1(%r11), %r9
	sarl	$12, %esi
	movq	%r10, %r14
	andq	%r9, %r10
	leal	1(%rsi), %r8d
	shrq	%cl, %r14
	movslq	%r8d, %rdi
	movl	%r14d, %eax
	movl	(%rdx,%rdi,4), %edi
	cmpl	%edi, %r14d
	jnb	.L60
	movslq	%esi, %rdi
	movl	%esi, %r8d
	movl	(%rdx,%rdi,4), %edi
.L60:
	movl	%r8d, -84(%rbp)
	movl	$0, (%r12)
	testl	%r8d, %r8d
	jle	.L61
	xorl	%edx, %edx
	subl	$1, %r8d
	divl	%edi
	movl	%r8d, -84(%rbp)
	addl	$48, %eax
	movb	%al, 0(%r13)
	movl	(%r12), %eax
	leal	1(%rax), %esi
	movl	%edx, %eax
	movl	%esi, (%r12)
	subl	$1, %ebx
	je	.L62
	xorl	%edx, %edx
	movl	$3435973837, %r14d
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%edx, %edx
	movslq	%esi, %rsi
	subl	$1, %r8d
	divl	%edi
	addl	$48, %eax
	movb	%al, 0(%r13,%rsi)
	movl	(%r12), %eax
	leal	1(%rax), %esi
	movl	%edx, %eax
	movl	$1, %edx
	movl	%esi, (%r12)
	subl	$1, %ebx
	je	.L115
.L63:
	movl	%edi, %edi
	imulq	%r14, %rdi
	shrq	$35, %rdi
	testl	%r8d, %r8d
	jne	.L64
	testb	%dl, %dl
	je	.L66
	movl	$0, -84(%rbp)
.L66:
	testl	%ebx, %ebx
	jle	.L72
	cmpq	$1, %r10
	jbe	.L72
	movl	-84(%rbp), %r14d
	movl	%ebx, %eax
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	(%r10,%r10,4), %r10
	movslq	%esi, %rsi
	leaq	(%rdi,%rdi,4), %r8
	subl	$1, %eax
	addq	%r10, %r10
	addq	%r8, %r8
	movq	%r10, %rdx
	movq	%r8, %rdi
	andq	%r9, %r10
	shrq	%cl, %rdx
	addl	$48, %edx
	movb	%dl, 0(%r13,%rsi)
	movl	(%r12), %esi
	leal	(%rax,%r14), %edx
	subl	%ebx, %edx
	addl	$1, %esi
	movl	%esi, (%r12)
	testl	%eax, %eax
	jle	.L81
	cmpq	%r10, %r8
	jb	.L70
.L81:
	movl	%edx, -84(%rbp)
	testl	%eax, %eax
	jne	.L72
	leaq	-84(%rbp), %r9
	movq	%r11, %rcx
	movq	%r10, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6717double_conversionL16RoundWeedCountedENS0_6VectorIcEEimmmPi.isra.0
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L79:
	movabsq	$4503599627370496, %rdx
	movl	$-1074, %eax
	.p2align 4,,10
	.p2align 3
.L58:
	addq	%rcx, %rcx
	subl	$1, %eax
	testq	%rdx, %rcx
	je	.L58
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r9, %r14
	movl	$-1074, %r8d
	movabsq	$4503599627370496, %rax
	.p2align 4,,10
	.p2align 3
.L21:
	addq	%r14, %r14
	subl	$1, %r8d
	testq	%rax, %r14
	je	.L21
	movq	%r9, %r11
	movl	$-1074, %r15d
	salq	$11, %r14
	subl	$11, %r8d
	testl	%edi, %edi
	je	.L116
.L23:
	pxor	%xmm1, %xmm1
	cvtsd2ss	%xmm0, %xmm1
	movd	%xmm1, %r10d
	movd	%xmm1, %r11d
	movd	%xmm1, %eax
	andl	$8388607, %r10d
	andl	$2139095040, %r11d
	je	.L77
	movd	%xmm1, %ecx
	leal	8388608(%r10), %r9d
	shrl	$23, %ecx
	movzbl	%cl, %ecx
	leal	-150(%rcx), %r15d
	subl	$151, %ecx
.L31:
	movabsq	$-18014398509481984, %rdi
	leaq	(%r9,%r9), %rdx
	movl	%ecx, %esi
	leaq	1(%rdx), %rbx
	.p2align 4,,10
	.p2align 3
.L32:
	salq	$10, %rbx
	subl	$10, %esi
	testq	%rdi, %rbx
	je	.L32
	testq	%rbx, %rbx
	js	.L33
	.p2align 4,,10
	.p2align 3
.L34:
	subl	$1, %esi
	addq	%rbx, %rbx
	jns	.L34
.L33:
	testl	%r11d, %r11d
	je	.L36
	testl	%r10d, %r10d
	jne	.L36
	shrl	$23, %eax
	cmpb	$1, %al
	je	.L36
	leal	-2(%r15), %ecx
	leaq	-1(,%r9,4), %rdx
.L37:
	subl	%esi, %ecx
	salq	%cl, %rdx
	movq	%rdx, %r15
.L30:
	movl	$-96, %esi
	leaq	-84(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	movl	%r8d, -104(%rbp)
	movl	$-124, %edi
	subl	%r8d, %esi
	movq	$0, -80(%rbp)
	subl	%r8d, %edi
	movl	$0, -72(%rbp)
	call	_ZN6icu_6717double_conversion16PowersOfTenCache36GetCachedPowerForBinaryExponentRangeEiiPNS0_5DiyFpEPi@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %r9
	movl	%r14d, %r14d
	shrq	$32, %r9
	movq	%r14, %rdx
	movl	-104(%rbp), %r8d
	movl	-72(%rbp), %esi
	movq	%rcx, %rdi
	movq	%r9, %rax
	movl	%ecx, %ecx
	movq	%rbx, %r11
	imulq	%rcx, %rax
	shrq	$32, %rdi
	leal	64(%r8,%rsi), %esi
	movl	%ebx, %ebx
	imulq	%rcx, %r14
	shrq	$32, %r11
	imulq	%rdi, %rdx
	movl	%eax, %r8d
	imulq	%rdi, %r9
	shrq	$32, %rax
	shrq	$32, %r14
	addq	%r14, %r8
	movl	$2147483648, %r14d
	movl	%edx, %r10d
	shrq	$32, %rdx
	addq	%r14, %r8
	addq	%rax, %rdx
	addq	%r10, %r8
	addq	%rdx, %r9
	movl	%r15d, %edx
	movq	%rbx, %r10
	shrq	$32, %r8
	imulq	%rcx, %rbx
	imulq	%rdi, %r10
	leaq	(%r8,%r9), %rax
	movq	%rdx, %r8
	movq	%rax, -112(%rbp)
	movq	%r15, %rax
	imulq	%rdi, %r8
	movq	%r11, %r15
	imulq	%rcx, %r15
	shrq	$32, %rax
	shrq	$32, %rbx
	movq	%rax, -128(%rbp)
	movq	%rax, %r9
	imulq	%rdi, %r11
	imulq	%rcx, %r9
	movl	%r15d, %eax
	shrq	$32, %r15
	addq	%rbx, %rax
	movl	%r10d, %ebx
	leaq	1(%r11,%r15), %r11
	addq	%r14, %rax
	addq	%rbx, %rax
	shrq	$32, %rax
	shrq	$32, %r10
	imulq	-128(%rbp), %rdi
	imulq	%rcx, %rdx
	addq	%r11, %r10
	movl	%esi, %ecx
	addl	$65, %esi
	imull	$1233, %esi, %esi
	leaq	(%rax,%r10), %rbx
	movl	$1, %eax
	negl	%ecx
	movq	%rax, %r11
	salq	%cl, %rax
	movq	%rbx, -104(%rbp)
	subq	%rdi, %r11
	shrq	$32, %rdx
	movq	%r9, %rdi
	movl	%r9d, %r9d
	shrq	$32, %rdi
	addq	%r9, %rdx
	sarl	$12, %esi
	leaq	_ZN6icu_6717double_conversionL17kSmallPowersOfTenE(%rip), %r9
	subq	%rdi, %r11
	addq	%rdx, %r14
	movq	%r8, %rdi
	movl	%r8d, %r8d
	shrq	$32, %rdi
	addq	%r8, %r14
	leal	1(%rsi), %r15d
	movq	%rbx, %rdx
	shrq	$32, %r14
	subq	%rdi, %r11
	movslq	%r15d, %r8
	shrq	%cl, %rdx
	subq	%r14, %r11
	movq	%rax, %r14
	movl	(%r9,%r8,4), %r8d
	movl	%edx, %eax
	leaq	-1(%r14), %rdi
	addq	%rbx, %r11
	andq	%rdi, %rbx
	movq	%rdi, -128(%rbp)
	movq	%rbx, %rdi
	cmpl	%r8d, %edx
	jnb	.L38
	movslq	%esi, %rdx
	movl	%esi, %r15d
	subl	$1, %esi
	movl	(%r9,%rdx,4), %r8d
.L38:
	movl	$0, (%r12)
	testl	%r15d, %r15d
	jle	.L78
	xorl	%edx, %edx
	movl	$3435973837, %r10d
	divl	%r8d
	addl	$48, %eax
	movl	%edx, %r9d
	movb	%al, 0(%r13)
	movslq	(%r12), %r15
	movq	%r9, %rax
	salq	%cl, %r9
	addq	%rdi, %r9
	leal	1(%r15), %ebx
	movl	%ebx, (%r12)
	cmpq	%r9, %r11
	jbe	.L41
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%edx, %edx
	divl	%r8d
	addl	$48, %eax
	movl	%edx, %edx
	movb	%al, (%r15)
	movslq	(%r12), %r15
	movq	%rdx, %rax
	salq	%cl, %rdx
	leaq	(%rdx,%rdi), %r9
	leal	1(%r15), %ebx
	movl	%ebx, (%r12)
	cmpq	%r9, %r11
	ja	.L40
.L41:
	movl	%r8d, %r8d
	movslq	%ebx, %r9
	subl	$1, %esi
	imulq	%r10, %r8
	leaq	0(%r13,%r9), %r15
	shrq	$35, %r8
	cmpl	$-1, %esi
	jne	.L52
	xorl	%r15d, %r15d
.L39:
	movq	-128(%rbp), %rax
	movl	$1, %r10d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L117:
	movslq	%r9d, %r9
.L53:
	leaq	(%rdi,%rdi,4), %rdi
	leaq	(%r10,%r10,4), %rdx
	movq	%r10, %r8
	subl	$1, %r15d
	addq	%rdi, %rdi
	leaq	(%r11,%r11,4), %rsi
	addq	%rdx, %rdx
	movq	%rdi, %rbx
	addq	%rsi, %rsi
	andq	%rax, %rdi
	movq	%rdx, %r10
	shrq	%cl, %rbx
	movq	%rsi, %r11
	addl	$48, %ebx
	movb	%bl, 0(%r13,%r9)
	movslq	(%r12), %rbx
	leal	1(%rbx), %r9d
	movl	%r9d, (%r12)
	cmpq	%rdi, %rsi
	jbe	.L117
	movq	-104(%rbp), %rax
	subq	-112(%rbp), %rax
	imulq	%rdx, %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rdi
	jnb	.L54
	movq	%rsi, %r9
	subq	%rdi, %r9
	cmpq	%r14, %r9
	jb	.L55
	subq	%r14, %r11
	movslq	%ebx, %r9
	subq	%rdi, %r11
	addq	%r13, %r9
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%rdi, %r10
	addq	%r14, %rdi
	cmpq	%rdi, %rcx
	ja	.L56
	movq	%rdx, %r9
	subq	%r10, %rcx
	subq	%rax, %r9
	addq	%rdi, %r9
	cmpq	%r9, %rcx
	jb	.L118
	subb	$1, 0(%r13,%rbx)
.L54:
	addq	%rax, %rdx
	cmpq	%rdi, %rdx
	jbe	.L55
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	%rax, %r14
	ja	.L55
	addq	%rdi, %r14
	cmpq	%r14, %rdx
	ja	.L72
	movq	%rdx, %rax
	subq	%rdx, %r14
	subq	%rdi, %rax
	cmpq	%r14, %rax
	ja	.L72
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	(%r8,%r8,4), %rax
	leaq	0(,%rax,4), %rdx
	cmpq	%rdi, %rdx
	ja	.L72
	salq	$3, %rax
	subq	%rax, %rsi
	cmpq	%rdi, %rsi
	jb	.L72
	movl	%r15d, %esi
.L112:
	subl	-84(%rbp), %esi
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L40:
	movq	-104(%rbp), %r10
	subq	-112(%rbp), %r10
	movl	%r8d, %r8d
	leaq	-1(%r10), %rax
	salq	%cl, %r8
	cmpq	%rax, %r9
	jnb	.L42
	movq	%r11, %rdx
	subq	%r9, %rdx
	cmpq	%rdx, %r8
	ja	.L46
	movslq	%r15d, %rcx
	subq	%r8, %rdx
	addq	%r13, %rcx
	.p2align 4,,10
	.p2align 3
.L74:
	movq	%r9, %rdi
	addq	%r8, %r9
	cmpq	%r9, %rax
	ja	.L44
	movq	-112(%rbp), %rbx
	movq	%r9, %rdx
	subq	-104(%rbp), %rdx
	subq	%rdi, %rax
	leaq	1(%rdx,%rbx), %rdx
	cmpq	%rdx, %rax
	jb	.L119
	subb	$1, 0(%r13,%r15)
.L42:
	addq	$1, %r10
	cmpq	%r9, %r10
	jbe	.L46
	movq	%r11, %rax
	subq	%r9, %rax
	cmpq	%rax, %r8
	ja	.L46
	addq	%r9, %r8
	cmpq	%r8, %r10
	ja	.L72
	movq	%r10, %rax
	subq	%r10, %r8
	subq	%r9, %rax
	cmpq	%r8, %rax
	ja	.L72
	.p2align 4,,10
	.p2align 3
.L46:
	cmpq	$1, %r9
	jbe	.L72
	subq	$4, %r11
	cmpq	%r9, %r11
	jnb	.L112
	.p2align 4,,10
	.p2align 3
.L72:
	xorl	%eax, %eax
.L17:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L120
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movl	%r8d, -84(%rbp)
.L62:
	movl	%eax, %edx
	movl	%edi, %r11d
	leaq	-84(%rbp), %r9
	movl	$1, %r8d
	movq	%rdx, %rax
	salq	%cl, %r11
	movq	%r13, %rdi
	salq	%cl, %rax
	movq	%r11, %rcx
	leaq	(%rax,%r10), %rdx
	call	_ZN6icu_6717double_conversionL16RoundWeedCountedENS0_6VectorIcEEimmmPi.isra.0
.L67:
	movl	-84(%rbp), %esi
	subl	-88(%rbp), %esi
	testb	%al, %al
	je	.L17
.L51:
	movq	-120(%rbp), %rax
	addl	(%r12), %esi
	movl	%esi, (%rax)
	movslq	(%r12), %rax
	movb	$0, 0(%r13,%rax)
	movl	$1, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L28:
	subq	$1, %rdx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L36:
	subq	$1, %rdx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%r11, %r10
	subb	$1, (%r9)
	subq	%r14, %r10
	cmpq	%r11, %r14
	ja	.L55
	movq	%r10, %r11
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%rdx, %rdi
	subb	$1, (%rcx)
	subq	%r8, %rdi
	cmpq	%rdx, %r8
	ja	.L46
	movq	%rdi, %rdx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L61:
	xorl	%esi, %esi
	testl	%ebx, %ebx
	jne	.L66
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r10, %rdi
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L119:
	movq	%rdi, %r9
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L78:
	xorl	%r9d, %r9d
	jmp	.L39
.L77:
	movl	%r10d, %r9d
	movl	$-150, %ecx
	movl	$-149, %r15d
	jmp	.L31
.L120:
	call	__stack_chk_fail@PLT
.L114:
	jmp	.L105
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_.cold, @function
_ZN6icu_6717double_conversion8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_.cold:
.LFSB169:
.L105:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE169:
	.text
	.size	_ZN6icu_6717double_conversion8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_, .-_ZN6icu_6717double_conversion8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_.cold, .-_ZN6icu_6717double_conversion8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_.cold
.LCOLDE0:
	.text
.LHOTE0:
	.section	.rodata
	.align 32
	.type	_ZN6icu_6717double_conversionL17kSmallPowersOfTenE, @object
	.size	_ZN6icu_6717double_conversionL17kSmallPowersOfTenE, 44
_ZN6icu_6717double_conversionL17kSmallPowersOfTenE:
	.long	0
	.long	1
	.long	10
	.long	100
	.long	1000
	.long	10000
	.long	100000
	.long	1000000
	.long	10000000
	.long	100000000
	.long	1000000000
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
