	.file	"csrmbcs.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CharsetRecog_sjis8nextCharEPNS_12IteratedCharEPNS_9InputTextE
	.type	_ZNK6icu_6717CharsetRecog_sjis8nextCharEPNS_12IteratedCharEPNS_9InputTextE, @function
_ZNK6icu_6717CharsetRecog_sjis8nextCharEPNS_12IteratedCharEPNS_9InputTextE:
.LFB2296:
	.cfi_startproc
	endbr64
	movb	$0, 12(%rsi)
	movl	8(%rsi), %eax
	movl	48(%rdx), %edi
	movl	%eax, 4(%rsi)
	cmpl	%edi, %eax
	jge	.L11
	movq	40(%rdx), %r8
	leal	1(%rax), %ecx
	movslq	%eax, %rdx
	movl	%ecx, 8(%rsi)
	movzbl	(%r8,%rdx), %edx
	leal	-161(%rdx), %r9d
	movl	%edx, (%rsi)
	cmpl	$62, %r9d
	jbe	.L9
	cmpl	$127, %edx
	jle	.L9
	cmpl	%ecx, %edi
	jle	.L12
	addl	$2, %eax
	movslq	%ecx, %rcx
	sall	$8, %edx
	movl	%eax, 8(%rsi)
	movzbl	(%r8,%rcx), %eax
	orl	%eax, %edx
	subl	$64, %eax
	movl	%edx, (%rsi)
	cmpl	$190, %eax
	ja	.L7
.L9:
	movl	$1, %eax
	ret
.L12:
	movb	$1, 13(%rsi)
.L7:
	movb	$1, 12(%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movb	$1, 13(%rsi)
	xorl	%eax, %eax
	movl	$-1, (%rsi)
	ret
	.cfi_endproc
.LFE2296:
	.size	_ZNK6icu_6717CharsetRecog_sjis8nextCharEPNS_12IteratedCharEPNS_9InputTextE, .-_ZNK6icu_6717CharsetRecog_sjis8nextCharEPNS_12IteratedCharEPNS_9InputTextE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Shift_JIS"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CharsetRecog_sjis7getNameEv
	.type	_ZNK6icu_6717CharsetRecog_sjis7getNameEv, @function
_ZNK6icu_6717CharsetRecog_sjis7getNameEv:
.LFB2298:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE2298:
	.size	_ZNK6icu_6717CharsetRecog_sjis7getNameEv, .-_ZNK6icu_6717CharsetRecog_sjis7getNameEv
	.section	.rodata.str1.1
.LC1:
	.string	"ja"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CharsetRecog_sjis11getLanguageEv
	.type	_ZNK6icu_6717CharsetRecog_sjis11getLanguageEv, @function
_ZNK6icu_6717CharsetRecog_sjis11getLanguageEv:
.LFB2299:
	.cfi_startproc
	endbr64
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
.LFE2299:
	.size	_ZNK6icu_6717CharsetRecog_sjis11getLanguageEv, .-_ZNK6icu_6717CharsetRecog_sjis11getLanguageEv
	.globl	_ZNK6icu_6719CharsetRecog_euc_jp11getLanguageEv
	.set	_ZNK6icu_6719CharsetRecog_euc_jp11getLanguageEv,_ZNK6icu_6717CharsetRecog_sjis11getLanguageEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716CharsetRecog_euc8nextCharEPNS_12IteratedCharEPNS_9InputTextE
	.type	_ZNK6icu_6716CharsetRecog_euc8nextCharEPNS_12IteratedCharEPNS_9InputTextE, @function
_ZNK6icu_6716CharsetRecog_euc8nextCharEPNS_12IteratedCharEPNS_9InputTextE:
.LFB2304:
	.cfi_startproc
	endbr64
	movb	$0, 12(%rsi)
	movl	8(%rsi), %eax
	movl	48(%rdx), %edi
	movl	%eax, 4(%rsi)
	cmpl	%edi, %eax
	jge	.L39
	movq	40(%rdx), %r8
	leal	1(%rax), %ecx
	movslq	%eax, %rdx
	movl	%ecx, 8(%rsi)
	movzbl	(%r8,%rdx), %edx
	movl	%edx, (%rsi)
	cmpl	$141, %edx
	jle	.L38
	leal	-161(%rdx), %r11d
	cmpl	%ecx, %edi
	jle	.L40
	movslq	%ecx, %rcx
	leal	2(%rax), %r9d
	movl	%r9d, 8(%rsi)
	movzbl	(%r8,%rcx), %r10d
	movl	%edx, %ecx
	sall	$8, %ecx
	orl	%r10d, %ecx
	movl	%ecx, (%rsi)
	cmpl	$93, %r11d
	jbe	.L37
	cmpl	$142, %edx
	je	.L37
	cmpl	$143, %edx
	je	.L41
.L38:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	cmpl	$160, %r10d
	jg	.L38
.L21:
	movb	$1, 12(%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	cmpl	%r9d, %edi
	jle	.L28
	addl	$3, %eax
	movslq	%r9d, %r9
	sall	$8, %ecx
	movl	%eax, 8(%rsi)
	movzbl	(%r8,%r9), %edx
	orl	%edx, %ecx
	movl	%ecx, (%rsi)
	cmpb	$-96, %dl
	ja	.L38
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L39:
	movb	$1, 13(%rsi)
	xorl	%eax, %eax
	movl	$-1, (%rsi)
	ret
.L40:
	movb	$1, 13(%rsi)
	cmpl	$93, %r11d
	jbe	.L21
	cmpl	$142, %edx
	je	.L21
	cmpl	$143, %edx
	jne	.L38
.L28:
	movb	$1, 13(%rsi)
	movl	$-1, (%rsi)
	jmp	.L21
	.cfi_endproc
.LFE2304:
	.size	_ZNK6icu_6716CharsetRecog_euc8nextCharEPNS_12IteratedCharEPNS_9InputTextE, .-_ZNK6icu_6716CharsetRecog_euc8nextCharEPNS_12IteratedCharEPNS_9InputTextE
	.section	.rodata.str1.1
.LC2:
	.string	"EUC-JP"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_euc_jp7getNameEv
	.type	_ZNK6icu_6719CharsetRecog_euc_jp7getNameEv, @function
_ZNK6icu_6719CharsetRecog_euc_jp7getNameEv:
.LFB2309:
	.cfi_startproc
	endbr64
	leaq	.LC2(%rip), %rax
	ret
	.cfi_endproc
.LFE2309:
	.size	_ZNK6icu_6719CharsetRecog_euc_jp7getNameEv, .-_ZNK6icu_6719CharsetRecog_euc_jp7getNameEv
	.section	.rodata.str1.1
.LC3:
	.string	"EUC-KR"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_euc_kr7getNameEv
	.type	_ZNK6icu_6719CharsetRecog_euc_kr7getNameEv, @function
_ZNK6icu_6719CharsetRecog_euc_kr7getNameEv:
.LFB2316:
	.cfi_startproc
	endbr64
	leaq	.LC3(%rip), %rax
	ret
	.cfi_endproc
.LFE2316:
	.size	_ZNK6icu_6719CharsetRecog_euc_kr7getNameEv, .-_ZNK6icu_6719CharsetRecog_euc_kr7getNameEv
	.section	.rodata.str1.1
.LC4:
	.string	"ko"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_euc_kr11getLanguageEv
	.type	_ZNK6icu_6719CharsetRecog_euc_kr11getLanguageEv, @function
_ZNK6icu_6719CharsetRecog_euc_kr11getLanguageEv:
.LFB2317:
	.cfi_startproc
	endbr64
	leaq	.LC4(%rip), %rax
	ret
	.cfi_endproc
.LFE2317:
	.size	_ZNK6icu_6719CharsetRecog_euc_kr11getLanguageEv, .-_ZNK6icu_6719CharsetRecog_euc_kr11getLanguageEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CharsetRecog_big58nextCharEPNS_12IteratedCharEPNS_9InputTextE
	.type	_ZNK6icu_6717CharsetRecog_big58nextCharEPNS_12IteratedCharEPNS_9InputTextE, @function
_ZNK6icu_6717CharsetRecog_big58nextCharEPNS_12IteratedCharEPNS_9InputTextE:
.LFB2323:
	.cfi_startproc
	endbr64
	movb	$0, 12(%rsi)
	movl	8(%rsi), %eax
	movl	48(%rdx), %r8d
	movl	%eax, 4(%rsi)
	cmpl	%r8d, %eax
	jge	.L57
	movq	40(%rdx), %r9
	leal	1(%rax), %edi
	movslq	%eax, %rdx
	movl	%edi, 8(%rsi)
	movzbl	(%r9,%rdx), %ecx
	movl	%ecx, %edx
	movl	%ecx, (%rsi)
	addl	$-128, %edx
	cmpb	$126, %dl
	ja	.L56
	cmpl	%edi, %r8d
	jle	.L58
	addl	$2, %eax
	movslq	%edi, %rdi
	sall	$8, %ecx
	movl	%eax, 8(%rsi)
	movzbl	(%r9,%rdi), %edx
	orl	%edx, %ecx
	movl	%edx, %eax
	andl	$127, %edx
	movl	%ecx, (%rsi)
	cmpb	$127, %dl
	je	.L50
	cmpb	$63, %al
	ja	.L56
.L50:
	movb	$1, 12(%rsi)
.L56:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movb	$1, 13(%rsi)
	xorl	%eax, %eax
	movl	$-1, (%rsi)
	ret
.L58:
	movb	$1, 13(%rsi)
	jmp	.L50
	.cfi_endproc
.LFE2323:
	.size	_ZNK6icu_6717CharsetRecog_big58nextCharEPNS_12IteratedCharEPNS_9InputTextE, .-_ZNK6icu_6717CharsetRecog_big58nextCharEPNS_12IteratedCharEPNS_9InputTextE
	.section	.rodata.str1.1
.LC5:
	.string	"Big5"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CharsetRecog_big57getNameEv
	.type	_ZNK6icu_6717CharsetRecog_big57getNameEv, @function
_ZNK6icu_6717CharsetRecog_big57getNameEv:
.LFB2324:
	.cfi_startproc
	endbr64
	leaq	.LC5(%rip), %rax
	ret
	.cfi_endproc
.LFE2324:
	.size	_ZNK6icu_6717CharsetRecog_big57getNameEv, .-_ZNK6icu_6717CharsetRecog_big57getNameEv
	.section	.rodata.str1.1
.LC6:
	.string	"zh"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CharsetRecog_big511getLanguageEv
	.type	_ZNK6icu_6717CharsetRecog_big511getLanguageEv, @function
_ZNK6icu_6717CharsetRecog_big511getLanguageEv:
.LFB2325:
	.cfi_startproc
	endbr64
	leaq	.LC6(%rip), %rax
	ret
	.cfi_endproc
.LFE2325:
	.size	_ZNK6icu_6717CharsetRecog_big511getLanguageEv, .-_ZNK6icu_6717CharsetRecog_big511getLanguageEv
	.globl	_ZNK6icu_6721CharsetRecog_gb_1803011getLanguageEv
	.set	_ZNK6icu_6721CharsetRecog_gb_1803011getLanguageEv,_ZNK6icu_6717CharsetRecog_big511getLanguageEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CharsetRecog_gb_180308nextCharEPNS_12IteratedCharEPNS_9InputTextE
	.type	_ZNK6icu_6721CharsetRecog_gb_180308nextCharEPNS_12IteratedCharEPNS_9InputTextE, @function
_ZNK6icu_6721CharsetRecog_gb_180308nextCharEPNS_12IteratedCharEPNS_9InputTextE:
.LFB2331:
	.cfi_startproc
	endbr64
	movb	$0, 12(%rsi)
	movl	8(%rsi), %eax
	movl	48(%rdx), %r8d
	movl	%eax, 4(%rsi)
	cmpl	%r8d, %eax
	jge	.L72
	movq	40(%rdx), %r9
	leal	1(%rax), %ecx
	movslq	%eax, %rdx
	movl	%ecx, 8(%rsi)
	movzbl	(%r9,%rdx), %edx
	movl	%edx, (%rsi)
	cmpb	$-128, %dl
	jbe	.L71
	leal	-129(%rdx), %r10d
	cmpl	%ecx, %r8d
	jle	.L73
	leal	2(%rax), %edi
	movslq	%ecx, %rcx
	sall	$8, %edx
	movl	%edi, 8(%rsi)
	movzbl	(%r9,%rcx), %ecx
	orl	%ecx, %edx
	movl	%edx, (%rsi)
	cmpl	$125, %r10d
	ja	.L71
	leal	-64(%rcx), %r10d
	cmpl	$190, %r10d
	jbe	.L71
	subl	$48, %ecx
	cmpl	$9, %ecx
	ja	.L69
	cmpl	%edi, %r8d
	jle	.L70
	movslq	%edi, %rcx
	leal	3(%rax), %r10d
	movl	%r10d, 8(%rsi)
	movzbl	(%r9,%rcx), %ecx
	leal	-129(%rcx), %edi
	cmpl	$125, %edi
	ja	.L69
	cmpl	%r10d, %r8d
	jle	.L70
	addl	$4, %eax
	movslq	%r10d, %r10
	movl	%eax, 8(%rsi)
	movzbl	(%r9,%r10), %eax
	leal	-48(%rax), %edi
	cmpl	$9, %edi
	ja	.L69
	sall	$16, %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	orl	%eax, %edx
	movl	$1, %eax
	movl	%edx, (%rsi)
	ret
.L70:
	movb	$1, 13(%rsi)
	.p2align 4,,10
	.p2align 3
.L69:
	movb	$1, 12(%rsi)
.L71:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	movb	$1, 13(%rsi)
	xorl	%eax, %eax
	movl	$-1, (%rsi)
	ret
.L73:
	movb	$1, 13(%rsi)
	cmpl	$125, %r10d
	ja	.L71
	jmp	.L69
	.cfi_endproc
.LFE2331:
	.size	_ZNK6icu_6721CharsetRecog_gb_180308nextCharEPNS_12IteratedCharEPNS_9InputTextE, .-_ZNK6icu_6721CharsetRecog_gb_180308nextCharEPNS_12IteratedCharEPNS_9InputTextE
	.section	.rodata.str1.1
.LC7:
	.string	"GB18030"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CharsetRecog_gb_180307getNameEv
	.type	_ZNK6icu_6721CharsetRecog_gb_180307getNameEv, @function
_ZNK6icu_6721CharsetRecog_gb_180307getNameEv:
.LFB2332:
	.cfi_startproc
	endbr64
	leaq	.LC7(%rip), %rax
	ret
	.cfi_endproc
.LFE2332:
	.size	_ZNK6icu_6721CharsetRecog_gb_180307getNameEv, .-_ZNK6icu_6721CharsetRecog_gb_180307getNameEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharsetRecog_sjisD2Ev
	.type	_ZN6icu_6717CharsetRecog_sjisD2Ev, @function
_ZN6icu_6717CharsetRecog_sjisD2Ev:
.LFB2293:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_mbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2293:
	.size	_ZN6icu_6717CharsetRecog_sjisD2Ev, .-_ZN6icu_6717CharsetRecog_sjisD2Ev
	.globl	_ZN6icu_6717CharsetRecog_sjisD1Ev
	.set	_ZN6icu_6717CharsetRecog_sjisD1Ev,_ZN6icu_6717CharsetRecog_sjisD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharsetRecog_big5D2Ev
	.type	_ZN6icu_6717CharsetRecog_big5D2Ev, @function
_ZN6icu_6717CharsetRecog_big5D2Ev:
.LFB2320:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_mbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2320:
	.size	_ZN6icu_6717CharsetRecog_big5D2Ev, .-_ZN6icu_6717CharsetRecog_big5D2Ev
	.globl	_ZN6icu_6717CharsetRecog_big5D1Ev
	.set	_ZN6icu_6717CharsetRecog_big5D1Ev,_ZN6icu_6717CharsetRecog_big5D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721CharsetRecog_gb_18030D2Ev
	.type	_ZN6icu_6721CharsetRecog_gb_18030D2Ev, @function
_ZN6icu_6721CharsetRecog_gb_18030D2Ev:
.LFB2328:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_mbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2328:
	.size	_ZN6icu_6721CharsetRecog_gb_18030D2Ev, .-_ZN6icu_6721CharsetRecog_gb_18030D2Ev
	.globl	_ZN6icu_6721CharsetRecog_gb_18030D1Ev
	.set	_ZN6icu_6721CharsetRecog_gb_18030D1Ev,_ZN6icu_6721CharsetRecog_gb_18030D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_euc_jpD2Ev
	.type	_ZN6icu_6719CharsetRecog_euc_jpD2Ev, @function
_ZN6icu_6719CharsetRecog_euc_jpD2Ev:
.LFB2306:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_mbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2306:
	.size	_ZN6icu_6719CharsetRecog_euc_jpD2Ev, .-_ZN6icu_6719CharsetRecog_euc_jpD2Ev
	.globl	_ZN6icu_6719CharsetRecog_euc_jpD1Ev
	.set	_ZN6icu_6719CharsetRecog_euc_jpD1Ev,_ZN6icu_6719CharsetRecog_euc_jpD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_euc_krD2Ev
	.type	_ZN6icu_6719CharsetRecog_euc_krD2Ev, @function
_ZN6icu_6719CharsetRecog_euc_krD2Ev:
.LFB2313:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_mbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2313:
	.size	_ZN6icu_6719CharsetRecog_euc_krD2Ev, .-_ZN6icu_6719CharsetRecog_euc_krD2Ev
	.globl	_ZN6icu_6719CharsetRecog_euc_krD1Ev
	.set	_ZN6icu_6719CharsetRecog_euc_krD1Ev,_ZN6icu_6719CharsetRecog_euc_krD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharsetRecog_sjisD0Ev
	.type	_ZN6icu_6717CharsetRecog_sjisD0Ev, @function
_ZN6icu_6717CharsetRecog_sjisD0Ev:
.LFB2295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_mbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2295:
	.size	_ZN6icu_6717CharsetRecog_sjisD0Ev, .-_ZN6icu_6717CharsetRecog_sjisD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharsetRecog_big5D0Ev
	.type	_ZN6icu_6717CharsetRecog_big5D0Ev, @function
_ZN6icu_6717CharsetRecog_big5D0Ev:
.LFB2322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_mbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2322:
	.size	_ZN6icu_6717CharsetRecog_big5D0Ev, .-_ZN6icu_6717CharsetRecog_big5D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721CharsetRecog_gb_18030D0Ev
	.type	_ZN6icu_6721CharsetRecog_gb_18030D0Ev, @function
_ZN6icu_6721CharsetRecog_gb_18030D0Ev:
.LFB2330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_mbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2330:
	.size	_ZN6icu_6721CharsetRecog_gb_18030D0Ev, .-_ZN6icu_6721CharsetRecog_gb_18030D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_euc_jpD0Ev
	.type	_ZN6icu_6719CharsetRecog_euc_jpD0Ev, @function
_ZN6icu_6719CharsetRecog_euc_jpD0Ev:
.LFB2308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_mbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2308:
	.size	_ZN6icu_6719CharsetRecog_euc_jpD0Ev, .-_ZN6icu_6719CharsetRecog_euc_jpD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_euc_krD0Ev
	.type	_ZN6icu_6719CharsetRecog_euc_krD0Ev, @function
_ZN6icu_6719CharsetRecog_euc_krD0Ev:
.LFB2315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_mbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2315:
	.size	_ZN6icu_6719CharsetRecog_euc_krD0Ev, .-_ZN6icu_6719CharsetRecog_euc_krD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712IteratedCharC2Ev
	.type	_ZN6icu_6712IteratedCharC2Ev, @function
_ZN6icu_6712IteratedCharC2Ev:
.LFB2284:
	.cfi_startproc
	endbr64
	movabsq	$-4294967296, %rax
	movl	$0, 8(%rdi)
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movw	%ax, 12(%rdi)
	ret
	.cfi_endproc
.LFE2284:
	.size	_ZN6icu_6712IteratedCharC2Ev, .-_ZN6icu_6712IteratedCharC2Ev
	.globl	_ZN6icu_6712IteratedCharC1Ev
	.set	_ZN6icu_6712IteratedCharC1Ev,_ZN6icu_6712IteratedCharC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712IteratedChar8nextByteEPNS_9InputTextE
	.type	_ZN6icu_6712IteratedChar8nextByteEPNS_9InputTextE, @function
_ZN6icu_6712IteratedChar8nextByteEPNS_9InputTextE:
.LFB2286:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	cmpl	48(%rsi), %eax
	jge	.L94
	movq	40(%rsi), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	movzbl	(%rdx,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movb	$1, 13(%rdi)
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2286:
	.size	_ZN6icu_6712IteratedChar8nextByteEPNS_9InputTextE, .-_ZN6icu_6712IteratedChar8nextByteEPNS_9InputTextE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharsetRecog_mbcsD2Ev
	.type	_ZN6icu_6717CharsetRecog_mbcsD2Ev, @function
_ZN6icu_6717CharsetRecog_mbcsD2Ev:
.LFB2288:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_mbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2288:
	.size	_ZN6icu_6717CharsetRecog_mbcsD2Ev, .-_ZN6icu_6717CharsetRecog_mbcsD2Ev
	.globl	_ZN6icu_6717CharsetRecog_mbcsD1Ev
	.set	_ZN6icu_6717CharsetRecog_mbcsD1Ev,_ZN6icu_6717CharsetRecog_mbcsD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharsetRecog_mbcsD0Ev
	.type	_ZN6icu_6717CharsetRecog_mbcsD0Ev, @function
_ZN6icu_6717CharsetRecog_mbcsD0Ev:
.LFB2290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_mbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2290:
	.size	_ZN6icu_6717CharsetRecog_mbcsD0Ev, .-_ZN6icu_6717CharsetRecog_mbcsD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CharsetRecog_mbcs10match_mbcsEPNS_9InputTextEPKti
	.type	_ZNK6icu_6717CharsetRecog_mbcs10match_mbcsEPNS_9InputTextEPKti, @function
_ZNK6icu_6717CharsetRecog_mbcs10match_mbcsEPNS_9InputTextEPKti:
.LFB2291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leal	-1(%rcx), %ebx
	subq	$72, %rsp
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -72(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rax, -80(%rbp)
	xorl	%eax, %eax
	movw	%ax, -68(%rbp)
	movl	%ebx, %eax
	shrl	$31, %eax
	movl	$0, -104(%rbp)
	addl	%ebx, %eax
	movl	$0, -92(%rbp)
	sarl	%eax
	movl	%ebx, -96(%rbp)
	xorl	%ebx, %ebx
	movl	%eax, -100(%rbp)
	.p2align 4,,10
	.p2align 3
.L99:
	movq	(%r12), %rax
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L100
	addl	$1, %ebx
	cmpb	$0, -68(%rbp)
	je	.L101
	addl	$1, %r13d
.L102:
	cmpl	$1, %r13d
	jle	.L99
	leal	0(%r13,%r13,4), %eax
	cmpl	-92(%rbp), %eax
	jl	.L99
	xorl	%eax, %eax
.L98:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L133
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movl	-80(%rbp), %esi
	cmpl	$255, %esi
	jbe	.L102
	addl	$1, -92(%rbp)
	cmpq	$0, -88(%rbp)
	je	.L102
	movl	-96(%rbp), %ecx
	testl	%ecx, %ecx
	js	.L102
	movl	-100(%rbp), %eax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L106:
	movq	-88(%rbp), %r8
	movslq	%eax, %rdi
	cmpw	(%r8,%rdi,2), %si
	je	.L103
.L134:
	jbe	.L104
	leal	1(%rax), %edx
	leal	(%rcx,%rdx), %edi
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	cmpl	%edx, %ecx
	jl	.L102
	movq	-88(%rbp), %r8
	movslq	%eax, %rdi
	cmpw	(%r8,%rdi,2), %si
	jne	.L134
.L103:
	cmpl	$-2147483648, %eax
	adcl	$0, -104(%rbp)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L104:
	leal	-1(%rax), %ecx
	leal	(%rdx,%rcx), %edi
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	cmpl	%ecx, %edx
	jle	.L106
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L100:
	movl	-92(%rbp), %eax
	cmpl	$10, %eax
	jg	.L109
	testl	%r13d, %r13d
	jne	.L109
	testl	%eax, %eax
	jne	.L118
	cmpl	$9, %ebx
	jle	.L98
.L118:
	movl	$10, %eax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L109:
	leal	0(%r13,%r13,4), %edx
	movl	-92(%rbp), %ebx
	xorl	%eax, %eax
	sall	$2, %edx
	cmpl	%ebx, %edx
	jg	.L98
	cmpq	$0, -88(%rbp)
	je	.L135
	pxor	%xmm0, %xmm0
	cvtsi2sdl	-92(%rbp), %xmm0
	mulsd	.LC8(%rip), %xmm0
	call	log@PLT
	movsd	%xmm0, -88(%rbp)
	pxor	%xmm0, %xmm0
	cvtsi2sdl	-104(%rbp), %xmm0
	addsd	.LC9(%rip), %xmm0
	call	log@PLT
	movl	$100, %eax
	movapd	%xmm0, %xmm1
	movsd	.LC10(%rip), %xmm0
	divsd	-88(%rbp), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	.LC11(%rip), %xmm0
	cvttsd2sil	%xmm0, %edx
	cmpl	$100, %edx
	cmovg	%eax, %edx
.L112:
	testl	%edx, %edx
	movl	$0, %eax
	cmovns	%edx, %eax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L135:
	imull	$-20, %r13d, %ecx
	movl	$100, %eax
	leal	30(%rbx,%rcx), %edx
	cmpl	$100, %edx
	jle	.L112
	jmp	.L98
.L133:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2291:
	.size	_ZNK6icu_6717CharsetRecog_mbcs10match_mbcsEPNS_9InputTextEPKti, .-_ZNK6icu_6717CharsetRecog_mbcs10match_mbcsEPNS_9InputTextEPKti
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CharsetRecog_sjis5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6717CharsetRecog_sjis5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6717CharsetRecog_sjis5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$57, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZN6icu_67L16commonChars_sjisE(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZNK6icu_6717CharsetRecog_mbcs10match_mbcsEPNS_9InputTextEPKti
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movl	%eax, %ebx
	movl	%eax, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2297:
	.size	_ZNK6icu_6717CharsetRecog_sjis5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6717CharsetRecog_sjis5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_euc_jp5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6719CharsetRecog_euc_jp5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6719CharsetRecog_euc_jp5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$100, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZN6icu_67L18commonChars_euc_jpE(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZNK6icu_6717CharsetRecog_mbcs10match_mbcsEPNS_9InputTextEPKti
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movl	%eax, %ebx
	movl	%eax, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2311:
	.size	_ZNK6icu_6719CharsetRecog_euc_jp5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6719CharsetRecog_euc_jp5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_euc_kr5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6719CharsetRecog_euc_kr5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6719CharsetRecog_euc_kr5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$100, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZN6icu_67L18commonChars_euc_krE(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZNK6icu_6717CharsetRecog_mbcs10match_mbcsEPNS_9InputTextEPKti
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movl	%eax, %ebx
	movl	%eax, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2318:
	.size	_ZNK6icu_6719CharsetRecog_euc_kr5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6719CharsetRecog_euc_kr5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CharsetRecog_big55matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6717CharsetRecog_big55matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6717CharsetRecog_big55matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$96, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZN6icu_67L16commonChars_big5E(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZNK6icu_6717CharsetRecog_mbcs10match_mbcsEPNS_9InputTextEPKti
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movl	%eax, %ebx
	movl	%eax, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2326:
	.size	_ZNK6icu_6717CharsetRecog_big55matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6717CharsetRecog_big55matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CharsetRecog_gb_180305matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6721CharsetRecog_gb_180305matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6721CharsetRecog_gb_180305matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$100, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZN6icu_67L20commonChars_gb_18030E(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZNK6icu_6717CharsetRecog_mbcs10match_mbcsEPNS_9InputTextEPKti
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movl	%eax, %ebx
	movl	%eax, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2334:
	.size	_ZNK6icu_6721CharsetRecog_gb_180305matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6721CharsetRecog_gb_180305matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CharsetRecog_eucD2Ev
	.type	_ZN6icu_6716CharsetRecog_eucD2Ev, @function
_ZN6icu_6716CharsetRecog_eucD2Ev:
.LFB2301:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_mbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2301:
	.size	_ZN6icu_6716CharsetRecog_eucD2Ev, .-_ZN6icu_6716CharsetRecog_eucD2Ev
	.globl	_ZN6icu_6716CharsetRecog_eucD1Ev
	.set	_ZN6icu_6716CharsetRecog_eucD1Ev,_ZN6icu_6716CharsetRecog_eucD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CharsetRecog_eucD0Ev
	.type	_ZN6icu_6716CharsetRecog_eucD0Ev, @function
_ZN6icu_6716CharsetRecog_eucD0Ev:
.LFB2303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_mbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2303:
	.size	_ZN6icu_6716CharsetRecog_eucD0Ev, .-_ZN6icu_6716CharsetRecog_eucD0Ev
	.weak	_ZTSN6icu_6717CharsetRecog_mbcsE
	.section	.rodata._ZTSN6icu_6717CharsetRecog_mbcsE,"aG",@progbits,_ZTSN6icu_6717CharsetRecog_mbcsE,comdat
	.align 16
	.type	_ZTSN6icu_6717CharsetRecog_mbcsE, @object
	.size	_ZTSN6icu_6717CharsetRecog_mbcsE, 29
_ZTSN6icu_6717CharsetRecog_mbcsE:
	.string	"N6icu_6717CharsetRecog_mbcsE"
	.weak	_ZTIN6icu_6717CharsetRecog_mbcsE
	.section	.data.rel.ro._ZTIN6icu_6717CharsetRecog_mbcsE,"awG",@progbits,_ZTIN6icu_6717CharsetRecog_mbcsE,comdat
	.align 8
	.type	_ZTIN6icu_6717CharsetRecog_mbcsE, @object
	.size	_ZTIN6icu_6717CharsetRecog_mbcsE, 24
_ZTIN6icu_6717CharsetRecog_mbcsE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717CharsetRecog_mbcsE
	.quad	_ZTIN6icu_6717CharsetRecognizerE
	.weak	_ZTSN6icu_6717CharsetRecog_sjisE
	.section	.rodata._ZTSN6icu_6717CharsetRecog_sjisE,"aG",@progbits,_ZTSN6icu_6717CharsetRecog_sjisE,comdat
	.align 16
	.type	_ZTSN6icu_6717CharsetRecog_sjisE, @object
	.size	_ZTSN6icu_6717CharsetRecog_sjisE, 29
_ZTSN6icu_6717CharsetRecog_sjisE:
	.string	"N6icu_6717CharsetRecog_sjisE"
	.weak	_ZTIN6icu_6717CharsetRecog_sjisE
	.section	.data.rel.ro._ZTIN6icu_6717CharsetRecog_sjisE,"awG",@progbits,_ZTIN6icu_6717CharsetRecog_sjisE,comdat
	.align 8
	.type	_ZTIN6icu_6717CharsetRecog_sjisE, @object
	.size	_ZTIN6icu_6717CharsetRecog_sjisE, 24
_ZTIN6icu_6717CharsetRecog_sjisE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717CharsetRecog_sjisE
	.quad	_ZTIN6icu_6717CharsetRecog_mbcsE
	.weak	_ZTSN6icu_6716CharsetRecog_eucE
	.section	.rodata._ZTSN6icu_6716CharsetRecog_eucE,"aG",@progbits,_ZTSN6icu_6716CharsetRecog_eucE,comdat
	.align 16
	.type	_ZTSN6icu_6716CharsetRecog_eucE, @object
	.size	_ZTSN6icu_6716CharsetRecog_eucE, 28
_ZTSN6icu_6716CharsetRecog_eucE:
	.string	"N6icu_6716CharsetRecog_eucE"
	.weak	_ZTIN6icu_6716CharsetRecog_eucE
	.section	.data.rel.ro._ZTIN6icu_6716CharsetRecog_eucE,"awG",@progbits,_ZTIN6icu_6716CharsetRecog_eucE,comdat
	.align 8
	.type	_ZTIN6icu_6716CharsetRecog_eucE, @object
	.size	_ZTIN6icu_6716CharsetRecog_eucE, 24
_ZTIN6icu_6716CharsetRecog_eucE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6716CharsetRecog_eucE
	.quad	_ZTIN6icu_6717CharsetRecog_mbcsE
	.weak	_ZTSN6icu_6719CharsetRecog_euc_jpE
	.section	.rodata._ZTSN6icu_6719CharsetRecog_euc_jpE,"aG",@progbits,_ZTSN6icu_6719CharsetRecog_euc_jpE,comdat
	.align 16
	.type	_ZTSN6icu_6719CharsetRecog_euc_jpE, @object
	.size	_ZTSN6icu_6719CharsetRecog_euc_jpE, 31
_ZTSN6icu_6719CharsetRecog_euc_jpE:
	.string	"N6icu_6719CharsetRecog_euc_jpE"
	.weak	_ZTIN6icu_6719CharsetRecog_euc_jpE
	.section	.data.rel.ro._ZTIN6icu_6719CharsetRecog_euc_jpE,"awG",@progbits,_ZTIN6icu_6719CharsetRecog_euc_jpE,comdat
	.align 8
	.type	_ZTIN6icu_6719CharsetRecog_euc_jpE, @object
	.size	_ZTIN6icu_6719CharsetRecog_euc_jpE, 24
_ZTIN6icu_6719CharsetRecog_euc_jpE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CharsetRecog_euc_jpE
	.quad	_ZTIN6icu_6716CharsetRecog_eucE
	.weak	_ZTSN6icu_6719CharsetRecog_euc_krE
	.section	.rodata._ZTSN6icu_6719CharsetRecog_euc_krE,"aG",@progbits,_ZTSN6icu_6719CharsetRecog_euc_krE,comdat
	.align 16
	.type	_ZTSN6icu_6719CharsetRecog_euc_krE, @object
	.size	_ZTSN6icu_6719CharsetRecog_euc_krE, 31
_ZTSN6icu_6719CharsetRecog_euc_krE:
	.string	"N6icu_6719CharsetRecog_euc_krE"
	.weak	_ZTIN6icu_6719CharsetRecog_euc_krE
	.section	.data.rel.ro._ZTIN6icu_6719CharsetRecog_euc_krE,"awG",@progbits,_ZTIN6icu_6719CharsetRecog_euc_krE,comdat
	.align 8
	.type	_ZTIN6icu_6719CharsetRecog_euc_krE, @object
	.size	_ZTIN6icu_6719CharsetRecog_euc_krE, 24
_ZTIN6icu_6719CharsetRecog_euc_krE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CharsetRecog_euc_krE
	.quad	_ZTIN6icu_6716CharsetRecog_eucE
	.weak	_ZTSN6icu_6717CharsetRecog_big5E
	.section	.rodata._ZTSN6icu_6717CharsetRecog_big5E,"aG",@progbits,_ZTSN6icu_6717CharsetRecog_big5E,comdat
	.align 16
	.type	_ZTSN6icu_6717CharsetRecog_big5E, @object
	.size	_ZTSN6icu_6717CharsetRecog_big5E, 29
_ZTSN6icu_6717CharsetRecog_big5E:
	.string	"N6icu_6717CharsetRecog_big5E"
	.weak	_ZTIN6icu_6717CharsetRecog_big5E
	.section	.data.rel.ro._ZTIN6icu_6717CharsetRecog_big5E,"awG",@progbits,_ZTIN6icu_6717CharsetRecog_big5E,comdat
	.align 8
	.type	_ZTIN6icu_6717CharsetRecog_big5E, @object
	.size	_ZTIN6icu_6717CharsetRecog_big5E, 24
_ZTIN6icu_6717CharsetRecog_big5E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717CharsetRecog_big5E
	.quad	_ZTIN6icu_6717CharsetRecog_mbcsE
	.weak	_ZTSN6icu_6721CharsetRecog_gb_18030E
	.section	.rodata._ZTSN6icu_6721CharsetRecog_gb_18030E,"aG",@progbits,_ZTSN6icu_6721CharsetRecog_gb_18030E,comdat
	.align 32
	.type	_ZTSN6icu_6721CharsetRecog_gb_18030E, @object
	.size	_ZTSN6icu_6721CharsetRecog_gb_18030E, 33
_ZTSN6icu_6721CharsetRecog_gb_18030E:
	.string	"N6icu_6721CharsetRecog_gb_18030E"
	.weak	_ZTIN6icu_6721CharsetRecog_gb_18030E
	.section	.data.rel.ro._ZTIN6icu_6721CharsetRecog_gb_18030E,"awG",@progbits,_ZTIN6icu_6721CharsetRecog_gb_18030E,comdat
	.align 8
	.type	_ZTIN6icu_6721CharsetRecog_gb_18030E, @object
	.size	_ZTIN6icu_6721CharsetRecog_gb_18030E, 24
_ZTIN6icu_6721CharsetRecog_gb_18030E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6721CharsetRecog_gb_18030E
	.quad	_ZTIN6icu_6717CharsetRecog_mbcsE
	.weak	_ZTVN6icu_6717CharsetRecog_mbcsE
	.section	.data.rel.ro._ZTVN6icu_6717CharsetRecog_mbcsE,"awG",@progbits,_ZTVN6icu_6717CharsetRecog_mbcsE,comdat
	.align 8
	.type	_ZTVN6icu_6717CharsetRecog_mbcsE, @object
	.size	_ZTVN6icu_6717CharsetRecog_mbcsE, 64
_ZTVN6icu_6717CharsetRecog_mbcsE:
	.quad	0
	.quad	_ZTIN6icu_6717CharsetRecog_mbcsE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6717CharsetRecog_sjisE
	.section	.data.rel.ro.local._ZTVN6icu_6717CharsetRecog_sjisE,"awG",@progbits,_ZTVN6icu_6717CharsetRecog_sjisE,comdat
	.align 8
	.type	_ZTVN6icu_6717CharsetRecog_sjisE, @object
	.size	_ZTVN6icu_6717CharsetRecog_sjisE, 64
_ZTVN6icu_6717CharsetRecog_sjisE:
	.quad	0
	.quad	_ZTIN6icu_6717CharsetRecog_sjisE
	.quad	_ZNK6icu_6717CharsetRecog_sjis7getNameEv
	.quad	_ZNK6icu_6717CharsetRecog_sjis11getLanguageEv
	.quad	_ZNK6icu_6717CharsetRecog_sjis5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6717CharsetRecog_sjisD1Ev
	.quad	_ZN6icu_6717CharsetRecog_sjisD0Ev
	.quad	_ZNK6icu_6717CharsetRecog_sjis8nextCharEPNS_12IteratedCharEPNS_9InputTextE
	.weak	_ZTVN6icu_6716CharsetRecog_eucE
	.section	.data.rel.ro._ZTVN6icu_6716CharsetRecog_eucE,"awG",@progbits,_ZTVN6icu_6716CharsetRecog_eucE,comdat
	.align 8
	.type	_ZTVN6icu_6716CharsetRecog_eucE, @object
	.size	_ZTVN6icu_6716CharsetRecog_eucE, 64
_ZTVN6icu_6716CharsetRecog_eucE:
	.quad	0
	.quad	_ZTIN6icu_6716CharsetRecog_eucE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.quad	_ZNK6icu_6716CharsetRecog_euc8nextCharEPNS_12IteratedCharEPNS_9InputTextE
	.weak	_ZTVN6icu_6719CharsetRecog_euc_jpE
	.section	.data.rel.ro.local._ZTVN6icu_6719CharsetRecog_euc_jpE,"awG",@progbits,_ZTVN6icu_6719CharsetRecog_euc_jpE,comdat
	.align 8
	.type	_ZTVN6icu_6719CharsetRecog_euc_jpE, @object
	.size	_ZTVN6icu_6719CharsetRecog_euc_jpE, 64
_ZTVN6icu_6719CharsetRecog_euc_jpE:
	.quad	0
	.quad	_ZTIN6icu_6719CharsetRecog_euc_jpE
	.quad	_ZNK6icu_6719CharsetRecog_euc_jp7getNameEv
	.quad	_ZNK6icu_6719CharsetRecog_euc_jp11getLanguageEv
	.quad	_ZNK6icu_6719CharsetRecog_euc_jp5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6719CharsetRecog_euc_jpD1Ev
	.quad	_ZN6icu_6719CharsetRecog_euc_jpD0Ev
	.quad	_ZNK6icu_6716CharsetRecog_euc8nextCharEPNS_12IteratedCharEPNS_9InputTextE
	.weak	_ZTVN6icu_6719CharsetRecog_euc_krE
	.section	.data.rel.ro.local._ZTVN6icu_6719CharsetRecog_euc_krE,"awG",@progbits,_ZTVN6icu_6719CharsetRecog_euc_krE,comdat
	.align 8
	.type	_ZTVN6icu_6719CharsetRecog_euc_krE, @object
	.size	_ZTVN6icu_6719CharsetRecog_euc_krE, 64
_ZTVN6icu_6719CharsetRecog_euc_krE:
	.quad	0
	.quad	_ZTIN6icu_6719CharsetRecog_euc_krE
	.quad	_ZNK6icu_6719CharsetRecog_euc_kr7getNameEv
	.quad	_ZNK6icu_6719CharsetRecog_euc_kr11getLanguageEv
	.quad	_ZNK6icu_6719CharsetRecog_euc_kr5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6719CharsetRecog_euc_krD1Ev
	.quad	_ZN6icu_6719CharsetRecog_euc_krD0Ev
	.quad	_ZNK6icu_6716CharsetRecog_euc8nextCharEPNS_12IteratedCharEPNS_9InputTextE
	.weak	_ZTVN6icu_6717CharsetRecog_big5E
	.section	.data.rel.ro.local._ZTVN6icu_6717CharsetRecog_big5E,"awG",@progbits,_ZTVN6icu_6717CharsetRecog_big5E,comdat
	.align 8
	.type	_ZTVN6icu_6717CharsetRecog_big5E, @object
	.size	_ZTVN6icu_6717CharsetRecog_big5E, 64
_ZTVN6icu_6717CharsetRecog_big5E:
	.quad	0
	.quad	_ZTIN6icu_6717CharsetRecog_big5E
	.quad	_ZNK6icu_6717CharsetRecog_big57getNameEv
	.quad	_ZNK6icu_6717CharsetRecog_big511getLanguageEv
	.quad	_ZNK6icu_6717CharsetRecog_big55matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6717CharsetRecog_big5D1Ev
	.quad	_ZN6icu_6717CharsetRecog_big5D0Ev
	.quad	_ZNK6icu_6717CharsetRecog_big58nextCharEPNS_12IteratedCharEPNS_9InputTextE
	.weak	_ZTVN6icu_6721CharsetRecog_gb_18030E
	.section	.data.rel.ro.local._ZTVN6icu_6721CharsetRecog_gb_18030E,"awG",@progbits,_ZTVN6icu_6721CharsetRecog_gb_18030E,comdat
	.align 8
	.type	_ZTVN6icu_6721CharsetRecog_gb_18030E, @object
	.size	_ZTVN6icu_6721CharsetRecog_gb_18030E, 64
_ZTVN6icu_6721CharsetRecog_gb_18030E:
	.quad	0
	.quad	_ZTIN6icu_6721CharsetRecog_gb_18030E
	.quad	_ZNK6icu_6721CharsetRecog_gb_180307getNameEv
	.quad	_ZNK6icu_6721CharsetRecog_gb_1803011getLanguageEv
	.quad	_ZNK6icu_6721CharsetRecog_gb_180305matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6721CharsetRecog_gb_18030D1Ev
	.quad	_ZN6icu_6721CharsetRecog_gb_18030D0Ev
	.quad	_ZNK6icu_6721CharsetRecog_gb_180308nextCharEPNS_12IteratedCharEPNS_9InputTextE
	.section	.rodata
	.align 32
	.type	_ZN6icu_67L20commonChars_gb_18030E, @object
	.size	_ZN6icu_67L20commonChars_gb_18030E, 200
_ZN6icu_67L20commonChars_gb_18030E:
	.value	-24159
	.value	-24158
	.value	-24157
	.value	-24156
	.value	-24144
	.value	-24143
	.value	-24079
	.value	-24077
	.value	-23647
	.value	-23636
	.value	-23622
	.value	-20056
	.value	-20040
	.value	-20034
	.value	-19781
	.value	-19511
	.value	-19466
	.value	-19213
	.value	-19011
	.value	-19004
	.value	-18973
	.value	-18769
	.value	-18732
	.value	-18720
	.value	-18526
	.value	-18520
	.value	-18499
	.value	-18474
	.value	-18467
	.value	-18252
	.value	-18209
	.value	-18186
	.value	-18005
	.value	-17975
	.value	-17960
	.value	-17926
	.value	-17923
	.value	-17715
	.value	-17497
	.value	-17450
	.value	-17439
	.value	-17414
	.value	-17220
	.value	-17189
	.value	-17154
	.value	-16948
	.value	-16691
	.value	-16675
	.value	-16460
	.value	-16442
	.value	-16439
	.value	-16204
	.value	-16147
	.value	-15925
	.value	-15653
	.value	-15417
	.value	-15140
	.value	-15126
	.value	-14900
	.value	-14601
	.value	-14344
	.value	-14165
	.value	-14133
	.value	-14123
	.value	-14105
	.value	-13873
	.value	-13830
	.value	-13647
	.value	-13643
	.value	-13625
	.value	-13616
	.value	-13610
	.value	-13579
	.value	-13571
	.value	-13076
	.value	-12808
	.value	-12630
	.value	-12604
	.value	-12590
	.value	-12571
	.value	-12363
	.value	-12350
	.value	-12330
	.value	-12094
	.value	-12091
	.value	-12080
	.value	-12076
	.value	-11865
	.value	-11606
	.value	-11598
	.value	-11595
	.value	-11589
	.value	-11564
	.value	-11325
	.value	-11312
	.value	-11267
	.value	-11070
	.value	-11046
	.value	-10782
	.value	-10544
	.align 32
	.type	_ZN6icu_67L16commonChars_big5E, @object
	.size	_ZN6icu_67L16commonChars_big5E, 192
_ZN6icu_67L16commonChars_big5E:
	.value	-24256
	.value	-24255
	.value	-24254
	.value	-24253
	.value	-24249
	.value	-24247
	.value	-24203
	.value	-24202
	.value	-23488
	.value	-23482
	.value	-23481
	.value	-23480
	.value	-23471
	.value	-23468
	.value	-23465
	.value	-23452
	.value	-23446
	.value	-23444
	.value	-23433
	.value	-23389
	.value	-23388
	.value	-23385
	.value	-23359
	.value	-23346
	.value	-23343
	.value	-23329
	.value	-23320
	.value	-23299
	.value	-23232
	.value	-23224
	.value	-23208
	.value	-23191
	.value	-23091
	.value	-23065
	.value	-22953
	.value	-22943
	.value	-22942
	.value	-22936
	.value	-22928
	.value	-22872
	.value	-22861
	.value	-22855
	.value	-22829
	.value	-22821
	.value	-22810
	.value	-22798
	.value	-22720
	.value	-22703
	.value	-22695
	.value	-22566
	.value	-22365
	.value	-22363
	.value	-22355
	.value	-22319
	.value	-22317
	.value	-22300
	.value	-22276
	.value	-22080
	.value	-22062
	.value	-22029
	.value	-21909
	.value	-21830
	.value	-21826
	.value	-21812
	.value	-21764
	.value	-21433
	.value	-21425
	.value	-21328
	.value	-21294
	.value	-21159
	.value	-20791
	.value	-20512
	.value	-20246
	.value	-20113
	.value	-19789
	.value	-19772
	.value	-19601
	.value	-19380
	.value	-19378
	.value	-19124
	.value	-19035
	.value	-19011
	.value	-18992
	.value	-18984
	.value	-18831
	.value	-18451
	.value	-18329
	.value	-18108
	.value	-17704
	.value	-17596
	.value	-17503
	.value	-16943
	.value	-15676
	.value	-15431
	.value	-15296
	.value	-15265
	.align 32
	.type	_ZN6icu_67L18commonChars_euc_krE, @object
	.size	_ZN6icu_67L18commonChars_euc_krE, 200
_ZN6icu_67L18commonChars_euc_krE:
	.value	-20319
	.value	-20301
	.value	-20283
	.value	-20275
	.value	-20268
	.value	-20250
	.value	-20243
	.value	-20232
	.value	-20230
	.value	-20228
	.value	-20040
	.value	-20039
	.value	-20025
	.value	-20009
	.value	-19998
	.value	-19542
	.value	-19525
	.value	-19262
	.value	-19249
	.value	-19239
	.value	-19221
	.value	-19035
	.value	-19019
	.value	-19009
	.value	-19001
	.value	-18967
	.value	-18701
	.value	-18513
	.value	-18494
	.value	-18482
	.value	-18266
	.value	-18258
	.value	-18250
	.value	-18248
	.value	-18245
	.value	-18199
	.value	-18005
	.value	-18002
	.value	-17972
	.value	-17970
	.value	-17923
	.value	-17736
	.value	-17714
	.value	-17712
	.value	-17679
	.value	-17433
	.value	-17421
	.value	-17411
	.value	-17235
	.value	-17222
	.value	-17198
	.value	-17162
	.value	-16966
	.value	-16960
	.value	-16957
	.value	-16955
	.value	-16698
	.value	-16696
	.value	-16673
	.value	-16658
	.value	-16648
	.value	-16646
	.value	-16479
	.value	-16471
	.value	-16448
	.value	-16412
	.value	-16405
	.value	-16404
	.value	-16392
	.value	-16217
	.value	-16209
	.value	-16200
	.value	-16198
	.value	-16197
	.value	-16195
	.value	-16185
	.value	-16180
	.value	-16178
	.value	-16177
	.value	-16170
	.value	-16166
	.value	-16155
	.value	-16133
	.value	-16132
	.value	-15964
	.value	-15962
	.value	-15946
	.value	-15914
	.value	-15905
	.value	-15882
	.value	-15880
	.value	-15199
	.value	-14899
	.value	-14674
	.value	-14385
	.value	-14383
	.value	-14382
	.value	-14376
	.value	-14363
	.value	-14163
	.align 32
	.type	_ZN6icu_67L18commonChars_euc_jpE, @object
	.size	_ZN6icu_67L18commonChars_euc_jpE, 200
_ZN6icu_67L18commonChars_euc_jpE:
	.value	-24159
	.value	-24158
	.value	-24157
	.value	-24154
	.value	-24132
	.value	-24118
	.value	-24117
	.value	-24106
	.value	-24105
	.value	-23390
	.value	-23388
	.value	-23386
	.value	-23384
	.value	-23382
	.value	-23381
	.value	-23380
	.value	-23379
	.value	-23377
	.value	-23375
	.value	-23373
	.value	-23371
	.value	-23369
	.value	-23367
	.value	-23365
	.value	-23363
	.value	-23361
	.value	-23360
	.value	-23359
	.value	-23357
	.value	-23356
	.value	-23354
	.value	-23353
	.value	-23352
	.value	-23351
	.value	-23350
	.value	-23349
	.value	-23346
	.value	-23345
	.value	-23344
	.value	-23330
	.value	-23329
	.value	-23327
	.value	-23326
	.value	-23324
	.value	-23320
	.value	-23319
	.value	-23318
	.value	-23317
	.value	-23316
	.value	-23313
	.value	-23310
	.value	-23309
	.value	-23134
	.value	-23133
	.value	-23132
	.value	-23130
	.value	-23129
	.value	-23126
	.value	-23123
	.value	-23121
	.value	-23120
	.value	-23117
	.value	-23115
	.value	-23113
	.value	-23112
	.value	-23111
	.value	-23105
	.value	-23101
	.value	-23098
	.value	-23097
	.value	-23096
	.value	-23095
	.value	-23093
	.value	-23088
	.value	-23083
	.value	-23082
	.value	-23081
	.value	-23074
	.value	-23072
	.value	-23071
	.value	-23067
	.value	-23063
	.value	-23062
	.value	-23061
	.value	-23060
	.value	-23059
	.value	-23053
	.value	-18263
	.value	-17964
	.value	-17682
	.value	-17464
	.value	-16656
	.value	-16457
	.value	-15126
	.value	-14596
	.value	-14403
	.value	-13640
	.value	-13581
	.value	-13348
	.value	-12847
	.align 32
	.type	_ZN6icu_67L16commonChars_sjisE, @object
	.size	_ZN6icu_67L16commonChars_sjisE, 114
_ZN6icu_67L16commonChars_sjisE:
	.value	-32448
	.value	-32447
	.value	-32446
	.value	-32443
	.value	-32421
	.value	-32407
	.value	-32406
	.value	-32395
	.value	-32394
	.value	-32096
	.value	-32094
	.value	-32092
	.value	-32087
	.value	-32086
	.value	-32085
	.value	-32083
	.value	-32081
	.value	-32079
	.value	-32077
	.value	-32075
	.value	-32073
	.value	-32067
	.value	-32066
	.value	-32063
	.value	-32060
	.value	-32059
	.value	-32058
	.value	-32056
	.value	-32055
	.value	-32052
	.value	-32051
	.value	-32036
	.value	-32032
	.value	-32025
	.value	-32024
	.value	-32023
	.value	-32022
	.value	-32016
	.value	-32015
	.value	-31935
	.value	-31933
	.value	-31922
	.value	-31921
	.value	-31912
	.value	-31906
	.value	-31902
	.value	-31897
	.value	-31883
	.value	-31882
	.value	-31863
	.value	-31862
	.value	-31861
	.value	-31859
	.value	-31853
	.value	-29034
	.value	-27654
	.value	-27222
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC8:
	.long	0
	.long	1070596096
	.align 8
.LC9:
	.long	0
	.long	1072693248
	.align 8
.LC10:
	.long	0
	.long	1079410688
	.align 8
.LC11:
	.long	0
	.long	1076101120
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
