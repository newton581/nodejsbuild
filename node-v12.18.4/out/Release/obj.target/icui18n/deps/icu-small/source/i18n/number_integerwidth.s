	.file	"number_integerwidth.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number12IntegerWidthC2Essb
	.type	_ZN6icu_676number12IntegerWidthC2Essb, @function
_ZN6icu_676number12IntegerWidthC2Essb:
.LFB2796:
	.cfi_startproc
	endbr64
	movb	$0, 8(%rdi)
	movw	%si, (%rdi)
	movw	%dx, 2(%rdi)
	movb	%cl, 4(%rdi)
	ret
	.cfi_endproc
.LFE2796:
	.size	_ZN6icu_676number12IntegerWidthC2Essb, .-_ZN6icu_676number12IntegerWidthC2Essb
	.globl	_ZN6icu_676number12IntegerWidthC1Essb
	.set	_ZN6icu_676number12IntegerWidthC1Essb,_ZN6icu_676number12IntegerWidthC2Essb
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number12IntegerWidth10zeroFillToEi
	.type	_ZN6icu_676number12IntegerWidth10zeroFillToEi, @function
_ZN6icu_676number12IntegerWidth10zeroFillToEi:
.LFB2798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$999, %edi
	ja	.L4
	movl	$-1, %eax
	movw	%di, -12(%rbp)
	movw	%ax, -10(%rbp)
	xorl	%eax, %eax
	movb	%al, -4(%rbp)
	movl	-4(%rbp), %edx
	movb	$0, -8(%rbp)
	movq	-12(%rbp), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movl	$1, %eax
	movl	$65810, -12(%rbp)
	movb	%al, -4(%rbp)
	movq	-12(%rbp), %rax
	movl	-4(%rbp), %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2798:
	.size	_ZN6icu_676number12IntegerWidth10zeroFillToEi, .-_ZN6icu_676number12IntegerWidth10zeroFillToEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number12IntegerWidth10truncateAtEi
	.type	_ZN6icu_676number12IntegerWidth10truncateAtEi, @function
_ZN6icu_676number12IntegerWidth10truncateAtEi:
.LFB2799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpb	$0, 8(%rdi)
	je	.L8
	movq	(%rdi), %rax
	movq	%rax, -12(%rbp)
	movl	8(%rdi), %eax
	movl	%eax, -4(%rbp)
	movq	-12(%rbp), %rax
	movl	-4(%rbp), %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movzwl	(%rdi), %eax
	cmpl	$999, %esi
	ja	.L10
	movswl	%ax, %edx
	cmpl	%esi, %edx
	jg	.L11
	movw	%ax, -12(%rbp)
	movw	%si, -10(%rbp)
	movb	$0, -8(%rbp)
	movq	-12(%rbp), %rax
	movb	$0, -4(%rbp)
	movl	-4(%rbp), %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	cmpl	$-1, %esi
	jne	.L11
	movw	%ax, -12(%rbp)
	movl	$-1, %eax
	movw	%ax, -10(%rbp)
	movb	$0, -8(%rbp)
	movq	-12(%rbp), %rax
	movb	$0, -4(%rbp)
	movl	-4(%rbp), %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$65810, -12(%rbp)
	movq	-12(%rbp), %rax
	movb	$1, -4(%rbp)
	movl	-4(%rbp), %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2799:
	.size	_ZN6icu_676number12IntegerWidth10truncateAtEi, .-_ZN6icu_676number12IntegerWidth10truncateAtEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number12IntegerWidth5applyERNS0_4impl15DecimalQuantityER10UErrorCode
	.type	_ZNK6icu_676number12IntegerWidth5applyERNS0_4impl15DecimalQuantityER10UErrorCode, @function
_ZNK6icu_676number12IntegerWidth5applyERNS0_4impl15DecimalQuantityER10UErrorCode:
.LFB2800:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	je	.L14
	movl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movswl	2(%rdi), %r13d
	cmpw	$-1, %r13w
	je	.L23
	cmpb	$0, 4(%rdi)
	jne	.L24
.L18:
	movswl	(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity13setMinIntegerEi@PLT
	movswl	2(%rbx), %esi
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity15applyMaxIntegerEi@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movswl	(%rdi), %esi
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity13setMinIntegerEi@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	%rsi, %rdi
	movq	%rdx, -40(%rbp)
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	cmpl	%eax, %r13d
	jge	.L18
	movq	-40(%rbp), %rdx
	movl	$1, (%rdx)
	jmp	.L18
	.cfi_endproc
.LFE2800:
	.size	_ZNK6icu_676number12IntegerWidth5applyERNS0_4impl15DecimalQuantityER10UErrorCode, .-_ZNK6icu_676number12IntegerWidth5applyERNS0_4impl15DecimalQuantityER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number12IntegerWidtheqERKS1_
	.type	_ZNK6icu_676number12IntegerWidtheqERKS1_, @function
_ZNK6icu_676number12IntegerWidtheqERKS1_:
.LFB2801:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	cmpl	%eax, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE2801:
	.size	_ZNK6icu_676number12IntegerWidtheqERKS1_, .-_ZNK6icu_676number12IntegerWidtheqERKS1_
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
