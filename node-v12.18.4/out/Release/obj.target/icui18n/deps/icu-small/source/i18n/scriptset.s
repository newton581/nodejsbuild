	.file	"scriptset.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ScriptSetC2Ev
	.type	_ZN6icu_679ScriptSetC2Ev, @function
_ZN6icu_679ScriptSetC2Ev:
.LFB2282:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rdi)
	movl	$0, 24(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE2282:
	.size	_ZN6icu_679ScriptSetC2Ev, .-_ZN6icu_679ScriptSetC2Ev
	.globl	_ZN6icu_679ScriptSetC1Ev
	.set	_ZN6icu_679ScriptSetC1Ev,_ZN6icu_679ScriptSetC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ScriptSetD2Ev
	.type	_ZN6icu_679ScriptSetD2Ev, @function
_ZN6icu_679ScriptSetD2Ev:
.LFB2285:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2285:
	.size	_ZN6icu_679ScriptSetD2Ev, .-_ZN6icu_679ScriptSetD2Ev
	.globl	_ZN6icu_679ScriptSetD1Ev
	.set	_ZN6icu_679ScriptSetD1Ev,_ZN6icu_679ScriptSetD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ScriptSetC2ERKS0_
	.type	_ZN6icu_679ScriptSetC2ERKS0_, @function
_ZN6icu_679ScriptSetC2ERKS0_:
.LFB2288:
	.cfi_startproc
	endbr64
	movdqu	(%rsi), %xmm0
	movups	%xmm0, (%rdi)
	movq	16(%rsi), %rax
	movq	%rax, 16(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	ret
	.cfi_endproc
.LFE2288:
	.size	_ZN6icu_679ScriptSetC2ERKS0_, .-_ZN6icu_679ScriptSetC2ERKS0_
	.globl	_ZN6icu_679ScriptSetC1ERKS0_
	.set	_ZN6icu_679ScriptSetC1ERKS0_,_ZN6icu_679ScriptSetC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ScriptSetaSERKS0_
	.type	_ZN6icu_679ScriptSetaSERKS0_, @function
_ZN6icu_679ScriptSetaSERKS0_:
.LFB2290:
	.cfi_startproc
	endbr64
	movdqu	(%rsi), %xmm0
	movq	%rdi, %rax
	movups	%xmm0, (%rdi)
	movq	16(%rsi), %rdx
	movq	%rdx, 16(%rdi)
	movl	24(%rsi), %edx
	movl	%edx, 24(%rdi)
	ret
	.cfi_endproc
.LFE2290:
	.size	_ZN6icu_679ScriptSetaSERKS0_, .-_ZN6icu_679ScriptSetaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679ScriptSeteqERKS0_
	.type	_ZNK6icu_679ScriptSeteqERKS0_, @function
_ZNK6icu_679ScriptSeteqERKS0_:
.LFB2291:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	cmpl	%eax, (%rdi)
	jne	.L13
	movl	4(%rsi), %eax
	cmpl	%eax, 4(%rdi)
	jne	.L13
	movl	8(%rsi), %eax
	cmpl	%eax, 8(%rdi)
	jne	.L13
	movl	12(%rsi), %eax
	cmpl	%eax, 12(%rdi)
	jne	.L13
	movl	16(%rsi), %eax
	cmpl	%eax, 16(%rdi)
	jne	.L13
	movl	20(%rsi), %eax
	cmpl	%eax, 20(%rdi)
	jne	.L13
	movl	24(%rsi), %eax
	cmpl	%eax, 24(%rdi)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2291:
	.size	_ZNK6icu_679ScriptSeteqERKS0_, .-_ZNK6icu_679ScriptSeteqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode
	.type	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode, @function
_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode:
.LFB2292:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L14
	cmpl	$223, %esi
	ja	.L18
	movl	%esi, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	movl	%esi, %ecx
	sarl	$5, %ecx
	testl	%eax, (%rdi,%rcx,4)
	setne	%al
.L14:
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$1, (%rdx)
	ret
	.cfi_endproc
.LFE2292:
	.size	_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode, .-_ZNK6icu_679ScriptSet4testE11UScriptCodeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ScriptSet3setE11UScriptCodeR10UErrorCode
	.type	_ZN6icu_679ScriptSet3setE11UScriptCodeR10UErrorCode, @function
_ZN6icu_679ScriptSet3setE11UScriptCodeR10UErrorCode:
.LFB2293:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	movl	(%rdx), %esi
	movq	%rdi, %rax
	testl	%esi, %esi
	jg	.L20
	cmpl	$223, %ecx
	ja	.L22
	movl	%ecx, %edx
	movl	$1, %esi
	sarl	$5, %edx
	sall	%cl, %esi
	movslq	%edx, %rdx
	orl	%esi, (%rdi,%rdx,4)
.L20:
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$1, (%rdx)
	ret
	.cfi_endproc
.LFE2293:
	.size	_ZN6icu_679ScriptSet3setE11UScriptCodeR10UErrorCode, .-_ZN6icu_679ScriptSet3setE11UScriptCodeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ScriptSet5resetE11UScriptCodeR10UErrorCode
	.type	_ZN6icu_679ScriptSet5resetE11UScriptCodeR10UErrorCode, @function
_ZN6icu_679ScriptSet5resetE11UScriptCodeR10UErrorCode:
.LFB2294:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	movl	(%rdx), %esi
	movq	%rdi, %rax
	testl	%esi, %esi
	jg	.L24
	cmpl	$223, %ecx
	ja	.L26
	movl	%ecx, %esi
	movl	$1, %edx
	sarl	$5, %esi
	sall	%cl, %edx
	movslq	%esi, %rsi
	notl	%edx
	andl	%edx, (%rdi,%rsi,4)
.L24:
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$1, (%rdx)
	ret
	.cfi_endproc
.LFE2294:
	.size	_ZN6icu_679ScriptSet5resetE11UScriptCodeR10UErrorCode, .-_ZN6icu_679ScriptSet5resetE11UScriptCodeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ScriptSet5UnionERKS0_
	.type	_ZN6icu_679ScriptSet5UnionERKS0_, @function
_ZN6icu_679ScriptSet5UnionERKS0_:
.LFB2295:
	.cfi_startproc
	endbr64
	movdqu	(%rsi), %xmm1
	movdqu	(%rdi), %xmm0
	movq	%rdi, %rax
	por	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	movl	16(%rsi), %edx
	orl	%edx, 16(%rdi)
	movl	20(%rsi), %edx
	orl	%edx, 20(%rdi)
	movl	24(%rsi), %edx
	orl	%edx, 24(%rdi)
	ret
	.cfi_endproc
.LFE2295:
	.size	_ZN6icu_679ScriptSet5UnionERKS0_, .-_ZN6icu_679ScriptSet5UnionERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ScriptSet9intersectERKS0_
	.type	_ZN6icu_679ScriptSet9intersectERKS0_, @function
_ZN6icu_679ScriptSet9intersectERKS0_:
.LFB2296:
	.cfi_startproc
	endbr64
	movdqu	(%rsi), %xmm1
	movdqu	(%rdi), %xmm0
	movq	%rdi, %rax
	pand	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	movl	16(%rsi), %edx
	andl	%edx, 16(%rdi)
	movl	20(%rsi), %edx
	andl	%edx, 20(%rdi)
	movl	24(%rsi), %edx
	andl	%edx, 24(%rdi)
	ret
	.cfi_endproc
.LFE2296:
	.size	_ZN6icu_679ScriptSet9intersectERKS0_, .-_ZN6icu_679ScriptSet9intersectERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ScriptSet9intersectE11UScriptCodeR10UErrorCode
	.type	_ZN6icu_679ScriptSet9intersectE11UScriptCodeR10UErrorCode, @function
_ZN6icu_679ScriptSet9intersectE11UScriptCodeR10UErrorCode:
.LFB2297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	(%rdx), %ecx
	movq	%fs:40, %rdi
	movq	%rdi, -8(%rbp)
	xorl	%edi, %edi
	movq	$0, -32(%rbp)
	movl	$0, -24(%rbp)
	movaps	%xmm0, -48(%rbp)
	testl	%ecx, %ecx
	jg	.L30
	movl	%esi, %ecx
	cmpl	$223, %esi
	ja	.L34
	movl	$1, %edx
	sarl	$5, %esi
	movdqu	(%rax), %xmm0
	sall	%cl, %edx
	movl	%edx, -48(%rbp,%rsi,4)
	movq	-32(%rbp), %rdx
	andq	%rdx, 16(%rax)
	movl	-24(%rbp), %edx
	andl	%edx, 24(%rax)
	pand	-48(%rbp), %xmm0
	movups	%xmm0, (%rax)
.L30:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L35
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movl	$1, (%rdx)
	jmp	.L30
.L35:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2297:
	.size	_ZN6icu_679ScriptSet9intersectE11UScriptCodeR10UErrorCode, .-_ZN6icu_679ScriptSet9intersectE11UScriptCodeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679ScriptSet10intersectsERKS0_
	.type	_ZNK6icu_679ScriptSet10intersectsERKS0_, @function
_ZNK6icu_679ScriptSet10intersectsERKS0_:
.LFB2298:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, (%rsi)
	jne	.L43
	movl	4(%rdi), %eax
	testl	%eax, 4(%rsi)
	jne	.L43
	movl	8(%rdi), %eax
	testl	%eax, 8(%rsi)
	jne	.L43
	movl	12(%rdi), %eax
	testl	%eax, 12(%rsi)
	jne	.L43
	movl	16(%rdi), %eax
	testl	%eax, 16(%rsi)
	jne	.L43
	movl	20(%rdi), %eax
	testl	%eax, 20(%rsi)
	jne	.L43
	movl	24(%rdi), %eax
	testl	%eax, 24(%rsi)
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2298:
	.size	_ZNK6icu_679ScriptSet10intersectsERKS0_, .-_ZNK6icu_679ScriptSet10intersectsERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679ScriptSet8containsERKS0_
	.type	_ZNK6icu_679ScriptSet8containsERKS0_, @function
_ZNK6icu_679ScriptSet8containsERKS0_:
.LFB2299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movdqu	(%rdi), %xmm0
	movdqu	(%rsi), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movl	24(%rdi), %edx
	movl	20(%rsi), %r8d
	movl	16(%rsi), %edi
	pand	%xmm1, %xmm0
	movq	%rax, %rcx
	movl	24(%rsi), %r9d
	movq	%rax, -32(%rbp)
	movd	%xmm0, %r10d
	shrq	$32, %rcx
	movl	%edx, -24(%rbp)
	andl	%edi, %eax
	movaps	%xmm0, -48(%rbp)
	andl	%r8d, %ecx
	andl	%r9d, %edx
	cmpl	(%rsi), %r10d
	jne	.L52
	movl	-44(%rbp), %r11d
	cmpl	%r11d, 4(%rsi)
	jne	.L52
	movl	8(%rsi), %r11d
	cmpl	%r11d, -40(%rbp)
	jne	.L52
	movl	12(%rsi), %esi
	cmpl	%esi, -36(%rbp)
	jne	.L52
	cmpl	%eax, %edi
	jne	.L52
	cmpl	%ecx, %r8d
	jne	.L52
	cmpl	%edx, %r9d
	sete	%al
.L44:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L54
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L44
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2299:
	.size	_ZNK6icu_679ScriptSet8containsERKS0_, .-_ZNK6icu_679ScriptSet8containsERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ScriptSet6setAllEv
	.type	_ZN6icu_679ScriptSet6setAllEv, @function
_ZN6icu_679ScriptSet6setAllEv:
.LFB2300:
	.cfi_startproc
	endbr64
	pcmpeqd	%xmm0, %xmm0
	movq	$-1, 16(%rdi)
	movq	%rdi, %rax
	movl	$-1, 24(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE2300:
	.size	_ZN6icu_679ScriptSet6setAllEv, .-_ZN6icu_679ScriptSet6setAllEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ScriptSet8resetAllEv
	.type	_ZN6icu_679ScriptSet8resetAllEv, @function
_ZN6icu_679ScriptSet8resetAllEv:
.LFB2301:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rdi)
	movq	%rdi, %rax
	movl	$0, 24(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE2301:
	.size	_ZN6icu_679ScriptSet8resetAllEv, .-_ZN6icu_679ScriptSet8resetAllEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679ScriptSet12countMembersEv
	.type	_ZNK6icu_679ScriptSet12countMembersEv, @function
_ZNK6icu_679ScriptSet12countMembersEv:
.LFB2302:
	.cfi_startproc
	endbr64
	leaq	28(%rdi), %rcx
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L60:
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L58
	.p2align 4,,10
	.p2align 3
.L59:
	leal	-1(%rax), %edx
	addl	$1, %r8d
	andl	%edx, %eax
	jne	.L59
.L58:
	addq	$4, %rdi
	cmpq	%rcx, %rdi
	jne	.L60
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2302:
	.size	_ZNK6icu_679ScriptSet12countMembersEv, .-_ZNK6icu_679ScriptSet12countMembersEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679ScriptSet8hashCodeEv
	.type	_ZNK6icu_679ScriptSet8hashCodeEv, @function
_ZNK6icu_679ScriptSet8hashCodeEv:
.LFB2303:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	xorl	4(%rdi), %eax
	xorl	8(%rdi), %eax
	xorl	12(%rdi), %eax
	xorl	16(%rdi), %eax
	xorl	20(%rdi), %eax
	xorl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE2303:
	.size	_ZNK6icu_679ScriptSet8hashCodeEv, .-_ZNK6icu_679ScriptSet8hashCodeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679ScriptSet10nextSetBitEi
	.type	_ZNK6icu_679ScriptSet10nextSetBitEi, @function
_ZNK6icu_679ScriptSet10nextSetBitEi:
.LFB2304:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	cmpl	$223, %esi
	ja	.L71
	movl	%esi, %edx
	movl	$1, %eax
	sarl	$5, %edx
	sall	%cl, %eax
	testl	%eax, (%rdi,%rdx,4)
	jne	.L73
	movl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L69:
	addl	$1, %ecx
	cmpl	$224, %ecx
	je	.L71
	movl	%ecx, %eax
	movl	%esi, %edx
	sarl	$5, %eax
	sall	%cl, %edx
	testl	%edx, (%rdi,%rax,4)
	je	.L69
.L73:
	movl	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2304:
	.size	_ZNK6icu_679ScriptSet10nextSetBitEi, .-_ZNK6icu_679ScriptSet10nextSetBitEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679ScriptSet7isEmptyEv
	.type	_ZNK6icu_679ScriptSet7isEmptyEv, @function
_ZNK6icu_679ScriptSet7isEmptyEv:
.LFB2305:
	.cfi_startproc
	endbr64
	movl	(%rdi), %r10d
	testl	%r10d, %r10d
	jne	.L81
	movl	4(%rdi), %r9d
	testl	%r9d, %r9d
	jne	.L81
	movl	8(%rdi), %r8d
	testl	%r8d, %r8d
	jne	.L81
	movl	12(%rdi), %esi
	testl	%esi, %esi
	jne	.L81
	movl	16(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L81
	movl	20(%rdi), %edx
	testl	%edx, %edx
	jne	.L81
	movl	24(%rdi), %eax
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2305:
	.size	_ZNK6icu_679ScriptSet7isEmptyEv, .-_ZNK6icu_679ScriptSet7isEmptyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679ScriptSet14displayScriptsERNS_13UnicodeStringE
	.type	_ZNK6icu_679ScriptSet14displayScriptsERNS_13UnicodeStringE, @function
_ZNK6icu_679ScriptSet14displayScriptsERNS_13UnicodeStringE:
.LFB2306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$1, %esi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L84:
	movl	%ecx, %eax
	movl	%esi, %edx
	movl	%ecx, %r12d
	sarl	$5, %eax
	sall	%cl, %edx
	addl	$1, %ecx
	testl	%edx, (%rbx,%rax,4)
	jne	.L101
	cmpl	$224, %ecx
	jne	.L84
	.p2align 4,,10
	.p2align 3
.L87:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$104, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	leaq	-128(%rbp), %r13
	movl	$1, %r15d
	.p2align 4,,10
	.p2align 3
.L83:
	movl	%r12d, %edi
	call	uscript_getShortName_67@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L85
	sarl	$5, %ecx
.L86:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	addl	$1, %r12d
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	$223, %r12d
	jg	.L87
	movl	%r12d, %eax
	movl	%r15d, %edx
	movl	%r12d, %ecx
	sarl	$5, %eax
	sall	%cl, %edx
	testl	%edx, (%rbx,%rax,4)
	jne	.L88
	.p2align 4,,10
	.p2align 3
.L89:
	addl	$1, %r12d
	cmpl	$224, %r12d
	je	.L87
	movl	%r12d, %eax
	movl	%r15d, %edx
	movl	%r12d, %ecx
	sarl	$5, %eax
	sall	%cl, %edx
	testl	%edx, (%rbx,%rax,4)
	je	.L89
.L88:
	movl	$32, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	leaq	-130(%rbp), %rsi
	movw	%ax, -130(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L85:
	movl	-116(%rbp), %ecx
	jmp	.L86
.L102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2306:
	.size	_ZNK6icu_679ScriptSet14displayScriptsERNS_13UnicodeStringE, .-_ZNK6icu_679ScriptSet14displayScriptsERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ScriptSet12parseScriptsERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_679ScriptSet12parseScriptsERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_679ScriptSet12parseScriptsERKNS_13UnicodeStringER10UErrorCode:
.LFB2307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movl	$0, 24(%rdi)
	movups	%xmm0, (%rdi)
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L104
	movl	$2, %edx
	leaq	-96(%rbp), %rcx
	movq	%rsi, %rbx
	xorl	%r15d, %r15d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -152(%rbp)
	leaq	-160(%rbp), %r14
	movq	%rax, -160(%rbp)
	movswl	8(%rsi), %eax
	movq	%rcx, -176(%rbp)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L128:
	sarl	$5, %eax
.L107:
	cmpl	%eax, %r15d
	jge	.L108
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movl	$1, %edx
	movl	%eax, %r12d
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movl	%r12d, %edi
	movl	%eax, %r15d
	call	u_isUWhiteSpace_67@PLT
	testb	%al, %al
	je	.L109
.L115:
	movswl	-152(%rbp), %edx
	movl	%edx, %eax
	sarl	$5, %edx
	testw	%ax, %ax
	cmovs	-148(%rbp), %edx
	testl	%edx, %edx
	jg	.L127
.L126:
	movswl	8(%rbx), %eax
.L105:
	testw	%ax, %ax
	jns	.L128
	movl	12(%rbx), %eax
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L127:
	movq	-176(%rbp), %r12
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	$39, %r8d
	movq	%r12, %rcx
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movq	%r12, %rsi
	movl	$4106, %edi
	movb	$0, -57(%rbp)
	call	u_getPropertyValueEnum_67@PLT
	movl	%eax, %ecx
	cmpl	$-1, %eax
	je	.L120
	movq	-168(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L108
	cmpl	$223, %ecx
	ja	.L120
	movl	%ecx, %eax
	movl	$1, %edx
	sarl	$5, %eax
	sall	%cl, %edx
	cltq
	orl	%edx, 0(%r13,%rax,4)
	movzwl	-152(%rbp), %edx
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, -152(%rbp)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L109:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movswl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L112
	movswl	%ax, %edx
	sarl	$5, %edx
.L113:
	cmpl	%edx, %r15d
	jge	.L115
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L120:
	movq	-168(%rbp), %rax
	movl	$1, (%rax)
.L108:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L104:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movl	12(%rbx), %edx
	jmp	.L113
.L129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2307:
	.size	_ZN6icu_679ScriptSet12parseScriptsERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_679ScriptSet12parseScriptsERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ScriptSet19setScriptExtensionsEiR10UErrorCode
	.type	_ZN6icu_679ScriptSet19setScriptExtensionsEiR10UErrorCode, @function
_ZN6icu_679ScriptSet19setScriptExtensionsEiR10UErrorCode:
.LFB2308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L130
	movl	%esi, %r13d
	leaq	-144(%rbp), %rsi
	movq	%rdi, %r14
	movq	%rdx, %rbx
	movq	%rsi, -160(%rbp)
	movl	$20, %r12d
	leaq	-164(%rbp), %r15
	movl	$20, -152(%rbp)
	movb	$0, -148(%rbp)
	movl	$0, -164(%rbp)
.L136:
	movl	%r12d, %edx
	movq	%r15, %rcx
	movl	%r13d, %edi
	call	uscript_getScriptExtensions_67@PLT
	movl	%eax, %r12d
	movl	-164(%rbp), %eax
	cmpl	$15, %eax
	jne	.L133
	testl	%r12d, %r12d
	jle	.L134
	movslq	%r12d, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L134
	cmpb	$0, -148(%rbp)
	jne	.L153
.L135:
	movq	%rsi, -160(%rbp)
	movl	%r12d, -152(%rbp)
	movb	$1, -148(%rbp)
	movl	$0, -164(%rbp)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$7, (%rbx)
	movq	-160(%rbp), %rdi
.L137:
	cmpb	$0, -148(%rbp)
	je	.L130
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L130:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movq	-160(%rbp), %rdi
	movq	%rax, -184(%rbp)
	call	uprv_free_67@PLT
	movq	-184(%rbp), %rsi
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L133:
	movq	-160(%rbp), %rdi
	testl	%eax, %eax
	jg	.L155
	testl	%r12d, %r12d
	jle	.L137
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L137
	leal	-1(%r12), %eax
	movq	%rdi, %rdx
	movl	$1, %esi
	leaq	4(%rdi,%rax,4), %r8
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L156:
	movl	%ecx, %eax
	movl	%esi, %r9d
	addq	$4, %rdx
	sarl	$5, %eax
	sall	%cl, %r9d
	cltq
	orl	%r9d, (%r14,%rax,4)
	cmpq	%r8, %rdx
	je	.L137
.L145:
	movl	(%rdx), %ecx
	cmpl	$223, %ecx
	jbe	.L156
	movl	$1, (%rbx)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L155:
	movl	%eax, (%rbx)
	jmp	.L137
.L154:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2308:
	.size	_ZN6icu_679ScriptSet19setScriptExtensionsEiR10UErrorCode, .-_ZN6icu_679ScriptSet19setScriptExtensionsEiR10UErrorCode
	.p2align 4
	.globl	uhash_equalsScriptSet_67
	.type	uhash_equalsScriptSet_67, @function
uhash_equalsScriptSet_67:
.LFB2309:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	cmpl	%eax, (%rdi)
	jne	.L164
	movl	4(%rsi), %eax
	cmpl	%eax, 4(%rdi)
	jne	.L164
	movl	8(%rsi), %eax
	cmpl	%eax, 8(%rdi)
	jne	.L164
	movl	12(%rsi), %eax
	cmpl	%eax, 12(%rdi)
	jne	.L164
	movl	16(%rsi), %eax
	cmpl	%eax, 16(%rdi)
	jne	.L164
	movl	20(%rsi), %eax
	cmpl	%eax, 20(%rdi)
	jne	.L164
	movl	24(%rsi), %eax
	cmpl	%eax, 24(%rdi)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2309:
	.size	uhash_equalsScriptSet_67, .-uhash_equalsScriptSet_67
	.p2align 4
	.globl	uhash_compareScriptSet_67
	.type	uhash_compareScriptSet_67, @function
uhash_compareScriptSet_67:
.LFB2310:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	leaq	28(%rdi), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L168:
	movl	(%r8), %edx
	testl	%edx, %edx
	je	.L166
	.p2align 4,,10
	.p2align 3
.L167:
	leal	-1(%rdx), %ecx
	addl	$1, %eax
	andl	%ecx, %edx
	jne	.L167
.L166:
	addq	$4, %r8
	cmpq	%r8, %r9
	jne	.L168
	movq	%rsi, %r9
	leaq	28(%rsi), %r10
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L171:
	movl	(%r9), %edx
	testl	%edx, %edx
	je	.L169
	.p2align 4,,10
	.p2align 3
.L170:
	leal	-1(%rdx), %ecx
	addl	$1, %r8d
	andl	%ecx, %edx
	jne	.L170
.L169:
	addq	$4, %r9
	cmpq	%r9, %r10
	jne	.L171
	subl	%r8d, %eax
	je	.L228
	ret
.L228:
	xorl	%edx, %edx
	movl	$1, %r9d
	.p2align 4,,10
	.p2align 3
.L172:
	movl	%edx, %ecx
	movl	%r9d, %r8d
	sall	%cl, %r8d
	movl	%edx, %ecx
	sarl	$5, %ecx
	testl	%r8d, (%rdi,%rcx,4)
	jne	.L174
	addl	$1, %edx
	cmpl	$224, %edx
	jne	.L172
	movl	$-1, %edx
.L174:
	movl	$1, %r9d
	.p2align 4,,10
	.p2align 3
.L176:
	movl	%eax, %ecx
	movl	%r9d, %r8d
	sall	%cl, %r8d
	movl	%eax, %ecx
	sarl	$5, %ecx
	testl	%r8d, (%rsi,%rcx,4)
	jne	.L175
	addl	$1, %eax
	cmpl	$224, %eax
	jne	.L176
	movl	$-1, %eax
.L175:
	movl	%edx, %ecx
	subl	%eax, %ecx
	jne	.L177
	movl	$1, %r8d
	testl	%edx, %edx
	jle	.L200
	.p2align 4,,10
	.p2align 3
.L227:
	addl	$1, %edx
	cmpl	$224, %edx
	je	.L229
	movl	%edx, %ecx
	movl	%r8d, %r9d
	sall	%cl, %r9d
	movl	%edx, %ecx
	sarl	$5, %ecx
	testl	%r9d, (%rdi,%rcx,4)
	je	.L227
	addl	$1, %eax
	cmpl	$224, %eax
	je	.L230
	movl	%eax, %r9d
	movl	%r8d, %r11d
	movl	%eax, %ecx
	sarl	$5, %r9d
	sall	%cl, %r11d
	testl	%r11d, (%rsi,%r9,4)
	jne	.L231
	.p2align 4,,10
	.p2align 3
.L186:
	addl	$1, %eax
	cmpl	$224, %eax
	je	.L232
.L224:
	movl	%eax, %ecx
	movl	%r8d, %r9d
	sall	%cl, %r9d
	movl	%eax, %ecx
	sarl	$5, %ecx
	testl	%r9d, (%rsi,%rcx,4)
	je	.L186
.L187:
	movl	%edx, %ecx
	subl	%eax, %ecx
	jne	.L177
	testl	%edx, %edx
	jg	.L227
.L200:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	addl	$1, %eax
	cmpl	$224, %eax
	je	.L200
	movl	%eax, %ecx
	movl	%r8d, %edx
	sall	%cl, %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	sarl	$5, %edx
	testl	%ecx, (%rsi,%rdx,4)
	jne	.L193
	addl	$1, %eax
	movl	$-1, %edx
	cmpl	$224, %eax
	jne	.L224
	.p2align 4,,10
	.p2align 3
.L232:
	movl	$-1, %eax
	jmp	.L187
.L193:
	notl	%eax
	movl	%eax, %ecx
.L177:
	movl	%ecx, %eax
	ret
.L230:
	leal	1(%rdx), %eax
	ret
.L231:
	movl	%edx, %ecx
	subl	%eax, %ecx
	je	.L227
	jmp	.L177
	.cfi_endproc
.LFE2310:
	.size	uhash_compareScriptSet_67, .-uhash_compareScriptSet_67
	.p2align 4
	.globl	uhash_hashScriptSet_67
	.type	uhash_hashScriptSet_67, @function
uhash_hashScriptSet_67:
.LFB2311:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	xorl	4(%rdi), %eax
	xorl	8(%rdi), %eax
	xorl	12(%rdi), %eax
	xorl	16(%rdi), %eax
	xorl	20(%rdi), %eax
	xorl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE2311:
	.size	uhash_hashScriptSet_67, .-uhash_hashScriptSet_67
	.p2align 4
	.globl	uhash_deleteScriptSet_67
	.type	uhash_deleteScriptSet_67, @function
uhash_deleteScriptSet_67:
.LFB2312:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L234
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L234:
	ret
	.cfi_endproc
.LFE2312:
	.size	uhash_deleteScriptSet_67, .-uhash_deleteScriptSet_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
