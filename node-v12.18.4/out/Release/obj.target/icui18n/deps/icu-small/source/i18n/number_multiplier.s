	.file	"number_multiplier.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl23MultiplierFormatHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.type	_ZNK6icu_676number4impl23MultiplierFormatHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode, @function
_ZNK6icu_676number4impl23MultiplierFormatHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode:
.LFB3364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	32(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	8(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi@PLT
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1
	leaq	-28(%rbp), %rdx
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantity10multiplyByERKNS1_6DecNumER10UErrorCode@PLT
.L1:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3364:
	.size	_ZNK6icu_676number4impl23MultiplierFormatHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode, .-_ZNK6icu_676number4impl23MultiplierFormatHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.section	.text._ZN6icu_676number4impl23MultiplierFormatHandlerD2Ev,"axG",@progbits,_ZN6icu_676number4impl23MultiplierFormatHandlerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl23MultiplierFormatHandlerD2Ev
	.type	_ZN6icu_676number4impl23MultiplierFormatHandlerD2Ev, @function
_ZN6icu_676number4impl23MultiplierFormatHandlerD2Ev:
.LFB4512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L11
	cmpb	$0, 12(%r13)
	jne	.L17
.L12:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L11:
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L12
	.cfi_endproc
.LFE4512:
	.size	_ZN6icu_676number4impl23MultiplierFormatHandlerD2Ev, .-_ZN6icu_676number4impl23MultiplierFormatHandlerD2Ev
	.weak	_ZN6icu_676number4impl23MultiplierFormatHandlerD1Ev
	.set	_ZN6icu_676number4impl23MultiplierFormatHandlerD1Ev,_ZN6icu_676number4impl23MultiplierFormatHandlerD2Ev
	.section	.text._ZN6icu_676number4impl23MultiplierFormatHandlerD0Ev,"axG",@progbits,_ZN6icu_676number4impl23MultiplierFormatHandlerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl23MultiplierFormatHandlerD0Ev
	.type	_ZN6icu_676number4impl23MultiplierFormatHandlerD0Ev, @function
_ZN6icu_676number4impl23MultiplierFormatHandlerD0Ev:
.LFB4514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L19
	cmpb	$0, 12(%r13)
	jne	.L25
.L20:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L19:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L20
	.cfi_endproc
.LFE4514:
	.size	_ZN6icu_676number4impl23MultiplierFormatHandlerD0Ev, .-_ZN6icu_676number4impl23MultiplierFormatHandlerD0Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3601:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3601:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3604:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L39
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L27
	cmpb	$0, 12(%rbx)
	jne	.L40
.L31:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L27:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L31
	.cfi_endproc
.LFE3604:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3607:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L43
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3607:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3610:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L46
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3610:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L52
.L48:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L53
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3612:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3613:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3613:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3614:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3614:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3615:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3615:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3616:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3616:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3617:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3617:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3618:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L69
	testl	%edx, %edx
	jle	.L69
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L72
.L61:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L61
	.cfi_endproc
.LFE3618:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L76
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L76
	testl	%r12d, %r12d
	jg	.L83
	cmpb	$0, 12(%rbx)
	jne	.L84
.L78:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L78
.L84:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3619:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L86
	movq	(%rdi), %r8
.L87:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L90
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L90
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3620:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3621:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L97
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3621:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3622:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3622:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3623:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3623:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3624:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3624:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3626:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3626:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3628:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3628:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number5ScaleC2EiPNS0_4impl6DecNumE
	.type	_ZN6icu_676number5ScaleC2EiPNS0_4impl6DecNumE, @function
_ZN6icu_676number5ScaleC2EiPNS0_4impl6DecNumE:
.LFB3343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	%esi, (%rdi)
	movq	%rdx, 8(%rdi)
	movl	$0, 16(%rdi)
	testq	%rdx, %rdx
	je	.L103
	movq	%rdx, %rdi
	call	_ZN6icu_676number4impl6DecNum9normalizeEv@PLT
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	cmpl	$1, (%rax)
	je	.L109
.L103:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	cmpb	$1, 9(%rax)
	jne	.L103
	call	_ZNK6icu_676number4impl6DecNum10isNegativeEv@PLT
	testb	%al, %al
	jne	.L103
	movq	8(%rbx), %r12
	movq	(%r12), %rdi
	movl	4(%rdi), %eax
	addl	%eax, (%rbx)
	cmpb	$0, 12(%r12)
	jne	.L110
.L107:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	$0, 8(%rbx)
	jmp	.L103
.L110:
	call	uprv_free_67@PLT
	jmp	.L107
	.cfi_endproc
.LFE3343:
	.size	_ZN6icu_676number5ScaleC2EiPNS0_4impl6DecNumE, .-_ZN6icu_676number5ScaleC2EiPNS0_4impl6DecNumE
	.globl	_ZN6icu_676number5ScaleC1EiPNS0_4impl6DecNumE
	.set	_ZN6icu_676number5ScaleC1EiPNS0_4impl6DecNumE,_ZN6icu_676number5ScaleC2EiPNS0_4impl6DecNumE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number5ScaleC2ERKS1_
	.type	_ZN6icu_676number5ScaleC2ERKS1_, @function
_ZN6icu_676number5ScaleC2ERKS1_:
.LFB3346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	cmpq	$0, 8(%rsi)
	movq	$0, 8(%rdi)
	movl	%eax, (%rdi)
	movl	16(%rsi), %eax
	movl	%eax, 16(%rdi)
	je	.L111
	movq	%rdi, %rbx
	movl	$96, %edi
	movl	$0, -44(%rbp)
	movq	%rsi, %r12
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L113
	movq	8(%r12), %rsi
	leaq	-44(%rbp), %rdx
	movq	%rax, %rdi
	call	_ZN6icu_676number4impl6DecNumC1ERKS2_R10UErrorCode@PLT
.L113:
	movq	%r13, 8(%rbx)
.L111:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L119:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3346:
	.size	_ZN6icu_676number5ScaleC2ERKS1_, .-_ZN6icu_676number5ScaleC2ERKS1_
	.globl	_ZN6icu_676number5ScaleC1ERKS1_
	.set	_ZN6icu_676number5ScaleC1ERKS1_,_ZN6icu_676number5ScaleC2ERKS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number5ScaleaSERKS1_
	.type	_ZN6icu_676number5ScaleaSERKS1_, @function
_ZN6icu_676number5ScaleaSERKS1_:
.LFB3348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	cmpq	$0, 8(%rsi)
	movl	%eax, (%rdi)
	je	.L121
	movl	$96, %edi
	movl	$0, -44(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L122
	movq	8(%rbx), %rsi
	leaq	-44(%rbp), %rdx
	movq	%rax, %rdi
	call	_ZN6icu_676number4impl6DecNumC1ERKS2_R10UErrorCode@PLT
.L122:
	movq	%r13, 8(%r12)
.L123:
	movl	16(%rbx), %eax
	movl	%eax, 16(%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	$0, 8(%rdi)
	jmp	.L123
.L129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3348:
	.size	_ZN6icu_676number5ScaleaSERKS1_, .-_ZN6icu_676number5ScaleaSERKS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number5ScaleC2EOS1_
	.type	_ZN6icu_676number5ScaleC2EOS1_, @function
_ZN6icu_676number5ScaleC2EOS1_:
.LFB3350:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	movq	8(%rsi), %rax
	movq	$0, 8(%rsi)
	movq	%rax, 8(%rdi)
	movl	16(%rsi), %eax
	movl	%eax, 16(%rdi)
	ret
	.cfi_endproc
.LFE3350:
	.size	_ZN6icu_676number5ScaleC2EOS1_, .-_ZN6icu_676number5ScaleC2EOS1_
	.globl	_ZN6icu_676number5ScaleC1EOS1_
	.set	_ZN6icu_676number5ScaleC1EOS1_,_ZN6icu_676number5ScaleC2EOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number5ScaleaSEOS1_
	.type	_ZN6icu_676number5ScaleaSEOS1_, @function
_ZN6icu_676number5ScaleaSEOS1_:
.LFB3352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	(%rsi), %eax
	movq	8(%rdi), %r13
	movl	%eax, (%rdi)
	testq	%r13, %r13
	je	.L132
	cmpb	$0, 12(%r13)
	jne	.L138
.L133:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L132:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	movl	16(%rbx), %eax
	movl	%eax, 16(%r12)
	movq	%r12, %rax
	movq	$0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L133
	.cfi_endproc
.LFE3352:
	.size	_ZN6icu_676number5ScaleaSEOS1_, .-_ZN6icu_676number5ScaleaSEOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number5ScaleD2Ev
	.type	_ZN6icu_676number5ScaleD2Ev, @function
_ZN6icu_676number5ScaleD2Ev:
.LFB3354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	testq	%r12, %r12
	je	.L139
	cmpb	$0, 12(%r12)
	jne	.L143
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3354:
	.size	_ZN6icu_676number5ScaleD2Ev, .-_ZN6icu_676number5ScaleD2Ev
	.globl	_ZN6icu_676number5ScaleD1Ev
	.set	_ZN6icu_676number5ScaleD1Ev,_ZN6icu_676number5ScaleD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number5Scale4noneEv
	.type	_ZN6icu_676number5Scale4noneEv, @function
_ZN6icu_676number5Scale4noneEv:
.LFB3356:
	.cfi_startproc
	endbr64
	movl	$0, (%rdi)
	movq	%rdi, %rax
	movq	$0, 8(%rdi)
	movl	$0, 16(%rdi)
	ret
	.cfi_endproc
.LFE3356:
	.size	_ZN6icu_676number5Scale4noneEv, .-_ZN6icu_676number5Scale4noneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number5Scale10powerOfTenEi
	.type	_ZN6icu_676number5Scale10powerOfTenEi, @function
_ZN6icu_676number5Scale10powerOfTenEi:
.LFB3357:
	.cfi_startproc
	endbr64
	movl	%esi, (%rdi)
	movq	%rdi, %rax
	movq	$0, 8(%rdi)
	movl	$0, 16(%rdi)
	ret
	.cfi_endproc
.LFE3357:
	.size	_ZN6icu_676number5Scale10powerOfTenEi, .-_ZN6icu_676number5Scale10powerOfTenEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number5Scale9byDecimalENS_11StringPieceE
	.type	_ZN6icu_676number5Scale9byDecimalENS_11StringPieceE, @function
_ZN6icu_676number5Scale9byDecimalENS_11StringPieceE:
.LFB3358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L147
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN6icu_676number4impl6DecNumC1Ev@PLT
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L163
	movl	$0, (%r12)
	movq	$0, 8(%r12)
	movl	%eax, 16(%r12)
	cmpb	$0, 12(%r13)
	jne	.L164
.L159:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L146:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L159
.L147:
	movl	-44(%rbp), %eax
	movl	$0, (%r12)
	movq	$0, 8(%r12)
	testl	%eax, %eax
	jle	.L155
	movl	%eax, 16(%r12)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	-44(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl6DecNum5setToENS_11StringPieceER10UErrorCode@PLT
	movl	-44(%rbp), %eax
	movl	$0, (%r12)
	testl	%eax, %eax
	jle	.L152
	movq	$0, 8(%r12)
	movl	%eax, 16(%r12)
	cmpb	$0, 12(%r13)
	je	.L159
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L155:
	movl	$7, 16(%r12)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L152:
	movq	%r13, 8(%r12)
	movq	%r13, %rdi
	movl	$0, 16(%r12)
	call	_ZN6icu_676number4impl6DecNum9normalizeEv@PLT
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	cmpl	$1, (%rax)
	jne	.L146
	cmpb	$1, 9(%rax)
	jne	.L146
	call	_ZNK6icu_676number4impl6DecNum10isNegativeEv@PLT
	testb	%al, %al
	jne	.L146
	movq	8(%r12), %r13
	movq	0(%r13), %rdi
	movl	4(%rdi), %eax
	addl	%eax, (%r12)
	cmpb	$0, 12(%r13)
	jne	.L166
.L154:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	$0, 8(%r12)
	jmp	.L146
.L165:
	call	__stack_chk_fail@PLT
.L166:
	call	uprv_free_67@PLT
	jmp	.L154
	.cfi_endproc
.LFE3358:
	.size	_ZN6icu_676number5Scale9byDecimalENS_11StringPieceE, .-_ZN6icu_676number5Scale9byDecimalENS_11StringPieceE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number5Scale8byDoubleEd
	.type	_ZN6icu_676number5Scale8byDoubleEd, @function
_ZN6icu_676number5Scale8byDoubleEd:
.LFB3359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$96, %edi
	subq	$32, %rsp
	movsd	%xmm0, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L168
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN6icu_676number4impl6DecNumC1Ev@PLT
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jle	.L184
	movl	$0, (%r12)
	movq	$0, 8(%r12)
	movl	%eax, 16(%r12)
	cmpb	$0, 12(%r13)
	jne	.L185
.L180:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L167:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L186
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L180
.L168:
	movl	-28(%rbp), %eax
	movl	$0, (%r12)
	movq	$0, 8(%r12)
	testl	%eax, %eax
	jle	.L176
	movl	%eax, 16(%r12)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L184:
	movsd	-40(%rbp), %xmm0
	leaq	-28(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl6DecNum5setToEdR10UErrorCode@PLT
	movl	-28(%rbp), %eax
	movl	$0, (%r12)
	testl	%eax, %eax
	jle	.L173
	movq	$0, 8(%r12)
	movl	%eax, 16(%r12)
	cmpb	$0, 12(%r13)
	je	.L180
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$7, 16(%r12)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L173:
	movq	%r13, 8(%r12)
	movq	%r13, %rdi
	movl	$0, 16(%r12)
	call	_ZN6icu_676number4impl6DecNum9normalizeEv@PLT
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	cmpl	$1, (%rax)
	jne	.L167
	cmpb	$1, 9(%rax)
	jne	.L167
	call	_ZNK6icu_676number4impl6DecNum10isNegativeEv@PLT
	testb	%al, %al
	jne	.L167
	movq	8(%r12), %r13
	movq	0(%r13), %rdi
	movl	4(%rdi), %eax
	addl	%eax, (%r12)
	cmpb	$0, 12(%r13)
	jne	.L187
.L175:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	$0, 8(%r12)
	jmp	.L167
.L186:
	call	__stack_chk_fail@PLT
.L187:
	call	uprv_free_67@PLT
	jmp	.L175
	.cfi_endproc
.LFE3359:
	.size	_ZN6icu_676number5Scale8byDoubleEd, .-_ZN6icu_676number5Scale8byDoubleEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number5Scale21byDoubleAndPowerOfTenEdi
	.type	_ZN6icu_676number5Scale21byDoubleAndPowerOfTenEdi, @function
_ZN6icu_676number5Scale21byDoubleAndPowerOfTenEdi:
.LFB3360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$40, %rsp
	movsd	%xmm0, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L189
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN6icu_676number4impl6DecNumC1Ev@PLT
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L206
.L205:
	movl	$0, (%r12)
	movq	$0, 8(%r12)
	movl	%eax, 16(%r12)
	cmpb	$0, 12(%r13)
	jne	.L207
.L201:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L188:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L201
.L189:
	movl	-44(%rbp), %eax
	movl	$0, (%r12)
	movq	$0, 8(%r12)
	testl	%eax, %eax
	jle	.L197
	movl	%eax, 16(%r12)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L206:
	movsd	-56(%rbp), %xmm0
	leaq	-44(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl6DecNum5setToEdR10UErrorCode@PLT
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L205
	movl	%ebx, (%r12)
	movq	%r13, %rdi
	movq	%r13, 8(%r12)
	movl	$0, 16(%r12)
	call	_ZN6icu_676number4impl6DecNum9normalizeEv@PLT
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	cmpl	$1, (%rax)
	jne	.L188
	cmpb	$1, 9(%rax)
	jne	.L188
	call	_ZNK6icu_676number4impl6DecNum10isNegativeEv@PLT
	testb	%al, %al
	jne	.L188
	movq	8(%r12), %r13
	movq	0(%r13), %rdi
	movl	4(%rdi), %eax
	addl	%eax, (%r12)
	cmpb	$0, 12(%r13)
	jne	.L209
.L196:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	$0, 8(%r12)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L197:
	movl	$7, 16(%r12)
	jmp	.L188
.L208:
	call	__stack_chk_fail@PLT
.L209:
	call	uprv_free_67@PLT
	jmp	.L196
	.cfi_endproc
.LFE3360:
	.size	_ZN6icu_676number5Scale21byDoubleAndPowerOfTenEdi, .-_ZN6icu_676number5Scale21byDoubleAndPowerOfTenEdi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number5Scale7applyToERNS0_4impl15DecimalQuantityE
	.type	_ZNK6icu_676number5Scale7applyToERNS0_4impl15DecimalQuantityE, @function
_ZNK6icu_676number5Scale7applyToERNS0_4impl15DecimalQuantityE:
.LFB3361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %esi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi@PLT
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L210
	leaq	-28(%rbp), %rdx
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantity10multiplyByERKNS1_6DecNumER10UErrorCode@PLT
.L210:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L217
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L217:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3361:
	.size	_ZNK6icu_676number5Scale7applyToERNS0_4impl15DecimalQuantityE, .-_ZNK6icu_676number5Scale7applyToERNS0_4impl15DecimalQuantityE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number5Scale17applyReciprocalToERNS0_4impl15DecimalQuantityE
	.type	_ZNK6icu_676number5Scale17applyReciprocalToERNS0_4impl15DecimalQuantityE, @function
_ZNK6icu_676number5Scale17applyReciprocalToERNS0_4impl15DecimalQuantityE:
.LFB3362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %esi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	negl	%esi
	call	_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi@PLT
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L218
	leaq	-28(%rbp), %rdx
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantity8divideByERKNS1_6DecNumER10UErrorCode@PLT
.L218:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L225
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L225:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3362:
	.size	_ZNK6icu_676number5Scale17applyReciprocalToERNS0_4impl15DecimalQuantityE, .-_ZNK6icu_676number5Scale17applyReciprocalToERNS0_4impl15DecimalQuantityE
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl23MultiplierFormatHandler11setAndChainERKNS0_5ScaleEPKNS1_19MicroPropsGeneratorE
	.type	_ZN6icu_676number4impl23MultiplierFormatHandler11setAndChainERKNS0_5ScaleEPKNS1_19MicroPropsGeneratorE, @function
_ZN6icu_676number4impl23MultiplierFormatHandler11setAndChainERKNS0_5ScaleEPKNS1_19MicroPropsGeneratorE:
.LFB3363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	cmpq	$0, 8(%rsi)
	movl	%eax, 8(%rdi)
	je	.L227
	movl	$96, %edi
	movl	$0, -44(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L228
	movq	8(%r12), %rsi
	leaq	-44(%rbp), %rdx
	movq	%rax, %rdi
	call	_ZN6icu_676number4impl6DecNumC1ERKS2_R10UErrorCode@PLT
.L228:
	movq	%r14, 16(%rbx)
.L229:
	movl	16(%r12), %eax
	movq	%r13, 32(%rbx)
	movl	%eax, 24(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L235
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	movq	$0, 16(%rdi)
	jmp	.L229
.L235:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3363:
	.size	_ZN6icu_676number4impl23MultiplierFormatHandler11setAndChainERKNS0_5ScaleEPKNS1_19MicroPropsGeneratorE, .-_ZN6icu_676number4impl23MultiplierFormatHandler11setAndChainERKNS0_5ScaleEPKNS1_19MicroPropsGeneratorE
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_676number4impl23MultiplierFormatHandlerE
	.section	.rodata._ZTSN6icu_676number4impl23MultiplierFormatHandlerE,"aG",@progbits,_ZTSN6icu_676number4impl23MultiplierFormatHandlerE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl23MultiplierFormatHandlerE, @object
	.size	_ZTSN6icu_676number4impl23MultiplierFormatHandlerE, 47
_ZTSN6icu_676number4impl23MultiplierFormatHandlerE:
	.string	"N6icu_676number4impl23MultiplierFormatHandlerE"
	.weak	_ZTIN6icu_676number4impl23MultiplierFormatHandlerE
	.section	.data.rel.ro._ZTIN6icu_676number4impl23MultiplierFormatHandlerE,"awG",@progbits,_ZTIN6icu_676number4impl23MultiplierFormatHandlerE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl23MultiplierFormatHandlerE, @object
	.size	_ZTIN6icu_676number4impl23MultiplierFormatHandlerE, 56
_ZTIN6icu_676number4impl23MultiplierFormatHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl23MultiplierFormatHandlerE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl19MicroPropsGeneratorE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTVN6icu_676number4impl23MultiplierFormatHandlerE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl23MultiplierFormatHandlerE,"awG",@progbits,_ZTVN6icu_676number4impl23MultiplierFormatHandlerE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl23MultiplierFormatHandlerE, @object
	.size	_ZTVN6icu_676number4impl23MultiplierFormatHandlerE, 40
_ZTVN6icu_676number4impl23MultiplierFormatHandlerE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl23MultiplierFormatHandlerE
	.quad	_ZN6icu_676number4impl23MultiplierFormatHandlerD1Ev
	.quad	_ZN6icu_676number4impl23MultiplierFormatHandlerD0Ev
	.quad	_ZNK6icu_676number4impl23MultiplierFormatHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
