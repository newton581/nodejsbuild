	.file	"measunit_extra.cpp"
	.text
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_117cleanupUnitExtrasEv, @function
_ZN6icu_6712_GLOBAL__N_117cleanupUnitExtrasEv:
.LFB3097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZN6icu_6712_GLOBAL__N_129kSerializedUnitExtrasStemTrieE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uprv_free_67@PLT
	movl	$1, %eax
	movq	$0, _ZN6icu_6712_GLOBAL__N_129kSerializedUnitExtrasStemTrieE(%rip)
	movl	$0, _ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip)
	mfence
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3097:
	.size	_ZN6icu_6712_GLOBAL__N_117cleanupUnitExtrasEv, .-_ZN6icu_6712_GLOBAL__N_117cleanupUnitExtrasEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"per-"
.LC1:
	.string	"square-"
.LC2:
	.string	"cubic-"
.LC3:
	.string	"p1"
	.text
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_115serializeSingleERKNS_14SingleUnitImplEbRNS_10CharStringER10UErrorCode, @function
_ZN6icu_6712_GLOBAL__N_115serializeSingleERKNS_14SingleUnitImplEbRNS_10CharStringER10UErrorCode:
.LFB3130:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%sil, %sil
	je	.L5
	movl	8(%rdi), %ecx
	testl	%ecx, %ecx
	js	.L27
.L5:
	cmpl	$-1, (%rbx)
	je	.L26
	movl	8(%rbx), %eax
	movl	8(%rbx), %r13d
	sarl	$31, %eax
	xorl	%eax, %r13d
	subl	%eax, %r13d
	movl	%r13d, %r15d
	testb	%r13b, %r13b
	jne	.L8
.L26:
	movl	$5, (%r12)
.L4:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	cmpb	$1, %r13b
	je	.L10
	leaq	-144(%rbp), %rdi
	leaq	.LC1(%rip), %rsi
	cmpb	$2, %r13b
	je	.L25
	cmpb	$3, %r13b
	je	.L29
	cmpb	$9, %r13b
	jle	.L30
	cmpb	$15, %r13b
	jle	.L31
	movl	$1, (%r12)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$112, %esi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	leal	48(%r13), %esi
	movsbl	%sil, %esi
.L24:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	%r12, %rdx
	movl	$45, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
.L10:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L4
	movl	4(%rbx), %edx
	testl	%edx, %edx
	je	.L15
	leaq	_ZN6icu_6712_GLOBAL__N_1L16gSIPrefixStringsE(%rip), %rax
	leaq	320(%rax), %rcx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$16, %rax
	cmpq	%rax, %rcx
	je	.L15
.L17:
	cmpl	8(%rax), %edx
	jne	.L16
	movq	(%rax), %rsi
	leaq	-144(%rbp), %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L4
	.p2align 4,,10
	.p2align 3
.L15:
	movslq	(%rbx), %rdx
	leaq	_ZN6icu_6712_GLOBAL__N_1L12gSimpleUnitsE(%rip), %rax
	leaq	-128(%rbp), %r13
	movq	%r13, %rdi
	movq	(%rax,%rdx,8), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	-144(%rbp), %rdi
	leaq	.LC0(%rip), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	-144(%rbp), %rdi
	leaq	.LC2(%rip), %rsi
.L25:
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	-144(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movsbw	%r13b, %ax
	movl	%r13d, %edx
	imull	$103, %eax, %eax
	sarb	$7, %dl
	sarw	$10, %ax
	subl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	subl	%eax, %r15d
	leal	48(%r15), %esi
	movzbl	%sil, %esi
	jmp	.L24
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3130:
	.size	_ZN6icu_6712_GLOBAL__N_115serializeSingleERKNS_14SingleUnitImplEbRNS_10CharStringER10UErrorCode, .-_ZN6icu_6712_GLOBAL__N_115serializeSingleERKNS_14SingleUnitImplEbRNS_10CharStringER10UErrorCode
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_118compareSingleUnitsEPKvS2_S2_, @function
_ZN6icu_6712_GLOBAL__N_118compareSingleUnitsEPKvS2_S2_:
.LFB3129:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	(%rsi), %rdx
	movq	(%r8), %rcx
	movl	8(%rdx), %r8d
	testl	%r8d, %r8d
	js	.L44
	je	.L35
	movl	8(%rcx), %esi
	movl	$-1, %eax
	testl	%esi, %esi
	js	.L32
.L35:
	movl	(%rcx), %edi
	movl	$-1, %eax
	cmpl	%edi, (%rdx)
	jl	.L32
	movl	$1, %eax
	jg	.L32
	movl	4(%rcx), %edi
	movl	$-1, %eax
	cmpl	%edi, 4(%rdx)
	jl	.L32
	setg	%al
	movzbl	%al, %eax
.L32:
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movl	8(%rcx), %edi
	movl	$1, %eax
	testl	%edi, %edi
	jle	.L35
	ret
	.cfi_endproc
.LFE3129:
	.size	_ZN6icu_6712_GLOBAL__N_118compareSingleUnitsEPKvS2_S2_, .-_ZN6icu_6712_GLOBAL__N_118compareSingleUnitsEPKvS2_S2_
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_110appendImplERNS_15MeasureUnitImplERKNS_14SingleUnitImplER10UErrorCode, @function
_ZN6icu_6712_GLOBAL__N_110appendImplERNS_15MeasureUnitImplERKNS_14SingleUnitImplER10UErrorCode:
.LFB3132:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rsi), %edi
	cmpl	$-1, %edi
	je	.L45
	movl	8(%r12), %r14d
	movq	%rsi, %rbx
	movq	%rdx, %r13
	testl	%r14d, %r14d
	jle	.L47
	movq	16(%r12), %rdx
	leal	-1(%r14), %ecx
	leaq	8(%rdx), %rax
	leaq	(%rax,%rcx,8), %rsi
	xorl	%ecx, %ecx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L48:
	je	.L50
	movl	8(%rbx), %r8d
	testl	%r8d, %r8d
	js	.L49
.L50:
	cmpl	(%rdx), %edi
	jne	.L49
	movl	4(%rbx), %r10d
	cmpl	%r10d, 4(%rdx)
	cmove	%rdx, %rcx
.L49:
	movq	%rax, %rdx
	cmpq	%rax, %rsi
	je	.L75
.L77:
	addq	$8, %rax
.L51:
	movq	(%rdx), %rdx
	movl	8(%rdx), %r10d
	testl	%r10d, %r10d
	jns	.L48
	movl	8(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L50
	movq	%rax, %rdx
	cmpq	%rax, %rsi
	jne	.L77
	.p2align 4,,10
	.p2align 3
.L75:
	testq	%rcx, %rcx
	je	.L47
	movl	8(%rbx), %eax
	addl	%eax, 8(%rcx)
.L52:
	testq	%rcx, %rcx
	sete	%al
.L45:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movl	24(%r12), %r15d
	cmpl	%r14d, %r15d
	jne	.L53
	cmpl	$8, %r14d
	je	.L54
	leal	(%r14,%r14), %r8d
	testl	%r8d, %r8d
	jg	.L78
.L59:
	movl	$7, 0(%r13)
	xorl	%eax, %eax
	jmp	.L45
.L54:
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movl	$32, %r8d
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L59
.L60:
	cmpl	%r15d, 24(%r12)
	movq	16(%r12), %r9
	movq	%rcx, %rdi
	movl	%r8d, -64(%rbp)
	cmovle	24(%r12), %r15d
	movq	%r9, %rsi
	movq	%r9, -56(%rbp)
	cmpl	%r15d, %r8d
	movslq	%r15d, %rdx
	cmovle	%r8, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	movl	-64(%rbp), %r8d
	movq	-56(%rbp), %r9
	movq	%rax, %rcx
.L56:
	cmpb	$0, 28(%r12)
	jne	.L79
.L57:
	movq	%rcx, 16(%r12)
	movl	%r8d, 24(%r12)
	movb	$1, 28(%r12)
.L53:
	movl	$12, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L58
	movl	$4294967295, %ecx
	movl	$1, 8(%rax)
	movq	%rcx, (%rax)
	movslq	8(%r12), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, 8(%r12)
	movq	16(%r12), %rcx
	movq	%rax, (%rcx,%rdx,8)
	movq	(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rdx, (%rax)
	movl	8(%rbx), %edx
	movl	%edx, 8(%rax)
	jmp	.L52
.L78:
	movslq	%r8d, %rdi
	movl	%r8d, -56(%rbp)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L59
	testl	%r14d, %r14d
	movq	16(%r12), %r9
	movslq	-56(%rbp), %r8
	jle	.L56
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r9, %rdi
	movq	%rcx, -64(%rbp)
	movl	%r8d, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-64(%rbp), %rcx
	movl	-56(%rbp), %r8d
	jmp	.L57
.L58:
	movslq	8(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 8(%r12)
	movq	16(%r12), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L59
	.cfi_endproc
.LFE3132:
	.size	_ZN6icu_6712_GLOBAL__N_110appendImplERNS_15MeasureUnitImplERKNS_14SingleUnitImplER10UErrorCode, .-_ZN6icu_6712_GLOBAL__N_110appendImplERNS_15MeasureUnitImplERKNS_14SingleUnitImplER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_16Parser9parseImplERNS_15MeasureUnitImplER10UErrorCode, @function
_ZN6icu_6712_GLOBAL__N_16Parser9parseImplERNS_15MeasureUnitImplER10UErrorCode:
.LFB3125:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	movl	(%rdx), %edx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testl	%edx, %edx
	jg	.L80
	movl	16(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L80
	movl	(%rdi), %r15d
	cmpl	%r15d, %eax
	jle	.L80
	movl	$0, -84(%rbp)
	movq	%rsi, %r13
	leaq	24(%rdi), %r14
	.p2align 4,,10
	.p2align 3
.L141:
	movl	$4294967295, %ecx
	movl	$1, -60(%rbp)
	movq	%rcx, -68(%rbp)
	testl	%edx, %edx
	jg	.L80
	movq	32(%rbx), %rdx
	movl	$-1, 48(%rbx)
	movq	%rdx, 40(%rbx)
	cmpl	%r15d, %eax
	jle	.L140
	movslq	%r15d, %rdi
	movl	$-1, %r8d
	movl	$-1, %r12d
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L89:
	movzwl	%si, %r8d
	cmpw	$16447, %si
	ja	.L92
	sarl	$6, %r8d
	leal	-1(%r8), %r12d
.L90:
	movslq	%edi, %r8
	cmpl	$2, %eax
	je	.L87
.L88:
	cmpl	%edi, 16(%rbx)
	jle	.L87
.L85:
	leal	1(%rdi), %esi
	movq	8(%rbx), %rax
	movl	%r8d, -88(%rbp)
	movl	%esi, (%rbx)
	movsbl	(%rax,%rdi), %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrie4nextEi@PLT
	movslq	-88(%rbp), %r8
	testl	%eax, %eax
	je	.L87
	movslq	(%rbx), %rdi
	cmpl	$1, %eax
	je	.L88
	movq	40(%rbx), %r9
	movzwl	(%r9), %esi
	testw	%si, %si
	jns	.L89
	movl	%esi, %r8d
	movl	%esi, %r12d
	andw	$32767, %r8w
	andl	$32767, %r12d
	testw	$16384, %si
	je	.L90
	movzwl	2(%r9), %esi
	cmpw	$32767, %r8w
	je	.L91
	leal	-16384(%r12), %edx
	movslq	%edi, %r8
	sall	$16, %edx
	orl	%esi, %edx
	movl	%edx, %r12d
	cmpl	$2, %eax
	jne	.L88
	.p2align 4,,10
	.p2align 3
.L87:
	testl	%r12d, %r12d
	js	.L140
	movq	-96(%rbp), %rax
	movl	%r8d, (%rbx)
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jg	.L80
	testl	%r15d, %r15d
	jne	.L97
	leal	-192(%r12), %eax
	movb	$0, -88(%rbp)
	cmpl	$63, %eax
	jbe	.L207
.L98:
	movl	$0, -100(%rbp)
.L134:
	cmpl	$127, %r12d
	jle	.L119
	cmpl	$255, %r12d
	jle	.L140
	cmpl	$511, %r12d
	jle	.L208
	movq	-96(%rbp), %rax
	leal	-512(%r12), %edx
	movl	%edx, -68(%rbp)
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L80
	movq	%rax, %rdx
	movq	96(%r13), %rax
	leaq	-68(%rbp), %rsi
	movq	%r13, %rdi
	movl	$0, 152(%r13)
	movb	$0, (%rax)
	call	_ZN6icu_6712_GLOBAL__N_110appendImplERNS_15MeasureUnitImplERKNS_14SingleUnitImplER10UErrorCode
	cmpb	$0, -88(%rbp)
	je	.L135
	testb	%al, %al
	je	.L140
	addl	$1, -84(%rbp)
	movl	-84(%rbp), %ecx
	movl	$2, %eax
	cmpl	$1, %ecx
	je	.L138
	cmpl	$2, -84(%rbp)
	je	.L209
.L139:
	cmpl	%eax, 0(%r13)
	jne	.L140
.L138:
	movl	(%rbx), %r15d
	movl	16(%rbx), %eax
	cmpl	%eax, %r15d
	jge	.L80
	movq	-96(%rbp), %rcx
	movl	(%rcx), %edx
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L97:
	leal	-128(%r12), %eax
	cmpl	$63, %eax
	jbe	.L210
.L140:
	movq	-96(%rbp), %rax
	movl	$1, (%rax)
.L80:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L211
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movzwl	2(%r9), %edx
	cmpl	$32703, %r8d
	jg	.L93
	andl	$32704, %esi
	subl	$16448, %esi
	sall	$10, %esi
	orl	%edx, %esi
	movl	%esi, %r12d
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L93:
	movzwl	4(%r9), %esi
	sall	$16, %edx
	orl	%esi, %edx
	movl	%edx, %r12d
	jmp	.L90
.L135:
	addl	$1, -84(%rbp)
	movl	-84(%rbp), %eax
	cmpl	$1, %eax
	je	.L138
	cmpl	$2, -84(%rbp)
	movl	$1, %eax
	jne	.L139
.L209:
	movl	%eax, 0(%r13)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L91:
	movzwl	4(%r9), %edx
	sall	$16, %esi
	orl	%edx, %esi
	movl	%esi, %r12d
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L119:
	cmpl	$2, -100(%rbp)
	je	.L140
	leal	-64(%r12), %edx
	movl	$2, -100(%rbp)
	movl	%edx, -64(%rbp)
.L125:
	movslq	(%rbx), %rsi
	cmpl	16(%rbx), %esi
	jge	.L140
	movq	32(%rbx), %rax
	movl	$-1, 48(%rbx)
	movl	$-1, %r15d
	movl	$-1, %r12d
	movq	%rax, 40(%rbx)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L129:
	movzwl	%di, %r9d
	cmpw	$16447, %di
	ja	.L132
	sarl	$6, %r9d
	leal	-1(%r9), %r12d
.L130:
	movl	%esi, %r15d
	cmpl	$2, %eax
	je	.L127
.L128:
	cmpl	%esi, 16(%rbx)
	jle	.L127
.L126:
	leal	1(%rsi), %edi
	movq	8(%rbx), %rax
	movl	%edi, (%rbx)
	movq	%r14, %rdi
	movsbl	(%rax,%rsi), %esi
	call	_ZN6icu_6710UCharsTrie4nextEi@PLT
	testl	%eax, %eax
	je	.L127
	movslq	(%rbx), %rsi
	cmpl	$1, %eax
	je	.L128
	movq	40(%rbx), %r8
	movzwl	(%r8), %edi
	testw	%di, %di
	jns	.L129
	movl	%edi, %r9d
	movl	%edi, %r12d
	andw	$32767, %r9w
	andl	$32767, %r12d
	testw	$16384, %di
	je	.L130
	movzwl	2(%r8), %edi
	cmpw	$32767, %r9w
	je	.L131
	leal	-16384(%r12), %edx
	movl	%esi, %r15d
	sall	$16, %edx
	orl	%edi, %edx
	movl	%edx, %r12d
	cmpl	$2, %eax
	jne	.L128
	.p2align 4,,10
	.p2align 3
.L127:
	testl	%r12d, %r12d
	js	.L140
	movq	-96(%rbp), %rax
	movl	%r15d, (%rbx)
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L134
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L132:
	movzwl	2(%r8), %edx
	cmpl	$32703, %r9d
	jg	.L133
	andl	$32704, %edi
	subl	$16448, %edi
	sall	$10, %edi
	orl	%edx, %edi
	movl	%edi, %r12d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L133:
	movzwl	4(%r8), %edi
	sall	$16, %edx
	orl	%edi, %edx
	movl	%edx, %r12d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L131:
	movzwl	4(%r8), %edx
	sall	$16, %edi
	orl	%edx, %edi
	movl	%edi, %r12d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L210:
	cmpl	$129, %r12d
	je	.L107
	cmpl	$130, %r12d
	je	.L108
	movb	$0, -88(%rbp)
	cmpl	$128, %r12d
	je	.L212
.L109:
	movq	32(%rbx), %rax
	movl	$-1, 48(%rbx)
	movq	%rax, 40(%rbx)
	cmpl	16(%rbx), %r8d
	jge	.L140
	movl	$-1, %r15d
	movl	$-1, %r12d
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L113:
	movzwl	%si, %r9d
	cmpw	$16447, %si
	ja	.L116
	sarl	$6, %r9d
	leal	-1(%r9), %r12d
.L114:
	movl	%r8d, %r15d
	cmpl	$2, %eax
	je	.L111
.L112:
	cmpl	%r8d, 16(%rbx)
	jle	.L111
.L110:
	leal	1(%r8), %esi
	movq	8(%rbx), %rax
	movq	%r14, %rdi
	movl	%esi, (%rbx)
	movsbl	(%rax,%r8), %esi
	call	_ZN6icu_6710UCharsTrie4nextEi@PLT
	testl	%eax, %eax
	je	.L111
	movslq	(%rbx), %r8
	cmpl	$1, %eax
	je	.L112
	movq	40(%rbx), %rdi
	movzwl	(%rdi), %esi
	testw	%si, %si
	jns	.L113
	movl	%esi, %r9d
	movl	%esi, %r12d
	andw	$32767, %r9w
	andl	$32767, %r12d
	testw	$16384, %si
	je	.L114
	movzwl	2(%rdi), %esi
	cmpw	$32767, %r9w
	je	.L115
	leal	-16384(%r12), %edx
	sall	$16, %edx
	orl	%esi, %edx
	movl	%edx, %r12d
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L116:
	movzwl	2(%rdi), %edx
	cmpl	$32703, %r9d
	jg	.L117
	andl	$32704, %esi
	subl	$16448, %esi
	sall	$10, %esi
	orl	%edx, %esi
	movl	%esi, %r12d
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L208:
	movl	-100(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L140
	movsbl	%r12b, %edx
	imull	-60(%rbp), %edx
	movl	$1, -100(%rbp)
	movl	%edx, -60(%rbp)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L117:
	movzwl	4(%rdi), %esi
	sall	$16, %edx
	orl	%esi, %edx
	movl	%edx, %r12d
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L111:
	testl	%r12d, %r12d
	js	.L140
	movq	-96(%rbp), %rax
	movl	%r15d, (%rbx)
	movl	(%rax), %esi
	testl	%esi, %esi
	jle	.L98
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L207:
	movq	32(%rbx), %rax
	movb	$1, 56(%rbx)
	movl	$-1, -60(%rbp)
	movq	%rax, 40(%rbx)
	movl	$-1, 48(%rbx)
	cmpl	16(%rbx), %r8d
	jge	.L140
	movl	$-1, %r15d
	movl	$-1, %r12d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L102:
	movzwl	%si, %r9d
	cmpw	$16447, %si
	ja	.L105
	sarl	$6, %r9d
	leal	-1(%r9), %r12d
.L103:
	movl	%r8d, %r15d
	cmpl	$2, %eax
	je	.L100
.L101:
	cmpl	%r8d, 16(%rbx)
	jle	.L100
.L99:
	leal	1(%r8), %esi
	movq	8(%rbx), %rax
	movq	%r14, %rdi
	movl	%esi, (%rbx)
	movsbl	(%rax,%r8), %esi
	call	_ZN6icu_6710UCharsTrie4nextEi@PLT
	testl	%eax, %eax
	je	.L100
	movslq	(%rbx), %r8
	cmpl	$1, %eax
	je	.L101
	movq	40(%rbx), %rdi
	movzwl	(%rdi), %esi
	testw	%si, %si
	jns	.L102
	movl	%esi, %r9d
	movl	%esi, %r12d
	andw	$32767, %r9w
	andl	$32767, %r12d
	testw	$16384, %si
	je	.L103
	movzwl	2(%rdi), %esi
	cmpw	$32767, %r9w
	je	.L104
	leal	-16384(%r12), %edx
	sall	$16, %edx
	orl	%esi, %edx
	movl	%edx, %r12d
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L105:
	movzwl	2(%rdi), %edx
	cmpl	$32703, %r9d
	jg	.L106
	andl	$32704, %esi
	subl	$16448, %esi
	sall	$10, %esi
	orl	%edx, %esi
	movl	%esi, %r12d
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L106:
	movzwl	4(%rdi), %esi
	sall	$16, %edx
	orl	%esi, %edx
	movl	%edx, %r12d
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L100:
	testl	%r12d, %r12d
	js	.L140
	movq	-96(%rbp), %rax
	movl	%r15d, (%rbx)
	movl	(%rax), %edi
	testl	%edi, %edi
	jg	.L80
	movb	$0, -88(%rbp)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L115:
	movzwl	4(%rdi), %edx
	sall	$16, %esi
	orl	%edx, %esi
	movl	%esi, %r12d
	jmp	.L114
.L104:
	movzwl	4(%rdi), %edx
	sall	$16, %esi
	orl	%edx, %esi
	movl	%esi, %r12d
	jmp	.L103
.L108:
	cmpb	$0, 56(%rbx)
	jne	.L140
	movb	$1, -88(%rbp)
	jmp	.L109
.L107:
	movzbl	56(%rbx), %eax
	movb	%al, -88(%rbp)
	testb	%al, %al
	je	.L109
	movl	$-1, -60(%rbp)
	movb	$0, -88(%rbp)
	jmp	.L109
.L212:
	movb	$1, 56(%rbx)
	movl	$-1, -60(%rbp)
	jmp	.L109
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3125:
	.size	_ZN6icu_6712_GLOBAL__N_16Parser9parseImplERNS_15MeasureUnitImplER10UErrorCode, .-_ZN6icu_6712_GLOBAL__N_16Parser9parseImplERNS_15MeasureUnitImplER10UErrorCode
	.section	.rodata.str1.1
.LC4:
	.string	"-and-"
.LC5:
	.string	"-per-"
	.text
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_19serializeERNS_15MeasureUnitImplER10UErrorCode, @function
_ZN6icu_6712_GLOBAL__N_19serializeERNS_15MeasureUnitImplER10UErrorCode:
.LFB3131:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L213
	movq	%rsi, %rbx
	movl	8(%rdi), %esi
	movq	%rdi, %r14
	testl	%esi, %esi
	je	.L213
	cmpl	$1, (%r14)
	movq	16(%rdi), %rdi
	je	.L243
.L216:
	movq	(%rdi), %rdi
	leaq	96(%r14), %r13
	movq	%rbx, %rcx
	movl	$1, %esi
	movq	%r13, %rdx
	call	_ZN6icu_6712_GLOBAL__N_115serializeSingleERKNS_14SingleUnitImplEbRNS_10CharStringER10UErrorCode
	cmpl	$1, 8(%r14)
	jle	.L213
	leaq	-144(%rbp), %rax
	movl	$8, %r15d
	movl	$1, %r12d
	movq	%rax, -168(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L233:
	movq	16(%r14), %rax
	cmpl	$2, (%r14)
	movq	(%rax,%r15), %r8
	leaq	-8(%rax,%r15), %rdx
	je	.L244
	movq	(%rdx), %rax
	movl	8(%rax), %esi
	testl	%esi, %esi
	jle	.L219
	movl	8(%r8), %ecx
	testl	%ecx, %ecx
	js	.L245
.L219:
	movq	%rbx, %rdx
	movl	$45, %esi
	movq	%r13, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-152(%rbp), %r8
.L220:
	cmpl	$-1, (%r8)
	je	.L242
	movl	8(%r8), %eax
	movl	8(%r8), %ecx
	sarl	$31, %eax
	xorl	%eax, %ecx
	subl	%eax, %ecx
	testb	%cl, %cl
	je	.L242
	cmpb	$1, %cl
	je	.L224
	cmpb	$2, %cl
	je	.L246
	cmpb	$3, %cl
	je	.L247
	cmpb	$9, %cl
	jle	.L248
	cmpb	$15, %cl
	jle	.L249
	movl	$1, (%rbx)
	addl	$1, %r12d
	addq	$8, %r15
	cmpl	8(%r14), %r12d
	jl	.L233
	.p2align 4,,10
	.p2align 3
.L213:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L250
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movl	$5, (%rbx)
.L218:
	addl	$1, %r12d
	addq	$8, %r15
	cmpl	8(%r14), %r12d
	jl	.L233
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L248:
	movl	$112, %esi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movl	%ecx, -172(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-172(%rbp), %ecx
	addl	$48, %ecx
	movsbl	%cl, %esi
.L240:
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	%rbx, %rdx
	movl	$45, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-152(%rbp), %r8
.L224:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L218
	movl	4(%r8), %edx
	testl	%edx, %edx
	je	.L231
	leaq	_ZN6icu_6712_GLOBAL__N_1L16gSIPrefixStringsE(%rip), %rax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L230:
	addq	$16, %rax
	leaq	320+_ZN6icu_6712_GLOBAL__N_1L16gSIPrefixStringsE(%rip), %rcx
	cmpq	%rax, %rcx
	je	.L231
.L232:
	cmpl	8(%rax), %edx
	jne	.L230
	movq	(%rax), %rsi
	movq	-168(%rbp), %rdi
	movq	%r8, -152(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	(%rbx), %eax
	movq	-152(%rbp), %r8
	testl	%eax, %eax
	jg	.L218
	.p2align 4,,10
	.p2align 3
.L231:
	movslq	(%r8), %rax
	leaq	_ZN6icu_6712_GLOBAL__N_1L12gSimpleUnitsE(%rip), %rdi
	movq	(%rdi,%rax,8), %rsi
	movq	-160(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-160(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	-160(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L244:
	movq	-168(%rbp), %rdi
	leaq	.LC4(%rip), %rsi
	movq	%r8, -152(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movl	$1, %esi
	movq	-152(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6712_GLOBAL__N_115serializeSingleERKNS_14SingleUnitImplEbRNS_10CharStringER10UErrorCode
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L245:
	movq	-168(%rbp), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%r8, -152(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-152(%rbp), %r8
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L243:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$8, %edx
	pushq	%rbx
	leaq	_ZN6icu_6712_GLOBAL__N_118compareSingleUnitsEPKvS2_S2_(%rip), %rcx
	call	uprv_sortArray_67@PLT
	movl	(%rbx), %r9d
	popq	%rdi
	popq	%r8
	testl	%r9d, %r9d
	jg	.L213
	movq	16(%r14), %rdi
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L246:
	movq	%r8, -152(%rbp)
	leaq	.LC1(%rip), %rsi
.L241:
	movq	-168(%rbp), %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-152(%rbp), %r8
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%r8, -152(%rbp)
	leaq	.LC2(%rip), %rsi
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L249:
	movq	-168(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	movb	%cl, -172(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movsbw	-172(%rbp), %ax
	movl	%eax, %r11d
	imull	$103, %eax, %eax
	movl	%r11d, %edx
	sarb	$7, %dl
	sarw	$10, %ax
	subl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	subl	%eax, %r11d
	leal	48(%r11), %esi
	movzbl	%sil, %esi
	jmp	.L240
.L250:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3131:
	.size	_ZN6icu_6712_GLOBAL__N_19serializeERNS_15MeasureUnitImplER10UErrorCode, .-_ZN6icu_6712_GLOBAL__N_19serializeERNS_15MeasureUnitImplER10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3389:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3389:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3392:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L264
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L252
	cmpb	$0, 12(%rbx)
	jne	.L265
.L256:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L252:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L256
	.cfi_endproc
.LFE3392:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3395:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L268
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3395:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3398:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L271
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3398:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L277
.L273:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L278
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L278:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3400:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3401:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3401:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3402:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3402:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3403:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3403:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3404:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3404:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3405:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3405:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3406:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L294
	testl	%edx, %edx
	jle	.L294
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L297
.L286:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L286
	.cfi_endproc
.LFE3406:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L301
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L301
	testl	%r12d, %r12d
	jg	.L308
	cmpb	$0, 12(%rbx)
	jne	.L309
.L303:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L303
.L309:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L301:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3407:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L311
	movq	(%rdi), %r8
.L312:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L315
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L315
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L315:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3408:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3409:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L322
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3409:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3410:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3410:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3411:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3411:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3412:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3412:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3414:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3414:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3416:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3416:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714SingleUnitImpl5buildER10UErrorCode
	.type	_ZNK6icu_6714SingleUnitImpl5buildER10UErrorCode, @function
_ZNK6icu_6714SingleUnitImpl5buildER10UErrorCode:
.LFB3134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-208(%rbp), %r14
	movq	%rdi, %r13
	pushq	%r12
	movq	%r14, %rdi
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$176, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-176(%rbp), %rax
	movb	$0, -180(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-99(%rbp), %rax
	movq	%rax, -112(%rbp)
	xorl	%eax, %eax
	movl	$0, -208(%rbp)
	movl	$0, -200(%rbp)
	movl	$8, -184(%rbp)
	movl	$0, -56(%rbp)
	movl	$40, -104(%rbp)
	movw	%ax, -100(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_110appendImplERNS_15MeasureUnitImplERKNS_14SingleUnitImplER10UErrorCode
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712_GLOBAL__N_19serializeERNS_15MeasureUnitImplER10UErrorCode
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711MeasureUnitC1EONS_15MeasureUnitImplE@PLT
	cmpb	$0, -100(%rbp)
	jne	.L338
.L329:
	movl	-200(%rbp), %eax
	movq	-192(%rbp), %rdi
	testl	%eax, %eax
	jle	.L330
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L334:
	movq	(%rdi,%rbx,8), %r8
	testq	%r8, %r8
	je	.L331
	movq	%r8, %rdi
	addq	$1, %rbx
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-200(%rbp), %eax
	movq	-192(%rbp), %rdi
	cmpl	%ebx, %eax
	jg	.L334
.L330:
	cmpb	$0, -180(%rbp)
	jne	.L339
.L328:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L340
	addq	$176, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L334
	cmpb	$0, -180(%rbp)
	je	.L328
.L339:
	call	uprv_free_67@PLT
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L338:
	movq	-112(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L329
.L340:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3134:
	.size	_ZNK6icu_6714SingleUnitImpl5buildER10UErrorCode, .-_ZNK6icu_6714SingleUnitImpl5buildER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715MeasureUnitImpl14takeReciprocalER10UErrorCode
	.type	_ZN6icu_6715MeasureUnitImpl14takeReciprocalER10UErrorCode, @function
_ZN6icu_6715MeasureUnitImpl14takeReciprocalER10UErrorCode:
.LFB3141:
	.cfi_startproc
	endbr64
	movl	$0, 152(%rdi)
	movq	96(%rdi), %rax
	movb	$0, (%rax)
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L341
	movq	16(%rdi), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L343:
	movq	(%rcx,%rax,8), %rdx
	addq	$1, %rax
	negl	8(%rdx)
	cmpl	%eax, 8(%rdi)
	jg	.L343
.L341:
	ret
	.cfi_endproc
.LFE3141:
	.size	_ZN6icu_6715MeasureUnitImpl14takeReciprocalER10UErrorCode, .-_ZN6icu_6715MeasureUnitImpl14takeReciprocalER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715MeasureUnitImpl6appendERKNS_14SingleUnitImplER10UErrorCode
	.type	_ZN6icu_6715MeasureUnitImpl6appendERKNS_14SingleUnitImplER10UErrorCode, @function
_ZN6icu_6715MeasureUnitImpl6appendERKNS_14SingleUnitImplER10UErrorCode:
.LFB3142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	96(%rdi), %rax
	movl	$0, 152(%rdi)
	movb	$0, (%rax)
	movl	(%rsi), %esi
	xorl	%eax, %eax
	cmpl	$-1, %esi
	je	.L345
	movq	%rdi, %r12
	movl	8(%rdi), %edi
	movq	%rdx, %r13
	testl	%edi, %edi
	jle	.L347
	movq	16(%r12), %rcx
	leal	-1(%rdi), %eax
	leaq	8(%rcx), %rdx
	leaq	(%rdx,%rax,8), %r8
	xorl	%eax, %eax
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L348:
	je	.L350
	movl	8(%rbx), %r9d
	testl	%r9d, %r9d
	js	.L349
.L350:
	cmpl	(%rcx), %esi
	jne	.L349
	movl	4(%rbx), %r9d
	cmpl	%r9d, 4(%rcx)
	cmove	%rcx, %rax
.L349:
	movq	%rdx, %rcx
	cmpq	%rdx, %r8
	je	.L375
.L377:
	addq	$8, %rdx
.L351:
	movq	(%rcx), %rcx
	movl	8(%rcx), %r11d
	testl	%r11d, %r11d
	jns	.L348
	movl	8(%rbx), %r10d
	testl	%r10d, %r10d
	jle	.L350
	movq	%rdx, %rcx
	cmpq	%rdx, %r8
	jne	.L377
	.p2align 4,,10
	.p2align 3
.L375:
	testq	%rax, %rax
	je	.L347
	movl	8(%rbx), %edx
	addl	%edx, 8(%rax)
.L352:
	testq	%rax, %rax
	sete	%al
.L345:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	movl	24(%r12), %r14d
	cmpl	%r14d, %edi
	jne	.L353
	cmpl	$8, %r14d
	je	.L354
	leal	(%r14,%r14), %ecx
	testl	%ecx, %ecx
	jg	.L378
.L359:
	movl	$7, 0(%r13)
	xorl	%eax, %eax
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L354:
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movl	$32, %ecx
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L359
.L360:
	cmpl	%r14d, 24(%r12)
	movq	16(%r12), %r8
	movq	%r15, %rdi
	movl	%ecx, -60(%rbp)
	cmovle	24(%r12), %r14d
	movq	%r8, %rsi
	movq	%r8, -56(%rbp)
	cmpl	%ecx, %r14d
	movslq	%r14d, %rdx
	cmovg	%rcx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %r8
.L356:
	cmpb	$0, 28(%r12)
	jne	.L379
.L357:
	movq	%r15, 16(%r12)
	movl	%ecx, 24(%r12)
	movb	$1, 28(%r12)
.L353:
	movl	$12, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L358
	movl	$4294967295, %edi
	movl	$1, 8(%rax)
	movq	%rdi, (%rax)
	movslq	8(%r12), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, 8(%r12)
	movq	16(%r12), %rcx
	movq	%rax, (%rcx,%rdx,8)
	movq	(%rbx), %rdx
	movq	%rdx, (%rax)
	movl	8(%rbx), %edx
	movl	%edx, 8(%rax)
	xorl	%eax, %eax
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L378:
	movslq	%ecx, %rdi
	movl	%ecx, -56(%rbp)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L359
	testl	%r14d, %r14d
	movq	16(%r12), %r8
	movslq	-56(%rbp), %rcx
	jle	.L356
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%r8, %rdi
	movl	%ecx, -56(%rbp)
	call	uprv_free_67@PLT
	movl	-56(%rbp), %ecx
	jmp	.L357
.L358:
	movslq	8(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 8(%r12)
	movq	16(%r12), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L359
	.cfi_endproc
.LFE3142:
	.size	_ZN6icu_6715MeasureUnitImpl6appendERKNS_14SingleUnitImplER10UErrorCode, .-_ZN6icu_6715MeasureUnitImpl6appendERKNS_14SingleUnitImplER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNO6icu_6715MeasureUnitImpl5buildER10UErrorCode
	.type	_ZNO6icu_6715MeasureUnitImpl5buildER10UErrorCode, @function
_ZNO6icu_6715MeasureUnitImpl5buildER10UErrorCode:
.LFB3143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZN6icu_6712_GLOBAL__N_19serializeERNS_15MeasureUnitImplER10UErrorCode
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnitC1EONS_15MeasureUnitImplE@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3143:
	.size	_ZNO6icu_6715MeasureUnitImpl5buildER10UErrorCode, .-_ZNO6icu_6715MeasureUnitImpl5buildER10UErrorCode
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC6:
	.string	"-"
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC7:
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC8:
	.string	"-"
	.string	"a"
	.string	"n"
	.string	"d"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC9:
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC10:
	.string	"s"
	.string	"q"
	.string	"u"
	.string	"a"
	.string	"r"
	.string	"e"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC11:
	.string	"c"
	.string	"u"
	.string	"b"
	.string	"i"
	.string	"c"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC12:
	.string	"p"
	.string	"2"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC13:
	.string	"p"
	.string	"3"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC14:
	.string	"p"
	.string	"4"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC15:
	.string	"p"
	.string	"5"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC16:
	.string	"p"
	.string	"6"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC17:
	.string	"p"
	.string	"7"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC18:
	.string	"p"
	.string	"8"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC19:
	.string	"p"
	.string	"9"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC20:
	.string	"p"
	.string	"1"
	.string	"0"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC21:
	.string	"p"
	.string	"1"
	.string	"1"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC22:
	.string	"p"
	.string	"1"
	.string	"2"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC23:
	.string	"p"
	.string	"1"
	.string	"3"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC24:
	.string	"p"
	.string	"1"
	.string	"4"
	.string	"-"
	.string	""
	.string	""
	.align 2
.LC25:
	.string	"p"
	.string	"1"
	.string	"5"
	.string	"-"
	.string	""
	.string	""
	.text
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_114initUnitExtrasER10UErrorCode, @function
_ZN6icu_6712_GLOBAL__N_114initUnitExtrasER10UErrorCode:
.LFB3098:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6712_GLOBAL__N_117cleanupUnitExtrasEv(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	xorl	%edi, %edi
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6717UCharsTrieBuilderC1ER10UErrorCode@PLT
	movl	0(%r13), %edi
	testl	%edi, %edi
	jg	.L387
	leaq	_ZN6icu_6712_GLOBAL__N_1L16gSIPrefixStringsE(%rip), %rbx
	leaq	-240(%rbp), %r12
	leaq	320(%rbx), %r15
	.p2align 4,,10
	.p2align 3
.L384:
	movq	(%rbx), %rsi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r12, %rdi
	addq	$16, %rbx
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movl	-8(%rbx), %eax
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leal	64(%rax), %edx
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpq	%r15, %rbx
	jne	.L384
	movl	0(%r13), %esi
	testl	%esi, %esi
	jle	.L398
.L387:
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilderD1Ev@PLT
.L382:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L399
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$128, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$129, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$130, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$192, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$258, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$259, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$258, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$259, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$260, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$261, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$262, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$263, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$264, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$265, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$266, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$267, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$268, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$269, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$270, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movl	$271, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L387
	leaq	_ZN6icu_6712_GLOBAL__N_1L12gSimpleUnitsE(%rip), %r15
	movl	$512, %ebx
	.p2align 4,,10
	.p2align 3
.L385:
	movq	(%r15), %rsi
	movq	%r12, %rdi
	movl	%ebx, -244(%rbp)
	addl	$1, %ebx
	addq	$8, %r15
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	-244(%rbp), %edx
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	$614, %ebx
	jne	.L385
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdx
	movq	%r13, %rcx
	xorl	%esi, %esi
	movq	%rax, -240(%rbp)
	movq	%r14, %rdi
	movl	$2, %eax
	movw	%ax, -232(%rbp)
	call	_ZN6icu_6717UCharsTrieBuilder18buildUnicodeStringE22UStringTrieBuildOptionRNS_13UnicodeStringER10UErrorCode@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L400
	movzwl	-232(%rbp), %eax
	testw	%ax, %ax
	js	.L388
	movswl	%ax, %r13d
	sarl	$5, %r13d
.L389:
	movslq	%r13d, %r13
	addq	%r13, %r13
	movq	%r13, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, _ZN6icu_6712_GLOBAL__N_129kSerializedUnitExtrasStemTrieE(%rip)
	movq	%rax, %rdi
	movzwl	-232(%rbp), %eax
	testb	$17, %al
	jne	.L393
	leaq	-230(%rbp), %rsi
	testb	$2, %al
	cmove	-216(%rbp), %rsi
.L390:
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6717UCharsTrieBuilderD1Ev@PLT
	jmp	.L382
.L388:
	movl	-228(%rbp), %r13d
	jmp	.L389
.L400:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L387
.L393:
	xorl	%esi, %esi
	jmp	.L390
.L399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3098:
	.size	_ZN6icu_6712_GLOBAL__N_114initUnitExtrasER10UErrorCode, .-_ZN6icu_6712_GLOBAL__N_114initUnitExtrasER10UErrorCode
	.section	.rodata.str1.1
.LC26:
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC27:
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715MeasureUnitImpl23forMeasureUnitMaybeCopyERKNS_11MeasureUnitER10UErrorCode
	.type	_ZN6icu_6715MeasureUnitImpl23forMeasureUnitMaybeCopyERKNS_11MeasureUnitER10UErrorCode, @function
_ZN6icu_6715MeasureUnitImpl23forMeasureUnitMaybeCopyERKNS_11MeasureUnitER10UErrorCode:
.LFB3140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	109(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -160(%rbp)
	movq	8(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L402
	movb	$0, 28(%rdi)
	xorl	%ecx, %ecx
	movl	$8, %r13d
	movl	$0, (%rdi)
	movl	$0, 8(%rdi)
	movl	$0, 152(%rdi)
	movl	$40, 104(%rdi)
	movq	%r15, 16(%rdi)
	movl	$8, 24(%rdi)
	movq	%r14, 96(%rdi)
	xorl	%edi, %edi
	movw	%di, 108(%r12)
	movl	(%rbx), %eax
	movq	$0, -152(%rbp)
	movl	8(%rbx), %r8d
	movl	%eax, (%r12)
	testl	%r8d, %r8d
	jle	.L404
	.p2align 4,,10
	.p2align 3
.L403:
	movq	16(%rbx), %rax
	movq	-152(%rbp), %rsi
	movq	(%rax,%rsi,8), %r15
	cmpl	%r13d, %ecx
	jne	.L405
	cmpl	$8, %r13d
	je	.L406
	leal	(%r13,%r13), %r14d
	testl	%r14d, %r14d
	jg	.L437
.L419:
	movq	-160(%rbp), %rax
	movl	$7, (%rax)
.L404:
	movl	152(%rbx), %edx
	movq	96(%rbx), %rsi
	leaq	96(%r12), %rdi
	movq	-160(%rbp), %rcx
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
.L401:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L438
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L419
	movl	$32, %r14d
.L420:
	cmpl	%r13d, 24(%r12)
	movl	%r13d, %edx
	movq	16(%r12), %r9
	movq	%rcx, %rdi
	cmovle	24(%r12), %edx
	movq	%r9, %rsi
	movq	%r9, -168(%rbp)
	cmpl	%r14d, %edx
	cmovg	%r14d, %edx
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, 28(%r12)
	movq	-168(%rbp), %r9
	movq	%rax, %rcx
	jne	.L439
.L409:
	movq	%rcx, 16(%r12)
	movl	%r14d, 24(%r12)
	movb	$1, 28(%r12)
.L405:
	movl	$12, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L410
	movq	(%r15), %rdx
	movq	16(%r12), %rsi
	addq	$1, -152(%rbp)
	movq	%rdx, (%rax)
	movl	8(%r15), %edx
	movl	%edx, 8(%rax)
	movslq	8(%r12), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, 8(%r12)
	movq	%rax, (%rsi,%rdx,8)
	movq	-152(%rbp), %rax
	cmpl	%eax, 8(%rbx)
	jle	.L404
	movl	24(%r12), %r13d
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L437:
	movslq	%r14d, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L419
	movq	16(%r12), %r9
	testl	%r13d, %r13d
	jg	.L420
	cmpb	$0, 28(%r12)
	je	.L409
.L439:
	movq	%r9, %rdi
	movq	%rcx, -168(%rbp)
	call	uprv_free_67@PLT
	movq	-168(%rbp), %rcx
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L402:
	movq	%rsi, %rdi
	call	_ZNK6icu_6711MeasureUnit13getIdentifierEv@PLT
	leaq	-144(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-160(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jle	.L413
.L417:
	leaq	-120(%rbp), %rdi
	leaq	.LC26(%rip), %rsi
	movl	$0, -128(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	$0, -104(%rbp)
	leaq	.LC27(%rip), %rax
.L435:
	movq	%rax, -96(%rbp)
	movq	%rax, -88(%rbp)
	movl	$-1, -80(%rbp)
	xorl	%eax, %eax
	movq	%r15, 16(%r12)
	leaq	-128(%rbp), %rdi
	movq	%r12, %rsi
	movl	$0, (%r12)
	movq	-160(%rbp), %rdx
	movl	$0, 8(%r12)
	movl	$8, 24(%r12)
	movb	$0, 28(%r12)
	movq	%r14, 96(%r12)
	movl	$0, 152(%r12)
	movl	$40, 104(%r12)
	movw	%ax, 108(%r12)
	movb	$0, -72(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_16Parser9parseImplERNS_15MeasureUnitImplER10UErrorCode
	leaq	-104(%rbp), %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L413:
	movq	-144(%rbp), %rdx
	movl	-136(%rbp), %ebx
	movl	_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L415
	leaq	_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %rdi
	movq	%rdx, -152(%rbp)
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	movq	-152(%rbp), %rdx
	testb	%al, %al
	je	.L415
	movq	-160(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_6712_GLOBAL__N_114initUnitExtrasER10UErrorCode
	movl	0(%r13), %eax
	leaq	_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movq	-152(%rbp), %rdx
.L416:
	movq	-160(%rbp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L417
	movl	$0, -128(%rbp)
	movq	_ZN6icu_6712_GLOBAL__N_129kSerializedUnitExtrasStemTrieE(%rip), %rax
	movq	%rdx, -120(%rbp)
	movl	%ebx, -112(%rbp)
	movq	$0, -104(%rbp)
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L415:
	movl	4+_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L416
	movq	-160(%rbp), %rbx
	movl	%eax, (%rbx)
	jmp	.L417
.L410:
	movslq	8(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 8(%r12)
	movq	16(%r12), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L419
.L438:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3140:
	.size	_ZN6icu_6715MeasureUnitImpl23forMeasureUnitMaybeCopyERKNS_11MeasureUnitER10UErrorCode, .-_ZN6icu_6715MeasureUnitImpl23forMeasureUnitMaybeCopyERKNS_11MeasureUnitER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit10reciprocalER10UErrorCode
	.type	_ZNK6icu_6711MeasureUnit10reciprocalER10UErrorCode, @function
_ZNK6icu_6711MeasureUnit10reciprocalER10UErrorCode:
.LFB3150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-208(%rbp), %r14
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$176, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6715MeasureUnitImpl23forMeasureUnitMaybeCopyERKNS_11MeasureUnitER10UErrorCode
	movq	-112(%rbp), %rax
	movl	$0, -56(%rbp)
	movb	$0, (%rax)
	movl	-200(%rbp), %eax
	movq	-192(%rbp), %rsi
	testl	%eax, %eax
	jle	.L445
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L444:
	movq	(%rsi,%rax,8), %rcx
	addq	$1, %rax
	negl	8(%rcx)
	cmpl	%eax, -200(%rbp)
	jg	.L444
.L445:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712_GLOBAL__N_19serializeERNS_15MeasureUnitImplER10UErrorCode
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnitC1EONS_15MeasureUnitImplE@PLT
	cmpb	$0, -100(%rbp)
	jne	.L457
.L443:
	movl	-200(%rbp), %eax
	movq	-192(%rbp), %r8
	testl	%eax, %eax
	jle	.L446
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L450:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L447
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-200(%rbp), %eax
	addq	$1, %rbx
	movq	-192(%rbp), %r8
	cmpl	%ebx, %eax
	jg	.L450
.L446:
	cmpb	$0, -180(%rbp)
	jne	.L458
.L440:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L459
	addq	$176, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L450
	cmpb	$0, -180(%rbp)
	je	.L440
.L458:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L457:
	movq	-112(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L443
.L459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3150:
	.size	_ZNK6icu_6711MeasureUnit10reciprocalER10UErrorCode, .-_ZNK6icu_6711MeasureUnit10reciprocalER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715MeasureUnitImpl13forIdentifierENS_11StringPieceER10UErrorCode
	.type	_ZN6icu_6715MeasureUnitImpl13forIdentifierENS_11StringPieceER10UErrorCode, @function
_ZN6icu_6715MeasureUnitImpl13forIdentifierENS_11StringPieceER10UErrorCode:
.LFB3136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$96, %rsp
	movl	(%rcx), %edi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jle	.L461
.L465:
	leaq	-88(%rbp), %rdi
	leaq	.LC26(%rip), %rsi
	movl	$0, -96(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	$0, -72(%rbp)
	leaq	.LC27(%rip), %rax
.L475:
	movq	%rax, -64(%rbp)
	movq	%rax, -56(%rbp)
	movl	$-1, -48(%rbp)
	leaq	32(%r12), %rax
	leaq	-96(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, 16(%r12)
	leaq	109(%r12), %rax
	movq	%rax, 96(%r12)
	xorl	%eax, %eax
	movw	%ax, 108(%r12)
	movl	$0, (%r12)
	movl	$0, 8(%r12)
	movl	$8, 24(%r12)
	movb	$0, 28(%r12)
	movl	$0, 152(%r12)
	movl	$40, 104(%r12)
	movb	$0, -40(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_16Parser9parseImplERNS_15MeasureUnitImplER10UErrorCode
	leaq	-72(%rbp), %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L476
	addq	$96, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movl	_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L463
	leaq	_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %rdi
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %rdx
	testb	%al, %al
	jne	.L477
.L463:
	movl	4+_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L464
	movl	%eax, 0(%r13)
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L477:
	movq	%r13, %rdi
	call	_ZN6icu_6712_GLOBAL__N_114initUnitExtrasER10UErrorCode
	movl	0(%r13), %eax
	leaq	_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %rdx
.L464:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L465
	movl	$0, -96(%rbp)
	movq	_ZN6icu_6712_GLOBAL__N_129kSerializedUnitExtrasStemTrieE(%rip), %rax
	movq	%rsi, -88(%rbp)
	movl	%edx, -80(%rbp)
	movq	$0, -72(%rbp)
	jmp	.L475
.L476:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3136:
	.size	_ZN6icu_6715MeasureUnitImpl13forIdentifierENS_11StringPieceER10UErrorCode, .-_ZN6icu_6715MeasureUnitImpl13forIdentifierENS_11StringPieceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MeasureUnit13forIdentifierENS_11StringPieceER10UErrorCode
	.type	_ZN6icu_6711MeasureUnit13forIdentifierENS_11StringPieceER10UErrorCode, @function
_ZN6icu_6711MeasureUnit13forIdentifierENS_11StringPieceER10UErrorCode:
.LFB3144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	subq	$256, %rsp
	.cfi_offset 3, -48
	movl	(%rcx), %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jle	.L479
.L483:
	leaq	-264(%rbp), %rdi
	leaq	.LC26(%rip), %rsi
	movl	$0, -272(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	leaq	.LC27(%rip), %rax
	movq	$0, -248(%rbp)
.L500:
	movq	%rax, -240(%rbp)
	movq	%rax, -232(%rbp)
	movl	$-1, -224(%rbp)
	leaq	-176(%rbp), %rax
	leaq	-208(%rbp), %r14
	movq	%r12, %rdx
	movb	$0, -216(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-99(%rbp), %rax
	movq	%r14, %rsi
	leaq	-272(%rbp), %rdi
	movq	%rax, -112(%rbp)
	xorl	%eax, %eax
	movw	%ax, -100(%rbp)
	movl	$0, -208(%rbp)
	movl	$0, -200(%rbp)
	movl	$8, -184(%rbp)
	movb	$0, -180(%rbp)
	movl	$0, -56(%rbp)
	movl	$40, -104(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_16Parser9parseImplERNS_15MeasureUnitImplER10UErrorCode
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712_GLOBAL__N_19serializeERNS_15MeasureUnitImplER10UErrorCode
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711MeasureUnitC1EONS_15MeasureUnitImplE@PLT
	cmpb	$0, -100(%rbp)
	jne	.L501
.L485:
	movl	-200(%rbp), %eax
	movq	-192(%rbp), %rdi
	testl	%eax, %eax
	jle	.L486
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L490:
	movq	(%rdi,%rbx,8), %r8
	testq	%r8, %r8
	je	.L487
	movq	%r8, %rdi
	addq	$1, %rbx
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-200(%rbp), %eax
	movq	-192(%rbp), %rdi
	cmpl	%ebx, %eax
	jg	.L490
.L486:
	cmpb	$0, -180(%rbp)
	jne	.L502
.L491:
	leaq	-248(%rbp), %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L503
	addq	$256, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L490
	cmpb	$0, -180(%rbp)
	je	.L491
.L502:
	call	uprv_free_67@PLT
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L501:
	movq	-112(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L479:
	movl	_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L481
	leaq	_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %rdi
	movq	%rdx, -288(%rbp)
	movq	%rsi, -280(%rbp)
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	movq	-280(%rbp), %rsi
	movq	-288(%rbp), %rdx
	testb	%al, %al
	jne	.L504
.L481:
	movl	4+_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L482
	movl	%eax, (%r12)
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L504:
	movq	%r12, %rdi
	call	_ZN6icu_6712_GLOBAL__N_114initUnitExtrasER10UErrorCode
	movl	(%r12), %eax
	leaq	_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movq	-280(%rbp), %rsi
	movq	-288(%rbp), %rdx
.L482:
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L483
	movq	%rsi, -264(%rbp)
	movq	_ZN6icu_6712_GLOBAL__N_129kSerializedUnitExtrasStemTrieE(%rip), %rax
	movl	$0, -272(%rbp)
	movl	%edx, -256(%rbp)
	movq	$0, -248(%rbp)
	jmp	.L500
.L503:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3144:
	.size	_ZN6icu_6711MeasureUnit13forIdentifierENS_11StringPieceER10UErrorCode, .-_ZN6icu_6711MeasureUnit13forIdentifierENS_11StringPieceER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6715MeasureUnitImpl14forMeasureUnitERKNS_11MeasureUnitERS0_R10UErrorCode.part.0, @function
_ZN6icu_6715MeasureUnitImpl14forMeasureUnitERKNS_11MeasureUnitERS0_R10UErrorCode.part.0:
.LFB3932:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$256, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6711MeasureUnit13getIdentifierEv@PLT
	leaq	-288(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L506
.L510:
	leaq	-264(%rbp), %rdi
	leaq	.LC26(%rip), %rsi
	movl	$0, -272(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	leaq	.LC27(%rip), %rax
	movq	$0, -248(%rbp)
.L530:
	movq	%rax, -240(%rbp)
	movq	%rax, -232(%rbp)
	movl	$-1, -224(%rbp)
	leaq	-99(%rbp), %rax
	leaq	-208(%rbp), %rsi
	movq	%r13, %rdx
	movb	$0, -216(%rbp)
	movq	%rax, -112(%rbp)
	xorl	%eax, %eax
	leaq	-176(%rbp), %r14
	leaq	-272(%rbp), %rdi
	movw	%ax, -100(%rbp)
	movl	$0, -208(%rbp)
	movl	$0, -200(%rbp)
	movq	%r14, -192(%rbp)
	movl	$8, -184(%rbp)
	movb	$0, -180(%rbp)
	movl	$0, -56(%rbp)
	movl	$40, -104(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_16Parser9parseImplERNS_15MeasureUnitImplER10UErrorCode
	movl	-208(%rbp), %eax
	cmpb	$0, 28(%r12)
	movl	%eax, (%r12)
	movl	-200(%rbp), %eax
	movl	%eax, 8(%r12)
	jne	.L531
.L512:
	movzbl	-180(%rbp), %eax
	movslq	-184(%rbp), %rdx
	movb	%al, 28(%r12)
	movq	-192(%rbp), %rax
	movl	%edx, 24(%r12)
	cmpq	%r14, %rax
	je	.L532
	movq	%rax, 16(%r12)
	movq	%r14, -192(%rbp)
	movl	$8, -184(%rbp)
	movb	$0, -180(%rbp)
.L514:
	leaq	-112(%rbp), %rsi
	leaq	96(%r12), %rdi
	movl	$0, -200(%rbp)
	call	_ZN6icu_6710CharStringaSEOS0_@PLT
	cmpb	$0, -100(%rbp)
	jne	.L533
.L515:
	movl	-200(%rbp), %eax
	movq	-192(%rbp), %rdi
	testl	%eax, %eax
	jle	.L516
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L520:
	movq	(%rdi,%rbx,8), %r8
	testq	%r8, %r8
	je	.L517
	movq	%r8, %rdi
	addq	$1, %rbx
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-200(%rbp), %eax
	movq	-192(%rbp), %rdi
	cmpl	%ebx, %eax
	jg	.L520
.L516:
	cmpb	$0, -180(%rbp)
	jne	.L534
.L521:
	leaq	-248(%rbp), %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L535
	addq	$256, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L520
	cmpb	$0, -180(%rbp)
	je	.L521
.L534:
	call	uprv_free_67@PLT
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L531:
	movq	16(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L533:
	movq	-112(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L532:
	leaq	32(%r12), %rdi
	salq	$3, %rdx
	movq	%r14, %rsi
	movq	%rdi, 16(%r12)
	call	memcpy@PLT
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L506:
	movq	-288(%rbp), %r14
	movl	-280(%rbp), %ebx
	movl	_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L508
	leaq	_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	jne	.L536
.L508:
	movl	4+_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L509
	movl	%eax, 0(%r13)
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L536:
	movq	%r13, %rdi
	call	_ZN6icu_6712_GLOBAL__N_114initUnitExtrasER10UErrorCode
	movl	0(%r13), %eax
	leaq	_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L509:
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L510
	movq	%r14, -264(%rbp)
	movq	_ZN6icu_6712_GLOBAL__N_129kSerializedUnitExtrasStemTrieE(%rip), %rax
	movl	$0, -272(%rbp)
	movl	%ebx, -256(%rbp)
	movq	$0, -248(%rbp)
	jmp	.L530
.L535:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3932:
	.size	_ZN6icu_6715MeasureUnitImpl14forMeasureUnitERKNS_11MeasureUnitERS0_R10UErrorCode.part.0, .-_ZN6icu_6715MeasureUnitImpl14forMeasureUnitERKNS_11MeasureUnitERS0_R10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715MeasureUnitImpl14forMeasureUnitERKNS_11MeasureUnitERS0_R10UErrorCode
	.type	_ZN6icu_6715MeasureUnitImpl14forMeasureUnitERKNS_11MeasureUnitERS0_R10UErrorCode, @function
_ZN6icu_6715MeasureUnitImpl14forMeasureUnitERKNS_11MeasureUnitERS0_R10UErrorCode:
.LFB3137:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L539
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	jmp	_ZN6icu_6715MeasureUnitImpl14forMeasureUnitERKNS_11MeasureUnitERS0_R10UErrorCode.part.0
	.cfi_endproc
.LFE3137:
	.size	_ZN6icu_6715MeasureUnitImpl14forMeasureUnitERKNS_11MeasureUnitERS0_R10UErrorCode, .-_ZN6icu_6715MeasureUnitImpl14forMeasureUnitERKNS_11MeasureUnitERS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SingleUnitImpl14forMeasureUnitERKNS_11MeasureUnitER10UErrorCode
	.type	_ZN6icu_6714SingleUnitImpl14forMeasureUnitERKNS_11MeasureUnitER10UErrorCode, @function
_ZN6icu_6714SingleUnitImpl14forMeasureUnitERKNS_11MeasureUnitER10UErrorCode:
.LFB3133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	-160(%rbp), %rax
	movb	$0, -164(%rbp)
	movq	%rax, -176(%rbp)
	leaq	-83(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	8(%rdi), %rax
	movl	$0, -192(%rbp)
	movl	$0, -184(%rbp)
	movl	$8, -168(%rbp)
	movl	$0, -40(%rbp)
	movl	$40, -88(%rbp)
	movw	%si, -84(%rbp)
	testq	%rax, %rax
	je	.L564
.L541:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L542
.L563:
	movl	$1, -196(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -204(%rbp)
.L543:
	testb	%dl, %dl
	jne	.L565
.L546:
	movl	-184(%rbp), %eax
	movq	-176(%rbp), %rdi
	testl	%eax, %eax
	jle	.L547
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L551:
	movq	(%rdi,%rbx,8), %r8
	testq	%r8, %r8
	je	.L548
	movq	%r8, %rdi
	addq	$1, %rbx
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-184(%rbp), %eax
	movq	-176(%rbp), %rdi
	cmpl	%ebx, %eax
	jg	.L551
.L547:
	cmpb	$0, -164(%rbp)
	jne	.L566
.L552:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	-204(%rbp), %rax
	movl	-196(%rbp), %edx
	jne	.L567
	addq	$216, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L548:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L551
	cmpb	$0, -164(%rbp)
	je	.L552
.L566:
	call	uprv_free_67@PLT
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L542:
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.L563
	cmpl	$1, %ecx
	je	.L568
	movl	$1, (%rbx)
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L565:
	movq	-96(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L568:
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rcx
	movl	8(%rax), %eax
	movq	%rcx, -204(%rbp)
	movl	%eax, -196(%rbp)
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L564:
	movq	%rbx, %rdx
	leaq	-192(%rbp), %rsi
	call	_ZN6icu_6715MeasureUnitImpl14forMeasureUnitERKNS_11MeasureUnitERS0_R10UErrorCode.part.0
	movzbl	-84(%rbp), %edx
	jmp	.L541
.L567:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3133:
	.size	_ZN6icu_6714SingleUnitImpl14forMeasureUnitERKNS_11MeasureUnitER10UErrorCode, .-_ZN6icu_6714SingleUnitImpl14forMeasureUnitERKNS_11MeasureUnitER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit11getSIPrefixER10UErrorCode
	.type	_ZNK6icu_6711MeasureUnit11getSIPrefixER10UErrorCode, @function
_ZNK6icu_6711MeasureUnit11getSIPrefixER10UErrorCode:
.LFB3146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	_ZN6icu_6714SingleUnitImpl14forMeasureUnitERKNS_11MeasureUnitER10UErrorCode
	leave
	.cfi_def_cfa 7, 8
	shrq	$32, %rax
	ret
	.cfi_endproc
.LFE3146:
	.size	_ZNK6icu_6711MeasureUnit11getSIPrefixER10UErrorCode, .-_ZNK6icu_6711MeasureUnit11getSIPrefixER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit12withSIPrefixENS_16UMeasureSIPrefixER10UErrorCode
	.type	_ZNK6icu_6711MeasureUnit12withSIPrefixENS_16UMeasureSIPrefixER10UErrorCode, @function
_ZNK6icu_6711MeasureUnit12withSIPrefixENS_16UMeasureSIPrefixER10UErrorCode:
.LFB3147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	movq	%rcx, %rsi
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6714SingleUnitImpl14forMeasureUnitERKNS_11MeasureUnitER10UErrorCode
	leaq	-52(%rbp), %rsi
	movq	%r13, %rdi
	movl	%edx, -44(%rbp)
	movq	%r12, %rdx
	movq	%rax, -52(%rbp)
	movl	%ebx, -48(%rbp)
	call	_ZNK6icu_6714SingleUnitImpl5buildER10UErrorCode
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L574
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L574:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3147:
	.size	_ZNK6icu_6711MeasureUnit12withSIPrefixENS_16UMeasureSIPrefixER10UErrorCode, .-_ZNK6icu_6711MeasureUnit12withSIPrefixENS_16UMeasureSIPrefixER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit17getDimensionalityER10UErrorCode
	.type	_ZNK6icu_6711MeasureUnit17getDimensionalityER10UErrorCode, @function
_ZNK6icu_6711MeasureUnit17getDimensionalityER10UErrorCode:
.LFB3148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	_ZN6icu_6714SingleUnitImpl14forMeasureUnitERKNS_11MeasureUnitER10UErrorCode
	movl	(%rbx), %ecx
	movq	%rax, -28(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L575
	cmpl	$-1, -28(%rbp)
	cmovne	%edx, %eax
.L575:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3148:
	.size	_ZNK6icu_6711MeasureUnit17getDimensionalityER10UErrorCode, .-_ZNK6icu_6711MeasureUnit17getDimensionalityER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit18withDimensionalityEiR10UErrorCode
	.type	_ZNK6icu_6711MeasureUnit18withDimensionalityEiR10UErrorCode, @function
_ZNK6icu_6711MeasureUnit18withDimensionalityEiR10UErrorCode:
.LFB3149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	movq	%rcx, %rsi
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6714SingleUnitImpl14forMeasureUnitERKNS_11MeasureUnitER10UErrorCode
	leaq	-52(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	%ebx, -44(%rbp)
	movq	%rax, -52(%rbp)
	call	_ZNK6icu_6714SingleUnitImpl5buildER10UErrorCode
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L583
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L583:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3149:
	.size	_ZNK6icu_6711MeasureUnit18withDimensionalityEiR10UErrorCode, .-_ZNK6icu_6711MeasureUnit18withDimensionalityEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit13getComplexityER10UErrorCode
	.type	_ZNK6icu_6711MeasureUnit13getComplexityER10UErrorCode, @function
_ZNK6icu_6711MeasureUnit13getComplexityER10UErrorCode:
.LFB3145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$176, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	-160(%rbp), %rax
	movb	$0, -164(%rbp)
	movq	%rax, -176(%rbp)
	leaq	-83(%rbp), %rax
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	movw	%ax, -84(%rbp)
	movq	8(%rdi), %rax
	movl	$0, -192(%rbp)
	movl	$0, -184(%rbp)
	movl	$8, -168(%rbp)
	movl	$0, -40(%rbp)
	movl	$40, -88(%rbp)
	testq	%rax, %rax
	je	.L597
	movl	(%rax), %r12d
.L584:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L598
	addq	$176, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	.cfi_restore_state
	leaq	-192(%rbp), %r8
	movq	%rsi, %rdx
	movq	%r8, %rsi
	call	_ZN6icu_6715MeasureUnitImpl14forMeasureUnitERKNS_11MeasureUnitERS0_R10UErrorCode.part.0
	cmpb	$0, -84(%rbp)
	movl	(%rax), %r12d
	je	.L596
	movq	-96(%rbp), %rdi
	call	uprv_free_67@PLT
.L596:
	movl	-184(%rbp), %eax
	movq	-176(%rbp), %r8
	testl	%eax, %eax
	jle	.L588
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L592:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L589
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-184(%rbp), %eax
	addq	$1, %rbx
	movq	-176(%rbp), %r8
	cmpl	%ebx, %eax
	jg	.L592
.L588:
	cmpb	$0, -164(%rbp)
	je	.L584
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L589:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L592
	jmp	.L588
.L598:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3145:
	.size	_ZNK6icu_6711MeasureUnit13getComplexityER10UErrorCode, .-_ZNK6icu_6711MeasureUnit13getComplexityER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit7productERKS0_R10UErrorCode
	.type	_ZNK6icu_6711MeasureUnit7productERKS0_R10UErrorCode, @function
_ZNK6icu_6711MeasureUnit7productERKS0_R10UErrorCode:
.LFB3151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-384(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movq	%rcx, %rdx
	subq	$344, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6715MeasureUnitImpl23forMeasureUnitMaybeCopyERKNS_11MeasureUnitER10UErrorCode
	leaq	-192(%rbp), %rax
	movq	8(%rbx), %r13
	xorl	%edx, %edx
	movq	%rax, -208(%rbp)
	leaq	-115(%rbp), %rax
	movl	$0, -224(%rbp)
	movl	$0, -216(%rbp)
	movl	$8, -200(%rbp)
	movb	$0, -196(%rbp)
	movq	%rax, -128(%rbp)
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	movw	%dx, -116(%rbp)
	testq	%r13, %r13
	je	.L627
.L600:
	cmpl	$2, -384(%rbp)
	je	.L601
	cmpl	$2, 0(%r13)
	je	.L601
	movl	8(%r13), %eax
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.L607
	.p2align 4,,10
	.p2align 3
.L604:
	movq	16(%r13), %rax
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	(%rax,%rbx,8), %rsi
	movq	-288(%rbp), %rax
	addq	$1, %rbx
	movl	$0, -232(%rbp)
	movb	$0, (%rax)
	call	_ZN6icu_6712_GLOBAL__N_110appendImplERNS_15MeasureUnitImplERKNS_14SingleUnitImplER10UErrorCode
	cmpl	%ebx, 8(%r13)
	jg	.L604
.L607:
	cmpl	$1, -376(%rbp)
	jle	.L606
	movl	$1, -384(%rbp)
.L606:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6712_GLOBAL__N_19serializeERNS_15MeasureUnitImplER10UErrorCode
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnitC1EONS_15MeasureUnitImplE@PLT
	cmpb	$0, -116(%rbp)
	jne	.L628
.L608:
	movl	-216(%rbp), %eax
	movq	-208(%rbp), %r8
	testl	%eax, %eax
	jle	.L609
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L613:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L610
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-216(%rbp), %eax
	addq	$1, %rbx
	movq	-208(%rbp), %r8
	cmpl	%ebx, %eax
	jg	.L613
.L609:
	cmpb	$0, -196(%rbp)
	jne	.L629
.L614:
	cmpb	$0, -276(%rbp)
	jne	.L630
.L615:
	movl	-376(%rbp), %eax
	movq	-368(%rbp), %r8
	testl	%eax, %eax
	jle	.L616
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L620:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L617
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-376(%rbp), %eax
	addq	$1, %rbx
	movq	-368(%rbp), %r8
	cmpl	%ebx, %eax
	jg	.L620
.L616:
	cmpb	$0, -356(%rbp)
	jne	.L631
.L599:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L632
	addq	$344, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L613
	cmpb	$0, -196(%rbp)
	je	.L614
.L629:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -276(%rbp)
	je	.L615
.L630:
	movq	-288(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L617:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L620
	cmpb	$0, -356(%rbp)
	je	.L599
.L631:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L601:
	movl	$1, (%r14)
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	cmpb	$0, -116(%rbp)
	je	.L608
.L628:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L627:
	leaq	-224(%rbp), %rsi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_6715MeasureUnitImpl14forMeasureUnitERKNS_11MeasureUnitERS0_R10UErrorCode.part.0
	movq	%rax, %r13
	jmp	.L600
.L632:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3151:
	.size	_ZNK6icu_6711MeasureUnit7productERKS0_R10UErrorCode, .-_ZNK6icu_6711MeasureUnit7productERKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711MeasureUnit18splitToSingleUnitsERiR10UErrorCode
	.type	_ZNK6icu_6711MeasureUnit18splitToSingleUnitsERiR10UErrorCode, @function
_ZNK6icu_6711MeasureUnit18splitToSingleUnitsERiR10UErrorCode:
.LFB3152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	xorl	%ecx, %ecx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -280(%rbp)
	movq	8(%rsi), %r15
	movq	%rsi, %rdi
	movq	%rdx, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-192(%rbp), %rax
	movb	$0, -196(%rbp)
	movq	%rax, -208(%rbp)
	leaq	-115(%rbp), %rax
	movl	$0, -224(%rbp)
	movl	$0, -216(%rbp)
	movl	$8, -200(%rbp)
	movq	%rax, -128(%rbp)
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	movw	%cx, -116(%rbp)
	testq	%r15, %r15
	je	.L662
.L634:
	movq	-264(%rbp), %rax
	movslq	8(%r15), %rbx
	movq	$-1, %rdi
	movl	%ebx, (%rax)
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rbx
	ja	.L635
	leaq	(%rbx,%rbx,2), %rax
	leaq	8(,%rax,8), %rdi
.L635:
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, -272(%rbp)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L636
	addq	$8, %r12
	movq	%rbx, (%rax)
	movq	%r12, -272(%rbp)
	subq	$1, %rbx
	js	.L637
	.p2align 4,,10
	.p2align 3
.L638:
	movq	%r12, %rdi
	subq	$1, %rbx
	addq	$24, %r12
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	cmpq	$-1, %rbx
	jne	.L638
.L637:
	movq	-264(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jle	.L641
.L639:
	movq	-272(%rbp), %r12
	xorl	%r13d, %r13d
	leaq	-256(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L642:
	movq	16(%r15), %rax
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	(%rax,%r13,8), %rsi
	addq	$1, %r13
	call	_ZNK6icu_6714SingleUnitImpl5buildER10UErrorCode
	movq	%r12, %rdi
	movq	%rbx, %rsi
	addq	$24, %r12
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	-264(%rbp), %rax
	cmpl	%r13d, (%rax)
	jg	.L642
	movq	-280(%rbp), %rcx
	movq	-272(%rbp), %rax
	movq	%rax, (%rcx)
	testq	%rax, %rax
	je	.L640
.L643:
	cmpb	$0, -116(%rbp)
	jne	.L663
.L645:
	movl	-216(%rbp), %eax
	movq	-208(%rbp), %r8
	testl	%eax, %eax
	jle	.L646
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L650:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L647
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-216(%rbp), %eax
	addq	$1, %rbx
	movq	-208(%rbp), %r8
	cmpl	%ebx, %eax
	jg	.L650
.L646:
	cmpb	$0, -196(%rbp)
	jne	.L664
.L633:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L665
	movq	-280(%rbp), %rax
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L650
	cmpb	$0, -196(%rbp)
	je	.L633
.L664:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L663:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L645
.L636:
	movq	-264(%rbp), %rax
	cmpl	$0, (%rax)
	jg	.L639
	movq	-280(%rbp), %rax
	movq	$0, (%rax)
	.p2align 4,,10
	.p2align 3
.L640:
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L643
	movl	$7, (%r14)
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L662:
	leaq	-224(%rbp), %rsi
	movq	%r14, %rdx
	call	_ZN6icu_6715MeasureUnitImpl14forMeasureUnitERKNS_11MeasureUnitERS0_R10UErrorCode.part.0
	movq	%rax, %r15
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L641:
	movq	-280(%rbp), %rax
	movq	-272(%rbp), %rcx
	movq	%rcx, (%rax)
	jmp	.L643
.L665:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3152:
	.size	_ZNK6icu_6711MeasureUnit18splitToSingleUnitsERiR10UErrorCode, .-_ZNK6icu_6711MeasureUnit18splitToSingleUnitsERiR10UErrorCode
	.local	_ZN6icu_6712_GLOBAL__N_129kSerializedUnitExtrasStemTrieE
	.comm	_ZN6icu_6712_GLOBAL__N_129kSerializedUnitExtrasStemTrieE,8,8
	.local	_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE
	.comm	_ZN6icu_6712_GLOBAL__N_119gUnitExtrasInitOnceE,8,8
	.section	.rodata.str2.2
	.align 2
.LC28:
	.string	"c"
	.string	"a"
	.string	"n"
	.string	"d"
	.string	"e"
	.string	"l"
	.string	"a"
	.string	""
	.string	""
	.align 2
.LC29:
	.string	"c"
	.string	"a"
	.string	"r"
	.string	"a"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC30:
	.string	"g"
	.string	"r"
	.string	"a"
	.string	"m"
	.string	""
	.string	""
	.align 2
.LC31:
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"c"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC32:
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"c"
	.string	"e"
	.string	"-"
	.string	"t"
	.string	"r"
	.string	"o"
	.string	"y"
	.string	""
	.string	""
	.align 2
.LC33:
	.string	"p"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"d"
	.string	""
	.string	""
	.align 2
.LC34:
	.string	"k"
	.string	"i"
	.string	"l"
	.string	"o"
	.string	"g"
	.string	"r"
	.string	"a"
	.string	"m"
	.string	""
	.string	""
	.align 2
.LC35:
	.string	"s"
	.string	"t"
	.string	"o"
	.string	"n"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC36:
	.string	"t"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC37:
	.string	"m"
	.string	"e"
	.string	"t"
	.string	"r"
	.string	"i"
	.string	"c"
	.string	"-"
	.string	"t"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC38:
	.string	"e"
	.string	"a"
	.string	"r"
	.string	"t"
	.string	"h"
	.string	"-"
	.string	"m"
	.string	"a"
	.string	"s"
	.string	"s"
	.string	""
	.string	""
	.align 2
.LC39:
	.string	"s"
	.string	"o"
	.string	"l"
	.string	"a"
	.string	"r"
	.string	"-"
	.string	"m"
	.string	"a"
	.string	"s"
	.string	"s"
	.string	""
	.string	""
	.align 2
.LC40:
	.string	"p"
	.string	"o"
	.string	"i"
	.string	"n"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC41:
	.string	"i"
	.string	"n"
	.string	"c"
	.string	"h"
	.string	""
	.string	""
	.align 2
.LC42:
	.string	"f"
	.string	"o"
	.string	"o"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC43:
	.string	"y"
	.string	"a"
	.string	"r"
	.string	"d"
	.string	""
	.string	""
	.align 2
.LC44:
	.string	"m"
	.string	"e"
	.string	"t"
	.string	"e"
	.string	"r"
	.string	""
	.string	""
	.align 2
.LC45:
	.string	"f"
	.string	"a"
	.string	"t"
	.string	"h"
	.string	"o"
	.string	"m"
	.string	""
	.string	""
	.align 2
.LC46:
	.string	"f"
	.string	"u"
	.string	"r"
	.string	"l"
	.string	"o"
	.string	"n"
	.string	"g"
	.string	""
	.string	""
	.align 2
.LC47:
	.string	"m"
	.string	"i"
	.string	"l"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC48:
	.string	"n"
	.string	"a"
	.string	"u"
	.string	"t"
	.string	"i"
	.string	"c"
	.string	"a"
	.string	"l"
	.string	"-"
	.string	"m"
	.string	"i"
	.string	"l"
	.string	"e"
	.string	""
	.string	""
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC49:
	.string	"m"
	.string	"i"
	.string	"l"
	.string	"e"
	.string	"-"
	.string	"s"
	.string	"c"
	.string	"a"
	.string	"n"
	.string	"d"
	.string	"i"
	.string	"n"
	.string	"a"
	.string	"v"
	.string	"i"
	.string	"a"
	.string	"n"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC50:
	.string	"1"
	.string	"0"
	.string	"0"
	.string	"-"
	.string	"k"
	.string	"i"
	.string	"l"
	.string	"o"
	.string	"m"
	.string	"e"
	.string	"t"
	.string	"e"
	.string	"r"
	.string	""
	.string	""
	.align 2
.LC51:
	.string	"e"
	.string	"a"
	.string	"r"
	.string	"t"
	.string	"h"
	.string	"-"
	.string	"r"
	.string	"a"
	.string	"d"
	.string	"i"
	.string	"u"
	.string	"s"
	.string	""
	.string	""
	.align 2
.LC52:
	.string	"s"
	.string	"o"
	.string	"l"
	.string	"a"
	.string	"r"
	.string	"-"
	.string	"r"
	.string	"a"
	.string	"d"
	.string	"i"
	.string	"u"
	.string	"s"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC53:
	.string	"a"
	.string	"s"
	.string	"t"
	.string	"r"
	.string	"o"
	.string	"n"
	.string	"o"
	.string	"m"
	.string	"i"
	.string	"c"
	.string	"a"
	.string	"l"
	.string	"-"
	.string	"u"
	.string	"n"
	.string	"i"
	.string	"t"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC54:
	.string	"l"
	.string	"i"
	.string	"g"
	.string	"h"
	.string	"t"
	.string	"-"
	.string	"y"
	.string	"e"
	.string	"a"
	.string	"r"
	.string	""
	.string	""
	.align 2
.LC55:
	.string	"p"
	.string	"a"
	.string	"r"
	.string	"s"
	.string	"e"
	.string	"c"
	.string	""
	.string	""
	.align 2
.LC56:
	.string	"s"
	.string	"e"
	.string	"c"
	.string	"o"
	.string	"n"
	.string	"d"
	.string	""
	.string	""
	.align 2
.LC57:
	.string	"m"
	.string	"i"
	.string	"n"
	.string	"u"
	.string	"t"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC58:
	.string	"h"
	.string	"o"
	.string	"u"
	.string	"r"
	.string	""
	.string	""
	.align 2
.LC59:
	.string	"d"
	.string	"a"
	.string	"y"
	.string	""
	.string	""
	.align 2
.LC60:
	.string	"d"
	.string	"a"
	.string	"y"
	.string	"-"
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"s"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC61:
	.string	"w"
	.string	"e"
	.string	"e"
	.string	"k"
	.string	""
	.string	""
	.align 2
.LC62:
	.string	"w"
	.string	"e"
	.string	"e"
	.string	"k"
	.string	"-"
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"s"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC63:
	.string	"m"
	.string	"o"
	.string	"n"
	.string	"t"
	.string	"h"
	.string	""
	.string	""
	.align 2
.LC64:
	.string	"m"
	.string	"o"
	.string	"n"
	.string	"t"
	.string	"h"
	.string	"-"
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"s"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC65:
	.string	"y"
	.string	"e"
	.string	"a"
	.string	"r"
	.string	""
	.string	""
	.align 2
.LC66:
	.string	"y"
	.string	"e"
	.string	"a"
	.string	"r"
	.string	"-"
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"s"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC67:
	.string	"d"
	.string	"e"
	.string	"c"
	.string	"a"
	.string	"d"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC68:
	.string	"c"
	.string	"e"
	.string	"n"
	.string	"t"
	.string	"u"
	.string	"r"
	.string	"y"
	.string	""
	.string	""
	.align 2
.LC69:
	.string	"a"
	.string	"m"
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC70:
	.string	"f"
	.string	"a"
	.string	"h"
	.string	"r"
	.string	"e"
	.string	"n"
	.string	"h"
	.string	"e"
	.string	"i"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC71:
	.string	"k"
	.string	"e"
	.string	"l"
	.string	"v"
	.string	"i"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC72:
	.string	"c"
	.string	"e"
	.string	"l"
	.string	"s"
	.string	"i"
	.string	"u"
	.string	"s"
	.string	""
	.string	""
	.align 2
.LC73:
	.string	"a"
	.string	"r"
	.string	"c"
	.string	"-"
	.string	"s"
	.string	"e"
	.string	"c"
	.string	"o"
	.string	"n"
	.string	"d"
	.string	""
	.string	""
	.align 2
.LC74:
	.string	"a"
	.string	"r"
	.string	"c"
	.string	"-"
	.string	"m"
	.string	"i"
	.string	"n"
	.string	"u"
	.string	"t"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC75:
	.string	"d"
	.string	"e"
	.string	"g"
	.string	"r"
	.string	"e"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC76:
	.string	"r"
	.string	"a"
	.string	"d"
	.string	"i"
	.string	"a"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC77:
	.string	"r"
	.string	"e"
	.string	"v"
	.string	"o"
	.string	"l"
	.string	"u"
	.string	"t"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC78:
	.string	"i"
	.string	"t"
	.string	"e"
	.string	"m"
	.string	""
	.string	""
	.align 2
.LC79:
	.string	"m"
	.string	"o"
	.string	"l"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC80:
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"m"
	.string	"i"
	.string	"l"
	.string	"l"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC81:
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"m"
	.string	"y"
	.string	"r"
	.string	"i"
	.string	"a"
	.string	"d"
	.string	""
	.string	""
	.align 2
.LC82:
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"m"
	.string	"i"
	.string	"l"
	.string	"l"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC83:
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"c"
	.string	"e"
	.string	"n"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC84:
	.string	"k"
	.string	"a"
	.string	"r"
	.string	"a"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC85:
	.string	"p"
	.string	"o"
	.string	"r"
	.string	"t"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC86:
	.string	"b"
	.string	"i"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC87:
	.string	"b"
	.string	"y"
	.string	"t"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC88:
	.string	"d"
	.string	"o"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC89:
	.string	"p"
	.string	"i"
	.string	"x"
	.string	"e"
	.string	"l"
	.string	""
	.string	""
	.align 2
.LC90:
	.string	"e"
	.string	"m"
	.string	""
	.string	""
	.align 2
.LC91:
	.string	"h"
	.string	"e"
	.string	"r"
	.string	"t"
	.string	"z"
	.string	""
	.string	""
	.align 2
.LC92:
	.string	"n"
	.string	"e"
	.string	"w"
	.string	"t"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC93:
	.string	"p"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"d"
	.string	"-"
	.string	"f"
	.string	"o"
	.string	"r"
	.string	"c"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC94:
	.string	"p"
	.string	"a"
	.string	"s"
	.string	"c"
	.string	"a"
	.string	"l"
	.string	""
	.string	""
	.align 2
.LC95:
	.string	"b"
	.string	"a"
	.string	"r"
	.string	""
	.string	""
	.align 2
.LC96:
	.string	"a"
	.string	"t"
	.string	"m"
	.string	"o"
	.string	"s"
	.string	"p"
	.string	"h"
	.string	"e"
	.string	"r"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC97:
	.string	"o"
	.string	"f"
	.string	"h"
	.string	"g"
	.string	""
	.string	""
	.align 2
.LC98:
	.string	"e"
	.string	"l"
	.string	"e"
	.string	"c"
	.string	"t"
	.string	"r"
	.string	"o"
	.string	"n"
	.string	"v"
	.string	"o"
	.string	"l"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC99:
	.string	"d"
	.string	"a"
	.string	"l"
	.string	"t"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC100:
	.string	"j"
	.string	"o"
	.string	"u"
	.string	"l"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC101:
	.string	"c"
	.string	"a"
	.string	"l"
	.string	"o"
	.string	"r"
	.string	"i"
	.string	"e"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC102:
	.string	"b"
	.string	"r"
	.string	"i"
	.string	"t"
	.string	"i"
	.string	"s"
	.string	"h"
	.string	"-"
	.string	"t"
	.string	"h"
	.string	"e"
	.string	"r"
	.string	"m"
	.string	"a"
	.string	"l"
	.string	"-"
	.string	"u"
	.string	"n"
	.string	"i"
	.string	"t"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC103:
	.string	"f"
	.string	"o"
	.string	"o"
	.string	"d"
	.string	"c"
	.string	"a"
	.string	"l"
	.string	"o"
	.string	"r"
	.string	"i"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC104:
	.string	"t"
	.string	"h"
	.string	"e"
	.string	"r"
	.string	"m"
	.string	"-"
	.string	"u"
	.string	"s"
	.string	""
	.string	""
	.align 2
.LC105:
	.string	"w"
	.string	"a"
	.string	"t"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC106:
	.string	"h"
	.string	"o"
	.string	"r"
	.string	"s"
	.string	"e"
	.string	"p"
	.string	"o"
	.string	"w"
	.string	"e"
	.string	"r"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC107:
	.string	"s"
	.string	"o"
	.string	"l"
	.string	"a"
	.string	"r"
	.string	"-"
	.string	"l"
	.string	"u"
	.string	"m"
	.string	"i"
	.string	"n"
	.string	"o"
	.string	"s"
	.string	"i"
	.string	"t"
	.string	"y"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC108:
	.string	"v"
	.string	"o"
	.string	"l"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC109:
	.string	"o"
	.string	"h"
	.string	"m"
	.string	""
	.string	""
	.align 2
.LC110:
	.string	"d"
	.string	"u"
	.string	"n"
	.string	"a"
	.string	"m"
	.string	""
	.string	""
	.align 2
.LC111:
	.string	"a"
	.string	"c"
	.string	"r"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC112:
	.string	"h"
	.string	"e"
	.string	"c"
	.string	"t"
	.string	"a"
	.string	"r"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC113:
	.string	"t"
	.string	"e"
	.string	"a"
	.string	"s"
	.string	"p"
	.string	"o"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.align 2
.LC114:
	.string	"t"
	.string	"a"
	.string	"b"
	.string	"l"
	.string	"e"
	.string	"s"
	.string	"p"
	.string	"o"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC115:
	.string	"f"
	.string	"l"
	.string	"u"
	.string	"i"
	.string	"d"
	.string	"-"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"c"
	.string	"e"
	.string	"-"
	.string	"i"
	.string	"m"
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"i"
	.string	"a"
	.string	"l"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC116:
	.string	"f"
	.string	"l"
	.string	"u"
	.string	"i"
	.string	"d"
	.string	"-"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"c"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC117:
	.string	"c"
	.string	"u"
	.string	"p"
	.string	""
	.string	""
	.align 2
.LC118:
	.string	"c"
	.string	"u"
	.string	"p"
	.string	"-"
	.string	"m"
	.string	"e"
	.string	"t"
	.string	"r"
	.string	"i"
	.string	"c"
	.string	""
	.string	""
	.align 2
.LC119:
	.string	"p"
	.string	"i"
	.string	"n"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC120:
	.string	"p"
	.string	"i"
	.string	"n"
	.string	"t"
	.string	"-"
	.string	"m"
	.string	"e"
	.string	"t"
	.string	"r"
	.string	"i"
	.string	"c"
	.string	""
	.string	""
	.align 2
.LC121:
	.string	"q"
	.string	"u"
	.string	"a"
	.string	"r"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC122:
	.string	"l"
	.string	"i"
	.string	"t"
	.string	"e"
	.string	"r"
	.string	""
	.string	""
	.align 2
.LC123:
	.string	"g"
	.string	"a"
	.string	"l"
	.string	"l"
	.string	"o"
	.string	"n"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC124:
	.string	"g"
	.string	"a"
	.string	"l"
	.string	"l"
	.string	"o"
	.string	"n"
	.string	"-"
	.string	"i"
	.string	"m"
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"i"
	.string	"a"
	.string	"l"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC125:
	.string	"b"
	.string	"u"
	.string	"s"
	.string	"h"
	.string	"e"
	.string	"l"
	.string	""
	.string	""
	.align 2
.LC126:
	.string	"b"
	.string	"a"
	.string	"r"
	.string	"r"
	.string	"e"
	.string	"l"
	.string	""
	.string	""
	.align 2
.LC127:
	.string	"k"
	.string	"n"
	.string	"o"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC128:
	.string	"g"
	.string	"-"
	.string	"f"
	.string	"o"
	.string	"r"
	.string	"c"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC129:
	.string	"l"
	.string	"u"
	.string	"x"
	.string	""
	.string	""
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZN6icu_6712_GLOBAL__N_1L12gSimpleUnitsE, @object
	.size	_ZN6icu_6712_GLOBAL__N_1L12gSimpleUnitsE, 816
_ZN6icu_6712_GLOBAL__N_1L12gSimpleUnitsE:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.quad	.LC81
	.quad	.LC82
	.quad	.LC83
	.quad	.LC84
	.quad	.LC85
	.quad	.LC86
	.quad	.LC87
	.quad	.LC88
	.quad	.LC89
	.quad	.LC90
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.quad	.LC94
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.quad	.LC113
	.quad	.LC114
	.quad	.LC115
	.quad	.LC116
	.quad	.LC117
	.quad	.LC118
	.quad	.LC119
	.quad	.LC120
	.quad	.LC121
	.quad	.LC122
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.quad	.LC129
	.section	.rodata.str1.1
.LC130:
	.string	"yotta"
.LC131:
	.string	"zetta"
.LC132:
	.string	"exa"
.LC133:
	.string	"peta"
.LC134:
	.string	"tera"
.LC135:
	.string	"giga"
.LC136:
	.string	"mega"
.LC137:
	.string	"kilo"
.LC138:
	.string	"hecto"
.LC139:
	.string	"deka"
.LC140:
	.string	"deci"
.LC141:
	.string	"centi"
.LC142:
	.string	"milli"
.LC143:
	.string	"micro"
.LC144:
	.string	"nano"
.LC145:
	.string	"pico"
.LC146:
	.string	"femto"
.LC147:
	.string	"atto"
.LC148:
	.string	"zepto"
.LC149:
	.string	"yocto"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZN6icu_6712_GLOBAL__N_1L16gSIPrefixStringsE, @object
	.size	_ZN6icu_6712_GLOBAL__N_1L16gSIPrefixStringsE, 320
_ZN6icu_6712_GLOBAL__N_1L16gSIPrefixStringsE:
	.quad	.LC130
	.long	24
	.zero	4
	.quad	.LC131
	.long	21
	.zero	4
	.quad	.LC132
	.long	18
	.zero	4
	.quad	.LC133
	.long	15
	.zero	4
	.quad	.LC134
	.long	12
	.zero	4
	.quad	.LC135
	.long	9
	.zero	4
	.quad	.LC136
	.long	6
	.zero	4
	.quad	.LC137
	.long	3
	.zero	4
	.quad	.LC138
	.long	2
	.zero	4
	.quad	.LC139
	.long	1
	.zero	4
	.quad	.LC140
	.long	-1
	.zero	4
	.quad	.LC141
	.long	-2
	.zero	4
	.quad	.LC142
	.long	-3
	.zero	4
	.quad	.LC143
	.long	-6
	.zero	4
	.quad	.LC144
	.long	-9
	.zero	4
	.quad	.LC145
	.long	-12
	.zero	4
	.quad	.LC146
	.long	-15
	.zero	4
	.quad	.LC147
	.long	-18
	.zero	4
	.quad	.LC148
	.long	-21
	.zero	4
	.quad	.LC149
	.long	-24
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
