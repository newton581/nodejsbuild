	.file	"nortrans.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6727NormalizationTransliterator17getDynamicClassIDEv
	.type	_ZNK6icu_6727NormalizationTransliterator17getDynamicClassIDEv, @function
_ZNK6icu_6727NormalizationTransliterator17getDynamicClassIDEv:
.LFB2385:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6727NormalizationTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2385:
	.size	_ZNK6icu_6727NormalizationTransliterator17getDynamicClassIDEv, .-_ZNK6icu_6727NormalizationTransliterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6727NormalizationTransliteratorD2Ev
	.type	_ZN6icu_6727NormalizationTransliteratorD2Ev, @function
_ZN6icu_6727NormalizationTransliteratorD2Ev:
.LFB2393:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6727NormalizationTransliteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6714TransliteratorD2Ev@PLT
	.cfi_endproc
.LFE2393:
	.size	_ZN6icu_6727NormalizationTransliteratorD2Ev, .-_ZN6icu_6727NormalizationTransliteratorD2Ev
	.globl	_ZN6icu_6727NormalizationTransliteratorD1Ev
	.set	_ZN6icu_6727NormalizationTransliteratorD1Ev,_ZN6icu_6727NormalizationTransliteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6727NormalizationTransliteratorD0Ev
	.type	_ZN6icu_6727NormalizationTransliteratorD0Ev, @function
_ZN6icu_6727NormalizationTransliteratorD0Ev:
.LFB2395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6727NormalizationTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2395:
	.size	_ZN6icu_6727NormalizationTransliteratorD0Ev, .-_ZN6icu_6727NormalizationTransliteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6727NormalizationTransliterator5cloneEv
	.type	_ZNK6icu_6727NormalizationTransliterator5cloneEv, @function
_ZNK6icu_6727NormalizationTransliterator5cloneEv:
.LFB2399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L6
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6727NormalizationTransliteratorE(%rip), %rax
	movq	%rax, (%r12)
	movq	88(%rbx), %rax
	movq	%rax, 88(%r12)
.L6:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2399:
	.size	_ZNK6icu_6727NormalizationTransliterator5cloneEv, .-_ZNK6icu_6727NormalizationTransliterator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6727NormalizationTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.type	_ZNK6icu_6727NormalizationTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, @function
_ZNK6icu_6727NormalizationTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona:
.LFB2400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$216, %rsp
	movb	%cl, -241(%rbp)
	movl	8(%rdx), %esi
	movq	%rdi, -232(%rbp)
	movl	12(%rdx), %eax
	movq	%rdx, -256(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movl	%esi, -216(%rbp)
	movl	%eax, -212(%rbp)
	cmpl	%eax, %esi
	jge	.L12
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movq	%rbx, %rdi
	movl	$2, %ecx
	movq	%rax, -192(%rbp)
	leaq	-192(%rbp), %r14
	movq	%rax, -128(%rbp)
	movq	(%rbx), %rax
	movl	$0, -196(%rbp)
	movw	%dx, -184(%rbp)
	movw	%cx, -120(%rbp)
	call	*80(%rax)
	movq	%r14, -224(%rbp)
	movl	%eax, %r12d
	leaq	-128(%rbp), %rax
	movq	%rax, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L30:
	movzwl	-184(%rbp), %edx
	movl	$2, %ecx
	movl	-216(%rbp), %r13d
	movl	%edx, %eax
	movl	%r13d, %r14d
	andl	$31, %eax
	andl	$1, %edx
	cmovne	%ecx, %eax
	movw	%ax, -184(%rbp)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L54:
	movq	120(%rax), %r13
	movq	(%rbx), %rax
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	*80(%rax)
	movq	%r15, %rdi
	movl	%eax, %r12d
	movl	%eax, %esi
	call	*%r13
	testb	%al, %al
	jne	.L53
.L18:
	movq	-224(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	xorl	%eax, %eax
	cmpl	$65535, %r12d
	seta	%al
	leal	1(%r14,%rax), %r14d
	movq	-232(%rbp), %rax
	movq	88(%rax), %r15
	movq	(%r15), %rax
	cmpl	%r14d, -212(%rbp)
	jg	.L54
	movl	%r14d, %r13d
	jne	.L19
	cmpb	$0, -241(%rbp)
	je	.L19
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	*128(%rax)
	testb	%al, %al
	je	.L34
	movq	-232(%rbp), %rax
	movq	88(%rax), %r15
	movq	(%r15), %rax
.L19:
	movq	-240(%rbp), %rdx
	movq	-224(%rbp), %rsi
	leaq	-196(%rbp), %rcx
	movq	%r15, %rdi
	call	*24(%rax)
	movl	-196(%rbp), %eax
	testl	%eax, %eax
	jg	.L51
	movswl	-120(%rbp), %ecx
	movzwl	-184(%rbp), %eax
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L21
	testw	%ax, %ax
	js	.L22
	movswl	%ax, %edx
	sarl	$5, %edx
.L23:
	testw	%cx, %cx
	js	.L24
	sarl	$5, %ecx
.L25:
	cmpl	%edx, %ecx
	jne	.L26
	testb	%sil, %sil
	je	.L55
.L26:
	movq	(%rbx), %rax
	movq	-240(%rbp), %rcx
	movl	%r13d, %edx
	movq	%rbx, %rdi
	movl	-216(%rbp), %esi
	call	*32(%rax)
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L28
	sarl	$5, %eax
.L29:
	movl	%r13d, %edx
	subl	-216(%rbp), %edx
	subl	%edx, %eax
	addl	%eax, -212(%rbp)
	leal	0(%r13,%rax), %ecx
	movl	%ecx, -216(%rbp)
.L27:
	movl	-212(%rbp), %ecx
	cmpl	%ecx, -216(%rbp)
	jl	.L30
.L34:
	movq	-224(%rbp), %r14
	movl	-216(%rbp), %r15d
.L20:
	movq	-256(%rbp), %rcx
	movl	-212(%rbp), %ebx
	movq	-240(%rbp), %rdi
	movl	%ebx, %eax
	subl	12(%rcx), %eax
	addl	%eax, 4(%rcx)
	movl	%r15d, 8(%rcx)
	movl	%ebx, 12(%rcx)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L12:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L55:
	.cfi_restore_state
	movq	-240(%rbp), %rsi
	movq	-224(%rbp), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L21:
	testb	%sil, %sil
	je	.L26
	movl	%r13d, -216(%rbp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L53:
	movq	-232(%rbp), %rax
	movl	%r14d, %r13d
	movq	88(%rax), %r15
	movq	(%r15), %rax
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L24:
	movl	-116(%rbp), %ecx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L22:
	movl	-180(%rbp), %edx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L28:
	movl	-116(%rbp), %eax
	jmp	.L29
.L51:
	movq	-224(%rbp), %r14
	movl	%r13d, %r15d
	jmp	.L20
.L56:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2400:
	.size	_ZNK6icu_6727NormalizationTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, .-_ZNK6icu_6727NormalizationTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.align 2
	.p2align 4
	.globl	_ZN6icu_6727NormalizationTransliterator7_createERKNS_13UnicodeStringENS_14Transliterator5TokenE
	.type	_ZN6icu_6727NormalizationTransliterator7_createERKNS_13UnicodeStringENS_14Transliterator5TokenE, @function
_ZN6icu_6727NormalizationTransliterator7_createERKNS_13UnicodeStringENS_14Transliterator5TokenE:
.LFB2388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	movq	%r12, %rsi
	leaq	-44(%rbp), %rcx
	xorl	%edi, %edi
	movsbl	1(%r12,%rax), %edx
	movl	$0, -44(%rbp)
	xorl	%r12d, %r12d
	call	_ZN6icu_6711Normalizer211getInstanceEPKcS2_19UNormalization2ModeR10UErrorCode@PLT
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jg	.L57
	movl	$96, %edi
	movq	%rax, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L57
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	leaq	16+_ZTVN6icu_6727NormalizationTransliteratorE(%rip), %rax
	movq	%rbx, 88(%r12)
	movq	%rax, (%r12)
.L57:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2388:
	.size	_ZN6icu_6727NormalizationTransliterator7_createERKNS_13UnicodeStringENS_14Transliterator5TokenE, .-_ZN6icu_6727NormalizationTransliterator7_createERKNS_13UnicodeStringENS_14Transliterator5TokenE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6727NormalizationTransliterator16getStaticClassIDEv
	.type	_ZN6icu_6727NormalizationTransliterator16getStaticClassIDEv, @function
_ZN6icu_6727NormalizationTransliterator16getStaticClassIDEv:
.LFB2384:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6727NormalizationTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2384:
	.size	_ZN6icu_6727NormalizationTransliterator16getStaticClassIDEv, .-_ZN6icu_6727NormalizationTransliterator16getStaticClassIDEv
	.section	.rodata
.LC0:
	.string	"nfc"
	.string	""
	.string	""
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC1:
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"-"
	.string	"N"
	.string	"F"
	.string	"C"
	.string	""
	.string	""
	.section	.rodata
.LC2:
	.string	"nfkc"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC3:
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"-"
	.string	"N"
	.string	"F"
	.string	"K"
	.string	"C"
	.string	""
	.string	""
	.section	.rodata
.LC4:
	.string	"nfc"
	.string	"\001"
	.section	.rodata.str2.2
	.align 2
.LC5:
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"-"
	.string	"N"
	.string	"F"
	.string	"D"
	.string	""
	.string	""
	.section	.rodata
.LC6:
	.string	"nfkc"
	.string	"\001"
	.section	.rodata.str2.2
	.align 2
.LC7:
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"-"
	.string	"N"
	.string	"F"
	.string	"K"
	.string	"D"
	.string	""
	.string	""
	.section	.rodata
.LC8:
	.string	"nfc"
	.string	"\002"
	.section	.rodata.str2.2
	.align 2
.LC9:
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"-"
	.string	"F"
	.string	"C"
	.string	"D"
	.string	""
	.string	""
	.section	.rodata
.LC10:
	.string	"nfc"
	.string	"\003"
	.section	.rodata.str2.2
	.align 2
.LC11:
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"-"
	.string	"F"
	.string	"C"
	.string	"C"
	.string	""
	.string	""
	.align 2
.LC12:
	.string	"N"
	.string	"F"
	.string	"D"
	.string	""
	.string	""
	.align 2
.LC13:
	.string	"N"
	.string	"F"
	.string	"C"
	.string	""
	.string	""
	.align 2
.LC14:
	.string	"N"
	.string	"F"
	.string	"K"
	.string	"D"
	.string	""
	.string	""
	.align 2
.LC15:
	.string	"N"
	.string	"F"
	.string	"K"
	.string	"C"
	.string	""
	.string	""
	.align 2
.LC16:
	.string	"F"
	.string	"C"
	.string	"C"
	.string	""
	.string	""
	.align 2
.LC17:
	.string	"F"
	.string	"C"
	.string	"D"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6727NormalizationTransliterator11registerIDsEv
	.type	_ZN6icu_6727NormalizationTransliterator11registerIDsEv, @function
_ZN6icu_6727NormalizationTransliterator11registerIDsEv:
.LFB2387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-200(%rbp), %r14
	pushq	%r13
	movq	%r14, %rdx
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rax
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	.LC0(%rip), %rdx
	leaq	_ZN6icu_6727NormalizationTransliterator7_createERKNS_13UnicodeStringENS_14Transliterator5TokenE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC3(%rip), %rax
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	.LC2(%rip), %rdx
	leaq	_ZN6icu_6727NormalizationTransliterator7_createERKNS_13UnicodeStringENS_14Transliterator5TokenE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC5(%rip), %rax
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	.LC4(%rip), %rdx
	leaq	_ZN6icu_6727NormalizationTransliterator7_createERKNS_13UnicodeStringENS_14Transliterator5TokenE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC7(%rip), %rax
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	.LC6(%rip), %rdx
	leaq	_ZN6icu_6727NormalizationTransliterator7_createERKNS_13UnicodeStringENS_14Transliterator5TokenE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC9(%rip), %rax
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	.LC8(%rip), %rdx
	leaq	_ZN6icu_6727NormalizationTransliterator7_createERKNS_13UnicodeStringENS_14Transliterator5TokenE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC11(%rip), %rax
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	.LC10(%rip), %rdx
	leaq	_ZN6icu_6727NormalizationTransliterator7_createERKNS_13UnicodeStringENS_14Transliterator5TokenE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator16_registerFactoryERKNS_13UnicodeStringEPFPS0_S3_NS0_5TokenEES5_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	-192(%rbp), %r13
	leaq	.LC12(%rip), %rbx
	leaq	-208(%rbp), %r15
	movq	%rbx, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$-1, %ecx
	movq	%r15, %rdx
	movq	%r13, %rdi
	leaq	.LC13(%rip), %rax
	movl	$1, %esi
	movq	%rax, -208(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6714Transliterator23_registerSpecialInverseERKNS_13UnicodeStringES3_a@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC14(%rip), %rax
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$-1, %ecx
	movq	%r15, %rdx
	movq	%r13, %rdi
	leaq	.LC15(%rip), %rax
	movl	$1, %esi
	movq	%rax, -208(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6714Transliterator23_registerSpecialInverseERKNS_13UnicodeStringES3_a@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rbx, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$-1, %ecx
	movq	%r15, %rdx
	movq	%r13, %rdi
	leaq	.LC16(%rip), %rax
	movl	$1, %esi
	movq	%rax, -208(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6714Transliterator23_registerSpecialInverseERKNS_13UnicodeStringES3_a@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC17(%rip), %rbx
	movq	%rbx, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$-1, %ecx
	movq	%r15, %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rbx, -208(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6714Transliterator23_registerSpecialInverseERKNS_13UnicodeStringES3_a@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L70
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L70:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2387:
	.size	_ZN6icu_6727NormalizationTransliterator11registerIDsEv, .-_ZN6icu_6727NormalizationTransliterator11registerIDsEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6727NormalizationTransliteratorC2ERKNS_13UnicodeStringERKNS_11Normalizer2E
	.type	_ZN6icu_6727NormalizationTransliteratorC2ERKNS_13UnicodeStringERKNS_11Normalizer2E, @function
_ZN6icu_6727NormalizationTransliteratorC2ERKNS_13UnicodeStringERKNS_11Normalizer2E:
.LFB2390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	leaq	16+_ZTVN6icu_6727NormalizationTransliteratorE(%rip), %rax
	movq	%r12, 88(%rbx)
	movq	%rax, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2390:
	.size	_ZN6icu_6727NormalizationTransliteratorC2ERKNS_13UnicodeStringERKNS_11Normalizer2E, .-_ZN6icu_6727NormalizationTransliteratorC2ERKNS_13UnicodeStringERKNS_11Normalizer2E
	.globl	_ZN6icu_6727NormalizationTransliteratorC1ERKNS_13UnicodeStringERKNS_11Normalizer2E
	.set	_ZN6icu_6727NormalizationTransliteratorC1ERKNS_13UnicodeStringERKNS_11Normalizer2E,_ZN6icu_6727NormalizationTransliteratorC2ERKNS_13UnicodeStringERKNS_11Normalizer2E
	.align 2
	.p2align 4
	.globl	_ZN6icu_6727NormalizationTransliteratorC2ERKS0_
	.type	_ZN6icu_6727NormalizationTransliteratorC2ERKS0_, @function
_ZN6icu_6727NormalizationTransliteratorC2ERKS0_:
.LFB2397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6727NormalizationTransliteratorE(%rip), %rax
	movq	%rax, (%rbx)
	movq	88(%r12), %rax
	movq	%rax, 88(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2397:
	.size	_ZN6icu_6727NormalizationTransliteratorC2ERKS0_, .-_ZN6icu_6727NormalizationTransliteratorC2ERKS0_
	.globl	_ZN6icu_6727NormalizationTransliteratorC1ERKS0_
	.set	_ZN6icu_6727NormalizationTransliteratorC1ERKS0_,_ZN6icu_6727NormalizationTransliteratorC2ERKS0_
	.weak	_ZTSN6icu_6727NormalizationTransliteratorE
	.section	.rodata._ZTSN6icu_6727NormalizationTransliteratorE,"aG",@progbits,_ZTSN6icu_6727NormalizationTransliteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6727NormalizationTransliteratorE, @object
	.size	_ZTSN6icu_6727NormalizationTransliteratorE, 39
_ZTSN6icu_6727NormalizationTransliteratorE:
	.string	"N6icu_6727NormalizationTransliteratorE"
	.weak	_ZTIN6icu_6727NormalizationTransliteratorE
	.section	.data.rel.ro._ZTIN6icu_6727NormalizationTransliteratorE,"awG",@progbits,_ZTIN6icu_6727NormalizationTransliteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6727NormalizationTransliteratorE, @object
	.size	_ZTIN6icu_6727NormalizationTransliteratorE, 24
_ZTIN6icu_6727NormalizationTransliteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6727NormalizationTransliteratorE
	.quad	_ZTIN6icu_6714TransliteratorE
	.weak	_ZTVN6icu_6727NormalizationTransliteratorE
	.section	.data.rel.ro._ZTVN6icu_6727NormalizationTransliteratorE,"awG",@progbits,_ZTVN6icu_6727NormalizationTransliteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6727NormalizationTransliteratorE, @object
	.size	_ZTVN6icu_6727NormalizationTransliteratorE, 152
_ZTVN6icu_6727NormalizationTransliteratorE:
	.quad	0
	.quad	_ZTIN6icu_6727NormalizationTransliteratorE
	.quad	_ZN6icu_6727NormalizationTransliteratorD1Ev
	.quad	_ZN6icu_6727NormalizationTransliteratorD0Ev
	.quad	_ZNK6icu_6727NormalizationTransliterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6727NormalizationTransliterator5cloneEv
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.quad	_ZNK6icu_6727NormalizationTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.quad	_ZNK6icu_6714Transliterator5getIDEv
	.quad	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE
	.local	_ZZN6icu_6727NormalizationTransliterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6727NormalizationTransliterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
