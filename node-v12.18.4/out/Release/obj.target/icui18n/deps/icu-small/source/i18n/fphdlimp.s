	.file	"fphdlimp.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FieldPositionOnlyHandlerD2Ev
	.type	_ZN6icu_6724FieldPositionOnlyHandlerD2Ev, @function
_ZN6icu_6724FieldPositionOnlyHandlerD2Ev:
.LFB2402:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2402:
	.size	_ZN6icu_6724FieldPositionOnlyHandlerD2Ev, .-_ZN6icu_6724FieldPositionOnlyHandlerD2Ev
	.globl	_ZN6icu_6724FieldPositionOnlyHandlerD1Ev
	.set	_ZN6icu_6724FieldPositionOnlyHandlerD1Ev,_ZN6icu_6724FieldPositionOnlyHandlerD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FieldPositionOnlyHandler12addAttributeEiii
	.type	_ZN6icu_6724FieldPositionOnlyHandler12addAttributeEiii, @function
_ZN6icu_6724FieldPositionOnlyHandler12addAttributeEiii:
.LFB2405:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	cmpl	8(%rax), %esi
	je	.L6
.L3:
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	cmpb	$0, 24(%rdi)
	jne	.L7
.L5:
	addl	8(%rdi), %edx
	movb	$1, 25(%rdi)
	movl	%edx, 12(%rax)
	addl	8(%rdi), %ecx
	movl	%ecx, 16(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	cmpb	$0, 25(%rdi)
	jne	.L3
	jmp	.L5
	.cfi_endproc
.LFE2405:
	.size	_ZN6icu_6724FieldPositionOnlyHandler12addAttributeEiii, .-_ZN6icu_6724FieldPositionOnlyHandler12addAttributeEiii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724FieldPositionOnlyHandler11isRecordingEv
	.type	_ZNK6icu_6724FieldPositionOnlyHandler11isRecordingEv, @function
_ZNK6icu_6724FieldPositionOnlyHandler11isRecordingEv:
.LFB2407:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	cmpl	$-1, 8(%rax)
	setne	%al
	ret
	.cfi_endproc
.LFE2407:
	.size	_ZNK6icu_6724FieldPositionOnlyHandler11isRecordingEv, .-_ZNK6icu_6724FieldPositionOnlyHandler11isRecordingEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6728FieldPositionIteratorHandler11isRecordingEv
	.type	_ZNK6icu_6728FieldPositionIteratorHandler11isRecordingEv, @function
_ZNK6icu_6728FieldPositionIteratorHandler11isRecordingEv:
.LFB2421:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	testl	%eax, %eax
	setle	%al
	ret
	.cfi_endproc
.LFE2421:
	.size	_ZNK6icu_6728FieldPositionIteratorHandler11isRecordingEv, .-_ZNK6icu_6728FieldPositionIteratorHandler11isRecordingEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FieldPositionOnlyHandlerD0Ev
	.type	_ZN6icu_6724FieldPositionOnlyHandlerD0Ev, @function
_ZN6icu_6724FieldPositionOnlyHandlerD0Ev:
.LFB2404:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2404:
	.size	_ZN6icu_6724FieldPositionOnlyHandlerD0Ev, .-_ZN6icu_6724FieldPositionOnlyHandlerD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728FieldPositionIteratorHandlerD2Ev
	.type	_ZN6icu_6728FieldPositionIteratorHandlerD2Ev, @function
_ZN6icu_6728FieldPositionIteratorHandlerD2Ev:
.LFB2416:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	leaq	16+_ZTVN6icu_6728FieldPositionIteratorHandlerE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%r8, %r8
	je	.L11
	movq	24(%rdi), %rsi
	leaq	32(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZN6icu_6721FieldPositionIterator7setDataEPNS_9UVector32ER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.cfi_endproc
.LFE2416:
	.size	_ZN6icu_6728FieldPositionIteratorHandlerD2Ev, .-_ZN6icu_6728FieldPositionIteratorHandlerD2Ev
	.globl	_ZN6icu_6728FieldPositionIteratorHandlerD1Ev
	.set	_ZN6icu_6728FieldPositionIteratorHandlerD1Ev,_ZN6icu_6728FieldPositionIteratorHandlerD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728FieldPositionIteratorHandlerD0Ev
	.type	_ZN6icu_6728FieldPositionIteratorHandlerD0Ev, @function
_ZN6icu_6728FieldPositionIteratorHandlerD0Ev:
.LFB2418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6728FieldPositionIteratorHandlerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L14
	movq	24(%r12), %rsi
	leaq	32(%r12), %rdx
	call	_ZN6icu_6721FieldPositionIterator7setDataEPNS_9UVector32ER10UErrorCode@PLT
.L14:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2418:
	.size	_ZN6icu_6728FieldPositionIteratorHandlerD0Ev, .-_ZN6icu_6728FieldPositionIteratorHandlerD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FieldPositionOnlyHandler9shiftLastEi
	.type	_ZN6icu_6724FieldPositionOnlyHandler9shiftLastEi, @function
_ZN6icu_6724FieldPositionOnlyHandler9shiftLastEi:
.LFB2406:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L19
	movq	16(%rdi), %rax
	cmpl	$-1, 8(%rax)
	je	.L19
	movl	12(%rax), %edx
	cmpl	$-1, %edx
	je	.L19
	addl	%esi, %edx
	addl	%esi, 16(%rax)
	movl	%edx, 12(%rax)
.L19:
	ret
	.cfi_endproc
.LFE2406:
	.size	_ZN6icu_6724FieldPositionOnlyHandler9shiftLastEi, .-_ZN6icu_6724FieldPositionOnlyHandler9shiftLastEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728FieldPositionIteratorHandler9shiftLastEi
	.type	_ZN6icu_6728FieldPositionIteratorHandler9shiftLastEi, @function
_ZN6icu_6728FieldPositionIteratorHandler9shiftLastEi:
.LFB2420:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	testl	%eax, %eax
	jg	.L40
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	testl	%esi, %esi
	jne	.L43
.L27:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	movl	8(%rdi), %r13d
	testl	%r13d, %r13d
	jle	.L27
	movq	24(%rdi), %rcx
	leal	-1(%r13), %edx
	movslq	%edx, %rax
	movl	(%rcx,%rax,4), %esi
	leaq	0(,%rax,4), %r14
	addl	%r12d, %esi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movq	24(%rbx), %rdi
	leal	-2(%r13), %edx
	cmpl	$1, %r13d
	je	.L29
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L29
	subl	%edx, %eax
	testl	%eax, %eax
	jle	.L29
	movq	24(%rdi), %rax
	addl	-4(%rax,%r14), %r12d
.L29:
	popq	%rbx
	.cfi_restore 3
	movl	%r12d, %esi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679UVector3212setElementAtEii@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	ret
	.cfi_endproc
.LFE2420:
	.size	_ZN6icu_6728FieldPositionIteratorHandler9shiftLastEi, .-_ZN6icu_6728FieldPositionIteratorHandler9shiftLastEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728FieldPositionIteratorHandler12addAttributeEiii
	.type	_ZN6icu_6728FieldPositionIteratorHandler12addAttributeEiii, @function
_ZN6icu_6728FieldPositionIteratorHandler12addAttributeEiii:
.LFB2419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L44
	movq	%rdi, %rbx
	movl	32(%rdi), %edi
	testl	%edi, %edi
	jg	.L44
	movl	%edx, %r14d
	movl	%ecx, %r13d
	cmpl	%ecx, %edx
	jl	.L76
.L44:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movslq	8(%r12), %rax
	movl	%esi, %r8d
	movl	36(%rbx), %r9d
	leaq	32(%rbx), %r15
	movl	%eax, %esi
	movl	%eax, -52(%rbp)
	addl	$1, %esi
	js	.L48
	cmpl	12(%r12), %esi
	jle	.L63
.L48:
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	%r8d, -56(%rbp)
	movl	%r9d, -64(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movl	-64(%rbp), %r9d
	movl	-56(%rbp), %r8d
	testb	%al, %al
	jne	.L50
	movq	24(%rbx), %rcx
.L51:
	movslq	8(%rcx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L52
	cmpl	12(%rcx), %esi
	jle	.L64
.L52:
	movq	%rcx, %rdi
	movq	%r15, %rdx
	movl	%r8d, -56(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	-64(%rbp), %rcx
	movl	-56(%rbp), %r8d
	testb	%al, %al
	jne	.L54
	movq	24(%rbx), %r12
.L55:
	movslq	8(%r12), %rax
	movl	8(%rbx), %ecx
	movl	%eax, %esi
	addl	$1, %esi
	js	.L56
	cmpl	12(%r12), %esi
	jle	.L65
.L56:
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	%ecx, -64(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movl	-64(%rbp), %ecx
	testb	%al, %al
	jne	.L58
	movq	24(%rbx), %rdi
.L59:
	movslq	8(%rdi), %rax
	movl	8(%rbx), %r12d
	movl	%eax, %esi
	addl	$1, %esi
	js	.L60
	cmpl	12(%rdi), %esi
	jle	.L61
.L60:
	movq	%r15, %rdx
	movq	%rdi, -64(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L62
	movq	-64(%rbp), %rdi
	movslq	8(%rdi), %rax
.L61:
	movq	24(%rdi), %rdx
	leal	(%r12,%r13), %ecx
	movl	%ecx, (%rdx,%rax,4)
	addl	$1, 8(%rdi)
.L62:
	movl	32(%rbx), %eax
	testl	%eax, %eax
	jle	.L44
	movq	24(%rbx), %rdi
	movl	-52(%rbp), %esi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679UVector327setSizeEi@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movslq	8(%r12), %rax
	movq	24(%rbx), %rcx
.L49:
	movq	24(%r12), %rsi
	movl	%r9d, (%rsi,%rax,4)
	addl	$1, 8(%r12)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r12, %rcx
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L58:
	movslq	8(%r12), %rax
	movq	24(%rbx), %rdi
.L57:
	movq	24(%r12), %rsi
	addl	%ecx, %r14d
	movl	%r14d, (%rsi,%rax,4)
	addl	$1, 8(%r12)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L54:
	movslq	8(%rcx), %rax
	movq	24(%rbx), %r12
.L53:
	movq	24(%rcx), %rsi
	movl	%r8d, (%rsi,%rax,4)
	addl	$1, 8(%rcx)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%rcx, %r12
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%r12, %rdi
	jmp	.L57
	.cfi_endproc
.LFE2419:
	.size	_ZN6icu_6728FieldPositionIteratorHandler12addAttributeEiii, .-_ZN6icu_6728FieldPositionIteratorHandler12addAttributeEiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720FieldPositionHandlerD2Ev
	.type	_ZN6icu_6720FieldPositionHandlerD2Ev, @function
_ZN6icu_6720FieldPositionHandlerD2Ev:
.LFB2391:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2391:
	.size	_ZN6icu_6720FieldPositionHandlerD2Ev, .-_ZN6icu_6720FieldPositionHandlerD2Ev
	.globl	_ZN6icu_6720FieldPositionHandlerD1Ev
	.set	_ZN6icu_6720FieldPositionHandlerD1Ev,_ZN6icu_6720FieldPositionHandlerD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720FieldPositionHandlerD0Ev
	.type	_ZN6icu_6720FieldPositionHandlerD0Ev, @function
_ZN6icu_6720FieldPositionHandlerD0Ev:
.LFB2393:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2393:
	.size	_ZN6icu_6720FieldPositionHandlerD0Ev, .-_ZN6icu_6720FieldPositionHandlerD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720FieldPositionHandler8setShiftEi
	.type	_ZN6icu_6720FieldPositionHandler8setShiftEi, @function
_ZN6icu_6720FieldPositionHandler8setShiftEi:
.LFB2394:
	.cfi_startproc
	endbr64
	movl	%esi, 8(%rdi)
	ret
	.cfi_endproc
.LFE2394:
	.size	_ZN6icu_6720FieldPositionHandler8setShiftEi, .-_ZN6icu_6720FieldPositionHandler8setShiftEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FieldPositionOnlyHandlerC2ERNS_13FieldPositionE
	.type	_ZN6icu_6724FieldPositionOnlyHandlerC2ERNS_13FieldPositionE, @function
_ZN6icu_6724FieldPositionOnlyHandlerC2ERNS_13FieldPositionE:
.LFB2399:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6724FieldPositionOnlyHandlerE(%rip), %rax
	movl	$0, 8(%rdi)
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movq	%rsi, 16(%rdi)
	movw	%ax, 24(%rdi)
	ret
	.cfi_endproc
.LFE2399:
	.size	_ZN6icu_6724FieldPositionOnlyHandlerC2ERNS_13FieldPositionE, .-_ZN6icu_6724FieldPositionOnlyHandlerC2ERNS_13FieldPositionE
	.globl	_ZN6icu_6724FieldPositionOnlyHandlerC1ERNS_13FieldPositionE
	.set	_ZN6icu_6724FieldPositionOnlyHandlerC1ERNS_13FieldPositionE,_ZN6icu_6724FieldPositionOnlyHandlerC2ERNS_13FieldPositionE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724FieldPositionOnlyHandler18setAcceptFirstOnlyEa
	.type	_ZN6icu_6724FieldPositionOnlyHandler18setAcceptFirstOnlyEa, @function
_ZN6icu_6724FieldPositionOnlyHandler18setAcceptFirstOnlyEa:
.LFB2408:
	.cfi_startproc
	endbr64
	movb	%sil, 24(%rdi)
	ret
	.cfi_endproc
.LFE2408:
	.size	_ZN6icu_6724FieldPositionOnlyHandler18setAcceptFirstOnlyEa, .-_ZN6icu_6724FieldPositionOnlyHandler18setAcceptFirstOnlyEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728FieldPositionIteratorHandlerC2EPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZN6icu_6728FieldPositionIteratorHandlerC2EPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZN6icu_6728FieldPositionIteratorHandlerC2EPNS_21FieldPositionIteratorER10UErrorCode:
.LFB2410:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6728FieldPositionIteratorHandlerE(%rip), %rax
	movl	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	(%rdx), %eax
	movq	%rsi, 16(%rdi)
	movq	$0, 24(%rdi)
	movl	%eax, 32(%rdi)
	movl	$0, 36(%rdi)
	testl	%eax, %eax
	jg	.L95
	testq	%rsi, %rsi
	jne	.L98
.L95:
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L84
	leaq	32(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
.L84:
	movq	%r12, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2410:
	.size	_ZN6icu_6728FieldPositionIteratorHandlerC2EPNS_21FieldPositionIteratorER10UErrorCode, .-_ZN6icu_6728FieldPositionIteratorHandlerC2EPNS_21FieldPositionIteratorER10UErrorCode
	.globl	_ZN6icu_6728FieldPositionIteratorHandlerC1EPNS_21FieldPositionIteratorER10UErrorCode
	.set	_ZN6icu_6728FieldPositionIteratorHandlerC1EPNS_21FieldPositionIteratorER10UErrorCode,_ZN6icu_6728FieldPositionIteratorHandlerC2EPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728FieldPositionIteratorHandlerC2EPNS_9UVector32ER10UErrorCode
	.type	_ZN6icu_6728FieldPositionIteratorHandlerC2EPNS_9UVector32ER10UErrorCode, @function
_ZN6icu_6728FieldPositionIteratorHandlerC2EPNS_9UVector32ER10UErrorCode:
.LFB2413:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6728FieldPositionIteratorHandlerE(%rip), %rax
	movl	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	(%rdx), %eax
	movq	$0, 16(%rdi)
	movq	%rsi, 24(%rdi)
	movl	%eax, 32(%rdi)
	movl	$0, 36(%rdi)
	ret
	.cfi_endproc
.LFE2413:
	.size	_ZN6icu_6728FieldPositionIteratorHandlerC2EPNS_9UVector32ER10UErrorCode, .-_ZN6icu_6728FieldPositionIteratorHandlerC2EPNS_9UVector32ER10UErrorCode
	.globl	_ZN6icu_6728FieldPositionIteratorHandlerC1EPNS_9UVector32ER10UErrorCode
	.set	_ZN6icu_6728FieldPositionIteratorHandlerC1EPNS_9UVector32ER10UErrorCode,_ZN6icu_6728FieldPositionIteratorHandlerC2EPNS_9UVector32ER10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6720FieldPositionHandlerE
	.section	.rodata._ZTSN6icu_6720FieldPositionHandlerE,"aG",@progbits,_ZTSN6icu_6720FieldPositionHandlerE,comdat
	.align 32
	.type	_ZTSN6icu_6720FieldPositionHandlerE, @object
	.size	_ZTSN6icu_6720FieldPositionHandlerE, 32
_ZTSN6icu_6720FieldPositionHandlerE:
	.string	"N6icu_6720FieldPositionHandlerE"
	.weak	_ZTIN6icu_6720FieldPositionHandlerE
	.section	.data.rel.ro._ZTIN6icu_6720FieldPositionHandlerE,"awG",@progbits,_ZTIN6icu_6720FieldPositionHandlerE,comdat
	.align 8
	.type	_ZTIN6icu_6720FieldPositionHandlerE, @object
	.size	_ZTIN6icu_6720FieldPositionHandlerE, 24
_ZTIN6icu_6720FieldPositionHandlerE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6720FieldPositionHandlerE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6724FieldPositionOnlyHandlerE
	.section	.rodata._ZTSN6icu_6724FieldPositionOnlyHandlerE,"aG",@progbits,_ZTSN6icu_6724FieldPositionOnlyHandlerE,comdat
	.align 32
	.type	_ZTSN6icu_6724FieldPositionOnlyHandlerE, @object
	.size	_ZTSN6icu_6724FieldPositionOnlyHandlerE, 36
_ZTSN6icu_6724FieldPositionOnlyHandlerE:
	.string	"N6icu_6724FieldPositionOnlyHandlerE"
	.weak	_ZTIN6icu_6724FieldPositionOnlyHandlerE
	.section	.data.rel.ro._ZTIN6icu_6724FieldPositionOnlyHandlerE,"awG",@progbits,_ZTIN6icu_6724FieldPositionOnlyHandlerE,comdat
	.align 8
	.type	_ZTIN6icu_6724FieldPositionOnlyHandlerE, @object
	.size	_ZTIN6icu_6724FieldPositionOnlyHandlerE, 24
_ZTIN6icu_6724FieldPositionOnlyHandlerE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6724FieldPositionOnlyHandlerE
	.quad	_ZTIN6icu_6720FieldPositionHandlerE
	.weak	_ZTSN6icu_6728FieldPositionIteratorHandlerE
	.section	.rodata._ZTSN6icu_6728FieldPositionIteratorHandlerE,"aG",@progbits,_ZTSN6icu_6728FieldPositionIteratorHandlerE,comdat
	.align 32
	.type	_ZTSN6icu_6728FieldPositionIteratorHandlerE, @object
	.size	_ZTSN6icu_6728FieldPositionIteratorHandlerE, 40
_ZTSN6icu_6728FieldPositionIteratorHandlerE:
	.string	"N6icu_6728FieldPositionIteratorHandlerE"
	.weak	_ZTIN6icu_6728FieldPositionIteratorHandlerE
	.section	.data.rel.ro._ZTIN6icu_6728FieldPositionIteratorHandlerE,"awG",@progbits,_ZTIN6icu_6728FieldPositionIteratorHandlerE,comdat
	.align 8
	.type	_ZTIN6icu_6728FieldPositionIteratorHandlerE, @object
	.size	_ZTIN6icu_6728FieldPositionIteratorHandlerE, 24
_ZTIN6icu_6728FieldPositionIteratorHandlerE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6728FieldPositionIteratorHandlerE
	.quad	_ZTIN6icu_6720FieldPositionHandlerE
	.weak	_ZTVN6icu_6720FieldPositionHandlerE
	.section	.data.rel.ro._ZTVN6icu_6720FieldPositionHandlerE,"awG",@progbits,_ZTVN6icu_6720FieldPositionHandlerE,comdat
	.align 8
	.type	_ZTVN6icu_6720FieldPositionHandlerE, @object
	.size	_ZTVN6icu_6720FieldPositionHandlerE, 56
_ZTVN6icu_6720FieldPositionHandlerE:
	.quad	0
	.quad	_ZTIN6icu_6720FieldPositionHandlerE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6724FieldPositionOnlyHandlerE
	.section	.data.rel.ro.local._ZTVN6icu_6724FieldPositionOnlyHandlerE,"awG",@progbits,_ZTVN6icu_6724FieldPositionOnlyHandlerE,comdat
	.align 8
	.type	_ZTVN6icu_6724FieldPositionOnlyHandlerE, @object
	.size	_ZTVN6icu_6724FieldPositionOnlyHandlerE, 56
_ZTVN6icu_6724FieldPositionOnlyHandlerE:
	.quad	0
	.quad	_ZTIN6icu_6724FieldPositionOnlyHandlerE
	.quad	_ZN6icu_6724FieldPositionOnlyHandlerD1Ev
	.quad	_ZN6icu_6724FieldPositionOnlyHandlerD0Ev
	.quad	_ZN6icu_6724FieldPositionOnlyHandler12addAttributeEiii
	.quad	_ZN6icu_6724FieldPositionOnlyHandler9shiftLastEi
	.quad	_ZNK6icu_6724FieldPositionOnlyHandler11isRecordingEv
	.weak	_ZTVN6icu_6728FieldPositionIteratorHandlerE
	.section	.data.rel.ro.local._ZTVN6icu_6728FieldPositionIteratorHandlerE,"awG",@progbits,_ZTVN6icu_6728FieldPositionIteratorHandlerE,comdat
	.align 8
	.type	_ZTVN6icu_6728FieldPositionIteratorHandlerE, @object
	.size	_ZTVN6icu_6728FieldPositionIteratorHandlerE, 56
_ZTVN6icu_6728FieldPositionIteratorHandlerE:
	.quad	0
	.quad	_ZTIN6icu_6728FieldPositionIteratorHandlerE
	.quad	_ZN6icu_6728FieldPositionIteratorHandlerD1Ev
	.quad	_ZN6icu_6728FieldPositionIteratorHandlerD0Ev
	.quad	_ZN6icu_6728FieldPositionIteratorHandler12addAttributeEiii
	.quad	_ZN6icu_6728FieldPositionIteratorHandler9shiftLastEi
	.quad	_ZNK6icu_6728FieldPositionIteratorHandler11isRecordingEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
