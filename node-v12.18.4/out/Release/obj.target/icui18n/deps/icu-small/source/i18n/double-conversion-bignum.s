	.file	"double-conversion-bignum.cpp"
	.text
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB0:
	.text
.LHOTB0:
	.align 2
	.p2align 4
	.type	_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi.part.0, @function
_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi.part.0:
.LFB1028:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpl	$26, %esi
	jle	.L67
	movzwl	(%rdi), %esi
	movl	%ecx, %r10d
	leaq	8(%rdi), %rbx
	movl	$4195354525, %r11d
	.p2align 4,,10
	.p2align 3
.L13:
	testw	%si, %si
	jle	.L5
	movswl	%si, %eax
	leaq	4(%rdi), %r9
	subl	$1, %eax
	leaq	(%rbx,%rax,4), %r12
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L9:
	movl	(%r9), %edx
	movq	%rax, %r8
	shrq	$28, %rax
	addq	$4, %r9
	andl	$268435455, %r8d
	movq	%rdx, %r13
	imulq	$1734723475, %rdx, %rdx
	imulq	%r11, %r13
	salq	$4, %rdx
	addq	%r13, %r8
	addq	%rax, %rdx
	movl	%r8d, %r13d
	shrq	$28, %r8
	andl	$268435455, %r13d
	leaq	(%r8,%rdx), %rax
	movl	%r13d, -4(%r9)
	cmpq	%r9, %r12
	jne	.L9
	movswq	%si, %rdx
	testq	%rax, %rax
	je	.L5
	.p2align 4,,10
	.p2align 3
.L11:
	cmpw	$127, %dx
	jg	.L19
	movl	%eax, %esi
	andl	$268435455, %esi
	movl	%esi, 4(%rdi,%rdx,4)
	leal	1(%rdx), %esi
	addq	$1, %rdx
	shrq	$28, %rax
	movw	%si, (%rdi)
	jne	.L11
.L5:
	subl	$27, %r10d
	cmpl	$26, %r10d
	jg	.L13
.L12:
	cmpl	$12, %r10d
	jle	.L3
	movzwl	(%rdi), %r8d
	leaq	8(%rdi), %r11
.L21:
	testw	%r8w, %r8w
	jle	.L14
	movswl	%r8w, %eax
	leaq	4(%rdi), %rdx
	subl	$1, %eax
	leaq	(%r11,%rax,4), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L18:
	movl	(%rdx), %esi
	addq	$4, %rdx
	imulq	$1220703125, %rsi, %rsi
	addq	%rsi, %rax
	movl	%eax, %esi
	shrq	$28, %rax
	andl	$268435455, %esi
	movl	%esi, -4(%rdx)
	cmpq	%rdx, %r9
	jne	.L18
	movswq	%r8w, %rdx
	testq	%rax, %rax
	je	.L14
	.p2align 4,,10
	.p2align 3
.L20:
	cmpw	$127, %dx
	jg	.L19
	movl	%eax, %esi
	leal	1(%rdx), %r8d
	andl	$268435455, %esi
	movl	%esi, 4(%rdi,%rdx,4)
	addq	$1, %rdx
	shrq	$28, %rax
	movw	%r8w, (%rdi)
	jne	.L20
.L14:
	subl	$13, %r10d
	cmpl	$13, %r10d
	je	.L21
.L3:
	testl	%r10d, %r10d
	jle	.L22
	subl	$1, %r10d
	leaq	_ZZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEiE12kFive1_to_12(%rip), %rax
	movslq	%r10d, %r10
	movl	(%rax,%r10,4), %r8d
	cmpl	$1, %r8d
	je	.L22
	testl	%r8d, %r8d
	je	.L68
	movzwl	(%rdi), %r10d
	testw	%r10w, %r10w
	je	.L1
	movswq	%r10w, %r11
	testl	%r11d, %r11d
	jle	.L36
	leal	-1(%r11), %eax
	leaq	4(%rdi), %rdx
	leaq	8(%rdi,%rax,4), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L33:
	movl	(%rdx), %esi
	addq	$4, %rdx
	imulq	%r8, %rsi
	addq	%rsi, %rax
	movl	%eax, %esi
	shrq	$28, %rax
	andl	$268435455, %esi
	movl	%esi, -4(%rdx)
	cmpq	%rdx, %r9
	jne	.L33
	movswq	%r10w, %rdx
	testq	%rax, %rax
	jne	.L34
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L69:
	movl	%eax, %esi
	leal	1(%rdx), %r10d
	andl	$268435455, %esi
	movl	%esi, 4(%rdi,%rdx,4)
	addq	$1, %rdx
	shrq	$28, %rax
	movw	%r10w, (%rdi)
	je	.L24
.L34:
	cmpw	$127, %dx
	jle	.L69
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L22:
	movzwl	(%rdi), %r10d
.L24:
	testw	%r10w, %r10w
	je	.L1
	movswq	%r10w, %r11
.L36:
	movslq	%ecx, %rax
	movl	%ecx, %edx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edx
	shrq	$32, %rax
	addl	%ecx, %eax
	sarl	$4, %eax
	subl	%edx, %eax
	addw	%ax, 2(%rdi)
	imull	$28, %eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %esi
	cmpw	$127, %r10w
	jg	.L19
	testl	%r11d, %r11d
	jle	.L1
	movl	$28, %ebx
	leal	-1(%r11), %eax
	leaq	4(%rdi), %rdx
	xorl	%r8d, %r8d
	subl	%ecx, %ebx
	leaq	8(%rdi,%rax,4), %r12
	.p2align 4,,10
	.p2align 3
.L35:
	movl	(%rdx), %eax
	movl	%r8d, %r9d
	movl	%ebx, %ecx
	addq	$4, %rdx
	movl	%eax, %r8d
	shrl	%cl, %r8d
	movl	%esi, %ecx
	sall	%cl, %eax
	addl	%r9d, %eax
	andl	$268435455, %eax
	movl	%eax, -4(%rdx)
	cmpq	%rdx, %r12
	jne	.L35
	testl	%r8d, %r8d
	je	.L1
	addl	$1, %r10d
	movl	%r8d, 4(%rdi,%r11,4)
	movw	%r10w, (%rdi)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	$0, (%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movl	%esi, %r10d
	jmp	.L12
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi.part.0.cold, @function
_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi.part.0.cold:
.LFSB1028:
.L19:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	call	abort@PLT
	.cfi_endproc
.LFE1028:
	.text
	.size	_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi.part.0, .-_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi.part.0
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi.part.0.cold, .-_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi.part.0.cold
.LCOLDE0:
	.text
.LHOTE0:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum8RawBigitEi
	.type	_ZN6icu_6717double_conversion6Bignum8RawBigitEi, @function
_ZN6icu_6717double_conversion6Bignum8RawBigitEi:
.LFB980:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	leaq	4(%rdi,%rsi,4), %rax
	ret
	.cfi_endproc
.LFE980:
	.size	_ZN6icu_6717double_conversion6Bignum8RawBigitEi, .-_ZN6icu_6717double_conversion6Bignum8RawBigitEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717double_conversion6Bignum8RawBigitEi
	.type	_ZNK6icu_6717double_conversion6Bignum8RawBigitEi, @function
_ZNK6icu_6717double_conversion6Bignum8RawBigitEi:
.LFB1038:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	leaq	4(%rdi,%rsi,4), %rax
	ret
	.cfi_endproc
.LFE1038:
	.size	_ZNK6icu_6717double_conversion6Bignum8RawBigitEi, .-_ZNK6icu_6717double_conversion6Bignum8RawBigitEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum12AssignUInt16Et
	.type	_ZN6icu_6717double_conversion6Bignum12AssignUInt16Et, @function
_ZN6icu_6717double_conversion6Bignum12AssignUInt16Et:
.LFB983:
	.cfi_startproc
	endbr64
	movl	$0, (%rdi)
	testw	%si, %si
	je	.L72
	movzwl	%si, %esi
	movl	$1, %eax
	movl	%esi, 4(%rdi)
	movw	%ax, (%rdi)
.L72:
	ret
	.cfi_endproc
.LFE983:
	.size	_ZN6icu_6717double_conversion6Bignum12AssignUInt16Et, .-_ZN6icu_6717double_conversion6Bignum12AssignUInt16Et
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum12AssignUInt64Em
	.type	_ZN6icu_6717double_conversion6Bignum12AssignUInt64Em, @function
_ZN6icu_6717double_conversion6Bignum12AssignUInt64Em:
.LFB984:
	.cfi_startproc
	endbr64
	movl	$0, (%rdi)
	testq	%rsi, %rsi
	je	.L74
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L76:
	movl	%esi, %edx
	andl	$268435455, %edx
	movl	%edx, (%rdi,%rax,4)
	movl	%eax, %edx
	addq	$1, %rax
	shrq	$28, %rsi
	jne	.L76
	movw	%dx, (%rdi)
.L74:
	ret
	.cfi_endproc
.LFE984:
	.size	_ZN6icu_6717double_conversion6Bignum12AssignUInt64Em, .-_ZN6icu_6717double_conversion6Bignum12AssignUInt64Em
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum12AssignBignumERKS1_
	.type	_ZN6icu_6717double_conversion6Bignum12AssignBignumERKS1_, @function
_ZN6icu_6717double_conversion6Bignum12AssignBignumERKS1_:
.LFB985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzwl	2(%rsi), %eax
	movq	%rdi, %rbx
	movzwl	(%rsi), %r12d
	movw	%ax, 2(%rdi)
	testw	%r12w, %r12w
	jle	.L82
	movswl	%r12w, %eax
	addq	$4, %rsi
	leaq	4(%rdi), %rdi
	subl	$1, %eax
	leaq	4(,%rax,4), %rdx
	call	memmove@PLT
.L82:
	movw	%r12w, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE985:
	.size	_ZN6icu_6717double_conversion6Bignum12AssignBignumERKS1_, .-_ZN6icu_6717double_conversion6Bignum12AssignBignumERKS1_
	.section	.text.unlikely
	.align 2
.LCOLDB1:
	.text
.LHOTB1:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum15AssignHexStringENS0_6VectorIKcEE
	.type	_ZN6icu_6717double_conversion6Bignum15AssignHexStringENS0_6VectorIKcEE, @function
_ZN6icu_6717double_conversion6Bignum15AssignHexStringENS0_6VectorIKcEE:
.LFB989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	28(,%rdx,4), %eax
	movl	$0, (%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$3612, %eax
	jg	.L101
	testl	%edx, %edx
	je	.L84
	movslq	%edx, %rax
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	leaq	-2(%rax,%rsi), %r10
	leaq	-1(%rax,%rsi), %r8
	leal	-1(%rdx), %eax
	xorl	%edx, %edx
	subq	%rax, %r10
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L105:
	leal	1(%rdx), %esi
	movswq	%dx, %rax
	movl	%r9d, %edx
	subq	$1, %r8
	andl	$268435455, %edx
	movw	%si, (%rdi)
	subl	$24, %ecx
	shrq	$28, %r9
	movl	%edx, 4(%rdi,%rax,4)
	movl	%esi, %edx
	cmpq	%r8, %r10
	je	.L103
.L91:
	movsbl	(%r8), %esi
	leal	-48(%rsi), %eax
	cmpl	$9, %eax
	jbe	.L104
	leal	-97(%rsi), %r11d
	leal	-55(%rsi), %eax
	subl	$87, %esi
	movslq	%esi, %rsi
	cltq
	cmpl	$5, %r11d
	cmovbe	%rsi, %rax
.L88:
	salq	%cl, %rax
	orq	%rax, %r9
	leal	4(%rcx), %eax
	cmpl	$27, %eax
	jg	.L105
	subq	$1, %r8
	movl	%eax, %ecx
	cmpq	%r8, %r10
	jne	.L91
.L103:
	testq	%r9, %r9
	je	.L92
	leal	1(%rdx), %ecx
	movswq	%dx, %rax
	movw	%cx, (%rdi)
	movl	%ecx, %edx
	movl	%r9d, 4(%rdi,%rax,4)
.L92:
	testw	%dx, %dx
	jle	.L84
	movswq	%dx, %rax
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L106:
	leal	-1(%rax), %edx
	subq	$1, %rax
	movw	%dx, (%rdi)
	testw	%ax, %ax
	jle	.L84
.L94:
	movl	(%rdi,%rax,4), %edx
	testl	%edx, %edx
	je	.L106
.L84:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	cltq
	jmp	.L88
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion6Bignum15AssignHexStringENS0_6VectorIKcEE.cold, @function
_ZN6icu_6717double_conversion6Bignum15AssignHexStringENS0_6VectorIKcEE.cold:
.LFSB989:
.L101:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE989:
	.text
	.size	_ZN6icu_6717double_conversion6Bignum15AssignHexStringENS0_6VectorIKcEE, .-_ZN6icu_6717double_conversion6Bignum15AssignHexStringENS0_6VectorIKcEE
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion6Bignum15AssignHexStringENS0_6VectorIKcEE.cold, .-_ZN6icu_6717double_conversion6Bignum15AssignHexStringENS0_6VectorIKcEE.cold
.LCOLDE1:
	.text
.LHOTE1:
	.section	.text.unlikely
	.align 2
.LCOLDB2:
	.text
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum9AddBignumERKS1_
	.type	_ZN6icu_6717double_conversion6Bignum9AddBignumERKS1_, @function
_ZN6icu_6717double_conversion6Bignum9AddBignumERKS1_:
.LFB991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movswl	2(%rdi), %ecx
	movswl	2(%rsi), %r12d
	movswl	(%rdi), %r14d
	cmpw	%r12w, %cx
	jle	.L108
	movl	%ecx, %r8d
	subl	%r12d, %ecx
	leal	(%rcx,%r14), %eax
	movl	%ecx, %r12d
	cmpl	$128, %eax
	jg	.L120
	movl	%r14d, %edx
	movl	%r14d, %r15d
	subl	$1, %edx
	js	.L115
	leal	(%rcx,%rdx), %ecx
	leal	-3(%rcx), %eax
	cmpl	%eax, %edx
	setl	%sil
	cmpl	$-1, %r12d
	setl	%al
	orb	%al, %sil
	je	.L112
	cmpl	$3, %edx
	jbe	.L112
	movswq	%r14w, %rcx
	movslq	%r12d, %rax
	addq	%rcx, %rax
	leaq	-12(%rdi,%rcx,4), %rdi
	movl	%r14d, %ecx
	shrl	$2, %ecx
	leaq	-12(%rbx,%rax,4), %rsi
	xorl	%eax, %eax
	negq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L114:
	movdqu	(%rdi,%rax), %xmm0
	movups	%xmm0, (%rsi,%rax)
	subq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L114
	movl	%r14d, %ecx
	movl	%edx, %eax
	andl	$-4, %ecx
	subl	%ecx, %eax
	cmpl	%ecx, %r14d
	je	.L115
	movslq	%eax, %rdx
	movl	4(%rbx,%rdx,4), %ecx
	leal	(%r12,%rax), %edx
	movslq	%edx, %rdx
	movl	%ecx, 4(%rbx,%rdx,4)
	leal	-1(%rax), %edx
	testl	%eax, %eax
	je	.L115
	movslq	%edx, %rcx
	addl	%r12d, %edx
	movl	4(%rbx,%rcx,4), %ecx
	movslq	%edx, %rdx
	movl	%ecx, 4(%rbx,%rdx,4)
	leal	-2(%rax), %edx
	cmpl	$1, %eax
	je	.L115
	movslq	%edx, %rax
	addl	%r12d, %edx
	movl	4(%rbx,%rax,4), %eax
	movslq	%edx, %rdx
	movl	%eax, 4(%rbx,%rdx,4)
.L115:
	testl	%r12d, %r12d
	jle	.L119
	leal	-1(%r12), %eax
	leaq	4(%rbx), %rdi
	xorl	%esi, %esi
	movl	%r8d, -52(%rbp)
	leaq	4(,%rax,4), %rdx
	call	memset@PLT
	movl	-52(%rbp), %r8d
.L119:
	leal	(%r15,%r12), %r14d
	subl	%r12d, %r8d
	movw	%r14w, (%rbx)
	movswl	%r8w, %ecx
	movswl	%r14w, %r14d
	movw	%r8w, 2(%rbx)
	movswl	2(%r13), %r12d
.L108:
	movswl	0(%r13), %r15d
	leal	(%rcx,%r14), %eax
	leal	(%r15,%r12), %edx
	cmpl	%edx, %eax
	cmovl	%edx, %eax
	addl	$1, %eax
	subl	%ecx, %eax
	cmpl	$128, %eax
	jg	.L120
	subl	%ecx, %r12d
	cmpl	%r14d, %r12d
	jle	.L124
	movl	%r12d, %eax
	xorl	%esi, %esi
	subl	%r14d, %eax
	subl	$1, %eax
	leaq	4(,%rax,4), %rdx
	movslq	%r14d, %rax
	leaq	4(%rbx,%rax,4), %rdi
	call	memset@PLT
.L124:
	testl	%r15d, %r15d
	jle	.L122
	cmpl	%r14d, %r12d
	jge	.L133
	leal	(%r12,%r15), %r8d
	movslq	%r12d, %rax
	movl	%r12d, %esi
	movl	$1, %ecx
	cmpl	%r14d, %r8d
	leaq	(%rbx,%rax,4), %rdi
	cmovg	%r14d, %r8d
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L127:
	movl	(%rdi,%rcx,4), %edx
	addl	0(%r13,%rcx,4), %edx
	addl	$1, %esi
	addl	%edx, %eax
	movl	%eax, %edx
	shrl	$28, %eax
	andl	$268435455, %edx
	movl	%edx, (%rdi,%rcx,4)
	movslq	%ecx, %rdx
	addq	$1, %rcx
	cmpl	%r8d, %esi
	jl	.L127
	cmpl	%edx, %r15d
	jle	.L129
.L125:
	movslq	%esi, %rsi
	subq	%rdx, %rsi
	leaq	(%rbx,%rsi,4), %rsi
	.p2align 4,,10
	.p2align 3
.L130:
	addl	4(%r13,%rdx,4), %eax
	movl	%eax, %ecx
	shrl	$28, %eax
	andl	$268435455, %ecx
	movl	%ecx, 4(%rsi,%rdx,4)
	addq	$1, %rdx
	cmpl	%edx, %r15d
	jg	.L130
.L129:
	addl	%r15d, %r12d
	movslq	%r12d, %rdx
	testl	%eax, %eax
	je	.L122
	.p2align 4,,10
	.p2align 3
.L132:
	movslq	%edx, %rcx
	leal	1(%rdx), %r12d
	cmpl	%edx, %r14d
	jg	.L156
	movl	%eax, 4(%rbx,%rcx,4)
.L122:
	cmpl	%r14d, %r12d
	cmovl	%r14d, %r12d
	movw	%r12w, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	addl	4(%rbx,%rdx,4), %eax
	movl	%eax, %ecx
	andl	$268435455, %ecx
	movl	%ecx, 4(%rbx,%rdx,4)
	addq	$1, %rdx
	shrl	$28, %eax
	jne	.L132
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L133:
	movl	%r12d, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L125
.L112:
	movslq	%edx, %rax
	movslq	%ecx, %rcx
	movswq	%r15w, %rdx
	subq	%rdx, %rcx
	leaq	(%rbx,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L118:
	movl	4(%rbx,%rax,4), %edx
	movl	%edx, 8(%rcx,%rax,4)
	subq	$1, %rax
	testl	%eax, %eax
	jns	.L118
	jmp	.L115
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion6Bignum9AddBignumERKS1_.cold, @function
_ZN6icu_6717double_conversion6Bignum9AddBignumERKS1_.cold:
.LFSB991:
.L120:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE991:
	.text
	.size	_ZN6icu_6717double_conversion6Bignum9AddBignumERKS1_, .-_ZN6icu_6717double_conversion6Bignum9AddBignumERKS1_
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion6Bignum9AddBignumERKS1_.cold, .-_ZN6icu_6717double_conversion6Bignum9AddBignumERKS1_.cold
.LCOLDE2:
	.text
.LHOTE2:
	.align 2
	.p2align 4
	.type	_ZN6icu_6717double_conversion6Bignum9AddUInt64Em.part.0, @function
_ZN6icu_6717double_conversion6Bignum9AddUInt64Em.part.0:
.LFB1036:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$528, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-528(%rbp), %r8
	movl	$0, -528(%rbp)
	testq	%rsi, %rsi
	je	.L158
	movl	$1, %eax
	leaq	-528(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L159:
	movl	%esi, %edx
	andl	$268435455, %edx
	movl	%edx, (%r8,%rax,4)
	movl	%eax, %edx
	addq	$1, %rax
	shrq	$28, %rsi
	jne	.L159
	movw	%dx, -528(%rbp)
.L158:
	movq	%r8, %rsi
	call	_ZN6icu_6717double_conversion6Bignum9AddBignumERKS1_
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L166
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L166:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1036:
	.size	_ZN6icu_6717double_conversion6Bignum9AddUInt64Em.part.0, .-_ZN6icu_6717double_conversion6Bignum9AddUInt64Em.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum9AddUInt64Em
	.type	_ZN6icu_6717double_conversion6Bignum9AddUInt64Em, @function
_ZN6icu_6717double_conversion6Bignum9AddUInt64Em:
.LFB990:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L167
	jmp	_ZN6icu_6717double_conversion6Bignum9AddUInt64Em.part.0
	.p2align 4,,10
	.p2align 3
.L167:
	ret
	.cfi_endproc
.LFE990:
	.size	_ZN6icu_6717double_conversion6Bignum9AddUInt64Em, .-_ZN6icu_6717double_conversion6Bignum9AddUInt64Em
	.section	.text.unlikely
	.align 2
.LCOLDB3:
	.text
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum19AssignDecimalStringENS0_6VectorIKcEE
	.type	_ZN6icu_6717double_conversion6Bignum19AssignDecimalStringENS0_6VectorIKcEE, @function
_ZN6icu_6717double_conversion6Bignum19AssignDecimalStringENS0_6VectorIKcEE:
.LFB987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	movl	$0, (%rdi)
	cmpl	$18, %edx
	jle	.L206
	leaq	19(%rsi), %r12
	xorl	%ecx, %ecx
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	-19(%r12), %rdx
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L173:
	movsbl	(%rdx), %eax
	leaq	(%rsi,%rsi,4), %rsi
	addq	$1, %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rsi,2), %rsi
	cmpq	%rdx, %r12
	jne	.L173
	addl	$19, %r14d
	subl	$19, %r13d
	testw	%cx, %cx
	je	.L175
	movswq	%cx, %r8
	testl	%r8d, %r8d
	jle	.L176
	leal	-1(%r8), %eax
	leaq	4(%rbx), %rdi
	leaq	8(%rbx), %r11
	movq	%rax, %r10
	movq	%rdi, %rdx
	leaq	(%r11,%rax,4), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L183:
	movl	(%rdx), %r15d
	addq	$4, %rdx
	imulq	$1220703125, %r15, %r15
	addq	%r15, %rax
	movl	%eax, %r15d
	shrq	$28, %rax
	andl	$268435455, %r15d
	movl	%r15d, -4(%rdx)
	cmpq	%rdx, %r9
	jne	.L183
	testq	%rax, %rax
	je	.L178
	movswq	%cx, %rdx
	.p2align 4,,10
	.p2align 3
.L181:
	cmpw	$127, %dx
	jg	.L179
	movl	%eax, %ecx
	andl	$268435455, %ecx
	movl	%ecx, 4(%rbx,%rdx,4)
	leal	1(%rdx), %ecx
	addq	$1, %rdx
	shrq	$28, %rax
	movw	%cx, (%rbx)
	jne	.L181
	testw	%cx, %cx
	je	.L175
	movswq	%cx, %r8
	testl	%r8d, %r8d
	jle	.L176
	leal	-1(%r8), %r10d
.L178:
	movq	%rdi, %r9
	leaq	(%r11,%r10,4), %r10
	xorl	%eax, %eax
.L188:
	movl	(%r9), %edx
	addq	$4, %r9
	imulq	$15625, %rdx, %rdx
	addq	%rdx, %rax
	movl	%eax, %edx
	shrq	$28, %rax
	andl	$268435455, %edx
	movl	%edx, -4(%r9)
	cmpq	%r9, %r10
	jne	.L188
	testq	%rax, %rax
	je	.L176
	cmpw	$127, %cx
	jg	.L179
	addl	$1, %ecx
	movl	%eax, 4(%rbx,%r8,4)
	movw	%cx, (%rbx)
	movswq	%cx, %r8
	cmpw	$127, %cx
	jg	.L240
.L189:
	leal	-1(%r8), %eax
	xorl	%edx, %edx
	leaq	(%r11,%rax,4), %r9
	.p2align 4,,10
	.p2align 3
.L190:
	movl	(%rdi), %eax
	movl	%edx, %r10d
	addq	$4, %rdi
	movl	%eax, %edx
	sall	$19, %eax
	addl	%r10d, %eax
	shrl	$9, %edx
	andl	$268435455, %eax
	movl	%eax, -4(%rdi)
	cmpq	%rdi, %r9
	jne	.L190
	testl	%edx, %edx
	je	.L175
	addl	$1, %ecx
	movl	%edx, 4(%rbx,%r8,4)
	movw	%cx, (%rbx)
	.p2align 4,,10
	.p2align 3
.L175:
	testq	%rsi, %rsi
	je	.L191
	movq	%rbx, %rdi
	call	_ZN6icu_6717double_conversion6Bignum9AddUInt64Em.part.0
.L191:
	addq	$19, %r12
	cmpl	$18, %r13d
	jle	.L241
	movzwl	(%rbx), %ecx
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L176:
	cmpw	$127, %cx
	jg	.L242
	testl	%r8d, %r8d
	jle	.L175
	leaq	4(%rbx), %rdi
	leaq	8(%rbx), %r11
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L241:
	leal	0(%r13,%r14), %eax
.L170:
	cmpl	%eax, %r14d
	jge	.L192
	movq	-56(%rbp), %rdi
	movslq	%r14d, %rcx
	subl	$1, %eax
	xorl	%r12d, %r12d
	subl	%r14d, %eax
	leaq	1(%rcx,%rdi), %rsi
	leaq	(%rdi,%rcx), %rdx
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L193:
	movsbl	(%rdx), %eax
	leaq	(%r12,%r12,4), %rcx
	addq	$1, %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %r12
	cmpq	%rdx, %rsi
	jne	.L193
	testl	%r13d, %r13d
	je	.L194
	cmpw	$0, (%rbx)
	je	.L195
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi.part.0
.L194:
	testq	%r12, %r12
	je	.L197
.L196:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6717double_conversion6Bignum9AddUInt64Em.part.0
.L197:
	movzwl	(%rbx), %edx
	testw	%dx, %dx
	jle	.L198
	movswq	%dx, %rax
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L243:
	leal	-1(%rax), %edx
	subq	$1, %rax
	movw	%dx, (%rbx)
	testw	%ax, %ax
	jle	.L198
.L201:
	movl	(%rbx,%rax,4), %edx
	testl	%edx, %edx
	je	.L243
.L169:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L198:
	.cfi_restore_state
	testw	%dx, %dx
	jne	.L169
.L203:
	xorl	%eax, %eax
	movw	%ax, 2(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L195:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L196
	jmp	.L203
.L192:
	testl	%r13d, %r13d
	je	.L197
	cmpw	$0, (%rbx)
	je	.L203
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi.part.0
	jmp	.L197
.L206:
	movl	%edx, %eax
	xorl	%r14d, %r14d
	jmp	.L170
.L240:
	jmp	.L179
.L242:
	jmp	.L179
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion6Bignum19AssignDecimalStringENS0_6VectorIKcEE.cold, @function
_ZN6icu_6717double_conversion6Bignum19AssignDecimalStringENS0_6VectorIKcEE.cold:
.LFSB987:
.L179:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE987:
	.text
	.size	_ZN6icu_6717double_conversion6Bignum19AssignDecimalStringENS0_6VectorIKcEE, .-_ZN6icu_6717double_conversion6Bignum19AssignDecimalStringENS0_6VectorIKcEE
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion6Bignum19AssignDecimalStringENS0_6VectorIKcEE.cold, .-_ZN6icu_6717double_conversion6Bignum19AssignDecimalStringENS0_6VectorIKcEE.cold
.LCOLDE3:
	.text
.LHOTE3:
	.section	.text.unlikely
	.align 2
.LCOLDB4:
	.text
.LHOTB4:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_
	.type	_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_, @function
_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_:
.LFB992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movswl	2(%rdi), %r14d
	movswl	2(%rsi), %edi
	movzwl	(%rbx), %r12d
	cmpw	%di, %r14w
	jle	.L245
	movl	%r14d, %r15d
	subl	%edi, %r14d
	movswl	%r12w, %edi
	leal	(%r14,%rdi), %eax
	cmpl	$128, %eax
	jg	.L289
	movl	%edi, %edx
	subl	$1, %edx
	js	.L252
	leal	(%r14,%rdx), %ecx
	leal	-3(%rcx), %eax
	cmpl	%eax, %edx
	setl	%sil
	cmpl	$-1, %r14d
	setl	%al
	orb	%al, %sil
	je	.L249
	cmpl	$3, %edx
	jbe	.L249
	movswq	%r12w, %rcx
	movslq	%r14d, %rax
	addq	%rcx, %rax
	leaq	-12(%rbx,%rcx,4), %r8
	movl	%edi, %ecx
	shrl	$2, %ecx
	leaq	-12(%rbx,%rax,4), %rsi
	xorl	%eax, %eax
	negq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L251:
	movdqu	(%r8,%rax), %xmm0
	movups	%xmm0, (%rsi,%rax)
	subq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L251
	movl	%edi, %ecx
	movl	%edx, %eax
	andl	$-4, %ecx
	subl	%ecx, %eax
	cmpl	%edi, %ecx
	je	.L252
	movslq	%eax, %rdx
	movl	4(%rbx,%rdx,4), %ecx
	leal	(%rax,%r14), %edx
	movslq	%edx, %rdx
	movl	%ecx, 4(%rbx,%rdx,4)
	leal	-1(%rax), %edx
	testl	%eax, %eax
	je	.L252
	movslq	%edx, %rcx
	addl	%r14d, %edx
	movl	4(%rbx,%rcx,4), %ecx
	movslq	%edx, %rdx
	movl	%ecx, 4(%rbx,%rdx,4)
	leal	-2(%rax), %edx
	cmpl	$1, %eax
	je	.L252
	movslq	%edx, %rax
	addl	%r14d, %edx
	movl	4(%rbx,%rax,4), %eax
	movslq	%edx, %rdx
	movl	%eax, 4(%rbx,%rdx,4)
.L252:
	testl	%r14d, %r14d
	jle	.L256
	leal	-1(%r14), %eax
	leaq	4(%rbx), %rdi
	xorl	%esi, %esi
	leaq	4(,%rax,4), %rdx
	call	memset@PLT
.L256:
	addl	%r14d, %r12d
	subl	%r14d, %r15d
	movswl	%r15w, %r14d
	movw	%r12w, (%rbx)
	movw	%r14w, 2(%rbx)
	movswl	2(%r13), %edi
.L245:
	subl	%r14d, %edi
	movl	%edi, %r8d
	movswl	0(%r13), %edi
	testl	%edi, %edi
	jle	.L257
	movslq	%r8d, %rax
	xorl	%edx, %edx
	leaq	(%rbx,%rax,4), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L261:
	movl	4(%rcx,%rdx,4), %esi
	subl	%eax, %esi
	movl	%esi, %eax
	subl	4(%r13,%rdx,4), %eax
	movl	%eax, %esi
	shrl	$31, %eax
	andl	$268435455, %esi
	movl	%esi, 4(%rcx,%rdx,4)
	addq	$1, %rdx
	cmpl	%edx, %edi
	jg	.L261
	testl	%eax, %eax
	je	.L257
	addl	%r8d, %edi
	movslq	%edi, %rdi
	leaq	4(%rbx,%rdi,4), %rdx
	.p2align 4,,10
	.p2align 3
.L264:
	movl	(%rdx), %eax
	addq	$4, %rdx
	subl	$1, %eax
	movl	%eax, %ecx
	andl	$268435455, %ecx
	movl	%ecx, -4(%rdx)
	testl	%eax, %eax
	js	.L264
.L257:
	movswq	%r12w, %rax
	testw	%r12w, %r12w
	jg	.L267
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L293:
	leal	-1(%rax), %r12d
	subq	$1, %rax
	movw	%r12w, (%rbx)
	testw	%ax, %ax
	jle	.L263
.L267:
	movl	(%rbx,%rax,4), %edx
	testl	%edx, %edx
	je	.L293
.L244:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	testw	%r12w, %r12w
	jne	.L244
	xorl	%eax, %eax
	movw	%ax, 2(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L249:
	.cfi_restore_state
	movslq	%edx, %rax
	movslq	%ecx, %rcx
	movswq	%r12w, %rdx
	subq	%rdx, %rcx
	leaq	(%rbx,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L255:
	movl	4(%rbx,%rax,4), %edx
	movl	%edx, 8(%rcx,%rax,4)
	subq	$1, %rax
	testl	%eax, %eax
	jns	.L255
	jmp	.L252
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_.cold, @function
_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_.cold:
.LFSB992:
.L289:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE992:
	.text
	.size	_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_, .-_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_.cold, .-_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_.cold
.LCOLDE4:
	.text
.LHOTE4:
	.section	.text.unlikely
	.align 2
.LCOLDB5:
	.text
.LHOTB5:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi
	.type	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi, @function
_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi:
.LFB993:
	.cfi_startproc
	endbr64
	movzwl	(%rdi), %r9d
	testw	%r9w, %r9w
	je	.L306
	movslq	%esi, %rax
	movl	%esi, %edx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movswq	%r9w, %r12
	shrq	$32, %rax
	pushq	%rbx
	.cfi_offset 3, -32
	addl	%esi, %eax
	sarl	$4, %eax
	subl	%edx, %eax
	addw	%ax, 2(%rdi)
	imull	$28, %eax, %eax
	subl	%eax, %esi
	cmpw	$127, %r9w
	jg	.L304
	testl	%r12d, %r12d
	jle	.L294
	movl	$28, %r10d
	leal	-1(%r12), %eax
	leaq	4(%rdi), %rdx
	xorl	%ebx, %ebx
	subl	%esi, %r10d
	leaq	8(%rdi,%rax,4), %r11
	.p2align 4,,10
	.p2align 3
.L299:
	movl	(%rdx), %eax
	movl	%ebx, %r8d
	movl	%r10d, %ecx
	addq	$4, %rdx
	movl	%eax, %ebx
	shrl	%cl, %ebx
	movl	%esi, %ecx
	sall	%cl, %eax
	addl	%r8d, %eax
	andl	$268435455, %eax
	movl	%eax, -4(%rdx)
	cmpq	%r11, %rdx
	jne	.L299
	testl	%ebx, %ebx
	je	.L294
	addl	$1, %r9d
	movl	%ebx, 4(%rdi,%r12,4)
	movw	%r9w, (%rdi)
.L294:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi.cold, @function
_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi.cold:
.LFSB993:
.L304:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	call	abort@PLT
	.cfi_endproc
.LFE993:
	.text
	.size	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi, .-_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi.cold, .-_ZN6icu_6717double_conversion6Bignum9ShiftLeftEi.cold
.LCOLDE5:
	.text
.LHOTE5:
	.section	.text.unlikely
	.align 2
.LCOLDB6:
	.text
.LHOTB6:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej
	.type	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej, @function
_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej:
.LFB994:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	je	.L330
	testl	%esi, %esi
	je	.L332
	movswq	(%rdi), %rdx
	testw	%dx, %dx
	jle	.L333
	movswl	%dx, %eax
	movl	%esi, %r8d
	leaq	4(%rdi), %rcx
	subl	$1, %eax
	leaq	8(%rdi,%rax,4), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L317:
	movl	(%rcx), %esi
	addq	$4, %rcx
	imulq	%r8, %rsi
	addq	%rsi, %rax
	movl	%eax, %esi
	shrq	$28, %rax
	andl	$268435455, %esi
	movl	%esi, -4(%rcx)
	cmpq	%r9, %rcx
	jne	.L317
	testq	%rax, %rax
	je	.L334
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	.p2align 4,,10
	.p2align 3
.L319:
	cmpw	$127, %dx
	jg	.L326
	movl	%eax, %ecx
	andl	$268435455, %ecx
	movl	%ecx, 4(%rdi,%rdx,4)
	leal	1(%rdx), %ecx
	addq	$1, %rdx
	shrq	$28, %rax
	movw	%cx, (%rdi)
	jne	.L319
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore 6
	movl	$0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	ret
.L334:
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej.cold, @function
_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej.cold:
.LFSB994:
.L326:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE994:
	.text
	.size	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej, .-_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej.cold, .-_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt32Ej.cold
.LCOLDE6:
	.text
.LHOTE6:
	.section	.text.unlikely
	.align 2
.LCOLDB7:
	.text
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt64Em
	.type	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt64Em, @function
_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt64Em:
.LFB995:
	.cfi_startproc
	endbr64
	cmpq	$1, %rsi
	je	.L359
	testq	%rsi, %rsi
	je	.L361
	movzwl	(%rdi), %r8d
	testw	%r8w, %r8w
	je	.L359
	movl	%esi, %r10d
	shrq	$32, %rsi
	testw	%r8w, %r8w
	jle	.L362
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movswl	%r8w, %eax
	leaq	4(%rdi), %r9
	subl	$1, %eax
	leaq	8(%rdi,%rax,4), %r11
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	.p2align 4,,10
	.p2align 3
.L343:
	movl	(%r9), %edx
	movq	%r10, %rcx
	movq	%rax, %rbx
	shrq	$28, %rax
	andl	$268435455, %ebx
	addq	$4, %r9
	imulq	%rdx, %rcx
	imulq	%rsi, %rdx
	addq	%rbx, %rcx
	movl	%ecx, %ebx
	salq	$4, %rdx
	andl	$268435455, %ebx
	addq	%rax, %rdx
	shrq	$28, %rcx
	movl	%ebx, -4(%r9)
	leaq	(%rcx,%rdx), %rax
	cmpq	%r9, %r11
	jne	.L343
	movswq	%r8w, %rdx
	testq	%rax, %rax
	je	.L335
	.p2align 4,,10
	.p2align 3
.L345:
	cmpw	$127, %dx
	jg	.L355
	movl	%eax, %ecx
	andl	$268435455, %ecx
	movl	%ecx, 4(%rdi,%rdx,4)
	leal	1(%rdx), %ecx
	addq	$1, %rdx
	shrq	$28, %rax
	movw	%cx, (%rdi)
	jne	.L345
.L335:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore 3
	.cfi_restore 6
	movl	$0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt64Em.cold, @function
_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt64Em.cold:
.LFSB995:
.L355:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE995:
	.text
	.size	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt64Em, .-_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt64Em
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt64Em.cold, .-_ZN6icu_6717double_conversion6Bignum16MultiplyByUInt64Em.cold
.LCOLDE7:
	.text
.LHOTE7:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi
	.type	_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi, @function
_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi:
.LFB996:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L363
	cmpw	$0, (%rdi)
	je	.L363
	jmp	_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi.part.0
	.p2align 4,,10
	.p2align 3
.L363:
	ret
	.cfi_endproc
.LFE996:
	.size	_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi, .-_ZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEi
	.section	.text.unlikely
	.align 2
.LCOLDB8:
	.text
.LHOTB8:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum6SquareEv
	.type	_ZN6icu_6717double_conversion6Bignum6SquareEv, @function
_ZN6icu_6717double_conversion6Bignum6SquareEv:
.LFB997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movswl	(%rdi), %esi
	leal	(%rsi,%rsi), %ebx
	cmpl	$128, %ebx
	jg	.L370
	movl	%esi, %r10d
	cmpw	$255, %si
	jg	.L370
	testl	%esi, %esi
	jle	.L395
	leal	3(%rsi), %eax
	leal	-1(%rsi), %edx
	cmpl	$6, %eax
	jbe	.L372
	cmpl	$3, %edx
	jbe	.L372
	movl	%esi, %edx
	movswq	%si, %rcx
	xorl	%eax, %eax
	shrl	$2, %edx
	leaq	4(%rdi,%rcx,4), %r8
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L373:
	movdqu	4(%rdi,%rax), %xmm7
	movups	%xmm7, (%r8,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L373
	movl	%esi, %eax
	andl	$-4, %eax
	testb	$3, %sil
	je	.L375
	movslq	%eax, %rdx
	movl	4(%rdi,%rdx,4), %r8d
	leal	(%rsi,%rax), %edx
	movslq	%edx, %rdx
	movl	%r8d, 4(%rdi,%rdx,4)
	leal	1(%rax), %edx
	cmpl	%edx, %esi
	jle	.L375
	movslq	%edx, %r8
	addl	%esi, %edx
	addl	$2, %eax
	movl	4(%rdi,%r8,4), %r8d
	movslq	%edx, %rdx
	movl	%r8d, 4(%rdi,%rdx,4)
	cmpl	%eax, %esi
	jle	.L375
	movslq	%eax, %rdx
	addl	%esi, %eax
	movl	4(%rdi,%rdx,4), %edx
	cltq
	movl	%edx, 4(%rdi,%rax,4)
.L375:
	leaq	4(,%rcx,4), %r12
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	leaq	-12(%rdi,%r12), %r11
	addq	%rdi, %r12
	.p2align 4,,10
	.p2align 3
.L383:
	movl	%r9d, %r13d
	cmpl	$2, %r8d
	jle	.L396
	movl	%r9d, %eax
	movq	%r11, %r14
	pxor	%xmm3, %xmm3
	movq	%r12, %rcx
	shrl	$2, %eax
	salq	$4, %rax
	subq	%rax, %r14
	movq	%r11, %rax
	.p2align 4,,10
	.p2align 3
.L381:
	movdqu	(%rax), %xmm5
	movdqu	(%rcx), %xmm1
	subq	$16, %rax
	addq	$16, %rcx
	pshufd	$27, %xmm5, %xmm0
	movdqa	%xmm1, %xmm4
	movdqa	%xmm0, %xmm2
	punpckldq	%xmm1, %xmm4
	punpckhdq	%xmm1, %xmm1
	punpckldq	%xmm0, %xmm2
	punpckhdq	%xmm0, %xmm0
	pmuludq	%xmm4, %xmm2
	pmuludq	%xmm1, %xmm0
	paddq	%xmm2, %xmm0
	paddq	%xmm0, %xmm3
	cmpq	%r14, %rax
	jne	.L381
	movdqa	%xmm3, %xmm0
	movl	%r8d, %r14d
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm3
	movq	%xmm3, %rax
	addq	%rax, %rdx
	movl	%r13d, %eax
	andl	$-4, %eax
	subl	%eax, %r14d
	cmpl	%r13d, %eax
	je	.L382
.L380:
	leal	(%rsi,%rax), %ecx
	leal	(%rsi,%r14), %r13d
	movslq	%ecx, %rcx
	movslq	%r13d, %r13
	movl	4(%rdi,%r13,4), %r13d
	movl	4(%rdi,%rcx,4), %ecx
	imulq	%r13, %rcx
	leal	-1(%r14), %r13d
	addq	%rcx, %rdx
	leal	1(%rax), %ecx
	testl	%r14d, %r14d
	je	.L382
	addl	%esi, %r13d
	addl	%esi, %ecx
	addl	$2, %eax
	movslq	%r13d, %r13
	movslq	%ecx, %rcx
	movl	4(%rdi,%rcx,4), %ecx
	movl	4(%rdi,%r13,4), %r13d
	imulq	%rcx, %r13
	leal	-2(%r14), %ecx
	addq	%r13, %rdx
	cmpl	$1, %r14d
	je	.L382
	addl	%esi, %eax
	addl	%esi, %ecx
	cltq
	movslq	%ecx, %rcx
	movl	4(%rdi,%rax,4), %eax
	movl	4(%rdi,%rcx,4), %ecx
	imulq	%rcx, %rax
	addq	%rax, %rdx
.L382:
	movl	%edx, %eax
	addl	$1, %r8d
	shrq	$28, %rdx
	addq	$4, %r11
	andl	$268435455, %eax
	movl	%eax, (%rdi,%r9,4)
	addq	$1, %r9
	cmpl	%r8d, %esi
	jne	.L383
.L371:
	cmpl	%ebx, %esi
	jge	.L389
	movswq	%r10w, %rax
	leal	-1(%rsi), %r13d
	leal	-1(%rsi,%rsi), %r12d
	movl	$1, %r9d
	movl	%r13d, %r8d
	leaq	8(%rdi,%rax,4), %r10
	leaq	-12(%rdi,%rax,8), %r14
	subl	%ebx, %r12d
	.p2align 4,,10
	.p2align 3
.L390:
	movl	%r9d, %eax
	leal	-1(%r8), %r11d
	cmpl	%r9d, %esi
	jle	.L384
	cmpl	$2, %r11d
	jbe	.L397
	movl	%r8d, %r15d
	movq	%r14, %rcx
	pxor	%xmm3, %xmm3
	movq	%r10, %rax
	shrl	$2, %r15d
	salq	$4, %r15
	addq	%r10, %r15
	.p2align 4,,10
	.p2align 3
.L387:
	movdqu	(%rcx), %xmm6
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	subq	$16, %rcx
	pshufd	$27, %xmm6, %xmm1
	movdqa	%xmm0, %xmm2
	movdqa	%xmm1, %xmm4
	punpckhdq	%xmm0, %xmm2
	punpckldq	%xmm0, %xmm0
	punpckhdq	%xmm1, %xmm4
	punpckldq	%xmm1, %xmm1
	pmuludq	%xmm4, %xmm2
	pmuludq	%xmm1, %xmm0
	paddq	%xmm2, %xmm0
	paddq	%xmm0, %xmm3
	cmpq	%r15, %rax
	jne	.L387
	movdqa	%xmm3, %xmm0
	movl	%r8d, %r15d
	movl	%r13d, %ecx
	psrldq	$8, %xmm0
	andl	$-4, %r15d
	paddq	%xmm0, %xmm3
	subl	%r15d, %ecx
	movq	%xmm3, %rax
	addq	%rax, %rdx
	leal	(%r9,%r15), %eax
	cmpl	%r8d, %r15d
	je	.L384
.L385:
	leal	(%rsi,%rax), %r8d
	leal	(%rsi,%rcx), %r15d
	movslq	%r8d, %r8
	movslq	%r15d, %r15
	movl	4(%rdi,%r15,4), %r15d
	movl	4(%rdi,%r8,4), %r8d
	imulq	%r15, %r8
	leal	-1(%rcx), %r15d
	addq	%r8, %rdx
	leal	1(%rax), %r8d
	cmpl	%r8d, %esi
	jle	.L384
	addl	%esi, %r15d
	addl	%esi, %r8d
	addl	$2, %eax
	subl	$2, %ecx
	movslq	%r15d, %r15
	movslq	%r8d, %r8
	movl	4(%rdi,%r15,4), %r15d
	movl	4(%rdi,%r8,4), %r8d
	imulq	%r8, %r15
	addq	%r15, %rdx
	cmpl	%eax, %esi
	jle	.L384
	addl	%esi, %eax
	addl	%esi, %ecx
	cltq
	movslq	%ecx, %rcx
	movl	4(%rdi,%rax,4), %eax
	movl	4(%rdi,%rcx,4), %ecx
	imulq	%rcx, %rax
	addq	%rax, %rdx
.L384:
	movl	%edx, %eax
	addl	$1, %r9d
	shrq	$28, %rdx
	movl	%r11d, %r8d
	andl	$268435455, %eax
	addq	$4, %r10
	movl	%eax, -8(%r10)
	cmpl	%r12d, %r11d
	jne	.L390
.L389:
	movswq	%bx, %rax
	movw	%bx, (%rdi)
	movq	%rax, %rdx
	salw	2(%rdi)
	testw	%bx, %bx
	jg	.L393
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L417:
	leal	-1(%rax), %edx
	subq	$1, %rax
	movw	%dx, (%rdi)
	testw	%ax, %ax
	jle	.L379
.L393:
	movl	(%rdi,%rax,4), %edx
	testl	%edx, %edx
	je	.L417
.L368:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	movl	%r8d, %r14d
	xorl	%eax, %eax
	jmp	.L380
.L397:
	movl	%r13d, %ecx
	jmp	.L385
.L379:
	testw	%dx, %dx
	jne	.L368
	xorl	%eax, %eax
	movw	%ax, 2(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L372:
	.cfi_restore_state
	leaq	4(%rdi), %rax
	leaq	8(%rdi,%rdx,4), %r8
	movswq	%r10w, %rcx
	.p2align 4,,10
	.p2align 3
.L376:
	movl	(%rax), %edx
	movl	%edx, (%rax,%rcx,4)
	addq	$4, %rax
	cmpq	%r8, %rax
	jne	.L376
	jmp	.L375
.L395:
	xorl	%edx, %edx
	jmp	.L371
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion6Bignum6SquareEv.cold, @function
_ZN6icu_6717double_conversion6Bignum6SquareEv.cold:
.LFSB997:
.L370:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE997:
	.text
	.size	_ZN6icu_6717double_conversion6Bignum6SquareEv, .-_ZN6icu_6717double_conversion6Bignum6SquareEv
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion6Bignum6SquareEv.cold, .-_ZN6icu_6717double_conversion6Bignum6SquareEv.cold
.LCOLDE8:
	.text
.LHOTE8:
	.section	.text.unlikely
	.align 2
.LCOLDB9:
	.text
.LHOTB9:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum17AssignPowerUInt16Eti
	.type	_ZN6icu_6717double_conversion6Bignum17AssignPowerUInt16Eti, @function
_ZN6icu_6717double_conversion6Bignum17AssignPowerUInt16Eti:
.LFB998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%edx, %edx
	je	.L512
	movzwl	%si, %r8d
	movzwl	%si, %eax
	xorl	%r12d, %r12d
	andl	$1, %esi
	movl	$0, (%rdi)
	movl	%edx, %r14d
	jne	.L424
	.p2align 4,,10
	.p2align 3
.L421:
	sarl	%eax
	addl	$1, %r12d
	movzwl	%ax, %r8d
	testb	$1, %al
	je	.L421
.L424:
	xorl	%esi, %esi
	testl	%eax, %eax
	je	.L422
	.p2align 4,,10
	.p2align 3
.L423:
	addl	$1, %esi
	sarl	%eax
	jne	.L423
	movl	%r14d, %eax
	imull	%esi, %eax
	cmpl	$3555, %eax
	jg	.L443
.L422:
	movzwl	%r8w, %r13d
	testl	%r14d, %r14d
	jle	.L425
	movl	$1, %ebx
	.p2align 4,,10
	.p2align 3
.L426:
	addl	%ebx, %ebx
	cmpl	%ebx, %r14d
	jge	.L426
	sarl	$2, %ebx
	je	.L425
	movl	$64, %ecx
	movq	$-1, %r9
	movq	%r13, %rax
	subl	%esi, %ecx
	xorl	%esi, %esi
	salq	%cl, %r9
	movl	$4294967295, %ecx
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L513:
	imulq	%r13, %rax
.L431:
	sarl	%ebx
	cmpq	%rcx, %rax
	ja	.L432
.L514:
	testl	%ebx, %ebx
	je	.L432
.L433:
	imulq	%rax, %rax
	testl	%ebx, %r14d
	je	.L431
	testq	%rax, %r9
	je	.L513
	movl	$1, %esi
	sarl	%ebx
	cmpq	%rcx, %rax
	jbe	.L514
	.p2align 4,,10
	.p2align 3
.L432:
	cmpw	$1, %r8w
	movl	$1, %ecx
	sete	-49(%rbp)
	movzbl	-49(%rbp), %edx
	xorl	$1, %esi
	orl	%edx, %esi
	movl	%esi, %r8d
	testq	%rax, %rax
	je	.L515
	.p2align 4,,10
	.p2align 3
.L428:
	movl	%eax, %esi
	movq	%rcx, %r9
	andl	$268435455, %esi
	shrq	$28, %rax
	movl	%esi, (%r15,%rcx,4)
	movl	%ecx, %esi
	leaq	1(%r9), %rcx
	jne	.L428
	movw	%si, (%r15)
	testb	%r8b, %r8b
	je	.L516
	testl	%ebx, %ebx
	jne	.L456
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L516:
	testw	%si, %si
	je	.L429
	movswq	%si, %r8
	testl	%r8d, %r8d
	jle	.L438
	leal	-1(%r8), %ecx
	leaq	4(%r15), %r10
	leaq	8(%r15,%rcx,4), %r11
	.p2align 4,,10
	.p2align 3
.L442:
	movl	(%r10), %ecx
	addq	$4, %r10
	imulq	%r13, %rcx
	addq	%rcx, %rax
	movl	%eax, %ecx
	shrq	$28, %rax
	andl	$268435455, %ecx
	movl	%ecx, -4(%r10)
	cmpq	%r10, %r11
	jne	.L442
	testq	%rax, %rax
	je	.L438
	cmpw	$127, %si
	jg	.L443
	leal	1(%r9), %esi
	movl	%eax, 4(%r15,%r8,4)
	movw	%si, (%r15)
	testl	%ebx, %ebx
	je	.L444
.L456:
	leaq	8(%r15), %rax
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L453:
	movq	%r15, %rdi
	call	_ZN6icu_6717double_conversion6Bignum6SquareEv
	testl	%ebx, %r14d
	je	.L447
	cmpb	$0, -49(%rbp)
	jne	.L447
	movzwl	(%r15), %r8d
	testw	%r8w, %r8w
	je	.L447
	movswq	%r8w, %r9
	testl	%r9d, %r9d
	jle	.L447
	movq	-64(%rbp), %rdi
	leal	-1(%r9), %eax
	leaq	4(%r15), %rcx
	leaq	(%rdi,%rax,4), %r10
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L452:
	movl	(%rcx), %esi
	addq	$4, %rcx
	imulq	%r13, %rsi
	addq	%rsi, %rax
	movl	%eax, %esi
	shrq	$28, %rax
	andl	$268435455, %esi
	movl	%esi, -4(%rcx)
	cmpq	%rcx, %r10
	jne	.L452
	testq	%rax, %rax
	je	.L447
	cmpw	$127, %r8w
	jg	.L443
	addl	$1, %r8d
	movl	%eax, 4(%r15,%r9,4)
	movw	%r8w, (%r15)
.L447:
	sarl	%ebx
	jne	.L453
	movzwl	(%r15), %esi
.L436:
	testw	%si, %si
	je	.L418
.L444:
	imull	%r14d, %r12d
	movzwl	2(%r15), %edx
	movswq	%si, %r8
.L457:
	movslq	%r12d, %rax
	movl	%r12d, %ecx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%r12d, %eax
	sarl	$4, %eax
	subl	%ecx, %eax
	addl	%eax, %edx
	imull	$28, %eax, %eax
	movw	%dx, 2(%r15)
	subl	%eax, %r12d
	cmpw	$127, %si
	jg	.L443
	testl	%r8d, %r8d
	jle	.L418
	movl	$28, %edx
	leal	-1(%r8), %eax
	leaq	4(%r15), %r9
	xorl	%r10d, %r10d
	subl	%r12d, %edx
	leaq	8(%r15,%rax,4), %rbx
	.p2align 4,,10
	.p2align 3
.L455:
	movl	(%r9), %eax
	movl	%r10d, %r11d
	movl	%edx, %ecx
	addq	$4, %r9
	movl	%eax, %r10d
	shrl	%cl, %r10d
	movl	%r12d, %ecx
	sall	%cl, %eax
	addl	%r11d, %eax
	andl	$268435455, %eax
	movl	%eax, -4(%r9)
	cmpq	%rbx, %r9
	jne	.L455
	testl	%r10d, %r10d
	je	.L418
	addl	$1, %esi
	movl	%r10d, 4(%r15,%r8,4)
	movw	%si, (%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	.cfi_restore_state
	movabsq	$4294967297, %rax
	movq	%rax, (%rdi)
.L418:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	testb	%sil, %sil
	jne	.L429
	movb	$0, -49(%rbp)
.L429:
	testl	%ebx, %ebx
	jne	.L456
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L425:
	movl	$1, %eax
	movl	%r8d, 4(%r15)
	movl	$1, %esi
	movw	%ax, (%r15)
	jmp	.L444
.L438:
	testl	%ebx, %ebx
	jne	.L456
	imull	%r14d, %r12d
	xorl	%edx, %edx
	jmp	.L457
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion6Bignum17AssignPowerUInt16Eti.cold, @function
_ZN6icu_6717double_conversion6Bignum17AssignPowerUInt16Eti.cold:
.LFSB998:
.L443:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE998:
	.text
	.size	_ZN6icu_6717double_conversion6Bignum17AssignPowerUInt16Eti, .-_ZN6icu_6717double_conversion6Bignum17AssignPowerUInt16Eti
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion6Bignum17AssignPowerUInt16Eti.cold, .-_ZN6icu_6717double_conversion6Bignum17AssignPowerUInt16Eti.cold
.LCOLDE9:
	.text
.LHOTE9:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717double_conversion6Bignum11ToHexStringEPci
	.type	_ZNK6icu_6717double_conversion6Bignum11ToHexStringEPci, @function
_ZNK6icu_6717double_conversion6Bignum11ToHexStringEPci:
.LFB1002:
	.cfi_startproc
	endbr64
	movswl	(%rdi), %eax
	testw	%ax, %ax
	jne	.L518
	cmpl	$1, %edx
	jle	.L517
	movl	$48, %eax
	movw	%ax, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	movswl	2(%rdi), %ecx
	leal	-1(%rax,%rcx), %ecx
	subl	$1, %eax
	cltq
	leal	0(,%rcx,8), %r8d
	movl	4(%rdi,%rax,4), %eax
	subl	%ecx, %r8d
	movl	%r8d, %ecx
	testl	%eax, %eax
	je	.L520
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L521:
	addl	$1, %ecx
	shrl	$4, %eax
	jne	.L521
	addl	%r8d, %ecx
.L520:
	leal	1(%rcx), %r8d
	xorl	%eax, %eax
	cmpl	%edx, %r8d
	jle	.L564
.L517:
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	leal	-1(%rcx), %r8d
	movslq	%ecx, %rcx
	xorl	%edx, %edx
	movslq	%r8d, %rax
	movb	$0, (%rsi,%rcx)
	addq	%rsi, %rax
	cmpw	$0, 2(%rdi)
	jle	.L525
	.p2align 4,,10
	.p2align 3
.L526:
	movb	$48, (%rax)
	addl	$1, %edx
	subl	$7, %r8d
	subq	$7, %rax
	movb	$48, 6(%rax)
	movb	$48, 5(%rax)
	movb	$48, 4(%rax)
	movb	$48, 3(%rax)
	movb	$48, 2(%rax)
	movb	$48, 1(%rax)
	movswl	2(%rdi), %ecx
	cmpl	%edx, %ecx
	jg	.L526
.L525:
	movswl	(%rdi), %eax
	movslq	%r8d, %rdx
	xorl	%r9d, %r9d
	addq	%rsi, %rdx
	subl	$1, %eax
	testl	%eax, %eax
	jg	.L541
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L565:
	addl	$55, %eax
.L563:
	movb	%al, -6(%rdx)
	movswl	(%rdi), %eax
	addq	$1, %r9
	subq	$7, %rdx
	subl	$1, %eax
	cmpl	%r9d, %eax
	jle	.L523
.L541:
	movl	4(%rdi,%r9,4), %eax
	movl	%eax, %r10d
	andl	$15, %r10d
	cmpl	$10, %r10d
	leal	55(%r10), %r11d
	leal	48(%r10), %ecx
	cmovge	%r11d, %ecx
	movb	%cl, (%rdx)
	movl	%eax, %ecx
	shrl	$4, %ecx
	andl	$15, %ecx
	leal	55(%rcx), %r11d
	cmpl	$10, %ecx
	leal	48(%rcx), %r10d
	movl	%r11d, %ecx
	cmovl	%r10d, %ecx
	movb	%cl, -1(%rdx)
	movl	%eax, %ecx
	shrl	$8, %ecx
	andl	$15, %ecx
	leal	55(%rcx), %r11d
	cmpl	$10, %ecx
	leal	48(%rcx), %r10d
	movl	%r11d, %ecx
	cmovl	%r10d, %ecx
	movb	%cl, -2(%rdx)
	movl	%eax, %ecx
	shrl	$12, %ecx
	andl	$15, %ecx
	leal	55(%rcx), %r11d
	cmpl	$10, %ecx
	leal	48(%rcx), %r10d
	movl	%r11d, %ecx
	cmovl	%r10d, %ecx
	movb	%cl, -3(%rdx)
	movl	%eax, %ecx
	shrl	$16, %ecx
	andl	$15, %ecx
	leal	55(%rcx), %r11d
	cmpl	$10, %ecx
	leal	48(%rcx), %r10d
	movl	%r11d, %ecx
	cmovl	%r10d, %ecx
	movb	%cl, -4(%rdx)
	movl	%eax, %ecx
	shrl	$20, %ecx
	andl	$15, %ecx
	leal	55(%rcx), %r11d
	cmpl	$10, %ecx
	leal	48(%rcx), %r10d
	movl	%r11d, %ecx
	cmovl	%r10d, %ecx
	shrl	$24, %eax
	subl	$7, %r8d
	andl	$15, %eax
	movb	%cl, -5(%rdx)
	cmpl	$9, %eax
	jg	.L565
	addl	$48, %eax
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L523:
	cltq
	movslq	%r8d, %r8
	movl	4(%rdi,%rax,4), %edx
	addq	%rsi, %r8
	testl	%edx, %edx
	je	.L544
	.p2align 4,,10
	.p2align 3
.L546:
	movl	%edx, %eax
	andl	$15, %eax
	cmpl	$9, %eax
	jle	.L566
	addl	$55, %eax
	subq	$1, %r8
	movb	%al, 1(%r8)
	shrl	$4, %edx
	jne	.L546
.L544:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	addl	$48, %eax
	subq	$1, %r8
	movb	%al, 1(%r8)
	shrl	$4, %edx
	jne	.L546
	jmp	.L544
	.cfi_endproc
.LFE1002:
	.size	_ZNK6icu_6717double_conversion6Bignum11ToHexStringEPci, .-_ZNK6icu_6717double_conversion6Bignum11ToHexStringEPci
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717double_conversion6Bignum11BigitOrZeroEi
	.type	_ZNK6icu_6717double_conversion6Bignum11BigitOrZeroEi, @function
_ZNK6icu_6717double_conversion6Bignum11BigitOrZeroEi:
.LFB1003:
	.cfi_startproc
	endbr64
	movswl	2(%rdi), %edx
	movswl	(%rdi), %eax
	addl	%edx, %eax
	cmpl	%eax, %esi
	jge	.L569
	cmpl	%edx, %esi
	jl	.L569
	subl	%edx, %esi
	movslq	%esi, %rsi
	movl	4(%rdi,%rsi,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1003:
	.size	_ZNK6icu_6717double_conversion6Bignum11BigitOrZeroEi, .-_ZNK6icu_6717double_conversion6Bignum11BigitOrZeroEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum7CompareERKS1_S3_
	.type	_ZN6icu_6717double_conversion6Bignum7CompareERKS1_S3_, @function
_ZN6icu_6717double_conversion6Bignum7CompareERKS1_S3_:
.LFB1004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movswl	(%rdi), %r11d
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movswl	2(%rdi), %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movswl	2(%rsi), %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movswl	(%rsi), %ebx
	addl	%r13d, %r11d
	addl	%r12d, %ebx
	cmpl	%r11d, %ebx
	jg	.L570
	movl	$1, %eax
	jl	.L570
	movl	%r13d, %r10d
	cmpw	%r12w, %r13w
	leal	-1(%r11), %edx
	cmovg	%r12d, %r10d
	cmpl	%r10d, %edx
	jl	.L578
	movl	%eax, %r14d
	leal	-2(%r11), %r9d
	subl	$1, %r10d
	subl	%r13d, %eax
	subl	%r12d, %r14d
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L586:
	cmpl	%edx, %r13d
	jg	.L572
	leal	(%rax,%r9), %r8d
	movslq	%r8d, %r8
	movl	4(%rdi,%r8,4), %r15d
	xorl	%r8d, %r8d
	testb	%cl, %cl
	je	.L585
	cmpl	%r8d, %r15d
	ja	.L581
.L575:
	subl	$1, %edx
	subl	$1, %r9d
	cmpl	%r10d, %edx
	je	.L578
.L574:
	cmpl	%edx, %ebx
	setle	%r8b
	cmpl	%edx, %r12d
	setg	%cl
	orl	%r8d, %ecx
	cmpl	%edx, %r11d
	jg	.L586
.L572:
	testb	%cl, %cl
	jne	.L575
	leal	(%r14,%r9), %ecx
	movslq	%ecx, %rcx
	movl	4(%rsi,%rcx,4), %ecx
	testl	%ecx, %ecx
	je	.L575
.L582:
	movl	$-1, %eax
.L570:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L585:
	.cfi_restore_state
	leal	(%r14,%r9), %ecx
	movslq	%ecx, %rcx
	movl	4(%rsi,%rcx,4), %r8d
	cmpl	%r15d, %r8d
	ja	.L582
	cmpl	%r8d, %r15d
	jbe	.L575
.L581:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L578:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1004:
	.size	_ZN6icu_6717double_conversion6Bignum7CompareERKS1_S3_, .-_ZN6icu_6717double_conversion6Bignum7CompareERKS1_S3_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_
	.type	_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_, @function
_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_:
.LFB1006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzwl	(%rdi), %r10d
	movzwl	2(%rdi), %r9d
	movzwl	(%rsi), %r8d
	movzwl	2(%rsi), %r11d
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L598:
	movl	%r9d, %eax
	movl	%r11d, %r9d
	movl	%r13d, %r10d
	movl	%eax, %r11d
	movq	%rdi, %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
.L588:
	movswl	%r8w, %ecx
	movswl	%r9w, %ebx
	movswl	%r10w, %eax
	movswl	%r11w, %r12d
	addl	%ebx, %eax
	addl	%r12d, %ecx
	movl	%r8d, %r13d
	movl	%r10d, %r8d
	cmpl	%ecx, %eax
	jl	.L598
	movswl	2(%rdx), %r13d
	movswl	(%rdx), %r10d
	leal	1(%rax), %r15d
	movl	$-1, %r14d
	addl	%r13d, %r10d
	movl	%r13d, %r8d
	cmpl	%r10d, %r15d
	jl	.L587
	movl	$1, %r14d
	cmpl	%eax, %r10d
	jl	.L587
	cmpl	%ecx, %ebx
	jl	.L608
	movl	$-1, %r14d
	cmpl	%eax, %r10d
	jg	.L587
.L608:
	movl	%ebx, %r14d
	cmpw	%r11w, %r9w
	jle	.L591
	movl	%r12d, %r14d
	movl	%r11d, %r9d
.L591:
	cmpw	%r9w, %r8w
	leal	-1(%r10), %r8d
	cmovl	%r13d, %r14d
	cmpl	%r14d, %r8d
	jl	.L604
	xorl	%r9d, %r9d
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L615:
	cmpl	%r8d, %eax
	jle	.L605
	movl	%r8d, %r11d
	subl	%ebx, %r11d
	movslq	%r11d, %r11
	movl	4(%rdi,%r11,4), %r11d
.L594:
	cmpl	%r8d, %r12d
	jg	.L595
	cmpl	%r8d, %ecx
	jle	.L595
	movl	%r8d, %r15d
	subl	%r12d, %r15d
	movslq	%r15d, %r15
	addl	4(%rsi,%r15,4), %r11d
.L595:
	cmpl	%r8d, %r10d
	jle	.L596
	cmpl	%r8d, %r13d
	jg	.L596
	movl	%r8d, %r15d
	subl	%r13d, %r15d
	movslq	%r15d, %r15
	addl	4(%rdx,%r15,4), %r9d
.L596:
	cmpl	%r9d, %r11d
	ja	.L606
	subl	%r11d, %r9d
	cmpl	$1, %r9d
	ja	.L607
	subl	$1, %r8d
	sall	$28, %r9d
	cmpl	%r14d, %r8d
	jl	.L614
.L597:
	cmpl	%r8d, %ebx
	jle	.L615
.L605:
	xorl	%r11d, %r11d
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L607:
	movl	$-1, %r14d
.L587:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_restore_state
	movl	$1, %r14d
	popq	%rbx
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	xorl	%r14d, %r14d
	testl	%r9d, %r9d
	popq	%rbx
	popq	%r12
	setne	%r14b
	popq	%r13
	negl	%r14d
	movl	%r14d, %eax
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L604:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L587
	.cfi_endproc
.LFE1006:
	.size	_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_, .-_ZN6icu_6717double_conversion6Bignum11PlusCompareERKS1_S3_S3_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum5ClampEv
	.type	_ZN6icu_6717double_conversion6Bignum5ClampEv, @function
_ZN6icu_6717double_conversion6Bignum5ClampEv:
.LFB1007:
	.cfi_startproc
	endbr64
	movzwl	(%rdi), %edx
	testw	%dx, %dx
	jle	.L617
	movswq	%dx, %rax
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L623:
	leal	-1(%rax), %edx
	subq	$1, %rax
	movw	%dx, (%rdi)
	testw	%ax, %ax
	jle	.L617
.L620:
	movl	(%rdi,%rax,4), %edx
	testl	%edx, %edx
	je	.L623
.L616:
	ret
	.p2align 4,,10
	.p2align 3
.L617:
	testw	%dx, %dx
	jne	.L616
	xorl	%eax, %eax
	movw	%ax, 2(%rdi)
	ret
	.cfi_endproc
.LFE1007:
	.size	_ZN6icu_6717double_conversion6Bignum5ClampEv, .-_ZN6icu_6717double_conversion6Bignum5ClampEv
	.section	.text.unlikely
	.align 2
.LCOLDB10:
	.text
.LHOTB10:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum5AlignERKS1_
	.type	_ZN6icu_6717double_conversion6Bignum5AlignERKS1_, @function
_ZN6icu_6717double_conversion6Bignum5AlignERKS1_:
.LFB1008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzwl	2(%rdi), %r13d
	movswl	2(%rsi), %eax
	cmpw	%ax, %r13w
	jle	.L624
	movq	%rdi, %rbx
	movswl	%r13w, %r12d
	movswl	(%rdi), %edi
	subl	%eax, %r12d
	leal	(%rdi,%r12), %eax
	movl	%edi, %r14d
	cmpl	$128, %eax
	jg	.L649
	movl	%edi, %edx
	subl	$1, %edx
	js	.L633
	leal	(%r12,%rdx), %ecx
	leal	-3(%rcx), %eax
	cmpl	%eax, %edx
	setl	%sil
	cmpl	$-1, %r12d
	setl	%al
	orb	%al, %sil
	je	.L630
	cmpl	$3, %edx
	jbe	.L630
	movswq	%di, %rcx
	movslq	%r12d, %rax
	addq	%rcx, %rax
	leaq	-12(%rbx,%rcx,4), %r8
	movl	%edi, %ecx
	shrl	$2, %ecx
	leaq	-12(%rbx,%rax,4), %rsi
	xorl	%eax, %eax
	negq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L632:
	movdqu	(%r8,%rax), %xmm0
	movups	%xmm0, (%rsi,%rax)
	subq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L632
	movl	%edi, %ecx
	movl	%edx, %eax
	andl	$-4, %ecx
	subl	%ecx, %eax
	cmpl	%ecx, %edi
	je	.L633
	movslq	%eax, %rdx
	movl	4(%rbx,%rdx,4), %ecx
	leal	(%r12,%rax), %edx
	movslq	%edx, %rdx
	movl	%ecx, 4(%rbx,%rdx,4)
	leal	-1(%rax), %edx
	testl	%eax, %eax
	je	.L633
	movslq	%edx, %rcx
	addl	%r12d, %edx
	movl	4(%rbx,%rcx,4), %ecx
	movslq	%edx, %rdx
	movl	%ecx, 4(%rbx,%rdx,4)
	leal	-2(%rax), %edx
	cmpl	$1, %eax
	je	.L633
	movslq	%edx, %rax
	addl	%r12d, %edx
	movl	4(%rbx,%rax,4), %eax
	movslq	%edx, %rdx
	movl	%eax, 4(%rbx,%rdx,4)
.L633:
	testl	%r12d, %r12d
	jle	.L628
	leal	-1(%r12), %eax
	leaq	4(%rbx), %rdi
	xorl	%esi, %esi
	leaq	4(,%rax,4), %rdx
	call	memset@PLT
.L628:
	addl	%r12d, %r14d
	subl	%r12d, %r13d
	movw	%r14w, (%rbx)
	movw	%r13w, 2(%rbx)
.L624:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L630:
	.cfi_restore_state
	movslq	%edx, %rax
	movslq	%ecx, %rcx
	movswq	%r14w, %rdx
	subq	%rdx, %rcx
	leaq	(%rbx,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L636:
	movl	4(%rbx,%rax,4), %edx
	movl	%edx, 8(%rcx,%rax,4)
	subq	$1, %rax
	testl	%eax, %eax
	jns	.L636
	jmp	.L633
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion6Bignum5AlignERKS1_.cold, @function
_ZN6icu_6717double_conversion6Bignum5AlignERKS1_.cold:
.LFSB1008:
.L649:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	call	abort@PLT
	.cfi_endproc
.LFE1008:
	.text
	.size	_ZN6icu_6717double_conversion6Bignum5AlignERKS1_, .-_ZN6icu_6717double_conversion6Bignum5AlignERKS1_
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion6Bignum5AlignERKS1_.cold, .-_ZN6icu_6717double_conversion6Bignum5AlignERKS1_.cold
.LCOLDE10:
	.text
.LHOTE10:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum15BigitsShiftLeftEi
	.type	_ZN6icu_6717double_conversion6Bignum15BigitsShiftLeftEi, @function
_ZN6icu_6717double_conversion6Bignum15BigitsShiftLeftEi:
.LFB1009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movswl	(%rdi), %ebx
	testl	%ebx, %ebx
	jle	.L652
	movl	$28, %r10d
	leal	-1(%rbx), %eax
	movl	%ebx, %r12d
	xorl	%r8d, %r8d
	subl	%esi, %r10d
	leaq	4(%rdi), %rdx
	leaq	8(%rdi,%rax,4), %r11
	.p2align 4,,10
	.p2align 3
.L655:
	movl	(%rdx), %eax
	movl	%r8d, %r9d
	movl	%r10d, %ecx
	addq	$4, %rdx
	movl	%eax, %r8d
	shrl	%cl, %r8d
	movl	%esi, %ecx
	sall	%cl, %eax
	addl	%r9d, %eax
	andl	$268435455, %eax
	movl	%eax, -4(%rdx)
	cmpq	%r11, %rdx
	jne	.L655
	testl	%r8d, %r8d
	je	.L652
	movslq	%ebx, %rbx
	addl	$1, %r12d
	movl	%r8d, 4(%rdi,%rbx,4)
	movw	%r12w, (%rdi)
.L652:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1009:
	.size	_ZN6icu_6717double_conversion6Bignum15BigitsShiftLeftEi, .-_ZN6icu_6717double_conversion6Bignum15BigitsShiftLeftEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum13SubtractTimesERKS1_i
	.type	_ZN6icu_6717double_conversion6Bignum13SubtractTimesERKS1_i, @function
_ZN6icu_6717double_conversion6Bignum13SubtractTimesERKS1_i:
.LFB1010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$8, %rsp
	cmpl	$2, %edx
	jle	.L690
	movswl	2(%rsi), %eax
	movswl	2(%rdi), %edx
	movswl	(%rsi), %r9d
	movswl	(%rdi), %r10d
	subl	%edx, %eax
	leal	(%rax,%r9), %r11d
	movl	%r10d, %esi
	testl	%r9d, %r9d
	jle	.L668
	cltq
	movslq	%ebx, %r8
	xorl	%ecx, %ecx
	leaq	(%rdi,%rax,4), %rdi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L670:
	movl	4(%r13,%rcx,4), %edx
	movl	4(%rdi,%rcx,4), %ebx
	imulq	%r8, %rdx
	addq	%rax, %rdx
	movl	%edx, %eax
	shrq	$28, %rdx
	andl	$268435455, %eax
	subl	%eax, %ebx
	movl	%ebx, %eax
	andl	$268435455, %ebx
	movl	%ebx, 4(%rdi,%rcx,4)
	shrl	$31, %eax
	addq	$1, %rcx
	addl	%edx, %eax
	cmpl	%ecx, %r9d
	jg	.L670
	cmpl	%r10d, %r11d
	jge	.L671
	testl	%eax, %eax
	je	.L662
	subl	$1, %r10d
	movslq	%r11d, %rcx
	subl	%r11d, %r10d
	leaq	(%r12,%rcx,4), %rdx
	addq	%rcx, %r10
	leaq	(%r12,%r10,4), %rdi
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L688:
	addq	$4, %rdx
	testl	%eax, %eax
	je	.L662
.L672:
	movl	4(%rdx), %ebx
	subl	%eax, %ebx
	movl	%ebx, %ecx
	movl	%ebx, %eax
	andl	$268435455, %ecx
	shrl	$31, %eax
	movl	%ecx, 4(%rdx)
	cmpq	%rdx, %rdi
	jne	.L688
.L671:
	movswq	%si, %rax
	testw	%si, %si
	jg	.L676
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L691:
	leal	-1(%rax), %esi
	subq	$1, %rax
	movw	%si, (%r12)
	testw	%ax, %ax
	jle	.L674
.L676:
	movl	(%r12,%rax,4), %edx
	testl	%edx, %edx
	je	.L691
.L662:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L690:
	.cfi_restore_state
	testl	%edx, %edx
	jle	.L662
	call	_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_
	cmpl	$1, %ebx
	je	.L662
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	testw	%si, %si
	jne	.L662
	xorl	%eax, %eax
	movw	%ax, 2(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L668:
	.cfi_restore_state
	cmpl	%r10d, %r11d
	jl	.L662
	jmp	.L671
	.cfi_endproc
.LFE1010:
	.size	_ZN6icu_6717double_conversion6Bignum13SubtractTimesERKS1_i, .-_ZN6icu_6717double_conversion6Bignum13SubtractTimesERKS1_i
	.section	.text.unlikely
	.align 2
.LCOLDB11:
	.text
.LHOTB11:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_
	.type	_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_, @function
_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_:
.LFB999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzwl	(%rsi), %esi
	movswl	2(%rdi), %eax
	movswl	(%rdi), %r9d
	movswl	2(%r13), %r10d
	movswl	%si, %r8d
	movw	%ax, -58(%rbp)
	leal	(%rax,%r9), %ecx
	leal	(%r10,%r8), %edx
	movw	%r8w, -62(%rbp)
	cmpl	%edx, %ecx
	jge	.L768
.L692:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L768:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	%r9d, %r12d
	cmpw	%r10w, %ax
	jle	.L694
	subl	%r10d, %eax
	movl	%eax, %r14d
	leal	(%r9,%rax), %eax
	cmpl	$128, %eax
	jg	.L757
	movl	%r9d, %edx
	subl	$1, %edx
	js	.L701
	leal	(%r14,%rdx), %ecx
	leal	-3(%rcx), %eax
	cmpl	%eax, %edx
	setl	%sil
	cmpl	$-1, %r14d
	setl	%al
	orb	%al, %sil
	je	.L698
	cmpl	$3, %edx
	jbe	.L698
	movswq	%r9w, %rcx
	movslq	%r14d, %rax
	addq	%rcx, %rax
	leaq	-12(%rbx,%rcx,4), %rdi
	movl	%r9d, %ecx
	shrl	$2, %ecx
	leaq	-12(%rbx,%rax,4), %rsi
	xorl	%eax, %eax
	negq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L700:
	movdqu	(%rdi,%rax), %xmm0
	movups	%xmm0, (%rsi,%rax)
	subq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L700
	movl	%r9d, %ecx
	movl	%edx, %eax
	andl	$-4, %ecx
	subl	%ecx, %eax
	cmpl	%ecx, %r9d
	je	.L701
	movslq	%eax, %rdx
	movl	4(%rbx,%rdx,4), %ecx
	leal	(%r14,%rax), %edx
	movslq	%edx, %rdx
	movl	%ecx, 4(%rbx,%rdx,4)
	leal	-1(%rax), %edx
	testl	%eax, %eax
	je	.L701
	movslq	%edx, %rcx
	addl	%r14d, %edx
	movl	4(%rbx,%rcx,4), %ecx
	movslq	%edx, %rdx
	movl	%ecx, 4(%rbx,%rdx,4)
	leal	-2(%rax), %edx
	cmpl	$1, %eax
	je	.L701
	movslq	%edx, %rax
	addl	%r14d, %edx
	movl	4(%rbx,%rax,4), %eax
	movslq	%edx, %rdx
	movl	%eax, 4(%rbx,%rdx,4)
.L701:
	testl	%r14d, %r14d
	jle	.L705
	leal	-1(%r14), %eax
	leaq	4(%rbx), %rdi
	xorl	%esi, %esi
	leaq	4(,%rax,4), %rdx
	call	memset@PLT
.L705:
	subw	%r14w, -58(%rbp)
	movswl	-58(%rbp), %eax
	addl	%r14d, %r12d
	movw	%r12w, (%rbx)
	movswl	%r12w, %r9d
	movw	%ax, 2(%rbx)
	movswl	0(%r13), %r8d
	leal	(%r9,%rax), %ecx
	movswl	2(%r13), %r10d
	movw	%r8w, -62(%rbp)
	leal	(%r10,%r8), %edx
.L694:
	xorl	%r14d, %r14d
	movw	%r14w, -60(%rbp)
	cmpl	%edx, %ecx
	jle	.L707
	.p2align 4,,10
	.p2align 3
.L706:
	leal	-1(%r9), %edi
	movslq	%edi, %rdx
	movl	4(%rbx,%rdx,4), %r15d
	addw	%r15w, -60(%rbp)
	cmpl	$2, %r15d
	jle	.L769
	movl	%r10d, %esi
	subl	%eax, %esi
	movslq	%esi, %rax
	leal	(%rax,%r8), %r14d
	testl	%r8d, %r8d
	jle	.L712
	leal	-1(%r8), %edx
	movslq	%r15d, %rsi
	leaq	(%rbx,%rax,4), %r11
	leaq	2(%rdx), %rcx
	xorl	%edx, %edx
	movq	%rcx, -56(%rbp)
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L714:
	movl	0(%r13,%rcx,4), %eax
	imulq	%rsi, %rax
	movq	%rax, %r15
	movl	%edx, %eax
	addq	%r15, %rax
	movl	(%r11,%rcx,4), %r15d
	movl	%eax, %edx
	shrq	$28, %rax
	andl	$268435455, %edx
	subl	%edx, %r15d
	movl	%r15d, %edx
	andl	$268435455, %r15d
	shrl	$31, %edx
	movl	%r15d, (%r11,%rcx,4)
	addq	$1, %rcx
	addl	%eax, %edx
	cmpq	%rcx, -56(%rbp)
	jne	.L714
	cmpl	%r14d, %r9d
	jle	.L715
	testl	%edx, %edx
	je	.L766
	movslq	%r14d, %rax
	subl	%r14d, %edi
	addq	%rax, %rdi
	leaq	(%rbx,%rax,4), %rcx
	leaq	(%rbx,%rdi,4), %rsi
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L758:
	addq	$4, %rcx
	testl	%edx, %edx
	je	.L766
.L719:
	movl	4(%rcx), %eax
	subl	%edx, %eax
	movl	%eax, %edx
	andl	$268435455, %eax
	movl	%eax, 4(%rcx)
	shrl	$31, %edx
	cmpq	%rcx, %rsi
	jne	.L758
.L715:
	movswq	%r12w, %rax
	testw	%r12w, %r12w
	jg	.L722
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L721:
	leal	-1(%rax), %r12d
	subq	$1, %rax
	movw	%r12w, (%rbx)
	testw	%ax, %ax
	jle	.L770
.L722:
	movl	(%rbx,%rax,4), %r11d
	movl	%eax, %r12d
	testl	%r11d, %r11d
	je	.L721
	movswl	0(%r13), %r8d
	movswl	-58(%rbp), %eax
	movswl	%r12w, %r9d
	movswl	2(%r13), %r10d
	movw	%r8w, -62(%rbp)
	.p2align 4,,10
	.p2align 3
.L711:
	leal	(%r8,%r10), %ecx
	leal	(%r9,%rax), %edx
	cmpl	%edx, %ecx
	jl	.L706
.L707:
	subl	$1, %r8d
	movslq	%r9d, %r9
	cmpw	$1, -62(%rbp)
	leaq	(%rbx,%r9,4), %rsi
	movslq	%r8d, %r8
	movl	(%rsi), %r15d
	movl	4(%r13,%r8,4), %ecx
	je	.L771
	leal	1(%rcx), %esi
	movl	%r15d, %eax
	xorl	%edx, %edx
	movzwl	-60(%rbp), %r14d
	divl	%esi
	movl	%ecx, -56(%rbp)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	%eax, %edx
	movl	%eax, %r12d
	addl	%eax, %r14d
	call	_ZN6icu_6717double_conversion6Bignum13SubtractTimesERKS1_i
	movl	-56(%rbp), %ecx
	addl	$1, %r12d
	imull	%ecx, %r12d
	cmpl	%r15d, %r12d
	ja	.L692
	.p2align 4,,10
	.p2align 3
.L731:
	movswl	2(%rbx), %r10d
	movswl	2(%r13), %r9d
	movswl	0(%r13), %edi
	movswl	(%rbx), %r8d
	movl	%r10d, %esi
	addl	%r9d, %edi
	addl	%r10d, %r8d
	cmpl	%r8d, %edi
	jl	.L727
	jg	.L692
	cmpw	%r10w, %r9w
	leal	-1(%rdi), %eax
	cmovle	%r9d, %esi
	cmpl	%esi, %eax
	jl	.L727
	movl	%r9d, %ecx
	movl	%r9d, %r12d
	subl	$1, %esi
	notl	%ecx
	subl	%r10d, %r12d
	addl	%edi, %ecx
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L773:
	cmpl	%edi, %eax
	jge	.L728
	movslq	%ecx, %r11
	movl	4(%r13,%r11,4), %r15d
	xorl	%r11d, %r11d
	testb	%dl, %dl
	je	.L772
.L729:
	cmpl	%r11d, %r15d
	ja	.L692
.L732:
	subl	$1, %eax
	subl	$1, %ecx
	cmpl	%esi, %eax
	je	.L727
.L730:
	cmpl	%r10d, %eax
	setl	%r11b
	cmpl	%r8d, %eax
	setge	%dl
	orl	%r11d, %edx
	cmpl	%r9d, %eax
	jge	.L773
.L728:
	testb	%dl, %dl
	jne	.L732
	leal	(%r12,%rcx), %edx
	movslq	%edx, %rdx
	movl	4(%rbx,%rdx,4), %edx
	testl	%edx, %edx
	je	.L732
	.p2align 4,,10
	.p2align 3
.L727:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	addl	$1, %r14d
	call	_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L712:
	cmpl	%r14d, %r9d
	jle	.L715
	.p2align 4,,10
	.p2align 3
.L766:
	movswl	-58(%rbp), %eax
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L772:
	leal	(%r12,%rcx), %edx
	movslq	%edx, %rdx
	movl	4(%rbx,%rdx,4), %r11d
	cmpl	%r11d, %r15d
	jnb	.L729
	movq	%r13, %rsi
	movq	%rbx, %rdi
	addl	$1, %r14d
	call	_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L769:
	movswl	-58(%rbp), %eax
	testl	%r15d, %r15d
	jle	.L711
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_
	cmpl	$1, %r15d
	je	.L710
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6717double_conversion6Bignum14SubtractBignumERKS1_
.L710:
	movswl	(%rbx), %r9d
	movswl	2(%rbx), %eax
	movswl	0(%r13), %r8d
	movswl	2(%r13), %r10d
	movw	%ax, -58(%rbp)
	movl	%r9d, %r12d
	movw	%r8w, -62(%rbp)
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L770:
	movswl	%r12w, %r9d
.L718:
	testw	%r12w, %r12w
	jne	.L774
	xorl	%edi, %edi
	xorl	%r10d, %r10d
	movw	%di, 2(%rbx)
	movzwl	0(%r13), %eax
	movw	%r10w, -58(%rbp)
	movswl	2(%r13), %r10d
	movswl	%ax, %r8d
	xorl	%eax, %eax
	movw	%r8w, -62(%rbp)
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L771:
	movl	%r15d, %eax
	xorl	%edx, %edx
	movzwl	-60(%rbp), %r14d
	divl	%ecx
	movl	%edx, (%rsi)
	addl	%eax, %r14d
	testw	%r12w, %r12w
	jle	.L725
	movswq	%r12w, %rax
	.p2align 4,,10
	.p2align 3
.L726:
	movl	(%rbx,%rax,4), %esi
	testl	%esi, %esi
	jne	.L692
	leal	-1(%rax), %r12d
	subq	$1, %rax
	movw	%r12w, (%rbx)
	testw	%ax, %ax
	jg	.L726
.L725:
	testw	%r12w, %r12w
	jne	.L692
	xorl	%ecx, %ecx
	movw	%cx, 2(%rbx)
	jmp	.L692
.L698:
	movslq	%edx, %rax
	movslq	%ecx, %rcx
	movswq	%r12w, %rdx
	subq	%rdx, %rcx
	leaq	(%rbx,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L704:
	movl	4(%rbx,%rax,4), %edx
	movl	%edx, 8(%rcx,%rax,4)
	subq	$1, %rax
	testl	%eax, %eax
	jns	.L704
	jmp	.L701
.L774:
	movswl	0(%r13), %r8d
	movswl	-58(%rbp), %eax
	movswl	2(%r13), %r10d
	movw	%r8w, -62(%rbp)
	jmp	.L711
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_.cold, @function
_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_.cold:
.LFSB999:
.L757:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE999:
	.text
	.size	_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_, .-_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_
	.section	.text.unlikely
	.size	_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_.cold, .-_ZN6icu_6717double_conversion6Bignum21DivideModuloIntBignumERKS1_.cold
.LCOLDE11:
	.text
.LHOTE11:
	.section	.rodata
	.align 32
	.type	_ZZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEiE12kFive1_to_12, @object
	.size	_ZZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEiE12kFive1_to_12, 48
_ZZN6icu_6717double_conversion6Bignum20MultiplyByPowerOfTenEiE12kFive1_to_12:
	.long	5
	.long	25
	.long	125
	.long	625
	.long	3125
	.long	15625
	.long	78125
	.long	390625
	.long	1953125
	.long	9765625
	.long	48828125
	.long	244140625
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
