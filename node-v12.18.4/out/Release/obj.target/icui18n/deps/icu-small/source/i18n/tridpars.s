	.file	"tridpars.cpp"
	.text
	.p2align 4
	.type	_deleteTransliteratorTrIDPars, @function
_deleteTransliteratorTrIDPars:
.LFB3149:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.cfi_endproc
.LFE3149:
	.size	_deleteTransliteratorTrIDPars, .-_deleteTransliteratorTrIDPars
	.p2align 4
	.type	_deleteSingleID, @function
_deleteSingleID:
.LFB3148:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L4
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	136(%rdi), %rdi
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	ret
	.cfi_endproc
.LFE3148:
	.size	_deleteSingleID, .-_deleteSingleID
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser5SpecsC2ERKNS_13UnicodeStringES4_S4_aS4_
	.type	_ZN6icu_6722TransliteratorIDParser5SpecsC2ERKNS_13UnicodeStringES4_S4_aS4_, @function
_ZN6icu_6722TransliteratorIDParser5SpecsC2ERKNS_13UnicodeStringES4_S4_aS4_:
.LFB3130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	$2, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	movl	$2, %ecx
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	movl	$2, %r8d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movw	%dx, 16(%rdi)
	movw	%cx, 80(%rdi)
	movq	%rax, 8(%rdi)
	movq	%rax, 72(%rdi)
	movq	%rax, 136(%rdi)
	movl	$2, %edi
	movw	%r8w, 208(%rbx)
	movw	%di, 144(%rbx)
	leaq	8(%rbx), %rdi
	movq	%rax, 200(%rbx)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	72(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	136(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movb	%r13b, 264(%rbx)
	addq	$8, %rsp
	movq	%r12, %rsi
	leaq	200(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	.cfi_endproc
.LFE3130:
	.size	_ZN6icu_6722TransliteratorIDParser5SpecsC2ERKNS_13UnicodeStringES4_S4_aS4_, .-_ZN6icu_6722TransliteratorIDParser5SpecsC2ERKNS_13UnicodeStringES4_S4_aS4_
	.globl	_ZN6icu_6722TransliteratorIDParser5SpecsC1ERKNS_13UnicodeStringES4_S4_aS4_
	.set	_ZN6icu_6722TransliteratorIDParser5SpecsC1ERKNS_13UnicodeStringES4_S4_aS4_,_ZN6icu_6722TransliteratorIDParser5SpecsC2ERKNS_13UnicodeStringES4_S4_aS4_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser8SingleIDC2ERKNS_13UnicodeStringES4_S4_
	.type	_ZN6icu_6722TransliteratorIDParser8SingleIDC2ERKNS_13UnicodeStringES4_S4_, @function
_ZN6icu_6722TransliteratorIDParser8SingleIDC2ERKNS_13UnicodeStringES4_S4_:
.LFB3133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	movl	$2, %edx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	movl	$2, %ecx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movw	%dx, 16(%rdi)
	movw	%cx, 80(%rdi)
	movq	%rax, 8(%rdi)
	movq	%rax, 72(%rdi)
	movq	%rax, 136(%rdi)
	movl	$2, %edi
	movw	%di, 144(%rbx)
	leaq	8(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	72(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	addq	$8, %rsp
	leaq	136(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	.cfi_endproc
.LFE3133:
	.size	_ZN6icu_6722TransliteratorIDParser8SingleIDC2ERKNS_13UnicodeStringES4_S4_, .-_ZN6icu_6722TransliteratorIDParser8SingleIDC2ERKNS_13UnicodeStringES4_S4_
	.globl	_ZN6icu_6722TransliteratorIDParser8SingleIDC1ERKNS_13UnicodeStringES4_S4_
	.set	_ZN6icu_6722TransliteratorIDParser8SingleIDC1ERKNS_13UnicodeStringES4_S4_,_ZN6icu_6722TransliteratorIDParser8SingleIDC2ERKNS_13UnicodeStringES4_S4_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser8SingleIDC2ERKNS_13UnicodeStringES4_
	.type	_ZN6icu_6722TransliteratorIDParser8SingleIDC2ERKNS_13UnicodeStringES4_, @function
_ZN6icu_6722TransliteratorIDParser8SingleIDC2ERKNS_13UnicodeStringES4_:
.LFB3136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	movl	$2, %edx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, 8(%rdi)
	movw	%dx, 16(%rdi)
	movq	%rax, 72(%rdi)
	movw	%cx, 80(%rdi)
	movq	%rax, 136(%rdi)
	movl	$2, %edi
	movw	%di, 144(%rbx)
	leaq	8(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	72(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	.cfi_endproc
.LFE3136:
	.size	_ZN6icu_6722TransliteratorIDParser8SingleIDC2ERKNS_13UnicodeStringES4_, .-_ZN6icu_6722TransliteratorIDParser8SingleIDC2ERKNS_13UnicodeStringES4_
	.globl	_ZN6icu_6722TransliteratorIDParser8SingleIDC1ERKNS_13UnicodeStringES4_
	.set	_ZN6icu_6722TransliteratorIDParser8SingleIDC1ERKNS_13UnicodeStringES4_,_ZN6icu_6722TransliteratorIDParser8SingleIDC2ERKNS_13UnicodeStringES4_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser7IDtoSTVERKNS_13UnicodeStringERS1_S4_S4_Ra
	.type	_ZN6icu_6722TransliteratorIDParser7IDtoSTVERKNS_13UnicodeStringERS1_S4_S4_Ra, @function
_ZN6icu_6722TransliteratorIDParser7IDtoSTVERKNS_13UnicodeStringERS1_S4_S4_Ra:
.LFB3152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	8(%r14), %edx
	testw	%dx, %dx
	js	.L16
	sarl	$5, %edx
.L17:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	_ZN6icu_67L3ANYE(%rip), %rcx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movzwl	8(%r15), %eax
	testb	$1, %al
	je	.L18
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L19:
	movzwl	8(%r13), %eax
	testb	$1, %al
	je	.L22
.L63:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L23:
	movswl	8(%r12), %ecx
	testw	%cx, %cx
	js	.L26
.L64:
	sarl	$5, %ecx
.L27:
	xorl	%edx, %edx
	movl	$45, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movswl	8(%r12), %ecx
	movl	%eax, %ebx
	testw	%cx, %cx
	js	.L28
	sarl	$5, %ecx
.L29:
	xorl	%edx, %edx
	movl	$47, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	js	.L59
.L30:
	movq	-56(%rbp), %rax
	movb	$0, (%rax)
	movq	(%r12), %rax
	testl	%ebx, %ebx
	js	.L60
	movq	24(%rax), %r10
	cmpl	%ebx, %r8d
	jle	.L36
	testl	%ebx, %ebx
	jne	.L61
.L37:
	movl	%r8d, -56(%rbp)
	leal	1(%rbx), %esi
	movq	%r15, %rcx
	movl	%r8d, %edx
	movq	%r12, %rdi
	call	*%r10
.L58:
	movswl	8(%r12), %edx
	movq	(%r12), %rax
	movl	-56(%rbp), %r8d
	testw	%dx, %dx
	movq	24(%rax), %rax
	js	.L38
	sarl	$5, %edx
.L39:
	movq	%r13, %rcx
	movl	%r8d, %esi
	movq	%r12, %rdi
	call	*%rax
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L43
.L65:
	sarl	$5, %eax
	testl	%eax, %eax
	jg	.L62
.L15:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L20
	movswl	%ax, %edx
	sarl	$5, %edx
.L21:
	testl	%edx, %edx
	je	.L19
	andl	$31, %eax
	movw	%ax, 8(%r15)
	movzwl	8(%r13), %eax
	testb	$1, %al
	jne	.L63
.L22:
	testw	%ax, %ax
	js	.L24
	movswl	%ax, %edx
	sarl	$5, %edx
.L25:
	testl	%edx, %edx
	je	.L23
	andl	$31, %eax
	movw	%ax, 8(%r13)
	movswl	8(%r12), %ecx
	testw	%cx, %cx
	jns	.L64
.L26:
	movl	12(%r12), %ecx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L36:
	testl	%r8d, %r8d
	jle	.L40
	movl	%r8d, -60(%rbp)
	movl	%r8d, %edx
	movq	%r14, %rcx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*%r10
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %r8d
	movb	$1, (%rax)
	movq	(%r12), %rax
	movq	24(%rax), %r10
.L40:
	movl	%ebx, %edx
	movq	%r13, %rcx
	movl	%r8d, %esi
	movq	%r12, %rdi
	call	*%r10
	movq	(%r12), %rax
	movswl	8(%r12), %edx
	leal	1(%rbx), %r14d
	movq	24(%rax), %rax
	testw	%dx, %dx
	js	.L41
	sarl	$5, %edx
.L42:
	movq	%r15, %rcx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*%rax
	movswl	8(%r13), %eax
	testw	%ax, %ax
	jns	.L65
.L43:
	movl	12(%r13), %eax
	testl	%eax, %eax
	jle	.L15
.L62:
	addq	$24, %rsp
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	popq	%rbx
	xorl	%ecx, %ecx
	popq	%r12
	movl	$1, %edx
	popq	%r13
	xorl	%esi, %esi
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movl	12(%r13), %edx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L20:
	movl	12(%r15), %edx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L16:
	movl	12(%r14), %edx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L60:
	movl	%r8d, -56(%rbp)
	movq	%r15, %rcx
	movl	%r8d, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L59:
	movswl	8(%r12), %r8d
	testw	%r8w, %r8w
	js	.L31
	sarl	$5, %r8d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L28:
	movl	12(%r12), %ecx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L38:
	movl	12(%r12), %edx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L61:
	movl	%r8d, -60(%rbp)
	movq	%r14, %rcx
	movl	%ebx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*%r10
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %r8d
	movb	$1, (%rax)
	movq	(%r12), %rax
	movq	24(%rax), %r10
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L41:
	movl	12(%r12), %edx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L31:
	movl	12(%r12), %r8d
	jmp	.L30
	.cfi_endproc
.LFE3152:
	.size	_ZN6icu_6722TransliteratorIDParser7IDtoSTVERKNS_13UnicodeStringERS1_S4_S4_Ra, .-_ZN6icu_6722TransliteratorIDParser7IDtoSTVERKNS_13UnicodeStringERS1_S4_S4_Ra
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser7STVtoIDERKNS_13UnicodeStringES3_S3_RS1_
	.type	_ZN6icu_6722TransliteratorIDParser7STVtoIDERKNS_13UnicodeStringES3_S3_RS1_, @function
_ZN6icu_6722TransliteratorIDParser7STVtoIDERKNS_13UnicodeStringES3_S3_RS1_:
.LFB3153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rdi, %rsi
	pushq	%r14
	movq	%rcx, %rdi
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L67
	sarl	$5, %eax
	testl	%eax, %eax
	je	.L101
.L69:
	movl	$45, %ecx
	leaq	-42(%rbp), %r14
	movq	%r12, %rdi
	xorl	%edx, %edx
	movw	%cx, -42(%rbp)
	movq	%r14, %rsi
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	8(%r15), %ecx
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L72
	sarl	$5, %ecx
.L73:
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L74
	sarl	$5, %eax
	testl	%eax, %eax
	jne	.L102
.L76:
	xorl	%eax, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L79
	movswl	%ax, %ecx
	sarl	$5, %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L100
	testl	%edx, %edx
	je	.L87
.L100:
	cmpl	%ecx, %edx
	jb	.L86
.L66:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, 8(%r12)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L67:
	movl	12(%r12), %eax
	testl	%eax, %eax
	jne	.L69
.L101:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L70
	sarl	$5, %edx
.L71:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	_ZN6icu_67L3ANYE(%rip), %rcx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L79:
	movl	12(%r12), %ecx
	leal	-1(%rcx), %edx
	testb	$1, %al
	je	.L83
	testl	%edx, %edx
	je	.L87
.L83:
	cmpl	%edx, %ecx
	jbe	.L66
	cmpl	$1023, %edx
	jle	.L86
	orl	$-32, %eax
	movl	%edx, 12(%r12)
	movw	%ax, 8(%r12)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L74:
	movl	12(%r13), %eax
	testl	%eax, %eax
	je	.L76
.L102:
	movl	$47, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movw	%dx, -42(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	8(%r13), %ecx
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L77
	sarl	$5, %ecx
.L78:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L72:
	movl	12(%r15), %ecx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L77:
	movl	12(%r13), %ecx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L70:
	movl	12(%r12), %edx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L66
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3153:
	.size	_ZN6icu_6722TransliteratorIDParser7STVtoIDERKNS_13UnicodeStringES3_S3_RS1_, .-_ZN6icu_6722TransliteratorIDParser7STVtoIDERKNS_13UnicodeStringES3_S3_RS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser22registerSpecialInverseERKNS_13UnicodeStringES3_aR10UErrorCode
	.type	_ZN6icu_6722TransliteratorIDParser22registerSpecialInverseERKNS_13UnicodeStringES3_aR10UErrorCode, @function
_ZN6icu_6722TransliteratorIDParser22registerSpecialInverseERKNS_13UnicodeStringES3_aR10UErrorCode:
.LFB3154:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r9d
	testl	%r9d, %r9d
	jle	.L159
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movl	_ZN6icu_67L24gSpecialInversesInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L160
.L106:
	movl	4+_ZN6icu_67L24gSpecialInversesInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L110
	movl	%eax, (%r12)
.L104:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	leaq	_ZN6icu_67L24gSpecialInversesInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L106
	movq	utrans_transliterator_cleanup_67@GOTPCREL(%rip), %rsi
	movl	$5, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L107
	movl	(%r12), %r8d
	movq	$0, (%rax)
	testl	%r8d, %r8d
	jle	.L161
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	xorl	%edi, %edi
.L108:
	movq	%r9, _ZN6icu_67L16SPECIAL_INVERSESE(%rip)
	call	uhash_setValueDeleter_67@PLT
.L127:
	movl	(%r12), %eax
	leaq	_ZN6icu_67L24gSpecialInversesInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L24gSpecialInversesInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L110:
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L104
	testb	%bl, %bl
	jne	.L111
.L122:
	xorl	%r15d, %r15d
.L112:
	leaq	_ZN6icu_67L4LOCKE(%rip), %rdi
	call	umtx_lock_67@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L123
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	_ZN6icu_67L16SPECIAL_INVERSESE(%rip), %rax
	movl	$64, %edi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L124
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-64(%rbp), %r8
.L124:
	movq	-56(%rbp), %rax
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r8, %rsi
	movq	(%rax), %rdi
	call	uhash_put_67@PLT
	testb	%r15b, %r15b
	je	.L125
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L123
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$64, %edi
	movq	_ZN6icu_67L16SPECIAL_INVERSESE(%rip), %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L126
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L126:
	movq	(%rbx), %rdi
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	uhash_put_67@PLT
.L125:
	leaq	-40(%rbp), %rsp
	leaq	_ZN6icu_67L4LOCKE(%rip), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movzwl	8(%r13), %esi
	testw	%si, %si
	js	.L113
	movswl	%si, %ecx
	sarl	$5, %ecx
.L114:
	movzwl	8(%r14), %eax
	testw	%ax, %ax
	js	.L115
	movswl	%ax, %edx
	sarl	$5, %edx
.L116:
	testb	$1, %sil
	je	.L117
	notl	%eax
	andl	$1, %eax
.L118:
	testb	%al, %al
	je	.L122
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L117:
	testl	%ecx, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L119
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L119:
	andl	$2, %esi
	leaq	10(%r13), %rcx
	jne	.L121
	movq	24(%r13), %rcx
.L121:
	subq	$8, %rsp
	xorl	%esi, %esi
	movq	%r14, %rdi
	pushq	$0
	call	_ZNK6icu_6713UnicodeString13doCaseCompareEiiPKDsiij@PLT
	popq	%rdx
	popq	%rcx
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L115:
	movl	12(%r14), %edx
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L113:
	movl	12(%r13), %ecx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L161:
	movq	uhash_compareCaselessUnicodeString_67@GOTPCREL(%rip), %rdx
	leaq	8(%rax), %rdi
	xorl	%ecx, %ecx
	movq	%r12, %r8
	movq	uhash_hashCaselessUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%rdi, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	uhash_init_67@PLT
	movl	(%r12), %edi
	movq	-64(%rbp), %r9
	testl	%edi, %edi
	movq	-56(%rbp), %rdi
	jle	.L109
	movq	(%r9), %rdi
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	jmp	.L108
.L109:
	movq	%rdi, (%r9)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r9, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	uhash_setKeyDeleter_67@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rsi
	movq	(%r9), %rdi
	jmp	.L108
.L123:
	movl	$7, (%r12)
	jmp	.L125
.L107:
	movq	$0, _ZN6icu_67L16SPECIAL_INVERSESE(%rip)
	movl	$7, (%r12)
	jmp	.L127
	.cfi_endproc
.LFE3154:
	.size	_ZN6icu_6722TransliteratorIDParser22registerSpecialInverseERKNS_13UnicodeStringES3_aR10UErrorCode, .-_ZN6icu_6722TransliteratorIDParser22registerSpecialInverseERKNS_13UnicodeStringES3_aR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser9specsToIDEPKNS0_5SpecsEi
	.type	_ZN6icu_6722TransliteratorIDParser9specsToIDEPKNS0_5SpecsEi, @function
_ZN6icu_6722TransliteratorIDParser9specsToIDEPKNS0_5SpecsEi:
.LFB3156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$2, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	$2, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	$2, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	subq	$312, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -320(%rbp)
	movw	%r12w, -312(%rbp)
	movq	%rbx, -256(%rbp)
	movw	%r13w, -248(%rbp)
	movq	%rbx, -192(%rbp)
	movw	%r14w, -184(%rbp)
	testq	%rdi, %rdi
	je	.L201
	movq	%rbx, -128(%rbp)
	movq	%rdi, %r14
	leaq	72(%rdi), %r9
	leaq	8(%rdi), %r8
	movl	$2, %r11d
	movw	%r11w, -120(%rbp)
	testl	%esi, %esi
	je	.L202
	movswl	80(%rdi), %ecx
	testw	%cx, %cx
	js	.L172
	sarl	$5, %ecx
.L173:
	leaq	-128(%rbp), %r12
	xorl	%edx, %edx
	movq	%r9, %rsi
	movq	%r8, -344(%rbp)
	movq	%r12, %rdi
	leaq	-322(%rbp), %r13
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$45, %r8d
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movl	$1, %ecx
	movw	%r8w, -322(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	16(%r14), %ecx
	movq	-344(%rbp), %r8
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L174
	sarl	$5, %ecx
.L175:
	xorl	%edx, %edx
	movq	%r8, %rsi
	leaq	-192(%rbp), %r15
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	144(%r14), %eax
	testw	%ax, %ax
	js	.L176
.L205:
	sarl	$5, %eax
	testl	%eax, %eax
	jne	.L203
.L178:
	leaq	-256(%rbp), %r13
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L181
	sarl	$5, %ecx
.L182:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	208(%r14), %r9d
	testw	%r9w, %r9w
	js	.L183
	sarl	$5, %r9d
	jne	.L186
.L185:
	leaq	-320(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L163:
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L187
	movq	%rbx, 8(%rax)
	movl	$2, %edx
	movl	$2, %eax
	movq	%r14, %rsi
	movl	$2, %ecx
	movq	%rbx, 72(%r12)
	leaq	8(%r12), %rdi
	movw	%ax, 16(%r12)
	movw	%dx, 80(%r12)
	movq	%rbx, 136(%r12)
	movw	%cx, 144(%r12)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	72(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L187:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	addq	$312, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	cmpb	$0, 264(%rdi)
	je	.L165
	movswl	16(%rdi), %ecx
	testw	%cx, %cx
	js	.L166
	sarl	$5, %ecx
.L167:
	leaq	-128(%rbp), %r12
	movq	%r8, %rsi
	xorl	%edx, %edx
	movq	%r9, -344(%rbp)
	movq	%r12, %rdi
	leaq	-322(%rbp), %r13
	leaq	-192(%rbp), %r15
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$45, %r10d
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movl	$1, %ecx
	movw	%r10w, -322(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-344(%rbp), %r9
.L168:
	movswl	80(%r14), %ecx
	testw	%cx, %cx
	js	.L169
	sarl	$5, %ecx
.L170:
	xorl	%edx, %edx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	144(%r14), %eax
	testw	%ax, %ax
	jns	.L205
.L176:
	movl	148(%r14), %eax
	testl	%eax, %eax
	je	.L178
.L203:
	movl	$47, %esi
	movl	$1, %ecx
	movq	%r12, %rdi
	xorl	%edx, %edx
	movw	%si, -322(%rbp)
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	144(%r14), %ecx
	leaq	136(%r14), %rsi
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L179
	sarl	$5, %ecx
.L180:
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L201:
	leaq	-192(%rbp), %r15
	leaq	-256(%rbp), %r13
	leaq	-320(%rbp), %r14
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L183:
	movl	212(%r14), %r9d
	testl	%r9d, %r9d
	je	.L185
.L186:
	leaq	200(%r14), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L181:
	movl	-116(%rbp), %ecx
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L166:
	movl	20(%rdi), %ecx
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L179:
	movl	148(%r14), %ecx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L172:
	movl	84(%rdi), %ecx
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L174:
	movl	20(%r14), %ecx
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L169:
	movl	84(%r14), %ecx
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L165:
	leaq	-192(%rbp), %r15
	movq	%r8, %rsi
	leaq	-128(%rbp), %r12
	movq	%r9, -344(%rbp)
	movq	%r15, %rdi
	leaq	-322(%rbp), %r13
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	$45, %r9d
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	movw	%r9w, -322(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-344(%rbp), %r9
	jmp	.L168
.L204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3156:
	.size	_ZN6icu_6722TransliteratorIDParser9specsToIDEPKNS0_5SpecsEi, .-_ZN6icu_6722TransliteratorIDParser9specsToIDEPKNS0_5SpecsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser21specsToSpecialInverseERKNS0_5SpecsER10UErrorCode
	.type	_ZN6icu_6722TransliteratorIDParser21specsToSpecialInverseERKNS0_5SpecsER10UErrorCode, @function
_ZN6icu_6722TransliteratorIDParser21specsToSpecialInverseERKNS0_5SpecsER10UErrorCode:
.LFB3157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$184, %rsp
	movswl	16(%rbx), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testw	%dx, %dx
	js	.L207
	sarl	$5, %edx
.L208:
	subq	$8, %rsp
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	pushq	$0
	leaq	_ZN6icu_67L3ANYE(%rip), %rcx
	leaq	_ZN6icu_67L3ANYE(%rip), %r13
	call	_ZNK6icu_6713UnicodeString13doCaseCompareEiiPKDsiij@PLT
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	jne	.L209
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L255
.L209:
	xorl	%r12d, %r12d
.L206:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L256
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movl	_ZN6icu_67L24gSpecialInversesInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L257
.L210:
	movl	4+_ZN6icu_67L24gSpecialInversesInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L214
	movl	%eax, (%r12)
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L257:
	leaq	_ZN6icu_67L24gSpecialInversesInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L210
	movq	utrans_transliterator_cleanup_67@GOTPCREL(%rip), %rsi
	movl	$5, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L211
	movq	$0, (%rax)
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L258
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	xorl	%edi, %edi
.L212:
	movq	%r14, _ZN6icu_67L16SPECIAL_INVERSESE(%rip)
	call	uhash_setValueDeleter_67@PLT
.L231:
	movl	(%r12), %eax
	leaq	_ZN6icu_67L24gSpecialInversesInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L24gSpecialInversesInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L214:
	movl	(%r12), %r12d
	testl	%r12d, %r12d
	jg	.L209
	leaq	_ZN6icu_67L4LOCKE(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	_ZN6icu_67L16SPECIAL_INVERSESE(%rip), %rax
	leaq	72(%rbx), %rsi
	movq	(%rax), %rdi
	call	uhash_get_67@PLT
	leaq	_ZN6icu_67L4LOCKE(%rip), %rdi
	movq	%rax, %r14
	call	umtx_unlock_67@PLT
	testq	%r14, %r14
	je	.L209
	movswl	208(%rbx), %ecx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r11d
	movq	%rax, -192(%rbp)
	movw	%r11w, -184(%rbp)
	testw	%cx, %cx
	js	.L215
	sarl	$5, %ecx
	leaq	-192(%rbp), %r15
	testl	%ecx, %ecx
	jne	.L259
.L217:
	cmpb	$0, 264(%rbx)
	leaq	-200(%rbp), %r12
	jne	.L260
.L218:
	movswl	8(%r14), %ecx
	testw	%cx, %cx
	js	.L219
.L262:
	sarl	$5, %ecx
.L220:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r13, -200(%rbp)
	leaq	-128(%rbp), %r13
	movl	$3, %ecx
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$45, %r9d
	movl	$1, %ecx
	movq	%r13, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movw	%r9w, -200(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	8(%r14), %ecx
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L221
	sarl	$5, %ecx
.L222:
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movswl	144(%rbx), %eax
	testw	%ax, %ax
	js	.L223
	sarl	$5, %eax
	testl	%eax, %eax
	jne	.L261
.L225:
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L230
	movl	$2, %edx
	movl	$2, %ecx
	movl	$2, %esi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, 16(%r12)
	leaq	8(%r12), %rdi
	movw	%si, 144(%r12)
	movq	%r15, %rsi
	movq	%rax, 8(%r12)
	movq	%rax, 72(%r12)
	movw	%cx, 80(%r12)
	movq	%rax, 136(%r12)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	72(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L230:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L207:
	movl	20(%rbx), %edx
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L215:
	movl	212(%rbx), %ecx
	leaq	-192(%rbp), %r15
	testl	%ecx, %ecx
	je	.L217
.L259:
	xorl	%edx, %edx
	leaq	200(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	cmpb	$0, 264(%rbx)
	leaq	-200(%rbp), %r12
	je	.L218
.L260:
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	$3, %ecx
	leaq	_ZN6icu_67L3ANYE(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$45, %r10d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movl	$1, %ecx
	movw	%r10w, -200(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	8(%r14), %ecx
	testw	%cx, %cx
	jns	.L262
.L219:
	movl	12(%r14), %ecx
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L223:
	movl	148(%rbx), %eax
	testl	%eax, %eax
	je	.L225
.L261:
	movl	$47, %r8d
	movl	$1, %ecx
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movw	%r8w, -200(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	144(%rbx), %ecx
	leaq	136(%rbx), %r8
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L226
	sarl	$5, %ecx
.L227:
	movq	%r8, %rsi
	xorl	%edx, %edx
	movq	%r8, -216(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$47, %edi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movw	%di, -200(%rbp)
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	144(%rbx), %ecx
	movq	-216(%rbp), %r8
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L228
	sarl	$5, %ecx
.L229:
	xorl	%edx, %edx
	movq	%r8, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L221:
	movl	12(%r14), %ecx
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L258:
	movq	uhash_compareCaselessUnicodeString_67@GOTPCREL(%rip), %rdx
	leaq	8(%r14), %r15
	movq	%r12, %r8
	xorl	%ecx, %ecx
	movq	uhash_hashCaselessUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	call	uhash_init_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L213
	movq	(%r14), %rdi
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L226:
	movl	148(%rbx), %ecx
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L228:
	movl	148(%rbx), %ecx
	jmp	.L229
.L213:
	movq	%r15, (%r14)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	uhash_setKeyDeleter_67@PLT
	movq	(%r14), %rdi
	movq	-216(%rbp), %rsi
	jmp	.L212
.L256:
	call	__stack_chk_fail@PLT
.L211:
	movq	$0, _ZN6icu_67L16SPECIAL_INVERSESE(%rip)
	movl	$7, (%r12)
	jmp	.L231
	.cfi_endproc
.LFE3157:
	.size	_ZN6icu_6722TransliteratorIDParser21specsToSpecialInverseERKNS0_5SpecsER10UErrorCode, .-_ZN6icu_6722TransliteratorIDParser21specsToSpecialInverseERKNS0_5SpecsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser19createBasicInstanceERKNS_13UnicodeStringEPS2_
	.type	_ZN6icu_6722TransliteratorIDParser19createBasicInstanceERKNS_13UnicodeStringEPS2_, @function
_ZN6icu_6722TransliteratorIDParser19createBasicInstanceERKNS_13UnicodeStringEPS2_:
.LFB3158:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6714Transliterator19createBasicInstanceERKNS_13UnicodeStringEPS2_@PLT
	.cfi_endproc
.LFE3158:
	.size	_ZN6icu_6722TransliteratorIDParser19createBasicInstanceERKNS_13UnicodeStringEPS2_, .-_ZN6icu_6722TransliteratorIDParser19createBasicInstanceERKNS_13UnicodeStringEPS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser4initER10UErrorCode
	.type	_ZN6icu_6722TransliteratorIDParser4initER10UErrorCode, @function
_ZN6icu_6722TransliteratorIDParser4initER10UErrorCode:
.LFB3159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$5, %edi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	utrans_transliterator_cleanup_67@GOTPCREL(%rip), %rsi
	call	ucln_i18n_registerCleanup_67@PLT
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L265
	movl	(%r12), %edx
	movq	$0, (%rax)
	movq	%rax, %rbx
	testl	%edx, %edx
	jle	.L272
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	xorl	%edi, %edi
.L266:
	movq	%rbx, _ZN6icu_67L16SPECIAL_INVERSESE(%rip)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uhash_setValueDeleter_67@PLT
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	leaq	8(%rax), %r13
	movq	uhash_compareCaselessUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	%r12, %r8
	xorl	%ecx, %ecx
	movq	uhash_hashCaselessUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	uhash_init_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L267
	movq	(%rbx), %rdi
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%r13, (%rbx)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	movq	%rsi, -40(%rbp)
	call	uhash_setKeyDeleter_67@PLT
	movq	(%rbx), %rdi
	movq	-40(%rbp), %rsi
	jmp	.L266
.L265:
	movl	$7, (%r12)
	movq	$0, _ZN6icu_67L16SPECIAL_INVERSESE(%rip)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3159:
	.size	_ZN6icu_6722TransliteratorIDParser4initER10UErrorCode, .-_ZN6icu_6722TransliteratorIDParser4initER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser7cleanupEv
	.type	_ZN6icu_6722TransliteratorIDParser7cleanupEv, @function
_ZN6icu_6722TransliteratorIDParser7cleanupEv:
.LFB3160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	_ZN6icu_67L16SPECIAL_INVERSESE(%rip), %r12
	testq	%r12, %r12
	je	.L274
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L275
	call	uhash_close_67@PLT
.L275:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	$0, _ZN6icu_67L16SPECIAL_INVERSESE(%rip)
.L274:
	movl	$0, _ZN6icu_67L24gSpecialInversesInitOnceE(%rip)
	mfence
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3160:
	.size	_ZN6icu_6722TransliteratorIDParser7cleanupEv, .-_ZN6icu_6722TransliteratorIDParser7cleanupEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser8SingleID14createInstanceEv
	.type	_ZN6icu_6722TransliteratorIDParser8SingleID14createInstanceEv, @function
_ZN6icu_6722TransliteratorIDParser8SingleID14createInstanceEv:
.LFB3138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movswl	80(%rdi), %eax
	testw	%ax, %ax
	js	.L284
	sarl	$5, %eax
.L285:
	leaq	8(%rbx), %r12
	testl	%eax, %eax
	jne	.L286
	leaq	-112(%rbp), %r13
	leaq	-120(%rbp), %rdx
	movl	$8, %ecx
	movl	$1, %esi
	leaq	_ZN6icu_67L8ANY_NULLE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6714Transliterator19createBasicInstanceERKNS_13UnicodeStringEPS2_@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L287:
	testq	%r12, %r12
	je	.L283
	movswl	144(%rbx), %eax
	testw	%ax, %ax
	js	.L289
	sarl	$5, %eax
.L290:
	testl	%eax, %eax
	je	.L283
	movl	$200, %edi
	movl	$0, -120(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L291
	movq	%rax, %rdi
	leaq	-120(%rbp), %rdx
	leaq	136(%rbx), %rsi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	-120(%rbp), %eax
	testl	%eax, %eax
	jle	.L292
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	.p2align 4,,10
	.p2align 3
.L283:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L302
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movl	148(%rbx), %eax
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L286:
	movq	%r12, %rsi
	leaq	72(%rbx), %rdi
	call	_ZN6icu_6714Transliterator19createBasicInstanceERKNS_13UnicodeStringEPS2_@PLT
	movq	%rax, %r12
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L284:
	movl	84(%rdi), %eax
	jmp	.L285
.L291:
	cmpl	$0, -120(%rbp)
	jg	.L283
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator11adoptFilterEPNS_13UnicodeFilterE@PLT
	jmp	.L283
.L302:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3138:
	.size	_ZN6icu_6722TransliteratorIDParser8SingleID14createInstanceEv, .-_ZN6icu_6722TransliteratorIDParser8SingleID14createInstanceEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser17parseGlobalFilterERKNS_13UnicodeStringERiiS4_PS1_
	.type	_ZN6icu_6722TransliteratorIDParser17parseGlobalFilterERKNS_13UnicodeStringERiiS4_PS1_, @function
_ZN6icu_6722TransliteratorIDParser17parseGlobalFilterERKNS_13UnicodeStringERiiS4_PS1_:
.LFB3147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movl	%edx, -172(%rbp)
	movq	%r8, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	movl	%eax, -176(%rbp)
	movl	(%rcx), %eax
	cmpl	$-1, %eax
	je	.L331
	cmpl	$1, %eax
	je	.L332
.L305:
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia@PLT
	movl	(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet16resemblesPatternERKNS_13UnicodeStringEi@PLT
	testb	%al, %al
	je	.L303
	movl	(%rbx), %eax
	movl	$200, %edi
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rcx
	movl	$-1, -132(%rbp)
	movq	%rcx, -144(%rbp)
	movl	%eax, -136(%rbp)
	movl	$0, -148(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L308
	leaq	-144(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-148(%rbp), %r9
	movl	$1, %ecx
	movq	%rax, %rdx
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode@PLT
	movl	-148(%rbp), %r11d
	testl	%r11d, %r11d
	jg	.L333
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r15
	movl	(%rbx), %esi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	(%r12), %rax
	movl	$2, %r10d
	movq	%r15, %rcx
	movw	%r10w, -120(%rbp)
	movl	-136(%rbp), %edx
	call	*24(%rax)
	movl	-136(%rbp), %eax
	movl	%eax, (%rbx)
	cmpl	$1, 0(%r13)
	je	.L334
.L311:
	cmpq	$0, -168(%rbp)
	je	.L312
	movl	-172(%rbp), %r9d
	movl	0(%r13), %eax
	leaq	-150(%rbp), %r12
	testl	%r9d, %r9d
	je	.L335
	testl	%eax, %eax
	je	.L336
.L317:
	movswl	-120(%rbp), %r9d
	testw	%r9w, %r9w
	js	.L318
	sarl	$5, %r9d
.L319:
	movq	-168(%rbp), %rdi
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movswl	-120(%rbp), %esi
	testw	%si, %si
	js	.L320
	sarl	$5, %esi
.L321:
	movq	-168(%rbp), %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	xorl	%edx, %edx
	movl	$59, %eax
	movl	$1, %r9d
	movw	%ax, -150(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
.L312:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-184(%rbp), %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
.L303:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L337
	addq	$152, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movl	$40, %edx
	call	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs@PLT
	testb	%al, %al
	setne	%al
	movzbl	%al, %eax
	movl	%eax, 0(%r13)
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L335:
	cmpl	$1, %eax
	je	.L338
.L314:
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L315
	sarl	$5, %ecx
.L316:
	movq	-168(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$59, %esi
	movl	$1, %ecx
	xorl	%edx, %edx
	movw	%si, -150(%rbp)
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L333:
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movl	-176(%rbp), %eax
	movl	%eax, (%rbx)
.L310:
	movq	-184(%rbp), %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L332:
	movl	$40, %edx
	call	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs@PLT
	testb	%al, %al
	jne	.L305
	movl	-176(%rbp), %eax
	xorl	%r14d, %r14d
	movl	%eax, (%rbx)
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L334:
	movl	$41, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs@PLT
	testb	%al, %al
	jne	.L311
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movl	-176(%rbp), %eax
	movq	%r15, %rdi
	movl	%eax, (%rbx)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L315:
	movl	-116(%rbp), %ecx
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L320:
	movl	-116(%rbp), %esi
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L318:
	movl	-116(%rbp), %r9d
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L336:
	movl	$40, %edx
	movq	%r12, %rcx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movw	%dx, -150(%rbp)
	movl	$1, %r9d
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movl	$41, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movw	%cx, -150(%rbp)
	movq	%r15, %rdi
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L338:
	movl	$40, %edi
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	xorl	%edx, %edx
	movw	%di, -150(%rbp)
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	$1, %r9d
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movl	$41, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	movw	%r8w, -150(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L314
.L337:
	call	__stack_chk_fail@PLT
.L308:
	movl	-176(%rbp), %eax
	movl	%eax, (%rbx)
	leaq	-144(%rbp), %rax
	movq	%rax, -184(%rbp)
	jmp	.L310
	.cfi_endproc
.LFE3147:
	.size	_ZN6icu_6722TransliteratorIDParser17parseGlobalFilterERKNS_13UnicodeStringERiiS4_PS1_, .-_ZN6icu_6722TransliteratorIDParser17parseGlobalFilterERKNS_13UnicodeStringERiiS4_PS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser15instantiateListERNS_7UVectorER10UErrorCode
	.type	_ZN6icu_6722TransliteratorIDParser15instantiateListERNS_7UVectorER10UErrorCode, @function
_ZN6icu_6722TransliteratorIDParser15instantiateListERNS_7UVectorER10UErrorCode:
.LFB3151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-176(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jle	.L367
.L341:
	leaq	_deleteSingleID(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L368
.L351:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L369
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	leaq	_deleteTransliteratorTrIDPars(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	movl	8(%r12), %r9d
	testl	%r9d, %r9d
	jle	.L342
	xorl	%r14d, %r14d
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L371:
	sarl	$5, %edx
	testl	%edx, %edx
	jne	.L370
.L345:
	addl	$1, %r14d
	cmpl	8(%r12), %r14d
	jge	.L342
.L347:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movswl	80(%rax), %edx
	testw	%dx, %dx
	jns	.L371
	movl	84(%rax), %edx
	testl	%edx, %edx
	je	.L345
.L370:
	movq	%rax, %rdi
	call	_ZN6icu_6722TransliteratorIDParser8SingleID14createInstanceEv
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L372
	movq	%rbx, %rdx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L345
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L342:
	movl	-168(%rbp), %edi
	testl	%edi, %edi
	jne	.L341
	leaq	-128(%rbp), %r15
	leaq	-184(%rbp), %rdx
	movl	$8, %ecx
	movl	$1, %esi
	leaq	_ZN6icu_67L8ANY_NULLE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6714Transliterator19createBasicInstanceERKNS_13UnicodeStringEPS2_@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r14, %r14
	je	.L373
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L341
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L368:
	leaq	_deleteTransliteratorTrIDPars(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	.p2align 4,,10
	.p2align 3
.L353:
	movl	-168(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L351
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L353
	testq	%r14, %r14
	je	.L354
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L354:
	movq	%r12, %rdi
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	jmp	.L351
.L372:
	movl	$65569, (%rbx)
	jmp	.L341
.L373:
	movl	$65568, (%rbx)
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	jmp	.L341
.L369:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3151:
	.size	_ZN6icu_6722TransliteratorIDParser15instantiateListERNS_7UVectorER10UErrorCode, .-_ZN6icu_6722TransliteratorIDParser15instantiateListERNS_7UVectorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser13parseFilterIDERKNS_13UnicodeStringERia
	.type	_ZN6icu_6722TransliteratorIDParser13parseFilterIDERKNS_13UnicodeStringERia, @function
_ZN6icu_6722TransliteratorIDParser13parseFilterIDERKNS_13UnicodeStringERia:
.LFB3155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r9d
	movl	$2, %r10d
	movl	$2, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$2, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	$2, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$616, %rsp
	movb	%dl, -609(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r9w, -568(%rbp)
	movq	%rax, -576(%rbp)
	movq	%rax, -512(%rbp)
	movq	%rax, -448(%rbp)
	movq	%rax, -384(%rbp)
	movq	%rax, -320(%rbp)
	movl	(%rsi), %eax
	movw	%r10w, -504(%rbp)
	movl	%eax, -616(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, -648(%rbp)
	leaq	-576(%rbp), %rax
	movq	%rax, -640(%rbp)
	leaq	-448(%rbp), %rax
	movq	%rax, -624(%rbp)
	leaq	-384(%rbp), %rax
	movw	%r11w, -440(%rbp)
	movq	%rax, -632(%rbp)
	movw	%r12w, -376(%rbp)
	xorl	%r12d, %r12d
	movw	%r14w, -312(%rbp)
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L375:
	movq	%r13, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia@PLT
	movswl	8(%rbx), %eax
	movl	0(%r13), %esi
	testw	%ax, %ax
	js	.L376
	sarl	$5, %eax
	cmpl	%eax, %esi
	je	.L379
.L453:
	cmpb	$0, -609(%rbp)
	je	.L380
	movswl	-312(%rbp), %eax
	testw	%ax, %ax
	js	.L381
	sarl	$5, %eax
	testl	%eax, %eax
	je	.L449
.L380:
	testw	%r12w, %r12w
	jne	.L386
	movzwl	8(%rbx), %eax
	movl	0(%r13), %edx
	testw	%ax, %ax
	js	.L389
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L390:
	cmpl	%edx, %ecx
	jbe	.L391
	testb	$2, %al
	je	.L392
	leaq	10(%rbx), %rax
	movslq	%edx, %rcx
	movzwl	(%rax,%rcx,2), %eax
	cmpw	$45, %ax
	je	.L450
.L394:
	cmpw	$47, %ax
	jne	.L391
	movswl	-376(%rbp), %ecx
	testw	%cx, %cx
	js	.L398
.L448:
	sarl	$5, %ecx
.L399:
	testl	%ecx, %ecx
	je	.L451
.L391:
	testl	%r14d, %r14d
	jne	.L379
.L386:
	leaq	-256(%rbp), %r15
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711ICU_Utility22parseUnicodeIdentifierERKNS_13UnicodeStringERi@PLT
	movswl	-248(%rbp), %eax
	testw	%ax, %ax
	js	.L401
	sarl	$5, %eax
	testl	%eax, %eax
	je	.L403
.L457:
	cmpw	$45, %r12w
	je	.L404
	cmpw	$47, %r12w
	je	.L405
	testw	%r12w, %r12w
	je	.L452
.L406:
	movq	%r15, %rdi
	addl	$1, %r14d
	xorl	%r12d, %r12d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L376:
	movl	12(%rbx), %eax
	cmpl	%eax, %esi
	jne	.L453
.L379:
	movzwl	-568(%rbp), %eax
	testw	%ax, %ax
	js	.L408
.L458:
	movswl	%ax, %edx
	sarl	$5, %edx
.L409:
	movswl	-440(%rbp), %eax
	testl	%edx, %edx
	je	.L447
	testw	%ax, %ax
	js	.L412
	sarl	$5, %eax
.L413:
	testl	%eax, %eax
	jne	.L414
	movq	-640(%rbp), %rsi
	movq	-624(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	-440(%rbp), %eax
.L447:
	leaq	-512(%rbp), %r12
.L411:
	movswl	-504(%rbp), %edx
	testw	%dx, %dx
	js	.L415
	sarl	$5, %edx
.L416:
	testl	%edx, %edx
	je	.L454
	movl	$1, %ebx
	testw	%ax, %ax
	js	.L423
.L461:
	sarl	$5, %eax
	testl	%eax, %eax
	je	.L455
.L425:
	movl	$272, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L387
	movl	$2, %edx
	movl	$2, %ecx
	movl	$2, %esi
	movl	$2, %edi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, 16(%r13)
	movw	%cx, 80(%r13)
	movw	%si, 144(%r13)
	movq	%r12, %rsi
	movw	%di, 208(%r13)
	leaq	8(%r13), %rdi
	movq	%rax, 8(%r13)
	movq	%rax, 72(%r13)
	movq	%rax, 136(%r13)
	movq	%rax, 200(%r13)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-624(%rbp), %rsi
	leaq	72(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-632(%rbp), %rsi
	leaq	136(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movb	%bl, 264(%r13)
	movq	-648(%rbp), %rsi
	leaq	200(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L387:
	movq	-648(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-632(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-624(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-640(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L456
	addq	$616, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	movl	-244(%rbp), %eax
	testl	%eax, %eax
	jne	.L457
.L403:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzwl	-568(%rbp), %eax
	testw	%ax, %ax
	jns	.L458
.L408:
	movl	-564(%rbp), %edx
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L381:
	movl	-308(%rbp), %eax
	testl	%eax, %eax
	jne	.L380
.L449:
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet16resemblesPatternERKNS_13UnicodeStringEi@PLT
	testb	%al, %al
	je	.L380
	leaq	-256(%rbp), %r11
	movl	0(%r13), %eax
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rcx
	leaq	-592(%rbp), %r15
	movq	%r11, %rdi
	movq	%r11, -656(%rbp)
	leaq	-596(%rbp), %r9
	movq	%rcx, -592(%rbp)
	movq	%r15, %rdx
	movl	$1, %ecx
	movl	%eax, -584(%rbp)
	movl	$-1, -580(%rbp)
	movl	$0, -596(%rbp)
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode@PLT
	movl	-596(%rbp), %r8d
	movq	-656(%rbp), %r11
	testl	%r8d, %r8d
	jg	.L459
	movq	(%rbx), %rax
	movq	%r11, -656(%rbp)
	movq	%rbx, %rdi
	movq	-648(%rbp), %rcx
	movl	-584(%rbp), %edx
	movl	0(%r13), %esi
	call	*24(%rax)
	movl	-584(%rbp), %eax
	movq	-656(%rbp), %r11
	movl	%eax, 0(%r13)
	movq	%r11, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L452:
	movq	-640(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L405:
	movq	-632(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L404:
	movq	-624(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L389:
	movl	12(%rbx), %ecx
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L392:
	movq	24(%rbx), %rax
	movslq	%edx, %rcx
	movzwl	(%rax,%rcx,2), %eax
	cmpw	$45, %ax
	jne	.L394
.L450:
	movswl	-440(%rbp), %ecx
	testw	%cx, %cx
	jns	.L448
	movl	-436(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L391
	.p2align 4,,10
	.p2align 3
.L451:
	addl	$1, %edx
	movl	%eax, %r12d
	movl	%edx, 0(%r13)
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L398:
	movl	-372(%rbp), %ecx
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L454:
	testw	%ax, %ax
	js	.L418
	sarl	$5, %eax
.L419:
	testl	%eax, %eax
	je	.L460
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	-504(%rbp), %eax
	testw	%ax, %ax
	js	.L421
	movswl	%ax, %edx
	sarl	$5, %edx
.L422:
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	_ZN6icu_67L3ANYE(%rip), %rcx
	xorl	%ebx, %ebx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movswl	-440(%rbp), %eax
	testw	%ax, %ax
	jns	.L461
.L423:
	movl	-436(%rbp), %eax
	testl	%eax, %eax
	jne	.L425
.L455:
	movq	-624(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	-440(%rbp), %eax
	testw	%ax, %ax
	js	.L426
	movswl	%ax, %edx
	sarl	$5, %edx
.L427:
	movq	-624(%rbp), %rdi
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	leaq	_ZN6icu_67L3ANYE(%rip), %rcx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L415:
	movl	-500(%rbp), %edx
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L414:
	movq	-640(%rbp), %rsi
	leaq	-512(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	-440(%rbp), %eax
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L459:
	movl	-616(%rbp), %eax
	movq	%r11, %rdi
	leaq	-512(%rbp), %r12
	movl	%eax, 0(%r13)
	xorl	%r13d, %r13d
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	jmp	.L387
.L421:
	movl	-500(%rbp), %edx
	jmp	.L422
.L426:
	movl	-436(%rbp), %edx
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L412:
	movl	-436(%rbp), %eax
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L418:
	movl	-436(%rbp), %eax
	jmp	.L419
.L460:
	movl	-616(%rbp), %eax
	movl	%eax, 0(%r13)
	xorl	%r13d, %r13d
	jmp	.L387
.L456:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3155:
	.size	_ZN6icu_6722TransliteratorIDParser13parseFilterIDERKNS_13UnicodeStringERia, .-_ZN6icu_6722TransliteratorIDParser13parseFilterIDERKNS_13UnicodeStringERia
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser13parseSingleIDERKNS_13UnicodeStringERiiR10UErrorCode
	.type	_ZN6icu_6722TransliteratorIDParser13parseSingleIDERKNS_13UnicodeStringERiiR10UErrorCode, @function
_ZN6icu_6722TransliteratorIDParser13parseSingleIDERKNS_13UnicodeStringERiiR10UErrorCode:
.LFB3139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	movl	$40, %edx
	subq	$40, %rsp
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	movl	%eax, -76(%rbp)
	call	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs@PLT
	testb	%al, %al
	jne	.L496
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6722TransliteratorIDParser13parseFilterIDERKNS_13UnicodeStringERia
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L470
	movl	$40, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs@PLT
	testb	%al, %al
	jne	.L463
	testl	%ebx, %ebx
	je	.L472
	movq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6722TransliteratorIDParser21specsToSpecialInverseERKNS0_5SpecsER10UErrorCode
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L535
.L492:
	leaq	200(%r12), %r14
	leaq	136(%r15), %rdi
	xorl	%r13d, %r13d
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L493:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L494:
	testq	%r13, %r13
	je	.L462
	leaq	200(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L462:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L536
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6722TransliteratorIDParser9specsToIDEPKNS0_5SpecsEi
	movq	%rax, %r15
.L491:
	testq	%r15, %r15
	jne	.L492
.L479:
	movq	-72(%rbp), %rax
	xorl	%r15d, %r15d
	movl	$7, (%rax)
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L496:
	xorl	%r12d, %r12d
.L463:
	movl	$41, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs@PLT
	testb	%al, %al
	je	.L537
.L467:
	xorl	%esi, %esi
	testl	%ebx, %ebx
	jne	.L474
	movq	%r13, %rdi
	call	_ZN6icu_6722TransliteratorIDParser9specsToIDEPKNS0_5SpecsEi
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN6icu_6722TransliteratorIDParser9specsToIDEPKNS0_5SpecsEi
	movq	%rax, %r15
	testq	%rbx, %rbx
	je	.L477
	testq	%rax, %rax
	je	.L534
	leaq	-58(%rbp), %r8
	movl	$40, %esi
	xorl	%edx, %edx
	movl	$1, %ecx
	movw	%si, -58(%rbp)
	leaq	8(%rax), %rdi
	movq	%r8, %rsi
	leaq	8(%rbx), %r14
	movq	%r8, -72(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	16(%rbx), %edx
	movq	-72(%rbp), %r8
	movq	%rax, %rdi
	testw	%dx, %dx
	js	.L480
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L481:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r8, -72(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-72(%rbp), %r8
	movl	$41, %ecx
	xorl	%edx, %edx
	movw	%cx, -58(%rbp)
	movq	%rax, %rdi
	movl	$1, %ecx
	movq	%r8, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	testq	%r12, %r12
	je	.L490
	leaq	200(%r12), %rsi
	leaq	136(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L490:
	leaq	136(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	testq	%r12, %r12
	je	.L494
	leaq	200(%r12), %r14
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L474:
	movq	%r12, %rdi
	call	_ZN6icu_6722TransliteratorIDParser9specsToIDEPKNS0_5SpecsEi
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN6icu_6722TransliteratorIDParser9specsToIDEPKNS0_5SpecsEi
	movq	%rax, %r15
	testq	%rbx, %rbx
	je	.L477
	testq	%rax, %rax
	je	.L534
	leaq	-58(%rbp), %r8
	movl	$40, %edx
	leaq	8(%rax), %rdi
	movl	$1, %ecx
	movw	%dx, -58(%rbp)
	movq	%r8, %rsi
	xorl	%edx, %edx
	leaq	8(%rbx), %r14
	movq	%r8, -72(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	16(%rbx), %edx
	movq	-72(%rbp), %r8
	movq	%rax, %rdi
	testw	%dx, %dx
	js	.L488
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L489:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r8, -72(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-72(%rbp), %r8
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%rax, %rdi
	movl	$41, %eax
	movq	%r8, %rsi
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	testq	%r13, %r13
	je	.L490
	leaq	200(%r13), %rsi
	leaq	136(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L534:
	testq	%rbx, %rbx
	je	.L477
	leaq	136(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L477:
	testq	%r15, %r15
	je	.L479
	leaq	136(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L488:
	movl	20(%rbx), %ecx
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L480:
	movl	20(%rbx), %ecx
	jmp	.L481
.L537:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6722TransliteratorIDParser13parseFilterIDERKNS_13UnicodeStringERia
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L471
	movl	$41, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs@PLT
	testb	%al, %al
	jne	.L467
.L471:
	testq	%r12, %r12
	je	.L470
	leaq	200(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L470:
	movl	-76(%rbp), %eax
	movl	%eax, (%r15)
	xorl	%r15d, %r15d
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L535:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6722TransliteratorIDParser9specsToIDEPKNS0_5SpecsEi
	movq	%rax, %r15
	jmp	.L491
.L536:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3139:
	.size	_ZN6icu_6722TransliteratorIDParser13parseSingleIDERKNS_13UnicodeStringERiiR10UErrorCode, .-_ZN6icu_6722TransliteratorIDParser13parseSingleIDERKNS_13UnicodeStringERiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser15parseCompoundIDERKNS_13UnicodeStringEiRS1_RNS_7UVectorERPNS_10UnicodeSetE
	.type	_ZN6icu_6722TransliteratorIDParser15parseCompoundIDERKNS_13UnicodeStringEiRS1_RNS_7UVectorERPNS_10UnicodeSetE, @function
_ZN6icu_6722TransliteratorIDParser15parseCompoundIDERKNS_13UnicodeStringEiRS1_RNS_7UVectorERPNS_10UnicodeSetE:
.LFB3150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	subq	$72, %rsp
	movl	%esi, -84(%rbp)
	movq	%r8, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	movl	$1, -60(%rbp)
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	movzwl	8(%r15), %eax
	movq	$0, (%r14)
	testb	$1, %al
	je	.L539
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L540:
	leaq	-60(%rbp), %rax
	movl	-84(%rbp), %edx
	leaq	-64(%rbp), %r13
	movq	%r15, %r8
	movq	%rax, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$0, -60(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser17parseGlobalFilterERKNS_13UnicodeStringERiiS4_PS1_
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L543
	movl	$59, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs@PLT
	testb	%al, %al
	je	.L608
	movl	-84(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L549
.L613:
	movq	-96(%rbp), %rax
	movq	%r14, (%rax)
.L550:
	leaq	-68(%rbp), %r14
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L609:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	-68(%rbp), %esi
	testl	%esi, %esi
	jg	.L560
	movl	$59, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs@PLT
	testb	%al, %al
	je	.L554
.L559:
	movq	%r13, %rsi
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6722TransliteratorIDParser13parseSingleIDERKNS_13UnicodeStringERiiR10UErrorCode
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L609
.L552:
	movl	8(%r12), %edx
	testl	%edx, %edx
	je	.L560
	movb	$1, -85(%rbp)
	jle	.L563
.L562:
	leaq	-70(%rbp), %rax
	xorl	%r14d, %r14d
	movq	%rax, -104(%rbp)
.L565:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movswl	16(%rax), %ecx
	leaq	8(%rax), %rsi
	testw	%cx, %cx
	js	.L566
	sarl	$5, %ecx
.L567:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	8(%r12), %eax
	subl	$1, %eax
	cmpl	%r14d, %eax
	jne	.L610
.L570:
	cmpb	$0, -85(%rbp)
	jne	.L563
.L571:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia@PLT
	movswl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L573
	sarl	$5, %eax
.L574:
	cmpl	%eax, -64(%rbp)
	jne	.L560
	movl	$1, %eax
.L538:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L611
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L541
	movswl	%ax, %edx
	sarl	$5, %edx
.L542:
	testl	%edx, %edx
	je	.L540
	andl	$31, %eax
	movw	%ax, 8(%r15)
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L541:
	movl	12(%r15), %edx
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L610:
	movq	-104(%rbp), %rsi
	movl	$59, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movw	%ax, -70(%rbp)
	addl	$1, %r14d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	%r14d, 8(%r12)
	jg	.L565
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L566:
	movl	20(%rax), %ecx
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L560:
	leaq	_deleteSingleID(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	movq	-96(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L558
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L558:
	movq	-96(%rbp), %rax
	movq	$0, (%rax)
	xorl	%eax, %eax
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L543:
	movl	-84(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L550
.L551:
	leaq	-68(%rbp), %r14
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L612:
	movq	%r12, %rdi
	movq	%r14, %rcx
	xorl	%edx, %edx
	call	_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode@PLT
	movl	-68(%rbp), %edi
	testl	%edi, %edi
	jg	.L560
	movl	$59, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs@PLT
	testb	%al, %al
	je	.L554
.L555:
	movl	-84(%rbp), %edx
	movq	%r13, %rsi
	movq	%r14, %rcx
	movq	%rbx, %rdi
	call	_ZN6icu_6722TransliteratorIDParser13parseSingleIDERKNS_13UnicodeStringERiiR10UErrorCode
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L612
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L554:
	movl	8(%r12), %ecx
	testl	%ecx, %ecx
	je	.L560
	jle	.L571
	movb	$0, -85(%rbp)
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L549:
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L608:
	movzwl	8(%r15), %eax
	testb	$1, %al
	je	.L545
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L546:
	movl	-84(%rbp), %r9d
	movl	$0, -64(%rbp)
	testl	%r9d, %r9d
	je	.L613
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L573:
	movl	12(%rbx), %eax
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L563:
	movq	%r15, %r8
	movl	-84(%rbp), %r15d
	movq	-112(%rbp), %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$1, -60(%rbp)
	movl	%r15d, %edx
	call	_ZN6icu_6722TransliteratorIDParser17parseGlobalFilterERKNS_13UnicodeStringERiiS4_PS1_
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L571
	movl	$59, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs@PLT
	cmpl	$1, %r15d
	jne	.L572
	movq	-96(%rbp), %rax
	movq	%r14, (%rax)
	jmp	.L571
.L545:
	testw	%ax, %ax
	js	.L547
	movswl	%ax, %edx
	sarl	$5, %edx
.L548:
	testl	%edx, %edx
	je	.L546
	andl	$31, %eax
	movw	%ax, 8(%r15)
	jmp	.L546
.L572:
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L571
.L547:
	movl	12(%r15), %edx
	jmp	.L548
.L611:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3150:
	.size	_ZN6icu_6722TransliteratorIDParser15parseCompoundIDERKNS_13UnicodeStringEiRS1_RNS_7UVectorERPNS_10UnicodeSetE, .-_ZN6icu_6722TransliteratorIDParser15parseCompoundIDERKNS_13UnicodeStringEiRS1_RNS_7UVectorERPNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722TransliteratorIDParser13parseFilterIDERKNS_13UnicodeStringERi
	.type	_ZN6icu_6722TransliteratorIDParser13parseFilterIDERKNS_13UnicodeStringERi, @function
_ZN6icu_6722TransliteratorIDParser13parseFilterIDERKNS_13UnicodeStringERi:
.LFB3146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movl	(%rsi), %r13d
	call	_ZN6icu_6722TransliteratorIDParser13parseFilterIDERKNS_13UnicodeStringERia
	testq	%rax, %rax
	je	.L622
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6722TransliteratorIDParser9specsToIDEPKNS0_5SpecsEi
	leaq	200(%r12), %r14
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L617
	leaq	136(%rax), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L617:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	.cfi_restore_state
	movl	%r13d, (%rbx)
	xorl	%r13d, %r13d
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3146:
	.size	_ZN6icu_6722TransliteratorIDParser13parseFilterIDERKNS_13UnicodeStringERi, .-_ZN6icu_6722TransliteratorIDParser13parseFilterIDERKNS_13UnicodeStringERi
	.local	_ZN6icu_67L4LOCKE
	.comm	_ZN6icu_67L4LOCKE,56,32
	.local	_ZN6icu_67L24gSpecialInversesInitOnceE
	.comm	_ZN6icu_67L24gSpecialInversesInitOnceE,8,8
	.local	_ZN6icu_67L16SPECIAL_INVERSESE
	.comm	_ZN6icu_67L16SPECIAL_INVERSESE,8,8
	.section	.rodata
	.align 16
	.type	_ZN6icu_67L8ANY_NULLE, @object
	.size	_ZN6icu_67L8ANY_NULLE, 18
_ZN6icu_67L8ANY_NULLE:
	.value	65
	.value	110
	.value	121
	.value	45
	.value	78
	.value	117
	.value	108
	.value	108
	.value	0
	.align 8
	.type	_ZN6icu_67L3ANYE, @object
	.size	_ZN6icu_67L3ANYE, 8
_ZN6icu_67L3ANYE:
	.value	65
	.value	110
	.value	121
	.value	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
