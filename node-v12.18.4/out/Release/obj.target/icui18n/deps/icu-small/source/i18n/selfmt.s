	.file	"selfmt.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712SelectFormat17getDynamicClassIDEv
	.type	_ZNK6icu_6712SelectFormat17getDynamicClassIDEv, @function
_ZNK6icu_6712SelectFormat17getDynamicClassIDEv:
.LFB2637:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712SelectFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2637:
	.size	_ZNK6icu_6712SelectFormat17getDynamicClassIDEv, .-_ZNK6icu_6712SelectFormat17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712SelectFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.type	_ZNK6icu_6712SelectFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, @function
_ZNK6icu_6712SelectFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE:
.LFB2657:
	.cfi_startproc
	endbr64
	movl	8(%rcx), %eax
	movl	%eax, 12(%rcx)
	ret
	.cfi_endproc
.LFE2657:
	.size	_ZNK6icu_6712SelectFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, .-_ZNK6icu_6712SelectFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712SelectFormat5cloneEv
	.type	_ZNK6icu_6712SelectFormat5cloneEv, @function
_ZNK6icu_6712SelectFormat5cloneEv:
.LFB2653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$456, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_676FormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6712SelectFormatE(%rip), %rax
	leaq	328(%rbx), %rsi
	movq	%rax, (%r12)
	leaq	328(%r12), %rdi
	call	_ZN6icu_6714MessagePatternC1ERKS0_@PLT
.L4:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2653:
	.size	_ZNK6icu_6712SelectFormat5cloneEv, .-_ZNK6icu_6712SelectFormat5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712SelectFormat16getStaticClassIDEv
	.type	_ZN6icu_6712SelectFormat16getStaticClassIDEv, @function
_ZN6icu_6712SelectFormat16getStaticClassIDEv:
.LFB2636:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712SelectFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2636:
	.size	_ZN6icu_6712SelectFormat16getStaticClassIDEv, .-_ZN6icu_6712SelectFormat16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712SelectFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712SelectFormatC2ERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712SelectFormatC2ERKNS_13UnicodeStringER10UErrorCode:
.LFB2639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$328, %r12
	subq	$8, %rsp
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6712SelectFormatE(%rip), %rax
	movq	%r13, %rsi
	movq	%rax, -328(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_6714MessagePatternC1ER10UErrorCode@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L16
.L11:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714MessagePattern16parseSelectStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L11
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714MessagePattern5clearEv@PLT
	.cfi_endproc
.LFE2639:
	.size	_ZN6icu_6712SelectFormatC2ERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712SelectFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6712SelectFormatC1ERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6712SelectFormatC1ERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6712SelectFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712SelectFormatC2ERKS0_
	.type	_ZN6icu_6712SelectFormatC2ERKS0_, @function
_ZN6icu_6712SelectFormatC2ERKS0_:
.LFB2642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_676FormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6712SelectFormatE(%rip), %rax
	leaq	328(%r12), %rsi
	movq	%rax, (%rbx)
	leaq	328(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714MessagePatternC1ERKS0_@PLT
	.cfi_endproc
.LFE2642:
	.size	_ZN6icu_6712SelectFormatC2ERKS0_, .-_ZN6icu_6712SelectFormatC2ERKS0_
	.globl	_ZN6icu_6712SelectFormatC1ERKS0_
	.set	_ZN6icu_6712SelectFormatC1ERKS0_,_ZN6icu_6712SelectFormatC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712SelectFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712SelectFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712SelectFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode:
.LFB2648:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L27
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	328(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%r12, %rdi
	movq	%rdx, %rbx
	xorl	%edx, %edx
	call	_ZN6icu_6714MessagePattern16parseSelectStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L28
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	popq	%rbx
	.cfi_restore 3
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714MessagePattern5clearEv@PLT
	.cfi_endproc
.LFE2648:
	.size	_ZN6icu_6712SelectFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712SelectFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712SelectFormat9toPatternERNS_13UnicodeStringE
	.type	_ZN6icu_6712SelectFormat9toPatternERNS_13UnicodeStringE, @function
_ZN6icu_6712SelectFormat9toPatternERNS_13UnicodeStringE:
.LFB2651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movl	424(%rdi), %eax
	testl	%eax, %eax
	je	.L35
	movswl	352(%rdi), %ecx
	leaq	344(%rdi), %rsi
	testw	%cx, %cx
	js	.L32
	sarl	$5, %ecx
.L33:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	356(%rdi), %ecx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2651:
	.size	_ZN6icu_6712SelectFormat9toPatternERNS_13UnicodeStringE, .-_ZN6icu_6712SelectFormat9toPatternERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712SelectFormataSERKS0_
	.type	_ZN6icu_6712SelectFormataSERKS0_, @function
_ZN6icu_6712SelectFormataSERKS0_:
.LFB2654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpq	%rsi, %rdi
	je	.L37
	addq	$328, %rsi
	leaq	328(%rdi), %rdi
	call	_ZN6icu_6714MessagePatternaSERKS0_@PLT
.L37:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2654:
	.size	_ZN6icu_6712SelectFormataSERKS0_, .-_ZN6icu_6712SelectFormataSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712SelectFormatD2Ev
	.type	_ZN6icu_6712SelectFormatD2Ev, @function
_ZN6icu_6712SelectFormatD2Ev:
.LFB2645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712SelectFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	328(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -328(%rdi)
	call	_ZN6icu_6714MessagePatternD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676FormatD2Ev@PLT
	.cfi_endproc
.LFE2645:
	.size	_ZN6icu_6712SelectFormatD2Ev, .-_ZN6icu_6712SelectFormatD2Ev
	.globl	_ZN6icu_6712SelectFormatD1Ev
	.set	_ZN6icu_6712SelectFormatD1Ev,_ZN6icu_6712SelectFormatD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712SelectFormatD0Ev
	.type	_ZN6icu_6712SelectFormatD0Ev, @function
_ZN6icu_6712SelectFormatD0Ev:
.LFB2647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712SelectFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	328(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -328(%rdi)
	call	_ZN6icu_6714MessagePatternD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676FormatD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2647:
	.size	_ZN6icu_6712SelectFormatD0Ev, .-_ZN6icu_6712SelectFormatD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712SelectFormat14findSubMessageERKNS_14MessagePatternEiRKNS_13UnicodeStringER10UErrorCode.part.0, @function
_ZN6icu_6712SelectFormat14findSubMessageERKNS_14MessagePatternEiRKNS_13UnicodeStringER10UErrorCode.part.0:
.LFB3567:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	xorl	%esi, %esi
	subq	$136, %rsp
	movq	%rdx, -160(%rbp)
	leaq	-136(%rbp), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZN6icu_67L20SELECT_KEYWORD_OTHERE(%rip), %rax
	movq	%rax, -136(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	96(%r14), %eax
	movq	88(%r14), %r13
	xorl	%r15d, %r15d
	movq	-160(%rbp), %r11
	movl	%eax, -148(%rbp)
	leaq	16(%r14), %rax
	movq	%rax, -168(%rbp)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L75:
	testb	%dl, %dl
	jne	.L46
	movswl	%ax, %r9d
	xorl	%r8d, %r8d
	sarl	$5, %r9d
.L63:
	movl	%r9d, %edx
	subl	%r8d, %edx
	cmpl	%r9d, %edx
	cmovle	%edx, %r9d
.L50:
	leaq	10(%r11), %rcx
	testb	$2, %al
	jne	.L52
	movq	24(%r11), %rcx
.L52:
	movzwl	8(%r13), %edx
	movl	4(%r13), %esi
	movq	%r11, -160(%rbp)
	movq	-168(%rbp), %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movq	-160(%rbp), %r11
	testb	%al, %al
	je	.L66
.L76:
	testl	%r15d, %r15d
	jne	.L53
	movzwl	-120(%rbp), %eax
	movl	%eax, %edx
	andl	$1, %edx
	testw	%ax, %ax
	js	.L54
	testb	%dl, %dl
	jne	.L55
	movswl	%ax, %r9d
	xorl	%r8d, %r8d
	sarl	$5, %r9d
.L62:
	movl	%r9d, %edx
	subl	%r8d, %edx
	cmpl	%r9d, %edx
	cmovle	%edx, %r9d
.L59:
	testb	$2, %al
	movzwl	8(%r13), %edx
	movl	4(%r13), %esi
	leaq	-118(%rbp), %rcx
	movq	-168(%rbp), %rdi
	cmove	-104(%rbp), %rcx
	movq	%r11, -160(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movq	-160(%rbp), %r11
.L58:
	testb	%al, %al
	cmove	%r12d, %r15d
.L53:
	movq	88(%r14), %r13
	cmpl	%r12d, 28(%r13,%rbx)
	cmovge	28(%r13,%rbx), %r12d
	movl	%r12d, %ebx
	addl	$1, %ebx
	cmpl	%ebx, -148(%rbp)
	jle	.L44
.L61:
	leal	1(%rbx), %r12d
	movslq	%ebx, %rbx
	salq	$4, %rbx
	addq	%rbx, %r13
	cmpl	$6, 0(%r13)
	je	.L44
	movzwl	8(%r11), %eax
	movl	%eax, %edx
	andl	$1, %edx
	testw	%ax, %ax
	jns	.L75
	movl	12(%r11), %r9d
	testb	%dl, %dl
	jne	.L46
	testl	%r9d, %r9d
	movl	$0, %r8d
	cmovle	%r9d, %r8d
	jns	.L63
	xorl	%r9d, %r9d
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L46:
	movzbl	24(%r14), %eax
	notl	%eax
	andl	$1, %eax
	testb	%al, %al
	jne	.L76
.L66:
	movl	%r12d, %r15d
.L44:
	movq	-176(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$136, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movzbl	24(%r14), %eax
	notl	%eax
	andl	$1, %eax
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L54:
	movl	-116(%rbp), %r9d
	testb	%dl, %dl
	jne	.L55
	testl	%r9d, %r9d
	movl	%r15d, %r8d
	cmovle	%r9d, %r8d
	jns	.L62
	xorl	%r9d, %r9d
	jmp	.L59
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3567:
	.size	_ZN6icu_6712SelectFormat14findSubMessageERKNS_14MessagePatternEiRKNS_13UnicodeStringER10UErrorCode.part.0, .-_ZN6icu_6712SelectFormat14findSubMessageERKNS_14MessagePatternEiRKNS_13UnicodeStringER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712SelectFormat14findSubMessageERKNS_14MessagePatternEiRKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712SelectFormat14findSubMessageERKNS_14MessagePatternEiRKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712SelectFormat14findSubMessageERKNS_14MessagePatternEiRKNS_13UnicodeStringER10UErrorCode:
.LFB2652:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L80
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	jmp	_ZN6icu_6712SelectFormat14findSubMessageERKNS_14MessagePatternEiRKNS_13UnicodeStringER10UErrorCode.part.0
	.cfi_endproc
.LFE2652:
	.size	_ZN6icu_6712SelectFormat14findSubMessageERKNS_14MessagePatternEiRKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712SelectFormat14findSubMessageERKNS_14MessagePatternEiRKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712SelectFormat6formatERKNS_13UnicodeStringERS1_RNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6712SelectFormat6formatERKNS_13UnicodeStringERS1_RNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6712SelectFormat6formatERKNS_13UnicodeStringERS1_RNS_13FieldPositionER10UErrorCode:
.LFB2650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L94
	movzwl	8(%rsi), %eax
	movq	%rdi, %rbx
	movq	%rsi, %r13
	movq	%r8, %r12
	testw	%ax, %ax
	js	.L83
	movswl	%ax, %esi
	sarl	$5, %esi
.L84:
	testb	$17, %al
	jne	.L92
	leaq	10(%r13), %rdi
	testb	$2, %al
	jne	.L85
	movq	24(%r13), %rdi
.L85:
	call	_ZN6icu_6712PatternProps12isIdentifierEPKDsi@PLT
	testb	%al, %al
	jne	.L87
	movl	424(%rbx), %ecx
	movl	$1, (%r12)
	leaq	328(%rbx), %r15
	xorl	%esi, %esi
	testl	%ecx, %ecx
	je	.L89
.L90:
	cmpl	$1, 336(%rbx)
	je	.L91
.L96:
	movq	416(%rbx), %rcx
	movslq	%esi, %rax
	movq	%r14, %rdi
	salq	$4, %rax
	addq	%rcx, %rax
	movzwl	8(%rax), %edx
	addl	4(%rax), %edx
	cmpl	%esi, 12(%rax)
	cmovge	12(%rax), %esi
	movslq	%esi, %rsi
	salq	$4, %rsi
	movl	4(%rcx,%rsi), %ecx
	leaq	344(%rbx), %rsi
	subl	%edx, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
.L94:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movl	12(%rsi), %esi
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L87:
	movl	424(%rbx), %edx
	testl	%edx, %edx
	je	.L89
	movl	(%r12), %eax
	leaq	328(%rbx), %r15
	testl	%eax, %eax
	jg	.L93
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6712SelectFormat14findSubMessageERKNS_14MessagePatternEiRKNS_13UnicodeStringER10UErrorCode.part.0
	cmpl	$1, 336(%rbx)
	movl	%eax, %esi
	jne	.L96
.L91:
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711MessageImpl33appendSubMessageWithoutSkipSyntaxERKNS_14MessagePatternEiRNS_13UnicodeStringE@PLT
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movl	$27, (%r12)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L92:
	xorl	%edi, %edi
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%esi, %esi
	jmp	.L90
	.cfi_endproc
.LFE2650:
	.size	_ZNK6icu_6712SelectFormat6formatERKNS_13UnicodeStringERS1_RNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6712SelectFormat6formatERKNS_13UnicodeStringERS1_RNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712SelectFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6712SelectFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6712SelectFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB2649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L101
.L98:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movq	%rdi, %r14
	movq	%rsi, %rdi
	movq	%rsi, %r12
	movq	%rcx, %r13
	movq	%r8, %rbx
	call	_ZNK6icu_6711Formattable7getTypeEv@PLT
	cmpl	$3, %eax
	je	.L102
	movl	$1, (%rbx)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6711Formattable9getStringER10UErrorCode@PLT
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rcx
	popq	%rbx
	movq	%r15, %rdx
	popq	%r12
	movq	%r14, %rdi
	popq	%r13
	movq	%rax, %rsi
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6712SelectFormat6formatERKNS_13UnicodeStringERS1_RNS_13FieldPositionER10UErrorCode
	.cfi_endproc
.LFE2649:
	.size	_ZNK6icu_6712SelectFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6712SelectFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712SelectFormateqERKNS_6FormatE
	.type	_ZNK6icu_6712SelectFormateqERKNS_6FormatE, @function
_ZNK6icu_6712SelectFormateqERKNS_6FormatE:
.LFB2655:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L105
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZNK6icu_676FormateqERKS0_@PLT
	testb	%al, %al
	jne	.L113
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	leaq	328(%r12), %rsi
	leaq	328(%rbx), %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6714MessagePatterneqERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2655:
	.size	_ZNK6icu_6712SelectFormateqERKNS_6FormatE, .-_ZNK6icu_6712SelectFormateqERKNS_6FormatE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712SelectFormatneERKNS_6FormatE
	.type	_ZNK6icu_6712SelectFormatneERKNS_6FormatE, @function
_ZNK6icu_6712SelectFormatneERKNS_6FormatE:
.LFB2656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_6712SelectFormateqERKNS_6FormatE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L115
	xorl	%eax, %eax
	cmpq	%rdi, %rsi
	je	.L114
	call	_ZNK6icu_676FormateqERKS0_@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	jne	.L121
.L114:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	leaq	328(%r13), %rsi
	leaq	328(%r12), %rdi
	call	_ZNK6icu_6714MessagePatterneqERKS0_@PLT
	popq	%r12
	popq	%r13
	testb	%al, %al
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	call	*%rax
	popq	%r12
	popq	%r13
	testb	%al, %al
	popq	%rbp
	.cfi_def_cfa 7, 8
	sete	%al
	ret
	.cfi_endproc
.LFE2656:
	.size	_ZNK6icu_6712SelectFormatneERKNS_6FormatE, .-_ZNK6icu_6712SelectFormatneERKNS_6FormatE
	.weak	_ZTSN6icu_6712SelectFormatE
	.section	.rodata._ZTSN6icu_6712SelectFormatE,"aG",@progbits,_ZTSN6icu_6712SelectFormatE,comdat
	.align 16
	.type	_ZTSN6icu_6712SelectFormatE, @object
	.size	_ZTSN6icu_6712SelectFormatE, 24
_ZTSN6icu_6712SelectFormatE:
	.string	"N6icu_6712SelectFormatE"
	.weak	_ZTIN6icu_6712SelectFormatE
	.section	.data.rel.ro._ZTIN6icu_6712SelectFormatE,"awG",@progbits,_ZTIN6icu_6712SelectFormatE,comdat
	.align 8
	.type	_ZTIN6icu_6712SelectFormatE, @object
	.size	_ZTIN6icu_6712SelectFormatE, 24
_ZTIN6icu_6712SelectFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712SelectFormatE
	.quad	_ZTIN6icu_676FormatE
	.weak	_ZTVN6icu_6712SelectFormatE
	.section	.data.rel.ro._ZTVN6icu_6712SelectFormatE,"awG",@progbits,_ZTVN6icu_6712SelectFormatE,comdat
	.align 8
	.type	_ZTVN6icu_6712SelectFormatE, @object
	.size	_ZTVN6icu_6712SelectFormatE, 88
_ZTVN6icu_6712SelectFormatE:
	.quad	0
	.quad	_ZTIN6icu_6712SelectFormatE
	.quad	_ZN6icu_6712SelectFormatD1Ev
	.quad	_ZN6icu_6712SelectFormatD0Ev
	.quad	_ZNK6icu_6712SelectFormat17getDynamicClassIDEv
	.quad	_ZNK6icu_6712SelectFormateqERKNS_6FormatE
	.quad	_ZNK6icu_6712SelectFormat5cloneEv
	.quad	_ZNK6icu_6712SelectFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_676Format6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712SelectFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.quad	_ZNK6icu_6712SelectFormatneERKNS_6FormatE
	.section	.rodata
	.align 8
	.type	_ZN6icu_67L20SELECT_KEYWORD_OTHERE, @object
	.size	_ZN6icu_67L20SELECT_KEYWORD_OTHERE, 12
_ZN6icu_67L20SELECT_KEYWORD_OTHERE:
	.value	111
	.value	116
	.value	104
	.value	101
	.value	114
	.value	0
	.local	_ZZN6icu_6712SelectFormat16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6712SelectFormat16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
